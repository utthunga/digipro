#############################################################################
# Copyright (c) 2017,2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org/flukept/starsky
# Author    : Lisa Leif
# Origin    : Starsky
#
# Doxygen related tools.
#
#############################################################################
# Use of the software source code and warranty disclaimers are
# identified in the Software Agreement associated herewith.
#############################################################################


#############################################################################
# Causes a fatal error and stops if Doxygen cannot be located.
#
function(flk_stop_if_doxygen_not_found)
    if(OPTION_BUILD_DOCS)
        find_package(Doxygen)
    
        if (NOT ${DOXYGEN_FOUND})
            message("ERROR ************************")
            message("Doxygen not found! Have you done the following?")
            message("(1) Install Doxygen. Instructions in the packaged binary distribution that we haven't quite made yet.  - Change this when you know more.")
    
            message(FATAL_ERROR "ERROR: Doxygen is required")
        else()
            message("Doxygen Path = ${DOXYGEN_EXECUTABLE}")
        endif()
        
        # Clear the docs_target name from the cache
        unset(DOXYGEN_DOCS_TARGET CACHE)
        
        # Clear the list of all doxygen files from the cache.
        unset(ALL_DOXYGEN_FILES CACHE)
    endif(OPTION_BUILD_DOCS)
endfunction()


#############################################################################
# INTERNAL USE ONLY!
#
# Configures standard Doxygen settings. This is used internally by
# flk_add_doxygen_sources and flk_create_docs_target.
#
function(flk_generate_doxygen_configuration target_name sources)

    # WHAT'S GOING ON HERE?
    #
    # The list of sources is a ';' separated list (lists passed as a single
    # string are converted to a ';' separated list by cmake).
    #
    # ##EXAMPLE##
    # foo.cpp;bar.cpp;bas.cpp
    #
    # The variable DOXYGEN_INPUT will be subsituted by configure_file command.
    # The Doxyfile.in (the input template) contains this line. When read by
    # configure_file, the @DOXYGEN_INPUT@ will be replaced with the string
    # contained in the cmake variable DOXYGEN_INPUT.
    #
    # ##EXAMPLE##
    # INPUT                  = @DOXYGEN_INPUT@
    #
    # The Doxyfile is expecting the value assigned to the variable INPUT to
    # be a space separated list of files and directories. Line continuation
    # characters ('\') are allowed at the end of lines to split long lines.
    # The string substitution command below will turn the argument sources
    # into a string that will replace @DOXYGEN_INPUT@ in the Doxyfile with
    # such a list.
    #
    # ##EXAMPLE##
    # INPUT                  = foo.cpp \
    #                          bar.cpp \
    #                          bas.cpp
    string(REPLACE ";" " \\\n                         " DOXYGEN_INPUT "${sources}")
    
    # The Doxyfile.in template defines the output directory with another
    # substitution.
    #
    # ##EXAMPLE##
    # OUTPUT_DIRECTORY       = @DOXYGEN_OUTPUT_DIR@    
    set(DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/docs)
    
    # The Doxyfile.in template is located in our cmake modules directory.
    find_file(DOXYGEN_INPUT_FILE Doxyfile.in
        PATHS ${CMAKE_MODULE_PATH}
        NO_DEFAULT_PATH
        NO_CMAKE_FIND_ROOT_PATH
    )
    
    # Process the template and produce the Doxyfile for this config.
    if(DOXYGEN_INPUT_FILE)
        configure_file(
            ${DOXYGEN_INPUT_FILE}
            ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                @ONLY
        )
    else()
        message("ERROR ************************")
        message("Could not find Doxyfile.in in ${CMAKE_MODULE_PATH}")
        message(FATAL_ERROR "ERROR: Doxygen.in is missing")        
    endif()
    
    # Create a custom target to build this projects documentation
    add_custom_target(${target_name}
        ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating documentation with Doxygen"
        VERBATUM
    )
    
    # Add the OUTPUT_DIRECTORY to the list directories to clean.
    set_property(DIRECTORY
        PROPERTY ADDITIONAL_MAKE_CLEAN_FILES
        "${DOXYGEN_OUTPUT_DIR}"
        APPEND
    )
    
endfunction()


#############################################################################
# Set up doxygen to scan sources and directories in this project by running
# the target "make <project>_docs". Source can be a list of individual
# source files, or a directory to scan using the FILE_PATTERNS defined
# in the Doxyfile.in template.
#
# Calling this function also adds this list of sources and directories to
# any collection started by flk_begin_doxygen_collection.
#
function(flk_add_doxygen_sources project_target sources)
    if(OPTION_BUILD_DOCS)
        set(project_target ${project_target}_docs)
        
        # Create a list of all the doxygen sources to use when generating
        # the docs target (flk_create_docs_target())
        set(collected_sources ${ALL_DOXYGEN_FILES} ${sources})
        set(ALL_DOXYGEN_FILES ${collected_sources}
            CACHE INTERNAL
            "List of files and directories for a doxygen collection")
        flk_generate_doxygen_configuration(${project_target} "${sources}")
    endif(OPTION_BUILD_DOCS)
endfunction()


#############################################################################
# Begin a new collection of files to scan with doxygen. Each list of files
# and directories collected by flk_add_doxygen_sources will be added to this
# enclosing collection. When the matching end collection is called, a
# build target will be generated to run doxygen on the collected list of
# files and directories to scan.
#
# The argument docs_target is the name of the build target that will build
# the collection. Build target names must be unique across a build system.
function(flk_begin_doxygen_collection docs_target)
    if(OPTION_BUILD_DOCS)
    
        # Save the docs_target name (calls to this function cannot be
        # nested.
        if(DEFINED DOXYGEN_DOCS_TARGET)
            message("ERROR ************************")
            message("Trying to begin an new doxygen collection for the target ${docs_target}")
            message("when a collection was already started for the target ${DOXYGEN_DOCS_TARGET}.")
            message(FATAL_ERROR
                "ERROR: Calls to flk_begin_doxygen_collection cannot be nested")
        endif()
        
        # Clear the list of all doxygen files from the cache.
        unset(ALL_DOXYGEN_FILES CACHE)
        
        # Save the docs_target in the cache.
        set(DOXYGEN_DOCS_TARGET ${docs_target}
           CACHE INTERNAL
           "Docs target for a doxygen collection.")        
        
    endif(OPTION_BUILD_DOCS)
endfunction()


#############################################################################
# Set up doxgen to scan all the sources collected by flk_add_doxygen_sources
# when running the target "make ${docs_target}". Once this target is
# generated, the collected list of sources collected by
# flk_add_doxygen_sources is cleared so the collection process starts over.
#
function(flk_end_doxygen_collection)
    if(OPTION_BUILD_DOCS)
        flk_generate_doxygen_configuration(${DOXYGEN_DOCS_TARGET} "${ALL_DOXYGEN_FILES}")
        
        # Clear the docs_target name from the cache
        unset(DOXYGEN_DOCS_TARGET CACHE)
    endif(OPTION_BUILD_DOCS)
endfunction()
