################################################################################
# Copyright (c) 2017,2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org:flukept/sw-components-log4cplus-adapter.git
# Author: Ryan Carney
# Origin: Starsky
#
# Module to download the external-cmake-modules repository at configure time
#
################################################################################
# Use of the software source code and warranty disclaimers are
# identified in the Software Agreement associated herewith.
################################################################################


#############################################################################
# Function to download the external-cmake-modules repository at configure time
#
function(ecm_download_external_cmake_modules)
    set(options)
    set(oneValueArgs GIT_TAG)
    set(multiValueArgs)
    cmake_parse_arguments(
        ARG
        "${options}"
        "${oneValueArgs}"
        "${multiValueArgs}"
        ${ARGN}
    )

    set(ECM_DOWNLOAD_DIR external-cmake-modules-download)

    # NOTE: NO_CMAKE_FIND_ROOT_PATH needed to find files outside of toolchain
    # root Necessary when cross compiling with Yocto generated toolchain
    find_file(
        INPUT_FILE
        ecm_CMakeLists.txt.in
        PATHS ${CMAKE_MODULE_PATH}
        NO_CMAKE_FIND_ROOT_PATH
        NO_DEFAULT_PATH
    )

    configure_file(
        ${INPUT_FILE}
        ${ECM_DOWNLOAD_DIR}/CMakeLists.txt
    )

    execute_process(
        COMMAND ${CMAKE_COMMAND} .
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/${ECM_DOWNLOAD_DIR}"
    )

    execute_process(
        COMMAND "${CMAKE_COMMAND}" --build .
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/${ECM_DOWNLOAD_DIR}"
    )

    set(EXTERNAL_CMAKE_MODULES_DIR "${CMAKE_BINARY_DIR}/external-cmake-modules")
    set(EXTERNAL_CMAKE_MODULES_DIR ${EXTERNAL_CMAKE_MODULES_DIR} PARENT_SCOPE)

    set(
        EXTERNAL_CMAKE_MODULES_STARSKY_DIR
        "${CMAKE_BINARY_DIR}/external-cmake-modules/project/starsky"
    )
    set(
        EXTERNAL_CMAKE_MODULES_STARSKY_DIR
        ${EXTERNAL_CMAKE_MODULES_STARSKY_DIR} PARENT_SCOPE
    )


    # Make the external-cmake-modules available to import()
    list(APPEND CMAKE_MODULE_PATH ${EXTERNAL_CMAKE_MODULES_DIR})
    list(APPEND CMAKE_MODULE_PATH ${EXTERNAL_CMAKE_MODULES_STARSKY_DIR})
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} PARENT_SCOPE)
endfunction()

#############################################################################
# Function to download and build dependencies provided by external-cmake-modules
# repository at configure time
#
function(ecm_download_and_build_dependencies)
    set(options)
    set(oneValueArgs INSTALL_DIR)
    set(multiValueArgs)
    cmake_parse_arguments(
        ARG
        "${options}"
        "${oneValueArgs}"
        "${multiValueArgs}"
        ${ARGN}
    )

    # NOTE: NO_CMAKE_FIND_ROOT_PATH needed to find files outside of toolchain
    # root Necessary when cross compiling with Yocto generated toolchain
    find_file(
        DEPS_INPUT_FILE
        ecm_download_and_build_deps_CMakeLists.txt.in
        PATHS ${CMAKE_MODULE_PATH}
        NO_CMAKE_FIND_ROOT_PATH
        NO_DEFAULT_PATH
    )

    configure_file(
        ${DEPS_INPUT_FILE}
        ${ARG_INSTALL_DIR}/CMakeLists.txt
    )

    execute_process(
        COMMAND ${CMAKE_COMMAND} .
        WORKING_DIRECTORY "${ARG_INSTALL_DIR}"
    )

    execute_process(
        COMMAND "${CMAKE_COMMAND}" --build .
        WORKING_DIRECTORY "${ARG_INSTALL_DIR}"
    )
endfunction()
