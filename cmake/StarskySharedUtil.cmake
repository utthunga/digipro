#############################################################################
# Copyright (c) 2017,2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org/flukept/starsky
# Author    : Matthew Kraus
# Origin    : Starsky
#
# Shared library for Starsky. Things that are shared (errors, config files,
# etc) are in this library that are common between the Starsky and Digipro
# projects.
#
#############################################################################
# Use of the software source code and warranty disclaimers are
# identified in the Software Agreement associated herewith.
#############################################################################


#############################################################################
# Downloads the starsky_shared library
function(flk_configure_starsky_shared)
    include(CMakeParseArguments)
    set(options)
    set(oneValueArgs
        GIT_TAG
        INSTALL_DIR
        SOURCE_DIR
    )
    set(multiValueArgs)
    cmake_parse_arguments(
        ARG
        "${options}"
        "${oneValueArgs}"
        "${multiValueArgs}"
        ${ARGN}
    )

    include(ExternalProject)

    set(PROJ_NAME "starsky_shared")
    set(EXTERNAL_PROJ external_proj_${PROJ_NAME})

    if("${ARG_INSTALL_DIR}" STREQUAL "")
        set(ARG_INSTALL_DIR ${CMAKE_BINARY_DIR})
    endif()

    set(BUILD_BYPRODUCTS "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}shared_errors${CMAKE_STATIC_LIBRARY_SUFFIX}")


    list(APPEND CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX:STRING=<INSTALL_DIR>")
    list(APPEND CMAKE_ARGS "-DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}")
    list(APPEND CMAKE_ARGS "-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN_FILE}")

    ExternalProject_Add(
        ${EXTERNAL_PROJ}
        GIT_REPOSITORY "git@bitbucket.org:flukept/starsky_shared.git"
        GIT_TAG ${ARG_GIT_TAG}
        INSTALL_DIR ${ARG_INSTALL_DIR}
        SOURCE_DIR ${ARG_SOURCE_DIR}
        CMAKE_ARGS ${CMAKE_ARGS}
        # setting BUILD_BYPRODUCTS is required to make the ninja generator work
        BUILD_BYPRODUCTS ${BUILD_BYPRODUCTS}
    )

    ExternalProject_Get_Property(${EXTERNAL_PROJ} BINARY_DIR)
    set(STARSKY_SHARED_INCLUDE_DIRS "${ARG_INSTALL_DIR}/include")
    set(STARSKY_SHARED_INCLUDE_DIRS "${STARSKY_SHARED_INCLUDE_DIRS}" PARENT_SCOPE)

    # file(MAKE_DIRECTORY) Required due to the bug related to specifying
    # INTERFACE_INCLUDE_DIRECTORIES that don't exist yet:
    # https://gitlab.kitware.com/cmake/cmake/issues/15052
    file(MAKE_DIRECTORY ${STARSKY_SHARED_INCLUDE_DIRS})
    file(MAKE_DIRECTORY ${STARSKY_SHARED_INCLUDE_DIRS}/ProtoMessage)


    add_library(shared_errors STATIC IMPORTED)
    add_dependencies(shared_errors ${EXTERNAL_PROJ})
    set_target_properties(shared_errors
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION ${BUILD_BYPRODUCTS}
    )

    add_library(shared_custom_menu STATIC IMPORTED)
    add_dependencies(shared_custom_menu ${EXTERNAL_PROJ})
    set_target_properties(shared_custom_menu
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}shared_custom_menu${CMAKE_STATIC_LIBRARY_SUFFIX}"
    )

    add_library(starsky_settings STATIC IMPORTED)
    add_dependencies(starsky_settings ${EXTERNAL_PROJ})
    set_target_properties(starsky_settings
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}settings${CMAKE_STATIC_LIBRARY_SUFFIX}"
    )

    add_library(shared_proto_message STATIC IMPORTED)
    add_dependencies(shared_proto_message ${EXTERNAL_PROJ})
    set_target_properties(shared_proto_message
       PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${STARSKY_SHARED_INCLUDE_DIRS}/ProtoMessage;${STARSKY_SHARED_INCLUDE_DIRS}"
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}shared_proto_message${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES protobuf
    )

    add_library(starsky_proto_message_test_lib_static STATIC IMPORTED)
    add_dependencies(starsky_proto_message_test_lib_static ${EXTERNAL_PROJ})
    set_target_properties(starsky_proto_message_test_lib_static
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}proto_message_test${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES protobuf
    )

    find_package(Qt5 5.8.0 REQUIRED COMPONENTS Core DBus)

    # add a replacement for protoFilesGenerated target
    add_custom_target(protoFilesGenerated)
    add_dependencies(protoFilesGenerated shared_proto_message)

    add_library(starsky_protorpc_lib_static STATIC IMPORTED)
    add_dependencies(starsky_protorpc_lib_static ${EXTERNAL_PROJ})
    set_target_properties(starsky_protorpc_lib_static
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}protorpc${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES "Qt5::Core;jsoncpp;protobuf;shared_proto_message;shared_errors;log4cplus;log4cplus_adapter"
    )

    add_library(starsky_proto_dbus_lib_static STATIC IMPORTED)
    add_dependencies(starsky_proto_dbus_lib_static ${EXTERNAL_PROJ})
    set_target_properties(starsky_proto_dbus_lib_static
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}protodbus${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES "Qt5::Core;Qt5::DBus;starsky_protorpc_lib_static;shared_errors;protobuf;log4cplus_adapter;log4cplus"
    )

    add_library(starsky_pubsub_lib_static STATIC IMPORTED)
    add_dependencies(starsky_pubsub_lib_static ${EXTERNAL_PROJ})
    set_target_properties(starsky_pubsub_lib_static
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}pubsub${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES "starsky_protorpc_lib_static;starsky_proto_dbus_lib_static;Qt5::Core;shared_errors"
    )

    add_library(starsky_api_lib_static STATIC IMPORTED)
    add_dependencies(starsky_api_lib_static ${EXTERNAL_PROJ})
    set_target_properties(starsky_api_lib_static
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${STARSKY_SHARED_INCLUDE_DIRS}
        IMPORTED_LOCATION "${ARG_INSTALL_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}api${CMAKE_STATIC_LIBRARY_SUFFIX}"
        INTERFACE_LINK_LIBRARIES "shared_errors"
    )
endfunction()
