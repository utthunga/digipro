#############################################################################
# Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org/flukept/starsky
# Author    : Lisa Leif
# Origin    : Starsky
#
# Clang-Tidy related tools.
#
#############################################################################
# Use of the software source code and warranty disclaimers are
# identified in the Software Agreement associated herewith.
#############################################################################


#############################################################################
# Causes a fatal error and stops if Clang cannot be located.
#
function(flk_stop_if_clang_not_found)
    #    find_program(CLANG "clang")
    
    #if (NOT CLANG)
    #    message("ERROR ************************")
    #    message("clang not found!  Have you done the following?")
    #    message("(1) Install clang - 'sudo apt-get install clang'. ")
    
    #    message(FATAL_ERROR "ERROR: Clang is required")
    #else()
    #    message("clang Path = ${CLANG}")
    #endif()

    find_program(CLANG_FORMAT "clang-format")
    
    if (NOT CLANG_FORMAT)
        message("ERROR ************************")
        message("Clang-format not found!  Have you done the following?")
        message("(1) Install Clang-format - 'sudo apt-get install clang-format'. ")
    
        message(FATAL_ERROR "ERROR: Clang-format is required")
    else()
        message("Clang-format Path = ${CLANG_FORMAT}")
    endif()
        
    find_program(CLANG_TIDY "clang-tidy")
    
    if (NOT CLANG_TIDY)
        message("ERROR ************************")
        message("Clang-tidy not found!  Have you done the following?")
        message("(1) Install Clang-tidy - 'sudo apt-get install clang-tidy'. ")
    
        message(FATAL_ERROR "ERROR: Clang-tidy is required")
    else()
        message("Clang-tidy Path = ${CLANG_TIDY}")
    endif()

     # Clear the clang_target name from the cache
     unset(CLANG_TARGET CACHE)
        
     # Clear the list of all CXX source files from the cache.
     unset(CPP_SOURCE_FILES CACHE)

     # Clear the list of all source files from the cache.
     unset(ALL_SOURCE_FILES CACHE)
endfunction()


#############################################################################
# Set up clang to scan sources and directories in this project by running
# the target "make <project>_clang". Source can be a list of individual
# source files.
#
# Calling this function also adds this list of sources and directories to
# any collection started by flk_begin_clang_collection.
#
function(flk_add_clang_sources project_target cppSources allSources)
        
    # Create a list of all the clang sources to use when generating
    # the clang target (flk_create_clang_target())
    set(collected_cpp_sources ${CPP_SOURCE_FILES} ${cppSources})
    set(CPP_SOURCE_FILES ${collected_cpp_sources}
        CACHE INTERNAL
        "List of cpp files for a clang collection")

    set(collected_all_sources ${ALL_SOURCE_FILES} ${allSources})
    set(ALL_SOURCE_FILES ${collected_all_sources}
        CACHE INTERNAL
        "List of files for a clang collection")

    flk_generate_clang_configuration(${project_target} "${cppSources}" "${allSources}")
endfunction()

#############################################################################
# Begin a new collection of files to scan with clang. Each list of files
# and directories collected by flk_add_clang_sources will be added to this
# enclosing collection. When the matching end collection is called, a
# build target will be generated to run doxygen on the collected list of
# files and directories to scan.
#
# The argument clang_target is the name of the build target that will build
# the collection. Build target names must be unique across a build system.
function(flk_begin_clang_collection clang_target)
    
    # Save the clang_target name (calls to this function cannot be
    # nested.
    if(DEFINED CLANG_TARGET)
        message("ERROR ************************")
        message("Trying to begin an new clang collection for the target ${clang_target}")
        message("when a collection was already started for the target ${CLANG_TARGET}.")
        message(FATAL_ERROR
            "ERROR: Calls to flk_begin_clang_collection cannot be nested")
    endif()
        
    # Clear the list of all CXX source files from the cache.
    unset(CPP_SOURCE_FILES CACHE)

    # Clear the list of all source files from the cache.
    unset(ALL_SOURCE_FILES CACHE)

    # Save the clang_target in the cache.
    set(CLANG_TARGET ${clang_target}
       CACHE INTERNAL
       "Clang target for a clang collection.")        
        
endfunction()


#############################################################################
# Set up clang to scan all the sources collected by flk_add_clang_sources
# when running the target "make ${clang_target}". Once this target is
# generated, the collected list of sources collected by
# flk_add_clang_sources is cleared so the collection process starts over.
#
function(flk_end_clang_collection)
    flk_generate_clang_configuration(${CLANG_TARGET} "${CPP_SOURCE_FILES}" "${ALL_SOURCE_FILES}")
        
    # Clear the clang_target name from the cache
    unset(CLANG_TARGET CACHE)
endfunction()


function(flk_generate_clang_configuration target_name cppSources allSources)
    add_custom_target(
        ${target_name}-format
        COMMAND ${CLANG_FORMAT}
        -i
        -style=file
        ${allSources}
    )

 # Adding clang-tidy target if executable is found
 # Skipped the:
 # cppcoreguidelines-pro-bounds-array-to-pointer-decay as qDebug triggers this error.
 # google-build-using-namespace as we allow this in our coding standard
 # llvm-header-guard and llvm-include-order conflicts with our coding standard
 # readability-else-after-return as we have already identified places in code this is preferable.
 # readability-simplify-boolean-expr as it doesn't seem to add clarity to the code.
 # modernize-pass-by-value was decided not to run as the difference is subtle and confusing.
 # misc-move-constructor-init for the same reason as modernize-pass-by-value.
 # cppcoreguidelines-pro-type-union-access as we are not using "boost".
     add_custom_target(
         ${target_name}-tidy
         COMMAND ${CLANG_TIDY}
         ${cppSources}
         -checks='-cppcoreguidelines-pro-bounds-array-to-pointer-decay,-google-build-using-namespace,-llvm-header-guard,-llvm-include-order,-readability-else-after-return,-readability-simplify-boolean-expr,-clang-analyzer-alpha*,-cppcoreguidelines-pro-type-vararg,-cppcoreguidelines-pro-type-reinterpret-cast,-google-runtime-int,-modernize-pass-by-value,-misc-move-constructor-init,-cppcoreguidelines-pro-type-union-access'
         -p ${CMAKE_BINARY_DIR}
         -header-filter='src/'
     )
endfunction()
