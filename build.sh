#!/bin/bash
#############################################################################
# Copyright (c) 2017,2018 Fluke Corporation, Inc. All rights reserved.
#
# Repository: https://bitbucket.org/flukept/starsky
# Author    : Santhosh Kumar Selvaraj
# Origin    : ProStar
#

#############################################################################
TEMP_CMAKE_DIR=build/cmake
CMAKE_BUILD_OPTION=" -DPROSTAR_BUILD=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON "
ECLIPSE_BUILD=0
PLATFORM="x86"
OS_BIT_SUPPORT="64_BIT_OS"
COVERAGE="OFF"

#############################################################################
# Help function
function show_help {
    echo "  Use command: bash build.sh <arm|x86> -repo <build|find> [rel|deb] [app|ut_app] [all] [clean] "
    echo "  Usage of command line options:"
    echo "     clean                -   Cleans build directory"
    echo "     -geneclipse          -   Generate necessary files for this project that can be loaded within eclipse"
    echo "                              The output files will be stored within $HOME/dp_eclipse/cmake_<build type>"
    echo "     <arm|x86>            -   Builds for corresponding architecture <arm or x86> "
    echo "     -repo <build|find>   -   Signifies where to get starsky lib tar ball that is required for building prostar application"
    echo "                              local, This option specifies to fetch starsky tar balls from local host system."
    echo "                                     It's an default option and starsky tarballs should be copied inside /usr/share/starsky-libs directory."
    echo "                              remote, This option specifies to fetch starsky tar balls from remote system (currently made it to fetch from jenkins)"
    echo "                              build, This option downloads and builds the libraries independent of Starsky / Jenkins"
    echo "                              find, This option finds dependencies without any downloading / building"
    echo "     <rel|deb>            -   Build type option for compiling prostar application (ex: bash build.sh rel app)."
    echo "     <app|ut_app|all>     -   app, This option specifies to build only communicator application for ProStar."
    echo "                              ut_app, This option specifies to build unit test application for ProStar."
    echo "                              all, This option specifies to build both communicator and unit test application for ProStar."
    echo "                              ddparser, build ddparser application and this option has not been implemented."
    echo "     all                  -   Builds both release and debug mode of both communicator and unit test application for ProStar."
    echo "                              If specified with build type option, it builds both communicator and unit test application for specified build type."
    echo "                              Example:"
    echo "                                 bash build.sh x86 -repo build all        - Build both communicator and unit test application in release & debug mode for x86"
    echo "                                 bash build.sh x86 -repo build rel all    - Build both communicator and unit test application in release mode for x86"
    echo "                                 bash build.sh x86 -repo build deb all    - Build both communicator and unit test application in debug mode for x86"
    echo "                                 bash build.sh arm -repo find all         - Build both communicator and unit test application in release & debug mode for arm"
    echo "                                 bash build.sh arm -repo find rel all     - Build both communicator and unit test application in release mode for arm"
    echo "                                 bash build.sh arm -repo find deb all     - Build both communicator and unit test application in debug mode for arm"
}

#############################################################################
# Build all the binary image including release and debug build
function build_all
{
    CMAKE_BUILD_OPTION+=" -DCMAKE_OS_BIT_SUPPORT=$OS_BIT_SUPPORT "
    CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_PLATFORM=$PLATFORM "
    CMAKE_BUILD_OPTION+=" -DOPTION_ENABLE_SANITIZER=OFF "
    CMAKE_BUILD_OPTION+=" -DAPPLICATION=ON -DUNITTEST=OFF "

    # Triggers release mode build for both application and unit test application
    CMAKE_BLD_OPTION_REL=" $CMAKE_BUILD_OPTION -DCMAKE_BUILD_TYPE=Release -DOPTION_BUILD_COVERAGE=OFF "
    TEMP_CMAKE_DIR_REL="$TEMP_CMAKE_DIR"_"$PLATFORM"_"rel"
    echo " ######################################################################### "
    echo " ####################### BUILDING $PLATFORM $OS_BIT_SUPPORT RELEASE ################## "
    echo " ######################################################################### "
    trigger_build "${TEMP_CMAKE_DIR_REL}" "${CMAKE_BLD_OPTION_REL}"

    # Triggers debug mode build for both application and unit test application
    CMAKE_BLD_OPTION_DEB=" $CMAKE_BUILD_OPTION -DCMAKE_BUILD_TYPE=Debug -DOPTION_BUILD_COVERAGE=$COVERAGE"
    TEMP_CMAKE_DIR_DEB="$TEMP_CMAKE_DIR"_"$PLATFORM"_"deb"
    echo " ######################################################################### "
    echo " ####################### BUILDING $PLATFORM $OS_BIT_SUPPORT DEBUG #################### "
    echo " ######################################################################### "
    trigger_build "${TEMP_CMAKE_DIR_DEB}" "${CMAKE_BLD_OPTION_DEB}"
}

#############################################################################
# Function that triggers prostar aplicaiton compilation
# $1 - Temporarily generated cmake directory
# $2 - Cmake build option for compilation
function trigger_build
{
   
    
    if [ "$ECLIPSE_BUILD" -eq 1 ]; then
        echo " ######################################################################### "
        echo " Generating Makefiles for eclipse Project                                  "
        echo " ######################################################################### "
        # Dump all tempoary cmake and makefile in a seperate directory
        cmake -G"Eclipse CDT4 - Unix Makefiles" -B${1} -H${PWD} ${2} || exit 1       
        echo " ######################################################################### "
        echo " Finished generating Makefiles for eclipse Project                         "
        echo " Follow the below steps to open the project in eclipse                     "
        echo "      1. Open Eclipse IDE using \"eclipse\" command                        "
        echo "      2. Import project by using selecting menu \"File\" -> \"New\" -> \"Makefile Using Existing Project\" "
        echo "      3. Provide Project name and select the base directory of make files "
        echo "      4. You can now compile and debug application in eclipse             "
        echo " ######################################################################### "
    else
        echo " ######################################################################### "
        echo " ########### BUILD OPTIONS ############################################### "
        echo $2
        echo " ######################################################################### "

        # Create a temporary directory for holding temporary cmake files
        mkdir -p ${PWD}/${1}
        # Dump all tempoary cmake and makefile in a seperate directory
        if [ "$PLATFORM" == "arm" ]; then
            bash -c "${BASH_ALIASES[cmake]} -B${PWD}/${1} -H${PWD} ${2}" || exit 1 
        elif [ "$PLATFORM" == "x86" ]; then
            cmake -B${PWD}/${1} -H${PWD} ${2} || exit 1
        else   
            echo "Invalid Platform Type"
        fi

        # Start compilation
        make -C ${PWD}/${1} || exit 1
    fi
}

#############################################################################
# Builds for 32bit ARM platform
function build_arm
{
case $2 in
    "-repo")
        if [ "$3" == "find" ]; then
            CMAKE_BUILD_OPTION+=" -DOPTION_FIND_DEPENDENCIES_ONLY=ON "
        else
            show_arm_help
            exit 1
        fi
        ;;
    *)
        show_arm_help
        exit 1
esac


case $4 in
    "all")
        source environment-setup-cortexa8hf-neon-oe-linux-gnueabi
        if [ "$5" == "-cov" ]; then
            COVERAGE="ON"
        fi

        build_all
        exit 1
        ;;
    *)
esac

case $6 in
    "-cov")
        COVERAGE="ON"
        ;;
    *)
esac


    source environment-setup-cortexa8hf-neon-oe-linux-gnueabi
    echo " ######################################################################### "
    echo " ########### ARM 32 BIT CROSS COMPILATION ################################ "
    echo $CC
    echo " ######################################################################### "

    CMAKE_BUILD_OPTION+=" -DCMAKE_OS_BIT_SUPPORT=32_BIT_OS "
    CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_PLATFORM=$PLATFORM "
    CMAKE_BUILD_OPTION+=" -DOPTION_ENABLE_SANITIZER=OFF "


    # Set build type and build option
    BUILD_TYPE=$4
    BUILD_OPTION=$5

    # Set cmake build type flags
    if [ "$BUILD_TYPE" == "rel" ]; then
        CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_TYPE=Release"
    elif [ "$BUILD_TYPE" == "deb" ]; then
        CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_TYPE=Debug"
        CMAKE_BUILD_OPTION+=" -DOPTION_BUILD_COVERAGE=$COVERAGE "
    else
        show_arm_help
        exit 1
    fi
    
    TEMP_CMAKE_DIR="$TEMP_CMAKE_DIR"_"$PLATFORM"_"$BUILD_TYPE"

    # Set cmake build option flags
    if [ "$BUILD_OPTION" == "app" ]; then
        CMAKE_BUILD_OPTION+=" -DAPPLICATION=ON "
    elif [ "$BUILD_OPTION" == "ut_app" ]; then
        CMAKE_BUILD_OPTION+=" -DUNITTEST=ON "
    elif [ "$BUILD_OPTION" == "all" ]; then
        CMAKE_BUILD_OPTION+=" -DAPPLICATION=ON -DUNITTEST=OFF "
    else
        show_arm_help
        exit 1
    fi

    trigger_build "$TEMP_CMAKE_DIR" "$CMAKE_BUILD_OPTION"
}

#############################################################################
# shows help for building 32 bit arm pltform
function show_arm_help
{
    echo "bash build.sh arm -repo find <rel|deb> <app|ut_app|all>"
    echo "############ OR ############ "
    echo "bash build.sh arm -repo find all"
}

#############################################################################
# Builds for 64bit x86 platform
function build_x86
{
case $2 in
    "-repo")
        if [ "$3" == "build" ]; then
            CMAKE_BUILD_OPTION+=" -DOPTION_DOWNLOAD_AND_BUILD_DEPENDENCIES=ON -DOPTION_FIND_DEPENDENCIES_ONLY=OFF "
        elif [ "$3" == "find" ]; then
            CMAKE_BUILD_OPTION+=" -DOPTION_FIND_DEPENDENCIES_ONLY=ON "
        else
            show_x86_help
            exit 1
        fi
        ;;
    *)
        show_x86_help
        exit 1
esac

case $4 in
    "all")
        if [ "$5" == "-cov" ]; then
            COVERAGE="ON"
        fi

        build_all
        exit 1
        ;;
    *)
esac

case $6 in
    "-cov")
        COVERAGE="ON"
        ;;
    *)
esac


    echo " ######################################################################### "
    echo " ################# X86 64 BIT COMPILATION ################################ "
    echo " ######################################################################### "
    echo $CC

    CMAKE_BUILD_OPTION+=" -DCMAKE_OS_BIT_SUPPORT=64_BIT_OS "
    CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_PLATFORM=$PLATFORM "


    # Set build type and build option
    BUILD_TYPE=$4
    BUILD_OPTION=$5

    # Set cmake build type flags
    if [ "$BUILD_TYPE" == "rel" ]; then
        CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_TYPE=Release"
    elif [ "$BUILD_TYPE" == "deb" ]; then
        CMAKE_BUILD_OPTION+=" -DCMAKE_BUILD_TYPE=Debug"
        CMAKE_BUILD_OPTION+=" -DOPTION_BUILD_COVERAGE=$COVERAGE "
    else
        show_x86_help
        exit 1
    fi
    
    TEMP_CMAKE_DIR="$TEMP_CMAKE_DIR"_"$PLATFORM"_"$BUILD_TYPE"

    # Set cmake build option flags
    if [ "$BUILD_OPTION" == "app" ]; then
        CMAKE_BUILD_OPTION+=" -DAPPLICATION=ON "
    elif [ "$BUILD_OPTION" == "ut_app" ]; then
        CMAKE_BUILD_OPTION+=" -DUNITTEST=ON "
    elif [ "$BUILD_OPTION" == "all" ]; then
        CMAKE_BUILD_OPTION+=" -DAPPLICATION=ON -DUNITTEST=OFF "
    else
        show_x86_help
        exit 1
    fi

    trigger_build "$TEMP_CMAKE_DIR" "$CMAKE_BUILD_OPTION"
}

#############################################################################
# shows help for building 64 bit x86 pltform
function show_x86_help
{
    echo "bash build.sh x86 -repo <build|find> <rel|deb> <app|ut_app|all>"
    echo "############ OR ############ "
    echo "bash build.sh x86 -repo <build|find> all"
}


#############################################################################
# Clean build directory
function build_clean
{
    echo "Cleaning build directory..."
    rm -rf ./build/
    rm -rf ./shared-libs/*.tgz
    rm -rf $HOME/dp_eclipse
    exit 0
}

#############################################################################
# Build type "all" does the compilation of both release and debug images
case $1 in
    "-geneclipse")
        ECLIPSE_BUILD=1
        CMAKE_BUILD_OPTION+=" -DFLK_TARBALL_LOCAL_REPO=ON -DFLK_TARBALL_LOCAL_REPO_PATH=$FLK_TARBALL_LOCAL_REPO_PATH "
        TEMP_CMAKE_DIR=$HOME/dp_eclipse/cmake
        build_all
        ;;
    "all")
        build_all
        ;;
    "clean")
        build_clean
        ;;
	"arm")
        PLATFORM="arm"
        OS_BIT_SUPPORT="32_BIT_OS"
        build_arm $@
        ;;
    "x86")
        PLATFORM="x86"
        OS_BIT_SUPPORT="64_BIT_OS"
        build_x86 $@
        ;;
    *)
        show_help
esac

