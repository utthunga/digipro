/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBusCommandHandler class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "DBusCommandHandler.h"
#include "PMErrorCode.h"
#include "CommonProtocolManager.h"

#include "Initialize.h"
#include "Shutdown.h"
#include "StartScan.h"
#include "StopScan.h"
#include "SetProtocolType.h"
#include "GetBusParameters.h"
#include "SetBusParameters.h"
#include "GetAppSettings.h"
#include "SetAppSettings.h"
#include "Connect.h"
#include "Disconnect.h"
#include "GetItem.h"
#include "SetItem.h"
#include "GetFieldDeviceConfig.h"
#include "SetFieldDeviceConfig.h"
#include "ProcessSubscription.h"
#include "SetLogLevel.h"
#include "SetLangCode.h"
#include "PubSub/PublisherEndpoint.h"
#include "ExecPreEditAction.h"
#include "ExecPostEditAction.h"
#include "flk_log4cplus_defs.h"


/* ========================================================================== */

/* Constants and Macros *******************************************************/



/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs DBusCommandHandler Class Instance
*/
DBusCommandHandler::DBusCommandHandler():
    m_pDbusServer(nullptr)
{
}

/*  ----------------------------------------------------------------------------
    Constructs DBusCommandHandler Class Instance
*/
DBusCommandHandler::~DBusCommandHandler()
{
    if (nullptr != m_pDbusServer)
    {
        delete m_pDbusServer;
        m_pDbusServer = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Initializes DBusCommandHandler 
*/
int DBusCommandHandler::initialise()
{   
    LOGF_TRACE(CPMAPP, "Initialize DBus command handler");

    int initStatus = PM_FAILURE;

    // Connect dbus service for CPM
    if (nullptr == m_pDbusServer)
    {
        m_pDbusServer = new ProtoDBus::Server;

        if (nullptr != m_pDbusServer)
        {   
            auto connectResponse = m_pDbusServer->connect(
                ProtoDBus::Endpoint(DBus::DBUS_CPM_URI, "Cpmcommand")
            );
            if (connectResponse.isError())
            {
                LOGF_FATAL(CPMAPP, "Cannot connect to the dbus service, " <<
                      connectResponse.getErrorMessage());
            }
            else
            {
                initStatus = PM_SUCCESS;
    
                // Command that need to exposed to the outside world
                m_commandTable["Initialize"] = new Initialize();
                m_commandTable["Shutdown"] = new Shutdown();
                m_commandTable["StartScan"] = new StartScan(); 
                m_commandTable["StopScan"] = new StopScan();
                m_commandTable["SetProtocolType"] = new SetProtocolType();
                m_commandTable["GetBusParameters"] = new GetBusParameters();
                m_commandTable["SetBusParameters"] = new SetBusParameters();
                m_commandTable["Connect"] = new Connect();
                m_commandTable["Disconnect"] = new Disconnect();
                m_commandTable["GetItem"] = new GetItem();
                m_commandTable["SetItem"] = new SetItem();
                m_commandTable["GetFieldDeviceConfig"] = new GetFieldDeviceConfig();
                m_commandTable["SetFieldDeviceConfig"] = new SetFieldDeviceConfig();
                m_commandTable["ProcessSubscription"] = new ProcessSubscription();
                m_commandTable["GetBusParameters"] = new GetBusParameters();
                m_commandTable["SetBusParameters"] = new SetBusParameters();
                m_commandTable["GetAppSettings"] = new GetAppSettings();
                m_commandTable["SetAppSettings"] = new SetAppSettings();
                m_commandTable["SetLogLevel"] = new SetLogLevel();
                m_commandTable["SetLangCode"] = new SetLangCode();
                m_commandTable["ExecPreEditAction"] = new ExecPreEditAction();
                m_commandTable["ExecPostEditAction"] = new ExecPostEditAction();
                // Register CPM commands once connect is succeeded
                registerCPMCommands();    
    
                // Register command handler object within CPM
                CommonProtocolManager::getInstance()->registerCommandHandler(this);
            }
        }
    }

    return initStatus;
}

/*  ----------------------------------------------------------------------------
    Register DBus commands
*/
void DBusCommandHandler::registerCPMCommands()
{
    LOGF_TRACE(CPMAPP, "Register CPM Commands");

    if (nullptr != m_pDbusServer)
    {
        /// Register command to the dbus layer
        for (auto & cmndElement : m_commandTable)
        {
             m_pDbusServer->registerCommand(cmndElement.second);
        }
    }
}

/*  ----------------------------------------------------------------------------
    Shutdown DBusCommandHandler
*/
int DBusCommandHandler::shutDown()
{
    // Clears up all the memory allocated from CPM instance
    CommonProtocolManager::getInstance()->clear();

    // Unregister the command handler instance present within CPM instance
    CommonProtocolManager::getInstance()->unregisterCommandHandler();

    // Unregister CPM command    
    unregisterCPMCommands();

    return 0;
}

/*  ----------------------------------------------------------------------------
    Unregister DBus commands
*/
void DBusCommandHandler::unregisterCPMCommands()
{
    LOGF_TRACE(CPMAPP, "Unregister CPM Commands");

    if (nullptr != m_pDbusServer)
    {
        for (auto & cmndElement : m_commandTable)
        {
             m_pDbusServer->unregisterCommand(cmndElement.first);
        }
    }
}


/*  ----------------------------------------------------------------------------
    Add publishers to the publisher table
*/
int DBusCommandHandler::addPublisher(std::string& publisherName)
{
    LOGF_TRACE(CPMAPP, "Add publisher " << publisherName << " to the publisher list");

    int pubStatus = PM_FAILURE;

    // Find any publisher with the same name already exists
    auto publisherTableItr = m_publisherTable.find(publisherName);

    /// Check if the publisher name does not exists within the table
    if (publisherTableItr == m_publisherTable.end())
    {   
        std::shared_ptr<pubsub::Publisher> publisherObj (new pubsub::Publisher);

        if (nullptr != publisherObj)
        {
            // TODO(kraus, salvaraj) a Publisher now takes a type-safe PublisherEndpoint object
            auto pubResponse = publisherObj->tryConnect(
                pubsub::PublisherEndpoint(
                    DBus::DBUS_CPM_URI, publisherName
                )
            );
            if (pubResponse.isError())
            {
                LOGF_FATAL(CPMAPP, "Publisher connect failed:"
                        << pubResponse.getErrorMessage());
            }
            else
            {
                pubStatus = PM_SUCCESS;
                // Insert the new publisher object in the publisher table
                m_publisherTable.insert(std::make_pair(publisherName, publisherObj));
            }
        }
        // TODO(selvaraj) what happens if publisherObj is null?
        // This is a serious runtime error; consider throwing an exception if
        // you are worried about running out of memory. Otherwise, I wouldn't even
        // bother checking for null if you can't do anything about it. It only makes
        // sense to check if 'new' returns null if there is something you can do about it.
    }

    return pubStatus;
}

/*  ----------------------------------------------------------------------------
    Remove publisher from the publisher table
*/
int DBusCommandHandler::removePublisher(std::string& publisherName)
{
    LOGF_TRACE(CPMAPP, "Remove publisher " << publisherName << " from the publisher list");

    int pubStatus = PM_FAILURE;

    // Find the publisher object from the publisher table
    auto publisherTableItr = m_publisherTable.find(publisherName);
 
    if (publisherTableItr != m_publisherTable.end())
    {
        // Erase the element from the list
        m_publisherTable.erase(publisherName);

        pubStatus = PM_SUCCESS;
    }
   
    return pubStatus;
}


/*  ----------------------------------------------------------------------------
    Find the publisher from the publisher table and notifies data to the 
    subscribed client
*/
int DBusCommandHandler::publishData(std::string& publisherName, Json::Value& data)
{
    LOGF_TRACE(CPMAPP, "Publish data " << data << " with the publisher " << publisherName);

    int pubStatus = PM_FAILURE;

    auto publisherTableItr = m_publisherTable.find(publisherName);

    if (publisherTableItr != m_publisherTable.end())
    {
        std::shared_ptr<pubsub::Publisher> publisherObj = publisherTableItr->second;

        if (nullptr != publisherObj)
        {
            pubStatus = PM_SUCCESS;
            publisherObj->update(data);
        }
    }

    return pubStatus;
}
