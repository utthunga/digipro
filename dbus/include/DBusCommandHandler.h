/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    DBus Command Handler class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <map>
#include "DBus/Uri.h"
#include "ProtoDBus/Server.h"
#include "PubSub/Publisher.h"
#include "JsonRpcServerCommand.h"
#include "ICommandHandler.h"

/* ========================================================================== */

/* Macros definition **********************************************************/
// TODO(kraus, santosh selvaraj) Consider using ProtoDBus::Endpoint from tools/dbus-client/include/DBusSubscriber.h
#define CPM_CMD_OBJ_PATH    "/cpmcommand"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of DBusCommandHandler 

    DBusCommandHandler acts as an interface layer between ProStar application 
    and Instrument Controller (provided by Starsky). It exposes the command to 
    the ouside world via DBus and process those command with the help of 
    CommonProtocolManager. This also has the ability to publish the data to the
    subscribed clients.
    
    This inteface will be using Starsky DBus framework for any external 
    communication
*/
class DBusCommandHandler : public ICommandHandler
{
public:

   /* Construction and Destruction ==========================================*/

   /*------------------------------------------------------------------------*/
   /** Constructs DBusCommandHandler Class.
   */
   DBusCommandHandler();

   /*------------------------------------------------------------------------*/
   /** virtual destructor, destroyes DBusCommandHandler class instance.
   */
   virtual ~DBusCommandHandler();


   /* Public Member Functions ===============================================*/

   /*------------------------------------------------------------------------*/
   /** Initializes DBusCommandHandler with the below scope

       1. Start DBus service for the ProStar application with URI as
          DBUS_CPM_URI and object name as "/cpmcommand"
            
       2. Register the required commands that can be accessed from outside
          world

       @return status of initialise function (SUCCESS/FAULRE)
   */
   int initialise();

   /*------------------------------------------------------------------------*/
   /** Shutdown DBusCommandHandler
       
       @return status of a shut down status (SUCCESS/FAILURE)
   */
   int shutDown();

   /*------------------------------------------------------------------------*/
   /**  Add publisher to the puiblisher table

        @param  publisherName,publisher name that needs to be added to the 
                             publisher table maintained by this command
                             handler
        @return status of adding publisher to the table (SUCCESS/FAILURE)
   */
   int addPublisher (std::string& publisherName);

   /*------------------------------------------------------------------------*/
   /**  Remove publisher from the publisher table

        @param publisherName,publisher name that needs to be removed from the 
                             publisher table
        @return status of removing publisher from the table (SUCCESS/FAILURE)
   */
   int removePublisher (std::string& publisherName);

   /*------------------------------------------------------------------------*/
   /**  Publish data to the subscribed clients via DBus channel

        @param  publisherName,publisher name to which the data to be sent
        @param  data,data that need to be published to the subscribers
        @return status of removing publisher from the table (SUCCESS/FAILURE)
   */
   int publishData(std::string& publisherName, Json::Value& data);

private:

    /* Private Data Members ==================================================*/

    /// Map table for publishers
    std::map <std::string, std::shared_ptr<pubsub::Publisher>> m_publisherTable;

    /// Map table for publishers
    std::map <std::string, JsonRpcServerCommand *> m_commandTable;

    /// DBus Server
    ProtoDBus::Server* m_pDbusServer;

    /* Private Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Register commands that can be accessed from outside world
    */    
    void registerCPMCommands();

    /*------------------------------------------------------------------------*/
    /** Register commands that can be accessed from outside world
    */    
    void unregisterCPMCommands();
};
