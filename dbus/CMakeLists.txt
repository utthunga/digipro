#############################################################################
#    Copyright (c) 2017,2018 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    compiler options #######################################################
#############################################################################
add_compile_options(${FLK_COMPILE_OPTIONS})

#############################################################################
#    set CMAKE variable #####################################################
#############################################################################
set(CMAKE_AUTOMOC ON)

set(CMAKE_PREFIX_PATH $ENV{HOME}/Qt/5.8/gcc_64)

find_package(Qt5 5.8.0 REQUIRED COMPONENTS
    Core DBus)


#############################################################################
#    include the source files ###############################################
#############################################################################
set (DBUS_SRC ${CMAKE_CURRENT_SOURCE_DIR}/DBusCommandHandler.cpp)

#############################################################################
#    include the header files ###############################################
#############################################################################
include_directories (${CMAKE_PROSTAR_SRC_DIR}/communicator/include      
                     ${CMAKE_PROSTAR_SRC_DIR}/communicator/commands/include
                     ${CMAKE_PROSTAR_SRC_DIR}/adaptor/include
                     ${CMAKE_PROSTAR_SRC_DIR}/dbus/include
                     ${CMAKE_PROSTAR_SRC_DIR}/commands/include
                     ${CMAKE_PROSTAR_SRC_DIR}/csl/connectionmanager/include
                     ${CMAKE_PROSTAR_SRC_DIR}/cpmutil/include
                     ${CMAKE_PROSTAR_SRC_DIR}/cpmutil/configparser/include
                     ${CMAKE_PROSTAR_SRC_DIR}/csl/parametercache/include
                     ${CMAKE_PROSTAR_SRC_DIR}/clogger
                     ${STARSKY_SHARED_INCLUDE_DIRS})

#############################################################################
#    library definition #####################################################
#############################################################################
add_library (dbusservice STATIC ${DBUS_SRC})

#############################################################################
#    target definitions #####################################################
#############################################################################
target_link_libraries(dbusservice starsky_pubsub_lib_static Qt5::Core Qt5::DBus)

#############################################################################
#    set CLANG and DOXYGEN Variables ########################################
#############################################################################
# Add files to clang and doxygen tools
flk_add_clang_sources (dbusservice "${DBUS_SRC}" "${DBUS_SRC};${DBUS_HDR}")
flk_add_doxygen_sources (dbusservice "${DBUS_SRC};${DBUS_HDR}")
