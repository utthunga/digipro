/*  Filename    : fbapi_sp.c                                                  */
/*                                                                           */
/*  Description : The module provides the serial interface for Hummgingbird  */
/*                                                                           */
/**********************        SVN Section         ***************************/
/*                                                                           */
/*  Fileversion : $Revision:: 11         $                                   */
/*  Checked in  : $Date:: 16.05.01 12:59 $                                   */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES

#ifndef API_LINUX
#   include <windows.h>
#   include <winbase.h>
#   include <string.h>
#   include <stdio.h>
#   include <io.h>
#   include <crtdbg.h>
#   include <tchar.h>
#else
#   include <errno.h>
#   include <stdio.h>
#   include "api_linux.h"
#   include "linux_win32_funcs.h"
#   include "serial_port.h"
    // from  https://stackoverflow.com/questions/12430346/define-snprintf-s-to-snprintf
#   define _snprintf_s(a,b,c,...) snprintf(a,b,__VA_ARGS__)
#endif

#include <wchar.h>

#include "fbapi_if.h"
#include "fbapi_sp.h"
#include "trace.h"

GLOBAL_DEFINES

LOCAL_DEFINES

/* Turn this off, it's no use under LINUX */
#ifdef API_LINUX
#  undef USE_WAIT_FOR_OBJECT
#endif

#define DEBUG_TIMESTAMP
#define INC_SEQ_NR(s)   (s)++; if ((s) >= 0x40) {(s) = 0;}

#define OFFSET_PROTOCOL_ID  4u

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA
extern DWORD          dwEnhancedOptions;
extern T_PORT_IF_DATA port_if_array[MAX_CHANNEL_ID+1];

LOCAL_DATA


/******************************************************************************** */
#ifndef API_LINUX

#ifdef USE_WAIT_FOR_OBJECT

/**
 * The function sends the content of the given transmission buffer by using
 * system function WriteFile. The function waits for completion of the transmission by
 * using GetOverlappedResult(wait=TRUE).
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                 Transmission successful
 *              E_IF_NO_CNTRL_RES    Transmission failed, possible error reasons as debug output                     :
 *
 * @param[in]   hComIF            Interface handle for communication interface
 * - type  :    HANDLE
 * - values:    valid comport handle
 *
 * @param[in]   lpoverWriteSync     Windows Asynchronous Write Events
 * - type  :    LPOVERLAPPED
 * - values:    valid pointer to overlapped structure
 *
 * @param[in]   buffer         pointer to buffer with data to be sent
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[in]   buf_len        length of data to be sent
 * - type  :    USIGN16
 * - values:    -
 *
 */
FUNCTION static INT16 fbapi_sp_send_serial_frame
        (
          IN  HANDLE        hComIF,
          IN  LPOVERLAPPED  lpoverWriteSync,
          IN  USIGN8       *buffer,
          IN  USIGN16       buf_len
         )
{
  LOCAL_VARIABLES
  BOOL              ret;
  DWORD             dummy = 0;
  DWORD             timeout;
  USIGN16           i;
  USIGN32           Offset;
  char              DebugOutBuffer[400];

#ifdef DEBUG_TIMESTAMP
  SYSTEMTIME        systime;
#endif

  FUNCTION_BODY

  /* default timeout 60 ms, check if sufficient for frame length,
     for baud rate = 38.4 kB: Byte Time = 0.208 ms */
  if (buf_len > (6uL * 48uL) - 6uL)
  {
      timeout = ((buf_len + 5uL) / 5uL) + 10uL;
  }
  else
  {
      timeout = 200uL;
  }
  fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

  /* --- send the frame --- */
  Offset = 0uL;

#ifdef DEBUG_TIMESTAMP
  GetLocalTime (&systime);
  Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 14, "%02d:%02d:%02d.%03d: ",
            systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif

  Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 8, "TxData: ");

  if ((dwEnhancedOptions & ENHANCED_OPTION_EXTENDED_DEBUG) == ENHANCED_OPTION_EXTENDED_DEBUG)
  {
    /* additional debug output */
    for (i = 0; i < buf_len; i++)
    {
      Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 3, "%02x ", buffer[i]);
      if (Offset > (sizeof(DebugOutBuffer) - 4u)) { break; }
    }
    DbgPrint("%s\n", DebugOutBuffer);
  }
  ret = WriteFile(hComIF, buffer, (DWORD) buf_len, &dummy, lpoverWriteSync);
#if 1   /* GetOverlappedResult hangs sometimes if device is reset */
  if ((!ret) && (GetLastError() == ERROR_IO_PENDING))
  {
    /* --- wait until sending is finished */
    //DbgPrint ("GetOverlappedResult\n");
#if 1
    ret = WaitForSingleObject(lpoverWriteSync->hEvent, timeout);
    if (ret == WAIT_OBJECT_0)
    {
        ret = GetOverlappedResult(hComIF, lpoverWriteSync, &dummy, FALSE);
    }
    else
    {
      if (ret == WAIT_TIMEOUT)
      {
        DbgPrint("Send Timeout %ld\n", timeout);
      }
      ret = 0;
    }
#else
    ret = GetOverlappedResult(hComIF, lpoverWriteSync, &dummy, TRUE);
#endif
  }
#endif

  fbapi_ReleaseMutex( m_hHAPIMutex );

  DbgPrint ("fbapi_sp_send_serial_frame() - sent=%ld, ret=%d\n", buf_len, ret);

  if (!ret)
  {
    ERR_PRINT ("fbapi_sp_send_serial_frame() - Failed Error=0x%x\n", GetLastError());
    return (E_IF_NO_CNTRL_RES);
  }
  else
  {
    return (E_OK);
  }
}


/******************************************************************************** */

/**
 * Use ReadFile to wait for a header (4 byte), then for the complete frame.
 * The length of the complete frame is derived from the content of the header.
 * If at least one byte is started the timeout value is reduced because the remote
 * station is expected to send a complete frame.
 *
 * The reception via ReadFile is done asynchronous, using overlapped structure and
 * WaitForSingleObject/GetOverlappedResult.
 *
 * @return
 * - type  :    INT16
 * - values:    1:      something was received
 *              0:      timeout occured
 *              -1:     error occurs
 *
 * @param[in]   hComIF            Interface handle for communication interface
 * - type  :    HANDLE
 * - values:    valid comport handle ComIf.hComIF
 *
 * @param[in]   lpoverReadSync     Windows Asynchronous Read Events
 * - type  :    LPOVERLAPPED
 * - values:    valid pointer to overlapped structure
 *
 * @param[in]   buffer         pointer to receive buffer
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[out]  buf_len        length of data to be sent
 * - type  :    USIGN16 *
 * - values:    valid pointer
 *
 * @param[out]  timeout        receive timeout in ms
 * - type  :    DWORD
 * - values:    -
 *
 */
FUNCTION static int fbapi_sp_recv_serial_frame
        (
          IN  HANDLE  hComIF,
          IN  LPOVERLAPPED  lpoverReadSync,
          IN  USIGN8       *buffer,
          IN  USIGN16      *buf_len,
          IN  DWORD         timeout
         )
{
  LOCAL_VARIABLES
  BOOL              ret;
  DWORD             bytesRead = 0uL;
  DWORD             sizeReadBuffer;
  DWORD             expectedSize;
  DWORD             readOffset;
  DWORD             i;
  USIGN8            tmp_buf[512];
  int               ret_val;
  int               pendingOperation=0;
  USIGN32			Offset;
  char              DebugOutBuffer[400];

#ifdef DEBUG_TIMESTAMP
  SYSTEMTIME        systime;
#endif

  FUNCTION_BODY

  fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

  /* --- receive the frame --- */
  sizeReadBuffer = (DWORD) *buf_len;
  readOffset     = 0;
  expectedSize   = 4;
  *buf_len       = 0;

  while (1)
  {
    if (!pendingOperation)
    {
#ifdef DEBUG_TIMESTAMP
      GetLocalTime (&systime);
      DbgPrint ("%02d:%02d:%02d.%03d: ReadFile\n",
         systime.wHour,systime.wMinute,
         systime.wSecond, systime.wMilliseconds);
#endif
      ret = ReadFile(hComIF, tmp_buf, sizeof(tmp_buf), NULL, lpoverReadSync);

      if ((!ret))
      {
      if (GetLastError() == ERROR_IO_PENDING)
      {
          pendingOperation = 1;
      }
      else
      {
          GetLocalTime (&systime);
          DbgPrint ("%02d:%02d:%02d.%03d: Error: %d",
               systime.wHour,systime.wMinute,
               systime.wSecond, systime.wMilliseconds, GetLastError());
          return (-1);
      }
      }
      else
      {
#ifdef DEBUG_TIMESTAMP
          GetLocalTime (&systime);
          DbgPrint ("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute,
               systime.wSecond, systime.wMilliseconds);
#endif
          DbgPrint ("fbapi_sp_recv_serial_frame() - ReadFile=%ld\n", bytesRead);
      }
    }
    else
    {
      ret = FALSE;
    }

    if (pendingOperation)
    {
      //DbgPrint("Pending with timeout %d\n", timeout);
#ifdef DEBUG_TIMESTAMP
      GetLocalTime (&systime);
      DbgPrint ("%02d:%02d:%02d.%03d: WaitForSingleObject\n",
         systime.wHour,systime.wMinute,
         systime.wSecond, systime.wMilliseconds);
#endif
      switch (WaitForSingleObject(lpoverReadSync->hEvent, timeout))
      {
        case WAIT_OBJECT_0:
          // The overlapped operation has completed
          if (GetOverlappedResult(hComIF, lpoverReadSync, &bytesRead, FALSE))
          {
#ifdef DEBUG_TIMESTAMP
            GetLocalTime (&systime);
            DbgPrint ("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute,
               systime.wSecond, systime.wMilliseconds);
#endif
            DbgPrint ("fbapi_sp_recv_serial_frame() - Received=%ld\n", bytesRead);
            pendingOperation=0;
            ret = TRUE;
          }
          else
          {
            DWORD   LastError;

            LastError = GetLastError();
            DbgPrint ("GetOverlappedResult fails %d\n", LastError);
            if ((LastError != ERROR_IO_PENDING) && (LastError != ERROR_IO_INCOMPLETE))
            {
              ret_val = -1;
            }
          }
          break;

        case WAIT_TIMEOUT:
          /* it is necessary to suspend the IO transaction now */
          if (CancelIo(hComIF) == 0)
          {
            DbgPrint ("fbapi_sp_recv_serial_frame() - CancelIO failed with %d\n", GetLastError());
          }

#ifdef DEBUG_TIMESTAMP
          GetLocalTime (&systime);
          DbgPrint ("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute,
               systime.wSecond, systime.wMilliseconds);
#endif
          DbgPrint ("fbapi_sp_recv_serial_frame() - Timeout %ld\n", timeout);
          ret_val = 0;
          break;

        default:
          DbgPrint("Error occured %d\n", GetLastError());
          ret_val = -1;
          break;
      }
    }

    if (ret == TRUE)
    {

      if (bytesRead)
      {
      timeout = 80;
      if (sizeReadBuffer >= readOffset+bytesRead)
      {
          memcpy (buffer+readOffset, tmp_buf, bytesRead);
      }
      else
      {
          DbgPrint("Size %d not sufficient for %d+%d\n", sizeReadBuffer, readOffset, bytesRead);
      }

      if ((dwEnhancedOptions & ENHANCED_OPTION_EXTENDED_DEBUG) == ENHANCED_OPTION_EXTENDED_DEBUG)
      {
          Offset = 0uL;

#ifdef DEBUG_TIMESTAMP
          GetLocalTime (&systime);
          Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 14, "%02d:%02d:%02d.%03d: ",
                        systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif

          Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 8, "RxData: ");

          /* additional debug output */
          for (i = 0; i < bytesRead; i++)
          {
            Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 3, "%02x ", tmp_buf[i]);
            if (Offset > (sizeof(DebugOutBuffer) - 4u)) { break; }
          }
          DbgPrint("%s, readOffset=%ld\n", DebugOutBuffer, readOffset);
      }

      readOffset += bytesRead;
      if (readOffset >= expectedSize)
      {

          if (expectedSize == 4)
          {
            expectedSize = (DWORD) (buffer[2]+0x100u*buffer[3]);
            DbgPrint("header received, expecting %ld now %ld: %x %x %x %x\n",
                    expectedSize, readOffset, buffer[0], buffer[1], buffer[2], buffer[3]);

            if (expectedSize > 512)
            {
              DbgPrint("Size exceeds buffer, data seem illegal\n");
              ret_val = -1;
            }

            if (readOffset >= expectedSize)
            {
                DbgPrint("Complete received, expecting %ld now %ld\n", expectedSize, readOffset);
                *buf_len = (USIGN16) (readOffset);
                //readOffset=0;
                ret_val = 1;
                break;
            }
          }
          else
          {
            DbgPrint("complete received, expecting %ld now %ld\n", expectedSize, readOffset);
            *buf_len   = (USIGN16) (readOffset);
            //readOffset = 0;
            ret_val    =  1;
            break;
          }
      }
      }
    }
    else
    {
      break;
    }
  }
  fbapi_ReleaseMutex ( m_hHAPIMutex );
  return ret_val;
}

#else /*ifdef USE_WAIT_FOR_OBJECT */
/**
 * The function sends the content of the given transmission buffer by using
 * synchronous WriteFile call.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                 Transmission successful
 *              E_IF_NO_CNTRL_RES    Transmission failed, possible error reasons as debug output                     :
 *
 * @param[in]   hComIF            Interface handle for communication interface
 * - type  :    HANDLE
 * - values:    valid comport handle ComIf.hComIF
 *
 * @param[in]   buffer         pointer to buffer with data to be sent
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[in]   buf_len        length of data to be sent
 * - type  :    USIGN16
 * - values:    -
 *
 */
FUNCTION static INT16 fbapi_sp_send_serial_frame
        (
          IN  PBHANDLE  hComIF,
          IN  USIGN8       *buffer,
          IN  USIGN16       buf_len
         )

/*----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

The function sends the content of the given transmission buffer by using
system function WriteFile. The function waits for completion of the transmissionby
using GetOverlappedResult(wait=TRUE).

Possible return values:

- E_OK              Transmission successful
- E_IF_NO_CNTRL_RES    Transmission failed, possible error reasons as debug output
-----------------------------------------------------------------------------*/
{
  LOCAL_VARIABLES
  BOOL              ret;
  DWORD             dummy = 0;
  USIGN16           i;
#ifdef DEBUG_TIMESTAMP
  SYSTEMTIME        systime;
#endif

  FUNCTION_BODY

  /* --- send the frame --- */
#ifdef DEBUG_TIMESTAMP
  GetLocalTime (&systime);
  DbgPrint("%02d:%02d:%02d.%03d: ",
            systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif
  DbgPrint("byte");
  for (i = 0; (i < buf_len) && (i < 8u); i++)
  {
    DbgPrint(" %x", buffer[i]);
  }
  DbgPrint("\n");
  ret = WriteFile(hComIF, buffer, (DWORD) buf_len, &dummy, NULL);
  DbgPrint ("fbapi_sp_send_serial_frame() - sent=%ld, ret=%d\n", buf_len, ret);

  if (!ret)
  {
    ERR_PRINT ("fbapi_sp_send_serial_frame() - Failed Error=0x%x\n", GetLastError());
    return (E_IF_NO_CNTRL_RES);
  }
  else
  {
    return (E_OK);
  }
}

/******************************************************************************** */


/**
 * Use ReadFile to wait for a header (4 byte), then for the complete frame.
 * The length of the complete frame is derived from the content of the header.
 * If at least one byte is started the timeout value is reduced because the remote
 * station is expected to send a complete frame.
 *
 * The reception via ReadFile is done synchronous
 *
 * @return
 * - type  :    INT16
 * - values:    1:      something was received
 *              0:      timeout occured
 *              -1:     error occurs
 *
 * @param[in]   hComIF            Interface handle for communication interface
 * - type  :    HANDLE
 * - values:    valid comport handle ComIf.hComIF
 *
 * @param[in]   buffer         pointer to receive buffer
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[out]  n_byte_to_rcv  number of bytes to be received
 * - type  :    DWORD
 * - values:    1 - 512
 *
 * @param[out]  timeout        receive timeout in ms
 * - type  :    DWORD
 * - values:    -
 *
 */
FUNCTION static DWORD fbapi_sp_recv_next_part
       (
         IN  PBHANDLE        hComIF,
         IN  USIGN8       *buffer,
         IN  DWORD         n_byte_to_rcv,
         IN  DWORD         timeout
         )
{
  USIGN8            tmp_buf[512];
  DWORD             i, bytesRead;
  BOOL              ret;
  DWORD             nBytes = 0;
  char              DebugOutBuffer[400];
  USIGN32           Offset;

  if (n_byte_to_rcv > sizeof(tmp_buf))
  {
      return 0;
  }

  timeout += 50uL;
  do
  {
    ret = ReadFile(hComIF, tmp_buf, n_byte_to_rcv, &bytesRead, NULL);

    if (ret)
    {
        for (i = 0; i < bytesRead; i++)
        {
            DbgPrint ("%02x ", tmp_buf[i]);
            *buffer++ = tmp_buf[i];
            nBytes++;
        }

        if (nBytes == n_byte_to_rcv)
        {
            break;
        }
    }

    Sleep(10);
    timeout -= 50uL;

  } while (timeout > 50uL);

  return (nBytes);
}


/******************************************************************************** */

/**
 * Use ReadFile to wait for a header (4 byte), then for the complete frame.
 * The length of the complete frame is derived from the content of the header.
 * If at least one byte is started the timeout value is reduced because the remote
 * station is expected to send a complete frame.
 *
 * The reception via ReadFile is done synchronous
 *
 * @return
 * - type  :    INT16
 * - values:    1:      something was received
 *              0:      timeout occured
 *              -1:     error occurs
 *
 * @param[in]   hComIF            Interface handle for communication interface
 * - type  :    HANDLE
 * - values:    valid comport handle ComIf.hComIF
 *
 * @param[in]   buffer         pointer to receive buffer
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[out]  buf_len        length of data to be sent
 * - type  :    USIGN16 *
 * - values:    valid pointer
 *
 * @param[out]  timeout        receive timeout in ms
 * - type  :    DWORD
 * - values:    -
 *
 */
FUNCTION static int fbapi_sp_recv_serial_frame
        (
          IN  PBHANDLE  hComIF,
          IN  USIGN8       *buffer,
          IN  USIGN16      *buf_len,
          IN  DWORD         timeout
         )
{
  LOCAL_VARIABLES
  DWORD             i, bytesRead;
  DWORD             sizeReadBuffer;
  DWORD             expectedSize;
  DWORD             readStart;
  DWORD             readOffset;
  int               ret_val;
  USIGN8            dummy[10];
#ifdef DEBUG_TIMESTAMP
  SYSTEMTIME        systime;
#endif

  FUNCTION_BODY

  /* --- receive the frame --- */
  sizeReadBuffer = (DWORD) *buf_len;
  readOffset     = 0;
  expectedSize   = 4;
  *buf_len       = 0;

  readOffset = 0uL;
  for (i = 0; i < expectedSize; i++)
  {
    bytesRead   = fbapi_sp_recv_next_part(hComIF, &buffer[readOffset], 1uL, timeout);
    readOffset += bytesRead;
  }

  DbgPrint ("Header Received=%ld\n", readOffset);
  if (readOffset == expectedSize)
  {
      expectedSize = (DWORD) (buffer[2]+0x100u*buffer[3]);
      if (expectedSize < 4uL)
      {
        return -1;
      }

      ret_val = 1;
      for (i = 0; i < expectedSize - 4uL; i++)
      {
          /* receive 1 byte or wait for timeout */
          if (readOffset < sizeReadBuffer)
          {
              bytesRead = fbapi_sp_recv_next_part(hComIF, &buffer[readOffset], 1uL, timeout);
              readOffset += bytesRead;
          }
          else
          {
              bytesRead = fbapi_sp_recv_next_part(hComIF, dummy, 1uL, timeout);
              ret_val = -1;
          }

          if (bytesRead == 0)
          {
             /* nothing received, stop here */
             DbgPrint ("Timeout\n");
             ret_val = 0;
             break;
          }
      }

#ifdef DEBUG_TIMESTAMP
      GetLocalTime (&systime);
      DbgPrint("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif
      DbgPrint("Total size is %ld\n", readOffset);
      *buf_len = (USIGN16) readOffset;
  }
  else if (bytesRead < expectedSize)
  {
#ifdef DEBUG_TIMESTAMP
      GetLocalTime (&systime);
      DbgPrint("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif
      DbgPrint ("NO HEADER\n");
      ret_val = -1;
  }
  else
  {
     /* too many bytes, should not occur, but does */
#ifdef DEBUG_TIMESTAMP
      GetLocalTime (&systime);
      DbgPrint("%02d:%02d:%02d.%03d: ",
               systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif

      DbgPrint("Received more than header %ld, now getting remaining: ", bytesRead);

      expectedSize = (DWORD) (buffer[2]+0x100u*buffer[3]);

      readOffset = bytesRead;
      readStart  = bytesRead;
      ret_val    = 1;
      for (i = readStart; i < expectedSize - 4uL; i++)
      {
          /* receive 1 byte or wait for timeout */
          if (readOffset < sizeReadBuffer)
          {
              bytesRead = fbapi_sp_recv_next_part(hComIF, &buffer[readOffset], 1uL, timeout);
              readOffset += bytesRead;
          }
          else
          {
              bytesRead = fbapi_sp_recv_next_part(hComIF, dummy, 1uL, timeout);
              ret_val = -1;
          }

          if (bytesRead == 0)
          {
             /* nothing received, stop here */
             DbgPrint ("Timeout\n");
             ret_val = 0;
             break;
          }
      }

      DbgPrint("Now total size %ld\n", readOffset);
      *buf_len = (USIGN16) readOffset;

  }

  return ret_val;
}

#endif /*def USE_WAIT_FOR_OBJECT */

#else /* #ifndef API_LINUX */

/* **************************************************
 * LINUX implementation of
 *    fbapi_sp_send_serial_frame
 *    fbapi_sp_recv_serial_frame
 * ***************************************************/

FUNCTION static INT16 fbapi_sp_send_serial_frame
        (
          IN HANDLE   hComIF,
          IN USIGN8        *buffer,
          IN USIGN16        buf_len
         )

{
LOCAL_VARIABLES

    BOOL        ret;
    USIGN16     i;
    SerialPort *sp;
    int         write_size;

#ifdef DEBUG_TIMESTAMP
    SYSTEMTIME systime;
#endif
FUNCTION_BODY
        /* --- send the frame --- */
#ifdef DEBUG_TIMESTAMP
    GetLocalTime (&systime);
    DbgPrint ("%02d:%02d:%02d.%03d: ",
            systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds);
#endif

    DbgPrint ("byte");

    for (i = 0; (i < buf_len) && (i < 8u); i++)
    {
        DbgPrint (" %x", buffer[i]);
    }

    DbgPrint ("\n");

    sp = (SerialPort *)hComIF;
    write_size = buf_len;

    ret = serialWrite(sp,buffer,&write_size);

    DbgPrint ("fbapi_sp_send_serial_frame() - sent=%ld, ret=%d\n", buf_len, ret);

    if (!ret)
    {
        ERR_PRINT ("fbapi_sp_send_serial_frame() - Failed Error=0x%x\n", errno /*GetLastError()*/);
        return (E_IF_NO_CNTRL_RES);
    }
    else
    {
        return (E_OK);
    }
}



void DebugPrintDate(const char *tag)
{
#ifdef DEBUG_TIMESTAMP
    SYSTEMTIME systime;
    GetLocalTime (&systime);
    DbgPrint ("%02d:%02d:%02d.%03d: %s\n",
        systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds,tag);
#endif
}

/*
 * The <hComIF> points to a SerialPort structure
 */
FUNCTION static int fbapi_sp_recv_serial_frame
        (
          IN HANDLE   hComIF,
          IN USIGN8        *buffer,
          IN USIGN16       *buf_len,
          IN DWORD          timeout
        )

{
LOCAL_VARIABLES

    BOOL        ret;
    DWORD       bytesRead;
    DWORD       sizeReadBuffer;
    DWORD       expectedSize;
    DWORD       readOffset;
    USIGN8      tmp_buf[512];
    int         ret_val;
    int         byte_count;
    int         total_bytes_read = 0;
    SerialPort *sp;
    SerialPort mySp;

FUNCTION_BODY

    /* --- receive the frame --- */
    sizeReadBuffer = (DWORD) * buf_len;
    readOffset = 0;
    expectedSize = 4;
    *buf_len = 0;

    sp = (SerialPort*) hComIF;

    // assert(sp);
    mySp = *sp;  // local debug 
    mySp.ms_timeout = timeout;
    if (mySp.ms_timeout != sp->ms_timeout) {
        DbgPrint("timeout reset from %d to %d\n", sp->ms_timeout, mySp.ms_timeout);
        //DebugPrintDate("timeout reset \n");
    }
#define sp &mySp     //


    while (1)
    {
        DebugPrintDate("ReadFile");
        byte_count = sizeof(tmp_buf);
        memset(tmp_buf,0,sizeof(tmp_buf)); // valgrind is complaining about uninitialised values in debug messages when ony 2 bytes are returned
        ret = serialRead(sp,tmp_buf,&byte_count);
        bytesRead = byte_count;
        total_bytes_read += byte_count;
        //ret = ReadFile (hComIF, tmp_buf, sizeof (tmp_buf), &bytesRead, NULL);

        DebugPrintDate("");
        DbgPrint ("fbapi_sp_recv_serial_frame() - Read=%ld, ret=%d, total_so_far=%d\n", bytesRead, ret, total_bytes_read);
        if (ret == TRUE)
        {
            if (bytesRead)
            {
                if (sizeReadBuffer >= readOffset + bytesRead)
                {
                    //printf("*** fbapi_sp_recv_serial_frame() - Copying %u bytes into buffer at %u\n",bytesRead,readOffset);
                    memcpy (buffer + readOffset, tmp_buf, bytesRead);
                }

                DebugPrintDate("");
                DbgPrint ("byte %x %x %x %x, readOffset %d\n", tmp_buf[0], tmp_buf[1], tmp_buf[2], tmp_buf[3], readOffset);
                readOffset += bytesRead;
                if (readOffset >= expectedSize)
                {
                    if (expectedSize == 4)
                    {
                        expectedSize = (DWORD) (buffer[2] + 0x100u * buffer[3]);
                        DbgPrint ("header received, expecting %ld now %ld: %x %x %x %x\n", expectedSize, readOffset, buffer[0], buffer[1], buffer[2], buffer[3]);
                        if (expectedSize > 512)
                        {
                            DbgPrint ("Size exceeds buffer, data seem illegal\n");
                            ret_val = -1;
                        }
                        if (readOffset >= expectedSize)
                        {
                            DbgPrint ("Complete received, expecting %ld now %ld\n", expectedSize, readOffset);
                            *buf_len = (USIGN16) (readOffset);
                            ret_val = 1;

                            //printf("*** fbapi_sp_recv_serial_frame() - Rx'd %u bytes\n",*buf_len);
                            //hexDump(buffer,readOffset);
                            break;
                        }
                    }

                    else
                    {
                        DbgPrint ("complete received, expecting %ld now %ld\n", expectedSize, readOffset);
                        *buf_len = (USIGN16) (readOffset);
                        ret_val = 1;

                        //printf("*** fbapi_sp_recv_serial_frame() - Rx'd %u bytes\n",*buf_len);
                        //hexDump(buffer,readOffset);
                        break;
                    }
                }
            }

            else
            {
                ret_val = 0;
                break;
            }
        }

        else
        {
            ret_val = -1;
            break;
        }
    }

    return ret_val;
#undef sp 
}

#endif /* #ifndef API_LINUX */


/******************************************************************************** */

/**
 * Set function pointer for send and receiving function according to selected
 * interface type.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                   : send/receive function set according to interface type
 *              E_IF_INVALID_CHANNEL_ID: unknown interface type
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION INT16 fbapi_sp_set_send_recv_fnc
        (
          IN  T_FBAPI_CHANNEL_ID ChannelId
        )
{
  LOCAL_VARIABLES
    INT16    ret_val;

  FUNCTION_BODY

  ASSERT ((ChannelId <= MAX_CHANNEL_ID) && (ChannelId >=0));

  port_if_array[ChannelId].send_frame_fnc = fbapi_sp_send_serial_frame;
  port_if_array[ChannelId].recv_frame_fnc = fbapi_sp_recv_serial_frame;
  ret_val = E_OK;

  return ret_val;
}


/******************************************************************************** */

/**
 * Send the transmission buffer and wait for a response/acknowledge for the given
 * timeout value. If a response/acknowledge is received, check if the CRC is valid.
 * On error i.e. no response or invalid response the transmission is repeated for
 * up to 2-times.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   tx_buffer       data buffer to be transmitted
 * - type  :    USIGN8*
 * - values:    valid pointer
 *
 * @param[in]   tx_buf_len     length of transmission buffer
 * - type  :    USIGN16
 * - values:    -
 *
 * @param[in]   rx_buffer       buffer for data reception
 * - type  :    USIGN8*
 * - values:    valid pointer or NULL
 *
 * @param[in]   rx_buf_len
 * - type  :    USIGN16*
 * - values:    valid pointer
 *
 * @param[in]   timeout     receive timeout in ms
 * - type  :    USIGN32
 * - values:    -
 *
 */
FUNCTION INT16 fbapi_sp_send_frame_and_wait
        (
          IN  T_FBAPI_CHANNEL_ID    ChannelId,
          IN  USIGN8               *tx_buffer,
          IN  USIGN16               tx_buf_len,
          IN  USIGN8               *rx_buffer,
          IN  USIGN16              *rx_buf_len,
          IN  USIGN32               timeout
         )
{
  LOCAL_VARIABLES
    USIGN16 i;
    int     retry = 3;
    int     resend_req;
    USIGN16 loc_rx_buf_len;
    USIGN16 loc_rx_buf_len_storage;
    USIGN8  loc_rx_buffer[20];
    INT16   send_res;
    char    DebugOutBuffer[400];
    USIGN32 Offset;

  FUNCTION_BODY

  if (rx_buffer == NULL)
  {
    rx_buffer      = loc_rx_buffer;
    loc_rx_buf_len = sizeof (loc_rx_buffer);
  }
  else
  {
    loc_rx_buf_len = *rx_buf_len;
  }

  loc_rx_buf_len_storage = loc_rx_buf_len;

  resend_req = 1;

  do {

      if (resend_req)
      {
          /* --- send the frame --- */
		  send_res = port_if_array[ChannelId].send_frame_fnc (port_if_array[ChannelId].hComIF,
#ifdef USE_WAIT_FOR_OBJECT
                                    &port_if_array[ChannelId].overWriteSync,
#endif
                                    tx_buffer, tx_buf_len);
      }
      else
      {
          resend_req = 1;
          send_res   = E_OK;
      }

      if (send_res == E_OK)
      {
          loc_rx_buf_len = loc_rx_buf_len_storage;

		  if (port_if_array[ChannelId].recv_frame_fnc (port_if_array[ChannelId].hComIF,
#ifdef USE_WAIT_FOR_OBJECT
                                    &port_if_array[ChannelId].overReadSync,
#endif
                                    rx_buffer, &loc_rx_buf_len, timeout) > 0)
          {
              USIGN16   crc;
              USIGN16   frame_length;

              if ((dwEnhancedOptions & ENHANCED_OPTION_EXTENDED_DEBUG) == ENHANCED_OPTION_EXTENDED_DEBUG)
              {
                  Offset = 0;
                  Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 3, ">>");
                  for (i = 0; i < loc_rx_buf_len; i++)
                  {
                      Offset += _snprintf_s(&DebugOutBuffer[Offset], sizeof(DebugOutBuffer)-Offset, 3, "%02x ", rx_buffer[i]);
                      if (Offset > (sizeof(DebugOutBuffer) - 20u)) { break; }
                  }
                  DbgPrint("%s\n", DebugOutBuffer);
              }

              /* --- check the received frame, start with CRC --- */
              frame_length = rx_buffer[2] + rx_buffer[3]*0x100u;
              if (frame_length > loc_rx_buf_len)
              {
                  ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait() - Too many byte %d\n", ChannelId, (frame_length - loc_rx_buf_len));
                  frame_length = loc_rx_buf_len;
              }
              else if (frame_length < CSC_SP_FRAME_WRAP_LEN)
              {
                  ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait() - frame too short %d\n", ChannelId, (frame_length - loc_rx_buf_len));
                  frame_length = CSC_SP_FRAME_WRAP_LEN;
              }

              crc = fbapi_sp_calc_crc (rx_buffer, frame_length-CSC_SP_CRC_LEN);
              if ((rx_buffer[frame_length-CSC_SP_CRC_LEN] == (USIGN8) crc) &&
                  (rx_buffer[frame_length-(CSC_SP_CRC_LEN-1)] == (USIGN8) (crc / 0x100u)))
              {
                  /* --- now check the sequence counter --- */
                  /* --- and if the type is okay        --- */
                  if ((rx_buffer[0] & 0x3fu) == (tx_buffer[0] & 0x3fu))
                  {
                      port_if_array[ChannelId].fwMode = rx_buffer[0] & SP_FIRMWARE_MODE;
                      *rx_buf_len = frame_length;
                      return E_OK;
                  }
                  else if (((rx_buffer[0]+1) & 0x3fu) == (tx_buffer[0] & 0x3fu))
                  {
                      ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait() - Old Frame? %x<->%x\n", ChannelId, rx_buffer[0], tx_buffer[0]);
                      resend_req = 0;
                  }
                  else
                  {
                    ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait() - Wrong seq %x<->%x\n", ChannelId, rx_buffer[0], tx_buffer[0]);
                  }
              }
              else
              {
                ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait() - Wrong crc %x\n", ChannelId, crc);
              }
          }
          else
          {
            ERR_PRINT ("CH: 0x%x fbapi_sp_send_frame_and_wait(%d) - nothing received\n", ChannelId, timeout);
          }

          Sleep (100);  
      }
      /* if no valid response is received, sending and receiving is repeated. */

  } while (retry-- > 1);

  return (E_IF_NO_CNTRL_RES);
}


/******************************************************************************** */

/**
 * Build a Sync Frame and send it. Wait for an acknowledge from the device for
 * the default timeout value. Retry if no response.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION INT16 fbapi_sp_sync_device
        (
          IN  T_FBAPI_CHANNEL_ID   ChannelId
         )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[CSC_SP_FRAME_WRAP_LEN];
  USIGN16           crc_16bit;
  USIGN16           total_length;
  USIGN16           rx_length;
  INT16             ret;

  FUNCTION_BODY

  DbgPrint("Sync %d\n", ChannelId);

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = CSC_SP_FRAME_WRAP_LEN;

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_SYNC;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- send the frame --- */
  ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer,
                                 total_length, NULL, &rx_length,
                                 port_if_array[ChannelId].ackTimeout);


  /* set new seq_nr */
  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  return (ret);

}


/******************************************************************************** */

/**
 * Build a set parameter frame and send it.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   param_id        id of parameter to be set
 * - type  :    USIGN16
 * - values:    valid id
 *
 * @param[in]   value           parameter value to be set
 * - type  :    USIGN32
 * - values:    depends on param_id
 *
 */
FUNCTION INT16 fbapi_sp_set_param
        (
          IN  T_FBAPI_CHANNEL_ID    ChannelId,
          IN  USIGN16               param_id,
          IN  USIGN32               value
         )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[CSC_SP_FRAME_WRAP_LEN+6];
  UCHAR             RxBuffer[8];
  USIGN16           crc_16bit;
  USIGN16           total_length;
  USIGN16           rx_length;
  INT16             ret;
  T_PARAM_REQ      *set_param_req;
  USIGN16           Result;

  FUNCTION_BODY

  DbgPrint("set param %d for channel %d\n", param_id, ChannelId);

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = CSC_SP_FRAME_WRAP_LEN + sizeof (T_PARAM_REQ);

  ASSERT (total_length <= sizeof (TxBuffer));

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_SET_PARAM;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);

//TODO: what about byte order and alignment?
  set_param_req = (T_PARAM_REQ *) &TxBuffer[4];
  set_param_req->id    = param_id;
  set_param_req->value = value;

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- send the frame --- */
  rx_length = sizeof (RxBuffer);
  ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer,
                                 total_length, RxBuffer, &rx_length,
                                 port_if_array[ChannelId].ackTimeout);


  /* set new seq_nr */
  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  if (ret == E_OK)
  {
    if (rx_length == CSC_SP_FRAME_WRAP_LEN + sizeof (Result))
    {
      Result = *((USIGN16 *) &RxBuffer[CSC_SP_HEADER_LEN]);
      if (Result != E_OK)
      {
          ERR_PRINT ("CH: 0x%x fbapi_sp_set_param() - failed=%d\n", ChannelId, Result);
          ret = E_IF_INIT_FAILED;
      }
    }
  }

  return (ret);

}

/******************************************************************************** */

/**
 * Send a start command via the serial communication port to the interface board
 * and wait for response from the device.
 * The interface board will react only when in bootblock mode, this should be checked by
 * the caller of this function.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   cmd_id          cmd for protocol stack to be started
 * - type  :    USIGN8
 * - values:    SP_START_PA_FW, SP_START_FF_FW, SP_START_HART_FW
 *
 * @param[in]   pAddError      buffer to return error code from bootloader
 * - type  :    USIGN16*
 * - values:    valid pointer
 *
 */
FUNCTION INT16 fbapi_sp_start_firmware
        (
          IN  T_FBAPI_CHANNEL_ID    ChannelId,
          IN  USIGN8                cmd_id,
          OUT USIGN16              *pAddError
        )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[CSC_SP_FRAME_WRAP_LEN+6];
  UCHAR             RxBuffer[8];
  USIGN16           StartFirmwareResult;
  USIGN16           crc_16bit;
  USIGN16           total_length;
  USIGN16           rx_length;
  INT16             ret;

  FUNCTION_BODY

  DbgPrint("Start Firmware %d\n", ChannelId);

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = CSC_SP_FRAME_WRAP_LEN;

  ASSERT (total_length <= sizeof (TxBuffer));
  ASSERT (pAddError != NULL);
  if ((pAddError == NULL) || (total_length > sizeof (TxBuffer)))
  {
    return E_IF_INVALID_PARAMETER;
  }

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = cmd_id;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- send the frame --- */
  rx_length = sizeof (RxBuffer);
  /* starting the firmware takes almost one second, so that the timeout to a
     higher value than usual */
  ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer,
                                 total_length, RxBuffer, &rx_length,
                                 2000uL);


  /* set new seq_nr */
  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  *pAddError = E_OK;
  if (ret == E_OK)
  {
    if (rx_length == CSC_SP_FRAME_WRAP_LEN + sizeof (StartFirmwareResult))
    {
      StartFirmwareResult = *((USIGN16 *) &RxBuffer[CSC_SP_HEADER_LEN]);
      if (StartFirmwareResult != E_OK)
      {
          ERR_PRINT ("CH: 0x%x fbapi_sp_start_firmware() - failed=%d\n", ChannelId, StartFirmwareResult);
          *pAddError = StartFirmwareResult;
          ret = E_IF_INIT_FAILED;
      }
      else
      {
          /* mobiLink requires additional time for complete initialization, wait for this to avoid
             that the next frame is not responded */
          Sleep(500);
      }
    }
  }

  return (ret);

}


/******************************************************************************** */

/**
 * Send a reset command via the serial communication port to the interface board.
 * Don't wait for a response but do a short sleep in order to wait until reset
 * and startup of bootblock has been done.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION INT16 fbapi_sp_reset_firmware
        (
          IN  T_FBAPI_CHANNEL_ID   ChannelId
        )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[CSC_SP_FRAME_WRAP_LEN];
#if 1
  UCHAR             RxBuffer[8];
  USIGN16           rx_length;
#endif
  USIGN16           crc_16bit;
  USIGN16           total_length;
  INT16             ret;

  FUNCTION_BODY

  DbgPrint("Reset Firmware %d\n", ChannelId);

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = CSC_SP_FRAME_WRAP_LEN;

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_RESET;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- send the frame --- */
#if 1
  rx_length = sizeof (RxBuffer);
  ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer,
                                 total_length, RxBuffer, &rx_length,
                                 port_if_array[ChannelId].ackTimeout);
#else

#endif

  ret = port_if_array[ChannelId].send_frame_fnc (port_if_array[ChannelId].hComIF,
#ifdef USE_WAIT_FOR_OBJECT
                                &port_if_array[ChannelId].overWriteSync,
#endif
                                TxBuffer, total_length);

  /* set new seq_nr */
  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  /* it is okay if there is no reponse, but it should not be invalid */
  if (ret == E_IF_NO_CNTRL_RES)
  {
    ret = E_OK;
  }

  return (ret);

}


/******************************************************************************** */

/**
 * Send a reset command via the serial communication port to the interface board.
 * Don't wait for a response but do a short sleep in order to wait until reset
 * and startup of bootblock has been done.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION INT16 fbapi_sp_poweroff
        (
          IN  T_FBAPI_CHANNEL_ID   ChannelId
        )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[CSC_SP_FRAME_WRAP_LEN];
  UCHAR             RxBuffer[8];
  USIGN16           rx_length;
  USIGN16           crc_16bit;
  USIGN16           total_length;
  INT16             ret;

  FUNCTION_BODY

  DbgPrint("Power off device %d\n", ChannelId);

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = CSC_SP_FRAME_WRAP_LEN;

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_POWEROFF;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- send the frame --- */
  rx_length = sizeof (RxBuffer);
  ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer,
                                 total_length, RxBuffer, &rx_length,
                                 port_if_array[ChannelId].ackTimeout);

  /* set new seq_nr */
  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  /* it is okay if there is no reponse, but it should not be invalid */
  if (ret == E_IF_NO_CNTRL_RES)
  {
    ret = E_OK;
  }

  return (ret);

}


/******************************************************************************** */

/**
 * Send a protocol firmware specific service request and wait for the acknowledge.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pSdb        pointer to protocol specific service descrption block
 * - type  :    VOID*
 * - values:    valid pointer
 *
 * @param[in]   sdb_len        length of the service description block
 * - type  :    USIGN16
 * - values:    depending on used protocol
 *
 * @param[in]   pData       service specific data
 * - type  :    USIGN8*
 * - values:    valid pointer or nuLL
 *
 * @param[in]   data_len      length of service data
 * - type  :    USIGN16
 * - values:    -
 *
 */
FUNCTION INT16 fbapi_sp_send_service
        (
          IN  T_FBAPI_CHANNEL_ID        ChannelId,
          IN  VOID                     *pSdb,
          IN  USIGN16                   sdb_len,
          IN  USIGN8                   *pData,
          IN  USIGN16                   data_len
         )
{
  LOCAL_VARIABLES
  UCHAR             TxBuffer[512];
  UCHAR             RxBuffer[32];
  USIGN16           crc_16bit;
  USIGN16           total_length;
  USIGN16           rx_length;
  INT16             ret;

  FUNCTION_BODY

#if 0
  DbgPrint("Send Request on ch %d service s=%d l=%d t=%d\n",
           ChannelId, pSdb->service, pSdb->layer, pSdb->primitive);
#endif

  /* calculate total size including 4 byte header and 2 byte CRC */
  total_length = data_len + CSC_SP_FRAME_WRAP_LEN + sdb_len;
  if ((total_length >= sizeof (TxBuffer)) || (total_length < data_len))
  {
    return (E_IF_INVALID_PARAMETER);
  }

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_IF_MSG;   /* type identifier */
  TxBuffer[2] = (USIGN8) total_length;
  TxBuffer[3] = (USIGN8) (total_length/0x100u);
  memcpy(&TxBuffer[4],(PUCHAR) pSdb, sdb_len);
  memcpy(&TxBuffer[4+sdb_len],(PUCHAR) pData, data_len);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, total_length-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[total_length-CSC_SP_CRC_LEN]) = crc_16bit;

  rx_length = sizeof (RxBuffer);

  /* --- send the frame --- */
  if ((ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, total_length,
                                 RxBuffer, &rx_length, port_if_array[ChannelId].ackTimeout)) == E_OK)
  {
#if 0
    /* set new seq_nr */
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
    /* no check for request acknowledge */
    return (E_OK);
#else
    int         retry = 3;

    /* send poll request to the FBK Host */
    /* calculate total size including 4 byte header and 2 byte CRC */

    while (retry--)
    {
      INC_SEQ_NR(port_if_array[ChannelId].seqNr);

      /* --- 4 byte header, add it --- */
      TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
      TxBuffer[1] = SP_POLL_IF;   /* type identifier */
      TxBuffer[2] = CSC_SP_FRAME_WRAP_LEN;
      TxBuffer[3] = 0u;

      /* --- calculate and add the CRC --- */
      crc_16bit = fbapi_sp_calc_crc (TxBuffer, CSC_SP_FRAME_WRAP_LEN-CSC_SP_CRC_LEN);
      *((USIGN16 *) &TxBuffer[CSC_SP_FRAME_WRAP_LEN-CSC_SP_CRC_LEN]) = crc_16bit;

      if ((ret == E_OK) && (rx_length == CSC_SP_FRAME_WRAP_LEN+CSC_SP_ACK_LEN))
      {
          break;
      }

      DbgPrint("Poll for request acknowledge\n");

      rx_length = sizeof (RxBuffer);

      /* --- send the frame --- */
      ret = fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, CSC_SP_FRAME_WRAP_LEN,
                                   RxBuffer, &rx_length, port_if_array[ChannelId].readTimeout);
    
      // If the received data does not contain any data, wait for 10ms and retry again  
      // If the received data length is just a frame length, it indicates there is no acknowledgement
      // for the request and henceforth forced to wait for 100ms in such scenarios
      if ((ret == E_OK) && (rx_length == CSC_SP_FRAME_WRAP_LEN))
      {
          DbgPrint ("No data in polling: Wait for 100ms and retry again\n");    
          Sleep (100);
      }  
    }

    if (retry >= 0)
    {
        INT16 ret_code;

        /* 2 byte data contain acknowledge code */
        ret_code = (INT16) (RxBuffer[4] + RxBuffer[5]*0x100);
        return (ret_code);

    }
    else
    {
        return (E_IF_NO_CNTRL_RES);
    }
#endif
  }
  else
  {
    /* set new seq_nr */
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
  }


  return (E_IF_NO_CNTRL_RES);

}


/******************************************************************************** */

/**
 * Send a protocol firmware specific service request and wait for the acknowledge.
 *
 * @return
 * - type  :    INT16
 * - values:    NO_CON_IND_RECEIVED:    nothing received
 *              CON_IND_RECEIVED:       data have been received and copied to given parameter
 *              E_IF_INVALID_PARAMETER: invalid invocation
 *              E_IF_NO_CNTRL_RES:      no response from device
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pSdb        pointer to protocol specific service descrption block
 * - type  :    VOID*
 * - values:    valid pointer
 *
 * @param[in]   sdb_len        length of the service description block
 * - type  :    USIGN16
 * - values:    depending on used protocol
 *
 * @param[in]   pData      buffer to store received service data
 * - type  :    USIGN8*
 * - values:    valid poitner
 *
 * @param[in]   data_len      length of data block buffer
 * - type  :    USIGN16*
 * - values:    -
 *
 */
FUNCTION INT16 fbapi_sp_receive_service
        (
          IN  T_FBAPI_CHANNEL_ID        ChannelId,
          IN  VOID                     *pSdb,
          IN  USIGN16                   sdb_len,
          IN  USIGN8                   *pData,
          IN  USIGN16                  *pDataLen
         )
{
  LOCAL_VARIABLES
  USIGN8    TxBuffer[CSC_SP_FRAME_WRAP_LEN];
  USIGN8    rx_buffer[512];
  USIGN16   crc_16bit;
  USIGN16   rx_length;
  USIGN16   data_len;

  FUNCTION_BODY

  DbgPrint("Poll channel %d\n", ChannelId);

  /* send poll request to the FBK Host */
  /* calculate total size including 4 byte header and 2 byte CRC */
  if (pData == NULL)
  {
    return (E_IF_INVALID_PARAMETER);
  }

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_POLL_IF;   /* type identifier */
  TxBuffer[2] = CSC_SP_FRAME_WRAP_LEN;
  TxBuffer[3] = 0u;

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, CSC_SP_FRAME_WRAP_LEN-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[CSC_SP_FRAME_WRAP_LEN-CSC_SP_CRC_LEN]) = crc_16bit;

  rx_length = sizeof (rx_buffer);

  /* --- send the frame --- */
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, CSC_SP_FRAME_WRAP_LEN,
                                 rx_buffer, &rx_length, port_if_array[ChannelId].readTimeout) == E_OK)
  {
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);

    if (rx_length == CSC_SP_FRAME_WRAP_LEN)
    {
        /* positive response, but no data are contained */
        return (NO_CON_IND_RECEIVED);
    }
    data_len = rx_length - CSC_SP_FRAME_WRAP_LEN - sdb_len;
    if ((data_len > rx_length) || (data_len > *pDataLen)) /* (data_len > rx_length) is constantly false! */
    {
        /* length caused an overflow */
        DbgPrint("Invalid length %d\n", rx_length);
        return (E_IF_INVALID_DATA_SIZE);
    }

    /* now copy the data from buffer to interface structures */
    *pDataLen = data_len;
    memcpy((PUCHAR) pSdb, &rx_buffer[4], sdb_len);
    memcpy((PUCHAR) pData, &rx_buffer[4+sdb_len], data_len);
    DbgPrint("<<< Con/Ind (length=%u) via channel %d\n", data_len, ChannelId);
    return (CON_IND_RECEIVED);
  }
  else
  {
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
    return (E_IF_NO_CNTRL_RES);
  }
}


/******************************************************************************** */

/**
 * Send a reset command via the serial communication port to the interface board.
 * Don't wait for a response but do a short sleep in order to wait until reset
 * and startup of bootblock has been done.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   cmd_id          cmd for protocol stack to be started
 * - type  :    USIGN8
 * - values:    SP_VENDOR_REQ_PA, SP_VENDOR_REQ_FF, SP_VENDOR_REQ_HART
 *
 * @param[in]   info_id         identifies what information shall be read
 * - type  :    USIGN16
 * - values:    depends on protocol firmware
 *
 * @param[in]   pDeviceInfo  buffer to store received information
 * - type  :    T_FBAPI_DEVICE_INFO *
 * - values:    valid pointer
 *
 */
FUNCTION INT16 fbapi_sp_get_vendor_info
        (
          IN    T_FBAPI_CHANNEL_ID  ChannelId,
          IN    USIGN8              cmd_id,
          IN    USIGN16             info_id,
          OUT   T_FBAPI_DEVICE_INFO      *pDeviceInfo
         )
{
  LOCAL_VARIABLES
  USIGN8    TxBuffer[CSC_SP_VENDOR_FRAME_LEN];
  USIGN8    rx_buffer[290];
  USIGN16   crc_16bit;
  USIGN16   rx_length;

  FUNCTION_BODY

  DbgPrint("Read Vendor Info %d\n", ChannelId);

  /* send poll request to the FBK Host */
  /* calculate total size including 4 byte header and 2 byte CRC */
  if (pDeviceInfo == NULL)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_get_vendor_info() - Invalid pointer\n");
    return (E_IF_INVALID_PARAMETER);
  }

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = cmd_id;   /* type identifier */
  TxBuffer[2] = CSC_SP_VENDOR_FRAME_LEN;
  TxBuffer[3] = 0u;

  /* --- set the id that identifies which data should be read --- */
  *((USIGN16 *) &TxBuffer[4]) = info_id;

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, CSC_SP_VENDOR_FRAME_LEN-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[CSC_SP_VENDOR_FRAME_LEN-CSC_SP_CRC_LEN]) = crc_16bit;

  rx_length = sizeof (rx_buffer);

  /* --- send the frame --- */
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, CSC_SP_VENDOR_FRAME_LEN,
                                     rx_buffer, &rx_length, port_if_array[ChannelId].ackTimeout) == E_OK)
  {

    INC_SEQ_NR(port_if_array[ChannelId].seqNr);

    if (rx_length != CSC_SP_FRAME_WRAP_LEN + sizeof (*pDeviceInfo))
    {
        /* positive response, but no data are contained */
        return (E_IF_NO_CNTRL_RES);
    }

    /* now copy the data from buffer to interface structures */
    memcpy((PUCHAR) pDeviceInfo, &rx_buffer[4], sizeof(*pDeviceInfo));
    return (E_OK);
  }
  else
  {
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
    return (E_IF_NO_CNTRL_RES);
  }
}


/**
 * This function is used to send a serial request to retrieve the result for a set parameter command
 * where the result is not available immediately.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                    : version information read successfully
 *              E_IF_INVALID_CHANNEL_ID : invalid channel id
 *              E_IF_API_NOT_INITIALIZED: API not initialized
 *              E_IF_INVALID_PARAMETER  : invalid PIN string
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   parameter_id  Identification of parameter that was set before
 * - type  :    USIGN16
 * - values:    any value
 *
 */
FUNCTION INT16 fbapi_sp_check_parameter_result
         (
          IN    T_FBAPI_CHANNEL_ID  ChannelId,
          IN    USIGN16             parameter_id
         )
{
LOCAL_VARIABLES
  USIGN8    TxBuffer[CSC_SP_SET_PARAM_LEN];
  USIGN8    rx_buffer[CSC_SP_GET_PARAM_LEN];
  USIGN16   crc_16bit;
  USIGN16   tx_length, rx_length;
  T_PARAM_ARRAY_REQ    *set_param_req;

FUNCTION_BODY

  DbgPrint("Check parameter result %d\n", ChannelId);

  /* send poll request to the Interface Board */
  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_SET_PARAM;   /* cype identifier */

  /* set the parameter id */
  set_param_req     = (T_PARAM_ARRAY_REQ *) &TxBuffer[4];
  set_param_req->id = parameter_id;

  /* insert the length */
  tx_length = CSC_SP_HEADER_LEN + CSC_SP_CRC_LEN + sizeof(set_param_req->id);
  TxBuffer[2] = (USIGN8) tx_length;
  TxBuffer[3] = (USIGN8) (tx_length / 0x100u);


  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, tx_length - CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[tx_length-CSC_SP_CRC_LEN]) = crc_16bit;

  rx_length = sizeof (rx_buffer);

  /* --- send the frame --- */
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, tx_length,
                                     rx_buffer, &rx_length, port_if_array[ChannelId].ackTimeout) == E_OK)
  {

    INC_SEQ_NR(port_if_array[ChannelId].seqNr);

    if (rx_length != CSC_SP_FRAME_WRAP_LEN+CSC_SP_ACK_LEN)
    {
        /* positive response, but incorrect length */
        return (E_IF_NO_CNTRL_RES);
    }

    return (E_OK);
  }
  else
  {
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
    return (E_IF_NO_CNTRL_RES);
  }

}

/******************************************************************************** */

/**
 * Send init download command with the header of the download file
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *              E_IF_INVALID_PARAMETER: Invalid request parameter
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   ProtocolId     id of protocol
 * - type  :    USIGN16
 * - values:    PROTOCOL_HART, PROTOCOL_FF, PROTOCOL_PA
 *
 * @param[in]   header          buffer with download file header
 * - type  :    USIGN8 *
 * - values:    valid pointer
 *
 * @param[in]   header_size     size of file header
 * - type  :    USIGN16
 * - values:    -
 *
 */
 INT16 fbapi_sp_init_download
        (
          IN    T_FBAPI_CHANNEL_ID  ChannelId,
          IN    USIGN16             ProtocolId,
          IN    USIGN8             *header,
          IN    USIGN16             header_size
        )
{
  LOCAL_VARIABLES

  USIGN8                TxBuffer[DWNL_HEADER_SIZE+CSC_SP_FRAME_WRAP_LEN+OFFSET_PROTOCOL_ID];
  USIGN8                RxBuffer[CSC_SP_FRAME_WRAP_LEN+2];
  USIGN16               TotalLength;
  USIGN16               RxLength;
  USIGN16               crc_16bit;
  USIGN16               RequestResult;

  FUNCTION_BODY

  if (header_size > DWNL_HEADER_SIZE) { return E_IF_INVALID_PARAMETER; }

  TotalLength = CSC_SP_FRAME_WRAP_LEN + header_size + OFFSET_PROTOCOL_ID;
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_INIT_FW_DWNLD;   /* type identifier */
  TxBuffer[2] = (USIGN8) TotalLength;
  TxBuffer[3] = (USIGN8) (TotalLength/0x100u);
  if (ProtocolId == PROTOCOL_FF)
  {
      TxBuffer[4] = 'F';
      TxBuffer[5] = 'F';
      TxBuffer[6] = 'm';
      TxBuffer[7] = 'b';
  }
  else if (ProtocolId == PROTOCOL_PA)
  {
      TxBuffer[4] = 'P';
      TxBuffer[5] = 'A';
      TxBuffer[6] = 'm';
      TxBuffer[7] = 'b';
  }
  else
  {
      TxBuffer[4] = 'n';
      TxBuffer[5] = 'o';
      TxBuffer[6] = 't';
      TxBuffer[7] = 's';
  }


  memcpy((char*) &TxBuffer[CSC_SP_HEADER_LEN+OFFSET_PROTOCOL_ID], (char*) header, header_size);

  DbgPrint ("CH: 0x%x fbapi_sp_download_firmware() - initiate loading firmware HeaderSize=%d\n",
            ChannelId, header_size);

  crc_16bit = fbapi_sp_calc_crc (TxBuffer, TotalLength-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[TotalLength-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- transfer header and check response from controller --- */
  RxLength      = sizeof (RxBuffer);
  RequestResult = E_IF_NO_CNTRL_RES;
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, TotalLength, RxBuffer, &RxLength,
                                     30*1000uL) == E_OK)
  {
    if (RxLength == CSC_SP_FRAME_WRAP_LEN + sizeof (RequestResult))
    {
      RequestResult = *((USIGN16 *) &RxBuffer[CSC_SP_HEADER_LEN]);
    }
  }

  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  if (RequestResult != E_OK)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_download_firmware() - REQUEST_DOWNLOAD_FW_SEGMENT failed RequestResult=0x%x\n",
               ChannelId, RequestResult);
    return(E_IF_DOWNLOAD_FIRMWARE);
  }

  return E_OK;
}

/**
 * Send download command with the header of the download file
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   data            buffer with download data
 * - type  :    USIGN8 *
 * - values:    valid pointer
 *
 * @param[in]   data_size       size of data
 * - type  :    USIGN16
 * - values:    -
 *
 */
INT16 fbapi_sp_download
        (
          IN    T_FBAPI_CHANNEL_ID  ChannelId,
          IN    USIGN8             *data,
          IN    USIGN16             data_size,
          IN    USIGN32             sequence_counter
        )
{
  LOCAL_VARIABLES

  USIGN8                TxBuffer[MAX_SREC_SIZE+CSC_SP_FRAME_WRAP_LEN+sizeof(sequence_counter)];
  USIGN8                RxBuffer[CSC_SP_FRAME_WRAP_LEN+2];
  USIGN16               TotalLength;
  USIGN16               RxLength;
  USIGN16               crc_16bit;
  USIGN16               RequestResult;

  FUNCTION_BODY

  if (data_size > MAX_SREC_SIZE)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_download() - REQUEST_DOWNLOAD_FW_SEGMENT Wrong data size=0x%x\n", ChannelId, data_size);
    return E_IF_INVALID_PARAMETER;
  }

  TotalLength = CSC_SP_FRAME_WRAP_LEN + data_size + sizeof(sequence_counter);

  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_FW_DWNLD;   /* type identifier */
  TxBuffer[2] = (USIGN8) TotalLength;
  TxBuffer[3] = (USIGN8) (TotalLength/0x100u);
  TxBuffer[4] = (USIGN8) sequence_counter;
  TxBuffer[5] = (USIGN8) (sequence_counter/0x100uL);
  TxBuffer[6] = (USIGN8) (sequence_counter/0x10000uL);
  TxBuffer[7] = (USIGN8) (sequence_counter/0x1000000uL);

  memcpy((char*) &TxBuffer[CSC_SP_HEADER_LEN+sizeof(sequence_counter)],  (char*) data, data_size);

  DbgPrint("CH: 0x%x fbapi_sp_download() - loading firmware\n", ChannelId);

  crc_16bit = fbapi_sp_calc_crc (TxBuffer, TotalLength-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[TotalLength-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- transfer segment and check response from controller --- */
  RxLength      = sizeof (RxBuffer);
  RequestResult = E_IF_NO_CNTRL_RES;
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, TotalLength, RxBuffer, &RxLength,
                                 port_if_array[ChannelId].ackTimeout) == E_OK)
  {
    if (RxLength == CSC_SP_FRAME_WRAP_LEN + sizeof (RequestResult))
    {
      RequestResult = *((USIGN16 *) &RxBuffer[CSC_SP_HEADER_LEN]);
    }
  }

  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

  if (RequestResult != E_OK)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_download_firmware() - DEVICE_REQUEST_DOWNLOAD_FW_SEGMENT failed RequestResult=0x%x\n",
               ChannelId, RequestResult);
    return(E_IF_DOWNLOAD_FIRMWARE);
  }

  return E_OK;

}


/**
 * Send term download command with the header of the download file
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK:               A valid response has been received
 *              E_IF_NO_CNTRL_RES:  No response or invalid response has been received
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
INT16 fbapi_sp_term_download
        (
          IN    T_FBAPI_CHANNEL_ID      ChannelId,
          IN    USIGN32                 sequence_counter
        )
{
  LOCAL_VARIABLES

  USIGN8                TxBuffer[CSC_SP_FRAME_WRAP_LEN+sizeof(sequence_counter)];
  USIGN8                RxBuffer[CSC_SP_FRAME_WRAP_LEN+2];
  USIGN16               TotalLength;
  USIGN16               RxLength;
  USIGN16               crc_16bit;
  USIGN16               RequestResult;

  FUNCTION_BODY

  TotalLength = CSC_SP_FRAME_WRAP_LEN + sizeof (sequence_counter);
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_TERM_FW_DWNLD;   /* type identifier */
  TxBuffer[2] = (USIGN8) TotalLength;
  TxBuffer[3] = (USIGN8) (TotalLength/0x100u);
  TxBuffer[4] = (USIGN8) sequence_counter;
  TxBuffer[5] = (USIGN8) (sequence_counter/0x100uL);
  TxBuffer[6] = (USIGN8) (sequence_counter/0x10000uL);
  TxBuffer[7] = (USIGN8) (sequence_counter/0x1000000uL);

  DbgPrint ("CH: 0x%x fbapi_sp_term_download() - terminate loading firmware\n", ChannelId);

  crc_16bit = fbapi_sp_calc_crc (TxBuffer, TotalLength-CSC_SP_CRC_LEN);
  *((USIGN16 *) &TxBuffer[TotalLength-CSC_SP_CRC_LEN]) = crc_16bit;

  /* --- indicate end of firmware. This will cause flashing of the firmare and check response from controller.
         The time for flashing/erasing is dependent on the size of the firmware but can take up to 7 seconds
         The controller will not respond in this time, because for erasing/flashing controller has to lock IRQs --- */
  RxLength      = sizeof (RxBuffer);
  RequestResult = E_IF_NO_CNTRL_RES;

#ifdef API_LINUX
  /* required time for flashing exceeds standard timeout, increase it */
  serialSetTimeout ((SerialPort *) port_if_array[ChannelId].hComIF, 30000L);
#endif

  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, TotalLength, RxBuffer, &RxLength,
                                     30*1000uL) == E_OK)
  {
    if (RxLength == CSC_SP_FRAME_WRAP_LEN + sizeof (RequestResult))
    {
      RequestResult = *((USIGN16 *) &RxBuffer[CSC_SP_HEADER_LEN]);
    }
  }

  INC_SEQ_NR(port_if_array[ChannelId].seqNr);

#ifdef API_LINUX
  /* set standard timeout again */
  serialSetTimeout ((SerialPort *) port_if_array[ChannelId].hComIF, 0L);
#endif

  if (RequestResult != E_OK)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_term_download() - DEVICE_TERMINATE_DOWNLOAD_FW_SEGMENT failed RequestResult=0x%x\n",
               ChannelId, RequestResult);
    return(E_IF_DOWNLOAD_FIRMWARE);
  }

  return (E_OK);
}

/******************************************************************************** */

/**
 * Read parameter value from bootloader or protocol stack firmware.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                    parameter data have been read
 *              E_IF_INVALID_PARAMETER  invalid invocation
 *              E_IF_NO_CNTRL_RES       no response from device
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   parameter_id    identifier for parameter to be read
 * - type  :    USIGN16
 * - values:
 *
 * @param[in]   value       buffer to store received information
 * - type  :    USIGN32*
 * - values:    valid pointer
 *
 */
FUNCTION INT16 fbapi_sp_read_parameter_info
        (
          IN    T_FBAPI_CHANNEL_ID  ChannelId,
          IN    USIGN16             ParameterID,
          OUT   USIGN32            *pValue
         )
{
  LOCAL_VARIABLES
  USIGN8    TxBuffer[CSC_SP_GET_PARAM_LEN];
  USIGN8    rx_buffer[20];
  USIGN16   crc_16bit;
  USIGN16   rx_length;
  T_GET_PARAM_RSP  *param_rsp;

  FUNCTION_BODY

  DbgPrint("Read parameter %d in channel: %d\n", ParameterID, ChannelId);

  /* send read request */
  /* calculate total size including 4 byte header and 2 byte CRC */
  if (pValue == NULL)
  {
    ERR_PRINT ("CH: 0x%x fbapi_sp_read_parameter_info() - Invalid pointer\n");
    return (E_IF_INVALID_PARAMETER);
  }

  /* --- 4 byte header, add it --- */
  TxBuffer[0] = port_if_array[ChannelId].seqNr + SP_FROM_HOST;
  TxBuffer[1] = SP_GET_PARAM;   /* type identifier */
  TxBuffer[2] = CSC_SP_GET_PARAM_LEN;
  TxBuffer[3] = 0u;


  /* --- set the id that identifies which data should be read --- */
  // this is a T_GET_PARAM_REQ parameter_id
  TxBuffer[4] = _U16_LOW_BYTE(ParameterID);
  TxBuffer[5] = _U16_HIGH_BYTE(ParameterID);

  /* --- calculate and add the CRC --- */
  crc_16bit = fbapi_sp_calc_crc (TxBuffer, CSC_SP_GET_PARAM_LEN-CSC_SP_CRC_LEN);
  TxBuffer[6] = _U16_LOW_BYTE(crc_16bit);
  TxBuffer[7] = _U16_HIGH_BYTE(crc_16bit);

  rx_length = sizeof (rx_buffer);

  /* --- send the frame --- */
  if (fbapi_sp_send_frame_and_wait (ChannelId, TxBuffer, CSC_SP_GET_PARAM_LEN,
                                     rx_buffer, &rx_length, port_if_array[ChannelId].ackTimeout) == E_OK)
  {
#ifdef API_LINUX
    T_GET_PARAM_RSP t_get_param_rsp;
#endif

    INC_SEQ_NR(port_if_array[ChannelId].seqNr);

#ifdef API_LINUX

    /* take care for alignment */
    memcpy(&(t_get_param_rsp.result),&(rx_buffer[4]),sizeof(USIGN16));
    memcpy(&(t_get_param_rsp.value),&(rx_buffer[6]),sizeof(USIGN32));
    param_rsp = &t_get_param_rsp;

#else /* API_LINUX */

    param_rsp = (T_GET_PARAM_RSP*) &rx_buffer[4];

#endif /* API_LINUX */

    if (rx_length == CSC_SP_FRAME_WRAP_LEN + sizeof (*param_rsp))
    {
        /* now copy the data from buffer to interface structures */
        *pValue = param_rsp->value;
        return (E_OK);
    }
    else if (rx_length == CSC_SP_FRAME_WRAP_LEN + sizeof (param_rsp->result))
    {
        return (E_IF_NO_CNTRL_RES);
    }

    return (E_IF_NO_CNTRL_RES);
  }
  else
  {
    INC_SEQ_NR(port_if_array[ChannelId].seqNr);
    return (E_IF_NO_CNTRL_RES);
  }
}

/******************************************************************************** */

/**
 * This function calculates the checksum from an array of bytes adressed by
 * 'binStream' with size 'nrBytes'. Used CRC polynomial is CRC_CCITT (X16+X12+X5+1)
 *
 * @return
 * - type  :    unsigned short
 * - values:    calculated CRC
 *
 * @param[in]   binStream   pointer to array buffer
 * - type  :    unsigned char *
 * - values:
 *
 */
FUNCTION static unsigned short fbapi_sp_calc_crc
(
    USIGN8     *binStream,
    USIGN32     nrBytes
)
{
    USIGN16         cs;
    USIGN16         CRCD;
    USIGN8          CRCIN;
    USIGN16         i;

    CRCD = 0;                           /* Initialize CRC data register     */

    while (nrBytes--)
    {
        CRCIN = *binStream++;           /* Write stream to CRC input reg.   */

        for (i=0; i<8u; i++)             /* use CRC polynom X^16+X^12+X^5+1  */
        {
            if ((CRCIN ^ CRCD) & 1)
            {
                CRCIN >>= 1;
                CRCD  >>= 1;
                CRCD  ^= 0x8408u;
            }
            else
            {
                CRCIN >>= 1;
                CRCD  >>= 1;
            }
        }
    }

    cs = ~CRCD;                         /* Get result from CRC data reg.    */
                                        /* Invert CRC to force != 0         */

    return cs;
} /* FUNCTION fbapi_sp_calc_crccalc_crc */

