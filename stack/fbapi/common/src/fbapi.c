/*  Filename    : hapi.c                                                     */
/*                                                                           */
/*  Description : The module provides the API for the Hummingboard module    */
/*                                                                           */
/*                Four parts can be distinguished:                           */
/*                                                                           */
/*  - Initialization:                                                        */
/*    ---------------                                                        */
/*    - fbapi_init             -> initialize the API                          */
/*    - fbapi_start            -> start the protocol stack                    */
/*                                                                           */
/*  - Service interface:                                                     */
/*    ------------------                                                     */
/*    - fbapi_snd_req_res      -> send service requests and responses         */
/*    - fbapi_rcv_con_ind      -> receive service indications/confirmations   */
/*                                                                           */
/*  - Exit                                                                   */
/*    ----                                                                   */
/*    - fbapi_exit             -> stop and reset the communication            */
/*                                                                           */
/*  - Util                                                                   */
/*    ----                                                                   */
/*    - fbapi_download_firmware -> download firmware                          */
/*    - fbapi_get_device_info   -> get device information                     */
/*                                                                           */
/*  The communication between host and controller is performed using the     */
/*  'common memory'. Softing has specified the data structures and the       */
/*  synchronization mechanism to be used as "Common Memory Interface" (CMI). */
/*  The driver at the host should provide the functions described below in   */
/*  order to offer a standardized procedural interface to the communication. */
/*                                                                           */
/*                                                                           */
/**********************        SVN Section         ***************************/
/*                                                                           */
/*  Fileversion : $Revision:: 11         $                                   */
/*  Checked in  : $Date:: 16.05.01 12:59 $                                   */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES

#ifndef API_LINUX
#   include <windows.h>
#   include <winbase.h>
#   include <crtdbg.h>
#   include <initguid.h>
#   include <stdio.h>
#   include <io.h>
#   include <ws2bth.h>
#   include <tchar.h>
#else
#   include <errno.h>
#   include "api_linux.h"
#   include "linux_win32_funcs.h"
#   include "serial_port.h"
#endif
#include <wchar.h>
#include "fbapi_if.h"
#include "ff_if.h"
#include "pb_if.h"
#include "fbapi_sp.h"
#include "protocol_if.h"
#include "trace.h"
#ifdef API_LINUX
#   undef USE_WAIT_FOR_OBJECT
#endif

GLOBAL_DEFINES

LOCAL_DEFINES
#define REGPATH_PARAMETER_FROM_HLM      "SOFTWARE\\Softing\\mobiLink\\SysLog"

#ifndef API_LINUX
/* {B62C4E8D-62CC-404b-BBBF-BF3E3BBB1374}*/
DEFINE_GUID(g_guidServiceClass, 0xb62c4e8d, 0x62cc, 0x404b, 0xbb, 0xbf, 0xbf, 0x3e, 0x3b, 0xbb, 0x13, 0x74);
#endif

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

static void fbapi_release_channel      (T_FBAPI_CHANNEL_ID ChannelId);
static INT16 fbapi_channel_initialized (T_FBAPI_CHANNEL_ID ChannelId);
static INT16 fbapi_serial_init         (T_FBAPI_CHANNEL_ID ChannelId, PT_FBAPI_INIT pInitParam, T_SERIAL_IF  *pSerialParam);

EXPORT_DATA
DWORD        dwEnhancedOptions;

T_PORT_IF_DATA port_if_array[MAX_CHANNEL_ID + 1];
OS_MUTEX       m_hHAPIMutex = NULL;

IMPORT_DATA

LOCAL_DATA

/* copyright and version ------------------------------------------------ */
static const char copyright[] = "commModule FBAPI (c) Copyright 2018. SOFTING Industrial Automation GmbH. All Rights Reserved.";

/******************************************************************************** */

/**
 * This function releases the resources assigned to the communication channel.
 * These are events, COM port handle.
 *
 * @return
 * - type  :    void
 *
 * @param[in]   ChannelId   Identification for communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION static void fbapi_release_channel (IN T_FBAPI_CHANNEL_ID ChannelId)
{
LOCAL_VARIABLES

FUNCTION_BODY

    if ((ChannelId > MAX_CHANNEL_ID) || (ChannelId < 0)) { return; }

    if (port_if_array[ChannelId].hComIF != INVALID_HANDLE_VALUE)
    {
#ifndef API_LINUX
        CloseHandle(port_if_array[ChannelId].hComIF);
        port_if_array[ChannelId].hComIF = INVALID_HANDLE_VALUE;
#else
        serialClose(port_if_array[ChannelId].hComIF);
#endif
    }

    fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

#ifdef USE_WAIT_FOR_OBJECT
    if (port_if_array[ChannelId].overReadSync.hEvent != NULL)
    {
        CloseHandle(port_if_array[ChannelId].overReadSync.hEvent);
        port_if_array[ChannelId].overReadSync.hEvent = NULL;
    }

    if (port_if_array[ChannelId].overWriteSync.hEvent != NULL)
    {
        CloseHandle(port_if_array[ChannelId].overWriteSync.hEvent);
        port_if_array[ChannelId].overWriteSync.hEvent = NULL;
    }
#endif /* def USE_WAIT_FOR_OBJECT */

    fbapi_ReleaseMutex ( m_hHAPIMutex );

}


/******************************************************************************** */

/**
 * Check if the given channel is initialized.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                    : interface is initialized
 *              E_IF_API_NOT_INITIALIZED: not initialized
 *              E_IF_INVALID_CHANNEL_ID : channel nr out of range
 *
 * @param[in]   ChannelId      identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 */
FUNCTION static INT16 fbapi_channel_initialized
        (
          IN  T_FBAPI_CHANNEL_ID       ChannelId
         )
{
LOCAL_VARIABLES

    INT16 rc = E_OK;

FUNCTION_BODY

    if ((ChannelId > MAX_CHANNEL_ID) || (ChannelId < 0))
    {
        rc = E_IF_INVALID_CHANNEL_ID;
    }
    else
    {
        if (port_if_array[ChannelId].hComIF == INVALID_HANDLE_VALUE)
        {
            rc = E_IF_API_NOT_INITIALIZED;
        }
    }

#ifdef ENABLE_PRINTF
    switch(rc)
    {
        case E_OK: break; //printf("*** fbapi_channel_initialized(%d) - returning E_OK\n",ChannelId); break;
        case E_IF_INVALID_CHANNEL_ID: printf("*** fbapi_channel_initialized(%d) - returning E_IF_INVALID_CHANNEL_ID\n",ChannelId); break;
        case E_IF_API_NOT_INITIALIZED: printf("*** fbapi_channel_initialized(%d) - returning E_IF_API_NOT_INITIALIZED\n",ChannelId); break;
        default: printf("*** fbapi_channel_initialized(%d) - Uh... returning <unknown> code\n",ChannelId); break;
    }
#endif /* ENABLE_PRINTF */

    return rc;
}


/******************************************************************************** */

#ifndef API_LINUX
/**
 * This function is used to open serial communication port and set serial communication
 * parameters
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                   : interface is initialized
 *              E_IF_INVALID_CHANNEL_ID: invalid channel id or in use already
 *              E_IF_NO_DEVICE         : cannot open COM port (see error_code for detail)
 *
 * @param[in]   ChannelId  Identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pInitParam   attributes for commuication interface
 * - type  :    PT_FBAPI_INIT
 * - values:    valid pointer
 *
 * @param[in]   pSerialParam   attributes for commuication interface set internally
 * - type  :    T_SERIAL_IF *
 * - values:    valid pointer
 *
 */
FUNCTION static INT16 fbapi_serial_init
        (
          IN  T_FBAPI_CHANNEL_ID    ChannelId,
          IN  PT_FBAPI_INIT         pInitParam,
          IN  T_SERIAL_IF          *pSerialParam
         )
{
LOCAL_VARIABLES

    int      retry = 2;
    INT16    ret_val;
    DWORD    dwStart;
    DWORD    dwEnd;

FUNCTION_BODY

    do
    {
        fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

        dwStart = GetTickCount();
        port_if_array[ChannelId].hComIF =
#   ifdef USE_WAIT_FOR_OBJECT
            CreateFile(pInitParam->serial.ComportName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
#   else
            CreateFile(pInitParam->serial.ComportName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
#   endif
        dwEnd = GetTickCount();

        fbapi_ReleaseMutex ( m_hHAPIMutex );

        /* if CreateFile takes longer than 5sec. or doesn't return a valid handle
         * close the conection (COM port), wait (D-Link Dongle needs it) and try again
         * 3 times maximum (according to experience from AE -> KR
         */
        if (port_if_array[ChannelId].hComIF == INVALID_HANDLE_VALUE)
        {
            DbgPrint("Invalid handle, retry count %d, time %ld\n", retry, dwEnd - dwStart);
            Sleep(5);
        }
        else
        {
            /* --- success, leave the loop --- */
            DbgPrint("Elapsed time for CreateFile is %ld\n", dwEnd - dwStart);
            break;
        }
    } while (retry--);

    if (port_if_array[ChannelId].hComIF == INVALID_HANDLE_VALUE)
    {
        ret_val = E_IF_NO_DEVICE;

    }
    else
    {
        DCB dcb;

        if (pInitParam->AcknowledgeTimeout < 100u)
        {
            pInitParam->AcknowledgeTimeout = 100u;
        }
        if (pInitParam->ReadTimeout < 100u)
        {
            pInitParam->ReadTimeout = 100u;
        }

        fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

        /* initialize port specific values and store information from application */
        /* timeouts are significantly higher than at controller side, because timing cannot be controlled
         * as exact at as in the embedded system */
        port_if_array[ChannelId].seqNr       = 0;
        port_if_array[ChannelId].ackTimeout  = pInitParam->AcknowledgeTimeout + 400u;  /* unit: ms */
        port_if_array[ChannelId].readTimeout = pInitParam->ReadTimeout + 400u;    /* unit: ms */
        port_if_array[ChannelId].portState   = PORT_STATE_OFFLINE;
        port_if_array[ChannelId].fwMode      = SP_BOOTMODE; /* starts in bootmode */

#ifdef USE_WAIT_FOR_OBJECT
        memset(&port_if_array[ChannelId].overReadSync, 0, sizeof(port_if_array[ChannelId].overReadSync));
        memset(&port_if_array[ChannelId].overWriteSync, 0, sizeof(port_if_array[ChannelId].overWriteSync));
#endif

        fbapi_ReleaseMutex ( m_hHAPIMutex );

        if (GetCommState(port_if_array[ChannelId].hComIF, &dcb))
        {
            dcb.BaudRate    = pSerialParam->baudrate;
            dcb.Parity      = pSerialParam->parity;
            dcb.StopBits    = pSerialParam->stop_bits;
            dcb.ByteSize    = pSerialParam->data_bits;
            //dcb.fRtsControl = RTS_CONTROL_DISABLE;
            dcb.fDtrControl = DTR_CONTROL_DISABLE;
            dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
            //dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;

            if (SetCommState(port_if_array[ChannelId].hComIF, &dcb))
            {
                COMMTIMEOUTS cot;
                if (GetCommTimeouts(port_if_array[ChannelId].hComIF, &cot))
                {
#   ifdef USE_WAIT_FOR_OBJECT
                    cot.ReadIntervalTimeout = 2;
                    cot.ReadTotalTimeoutMultiplier = 0;
                    cot.ReadTotalTimeoutConstant = 0;
                    cot.WriteTotalTimeoutMultiplier = 0;
                    cot.WriteTotalTimeoutConstant = 500;
#   else
                    cot.ReadIntervalTimeout = 2;
                    cot.ReadTotalTimeoutMultiplier = 0;
                    cot.ReadTotalTimeoutConstant = 800;
                    cot.WriteTotalTimeoutMultiplier = 0;
                    cot.WriteTotalTimeoutConstant = 500;
#endif /*def USE_WAIT_FOR_OBJECT*/

                    SetCommTimeouts(port_if_array[ChannelId].hComIF, &cot);

                    /* allocate events for serial comm */
#   ifdef USE_WAIT_FOR_OBJECT

                    fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

                    port_if_array[ChannelId].overReadSync.hEvent  = CreateEvent(NULL, FALSE, FALSE, NULL);
                    port_if_array[ChannelId].overWriteSync.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

                    fbapi_ReleaseMutex ( m_hHAPIMutex );

#   endif

                    ret_val = E_OK;
                }
            }
            else
            {
                fbapi_release_channel(ChannelId);
                ret_val = E_IF_INVALID_PARAMETER;
            }
        }
        else
        {
            fbapi_release_channel(ChannelId);
            ret_val = E_IF_INVALID_PARAMETER;
        }
    }

    return ret_val;
}
#endif // ifndef API_LINUX



/******************************************************************************** */

#ifndef API_LINUX
/**
 * This function is used to initialize the communication interface and tries
 * to communicate with the target board. The communication interface is referred to
 * by pInitParam. Parameter ChannelId is used for identification of the channel for
 * following API function calls using this communication interface.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                   : interface is initialized
 *              E_IF_INVALID_CHANNEL_ID: invalid channel id or in use already
 *              E_IF_INVALID_PROTOCOL  : invalid protocol id
 *              E_IF_NO_DEVICE         : cannot open COM port (see error_code for detail)
 *              E_IF_INVALID_PARAMETER : wrong parameter
 *
 * @param[in]   ChannelId  Identification of communication channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   ProtocolId  Identification of communication protocol
 * - type  :    USIGN16
 * - values:    PROTOCOL_FF, PROTOCOL_PA
 *
 * @param[in]   pInitParam   attributes for commuication interface
 * - type  :    PT_FBAPI_INIT
 * - values:    valid pointer
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_init
        (
          IN  T_FBAPI_CHANNEL_ID  ChannelId,
          IN  USIGN16             ProtocolId,
          IN  PT_FBAPI_INIT       pInitParam
         )
{
LOCAL_VARIABLES

    INT16       ret_val;
    T_SERIAL_IF serial_if_param;

FUNCTION_BODY

    DbgPrint("fbapi_api() - init=%d\n", ChannelId);

    if (fbapi_channel_initialized(ChannelId) != E_IF_API_NOT_INITIALIZED)
    {
        return (E_IF_INVALID_CHANNEL_ID);
    }

    if ((ProtocolId != PROTOCOL_FF) && (ProtocolId != PROTOCOL_PA))
    {
        return (E_IF_INVALID_PROTOCOL);
    }

    serial_if_param.baudrate  = CBR_115200;
    serial_if_param.parity    = NOPARITY;
    serial_if_param.stop_bits = ONESTOPBIT;
    serial_if_param.data_bits = 8;

    /* Create a mutex */
    if ( m_hHAPIMutex == NULL )
    {
        fbapi_CreateMutex(m_hHAPIMutex);
    }

    if ( m_hHAPIMutex == NULL )
    {
        return (E_IF_OS_ERROR);
    }

    if ( !CreateLogFile())
    {
        return (E_IF_OS_ERROR);
    }

    /* open communication channel either COM port or socket */
    DbgPrint("Open serial port %s\n", pInitParam->serial.ComportName);
    ret_val = fbapi_serial_init(ChannelId, pInitParam, &serial_if_param);

    if (ret_val == E_OK)
    {
        /* initialize port specific values and store information from application */
        /* timeouts are significantly higher than at controller side, because timing cannot be controlled
         as exact at as in the embedded system */
        port_if_array[ChannelId].seqNr       = 0;
        port_if_array[ChannelId].ackTimeout  = pInitParam->AcknowledgeTimeout + 400u;   /* unit: ms */
        port_if_array[ChannelId].readTimeout = pInitParam->ReadTimeout + 400u;  /* unit: ms */
        port_if_array[ChannelId].portState   = PORT_STATE_OFFLINE;
        port_if_array[ChannelId].fwMode      = SP_BOOTMODE; /* starts in bootmode */

        /* communication channel is open, set function to be used for sending or
           receiving telegrams via the communication channel */
        fbapi_sp_set_send_recv_fnc(ChannelId);

        /* send a ping to test communication and availability of device */
        ret_val = fbapi_sp_sync_device(ChannelId);

        if (ret_val == E_OK)
        {
            port_if_array[ChannelId].portState = PORT_STATE_IN_SYNC;
        }
    }

    if (ret_val != E_OK)
    {
        /* free the communication resources */
        fbapi_release_channel(ChannelId);
        DbgPrint("fbapi_api() - init failed=%d\n", ret_val);
    }
    else
    {
        /* store initialization parameter */
        port_if_array[ChannelId].pInitParam = *pInitParam;
        port_if_array[ChannelId].ProtocolId = ProtocolId;
        DbgPrint("fbapi_api() - init ok\n");
    }


    return (ret_val);
}
#else

/* ********************************************************
 * LINUX version of the above
 ********************************************************** */

FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_init
        (
          IN  T_FBAPI_CHANNEL_ID ChannelId,
          IN  USIGN16            ProtocolId,
          IN  PT_FBAPI_INIT      pInitParam
         )

{
LOCAL_VARIABLES

    int retry = 2;
    int port_open = 0;
    INT16 ret_val;

FUNCTION_BODY

    DbgPrint("fbapi_api() - init=%d\n", ChannelId);

    if (fbapi_channel_initialized(ChannelId) != E_IF_API_NOT_INITIALIZED)
    {
        ret_val = E_IF_INVALID_CHANNEL_ID;
    }

    if ((ProtocolId != PROTOCOL_FF) && (ProtocolId != PROTOCOL_PA))
    {
        return (E_IF_INVALID_PROTOCOL);
    }

    /* Create a mutex */
    if ( m_hHAPIMutex == NULL )
    {
        fbapi_CreateMutex(m_hHAPIMutex);
        if ( m_hHAPIMutex == NULL )
            return FALSE;
    }

    if ( !CreateLogFile())
    {
        return FALSE;
    }

    DbgPrint("Open serial port %s\n", pInitParam->serial.ComportName);

    do
    {
        DWORD dwStart;
        DWORD dwEnd = 0;

        fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

        dwStart = GetTickCount();

        if (port_if_array[ChannelId].hComIF == NULL)
        {
            port_if_array[ChannelId].hComIF = serialAllocate();
        }
        else
        {
            DbgPrint("SerialPort already on channel %d\n",ChannelId);
        }

        serialInitialise(port_if_array[ChannelId].hComIF, pInitParam->serial.ComportName);
        if ((port_open = serialOpen(port_if_array[ChannelId].hComIF)))
        {
            fbapi_ReleaseMutex( m_hHAPIMutex );

            if (dwEnd == 0)
            {
                dwEnd = GetTickCount(); /* KT OFC 20140214 - used uninitialised */
            }
            DbgPrint("Elapsed time for CreateFile is %ld\n", dwEnd - dwStart);
            break;
        }
        else
        {
            fbapi_ReleaseMutex( m_hHAPIMutex );

            if (dwEnd == 0)
            {
                dwEnd = GetTickCount(); /* KT OFC 20140214 - used uninitialised */
            }
            DbgPrint("Invalid handle, retry count %d, time %ld\n", retry, dwEnd - dwStart);
            Sleep(5);
        }
        dwEnd = GetTickCount();

    } while (retry--);

    if (!port_open)
    {
        ret_val = E_IF_NO_DEVICE;
    }
    else
    {
        if (pInitParam->AcknowledgeTimeout < 100u)
        {
            pInitParam->AcknowledgeTimeout = 100u;
        }
        if (pInitParam->ReadTimeout < 100u)
        {
            pInitParam->ReadTimeout = 100u;
        }

        /* initialize port specific values and store information from application */
        /* timeouts are significantly higher than at controller side, because timing cannot be controlled
         as exact at as in the embedded system */
        port_if_array[ChannelId].seqNr       = 0;
        port_if_array[ChannelId].ackTimeout  = pInitParam->AcknowledgeTimeout + 400u;   /* unit: ms */
        port_if_array[ChannelId].readTimeout = pInitParam->ReadTimeout + 400u;  /* unit: ms */
        port_if_array[ChannelId].portState   = PORT_STATE_OFFLINE;
        port_if_array[ChannelId].fwMode      = SP_BOOTMODE; /* starts in bootmode */

        fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

        /* communication channel is open, set function to be used for sending or
           receiving telegrams via the communication channel */
        fbapi_sp_set_send_recv_fnc(ChannelId);

        /* send a ping to test communication and availability of device */
        ret_val = fbapi_sp_sync_device(ChannelId);

        fbapi_ReleaseMutex( m_hHAPIMutex );

        if (pInitParam->AcknowledgeTimeout != 100u)
        {
//TODO: verify this ACk Timeout
            ret_val = fbapi_sp_set_param(ChannelId, SP_PARAM_ACK_TIMEOUT, pInitParam->AcknowledgeTimeout);
        }

        if (ret_val != E_OK)
        {
            fbapi_release_channel(ChannelId);
            ret_val = E_IF_INVALID_PARAMETER;
            return (ret_val);
        }

        if (pInitParam->ReadTimeout != 100u)
        {
            ret_val = fbapi_sp_set_param(ChannelId, SP_PARAM_READ_TIMEOUT, pInitParam->ReadTimeout);
        }

        if (ret_val != E_OK)
        {
            fbapi_release_channel(ChannelId);
            ret_val = E_IF_INVALID_PARAMETER;
            return (ret_val);
        }

        port_if_array[ChannelId].pInitParam = *pInitParam;

        port_if_array[ChannelId].pInitParam.serial.ComportName = malloc (strlen (pInitParam->serial.ComportName) + 1);
        memset (port_if_array[ChannelId].pInitParam.serial.ComportName, '\0', (strlen (pInitParam->serial.ComportName) + 1));
        (void) strcpy (port_if_array[ChannelId].pInitParam.serial.ComportName, pInitParam->serial.ComportName);

        port_if_array[ChannelId].ProtocolId = ProtocolId;
    }

    return (ret_val);
}
#endif /* #ifndef API_LINUX */




/******************************************************************************** */

/**
 * This function is used to start the stack firmware with the assigned channel
 * number if the commuication board is running in boot mode. The function will wait for
 * an acknowledge from the started firmware.
 * If the firmware has been started already, the function will return immediately with
 * positive result.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                     : stack firmware is started successfully
 *              E_IF_API_NOT_INITIALIZED : channel not initialized
 *              E_IF_INVALID_CHANNEL_ID  : invalid channel id
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pAddError       buffer to return error code from bootloader
 * - type  :    USIGN16*
 * - values:    valid pointer
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_start
        (
          IN  T_FBAPI_CHANNEL_ID ChannelId,
          OUT USIGN16           *pAddError
        )
{
LOCAL_VARIABLES

    INT16    ret_val;
    USIGN8   cmd_id;

FUNCTION_BODY

    DbgPrint("fbapi_api() - start firmware for ch=%d\n", ChannelId);

    if (pAddError != NULL)
    {
        *pAddError = E_OK;
    }

    if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
    {
        return (ret_val);
    }

    switch (port_if_array[ChannelId].ProtocolId)
    {
        case PROTOCOL_FF:
            cmd_id = SP_START_FF_FW;
            break;

        case PROTOCOL_PA:
            cmd_id = SP_START_PA_FW;
            break;

        default:
            ret_val = E_IF_INVALID_PROTOCOL;
            break;

    }

    if (ret_val == E_OK)
    {
        /* check if firmware is running; this is permitted, no error shall be
         * reported but no further action is necessary */
        if (port_if_array[ChannelId].fwMode == SP_BOOTMODE)
        {
            fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );
            ret_val = fbapi_sp_start_firmware(ChannelId, cmd_id, pAddError);
            fbapi_ReleaseMutex ( m_hHAPIMutex );
        }
    }

    return (ret_val);

}


/******************************************************************************** */

/**
 * This function is used to Restart the communication board and clode the
 * communication. Depending on the parameter 'Restart' the bootloader will be
 * executed afterwards or the firmware will be running again.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                     : stack firmware is started successfully
 *              E_IF_API_NOT_INITIALIZED : channel not initialized
 *              E_IF_INVALID_CHANNEL_ID  : invalid channel id
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   Restart
 * - type  :    USIGN16
 * - values:    TRIGGER_HW_RESET, CONTINUE_NO_RESTART
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_exit
         (
           IN  T_FBAPI_CHANNEL_ID  ChannelId,
           IN  USIGN16             Restart
         )
{
LOCAL_VARIABLES

    INT16   ret_val;

FUNCTION_BODY

    DbgPrint("fbapi_api() - exit=%d\n", ChannelId);

    if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
    {
        return (ret_val);
    }

    fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );
    if (Restart == TRIGGER_HW_RESET)
    {
        fbapi_sp_reset_firmware(ChannelId);
    }

#ifndef API_LINUX
    CloseHandle(port_if_array[ChannelId].hComIF);
    port_if_array[ChannelId].hComIF = INVALID_HANDLE_VALUE;
#else
    serialClose(port_if_array[ChannelId].hComIF);
#endif

    if (NULL != port_if_array[ChannelId].pInitParam.serial.ComportName)
    {
        free (port_if_array[ChannelId].pInitParam.serial.ComportName);
        port_if_array[ChannelId].pInitParam.serial.ComportName = NULL;
    }

    fbapi_ReleaseMutex ( m_hHAPIMutex );

    fbapi_CloseMutex ( m_hHAPIMutex );

    CloseLogFile();

    return (E_OK);
}


/******************************************************************************** */

/**
 * This function is used to send a service request or a service response
 * to the protocol stack.
 *
 * @return
 * - type  :    INT16
 * - values:    E_OK                        : no error occured
 *              E_IF_FATAL_ERROR            : unrecoverable error in communication stack
 *              E_IF_INVALID_CHANNEL_ID     : invalid chennel id
 *              E_IF_API_NOT_INITIALIZED    : API not initialized
 *              E_IF_INVALID_LAYER          : invalid layer
 *              E_IF_INVALID_SERVICE        : invalid service identifier
 *              E_IF_INVALID_PRIMITIVE      : invalid service primitive
 *              E_IF_INVALID_COMM_REF       : invalid communication reference
 *              E_IF_RESOURCE_UNAVAILABLE   : no resource available
 *              E_IF_NO_PARALLEL_SERVICES   : no parallel services allowed
 *              E_IF_SERVICE_CONSTR_CONFLICT: service temporarily not executable
 *              E_IF_SERVICE_NOT_SUPPORTED  : service not supported in subset
 *              E_IF_SERVICE_NOT_EXECUTABLE : service not executable
 *              E_IF_NO_CNTRL_RES           : controller does not respond  (CMI_TIMEOUT)
 *              E_IF_INVALID_DATA_SIZE      : not enough cmi data block memory
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pSdb     Pointer to protocol specific service description block
 * - type  :    PVOID
 * - values:    valid pointer
 *
 * @param[in]   pData     Pointer to service specific data block
 * - type  :    PVOID
 * - values:    valid pointer or NULL (if no service data)
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_snd_req_res
         (
          IN T_FBAPI_CHANNEL_ID         ChannelId,
          IN PVOID                      pSdb,
          IN PVOID                      pData
         )
{
LOCAL_VARIABLES

    INT16    ret_val;   /* return value   */
    INT16    data_len;  /* size of datas  */
    USIGN16  sdb_len;   /* protocol specific length of SDB block */

FUNCTION_BODY

    DbgPrint("\n>>>fbapi_api() - Request for ch=%d\n", ChannelId);

    fbapi_WaitForMutex ( m_hHAPIMutex, INFINITE );

    if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
    {
        fbapi_ReleaseMutex ( m_hHAPIMutex );
        return (ret_val);
    }

    if (port_if_array[ChannelId].fwMode == SP_BOOTMODE)
    {
        fbapi_ReleaseMutex ( m_hHAPIMutex );
        return (E_IF_API_NOT_INITIALIZED);
    }

    ASSERT(pSdb);

    ret_val = E_OK;
    data_len = 0;

    switch (port_if_array[ChannelId].ProtocolId)
    {
        case PROTOCOL_FF:
            ret_val = ffapi_check_send_req (pSdb, &sdb_len,
                                            pData, &data_len);
            break;

        case PROTOCOL_PA:
            ret_val = papi_check_send_req (pSdb, &sdb_len,
                                           pData, &data_len);
            break;

        default:
            ret_val = E_IF_FATAL_ERROR;
            break;
    }

    if (ret_val == E_OK)
    {
        /* send the request ---------------------------------------------------- */
        ret_val = fbapi_sp_send_service(ChannelId, pSdb, sdb_len, pData, data_len);

        fbapi_ReleaseMutex ( m_hHAPIMutex );

        DbgPrint("<<<fbapi_api() - Sending returns=%d\n", ret_val);
    }
    else
    {
            fbapi_ReleaseMutex ( m_hHAPIMutex );
    }


    return (ret_val);
}


/******************************************************************************** */

/**
 * This function is used to receive a Service-Indication or a Service-Confirmation
 * from the protocol stack.
 *
 * @return
 * - type  :    INT16
 * - values:    CON_IND_RECEIVED        : a confirmation or indication has been received
 *              NO_CON_IND_RECEIVED     : no confirmation or indication has been received
 *              E_IF_FATAL_ERROR        : unrecoverable error in communication stack
 *              E_IF_INVALID_CHANNEL_ID : invalid channel id
 *              E_IF_API_NOT_INITIALIZED: API not initialized
 *              E_IF_INVALID_DATA_SIZE  : size of data block provided not sufficient
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   pSdb     Pointer to protocol specific service description block
 * - type  :    VOID *
 * - values:    valid pointer
 *
 * @param[in]   pData     Pointer to service specific data block
 * - type  :    VOID *
 * - values:    valid pointer or NULL (if no service data)
 *
 * @param[inout] pDataLen Pointer to maximum/actual data block length
 * - type  :    USIGN16 *
 * - values:    valid pointer or NULL (if no service data)
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_rcv_con_ind
         (
          IN    T_FBAPI_CHANNEL_ID         ChannelId,
          OUT   PVOID                      pSdb,
          OUT   PVOID                      pData,
          INOUT USIGN16                   *pDataLen
         )
{
LOCAL_VARIABLES

    INT16    ret_val = E_OK;    /* return value */
    USIGN16  sdb_len;

FUNCTION_BODY

    ASSERT(pSdb);
    ASSERT(pData);
    ASSERT(pDataLen);

    if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
    {
        return (ret_val);
    }

    if (port_if_array[ChannelId].fwMode == SP_BOOTMODE)
    {
        return (E_IF_API_NOT_INITIALIZED);
    }

    switch (port_if_array[ChannelId].ProtocolId)
    {
        case PROTOCOL_FF:
            sdb_len = ffapi_get_sdb_len ();
            break;

        case PROTOCOL_PA:
            sdb_len = papi_get_sdb_len ();
            break;

        default:
            return (E_IF_FATAL_ERROR);
            break;
    }

    return (fbapi_sp_receive_service(ChannelId, pSdb, sdb_len, pData, pDataLen));
}


/******************************************************************************** */

/**
 * This function is used to read information on version number of the bootloader,
 * the communication board hardware revision and the version of the protocol firmware.
 *
 * @return
 * - type  :    INT16
 *   + #E_OK                    : Version information read successfully
 *   + #E_IF_INVALID_CHANNEL_ID : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED: FBAPI is not initialized for this channel
 *
 * @param[in]   ChannelId  Identification of communication stack (as in fbapi_init)
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0 - MAX_CHANNEL_ID
 *
 * @param[in]   ProtocolId  Identification of communication stack to be loaded
 * - type  :    USIGN16
 * - values:    PROTOCOL_FF, PROTOCOL_PA
 *
 * @param[out]  pDeviceInfo  pointer to buffer to store read version information
 * - type  :    T_FBAPI_DEVICE_INFO
 * - values:    valid pointer
 *
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_get_device_info
         (
          IN    T_FBAPI_CHANNEL_ID   ChannelId,
          IN    USIGN16              ProtocolId,
          OUT   T_FBAPI_DEVICE_INFO *pDeviceInfo
         )
{
LOCAL_VARIABLES

    INT16    ret_val = E_OK;    /* return value */
    USIGN8   cmd_id;

FUNCTION_BODY

    ASSERT(pDeviceInfo);
    if (pDeviceInfo == NULL) { return E_IF_INVALID_PARAMETER; };

    ret_val = fbapi_channel_initialized(ChannelId);

    if (ret_val != E_OK)
    {
        return (ret_val);
    }

    switch (ProtocolId)
    {
        case PROTOCOL_FF:
            cmd_id = SP_VENDOR_REQ_FF;
            break;

        case PROTOCOL_PA:
            cmd_id = SP_VENDOR_REQ_PA;
            break;

        default:
            ret_val = E_IF_INVALID_PROTOCOL;
            break;

    }

    if (ret_val == E_OK)
    {
        ret_val = fbapi_sp_get_vendor_info(ChannelId, cmd_id, SP_VR_DEVICE_INFO, pDeviceInfo);
    }

    return (ret_val);

}




/******************************************************************************** */

/**
 * This function is used to get device parameters specified by ParamterId
 *
 * @return
 * - type  :     INT16
 * - values:     E_OK                    : OK
 *               E_IF_INVALID_CHANNEL_ID : invalid channel id
 *               E_IF_API_NOT_INITIALIZED: API not initialized
 *               E_IF_INVALID_PARAMETER  : no parameter buffer
 *
 * @param[in]    ChannelId
 * - type  :     unsigned int
 * - values:     0..MAX_CHANNEL_ID
 *
 * @param[in]    ParameterId  Identification of communication stack (as used in fbapi_init())
 * - type  :     USIGN16
 * - values:     PARAM_ID_GET_RESET_REASON
 *
 * @param[inout] ParameterLength  length of "Paramters" in bytes
 * - type  :     USIGN16
 * - values:     IN:  length of "parameter buffer" in bytes
 *               OUT: number of parameters read in bytes
 *
 * @param[inout] pParameter    pointer to parameters
 * - type  :     USIGN8*
 * - values:     valid pointer
 */
FUNCTION FBAPI INT16 FBAPI_CALL_CONV fbapi_get_device_parameter
    (
      unsigned int   ChannelId,
      USIGN16        ParameterId,
      USIGN16        ParameterLength,
      USIGN8*        pParameter
    )
{
  LOCAL_VARIABLES

  INT16    ret_val = E_OK;

  FUNCTION_BODY

  if (ChannelId > MAX_CHANNEL_ID)
  {
    return (E_IF_INVALID_CHANNEL_ID);
  }

  if ((pParameter == NULL) || (ParameterLength == 0))
  {
    return E_IF_INVALID_PARAMETER;
  }

  if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
  {
    return (ret_val);
  }

  ret_val = fbapi_sp_read_parameter_info(ChannelId, ParameterId, (USIGN32*)pParameter);

  return (ret_val);
}


/******************************************************************************** */

/**
 * This function is used to perform firmware update
 *
 * @return
 * - type  :    INT16
 * - values:
 * - values:
 *   + #E_OK                    : Version information read successfully
 *   + #E_IF_INVALID_PARAMETER  : No firmware file name given
 *   + #E_IF_SERVICE_CONSTR_CONFLICT: Service can be executed only while bootloader is running
 *   + #E_IF_DOWNLOAD_FIRMWARE  : Error during download of file
 *   + #E_IF_INVALID_CHANNEL_ID : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED: FBAPI is not initialized for this channel
 *
 * @param[in]   ChannelId  Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   ProtocolId  Identification of communication stack
 * - type  :    USIGN16
 * - values:    PROTOCOL_FF, PROTOCOL_PA
 *
 * @param[out]  pDownloadFirmware       name of firmware file
 * - type  :    T_DOWNLOAD_FIRMWARE*
 * - values:    valid pointer
 */
 FBAPI INT16 FBAPI_CALL_CONV fbapi_download_firmware
    (
        T_FBAPI_CHANNEL_ID          ChannelId,
        USIGN16                     ProtocolId,
        T_FBAPI_DOWNLOAD_FIRMWARE  *pDownloadFirmware
    )
{
LOCAL_VARIABLES
    INT16       ret_val = E_OK;    /* return value */
    USIGN8      buffer[DWNL_HEADER_SIZE];
    USIGN32     download_sequence;
    USIGN16     size;
    FILE       *pFirmwareFile;
    long        FirmwareFileLength;
    size_t      read_length;
#ifdef API_LINUX
    struct stat FwFileStat;
#else
    errno_t     OpenError;
#endif

FUNCTION_BODY

    ASSERT(pDownloadFirmware);
    if (pDownloadFirmware == NULL) { return E_IF_INVALID_PARAMETER; };

    if ((ret_val = fbapi_channel_initialized(ChannelId)) != E_OK)
    {
        return (ret_val);
    }

    if ((ProtocolId != PROTOCOL_FF) && (ProtocolId != PROTOCOL_PA))
    {
        return (E_IF_INVALID_PROTOCOL);
    }

    if (port_if_array[ChannelId].fwMode != SP_BOOTMODE)
    {
        return (E_IF_SERVICE_CONSTR_CONFLICT);
    }

    /* --- open the file with given filename and determine length of file --- */
#ifdef API_LINUX
#if 0
    pFirmwareFile = _wfopen (pDownloadFirmware->FileName, L"rb");
    //pFirmwareFile = fopen(download_firmware_name, "rb");
#else
//TODO:  resolve the long file name open vs. _wfopen
    pFirmwareFile = fopen((const char *)pDownloadFirmware->FileName, "rb");
#endif
#else
    OpenError = _wfopen_s(&pFirmwareFile, pDownloadFirmware->FileName, L"rb");
    if (OpenError != 0)
    {
        ERR_PRINT("CH: 0x%x fbapi_sp_download_firmware() - leave - can not open firmware file Status=0x%x\n", ChannelId, OpenError);
        return(E_IF_DOWNLOAD_FIRMWARE);
    }
#endif /* API_LINUX */

#ifdef API_LINUX
    if (fstat (fileno (pFirmwareFile), &FwFileStat) == 0)
    {
        FirmwareFileLength = (long) FwFileStat.st_size;
    }
    else
    {
        FirmwareFileLength = 0uL;
    }
#else /* API_LINUX */
    FirmwareFileLength = _filelength(_fileno(pFirmwareFile));
#endif /* API_LINUX */

    if (FirmwareFileLength <= DWNL_HEADER_SIZE)
    {
        ERR_PRINT("CH: 0x%x fbapi_sp_download_firmware() - leave - empty firmware file", ChannelId);
        fclose(pFirmwareFile);
        return(E_IF_DOWNLOAD_FIRMWARE);
    }

    if (ret_val == E_OK)
    {
        read_length = fread (buffer, sizeof (unsigned char), DWNL_HEADER_SIZE, pFirmwareFile);
        if (read_length == DWNL_HEADER_SIZE)
        {
            ret_val = fbapi_sp_init_download(ChannelId, ProtocolId, buffer, DWNL_HEADER_SIZE);
        }
        else
        {
            ERR_PRINT("CH: 0x%x fbapi_sp_download_firmware() - leave - failed to read header, len=%d", ChannelId, read_length);
            fclose(pFirmwareFile);
            return(E_IF_DOWNLOAD_FIRMWARE);
        }
    }

    download_sequence = 0;
    while (ret_val == E_OK)
    {
        read_length = fread (buffer, sizeof (unsigned char), MAX_SREC_SIZE, pFirmwareFile);
        if (read_length == MAX_SREC_SIZE)
        {
            DBG_PRINT(("CH: 0x%x Downloading data size: %ld %ld\n", ChannelId, read_length, download_sequence));
            ret_val = fbapi_sp_download(ChannelId, buffer, MAX_SREC_SIZE, download_sequence);
        }
        else if (read_length > 0u)
        {
            /* remaining firmware data, terminate after downloading these data */
            size = (USIGN16) read_length;
            DBG_PRINT(("CH: 0x%x Downloading incomplete (last) segment, data size: %ld %ld\n", ChannelId, read_length, download_sequence));
            ret_val = fbapi_sp_download(ChannelId, buffer, size, download_sequence);
            break;
        }
        else
        {
            if (feof(pFirmwareFile) != 0)
            {
                /* no remaining bytes in the file, terminate now */
                DBG_PRINT(("CH: 0x%x No more data for download\n", ChannelId));
                download_sequence--;
                ret_val = E_OK;
            }
            else
            {
                ERR_PRINT("CH: 0x%x fbapi_sp_download_firmware() - leave - failed to firmware, len=%d", ChannelId, read_length);
                ret_val = E_IF_DOWNLOAD_FIRMWARE;
            }
            break;
        }
        download_sequence++;
    }

    if (ret_val == E_OK)
    {
        fclose(pFirmwareFile);
        DBG_PRINT(("CH: 0x%x Terminate download last seq = %ld\n", ChannelId, download_sequence));
        ret_val = fbapi_sp_term_download(ChannelId, download_sequence);
    }


    return (ret_val);

}



/******************************************************************************** */


#ifndef API_LINUX                /* not needed for unix */

/******************************************************************************** */

/**
 * Function to read registry key content and return it.
 *
 * @return
 * - type  :    ULONG
 * - values:    registry key value or 0 if key or value not existing
 *
 */
FUNCTION static DWORD fbapi_read_registry (VOID)
{
  LOCAL_VARIABLES

  LONG   RetVal;
  HKEY   keyParameters = NULL;
  DWORD  keySize;
  DWORD  keyType;
  DWORD  keyEnhancedOperation;

  FUNCTION_BODY

  keyEnhancedOperation = 0uL;

  if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE,REGPATH_PARAMETER_FROM_HLM,0,KEY_READ,&keyParameters))
  {
    keySize = sizeof(keyEnhancedOperation);

    RetVal = RegQueryValueEx(keyParameters,"EnhancedOperation",NULL,&keyType,(LPBYTE) &keyEnhancedOperation,&keySize);
    if (RetVal == ERROR_SUCCESS)
    {
      if (keyType != REG_DWORD)
      {
        keyEnhancedOperation = 0uL;
      }
    }
  }

  return keyEnhancedOperation;

}

/**
 * Windows DLL Main function. The function is invoked by Windows while loading the
 * DLL.
 *
 * @return
 * - type  :    BOOL
 * - values:    TRUE
 *
 * @param[in]   hInstDLL   A handle to the DLL module. The value is the base address
 *                         of the DLL. The HINSTANCE of a DLL is the same as the HMODULE
 *                         of the DLL, so hinstDLL can be used in calls to functions
 *                         that require a module handle.
 * - type  :    HINSTANCE
 * - values:    valid handled
 *
 * @param[in]   Reason
 * - type  :    ULONG
 * - values:    DLL_PROCESS_ATTACH, DLL_PROCESS_DETACH,
 *              DLL_THREAD_ATTACH, DLL_THREAD_DETACH
 *
 * @param[in]   pReserved
 * - type  :    LPVOID
 * - values:
 *
 */
FUNCTION BOOL __stdcall DllMain
         (
            IN HINSTANCE  hInstDLL,
            IN ULONG      Reason,
            IN LPVOID     pReserved
          )
{
LOCAL_VARIABLES

    T_FBAPI_CHANNEL_ID ChannelId;

FUNCTION_BODY

    switch (Reason)
    {
        case DLL_PROCESS_ATTACH:
            for(ChannelId = 0; ChannelId <= MAX_CHANNEL_ID; ChannelId++)
            {
                port_if_array[ChannelId].hComIF = INVALID_HANDLE_VALUE;
                m_hHAPIMutex                     = NULL;

#   ifdef USE_WAIT_FOR_OBJECT
                port_if_array[ChannelId].overReadSync.hEvent  = NULL;
                port_if_array[ChannelId].overWriteSync.hEvent = NULL;
#   endif
            }

            dwEnhancedOptions = fbapi_read_registry();

            break;

        case DLL_THREAD_ATTACH:
            break;

        case DLL_THREAD_DETACH:
            break;

        case DLL_PROCESS_DETACH:
            for(ChannelId = 0; ChannelId < MAX_CHANNEL_ID; ChannelId++)
            {
                if (fbapi_channel_initialized(ChannelId) == E_OK)
                {
                    (VOID) fbapi_exit (ChannelId, TRIGGER_HW_RESET);
                }
            }
            break;

        default:
            break;
    }

    return TRUE;
}

#endif //#ifndef API_LINUX
