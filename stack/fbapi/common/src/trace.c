// TRACE.C
//
// Trace utilities.
//
// $Revision: 12 $
// $Date: 5/09/02 5:24p $
//
// Copyright © 2008, Softing AG.
// All rights reserved.

#ifdef API_LINUX
#    include "api_linux.h"
#    include "linux_win32_funcs.h"
#    include <stdlib.h>
#    include <string.h>
#    include <stdarg.h>
#else
#    include <windows.h>
#endif

#include <stdio.h>

#include "trace.h"

#ifdef API_LINUX
static FILE  *h_TraceFile = NULL;

#else
static PBHANDLE h_TraceFile = INVALID_HANDLE_VALUE;
#endif


/*===========================================================================*/
BOOL CreateLogFile(VOID)
{
#ifdef API_LINUX

    h_TraceFile = fopen(API_TRACE_FILE, "w");
    if (h_TraceFile == NULL)
    {
        return FALSE;
    }

#else /* API_LINUX */

    if (h_TraceFile == INVALID_HANDLE_VALUE)
    {
        h_TraceFile = CreateFile(API_TRACE_FILE , GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (h_TraceFile == INVALID_HANDLE_VALUE)
        {
            return FALSE;
        }
    }

#endif /* #ifdef API_LINUX */
    return TRUE;
}

/*===========================================================================*/
void CloseLogFile(VOID)
{
#ifdef API_LINUX
    if (h_TraceFile != NULL)
    {
        fclose(h_TraceFile);
        h_TraceFile = NULL;
    }

#else /* API_LINUX */
    if (h_TraceFile != INVALID_HANDLE_VALUE)
    {
        CloseHandle(h_TraceFile);
        h_TraceFile = INVALID_HANDLE_VALUE;
    }
#endif /* ifdef API_LINUX */
}

void DbgPrint (const char* pString, ...)
{
  char      Buffer [512];
  va_list   Arguments;
#ifndef API_LINUX
  DWORD     bytes;
#endif

  va_start (Arguments, pString);

#ifdef API_LINUX
  // vsnprintf (Buffer, sizeof(Buffer), pString, Arguments);

  if (h_TraceFile != NULL)
  {
     vfprintf (h_TraceFile, pString, Arguments);
     fflush(h_TraceFile);
  }

#else /* API_LINUX */
  vsprintf_s (Buffer, sizeof(Buffer), pString, Arguments);
  OutputDebugString (Buffer);

  if (h_TraceFile != INVALID_HANDLE_VALUE)
  {
     DWORD bufferSize = strlen(Buffer);
     BOOL result = WriteFile ( h_TraceFile, (LPCVOID)Buffer, bufferSize, &bytes, NULL);
     if (result == FALSE)
     {
         OutputDebugString("Error writing in log file.\n");
     }
  }
#endif /* ifdef API_LINUX */

  va_end (Arguments);

  return;
}

#ifdef SOFTING_TRACE

/*===========================================================================*/

void DbgPrint (const char* pString, ...)
{
  char      Buffer [512];
  va_list   Arguments;
#ifndef API_LINUX
  DWORD     bytes;
#endif

  va_start (Arguments, pString);

#ifdef API_LINUX
  vsnprintf (Buffer, sizeof(Buffer), pString, Arguments);

  if (h_TraceFile != NULL)
  {
     vfprintf (h_TraceFile, pString, Arguments);
     fflush(h_TraceFile);
  }
#else /* API_LINUX */
  vsprintf_s (Buffer, sizeof(Buffer), pString, Arguments);
  OutputDebugString (Buffer);

  if (h_TraceFile != INVALID_HANDLE_VALUE)
  {
     DWORD bufferSize = strlen(Buffer);
     BOOL result = WriteFile ( h_TraceFile, (LPCVOID)Buffer, bufferSize, &bytes, NULL);
     if (result == FALSE)
     {
         OutputDebugString("Error writing in log file.\n");
     }
  }
#endif /* ifdef API_LINUX */

  va_end (Arguments);

  return;
}

#endif // SOFTING_TRACE


