#include "circular_buffer.h"


#undef CircularByteBuffer_DEBUG_PRINT


/* 
 * This should never be called by the end-user
 */
extern bool cbbGrowBuffer(CircularByteBuffer *cbb,unsigned int by_at_least,bool exactly_to);



/*
 * Some handy utility functions
 *
 */
inline unsigned int cbbGetContentSize(CircularByteBuffer *cbb)
{
    unsigned int rc;

    if (cbb->head <= cbb->tail)
    {
        rc = cbb->tail - cbb->head;
    }
    else
    {
        /// the tail is before the head
        rc = cbb->tail + (cbb->max_size - cbb->head);
    }

    return rc;
}

inline unsigned int cbbGetMaxSize(CircularByteBuffer *cbb) { return cbb->max_size; }
inline unsigned int cbbGetRemainingCapacity(CircularByteBuffer *cbb) { return cbb->max_size - cbbGetContentSize(cbb); }
inline bool cbbIsEmpty(CircularByteBuffer *cbb) { return (cbbGetContentSize(cbb) == 0);        }
inline bool cbbIsFull(CircularByteBuffer *cbb)  { return (cbbGetContentSize(cbb) == cbb->max_size); }




/*
 * Allocate a new Circular Byte Buffer
 */
CircularByteBuffer *cbbInitialise(unsigned int start_size)
{
    CircularByteBuffer *cbb = (CircularByteBuffer*)malloc(sizeof(CircularByteBuffer));
    if (cbb)
    {
        cbb->buffer       = (unsigned char *)malloc(start_size);
        //memset(cbb->buffer,'.',start_size);
        cbb->max_size     = start_size;
        cbb->head         = 0;
        cbb->tail         = 0;
    }

    return cbb;
}


/*
 * Deallocate a Circular Byte Buffer
 */
void cbbFree(CircularByteBuffer *cbb)
{
    if (cbb)
    {
        if (cbb->buffer)
            free(cbb->buffer);
        free(cbb);
    }
}    




/*
 * Nuke it from orbit
 */
void cbbPurge(CircularByteBuffer *cbb)
{
    cbb->head = 0;
    cbb->tail = 0;
    //memset(cbb->buffer,'_',cbb->max_size);
}



/*
 * Copy up to <size> bytes from our internal buffer into <to_buffer>
 * If there is less data than <size> only this amount is copied
 *
 * Returns the actual amount copied
 */
unsigned int cbbGetDataFromBuffer(CircularByteBuffer *cbb,unsigned int size, unsigned char *to_buffer)
{
    unsigned int bytes_copied = 0;

#ifdef CircularByteBuffer_DEBUG_PRINT    
    printf("cbbGetDataFromBuffer(size=%u)\n",size);
    
    printf("BEGIN-cbbGetDataFromBuffer(%p) ... \n",cbb);
    printf("    max_size %u\n",getMaxSize(cbb)); 
    printf("    head     %u\n",cbb->head);
    printf("    tail     %u\n",cbb->tail);
    printf("    content  %u bytes\n",cbbGetContentSize(cbb));
#endif


    if (size > 0 && cbbGetContentSize(cbb) > 0)
    {
        /// Copy as many bytes as requested, or as many as we have
        bytes_copied = size;
        if (bytes_copied > cbbGetContentSize(cbb))
            bytes_copied = cbbGetContentSize(cbb);

        /// Is the data we need wholly contailed Head-to-tail or Head-to-End-of-Buffer
        if (cbb->head < cbb->tail || bytes_copied <= (cbb->max_size - cbb->head))
        {
            /// Simple copy  ->  no need to adjust tail
            unsigned char *from_ptr = cbb->buffer + cbb->head;
            memcpy(to_buffer,from_ptr,bytes_copied);
            //memset(from_ptr,'.',bytes_copied); // Debug clear
            cbb->head += bytes_copied;
        }
        else
        {
            /// Oooh, nasty!   Tail is before head, and we need to copy from both sections
            unsigned char *from_ptr = cbb->buffer + cbb->head;

            /// Copy from head -> end-of-buffer
            unsigned int end_copy_size = cbb->max_size - cbb->head;
            memcpy(to_buffer, from_ptr, end_copy_size);
            //memset(from_ptr,'.',end_copy_size); // Debug clear

            /// Copy from start-of-buffer 
            memcpy(to_buffer + end_copy_size, cbb->buffer, bytes_copied - end_copy_size);
            //memset(buffer,'.',bytes_copied - end_copy_size); // Debug clear

            /// And make a new head
            cbb->head = bytes_copied - end_copy_size;
        }
    }
#ifdef CircularByteBuffer_DEBUG_PRINT    
    printf("END-cbbGetDataFromBuffer(%p) ... \n",cbb);
    printf("    max_size %u\n",getMaxSize(cbb)); 
    printf("    head     %u\n",cbb->head);
    printf("    tail     %u\n",cbb->tail);
#endif

    if (cbb->head == cbb->tail)
        cbb->head=cbb->tail=0;   /// Optimise buffer position when we become empty

    return bytes_copied;
}





/*
 * Store as much of the data from <from_buffer> as we can.
 *
 * Returns true if all <size> data was stored, if false is returned
 * something bad has happened, because we grow to store everything.
 */
bool cbbAddDataToBuffer(CircularByteBuffer *cbb,unsigned int size,const unsigned char *from_buffer)
{
    bool rc = true;

    unsigned int current_size = cbbGetContentSize(cbb);
    unsigned int copy_size = size;

    /// If we can't store this much data, increrase the buffer size, 
    /// or only store what we can
    if (size >= cbbGetRemainingCapacity(cbb))
    {
        if (!cbbGrowBuffer(cbb, size - cbbGetRemainingCapacity(cbb),false))
            size = cbb->max_size - current_size;  // the grow failed, only copy what we can
    }

    /// Add the data at buffer[tail], growing from there, but must watch out for the end-of-buffer
    //printf("IS %u - %u (%u) >= %u?\n",cbb->max_size,cbb->tail,cbb->max_size - cbb->tail,copy_size); 
    if ((cbb->head <= cbb->tail && cbb->max_size - cbb->tail >= copy_size) || (cbb->head > cbb->tail && cbb->head - cbb->tail >= copy_size))
    {
        /// Easy! Blat it in
        unsigned char *copy_ptr = cbb->buffer + cbb->tail;
        memcpy(copy_ptr,from_buffer,copy_size);
        cbb->tail += copy_size;
    }
    else
    {
        /// The tail will grow past the end-of-buffer into the start-of-buffer (but before <head>)
        unsigned int part_copy_size = cbb->max_size - cbb->tail;
        unsigned char *copy_ptr = cbb->buffer + cbb->tail;
        memcpy(copy_ptr,from_buffer,part_copy_size); /// now buffer is filled from middle to end
        copy_ptr = cbb->buffer;
        memcpy(copy_ptr,from_buffer + part_copy_size,copy_size - part_copy_size);  /// filled from start-of-buffer to somewhere in the middle (but before <head>)
        cbb->tail = copy_size - part_copy_size;
    }
    
    return rc;
}







/*
 * Make the buffer bigger by around <by_at_least> bytes.  
 * But if <exactly_to> is set, the size is set to match <by_at_least>
 * instead of just growing by this much.
 *
 * This should never be called by the end-user
 *
 * Returns false if you're out of memory
 */
bool cbbGrowBuffer(CircularByteBuffer *cbb,unsigned int by_at_least,bool exactly_to)
{
    bool rc = true;
    unsigned int extra = 19;  // prime

    /// We're adding this extra bit here, because if you add exactly enough, 
    /// you end up exactly full, but with head == tail, which also indicates emptiness
    unsigned int new_size = extra + by_at_least + cbb->max_size;

    if (exactly_to)
        new_size = extra + by_at_least;

    if (cbb->head <= cbb->tail)
    {
        /// Block is in a simple head-tail-ordering, so we can just grow it
        unsigned char *bigger_block = (unsigned char *)realloc(cbb->buffer,new_size);
        if (bigger_block)
        {
#ifdef CircularByteBuffer_DEBUG_PRINT            
            printf("RESIZED BLOCK TO %u bytes\n",new_size); 
#endif            
            //memset(cbb->buffer+cbb->tail,'.',new_size - cbb->tail);
            cbb->buffer = bigger_block;
            cbb->max_size = new_size;
        }
        else
        {
            rc = false;
        }
    }
    else
    {
        /// Block is in round-the-horn mode (tail-head-ordering), can't just increase size
        /// as it would make a hole in the contiguous data 
        unsigned char *bigger_block = (unsigned char *)malloc(new_size);
        if (bigger_block)
        {
            //memset(bigger_block,'.',new_size);
            memcpy(bigger_block,cbb->buffer+cbb->head,cbb->max_size - cbb->head);
            memcpy(bigger_block+(cbb->max_size - cbb->head),cbb->buffer,cbb->tail);
            free(cbb->buffer);
            cbb->buffer = bigger_block;
            cbb->max_size = new_size;
            cbb->tail = (cbb->max_size - cbb->head) + cbb->tail;
            cbb->head = 0;
        }
        else
        {
            rc = false;
        }
        
    }

    if (!rc)
    {
        // realloc() failed ... OOPS!
#ifdef CircularByteBuffer_DEBUG_PRINT        
        printf("REALLOC FAILED\n"); 
#endif        
    }

    return rc;
}


bool cbbSizeTo(CircularByteBuffer *cbb,unsigned int new_max_size)
{
    bool rc = false;

    if (new_max_size > cbb->max_size)
    {
        rc = cbbGrowBuffer(cbb,new_max_size,true);
    }
    else
    {
        unsigned int extra = 7;
        unsigned char *smaller_buffer = (unsigned char *)malloc(extra + new_max_size);
        if (smaller_buffer)
        {
            /// Copy out as much of the data as we can, then replace the old buffer
            unsigned int copy_size = cbbGetContentSize(cbb);
            if (copy_size > new_max_size)
                copy_size = new_max_size;
            cbbGetDataFromBuffer(cbb,copy_size,smaller_buffer);
            cbb->head = 0;
            cbb->tail = copy_size;
            cbb->max_size = extra + new_max_size;
            free(cbb->buffer);
            cbb->buffer = smaller_buffer;
            rc = true;
        }
    }
    return rc;
}




