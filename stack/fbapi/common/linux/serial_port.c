#include "serial_port.h"
#include "api_linux.h"
#include "linux_win32_funcs.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

// #define DEBUG_STRINGS

#define SERIAL_BAUD_RATE 115200
/* 8N1 */
#define SERIAL_DATA_BITS CS8
#define SERIAL_PARITY    0
#define SERIAL_STOP_BITS CSTOPB

#define SERIAL_DEFAULT_TIMEOUT 1000L /* milliseconds */
#define MIN_SERIAL_TIMEOUT     100L  /* ms */

SerialPort *serialAllocate()
{
    SerialPort *sp = (SerialPort *) malloc(sizeof(SerialPort));
    if (sp)
    {
        sp->handle    = -1;
        sp->connected =  0;
        sp->device_name[0] = '\0';
        sp->ms_timeout = SERIAL_DEFAULT_TIMEOUT; // milliseconds
    }
    else
    {
        fprintf(stderr,"serialAllocate() - failed to allocate a port\n");
        fflush(stderr);
        exit(1);
    }

    return sp;
}

void serialFree(SerialPort *sp)
{
    if (sp)
        free(sp);
}



/*
 * The is a mandatory pre-step before Open()ing the port.
 * It sets all data structures to initial states, and prepares
 * bit-flags for the desired speed and format of the data.
 * It does not actually touch the hardware at this stage, so
 * no errors can be generated.
 */
void serialInitialise(SerialPort *sp, const char *dev_name)
{
    //printf("*** serialInitialise(sp=%p, dev_name=%s) ... \n",sp,dev_name); fflush(stdout);
    if (sp != NULL)
    {
        sp->device_name[0] = '\0';
        if (dev_name != NULL)
        {
            strncpy(sp->device_name,dev_name,SERIAL_DEVICE_NAME_SIZE);
            sp->device_name[SERIAL_DEVICE_NAME_SIZE-1] = '\0';
        }

        sp->connected  = 0;
        sp->handle     = 0;
        sp->ms_timeout = SERIAL_DEFAULT_TIMEOUT; // milliseconds
    }
    //printf("*** serialInitialise(sp=%p, dev_name=%s) - done\n",sp,dev_name); fflush(stdout);

}



/*
 * Sets the state of the port to how it was when we found it
 */
void serialUnInitialise(SerialPort *sp)
{
    if (sp)
    {
        tcsetattr(sp->handle,TCSANOW,&(sp->original_settings));
        sp->connected = 0;
        sp->handle    = 0;
    }
}


/*
 * Open the port - this is the business-end of the initialisation process.
 * This performs the actual dealing with the hardware, and can generate
 * lots of different errors.
 *
 * Returns 1 on success, 0 on error.
 * If zero is returned call spGetError() to see what went wrong.
 */
int serialOpen(SerialPort *sp)
{
    int rc = 0;

    //printf("*** serialOpen(sp=%p) ... \n",sp); fflush(stdout);

    if (sp != NULL)
    {
        if (sp->connected != 0)
        {
            serialClose(sp);
        }

        sp->handle = open(sp->device_name,O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
        if (sp->handle < 0)
        {
            /* Failed, consult errno */
            sp->handle = 0;
            printf("*** serialOpen() - failed to open [%s]\n",sp->device_name);
        }
        else
        {
            //printf("*** serialOpen() - Opened [%s] OK\n",sp->device_name);

            sp->connected = 1;
            rc = 1;

            tcgetattr(sp->handle,&(sp->original_settings)); /* save current port settings */
            tcgetattr(sp->handle,&(sp->port_settings));

            memset(&(sp->port_settings),0,sizeof(struct termios));

            sp->port_settings.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
            sp->port_settings.c_oflag = 0;
            cfmakeraw(&(sp->port_settings));

            sp->port_settings.c_cc[VINTR]    = 0;     /* Ctrl-c */
            sp->port_settings.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
            sp->port_settings.c_cc[VERASE]   = 0;     /* del */
            sp->port_settings.c_cc[VKILL]    = 0;     /* @ */
            sp->port_settings.c_cc[VEOF]     = 0;     /* Ctrl-d */
            sp->port_settings.c_cc[VTIME]    = 0;     /* inter-character timer unused */
            sp->port_settings.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
            sp->port_settings.c_cc[VSWTC]    = 0;     /* '\0' */
            sp->port_settings.c_cc[VSTART]   = 0;     /* Ctrl-q */
            sp->port_settings.c_cc[VSTOP]    = 0;     /* Ctrl-s */
            sp->port_settings.c_cc[VSUSP]    = 0;     /* Ctrl-z */
            sp->port_settings.c_cc[VEOL]     = 0;     /* '\0' */
            sp->port_settings.c_cc[VREPRINT] = 0;     /* Ctrl-r */
            sp->port_settings.c_cc[VDISCARD] = 0;     /* Ctrl-u */
            sp->port_settings.c_cc[VWERASE]  = 0;     /* Ctrl-w */
            sp->port_settings.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
            sp->port_settings.c_cc[VEOL2]    = 0;     /* '\0' */



            fcntl(sp->handle, F_SETFL, 0); /* tell it to block on reads */
            tcflush(sp->handle,TCIFLUSH);
            tcsetattr(sp->handle,TCSAFLUSH,&(sp->port_settings));
        }
    }


    //printf("*** serialOpen(sp=%p) - done \n",sp); fflush(stdout);

    return rc;
}

/*
 * Close the port.
 * Probably want to call spUnInitialise() next
 */
void serialClose(SerialPort *sp)
{
    if (sp)
    {
        if (sp->connected)
            close(sp->handle);
        sp->handle    = 0;
        sp->connected = 0;
    }
}




/*
 * Get the number of bytes in the output queue
 * waiting to be serialised by the port
 */
int serialGetWriteQueueSize(SerialPort *sp)
{
    int bytes = 0;
    if (sp != NULL)
        ioctl(sp->handle, TIOCOUTQ, &bytes);

    return bytes;
}


void serialWriteWaitComplete(SerialPort *sp, int byte_count)
{
    if (sp != NULL)
        ioctl(sp->handle, TCSBRK, 1);

}




void serialBreak(SerialPort *sp, int duration)
{
    if (sp)
        tcsendbreak(sp->handle,duration);

}



/*
 * Get the number of bytes sitting in the input
 * queue, waiting to be read()
 */
int serialGetReadQueueSize(SerialPort *sp)
{
    int bytes = 0;
    if (sp)
        ioctl(sp->handle, FIONREAD, &bytes);  //TIOCINQ

    return bytes;
}



void serialFlushInput(SerialPort *sp)
{
    if (sp)
    {
        tcflush(sp->handle,TCIFLUSH);
    }
}
void serialFlushOutput(SerialPort *sp)
{
    if (sp)
        tcflush(sp->handle,TCOFLUSH);
}


void serialSetBlocking(SerialPort *sp)
{
    if (sp)
#if defined ( QNX )
        fcntl(sp->handle,F_SETFL, O_NDELAY);
#else
        fcntl(sp->handle,F_SETFL, FNDELAY);
#endif
}



void serialSetNonBlocking(SerialPort *sp)
{
    if (sp)
        fcntl(sp->handle,F_SETFL,0);
}



/*
 * Use select() to watch the serial port for incoming data.
 * This is (probably) the most accurate, and cross-platform way
 * of implementing a serial port timeout.
 *
 * Returns 1 on data arrived, and 0 on timeout or error
 */
int serialWaitForData(SerialPort *sp)
{
    int rc = 0;
    struct timeval tv;
    fd_set read_fds,exception_fds;

    tv.tv_sec = sp->ms_timeout / 1000L;
    tv.tv_usec = 1000L * (sp->ms_timeout % 1000L);

    FD_ZERO(&read_fds);
    FD_SET(sp->handle, &read_fds);
    FD_ZERO(&exception_fds);
    FD_SET(sp->handle,&exception_fds);

    int select_error = select((sp->handle)+1, &read_fds, NULL, &exception_fds, &tv);
    if (select_error > 0)
    {
        if (FD_ISSET(sp->handle, &read_fds))
            rc = 1;
        else if (FD_ISSET(sp->handle, &exception_fds))
            rc = 0;  // some exception flagged, change here to handle separately
    }

    return rc;
}


/*
 * set the read-timeout parameter
 */
void serialSetTimeout(SerialPort *sp, long millisecond_timeout)
{
    if (sp)
    {
        if (millisecond_timeout < MIN_SERIAL_TIMEOUT)
        {
            millisecond_timeout = SERIAL_DEFAULT_TIMEOUT;
        }
        sp->ms_timeout = millisecond_timeout;
    }
}


/*
 * Write the c-style string to the port.
 * This is just a simpler version (how?) of serialWrite(), see that
 * for further detail
 *
 * Note: It appears that this functions is not used.
 */
int serialWriteStr(SerialPort *sp,const unsigned char *buffer,int *byte_count)
{
    int count = 0;
    int rc = 0;

    if (sp)
    {
        count = (int)strlen((char *)buffer);
        rc = serialWrite(sp,buffer,&count);
    }

    if (rc)
        *byte_count = count;
    else
        *byte_count = 0;

    return rc;
}


/*
 * Write the given data to the serial port.
 * The amount of written is returned in <byte_count>, this
 * may or may not be equal to the amount you passed over.
 * Not achieving a full transmit is not an error, you will only
 * get an error if 0 (zero) bytes are written.
 *
 * Returns 1 on (partial-)success, and 0 on error
 */
int serialWrite(SerialPort *sp,const unsigned char *buffer,int *byte_count)
{
    int rc = 0;
    int written = 0;

    if (sp)
        written = (int)write(sp->handle,buffer,*byte_count);

    if (written >= 0)
    {
        /* ok */
        *byte_count = written;
        rc = 1;

#ifdef DEBUG_STRINGS
        fprintf(stderr,"*** serialWrite() - write() %u bytes\n",written); fflush(stderr);
        hexDump((unsigned char *)buffer,written);
#endif
    }
    else
    {
        /* error */
        *byte_count = 0;
    }

    return rc;
}



/*
 * Read up to <byte_count> bytes from the port.
 * This function will block if the port is in blocking mode.
 *
 * On return <byte_count> indicates the number of bytes placed in
 * <buffer>.  On error, <byte_count> will be zero, and the
 * content of <buffer> undefined.
 *
 * Returns 1 on success, 0 or error
 */
int serialRead(SerialPort *sp,unsigned char *buffer,int *byte_count)
{
    int rc = 0;
    int did_read = 0;
    int timeout = 0;

    char local_buffer[512];
    int  local_buffer_size = sizeof(local_buffer);

    if (*byte_count < local_buffer_size)
        local_buffer_size = *byte_count;

    if (sp)
    {
//TODO: what happens if one byte is available, but we try to read a bunch of data?  (i.e. block or timeout? )
        if (serialWaitForData(sp))
        {
            //fprintf(stderr,"*** serialRead() - sp=%p, buffer=%p, byte_count=%p %d\n",sp,buffer,byte_count,(byte_count?*byte_count:-1)); fflush(stderr);
            did_read = (int)read(sp->handle,local_buffer,local_buffer_size);
            *byte_count = did_read;
        }
        else
        {
            // Timed out
            *byte_count = 0;
            timeout = 1;
#ifdef DEBUG_STRINGS
            fprintf(stderr,"*** serialRead() - timed out (%ums) before read\n",sp->ms_timeout); fflush(stderr);
#endif
        }
    }

    if (did_read >= 0)
    {
        /* ok */
        rc = 1;

        memcpy(buffer,local_buffer,did_read);

#ifdef DEBUG_STRINGS
        fprintf(stderr,"*** serialRead() - read() returned %u bytes\n",did_read); fflush(stderr);
        hexDump((unsigned char *)local_buffer,did_read);
#endif
    }
    else if (timeout == 0)
    {
        switch(errno)
        {
            case EFAULT: printf("*** serialRead() - EFAULT\n"); break;
            case EINVAL: printf("*** serialRead() - EINVAL\n"); break;
            case EIO:    printf("*** serialRead() - EIO\n");    break;
            default:
                printf("*** serialRead() - Other errno - %d\n",errno);
        }
        fflush(stderr);

    }

    //fprintf(stderr,"*** fbkhost_sp_recv_frame() - serialRead() returning %s, %u bytes\n",rc?"true":"false",*byte_count); fflush(stderr);

    return rc;
}


