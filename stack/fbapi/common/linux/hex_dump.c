#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "hex_dump.h"


// output binary data like:
//     b3 11 21 00  00 00 00 41  40 00 00 b1  11 21 00 00    ..!....A@....!..
//     00 00 20 40  00 00 b1 10  20 00 00 00  00 60 40 00    .. @.... ....`@.
//     00 b1 11 22  00 00 00 00  70 40 00 00  b1 11 23 00    ..."....p@....#.

void hexDump(unsigned char *buffer,unsigned int length)
{
    unsigned int  looper  = 0;
    unsigned int  left_over = 0;
    unsigned int  i;
    unsigned char human_readable[17];

    memset(human_readable,' ',16);
    human_readable[16] = '\0';

    for (i=0; i<length; i++)
    {
        if (i > 0 && i % 16 == 0)
        {
            fprintf(stdout,"    %s\n",human_readable);
            looper = 0;
            memset(human_readable,' ',16);
        }

        if (isprint(buffer[i]))
            human_readable[looper] = buffer[i];
        else
            human_readable[looper] = '.'; // non-printable
        looper += 1;


        if (i > 0 && i % 4 == 0 && i%16 != 0)
            fprintf(stdout," ");

        fprintf(stdout,"%02x ",(unsigned int)buffer[i]);
    }

    // Add any spacing needed by short lines
    if (length % 16 != 0)
    {
        left_over = 16 - (length % 16);
        for (i=0; i<left_over; i++)
            fprintf(stdout,"   ");
        if (left_over >= 12)
            fprintf(stdout,"   ");
        else if (left_over >= 8)
            fprintf(stdout,"  ");
        else if (left_over >= 4)
            fprintf(stdout," ");
    }

    if (length > 0)
        fprintf(stdout,"    %s\n",human_readable);
}


