#include "single_keypress.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <termios.h>
#include <sys/select.h>
#include <sys/ioctl.h>

/* This holds the current terminal settings so we can restore them */
static struct termios stored_settings;

/* 
 * We need to catch some signals to restore the terminal settings 
 * This function is called by the signal() catcher...
 */
void resetSignalHandler(int sig)
{
    resetKeypressMode();
}



/*
 * Read a keystroke.
 * Will return immeadiately if setKeypressMode() is called first.
 */
unsigned char getKeypress()
{
    char ch;

    ch = getc(stdin);

    return ch;
}


/*
 * Determine the number of key-presses waiting to be read
 */
int keyPressed()
{
    int waiting = 0;
    struct timeval timeout;
    fd_set readers;

    timeout.tv_sec = 0;
    timeout.tv_usec = 100000; /* 100,000 is 0.1 seconds */

    FD_ZERO(&readers);
    FD_SET(fileno(stdin),&readers);

    /* use the select() to pause a split-second for a keypress */
    if (select(fileno(stdin)+1,&readers,NULL,NULL,&timeout))  
        ioctl(0,FIONREAD,&waiting);

    return waiting;
}


/*
 * Put the terminal into un-buffered mode so we can read
 * a single keypress (without [Enter]).
 */
void setKeypressMode()
{
    struct termios new_settings;

    tcgetattr(0,&stored_settings);
    new_settings = stored_settings;

    /* Disable canonical mode, and set buffer size to 1 byte */
    new_settings.c_lflag &= (~ICANON);
    new_settings.c_lflag &= (~ECHO);   /* turn echo off too */
    new_settings.c_cc[VTIME] = 0;
    new_settings.c_cc[VMIN] = 1;

    tcsetattr(0,TCSANOW,&new_settings);

    /* We need to hook into a bunch of signals in case the user 
     * halts / ^C the programme or suchlike
     *
     * TODO: using signal() is *bad*, don't do it.
     *       change all to use sigaction()
     */
    signal(SIGHUP,resetSignalHandler);
    signal(SIGINT,resetSignalHandler);
    signal(SIGQUIT,resetSignalHandler);
    signal(SIGABRT,resetSignalHandler);
    signal(SIGTERM,resetSignalHandler);

}

/* 
 * Exit single-keypress mode
 * The reset is necessary, since the terminal settings need
 * to be restored.
 */
void resetKeypressMode()
{
    tcsetattr(0,TCSANOW,&stored_settings);
}


