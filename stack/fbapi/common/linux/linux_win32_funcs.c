
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>  /* for strcasecmp */

#include "single_keypress.h"
#include "linux_win32_funcs.h"

/*
 * Delay the current thread for a short period.
 * (as opposed to LINUX sleep() which delays the whole process (i.e.: all threads)
 * The OS often does not delay for *exactly* this period, but it's usually within
 * a timeslice or two.
 *
 * <milliseconds> - the length of the delay
 *
 */
void Sleep(unsigned int milliseconds)
{
    struct timespec delay;
    delay.tv_sec  = milliseconds / 1000;
    delay.tv_nsec = (milliseconds - ((milliseconds / 1000) * 1000)) * 1000000;
    /* nano seconds x10^(-9) */
    nanosleep(&delay,&delay);
}


/*
 * Get the number of milliseconds the process has been running for.
 *
 */
unsigned int GetTickCount()
{
    unsigned int msec;
    unsigned int start_seconds;
    /* WARNING - Not thread safe */
    static unsigned int global_start_seconds = 0;  /* pass a start in for threaded apps */

    start_seconds = global_start_seconds;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    if (start_seconds == 0)
    {
        /* We need to store some base-point for time-zero */
        global_start_seconds =  tv.tv_sec;
        msec = (unsigned int)(tv.tv_usec / 1000);
    }
    else
    {
        /*           Seconds -> Milli-Seconds            Micro-seconds -> Milli-Seconds   */
        msec = (1000 * (tv.tv_sec - start_seconds)) + (unsigned int)(tv.tv_usec / 1000);
    }
    return msec;
}


/*
 * Given a FILE* handle, determine the length of the file,
 * returning the file pointer back to its original position
 *
 * TODO: should return long long, but portability is unclear
 */
unsigned long _filelength(FILE *fin)
{
    unsigned long rc = 0;
    if (fin)
    {
        unsigned long start_pos = ftell(fin);
        if (fseek(fin,0,SEEK_END))
        {
            rc = ftell(fin);
            fseek(fin,start_pos,SEEK_SET); /* restore to the original position */
        }
    }

    return rc;
}



/*
 * Case independant string comparison
 *
 */
int _stricmp(const char *str1, const char *str2)
{
    int rc = 0;

    /* strcasecmp() is POSIX compliant, but maybe not so portable */
    rc = strcasecmp(str1,str2);

    return rc;
}
int _strnicmp(const char *str1, const char *str2,size_t length)
{
    int rc = 0;

    /* strncasecmp() is POSIX compliant, but maybe not so portable */
    rc = strncasecmp(str1,str2,length);

    return rc;
}



/*
 * Just do something with this
 *
 */
void OutputDebugString(const char *str)
{
#ifdef DEBUG_STRINGS
    if (str)
        fprintf(stderr,"%s",str);
#endif
}



/*
 * Has *any* key been pressed
 */
int _kbhit()
{
    int press_count;

    setKeypressMode();
    press_count = (int)keyPressed();
    resetKeypressMode();

    return press_count;
}


/*
 * Get a keypress (without [Enter])
 */
int _getch()
{
    int ch;

    setKeypressMode();
    ch = (int)getKeypress();
    resetKeypressMode();

    return ch;
}




/*
 * Somewhat implement this windows time function using POSIX/C99 calls
 */
void GetLocalTime(SYSTEMTIME *system_time)
{
    time_t now;
    struct tm now_tm;

    if (system_time)
    {
        now = time(NULL);
        localtime_r(&now,&now_tm);

        system_time->wYear      = 1900 + now_tm.tm_year;
        system_time->wMonth     = 1 + now_tm.tm_mon;
        system_time->wDayOfWeek = now_tm.tm_wday;
        system_time->wDay       = now_tm.tm_mday;
        system_time->wHour      = now_tm.tm_hour;
        system_time->wMinute    = now_tm.tm_min;
        system_time->wSecond    = now_tm.tm_sec;
        system_time->wMilliseconds = 0;
    }
}



char *_getcwd(char *buffer, int maxlen)
{
    return getcwd(buffer,maxlen);
}

wchar_t *_wgetcwd(wchar_t *buffer, int maxlen)
{
    char *buf = (char*)malloc(sizeof(char) * maxlen);
    if (buf)
    {
        getcwd(buf,maxlen);
        const wchar_t *format = L"%hs";
        swprintf(buffer, maxlen, format, buf);  /* convert char to wchar_t */
        free(buf);
    }

    return buffer;
}

/*
 * NOTE: as per the win32 version on this function,
 * string lengths are not checked, and this can overwrite buffers
 * leading to security (and other) issues.
 *
 * But the original code uses this function, so we have implemented it.
 */
void _wsplitpath(const wchar_t *path, wchar_t *drive, wchar_t *dir, wchar_t *fname, wchar_t *ext)
{
    wchar_t *full_path_copy = (wchar_t*)malloc(sizeof(wchar_t) * (1 + wcslen(path)));
    if (full_path_copy)
    {
        wchar_t *path_copy = full_path_copy;
        wcscpy(full_path_copy,path); /* make a copy so we can cut it up */

        /* drive letter */
        if (drive)
            *drive = L'\0'; /* not for unix */

        /* extension */
        if (ext)
        {
            *ext = L'\0';
            wchar_t *wptr = wcsrchr(path_copy,L'.');
            if (wptr)
            {
                wptr++;
                wcscpy(ext,wptr);
            }
        }

        /* filename */
        if (fname)
        {
            *fname = L'\0';
            wchar_t *wptr = wcsrchr(path_copy,L'/');  /* we're assuming a LINUX path only */
            if (wptr)
            {
                wptr++;
                wcscpy(fname,wptr);
            }
            else
            {
                /* no path separators in given string - take everything */
                wcscpy(fname,path_copy);
            }

            /* spec. says no extension in the filename */
            wptr = wcsrchr(path_copy,L'.');
            if (wptr)
                *wptr = L'\0';
        }

        /* directory */
        if (dir)
        {
            *dir = L'\0';

            /* find the filename */
            wchar_t *wptr = wcsrchr(path_copy,L'/');  /* we're assuming a LINUX path only */
            if (wptr)
            {
                wptr += 1;
                *wptr = L'\0';  /* trim off the filename */

                /* now copy the remaining path */
                wcscpy(dir,path_copy);
            }
        }

        free(full_path_copy);
    }
}




wchar_t *_wfullpath(wchar_t *absPath, const wchar_t *relPath, size_t max_length)
{
    wchar_t *wcwd = (wchar_t *)malloc((1+max_length) * sizeof(wchar_t));
    if (wcwd)
    {
        _wgetcwd(wcwd,max_length);

        wcscpy(absPath,wcwd);
        wcscat(absPath,relPath);

        free(wcwd);
    }

    return absPath;
}



