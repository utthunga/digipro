#ifndef __SERIAL_PORT_H__
#define __SERIAL_PORT_H__

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#define SERIAL_DEVICE_NAME_SIZE 64

/*
 * Structure to hold the current state of the serial port.
 * This gets passed to every serial operation function.
 */
typedef struct _SerialPort
{
    int    handle;                    /* file handle of the port */
    int    connected;                 /* 1=connected, 0=disconnected */
    long   ms_timeout;                /* milliseconds read timeout */
    char   device_name[SERIAL_DEVICE_NAME_SIZE]; /* string name of device, e.g.: /dev/ttyS3 */
    struct termios original_settings; /* port stats to restore on close (copied on open) */
    struct termios port_settings;     /* holds the current port stats */
} SerialPort;


/* Formal Descriptions for these are in the .C code */
SerialPort *serialAllocate();
void serialFree(SerialPort *sp);
void serialInitialise(SerialPort *sp, const char *dev_name);
void serialUnInitialise(SerialPort *sp);
int  serialOpen(SerialPort *sp);
void serialClose(SerialPort *sp);
int  serialGetWriteQueueSize(SerialPort *sp);
void serialWriteWaitComplete(SerialPort *sp, int byte_count);
void serialBreak(SerialPort *sp, int duration);
int  serialGetReadQueueSize(SerialPort *sp);
void serialFlushInput(SerialPort *sp);
void serialFlushOutput(SerialPort *sp);
void serialSetBlocking(SerialPort *sp);
void serialSetNonBlocking(SerialPort *sp);
int  serialWriteStr(SerialPort *sp,const unsigned char *buffer,int *byte_count);
int  serialWrite(SerialPort *sp,const unsigned char *buffer,int *byte_count);
int  serialRead(SerialPort *sp,unsigned char *buffer,int *byte_count);
int  serialWaitForData(SerialPort *sp);
void serialSetTimeout(SerialPort *sp,long ms_timeout);

#endif /* #ifndef __SERIAL_PORT_H__ */



