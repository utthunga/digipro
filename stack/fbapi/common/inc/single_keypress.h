#ifndef __KEYPRESS_H__
#define __KEYPRESS_H__

/*
 * Put the terminal into un-buffered mode so we can read
 * a single keypress (without [Enter]).
 */
unsigned char getKeypress();

/*
 * Get the umber of key-presses waiting to be read
 */
int keyPressed();

/*
 * Enter and exit single-keypress mode
 * The reset is necessary, since the terminal settings need
 * to be restored.
 */
void setKeypressMode();
void resetKeypressMode();

#endif /*#ifndef __KEYPRESS_H__*/
