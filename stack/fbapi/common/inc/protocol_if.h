/*  Filename    : protocol_if.h                                              */
/*                                                                           */
/*  Description : Header file for protocol specific processing functions     */
/*                                                                           */
/*****************************************************************************/

#ifndef __PROTOCOL_IF_H__
#define __PROTOCOL_IF_H__


/* --- Function prototypes of internal functions -------------------- */
extern INT16 ffapi_check_send_req (VOID  *sdb_ptr,
    USIGN16 *sdb_len, VOID *data_ptr, INT16 *sdbdata_len);
extern INT16 papi_check_send_req (VOID  *sdb_ptr,
    USIGN16 *sdb_len, VOID *data_ptr, INT16 *sdbdata_len);
extern INT16 hart_api_check_send_req (VOID  *sdb_ptr,
    USIGN16 *sdb_len, VOID *data_ptr, INT16 *sdbdata_len);

extern USIGN16 ffapi_get_sdb_len (VOID);
extern USIGN16 papi_get_sdb_len (VOID);
extern USIGN16 hart_api_get_sdb_len (VOID);
extern INT16 fmsgdl_get_data_len(INT16 result, USIGN8 service, USIGN8 primitive, USIGN8 * data_ptr, INT16 * data_len);
extern INT16 diagdl_get_data_len(INT16 result, USIGN8 service, USIGN8 primitive, USIGN8 * data_ptr, INT16 * data_len);
extern INT16 nmagdl_get_data_len(INT16 result, USIGN8 service, USIGN8 primitive, USIGN8 * data_ptr, INT16 * data_len);
extern INT16 smgdl_get_data_len (INT16 result, USIGN8 service, USIGN8 primitive, USIGN8 * data_ptr, INT16 * data_len);
extern INT16 dpgdl_get_data_len (INT16,USIGN8,USIGN8,USIGN8*,INT16*);
extern INT16 fmbgdl_get_data_len(INT16,USIGN8,USIGN8,USIGN8*,INT16*);


#endif /*ndef __PROTOCOL_IF_H__ */

