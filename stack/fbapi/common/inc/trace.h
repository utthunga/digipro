/////////////////////////////////////////////////////////////////////////////
// TRACE.H
//
// Trace module header file.
//
// $Revision: 14 $
// $Date: 4/12/01 9:33p $
//
// Copyright � 2008, Softing AG.

// All rights reserved.

#ifndef TRACE_H
#define TRACE_H


#define ERR_PRINT  DbgPrint


#ifdef _DEBUG

#ifndef DBG_PRINT
 #define DBG_PRINT(_x_)  DbgPrint _x_
#endif

 #ifndef ASSERT
   #define ASSERT(c) {if(!(c)) DbgPrint("AssertionFailure File: %s Line: %s\n",__FILE__,__LINE__);}
 #endif

  #ifndef VERIFY
    #define VERIFY(c) ASSERT(c)
  #endif

#else

  #ifndef DBG_PRINT
    #define DBG_PRINT(_x_)
  #endif

  #ifndef ASSERT
    #define ASSERT(c) ((VOID)0)
  #endif

  #ifndef VERIFY
    #define VERIFY(c) c
  #endif

#endif

#define API_TRACE_FILE      "trace.log"

extern BOOL  CreateLogFile ();
extern void  CloseLogFile ();
extern void  DbgPrint (const char* pString, ...);

#endif
