/*  Filename    : fbapi_sp.h                                               */
/*                                                                           */
/*  Description : Header file for internal data structures and defines       */
/*                                                                           */
/*****************************************************************************/

#ifndef __FBAPI_SP_H__
#define __FBAPI_SP_H__


/** @brief defines for bits of enhanced options, set via registry key
 */
#define ENHANCED_OPTION_EXTENDED_DEBUG      0x0001uL    /**< enable additional debug output */


#ifndef API_LINUX
#    pragma pack(push, 2)
#    define PACK_WORD_ALIGNMENT
#else
#    include <pthread.h>
#    include "serial_port.h"
    /* this packing is the same as #pragma pack(push,2) but only for the given structure */
#    define PACK_WORD_ALIGNMENT  __attribute__ ((aligned(2),packed))
#endif

#define MAX_CHANNEL_ID      10




/* wait for serial action via WaitForSingleObject or use synchronous Read/WriteFile call*/
#define USE_WAIT_FOR_OBJECT

#ifdef API_LINUX
#undef USE_WAIT_FOR_OBJECT

#define INFINITE            0xFFFFFFFF  // Infinite timeout

typedef pthread_mutex_t *    OS_MUTEX;
pthread_mutex_t m_HapiMutex;

#define fbapi_CreateMutex(m)      do {(m) = &m_HapiMutex; \
                                       pthread_mutex_init ((m), NULL);} while(0)
#define fbapi_WaitForMutex(m,t)   do {pthread_mutex_lock( (m) );} while(0)
#define fbapi_ReleaseMutex(m)     do {pthread_mutex_unlock( (m) );} while (0)
#define fbapi_CloseMutex(m)       do {pthread_mutex_destroy( (m) );(m) = NULL;} while(0)

#else

typedef void *HANDLE;

typedef HANDLE  OS_MUTEX;

#define INFINITE            0xFFFFFFFF  // Infinite timeout

#define fbapi_CreateMutex(m)      do {(m) = CreateMutex( NULL, FALSE, "PapiAccess");} while(0)
#define fbapi_WaitForMutex(m,t)   do {WaitForSingleObject( (m), (t) );} while(0)
#define fbapi_ReleaseMutex(m)     do {ReleaseMutex( (m) );} while (0)
#define fbapi_CloseMutex(m)       do {CloseHandle( (m) ); (m) = NULL;} while (0)

#endif

extern OS_MUTEX    m_hHAPIMutex;

/*
 * access macros
 */
#define _U32_HIGH_BYTE(U32)                ((unsigned char) ((U32) >> 24))
#define _U32_BYTE2(U32)                    ((unsigned char) ((U32) >> 16))
#define _U32_BYTE3(U32)                    ((unsigned char) ((U32) >> 8) )
#define _U32_LOW_BYTE(U32)                 ((unsigned char)  (U32)       )

#define _U16_HIGH_BYTE(U16)                ((unsigned char) ((U16) >> 8))
#define _U16_LOW_BYTE(U16)                 ((unsigned char) (U16)       )

#define _HIGH_LOW_WORDS_TO_U32(High,Low)   ((unsigned long) ((((unsigned long) (High)) << 16) + (Low)))   /**< Returns the combined 32 bit value of the 16 bit parameters High and Low */
#define _HIGH_LOW_BYTES_TO_U16(High,Low)   ((unsigned short)((((unsigned short)(High)) << 8 ) + (Low)))   /**< Returns the combined 16 bit value of the 8 bit parameters High and Low */



/* --- type identifiers for serial protocol --- */
#define SP_SYNC                 1
#define SP_RESET                2
#define SP_SET_PARAM            3
#define SP_GET_PARAM            4
#define SP_READ_TRACE           5
#define SP_IF_MSG               6
#define SP_POLL_IF              7
#define SP_INIT_FW_DWNLD        8
#define SP_FW_DWNLD             9
#define SP_TERM_FW_DWNLD        10
#define SP_START_PA_FW          11
#define SP_START_FF_FW          12
#define SP_START_HART_FW        13
#define SP_VENDOR_REQ_PA        14
#define SP_VENDOR_REQ_FF        15
#define SP_VENDOR_REQ_HART      16
#define SP_POWEROFF             17


/* --- Sequence number has range 0 - 0x3f ---*/
/* --- Bit 0x80: 0: Host -> Device; 1: Device -> Host */
/* --- Bit 0x40: 0: Bootmode; 1: Firmware is running */
#define SP_FROM_HOST        0x00u
#define SP_FROM_DEVICE      0x80u

#define SP_BOOTMODE         0x00u
#define SP_FIRMWARE_MODE    0x40u

typedef struct _T_CSC_SP_HEADER
{
    USIGN8      seq_nr;
    USIGN8      type_id;
    USIGN16     length;

} T_CSC_SP_HEADER;

/* format of frame:  T_CSC_SP_HEADER + data + USIGN16 CRC */

/* definition of frame length */
#define CSC_SP_HEADER_LEN           4u
#define CSC_SP_CRC_LEN              2u
#define CSC_SP_ACK_LEN              2u
#define CSC_SP_FRAME_WRAP_LEN       6u
#define CSC_SP_VENDOR_FRAME_LEN     8u
#define CSC_SP_GET_PARAM_LEN        8u
#define CSC_SP_SET_PARAM_LEN        24u

/* --- Ids for Vendor Request                           ---*/
/* --- sent in request as USIGN16 value in Intel format --- */
#define SP_VR_DEVICE_INFO   1u      /* returned data is of type T_FBAPI_DEVICE_INFO */

/* --- typedefs, ids for setting parameter values --- */
#define SP_PARAM_ACK_TIMEOUT    1u
#define SP_PARAM_READ_TIMEOUT   2u


/**
 * Set parameter structure for parameter of type USIGN8, USIGN16 or USIGN32
 */
typedef struct _T_PARAM_REQ
{
    USIGN16     id;
    USIGN32     value;

} PACK_WORD_ALIGNMENT T_PARAM_REQ;

/**
 * Set parameter structure for parameter which are USIGN8 array of a given length.
 * Length is given implicitly by length of SP telegram
 */
typedef struct _T_PARAM_ARRAY_REQ
{
    USIGN16     id;
    USIGN8      value[4];

} PACK_WORD_ALIGNMENT T_PARAM_ARRAY_REQ;

typedef struct _T_GET_PARAM_REQ
{
    USIGN16     parameter_id;

}PACK_WORD_ALIGNMENT  T_GET_PARAM_REQ;

typedef struct _T_GET_PARAM_RSP
{
    USIGN16     result;
    USIGN32     value;

} PACK_WORD_ALIGNMENT T_GET_PARAM_RSP;


typedef enum
{
    PORT_STATE_OFFLINE = 0,
    PORT_STATE_IN_SYNC,
    PORT_STATE_NO_RESPONSE


} T_PORT_STATE;

/**
 * @brief Parameters of the COM port interface
 *
 */
typedef struct _T_SERIAL_IF
{
  USIGN32       baudrate;           /**< Baudrate for COM port              */
  USIGN8        parity;             /**< Parity used or not                 */
  USIGN8        stop_bits;          /**< Number of stop bits: 1             */
  USIGN8        data_bits;          /**< Number of data bits: 8             */

} T_SERIAL_IF;


/**
 * Data structure to describe used communication channel
 */
typedef struct _T_PORT_IF_DATA
{
    HANDLE          hComIF;        /**< handle for communication channel */

    unsigned int    seqNr;         /**< sequence number of last message */
    USIGN32         ackTimeout;    /**< acknowledge timeout             */
    USIGN32         readTimeout;   /**< timeout for POLL interface req  */

    USIGN16         ProtocolId;    /**< protocol selected for comm channel */
    T_PORT_STATE    portState;     /**< state of the communication channel */
    int             fwMode;        /**< 1: FW Mode, operational, 0: Bootmode */

#ifdef USE_WAIT_FOR_OBJECT
    OVERLAPPED      overWriteSync; /**< synchronizsation object for sending */
    OVERLAPPED      overReadSync;  /**< synchronization object for reception */
#endif

    T_FBAPI_INIT   pInitParam;    /**< initialization parameter storage */

    INT16 (*send_frame_fnc) (IN  HANDLE        hComIF,
#ifdef USE_WAIT_FOR_OBJECT
                             IN  LPOVERLAPPED  lpoverWriteSync,
#endif
                             IN  USIGN8       *buffer,
                             IN  USIGN16       buf_len);

    int (*recv_frame_fnc) (IN  HANDLE        hComIF,
#ifdef USE_WAIT_FOR_OBJECT
                           IN  LPOVERLAPPED  lpoverReadSync,
#endif
                           IN  USIGN8       *buffer,
                           IN  USIGN16      *buf_len,
                           IN  DWORD         timeout);

} T_PORT_IF_DATA;


#define DWNL_HEADER_SIZE    (0x80u+16u)
#define MAX_SREC_SIZE       (0x80u+8u)


/* --- Function prototypes of internal functions -------------------- */
extern INT16 fbapi_sp_set_send_recv_fnc  (T_FBAPI_CHANNEL_ID ChannelId);
extern INT16 fbapi_sp_sync_device        (T_FBAPI_CHANNEL_ID ChannelId);
extern INT16 fbapi_sp_set_param          (T_FBAPI_CHANNEL_ID ChannelId, USIGN16 param_id, USIGN32 value);
extern INT16 fbapi_sp_send_service       (T_FBAPI_CHANNEL_ID ChannelId, VOID *pSdb, USIGN16 sdb_len, USIGN8 *pData, USIGN16 data_len);
extern INT16 fbapi_sp_receive_service    (T_FBAPI_CHANNEL_ID ChannelId, VOID *pSdb, USIGN16 sdb_len, USIGN8 *pData, USIGN16 *pDataLen);
extern INT16 fbapi_sp_get_vendor_info    (T_FBAPI_CHANNEL_ID ChannelId, USIGN8 cmd_id, USIGN16 info_id, T_FBAPI_DEVICE_INFO *pDeviceInfo);
extern INT16 fbapi_sp_check_parameter_result (T_FBAPI_CHANNEL_ID ChannelId, USIGN16 parameter_id);
extern INT16 fbapi_sp_init_download      (T_FBAPI_CHANNEL_ID ChannelId, USIGN16 ProtocolId, USIGN8 *header, USIGN16 header_size);
extern INT16 fbapi_sp_download           (T_FBAPI_CHANNEL_ID ChannelId, USIGN8 *data, USIGN16 data_size, USIGN32 sequence_counter);
extern INT16 fbapi_sp_term_download      (T_FBAPI_CHANNEL_ID ChannelId, USIGN32 sequence_counter);
extern INT16 fbapi_sp_start_firmware     (T_FBAPI_CHANNEL_ID ChannelId, USIGN8 cmd_id, USIGN16 *pAddError);
extern INT16 fbapi_sp_reset_firmware     (T_FBAPI_CHANNEL_ID ChannelId);
extern INT16 fbapi_sp_poweroff           (T_FBAPI_CHANNEL_ID ChannelId);
extern INT16 fbapi_sp_read_parameter_info(T_FBAPI_CHANNEL_ID ChannelId, USIGN16 parameter_id, USIGN32 *value);

static unsigned short fbapi_sp_calc_crc (USIGN8 *binStream, USIGN32 nrBytes);


#ifndef API_LINUX
#pragma pack(pop)
#endif

#endif /*ndef __FBAPI_SP_H__ */

