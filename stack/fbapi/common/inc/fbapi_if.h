/*******************************************************************************************/
/*  Filename    : fbapi_if.h                                                               */
/*                                                                                         */
/*  Description : This file contains the global function declarations,                     */
/*                defines and types of the mobiLink/FFusb communication interface          */
/*                                                                                         */
/*                                                                                         */
/*  - Initialization:                                                                      */
/*    ---------------                                                                      */
/*    - fbapi_init                           -> initialize the API                         */
/*    - fbapi_start                          -> start the protocol stack                   */
/*                                                                                         */
/*  - Service interface:                                                                   */
/*    ------------------                                                                   */
/*    - fbapi_snd_req_res                    -> send service requests and responses        */
/*    - fbapi_rcv_con_ind                    -> receive service indications/confirmations  */
/*                                                                                         */
/*  - Exit                                                                                 */
/*    ----                                                                                 */
/*    - fbapi_exit                           -> stop and reset the communication           */
/*                                                                                         */
/*  - Util                                                                                 */
/*    ----                                                                                 */
/*    - fbapi_download_firmware              -> download firmware as file                  */
/*    - fbapi_get_device_info                -> get device information                     */
/*    - fbapi_get_device_paramter            -> get device parameters                      */
/*                                                                                         */
/*                                                                                         */
/*                                                                                         */
/*******************************************************************************************/


#ifndef __FBAPI_IF_H__
#define __FBAPI_IF_H__

#pragma pack(push, 2)

#include <wchar.h>
#include "api_linux.h"
#include "fbapi_type.h"
#include "ff_if.h"
#include "pb_if.h"

/**
 * @anchor MAX_Dev_REF maximum number of devices in list
 */
#define MAX_FB_DEVICES                    30

/** @name Positive return value of the FBAPI interface functions
* @{ */
#define E_OK                               0  /**< No error occurred */
/** @} */


/** @name Positive return values of the FBAPI interface function fbapi_rcv_con_ind()
* @{ */
#define NO_CON_IND_RECEIVED                0  /**< Nothing has been received */
#define CON_IND_RECEIVED                   1  /**< A confirmation or indication has been received */
/** @} */


/** \name Error return values of the FBAPI interface functions
* @{ */
#define   E_IF_FATAL_ERROR                 7 /**< Unrecoverable error on commModule HW */
#define   E_IF_DOWNLOAD_FIRMWARE           8 /**< Download firmware error */
#define   E_IF_INVALID_CHANNEL_ID          9 /**< Invalid channel ID */
#define   E_IF_NO_CNTRL_RES               10 /**< Device does not respond */
#define   E_IF_DEVICE_IN_USE              11 /**< Device in use */
#define   E_IF_INVALID_LAYER              12 /**< Invalid layer */
#define   E_IF_INVALID_SERVICE            13 /**< Invalid service identifier */
#define   E_IF_INVALID_PRIMITIVE          14 /**< Invalid service primitive */
#define   E_IF_INVALID_DATA_SIZE          15 /**< Data buffer is too small */
#define   E_IF_INVALID_COMM_REF           16 /**< Invalid communication reference */
#define   E_IF_NO_DEVICE                  17 /**< No device found */
#define   E_IF_INIT_INVALID_PARAMETER     18 /**< invalid initialize parameter */

#define   E_IF_COM_ERROR                  20 /**< Error occured in COM send/rx */
#define   E_IF_RESOURCE_UNAVAILABLE       21 /**< No resource available */
#define   E_IF_NO_PARALLEL_SERVICES       22 /**< No parallel services allowed */
#define   E_IF_SERVICE_CONSTR_CONFLICT    23 /**< Serv. tempor. not executable */
#define   E_IF_SERVICE_NOT_SUPPORTED      24 /**< Service not supported */
#define   E_IF_SERVICE_NOT_EXECUTABLE     25 /**< Service not executable */
#define   E_IF_STATE_CONFLICT             27 /**< PA stack only: MSAC2 state conflict */
#define   E_IF_INVALID_PARAMETER          30 /**< Wrong parameter in REQ */
#define   E_IF_INIT_FAILED                31 /**< Initialization of FBAPI or commModule HW failed  */
#define   E_IF_EXIT_FAILED                32 /**< Exit FBAPI or commModule HW failed */
#define   E_IF_API_NOT_INITIALIZED        33 /**< FBAPI is not initialized for this channel */
#define   E_IF_INVALID_PROTOCOL           34 /**< Unkown protocol ID */
#define   E_IF_INVALID_DEVICE             35 /**< Unkown device ID (not used for commModule) */
#define   E_IF_START_FAILED               36 /**< Failed to start firmware */
#define   E_IF_BOOTMODE                   37 /**< Spontanous switch from firmware mode to boot mode */
#define   E_IF_OS_ERROR                  255 /**< OS system error */
/** @} */


/** \name Service results
* @{ */
#define POS                                0 /**< Positive service result */
#define NEG                                1 /**< Negative service result. Typically a service-specific
                                                  error structure gives additional information on the
                                                  reason of the problem. */
/** @} */



/** \name Defines for selecting the communication protocol stack (fbapi_init, download)
 *
 * @{ */

#define  PROTOCOL_FF    0x0001u         /**< Use FF protocol */
#define  PROTOCOL_PA    0x0002u         /**< Use PROFIBUS-PA protocol */
/** @} */


/**
 * @brief Specify a communication channel for interfacing with a commModule hardware
 *
 * Parallel communication with multiple commModule hardwares is not supported yet.
 * When calling an FBAPI interface function the ChannelId 0 has to be used.
 *
 */
typedef unsigned int   T_FBAPI_CHANNEL_ID;


/** \name Defines for Restart value in the fbapi_exit functions
 *
 * @{ */

#define TRIGGER_HW_RESET    0x0000u      /**< Trigger HW reset on commModule HW */
#define CONTINUE_NO_RESTART 0x0002u      /**< Continue execution without a Restart */
/** @} */


/** \name Additional error codes for fbapi_start function
 * @anchor start_err_code
 * - <B>fbapi_start additional error codes</B> The following defines can be used:
 *
 * @{ */
#define START_ERR_FW_TYPE           0x1Eu       /**< Unknown firmware type       */
#define START_ERR_FW_INVALID        0x15u       /**< No valid firmware found     */
/** @} */


/** \name Ids for Set/Get device commModule parameters
 * @anchor param_id_val
 * @{ */
#define PARAM_ID_GET_RESET_REASON      0x0006u    /**< Reading reset reason */
#define PARAM_ID_LAS_ADDRESS           0x0009u    /**< Supported by FF stack only. Permits to read the node address of the station acting as LAS */
/** @} */


/** \name Defines for possible values indicating the reboot reason ie. for parameterID = PARAM_ID_GET_RESET_REASON
 *  The request is responded only if commModule is running in Bootblock mode. If protocol stack is executed fbapi_get_parameter_request is returned negative
 *
 * @{ */
#define RESET_REASON_UNKOWN     0x00        /**< reset reason is unknown */
#define RESET_REASON_POWER      0x01        /**< power on */
#define RESET_REASON_LVD0       0x02        /**< LVD0 voltage supervision triggered reset */
#define RESET_REASON_WATCHDOG   0x03        /**< hardware watchdog has expired */
#define RESET_REASON_SOFTWARE   0x04        /**< software reset: exception or via fbapi_exit */
#define RESET_REASON_WARMSTART  0x05        /**< processor warmstart detected */
#define RESET_REASON_HARDWARE   0x06        /**< hardware reset occured */
/** @} */


/**
 * @brief Parameters of the COM port interface
 *
 */
typedef struct
{
  LPCTSTR       ComportName;       /**< Name of COM port to be used        */

} T_SERIAL_PARAM;




/**
 * @brief Structure for configuring the connection to the commModule MBP hardware
 *
 */
typedef struct _T_FBAPI_INIT
{
  USIGN32       AcknowledgeTimeout;     /**< Timeout in ms for acknowledge      */
  USIGN32       ReadTimeout;            /**< Timeout in ms for read response    */

  T_SERIAL_PARAM    serial;             /**< serial com port settings */

} T_FBAPI_INIT;

typedef T_FBAPI_INIT*  PT_FBAPI_INIT;

#pragma pack(pop)



/*****************************************************************************/
/**
 * @brief Structure for getting information on the commModule hardware and firmware
 *
 */
typedef struct
{
  USIGN16   DeviceId;                    /**< Softing device identification of commModule hardware */
  USIGN16   HardwareRevision;            /**< Revision of the commModule hardware */
  USIGN32   SerialDeviceNumber;          /**< Serial number of the commModule hardware */
  USIGN32   BootblockVersion;            /**< Version of the commModule bootblock */
  STRINGV   BootblockVersionString[64];  /**< Version string of the commModule bootblock */
  USIGN32   FirmwareVersion;             /**< Version of the commModule firmware */
  STRINGV   FirmwareVersionString[64];   /**< Version string of the commModule firmware */

} T_FBAPI_DEVICE_INFO;



/*****************************************************************************/
/**
 * @brief Structure for updating the commModule firmware. (Firmware update is not supported yet.)
 *
 */
typedef struct _T_FBAPI_DOWNLOAD_FIRMWARE
{
  char FileName[260];       /**< Name of the file containing the firmware to be downloaded */

} T_FBAPI_DOWNLOAD_FIRMWARE;




/* The following ifdef block is the standard way of creating macros which make exporting
 * from a DLL simpler. All files within this DLL are compiled with the FBAPI_EXPORTS
 * symbol defined on the command line. This symbol should not be defined on any project
 * that uses this DLL. This way any other project whose source files include this file see
 * FBAPI functions as being imported from a DLL, whereas this DLL sees symbols
 * defined with this macro as being exported. */

#ifdef FBAPI_EXPORTS
#define FBAPI __declspec(dllexport)
#else
#define FBAPI __declspec(dllimport)
#endif

// TODO - analyze this
#undef FBAPI
#define FBAPI

/**
 * Following define can be set to __stdcall or __cdecl to implicitly define the
 * calling convention. The DLL is compiled with /Gd compiler flag i.e. using __cdecl convention
 */
#define FBAPI_CALL_CONV

#ifdef __cplusplus
  extern "C" {
#endif

/**
 * @brief Initialize a communication interface
 *
 * fbapi_init() is used to initialize the communication with a commModule hardware.
 *
 * The commModule hardware as well as the communication parameters are specified
 * in the pInitParam structure
 *
 * Parameter ChannelId is used as a handle for identifying the commModule hardware
 * in the following FBAPI function calls.
 *
 * @return
 * - type  :    INT16
 * - values:
 *    + #E_OK                   : Interface to commModule HW is initialized
 *    + #E_IF_INVALID_CHANNEL_ID: Invalid ChannelId or ChannelId already in use
 *    + #E_IF_INVALID_PROTOCOL  : Invalid ProtocolId
 *    + #E_IF_NO_DEVICE         : Initialization of the interface failed.
 *    + #E_IF_INVALID_PARAMETER : Wrong parameter
 *
 * @param[in]   ChannelId  Identifier for selected Humminbird hardware
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   ProtocolId  Identification of communication protocol
 * - type  :    USIGN16
 * - values:    PROTOCOL_HART, PROTOCOL_FF
 *
 * @param[in]   pInitParam   attributes for commuication interface
 * - type  :    #PT_FBAPI_INIT
 * - values:    valid pointer
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_init
    (
        T_FBAPI_CHANNEL_ID  ChannelId,
        USIGN16             ProtocolId,
        PT_FBAPI_INIT       pInitParam
    );


/**
 * @brief Start the commModule communication stack
 *
 * After startup the commModule firmware is running in boot mode.
 * fbapi_start() is used to start one of the communication stacks implemented
 * in the commModule firmware.
 *
 * The function waits for an acknowledgement from the started communication stack.
 *
 * If the communication stack is already running the function returns immediately
 * with positive result.
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #E_OK                     : stack firmware is started successfully
 *   + #E_IF_API_NOT_INITIALIZED : channel not initialized
 *   + #E_IF_INVALID_CHANNEL_ID  : invalid channel id
 *   + #E_IF_START_FAILED        : failed to start FW. See pAddError for additional error information
 *                                 Values for error information are given in @ref start_err_code "additional error codes for fbapi_start"
 *   + #E_IF_OS_ERROR            : Send or receive error. Connection might be aborted by local or remote peer.
 *
 * @param[in]   ChannelId  Identification of commModule HW (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   pAddError      buffer to return error code from bootloader
 * - type  :    USIGN16*
 * - values:    valid pointer
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_start
    (
        T_FBAPI_CHANNEL_ID    ChannelId,
        USIGN16              *pAddError
    );



/**
 * @brief Reset a communication interface
 *
 * fbapi_exit() is used to Restart the moviLink hardware and close the
 * communication interface
 *
 * Depending on the parameter 'Restart' the bootloader will be
 * executed afterwards or the communication stack will be running again.
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #E_OK                     : stack firmware is started successfully
 *   + #E_IF_API_NOT_INITIALIZED : channel not initialized
 *   + #E_IF_INVALID_CHANNEL_ID  : invalid channel id
 *
 * @param[in]   ChannelId  Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   Restart
 * - type  :    USIGN16
 * - values:    TRIGGER_HW_RESET, CONTINUE_NO_RESTART
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_exit
    (
        T_FBAPI_CHANNEL_ID  ChannelId,
        USIGN16             Restart
    );



/**
 * @brief Send service requests
 *
 * fbapi_snd_req_res() is used to send HART and FF service requests.
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #E_OK                        : No error occured
 *   + #E_IF_FATAL_ERROR            : Unrecoverable error on commModule HW
 *   + #E_IF_INVALID_CHANNEL_ID     : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED    : FBAPI not initialized for this channel
 *   + #E_IF_INVALID_LAYER          : Invalid layer (for FF only)
 *   + #E_IF_INVALID_SERVICE        : Invalid service identifier
 *   + #E_IF_INVALID_PRIMITIVE      : Invalid service primitive
 *   + #E_IF_INVALID_COMM_REF       : Invalid communication reference (for FF only)
 *   + #E_IF_RESOURCE_UNAVAILABLE   : For lack of resources commModule HW is not able to handle the request
 *   + #E_IF_NO_PARALLEL_SERVICES   : Parallel services are not allowed
 *   + #E_IF_SERVICE_CONSTR_CONFLICT: Service temporarily not executable
 *   + #E_IF_SERVICE_NOT_SUPPORTED  : Service not supported in subset
 *   + #E_IF_SERVICE_NOT_EXECUTABLE : Service not executable
 *   + #E_IF_NO_CNTRL_RES           : commModule HW does not respond
 *   + #E_IF_INVALID_DATA_SIZE      : Number of data bytes not supported (too many data bytes)
 *
 * @anchor fbapi_snd_param
 * @param[in]   ChannelId  Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   pSdb     Pointer to protocol specific service description block
 * - type  :    PVOID
 * - values:
 *   + In case of PROTOCOL_FF a pointer to a T_FIELDBUS_SERVICE_DESCR structure is required
 *
 * @param[in]   pData     Pointer to service specific data block
 * - type  :    PVOID
 * - values:    valid pointer or NULL (if no service data)
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_snd_req_res
    (
        T_FBAPI_CHANNEL_ID    ChannelId,
        PVOID                 pSdb,
        PVOID                 pData
    );



/**
 * @brief Receive service confirmations and indications
 *
 * fbapi_rcv_con_ind() is used to receive HART service confirmations as
 * well as FF service confirmations and service indications
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #CON_IND_RECEIVED            : A confirmation or indication has been received
 *   + #NO_CON_IND_RECEIVED         : No confirmation or indication has been received
 *   + #E_IF_FATAL_ERROR            : Unrecoverable error on commModule HW
 *   + #E_IF_INVALID_CHANNEL_ID     : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED    : FBAPI not initialized for this channel
 *   + #E_IF_INVALID_DATA_SIZE      : Number of data bytes not supported (too many data bytes)
 *
 * @anchor fbapi_rcv_param
 * @param[in]   ChannelId        Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   pSdb           Pointer to protocol specific service description block
 * - type  :    PVOID
 * - values:
 *   + In case of PROTOCOL_FF a pointer to a #T_FIELDBUS_SERVICE_DESCR structure is required
 *
 * @param[out]  pData          Pointer to buffer for receiving confirmations and indcations
 * - type  :    PVOID
 * - values:    Buffer pointer. When fbapi_rcv_con_ind () returns with #CON_IND_RECEIVED then
 *              the buffer contains the confirmation or indication data
 *
 * @param[inout] pDataLen     Pointer to maximum/actual data block length
 * - type  :    USIGN16 *
 * - values:
 *   + [in] When the application calls fbapi_rcv_con_ind () it has to specify the length
 *          of the receive buffer
 *   + [out] If fbapi_rcv_con_ind () returns with #CON_IND_RECEIVED then FBAPI will insert the
 *           length of the confirmation or indication data
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_rcv_con_ind
    (
        T_FBAPI_CHANNEL_ID   ChannelId,
        PVOID                pSdb,
        PVOID                pData,
        USIGN16 *            pDataLen
    );



/**
 * @brief Get information on the commModule hardware and firmware
 *
 * fbapi_get_device_info() is used to get information on the commModule hardware
 * and firmware.
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #E_OK                    : Version information read successfully
 *   + #E_IF_INVALID_CHANNEL_ID : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED: FBAPI is not initialized for this channel
 *
 * @param[in]   ChannelId  Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   ProtocolId  Identification of communication stack
 * - type  :    USIGN16
 * - values:    PROTOCOL_HART, PROTOCOL_FF
 *
 * @param[out]  pDeviceInfo  Pointer to buffer to store read version information
 * - type  :    #T_FBAPI_DEVICE_INFO
 * - values:    valid pointer
 *
 */
FBAPI INT16 FBAPI_CALL_CONV fbapi_get_device_info
    (
        T_FBAPI_CHANNEL_ID   ChannelId,
        USIGN16              ProtocolId,
        T_FBAPI_DEVICE_INFO *pDeviceInfo
    );

/**
 * This function is used to read the device (commModule/FFusb) parameters specfied by ParameterId.
 * Currently all parameters are of type USIGN32 i.e. the data length is always 4.
 * Returned data are in Little Endian (Intel) data format.
 *
 * @return
 * - type  :    INT16
 * - values:
 *     + #E_OK                    : version information read successfully
 *     + #E_IF_INVALID_CHANNEL_ID : invalid channel id
 *     + #E_IF_API_NOT_INITIALIZED: API not initialized
 *     + #E_IF_BOOTMODE           : Bootblock is executed instead of firmware. Use fbapi_get_parameter for parameter = PARAM_ID_GET_RESET_REASON to identify reason
 *     + #E_IF_OS_ERROR           : Send or receive error. Connection might be aborted by local or remote peer.
 *
 * @param[in]   ChannelId               Identification of communication stack channel
 * - type  :    T_FBAPI_CHANNEL_ID
 * - values:    0..MAX_FB_DEVICES
 *
 * @param[in]   ParameterId             @ref param_id_val "ID of the parameter" to be retrieved
 * - type  :    USIGN16
 * - values:    described in section parameters above

 *
 * @param[in]   ParameterDataLen  length of parameter data
 * - type  :    USIGN16
 * - values:    length of Parameter Data to read
 *
 *
 * @param[out]  pParameterData      pointer to parameter data
 * - type  :    USIGN8*
 * - values:    valid pointer
 */

FBAPI INT16 FBAPI_CALL_CONV fbapi_get_device_parameter
    (
      T_FBAPI_CHANNEL_ID    ChannelId,
      USIGN16               ParameterId,
      USIGN16               ParameterDataLen,
      USIGN8*               pParameterData
    );


/**
 * This function is used to perform firmware update
 *
 * @return
 * - type  :    INT16
 * - values:
 *   + #E_OK                    : Version information read successfully
 *   + #E_IF_INVALID_PARAMETER  : No firmware file name given
 *   + #E_IF_SERVICE_CONSTR_CONFLICT: Service can be executed only while bootloader is running
 *   + #E_IF_DOWNLOAD_FIRMWARE  : Error during download of file
 *   + #E_IF_INVALID_CHANNEL_ID : Invalid channel id
 *   + #E_IF_API_NOT_INITIALIZED: FBAPI is not initialized for this channel
 *   +
 *
 * @param[in]   ChannelId  Identification of communication stack (as used in fbapi_init())
 * - type  :    #T_FBAPI_CHANNEL_ID
 * - values:    0 - Parallel access to multiple commModule interfaces is not supported yet.
 *
 * @param[in]   ProtocolId  Identification of communication stack
 * - type  :    USIGN16
 * - values:    PROTOCOL_HART, PROTOCOL_FF
 *
 * @param[out]  pDownloadFirmware       name of firmware file
 * - type  :    T_DOWNLOAD_FIRMWARE*
 * - values:    valid pointer
 */
 FBAPI INT16 FBAPI_CALL_CONV fbapi_download_firmware
    (
        T_FBAPI_CHANNEL_ID          ChannelId,
        USIGN16                     ProtocolId,
        T_FBAPI_DOWNLOAD_FIRMWARE  *pDownloadFirmware
    );




#ifdef __cplusplus
 }
#endif

#endif /* __FBAPI_IF_H__ */

