#ifndef __CIRCULAR_BYTE_BUFFER__
#define __CIRCULAR_BYTE_BUFFER__

/*
 * Kingsley Turner
 * 2011-Aug-19
 *
 * This objects implements a compact data buffer based on an array
 * which grows (but never auto. shrinks) to cover its content needs.
 * It's a FIFO/Queue/Ring Buffer/Circular Buffer, call it what you will.
 *
 * It maintains a FIFO queue inside a block, marking head and tail
 * positions so that it can efficiently add and remove from the 
 * block.  Eventually it will get into the position where the head
 * marker is after the tail marker in the block.  This is not a
 * completely optimal way to be, as sometimes two function calls
 * are necessary to get data in and out.  But the data structure is
 * space efficient, and otherwise reasonably fast.
 *
 */

/*
 * Ported from C++ to C version 2014-04-29
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct _CircularByteBuffer
{
    /// the buffer is implemented as a circular buffer
    /// it holds valid data in <buffer> from <head> to <tail>
    /// if head > tail, then you must go around the horn
	unsigned char *buffer;
    unsigned int   max_size;
	unsigned int   head;
    unsigned int   tail;

} CircularByteBuffer;


CircularByteBuffer *cbbInitialise(unsigned int start_size);
void cbbFree(CircularByteBuffer *cbb);

// Move <size> bytes out of the buffer
unsigned int cbbGetDataFromBuffer(CircularByteBuffer *cbb,unsigned int size, unsigned char *to_buffer);

// Copy <size> bytes into the buffer.
// A return of false means you are out of memory, and some/none of the copy was done
bool cbbAddDataToBuffer(CircularByteBuffer *cbb,unsigned int size,const unsigned char *from_buffer);

// clear all content
void cbbPurge(CircularByteBuffer *cbb);  

// Grow or shrink the buffer 
// Shrinking past content size loses data from the tail-end
bool cbbSizeTo(CircularByteBuffer *cbb,unsigned int new_max_size);


// Get a bunch of statistics about the buffer
unsigned int cbbGetContentSize(CircularByteBuffer *cbb);
unsigned int cbbGetMaxSize(CircularByteBuffer *cbb);
unsigned int cbbGetRemainingCapacity(CircularByteBuffer *cbb);

bool cbbIsEmpty(CircularByteBuffer *cbb);
bool cbbIsFull(CircularByteBuffer *cbb);



#endif //#ifndef __CIRCULAR_BYTE_BUFFER__

