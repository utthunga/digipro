/*  Filename    : fbapi_type.h                                               */
/*                                                                           */
/*  Description : This file contains the FBAPI basic types                   */
/*                                                                           */
/*****************************************************************************/
#ifndef __FBAPI_TYPE_H__
#define __FBAPI_TYPE_H__

#if 0
typedef     unsigned char        FB_BOOL;
typedef     unsigned char        USIGN8;
typedef     unsigned short       USIGN16;
typedef     unsigned int         USIGN32;
typedef     unsigned char        STRINGV;
typedef     signed char          INT08;
typedef     signed short         INT16;

typedef     float                FLOAT;
typedef     double               DOUBLE;

#ifdef WIN32
typedef     int                  INT32;
#else
typedef     signed int           INT32;
#endif
#endif

typedef     void*                PVOID;

#ifndef API_LINUX
typedef     unsigned char        FB_BOOL;
typedef     unsigned char        OCTET;
typedef     unsigned char        USIGN8;
typedef     signed char          FB_INT8;
typedef     signed short         FB_INT16;
typedef     unsigned short       USIGN16;
typedef     unsigned long        USIGN32;
typedef     signed long          FB_INT32;
#else
// It's not portable to assume a long is only 32 bits.
// The formal definition is that it is at *least* 32 bits
//  - We'll re-define all these just to be sure we get exactly the size we originally had
#include <stdint.h>
typedef     uint8_t              FB_BOOL;
typedef     uint8_t              OCTET;
typedef     uint8_t              USIGN8;
typedef     int8_t               FB_INT8;
typedef     int16_t              FB_INT16;
typedef     uint16_t             USIGN16;
typedef     uint32_t             USIGN32;
typedef     int32_t              FB_INT32;

typedef     uint8_t              STRINGV;
typedef     int8_t               INT08;
typedef     int16_t              INT16;
typedef     int32_t              INT32;
#endif

typedef     float                FLOAT;
typedef     double               DOUBLE;

/* --- FBAPI true and false definitions --------------------------------------- */
#define     FB_TRUE              0xFFu
#define     FB_FALSE             0x00u



/* Additional basic types */
/* ---------------------- */
/*
   FBAPI uses additional basic types such as

   VOID
   BOOL   (TRUE,FALSE)
   INT8
   INT16
   INT32

   These basic types are provided by the WINDOWS/MSVC environment.

   If FBAPI is hosted to other environments (Linux, Android) missing basic types
   can be defined here.
*/

#endif /* __FBAPI_TYPE_H__ */

