/*---------------------------------------------------------------------------*/
/*  Filename    : keywords.h                                                 */
/*                                                                           */
/*  Description : This include file defines some keywords which are used     */
/*                for structuring and formatting the FFusb source files.     */
/*                These keywords are ignored by the the compiler.            */
/*                                                                           */
/*---------------------------------------------------------------------------*/


#ifndef __KEYWORDS_H__
#define __KEYWORDS_H__

#define INCLUDES
#define GLOBAL_DEFINES
#define LOCAL_DEFINES
#define EXPORT_TYPEDEFS
#define LOCAL_TYPEDEFS
#define EXPORT_DATA
#define IMPORT_DATA
#define LOCAL_DATA
#define FUNCTION
#define IN
#define OUT
#define INOUT
#define FUNCTIONAL_DESCRIPTION
#define ASSERTIONS
#define LOCAL_VARIABLES
#define FUNCTION_BODY
#define FUNCTION_DECLARATIONS
#define LOCAL                static
#define PUBLIC
#define GLOBAL

#endif /* __KEYWORDS_H__ */
