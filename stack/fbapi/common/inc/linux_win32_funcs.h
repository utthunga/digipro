#ifndef __LINUX_WIN32_FUNCS_H__
#define __LINUX_WIN32_FUNCS_H__

#include <stdio.h>
#include <wchar.h>

/* delay the thread for a bit */
void Sleep(unsigned int milliseconds);

/* How many milliseconds has the process been running */
unsigned int GetTickCount();

/* How big is a file */
unsigned long _filelength(FILE *fin);

/* Output a debug message */
void OutputDebugString(const char *str);

/* case insensitve compare */
int _stricmp(const char *str1, const char *str2);
int _strnicmp(const char *str1, const char *str2,size_t length);

/* make upper case */
#define toupper(ch) (((ch)>='a'&&(ch)<='z')?((ch)-('z'-'Z')):(ch))

/* get a character (without enter) */
int _kbhit();
int _getch();

/* current paths, etc. */
char *_getcwd(char *buffer, int maxlen);
wchar_t *_wgetcwd(wchar_t *buffer, int maxlen);
void _wsplitpath(const wchar_t *path, wchar_t *drive, wchar_t *dir, wchar_t *fname, wchar_t *ext);
wchar_t *_wfullpath(wchar_t *absPath, const wchar_t *relPath, size_t maxLength);

/* SYSTEMTIME */
typedef struct _SYSTEMTIME
{
    unsigned short wYear;
    unsigned short wMonth;
    unsigned short wDayOfWeek;
    unsigned short wDay;
    unsigned short wHour;
    unsigned short wMinute;
    unsigned short wSecond;
    unsigned short wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;

void GetLocalTime(SYSTEMTIME *system_time);

#endif // #ifndef __LINUX_WIN32_FUNCS_H__
