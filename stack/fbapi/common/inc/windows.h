// Stolen from #include <pb_type.h>

#ifndef __WINDOWS__
#define __WINDOWS__

#include "api_linux.h"

#ifndef NULL
   #ifdef __cplusplus
       #define NULL    0
   #else
       #define NULL ((void *)0)
   #endif
#endif


/* --- global type definitions --------------------------------------------- */
#ifndef VOID
#define     VOID                 void
#endif


typedef     unsigned char        OCTET;
typedef     unsigned char        USIGN8;
typedef     unsigned short       USIGN16;
typedef     unsigned int         USIGN32;

typedef     signed char          INT8;
typedef     signed short         INT16;

typedef     signed int           INT32;

typedef     float                FLOAT;
typedef     double               DOUBLE;

// typedef     char                 STRINGV;


// from /usr/include/w32api/_mingw.h
//#define _int64 long long

// from /usr/include/w32api/_bsd_types.h
typedef unsigned long long u_int64;
typedef long long _int64;


// Invented workarounds
#define __declspec(q) 

#endif
