#ifndef __HEX_DUMP_H__
#define __HEX_DUMP_H__


// output binary data like to stdout:
//     b3 11 21 00  00 00 00 41  40 00 00 b1  11 21 00 00    ..!....A@....!..
//     00 00 20 40  00 00 b1 10  20 00 00 00  00 60 40 00    .. @.... ....`@.
//     00 b1 11 22  00 00 00 00  70 40 00 00  b1 11 23 00    ..."....p@....#.

extern void hexDump(unsigned char *buffer,unsigned int length);


#endif //#ifndef __HEX_DUMP_H__
