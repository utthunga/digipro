#ifndef __API_LINUX_H__
#define __API_LINUX_H__

#include <unistd.h>
#include <stdint.h>
#include <wchar.h>
#include "hex_dump.h"

#define HANDLE       SerialPort *
#define UCHAR        unsigned char
#define DWORD        unsigned int
#define BOOL         int
#define FALSE        0
#define TRUE         1

#define ULONG        uint32_t /*unsigned long*/
#define LPVOID       void *
#define LPCTSTR      char *
#define PUCHAR       unsigned char *
#define LPOVERLAPPED int *
#define OVERLAPPED   int
#define _MAX_PATH    256
#define _MAX_DRIVE   256
#define _MAX_DIR     256
#define _MAX_FNAME   256
#define _MAX_EXT     256

// these are not actually used by the unix code
// but define them here for a clean compile
#define CBR_115200  115200
#define NOPARITY    0x00
#define ONESTOPBIT  0x00

#define INVALID_HANDLE_VALUE 0x00000000



#endif // #ifndef __API_LINUX_H__
