/*******************************************************************************************/
/*  Filename    : fmbgdl.c                                                                 */
/*                                                                                         */
/*  Description : The module provides functions for calculating the length of the service  */
/*                parameter blocks for FMB (Basic Fieldbus Management) service requests or */
/*                responses.                                                               */
/*                                                                                         */
/*******************************************************************************************/

#include "keywords.h"

INCLUDES

#ifndef API_LINUX
#    include <windows.h>
#    include <tchar.h>
#else
#   include <errno.h>
#   include "api_linux.h"
#   include "serial_port.h"
#endif /* API_LINUX */

#include "fbapi_if.h"

GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA

#ifndef API_LINUX
#pragma check_stack (off)
#endif

FUNCTION PUBLIC INT16 fmbgdl_get_data_len
          (
            IN  INT16      result,                         /* Service-Result */
            IN  USIGN8     service,                               /* Service */
            IN  USIGN8     primitive,                   /* Service-Primitive */
            IN  USIGN8     *data_ptr,                     /* pointer to data */
            OUT INT16      *data_len_ptr                   /* length of data */
          )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function is used to return the request data length of FMB-SERVICES

possible return values:
- E_OK
- E_IF_INVALID_SERVICE

-----------------------------------------------------------------------------*/
{
  LOCAL_VARIABLES

  INT16 ret_val = E_OK;

  FUNCTION_BODY

  switch (service)
  {
     case FMB_SET_CONFIGURATION:
          {
          T_FMB_SET_CONFIGURATION_REQ * req_ptr = (T_FMB_SET_CONFIGURATION_REQ *) data_ptr;
#ifdef WIN32
          req_ptr->sm7_active &= 0xFE;
#else
          req_ptr->sm7_active  = PB_FALSE;
#endif
          *data_len_ptr = sizeof(T_FMB_SET_CONFIGURATION_REQ);
          }
          break;

     case FMB_SET_BUSPARAMETER:
          *data_len_ptr = sizeof(T_FMB_SET_BUSPARAMETER_REQ);
          break;

#ifdef ALL_PB_SERVICES_SUPPORTED
     case FMB_SET_VALUE:
          {
          T_FMB_SET_VALUE_REQ *req = (T_FMB_SET_VALUE_REQ *) data_ptr;
          *data_len_ptr = (sizeof (T_FMB_SET_VALUE_REQ) + req->length);
          break;
          }

     case FMB_READ_VALUE:
          *data_len_ptr = sizeof(T_FMB_READ_VALUE_REQ);
          break;

     case FMB_LSAP_STATUS:
          *data_len_ptr = sizeof(T_FMB_LSAP_STATUS_REQ);
          break;

     case FMB_VALIDATE_MASTER:
          *data_len_ptr = sizeof(T_FMB_VALIDATE_MASTER_REQ);
          break;

     case FMB_READ_BUSPARAMETER:
     case FMB_EXIT:
     case FMB_RESET:
          *data_len_ptr = 0;
          break;
#endif /* ALL_PB_SERVICES_SUPPORTED */

     case FMB_GET_LIVE_LIST:
          *data_len_ptr = 0;
          break;

     default:
          *data_len_ptr = 0;
          ret_val   = E_IF_INVALID_SERVICE;
          break;
  }
  return(ret_val);
}

#ifndef API_LINUX
#pragma check_stack
#endif
