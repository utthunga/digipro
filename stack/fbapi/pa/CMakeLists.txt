#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    include the source files ###############################################
#############################################################################
set (PA_SRC ${PA_SRC}   ${CMAKE_CURRENT_SOURCE_DIR}/dpgdl.c
                          ${CMAKE_CURRENT_SOURCE_DIR}/fmbgdl.c
                          ${CMAKE_CURRENT_SOURCE_DIR}/papi.c)

set (FBAPI_SRC  ${FBAPI_SRC}    ${PA_SRC}   PARENT_SCOPE)
