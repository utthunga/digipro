/*-----------------------------------------------------------------------------------------*/
/*  Filename    : pb_err.h                                                                 */
/*                                                                                         */
/*  Description : This file contains the ABORT, REJECT, EVENT and ERROR                    */
/*                types and defines and the according reason codes created by PROFIBUS     */
/*                stack.                                                                   */
/*                                                                                         */
/*-----------------------------------------------------------------------------------------*/

#ifndef __PB_ERR__
#define __PB_ERR__

#pragma warning (disable : 4103)     /* used #pragma pack to change alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(push,2)
#else
#pragma pack(2)
#endif
#pragma warning (default : 4103)


/*****************************************************************************/
/*************   DP/V1 ABORT INSTANCES AND REASON CODES  *********************/
/*****************************************************************************/

#define DPM_ABORT_SUBNET_NO        0              /**< abort: no subnet */
#define DPM_ABORT_SUBNET_LOCAL     1              /**< abort: local subnet */
#define DPM_ABORT_SUBNET_REMOTE    2              /**< abort: remote subnet */

#define DPM_ABORT_REASON_MASK      0x0F           /**< abort: instance+reason: mask for reason */
#define DPM_ABORT_INSTANCE_MASK    0x30           /**< abort: instance+reason: mask for instance */

#define DPM_ABORT_INSTANCE_FDL     0x00           /**< abort instance code for FDL */
#define DPM_ABORT_INSTANCE_DDLM    0x10           /**< abort instance code for DDLM */
#define DPM_ABORT_INSTANCE_MSC2    0x10           /**< abort instance code for master-slave acyclic class 2 */
#define DPM_ABORT_INSTANCE_USER    0x30           /**< abort instance code for user */

#define DPM_ABORT_FDL_UE           (DPM_ABORT_INSTANCE_FDL|0x01)    /**< FDL user error */
#define DPM_ABORT_FDL_RR           (DPM_ABORT_INSTANCE_FDL|0x02)    /**< FDL no resource */
#define DPM_ABORT_FDL_RS           (DPM_ABORT_INSTANCE_FDL|0x03)    /**< FDL service is rejected */
#define DPM_ABORT_FDL_NR           (DPM_ABORT_INSTANCE_FDL|0x09)    /**< FDL no response data */
#define DPM_ABORT_FDL_DH           (DPM_ABORT_INSTANCE_FDL|0x0A)    /**< FDL data high reply */
#define DPM_ABORT_FDL_LR           (DPM_ABORT_INSTANCE_FDL|0x0B)    /**< FDL no local resource */
#define DPM_ABORT_FDL_RDL          (DPM_ABORT_INSTANCE_FDL|0x0C)    /**< FDL request data low rejected */
#define DPM_ABORT_FDL_RDH          (DPM_ABORT_INSTANCE_FDL|0x0D)    /**< FDL request data high rejected */
#define DPM_ABORT_FDL_DS           (DPM_ABORT_INSTANCE_FDL|0x0E)    /**< FDL master is not in token ring */
#define DPM_ABORT_FDL_NA           (DPM_ABORT_INSTANCE_FDL|0x0F)    /**< no response from remote FDL */

#define DPM_ABORT_MSC2_ABT_SE      (DPM_ABORT_INSTANCE_MSC2|0x01)   /**< service not allowed in this state */
#define DPM_ABORT_MSC2_ABT_FE      (DPM_ABORT_INSTANCE_MSC2|0x02)   /**< invalid request PDU received */
#define DPM_ABORT_MSC2_ABT_TO      (DPM_ABORT_INSTANCE_MSC2|0x03)   /**< connection timeout */
#define DPM_ABORT_MSC2_ABT_RE      (DPM_ABORT_INSTANCE_MSC2|0x04)   /**< invalid response PDU received */
#define DPM_ABORT_MSC2_ABT_IV      (DPM_ABORT_INSTANCE_MSC2|0x05)   /**< invalid service requested from user */
#define DPM_ABORT_MSC2_ABT_STO     (DPM_ABORT_INSTANCE_MSC2|0x06)   /**< send timeout in initiate was to small for slave */
#define DPM_ABORT_MSC2_ABT_IA      (DPM_ABORT_INSTANCE_MSC2|0x07)   /**< invalid additional address in slave */
#define DPM_ABORT_MSC2_ABT_OC      (DPM_ABORT_INSTANCE_MSC2|0x08)   /**< waiting for FDL_DATA_REPLY.con */


/****************************************************************************/
/*************    FMB ERROR CLASSES and ERROR CODES     *********************/
/****************************************************************************/

/** @name FMB error codes
* @anchor FMB_err_codes
* @{ */

#define E_FMB_CFG_DP_OTHER                 0x0900
#define E_FMB_CFG_DP_TOO_MANY_SLAVES       0x0901
#define E_FMB_CFG_DP_WRONG_IO_DATA_LEN     0x0902
#define E_FMB_CFG_DP_IO_ALIGNMENT_PB_ERROR 0x0903
#define E_FMB_CFG_DP_TOO_FEW_DIAG_ENTRIES  0x0904
#define E_FMB_CFG_DP_WRONG_DIAG_DATA_LEN   0x0905
#define E_FMB_CFG_DP_WRONG_BUS_PARA_LEN    0x0906
#define E_FMB_CFG_DP_WRONG_SLAVE_PARA_LEN  0x0907
#define E_FMB_CFG_DP_DPRAM_ERROR           0x0908

/** @} */

/****************************************************************************/
/*************    DP ERROR CLASSES and ERROR CODES      *********************/
/****************************************************************************/

/** @name DP error codes
* @anchor DP_err_codes
* @{ */

#define E_DP_OK                   0x0000        /**< acknowledgement positive */
#define E_DP_UE                   0x0001        /**< remote user error */
#define E_DP_RR                   0x0002        /**< resource at remote station are insufficient */
#define E_DP_RS                   0x0003        /**< remote service/SAP deactivated */
#define E_DP_RA                   0x0004        /**< access of remote SAP has been blocked */
#define E_DP_DL                   0x0008        /**< data low */
#define E_DP_NR                   0x0009        /**< no response */
#define E_DP_DH                   0x000A        /**< data high */
#define E_DP_NA                   0x0011        /**< no reaction from remote station */
#define E_DP_DS                   0x0012        /**< local entity disconnected */
#define E_DP_NO                   0x0013        /**< service not possible in this state */
#define E_DP_LR                   0x0014        /**< local resource not available */
#define E_DP_IV                   0x0015        /**< invalid parameter in request */
#define E_DP_TO                   0x0016        /**< service timeout expired */

#define E_DP_FE                   0x00C1        /**< format error in request frame */
#define E_DP_NI                   0x00C2        /**< function not implemented */
#define E_DP_AD                   0x00C3        /**< access denied */
#define E_DP_EA                   0x00C4        /**< area too large */
#define E_DP_LE                   0x00C5        /**< data block length exceeded */
#define E_DP_RE                   0x00C6        /**< format error in response frame */
#define E_DP_IP                   0x00C7        /**< invalid parameter */
#define E_DP_SC                   0x00C8        /**< sequence conflict */
#define E_DP_SE                   0x00C9        /**< sequence error */
#define E_DP_NE                   0x00CA        /**< area non-existent */
#define E_DP_DI                   0x00CB        /**< data incomplete */
#define E_DP_NC                   0x00CC        /**< not connected */

/** @} */


/*****************************************************************************/
/*************   DP/V1 ERROR PDU CODING                  *********************/
/*****************************************************************************/

/** @name DP error profile codes
* @anchor DP_err_profile_codes
* @{ */
#define DP_ERROR_DECODE_DPV1        0x80            /**< DPV1 protocol */
#define DP_ERROR_DECODE_PROFILE1    0xFE            /**< profile 1 */
#define DP_ERROR_DECODE_PROFILE2    0xFF            /**< profile 2 */

                                                            /* error_code_1 */

/*****************************************************************************/
/*************        ERROR DATA STRUCTURES      *****************************/
/*****************************************************************************/

#define MAX_PB_ERROR_DESCR_LENGTH    _PB_NAME_LENGTH(PB_ERROR_DESCR_LENGTH)

/** @brief PROFIBUS error structure
 *
 * @anchor profi_err_struct
 * If a negative service result is returned for a confirmation the related data block uses
 * the following data structure to provide additional information on the error.
 */
typedef struct _T_PB_ERROR
{
  USIGN16 class_code;                              /**< Service error codes. Depending on the used layer the service codes are listed
                                                        @ref DP_err_codes "here" or @ref FMB_err_codes "here" */
  INT16   add_detail;                              /**< Additional detail information depending on the error code */
  PB_STRINGV add_description[MAX_PB_ERROR_DESCR_LENGTH]; /**< Additional description of the error, service dependent */

} T_PB_ERROR;


/*****************************************************************************/
/*************      EXCEPTION STRUCTURES         *****************************/
/*****************************************************************************/

typedef struct _T_PB_EXCEPTION
{
  USIGN8  task_id;              /* task identifier in which execption occurs */
  USIGN8  par1;                                               /* parameter 1 */
  USIGN16 par2;                                               /* parameter 2 */
  USIGN16 par3;                                               /* parameter 3 */
} T_PB_EXCEPTION;


#pragma warning (disable : 4103)     /* used #pragma pack to reset alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(pop)
#else
#pragma pack()
#endif
#pragma warning (default : 4103)


#endif

