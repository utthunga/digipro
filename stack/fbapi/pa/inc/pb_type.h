/*-----------------------------------------------------------------------------------------*/
/*  Filename    : pb_type.h                                                                */
/*                                                                                         */
/*  Description : This file defines basic data types used by the PROFIBUS stack and API.   */
/*                                                                                         */
/*                                                                                         */
/*-----------------------------------------------------------------------------------------*/

#ifndef __PB_TYPE__
#define __PB_TYPE__

#include "pb_conf.h"                        /* PAPI configuration parameters */

/* --- other basic data types are defined in fbapi_type.h --- */
#ifndef NULL
   #ifdef __cplusplus
       #define NULL    0
   #else
       #define NULL ((void *)0)
   #endif
#endif


/* --- global type definitions --------------------------------------------- */
#ifndef VOID
#define     VOID                 void
#endif

typedef     unsigned char        PB_BOOL;
typedef     unsigned char        OCTET;

typedef     char                 PB_STRINGV;

#ifndef PB_CSTRING_NOT_USED
typedef     char                 CSTRING;
#endif


/* --- PROFIBUS true and false definitions --------------------------------- */
#define     PB_TRUE              0xFF
#define     PB_FALSE             0x00

/* --- PROFIBUS function declarations -------------------------------------- */
#define     LOCAL                static
#define     PUBLIC
#define     GLOBAL

#endif

