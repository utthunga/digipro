/*-----------------------------------------------------------------------------------------*/
/*  Filename    : pb_conf.h                                                                */
/*                                                                                         */
/*  Description : This file contains defines that determine the operating system           */
/*                of the PROFIBUS API.                                                     */
/*                                                                                         */
/*                                                                                         */
/*-----------------------------------------------------------------------------------------*/

#ifndef __PB_CONF__
#define __PB_CONF__

/*****************************************************************************/
/* COMPILER SWITCHES FOR COMPABLITY                                          */
/*****************************************************************************/


#ifndef PB_VER
#define PB_VER  546
#endif


/*****************************************************************************/
/* COMPILER SWITCHES DEPENDING ON OPERATING SYSTEM                           */
/*****************************************************************************/

#undef FAR
#undef HUGE

#ifndef API_LINUX

#if defined (WIN64) || defined (_WIN64)        /* compiling as WIN64 (amd64) */
   #undef  DOS16
   #undef  WIN32
   #undef  WIN64
   #undef  EXPORT
   #define WIN32
   #define EXPORT  __export
   #define FAR
   #define HUGE
   #define CALL_CONV  APIENTRY               /* call convention using WIN32 */

   #ifdef UNDER_RTSS
      #undef  CALL_CONV
      #define CALL_CONV  _stdcall      /* call convention using Windows RTX */
   #endif

   #ifdef PB_API_FUNC_NOT_USED
      #undef  CALL_CONV
      #define CALL_CONV                     /* call convention using NT-DDK */
   #endif

#elif defined (WIN32) || defined (_WIN32)          /* compiling as WIN32 */
   #undef  DOS16
   #undef  WIN32
   #undef  WIN64
   #undef  EXPORT
   #define WIN32
   #define EXPORT  __export
   #define FAR
   #define HUGE
   #define CALL_CONV  APIENTRY               /* call convention using WIN32 */

   #ifdef UNDER_RTSS
      #undef  CALL_CONV
      #define CALL_CONV  _stdcall            /* call convention using Windows RTX */
   #endif

   #ifdef PB_API_FUNC_NOT_USED
      #undef  CALL_CONV
      #define CALL_CONV                      /* call convention using NT-DDK */
   #endif
#else                                                  /* compiling as WIN16 */
   #if defined (WIN_DLL) || defined (_WINDOWS) || defined (_WINDLL)
       #undef  DOS16
       #undef  WIN16
       #undef  WIN32
       #undef  WIN64
       #define WIN16
   #endif

   #if defined (WIN16) || defined (_WIN16)
       #undef  DOS16
       #undef  WIN16
       #undef  WIN32
       #undef  WIN64
       #undef  EXPORT
       #undef  PASCAL
       #define WIN16
       #define EXPORT  __export
       #define FAR      _far
       #define PASCAL   _pascal
       #define CDECL    _cdecl
       #define HUGE     _huge
       #define CALL_CONV FAR pascal         /* calling convention using WIN16 */
   #endif
#endif

#endif


/*****************************************************************************/
/*              SUPPORTED SERVICES                                           */
/*****************************************************************************/
#define DP_SERVICES_SUPPORTED


/*****************************************************************************/
/*                        Implementation Constants                           */
/*                                                                           */
/* The constants given below define the sizes of various data structures in  */
/* the protocol software and thus influence memory consumption.              */
/*                                                                           */
/* NOTE: Do not change the following constants without recompiling the       */
/*       the protocol software on the communication controller               */
/*****************************************************************************/

/* -- constants of internal sizes of byte arrays --------------------------- */

#define PB_ERROR_DESCR_LENGTH         32     /* max length of error descript.*/

/*****************************************************************************/
/* USEFUL MACROS                                                             */
/*****************************************************************************/

/*****************************************************************************/
/* MACRO TO CALCULATE MAX_xxxx_NAME_LENGTH                                   */
/*                                                                           */
/* This macro calculates the internal sizes of byte arrays in a way that the */
/* desired alignment on byte, word or long word boundaries is achieved.      */
/* The alignment is specified by the constant ALIGNMENT (e. g. longword = 4) */
/*                                                                           */
/*****************************************************************************/

#define PB_ALIGNMENT                  0x02        /* alignment on word boundary */

#define _PB_NAME_LENGTH(length) ((length) + PB_ALIGNMENT - ((length) % PB_ALIGNMENT))

#endif
