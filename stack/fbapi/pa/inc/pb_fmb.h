/*-----------------------------------------------------------------------------------------*/
/*  Filename    : pb_fmb.h                                                                 */
/*                                                                                         */
/*  Description : This file contains the data types and defines of the Fieldbus-Basic      */
/*                Management component of the PROFIBUS stack.                              */
/*                                                                                         */
/*-----------------------------------------------------------------------------------------*/

#ifndef __PB_FMB__
#define __PB_FMB__

#pragma warning (disable : 4103)     /* used #pragma pack to change alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(push,2)
#else
#pragma pack(2)
#endif
#pragma warning (default : 4103)


/** @name FMB service identifier
 *
 * This contains the list of all available FMB services.
 * @anchor FMB_svc
 * @{ */
#define FMB_FM2_EVENT             19 /**< Event indication from the PA stack */
#define FMB_SET_BUSPARAMETER      22 /**< set the busparameter to be used when going online. This services
                                          uses the data structure @ref set_bp_req "FMB Set Busparameter" */
#define FMB_GET_LIVE_LIST         26 /**< @anchor FMB_LIVELIST_ref. Query all available addresses for active or passive stations. This services uses the data structure
                                          @ref get_ll_cnf "Live List Request" for the confirmation data */
#define FMB_SET_CONFIGURATION     27 /**< set the configuration for the master stack. This services uses the data structure
                                          @ref fmb_set_cfg "FMB Set Configuration". It is available for use in legacy applications
                                          based on the PROFIBUS V5 stack. The current stack has a preconfigured setting that cannot
                                          be changed. All attributes of the services will be accepted but it will have no impact on
                                          current configuration. */


/** @} */

/*****************************************************************************/
/***     FMB data structures                                               ***/
/*****************************************************************************/

/*****************************************************************************/
/* FMB-Configuration-Management                                              */
/*****************************************************************************/

/* VFD-Configuration ------------------------------------------------------- */
typedef struct _T_FMB_CONFIG_VFD
{
  USIGN16     max_no_vfds;                           /* max. number of VFD's */
  USIGN16     max_no_obj_descr;            /* max. number of OD object descr.*/
  USIGN8      max_obj_name_length;            /* max. size of OD object name */
  USIGN8      max_obj_ext_length;        /* max. size of OD object extension */
} T_FMB_CONFIG_VFD;


/* --- CRL-Configuration --------------------------------------------------- */
typedef struct _T_FMB_CONFIG_CRL
{
  USIGN16     max_no_fal_sdbs;                    /* max. number of FAL-SDBs */
  USIGN16     max_no_fdl_sdbs;                    /* max. number of FDL-SDBs */
  USIGN16     max_no_data_buffer;              /* max. number of PDU buffers */
  USIGN16     max_no_api_buffer;               /* max. number of API buffers */
  USIGN16     max_no_poll_entries;       /* max. number of poll list entries */
  USIGN16     max_no_subscr_entries;   /* max. number of subscr.list entries */
  PB_BOOL     resrc_check;                                 /* for future use */
  USIGN8      max_no_parallel_req;                         /* for future use */
  USIGN8      max_no_parallel_ind;                         /* for future use */
  USIGN8      dummy;                                       /* alignment byte */
} T_FMB_CONFIG_CRL;

/* Notes on T_FMB_CONFIG_CRL: ------------------------------------------------

component 'max_no_subscr_entries' is of significance for PROFIBUS-PA only
component 'resrc_check' is introduced for future use - for version 5.10
           set "resrc_check = PB_TRUE;"
component 'max_no_parallel_req' is introduced for future use - for
           version 5.10 set "max_no_parallel_req = 0;"
component 'max_no_parallel_ind' is introduced for future use - for
           version 5.10 set "max_no_parallel_ind = 0;"

end of notes on T_FMB_CONFIG_CRL ------------------------------------------ */


/**
 * @brief DP Configuration
 *
 * The data structure is used to set attributes for the DP/PA master specific configuration before
 * the master goes to online state.
 * @anchor fmb_dp_cfg
 */
typedef struct _T_FMB_CONFIG_DP
{
  USIGN8      max_number_slaves;      /**< maximum number DP Slaves supported */
  USIGN8      max_slave_output_len;   /**< maximum length of slave output data */
  USIGN8      max_slave_input_len;    /**< maximum length of slave input data */
  USIGN8      max_slave_diag_len;     /**< maximum length of one diag entry */
  USIGN16     max_slave_diag_entries; /**< maximum number entries in diag buffer */
  USIGN16     max_bus_para_len;       /**< maximum length of bus parameter set */
  USIGN16     max_slave_para_len;     /**< maximum length of slave parameter set */

} T_FMB_CONFIG_DP;


/* FDLIF-Configuration ----------------------------------------------------- */
typedef struct _T_FMB_CONFIG_FDLIF
{
  USIGN8      send_req_credits;  /* max. number of send credits for SDA and SDN services */
  USIGN8      srd_req_credits;   /* max. number of send credits for SRD services */
  USIGN8      receive_credits;             /* max. number of receive credits */
  USIGN8      max_no_resp_saps;         /* max. number of FDL responder SAPs */
} T_FMB_CONFIG_FDLIF;


/* SM7-Configuration ------------------------------------------------------- */
typedef struct _T_FMB_CONFIG_SM7
{
   USIGN16    reserved;
} T_FMB_CONFIG_SM7;


/**
 * @brief Fieldbus Basic Management Configuration
 *
 * The data structure is used to set attributes for the master configuration before
 * the master goes to online state.
 * @anchor fmb_set_cfg
 */
typedef struct _T_FMB_SET_CONFIGURATION_REQ
{
  PB_BOOL             fms_active;         /**< always set to PB_FALSE because FMS and FM7 services are not supported by mobiLink */
  PB_BOOL             dp_active;          /**< should be PB_TRUE in order to use DP services */
  PB_BOOL             fdlif_active;       /**< always set to PB_FALSE because FDLIF services are not supported by mobiLink */
  PB_BOOL             sm7_active;         /**< always set to PB_FALSE because SM7 services are not supported by mobiLink */
  USIGN16             fdl_evt_receiver;   /**< configures the receiver layer of FDL events: for mobiLink only
                                               FMB_USR and DP_USR can be used */
  USIGN16             data_buffer_length; /**< max. size of PDU (protocol data unit) buffer */
  T_FMB_CONFIG_VFD    vfd;                /**< VFD configuration parameters, not supported by mobiLink  */
  T_FMB_CONFIG_CRL    crl;                /**< CRL configuration parameters, not supported by mobiLink  */
  T_FMB_CONFIG_DP     dp;                 /**< @ref fmb_dp_cfg "DP configuration parameter" */
  T_FMB_CONFIG_FDLIF  fdlif;              /**< FDLIF configuration parameter, not supported by mobiLink */
  T_FMB_CONFIG_SM7    sm7;                /**< SM7 configuration parameters, not supported by mobiLink  */

} T_FMB_SET_CONFIGURATION_REQ;


/*****************************************************************************/
/* FMB-Set-FDL-Busparameter                                                  */
/*****************************************************************************/

/** @name Baudrate
* @anchor fdl_baudrate
* @{ */
#define KBAUD_31_25                0x0A
/** @} */


/** @name Medium Redundancy
* @anchor med_red
* @{ */
#define NO_REDUNDANCY              0x00
/** @} */

/** @name In Ring Desired
* @anchor ird_values
* @{ */
#define IN_RING_DESIRED            PB_TRUE
/** @} */


/**
 * @brief FMB Set Busparameter
 *
 * The data structure is used to set the busparameter used by DP/PA master specific configuration
 * when going online
 * @anchor set_bp_req
 */
typedef struct _T_FMB_SET_BUSPARAMETER_REQ
{
  USIGN8         loc_add;             /**< local node address for master, range 0 - 125 */
  USIGN8         loc_segm;            /**< local segment: 255: no segment: 0 - 63: segment address
                                           mobiLink does not support segment address, so the parameter value has to be 255 */
  USIGN8         baud_rate;           /**< @ref fdl_baudrate "baud rate" -> any value can be used because KBAUD_31_25 is used internally by protocol stack because PA physical layer */
  USIGN8         medium_red;          /**< @ref med_red "medium redundancy": has to be NO_REDUNDANCY because redundancy not supported by mobiLink */
  USIGN16        tsl;                 /**< slot time, range 37 - 16383 */
  USIGN16        min_tsdr;            /**< min. station delay time resp., range 11 - 1023 */
  USIGN16        max_tsdr;            /**< max. station delay time resp., range 37 - 65535 */
  USIGN8         tqui;                /**< quiet time, range 0 - 493 */
  USIGN8         tset;                /**< setup time, range 1 - 494*/
  USIGN32        ttr;                 /**< target token rotation time, range 256 - 16776960 */
  USIGN8         g;                   /**< gap update factor, range 0 - 100. Stack will use highest permitted value if value exceeds the range */
  PB_BOOL        in_ring_desired;     /**< @ref ird_values "In Ring Desired" IN_RING_DESIRED=master or NOT_IN_RING_DESIRED=slave.
                                           mobiLink only supports master role, so the parameter has to be IN_RING_DESIRED */
  USIGN8         hsa;                 /**< highest station address, range 1 - 126. Stack will use highest permitted value if value exceeds the range */
  USIGN8         max_retry_limit;     /**< max. retry limit, range 0 - 7. Stack will use highest permitted value if value exceeds the range */

} T_FMB_SET_BUSPARAMETER_REQ;


/*****************************************************************************/
/* FMB-Read-FDL-Busparameter                                                 */
/*****************************************************************************/

typedef struct _T_FMB_READ_BUSPARAMETER_CNF
{
  USIGN8         loc_add;                                   /* local station */
  USIGN8         loc_segm;                                  /* local segment */
  USIGN8         baud_rate;                                     /* baud rate */
  USIGN8         medium_red;                            /* medium redundancy */
  USIGN16        tsl;                                           /* slot time */
  USIGN16        min_tsdr;                  /* min. station delay time resp. */
  USIGN16        max_tsdr;                  /* max. station delay time resp. */
  USIGN8         tqui;                                         /* quiet time */
  USIGN8         tset;                                         /* setup time */
  USIGN32        ttr;                          /* target token rotation time */
  USIGN8         g;                                     /* gap update factor */
  PB_BOOL        in_ring_desired;               /* active or passive station */
  USIGN8         hsa;                             /* highest station address */
  USIGN8         max_retry_limit;                        /* max. retry limit */
  USIGN16        reserved;                                       /* not used */
  USIGN8         ident[202];                             /* FDL-Ident-String */
} T_FMB_READ_BUSPARAMETER_CNF;


/*****************************************************************************/
/* FMB-Get-Live-List Service                                                 */
/*****************************************************************************/
/*  ----------------------------------------- */
/** @name Status of Stations in Live-List
* @anchor ll_stat
* @{ */

#define PASSIVE               0x00
#define ACTIVE_NOT_READY      0x01
#define ACTIVE_READY          0x02
#define ACTIVE_IN_RING        0x03
/** @} */

/**
 * @brief FMB Live List response
 *
 * Structure to read live list information from the stack
 * @anchor ll_data
 */
typedef struct _T_FMB_LIVE_LIST
{
  USIGN8     station;               /**< station number */
  USIGN8     status;                /**< @ref ll_stat "current status" of station */
} T_FMB_LIVE_LIST;

/**
 * @brief FMB Live List response
 *
 * Structure to return live list information from the stack
 * The @ref ll_data live list information is not part of the data structure but
 * the list is present with the number given by no_of_elements located in the data
 * block directly after T_FMB_GET_LIVE_LIST_CNF.
 * @anchor get_ll_cnf
 */
typedef struct _T_FMB_GET_LIVE_LIST_CNF
{
  USIGN8           dummy;                       /**< alignment */
  USIGN8           no_of_elements;              /**< number of live list elements */
/* T_FMB_LIVE_LIST live_list[no_of_elements];     list of live list elements */
} T_FMB_GET_LIVE_LIST_CNF;


/****************************************************************************/
/* FMB-Event-Management                                                     */
/****************************************************************************/

#define FM2_FAULT_ADDRESS          0x01     /* duplicate address recognized */
#define FM2_FAULT_PHY              0x02     /* phys.layer is malfunctioning */
#define FM2_FAULT_TTO              0x03         /* time out on bus detected */
#define FM2_FAULT_SYN              0x04      /* no receiver synchronization */
#define FM2_FAULT_OUT_OF_RING      0x05              /* station out of ring */
#define FM2_GAP_EVENT              0x06              /* new station in ring */

/* Additional FM2-Events (Error messages from ASPC2) ---------------------- */
#define FM2_MAC_ERROR              0x13                  /* fatal MAC error */
#define FM2_HW_ERROR               0x14                   /* fatal HW error */

typedef struct _T_FMB_FM2_EVENT_IND
{
  USIGN16    reason;                                         /* reason code */
} T_FMB_FM2_EVENT_IND;


#pragma warning (disable : 4103)     /* used #pragma pack to reset alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(pop)
#else
#pragma pack()
#endif
#pragma warning (default : 4103)

#endif  /* __PB_FMB__ */
