/*----------------------------------------------------------------------------------------**/
/*  Filename    : pb_if.h                                                                  */
/*                                                                                         */
/*  Description : This file contains the data types and defines of the PROFIBUS API.       */
/*                Management component of the PROFIBUS stack.                              */
/*                                                                                         */
/*----------------------------------------------------------------------------------------**/

#ifndef __PB_IF__
#define __PB_IF__

#include "pb_type.h"
#include "pb_conf.h"
#include "pb_err.h"
#include "pb_fmb.h"
#include "pb_dp.h"


#pragma warning (disable : 4103)     /* used #pragma pack to change alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(push,2)
#else
#pragma pack(2)
#endif
#pragma warning (default : 4103)


/** @name PROFIBUS service primitives
* @anchor PROFI_svc_primitive
* @{ */
#define REQ                 0x00    /**< Service request primitive */
#define CON                 0x01    /**< Service confirmation primitive */
#define IND                 0x02    /**< Service indication primitive */
#define RES                 0x03    /**< Service response primitive */

/** @} */

/* --- layer identifiers -------------------------------------------------------- */

/** @name PROFIBUS stack layer
* @anchor PROFI_layer
* These layers indicate the PA stack instance by which a service is processed.
* @{ */
#define FMB                 0x05    /**< Fieldbus Management Base FMB layer */
#define FMB_USR             0x08    /**< from FMB layer to user */
#define DP                  0x0B    /**< DP layer */
#define DP_USR              0x0C    /**< from DP layer to user */

#define PAPI                0xF0    /**< layer id of the Application-Layer-IF */

/** @} */


/**
 * @brief PROFIBUS Service Description Block
 *
 * @anchor profi_service_desc
 * PROFIBUS Service Description Block (PROFIBUS SDB)
 * ------------------------------
 *
 */
typedef struct _T_PROFI_SERVICE_DESCR
{
  USIGN16     comm_ref;    /**< communication reference to used connection */
  USIGN8      layer;       /**< layer identifier as listed in  @ref PROFI_layer */
  USIGN8      service;     /**< PROFIBUS service identifier depending on the layer. This either is a @ref DP_svc "DP service"
                                or a @ref FMB_svc "Fieldbus Management service" */
  USIGN8      primitive;   /**< service primitive as listed in @ref PROFI_svc_primitive */
  INT08       invoke_id;   /**< invoke id */
  INT16       result;      /**< service result (POS or NEG) for confirmations/responses */

} T_PROFI_SERVICE_DESCR;




#pragma warning (disable : 4103)    /* used #pragma pack to reset alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(pop)
#else
#pragma pack()
#endif
#pragma warning (default : 4103)


#endif

