/*-----------------------------------------------------------------------------------------*/
/*  Filename    : pb_dp.h                                                                  */
/*                                                                                         */
/*  Description : This file contains defines and data types for the DP component of the    */
/*                PROFIBUS stack.                                                          */
/*                                                                                         */
/*                                                                                         */
/*-----------------------------------------------------------------------------------------*/

#ifndef __PB_DP__
#define __PB_DP__


#pragma warning (disable : 4103)     /* used #pragma pack to change alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(push,1)
#else
#pragma pack(1)
#endif
#pragma warning (default : 4103)



/*--- COMMON DP CONSTANTS --------------------------------------------------*/

#define DP_MAX_NUMBER_STATIONS      127
#define DP_MAX_NUMBER_SLAVES        125

#define DP_GLOBAL_STATION_ADDRESS   127
#define DP_DEFAULT_SLAVE_ADDRESS    126
#define DP_NO_MASTER_ADDRESS        255
#define DP_MAX_SLAVE_ADDRESS        125

#define DP_MAX_TELEGRAM_LEN         244                /* 246 not supported */
#define DP_STATUS_INFO_LEN          128

#define DP_MASTER_USER_DATA_LEN     34
#define DP_MASTER_CLASS2_NAME_LEN   32

#define DP_MIN_SLAVE_DIAG_LEN       6     /* min/max length of service data */
#define DP_MIN_PRM_DATA_LEN         9
#define DP_MIN_CFG_DATA_LEN         3
#define DP_MIN_SLAVE_PARA_LEN       32
#define DP_MIN_BUS_PARA_LEN         66

#define DP_MAX_SLAVE_DIAG_DATA_LEN  244   /* SLAVE- != MASTER- MAX_DIAG_LEN */
#define DP_MAX_MASTER_DIAG_DATA_LEN 242
#define DP_MAX_EXT_DIAG_DATA_LEN    238
#define DP_MAX_OUTPUT_DATA_LEN      244
#define DP_MAX_INPUT_DATA_LEN       244
#define DP_MAX_PRM_DATA_LEN         244
#define DP_MAX_CFG_DATA_LEN         244
#define DP_MAX_AAT_DATA_LEN         492
#define DP_MAX_USER_PRM_DATA_LEN    234
#define DP_MAX_DOWNLOAD_DATA_LEN    240
#define DP_MAX_UPLOAD_DATA_LEN      240
#define DP_MAX_REM_SLAVE_DATA_LEN   238
#define DP_MAX_SLAVE_USER_DATA_LEN  65470
#define DP_MAX_MASTER_USER_DATA_LEN 65470
#define DP_MAX_SET_BUSPARAMETER_LEN 196
#define DP_MAX_MSAC2_DATA_LEN       220

/*--------------------------------------------------------------------------*/
/* DP USER SERVICE CODES                                                    */
/*--------------------------------------------------------------------------*/

#define DP_SERVICE_USIF             0x80
#define DP_SERVICE_DDLM             0x00

#define DP_SERVICE_SCHED            (DP_SERVICE_USIF | 0x40)

#define DP_SERVICE_MSAC2            (DP_SERVICE_DDLM | 0x40)


/*--- DP SERVICE HANDLER SERVICES ------------------------------------------*/

/** @name DP service identifier
 *
 * This contains the list of all available DP services.
 * @anchor DP_svc
 * @{ */

/*--- DP SCHEDULER SERVICES ------------------------------------------------*/

#define DP_INIT_MASTER              (DP_SERVICE_SCHED | 0x01) /**< DP service to initialize the PROFIBUS DP/PA master stack.
                                                                   The service request uses the data structure @ref dp_init_master_req "DP/PA Master Initialization".
                                                                   Confirmation data uses data structure @ref dp_init_master_con "Init Confirmation" */

#define DP_ACT_PARAM_LOC            (DP_SERVICE_SCHED | 0x02) /**< DP service to set the operation mode of the DP/PA master.
                                                                   This service uses the data structure @ref dp_act_param_req "T_DP_ACT_PARAM_REQ" for service requests and
                                                                   @ref dp_act_param_con "T_DP_ACT_PARAM_RES_CON" for confirmation */
/*--- DP DDLM SERVICES -----------------------------------------------------*/

#define DP_GET_CFG                  (DP_SERVICE_DDLM | 0x02) /**< @anchor DP_GET_CFG_ref. Service to read the current DP configuration of a PA slave.
                                                                  This service uses the data structure @ref dp_get_cfg_req "T_DP_GET_CFG_REQ" for service requests and
                                                                  @ref dp_get_cfg_con "T_DP_GET_CFG_CON" for confirmation */
#define DP_SLAVE_DIAG               (DP_SERVICE_DDLM | 0x03) /**< @anchor DP_SLAVE_DIAG_ref. Service to query for ident number and diagnostic information from a PA slave device
                                                                  This service uses the data structure @ref slave_diag_req "T_DP_SLAVE_DIAG_REQ" for service requests and
                                                                  @ref slave_diag_con "T_DP_SLAVE_DIAG_CON" for confirmation */
#define DP_RD_INP                   (DP_SERVICE_DDLM | 0x04) /**< @anchor DP_RD_INP_ref. Read input I/O data of a PA slave. The slave has to be in data exchange mode.
                                                                  This service uses the data structure @ref dp_rd_inp_req "T_DP_RD_INP_REQ" for service requests and
                                                                  @ref dp_rd_inp_con "T_DP_RD_INP_CON" for confirmation */
#define DP_RD_OUTP                  (DP_SERVICE_DDLM | 0x05) /**< @anchor DP_RD_OUTP_ref. Read output I/O data of a PA slave. The slave has to be in data exchange mode.
                                                                  This service uses the data structure @ref dp_rd_outp_req "T_DP_RD_OUTP_REQ" for service requests and
                                                                  @ref dp_rd_outp_con "T_DP_RD_OUTP_CON" for confirmation */
#define DP_SET_SLAVE_ADD            (DP_SERVICE_DDLM | 0x07) /**< @anchor DP_SET_SLAVE_ADD_ref. Service to set the node address of a PA slave.
                                                                  This service uses the data structure @ref dp_set_slave_addr_req "T_DP_SET_SLAVE_ADD_REQ" for service requests and
                                                                  @ref dp_set_slave_addr_con "T_DP_SET_SLAVE_ADD_CON" for confirmation */

#define DP_FAULT                    (DP_SERVICE_DDLM | 0x10) /**< service indication to report an error */

/*--- DP DDLM SERVICE FOR DP/V1 --------------------------------------------*/

/** @brief MSAC2 services / connection establishment
 *
 * @anchor DP_INITIATE_ref
 * DP service to initiate an MSAC2 connection (MSAC2: Master-Slave-Acyclic-Class2)to a slave.
 * The implementation supports several (DP_MAX_CHANNELS_MSAC2) parallel connections. On each connection,
 * there is only one service allowed at one time, except the DP_ABORT service, which is allowed at any time.
 * This service uses the data structure @ref dp_initiate_req "T_DP_INITIATE_REQ" for service requests and
 * @ref dp_initiate_con "T_DP_INITIATE_CON" for confirmation
 *
 * <B>Successful connection establishment</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is closed" ];
 * "Application"      => "commModule Stack"       [ label = "DP_INITIATE", URL="@ref dp_initiate_req" ];
 * "commModule Stack" :> "PA slave"             [ label = "Request to PA slave" ];
 * "PA slave"         :> "commModule Stack"       [ label = "Positive Resource Management response from PA slave" ];
 * "commModule Stack" :> "PA slave"             [ label = "Poll request PA slave" ];
 * "PA slave"         :> "commModule Stack"       [ label = "Positive Initiate response from PA slave" ];
 * ---  [ label = "MSAC2 connection is opened" ];
 * "commModule Stack" >> "Application"          [ label = "positive Initiate con" ];
 * "commModule Stack" >> "commModule"             [ label = "Timer expires if no services are used on MSAC2 connection" ];
 * "commModule Stack" :> "PA slave"             [ label = "Idle message" ];
 * "PA slave"         :> "commModule Stack"       [ label = "Acknowledge" ];
 * @endmsc
 *
 * <B>Connection establishment fails due to not supported Initiate parameter</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is closed" ];
 * "Application"      => "commModule Stack"       [ label = "DP_INITIATE", URL="@ref dp_initiate_req" ];
 * "commModule Stack" :> "PA slave"             [ label = "Request to PA slave" ];
 * "PA slave"         :> "commModule Stack"       [ label = "Negative response from PA slave e.g. send_timeout toö small" ];
 * "commModule Stack" >> "Application"          [ label = "Abort indication" ];
 * @endmsc
 *
 * <B>Connection establishment fails, PA slave not present</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is closed" ];
 * "Application"        => "commModule Stack"       [ label = "DP_INITIATE", URL="@ref dp_initiate_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Request to PA slave which is not present" ];
 * "commModule Stack"   :> "commModule Stack"       [ label = "Timeout" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Abort request" ];
 * "commModule Stack"   >> "Application"          [ label = "Abort indication" ];
 * @endmsc
 *
 */
#define DP_INITIATE                 (DP_SERVICE_MSAC2 | 0x00)

/** @brief MSAC2 services / connection abortion
 *
 * @anchor DP_ABORT_ref
 * DP service to abort an acyclic class 2 connection.
 * This service uses the data structure @ref dp_abort_req "T_DP_ABORT_REQ" for service requests and
 * @ref dp_abort_ind "T_DP_ABORT_IND" for indications from the stack
 *
 * <B>Successful connection abortion</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"        => "commModule Stack"     [ label = "DP_ABORT", URL="@ref dp_abort_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Abort request to PA slave" ];
 * ---  [ label = "MSAC2 connection is closed" ];
 * "commModule Stack"   >> "Application"          [ label = "Abort indication" ];
 * @endmsc
 *
 * <B>Connection abortion for closed connection</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is closed" ];
 * "Application"      => "commModule Stack"     [ label = "DP_ABORT", URL="@ref dp_abort_req" ];
 * "commModule Stack" >> "Application"          [ label = "Interface error for fbapi_snd_req_res(): E_IF_STATE_CONFLICT" ];
 * @endmsc
 *
 */
#define DP_ABORT                    (DP_SERVICE_MSAC2 | 0x01)

/** @brief MSAC2 service: Read access to PA slave parameter
 *
 * @anchor DP_READ_ref
 * DP service to read parameter values via an opened class 2 connection.
 * This service uses the data structure @ref dp_read_req "T_DP_READ_REQ" for service requests and
 * @ref dp_read_con "T_DP_READ_CON" for confirmation
 *
 * <B>Read access to PA slave</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"      => "commModule Stack"       [ label = "DP_READ", URL="@ref dp_read_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Parameter exists: Positive read response with parameter values" ];
 * "commModule Stack"   >> "Application"          [ label = "Positive read confirmation" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Parameter does not exist: Negative read response with parameter values" ];
 * "commModule Stack"   >> "Application"          [ label = "Negative read confirmation" ];
 * @endmsc
 *
 * <B>Read access via closed connection</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is closed" ];
 * "Application"      => "commModule Stack"       [ label = "DP_READ", URL="@ref dp_read_req" ];
 * "commModule Stack"   >> "Application"          [ label = "Interface error for fbapi_snd_req_res(): E_IF_STATE_CONFLICT" ];
 * @endmsc
 *
 * <B>Read access to not responding PA slave</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"      => "commModule Stack"       [ label = "DP_READ", URL="@ref dp_read_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "No response" ];
 * "commModule Stack"   :> "commModule Stack"       [ label = "Timeout" ];
 * ---  [ label = "MSAC2 connection is closed" ];
 * "commModule Stack"   >> "Application"          [ label = "Negative read confirmation, status = E_DP_NA" ];
 * "commModule Stack"   >> "Application"          [ label = "Abort indication" ];
 * @endmsc
 *
 */
#define DP_READ                     (DP_SERVICE_MSAC2 | 0x02)

/** @brief MSAC2 service: Write access to PA slave parameter
 *
 * @anchor DP_WRITE_ref
 * DP service to write parameter values via an opened class 2 connection.
 * This service uses the data structure @ref dp_write_req "T_DP_WRITE_REQ" for service requests and
 * @ref dp_write_con "T_DP_WRITE_CON" for confirmation
 *
 * <B>Write access to PA slave</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"      => "commModule Stack"       [ label = "DP_WRITE", URL="@ref dp_write_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Parameter exists: Positive write response with parameter values" ];
 * "commModule Stack"   >> "Application"          [ label = "Positive write confirmation" ];
 * "commModule Stack"   :> "PA slave"             [ label = "Parameter does not exist: Negative write response with parameter values" ];
 * "commModule Stack"   >> "Application"          [ label = "Negative read confirmation" ];
 * @endmsc
 *
 * <B>Write access via closed connection</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"      => "commModule Stack"       [ label = "DP_WRITE", URL="@ref dp_write_req" ];
 * "commModule Stack" >> "Application"          [ label = "Interface error for fbapi_snd_req_res(): E_IF_STATE_CONFLICT" ];
 * @endmsc
 *
 * <B>Write access to not responding PA slave</B>
 *
 * @msc
 * hscale = "2";
 * "Application", "commModule Stack", "PA slave";
 *
 * ---  [ label = "MSAC2 connection is opened" ];
 * "Application"        => "commModule Stack"     [ label = "DP_WRITE", URL="@ref dp_write_req" ];
 * "commModule Stack"   :> "PA slave"             [ label = "No response" ];
 * "commModule Stack"   :> "commModule Stack"     [ label = "Timeout" ];
 * ---  [ label = "MSAC2 connection is closed" ];
 * "commModule Stack"   >> "Application"          [ label = "Negative write confirmation, status = E_DP_NA" ];
 * "commModule Stack"   >> "Application"          [ label = "Abort indication" ];
 * @endmsc
 *
 */
#define DP_WRITE                    (DP_SERVICE_MSAC2 | 0x03)

/** @} */


/*--------------------------------------------------------------------------*/
/* DP SERVICE CONSTANTS                                                     */
/*--------------------------------------------------------------------------*/

/** @name MASTER AUTOMATIC REMOTE SERVICES
 *
 * @anchor MASTER_auto_svc
 * @{ */
#define DP_AUTO_GET_MASTER_DIAG     0x80
#define DP_AUTO_UPLOAD_DOWNLOAD_SEQ 0x40
#define DP_AUTO_ACT_PARAM           0x20

#define DP_AUTO_REMOTE_SERVICES     ( DP_AUTO_GET_MASTER_DIAG     |          \
                                      DP_AUTO_UPLOAD_DOWNLOAD_SEQ |          \
                                      DP_AUTO_ACT_PARAM )

#define DP_USER_REMOTE_SERVICES     0x00
/** @} */

/** @name SLAVE DPRAM ADDRESS ASSIGNMENT MODES
 * @anchor slave_dpram_modes
 * @{ */
#define DP_AAM_ARRAY                0x00
#define DP_AAM_DEFINED              0x01
#define DP_AAM_COMPACT              0x02
#define DP_AAM_IO_BLOCKS            0x03
/** @} */

/** @name DP Slave flags
 *
 * @anchor DP_slave_flags
 * @{ */
#define DP_SL_ACTIVE                0x80
#define DP_SL_NEW_PRM               0x40
#define DP_SL_FAIL_SAFE             0x20

#define DP_SL_FLAGS                 ( DP_SL_ACTIVE    |                      \
                                      DP_SL_NEW_PRM   |                      \
                                      DP_SL_FAIL_SAFE )
/** @} */

/** @name DP AREA CODES
 *
 * @anchor DP_area_codes
 * @{ */
#define DP_AREA_SLAVE_PARAM         0x00
#define DP_AREA_BUS_PARAM           0x7F
#define DP_AREA_SET_MODE            0x80
#define DP_AREA_STAT_COUNT          0x81
#define DP_AREA_NO_PROTECTION       0xFF

/** @} */


/** @name DP AREA / ACTIVATION CODES
 *
 * @anchor DP_act_codes
 * @{ */
#define DP_SLAVE_ACTIVATE           0x80  /**< values are area code dependent */
#define DP_SLAVE_DEACTIVATE         0x00

#define DP_BUS_PAR_ACTIVATE         0xFF

#define DP_OP_MODE_OFFLINE          0x00 /**< target mode for master is offline */
#define DP_OP_MODE_STOP             0x40 /**< target mode for master is online  */
#define DP_OP_MODE_CLEAR            0x80
#define DP_OP_MODE_RED_CLEAR        0x81            /* new redundancy state */
#define DP_OP_MODE_OPERATE          0xC0
#define DP_OP_MODE_RED_OPERATE      0xC1            /* new redundancy state */

/** @} */

/*--- DP MASTER DIAG IDENTIFIERS -------------------------------------------*/

#define DP_DIAG_SLAVE_DATA          0x00
#define DP_DIAG_SYSTEM_DIAGNOSTIC   0x7E
#define DP_DIAG_MASTER_STATUS       0x7F
#define DP_DIAG_DATA_TRANSFER_LIST  0x80

/*--- DP SLAVE PARAM IDENTIFIERS -------------------------------------------*/

#define DP_SLAVE_PARAM_HEADER       0x01
#define DP_SLAVE_PARAM_PRM_DATA     0x02
#define DP_SLAVE_PARAM_CFG_DATA     0x03
#define DP_SLAVE_PARAM_AAT_DATA     0x04
#define DP_SLAVE_PARAM_USER_DATA    0x05
#define DP_SLAVE_PARAM_SLAVE_INFO   0x06
#define DP_SLAVE_PARAM_SYS_INFO     0x07
#define DP_SLAVE_PARAM_FLUSH_DIAG   0x08

/*--- DP SET MASTER PARAM IDENTIFIERS --------------------------------------*/

#define DP_SET_IDENT_NUMBER         0x00

/*--- DP SLAVE DIAG BITS ---------------------------------------------------*/

#define DP_DIAG_1_MASTER_LOCK           0x80     /* influenced by DP Master */
#define DP_DIAG_1_PRM_FAULT             0x40
#define DP_DIAG_1_INVALID_SLAVE_RES     0x20     /* influenced by DP Master */
#define DP_DIAG_1_NOT_SUPPORTED         0x10
#define DP_DIAG_1_EXT_DIAG              0x08
#define DP_DIAG_1_CFG_FAULT             0x04
#define DP_DIAG_1_STATION_NOT_READY     0x02
#define DP_DIAG_1_STATION_NON_EXISTENT  0x01     /* influenced by DP Master */

#define DP_DIAG_1_STATUS                0xFF

#define DP_DIAG_2_DEACTIVATED           0x80     /* influenced by DP Master */
#define DP_DIAG_2_SYNC_MODE             0x20
#define DP_DIAG_2_FREEZE_MODE           0x10
#define DP_DIAG_2_WD_ON                 0x08
#define DP_DIAG_2_DEFAULT               0x04
#define DP_DIAG_2_STAT_DIAG             0x02
#define DP_DIAG_2_PRM_REQ               0x01

#define DP_DIAG_2_STATUS                ( DP_DIAG_2_DEACTIVATED |            \
                                          DP_DIAG_2_SYNC_MODE   |            \
                                          DP_DIAG_2_FREEZE_MODE |            \
                                          DP_DIAG_2_WD_ON       |            \
                                          DP_DIAG_2_PRM_REQ )

#define DP_DIAG_3_EXT_DIAG_OVERFLOW     0x80

#define DP_DIAG_3_STATUS                ( DP_DIAG_3_EXT_DIAG_OVERFLOW )

#define DP_SLAVE_DIAG_OVERFLOW          (-1)
#define DP_STATION_NON_EXISTENT         0x0100
#define DP_INVALID_SLAVE_RESPONSE       0x2000

/*--- DP STATION STATUS ----------------------------------------------------*/

#define DP_STATE_STATION_TYPE           0x80         /* station status bits */

#define DP_STATE_MASTER_STATION         0x80
#define DP_STATE_SLAVE_STATION          0x00

#define DP_STATE_SLAVE_DIAG_DATA        0x02
#define DP_STATE_MASTER_DIAG_DATA       0x02

#define DP_STATE_SLAVE_ERROR            0x01
#define DP_STATE_MASTER_UP_DOWN_LOAD    0x01

/*--------------------------------------------------------------------------*/
/* DP DATA STRUCTURE DEFINITIONS                                            */
/*--------------------------------------------------------------------------*/
/* PCI: Protocol Control Information                                        */
/* SDU: Service Data Unit                                                   */
/* PDU: Protocol Data Unit = PCI + SDU                                      */
/*                                                                          */
/* !!! DATA STRUCTURES: MOTOROLA BYTE ORDERING (HIGH, LOW) !!!              */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/

typedef struct _T_DP_CFG_DATA
{
   USIGN16    cfg_data_len;                                       /* 2..244 */
/* OCTET      cfg_data [cfg_data_len - 2];                                  */

} T_DP_CFG_DATA;


typedef struct _T_DP_DIAG_DATA
{
   OCTET      station_status_1;                            /* DP_DIAG_1_xxx */
   OCTET      station_status_2;                            /* DP_DIAG_2_xxx */
   OCTET      station_status_3;                            /* DP_DIAG_3_xxx */
   USIGN8     master_add;     /* DP Master which parameterized the DP Slave */
   USIGN16    ident_number;             /* PNO ident number of the DP Slave */
/* OCTET      ext_diag_data [DP_MAX_EXT_DIAG_DATA_LEN];                     */

} T_DP_DIAG_DATA;


/*--------------------------------------------------------------------------*/
/*--- DP USIF SERVICE STRUCTURES -------------------------------------------*/
/*--------------------------------------------------------------------------*/

/**
 * @brief PA Master Initialization
 *
 * The data structure contains the parameter that has to be given for initialization
 * of the PA class 2 master.
 * @anchor dp_init_master_req
 */
typedef struct _T_DP_INIT_MASTER_REQ
{
  USIGN8       master_default_address;    /**< default station address of master. Permitted values 0 - 125 */
  PB_BOOL      master_class2;             /**< support M2 requester functionality i.e. use SAP 54 as requestor -> should always be set to PB_TRUE
                                               because commModule provides only class 2 master functinoality */
  PB_STRINGV   master_class2_name [32];   /**< vendor name of class 2 master */

  USIGN8       lowest_slave_address;      /**< station address of first DP Slave 0 - DP_MAX_SLAVE_ADDRESS */

  USIGN8       slave_io_address_mode;     /**< @ref slave_dpram_modes "Address Assignment Mode of slave I/O" */

  PB_BOOL      clear_outputs;             /**< clear output areas in DP_OP_MODE_CLEAR or not, not relevant because no cyclic data transfer */
  USIGN8       auto_remote_services;      /**< @ref MASTER_auto_svc "Automatic remote services": Bit mask to determine which M1 responses to
                                               M2 requests should be acknowledged automatically by the stack */
  PB_BOOL      cyclic_data_transfer;      /**< DATA_TRANSFER service request supported when in CLEAR or OPERATE state: use PB_FALSE because this is not supported */

  USIGN8       dummy;                     /**< alignment byte for word alignment */

} T_DP_INIT_MASTER_REQ;

/**
 * @brief PA Master Initialization
 *
 * The data structure is returned in the confirmation
 * @anchor dp_init_master_con
 */
typedef struct _T_DP_INIT_MASTER_CON
{
  USIGN16   status;       /**< possible @ref DP_err_codes "status values": E_DP_OK, E_DP_IV, E_DP_NO */

} T_DP_INIT_MASTER_CON;




typedef struct _T_DP_EXIT_MASTER_CON
{
  USIGN16   status;          /**< possible @ref DP_err_codes "status values": E_DP_OK, E_DP_NO */

} T_DP_EXIT_MASTER_CON;

/**
 * @brief DP ACTIVATE
 *
 * The data structure contains the parameter that has to be given for DP_ACT_PARAM_LOC service
 * @anchor dp_act_param_req
 */
typedef struct _T_DP_ACT_PARAM_REQ
{
  USIGN8    rem_add;    /**< station address DP Master (class 1) */
  USIGN8    area_code;  /**< @ref DP_area_codes "DP AREA codes": user DP_AREA_SET_MODE to set the mode via activate parameter */
  USIGN8    activate;   /**< @ref DP_act_codes "Activation codes". user DP_OP_MODE_STOP to go online after initialization */
  USIGN8    dummy;      /**< alignment byte */

} T_DP_ACT_PARAM_REQ;

/**
 * @brief DP ACTIVATE
 *
 * The data structure contains the parameter that has to be given for DP_ACT_PARAM_LOC service
 * @anchor dp_act_param_con
 */
typedef struct _T_DP_ACT_PARAM_RES_CON
{
  USIGN16   status;              /**< possible @ref DP_err_codes "status values": E_DP_OK, E_DP_DS, E_DP_NA, E_DP_RS, E_DP_RR,
                                      E_DP_UE, E_DP_TO, E_DP_FE, E_DP_RE, E_DP_NE, E_DP_AD, E_DP_IP, E_DP_SC,
                                      E_DP_NI, E_DP_DI, E_DP_EA, E_DP_LE */

} T_DP_ACT_PARAM_RES_CON;

/*--------------------------------------------------------------------------*/
/*--- DP DDLM SERVICE STRUCTURES -------------------------------------------*/
/*--------------------------------------------------------------------------*/

/**
 * @brief DP SLAVE_DIAG_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor slave_diag_req
 */
typedef struct _T_DP_SLAVE_DIAG_REQ
{
  USIGN8    rem_add;                     /**< node address of slave to be queried: Range = 0..126 */
  USIGN8    dummy;                       /**< alignment byte */

} T_DP_SLAVE_DIAG_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP SLAVE_DIAG_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor slave_diag_con
 */
typedef struct _T_DP_SLAVE_DIAG_CON
{
   USIGN16   status;                       /**< Confirmation status: OK, DS, NA, RS, UE, NR, RE */
   USIGN8    rem_add;                      /**< node address of responding: Range = 0..126 */
   USIGN8    dummy;                        /**< alignment byte */
   USIGN16   diag_data_len;                /**< number of diagnostic data: Range 0..DP_MAX_SLAVE_DIAG_DATA_LEN */
/* OCTET     diag_data [diag_data_len];                                     */

} T_DP_SLAVE_DIAG_CON;

/*==========================================================================*/

/**
 * @brief DP READ_INP_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_rd_inp_req
 */
typedef struct _T_DP_RD_INP_REQ
{
  USIGN8    rem_add;              /**< node address of slave to be queried: Range = 0...126 */
  USIGN8    dummy;                /**< alignment byte */

} T_DP_RD_INP_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP READ_INP_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_rd_inp_con
 */
typedef struct _T_DP_RD_INP_CON
{
   USIGN16   status;                   /**< OK, DS, NA, RS, UE, NR, RE */
   USIGN8    rem_add;                  /**< node address of slave to be queried: Range = 0...126 */
   USIGN8    dummy;                    /**< alignment byte */
   USIGN16   inp_data_len;             /**< length of input data that has been read 0..DP_MAX_INPUT_DATA_LEN */
/* OCTET     inp_data [inp_data_len];                                       */

} T_DP_RD_INP_CON;

/**
 * @brief DP READ_OUTP_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_rd_outp_req
 */
typedef struct _T_DP_RD_OUTP_REQ
{
  USIGN8    rem_add;              /**< node address of slave to be queried: Range = 0...126 */
  USIGN8    dummy;                /**< alignment byte */

} T_DP_RD_OUTP_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP READ_OUTP_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_rd_outp_con
 */
typedef struct _T_DP_RD_OUTP_CON
{
   USIGN16   status;                   /**< OK, DS, NA, RS, UE, NR, RE */
   USIGN8    rem_add;                  /**< node address of slave to be queried: Range = 0...126 */
   USIGN8    dummy;                    /**< alignment byte */
   USIGN16   outp_data_len;            /**< length of input data that has been read 0..DP_MAX_INPUT_DATA_LEN */
/* OCTET     outp_data [outp_data_len];                                     */

} T_DP_RD_OUTP_CON;

/**
 * @brief DP GET_CFG
 *
 * The data structure contains the parameter that has to be given for DP_GET_CFG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_get_cfg_req
 */
typedef struct _T_DP_GET_CFG_REQ
{
  USIGN8    rem_add;                     /**< node adress of the slave: 0..126 */
  USIGN8    dummy;                       /**< alignment byte */

} T_DP_GET_CFG_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP GET_CON
 *
 * The data structure contains information that is returned in a DP_GET_CFG confirmation.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_get_cfg_con
 */
typedef struct _T_DP_GET_CFG_CON
{
  USIGN16         status;                     /**< OK, DS, NA, RS, UE, NR, RE */
  USIGN8          rem_add;                    /**< Node address of the responding slave, range is 0..126 */
  USIGN8          dummy;                      /**< alignment byte */
  T_DP_CFG_DATA   real_cfg_data;              /**< length of data and slave configuration data */

} T_DP_GET_CFG_CON;

/*==========================================================================*/

/**
 * @brief DP SET_SLAVE_ADD_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_set_slave_addr_req
 */
typedef struct _T_DP_SET_SLAVE_ADD_REQ
{
   USIGN8    rem_add;                /**< node address of slave which shall be set to new address: Range = 0...126 */
   USIGN8    dummy;                  /**< alignment byte */
   USIGN16   rem_slave_data_len;     /**< Length of slave data, passed at the end of the data structure. Range: 0..DP_MAX_REM_SLAVE_DATA_LEN */
   USIGN8    new_slave_add;          /**< Slave address to be assigned. Range is 0..125 */
   USIGN8    ident_number_high;      /**< High byte of slave ident number */
   USIGN8    ident_number_low;       /**< Low byte of slave ident number */
   PB_BOOL   no_add_chg;             /**< DP_TRUE: address change after reset only
                                          DP_FALSE: immediately change node address */
/* OCTET     rem_slave_data [rem_slave_data_len];                           */

} T_DP_SET_SLAVE_ADD_REQ;

/*--------------------------------------------------------------------------*/


/**
 * @brief DP SET_SLAVE_ADD_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_set_slave_addr_con
 */
typedef struct _T_DP_SET_SLAVE_ADD_CON
{
  USIGN16   status;                 /**< response status: OK, DS, NA, RS, RR, UE, RE */
  USIGN8    rem_add;                /**< Node address of the responding slave, range is 0...126 */
  USIGN8    dummy;                  /**< alignment byte */

} T_DP_SET_SLAVE_ADD_CON;

typedef struct _T_DP_FM2_EVENT_IND
{
  USIGN16   reason;                /**< FM2 reason code */

} T_DP_FM2_EVENT_IND;

/*--------------------------------------------------------------------------*/
/*--- DPV1 DATA STRUCTURES -------------------------------------------------*/
/*--------------------------------------------------------------------------*/


/**
 * Additional address information for DP_INITIATE request.
 • Data Transfer from one DP-Network to another DP-Network over a link
 • Data Transfer from an alien network to DP-Network over a gatewaytypedef struct _T_ADD_ADDR
 */
typedef struct _T_ADDR
{
   USIGN8   api;                    /**< application process instance, default: set to 0 */
   USIGN8   scl;                    /**< security level, default: set to 0 */
/* OCTET    network_address[6];   *//**< network identification, use only when [d|s]_type is set to 1 */
/* OCTET    mac_address[x_len-8]; *//**< MAC address, use only when [d|s]_type is set to 1 */

} T_ADDR;

/*--------------------------------------------------------------------------*/


/**
 * The DP_INITIATE service offers the possibility to communicate over multiple interconnected networks.
 • Data Transfer from one DP-Network to another DP-Network over a link
 • Data Transfer from an alien network to DP-Network over a gatewaytypedef struct _T_ADD_ADDR
 */
typedef struct _T_ADD_ADDR
{
   USIGN8   s_type;                  /**< indicates the presence of the optional network source address, default: set to 0 */
   USIGN8   s_len;                   /**< indicates the length of the s_addr parameter, default: set to 2 */
   USIGN8   d_type;                  /**< indicates the presence of the optional network destination, default: set to 0 */
   USIGN8   d_len;                   /**< indicates the length of the s_addr parameter, default: set to 2 */
/* USIGN8   s_addr[s_len]; */        /**< additional source address information using T_ADDR */
/* USIGN8   d_addr[d_len]; */        /**< additional destination address information using T_ADDR */

} T_ADD_ADDR;


/*--------------------------------------------------------------------------*/


/** \addtogroup DP_CTXT
 *  @{
 */

/**
 * @brief DP INITIATE_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_initiate_req
 */
typedef struct _T_DP_INITIATE_REQ
{
  USIGN8        rem_add;                          /**< node address of slave, range 0..126 */
  USIGN8        reserved [3];                     /**< alignment bytes */
  USIGN16       send_timeout;                     /**< control time for supervision. Unit: 10 ms, range = 1 - 0xFFFFu */
  OCTET         features_supported [2];           /**< supported features of the master: Set to 0x01, 0x00 */
  OCTET         profile_features_supported [2];   /**< supported features regarding used profiles. Set to 0x00, 0x00 */
  USIGN16       profile_ident_number;             /**< profile ident number */
  T_ADD_ADDR    add_addr_param;                   /**< additional address information */

} T_DP_INITIATE_REQ;

/*--------------------------------------------------------------------------*/

#define DP_INITIATE_S_ADDR(x)     ((T_ADDR *)(((USIGN8 *)&((x)->add_addr_param))+sizeof(T_ADD_ADDR)))
#define DP_INITIATE_D_ADDR(x)     ((T_ADDR *)(((USIGN8 *)&((x)->add_addr_param))+sizeof(T_ADD_ADDR)+(x)->add_addr_param.s_len))

/*--------------------------------------------------------------------------*/

/**
 * @brief DP INITIATE_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_initiate_con
 */
typedef struct _T_DP_INITIATE_CON
{
  USIGN16       status;                            /**< response status: OK */
  USIGN8        rem_add;                           /**< adderess of responding slave, range is 0..126 */
  USIGN8        max_len_data_unit;                 /**< maximum data-length, the slave can provide, 1...DP_MSAC2_DATA_LEN */
  OCTET         features_supported [2];            /**< supported features of the slave, should be 0x01, 0x00 */
  OCTET         profile_features_supported [2];    /**< supported features regarding used profiles */
  USIGN16       profile_ident_number;              /**< profile ident number */
  T_ADD_ADDR    add_addr_param;                    /**< additional address information */

} T_DP_INITIATE_CON;

/** @}*/

/*--------------------------------------------------------------------------*/

/**
 * @brief DP READ_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_read_req
 */

typedef struct _T_DP_READ_REQ
{
  USIGN8    rem_add;               /**< remote address of slave, not used if class 2 connection is  used. Range is 0...126 */
  USIGN8    slot_number;           /**< Slot number of parameter to be read. Range is 0..254 */
  USIGN8    index;                 /**< Index within slot of parameter to be read. Range is 0..254 */
  USIGN8    length;                /**< data length (0..DP_MSAC2_DATA_LEN), restricted by the results of the initiate confirmation */

} T_DP_READ_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP READ_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_read_con
 */typedef struct _T_DP_READ_CON
{
   USIGN16    status;               /**< response status, E_DP_OK */
   USIGN8     rem_add;              /**< remote address of the slave, range is 0..126 */
   USIGN8     slot_number;          /**< Slot number of parameter to be read. Range is 0..254 */
   USIGN8     index;                /**< Index within slot of parameter to be read. Range is 0..254 */
   USIGN8     length;               /**< length of data from slave */
/* OCTET      data [length]*/       /**< parameter data */

} T_DP_READ_CON;

/*--------------------------------------------------------------------------*/


/**
 * @brief DP WRITE_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_write_req
 */
typedef struct _T_DP_WRITE_REQ
{
   USIGN8   rem_add;                /**< remote address of slave, not used if class 2 connection is  used. Range is 0...126 */
   USIGN8   slot_number;            /**< Slot number of parameter to be written. Range is 0..254 */
   USIGN8   index;                  /**< Index within slot of parameter to be written. Range is 0..254 */
   USIGN8   length;                 /**< data length (0..DP_MSAC2_DATA_LEN) to be written */
/* OCTET    data [length]*/         /**< data to be written to parameter */

} T_DP_WRITE_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP WRITE_CON
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_write_con
 */
typedef struct _T_DP_WRITE_CON
{
  USIGN16   status;                 /**< response status, E_DP_OK */
  USIGN8    rem_add;                /**< remote address of the slave, range is 0..126 */
  USIGN8    slot_number;            /**< Slot number of parameter to be written. Range is 0..254 */
  USIGN8    index;                  /**< Index within slot of parameter to be written. Range is 0..254 */
  USIGN8    length;                 /**< Length of data that have been written */

} T_DP_WRITE_CON;

/*--------------------------------------------------------------------------*/

/** \addtogroup DP_CTXT DP context service
 *  @{
 */
/**
 * @brief DP ABORT_REQ
 *
 * The data structure contains the parameter that has to be given for DP_SLAVE_DIAG service request.
 * It defines the node address of the slave to which the DP_GET_CFG service request is sent
 * @anchor dp_abort_req
 */
typedef struct _T_DP_ABORT_REQ
{
  USIGN8    subnet;                      /**< subnet location. Permitted values: 0 (NO), 1 (SUBNET_LOCAL), 2 (SUBNET_REMOTE)*/
  USIGN8    reason;                      /**< user abort reason code: 0..0x3F */

} T_DP_ABORT_REQ;

/*--------------------------------------------------------------------------*/

/**
 * @brief DP ABORT_IND
 *
 * The data structure contains the data sent as confirmation to a DP_SLAVE_DIAG service.
 * @anchor dp_abort_ind
 */
typedef struct _T_DP_ABORT_IND
{
  PB_BOOL   locally_generated;       /**< abort created by local stack or by remote slave device */
  USIGN8    subnet;                  /**< subnet location. Possible values: 0 (NO), 1 (SUBNET_LOCAL), 2 (SUBNET_REMOTE)*/
  USIGN8    reason;                  /**< procotol instance and abort reason codes */
  USIGN8    dummy;                   /**< alignment byte */
  USIGN16   additional_detail;       /**< Additional detail */

} T_DP_ABORT_IND;

/** @}*/

/*--------------------------------------------------------------------------*/

/**
 * @brief DP ERROR_CON
 *
 * The data structure contains error description in case of a negative confirmation for a DP service.
 * @anchor dp_error_con
 */
typedef struct _T_DP_ERROR_CON
{
  USIGN16   status;             /**< confirmation status code = error code */
  USIGN8    rem_add;            /**< remote station address */
  USIGN8    error_decode;       /**< @ref DP_err_profile_codes "profile identifier code" */
  USIGN8    error_code_1;       /**< error code */
  USIGN8    error_code_2;       /**< error code */

} T_DP_ERROR_CON;


#pragma warning (disable : 4103)     /* used #pragma pack to reset alignment */
#if defined (WIN32) || defined (WIN64)
#pragma pack(pop)
#else
#pragma pack()
#endif
#pragma warning (default : 4103)


#endif /* __PB_DP__ */
