/*******************************************************************************************/
/*  Filename    : papi.c                                                                   */
/*                                                                                         */
/*  Description : The module provides consistency checks for PROFIBUS PA service requests  */
/*                and fills in the service specific data size for the request data.        */
/*                                                                                         */
/*******************************************************************************************/

#include "keywords.h"

INCLUDES

#ifndef API_LINUX
#   include <windows.h>
#   include <winbase.h>
#   include <crtdbg.h>
#   include <initguid.h>
#   include <ws2bth.h>
#   include <tchar.h>
#else
#   include <errno.h>
#   include "api_linux.h"
#   include "linux_win32_funcs.h"
#   include "serial_port.h"
#endif
#include <wchar.h>
#include "fbapi_if.h"
#include "protocol_if.h"
#include "trace.h"

GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA


/******************************************************************************** */

FUNCTION INT16 papi_check_send_req
         (
          IN  VOID                   *p_sdb,
          OUT USIGN16                *sdb_len,
          IN  VOID                   *data_ptr,
          OUT INT16                  *sdbdata_len
         )

/*----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function checks the request parameter for an FF service


INPUT:  sdb_ptr                  -> pointer to SERVICE-DESCRIPTION-BLOCK
        sdb_len                  -> length of sdb
        data_ptr                 -> pointer to service specific data


Possible return values:

- E_OK                           -> no error occured

- E_IF_INVALID_LAYER             -> invalid layer
- E_IF_INVALID_SERVICE           -> invalid service identifier
- E_IF_INVALID_PRIMITIVE         -> invalid service primitive
- E_IF_INVALID_COMM_REF          -> invalid communication reference
----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

    INT16 ret_val;   /* return value   */   
    T_PROFI_SERVICE_DESCR  *sdb_ptr;

FUNCTION_BODY

    ASSERT(p_sdb);

    sdb_ptr = (T_PROFI_SERVICE_DESCR  *) p_sdb; 
    ret_val      = E_OK;
    *sdbdata_len = 0;
    *sdb_len     = sizeof(*sdb_ptr);

    /* check communication reference --------------------------------------- */
    if (sdb_ptr->comm_ref > MAX_COMREF)
    {
        return (E_IF_INVALID_COMM_REF);
    }

    /* check Service-Primitive --------------------------------------------- */
    if ((sdb_ptr->primitive != REQ) && (sdb_ptr->primitive != RES))
    {
        return (E_IF_INVALID_PRIMITIVE);
    }

    /* set the result parameter to POS if a request is send ---------------- */
    if (sdb_ptr->primitive == REQ)
    {
        sdb_ptr->result = POS;
    }

    switch (sdb_ptr->layer)
    {
        /* get service specific data length ------------------------------------ */
        case DP:
        {
            ret_val = dpgdl_get_data_len(sdb_ptr->result,
                                   sdb_ptr->service,
                                   sdb_ptr->primitive,
                                   data_ptr,
                                   sdbdata_len
                                   );
            break;
        }

        case FMB:
        {
            ret_val = fmbgdl_get_data_len(sdb_ptr->result,
                                   sdb_ptr->service,
                                   sdb_ptr->primitive,
                                   data_ptr,
                                   sdbdata_len
                                   );
            break;
        }


        default:
        {
            ret_val = E_IF_INVALID_LAYER;
            break;
        }
    }

    return (ret_val);
}

/******************************************************************************** */

FUNCTION USIGN16 papi_get_sdb_len (VOID)

/*----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function calculates the size of the PROFIBUS service description block.

*/
{
LOCAL_VARIABLES

FUNCTION_BODY

    return ((USIGN16) sizeof(T_PROFI_SERVICE_DESCR));
}
