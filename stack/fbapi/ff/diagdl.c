/*  Filename    : diagdl.c                                                   */
/*                                                                           */
/*  Description : The module provides functions for calculating the length   */
/*                of the service parameter blocks for diagnostic service     */
/*                requests or responses.                                    */
/*                                                                           */
/**********************        VSS Section         ***************************/
/*                                                                           */
/*  Fileversion : $Revision:: 6          $                                   */
/*  Checked in  : $Date:: 16.05.01 12:59 $                                   */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES


#ifdef API_LINUX
#    include "api_linux.h"
#else
#    include <windows.h>
#    include <tchar.h>
#endif

#include "ff_if.h"

#if DIAG_SUPP

GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA



FUNCTION PUBLIC INT16 diagdl_get_data_len
         (
          IN   INT16        result,             /* Service-Result       */
          IN   USIGN8       service,            /* Service Code         */
          IN   USIGN8       primitive,          /* Service-Primitive    */
          IN   USIGN8       *data_ptr,          /* pointer to data      */
          OUT  INT16        *data_len           /* length of data       */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

this function is used to return the data length of diagnostics services.
There are only service requests, no responses

possible return values:
- E_OK                 if service code is valid
- E_IF_INVALID_SERVICE if service code is invalid

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

*data_len = 0;

switch (service)
{

    case DIAG_RUNTIME:
        *data_len  =  sizeof (T_RUNTIME_DIAG);
        break;

    case DIAG_READ_INFO:
        *data_len  =  sizeof (T_DIAG_INFO_REQ);
        break;

    case DIAG_SET_LOOPBACK:
        *data_len  =  sizeof (T_SET_LOOPBACK);
        break;

    default:
         return (E_IF_INVALID_SERVICE);
}

return (E_OK);

}

#endif
