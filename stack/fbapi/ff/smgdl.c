/*  Filename    : smgdl.c                                                    */
/*                                                                           */
/*  Description : The module provides functions for calculating the length   */
/*                of the service parameter blocks for SM service requests or */
/*                responses.                                                 */
/*                                                                           */
/**********************        VSS Section         ***************************/
/*                                                                           */
/*  Fileversion : $Revision:: 4          $                                   */
/*  Checked in  : $Date:: 16.05.01 12:59 $                                   */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES
#ifdef API_LINUX
#   include "api_linux.h"
#else
#   include <windows.h>
#   include <tchar.h>
#endif
#include "fbapi_if.h"
#include "ff_if.h"

GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA


FUNCTION LOCAL INT16 smgdl_get_spd_len (VOID)

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Set Physical Device Tag
Request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

     return (sizeof (T_SET_PD_TAG_REQ));

}



FUNCTION LOCAL INT16 smgdl_get_sadr_len (VOID)

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Set Address Request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

     return (sizeof (T_SET_ADDRESS_REQ));

}




FUNCTION LOCAL INT16 smgdl_get_cadr_len (VOID)

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Clear Address Request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

     return (sizeof (T_CLEAR_ADDRESS_REQ));

}





FUNCTION LOCAL INT16 smgdl_get_id_len (VOID)

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for an Identify request

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

     return (sizeof (T_SM_IDENTIFY_REQ));

}




FUNCTION LOCAL INT16 smgdl_get_tq_len
         (
           IN  USIGN8    *data_ptr               /* pointer to data      */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a TAG QUERY service.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES
    T_TAG_QUERY_REQ *tag_query  =  (T_TAG_QUERY_REQ *) data_ptr;
    INT16           d_size;

FUNCTION_BODY

     d_size = sizeof (tag_query->tag_type) + sizeof (tag_query->dest_addr);

    switch (tag_query->tag_type)
    {
         case QUERY_VFD_TAG:
             d_size += sizeof (tag_query->query.vfd_tag);
             break;

         case QUERY_PD_TAG:
             d_size += sizeof (tag_query->query.pd_tag);
             break;

         case QUERY_FB_TAG:
             d_size += sizeof (tag_query->query.fb_tag);
             break;

         default:
             break;
     }

     return (d_size);

}



#ifdef ALL_FF_SERVICES_SUPPORTED
FUNCTION LOCAL INT16 smgdl_get_tr_len
         (
           IN  USIGN8     *data_ptr               /* pointer to data      */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a TAG REPLY service.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES
    T_TAG_REPLY_REQ   *tag_reply  =  (T_TAG_REPLY_REQ *) data_ptr;
    INT16             d_size;

FUNCTION_BODY

     d_size = sizeof (tag_reply->tag_type) + sizeof (tag_reply->dest_addr) +
              sizeof (tag_reply->resp_node_addr);

    switch (tag_reply->tag_type)
    {

         case QUERY_VFD_TAG:
             d_size += sizeof (tag_reply->reply.vfd_tag) +
                       tag_reply->reply.vfd_tag.n_cr * sizeof (USIGN16);
             break;

         case QUERY_PD_TAG:
             d_size += sizeof (tag_reply->reply.pd_tag);
             break;

         case QUERY_FB_TAG:
             d_size += sizeof (tag_reply->reply.fb_tag) +
                       tag_reply->reply.fb_tag.n_cr * sizeof (USIGN16);
             break;

         default:
             break;
     }

     return (d_size);

}
#endif /* ALL_FF_SERVICES_SUPPORTED */



FUNCTION LOCAL INT16 smgdl_get_set_cfg_len (VOID)

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Set Configuration Request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

     return (sizeof (T_SM_SET_CONFIGURATION_REQ));

}


#ifdef ALL_FF_SERVICES_SUPPORTED
FUNCTION LOCAL INT16 smgdl_get_vfd_ref_len
         (
           IN  USIGN8    *data_ptr               /* pointer to data      */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Load VFD reference list
request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

    T_SM_LOAD_VFD_REF   *vfd_ref = (T_SM_LOAD_VFD_REF *) data_ptr;

FUNCTION_BODY


     return (sizeof (*vfd_ref) + vfd_ref->no_of_elements * sizeof (T_VFD_REF_ENTRY));

}



FUNCTION LOCAL INT16 smgdl_get_sched_len
         (
           IN  USIGN8    *data_ptr               /* pointer to data      */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

This function returns the data block length for a Load Schedule request.

possible return values:
- data length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES
     T_SM_LOAD_SCHEDULE   *load_sched  = (T_SM_LOAD_SCHEDULE *) data_ptr;

FUNCTION_BODY

     return (sizeof (*load_sched) + load_sched->no_of_elements * sizeof (T_FB_START_ENTRY));

}
#endif /* ALL_FF_SERVICES_SUPPORTED */


FUNCTION PUBLIC INT16 smgdl_get_error_data_len
         (
          IN    USIGN8     service           /* Service  */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function is used to return the length of response-error-datas
of the following FMS-Services.

possible return values:
- Data-length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

return(sizeof(T_ERROR));
}


FUNCTION PUBLIC INT16 smgdl_get_data_len
         (
          IN   INT16        result,             /* Service-Result       */
          IN   USIGN8       service,            /* Service Code         */
          IN   USIGN8       primitive,          /* Service-Primitive    */
          IN   USIGN8       *data_ptr,          /* pointer to data      */
          OUT  INT16        *data_len           /* length of data       */
         )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

this function is used to return the data length of SM services.
There is a response only for the BIND_TAG service. For all other services
only a Request exist.

possible return values:
- E_OK                 if service code is valid
- E_IF_INVALID_SERVICE if service code is invalid

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

    *data_len = 0;

    switch (service)
    {
        case SM_SET_PD_TAG:
            *data_len = smgdl_get_spd_len();
            break;

        case SM_SET_ADDRESS:
            *data_len = smgdl_get_sadr_len();
            break;

        case SM_CLEAR_ADDRESS:
            *data_len = smgdl_get_cadr_len();
            break;

        case SM_IDENTIFY:
            *data_len = smgdl_get_id_len();
            break;

        case SM_FIND_TAG_QUERY:
            *data_len = smgdl_get_tq_len(data_ptr);
            break;

#ifdef ALL_FF_SERVICES_SUPPORTED
        case SM_FIND_TAG_REPLY:
            *data_len = smgdl_get_tr_len(data_ptr);
            break;
#endif /* ALL_FF_SERVICES_SUPPORTED */

        case SM_SET_CONFIGURATION:
            *data_len = smgdl_get_set_cfg_len();
            break;

#ifdef ALL_FF_SERVICES_SUPPORTED
        case SM_LOAD_VFD_REF:
            *data_len = smgdl_get_vfd_ref_len(data_ptr);
            break;

        case SM_LOAD_SCHEDULE:
            *data_len = smgdl_get_sched_len(data_ptr);
            break;
#endif /* ALL_FF_SERVICES_SUPPORTED */

        default:
            return (E_IF_INVALID_SERVICE);
    }

    return (E_OK);

}

