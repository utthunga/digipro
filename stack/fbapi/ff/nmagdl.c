/*  Filename    : nmagdl.c                                                   */
/*                                                                           */
/*  Description : The module provides functions for calculating the length   */
/*                of the service parameter blocks for NMA service requests   */
/*                or responses.                                              */
/*                                                                           */
/**********************        VSS Section         ***************************/
/*                                                                           */
/*  Fileversion : $Revision:: 7          $                                   */
/*  Checked in  : $Date:: 16.05.01 12:59 $                                   */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES

#ifdef API_LINUX
#    include "api_linux.h"
#else
#    include <windows.h>
#    include <tchar.h>
#endif

#include "fbapi_if.h"
#include "ff_if.h"


GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA



FUNCTION LOCAL   INT16  nmagdl_get_ctxt_data_len
          (
            IN  USIGN8     service,          /* Service              */
            IN  USIGN8     primitive,        /* Service-Primitive    */
            IN  USIGN8     *data_ptr         /* pointer to data      */
          )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function is used to return the length of request-datas or response-datas
of the following FMS-Context-Management-Services.

possible return values:
- Data-length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

FUNCTION_BODY

switch (service)
{
  case MGMT_INITIATE:
       if (primitive == REQ) return (sizeof (T_CTXT_INIT_REQ));
       else                  return (0);

  default:
       return(0);
}
}


FUNCTION LOCAL INT16 nmagdl_get_var_data_len
          (
            IN  USIGN8      service,          /* Service              */
            IN  USIGN8      primitive,        /* Service-Primitive    */
            IN  USIGN8      *data_ptr         /* pointer to data      */
          )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function is used to return the length of request-datas or response-datas
of the following Variable-Access-Services.

possible return values:
- Data-length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES


FUNCTION_BODY

switch(service)
{
  case MGMT_READ:
       if (primitive == REQ)
       {
         return(sizeof(T_VAR_READ_REQ));
       }
       else
       {
          T_VAR_READ_CNF *rsp = (T_VAR_READ_CNF *) data_ptr;
          return(sizeof(T_VAR_READ_CNF) + rsp->length);
       }

  case MGMT_WRITE:
       if (primitive == REQ)
       {
         T_VAR_WRITE_REQ *req = (T_VAR_WRITE_REQ *) data_ptr;
         return(sizeof(T_VAR_WRITE_REQ) + req->length);
       }
       else  return(0);

  default:
       return(0);
}
}



FUNCTION PUBLIC INT16 nmagdl_get_data_len
          (
            IN  INT16      result,           /* Service-Result       */
            IN  USIGN8     service,          /* Service              */
            IN  USIGN8     primitive,        /* Service-Primitive    */
            IN  USIGN8     *data_ptr,        /* pointer to data      */
            OUT INT16      *data_len         /* length of data       */
          )

/*-----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION

this function is used to return the data length of NMA-SERVICES

possible return values:
- Data-length

-----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES


FUNCTION_BODY

switch (service)
{

   case NMA_LOAD_VCRL_ENTRY:
        *data_len = sizeof (T_LOAD_VCRL_ENTRY_REQ);
        break;

   case NMA_INIT_LOAD_VCRL:
   case NMA_TERM_LOAD_VCRL:
#ifdef ALL_FF_SERVICES_SUPPORTED
   case NMA_READ_IDENT:
   case NMA_RESET:
#endif /* ALL_FF_SERVICES_SUPPORTED */
        *data_len = 0;
        break;

#ifdef ALL_FF_SERVICES_SUPPORTED
   case NMA_LOAD_CONFIG:
        *data_len = sizeof (T_LOAD_CONFIG_REQ);
        break;
#endif /* ALL_FF_SERVICES_SUPPORTED */

   case NMA_LOAD_DLL_CONFIG:
        *data_len = sizeof (T_LOAD_DLL_CONFIG_REQ);
        break;

   case NMA_INIT_LOAD_SCHED_LOC:
   case NMA_TERM_LOAD_SCHED_LOC:
        *data_len = sizeof (T_DOM_REQ);
        break;

   case NMA_LOAD_SCHED_LOC:
        {
           T_GEN_DNL_SEG_REQ *req = (T_GEN_DNL_SEG_REQ *) data_ptr;
           *data_len = sizeof (*req) + req->data_len;
        }
        break;

#ifdef ALL_FF_SERVICES_SUPPORTED
   case NMA_INIT_UPL_SCHED_LOC:
   case NMA_UPL_SCHED_LOC:
   case NMA_TERM_UPL_SCHED_LOC:
        *data_len = sizeof (T_DOM_REQ);
        break;
#endif /* ALL_FF_SERVICES_SUPPORTED */

   case MGMT_INITIATE:
        if (result == POS) *data_len = nmagdl_get_ctxt_data_len(service,primitive,data_ptr);
        else               *data_len = 0;
        break;

   case MGMT_READ:
   case MGMT_WRITE:
        if (result == POS) *data_len = nmagdl_get_var_data_len(service,primitive,data_ptr);
        else               *data_len = sizeof(T_ERROR);
        break;

   default:
        return(E_IF_INVALID_SERVICE);
 }

return(E_OK);
}
