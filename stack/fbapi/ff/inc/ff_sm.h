/*---------------------------------------------------------------------------*/
/*                                                                           */
/*  Filename    : ff_sm.h                                                    */
/*                                                                           */
/*  Description : The file contains defines and data structures used at      */
/*                system management user interface                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/


#ifndef __FF_SM_H__
#define __FF_SM_H__

#pragma pack(push, 2)

/* --- maximal length of system management tag -------------------------- */
#define SM_TAG_LEN                      32
#define SM_TAG_SIZE                     (SM_TAG_LEN+2)
#define MAX_FB_ELEMS                    20

/* --- End of list Identifier ------------------------------------------- */
#define EOL                             0xffff

/* --- SM-Service-Identifiers ------------------------------------------- */
/* --- remote SM services ----------------------------------------------- */
#define SM_SET_PD_TAG                  0x00
#define SM_SET_ADDRESS                 0x01
#define SM_CLEAR_ADDRESS               0x02
#define SM_IDENTIFY                    0x03
#define SM_FIND_TAG_QUERY              0x04
#define SM_FIND_TAG_REPLY              0x05


/* --- local SM services ------------------------------------------------ */
#define SM_SET_CONFIGURATION           0x07
#define SM_EVENT                       0x0a

/* --- enhanced service available for IOP/FIM platform ------------------ */
#define SM_DLL_EVENT                   0x0b

/* --- Ident number for MIB objects ------------------------------------- */
#define ID_SM_DIRECTORY                 256

/* --- Agent objects ---------------------------------------------------- */
#define ID_SM_SUPPORT                   (ID_SM_DIRECTORY+2)
#define ID_T1                           (ID_SM_SUPPORT+1)
#define ID_T2                           (ID_T1+1)
#define ID_T3                           (ID_T2+1)

/* --- Sync and scheduling objects -------------------------------------- */
#define ID_CURRENT_TIME                 (ID_T3+1)
#define ID_LOCAL_TIME_DIFF              (ID_CURRENT_TIME+1)
#define ID_AP_CLOCK_SYNC_INTERVAL       (ID_LOCAL_TIME_DIFF+1)
#define ID_TIME_LAST_RCVD               (ID_AP_CLOCK_SYNC_INTERVAL+1)
#define ID_PRIMARY_AP_TIME_PUBLISHER    (ID_TIME_LAST_RCVD+1)
#define ID_TIME_PUBLISHER_ADDR          (ID_PRIMARY_AP_TIME_PUBLISHER+1)
#define ID_LAST_T0                      (ID_TIME_PUBLISHER_ADDR+1)
#define ID_MACROCYCLE_DURATION          (ID_LAST_T0+1)

/* --- subindex for which write access to current time is permitted ----- */
#define ID_WRITE_CURRENT_TIME           99

/* --- Address assignment objects --------------------------------------- */
#define ID_DEV_ID                       (ID_MACROCYCLE_DURATION+1)
#define ID_PD_TAG                       (ID_DEV_ID+1)
#define ID_OP_POWERUP                   (ID_PD_TAG+1)

/* --- SM DLL event codes ----------------------------------------------- */
#define EVT_LAS_ROLE_CHANGED            0x00
#define EVT_LL_CLEARED                  0x01
#define EVT_LL_CHANGED                  0x02

#define MAX_LL_CHG                      50

/* --- VFD reference list objects --------------------------------------- */
#define ID_VFD_REF_ENTRY_OBJS           (ID_OP_POWERUP+1)

/* --- FB scheduling objects -------------------------------------------- */
#define ID_VERSION_OF_SCHEDULE          (ID_VFD_REF_ENTRY_OBJS+MAX_VFD)
#define ID_FB_START_ENTRY_OBJS          (ID_VERSION_OF_SCHEDULE+1)


/* --- Length of octet and visible strings ------------------------------ */
#define SERV_SUPP_LEN                   FEAT_SUPP_LEN

/* --- SM event codes --------------------------------------------------- */
#define SM_EVT_PD_TAG_SET               0x00
#define SM_EVT_PD_TAG_CLEARED           0x01
#define SM_EVT_NODE_ADDR_SET            0x02
#define SM_EVT_NODE_ADDR_CLEARED        0x03
#define SM_EVT_DUPLICATE_NODE_ADDR      0x04

/* --- Tag identifiery for Tag Query / Tag Reply services --------------- */
#define QUERY_PD_TAG                    0x00
#define QUERY_VFD_TAG                   0x01
#define QUERY_FB_TAG                    0x02

/* --- constants for identifying SM exceptions ------------------------ */
#define MOD_SM                         0x00
#define MOD_SMAUX                      0x01
#define MOD_SMMEM                      0x02
#define MOD_SMTIM                      0x03
#define MOD_SMDPR                      0x04
#define MOD_SMSND                      0x05
#define MOD_SMID                       0x06
#define MOD_SMTQR                      0x07
#define MOD_SMADR                      0x08
#define MOD_SMSCE                      0x09
#define MOD_SMRST                      0x0a
#define MOD_SMSPD                      0x0b

/*------------------------------------------------------------------------
    new basic datatypes for time values, time differences and
    communication data
--------------------------------------------------------------------------*/
typedef struct _T_SM_TIME
{
    USIGN32     high_u32;
    USIGN32     low_u32;

} T_SM_TIME ;


/*------------------------------------------------------------------------
    Set Configuration service
--------------------------------------------------------------------------*/

typedef struct _T_SM_SET_CONFIGURATION_REQ
{

    STRINGV         pd_tag [SM_TAG_SIZE];   /**< physical device tag         */
    T_SM_TIME       current_time;           /**< preset value for time in 1/32 ms since 1.1.1972 */
    INT32           local_time_diff;        /**< adjustment for timezone     */
    USIGN32         macrocycle_duration;    /**< duration of macrocycle      */
    USIGN8          sm_support [4];         /**< supported sm features       */
    USIGN32         t1;                     /**< supervision timer T1        */
    USIGN32         t2;                     /**< supervision timer T2        */
    USIGN32         t3;                     /**< supervision timer T3        */
    USIGN8          ap_clock_sync_interval; /**< synchronization interval    */
    USIGN8          primary_time_publisher; /**< default time publisher      */
    FB_BOOL         op_powerup;             /**< operational powerup         */
    USIGN8          max_fb_elems;           /**< maximal number of FBs       */

} T_SM_SET_CONFIGURATION_REQ;


/*************************************************************************
 ******************* system management remote services *******************
 *************************************************************************/

/*------------------------------------------------------------------------
    Set physical device tag
--------------------------------------------------------------------------*/

typedef struct _T_SET_PD_TAG_REQ
{

    USIGN8      node;                       /* node to be set           */
    FB_BOOL     clear;                      /* Clear or set PD tag      */
    STRINGV     pd_tag [SM_TAG_SIZE];       /* physical device tag      */
    STRINGV     dev_id [SM_TAG_SIZE];       /* device id                */

} T_SET_PD_TAG_REQ;


/*------------------------------------------------------------------------
    Set address
--------------------------------------------------------------------------*/

typedef struct _T_SET_ADDRESS_REQ
{

    USIGN8      node;                       /* new node address         */
    USIGN8      dummy;                      /* alignment byte           */
    STRINGV     pd_tag [SM_TAG_SIZE];       /* physical device tag      */
    USIGN8      ap_clock_sync_interval;     /* synchronization interval */
    USIGN8      primary_time_publisher;     /* default time publisher   */

} T_SET_ADDRESS_REQ;


/*------------------------------------------------------------------------
    Clear address
--------------------------------------------------------------------------*/

typedef struct _T_CLEAR_ADDRESS_REQ {

    USIGN8      node;                       /* node to be cleared       */
    USIGN8      dummy;                      /* alignment byte           */
    STRINGV     pd_tag [SM_TAG_SIZE];       /* physical device tag      */
    STRINGV     dev_id [SM_TAG_SIZE];       /* device id                */

} T_CLEAR_ADDRESS_REQ;


/*------------------------------------------------------------------------
    Identify device
--------------------------------------------------------------------------*/

typedef struct _T_SM_IDENTIFY_REQ {

    USIGN8      node;                       /* of device that is searched */
    USIGN8      dummy;                      /* alignment byte             */

} T_SM_IDENTIFY_REQ;

typedef struct T_SM_IDENTIFY_CNF {

    STRINGV     pd_tag [SM_TAG_SIZE];       /* physical device tag      */
    STRINGV     dev_id [SM_TAG_SIZE];       /* device id                */

} T_SM_IDENTIFY_CNF;


/*------------------------------------------------------------------------
    Tag Query, Tag Reply
--------------------------------------------------------------------------*/

typedef struct _T_QUERY_PD_TAG {

    STRINGV      pd_tag [SM_TAG_SIZE];      /* physical device tag      */

} T_QUERY_PD_TAG;

typedef struct _T_QUERY_VFD_TAG {

    STRINGV      pd_tag [SM_TAG_SIZE];      /* PD tag                   */
    STRINGV      vfd_tag [SM_TAG_SIZE];     /* VFD tag                  */

} T_QUERY_VFD_TAG;

typedef struct _T_QUERY_FB_TAG {

    FB_BOOL     with_param;                 /* parameter contained      */
    USIGN8      dummy;                      /* alignment byte           */
    USIGN32     param_id;                   /* parameter id             */
    STRINGV     fb_tag [SM_TAG_SIZE];       /* function block tag       */

} T_QUERY_FB_TAG;


typedef struct _T_TAG_QUERY_REQ {

    USIGN16         tag_type;               /* PD Tag, VFD Tag or FB Tag */
    USIGN32         dest_addr;              /* destination address       */
    union {
        T_QUERY_PD_TAG      pd_tag;         /* query for PD Tag          */
        T_QUERY_VFD_TAG     vfd_tag;        /* query for VFD Tag         */
        T_QUERY_FB_TAG      fb_tag;         /* query for FB Tag          */
    } query;

} T_TAG_QUERY_REQ;


typedef struct _T_REPLY_PD_TAG {

    STRINGV     dev_id [SM_TAG_SIZE];       /* device identification    */

} T_REPLY_PD_TAG;

typedef struct _T_REPLY_VFD_TAG {

    USIGN32     vfd_ref;                    /* VFD reference            */
    USIGN16     od_version;                 /* version # of OD          */
    FB_BOOL     more_vcrl;                  /* more VCRL available      */
    USIGN8      n_cr;                       /* # of references in list  */
/*  USIGN16     cr [n_cr];                     communication references */

} T_REPLY_VFD_TAG;

typedef struct _T_REPLY_FB_TAG {

    USIGN32     vfd_ref;                    /* VFD reference            */
    USIGN16     od_version;                 /* version # of OD          */
    USIGN16     index;                      /* index in OD              */
    FB_BOOL     more_vcrl;                  /* more VCRL available      */
    USIGN8      n_cr;                       /* # of references in list  */
/*  USIGN16     cr [n_cr];                     communication references */

} T_REPLY_FB_TAG;

typedef struct _T_TAG_REPLY_IND {

    USIGN16         tag_type;               /* PD Tag, VFD Tag or FB Tag */
    USIGN32         src_addr;               /* source address            */
    USIGN32         resp_node_addr;         /* responding node address   */
    union {
        T_REPLY_PD_TAG      pd_tag;         /* reply for PD Tag          */
        T_REPLY_VFD_TAG     vfd_tag;        /* reply for VFD Tag         */
        T_REPLY_FB_TAG      fb_tag;         /* reply for FB Tag          */
    } reply;

} T_TAG_REPLY_IND;


/*------------------------------------------------------------------------
    SM Event Indication
--------------------------------------------------------------------------*/
typedef struct _T_SM_EVENT_IND
{
    USIGN8      evt_code;                    /* code of the event       */
    USIGN8      dummy;                       /* alignment byte          */
    USIGN8      description [SM_TAG_SIZE];   /* event description       */

} T_SM_EVENT_IND;

/*------------------------------------------------------------------------
    SM DLL Event Indication: enhancement for IOP/FIM version only
--------------------------------------------------------------------------*/
typedef struct _T_CHG_LIST
{
    USIGN8      node;
    USIGN8      status;

} T_CHG_LIST;

typedef struct _T_LL_CHG
{

    USIGN8      elems;                  /* number of elements in ll_chg */
    USIGN8      dummy;                  /* alignment byte               */
    T_CHG_LIST  list[MAX_LL_CHG];       /* list of node address with changed liveness status */

} T_LL_CHG;

typedef struct _T_SM_DLL_EVENT {

    USIGN8          evt_code;               /* EVT_LL_CHANGED, EVT_ROLE_CHANGED */
    USIGN8          dummy;                  /* alignment byte                   */
    union {
        FB_BOOL             role_is_las;    /* FB_TRUE: acting as LAS           */
        T_LL_CHG            ll_chg;         /* life list changes                */
    } data;

} T_SM_DLL_EVENT;

#pragma pack(pop)

#endif  /* __FF_SM_H__ */
