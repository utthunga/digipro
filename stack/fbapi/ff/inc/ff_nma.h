/*---------------------------------------------------------------------------*/
/*                                                                           */
/*  Filename    : ff_nma.h                                                   */
/*                                                                           */
/*  Description : This file contains the types and defines for the NMA       */
/*                service interface.                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef __FF_NMA_H__
#define __FF_NMA_H__

#pragma pack(push, 2)

/* --- service identifiers based on FMS service identifiers ------------- */
#define MGMT_SERV_OFFSET        0x20

#define MGMT_INITIATE           (MGMT_SERV_OFFSET+FMS_INITIATE)
#define MGMT_ABORT              (MGMT_SERV_OFFSET+FMS_ABORT)
#define MGMT_READ               (MGMT_SERV_OFFSET+FMS_READ)
#define MGMT_WRITE              (MGMT_SERV_OFFSET+FMS_WRITE)

/* -------------------------------------------------------------------------- */
/* For the service indication the data types are the FMS data types           */
/* the MGMT_INITIATE response is without a data structure                     */
/*                                                                            */
/* MGMT_INITIATE:   indication:     T_CTXT_INIT_REQ                           */
/*                  response:       -                                         */
/* MGMT_ABORT:      indication:     T_CTXT_ABORT_REQ                          */
/* MGMT_READ:       indication:     T_VAR_READ_REQ                            */
/*                  request:        T_VAR_READ_REQ                            */
/*                  confirmation:   T_VAR_READ_CNF / T_ERROR                  */
/*                  response:       T_VAR_READ_CNF / T_ERROR                  */
/* MGMT_WRITE:      indication:     T_VAR_WRITE_REQ                           */
/*                  request:        T_VAR_WRITE_REQ                           */
/*                  confirmation:   - / T_ERROR                               */
/*                  response:       - / T_ERROR                               */
/*                                                                            */
/* -------------------------------------------------------------------------- */



/****************************************************************************/
/*************    NMA-SERVICE-IDENTIFIERS                ********************/
/****************************************************************************/

/*---NMA-Service-Identifiers ---------------------------------------------  */

/*---- local services ----------------------------------------------------  */
#define NMA_LOAD_DLL_CONFIG     0x02
#define NMA_INIT_LOAD_VCRL      0x03
#define NMA_LOAD_VCRL_ENTRY     0x04
#define NMA_TERM_LOAD_VCRL      0x05

#define NMA_INIT_LOAD_SCHED_LOC 0x0A
#define NMA_LOAD_SCHED_LOC      0x0B
#define NMA_TERM_LOAD_SCHED_LOC 0x0C

/****************************************************************************/
/* Load-DLL-Configuration Service (only local)                              */
/****************************************************************************/

#define NMA_BASIC_DEVICE    1
#define NMA_LINK_MASTER     2
#define NMA_BRIDGE          3


/**
 * Data structure for NMA_LOAD_DLL_CONFIG service request to set values for Data Link timing values.
 * The permitted range of some of the parameters is determined by the stack implementation. This is the
 * case for the minimum values for SlotTime, MaxReplyDelay and their product and for MinInterPDUDelay
 */
typedef struct _T_DLL_CONFIG_LM_DECIDED
{
    USIGN16         slot_time;                 /**< unit: 1 octet duration, TSL_MIN - 4095         */
    USIGN8          phl_overhead;              /**< unit: 1 octet duration, 2 - 63                 */
    USIGN8          max_reply_delay;           /**< unit: 1 slot time: MRD-MIN - 11                */
    USIGN8          min_inter_pdu_delay;       /**< unit: 1 octet duration: MID-MIN - 0xFFFFu      */
    USIGN8          phl_preamble_extension;    /**< unit: 1/2 octet duration: 0 - 7                */
    USIGN8          phl_gap_extension;         /**< unit: 1/2 octet duration: 0 - 7                */
    USIGN8          max_inter_chan_signal_skew;/**< not used, only one channel: 0 - 7              */
    USIGN8          time_sync_class;           /**< V(TSC): 0 - 7                                  */
    USIGN8          first_unpolled_node;       /**< V(FUN): 0x14 - 0xf7                            */
    USIGN8          num_unpolled_node;         /**< V(NUN): 0x00 - 0xe4 && V(FUN) + V(NUN) <= 0xf8 */
    USIGN8          dummy;                     /**< alignment byte                                 */
    USIGN16         min_token_delegation_time; /**< V(DMDT): 0x20 - 0x7fff octet times             */
    USIGN16         default_token_hold_time;   /**< V(DTHT): 0x114 - 65000 octet times             */
    USIGN16         target_token_rot_time;     /**< V(TTRT): 1 - 6000 ms                           */
    USIGN16         max_link_maint_time;       /**< V(LTHT): 0x124 - 65000 octet times             */
    USIGN16         max_inact_claim_las_delay; /**< V(MICD): 1 - 4095 bit times                    */
    USIGN16         dtb_sta_spdu_distr_period; /**< V(LDDP): 100 - 55000 ms                        */
    USIGN32         time_distribution_period;  /**< V(TDP):  10 - 55000 ms                         */

} T_DLL_CONFIG_LM_DECIDED;

/**
 * Data structure for NMA_LOAD_DLL_CONFIG service request with some Link Master/Bridge
 * object value. Attribute primary_las permits to set the initial value of the MIB object
 * PrimaryLinkMaster flag. If this is set the host stack tries to assume the LAS role after
 * joining the token ring. It is also possible to give the value 0xFA for primary_las when
 * the host stack is configured to run as a LinkMaster. In this case the host stack will act
 * as Link Master but will not grap the LAS role in case of H1 Link inactivity.
 * Parameter decided_val indicates if the values of the Data Link parameters shall be set
 * as given in the request or if default values will be used. The used default values are:
 *  slot_time                   =  6u;
 *  phl_overhead                =  4u;
 *  max_reply_delay             =  3u;
 *  preamble_extension          =  2u;
 *  gap_extension               =  0u;
 *  min_inter_pdu_delay         =  8u;
 *  max_inter_chan_signal_skew  =  0u;
 *  this_link                   =  0u;
 *  time_sync_class             =  5u; -> time sync class 100us
 *
 *  first_unpolled_node         =  0xf7u;
 *  num_unpolled_node           =  0x00;
 *
 *  def_min_token_deleg_time    =  0x54u + phl_overhead;
 *  def_token_hold_time         =  0x114u + phl_overhead;
 *  target_token_rot_time       =  59999u;
 *  dtb_sta_spdu_distr_period   =  5000u;
 *  time_distribution_period    =  5000uL;
 *  max_inact_claim_las_delay   =  100u;
 *  link_maint_tok_hold_time    =  0x124u + (3u*phl_overhead + 2*slot_time);
 *
 */
typedef struct _T_DLL_CONFIG_LM
{
    USIGN16                    this_link;      /**< local link id (= 0)           */
    FB_BOOL                    primary_las;    /**< FB_TRUE: act as Primary LAS
                                                    FB_FALSE: don_t act as Primary LAS
                                                    0xFAu: don't assume LAS role       */
    FB_BOOL                    use_decided;    /**< FB_FALSE use default values
                                                    FB_TRUE: use values in decided_val */
    T_DLL_CONFIG_LM_DECIDED    decided_val;    /**< for use_decided == TRUE    */

} T_DLL_CONFIG_LM;

/**
 * Data structure for NMA_LOAD_DLL_CONFIG service request. It will determine if
 * the FF host stack will act as Link Master, Basic Field Device or Bridge, its
 * node address and the timing parameters to be used in the Data Link Layer.
 */
typedef struct _T_LOAD_DLL_CONFIG_REQ
{
    USIGN8                  type;         /**< NMA_LINK_MASTER, NMA_BASIC_DEVICE,
                                               NMA_BRIDGE                        */
    USIGN8                  this_node;    /**< local node address 10 - 255       */

    USIGN16                 dummy;        /**< alignment for 4 byte boundary     */
    T_DLL_CONFIG_LM         lm;           /**< attributes to be used as Link
                                               Master or Bridge device           */

} T_LOAD_DLL_CONFIG_REQ;

/****************************************************************************/
/* Confirmation for Load-DLL-Configuration Service                          */
/****************************************************************************/

/**
 * Data structure for confirmation of NMA_LOAD_DLL_CONFIG service. It contains the
 * first index of all local MIB sections as described in the FF NMA specication
 * FF-801 and the FF SM specification FF-880.
 */
typedef struct _T_NMA_INFO
{
    USIGN16         stack_mgmt_idx;     /* first index: StackManagement objects    */
    USIGN16         vcr_list_idx;       /* first index: VCR List objects           */
    USIGN16         dlme_basic_idx;     /* first index: DlmeBasic objects          */
    USIGN16         dlme_lm_idx;        /* first index: DlmeLinkMaster objects     */
    USIGN16         link_sched_idx;     /* first index: LinkScheduleList objects   */
    USIGN16         dlme_bridge_idx;    /* reserved for future use                 */
    USIGN16         plme_basic_idx;     /* first index: PlmeBasic objects          */
    USIGN16         sm_agent_idx;       /* first index: SM agent objects           */
    USIGN16         sync_sched_idx;     /* first index: SyncAndSchedule objects    */
    USIGN16         addr_assign_idx;    /* first index: Address Assignment objects */
    USIGN16         vfd_ref_idx;        /* first index: VFD reference list objects */
    USIGN16         fb_sched_idx;       /* first index: FB schedule objects        */
    USIGN32         unused_memory;      /* free memory, not used by block manager  */

} T_NMA_INFO;

/****************************************************************************/
/* VCRL-Management                                                          */
/****************************************************************************/

/*---- AR-type and -role ---------------------------------------------------*/
#define QUB_PEER_INITIATOR        0x92
#define QUB_PEER_RESPONDER        0x12
#define QUB_CLIENT                0xA2
#define QUB_SERVER                0x32
#define QUU_SOURCE                0x44
#define QUU_SINK                  0x54
#define BNU_PUBLISHER             0x66
#define BNU_SUBSCRIBER            0x76
/*old names (used in previous versions of the FF specification */
#define QUU_NOTIFIER              QUU_SOURCE
#define QUU_NOTIFICATION_RECEIVER QUU_SINK

/*---- priority ------------------------------------------------------------*/
#define PRIO_NONE                 0
#define PRIO_TIME_AVAILABLE       3
#define PRIO_NORMAL               2
#define PRIO_URGENT               1

/*---- authentication ------------------------------------------------------*/
#define AUTHENT_ORDINARY          (0<<2)
#define AUTHENT_SOURCE            (2<<2)
#define AUTHENT_MAXIMAL           (3<<2)

/*---- delivery ------------------------------------------------------------*/
#define DELIV_UNORDERED           (0x00>>2)
#define DELIV_ORDERED             (0x40>>2)
#define DELIV_DISORDERED          (0x80>>2)
#define DELIV_CLASSICAL           (0xC0>>2)

/*---- scheduling ----------------------------------------------------------*/
#define SCHEDG_CYCLIC             0x80          /* explicit scheduling */
#define SCHEDG_ACYCLIC            0x00          /* implicit scheduling */

/****************************************************************************/
/*  Load-VCRL Service     (Local)                                           */
/****************************************************************************/

/**
 * Data structure to be used for Load-VCRL service during startup of the FF host stack.
 * The VCR is referred to by local communication reference (cr) which is from 1 -
 * MAX_COMREF. The other attricutes of the data structure are according to the
 * FF NMA spec FF-801. The comments of the attributes indicate which of the attributes
 * of an object of class VcrStaticEntry is referred.
 */
typedef struct _T_LOAD_VCRL_ENTRY_REQ
{
  USIGN16 cr;                             /**< -) communication reference           */
  USIGN16 fas_dll_max_cnf_delay_for_conn; /**< 5) FasDllMaxConfirmDelayOnConnect    */
  USIGN16 fas_dll_max_cnf_delay_for_data; /**< 6) FasDllMaxConfirmDelayOnData       */
  USIGN16 fas_dll_size;                   /**< 7) FasDllMaxDlsduSize                */

  USIGN32 fas_dll_local_addr;             /**< 2) FasDllLocalAddr                   */
  USIGN32 fas_dll_configured_remote_addr; /**< 3) FasDllConfiguredRemoteAddr        */

  USIGN8  fas_dll_priority;               /**< 4.4) priority                        */
  USIGN8  fas_dll_authentication;         /**< 4.3) authentication                  */
  USIGN8  fas_dll_delivery;               /**< 4.2) delivery                        */
  USIGN8  scheduling;                     /**< 4.1) cyclic or acyclic               */

  USIGN32 fms_vfd_id;                     /**< 14) virtual field device id          */

  USIGN8  fas_ar_type_and_role;           /**< 1) applic. relationship type / role  */
  USIGN8  max_scc;                        /**< 15) FmsMaxOutstandingServicesCalling */
  USIGN8  max_rcc;                        /**< 16) FmsMaxOutstandingServicesCalled  */
  FB_BOOL resid_act_supp;                 /**< 8) ResidualActivitySupported         */

  USIGN8  fms_feature_supp[FEAT_SUPP_LEN];/* 17) FMS features supported           */

  /* The following attributes are constants known during compiling time.
   * For this reason they are not part of this structure.
   *    10) FasDllTimelinessClass       = "None"
   *    11..13) all attributes          = 0
   */
} T_LOAD_VCRL_ENTRY_REQ;

/****************************************************************************/
/* Read Identification Service  (Local)                                     */
/****************************************************************************/
#define   FMAGT_MAX_NMA_TAG_LEN    34

/****************************************************************************/
/*  Read local Service    (Local)                                           */
/****************************************************************************/
#define T_VAR_READ_LOC_REQ T_VAR_READ_REQ
#define T_VAR_READ_LOC_CNF T_VAR_READ_CNF

/****************************************************************************/
/*  Write local Service    (Local)                                          */
/****************************************************************************/
#define T_VAR_WRITE_LOC_REQ T_VAR_WRITE_REQ

#pragma pack(pop)

#endif /* __FF_NMA_H__ */
