/*---------------------------------------------------------------------------*/
/*  Filename    : ffh1_error.h                                               */
/*                                                                           */
/*  Description : This file contains the ABORT, REJECT, EVENT and ERROR      */
/*                types and defines and the according reason codes.          */
/*                                                                           */
/*---------------------------------------------------------------------------*/


#ifndef __FF_ERROR_H__
#define __FF_ERROR_H__

#pragma pack(push, 2)

/*****************************************************************************/
/*************   ABORT IDENTIFIERS                       *********************/
/*****************************************************************************/
/* see "LAYER IDENTIFIER" in header file "ffh1_if.h"                         */

/*****************************************************************************/
/*************   ABORT REASON CODES                      *********************/
/*****************************************************************************/

/* --- USER abort reasons -------------------------------------------------- */
#define USR_ABT_RC1      0    /* disconnected by user                        */
#define USR_ABT_RC2      1    /* version object dictionary incompatible      */
#define USR_ABT_RC3      2    /* password error                              */
#define USR_ABT_RC4      3    /* profile number incompatible                 */
#define USR_ABT_RC5      4    /* limited services permitted                  */
#define USR_ABT_RC6      5    /* OD loading interacting (OD is beeing loaded)*/

/* --- FMS abort reasons --------------------------------------------------- */
#define FMS_ABT_RC1      0   /* FMS-VCR error (VCRL entry invalid)           */
#define FMS_ABT_RC2      1   /* user error (protocol violation by user)      */
#define FMS_ABT_RC3      2   /* FMS-PDU error (invalid FMS PDU received)     */
#define FMS_ABT_RC4      3   /* connection state conflict FAS                */
#define FMS_ABT_RC5      4   /* FAS error                                    */
#define FMS_ABT_RC6      5   /* PDU size exceeds maximum PDU size            */
#define FMS_ABT_RC7      6   /* feature not supported                        */
#define FMS_ABT_RC8      7   /* invoke id error in service response          */
#define FMS_ABT_RC9      8   /* max services overflow                        */
#define FMS_ABT_RC10     9   /* connection state conflict FMS (Initiate.req) */
#define FMS_ABT_RC11    10   /* service error (res != ind or con != req)     */
#define FMS_ABT_RC12    11   /* invoke id error in service request           */
#define FMS_ABT_RC13    12   /* FMS is disabled                              */

/* --- FAS abort reasons --------------------------------------------------- */
#define FAS_ABT_RC1               0x31 /* invalid FAS PDU                    */
#define FAS_ABT_RC2               0x32 /* remote address mismatch            */
#define FAS_ABT_RC3               0x33 /* multiple initiators                */
#define FAS_ABT_RC4               0x34 /* invalid DL event                   */
#define FAS_ABT_RC5               0x35 /* AREP busy                          */
#define FAS_ABT_RC6               0x36 /* AREP not found                     */
#define FAS_ABT_RC7               0x37 /* invalid AREP type                  */
#define FAS_ABT_RC8               0x38 /* DLCEP not found                    */

/* --- DLL abort reasons --------------------------------------------------- */
#define DLL_DC_PP_PAIRING         0x40 /* incorrect DLCEP pairing, permanent */
#define DLL_DC_PP_REV             0x41 /* incorrect DLCEP revision, permanent*/
#define DLL_DC_PP_OTHER           0x42 /* other permanent condition          */
#define DLL_DC_PP_FORMAT          0x43 /* wrong DLPDU format, permanent      */
#define DLL_DC_PP_SIZE            0x44 /* wrong DLSDU size, permanent        */
#define DLL_DC_PT_DISC            0x45 /* transient condition                */
#define DLL_DC_P_TIMEOUT          0x46 /* timeout                            */
#define DLL_DC_P_UNSPECIFIED      0x5E /* disconnection or rejection un-     */
                                       /* specified connection rejection     */
#define DLL_DC_P_REJ_ADDR_UNKNOWN 0x60 /* DL(SAP) address unknown            */
#define DLL_DC_PP_REJ_UNREACHABLE 0x62 /* DLSAP unreachable, permanent       */
#define DLL_DC_PT_REJ_UNREACHABLE 0x64 /* DLSAP unreachable, transient       */
#define DLL_DC_PP_REJ_QOS         0x66 /* QoS unavailable,permanent condition*/
#define DLL_DC_PP_REJ_STATE       0x65 /* wrong DLCEP state, permanent       */
#define DLL_DC_PT_REJ_QOS         0x68 /* QoS unavailable,transient condition*/
#define DLL_DC_UNKNOWN            0x7E /* unknown origin _ reason unspecified*/


/*****************************************************************************/
/*************   REJECT PDU TYPES and REASON CODES       *********************/
/*****************************************************************************/

/* --- PDU types ----------------------------------------------------------- */
#define CONFIRMED_REQUEST_PDU       1
#define CONFIRMED_RESPONSE_PDU      2
#define UNCONFIRMED_PDU             3
#define UNKNOWN_PDU_TYPE            4

/* --- reason codes -------------------------------------------------------- */
#define REJ_RC0  0            /* other than RC1 to RC6                       */
#define REJ_RC1  1            /* invoke id exists already                    */
#define REJ_RC2  2            /* max services overflow (max. SCC exceeded)   */
#define REJ_RC3  3            /* feature not supported - connection oriented */
#define REJ_RC4  4            /* feature not supported - connectionless      */
#define REJ_RC5  5            /* PDU size exceeds maximum PDU size allowed   */
#define REJ_RC6  6            /* user error on connectionless relation       */


/*****************************************************************************/
/*************   FMS ERROR CLASSES and ERROR CODES       *********************/
/*****************************************************************************/

/* The error class is encoded in the high byte of the 16-bit-result,         */
/* the error code in the low byte.                                           */

#define E_FMS_INIT                       0x0000
#define E_FMS_INIT_OTHER                 0x0000
#define E_FMS_INIT_MAX_PDU_SIZE_INSUFF   0x0001
#define E_FMS_INIT_FEAT_NOT_SUPPORTED    0x0002
#define E_FMS_INIT_OD_VERSION_INCOMP     0x0003
#define E_FMS_INIT_USER_DENIED           0x0004
#define E_FMS_INIT_PASSWORD_ERROR        0x0005
#define E_FMS_INIT_PROFILE_NUMB_INCOMP   0x0006

#define E_FMS_VFD_STATE_OTHER            0x0100

#define E_FMS_APPLICATION_OTHER          0x0200
#define E_FMS_APPLICATION_UNREACHABLE    0x0201

#define E_FMS_DEF_OTHER                  0x0300
#define E_FMS_DEF_OBJ_UNDEF              0x0301
#define E_FMS_DEF_OBJ_ATTR_INCONSIST     0x0302
#define E_FMS_DEF_OBJECT_ALREADY_EXISTS  0x0303

#define E_FMS_RESOURCE_OTHER             0x0400
#define E_FMS_RESOURCE_MEM_UNAVAILABLE   0x0401

#define E_FMS_SERV_OTHER                 0x0500
#define E_FMS_SERV_OBJ_STATE_CONFLICT    0x0501
#define E_FMS_SERV_PDU_SIZE              0x0502
#define E_FMS_SERV_OBJ_CONSTR_CONFLICT   0x0503
#define E_FMS_SERV_PARAM_INCONSIST       0x0504
#define E_FMS_SERV_ILLEGAL_PARAM         0x0505

#define E_FMS_ACCESS_OTHER               0x0600
#define E_FMS_ACCESS_OBJ_INVALIDATED     0x0601
#define E_FMS_ACCESS_HARDWARE_FAULT      0x0602
#define E_FMS_ACCESS_OBJ_ACCESS_DENIED   0x0603
#define E_FMS_ACCESS_ADDR_INVALID        0x0604
#define E_FMS_ACCESS_OBJ_ATTR_INCONST    0x0605
#define E_FMS_ACCESS_OBJ_ACCESS_UNSUPP   0x0606
#define E_FMS_ACCESS_OBJ_NON_EXIST       0x0607
#define E_FMS_ACCESS_TYPE_CONFLICT       0x0608
#define E_FMS_ACCESS_NAME_ACCESS_UNSUP   0x0609
#define E_FMS_ACCESS_TO_ELEMENT_UNSUPP   0x060A

#define E_FMS_OD_OTHER                   0x0700
#define E_FMS_OD_NAME_LEN_OVERFLOW       0x0701
#define E_FMS_OD_OVERFLOW                0x0702
#define E_FMS_OD_WRITE_PROTECT           0x0703
#define E_FMS_OD_EXTENSION_LEN_OVERFLOW  0x0704
#define E_FMS_OD_OBJ_DESCR_OVERFLOW      0x0705
#define E_FMS_OD_OPERAT_PROBLEM          0x0706

#define E_FMS_OTHER                      0x0800


/*****************************************************************************/
/*************   NMA ERROR CLASSES and ERROR CODES       *********************/
/*****************************************************************************/

/* The error class is encoded in the high byte of the 16-bit-result,         */
/* the error code in the low byte.                                           */

#define E_NMA_APPLICATION_OTHER         0x0100
#define E_NMA_APPLICATION_UNREACHABLE   0x0101

#define E_NMA_RESOURCE_OTHER            0x0200
#define E_NMA_RESOURCE_MEM_UNAVAILABLE  0x0201

#define E_NMA_SERV_OTHER                0x0300
#define E_NMA_SERV_OBJ_STATE_CONFLICT   0x0301
#define E_NMA_SERV_OBJ_CONSTR_CONFLICT  0x0302
#define E_NMA_SERV_PARAM_INCONSIST      0x0303
#define E_NMA_SERV_ILLEGAL_PARAM        0x0304
#define E_NMA_SERV_PERM_INTERN_FAULT    0x0305

#define E_NMA_USR_OTHER                 0x0400
#define E_NMA_USR_DONT_WORRY_BE_HAPPY   0x0401
#define E_NMA_USR_MEM_UNAVAILABLE       0x0402

#define E_NMA_ACCESS_OTHER              0x0500
#define E_NMA_ACCESS_OBJ_ACC_UNSUP      0x0501
#define E_NMA_ACCESS_OBJ_NON_EXIST      0x0502
#define E_NMA_ACCESS_OBJ_ACCESS_DENIED  0x0503
#define E_NMA_ACCESS_HARDWARE_FAULT     0x0504
#define E_NMA_ACCESS_TYPE_CONFLICT      0x0505

#define E_NMA_CRL_OTHER                 0x0600
#define E_NMA_CRL_INVALID_ENTRY         0x0601
#define E_NMA_CRL_NO_CRL_ENTRY          0x0602
#define E_NMA_CRL_INVALID_CRL           0x0603
#define E_NMA_CRL_NO_CRL                0x0604
#define E_NMA_CRL_WRITE_PROTECTED       0x0605
#define E_NMA_CRL_NO_ENTRY_FOUND        0x0606
#define E_NMA_CRL_NO_MULT_VFD_SUPP      0x0607

#define E_NMA_OTHER                     0x0700

/*****************************************************************************/
/*************   NMA ERROR CLASSES and ERROR CODES       *********************/
/*****************************************************************************/

/* The error class is encoded in the high byte of the 16-bit-result,         */
/* the error code in the low byte.                                           */

/* --- Reason codes of system management ----------------------------------- */
#define E_SM_INVALID_STATE         0x01    /* Remote error invalid state              */
#define E_SM_WRONG_PD_TAG          0x02    /* Remote error PD Tag not match           */
#define E_SM_WRONG_DEVICE_ID       0x03    /* Remote error device id not match        */
#define E_SM_WRITE_FAILED          0x04    /* Remote error SMIB object write failed   */
#define E_SM_SM_NOT_TO_OP          0x05    /* mote error starting SM operational    */

#define E_SM_DLL_NO_RESOURCE       0x10    /* DLL error insufficient resources        */
#define E_SM_DLL_QUEUE_FULL        0x11    /* DLL error send queue full               */
#define E_SM_DLL_TIMEOUT           0x12    /* DLL error timeout                       */
#define E_SM_DLL_UNKNOWN           0x13    /* DLL error unspecified reason            */

#define E_SM_SERV_NOT_SUPPORTED    0x20    /* Service not supported                   */
#define E_SM_PARAM_IV              0x21    /* Parameter invalid                       */
#define E_SM_NR_TO_SET_PD_TAG      0x22    /* No response to SET_PD_TAG               */
#define E_SM_NR_TO_WHO_HAS_PD_TAG  0x23    /* No response to WHO_HAS_PD_TAG           */
#define E_SM_NR_TO_SET_ADDR        0x24    /* No response to SET_ADDR                 */
#define E_SM_NR_TO_IDENTIFY        0x25    /* No response to IDENTIFY                 */
#define E_SM_NR_TO_ENABLE_SM_OP    0x26    /* No response to ENABLE_SM_OP             */
#define E_SM_NR_TO_CLEAR_ADDRESS   0x27    /* No response to CLEAR_ADDRESS            */
#define E_SM_MULTIPLE_TAGS         0x28    /* Multiple response to WHO_HAS_PD_TAG     */
#define E_SM_TAG_WHO_HAS_PD_TAG    0x29    /* Non-Matching PD-TAG for WHO_HAS_TAG_PDU */
#define E_SM_PD_TAG_IDENTIFY       0x2a    /* Non-Matching PD-TAG for IDENTIFY        */
#define E_SM_DEV_ID_IDENTIFY       0x2b    /* Non-Matching DEV_ID for IDENTIFY        */


/* --- Error codes of system management ------------------------------------ */
#define E_SM_CONFIGURATION         0x31    /* configuration data false      */
#define E_SM_ALREADY_CONFIGURED    0x32    /* SM is already configured      */
#define E_SM_LIST_TO_LONG          0x33    /* list has to many elements     */
#define E_SM_ALREADY_STARTED       0x34    /* time master is activated      */
#define E_SM_ALREADY_STOPPED       0x35    /* time master is stopped        */
#define E_SM_TI_NOT_SET            0x36    /* clock interval is not set     */

#define E_SM_LOCAL_ERROR           0x41    /* local SM error occured       */
#define E_SM_NO_RESOURCE           0x42    /* no SM ressources available    */
#define E_SM_WRONG_ADDRESS         0x43    /* wrong source address          */

#define E_SM_ENCODING              0x51    /* encoding failed               */
#define E_SM_ILLEGAL_PDU           0x52    /* wrong type of SM pdu          */



/*****************************************************************************/
/*************        ERROR DATA STRUCTURES      *****************************/
/*****************************************************************************/

#define MAX_ERROR_DESCR_LENGTH    _NAME_LENGTH(ERROR_DESCR_LENGTH)

/* --- standard error data structure --------------------------------------- */
typedef struct _T_ERROR
{
  USIGN16  class_code;                             /* class and code         */
  INT16    add_detail;                             /* additional detail      */
  STRINGV  add_description[MAX_ERROR_DESCR_LENGTH];/* additional description */

} T_ERROR;


/* --- System-Management error data structure -------------------------------*/
typedef struct _T_SM_ERROR
{
  USIGN8    rc;                                  /* reason code              */
  USIGN8    dummy;                               /* alignment byte           */
  USIGN16   add_detail;                          /* additional detail        */

} T_SM_ERROR;

/*****************************************************************************/
/*************      EXCEPTION STRUCTURES         *****************************/
/*****************************************************************************/

typedef struct _T_EXCEPTION
{
  USIGN8  task_id;            /* task identifier in which execption occurs  */
  USIGN8  modul_id;           /* modul identifier in which execption occurs */
  USIGN16 line;               /* line in which execption occurs             */
  USIGN16 class_code;         /* exception class and code                   */
  USIGN16 add_detail;         /* addition detail                            */
} T_EXCEPTION;

#pragma pack(pop)

#endif /* __FF_ERROR_H__ */

