/*---------------------------------------------------------------------------*/
/*  Filename    : ff_if.h                                                    */
/*                                                                           */
/*  Description : This file contains the FF specific header files, defines   */
/*                and data types                                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef __FF_IF_H__
#define __FF_IF_H__

#pragma pack(push, 2)

#include "fbapi_type.h"
#include "ff_conf.h"
#include "ff_error.h"
#include "ff_fms.h"
#include "ff_nma.h"
#include "ff_sm.h"


/*****************************************************************************/
/* INTERFACE DEFINES                                                         */
/*****************************************************************************/

/* --- service primitives -------------------------------------------------- */
#define REQ                 0x00   /* request primitive                      */
#define CON                 0x01   /* confirmation primitive                 */
#define IND                 0x02   /* indication primitive                   */
#define RES                 0x03   /* response primitive                     */


/* --- layer identifiers --------------------------------------------------- */
#define USR                 0x01      /* identifier USER                     */
#define FMS                 0x02      /* identifier FMS                      */
#define FAS                 0x03      /* identifier FAS                      */
#define DLL                 0x04      /* identifier DLL                      */
#define NMA                 0x05      /* identifier NMA                      */
#define SM                  0x06      /* identifier SM                       */
#define FMS_USR             0x07      /* identifier FMS-USER                 */
#define NMA_USR             0x08      /* identifier NMA-USER                 */
#define SM_USR              0x09      /* identifier SM-USER                  */

#define TRACE_ID_DLL        1
#define TRACE_ID_MSG        2

/*****************************************************************************/
/* FIELDBUS SERVICE DESCRIPTION BLOCK                                        */
/*****************************************************************************/

typedef struct _T_FIELDBUS_SERVICE_DESCR
{
  USIGN16     comm_ref;                /* communication reference            */
  USIGN8      layer;                   /* layer                              */
  USIGN8      service;                 /* service identifier                 */
  USIGN8      primitive;               /* service primitive                  */
  INT08       invoke_id;               /* invoke id                          */
  INT16       result;                  /* service result (POS or NEG)        */

} T_FIELDBUS_SERVICE_DESCR;

#pragma pack(pop)

#endif /* __FF_IF_H__ */

