/*---------------------------------------------------------------------------*/
/*                                                                           */
/*  Filename    : ff_fms.h                                                   */
/*                                                                           */
/*  Description : This file contains the types and defines for using FMS     */
/*                services.                                                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef __FF_FMS_H__
#define __FF_FMS_H__

#pragma pack(push, 2)

/*****************************************************************************/
/*                                                                           */
/*                             D E F I N E S                                 */
/*                                                                           */
/*****************************************************************************/


/*****************************************************************************/
/*************   FMS STRING LENGTH CONSTANTS             *********************/
/*****************************************************************************/

#define MAX_VFD_STRING_LENGTH           _NAME_LENGTH(VFD_STRING_LENGTH)

#define MAX_ACCESS_NAME_LENGTH          _NAME_LENGTH(ACCESS_NAME_LENGTH)
#define MAX_OBJECT_NAME_LENGTH          _NAME_LENGTH(OBJECT_NAME_LENGTH)
#define MAX_EXTENSION_LENGTH            _NAME_LENGTH(EXTENSION_LENGTH)
#define MAX_EXECUTION_ARGUMENT_LENGTH   _NAME_LENGTH(EXECUTION_ARGUMENT_LENGTH)


/*****************************************************************************/
/*************   FMS SERVICE IDENTIFIERS                 *********************/
/*****************************************************************************/

/* --- remote FMS services ------------------------------------------------- */
#define FMS_INITIATE                   0

#define FMS_READ                       4
#define FMS_WRITE                      5
#define FMS_GET_OD                     6

#define FMS_ABORT                     38
#define FMS_REJECT                    39

#define FMS_GEN_INIT_DOWNL_SEQ        61
#define FMS_GEN_DOWNL_SEG             62
#define FMS_GEN_TERM_DOWNL_SEQ        63

/*****************************************************************************/
/*************   ACCESS SPECIFICATION                    *********************/
/*****************************************************************************/

/* --- codes for object access mode, to be used as tag in T_ACC_SPEC-------- */
/* --- fixed size ---------------------------------------------------------- */
#define ACCESS_INDEX                   0    /* access by index               */


/* --- codes for named access specification, to be used for od services only */
/* --- fixed size ---------------------------------------------------------- */
#define  INDEX_ACCESS                  1    /* access by index               */


/*****************************************************************************/
/*************   ACCESS PROTECTION                       *********************/
/*****************************************************************************/

/* --- variable and variable list objects ---------------------------------- */

#define  FMS_ACC_READ             0x0001          /* Read with password      */
#define  FMS_ACC_WRITE            0x0002          /* Write with password     */


/*****************************************************************************/
/*************   FMS CONTEXT MANAGEMENT                  *********************/
/*****************************************************************************/

#define DETAIL_LENGTH                 16      /* length of abort detail      */
#define FEAT_SUPP_LEN                  8      /* length of supp. feat. field */

/*****************************************************************************/
/*************   VFD SUPPORT MANAGEMENT                  *********************/
/*****************************************************************************/

/* --- logical status of the communcation ---------------------------------- */
#define STATE_CHANGES_ALLOWED          0
#define LIMITED_SERVICES_PERMITTED     2

/* --- physical status of the application device --------------------------- */
#define OPERATIONAL                    0
#define PARTIALLY_OPERATIONAL          1
#define INOPERABLE                     2
#define NEEDS_COMMISSIONING            3


/*****************************************************************************/
/*************   OD-MANAGEMENT                           *********************/
/*****************************************************************************/

/* --- OD-states ----------------------------------------------------------- */
#define OD_LOADABLE                    2
#define OD_READY                       3
#define OD_LOADING_NON_INTERACTING     4
#define OD_LOADING_INTERACTING         5
#define OD_NOT_EXISTENT                6

/* --- indices in OD type description for standard data types -------------- */
#define BOOLEAN                        1
#define INTEGER8                       2
#define INTEGER16                      3
#define INTEGER32                      4
#define UNSIGNED8                      5
#define UNSIGNED16                     6
#define UNSIGNED32                     7
#define FLOATING_POINT                 8
#define VISIBLE_STRING                 9
#define OCTET_STRING                  10
#define DATE_TYPE                     11
#define TIME_OF_DAY                   12
#define TIME_DIFFERENCE               13
#define BIT_STRING                    14

/* --- indices in OD type description for extended standard data types ----- */
#define TIME_VALUE                    21
#define DOUBLE_FLOAT                  27
#define ISO_10646_STRING              28


/* --- codes of FMS objects ------------------------------------------------ */
#define  NULL_OBJECT                   0    /* null object                   */
#define  OD_OBJECT                     1    /* OD object                     */
#define  DOMAIN_OBJECT                 2    /* domain object                 */
#define  INVOCATION_OBJECT             3    /* program invocation object     */
#define  EVENT_OBJECT                  4    /* event object                  */
#define  TYPE_OBJECT                   5    /* basic data type description   */
#define  TYPE_STRUCT_OBJECT            6    /* structured data type descript.*/
#define  SIMPLE_VAR_OBJECT             7    /* simple variable object        */
#define  ARRAY_OBJECT                  8    /* array object                  */
#define  RECORD_OBJECT                 9    /* record object                 */
#define  VAR_LIST_OBJECT              10    /* variable list object          */

#define  VAR_OBJECT                   11    /* class of all variable objects */
#define  ALL_OBJECT                   12    /* super class of all objects    */


/*****************************************************************************/
/*************   VARIABLE ACCESS                         *********************/
/*****************************************************************************/

/* --- tag values for type description (T_TYPE_DESCR)----------------------- */
#define SIMPLE_TYPE                    1
#define ARRAY_TYPE                     2
#define RECORD_TYPE                    3


/*****************************************************************************/
/*                                                                           */
/*                       D A T A   S T R U C T U R E S                       */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************/
/*************   ACCESS CONTROL                          *********************/
/*****************************************************************************/

typedef struct _T_ACCESS
{
  USIGN8     pass_word;                 /* password                          */
  USIGN8     acc_groups;                /* access groups                     */
  USIGN16    acc_right;                 /* access rights                     */

} T_ACCESS;


/*****************************************************************************/
/*************   ACCESS SPECIFICATION                    *********************/
/*****************************************************************************/

/* --- standard access specification data structure (fixed size) ----------- */
typedef struct _T_ACC_SPEC
{
  USIGN8     tag;                       /* id of the access specification    */
  USIGN8     dummy;                     /* alignment byte                    */
  union
  {
    USIGN16  index;                          /* access by index              */
    STRINGV  name[MAX_ACCESS_NAME_LENGTH];   /* access by symbolic name      */
  } id;

} T_ACC_SPEC;

/* --- dynamic access specification data structure (used in T_PI_CR8_REQ
       and in T_VAR_DEFINE_VAR_LIST_REQ data structure (dynamic size) ----- */
typedef struct _T_DYN_ACC_SPEC
{
   USIGN8     tag;                    /* id of the access specification      */
   USIGN8     length;                 /* length of access specification      */
/* USIGN8     acc_spec[length]           access specification (index / name) */

} T_DYN_ACC_SPEC;


/*****************************************************************************/
/*************   FMS CONTEXT MANAGEMENT                  *********************/
/*****************************************************************************/

/* --- Initiate-Service -----------------------------------------------------*/
typedef struct _T_CTXT_INIT_REQ
{
  USIGN32   dl_address;                 /* DL address                        */
  INT16     od_version;                 /* od version                        */
  USIGN8    profile_number[2];          /* profile number                    */
  FB_BOOL   protection;                 /* access protection                 */
  USIGN8    password;                   /* password                          */
  USIGN8    access_groups;              /* access groups                     */
  FB_BOOL   indicate_buffer_sent;       /* indicate buffer is sent           */

} T_CTXT_INIT_REQ;


typedef struct _T_CTXT_INIT_CNF
{
  INT16     od_version;                 /* od version                        */
  USIGN8    profile_number[2];          /* profile number                    */
  FB_BOOL   protection;                 /* access protection                 */
  USIGN8    password;                   /* password                          */
  USIGN8    access_groups;              /* access groups                     */
  USIGN8    dummy;                      /* alignment byte                    */

} T_CTXT_INIT_CNF;


typedef struct _T_CTXT_INIT_ERR_CNF
{
  USIGN16 class_code;                   /*  error class, error code          */
  USIGN8  snd_len;                      /*  max pdu size to send             */
  USIGN8  rcv_len;                      /*  max pdu size to receive          */
  USIGN8  supported_features[FEAT_SUPP_LEN];   /*  supported features        */

} T_CTXT_INIT_ERR_CNF;


/*--- abort service ---------------------------------------------------------*/
typedef struct _T_CTXT_ABORT_REQ
{
  FB_BOOL  local;                       /* local or remote detected          */
  USIGN8   abort_id;                    /* abort identifier USR, FMS, ...    */
  USIGN8   reason;                      /* abort reason code                 */
  USIGN8   detail_length;               /* length of abort detail            */
  USIGN8   detail[DETAIL_LENGTH];       /* detail information                */

} T_CTXT_ABORT_REQ;


/*--- reject service --------------------------------------------------------*/
typedef struct _T_CTXT_REJECT_IND
{
  FB_BOOL   detected_here;              /* local or remote detected          */
  USIGN8    orig_invoke_id;             /* original invoke ID                */
  USIGN8    pdu_type;                   /* reject PDU types                  */
  USIGN8    reject_code;                /* reject code                       */

} T_CTXT_REJECT_IND;



/*****************************************************************************/
/*************   VARIABLE ACCESS MANAGEMENT              *********************/
/*****************************************************************************/

typedef struct _T_SIMPLE_TYPE
{
   USIGN16    data_type_index;          /* index of data type                */
   USIGN8     length;                   /* size of data type                 */
   USIGN8     dummy;                    /* alignment byte                    */
}  T_SIMPLE_TYPE;


typedef struct _T_ARRAY_TYPE
{
   USIGN16    data_type_index;          /* index of data type                */
   USIGN8     length;                   /* size of data type                 */
   USIGN8     no_of_elements;           /* number of data types              */
}  T_ARRAY_TYPE;


typedef struct _T_RECORD_TYPE
{
   USIGN8        no_of_elements;                  /* number of record elem.  */
   USIGN8        dummy;                           /* alignment byte          */
   T_SIMPLE_TYPE simple[MAX_VAR_RECORD_ELEMENTS]; /* list of simple types    */
}  T_RECORD_TYPE;


typedef struct _T_TYPE_DESCR
{
   USIGN8     tag;                      /* type description identifier       */
   USIGN8     dummy;                    /* alignment byte                    */
   union
   {
     T_SIMPLE_TYPE  simple;             /* simple type                       */
     T_ARRAY_TYPE   array;              /* array type                        */
     T_RECORD_TYPE  record;             /* record type                       */
   } id;
}  T_TYPE_DESCR;



/*--- VARIABLE-OBJECT-DESCRIPTIONS ------------------------------------------------*/

/* --- Read-Service ---------------------------------------------------------------*/
typedef struct _T_VAR_READ_REQ
{
   T_ACC_SPEC acc_spec;                        /* access specification             */
   USIGN8     subindex;                        /* subindex                         */
   USIGN8     dummy;                           /* alignment byte                   */

} T_VAR_READ_REQ;


typedef struct _T_VAR_READ_CNF
{
   USIGN8  dummy;                              /* alignment byte                   */
   USIGN8  length;                             /* length of values in bytes        */
/* USIGN8  value[lenght];                         list of data                     */

} T_VAR_READ_CNF;



/*--- Write-Service ---------------------------------------------------------------*/
typedef struct _T_VAR_WRITE_REQ
{
   T_ACC_SPEC acc_spec;                             /* access specification        */
   USIGN8     subindex;                             /* subindex                    */
   USIGN8     length;                               /* # number of values in bytes */
/* USIGN8     value[length];                           list of values              */

} T_VAR_WRITE_REQ;



/*--- ACCESS to OBJECT-DESCRIPTIONS ---------------------------------------- */

/* --- OD-PACKED-OBJECT-DESCRIPTION ---------------------------------------- */
typedef struct _T_PACKED_OBJECT_DESCR
{
   USIGN8    length;                   /* length of packed object description*/
/* USIGN8    packed_obj_descr[length];    packed object description          */

} T_PACKED_OBJECT_DESCR;


/* --- Get-OD-Service -------------------------------------------------------*/
typedef struct _T_GET_OD_REQ
{
   FB_BOOL    format;              /* TRUE = long format / FALSE = short    */
   USIGN8     dummy;                /* alignment byte                        */
   T_ACC_SPEC acc_spec;             /* access specification                  */

} T_GET_OD_REQ;


typedef struct _T_GET_OD_CNF
{
   FB_BOOL                more_follows;                   /* further object descr. follow */
   USIGN8                 no_of_od_descr;                 /* # of object description      */
/* T_PACKED_OBJECT_DESCR  obj_descr_list[no_of_od_descr];    list of object descriptions  */

} T_GET_OD_CNF;

/*--- Domain-Download-Services ----------------------------------------------*/
/*--- Generic-Domain-Download-Services --------------------------------------*/
/*--- Domain-Upload-Services   ----------------------------------------------*/

typedef struct _T_DOM_REQ
{
  T_ACC_SPEC  acc_spec;                    /* access specification          */
} T_DOM_REQ;


typedef struct _T_GEN_DNL_SEG_REQ
{
   T_ACC_SPEC  acc_spec;                   /* access specification          */
   FB_BOOL     more_follows;               /* more_follows                  */
   USIGN8      data_len;                   /* data length                   */
/* USIGN8      data[data_len];                list of data                  */
} T_GEN_DNL_SEG_REQ;


typedef struct _T_GEN_TERM_DNL_CNF
{
   FB_BOOL     final_result;                /* final result                  */
   USIGN8      dummy;                       /* alignment                     */
} T_GEN_TERM_DNL_CNF;


typedef struct _T_REQUEST_DOM_REQ
{
   T_ACC_SPEC  acc_spec;                    /* access specification          */
   USIGN8      dummy;                       /* alignment                     */
   USIGN8      add_info_length;             /* length of add. information    */
/* STRINGV     add_info[add_info_length];      additional information        */
} T_REQUEST_DOM_REQ;


#pragma pack(pop)

// CPMHACK
#define  MAX_OBJECT_DESC              13    /* Maximum FMS code value  */

#endif /* __FF_FMS_H__ */
