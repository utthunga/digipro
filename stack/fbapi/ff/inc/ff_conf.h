/*---------------------------------------------------------------------------*/
/*  Filename    : ff_conf.h                                                  */
/*                                                                           */
/*  Description : This file contains the FF configuration and                */
/*                implementation definitions                                 */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef __FF_CONF_H__
#define __FF_CONF_H__


/*---------------------------------------------------------------------------*/
/*               FMS Implementation Constants                                */
/*                                                                           */
/* The constants given below define the sizes of various data structures in  */
/* the protocol software and thus influence memory consumption.              */
/*                                                                           */
/* NOTE: Do not change the following constants without recompiling the       */
/*       the protocol software on the communication controller               */
/*---------------------------------------------------------------------------*/

/* -- constants of internal sizes of byte arrays --------------------------- */
#define VFD_STRING_LENGTH             32  /* max length of the VFD string    */

#define ACCESS_NAME_LENGTH            32  /* max length for name adressing   */
#define OBJECT_NAME_LENGTH            16  /* max length of object name       */
#define EXTENSION_LENGTH               9  /* max length of object extension  */
#define EXECUTION_ARGUMENT_LENGTH     32  /* max length of exec. argument    */
#define ERROR_DESCR_LENGTH            32  /* max length of error descript.   */

#define MAX_VAR_RECORD_ELEMENTS       50  /* max no of record elements       */
#define MAX_VAR_LIST_ELEMENTS         50  /* max no of variable list elements*/
#define MAX_DOM_LIST_ELEMENTS         10  /* max no of domain list elements  */

#define MAX_COMREF                   128  /* max supported CRs               */
#define MAX_VFD                        5  /* max supported VFDs              */



#define MAX_CHANNEL_ID                10  /* max. FBK Host device channels     */

/*---------------------------------------------------------------------------*/
/* MACRO TO CALCULATE MAX_xxxx_NAME_LENGTH                                   */
/*                                                                           */
/* This macro calculates the internal sizes of byte arrays in a way that the */
/* desired alignment on byte, word or long word boundaries is achieved.      */
/* The alignment is specified by the constant ALIGNMENT (e. g. longword = 4) */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#define ALIGNMENT                  0x02   /* alignment on word boundary      */

#define _NAME_LENGTH(length) (length + ALIGNMENT - (length % ALIGNMENT))

#endif /* __FF_CONF_H__ */
