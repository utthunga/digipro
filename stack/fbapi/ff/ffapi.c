/*  Filename    : ffapi.c                                                    */
/*                                                                           */
/*  Description : The module provides processing for FF API calls            */
/*                                                                           */
/*****************************************************************************/

#include "keywords.h"

INCLUDES

#ifndef API_LINUX
#   include <windows.h>
#   include <winbase.h>
#   include <crtdbg.h>
#   include <initguid.h>
#   include <ws2bth.h>
#   include <tchar.h>
#else
#   include <errno.h>
#   include "api_linux.h"
#   include "linux_win32_funcs.h"
#   include "serial_port.h"
#endif
#include <wchar.h>
#include "fbapi_if.h"
#include "protocol_if.h"
#include "ff_if.h"
#include "trace.h"

GLOBAL_DEFINES

LOCAL_DEFINES

EXPORT_TYPEDEFS

LOCAL_TYPEDEFS

FUNCTION_DECLARATIONS

EXPORT_DATA

IMPORT_DATA

LOCAL_DATA


/******************************************************************************** */

FUNCTION INT16 ffapi_check_send_req
         (
          IN  VOID                      *p_sdb,
          OUT USIGN16                   *sdb_len,
          IN  VOID                      *data_ptr,
          OUT INT16                     *sdbdata_len
         )

/*----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function checks the request parameter for an FF service


INPUT:  sdb_ptr                  -> pointer to SERVICE-DESCRIPTION-BLOCK
        sdb_len                  -> length of sdb
        data_ptr                 -> pointer to service specific data


Possible return values:

- E_OK                           -> no error occured

- E_IF_FATAL_ERROR               -> unrecoverable error in communication stack
- E_IF_INVALID_LAYER             -> invalid layer
- E_IF_INVALID_SERVICE           -> invalid service identifier
- E_IF_INVALID_PRIMITIVE         -> invalid service primitive
- E_IF_INVALID_COMM_REF          -> invalid communication reference
----------------------------------------------------------------------------*/
{
LOCAL_VARIABLES

    INT16                     ret_val;   /* return value   */
    T_FIELDBUS_SERVICE_DESCR  *sdb_ptr;

FUNCTION_BODY

    ASSERT(p_sdb);
    sdb_ptr      = (T_FIELDBUS_SERVICE_DESCR*) p_sdb;
    ret_val      = E_OK;
    *sdbdata_len = 0;
    *sdb_len     = sizeof(*sdb_ptr);

    /* check communication reference --------------------------------------- */
    if (sdb_ptr->comm_ref > MAX_COMREF)
    {
        return (E_IF_INVALID_COMM_REF);
    }

    /* check Service-Primitive --------------------------------------------- */
    if ((sdb_ptr->primitive != REQ) && (sdb_ptr->primitive != RES))
    {
        return (E_IF_INVALID_PRIMITIVE);
    }

    /* set the result parameter to POS if a request is send ---------------- */
    if (sdb_ptr->primitive == REQ)
    {
        sdb_ptr->result = POS;
    }

    /* get service specific data length ------------------------------------ */
    switch (sdb_ptr->layer)
    {
        case FMS:
        {
            ret_val = fmsgdl_get_data_len(sdb_ptr->result,
                                        sdb_ptr->service,
                                        sdb_ptr->primitive,
                                        data_ptr,
                                        sdbdata_len
                                       );
            break;
        }
        case NMA:
        {
            ret_val = nmagdl_get_data_len(sdb_ptr->result,
                                        sdb_ptr->service,
                                        sdb_ptr->primitive,
                                        data_ptr,
                                        sdbdata_len
                                       );
            break;
        }
        case SM:
        {
            ret_val = smgdl_get_data_len(sdb_ptr->result,
                                       sdb_ptr->service,
                                       sdb_ptr->primitive,
                                       data_ptr,
                                       sdbdata_len
                                      );
            break;
        }
#ifdef DIAG_SUPP
        case DIAG:
        {
            ret_val = diagdl_get_data_len(sdb_ptr->result,
                                        sdb_ptr->service,
                                        sdb_ptr->primitive,
                                        data_ptr,
                                        sdbdata_len
                                       );
            break;
        }
#endif
        default:
        {
            ret_val = E_IF_INVALID_LAYER;
            break;
        }
    }

    return (ret_val);
}

/******************************************************************************** */

FUNCTION USIGN16 ffapi_get_sdb_len (VOID)

/*----------------------------------------------------------------------------
FUNCTIONAL_DESCRIPTION
This function calculates the size of FF service description block.

*/
{
LOCAL_VARIABLES

FUNCTION_BODY

    return ((USIGN16) sizeof(T_FIELDBUS_SERVICE_DESCR));
}
