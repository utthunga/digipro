/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: hlsstate.c $
 * $Archive: /SwRICode/HMaster/src/hlsstate.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   HART Link Layer state processing.
 * The state machine in this file manages events which drive
 * the HART Master Data Link Layer.
 * These are internal functions, not for general use.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: hlsstate.c $
 * 
 * *****************  Version 8  *****************
 * User: Kholladay    Date: 5/15/98    Time: 4:48p
 * Updated in $/SwRICode/HMaster/src
 * Fix reentry problem.
 *
 * *****************  Version 7  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:44p
 * Updated in $/SwRICode/HMaster/src
 * Reset hart serial port during flush of queues.
 *
 * *****************  Version 6  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/06/98    Time: 3:41p
 * Updated in $/SwRICode/HMaster/src
 * Add Gap timer support
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/03/98    Time: 4:53p
 * Updated in $/SwRICode/HMaster/src
 * Correct timer for transition from Enabled to Using
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/

#include <string.h>
#include "hartmdll.h"

#include <stdio.h>
#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "stacktrace.h"

/***************************************************************************
 * Private Definitions
 ***************************************************************************/

/*
 * States for the HART link layer state machine.
 */
#define LSTATE_WATCHING  0
#define LSTATE_ENABLED   1
#define LSTATE_USING     2

/*
 * Message Types.
 */
#define MSG_STX          0    /* Messages from this master  */
#define MSG_ACK          1    /* Slave acks to this master  */
#define MSG_BACK         2    /* Burst acks to this master  */
#define MSG_OSTX         3    /* Messages from other master */
#define MSG_OACK         4    /* Slave acks to other master */
#define MSG_OBACK        5    /* Burst acks to other master */
#define MSG_ERR          6    /* Can't determine the type   */

/***************************************************************************
 * Private Data Types for this file
 ***************************************************************************/

/*
 * Function types for the state processor.
 */
typedef BYTE (*PINSTATE)( PHART_CTRL, BYTE );
typedef void (*PTOSTATE)( PHART_CTRL );


/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/

static BYTE watching_in_state( PHART_CTRL phc, BYTE cEvent );
static BYTE enabled_in_state( PHART_CTRL phc, BYTE cEvent );
static BYTE using_in_state( PHART_CTRL phc, BYTE cEvent );

static void enabled_to_state( PHART_CTRL phc );

static FSTAT check_busy_retry( PHART_CTRL phc );
static BYTE check_message( PHART_CTRL phc );
static BYTE handle_unwanted_msg( PHART_CTRL phc );
static void copy_response_to_q( PHART_CTRL phc, BYTE cNewQState );
static void send_queued_message( PHART_CTRL phc );
static void process_gap_timeout( PHART_CTRL phc, WORD wTime );

/***************************************************************************
 * Private variables for this file
 ***************************************************************************/

/*
 * Table of functions to be called while in each state.
 */
static const PINSTATE in_state[ ] =
{
     watching_in_state,        /* LSTATE_WATCHING */
     enabled_in_state,         /* LSTATE_ENABLED */
     using_in_state            /* LSTATE_USING */

};


/**************************************************************************
 * FUNCTION: hls_init_state_machine
 *
 * DESCRIPTION:
 *   This function initializes the hart link layer state machine that
 * is using the passed control structure.
 *
 * ARGUMENTS:
 *   phc       ptr to hart control structure
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void hls_init_state_machine( PHART_CTRL phc )
{
     phc->cState = LSTATE_WATCHING;
     phc->bBurst = FALSE;
     phc->pCommq = 0;

     /*
      * Init the receive state machine.
      */
     rcv_init_state_machine( &phc->rc );

     /*
      * Start the timer for the watching state.
      */
     phc->pfSetTimer( phc->wRT1 );
}

#ifdef DEBUG
const char* eventStr(BYTE cEvent)
{
    switch (cEvent) {
    case EVENT_RCVD_MSG: return "RCVD_MSG";
    case EVENT_XMIT_DONE: return "XMIT_DONE";
    case EVENT_TIMEOUT: return "TIMEOUT";
    case EVENT_XMIT_PENDING: return "XMIT_PENDING";
    default: return "?";
    }
}

const char *stateStr( BYTE state )
{
    switch( state ) {
    case LSTATE_WATCHING: return "watching";
    case LSTATE_ENABLED:  return "enabled";
    case LSTATE_USING:    return "using";
    }
    return "?";

}
#endif // DEBUG


/**************************************************************************
 * FUNCTION: hls_state_machine
 *
 * DESCRIPTION:
 *   State machine for the HART data link layer.
 * NOTE: This function is called for four events
 *  EVENT_TIMEOUT
 *  EVENT_RCVD_MSG
 *  EVENT_XMIT_DONE
 *  EVENT_XMIT_PENDING
 *  All of these are called from within interrupts EXCEPT XMIT_PENDING
 * which is called while interrupts are disabled.
 *
 * ARGUMENTS:
 *   phc       ptr to hart control structure
 *   cEvent    event that caused a pass through the state machine.
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void hls_state_machine( PHART_CTRL phc, BYTE cEvent )
{
     BYTE      cNewState;
#ifdef DEBUG
     BYTE      cOldState = phc->cState;
     dbgPrint("hls_state_machine( ,%s)\n", eventStr(cEvent));
#endif

     /*
      * perform any "in state" actions for this event
      */
     cNewState = in_state[ phc->cState ]( phc, cEvent );
#ifdef DEBUG
    dbgPrint("  %s -> %s\n", stateStr( cOldState ), stateStr( cNewState ));
#endif

     if( cNewState != phc->cState )
     {
          phc->cState = cNewState;
          if( cNewState == LSTATE_ENABLED )
          {
               enabled_to_state( phc );
          }
     }
}


/**************************************************************************
 * FUNCTION: hls_process_received_char
 *
 * DESCRIPTION:
 *   Called by the processor serial port interrupt handler when a character
 * is received.
 *
 * ARGUMENTS:
 *   cData          the received data byte.
 *   cQuality       the quality of the data byte.  This will be one of:
 *                  STATUS_OK, PARITY_ERROR, FRAMING_ERROR, OVERRUN_ERROR
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void hls_process_received_char( PHART_CTRL phc, BYTE cData, BYTE cQuality )
{
     BYTE cStatus;

     /*
      * Process the received character.
      */
     cStatus = rcv_state_machine( &phc->rc, cData, cQuality );
     if( !cStatus )
     {
          /*
           * A message of some sort has been completed.
           */
          hls_state_machine( phc, EVENT_RCVD_MSG );

          /*
           * Get ready for the next one.
           */
          rcv_init_state_machine( &phc->rc );
     }
     else
     {
          if( phc->rc.bGapActive )
          {
               /*
                * Expecting more input.
                */
               phc->pfSetTimer( TGAP );
          }
          else
          {
               /*
                * Restart the timer.
                */
               phc->pfSetTimer( 0 );
          }
     }
}


/***************************************************************************
 *
 * Hereafter lie all of the state functions.
 *
 * For all state functions, the timer interrupt has been turned off.
 *
 ***************************************************************************/


static BYTE watching_in_state( PHART_CTRL phc, BYTE cEvent )
{
     BYTE cNewState = LSTATE_WATCHING;

     switch( cEvent )
     {
     case EVENT_TIMEOUT:
          if( phc->rc.bGapActive )
          {
            dbgPrint("process_gap_timeout wRT1 (%d)\n", phc->wRT1);
			process_gap_timeout( phc, phc->wRT1 );
          }
          else if( phc->pfReadCD() )
          {
            /*
            * Someone has carrier on,so forget a state transition.
            */
            dbgPrint("pfSetTimer wRT1 (%d)\n", phc->wRT1);
			phc->pfSetTimer( phc->wRT1 );
          }
          else
          {
               /*
                * The bus is mine.
                */
// L&T Modifications : Real Tx. Support - start
// Commenting the printf
//				printf(" bus enabled\n");
// L&T Modifications : Real Tx. Support - end
               phc->bBurst = FALSE;
               cNewState = LSTATE_ENABLED;
          }
          break;

     case EVENT_RCVD_MSG:
          cNewState = handle_unwanted_msg( phc );
          break;

     case EVENT_XMIT_PENDING:
          break;

     case EVENT_XMIT_DONE:
          /*
           * We should never get here, but just in case, pretend we were
           * in enabled.
           */
          phc->LinkStats.wSent++;
          phc->LinkStats.dwTotal++;
          cNewState = LSTATE_USING;
          phc->pfSetTimer( phc->wRT1 );
          break;
     }

     return( cNewState );
}

static BYTE enabled_in_state( PHART_CTRL phc, BYTE cEvent )
{
     BYTE cNewState = LSTATE_WATCHING;

     switch( cEvent )
     {
     case EVENT_TIMEOUT:
          /*
           * Either the HOLD timer expired (we didn't use the bus) or
           * the Gap timer expired (someone else stepped on us).
           */
          if( phc->rc.bGapActive )
          {
               process_gap_timeout( phc, phc->wRT1 );
          }
          else
          {
               phc->pfSetTimer( 2 * phc->wRT1 );
          }
          break;

     case EVENT_RCVD_MSG:
          /*
           * Someone has been on the bus while we have been waiting for
           * something to transmit.
           */
          cNewState = handle_unwanted_msg( phc );
          break;

     case EVENT_XMIT_PENDING:
          if( !phc->pCommq )
          {
               /*
                * there is no message currently being worked on, so
                * get this new one.  Make sure the timer is OFF.
                */
               phc->pfSetTimer( -1 );
               if( !phc->pfGetQ( &phc->pCommq ) )
               {
                    phc->cRetriesLeft = phc->cRetries +1;
                    cNewState = LSTATE_ENABLED;
                    send_queued_message( phc );
               }
               else
               {
                    /*
                     * This shouldn't happen since the event says that
                     * there must be a message in the queue.  However...
                     */
                    phc->pfSetTimer( phc->wRT1 );
               }
          }
          else
          {
               /*
                * We are working on something, probably waiting for a message
                * complete or a timeout, so let the new message sit in the
                * queue.
                */
               cNewState = LSTATE_ENABLED;
          }
          break;

     case EVENT_XMIT_DONE:
          /*
           * Finished transmitting a message.
           * Go to the using state and wait for a reply.
           */
          phc->LinkStats.wSent++;
          phc->LinkStats.dwTotal++;
          cNewState = LSTATE_USING;
          phc->pfSetTimer( phc->wRT1 + phc->wExtend );
          break;
     }

     return( cNewState );
}

static BYTE using_in_state( PHART_CTRL phc, BYTE cEvent )
{
     BYTE cNewState = LSTATE_USING;
     BYTE cType;
     WORD wTime;

     switch( cEvent )
     {
     case EVENT_TIMEOUT:
          /*
           * Either the Gap timer or response timer expired.
           * The handling is the same for both since this constitutes
           * an error.
           */
          if( phc->pCommq )
          {
               phc->pCommq->cRspQuality = NORESPONSE_ERROR;
          }
          phc->LinkStats.wNoResp++;
          cNewState = LSTATE_WATCHING;
          if( phc->rc.bGapActive )
          {
               process_gap_timeout( phc, phc->wRT1 );
          }
          else if( phc->bBurst )
          {
               phc->pfSetTimer( phc->wRT1 );
          }
          else if( phc->bPrimary )
          {
               cNewState = LSTATE_ENABLED;
          }
          else
          {
               phc->pfSetTimer( RT3 );
          }
          break;

     case EVENT_RCVD_MSG:
          cType = check_message( phc );
          wTime = phc->wRT1;
          if( phc->pCommq && cType == MSG_ACK )
          {
               /*
                * This is a valid response to a command.
                * If busy or comm error, retry by letting state machine
                * go around again.
                */
               if( !check_busy_retry( phc ) )
               {
                    /*
                     * Transfer the response and mark it as complete.
                     */
                    copy_response_to_q( phc, QSTATE_DONE );
                    if( !phc->bBurst )
                    {
                         wTime = RT2;
                    }
               }
          }
          /*
           * Always go back to watching if we receive anything.
           */
          phc->pfSetTimer( wTime );
          cNewState = LSTATE_WATCHING;
          break;

     case EVENT_XMIT_PENDING:
          break;

     case EVENT_XMIT_DONE:
          /*
           * We should never get here, but just in case, pretend we were
           * in enabled.
           */
          phc->LinkStats.wSent++;
          phc->LinkStats.dwTotal++;
          phc->pfSetTimer( phc->wRT1 );
          break;
     }

     return( cNewState );
}


static void enabled_to_state( PHART_CTRL phc )
{

// L&T Modifications : Real Tx. Support - start
// Commenting the printf
// printf("enabled_to_state\n");
// L&T Modifications : Real Tx. Support - end
     if( phc->pCommq )
     {
          /*
           * There is a message already in progress.  See if any
           * retries are left.
           */
//printf(" cRetriesLeft=%d\n", phc->cRetriesLeft);
          if( phc->cRetriesLeft )
          {
//printf(" retry\n");
               send_queued_message( phc );
          }
          else
          {
               /*
                * must be dead.
                */
               copy_response_to_q( phc, QSTATE_DEAD );
          }
     }
     else if( !phc->pfGetQ( &phc->pCommq ) )
     {
          /*
           * There is a queued message to send.
           */
          phc->cRetriesLeft = phc->cRetries +1;
//printf("  send_queued_message cRetriesLeft=%d\n", phc->cRetriesLeft);
          send_queued_message( phc );
     }
     else {
// L&T Modifications : Real Tx. Support - start
// Commenting the printf
//        printf(" ets ??\n");
// L&T Modifications : Real Tx. Support - end
     }
}


/**************************************************************************
 * FUNCTION: check_busy_retry
 *
 * DESCRIPTION:
 *   Determines if the response we got has a busy or comm_error status
 * to determine if a retry is necessary at this level.
 *
 * ARGUMENTS:
 *   phc
 *
 * RETURNS:
 *   STATUS_OK      response is OK and should be transferred to queue
 *   STATUS_WORKING need to retry.
 ***************************************************************************/

static FSTAT check_busy_retry( PHART_CTRL phc )
{
     FSTAT nStatus;
     BYTE  cHeaderLength;
     BYTE  cResponseType;


     hutil_header_length( *(phc->rc.acResponse), &cHeaderLength );
     hutil_response_code_type( phc->rc.acResponse[ cHeaderLength ], &cResponseType );

     if( cResponseType == RSP_COMM_ERROR ||
         cResponseType == RSP_DEVICE_BUSY )
     {
          nStatus = STATUS_WORKING;
     }
     else
     {
          nStatus = STATUS_OK;
     }

     return( nStatus );
}


/**************************************************************************
 * FUNCTION: check_message
 *
 * DESCRIPTION:
 *   Counts message types.
 *
 * ARGUMENTS:
 *   phc
 *
 * RETURNS:
 *   the type of message
 ***************************************************************************/

static BYTE check_message( PHART_CTRL phc )
{
     BYTE cType;
     BYTE cDelimMask;
     BOOL bThisMaster;
     BOOL bPrimary;

     phc->LinkStats.dwTotal++;

     switch( phc->rc.cRspQuality )
     {
     case STATUS_OK:
          cDelimMask = phc->rc.acResponse[0] & 0x07;
          bPrimary = (*(phc->rc.acResponse +1) & 0x80);
          bThisMaster = (bPrimary == ( phc->bPrimary ? 0x80 : 0x00 ));


#if defined(linux) || defined(sparc)

        /*
        ** Trace output messages if HART_DEBUG set.
        */
        if (LOG_LEVEL_TRACE == getLog4cplusLogLevel()) 
        {
            int i;
            char d;
            switch( cDelimMask ) {
                case FRAME_BURST: d='B'; break;       /* 0x01 burst frame */
                case FRAME_STX:   d='S'; break;       /* 0x02 STX frame */
                case FRAME_ACK:   d='A'; break;       /* 0x06 ACK frame */
                default:          d='U'; break; /* undefined (can't happen) */
            }
            if (!bPrimary) d += 'a'-'A';
            printf("\n%c %02x %02x:",
                        d, phc->rc.cRspQuality, phc->rc.cRspCount);
            for (i=0; i<phc->rc.cRspCount; i++) {
                printf("%02x", phc->rc.acResponse[i]);
            }
            printf("\n");
        }
#endif

// L&T Modifications : Real Tx. Support - start
int i=0;
//printf("\n%02x %02x:", ghc.rc.cRspQuality, ghc.rc.cRspCount);
/*for (i=0; i<ghc.rc.cRspCount; i++)
{
    printf("%02x", ghc.rc.acResponse[i]);
}*/
//printf("\n");
ghc2.rc.cRspQuality = ghc.rc.cRspQuality;
ghc2.rc.cRspCount = ghc.rc.cRspCount;
/*for (i=0; i<ghc.rc.cRspCount; i++)
{
    ghc2.rc.acResponse[i] = ghc.rc.acResponse[i];
    //printf("%02x", ghc.rc.acResponse[i]);
}
printf("\n");*/
for (i=0; i<ghc2.rc.cRspCount; i++)
{
    ghc2.rc.acResponse[i] = ghc.rc.acResponse[i];
    //printf("%02x", ghc2.rc.acResponse[i]);
}
//printf("\n");
for (i=0; i<phc->rc.cRspCount; i++)
{
    ghc2.rc.acResponse[i] = phc->rc.acResponse[i];
    //printf("%02x", ghc2.rc.acResponse[i]);
}

          switch( cDelimMask )
          {
          case FRAME_BURST:        /* 0x01 burst frame */
               phc->bBurst = TRUE;
               if( bThisMaster )
               {
                    cType = MSG_BACK;
               }
               else
               {
                    cType = MSG_OBACK;
               }
               if( bPrimary )
               {
                    phc->LinkStats.wPBACK++;
               }
               else
               {
                    phc->LinkStats.wSBACK++;
               }
               /*
                * Save the burst message.
                */
               memcpy( phc->acBurstBuf, phc->rc.acResponse, phc->rc.cRspCount );
               phc->cBurstLen = phc->rc.cRspCount;
               break;

          case FRAME_STX:          /* 0x02 STX frame */
               if( bThisMaster )
               {
                    cType = MSG_STX;
               }
               else
               {
                    cType = MSG_OSTX;
               }
               if( bPrimary )
               {
                    phc->LinkStats.wPSTX++;
               }
               else
               {
                    phc->LinkStats.wSSTX++;
               }
               break;

          case FRAME_ACK:          /* 0x06 ACK frame */
               phc->bBurst = *(phc->rc.acResponse +1) & 0x40;
               if( bThisMaster )
               {
                    cType = MSG_ACK;
               }
               else
               {
                    cType = MSG_OACK;
               }
               if( bPrimary )
               {
                    phc->LinkStats.wPACK++;
               }
               else
               {
                    phc->LinkStats.wSACK++;
               }
               break;
          }
          break;

     case SOM_ERROR:
          cType = MSG_ERR;
          phc->LinkStats.wSomErr++;
          break;

     case OVERRUN_ERROR:
     case FRAMING_ERROR:
     case PARITY_ERROR:
          cType = MSG_ERR;
          phc->LinkStats.wCommErr++;
          break;

     case CHECKSUM_ERROR:
          cType = MSG_ERR;
          phc->LinkStats.wChkErr++;
          break;
     }


#if defined(linux) || defined(sparc)
    {
     const char *qstr;
     switch( phc->rc.cRspQuality )
     {
     default:                   qstr = 0; break;
     case SOM_ERROR:            qstr = "SOM"; break;
     case OVERRUN_ERROR:        qstr = "Overrun"; break;
     case FRAMING_ERROR:        qstr = "Framing"; break;
     case PARITY_ERROR:         qstr = "Parity"; break;
     case CHECKSUM_ERROR:       qstr = "Checksum"; break;
     }
    if (qstr) {

        /*
        ** Trace output messages if HART_DEBUG set.
        */
        if (LOG_LEVEL_TRACE == getLog4cplusLogLevel()) 
        {
            int i;
            printf("\nE %02x %02x:",
                        phc->rc.cRspQuality, phc->rc.cRspCount);
            for (i=0; i<phc->rc.cRspCount; i++) {
                printf("%02x", phc->rc.acResponse[i]);
            }
            printf(" %s error\n", qstr);
        }
     }
    }
#endif

     return( cType );
}


/**************************************************************************
 * FUNCTION: handle_unwanted_msg
 *
 * DESCRIPTION:
 *   called when a message is received in either the watching or enabled
 * states when we are not expecting a reply to a sent message.
 *
 * ARGUMENTS:
 *   phc
 *
 * RETURNS:
 *   state we should be in next based on type of msg recevied.
 ***************************************************************************/

static BYTE handle_unwanted_msg( PHART_CTRL phc )
{
     BYTE cNewState = LSTATE_WATCHING;
     BYTE cType;

     cType = check_message( phc );
     switch( cType )
     {
     case MSG_OACK:
          if( phc->bBurst )
          {
               phc->pfSetTimer( phc->wRT1 );
          }
          else
          {
               phc->pfSetTimer( THOLD );
               cNewState = LSTATE_ENABLED;
          }
          break;

     case MSG_OBACK:
          phc->bBurst = TRUE;
          phc->pfSetTimer( THOLD );
          cNewState = LSTATE_ENABLED;
          break;

     case MSG_BACK:
          phc->bBurst = TRUE;
     case MSG_OSTX:
     case MSG_STX:
     case MSG_ACK:
          phc->pfSetTimer( phc->wRT1 );
          break;


     case MSG_ERR:
     default:
          phc->pfSetTimer( 2 * phc->wRT1 );
          break;
     }

     return( cNewState );
}


/**************************************************************************
 * FUNCTION: copy_response_to_q
 *
 * DESCRIPTION:
 *   copies a response from the receive control structure to the queue
 * structure.
 *
 * ARGUMENTS:
 *   phc
 *   cNewQState
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void copy_response_to_q( PHART_CTRL phc, BYTE cNewQState )
{
     phc->pCommq->cRspQuality = phc->rc.cRspQuality;
     if( phc->rc.cRspCount )
     {
          phc->pCommq->cRspCount   = phc->rc.cRspCount;
          memcpy( phc->pCommq->acResponse, phc->rc.acResponse, phc->rc.cRspCount );
     }
     phc->pCommq->cQState = cNewQState;
     phc->pCommq = 0;
}


/**************************************************************************
 * FUNCTION: send_queued_message
 *
 * DESCRIPTION:
 *   sends the message in the currently active queue entry.
 *
 * ARGUMENTS:
 *   phc
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void send_queued_message( PHART_CTRL phc )
{
     rcv_init_state_machine( &phc->rc );
     phc->cRetriesLeft--;
//printf("pfSendMsg\n");
     phc->pfSendMsg( phc->cPreambles,
                      phc->pCommq->acMessage,
                      phc->pCommq->cMsgLength );
}

/**************************************************************************
 * FUNCTION: process_gap_timeout
 *
 * DESCRIPTION:
 *   resets the receive state machine and timer for a gap timeout
 *
 * ARGUMENTS:
 *   phc
 *   wTime     new time to set
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void process_gap_timeout( PHART_CTRL phc, WORD wTime )
{
     phc->LinkStats.wGapErr++;
     rcv_init_state_machine( &phc->rc );
     phc->pfSetTimer( wTime );
}
