/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: hll.c $
 * $Archive: /SwRICode/HMaster/src/hll.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   HART Master Data Link Layer user functions.
 *   These are part of the services provided by the HART Master Data
 * Link Layer module.  Since these are user functions, their prototypes
 * are in hartmdll.h.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: hll.c $
 * 
 * *****************  Version 9  *****************
 * User: Kholladay    Date: 5/15/98    Time: 4:48p
 * Updated in $/SwRICode/HMaster/src
 * Fix reentry problem.
 *
 * *****************  Version 8  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:55a
 * Updated in $/SwRICode/HMaster/src
 *
 * *****************  Version 7  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:44p
 * Updated in $/SwRICode/HMaster/src
 * Reset hart serial port during flush of queues.
 *
 * *****************  Version 6  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 11/05/97   Time: 10:16a
 * Updated in $/SwRICode/HMaster/src
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 11/05/97   Time: 8:57a
 * Updated in $/SwRICode/HMaster/src
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#include "intr6811.h"
#include <stdio.h>
#include <string.h>
#include "hartmdll.h"

/***************************************************************************
 * Global variables for a single HART state machine.
 ***************************************************************************/

/*
 * HART link layer state machine control structure.
 */
HART_CTRL    ghc;

HART_CTRL    ghc2;

/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/

/***************************************************************************
 * Private Data Types for this file
 ***************************************************************************/

/***************************************************************************
 * Private variables for this file
 ***************************************************************************/

/*
 * Queue control variables.
 */
static BYTE         gcNextMsgId = 1;
static COMMQ        gaQueueEntry[ HART_QUEUE_DEPTH ];
static PCOMMQ       gapQueue[ HART_QUEUE_DEPTH ];
static BYTE         gcQueueNextIn = 0;
static BYTE         gcQueueNextOut = 0;


/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/

static FSTAT next_queue_to_process( PCOMMQ *ppCommQ );


/**************************************************************************
 * FUNCTION: hll_flush_hart_queue
 *
 * DESCRIPTION:
 *   Flushes hart queue and resets state machine.  Use to clear unwanted
 * outstanding messages.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

int hll_flush_hart_queue( void )
{
     int ret_code = STATUS_OK;
     disable_interrupt();
     gcQueueNextOut = 0;
     gcQueueNextIn  = 0;
     memset( gapQueue, 0, sizeof( gapQueue ) );
     memset( gaQueueEntry, 0, sizeof( gaQueueEntry ) );

     /*
      * Kill anything that might be transmitting, and reset to receive conditions.
      */
     ret_code = mc_initialization();

     /*
      * Init the HART link layer state machine.
      */
     hls_init_state_machine( &ghc );
     enable_interrupt();

     return ret_code;
}


/**************************************************************************
 * FUNCTION: hll_initialize_hart
 *
 * DESCRIPTION:
 *   This function must be called during program initialization to get
 * the HART state machine started.  It must be called before any of the other
 * functions are used.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

int hll_initialize_hart( void )
{
    int ret_code = STATUS_OK;
     /*
      * Initial values for control variables.
      */
     memset( &ghc, 0, sizeof( ghc ) );

     hll_set_master( HART_SECONDARY );
     //hll_set_preambles( HART_DEFAULT_PREAMBLES );
     hll_set_preambles( HART_MINIMUM_PREAMBLES );
     hll_set_retries( HART_DEFAULT_RETRIES );

     ghc.pfGetQ     = next_queue_to_process;
     ghc.pfSendMsg  = mc_send_hart_message;
     ghc.pfSetTimer = mc_set_hart_timer;
     ghc.pfReadCD   = mc_hart_read_cd;

     ret_code = hll_flush_hart_queue();

     return ret_code;
}

/**************************************************************************
 * FUNCTION: hll_set_preambles
 *
 * DESCRIPTION:
 *   Sets the number of preambles that are used when sending a HART
 * command.  The number must fall between 3 and 25 or it is adjusted.
 *
 * ARGUMENTS:
 *   cPreambles     number of preambles to use.
 *
 * RETURNS:
 *   The number of preambles actually set.
 ***************************************************************************/

BYTE hll_set_preambles( BYTE cPreambles )
{
     if( cPreambles < HART_MINIMUM_PREAMBLES )
     {
          cPreambles = HART_MINIMUM_PREAMBLES;
     }
     else if( cPreambles > HART_MAXIMUM_PREAMBLES )
     {
          cPreambles = HART_MAXIMUM_PREAMBLES;
     }
     ghc.cPreambles = cPreambles;
     return( cPreambles );
}


/**************************************************************************
 * FUNCTION: hll_get_preambles
 *
 * DESCRIPTION:
 *   Returns the number of preambles currently being used when sending
 * HART commands
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   number of preambles being used.
 ***************************************************************************/

BYTE hll_get_preambles( void )
{
     return( ghc.cPreambles );
}


/**************************************************************************
 * FUNCTION: hll_set_master
 *
 * DESCRIPTION:
 *   Sets the master status of the data link layer to be either a
 * primary or secondary master.
 *
 * ARGUMENTS:
 *   cMaster        new master setting.
 *
 * RETURNS:
 *   actual master setting used.
 ***************************************************************************/

BYTE hll_set_master( BYTE cMaster )
{
     if( cMaster == HART_PRIMARY )
     {
          ghc.bPrimary = TRUE;
          ghc.wRT1     = RT1PRI;
     }
     else
     {
          ghc.bPrimary = FALSE;
          ghc.wRT1     = RT1SEC;
          cMaster      = HART_SECONDARY;
     }
     return( cMaster );
}


/**************************************************************************
 * FUNCTION: hll_get_master
 *
 * DESCRIPTION:
 *   Returns the primary/secondary master setting for the link layer.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   current master setting.
 ***************************************************************************/

BYTE hll_get_master( void )
{
     return( ghc.bPrimary ? HART_PRIMARY : HART_SECONDARY );
}


/**************************************************************************
 * FUNCTION: hll_set_retries
 *
 * DESCRIPTION:
 *   Sets the number of retries used when sending a command.  The actual
 * number of times the command is sent is retries + 1;
 *
 * ARGUMENTS:
 *   cRetries       number of retries.  This must be 0 < cRetries < 5
 *
 * RETURNS:
 *   number of retries actually set
 ***************************************************************************/

BYTE hll_set_retries( BYTE cRetries )
{
     if( cRetries > 5 )
     {
          cRetries = 5;
     }
     ghc.cRetries = cRetries;
     return( ghc.cRetries );
}


/**************************************************************************
 * FUNCTION: hll_get_retries
 *
 * DESCRIPTION:
 *   Retrieves the current retry setting.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   number of retries being used.
 ***************************************************************************/

BYTE hll_get_retries( void )
{
     return( ghc.cRetries );
}


/**************************************************************************
 * FUNCTION: hll_extend_timeout
 *
 * DESCRIPTION:
 *   Extends the slave response timeout by the given number of milliseconds.
 * A zero value resets the extension to zero.
 *
 * ARGUMENTS:
 *   cMilliSec      milliseconds to extend.
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void hll_extend_timeout( WORD wMilliSec )
{
     if( wMilliSec )
     {
          ghc.wExtend += wMilliSec;
     }
     else
     {
          ghc.wExtend = 0;
     }
}

/**************************************************************************
 * FUNCTION: hll_read_statistics
 *
 * DESCRIPTION:
 *   Reads the statistical counters.
 *
 * ARGUMENTS:
 *   pStats         pointer to location to store statistics.
 *
 * RETURNS:
 *   STATUS_OK      always.
 ***************************************************************************/

FSTAT hll_read_statistics( PLINKSTATS pStats )
{
     *pStats = ghc.LinkStats;
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hll_clear_statistics
 *
 * DESCRIPTION:
 *   Zero's out the statistical counters.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   STATUS_OK      always.
 ***************************************************************************/

FSTAT hll_clear_statistics( void )
{
     memset( &ghc.LinkStats, 0, sizeof( LINKSTATS ) );
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hll_get_last_burst
 *
 * DESCRIPTION:
 *   Reads the most recent burst message
 *
 * ARGUMENTS:
 *   acBuf     ptr to place message
 *
 * RETURNS:
 *   STATUS_OK      If a burst message has been received since last clear.
 *   STATUS_ERROR   If no burst message to retrieve.
 ***************************************************************************/

FSTAT hll_get_last_burst( PBYTE acBuf )
{
     if( ghc.cBurstLen )
     {
          memcpy( acBuf, ghc.acBurstBuf, ghc.cBurstLen );
          return( STATUS_OK );
     }
     else
     {
          return( STATUS_ERROR );
     }
}


/**************************************************************************
 * FUNCTION: hll_clear_burst_buf
 *
 * DESCRIPTION:
 *   Clears the burst message buffer.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void hll_clear_burst_buf( void )
{
     ghc.cBurstLen = 0;
}


/**************************************************************************
 * FUNCTION: hll_send_command
 *
 * DESCRIPTION:
 *   Puts a formatted HART message into a queue for sending.
 *
 * ARGUMENTS:
 *   pcId      pointer to byte where ID for the queued message is placed.
 *             This id is necessary to retrieve the response.
 *
 * RETURNS:
 *   STATUS_OK      command was queued and will be processed.
 *   STATUS_ERROR   queue is full, command cannot be processed now.
 ***************************************************************************/

FSTAT hll_send_command( PBYTE pcId, PBYTE acMessage, BYTE cLength )
{
     FSTAT     nStatus = STATUS_ERROR;
     WORD      wIdx;
     PCOMMQ    pCommQ = 0;

//DbgPrint"hll_send_command(,,%d\n", cLength);
     /*
      * Find an empty queue entry.
      */
     for( wIdx = 0; wIdx < HART_QUEUE_DEPTH; wIdx++ )
     {
          if( gaQueueEntry[ wIdx ].cQState == QSTATE_EMPTY )
          {
               pCommQ = &gaQueueEntry[ wIdx ];
//DbgPrint" found wIdx=%d\n", wIdx);
               break;
          }
     }

     if( pCommQ )
     {
          nStatus = STATUS_OK;
          /*
           * Fill in the information required to process the message.
           */
          pCommQ->cQState = QSTATE_WAITING;
          pCommQ->cMsgId = gcNextMsgId;
          *pcId = gcNextMsgId;
//DbgPrint" pcId=%d\n", *pcId);
          gcNextMsgId++;
          if( !gcNextMsgId )
          {
               /*
                * Don't use 0 as a message ID.
                */
               gcNextMsgId = 1;
          }
          memcpy( pCommQ->acMessage, acMessage, cLength );
          pCommQ->cMsgLength = cLength;
          memset( pCommQ->acResponse, 0, MAX_HART_MSG_LEN );
          pCommQ->cRspCount = 0;
          pCommQ->cRspQuality = STATUS_OK;

          /*
           * Add entry to the queue.
           */
          disable_interrupt();
          gapQueue[ gcQueueNextIn ] = pCommQ;
          gcQueueNextIn++;
          if( gcQueueNextIn >= HART_QUEUE_DEPTH )
          {
               gcQueueNextIn = 0;
          }
          hls_state_machine( &ghc, EVENT_XMIT_PENDING );
          enable_interrupt();
     }

     return( nStatus );
}


/**************************************************************************
 * FUNCTION: hll_read_response
 *
 * DESCRIPTION:
 *   Retrieves the response to a previously sent command.
 *   If the returned status is either STATUS_OK or STATUS_ERROR, then the
 *   Id will be invalidated and you cannot request the response again.
 *   If the returned status is STATUS_WORKING, then nothing is copied, and
 *   you must try again later.
 *   YOU MUST EVENTUALLY RETRIEVE THE DATA FROM ALL COMMANDS OR THE QUEUE
 *   WILL FILL UP.
 *
 * ARGUMENTS:
 *   cId            Id of the message as returned from hll_send_command.
 *   acResponse     Pointer to buffer to place response.  Must be at least
 *                  MAX_HART_MSG_LEN bytes long.
 *
 * RETURNS:
 *   STATUS_OK      Response was read into the buffer.
 *   STATUS_BADID   No such Id in the queue.
 *   STATUS_WORKING Response has not yet been received.
 *   STATUS_ERROR   Never got a valid response.  Command is dead.
 ***************************************************************************/

FSTAT hll_read_response( BYTE cId, PBYTE acResponse )
{
     FSTAT     nStatus = STATUS_BADID;
     WORD      wIdx;
     PCOMMQ    pCommQ = 0;

     /*
      * Find the Id in a queue entry.
      */
     for( wIdx = 0; wIdx < HART_QUEUE_DEPTH; wIdx++ )
     {
          /* only need to search the queue slots that are used */
          pCommQ = gapQueue[ wIdx ];
          if( (pCommQ != NULL) && (pCommQ->cMsgId == cId) )
          {
               break;
          }
     }

     if( wIdx < HART_QUEUE_DEPTH )
     {
          disable_interrupt();
          switch( pCommQ->cQState )
          {
          case QSTATE_DONE:
               nStatus = STATUS_OK;
               pCommQ->cQState = QSTATE_EMPTY;
               gapQueue[ wIdx ] = NULL;
               memcpy( acResponse, pCommQ->acResponse, pCommQ->cRspCount );
               break;

          case QSTATE_WAITING:
          case QSTATE_WORKING:
               nStatus = STATUS_WORKING;
               break;

          case QSTATE_DEAD:
               nStatus = STATUS_ERROR;
               pCommQ->cQState = QSTATE_EMPTY;
               gapQueue[ wIdx ] = NULL;
               /* return a 'busy' response.  */
               if (pCommQ->cRspCount) {
                    memcpy(acResponse, pCommQ->acResponse, pCommQ->cRspCount );
                    /* Zero the rest of the buffer in case it was short. */
                    memset(acResponse + pCommQ->cRspCount, 0,
                    MAX_HART_MSG_LEN-pCommQ->cRspCount);
               } else {
                    acResponse[0] = 0;
               }

               break;

          case QSTATE_EMPTY:
          default:
               nStatus = STATUS_BADID;
               pCommQ->cQState = QSTATE_EMPTY;
               gapQueue[ wIdx ] = NULL;
               break;
          }
          enable_interrupt();
     }

     return( nStatus );
}


/**************************************************************************
 * FUNCTION: next_queue_to_process
 *
 * DESCRIPTION:
 *   gets a pointer to the next queue entry that is ready to process.
 *
 * ARGUMENTS:
 *   ppCommQ        pointer to location to put pointer to queue data entry.
 *
 * RETURNS:
 *   STATUS_OK      pointer retrieved.
 *   STATUS_ERROR   nothing to process.
 ***************************************************************************/

static FSTAT next_queue_to_process( PCOMMQ *ppCommQ )
{
     PCOMMQ    pCommQ;
     FSTAT     nStatus = STATUS_ERROR;

     pCommQ = gapQueue[ gcQueueNextOut ];
     if( (pCommQ != NULL) && (pCommQ->cQState == QSTATE_WAITING) )
     {
//DbgPrint" next_queue_to_process QSTATE_WORKING\n");
          pCommQ->cQState = QSTATE_WORKING;
          *ppCommQ = pCommQ;
          nStatus = STATUS_OK;
          gcQueueNextOut++;
          if( gcQueueNextOut >= HART_QUEUE_DEPTH )
          {
               gcQueueNextOut = 0;
          }
     }
     return( nStatus );
}

