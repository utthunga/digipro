/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: microctl.c $
 * $Archive: /SwRICode/MasterDLL/src/microctl.c $
 * $Revision: 1.1 $
 * $Date: 1997/12/02 23:07:00 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1997, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   This file contains 68HC11 microcontroller specific interface functions
 * to the HART Data Link Layer processor.  These functions will require
 * modification if other than the following are used:
 *   OC2  for the HART Timer
 *   PA0  for the Carrier Detect Input
 *   PA4  for the RTS Output
 * Adjustments for clock speed are made in microctl.h.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: microctl.c $
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/21/96   Time: 1:08p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Added new timer functions.
 * Removed enable interrupt from this file.
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/

// dpc_opts.h
#define OPT_HIGH_RESOLUTION_TIMER

// std.h
typedef short word;                /* Signed 16 bits */

#if !defined(__cplusplus)
typedef unsigned char bool;
#define false 0
#define true 1
#endif // !defined(__cplusplus)


// For "struct serial_icounter_struct"
#include <linux/serial.h>


#include "microctl.h"
#include "lnkctrl.h"

#include "flk_log4cplus_clogger.h"
#include "flk_log4cplus_defs.h"
#include "stacktrace.h"

#if defined(linux)
#include <stdio.h>
#include "hartmdll.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>        // for clock_gettime and CLOCK_MONOTONIC
#include <errno.h>
#include <semaphore.h>


#define HART_SLEEP_DELAY  10       /* Delay used for sleep in hart  */

static int HARTFD = -1;
static int inverted_cd = 0;
int hart_debug = 0;
static int init_performed = 0;
// Semaphore object for thread signaling
sem_t m_semaphore;
// initialize semaphore



static void pexit(void)
{
   char buf[2];
   dbgPrint("press return to exit\n");
   fgets(buf, 2, stdin);
   exit(1);
}

int sys_safe_usleep (
    __useconds_t __useconds ///< Sleep time.
);


#undef USE_CLONE

#ifdef USE_CLONE
# define __USE_GNU
# include <sched.h>
#else
# include <pthread.h> // compile an link with -pthread
#endif

static volatile int EI=0;
static volatile int bg_thread_created = 0;

#ifdef USE_CLONE
#define CLONE_STACK_SIZE  (100*1024)    // stack size of cloned thread
char clone_stack[CLONE_STACK_SIZE];
#else
#endif

#ifdef USE_CLONE
int bg(void *);
#else
void * bg(void *);
#endif


void enable_interrupt(void) { 
    // if (!EI) dbgPrint("EI\n");
    EI=1; 
    if (!bg_thread_created) {
        int r;
        bg_thread_created = 1;
#ifdef USE_CLONE
        r = clone( bg, clone_stack+CLONE_STACK_SIZE,
                        CLONE_FILES | CLONE_IO, (void *)0x123 );
#else
        
        static pthread_t thread;
        r = pthread_create(&thread,  NULL, bg, 0);
        if (r) {
            perror("pthread_create failed\n");
            exit(1);
        }

#endif
        dbgPrint("background thread %d started\n", r);
    }
}

void disable_interrupt(void)
{ 
    // if (EI) dbgPrint("DI\n");
    EI=0; 
}

#endif // linux

/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/

#define XMSTATE_IDLE          0
#define XMSTATE_PREAMBLES     1
#define XMSTATE_MESSAGE       2
#define XMSTATE_SHUTDOWN      3

#define HART_PREAMBLE         0xFF


/***************************************************************************
 * Private Data Types for this file
 ***************************************************************************/

typedef void (*RESETFUNC)( void );

/***************************************************************************
 * Private Variables for this file
 ***************************************************************************/

static BYTE      vcTimer;

/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/

static void send_hart_byte( BYTE cData );

static int linux_init(void);
static void linux_rts_on(void);
static void linux_rts_off(void);
static int linux_read_cd(void);
static void linux_send_hart_byte( BYTE cData );
static void linux_dtr_on(void);
static void linux_dtr_off(void);

static BYTE linux_sending;
static BYTE timerEnabled;


/**************************************************************************
 * FUNCTION: mc_initialization
 *
 * DESCRIPTION:
 *   Processor specific initialization function.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/


int mc_initialization( void )
{
   return linux_init();
}

/**************************************************************************
 * FUNCTION: mc_shutdown
 *
 * DESCRIPTION:
 *   Processor specific shutdown function.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/


int mc_shutdown( void )
{
   return linux_exit();
}


/**************************************************************************
 * FUNCTION: mc_set_hart_timer
 *
 * DESCRIPTION:
 *   Sets the hart timer so that an interrupt will occur in the given
 * time period. 
 * The minimum resolution is 5 milliseconds, with the input value
 * rounded down to the next closest multiple of 5 milliseconds.
 *
 * ARGUMENTS:
 *   wMilliSeconds       milliseconds until interrupt.  If 0, turns off
 *                       the interrupt.
 *
 * RETURNS:
 *   nothing.
 ***************************************************************************/

static void linux_set_timer(int wMilliSeconds);

void mc_set_hart_timer( int nMilliSeconds )
{

static word last_nonGapTimer = RT1PRI;

    if (nMilliSeconds < 0) {
        timerEnabled = FALSE;
        return;
    }

    if (nMilliSeconds == 0) {
        // Resume the old count.
        vcTimer = last_nonGapTimer;
        timerEnabled = TRUE;
        return;
    }

#if 0 && DEBUG
    dbgPrint("mc_set_hart_timer( %d )\n", nMilliSeconds);
#endif
     linux_set_timer(nMilliSeconds);


    if (nMilliSeconds != TGAP) {
        last_nonGapTimer = vcTimer;
    }
    timerEnabled = TRUE;
    return;
}



/**************************************************************************
 * FUNCTION: mc_send_hart_message
 *
 * DESCRIPTION:
 *   This is the function that handles the transmission of a message.
 * Don't call this function unless you are sure that it is ok to send
 * a message.  I.e., you have the bus, and no other transmission is in
 * progress.
 *
 * ARGUMENTS:
 *   cPreambles     Number of preambles to send.
 *   acMessage      Pointer to message to send.  Message should not be
 *                  changed until after transmission is complete since the
 *                  message is not copied locally.
 *   cLength        Length of message to send.
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void mc_send_hart_message( BYTE cPreambles, PBYTE acMessage, BYTE cLength )
{
char buf[HART_MAXIMUM_PREAMBLES+MAX_HART_MSG_LEN+1];
int len;

    dbgPrint("mc_send_hart_message( %d, , %d )\n", cPreambles, cLength );

    /*
    ** Trace output messages if HART_DEBUG set.
    */
    if (LOG_LEVEL_TRACE == getLog4cplusLogLevel()) 
    {
        int i;
        char d = (acMessage[1]&0x80) ? 'S' : 's';
        printf("\n%c .. %02x:", d, cLength);
        for (i=0; i<cLength; i++) {
            printf("%02x", acMessage[i]);
        }
        printf("\n");
    }

    /*
    * Turn on RTS (Port A, Bit 3)
    */
    linux_rts_on();
    if (cPreambles) {
	memset(buf, HART_PREAMBLE, cPreambles);
    }
    memcpy(buf+cPreambles, acMessage, cLength);
    len = cPreambles+cLength;
{
    int r = write(HARTFD, buf, len);
    if (r != len ) 
    {
        dbgPrint("write(%d,,%d) = %d\n", HARTFD, len, r);
	    perror("mc_send_hart_message");
	    pexit();
    }
}
    linux_sending = (BYTE) len;
     
}


/**************************************************************************
 * FUNCTION: mc_hart_read_cd
 *
 * DESCRIPTION:
 *   Reads the state of the carrier detect line.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   TRUE      if cd is on
 *   FALSE     if cd is off
 ***************************************************************************/

BYTE mc_hart_read_cd( void )
{
    return linux_read_cd();
}


/**************************************************************************
 * FUNCTION: send_hart_byte
 *
 * DESCRIPTION:
 *   Sends a data byte out the SPI port using odd parity
 *
 * ARGUMENTS:
 *   cData          byte to send
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void send_hart_byte( BYTE cData )
{
     linux_send_hart_byte(cData);
}



#if defined(linux) 

#include <linux/major.h>
#include <linux/kdev_t.h>


/*SUBTITLE linux_init */
/*FUNCTION*********************************************************************
      Set up for HART communication.
      1) 9 data bits (8 data bits + odd parity), 1 stop bit
      2) Baud rate 1200
      3) Turn on receiver and receive interrupt.
      Shut down RTS.
******************************************************************************/
int linux_init()
{
struct stat stat;
    struct termios tb;

    const char *devname;

    if (!init_performed) {
            init_performed = 1;

        /*
        ** We have both types of modems (MACTek VIATOR, and SMAR)
        **  Since the SMAR modem inverts CD, and we have more
        **  of them that the MACTek's we will assume that the
        **  modem inverts CD, to get the non-inverted CD
        **    export HART_MODEM=MACTEK
        **      or
        **    export HART_MODEM=VIATOR
        **  (case is ignored)
        **  To specify inverted CD set
        **    export HART_MODEM=SMAR
        **  or don't set the HART_MODEM environment variable
        */

        /**
         * Manually setting env variables for testing purpose. Need to remove.
         **/
       // setenv("HART_MODEM", "VIATOR", 1);
       // setenv("HART_DEBUG", "1", 1);
       // setenv("HART", "/dev/ttyACM0", 1);
        //setenv("HART", "/dev/ttyACM0", 1);
        //setenv("HART", "/dev/bus/usb/003/001", 1);
        //setenv("HART", "/dev/USB", 1);

        const char *modem = ghc.hartModem;
        if (modem == 0 || strcasecmp(modem, "SMAR")==0) {
            inverted_cd = 1;
        }
        dbgPrint("HART_MODEM=%s%s\n",
            modem ? modem : "unset",
            inverted_cd ? " (inverted CD)" : "");

        if (!(devname = ghc.deviceFile))
            devname = "/dev/ttyS0";
        dbgPrint("HART=%s\n", devname);

        if ((HARTFD = open(devname, O_RDWR |  O_NONBLOCK )) == -1) {
            dbgPrint("cannot open %s for reading\n", devname);
            init_performed = 0;
            return STATUS_ERROR;//exit(1);
        }

        /* Set up read channel */
        tcgetattr(HARTFD, &tb);

        /* Must read at least one character */
        tb.c_cc[VMIN] = 1;

        /* no input post-processing */
        tb.c_iflag = 0;        // TODO - add INPCK

        /* no output post-processing */
        tb.c_oflag = 0;

        /* enab. modem ctrl. | odd par. | enab. par. | enab. recvr. | 1200 baud | 8  */
        tb.c_cflag = PARODD | PARENB | CREAD | B1200 | CS8 | CLOCAL ;
        /* Use non-canonical input */
        tb.c_lflag = 0;

        /* TCSANOW means make the change effective immediately */
        /* TCSADRAIN means make the change as soon as... */
        /* TCSAFLUSH means make the change after all i/o cleared */
        if (tcsetattr(HARTFD, TCSANOW, &tb) == -1) {
            dbgPrint("cannot configure read channel %s\n", devname);
            init_performed = 0;
            return STATUS_ERROR;
        }

        // initializes the semaphore
        sem_init(&m_semaphore, 0, 1);
        linux_dtr_on(); 	/* Raise DTR */
    }

    linux_rts_off(); 	/* Drop RTS */

    return STATUS_OK;
}

static void linux_send_hart_byte( BYTE cData )
{
    BYTE buf[20];
    buf[0] = cData;
    dbgPrint(">%02x\n", cData);
}


static void linux_dtr_on(void)
{
    int b = TIOCM_DTR;
    dbgPrint("dtr on\n");
    if (ioctl(HARTFD,TIOCMBIS, &b) != 0) {
        perror("linux_dtr_on:ioctl failed");
    }
}

static void linux_rts_on(void)
{
    int b = TIOCM_RTS;
    if (ioctl(HARTFD,TIOCMBIS, &b) != 0) {
        perror("linux_rts_on: ioctl failed");
    }

    dbgPrint("rts on\n");
}

static void linux_rts_off(void)
{
    int b = TIOCM_RTS;
    if (ioctl(HARTFD,TIOCMBIC, &b) != 0) {
        perror("linux_rts_off: ioctl failed");
    }

    dbgPrint("rts off\n");
}


int linux_read_cd(void)
{

    static int oldcd = -1;
    int cd;
    int newbits = 0;

    if (ioctl(HARTFD,TIOCMGET, &newbits) == -1) {
        dbgPrint("linux_read_cd: ioctl(TIOCMGET,)\n");
    }

    {

#if defined(_754)
        // 754 digital board uses inverted CTS for CD
        cd = (newbits & TIOCM_CTS) ? 1 : 0;
        cd = !cd;
#else
        // 754 desktop (external HART modem) uses CD
        cd = (newbits & TIOCM_CAR) ? 1 : 0;
#endif

        if (inverted_cd) {
            /* the SMAR modem inverts CD */
            cd = !cd;
        }

	if (cd != oldcd) { 
	    oldcd = cd; 

            /*
            ** Trace CD changes if HART_DEBUG set.
            */
        dbgPrint("cd %s\n", cd ? "on" : "off");
	}
	return cd;
    }
#endif
    return 0;
}

#if defined(OPT_HIGH_RESOLUTION_TIMER)
static struct timespec stop_time;
#else /* defined(OPT_HIGH_RESOLUTION_TIMER) */
static struct timeval stop_time;
#endif /* defined(OPT_HIGH_RESOLUTION_TIMER) */



/*SUBTITLE linux_set_timer */
/*FUNCTION*********************************************************************
  TBD.
******************************************************************************/
static void linux_set_timer(
int nMilliSeconds)
{
    if (nMilliSeconds < 0) {
        timerEnabled = FALSE;
        return;
    }

    if ( nMilliSeconds ) 
    {
        // Resume the old count.
        timerEnabled = TRUE;

        long msecs;
        long secs;
#if defined(OPT_HIGH_RESOLUTION_TIMER)
        long nsecs;
        vcTimer = (nMilliSeconds + 4) / 5; /* round up */
        clock_gettime(CLOCK_MONOTONIC, &stop_time);

        msecs = vcTimer * 5;
        if (msecs > 1000) {
                secs = msecs / 1000;
                stop_time.tv_sec += secs;
                msecs -= secs * 1000;
        }
        nsecs = msecs * 1000000;
        stop_time.tv_nsec += nsecs;
        if ((stop_time.tv_nsec) > 1000000000) {
                stop_time.tv_sec += 1;
                stop_time.tv_nsec -= 1000000000;
        }
#else /* defined(OPT_HIGH_RESOLUTION_TIMER) */
        long usecs;
        vcTimer = (nMilliSeconds + 4) / 5; /* round up */
        gettimeofday(&stop_time, 0);

        msecs = vcTimer * 5;
        if (msecs > 1000) {
                secs = msecs / 1000;
                stop_time.tv_sec += secs;
                msecs -= secs * 1000;
        }
        usecs = msecs * 1000;
        stop_time.tv_usec += usecs;
        if ((stop_time.tv_usec) > 1000000) {
            stop_time.tv_sec += 1;
            stop_time.tv_usec -= 1000000;
        }
#endif /* defined(OPT_HIGH_RESOLUTION_TIMER) */

    }
}



/*SUBTITLE linux_sleep */
/*FUNCTION*********************************************************************

calls:
    hls_state_machine( &ghc, EVENT_TIMEOUT );

******************************************************************************/


#define timer_interval_finish()

void linux_sleep(void)
{
    char buf[1];
    BYTE cQuality = STATUS_OK;
    BYTE cData;

    /* 
    	Serial port statistics, including bit errors. 
        Note: 'static' to retain value across calls.
    */
    static struct serial_icounter_struct previousPortStats, presentPortStats;

    if (linux_sending) {
        int result, arg;
        result = ioctl(HARTFD, TIOCOUTQ, &arg); /* bufchar_count / 
                                           bufchars_remain */
        if (result == -1) {
            dbgPrint("ioctl(%d, TIOCOUTQ, )\n", HARTFD);
            goto chkuart;
        }
        if (arg == 0) {
            int result,arg;
            chkuart:
            result = ioctl(HARTFD, TIOCSERGETLSR, &arg);	/* uart_empty */
            if (result == -1) {
                dbgPrint("ioctl(%d, TIOCSERGETLSR, )\n", HARTFD);
                //pexit();
                arg = 1;
            }
            // result = ioctl(HARTFD, TIOCGSERIAL, &arg);	/* uart_empty */
            //if (result == -1) {
           //     dbgPrint("ioctl(%d, TIOCSERGETLSR, )\n", HARTFD);
            //    pexit();
            //}

            if (arg == 1 ) {
                /* 
                    Get the counts of parity, overrun and frame into 
                    previousPortStats before we disable the modem transmitter
                    (i.e., before enabling the modem receiver).
                */
                memset(&previousPortStats, 0, sizeof(previousPortStats));
                if(ioctl(HARTFD, TIOCGICOUNT, &previousPortStats) != 0) {
                     dbgPrint("TIOCGICOUNT previousPortStats failed\n");
                }

                /* Last byte is out, so shut down */
                linux_rts_off();
                linux_sending = 0;
                hls_state_machine( &ghc, EVENT_XMIT_DONE );

            }
        }
        sys_safe_usleep(HART_SLEEP_DELAY); /* Delay between characters to write */
    } else {
        int ret = 0;
	fd_set Rdfd;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = timerEnabled ? 10:2;
	FD_ZERO(&Rdfd);
	FD_SET(HARTFD,&Rdfd);

	/*
	    While not pretty, this code respects the possible EINTR return.
	    If we get interrupt by a signal, since this wait is so short, we
	    currently try again. Note that timeout has to be reset, as it
	    is not guaranteed to still be good after a error return.
	*/
    bool isSelectDone = false;
	while ( false == isSelectDone)
	{
        ret = select(FD_SETSIZE,&Rdfd,NULL,NULL,&timeout);

        if (ret >= 0)
        {
            isSelectDone = true;
        }
        else
        {
            if (errno != EINTR)
            {
                // Other error, continue.
                isSelectDone = true;
            }
            else
            {
                // signal error - refresh timeout and try again.
                timeout.tv_sec = 0;
                timeout.tv_usec = timerEnabled ? 10:2;
            }
        }
	}
	int bytes_read = 0;
	if (ret > 0) {   /* character(s) available to read */
	    if (FD_ISSET(HARTFD,&Rdfd)) { 

                bytes_read = read(HARTFD, buf, 1);
           }
        }

        if (bytes_read == 1) {

            // One character has been read into buf.

            timerEnabled = FALSE;
            cData = buf[0];

            /* 
                Get the counts of parity, overrun and frame into 
                presentPortStats after we've received a character.
                The counts could indicate errors of yet unread characters,
                so use FIONREAD (or TIOCINQ) to determine how many other 
                characters are still unread.
            */
            memset(&presentPortStats, 0, sizeof(presentPortStats));
            if (ioctl(HARTFD, TIOCGICOUNT, &presentPortStats) != 0) {
                 dbgPrint("TIOCGICOUNT presentPortStats failed\n");
	    }

            int unreadCharacters = 0;
#if defined(PRE_REV_X_HARDWARE)   // TBD - make this runtime
            (void) ioctl(HARTFD, FIONREAD, &unreadCharacters);
#endif
            /*
                If the bit error counts from the presentPortStats don't 
                match with the counts from the previousPortStats, determine 
                and set cQuality status.

                Note: Because cQuality is a *not* a bitmask, the priority
                of what cQuality gets set to is the same as was in the 744.
            */

            int overrun = presentPortStats.overrun - previousPortStats.overrun;
	    int frame   = presentPortStats.frame - previousPortStats.frame;
	    int parity  = presentPortStats.parity - previousPortStats.parity;
	    int brk  = presentPortStats.brk - previousPortStats.brk;

            // HART protocol ignores parity errors until gap timeout active.
            bool HART_ignoring_errors = ( ! ghc.rc.bGapActive );

            if (! HART_ignoring_errors && (overrun || frame || parity || brk)) {

                // We can't prove that this byte is bad. (Bug #58668)
                // bool Linux_ignoring_errors = (0 != unreadCharacters);

                /* ( ghc.rc.cState in hart/rcvstate.c) */
                #define RCV_DATA 2  

                // The number of expected characters in this HART packet
                // including the checksum.  ( when in RCV_DATA state ).
                int charsLeftToParse = ghc.rc.cTemp;

                // We are on-or-after the checksum if the current
                // character we have read plus the characters that
                // Linux has in its buffers would take us past
                // the checksum byte. (This is where the parity error occurs.)
                bool postChecksum = (RCV_DATA == ghc.rc.cState) &&
                              ((1 + unreadCharacters) >= charsLeftToParse);

                if (!postChecksum) {

                    if (overrun) {
                        cQuality = OVERRUN_ERROR;
                        dbgPrint("OVERRUN_ERROR, ");
                    }
                    else if (frame) {
                        cQuality = FRAMING_ERROR;
                        dbgPrint("FRAMING_ERROR, ");
                    }
                    else if (parity) {
                        cQuality = PARITY_ERROR;
                        dbgPrint("PARITY_ERROR, ");
                    }
                }
                
                if (LOG_LEVEL_TRACE == getLog4cplusLogLevel()) 
                {
                    dbgPrint("C:%02x CQ=%d, O/F/P/B: %d/%d/%d/%d"
                           ", UC:%d"
                           ", rc:S/G/T/q/c: %d/%d/%d/%d/%d"
                           "\n",
                        cData, cQuality, overrun, frame, parity, brk, 
                        unreadCharacters,
                        ghc.rc.cState, ghc.rc.bGapActive, ghc.rc.cTemp, 
                            ghc.rc.cRspQuality, ghc.rc.cRspCount);
                }
            }
            /*
                Backup presentPortStats to previousPortStats for the next 
                received character.
            */
            previousPortStats = presentPortStats;

            hls_process_received_char( &ghc, cData, cQuality );
        }
        else {
            goto Check_timer;
        }

#if defined(OPT_HIGH_RESOLUTION_TIMER)
        struct timespec cur_time;

Check_timer:

        clock_gettime(CLOCK_MONOTONIC, &cur_time);
        if (timerEnabled  /* it was enabled */
  	    && (  (cur_time.tv_sec > stop_time.tv_sec)
            || (   (cur_time.tv_sec == stop_time.tv_sec)
	    && (cur_time.tv_nsec > stop_time.tv_nsec))) ) {

#else /* defined(OPT_HIGH_RESOLUTION_TIMER) */
        struct timeval cur_time;

Check_timer:

	gettimeofday(&cur_time, 0);
	if (timerEnabled  /* it was enabled */
	    && (  (cur_time.tv_sec > stop_time.tv_sec)
	    || (   (cur_time.tv_sec == stop_time.tv_sec)
	    && (cur_time.tv_usec > stop_time.tv_usec))) ) { // }

#endif /* defined(OPT_HIGH_RESOLUTION_TIMER) */
 
	    /* Timer has expired */
	    timerEnabled = FALSE;
	    vcTimer = 0;
	    timer_interval_finish();
// L&T Modifications : Real Tx. Support - start
// Commenting the printf
 dbgPrint("EVENT_TIMEOUT\n");
// L&T Modifications : Real Tx. Support - end
	    hls_state_machine( &ghc, EVENT_TIMEOUT );
	    rcv_init_state_machine( &ghc.rc );
        } 


    }
}

/*
        the linux backgorun task
*/
#ifdef USE_CLONE
int bg(void *arg)
#else
void * bg(void *arg)
#endif
{
    int val = 0;
    dbgPrint("in background task\n");
    for(;;) {
        if (EI) {
            // interrupts "enabled"   TODO( use a real semaphore )
            linux_sleep();
        }

        if (0 == init_performed)
        {
            break;
        }

       // (void) sys_safe_uforsleep(100 * 1000);
       // (void) sys_safe_usleep(100 * 200);  // L&T Modification: For Sync read & write with transmitter

       /* CPMHACK : Much smaller sleep is required here, giving more sleep 
          affects processing the command request and response correctly especially when the device is bursting.
          2 ms second is chosen to reduce the CPU usage when this loop doesn't do any job.*/
      
       (void) sys_safe_usleep(1000 * 2);
    }
    // signal the waiting thread
    sem_post(&m_semaphore);
    return 0;
}

#define USEC_TO_NSEC(us) ((us) * 1000)
#define ONE_SECOND_IN_MICRO_SEC_TICKS  (1000000)

int sys_safe_usleep (
    __useconds_t __useconds
)
{
    // We use nanosleep here, so we can continue on a signal interrupt.
    struct timespec sleepTime;
    if (__useconds >= ONE_SECOND_IN_MICRO_SEC_TICKS)
    {
        sleepTime.tv_sec = __useconds / ONE_SECOND_IN_MICRO_SEC_TICKS;
        sleepTime.tv_nsec = USEC_TO_NSEC(
            (__useconds % ONE_SECOND_IN_MICRO_SEC_TICKS)
        );
    }
    else
    {
        sleepTime.tv_sec = 0;
        sleepTime.tv_nsec = USEC_TO_NSEC(__useconds);
    }

    int isDone = 0;
    int sleep_return = -1;

    while ( 0 == isDone)
    {
        struct timespec sleepTimeRemaining;
        sleep_return = nanosleep(&sleepTime, &sleepTimeRemaining);

        /*
            Fix for bugaboo: http://bugaboo.tc.fluke.com/60243
            Now routine will continue to sleep on an EINTR signal.
        */
        if (sleep_return >= 0)

        {
            isDone = 1;
        }
        else
        {
            if (errno == EINTR)
            {
                // We got a signal, go back to sleep with the remaining time.
                sleepTime.tv_sec = sleepTimeRemaining.tv_sec;
                sleepTime.tv_nsec = sleepTimeRemaining.tv_nsec;
                sleepTimeRemaining.tv_sec = 0;
                sleepTimeRemaining.tv_nsec = 0;
            }
            else
            {
                // Some other error, return that.
                isDone = 1;
            }
        }
    }
    return sleep_return;
}

int linux_exit()
{
    init_performed = 0;
    int retCode = STATUS_OK;

    if (bg_thread_created)
    {
        // blocks the thread till the background thread exits
        sem_wait(&m_semaphore);
        bg_thread_created = 0;
    }

    if (-1 != HARTFD)
    {
        // close the device file
        retCode = close(HARTFD);
        HARTFD = -1;
    }

    // destroys the semaphore
    sem_destroy(&m_semaphore);

    return retCode;
}
