/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: happ.c $
 * $Archive: /SwRICode/HMaster/src/happ.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   HART Application layer user functions.
 * Modify these to add support for more HART commands, or to remove support.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: happ.c $
 * 
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:36a
 * Updated in $/SwRICode/HMaster/src
 * Add new variable for change poll address command.
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:58p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "hartmdll.h"
#include "stacktrace.h"

/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/

static void extract_block_4( PBYTE pData, PDEVDATA pDevData, BYTE cBlock, WORD wDataCount );
static void extract_block_5( PBYTE pData, PDEVDATA pDevData, BYTE cBlock, WORD wDataCount );


/**************************************************************************
 * FUNCTION: happ_format_header
 *
 * DESCRIPTION:
 *   Formats a header for the given HART command number.  Formats the header
 * up to (but not including) the byte count
 *
 * ARGUMENTS:
 *   acMessage      ptr to buffer for formatted message.
 *   ppData         ptr to location to store pointer to byte count
 *   cCmd           command number to format.
 *   pDevData       pointer to DevData struct where xmtr data is located.
 *
 * RETURNS:
 *   STATUS_OK      no error
 *   STATUS_ERROR   error in formatting command
 *
 ***************************************************************************/

void happ_format_header( PBYTE acMessage, PBYTE *ppCnt, BYTE cCmd, PDEVDATA pDevData, PBOOL pbLongFrame )
{
     BOOL      bLongFrame = FALSE;
     BOOL      bPrimary;
     PBYTE     pData;
     BYTE      cPoll;
     BYTE      szTag[ HART_TAG_LEN + 1 ];

     /*
      * If this is an identify command, then clean out the Device Data.
      */
     if( cCmd == CMD_READ_UID )
     {
          hll_set_preambles( HART_MINIMUM_PREAMBLES );
          //hll_set_preambles( HART_DEFAULT_PREAMBLES );
          cPoll = pDevData->cPollAddr;
          memset( pDevData, 0, sizeof( DEVDATA ) );
          pDevData->cPollAddr = cPoll;
     }
     else if( cCmd == CMD_READ_UID_BYTAG )
     {
          hll_set_preambles( HART_MINIMUM_PREAMBLES );
         // hll_set_preambles( HART_DEFAULT_PREAMBLES );
          strcpy( szTag, pDevData->szTag );
          memset( pDevData, 0, sizeof( DEVDATA ) );
          strcpy( pDevData->szTag, szTag );
     }

     /*
      * Use long frame format for Id by Tag or for Rev 5 xmtrs.
      */
     if( cCmd == CMD_READ_UID_BYTAG )
     {
          bLongFrame = TRUE;
     }
     else if( pDevData->cUniversalRev >= 5 )
     {
          bLongFrame = TRUE;
     }

     bPrimary = hll_get_master();

     /*
      * Start building the header with the delimiter.
      */
     pData = acMessage;
     *pData++ = bLongFrame ? 0x82 : 0x02;

     /*
      * Next is the address. Set the high bit if primary master and clear
      * the burst mode bit.
      */
     if( bLongFrame )
     {
          *pData = (BYTE)(pDevData->cManufacturer & 0x3F);
          if( bPrimary )
          {
               *pData = (BYTE)(*pData | 0x80);
          }
          pData++;
          *pData++ = (BYTE)pDevData->cDevice ;
          hutil_long_to_hartbuf( pData, pDevData->dwDeviceId );
          pData += 3;
     }
     else
     {
          *pData = pDevData->cPollAddr;
          if( bPrimary )
          {
               *pData |= 0x80;
          }
          pData++;
     }

     *pData = cCmd;
     *ppCnt = ++pData;

     if( pbLongFrame )
     {
          *pbLongFrame = bLongFrame;
     }
}

/**************************************************************************
 * FUNCTION: happ_format_command
 *
 * DESCRIPTION:
 *   Formats the given HART command number.  This function must be tailored
 * to work with the set of commands that will be encountered by the HART
 * master.
 *
 * ARGUMENTS:
 *   acMessage      ptr to buffer for formatted message.
 *   pcLength       ptr to location to store length of created message.
 *   cCmd           command number to format.
 *   pDevData       pointer to DevData struct where xmtr data is located.
 *
 * RETURNS:
 *   STATUS_OK      no error
 *   STATUS_ERROR   error in formatting command
 ***************************************************************************/

FSTAT happ_format_command( PBYTE acMessage, PBYTE pcLength, BYTE cCmd, PDEVDATA pDevData )
{
     FSTAT     nStatus = STATUS_OK;
     BOOL      bLongFrame;
     PBYTE     pCountByte;
     PBYTE     pCmdByte;
     PBYTE     pData;

     happ_format_header( acMessage, &pCountByte, cCmd, pDevData, &bLongFrame );


     /*
      * Set the pointers to other fields.
      */
     pCmdByte = pCountByte - 1;
     pData = pCountByte + 1;

     /*
      * Command specific data.
      */
	 /* Commenting the switch case to avoid the encoding here.
		Instead of this, the data is used directly. */
 #if 0
     switch( cCmd )
     {
     case CMD_READ_UID:
     case CMD_READ_PV:
     case CMD_READ_PVMA:
     case CMD_READ_VARS:
          *pCountByte = 0;
          break;

     case CMD_WRITE_POLLADDR:
          *pCountByte = 1;
          *pData++ = pDevData->cPollAddrNew;
          break;

     case CMD_READ_UID_BYTAG:
          *pCountByte = 6;
          hutil_pad_with_spaces( pDevData->szTag, HART_TAG_LEN );
          hutil_pack_ascii( pData, pDevData->szTag );
          pData += HART_TAG_LEN / 4 * 3;
          break;

     case CMD_READ_MESSAGE:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 4 block 0.
                */
               *pCountByte = 1;
               *pCmdByte = 4;
               *pData++ = 0;
          }
          else
          {
               *pCountByte = 0;
          }
          break;

     case CMD_READ_TAG_DESC_DATE:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 4 block 1.
                */
               *pCountByte = 1;
               *pCmdByte = 4;
               *pData++ = 1;
          }
          else
          {
               *pCountByte = 0;
          }
          break;

     case CMD_READ_PV_SENSOR_INFO:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 4 block 2.
                */
               *pCountByte = 1;
               *pCmdByte = 4;
               *pData++ = 2;
          }
          else
          {
               *pCountByte = 0;
          }
          break;

     case CMD_READ_PV_OUTPUT_INFO:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 4 block 3.
                */
               *pCountByte = 1;
               *pCmdByte = 4;
               *pData++ = 3;
          }
          else
          {
               *pCountByte = 0;
          }
          break;

     case CMD_READ_FAN:
          if( !bLongFrame )
          {
               /*
                * FAN didn't exist before revision 5.
                */
               nStatus = STATUS_ERROR;
          }
          else
          {
               *pCountByte = 0;
          }
          break;


     case CMD_WRITE_MESSAGE:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 5 block 0.
                */
               *pCountByte = 25;
               *pCmdByte = 5;
               *pData++ = 0;
          }
          else
          {
               *pCountByte = 24;
          }
          hutil_pad_with_spaces( pDevData->szMessage, HART_MESSAGE_LEN );
          hutil_pack_ascii( pData, pDevData->szMessage );
          pData += HART_MESSAGE_LEN / 4 * 3;
          break;

     case CMD_WRITE_TAG_DES_DATE:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 5 block 1.
                */
               *pCountByte = 25;
               *pCmdByte = 5;
               *pData++ = 1;
          }
          else
          {
               *pCountByte = 21;
          }
          hutil_pad_with_spaces( pDevData->szTag, HART_TAG_LEN );
          hutil_pack_ascii( pData, pDevData->szTag );
          pData += HART_TAG_LEN / 4 * 3;

          hutil_pad_with_spaces( pDevData->szDescript, HART_DESCRIPT_LEN );
          hutil_pack_ascii( pData, pDevData->szDescript );
          pData += HART_DESCRIPT_LEN / 4 * 3;

          *pData++ = pDevData->acDate[0];
          *pData++ = pDevData->acDate[1];
          *pData++ = pDevData->acDate[2];
          if( !bLongFrame )
          {
               *pData++ = 250;
               *pData++ = 250;
               *pData++ = 250;
          }
          break;

     case CMD_WRITE_FAN:
          if( !bLongFrame )
          {
               /*
                * Substitute Rev 4 command 5 block 4.
                */
               *pCountByte = 25;
               *pCmdByte = 5;
               *pData++ = 4;
          }
          else
          {
               *pCountByte = 3;
          }
          hutil_long_to_hartbuf( pData, pDevData->dwFinalAssyNum );
          pData+= 3;
          break;

     case CMD_WRITE_PV_DAMPING:
          *pCountByte = 4;
          hutil_float_to_hartbuf( pData, pDevData->rDamping );
          pData += 4;
          break;

     case CMD_WRITE_PV_RANGE:
          *pCountByte = 9;
          *pData++ = pDevData->cRangeUnits;
          hutil_float_to_hartbuf( pData, pDevData->rRangeUpper );
          pData += 4;
          hutil_float_to_hartbuf( pData, pDevData->rRangeLower );
          pData += 4;
          break;

     case CMD_RESET_CFG_CHANGE:
          *pCountByte = 0;
          break;

     case CMD_BURN_EEPROM:
          *pCountByte = 1;
          *pData++ = 0;
          break;

     case CMD_SELF_TEST:
     case CMD_MASTER_RESET:
          *pCountByte = 0;
          break;

     case CMD_WRITE_PV_UNITS:
          *pCountByte = 1;
          *pData++ = pDevData->cRangeUnits;
          break;

     case CMD_WRITE_PV_TRANSFER_FUNC:
          *pCountByte = 1;
          *pData++ = pDevData->cTransferFunc;
          break;

     case CMD_READ_MORE_STATUS:
          *pCountByte = 0;
          break;

     case CMD_WRITE_SENSOR_SN:
          *pCountByte = 3;
          hutil_long_to_hartbuf( pData, pDevData->dwSensorSerialNum );
          pData += 3;
          break;

     case CMD_WRITE_BURST_COMMAND:
          *pCountByte = 1;
          *pData++ = pDevData->cBurstCmd;
          break;

     case CMD_BURST_MODE_CONTROL:
          *pCountByte = 1;
          *pData++ = pDevData->bBurst;
          break;

     case CMD_SET_PV_UPPER_RANGE:
     case CMD_SET_PV_LOWER_RANGE:
     case CMD_SET_PV_ZERO:
          *pCountByte = 0;
          break;

     case CMD_FIXED_PV_CURRENT:
          *pCountByte = 4;
          // TODO  must get this data from somewhere.
          hutil_float_to_hartbuf( pData, 0.0 );
          pData += 4;
          break;

     case CMD_TRIM_PV_DAC_ZERO:
          *pCountByte = 4;
          // TODO  must get this data from somewhere.
          hutil_float_to_hartbuf( pData, 0.0  );
          pData += 4;
          break;

     case CMD_TRIM_PV_DAC_GAIN:
          *pCountByte = 4;
          // TODO  must get this data from somewhere.
          hutil_float_to_hartbuf( pData, 0.0 );
          pData += 4;
          break;

     default:
// L&T Modifications : Real Tx. Support - start
          //nStatus = STATUS_ERROR;
        dbgPrint("Command number not in switch cases\n");
		*pCountByte = 0;
		break;
// L&T Modifications : Real Tx. Support - end
     }
#endif

     *pCountByte = pDevData->DataCount;
     memcpy(pData, pDevData->acData, pDevData->DataCount);
     pData += pDevData->DataCount;
     if( !nStatus )
     {
          /*
           * Checksum
           */
          hutil_checksum( acMessage, pData );
          hutil_message_length( acMessage, pcLength );
     }

     return( nStatus );
}


/**************************************************************************
 * FUNCTION: happ_interpret_response
 *
 * DESCRIPTION:
 *   Interprets the response received from an xmtr by moving the message
 * data into the DevData structure.
 *
 * ARGUMENTS:
 *   acMessage      ptr to buffer containing HART response from xmtr.
 *   pDevData       pointer to DevData struct for xmtr data.
 *
 * RETURNS:
 *   STATUS_OK      no error
 *   STATUS_ERROR   error interpreting response
 *   STATUS_BADCMD  the command number in the message is not handled by this
 *                  function.
 ***************************************************************************/

FSTAT happ_interpret_response( PBYTE acMessage, PDEVDATA pDevData )
{
     BOOL      bLongFrame;
     WORD      wExtra;
     BYTE      cCmd;
     BYTE      cDataCount;
     BYTE      cClass;
     PBYTE     pResponse;
     PBYTE     pData;


     /*
      * Decompose header to get info and pointers.
      */
     bLongFrame = *acMessage & 0x80;
     if( bLongFrame )
     {
          wExtra = (*acMessage & 0x60) >> 5;
          cCmd   = acMessage[6];
          pData  = &acMessage[ 7 + wExtra ];
          cDataCount = *pData++;
          pResponse = pData;
          pData += 2;
     }
     else
     {
          wExtra = 0;
          cCmd   = acMessage[2];
          cDataCount = acMessage[ 3 ];
          pResponse = &acMessage[ 4 ];
          pData = pResponse + 2;
     }

     /*
      * record the status of burst mode.
      */
     pDevData->bBurst = acMessage[1] & 0x40 ? TRUE : FALSE;

     /*
      * Must have some data to be valid for interpretation.
      */
     if( cDataCount < 2 )
     {
          return( STATUS_ERROR );
     }

     /*
      * Store the response codes, but only
      * parse data messages that xmtr says are OK.
      */
     pDevData->cCmd = cCmd;
     pDevData->cCmdResponse = *pResponse;
     pDevData->cDeviceStatus = *(pResponse +1);

    /* The below function has a switch case for limited response codes.
        It fills cClass with RSP_CMD_ERROR for the response codes which are
        not mapped in the switch cases and returns from the function.
        Hence Commenting the below function call and the if condition,
        to avoid return of the function because of the different response
        code(even if the command response is correct).
    */
#if 0
     hutil_response_code_type( *pResponse, &cClass );
     if( (cClass != RSP_NO_ERROR)  &&  (cClass != RSP_CMD_WARNING) )
     {
          return( STATUS_ERROR );
     }
#endif

     /*
      * Parse the data.
      */
     switch( cCmd )
     {
     case CMD_READ_UID:
     case CMD_READ_UID_BYTAG:
          /*
           * 254 indicates the presence of an expanded device type code.
           * The byte following the expansion code contains the Manuf's Id Code.
           */
          if( *pData == 254 )
          {
               pData++;
               pDevData->cManufacturer = *pData++;
          }
          else
          {
               /*
                * Don't know who it is, so assume Rosemount for older
                * HART devices.
                */
               pDevData->cManufacturer = ROSEMOUNT_MFID;
          }
          pDevData->cDevice        = *pData++;
          pDevData->cPreambles     = *pData++;
          pDevData->cUniversalRev  = *pData++;
          pDevData->cSpecificRev   = *pData++;
          pDevData->cSoftwareRev   = *pData++;
          pDevData->cHardwareRev   = *pData++;
          pDevData->cFlags         = *pData++;
          hutil_hartbuf_to_long( &pDevData->dwDeviceId, pData );
          pData += 3;
          if( pDevData->cUniversalRev <= 4 )
          {
               pDevData->dwFinalAssyNum = pDevData->dwDeviceId;
          }
          break;

     case CMD_READ_PV:
          pDevData->cPVUnits = *pData++;
          hutil_hartbuf_to_float( &pDevData->rPV, pData );
          break;

     case CMD_READ_PVMA:
          hutil_hartbuf_to_float( &pDevData->rMa, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rPVPctRange, pData );
          break;

     case CMD_READ_VARS:
          hutil_hartbuf_to_float( &pDevData->rMa, pData );
          pData += 4;
          pDevData->cPVUnits = *pData++;
          hutil_hartbuf_to_float( &pDevData->rPV, pData );
          pData += 4;
          /*
           * The rest don't have to be here.
           */
          pDevData->cSVUnits = 250;
          hutil_set_nan( &pDevData->rSV );
          pDevData->cTVUnits = 250;
          hutil_set_nan( &pDevData->rTV );
          pDevData->cQVUnits = 250;
          hutil_set_nan( &pDevData->rQV );
          if( cDataCount > 11 )
          {
               pDevData->cSVUnits = *pData++;
               hutil_hartbuf_to_float( &pDevData->rSV, pData );
               pData += 4;
          }
          if( cDataCount > 16 )
          {
               pDevData->cTVUnits = *pData++;
               hutil_hartbuf_to_float( &pDevData->rTV, pData );
               pData += 4;
          }
          if( cDataCount > 21 )
          {
               pDevData->cQVUnits = *pData++;
               hutil_hartbuf_to_float( &pDevData->rQV, pData );
          }
          break;

     case 4: /* HART Revision 4 read common static data */
          extract_block_4( pData +1, pDevData, *pData, cDataCount - 3 );
          break;

     case 5: /* HART Revision 4 write common static data */
          extract_block_5( pData +1, pDevData, *pData, cDataCount - 3 );
          break;

     case CMD_WRITE_POLLADDR:
          pDevData->cPollAddr = *pData;
          break;

     case CMD_READ_MESSAGE:
          extract_block_4( pData, pDevData, 0, cDataCount - 2 );
          break;

     case CMD_READ_TAG_DESC_DATE:
          extract_block_4( pData, pDevData, 1, cDataCount - 2 );
          break;

     case CMD_READ_PV_SENSOR_INFO:
          extract_block_4( pData, pDevData, 2, cDataCount - 2 );
          break;

     case CMD_READ_PV_OUTPUT_INFO:
          extract_block_4( pData, pDevData, 3, cDataCount - 2 );
          break;

     case CMD_READ_FAN:
          extract_block_5( pData, pDevData, 4, cDataCount - 2 );
          break;

     case CMD_WRITE_MESSAGE:
          extract_block_5( pData, pDevData, 0, cDataCount - 2 );
          break;

     case CMD_WRITE_TAG_DES_DATE:
          extract_block_5( pData, pDevData, 1, cDataCount - 2 );
          break;

     case CMD_WRITE_FAN:
          extract_block_5( pData, pDevData, 4, cDataCount - 2 );
          break;

     case CMD_WRITE_PV_RANGE:
          pDevData->cRangeUnits = *pData;
          pData++;
          hutil_hartbuf_to_float( &pDevData->rRangeUpper, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rRangeLower, pData );
          break;

     case CMD_RESET_CFG_CHANGE:
     case CMD_BURN_EEPROM:
     case CMD_SELF_TEST:
     case CMD_MASTER_RESET:
          break;

     case CMD_WRITE_PV_UNITS:
          pDevData->cRangeUnits = *pData;
          break;

     case CMD_WRITE_PV_TRANSFER_FUNC:
          pDevData->cTransferFunc = *pData;
          break;

     case CMD_READ_MORE_STATUS:
          pDevData->cStatusByteCount = cDataCount - 2;
          if( pDevData->cStatusByteCount >= sizeof( pDevData->acMoreStatus ) )
          {
               pDevData->cStatusByteCount = sizeof( pDevData->acMoreStatus );
          }
          memcpy( pDevData->acMoreStatus, pData, pDevData->cStatusByteCount );
          break;

     case CMD_WRITE_SENSOR_SN:
          hutil_hartbuf_to_long( &pDevData->dwSensorSerialNum, pData );
          break;

     case CMD_WRITE_BURST_COMMAND:
          pDevData->cBurstCmd = *pData;
          break;

     case CMD_BURST_MODE_CONTROL:
          pDevData->bBurst = *pData;
          break;

     case CMD_SET_PV_UPPER_RANGE:
          break;

     case CMD_SET_PV_LOWER_RANGE:
          break;

     case CMD_FIXED_PV_CURRENT:
          // TODO hutil_hartbuf_to_float( &WhereToPutData, pData );
          pData += 4;
          break;

     case CMD_SET_PV_ZERO:
          break;

     case CMD_TRIM_PV_DAC_ZERO:
          // TODO hutil_hartbuf_to_float( &WhereToPutData, pData );
          pData += 4;
          break;

     case CMD_TRIM_PV_DAC_GAIN:
          // TODO hutil_hartbuf_to_float( &WhereToPutData, pData );
          pData += 4;
          break;

     default:
          // Commenting the below return and return Success for all the commands.
          // return( STATUS_BADCMD );
          break;
     }

     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: extract_block_4
 *
 * DESCRIPTION:
 *   Extracts data from command 4 blocks
 *
 * ARGUMENTS:
 *   pData          ptr to data (past block number for rev 4).
 *   pDevData       ptr to xmtr data structure.
 *   cBlock         command 4 block number.
 *   wDataCount     Count of the number of data bytes to process.
 *                  (does not include status bytes or block number byte)
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void extract_block_4( PBYTE pData, PDEVDATA pDevData, BYTE cBlock, WORD wDataCount )
{
     switch( cBlock )
     {
     case 0: /* message */
          hutil_unpack_ascii( pDevData->szMessage, pData, 24 );
          pDevData->szMessage[ HART_MESSAGE_LEN ] = 0;
          break;

     case 1: /* tag, description, date */
          hutil_unpack_ascii( pDevData->szTag, pData, 6 );
          pDevData->szTag[ HART_TAG_LEN ] = 0;
          pData += 6;
          hutil_unpack_ascii( pDevData->szDescript, pData, 12 );
          pDevData->szDescript[ HART_DESCRIPT_LEN ] = 0;
          pData += 12;
          pDevData->acDate[0] = *pData++;
          pDevData->acDate[1] = *pData++;
          pDevData->acDate[2] = *pData++;
          break;

     case 2: /* PV sensor info */
          hutil_hartbuf_to_long( &pDevData->dwSensorSerialNum, pData );
          pData += 3;
          pDevData->cLimitUnits = *pData++;
          hutil_hartbuf_to_float( &pDevData->rLimitUpper, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rLimitLower, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rMinimumSpan, pData );
          break;

     case 3: /* PV output info */
          pDevData->cAlarmSelect  = *pData++;
          pDevData->cTransferFunc = *pData++;
          pDevData->cRangeUnits   = *pData++;
          hutil_hartbuf_to_float( &pDevData->rRangeUpper, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rRangeLower, pData );
          pData += 4;
          hutil_hartbuf_to_float( &pDevData->rDamping, pData );
          pData += 4;
          pDevData->cWriteProtect = *pData++;
          if( wDataCount >= 17  &&  *pData < 250 )
          {
               pDevData->cPrivateLabel = *pData;
          }
          else
          {
               pDevData->cPrivateLabel = pDevData->cManufacturer;
          }
          break;
     }
}


/**************************************************************************
 * FUNCTION: extract_block_5
 *
 * DESCRIPTION:
 *   Extracts data from command 5 blocks
 *
 * ARGUMENTS:
 *   pData          ptr to data (past block number for rev 4).
 *   pDevData       ptr to xmtr data structure.
 *   cBlock         command 5 block number.
 *   wDataCount     Count of the number of data bytes to process.
 *                  (does not include status bytes or block number byte)
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

static void extract_block_5( PBYTE pData, PDEVDATA pDevData, BYTE cBlock, WORD wDataCount )
{
     switch( cBlock )
     {
     case 0: /* message */
          extract_block_4( pData, pDevData, 0, wDataCount );
          break;

     case 1: /* tag, description, date */
          extract_block_4( pData, pDevData, 1, wDataCount );
          break;

     case 4: /* final assembly number */
          hutil_hartbuf_to_long( &pDevData->dwFinalAssyNum, pData );
          if( pDevData->cUniversalRev <= 4 )
          {
               pDevData->dwDeviceId = pDevData->dwFinalAssyNum;
          }
          break;
     }
}
