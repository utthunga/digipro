/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: mdlldemo.c $
 * $Archive: /SwRICode/HMaster/src/mdlldemo.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   This is the demonstration file for the HART Master Data Link Layer in C.
 * This file has been tailored for the 68HC11 using the IAR Systems
 * C Compiler.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: mdlldemo.c $
 * 
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:45a
 * Updated in $/SwRICode/HMaster/src
 * Changed startup to read an I/O pin for primary/secondary master
 * selection.
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 10/21/96   Time: 1:01p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Minor updates.
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 6/14/96    Time: 2:37p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/

#include "io6811.h"
#include "intr6811.h"
#include <stdio.h>
#include "hartmdll.h"
#include "mdlldemo.h" // L&T Modification 
#include "pthread.h"

int send_receive( BYTE cCmd );

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/

/***************************************************************************
 * Private Data elements for this file
 ***************************************************************************/
/* #if 0
static DEVDATA   vDeviceData;
static BYTE      vacCommand[ MAX_HART_MSG_LEN ];
static BYTE      vacResponse[ MAX_HART_MSG_LEN ];
static LINKSTATS vLStat;
#endif
*/
/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/
/*#if 0
static int find_transmitter( void );
static int identify_transmitter( void );
static int send_receive( BYTE cCmd );
#endif
*/
/**************************************************************************
 * FUNCTION: main
 *
 * DESCRIPTION:
 *   This is where the test program starts.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   never
 ***************************************************************************/

#if 0
void main(void)
{
     /*
      * Initialize the required registers.
      * The following two functions should be called before any other
      * HART related work is performed.
      */
     mc_initialization();
     hll_initialize_hart();
     enable_interrupt();

     /*
      * Initialize the Link Layer control.
      * The default settings in the Link Layer are controled by constants
      * in hartmdll.h.
      */
     hll_set_retries( 2 );
     hll_clear_statistics();

     /*
      * Check PA1 to see whether to set for primary or secondary master.
      */
     if( PORTA & 0x02 )
     {
          hll_set_master( HART_PRIMARY );
     }
     else
     {
          hll_set_master( HART_SECONDARY );
     }

     /*
      * Start a perpetual loop that tries to identify a transmitter
      * starting at poll address 0 and incrementing to 15 until one is found.
      * If one is found, then we issue commands 13 and 15 to read info,
      * then repeatedly issue command 3 until communication is lost.
      * Any bad response at any time starts the search process over.
      */
     while( TRUE )
     {
          if( !find_transmitter() )
          {
               if( !identify_transmitter() )
               {
                    while( !send_receive( CMD_READ_VARS ) );
               }
          }
     }
}
#endif

/**************************************************************************
 * FUNCTION: find_transmitter
 *
 * DESCRIPTION:
 *   Searches for a transmitter beginning at poll address 0.
 *   Uses the global vDeviceData structure to track the poll address.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   STATUS_OK      transmitter found
 *   STATUS_ERROR   transmitter not found
 ***************************************************************************/

int find_transmitter(BYTE cPoll)
{
     int  nStatus = STATUS_ERROR;
     vDeviceData.cPollAddr = cPoll;

     if( !send_receive( CMD_READ_UID ) )
     {
        nStatus = STATUS_OK;
     }


     /*
      * Found a transmitter.
      * When the happ_format_command function sees that it is building
      * a command 0 or 11, it automatically sets the preambles to the
      * default value.  Once an instrument is found, you are responsible
      * for setting the the link layer to use the correct number of preambles.
      */
     if( !nStatus )
     {
          hll_set_preambles( vDeviceData.cPreambles );
     }

     return( nStatus );
}

/**************************************************************************
 * FUNCTION: identify_transmitter
 *
 * DESCRIPTION:
 *   Issues a variety of universal commands to the transmitter.
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   STATUS_OK      transmitter read ok.
 *   STATUS_ERROR   transmitter did not respond correctly.
 ***************************************************************************/

int identify_transmitter( void )
{
     int  nStatus = STATUS_ERROR;

     do
     {
          if( send_receive( CMD_READ_MESSAGE ) )
          {
               break;
          }
          if( send_receive( CMD_READ_TAG_DESC_DATE ) )
          {
               break;
          }
          if( send_receive( CMD_READ_PV_SENSOR_INFO ) )
          {
               break;
          }
          if( send_receive( CMD_READ_PV_OUTPUT_INFO ) )
          {
               break;
          }
          if( send_receive( CMD_READ_FAN ) )
          {
               break;
          }
          if( send_receive( CMD_READ_MORE_STATUS ) )
          {
               break;
          }
          nStatus = STATUS_OK;

     } while( FALSE );

     return( nStatus );
}


/**************************************************************************
 * FUNCTION: send_receive
 *
 * DESCRIPTION:
 *   Builds and sends a command, then waits for the response.  If response
 * is received ok, then it is processed into vDeviceData before returning.
 *
 * ARGUMENTS:
 *   cCmd           command to send.
 *
 * RETURNS:
 *   STATUS_OK      received a valid response
 *   STATUS_ERROR   no answer or invalid response
 ***************************************************************************/

int send_receive( BYTE cCmd )
{
     pthread_mutex_lock(&mutex);
     int  nStatus = STATUS_ERROR;
     BYTE cLength;
     BYTE cId;

     if( happ_format_command( vacCommand, &cLength, cCmd, &vDeviceData ) )
     {
         pthread_mutex_unlock(&mutex);
          return( STATUS_ERROR );
     }

     if( hll_send_command( &cId, vacCommand, cLength ) )
     {
         pthread_mutex_unlock(&mutex);
          return( STATUS_ERROR );
     }

     do
     {
         // TODO(Bharath & Santhosh): SDC-625 threading issue to be fixed
          nStatus = hll_read_response( cId, vacResponse );
          if( !nStatus )
          {
               /*
                * got a valid response.
                */
               nStatus = happ_interpret_response( vacResponse, &vDeviceData );
               break;
          }
     } while( nStatus == STATUS_WORKING );

     pthread_mutex_unlock(&mutex);
     return( nStatus );
}

// L&T Modifications : Real Tx. Support - start
DEVDATA* read_data(int CmdNum)
{
    int test = 0;
    if(CmdNum == 12)
    {
        do
        {
            test = send_receive( CMD_READ_MESSAGE );
        }while(test);
    }

    if(CmdNum == 13)
    {
        do
        {
            test = send_receive( CMD_READ_TAG_DESC_DATE );
        }while(test);
    }

    if(CmdNum == 0)
    {
        do
        {
            test = send_receive( CMD_READ_UID );
        }while(test);
    }
    return &vDeviceData;
}

void write_data(int CmdNum, DEVDATA* vDeviceData2)
{
    int test = 0;
    if(CmdNum == 17)
    {
        memcpy(vDeviceData.szMessage, vDeviceData2->szMessage, sizeof(vDeviceData.szMessage));
        do
        {
            test = send_receive( CMD_WRITE_MESSAGE );
        }while(test);
    }

    if(CmdNum == 18)
    {
        memcpy(vDeviceData.szTag, vDeviceData2->szTag, sizeof(vDeviceData.szTag));
        memcpy(vDeviceData.szDescript, vDeviceData2->szDescript, sizeof(vDeviceData.szDescript));
        memcpy(vDeviceData.acDate, vDeviceData2->acDate, sizeof(vDeviceData.acDate));

        do
        {
            test = send_receive( CMD_WRITE_TAG_DES_DATE );
        }while(test);
    }

    if(CmdNum == 6)
    {
        vDeviceData.cPollAddrNew = vDeviceData2->cPollAddrNew;

        do
        {
            test = send_receive( CMD_WRITE_POLLADDR );
        }while(test);
    }
}
// L&T Modifications : Real Tx. Support - end


