/*TITLE intr6811.h - */
/*SUBTITLE File header */
/*BOTLIN Prometer Software*/
/*AUTHOR David Dyck */
/*****************************************************************************
 

    Copyright (c) 1998 FLUKE Corporation.   All Rights Reserved.      

    This unpublished software contains the trade secrets and confidential   
    proprietary information of FLUKE.  Unless otherwise provided in a       
    Software Agreement associated herewith, it is nonexclusively licensed   
    in confidence "AS IS" and is not to be reproduced, in whole or part, by 
    any means except for backup.  Use, duplication, or disclosure by the    
    Government is subject to the restrictions in paragraph (c)(1)(ii) of    
    the Rights in Technical Data and Computer Software clause in DFAR       
    52.227-7013 (May 1987) Software owned by the Fluke Corporation,         
    6920 Seaway Blvd., Everett, WA, 98206.  Contract No.  ________________).

    $Source: /evtfs/nfswkgrp/hobbes/sw/family/dpc/src/hart/RCS/intr6811.h,v $
    $Revision: 1.2 $
    $Author: dcd $
    $Date: 1998/02/14 18:45:54 $

    Revision history is available via the RCS command 'rlog'.
 
*****************************************************************************/
/*DESIGN**********************************************************************

  >CODE REVIEWS

    DATE---------VERSION--ATTENDEES-------------------------------------------
    YYMMDDHHMM   X.Y      WHO

*****************************************************************************/
#ifndef intr6811_h
#define	intr6811_h		/* Test this to determine if this header is included. */

#if defined(__linux__) || defined(sparc)

// L&T Modifications : Real Tx. Support - start
#ifdef __cplusplus
extern "C" {
extern void disable_interrupt();
extern void enable_interrupt();
}
#endif
// L&T Modifications : Real Tx. Support - end

#elif defined(mc68000)

#include <std.h>

#if !defined(NO_KERNEL)
    #define KDRIVER                     /* This module is a kernel driver */
    #include <kernel.h>
#endif

#define disable_interrupt() { int level = KdisIntr();
#define enable_interrupt()  KrestIntr(level); }

#endif

#endif /* intr6811_h */
