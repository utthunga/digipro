/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: hutil.c $
 * $Archive: /SwRICode/HMaster/src/hutil.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   HART Utility user functions.  These are part of the services provided by
 * the HART Master Data Link Layer module.  Since these are user functions,
 * their prototypes are in hartmdll.h.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: hutil.c $
 * 
 * *****************  Version 7  *****************
 * User: Kholladay    Date: 5/15/98    Time: 4:48p
 * Updated in $/SwRICode/HMaster/src
 * Fix reentry problem.
 *
 * *****************  Version 6  *****************
 * User: Kholladay    Date: 4/17/98    Time: 1:59p
 * Updated in $/SwRICode/HMaster/src
 * Replace numbers with definitions.
 *
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:38a
 * Updated in $/SwRICode/HMaster/src
 * New define for MUST_ALIGN in float copying utilities.
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/21/96   Time: 1:00p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Added support for "Not a Number" in floating point interpret.
 * Changed return codes to BYTES for consistency, speed, and size.
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#include <stdio.h>
#include <string.h>
#include "hartmdll.h"


/**************************************************************************
 * FUNCTION: hutil_header_length
 *
 * DESCRIPTION:
 *   Determines the header length of a HART message from the delimiter byte.
 * The length includes the delimiter, address, extra, and command, and
 * count bytes.
 *
 * ARGUMENTS:
 *   cDelim         the delimiter
 *   pcLength       ptr to byte where length will be stored
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_header_length( BYTE cDelim, PBYTE pcLength )
{
     BYTE      cLength;
     BYTE      cExtra;

     if( cDelim & 0x80 )
     {
          /*
           * It is a long frame message.
           * Check delimiter for forward compatibility extra bytes
           * between address and command.
           * Length = 1 byte delimiter + 5 byte address + extra +
           *          1 byte command + 1 byte count
           */
          cExtra = (cDelim & 0x60) >> 5;
          cLength = 8 + cExtra;
     }
     else
     {
          /*
           * It is a short frame message.
           * Length = 1 byte delimiter + 1 byte address +
           *          1 byte command + 1 byte count
           */
          cLength = 4;
     }

     *pcLength = cLength;

     return( 0 );
}


/**************************************************************************
 * FUNCTION: hutil_message_length
 *
 * DESCRIPTION:
 *   Determines the length of a HART message.  Assumes the message is
 * correctly formatted.
 *
 * ARGUMENTS:
 *   acMessage      ptr to buffer containing HART message
 *   pcLength       ptr to byte where length will be stored
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_message_length( PBYTE acMessage, PBYTE pcLength )
{
     BYTE      cHeaderLength;

     hutil_header_length( *acMessage, &cHeaderLength );

     /*
      * The last byte of the header is the data count, then need 1 more
      * byte for the checksum.
      */
     *pcLength = cHeaderLength + 1 + *(acMessage + cHeaderLength -1);

     return( 0 );
}


/**************************************************************************
 * FUNCTION: hutil_checksum
 *
 * DESCRIPTION:
 *   Calculates the checksum for a HART message
 *
 * ARGUMENTS:
 *   acMessage      ptr to buffer containing HART message
 *   pcCheckSum     ptr to byte where checksum will be stored
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_checksum( PBYTE acMessage, PBYTE pcCheckSum )
{
     BYTE cLength;
     BYTE cIdx;
     BYTE cSum;

     hutil_message_length( acMessage, &cLength );
     cLength--;

     for( cSum = 0, cIdx = 0; cIdx < cLength; cIdx ++ )
     {
          cSum ^= *acMessage;
          acMessage++;
     }

     *pcCheckSum = cSum;

     return( 0 );
}


/**************************************************************************
 * FUNCTION: hutil_response_code_type
 *
 * DESCRIPTION:
 *   Determines the classification of the response code from a slave device.
 *
 * ARGUMENTS:
 *   cCode          The first byte of the response code from the slave.
 *   pcClass        Pointer to location to store classification.
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_response_code_type( BYTE cCode, PBYTE pcClass )
{

     if( !cCode )
     {
          *pcClass = RSP_NO_ERROR;
     }
     else if( cCode & 0x80 )
     {
          *pcClass = RSP_COMM_ERROR;
     }
     else if( cCode == RSP_DEVICE_BUSY )
     {
          *pcClass = RSP_DEVICE_BUSY;
     }
     else if( cCode == RSP_CMD_NOTIMPLEMENTED )
     {
          *pcClass = RSP_CMD_NOTIMPLEMENTED;
     }
     else if( cCode == RSP_DR_INITIATE )
     {
          *pcClass = RSP_DR_INITIATE;
     }
     else if( cCode == RSP_DR_RUNNING )
     {
          *pcClass = RSP_DR_RUNNING;
     }
     else if( cCode == RSP_DR_DEAD )
     {
          *pcClass = RSP_DR_DEAD;
     }
     else if( cCode >= 96 && cCode <=127 )
     {
          *pcClass = RSP_CMD_WARNING;
     }
     else
     {
          switch( cCode )
          {
          case 8:
          case 14:
          case 24:
          case 25:
          case 26:
          case 27:
          case 30:
          case 31:
               *pcClass = RSP_CMD_WARNING;
               break;

          default:
               *pcClass = RSP_CMD_ERROR;
          }
     }

     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_pad_with_spaces
 *
 * DESCRIPTION:
 *   Extends the length of a null terminated string using spaces.
 * Remember that the buffer length must be at least wLength +1
 *
 * ARGUMENTS:
 *   pszString      ptr to buffer containing the string.
 *   wLength        length desired for string.
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_pad_with_spaces( PBYTE pszString, WORD wLength )
{
     WORD wIdx;

     for( wIdx = strlen( pszString ); wIdx < wLength; wIdx++ )
     {
          pszString[ wIdx ] = ' ';
     }
     pszString[ wIdx ] = 0;

     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_pack_ascii
 *
 * DESCRIPTION:
 *   Translates a null terminated string into a HART packed ASCII string.
 * Only packes closest multiple of 4 bytes to string length.
 *
 * ARGUMENTS:
 *   pPacked        Ptr to buffer for the packed ASCII string.
 *   pszUnpacked    Ptr to buffer containing the null terminated string.
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_pack_ascii( PBYTE pPacked, CPSTR pszUnPacked )
{
     WORD      wIdx;
     WORD      wGroupCnt;
     WORD      wMaxGroups;    /* Number of 4 byte groups to pack. */
     WORD      awBuf[ 4 ];

     wMaxGroups = strlen( pszUnPacked ) / 4;

     for( wGroupCnt = 0; wGroupCnt < wMaxGroups; wGroupCnt++ )
     {
          /*
           * Clear bits 6 and 7 of the next group of 4 to be packed.
           */
          for( wIdx = 0; wIdx < 4; wIdx++ )
          {
               awBuf[ wIdx ]  = *pszUnPacked & 0x3F;
               pszUnPacked++;
          }

          /*
           * Now pack the results.
           */
          *pPacked++ =  (BYTE)(( awBuf[0] << 2) | (awBuf[1] >> 4) );
          *pPacked++ =  (BYTE)(( awBuf[1] << 4) | (awBuf[2] >> 2) );
          *pPacked++ =  (BYTE)(( awBuf[2] << 6) |  awBuf[3] );
     }

     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_unpack_ascii
 *
 * DESCRIPTION:
 *   Translates a HART packed ASCII string into a null terminated string.
 * Only translates the closes multiple of 3 of the packed length.
 *
 * ARGUMENTS:
 *   pszUnpacked    Ptr to buffer for the null terminated string.
 *   pPacked        Ptr to buffer containing the packed ASCII string.
 *   cPackedLength  number of bytes in the packed string.
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_unpack_ascii( PSTR pszUnPacked, const BYTE* pPacked, BYTE cPackedLength )
{
     WORD      wIdx;
     WORD      wGroupCnt;
     WORD      wMaxGroups;    /* Number of 4 byte groups to pack. */
     WORD      wMask;
     WORD      awBuf[ 4 ];


     wMaxGroups = cPackedLength / 3;

     for( wGroupCnt = 0; wGroupCnt < wMaxGroups; wGroupCnt++ )
     {
          /*
           * First unpack 3 bytes into a group of 4 bytes, clearing bits 6 & 7.
           */
          awBuf[0]  = *pPacked >> 2;
          awBuf[1]  = ((*pPacked << 4) & 0x30) | (*(pPacked +1) >> 4);
          awBuf[2]  = ((*(pPacked +1) << 2) & 0x3C) | (*(pPacked +2) >> 6);
          awBuf[3]  = *(pPacked +2) & 0x3F;
          pPacked += 3;

          /*
           * Now transfer to unpacked area, setting bit 6 to complement of bit 5.
           */
          for( wIdx = 0; wIdx < 4; wIdx++)
          {
               wMask = ((awBuf[wIdx] & 0x20) << 1) ^ 0x40;
               *pszUnPacked = (BYTE)( awBuf[wIdx] | wMask );
               pszUnPacked++;
          }
     }
     /*
      * Null terminate the new string.
      */
     *pszUnPacked = 0;

     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_float_to_hartbuf
 *
 * DESCRIPTION:
 *   Copies information from a float variable to a hart data buffer/
 *
 * ARGUMENTS:
 *   pcData         pointer to 4 byte data location
 *   rFloat         float to store
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_float_to_hartbuf( PBYTE pcData, float rFloat )
{
#if defined( BYTESWAP )
     *pcData++ = ((PBYTE)&rFloat)[3];
     *pcData++ = ((PBYTE)&rFloat)[2];
     *pcData++ = ((PBYTE)&rFloat)[1];
     *pcData++ = ((PBYTE)&rFloat)[0];
#elif defined( MUST_ALIGN )
     *pcData++ = ((PBYTE)&rFloat)[0];
     *pcData++ = ((PBYTE)&rFloat)[1];
     *pcData++ = ((PBYTE)&rFloat)[2];
     *pcData++ = ((PBYTE)&rFloat)[3];
#else
     *((PFLOAT)pcData) = rFloat;
#endif
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_hartbuf_to_float
 *
 * DESCRIPTION:
 *   Copies information from the hart data input to a float variable
 *
 * ARGUMENTS:
 *   prFloat        pointer to float location
 *   pcData         pointer to 4 byte data location
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_hartbuf_to_float( PFLOAT prFloat, PBYTE pcData )
{
#if defined( BYTESWAP )
     ( (PBYTE)prFloat )[3] = *pcData++;
     ( (PBYTE)prFloat )[2] = *pcData++;
     ( (PBYTE)prFloat )[1] = *pcData++;
     ( (PBYTE)prFloat )[0] = *pcData++;
#elif defined( MUST_ALIGN )
     ( (PBYTE)prFloat )[0] = *pcData++;
     ( (PBYTE)prFloat )[1] = *pcData++;
     ( (PBYTE)prFloat )[2] = *pcData++;
     ( (PBYTE)prFloat )[3] = *pcData++;
#else
     *prFloat = *((PFLOAT)pcData);
#endif
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_long_to_hartbuf
 *
 * DESCRIPTION:
 *   Copies the three least significant bytes from a 4 byte long variable
 * into a buffer where a hart command is being constructed.
 *
 * ARGUMENTS:
 *   pcData         pointer to buffer
 *   dwLong         long data to be copied
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_long_to_hartbuf( PBYTE pcData, DWORD dwLong )
{
#if defined( BYTESWAP )
     *pcData++ = ((PBYTE)&dwLong)[2];
     *pcData++ = ((PBYTE)&dwLong)[1];
     *pcData++ = ((PBYTE)&dwLong)[0];
#else
     *pcData++ = ((PBYTE)&dwLong)[1];
     *pcData++ = ((PBYTE)&dwLong)[2];
     *pcData++ = ((PBYTE)&dwLong)[3];
#endif
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_hartbuf_to_long
 *
 * DESCRIPTION:
 *   Copies three bytes from a hart data buffer into a double word variable.
 * Only the three least significant bytes are copied.  The most significant
 * is set to 0.
 *
 * ARGUMENTS:
 *   pdwLong        pointer to long data storage
 *   pcData         pointer to buffer
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_hartbuf_to_long( PDWORD pdwLong, PBYTE pcData )
{
#if defined( BYTESWAP )
     ((PBYTE)pdwLong)[3] = 0;
     ((PBYTE)pdwLong)[2] = *pcData++;
     ((PBYTE)pdwLong)[1] = *pcData++;
     ((PBYTE)pdwLong)[0] = *pcData++;
#else
     ((PBYTE)pdwLong)[0] = 0;
     ((PBYTE)pdwLong)[1] = *pcData++;
     ((PBYTE)pdwLong)[2] = *pcData++;
     ((PBYTE)pdwLong)[3] = *pcData++;
#endif
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_set_nan
 *
 * DESCRIPTION:
 *   Sets a floating point number to the value for NOT A NUMBER.
 *
 * ARGUMENTS:
 *   prFloat
 *
 * RETURNS:
 *   STATUS_OK      always
 ***************************************************************************/

FSTAT hutil_set_nan( PFLOAT prFloat )
{
     *((PDWORD)prFloat) = NOT_A_NUMBER;
     return( STATUS_OK );
}


/**************************************************************************
 * FUNCTION: hutil_is_nan
 *
 * DESCRIPTION:
 *   Determines whether a floating point number is equal to NOT A NUMBER.
 *
 * ARGUMENTS:
 *   rFloat
 *
 * RETURNS:
 *   TRUE       if value is NAN
 *   FALSE      if not
 ***************************************************************************/

BOOL hutil_is_nan( FLOAT rFloat )
{
     if( (*((PDWORD)&rFloat) == NOT_A_NUMBER ) ||
         (*((PDWORD)&rFloat) == 0xFFFFFFFF ) ||
         (*((PDWORD)&rFloat) == 0x7FFFFFFF ) ||
         (*((PDWORD)&rFloat) == 0x7FE00000 ) )
     {
          return( TRUE );
     }
     return( FALSE );
}

