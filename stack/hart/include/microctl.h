/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: microctl.h $
 * $Archive: /SwRICode/MasterDLL/src/microctl.h $
 * $Revision: 1.2 $
 * $Date: 1998/09/15 04:31:34 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1997, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   This is the header file for the 68HC11 microcontroller specific
 * interface to the HART Data Link Layer processor.
 * It also contains configuration constants that can be used to optimize
 * RAM use for a specific target.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: microctl.h $
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 11/12/96   Time: 9:15a
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Fix timer for 8MHz clock
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 10/21/96   Time: 1:12p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Changed default buffer to 50 bytes.
 * Changed timer logic to use faster interrupt.
 * New function prototypes.
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 7/22/96    Time: 4:13p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Updates for Directory change
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:58p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#ifndef MICROCTL_H
#define MICROCTL_H

#include "common.h"

/***************************************************************************
 * RAM Control Parameters
 ***************************************************************************/
/*
 * Maximum HART message length, not including preambles.
 * 8 byte header + 255 bytes data + 1 byte checksum.
 * Most slaves don't respond with over 26 bytes of data, but there are
 * some that use up to 32, and the protocol (Rev 5) does not DEFINE a
 * maximum limit.  Best guess at this time is that the value should be
 * somewhere between 50 and 255.  This code has been written to use byte
 * values for lengths and counts, so do not exceed 255.
 */

/*  CPMHACK: Maximum HART message length 50 affects the command
   if its size is greater than 50. So giving the maximum HART Message
   length 255 */

#define MAX_HART_MSG_LEN      255 /* 50 */

/*
 * Maximum number of outgoing HART messages that can be placed in the
 * Queue.  Only one message can be processed at a time, but in a
 * multitasking system, more than one task may submit a message to the
 * Queue.
 */
#define HART_QUEUE_DEPTH      2


/***************************************************************************
 * Timer Control Parameters
 ***************************************************************************/
/*
 * The following constants must be altered based on the prescaler selected
 * in the startup file and the crystal frequency.  For a divide by 16
 * prescaler, the following values apply.
 *
 *   Crystal  Prescale      Ticks per      Overflow time
 *   (MHz)    Divisor       5 millisec
 *   -------- -------       ----------     -------------
 *   8.0          16              625       524.28
 *   7.3728       16              576       568.89
 *   4.0          16              313      1048.58
 *   3.6864       16              288      1137.78
 *
 *   8.0           0            10000        32.77
 *   7.3728        0             9216        35.56
 *   4.0           0             5000        65.54
 *   3.6864        0             4608        71.11
 */

#define ONE_MS_TICKS      2000
#define FIVE_MS_TICKS    10000
#define TEN_MS_TICKS     ( FIVE_MS_TICKS * 2 )


/***************************************************************************
 * Function Prototypes
 ***************************************************************************/
/*
 * These are microprocessor specific functions used by the generic
 * link layer code.  They must be modified for any changes in hardware.
 */
#ifdef __cplusplus
extern "C" {

int mc_initialization( void );
int mc_shutdown(void);
}
#endif

void mc_set_hart_timer( int nMilliSeconds );

void mc_send_hart_message( BYTE cPreambles, PBYTE acMessage, BYTE cLength );

BYTE mc_hart_read_cd( void );

#endif /* MICROCTL_H */
