/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: common.h $
 * $Archive: /SwRICode/HMaster/src/common.h $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   Common definitions and prototypes required by the Master Data Link Layer.
 * Most of the definitions and prototypes in this file are for internal
 * use by the hart link layer processes.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: common.h $
 * 
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:58p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#ifndef COMMON_H
#define COMMON_H

/***************************************************************************
 * Shortcut Data Types
 ***************************************************************************/

#ifndef STR
typedef char             STR;
typedef char *           PSTR;
typedef const char *     CPSTR;
#endif

#ifndef BYTE
typedef unsigned char    BYTE;
typedef BYTE *           PBYTE;
#endif

#ifndef BOOL
typedef int              BOOL;   // L&T Modification
typedef BOOL *           PBOOL;
#endif

#ifndef WORD
typedef unsigned short   WORD;
typedef WORD *           PWORD;
#endif

#ifndef DWORD
typedef unsigned long    DWORD;
typedef DWORD *          PDWORD;

#endif

#ifndef FLOAT
typedef float            FLOAT;
typedef float *          PFLOAT;
#endif

/*
 * Standard function status return data type
 */
typedef BYTE             FSTAT;


/***************************************************************************
 * Definitions
 ***************************************************************************/

#ifndef TRUE
#define TRUE   -1
#endif

#ifndef FALSE
#define FALSE  0
#endif

#ifndef ON
#define ON     -1
#endif

#ifndef OFF
#define OFF    0
#endif

/*
 * Return codes for functions that return a status.
 */
#define STATUS_OK        0
#define STATUS_WORKING   1
#define STATUS_BUSY      2

#define STATUS_NEXT      10
#define STATUS_SLEEP     11

#define STATUS_ERROR     255
#define STATUS_BADID     254
#define STATUS_BADCMD    253

/*
 * HART Master Types.
 */
#define HART_PRIMARY     1
#define HART_SECONDARY   0

/*
 * Correct NAN.
 */
#define NOT_A_NUMBER     0x7FA00000

/***************************************************************************
 * Data Structures
 ***************************************************************************/

/***************************************************************************
 * Global extern variables.
 ***************************************************************************/

/***************************************************************************
 * Function Prototypes
 ***************************************************************************/

#endif /* COMMON_H */
