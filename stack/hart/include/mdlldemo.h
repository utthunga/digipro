/*TITLE mdlldemo.h - */
/*SUBTITLE File header */
/*AUTHOR L&T */
/*****************************************************************************


    Copyright (c) 1998 FLUKE Corporation.   All Rights Reserved.      

	The following function has to be called, which was earlier
	static to the mdlldemo.c file.
	Hence in order to call this method, this .h file is created
	and the function is made as non-static, i.e removed static
	key word, to access this function
 
*****************************************************************************/
#ifdef __cplusplus
extern "C" {

int find_transmitter( BYTE pollAdress );
int identify_transmitter( void );
int send_receive( BYTE cCmd );
DEVDATA* read_data(int CmdNum);
void write_data(int CmdNum, DEVDATA* vDeviceData2);
}
#endif

DEVDATA   vDeviceData;
BYTE vacCommand[ MAX_HART_MSG_LEN ];
BYTE vacResponse[ MAX_HART_MSG_LEN ];
LINKSTATS vLStat;
