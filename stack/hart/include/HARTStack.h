/*************************************************************************************************
 * Description: This class is designed for establishing communication between
 *              the console application and the real transmitter in Linux.
 *              The class is similar to CdocSimmInfc class. However for
 *              communicating to the real transmitter the hart stack function
 *              calls are invoked.
 * #include "ddlCommSimminfc.h".		
 */
#ifndef REALCOMMINFC_H
#define REALCOMMINFC_H
#include "../../SDC625/DDLBaseComm.h"
class CRealCommInfc : public CbaseComm
{
protected:
    BYTE hstAddr;	// Holds the host address.
public:
    CRealCommInfc(); // Constructor
    virtual ~CRealCommInfc(); // Destructor

    RETURNCODE  initComm(void);	// setup connection (called after instantiation of device)
    void setHostAddress(BYTE ha);  // boolean - set Primary or not (is set secondary)
    void enableComm(void);

	/*  This function invokes find_transmitter() of HART Stack and
    fils the Indentity_t appropriately */
    RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr);

    bool appCommEnabled (void);

    hCPktQueue thePktQueue;
    hPkt* getMTPacket(bool pendOnMT=false);// gets an empty packet for generating a command 
						 // (gives caller ownership of the packet*)
    void  putMTPacket(hPkt*);	// returns the packet memory back to the interface 
			              // (this takes ownership of the packet*)

    RETURNCODE SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus); // puts a packet into the send queue  
										 // (takes ownership of the packet*)

    RETURNCODE SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

    virtual RETURNCODE SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
    // puts a packet into the front of the send queue(takes ownership of the pkt*)

    void  enableAppCommRequests (void);
    void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);

    /* To send a write/read command. In these functions, the hart stack function
	   send_receive() is called. */
    RETURNCODE WriteCommand(unsigned int cmdNum);// Send WriteCommand
    RETURNCODE ReadCommand(unsigned int cmdNum); // Send ReadCommand
protected:
    // format conversion
    RETURNCODE  pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt);
    RETURNCODE  pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen);
};

#endif
