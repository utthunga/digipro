/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: lnkctrl.h $
 * $Archive: /SwRICode/HMaster/src/lnkctrl.h $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   Common definitions and prototypes required by the Master Data Link Layer.
 * Most of the definitions and prototypes in this file are for internal
 * use by the hart link layer processes.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: lnkctrl.h $
 * 
 * *****************  Version 7  *****************
 * User: Kholladay    Date: 5/15/98    Time: 4:48p
 * Updated in $/SwRICode/HMaster/src
 * Fix reentry problem.
 *
 * *****************  Version 6  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:43a
 * Updated in $/SwRICode/HMaster/src
 * Gap timer support.
 *
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 2/06/98    Time: 3:41p
 * Updated in $/SwRICode/HMaster/src
 * Add Gap timer support
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:58p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#ifndef LNKCTRL_H
#define LNKCTRL_H

#include "common.h"
#include "microctl.h"

/***************************************************************************
 * Definitions
 ***************************************************************************/
/*
 * States for queue entries.
 */
#define QSTATE_EMPTY          0
#define QSTATE_WAITING        1
#define QSTATE_WORKING        2
#define QSTATE_DONE           3
#define QSTATE_DEAD           4

/*
 * Events that cause a trip through the HART link layer state machine.
 * The values are chosen to accomodate a bitmask for queued events.
 * The lower numbers are higher priority.
 */
#define EVENT_RCVD_MSG        1    /* received a message to handle */
#define EVENT_XMIT_DONE       2    /* finished sending a message */
#define EVENT_TIMEOUT         4    /* a timer expired */
#define EVENT_XMIT_PENDING    8    /* message to send has been placed in queue */

/*
 * Communication errors.
 */
#define PARITY_ERROR     3
#define FRAMING_ERROR    4
#define OVERRUN_ERROR    5
#define CHECKSUM_ERROR   6
#define NORESPONSE_ERROR 7
#define SOM_ERROR        8

/*
 * Millisecond values for timers.
 */
#define RT1PRI      305
#define RT1SEC      380
#define RT2          75
#define RT3         (RT1SEC - RT1PRI)
#define THOLD        20
#if 0
#define TGAP         20 // SWRI
#else
#define TGAP         50 // wallapp
#endif

/*
 * Bit patterns for delimiters.
 */
#define FRAME_BURST      0x01
#define FRAME_STX        0x02
#define FRAME_ACK        0x06

/***************************************************************************
 * Data Structures
 ***************************************************************************/

/*
 * This data structure is used to collect statistics on the link layer
 * activity.  It can be accessed by hll_ calls defined in hartmdll.h.
 * It is defined here since it must be included in the hart state machine
 * control structure.
 */
typedef struct link_statistics
{
     WORD  wSent;    /* Messages sent by this master  */
     WORD  wPSTX;    /* Messages rcvd from Primary master  */
     WORD  wPACK;    /* Slave acks to Primary master  */
     WORD  wPBACK;   /* Burst acks to Primary master  */
     WORD  wSSTX;    /* Messages rcvd from Secondary master */
     WORD  wSACK;    /* Slave acks to Secondary master */
     WORD  wSBACK;   /* Burst acks to Secondary master */
     WORD  wCommErr; /* framing, parity, or overrun*/
     WORD  wChkErr;  /* msgs with checksum errors  */
     WORD  wSomErr;  /* Invalid Start of Message */
     WORD  wNoResp;  /* Messages from this master with no response */
     WORD  wGapErr;  /* Number of gap timeouts */
     DWORD dwTotal;  /* total messages tallied */

} LINKSTATS, *PLINKSTATS;

/*
 * Queue entry used to track multiple message requests.
 */
typedef struct commq
{
     BYTE      cQState;
     BYTE      cMsgId;
     BYTE      acMessage[ MAX_HART_MSG_LEN ];
     BYTE      cMsgLength;
     BYTE      acResponse[ MAX_HART_MSG_LEN ];
     BYTE      cRspCount;
     BYTE      cRspQuality;   /* STATUS_OK, or one of the comm errors */
} COMMQ, *PCOMMQ;


/*
 * This structure is used to track the operation of the receive state machine.
 */
typedef struct rcv_ctrl
{
     BYTE      cState;        /* current state                         */
     BOOL      bGapActive;    /* True if Gap timer is running         */
     BYTE      cTemp;         /* counter for header and data locations */
     BYTE      cCheckSum;     /* checksum accumulator                  */
     BYTE      cRspCount;     /* count of received characters          */
     BYTE      cRspQuality;   /* STATUS_OK, or one of the comm errors  */
     BYTE      acResponse[ MAX_HART_MSG_LEN ];  /* recieve buffer      */
} RCV_CTRL, *PRCV_CTRL;

/*
 * The Generic HART link layer processor call functions to perform tasks
 * that are specific to the link that it is servicing.  These include:
 * 1.  deterimining if a message is ready to send
 * 2.  sending a message
 * 3.  setting timer values
 * 4.  checking carrier detect status
 */
typedef FSTAT (*PGETQFUNC)( PCOMMQ * );
typedef void (*PSENDFUNC)( BYTE, PBYTE, BYTE );
typedef void (*PTMRFUNC)( int );
typedef BYTE (*PCDFUNC)( void );

/*
 * This structure is used to track the operation of the HART state machine.
 */
typedef struct hart_ctrl
{
     /*
      * These values control the operation of the state machine and must
      * be supplied by the caller.
      */
     BYTE      cPreambles;    /* # preambles to use                   */
     BOOL      bPrimary;      /* TRUE if a PRIMARY master setting     */
     BYTE      cRetries;      /* # times message can be retried       */
     WORD      wRT1;          /* current value of RT1 based on master */
     WORD      wExtend;       /* Timeout extension for slow response slaves */
     PGETQFUNC pfGetQ;        /* ptr to function to get next queue entry */
     PSENDFUNC pfSendMsg;
     PTMRFUNC  pfSetTimer;
     PCDFUNC   pfReadCD;

     /*
      * These are the working variables for the state machine.
      */
     BYTE      cState;        /* current state                        */
     BOOL      bBurst;        /* TRUE if burst msgs are being rcvd.   */
     PCOMMQ    pCommq;        /* ptr to comm q being processed.       */
     BYTE      cRetriesLeft;
     LINKSTATS LinkStats;
     BYTE      acBurstBuf[ MAX_HART_MSG_LEN ];
     BYTE      cBurstLen;
     RCV_CTRL  rc;            /* area controlling char rcv.           */
     //Adding hartModem and deviceFile settings for configuration purpose
     const char*     hartModem;     /* hart modem type */
     const char*     deviceFile;    /* hart device file */

} HART_CTRL, *PHART_CTRL;


/***************************************************************************
 * Global extern variables.
 ***************************************************************************/
/*
 * global HART control
 */
extern HART_CTRL    ghc;

extern HART_CTRL    ghc2;

/***************************************************************************
 * Function Prototypes
 ***************************************************************************/
/*
 * These are interface to the receive character state machine.
 * They are not for general use.
 */
FSTAT rcv_state_machine( PRCV_CTRL psc, BYTE cData, BYTE cStatus );
void  rcv_init_state_machine( PRCV_CTRL psc );

/*
 * These are interface functions to the HART link layer state machine.
 * They are not for general use.
 */
void hls_process_received_char( PHART_CTRL phc, BYTE cData, BYTE cQuality );
void hls_state_machine( PHART_CTRL phc, BYTE cEvent );
void hls_init_state_machine( PHART_CTRL phc );

#endif /* LNKCTRL_H */
