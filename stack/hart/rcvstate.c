/***************************************************************************
 *
 * SourceSafe Header
 *
 * $Workfile: rcvstate.c $
 * $Archive: /SwRICode/HMaster/src/rcvstate.c $
 * $Revision: 1.1 $
 * $Date: 2013/07/18 23:55:22 $
 *
 ***************************************************************************/

/***************************************************************************
 * Copyright 1996-1998, Southwest Research Institute.
 ***************************************************************************/

/***************************************************************************
 *
 * File Description:
 *   Character receive state processing functions.
 *   The state machine in this file interprets character input to frame
 * HART messages.
 *
 ***************************************************************************/

/***************************************************************************
 *
 * $History: rcvstate.c $
 * 
 * *****************  Version 6  *****************
 * User: Kholladay    Date: 4/15/98    Time: 10:48a
 * Updated in $/SwRICode/HMaster/src
 * Added Gap timer support.  This layer now requires 3 good preambles.
 *
 * *****************  Version 5  *****************
 * User: Kholladay    Date: 2/09/98    Time: 7:24p
 * Updated in $/SwRICode/HMaster/src
 * Release 2.0 with gap timer support
 *
 * *****************  Version 4  *****************
 * User: Kholladay    Date: 2/06/98    Time: 4:27p
 * Updated in $/SwRICode/HMaster/src
 * Update copyrights for release 2.0
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 11/14/97   Time: 3:11p
 * Updated in $/SwRICode/HMaster/src
 * Fix mask bug for delimiter.
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 10/30/97   Time: 2:01p
 * Updated in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 9/15/97    Time: 12:58p
 * Created in $/SwRICode/MasterDLL/src
 *
 * *****************  Version 3  *****************
 * User: Kholladay    Date: 10/21/96   Time: 1:10p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Made fixed arrays static.
 * Fixed frame detection bug.
 *
 * *****************  Version 2  *****************
 * User: Kholladay    Date: 8/30/96    Time: 3:11p
 * Updated in $/Master Data Link Layer (HC11)/mdll
 * Fix Delimiter recognition.
 *
 * *****************  Version 1  *****************
 * User: Kholladay    Date: 6/13/96    Time: 4:59p
 * Created in $/Master Data Link Layer (HC11)/mdll
 *
 ***************************************************************************/


#include <stdio.h>
#include <string.h>
#include "hartmdll.h"


/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/

/*
 * States for the HART receive character processor.
 */
#define RCV_WAIT_FOR_DELIM      0
#define RCV_HEADER              1
#define RCV_DATA                2
#define RCV_DONE                3


/***************************************************************************
 * Private Data Types for this file
 ***************************************************************************/

/*
 * Function types for the state processor.
 */
typedef BYTE (*PINSTATE)( PRCV_CTRL, BYTE, BYTE );
typedef void (*PTOSTATE)( PRCV_CTRL );


/***************************************************************************
 * Private function prototypes for this file
 ***************************************************************************/

static BYTE delim_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality );
static BYTE head_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality );
static BYTE body_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality );
static BYTE done_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality );

static void delim_to_state( PRCV_CTRL prc );
static void head_to_state( PRCV_CTRL prc );
static void body_to_state( PRCV_CTRL prc );
static void done_to_state( PRCV_CTRL prc );


/****************************************************************************
 *  Private variables for this module
 ***************************************************************************/

/*
 * Table of functions to be called while in each state.
 */
static const PINSTATE in_state[ ] =
{
     delim_in_state,
     head_in_state,
     body_in_state,
     done_in_state      /* don't do anything in the done state till init */
};

/*
 * Table of functions to call when first entering a state.
 */
static const PTOSTATE to_state[ ] =
{
     delim_to_state,     /* we never actually make this transition */
     head_to_state,
     body_to_state,
     done_to_state,
};


/**************************************************************************
 * FUNCTION: rcv_init_state_machine
 *
 * DESCRIPTION:
 *   This functions initializes the receiving state machine to
 *   begin processing for the reception of a message.
 *
 * ARGUMENTS:
 *   prc       pointer to comm use structure to use
 *
 * RETURNS:
 *   nothing
 ***************************************************************************/

void rcv_init_state_machine( PRCV_CTRL prc )
{
     /*
      * This function is used to set up for the reception of a message.
      */
     prc->cState = RCV_WAIT_FOR_DELIM;
     prc->cTemp = 0;
     prc->cRspQuality = STATUS_OK;
     prc->cRspCount = 0;
     prc->bGapActive = FALSE;
}


/**************************************************************************
 * FUNCTION:  rcv_state_machine
 *
 * DESCRIPTION:
 *   State machine for receiving a HART message
 * Two functions are required for each state.
 * The first is the "in" state.  The "in" state does whatever
 * work is required while in the current state.  The in state function
 * returns a value indicating the proper state to handle the next character.
 * If that state is different from the current state, then the
 * second function for the NEW state (the "to" function) is executed.
 * The "to" function is responsible for doing any initialization required
 * to properly enter the new state.
 *
 * ARGUMENTS:
 *   prc            pointer to Communications usage structure
 *   cData          data byte to process
 *   cQuality       receive status of the data byte
 *
 * RETURNS:
 *   STATUS_WORKING      if message is ok so far, but more is expected.
 *   STATUS_OK           if message is complete or error prevents further work.
 ***************************************************************************/

FSTAT rcv_state_machine( PRCV_CTRL prc, BYTE cData, BYTE cQuality )
{
     FSTAT     nStatus = STATUS_WORKING;
     BYTE      cNewState;

     /*
      * perform any "in state" actions
      */
     cNewState = in_state[ prc->cState ]( prc, cData, cQuality );

     if( cNewState != prc->cState )
     {
          /*
           * a state transition is required, move to the new state
           */
          prc->cState = cNewState;
          to_state[ prc->cState ]( prc );

          /*
           * if state is DONE, let the caller know.
           */
          if( prc->cState == RCV_DONE )
          {
               prc->bGapActive = FALSE;
               nStatus = STATUS_OK;
          }
     }
     return( nStatus );
}


/***************************************************************************
 *
 * Hereafter lie all of the state functions.
 *
 ***************************************************************************/

static BYTE done_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality )
{
     /*
      * nothing to do but wait.
      * Just for debug, save the data if it is OK.
      */
     if( !cQuality )
     {
          prc->cTemp = cData;
     }
     return( RCV_DONE );
}

static BYTE delim_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality )
{
     BYTE cNextState = RCV_WAIT_FOR_DELIM;
     BYTE cMasked = cData & 0x07;

     /*
      * While waiting for a delimiter, just count valid preambles.
      * Although the specification only requires two valid preambles, this
      * layer requires three (all known instruments use at least 5).  This
      * attempts to work around a problem known to occurr with the 20C15 where
      * the slave transmit shutdown looks like it is puting out an extra FF
      * (dribble byte) right at the end of the message.  Without this fix,
      * throughput will drop dramatically since each master will have a gap timeout
      * forcing a resynch of the bus.  Therefore, we effectively ignore one
      * preamble, starting the gap timing only on the second one.
      */

     /*
      * Have we started counting yet?
      */
     if( prc->cTemp <= 1 )
     {
          if( cQuality )
          {
               /*
                * Bad data, so reset.
                */
               prc->cTemp = 0;
          }
          else
          {
               if (cData == 0xff) { // only count leading preambles
                   prc->cTemp++;
               }
               if( prc->cTemp > 1 )
               {
                    /*
                     * Hopefully we are past any noise and it is
                     * time to start looking for a real message.
                     */
                    prc->bGapActive = TRUE;
               }
          }
     }
     else if( cQuality )
     {
          /*
           * Must record this error.
           */
          prc->cRspQuality = cQuality;
          cNextState = RCV_DONE;
     }
     else if( cData == 0xFF )
     {
          prc->cTemp++;
     }
     else if( prc->cTemp < 3 )
     {
          /*
           * This wasn't a preamble, and we didn't see enough good ones.
           */
          prc->cRspQuality = SOM_ERROR;
          cNextState = RCV_DONE;
     }
     else
     {
          /*
           * Finally a delimiter.
           */
          if( ( FRAME_BURST == cMasked ) ||
              ( FRAME_STX   == cMasked ) ||
              ( FRAME_ACK   == cMasked ) )
          {
               /*
                * It's valid, so save it and go on.
                */
               prc->acResponse[ 0 ] = cData;
               prc->cCheckSum = cData;
               prc->cRspCount = 1;
               prc->cRspQuality = STATUS_OK;
               cNextState = RCV_HEADER;
          }
          else
          {
               /*
                * Not valid, so log an invalid start of message.
                */
               prc->cRspQuality = SOM_ERROR;
               cNextState = RCV_DONE;
          }
     }

     return( cNextState );
}

static BYTE head_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality )
{
     BYTE cNextState = RCV_HEADER;

     if( cQuality )
     {
          /*
           * Bad news, we got a comm error.
           */
          prc->cRspQuality = cQuality;
          cNextState = RCV_DONE;
     }
     else
     {
          /*
           * While in the Header State, store the received character,
           * include it in the checksum, and decrement the number of characters
           * expected in the header.
           */
          prc->acResponse[ prc->cRspCount ] = cData;
          prc->cCheckSum ^= cData;
          prc->cRspCount++;
          prc->cTemp--;
          if( !prc->cTemp )
          {
               /*
                * Header is finished.
                */
               cNextState = RCV_DATA;
          }
     }
     return( cNextState );
}

static BYTE body_in_state( PRCV_CTRL prc, BYTE cData, BYTE cQuality )
{
     BYTE cNextState = RCV_DATA;

     if( cQuality )
     {
          /*
           * Bad news, we got a comm error.
           */
          prc->cRspQuality = cQuality;
          cNextState = RCV_DONE;
     }
     else
     {
          /*
           * While in the Body State, store the received character,
           * include it in the checksum,
           * and decrement the number of characters expected in the body.
           */
          prc->acResponse[ prc->cRspCount  ] = cData;
          prc->cRspCount++;
          prc->cCheckSum ^= cData;
          prc->cTemp--;
          if( !prc->cTemp )
          {
               /*
                * all done.
                */
               cNextState = RCV_DONE;
          }
     }
     return( cNextState );
}

static void delim_to_state( PRCV_CTRL prc )
{
     prc->cState = RCV_WAIT_FOR_DELIM;
     prc->cTemp = 0;
     prc->cRspQuality = STATUS_OK;
     prc->cRspCount = 0;
     return;
}

static void head_to_state( PRCV_CTRL prc )
{
     /*
      * Determine the number of characters expected in the header.
      */
     hutil_header_length( *prc->acResponse, &prc->cTemp );

     /*
      * We already have the delimiter.
      */
     prc->cTemp--;
}

static void body_to_state( PRCV_CTRL prc )
{
     /*
      * The number of expected characters in the body is determined from
      * the last character received ( the byte count ) plus one for the
      * checksum byte.
      */
     prc->cTemp = prc->acResponse[ prc->cRspCount - 1 ] + 1;
}

static void done_to_state( PRCV_CTRL prc )
{
     /*
      * Once the message is considered done, the calculated checksum should
      * be zero.
      */
     if( !prc->cRspQuality && prc->cRspCount )
     {
          if( prc->cCheckSum )
          {
               prc->cRspQuality = CHECKSUM_ERROR;
          }
     }
}
