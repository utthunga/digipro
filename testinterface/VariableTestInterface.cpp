/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Adesh Soni
    Origin:            ProStar
*/

/** @file
    VariableTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "VariableTestInterface.h"
#include <iostream>
#include <cstring>


using namespace std;



VariableTestInterface::VariableTestInterface():
		m_variableItemID(0),m_enumSize(0)
{

}
/*
 * Parse the response data required provided by CLR or UI
 */
int VariableTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    int testIntfResult = TESTINTF_FAILURE;
    switch(requestType)
    {
        case REQUEST_TYPE_GET: //FALLTHROUGH
        case REQUEST_TYPE_NONE:
        {
            int itemStatus = ItemBasicTestInterface::parseResponseData(requestType, jsonData);
            clearValues();

            if (TESTINTF_SUCCESS == itemStatus)
            {
                if (    (true == jsonData.isNull()) || 
                        (Json::objectValue != jsonData.type()) || 
                        (false == jsonData.isMember("ItemInfo")))
                {
                    return TESTINTF_FAILURE;
                }

                m_varType = jsonData["ItemInfo"]["VarType"].asString();
                m_varContainerID = jsonData["ItemInfo"]["VarContainerID"].asLargestUInt();
                m_varSubIndex = jsonData["ItemInfo"]["VarSubIndex"].asUInt();
                m_isVarReadOnly  = jsonData["ItemInfo"]["ItemReadOnly"].asBool();

                if (m_varType.compare("ARITHMETIC") == 0)
                {
                    parseArithmeticResponse(jsonData);
                }
                else if (m_varType.compare("ENUMERATION") ==0)
                {
                    parseEnumResponse(jsonData);
                }
                
                else if (m_varType.compare("STRING") == 0)
                {
                    parseStringResponse(jsonData);
                }
                
                else if(m_varType.compare("DATE_TIME") == 0)
                {
                    parseDateTimeResponse(jsonData);
                }
            }
            testIntfResult = itemStatus;
        }
        break;

        case REQUEST_TYPE_SET_UPDATE: //FALLHROUGH
        case REQUEST_TYPE_SET_EDIT:
        {
            testIntfResult = TESTINTF_SUCCESS;
        }
        break;
        
        default:
        {
        }
    }

    return testIntfResult;
}


void VariableTestInterface::clearValues()
   {
   	 m_variableItemID = 0;
   	 m_variableContainerTag.clear();
   	 m_varType.clear();
   	 m_varValType.clear();
   	 m_varValDisplayFormat.clear();
   	 m_varValEditFormat.clear();
   	 m_varUnit.clear();
   	 m_varValue = Json::Value::null;
   	 m_varValLenghth = 0;
   	 m_varCharSet =0;
     m_varContainerID = 0;
     m_varSubIndex = 0;
   	 minMaxRange.clear();
   	 clrEnumsValues.clear();
   	 m_enumSize = 0;
   	 m_isNAN = false;
   	 m_isINF = false;
   	 m_isVarReadOnly = false;
}

/*
 * Get the request param value data
 */
Json::Value VariableTestInterface::getRequestData(const REQUEST_TYPE requestType)
{
	Json::Value tempObj;

    tempObj =  ItemBasicTestInterface::getRequestData(requestType);
    
    tempObj["ItemInfo"]["VarSubIndex"] = getVarSubIndex();
    tempObj["ItemInfo"]["VarContainerID"] = getVarContainerID();
    
    switch(requestType)
    {
        case REQUEST_TYPE_SET_EDIT:
        {
            tempObj["ItemReqType"] = "EDIT";
        }
        break;

        case REQUEST_TYPE_SET_UPDATE:
        {
            tempObj["ItemReqType"] = "UPDATE";
            tempObj["ItemInfo"]["VarValue"] = getVarValue();
        }
        break;

        case REQUEST_TYPE_GET: //FALLTHROUH
        case REQUEST_TYPE_NONE: //FALLTHROUGH
        default:
        {
        }
        break;
    }
	return tempObj;
}

/*
 * Get the variable type
 */
Json::Value VariableTestInterface::getVarType()
{
	return m_varType;
}

/*
 * Get the variable value
 */
Json::Value VariableTestInterface::getVarValue()
{
	return m_varValue;
}

/*
 * Get the variable get var type
 */
Json::Value VariableTestInterface::getVarValType()
{

	return m_varValType;
}

/*
 * Get the variable Display format
 */
Json::Value VariableTestInterface::getVarValDisplayFormat()
{
	return m_varValDisplayFormat;
}

/*
 * Get the variable edit format
 */
Json::Value VariableTestInterface::getVarValEditFormat()
{
	return m_varValEditFormat;
}

/*
 * Get the variable unit
 */
Json::Value VariableTestInterface::getVarUnit()
{
	return m_varUnit;
}

/*
 * Get the variable contaiiner ID
 */
Json::Value VariableTestInterface::getVarContainerID()
{
	return static_cast<Json::Value::LargestUInt>(m_varContainerID);
}
/*
 * Get NAN Status
 */
Json::Value VariableTestInterface::getNANStatus()
{
	return m_isNAN;
}

/*
 * Get INF Status
 */
Json::Value VariableTestInterface::getINFStatus()
{
	return m_isINF;
}

/*
 * Get the variable subIndex
 */
Json::Value VariableTestInterface::getVarSubIndex()
{
	return m_varSubIndex;
}


Json::Value VariableTestInterface::getIsReadOnlyStatus()
{
	return m_isVarReadOnly;
}

/*
 * Get the data on given index
 */
void VariableTestInterface::getMinMaxValue()
{
	int vecSize = minMaxRange.size();
	for (int index = 0 ; index < vecSize ; index++ )
	{
		cout<<"		"<<"-----------------------------------------------------"<<endl;
		cout<<"		"<<"VarValMin-"<<index<<"		:"<<minMaxRange.at(index).minValue<<endl;
		cout<<"		"<<"VarValMax-"<<index<<"		:"<<minMaxRange.at(index).maxValue<<endl;
		cout<<"		"<<"-----------------------------------------------------"<<endl;
	}
}

/*
 * Get the enum count
 */
int VariableTestInterface::getEnumCount()
{
	return m_enumSize;
}

/*
 * Get the enum data on given index
 */
void VariableTestInterface::getEnumValues()
{
	if(clrEnumsValues.empty())
	{
		LOGF_INFO(CLR, "vector is NULL ");
		return;
	}
	cout<<"		"<<"VarEnumInfo-"<<"		:"<<m_varValue<<endl;
	for (int index = 0 ; index < m_enumSize ; index++ )
	{
		cout<<"		"<<"-----------------------------------------------------"<<endl;
		cout<<"		"<<"LABEL-"<<index<<"		:"<<clrEnumsValues.at(index).label<<endl;
		cout<<"		"<<"HELP-"<<index<<"		:"<<clrEnumsValues.at(index).help<<endl;
		cout<<"		"<<"VALUE-"<<index<<"		:"<<clrEnumsValues.at(index).value<<endl;
		cout<<"		"<<"-----------------------------------------------------"<<endl;
	}

}

void VariableTestInterface::getStringValues()
{
	cout<<"VarValLenghth"<<"		"<<m_varValLenghth<<endl;
	cout<<"VarCharSetType"<<"		"<<m_varCharSet<<endl;
}

/*
 * parse Arthmetic json data
 */
void VariableTestInterface::parseArithmeticResponse(const Json::Value& varJsonParseResp)
{
	const Json::Value *tempJsonObj = &varJsonParseResp["ItemInfo"]["VarInfo"];

	if ((*tempJsonObj).isMember("VarValType"))
	{
		m_varValType = (*tempJsonObj)["VarValType"].asString();

		m_varValDisplayFormat = (*tempJsonObj)["VarValDisplayFormat"].asString();
		m_varValEditFormat = (*tempJsonObj)["VarValEditFormat"].asString();
		m_varUnit = (*tempJsonObj)["VarUnit"].asString();

		if (m_varValType.compare("INTEGER") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asInt();
		}
		else if(m_varValType.compare("UNSIGNED_INTEGER") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asUInt();
		}
		else if(m_varValType.compare("FLOAT") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asFloat();
			m_isNAN = (*tempJsonObj)["VarValIsNAN"].asBool();
			m_isINF = (*tempJsonObj)["VarValIsINF"].asBool(); 
        }
		else if(m_varValType.compare("DOUBLE") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asDouble();
			m_isNAN = (*tempJsonObj)["VarValIsNAN"].asBool();
			m_isINF = (*tempJsonObj)["VarValIsINF"].asBool();
		}
		else if(m_varValType.compare("INDEX")== 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asUInt();
		}
		else
		{
			LOGF_ERROR(CLR, "Invalid Arithmetic type");
		}

		if((*tempJsonObj)["VarValRange"].isArray())
		{
			parseMinMaxValues(varJsonParseResp);
		}
	}
}

/*
 * parse string json data
 */
void VariableTestInterface::parseStringResponse(const Json::Value& varJsonParseResp)
{
	const Json::Value *tempJsonObj = &varJsonParseResp["ItemInfo"]["VarInfo"];

	m_varValType = (*tempJsonObj)["VarValType"].asString();
	if ((*tempJsonObj).isMember("VarValType"))
	{
		m_varValue = (*tempJsonObj)["VarValue"].asString();
		m_varValLenghth = (*tempJsonObj)["VarValLength"].asUInt();
		m_varCharSet =  (*tempJsonObj)["VarCharSet"].asUInt();

		/*TODO: assign charSet
		 * m_varCharSet = (*tempJsonObj)["VarCharSet"].asChar();
		 */
	}
}

/*
 * parse string ENUM data
 */

void VariableTestInterface::parseEnumResponse (const Json::Value& varJsonParseResp)
{
	const Json::Value *tempJsonObj = &varJsonParseResp["ItemInfo"]["VarInfo"];
	m_varValType = (*tempJsonObj)["VarValType"].asString();
	clrEnumsValues.clear();
	if ((m_varValType.compare("ENUMERATION") == 0) || (m_varValType.compare("BITENUMERATION") == 0))
	{
		m_varValDisplayFormat =
				(*tempJsonObj)["VarValDisplayFormat"].asString();
		m_enumSize = (*tempJsonObj)["VarEnumInfo"].size();
		m_varValue = (*tempJsonObj)["VarValue"].asInt64();

		if ((*tempJsonObj)["VarEnumInfo"].isArray())
		{
			for (int index = 0; index < m_enumSize; index++)
			{

				CLREnumValue enumsValues_t { };
				enumsValues_t.value =
						(*tempJsonObj)["VarEnumInfo"][index]["VarEnumValue"].asUInt64();
				enumsValues_t.label =
						(*tempJsonObj)["VarEnumInfo"][index]["VarEnumLabel"].asString();
				enumsValues_t.help =
						(*tempJsonObj)["VarEnumInfo"][index]["VarEnumHelp"].asString();

				clrEnumsValues.push_back(enumsValues_t);
			}
		}
	}
	else
	{
		/* TODO : <ADESH>: need to support
		 (*tempJsonObj)["VarValEnum"]
		 */
		LOGF_INFO(CLR, "bit enum type not supported type or vector is NULL");
	}

	//}
}

/*
 * parse string DATE TIME data
 */
void VariableTestInterface::parseDateTimeResponse(const Json::Value& varJsonParseResp)
{

	const Json::Value *tempJsonObj = &varJsonParseResp["ItemInfo"]["VarInfo"];
	m_varValType = (*tempJsonObj)["VarValType"].asString();
	if ((*tempJsonObj).isMember("VarValType"))
	{
		if (m_varValType.compare("DATE") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asString();
		}
		else if(m_varValType.compare("TIME") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asString();
		}
		else if(m_varValType.compare("DATE_AND_TIME") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asString();
		}
		else if(m_varValType.compare("DURATION") == 0)
		{
			m_varValue = (*tempJsonObj)["VarValue"].asString();
		}
		else if(m_varValType.compare("TIME_VALUE") == 0)
		{

			m_varValue = (*tempJsonObj)["VarValue"].asString();
		}
		else
		{
			LOGF_ERROR(CLR, "Invalid date time  type");
		}


		m_varValDisplayFormat =
						(*tempJsonObj)["VarValDisplayFormat"].asString();
		// parse for min max values
		if ((*tempJsonObj).isMember("VarValRange"))
		{
			parseMinMaxValues(varJsonParseResp);
		}
	}
}


/*
 * parse string MIN MAX values
 */
void VariableTestInterface::parseMinMaxValues(const Json::Value& varJsonParseResp)
{

	const Json::Value  (*tempJsonObj) =  &varJsonParseResp["ItemInfo"]["VarInfo"]["VarValRange"];
	minMaxRange.clear();
	if((*tempJsonObj).isArray())
	{
		int varValRangeSize = (*tempJsonObj).size();
		for (int index = 0; index < varValRangeSize ; index++)
		{
			MinMaxValues minmaxVal;
			if (m_varValType.compare("INTEGER") == 0)
			{
				minmaxVal.minValue =  (*tempJsonObj)[index]["VarValMin"].asInt();
				minmaxVal.maxValue =  (*tempJsonObj)[index]["VarValMax"].asInt();
			}
			else if ((m_varValType.compare("UNSIGNED_INTEGER") == 0) || (m_varValType.compare("DATE") == 0) ||
					(m_varValType.compare("TIME") == 0) || (m_varValType.compare("DATE_AND_TIME")  == 0) ||
					(m_varValType.compare("DURATION") == 0)
					)
			{
				minmaxVal.minValue =  (*tempJsonObj)[index]["VarValMin"].asUInt();
				minmaxVal.maxValue =  (*tempJsonObj)[index]["VarValMax"].asUInt();
			}
			else if (m_varValType.compare("FLOAT") == 0)
			{
				minmaxVal.minValue =  (*tempJsonObj)[index]["VarValMin"].asFloat();
				minmaxVal.maxValue =  (*tempJsonObj)[index]["VarValMax"].asFloat();
			}
			else if (m_varValType.compare("DOUBLE") == 0)
			{
				minmaxVal.minValue  =  (*tempJsonObj)[index]["VarValMin"].asDouble();
				minmaxVal.maxValue  =  (*tempJsonObj)[index]["VarValMax"].asDouble();
			}
			else
			{
				LOGF_ERROR(CLR, "Invalid datatype");
			}
			minMaxRange.push_back(minmaxVal);
		}
	}
}

/*
 * Sets Variable value member
 */
void VariableTestInterface::setVarValue(const std::string& varValue)
{
    std::string::size_type strSize;

    if ("BOOLEAN" == m_varValType)
    {
        if (0 == varValue.compare("1") || 
            0 == varValue.compare("true"))
        {
            m_varValue = true;
        }
        else if (0 == varValue.compare("0") ||
                 0 == varValue.compare("false"))
        {
            m_varValue = false;
        }
        else
        {
            printExceptionMessage(m_varValType);
            return;
        }
    }
    else if ("INTEGER" == m_varValType)
    {
        try
        {
            m_varValue = static_cast<int>(stoi(varValue.c_str(), &strSize));
        }
        catch(...)
        {
            printExceptionMessage(m_varValType);
        }
    }

    else if ("FLOAT" == m_varValType)
    {
        try
        {
            m_varValue = static_cast<float>(stof(varValue.c_str(), &strSize));
        }
        catch(...)
        {
            printExceptionMessage(m_varValType);
        }
    }

    else if ("UNSIGNED_INTEGER" == m_varValType || 
             "INDEX" == m_varValType || 
             "BITENUMERATION" == m_varValType || 
             "ENUMERATION" ==  m_varValType)
    {
        try
        {
            m_varValue = static_cast<unsigned int>(stoul(varValue, nullptr,0));
        }
        catch(...)
        {
            printExceptionMessage(m_varValType);
        }
    }
        
    else if ("DOUBLE" == m_varValType)
    {
        try
        {
            m_varValue = static_cast<double>(stod(varValue, &strSize));
        }
        catch(...)
        {
            printExceptionMessage(m_varValType);
        }
    }

    else if ("DATE_AND_TIME" == m_varValType || 
             "TIME_VALUE" == m_varValType ||
             "DURATION" == m_varValType || 
             "DATE" == m_varValType ||
             "TIME" == m_varValType)
    {
        string::size_type sz = 0;
        uint64_t val;
        try
        {
            val = stoull (varValue,&sz,0);
            m_varValue = val;
        }
        catch(...)
        {
            printExceptionMessage(m_varValType);
        }
    }

    else if ("ASCII" == m_varValType ||
             "PACKED_ASCII" == m_varValType ||
             "PASSWORD" == m_varValType ||
             "BITSTRING" == m_varValType ||
             "OCTETSTRING" == m_varValType ||
             "VISIBLESTRING"  == m_varValType ||
             "EUC" == m_varValType)
    {
        m_varValue = varValue;
    }
	
    else
    {
        cout <<"Unsupported data type" <<endl;
    }
}

void VariableTestInterface::printExceptionMessage(const std::string& varType)
{
    cout<<"Entered "<<varType << " is incorrect!!" <<endl;
}
