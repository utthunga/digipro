/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    ScanTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ScanTestInterface.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Parse the response data
*/
int ScanTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    if ((true == jsonData.isNull()) || (Json::objectValue != jsonData.type()) ||
         (false == jsonData.isMember("DeviceInfo")))
        return TESTINTF_FAILURE;
 
    if (jsonData["DeviceInfo"].isMember("DeviceNodeAddress"))
        m_nodeAddress = jsonData["DeviceInfo"]["DeviceNodeAddress"].asInt();

    if (jsonData["DeviceInfo"].isMember("DeviceTag"))
        m_deviceTag = jsonData["DeviceInfo"]["DeviceTag"].asString();

    if (jsonData["DeviceInfo"].isMember("DeviceID"))
        m_deviceIdTag = jsonData["DeviceInfo"]["DeviceID"].asString();

    if (jsonData.isMember("ScanSeqId"))
        m_scanSeqId = jsonData["ScanSeqId"].asInt();

    return TESTINTF_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Get the node address of the device
*/
int ScanTestInterface::getNodeAddress()
{
    return m_nodeAddress;
}

/*  ----------------------------------------------------------------------------
    Get the scan sequence id
*/
int ScanTestInterface::getScanSeqId()
{
    return m_scanSeqId;
}

/*  ----------------------------------------------------------------------------
    Get the device tag of the device
*/
std::string ScanTestInterface::getDeviceTag()
{
    return m_deviceTag;
}

/*  ----------------------------------------------------------------------------
    Get the device id tag of the device
*/
std::string ScanTestInterface::getDeviceIdTag()
{
    return m_deviceIdTag;
}
