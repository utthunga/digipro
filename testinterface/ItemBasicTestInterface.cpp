/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    ItemBasicTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "ItemBasicTestInterface.h"

/*  ----------------------------------------------------------------------------
    Constructor
*/
ItemBasicTestInterface::ItemBasicTestInterface():
    m_itemID(0),
    m_itemLabel(""),
    m_itemType(""),
    m_itemHelp(""), 
    m_itemContainerTag(""),
    m_itemInDD(false),
    m_itemSelection(false)
{
}

/*  ----------------------------------------------------------------------------
    Get the request information
*/
Json::Value ItemBasicTestInterface::getRequestData(const REQUEST_TYPE reqType)
{
    m_paramValue.clear();

    m_paramValue["ItemID"] = static_cast<Json::Value::LargestUInt>(m_itemID);
    m_paramValue["ItemType"] = m_itemType;
    m_paramValue["ItemContainerTag"] = m_itemContainerTag;

    if ((REQUEST_TYPE_GET == reqType) || (REQUEST_TYPE_NONE == reqType))
    {
        m_paramValue["ItemInDD"] = m_itemInDD;
    }

    return m_paramValue;
}

/*  ----------------------------------------------------------------------------
    Parse the menu response data
*/
int ItemBasicTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    if ((true == jsonData.isNull()) || (Json::objectValue != jsonData.type()))
        return TESTINTF_FAILURE;

    if (jsonData.isMember("ItemID"))
        m_itemID = jsonData["ItemID"].asLargestUInt();

    if (jsonData.isMember("ItemLabel"))
        m_itemLabel = jsonData["ItemLabel"].asString();

    if (jsonData.isMember("ItemType"))
        m_itemType = jsonData["ItemType"].asString();

    if (jsonData.isMember("ItemHelp"))
        m_itemHelp = jsonData["ItemHelp"].asString();

    if (jsonData.isMember("ItemContainerTag"))
        m_itemContainerTag = jsonData["ItemContainerTag"].asString();

    if (jsonData.isMember("ItemInDD"))
        m_itemInDD = jsonData["ItemInDD"].asBool();

    if (jsonData.isMember("ItemSelection"))
        m_itemSelection = jsonData["ItemSelection"].asBool();

    return TESTINTF_SUCCESS;
}
