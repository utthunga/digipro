/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    SetProtocolTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "SetProtocolTestInterface.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Set protocol type field
*/
void SetProtocolTestInterface::setProtocolType(std::string protocolType)
{
    m_protocolType = protocolType;
    
    m_paramValue["Protocol"] = m_protocolType;
}

/*  ----------------------------------------------------------------------------
    Get value of procotol type field
*/
std::string SetProtocolTestInterface::getProtocolType()
{
    return m_protocolType;
}

/*  ----------------------------------------------------------------------------
    Parse the response data
*/
int SetProtocolTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    int result = CommonTestInterface::isErrorResponse(jsonData);

    if (TESTINTF_FAILURE == result)
    {
        if (90005 == jsonData["error"]["code"].asInt())
            return TESTINTF_SUCCESS;
    }

    return result;
}

