/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    MenuTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "MenuTestInterface.h"

/*  ----------------------------------------------------------------------------
    Constructor
*/
MenuTestInterface::MenuTestInterface():
    m_menuItemCount(-1)
{
    m_menuItemList.clear();
}

/*  ----------------------------------------------------------------------------
    Parse response data for menu
*/
int MenuTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    int itemStatus = ItemBasicTestInterface::parseResponseData(requestType, jsonData);

    if (TESTINTF_SUCCESS == itemStatus)
    {
        itemStatus = TESTINTF_FAILURE;

        if (jsonData.isMember("ItemInfo") && jsonData["ItemInfo"]["ItemList"].isArray())
        {
            m_menuItemCount = jsonData["ItemInfo"]["ItemList"].size();
            m_menuItemList = jsonData["ItemInfo"]["ItemList"];

            itemStatus = TESTINTF_SUCCESS;
        }
    }

    return itemStatus;
}

/*  ----------------------------------------------------------------------------
    Set node address for the connect call
*/
int MenuTestInterface::getMenuItemCount()
{
    return m_menuItemCount;
}

/*  ----------------------------------------------------------------------------
    Get node address used for the connect call
*/
Json::Value MenuTestInterface::getMenuItemListInfo(int itemIndex, std::string& itemType)
{
    if (false == m_menuItemList.isValidIndex(itemIndex))
        return -1;

    itemType = m_menuItemList.get(itemIndex, Json::Value(""))["ItemType"].asString();

    return m_menuItemList.get(itemIndex, Json::Value(""));
}
