/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CommonTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include <testlibdefs.h>
#include <string>
#include "json/json.h"
#include <flk_log4cplus/Log4cplusAdapter.h>
#include "flk_log4cplus_defs.h"

using namespace std;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of CommonTestInterface
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga)
*/
class CommonTestInterface
{
public: 
    /* Public Member Functions ===============================================*/    
    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~CommonTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /**  Get the request param value data

        @param reqType, type of the request data (get/set)
        @return param data of Json::Value type
    */
    virtual Json::Value getRequestData(const REQUEST_TYPE reqType);

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */

    virtual int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Get the error code
        
        @return error code
    */  
    virtual int getErrorCode()
    {
        return m_errorCode;
    }

    /*------------------------------------------------------------------------*/
    /** Get the error message
        
        @return error message
    */  
    virtual std::string getErrorMessage()
    {
        return m_errorMessage;
    }

protected:
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Check if the response is with error
    
        @param return the status of the response message    
    */
    virtual int isErrorResponse(const Json::Value& jsonData);

    /// Param value use by either get or set call
    Json::Value m_paramValue;
    
    /// Json error code and response
    int m_errorCode;

    /// Json error message
    std::string m_errorMessage;
};
