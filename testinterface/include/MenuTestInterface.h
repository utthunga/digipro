/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    MenuTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "ItemBasicTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of MenuTestInterface
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    and parsing the menu json data
*/
class MenuTestInterface : public ItemBasicTestInterface
{
public: 
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Constructor */
    MenuTestInterface();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~MenuTestInterface() = default;


    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Get total menu item count */
    int getMenuItemCount();

    /*------------------------------------------------------------------------*/    
    /** Get menu item information */
    Json::Value getMenuItemListInfo(int nIndex, std::string& itemType);

private:
    /// Holds the total menu item count
    int m_menuItemCount;

    /// Holds the menu item list
    Json::Value m_menuItemList;
};
