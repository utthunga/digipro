/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    SendJson class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "json/json.h"

/* CLASS DECLARATIONS *********************************************************/
/** class description of VariableTestInterface

    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing variable information.
*/


class SendJson 
{
public:
    /* Public Member Functions ===============================================*/

	/*------------------------------------------------------------------------*/
    /** Constructor for FF connect test interface
    */
	SendJson() = default;

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes VariableTestInterface class instance.
    */
    virtual ~SendJson() = default;


    /*------------------------------------------------------------------------*/
    /** Prepares Json request from file
    */
    Json::Value prepareReq();

};

