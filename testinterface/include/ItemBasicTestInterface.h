/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    ItemBasicTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "CommonTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of ItemBasicTestInterface
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    and parsing the basic json data
*/
class ItemBasicTestInterface : public CommonTestInterface
{
public: 
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Constructor */
    ItemBasicTestInterface();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~ItemBasicTestInterface() = default;



    /*------------------------------------------------------------------------*/
    /**  Get the request param value data

        @param reqType, type of the request data (get/set)
        @return param data of Json::Value type
    */
    virtual Json::Value getRequestData(const REQUEST_TYPE reqType);

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    virtual int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);



    /* Public Member function ===============================================*/
    /* API's for setting item basic information */
    virtual void setItemID(unsigned long itemID)
    {
        m_itemID = itemID;
    }
    virtual void setItemType(std::string itemType)
    {
        m_itemType = itemType;
    }
    virtual void setItemInDD(bool itemInDD)
    {
        m_itemInDD = itemInDD;
    }
    virtual void setItemContainerTag(std::string itemContainerTag)
    {
        m_itemContainerTag = itemContainerTag;
    }


    /* Public Member function ===============================================*/
    /* API's for getting item basic information */
    virtual unsigned long getItemID()
    {
        return m_itemID;
    }
    virtual std::string getItemLabel()
    {
        return m_itemLabel;
    }
    virtual std::string getItemType()
    {
        return m_itemType;
    }
    virtual std::string getItemHelp()
    {
        return m_itemHelp;
    }
    std::string getItemContainerTag()
    {
        return m_itemContainerTag;
    }
    virtual bool isItemInDD()
    {
        return m_itemInDD;
    }

    virtual bool isItemSelected()
    {
        return m_itemSelection;
    }

private:

    /* Private Data Members ===============================================*/
    ///< Holds item id information for any DD item
    unsigned long m_itemID;

    ///< Holds item label information for a DD item
    std::string m_itemLabel;

    ///< Holds item type information for a DD item
    std::string m_itemType;

    ///< Holds item help information for a DD item
    std::string m_itemHelp;

    ///< Holds the block container tag information
    std::string m_itemContainerTag;

    ///< Holds whether the item belongs to DD binary
    bool m_itemInDD;

    ///< Holds whether the item can be selected or not
    bool m_itemSelection;
};
