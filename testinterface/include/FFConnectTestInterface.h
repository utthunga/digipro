/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    FFConnectTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "ConnectTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of FFConnectTestInterface
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing bus parameter
*/
class FFConnectTestInterface : public ConnectTestInterface
{
public: 
    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Constructor for FF connect test interface
    */
    FFConnectTestInterface();
 
    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~FFConnectTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /** Set node address for the connect call
    */
    void setNodeAddress(int nNodeAddress);

    /*------------------------------------------------------------------------*/
    /** Get node address used for the connect call
    */
    int getNodeAddress();

private:
    /// Holds the node address for the device to be connected
    unsigned int m_nodeAddress;
};
