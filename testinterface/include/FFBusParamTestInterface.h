/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    SetProtocolType class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "CommonTestInterface.h"
#include "BusParamTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetProtocolType
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing bus parameter
*/
class FFBusParamTestInterface : public BusParamTestInterface
{
public: 
    /* Public Member Functions ===============================================*/    
    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~FFBusParamTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /**  Get the request param value data

        @param reqType, type of the request data (get/set)
        @return param data of Json::Value type
    */
    Json::Value getRequestData(const REQUEST_TYPE reqType);

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Set bus param to the json structure
    */
    void setBusParam (const std::string& param, const Json::Value& value);

    /* Get function for the FF bus parameters --------------------------------*/
    /*------------------------------------------------------------------------*/
    /** Get the current node for FF
    */
    unsigned int getLink()
    {
        return m_thisLink;
    }
    /*------------------------------------------------------------------------*/
    /** Get slot time
    */
    unsigned int getSlotTime()
    {
        return m_slotTime;
    }    
    /*------------------------------------------------------------------------*/
    /** Get data link layer pdu overload
    */
    unsigned int getDlPduOverload()
    {
        return m_phlo;
    }
    /*------------------------------------------------------------------------*/
    /** Get maximum response delay
    */
    unsigned int getMaxResDelay()
    {
        return m_mrd;
    }
    /*------------------------------------------------------------------------*/
    /** Get minimum inter PDU delay
    */
    unsigned int getMinInterPDUDelay()
    {
        return m_mipd;
    }
    /*------------------------------------------------------------------------*/
    /** Get time sync class
    */
    unsigned int getTimeSyncClass()
    {
        return m_tsc;
    }
    /*------------------------------------------------------------------------*/
    /** Get inter channel signal skew
    */
    unsigned int getInterChannelSignalSkew()
    {
        return m_phisSkew;
    }
    /*------------------------------------------------------------------------*/
    /** Get PHL preamble extension
    */
    unsigned int getPhlPreambleExt()
    {
        return m_phpe;
    }
    /*------------------------------------------------------------------------*/
    /** Get PHL gap extension
    */
    unsigned int getPhlGapExt()
    {
        return m_phge;
    }
    /*------------------------------------------------------------------------*/
    /** Get first unpolled node address
    */
    unsigned int getFun()
    {
        return m_fun;
    }
    /*------------------------------------------------------------------------*/
    /** Get number of consecutive unpolled node address
    */
    unsigned int getNun()
    {
        return m_nun;
    }

private:
    /// Holds current node of FBK
    unsigned int m_thisLink;               /** < represt the current node of the FBK */
    /// Holds slot time
    unsigned int m_slotTime;
    /// Holds DL PDU overload
    unsigned int m_phlo;
    /// Holds maximum response delay
    unsigned int m_mrd;
    /// Holds min Inter PDU delay
    unsigned int m_mipd;
    /// Holds time sync class
    unsigned int m_tsc;
    /// Holds max inter channel signal skew
    unsigned int m_phisSkew;
    /// Holds PHL preamble extension
    unsigned int m_phpe;
    /// Holds PHL Gap extension
    unsigned int m_phge;
    /// Holds first unpolled node
    unsigned int m_fun;
    /// Holds no. of consecutive unpolled nodes
    unsigned int m_nun;                     /** <Number of consecutive unpolled nodes */
};
