/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    Contains macro and enum definition that can be used within test lib
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* ===========================================================================*/
/// Indicates success case for any module that is out of PM scope
#define TESTINTF_SUCCESS    0
/// Indicates failure case for any module that is out of PM scope
#define TESTINTF_FAILURE    -1

/// Enumeration defines the Request type required for test lib
enum REQUEST_TYPE
{
    REQUEST_TYPE_NONE = 0,
    REQUEST_TYPE_GET = 1, ///< Indicates the request type for getting any information
    REQUEST_TYPE_SET, ///< Indicates the request type for setting any information
    REQUEST_TYPE_SET_EDIT, ///< Indicates the request type for setting and editing a variable
    REQUEST_TYPE_SET_UPDATE ///< Indicates the rquest type for setting and updating variable value
};
