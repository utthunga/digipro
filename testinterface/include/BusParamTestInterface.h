/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    BusParamTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "CommonTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of BusParamTestInterface
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing bus parameter
*/
class BusParamTestInterface : public CommonTestInterface
{
public: 
    /* Public Member Functions ===============================================*/    
    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~BusParamTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /**  Get the request param value data

        @param reqType, type of the request data (get/set)
        @return param data of Json::Value type
    */
    virtual Json::Value getRequestData(const REQUEST_TYPE reqType)
    {
        return CommonTestInterface::getRequestData(reqType);
    }        

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    virtual int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
    {
        return CommonTestInterface::parseResponseData(requestType, jsonData);
    }

    /*------------------------------------------------------------------------*/
    /** Set the bus paramter for the respective protocol
    */
    virtual void setBusParam(const std::string& param, const Json::Value& jsonData) = 0;

};
