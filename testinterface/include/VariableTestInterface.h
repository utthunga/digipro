/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Adesh soni
    Origin:            ProStar
*/

/** @file
    VariableTestInterface class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "CommonTestInterface.h"
#include "json/json.h"
#include "ItemBasicTestInterface.h"
#include <vector>


/* ========================================================================== */

/// Structure  defines minmax values of variable
typedef struct MinMaxValues_t
{
	Json::Value minValue;
	Json::Value maxValue;
}MinMaxValues;

/* ========================================================================== */
/// Structure  defines enum values of variable
typedef struct
{
	unsigned long   value;            // Value for enum string
    std::string     label;            // Variable label
    std::string     help;             // Variable help string
   // EnumType        enumType;         // enumType (Default/Bit)
    //unsigned long   action;           // Action items
  //  unsigned long   functionalClass;  // Function class
   // unsigned long   status;
} CLREnumValue;

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of VariableTestInterface

    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing variable information.
*/


class VariableTestInterface : public ItemBasicTestInterface
{
public:
    /* Public Member Functions ===============================================*/

	/*------------------------------------------------------------------------*/
    /** Constructor for FF connect test interface
    */
	VariableTestInterface();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes VariableTestInterface class instance.
    */
    virtual ~VariableTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /**  Get the request param value data

            @param reqType, type of the request data (get/set)
            @return param data of Json::Value type
     */
     virtual Json::Value getRequestData(const REQUEST_TYPE reqType);

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    virtual int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Get the type of the variable like Arithmetic etc
    */
    Json::Value getVarType();

    /*------------------------------------------------------------------------*/
    /** Get the value of the variable
    */
    Json::Value getVarValue();

    /*------------------------------------------------------------------------*/
    /** Get the sub type of the variable like INTEGER, FLOAT etc
    */
    Json::Value  getVarValType();

    /*------------------------------------------------------------------------*/
    /** Get the display format of the variable.
    */
    Json::Value getVarValDisplayFormat();

    /*------------------------------------------------------------------------*/
    /** Get the Edit format of the variable.
    */
    Json::Value getVarValEditFormat();

    /*------------------------------------------------------------------------*/
    /** Get the Unit of the variable.
    */
    Json::Value getVarUnit();

    /*------------------------------------------------------------------------*/
    /** Get the Container ID of the variable.
    */
    Json::Value getVarContainerID();

    /*------------------------------------------------------------------------*/
    /** Get the SubIndex of the variable.
    */
    Json::Value getVarSubIndex();

    /*------------------------------------------------------------------------*/
    /** Get the minimum and maximum values.
    */
    void getMinMaxValue();

    /*------------------------------------------------------------------------*/
    /** Get the Enum values
    */
   // void getEnumValues(int enumIndex,EnumValue &enumVal);
    void getEnumValues();

    /*------------------------------------------------------------------------*/
    /** Get the enum  count.
    */
    int getEnumCount();

    /*------------------------------------------------------------------------*/
    /** Get the string values.
    */
    void getStringValues();
	
    /*------------------------------------------------------------------------*/
    /** Sets variable value.
    */
    void setVarValue(const std::string& varValue);

    /*------------------------------------------------------------------------*/
    /** reset the values
    */
    void clearValues();

    /*------------------------------------------------------------------------*/
    /** printExceptionMessage
    */
    void printExceptionMessage(const std::string& varType);

    /*------------------------------------------------------------------------*/
    /** Get the NAN  of the variable.
    */
    Json::Value getNANStatus();

    /*------------------------------------------------------------------------*/
    /** Get the INF  of the variable.
    */
    Json::Value getINFStatus();

    /*------------------------------------------------------------------------*/
    /** Get the readonly status.
    */
    Json::Value getIsReadOnlyStatus();



private:
    void parseArithmeticResponse(const Json::Value& varJsonParseResp);
    void parseMinMaxValues(const Json::Value& varJsonParseResp);
    void parseEnumResponse(const Json::Value& varJsonParseResp);
    void parseStringResponse(const Json::Value& varJsonParseResp);
    void parseDateTimeResponse(const Json::Value& varJsonParseResp);
private:

    unsigned long m_variableItemID;
    std::string m_variableContainerTag;
    std::string m_varType;
	std::string m_varValType;
	std::string m_varValDisplayFormat;
	std::string m_varValEditFormat;
	std::string m_varUnit;
	Json::Value m_varValue;
	unsigned short m_varValLenghth;
	unsigned short m_varCharSet;
    unsigned long  m_varContainerID;
    unsigned short m_varSubIndex;
	std::vector<MinMaxValues> minMaxRange;
	std::vector<CLREnumValue> clrEnumsValues;
	Json::Value m_varValEnum;
	int m_enumSize;
	bool m_isNAN;
	bool m_isINF;
	bool m_isVarReadOnly;
};

