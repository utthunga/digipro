/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git@bitbucket.org:flukept/digipro.git
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    SetProtocolType class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "CommonTestInterface.h"
#include "json/json.h"

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of SetProtocolType
    
    This interface contains test interface API's that will be used both by
    CLR (developed by Fluke IDC) and UI (developed by Utthunga) for preparing/
    parsing scan related information
*/
class ScanTestInterface : public CommonTestInterface
{
public: 
    /* Public Member Functions ===============================================*/    
    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes CommonTestInterface class instance.
    */
    virtual ~ScanTestInterface() = default;

    /*------------------------------------------------------------------------*/
    /** Parse the response data required provided by CLR or UI

        @param jsonData, data in json format provided by CLR or UI
        @return param status of the parsing the response data
    */
    virtual int parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/    
    /** Get the node address of the device
    */
    int getNodeAddress();

    /*------------------------------------------------------------------------*/    
    /** Get the device id of the field device
    */
    std::string getDeviceIdTag();

    /*------------------------------------------------------------------------*/    
    /** Get the physical device tag of the device
    */
    std::string getDeviceTag();

    /*------------------------------------------------------------------------*/    
    /** Get the scan id of the device
    */
    int getScanSeqId();
    
private:
    /// Param value use by either or set call
    std::string m_protocolType;

    /// Holds the node address of the device (FF/HART)
    int m_nodeAddress;

    /// Holds the physical device tag of the device (FF)
    std::string m_deviceTag;

    /// Holds the physical device id tag of the device (FF)  
    std::string m_deviceIdTag;

    /// Holds the scan sequence id of the device (FF/HART/PB)
    int m_scanSeqId;
};
