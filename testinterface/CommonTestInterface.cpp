/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    CommonTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "CommonTestInterface.h"

/* ========================================================================== */
/* Constants and Macros *******************************************************/



/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
/*  ----------------------------------------------------------------------------
    Get the request data
*/
Json::Value CommonTestInterface::getRequestData(const REQUEST_TYPE requestType)
{
    return m_paramValue;
}

/* ----------------------------------------------------------------------------
   Check for error response data
*/
int CommonTestInterface::isErrorResponse(const Json::Value& jsonData)
{
    if ((false == jsonData.isNull()) &&
        (Json::objectValue == jsonData.type()) &&
        (jsonData.isMember("code")) && (jsonData.isMember("message")))
    {
        m_errorCode = jsonData["code"].asInt();
        m_errorMessage = jsonData["message"].asString();

        return TESTINTF_FAILURE;
    }

    return TESTINTF_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Parse the response data
*/
int CommonTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    return isErrorResponse(jsonData);
}
