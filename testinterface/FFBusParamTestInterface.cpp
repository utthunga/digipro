/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    FFBusParamTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "FFBusParamTestInterface.h"

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/
Json::Value FFBusParamTestInterface::getRequestData(const REQUEST_TYPE requestType)
{
    if (REQUEST_TYPE_SET == requestType)
        return m_paramValue;

    return Json::Value("");
}
    
/*  ----------------------------------------------------------------------------
    Parse the response data
*/
int FFBusParamTestInterface::parseResponseData(const REQUEST_TYPE requestType, const Json::Value& jsonData)
{
    switch (requestType)
    {
        case REQUEST_TYPE_GET:
            {
                if ((true == jsonData.isNull()) || (Json::objectValue != jsonData.type()) ||
                    (false == jsonData.isMember("FF_BUS_PARAMS")))
                    return TESTINTF_FAILURE;

                if (jsonData["FF_BUS_PARAMS"].isMember("fun"))
                    m_fun = jsonData["FF_BUS_PARAMS"]["fun"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("nun"))
                    m_nun = jsonData["FF_BUS_PARAMS"]["nun"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("mipd"))
                    m_mipd = jsonData["FF_BUS_PARAMS"]["mipd"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("mrd"))
                    m_mrd = jsonData["FF_BUS_PARAMS"]["mrd"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("phge"))
                    m_phge = jsonData["FF_BUS_PARAMS"]["phge"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("phpe"))
                    m_phpe = jsonData["FF_BUS_PARAMS"]["phpe"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("phis_skew"))
                    m_phisSkew = jsonData["FF_BUS_PARAMS"]["phis_skew"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("phlo"))
                    m_phlo = jsonData["FF_BUS_PARAMS"]["phlo"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("tsc"))
                    m_tsc = jsonData["FF_BUS_PARAMS"]["tsc"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("slot_time"))
                    m_slotTime = jsonData["FF_BUS_PARAMS"]["slot_time"].asUInt();

                if (jsonData["FF_BUS_PARAMS"].isMember("this_link"))
                    m_thisLink = jsonData["FF_BUS_PARAMS"]["this_link"].asUInt();
            }
            break;

        case REQUEST_TYPE_SET:
            {   
                return BusParamTestInterface::parseResponseData(requestType, jsonData);
            }
            break;

        default:
            break;
    }

    return TESTINTF_SUCCESS;
}

/*  ----------------------------------------------------------------------------
    Set bus param
*/
void FFBusParamTestInterface::setBusParam(const std::string& param, const Json::Value& value)
{
    m_paramValue.clear();

    LOGF_INFO(CLR, "Param: " << param << ", " << "Value: " << value);

    if (0 != param.compare("SET_DEFAULT"))
    {
        m_paramValue["FF_BUS_PARAMS"][param] = value.asInt();
    }
    else
    {
        m_paramValue["SET_DEFAULT"] = value.asInt();;
    }
}
