/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Santhosh Kumar Selvaraj
    Origin:            ProStar
*/

/** @file
    FFConnectTestInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "FFConnectTestInterface.h"

/*  ----------------------------------------------------------------------------
    Constructor
*/
FFConnectTestInterface::FFConnectTestInterface():
    m_nodeAddress(-1)
{
}

/*  ----------------------------------------------------------------------------
    Set node address for the connect call
*/
void FFConnectTestInterface::setNodeAddress(int nNodeAddress)
{
    m_paramValue.clear();

    m_paramValue["DeviceNodeAddress"] = nNodeAddress;

    m_nodeAddress = nNodeAddress;
}

/*  ----------------------------------------------------------------------------
    Get node address used for the connect call
*/
int FFConnectTestInterface::getNodeAddress()
{
    return m_nodeAddress;
}
