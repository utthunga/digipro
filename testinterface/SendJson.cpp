/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    VariableRecordInterface class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <SendJson.h>
#include <fstream>
#include <cstring>

using namespace std;

/*
 * Parse the response data required provided by CLR or UI
 */
Json::Value SendJson::prepareReq()
{
    Json::Value jsonreq;

    std::ifstream stream("jsonreq.txt");
    std::string jsonStr((std::istreambuf_iterator<char>(stream)),
                     std::istreambuf_iterator<char>());

    Json::Reader reader;
    bool parsingSuccessful = reader.parse( jsonStr.c_str(), jsonreq );

    return jsonreq;
}


