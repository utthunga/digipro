#include <QCoreApplication>
#include <QtDBus/QDBusConnection>
#include <testpmserver.h>
#include <testpmserver_adaptor.h>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TestPMServer *pPMServerObj = new TestPMServer();
    TestpmserverAdaptor* pPMServerAdaptorObj = new TestpmserverAdaptor(pPMServerObj);

    QDBusConnection qDBusServerConnection = QDBusConnection::sessionBus();
    qDBusServerConnection.registerObject("/pmserverobj", pPMServerObj);
    qDBusServerConnection.registerService("org.example.testpmserver");

    // pPMServerObj->GetJSONString();

    return a.exec();
}
