
#include <testpmserver.h>
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>


QString TestPMServer::GetJSONString()
{
    QString qJsonFileContent;

    QFile qJsonFile("/home/devuser/QtApplication/DBusApplication/PMData.json");
    qJsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
    qJsonFileContent = qJsonFile.readAll();
    qJsonFile.close();

    qDebug() << "JSON Content: " << qJsonFileContent;

    return qJsonFileContent;
}
