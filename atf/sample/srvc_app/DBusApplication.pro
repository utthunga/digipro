QT += dbus core
QT -= gui

CONFIG += c++14


DBUS_ADAPTORS += org.example.testpmserver.xml

TARGET = DBusApplication
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    testpmserver.cpp

HEADERS += \
    testpmserver.h


DISTFILES += \
    PMData.json \
    ../../../../usr/share/dbus-1/interfaces/org.example.testpmserver.xml \
    ../../../../usr/share/dbus-1/services/org.example.testpmserver.service
