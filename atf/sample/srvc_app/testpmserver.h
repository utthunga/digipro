#ifndef TESTPMSERVER_H
#define TESTPMSERVER_H

#include <QObject>
#include <QString>
#include <QMap>

class TestPMServer : public QObject
{
    Q_OBJECT

public Q_SLOTS:
    QString GetJSONString();
};

#endif // TESTPMSERVER_H
