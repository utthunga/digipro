#!/usr/bin/env python2.7

import dbus
import json

jsonString = ''
jsonObject = 0

def scan_ff_devices():
	sessionBus = dbus.SessionBus()
	pmServerObj = sessionBus.get_object('org.example.testpmserver',"/pmserverobj")
	dbusJSONStringObj = pmServerObj.get_dbus_method('GetJSONString', 'org.example.testpmserver')

	global jsonString 
	jsonString = dbusJSONStringObj()

def load_json_string():
	global jsonObject
	jsonObject = json.loads(jsonString)

def get_device_list():
	return jsonObject['DeviceList']

def get_device_count():
	return jsonObject['DeviceCount']

def execute_method():
	return
	
def handle_method_actions():
	return

#if __name__ == '__main__':
#	scan_ff_devices()
