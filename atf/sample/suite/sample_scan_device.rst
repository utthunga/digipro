# -*- coding: robot -*-

.. code:: robotframework
	
	*** Settings ***	
	Library		OperatingSystem
	Library		../lib/sample_scan_device.py

	*** Variables ***
	${DeviceList} =		[{'DeviceName': 'Device1', 'DeviceAddress': 21, 'DeviceTag': 'Transmitter1'}, {'DeviceName': 'Device2', 'DeviceAddress': 22, 'DeviceTag': 'Transmitter2'}, {'DeviceName': 'Device3', 'DeviceAddress': 23, 'DeviceTag': 'Transmitter3'}, {'DeviceName': 'Device4', 'DeviceAddress': 24, 'DeviceTag': 'Transmitter4'}, {'DeviceName': 'Device5', 'DeviceAddress': 25, 'DeviceTag': 'Transmitter5'}, {'DeviceName': 'Device6', 'DeviceAddress': 26, 'DeviceTag': 'Transmitter6'}]
	${DeviceCount} =	6

	*** Test Cases ***
	Verify FF device are connected in the network
		[Tags]				critical
		Scan FF Devices
		Load JSON String
		${CurrentDeviceCount} = 	Get Device Count
		Should Be True			${CurrentDeviceCount}==${DeviceCount}

	Verify parameters of connected FF devices in the network
		[Tags]				noncritical
		Scan FF Devices
		Load JSON String
		${CurrentDeviceList} =		Get Device List
		Should Be True			${CurrentDeviceList}==${DeviceList}

	
