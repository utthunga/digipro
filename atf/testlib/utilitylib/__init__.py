#!/usr/bin/env python2.7

## @package utilitylib
# Contains utility service APIs required for other ATF modules
# Following utility services are currently included in this package
#   protobuf,
#   dbus service,
#   json,
#   string