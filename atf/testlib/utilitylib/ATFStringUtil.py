#!/usr/bin/env python2.7

## @module ATFStringUtil Contains user methods that operates on python string obejcts
# @author  Santhosh Kumar Selvaraj

from utilitylib.protolib.ATFPBRpc import *

## Compares string data and return the result
# @param inputStr     python string data
# @param expectedStr  python string data
def compare_string_data(inputStr, expectedStr):
	if (inputStr == expectedStr):
		return True
	print("String Mismatch - ")
	print("Response String: ",inputStr)
	print("Expected String: ",expectedStr)
	return False

## Extratcs json string data from protobuf message
# @param publisherData protobuf encoded json message
def extractJsonStringData(publisherData):
	unwantedCharPos = 1
	startData = -1
	if publisherData[unwantedCharPos] == '{':
		startData = publisherData[unwantedCharPos + 1].find('{')
		startData = unwantedCharPos + 1
	else:
		startData = publisherData.find('{')
	endData = publisherData.rfind('}')
	publisherData = publisherData[startData : endData+1]
	return publisherData
