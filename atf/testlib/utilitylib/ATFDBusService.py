#!/usr/bin/env python2.7

## @module ATFDBusService contains low level dbus service API call
# This service is currently written as a wrapper to dbus-python package
# @author  Santhosh Kumar Selvaraj

import dbus
import dbus.service
import threading

from gi.repository import GLib
from enum import Enum

## Hold the type of dbus call
# The call type has been described as enum
class CallType(Enum):
    SYNC_CALL=1
    ASYNC_CALL=2

## ATFDBusService - Provides low level dbus service to the application or above immediate layer
#
class ATFDBusService(dbus.service.Object):
    ## Constructor for atf dbuslib service
    # @param self           object pointer.
    # @param serviceName    DBus service name (ex: com.fluke.starsky.cpm)
    # @param interfacePath  DBus interface path (ex: local.communicator.ProtoDBus.DbusServerImpl)
    # @param objectPath     DBus object path (ex: /Cpmcommand)
    def __init__(self, serviceName="", interfacePath="", objectPath=""):
        super(ATFDBusService, self).__init__()
        self.serviceName = serviceName
        self.interfacePath = interfacePath
        self.objectPath = objectPath
        self.notifyCallBackFuncPtr = None

    ## Start the dbus loop that is required for receiving dbus messagae
    # from outside application
    # @param self   object pointer.
    def startService(self):
        busName = dbus.service.BusName(self.serviceName, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, None, None, busName)
        self.dbusLoop = GLib.MainLoop()
        self.dbusLoop.run()	
        return

    ## Stop the dbus loop and from this point this does not receive any dbus messages
    # from outside application
    # @param self   object pointer.
    def stopService(self):
        self.dbusLoop.quit()
        return

    ## Sends dbus messages to the specified DBus service in the specified interface and
    # object path. This will be a blocking call.
    # @param self   object pointer.
    # @param reqStr request data that need to be sent over dbus
    def sendData(self, reqStr):
        sessionBus = dbus.SessionBus()
        proxyServerObj = sessionBus.get_object(self.serviceName, self.objectPath)
        interfaceObj = dbus.Interface(proxyServerObj, dbus_interface=self.interfacePath)
        while True:
            try:
                # Timeout of maximum 60 secs specifies reply timeout for dbuslib call
                respStr = interfaceObj.handleRequest(reqStr, timeout=60)
                return respStr
            except:
                print ("ATFDBusService: handleRequest failed, retrying....", )

    ## Allows multiple object path to be used for this dbus service
    # @param self           object pointer.
    # @param bMultiObjPath  True (default), if multiple object path need to be enabled
    #                       False otherwise
    def enableMultiObjectPath(self, bMultiObjPath=True):
        self.SUPPORTS_MULTIPLE_OBJECT_PATHS = bMultiObjPath

    ## Adds provided object path for this dbus service
    # @param self           object pointer.
    # @param objectPath     Object path that need to be added for this dbus service
    def subscribeDBusObject(self, objectPath):
        self.objectPath = self.objectPath + objectPath
        self.add_to_connection(dbus.SessionBus(), self.objectPath)

    ## Removes provided object path from this dbus service and after this no message
    # can be sent to this object path from outside application
    # @param self           object pointer.
    def unsubscribeDBusObject(self):
        self.remove_from_connection(dbus.SessionBus(), self.objectPath)

    ## Registers application call back function in dbus service layer
    # This is required if dbus services exposes its API (ex: handleRequest()) to outside application and
    # triggers call back with the provided data to achieve any required functionality
    # @param self                   object pointer.
    # @param notifyCallBackFuncPtr  Notify call back function from application or above immediate layer
    def registerNotifyCallBackFuncPtr(self, notifyCallBackFuncPtr):
        self.notifyCallBackFuncPtr = notifyCallBackFuncPtr

    ## Handles the request from outside application
    # @param self   object pointer.
    # @param data   data requested or notified from outside application
    @dbus.service.method(dbus_interface="local.communicator.ProtoDBus.DbusServerImpl",in_signature='ay',out_signature='ay')
    def handleRequest(self, data):
        return self.notifyCallBackFuncPtr(data)

"""Send D-Bus request message to the requested application."""
def sendDBusMessage(serviceName, interfacePath, objectPath, jsonReqStr, callType=CallType.SYNC_CALL):
    # Create a new atf dbuslib service instance.
    atfDbusService = ATFDBusService(serviceName, interfacePath, objectPath)
    # Check for call type and make corresponding call with required logic.
    if callType == CallType.SYNC_CALL:
        # Make a sync call to the communicator application
        jsonRespStr = atfDbusService.sendData(jsonReqStr)
        return jsonRespStr
    elif callType == CallType.ASYNC_CALL:   
        # Need to implement for the async call.
	    return jsonRespStr

"""Create a new atf dbuslib service instance and return the newly created instance."""
def createDBusService(serviceName, interfacePath="", objectPath=""):
    # Create a new atf dbuslib service instance.
    atfDBusService = ATFDBusService(serviceName, interfacePath, objectPath)
    # Set multiple object path feature enabled within the atf dbuslib service instance.
    atfDBusService.enableMultiObjectPath()
    # Return the newly created instance
    return atfDBusService
 
"""Register new object path for the provided atf dbuslib service instance."""
def subscribeDBusObject(atfDBusService, objectPath):
    # Add object path to the existing dbuslib service instance
    atfDBusService.subscribeDBusObject(objectPath)

"""Unregister object path for the provided atf dbuslib service instance."""
def unsubscribeDBusObject(atfDBusService):
    # Remove object path from existing dbuslib service instance
    atfDBusService.unsubscribeDBusObject()

"""Register call back function pointer."""
def registerNotifyCallBackFuncPtr(atfDBusService, notifyCallBackFuncPtr):
    atfDBusService.notifyCallBackFuncPtr = notifyCallBackFuncPtr
#    atfDBusService.registerNotifyCallBackFuncPtr(notifyCallBackFuncPtr)

"""Start the dbuslib service object."""
def startDBusService(atfDBusService):
    # Create a new thread and start the dbuslib loop within that thread context
    dbus_thread = threading.Thread(target=atfDBusService.startService)
    # Start the thread
    dbus_thread.start()
 
"""Stop the dbuslib service object."""
def stopDBusService(atfDBusService):
    """Stops the D-Bus service."""
    atfDBusService.stopService()

from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

#def notifyCallBackFunc(jsonRespStr):
#    return jsonRespStr

#if __name__=='__main__':	
#    atfDBusObject = createDBusService("com.fluke.cpm.atf", "local.communicator.DbusServerImpl")
#    subscribeDBusObject(atfDBusObject, "/ITEM_PUBLISHER")
#    subscribeDBusObject(atfDBusObject, "/SCAN_PUBLISHER")
#    registerNotifyCallBackFuncPtr(atfDBusObject, notifyCallBackFunc)
#bfhlrtw-[]    startDBusService(atfDBusObject)
