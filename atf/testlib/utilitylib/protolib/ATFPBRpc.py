#!/usr/bin/env python2.7

import random
import Rpc_pb2
import PubSub_pb2


class ATFPBRpc():
    def createPBRpcRequestCommand(self, methodName, methodParam):
        request = Rpc_pb2.RpcRequestCommand()
        request.id = str(random.randint(1, 200))
        request.method = methodName
        request.params = str.encode(methodParam)
        dataBytes = request.SerializeToString()
        return dataBytes

    def extractPBRpcResponseCommand(self, respData):
        response = Rpc_pb2.RpcResponseCommand()
        byteArray = ''.join([chr(byte) for byte in respData])
        responseStr = str.encode(byteArray)
        response.ParseFromString(responseStr)
        return response

    def createPBRegisterRequest(self, methodName, subscriberName, componentName):
        registerReq = PubSub_pb2.RegisterRequest()
        registerReq.subscriber = subscriberName
        registerReq.component = componentName
        return registerReq.SerializeToString()

