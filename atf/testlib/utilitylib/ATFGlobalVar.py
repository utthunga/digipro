#!/usr/bin/env python2.7

## @module ATFGloblaVar contains global variables that is required to access
# across all ATF python modules
# @author  Santhosh Kumar Selvaraj

from enum import Enum

## Specifies communication type that is currently in use for ATF
commType = "DBUS"

## DBus specific variables which is mostly used in low level dbus service layer module (ATFDBusService.py)
# DBus service information of the application that need to be tested with ATF framework
cpmDBusServiceName = 'com.fluke.starsky.cpm'
cpmDBusInterfacePath = 'local.communicator.ProtoDBus.DbusServerImpl'
cpmDBusCommandObjectPath = '/Cpmcommand'
cpmPublisherPrefix = "Publisher_"
cpmSubscriberPrefix = "Subscriber_"

## DBus service information of ATF application
atf_dbus_service_name = 'com.fluke.cpm.atf'
atf_dbus_interface_path = cpmDBusInterfacePath

## Holds message type required in ATF application layer
class MessageType(Enum):
    MSG_TYPE_COMMAND=1

## RequestType - Holds request type required in ATF application layer
# REQ_TYPE_PING - Specifies ping request from outside application
# REQ_TYPE_UPDATE - Specifies update request from outside application
class RequestType(Enum):
    REQ_TYPE_PING=1
    REQ_TYPE_UPDATE=2

## RequestType - Event state of the command
class CommandEventState(Enum):
    NO_EVENT=0
    WAIT_FOR_EVENT=1
    SET_EVENT=2

## Applicaiton interface object that is used across all ATF modules
gAppInstance = None

