#!/usr/bin/env python2.7

## @module ATFJsonUtil Contains user methods that operates on JSON objects
# The APIs in this module is achived with python package 'json'
# @author  Santhosh Kumar Selvaraj

import json

current_key = None
skip_key_list = ["VarUnit"]

def check_key_exists(key):
    for skip_key in skip_key_list:
        if (key == skip_key):
            return True

def compare_json_data(source_data_a, source_data_b):
    def compare(data_a, data_b):
        global current_key
        # This condition will be used for JSON object containing array
        if (type(data_a) is list):
            # is [data_b] a list and of same length as [data_a]?
            if ((type(data_b) != list) or
                (len(data_a) != len(data_b))):
                return False
            # iterate over list items
            for list_index,list_item in enumerate(data_a):
                # compare [data_a] list item against [data_b] at index
                if (not compare(list_item,data_b[list_index])):
                    return False
            return True
        # This condition will be used for nested JSON object
        if (type(data_a) is dict):
            # is [data_b] a dictionary?
            if (type(data_b) != dict):
                return False
            # iterate over dictionary keysd,l
            for dict_key,dict_value in data_a.items():
                # key exists in [data_b] dictionary, and same value?
                current_key = dict_key
                if (check_key_exists(current_key) == True):
                    continue
                if ((dict_key not in data_b) or
                    (not compare(dict_value,data_b[dict_key]))):
                    if (dict_key not in data_b):
                        print ("JSON DIFF: Key ", dict_key, " missing in expected JSON data")
                    return False
            return True
        # This condition will be used for scalar JSON value comparision
        if (((data_a == data_b) and (type(data_a) is type(data_b)))):
            return  True
        else:
            print ("JSON DIFF: Data mismatch in key: ", current_key)
            print ("JSON DIFF: Actual JSON VALUE: ", data_a)
            print ("JSON DIFF: Expected JSON VALUE: ", data_b)
            return False
    return (compare(source_data_a,source_data_b) and compare(source_data_b,source_data_a))

## Compares provided json object and returns the result to the request module
# @param jsonStr1 Json data in string format
# @param jsonStr2 Json data in string format
# def compare_json_objects(jsonStr1, jsonStr2):
    # jsonObj1 = json.dumps(jsonStr1, sort_keys=True)
    # jsonObj2 = json.dumps(jsonStr2, sort_keys=True)
    #
    # if (jsonObj1 == jsonObj2):
    #     return True
    # print('JSON mismatch: ')
    # print('jsonObj1: ',jsonObj1)
    # print('jsonObj2: ',jsonObj2)
    # return False

## Compares provided json object and returns the result to the request module
# @param jsonStr1 Json data in string format
# @param jsonStr2 Json data in string format
def compare_json_objects(jsonObj1, jsonObj2):
    if (compare_json_data(jsonObj1, jsonObj2) == False):
        print ("JSON DIFF: Actual JSON data: ", json.dumps(jsonObj1, sort_keys=True))
        print ("JSON DIFF: Expected JSON data: ", json.dumps(jsonObj2, sort_keys=True))
        return False
    return True

def is_json_key_exists(jsonObj, jsonKey):
    if jsonKey in jsonObj:
        return True
    return False

def get_command_name(jsonObj):
    if 'CommandName' not in jsonObj:
        print ("CommandName not exits in the json response")
        return ""
    return jsonObj['CommandName']

def is_var_subscription_data(jsonObj):
    if 'ItemSubscriptionID' not in jsonObj:
        return False
    return True