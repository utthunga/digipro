#!/usr/bin/env python2.7

## ATFTestLib.py contains insterfaces for ATF test suite
# @author Santhosh Kumar Selvaraj

from interfacelib.ATFAppIntf import *
from utilitylib.ATFJsonUtil import *
import utilitylib.ATFGlobalVar as globalVar
import time

## Get application interface instance
def get_app_instance():
    if globalVar.gAppInstance == None:
        globalVar.gAppInstance = ATFAppIntf()
        globalVar.gAppInstance.initialize()
    return globalVar.gAppInstance


## Wrapper method for setting protocol type
def set_protocol_type(commandParam):
    print ("Called set protocol type")
    return get_app_instance().executeCommand('SetProtocolType', commandParam)


## Wrapper method for initializing the cpm application
def initialize(commandParam):
    return get_app_instance().executeCommand('Initialize', commandParam)


## Wrapper method for Get Bus parameters
def get_bus_param(commandParam):
    return get_app_instance().executeCommand('GetBusParameters', commandParam)


## Wrapper method for Get Bus parameters
def set_bus_param(commandParam):
    return get_app_instance().executeCommand('SetBusParameters', commandParam)


## Wrapper method for Disconnect from device
def disconnect(commandParam):
    return get_app_instance().executeCommand('Disconnect', commandParam)


## Wrapper method for connect with device
def connect(commandParam):
    return get_app_instance().executeCommand('Connect', commandParam)


# Wrapper method for field device setup
def get_field_device_config(commandParam):
    return get_app_instance().executeCommand('GetFieldDeviceConfig', commandParam)


# Wrapper method for set field device  setup
def set_field_device_config(commandParam):
    return get_app_instance().executeCommand('SetFieldDeviceConfig', commandParam)


## Wrapper method for shutting down the protocol
def shutdown(commandParam):
    return get_app_instance().executeCommand('Shutdown', commandParam)


def start_scan(commandParam):
    return get_app_instance().executeCommand('StartScan', commandParam)


def retrieve_scan_data(waitTime):
    return get_app_instance().retrieveNotificationData(waitTime)


def stop_scan(commandParam):
    return get_app_instance().executeCommand('StopScan', commandParam)


def register_callback(commandName):
    return get_app_instance().registerNotifyFunc(commandName)


def unregister_callback(commandName):
    return get_app_instance().unregisterNotifyFunc(commandName)


def get_item(commandParam):
    return get_app_instance().executeCommand('GetItem', commandParam)


def set_item(commandParam):
    return get_app_instance().executeCommand('SetItem', commandParam)

def Exec_Pre_Edit_Action(commandParam):
    return get_app_instance().executeCommand('ExecPreEditAction', commandParam)

def Exec_Post_Edit_Action(commandParam):
    return get_app_instance().executeCommand('ExecPostEditAction', commandParam)

def process_subscription(commandParam):
    return get_app_instance().executeCommand('ProcessSubscription', commandParam)


def set_lang_code(commandParam):
    return get_app_instance().executeCommand('SetLangCode', commandParam)

def set_app_setting(commandParam):
    return get_app_instance().executeCommand('SetAppSettings', commandParam)

def get_app_setting(commandParam):
    return get_app_instance().executeCommand('GetAppSettings', commandParam)

def retrieve_item_data(commandName=None, waitTime=None):
    return get_app_instance().retrieveNotificationData(waitTime, commandName)


## Add your commands here

## Utility Functions
## Wrapper method for comparing JSON objects
def compare_json_object(jsonObj1, jsonObj2):
    result = compare_json_objects(jsonObj1, jsonObj2)
    return result


def is_json_key_exist(jsonObj, jsonKey):
    return is_json_key_exists(jsonObj, jsonKey)

# def get_memory_data():
#     p = psutil.Process()
#     with p.oneshot():
#         print(p.name())
#         print(p.pid)
#         print(p.ppid())
#         print(p.create_time())
#         prs_id = p.pid
#         meminfo = p.memory_info().rss
#         memo = p.memory_info().vms
#         memory_2 = memo /1024/1024
#         memory_1 = meminfo/(1024 * 1024)
#         print("used memory is %.3fMB" % (memory_2))
#         print(memory_1)
#         return memory_2


