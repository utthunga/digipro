#!/usr/bin/env python2.7
 
from utilitylib.ATFDBusService import *
from utilitylib.JsonRpc import *

# CPM application dbus server name and it's interface/object path
cpm_dbus_service_name='com.fluke.starsky.cpm'
cpm_dbus_object_path='/Cpmcommand'
cpm_dbus_interface_path='local.communicator.ProtoDBus.DbusServerImpl'
cpmPublisherPrefix = "Publisher_"
cpmSubscriberPrefix = "Subscriber_"

# ATF, dbus server name and it's interface/object path
atf_dbus_service_name='com.fluke.cpm.atf'
atf_dbus_interface_path=cpm_dbus_interface_path

# ATF related function
def registerSubscriber(subscriberName, publisherName):
    methodName = "RegisterSubscriber"
    scanSubscriber = cpmSubscriberPrefix + subscriberName
    methodParam = {"component":atf_dbus_service_name, "subscriber":scanSubscriber}
    jsonRpcReqStr = create_json_rpc_req_data(methodName, methodParam)
    jsonRpcRespStr = sendDBusMessage(cpm_dbus_service_name, cpm_dbus_interface_path, publisherName, jsonRpcReqStr)
    return extract_json_rpc_resp_data(jsonRpcRespStr)

def invokeCPMMethod(methodName, methodParam):
    jsonRpcReqStr = create_json_rpc_req_data(methodName, methodParam)
    jsonRpcRespStr = sendDBusMessage(cpm_dbus_service_name, cpm_dbus_interface_path, cpm_dbus_object_path, jsonRpcReqStr)
    return extract_json_rpc_resp_data(jsonRpcRespStr)
 
# CPM API that is exposed to the test suite   
# Invoke 'SetProtocolTytpe' command of CPM
def set_protocol_type(methodParam):
    methodName = "SetProtocolType"
    return invokeCPMMethod(methodName, methodParam)

# Invoke 'SetBusParameters' command of CPM
def set_bus_parameters(methodParam):
    methodName = "SetBusParameters"
    return invokeCPMMethod(methodName, methodParam)
        
# Invoke 'GetBusParameters' command of CPM
def get_bus_parameters():
    methodName = "GetBusParameters"
    return invokeCPMMethod(methodName, {})

# Invoke 'StartScan' command of CPM
def startScan():
    methodName = "StartScan"
    return invokeCPMMethod(methodName, {})

# Invoke 'StopScan' command of CPM
def stopScan():
    methodName = "StopScan"
    return invokeCPMMethod(methodName, {})

# Invoke 'GetFieldDeviceConfig' command of CPM for custom menu
def getFieldDeviceConfig(methodParam):
	methodName = "getFieldDeviceConfig"
	return invokeCPMMethod(methodName, methodParam)

# Invoke 'SetFieldDeviceConfig' command of CPM for custom menu
def getFieldDeviceConfig(methodParam):
	methodName = "setFieldDeviceConfig"
	return invokeCPMMethod(methodName, methodParam)

# Invoke 'Connect' command of CPM
def connect(methodParam):
	methodName = "Connect"
	return invokeCPMMethod(methodName, methodParam)	

# Invoke 'GetItem' command of CPM for custom menu
def getItem(methodParam):
	methodName = "GetItem"
	return invokeCPMMethod(methodName, methodParam)

# Invoke 'DisConnect' command of CPM
def disconnect():
	methodName = "Disconnect"
	return invokeCPMMethod(methodName, {})

# Invoke 'SetLangCode' command of CPM
def setLangCode():
	methodName = "SetLangCode"
	return invokeCPMMethod(methodName, {})
