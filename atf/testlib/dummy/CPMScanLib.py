#!/usr/bin/env python3

from utilitylib.ATFStringUtil import *
from CPMTestLib import *

scanSequenceId = -1
scanArrayData = []
scanEventLock = threading.Event()
atfDBusSrvc = None

# Notify callback function for the scan operation
def scanNotifyCallBack(jsonScanData):
    global scanSequenceId
    global atfDBusSrvc
    if 'message' in jsonScanData:
        jsonDevInfo = jsonScanData['message']
        jsonScanSeqId = int(jsonDevInfo['ScanSeqID'])
        if ((scanSequenceId > -1) and (scanSequenceId != jsonScanSeqId)):
            stopScan()
            scanEventLock.set()
            stopDBusService(atfDBusSrvc)
            return
        else:
            scanSequenceId = jsonScanSeqId
        scanArrayData.append(jsonDevInfo['DeviceInfo'])
    else:
        stopScan()
        stopDBusService(atfDBusSrvc)
        scanEventLock.set()
    return

# Triggers scan operation and return the result to the test suite
def scan_network():
    # Subscriber name for the scan operation
    subscriberName = "SCAN_PUBLISHER"

    # Create dbuslib service and subscribe the required object path
    global atfDBusSrvc
    atfDBusSrvc = createDBusService(atf_dbus_service_name)
    atfScanObjectPath = '/' + cpmSubscriberPrefix + subscriberName
    subscribeDBusObject(atfDBusSrvc, cpm_dbus_object_path)
    subscribeDBusObject(atfDBusSrvc, atfScanObjectPath)

    # Register the call back function that need to be nofified for any operation
    registerNotifyCallBackFuncPtr(atfDBusSrvc, scanNotifyCallBack)

    # Start the dbuslib service
    startDBusService(atfDBusSrvc)
    
    # Send message to CPM to register the subscriber
    regJsonRespStr = registerSubscriber(subscriberName, '/' + cpmPublisherPrefix + subscriberName)
    if (False == compare_string_data(regJsonRespStr['status'], "Subscriber " + cpmSubscriberPrefix + subscriberName + " Registered")):
        stopDBusService(atfDBusSrvc)
        return False

    # Start the scan operation
    startScan()

    # Wait until the scan data is filled
    scanEventLock.clear()
    scanEventLock.wait(120)
    
    # Return the result data
    print('scanArrayData: ', scanArrayData)
    return scanArrayData

def registerObject(subscriberName):
    global atfCpmCommandDBusSrvc
    atfCpmCommandDBusSrvc =  createDBusService(atf_dbus_service_name)
    atfCpmCommandObjectPath = '/' + cpmSubscriberPrefix + subscriberName
    subscribeDBusObject(atfCpmCommandDBusSrvc, atfCpmCommandObjectPath)
    startDBusService(atfCpmCommandDBusSrvc)
    return

if __name__=='__main__':
    #setProtocolParam = {"Protocol":"FF"}
    #setConnectDeviceNode = {"DeviceNodeAddress":34}
    #setCustomMenu = {"ItemContainerTag":"","ItemID":1,"ItemInDD":0,"ItemType":"MENU"}
    #setFieldDevConfig_comm = {"DeviceConfig" : "COMMISSION","NewDeviceNodeAddress": 250,"NewDeviceTag" : "644 Device"}
    #set_protocol_type(setProtocolParam)
    #scan_network()
    #getFieldDeviceConfig(setConnectDeviceNode)
    #setFieldDeviceConfig(setFieldDevConfig_comm)
    #connect(setConnectDeviceNode)     
    #getItem(setCustomMenu)    
    #disconnect()


