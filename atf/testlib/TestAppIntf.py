#!/usr/bin/env python2.7

## Sample application for testing ATF libraries
# @author Santhosh Kumar Selvaraj

from interfacelib.ATFAppIntf import *
import utilitylib.ATFGlobalVar as globalVar
from utilitylib.ATFJsonUtil import *
import time

if __name__=='__main__':
    globalVar.gAppInstance = ATFAppIntf()
    globalVar.gAppInstance.initialize()
    jsonResp = globalVar.gAppInstance.executeCommand('SetLangCode', {
    "LangCode": "en",
    "ShortStrFlag": True
    })
    jsonResp = globalVar.gAppInstance.executeCommand('SetProtocolType', {"Protocol": "FF"})
    print (jsonResp)
    jsonResp = globalVar.gAppInstance.executeCommand('Initialize', {})
    print (jsonResp)
    globalVar.gAppInstance.executeCommand('StartScan', {})
    print ("Scan data: ", globalVar.gAppInstance.retrieveNotificationData(30))
    globalVar.gAppInstance.executeCommand('StopScan', {})
    # for testBusPram in range(0, 10):
    #     print ("Iteration: ", testBusPram)
    #     globalVar.gAppInstance.executeCommand('GetBusParameters', {})
    #     globalVar.gAppInstance.executeCommand('SetBusParameters', {"FF_BUS_PARAMS": {"slot_time": 16}})
    # globalVar.gAppInstance.executeCommand('SetProtocolType', {"Protocol": "FF"})
    #globalVar.gAppInstance.executeCommand('Shutdown', {})
    # globalVar.gAppInstance.executeCommand('StartScan', {})
    # globalVar.gAppInstance.retrieveNotificationData(30)
    # globalVar.gAppInstance.executeCommand('StopScan', {})
    # for testConnect in range (0, 20):
    #     globalVar.gAppInstance.executeCommand('Connect', {"DeviceNodeAddress":33})
    #     globalVar.gAppInstance.executeCommand('Disconnect', {})
    #     time.sleep(5)
    # globalVar.gAppInstance.executeCommand('Shutdown', {})

    # Method execution verification
    # globalVar.gAppInstance.executeCommand('Connect', {"DeviceNodeAddress": 33})
    # fileContent = open('/home/devuser/repository/atf_update/digipro/atf/testsuite/ffsuite/SampleSuite/resource/ATC006_FF_ME_Rsc.json').read()
    # meJsonObj = json.loads(fileContent)
    #
    # globalVar.gAppInstance.registerNotifyFunc('GetItem')
    # getItemJsonObj = globalVar.gAppInstance.executeCommand('GetItem', meJsonObj["Request"][0])
    # print ("Response for the get item: ", getItemJsonObj)
    # # time.sleep(20)
    # notifyDataCount = 0
    # while (notifyDataCount < 19):
    #     notificationJsonObj = globalVar.gAppInstance.retrieveNotificationData(5)
    #     print ("Notification data: ", notificationJsonObj)
    #     notifyDataCount = notifyDataCount + 1
    # globalVar.gAppInstance.unregisterNotifyFunc()
    #
    # globalVar.gAppInstance.registerNotifyFunc('SetItem')
    # setItemJsonObj = globalVar.gAppInstance.executeCommand('SetItem', meJsonObj["Request"][1])
    # print ("Response for the set item: ", setItemJsonObj)
    # # time.sleep(20)
    # notifyDataCount = 0
    # while (notifyDataCount < 4):
    #     notificationJsonObj = globalVar.gAppInstance.retrieveNotificationData(5)
    #     print ("Notification data: ", notificationJsonObj)
    #     notifyDataCount = notifyDataCount + 1
    # globalVar.gAppInstance.unregisterNotifyFunc()

    # # Subscription verification
    # globalVar.gAppInstance.executeCommand('Connect', {"DeviceNodeAddress": 247})
    #
    # fileContent = open('/home/devuser/repository/atf_update/digipro/atf/testsuite/ffsuite/SampleSuite/resource/ATC008_FF_Variable_Rsc.json').read()
    # subsJsonObj = json.loads(fileContent)
    # globalVar.gAppInstance.registerNotifyFunc('ProcessSubscription')
    # # Create subscription
    # jsonResp = globalVar.gAppInstance.executeCommand('ProcessSubscription', subsJsonObj["Subscription"]["Create"]["Request"])
    # isKeyExists = is_json_key_exists(jsonResp["CommandResponse"], "result")
    # print ("Result key exists: ", isKeyExists)
    # subscriptionId = jsonResp["CommandResponse"]["result"]["ItemSubscriptionID"]
    # print ("Subscription ID: ", subscriptionId)
    # # Start subscription
    # startReqObj = subsJsonObj["Subscription"]["Start"]["Request"]
    # startReqObj["ItemSubscriptionID"] = subscriptionId
    # print ("Start subscription request: ", startReqObj)
    # globalVar.gAppInstance.executeCommand('ProcessSubscription', startReqObj)
    # # Get notification item data
    # notificationData = globalVar.gAppInstance.retrieveNotificationData(None, 'ProcessSubscription')
    # print ("Subsctiption Notification Data: ", notificationData)
    #
    # # Execute method in parallel to subscription
    # globalVar.gAppInstance.registerNotifyFunc('GetItem')
    # methodJsonReq = json.loads("{\"ItemContainerTag\":\"__resource_block_2\",\"ItemID\":131649,\"ItemInDD\":true,\"ItemType\":\"METHOD\"}")
    # print ("Method Request Data: ", methodJsonReq)
    # methodJsonResp = globalVar.gAppInstance.executeCommand('GetItem', methodJsonReq)
    # print ("Method Response Data: ", methodJsonResp)
    # methodNotifyData = globalVar.gAppInstance.retrieveNotificationData(5, 'GetItem')
    # print ("Method Notify Data: ", methodNotifyData)
    # methodNotifyData = globalVar.gAppInstance.retrieveNotificationData(5, 'GetItem')
    # print ("Method Notify Data: ", methodNotifyData)
    # globalVar.gAppInstance.unregisterNotifyFunc('GetItem')
    #
    # globalVar.gAppInstance.registerNotifyFunc('SetItem')
    # methodJsonReq = json.loads("{\"ItemContainerTag\":\"\",\"ItemID\":0,\"ItemInfo\":{\"VarValue\":\"0\"},\"ItemReqType\":\"\",\"ItemType\":\"METHOD\"}")
    # print ("Method Request Data: ", methodJsonReq)
    # methodJsonResp = globalVar.gAppInstance.executeCommand('SetItem', methodJsonReq)
    # print ("Method Response Data: ", methodJsonResp)
    # methodNotifyData = globalVar.gAppInstance.retrieveNotificationData(5, 'SetItem')
    # print ("Method Notify Data: ", methodNotifyData)
    # globalVar.gAppInstance.unregisterNotifyFunc('SetItem')
    #
    # # Stop subscription
    # stopReqObj = subsJsonObj["Subscription"]["Stop"]["Request"]
    # stopReqObj["ItemSubscriptionID"] = subscriptionId
    # globalVar.gAppInstance.executeCommand('ProcessSubscription', stopReqObj)
    # # Delete subscription
    # delReqObj = subsJsonObj["Subscription"]["Delete"]["Request"]
    # delReqObj["ItemSubscriptionID"] = subscriptionId
    # globalVar.gAppInstance.executeCommand('ProcessSubscription', delReqObj)
    # globalVar.gAppInstance.unregisterNotifyFunc('ProcessSubscription')

    jsonResp = globalVar.gAppInstance.executeCommand('Disconnect', {})
    print (jsonResp)
    jsonResp = globalVar.gAppInstance.executeCommand('Shutdown', {})
    print (jsonResp)