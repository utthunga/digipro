#!/usr/bin/env python2.7

from interfacelib.commandlib.ATFCmdAbstract import *
from utilitylib.protolib.ATFPBRpc import *
from threading import Lock
import time

class ATFCmdScan(ATFCmdIntf, object):
    def __init__(self):
        super(ATFCmdScan, self).__init__()
        self.commandResult = ''
        self.commandName = ''
        self.notifyScanData = []
        self.scanDataLock = Lock()
        self.scanData = []
        self.scanSequenceID = 1

    __singletonInstance = None
    def __new__(cls, *args, **kwargs):
        if cls.__singletonInstance == None:
            cls.__singletonInstance = object.__new__(cls)
        return cls.__singletonInstance

    def executeCommand(self, commandName, commandParam):
        print ("Scan command: ", commandName, " , commandParam: ", commandParam)
        self.commandName = commandName
        globalVar.gAppInstance.getCommInstance().registerNotifyCallBack("Scan", self.commandNotifyFunc)
        cmdMsgReq = ATFPBRpc().createPBRpcRequestCommand(commandName, json.dumps(commandParam))
        msgResp = globalVar.gAppInstance.getCommInstance().sendAndReceiveData(globalVar.MessageType.MSG_TYPE_COMMAND, cmdMsgReq)
        cmdMsgResp = ATFPBRpc().extractPBRpcResponseCommand(msgResp)
        self.waitForEvent()
        return self.commandResult

    def retriveNotificationData(self, notifyWaitTime=None):
        if notifyWaitTime != None:
            time.sleep(notifyWaitTime)
        self.scanDataLock.acquire()
        scanNotifyData = self.notifyScanData
        # print ("ATFCmdScan: Notification Data: ", self.notifyScanData)
        self.scanDataLock.release()
        return scanNotifyData

    def commandNotifyFunc(self, publisherName, publisherData):
        publisherData = json.loads(extractJsonStringData(publisherData))
        if publisherName == "CMD_RESP_PUBLISHER":
            if get_command_name(publisherData) == 'StartScan':
                self.commandResult = publisherData
            elif get_command_name(publisherData) == 'StopScan':
                globalVar.gAppInstance.getCommInstance().unregisterNotifyCallBack("Scan")
                self.commandResult = publisherData
                self.notifyScanData = ''
            self.setEvent()
        elif publisherName == "SCAN_PUBLISHER":
            scanDataObj = publisherData
            self.scanDataLock.acquire()
            if self.scanSequenceID == scanDataObj['ScanSeqID']:
                if 'DeviceInfo' in scanDataObj:
                    self.scanData.append(scanDataObj['DeviceInfo'])
            else:
                self.scanSequenceID = scanDataObj['ScanSeqID']
                self.notifyScanData = self.scanData
                self.scanData = []
                if 'DeviceInfo' in scanDataObj:
                    self.scanData.append(scanDataObj['DeviceInfo'])
            self.scanDataLock.release()
        # print("ATFCmdScan: publisherName: ", publisherName, " , ", publisherData)