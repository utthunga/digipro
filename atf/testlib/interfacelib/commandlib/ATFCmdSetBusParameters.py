#!/usr/bin/env python2.7

from interfacelib.commandlib.ATFCmdAbstract import *
from utilitylib.protolib.ATFPBRpc import *
from threading import Lock

class ATFCmdSetBusParameters(ATFCmdIntf, object):
    def __init__(self):
        super(ATFCmdSetBusParameters, self).__init__()
        self.commandName = ''
        self.commandResult = ""

    def executeCommand(self, commandName, commandParam):
        self.commandName = commandName
        globalVar.gAppInstance.getCommInstance().registerNotifyCallBack(self.commandName, self.commandNotifyFunc)
        cmdMsgReq = ATFPBRpc().createPBRpcRequestCommand(self.commandName, json.dumps(commandParam))
        msgResp = globalVar.gAppInstance.getCommInstance().sendAndReceiveData(globalVar.MessageType.MSG_TYPE_COMMAND, cmdMsgReq)
        cmdMsgResp = ATFPBRpc().extractPBRpcResponseCommand(msgResp)
        self.waitForEvent()
        return self.commandResult

    def commandNotifyFunc(self, publisherName, publisherData):
        publisherData = json.loads(extractJsonStringData(publisherData))
        if publisherName == "CMD_RESP_PUBLISHER":
            if get_command_name(publisherData) == self.commandName:
                self.commandResult = publisherData
                globalVar.gAppInstance.getCommInstance().unregisterNotifyCallBack(self.commandName)
                self.setEvent()
        # print("ATFCmdSetBusParameters: publisherName: ", publisherName, " , ", publisherData)