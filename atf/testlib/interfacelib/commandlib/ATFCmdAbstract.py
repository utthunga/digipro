#!/usr/bin/env python2.7

## @module ATFCmdIntf Abstract base class for command
# @author  Santhosh Kumar Selvaraj

import json
import threading
from abc import ABCMeta, abstractmethod
import utilitylib.ATFGlobalVar as globalVar
from utilitylib.ATFJsonUtil import *
from utilitylib.ATFStringUtil import *

class ATFCmdIntf():
    ## Constructor
    def __init__(self):
        self.eventLock = threading.Event()
        self.eventState = globalVar.CommandEventState.NO_EVENT
        self.eventStateLock = threading.Condition()

    @abstractmethod
    ## Abstract method for command execution
    def executeCommand(self, commandName, commandParam): pass
    ## Method to retrieve notification data and it can be overridden in derived classes
    def retriveNotificationData(self, notifyWaitTime=None):
        return ''

    ## Wait for event from other thread and wakeup once its notified
    # This is used for the purpose of receiving async command response
    def waitForEvent(self):
        self.eventStateLock.acquire()
        if self.eventState == globalVar.CommandEventState.NO_EVENT:
            self.eventState = globalVar.CommandEventState.WAIT_FOR_EVENT
            self.eventStateLock.wait()
        else:
            # Reset the event to NO_EVENT if setEvent() is triggered first
            self.eventState = globalVar.CommandEventState.NO_EVENT
        self.eventStateLock.release()


    ## Notify other thread(s) who is waiting with the specified event
    # This is used for the purpose of receiving async command response
    def setEvent(self):
        self.eventStateLock.acquire()
        if self.eventState == globalVar.CommandEventState.WAIT_FOR_EVENT:
            self.eventState = globalVar.CommandEventState.NO_EVENT
            self.eventStateLock.notify()
        else:
            # Set the event as SET_EVENT to indicate setEvent() has been triggered
            # before waitForEvent
            self.eventState = globalVar.CommandEventState.SET_EVENT
        self.eventStateLock.release()
