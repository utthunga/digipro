#!/usr/bin/env python2.7

from interfacelib.commandlib.ATFCmdAbstract import *
from utilitylib.protolib.ATFPBRpc import *
from threading import Lock
import time

class ATFCmdProcessSubscription(ATFCmdIntf, object):
    def __init__(self):
        super(ATFCmdProcessSubscription, self).__init__()
        self.commandName = ""
        self.commandResult = ""
        self.notifyData = None
        self.notifyEventLock = threading.Condition()
        self.notifyDataLock = Lock()
        self.notifyMsgState = globalVar.CommandEventState.NO_EVENT

    def executeCommand(self, commandName, commandParam):
        self.notifyData = None
        self.commandName = commandName
        cmdMsgReq = ATFPBRpc().createPBRpcRequestCommand(self.commandName, json.dumps(commandParam))
        msgResp = globalVar.gAppInstance.getCommInstance().sendAndReceiveData(globalVar.MessageType.MSG_TYPE_COMMAND, cmdMsgReq)
        cmdMsgResp = ATFPBRpc().extractPBRpcResponseCommand(msgResp)
        self.waitForEvent()
        return self.commandResult

    def retriveNotificationData(self, notifyWaitTime=None):
        if notifyWaitTime != None:
            time.sleep(notifyWaitTime)
            self.notifyDataLock.acquire()
            notifyData = self.notifyData
            self.notifyDataLock.release()
        else:
            # Wait until you receive data for subscribed variable
            if self.notifyMsgState == globalVar.CommandEventState.SET_EVENT:
                self.notifyDataLock.acquire()
                notifyData = self.notifyData
                self.notifyDataLock.release()
            elif self.notifyMsgState == globalVar.CommandEventState.NO_EVENT:
                self.notifyMsgState = globalVar.CommandEventState.WAIT_FOR_EVENT
                self.notifyEventLock.acquire()
                self.notifyEventLock.wait()
                self.notifyEventLock.release()
                self.notifyDataLock.acquire()
                notifyData = self.notifyData
                self.notifyDataLock.release()
            self.notifyMsgState = globalVar.CommandEventState.NO_EVENT
        # print ("ATFCmdProcessSubscription: Notification Data: ", self.notifyData)
        return notifyData

    def commandNotifyFunc(self, publisherName, publisherData):
        publisherData = json.loads(extractJsonStringData(publisherData))
        if publisherName == "CMD_RESP_PUBLISHER":
            if get_command_name(publisherData) == self.commandName:
                self.commandResult = publisherData
                self.setEvent()
        elif publisherName == "ITEM_PUBLISHER":
            if is_var_subscription_data(publisherData) == True:
                self.notifyDataLock.acquire()
                # TODO: Consider only first notification message as it contains valid data
                # and discard other notification messages
                if self.notifyData == None:
                    self.notifyData = publisherData
                    if self.notifyMsgState == globalVar.CommandEventState.WAIT_FOR_EVENT:
                        self.notifyEventLock.acquire()
                        self.notifyEventLock.notify()
                        self.notifyEventLock.release()
                    self.notifyMsgState = globalVar.CommandEventState.SET_EVENT
                self.notifyDataLock.release()
        # print("ATFCmdProcessSubscription: publisherName: ", publisherName, " , ", publisherData)