#!/usr/bin/env python2.7

from interfacelib.commandlib.ATFCmdAbstract import *
from utilitylib.protolib.ATFPBRpc import *
from threading import Lock
from collections import deque
import time

class ATFCmdSetItem(ATFCmdIntf, object):
    def __init__(self):
        super(ATFCmdSetItem, self).__init__()
        self.commandName = ''
        self.commandResult = ""
        self.notifyData = deque([])
        self.notifyEventLock = Lock()
        self.notifyDataLock = Lock()

    def executeCommand(self, commandName, commandParam):
        self.commandName = commandName
        self.notifyData.clear()
        cmdMsgReq = ATFPBRpc().createPBRpcRequestCommand(self.commandName, json.dumps(commandParam))
        msgResp = globalVar.gAppInstance.getCommInstance().sendAndReceiveData(globalVar.MessageType.MSG_TYPE_COMMAND, cmdMsgReq)
        cmdMsgResp = ATFPBRpc().extractPBRpcResponseCommand(msgResp)
        self.waitForEvent()
        return self.commandResult

    def retriveNotificationData(self, notifyWaitTime=None):
        notifyItemData = ''
        notifyItemCount = len(self.notifyData)
        if notifyItemCount == 0:
            if notifyWaitTime != None:
                time.sleep(notifyWaitTime)
            notifyItemCount = len(self.notifyData)
        if notifyItemCount > 0:
            self.notifyDataLock.acquire()
            notifyItemData = self.notifyData.popleft()
            self.notifyDataLock.release()
        return notifyItemData

    def commandNotifyFunc(self, publisherName, publisherData):
        publisherData = json.loads(extractJsonStringData(publisherData))
        if publisherName == "CMD_RESP_PUBLISHER":
            if get_command_name(publisherData) == self.commandName:
                self.commandResult = publisherData
                self.setEvent()
        elif publisherName == "ITEM_PUBLISHER":
            if is_var_subscription_data(publisherData) == False:
                self.notifyDataLock.acquire()
                self.notifyData.append(publisherData)
                self.notifyDataLock.release()
        # print("ATFCmdSetItem: publisherName: ", publisherName, " , ", publisherData)