#!/usr/bin/env python2.7

## @module ATFAppIntf Application interface layer
# @author  Santhosh Kumar Selvaraj

from utilitylib.ATFGlobalVar import *
from commlib.dbuslib.ATFCommDBus import *
from interfacelib.commandlib.ATFCmdSetProtocolType import *
from interfacelib.commandlib.ATFCmdInitialize import *
from interfacelib.commandlib.ATFCmdShutdown import *
from interfacelib.commandlib.ATFCmdConnect import *
from interfacelib.commandlib.ATFCmdDisconnect import *
from interfacelib.commandlib.ATFCmdGetBusParameters import *
from interfacelib.commandlib.ATFCmdSetBusParameters import *
from interfacelib.commandlib.ATFCmdGetItem import *
from interfacelib.commandlib.ATFCmdSetItem import *
from interfacelib.commandlib.ATFCmdGetFieldDeviceConfig import *
from interfacelib.commandlib.ATFCmdSetFieldDeviceConfig import *
from interfacelib.commandlib.ATFCmdProcessSubscription import *
from interfacelib.commandlib.ATFCmdGetAppSettings import *
from interfacelib.commandlib.ATFCmdSetAppSettings import *
from interfacelib.commandlib.ATFCmdScan import *
from interfacelib.commandlib.ATFCmdSetLangCode import *
from interfacelib.commandlib.ATFCmdExecPreEditAction import *
from interfacelib.commandlib.ATFCmdExecPostEditAction import *
from threading import Lock

## Setting default encoding scheme to 'latin1'
# This is required to encode / decode protobuf messages (as it contains raw byte value > 128)
import sys
reload(sys)
sys.setdefaultencoding("latin1")

## ATFAppIntf Application interface layer class
# This class serves following purpose
#   1. Creates and initializes the communication layer instance
#   2. Maintains all command instance
#   3. Acts as a main interface layer and hides command/communication layer to the user
class ATFAppIntf(object):
    ## Constructor
    def __init__(self):
        self.commInstance = None
        self.commandTable = dict()
        self.commandLock = Lock()
        self.commandInstance = None

    ## Overrided __new__ method and return singleton instance
    __singletonInstance = None
    def __new__(cls, *args, **kwargs):
        if cls.__singletonInstance == None:
            cls.__singletonInstance = object.__new__(cls)
        return cls.__singletonInstance

    ## Initializes command / communication layer
    # @param self   object pointer
    def initialize(self):
        if self.getCommInstance() != None:
            self.commInstance.initialize()
        self.buildCommandTable()

    ## Shutdown command / communication layer
    # @param self   object pointer
    def shutdown(self):
        if self.getCommInstance() != None:
            self.getCommInstance().shutdown()
        self.commandTable.clear()

    ## Provides communication instance to the required module
    # @param self   object pointer
    def getCommInstance(self):
        if self.commInstance == None:
            if commType == "DBUS":
                self.commInstance = ATFCommDBus()
        return self.commInstance

    ## Contructs command table with the help of python dictionary
    # @param self   object pointer
    def buildCommandTable(self):
        self.commandTable['SetProtocolType'] = ATFCmdSetProtocolType()
        self.commandTable['Initialize'] = ATFCmdInitialize()
        self.commandTable['Shutdown'] = ATFCmdShutdown()
        self.commandTable['Connect'] = ATFCmdConnect()
        self.commandTable['Disconnect'] = ATFCmdDisconnect()
        self.commandTable['GetBusParameters'] = ATFCmdGetBusParameters()
        self.commandTable['SetBusParameters'] = ATFCmdSetBusParameters()
        self.commandTable['GetItem'] = ATFCmdGetItem()
        self.commandTable['SetItem'] = ATFCmdSetItem()
        self.commandTable['GetFieldDeviceConfig'] = ATFCmdGetFieldDeviceConfig()
        self.commandTable['SetFieldDeviceConfig'] = ATFCmdSetFieldDeviceConfig()
        self.commandTable['ProcessSubscription'] = ATFCmdProcessSubscription()
        self.commandTable['GetAppSettings'] = ATFCmdGetAppSettings()
        self.commandTable['SetAppSettings'] = ATFCmdSetAppSettings()
        self.commandTable['StartScan'] = ATFCmdScan()
        self.commandTable['StopScan'] = ATFCmdScan()
        self.commandTable['SetLangCode'] = ATFCmdSetLangCode()
        self.commandTable['ExecPreEditAction'] = ATFCmdExecPreEditAction()
        self.commandTable['ExecPostEditAction'] = ATFCmdExecPostEditAction()


    ## Retrives notificaiton data if there is any available in command layer
    # @param self   object pointer
    # @param waitTime   waiting time for notification messages (this API will be blocked until waitTime and its in
    #                   seconds)
    def retrieveNotificationData(self, waitTime=None, commandName=None):
        if commandName != None:
            commandInstance = self.commandTable[commandName]
            notifyJsonObj = commandInstance.retriveNotificationData(waitTime)
        else:
            notifyJsonObj = self.commandInstance.retriveNotificationData(waitTime)
        # print ("retrieveNotificationData: ", notifyJsonObj)
        return notifyJsonObj

    ## Register command notify function for the required command
    # @param self   object pointer
    # @param commandName    command name of the method that needs registration
    def registerNotifyFunc(self, commandName):
        self.getCommInstance().registerNotifyCallBack(commandName, self.commandTable[commandName].commandNotifyFunc)

    ## Unregister command notify function for the required command
    def unregisterNotifyFunc(self, commandName):
        self.getCommInstance().unregisterNotifyCallBack(commandName)

    ## Execute specific command provided by the use
    # @param self   object pointer
    # @param commandName   command that need to be executed
    # @param commandParam   command parameter
    def executeCommand(self, commandName, commandParam):
        self.commandInstance = self.commandTable[commandName]
        return self.commandInstance.executeCommand(commandName, commandParam)
