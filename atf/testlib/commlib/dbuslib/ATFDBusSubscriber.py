#!/usr/bin/env python2.7

## @module ATFDBusSubscriber Abstract Base class for all the subscribers
# @author  Santhosh Kumar Selvaraj

from utilitylib.ATFGlobalVar import *
from utilitylib.ATFDBusService import *
from utilitylib.protolib.ATFPBRpc import *
from abc import abstractmethod

## ATFDBusSubscriber Handles notification data from cpm application
class ATFDBusSubscriber():
    # Constructor for atf dbus subscriber
    def __init__(self):
        self.dbusSubscriberObj = ""
        self.dbusSubscriberObjPath = ""
        self.dbusIntfNotifyCallBackFuncPtr = None

    @abstractmethod
    ## Provides publisher name for the derived instance
    # @param self   object pointer
    def publisher(self): pass
    ## Provides subscriber name
    # @param self   object pointer
    def subscriber(self): pass
    ## Parse notification data
    # @param self   object pointer
    # @param notifyData  data published by cpm application
    def parseNotification(self, notifyData): pass

    ## Create subscriber instance and register it to the dbuslib connection
    # @param self   object pointer
    # @param notifyFuncPtr  call back function
    def startSubscriber(self, notifyFuncPtr):
        # Create a dbuslib service objectl
        self.dbusSubscriberObj = createDBusService(atf_dbus_service_name)
        self.dbusSubscriberObjPath = '/' + self.subscriber()
        # Export dbuslib subscriber object to dbuslib connection and register the object path
        subscribeDBusObject(self.dbusSubscriberObj, self.dbusSubscriberObjPath)
        # Start the dbuslib service object instance
        startDBusService(self.dbusSubscriberObj)
        # Register subscriber for further communication
        self.registerSubscriber()
        # Register notify call back function for subscriber
        self.dbusSubscriberObj.registerNotifyCallBackFuncPtr(self.notifyFunc)
        # Set callback for dbus interface
        self.dbusIntfNotifyCallBackFuncPtr = notifyFuncPtr

    ## Remove object from dbuslib connection and destroy subscriber instance
    # @param self   object pointer
    def stopSubscriber(self):
        # Unregister the subscriber
        self.unregisterSubscriber()
        # Stop the dbuslib service object instance
        stopDBusService(self.dbusSubscriberObj)
        # Remove dbuslib subscriber object from dbuslib connection and unregister the object path
        # TODO: Need to to proper unsubscriber
        # unsubscribeDBusObject(self.dbusSubscriberObj)
        self.notifyCallBackFuncPtr = None

    ## Register subscriber in the specified object path for ATF service
    # @param self   object pointer
    def registerSubscriber(self):
        pbJsonRpcObj = ATFPBRpc()
        methodName = "RegisterSubscriber"
        # Create protobuf register message and encapsulate request within it
        methodParam = pbJsonRpcObj.createPBRegisterRequest(methodName, ('/' + self.subscriber()), atf_dbus_service_name)
        # Create protobuf request command message
        reqData = pbJsonRpcObj.createPBRpcRequestCommand(methodName, methodParam)
        # Send protobuf message over dbus
        respData = ATFDBusService(cpmDBusServiceName, cpmDBusInterfacePath, '/' + cpmPublisherPrefix + self.publisher()).sendData(reqData)
        # Extract response from protobuf message
        return pbJsonRpcObj.extractPBRpcResponseCommand(respData)

    ## Un-Register subscriber or remove specific object path from ATF service
    # @param self   object pointer
    def unregisterSubscriber(self):
        pbJsonRpcObj = ATFPBRpc()
        methodName = "UnRegisterSubscriber"
        # Create protobuf register message and encapsulate request within it
        methodParam = pbJsonRpcObj.createPBRegisterRequest(methodName, ('/' + self.subscriber()), atf_dbus_service_name)
        # Create protobuf request command message
        reqData = pbJsonRpcObj.createPBRpcRequestCommand(methodName, methodParam)
        # Send protobuf message over dbus
        respData = ATFDBusService(cpmDBusServiceName, cpmDBusInterfacePath, ('/' + cpmPublisherPrefix + self.publisher())).sendData(reqData)
        # Extract response from protobuf message
        return pbJsonRpcObj.extractPBRpcResponseCommand(respData)

    ## Notify function that is triggered if any of the subscribers request / notify message from CPM application
    # @param self   object pointer
    # @param notifyData     notify message
    def notifyFunc(self, notifyData):
        # Prepare dummy protobuf request command object
        request = Rpc_pb2.RpcRequestCommand()
        # Convert dbus array data to byte array
        notifyByteArray = ''.join([chr(byte) for byte in notifyData])
        # Convert byte array to str (in python 'str' meant to be any byte sequence where
        # each byte contain data of specified encoding format)
        notifyStr = str.encode(notifyByteArray)
        # Parse protobuf message and extract the request message from CPM application
        request.ParseFromString(notifyStr)
        if request.method == 'Update':
            requestType = RequestType.REQ_TYPE_UPDATE
            notifyJsonData = self.parseNotification(request.params)
            # Send the extracted message to the dbus interface layer
            self.dbusIntfNotifyCallBackFuncPtr(self.publisher(), notifyJsonData)
        elif request.method == 'Ping':
            requestType = RequestType.REQ_TYPE_PING
        # Prepare response data
        response = Rpc_pb2.RpcResponseCommand()
        response.id = request.id
        response.result = ''
        return response.SerializeToString()