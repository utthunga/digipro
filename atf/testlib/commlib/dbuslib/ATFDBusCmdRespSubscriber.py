#!/usr/bin/env python2.7

## @module ATFDBusCmdRespSubscriber Handled notification messages that is sent for Command Response Publisher
# @author  Santhosh Kumar Selvaraj

from commlib.dbuslib.ATFDBusSubscriber import *

## ATFDBusCmdRespSubscriber Handled notification messages that is sent for Command Response Publisher
class ATFDBusCmdRespSubscriber(ATFDBusSubscriber,object):
    ## Constructor for atf dbus command response subscriber
    def __init__(self):
        super(ATFDBusCmdRespSubscriber, self).__init__()
        self.publisherName = "CMD_RESP_PUBLISHER"

    ## Provides publisher name for command response publisher
    # @param self   object pointer
    def publisher(self):
        return self.publisherName

    ## Provides subscriber name for command response publisher
    # @param self   object pointer
    def subscriber(self):
        return (cpmSubscriberPrefix + self.publisherName)

    ## Parse message intermittently (currenlt not used and its for future purpose)
    def parseNotification(self, notifyData):
        return notifyData