#!/usr/bin/env python2.7

## @module ATFDBusCmdRespSubscriber Handled notification messages that is sent for Item Publisher
# @author  Santhosh Kumar Selvaraj

from commlib.dbuslib.ATFDBusSubscriber import *

## ATFDBusCmdRespSubscriber Handled notification messages that is sent for Item Publisher
class ATFDBusItemSubscriber(ATFDBusSubscriber,object):
    ## Constructor for atf dbus command response subscriber
    def __init__(self):
        super(ATFDBusItemSubscriber, self).__init__()
        self.publisherName = "ITEM_PUBLISHER"

    ## Provides publisher name for command response publisher
    # @param self   object pointer
    def publisher(self):
        return self.publisherName

    ## Provides subscriber name for item publisher
    # @param self   object pointer
    def subscriber(self):
        return (cpmSubscriberPrefix + self.publisherName)

    ## Parse message intermittently (currenlt not used and its for future purpose)
    def parseNotification(self, notifyData):
        return notifyData