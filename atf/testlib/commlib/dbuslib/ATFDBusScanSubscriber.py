#!/usr/bin/env python2.7

## @module ATFDBusCmdRespSubscriber Handled notification messages that is sent for Scan Publisher
# @author  Santhosh Kumar Selvaraj

from commlib.dbuslib.ATFDBusSubscriber import *


## ATFDBusCmdRespSubscriber Handled notification messages that is sent for Scan Publisher
class ATFDBusScanSubscriber(ATFDBusSubscriber,object):
    def __init__(self):
        super(ATFDBusScanSubscriber, self).__init__()
        self.publisherName = "SCAN_PUBLISHER"

    # Provides publisher name for the derived instance
    def publisher(self):
        return self.publisherName

    ## Provides subscriber name for scan publisher
    # @param self   object pointer
    def subscriber(self):
        return (cpmSubscriberPrefix + self.publisherName)

    ## Parse message intermittently (currenlt not used and its for future purpose)
    def parseNotification(self, notifyData):
        return notifyData