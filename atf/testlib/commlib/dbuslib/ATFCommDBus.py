#!/usr/bin/env python2.7

## @module ATFCommDBusIntf Acts more like dbus communication layer and it exposes APIs to
# application interface modules
# @author  Santhosh Kumar Selvaraj

import time
from threading import Lock
from commlib.ATFCommIntf import *
from utilitylib.ATFDBusService import *
from commlib.dbuslib.ATFDBusItemSubscriber import *
from commlib.dbuslib.ATFDBusScanSubscriber import *
from commlib.dbuslib.ATFDBusCmdRespSubscriber import *

## ATFCommDBusIntf Maintains dbus subscribers and acts as a interface layer for dbus service
class ATFCommDBus(ATFCommIntf):
    # Constructor for atf dbus comm interface
    def __init__(self):
        self.notifyLock = Lock()
        self.notifyCallBackFuncLock = Lock()
        self.itemSubscriberObj = ATFDBusItemSubscriber()
        self.scanSubscriberObj = ATFDBusScanSubscriber()
        self.cmdRespSubscriberObj = ATFDBusCmdRespSubscriber()
        self.notifyCallBackFuncPtr = dict()

    ## Initializes dbus service and starts all dbus subscribers
    # @param self   object pointer
    def initialize(self):
        self.startSubscribers()

    ## Shutdown all dbus subscribers
    # @param self   object pointer
    def shutdown(self):
        self.stopSubcribers()

    ## Send message over dbus provided from above layer via dbus service
    # @param self   object pointer
    # @param msgType message type (currently onym essage type command is considered
    # @param nsgDats Data that need to be sent to other application
    def sendAndReceiveData(self, msgType, msgData):
        # Ignore msgType currently as it's not used anywhere and it has been declared only for future purpose
        if msgType == MessageType.MSG_TYPE_COMMAND:
            # Creates dbus service instance and send message over dbus
            atfDBusServiceObj = ATFDBusService(cpmDBusServiceName, cpmDBusInterfacePath, cpmDBusCommandObjectPath)
            return atfDBusServiceObj.sendData(msgData)
        else:
            return None

    ## Register call back function needed for application layer
    # @param self   object pointer
    # @param callBackFuncPtr    call back function from application layer
    def registerNotifyCallBack(self, callBackFuncKey, callBackFuncPtr):
        self.notifyCallBackFuncLock.acquire()
        self.notifyCallBackFuncPtr[callBackFuncKey] = callBackFuncPtr
        self.notifyCallBackFuncLock.release()

    ## Unregisters call back function
    # @param self   object pointer
    def unregisterNotifyCallBack(self, callBackFuncKey):
        self.notifyCallBackFuncLock.acquire()
        if len(self.notifyCallBackFuncPtr) != 0:
            del self.notifyCallBackFuncPtr[callBackFuncKey]
        self.notifyCallBackFuncLock.release()

    ## Notify function that is triggered as call back from low level dbus service layer
    # This API wil be called whenever we receive any message from outside application
    # @param self   object pointer
    # @param publisherName  name of the publisher that publishes this data
    # @param publisherData  data provide dby the publisher
    def notifyFunc(self, publisherName, publisherData):
        self.notifyLock.acquire()
        self.notifyCallBackFuncLock.acquire()
        # Shallow copy the content to avoid manipulation of dictionary object during
        # un-register notify call back function
        notifyDict = self.notifyCallBackFuncPtr.copy()
        self.notifyCallBackFuncLock.release()
        # Trigger notification message to all  notify call function
        # in the dictionary
        for notifyCallBackFuncKey in notifyDict:
           notifyDict[notifyCallBackFuncKey](publisherName, publisherData)
        self.notifyLock.release()

    ## Start all dbus subscriber services
    # @param self   object pointer
    def startSubscribers(self):
        self.cmdRespSubscriberObj.startSubscriber(self.notifyFunc)
        self.scanSubscriberObj.startSubscriber(self.notifyFunc)
        self.itemSubscriberObj.startSubscriber(self.notifyFunc)

    ## Stop all dbus subscriber services
    # @param self   object pointer
    def stopSubcribers(self):
        self.itemSubscriberObj.stopSubscriber()
        self.scanSubscriberObj.stopSubscriber()
        self.cmdRespSubscriberObj.stopSubscriber()