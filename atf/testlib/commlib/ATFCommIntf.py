#!/usr/bin/env python2.7

## @module ATFCommIntf Abstract base class for communication layer
# @author  Santhosh Kumar Selvaraj

from abc import ABCMeta, abstractmethod

## ATFCommIntf Abstract base class for communication layer
class ATFCommIntf():
    @abstractmethod
    def initialize(self): pass
    def shutdown(self): pass
    def sendAndReceiveData(self, msgType, msgData): pass
    def registerNotifyCallBack(self, callBackFuncKey, callBackFuncPtr): pass
    def unregisterNotifyCallBack(self, callBackFuncKey): pass
    def notifyFunc(self, publisherName, publisherData): pass