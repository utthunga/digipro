*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup     run keywords        SetLangCodeEnKW
...             AND                 SetProtocolTypeKW
...             AND                 InitializeStackKW
...             AND                 Connect644SuperDDKW

Suite Teardown   Run Keywords       DisconnectDeviceKW     "TRUE"
...              AND                ShutdownStackKW        "TRUE"

*** Variables ***
${WriteAsOne}     ${SuperDDResource}/Write_As_one
${EditDisplay}     ${SuperDDResource}/Edit_Display


*** Test Cases ***

ATC001 - Verify user can fetch ItemType as Menu in Write_As_one
    [Tags]           Critical        Menu    WriteAsOne
     ${JsonFileContent}         get file                ${WriteAsOne}/FF_Write_as_one_menu_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify user can fetch ItemType as Variable in Write_As_one
    [Tags]           Critical        Variable    WriteAsOne
    ${JsonFileContent}          get file                ${WriteAsOne}/FF_Write_as_one_Variable_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can verify read/write inside Write_As_one
    [Tags]  Critical    Variable    WriteAsOne
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][0]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC004 - Verify user cannot fetch WriteAsOne with Invalid ItemID =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${WriteAsOne}/FF_Write_as_one_ItemID_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
#

ATC005 - Verify user cannot fetch WriteAsOne with Invalid ItemInDD =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${WriteAsOne}/FF_Write_as_one_ItemID_Error_Rsc.json
   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
#

#ATC006 - Verify user cannot fetch WriteAsOne with Invalid ItemType =Error
#    [Tags]  Critical    Menu    Invalid
#    ${JsonFileContent}          get file               ${WriteAsOne}/FF_Write_as_one_ItemID_Error_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                   register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
#                                         unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}