*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup     run keywords        SetLangCodeEnKW
...             AND                 SetProtocolTypeKW
...             AND                 InitializeStackKW
...             AND                 Connect644SuperDDKW

Suite Teardown   Run Keywords       DisconnectDeviceKW     "TRUE"
...              AND                ShutdownStackKW        "TRUE"

*** Variables ***
${EditDisplay}     ${SuperDDResource}/Edit_Display

*** Test Cases ***
ATC001 - Verify user can fetch Edit_Display menu
    [Tags]           Critical        Menu    Edit_Display
     ${JsonFileContent}         get file                ${EditDisplay}/FF_Edit_display_menu_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

# Issue in CPM Menu should be validate for the certain variable.
#ATC002 - Verify user can fetch Edit_Display Variable
#    [Tags]           Critical        Menu    Edit_Display
#    ${JsonFileContent}           get file                ${EditDisplay}/FF_Edit_display_variable_Rsc.json
#     ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                 register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
#                                 unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}

# Issue in CPM Menu should be validate for the certain variable.
#ATC003 - Verify user can fetch Edit_Display,Write_as_one in single menu
#    [Tags]           Critical        Menu    Edit_Display
#    ${JsonFileContent}          get file                ${EditDisplay}/FF_Edit_display & WriteAsone_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}
###

ATC004 - Verify user can fetch Edit Items as read/write inside Edit_display
    [Tags]  Critical    Variable    EDIT_ITEMS
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][0]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

#
#
ATC005 - Verify user can fetch Display Items as read inside Edit_display
    [Tags]  Critical    Variable    EDIT_ITEMS
     ${JsonFileContent}          get file               ${EditDisplay}/FF_Display_items_in_Edit_display_Rsc.json
     ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                 register callback       GetItem
     ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                 unregister callback     GetItem
     ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
     ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true             True==${VariableCompStatus}

ATC006 - Verify Pre_edit_Actions for menu
    [Tags]  Critical          Pre_edit_Actions
    ${JsonFileContent}          get file                ${EditDisplay}/FF_Edit_Display_PreEditActions_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ExecPreEditAction
    ${SetItemResponse}          Exec_Pre_Edit_Action              ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      ExecPreEditAction     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                               unregister callback       ExecPreEditAction
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     SetItem
    ${Status}                           register callback       GetItem
    ${ActualStatus}             get item                ${JsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${JsonObj['Response'][2]}
                                unregister callback       GetItem
    ${GetItemStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetItemStatus}

ATC007 - Verify Post_edit_Actions for menu
    [Tags]  Critical          Post_edit_Actions
    ${JsonFileContent}          get file                ${EditDisplay}/FF_Edit_Display_PostEditActions_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ExecPostEditAction
    ${SetItemResponse}          Exec_Post_Edit_Action              ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      ExecPostEditAction     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                               unregister callback       ExecPostEditAction
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     SetItem
    ${Status}                           register callback       GetItem
    ${ActualStatus}             get item                ${JsonObj["Request"][2]}
    ${ExpectedStatus}           set variable            ${JsonObj['Response'][2]}
                                unregister callback       GetItem
    ${GetItemStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetItemStatus}


ATC008 - Verify user can Validate ItemReadOnly in Edit Items for Edit_display =Error
    [Tags]  Critical    Variable    EDIT_ITEMS
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC009 - Verify user can Validate ItemReadOnly in Display Items for Edit_display =Error
    [Tags]  Critical    Variable    EDIT_ITEMS
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Display_items_in_Edit_display_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC010 - Verify user cannot fetch EDit_display with Invalid ItemID =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_display_menu_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC0011 - Verify user cannot fetch EDit_display with Invalid ItemInDD =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_display_menu_Error_Rsc.json
   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

#ATC0012 - Verify user cannot fetch EDit_display with Invalid ItemType =Error
#    [Tags]  Critical    Menu    Invalid
#    ${JsonFileContent}          get file               ${EditDisplay}/FF_Edit_display_menu_Error_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                   register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
#                                         unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}
#