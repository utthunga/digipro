*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

*** Variables ***
${EditActionResource}     ${HTKResource}/editActions

*** Test Cases ***
ATC001 - Verify connect with 0644 device with super DD.
    SetProtocolTypeKW
    SetProtocolTypeKW
    InitializeStackKW      "TRUE"
    [Tags]           FF      0644       Critical        HTK       Connect_Disconnect        SuperDD
    ${JsonFileContent}          get file                ${EditActionResource}/FF_ConnectDevice_0644_SuperDD_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

ATC002 - Verify user can execute single pre edit action.
    [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${EditActionResource}/FF_0644_EditActions_Single_PeEditAction_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer     20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_1}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
              \   ${NotifyIndex_1}            evaluate                  ${NotifyIndex_1}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}

ATC003 - Verify user can execute single pre and single post edit action.
    [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${EditActionResource}/FF_0644_EditActions_Pre-PostEditAction_Single_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer     20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_1}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
              \   ${NotifyIndex_1}            evaluate                  ${NotifyIndex_1}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${Status_4R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
     ${NotifyTimeout_4}          convert to integer     20
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_4}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
              \   ${NotifyIndex_4}            evaluate                  ${NotifyIndex_4}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][3]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_5}        set item                ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_5"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_5}          compare json object     ${SetItemResponse_5}    ${MEJsonObj["Response"][4]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_6}        set item                ${MEJsonObj["Request"][5]}
     ${NotifyTimeout_6}          convert to integer      10
     ${NotifyIndex_6}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_6}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_6"][${NotifyIndex_6}]}
         \   ${NotifyIndex_6}            evaluate                ${NotifyIndex_6}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_6}    ${MEJsonObj["Response"][5]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}


ATC004 - Verify user can Cancel method with single pre edit action.
    [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${EditActionResource}/FF_0644_EditActions_Single_PeEditAction_CancelMethod_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer     20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_1}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
              \   ${NotifyIndex_1}            evaluate                  ${NotifyIndex_1}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}

ATC005 - Verify user can execute single pre edit action with abort method.
    [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${EditActionResource}/FF_0644_EditActions_Single_PeEditAction_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer     20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_1}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
              \   ${NotifyIndex_1}            evaluate                  ${NotifyIndex_1}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_4}        set item                ${MEJsonObj["Request"][3]}
     ${NotifyTimeout_4}          convert to integer      10
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_4}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object     ${SetItemResponse_4}    ${MEJsonObj["Response"][3]}
     ${Status_4U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_4}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_5}        set item                ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_5"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object     ${SetItemResponse_5}    ${MEJsonObj["Response"][]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}

ATC006 - Verify user can execute single post edit action.
    [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${EditActionResource}/FF_0644_EditActions_Single_PostEditAction_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       SetItem
     ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer     20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   3
              \   ${NotificationData}        retrieve item data        SetItem     ${NotifyTimeout_1}
              \   ${ExpectedNotification}     set variable              ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
              \   ${NotifyIndex_1}            evaluate                  ${NotifyIndex_1}+1
              \   ${NotifyStatus}             compare json object       ${NotificationData}     ${ExpectedNotification}
              \   should be true              True==${NotifyStatus}
     ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${SetItemStatus}
     ${StatusU}                  unregister callback     SetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}

