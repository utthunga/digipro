*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot


Suite Setup     run keywords        SetLangCodeEnKW
...             AND                 SetProtocolTypeKW
...             AND                 InitializeStackKW
...             AND                 Connect644SuperDDKW

Suite Teardown   Run Keywords       DisconnectDeviceKW     "TRUE"
...              AND                ShutdownStackKW        "TRUE"

*** Variables ***
${BitEnumItem}     ${SuperDDResource}/BitEnum_Item

*** Test Cases ***
ATC001 - Verify user can fetch Bit Enumeration items in menu
    [Tags]           Critical       Menu                    BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Item_Menu_Rsc.json
    ${BitEnumItemMenuJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemMenuJsonObj["BitEnumMenu"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemMenuJsonObj["BitEnumMenu"]["Response"]}
    ${MenuCompStatus}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${MenuCompStatus}

ATC002 - Verify user can get BitEnumeration ReadOnly Item
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Item_Var_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["BitEnum_Item_ReadOnly"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_Item_ReadOnly"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC003 - Verify user can set BitEnumeration ReadOnly Item
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Item_Var_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       SetItem
    ${ActualStatus}                 set item                ${BitEnumItemJsonObj["BitEnum_Item_Write_ReadOnly"]["Request"]}
                                    unregister callback     SetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_Item_Write_ReadOnly"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC004 - Verify user can get BitEnumeration Item
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Item_Var_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["BitEnum_ReadVar"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_ReadVar"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC005 - Verify user can set BitEnumeration Item
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Item_Var_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       SetItem
    ${ActualStatus}                 set item                ${BitEnumItemJsonObj["BitEnum_WriteVar"]["SetVar"]["Request"]}
                                    unregister callback     SetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_WriteVar"]["SetVar"]["Response"]}
    ${SetVarStatus}                 compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true                 True==${SetVarStatus}
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["BitEnum_WriteVar"]["GetVar"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_WriteVar"]["GetVar"]["Response"]}
    ${GetVarStatus}                 compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetVarStatus}

ATC007 - Verify user can get BitEnumeration Item with Incorrect Bit Index (BitIndex - 0)
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["IncorrectBitIndex"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["IncorrectBitIndex"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC008 - Verify user can get BitEnumeration Item with Invalid Bit Index - (BitIndex - "abc")
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["InvalidBitIndex"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["InvalidBitIndex"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

#DP-1307
#ATC009 - Verify user can get BitEnumeration Item with Incorrect Container ID - (Container ID - 0)
#    [Tags]           Critical       BitEnum Var             BitEnum_Item
#    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
#    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
#                                    register callback       GetItem
#    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["IncorrectContID"]["Request"]}
#                                    unregister callback     GetItem
#    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["IncorrectContID"]["Response"]}
#    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true                  True==${VarCompStatus}

#DP-1307
#ATC0010 - Verify user can get BitEnumeration Item with Invalid Container ID - (Container ID - "abc")
#    [Tags]           Critical       BitEnum Var             BitEnum_Item
#    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
#    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
#                                    register callback       GetItem
#    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["InvalidContID"]["Request"]}
#                                    unregister callback     GetItem
#    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["InvalidContID"]["Response"]}
#    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true                  True==${VarCompStatus}
#    sleep                           1s

ATC011 - Verify user can get BitEnumeration Item with Incorrect SubIndex - (SubIndex - 0)
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["IncorrectSubIndex"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["IncorrectSubIndex"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC0012 - Verify user can get BitEnumeration Item with Invalid SubIndex - (SubIndex - "abc")
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["InvalidSubIndex"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["InvalidSubIndex"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC013 - Verify user can get BitEnumeration Item with Incorrect ItemID - (ItemID - 0)
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["IncorrectItemID"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["IncorrectItemID"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC0014 - Verify user can get BitEnumeration Item with Invalid ItemID - (ItemID - "abc")
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["InvalidItemID"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["InvalidItemID"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

ATC0015 - Verify user can get BitEnumeration Item with Incorrect WAO - (ItemHasRelation - true)
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["IncorrectWAO"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["IncorrectWAO"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}

#DP-1308
#ATC0015 - Verify user can set BitEnumeration Item with Unsigned Int Value - (VarValue": 100)
#    [Tags]           Critical       BitEnum Var             BitEnum_Item
#    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
#    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
#                                    register callback       GetItem
#    ${ActualStatus}                 get item                ${BitEnumItemJsonObj["BitEnum_UintVarVal"]["Request"]}
#                                    unregister callback     GetItem
#    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_UintVarVal"]["Response"]}
#    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true                  True==${VarCompStatus}

ATC0016 - Verify user can set BitEnumeration Item with Negative Int Value - (VarValue": -1)
    [Tags]           Critical       BitEnum Var             BitEnum_Item
    ${JsonFileContent}              get file                ${BitEnumItem}/FF_BitEnum_Negative_Cases_Rsc.json
    ${BitEnumItemJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       SetItem
    ${ActualStatus}                 set item                ${BitEnumItemJsonObj["BitEnum_NegVarVal"]["Request"]}
                                    unregister callback     SetItem
    ${ExpectedStatus}               set variable            ${BitEnumItemJsonObj["BitEnum_NegVarVal"]["Response"]}
    ${VarCompStatus}                compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${VarCompStatus}
