*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup     run keywords        SetLangCodeEnKW
...             AND                 SetProtocolTypeKW
...             AND                 InitializeStackKW

Suite Teardown  run keywords        ShutdownStackKW


*** Variables ***
${ConnectResource}     ${HTKResource}/connectDevice

*** Test Cases ***

ATC0091 - Verify connect-diconnect with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Connect_Disconnect
    :FOR    ${i}    IN RANGE    1
    \    Log    ${i}
    \    ${JsonFileContent}          get file                ${ConnectResource}/FF_Connectdevice_Rsc.json
    \    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    \    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    \    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    \    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    \    should be true              True==${SetConnectCompStatus}
    \    sleep                       3
    \    ${JsonFileContent}          get file                ${ConnectResource}/FF_DisConnectdevice_Rsc.json
    \    ${SetDisconnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    \    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["FF"]["Request"]}
    \    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj["FF"]['Response']}
    \    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    \    should be true              True==${SetDisconnectCompStatus}
    \    Exit For Loop If    ${i} == 1

ATC0092 - connect with device- HTK
    [Tags]           FF      HOST       Critical        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Connectdevice_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    sleep                       5s
    should be true              True==${SetConnectCompStatus}

ATC0093 - Verify send scan after device is connected. - Error
    [Tags]           FF      HOST       Critical        HTK       Scanning      Error_case
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Scan_after_connect_error_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["StartScan"]["Response"]}
    ${ScanStatus}               compare json object     ${StartStatus}     ${ExpectedStatus}
    should be true              True==${ScanStatus}

ACT0094 - Verify to set protocol after connect. -Error
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect       Error_Case
    ${JsonFileContent}          get file                ${ConnectResource}/FF_SetProtocolType_after_connect_error_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ACT0095 - Verify send shutdown stack, while device is connected. - Error
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect       Error_Case
    ${JsonFileContent}          get file                ${ConnectResource}/FF_ShutDownProtocol_after_connect_error_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC00096 - Verify re-send connect request to HTK -Error
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Connectdevice_Resend_ERROR_Rsc.json
    ${SetConnectErrorJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectErrorJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectErrorJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

ATC0097 - Verify send connect to 0644 while connected with HTK -ERROR
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Connectdevice_0644_ERROR_Rsc.json
    ${Setconnect644JsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${Setconnect644JsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${Setconnect644JsonObj["FF"]['Response']}
    ${SetConnect644CompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnect644CompStatus}

ATC0098 - Verify Disconnect with field device HTK
    [Tags]           FF      HOST      Critical        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_DisConnectdevice_Rsc.json
    ${SetDisconnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj["FF"]['Response']}
    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectCompStatus}

ATC0099 - Verify connect with wrong device address - Error
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Connectdevice_WrngAddress_Rsc.json
    ${SetconnectwrongJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetconnectwrongJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetconnectwrongJsonObj["FF"]['Response']}
    ${SetconnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetconnectCompStatus}

ATC00100 - Verify Disconnect with invnalid device - Error.
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_DisonnectFail_afterDisonnect_Rsc.json
    ${SetDisConnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisConnectJsonObj["FF"]['Response']}
    ${SetDisconnectStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectStatus}

ATC00101 - Verify user can diconnect again after Disonnect -Error
    [Tags]           FF      HOST      Common        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${ConnectResource}/FF_Disconnectdevice_Resend_ERROR_Rsc.json
    ${SetDisConnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect                 ${SetDisConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisConnectJsonObj["FF"]['Response']}
    ${SetDisconnectStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectStatus}

ATC00102 - Verify user can shutdown the stack
    [Tags]           FF      HOST       Critical       ShutDown_Stack
    ${JsonFileContent}          get file                ${ConnectResource}/FF_ShutDownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC00103 - Verify user can initialize the stack after Shut down without set protocol -Error
    [Tags]           FF      HOST       Critical       ShutDown_Stack
    ${JsonFileContent}          get file                ${ConnectResource}/FF-InitStack_afterShutDown_withoutSetprotocl_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
   should be true              True==${InitializeCompStatus}

ATC00104 - Verify user can set required protocol type
    [Tags]           FF      HOST       Critical       Set_Protocol
    ${JsonFileContent}          get file                ${ConnectResource}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC00105 - Verify user can initialize the stack
    [Tags]           FF      HOST       Critical       Initialize_Stack
    ${JsonFileContent}          get file                ${ConnectResource}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC00106 - Verify user can shutdown the stack
    [Tags]           FF      HOST       Critical       ShutDown_Stack
    ${JsonFileContent}          get file                ${ConnectResource}/FF_ShutDownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}
