*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"
Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"


*** Variables ***
${SleepDuration}   0ms
${VariableResource}     ${HTKResource}/variable

*** Test Cases ***
###################################################################################################
###################################### HTK Test cases #############################################
#Refer : FF_HTK_VAR_DisplayFormat_Rsc.json
#Refer : FF_HTK_VAR_EditFormat_Rsc.json
#Refer : FF_HTK_VAR_MinVal_Rsc.json
#Refer : FF_HTK_VAR_MaxVal_Rsc.json
#Refer : FF_HTK_VAR_ConstUnit_Rsc.json
#Refer : FF_HTK_VAR_Handling_Rsc.json
#Refer : FF_HTK_VAR_Enum_Rsc.json
#Refer : FF_HTK_VAR_BitEnum_Rsc.json
###################################################################################################
ATC001 - Verify for 5.2f Display Format for float variable -HTK
    [Tags]           Critical    Variable        HTK       Display_Format
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_DisplayFormat_Rsc.json
                                register callback       SetItem
    ${GetDispFormatJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetValStatus}             set item                ${GetDispFormatJsonObj["DisplayFormat_5.2f"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetVarStatus}     set variable            ${GetDispFormatJsonObj["DisplayFormat_5.2f"]["Input"]["Response"]}
    ${SetDispFormatStatus}      compare json object     ${SetValStatus}     ${ExpectedSetVarStatus}
    should be true              True==${SetDispFormatStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetDispFormatJsonObj["DisplayFormat_5.2f"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetDispFormatJsonObj["DisplayFormat_5.2f"]["Evaluate"]["Response"]}
    ${GetDispFormatStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetDispFormatStatus}

ATC002 - Verify for 5.4f Display Format for float variable -HTK
    [Tags]           Critical    Variable        HTK       Display_Format
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_DisplayFormat_Rsc.json
                                register callback       SetItem
    ${GetDispFormatJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetValStatus}             set item                ${GetDispFormatJsonObj["DisplayFormat_5.4f"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetVarStatus}     set variable            ${GetDispFormatJsonObj["DisplayFormat_5.4f"]["Input"]["Response"]}
    ${SetDispFormatStatus}      compare json object     ${SetValStatus}     ${ExpectedSetVarStatus}
    should be true              True==${SetDispFormatStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetDispFormatJsonObj["DisplayFormat_5.4f"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetDispFormatJsonObj["DisplayFormat_5.4f"]["Evaluate"]["Response"]}
    ${GetDispFormatStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetDispFormatStatus}

ATC003 - Verify for 5.2f Edit Format for float variable -HTK
    [Tags]           Critical    Variable        HTK       Edit_Format
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_EditFormat_Rsc.json
                                register callback       SetItem
    ${GetEditFormatJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEditFormatJsonObj["EditFormat_5.2f"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEditFormatJsonObj["EditFormat_5.2f"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEditFormatJsonObj["EditFormat_5.2f"]["Evaluate"]["Response"]}
    ${GetEditFormatStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEditFormatStatus}

ATC004 - Verify for 5.4f Edit Format for float variable -HTK
    [Tags]           Critical    Variable        HTK       Edit_Format
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_EditFormat_Rsc.json
                                register callback       SetItem
    ${GetEditFormatJsonObj}     evaluate                   json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEditFormatJsonObj["EditFormat_5.4f"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEditFormatJsonObj["EditFormat_5.4f"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEditFormatJsonObj["EditFormat_5.4f"]["Evaluate"]["Response"]}
    ${GetEditFormatStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEditFormatStatus}

ATC005 - Verify for Min Value 1 for float variable -HTK
    [Tags]           Critical    Variable        HTK       Min_Value
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_MinVal_Rsc.json
                                register callback       SetItem
    ${GetMinValJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetMinValJsonObj["MinVal1"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetMinValJsonObj["MinVal1"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetMinValJsonObj["MinVal1"]["Evaluate"]["Response"]}
    ${GetMinValStatus}          compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetMinValStatus}

ATC006 - Verify for Min Value 2 for float variable -HTK
    [Tags]           Critical    Variable        HTK       Min_Value
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_MinVal_Rsc.json
                                register callback       SetItem
    ${GetMinValJsonObj}         evaluate                    json.loads('''${JsonFileContent}''')   json
                                set item                ${GetMinValJsonObj["MinVal2"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetMinValJsonObj["MinVal2"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetMinValJsonObj["MinVal2"]["Evaluate"]["Response"]}
    ${GetMinValStatus}          compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetMinValStatus}

ATC007 - Verify for Max Value 1 for float variable -HTK
    [Tags]           Critical    Variable        HTK       Max_Value
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_MaxVal_Rsc.json
                                register callback       SetItem
    ${GetMaxValJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetMaxValJsonObj["MaxVal1"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetMaxValJsonObj["MaxVal1"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetMaxValJsonObj["MaxVal1"]["Evaluate"]["Response"]}
    ${GetMaxValStatus}          compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetMaxValStatus}

ATC008 - Verify for Max Value 2 for float variable -HTK
    [Tags]           Critical    Variable        HTK       Max_Value
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_MaxVal_Rsc.json
                                register callback       SetItem
    ${GetMaxValJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetMaxValJsonObj["MaxVal2"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetMaxValJsonObj["MaxVal2"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetMaxValJsonObj["MaxVal2"]["Evaluate"]["Response"]}
    ${GetMaxValStatus}          compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetMaxValStatus}

ATC009 - Verify for const Unit milli second for float variable -HTK
    [Tags]           Critical    Variable        HTK       Const_Unit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_ConstUnit_Rsc.json
                                register callback       SetItem
    ${GetConstUnitJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetConstUnitJsonObj["ConstUnit_msec"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetConstUnitJsonObj["ConstUnit_msec"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetConstUnitJsonObj["ConstUnit_msec"]["Evaluate"]["Response"]}
    ${GetConstUnitStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetConstUnitStatus}

ATC010 - Verify for const Unit second float variable -HTK
    [Tags]           Critical    Variable        HTK       Const_Unit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_ConstUnit_Rsc.json
                                register callback       SetItem
    ${GetConstUnitJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetConstUnitJsonObj["ConstUnit_sec"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetConstUnitJsonObj["ConstUnit_sec"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetConstUnitJsonObj["ConstUnit_sec"]["Evaluate"]["Response"]}
    ${GetConstUnitStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetConstUnitStatus}

ATC011 - Verify for ReadOnly handling for float variable -HTK
    [Tags]           Critical    Variable        HTK       Handling
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Handling_Rsc.json
                                register callback       SetItem
    ${GetHandlingJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetHandlingJsonObj["ReadOnly"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetHandlingJsonObj["ReadOnly"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetHandlingJsonObj["ReadOnly"]["Evaluate"]["Response"]}
    ${GetHandlingStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetHandlingStatus}

ATC012 - Verify for ReadWrite Handling float variable -HTK
    [Tags]           Critical    Variable        HTK       Handling
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Handling_Rsc.json
                                register callback       SetItem
    ${GetHandlingJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetHandlingJsonObj["ReadWrite"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetHandlingJsonObj["ReadWrite"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetHandlingJsonObj["ReadWrite"]["Evaluate"]["Response"]}
    ${GetHandlingStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetHandlingStatus}

ATC013 - Verify for Enumeration 1 variable -HTK
    [Tags]           Critical    Variable        HTK       Enum
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Enum_Rsc.json
                                register callback       SetItem
    ${GetEnum1JsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnum1JsonObj["Enum1"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnum1JsonObj["Enum1"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnum1JsonObj["Enum1"]["Evaluate"]["Response"]}
    ${GetEnum1Status}          compare json object      ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnum1Status}

ATC014 - Verify for Enumeration 2 variable -HTK
    [Tags]           Critical    Variable        HTK       Enum
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Enum_Rsc.json
                                register callback       SetItem
    ${GetEnum2JsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnum2JsonObj["Enum2"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnum2JsonObj["Enum2"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnum2JsonObj["Enum2"]["Evaluate"]["Response"]}
    ${GetEnum2Status}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnum2Status}

ATC015 - Verify for BitEnumeration 1 variable -HTK
    [Tags]           Critical    Variable        HTK       BitEnum
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_BitEnum_Rsc.json
                                register callback       SetItem
    ${GetBitEnum1JsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetBitEnum1JsonObj["BitEnum1"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetBitEnum1JsonObj["BitEnum1"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetBitEnum1JsonObj["BitEnum1"]["Evaluate"]["Response"]}
    ${GetBitEnum1Status}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnum1Status}

ATC016 - Verify for BitEnumeration 2 variable -HTK
    [Tags]           Critical    Variable        HTK       BitEnum
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_BitEnum_Rsc.json
                                register callback       SetItem
    ${GetBitEnum2JsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetBitEnum2JsonObj["BitEnum2"]["Input"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetBitEnum2JsonObj["BitEnum2"]["Evaluate"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetBitEnum2JsonObj["BitEnum2"]["Evaluate"]["Response"]}
    ${GetBitEnum2Status}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnum2Status}

###################################################################################################
############################ Variable Get and Set Test cases ######################################
# Refer : FF_HTK_VAR_GetSetVariable_Rsc.json
###################################################################################################
ATC017 - Verify Set and Get Variable for Bool value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal      Bool_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetBoolMinJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetBoolMinJsonObj["Bool_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetBoolMinJsonObj["Bool_Min"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetBoolMinJsonObj["Bool_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetBoolMinJsonObj["Bool_Min"]["GetVar"]["Response"]}
    ${GetBoolMinStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBoolMinStatus}

ATC018 - Verify Set and Get Variable for Bool value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Bool_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetBoolMaxJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetBoolMaxJsonObj["Bool_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetBoolMaxJsonObj["Bool_Max"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetBoolMaxJsonObj["Bool_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetBoolMaxJsonObj["Bool_Max"]["GetVar"]["Response"]}
    ${GetBoolMaxStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBoolMaxStatus}

ATC019 - Verify Set and Get Variable for Int8 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer8_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt8MinJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt8MinJsonObj["Int8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt8MinJsonObj["Int8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt8MinJsonObj["Int8_Min"]["GetVar"]["Response"]}
    ${GetInt8MinStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt8MinStatus}

ATC020 - Verify Set and Get Variable for Int8 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer8_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt8MaxJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt8MaxJsonObj["Int8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt8MaxJsonObj["Int8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt8MaxJsonObj["Int8_Max"]["GetVar"]["Response"]}
    ${GetInt8Maxtatus}         compare json object      ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt8Maxtatus}

ATC021 - Verify Set and Get Variable for Int16 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer16 _Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt16MinJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt16 MinJsonObj["Int16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt16 MinJsonObj["Int16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt16 MinJsonObj["Int16_Min"]["GetVar"]["Response"]}
    ${GetInt16 MinStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt16 MinStatus}

ATC022 - Verify Set and Get Variable for Int16 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer16 _Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt16MaxJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt16MaxJsonObj["Int16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt16MaxJsonObj["Int16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt16MaxJsonObj["Int16_Max"]["GetVar"]["Response"]}
    ${GetInt16MaxStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt16MaxStatus}

ATC023 - Verify Set and Get Variable for Int32 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer32 _Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt32MinJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt32MinJsonObj["Int32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt32MinJsonObj["Int32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt32MinJsonObj["Int32_Min"]["GetVar"]["Response"]}
    ${GetInt16 MinStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt16 MinStatus}

ATC024 - Verify Set and Get Variable for Int32 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer32 _Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetInt32MaxJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetInt32MaxJsonObj["Int32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetInt32MaxJsonObj["Int32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetInt32MaxJsonObj["Int32_Max"]["GetVar"]["Response"]}
    ${GetInt32MaxStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetInt32MaxStatus}


ATC025 - Verify Set and Get Variable for UInt8 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unsigned_Integer8_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt8MinJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt8MinJsonObj["UInt8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt8MinJsonObj["UInt8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt8MinJsonObj["UInt8_Min"]["GetVar"]["Response"]}
    ${GetUInt8MinStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt8MinStatus}

ATC026 - Verify Set and Get Variable for UInt8 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unisigned_Integer8_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt8MaxJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt8MaxJsonObj["UInt8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt8MaxJsonObj["UInt8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt8MaxJsonObj["UInt8_Max"]["GetVar"]["Response"]}
    ${GetUInt8MaxStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt8MaxStatus}

ATC027 - Verify Set and Get Variable for UInt16 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unsigned_Integer16_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt16MinJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt16MinJsonObj["UInt16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt16MinJsonObj["UInt16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt16MinJsonObj["UInt16_Min"]["GetVar"]["Response"]}
    ${GetUInt16MinStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt16MinStatus}

ATC028 - Verify Set and Get Variable for UInt16 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unisigned_Integer16_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt16MaxJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt16MaxJsonObj["UInt16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt16MaxJsonObj["UInt16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt16MaxJsonObj["UInt16_Max"]["GetVar"]["Response"]}
    ${GetUInt16MaxStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt16MaxStatus}

ATC029 - Verify Set and Get Variable for UInt32 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unsigned_Integer32_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt32MinJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt32MinJsonObj["UInt32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt32MinJsonObj["UInt32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt32MinJsonObj["UInt32_Min"]["GetVar"]["Response"]}
    ${GetUInt32MinStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt32MinStatus}

ATC030 - Verify Set and Get Variable for UInt32 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Unisigned_Integer32_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetUInt32MaxJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetUInt32MaxJsonObj["UInt32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetUInt32MaxJsonObj["UInt32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUInt32MaxJsonObj["UInt32_Max"]["GetVar"]["Response"]}
    ${GetUInt32MaxStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUInt32MaxStatus}

ATC031 - Verify Set and Get Variable for EnumUInt8 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer8_Enumration_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt8MinJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt8MinJsonObj["EnumUInt8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt8MinJsonObj["EnumUInt8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt8MinJsonObj["EnumUInt8_Min"]["GetVar"]["Response"]}
    ${GetEnumUInt8MinStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt8MinStatus}

ATC032 - Verify Set and Get Variable for EnumUInt8 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer8_Enumration_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt8MaxJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt8MaxJsonObj["EnumUInt8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt8MaxJsonObj["EnumUInt8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt8MaxJsonObj["EnumUInt8_Max"]["GetVar"]["Response"]}
    ${GetEnumUInt8MaxStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt8MaxStatus}

ATC033 - Verify Set and Get Variable for EnumUInt16 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer16_Enumration_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt16MinJsonObj}  evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt16MinJsonObj["EnumUInt16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt16MinJsonObj["EnumUInt16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt16MinJsonObj["EnumUInt16_Min"]["GetVar"]["Response"]}
    ${GetEnumUInt16MinStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt16MinStatus}

ATC034 - Verify Set and Get Variable for EnumUInt16 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer16_Enumration_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt16MaxJsonObj}  evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt16MaxJsonObj["EnumUInt16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt16MaxJsonObj["EnumUInt16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt16MaxJsonObj["EnumUInt16_Max"]["GetVar"]["Response"]}
    ${GetEnumUInt16MaxStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt16MaxStatus}

ATC035 - Verify Set and Get Variable for EnumUInt32 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer32_Enumration_Min
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt32MinJsonObj}  evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt32MinJsonObj["EnumUInt32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt32MinJsonObj["EnumUInt32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt32MinJsonObj["EnumUInt32_Min"]["GetVar"]["Response"]}
    ${GetEnumUInt32MinStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt32MinStatus}

ATC036 - Verify Set and Get Variable for EnumUInt32 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Integer32_Enumration_Max
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumUInt32MaxJsonObj}  evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumUInt32MaxJsonObj["EnumUInt32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumUInt32MaxJsonObj["EnumUInt32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumUInt32MaxJsonObj["EnumUInt32_Max"]["GetVar"]["Response"]}
    ${GetEnumUInt32MaxStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetEnumUInt32MaxStatus}

ATC037 - Verify Set and Get Variable for Float value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Float
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                register callback       SetItem
    ${GetEnumFloatJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetEnumFloatJsonObj["Float"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetEnumFloatJsonObj["Float"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetEnumFloatJsonObj["Float"]["GetVar"]["Response"]}
    ${GetFloatStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFloatStatus}

ATC038 - Verify Set and Get Variable for BitEnumUInt8 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt_BitEnumration_Min
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt8MinJsonObj}    evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt8MinJsonObj["BitEnumUInt8_Min"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt8MinJsonObj["BitEnumUInt8_Min"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt8MinJsonObj["BitEnumUInt8_Min"]["GetVar"]["Response"]}
    ${GetBitEnumUInt8MinStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt8MinStatus}

ATC039 - Verify Set and Get Variable for BitEnumUInt8 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt8_BitEnumration_Max
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt8MaxJsonObj}    evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt8MaxJsonObj["BitEnumUInt8_Max"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt8MaxJsonObj["BitEnumUInt8_Max"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt8MaxJsonObj["BitEnumUInt8_Max"]["GetVar"]["Response"]}
    ${GetBitEnumUInt8Maxtatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt8Maxtatus}

ATC040 - Verify Set and Get Variable for BitEnumUInt16 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt16_BitEnumration_Min
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt16MinJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt16MinJsonObj["BitEnumUInt16_Min"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt16MinJsonObj["BitEnumUInt16_Min"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt16MinJsonObj["BitEnumUInt16_Min"]["GetVar"]["Response"]}
    ${GetBitEnumUInt16MinStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt16MinStatus}

ATC041 - Verify Set and Get Variable for BitEnumUInt16 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt16_BitEnumration_Max
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt16MaxJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt16MaxJsonObj["BitEnumUInt16_Max"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt16MaxJsonObj["BitEnumUInt16_Max"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt16MaxJsonObj["BitEnumUInt16_Max"]["GetVar"]["Response"]}
    ${GetBitEnumUInt16Maxtatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt16Maxtatus}


ATC042 - Verify Set and Get Variable for BitEnumUInt32 value Min -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt32_BitEnumration_Min
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt32MinJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt32MinJsonObj["BitEnumUInt32_Min"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt32MinJsonObj["BitEnumUInt32_Min"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt32MinJsonObj["BitEnumUInt32_Min"]["GetVar"]["Response"]}
    ${GetBitEnumUInt32MinStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt32MinStatus}

ATC043 - Verify Set and Get Variable for BitEnumUInt32 value Max -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       UInt32_BitEnumration_Max
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitEnumUInt32MaxJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitEnumUInt32MaxJsonObj["BitEnumUInt32_Max"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitEnumUInt32MaxJsonObj["BitEnumUInt32_Max"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitEnumUInt32MaxJsonObj["BitEnumUInt32_Max"]["GetVar"]["Response"]}
    ${GetBitEnumUInt32MaxStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitEnumUInt32MaxStatus}

ATC044 - Verify Set and Get Variable for Visible String value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Visible String
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetVisibleStrJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetVisibleStrJsonObj["VisibleStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetVisibleStrJsonObj["VisibleStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetVisibleStrJsonObj["VisibleStr"]["GetVar"]["Response"]}
    ${GetVisibleStrJsonObjStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVisibleStrJsonObjStatus}

ATC045 - Verify Set and Get Variable for Octet String value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Octet String
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetOctetStrJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetOctetStrJsonObj["OctetStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetOctetStrJsonObj["OctetStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetOctetStrJsonObj["OctetStr"]["GetVar"]["Response"]}
    ${GetOctetStrJsonObjStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetOctetStrJsonObjStatus}

ATC046 - Verify Set and Get Variable for Password String value -HTK
    [Tags]           Critical   Variable        GetSetMinMaxVal       Password String
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetPasswordStrJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetPasswordStrJsonObj["PasswordStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetPasswordStrJsonObj["PasswordStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetPasswordStrJsonObj["PasswordStr"]["GetVar"]["Response"]}
    ${GetPasswordStrJsonObjStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetPasswordStrJsonObjStatus}

ATC047 - Verify Set and Get Variable for BitStr8 value -HTK
    [Tags]           Critical   Variable        GetSetMinMaxVal       BitString 8
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitStr8StrJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitStr8StrJsonObj["BitStr8Str"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitStr8StrJsonObj["BitStr8Str"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitStr8StrJsonObj["BitStr8Str"]["GetVar"]["Response"]}
    ${GetBitStr8StrJsonObjStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitStr8StrJsonObjStatus}

ATC048 - Verify Set and Get Variable for BitStr16 value -HTK
    [Tags]           Critical   Variable        GetSetMinMaxVal       BitString 16
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitStr16StrJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitStr16StrJsonObj["BitStr16Str"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitStr16StrJsonObj["BitStr16Str"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitStr16StrJsonObj["BitStr16Str"]["GetVar"]["Response"]}
    ${GetBitStr16StrJsonObjStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitStr16StrJsonObjStatus}

ATC049 - Verify Set and Get Variable for BitStr32 value -HTK
    [Tags]           Critical   Variable        GetSetMinMaxVal       BitString 32
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetBitStr32StrJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetBitStr32StrJsonObj["BitStr32Str"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetBitStr32StrJsonObj["BitStr32Str"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetBitStr32StrJsonObj["BitStr32Str"]["GetVar"]["Response"]}
    ${GetBitStr32StrJsonObjStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBitStr32StrJsonObjStatus}


ATC050 - Verify Set and Get Variable for Date_Time value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Date_Time
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetDateTimeJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetDateTimeJsonObj["DateTime"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetDateTimeJsonObj["DateTime"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetDateTimeJsonObj["DateTime"]["GetVar"]["Response"]}
    ${GetDateTimeJsonObjStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetDateTimeJsonObjStatus}

ATC051 - Verify Set and Get Variable for Time value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Time
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetTimeJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetTimeJsonObj["Time"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetTimeJsonObj["Time"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetTimeJsonObj["Time"]["GetVar"]["Response"]}
    ${GetTimeJsonObjStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTimeJsonObjStatus}

ATC052 - Verify Set and Get Variable for Duration value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       Duration
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetDurationJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetDurationJsonObj["Duration"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetDurationJsonObj["Duration"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetDurationJsonObj["Duration"]["GetVar"]["Response"]}
    ${GetDurationJsonObjStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetDurationJsonObjStatus}

ATC053 - Verify Set and Get Variable for TimeVal value -HTK
    [Tags]           Critical    Variable        GetSetMinMaxVal       TimeVal
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Rsc.json
                                    register callback       SetItem
    ${GetTimeValJsonObj}            evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetTimeValJsonObj["TimeVal"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetTimeValJsonObj["TimeVal"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetTimeValJsonObj["TimeVal"]["GetVar"]["Response"]}
    ${GetTimeValJsonObjStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTimeValJsonObjStatus}

###################################################################################################
########################## Variable Values Below and Beyond Boundary  #############################
# Refer FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
###################################################################################################

ATC100 - Verify Set and Get Variable for Bool value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Bool_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Bool_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Bool_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Bool_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Bool_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Bool_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC101 - Verify Set and Get Variable for Bool value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Bool_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Bool_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Bool_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Bool_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Bool_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Bool_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC102 - Verify Set and Get Variable for Int8 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Integer8_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int8_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int8_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int8_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})


ATC103 - Verify Set and Get Variable for Int8 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Integer8_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int8_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int8_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int8_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC104 - Verify Set and Get Variable for Int16 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Integer16_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int16_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int16_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int16_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC105 - Verify Set and Get Variable for Int16 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Integer16_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int16_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int16_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int16_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC106 - Verify Set and Get Variable for Int32 value Min Limit-HTK
    [Tags]           Critical    Variable        LimitViolation       Integer32_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int32_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int32_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int32_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC107 - Verify Set and Get Variable for Int32 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Integer32_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Int32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Int32_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Int32_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Int32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int32_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC108 - Verify Set and Get Variable for UInt8 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer8_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt8_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt8_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt8_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC109 - Verify Set and Get Variable for UInt8 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer8_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt8_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt8_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt8_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC110 - Verify Set and Get Variable for UInt16 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer16_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt16_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt16_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt16_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC111 - Verify Set and Get Variable for UInt16 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer16_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt16_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt16_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt16_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC112 - Verify Set and Get Variable for UInt32 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer32_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt32_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt32_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt32_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC113 - Verify Set and Get Variable for UInt32 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Unsigned Integer32_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["UInt32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["UInt32_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["UInt32_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["UInt32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt32_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC114 - Verify Set and Get Variable for EnumUInt8 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt8_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt8_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt8_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt8_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC115 - Verify Set and Get Variable for EnumUInt8 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt8__Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt8_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt8_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt8_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC116 - Verify Set and Get Variable for EnumUInt16 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt16__Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt16_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt16_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt16_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC117 - Verify Set and Get Variable for EnumUInt16 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt16__Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt16_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt16_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt16_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC118 - Verify Set and Get Variable for EnumUInt32 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt32__Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt32_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt32_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt32_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC119 - Verify Set and Get Variable for EnumUInt32 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       EnumUInt32__Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["EnumUInt32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["EnumUInt32_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["EnumUInt32_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["EnumUInt32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt32_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC120 - Verify Set and Get Variable for BitEnumUInt8 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt8_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt8_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt8_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt8_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt8_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt8_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC121 - Verify Set and Get Variable for BitEnumUInt8 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt8_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt8_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt8_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt8_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt8_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt8_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC122 - Verify Set and Get Variable for BitEnumUInt16 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt16_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt16_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt16_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt16_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt16_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt16_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC123 - Verify Set and Get Variable for BitEnumUInt16 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt16_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt16_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt16_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt16_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt16_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt16_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC124 - Verify Set and Get Variable for BitEnumUInt32 value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt32_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt32_Min"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt32_Min"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt32_Min"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt32_Min"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt32_Min"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC125 - Verify Set and Get Variable for BitEnumUInt32 value Max Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       BitEnumUInt32_Max_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["BitEnumUInt32_Max"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["BitEnumUInt32_Max"]["SetHighLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["BitEnumUInt32_Max"]["SetHighLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["BitEnumUInt32_Max"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt32_Max"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC126 - Verify Set and Get Variable for Date value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       DateTime_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["DateTime"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["DateTime"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["DateTime"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["DateTime"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["DateTime"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC127 - Verify Set and Get Variable for Time value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Time_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Time"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Time"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Time"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Time"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Time"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC128 - Verify Set and Get Variable for Duration value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       Duration_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["Duration"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["Duration"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["Duration"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["Duration"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Duration"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

ATC129 - Verify Set and Get Variable for TimeVal value Min Limit -HTK
    [Tags]           Critical    Variable        LimitViolation       TimeVal_Min_Limit
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_GetSetVariable_Limits_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
                                set item                ${GetJsonObj["TimeVal"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
                                register callback       SetItem
    ${ErrorStatus}              set item                ${GetJsonObj["TimeVal"]["SetLowLim"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedErrorStatus}      set variable            ${GetJsonObj["TimeVal"]["SetLowLim"]["Response"]}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetJsonObj["TimeVal"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["TimeVal"]["GetVar"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==(${ExpectedErrorStatus} and ${GetTestStatus})

###################################################################################################
########################## Variable with invalid Input Values   ###################################
# Refer FF_HTK_VAR_SetVariable_InvalidValue_Rsc.json
###################################################################################################

ATC150 - Verify Set Variable string value for Bool -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Bool_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Bool"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Bool"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC085 - Verify Set Variable string value for Int8 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Int8_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Int8"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int8"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC151 - Verify Set Variable string value for Int16 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Int16_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Int16"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int16"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC152 - Verify Set Variable string value for Int32 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Int32_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Int32"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Int32"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC153 - Verify Set Variable string value for UInt8 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       UInt8_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["UInt8"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt8"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC154 - Verify Set Variable string value for UInt16 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       UInt16_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["UInt16"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt16"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC155 - Verify Set Variable string value for UInt32 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       UInt32_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["UInt32"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["UInt32"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC156 - Verify Set Variable string value for EnumUInt8 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       EnumUInt8_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["EnumUInt8"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt8"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC157 - Verify Set Variable string value for EnumUInt16 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       EnumUInt16_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["EnumUInt16"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt16"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC158 - Verify Set Variable string value for EnumUInt32 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       EnumUInt32_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["EnumUInt32"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["EnumUInt32"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC159 - Verify Set Variable string value for BitEnumUInt8 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       BitEnumUInt8_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["BitEnumUInt8"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt8"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC160 - Verify Set Variable string value for BitEnumUInt16 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       BitEnumUInt16_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["BitEnumUInt16"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt16"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC161 - Verify Set Variable string value for BitEnumUInt32 -HTK
    [Tags]           Critical    Variable        InvalidValueInput       BitEnumUInt32_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["BitEnumUInt32"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["BitEnumUInt32"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC162 - Verify Set Variable string value for DateTime -HTK
    [Tags]           Critical    Variable        InvalidValueInput       DateTime_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["DateTime"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["DateTime"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC163 - Verify Set Variable string value for Time -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Time_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Time"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Time"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC164 - Verify Set Variable string value for Duration -HTK
    [Tags]           Critical    Variable        InvalidValueInput       Duration_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["Duration"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["Duration"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

ATC165 - Verify Set Variable string value for TimeVal -HTK
    [Tags]           Critical    Variable        InvalidValueInput       TimeVal_Invalid_Val
    ${JsonFileContent}          get file                ${VariableResource}/FF_HTK_VAR_Set_Invalid_Value_Rsc.json
                                register callback       SetItem
    ${GetJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${GetJsonObj["TimeVal"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetJsonObj["TimeVal"]["Response"]}
    ${GetTestStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetTestStatus}

###################################################################################################
########################## String Variables with maximum Input characters #########################
# Refer FF_HTK_VAR_String_Max_Chars_Rsc.json
###################################################################################################

ATC200 - Verify Set and Get Variable for Visible String for maximum character length -HTK
    [Tags]           Critical    Variable        MaxStringChars       Visible String_Max_Length
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_String_Max_Chars_Rsc.json
                                    register callback       SetItem
    ${GetVisibleStrJsonObj}         evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetVisibleStrJsonObj["VisibleStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetVisibleStrJsonObj["VisibleStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetVisibleStrJsonObj["VisibleStr"]["GetVar"]["Response"]}
    ${GetVisibleStrJsonObjStatus}   compare json object   ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVisibleStrJsonObjStatus}

ATC201 - Verify Set and Get Variable for Octet String for maximum character length -HTK
    [Tags]           Critical    Variable        MaxStringChars       Octet String_Max_Length
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_String_Max_Chars_Rsc.json
                                    register callback       SetItem
    ${GetOctetStrJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetOctetStrJsonObj["OctetStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetOctetStrJsonObj["OctetStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetOctetStrJsonObj["OctetStr"]["GetVar"]["Response"]}
    ${GetOctetStrJsonObjStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetOctetStrJsonObjStatus}

ATC202 - Verify Set and Get Variable for Password String for maximum character length -HTK
    [Tags]           Critical   Variable        MaxStringChars       Password String_Max_Length
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_String_Max_Chars_Rsc.json
                                    register callback       SetItem
    ${GetPasswordStrJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
                                    set item                ${GetPasswordStrJsonObj["PasswordStr"]["SetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     SetItem
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetPasswordStrJsonObj["PasswordStr"]["GetVar"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetPasswordStrJsonObj["PasswordStr"]["GetVar"]["Response"]}
    ${GetPasswordStrJsonObjStatus}  compare json object   ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetPasswordStrJsonObjStatus}


###################################################################################################
########################## Incorrect JSON Request #################################################
# Refer FF_HTK_VAR_IncorrectJson_Rsc.json
###################################################################################################
ATC250 - Verify Get Variable with Empty Container ID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Empty_Cont_Tag
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["EmptyContainerTag"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["EmptyContainerTag"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC251 - Verify Get Variable with No Container ID -HTK
        [Tags]           Critical    Variable    InvalidJsonInput       No_Cont_Tag
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["NoContainerTag"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["NoContainerTag"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC252 - Verify Get Variable with Invalid Container ID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       No_Cont_Tag
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidContainerTag"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidContainerTag"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetJsonObjStatus}

ATC253 - Verify Get Variable with Empty ItemID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Empty_ItemID
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["EmptyItemID"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["EmptyItemID"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC254 - Verify Get Variable with No ItemID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       No_ItemID
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["NoItemID"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["NoItemID"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC255 - Verify Get Variable with Invalid ItemID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Invalid_ItemID_String
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidItemID_String"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidItemID_String"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC256 - Verify Get Variable with Invalid ItemID -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Invalid_ItemID_Zero
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidItemID_Zero"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidItemID_Zero"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC257 - Verify Get Variable with Empty ItemInDD -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Empty_ItemInDD
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["EmptyItemInDD"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["EmptyItemInDD"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC258 - Verify Get Variable with No ItemInDD -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       No_ItemInDD
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["NoItemInDD"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["NoItemInDD"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC259 - Verify Get Variable with Invalid ItemInDD -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Invalid_ItemInDD
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidItemInDD"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidItemInDD"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC260 - Verify Get Variable with Empty ItemType -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Empty_ItemType
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["EmptyItemType"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["EmptyItemType"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC261 - Verify Get Variable with No ItemType -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       No_ItemType
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["NoItemType"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["NoItemType"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}

ATC262 - Verify Get Variable with Invalid ItemType -HTK
    [Tags]           Critical    Variable    InvalidJsonInput       Invalid_ItemType
    ${JsonFileContent}              get file                ${VariableResource}/FF_HTK_VAR_IncorrectJson_Rsc.json
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
                                    register callback       GetItem
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidItemType"]["Request"]}
                                    sleep                   ${SleepDuration}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidItemType"]["Response"]}
    ${GetJsonObjStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetJsonObjStatus}