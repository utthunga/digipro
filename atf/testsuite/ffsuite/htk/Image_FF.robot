*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     Collections
Library     ATFTestLib.py

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"
Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"

*** Variables ***
${ImageResource}     ${HTKResource}/image

*** Test Cases ***
ATC001 - Verify that host properly displays a jpg image in a window in HTK device.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_001_jpeg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_1.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

ATC002 - Verify that host properly displays a gif image in a window in HTK device.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_002_gif_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_2.gif
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}      ${ReceivedImagePath}  0.1

ATC003 - Verify that host properly displays a png image in a window HTK device.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_003_png_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_3.png
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}      ${ReceivedImagePath}  0.1

ATC004 - Verify that host properly displays a jpg image in a window with the Validity on HTK device. The jpg image shall link to another window.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_004_Validity_ON_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_4_validity_ON.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}      ${ReceivedImagePath}  0.1

ATC005- Verify that host properly displays a jpg image in a window with the Validity on and image shall link to a method HTK device.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_005_method_link_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_5_method_link.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

#Error case. This should not execute or display image as validaity is off.Compare Images
#DP-1065
ATC006- Verify that host does not display a jpg image when the VALIDITY is set to FALSE. - ERROR
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_006_Validaity_off_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]["Response"]}
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}

#error, not able to display large image properly
ATC007- Verify that host gracefully handles an attempt to display a large image.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_007_large_image_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_7_largeImage.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

#follow this with in JIRA. INLINE issue.
ATC008- Verify that host properly displays a page with an image that is not defined as INLINE.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_008_inline_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_8_1_Not_inline.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

#follow this with in JIRA. INLINE issue.
ATC009- Verify that host properly displays a page with an image that is not defined as INLINE.
    [Tags]           Critical        HTK       Image
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_008_not_inline_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${ImageResource}/image_8_2_INLINE.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

ATC0010 - Verify user can not access a image with wrong "ItemID" in HTK Device. - Error.
    [Tags]           Critical        HTK       Image     Error_case
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_Error_wrong_ItemID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC0011 - Verify user can not access a image with "ItemInDD" as false in HTK Device. - Error.
    [Tags]           Critical        HTK       Image     Error_case
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_Error_IteminDD_false_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC0012 - Verify user can not access a image with wrong ConatinerTag value in HTK Device. - Error.
    [Tags]           Critical        HTK       Image     Error_case
    ${JsonFileContent}          get file                ${ImageResource}/FF_Image_Error_wrong_ConatinerTag_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["IMAGE"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["IMAGE"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}