*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW

*** Variables ***
${InitStackResource}        ${HTKResource}/initStack

*** Test Cases ***
ACT0040 - Verify if user can connect before init stack.- ERROR
    [Tags]          FF      HOST      Common        Connect_Disconnect        Error_case
    ${JsonFileContent}          get file                ${InitStackResource}/FF_Connectdevice_before_initStack_error_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

ACT0041 - Verify if user can start Scan before initliazing the stack -Error
    [Tags]          FF      HOST      Common        Initialize_Stack        Error_case
    ${JsonFileContent}          get file                ${InitStackResource}/FF_Scan_before_initStack_Rsc.json
    ${SetScanObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${SetScanObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetScanObj["StartScan"]["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0042 - Verify user can initialize the stack
    [Tags]          FF      HOST      Critical        Initialize_Stack
    ${JsonFileContent}          get file                ${InitStackResource}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC0043 - Verify user can initialize the stack after initialized stack - Error
    [Tags]          FF      HOST      Common        Initialize_Stack
    ${JsonFileContent}          get file                ${InitStackResource}/FF_Re-InitializeProtocol_Rsc.json
    ${InitializeAgainJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeAgainJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeAgainJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

#sleep is introduced because shutdown stack takes time.
ATC0044 - Verify user can shutdown the stack
    [Tags]          FF      HOST      Critical        ShutDown_Stack
    ${JsonFileContent}          get file                ${InitStackResource}/FF_ShutDownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC0045 - Verify user can set required protocol type
    [Tags]          FF      HOST      Critical        Set_protocol
    ${JsonFileContent}          get file                ${InitStackResource}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC0046 - Verify user can initialize the stack
    [Tags]           FF     HOST      Critical        Initialize_Stack
    ${JsonFileContent}          get file                ${InitStackResource}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC0047 - Verify user can set protocol as "HART" after initialize stack as FF.
    [Tags]          FF     HOST      Critical        Initialize_Stack       Error_Case
    ${JsonFileContent}          get file                ${InitStackResource}/FF_SetProtocolType_as_HART_after_init_FF_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${InitializeJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["FF"]["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}


#sleep is introduced because shutdown stack takes time.
ATC0048 - Verify user can shutdown the stack
    [Tags]          FF      HOST      Critical        ShutDown_Stack
    ${JsonFileContent}          get file                ${InitStackResource}/FF_ShutDownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}