*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW

*** Variables ***
${BusParamResource}        ${HTKResource}/busParams

*** Test Cases ***
ATC0021 - Verify FF Get Bus Parameter Feature (Fetch default params)
    [Tags]          FF      HOST      Critical        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_GetBusParam_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamCompStatus}

ATC0022 - Verify FF Set Bus Parameter Feature (Send all params along with modified params slot_time as 16)
    [Tags]          FF      HOST      Critical        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_st16_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0023 - Verify FF Set Bus Parameter Feature (Send all params along with modified params fun as 30)
    [Tags]          FF      HOST     Critical        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_fun30_Rsc.json
    ${SetBusfunParamObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusfunParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusfunParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0024 - Verify FF Set Bus Parameter Feature (Send all params along with modified params nun as 10)
    [Tags]          FF      HOST      Critical        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_nun10_Rsc.json
    ${SetBusnunParamObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusnunParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusnunParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0025 - Verify FF changed Bus Parameter ( slot_time :16, nun: 10, fun:30)
    [Tags]          FF      HOST      Common        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_GetBusParam_validate_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamCompStatus}

ATC0026 - Verify FF Set Bus Parameter Feature ERROR condition (fun:240 + nun:10 = Error)
    [Tags]          FF      HOST      Common        Bus_Parameters      Error_Case
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_ErrorNunFun_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0027 - Verify FF Set Bus Parameter Feature ERROR condition as nun:300(fun:240 + nun:300 = Error)
    [Tags]          FF      HOST     Critical        Bus_Parameters     Error_Case
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_ErrorNun300_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0028 - Verify FF Set Bus Parameter Feature ERROR condition as slot_time:20 - ERROR
    [Tags]          FF      HOST      Common        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_Error_Slot_time20_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0029 - Verify FF Set Bus Parameter Feature ERROR condition as mipd:23- ERROR
    [Tags]          FF      HOST     Common        Bus_Parameters       Error_Case
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_Error_mipd23_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             Set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0030 - Verify FF Set Bus Parameter Feature ERROR condition as invalid bus param- ERROR
    [Tags]          FF      HOST      Common        Bus_Parameters     Error_Case
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_Error_invalidBusParam_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             Set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

#handle during CPM validation
#ATC0031 - Verify FF Set Bus Parameter Feature ERROR condition as invalid slot_time as "qwert"- ERROR - JSON logic error
#    [Tags]          FF     HOST      Critical        Bus_Parameters
#    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_Error_Slot_time_invalid_string_Rsc.json
#    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set bus Param           ${SetBusErrorParamObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
#    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetParamCompStatus}

ATC0032 - Verify FF Set default Bus parameters
    [Tags]          FF      HOST      Common        Bus_Parameters
    ${JsonFileContent}          get file                ${BusParamResource}/FF_SetBusParam_default_Rsc.json
    ${SetBusDefaultParamObj}    evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusDefaultParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusDefaultParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

