*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     Collections
Library     ATFTestLib.py

Resource    ../suite_resource/common.robot


*** Variables ***
${SetLangugeResource}     ${HTKResource}/setLanguage


*** Test Cases ***

#raise issuee
ATC001 - Verify user can set wrong string as language as "23". -ERROR.
    SetProtocolTypeKW
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_InvalidString_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetLanguageCompStatus}


ATC002 - Verify user can set language as "en" .
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_English_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    InitializeStackKW
    ConnectDeviceKW                                     ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"
    should be true              True==${SetLanguageCompStatus}


ATC003 - Verify language has set as English.
    [Tags]           FF      HOST       Critical        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_Verify_English_Rsc.json
    ${SetVEnglishJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${SetVEnglishJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetVEnglishJsonObj["FF"]['Response']}
                                unregister callback     GetItem
    ${SetVLanguageCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    DisconnectDeviceKW     "TRUE"
    ShutdownStackKW        "TRUE"
    should be true              True==${SetVLanguageCompStatus}

ATC004 - Verify user can set language as "it" (itlaino) .
    SetProtocolTypeKW
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_Itlanio_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetLanguageCompStatus}

ATC005 - Verify language has set as Itliano.
    InitializeStackKW
    ConnectDeviceKW                                     ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"
    [Tags]           FF      HOST       Critical        HTK       Connect_Disconnect
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_Verify_Itlanio_Rsc.json
    ${SetVEnglishJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${SetVEnglishJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetVEnglishJsonObj["FF"]['Response']}
                                unregister callback     GetItem
    ${SetVLanguageCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    DisconnectDeviceKW     "TRUE"
    ShutdownStackKW        "TRUE"
    should be true              True==${SetVLanguageCompStatus}

ATC006 - Verify user can set language Json value as "" (it's postive case, beacuse default langugae is english.)
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_English_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetLanguageCompStatus}

ATC007 - Verify user can enable shot laguage strings for English.
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetLangugeResource}/FF_Set_Language_English_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetLanguageCompStatus}