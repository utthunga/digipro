*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"

Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"

*** Variables ***
${SubscriptionResource}      ${HTKResource}/subscription


*** Test Cases ***
ATC001 - Verify for Variable Subscription Operations -HTK
    [Tags]           Critical        HTK       Subscription
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json

    # SetVariable Value
    ${Status}                   register callback       SetItem
    ${SetValStatus}             set item                ${SubJsonObj["Sub"]["SetVal"]["Request"]}
    ${Status}                   unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SubJsonObj["Sub"]["SetVal"]["Response"]}
    ${GetSetValStatus}          compare json object     ${SetValStatus}     ${ExpectedStatus}
    should be true              True==${GetSetValStatus}

    #Create Subscription
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubJsonObj["Sub"]["Create"]["Request"]}
    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}

    set to dictionary           ${SubJsonObj["Sub"]["Create"]["Response"]["CommandResponse"]["result"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubJsonObj["Sub"]["Create"]["Response"]}
    should be true              True==${CreateStatus}

    #Start Subscription
    ${StartSubResp}             process subscription    ${SubJsonObj["Sub"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubJsonObj["Sub"]["Start"]["Response"]}
    should be true              True==${StartStatus}

    #Subscription Notification
    ${NotifyTimeout}            convert to integer      20
    ${NotifyData}               retrieve item data      ProcessSubscription     ${NotifyTimeout}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubJsonObj["Sub"]["Notify"]}
    should be true              True==${NotifyStatus}

    #Stop Subscription
    ${StopSubResp}              process subscription    ${SubJsonObj["Sub"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubJsonObj["Sub"]["Stop"]["Response"]}
    should be true              True==${StopStatus}

    #Delete Subscription
    ${DeleteSubResp}            process subscription    ${SubJsonObj["Sub"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubJsonObj["Sub"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}

ATC002 - Verify for Start subscription for invalid Subscription ID -HTK
    [Tags]           Critical        HTK       StartSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${StartSubResp}             process subscription    ${SubJsonObj["Start_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${StartSubStatus}           compare json object     ${StartSubResp}     ${SubJsonObj["Start_Error"]["Response"]}
    should be true              True==${StartSubStatus}

ATC003 - Verify for Stop subscription for invalid Subscription ID -HTK
    [Tags]           Critical        HTK       StopSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${StopSubResp}              process subscription    ${SubJsonObj["Stop_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${StopSubStatus}            compare json object     ${StopSubResp}     ${SubJsonObj["Stop_Error"]["Response"]}
    should be true              True==${StopSubStatus}

ATC004 - Verify for Delete subscription for invalid Subscription ID -HTK
    [Tags]           Critical        HTK       DeleteSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${DeleteSubResp}            process subscription    ${SubJsonObj["Delete_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${DeleteSubStatus}          compare json object     ${DeleteSubResp}     ${SubJsonObj["Delete_Error"]["Response"]}
    should be true              True==${DeleteSubStatus}


ATC005 - Verify for Invalid ItemAction -HTK
    [Tags]           Critical        HTK       Invalid_ItemAction
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidItemAction"]["Response"]}
    should be true              True==${createStatus}

ATC006 - Verify for No ItemAction -HTK
    [Tags]           Critical        HTK       No_ItemAction
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoItemAction"]["Response"]}
    should be true              True==${createStatus}

ATC007 - Verify for Invalid ItemContainer Tag -HTK
    [Tags]           Critical        HTK       Invalid_ItemContainerTag
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidContTag"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidContTag"]["Response"]}
    should be true              True==${createStatus}

ATC008 - Verify for No ItemContainer Tag -HTK
    [Tags]           Critical        HTK       No_ItemContainerTag
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoItemContTag"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoItemContTag"]["Response"]}
    should be true              True==${createStatus}

#ATC009 - Verify for InvalidItemID Tag -HTK
#    [Tags]           Critical        HTK       Invalid_InvalidItemID
#    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidItemID"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidItemID"]["Response"]}
#    should be true              True==${createStatus}

ATC010 - Verify for No InvalidItemID Tag -HTK
    [Tags]           Critical        HTK       No_ItemID
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoItemID"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoItemID"]["Response"]}
    should be true              True==${createStatus}

#ATC011 - Verify for InvalidRefreshRate Tag -HTK
#    [Tags]           Critical        HTK       Invalid_InvalidRefreshRate
#    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidRefreshRate"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidRefreshRate"]["Response"]}
#    should be true              True==${createStatus}

ATC012 - Verify for No InvalidRefreshRate Tag -HTK
    [Tags]           Critical        HTK       No_InvalidRefreshRate
    ${JsonFileContent}          get file                ${SubscriptionResource}/FF_HTK_Subscription_Invalid_Json_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoRefreshRate"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoRefreshRate"]["Response"]}
    should be true              True==${createStatus}
