*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"


*** Variables ***
${MethodExectionRsc}      ${HTKResource}/methodExecution

*** Test Cases ***
ATC001 - Verify user could get proper method label and help string attribute values, when a method is triggered
     [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      9
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   44
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      1
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   10
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${Status_3R}                register callback           SetItem
     ${SetItemResponse_3}        set item                    ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      1
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object         ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${Status_4R}                register callback           SetItem
     ${SetItemResponse_4}        set item                   ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_4}          convert to integer      1
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_4}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object         ${SetItemResponse_4}    ${MEJsonObj["Response"][3]}
     ${Status_4}                 unregister callback         SetItem
     should be true              True==${SetItemStatus_4}
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   4
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_5"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_5}                 unregister callback         SetItem
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][2]}
     should be true              True==${SetItemStatus_5}
     ${Status_6R}                register callback           SetItem
     ${SetItemResponse_6}        set item                    ${MEJsonObj["Request"][5]}
     ${NotifyTimeout_6}          convert to integer      10
     ${NotifyIndex_6}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_6}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_6"][${NotifyIndex_6}]}
         \   ${NotifyIndex_6}            evaluate                ${NotifyIndex_6}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_6}                 unregister callback         SetItem
     ${SetItemStatus_6}          compare json object         ${SetItemResponse_6}    ${MEJsonObj["Response"][5]}
     should be true              True==${SetItemStatus_6}
     ${Status_7R}                register callback           SetItem
     ${SetItemResponse_7}        set item                    ${MEJsonObj["Request"][6]}
     ${NotifyTimeout_7}          convert to integer      10
     ${NotifyIndex_7}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_7}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_7"][${NotifyIndex_7}]}
         \   ${NotifyIndex_7}            evaluate                ${NotifyIndex_7}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_7}                 unregister callback         SetItem
     ${SetItemStatus_7}          compare json object         ${SetItemResponse_7}    ${MEJsonObj["Response"][6]}
     should be true              True==${SetItemStatus_7}
     ${Status_8R}                register callback           SetItem
     ${SetItemResponse_8}        set item                    ${MEJsonObj["Request"][7]}
     ${NotifyTimeout_8}          convert to integer      10
     ${NotifyIndex_8}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_8}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_8"][${NotifyIndex_8}]}
         \   ${NotifyIndex_8}            evaluate                ${NotifyIndex_8}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_8}                 unregister callback         SetItem
     ${SetItemStatus_8}          compare json object         ${SetItemResponse_8}    ${MEJsonObj["Response"][7]}
     should be true              True==${SetItemStatus_8}
     ${Status_9R}                register callback           SetItem
     ${SetItemResponse_9}        set item                    ${MEJsonObj["Request"][8]}
     ${NotifyTimeout_9}          convert to integer      10
     ${NotifyIndex_9}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_9}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_9"][${NotifyIndex_9}]}
         \   ${NotifyIndex_9}            evaluate                ${NotifyIndex_9}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_9}                 unregister callback         SetItem
     ${SetItemStatus_9}          compare json object         ${SetItemResponse_9}    ${MEJsonObj["Response"][8]}
     should be true              True==${SetItemStatus_9}
     ${Status_10R}                register callback           SetItem
     ${SetItemResponse_10}        set item                    ${MEJsonObj["Request"][9]}
     ${NotifyTimeout_10}          convert to integer      10
     ${NotifyIndex_10}            convert to integer      0
     :for    ${Index}    in range    0   16
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_10}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_10"][${NotifyIndex_10}]}
         \   ${NotifyIndex_10}           evaluate                ${NotifyIndex_10}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_10}                 unregister callback         SetItem
     ${SetItemStatus_10}          compare json object         ${SetItemResponse_9}    ${MEJsonObj["Response"][9]}
     should be true              True==${SetItemStatus_10}

#
#need to implement when validity is implemented.
#ATC002 - Verify method validity attribute is affected, when a variable related to validity attribute is modified (validity = true).
#ATC003 - Verify method validity attribute is affected, when a variable related to validity attribute is modified (validity = false).

ATC004 - Verify method execution can provide all type of notification messages CONTINIOUS & COMPLETED.
     [Tags]          FF  Method_Execution           0644
     sleep                      5
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_CONTINOUS_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemRequest}           get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemRequest}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${Status_3R}                register callback           SetItem
     ${SetItemResponse_3}        set item                    ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object         ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}

ATC005 - Verify user can abort or cancel method execution in any of the stage (stage = ACK).
     [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Cancel_After_ACK_Rsc.json
     ${MEJsonObj_1}                evaluate              json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj_1["Request"][0]}
     ${NotifyTimeout_11}          convert to integer      10
     ${NotifyIndex_11}            convert to integer      0
     :for    ${Index}    in range    0   44
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_11}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj_1["Notification_1"][${NotifyIndex_11}]}
         \   ${NotifyIndex_11}            evaluate               ${NotifyIndex_11}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj_1["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj_1["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj_1["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj_1["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}

ATC006 - Verify user can abort or cancel method execution in any of the stage (stage = GETVALUE).
     [Tags]          FF  Method_Execution      HTK
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_CANCEL_GETVALUE_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      10
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   44
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   5
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${Status_3R}                register callback           SetItem
     ${SetItemResponse_3}        set item                    ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object         ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${Status_4R}                register callback           SetItem
     ${SetItemResponse_4}        set item                   ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_4}          convert to integer      10
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_4}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object         ${SetItemResponse_4}    ${MEJsonObj["Response"][3]}
     ${Status_4}                 unregister callback         SetItem
     should be true              True==${SetItemStatus_4}
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   4
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_5"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_5}                 unregister callback         SetItem
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][2]}
     should be true              True==${SetItemStatus_5}
     ${Status_6R}                register callback           SetItem
     ${SetItemResponse_6}        set item                    ${MEJsonObj["Request"][5]}
     ${NotifyTimeout_6}          convert to integer      10
     ${NotifyIndex_6}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_6}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_6"][${NotifyIndex_6}]}
         \   ${NotifyIndex_6}            evaluate                ${NotifyIndex_6}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_6}                 unregister callback         SetItem
     ${SetItemStatus_6}          compare json object         ${SetItemResponse_6}    ${MEJsonObj["Response"][5]}
     should be true              True==${SetItemStatus_6}

ATC007 - Verify user can abort or cancel method execution in any of the stage (stage = CONTINUOUS).
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Cancel_After_CONTINUOUS_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      10
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   44
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   4
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${Status_3R}                register callback           SetItem
     ${SetItemResponse_3}        set item                    ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object         ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}

#will implement later
#ATC009 - Verify user can execute edit action during method execution phase.

#depending upon DP-1153
#ATC010 - Verify user can set variable value that are part of method execution.

#Same as test case ACT001
#ATC011 - Verify user can select a enumeration value during SELECTION notify messages.

ATC012 - Verify user can trigger two methods simultaneously, if a method is already in progress (ERROR).
     [Tags]          FF  Method_Execution      HTKl
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Two_method_Error_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      15
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   44
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_2}        set item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      15
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   5
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_2}          compare json object     ${SetItemResponse_2}    ${MEJsonObj["Response"][1]}
     ${Status_2U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_2}
     ${StatusR}                  register callback       GetItem
     ${SetItemResponse_OM}       Get item                ${MEJsonObj["Request"][2]}
     ${SetItemStatus_OM}         compare json object     ${SetItemResponse_OM}    ${MEJsonObj["Response"][2]}
     ${Status_OM}                unregister callback     GetItem
     should be true              True==${SetItemStatus_2}
     ${Status_3R}                register callback           SetItem
     ${SetItemResponse_3}        set item                    ${MEJsonObj["Request"][3]}
     ${NotifyTimeout_3}          convert to integer      15
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object         ${SetItemResponse_3}    ${MEJsonObj["Response"][3]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${Status_4R}                register callback           SetItem
     ${SetItemResponse_4}        set item                   ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_4}          convert to integer      15
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   3
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_4}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object         ${SetItemResponse_4}    ${MEJsonObj["Response"][4]}
     ${Status_4}                 unregister callback         SetItem
     should be true              True==${SetItemStatus_4}
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][5]}
     ${NotifyTimeout_5}          convert to integer      15
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   4
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_5"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_5}                 unregister callback         SetItem
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][5]}
     should be true              True==${SetItemStatus_5}
     ${Status_6R}                register callback           SetItem
     ${SetItemResponse_6}        set item                    ${MEJsonObj["Request"][6]}
     ${NotifyTimeout_6}          convert to integer      15
     ${NotifyIndex_6}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_6}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_6"][${NotifyIndex_6}]}
         \   ${NotifyIndex_6}            evaluate                ${NotifyIndex_6}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_6}                 unregister callback         SetItem
     ${SetItemStatus_6}          compare json object         ${SetItemResponse_6}    ${MEJsonObj["Response"][6]}
     should be true              True==${SetItemStatus_6}
     ${Status_7R}                register callback           SetItem
     ${SetItemResponse_7}        set item                    ${MEJsonObj["Request"][7]}
     ${NotifyTimeout_7}          convert to integer      15
     ${NotifyIndex_7}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_7}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_7"][${NotifyIndex_7}]}
         \   ${NotifyIndex_7}            evaluate                ${NotifyIndex_7}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_7}                 unregister callback         SetItem
     ${SetItemStatus_7}          compare json object         ${SetItemResponse_7}    ${MEJsonObj["Response"][7]}
     should be true              True==${SetItemStatus_7}
     ${Status_8R}                register callback           SetItem
     ${SetItemResponse_8}        set item                    ${MEJsonObj["Request"][8]}
     ${NotifyTimeout_8}          convert to integer      15
     ${NotifyIndex_8}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_8}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_8"][${NotifyIndex_8}]}
         \   ${NotifyIndex_8}            evaluate                ${NotifyIndex_8}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_8}                 unregister callback         SetItem
     ${SetItemStatus_8}          compare json object         ${SetItemResponse_8}    ${MEJsonObj["Response"][8]}
     should be true              True==${SetItemStatus_8}
     ${Status_9R}                register callback           SetItem
     ${SetItemResponse_9}        set item                    ${MEJsonObj["Request"][9]}
     ${NotifyTimeout_9}          convert to integer      15
     ${NotifyIndex_9}            convert to integer      0
     :for    ${Index}    in range    0   2
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_9}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_9"][${NotifyIndex_9}]}
         \   ${NotifyIndex_9}            evaluate                ${NotifyIndex_9}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_9}                 unregister callback         SetItem
     ${SetItemStatus_9}          compare json object         ${SetItemResponse_9}    ${MEJsonObj["Response"][9]}
     should be true              True==${SetItemStatus_9}
     ${Status_10R}                register callback           SetItem
     ${SetItemResponse_10}        set item                    ${MEJsonObj["Request"][10]}
     ${NotifyTimeout_10}          convert to integer      15
     ${NotifyIndex_10}            convert to integer      0
     :for    ${Index}    in range    0   16
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_10}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_10"][${NotifyIndex_10}]}
         \   ${NotifyIndex_10}           evaluate                ${NotifyIndex_10}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${Status_10}                 unregister callback         SetItem
     ${SetItemStatus_10}          compare json object         ${SetItemResponse_9}    ${MEJsonObj["Response"][10]}
     should be true              True==${SetItemStatus_10}

#need to check this one
#ATC013 - Verify user can execute the method with variable already subscribed (ERROR).

ATC014 - Verify user can not execute method with incorrect item id (ERROR).
     [Tags]          FF  Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_wrong_itemID_Error.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
     should be true              True==${GetItemStatus}


ATC015 - Verify user can execute method with empty item id (ERROR).
     [Tags]          FF  Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_wrong_NoItemId_Error.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
     should be true              True==${GetItemStatus}

ATC016 - Verify user can execute method with all fields as empty (ERROR).
     [Tags]          FF  Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_wrong_All_fields_empty_Error.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
     should be true              True==${GetItemStatus}

ATC017 - Verify user can execute method for ItemInDD attribute as false even though the method its present within DD binary (ERROR).
     [Tags]          FF  Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_wrong_IteminDD_False_Error.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
     should be true              True==${GetItemStatus}

#raise bug
ATC018 - Verify user can proceed with the execution with invalid request during method execution phase (ERROR).
    [Tags]          FF  Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Invalid_Request_Inbetween_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      20
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   7
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${Status_2R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      20
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      20
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${Status_4R}                register callback           SetItem
     ${SetItemResponse_4}        set item                    ${MEJsonObj["Request"][3]}
     ${NotifyTimeout_4}          convert to integer      20
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_4}          compare json object         ${SetItemResponse_4}    ${MEJsonObj["Response"][3]}
     ${Status_4U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_4}

##same as test case 1
##ATC019 - Verify user can get help message during method execution.
#
##same as test case 1
##ATC020 - Verify all built-in functions that are part of method execution.


ATC021 - Verify method execution is possible during variable subscription.
    [Tags]          FF  Method_Execution      HTK      Error_Case
    ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_HTK_Subscription_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json

    # SetVariable Value
    ${Status}                   register callback       SetItem
    ${SetValStatus}             set item                ${SubJsonObj["Sub"]["SetVal"]["Request"]}
    ${Status}                   unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SubJsonObj["Sub"]["SetVal"]["Response"]}
    ${GetSetValStatus}          compare json object     ${SetValStatus}     ${ExpectedStatus}
    should be true              True==${GetSetValStatus}

    #Create Subscription
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubJsonObj["Sub"]["Create"]["Request"]}
    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}

    set to dictionary           ${SubJsonObj["Sub"]["Create"]["Response"]["CommandResponse"]["result"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubJsonObj["Sub"]["Create"]["Response"]}
    should be true              True==${CreateStatus}

    #Start Subscription
    ${StartSubResp}             process subscription    ${SubJsonObj["Sub"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubJsonObj["Sub"]["Start"]["Response"]}
    should be true              True==${StartStatus}

    #Subscription Notification
    ${NotifyTimeout}            convert to integer      20
    ${NotifyData}               retrieve item data      ProcessSubscription     ${NotifyTimeout}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubJsonObj["Sub"]["Notify"]}
    should be true              True==${NotifyStatus}

    #Method executionll
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Invalid_Request_Inbetween_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     ${Status_4R}                register callback           SetItem
     ${SetItemResponse_4}        set item                    ${MEJsonObj["Request"][3]}
     ${NotifyTimeout_4}          convert to integer      5
     ${NotifyIndex_4}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_4}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_4}]}
         \   ${NotifyIndex_4}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
    ${SetItemStatus_4}          compare json object         ${SetItemResponse_4}    ${MEJsonObj["Response"][3]}
    ${Status_4U}                unregister callback         SetItem
    should be true              True==${SetItemStatus_4}
    #Stop Subscription
    ${StopSubResp}              process subscription    ${SubJsonObj["Sub"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubJsonObj["Sub"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    #Delete Subscription
    ${DeleteSubResp}            process subscription    ${SubJsonObj["Sub"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubJsonObj["Sub"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}


#need to handle this during validation, otherwise code crash
#ATC022 - Verify method execution is getting aborted with invalid value for the variable.

#ATC023 - Verify method can be executed, if write lock is enabled in the device.

ATC024 - Verify user can triger stack Initialize during Method execution. -Error
     [Tags]     FF      Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_initStack_Error_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      10
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   7
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${Status_2R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      20
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     #init stack call
     ${ActualStatus}             initialize              ${MEJsonObj["Request"][3]}
     ${ExpectedStatus}           set variable            ${MEJsonObj["Response"][3]}
     ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true              True==${InitializeCompStatus}
     # init stack call end
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][4]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}

ATC025 - Verify user can triger stack Shutdown during Method execution. -Error.
    [Tags]     FF      Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_shutDown_Stack_Error_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      10
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   7
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${Status_2R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      20
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      20
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     #shutdown stack call
     ${ActualStatus}             shutdown                ${MEJsonObj["Request"][3]}
     ${ExpectedStatus}           set variable            ${MEJsonObj["Response"][3]}
     ${ShutDownStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true              True==${ShutDownStatus}
     #shutdown stack call end
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      20
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][4]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}

ATC026 - Verify user can triger stack Connect during Method execution. - Error
    [Tags]     FF      Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_Connect_device_Error_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      10
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   7
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${Status_2R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      10
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      10
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     #connect device stack call
     ${ActualStatus}             connect                 ${MEJsonObj["Request"][3]}
     ${ExpectedStatus}           set variable            ${MEJsonObj["Response"][3]}
     ${ShutDownStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true              True==${ShutDownStatus}
      #connect device stack call end
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      10
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][4]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}

ATC027 - Verify user can triger Set protocol during Method execution. -Error
     [Tags]     FF      Method_Execution      HTK      Error_Case
     ${JsonFileContent}          get file                ${MethodExectionRsc}/FF_ME_SetProtocol_Error_Rsc.json
     ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
     ${Status_1R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
     ${NotifyTimeout_1}          convert to integer      15
     ${NotifyIndex_1}            convert to integer      0
     :for    ${Index}    in range    0   7
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_1}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_1"][${NotifyIndex_1}]}
         \   ${NotifyIndex_1}            evaluate                ${NotifyIndex_1}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${Status_2R}                register callback       GetItem
     ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
     ${NotifyTimeout_2}          convert to integer      15
     ${NotifyIndex_2}            convert to integer      0
     :for    ${Index}    in range    0   6
         \   ${NotificationData}         retrieve item data      GetItem    ${NotifyTimeout_2}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_2"][${NotifyIndex_2}]}
         \   ${NotifyIndex_2}            evaluate                ${NotifyIndex_2}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
     should be true              True==${GetItemStatus}
     ${StatusU}                  unregister callback     GetItem
     ${StatusR}                  register callback       SetItem
     ${SetItemResponse_3}        set item                ${MEJsonObj["Request"][2]}
     ${NotifyTimeout_3}          convert to integer      15
     ${NotifyIndex_3}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_3}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_3"][${NotifyIndex_3}]}
         \   ${NotifyIndex_3}            evaluate                ${NotifyIndex_3}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_3}          compare json object     ${SetItemResponse_3}    ${MEJsonObj["Response"][2]}
     ${Status_3U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_3}
     #init stack call
     ${ActualStatus}             set protocol type       ${MEJsonObj["Request"][3]}
     ${ExpectedStatus}           set variable            ${MEJsonObj["Response"][3]}
     ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
     should be true              True==${InitializeCompStatus}
     #init stack call end
     ${Status_5R}                register callback           SetItem
     ${SetItemResponse_5}        set item                    ${MEJsonObj["Request"][4]}
     ${NotifyTimeout_5}          convert to integer      15
     ${NotifyIndex_5}            convert to integer      0
     :for    ${Index}    in range    0   1
         \   ${NotificationData}         retrieve item data      SetItem    ${NotifyTimeout_5}
         \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification_4"][${NotifyIndex_5}]}
         \   ${NotifyIndex_5}            evaluate                ${NotifyIndex_5}+1
         \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
         \   should be true              True==${NotifyStatus}
     ${SetItemStatus_5}          compare json object         ${SetItemResponse_5}    ${MEJsonObj["Response"][4]}
     ${Status_5U}                unregister callback         SetItem
     should be true              True==${SetItemStatus_5}
