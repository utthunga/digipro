*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

*** Variables ***
${SetProtoclRsc}        ${HTKResource}/setProtocol

*** Test Cases ***
ATC001 - Verify user can set required protocol type with empty string _ERROR
    [Tags]          FF      HOST        Common      Set_protocol        Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_SetProtocolType_EmptyString_before_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC002 - Verify user can set required protocol type with invalid string "qwert" -ERROR
    [Tags]          FF      HOST      Common      Set_protocol      Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_SetProtocolType_InvalidString_before_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC003 - Verify user can set required protocol type as "FF"
    [Tags]          FF      HOST      Critical        Set_protocol
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC004 - Verify user can set language as "en".
    [Tags]          FF      HOST      Critical        Set_Language
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_Set_Language_English_Rsc.json
    ${SetLanguageJsonObj}       evaluate                json.loads('''${JsonFileContent}''',strict=False)   json
    ${ActualStatus}             set lang code           ${SetLanguageJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetLanguageJsonObj["FF"]['Response']}
    ${SetLanguageCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetLanguageCompStatus}

ATC005 - Verify if user can start Scan before set Protocol - Error
    [Tags]          FF      HOST      Common      Set_protocol      Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_StartScan_before_SetProtocol_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["StartScan"]['Response']}
    ${ScanCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ScanCompStatus}

#error- DP-1167 - https://flukept.atlassian.net/projects/DP/issues/DP-1167
ATC006 - Verify set protocol by sending request again (when protocol is already set) as "FF"- Error
    [Tags]          FF     HOST     Critical    Set_protocol        Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_SetProtocolType_Resend_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC007 - Verify set protocol by sending string as "qwert" (when protocol is already set)- Error
    [Tags]          FF      HOST      Common      Set_protocol      Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_SetProtocolType_InvalidString_after_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC008 - Verify shut down protocl before init stack. -Error
    [Tags]          FF      HOST      Common      Set_protocol      Error_Case
    ${JsonFileContent}          get file                ${SetProtoclRsc}/FF_ShutDownProtocol_before_initStack_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

