*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "TRUE"
Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"

*** Variables ***
${TestCount}        5
${MenuResource}     ${HTKResource}/menu

*** Test Cases ***
ATC001 - Verify for Advance device menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}          get file                ${MenuResource}/FF_HTK_AdvRootMenu_Rsc.json
                                register callback       GetItem
    ${GetRootMenuJonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetRootMenuJonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus1}          set variable            ${GetRootMenuJonObj["Response1"]}
    ${GetRootMenuStatus1}       compare json object     ${ActualStatus}     ${ExpectedStatus1}
    ${ExpectedStatus2}          set variable            ${GetRootMenuJonObj["Response2"]}
    ${GetRootMenuStatus2}       compare json object     ${ActualStatus}     ${ExpectedStatus2}

    should be true              True==(${GetRootMenuStatus1} or ${GetRootMenuStatus2})

ATC002 - Verify for Visual block root menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}          get file                ${MenuResource}/FF_HTK_VisualBlockMenus_Rsc.json
                                register callback       GetItem
    ${GetVisualBlockJonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetVisualBlockJonObj["VisualBlockRootMenu"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetVisualBlockJonObj["VisualBlockRootMenu"]["Response"]}
    ${GetVisualBlockStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVisualBlockStatus}

ATC003 - Verify for Visual block advance menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}          get file                ${MenuResource}/FF_HTK_VisualBlockMenus_Rsc.json
                                register callback       GetItem
    ${GetVisualBlockJonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetVisualBlockJonObj["VisualBlockAdvMenu"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetVisualBlockJonObj["VisualBlockAdvMenu"]["Response"]}
    ${GetVisualBlockStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVisualBlockStatus}

ATC004 - Verify for Visual block advance menu items -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}          get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                register callback       GetItem
    ${GetVisualBlockJonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetVisualBlockJonObj["VisualBlockAdvMenu"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetVisualBlockJonObj["VisualBlockAdvMenu"]["Response"]}
    ${GetVisualBlockStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVisualBlockStatus}

ATC005 - Verify for ParameterList menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetParameterListJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetParameterListJonObj["ParameterList"]["Request"]}
    ${ActualStatus}                 get item                ${GetParameterListJonObj["ParameterList"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetParameterListJonObj["ParameterList"]["Response"]}
    ${GetParameterListStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetParameterListStatus}

ATC006 - Verify for Views menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetViewsJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetViewsJsonObj["Views"]["Request"]}
    ${ActualStatus}                 get item                ${GetViewsJsonObj["Views"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetViewsJsonObj["Views"]["Response"]}
    ${GetViewsStatus}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetViewsStatus}

ATC007 - Verify for View 1 menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetView1JsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetView1JsonObj["View 1"]["Request"]}
    ${ActualStatus}                 get item                ${GetView1JsonObj["View 1"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetView1JsonObj["View 1"]["Response"]}
    ${GetView1Status}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetView1Status}

ATC008 - Verify for View 2 menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetView2JsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetView2JsonObj["View 2"]["Request"]}
    ${ActualStatus}                 get item                ${GetView2JsonObj["View 2"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetView2JsonObj["View 2"]["Response"]}
    ${GetView2Status}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetView2Status}

ATC009 - Verify for View 3 menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetView3JsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetView3JsonObj["View 3"]["Request"]}
    ${ActualStatus}                 get item                ${GetView3JsonObj["View 3"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetView3JsonObj["View 3"]["Response"]}
    ${GetView3Status}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetView3Status}

ATC010 - Verify for View 4 menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetView4JsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetView4JsonObj["View 4"]["Request"]}
    ${ActualStatus}                 get item                ${GetView4JsonObj["View 4"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetView4JsonObj["View 4"]["Response"]}
    ${GetView4Status}               compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetView4Status}

ATC011 - Verify for Alarms menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetAlarmsJsonObj}              evaluate               json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetAlarmsJsonObj["Alarms"]["Request"]}
    ${ActualStatus}                 get item                ${GetAlarmsJsonObj["Alarms"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetAlarmsJsonObj["Alarms"]["Response"]}
    ${GetAlarmsStatus}               compare json object    ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAlarmsStatus}

ATC012 - Verify for Configuration menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetConfigurationJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetConfigurationJonObj["Configuration"]["Request"]}
    ${ActualStatus}                 get item                ${GetConfigurationJonObj["Configuration"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetConfigurationJonObj["Configuration"]["Response"]}
    ${GetConfigurationStatus}       compare json object    ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetConfigurationStatus}

ATC013 - Verify for Status menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetStatusJonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetStatusJonObj["Status"]["Request"]}
    ${ActualStatus}                 get item                ${GetStatusJonObj["Status"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetStatusJonObj["Status"]["Response"]}
    ${GetStatusStatus}              compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetStatusStatus}

ATC014 - Verify for Methods menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_VisualBlock_Adv_Menu_Rsc.json
                                    register callback       GetItem
    ${GetMethodsJonObj}             evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetMethodsJonObj["Methods"]["Request"]}
    ${ActualStatus}                 get item                ${GetMethodsJonObj["Methods"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetMethodsJonObj["Methods"]["Response"]}
    ${GetMethodsStatus}             compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetMethodsStatus}

#ATC014 - Verify for Incorrect Item In DD custom menu -HTK
#    [Tags]           FF      HOST       Critical        HTK       Get_Item
#    ${JsonFileContent}              get file                resource/FF_HTK_Menu_Error_Rsc.json
#                                    register callback       GetItem
#    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
#    repeat keyword  ${TestCount}    get item                ${GetJsonObj["IncorrectItemInDD1"]["Request"]}
#    ${ActualStatus}                 get item                ${GetJsonObj["IncorrectItemInDD1"]["Request"]}
#                                    unregister callback
#    ${ExpectedStatus}               set variable            ${GetJsonObj["IncorrectItemInDD1"]["Response"]}
#    ${GetStatus}                    compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true                  True==${GetStatus}

ATC015 - Verify for Incorrect Item In DD custom menu -HTK
    [Tags]           Critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_Menu_Error_Rsc.json
                                    register callback       GetItem
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetJsonObj["IncorrectItemInDD2"]["Request"]}
    ${ActualStatus}                 get item                ${GetJsonObj["IncorrectItemInDD2"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["IncorrectItemInDD2"]["Response"]}
    ${GetStatus}                    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetStatus}

ATC016 - Verify for Incorrect Menu Request -HTK
    [Tags]           Non-critical        HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_Menu_Error_Rsc.json
                                    register callback       GetItem
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetJsonObj["IncorrectReq"]["Request"]}
    ${ActualStatus}                 get item                ${GetJsonObj["IncorrectReq"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["IncorrectReq"]["Response"]}
    ${GetStatus}                    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetStatus}

ATC017 - Verify for Invalid Container Tag in Menu Request -HTK
    [Tags]           Non-critical      HTK       Menu     Get_Item
    ${JsonFileContent}              get file                ${MenuResource}/FF_HTK_Menu_Error_Rsc.json
                                    register callback       GetItem
    ${GetJsonObj}                   evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword  ${TestCount}    get item                ${GetJsonObj["InvalidContTag"]["Request"]}
    ${ActualStatus}                 get item                ${GetJsonObj["InvalidContTag"]["Request"]}
                                    unregister callback     GetItem
    ${ExpectedStatus}               set variable            ${GetJsonObj["InvalidContTag"]["Response"]}
    ${GetStatus}                    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetStatus}