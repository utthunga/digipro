*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${HTKResource}/common/FF_HTK_Common_Rsc.json     "FALSE"

Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"

*** Variables ***
${RecordArrayResource}      ${HTKResource}/record_array

*** Test Cases ***
#ATC001 - Verify user can access Record "BlockMode" with field device -HTK
#    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
#    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Rsc.json
#    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
#    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${GetParamListCompStatus}

#DP-1050
ATC002 - Verify user can access Record "BlockMode" with field device HTK by passing wrong Item ID - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_wrong_ItemInDD_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}


ATC003 - Verify user can access Record "BlockMode" member "Target" with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Target_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC004 - Verify user can access Record "BlockMode" member "Actual" with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Actual_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC005 - Verify user can access Record "BlockMode" member "Permitted" with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Permitted_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}


ATC006 - Verify user can access Record "BlockMode" member "Normal" with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Normal_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC007 - Verify user can set value for Record "BlockMode" member "Permitted" with value "OOS" with field device -HTK
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_Permitted_vManRsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

#DP-1052
ATC008 - Verify user can not access Record "BlockMode" member with wrong itemID with field device HTK - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_wrong_ItemID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

#DP-1053
ATC009 - Verify user can not access Record "BlockMode" member with wrong VarContainerID with field device HTK - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_wrong_varContainerID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}


#DP-1054
ATC0010 - Verify user can not access Record "BlockMode" member with wrong containerTag with field device HTK - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record      Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_wrong_ContainerTag_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

#DP-1055
ATC0011 - Verify user can not access Record "BlockMode" member with wrong varSubIndex with field device HTK - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Record      Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_wrong_VarSubIndex_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}


ATC0012 - Verify user can set Record "BlockMode" ReadOnly member -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_BlockMode_setVar_ReadOnly_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0013 - Verify user can set Record "Test Read Wrte" member "Test Boolean" by passing value as true
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_Bool_v1_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0014 - Verify user can set Record "Test Read Wrte" member "Test Boolean" by passing value as "257" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testBool_Error_v257_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0015 - Verify user can set Record "Test Read Wrte" member "Test Integer8" by passing value as "127"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt8_v127_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0016 - Verify user can set Record "Test Read Wrte" member "Test Integer8" by passing value as "-127"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt8_v-127_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC0017 - Verify user can set Record "Test Read Wrte" member "Test Integer8" by passing value as "150" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt8_Error_v150_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0018 - Verify user can set Record "Test Read Wrte" member "Test Integer16" by passing value as "1000"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt16_v1000_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0019 - Verify user can set Record "Test Read Wrte" member "Test Integer16" by passing value as "-123456" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt16_Error_v-123456_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0020 - Verify user can set Record "Test Read Wrte" member "Test Integer16" by passing value as "1234567" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt16_Error_v123456_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0021 - Verify user can set Record "Test Read Wrte" member "Test Integer32" by passing value as "123456789"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt32_v123456789_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0022 - Verify user can set Record "Test Read Wrte" member "Test Integer32" by passing value as "12345678912345" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt32_Error_v12345678912345_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0023 - Verify user can set Record "Test Read Wrte" member "Test Integer32" by passing value as "-12345678912345" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testInt32_Error_v12345678912345_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0024 - Verify user can set Record "Test Read Wrte" member "Test UInteger8" by passing value as "255"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt8_v255_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#error - when we set value by UI, its fine because chaniging in long value but when direct JSO, its crash.JIRA need fix.
ATC0025 - Verify user can set Record "Test Read Wrte" member "Test UInteger8" by passing value as "-100" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record     Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt8_Error_v-100_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0026 - Verify user can set Record "Test Read Wrte" member "Test UInteger16" by passing value as "123456678900" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt16_Error_v12345678900_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0027 - Verify user can set Record "Test Read Wrte" member "Test UInteger16" by passing value as "v-1000" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt16_Error_v-1000_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0028 - Verify user can set Record "Test Read Wrte" member "Test UInteger16" by passing value as "1234"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt16_v1234_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#need to raise JIRA
ATC0029 - Verify user can set Record "Test Read Wrte" member "Test UInteger32" by passing value as "123456789"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testUInt32_v123456789_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0030 - Verify user can set Record "Test Read Wrte" member "Test FLOAT" by passing value as "1234.34"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testFloat_v1234.34_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

ATC0031 - Verify user can set Record "Test Read Wrte" member "Test Visible String" by passing value as "Empty String" -ERROR
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record      Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testVisibleStr_vEmptyStr_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#DP-1056
ATC0032 - Verify user can set Record "Test Read Wrte" member "Test Visible String" by passing value as "HTKdevice"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testVisibleStr_vHTKDevice_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#Error ----- DP-1059
ATC0032 - Verify user can set Record "Test Read Wrte" member "Test Octal String" by passing value as "HTK_device"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testOctalString_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#set value as 03/11/2018 05:30:00 / 1541203200
ATC0033 - Verify user can set Record "Test Read Wrte" member "Test Date" by passing value as "03/11/2018"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testDate_v1541203200_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#set value as 03/11/2018 07:30:00 / 1541210400
ATC0034 - Verify user can set Record "Test Read Wrte" member "Test Time" by passing value as "1541210400"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testTime_v1541210400_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}


ATC0035 - Verify user can set Record "Test Read Wrte" member "Test BitString" by passing value as "HTK_device"
    [Tags]           FF      HOST       Critical        HTK       Set_Item      Record
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Record_TestReadWrite_testBitStr_Rsc.json
    ${SetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${SetParamListJsonObj["FF"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SetParamListJsonObj["FF"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamListCompStatus}

#ATC001 - Verify user can access Array "Collection Directory" in HTK device.
#    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
#    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_Rsc.json
#    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
#    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${GetParamListCompStatus}

#ERROR
ATC002 - Verify user can not access Array "Collection Directory" when "iteminDD" is set to false in HTK device. - ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array       Error_case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_Error_iteminDD_False_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC003 - Verify user can access Array "Collection Directory" member in HTK device.
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_charterstics_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC004- Verify user can not access Array "Collection Directory" member with "subindex=5"in HTK device. -ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array       Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_charterstics_Error_subIndex_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC005 - Verify user can not access Array "Collection Directory" member with "itemID=21476155756"in HTK device. -ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array       Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_charterstics_Error_itemID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC006 - Verify user can not access Array "Collection Directory" member with "ContainerID=214761557477"in HTK device. -ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array       Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_charterstics_Error_conatinerID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC007 - Verify user can not access Array "Collection Directory" member with "ContainerTag=VISUALTB__"in HTK device. -ERROR
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array       Error_Case
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_CollectDir_charterstics_Error_conatinerTag_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["FF"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj["FF"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}


#DP-1172
### conditional Test Cases example.*** Keywords ***
ATC008 - Verify array condtional test cases by setting "Display_Format" value as "1" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_DisplayFormat_v1_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC009 - Verify array condtional test cases by setting "Display_Format" value as "2" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_DisplayFormat_v2_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}


ATC0010 - Verify array condtional test cases by setting "Edit_Format" value as "3" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_EditFormat_v3_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0012 - Verify array condtional test cases by setting "Edit_Format" value as "4" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_EditFormat_v4_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

#error . not implemented.
#ATC0013 - Verify array condtional test cases by setting "Min Value" value as "2" in HTK Device.
#ATC0014 - Verify array condtional test cases by setting "Min Value" value as "1" in HTK Device.
#ATC0015 - Verify array condtional test cases by setting "Max Value" value as "5" in HTK Device.
#ATC0016 - Verify array condtional test cases by setting "Min Value" value as "10" in HTK Device.

ATC0017 - Verify array condtional test cases by setting "Enum_value" value as "YES,NO" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_EnumValue_v9_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0018 - Verify array condtional test cases by setting "Enum_value" value as "YES,NO" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_EnumValue_v10_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0019 - Verify array condtional test cases by setting "Bit_enum" value as "0,1,2" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_BitEnum_v11_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0020 - Verify array condtional test cases by setting "Bit_enum" value as "0,1,2,6" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_BitEnum_v12_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0021 - Verify array condtional test cases by setting "Constant_unit" value as "msec" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_ConstantUnit_v13_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0022 - Verify array condtional test cases by setting "Constant_unit" value as "sec" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_ConstantUnit_v14_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0023 - Verify array condtional test cases by setting "Handling" value as "Read" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_Handling_v15_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

ATC0024 - Verify array condtional test cases by setting "Handling" value as "Read/write" in HTK Device
    [Tags]           FF      HOST       Critical        HTK       Get_Item      Array
    ${JsonFileContent}          get file                ${RecordArrayResource}/FF_Array_Condtional_Handling_v16_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualSetStatus}          Set item                ${GetParamListJsonObj["INPUT"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetParamListJsonObj["INPUT"]['Response']}
    ${SetParamListCompStatus}   compare json object     ${ActualSetStatus}     ${ExpectedSetStatus}
    should be true              True==${SetParamListCompStatus}
                                register callback       GetItem
    ${ActualGetStatus}          Get item                ${GetParamListJsonObj["EVAL"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedGetStatus}        set variable            ${GetParamListJsonObj["EVAL"]['Response']}
    ${GetParamListCompStatus}   compare json object     ${ActualGetStatus}     ${ExpectedGetStatus}
    should be true              True==${GetParamListCompStatus}

