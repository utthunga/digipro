*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py

*** Test Cases ***
ATC001 - Verify user can set required protocol type
    [Tags]          FF  Critical    Common  HTK
    ${JsonFileContent}          get file                resource/ATC001_FF_SetProtocolType.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["FF"]['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC002 - Verify user can initialize the stack
    [Tags]          FF  Critical    Common
    ${JsonFileContent}          get file                resource/ATC002_FF_InitializeProtocol.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC003 - Verify user can shutdown the stack
    [Tags]          FF  Common
    ${JsonFileContent}          get file                resource/ATC003_FF_ShutdownProtocol.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}