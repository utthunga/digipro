# -*- coding: robot -*-

.. code:: robotframework
 
    *** Setting ***
    Library		OperatingSystem
    Library		utilitylib/JsonUtil.py
    Library     CPMScanLib.py

    *** Variable ***
    ${ExpectedStatus}               "ok"

    *** Test Case ***
    Verify Set ProTocol Type Feature
        [Tags]					Critical	SetProtocol
        ${SetProtoStr}              Set Variable            {"Protocol":"FF"}
        ${SetProtoJsonObj}          evaluate                json.loads('''${SetProtoStr}''')   json
        ${ActualStatus}             Set Protocol Type       ${SetProtoJsonObj}
        ${SetProtocolCompStatus}    Compare String Data     "${ActualStatus}"      ${ExpectedStatus}
        Should Be True              True==${SetProtocolCompStatus}

    Verify FF Get Bus Parameter Feature (Fetch default params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Send all params along with modified params slot_time as 16)
        [Tags]					Critical    FFBusParameters	
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${BusParamJsonObj}      evaluate                json.loads('''${BusParamStr}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result}               Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of all bus params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Set Individual Param: slot_time=8)
        [Tags]					Critical	FFBusParameters
        ${SetBusParam}          Set Variable            {"FF_BUS_PARAMS":{"slot_time":8}}
        ${BusParamJsonObj}      evaluate                json.loads('''${SetBusParam}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result} = 			Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of slot_time as 8)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}
        
    Verify Connect with 2051 Pressure Transmitter Feature
        [Tags]                  Critical    FFConnect
        ${DeviceNode}           Set Variable            {"DeviceNodeAddress":21}
        ${SetConnectObj}        evaluate                json.loads('''${DeviceNode}''')     json
        ${ActualStatus}			Connect                 ${SetConnectObj}
        ${ConnectMenu}          Set Variable            {"ItemContainerTag":"","ItemHelp":"Top Level Root Menu for the Device","ItemID":0,"ItemInDD":false,"ItemInfo":{"ItemList":[{"ItemContainerTag":"","ItemHelp":"Advanced Device Menu Options","ItemID":1,"ItemInDD":false,"ItemLabel":"Advanced","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}]},"ItemLabel":"Device Menu","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}
        ${ExpectedConnect}      evaluate                json.loads('''${ConnectMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualStatus}      ${ExpectedConnect}
        Should Be True          True==${Result}
    
    Verify GetItem Feature(check for custom menu-block list for device 2051)
        [Tags]                  Critical    FFGetItem
        ${CustomMenuJson}       Set Variable            {"ItemContainerTag":"","ItemID":1,"ItemInDD":false,"ItemType":"MENU"}
        ${SetMenuObj}           evaluate                json.loads('''${CustomMenuJson}''')     json
        ${ActualData}           GetItem                 ${SetMenuObj}
        ${TopLevelMenu}         Get File                ffsuite/2051/1_2051BlockListRes.json
        ${ExpectedData}         evaluate                json.loads('''${TopLevelMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualData}      ${ExpectedData}
        Should Be True          True==${Result}
             
     Verify GetItem Feature(check for list for device blocks 2051-AI1400)
        [Tags]                  Critical    FFGetItem
        ${ResourceJson}         Get File               ffsuite/2051/2_2051BlckMenu_req.json
        ${SetAdvnceMenuObj}     evaluate               json.loads('''${ResourceJson}''')     json
        ${ActualData}           GetItem                ${SetAdvnceMenuObj}
        ${AdvanceMenuData}      Get File               ffsuite/2051/2_2051BlckMenu_res.json
        ${ExpectedData}         evaluate               json.loads('''${AdvanceMenuData}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify GetItem Feature(check for list for device 2051 Advance Block Menu)
        [Tags]                  Critical    FFGetItem
        ${ResourcePJson}        Get File               ffsuite/2051/3_2051BlockMenu_req.json
        ${SetParaMenuObj}       evaluate               json.loads('''${ResourcePJson}''')     json
        ${ActualData}           GetItem                ${SetParaMenuObj}
        ${AdvanceMenu}          Get File               ffsuite/2051/3_2051BlockMenu_res.json
        ${ExpectedData}         evaluate               json.loads('''${AdvanceMenu}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify GetItem Feature(check for list for device 2051 ParamList Block)
        [Tags]                  Critical    FFGetItem
        ${VariableJson}         Get File               ffsuite/2051/4_2051VariableList_req.json
        ${SetVariableObj}       evaluate               json.loads('''${VariableJson}''')     json
        ${ActualData}           GetItem                ${SetVariableObj}
        ${VariableDeatils}      Get File               ffsuite/2051/4_2051VariableList_res.json
        ${ExpectedData}         evaluate               json.loads('''${VariableDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ARITHMETIC FLOAT Variable type in 2051- ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${AritVariableJson}     Get File               ffsuite/2051/variables/2051_ART_float_req.json
        ${SetAVariableObj}      evaluate               json.loads('''${AritVariableJson}''')     json
        ${ActualData}           GetItem                ${SetAVariableObj}
        ${ArithVarDeatils}      Get File               ffsuite/2051/variables/2051_ART_float_res.json
        ${ExpectedData}         evaluate               json.loads('''${ArithVarDeatils}''',strict=False)     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ARITHMETIC UI Variable type in 2051 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${AritVariableJson}     Get File               ffsuite/2051/variables/2051_ART_UI_req.json
        ${SetAVariableObj}      evaluate               json.loads('''${AritVariableJson}''')     json
        ${ActualData}           GetItem                ${SetAVariableObj}
        ${ArithVarDeatils}      Get File               ffsuite/2051/variables/2051_ART_UI_res.json
        ${ExpectedData}         evaluate               json.loads('''${ArithVarDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ENUM Enum Variable type in 2051 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${VarEnumJson}          Get File               ffsuite/2051/variables/2051_Enum_req.json
        ${SetEEnumObj}          evaluate               json.loads('''${VarEnumJson}''')     json
        ${ActualData}           GetItem                ${SetEEnumObj}
        ${EEnumDeatils}         Get File               ffsuite/2051/variables/2051_Enum_res.json
        ${ExpectedData}         evaluate               json.loads('''${EEnumDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ENUM BitEnum Variable type in 2051 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${VarBEnumJson}         Get File               ffsuite/2051/variables/2051_BitEnum_req.json
        ${SetBEnumObj}          evaluate               json.loads('''${VarBEnumJson}''')     json
        ${ActualData}           GetItem                ${SetBEnumObj}
        ${BEnumDetails}         Get File               ffsuite/2051/variables/2051_Bitenum_res.json
        ${ExpectedData}         evaluate               json.loads('''${BEnumDetails}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify Disconnect Feature
       [Tags]                  Critical    FFDisconnect
       ${ExpectedDisconnect}   evaluate               json.loads(''' ${ExpectedStatus}''')     json
       ${ActualData}           Disconnect
       ${Result}               Compare Json Objects      ${ActualData}      ${ExpectedDisconnect}
       Should Be True          True==${Result}
       
     
        