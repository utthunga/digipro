*** Settings ***
Library     Process
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library  Collections
#Suite Setup     start cpm application
#Suite Teardown  stop cpm application

#*** Keywords ***
#start cpm application
#    terminate all processes
#    ${CPMProcessHandle}     start process   /home/devuser/repository/atf_update/digipro/build/x86/release/bin/communicator      stdout=/home/devuser/result/cpm.log    stderr=/home/devuser/cpm.log
#    sleep   5s
#stop cpm application
#    terminate all processes

*** Test Cases ***
ATC001 - Verify user can set required protocol type
    [Tags]          FF  Critical    Common  HTK
    ${JsonFileContent}          get file                resource/ATC001_FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["Response"]}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC002 - Verify user can initialize the stack
    [Tags]          FF  Critical    Common
    ${JsonFileContent}          get file                resource/ATC002_FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

#ATC004 - Verify user can find available device in the network
#    [Tags]          FF  Common
#    ${JsonFileContent}          get file                resource/ATC004_FF_Scan_Rsc.json
#    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
#    ${NotifyTimeout}            convert to integer      30
#    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
#    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
#    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
#    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
#    should be true              True==${ScanStatus}

ATC005 - Verify user can connect to field device
    [Tags]          FF  Method
    ${JsonFileContent}          get file                resource/ATC005_FF_Connect_Rsc.json
    ${ConnectJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualResult}             connect                 ${ConnectJsonObj["Request"]}
    ${ExpectedResult}           set variable            ${ConnectJsonObj["Response"]}
    ${ConnectStatus}            compare json object     ${ActualResult}     ${ExpectedResult}
    should be true              True==${ConnectStatus}

ATF007 - Verify user can subscribe for variable and check the value of it
    [Tags]          FF  Variable Subscription       E+H
    ${JsonFileContent}          get file                resource/ATC008_FF_Variable_Subscription_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    #${NotifyTimeout}            convert to integer      ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]["ItemRefreshRate"]}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyData}               retrieve item data      ${NotifyTimeOut}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Notify"]}
    should be true              True==${NotifyStatus}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback
    should be true              True==${NotifyStatus}

#ATC006 - Verify user can execute method that evaluates built-in functions within DD services
#    [Tags]          FF  Method      HTK
#    ${JsonFileContent}          get file                resource/ATC006_FF_ME_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   20
#        \   ${NotificationData}         retrieve item data      ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#    ${Status}                   unregister callback
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    :for    ${Index}    in range    0   4
#        \   ${NotificationData}         retrieve item data      ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#    ${Status}                   unregister callback
#

#ATC007 - Verify user can disconnect field device
#    [Tags]          FF  Method
#    ${JsonFileContent}          get file                resource/ATC007_FF_Disconnect_Rsc.json
#    ${ConnectJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualResult}             disconnect              ${ConnectJsonObj["Request"]}
#    ${ExpectedResult}           set variable            ${ConnectJsonObj["Response"]}
#    ${ConnectStatus}            compare json object     ${ActualResult}     ${ExpectedResult}
#    should be true              True==${ConnectStatus}
#
#ATC003 - Verify user can shutdown the stack
#    [Tags]          FF  Common
#    ${JsonFileContent}          get file                resource/ATC003_FF_ShutdownProtocol_Rsc.json
#    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
#    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${ShutdownCompStatus}