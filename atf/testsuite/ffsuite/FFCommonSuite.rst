# -*- coding: robot -*-

.. code:: robotframework
 
    *** Setting ***
    Library		OperatingSystem
    Library		utilitylib/JsonUtil.py
    Library     CPMScanLib.py

    *** Variable ***
    ${ExpectedStatus}               "ok"

    *** Test Case ***
    Verify Set ProTocol Type Feature
        [Tags]					Critical	SetProtocol
        ${SetProtoStr}              Set Variable            {"Protocol":"FF"}
        ${SetProtoJsonObj}          evaluate                json.loads('''${SetProtoStr}''')   json
        ${ActualStatus}             Set Protocol Type       ${SetProtoJsonObj}
        ${SetProtocolCompStatus}    Compare String Data     "${ActualStatus}"      ${ExpectedStatus}
        Should Be True              True==${SetProtocolCompStatus}

    Verify FF Get Bus Parameter Feature (Fetch default params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Send all params along with modified params slot_time as 16)
        [Tags]					Critical    FFBusParameters	
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${BusParamJsonObj}      evaluate                json.loads('''${BusParamStr}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result}               Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of all bus params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Set Individual Param: slot_time=8)
        [Tags]					Critical	FFBusParameters
        ${SetBusParam}          Set Variable            {"FF_BUS_PARAMS":{"slot_time":8}}
        ${BusParamJsonObj}      evaluate                json.loads('''${SetBusParam}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result} = 			Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of slot_time as 8)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Device Recognition Feature 
        [Tags]                  Critical    FFDeviceRecognition
        ${DeviceList}           Set Variable            [{"DeviceNodeAddress": 21,"DeviceCommissioned": true, "DeviceIdInfo": "001151AC00092316013747-000075165", "DeviceTag": "PT2051${SPACE * 2}DAG${SPACE * 21}"}, {"DeviceNodeAddress": 34, "DeviceIdInfo": "0011510644-EPM-TEMP-0x24951C23${SPACE * 2}", "DeviceTag": "644-IDC${SPACE * 25}"}]
        ${ExpectedDevList}      evaluate                json.loads('''${DeviceList}''')     json
        ${ActualDevList}        Scan Network
        ${Result}               Compare Json Objects    ${ActualDevList}        ${ExpectedDevList}
        Should Be True          True==${Result}
        
     Verify Connect Feature 
        [Tags]                  Critical    FFConnect
        ${DeviceNode}           Set Variable            {"DeviceNodeAddress":34}
        ${SetConnectObj}        evaluate                json.loads('''${DeviceNode}''')     json
        ${ActualStatus}         Connect                 ${SetConnectObj}
        ${ConnectMenu}          Set Variable            {"ItemContainerTag":"","ItemHelp":"Top Level Root Menu for the Device","ItemID":0,"ItemInDD":false,"ItemInfo":{"ItemList":[{"ItemContainerTag":"","ItemHelp":"Advanced Device Menu Options","ItemID":1,"ItemInDD":false,"ItemLabel":"Advanced","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}]},"ItemLabel":"Device Menu","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}
        ${ExpectedConnect}      evaluate                json.loads('''${ConnectMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualStatus}      ${ExpectedConnect}
        Should Be True          True==${Result}
        
     Verify GetItem Feature(check for custom menu-block list for device 0644)
        [Tags]                  Critical    FFGetItem
        ${CustomMenuJson}       Set Variable            {"ItemContainerTag":"","ItemID":1,"ItemInDD":false,"ItemType":"MENU"}
        ${SetMenuObj}           evaluate                json.loads('''${CustomMenuJson}''')     json
        ${ActualStatus}         GetItem                 ${SetMenuObj}
        ${TopLevelMenu}         Get File                /home/adesh/repos/repos/ATF_work/digipro/atf/testsuite/ffsuite/644AdvanceDeviceMenu.json
        ${ExpectedMenu}         evaluate                json.loads('''${TopLevelMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualStatus}      ${ExpectedMenu}
        Should Be True          True==${Result}
     
     
     Verify GetItem Feature(check for list for device 0644-Resource Block)
        [Tags]                  Critical    FFGetItem
        ${ResourceJson}         Get File               /home/adesh/repos/repos/ATF_work/digipro/atf/testsuite/ffsuite/644Resourceblock_Request.json
        ${SetAMenuObj}          evaluate               json.loads('''${ResourceJson}''')     json
        ${ActualStatus}         GetItem                ${SetAMenuObj}
        ${AdvanceMenu}          Get File               /home/adesh/repos/repos/ATF_work/digipro/atf/testsuite/ffsuite/644Resourceblock_Response.json
        ${ExpectedAMenu}        evaluate               json.loads('''${AdvanceMenu}''')     json
        ${Result}               Compare Json Objects   ${ActualStatus}      ${ExpectedAMenu}
        
     Verify Disconnect Feature
       [Tags]                  Critical    FFDisconnect
       ${ExpectedDisconnect}   evaluate               json.loads(''' ${ExpectedStatus}''')     json
       ${ActualData}           Disconnect
       ${Result}               Compare Json Objects      ${ActualData}      ${ExpectedDisconnect}
       Should Be True          True==${Result}
       
       
     
