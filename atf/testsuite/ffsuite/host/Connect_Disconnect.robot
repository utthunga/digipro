*** Settings ***
Documentation    Connect / Disconnect feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Setup     run keywords        SetLangCodeEnKW
...             AND                 SetProtocolTypeKW
...             AND                 InitializeStackKW
Suite Teardown  run keywords        ShutdownStackKW

*** Test Cases ***
#Test cases for connect/dis-connect, shoutdown, init stack (91-110)

#put connect/disconnect in loop for 10 times
ATC0091 - Verify connect-diconnect with field device -HTK
    [Tags]           Critical        HTK       Connect_Disconnect
    :FOR    ${i}    IN RANGE    1
    \    Log    ${i}
    \    ${JsonFileContent}          get file                ${HostResources}/FF_Connectdevice_Rsc.json
    \    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    \    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    \    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    \    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    \    should be true              True==${SetConnectCompStatus}
    \    sleep                       3
    \    ${JsonFileContent}          get file                ${HostResources}/FF_DisConnectdevice_Rsc.json
    \    ${SetDisconnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    \    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["FF"]["Request"]}
    \    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj["FF"]['Response']}
    \    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    \    should be true              True==${SetDisconnectCompStatus}
    \    Exit For Loop If    ${i} == 1

ATC0092 - Verify connect with field device -HTK
    [Tags]           Critical        HTK       Connect
    ${JsonFileContent}          get file                ${HostResources}/FF_Connectdevice_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

ATC00093 - Verify re-send connect request to HTK -Error
    [Tags]           Common        HTK       Connect
    ${JsonFileContent}          get file                ${HostResources}/FF_Connectdevice_Resend_ERROR_Rsc.json
    ${SetConnectErrorJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectErrorJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectErrorJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

ATC0094 - Verify send connect to 0644 while connected with HTK -ERROR
    [Tags]           Common        HTK       Connect
    ${JsonFileContent}          get file                ${HostResources}/FF_Connectdevice_0644_ERROR_Rsc.json
    ${Setconnect644JsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${Setconnect644JsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${Setconnect644JsonObj["FF"]['Response']}
    ${SetConnect644CompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnect644CompStatus}

ATC0095 - Verify Disconnect with field device HTK
    [Tags]           Critical        HTK       Disconnect
    ${JsonFileContent}          get file                ${HostResources}/FF_DisConnectdevice_Rsc.json
    ${SetDisconnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj["FF"]['Response']}
    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectCompStatus}

ATC0096 - Verify connect with wrong device address - Error
    [Tags]           Common        HTK       Connect
    ${JsonFileContent}          get file                ${HostResources}/FF_Connectdevice_WrngAddress_Rsc.json
    ${SetconnectwrongJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetconnectwrongJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetconnectwrongJsonObj["FF"]['Response']}
    ${SetconnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetconnectCompStatus}

ATC0097 - Verify Disconnect with ivnalid device - Error.
    [Tags]           Common        HTK       Disconnect
    ${JsonFileContent}          get file                ${HostResources}/FF_DisonnectFail_afterDisonnect_Rsc.json
    ${SetDisConnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisConnectJsonObj["FF"]['Response']}
    ${SetDisconnectStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectStatus}

ATC0098 - Verify user can diconnect again after Disonnect -Error
    [Tags]           Common        HTK       Disconnect
    ${JsonFileContent}          get file                ${HostResources}/FF_Disconnectdevice_Resend_ERROR_Rsc.json
    ${SetDisConnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect                 ${SetDisConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisConnectJsonObj["FF"]['Response']}
    ${SetDisconnectStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectStatus}

ACT0040 - Verify if user can connect before init stack.- ERROR
    [Tags]          Common        Connect
    ${JsonFileContent}          get file                resource/FF_Connectdevice_before_initStack_error_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

#ACT0099 - verify connect  before init stack.
#ACT00100 - Verify to set protocol after connect

