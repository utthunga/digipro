*** Settings ***
Documentation    Set Protocol Feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
#Testcase ACT001,2,4,6  -- DP-1040 [ FF- YAML] Error strings are not decoding properly -
ATC001 - Verify user can set required protocol type with empty string _ERROR
    [Tags]          Non-Critical    SetProtocol
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_EmptyString_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC002 - Verify user can set required protocol type with invalid string "qwert" -ERROR
    [Tags]          Non-Critical    SetProtocol
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_InvalidString_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC003 - Verify if user can start scan before set Protocol - Error
    [Tags]          Non-Critical    SetProtocol
    ${JsonFileContent}          get file                ${HostResources}/FF_StartScan_before_SetProtocol_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${ScanJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj['Response']}
    ${ScanCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ScanCompStatus}

ATC004 - Verify user can set required protocol type as "FF"
    [Tags]          Critical        SetProtocol
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC005 - Verify user can set required protocol type after initialize / shutdown the stack
    [Tags]          Critical            SetProtocol
    [Setup]         run keywords        SetProtocolTypeKW
    ...             AND                 InitializeStackKW
    ...             AND                 ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC006 - Verify set protocol by sending request again (when protocol is already set) as "FF"- Error
    [Tags]          Non-Critical    SetProtocol
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_Resend_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC007 - Verify set protocol by sending string as "qwert" (when protocol is already set)- Error
    [Tags]          Non-Critical    SetProtocol
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_InvalidString_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}
#
##raised JIRA DP-1033
#ATC008 - Verify user can set protocol as "FF" after initialize stack
#    [Tags]          Non-Critical    SetProtocol
#    [Setup]         Run Keywords    SetProtocolTypeKW     InitializeStackKW     ShutdownStackKW
#    ${JsonFileContent}          get file                resource/
#    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set protocol type       ${InitializeJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
#    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${InitializeCompStatus}

#ATC008 - Verify set protocl after shut down protocl before init stack

#ATC009 - Verify set protocl by shut down  and again send shut down before init stack
