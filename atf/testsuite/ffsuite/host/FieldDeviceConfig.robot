*** Settings ***
Documentation    Field device configs feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW


*** Test Cases ***
#Field device setup
ATC0068 - Verify the Get Field DeviceConfig - 0644
    [Tags]           FF     HOST      Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_GetFiledDeviceSetup_Rsc.json
    ${GetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get field device config     ${GetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${GetFieldJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFieldDCompareJsonObj}

ATC0069 - Verify the set field device 0644 as Link Master
    [Tags]           FF        HOST        Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_SetFiledDevice_LM_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}

ATC0070 - Verify user Scan with 0644 as Link Master
    [Tags]           FF     HOST      Critical        Scanning
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
    should be true              True==${ScanStatus}

ATC0071 - Verify the set field device 0644 as Basic Device
    [Tags]           FF        HOST        Critica        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_SetFiledDevice_BasicDevice_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}

ATC0072 - Verify user Scan with 0644 as Basic Master.
    [Tags]           FF     HOST      Critical        Scanning
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
    should be true              True==${ScanStatus}

ATC0073 - Verify the Get Field DeviceConfig - 0644
    [Tags]           FF        HOST      Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_GetFiledDeviceSetup_Rsc.json
    ${GetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get field device config     ${GetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${GetFieldJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFieldDCompareJsonObj}

ATC0074 - Verify user can get device setup for HTK - Error
    [Tags]           FF      HOST       Critical        HTK     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_GetFieldDeviceSetup_HTK_Rsc.json
    ${GetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get field device config     ${GetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${GetFieldJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFieldDCompareJsonObj}

ATC0075 - Verify user can de-commisssion device- 0644
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_SetFieldDeviceSetup_Decommsion_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}


#Error - 1082
ATC0076 - Verify user can not De-Commission a device which is already De-commisssioned. - Error.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup     Error_case
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${DeviceCount}              evaluate                len('''${ScanResult}''')
    ${newVariable}              evaluate                ${ScanResult}[1]['DeviceNodeAddress']
    ${newVariable2}             convert to integer      ${newVariable}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${JsonFileDeContent}        get file                resource/FF_SetFieldDeviceSetup_Decommsion_Error_DeComm_Rsc.json
    ${target value}             Create List             ${newVariable2}
    ${SetDeFieldJsonObj}        evaluate                json.loads('''${JsonFileDeContent}''')   json
    set to dictionary           ${SetDeFieldJsonObj["FF"]["Request"]}    DeviceNodeAddress=${newVariable2}
    ${ActualDeStatus}           set field device config     ${SetDeFieldJsonObj["FF"]["Request"]}
    ${ExpectedDeStatus}         set variable                ${SetDeFieldJsonObj["FF"]['Response']}
    ${GetFiellDeCmpreJsonObj}   compare json object         ${ActualDeStatus}     ${ExpectedDeStatus}
    should be true              True==${GetFiellDeCmpreJsonObj}

ATC0077 - Verify user can Commission 0644 device with valid node address.
    [Tags]           FF     HOST      Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${DeviceCount}              evaluate                len('''${ScanResult}''')
    ${newVariable}              evaluate                ${ScanResult}[1]['DeviceNodeAddress']
    ${newVariable2}             convert to integer      ${newVariable}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_Commission_Rsc.json
    ${target value}             Create List             ${newVariable2}
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    set to dictionary           ${CommJsonObj["FF"]["Request"]}    DeviceNodeAddress=${newVariable2}
    ${ActualStatus}             set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedStatus}           set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFieldDCompareJsonObj}

ATC0078 - Verify user De-commission 0644 device and can not commission device with long tag and it will take 32 chars.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_SetFieldDeviceSetup_Decommsion_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${DeviceCount}              evaluate                len('''${ScanResult}''')
    ${newVariable}              evaluate                ${ScanResult}[1]['DeviceNodeAddress']
    ${newVariable2}             convert to integer      ${newVariable}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_CommissionError_LongTag_Rsc.json
    ${target value}             Create List             ${newVariable2}
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    set to dictionary           ${CommJsonObj["FF"]["Request"]}    DeviceNodeAddress=${newVariable2}
    ${ActualCommStatus}         set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedCommStatus}       set variable                ${CommJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualCommStatus}     ${ExpectedCommStatus}
    should be true              True==${SetFieldDCompareJsonObj}

ATC0079 - Verify user can not commission device with empty tags. - Error.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup     Error_Case
    ${JsonFileContent}          get file                    resource/FF_SetFieldDeviceSetup_Decommsion_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${DeviceCount}              evaluate                len('''${ScanResult}''')
    ${newVariable}              evaluate                ${ScanResult}[1]['DeviceNodeAddress']
    ${newVariable2}             convert to integer      ${newVariable}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_CommissionError_EmptyTag_Rsc.json
    ${target value}             Create List             ${newVariable2}
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    set to dictionary           ${CommJsonObj["FF"]["Request"]}    DeviceNodeAddress=${newVariable2}
    ${ActualEptyStatus}         set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedEptyStatus}       set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldEtCmprJsonObj}    compare json object         ${ActualEptyStatus}     ${ExpectedEptyStatus}
    should be true              True==${GetFieldEtCmprJsonObj}

ATC0080 - Verify user can not commission device with NULL tags. - Error.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup     Error_Case
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_CommissionError_EmptyTag_Rsc.json
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    ${ActualNullStatus}         set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedNullStatus}       set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldNullCmprJsonObj}  compare json object         ${ActualNullStatus}     ${ExpectedNullStatus}
    should be true              True==${GetFieldNullCmprJsonObj}

ATC0081 - Verify user can De-commission 0644 device and can commission device with valid tag.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup
    ${JsonFileContent}          get file                    resource/FF_SetFieldDeviceSetup_Decommsion_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}
    ${JsonFileContent}          get file                resource/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${DeviceCount}              evaluate                len('''${ScanResult}''')
    ${newVariable}              evaluate                ${ScanResult}[1]['DeviceNodeAddress']
    ${newVariable2}             convert to integer      ${newVariable}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_Commission_Rsc.json
    ${target value}             Create List             ${newVariable2}
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    set to dictionary           ${CommJsonObj["FF"]["Request"]}    DeviceNodeAddress=${newVariable2}
    ${ActualTagStatus}          set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedTagStatus}        set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldTagCpareJsonObj}  compare json object         ${ActualTagStatus}     ${ExpectedTagStatus}
    should be true              True==${GetFieldTagCpareJsonObj}

ATC0082 - Verify user can not Commission 0644 device without De-Commission. -Error.
    [Tags]           FF     HOST      Critical        0644     Field_Device_Setup       Error_Case
    ${JsonFileCCommContent}     get file                    resource/FF_SetFiledDeviceSetup_Commission_without_DeComm_Rsc.json
    ${CommJsonObj}              evaluate                    json.loads('''${JsonFileCCommContent}''')   json
    ${ActualStatus}             set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedStatus}           set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFieldDCompareJsonObj}

#Error- De-commsion is throwing an error but it is decommsioing the device. that's why this test case is grtting failed.
ATC0083 - Verify user can De-Commission 0644 device and can not Commission it with wrong current device address - ERROR.
    [Tags]           FF      HOST       Critical        0644     Field_Device_Setup     Error_Case
    ${JsonFileContent}          get file                    resource/FF_SetFieldDeviceSetup_Decommsion_Rsc.json
    ${SetFieldJsonObj}          evaluate                    json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set field device config     ${SetFieldJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable                ${SetFieldJsonObj["FF"]['Response']}
    ${SetFieldDCompareJsonObj}  compare json object         ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetFieldDCompareJsonObj}l
    ${JsonFileCCommContent}     get file                resource/FF_SetFiledDeviceSetup_CommissionError_wrongAddress_Rsc.json
    ${CommJsonObj}              evaluate                json.loads('''${JsonFileCCommContent}''')   json
    ${NewInvalidAddress}        convert to integer      67
    set to dictionary           ${CommJsonObj["FF"]["Request"]}    DeviceNodeAddress=${NewInvalidAddress}
    ${ActualCommStatus}         set field device config     ${CommJsonObj["FF"]["Request"]}
    sleep                       60s
    ${ExpectedCommStatus}       set variable                ${CommJsonObj["FF"]['Response']}
    ${GetFieldDCompareJsonObj}  compare json object         ${ActualCommStatus}     ${ExpectedCommStatus}
    should be true              True==${GetFieldDCompareJsonObj}
