*** Settings ***
Documentation    Scanning feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
# for scan functionallty
#put start scan and stop in 50 loops
#ATC0061 - Verify user can find available device in the network - Start and stop Scan(HTK and 0644)
#    [Tags]           Critical        Scanning
#    [Setup]         run keywords    SetProtocolTypeKW       InitializeStackKW
#    :FOR    ${i}    IN RANGE    50
#    \    Log    ${i}
#    \    ${JsonFileContent}          get file                ${HostResources}/FF_Scan_Rsc.json
#    \    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    \    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
#    \    ${NotifyTimeout}            convert to integer      20
#    \    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
#    \    sleep                       3
#    \    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
#    \    sleep                       3
#    \    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
#    \    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
#    \    should be true              True==${ScanStatus}
#    \    Exit For Loop If    ${i} == 50

ATC0062 - Verify user can find available device in the network - Start and stop Scan(HTK and 0644)
    [Tags]           Critical        Scanning
    [Setup]          run keywords    SetProtocolTypeKW       InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${NotifyTimeout}            convert to integer      30
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Notification"]}
    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
    should be true              True==${ScanStatus}

#need to handle
#ATC0063 - Verify user can send start scan, while scanning is in progress - Error (Failed to start scanning in CPM application)

ATC0064 - Verify user can stop scan after scan is stopped - Error
    [Tags]           Common        Scanning
    [Setup]          run keywords    SetProtocolTypeKW
    ...              AND                InitializeStackKW
    [Teardown]       run keywords        ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_Stop_Rsc.json
    ${StopScanJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             stop scan               ${StopScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${StopScanJsonObj["StopScan"]["Response"]}
    ${StopScanStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${StopScanStatus}

ACT0041 - Verify if user can start Scan before initliazing the stack -Error
    [Tags]          Common        Scanning
    [Setup]         run keywords    SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_Scan_before_initStack_Rsc.json
    ${SetScanObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${SetScanObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetScanObj["StartScan"]["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

#ATC0065 - while scanning is in progress, shut down stack
#ATC0066 - send scan after device is connected.
#ATC0067 - switch protocl while scanning is in progress
