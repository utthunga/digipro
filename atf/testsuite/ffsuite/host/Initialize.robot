*** Settings ***
Documentation    Stack initialization feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
ATC0001 - Verify user can initialize the stack
    [Tags]          Critical        Initialize
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC0002 - Verify user can initialize the stack after initialized stack - Error
    [Tags]          Common        Initialize
    ${JsonFileContent}          get file                ${HostResources}/FF_Re-InitializeProtocol_Rsc.json
    ${InitializeAgainJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeAgainJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeAgainJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

#DP-1038 : code crash
#ACT00 - Verify if user can start scan before initliazing the stack -Error