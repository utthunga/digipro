*** Settings ***
Documentation    Shutdown feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot


*** Test Cases ***
ATC0001 - Verify user can shutdown the stack
    [Tags]          Critical        Shutdown
    [Setup]         run keywords    SetProtocolTypeKW
    ...             AND             InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_ShutdownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC002 - Verify user can initialize the stack after shut down without set protocol - Error
    [Tags]           Common       Shutdown
    ${JsonFileContent}          get file                ${HostResources}/FF-InitStack_afterShutDown_withoutSetprotocl_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

    #ACT00101 - verify send shutdown  , while device is connected.