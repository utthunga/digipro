*** Settings ***
Documentation    Bus paramater feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
# Test cases for Bus params (21-40)
ATC0021 - Verify FF Get Bus Parameter Feature (Fetch default params)
    [Tags]         Critical     BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_GetBusParam_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamCompStatus}

ATC0022 - Verify FF Set Bus Parameter Feature (Send all params along with modified params slot_time as 16)
    [Tags]         Critical        BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_st16_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0023 - Verify FF Set Bus Parameter Feature (Send all params along with modified params fun as 30)
    [Tags]          Critical        BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_fun30_Rsc.json
    ${SetBusfunParamObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusfunParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusfunParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0024 - Verify FF Set Bus Parameter Feature (Send all params along with modified params nun as 10)
    [Tags]          Critical        BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_nun10_Rsc.json
    ${SetBusnunParamObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusnunParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusnunParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0025 - Verify FF changed Bus Parameter ( slot_time :16, nun: 10, fun:30)
    [Tags]          Critical    Common        BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_GetBusParam_validate_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamCompStatus}

ATC0046 - Verify user can initialize stack with modified bus paramaters (slot_time :16, nun: 10, fun:30)
    [Tags]          Critical    Common        BusParameters
    [Setup]         run keywords    SetProtocolTypeKW
    [Teardown]      run keywords    ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_GetBusParam_validate_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamCompStatus}
    ${JsonFileContent}          get file                ${HostResources}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}


ATC0026 - Verify FF Set Bus Parameter Feature ERROR condition (fun:240 + nun:10 = Error)
    [Tags]          Common      BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_ErrorNunFun_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0027 - Verify FF Set Bus Parameter Feature ERROR condition as nun:300(fun:240 + nun:300 = Error)
    [Tags]          Critical    BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_ErrorNun300_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0028 - Verify FF Set Bus Parameter Feature ERROR condition as slot_time:20 - ERROR
    [Tags]          Common      BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_Error_Slot_time20_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0029 - Verify FF Set Bus Parameter Feature ERROR condition as mipd:23- ERROR
    [Tags]          Common      BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_Error_mipd23_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             Set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

ATC0030 - Verify FF Set Bus Parameter Feature ERROR condition as invalid bus param- ERROR
    [Tags]          Common      BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_Error_invalidBusParam_Rsc.json
    ${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             Set bus param           ${SetBusErrorParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}

#handle during CPM validation
#ATC0031 - Verify FF Set Bus Parameter Feature ERROR condition as invalid slot_time as "qwert"- ERROR - JSON logic error
    #[Tags]          FF     HOST      BusParameters
    #${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_Error_Slot_time_invalid_string_Rsc.json
    #${SetBusErrorParamObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    #${ActualStatus}             set bus Param           ${SetBusErrorParamObj["Request"]}
    #${ExpectedStatus}           set variable            ${SetBusErrorParamObj["Response"]}
    #${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    #should be true              True==${SetParamCompStatus}

ATC0032 - Verify FF Set default Bus parameters
    [Tags]          Common      BusParameters
    [Setup]        run keywords     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_default_Rsc.json
    ${SetBusDefaultParamObj}    evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusDefaultParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusDefaultParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}


ATC0046 - Verify user can initialize the stack after setting default bus paramters
    [Tags]          Critical        Initialize
    [Setup]         run keywords    SetProtocolTypeKW
    [Teardown]      run keywords    ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/FF_SetBusParam_default_Rsc.json
    ${SetBusDefaultParamObj}    evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusDefaultParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusDefaultParamObj["Response"]}
    ${SetParamCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetParamCompStatus}
    ${JsonFileContent}          get file                ${HostResources}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

