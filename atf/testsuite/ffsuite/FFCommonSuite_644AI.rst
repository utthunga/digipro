# -*- coding: robot -*-

.. code:: robotframework
 
    *** Setting ***
    Library		OperatingSystem
    Library		utilitylib/JsonUtil.py
    Library     CPMScanLib.py

    *** Variable ***
    ${ExpectedStatus}               "ok"

    *** Test Case ***
    Verify Set ProTocol Type Feature
        [Tags]					Critical	SetProtocol
        ${SetProtoStr}              Set Variable            {"Protocol":"FF"}
        ${SetProtoJsonObj}          evaluate                json.loads('''${SetProtoStr}''')   json
        ${ActualStatus}             Set Protocol Type       ${SetProtoJsonObj}
        ${SetProtocolCompStatus}    Compare String Data     "${ActualStatus}"      ${ExpectedStatus}
        Should Be True              True==${SetProtocolCompStatus}

    Verify FF Get Bus Parameter Feature (Fetch default params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Send all params along with modified params slot_time as 16)
        [Tags]					Critical    FFBusParameters	
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${BusParamJsonObj}      evaluate                json.loads('''${BusParamStr}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result}               Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of all bus params)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":16,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}

    Verify FF Set Bus Parameter Feature (Set Individual Param: slot_time=8)
        [Tags]					Critical	FFBusParameters
        ${SetBusParam}          Set Variable            {"FF_BUS_PARAMS":{"slot_time":8}}
        ${BusParamJsonObj}      evaluate                json.loads('''${SetBusParam}''')    json
        ${ActualStatus}         Set Bus Param           ${BusParamJsonObj}
        ${Result} = 			Compare String Data		"${ActualStatus}"       ${ExpectedStatus}
        Should Be True			True==${Result}

    Verify FF Get Bus Parameter Feature (Check for previous set of slot_time as 8)
        [Tags]					Critical	FFBusParameters
        ${BusParamStr}          Set Variable            {"FF_BUS_PARAMS":{"fun":248,"mipd":16,"mrd":10,"nun":0,"phge":0,"phis_skew":0,"phlo":6,"phpe":2,"slot_time":8,"this_link":0,"tsc":3}}
        ${ExpectedBusParam}     evaluate                json.loads('''${BusParamStr}''')   json
        ${ActualBusParam}       Get Bus Param 
        ${Result}               Compare Json Objects    ${ActualBusParam}       ${ExpectedBusParam}
        Should Be True			True==${Result}
        
    Verify Connect Feature 
        [Tags]                  Critical    FFConnect
        ${DeviceNode}           Set Variable            {"DeviceNodeAddress":34}
        ${SetConnectObj}        evaluate                json.loads('''${DeviceNode}''')     json
        ${ActualStatus}         Connect                 ${SetConnectObj}
        ${ConnectMenu}          Set Variable            {"ItemContainerTag":"","ItemHelp":"Top Level Root Menu for the Device","ItemID":0,"ItemInDD":false,"ItemInfo":{"ItemList":[{"ItemContainerTag":"","ItemHelp":"Advanced Device Menu Options","ItemID":1,"ItemInDD":false,"ItemLabel":"Advanced","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}]},"ItemLabel":"Device Menu","ItemReadOnly":true,"ItemSelection":true,"ItemType":"MENU"}
        ${ExpectedConnect}      evaluate                json.loads('''${ConnectMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualStatus}      ${ExpectedConnect}
        Should Be True          True==${Result}
        
     Verify GetItem Feature(check for custom menu-block list for device 0644)
        [Tags]                  Critical    FFGetItem
        ${CustomMenuJson}       Set Variable            {"ItemContainerTag":"","ItemID":1,"ItemInDD":false,"ItemType":"MENU"}
        ${SetMenuObj}           evaluate                json.loads('''${CustomMenuJson}''')     json
        ${ActualData}           GetItem                 ${SetMenuObj}
        ${TopLevelMenu}         Get File                ffsuite/644_AI1400/644AdvanceDeviceMenu.json
        ${ExpectedData}         evaluate                json.loads('''${TopLevelMenu}''')     json
        ${Result}               Compare Json Objects    ${ActualData}      ${ExpectedData}
        Should Be True          True==${Result}
             
     Verify GetItem Feature(check for list for device 0644-AI1400)
        [Tags]                  Critical    FFGetItem
        ${ResourceJson}         Get File               ffsuite/644_AI1400/644AI1400block_Request.json
        ${SetAdvnceMenuObj}     evaluate               json.loads('''${ResourceJson}''')     json
        ${ActualData}           GetItem                ${SetAdvnceMenuObj}
        ${AdvanceMenuData}      Get File               ffsuite/644_AI1400/644AI1400block_Response.json
        ${ExpectedData}         evaluate               json.loads('''${AdvanceMenuData}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify GetItem Feature(check for list for device 0644-AI1400 Advance Block)
        [Tags]                  Critical    FFGetItem
        ${ResourcePJson}        Get File               ffsuite/644_AI1400/644AIAdvBlockMenu_request.json
        ${SetParaMenuObj}       evaluate               json.loads('''${ResourcePJson}''')     json
        ${ActualData}           GetItem                ${SetParaMenuObj}
        ${AdvanceMenu}          Get File               ffsuite/644_AI1400/644AIAdvBlockMenu_response.json
        ${ExpectedData}         evaluate               json.loads('''${AdvanceMenu}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify GetItem Feature(check for list for device 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFGetItem
        ${VariableJson}         Get File               ffsuite/644_AI1400/644AIParamList_request.json
        ${SetVariableObj}       evaluate               json.loads('''${VariableJson}''')     json
        ${ActualData}           GetItem                ${SetVariableObj}
        ${VariableDeatils}      Get File               ffsuite/644_AI1400/644AIParamList_response.json
        ${ExpectedData}         evaluate               json.loads('''${VariableDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ARITHMETIC FLOAT Variable type in 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${AritVariableJson}     Get File               ffsuite/644_AI1400/variables/644Variables_Arithm_Float_req.json
        ${SetAVariableObj}      evaluate               json.loads('''${AritVariableJson}''')     json
        ${ActualData}           GetItem                ${SetAVariableObj}
        ${ArithVarDeatils}      Get File               ffsuite/644_AI1400/variables/644Variables_Arithm_Float_res.json
        ${ExpectedData}         evaluate               json.loads('''${ArithVarDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ARITHMETIC UI Variable type in 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${AritVariableJson}     Get File               ffsuite/644_AI1400/variables/644Variables_Arithm_UI_req.json
        ${SetAVariableObj}      evaluate               json.loads('''${AritVariableJson}''')     json
        ${ActualData}           GetItem                ${SetAVariableObj}
        ${ArithVarDeatils}      Get File               ffsuite/644_AI1400/variables/644Variables_Arithm_UI_res.json
        ${ExpectedData}         evaluate               json.loads('''${ArithVarDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for STRING Oct Variable type in 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${StringOctJson}        Get File               ffsuite/644_AI1400/variables/644Variables_String_OCTET_req.json
        ${SetStrOctObj}         evaluate               json.loads('''${StringOctJson}''')     json
        ${ActualData}           GetItem                ${SetStrOctObj}
        ${StrOctDeatils}        Get File               ffsuite/644_AI1400/variables/644Variables_String_OCTET_res.json
        ${ExpectedData}         evaluate               json.loads('''${StrOctDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ENUM Enum Variable type in 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${VarEnumJson}          Get File               ffsuite/644_AI1400/variables/644Variables_Enum_Enum_req.json
        ${SetEEnumObj}          evaluate               json.loads('''${VarEnumJson}''')     json
        ${ActualData}           GetItem                ${SetEEnumObj}
        ${EEnumDeatils}         Get File               ffsuite/644_AI1400//variables/644Variables_Enum_Enum_res.json
        ${ExpectedData}         evaluate               json.loads('''${EEnumDeatils}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify getVariable Feature(check for ENUM BitEnum Variable type in 0644-AI1400 ParamList Block)
        [Tags]                  Critical    FFVaribales
        ${VarBEnumJson}         Get File               ffsuite/644_AI1400/variables/644Variables_Enum_BitEnum_req.json
        ${SetBEnumObj}          evaluate               json.loads('''${VarBEnumJson}''')     json
        ${ActualData}           GetItem                ${SetBEnumObj}
        ${BEnumDetails}         Get File               ffsuite/644_AI1400/variables/644Variables_Enum_BitEnum_res.json
        ${ExpectedData}         evaluate               json.loads('''${BEnumDetails}''')     json
        ${Result}               Compare Json Objects   ${ActualData}      ${ExpectedData}
        
    Verify Disconnect Feature
       [Tags]                  Critical    FFDisconnect
       ${ExpectedDisconnect}   evaluate               json.loads(''' ${ExpectedStatus}''')     json
       ${ActualData}           Disconnect
       ${Result}               Compare Json Objects      ${ActualData}      ${ExpectedDisconnect}
       Should Be True          True==${Result}
       
       
     
