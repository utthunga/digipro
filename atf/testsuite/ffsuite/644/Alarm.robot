*** Settings ***
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections


Resource    ../suite_resource/common.robot

Suite Setup      Run Keywords    SetLangCodeEnKW
...              AND             SetProtocolTypeKW
...              AND             InitializeStackKW      "TRUE"
...              AND             ConnectDeviceKW        ${644Resource}/common/FF_HTK_Common_644_Rsc.json     "FALSE"
Suite Teardown   Run Keywords    DisconnectDeviceKW     "TRUE"
...              AND             ShutdownStackKW        "TRUE"

*** Variables ***
${SleepDuration}   0ms
${AlarmResource}        ${644Resource}/alarm

*** Test Cases ***
ATC001 - Verify for Alarm in Resource Block
    [Tags]            Critical        644         Alarms     AlarmList
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_ResourceBlock_Rsc.json
    ${GetAlarmListJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${ActualStatus}             get item                ${GetAlarmListJonObj["AlarmList"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetAlarmListJonObj["AlarmList"]["Response"]}
    ${GetAlarmListStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetAlarmListStatus}

ATC002 - Verify for Alarm GetSet Fault Notify
    [Tags]            Critical        644         Alarms     FaultState
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_ResourceBlock_Rsc.json
                                register callback       SetItem
    ${GetFaultStateJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetFaultStateJonObj["FaultState"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetFaultStateJonObj["FaultState"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetFaultStateJonObj["FaultState"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetFaultStateJonObj["FaultState"]["GetVar"]["Response"]}
    ${GetFaultStateStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFaultStateStatus}

ATC003 - Verify for Alarm GetSet Limit Notify
    [Tags]            Critical        644         Alarms     LimitNotify
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_ResourceBlock_Rsc.json
                                register callback       SetItem
    ${GetLimitNotifyJonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetLimitNotifyJonObj["LimNotify"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetLimitNotifyJonObj["LimNotify"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetLimitNotifyJonObj["LimNotify"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetLimitNotifyJonObj["LimNotify"]["GetVar"]["Response"]}
    ${GetLimitNotifyStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetLimitNotifyStatus}

ATC004 - Verify for Alarm GetSet Feature Selection
    [Tags]            Critical        644         Alarms     FeatureSelection
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_ResourceBlock_Rsc.json
                                register callback       SetItem
    ${GetFeatureSelJonObj}  	evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetFeatureSelJonObj["FeatureSelection"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetFeatureSelJonObj["FeatureSelection"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetFeatureSelJonObj["FeatureSelection"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetFeatureSelJonObj["FeatureSelection"]["GetVar"]["Response"]}
    ${GetFeatureSelStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetFeatureSelStatus}

ATC005 - Verify for Alarm in Transducer Block -HTK
    [Tags]            Critical        644         Alarms     AlarmList
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_TransducerBlock_Rsc.json
                                register callback       GetItem
    ${GetAlarmListJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetAlarmListJonObj["AlarmList"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetAlarmListJonObj["AlarmList"]["Response"]}
    ${GetAlarmListStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetAlarmListStatus}

ATC006 - Verify for Alarm in Transducer BlockAlarm Record -HTK
    [Tags]            Critical        644         Alarms     BlockAlarm
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_TransducerBlock_Rsc.json
                                register callback       GetItem
    ${GetBlockAlarmJonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetBlockAlarmJonObj["BlockAlarm"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetBlockAlarmJonObj["BlockAlarm"]["Response"]}
    ${GetBlockAlarmStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBlockAlarmStatus}

ATC007 - Verify for Alarm in Transducer UpdateEvent Record -HTK
    [Tags]            Critical        644         Alarms     UpdateEvent
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_TransducerBlock_Rsc.json
                                register callback       GetItem
    ${GetUpdateEventJonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${GetUpdateEventJonObj["UpdateEvent"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetUpdateEventJonObj["UpdateEvent"]["Response"]}
    ${GetUpdateEventStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetUpdateEventStatus}

ATC008 - Verify for Alarm in AI1400 Block -HTK
    [Tags]            Critical        644         Alarms     Get_Item
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_AI1400Block_Rsc.json
    ${GetAlarmListJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${ActualStatus}             get item                ${GetAlarmListJonObj["AlarmList"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetAlarmListJonObj["AlarmList"]["Response"]}
    ${GetAlarmListStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetAlarmListStatus}

ATC009 - Verify for Alarm GetSet Acknowledgement Option
    [Tags]            Critical        644         Alarms     AckOption
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_AI1400Block_Rsc.json
                                register callback       SetItem
    ${GetAckOptionJonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetAckOptionJonObj["AckOption"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetAckOptionJonObj["AckOption"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetAckOptionJonObj["AckOption"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetAckOptionJonObj["AckOption"]["GetVar"]["Response"]}
    ${GetAckOptionStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetAckOptionStatus}

ATC010 - Verify for Alarm GetSet Limit Notify
    [Tags]            Critical        644         Alarms     HighHighPriority
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_AI1400Block_Rsc.json
                                register callback       SetItem
    ${GetHHPriorityJonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetHHPriorityJonObj["HighHighPriority"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetHHPriorityJonObj["HighHighPriority"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetHHPriorityJonObj["HighHighPriority"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetHHPriorityJonObj["HighHighPriority"]["GetVar"]["Response"]}
    ${GetHHPriorityStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetHHPriorityStatus}

ATC011 - Verify for Alarm GetSet Feature Selection
    [Tags]            Critical        644         Alarms     HighHighLimit
    ${JsonFileContent}          get file                ${AlarmResource}/FF_644_Alarms_AI1400Block_Rsc.json
                                register callback       SetItem
    ${GetHHLimitJonObj}  	    evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualSetStatus}          set item                ${GetHHLimitJonObj["HighHighLimit"]["SetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     SetItem
    ${ExpectedSetStatus}        set variable            ${GetHHLimitJonObj["HighHighLimit"]["SetVar"]["Response"]}
    ${SetValStatus}             compare json object     ${ActualSetStatus}  ${ExpectedSetStatus}
    should be true              True==${SetValStatus}
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetHHLimitJonObj["HighHighLimit"]["GetVar"]["Request"]}
                                sleep                   ${SleepDuration}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${GetHHLimitJonObj["HighHighLimit"]["GetVar"]["Response"]}
    ${GetHHLimitStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetHHLimitStatus}