*** Settings ***
Documentation    Holds variable, keyword definition required by other test suites

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Library     robot.libraries.String

*** Variables ***
${HostResources}                ffsuite/host/resource
${HTKResource}                  ffsuite/htk/resource
${644Resource}                  ffsuite/644/resource
${SuperDDResource}              ffsuite/super_dd/resource
${IMAGE_COMPARATOR_COMMAND}     /usr/bin/convert-im6 __REFERENCE__ __TEST__ -metric RMSE -compare -format  "%[distortion]" info:

*** Keywords ***
SetProtocolTypeKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_SetProtocolType_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

InitializeStackKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_InitializeProtocol_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

ShutdownStackKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_ShutdownProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

SetLangCodeEnKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_Set_Language_Rsc.json
    ${SetLangJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set lang code           ${SetLangJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetLangJsonObj["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

ConnectDeviceKW
    [Arguments]                 ${RESOURCE_FILE}        ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${RESOURCE_FILE}
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Connect"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["Connect"]["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

Connect644SuperDDKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_Host_Connect_SuperDD_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["FF"]["Request"]}
    sleep       20s
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]['Response']}
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${CompStatus}

DisconnectDeviceKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/FF_DisConnectdevice_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetConnectJsonObj["FF"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj["FF"]["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}


ResetToDefaultKW
    [Arguments]                 ${RESOURCE_FILE}        ${ASSERT}="TRUE"
    ${JsonFileContent}          get file                FF_HTK_Common_644_Rsc.json
    ${ResetDefaultJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${StartMethodStatus}        get item                ${ResetDefaultJsonObj["ResetToDefault"]["MethodStart"]["Request"]}
    ${ExpectedStartStatus}      set variable            ${ResetDefaultJsonObj["ResetToDefault"]["MethodStart"]["Response"]}
    ${Result}                   compare json object     ${StartMethodStatus}     ${ExpectedStartStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}
    #${StartNotifyData}          retrieve item data
    #log                         ${StartNotifyData}
    #${AckNotifyData}            retrieve item data
    #log                         ${AckNotifyData}
    ${Status}                   unregister callback     GetItem
                                sleep   2s
###################################
    ${Status}                   register callback       SetItem
    ${ClickNextStatus}          set item                ${ResetDefaultJsonObj["ResetToDefault"]["Next_1"]["Request"]}
    ${ExpectedStatus}           set variable            ${ResetDefaultJsonObj["ResetToDefault"]["Next_1"]["Response"]}
    ${Result}                   compare json object     ${ClickNextStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}
    #${NotifyData}               retrieve item data
    #log                         ${NotifyData}
    ${Status}                   unregister callback     SetItem
                                sleep   2s
###################################
    ${Status}                   register callback       SetItem
    ${ResetypeStatus}           set item                ${ResetDefaultJsonObj["ResetToDefault"]["ResetDefaultType"]["Request"]}
    ${ExpectedStatus}           set variable            ${ResetDefaultJsonObj["ResetToDefault"]["ResetDefaultType"]["Response"]}
    ${Result}                   compare json object     ${ResetypeStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}
    #${DispNotifyData}           retrieve item data
    #log                         ${DispNotifyData}
    #${AckNotifyData}            retrieve item data
    #log                         ${AckNotifyData}
    ${Status}                   unregister callback     SetItem
                                sleep   40s
###################################
    ${Status}                   register callback       SetItem
    ${ClickNextStatus}          set item                ${ResetDefaultJsonObj["ResetToDefault"]["Next_1"]["Request"]}
    ${ExpectedStatus}           set variable            ${ResetDefaultJsonObj["ResetToDefault"]["Next_1"]["Response"]}
    ${Result}                   compare json object     ${ClickNextStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}
   # ${NotifyData}               retrieve item data
   # log                         ${NotifyData}
    ${Status}                   unregister callback     SetItem
                                sleep   2s
###################################
    ${Status}                   register callback       SetItem
    ${CompletedStatus}          set item                ${ResetDefaultJsonObj["ResetToDefault"]["Completed"]["Request"]}
    ${ExpectedStatus}           set variable            ${ResetDefaultJsonObj["ResetToDefault"]["Completed"]["Response"]}
    ${Result}                   compare json object     ${CompletedStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}
    #${NotifyData}               retrieve item data
    #log                         ${NotifyData}
    ${Status}                   unregister callback     SetItem

Compare Images
   [Arguments]      ${Reference_Image_Path}    ${Test_Image_Path}    ${Allowed_Threshold}
   ${TEMP}          Replace String     ${IMAGE_COMPARATOR_COMMAND}    __REFERENCE__     ${Reference_Image_Path}
   ${COMMAND}       Replace String     ${TEMP}    __TEST__     ${Test_Image_Path}
   Log              Executing: ${COMMAND}
   ${RC}            ${OUTPUT}=     Run And Return Rc And Output     ${COMMAND}
   Log              Return Code: ${RC}
   Log              Return Output: ${OUTPUT}
   ${RESULT}        Evaluate    ${OUTPUT} < ${Allowed_Threshold}
   Should be True   ${RESULT}
