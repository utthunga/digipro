*** Settings ***
Documentation    This test suite contains the test cases for Select Menu and Menu Navigation
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW     "TRUE"
...             AND             InitializeStackKW   "TRUE"
...             AND             ConnectDevice3051   "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW  "TRUE"
...             AND             ShutdownStackKW     "TRUE"



*** Variables ***
${3051H5MenuResources}              ${3051_H5_Resource}/menu
${3051H5VariableResources}          ${3051_H5_Resource}/variable

*** Keywords ***
SetScaledDataOption
    [Arguments]         ${arg}
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_SelectScaledDataOptions_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]${arg}}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

*** Test Cases ***
#Root menu fecthing is done by using connect command
########################################################################################################################
################################################### Menu  ##############################################################
########################################################################################################################
ATC001 - Verify user can fetch expected root menus upon successful Device connection
    [Tags]      Critical        Menu        Root_Menu
    [Setup]                     DisconnectDeviceKW
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051Menu_RootMenu_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${MenuJsonObj["Request"]}
    sleep       20s
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}


ATC002 - Verify user can fetch menu and Menu Items with the attributes (5 times)
    [Tags]       Critical        Menu       Menu_Items
    # Get Guided Setup menu and validate
    [Setup]                     register callback       GetItem
    [Teardown]                  unregister callback  GetItem
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_AllAtributes_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword    ${TestCount}      get item        ${MenuJsonObj["Request"][0]}
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    # Get Basic Setup menu and validate
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
    :for    ${index}    in range    0   9
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC003 - Verify user can Naviagte from Configuration menu-> Mannual Menu->Basic setup menu->range value menu
    [Tags]       Critical        Menu       Menu_Navigation
    # Get Configuration menu and validate
    [Setup]                     register callback       GetItem
    [Teardown]                  unregister callback  GetItem
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_Navigation_Rsc.Json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    # Get Manual menu and validate
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    # Get Basic Setup menu and validate
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][2]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][2]}
    :for    ${index}    in range    0   2
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    :for    ${index}    in range    3   6
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}

    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    # Get range value menu and validate
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][3]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][3]}
    :for    ${index}    in range    0   2
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

########################################################################################################################
################################################ Menu Error Condition ##################################################
########################################################################################################################
#TODO:ATF issue
#ATC001 - Verify user can not fetch menu with invalid itemID-Error
#    [Tags]    Non-Critical       menu
#    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_Invalid_Menu_ItemID_Rsc.json
#    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
#    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
#    ${MenuCompStatus}           compare json object     ${ExpectedStatus}     ${ExpectedStatus}
#    should be true              True==${MenuCompStatus}

##TODO[CPM_Validation]: It is crashing now, verify after the bug fix (crash)
#ATC002 - Verify user can not fetch menu with invalid itemID value in string-Error
#    [Tags]     Non-Critical        Menu
#    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_Invalid_Menu_ItemID_Rsc.json
#    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
#    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
#    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${MenuCompStatus}


#ATC003 - Verify user can not fetch menu without itemID attributes-Error
#    [Tags]     Non-Critical       Menu
#    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_Invalid_Menu_ItemID_Rsc.json
#    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${MenuJsonObj["Request"][2]}
#    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][2]}
#    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${MenuCompStatus}

########################################################################################################################
################################################ Menu Validity Cases ###################################################
########################################################################################################################
ATC001 - Verify user can view the menu when the validity condition is met (Manual Setup->Scaled variable(17320))
    [Tags]  Critical      Menu        Menu_Validity
    [Setup]     SetScaledDataOption      [1]
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_ManualsetupValidity_ScaledVariable_Rsc.json
                                register callback       GetItem
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_ScaledVariableMenuValidity_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
    :for    ${Index}    in range    0   4
        \   Set to dictionary   ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify user cannot view the menu when the validity condition is not met (Manual Setup->scaled variable(17319))
    [Tags]  Critical      Menu        Menu_Validity
    [Setup]     SetScaledDataOption      [0]
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_ManualsetupValidity_ScaledVariable_Rsc.json
                                register callback       GetItem
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    ${JsonFileContent}          get file                ${3051H5MenuResources}/HART_3051_Menu_ScaledVariableMenuValidity_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
    :for    ${index}    in range    0   3
        \   Set to dictionary   ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}


