*** Settings ***
Documentation    This test suite contains the test cases for Variable
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW         "TRUE"
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDevice3051       "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${3051H5MenuResources}              ${3051_H5_Resource}/menu
${3051H5VariableResources}          ${3051_H5_Resource}/variable

*** Keywords ***
ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3501_Variable_WriteProtectStatus_Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response']}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}


*** Test Cases ***
########################################################################################################################
################################################### Bit enumeration  ###################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type BIT-ENUMERATION within the BIT-ENUMERATION range
    [Tags]  Critical    Variable      Bit_Enumeration
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type BIT-ENUMERATION with desire attributes
    [Tags]  Critical    Variable    Bit_Enumeration

    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type BIT-ENUMERATION after subcription
    [Tags]  Critical    Variable    Bit_Enumeration
    [Setup]   Run Keywords    DisconnectDevice
    ...             AND       ConnectDevice3051
    ...             AND       Subcription       ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]:crash
#ATC004 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Bit_Enumeration
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    #                            register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write wrong VarValue to the variable type BIT-ENUMERATION- ERROR
    [Tags]  Non-Critical    Variable    Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#taking value before decimal
ATC006 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(float)
    [Tags]  Non-Critical   Variable     Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]
#ATC007 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value (negative) -ERROR
#    [Tags]  Non-Critical    Variable       Bit_Enumeration
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Date / Time Variable  ##############################################
########################################################################################################################
ATC001 - Verify user can write valid Date value to the variable
    [Tags]  Critical    Variable    Date
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get valid Date value for the variable
    [Tags]  Critical    Variable    Date

    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type DATE after subcription
    [Tags]  Critical    Variable    Date
    [Setup]   Run Keywords    DisconnectDevice
    ...             AND       ConnectDevice3051
    ...             AND       Subcription       ${3051H5VariableResources}/HART_3051_Variable_DATE_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[Jira Issue]:verify after the bug fix,Now it is crashing.
#TODO[CPM_Validation]
#ATC004 - Verify user can write Invalid value to the variable type DATE(float value)-ERROR
#    [Tags]  Non-Critical    Variable   Date
#    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#TODO[CPM_Validation]
#ATC005 - Verify user can write Invalid value to the variable type DATE(string value)-ERROR
#    [Tags]  Non-Critical    Variable   Date
#    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#TODO[CPM_Validation]
#ATC006 - Verify user can write Invalid value to the variable type DATE(Negative value)-ERROR
#    [Tags]  Non-Critical    Variable   Date
#    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Enumeration Variable  ##############################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type ENUMERATION within the ENUMERATION range
    [Tags]  Critical    Variable    Enumeration
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type ENUMERATION with desire attributes
    [Tags]  Critical    Variable       Enumeration

    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type ENUMERATION after subcription
    [Tags]  Critical    Variable    Enumeration
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice3051
    ...             AND       Subcription           ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]
#ATC004 - Verify user can write value to the variable type ENUMERATION with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Enumeration
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#
ATC005 - Verify user can write wrong VarValue to the variable type ENUMERATION- ERROR
    [Tags]  Non-Critical    Variable    Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can write value to the variable type ENUMERATION with Invalid value(float)
    [Tags]  Non-Critical    Variable    Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]
#ATC007 - Verify user can write value to the variable type ENUMERATION with Invalid value (negative) -ERROR
#    [Tags]  Non-Critical    Variable   Enumeration
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_ENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Float Variable  ####################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type FLOAT within the length given and allowed range
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item               ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type FLOAT with desire attributes
    [Tags]  Critical    Variable    Float

    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type FLOAT after subcription
    [Tags]  Critical    Variable    Float
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDeviceKW          ${HostResources}/HART_Host_3051_Connect_Device_Rsc.json     "TRUE"
    ...             AND       Subcription       ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]
#ATC004 - Verify user can write value to the variable type FLOAT with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Float
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback       SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write string to the variable type FLOAT less than the min allowed range(Negative value)
    [Tags]  Non-Critical   Variable    Float
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can write integer value to the variable type FLOAT
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}            get file               ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}            evaluate                json.loads('''${JsonFileContent}''')   json
                                  register callback       SetItem
    ${ActualStatus}               set item                 ${VariableJsonObj["Request"][3]}
                                  unregister callback      SetItem
    ${ExpectedStatus}             set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                True==${VariableCompStatus}

########################################################################################################################
################################################### PACKED ASCII Variable  #############################################
########################################################################################################################
ATC001 - Verify user can write value to the variable within the length given and allowed characters
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type PACKED_ASCII with desire attributes
    [Tags]  Critical    Variable    Packed_ASCII

    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type PACKED_ASCII after subcription
    [Tags]  Critical    Variable    Packed_ASCII
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDeviceKW          ${HostResources}/HART_Host_3051_Connect_Device_Rsc.json     "TRUE"
    ...             AND       Subcription              ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate               json.loads('''${JsonFileContent}''')   json
                                register callback      GetItem
    ${ActualStatus}             get item               ${VariableJsonObj["Request"][0]}
                                unregister callback    GetItem
    ${ExpectedStatus}           set variable           ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object    ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify user can write string to the variable type PACKED_ASCII more than the length allowed
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]:
#ATC006 - Verify user can write value to the variable type PACKED_ASCII with Invalid character -ERROR
#    [Tags]  Non-Critical    Variable   Packed_ASCII
#    ${JsonFileContent}          get file                ${644VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Read Only Variable  ################################################
########################################################################################################################
ATC001 - Verify user can get parameter for Read Only Parameter
    [Tags]  Critical    Variable    Read_only
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UniversalRevision_ReadOnlyParameter_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user cannot set parameter for Read Only Parameter
    [Tags]  Critical    Variable    Read_only
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UniversalRevision_ReadOnlyParameter_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Single Bit Enum  ###################################################
########################################################################################################################
ATC001 - Verify user can fetch single BIT-ENUMERATION variable in menu
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_Bit_Enumuration_Fetch_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
        :for    ${index}    in range    0   4
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can write VarValue to the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can fetch value from the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify user can get device value for variable type single BIT-ENUMERATION after subcription
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    [Setup]   Run Keywords    DisconnectDevice
    ...             AND       ConnectDevice
    ...             AND       Subcription           ${3051H5VariableResources}/HART_3051_Variable_Single_bit_enumeration_subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write to the variable type single BIT-ENUMERATION with wrong VarBitIndex-
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can fetch value from the variable type single BIT-ENUMERATION with wrong VarBitIndex
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
#ATC007 - Verify user can write wrong(string) VarValue to the variable type single BIT-ENUMERATION-ERROR
#    [Tags]  Critical    Variable       Single_Bit_Enumeration
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Single_BitEnumuration_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Unsigned Integer  ##################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type UNSIGNED_INTEGER within the length given and allowed range
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type UNSIGNED_INTEGER with desire attributes
    [Tags]  Critical    Variable    Unsigned_Integer

    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type UNSIGNED_INTEGER after subcription
    [Tags]  Critical    Variable    Unsigned_Integer
    [Setup]   Run Keywords    DisconnectDevice
    ...             AND       ConnectDevice
    ...             AND       Subcription       ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]:
#ATC004 - Verify user can write value to the variable with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Unsigned_Integer
#    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write string to the variable type UNSIGNED_INTEGER more than the max allowed range
    [Tags]  Non-Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]:crash
#ATC006 - Verify user can write string to the variable less than the min allowed range(Negative value)
#    [Tags]  Non-Critical    Variable   Unsigned_Integer
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC007 - Verify user can write Float value to the variable type UNSIGNED_INTEGER type UNSIGNED_INTEGER
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Variable Error  ####################################################
########################################################################################################################
ATC001 - Verify the behavior when the user tries to write the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify the behavior when the user tries to fetch the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify the behavior when the user tries to write the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify the behavior when the user tries to fetch the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
#TODO[CPM_Validation]
#ATC005 - Verify the behavior when the user tries to write the VARIABLE with string as Item ID -ERROR
#
#    [Tags]  Non-Critical    Variable
#    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC006 - Verify the behavior when the user tries to fetch the VARIABLE with string as Item ID -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC007 - Verify the behavior when the user tries to write the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC008 - Verify the behavior when the user tries to fetch the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC009 - Verify the behavior when the user tries to write the VARIABLE without ItemType attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC010 - Verify the behavior when the user tries to fetch the VARIABLE without ItemType attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][3]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC011 - Verify the behavior when the user tries to write the VARIABLE with invalid ItemType (Integer) attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC012 - Verify the behavior when the user tries to fetch the VARIABLE with invalid ItemType (Integer) attribute -ERROR

    [Tags]  Non-Critical   Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC013 - Verify the behavior when the user tries to fetch the VARIABLE with wrong "ItemInDD"=FALSE -ERROR
    [Tags]  Non-Critical   Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
ATC014 - Verify the behavior when the user tries to fetch the VARIABLE without "ItemInDD" attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC015 - Verify the behavior when the user tries to write the value without VarValue attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_WithoutVarValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC0016 - Verify the user can write value to the variable when subcription is in progress.
    [Tags]  Non-Critical          Variable        Subcription
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Subcription_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data      ProcessSubscription
    ${JsonFile}                 get file                ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFile}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${VariableCompStatus}


ATC0017 - Verify the user can fetch value from the variable when subcription is in progress.
    [Tags]  Non-Critical          Variable        Subcription
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_FLOAT_Subcription_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data      ProcessSubscription
    ${JsonFile}                 get file                ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFile}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${VariableCompStatus}


########################################################################################################################
################################################### Variable Releation  ################################################
########################################################################################################################
ATC001 - Verify user can get unit relation of Variable after modifying and subscribing.
    [Tags]  Critical    UnitRelation  subcription
    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_Publish_Variable_PressureUnit_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Setvar"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["Setvar"]['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

    ${SubscriptionJsonObj}      set variable            ${VariableJsonObj}
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}
    should be true              True==${NotifyStatus}
                                register callback       GetItem
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_RangeValue_UpperRange_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


########################################################################################################################
################################################### Write Protection Releation  ########################################
########################################################################################################################
ATC001 - Verify user cannot set varible when Write Protect Status ON
    [Tags]  Critical    WriteProtectMode Variable
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][1]
    [Teardown]  unregister callback  SetItem
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can set variable when Write Protect Status OFF
    [Tags]  Critical    WriteProtectMode  Variable
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][0]
    [Teardown]  unregister callback     SetItem
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3051_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
