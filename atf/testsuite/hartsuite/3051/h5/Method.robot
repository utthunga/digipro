*** Settings ***
Documentation    This test suite contains the test cases for Method
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW         "TRUE"
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDevice3051       "TRUE"
...             AND             ChangeAddressKW          ${HostResources}/HART_Host_Change_Address_Rsc.json              [0]
...             AND             DisconnectDeviceKW      "TRUE"
...             AND             ConnectDeviceAddressKW   [0]        "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${3051H5MethodResources}                ${3051_H5_Resource}/method
${3051H5VariableResources}              ${3051_H5_Resource}/variable

*** Keywords ***
ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${3051H5VariableResources}/HART_3501_Variable_WriteProtectStatus_Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response']}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}
                                unregister callback     SetItem


*** Test Cases ***
########################################################################################################################
################################################### Method Execution ###################################################
########################################################################################################################
ATC001 - Verify the execution of Zero Trim method and verify help option
    [Tags]          critical   Method       Non-Multidrop
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_ZeroTrim_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      40
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback  GetItem
     #
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    #
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    #
    ${GetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${GetItemStatus}
    :for    ${Index}    in range    0   3
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    #
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback  SetItem

ATC002 - Verify the execution of Analog Output Calibration method
    [Tags]          critical   Method   Non-Multidrop
    ${JsonFileContentME}        get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContentME}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   run keyword if               ${Index}==1      set to dictionary        ${MEJsonObj["Notification"][${NotifyIndex}]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback  GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][${NotifyIndex}]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][${NotifyIndex}]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][5]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][${NotifyIndex}]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][6]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][${NotifyIndex}]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback  SetItem

ATC003 - Verify the execution of loop test method(START,ACK,SELECTION,GETVALUE,DISPLAY,COMPLETED)
    [Tags]          critical   Method       Non-Multidrop
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    #option1
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_performloopTestOption1_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

     #option2
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_PerformLoopTestOption2_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

      #option3
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_performLoopTestOption3_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]}   Message=${NotificationData["ItemInfo"]["Message"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

     #option4
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    #option5
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_PerformLoopTestOption5_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          Set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem

########################################################################################################################
################################################### Method Abort Condition #############################################
########################################################################################################################
ATC001 - Verify the User Abort of Analog Output Calibration method in 1th (Selection enumeration) screen
    [Tags]          critical   Method  Manual_Abort     Non-Multidrop
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${JsonAbort}                get file                ${3051H5MethodResources}/HART_3051_Method_AbortMethod_Rsc.json
    ${AbortJsonObj}             evaluate                json.loads('''${JsonAbort}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   run keyword if               ${Index}==1      set to dictionary        ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback       GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${AbortJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${AbortJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${AbortJsonObj["Notification"][0]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback       SetItem
ATC002 - Verify the User Abort of Analog Output Calibration method in 3th (ACK) screen
    [Tags]          critical   Method      Manual_Abort     Non-Multidrop
    ${JsonFileContentME}        get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContentME}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}        retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   run keyword if               ${Index}==1      set to dictionary        ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback       GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonAbort}                get file                ${3051H5MethodResources}/HART_3051_Method_AbortMethod_Rsc.json
    ${AbortJsonObj}             evaluate                json.loads('''${JsonAbort}''')   json
    ${SetItemResponse}          set item                ${AbortJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${AbortJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${AbortJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${AbortJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${AbortJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${AbortJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback       SetItem


ATC003 - Verify the User Abort of Analog Output Calibration method in 5th (Getvalue-Float) screen
     [Tags]          critical   Method      Manual_Abort     Non-Multidrop
    ${JsonFileContentME}        get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContentME}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}        retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   run keyword if               ${Index}==1      set to dictionary        ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback       GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary        ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonAbort}                get file                ${3051H5MethodResources}/HART_3051_Method_AbortMethod_Rsc.json
    ${AbortJsonObj}             evaluate                json.loads('''${JsonAbort}''')   json
    ${SetItemResponse}          set item                ${AbortJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${AbortJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${AbortJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback       SetItem

ATC004 - Verify the User Abort of Analog Output Calibration method in 9th (Selection) screen
     [Tags]          critical   Method         Manual_Abort     Non-Multidrop
    ${JsonFileContentME}        get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContentME}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}        retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   run keyword if               ${Index}==1      set to dictionary        ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback       GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][5]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}

    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][6]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonAbort}                get file                ${3051H5MethodResources}/HART_3051_Method_AbortMethod_Rsc.json
    ${AbortJsonObj}             evaluate                json.loads('''${JsonAbort}''')   json
    ${SetItemResponse}          set item                ${AbortJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${AbortJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${AbortJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem

ATC005 -Verify method gets aborted when user sets invalid value for GETVALUE notify message (perform loop test method)
    [Tags]          Common   Method     Process_Abort     Non-Multidrop
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}

    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}        retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback       GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    # checking for error abort
    ${NotificationData}        retrieve item data       SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][3]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    # exiting the loop
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_AbortLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          Set item                ${MEJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem


ATC006 - Verify user can execute the method when Write Lock is enabled in the device
    [Tags]        critical    Method       Process_Abort     Non-Multidrop
    [Setup]                     ChangeWriteProtectMode  ["Request"][1]
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_AbortWhenWriteProtect_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      30
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem
    [Teardown]                  ChangeWriteProtectMode  ["Request"][0]


########################################################################################################################
################################################### Method Error Condition #############################################
########################################################################################################################
#TODO[CPM validation]
#ATC001 - Verify the behavior when user tries to perform Disconnect device when method execution is running
#    [Tags]          Non-Critical   Method  Non-Multidrop
#    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_ZeroTrim_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#    [Teardown]                  DisconnectDevice

#TODO[StateMachine]:To Verify this test case once state machine is implemented
#ATC002 - Verify the behavior when user tries to perform ShutDown device when method execution is running
#    [Tags]          Non-Critical   method      Non-Multidrop
#    [Setup]                     ConnectDevice
#    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_ZeroTrim_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#    [Teardown]                  ShutDownProtocol


#TODO[StateMachine]:To Verify this test case once state machine is implemented
#ATC003 - Verify the behavior when user tries to switch protocol when method execution is running
#    [Tags]          Non-Critical    method        Non-Multidrop
#    [Setup]  Run Keywords       SetProtocol     AND     InitializeProtocol
#    ...             AND             ConnectDevice
#    ${JsonFileContentME}        get file                ${3051H5MethodResources}/HART_3051_Method_AnalogOutputCalibration_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContentME}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                        unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    #Setting FF protocal when Hart is set and method is running
#    ${JsonFileContent}          get file                ${HostResources}/AT005_setprotocol_HART_then_set_FF_protocol.json
#    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
#    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetProtocolCompStatus}
#                                unregister callback     SetItem
#    #[Teardown]                  DisconnectDevice


ATC004 - Verify user can execute method with incorrect item id - ERROR
    [Tags]          Non-Critical    Method      Common
    #[Setup]                     ConnectDevice3051
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_InvalidItemID_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
                                unregister callback     GetItem

#TODO[CPM_Validation]:crash
#ATC005 - Verify user can execute method with item id value in string - ERROR
#    [Tags]          Non-Critical    Method     Common
#    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_InvalidItemID_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${GetItemStatus}
#                                unregister callback     GetItem

ATC006 - Verify user can proceed with the execution with invalid request during method execution phase - ERROR
    [Tags]          Non-Critical   Method       Common
    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_InvalidRequest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
    should be true              True==${GetItemStatus}
                                unregister callback     GetItem
#TODO[StateMachine]:Ok response
#ATC007 -Verify user can trigger two methods simultaneously, if a method is already in progress- ERROR
#    [Tags]          Non-Critical    Method          Non-Multidrop
#   ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_ZeroTrim_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_TwoMethodSimultaneously_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          get item                ${MEJsonObj["Request"]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
#    should be true              True==${SetItemStatus}
#                                unregister callback     GetItem

#Note :Response is "Ok" not "Error"(having doubt in funcianality needs to be clear)
#ATC008 - Verify user can set a value for GetItem and SetItem Variable type when method execution is running -ERROR
#    [Tags]          Non-Critical    Method      Non-Multidrop
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
#                                register callback       SetItem
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#                                unregister callback     SetItem
#    ${JsonFileContent}          get file               ${3051H5VariableResources}/HART_3051_Variable_PACKED_ASCII_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#                                unregister callback     GetItem
#    [Teardown]                  Run Keywords             DisconnectDevice
#          ...                   AND                      ConnectDevice

########################################################################################################################
############################################ Multidrop Method Execution ################################################
########################################################################################################################
ATC001 - Verify the abort of perform loop test method in mutlidrop mode
    [Tags]          critical   Method       Multidrop
    [Setup]         Run Keywords        ChangeAddressKW             ${HostResources}/HART_Host_Change_Address_Rsc.json              [3]
    ...             AND                 DisconnectDeviceKW
    ...             AND                 ConnectDevice3051      "TRUE"

    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${JsonFileContent}          get file                ${3051H5MethodResources}/HART_3051_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][4]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotifyIndex}              convert to integer      5
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][7]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem



