Pre-Requisites for running HartSuits(host_suites,644_h5_suites,644_h7_suites,3051_h5_suites) at one session.

1.Three devices is needed in network to run host_suites,644_h5_suites,644_h7_suites,3051_h5_suites.
2.For Running Hart_suite in a single session arguments file "Main.txt" is used.
3.All three devices needs to be in different address,
       a.To run host suites, 644_h7, 644_h5, 3051_h5 devices should be in address 1, 2, 3 respectively
       b.To run 644_h7_suites, 644_h7 device needs to be in address 1
       c.To run 644_h5_suites- 644_h5 device needs to be in address 2
       d.To run 3051_h5_suites- 3051-h5 device needs to be in address 3
4.All devices needs to be in unlock write and unlock HART status.
5.For Scanning.robot, need to update the json according for device id,tag name. Below is the json which needs to be updated.
  1.HART_Host_Scan_Rsc.json
  2.HART_Host_Start_Scan_Error_Rsc.json
  3.HART_Host_Stop_Scan_Error_Rsc.json


6.For running Host_suites alone 3 devices is needed in network otherwise some test case may be fail
  To avoid this needs to exclude tag Multidrop so that hosts_suites will run only with one devices in network with device address 0.
