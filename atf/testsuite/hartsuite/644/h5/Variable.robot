*** Settings ***
Documentation  This test suite contains the test case for variable data type
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW       "TRUE"
...             AND             SetLangCodeEnKW         "TRUE"
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDevice644H5      "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${644H5VariableResources}       ${644_H5_Resource}/variable

*** Keywords ***
SetSensor1typevalue
    [Arguments]         ${arg}
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_SetSensor1Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]${arg}}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_WriteProtectMode_Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response']}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}

*** Test Cases ***
########################################################################################################################
################################################### Bit enumeration  ###################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type BIT-ENUMERATION within the BIT-ENUMERATION range
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type BIT-ENUMERATION with desire attributes
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type BIT-ENUMERATION after subcription
    [Tags]  Critical    Variable
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription              ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(string) -ERROR
#    [Tags]  Non-Critical   Variable
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback  SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write wrong VarValue to the variable type BIT-ENUMERATION- ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(float) -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC007 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value (negative) -ERROR
#    [Tags]  Non-Critical    Variable
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


########################################################################################################################
################################################### Date / Time Variable  ##############################################
########################################################################################################################
ATC001 - Verify user can write valid DATE value to the variable
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_Date_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword           ${TestCount}      set item                ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type DATE and its attributes
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_Date_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    repeat keyword           ${TestCount}      get item                ${VariableJsonObj["Request"][0]}
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC003 - Verify user can get device value for variable type DATE after subcription
    [Tags]  Critical    Variable
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription              ${644H5VariableResources}/HART_644_Variable_Date_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Date_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    repeat keyword           ${TestCount}      get item                ${VariableJsonObj["Request"][0]}
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write Invalid value to the variable type DATE(float)-ERROR
#    [Tags]  Non-Critical    Variable
#    ${JsonFileContent}          get file                ${644VariableResources}/HART_644_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#        repeat keyword           ${TestCount}      set item                ${VariableJsonObj["Request"][2]}
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC005 - Verify user can write Invalid value to the variable type DATE(string)-ERROR
#    [Tags]  Non-Critical   Variable     Date
#    ${JsonFileContent}          get file                ${644VariableResources}/HART_644_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC006 - Verify user can write Invalid value to the variable type DATE(negative)-ERROR
#    [Tags]  Non-Critical   Variable     Date
#    ${JsonFileContent}          get file                ${644VariableResources}/HART_644_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Enumeration Variable  ##############################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type ENUMERATION within the ENUMERATION range
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword           ${TestCount}      set item                ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
ATC002 - Verify user can get item for Variable Verify user can get device value for variable type ENUMERATION after subcription with desire attributes
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    repeat keyword           ${TestCount}      get item                ${VariableJsonObj["Request"][0]}
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
#
ATC003 - Verify user can get device value for VARIABLE Verify user can get device value for variable type ENUMERATION after subcription after subcription
    [Tags]  Critical    Variable
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription              ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC004 - Verify user can write value to the variable type ENUMERATION with Invalid value(string) -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write wrong VarValue to the variable type ENUMERATION- ERROR
    [Tags]  Non-Critical   Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can write value to the variable type ENUMERATION with Invalid value(float) -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO: ATF Test case hangs
#ATC007 - Verify user can write value to the variable type ENUMERATION with Invalid value (negative) -ERROR
#    [Tags]  Non-Critical    Variable
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC008 - Verify user can get sensor_1_unit_enumeration value Ohms when Ohms is set in sensor_1_type variable(conditionalvalidity)
    [Tags]  Critical    Variable
    SetSensor1typevalue         [0]
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Getsensor1unitValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj["Response"][0]}
                                unregister callback     GetItem
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC009 - Verify user can get sensor_1_unit_enumeration value-degC,degF,degR,Kelvin and Ohms when sensor_1_type variable is less and equal to "PT200 A3916"(conditionalvalidity)
    [Tags]  Critical    Variable
    SetSensor1typevalue         [2]
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Getsensor1unitValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
                               unregister callback      GetItem
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Float Variable  ####################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type FLOAT within the length given and allowed range
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}      set item         ${VariableJsonObj["Request"][1]}

    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type FLOAT with desire attributes
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type FLOAT after subcription
    [Tags]  Critical    Variable    Float
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription              ${644H5VariableResources}/HART_644_Variable_FLOAT_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write value to the variable type FLOAT with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Float
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


ATC005 - Verify user can write value to the variable type FLOAT less than the min allowed range(Negative value)
    [Tags]  Non-Critical   Variable     Float
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can write integer value to the variable type FLOAT
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}            get file               ${644H5VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}            evaluate                json.loads('''${JsonFileContent}''')   json
                                  register callback       SetItem
    ${ActualStatus}               set item                 ${VariableJsonObj["Request"][3]}
                                  unregister callback     SetItem
    ${ExpectedStatus}             set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                True==${VariableCompStatus}

########################################################################################################################
################################################### Index Variable  ####################################################
########################################################################################################################
#TODO:BustMode is not Implement for setItem verify Index(second variable) after bust mode implimentation

#ATC001 - Verify user can write value to the variable type INDEX within the index range
#    [Tags]  Critical    Variable    Index
#    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type INDEX and its attributes
    [Tags]  Critical    Variable    Index
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}       VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC003 - Verify user can get device value for variable after subcription
#    [Tags]  Critical    Variable    Index
#    [Setup]   Run Keywords    DisconnectDevice
#    ...             AND       ConnectDevice
#    ...             AND       Subcription       ${644H5VariableResources}/HART_644_Variable_INDEX_Subcription_Rsc.json
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write value to the variable type INDEX with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Index
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

#ATC005 - Verify user can write wrong VarValue to the variable type INDEX- ERROR
#    [Tags]  Non-Critical    Variable    Index
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
##TODO[CPM_Validation]:
#ATC006 - Verify user can write value to the variable type INDEX with Invalid value(float) -ERROR
#    [Tags]  Non-Critical   Variable      Index
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
##TODO[CPM_Validation]:
##ATC007 - Verify user can write value to the variable type INDEX with Invalid value (negative) -ERROR
##    [Tags]  Non-Critical    Variable        Index
##    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_INDEX_Rsc.json
##    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
##                                register callback       SetItem
##    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
##                                unregister callback     SetItem
##    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
##    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

########################################################################################################################
################################################### PACKED ASCII Variable  #############################################
########################################################################################################################
ATC001 - Verify user can write value to the variable within the length given and allowed characters
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword      ${TestCount}        set item        ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type PACKED_ASCII and its attributes
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type PACKED_ASCII after subcription
    [Tags]  Critical    Variable    Packed_ASCII
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription               ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC004 - Verify user can write string to the variable type PACKED_ASCII more than the length allowed
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC006 - Verify user can write value to the variable with Invalid character -ERROR
#    [Tags]  Non-Critical    Variable   Packed_ASCII
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Read Only Variable  ################################################
########################################################################################################################
ATC001 - Verify user cannot set parameter for Read Only Parameter
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_ReadOnlyParamater_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can get parameter for Read Only Parameter
    [Tags]  Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_ReadOnlyParamater_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}    VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


########################################################################################################################
################################################### Single Bit Enumeration #############################################
########################################################################################################################
ATC001 - Verify user can fetch single BIT-ENUMERATION variable in menu
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_Bit_Enumuration_Fetch_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
        :for    ${index}    in range    0   5
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can write VarValue to the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can fetch value from the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify user can get device value for variable type single BIT-ENUMERATION after subcription
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription               ${644H5VariableResources}/HART_644_Variable_Single_bit_enumeration_subcription_Rsc.json
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write to the variable type single BIT-ENUMERATION with wrong VarBitIndex
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can fetch value from the variable type single BIT-ENUMERATION with wrong VarBitIndex
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC007 - Verify user can write wrong(string) VarValue to the variable type single BIT-ENUMERATION-ERROR
#    [Tags]  Critical    Variable       Single_Bit_Enumeration
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Unsigned Integer ###################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type UNSIGNED_INTEGER within the length given and allowed range
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify user can get item for variable type UNSIGNED_INTEGER and its attributes
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify user can get device value for variable type UNSIGNED_INTEGER after subcription
    [Tags]  Critical    Variable        Unsigned_Integer
    [Setup]   Run Keywords    DisconnectDeviceKW
    ...             AND       ConnectDevice644H5      "TRUE"
    ...             AND       Subcription               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Subcription_Rsc.json
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write value to the variable type UNSIGNED_INTEGER with Invalid value(string) -ERROR
#    [Tags]  Non-Critical    Variable   Unsigned_Integer
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write string to the variable type UNSIGNED_INTEGER more than the max allowed range-ERROR
    [Tags]  Non-Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
#communicator crash
#ATC006 - Verify user can write string to the variable type UNSIGNED_INTEGER less than the min allowed range(Negative value)-ERROR
#    [Tags]  Non-Critical    Variable   Unsigned_Integer
#    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC007 - Verify user can write Float value to the variable type UNSIGNED_INTEGER
    [Tags]  Non-Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Variable Error #####################################################
########################################################################################################################
ATC001 - Verify the behavior when the user tries to write the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify the behavior when the user tries to fetch the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][5]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC003 - Verify the behavior when the user tries to write the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify the behavior when the user tries to fetch the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC005 - Verify the behavior when the user tries to write the VARIABLE with string as Item ID -ERROR
#
#    [Tags]  Non-Critical    Variable
#    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC006 - Verify the behavior when the user tries to fetch the VARIABLE with string as Item ID -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC007 - Verify the behavior when the user tries to write the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC008 - Verify the behavior when the user tries to fetch the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC009 - Verify the behavior when the user tries to write the VARIABLE without ItemType attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC010 - Verify the behavior when the user tries to fetch the VARIABLE without ItemType attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][3]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC011 - Verify the behavior when the user tries to write the VARIABLE with invalid ItemType (Integer) attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC012 - Verify the behavior when the user tries to fetch the VARIABLE with invalid ItemType (Integer) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC013 - Verify the behavior when the user tries to fetch the VARIABLE with wrong "ItemInDD"=FALSE -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC014 - Verify the behavior when the user tries to fetch the VARIABLE without "ItemInDD" attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC015 - Verify the behavior when the user tries to fetch the VARIABLE with invalid "ItemInDD"(Integer) -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC016 - Verify the behavior when the user tries to write the value without VarValue attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_WithoutVarValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Variable Releation #################################################
########################################################################################################################
ATC001 - Verify Refresh relation is applied for Variable through subcription
    [Tags]  Critical    RefreshRelation  subcription

    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_RefreshRelation_sensor1Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Setvar"]["Request"]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj["Setvar"]['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
                                unregister callback       SetItem
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_Publish_Variable_sensor1Unit_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    should be true              True==${NotifyStatus}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback       ProcessSubscription
    should be true              True==${NotifyStatus}
                                register callback     GetItem
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_RefreshRelation_Sensor1Unit_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC002 - Verify UNIT relation is applied for variable through subcription
    [Tags]  Critical    UnitRelation  subcription

    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_unitRelation_sensor1Unit_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Setvar"]["Request"]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj["Setvar"]['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
                                unregister callback     SetItem
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_publish_variable_upperRange_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]["ItemList"][0]}    ItemInfo=${NotifyData["ItemList"][0]["ItemInfo"]}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    should be true              True==${NotifyStatus}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}
                                register callback     GetItem
    ${JsonFileContent}          get file               ${644H5VariableResources}/HART_644_Variable_UnitRelation_sensorLimit_UpperRange_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    set to dictionary           ${VariableJsonObj["Response"]["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}   VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


########################################################################################################################
################################################### Write Potection ####################################################
########################################################################################################################
TC001 - Verify user cannot set varible when Write Protect Mode ON
    [Tags]  Critical   WriteProtectMode
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][1]
    [Teardown]  unregister callback  SetItem
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    repeat keyword      ${TestCount}     set item       ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can set variable when Write Protect Mode OFF
    [Tags]  Critical   WriteProtectMode
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][0]
    [Teardown]  unregister callback  SetItem
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

