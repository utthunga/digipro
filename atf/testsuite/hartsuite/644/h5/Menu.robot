*** Settings ***
Documentation    This test suite contains the test cases for Select Menu and Menu Navigation
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW         "TRUE"
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDevice644H5      "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${644H5MenuResources}       ${644_H5_Resource}/menu

*** Keywords ***
SetSensor1Type
    [Arguments]     ${arg}
    ${JsonFileContent}          get file                ${644H5MenuResources}/${arg}
                                register callback       SetItem
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${MEJsonObj["Request"]}

    ${ExpectedStatus}           set variable            ${MEJsonObj['Response']}
    ${GetItemStatus}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetItemStatus}

*** Test Cases ***
########################################################################################################################
################################################### Menu  ##############################################################
########################################################################################################################
 #Root menu is validated in Connect.robot suite
ATC001 - Verify user can fetch expected root menus upon successful Device connection
    [Tags]      Critical        Menu        Root_Menu
    [Setup]     run keyword     DisconnectDeviceKW
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Get_root_Menu_item_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${MenuJsonObj["Request"]}
    sleep       20s
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify user can fetch the Menu and Menu Items with the attributes (5 times)
    [Tags]      Critical        Menu    Menu_Items
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_ItemAttribute_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    repeat keyword              ${TestCount}  get item            ${MenuJsonObj["Request"]}
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
    Set to dictionary           ${MenuJsonObj['Response']["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}


ATC003 - Verify user can navigate through the menu (Configure->Manual Setup->Sensor 1->Sensor Limits
    [Tags]       Critical        Menu       Menu_Navigation
    # Get Configure menu and validate
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Get_Item_for_configure_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback     GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

    # Get Manual Setup menu and validate
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Get_Item_ManualSetup_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

    # Get device menu and validate
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Navigation_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
    :for    ${index}    in range    1   4
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

    # Get Device information menu and validate
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
    :for    ${index}    in range    0   5
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC004 - Verify user can fetch the Menu for Image menu item
    [Tags]           Critical        Menu       Menu_Image
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Get_Menu_for_identification_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback     GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    :for    ${index}    in range    0   7
        \   set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}   VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]["VarValue"]}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][7]["ItemInfo"]}              ImagePath=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][7]["ItemInfo"]["ImagePath"]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

########################################################################################################################
################################################ Menu Error Condition ##################################################
########################################################################################################################

#TODO: This test case is not working only with ATF. Need to verify this.
#ATC001 - Verify user can not fetch menu with Wrong itemID-Error
#    [Tags]     Non-Critical        Menu
#    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Invalid_Menu_ItemID_Rsc.json
#    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${MenuJsonObj["Request"][0]}
#    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][0]}
#    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${MenuCompStatus}

#TODO[CPM_Validation]:
#ATC002 - Verify user can not fetch menu with invalid itemID value in string-Error
#    [Tags]     Non-Critical        Menu
#    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Invalid_Menu_ItemID_Rsc.json
#    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${MenuJsonObj["Request"][1]}
#    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][1]}
#    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${MenuCompStatus}


ATC003 - Verify user can not fetch menu without itemID attributes-Error
    [Tags]     Non-Critical         Menu    Menu_Error
    [Setup]                     register callback       GetItem
    [Teardown]                  unregister callback     GetItem
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_Invalid_Menu_ItemID_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"][2]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response'][2]}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

########################################################################################################################
################################################ Menu Validity Cases ###################################################
########################################################################################################################
ATC001 - Verify user can view the menu when the validity condition is met (Manual Setup->Sensor 1)
    [Tags]  Critical     Menu        Menu_Validity
    [Setup]                     SetSensor1Type          HART_644_Menu_Set_SensorType1_Value_CalVanDusan_Rsc.json
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_SensorMatchinh-CVD_after_calvanDusan_Rsc.json
                                register callback       GetItem
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback     GetItem
    :for    ${index}    in range    0   4
        \   Set to dictionary           ${MenuJsonObj['Response']["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}


ATC002 - Verify user cannot view the menu when the validity condition is not met (Manual Setup->Sensor 1)
    [Tags]      Critical     Menu        Menu_Validity
    [Setup]     SetSensor1Type                          HART_644_Menu_Set_SensorType1_Value_Omhs_Rsc.json
    ${JsonFileContent}          get file                ${644H5MenuResources}/HART_644_Menu_SensorMatching_After_Omhs_Rsc.json
                                register callback       GetItem
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback     GetItem
    :for    ${index}    in range    0   4
        \   Set to dictionary           ${MenuJsonObj['Response']["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    [Teardown]                  SetSensor1Type          HART_644_Menu_Set_SensorType1_Value_CalVanDusan_Rsc.json