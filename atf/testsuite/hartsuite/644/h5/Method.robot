*** Settings ***
Documentation    This suite contains the test cases for Manual Abort, Process Abort, Write Lock abort, HART Lock abort, Multi-drop abort conditions
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW
...             AND             ConnectDevice644H5         "TRUE"
...             AND             ChangeAddressKW             ${HostResources}/HART_Host_Change_Address_Rsc.json              [0]
...             AND             DisconnectDeviceKW
...             AND             ConnectDeviceAddressKW   [0]    "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW
...             AND             ShutdownStackKW


*** Variables ***
${644H5VariableResources}           ${644_H5_Resource}/variable
${644H5MethodResources}             ${644_H5_Resource}/method

*** Keywords ***
ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_WriteProtect_Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response'][1]}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}
                                unregister callback     SetItem
*** Test Cases ***
########################################################################################################################
################################################### Method Execution ###################################################
########################################################################################################################
ATC001 - Verify the execution of method that evaluates the DD revision and verify help option
    [Tags]          critical   Method       Common
    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_DD_Revision2_rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}

    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                 unregister callback    SetItem

#ATC002 - Verify the execution of perform loop test method(START,ACK,SELECTION,GETVALUE,DISPLAY,COMPLETED)
#    [Tags]          critical   Method       Non-multidrop
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performLoopTest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#    #option1
#
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performloopTestOption1_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
#    set to dictionary           ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#     #option2
#
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption2_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    set to dictionary           ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#      #option3
#
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performLoopTestOption3_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]}   Message=${NotificationData["ItemInfo"]["Message"]}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    set to dictionary           ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#     #option4
#
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#    #option5
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption5_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${GetItemResponse}          Set item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#
#########################################################################################################################
#################################################### Method Abort Condition #############################################
#########################################################################################################################
#ATC001 - Verify user can not execute the method when Write Lock is enabled in the device
#    [Tags]          critical    Method       Non-Multidrop      Process_Abort
#    [Setup]                     ChangeWriteProtectMode     ["Request"][1]
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_abortWhenWriteProtect_Rsc.json
#    ${MEJsonObj}                evaluate                   json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback          GetItem
#    ${GetItemResponse}          get item                   ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object        ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                        unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#    [Teardown]                   ChangeWriteProtectMode        ["Request"][3]
#
#
#ATC002 - Verify the Aborting of method through user intervention
#    [Tags]          critical   Method       Non-Multidrop   Manual-Abort
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   run keyword if               ${Index}==1      set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#
#ATC003 -Verify method gets aborted when user sets invalid value for GETVALUE notify message (perform loop test method)
#    [Tags]          Critical   Method           Non-Multidrop       Process_Abort
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performLoopTest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      30
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][3]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption5_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${GetItemResponse}          Set item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#                                unregister callback     SetItem
#
#########################################################################################################################
#################################################### Method Error Condition #############################################
#########################################################################################################################
##TODO[StateMachine]:To Verify this test case once state machine is implemented
##ATC001 - Verify the behavior when user tries to perform Disconnect device when method execution is running
##    [Tags]          Non-Critical    Method   Non-Multidrop
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performLoopTestOption3_Rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${Status}                   register callback       SetItem
##    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
##    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
##    should be true              True==${SetItemStatus}
##    ${NotifyTimeout}            convert to integer      20
##    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
##    ${KeyExists}                is json key exist      ${NotificationData}      ItemInfo
##    should be true              True==${KeyExists}
##                                unregister callback     SetItem
##    [Teardown]                  DisconnectDevice
#
##TODO[StateMachine]:To Verify this test case once state machine is implemented
##ATC002 - Verify the behavior when user tries to perform ShutDown device when method execution is running
##    [Tags]          Non-Critical    Method      Non-Multidrop
##    [Setup]                     ConnectDevice
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${Status}                   register callback       GetItem
##    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
##    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
##    should be true              True==${GetItemStatus}
##    ${NotifyTimeout}            convert to integer      10
##    ${NotifyIndex}              convert to integer      0
##    :for    ${Index}    in range    0   2
##        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
##        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
##        \   run keyword if               ${Index}==1            set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValEnum=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValEnum"]}
##        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
##        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
##        \   should be true              True==${NotifyStatus}
##                                unregister callback       GetItem
##    [Teardown]                  ShutDownProtocol
#
#
##TODO[StateMachine]:To Verify this test case once state machine is implemented
##ATC003 - Verify the behavior when user tries to switch protocol when method execution is running
##    [Tags]          Non-Critical    Method      Non-Multidrop
##    [Setup]  Run Keywords       SetProtocol     AND     InitializeProtocol
##    ...             AND             ConnectDevice
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${Status}                   register callback       GetItem
##    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
##    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
##    should be true              True==${GetItemStatus}
##    ${NotifyTimeout}            convert to integer      10
##    ${NotifyIndex}              convert to integer      0
##    :for    ${Index}    in range    0   2
##        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
##        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
##        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
##        \   run keyword if              ${Index}==1             set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
##        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
##        \   should be true              True==${NotifyStatus}
##                                unregister callback     GetItem
##    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_HART_then_set_FF_protocol_Rsc.json
##    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
##    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
##    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
##    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
##    should be true              True==${SetProtocolCompStatus}
##    [Teardown]                  DisconnectDevice
#
#ATC004 - Verify user can execute method with incorrect item id - ERROR
#    [Tags]          Non-Critical    Method      Common
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_InvalidItemID_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#                                unregister callback     GetItem
##Crash
##TODO[CPM_Validation]:Verify after the bug get fixed
##ATC005 - Verify user can execute method with invalid item id value in string - ERROR
##    [Tags]          Non-Critical    Method     Common
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_InvalidItemID_Rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${Status}                   register callback       GetItem
##    ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
##    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
##    should be true              True==${GetItemStatus}
##                                unregister callback     GetItem
#
#ATC006 - Verify user can proceed with the execution with invalid request during method execution phase - ERROR
#    [Tags]          Non-Critical    Method      Common
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_InvalidRequest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
#    should be true              True==${GetItemStatus}
#                                unregister callback     GetItem
##issue ok response
##TODO[StateMachine]:To Verify this test case once state machine is implemented
##ATC007 -Verify user can trigger two methods simultaneously, if a method is already in progress - ERROR
##    [Tags]          Non-Critical    Method      Non-Multidrop
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_DD_Revision2_rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${Status}                   register callback       GetItem
##    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
##    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
##    should be true              True==${GetItemStatus}
##    ${NotifyTimeout}            convert to integer      20
##    ${NotifyIndex}              convert to integer      0
##    :for    ${Index}    in range    0   2
##        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
##        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
##        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
##        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
##        \   should be true              True==${NotifyStatus}
##    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_TwoMethodSimultaneously_Rsc.json
##    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
##    ${SetItemResponse}          get item                ${MEJsonObj["Request"]}
##    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
##    should be true              True==${SetItemStatus}
##                               unregister callback     GetItem
#
##Note :Response is "Ok" not "Error"(having doubt in funcianality needs to be clear)
##ATC008 - Verify user can set a value for GetItem and SetItem variable type when method execution is running - ERROR
##    [Tags]          Non-Critical    method          Non-multidrop
##    ${JsonFileContent}          get file               ${644VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
##                                register callback       SetItem
##    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
##    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
##    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
##    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
##    should be true              True==${VariableCompStatus}
##                                unregister callback     SetItem
##    ${JsonFileContent}          get file               ${644VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
##    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
##                                register callback       GetItem
##    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
##    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
##    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
##    should be true              True==${VariableCompStatus}
##                                unregister callback      GetItem
##   [Teardown]                  Run Keywords             DisconnectDevice
##         ...                   AND                      ConnectDevice
#########################################################################################################################
############################################# Multidrop Method Execution ################################################
#########################################################################################################################
#ATC001 - Verify the abort of perform loop test method in mutlidrop mode
#    [Tags]          critical    Method       Multidrop
#    [Setup]         Run Keywords        ChangeAddressKW         ${HostResources}/HART_Host_Change_Address_Rsc.json      [2]
#    ...             AND                 DisconnectDeviceKW
#    ...             AND                 ConnectDevice644H5      "TRUE"
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_performLoopTest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback     GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#    ${JsonFileContent}          get file                ${644H5MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
#    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][4]}
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
#    should be true              True==${SetItemStatus}
#    ${NotifyIndex}              convert to integer      5
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                        unregister callback     SetItem
