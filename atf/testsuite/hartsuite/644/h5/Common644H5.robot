*** Settings ***
Documentation    Suite contain common test cases for 644_H5.
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    ../resource/common.robot

*** Test Cases ***
ATC001 - Verify the behavior when the user tries to Set Item with blank SetItem request -ERROR

    [Tags]  Non-Critical     Variable  Menu
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_BlankRequest_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify the behavior when the user tries to Get Item with blank GetItem request -ERROR

    [Tags]  Non-Critical     Variable   Menu
    ${JsonFileContent}          get file                ${644H5VariableResources}/HART_644_Variable_BlankRequest_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


