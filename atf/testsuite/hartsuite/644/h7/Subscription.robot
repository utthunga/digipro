*** Settings ***
Documentation    This test suite contains the test cases for Subscription
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetLangCodeEnKW
...             AND             SetProtocolTypeKW
...             AND             InitializeStackKW
...             AND             ConnectDeviceKW         ${HostResources}/HART_Host_Connect_device_Rsc.json      "FALSE"

Suite Teardown  Run Keywords    DisconnectDeviceKW
...             AND             ShutdownStackKW

*** Variables ***
${644H7VariableResources}           ${644_H7_Resource}/variable
${644H7MethodResources}             ${644_H7_Resource}/method

*** Test Cases ***
ATC001 - Verify for Variable Subscription Operations
    [Tags]          Critical        Subscription
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json

    # SetVariable Value
    ${Status}                   register callback       SetItem
    ${SetValStatus}             set item                ${SubJsonObj["Sub"]["SetVal"]["Request"]}
    ${Status}                   unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${SubJsonObj["Sub"]["SetVal"]["Response"]}
    ${GetSetValStatus}          compare json object     ${SetValStatus}     ${ExpectedStatus}
    should be true              True==${GetSetValStatus}

    #Create Subscription
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubJsonObj["Sub"]["Create"]["Request"]}
    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}

    set to dictionary           ${SubJsonObj["Sub"]["Create"]["Response"]["CommandResponse"]["result"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubJsonObj["Sub"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubJsonObj["Sub"]["Create"]["Response"]}
    should be true              True==${CreateStatus}

    #Start Subscription
    ${StartSubResp}             process subscription    ${SubJsonObj["Sub"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubJsonObj["Sub"]["Start"]["Response"]}
    should be true              True==${StartStatus}

    #Subscription Notification
    ${NotifyData}               retrieve item data      ProcessSubscription
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubJsonObj["Sub"]["Notify"]}
    should be true              True==${NotifyStatus}

    #Stop Subscription
    ${StopSubResp}              process subscription    ${SubJsonObj["Sub"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubJsonObj["Sub"]["Stop"]["Response"]}
    should be true              True==${StopStatus}

    #Delete Subscription
    ${DeleteSubResp}            process subscription    ${SubJsonObj["Sub"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubJsonObj["Sub"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}

#ATC002 - Verify for Start subscription for invalid Subscription ID
#    [Tags]           Non-Critical        StartSubscription_Invalid_SubID
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${StartSubResp}             process subscription    ${SubJsonObj["Start_Error"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${StartSubStatus}           compare json object     ${StartSubResp}     ${SubJsonObj["Start_Error"]["Response"]}
#    should be true              True==${StartSubStatus}
#
#ATC003 - Verify for Stop subscription for invalid Subscription ID
#    [Tags]           Non-Critical               StopSubscription_Invalid_SubID
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${StopSubResp}              process subscription    ${SubJsonObj["Stop_Error"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${StopSubStatus}            compare json object     ${StopSubResp}     ${SubJsonObj["Stop_Error"]["Response"]}
#    should be true              True==${StopSubStatus}
#
#ATC004 - Verify for Delete subscription for invalid Subscription ID
#    [Tags]           Non-Critical        HTK       DeleteSubscription_Invalid_SubID
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${DeleteSubResp}            process subscription    ${SubJsonObj["Delete_Error"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${DeleteSubStatus}          compare json object     ${DeleteSubResp}     ${SubJsonObj["Delete_Error"]["Response"]}
#    should be true              True==${DeleteSubStatus}
#
#ATC005 - Verify for Create and Delete subscription
#    [Tags]           Critical        Create_And_Delete
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${CreateResp}               process subscription    ${SubJsonObj["CreateAndDelete"]["Create"]["Request"]}
#    ${createStatus}             compare json object     ${CreateResp}     ${SubJsonObj["CreateAndDelete"]["Create"]["Response"]}
#    should be true              True==${createStatus}
#
#    ${Delete1Resp}              process subscription    ${SubJsonObj["CreateAndDelete"]["Delete"]["Request"]}
#    ${Delete1Status}            compare json object     ${Delete1Resp}     ${SubJsonObj["CreateAndDelete"]["Delete"]["Response_1"]}
#    should be true              True==${Delete1Status}
#
#    ${Delete2Resp}              process subscription    ${SubJsonObj["CreateAndDelete"]["Delete"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${Delete2Status}            compare json object     ${Delete2Resp}     ${SubJsonObj["CreateAndDelete"]["Delete"]["Response_2"]}
#    should be true              True==${Delete2Status}

ATC006 - Verify for Invalid ItemAction -ERROR
    [Tags]           Non-Critical        Invalid_ItemAction
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidItemAction"]["Response"]}
    should be true              True==${createStatus}

ATC007 - Verify for No ItemAction -ERROR
    [Tags]      Non-Critical       No_ItemAction
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoItemAction"]["Response"]}
    should be true              True==${createStatus}

#ATC008 - Verify for InvalidRefreshRate Tag -ERROR
#    [Tags]      Non-Critical        Invalid_InvalidRefreshRate
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidRefreshRate"]["Request"]}
#    ${Status}                   unregister callback     ProcessSubscription
#    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidRefreshRate"]["Response"]}
#    should be true              True==${createStatus}

ATC009 - Verify for No InvalidRefreshRate Tag -ERROR
    [Tags]      Non-Critical        No_InvalidRefreshRate
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoRefreshRate"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoRefreshRate"]["Response"]}
    should be true              True==${createStatus}