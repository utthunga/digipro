*** Settings ***
Documentation    This test suite contains the test cases for Image
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW       "TRUE"
...             AND             SetLangCodeEnKW         "TRUE"
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDeviceKW    "True"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"


*** Variables ***
${644H7ImageResources}           ${644_H7_Resource}/image

*** Test Cases ***

#error
ATC001- Verify user can fetch the large image.
    [Tags]    Critical       Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_LargeImage_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${644H7ImageResources}/26180902_window.gif
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

ATC002 - Verify user can fetch the image for JPG image type
    [Tags]           Critical          Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_jpg_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${644H7ImageResources}/26180902_single_rmt_copy.jpg
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

ATC003 - Verify user can fetch the image for GIF image type
    [Tags]           Critical          Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_gif_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ReceivedImagePath}        set variable            ${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]}    ImagePath=${ReceivedImagePath}
    ${StoredImage}              set variable            ${644H7ImageResources}/26180902_sensor_open_short.gif
                                unregister callback     GetItem
    ${ImageJsonComp}            compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ImageJsonComp}
                                compare images          ${StoredImage}    ${ReceivedImagePath}  0.1

ATC004 - Verify the behavior when user fetch the image with wrong "ItemID" - Error
    [Tags]   Non-Critical       Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_Invalid_ItemID_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
                                unregister callback     GetItem
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC005 - Verify the behavior when user fetch the image with "ItemInDD" as false - Error
    [Tags]   Non-Critical         Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_Invalid_ItemInDD_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
                                unregister callback     GetItem
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}

ATC006 - Verify the behavior when user fetch image with wrong ItemType(=MENU) - Error
    [Tags]  Non-Critical        Image
    ${JsonFileContent}          get file                ${644H7ImageResources}/HART_644_Variable_Image_InValid_ItemType_Rsc.json
    ${GetParamListJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${GetParamListJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetParamListJsonObj['Response']}
                                unregister callback     GetItem
    ${GetParamListCompStatus}   compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetParamListCompStatus}