*** Settings ***
Documentation  This test suite contains the test case for variable data type
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW
...             AND             ConnectDeviceKW         "TRUE"
...             AND             ChangeAddressKW             ${HostResources}/HART_Host_Change_Address_Rsc.json              [0]
...             AND             DisconnectDeviceKW
...             AND             ConnectDeviceAddressKW   [0]    "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW
...             AND             ShutdownStackKW

*** Variables ***
${644H7VariableResources}           ${644_H7_Resource}/variable
${644H7MethodResources}             ${644_H7_Resource}/method

*** Keywords ***
ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_WriteProtectMode__Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response']}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}
                                unregister callback     SetItem

HARTLock
    [Arguments]             ${arg}
    ${JsonFileContent}          get file                ${644H7MethodResources}/${arg}
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \    run keyword if             ${NotifyIndex}==2      set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}     VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     SetItem

*** Test Cases ***
########################################################################################################################
################################################### Menu  Abort ########################################################
########################################################################################################################
ATC001 - Verify user can execute the method when Write Lock is enabled in the device
    [Tags]          Critical   Method       Process_Abort   Non-Multidrop
    [Setup]                     ChangeWriteProtectMode     ["Request"][1]
    ${JsonFileContent}          get file                   ${644H7MethodResources}/HART_644_Method_abortWhenWriteProtect_Rsc.json
    ${MEJsonObj}                evaluate                   json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback          GetItem
    ${GetItemResponse}          get item                   ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object        ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem             ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationDATA}         retrieve item data      SetItem             ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationDATA}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback           SetItem
    [Teardown]                  ChangeWriteProtectMode        ["Request"][0]

ATC002 - Verify user can execute the method when HART Lock is enabled in the device
    [Tags]          Critical   Method    Common    Process_Abort
    [Setup]                     HARTLock             HART_644_Method_Lock_Rsc.json
    ${JsonFileContent}          get file                   ${644H7MethodResources}/HART_644_Method_abortWhenHartLock_Rsc.json
    ${MEJsonObj}                evaluate                   json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback          GetItem
    ${GetItemResponse}          get item                   ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object        ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem
    [Teardown]                  HARTLock           HART_644_Method_unlock_Rsc.json


ATC003 - Verify the method execution when user aborts the method manually in GETVALUE screen
    [Tags]          Critical   Method   Non-multidrop    Manual_Abort
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \  run keyword if               ${Index}==1      set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback             GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}

    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem

ATC004 - Verify the method execution when user aborts the method manually in NotificationType "ACK" screen
    [Tags]          Critical   Method  Non-multidrop        Manual_Abort
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   run keyword and continue on failure               should be true              True==${NotifyStatus}
                                unregister callback    GetItem
    ${Status}                   register callback       SetItem
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_AbortLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          Set item                ${MEJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem

ATC005 - Verify the method execution when user aborts the method manually in "SELECTION" screen
    [Tags]          Critical   Method       Non-Multidrop   Manual_Abort
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback    GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_AbortLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          Set item                ${MEJsonObj["Request"]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem

ATC006 -Verify method execution when user sets invalid value for GETVALUE notify message (perform loop test method)
    [Tags]          Critical   Method     Non-Multidrop    Process_Abort
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}

    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][3]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    #option5- exit loop
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption5_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${GetItemResponse}          Set item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback    SetItem

########################################################################################################################
################################################### Menu  Error ########################################################
########################################################################################################################
#TODO[StateMachine]: It is crashing now.
#ATC001 - Verify the behavior when user tries to perform Disconnect device when method execution is running
#    [Tags]          Non-Critical   Method     Disconnect  Non-Multidrop
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback    GetItem
#    ${Status}                   register callback       SetItem
#    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${SetItemStatus}
#    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
#    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#    should be true              True==${NotifyStatus}
#
#TODO[StateMachine]: It is crashing now.
#ATC002 - Verify the behavior when user tries to perform ShutDown device when method execution is running
#    [Tags]          Non-Critical   Method     ShutDown_Stack      Non-Multidrop#    [Setup]                     ConnectDevice
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   run keyword if               ${Index}==1            set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback         GetItem
#
#TODO[StateMachine]: It is crashing now.
#ATC003 - Verify the behavior when user tries to switch protocol when method execution is running
#    [Tags]          Non-Critical   Method          Non-Multidrop
#    [Setup]  Run Keywords       SetProtocol     AND     InitializeProtocol
#    ...             AND             ConnectDevice
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_abortConfiguraeDisplay_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   run keyword if              ${Index}==1             set to dictionary        ${MEJsonObj["Notification"][1]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValEnum=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValEnum"]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#                                unregister callback    GetItem
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_HART_then_set_FF_protocol_Rsc.json
#    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
#    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetProtocolCompStatus}
#
#ATC004 - Verify user can execute method with incorrect item ID -ERROR
#    [Tags]          Non-Critical       Method       Common
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_InvalidItemID_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#                                unregister callback     GetItem
#
#TODO[CPM_Validation]: It is crashing. To verify this once this issue is fixed. It is crashing now.
#ATC005 - Verify user can execute method with invalid item ID (string value) -ERROR
#    [Tags]          Non-Critical     Method    Common
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_InvalidItemID_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][1]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][1]}
#    should be true              True==${GetItemStatus}
#                                unregister callback     GetItem
#
#TODO[StateMachine]: Now it response with "ok"
#ATC006 - Verify user can proceed with the execution with invalid request during method execution phase -ERROR
#    [Tags]          Non-Critical      Method           Common
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_InvalidRequest_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"]}
#    should be true              True==${GetItemStatus}
#                                unregister callback      GetItem
#
#TODO[StateMachine]: Now it response with "ok"
#ATC007 -Verify user can trigger two methods simultaneously, if a method is already in progress -ERROR
#    [Tags]     Non-Critical       Method       common
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_DD_Revision2_rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       GetItem
#    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
#    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
#    should be true              True==${GetItemStatus}
#    ${NotifyTimeout}            convert to integer      10
#    ${NotifyIndex}              convert to integer      0
#    :for    ${Index}    in range    0   2
#        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
#        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
#        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
#        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
#        \   should be true              True==${NotifyStatus}
#    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_TwoMethodSimultaneously_Rsc.json
#    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
#    ${SetItemResponse}          get item                ${MEJsonObj["Request"]}
#    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"]}
#    should be true              True==${SetItemStatus}
#                                unregister callback     GetItem
#
#Depends on ATC007
ATC007 - Verify user can set a value for GetItem and SetItem Variable type when method execution is running -ERROR
    [Tags]          Non-Critical     Method    common
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
                                register callback       SetItem
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
                                unregister callback     SetItem
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
                                unregister callback      GetItem

#########################################################################################################################
#################################################### Menu  Execute ######################################################
#########################################################################################################################
ATC008 - Verify user could get proper method label and help string attribute values, when a method is triggered
    [Tags]          critical   Method   Common
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_DD_Revision2_rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}

    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                 unregister callback    SetItem

ATC009 - Verify setting value during method execution is possible when variable subscription is active
    [Tags]          Common   Method       Subcription   Common
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_FLOAT_Subcription_Rsc.json
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data     ProcessSubscription
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemList=${NotifyData["ItemList"]}

    #start Method
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_DD_Revision2_rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}

    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem   ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     GetItem

    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}

    ${NotificationData}         retrieve item data      SetItem       ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback     SetItem
    # Method finished
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}


ATC010 - Verify the execution of perform loop test method(START,ACK,SELECTION,GETVALUE,DISPLAY,COMPLETED)
    [Tags]          critical   Method       Non-Multidrop
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback    GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    #option1

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performloopTestOption1_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
     #option2

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption2_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
      #option3

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTestOption3_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]}   Message=${NotificationData["ItemInfo"]["Message"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

     #option4

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]}   VarInfo=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][1]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][2]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    #option5
    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption5_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${GetItemResponse}          Set item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
                                unregister callback    SetItem
#
#########################################################################################################################
#################################################### Menu  Execute ######################################################
#########################################################################################################################
ATC0011 - Verify the abort of perform loop test method in mutlidrop mode
    [Tags]          Critical   Method       Multidrop
    [Setup]         Run Keywords        ChangeAddressKW             ${HostResources}/HART_Host_Change_Address_Rsc.json              [1]
    ...             AND                 DisconnectDeviceKW
    ...             AND                 ConnectDeviceAddressKW      [1]     "TRUE"

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_performLoopTest_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      20
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    set to dictionary           ${MEJsonObj["Notification"][2]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${JsonFileContent}          get file                ${644H7MethodResources}/HART_644_Method_PerformLoopTestOption4_Rsc.json
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][0]}
    set to dictionary           ${MEJsonObj["Notification"][0]["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}   VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][4]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotifyIndex}              convert to integer      5
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     SetItem