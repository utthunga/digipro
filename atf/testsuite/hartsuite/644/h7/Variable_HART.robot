*** Settings ***
Documentation    This test suite contains the test cases for Variable
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW  "True"
...             AND             SetLangCodeEnKW    "True"
...             AND             InitializeStackKW   "True"
...             AND             ConnectDeviceKW    "True"

Suite Teardown  Run Keywords    DisconnectDeviceKW  "True"
...             AND             ShutdownStackKW     "True"

*** Variables ***
${644H7VariableResources}           ${644_H7_Resource}/variable
${644H7MethodResources}             ${644_H7_Resource}/method

*** Keywords ***
SetSensor1typevalue
    [Arguments]         ${arg}
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_SetSensor1Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]${arg}}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

HARTLock
    [Arguments]             ${arg}
    ${JsonFileContent}          get file                ${644H7MethodResources}/${arg}
    ${MEJsonObj}                evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       GetItem
    ${GetItemResponse}          get item                ${MEJsonObj["Request"][0]}
    ${GetItemStatus}            compare json object     ${GetItemResponse}    ${MEJsonObj["Response"][0]}
    should be true              True==${GetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      GetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     GetItem
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${MEJsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${MEJsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${MEJsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                        unregister callback     SetItem
ChangeWriteProtectMode
    [Arguments]     ${arg1}
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_WriteProtectMode__Rsc.json
    ${ProtectModeJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${ProtectModeJsonObj${arg1} }
    ${ExpectedStatus}           set variable            ${ProtectModeJsonObj['Response']}
    ${ProtectModeCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ProtectModeCompStatus}

*** Test Cases ***
########################################################################################################################
################################################### ASCII Variable  ####################################################
########################################################################################################################
ATC001 - Verify user can write value to the variable type ASCII within the length given and allowed characters
    [Tags]  Critical    Variable      ASCII
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
   #repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can get item for variable type ASCII and attributes
    [Tags]  Critical    Variable      ASCII

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC003 - Verify user can get device value for VARIABLE type ASCII after subcription
#    [Tags]  Critical    Variable    ASCII
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_ASCII_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ASCII_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC004 - Verify user can write value to the variable Type ASCII more than the length allowed -ERROR
    [Tags]  Critical    Variable    ASCII
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify user can write value to the variable type ASCII with Invalid character -ERROR
    [Tags]  Non-Critical    Variable   ASCII
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Bit enumeration variable  ##########################################
########################################################################################################################

ATC006 - Verify user can write value to the variable type BIT-ENUMERATION within the BIT-ENUMERATION range
    [Tags]  Critical    Variable    Bit_Enumeration
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
   # repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC007 - Verify user can get item for variable type BIT-ENUMERATION with desire attributes
    [Tags]  Critical    Variable    Bit_Enumeration

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC008 - Verify user can get device value for variable type BIT-ENUMERATION after subcription
#    [Tags]  Critical    Variable    Bit_Enumeration
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC009 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(string) -ERROR
    [Tags]  Non-Critical   Variable    Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback  SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC0010 - Verify user can write wrong VarValue to the variable type BIT-ENUMERATION- ERROR
    [Tags]  Non-Critical    Variable    Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC006 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value(float)
#    [Tags]  Non-Critical    Variable    Bit_Enumeration
#    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}]
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC0011 - Verify user can write value to the variable type BIT-ENUMERATION with Invalid value (negative) -ERROR
    [Tags]  Non-Critical    Variable       Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BITENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Date / Time variable  ##############################################
########################################################################################################################
ATC0012 - Verify user can write valid DATE value to the variable
    [Tags]  Critical    Variable    Date
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    #repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC0013 - Verify user can get item for variable type DATE and its attributes
    [Tags]  Critical    Variable    Date
     ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC0014 - Verify user can get device value for variable type DATE after subcription
#    [Tags]  Critical    Variable    Date
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_DATE_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


ATC0015 - Verify user can write Invalid value to the variable type DATE(float)-ERROR
    [Tags]  Common    Variable     Date
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC0016 - Verify user can write Invalid value to the variable type DATE(string)-ERROR
    [Tags]  Common    Variable     Date
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC0017 - Verify user can write Invalid value to the variable type DATE(negative)-ERROR
    [Tags]  Common    Variable     Date
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_DATE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Enumeration variable  ##############################################
########################################################################################################################
ATC0018 - Verify user can write value to the variable type ENUMERATION within the ENUMERATION range
    [Tags]  Critical    Variable    Enumeration
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    #repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC0019 - Verify user can get item for variable type ENUMERATION with desire attributes
    [Tags]  Critical    Variable    Enumeration

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC0020 - Verify user can get device value for variable type ENUMERATION after subcription
#    [Tags]  Critical    Variable    Enumeration
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

#TODO[CPM_Validation]: It is crashing now
#ATC004 - Verify user can write value to the variable type ENUMERATION with Invalid value(string) -ERROR
#    [Tags]  Non-Critical   Variable    Enumeration
#    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC0021 - Verify user can write wrong VarValue to the variable type ENUMERATION- ERROR
    [Tags]  Non-Critical    Variable    Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC0022 - Verify user can write value to the variable type ENUMERATION with Invalid value(float)
    [Tags]  Non-Critical    Variable    Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC0023 - Verify user can write value to the variable type ENUMERATION with Invalid value (negative) -ERROR
    [Tags]  Non-Critical    Variable   Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_ENUMERATION_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

# conditionalvalidity
ATC008 - Verify user can get sensor_1_unit_enumeration value Ohms when Ohms is set in sensor_1_type variable(conditionalvalidity)
    [Tags]  Critical    Variable    Enumeration
    SetSensor1typevalue         [0]
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Getsensor1unitValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
#
## conditionalvalidity
ATC009 - Verify user can get sensor_1_unit_enumeration value-degC,degF,degR,Kelvin and Ohms when sensor_1_type variable is less and equal to "PT200 A3916"(conditionalvalidity)
    [Tags]  Critical    Variable    Enumeration
    SetSensor1typevalue         [2]
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Getsensor1unitValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Bit Enumeration Item  ################################################
########################################################################################################################
ATC001 - Verify user can get BitEnumeration Items Menu
    [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["BitEnumItemMenu"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["BitEnumItemMenu"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC002 - Verify user can Get Set BitEnumeration Items
    [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["BitEnumGetSet"]["SetVar"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["BitEnumGetSet"]["SetVar"]["Response"]}
    ${SetVarCompStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetVarCompStatus}

    register callback           GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["BitEnumGetSet"]["GetVar"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["BitEnumGetSet"]["GetVar"]["Response"]}
    ${GetVarCompStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetVarCompStatus}

ATC003 - Verify user can get BitEnumeration Items with Invalid BitIndex = "abc"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["InvalidBitIndex"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["InvalidBitIndex"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC004 - Verify user can get BitEnumeration Items with Incorrect BitIndex - "16"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["IncorrectBitIndex"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["IncorrectBitIndex"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify user can get BitEnumeration Items with Invalid ItemID = "abc"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["InvalidItemID"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["InvalidItemID"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify user can get BitEnumeration Items with Incorrect ItemID - "17102"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["IncorrectItemID"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["IncorrectItemID"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC007 - Verify user can get BitEnumeration Items with Invalid ItemType = "1234"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["InvalidItemType"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["InvalidItemType"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC008 - Verify user can get BitEnumeration Items with Incorrect ItemType - "MENU"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["IncorrectItemType"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["IncorrectItemType"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC009 - Verify user can get BitEnumeration Items with Invalid Var Value = "abc"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["InvalidVarValue"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["InvalidVarValue"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC010 - Verify user can get BitEnumeration Items with Incorrect Var Value - "-1"
       [Tags]  Critical    Variable    BitEnumItem
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_BitEnumItem_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["IncorrectVarValue"]["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["IncorrectVarValue"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Float variable  ####################################################
########################################################################################################################
ATC0024 - Verify user can write value to the variable type FLOAT within the length given and allowed range
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    #repeat keyword    ${TestCount}  set item                ${VariableJsonObj["Request"][1]}

    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC0025 - Verify user can get item for variable type FLOAT with desire attributes
    [Tags]  Critical    Variable    Float

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC0026 - Verify user can get device value for variable type FLOAT after subcription
#    [Tags]  Critical    Variable    Float
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_FLOAT_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC0027 - Verify user can write value to the variable type FLOAT with Invalid value(string) -ERROR
    [Tags]  Non-Critical    Variable   Float
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#TODO: CPM is crashing
#ATC005 - Verify user can write value to the variable type FLOAT less than the min allowed range(Negative value)
#    [Tags]  Non-Critical   Variable     Float
#    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC0028 - Verify user can write integer value to the variable type FLOAT
    [Tags]  Critical    Variable    Float
    ${JsonFileContent}            get file               ${644H7VariableResources}/HART_644_Variable_FLOAT_Rsc.json
    ${VariableJsonObj}            evaluate                json.loads('''${JsonFileContent}''')   json
                                  register callback       SetItem
    ${ActualStatus}               set item                 ${VariableJsonObj["Request"][3]}
                                  unregister callback     SetItem
    ${ExpectedStatus}             set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                True==${VariableCompStatus}

########################################################################################################################
######################################### Index Variable ###############################################################
########################################################################################################################
#TODO:BustMode is not Implement for setItem verify Index(second variable) after bust mode implimentation
ATC028 - Verify user can write value to the variable type INDEX within the index range
    [Tags]  Critical    Variable    Index
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC029 - Verify user can get item for variable type INDEX and its attributes
    [Tags]  Critical    Variable    Index

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}       VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC030 - Verify user can get device value for variable type INDEX after subcription
#    [Tags]  Critical    Variable    Index
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_INDEX_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}       VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC031 - Verify user can write value to the variable type INDEX with Invalid value(string) -ERROR
    [Tags]  Non-Critical    Variable   Index
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC032 - Verify user can write wrong VarValue to the variable type INDEX- ERROR
    [Tags]  Non-Critical    Variable    Index
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC033 - Verify user can write value to the variable type INDEX with Invalid value(float) -ERROR
    [Tags]  Non-Critical   Variable      Index
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC034 - Verify user can write value to the variable type INDEX with Invalid value (negative) -ERROR
    [Tags]  Non-Critical    Variable        Index
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_INDEX_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### PACKED ASCII Variable ########################################################
########################################################################################################################
ATC035 - Verify user can write value to the variable type PACKED_ASCII within the length given and allowed characters
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    #repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC036 - Verify user can get item for variable type PACKED_ASCII and its attributes
    [Tags]  Critical    Variable    Packed_ASCII

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC037 - Verify user can get device value for variable type PACKED_ASCII after subcription
#    [Tags]  Critical    Variable    Packed_ASCII
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Subcription_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC038 - Verify user can write string to the variable type PACKED_ASCII more than the length allowed
    [Tags]  Critical    Variable    Packed_ASCII
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC039 - Verify user can write value to the variable type PACKED_ASCII with Invalid character -ERROR
    [Tags]  Non-Critical    Variable   Packed_ASCII
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Read only Variable ###########################################################
########################################################################################################################
#ATC040 - Verify user can get parameter for Read Only Parameter
#    [Tags]  Critical    Variable    Read_only
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ReadOnlyParamater_Rsc.json
#                                register callback       GetItem
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}    VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#
#
#ATC041 - Verify user cannot set parameter for Read Only Parameter
#    [Tags]  Critical    Variable    Read_only
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_ReadOnlyParamater_Rsc.json
#                                register callback       SetItem
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


########################################################################################################################
######################################### Single Bit Enumeration ######################################################
########################################################################################################################
ATC042 - Verify user can fetch single BIT-ENUMERATION variable in menu
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_Bit_Enumuration_Fetch_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
        :for    ${index}    in range    0   5
        \   Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]}   VarInfo=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][${index}]["ItemInfo"]["VarInfo"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC043 - Verify user can write VarValue to the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC044 - Verify user can fetch value from the variable type single BIT-ENUMERATION
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC045 - Verify user can get device value for variable type single BIT-ENUMERATION after subcription
#    [Tags]  Critical    Variable       Single_Bit_Enumeration
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_Single_bit_enumeration_subcription_Rsc.json
#    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

ATC046 - Verify user can write to the variable type single BIT-ENUMERATION with wrong VarBitIndex-
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC047 - Verify user can fetch value from the variable type single BIT-ENUMERATION with wrong VarBitIndex
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC048 - Verify user can write wrong(string) VarValue to the variable type single BIT-ENUMERATION -ERROR
    [Tags]  Critical    Variable       Single_Bit_Enumeration
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_Single_BitEnumuration_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][5]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Unsigned Interger ############################################################
########################################################################################################################
ATC049 - Verify user can write value to the variable type UNSIGNED_INTEGER within the length given and allowed range
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    #repeat keyword    ${TestCount}  set item            ${VariableJsonObj["Request"][1]}
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC050 - Verify user can get item for variable type UNSIGNED_INTEGER and its attributes
    [Tags]  Critical    Variable    Unsigned_Integer

    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

#ATC051 - Verify user can get device value for variable type UNSIGNED_INTEGER after subcription
#    [Tags]  Critical    Variable        Unsigned_Integer
#    [Setup]   Run Keywords    DisconnectDeviceKW
#    ...             AND       ConnectDeviceKW           "TRUE"
#    ...             AND       Subcription               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Subcription_Rsc.json
#    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       GetItem
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


ATC052 - Verify user can write value to the variable type UNSIGNED_INTEGER with Invalid value(string) -ERROR
    [Tags]  Non-Critical    Variable   Unsigned_Integer
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC053 - Verify user can write string to the variable type UNSIGNED_INTEGER more than the max allowed range-ERROR
    [Tags]  Non-Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC054 - Verify user can write string to the variable type UNSIGNED_INTEGER less than the min allowed range(Negative value)-ERROR
    [Tags]  Non-Critical    Variable   Unsigned_Integer
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC055 - Verify user can write Float value to the variable type UNSIGNED_INTEGER
    [Tags]  Critical    Variable    Unsigned_Integer
    ${JsonFileContent}          get file               ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Variable Error ###############################################################
########################################################################################################################
ATC056 - Verify the behavior when the user tries to write the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC057 - Verify the behavior when the user tries to fetch the VARIABLE with wrong Item ID attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][5]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC058 - Verify the behavior when the user tries to write the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC059 - Verify the behavior when the user tries to fetch the VARIABLE without ItemID attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC005 - Verify the behavior when the user tries to write the VARIABLE with string as Item ID -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemID_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC060 - Verify the behavior when the user tries to fetch the VARIABLE with string as Item ID -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC061 - Verify the behavior when the user tries to write the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC062 - Verify the behavior when the user tries to fetch the VARIABLE with wrong ItemType (=MENU) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC063 - Verify the behavior when the user tries to write the VARIABLE without ItemType attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC064 - Verify the behavior when the user tries to fetch the VARIABLE without ItemType attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][3]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC065 - Verify the behavior when the user tries to write the VARIABLE with invalid ItemType (Integer) attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC066 - Verify the behavior when the user tries to fetch the VARIABLE with invalid ItemType (Integer) attribute -ERROR

    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemType_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][4]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}


ATC067 - Verify the behavior when the user tries to fetch the VARIABLE with wrong "ItemInDD"=FALSE -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
ATC068 - Verify the behavior when the user tries to fetch the VARIABLE without "ItemInDD" attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_InvalidItemInDD_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC069 - Verify the behavior when the user tries to write the value without VarValue attribute -ERROR
    [Tags]  Non-Critical    Variable
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_WithoutVarValue_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Variable Relation ###########################################################
########################################################################################################################
#ATC001 - Verify refresh relation is applied for Variable through subcription
#    [Tags]  Critical    RefreshRelation  subcription
#    ${JsonFileContent}         get file               ${644H7VariableResources}/HART_644_Variable_RefreshRelation_sensor1Type_Rsc.json
#    ${MEJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback        SetItem
#    ${ActualStatus}            set item                ${MEJsonObj["Setvar"]["Request"]}
#                               unregister callback     SetItem
#    ${ExpectedStatus}          set variable            ${MEJsonObj["Setvar"]['Response']}
#    ${GetItemStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${GetItemStatus}
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_Publish_Variable_sensor1Unit_Rsc.json
#    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
#    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
#    should be true              True==${KeyExists}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    log dictionary              ${SubscriptionJsonObj}
#    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
#    should be true              True==${CreateStatus}
#    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
#    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
#    should be true              True==${StartStatus}
#    ${NotifyData}               retrieve item data      ProcessSubscription
#    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
#    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
#    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
#    should be true              True==${StopStatus}
#    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
#    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
#    should be true              True==${DeleteStatus}
#    ${Status}                   unregister callback     ProcessSubscription
#    should be true              True==${NotifyStatus}
#                                register callback       GetItem
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_RefreshRelation_Sensor1Unit_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
#                                unregister callback     GetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}


#ATC070 - Verify unit relation is applied for variable through subcription
#    [Tags]  Critical    UnitRelation  subcription
#    ${JsonFileContent}         get file               ${644H7VariableResources}/HART_644_Variable_unitRelation_sensor1Unit_Rsc.json
#    ${MEJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback        SetItem
#    ${ActualStatus}            set item                ${MEJsonObj["Setvar"]["Request"]}
#                               unregister callback      SetItem
#    ${ExpectedStatus}          set variable            ${MEJsonObj["Setvar"]['Response']}
#    ${GetItemStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${GetItemStatus}
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_publish_variable_upperRange_Rsc.json
#    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
#    ${Status}                   register callback       ProcessSubscription
#    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
#    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
#    should be true              True==${KeyExists}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    log dictionary              ${SubscriptionJsonObj}
#    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
#    should be true              True==${CreateStatus}
#    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
#    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
#    should be true              True==${StartStatus}
#    ${NotifyData}               retrieve item data      ProcessSubscription
#    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]["ItemList"][0]}    ItemInfo=${NotifyData["ItemList"][0]["ItemInfo"]}
#    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
#    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
#    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
#    should be true              True==${StopStatus}
#    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
#    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
#    should be true              True==${DeleteStatus}
#    ${Status}                   unregister callback     ProcessSubscription
#    should be true              True==${NotifyStatus}
#                                register callback       GetItem
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_UnitRelation_sensorLimit_UpperRange_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
#                                unregister callback     GetItem
#    set to dictionary           ${VariableJsonObj["Response"]["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]}   VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["VarInfo"]["VarValue"]}
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### Write Protect Mode ON/OFF Condition ###########################################################
########################################################################################################################
ATC071 - Verify user cannot set varible when Write Protect Mode ON
    [Tags]  Critical   WriteProtectMode     Variable
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][1]
    [Teardown]  unregister callback     SetItem
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC072 - Verify user can set variable when Write Protect Mode OFF
    [Tags]  Critical   WriteProtectMode     Variable
    [Setup]     run keywords            register callback           SetItem
    ...         AND                     ChangeWriteProtectMode        ["Request"][0]
    [Teardown]  unregister callback     SetItem
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_UNSIGNED_INTEGER_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

########################################################################################################################
######################################### HART Lock / Unlock Condition  ################################################
########################################################################################################################
#ATC073 Verify Set item for Variable for Hart Lock ON
#    [Tags]  Critical       Variable     Hart_Lock
#    [Setup]                     HARTLock       HART_644_Method_Lock_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
#                                register callback       SetItem
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
#
#
#ATC074 Verify Set item for Variable for Hart Lock OFF
#    [Tags]  Critical     Variable       Hart_Lock
#    [Setup]                     HARTLock       HART_644_Method_unlock_Rsc.json
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Variable_PACKED_ASCII_Rsc.json
#                                register callback       SetItem
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}

########################################################################################################################
################################################### Process Subscription Actions ####################################################
########################################################################################################################
#ATC075 - Verify for Variable Subscription Operations
#    [Tags]          Critical        Subscription
#    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
#    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
#
#    # SetVariable Value
#    ${Status}                   register callback       SetItem
#    ${SetValStatus}             set item                ${SubJsonObj["Sub"]["SetVal"]["Request"]}
#    ${Status}                   unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${SubJsonObj["Sub"]["SetVal"]["Response"]}
#    ${GetSetValStatus}          compare json object     ${SetValStatus}     ${ExpectedStatus}
#    should be true              True==${GetSetValStatus}
#
#    #Create Subscription
#    ${Status}                   register callback       ProcessSubscription
#    ${CreateSubResp}            process subscription    ${SubJsonObj["Sub"]["Create"]["Request"]}
#    ${KeyExists}                is json key exist       ${CreateSubResp["CommandResponse"]}    result
#    should be true              True==${KeyExists}
#
#    set to dictionary           ${SubJsonObj["Sub"]["Create"]["Response"]["CommandResponse"]["result"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubJsonObj["Sub"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubJsonObj["Sub"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubJsonObj["Sub"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    set to dictionary           ${SubJsonObj["Sub"]["Notify"]}                ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
#    log dictionary              ${SubJsonObj}
#    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubJsonObj["Sub"]["Create"]["Response"]}
#    should be true              True==${CreateStatus}
#
#    #Start Subscription
#    ${StartSubResp}             process subscription    ${SubJsonObj["Sub"]["Start"]["Request"]}
#    ${StartStatus}              compare json object     ${StartSubResp}     ${SubJsonObj["Sub"]["Start"]["Response"]}
#    should be true              True==${StartStatus}
#
#    #Subscription Notification
#    ${NotifyData}               retrieve item data      ProcessSubscription
#    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubJsonObj["Sub"]["Notify"]}
#    should be true              True==${NotifyStatus}
#
#    #Stop Subscription
#    ${StopSubResp}              process subscription    ${SubJsonObj["Sub"]["Stop"]["Request"]}
#    ${StopStatus}               compare json object     ${StopSubResp}      ${SubJsonObj["Sub"]["Stop"]["Response"]}
#    should be true              True==${StopStatus}
#
#    #Delete Subscription
#    ${DeleteSubResp}            process subscription    ${SubJsonObj["Sub"]["Delete"]["Request"]}
#    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubJsonObj["Sub"]["Delete"]["Response"]}
#    should be true              True==${DeleteStatus}
#    ${Status}                   unregister callback     ProcessSubscription
#    should be true              True==${NotifyStatus}

ATC076 - Verify for Start subscription for invalid Subscription ID
    [Tags]           Non-Critical        StartSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${StartSubResp}             process subscription    ${SubJsonObj["Start_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${StartSubStatus}           compare json object     ${StartSubResp}     ${SubJsonObj["Start_Error"]["Response"]}
    should be true              True==${StartSubStatus}

ATC077 - Verify for Stop subscription for invalid Subscription ID
    [Tags]           Non-Critical               StopSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${StopSubResp}              process subscription    ${SubJsonObj["Stop_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${StopSubStatus}            compare json object     ${StopSubResp}     ${SubJsonObj["Stop_Error"]["Response"]}
    should be true              True==${StopSubStatus}

ATC078 - Verify for Delete subscription for invalid Subscription ID
    [Tags]           Non-Critical        HTK       DeleteSubscription_Invalid_SubID
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Actions_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${DeleteSubResp}            process subscription    ${SubJsonObj["Delete_Error"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${DeleteSubStatus}          compare json object     ${DeleteSubResp}     ${SubJsonObj["Delete_Error"]["Response"]}
    should be true              True==${DeleteSubStatus}

ATC079 - Verify for Invalid ItemAction -ERROR
    [Tags]           Non-Critical        Invalid_ItemAction
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidItemAction"]["Response"]}
    should be true              True==${createStatus}

ATC080 - Verify for No ItemAction -ERROR
    [Tags]      Non-Critical       No_ItemAction
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoItemAction"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoItemAction"]["Response"]}
    should be true              True==${createStatus}


ATC008- Verify for InvalidRefreshRate -ERROR
    [Tags]      Non-Critical        Invalid_InvalidRefreshRate
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["InvalidRefreshRate"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["InvalidRefreshRate"]["Response"]}
    should be true              True==${createStatus}

ATC081 - Verify for No InvalidRefreshRate -ERROR
    [Tags]      Non-Critical        No_InvalidRefreshRate
    ${JsonFileContent}          get file                ${644H7VariableResources}/HART_644_Process_Subscription_Invalid_Request_Rsc.json
    ${SubJsonObj}               evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${JsonResp}                 process subscription    ${SubJsonObj["NoRefreshRate"]["Request"]}
    ${Status}                   unregister callback     ProcessSubscription
    ${createStatus}             compare json object     ${JsonResp}     ${SubJsonObj["NoRefreshRate"]["Response"]}
    should be true              True==${createStatus}


