*** Settings ***
Documentation    Host Test Suit for HART devices

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

*** Keywords ***
SetAppSettings
    [Arguments]                 ${RESOURCE_FILE}        ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/${RESOURCE_FILE}
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${SetAppSettingsStatus}

ScanDevice
    [Arguments]                 ${NOTIFY_TIMEOUT}
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["StartScan"]["Response"][0]}
    ${StartScanStatus}          compare json object    ${ActualStatus}     ${ExpectedStatus}
    ${ScanResult}               retrieve scan data      ${NOTIFY_TIMEOUT}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]
    [Return]                    ${ScanResult}

ConnectDiffrentAddress
    [Arguments]     ${Status}      ${ExpectedStatus}
    ${GetStatus}                connect                 ${Status}
    sleep       10s
    ${ActualStatus}             set variable            ${GetStatus["CommandName"]}
    sleep       10s
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}
    Run keywords                DisconnectDeviceKW

*** Test Cases ***
# [TODO] communicator is crashing only with ATF, works with IC
#ATC001 - Verify user can Connect with junk device address -ERROR
#    [Tags]     Non-Critical          Connect
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_with_junkaddress_Rsc.json
#    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
#    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetConnectCompStatus}
##
## [TODO] communicator is crashing only with ATF, works with IC
#ATC002 - Verify user can Connect with invalid device address -ERROR
#    [Tags]     Non-Critical          Connect
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_with_invalid_address_Rsc.json
#    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
#    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetConnectCompStatus}
#
#ATC003 - Verify user can Connect with valid device address
#    [Tags]      Critical          Connect
#    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW
#    [Teardown]  run keywords    DisconnectDeviceKW      ShutdownStackKW
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_Rsc.json
#    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    sleep       10s
#    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
#    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             False==${SetConnectCompStatus}

#ATC005 - Verify user can Connect & Disconnect device 5 times
#    [Tags]     Critical          Connect     Disconnect
#    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW
#    [Teardown]  run keywords    ShutdownStackKW
#    ${JsonFileCon}          get file                ${HostResources}/HART_Host_Connect_device_Rsc.json
#    ${SetJsoncon}           evaluate                json.loads('''${JsonFileCon}''')   json
#    ${JsonFiledis}          get file                ${HostResources}/HART_Host_Disconnect_device_Rsc.json
#    ${SetJsondis}           evaluate                json.loads('''${JsonFiledis}''')   json
#    :for    ${Index}    in range    0   1
#        \   sleep       10s
#        \   ${ActualStatus}          connect               ${SetJsoncon["Request"]}
#        \   ${ExpectedNotification}  set variable            ${SetJsoncon['Response']}
#        \   ${NotifyStatus}          compare json object     ${ActualStatus}     ${ExpectedNotification}
#        \   should be true           False==${NotifyStatus}
#        \    ${ActualStatus}         disconnect               ${SetJsondis["Request"]}
#        \   ${ExpectedNotification}  set variable            ${SetJsondis['Response']}
#        \   ${NotifyStatus}          compare json object     ${ActualStatus}     ${ExpectedNotification}
#        \   should be true           True==${NotifyStatus}

ATC006 - Verify user can Connect device when device is already connected -ERROR
    [Tags]      Non-Critical           Connect
    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW     ConnectDeviceKW
    [Teardown]  run keywords    DisconnectDeviceKW      ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_Devices_Again_when_alreadyConnected_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}

#ATC007 - Verify user can Connect with device after disconnect
#    [Tags]      Critical          Connect  Disconnect
#    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW     ConnectDeviceKW     DisconnectDeviceKW
#    [Teardown]  run keywords    DisconnectDeviceKW      ShutdownStackKW
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_Rsc.json
#    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    sleep       10s
#    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
#    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             False==${SetConnectCompStatus}

ATC008 - Verify user can Connect device and shutdown without disconnect-ERROR
    [Tags]      Non-Critical           Connect
    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW     ConnectDeviceKW
    [Teardown]  run keywords    DisconnectDeviceKW      ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_ShutDown_stack_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    should be true              False==${CompStatus}

### Disconnect feature

ATC001 - Verify user can Disconnect the device without connect -ERROR
    [Tags]       Non-Critical          Connect     Disconnect
    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW
    [Teardown]  run keywords   ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Disconnect_fail_Rsc.json
    ${SetDisconnectJsonObj}        evaluate             json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj['Response']}
    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectCompStatus}

#ATC002 - Verify user can Disconnect the connected device
#    [Tags]      Critical          Connect     Disconnect
#    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW     ConnectDeviceKW
#    [Teardown]  run keywords    ShutdownStackKW
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Disconnect_device_Rsc.json
#    ${SetDisconnectJsonObj}     evaluate             json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj['Response']}
#    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetDisconnectCompStatus}

ATC003 - Verify user can perform multiple disconnect -ERROR
    [Tags]      Non-Critical           Disconnect
    [Setup]     run keywords    SetProtocolTypeKW     InitializeStackKW     ConnectDeviceKW    DisconnectDeviceKW
    [Teardown]  run keywords    ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Disconnect_fail_Rsc.json
    ${SetDisconnectJsonObj}     evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetDisconnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetDisconnectJsonObj['Response']}
    ${SetDisconnectCompStatus}  compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetDisconnectCompStatus}



