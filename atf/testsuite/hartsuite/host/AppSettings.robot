*** Settings ***
Documentation    Host Test Suit for HART devices

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot


Suite Teardown  Run Keywords    ShutdownStackKW

*** Test Cases ***

ATC001 - Verify user can set Application Settings without SetProtocol -ERROR
    [Tags]      Non-Critical        SetAppSettings
    [Setup]     run keyword        ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting           ${SetAppSettingsObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}

ATC002 - Verify user can set PollByAddressOption to 0-63
    [Tags]      Critical        AppSettings    SetAppSettings
    [Setup]     run keyword         SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_0-63_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}
     # Get Application Settings and check if it is modified
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_GetAppSettings_modified_Rsc.json
    ${GetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}               set variable            ${GetAppSettingsObj["Response"][2]}
    ${GetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}

ATC003 - Verify user can set PollByAddressOption to 0-15
    [Tags]      Critical        AppSettings    SetAppSettings
    [Setup]     run keyword         SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_0-15_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}
    # Get Application Settings and check if it is modified
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_GetAppSettings_modified_Rsc.json
    ${GetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}               set variable            ${GetAppSettingsObj["Response"][1]}
    ${GetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}

ATC004 - Verify user can set PollByAddressOption to 0
    [Tags]      Critical        AppSettings    SetAppSettings
    [Setup]     run keyword         SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_0_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}
    # Get Application Settings and check if it is modified
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetAppSettings_modified_Rsc.json
    ${GetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetAppSettingsObj["Response"][0]}
    ${GetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}

ATC005 - Verify user can reset Application Settings
    [Tags]      Critical       AppSettings     SetAppSettings
    [Setup]     run keyword         SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_reset_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}
    # Get Application Settings and check if it is modified
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_GetAppSettings_default_Rsc.json
    ${GetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}               set variable            ${GetAppSettingsObj["Response"]}
    ${GetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}

ATC006 - Verify user can set negative value to the Application Settings (PollByAddressOption = -2) -ERROR
    [Tags]      Non-Critical       AppSettings   SetAppSettings
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}

ATC007 - Verify user can set Invalid Application Settings (PollByAddressOption = 4) -ERROR
    [Tags]      Non-Critical       AppSettings   SetAppSettings
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}

ATC008 - Verify user can set float value to the Application Settings (PollByAddressOption = 2.0) -ERROR
    [Tags]      Non-Critical       AppSettings   SetAppSettings
    [Setup]     run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"][2]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}

ATC009 - Verify user can set string value to the Application Settings (PollByAddressOption = "@qwert") -ERROR
    [Tags]      Non-Critical       AppSettings   SetAppSettings
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"][3]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetAppSettingsStatus}

ATC0010 - Verify user can set Application Settings after initialize stack -ERROR
    [Tags]      Critical        SetAppSettings
    [Setup]     run keywords         SetProtocolTypeKW   InitializeStackKW
    [Teardown]  run keyword         ShutdownStackKW
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_SetAppSettings_Invalid_Rsc.json
    ${SetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 set app setting         ${SetAppSettingsObj["Request"][4]}
    ${ExpectedStatus}               set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${SetAppSettingsStatus}

### Get application settings feature

ATC001 - Verify user can get Application Settings without SetProtocol -ERROR
    [Tags]       Non-Critical        GetAppSettings
    [Setup]     run keyword        ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetAppSettings_Without_SetProtocol_Rsc.json
    ${GetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get app setting           ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetAppSettingsObj["Response"][0]}
    ${GetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetAppSettingsStatus}

ATC002 - Verify user can get Application Settings after SetProtocol
    [Tags]      Critical        GetAppSettings
    [Setup]     run keyword         SetProtocolTypeKW
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_GetAppSettings_modified_Rsc.json
    ${GetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}               set variable            ${GetAppSettingsObj["Response"][0]}
    Set to dictionary           ${ExpectedStatus["CommandResponse"]["result"]["HART_APP_SETTINGS"]}   PollByAddressOption=${ActualStatus["CommandResponse"]["result"]["HART_APP_SETTINGS"]["PollByAddressOption"]}
    ${GetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}

# depends on ATC002
ATC003 - Verify user can get Application Settings after initialize stack -ERROR
    [Tags]      Non-Critical        GetAppSettings
    [Setup]     run keywords         SetProtocolTypeKW   InitializeStackKW
    ${JsonFileContent}              get file                ${HostResources}/HART_Host_GetAppSettings_modified_Rsc.json
    ${GetAppSettingsObj}            evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}                 get app setting         ${GetAppSettingsObj["Request"]}
    ${ExpectedStatus}               set variable            ${GetAppSettingsObj["Response"][3]}
    ${GetAppSettingsStatus}         compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true                  True==${GetAppSettingsStatus}


