*** Settings ***
Documentation    Host Test Suit for HART devices

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown  run Keyword    ShutdownStackKW

*** Keywords ***
GetBusParameters
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetBusParam_Default_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}

*** Test Cases ***
ATC001 - Verify user can set Bus Parameter without SetProtocol - ERROR
    [Tags]      Critical        BusParameters      SetBusParameters
    [Setup]     run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_without_setprotocol_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][1]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

ATC002 - Verify user can set valid Bus Parameter after SetProtocol(master Type = 1 (Primary))
    [Tags]      Critical        BusParameters      SetBusParameters
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

ATC003 - Verify user can reset the Bus parameter settings
    [Tags]      Critical        BusParameters      SetBusParameters
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Reset_Rsc.json
    ${SetBusDefaultParamObj}    evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusDefaultParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusDefaultParamObj["Response"]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}
    # Get Bus Parameter and check if default values are reset
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetBusParam_Default_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetBusParamStatus}        compare json object     ${ActualStatus}  ${ExpectedStatus}
    should be true              True==${GetBusParamStatus}

ATC004 - Verify user can set invalid Bus Parameter (master type = 22) -ERROR
    [Tags]      Non-Critical        BusParameters        SetBusParameters
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Invalid_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"][0]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][0]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

ATC005 - Verify user can set negative value to the set Bus Parameter (masterType = -1) -ERROR
    [Tags]      Non-Critical         BusParameters        SetBusParameters
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Invalid_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"][1]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][0]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

ATC006 - Verify user can set float value to the set Bus Parameter (masterType = 1.0)
    [Tags]      Non-Critical         BusParameters        SetBusParameters
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Invalid_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"][2]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][0]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

ATC007 - Verify user can set string value to the set Bus Parameter (masterType = "@qwert") -ERROR
    [Tags]      Non-Critical        BusParameters        SetBusParameters
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_Invalid_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"][4]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][0]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}


ATC008 - Verify user can set Bus Parameter after initialize stack -ERROR
    [Tags]      Critical        BusParameters      SetBusParameters
    [Setup]         run keywords        SetProtocolTypeKW   InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetBusParam_without_setprotocol_Rsc.json
    ${SetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set bus param           ${SetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetBusParamObj["Response"][1]}
    ${SetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetBusParamStatus}

### Get bus parameters feature

ATC001 - Verify user can get Bus Parameter without SetProtocol - ERROR
    [Tags]      Non-Critical        BusParameters       GetBusParameters
    [Setup]     run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetBusParam_without_SetProtocol_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBusParamStatus}

ATC002 - Verify user can get Bus Parameter after SetProtocol
    [Tags]      Critical        BusParameters       GetBusParameters
    [Setup]         run keyword     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_GetBusParam_Modified_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBusParamStatus}

ATC003 - Verify user can get Bus Parameter after initialize stack -ERROR
    [Tags]      Non-Critical         Initialize     GetBusParameters
    [Setup]         run keywords        SetProtocolTypeKW   InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_getBusParam_after_initilizedstack_Rsc.json
    ${GetBusParamObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             get bus Param           ${GetBusParamObj["Request"]}
    ${ExpectedStatus}           set variable            ${GetBusParamObj["Response"]}
    ${GetBusParamStatus}        compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${GetBusParamStatus}
