*** Settings ***
Documentation    Set Protocol Feature will be verified with below test cases

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
ATC001 - Verify user can set protocol type with empty string -ERROR
    [Tags]  Non-Critical     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_empty_string_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj["Response"]}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC002 - Verify user can set required protocol type with invalid string "@Send" -ERROR
    [Tags]  Non-Critical     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_invalid_string_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response'][1]}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC003 - Verify user can set required protocol type as "HART"
    [Tags]  Critical        SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC004 - Verify user can set HART protocol again when HART protocol is already set -ERROR
    [Tags]  Non-Critical     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_Set_HART_protocol_again_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC005 - Verify user can set FF Protocol when HART protocol is already set -ERROR
    [Tags]  Non-Critical      SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_HART_then_set_FF_protocol_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

ATC006 - Verify user can set invalid Protocol when HART protocol is already set -ERROR
    [Tags]  Non-Critical      SetProtocolTypeKW
    [Teardown]  Run Keywords     InitializeStackKW
    ...                 AND       ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_invalid_string_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response'][1]}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

