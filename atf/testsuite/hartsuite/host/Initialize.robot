*** Settings ***
Documentation    Host Test Suit for HART devices

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

Suite Teardown      ShutdownStackKW

*** Test Cases ***
ATC001 - Verify user can initialize the HART stack before SetProtocol -ERROR
    [Tags]      Critical        Initialize
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_without_protocolSet_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}
    
ATC002 - Verify user can initialize HART stack After Set Protocol
    [Tags]      Critical        Initialize
    [Setup]     run keyword     SetProtocolTypeKW
    [Teardown]  run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}
    
ATC003 - Verify user can initialize the HART stack when the stack is already initialized -ERROR
    [Tags]      Non-Critical        Initialize
    [Setup]     run keywords     SetProtocolTypeKW       InitializeStackKW
    [Teardown]  run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_Re_InitializeProtocol_Rsc.json
    ${InitializeAgainJsonObj}   evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeAgainJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeAgainJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC004 - Verify user can set FF Protocol when HART protocol is already set and initilized -ERROR
    [Tags]  Non-Critical      SetProtocol
    [Setup]     run keywords    SetProtocolTypeKW       InitializeStackKW
    [Teardown]  run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_HART_then_set_FF_protocol_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}

