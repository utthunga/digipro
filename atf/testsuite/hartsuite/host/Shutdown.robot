*** Settings ***
Documentation    Host Test Suit for HART devices

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot

*** Test Cases ***
ATC001 - Verify user can shutdown the HART protocol after HART stack is initialized
    [Tags]  Critical        Shutdown
    [Setup]     run Keywords     SetProtocolTypeKW    InitializeStackKW
    [Teardown]      run keyword    ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_ShutDown_stack_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       1s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"][0]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC002 - Verify user can set and initialize the HART protocol after shutdown
    [Tags]  Critical        SetProtocol    Initialize  Shutdown
    [Setup]     run keywords    ShutdownStackKW     SetProtocolTypeKW
    [Teardown]  run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC003 - Verify user can set & Initialize FF Protocol when HART protocol is shutdown(FF Environment is set)
   [Tags]  Critical        SetProtocol    Initialize  Shutdown ff_environment_set
    [Setup]     run keywords    SetProtocolTypeKW    InitializeStackKW     ShutdownStackKW
    [Teardown]  run keyword     ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_setprotocal_FF_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC004 - Verify user can set & Initialize FF Protocol when HART protocol is shutdown(FF Environment is not set)
    [Tags]  Critical        SetProtocol    Initialize  Shutdown
    [Setup]     Run Keywords     SetProtocolTypeKW    InitializeStackKW     ShutdownStackKW
    [Teardown]  run keyword      ShutdownStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_setprotocal_FF_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${SetProtocolCompStatus}    compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetProtocolCompStatus}
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_without_protocolSet_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${InitializeCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${InitializeCompStatus}

ATC005 - Verify user can perform Shutdown without SetProtocol -ERROR
    [Tags]  Non-Critical        Shutdown
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_ShutDown_without_setProtocol_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       1s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

ATC006 - Verify user can perform Shutdown when scan is in progress -ERROR
    [Tags]  Non-Critical        Scanning    Shutdown
    [Setup]     run Keywords    SetProtocolTypeKW   InitializeStackKW   StartScanKW
    [Teardown]  run keywords     StopScanKW         ShutdownStackKW
    ${JsonFileContent}         get file                ${HostResources}/HART_Host_ShutDown_stack_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}            shutdown                ${ShutdownJsonObj["Request"]}
    sleep                      1s
    ${ExpectedStatus}          set variable            ${ShutdownJsonObj["Response"][1]}
    ${ShutdownCompStatus}      compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${ShutdownCompStatus}


ATC007 - Verify user can perform Shutdown after SetProtocol and before Initialize -ERROR
    [Tags]      Non-Critical           Shutdown
    [Setup]     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Shutdown_without_Initialize_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       1s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"]}
    ${ShutdownCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${ShutdownCompStatus}

#ATC008 - Verify user can Connect and Shutdown without Disconnect device -ERROR
#    [Tags]     Non-Critical          Connect
#    [Setup]    run Keywords     SetProtocolTypeKW
#    ...           AND           InitializeStackKW
#    ...           AND            ConnectDeviceKW
#    ${JsonFileContent}          get file                ${HostResources}/HART_Host_ShutDown_stack_Rsc.json
#    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
#    ${ActualStatus}             shutdown                 ${SetConnectJsonObj["Request"]}
#    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response'][1]}
#    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetConnectCompStatus}
#    sleep                       1s
#    [Teardown]                  run keywords             DisconnectDeviceKW
#    ...                         AND                      ShutdownStackKW