*** Settings ***
Documentation    Host Test Suit for HART devices


Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Resource    ../suite_resource/common.robot


*** Keywords ***
SetApplicationSettings
    [Arguments]     ${FileName}
    ${JsonFileContent}          get file                ${HostResources}/${FileName}
    ${SetAppSettingsObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set app setting         ${SetAppSettingsObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetAppSettingsObj["Response"]}
    ${SetAppSettingsStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${SetAppSettingsStatus}

PerformScan
    [Arguments]     ${NotifyTimeout}  ${JsonKey}
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ScanResult}               retrieve scan data       ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj[${JsonKey}]}
    ${ScanStatus}               compare json object     ${ScanResult}     ${ExpectedStatus}
    [Return]                    ${ScanStatus}

ScanDevice
    ${NotifyTimeout}            convert to integer      20
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ScanResult}               retrieve scan data      ${NotifyTimeout}
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]
    [Return]                   ${ScanResult}

*** Test Cases ***
#TODO: ATC003 is not running if this test case is run. Need to check with Santhosh
ATC001 - Verify the behavior when user performs start scan without SetProtocol - ERROR
   [Tags]      Non-Critical        Scanning
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["StartScan"]["Response"][2]}
    ${ScanStatus}               compare json object     ${StartStatus}     ${ExpectedStatus}
                                unregister callback     Scan
    should be true              True==${ScanStatus}


ATC002 - Verify the behavior when user performs start scan without InitializeProtocol - ERROR
   [Tags]      Non-Critical        Scanning
   [Setup]     SetProtocolTypeKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["StartScan"]["Response"][1]}
    ${ScanStatus}               compare json object     ${StartStatus}     ${ExpectedStatus}
                                unregister callback     Scan
    should be true              True==${ScanStatus}


ATC003 - Verify user can perform Scan for Poll Address 0
    [Tags]      Critical        Scanning
    [Setup]     Run Keywords    SetProtocolTypeKW
    ...         AND             SetApplicationSettings      HART_Host_SetAppSettings_0_Rsc.json
    ...         AND             InitializeStackKW
    ${NotifyTimeout}            convert to integer      20
    ${ResultJsonKey}            set variable            "Notification0"
    ${ScanStatus}               PerformScan             ${NotifyTimeout}    ${ResultJsonKey}
    should be true              True==${ScanStatus}
    [Teardown]      ShutdownStackKW

ATC004 - Verify user can perform Scan for Poll Address 0 to 15
    [Tags]      Critical        Scanning        Multidrop
    [Setup]     Run Keywords    SetProtocolTypeKW
    ...         AND             SetApplicationSettings  HART_Host_SetAppSettings_0-15_Rsc.json
    ...         AND             InitializeStackKW
    ${NotifyTimeout}            convert to integer      40
    ${ResultJsonKey}            set variable            "Notification0-15"
    ${ScanStatus}               PerformScan             ${NotifyTimeout}    ${ResultJsonKey}
    should be true              True==${ScanStatus}
    [Teardown]      ShutdownStackKW

ATC005 - Verify user can perform Scan for Poll Address 0 to 63
    [Tags]      Critical        Scanning        Multidrop
    [Setup]     Run Keywords    SetProtocolTypeKW
    ...         AND             SetApplicationSettings     HART_Host_SetAppSettings_0-63_Rsc.json
    ...         AND             InitializeStackKW
    ${NotifyTimeout}            convert to integer      120
    ${ResultJsonKey}            set variable            "Notification0-63"
    ${ScanStatus}               PerformScan             ${NotifyTimeout}    ${ResultJsonKey}
    should be true              True==${ScanStatus}
    [Teardown]      ShutdownStackKW

ATC006 - Verify user can perform Start Scan when scan is already in progress -ERROR
    [Tags]      Non-Critical        Scanning
    [Setup]     Run Keywords    SetProtocolTypeKW
    ...         AND             InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Start_Scan_Error_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StartStatus}              start scan              ${ScanJsonObj["Request"]}
    ${StartStatus}              start scan              ${ScanJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Response"]}
    ${ScanStatus}               compare json object     ${StartStatus}     ${ExpectedStatus}
    should be true              True==${ScanStatus}
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StopStatus}               stop scan               ${ScanJsonObj["StopScan"]["Request"]
    [Teardown]  ShutdownStackKW

ATC007 - Verify user can perform Stop Scan without start scan -ERROR
     [Tags]      Non-Critical        Scanning
    [Setup]     Run Keywords    SetProtocolTypeKW
    ...         AND             InitializeStackKW
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Stop_Scan_Error_Rsc.json
    ${ScanJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
    ${StopStatus}               stop scan               ${ScanJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${ScanJsonObj["Response"]}
    ${StopStatus}               compare json object     ${StopStatus}     ${ExpectedStatus}
    should be true              True==${StopStatus}
   [Teardown]  ShutdownStackKW

ATC0010 - Verify the behavior when user performs start scan and Connect Device without stop scan
    [Tags]      Critical        Scanning
    [Setup]     run keywords   SetProtocolTypeKW
     ...         AND           InitializeStackKW
     ...         AND           StartScanKW
    [Teardown]  run keywords   StopScanKW       ShutdownStackKW
    # Connect
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_Devices_Again_when_alreadyConnected_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    ${SetConnectCompStatus}     compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${SetConnectCompStatus}








