*** Settings ***
Documentation    This test suite contains the test cases for EDIT_DISPLAY
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDeviceKW         "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${SuperDDEditdisplay}          ${SuperDDResource}/Editdisplay
${SuperDDWriteASOne}           ${SuperDDResource}/WriteAsOne


*** Test Cases ***
ATC001 - Verify user can fetch Edit_Display item as Menu
    [Tags]           Critical        Menu    Edit_Display
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Edit_Display_Menu_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                               unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify user can fetch Edit_Display as Menu item in a Menu
    [Tags]           Critical        Variable    Edit_Display
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Edit_Display_Menu_variable_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                               unregister callback      GetItem
     set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][2]["ItemInfo"]}      ImagePath=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][2]["ItemInfo"]["ImagePath"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}
    
ATC003 - Verify user can fetch Edit Items as read/write inside EDIT_DISPLAY
    [Tags]  Critical    Variable    EDIT_ITEMS
    ##get dictionary items
    ${JsonFileContent}          get file               ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Edit_items_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Edit_items_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC004 - Verify user can fetch Display_Items as Read Only items inside EDIT_DISPLAY
    [Tags]           Critical      Variables    DISPLAY_ITEMS
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Display_items_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
                                unregister callback     GetItem

ATC005 - Verify user can fetch Write_As_one as part of Edit_Display
    # fetch both Edit_Display, Write_AS_One in single menu
    [Tags]  Critical    Variable    EDIT_ITEMS
    {JsonFileContent}          get file                ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Items_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                               unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC006 - Verify PRE-EDIT Actions are performed for EDIT_DISPLAY
    [Tags]  Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Variable_PREEDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       ExecPreEditAction
    ${SetItemResponse}          Exec_Pre_Edit_Action               ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      ExecPreEditAction     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     ExecPreEditAction
                                register callback       SetItem

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
     :for    ${Index}    in range    0   3
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
     should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
     :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

    ${SetItemResponse}             set item                ${JsonObj["Request"][0]}
    ${SetItemStatus}               set variable            ${JsonObj['Response'][2]}
    set to dictionary              ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${VariableCompStatus}          compare json object     ${SetItemResponse}     ${SetItemStatus}
    should be true                 True==${VariableCompStatus}
                                   unregister callback     SetItem

ATC007 - Verify POST-EDIT Actions are performed for EDIT_DISPLAY
    [Tags]  Critical    Variable    EDIT_ACTIONS    Post_EDIT_ACTIONS
    ${JsonFileContent}          get file                ${SuperDDEditdisplay}/HART_SuperDD_Variable_POSTEDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       ExecPostEditAction
    ${SetItemResponse}          Exec_Post_Edit_Action               ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      ExecPostEditAction     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
                                unregister callback     ExecPostEditAction
                                register callback       SetItem

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
     :for    ${Index}    in range    0   3
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
     should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}

    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
   :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

    ${SetItemResponse}             set item                ${JsonObj["Request"][5]}
    ${SetItemStatus}               set variable            ${JsonObj['Response'][2]}
    set to dictionary              ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${VariableCompStatus}          compare json object     ${SetItemResponse}     ${SetItemStatus}
    should be true                 True==${VariableCompStatus}
                                   unregister callback     SetItem

ATC0010 - Verify user cannot fetch EDit_display with Invalid ItemID =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Edit_items_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC0011 - Verify user cannot fetch EDit_display with Invalid ItemInDD =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Edit_items_Error_Rsc.json
   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC0012 - Verify user cannot fetch EDit_display with Invalid ItemType =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDEditdisplay}/HART_SuperDD_Edit_display_Edit_items_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
