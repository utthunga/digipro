*** Settings ***
Documentation    This test suite contains the test cases for Select Menu and Menu Navigation
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDeviceKW         "TRUE"
Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${SuperDDMenuResources}          ${SuperDDResource}/menu

*** Keywords ***
SetValidityValue
    [Arguments]         ${arg}
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/${arg}
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Variable"]["Request"]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj["Variable"]['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

*** Test Cases ***
ATC001 - Verify user can fetch the Menu attributes and Menu items
    [Tags]           Critical        Menu
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Items_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Menu"]["Request"]}
                                unregister callback     GetItem
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]}      ImagePath=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]["ImagePath"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]["VarValue"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][4]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][4]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj["Menu"]['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify Menu Qualifiers DISPLAY_VALUE, READ_ONLY, NO_LABEL, NO_UNIT are applied for a menu
    [Tags]           Critical        Menu       Menu_Qualifiers
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Qualifiers_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC003 - Verify the following Menu Items are displayed in a menu When the VALIDITY condition is met
    [Tags]           Critical        Menu     Menu_Validity
    [Setup]          SetValidityValue       HART_SuperDD_Menu_Items_Rsc.json
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Items_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Menu"]["Request"]}
                                unregister callback      GetItem
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]}      ImagePath=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]["ImagePath"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]["VarValue"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][4]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][4]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${ExpectedStatus}           set variable            ${MenuJsonObj["Menu"]['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC004 - Verify the following Menu Items are displayed in a menu When the VALIDITY condition is not met
    [Tags]           Critical        Menu     Menu_Validity
    [Setup]          SetValidityValue       HART_SuperDD_Menu_Validity_Fail_Rsc.json
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Validity_Fail_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Menu"]["Request"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][0]["ItemInfo"]["VarInfo"]["VarValue"]}
    set to dictionary           ${MenuJsonObj["Menu"]["Response"]["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]["VarInfo"]}      VarValue=${ActualStatus["CommandResponse"]["result"]["ItemInfo"]["ItemList"][1]["ItemInfo"]["VarInfo"]["VarValue"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj["Menu"]['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC005 - Verify the variables of types (INTEGER, UNSIGNED INTEGER, FLOAT, DOUBLE, ASCII, Packed ASCII, PASSWORD, DATE, TIME, ENUMERATED, BIT-ENUMERATED) are displayed in a menu
    [Tags]           Critical        Menu     Variables
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Variable_Items_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC006 - Verify the variables of types (INTEGER, UNSIGNED INTEGER, FLOAT, DOUBLE, ASCII, Packed ASCII, PASSWORD, DATE, TIME, ENUMERATED, BIT-ENUMERATED) default values are displayed are displayed in a menu
    [Tags]           Critical        Menu     Variables
    ${JsonFileContent}          get file                ${SuperDDMenuResources}/HART_SuperDD_Menu_Variables_Default_Values_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                                unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}


