*** Settings ***
Documentation    This test suite contains the test cases for Select Menu and Menu Navigation
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDeviceKW         "TRUE"
Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${SuperDDWriteASOne}           ${SuperDDResource}/WriteAsOne



*** Test Cases ***


ATC001 - Verify user can fetch the WRITE_AS_ONE Item as Menu Type
   [Tags]           Critical        Menu     Variables
    ${JsonFileContent}          get file                ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Items_Rsc.json
    ${MenuJsonObj}              evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${MenuJsonObj["Request"]}
                               unregister callback      GetItem
    ${ExpectedStatus}           set variable            ${MenuJsonObj['Response']}
    ${MenuCompStatus}           compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${MenuCompStatus}

ATC002 - Verify user can fetch Write_As_One as Variable type in Menu
   [Tags]           Critical          Variables
    ${JsonFileContent}          get file                ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Variable_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC003 - Verify user can write the WRITE_AS_ONE variable
   [Tags]  Critical            write   Variable
    ${JsonFileContent}          get file               ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ##get dictionary items
    ${JsonFileContent}          get file                ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC004 - Verify user cannot fetch WRITE_AS_ONE with Invalid ItemID =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC005 - Verify user cannot fetch WRITE_AS_ONE with Invalid ItemInDD =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Error_Rsc.json
   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC006 - Verify user cannot fetch WRITE_AS_ONE with Invalid ItemType =Error
    [Tags]  Critical    Menu    Invalid
    ${JsonFileContent}          get file               ${SuperDDWriteASOne}/HART_SuperDD_Write_As_One_Error_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                   register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][2]}
                                         unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}




