*** Settings ***
Documentation    This test suite contains the test cases for Select Menu and Menu Navigation
Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections

Resource    hartsuite/suite_resource/common.robot

Suite Setup     Run Keywords    SetProtocolTypeKW
...             AND             SetLangCodeEnKW
...             AND             InitializeStackKW       "TRUE"
...             AND             ConnectDeviceKW         "TRUE"

Suite Teardown  Run Keywords    DisconnectDeviceKW      "TRUE"
...             AND             ShutdownStackKW         "TRUE"

*** Variables ***
${SuperDDVariableResources}          ${SuperDDResource}/variable

*** Keywords ***
GetTimeValueVariable
    [Arguments]         ${arg1}     ${arg2}     ${arg3}
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/${arg1}
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][${arg2}]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][${arg3}]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    [Return]        ${VariableCompStatus}

SetTimeValueVariable
    [Arguments]         ${arg1}     ${arg2}
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Modify_TIME_VALUE_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][${arg1}]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][${arg2}]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    [Return]        ${VariableCompStatus}

GetIntergerSizeVariable
    [Arguments]         ${arg1}     ${arg2}     ${arg3}
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/${arg1}
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][${arg2}]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][${arg3}]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    [Return]        ${VariableCompStatus}

SetIntergerSizeVariable
    [Arguments]         ${arg1}     ${arg2}     ${arg3}
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/${arg1}
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][${arg2}]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][${arg3}]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    [Return]        ${VariableCompStatus}

*** Test Cases ***
########################################################################################################################
########################################### Double type variable  ######################################################
########################################################################################################################
ATC001 - Verify user can get item for variable type DOUBLE and attributes
    [Tags]  Critical    Variable    Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC002 - Verify user can get item for variable type DOUBLE and attributes with default values
    [Tags]  Critical    Variable    Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC003 - Verify user can write value to the variable type Double within the MIN_VALUE1 and MAX_VALUE1 range
    [Tags]  Critical    Variable    Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][0]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ##get dictionary items
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC004 - Verify user can write value to the variable type Double within the MIN_VALUE2 and MAX_VALUE2 range
    [Tags]  Critical    Variable    Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][5]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ##get dictionary items
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC005 - Verify user can write value to the variable type Double with Invalid value(string) -ERROR
   [Tags]  Non-Critical    Variable      Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC006 - Verify user can write value to the variable type Double more than the allowed data type range -ERROR
   [Tags]  Non-Critical    Variable   Double_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

# TODO : Below test cases will be enabled once MIN/MAX validation is done in CPM
#ATC007 - Verify user can write out of range value to the variable type Double more than the MAX_VALUE1 -ERROR
#   [Tags]  Non-Critical    Variable   Double_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][6]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}

#ATC008 - Verify user can write out of range value to the variable type Double less than the MIN_VALUE1(negative value) -ERROR
#    [Tags]  Non-Critical    Variable  Double_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}

#ATC009 - Verify user can write out of range value to the variable type Double inbetween MAX_VALUE1 and MIN_VALUE2 -ERROR
#   [Tags]  Non-Critical    Variable   Double_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Double_Type_Rsc.json
#    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                                register callback       SetItem
#    ${ActualStatus}             set item                ${VariableJsonObj["Request"][7]}
#                                unregister callback     SetItem
#    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true             True==${VariableCompStatus}

########################################################################################################################
########################################### Integer Type variable  #####################################################
########################################################################################################################
ATC001 - Verify user can get item for variable type INTEGER and attributes
    [Tags]  Critical    Variable    Integer_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC001 - Verify user can get item for variable type INTEGER and attributes with default values
    [Tags]  Critical    Variable    Integer_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC003 - Verify user can write value to the variable type INTEGER within the MIN_VALUE1 and MAX_VALUE1 range
    [Tags]  Critical    Variable    Integer_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][0]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ###GetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC004 - Verify user can write value to the variable type INTEGER within the MIN_VALUE2 and MAX_VALUE2 range
    [Tags]  Critical    Variable    Integer_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ###GetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Integer_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

# TODO : Below test cases will be enabled once MIN/MAX validation is done in CPM
#ATC007 - Verify user can write out of range value to the variable type INTEGER more than the MAX_VALUE2 -ERROR
#   [Tags]  Non-Critical   Variable   Integer_Type
#   ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Integer_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}
#
#ATC009 - Verify user can write out of range value to the variable type INTEGER inbetween MAX_VALUE1 and MIN_VALUE2 -ERROR
#   [Tags]  Non-Critical    Variable   Integer_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Integer_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}
#
#ATC009 - Verify user can write out of range value to the variable type INTEGER less than MIN_VALUE1 -ERROR
#   [Tags]  Non-Critical    Variable   Integer_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_Integer_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}

########################################################################################################################
########################################### Unsigned Integer Type variable  ####################################################
########################################################################################################################
ATC001 - Verify user can get item for variable type UINT and attributes
    [Tags]  Critical    Variable    UINT_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

ATC001 - Verify user can get item for variable type UINT and attributes with default values
    [Tags]  Critical    Variable    UINT_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC003 - Verify user can write value to the variable type UINT within the MIN_VALUE1 and MAX_VALUE1 range
    [Tags]  Critical    Variable    UINT_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][0]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ###GetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}


ATC004 - Verify user can write value to the variable type UINT within the MIN_VALUE2 and MAX_VALUE2 range
    [Tags]  Critical    Variable    UINT_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][1]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][0]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}
    ###GetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_UInteger_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][3]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true             True==${VariableCompStatus}

# TODO : Below test cases will be enabled once MIN/MAX validation is done in CPM
#ATC006 - Verify user can write out of range value to the variable type UINT more than the MAX_VALUE2 -ERROR
#   [Tags]  Non-Critical   Variable   UINT_Type
#   ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_UInteger_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}
#
#ATC007 - Verify user can write out of range value to the variable type UINT less MIN_VALUE1 -ERROR
#   [Tags]  Non-Critical    Variable   UINT_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_UInteger_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}
#
#ATC007 - Verify user can write out of range value to the variable type UINT inbetween MAX_VALUE1 and MIN_VALUE2 -ERROR
#   [Tags]  Non-Critical    Variable   UINT_Type
#    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_Super_DD_Variable_Modify_UInteger_Type_Rsc.json
#   ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
#                               register callback       SetItem
#   ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
#                               unregister callback     SetItem
#   ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
#   ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#   should be true             True==${VariableCompStatus}

########################################################################################################################
########################################### Password Type variable  ####################################################
########################################################################################################################
ATC001 - Verify user can get item for Variable Type PASSWORD with attributes
    [Tags]  Critical    Variable    Password_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

ATC002 - Verify user can get item for Variable Type PASSWORD with default values for attributes
    [Tags]  Critical    Variable    Password_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][1]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][1]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

ATC003 - Verify user can write value for Variable Type PASSWORD with allowed length
    [Tags]  Critical    Variable    Password_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][2]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][4]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       GetItem
    ${ActualStatus}             get item                ${VariableJsonObj["Request"][0]}
                                unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][2]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

ATC004 - Verify user can write value for Variable Type PASSWORD with more length than allowed
    [Tags]  Critical    Variable    Password_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][3]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][5]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

ATC005 - Verify user can write invalid value for Variable Type PASSWORD -ERROR
    [Tags]  Non-Critical    Variable  Password_Type
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_Password_Type_Rsc.json
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"][4]}
                                unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response'][5]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}

########################################################################################################################
########################################### Time Value variable  #######################################################
########################################################################################################################
ATC001 - Verify user can get TIME VALUE Variable and the attributes
    [Tags]  Non-Critical    Variable    Time_Value_Type
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable            HART_SuperDD_Variable_TIME_VALUE_Default_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC002 - Verify user can get TIME VALUE Variable with TIME_SCALE unit defined as HOURS
    [Tags]  Non-Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable            HART_SuperDD_Variable_TIME_VALUE_Default_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC003 - Verify user can get TIME VALUE Variable with TIME_SCALE unit defined as SECONDS
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable            HART_SuperDD_Variable_TIME_VALUE_Default_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC004 - Verify user can get TIME VALUE Variable with TIME_SCALE unit defined as MINUTES
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      3
    ${FileName}                set variable            HART_SuperDD_Variable_TIME_VALUE_Default_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC005 - Verify user can get TIME VALUE Variable with TIME_FORMAT defined
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      4
    ${ResponseIndex}           convert to integer      4
    ${FileName}                set variable            HART_SuperDD_Variable_TIME_VALUE_Default_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC006 - Verify user can set value for TIME VALUE Variable with TIME_SCALE unit defined as HOURS
    [Tags]  Critical    Variable            Time_Value_Type
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}
    ${FileName}                set variable            HART_SuperDD_Variable_Get_TIME_VALUE_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC007 - Verify user can set value for TIME VALUE Variable with TIME_SCALE unit defined as SECONDS
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      0
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable            HART_SuperDD_Variable_Get_TIME_VALUE_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC008 - Verify user can set value for TIME VALUE Variable with TIME_SCALE unit defined as MINUTES
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      0
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable            HART_SuperDD_Variable_Get_TIME_VALUE_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC009 - Verify user can set value for TIME VALUE Variable with TIME_FORMAT defined
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      0
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}
     ${RequestIndex}            convert to integer     3
    ${ResponseIndex}           convert to integer      3
    ${FileName}                set variable            HART_SuperDD_Variable_Get_TIME_VALUE_Values_Rsc.json
    ${ActualStatus}            GetTimeValueVariable    ${FileName}  ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC010 - Verify user can set out of range value than data type range for the TIME VALUE variable
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      5
    ${ResponseIndex}           convert to integer      1
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC011 - Verify user can set negative value for the TIME VALUE variable
    [Tags]  Non-Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      4
    ${ResponseIndex}           convert to integer      1
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC012 - Verify user can set float value for the TIME VALUE variable -ERROR
    [Tags]  Non-Critical   Variable     Time_Value_Type
    ${RequestIndex}            convert to integer      7
    ${ResponseIndex}           convert to integer      1
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC013 - Verify user can set string value for the TIME VALUE variable -ERROR
    [Tags]  Critical    Variable        Time_Value_Type
    ${RequestIndex}            convert to integer      5
    ${ResponseIndex}           convert to integer      1
    ${ActualStatus}            SetTimeValueVariable    ${RequestIndex}     ${ResponseIndex}
    should be true             True==${ActualStatus}

#Test Size attributes
ATC001 - Verify user can write value for variable type INTEGER SIZE 1
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer         1
    ${ResponseIndex}           convert to integer         1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC002 - Verify user can get value and attributes for variable type INTEGER SIZE 1
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer         0
    ${ResponseIndex}           convert to integer         0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_1_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC003 - Verify user can write value more than max range for variable type INTEGER SIZE 1-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC004 - Verify user can write value less than min range for variable type INTEGER SIZE 1-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC006 - Verify user can write value for variable type INTEGER SIZE 2
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC007 - Verify user can get value and attributes for variable type INTEGER SIZE 2
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_2_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC008 - Verify user can write value more than max range for variable type INTEGER SIZE 2-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
     should be true             True==${ActualStatus}
ATC009 - Verify user can write value less than min range for variable type INTEGER SIZE 2 -ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC010 - Verify user can write value for variable type INTEGER SIZE 3
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC011 - Verify user can get value and attributes for variable type INTEGER SIZE 3
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_3_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

##issue accepting value more than and min than given range
ATC012 - Verify user can write value more than max range for variable type INTEGER SIZE 3 -ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}
##issue
ATC013 - Verify user can write value less than min range for variable type INTEGER SIZE 3 -ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC014 - Verify user can write value for variable type INTEGER SIZE 4
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC015 - Verify user can get value and attributes for variable type INTEGER SIZE 4
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_4_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC016 - Verify user can write value more than max range for variable type INTEGER SIZE 4-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}
ATC017 - Verify user can write value less than min range for variable type INTEGER SIZE 4-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC018 - Verify user can write value for variable type INTEGER SIZE 5
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC019 - Verify user can get value and attributes for variable type INTEGER SIZE 5
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_5_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC020 - Verify user can write value more than max range for variable type INTEGER SIZE 5-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}
ATC021 - Verify user can write value less than min range for variable type INTEGER SIZE 5-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC022 - Verify user can write value for variable type INTEGER SIZE 6
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC023 - Verify user can get value and attributes for variable type INTEGER SIZE 6
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_6_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC024 - Verify user can write value more than max range for variable type INTEGER SIZE 6-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC025 - Verify user can write value less than min range for variable type INTEGER SIZE 6-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC026 - Verify user can write value for variable type INTEGER SIZE 7
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC027 - Verify user can get value and attributes for variable type INTEGER SIZE 7
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_7_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC028 - Verify user can write value more than max range for variable type INTEGER SIZE 7-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC029 - Verify user can write value less than min range for variable type INTEGER SIZE 7-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC030 - Verify user can write value for variable type INTEGER SIZE 8
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_8_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC031 - Verify user can get value and attributes for variable type INTEGER SIZE 8
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_8_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

#ATC032 - Verify user can write value more than max range for variable type INTEGER SIZE 8-ERROR
#    [Tags]  Critical    Variable    Integer_Size
#    ${RequestIndex}            convert to integer      2
#    ${ResponseIndex}           convert to integer      2
#    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_8_Rsc.json
#    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
#    should be true             True==${ActualStatus}

ATC033 - Verify user can write value less than min range for variable type INTEGER SIZE 8-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_Integer_Size_8_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

## Test size attributes for UINT
ATC001 - Verify user can write value for variable type UINT SIZE 1
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer         1
    ${ResponseIndex}           convert to integer         1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC002 - Verify user can get value and attributes for variable type UINT SIZE 1
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer         0
    ${ResponseIndex}           convert to integer         0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_1_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC003 - Verify user can write value more than max range for variable type UINT SIZE 1-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC004 - Verify user can write value less than min range for variable type UINT SIZE 1-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_1_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC006 - Verify user can write value for variable type UINT SIZE 2
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC007 - Verify user can get value and attributes for variable type UINT SIZE 2
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_2_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC008 - Verify user can write value more than max range for variable type UINT SIZE 2-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
     should be true             True==${ActualStatus}
ATC009 - Verify user can write value less than min range for variable type UINT SIZE 2 -ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_2_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC010 - Verify user can write value for variable type UINT SIZE 3
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC011 - Verify user can get value and attributes for variable type UINT SIZE 3
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_3_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

###issue accepting value more than and min than given range
ATC012 - Verify user can write value more than max range for variable type UINT SIZE 3 -ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

###issue
ATC013 - Verify user can write value less than min range for variable type UINT SIZE 3 -ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_3_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC014 - Verify user can write value for variable type UINT SIZE 4
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC015 - Verify user can get value and attributes for variable type UINT SIZE 4
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_4_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC016 - Verify user can write value more than max range for variable type UINT SIZE 4-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC017 - Verify user can write value less than min range for variable type UINT SIZE 4-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_4_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC018 - Verify user can write value for variable type UINT SIZE 5
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC019 - Verify user can get value and attributes for variable type UINT SIZE 5
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_5_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC020 - Verify user can write value more than max range for variable type UINT SIZE 5-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC021 - Verify user can write value less than min range for variable type UINT SIZE 5-ERROR
    [Tags]  Critical    Variable    Integer_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_5_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC022 - Verify user can write value for variable type UINT SIZE 6
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC023 - Verify user can get value and attributes for variable type UINT SIZE 6
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_6_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC024 - Verify user can write value more than max range for variable type UINT SIZE 6-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC025 - Verify user can write value less than min range for variable type UINT SIZE 6-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_6_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC026 - Verify user can write value for variable type UINT SIZE 7
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC027 - Verify user can get value and attributes for variable type UINT SIZE 7
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_7_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC028 - Verify user can write value more than max range for variable type UINT SIZE 7-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC029 - Verify user can write value less than min range for variable type UINT SIZE 7-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_7_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC030 - Verify user can write value for variable type UINT SIZE 8
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      1
    ${ResponseIndex}           convert to integer      1
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_8_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC031 - Verify user can get value and attributes for variable type UINT SIZE 8
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      0
    ${ResponseIndex}           convert to integer      0
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_8_Rsc.json
    ${ActualStatus}            GetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC032 - Verify user can write value more than max range for variable type UINT SIZE 8-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      2
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_8_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

ATC033 - Verify user can write value less than min range for variable type UINT SIZE 8-ERROR
    [Tags]  Critical    Variable    UINT_Size
    ${RequestIndex}            convert to integer      3
    ${ResponseIndex}           convert to integer      2
    ${FileName}                set variable               HART_Super_DD_Variable_UInteger_Size_8_Rsc.json
    ${ActualStatus}            SetIntergerSizeVariable    ${FileName}  ${RequestIndex}  ${ResponseIndex}
    should be true             True==${ActualStatus}

## Test Min Max for Arithmetic types
########################################################################################################################
########################################### Variable Action variable  ##################################################
########################################################################################################################
ATC001 - Verify the behavior when PRE-EDIT actions are performed for a VARIABLE which does not have PRE-EDIT Actions
    [Tags]  Non-Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]     registercallback       SetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_PRE_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    [Teardown]      unregistercallback      SetItem

ATC002 - Verify PRE-EDIT Actions are performed for a Variable
    [Tags]  Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]         registercallback       SetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_PRE_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
   :for    ${Index}    in range    0   1
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

   [Teardown]      unregistercallback      SetItem

ATC003 - Verify PRE-EDIT Actions are aborted for a Variable
    [Tags]  Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]         registercallback       SetItem
     ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_PRE_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
     should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][2]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][3]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][7]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][5]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    [Teardown]      unregistercallback      SetItem

ATC001 - Verify the behavior when POST-EDIT actions are performed for a VARIABLE which does not have POST-EDIT Actions
    [Tags]  Non-Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]     registercallback       SetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_POST_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${SetItemResponse}          set item                ${JsonObj["Request"][0]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][0]}
    [Teardown]      unregistercallback      SetItem

ATC002 - Verify POST-EDIT Actions are performed for a Variable
    [Tags]  Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]         registercallback       SetItem
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_POST_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
    should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}        retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
    ${NotifyIndex}              evaluate                ${NotifyIndex}+1
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
   :for    ${Index}    in range    0   1
        \   ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}

   [Teardown]      unregistercallback      SetItem

ATC003 - Verify POST-EDIT Actions are aborted for a Variable
    [Tags]  Critical    Variable    EDIT_ACTIONS    PRE_EDIT_ACTIONS
    [Setup]         registercallback       SetItem
     ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_POST_EDIT_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${SetItemResponse}          set item                ${JsonObj["Request"][1]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][1]}
     should be true              True==${SetItemStatus}
    ${NotifyTimeout}            convert to integer      10
    ${NotifyIndex}              convert to integer      0
    :for    ${Index}    in range    0   2
        \   ${NotificationData}         retrieve item data      SetItem     ${NotifyTimeout}
        \   ${ExpectedNotification}     set variable            ${JsonObj["Notification"][${NotifyIndex}]}
        \   ${NotifyIndex}              evaluate                ${NotifyIndex}+1
        \   ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
        \   should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][3]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data          SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][2]}
    set to dictionary           ${ExpectedNotification["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]}      VarValue=${NotificationData["ItemInfo"]["MethodVarInfo"]["ItemInfo"]["VarInfo"]["VarValue"]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][3]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][4]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][7]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    ${SetItemResponse}          set item                ${JsonObj["Request"][2]}
    ${SetItemStatus}            compare json object     ${SetItemResponse}    ${JsonObj["Response"][2]}
    should be true              True==${SetItemStatus}
    ${NotificationData}         retrieve item data      SetItem         ${NotifyTimeout}
    ${ExpectedNotification}     set variable            ${JsonObj["Notification"][5]}
    ${NotifyStatus}             compare json object     ${NotificationData}     ${ExpectedNotification}
    should be true              True==${NotifyStatus}
    [Teardown]      unregistercallback      SetItem

ATC005 - Verify POST-READ Actions are performed for a Variable
    [Tags]  Critical    Variable    READ_ACTIONS    POST_READ_ACTIONS
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_POST_READ_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
     ${Status}                  register callback       SetItem
    ${ActualStatus}             set item                ${JsonObj["SetItem"]["Request"]}
     ${Status}                  unregister callback     SetItem
    ${ExpectedStatus}           set variable            ${JsonObj["SetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${JsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${JsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${JsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${JsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${JsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${JsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${JsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${JsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${JsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${JsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data
    ${NotifyStatus}             compare json object     ${NotifyData}       ${JsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${JsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${JsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${JsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${JsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback    ProcessSubscription
    should be true              True==${NotifyStatus}
    ${Status}                   register callback     GetItem
    ${ActualStatus}             get item                ${JsonObj["GetItem"]["Request"]}
    ${Status}                   unregister callback    GetItem
    ${ExpectedStatus}           set variable            ${JsonObj["GetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC005 - Verify POST-WRITE Actions are performed for a Variable
    [Tags]  Critical    Variable    WRITE_ACTIONS    POST_WRITE_ACTIONS
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_POST_READ_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${ActualStatus}             set item                ${JsonObj["SetItem"]["Request"]}
    ${Status}                   unregister callback      SetItem
    ${ExpectedStatus}           set variable            ${JsonObj["SetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
    ${Status}                   register callback       GetItem
    ${ActualStatus}             get item                ${JsonObj["GetItem"]["Request"]}
    ${Status}                   unregister callback     GetItem
    ${ExpectedStatus}           set variable            ${JsonObj["GetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

ATC006 - Verify PRE-WRITE Actions are performed for a Variable
    [Tags]  Critical    Variable    WRITE_ACTIONS    PRE_WRITE_ACTIONS
    ${JsonFileContent}          get file                ${SuperDDVariableResources}/HART_SuperDD_Variable_PRE_WRITE_ACTION_Rsc.json
    ${JsonObj}                  evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       SetItem
    ${ActualStatus}             set item                ${JsonObj["SetItem"]["Request"]}
    ${Status}                   unregister callback       SetItem
    ${ExpectedStatus}           set variable            ${JsonObj["SetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
#    should be true              True==${VariableCompStatus}
    ${Status}                   register callback       GetItem
    ${ActualStatus}             get item                ${JsonObj["GetItem"]["Request"]}
    ${Status}                   unregister callback       GetItem
    ${ExpectedStatus}           set variable            ${JsonObj["GetItem"]["Response"]}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}

