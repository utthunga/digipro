*** Settings ***
Resource    hartsuite/suite_resource/common.robot

Force Tags      HART    Host    Super_DD_Device

#Suite Setup  Run Keywords       SetProtocol     AND     InitializeProtocol
#...             AND             ConnectDeviceAddress          [0]
#...
#
##Suite Teardown  Run Keywords    ChangeAddress       [4]
##...             AND             DisconnectDevice
##...             AND             ShutDownProtocol
#
#Suite Teardown  Run Keywords    DisconnectDevice
#...             AND             ShutDownProtocol
#

