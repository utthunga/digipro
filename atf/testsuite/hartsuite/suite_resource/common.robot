*** Settings ***
Documentation    Suite description

Library     OperatingSystem
Library     BuiltIn
Library     ATFTestLib.py
Library     Collections
Library     robot.libraries.String

*** Variables ***

${HostResources}                hartsuite/host/resource
${644_H5_Resource}              hartsuite/644/h5/resource
${644_H7_Resource}              hartsuite/644/h7/resource
${3051_H5_Resource}             hartsuite/3051/h5/resource
${SuperDDResource}              hartsuite/super_dd/resource
${TestCount}                       5

${IMAGE_COMPARATOR_COMMAND}     /usr/bin/convert-im6 __REFERENCE__ __TEST__ -metric RMSE -compare -format  "%[distortion]" info:

*** Keywords ***
SetLangCodeEnKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetLanguage.json
    ${SetLangJsonObj}           evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set lang code           ${SetLangJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetLangJsonObj["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

SetProtocolTypeKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_SetProtocol_Rsc.json
    ${SetProtocolJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             set protocol type       ${SetProtocolJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetProtocolJsonObj['Response']}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

InitializeStackKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Initialize_Rsc.json
    ${InitializeJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             initialize              ${InitializeJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${InitializeJsonObj["Response"]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

ShutdownStackKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_ShutDown_stack_Rsc.json
    ${ShutdownJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             shutdown                ${ShutdownJsonObj["Request"]}
    sleep                       5s
    ${ExpectedStatus}           set variable            ${ShutdownJsonObj["Response"][0]}
    ${Result}                   compare json object     ${ActualStatus}     ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${Result}

ConnectDeviceKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    sleep       10s
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          False==${CompStatus}

DisconnectDeviceKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Disconnect_device_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             disconnect              ${SetConnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    sleep       10s
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${CompStatus}

StartScanKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${SetStartScanJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             start scan              ${SetStartScanJsonObj["StartScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetStartScanJsonObj["StartScan"]['Response']}
    sleep       10s
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${CompStatus}

StopScanKW
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Scan_Rsc.json
    ${SetStopScanJsonObj}       evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             stop scan               ${SetStopScanJsonObj["StopScan"]["Request"]}
    ${ExpectedStatus}           set variable            ${SetStopScanJsonObj["StopScan"]['Response']}
    sleep       10s
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          True==${CompStatus}

ConnectDevice3051
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_3051_Connect_Device_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
     sleep       20s
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          False==${CompStatus}

ConnectDevice644H5
    [Arguments]                 ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_644_h5_Connect_Device_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    sleep       10s
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          False==${CompStatus}

ConnectDeviceAddressKW
    [Arguments]                 ${ARGUMENT}             ${ASSERT}="FALSE"
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_Address_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]${ARGUMENT}}
    sleep       10s
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    run keyword if              ${ASSERT}=="TRUE"
    ...                         should be true          False==${CompStatus}

ConnectDeviceAddress2
    ${JsonFileContent}          get file                ${HostResources}/HART_Host_Connect_device_Address_2_Rsc.json
    ${SetConnectJsonObj}        evaluate                json.loads('''${JsonFileContent}''')   json
    ${ActualStatus}             connect                 ${SetConnectJsonObj["Request"]}
    sleep       10s
    ${ExpectedStatus}           set variable            ${SetConnectJsonObj['Response']}
    sleep       10s
    ${CompStatus}               compare json object     ${ActualStatus}    ${ExpectedStatus}
    should be true              True==${CompStatus}

ChangeAddressKW
    [Arguments]                 ${RESOURCE_FILE}        ${ARGUMENT}
    ${JsonFileContent}          get file                ${RESOURCE_FILE}
    ${VariableJsonObj}          evaluate                json.loads('''${JsonFileContent}''')   json
                                register callback       SetItem
    ${ActualStatus}             set item                ${VariableJsonObj["Request"]${ARGUMENT}}
    ${ExpectedStatus}           set variable            ${VariableJsonObj['Response']}
    ${VariableCompStatus}       compare json object     ${ActualStatus}     ${ExpectedStatus}
    should be true              True==${VariableCompStatus}
                                unregister callback     SetItem

Subcription
    [Arguments]                     ${arg}
    ${JsonFileContent}          get file                    ${arg}
    ${SubscriptionJsonObj}      evaluate                json.loads('''${JsonFileContent}''')   json
    ${Status}                   register callback       ProcessSubscription
    ${CreateSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Create"]["Request"]}
    ${KeyExists}                is json key exist      ${CreateSubResp["CommandResponse"]}    result
    should be true              True==${KeyExists}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]["CommandResponse"]["result"]}   ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}      ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}       ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}     ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    set to dictionary           ${SubscriptionJsonObj["Subscription"]["Notify"]}        ItemSubscriptionID=${CreateSubResp["CommandResponse"]["result"]["ItemSubscriptionID"]}
    log dictionary              ${SubscriptionJsonObj}
    ${CreateStatus}             compare json object     ${CreateSubResp}    ${SubscriptionJsonObj["Subscription"]["Create"]["Response"]}
    should be true              True==${CreateStatus}
    ${StartSubResp}             process subscription    ${SubscriptionJsonObj["Subscription"]["Start"]["Request"]}
    ${StartStatus}              compare json object     ${StartSubResp}     ${SubscriptionJsonObj["Subscription"]["Start"]["Response"]}
    should be true              True==${StartStatus}
    ${NotifyData}               retrieve item data      ProcessSubscription
    #${Value}                    set variable            ${NotifyData}
    #set to dictionary           ${NotifyData['ItemList'][0]['ItemInfo']['VarInfo']}        VarValue=${Value}
    ${NotifyStatus}             compare json object     ${NotifyData}       ${SubscriptionJsonObj["Subscription"]["Notify"]}
    ${StopSubResp}              process subscription    ${SubscriptionJsonObj["Subscription"]["Stop"]["Request"]}
    ${StopStatus}               compare json object     ${StopSubResp}      ${SubscriptionJsonObj["Subscription"]["Stop"]["Response"]}
    should be true              True==${StopStatus}
    ${DeleteSubResp}            process subscription    ${SubscriptionJsonObj["Subscription"]["Delete"]["Request"]}
    ${DeleteStatus}             compare json object     ${DeleteSubResp}    ${SubscriptionJsonObj["Subscription"]["Delete"]["Response"]}
    should be true              True==${DeleteStatus}
    ${Status}                   unregister callback     ProcessSubscription
    should be true              True==${NotifyStatus}

Compare Images
   [Arguments]      ${Reference_Image_Path}    ${Test_Image_Path}    ${Allowed_Threshold}
   ${TEMP}          Replace String     ${IMAGE_COMPARATOR_COMMAND}    __REFERENCE__     ${Reference_Image_Path}
   ${COMMAND}       Replace String     ${TEMP}    __TEST__     ${Test_Image_Path}
   Log              Executing: ${COMMAND}
   ${RC}            ${OUTPUT}=     Run And Return Rc And Output     ${COMMAND}
   Log              Return Code: ${RC}
   Log              Return Output: ${OUTPUT}
   ${RESULT}        Evaluate    ${OUTPUT} < ${Allowed_Threshold}
   Should be True   ${RESULT}
