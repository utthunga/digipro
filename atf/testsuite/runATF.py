#!/usr/bin/env python2.7

import os
import json
import sys
import robot

LIBRARY_PATH = ""
OUTPUT_DIR = ""
TEST_SUITE_PATH = ""

ROBOT_COLUMN_OUTPUT_WIDTH = 128
g_commandList = []

# Load ATF configuration file for running test suite
def loadAndParseATFConfig(atfConfig):
    global LIBRARY_PATH
    global OUTPUT_DIR
    global TEST_SUITE_PATH
    atfConfObj = json.load(atfConfig)
    if ('LibraryPath' not in atfConfObj or 'TestOutputDir' not in atfConfObj or 'TestSuitePath' not in atfConfObj):
        raise ValueError("runATF: Missing attribute in ATF configuration file")
    LIBRARY_PATH = atfConfObj['LibraryPath']
    OUTPUT_DIR = atfConfObj['TestOutputDir']
    TEST_SUITE_PATH = atfConfObj['TestSuitePath']
    # Parse the content of ATF configuration file and push all the commands in the list
    if ('HostAppSetup' in atfConfObj and atfConfObj['HostAppSetup'] == 'REMOTE'):
        if ('HostConfig' in atfConfObj and 'HostIpAddress' in atfConfObj['HostConfig'] and 'HostPort' in atfConfObj['HostConfig']):
            remoteAddress = atfConfObj['HostConfig']['HostIpAddress']
            remotePort = atfConfObj['HostConfig']['HostPort']
            os.environ["DBUS_SESSION_BUS_ADDRESS"] = "tcp:host={0},port={1},family=ipv4".format(remoteAddress, remotePort)
        else:
            raise ValueError("runATF: Remote configuration does not exists in the ATF config file")
    print("runATF: DBus Session Address: ", os.environ["DBUS_SESSION_BUS_ADDRESS"])
    if ('TestSetUp' in atfConfObj and len(atfConfObj['TestSetUp']) > 0):
        buildCommandTable(atfConfObj)
    else:
        raise ValueError("runATF: Test set up does not exists or no elements in the configuration file")
    if len(g_commandList) > 0:
        executeATFCommand()

# Execute each individual commands in the list
def buildCommandTable(atfTestSetup):
    for testSetup in atfTestSetup['TestSetUp']:
        if 'TestSuite' in testSetup and 'TestDevice' in testSetup :
            testSuite = testSetup['TestSuite'] + "." + testSetup['TestDevice']
            testDevice = testSetup['TestDevice']
            for testFeature in testSetup['TestFeatureList']:
                testFeatureName = testFeature['TestFeature']
                testTag = ''
                if 'TestFeatureTag' in testFeature:
                    testFeatureTag = testFeature['TestFeatureTag']
                    if 'IncludeTag' in testFeatureTag:
                        for includeTag in testFeatureTag['IncludeTag']:
                            testTag = " -i " + includeTag
                    if 'ExcludeTag' in testFeatureTag:
                        for excludeTag in testFeatureTag['ExcludeTag']:
                            testTag = " -e " + excludeTag
                command = ' -P ' + LIBRARY_PATH
                command = command + " -s " + testSuite + "." + testFeatureName
                command = command + testTag
                command = command + " -d " + OUTPUT_DIR + '//' + testDevice + '//'+ testFeatureName
                command = command + " -W " + str(ROBOT_COLUMN_OUTPUT_WIDTH)
                command = command + " " + TEST_SUITE_PATH
                g_commandList.append(command)
        else:
            raise ValueError("runATF: No test suite and device configured")

def executeATFCommand():
    for commandArgs in g_commandList:
        robot.run_cli(commandArgs.split(), exit=False)

if __name__ == "__main__":
    # Support python version 2.7
    assert sys.version_info != (2, 7)
    if len(sys.argv) < 2:
        print("runATF: Pass ATF configuration file to start executing ATF")
        quit(1)
    # Load ATF configuration file
    print("runATF: Load configuration file: ", sys.argv[1])
    with open(sys.argv[1]) as atfConfigContent:
        loadAndParseATFConfig(atfConfigContent)
