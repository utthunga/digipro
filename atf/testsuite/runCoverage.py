#!/usr/bin/env python2.7

import json
import os

# Load coverage configuration file for running test suite
with open(os.curdir+"/"+'coverageConfig.json') as covJsonFile:
    covConfObj = json.load(covJsonFile)

if ('SrcDirPath' not in covConfObj or 'BuildDirName' not in covConfObj or 'BuildTool' not in covConfObj or 'Arch' not in covConfObj or 'BuildType' not in covConfObj or 'ScriptPath' not in covConfObj or 'ScriptFile' not in covConfObj):
    raise ValueError("runCoverage: Missing attribute in code coverage configuration file")

SRC_DIR_PATH = covConfObj['SrcDirPath']
BUILD_DIR_NAME = covConfObj['BuildDirName']
BUILD_DIR_PATH = SRC_DIR_PATH+"/"+BUILD_DIR_NAME
BUILD_TOOL = covConfObj['BuildTool']
BUILD_ARCH = covConfObj['Arch']
BUILD_TYPE = covConfObj['BuildType']
SCRIPT_PATH = covConfObj['ScriptPath']
SCRIPT_FILE_NAME = covConfObj['ScriptFile']

# Append the cmake path name
CMAKE_DIR_PATH = BUILD_DIR_PATH+"/"+BUILD_TOOL+"_"+BUILD_ARCH+"_"+BUILD_TYPE
print "complete path is : ", CMAKE_DIR_PATH

# Append the script path with script file name
SCRIPT_FILE = SRC_DIR_PATH+"/"+SCRIPT_PATH+"/"+SCRIPT_FILE_NAME
print "script file is : ", SCRIPT_FILE

# Execute the code coverage report generation script
bashCommand = SCRIPT_FILE + " " + CMAKE_DIR_PATH
os.system(bashCommand)



