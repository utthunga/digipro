/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand S Ugare
    Origin:            ProStar
*/

/** @file
    HART Adaptor class declaration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _HART_ADAPTOR_H
#define _HART_ADAPTOR_H

/* INCLUDE FILES **************************************************************/

#include "BusinessLogic.h"
#include "HARTConnectionManager.h"
#include "HARTParameterCache.h"
#include "IProtocolAdaptor.h"
#include "SDC625Controller.h"

#include "AdaptorDef.h"
#include <climits> // for datatype ranges


/* MACRO DEFINITIONS **********************************************************/



#define DEFAULT_POLL_ADDR 0
#define DEFAULT_VAL 1
#define DFLT_PREAMBLE 5
#define MAX_PREAMBLE 20

#define DEVICE_SCAN_NOTIFY_INERVAL 500

#define PREAMBLE "Preamble"
#define SET_DEFLT "set_default"
#define HART_BUS_PARAMS_KEY "HART_BUS_PARAMS"
#define HART_APP_SET_KEY "HART_APP_SETTINGS"
#define POLL_BY_ADDR_OPTION "PollByAddressOption"
#define HART_MASTER_TYPE "masterType"

/// SYM IDs
#define CONFIG_CHANGE_COUNTER_SYM_ID   207

#define HART_REV_6                     6

#define BIT_ON          1
#define BIT_OFF         0
/* ENUMERATIONS AND STRUCTURES ************************************************/



/// Defines the bus parameters for HART protocol
typedef struct HART_BusParameters
{
    BYTE preamble;      /// Preambles value
    HartMasterType masterType;  /// HART master type

} HART_BUS_PARAMS;

/// Other HART APPLICATION SETTINGS
typedef struct HartAppSettings
{
    PollByAddress pollAddrOption; /// polling option
}HART_APP_SETTINGS;

/// structure for holding HART scan results
typedef struct HartScanTable
{
    Indentity_t  identity;
    struct HartScanTable  *next;

}HART_SCAN_TBL;

typedef struct HartScanDevTbl
{
    int             count;
    HART_SCAN_TBL    *list;

}HART_SCAN_DEV_TBL;

/* CLASS DECLARATIONS *********************************************************/
/*
    Forward declartion of HARTConnectionManager
*/
class HARTConnectionManager;

/*============================================================================*/
/** HARTAdaptor Class Definition.

    HARTAdaptor class provide adaptor functionalities of
    HART protocol type.
*/

class HARTAdaptor : public IProtocolAdaptor
{
public:
    /* Construction and Destruction ==========================================*/
    /*------------------------------------------------------------------------*/
    /// Constructor of the HART adaptor
    HARTAdaptor();

    /*------------------------------------------------------------------------*/
    /// Destructor of the HART adaptor
    virtual ~HARTAdaptor();

    /* Public Member Functions ===============================================*/
    /*------------------------------------------------------------------------*/
    /** Assigns the instance to memeber variables

        @return JSon result
    */
    Json::Value initialise();

    /*------------------------------------------------------------------------*/
    /** Retrieves the current active protocol

        @return active protocol
    */
    CPMPROTOCOL getActiveProtocol();

    /*------------------------------------------------------------------------*/
    /// Deallocates and shuts down the HART adaptor and its properties
    Json::Value shutDown();

    /*------------------------------------------------------------------------*/
    /** Gets device description header information

        @return JSON result
    */
    Json::Value getDDHeaderInfo();

    /*------------------------------------------------------------------------*/
    /** Updates device description data

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value updateDD(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Reads HART basic device data

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value getItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Writes data to HART basic device

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value setItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Process requested subscription action

     	@param subscriptionReq subscription action request in Json::Value

     	@return Process subscription response
    */
    Json::Value processSubscription(const Json::Value& subReq);

    /*------------------------------------------------------------------------*/
    /** Start device scanning operation on H1 network

        @return JSON result
    */
    Json::Value startScan(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stops device scanning operation on H1 network

        @return JSON result
    */
    Json::Value stopScan();

    /*------------------------------------------------------------------------*/
    /** Scans H1 network for basic devices

        @return JSON result
     */
    PMStatus scanNetwork();

    /*------------------------------------------------------------------------*/
    /** Connects to HART basic device

        @return JSON result
    */
    Json::Value connect(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Disconnects from HART basic device

        @return JSON result
    */
    Json::Value disconnect();

    /*------------------------------------------------------------------------*/
    /** Sets H1 network bus parameters

        @return JSON result
    */
    Json::Value setBusParameters(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves the H1 bus parameters

        @return JSON result
    */
    Json::Value getBusParameters();

    /*------------------------------------------------------------------------*/
    /** Get Default Bus parameters from PS

         @param  default Bus Parameters in string

    */
    void getDefaultBusParameters(Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Get Default Bus parameters from PS

         @param  default Bus Parameters in string

    */
    void getDefaultAppSettings(Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Populates scanned device information

        @return JSON result
    */
    PMStatus populateDeviceInfo();

    /*------------------------------------------------------------------------*/
    /// Sets HART communication failure
    void setCommunicationFailure();

    /*------------------------------------------------------------------------*/
    /** Processes the device and communication related alerts

        @param alerts - contains alerts
    */
    void processAlerts(void* alerts);

    /*------------------------------------------------------------------------*/
    /** Retrieves bus parameters and application settings from persisitent storage

        @param params - contains bus parameters

        @return status
    */
    PMStatus getStackConfiguration();

    /*------------------------------------------------------------------------*/
    /** Retrieves the basic field device information such as Device Address,
        Device Tag and Device Class info in JSON format.

        @param paramInfo - contains field device info such as id

        @return json value
    */
    Json::Value getFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Assigns the user configuration such as Node Address,
        Physical Device Tag, and Device Class info to physical device and
        returns the status in JSON format

        @param paramInfo - contains field device info such as id

        @return json value
    */
    Json::Value setFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stores the other configuration settings in the persistent storage.

        @param jsonData - application setting in JSON structure

        @return return  - error/success JSON data
    */
    Json::Value setAppSettings(const Json::Value &jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves application configuration settings from the persistent storage.

        @return - error/success JSON data
    */
    Json::Value getAppSettings();

    /*------------------------------------------------------------------------*/
    /** check if Bus parameters are available.

        @param bus param json data

        @return true or false
     */
     bool isBusParamAvailable(const Json::Value& jsonData);

     /*------------------------------------------------------------------------*/
     /** check if Application Settings are available

         @param jsonData - conatins bus param json

         @return true or false
     */
     bool isAppSettingsAvailable(const Json::Value& jsonData);

     /*------------------------------------------------------------------------*/
     /** setData function is responsible to set data which is come from UI as
         user input

         @param itemInfo - contains user input value in json data

         @return status - Holds the status of set method data
     */
     PMStatus setMethodData(Json::Value itemInfo);

     /*------------------------------------------------------------------------*/
     /** Creates Method Execution timer thread
     */
     void createMethodExecutionThread();

     /*------------------------------------------------------------------------*/
     /** Destroyes Method Execution thread
     */
     void destroyMethodExecutionThread();

     /*------------------------------------------------------------------------*/
     /** Executes the method
     */
     void executeMethod();

     /*------------------------------------------------------------------------*/
     /** Sets the Method Execution thread state

         @param eThreadState - contains the thread state
     */
     void setMethodExecutionThreadState(METhreadStateT eThreadState);

     /*------------------------------------------------------------------------*/
     /** Retrieves the current state of Method Execution thread

         @return thread state
      */
     METhreadStateT getMethodExecutionThreadState();

     /*------------------------------------------------------------------------*/
     /** Sends method information to UI layer using ITEM_PUBLISHER

         @param response - cotains method execution response to construct the Json
         data and send to UI layer

         @param notificationType - contains method notification type
     */
     void sendMethodUIMessage(std::string response, string notificationType);

     /*------------------------------------------------------------------------*/
     /** Sends method error message to UI layer occurred during method execution
         using ITEM_PUBLISHER publisher
     */
     void sendMethodErrorMessage();

     /*------------------------------------------------------------------------*/
     /** getMethodData function is responsible to get data from to UI layer
         or User

         @param message - cotains method message to UI layer
         @param structMethodsUIData - contains method information data to UI
                layer

         @return pmstatus (success or failure)
     */
     PMStatus getMethodData(std::string message, ACTION_UI_DATA structMethodsUIData);

     /*------------------------------------------------------------------------*/
     /** Retrieves the method local variable information

         @param localVar - contains method local variable information
         @param varType - output paramter to hold the local variable information

         @return pmstatus (success or failure)
     */
     PMStatus getMethodLocalVar(UI_DATA_EDIT_BOX localVar,
                                             VariableType& varType);

     /*------------------------------------------------------------------------*/
     /** Fills the enumeration data and send to IC

         @param enumMap - contains enumeration data
     */
     void fillMethodEnumValAsJson(std::string message,
                            ACTION_UI_DATA structMethodsUIData);

     /*------------------------------------------------------------------------*/
     /**  This function returns parameter cache instance

         @param return - returns ParameterCache instance
     */
     ParameterCache* getParameterCacheInstance();

     /* Static callback Functions =============================================*/
     /*------------------------------------------------------------------------*/
     /** Implements Method Execution thread
         @param adaptor - contains current protocol adaptor instance
     */
     static void methodExecutionThread(HARTAdaptor* adaptor);

     /*------------------------------------------------------------------------*/
     /** Implements variable actions execution thread
         @param adaptor - contains current protocol adaptor instance
     */
     static void methodActionExecutionThread(HARTAdaptor* adaptor);

     /*------------------------------------------------------------------------*/
     /** Executes the method based on the edit actions

         @param itemID - Holds the method ID

         @return status info
     */
     PMStatus processExecuteEditMethod(itemID_t itemID);

     /*------------------------------------------------------------------------*/
     /** Executes pre edit action for Edit Display

         @param data - contains JSON input

         @return JSON result
     */
     Json::Value execPreEditAction(const Json::Value& data);

     /*------------------------------------------------------------------------*/
     /** Executes post edit action for Edit Display

         @param data - contains JSON input

         @return JSON result
     */
     Json::Value execPostEditAction(const Json::Value& data);

     /*------------------------------------------------------------------------*/
     /** Validates Pre Post Edit Action input JSON

         @param data - contains JSON input
         @param paramInfo - parameter info

         @return status
     */
     PMStatus validateEditAction(const Json::Value& data,
                                   paramItems& paramInfo);

protected:
    /* Protected Data Members ================================================*/
    /// HARTConnectionManager instance
    HARTConnectionManager* m_pConnectionManager;

    /// HARTParameterCache instance
    HARTParameterCache* m_pParameterCache;

    /// BusinessLogic instance
    BusinessLogic* m_pBusinessLogic;

    /// SDC625Controller instance
    SDC625Controller* m_pSDC625Controller;

    /// Subscription Controller instance
    SubscriptionController* m_pSubscriptionController;

private:
    /* Private Member functions ==============================================*/

    /*------------------------------------------------------------------------*/
    /** Fetches default bus parameters to assign default values

     */
    void setDefaultBusParameters();

    /*------------------------------------------------------------------------*/
    /** Validates the configuration against the user modified value.
        If invalid configuration is found, then the error status will be sent.

         @return status
    */
    PMStatus validateBusParameters();

    /*------------------------------------------------------------------------*/
    /** Retrieves the default values for the application settings.

        @param data - json data in string format
    */
    void setDefaultAppSettings();

    /*------------------------------------------------------------------------*/
    /** Validates the configuration against the user modified value.
        If invalid configuration is found, then the error status will be sent.

        @return status
    */
    PMStatus validateAppSettings();

    /*------------------------------------------------------------------------*/
    /**  Initialize the publisher table
    */
    void initializePublisherList();

    /*-----------------------------------------------------------------------*/
    /** Retrieves the root menu information

       @param itemID List contains the requested item ID information

       @param Json::Value - Fills the menu information into Json Object
    */
    PMStatus getDDMenu(itemID_t itemID, Json::Value &output);

    /*-----------------------------------------------------------------------*/
    /** Retrieves the edit display information

       @param itemID contains the requested item ID information

       @param Json::Value - Fills the edit display information into Json Object
    */
    PMStatus getEditDisplayItems(itemID_t itemID, Json::Value &output);

    /*-----------------------------------------------------------------------*/
    /** Retrieves the WAO relation items for the requested variable item

       @param itemID contains the variable item ID

       @param Json::Value - Fills the wao list items into Json Object
    */
    PMStatus getVarWaoListItems(itemID_t itemID, Json::Value &output);

    /*-----------------------------------------------------------------------*/
    /** Builds the menu information as a JSON string

        @param ItemBasicInfo List containing the each item information

        @param Json::Value - Fills the menu information into Json Object
    */
    void getMenuAsJson(VarBasicInfo& menuItem,
                       Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Fills Item Basic information into Json response

        @param itemBasicInfo Structure containing item basic information

        @param jsonResp Json response to be filled

    */
    void fillItemBasicJsonInfo(	ItemBasicInfo& itemBasicInfo,
                                Json::Value& jsonResp);

    /*------------------------------------------------------------------------*/
    /** Fills menu items Basic information into Json response

        @param locMenuList contains the list of menu items information

        @param jsonResp Json response to be filled

    */
    PMStatus fillMenuItemsJson(	menuItemList_t& locMenuList,
    						Json::Value &output);

    /*------------------------------------------------------------------------*/
    /** Fills variable Basic information into Json response

        @param varBasicInfo Structure containing variable basic information

        @param jsonResp Json response to be filled

    */
    void fillVarBasicJsonInfo(	VarBasicInfo& varBasicInfo,
                                Json::Value& jsonResp);

    /*------------------------------------------------------------------------*/
    /** Processes Variable information

        @param varRequest Variable item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return json info
    */
    Json::Value processVariable(const Json::Value& varRequest,
                                const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Image information

        @param imageRequest Image item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return json info
    */
    Json::Value processImage(const Json::Value& imageRequest,
                             const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Method information

        @param method item information in Json

        @param ITEM_QUERY to set or get an item

        @return Error status
    */
    Json::Value processMethod(const Json::Value& methodRequest,
                              const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Retrieves Variable value with attributes

        @param ITEMID Contains information of Item ID

        @param maskType Attribute mask differentiating between minimum attribute
               information and complete attribute information

        @return Variable information with attributes as Json::Value
    */
    Json::Value getVariable(paramItems& paramInfo,
                            MaskType maskType = MaskType::ITEM_ATTR_ALL_MASK);

    /*------------------------------------------------------------------------*/
    /** Retrieves Image value with attributes

        @param ITEMID Contains information of Item ID

        @param maskType Attribute mask differentiating between minimum attribute
               information and complete attribute information

        @return Image information with attributes as Json::Value
    */
    Json::Value getImage(ITEM_ID& itemID,
                         MaskType maskType = MaskType::ITEM_ATTR_ALL_MASK);

    /*------------------------------------------------------------------------*/
    /** Sets Variable value into the device

        @param ITEMID Contains information of Item ID

        @param maskType Attribute mask differentiating between minimum attribute
            information and complete attribute information

        @return status of set variable as Json::Value
    */
    Json::Value setVariable(paramItems& paramItems,
                            Json::Value& itemInfo,
                            MaskType maskType = MaskType::ITEM_ATTR_ALL_MASK);

    /*------------------------------------------------------------------------*/
    /** Sets Variable value into the parameter info structure

        @param itemInfo holds the variable itemInfo request in Json::Value

        @param varParam structure holds variable parameter information

        @return status of setVariableValue method
    */
    PMStatus setVariableValue(const Json::Value& itemInfo,VariableType* varParam);

    /*------------------------------------------------------------------------*/
    /** Processes Menu information

        @param menuRequest Menu item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processMenu(const Json::Value& menuRequest,
                            const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Applies all the qualifiers for a Variable menu item as per the EDDL Spec

       @param qualifier Qualifiers received for the menu item

       @param itemInfoJson Updated Json structure after applying qualifiers
    */
    void applyQualifiersForVariables(unsigned short qualifier,
                                      Json::Value & itemJsonInfo);

    /*------------------------------------------------------------------------*/
    /**  Executes respective mapped method through function pointer based
         on item type

        @param itemRequest Information containing item type in Json

        @param ITEMQUERY  enumeration differenciates set or get an item

        @return complete item information queried with status
    */
    Json::Value executeMappedMethod(const Json::Value& itemRequest,
                                    const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /**  Maps the processItem method function pointers to corresponding
         method key
    */
    void mapItemMethods();

    /*------------------------------------------------------------------------*/
    /** Fills Variable information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable information

        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVariableJsonInfo( VariableType* varParam,
                               Json::Value& varJsonResp,
                               VAR_JSON_ATTR_MASK varMaskType = 
                               VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*------------------------------------------------------------------------*/
    /** Fills Variable information into Json response

        @param imageParam Structure consisting of image type information

        @param imageParamJson Json::Value to contain variable information
    */
    void fillImageJsonInfo(ImageType* imageParam,
                           Json::Value& imageParamJson);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable Arithmetic information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable arithmetic info

        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarArithmeticJson(	VariableType* varParam,
                                Json::Value& varJsonResp,
                                VAR_JSON_ATTR_MASK varMaskType = 
                                VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable Enumeration information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable enumeration info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarEnumerationJson( VariableType* varParam,
                                 Json::Value& varJsonResp,
                                 VAR_JSON_ATTR_MASK varMaskType = 
                                 VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable String information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable string info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillStringJsonValue( VariableType* varParam,
                              Json::Value& varJsonResp,
                              VAR_JSON_ATTR_MASK varMaskType = 
                              VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /** Fills Date-Time Information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable date/time info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarDateTimeJson( VariableType* varParam,
                              Json::Value& varJsonResp,
                              VAR_JSON_ATTR_MASK varMaskType = 
                              VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /** Fills Json value key with variable value

        @param varParam VariableType structure information

        @param varJsonResp Json::Value to to fill variable value
    */
    void fillVarValue(VariableType* varParam, Json::Value& varValueJson);

    /*-----------------------------------------------------------------------*/
    /** Assigns min and max range values

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable date/time info
    */
    void assignMinMaxValues( VariableType* varParam,
                             Json::Value& varJsonResp);

    /*-----------------------------------------------------------------------*/
    /** Retrieves Variable valye type from the map

        @param varType holds the variable value type

        @returns Variable valye type from the map
    */
    std::string getVarValType(unsigned int varType);

    /*-----------------------------------------------------------------------*/
    /** Retrieves character set type from the map

        @param varType holds the char set value 

        @returns character set type from the map
    */
    std::string getCharSetType(unsigned int charSetVal);

    /*-----------------------------------------------------------------------*/
    /** Returns string from StringType

        @param strType StringType structure information

        @return string data extracted from strType
    */
    std::string validateStringValue(StringType& strType);

    /*------------------------------------------------------------------------*/
    /** Creates subscriptions

        @param createSubReq subscription request in Json::Value

        @return status of create subscription
    */
    Json::Value createSubscription(const Json::Value& createSubReq);

    /*------------------------------------------------------------------------*/
    /** Starts subscriptions

        @param startSubReq subscription request in Json::Value

        @return status of start subscription
    */
    Json::Value startSubscription(const Json::Value& startSubReq);

    /*------------------------------------------------------------------------*/
    /** Stops subscriptions

        @param stopSubReq subscription request in Json::Value

        @return status of stop subscription
    */
    Json::Value stopSubscription(const Json::Value& stopSubReq);

    /*------------------------------------------------------------------------*/
    /** Deletes subscriptions

        @param delSubReq subscription request in Json::Value

        @return status of delete subscription
    */
    Json::Value deleteSubscription(const Json::Value& delSubReq);

    /*------------------------------------------------------------------------*/
    /** Processes Get Item for method execution

        @data method item information in Json

        @return status info
    */
    Json::Value processGetMethodExecution(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Processes Set Item for method execution

        @data method item information in Json

        @return status info
    */
    Json::Value processSetMethodExecution(const Json::Value& data);

    /*-----------------------------------------------------------------------*/
    /** Adds the static parameters to the internal subscription table and
        skips the item if it is already added

        @param nSubscriptionID contains the subscription ID

        @returns status info
    */
    PMStatus addSubscriptionCacheItems(unsigned int subscriptionID);

    /*-----------------------------------------------------------------------*/
    /** Removes the static parameters from the internal subscription table.

        @param nSubscriptionID contains the subscription ID

        @returns status info
    */
    PMStatus removeSubscriptionCacheItems(unsigned int subscriptionID = 0);

    /*------------------------------------------------------------------------*/
    /** Creates internal subscription to update the cache table when the
        configuration counter increases.

        @return status of create subscription
    */
    PMStatus createsInternalSubscription();

    /*------------------------------------------------------------------------*/
    /** Creates image file by using image raw data.

        @return image file path
    */
    const char* storeImage(ImageType* imageParam);
	
    /*------------------------------------------------------------------------*/
    /** Utility function for input validation for the variables

        @return FALSE if input fails with in range
	*/
	template<typename RangeType, typename ValueType>
	bool validateIntegerRange(ValueType valuetype);

	/*------------------------------------------------------------------------*/
    /** Utility function for input validation with variable size

        @return SUCCESS if input value within range
	*/
    int validateVariableLimit(const Json::Value& itemInfo, VariableType* varParam);

    /*------------------------------------------------------------------------*/
    /** Validates Item has relation

        @param jsonRequest holds the JSON input request for an item

        @param itemHasRelation holds the value of ItemHasRelation of an Item

        @return SUCCESS if the ItemHasRelation is as required in JSON is valid else FAILURE
    */
    int validateItemHasRelation(const Json::Value& jsonRequest, bool& itemHasRelation);

    /*------------------------------------------------------------------------*/
    /** Utility function to validate non standard Signed data values

        @param size size of the input data in bytes

        @param value input value to be validted (against the given size)

        @return TRUE if input value is within range of the given size
    */
    bool validateNonStandardIntData(unsigned short size, long long value);

    /*------------------------------------------------------------------------*/
    /** Utility function to validate non standard Unsigned data values

        @param size size of the input data in bytes

        @param value input value to be validted (against the given size)

        @return TRUE if input value is within range of the given size
    */
    bool validateNonStandardUIntData(unsigned short size, unsigned long long value);

    /*------------------------------------------------------------------------*/
    /** Checks whether float value is not a number

        @param floatVal holds the float value input 

        @return true if the float value is not a number
    */
    bool isNAN(float floatVal);

    /*------------------------------------------------------------------------*/
    /** Checks whether float value is infinity

        @param floatVal holds the float value input 

        @return true if the float value is infinity
    */
    bool isINF(float floatVal);

    /*------------------------------------------------------------------------*/
    /** Checks whether double value is not a number

        @param floatVal holds the double value input 

        @return true if the double value is not a number
    */
    bool isNAN(double doubleVal);

    /*------------------------------------------------------------------------*/
    /** Checks whether double value is infinity

        @param floatVal holds the double value input 

        @return true if the double value is infinity
    */
    bool isINF(double doubleVal);

    /*-----------------------------------------------------------------------*/
    /** Stops active methid execution thread 
    */
    void stopMethodExec();

    /*-----------------------------------------------------------------------*/
    /** Clears subscription and disconnects device
        
        return device disconnection error status
    */
    PMStatus disconnectDevice();

    /*------------------------------------------------------------------------*/
    /** Validates Item in DD request input
        
        @param jsonRequest holds the JSON input request for an item

        @param requiredStatw holds the state of ItemInDD required for an Item

        @return SUCCESS if the ItemInDD is as required in JSON is valid else FAILURE
    */
    int validateItemInDD(const Json::Value& jsonRequest, bool requiredState);

    /* Private Data Members ==================================================*/
    /// Scan sequence ID
    int m_unScanSeqID;

    /// HART bus parameters
    HART_BUS_PARAMS m_busParams;

    /// HART app settings
    HART_APP_SETTINGS m_appSetting;

    /// Holds HART dd binary path
    std::string m_hartDDBinaryPath;

    /// Holds HART device file
    std::string m_hartDeviceFile;

    /// Holds HART modem type
    std::string m_hartModem;

    /// Protocol type
    CPMPROTOCOL m_Protocol;

    /// Internal Subscription Rate
    int m_internalSubscriptionRate;

    /// Subscription table
    SUBSCRIPTION_CACHE_TBL m_subscriptionCacheTable;

    /// Holds publisher list
    std::vector<string> m_publisherList;

    /// Method execution thread components
    METhreadState m_eThreadState;

    /// Function pointer for process item methods
    typedef Json::Value (HARTAdaptor::*processItemMethod)(
                                const Json::Value&,
                                const ITEM_QUERY);

    /// Map holds process item methods against corresponding method key
    std::map <std::string, processItemMethod> m_processMethodMap;

    /// Map holding DDS type with corresponding variable value type name
    std::map <int, std::string> m_sdcVarValMap =
    {
        std::make_pair(vT_Integer, JSON_VAR_VAL_TYPE_INT),
        std::make_pair(vT_Unsigned,JSON_VAR_VAL_TYPE_UINT),
        std::make_pair(vT_FloatgPt, JSON_VAR_VAL_TYPE_FLOAT),
        std::make_pair(vT_Double, JSON_VAR_VAL_TYPE_DOUBLE),
        std::make_pair(vT_Enumerated, JSON_VAR_TYPE_ENUM),
        std::make_pair(vT_BitEnumerated, JSON_VAR_VAL_TYPE_BITENUM),
        std::make_pair(vT_HartDate, JSON_VAR_VAL_TYPE_DATE),
        std::make_pair(vT_Time, JSON_VAR_VAL_TYPE_TIME),
        std::make_pair(vT_PackedAscii, JSON_VAR_VAL_TYPE_PACKED_ASCII),
        std::make_pair(vT_Password, JSON_VAR_VAL_TYPE_PASSWORD),
        std::make_pair(vT_Ascii, JSON_VAR_VAL_TYPE_ASCII),
        std::make_pair(vT_EUC, JSON_VAR_VAL_TYPE_EUC),
        std::make_pair(vT_BitString, JSON_VAR_VAL_TYPE_BITSTRING),
        std::make_pair(vT_OctetString, JSON_VAR_VAL_TYPE_OCTETSTRING),
        std::make_pair(vT_VisibleString, JSON_VAR_VAL_TYPE_VISIBLESTRING),
        std::make_pair(vT_DateAndTime, JSON_VAR_VAL_TYPE_DATE_TIME),
        std::make_pair(vT_Duration, JSON_VAR_VAL_TYPE_DURATION),
        std::make_pair(vT_Index, JSON_VAR_VAL_TYPE_INDEX)
    };

    /// Map holds the character set type with character set value
    std::map <unsigned int, std::string> charSetMap = 
    {    
        std::make_pair(0, JSON_CHAR_SET_ASCII),
        std::make_pair(1, JSON_CHAR_SET_ISO_LATIN),
        std::make_pair(2, JSON_CHAR_SET_EUC)
    };
};

#endif  // _HART_ADAPTOR_H
