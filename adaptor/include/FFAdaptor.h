/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    FF Adaptor class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "IProtocolAdaptor.h"
#include "FFConnectionManager.h"
#include "FFParameterCache.h"
#include "DDSController.h"
#include "BusinessLogic.h"
#include "ddldefs.h"
#include "ddsbltin.h"
#include "custom_menu/CustomMenuInfo.h"
#include "error/CpmErrors.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <map> // used for multimap
#include <iostream>
#include <fstream> // used for ifstream
#include <sstream> // used for stringstream
#include <array> // used for array    
#include <vector> // used for array
#include <limits> // for datatype ranges

#include "ddi_lib.h"
#include "int_diff.h"
#include "SubscriptionController.h"

/* ========================================================================== */

/// MACRO DEFINITIONS for FF Bus Parameters

#include "ff_defaults.h"

#define CHECK_NAN_VALUE 0

#define DFLT_THIS_LINK 0


#define DEFAULT_PARAMS 1
#define SLOT_TIME_MIN 8
#define SLOT_TIME_MAX 16
#define FUN_MIN 20
#define FUN_MAX 247
#define NUN_MAX 228
#define POLLING_LIMIT 248

#define SLOT_TIME                  "slot_time"
#define THIS_LINK                  "this_link"
#define PHL_OVERHEAD               "phlo"
#define MAX_RPLY_DELAY             "mrd"
#define MIN_INTER_PDU_DELAY        "mipd"
#define TIME_SYNC_CLASS            "tsc"
#define MAX_INTER_CHAN_SIG_SKEW    "phis_skew"
#define PHL_PREAMBLE_EXTN          "phpe"
#define PHL_GAP_EXTN               "phge"
#define FIRST_UNPOLLED_NODE        "fun"
#define NUM_UNPOLLED_NODE          "nun"

#define ROOT              root["FF_BUS_PARAMS"]
#define SET_DEFAULT       "set_default"  /** < set the default parameters from the UI/USER */
#define DEFAULT_INITIAL    0

/// macro to define rite bit is set in handling
#define READWRITE_HANDLING_MASK   (0x02)/** < 0010 binary*/

/// Adavanced block configuration Item ID's
#define MODE_BLOCK_ID                  0x80020126
#define CHANNEL_ID                     0x800200B4
#define XD_SCALE_ID                    0x80020192
#define OUT_SCALE_ID                   0x80020132
#define BLOCK_TAG_ID                   0x80020001
#define L_TYPE_ID                      0x8002010C

/// Block ID's
#define AI_BLOCK_ID                    0x800201D0
#define AO_BLOCK_ID                    0x800201F0
#define DO_BLOCK_ID                    0x80020230
#define DI_BLOCK_ID                    0x80020210

/// FF address range info
#define MIN_VALID_ADDRESS               16
#define MAX_VALID_ADDRESS               247
#define MAX_DEFAULT_ADDRESS             251
#define MAX_PATH_LEN                    512

/// FF Commission/decomission state
#define COMMISSIONED                    0
#define DECOMMISSIONED                  1
#define NOT_APPLICABLE                  2

#define VIEW_1 1
#define VIEW_2 2
#define VIEW_3 3
#define VIEW_4 4


#define BIT_ON	(1)
#define BIT_OFF	(0)

#define ST_REV_DD_NAME                  "__st_rev"

#define HH_MAGIC_STRING_1   ("Menu_Top_")
#define HH_MAGIC_STRING_2   ("_Menu_Top")
#define SYM_TABLE_MENU      ("menu")
#define MAX_LEN             (1024)
#define DD4_PROF_DD_EXT     ("ffo")
#define DD5_PROF_DD_EXT     ("ff5")
#define DD4_PROF_SYM_EXT    ("sym")
#define DD5_PROF_SYM_EXT    ("sy5")
#define MAX_PROF_SYM_EXT    (2)
#define NUM_OF_COLUMN       (3)

/* ========================================================================== */

/// Enumeration defines for Stack status in FF layer
typedef enum class StackStatus
{
    STACK_STATUS_SUCCESS = 1,
    STACK_STATUS_FAIL = -1
}STACK_STATUS;

/* ========================================================================== */

/// Enumeration defines BusParameters status in FF Adapter layer
typedef enum class BusParametersStatus
{
    FF_BUSPARAM_SUCCESS = 1,
    FF_BUSPARAM_FAIL = -1,
    FF_BUSPARAM_OUT_OF_RANGE_VALUE_FOR_NUN_FUN_PARAMS = -2,
    FF_BUSPARAM_GET_BUS_PARAMS_FAIL = -3,
	FF_BUSPARAM_WRONG_VALUE_FOR_SLOT_TIME = -4,
	FF_BUSPARAM_DEFUALT_PARAM_FAILED = -5,
	FF_BUSPARAM_INVALID_BUS_PARAMS = -6
}FF_BUSPARAM_STATUS;

/* ========================================================================== */

/// Enum to differenciate variable value mask
enum class VARIABLE_VAL_MASK
{
    VAR_ATTR_DD_VAL_INFO = 0, ///< Fills all variable attribute information with DD value
    VAR_ATTR_DEV_VAL_INFO     ///< Fills all variable attribute information with device value
};

/* ========================================================================== */

/// Enum to edit action type
enum class EDIT_ACTION_TYPE
{
    PRE_EDIT_ACTION = 0,   ///< Holds the pre edit action type
    POST_EDIT_ACTION       ///< Holds the post edit action type
};

/* ========================================================================== */

/// Structure  defines FF BusParameters
typedef struct FF_BusParameters
{
    unsigned short      this_link;               /** < represt the current node of the FBK */
    unsigned short      slot_time;               /** < slot_time*/
    unsigned char       phlo;                    /** <per DL PDU overload */
    unsigned char       mrd;                     /** < max response delay */
    unsigned char       mipd;                    /** < min Inter PDU delay */
    unsigned char       tsc;                     /** < time sync class */
    unsigned char       phis_skew;               /** < max inter channel signal skew */
    unsigned char       phpe;                    /** < PHL preamble extension */
    unsigned char       phge;                    /** < PHL Gap extension */
    unsigned char       fun;                     /** < first unpolled node */
    unsigned char       nun;                     /** <Number of consecutive unpolled nodes */
}FF_BUS_PARAMS;

/// Structure holds symbol table information
typedef struct symParamData
{
    string symParamName;
    string symParamType;
    ITEM_ID symParamID;
}PARAM_DATA;

// edit action param for thread structure
typedef struct editActiondata
{
    ITEM_ID itemId;
    std::string blockTag;
    EditActionTypes actionTypes;
    EditActionItemTypes actionItemTypes;
} EDIT_ACTION_DATA;

// Structure holds Bit Enumeration Item Label and Help Strings
struct BitEnumItemStr
{
    std::string helpStr;
    std::string labelStr;
    unsigned long bitIndex;
};

/* ========================================================================== */

/* CLASS DECLARATIONS *********************************************************/
/** class description of FFAdaptor class.

    FFAdaptor Class consists of methods related to FieldBus initialization, connect
    & disconnect device, get & set data DD data, Bus parameters, device scanning.

*/
class FFAdaptor : public IProtocolAdaptor
{
public:

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs FFAdaptor Class.
    */
    FFAdaptor();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes FFAdaptor class.
    */
    virtual ~FFAdaptor();

    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Initializes FF Adaptor

        @return Error string
    */
    Json::Value initialise();

    /*------------------------------------------------------------------------*/
    /** Retrieves the current active protocol

        @return Active protocol type
    */
    CPMPROTOCOL getActiveProtocol();

    /*------------------------------------------------------------------------*/
    /** Deallocates and shuts down the FF adaptor and its properties

        @return status error/success
    */
    Json::Value shutDown();

    /*------------------------------------------------------------------------*/
    /** Gets device description header information
    */
    Json::Value getDDHeaderInfo();

    /*------------------------------------------------------------------------*/
    /** Updates device description data
    */
    Json::Value updateDD(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Reads FF basic device data

        @param request to fetch item information in Json

        @return item information/error in Json
    */
    Json::Value getItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Writes data to FF basic device

        @param request to set item value in json

        @return response/error in Json
    */
    Json::Value setItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Process requested subscription action

        @param subReq subscription action request in Json::Value

        @return Process subscription response
    */
    Json::Value processSubscription(const Json::Value& subReq);

    /*------------------------------------------------------------------------*/
    /** Start device scanning operation on H1 network

        @param data scan related Json request

        @return Error Status
    */
    Json::Value startScan(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Stops device scanning operation on H1 network

        @return Error response
    */
    Json::Value stopScan();

    /*------------------------------------------------------------------------*/
    /** Scans H1 network for basic devices

        @return PMStatus status os FF scan network
    */
    PMStatus scanNetwork();

    /*------------------------------------------------------------------------*/
    /** Connets to FF basic device

        @param connection request in Json

        @return error response
    */
    Json::Value connect(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Disconnets from FF basic device

        @return error response
    */
    Json::Value disconnect();

    /*------------------------------------------------------------------------*/
    /** Sets H1 network bus parameters

        @param Bus parameters json data string

        @return SUCCESS/Error String
    */
    Json::Value setBusParameters(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves the H1 bus parameters

        @return bus parameters in Json data string
    */
    Json::Value getBusParameters();

    /*------------------------------------------------------------------------*/
    /** Populates scanned device information

        @return uint32_t : device information populate error
    */
    PMStatus populateDeviceInfo();

    /*------------------------------------------------------------------------*/
    /** Interfaces for the below layers to talk to
        Protocol Adaptor.Sets FF communication failure
    */
    void setCommunicationFailure();

    /*------------------------------------------------------------------------*/
    /**  Processes device alerts

         @param
    */
    void processAlerts(void *);

    /*------------------------------------------------------------------------*/
    /** Fetch the Bus Params from PS

        @param Bus paramters struct

        @return Error STATUS
    */
    PMStatus fetchBusParameters(void* params);

    /*------------------------------------------------------------------------*/
    /** Get Default Bus parameters from PS

        @param  default Bus Parameters in string
    */
    void getDefaultBusParameters(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /** Retrieves the basic field device information such as Device Address,
            Device Tag and Device Class info in JSON format.

        @param jsonData Contains field device info such as id

        @return json value
    */
    Json::Value getFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Assigns the user configuration such as Node Address,
        Physical Device Tag, and Device Class info to physical device and
        returns the status in JSON format

        @param jsonData Contains field device info such as id

        @return json value
    */
    Json::Value setFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stores the other configuration settings in the persistent storage.

        @param jsonData - application setting in JSON structure

        @return return  - error/success JSON data
    */
    Json::Value setAppSettings(const Json::Value &jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves application configuration settings from the persistent storage.

        @return - error/success JSON data
    */
    Json::Value getAppSettings();

    /*------------------------------------------------------------------------*/
    /** Get Default application settings from PS

         @param  default application settings in string
    */
    void getDefaultAppSettings(Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** sendMessage function is responsible for delivering message to UI layer

        @param response - contains method execution response to construct the
        Json data and send to UI layer

        @param notificationType - contains notification type indicate which
        type of notification
    */
    void sendMethodUIMessage(string response, string notificationType);

    /*------------------------------------------------------------------------*/
    /** getData function is responsible to get data from to UI layer or User

        @param msg - contains the message to display in UI as part of method
        execution

        @param item - contains the item data of the method

        @param subIndex - contains the subIndex of the variable
    */
    void getMethodData(std::string msg, DDI_GENERIC_ITEM* item,
                       unsigned short subIndex = 0);

    /*------------------------------------------------------------------------*/
    /** getMethodData function is responsible to get data from to UI layer or User
        and it will fill the default data for local reference

        @param msg - contains the message to display in UI as part of method
        execution

        @param varValue - contains value to be send and retrieve from UI
    */
    void getMethodDefaultData(std::string msg, EVAL_VAR_VALUE* varValue);

    /*------------------------------------------------------------------------*/
    /** getEnum function is responsible for sending enumeration to UI
        layer or User

        @param msg - contains message to display in UI
        @param enumMap - contains enumeration data
        @param selection - contains currently selected item
    */
    void fillMethodEnumValAsJson(string msg, std::map<int, std::string> enumMap,
                                 int selection);

    /*------------------------------------------------------------------------*/
    /** setData function is responsible to set data which is come from UI as
        user input

        @param itemInfo - contains user input value

        @param milliSec - holds millisecond value applicable for DATE_TIME types

        @param microSec - holds microsecond value applicable for TIME_VALUE_EPOCH
    */
    PMStatus setMethodData(Json::Value itemInfo, unsigned int milliSec, unsigned int microSec);

    /*------------------------------------------------------------------------*/
    /** Creates Method Execution timer thread
    */
    void createMethodExecutionThread();

    /*------------------------------------------------------------------------*/
	/** Creates edit action method thread
	*/ 
    void createEditActionThread();

    /*------------------------------------------------------------------------*/
    /** Destroyes Method Execution thread
    */
    void destroyMethodExecutionThread();
    /*
     *  Destroy edit action method thread
     */
    void destroyEditActionThread();

    /*------------------------------------------------------------------------*/
    /** Sets the Method Execution thread state

        @param eThreadState - contains the thread state
    */
    void setMethodExecutionThreadState(THREAD_STATE eThreadState);
    /*------------------------------------------------------------------------*/
	/** Sets the edit action execution thread state

		@param eThreadState - contains the thread state
	*/
    void setEditActionThreadState(THREAD_STATE eThreadState);

    /*------------------------------------------------------------------------*/
    /** Executes the method
    */
    void executeMethod();
    /*------------------------------------------------------------------------*/
	/** Execute post and pre edit action.
	 *
	 *  @return - error/success status.
	*/
    PMStatus executePrePostEditMethod();

    /*------------------------------------------------------------------------*/
    /** Retrieves the current state of Method Execution thread

        @return thread state
     */
    THREAD_STATE getMethodExecutionThreadState();
    /*------------------------------------------------------------------------*/
	/** Retrieves the current state of edit action method

		@return thread state
	 */
    THREAD_STATE getEditActionThreadState();

    /* Static callback Functions =============================================*/
    /*------------------------------------------------------------------------*/
    /** Implements Method Execution thread

        @param arg - contains instance of FFAdaptor
    */
    void methodExecutionThread(const void* arg);
    /** Implements edit action method thread

		@param arg - contains instance of FFAdaptor
	*/
    void editActionThreadProc(const void* arg);

    /* -------------------------------------------------------------------------
        Prepare Block config menu data
    */
    void prepareConfigMenuData(ParamInfo *paramInfo);

    /*------------------------------------------------------------------------*/
    /** check if Bus parameters are available.

        @param bus param json data

        @return true or false
    */
    bool isBusParamAvailable(const Json::Value& jsonData);

    /*-----------------------------------------------------------------------*/
    /** check if Application Settings are available

        @param jsonData - conatins bus param json

        @return true or false
    */
     bool isAppSettingsAvailable(const Json::Value& jsonData);
	 
    /*-----------------------------------------------------------------------*/
    /** Process pre/post edit actions

        @param flat_var - flat var structure

        @param block_tag  - block tag

        @param actionType   type of edit action.

        @return status of edit method execution
    */
     PMStatus processEditAction();

     /*-----------------------------------------------------------------------*/
	 /** clean up function after device is diconnected form the network

	    @return Error response
	 */
     PMStatus doCleanUP(unsigned char node_number);
	 
	/*-----------------------------------------------------------------------*/
    /** Sets language code to extract language specific string
        
        @param langCodeReq holds the lanuage code and short string flag

        @return Error response
    */
    Json::Value setLangCode(const Json::Value& langCodeReq);

    /*------------------------------------------------------------------------*/
    /** Executes edit action for Edit Display

        @param data - contains JSON input
        @param editActionType - contains edit action type
        @param genericItem - conatin generic item for edit display
        @param error - contrains error

        @return status
    */
    PMStatus execEditAction(const Json::Value& data,
                            EDIT_ACTION_TYPE editActionType,
                            DDI_GENERIC_ITEM &genericItem,
                            Error::Entry &error);

    /*------------------------------------------------------------------------*/
    /** Executes pre edit action for Edit Display

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value execPreEditAction(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Executes post edit action for Edit Display

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value execPostEditAction(const Json::Value& data);
	
	/*-----------------------------------------------------------------------*/
    /** Get the FFConnection manager instance.

        @return connection manager instance.
    */
	FFConnectionManager* getConnMangerInstance();

private:
    /* Private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/
    /**  Clears the scan table list

         @param pList Scan table pointer.
    */
    void cleanDeviceList(SCAN_TBL *pList);

    /*------------------------------------------------------------------------*/
    /**  set/copy default FF bus params
    */
    void setDefaultBusParams();

    /*------------------------------------------------------------------------*/
    /**  Assigns and validates the bus parameters values received from UI

         @param Json root.

         @return BusParameters status
    */
    FF_BUSPARAM_STATUS fillValues(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Updates Json values with user input and stores in Persistence storage

         @param Json root.
    */
    void modifyParams(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Store bus parameters in the persistence storage

         @param Json root.

         @return BusParameters status
    */
    FF_BUSPARAM_STATUS storeBusParams(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Sets the bus parameters value in the Persistence storage

         @param Json root.

         @return BusParameters status
    */
    FF_BUSPARAM_STATUS setBusParams(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Retrieves the bus parameters value from the Persistence storage

         @param Json root.

         @return BusParameters status
    */
    FF_BUSPARAM_STATUS getBusParams(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /** Returns the top level menu for the device as a JSON string

        @param  Reference to JSON::value information to be returned
    */
    void getTopLevelMenu(Json::Value & );

    /*-----------------------------------------------------------------------*/
    /** Builds the menu information as a JSON string

        @param ItemBasicInfo List containing the each item information

        @param Json::Value - Fills the menu information into Json Object
    */
    void getMenuAsJson(ItemBasicInfo &, list<ItemBasicInfo> &, Json::Value &);

    /* Process Item Methods ==================================================*/

    /*------------------------------------------------------------------------*/

    /*------------------------------------------------------------------------*/
    /**  Maps the processItem method function pointers to corresponding 
         method key
    */
    void mapDDSItemTypes();

    /*------------------------------------------------------------------------*/ 
    /**  Executes respective mapped method through function pointer based 
         on item type

        @param itemRequest Information containing item type in Json

        @param ITEMQUERY  enumeration differenciates set or get an item

        @return complete item information queried with status
    */
    Json::Value executeMappedMethod(const Json::Value& itemRequest,
                                    const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Image information

        @param imageRequest Image item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return json info
    */
    Json::Value processImage(const Json::Value& imageRequest,
                             const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Variable information

        @param varRequest Variable item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processVariable(const Json::Value& varRequest,
                                const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes variable Write-As-One list information

        @param varRequest Variable WAO item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value getVarWaoListItems(const Json::Value& varRequest,
                                  const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Get Edit Display items information

        @param input Edit Display item request information in Json

        @param output contains Edit Display items menu
    */
    void getEditDisplay(const Json::Value & input, Json::Value & output);

    /*------------------------------------------------------------------------*/
    /** Processes Menu information

        @param menuRequest Menu item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processMenu(const Json::Value& menuRequest,
                            const ITEM_QUERY ITEMQUERY);


    /*------------------------------------------------------------------------*/
    /** Processes Block information

        @param blockRequest Holds the resuest information to process block

        @param ITEMQUERY Enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processBlock(const Json::Value& blockRequest, const ITEM_QUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Method information

        @param method item information in Json

        @param ITEM_QUERY to set or get an item

        @return Error status
    */
    Json::Value processMethod(const Json::Value& methodRequest,
                                    const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Get Item for method execution

        @data method item information in Json

        @return status info
    */
    Json::Value processGetMethodExecution(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Processes Set Item for method execution

        @data method item information in Json

        @return status info
    */
    Json::Value processSetMethodExecution(const Json::Value& data);

    /* Get Item Methods ======================================================*/
    /*------------------------------------------------------------------------*/
    /** Retrieves Variable value with attributes

        @param varItems structure contains ParamItems information

        @param varValMask differenciates between variable with DD value 
                            and Device value

        @return Variable information with attributes as Json::Value
    */
    Json::Value getVariable(ParamItems& varItems,
                            bool& bIsValid,
                            VARIABLE_VAL_MASK varValMask = 
                            VARIABLE_VAL_MASK::VAR_ATTR_DEV_VAL_INFO);

    /*------------------------------------------------------------------------*/
    /** Retrieves Block Parameters with attributes and value

        @param itemContainerTag Contains block tag information

        @param menuJsonResp Json response containing menu information
    */
    void getBlockParams( std::string itemContainerTag,
                         Json::Value& menuJsonResp);

    /*------------------------------------------------------------------------*/
    /** Retrieves Record elements with attributes
        
        @param recReqItems ParamItems structure containing record item information

        @param output holds Json::Value response

        @param qualifier holds the menu qualifiers
    */
    void getRecord(ParamItems& recReqItems, Json::Value& output, unsigned short qualifier);

    /*------------------------------------------------------------------------*/
    /** Retrieves Record Variable elements with attributes
        
        @param recReqItems ParamItems structure containing record item information

        @param output holds Json::Value response

        @param qualifier holds the menu qualifiers
    */
    void getRecordVar(ParamItems& recReqItems, Json::Value& output, unsigned short qualifier);

    /*------------------------------------------------------------------------*/
    /** Retrieves Array elements with attributes

        @param arrReqItems ParamItems structure containing Array item information
        
        @param output holds Json::Value response

        @param qualifier holds the menu qualifiers
    */
    void getArray(ParamItems& arrReqItems, Json::Value& output, unsigned short qualifier);

    /*------------------------------------------------------------------------*/
    /** Retrieves Image value with attributes

        @param itemID Contains image Item ID

        @param itemContainerTag holds block tag name

        @return Variable information with attributes as Json::Value
    */
    Json::Value getImage(ITEM_ID& itemID, string itemContainerTag, bool& bIsValid);
	
    /*------------------------------------------------------------------------*/
	/** Execute post/pre edit actions

		@param methodID  contains method ID

		@param blockID  holds block id

		@return Status of execution
        */
    PMStatus executeEditMethod(ITEM_ID methodID, ITEM_ID blockID);

    /* Set Item Methods ======================================================*/
    /*------------------------------------------------------------------------*/
    /** Sets Variable value intot the device

        @param VarItems structure contains ParamItems information

        @param itemInfo variable information in Json

        @return status of set variable as Json::Value
    */
    Json::Value setVariable(ParamItems& varItems, Json::Value& itemInfo);

    /*------------------------------------------------------------------------*/
    /** Sets Variable value into the parameter info structure

        @param itemInfo holds the variable itemInfo request in Json::Value

        @param varParam structure holds variable parameter information

        @return status of setVariableValue method
    */
    PMStatus setVariableValue(  const Json::Value& itemInfo,
                                VariableType& varParam);

    /* Fill Item Methods =====================================================*/
    /*------------------------------------------------------------------------*/
    /** Fills Item Basic information into Json response

        @param itemBasicInfo Structure containing item basic information

        @param jsonResp Json response to be filled

    */
    void fillItemBasicJsonInfo(	ItemBasicInfo& itemBasicInfo,
                                Json::Value& jsonResp);

    /*------------------------------------------------------------------------*/
    /** Fills variable Basic information into Json response

        @param varBasicInfo Structure containing variable basic information

        @param jsonResp Json response to be filled

    */
    void fillVarBasicJsonInfo(	VarBasicInfo& varBasicInfo,
                                Json::Value& jsonResp);

    /*------------------------------------------------------------------------*/
    /** Fills Variable information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable information

        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVariableJsonInfo( VariableType* varParam,
                               Json::Value& varJsonResp,
                               VAR_JSON_ATTR_MASK varMaskType = 
                               VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*  ----------------------------------------------------------------------------
        Check the Write As One relation for the variable and fills it in variable
        JSON structure

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable information
    */
    void fillVariableHasRelation(VariableType *varParam,
                                 Json::Value& varJsonResp);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable Arithmetic information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable arithmetic info

        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarArithmeticJson(	VariableType* varParam,
                                Json::Value& varJsonResp,
                                VAR_JSON_ATTR_MASK varMaskType = 
                                VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable Enumeration information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable enumeration info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarEnumerationJson( VariableType* varParam,
                                 Json::Value& varJsonResp,
                                 VAR_JSON_ATTR_MASK varMaskType = 
                                 VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /**	Fills Variable String information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable string info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarStringJson( VariableType* varParam,
                            Json::Value& varJsonResp,
                            VAR_JSON_ATTR_MASK varMaskType = 
                            VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);
    /*-----------------------------------------------------------------------*/
    /** Fills Date-Time Information into Json response

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable date/time info
        
        @param VAR_JSON_ATTR_MASK holds enumeration value of variable attribute mask
    */
    void fillVarDateTimeJson( VariableType* varParam,
                              Json::Value& varJsonResp,
                              VAR_JSON_ATTR_MASK varMaskType = 
                              VAR_JSON_ATTR_MASK::VAR_JSON_ATTR_COMPLETE_MASK);

    /*-----------------------------------------------------------------------*/
    /** Fills Block parameters Json response

        @param paramInfo Structure consisting of parameter information

        @param blkParamResp Json::Value to contain block parameters info
    */
    void fillBlockParamsJson( ParamInfo* paramInfo,
                              Json::Value& blkParamResp);

    /*------------------------------------------------------------------------*/
    /** Fills Record information into Json response

        @param recParam Structure consisting of record type information

        @param blockParamJson as Json::Value to contain block parameters info

        @param itemContainerTag holds the block tag name       
    */
    void fillRecordJsonInfo(RecordType recParam,
                            Json::Value& blockParamJson,
                            string itemContainerTag);

    /*------------------------------------------------------------------------*/
    /** Fills Array information into Json response

        @param arrParam Structure consisting of array type information

        @param blockParamJson as Json::Value to contain block parameters info

        @param itemContainerTag holds the block tag name       
    */
    void fillArrayJsonInfo(ArrayType arrParam,
                           Json::Value& blockParamJson,
                           string itemContainerTag);

    /*------------------------------------------------------------------------*/
    /** Fills Image information into Json response

        @param imageParam Structure consisting of image type information

        @param blockParamJson as Json::Value to contain block parameters info
    */
    void fillImageJsonInfo(ImageType* imageParam,
                           Json::Value& imageParamJson);

    /*-----------------------------------------------------------------------*/
    /** Assigns min and max range values

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable date/time info
    */
    void assignMinMaxValues( VariableType* varParam,
                             Json::Value& varJsonResp);

    /*-----------------------------------------------------------------------*/
    /** Fills Json value key with variable value

        @param varParam VariableType structure information

        @param varJsonResp Json::Value to to fill variable value
    */
    void fillVarValue(  VariableType* varParam,
                        Json::Value& varValueJson);

    /*-----------------------------------------------------------------------*/
    /** Validate and get exact string value

        @return unicode/iso-latin-1 (includes ASCII) string
    */
    std::string validateAndGetStringValue (const char* string, unsigned int length);
    
    /*------------------------------------------------------------------------*/
    /** Get DD Menu information

        @param input Menu item request information in Json format

        @param output Return DD Menu content/error status infromation as Json
    */
    void getDDMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Custom Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getCustomMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Device Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getDeviceMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Advanced Device Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getAdvancedDeviceMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Level Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Advaced Block Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getAdvancedBlockMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Parameter List Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockParamListMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Views Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockViewsMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Methods Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockMethodsMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Configuration Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockConfigurationMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Status Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockStatusMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Alarms Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockAlarmsMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block View 1 Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockView1Menu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block View 2 Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockView2Menu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block View 3 Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockView3Menu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block View 4 Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockView4Menu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Fills all the required Json attributes for Custom Menu's

        @param input Menu item request information in Json format

        @param output Return Filled ItemBasicInfo Structure
    */
    void fillBasicMenuAttributes(const Json::Value& input,
                                 ItemBasicInfo& output);

    /*-----------------------------------------------------------------------*/
    /** Retrieves Variable value type from the map

        @param varType holds the variable value type

        @returns Variable value type from the map
    */
    std::string getVarValType(unsigned int varType);
    
    /*-----------------------------------------------------------------------*/
    /** Retrieves character set type from the map

        @param varType holds the char set value 

        @returns character set type from the map
    */
    std::string getCharSetType(unsigned int charSetVal);

    /*------------------------------------------------------------------------*/
    /** Creates subscriptions

        @param createSubReq subscription request in Json::Value

        @return status of create subscription
    */
    Json::Value createSubscription(const Json::Value& createSubReq);

    /*------------------------------------------------------------------------*/
    /** Starts subscriptions

        @param startSubReq subscription request in Json::Value

        @return status of start subscription
    */
    Json::Value startSubscription(const Json::Value& startSubReq);

    /*------------------------------------------------------------------------*/
    /** Stops subscriptions

        @param stopSubReq subscription request in Json::Value

        @return status of stop subscription
    */
    Json::Value stopSubscription(const Json::Value& stopSubReq);

    /*------------------------------------------------------------------------*/
    /** Deletes subscriptions

        @param delSubReq subscription request in Json::Value

        @return status of delete subscription
    */
    Json::Value deleteSubscription(const Json::Value& delSubReq);

    /*------------------------------------------------------------------------*/
    /**  Maps the processItem method function pointers to corresponding
         method key
    */
    void mapItemMethods();
    
    /*------------------------------------------------------------------------*/
    /** To fill specific information about each menu item based on the type

       @param sMenuItemInfo ItemInfo structure where label is filled

       @param ddiGenericItem Return value of ddi_get_item & ddi_get_item2 API
    */
    bool fillMenuItemInfoByType(ItemBasicInfo & sMenuItemInfo, DDI_GENERIC_ITEM & ddiGenericItem);

    /*------------------------------------------------------------------------*/
    /** Applies all the qualifiers for a Variable menu item as per the EDDL Spec

       @param qualifier Qualifiers received for the menu item 

       @param itemInfoJson Updated Json structure after applying qualifiers
    */
    void applyQualifiersForVariables(unsigned short qualifier,
				      Json::Value & itemJsonInfo);

    /*-----------------------------------------------------------------------*/
    /** Validates item for subscription if already subscribed or not

        @param itemId holds the Item ID requested for subscription
        @param paramInfo contains the list to verify if item already exists
          or not

        @returns if item exists in the list, returns true other false
    */
    bool isSubscriptionItemExists(ITEM_ID itemId, ParamInfo paramInfo);

    /*-----------------------------------------------------------------------*/
    /** Adds items to the internal subscription table

        @param varItem contains parameter information

        @returns status
    */
    PMStatus getSubParamInfo(VariableType* varItem);

    /*-----------------------------------------------------------------------*/
    /** Deletes all items from the internal subscription table
    */
    void deleteSubscriptionTableItem();

    /*------------------------------------------------------------------------*/
    /**  Creates internal subscription to update the cache table when the
         ST_REV counter increases

        @return status of create subscription
    */
    PMStatus createsInternalSubscription();

    /*------------------------------------------------------------------------*/
    /** Creates image file by using image raw data.

        @return image file path
    */
    const char* storeImage(ImageType* imageParam);

    /*------------------------------------------------------------------------*/
    /** Prepares symbol table of DD items by parsing symbol.txt file.

        @return status of preparing symbol table
    */
    PMStatus prepareSymTable();

    /*------------------------------------------------------------------------*/
    /** Returns Block menu name from symbol table

        @return Block menu name from symbol table
    */
    std::string fetchMenuNameFromMap(ITEM_ID menuID);

    /*------------------------------------------------------------------------*/
    /** Utility function for input validation for the variables
    
      @return FALSE if input fails with in range
    */
    template<typename RangeType, typename ValueType>
    bool validateIntegerRange(ValueType valuetype);

    /*------------------------------------------------------------------------*/
    /** Utility function for input validation with variable size
    
      @return SUCCESS if input value within range
    */
    int validateVariableLimit(const Json::Value& itemInfo, VariableType& varParam);

    /*  -----------------------------------------------------------------------*/
    /** Validates container tag input

        @param jsonRequest holds the JSON input request for an item

        @param itemContainerTag holds the container Tag

        @return SUCCESS if the containerTag request in JSON is valid, else FAILURE
    */
    int validateItemContTag(const Json::Value& jsonRequest, std::string& itemContainerTag);

    /*  ------------------------------------------------------------------------*/
    /** Validates Item ID input

        @param jsonRequest holds the JSON input request for an item

        @param itemID holds the container ItemID

        @return SUCCESS if the ItemID request in JSON is valid else FAILURE
    */
    int validateItemID(const Json::Value& jsonRequest, ITEM_ID& itemID);

    /*  -------------------------------------------------------------------------*/
    /** Validates Item Container ID input

        @param jsonRequest holds the JSON input request for an item

        @param containerID holds the ItemContainerID

        @return SUCCESS if the ContainerID request in JSON is valid else FAILURE
    */
    int validateItemContID(const Json::Value& jsonRequest, ITEM_ID& containerID);

    /*  -------------------------------------------------------------------------*/
    /** Validates Item Subindex input

        @param jsonRequest holds the JSON input request for an item

        @param subIndex holds the ItemSubIndex

        @return SUCCESS if the SubIndex request in JSON is valid else FAILURE
    */
    int validateItemSubIndex(const Json::Value& jsonRequest, unsigned short& subIndex);

   	/*  ------------------------------------------------------------------------*/
    /**  Validates item bit index input

        @param jsonRequest holds the JSON input request for an item

        @param itemBitIndex holds the ItemBItIndex

        @return SUCCESS if the bit index request in JSON is valid else FAILURE
     */
    int validateItemBitIndex(const Json::Value& jsonRequest, unsigned long& itemBitIndex);

   	/*  ------------------------------------------------------------------------*/
    /**  Validates Item Request Type input

        @param jsonRequest holds the JSON input request for an item

        @param reqType holds the ItemRequestType

        @return SUCCESS if the ItemRequestType in JSON is valid else FAILURE
    */
    int validateItemReqType(const Json::Value& jsonRequest, std::string& reqType);

    /*  ------------------------------------------------------------------------*/
    /** Validates Item in DD request input

        @param jsonRequest holds the JSON input request for an item

        @param itemType holds the ItemType

        @return SUCCESS if the ItemType in JSON is valid else FAILURE
    */
    int validateItemType(const Json::Value& jsonRequest, std::string& itemType);

    /*  ------------------------------------------------------------------------*/
    /**  Validates Item in DD request input

        @param jsonRequest holds the JSON input request for an item

        @param requiredStatw holds the state of ItemInDD required for an Item

        @return SUCCESS if the ItemInDD is as required in JSON is valid else FAILURE
    */
    int validateItemInDD(const Json::Value& jsonRequest, bool requiredState);

    /*  ------------------------------------------------------------------------*/
    /**  Validates Item has relation

        @param jsonRequest holds the JSON input request for an item

        @param itemHasRelation holds the value of ItemHasRelation of an Item

        @return SUCCESS if the ItemHasRelation is as required in JSON is valid else FAILURE
    */
    int validateItemHasRelation(const Json::Value& jsonRequest, bool& itemHasRelation);

    /*  ------------------------------------------------------------------------*/
    /**  Checks whether float value is not a number

        @param floatVal holds the float value input 

        @return true if the float value is not a number
    */
    bool isNAN(float floatVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether float value is infinity

        @param floatVal holds the float value input 

        @return true if the float value is infinity
    */
    bool isINF(float floatVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether double value is not a number

        @param floatVal holds the double value input 

        @return true if the double value is not a number
    */
    bool isNAN(double doubleVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether double value is infinity

        @param floatVal holds the double value input 

        @return true if the double value is infinity
    */
    bool isINF(double doubleVal);

    /*  ----------------------------------------------------------------------------*/
    /**  Returns array index in required format for Array Label     

        @param subIndex holds the aray index

        @return array index in required string format
    */
    std::string getArrayIndex(unsigned short subIndex);
	
    /*  ----------------------------------------------------------------------------*/
    /**  Fetches label string for Bit Enumeration Item

        @param varType variable type structure 

        @param bitEnumItem Reference to structure BitEnumItemStr
    */
    void getBitEnumItemStr(VariableType* varType, BitEnumItemStr& bitEnumItem);

    /*  ----------------------------------------------------------------------------*/
    /**  Sets BitEnmeration item value to parent Bit Enumeration variable

        @param devBitEnumVal, existing device value of parent Bit Enumeration variable

        @param bitEnumItemValue, Value to beset to a particular bit of parant 
                Bit Enumeration variable value

        @param bitIndex , position of bot to which the value needs to be modified
    */
    void setBitEnumItemVal( unsigned long& devBitEnumVal,
      					 bool bitEumItemVal,
                         unsigned long bitIndex);
						 
    /*-----------------------------------------------------------------------*/
    /** Adds the static parameters to the internal subscription table and
        skips the item if it is already added

        @param subscriptionID, contains the subscription ID

        @returns status info
    */
    PMStatus addSubscriptionCacheItems(unsigned int subscriptionID);

    /*-----------------------------------------------------------------------*/
    /** Removes the static parameters from the internal subscription table.

        @param subscriptionID, contains the subscription ID

        @returns status info
    */
    PMStatus removeSubscriptionCacheItems(unsigned int subscriptionID = 0);

    /*-----------------------------------------------------------------------*/
    /** Stops active methid execution thread 
    */
    void stopMethodExec();

    /*-----------------------------------------------------------------------*/
    /** Stops  and clears all active external/internal subscriptions  

        return Error status
    */
    PMStatus stopAndClearSubscription();

    /*-----------------------------------------------------------------------*/
    /** Clears subscription and disconnects device
        
        return device disconnection error status
    */
    PMStatus disconnectDevice();

    /*------------------------------------------------------------------------*/
    /** Stops device scanning thread on H1 network

        @return Error status
    */
    PMStatus stopScanThread();

    /* Explicitly Disabled Methods ===========================================*/
    /// copy constructor
    FFAdaptor(const FFAdaptor&) = delete;

    /// overloaded assignment operator
    FFAdaptor& operator= (const FFAdaptor& ) = delete;

public:
    /* Public Data Members ===================================================*/

	/// Holds current connected node address 
    int m_nodeAddress;

#ifdef UNITTEST
    FFConnectionManager *m_pFFCm;
    FFParameterCache *m_pFFPc;
    DDSController *m_pDds;
#endif

protected:
    /* Protected Data Members ================================================*/

    /// Pointer to DDS controller instance
    DDSController * m_pDDSController;

    /// Pointer to FFConnectionManager instance
    FFConnectionManager *m_pFFConnectionManager;

    /// Pointer to FFParameterCache instance
    FFParameterCache *m_pFFParameterCache;

    /// Pointer to BusinessLogic instance
    BusinessLogic *m_pBusinessLogic;

    /// Subscription Controller instance
    SubscriptionController* m_pSubscriptionController;

private:
    /* Private Data Members ==================================================*/

    /// Holds the current device scan iteration
    unsigned int m_unScanSeqID;

    ///  Holds the device scan table
    SCAN_DEV_TBL m_sScanDevTable;

    /// Member struct for FF bus Param
    FF_BUS_PARAMS m_busParams;

    /// Holds current Protocol Type
    CPMPROTOCOL m_Protocol;

    /// Holds publisher list
    std::vector<string> m_publisherList;

    /// Holds FF dd binary path
    std::string m_ffDDBinaryPath;

    /// Holds FF device path file
    std::string m_ffDeviceFile;

    /// internal subscription rate
    int m_internalSubscriptionRate;

    /// Mutex lock for thread ID's
    std::mutex m_threadIdLock;

    /// Mutex Thread Lock for connection
    std::mutex m_connectLock;

    /// For edit action
    EDIT_ACTION_DATA editData;

    /// Function pointer for process item methods
    typedef Json::Value (FFAdaptor::*processItemMethod)(const Json::Value&,
                                                        const ITEM_QUERY);

    /// Map holds process item methods against corresponding method key
    std::map <std::string, processItemMethod> processMethodMap;
    
    /// Map to hold DDS Item Types to CPM item strings
    std::map<unsigned int, std::string> m_itemTypeMap;

    std::vector<SUBSCRIPTION_CACHE_TBL*> m_subscriptionCacheTable;
    
    /// Array of function pointers to custom menu definitions
    void (FFAdaptor::*customMenu[CUSTOM_MENU_COUNT])(const Json::Value&, Json::Value &)
    {
        /// Make sure the order of initialisation matches with the CUSTOM_MENU_ID declaration
        &FFAdaptor::getDeviceMenu,
        &FFAdaptor::getAdvancedDeviceMenu,
        &FFAdaptor::getBlockMenu,
        &FFAdaptor::getAdvancedBlockMenu,
        &FFAdaptor::getBlockParamListMenu,
        &FFAdaptor::getBlockViewsMenu,
        &FFAdaptor::getBlockMethodsMenu,
        &FFAdaptor::getBlockConfigurationMenu,
        &FFAdaptor::getBlockView1Menu,
        &FFAdaptor::getBlockView2Menu,
        &FFAdaptor::getBlockView3Menu,
        &FFAdaptor::getBlockView4Menu,
        &FFAdaptor::getBlockAlarmsMenu,
        &FFAdaptor::getBlockStatusMenu
    };

    // Holds method thread id
    unsigned long m_methodThreadId;

    //Hold edit action thread id
    unsigned long  m_editActionThreadId;

    /// Map holding DDS type with corresponding variable value type name
    std::map <int, std::string> ddsVarValMap =
    {
        std::make_pair(DDS_INTEGER, JSON_VAR_VAL_TYPE_INT),
        std::make_pair(DDS_UNSIGNED,JSON_VAR_VAL_TYPE_UINT),
        std::make_pair(DDS_FLOAT, JSON_VAR_VAL_TYPE_FLOAT),
        std::make_pair(DDS_DOUBLE, JSON_VAR_VAL_TYPE_DOUBLE),
        std::make_pair(DDS_ENUMERATED, JSON_VAR_TYPE_ENUM),
        std::make_pair(DDS_BIT_ENUMERATED, JSON_VAR_VAL_TYPE_BITENUM),
        std::make_pair(DDS_DATE, JSON_VAR_VAL_TYPE_DATE),
        std::make_pair(DDS_TIME, JSON_VAR_VAL_TYPE_TIME),
        std::make_pair(DDS_PACKED_ASCII, JSON_VAR_VAL_TYPE_PACKED_ASCII),
        std::make_pair(DDS_PASSWORD, JSON_VAR_VAL_TYPE_PASSWORD),
        std::make_pair(DDS_ASCII, JSON_VAR_VAL_TYPE_ASCII),
        std::make_pair(DDS_EUC, JSON_VAR_VAL_TYPE_EUC),
        std::make_pair(DDS_BITSTRING, JSON_VAR_VAL_TYPE_BITSTRING),
        std::make_pair(DDS_OCTETSTRING, JSON_VAR_VAL_TYPE_OCTETSTRING),
        std::make_pair(DDS_VISIBLESTRING, JSON_VAR_VAL_TYPE_VISIBLESTRING),
        std::make_pair(DDS_DATE, JSON_VAR_VAL_TYPE_DATE),
        std::make_pair(DDS_DATE_AND_TIME, JSON_VAR_VAL_TYPE_DATE_TIME),
        std::make_pair(DDS_DURATION, JSON_VAR_VAL_TYPE_DURATION),
        std::make_pair(DDS_TIME_VALUE, JSON_VAR_VAL_TYPE_TIME_VAL_EPOCH),
        std::make_pair(DDS_BOOLEAN_T, JSON_VAR_VAL_TYPE_BOOLEAN),
        std::make_pair(DDS_MAX_TYPE, JSON_VAR_VAL_TYPE_MAXTYPE)
    };

    /// Map holds the character set type with character set value
    std::map <unsigned int, std::string> charSetMap = 
    {
        std::make_pair(0, JSON_CHAR_SET_ASCII),
        std::make_pair(1, JSON_CHAR_SET_ISO_LATIN),
        std::make_pair(2, JSON_CHAR_SET_EUC)
    };

    /// Holds the parsed symbol table from symbol file
    multimap<string, PARAM_DATA> m_SymTable;
};
