/******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    Global declaration require for Adaptors
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#ifndef _ADAPTOR_DEF_H
#define _ADAPTOR_DEF_H

#include "CommonDef.h"
#include "ParameterCache.h"

/* MACRO DEFINIIONS ***********************************************************/
#define INVALID_HANDLE (0xDEADBEEF)

#define DATE_TIME_CHAR_ARRAY_SIZE (8)
#define MAX_IMAGE_FILENAME_LEN    (256)
#define REPLACE_RANDOM_STRING     "XXXXXX"
#define INIT_STD_VALUE            0xffffffff

/// macro defines protocol type names
#define FF_PROTOCOL               "FF"
#define HART_PROTOCOL             "HART"
#define PROFIBUS_PROTOCOL         "PROFIBUS"

#define ATTR_MIN_MASK 0
#define ATTR_ALL_MASK 1
/// macro defines item type names
#define JSON_VARIABLE_ITEM_TYPE   "VARIABLE"
#define JSON_IMAGE_ITEM_TYPE      "IMAGE"
#define JSON_MENU_ITEM_TYPE       "MENU"
#define JSON_METHOD_ITEM_TYPE     "METHOD"
#define JSON_BLOCK_ITEM_TYPE	  "BLOCK"

/// macro defines variable type names
#define JSON_VAR_TYPE_ARITHMETIC "ARITHMETIC"
#define JSON_VAR_TYPE_ENUM       "ENUMERATION"
#define JSON_VAR_TYPE_STRING     "STRING"
#define JSON_VAR_TYPE_DATE_TIME  "DATE_TIME"

/// macro defines variable request type
#define JSON_VAR_TYPE_EDIT       "EDIT"
#define JSON_VAR_TYPE_UPDATE     "UPDATE"
#define JSON_VAR_TYPE_WRITE      "WRITE"

/// macro defines variable value type names
#define JSON_VAR_VAL_TYPE_INT            "INTEGER"
#define JSON_VAR_VAL_TYPE_UINT           "UNSIGNED_INTEGER"
#define JSON_VAR_VAL_TYPE_FLOAT          "FLOAT"
#define JSON_VAR_VAL_TYPE_DOUBLE         "DOUBLE"
#define JSON_VAR_VAL_TYPE_INDEX          "INDEX"
#define JSON_VAR_VAL_TYPE_MAXTYPE        "MAXTYPE"
#define JSON_VAR_VAL_TYPE_BOOLEAN        "BOOLEAN"
#define JSON_VAR_VAL_TYPE_DATE           "DATE"
#define JSON_VAR_VAL_TYPE_TIME           "TIME"
#define JSON_VAR_VAL_TYPE_TIME_VAL_EPOCH "TIME_VALUE_EPOCH"
#define JSON_VAR_VAL_TYPE_TIME_VAL_DIFF  "TIME_VALUE_DIFF"
#define JSON_VAR_VAL_TYPE_TIME_VAL_SCALED "TIME_VALUE_SCALED"
#define JSON_VAR_VAL_TYPE_DATE_TIME      "DATE_AND_TIME"
#define JSON_VAR_VAL_TYPE_DURATION       "DURATION"
#define JSON_VAR_VAL_TYPE_BITENUM        "BITENUMERATION"
#define JSON_VAR_VAL_TYPE_BITENUM_ITEM   "BITENUMERATION_ITEM"
#define JSON_VAR_VAL_TYPE_ENUM           "ENUMERATION"
#define JSON_VAR_VAL_TYPE_PACKED_ASCII   "PACKED_ASCII"
#define JSON_VAR_VAL_TYPE_ASCII          "ASCII"
#define JSON_VAR_VAL_TYPE_PASSWORD       "PASSWORD"
#define JSON_VAR_VAL_TYPE_EUC            "EUC"
#define JSON_VAR_VAL_TYPE_BITSTRING      "BITSTRING"
#define JSON_VAR_VAL_TYPE_OCTETSTRING    "OCTETSTRING"
#define JSON_VAR_VAL_TYPE_VISIBLESTRING  "VISIBLESTRING"

/// macro defines item basic type names
#define JSON_ITEM_LABEL      "ItemLabel"
#define JSON_ITEM_HELP       "ItemHelp"
#define JSON_ITEM_TYPE       "ItemType"
#define JSON_ITEM_CONT_TAG   "ItemContainerTag"
#define JSON_ITEM_ID         "ItemID"
#define JSON_ITEM_READONLY   "ItemReadOnly"
#define JSON_ITEM_IN_DD      "ItemInDD"
#define JSON_ITEM_REQ_TYPE   "ItemReqType"
#define JSON_ITEM_CLASS      "ItemClass"

/// macro defines scanning device information
#define JSON_DEV_INFO          "DeviceInfo"
#define JSON_DEV_TAG           "DeviceTag"
#define JSON_DEV_ADDR          "DeviceNodeAddress"
#define JSON_DEV_ID_INFO       "DeviceID"
#define JSON_DEV_MANFCTR       "DeviceManufacturer"
#define JSON_DEV_TYPE          "DeviceType"
#define JSON_DEV_REV           "DeviceRevision"
#define JSON_DEV_DD_REV        "DDRevision"
#define JSON_DEV_COMMISSIONED  "DeviceCommissioned"
#define JSON_DEV_SCAN_SEQ_ID   "ScanSeqID"
#define JSON_DEV_CONFIG        "DeviceConfig"

/// macro defines for field device configuration
#define JSON_DEV_CLASS            "DeviceClass"
#define JSON_NEW_DEV_TAG          "NewDeviceTag"
#define JSON_NEW_DEV_ADDR         "NewDeviceNodeAddress"
#define JSON_DEVICE_CLASS         "DEVICECLASS"
#define JSON_DEVICE_COMMISSION    "COMMISSION"
#define JSON_DEVICE_DECOMMISSION  "DECOMMISSION"
#define JSON_STATUS_MESSAGE       "StatusMessage"

/// macro defines for subscription types
#define JSON_SUB_CREATE      "create"
#define JSON_SUB_START       "start"
#define JSON_SUB_STOP        "stop"
#define JSON_SUB_DELETE      "delete"

/// macro definition for Json key names
#define JSON_ITEM_INFO       "ItemInfo"
#define JSON_ITEM_LIST       "ItemList"

#define JSON_ITEM_HAS_RELATION "ItemHasRelation"

#define JSON_VAR_TYPE        "VarType"
#define JSON_VAR_INFO        "VarInfo"
#define JSON_VAR_UNIT        "VarUnit"
#define JSON_VAR_VALUE       "VarValue"
#define JSON_VAR_CONT_ID     "VarContainerID"
#define JSON_VAR_SUBINDEX    "VarSubIndex"
#define JSON_DISPLAY_FORMAT  "VarValDisplayFormat"
#define JSON_EDIT_FORMAT     "VarValEditFormat"
#define JSON_VAR_VAL_INFO    "VarValInfo"
#define JSON_VAR_VAL_MILLISEC "VarValMilliSecPart"
#define JSON_VAR_VAL_MICROSEC "VarValMicroSecPart"
#define JSON_VAR_VAL_SCALED   "VarValueScaled"

#define JSON_VAR_VAL_TYPE    "VarValType"
#define JSON_VAR_BIT_INDEX   "VarBitIndex"
#define JSON_VAR_VAL_LENGTH  "VarValLength"
#define JSON_VAR_CHAR_SET    "VarCharSet"
#define JSON_VAR_VAL_RANGE   "VarValRange"
#define JSON_VAR_VAL_MIN     "VarValMin"
#define JSON_VAR_VAL_MAX     "VarValMax"
#define JSON_VAR_VAL_IS_NAN  "VarValIsNAN"
#define JSON_VAR_VAL_IS_INF  "VarValIsINF"

#define JSON_IMAGE_PATH      "ImagePath"
#define JSON_IMAGE_LENGTH    "ImageLength"
#define JSON_IMAGE_DATA      "ImageData"

#define JSON_VAR_ENUM_VALUE     "VarEnumValue"
#define JSON_VAR_ENUM_LABEL     "VarEnumLabel"
#define JSON_VAR_ENUM_HELP      "VarEnumHelp"
#define JSON_VAR_ENUM_INFO      "VarEnumInfo" 

#define JSON_METHOD_NOTIFY_TYPE "NotificationType"
#define JSON_METHOD_NOTIFY_MSG  "Message"
#define JSON_METHOD_ID          "MethodID"
#define JSON_METHOD_NAME        "MethodName"
#define JSON_METHOD_STATE       "MethodState"
#define JSON_METHOD_VAR_INFO    "MethodVarInfo"

#define JSON_METHOD_NOTIFY_ACK         "ACK"
#define JSON_METHOD_NOTIFY_ERROR       "ERROR"
#define JSON_METHOD_NOTIFY_START       "START"
#define JSON_METHOD_NOTIFY_ABORT       "ABORT"
#define JSON_METHOD_NOTIFY_DISPLAY     "DISPLAY"
#define JSON_METHOD_NOTIFY_GETVALUE    "GETVALUE"
#define JSON_METHOD_NOTIFY_COMPLETED   "COMPLETED"
#define JSON_METHOD_NOTIFY_SELECTION   "SELECTION"
#define JSON_METHOD_NOTIFY_CONTINUOUS  "CONTINUOUS"

#define JSON_FF_BUS_PARAMS     "FF_BUS_PARAMS"
#define JSON_HART_BUS_PARAMS   "HART_BUS_PARAMS"
#define JSON_PB_BUS_PARAMS     "PB_BUS_PARAMS"
#define JSON_HART_APP_SETTINGS "HART_APP_SETTINGS"
#define JSON_FF_APP_SETTINGS   "FF_APP_SETTINGS"
#define JSON_PB_APP_SETTINGS   "PB_APP_SETTINGS"

#define JSON_CHAR_SET_ISO_LATIN "ISO Latin-1"
#define JSON_CHAR_SET_EUC       "ISO EUC"
#define JSON_CHAR_SET_ASCII     "ISO ASCII"

/// macro definition for Json key names for subscription
#define JSON_ITEM_SUB_ACTION        "ItemAction"
#define JSON_ITEM_SUB_ID            "ItemSubscriptionID"
#define JSON_ITEM_SUB_REFRESH_RATE  "ItemRefreshRate"
#define JSON_ITEM_RES               "result"

#define JSON_LANG_CODE      "LangCode"
#define JSON_SHORT_STR_FLAG "ShortStrFlag"
#define DEFAULT_LANG_CODE   "en"
#define LANG_SHORT_STR      "zz"

/// macro for set prototocol Json key name
#define JSON_ITEM_PROTOCOL  "Protocol"

/// macro for pre/post edit actions
#define NO_ACTION 0
#define JSON_ITEM_STATUS_CLASS   "ItemStatusClass"
#define JSON_ITEM_STATUS		 "ItemStatus"
#define JSON_EDIT_ACTION		 "EDIT_ACTION"
#define JSON_CONNECT_NOTIFICATION 		 "CONNECT"

/// macros related to variables
#define MIN_LIMIT 0
#define VAR_SIZE_TWO 2
#define VAR_SIZE_ONE 1
#define SIGNED_VAR_SIZE_ONE 1
#define SIGNED_VAR_SIZE_TWO 2
/// Block IDs
#define AI_BLOCK_ID               0x800201D0
#define AO_BLOCK_ID               0x800201F0
#define DO_BLOCK_ID               0x80020230
#define DI_BLOCK_ID               0x80020210

/// Static Block Alarm Menus Item IDs
#define BLOCK_ERROR               0x800200AC
#define SET_FSTATE                0x80020162
#define FAULT_STATE               0x800200DA
#define CLR_FSTATE                0x800200B6
#define MAX_NOTIFY                0x80020120
#define LIM_NOTIFY                0x8002010E
#define CONFIRM_TIME              0x800200B8
#define ALARM_SUM_RES2            0x80020AF7
#define ACK_OPTION_RES2           0x80020AF6
#define ALARM_SUM_RES             0x80020318
#define ACK_OPTION_RES            0x80020317
#define WRITE_PRI                 0x80020190
#define ALARM_SUM                 0x80020035
#define ACK_OPTION                0x80020090
#define FEATURES                  0x800200DE
#define FEATURES_SEL              0x800200DC
#define BLOCK_ALM                 0x800200AA
#define UPDATE_EVT                0x80020030
#define FD_CHECK_ACTIVE           0x80020B29
#define FD_CHECK_MASK             0x80020B2C
#define FD_CHECK_ALM              0x80020B2A
#define FD_CHECK_PRI              0x80020B2D
#define FD_CHECK_MAP              0x80020B2B
#define FD_EXTENDED_ACTIVE_n      0x80020B54
#define FD_EXTENDED_MAP_n         0x800200DE
#define FD_FAIL_ACTIVE            0x80020B30

#define FD_FAIL_MAP               0x80020B32
#define FD_FAIL_ALM               0x80020B31
#define FD_FAIL_PRI               0x80020B34
#define FD_FAIL_MASK              0x80020B33

#define FD_OFFSPEC_MAP            0x80020B3C
#define FD_OFFSPEC_MASK           0x80020B3D
#define FD_OFFSPEC_PRI            0x80020B3E
#define FD_OFFSPEC_ALM            0x80020B3B
#define FD_OFFSPEC_ACTIVE         0x80020B3A

#define FD_MAINT_ACTIVE           0x80020B35
#define FD_MAINT_MAP              0x80020B37
#define FD_MAINT_MASK             0x80020B38
#define FD_MAINT_ALM              0x80020B36
#define FD_MAINT_PRI              0x80020B39
#define FD_RECOMMEND_ACT          0x80020B3F
#define FD_SIMULATE               0x80020B53
#define FD_VER                    0x80020B42

#define HI_ALM                    0x800200FA
#define HI_HI_PRI                 0x80020100
#define HI_HI_LIM                 0x800200FE
#define HI_PRI                    0x80020104
#define HI_LIM                    0x80020102
#define HI_HI_ALM                 0x800200FC
#define LO_ALM                    0x80020112
#define LO_LO_PRI                 0x8002011C
#define LO_LO_LIM                 0x8002011A
#define LO_PRI                    0x80020116
#define LO_LIM                    0x80020114
#define LO_LO_ALM                 0x80020118

#define DV_LO_ALM                 0x800200D4
#define DV_LO_PRI                 0x800200D8
#define DV_LO_LIM                 0x800200D6
#define DV_HI_ALM                 0x800200CE
#define DV_HI_PRI                 0x800200D2
#define DV_HI_LIM                 0x800200D0

///Polling Address Ranges
#define MAX_POLL_ADDRESS_0 0 ///Default address 0
#define MAX_POLL_ADDRESS_15 15 ///Poll Address Range 0 to 15
#define MAX_POLL_ADDRESS_63 63 ///Poll Address Range 0 to 63

#define INTERNAL_SUBSCRIPTION_REFRESH_RATE  2
#define METHOD_EXEC_SLEEP_INTERVAL  (500)

/* STRUCTURES and ENUMS DECLARATIONS ******************************************/
/// enum to differenciate get or set item query
enum class ITEM_QUERY
{
    GET_ITEM = 0,   ///< Get item query
    SET_ITEM        ///< Set item query
};

/// Enum to differenciate variable masks
enum class VAR_JSON_ATTR_MASK
{
    VAR_JSON_ATTR_COMPLETE_MASK = 0, ///< Fills all variable attribute information in JSON
    VAR_JSON_VALUE_MASK             ///< Fills only value into JSON response
};

/*----------------------------------------------------------------------------*/
/// Item basic Information
typedef struct itemBasicInfo
{
    std::string itemHelp{""};           ///< Holds help string
    std::string itemLabel{""};          ///< Holds label string
    std::string itemType{""};           ///< Holds item type
    std::string itemContainerTag{""};   ///< Holds the container Id
    unsigned long itemID{0};            ///< Holds item Id
    bool itemInDD{true};                ///< Holds the item availabilty in dd
    unsigned long bitIndex;			///< Holds BitIndex
}ItemBasicInfo;

/// Variable basic Information
typedef struct varBasicInfo : ItemBasicInfo
{
    ItemBasicInfo basicInfo{};  ///< Holds item basic information
    bool itemReadOnly{true};    ///< Holds the item handling value
    unsigned long itemClass{0}; ///< Holds class attribute
}VarBasicInfo;

/// Method basic Information
typedef struct methodBasicInfo : VarBasicInfo
{
    ItemBasicInfo basicInfo{};  ///< Holds item basic information
    bool itemReadOnly{true};    ///< Holds the item handling value
}MethodBasicInfo;

/// Variable items information
typedef struct paramItems
{
    unsigned long   paramItemID{0};
    unsigned long   paramContainerID{0};
    unsigned short  paramSubIndex{0};
    std::string     paramContainerTag{""};
    MaskType        paramMaskType{MaskType::ITEM_ATTR_ALL_MASK};
    unsigned short  paramBitIndex{0};
    std::string     paramType{0};
}ParamItems;

/// Subscription reference items information
typedef struct subscriptionCacheItem
{
    unsigned long  itemID;
    VariableType* varType;
    int refCounter{0};

}SUBSCRIPTION_CACHE_ITEM;

/// Variable items information
typedef struct subscriptionCacheTable
{
      char* BlockTag;
      std::vector<subscriptionCacheItem> cacheItems{};

}SUBSCRIPTION_CACHE_TBL;


/* ENUMERATIONS AND STRUCTURES ************************************************/
///Enumerations for poll by address options
enum class PollByAddress
{
    ADDRESS_0 = 0, ///Default address 0
    ADDRESS_0_15, ///HART revision 5 uses polling addresses from 0 to 15
    ADDRESS_0_63 ///Higher HART revision uses polling addresses from 0 to 63
};

/// HART Master Types
enum class HartMasterType
{
    HART_SECONDARY_MASTER = 0, ///Secondary master
    HART_PRIMARY_MASTER = 1 ///Primary master
};

/// Enumeration definies the method execution thread state
typedef enum class METhreadState
{
    RUN = 0,	/**< runs method execution thread */
    STOP = 1	/**< terminates method execution thread */
}METhreadStateT;

enum class EditActionTypes
{
    PRE_EDIT_ACTION = 1,
    POST_EDIT_ACTION
};

enum class EditActionItemTypes
{
    VARIABLE_ACTION = 1,
    EDIT_DISPLAY_ACTION
};

#endif //__ADAPTOR_DEF_H
