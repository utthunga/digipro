/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PB Adaptor class declaration
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES *************************************************************/
#include "IProtocolAdaptor.h"
#include "PBConnectionManager.h"
#include "PBParameterCache.h"
#include "IFakController.h"
#include "BusinessLogic.h"
#include "commonstructure/CommonStructures.h"
#include "custom_menu/CustomMenuInfo.h"
#include "error/CpmErrors.h"
#include "IAppConfigParser.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <array>
#include <vector>
#include <limits>

#define HACK_CPM

/* ========================================================================== */

/// Enumeration defines BusParameters status in PB Adapter layer
typedef enum class PBBusParametersStatus
{
    PB_BUSPARAM_SUCCESS                         =  0,
    PB_BUSPARAM_FAIL                            = -1,
    PB_BUSPARAM_GET_BUS_PARAMS_FAIL             = -2,
    PB_BUSPARAM_DEFUALT_PARAM_FAILED            = -3,
    PB_BUSPARAM_INVALID_BUS_PARAMS              = -4,
    PB_BUSPARAM_WRONG_VALUE_FOR_TSL             = -5,
    PB_BUSPARAM_WRONG_VALUE_FOR_MIN_TSDR        = -6,
    PB_BUSPARAM_WRONG_VALUE_FOR_MAX_TSDR        = -7,
    PB_BUSPARAM_WRONG_VALUE_FOR_TQUI            = -8,
    PB_BUSPARAM_WRONG_VALUE_FOR_TSET            = -9,
    PB_BUSPARAM_WRONG_VALUE_FOR_TTR             = -10,
    PB_BUSPARAM_WRONG_VALUE_FOR_G               = -11,
    PB_BUSPARAM_WRONG_VALUE_FOR_HSA             = -12,
    PB_BUSPARAM_WRONG_VALUE_FOR_MAX_RETRY_LIMIT = -13
}PB_BUSPARAM_STATUS;

/// Structure defines PB BusParameters
typedef struct PB_BusParameters
{
    USIGN16        tsl;           /**< slot time, range 37 - 16383 */
    USIGN16        minTsdr;       /**< min. station delay time resp., range 11 - 1023 */
    USIGN16        maxTsdr;       /**< max. station delay time resp., range 37 - 65535 */
    USIGN8         tqui;          /**< quiet time, range 0 - 493 */
    USIGN8         tset;          /**< setup time, range 1 - 494*/
    USIGN32        ttr;           /**< target token rotation time, range 256 - 16776960 */
    USIGN8         g;             /**< gap update factor, range 0 - 100. Stack will use highest permitted value if value exceeds the range */
    USIGN8         hsa;           /**< highest station address, range 1 - 126. Stack will use highest permitted value if value exceeds the range */
    USIGN8         maxRetryLimit; /**< max. retry limit, range 0 - 7. Stack will use highest permitted value if value exceeds the range */
}PB_BUS_PARAMS;

/// Other HART APPLICATION SETTINGS
typedef struct PB_AppSettings
{
    USIGN32 address; // local node address for master, range 0 - 125
}PB_APP_SETTINGS;

/// Structure defines Device Scan Table
typedef struct {
    USIGN16        deviceManufacturer;
    USIGN16        deviceType;
    USIGN16        deviceRevision;
    USIGN16        ddRevision;
} PB_DD_DEVICE_ID;

typedef struct _PB_SCAN_TBL
{
    USIGN16      stationAddress;
    PB_DD_DEVICE_ID deviceID;
}PB_SCAN_TBL;

typedef struct _PB_SCAN_DEV_TBL
{
    std::list<PB_SCAN_TBL> list;
}PB_SCAN_DEV_TBL;

/// MACRO DEFINITIONS for PB Bus Parameters
#define PB_BUS_TSL                  "pb_bus_tsl"
#define PB_BUS_MIN_TSDR             "pb_bus_min_tsdr"
#define PB_BUS_MAX_TSDR             "pb_bus_max_tsdr"
#define PB_BUS_TQUI                 "pb_bus_tqui"
#define PB_BUS_TSET                 "pb_bus_tset"
#define PB_BUS_TTR                  "pb_bus_ttr"
#define PB_BUS_G                    "pb_bus_g"
#define PB_BUS_HSA                  "pb_bus_hsa"
#define PB_BUS_MAX_RETRY_LIMIT      "pb_bus_max_retry_limit"

/// MACRO DEFINITIONS for PB App Settings
#define PB_LOC_ADDR "pb_loc_add"

#define ROOT                        root["PB_BUS_PARAMS"]
#define PB_BUS_PARM_KEY                 "PB_BUS_PARAMS"
#define PB_APP_SET_KEY              "PB_APP_SETTINGS"
#define SET_DEFAULT                 "set_default"

#define INITIAL_VALUE               (-1)
#define DEFAULT_INITIAL             0
#define DEFAULT_PARAMS              1
#define DEFAULT_MASTER              2

#define DEFAULT_TSL                 600
#define DEFAULT_MIN_TSDR            11
#define DEFAULT_MAX_TSDR            60
#define DEFAULT_TQUI                0
#define DEFAULT_TSET                1
#define DEFAULT_TTR                 60000
#define DEFAULT_G                   1
#define DEFAULT_HSA                 126
#define DEFAULT_MAX_RETRY_LIMIT     3

#define DEFAULT_LOC_ADDR            2
#define PB_APP_LOC_ADD_MIN          0
#define PB_APP_LOC_ADD_MAX          125

#define DEFAULT_LSEG                255
#define PB_BUS_TSL_MIN              37
#define PB_BUS_TSL_MAX              16383
#define PB_BUS_MIN_TSDR_MIN         11
#define PB_BUS_MIN_TSDR_MAX         1023
#define PB_BUS_MAX_TSDR_MIN         37
#define PB_BUS_MAX_TSDR_MAX         65535
#define PB_BUS_TQUI_MIN             0
#define PB_BUS_TQUI_MAX             493
#define PB_BUS_TSET_MIN             1
#define PB_BUS_TSET_MAX             494
#define PB_BUS_TTR_MIN              256
#define PB_BUS_TTR_MAX              16776960
#define PB_BUS_G_MIN                0
#define PB_BUS_G_MAX                100
#define PB_BUS_HSA_MIN              1
#define PB_BUS_HSA_MAX              126
#define PB_BUS_MAX_RETRY_LIMIT_MIN  0
#define PB_BUS_MAX_RETRY_LIMIT_MAX  7

#define MAX_NUM_SLAVES              125
#define MAX_BUFF_LEN                32
#define MAX_SLAVE_ENTRY             300
#define MAX_BUS_PARAM_LEN           128
#define MAX_RECV_TIMEOUT            100
#define MAX_SEND_TIMEOUT            200
#define MAX_CONN_TIMEOUT            1000

/// PB address range info
#define MIN_VALID_ADDRESS           0
#define MAX_VALID_ADDRESS           126
#define MAX_DEFAULT_ADDRESS         126

/// PB Commission/decomission state
#define COMMISSIONED                0
#define DECOMMISSIONED              1
#define NOT_APPLICABLE              2

/* CLASS DECLARATIONS *********************************************************/
/** class description of PBAdaptor class.

    PBAdaptor Class consists of methods related to FieldBus initialization, connect
    & disconnect device, get & set data DD data, Bus parameters, device scanning.
*/
class PBAdaptor : public IProtocolAdaptor
{
public:

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs PBAdaptor Class.
    */
    PBAdaptor();

    /*------------------------------------------------------------------------*/
    /** virtual destructor, destroyes PBAdaptor class.
    */
    virtual ~PBAdaptor();

    /* Public Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Initializes PB Adaptor

        @return Error string
    */
    Json::Value initialise();

    /*------------------------------------------------------------------------*/
    /** Retrieves the current active protocol

        @return Active protocol type
    */
    CPMPROTOCOL getActiveProtocol();

    /*------------------------------------------------------------------------*/
    /** Deallocates and shuts down the PB adaptor and its properties

        @return status error/success
    */
    Json::Value shutDown();

    /*------------------------------------------------------------------------*/
    /** Gets device description header information
    */
    Json::Value getDDHeaderInfo();

    /*------------------------------------------------------------------------*/
    /** Updates device description data
    */
    Json::Value updateDD(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Reads PB basic device data

        @param request to fetch item information in Json

        @return item information/error in Json
    */
    Json::Value getItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Writes data to PB basic device

        @param request to set item value in json

        @return response/error in Json
    */
    Json::Value setItem(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Process requested subscription action

        @param subReq subscription action request in Json::Value

        @return Process subscription response
    */
    Json::Value processSubscription(const Json::Value& subReq);

    /*------------------------------------------------------------------------*/
    /** Start device scanning operation on Profibus DP network

        @param data scan related Json request

        @return Error Status
    */
    Json::Value startScan(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Stops device scanning operation on Profibus network

        @return Error response
    */
    Json::Value stopScan();

    /*------------------------------------------------------------------------*/
    /** Scans Profibus network for PA devices

        @return PMStatus status os PB scan network
    */
    PMStatus scanNetwork();

    /*------------------------------------------------------------------------*/
    /** Connets to PB basic device

    @param connection request in Json

        @return error response
    */
    Json::Value connect(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Disconnets from PB basic device

        @return error response
    */
    Json::Value disconnect();

    /*------------------------------------------------------------------------*/
    /** Fills menu items Basic information into Json response

        @param locMenuList contains the list of menu items information

        @param jsonResp Json response to be filled

    */
    PMStatus fillMenuItemsJson(	edd::Interpreter::menuitems_t& locMenuList, Json::Value &output);

    /*------------------------------------------------------------------------*/
    /** Sets Profibus network bus parameters

        @param Bus parameters json data string

        @return SUCCESS/Error String
    */
    Json::Value setBusParameters(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves the Profibus network  bus parameters

        @return bus parameters in Json data string
    */
    Json::Value getBusParameters();

    /*------------------------------------------------------------------------*/
    /** Populates scanned device information

        @return uint32_t : device information populate error
    */
    PMStatus populateDeviceInfo();

    /*------------------------------------------------------------------------*/
    /** Interfaces for the below layers to talk to
        Protocol Adaptor sets PB communication failure
    */
    void setCommunicationFailure();

    /*------------------------------------------------------------------------*/
    /**  Processes device alerts

         @param
    */
    void processAlerts(void *);

    /*------------------------------------------------------------------------*/
    /** Get Default Bus parameters from PS

        @param  default Bus Parameters in string
    */
    void getDefaultBusParameters(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /** Retrieves the basic field device information such as Device Address,
            Device Tag and Device Class info in JSON format.

        @param jsonData Contains field device info such as id

        @return json value
    */
    Json::Value getFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Assigns the user configuration such as Node Address,
        Physical Device Tag, and Device Class info to physical device and
        returns the status in JSON format

        @param jsonData Contains field device info such as id

        @return json value
    */
    Json::Value setFieldDeviceConfig(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Stores the other configuration settings in the persistent storage.

        @param jsonData - application setting in JSON structure

        @return return  - error/success JSON data
    */
    Json::Value setAppSettings(const Json::Value &jsonData);

    /*------------------------------------------------------------------------*/
    /** Retrieves application configuration settings from the persistent storage.

        @return - error/success JSON data
    */
    Json::Value getAppSettings();

    /*------------------------------------------------------------------------*/
    /** Get Default application settings from PS

         @param  default application settings in string
    */
    void getDefaultAppSettings(Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** check if Bus parameters are available.

        @param bus param json data

        @return true or false
    */
    bool isBusParamAvailable(const Json::Value& jsonData);

    /*-----------------------------------------------------------------------*/
    /** check if Application Settings are available

        @param jsonData - conatins bus param json

        @return true or false
    */
     bool isAppSettingsAvailable(const Json::Value& jsonData);

    /*------------------------------------------------------------------------*/
    /** Executes pre edit action for Edit Display

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value execPreEditAction(const Json::Value& data);

    /*------------------------------------------------------------------------*/
    /** Executes post edit action for Edit Display

        @param data - contains JSON input

        @return JSON result
    */
    Json::Value execPostEditAction(const Json::Value& data);

private:
    /* Private Member Functions ==============================================*/
    /*------------------------------------------------------------------------*/ 
    /**  Executes respective mapped method through function pointer based 
         on item type

        @param itemRequest Information containing item type in Json

        @param ITEMQUERY  enumeration differenciates set or get an item

        @return complete item information queried with status
    */
    Json::Value executeMappedMethod(const Json::Value& itemRequest,
                                    const ITEM_QUERY ITEMQUERY);

    
    /*------------------------------------------------------------------------*/
    /** Processes Menu information

        @param menuRequest Menu item request information in Json

        @param ITEMQUERY enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processMenu(const Json::Value& menuRequest,
                            const ITEM_QUERY ITEMQUERY);

    /*------------------------------------------------------------------------*/
    /** Processes Block information

        @param blockRequest Holds the resuest information to process block

        @param ITEMQUERY Enumeration differenciates set or get an item

        @return Error status
    */
    Json::Value processBlock(const Json::Value& blockRequest, const ITEM_QUERY);

    /*------------------------------------------------------------------------*/
    /**  Maps the processItem method function pointers to corresponding
         method key
    */
    void mapItemMethods();


    /*------------------------------------------------------------------------*/
    /** Fetch the Bus Params from PS

        @param Bus paramters struct

        @return Error STATUS
    */
    PMStatus fetchBusParameters(void* params);

    /*------------------------------------------------------------------------*/
    /** gives stack configurable parameters

        @return STATUS
    */
    PMStatus getStackConfiguration();

    /*------------------------------------------------------------------------*/
    /**  set/copy default PB bus params
    */
    void setDefaultBusParams();

    /*------------------------------------------------------------------------*/
    /**  set/copy default PB app settings
    */
    void setDefaultAppSettings();

    /*------------------------------------------------------------------------*/
    /**  validates PB app settings
         @return status for app setting validation
    */
    PMStatus validateAppSettings();

    /*------------------------------------------------------------------------*/
    /**  Assigns and validates the bus parameters values received from UI

         @param Json root.

         @return BusParameters status
    */
    PB_BUSPARAM_STATUS validateBusParameters(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Updates Json values with user input and stores in Persistence storage

         @param Json root.
    */
    void modifyParams(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Store bus parameters in the persistence storage

         @param Json root.

         @return BusParameters status
    */
    PB_BUSPARAM_STATUS storeBusParams(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Sets the bus parameters value in the Persistence storage

         @param Json root.

         @return BusParameters status
    */
    PB_BUSPARAM_STATUS setBusParams(const Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Retrieves the bus parameters value from the Persistence storage

         @param Json root.

         @return BusParameters status
    */
    PB_BUSPARAM_STATUS getBusParams(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /**  Creates internal subscription to update the cache table

        @return status of create subscription
    */
    PMStatus createsInternalSubscription();
    
    /*-----------------------------------------------------------------------*/
    /** Deletes all items from the internal subscription table

        @returns status
    */
    PMStatus deleteSubscriptionTableItem();    

    /* Get Item Methods ======================================================*/
    /*------------------------------------------------------------------------*/
    /** Returns the top level menu for the device as a JSON string

        @param  Reference to JSON::value information to be returned
    */
    void getTopLevelMenu(Json::Value& root);

    /*-----------------------------------------------------------------------*/
    /** Retrieves the DD menu information

       @param itemID contains the requested item ID information

       @param Json::Value - Fills the menu information into Json Object
    */
    PMStatus getDDMenu(const unsigned long itemID, Json::Value & output);

    /*-----------------------------------------------------------------------*/
    /** Builds the menu information as a JSON string

        @param ItemBasicInfo List containing the each item information

        @param Json::Value - Fills the menu information into Json Object
    */
    void getMenuAsJson(ItemBasicInfo &, list<ItemBasicInfo> &, Json::Value &);

    /*------------------------------------------------------------------------*/
    /** Get Custom Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getCustomMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Device Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getDeviceMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Advanced Device Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getAdvancedDeviceMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Level Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getBlockMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Block Parameter List Menu Information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
     */
     void getBlockParamListMenu(const Json::Value& input, Json::Value& output);

    /*------------------------------------------------------------------------*/
    /** Get Advaced Block Menu information

        @param input Menu item request information in Json format

        @param output Return Custom Menu content/error status information as Json
    */
    void getAdvancedBlockMenu(const Json::Value& input, Json::Value& output);

    /* Fill Item Methods =====================================================*/
    /*------------------------------------------------------------------------*/
    /** Fills all the required Json attributes for Custom Menu's

        @param input Menu item request information in Json format

        @param output Return Filled ItemBasicInfo Structure
    */
    void fillBasicMenuAttributes(const Json::Value& input,
                                 ItemBasicInfo& output);


    /*------------------------------------------------------------------------*/
    /** Applies all the qualifiers for a Variable menu item as per the EDDL Spec

       @param qualifier Qualifiers received for the menu item

       @param itemInfoJson Updated Json structure after applying qualifiers
    */
    void applyQualifiersForVariables(unsigned short qualifier,
                      Json::Value & itemJsonInfo);

    /*------------------------------------------------------------------------*/
    /** Fills Item Basic information into Json response

        @param itemBasicInfo Structure containing item basic information

        @param jsonResp Json response to be filled
    */
    void fillItemBasicJsonInfo(ItemBasicInfo& itemBasicInfo,
                               Json::Value&   jsonResp);

    /*  -----------------------------------------------------------------------*/
    /** Validates container tag input

        @param jsonRequest holds the JSON input request for an item

        @param itemContainerTag holds the container Tag

        @return SUCCESS if the containerTag request in JSON is valid, else FAILURE
    */
    int validateItemContTag(const Json::Value& jsonRequest, std::string& itemContainerTag);
    
    /*  ------------------------------------------------------------------------*/
    /**  Validates Item in DD request input

        @param jsonRequest holds the JSON input request for an item

        @param requiredStatw holds the state of ItemInDD required for an Item

        @return SUCCESS if the ItemInDD is as required in JSON is valid else FAILURE
    */
    int validateItemInDD(const Json::Value& jsonRequest, bool requiredState);
    
    /*  -------------------------------------------------------------------------*/
    /** Validates Item Container ID input

        @param jsonRequest holds the JSON input request for an item

        @param containerID holds the ItemContainerID

        @return SUCCESS if the ContainerID request in JSON is valid else FAILURE
    */
    int validateItemContID(const Json::Value& jsonRequest, unsigned long& containerID);

    /*  ------------------------------------------------------------------------*/
    /** Validates Item ID input

        @param jsonRequest holds the JSON input request for an item

        @param itemID holds the container ItemID

        @return SUCCESS if the ItemID request in JSON is valid else FAILURE
    */    
    int validateItemID(const Json::Value& jsonRequest, unsigned long& itemID);

    /** Retrieves Variable value type from the map

        @param varType holds the variable value type

        @returns Variable value type from the map
    */
    std::string getVarValType(unsigned int varType);
    
    /*-----------------------------------------------------------------------*/
    /** Assigns min and max range values

        @param varParam Structure consisting of variable type information

        @param varJsonResp Json::Value to contain variable date/time info
    */
    void assignMinMaxValues( VariableType* varParam,
                             Json::Value& varJsonResp);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether float value is not a number

        @param floatVal holds the float value input 

        @return true if the float value is not a number
    */
    bool isNAN(float floatVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether float value is infinity

        @param floatVal holds the float value input 

        @return true if the float value is infinity
    */
    bool isINF(float floatVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether double value is not a number

        @param floatVal holds the double value input 

        @return true if the double value is not a number
    */
    bool isNAN(double doubleVal);
    
    /*  ------------------------------------------------------------------------*/
    /**  Checks whether double value is infinity

        @param floatVal holds the double value input 

        @return true if the double value is infinity
    */
    bool isINF(double doubleVal);

    /* Set Item Methods ======================================================*/
    /*------------------------------------------------------------------------*/
    /** Sets Variable value intot the device

        @param VarItems structure contains ParamItems information

        @param itemInfo variable information in Json

        @return status of set variable as Json::Value
    */
    Json::Value setVariable(ParamItems& varItems, Json::Value& itemInfo);

    /*------------------------------------------------------------------------*/
    /** Sets Variable value into the parameter info structure

        @param itemInfo holds the variable itemInfo request in Json::Value

        @param varParam structure holds variable parameter information

        @return status of setVariableValue method
    */
    PMStatus setVariableValue(  const Json::Value& itemInfo,
                                VariableType& varParam);

#ifdef HACK_CPM
    /* ----------------------------------------------------------------------------
        This function loads custom dd binary file
    */      
    void loadCustomDDBinaryFile(IAppConfigParser* pAppConfigParser);
#endif

    /* Explicitly Disabled Methods ===========================================*/
    /// copy constructor
    PBAdaptor(const PBAdaptor&) = delete;

    /// overloaded assignment operator
    PBAdaptor& operator= (const PBAdaptor& ) = delete;

public:
    /* Public Data Members ===================================================*/

    /// Holds current connected node address
    int m_nodeAddress;

protected:
    /* Protected Data Members ================================================*/

private:
    /* Private Data Members ==================================================*/

    /// Holds the current device scan iteration
    unsigned int m_unScanSeqID;

    ///  Holds the device scan table
    PB_SCAN_DEV_TBL m_sScanDevTable;

    /// Holds IFak Controller instance
    IFakController *m_pIFakController;

    /// Pointer to PBConnectionManager instance
    PBConnectionManager *m_pPBConnectionManager;

    /// Pointer to PBParameterCache instance
    PBParameterCache *m_pPBParameterCache;

    /// Subscription Controller instance
    SubscriptionController* m_pSubscriptionController;

    /// Pointer to BusinessLogic instance
    BusinessLogic *m_pBusinessLogic;

    /// Holds current Protocol Type
    CPMPROTOCOL m_Protocol;

    /// Member struct for PB bus Param
    PB_BUS_PARAMS m_busParams;

    /// Member struct for PB app settings
    PB_APP_SETTINGS m_appSetting;

    /// Holds PB DDL source path
    std::string m_pbDDLSourcePath;

    /// Holds PB device path file
    std::string m_pbDeviceFile;

    /// internal subscription rate
    int m_internalSubscriptionRate;

    /// Mutex Thread Lock for connection
    std::mutex m_connectLock;

    /// Function pointer for process item methods
    typedef Json::Value (PBAdaptor::*processItemMethod)(const Json::Value&,
                                                        const ITEM_QUERY);

    /// Map holds process item methods against corresponding method key
    std::map <std::string, processItemMethod> processMethodMap;

    /// Array of function pointers to custom menu definitions
    void (PBAdaptor::*customMenu[CUSTOM_MENU_COUNT])(const Json::Value&, Json::Value &)
    {
        /// Make sure the order of initialisation matches with the CUSTOM_MENU_ID declaration
        &PBAdaptor::getDeviceMenu,
        &PBAdaptor::getAdvancedDeviceMenu,
        &PBAdaptor::getBlockMenu,
        &PBAdaptor::getAdvancedBlockMenu,
        &PBAdaptor::getBlockParamListMenu
// TODO: Below menu will be implemented once we extend feature for menu / variable
        #if 0
        &PBAdaptor::getBlockViewsMenu,
        &PBAdaptor::getBlockMethodsMenu,
        &PBAdaptor::getBlockConfigurationMenu,
        &PBAdaptor::getBlockView1Menu,
        &PBAdaptor::getBlockView2Menu,
        &PBAdaptor::getBlockView3Menu,
        &PBAdaptor::getBlockView4Menu,
        &PBAdaptor::getBlockAlarmsMenu,
        &PBAdaptor::getBlockStatusMenu
#endif
    };

    /// Holds the Subscription items
    std::vector<SUBSCRIPTION_CACHE_TBL*> m_subscriptionCacheTable;

    // Store the device information in connect
    DeviceInfo* m_devInfo;
};
