/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Protocol Adaptor Interface class definition
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

#pragma once

/* INCLUDE FILES **************************************************************/
#include <string>
#include <json/json.h>
#include "PMStatus.h"
#include "PMErrorCode.h"
#include "AdaptorDef.h"

using namespace std;

/* CLASS FORWARD DECLARATIONS *************************************************/
class IConnectionManager; ///< Forward declaration of connection manager
class IParameterCache; ///< Forward declaration of parameter cache

/// Publisher information for both FF & HART protocol
#define SCAN_PUBLISHER              "SCAN_PUBLISHER"
#define ITEM_PUBLISHER              "ITEM_PUBLISHER"
#define STATUS_PUBLISHER            "STATUS_PUBLISHER"

/* INTERFACE CLASS DECLARATIONS ***********************************************/
/// Protocol Adaptor Interface Class

class IProtocolAdaptor
{
public:

    /* Public Types ==========================================================*/

    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Destruct a IProtocolAdaptor.
    *
    */
    virtual ~IProtocolAdaptor()
    {
    }

    /* Public Member Functions ===============================================*/

        /*--------------------------------------------------------------------*/
        /** Interface function for adaptor initialization

            @return status of the initiaslization in json string format
        */
        virtual Json::Value initialise() = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for destroying objects and shutdown it properly
         *
         */
        virtual Json::Value shutDown() = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for getting active protocol
         *
         *  @return currently active protocol
         */
        virtual CPMPROTOCOL getActiveProtocol() = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for getting dd header information
         *
         *  @return dd header inforamtion in json string format
         */
        virtual Json::Value getDDHeaderInfo() = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for updating dd binary file
         *
         *  @return status of the update
         */
        virtual Json::Value updateDD(const Json::Value& data) = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for get item data
         *
         *  @return status of read operation
         */
        virtual Json::Value getItem(const Json::Value& data) = 0;

        /*--------------------------------------------------------------------*/
        /** Interface function for set item data
         *
         *  @return status of write operation
         */
        virtual Json::Value setItem(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to start subscription
         *
         *  @param status of process susbcription 
         */
        virtual Json::Value processSubscription(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function for triggering scan operation
         *
         *  @return status of the scan operation
         */
        virtual Json::Value startScan(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to stop scan operation
         *
         *  @return status of the scan operation
         */
        virtual Json::Value stopScan() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to connect with field devices
         *
         *  @parma  contsins requires connect attributes in json format
         *  @return status of the connect operation
         */
        virtual Json::Value connect(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to disconnect from field device
         *
         *  @return status of the dicsonnect operation
         */
        virtual Json::Value disconnect() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to set bus parametere
         *
         *  @param  contains bus paramaters to be in json format
         *  @return status of the operation
         */
        virtual Json::Value setBusParameters(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to get bus paramters
         *
         *  @return bus parameters in json string format
         */
        virtual Json::Value getBusParameters() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to populate device inforamtion
         *
         *  @return protocol manager status structure
         */
        virtual PMStatus populateDeviceInfo() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to populayte device inforamtion
         *
         *  @return protocol manager status structure
         */
        virtual PMStatus scanNetwork() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to set communication failure
         *
         */
        virtual void setCommunicationFailure() = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to process alarm alerts
         *
         */
        virtual void processAlerts(void *) = 0;

        /*------------------------------------------------------------------------*/
        /** Interface function to get default bus parameters
         *
         *	@param  contains default bus parameters
         */
        virtual void getDefaultBusParameters(Json::Value& defaultParam) = 0;

        /*------------------------------------------------------------------------*/
        /** Retrieves the basic field device information such as Device Address,
            Device Tag and Device Class info in JSON format.

            @param paramInfo - contains field device info such as id

            @return json value
         */
        virtual Json::Value getFieldDeviceConfig(const Json::Value& jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** Assigns the user configuration such as Node Address,
            Physical Device Tag, and Device Class info to physical device and
            returns the status in JSON format

            @param paramInfo - contains field device info such as id

            @return json value
         */
        virtual Json::Value setFieldDeviceConfig(const Json::Value& jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** Stores the other configuration settings in the persistent storage.

            @param jsonData - application setting in JSON structure

            @return return  - error/success JSON data
        */
        virtual Json::Value setAppSettings(const Json::Value &jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** Retrieves application configuration settings from the persistent storage.

            @return - error/success JSON data
        */
        virtual Json::Value getAppSettings() = 0;

        /*------------------------------------------------------------------------*/
        /** Retrieves the Default HART Settings

            @param jsonData - contains default Bus Parameters and application
                    settings for HART protocol
        */
        virtual void getDefaultAppSettings(Json::Value& jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** check if Bus parameters are available

            @param jsonData - conatins bus param json

            @return true or false
        */
        virtual bool isBusParamAvailable(const Json::Value& jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** check if Application Settings are available

            @param jsonData - conatins bus param json

            @return true or false
        */
        virtual bool isAppSettingsAvailable(const Json::Value& jsonData) = 0;

        /*------------------------------------------------------------------------*/
        /** Executes pre edit action for Edit Display

            @param data - contains JSON input

            @return JSON result
        */
        virtual Json::Value execPreEditAction(const Json::Value& data) = 0;

        /*------------------------------------------------------------------------*/
        /** Executes post edit action for Edit Display

             @param data - contains JSON input

             @return JSON result
        */
        virtual Json::Value execPostEditAction(const Json::Value& data) = 0;

    protected:
        /* Protected Types ======================================================*/

        /// Holds current protocol type
        CPMPROTOCOL m_protocol;
};
