
/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Adesh Soni
    Origin:            ProStar
*/

/** @file
    PresistanceStorageInterfaceStatus class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <algorithm>
#include <sstream>
#include <string.h>
#include "PersistenceStorageInterface.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

/*  ----------------------------------------------------------------------------    
    get bus paramater configuration file
*/
std::string PersistenceStorageInterface::getBusParamFile()
{
    std::string busParamFile;

    IAppConfigParser *pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

    if (nullptr != pAppConfigParser)
    {
        std::string settingsPath = pAppConfigParser->getSettingsPath();
        busParamFile = settingsPath + '/' + std::string(BUS_PARAM_FILE);
    }

    return busParamFile;
}
/*  ----------------------------------------------------------------------------    
    Reads from persistence storage file
*/
PS_STATUS PersistenceStorageInterface::read(std::string &readData)
{
	PS_STATUS readstatus = PS_STATUS::PSI_READ_FAIL;
    std::fstream paramFile;
	paramFile.open(getBusParamFile().c_str(), std::ios::in);
	if(paramFile.is_open()) 
    {
		std::stringstream buff;
		buff << paramFile.rdbuf();
		readData = buff.str();
		readData.erase(std::remove(readData.begin(),readData.end(),'\n'),readData.end());
		paramFile.close();
		readstatus = PS_STATUS::PSI_SUCCESS;
	}

	return readstatus;
}

/*  ----------------------------------------------------------------------------    
    Writes a file in persistence storage
*/
PS_STATUS PersistenceStorageInterface::write(std::string &data)
{
	PS_STATUS status = PS_STATUS::PSI_WRITE_FAIL;

    std::fstream paramFile;
	paramFile.open(getBusParamFile().c_str(), std::ios::out);
	if (paramFile.is_open())
	{
        LOGF_INFO(CPMAPP,"BUS_PARAM_INFO: writing %s data on file " << data.c_str());
		paramFile<<data;
		paramFile.close();
		status = PS_STATUS::PSI_SUCCESS;
	}
	return status;
}
/*  ----------------------------------------------------------------------------    
    Checks for file in persistence storage
*/
PS_STATUS PersistenceStorageInterface::checkFile()
{
	PS_STATUS checkStatus = PS_STATUS::PSI_FILE_NOT_PRESENT;

	if (std::ifstream(getBusParamFile().c_str()))
	{
         LOGF_INFO(CPMAPP,"BUS_PARAM_INFO: File already exists");
		 checkStatus = PS_STATUS::PSI_FILE_PRESENT;
	}
	return checkStatus;
}

/*  ----------------------------------------------------------------------------    
    Remove file in persistence storage
*/
PS_STATUS PersistenceStorageInterface::remove(std::string &file_name)
{
	PS_STATUS status = PS_STATUS::PSI_FAIL;

	if (0 != std::remove(file_name.c_str()))
	{
        LOGF_INFO(CPMAPP,"BUS_PARAM_INFO: removing file failed " << file_name.c_str());
	}
    else
    {
	    status = PS_STATUS::PSI_SUCCESS;
    }
	return status;
}
