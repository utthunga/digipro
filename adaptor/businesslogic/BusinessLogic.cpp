/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj, Adesh Soni
    Origin:            ProStar
*/

/** @file
    BusinessLogic class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include "BusinessLogic.h"
#include "CPMStateMachine.h"
#include "iostream"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

using namespace std;


#define SUCCESS 0
#define FAILURE 1

#define HART_DEVICE_SCAN_NOTIFY_INTERVAL      2000

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs BusinessLogic Class
*/
BusinessLogic::BusinessLogic(IProtocolAdaptor* pAdaptorInstance)
    :m_pAdaptorInstance(nullptr), m_scanThreadID(0)
{
    m_pAdaptorInstance = pAdaptorInstance;
}

/*  ---------------------------------------------------------------------------
    Initializes business logic and creates Bus parameter file
*/
PMStatus BusinessLogic::initialize()
{
    return prepareBusParams();
}

/*  ---------------------------------------------------------------------------
   Creates scan/re-scan timer thread
*/
void BusinessLogic::createScanThread()
{
    m_threadIdLock.lock();
    std::function<void (const void *)> scanFuncPtr =
                        std::bind(&BusinessLogic::scanningThread, this, this);

    m_scanThreadID = ThreadManager::getInstance()->createThread(scanFuncPtr, this);
    m_threadIdLock.unlock();
}

/*  ---------------------------------------------------------------------------
    Destroyes device scanning thread
*/
void BusinessLogic::destroyScanThread()
{
    // Set the scanning thread state to stop
    setScanThreadState(THREAD_STATE_STOP);

    // Clear the scan in progress state
    CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_SCANNING_IN_PROGRESS);
    
    // Wait until the previous scan thread exits correctly
    std::unique_lock<std::mutex> scanLock(m_scanProceedMutex);
    m_scanProceed.wait(scanLock,[this] { return THREAD_STATE_INVALID == getScanThreadState();});
}

/*  ---------------------------------------------------------------------------
    Implements device scanning thread
*/
void BusinessLogic::scanningThread(const void* arg)
{
    m_threadIdLock.lock();
    BusinessLogic* pBLInstance = static_cast<BusinessLogic*>(const_cast<void*>(arg));
    m_threadIdLock.unlock();
    pBLInstance->doScanning();
}

/*  ---------------------------------------------------------------------------
    Scans the bus for devices and populates the device data
*/
void BusinessLogic::doScanning()
{
    PMStatus doScanStatus;

    // Set state as scanning in progress
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_SCANNING_IN_PROGRESS);

    while (THREAD_STATE_RUN == getScanThreadState())
    {
        doScanStatus = m_pAdaptorInstance->scanNetwork();

        // Check for ScanNetwork, if successul Populate Device Information
        if (doScanStatus.hasError())
        {
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_SCANNING_IN_PROGRESS);
            break;
        }
    }
    
    // Remove the thread id from the thread table maintained by thread manager
    ThreadManager::getInstance()->destroyThread(m_scanThreadID);

    // Reset the thread id
    m_scanThreadID = 0;

    // Set the scanning thread state to invalid
    setScanThreadState(THREAD_STATE_INVALID);
    // Notify proceed to the new Scan request 
    m_scanProceed.notify_one();
}

/*  ---------------------------------------------------------------------------
    Sets the device scanning thread state
*/
void BusinessLogic::setScanThreadState(THREAD_STATE eThreadState)
{
    ThreadManager::getInstance()->setThreadState(m_scanThreadID, eThreadState);
}

/*  ---------------------------------------------------------------------------
    Retrieves the current state of device scanning thread
*/
THREAD_STATE BusinessLogic::getScanThreadState()
{
    return ThreadManager::getInstance()->getThreadState(m_scanThreadID);
}

/*  ---------------------------------------------------------------------------
    Gets bus parameters from persistent storage
*/
PMStatus BusinessLogic::readConfigurationValues(Json::Value& root)
{
    PMStatus status;
    std::string readData;
    Json::Reader reader(Json::Features::strictMode());

    PS_STATUS retPSI = PS_STATUS::PSI_SUCCESS;
    PS_STATUS readStatus = PS_STATUS::PSI_READ_FAIL;

    auto *pPSI = new PersistenceStorageInterface();
    if (nullptr == pPSI)
    {
        status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL, PM_ERR_CL_FF_ADAPTOR,FF_ADAPTOR_SC_BL);
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: PSI instance is NULL in BL getBusParameters()");
        return status;
    }

    // read the std::string data from the file/datbase and pass it to JSON parse
    retPSI = pPSI->read(readData);
    
    if (PS_STATUS::PSI_SUCCESS != retPSI)
    {
        status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_READ_FAIL,
                                    PM_ERR_CL_FF_ADAPTOR,FF_ADAPTOR_SC_BL);
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: read fail in BL getBusParameters()");
    
        // deleting PS instance here(if read fail or success), because we don't need it further
        delete pPSI;
        return status;
    }

    // will get JSON::value from read data from the File/Database
    bool parsingString = reader.parse(readData, root);
    if (parsingString)
    {
        parsingString = validateJson(readData);
    }

    if (!parsingString)
    {
        std::string file_name = pPSI->getBusParamFile();
        pPSI->remove(file_name);
        LOGF_INFO(CPMAPP, "busparam.json  file is corrupted!!, restoroing default parameters");

        Json::Value temp_root;
        // get default bus params to store in persistance storage
        m_pAdaptorInstance->getDefaultBusParameters(temp_root);

        // get default app setting to store in persistance storage
        m_pAdaptorInstance->getDefaultAppSettings(temp_root);

        // write data (convert JSon::Value to std::string) in opened  file.
        Json::StyledWriter styleWriter;
        std::string defaultBusParams = styleWriter.write(temp_root);

        if (PS_STATUS::PSI_SUCCESS == pPSI->write(defaultBusParams))
        {
            // read the std::string data from the file/datbase and pass it to JSON parse
            readData.clear();

            readStatus = pPSI->read(readData);
    
            reader.parse(readData,root);
        }
        
        if (PS_STATUS::PSI_SUCCESS != readStatus)
        {
            status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_READ_FAIL,
                                    PM_ERR_CL_FF_ADAPTOR,FF_ADAPTOR_SC_BL);
            delete pPSI;
            return status;
        }
    }
    
    // deleting PS instance here(if read fail or success), because we don't need it further
    delete pPSI;
    return status;
}

/*  ---------------------------------------------------------------------------
    Saves bus parameters into persistent storage
*/
PMStatus BusinessLogic::saveConfigurationValues(Json::Value &root)
{
    PMStatus status;
    int errorCode = SUCCESS;

    PS_STATUS retPSI = PS_STATUS::PSI_SUCCESS;
    BL_STATUS retBlStatus = BL_STATUS::BL_SUCCESS;

    Json::StyledWriter stylewriter;
    std::string writeData;

    auto *pPSI = new PersistenceStorageInterface();
    if( nullptr == pPSI)
    {
        errorCode = FAILURE;
        status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL,
                PM_ERR_CL_FF_ADAPTOR, FF_ADAPTOR_SC_BL);

        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: PSI instance is NULL in BL saveConfigurationValues()");
    }

    // write the data
    if(errorCode == SUCCESS)
    {
        // write data (convert JSon::Value to std::string) in opened  file.
        writeData = stylewriter.write(root);

        // write data on file
        retPSI = pPSI->write(writeData);

        if(retPSI == PS_STATUS::PSI_WRITE_FAIL)
        {
            status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_SAVE_PARAM_PSI_WRITE_FAIL,
                    PM_ERR_CL_FF_ADAPTOR, FF_ADAPTOR_SC_BL);
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: write in BL saveConfigurationValues()");
        }
        delete pPSI;
    }
    return status;
}

/*  ---------------------------------------------------------------------------
    Prepared Bus parameters
*/
PMStatus BusinessLogic::prepareBusParams()
{
    std::string defaultBusParams;

    PMStatus status;
    Json::Value root;
    bool busParamAvail = false;
    bool appSettingsAvail = false;

    PS_STATUS retPSI = PS_STATUS::PSI_SUCCESS;
    auto *pPSI = new PersistenceStorageInterface();
    if(nullptr == pPSI)
    {
        status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL,
                        PM_ERR_CL_FF_ADAPTOR, FF_ADAPTOR_SC_BL);
        LOGF_ERROR(CPMAPP,
                  "BUS_PARAM_ERROR: PSI instance is NULL in BL initialize()");
    }

    if(!status.hasError())
    {
        retPSI = pPSI->checkFile();

        if(retPSI == PS_STATUS::PSI_FILE_PRESENT)
        {
            status = readConfigurationValues(root);

            if (status.hasError())
            {
                status.setError(FF_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL,
                                PM_ERR_CL_FF_ADAPTOR,FF_ADAPTOR_SC_BL);
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: init fail in  BL getBusParameters()");
            }
            else
            {
                //check if bus param and application setting available
                busParamAvail = m_pAdaptorInstance->isBusParamAvailable(root);
                appSettingsAvail = m_pAdaptorInstance->isAppSettingsAvailable(root);

                if (busParamAvail && appSettingsAvail)
                {
                    /* "Bus Parameter" and "App settings" are already available,
                       so don't need to write any data to the Busparam.json file
                       during initialization*/
                    delete pPSI;
                    return status;
                }
            }
        }

        if(!busParamAvail)
        {
            // get default bus params to store in persistance storage
            m_pAdaptorInstance->getDefaultBusParameters(root);
        }

        if(!appSettingsAvail)
        {
            // get default app setting to store in persistance storage
            m_pAdaptorInstance->getDefaultAppSettings(root);
        }

        // write data (convert JSon::Value to std::string) in opened  file.
        Json::StyledWriter styleWriter;
        defaultBusParams = styleWriter.write(root);

        retPSI = pPSI->write(defaultBusParams);

        if(retPSI == PS_STATUS::PSI_WRITE_FAIL)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: write fail on the file,"
                          " Business Logic initialize fail");
            status.setError(
                        FF_ADAPTOR_ERR_BUSPARAM_BL_SAVE_PARAM_PSI_WRITE_FAIL,
                        PM_ERR_CL_FF_ADAPTOR, FF_ADAPTOR_SC_BL);
        }
    }

    delete pPSI;

    return status;
}

/*  ---------------------------------------------------------------------------
    validates Json File for number of braces
*/
bool BusinessLogic::validateJson(std::string& jsonData)
{
    // jsoncpp library doesnot validates the correctness of braces
    // in strict mode, this logic is needed to detect jsonfile corruption
     int count = 0;
     for(char ch : jsonData)
     {
        if(ch == '{')
        {
            count++;
        }
        if(ch == '}')
        {
            count--;
        }
     }
     
     return count == 0;
}
