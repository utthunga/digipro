/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://aravindnagaraj@bitbucket.org/flukept/digipro.git
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    TestBusinessLogic class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////
#include "gtest/gtest.h"
#include "BusinessLogic.h"
#include "FFAdaptor.h"

/////////////////////////////////////////////////////////////////////////////
// Global data
/////////////////////////////////////////////////////////////////////////////
static FFAdaptor* g_pFFAdaptor = nullptr;
static BusinessLogic* g_pBusinessLogic = nullptr;
/////////////////////////////////////////////////////////////////////////////
// Following are the declarations of classes
/////////////////////////////////////////////////////////////////////////////
class TestBusinessLogic : public ::testing::Test
{

public:
    TestBusinessLogic();

    ~TestBusinessLogic();
};


//==============================================================================
/**
* Constructor for TestBusinessLogic class
*
* @return   none
*/
//==============================================================================
TestBusinessLogic::TestBusinessLogic()
{
}

//==============================================================================
/**
* Destructor for BusinessLogic class
*
* @return   none
*/
//==============================================================================
TestBusinessLogic::~TestBusinessLogic()
{
}

//==============================================================================
/**
*	testCreateScanThread
*/
//==============================================================================
TEST_F (TestBusinessLogic, testCreateScanThread)
{
    g_pFFAdaptor = new FFAdaptor();
    g_pBusinessLogic = new BusinessLogic(g_pFFAdaptor);

    /// create the scan thread
    g_pBusinessLogic->createScanThread();
    
    /// pass the test if the thread state is RUN
    EXPECT_EQ(THREAD_STATE_RUN, g_pBusinessLogic->getScanThreadState());
}

//==============================================================================
/**
*       testDestroyScanThread
*/
//==============================================================================
TEST_F (TestBusinessLogic, testDestroyScanThread)
{
    /// stop the scan thread
    g_pBusinessLogic->destroyScanThread();

    /// pass the test if thread state is stop
    EXPECT_EQ(THREAD_STATE_STOP, g_pBusinessLogic->getScanThreadState());
}

TEST_F (TestBusinessLogic, getBusParameters)
{
	PMStatus  retBP;
    Json::Value root;
    int errorCheck =0;

    EXPECT_NO_THROW(retBP = g_pBusinessLogic->readConfigurationValues(root));
    if(retBP.hasError())
    {
        	errorCheck = 1;
    }
    EXPECT_EQ(errorCheck,retBP.getErrorCode());
}

TEST_F (TestBusinessLogic, saveBusParameters)
{
    std::string retData;
    PMStatus retsaveBP;
    Json::Value root;

    int errorCheck = 0;

    root["FF_DLLPARAMS"]["this_link"]   = 0;
    root["FF_DLLPARAMS"]["slot_time"]   = 8;
    root["FF_DLLPARAMS"]["phlo"]        = 6;
    root["FF_DLLPARAMS"]["mrd"]         = 10;
    root["FF_DLLPARAMS"]["mipd"]        = 8;
    root["FF_DLLPARAMS"]["tsc"]         = 3;
    root["FF_DLLPARAMS"]["phis_skew"]   = 0;
    root["FF_DLLPARAMS"]["phpe"]        = 2;
    root["FF_DLLPARAMS"]["phge"]        = 0;
    root["FF_DLLPARAMS"]["fun"]         = 248;
    root["FF_DLLPARAMS"]["nun"]         = 0;


    EXPECT_NO_THROW(retsaveBP = g_pBusinessLogic->readConfigurationValues(root));
    if(retsaveBP.hasError())
    {
    	errorCheck = 1;
    }
    EXPECT_EQ(errorCheck,retsaveBP.getErrorCode());

    delete g_pBusinessLogic;
    delete g_pFFAdaptor;
}
