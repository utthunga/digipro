/******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
*******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    BusinessLogic class declaration
*/

/******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#pragma once

/* INCLUDE FILES **************************************************************/
#include <thread>
#include <mutex>
#include "IProtocolAdaptor.h"
#include "PersistenceStorageInterface.h"
#include <iostream>
#include <fstream>
#include <string>
#include "json/json.h"
#include "PMErrorCode.h"
#include "PMStatus.h"
#include "ThreadManager.h"
#include <condition_variable>

/* ===========================================================================*/
/// Device scan interval in milli second
#define DEVICE_SCAN_INTERVAL 500 

/* ===========================================================================*/
/* ===========================================================================*/
/// Enumeration defines the Business Logic Status
typedef enum class BusinessLogicStatus
{
    BL_SUCCESS = 1,
    BL_SAVE_PARAM_FAIL   =  -1,
    BL_GET_PARAM_FAIL    =  -2,
    BL_FAIL = -3
}BL_STATUS;

/* CLASS DECLARATIONS *********************************************************/
/** class description of BusinessLogic class.

    BusinessLogic Class consists of methods related to device scanning and
    Persistance storage interface for Bus Parameters.
*/
class BusinessLogic
{
public:
   
    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs BusinessLogic Class.
        
        Assigns the ProtocolAdaptor instance received from corresponding adaptors
        to BuinessLogic pointer
    */
    BusinessLogic(IProtocolAdaptor *pAdaptorInstance);

    /*------------------------------------------------------------------------*/
    /** Destroyes BusinessLogic class.
    */
    ~BusinessLogic() = default;

    /* Public Member Functions ===============================================*/
    
    /*------------------------------------------------------------------------*/
    /** Creates scan/re-scan timer thread     
    */
    void createScanThread();

    /*------------------------------------------------------------------------*/
    /** Destroyes scan/re-scan timer thread 
    */
    void destroyScanThread();

    /*------------------------------------------------------------------------*/
    /** Retrieves the scanning thread state 

        @return Current scanning thread state
    */
    THREAD_STATE getScanThreadState();

    /*------------------------------------------------------------------------*/
    /** Gets bus parameters and application settings from persistent storage
        
        @param root JSON Object holding the bus parameters

        @return Error status
    */    
    PMStatus readConfigurationValues(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /** Saves bus parameters and application settings into persistent storage
        
        @param root JSON Object holding the bus parameters

        @return Error status
    */    
    PMStatus saveConfigurationValues(Json::Value& root);

    /*------------------------------------------------------------------------*/
    /** Initializes business logic and creates Bus parameter file
        
        @return Error status
    */    
    PMStatus initialize();

    /*------------------------------------------------------------------------*/
    /** Prepares Bus parameters file
    
        @return Error status    
    */    
    PMStatus prepareBusParams();

private: 
    /* Private Member Functions ===============================================*/

    /*------------------------------------------------------------------------*/
    /** Implements device scanning thread
        
        @param arg Thread arguments consists of BusinessLogic instance

    */    
    void scanningThread(const void* arg);

    /*------------------------------------------------------------------------*/
    /** Scanns the bus and populates device information
    */    
    void doScanning();
    
    /*------------------------------------------------------------------------*/
    /** Sets the scanning thread state
        
        @param eThreadstate enumeration holds the current thread state
    */    
    void setScanThreadState(THREAD_STATE eThreadState);

    /*------------------------------------------------------------------------*/
    /** Validates Json file
        
        @param jsonData : json file information

        @return : returns true if file is valid
    */    
    bool validateJson(std::string& jsonData);

    /* Explicitly Disabled Methods ===========================================*/

    /// Assignment operator (disabled).
    BusinessLogic& operator = (const BusinessLogic& rhs) = delete;

    /// Copy constructor (disabled).
    BusinessLogic(const BusinessLogic& rhs) = delete;

private:
    /* Private Data Members ==================================================*/
    
    /// Holds protocol adaptor instance
    IProtocolAdaptor* m_pAdaptorInstance;

    /// Mutex lock for scanning thread
    std::mutex m_threadIdLock;

    /// Scan thread id
    uint64_t m_scanThreadID;

    /// Mutex for scan proceed signal
    std::mutex m_scanProceedMutex;
    
    /// Condition variable for scan proceed operation
    std::condition_variable m_scanProceed;
};
