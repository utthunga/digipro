
/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro 
    Authored By:       Adesh Soni
    Origin:            ProStar
*/

/** @file
    PersistanceStorageInterfaceStatus class declration
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

#pragma once

/* INCLUDE FILES **************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include "json/json.h"

using namespace std;

#define BUS_PARAM_FILE "busparam.json"
/* ===========================================================================*/
/// Enumeration defines the Persistance storage interface Status
typedef enum class PresistanceStorageInterfaceStatus
{
	PSI_SUCCESS = 1,
	PSI_FAIL = -1,
	PSI_READ_FAIL = -2,
	PSI_WRITE_FAIL = -3,
	PSI_FILE_PRESENT = 2,
	PSI_FILE_NOT_PRESENT = 3
}PS_STATUS;

/* CLASS DECLARATIONS *********************************************************/
/** class description of PersistanceStorageInterface class.

    PersistenceStorageInterface Class consists of methods related to reading
    and writing bus parameters form srotage
*/

class PersistenceStorageInterface
{
public:
    /* Construction and Destruction ==========================================*/

    /*------------------------------------------------------------------------*/
    /** Constructs PersistenceStorageInterface Class.
    */
	PersistenceStorageInterface() = default;
	
    /*------------------------------------------------------------------------*/
    /** Destroyes PersistenceStorageInterface class.
    */
	~PersistenceStorageInterface() = default;

    /* Explicitly Disabled Methods ===========================================*/

    /// Assignment operator (disabled).
    PersistenceStorageInterface& operator = 
                            (const PersistenceStorageInterface& rhs) = delete;

    /// Copy constructor (disabled).
    PersistenceStorageInterface(const PersistenceStorageInterface& rhs) =delete;


public:
    /* Public Member Functions ===============================================*/
	
    /*------------------------------------------------------------------------*/
    /** writes a file in persistence storage
        
        @return Persistance storage interface status
    */
	PS_STATUS write(std::string &data);
	
    /*------------------------------------------------------------------------*/
    /** Reads from persistence storage file 

        @return Persistence storage interface status
    */
	PS_STATUS read(std::string &readData);
	
    /*------------------------------------------------------------------------*/
    /** Removes from persistence storage file 

        @return Persistence storage interface status
    */
	PS_STATUS remove(std::string &file_name);

    /*------------------------------------------------------------------------*/
    /** Checks for file in persistence storage
        
        @return Error status
    */
	PS_STATUS checkFile();
	
    /*------------------------------------------------------------------------*/
    /** Provide bus param file 
     * */
    std::string getBusParamFile();
};
