/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Authored By:       Sadanand S Ugare
    Origin:            ProStar
*/

/** @file
    HART Adaptor class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/

#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>
#include "HARTAdaptor.h"
#include "json/json.h"
#include "JsonRpcUtil.h"
#include "CommonProtocolManager.h"
#include "SDC625Controller.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include <unistd.h>
#include "ddbItemLists.h"
#include "CPMUtil.h"
#include "error/CpmErrors.h"
#include <math.h>
#include <algorithm>
#include <vector>
#include "SubscriptionController.h"
#include "stacktrace.h"

using namespace std;

// Macro Definitions
#define THREE_BYTES 3
#define FIVE_BYTES 5
#define SIX_BYTES 6
#define SEVEN_BYTES 7
#define EIGHT_BYTES 8

// Data Range of Non Standard unsigned integer data types
#define UINT_SIZE_3_MAX 16777216
#define UINT_SIZE_5_MAX 1099511627776
#define UINT_SIZE_6_MAX 281474976710656
#define UINT_SIZE_7_MAX 72057594037927936

// Data Range of Non Standard Signed integer data types

#define INT_SIZE_3_MIN -8388608
#define INT_SIZE_3_MAX  8388607

#define INT_SIZE_5_MIN -549755813888
#define INT_SIZE_5_MAX  549755813887

#define INT_SIZE_6_MIN -140737488355328
#define INT_SIZE_6_MAX  140737488355327

#define INT_SIZE_7_MIN -36028797018963968
#define INT_SIZE_7_MAX  36028797018963967


/* ========================================================================== */

/* static member initialization ***********************************************/

// stores device information during connect and provides it to the application
// as and when needed.
DeviceInfo gHARTDeviceInfo{};

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    HARTAdaptor class instance initialization
*/
HARTAdaptor::HARTAdaptor()
    : m_eThreadState(METhreadState::STOP),
      m_pConnectionManager(nullptr),
      m_pParameterCache(nullptr),
      m_pBusinessLogic(nullptr),
      m_pSDC625Controller(nullptr),
      m_unScanSeqID(0),
      m_internalSubscriptionRate(0),
      m_subscriptionCacheTable{}
{
    initializePublisherList();

    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    // Set the Protocol Type
    m_Protocol = CPMPROTOCOL::CPMPROTOCOL_HART;
}

/*  ----------------------------------------------------------------------------
    Destroys the memory if allocated for member variables
*/
HARTAdaptor::~HARTAdaptor()
{
    if (nullptr != m_pSDC625Controller)
    {
        delete m_pSDC625Controller;
        m_pSDC625Controller = nullptr;
    }

    if (nullptr != m_pConnectionManager)
    {
        delete m_pConnectionManager;
        m_pConnectionManager = nullptr;
    }

    if (nullptr != m_pParameterCache)
    {
        delete m_pParameterCache;
        m_pParameterCache = nullptr;
    }

    if (nullptr != m_pBusinessLogic)
    {
        delete m_pBusinessLogic;
        m_pBusinessLogic = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Initializes Parameter Cache, Connection Manager and DDS Controller
*/
Json::Value HARTAdaptor::initialise()
{
    /* Creates all the required instances of SDC 625 Controller, Parameter
       Cache and Connection manager and injects them to the required objects
    */
    PMStatus status;

    Json::Value jsonResult;

    // Instantiate the Connection Manager, Parameter Cache & SDC625 Controller
    m_pConnectionManager = new HARTConnectionManager;
    m_pParameterCache = new HARTParameterCache;
    m_pSDC625Controller = new SDC625Controller;

    // Check for improper initialisation
    if ((nullptr != m_pConnectionManager) &&
       (nullptr != m_pParameterCache) &&
       (nullptr != m_pSDC625Controller) &&
       (nullptr != m_pBusinessLogic))
    {

       m_pSubscriptionController = m_pParameterCache->getSubscriptionController();
	   
       // Retrieves HART applications settings
       IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

       if (nullptr != pAppConfigParser)
       {
           bool isTraceFile = false;
           std::string traceFileName = "";
           std::string traceFileDir = "";

           m_hartDDBinaryPath = pAppConfigParser->getDeviceDescriptionPath();
           m_hartDeviceFile = pAppConfigParser->getDevicePort();
           m_hartModem = pAppConfigParser->getModemName();
           m_internalSubscriptionRate = pAppConfigParser->getSubscriptionRate();
           traceFileName = pAppConfigParser->getTraceFileName();
           traceFileDir = pAppConfigParser->getTraceLogPath();
      
           if (!traceFileName.empty())
           {
               isTraceFile = true;
           }

           if (!CPMUtil::isDirExists(traceFileDir))
           {
               CPMUtil::createDirectory(traceFileDir);
           }

           std::string traceFilePath = traceFileDir + "/" + traceFileName;
           if (!createLogFile(HART_PROTOCOL, traceFilePath.c_str(), isTraceFile))
           {    
               LOGF_ERROR(CPMAPP, "Failed to create trace file; redirecting stack traces to CPMLOGS");
           }
       }

       // Initialize SDC 625 Controller
       status = m_pSDC625Controller->initialize(
                   m_pConnectionManager, m_pParameterCache, this);

       if (!status.hasError())
       {
           // Initialize Connection Manager
           status = m_pConnectionManager->initialize(
                       m_pSDC625Controller, m_pParameterCache);
           if (!status.hasError())
           {
               // Initialize Parameter Cache
               status = m_pParameterCache->initialize(
                           m_pSDC625Controller, m_pConnectionManager);
               if (!status.hasError())
               {
                   //Initialize the business logic
                   status = m_pBusinessLogic->initialize();
                   if (!status.hasError())
                   {
                       // Get the bus parameters from PS to intialise stack
                       status = getStackConfiguration();

                       if (!status.hasError())
                       {
                           //validates dd path
                           const char* ddPath = m_hartDDBinaryPath.length()
                                   > 0 ? m_hartDDBinaryPath.c_str() : nullptr;

                           // Initialize the stack
                           status = m_pConnectionManager->initStack
                                   (&m_busParams, &m_appSetting,ddPath,
                                    m_hartDeviceFile.c_str(),
                                    m_hartModem.c_str());
                       }
                   }
               }
           }
       }
    }

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
         // Set the protocol state as initialized
        CPMStateMachine::getInstance()->setCpmState(CPMSTATE_PROTOCOL_INITIALIZED);
        // Set language code state
        CPMStateMachine::getInstance()->setCpmState(CPMSTATE_LANGUAGE_CODE_SET);
        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }

    // Add Mapped Methods
    mapItemMethods();

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Add publisher to the publisher list table
*/
void HARTAdaptor::initializePublisherList()
{
    // Insert publisher in the publisher list
    // Publisher for sending scan information
    m_publisherList.push_back(std::string(SCAN_PUBLISHER));
    // Publisher for sending item information
    m_publisherList.push_back(std::string(ITEM_PUBLISHER));
}

/*  ----------------------------------------------------------------------------
    Gets the DD header info from binary file
*/
Json::Value HARTAdaptor::getDDHeaderInfo()
{
    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Shuts down the stack
*/
Json::Value HARTAdaptor::shutDown()
{
    PMStatus status;
    Json::Value jsonResult;

    if (CPMSTATE_SCANNING_IN_PROGRESS == (CPMSTATE_SCANNING_IN_PROGRESS & CPMStateMachine::getInstance()->getCpmState()))
    {
        // Destroy scan thread only if the existing thread is in running state
        if (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
        {
            // sets stop scan flag and stops device scanning process
            m_pConnectionManager->stopScan();

            while (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
            {
                ::sleep(1);
            }

            // destroy scan thread
            m_pBusinessLogic->destroyScanThread();

            // Clears CPM state
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_SCANNING_IN_PROGRESS);
        }
    }

    if (CPMSTATE_DEVICE_CONNECTED == (CPMSTATE_DEVICE_CONNECTED & CPMStateMachine::getInstance()->getCpmState()))
    {
        stopMethodExec();
        status = disconnectDevice();
    }

    if (nullptr != m_pConnectionManager)
    {
        m_pParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_NONE);

        if (!status.hasError())
        {
            status = m_pConnectionManager->shutDownStack();
            
            //deletes device
            if (nullptr != m_pSDC625Controller)
            {
                m_pSDC625Controller->deleteDevice();
            }
        }

        if (!status.hasError())
        {
            closeLogFile();
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_PROTOCOL_INITIALIZED);

            // Clear language code state
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_LANGUAGE_CODE_SET);

            jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
        }
        else
        {
            Error::Entry error = Error::cpm.PROTOCOL_SHUTDOWN_FAILED;
            jsonResult =  JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
        jsonResult =  JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Retrives the active protocol
*/
CPMPROTOCOL HARTAdaptor::getActiveProtocol()
{
    return m_Protocol;
}

/*  ----------------------------------------------------------------------------
    Updates the DD
*/
Json::Value HARTAdaptor::updateDD(const Json::Value& data)
{
    /* Typecasted with void to supress the unused parameter
       warning, must remove when implementation takes place
    */
    (void) data;

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Read the specified value
*/
Json::Value HARTAdaptor::getItem(const Json::Value& data)
{
    // Invoke the appropriate method for processing the request
    return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::GET_ITEM));
}

/*  ----------------------------------------------------------------------------
    Executes respective mapped method through function pointer based on item type
*/
Json::Value HARTAdaptor::executeMappedMethod
(
    const Json::Value& itemRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value response;

    // Fetch the corresponding function pointer from the map
    std::string itemType = itemRequest[JSON_ITEM_TYPE].asString();

    auto methodItr = m_processMethodMap.find(itemType);

    if (methodItr != m_processMethodMap.end())
    {
        // Method available inside map; execute it
        response = (*this.*(methodItr->second))(itemRequest, ITEMQUERY);
    }
    return response;
}

/*  ----------------------------------------------------------------------------
    Writes the value
*/
Json::Value HARTAdaptor::setItem(const Json::Value& data)
{
    // Invoke the appropriate method for processing the request
    return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::SET_ITEM));
}

/*  ----------------------------------------------------------------------------
    Validates Pre Post Edit Action input JSON
*/
 PMStatus HARTAdaptor::validateEditAction(const Json::Value& data,
                                        paramItems& paramInfo)
{
     PMStatus status;
     std::string reqType{""};

    if (data.isMember(JSON_ITEM_ID) &&
        data[JSON_ITEM_ID].isIntegral() &&
        (data[JSON_ITEM_ID].asLargestInt() > 0))
    {
        paramInfo.paramItemID = data[JSON_ITEM_ID].asLargestUInt();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);     
    }

    if (!status.hasError() &&
        data.isMember(JSON_ITEM_TYPE) &&
        data[JSON_ITEM_TYPE].isString())
    {
        reqType = data[JSON_ITEM_TYPE].asString();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid ItemType");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
    }

    if (!status.hasError() &&
        reqType.compare(JSON_MENU_ITEM_TYPE) != 0)
    {
        LOGF_ERROR(CPMAPP, "Invalid ItemType");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Executes Pre Edit Action for Edit Display
*/
Json::Value HARTAdaptor::execPreEditAction(const Json::Value& data)
{
    Json::Value editDispResponse;
    PMStatus status;
    paramItems paramInfo{};

    status = validateEditAction(data, paramInfo);

    if (!status.hasError())
    {
        // set the item id to execute pre/post edit action
        SDC625Controller::s_hartVarActionData.itemID = paramInfo.paramItemID;
        SDC625Controller::s_hartVarActionData.actionItemType = EditActionItemTypes::EDIT_DISPLAY_ACTION;
        SDC625Controller::s_hartVarActionData.actionType = EditActionTypes::PRE_EDIT_ACTION;

        itemType_t itemType;
        status = m_pSDC625Controller->getItemType(paramInfo.paramItemID, itemType);
        bool editActionExists = false;

        if (!status.hasError())
        {
            // Edit Action supported only for Edit Display artifact
            if (iT_EditDisplay == itemType)
            {
                // check if edit action exists
                editActionExists = m_pSDC625Controller->editDispActionExists(paramInfo.paramItemID, eddispAttrPreEditAct);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
            // RequestType is unavailable; return error
            Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return editDispResponse;
        }

        // check if pre edit action exists
        if (editActionExists)
        {
            // command response publisher with message.
            Error::Entry error = Error::cpm.PRE_EDIT_ACTION_IS_IN_PROGRESS;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

            // starts the pre edit action thread
            status = processExecuteEditMethod(paramInfo.paramItemID);

            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
                // RequestType is unavailable; return error
                Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOGF_INFO(CPMAPP,"Edit Display has no pre Edit actions with Item ID : " << paramInfo.paramItemID);
            Error::Entry error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid JSON input for Edit Actions");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return editDispResponse;
}

/*  ----------------------------------------------------------------------------
    Executes Post Edit Action for Edit Display
*/
Json::Value HARTAdaptor::execPostEditAction(const Json::Value& data)
{
    Json::Value editDispResponse;
    PMStatus status;
    paramItems paramInfo{};

    status = validateEditAction(data, paramInfo);
    if (!status.hasError())
    {
        // set the item id to execute pre/post edit action
        SDC625Controller::s_hartVarActionData.itemID = paramInfo.paramItemID;
        SDC625Controller::s_hartVarActionData.actionItemType = EditActionItemTypes::EDIT_DISPLAY_ACTION;
        SDC625Controller::s_hartVarActionData.actionType = EditActionTypes::POST_EDIT_ACTION;

        itemType_t itemType;
        status = m_pSDC625Controller->getItemType(paramInfo.paramItemID, itemType);
        bool editActionExists = false;

        if (!status.hasError())
        {
            // Edit Action supported only for Edit Display artifact
            if (iT_EditDisplay == itemType)
            {
                // check if edit action exists
                editActionExists = m_pSDC625Controller->editDispActionExists(
                            paramInfo.paramItemID, eddispAttrPostEditAct);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
            // RequestType is unavailable; return error
            Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return editDispResponse;
        }

        // check if post edit action exists
        if (editActionExists)
        {
            // command response publisher with message.
            Error::Entry error = Error::cpm.POST_EDIT_ACTION_IS_IN_PROGRESS;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

            // starts the post edit action thread
            status = processExecuteEditMethod(paramInfo.paramItemID);

            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
                // RequestType is unavailable; return error
                Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOGF_INFO(CPMAPP,"Edit Display has no post Edit actions with Item ID : " << paramInfo.paramItemID);
            Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
            editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid JSON input for Edit Actions");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        editDispResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return editDispResponse;
}

/*  ----------------------------------------------------------------------------
    Process requested subscription action
*/
Json::Value HARTAdaptor::processSubscription(const Json::Value& subReq)
{
    Json::Value subResp;
    PMStatus status;

    if (subReq.isMember(JSON_ITEM_SUB_ACTION))
    {
        std::string itemAction = subReq[JSON_ITEM_SUB_ACTION].asString();

        if (0 == itemAction.compare(JSON_SUB_CREATE))
        {
            // creates subscription
            subResp = createSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_START))
        {
            // Starts subscription
            subResp = startSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_STOP))
        {
            // Stops subscription
            subResp = stopSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_DELETE))
        {
            // deletes subscription
            subResp = deleteSubscription(subReq);
        }
        else
        {
            // Invalid subscription action type
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        // Invalid request type; return immediately with error!!
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(subResp);
}

/*  ----------------------------------------------------------------------------
    Creates the subscription for specified item
*/
Json::Value HARTAdaptor::createSubscription(const Json::Value& createSubReq)
{
    Json::Value response;
    PMStatus status;
    Error::Entry error;
    unsigned int itemRefreshRate = 0;
    ParamInfo monitoredItems{};
    uint subscriptionID = 0;

    // verifies json keys are present or not
    if (createSubReq.isMember(JSON_ITEM_SUB_REFRESH_RATE) &&
        createSubReq[JSON_ITEM_SUB_REFRESH_RATE].isIntegral() &&
        (createSubReq[JSON_ITEM_SUB_REFRESH_RATE].asLargestInt() > 0) &&
        createSubReq.isMember(JSON_ITEM_LIST))
    {
        itemRefreshRate = createSubReq[JSON_ITEM_SUB_REFRESH_RATE].asUInt();

        for (auto &item : createSubReq[JSON_ITEM_LIST])
        {
            auto varType = new VariableType();
            varType->isMemAllocated = true;
            varType->itemType = ParamItemType::PARAM_VARIABLE;

            if (item.isMember(JSON_ITEM_ID) && item[JSON_ITEM_ID].isIntegral() &&
                (item[JSON_ITEM_ID].asLargestInt() > 0))
            {
                varType->itemID = item[JSON_ITEM_ID].asLargestUInt();
            }
            else
            {
                //Json attribute missing
                LOGF_ERROR(CPMAPP, "Item ID is not available or invalid");
                status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
                error = Error::cpm.INVALID_JSON_REQUEST;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }

            if (item.isMember(JSON_ITEM_INFO) &&
                item[JSON_ITEM_INFO].isMember(JSON_VAR_BIT_INDEX) &&
                item[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].isIntegral() &&
                (item[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].asLargestInt() >= 0))
            {
                varType->bitIndex = item[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].asUInt();
            }
            else
            {
                //Json attribute missing
                LOGF_ERROR(CPMAPP, "Item Info Not available or Invalid Bit Index");
                status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
                error = Error::cpm.INVALID_JSON_REQUEST;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }

            if(!status.hasError())
            {
                varType->value.stdValue = {INIT_STD_VALUE};
                monitoredItems.paramList.push_back(*varType);
                monitoredItems.requestInfo.maskType = ITEM_ATTR_SUBSCRIPTION_MASK;
            }
        }

        DataChangedCallback callback = [this](uint subscriptionID,
                ParamInfo* monitorItem)
        {
            Json::Value subJsonData;
            subJsonData[JSON_ITEM_SUB_ID] = subscriptionID;
            subJsonData[JSON_ITEM_TYPE] = JSON_VARIABLE_ITEM_TYPE;

            if (nullptr != monitorItem)
            {
                for (auto& paramItemRef : monitorItem->paramList)
                {
                    auto paramItem = &paramItemRef.get();
                    auto varParam = dynamic_cast<VariableType*>(paramItem);

                    Json::Value tempJsonData;

                    tempJsonData[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(varParam->itemID);

                    wstring unit;
                    varParam->unit = {};

                    // Gives unit string
                    m_pSDC625Controller->getVariablePtr(varParam->itemID)->getUnitString(unit);

                    wstring wsOptions(unit);

                    // Converts Unicode to UTF8
                    string varUnit = TStr2AStr(wsOptions);
                    if (!varUnit.empty())
                    {
                        m_pParameterCache->assignStringValue(varParam->unit, varUnit);
                    }

                    fillVariableJsonInfo(varParam, tempJsonData, VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK);
                    subJsonData[JSON_ITEM_LIST].append(tempJsonData);

                    // Deallocates memory allocated for unit string
                    if (varParam->unit.flag == FREE_STRING)
                    {
                        delete[] varParam->unit.str;
                        varParam->unit.str = nullptr;
                        varParam->unit.len = 0;
                        varParam->unit.flag = DONT_FREE_STRING;
                    }
                }
            }

            // notify CPM with subscription response
            std::string subPubStr(ITEM_PUBLISHER);
            CommonProtocolManager::getInstance()->notifyMessage(subPubStr, subJsonData);
        };

        if (monitoredItems.paramList.empty())
        {
            LOGF_ERROR(CPMAPP, "create subscription failed or invalid subscriptionID");
            status.setError(HART_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED,
                            PM_ERR_CL_HART_ADAPTOR);
            error = Error::cpm.CREATE_SUBSCRIPTION_FAILED;
        }
        else
        {
            status = m_pParameterCache->getParameter(&monitoredItems);

            if (!status.hasError())
            {
                monitoredItems.requestInfo.maskType = ITEM_ATTR_SUBSCRIPTION_MASK;
                // creates subscription and generates subscription id.
                status = m_pParameterCache->createSubscription(monitoredItems,
                                                               itemRefreshRate,
                                                               false, //read from cache
                                                               callback,
                                                               subscriptionID,
                                                               SUBSCRIPTION_TYPE_EXTERNAL);
            }

            if (!status.hasError() && gHARTDeviceInfo.hartRevision >= HART_REV_6)
            {
                // Add the static parameters to internal subscription table
                status = addSubscriptionCacheItems(subscriptionID);
            }

            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "create subscription failed or invalid subscriptionID");
                status.setError(HART_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED,
                                PM_ERR_CL_HART_ADAPTOR);
                error = Error::cpm.CREATE_SUBSCRIPTION_FAILED;
            }
            else
            {
                response[JSON_ITEM_SUB_ID] = subscriptionID;
            }
        }
    }
    else
    {
        // Invalid refresh rate item, return error immediately
        LOGF_ERROR(CPMAPP,"Invalid Json req, Item Refresh Rate or ItemList is unavailable");
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    m_pParameterCache->cleanParamInfo(&monitoredItems);

    return response;
}

/*  ----------------------------------------------------------------------------
       Starts the subscription for specified item
   */
Json::Value HARTAdaptor::startSubscription(const Json::Value& startSubReq)
{
    Json::Value startSubResp("ok");
    PMStatus status;
    Error::Entry error;

    if (startSubReq.isMember(JSON_ITEM_SUB_ID) &&
        startSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (startSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionId = startSubReq[JSON_ITEM_SUB_ID].asUInt();
        // starts subscription
        status = m_pParameterCache->startSubscription(subscriptionId);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Start subscription failed with error code = "
                       << status.getErrorCode());
            status.setError(HART_ADAPTOR_ERR_START_SUBSCRIPTION_FAILED, PM_ERR_CL_HART_ADAPTOR);
            error = Error::cpm.START_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return startSubResp;
}

/*  ----------------------------------------------------------------------------
       Stops the subscription for specified item
   */
Json::Value HARTAdaptor::stopSubscription(const Json::Value& stopSubReq)
{
    Json::Value stopSubResp("ok");
    PMStatus status;
    Error::Entry error;

    if (stopSubReq.isMember(JSON_ITEM_SUB_ID) &&
        stopSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (stopSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionId = stopSubReq[JSON_ITEM_SUB_ID].asUInt();
        // Unsubscribe and stop the subscription thread
        status =  m_pParameterCache->unsubscribe(subscriptionId);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Stop subscription failed with error code = "
                       << status.getErrorCode());
            status.setError(HART_ADAPTOR_ERR_STOP_SUBSCRIPTION_FAILED, PM_ERR_CL_HART_ADAPTOR);
            error = Error::cpm.STOP_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return stopSubResp;
}

/*  ----------------------------------------------------------------------------
       Deletes the subscription for specified item
*/
Json::Value HARTAdaptor::deleteSubscription(const Json::Value& delSubReq)
{
    Json::Value delSubResp("ok");
    PMStatus status;
    Error::Entry error;

    if (delSubReq.isMember(JSON_ITEM_SUB_ID) &&
        delSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (delSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionID = delSubReq[JSON_ITEM_SUB_ID].asUInt();

        if (gHARTDeviceInfo.hartRevision >= HART_REV_6)
        {
             status = removeSubscriptionCacheItems(subscriptionID);
        }

        if (0 == subscriptionID)
        {
            // Deletes all external subscriptions
            status = m_pParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_EXTERNAL);
        }
        else
        {
            // Stop subscription and delete the subscription from the list
            status = m_pParameterCache->deleteSubscription(subscriptionID);
        }

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Delete subscription failed with error code = "
                       << status.getErrorCode());
            status.setError(HART_ADAPTOR_ERR_DELETE_SUBSCRIPTION_FAILED, PM_ERR_CL_HART_ADAPTOR);
            error = Error::cpm.DELETE_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return delSubResp;
}


/*  ----------------------------------------------------------------------------
    Creates internal subscription to update the cache table when the
    configuration counter increases.

    The configuration_counter parameter available only for HART 6 & higher
    revision
*/
PMStatus HARTAdaptor::createsInternalSubscription()
{
    ParamInfo monitoredItems{};
    PMStatus status;

    // Add internal subscription items
    // The below memory is deallocated in disconnect() API
    auto varType = new VariableType();
    varType->itemID = CONFIG_CHANGE_COUNTER_SYM_ID;
    varType->isMemAllocated = true;
    monitoredItems.paramList.push_back(*varType);
    monitoredItems.requestInfo.maskType = ITEM_ATTR_SUBSCRIPTION_MASK;

    status = m_pParameterCache->getParameter(&monitoredItems);

    if (!status.hasError())
    {
        DataChangedCallback callback = [this](uint subscriptionID,
                ParamInfo* monitorItem)
        {
            (void)subscriptionID;

            if (nullptr != monitorItem)
            {
                PMStatus status;

                for (auto& paramItemRef : monitorItem->paramList)
                {
                    auto paramItem = &paramItemRef.get();
                    auto varParam = dynamic_cast<VariableType*>(paramItem);

                    /* Read static parameters to update the cache table
                            when configuration changed counter increases */

                    if (varParam->itemID == CONFIG_CHANGE_COUNTER_SYM_ID &&
                            varParam->valueChanged )
                    {
                        ParamInfo paramInfo{};

                        for (auto item : m_subscriptionCacheTable.cacheItems)
                        {
                            /* add it to the read list only if refCounter is greater
                               than or equal to 1 */
                            if (item.refCounter >= 1)
                            {
                                paramInfo.paramList.push_back(*item.varType);
                            }
                        }

                        status = m_pParameterCache->updateCacheTable(
                                    paramInfo, false);
                    }

                }
            }
        };

        uint subscriptionId = 0;
        // creates subscription and generates subscription id.
        status = m_pParameterCache->createSubscription(monitoredItems,
                                                       m_internalSubscriptionRate,
                                                       true, //read from device
                                                       callback,
                                                       subscriptionId,
                                                       SUBSCRIPTION_TYPE_INTERNAL);
        if (!status.hasError())
        {
            status = m_pParameterCache->startSubscription(subscriptionId);
        }

        /* clears the list of parameter items, which are used to create the
           subscription as well releases memory allocated to string types
        */
        m_pParameterCache->cleanParamInfo(&monitoredItems);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Adds the Static parameters for the given subscription ID to the
    internal subscription table.
*/
PMStatus HARTAdaptor::addSubscriptionCacheItems(UINT subscriptionID)
{
    PMStatus status;
    ParamInfo paramInfo{};

    if (nullptr == m_pSubscriptionController || nullptr == m_pParameterCache )
    {
        LOGF_ERROR(CPMAPP,"Subscription Controller or HART Parameter cache"
                       "instance is null");
        status.setError(HART_ADAPTOR_ERR_NULL_INSTANCE,
                        PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    // get the monitor items for the given subscription ID
    paramInfo = m_pSubscriptionController->getMonitorItems(subscriptionID);

    for (auto item :  paramInfo.paramList)
    {
        // gets the ItemInfo Type
        ItemInfoType* subItemInfo = &item.get();
        VariableType* variable = dynamic_cast<VariableType*>(subItemInfo);
        bool isItemExists = false;

        if (nullptr == variable)
            continue;

        // skip if it is dynamic parameters
        if (variable->misc.class_attr & maskDynamic)
        {
            continue;
        }

        unsigned long itemID = variable->itemID;

        if(m_subscriptionCacheTable.cacheItems.size() > 0)
        {
            auto it = std::find_if(begin(m_subscriptionCacheTable.cacheItems),
                                   end(m_subscriptionCacheTable.cacheItems),
                                   [&itemID](const subscriptionCacheItem& cacheItem)
            {
                return cacheItem.itemID == itemID;
            });

            isItemExists = (it != std::end(m_subscriptionCacheTable.cacheItems));

            // If the item already exists, just increment the reference counter
            if (isItemExists)
            {
                subscriptionCacheItem &item =  *it;
                item.refCounter++;
            }
        }

        // if item not exists, add it to the internal subscription table
        if (false == isItemExists)
        {
            subscriptionCacheItem cacheItem{};
            cacheItem.varType = variable->clone();
            cacheItem.itemID = variable->itemID;
            cacheItem.refCounter = 1;
            m_subscriptionCacheTable.cacheItems.push_back(cacheItem);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Removes the Static parameters reference for the given subscription ID
    from the internal subscription table
*/
PMStatus HARTAdaptor::removeSubscriptionCacheItems(UINT subscriptionID)
{
    PMStatus status;
    ParamInfo paramInfo{};

    if (nullptr == m_pSubscriptionController || nullptr == m_pParameterCache )
    {
        LOGF_ERROR(CPMAPP,"Subscription Controller or HART Parameter cache"
                       "instance is null");
        status.setError(HART_ADAPTOR_ERR_NULL_INSTANCE,
                        PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    std::vector<Subscription*> subscription = m_pSubscriptionController->getAllSubscription();
    std::vector<Subscription*>::iterator it;

    for (it = subscription.begin();it != subscription.end(); ++it)
    {
        /* if subscription ID is 0, remove parameters reference for all the subscriptions
           otherwise only specified subscription ID.
        */

        if (subscriptionID != 0)
        {
            if(subscriptionID == (*it)->getSubscriptionID())
            {
                paramInfo = (*it)->getMonitorItems();
            }
            else
            {
                continue;
            }
        }
        else
        {
            paramInfo = (*it)->getMonitorItems();
        }

        // get the monitor items for the given subscription ID
        paramInfo = m_pSubscriptionController->getMonitorItems((*it)->getSubscriptionID());

        for (auto item : paramInfo.paramList)
        {
            // gets the ItemInfo Type
            ItemInfoType* subItemInfo = &item.get();
            VariableType* variable = dynamic_cast<VariableType*>(subItemInfo);
            bool isItemExists = false;

            if (nullptr == variable)
                continue;

            // skip if it is dynamic parameters
            if (variable->misc.class_attr & maskDynamic)
            {
                continue;
            }

            unsigned long itemID = variable->itemID;

            if(m_subscriptionCacheTable.cacheItems.size() > 0)
            {
                auto it = std::find_if(begin(m_subscriptionCacheTable.cacheItems),
                                       end(m_subscriptionCacheTable.cacheItems),
                                       [&itemID](const subscriptionCacheItem& cacheItem)
                {
                    return cacheItem.itemID == itemID;
                });

                isItemExists = (it != std::end(m_subscriptionCacheTable.cacheItems));

                // If the exists, just decrement the reference counter
                if (isItemExists)
                {
                    subscriptionCacheItem &cacheItem =  *it;
                    cacheItem.refCounter--;
                }
            }
        }

        if (subscriptionID != 0)
        {
            break;
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Starts the device scanning process
*/
Json::Value HARTAdaptor::startScan(const Json::Value& jsonData)
{
    PMStatus status;
    Json::Value jsonResult;

    /* Typecasted with void to supress the unused parameter
       warning, must remove when implementation takes place
    */
    (void) jsonData;

    // Create scan thread only if no scan thread exists
    if (THREAD_STATE_INVALID == m_pBusinessLogic->getScanThreadState())
    {
        // Reset the scan sequence id
        m_unScanSeqID = 0;
        // Create device scan thread
        m_pBusinessLogic->createScanThread();

        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        Error::Entry error = Error::cpm.START_SCAN_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Stops the device scanning process
*/
Json::Value HARTAdaptor::stopScan()
{
    PMStatus status;
    Json::Value jsonResult;

    // Destroy scan thread only if the existing thread is in running state
    if (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
    {
        // sets stop scan flag and stops device scanning process
        m_pConnectionManager->stopScan();

        while (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
        {
            ::sleep(1);
        }

        // destroy scan thread
        m_pBusinessLogic->destroyScanThread();

        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        Error::Entry error = Error::cpm.STOP_SCAN_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Send scan request to find the available devices in the network and fills
    the scan table information
*/
PMStatus HARTAdaptor::scanNetwork()
{
    PMStatus scanStatus;

    m_unScanSeqID++;

    /* scans available devices in the network and
       prepare device data to notify to upper layer
    */
    scanStatus = m_pConnectionManager->scanDevices([this](bool deviceFound,
                                                   Indentity_t identity)
    {
        Json::Value jsondata;

        if (deviceFound)
        {
            std::stringstream stream;

            stream << identity.dwDeviceId;
            std::string strDeviceId = stream.str();

            // Prepare the corresponding JSON information and call notify
            jsondata[JSON_DEV_INFO][JSON_DEV_TAG] = identity.sTag;
            jsondata[JSON_DEV_INFO][JSON_DEV_ADDR] = identity.PollAddr;
            jsondata[JSON_DEV_INFO][JSON_DEV_ID_INFO] = strDeviceId;
        }

        jsondata[JSON_DEV_SCAN_SEQ_ID] = m_unScanSeqID;

        // notify CPM with device information
        std::string scanPubStr(SCAN_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(scanPubStr, jsondata);
    });

   return scanStatus;
}

/*  ----------------------------------------------------------------------------
    Maps the processItem method function pointers to corresponding method key
*/
void HARTAdaptor::mapItemMethods()
{
    //  Map the function pointer to respective ItemType String
    m_processMethodMap[JSON_VARIABLE_ITEM_TYPE] = &HARTAdaptor::processVariable;
    m_processMethodMap[JSON_IMAGE_ITEM_TYPE]    = &HARTAdaptor::processImage;
    m_processMethodMap[JSON_MENU_ITEM_TYPE]     = &HARTAdaptor::processMenu;
    m_processMethodMap[JSON_METHOD_ITEM_TYPE]   = &HARTAdaptor::processMethod;
}

/*  ----------------------------------------------------------------------------
    Connects the specified device
*/
Json::Value HARTAdaptor::connect(const Json::Value& jsonData)
{
    DeviceInfo deviceInfo;
    Json::Value output;
    PMStatus status;

    if (jsonData.isMember(JSON_DEV_ADDR) &&
        jsonData[JSON_DEV_ADDR].isIntegral() &&
        (jsonData[JSON_DEV_ADDR].asLargestInt() >= 0))
    {
        deviceInfo.node_address = jsonData[JSON_DEV_ADDR].asUInt();
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Invalid Device Address: " << jsonData[JSON_DEV_ADDR].asUInt());
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Initialize the Device Manager
    status = m_pSDC625Controller->initializeDeviceManager(m_hartDDBinaryPath);
    if (status.hasError())
    {
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        //Establish the logical device connection
        status = m_pConnectionManager->connectDevice(&deviceInfo);
        if (status.hasError())
        {
            Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
        else
        {
            // Store the device info locally - To be removed later
            // once connection manager provides the same information
            memcpy(&gHARTDeviceInfo, &deviceInfo, sizeof(DeviceInfo));

            /* creates internal subscription to update the cache table for
               static parameters for HART Revision 6 and above*/
            if (gHARTDeviceInfo.hartRevision >= HART_REV_6)
            {
                /* No need of checking the status as this is expected to be fail
                   in backward compatibility DD is loaded */
                createsInternalSubscription();
            }

	        if (!status.hasError())
	        {
	            IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

	            if (nullptr != pAppConfigParser)
	            {
	                std::string imagePath = pAppConfigParser->getImagePath();
            
	                // Creates a specific directory
	                // If the directory already exists clear the content of the directory
	                if (false == CPMUtil::createDirectory(imagePath))
	                    CPMUtil::clearDirectory(imagePath);
	            }

	            // get the root menu information
	            status = getDDMenu(ROOT_MENU, output);
	        }

	        if (status.hasError())
	        {
	            LOGF_ERROR(CPMAPP,"Create internal subscription failed!!");
	            Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
	            output =  JsonRpcUtil::setErrorObject(error.code, error.message);
	        }
	        else
	        {
	            output = JsonRpcUtil::getResponseObject(output);

	            // Set state as connected
	            CPMStateMachine::getInstance()->setCpmState(CPMSTATE_DEVICE_CONNECTED);
	        }
		}
    }

    // Uninitialize the device manager in connection failure case
    if (status.hasError())
    {
        m_pSDC625Controller->uninitializeDeviceManager();
    }

    return output;
}

/*  ----------------------------------------------------------------------------
    gives information about menu items.
*/
PMStatus HARTAdaptor::getDDMenu(itemID_t itemID, Json::Value &output)
{
    PMStatus status;
    hCmenu* menuInfo = nullptr;
    ItemBasicInfo basicMenuInfo;
    menuItemList_t locMenuList;
    string label{};
    string help{};

    // gives menu information
    status = m_pSDC625Controller->getMenuItem(itemID, &menuInfo);
    if (status.hasError() || nullptr == menuInfo)
    {
        status.setError(HART_ADAPTOR_GET_DD_MENU_FAILED, PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    menuInfo->Label(label);
    menuInfo->Help(help);
    basicMenuInfo.itemID    = menuInfo->getID();
    basicMenuInfo.itemLabel = label;
    basicMenuInfo.itemHelp = help;
    basicMenuInfo.itemType  = JSON_MENU_ITEM_TYPE;

    menuInfo->aquire(locMenuList);

    fillMenuItemsJson(locMenuList, output);

    // Fill the attributes for the Menu
    fillItemBasicJsonInfo(basicMenuInfo, output);

    //clears the elements present in the list
    locMenuList.clear();

    return status;
}

/*------------------------------------------------------------------------*/
/** Fills menu items Basic information into Json response

    @param locMenuList contains the list of menu items information

    @param jsonResp Json response to be filled

*/
PMStatus HARTAdaptor::fillMenuItemsJson(menuItemList_t& locMenuList,
                                       Json::Value &output)
{
    PMStatus status;
    VarBasicInfo subMenuInfo;
    list<VarBasicInfo> itemList;
    Json::Value itemJsonInfo;
    string label{};
    string help{};

    // For every item in the menu...
    for (auto menuItemList : locMenuList)
    {
        hCitemBase* hcItem = nullptr;

        // Get a pointer to the item
        auto menuItem = &menuItemList;

        // Resolve a reference to the item
        hCreference locRef(menuItem->getRef());
        locRef.resolveID(hcItem);

        // If the item actually exists
        if (nullptr != hcItem)
        {
            subMenuInfo.basicInfo.itemID = hcItem->getID();

            // gives itemtype
            auto getType = [](auto itemType)
            {
                switch(itemType)
                {
                case iT_Variable:
                    return  JSON_VARIABLE_ITEM_TYPE;
                case iT_Menu:
                    return JSON_MENU_ITEM_TYPE;
                case iT_Method:
                    return JSON_METHOD_ITEM_TYPE;
                case iT_Image:
                    return JSON_IMAGE_ITEM_TYPE;
                case iT_EditDisplay:
                    return JSON_MENU_ITEM_TYPE;
                default:
                    LOGF_ERROR(CPMAPP, "invalid item type");
                    return "";
                }
            };

            // get the itemType
            auto itemType = getType(hcItem->getIType());
            subMenuInfo.basicInfo.itemType = itemType;
            paramItems paramInfo{};
            paramInfo.paramItemID = subMenuInfo.basicInfo.itemID;

            if (itemType == JSON_VARIABLE_ITEM_TYPE)
            {
                //gives variable qualifier in a menu.
                ulong bitstringVal = menuItem->qualifier.bitstringVal;

                hCVar* var = dynamic_cast<hCVar*>(hcItem);
                if (nullptr != var)
                {
                    hCexpression* pExpr = nullptr;
                    if (var->VariableType() == vT_BitEnumerated)
                    {
                        hCBitEnum* enums = nullptr;
                        enums = dynamic_cast<hCBitEnum*>(var);
                        if (nullptr != enums)
                        {
                            //added to handle bit enum variables in a Menu
                            pExpr = menuItem->itemRef.pExpr;
                            if (nullptr != pExpr)
                            {
                                for (auto item : pExpr->expressionL)
                                {
                                    paramInfo.paramBitIndex = item.expressionData.vValue.longlongVal;
                                    // Get the minimum information for Variable to be shown in Menu
                                    itemJsonInfo = getVariable(paramInfo,
                                                               ITEM_ATTR_MIN_MASK);

                                    /* If the variable has WAO relation, set the "ItemHasRelation"
                                       attribute to "true" */
                                    if (var->getWaoRel() != 0)
                                    {
                                        itemJsonInfo[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;
                                    }

                                    applyQualifiersForVariables(bitstringVal, itemJsonInfo);
                                    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
                                }

                                //clears the expression list
                                pExpr->expressionL.clear();
                            }
                        }
                        else
                        {
                            LOGF_ERROR(CPMAPP, "null bit enum variable data in getDDMenu");
                        }
                    }

                    /*This condition is added for variables and bit enum
                      items where bit is not specified in the Menu.
                    */
                    if (nullptr == pExpr)
                    {
                        // Get the minimum information for Variable to be shown in Menu
                        itemJsonInfo = getVariable(paramInfo, ITEM_ATTR_MIN_MASK);

                        /* If the variable has WAO relation, set the "ItemHasRelation"
                           attribute to "true" */
                        if (var->getWaoRel() != 0)
                        {
                            itemJsonInfo[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;
                        }

                        applyQualifiersForVariables(bitstringVal, itemJsonInfo);
                        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "null variable data in getDDMenu");
                }
            }
            else if (itemType == JSON_IMAGE_ITEM_TYPE)
            {
                itemID_t itemID = static_cast<unsigned long>(subMenuInfo.basicInfo.itemID);
                // Get the minimum information for Variable to be shown in Menu
                itemJsonInfo = getImage(itemID, ITEM_ATTR_MIN_MASK);

                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
            }
            else if (itemType == JSON_MENU_ITEM_TYPE || itemType == JSON_METHOD_ITEM_TYPE)
            {
                hcItem->Label(label);
                hcItem->Help(help);
                subMenuInfo.basicInfo.itemLabel = label;
                subMenuInfo.basicInfo.itemHelp = help;

                // add the submenu info to the list
                itemList.push_back(subMenuInfo);

                getMenuAsJson(subMenuInfo, output);
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    gives information about edit display items.
*/
PMStatus HARTAdaptor::getEditDisplayItems(itemID_t itemID, Json::Value &output)
{
    PMStatus status;
    hCeditDisplay* pEditDispInfo = nullptr;
    ItemBasicInfo basicInfo;
    Json::Value itemJsonInfo;
    varPtrList_t varPtrList;
    string label{};
    string help{};

    // gives edit display information
    status = m_pSDC625Controller->getEditDisplayItem(itemID,
                                                     &pEditDispInfo);

    if (status.hasError() || nullptr == pEditDispInfo)
    {
        status.setError(HART_ADAPTOR_GET_EDIT_DISPLAY_FAILED,
                        PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    pEditDispInfo->Label(label);
    pEditDispInfo->Help(help);
    basicInfo.itemID    = pEditDispInfo->getID();
    basicInfo.itemLabel = label;
    basicInfo.itemHelp = help;
    basicInfo.itemType  = JSON_MENU_ITEM_TYPE;

    pEditDispInfo->aquireEditList(varPtrList);

    // For every variable in the edit items...
    for (auto varItem : varPtrList)
    {
        if (nullptr != varItem)
        {
            paramItems paramInfo{};
            paramInfo.paramItemID = varItem->getID();

            // Get the minimum information for Variable to be shown in Menu
            itemJsonInfo = getVariable(paramInfo, ITEM_ATTR_MIN_MASK);
            output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "null variable data in Edit Items");
        }
    }

    // clear the variable lists
    varPtrList.clear();

    // Retrive the display items present in edit display
    pEditDispInfo->aquireDispList(varPtrList);

    // For every variable in the display items...
    for (auto varItem : varPtrList)
    {
        if (nullptr != varItem)
        {
            paramItems paramInfo{};
            paramInfo.paramItemID = varItem->getID();

            /* Get the minimum information for Variable to be shown in
               Edit Display window */
            itemJsonInfo = getVariable(paramInfo, ITEM_ATTR_MIN_MASK);
            output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "null variable data in Display Items");
        }
    }

    // clears the variable list
    varPtrList.clear();

    // Fill the header information for the Edit Display
    fillItemBasicJsonInfo(basicInfo, output);

    return status;
}

/*  ----------------------------------------------------------------------------
    gives wao list items for the given Variable item ID
*/
PMStatus HARTAdaptor::getVarWaoListItems(itemID_t itemID, Json::Value &output)
{
    PMStatus status;
    hCwao* pWaoInfo = nullptr;
    ItemBasicInfo basicInfo;
    Json::Value itemJsonInfo;
    varPtrList_t varPtrList;
    string label{};
    string help{};
    hCVar* pVar = nullptr;

    pVar = m_pSDC625Controller->getVariablePtr(itemID);

    if (pVar != nullptr && pVar->getWaoRel() != 0)
    {
        // gives edit display information
        status = m_pSDC625Controller->getWaoItem(pVar->getWaoRel(), &pWaoInfo);

        if (status.hasError() || nullptr == pWaoInfo)
        {
            status.setError(HART_ADAPTOR_GET_WAO_FAILED,
                            PM_ERR_CL_HART_ADAPTOR);
            return status;
        }

        pWaoInfo->Label(label);
        pWaoInfo->Help(help);
        basicInfo.itemID    = pWaoInfo->getID();
        basicInfo.itemLabel = label;
        basicInfo.itemHelp = help;
        basicInfo.itemType  = JSON_MENU_ITEM_TYPE;

        pWaoInfo->getWAOVarPtrs(varPtrList);

        // For every variable in the edit items...
        for (auto varItem : varPtrList)
        {
            if (nullptr != varItem)
            {
                paramItems paramInfo{};
                paramInfo.paramItemID = varItem->getID();

                // Get all the information for Variable to be shown in Menu
                itemJsonInfo = getVariable(paramInfo, ITEM_ATTR_ALL_MASK);
                itemJsonInfo[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;
                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemJsonInfo);
            }
            else
            {
                LOGF_ERROR(CPMAPP, "null variable data in Edit Items");
            }
        }

        // clear the variable lists
        varPtrList.clear();

        // Fill the header information for the WAO item
        fillItemBasicJsonInfo(basicInfo, output);
        output[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;
    }
    else
    {
        status.setError(HART_ADAPTOR_GET_WAO_FAILED,
                        PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    processes menu and gives menu item information
*/
Json::Value HARTAdaptor::processMenu(const Json::Value& menuRequest,
                                     const ITEM_QUERY ITEMQUERY)
{
    (void)ITEMQUERY;

    Json::Value menuResponse;
    itemID_t itemID{0};
    PMStatus status;
    itemType_t itemType;

    // get the requested id
    if (menuRequest.isMember(JSON_ITEM_ID) &&
        menuRequest[JSON_ITEM_ID].isIntegral() &&
        (menuRequest[JSON_ITEM_ID].asLargestInt() > 0))
    {
        itemID = menuRequest[JSON_ITEM_ID].asLargestUInt();

        status = m_pSDC625Controller->getItemType(itemID, itemType);

        if (!status.hasError())
        {
            if (iT_Menu == itemType)
            {
                // gives menu information
                status = getDDMenu(itemID, menuResponse);
            }
            else if(iT_EditDisplay == itemType)
            {
                // gives edit display information
                status = getEditDisplayItems(itemID, menuResponse);
            }
        }
        else
        {
            Error::Entry error = Error::cpm.GET_MENU_FAILED;
            menuResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        menuResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return menuResponse;
}

/*  ----------------------------------------------------------------------------
    Processes variable information
*/
Json::Value HARTAdaptor::processVariable(const Json::Value& varRequest,
                                         const ITEM_QUERY ITEMQUERY)
{
    Json::Value varResponse;
    PMStatus status;
    paramItems paramInfo{};
    std::string reqType{""};
    ITEM_ID itemID = 0;
    unsigned long itemBitIndex = 0;

    if (varRequest.isMember(JSON_ITEM_ID) &&
        varRequest[JSON_ITEM_ID].isIntegral() &&
        (varRequest[JSON_ITEM_ID].asLargestInt() > 0))
    {        
        itemID = varRequest[JSON_ITEM_ID].asLargestUInt();
        paramInfo.paramItemID = itemID;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID or ItemRequestType or Bit Index");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        return varResponse;
    }

    if (varRequest.isMember(JSON_ITEM_REQ_TYPE))
    {
        if (varRequest[JSON_ITEM_REQ_TYPE].isString())
        {
            reqType = varRequest[JSON_ITEM_REQ_TYPE].asString();
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid ItemRequestType");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }
    }
    

    if (varRequest.isMember(JSON_ITEM_INFO) &&
        varRequest[JSON_ITEM_INFO].isMember(JSON_VAR_BIT_INDEX))
    {
        if (varRequest[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].isIntegral() &&
           (varRequest[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].asLargestInt() >= 0))
        {
            itemBitIndex = varRequest[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].asUInt();
            paramInfo.paramBitIndex = itemBitIndex;
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid Bit Index");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }
    }

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
    {
        if ((false == varRequest.isMember(JSON_ITEM_INFO)) && (FAILURE == validateItemInDD(varRequest,true)))
        {
            LOGF_ERROR(CPMAPP, "Invalid ItemInDD");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }
        
        bool itemHasRelation = false;
        validateItemHasRelation(varRequest[JSON_ITEM_INFO], itemHasRelation);

        if (itemHasRelation)
        {
             // gives WAO items information
             status = getVarWaoListItems(paramInfo.paramItemID, varResponse);

             if (status.hasError())
             {
                 Error::Entry error = Error::cpm.GET_MENU_FAILED;
                 varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                 return varResponse;
             }
        }
        else
        {
             varResponse = getVariable(paramInfo);
        }
    }
    else
    {
        // set the item id to execute pre/post edit action
        SDC625Controller::s_hartVarActionData.itemID = paramInfo.paramItemID;

        if (reqType.compare(JSON_VAR_TYPE_EDIT) == 0)
        {
            SDC625Controller::s_hartVarActionData.actionItemType = EditActionItemTypes::VARIABLE_ACTION;
            SDC625Controller::s_hartVarActionData.actionType = EditActionTypes::PRE_EDIT_ACTION;

            // check if pre edit action exists
            if (m_pSDC625Controller->varActionExists(paramInfo.paramItemID, varAttrPreEditAct))
            {
            	// command response publisher with message.
				Error::Entry error = Error::cpm.PRE_EDIT_ACTION_IS_IN_PROGRESS;
				varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

                // starts the pre edit action thread
                status = processExecuteEditMethod(paramInfo.paramItemID);

                if (status.hasError())
                {
                    LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
                    // RequestType is unavailable; return error
                    Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                    varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                }
            }
            else
            {
                LOGF_INFO(CPMAPP,"Variable has no pre Edit actions with Item ID : " <<paramInfo.paramItemID);
            	Error::Entry error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
				varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else if (reqType.compare(JSON_VAR_TYPE_UPDATE) == 0)
        {
            SDC625Controller::s_hartVarActionData.actionItemType = EditActionItemTypes::VARIABLE_ACTION;
            SDC625Controller::s_hartVarActionData.actionType = EditActionTypes::POST_EDIT_ACTION;

			// check if post edit action exists
			if (m_pSDC625Controller->varActionExists(paramInfo.paramItemID, varAttrPostEditAct))
			{
                Json::Value itemInfo = varRequest[JSON_ITEM_INFO];
                // Write the edited variable values to parameter cache for post edit action validation
                ParamInfo editParamInfo{};
                VariableType editVarType(itemID);
                editVarType.bitIndex = itemBitIndex;
                editParamInfo.paramList.push_back(editVarType);
                
                // Get the parameter information of the variable for post edit action
                PMStatus getParamStatus = m_pParameterCache->getParameter(&editParamInfo);
                if (getParamStatus.hasError())
                {
                    m_pParameterCache->cleanParamInfo(&editParamInfo);
                    LOGF_ERROR(CPMAPP, "Getting Parameter Info Failed");
			        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                    return varResponse;
                }
                
                // Set the value entered in IC
                PMStatus setValStatus = setVariableValue(itemInfo, &editVarType);
                if (setValStatus.hasError())
                {
                    m_pParameterCache->cleanParamInfo(&editParamInfo);
                    LOGF_ERROR(CPMAPP, "Setting Variable Value Failed");
			        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                    return varResponse;
                }

                // Update the parameter cache with IC value
                PMStatus editVarStatus = m_pParameterCache->writeEditValue(&editParamInfo);
                if (editVarStatus.hasError())
                {
                    m_pParameterCache->cleanParamInfo(&editParamInfo);
                    LOGF_ERROR(CPMAPP, "Write to Cache of Edited Values Failed");
			        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                    return varResponse;
                }

				// command response publisher with message.
				Error::Entry error = Error::cpm.POST_EDIT_ACTION_IS_IN_PROGRESS;
				varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

				// starts the post edit action thread
				status = processExecuteEditMethod(paramInfo.paramItemID);

				if (status.hasError())
				{
                    LOGF_ERROR(CPMAPP, "Error in calling processExecuteEditMethod in Adaptor");
					// RequestType is unavailable; return error
					Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
				}
			}
			else
			{
                LOGF_INFO(CPMAPP,"Variable has no post Edit actions with Item ID : " <<paramInfo.paramItemID);
				// command response publisher  with no edit action.
				Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
				varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
			}
        }
        else if (reqType.compare(JSON_VAR_TYPE_WRITE) == 0)
        {
        	// Requested set item action is update variable
            if (varRequest.isMember(JSON_ITEM_INFO))
            {
                Json::Value itemInfo = varRequest[JSON_ITEM_INFO];
                varResponse = setVariable(paramInfo, itemInfo);
            }
            else
            {
                LOGF_ERROR(CPMAPP, "ItemInfo is unavailable in setItem request");
                Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
                varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "ItemRequestType is unavailable in setItem request");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }

    return varResponse;
}

/*  ----------------------------------------------------------------------------
    Processes Image information
*/
Json::Value HARTAdaptor::processImage
(
    const Json::Value& imageRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    PMStatus status;
    Json::Value imageResponse = Json::Value("ok");

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY &&
        imageRequest.isMember(JSON_ITEM_ID) &&
        imageRequest[JSON_ITEM_ID].isIntegral() &&
        (imageRequest[JSON_ITEM_ID].asLargestInt() > 0))
    {
        ITEM_ID itemID = imageRequest[JSON_ITEM_ID].asLargestUInt();
        imageResponse = getImage(itemID);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item query or Item ID for the Image");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        imageResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return imageResponse;
}

/*  ----------------------------------------------------------------------------
    Processes GetItem and SetItem request for Method execution based on type
*/
Json::Value HARTAdaptor::processMethod
(
    const Json::Value& methodRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value methodResponse = Json::Value("ok");

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
    {
        methodResponse = processGetMethodExecution(methodRequest);
    }
    else if (ITEM_QUERY::SET_ITEM == ITEMQUERY)
    {
        methodResponse = processSetMethodExecution(methodRequest);
    }

    return methodResponse;
}

/*  ----------------------------------------------------------------------------
    Retrieves Variable value with attributes
*/
Json::Value HARTAdaptor::getVariable(paramItems& paramItem,
                                     MaskType maskType)
{
    Json::Value getVarResp;
    PMStatus status;
    Error::Entry error;

    ParamInfo paramInfo;
    VariableType varType(paramItem.paramItemID);
    // BitIndex is used only for single BITENUMERATTION_ITEM type
    varType.bitIndex = paramItem.paramBitIndex;

    paramInfo.requestInfo.maskType = maskType;
    paramInfo.paramList.push_back(varType);

    // Call getParameter and retrieve parameter information
    status = m_pParameterCache->getParameter(&paramInfo);

    if (!status.hasError())
    {
        /*  TODO (Aravind) : We are not providing device values as part of getVariable call,
            its been agreed that subscription is requried to read device value irrespective
            of paraemter type (Variable, Array, Record)
        */
        //status = m_pParameterCache->readValue(&paramInfo);
        if (!status.hasError())
        {
            auto varParamInfo = dynamic_cast<VariableType*>(
                                &paramInfo.paramList.at(0).get());

            // Fill itemBasicInfo structure
            VarBasicInfo varBasicInfo{};
            varBasicInfo.basicInfo.itemID = varParamInfo->itemID;
            varBasicInfo.basicInfo.itemHelp = validateStringValue(varParamInfo->help);
            varBasicInfo.basicInfo.itemLabel = validateStringValue(varParamInfo->label);
            varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
            if (READ_HANDLING != varParamInfo->misc.handling)
            {
                varBasicInfo.itemReadOnly = false;
            }
            varBasicInfo.itemClass = varParamInfo->misc.class_attr;

            // Fill the basic information in JsonResponse
            fillVarBasicJsonInfo(varBasicInfo, getVarResp);

            // Fill the variable attribute information
            fillVariableJsonInfo(varParamInfo, getVarResp);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "ReadValue Failed!!");
            error = Error::cpm.READ_VALUE_FAILED;
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
        error = Error::cpm.GET_PARAMETER_FAILED;
    }

    if (status.hasError())
    {
        getVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    //cleans param Info
    m_pParameterCache->cleanParamInfo(&paramInfo);

    return getVarResp;
}

/*  ----------------------------------------------------------------------------
    Retrieves Image value with attributes
*/
Json::Value HARTAdaptor::getImage
(
    ITEM_ID& itemID,
    MaskType maskType
)
{
    Json::Value getImageResp;
    PMStatus status;
    Error::Entry error;

    ParamInfo paramInfo;
    ImageType imageType(itemID);
    paramInfo.requestInfo.maskType = maskType;
    paramInfo.paramList.push_back(imageType);

    // Call getParameter and retrieve parameter information
    status = m_pParameterCache->getParameter(&paramInfo);

    if (!status.hasError())
    {
        auto imageParamInfo = dynamic_cast<ImageType*>(
                            &paramInfo.paramList.at(0).get());

        // Fill itemBasicInfo structure
        ItemBasicInfo imageBasicInfo{};
        imageBasicInfo.itemID = imageParamInfo->itemID;
        imageBasicInfo.itemHelp = validateStringValue(imageParamInfo->help);
        imageBasicInfo.itemLabel = validateStringValue(imageParamInfo->label);
        imageBasicInfo.itemType = JSON_IMAGE_ITEM_TYPE;

        // Fill the basic information in JsonResponse
        fillItemBasicJsonInfo(imageBasicInfo, getImageResp);

        // Fill the variable attribute information
        fillImageJsonInfo(imageParamInfo, getImageResp);

        // clears the paramInfo
        paramInfo.paramList.clear();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
        error = Error::cpm.GET_PARAMETER_FAILED;
    }

    if (status.hasError())
    {
        getImageResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return getImageResp;
}

/*  ----------------------------------------------------------------------------
    Creates image file by using image raw data
*/
const char* HARTAdaptor::storeImage(ImageType* imageParam)
{
    char* imagePath = nullptr;
    if (nullptr != imageParam && nullptr != imageParam->data
        && imageParam->length > 0)
    {
        char oBuffer[MAX_IMAGE_FILENAME_LEN];
        snprintf(oBuffer, sizeof(oBuffer), "%ld", imageParam->itemID);
        strcat(oBuffer, REPLACE_RANDOM_STRING);
        
        IAppConfigParser *pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();
        if (nullptr != pAppConfigParser)
        {
            std::string imageFile;     
            imageFile = pAppConfigParser->getImagePath() + "/" + std::string(oBuffer);

            memset (oBuffer, '\0', MAX_IMAGE_FILENAME_LEN);
            strncpy (oBuffer, imageFile.c_str(), imageFile.length());
        }
    
        char* ouputFileName = oBuffer;

        // Create a image file with trailing "XXXXXX" character at the end of file name
        int fd = mkstemp(ouputFileName);
        // Write the image content to the file
        write(fd, imageParam->data, imageParam->length);
        // Close the file descriptor
        close(fd);
        // Get the complete image file name
        imagePath = realpath(ouputFileName, NULL);
    }

    return imagePath;
}

/*  ----------------------------------------------------------------------------
    Sets value into a device variable parameter
*/
Json::Value HARTAdaptor::setVariable(ParamItems& paramItem,
                                     Json::Value& itemInfo,
                                     MaskType maskType)
{
    Json::Value setVarResp = Json::Value("ok");
    PMStatus status;

    ParamInfo setParamInfo{};
    setParamInfo.requestInfo.maskType = maskType;
    VariableType varType(paramItem.paramItemID);
    // BitIndex is used only for single BITENUMERATTION_ITEM type
    varType.bitIndex = paramItem.paramBitIndex;
    setParamInfo.paramList.push_back(varType);

    m_pParameterCache->pauseSubscription();

    // Retrieve parameter information
    status = m_pParameterCache->getParameter(&setParamInfo);

    if (!status.hasError())
    {
        auto type = &setParamInfo.paramList.at(0).get();
        auto varParam = dynamic_cast<VariableType*>(type);

        if (READ_HANDLING != varParam->misc.handling)
        {
            // set the user modified value into paramInfo sructure
            status = setVariableValue(itemInfo, varParam);

            if (!status.hasError())
            {
                // write the value to the device and parameter cache
                status = m_pParameterCache->writeValue(&setParamInfo);
            }

            if (status.hasError())
            {
                Error::Entry error = Error::cpm.WRITE_VALUE_FAILED;
                setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Parameter is readonly!, cannot be written");
            Error::Entry error = Error::cpm.PARAM_READONLY;
            setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        // Getting parameter information failed
        Error::Entry error = Error::cpm.SET_ITEM_FAILED;
        setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // clears the paramInfo
    m_pParameterCache->cleanParamInfo(&setParamInfo);
    m_pParameterCache->resumeSubscription();

    return setVarResp;
}

/*  ----------------------------------------------------------------------------
    Sets variable value into parameter info structure
*/
PMStatus HARTAdaptor::setVariableValue
(
    const Json::Value& itemInfo,
    VariableType* varParam
)
{
    PMStatus status;
    int errorCode = FAILURE;

    if (itemInfo.isNull() || !(itemInfo.isMember(JSON_VAR_VALUE)))
    {
        status.setError(HART_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_HART_ADAPTOR);
    }

    if (!status.hasError())
    {
        switch(varParam->type)
        {
            case vT_undefined:
                break;

            case vT_Integer:
            {
                if (itemInfo[JSON_VAR_VALUE].isIntegral())
                {
                    // For validation of range, verified with value -62362362889
                    long long tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();

                    if (varParam->size == THREE_BYTES ||  (varParam->size >= FIVE_BYTES && varParam->size <= EIGHT_BYTES) )
                    {
                        if (validateNonStandardIntData(varParam->size, tempValue))
                        {
                            varParam->value.stdValue.l = itemInfo[JSON_VAR_VALUE].asLargestInt();
                            errorCode = SUCCESS;
                        }
                    }
                    else
                    {
                        if (validateIntegerRange<int, long>(tempValue))
                        {
                            errorCode = validateVariableLimit(itemInfo,varParam);
                            if (errorCode == SUCCESS)
                            {
                                varParam->value.stdValue.l = itemInfo[JSON_VAR_VALUE].asLargestInt();
                            }
                        }
                    }
                }
                else
                {
                     LOGF_ERROR(CPMAPP, "HART:Invalid input(range) for Integer variable: " << varParam->type);
                }
            }
            break;

            case vT_BitEnumerated:
                // Added below condition to handle singe BITENUMERATION_ITEM type
                if (varParam->bitIndex > 0)
                {
                    if (itemInfo[JSON_VAR_VALUE].isBool())
                    {
                        varParam->value.stdValue.ul =
                                itemInfo[JSON_VAR_VALUE].asBool() == true ? 1 : 0;
                         errorCode = SUCCESS;
                    }
                    else
                    {
                         LOGF_ERROR(CPMAPP, "Not a valid input for variable "
                                    << varParam->type);                        
                    }
                     break;
                }// FALLTHROUGH if varParam->bitIndex is 0 i.e, for BITENUMERATION type
            case vT_Enumerated:
            case vT_Unsigned:
            {
                unsigned long long tempValue = 0;
                if (itemInfo[JSON_VAR_VALUE].isIntegral())
                {
                    // for handling exception, if value has entered more than limit.
                    try
                    {
                        // For validation of range, verified with value 712376526151635
                        tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                    }
                    catch(...)
                    {
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                    PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }

                    if (varParam->size == THREE_BYTES ||  (varParam->size >= FIVE_BYTES && varParam->size <= EIGHT_BYTES) )
                    {
                        if (validateNonStandardUIntData(varParam->size, tempValue))
                        {
                            varParam->value.stdValue.l = itemInfo[JSON_VAR_VALUE].asLargestInt();
                            errorCode = SUCCESS;
                        }
                    }
                    else
                    {
                        if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                        {
                            errorCode = validateVariableLimit(itemInfo,varParam);

                            if (errorCode == SUCCESS)
                            {
                                varParam->value.stdValue.ul = itemInfo[JSON_VAR_VALUE].asUInt();
                            }
                        }
                    }
                }
                else
                {
                     LOGF_ERROR(CPMAPP, "Not a valid input for arithmetic Unsigned value " << varParam->type);
                     errorCode = FAILURE;
                }
             }
                break;

            case vT_FloatgPt:
            {
                if (itemInfo[JSON_VAR_VALUE].isNumeric() && Json::ValueType::realValue == itemInfo[JSON_VAR_VALUE].type())
                {
                    // For validation of range, verified with value 632732323652361251236156125126351236.3843282643643;
                    double tempValue = itemInfo[JSON_VAR_VALUE].asDouble();

                    if ((tempValue >= std::numeric_limits<float>::lowest())
                            && (tempValue <= std::numeric_limits<float>::max()))
                    {
                        varParam->value.stdValue.f = itemInfo[JSON_VAR_VALUE].asFloat();
                        errorCode = SUCCESS;
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "HART:Invalid input(range) for variable: " << varParam->type);
                }

            }
                break;

            case vT_Double:
            {
                if (itemInfo[JSON_VAR_VALUE].isNumeric() && Json::ValueType::realValue == itemInfo[JSON_VAR_VALUE].type())
                {
                    // no validation required for double variable
                    varParam->value.stdValue.d = itemInfo[JSON_VAR_VALUE].asDouble();
                    errorCode = SUCCESS;
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "HART:Invalid input(range) for variable: " << varParam->type);
                }
            }
                break;

            case vT_TimeValue:
            {
                unsigned long long tempValue = 0;
                if (itemInfo[JSON_VAR_VALUE].isIntegral())
                {
                    try
                    {
                        // For validation of range, verified with value 712376526151635
                        tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                    }
                    catch(...)
                    {
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                    PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }
                    // For validation of range, verified with value 712376526151635
                    if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                    {
                        errorCode = validateVariableLimit(itemInfo,varParam);

                        if (errorCode == SUCCESS)
                        {
                            varParam->value.stdValue.ul = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                        }
                    }
                }
                else
                {
                     LOGF_ERROR(CPMAPP, "HART:Invalid input(range) for variable: " << varParam->type);
                }

            }
                break;
            case vT_HartDate:
            {
                long long tempValue = 0;
                if (itemInfo[JSON_VAR_VALUE].isIntegral())
                {
                    try
                    {
                        // For validation of range, verified with value 712376526151635
                        tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();
                    }
                    catch(...)
                    {
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                    PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }
                    // For validation of range, verified with value 712376526151635
                    if (validateIntegerRange<long long, long long>(tempValue))
                    {
                        errorCode = validateVariableLimit(itemInfo,varParam);

                        if (errorCode == SUCCESS)
                        {
                            varParam->value.stdValue.l = itemInfo[JSON_VAR_VALUE].asLargestInt();
                        }
                    }
                }
                else
                {
                     LOGF_ERROR(CPMAPP, "HART:Invalid input(range) for variable: " << varParam->type);
                }
            }
            break;

            case vT_Ascii:
            case vT_PackedAscii:
            case vT_Password:
            case vT_BitString:
            case vT_EUC:
            case vT_OctetString:
            case vT_VisibleString:
            {
                if(itemInfo[JSON_VAR_VALUE].isString())
                {
                    std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                    // validation:check if string is NULL
                    if (!tempStr.empty())
                    {
                        if (tempStr.length() > varParam->value.strData.len)
                        {
                            LOGF_INFO(CPMAPP, "Can not set the string which is more than "
                                          << varParam->value.strData.len << " characters");
                        }
                        else
                        {
                            if (nullptr != varParam->value.strData.str && varParam->value.strData.flag == FREE_STRING)
                            {
                                delete[] varParam->value.strData.str;
                                varParam->value.strData.str = nullptr;
                                varParam->value.strData.len = 0;
                                varParam->value.strData.flag = DONT_FREE_STRING;
                            }

                            if ((vT_Ascii == varParam->type) ||
                                (vT_Password == varParam->type))
                            {
                                // Convert UTF8 string to ISO-Latin-1 before writing the value to device
                                size_t utfStrLen = tempStr.length();
                                char* utf8Str = (char *) calloc ((utfStrLen + 1), sizeof(char));
                                if (nullptr == utf8Str)        
                                {
                                    LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                                    break;
                                }

                                size_t isoLatinStrLen = (utfStrLen * 2) + 1;
                                char* isoLatinStr = (char *) calloc (isoLatinStrLen, sizeof(char));
                                if (nullptr == isoLatinStr)        
                                {
                                    LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                                    break;
                                }

                                strncpy (utf8Str, tempStr.c_str(), tempStr.length());
                            
                                char *tempUtf8Str = utf8Str;
                                char *tempIsoLatintStr = isoLatinStr;
                                bool convRetVal = CPMUtil::convertUtf8ToIsolatin1 (&tempUtf8Str, &utfStrLen,     
                                                                &tempIsoLatintStr, &isoLatinStrLen);

                                if (true == convRetVal)
                                {
                                    tempStr = isoLatinStr;
                                    //  Free-up the memory
                                    free (isoLatinStr);
                                    free (utf8Str);
                                }   
                                else
                                {
                                    LOGF_ERROR (CPMAPP, "Cannot convert UTF8 character to ISO-Latin-1");
                                    //  Free-up the memory
                                    free (isoLatinStr);
                                    free (utf8Str);
                                    break;
                                }
                            }

                            varParam->value.strData.str =  new char[tempStr.length()+1];
                            strncpy(varParam->value.strData.str, tempStr.c_str(), tempStr.length());
                            varParam->value.strData.str[tempStr.length()] = '\0';
                            varParam->value.strData.len = tempStr.length();
                            varParam->value.strData.flag = FREE_STRING;
                            errorCode = SUCCESS;
                        }
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Can not set Empty String: " << varParam->type);
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "HART:Not a valid input for String type " << varParam->type);
                }
            }
            break;

            default:
            {
                LOGF_ERROR(CPMAPP, "Invalid Variable Type: " << varParam->type);
            }
            break;
        } //!-- switch case
    } //  !-- if
    if(errorCode == FAILURE)
    {
        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Fills Variable information into Json response
*/
void HARTAdaptor::fillVariableJsonInfo
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    if (varParam == nullptr)
    {
        LOGF_ERROR(CPMAPP,"GET Variable ERROR:null pointer");
        return;
    }

    if ((varParam->type == vT_Integer) ||
        (varParam->type == vT_Unsigned) ||
        (varParam->type == vT_FloatgPt) ||
        (varParam->type == vT_Double) ||
        (varParam->type == vT_Boolean) ||
        (varParam->type == vT_MaxType))
    {
        fillVarArithmeticJson(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == vT_Enumerated) ||
             (varParam->type == vT_BitEnumerated) ||
             (varParam->type == vT_Index))
    {
        fillVarEnumerationJson(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == vT_OctetString) ||
             (varParam->type == vT_VisibleString) ||
             (varParam->type == vT_Ascii) ||
             (varParam->type == vT_PackedAscii) ||
             (varParam->type == vT_BitString) ||
             (varParam->type == vT_EUC) ||
             (varParam->type == vT_Password))
    {
        fillStringJsonValue(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == vT_HartDate) ||
             (varParam->type == vT_TimeValue))
    {
        fillVarDateTimeJson(varParam, varJsonResp, varMaskType);
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Invalid Variable Type: " << varParam->type);
    }
}

/*  ----------------------------------------------------------------------------
    Fills Image information into Json response
*/
void HARTAdaptor::fillImageJsonInfo
(
    ImageType* imageParam,
    Json::Value& imageParamJson
)
{
    char* imagePath = const_cast<char*>(storeImage(imageParam));
    if (nullptr == imagePath)
    {
        LOGF_ERROR(CPMAPP, "Failed to get Image : " << imageParam->itemID);
        return;
    }

    imageParamJson[JSON_ITEM_INFO][JSON_IMAGE_PATH] = imagePath;

    if (nullptr != imagePath)
    {
        free(imagePath);
        imagePath = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Fills Variable Arithmetic information into Json response
*/
void HARTAdaptor::fillVarArithmeticJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];

        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ARITHMETIC;
        (*tempJsonObj)[JSON_VAR_UNIT] = validateStringValue(varParam->unit);
        (*tempJsonObj)[JSON_DISPLAY_FORMAT] = validateStringValue(varParam->displayFormat);
        (*tempJsonObj)[JSON_EDIT_FORMAT] = validateStringValue(varParam->editFormat);
        (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);

        // Assign min and max values
        assignMinMaxValues(varParam, varJsonResp);

    }

    (*tempJsonObj)[JSON_VAR_UNIT] = validateStringValue(varParam->unit);
    fillVarValue(varParam, *tempJsonObj);

    if (vT_FloatgPt == varParam->type)
    {
        (*tempJsonObj)[JSON_VAR_VAL_IS_NAN] = isNAN(varParam->value.stdValue.f);
        (*tempJsonObj)[JSON_VAR_VAL_IS_INF] = isINF(varParam->value.stdValue.f);

        if ((*tempJsonObj)[JSON_VAR_VAL_IS_NAN].asBool() ||
           (*tempJsonObj)[JSON_VAR_VAL_IS_INF].asBool())
        {
            (*tempJsonObj)[JSON_VAR_VALUE] = std::numeric_limits<float>::max();
        }
    }

    if (vT_Double == varParam->type)
    {
        (*tempJsonObj)[JSON_VAR_VAL_IS_NAN] = isNAN(varParam->value.stdValue.d);
        (*tempJsonObj)[JSON_VAR_VAL_IS_INF] = isINF(varParam->value.stdValue.d);
        
        if ((*tempJsonObj)[JSON_VAR_VAL_IS_NAN].asBool() ||
           (*tempJsonObj)[JSON_VAR_VAL_IS_INF].asBool())
        {
            (*tempJsonObj)[JSON_VAR_VALUE] = std::numeric_limits<double>::max();
        }
    }
}

/*  ----------------------------------------------------------------------------
    Fills Enumeration information into Json response
*/
void HARTAdaptor::fillVarEnumerationJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ENUM;
        switch (varParam->type)
        {
        case vT_Enumerated:
            (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_TYPE_ENUM;
            break;

        case vT_BitEnumerated:
            if (varParam->bitIndex > 0)
            {
                (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_BITENUM_ITEM;
                (*tempJsonObj)[JSON_VAR_BIT_INDEX] = static_cast<Json::Value::LargestUInt>(varParam->bitIndex);
            }
            else
            {
                (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_BITENUM;
            }
            break;
        case vT_Index:
            (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_INDEX;
            break;

        default:
            LOGF_ERROR(CPMAPP,"Invalid ENUM/BITEnum type "<< varParam->type);
            break;
        }

        if (varParam->bitIndex == 0)
        {
            int countEnum = varParam->enums.size();

            for (int index = 0; index < countEnum; index++)
            {
                Json::Value enumJsonObj;

                enumJsonObj[JSON_VAR_ENUM_VALUE] = static_cast<Json::Value::LargestUInt>(varParam->enums.at(index).value);
                enumJsonObj[JSON_VAR_ENUM_LABEL] = validateStringValue(varParam->enums.at(index).label);
                enumJsonObj[JSON_VAR_ENUM_HELP] =  validateStringValue(varParam->enums.at(index).help);

                (*tempJsonObj)[JSON_VAR_ENUM_INFO].append(enumJsonObj);
            }
        }
    }

    if ((varParam->type == vT_BitEnumerated) && (varParam->bitIndex > 0))
    {
        (*tempJsonObj)[JSON_VAR_VALUE] = (varParam->value.stdValue.l & BIT_ON) ? true : false;
    }
    else
    {
        (*tempJsonObj)[JSON_VAR_VALUE] = static_cast<Json::Value::LargestInt>(varParam->value.stdValue.l);
    }
}

/*   ----------------------------------------------------------------------------
     Returns string extracted from StringType parameter
*/
std::string HARTAdaptor::validateStringValue(StringType& strType)
{
    std::string retStr("");

    if ((nullptr != strType.str) &&
        (0 != strType.len) &&
        ('\0' != (*(strType.str))))
    {
        //retStr.assign(strType.str, strType.len);
        retStr.assign(strType.str);
    }

    return retStr;
}

/*  ----------------------------------------------------------------------------
    Retrieves variable value type from ddsVarValMap
*/
std::string HARTAdaptor::getVarValType(unsigned int varType)
{
    std::string varValueType("");
    auto sdcVarValMapItr = m_sdcVarValMap.find(varType);

    // Check if the variable value type exists for the provided variable type
    if (sdcVarValMapItr != m_sdcVarValMap.end())
    {
        varValueType = sdcVarValMapItr->second;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Could not find variable value type: " << varType);
    }

    // Return the variable value type
    return varValueType;
}

/*  ----------------------------------------------------------------------------
    Retrieves character set type form map
*/
std::string HARTAdaptor::getCharSetType(unsigned int charSetVal)
{
    auto it = charSetMap.find(charSetVal);

    return it->second;
}

/*  ----------------------------------------------------------------------------
    Fills String Information into Json response
*/
void HARTAdaptor::fillStringJsonValue
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE]= JSON_VAR_TYPE_STRING;

        Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
        (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);

        if ((0 < varParam->value.strData.len) &&
            (*(varParam->value.strData.str) != '\0'))
        {
            (*tempJsonObj)[JSON_VAR_VAL_LENGTH] = varParam->value.strData.len;
            (*tempJsonObj)[JSON_VAR_CHAR_SET] = getCharSetType(varParam->value.strData.ucType);
        }
    }

    std::string strValue = "";

    if (nullptr != varParam->value.strData.str)
    {
        strValue = varParam->value.strData.str;
    }

   (*tempJsonObj)[JSON_VAR_VALUE] = strValue;
}

/*  ----------------------------------------------------------------------------
    Fills Date-Time Information into Json response
*/
void HARTAdaptor::fillVarDateTimeJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_DATE_TIME;

        (*tempJsonObj)[JSON_VAR_UNIT] = validateStringValue(varParam->unit);
        (*tempJsonObj)[JSON_DISPLAY_FORMAT] = validateStringValue(varParam->displayFormat);
        (*tempJsonObj)[JSON_EDIT_FORMAT] = validateStringValue(varParam->editFormat);
            
        if (TIME_VALUE_SCALED == varParam->misc.timeValType)
        {
            (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_TIME_VAL_SCALED;
            (*tempJsonObj)[JSON_VAR_VAL_INFO][JSON_VAR_VAL_SCALED] = (*tempJsonObj)[JSON_VAR_UNIT];
        }
        else if (TIME_VALUE_DIFF == varParam->misc.timeValType)
        {
            (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_TIME_VAL_DIFF;

        }
        else 
        {
            (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);
        }

        assignMinMaxValues(varParam, varJsonResp);
    }
    
    fillVarValue(varParam, *tempJsonObj);
}

/*  ----------------------------------------------------------------------------
    Fills Json value key with variable value
*/
void HARTAdaptor::fillVarValue(VariableType* varParam, Json::Value& varValueJson)
{
    switch(varParam->type)
    {
        // Integer and enumeration types
        case vT_Integer:        //FALLTHROUGH
        case vT_Index:          //FALLTHROUGH
        case vT_Enumerated:     //FALLTHROUGH
        case vT_BitEnumerated:
		{
        	varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestInt>(varParam->value.stdValue.l);
		}
        break;

        case vT_FloatgPt:
		{
            varValueJson[JSON_VAR_VALUE] = varParam->value.stdValue.f;
		}
        break;

        case vT_Double:
        {
            varValueJson[JSON_VAR_VALUE] = varParam->value.stdValue.d;
        }
        break;

        case vT_Unsigned:     //FALLTHROUGH
        case vT_Time:         //FALLTHROUGH
        case vT_Duration:     //FALLTHROUGH
        case vT_TimeValue:    //FALLTHROUGH
		{
            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestUInt>(varParam->value.stdValue.ul);
		}
        break;

        case vT_HartDate:
        {
            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestInt>(varParam->value.stdValue.l);
        }
        break;

        case vT_Ascii:          //FALLTHROUGH
        case vT_OctetString:    //FALLTHROUGH
        case vT_VisibleString:  //FALLTHROUGH
        case vT_PackedAscii:    //FALLTHROUGH
        case vT_Password:       //FALLTHROUGH
        case vT_EUC:            //FALLTHROUGH
        case vT_BitString:      //FALLTHROUGH
        {
            if ((varParam->value.strData.str != nullptr) &&
                (0 < varParam->value.strData.len) &&
                (*(varParam->value.strData.str) != '\0'))
            {
                std::string strValue;
                strValue.assign(varParam->value.strData.str);
                varValueJson[JSON_VAR_VALUE] = strValue;
            }
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid Variable Type: " << varParam->type);
        }
        break;
    }
}

/*  ----------------------------------------------------------------------------
    Assign min and max range values
*/
void HARTAdaptor::assignMinMaxValues
(
    VariableType* varParam,
    Json::Value& varJsonResp
)
{
    int vectorSize = varParam->misc.minMaxRange.size();

    if (vectorSize != 0)
    {
        for (int index = 0 ; index < vectorSize; index++)
        {
            Json::Value tempJsonObj;

            switch(varParam->type)
            {
                case vT_Integer:
                {
                     tempJsonObj[JSON_VAR_VAL_MIN] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).min.l);
                     tempJsonObj[JSON_VAR_VAL_MAX] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).max.l);
                }
                break;

                case vT_HartDate:
                {
                    tempJsonObj[JSON_VAR_VAL_MIN] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).min.l);
                    tempJsonObj[JSON_VAR_VAL_MAX] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).max.l);
                }
                break;

                case vT_Unsigned:
                case vT_TimeValue:
                {
                    tempJsonObj[JSON_VAR_VAL_MIN] =
                                static_cast<Json::Value::LargestUInt>(varParam->misc.minMaxRange.at(index).min.ul);
                    tempJsonObj[JSON_VAR_VAL_MAX] =
                                static_cast<Json::Value::LargestUInt>(varParam->misc.minMaxRange.at(index).max.ul);
                }
                break;

                case vT_FloatgPt:
                {
                    tempJsonObj[JSON_VAR_VAL_MIN] =
                                varParam->misc.minMaxRange.at(index).min.f;
                    tempJsonObj[JSON_VAR_VAL_MAX] =
                                varParam->misc.minMaxRange.at(index).max.f;
                }
                break;

                case vT_Double:
                {
                    tempJsonObj[JSON_VAR_VAL_MIN] =
                                varParam->misc.minMaxRange.at(index).min.d;
                    tempJsonObj[JSON_VAR_VAL_MAX] =
                                varParam->misc.minMaxRange.at(index).max.d;
                }
                break;

                default:
                {
                    LOGF_ERROR(CPMAPP,"Invalid Type: "<< varParam->type);
                }
                break;
            }
            varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VAL_RANGE].append(tempJsonObj);
        }
    }
}

/*----------------------------------------------------------------------------
   Returns menu in Json format for the given item list
*/
void HARTAdaptor::getMenuAsJson(VarBasicInfo& menuItem, Json::Value& output)
{
    // Fill the Menu Items
    Json::Value itemNode;
    fillVarBasicJsonInfo(menuItem, itemNode);

    // Append to the item List
    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemNode);
}

/*  ----------------------------------------------------------------------------
    Fills item basic attribute information into Json response
*/
void HARTAdaptor::fillItemBasicJsonInfo
(
    ItemBasicInfo& itemBasicInfo,
    Json::Value& jsonResp
)
{
    // Fill the basic attribute information into json response
    jsonResp[JSON_ITEM_LABEL] = itemBasicInfo.itemLabel;
    jsonResp[JSON_ITEM_HELP] = itemBasicInfo.itemHelp;
    jsonResp[JSON_ITEM_TYPE] = itemBasicInfo.itemType;
    jsonResp[JSON_ITEM_CONT_TAG] = itemBasicInfo.itemContainerTag;
    jsonResp[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(itemBasicInfo.itemID);
    jsonResp[JSON_ITEM_IN_DD] = itemBasicInfo.itemInDD;
}

/*  ----------------------------------------------------------------------------
    Fills variable attribute information into Json response
*/
void HARTAdaptor::fillVarBasicJsonInfo
(
    VarBasicInfo& varBasicInfo,
    Json::Value& jsonResp
)
{
    // Fill the default minimum information required for a variable
    fillItemBasicJsonInfo(varBasicInfo.basicInfo, jsonResp);
    // Fill the default minimum information required for a variable
    jsonResp[JSON_ITEM_INFO][JSON_ITEM_READONLY] = varBasicInfo.itemReadOnly;
    jsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = static_cast<Json::Value::LargestUInt>(varBasicInfo.itemClass);
}

/*  ----------------------------------------------------------------------------
    Disconnects the connected device
*/
Json::Value HARTAdaptor::disconnect()
{
    if (CPMSTATE_METHOD_EXEC_IN_PROGRESS == (CPMSTATE_METHOD_EXEC_IN_PROGRESS & CPMStateMachine::getInstance()->getCpmState()))
    {
        // Stop current method execution before disconenction
        stopMethodExec();
    }

    PMStatus status = disconnectDevice();

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Device Disconnect Failed: " << status.getErrorCode());
        Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Clears subscription and disconnects device
*/
PMStatus HARTAdaptor::disconnectDevice()
{
    PMStatus status;

    if (nullptr != m_pParameterCache)
    {
        // Delete all subscription
        status = m_pParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_NONE);

        if (!status.hasError())
        {
            for (auto item : m_subscriptionCacheTable.cacheItems)
            {
                if (nullptr != item.varType)
                {
                    delete item.varType;
                    item.varType = nullptr;
                }
            }
            m_subscriptionCacheTable.cacheItems.clear();

            status = m_pConnectionManager->disconnectDevice();
        }
    }

    if (!status.hasError())
    {
        // Uninitialize the device manager in device disconnection
        m_pSDC625Controller->uninitializeDeviceManager();

        // Clear the connect state
        CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_DEVICE_CONNECTED);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves the basic field device information
*/
Json::Value HARTAdaptor::getFieldDeviceConfig(const Json::Value& jsonData)
{
    /* Typecasted with void to supress the unused parameter
       warning, must remove when implementation takes place
    */
    (void) jsonData;

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Assigns the user configured data
*/
Json::Value HARTAdaptor::setFieldDeviceConfig(const Json::Value& jsonData)
{
    /* Typecasted with void to supress the unused parameter
       warning, must remove when implementation takes place
    */
    (void) jsonData;

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Populates device information of the available devices in the network using
    getDeviceInfo() Adaptor API
*/
PMStatus HARTAdaptor::populateDeviceInfo()
{
    PMStatus pmstatus;
    return pmstatus;
}

/*  ----------------------------------------------------------------------------
    check if HART bus params are available
*/
bool HARTAdaptor::isBusParamAvailable(const Json::Value& jsonData)
{
    return jsonData.isMember(JSON_HART_BUS_PARAMS);
}

/*  ----------------------------------------------------------------------------
    check if HART Application Settings Available are available.
*/
bool HARTAdaptor::isAppSettingsAvailable(const Json::Value& jsonData)
{
    return jsonData.isMember(JSON_HART_APP_SETTINGS);
}

/*  ----------------------------------------------------------------------------
    Interface to CPM layer, return the status success/fail in json
    data(Json::Value) if bus params provided by UI/User set to HART stack
*/
Json::Value HARTAdaptor::setBusParameters(const Json::Value& jsonData)
{
    PMStatus status;
    unsigned int masterType;
    Error::Entry error;

    Json::Value jsonResult;

    if (jsonData.isNull() || nullptr == m_pBusinessLogic)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: json or BusinessLogic "
                              "object is null in setBusParameters");

        status.setError(HART_ADAPTOR_ERR_JSON_PARSE, PM_ERR_CL_HART_ADAPTOR);
    }

    if (!status.hasError())
    {
        /* Set default bus paramters if ["set_default"] is
           set to 1 from UI
        */
        if (jsonData.isMember(HART_BUS_PARAMS_KEY) &&
                jsonData[HART_BUS_PARAMS_KEY].isMember(SET_DEFLT))
        {
            if (jsonData[HART_BUS_PARAMS_KEY][SET_DEFLT].isIntegral() &&
                (jsonData[HART_BUS_PARAMS_KEY][SET_DEFLT].asLargestInt() >= 0))
            {
                if (DEFAULT_VAL == jsonData[HART_BUS_PARAMS_KEY][SET_DEFLT].asUInt())
                {
                    // Sets the default bus parameters.
                    setDefaultBusParameters();
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Invalid Bus Parameters");
                status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                                PM_ERR_CL_HART_ADAPTOR);
            }
        }
        else
        {
            if (jsonData.isMember(HART_BUS_PARAMS_KEY) &&
                jsonData[HART_BUS_PARAMS_KEY].isMember(HART_MASTER_TYPE))
            {
                /* Assign and validating the bus parameters values recived from
                   user/UI.
                */
                if (jsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].isIntegral() &&
                    (jsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asLargestInt() >= 0))
                {
                    masterType = jsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asUInt();
                    m_busParams.masterType = static_cast<HartMasterType>(masterType);

                    // Validates the configuration against the user modified value
                    status = validateBusParameters();
                }
                else
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                                    PM_ERR_CL_HART_ADAPTOR);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Invalid Bus Parameters");
                status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                                PM_ERR_CL_HART_ADAPTOR);
            }
        }
    }

    Json::Value param = jsonData;
    if (!status.hasError())
    {
        // Fetch bus parameters from persistence storage.
        status = m_pBusinessLogic->readConfigurationValues(param);
        if (!status.hasError())
        {
            masterType = static_cast<unsigned int>(m_busParams.masterType);

            // Update Json values with user i/p.
            param[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE] = masterType;
            /* No need to re-write Preamble settings, as this will not be
               edited by user */
            //param[HART_BUS_PARAMS_KEY][PREAMBLE] = m_busParams.preamble;

            // To save the parameters in PS
            status = m_pBusinessLogic->saveConfigurationValues(param);
            if(!status.hasError())
            {
                jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
            }
            else
            {
                error = Error::cpm.SET_BUS_PARAM_FAILED;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            error = Error::cpm.SET_BUS_PARAM_FAILED;
            jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        error = Error::cpm.SET_BUS_PARAM_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Fetches default bus parameters to assign default values
*/
void HARTAdaptor::setDefaultBusParameters()
{
    m_busParams.masterType = HartMasterType::HART_SECONDARY_MASTER;
    m_busParams.preamble = DFLT_PREAMBLE;
}

/*  ----------------------------------------------------------------------------
    If user want to restore default bus parameters
*/
PMStatus HARTAdaptor::validateBusParameters()
{
    PMStatus status;

    // checks for valid master type
    if (m_busParams.masterType != HartMasterType::HART_PRIMARY_MASTER &&
            m_busParams.masterType != HartMasterType::HART_SECONDARY_MASTER)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Bus param validity fail "
                              "in validateBusParameters");
        status.setError(
                    HART_ADAPTOR_ERR_INVALID_MASTERTYPE,
                    PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}


/*  ----------------------------------------------------------------------------
    Provide interface function to connection manager to pass the bus
    parameters to HART stack
*/
PMStatus HARTAdaptor::getStackConfiguration()
{
    PMStatus status;
    Json::Value root;

    // Gets the bus parameters from the PS.
    if (nullptr != m_pBusinessLogic)
    {
        status = m_pBusinessLogic->readConfigurationValues(root);
    }
    else
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: m_pBusinessLogic object is"
                              "null in fetchBusParameters");
        status.setError(HART_ADAPTOR_ERR_GET_BUS_PARAM, PM_ERR_CL_HART_ADAPTOR);
    }

    if (!status.hasError())
    {
        if (root.isMember(HART_BUS_PARAMS_KEY))
        {
            if (root[HART_BUS_PARAMS_KEY].isMember(PREAMBLE))
            {
                // Updating the bus param structure with Bus param values in PS.
                m_busParams.preamble = root[HART_BUS_PARAMS_KEY][PREAMBLE].asUInt();
            }

            if (root[HART_BUS_PARAMS_KEY].isMember(HART_MASTER_TYPE))
            {

                unsigned int masterType = root[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asUInt();
                m_busParams.masterType = static_cast<HartMasterType>(masterType);
            }

            if (root[HART_APP_SET_KEY].isMember(POLL_BY_ADDR_OPTION))
            {

                m_appSetting.pollAddrOption =
                        static_cast<PollByAddress>(
                            root[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].asUInt());
            }

        }
        else
        {
            status.setError(HART_ADAPTOR_ERR_GET_BUS_PARAM, PM_ERR_CL_HART_ADAPTOR);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Interface to CPM layer,will return bus parameters in JSON
    DATA (Json::Value) to CPM, if fail return error
*/
Json::Value HARTAdaptor::getBusParameters()
{
    PMStatus status;
    Json::Value returnData;
    Json::Value busParam;

    // Gets the bus parameters from the PS in business logic layer.
    if (nullptr != m_pBusinessLogic)
    {
        status = m_pBusinessLogic->prepareBusParams();
        if (!status.hasError())
        {
            status = m_pBusinessLogic->readConfigurationValues(returnData);
        }
    }

    if (!status.hasError())
    {
        /* Send only HART Bus Param Json structure to UI and ignore others
           as we use single Json file to store HART & FF Bus Param & App settings.*/

        for (Json::ValueConstIterator it = returnData.begin() ;
                                            it != returnData.end(); ++it)
        {
            if (!it.key().isNull() && it.key().isString())
            {
                std::string key = it.key().asString().c_str();

                if (key == HART_BUS_PARAMS_KEY)
                {
                    busParam[HART_BUS_PARAMS_KEY] = *it;
                    break;
                }
            }
        }

        // Check if HART_BUS_PARAMS exists, otherwise set error
        if (busParam.isMember(HART_BUS_PARAMS_KEY))
        {
            returnData = busParam;
        }
        else
        {
            LOGF_ERROR(CPMAPP,"HART_BUS_PARAMS Json structure is not available");
            status.setError(HART_ADAPTOR_ERR_BUS_PARAM_NOT_AVAIL, PM_ERR_CL_HART_ADAPTOR);
        }
    }

    // Return error if status is failure.
    if (!status.hasError() )
    {
        returnData = JsonRpcUtil::getResponseObject(busParam);
    }
    else
    {
        Error::Entry error = Error::cpm.GET_BUS_PARAM_FAILED;
        returnData = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return returnData;
}

/*  ----------------------------------------------------------------------------
    Sets the communication failure
*/
void HARTAdaptor::setCommunicationFailure()
{
}

/*  ----------------------------------------------------------------------------
    Processes the alerts
*/
void HARTAdaptor::processAlerts(void* alerts)
{
    /* Typecasted with void to supress the unused parameter
       warning, must remove when implementation takes place
    */
    (void) alerts;
}

/*  ----------------------------------------------------------------------------
    Processes the alerts
*/
void HARTAdaptor::getDefaultBusParameters(Json::Value& jsonData)
{
    // Assign default values to the hart bus params.

    jsonData[HART_BUS_PARAMS_KEY][PREAMBLE] = DFLT_PREAMBLE;
    jsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE] =
    static_cast<unsigned int>(HartMasterType::HART_SECONDARY_MASTER);
}


/*  ----------------------------------------------------------------------------
   to get the default HART settings if the file is not present in persistent
   storage
*/
void HARTAdaptor::getDefaultAppSettings(Json::Value &jsonData)
{
    // Assign default values to the hart app settings.
    jsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION] =
    static_cast<int>(PollByAddress::ADDRESS_0_15);
}

/*  ----------------------------------------------------------------------------
    Stores the application configuration settings in the persistent storage.
*/
Json::Value HARTAdaptor::setAppSettings(const Json::Value& jsonData)
{
    PMStatus status;
    Json::Value jsonResult;

    if (jsonData.isNull() || nullptr == m_pBusinessLogic)
    {
        LOGF_ERROR(CPMAPP,"APP_SET_ERROR: json or BusinessLogic object is"
                          " null in setAppSettings");
        status.setError(HART_ADAPTOR_ERR_JSON_PARSE, PM_ERR_CL_HART_ADAPTOR);
    }

    if (!status.hasError())
    {
        /* Set default app settings if root["set_default"] is
           set to 1 from UI
        */

        if (jsonData.isMember(HART_APP_SET_KEY) && jsonData[HART_APP_SET_KEY].isMember(SET_DEFLT))
        {
            if (jsonData[HART_APP_SET_KEY][SET_DEFLT].isIntegral() &&
                jsonData[HART_APP_SET_KEY][SET_DEFLT].asLargestInt() >= 0 &&
                DEFAULT_VAL == jsonData[HART_APP_SET_KEY][SET_DEFLT].asUInt())
            {
                // Sets the default app settings.
                setDefaultAppSettings();
            }
            else
            {
                status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                                PM_ERR_CL_HART_ADAPTOR);
            }
        }
        else if (jsonData.isMember(HART_APP_SET_KEY) && jsonData[HART_APP_SET_KEY].isMember(POLL_BY_ADDR_OPTION))
        {
            /* Assign and validating the polling address option recived from
               user/UI.
            */
            if (jsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].isIntegral() &&
                jsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].asLargestInt() >= 0)
            {
                m_appSetting.pollAddrOption =
                        static_cast<PollByAddress>(jsonData[HART_APP_SET_KEY]
                                                   [POLL_BY_ADDR_OPTION].asUInt());
            }
            else
            {
                status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                                PM_ERR_CL_HART_ADAPTOR);
            }

            // Validates the configuration against the user modified app settings
            status = validateAppSettings();
        }
        else
        {
            status.setError(HART_ADAPTOR_ERR_INVALID_JSON_REQUEST,
                            PM_ERR_CL_HART_ADAPTOR);
        }
    }

    if (!status.hasError() && nullptr != m_pBusinessLogic)
    {
        Json::Value param;
        // Fetch bus parameters from persistence storage.
        status = m_pBusinessLogic->readConfigurationValues(param);
        if (!status.hasError())
        {
            // Update Json values with user i/p.
            param[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION] =
                    static_cast<int>(m_appSetting.pollAddrOption);

            // To save the parameters in PS
            status = m_pBusinessLogic->saveConfigurationValues(param);
        }
    }

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.SET_APP_SETTINGS_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Retrieves application configuration settings from the persistent storage
*/
Json::Value HARTAdaptor::getAppSettings()
{
    PMStatus status;
    Json::Value returnData;
    Json::Value appSettings;

    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    //Preparing App settings and Bus parameters
    m_pBusinessLogic->prepareBusParams();

    // Gets the app settings from the PS in business logic layer.
    if (nullptr != m_pBusinessLogic)
    {
        status = m_pBusinessLogic->readConfigurationValues(returnData);
    }

    if (!status.hasError() )
    {
        /* Send only HART App settings Json structure to UI and ignore others
           as we use single Json file to store HART & FF Bus Param & App settings.
        */
        for (Json::ValueConstIterator it = returnData.begin(); it != returnData.end(); ++it)
        {
            if (!it.key().isNull() && it.key().isString())
            {
                std::string key = it.key().asString().c_str();

                if (key == HART_APP_SET_KEY)
                {
                    appSettings[HART_APP_SET_KEY] = *it;
                    break;
                }
            }
        }

        // check if HART_APP_SETTINGS exists, otherwise set error
        if (appSettings.isMember(HART_APP_SET_KEY))
        {
            returnData = appSettings;
        }
        else
        {
            LOGF_ERROR(CPMAPP,"HART_APP_SETTINGS Json structure is not available");
            status.setError(HART_ADAPTOR_ERR_APP_SETTINGS_NOT_AVAIL,
                            PM_ERR_CL_HART_ADAPTOR);
        }
    }

    // return error if status is failure.
    if (!status.hasError())
    {
         returnData = JsonRpcUtil::getResponseObject(appSettings);
    }
    else
    {

        Error::Entry error = Error::cpm.GET_APP_SETTINGS_FAILED;
        returnData = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return returnData;
}

/*  ----------------------------------------------------------------------------
   Retrieves the default values for the application settings.
*/
void HARTAdaptor::setDefaultAppSettings()
{
    // Assign default values to the hart app settings.
    m_appSetting.pollAddrOption = PollByAddress::ADDRESS_0_15;
}

/*  ----------------------------------------------------------------------------
    Validates the configuration against the user modified value.
    If invalid configuration is found, then the error status will be sent.
*/
PMStatus HARTAdaptor::validateAppSettings()
{
    PMStatus status;

    // validates poll address range
    if (m_appSetting.pollAddrOption != PollByAddress::ADDRESS_0
        && m_appSetting.pollAddrOption != PollByAddress::ADDRESS_0_15
        && m_appSetting.pollAddrOption != PollByAddress::ADDRESS_0_63)
    {

        LOGF_ERROR(CPMAPP,"APP_SET_ERROR: validity for app setting failed"
                          "in validateAppSettings");
        status.setError(
                    HART_ADAPTOR_ERR_POLL_OPTION_INVALID,
                    PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Processes Get Item for Method execution
*/
Json::Value HARTAdaptor::processGetMethodExecution(const Json::Value& data)
{
    PMStatus status;
    Json::Value jsonResult = Json::Value("ok");
    Error::Entry error;

    // Start method execution only if no other method execution is running
    if ((METhreadState::STOP == getMethodExecutionThreadState()) &&
        (!(CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_METHOD_EXEC_IN_PROGRESS)))
    {
        if (data.isMember(JSON_ITEM_ID) && data[JSON_ITEM_ID].isIntegral() &&
            (data[JSON_ITEM_ID].asLargestInt() > 0))
        {
            itemID_t itemID = data[JSON_ITEM_ID].asLargestUInt();

            hCmethod* methodInfo = nullptr;

            // Retrives method info given the Item ID
            status =  m_pSDC625Controller->getMethodItem(itemID,
                                                         &methodInfo);

            // Creates method execution thread only for valid method
            if (!status.hasError())
            {
                SDC625Controller::s_hartMEData.methodID = itemID;
                // starts method exeuction thread
                createMethodExecutionThread();
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Method execution failed with error code : "
                          << status.getErrorCode());
                status.setError(HART_ADAPTOR_ERR_GET_METHOD_FAILED, PM_ERR_CL_HART_ADAPTOR);
                error = Error::cpm.METHOD_EXEC_FAILED;
            }
        }
        else
        {
             LOGF_ERROR(CPMAPP, "Method execution failed, Item ID Unavailable / Invalid in the request");
             status.setError(HART_ADAPTOR_ERR_GET_METHOD_FAILED, PM_ERR_CL_HART_ADAPTOR);
             error = Error::cpm.INVALID_JSON_REQUEST;
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Method execution failed, method is already running");
        status.setError(HART_ADAPTOR_ERR_METHOD_ALREADY_RUNNING, PM_ERR_CL_HART_ADAPTOR);
        error = Error::cpm.METHOD_EXEC_FAILED;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    This function returns parameter cache instance
*/
ParameterCache *HARTAdaptor::getParameterCacheInstance()
{
    return m_pParameterCache;
}

/*  ----------------------------------------------------------------------------
    Execute pre/post actions
*/
PMStatus HARTAdaptor::processExecuteEditMethod(itemID_t itemID)
{
    PMStatus status;

    // Start method execution only if no other method execution is running
    if (METhreadState::STOP == getMethodExecutionThreadState())
    {

        std::thread METhread(methodActionExecutionThread, this);
        METhread.detach();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Method execution failed, method is already running");
        status.setError(HART_ADAPTOR_ERR_METHOD_ALREADY_RUNNING,
                        PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ---------------------------------------------------------------------------
   Creates Method Execution thread
*/
void HARTAdaptor::createMethodExecutionThread()
{
    setMethodExecutionThreadState(METhreadState::RUN);
    std::thread METhread(methodExecutionThread, this);
    METhread.detach();
}

/*  ---------------------------------------------------------------------------
    Destroyes Method Execution thread
*/
void HARTAdaptor::destroyMethodExecutionThread()
{
    setMethodExecutionThreadState(METhreadState::STOP);
}

/*  ---------------------------------------------------------------------------
    Performs Method Execution thread
*/
void HARTAdaptor::methodExecutionThread(HARTAdaptor* adaptor)
{
    PMStatus status;
    ITEM_ID itemId = SDC625Controller::s_hartMEData.methodID;

    // Set state as method execution is in progress
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);

    status = adaptor->m_pSDC625Controller->executeMethod(itemId);

    // Set state as method execution is in progress
    CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Method Execution failed with error code : "
                  << status.getErrorCode());
        // Send error message to UI if method failed due to some internal error
        adaptor->sendMethodErrorMessage();
        adaptor->destroyMethodExecutionThread();
    }
}

/*  ---------------------------------------------------------------------------
    Performs Method Execution thread for Variable Actions
*/
void HARTAdaptor::methodActionExecutionThread(HARTAdaptor* adaptor)
{
    PMStatus status;
    Json::Value jsonResultObject = JsonRpcUtil::getResponseObject(Json::Value("ok")) ;

    ITEM_ID itemId = SDC625Controller::s_hartVarActionData.itemID;

    if (nullptr == adaptor)
    {
        LOGF_ERROR(CPMAPP, "Adaptor instance is null, action cannot be executed.");
        return;
    }

    if (SDC625Controller::s_hartVarActionData.actionItemType == EditActionItemTypes::VARIABLE_ACTION)
    {
        if (SDC625Controller::s_hartVarActionData.actionType == EditActionTypes::PRE_EDIT_ACTION)
        {
            status = adaptor->m_pSDC625Controller->preEditActions(itemId);
        }
        else if (SDC625Controller::s_hartVarActionData.actionType == EditActionTypes::POST_EDIT_ACTION)
        {
            status = adaptor->m_pSDC625Controller->postEditActions(itemId);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid action type, action cannot be executed.");
            return;
        }
    }
    else if (SDC625Controller::s_hartVarActionData.actionItemType == EditActionItemTypes::EDIT_DISPLAY_ACTION)
    {
        if (SDC625Controller::s_hartVarActionData.actionType == EditActionTypes::PRE_EDIT_ACTION)
        {
            status = adaptor->m_pSDC625Controller->preEditDispActions(itemId);
        }
        else if (SDC625Controller::s_hartVarActionData.actionType == EditActionTypes::POST_EDIT_ACTION)
        {
            status = adaptor->m_pSDC625Controller->postEditDispActions(itemId);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid action type, action cannot be executed.");
            return;
        }
    }

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Method Execution failed with error code : " << status.getErrorCode());
        // Send error message to UI if method failed due to some internal error

        status.setError(HART_ADAPTOR_ERR_EDIT_ACTION_FAILED, PM_ERR_CL_HART_ADAPTOR);
		Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
		jsonResultObject= JsonRpcUtil::setErrorObject(error.code, error.message);

        //adaptor->sendMethodErrorMessage();
        adaptor->destroyMethodExecutionThread();
    }

    // preapre JSON for pre post edit action
    Json::Value jsondata;
	VariableType varType(itemId);

	std::string publisher = STATUS_PUBLISHER;

	jsondata[JSON_ITEM_STATUS_CLASS] = JSON_EDIT_ACTION;

	jsondata[JSON_ITEM_INFO][JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(itemId);
	jsondata[JSON_ITEM_INFO][JSON_VAR_CONT_ID] = static_cast<Json::Value::LargestUInt>(varType.containerId);
	jsondata[JSON_ITEM_INFO][JSON_VAR_SUBINDEX] = varType.subIndex;
	jsondata[JSON_ITEM_STATUS] = jsonResultObject;

	CommonProtocolManager::getInstance()->notifyMessage(publisher, jsondata);
}


/*  ---------------------------------------------------------------------------
    Sets the Method Execution thread state
*/
void HARTAdaptor::setMethodExecutionThreadState(METhreadStateT eThreadState)
{
    m_pSDC625Controller->m_methodExecutionLock.lock();
    m_eThreadState = eThreadState;
    m_pSDC625Controller->m_methodExecutionLock.unlock();
}

/*  ---------------------------------------------------------------------------
    Retrieves the current state of Method Execution thread
*/
METhreadState HARTAdaptor::getMethodExecutionThreadState()
{
    return m_eThreadState;
}

/*  ----------------------------------------------------------------------------
    Processes Set Item for Method execution
*/
Json::Value HARTAdaptor::processSetMethodExecution(const Json::Value& data)
{
    Json::Value jsonResult = Json::Value("ok");

    // Abort the execution of method
    SDC625Controller::s_hartMEData.isAborted = false;

    if (data.isMember(JSON_ITEM_REQ_TYPE) && data[JSON_ITEM_REQ_TYPE].isString())
    {
        std::string itemReqType = data[JSON_ITEM_REQ_TYPE].asString();
        if (itemReqType.compare(JSON_METHOD_NOTIFY_ABORT) == 0)
        {
            // Abort the execution of method
            SDC625Controller::s_hartMEData.isAborted = true;
        }
        else
        {
            if (data.isMember(JSON_ITEM_INFO) &&
                data[JSON_ITEM_INFO].isMember(JSON_VAR_VALUE))
            {
                PMStatus status = setMethodData(data[JSON_ITEM_INFO]);
                if (status.hasError())
                {
                    LOGF_ERROR(CPMAPP, "invalid user input");

                    //resumes subscription
                    m_pParameterCache->resumeSubscription();

                    Error::Entry error = Error::cpm.INVALID_VARIABLE_VALUE;
                    return JsonRpcUtil::setErrorObject(error.code, error.message);
                }
            }
            else
            {
                // Resume Subscription
				m_pParameterCache->resumeSubscription();

                LOGF_ERROR(CPMAPP, "Invalid JSON : Item Info or Var Value missing");
                Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }

        SDC625Controller::s_hartMEData.isItemSet = true;
        // Unlocks the ME thread and notify that data has arrived
        m_pSDC625Controller->releaseLock();
    }
    else
    {
        //resumes subscription
        m_pParameterCache->resumeSubscription();

        LOGF_ERROR(CPMAPP, "Invalid JSON : Missing or Invalid request type !");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    setMethodData function is responsible to get data from UI layer and send to
    DDS core
*/
PMStatus HARTAdaptor::setMethodData(Json::Value itemInfo)
{
    PMStatus status;
    bool errorCode = FAILURE;

    UI_DATA_TYPE dataType =
        SDC625Controller::s_hartMEData.userInputType.userInterfaceDataType;

    if (dataType == EDIT ||
        dataType == HARTDATE ||
        dataType == TIME)
    {
        CValueVarient editBoxValue;
        switch(SDC625Controller::s_hartMEData.userInputType.EditBox.editBoxType)
        {
            case EDIT_BOX_TYPE_INTEGER:
            {
                long long tempValue = 0;
                try
                {
                    // For validation of range, verified with value -62362362889
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();
                }
                catch(...)
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_HART_ADAPTOR);
                    return status;
                }

                if (validateIntegerRange<int, long>(tempValue))
                {
                    editBoxValue.vType = editBoxValue.valueType_t::isVeryLong;
                    editBoxValue.vValue.longlongVal = tempValue;
                    errorCode = SUCCESS;
                }
            }
            break;

            case EDIT_BOX_TYPE_FLOAT:
            {
                double tempValue = 0;
                try
                {
                    // For validation of range, verified with value 632732323652361251236156125126351236.3843282643643;
                    tempValue = itemInfo[JSON_VAR_VALUE].asDouble();
                }
                catch(...)
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_HART_ADAPTOR);
                    return status;
                }

                if ((tempValue >= std::numeric_limits<float>::lowest())
                        && (tempValue <= std::numeric_limits<float>::max()))
                {
                    float value = itemInfo[JSON_VAR_VALUE].asFloat();
                    editBoxValue.vType = editBoxValue.valueType_t::isFloatConst;
                    editBoxValue.vValue.fFloatConst = value;
                    errorCode = SUCCESS;
                }
            }
            break;

            case EDIT_BOX_TYPE_DATE:
            {
                unsigned long long tempValue = 0;
                /* converting a negative value to largest UInt may crash the application
                * handle the exception and return corresponding error
                */
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_HART_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    string dateString = m_pSDC625Controller->convertsEpochTimeToUserTime(
                                tempValue, DATE_DATA_FORMAT);
                    wstring str(dateString.length(), L' ');
                    std::copy(dateString.begin(), dateString.end(), str.begin());
                    editBoxValue = str;
                    errorCode = SUCCESS;
                }
            }
            break;
            case EDIT_BOX_TYPE_TIME:
            {
                unsigned long long tempValue = 0;
                /* converting a negative value to largest UInt may crash the application
                 * handle the exception and return corresponding error
                 */
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_HART_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    hCVar* pVar = SDC625Controller::s_hartMEData.userInputData.pVar4ItemID;
                    if (nullptr != pVar)
                    {
                        hCTimeValue* time = (hCTimeValue*)pVar;
                        double scaling_fact = 1;
                        editBoxValue.vType = editBoxValue.valueType_t::isVeryLong;
                        if (nullptr != time)
                        {
                            if (time->isTimeScale())
                            {
                                scaling_fact = time->GetScalingFactor();
                                editBoxValue.vValue.longlongVal = lround(tempValue * scaling_fact);
                            }
                            else
                            {
                                editBoxValue.vValue.longlongVal = tempValue;
                            }
                            errorCode = SUCCESS;
                        }
                    }
                }
            }
            break;
            case EDIT_BOX_TYPE_STRING:
            case EDIT_BOX_TYPE_PASSWORD:
            {
                try
                {
                    string strValue = itemInfo[JSON_VAR_VALUE].asString();

                    // Convert UTF8 string to ISO-Latin-1 before writing the value to device
                    size_t utfStrLen = strValue.length();
                    char* utf8Str = (char *) calloc ((utfStrLen + 1), sizeof(char));
                    if (nullptr == utf8Str)
                    {
                        LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }

                    size_t isoLatinStrLen = (utfStrLen * 2) + 1;
                    char* isoLatinStr = (char *) calloc (isoLatinStrLen, sizeof(char));
                    if (nullptr == isoLatinStr)
                    {
                        LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }

                    strncpy(utf8Str, strValue.c_str(), strValue.length());

                    char *tempUtf8Str = utf8Str;
                    char *tempIsoLatintStr = isoLatinStr;
                    bool convRetVal = CPMUtil::convertUtf8ToIsolatin1 (&tempUtf8Str, &utfStrLen,
                                                    &tempIsoLatintStr, &isoLatinStrLen);

                    if (true == convRetVal)
                    {
                        strValue = isoLatinStr;
                        //  Free-up the memory
                        free (isoLatinStr);
                        free (utf8Str);
                    }
                    else
                    {
                        LOGF_ERROR (CPMAPP, "Cannot convert UTF8 character to ISO-Latin-1");
                        //  Free-up the memory
                        free (isoLatinStr);
                        free (utf8Str);
                        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_HART_ADAPTOR);
                        return status;
                    }
                    editBoxValue = strValue;
                    errorCode = SUCCESS;
                }
                catch(...)
                {
                    status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_HART_ADAPTOR);
                    return status;
                }
            }
            break;

            case UNKNOWN_EDIT_BOX_TYPE:
            default:
            {
                LOGF_ERROR(CPMAPP,"Invalid Data Type.. ");
            }
            break;
        }

        SDC625Controller::s_hartMEData.userInputType.EditBox.editBoxValue =
                editBoxValue;
    }
    else if (dataType == COMBO)
    {
        unsigned long long tempValue = 0;
        /* converting a negative value to largest UInt may crash the application
         * handle the exception and return corresponding error
         */
        try
        {
            // For validation of range, verified with value 712376526151635
            tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
        }
        catch(...)
        {
            status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                    PM_ERR_CL_HART_ADAPTOR);
            return status;
        }
        // For validation of range, verified with value 712376526151635
        if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
        {
            SDC625Controller::s_hartMEData.userInputType.nComboSelection = tempValue;
            errorCode = SUCCESS;
        }
    }
    else
    {
        errorCode = SUCCESS;
    }

    if (FAILURE == errorCode)
    {
        LOGF_ERROR(CPMAPP, "Invalid input value for variable: " << dataType);
        status.setError(HART_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                    PM_ERR_CL_HART_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    sendMessage function is responsible for delivering message to UI layer
*/
void HARTAdaptor::sendMethodUIMessage(string response, string notificationType)
{
    Json::Value jsondata;

    ItemBasicInfo methBasicInfo{};
    methBasicInfo.itemID = SDC625Controller::s_hartMEData.methodID;
    methBasicInfo.itemHelp = SDC625Controller::s_hartMEData.methodHelp;
    methBasicInfo.itemLabel = SDC625Controller::s_hartMEData.methodName;
    methBasicInfo.itemType = JSON_METHOD_ITEM_TYPE;
    methBasicInfo.itemContainerTag = "";
    // Fill the basic information in JsonResponse
    fillItemBasicJsonInfo(methBasicInfo, jsondata);

    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] = notificationType;
    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = response;

    // Sends method state for Abort status to UI
    if (SDC625Controller::s_hartMEData.isAborted)
    {
        jsondata[JSON_METHOD_STATE] = TRUE;
    }

    // notify CPM with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr, jsondata);
}

/*  ----------------------------------------------------------------------------
    Sends error message if any error occurred during method execution to
    UI layer using ITEMDATA publisher
*/
void HARTAdaptor::sendMethodErrorMessage()
{
    Json::Value errorJson;

    Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
    errorJson = JsonRpcUtil::setErrorObject(error.code, error.message);
    errorJson[JSON_ITEM_TYPE] = JSON_METHOD_ITEM_TYPE;

    // Sends method state for Abort status to UI
    if (SDC625Controller::s_hartMEData.isAborted)
    {
        errorJson[JSON_METHOD_STATE] = TRUE;
    }

    // notify UI with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr, errorJson);
    destroyMethodExecutionThread();
}

/*  ----------------------------------------------------------------------------
    getMethodData function is responsible to get data from to UI layer or User
*/
PMStatus HARTAdaptor::getMethodData(std::string message,
                                    ACTION_UI_DATA structMethodsUIData)
{
    PMStatus status;
    Json::Value methJsonResp;
    ParamInfo paramInfo;
    VariableType varType{};
    varType.itemType = PARAM_VARIABLE;

    if (nullptr != structMethodsUIData.pVar4ItemID)
    {
        varType.itemID = structMethodsUIData.DDitemId;
        paramInfo.paramList.push_back(varType);

        status = m_pParameterCache->getParameter(&paramInfo);

        if (!status.hasError())
        {
            status = m_pParameterCache->readValue(&paramInfo);
            varType = dynamic_cast<VariableType&>(paramInfo.paramList.at(0).get());
        }
    }
    else
    {
        status = getMethodLocalVar(structMethodsUIData.EditBox,
                                   varType);
    }
    // Fill itemBasicInfo structure
    MethodBasicInfo methBasicInfo{};
    methBasicInfo.basicInfo.itemID = SDC625Controller::s_hartMEData.methodID;
    methBasicInfo.basicInfo.itemHelp = SDC625Controller::s_hartMEData.methodHelp;
    methBasicInfo.basicInfo.itemLabel = SDC625Controller::s_hartMEData.methodName;
    methBasicInfo.basicInfo.itemType = JSON_METHOD_ITEM_TYPE;
    methBasicInfo.itemReadOnly = false;
    methBasicInfo.basicInfo.itemContainerTag = "";
    methBasicInfo.itemClass = 0;
    // Fill the basic information in JsonResponse
    fillItemBasicJsonInfo(methBasicInfo.basicInfo, methJsonResp);

    methJsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = static_cast<Json::Value::LargestUInt>(methBasicInfo.itemClass);
    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] =
            JSON_METHOD_NOTIFY_GETVALUE;

    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = message;

    Json::Value varJsonResp;
    // Fill itemBasicInfo structure
    VarBasicInfo varBasicInfo{};
    varBasicInfo.basicInfo.itemID = structMethodsUIData.DDitemId;
    varBasicInfo.basicInfo.itemHelp = validateStringValue(varType.help);
    varBasicInfo.basicInfo.itemLabel = validateStringValue(varType.label);

    varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;

    // Set readonly state and class attribute using varParam variable;
    varBasicInfo.itemReadOnly = false;
    varBasicInfo.itemClass = 0;

    varBasicInfo.basicInfo.itemContainerTag = "";
    // Fill the basic information in JsonResponse
    fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

    // Fill the variable attribute information
    fillVariableJsonInfo(&varType, varJsonResp);

    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_VAR_INFO] = varJsonResp;

    // Sends method state for Abort status to UI
    if (SDC625Controller::s_hartMEData.isAborted)
    {
        methJsonResp[JSON_METHOD_STATE] = TRUE;
    }

    // notify CPM with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr,
                                                        methJsonResp);
    // paramlist is stack memory just clear the list
    //paramInfo.paramList.clear();
    m_pParameterCache->cleanParamInfo(&paramInfo);

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves the method local variable information
*/
PMStatus HARTAdaptor::getMethodLocalVar(UI_DATA_EDIT_BOX localVar,
                                        VariableType& varType)
{
    PMStatus status;

    switch (localVar.editBoxType)
    {
        case EDIT_BOX_TYPE_INTEGER:
           varType.type = vT_Integer;
        break;
        case EDIT_BOX_TYPE_FLOAT:
           varType.type =  vT_FloatgPt;
        break;
        case EDIT_BOX_TYPE_STRING:
           varType.type = vT_VisibleString;
        break;
        case EDIT_BOX_TYPE_PASSWORD:
           varType.type = vT_Password;
        break;
        case EDIT_BOX_TYPE_TIME:
           varType.type = vT_Time;
        break;
        case EDIT_BOX_TYPE_DATE:
           varType.type = vT_HartDate;
        break;
    default:
         LOGF_ERROR(CPMAPP, "Unknown local variable type!!");
        break;

    }
    m_pParameterCache->setValuesToParamInfo(localVar.editBoxValue,
                                            &varType);

    wstring wSeditFormat = localVar.strEdtFormat;
    std::string sEditFormat( wSeditFormat.begin(), wSeditFormat.end());
    if (!sEditFormat.empty())
    {
        m_pParameterCache->assignStringValue(varType.editFormat, sEditFormat);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    send list of options message to user and get selected option from user
*/
void HARTAdaptor::fillMethodEnumValAsJson(std::string message,
                                    ACTION_UI_DATA structMethodsUIData)
{
    std::map<int, std::string> enumMap;
    string sOptions = "";

    if (structMethodsUIData.ComboBox.pchComboElementText != nullptr)
    {
        wstring wsOptions(structMethodsUIData.ComboBox.pchComboElementText);

        // converts Unicode to UTF8
        sOptions = TStr2AStr(wsOptions);
    }



    // Key should start with 1, because the enumeration value start with 1
    int key = 1;
    std::size_t startPos = 0;
    std::size_t endPos = 0;
    // Parse the selection and add into enumeration map
    while (key <= structMethodsUIData.ComboBox.iNumberOfComboElements)
    {
        std::string item;
        endPos = sOptions.find(";", startPos);

        if (endPos != std::string::npos)
        {
            item = sOptions.substr(startPos, endPos-startPos);
            // Increment by 1, because index value to match the enumeration
            startPos = endPos + 1;
        }
        else
        {
            item = sOptions.substr(startPos);
        }

        enumMap.insert(std::pair<int, std::string>(key, item));
        ++key;
    }

    Json::Value methJsonResp;

    MethodBasicInfo methBasicInfo{};
    methBasicInfo.basicInfo.itemID = SDC625Controller::s_hartMEData.methodID;
    methBasicInfo.basicInfo.itemHelp = SDC625Controller::s_hartMEData.methodHelp;
    methBasicInfo.basicInfo.itemLabel = SDC625Controller::s_hartMEData.methodName;
    methBasicInfo.basicInfo.itemType = JSON_METHOD_ITEM_TYPE;
    methBasicInfo.itemReadOnly = false;
    methBasicInfo.itemClass = 0;
    methBasicInfo.basicInfo.itemContainerTag = "";

    // Fill the basic information in JsonResponse
    fillItemBasicJsonInfo(methBasicInfo.basicInfo, methJsonResp);

    methJsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = static_cast<Json::Value::LargestUInt>(methBasicInfo.itemClass);

    if (nullptr != structMethodsUIData.pVar4ItemID)
    {
        methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE]
                = JSON_METHOD_NOTIFY_GETVALUE;
    }
    else
    {
         methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] =
                 JSON_METHOD_NOTIFY_SELECTION;
    }

    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = message;

    Json::Value varJsonResp;
    VarBasicInfo varBasicInfo{};

    if (nullptr != structMethodsUIData.pVar4ItemID)
    {
        varBasicInfo.basicInfo.itemID = structMethodsUIData.pVar4ItemID->getID();
        std::string label;
        structMethodsUIData.pVar4ItemID->Label(label);
        std::string help;
        structMethodsUIData.pVar4ItemID->Help(help);
        varBasicInfo.basicInfo.itemHelp = help;
        varBasicInfo.basicInfo.itemLabel = label;
    }

    varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
    varBasicInfo.itemReadOnly = false;
    varBasicInfo.itemClass = 0;
    varBasicInfo.basicInfo.itemContainerTag = "";

    // Fill the basic information in JsonResponse
    fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

    varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ENUM;

    if (structMethodsUIData.ComboBox.comboBoxType ==
            COMBO_BOX_TYPE_SINGLE_SELECT)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VAL_TYPE] =
                JSON_VAR_TYPE_ENUM;
    }
    else
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VAL_TYPE] =
                JSON_VAR_VAL_TYPE_BITENUM;
    }

    // Increment by 1, because index value to match the enumeration
    varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VALUE] =
            static_cast<Json::Value::LargestUInt>(structMethodsUIData.ComboBox.nCurrentIndex + 1);


    std::map<int, std::string>::const_iterator it;
    for (it = enumMap.begin(); it != enumMap.end(); ++it)
    {
        Json::Value enumdata;
        enumdata[JSON_VAR_ENUM_VALUE] = it->first;
        enumdata[JSON_VAR_ENUM_LABEL] = it->second;
        enumdata[JSON_VAR_ENUM_HELP] = "";
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_ENUM_INFO]
                .append(enumdata);
    }

    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_VAR_INFO] = varJsonResp;

    // Sends method state for Abort status to UI
    if (SDC625Controller::s_hartMEData.isAborted)
    {
        methJsonResp[JSON_METHOD_STATE] = TRUE;
    }

    // notify CPM with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr,
                                                        methJsonResp);
}

/*  ----------------------------------------------------------------------------
    Apply the qualifiers for Variables
*/
void HARTAdaptor::applyQualifiersForVariables(unsigned short qualifier,
                                              Json::Value & itemJsonInfo)
{
    // Check for Read Only qualifier
    if (qualifier & READ_ONLY_ITEM)
    {
        itemJsonInfo[JSON_ITEM_INFO][JSON_ITEM_READONLY] = true;
    }

    // Check for Label Qualifier
    if (qualifier & NO_LABEL_ITEM)
    {
        itemJsonInfo[JSON_ITEM_LABEL] = "";
    }

    // Check for Unit Qualifier
    if (qualifier & NO_UNIT_ITEM)
    {
        itemJsonInfo[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_UNIT] = "";
    }
}

/*  --------------------------------------------------------------------l--------
    Utility function for input validation for the variables
*/
template<typename RangeType, typename ValueType>
bool HARTAdaptor::validateIntegerRange(ValueType value)
{
    // checking if variable is float and double type
    if (!numeric_limits<RangeType>::is_integer)
    {
        return false;
    }

    // checking for signed integer
    if (numeric_limits<RangeType>::is_signed ==
         numeric_limits<ValueType>::is_signed )
    {
        return ((value >= numeric_limits<RangeType>::min()) &&
               (value <= numeric_limits<RangeType>::max()));
    }
    else if (numeric_limits<RangeType>::is_signed )
    {
        return (value <= numeric_limits<RangeType>::max());
    }
    else
    {
        return ((value >= 0) && (value <= numeric_limits<RangeType>::max()));
    }
}


/*  ----------------------------------------------------------------------------
    Utility function for input validation with variable size
*/
int HARTAdaptor::validateVariableLimit
(
    const Json::Value& itemInfo,
    VariableType* varParam
)
{
    int errorCode = SUCCESS;

    // check if var size is less than and equal to 2
    if (varParam->size <= VAR_SIZE_TWO)
    {
        switch (varParam->type)
        {
            case vT_Integer:
            {
                long tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();

                if (varParam->size == VAR_SIZE_ONE)
                {
                    if (!((tempValue >= SCHAR_MIN) &&
                            (tempValue <= SCHAR_MAX)))
                    {
                        errorCode = FAILURE;
                    }
                }
                else if (varParam->size == VAR_SIZE_TWO)
                {
                    if (!((tempValue >= SHRT_MIN) &&
                        (tempValue <= SHRT_MAX)))
                    {
                        errorCode = FAILURE;
                    }
                }
            }
            break;
            case vT_Enumerated:
            case vT_BitEnumerated:
            case vT_Unsigned:
            case vT_Boolean:
            case vT_MaxType:
            case vT_Index:
            {
                unsigned long long tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();

                if (varParam->size == VAR_SIZE_ONE)
                {
                    if (!((tempValue >= MIN_LIMIT) &&
                            (tempValue <= UCHAR_MAX)))
                    {
                        errorCode = FAILURE;
                    }
                }
                else if (varParam->size == VAR_SIZE_TWO)
                {
                    if (!((tempValue >= MIN_LIMIT) &&
                        (tempValue <= USHRT_MAX)))
                    {
                        errorCode = FAILURE;
                    }
                }
            }
            break;

            default:
            {
                 LOGF_INFO(CPMAPP,"SET_VARIABLE: VAR size is not in this scope");
            }
            break;
        }
    }
    return errorCode;
}

/*  ----------------------------------------------------------------------------
    Validates Item has relation
*/
int HARTAdaptor::validateItemHasRelation
(
    const Json::Value& jsonRequest,
    bool& itemHasRelation
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_ITEM_HAS_RELATION) &&
        jsonRequest[JSON_ITEM_HAS_RELATION].isBool())
    {
        itemHasRelation = jsonRequest[JSON_ITEM_HAS_RELATION].asBool();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Checks whether float value is not a number     
*/
bool HARTAdaptor::isNAN(float floatVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isnan(floatVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isnan(floatVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;
}

/*  ----------------------------------------------------------------------------
    Checks whether double value is not a number     
*/
bool HARTAdaptor::isNAN(double doubleVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isnan(doubleVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isnan(doubleVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;
}

/*  ----------------------------------------------------------------------------
    Checks whether float value is infinity     
*/
bool HARTAdaptor::isINF(float floatVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isinf(floatVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isinf(floatVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;

}

/*  ----------------------------------------------------------------------------
    Checks whether double value is infinity     
*/
bool HARTAdaptor::isINF(double doubleVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isinf(doubleVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isinf(doubleVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;

}

/*  ----------------------------------------------------------------------------
    Validates the range of Non Standard Integer data
*/
bool HARTAdaptor::validateNonStandardIntData(unsigned short size, long long value)
{
    bool bValidity = false;

    switch(size)
    {
        case THREE_BYTES:
        {
            if (value >= INT_SIZE_3_MIN && value <= INT_SIZE_3_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case FIVE_BYTES:
        {
            if (value >= INT_SIZE_5_MIN && value <= INT_SIZE_5_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case SIX_BYTES:
        {
            if (value >= INT_SIZE_6_MIN && value <= INT_SIZE_6_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case SEVEN_BYTES:
        {
            if (value >= INT_SIZE_7_MIN && value <= INT_SIZE_7_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case EIGHT_BYTES:
        {
            if (validateIntegerRange<long long, long long>(value))
            {
                bValidity = true;
            }
        }
        break;
        default:
        break;
    }
    return bValidity;
}

/*  ----------------------------------------------------------------------------
    Validates the range of Non Standard Unsigned Integer data
*/
bool HARTAdaptor::validateNonStandardUIntData(unsigned short size, unsigned long long value)
{
    bool bValidity = false;

    switch(size)
    {
        case THREE_BYTES:
        {
            if (value <= UINT_SIZE_3_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case FIVE_BYTES:
        {
            if (value <= UINT_SIZE_5_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case SIX_BYTES:
        {
            if (value <= UINT_SIZE_6_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case SEVEN_BYTES:
        {
            if (value <= UINT_SIZE_7_MAX)
            {
                bValidity = true;
            }
        }
        break;
        case EIGHT_BYTES:
        {
            if (validateIntegerRange<unsigned long long, unsigned long long>(value))
            {
                bValidity = true;
            }
        }
        break;
        default:
        break;
    }
    return bValidity;
}

/*  ----------------------------------------------------------------------------
    Stops method execution
*/
void HARTAdaptor::stopMethodExec()
{
    while((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_METHOD_EXEC_IN_PROGRESS))
    {    
        LOGF_DEBUG(CPMAPP, "Stopping current method execution");
        // Abort the execution of method
        SDC625Controller::s_hartMEData.isAborted = true;
        SDC625Controller::s_hartMEData.isItemSet = true;
        // Unlocks the ME thread and notify that data has arrived
        m_pSDC625Controller->releaseLock();
        std::this_thread::sleep_for (std::chrono::milliseconds(METHOD_EXEC_SLEEP_INTERVAL));
    }
    LOGF_DEBUG(CPMAPP, "Method execution stopped!!");
}

/*  ----------------------------------------------------------------------------
    Validates Item in DD input
*/
int HARTAdaptor::validateItemInDD
(
    const Json::Value& jsonRequest, 
    bool requiredState
)
{
    int status = FAILURE;
    if (jsonRequest.isMember(JSON_ITEM_IN_DD) &&
            jsonRequest[JSON_ITEM_IN_DD].isBool())
    {
        if (requiredState == jsonRequest[JSON_ITEM_IN_DD].asBool())
        {
            status = SUCCESS;
        }
    }
    return status;
}
