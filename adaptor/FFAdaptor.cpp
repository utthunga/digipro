/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    FF Adaptor class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <string>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include "FFAdaptor.h"
#include "json/json.h"
#include "JsonRpcUtil.h"
#include "CPMUtil.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include "error/CpmErrors.h"
#include "stacktrace.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* Constants and Macros *******************************************************/

/* ========================================================================== */

/* Global Variables ***********************************************************/

// Standard Device Level Root Menu names
char gRootMenus[][32]{
    "hh_process_variables_root_menu",
    "hh_device_root_menu",
    "hh_diagnostic_root_menu",
    "process_variables_root_menu_hh",
    "device_root_menu_hh",
    "diagnostic_root_menu_hh"
};

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs FFAdaptor Class
*/
FFAdaptor::FFAdaptor()
    // TODO(Santhosh): Need to handle with threading mechanism implementation
   :m_pDDSController(nullptr),
    m_pFFConnectionManager(nullptr),
    m_pFFParameterCache(nullptr),
    m_pBusinessLogic(nullptr),
    m_unScanSeqID(0),
    m_methodThreadId(0),
    m_internalSubscriptionRate(0)
{
    memset(static_cast<void*> (&m_busParams),0,sizeof(m_busParams));

    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    // Set the Protocol Type
    m_Protocol = CPMPROTOCOL::CPMPROTOCOL_FF;
}

/*  ----------------------------------------------------------------------------
    Destructor for FFAdaptor Class
*/
FFAdaptor::~FFAdaptor()
{
    if (nullptr != m_pDDSController)
    {
        delete m_pDDSController;
        m_pDDSController = nullptr;
    }

    if (nullptr != m_pFFConnectionManager)
    {
        delete m_pFFConnectionManager;
        m_pFFConnectionManager = nullptr;
    }

    if (nullptr != m_pFFParameterCache)
    {
        delete m_pFFParameterCache;
        m_pFFParameterCache = nullptr;
    }

    if (nullptr != m_pBusinessLogic)
    {
        delete m_pBusinessLogic;
        m_pBusinessLogic = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Initializes Parameter Cache, Connection Manager and DDS Controller
*/
Json::Value FFAdaptor::initialise()
{
    PMStatus status;

    /* Creates all the required instances of DDS Controller, Parameter
       Cache and Connection manager and injects them to the required objects
    */
    Json::Value jsonResult;

    // Instantiate the Connection Manager, Parameter Cache & DDS Controller
    if (nullptr == m_pFFConnectionManager)
    {
        m_pFFConnectionManager = new FFConnectionManager;
    }
   
    if (nullptr == m_pFFParameterCache)
    {
        m_pFFParameterCache	= new FFParameterCache;
    }

    if (nullptr == m_pDDSController)
    {
        m_pDDSController = new DDSController;
    }

    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    // Check for improper initialisation
    if ((nullptr == m_pFFConnectionManager) ||
        (nullptr == m_pFFParameterCache) ||
        (nullptr == m_pDDSController) ||
        (nullptr == m_pBusinessLogic))
    {
        Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;

        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Read FF attribue information such as dd path, port
    IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();
    if (nullptr != pAppConfigParser)
    {
        std::string traceFileDir = "";
        bool isTraceFile = false;
        std::string traceFileName = ""; 
        
        traceFileDir = pAppConfigParser->getTraceLogPath();
        traceFileName = pAppConfigParser->getTraceFileName();

        if (!traceFileName.empty())
        {
            isTraceFile = true;
        }
    
        if (!CPMUtil::isDirExists(traceFileDir))
        {    
            CPMUtil::createDirectory(traceFileDir);
        }  
        std::string traceFilePath = traceFileDir + "/" + traceFileName;
        if (!createLogFile(FF_PROTOCOL, traceFilePath.c_str(), isTraceFile))
        {
            LOGF_ERROR(CPMAPP, "Failed to create trace file; redirecting stack traces to CPMLOGS");
        }
    }
    
    // Initialise DDS Controller with Connection Manger & Parameter cache
    m_pDDSController->initialize(this, m_pFFConnectionManager,m_pFFParameterCache);

    // Initialise Connection Manager with DDS Controller & Parameter Cache
    m_pFFConnectionManager->initialize(m_pDDSController, m_pFFParameterCache);

    // Initialise Parameter Cache with DDS Controller & Connection Manager
    m_pFFParameterCache->initialize(m_pDDSController, m_pFFConnectionManager, this);

    // Get subscripiton controller instance
    m_pSubscriptionController = m_pFFParameterCache->getSubscriptionController();

    // Prepare Bus Parameters
    status = m_pBusinessLogic->prepareBusParams();

    if (!status.hasError())
    {
        // get the bus parameters from PS, update BUS _PARAm structure with UI values
        status = fetchBusParameters(static_cast<void*>(&m_busParams));
        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Failed to fetch bus params from Adaptor");
            Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
        else
        {
            // Read FF attribue information such as dd path, port
            if (nullptr != pAppConfigParser)
            {
                m_ffDDBinaryPath = pAppConfigParser->getDeviceDescriptionPath();
                m_ffDeviceFile = pAppConfigParser->getDevicePort();
                m_internalSubscriptionRate = pAppConfigParser->getSubscriptionRate();
            }

            status = m_pFFConnectionManager->initStack(&m_busParams, 
                         ((m_ffDDBinaryPath.length() > 0) ? m_ffDDBinaryPath.c_str() : nullptr),
                         ((m_ffDeviceFile.length() > 0) ? m_ffDeviceFile.c_str() : nullptr));

            if (status.hasError())
            {
                Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            else
            {
#ifdef UNITTEST
                m_pDds = m_pDDSController;
                m_pFFCm = m_pFFConnectionManager;
                m_pFFPc = m_pFFParameterCache;
#endif
            }
        }
    }
    else
    {
        Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Add Mapped Methods
    mapItemMethods();

    // Map DDS item Types to Json Item Type Strings
    mapDDSItemTypes();

    // Set the protocol state as initialized
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_PROTOCOL_INITIALIZED);
    // Set language code state
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_LANGUAGE_CODE_SET);

    // Get the positive result json value
    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Shutsdown FF Adaptor
*/
Json::Value FFAdaptor::shutDown()
{
    PMStatus status;
    Json::Value jsonResult;

    if (CPMSTATE_SCANNING_IN_PROGRESS == 
            (CPMSTATE_SCANNING_IN_PROGRESS & CPMStateMachine::getInstance()->getCpmState()))
    {
        status = stopScanThread();
    }
    
    if (CPMSTATE_DEVICE_CONNECTED == 
            (CPMSTATE_DEVICE_CONNECTED & CPMStateMachine::getInstance()->getCpmState()))
    {
        stopMethodExec();
        status = disconnectDevice();
    }

    if (status.hasError())
    {
        status = m_pFFConnectionManager->shutDownStack();
    }

    if (!status.hasError())
    {
		closeLogFile();
		
        CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_PROTOCOL_INITIALIZED);
        // Clear language code set state
        CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_LANGUAGE_CODE_SET);

        // Get the positive result json value
        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        Error::Entry error = Error::cpm.PROTOCOL_SHUTDOWN_FAILED;
        jsonResult =  JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Retrieves current active protocol
*/
CPMPROTOCOL FFAdaptor::getActiveProtocol()
{
    return m_Protocol;
}

/*  ----------------------------------------------------------------------------
    Gets device description header information
*/
Json::Value FFAdaptor::getDDHeaderInfo()
{
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Executes Edit Action for Edit Display
*/
PMStatus FFAdaptor::execEditAction(const Json::Value& data,
                                   EDIT_ACTION_TYPE editActionType,
                                   DDI_GENERIC_ITEM& genericItem,
                                   Error::Entry& error)
{
    PMStatus status;

    if (FAILURE == validateItemInDD(data, true))
    {
        LOG_INFO(CPMAPP, "Edit actions are not applicable for custom menus");
        if (editActionType == EDIT_ACTION_TYPE::PRE_EDIT_ACTION)
        {
            error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
        }
        else
        {
            error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
        }
        status.setError(FF_ADAPTOR_ERR_NO_PRE_POST_EDIT_ACTION_CONFIG,
                        PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    // Get the Item ID
    ITEM_ID itemID = 0;
    if (FAILURE == validateItemID(data, itemID))
    {
        LOGF_ERROR(CPMAPP, "Invalid item ID");
        error = Error::cpm.INVALID_JSON_REQUEST;
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                        PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    // Get the Block tag
    std::string itemContainerTag;
    if (FAILURE == validateItemContTag(data, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container tag");
        error = Error::cpm.INVALID_JSON_REQUEST;
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                        PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    // Get the Item type
    std::string reqType{""};
    if (FAILURE == validateItemType(data, reqType))
    {
        LOGF_ERROR(CPMAPP, "Invalid ItemType");
        error = Error::cpm.INVALID_JSON_REQUEST;
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                        PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    ITEM_TYPE itemType = m_pDDSController->getItemType(itemID, itemContainerTag);
    if (reqType.compare(JSON_MENU_ITEM_TYPE) == 0 && itemType == EDIT_DISP_ITYPE)
    {
        DDI_BLOCK_SPECIFIER ddiBlockSpecifier{};
        DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
        ENV_INFO envInfo{};
        APP_INFO appInfo{};

        // Fill the Item Specifier
        ddiItemSpecifier.type = DDI_ITEM_ID;
        ddiItemSpecifier.item.id = itemID;

        if (itemContainerTag.empty())
        {
            /* Device Level Standard Menu's, are not associated with any blocks,
               needs to be handled seperately
            */
            ENV_INFO2 envInfo2{};
            DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

            // Get the current Device handle
            status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                error = Error::cpm.EDIT_ACTION_FAILED;
                status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                                PM_ERR_CL_FF_ADAPTOR);
                return status;
            }

            // Set the Environment Info for ddi_get_tem2
            envInfo2.device_handle = deviceHandle;
            envInfo2.type = ENV_DEVICE_HANDLE;

            envInfo2.app_info = &appInfo;
            appInfo.status_info = USE_EDIT_VALUE;

            // Deveic Level standard Menu's, hence use ddi_get_item2
            status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                                   ALL_ITEM_ATTR_MASK, &genericItem);
        }
        else
        {
            // Fill the Block Specifier
            ddiBlockSpecifier.type = DDI_BLOCK_TAG;
            ddiBlockSpecifier.block.tag = const_cast<char *>(itemContainerTag.c_str());
            envInfo.app_info = &appInfo;

            // Must be Block Level Menu's - Hence use ddi_get_item
            status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier,
                                                  &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
        }

        if (status.hasError())
        {
            LOG_INFO(CPMAPP, "Error in getting the Item" <<itemID);
            // command response publisher  with no edit action.
            error = Error::cpm.GET_ITEM_FAILED;
            status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                            PM_ERR_CL_FF_ADAPTOR);
            return status;
        }
    }
    else
    {
        LOG_INFO(CPMAPP, "Edit Display has no pre Edit actions with Item ID : " <<itemID);
        if (editActionType == EDIT_ACTION_TYPE::PRE_EDIT_ACTION)
        {
            error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
        }
        else
        {
            error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
        }
        status.setError(FF_ADAPTOR_ERR_NO_PRE_POST_EDIT_ACTION_CONFIG,
                        PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Executes Pre Edit Action for Edit Display
*/
Json::Value FFAdaptor::execPreEditAction(const Json::Value& data)
{
    Json::Value jsonResult;
    PMStatus status;
    DDI_GENERIC_ITEM genericItem{};
    Error::Entry statusError;
    EDIT_ACTION_TYPE actionType = EDIT_ACTION_TYPE::PRE_EDIT_ACTION;

    status = execEditAction(data, actionType, genericItem, statusError);
    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(statusError.code, statusError.message);
    }

    // Get the Block tag
    std::string itemContainerTag;
    if (FAILURE == validateItemContTag(data, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    FLAT_EDIT_DISPLAY * pEditDisplay = static_cast<FLAT_EDIT_DISPLAY *>(genericItem.item);
    if (nullptr != pEditDisplay)
    {
        // check if any pre-edit function is present
        if (pEditDisplay->pre_edit_act.count > NO_ACTION)
        {
            editData.itemId = pEditDisplay->id;
            editData.blockTag = itemContainerTag;
            editData.actionTypes  = EditActionTypes::PRE_EDIT_ACTION;
            editData.actionItemTypes = EditActionItemTypes::EDIT_DISPLAY_ACTION;

             // create edit action thread
            if (THREAD_STATE_INVALID == getEditActionThreadState())
            {
                // command response publisher with message.
                Error::Entry error = Error::cpm.PRE_EDIT_ACTION_IS_IN_PROGRESS;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);

                // create edit action thread
                createEditActionThread();
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Failed to start pre edit Action thread, already running");
                status.setError(FF_ADAPTOR_ERR_INVALID_STATE_OF_PRE_EDIT_ACTION_THREAD,
                                PM_ERR_CL_FF_ADAPTOR);
                Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOG_INFO(CPMAPP,"Edit Display has no pre Edit actions");
            // command response publisher with no edit action.
            Error::Entry error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Executes Post Edit Action for Edit Display
*/
Json::Value FFAdaptor::execPostEditAction(const Json::Value& data)
{
    Json::Value jsonResult;
    PMStatus status;
    DDI_GENERIC_ITEM genericItem{};
    Error::Entry statusError;
    EDIT_ACTION_TYPE actionType = EDIT_ACTION_TYPE::POST_EDIT_ACTION;

    status = execEditAction(data, actionType, genericItem, statusError);
    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(statusError.code, statusError.message);
    }

    // Get the Block tag
    std::string itemContainerTag;
    if (FAILURE == validateItemContTag(data, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    FLAT_EDIT_DISPLAY * pEditDisplay = static_cast<FLAT_EDIT_DISPLAY *>(genericItem.item);
    if (nullptr != pEditDisplay)
    {
        // check if any post edit function is present
        if (pEditDisplay->post_edit_act.count > NO_ACTION)
        {
            editData.itemId = pEditDisplay->id;
            editData.blockTag = itemContainerTag;
            editData.actionTypes  = EditActionTypes::POST_EDIT_ACTION;
            editData.actionItemTypes = EditActionItemTypes::EDIT_DISPLAY_ACTION;

            // create edit action  thread
            if (THREAD_STATE_INVALID == getEditActionThreadState())
            {
                // command response publisher with message "POST_EDIT_ACTION_IN_PROGRESS"
                Error::Entry error = Error::cpm.POST_EDIT_ACTION_IS_IN_PROGRESS;
                jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);

                // createThread for edit actions
                createEditActionThread();
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Failed to start post edit Action thread, already running");
                status.setError(FF_ADAPTOR_ERR_INVALID_STATE_OF_POST_EDIT_ACTION_THREAD,
                                PM_ERR_CL_FF_ADAPTOR);
                Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOG_INFO(CPMAPP,"Edit Display has no post Edit actions");
            // command response publisher with no edit action.
            Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOG_INFO(CPMAPP,"Edit Display has no post Edit actions");
        // command response publisher  with no edit action.
        Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Updates device description data
*/
Json::Value FFAdaptor::updateDD(const Json::Value& data)
{
    (void) data;
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Reads FF basic device data
*/
Json::Value FFAdaptor::getItem(const Json::Value& data)
{
    // Invoke the appropriate method and return the response
	return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::GET_ITEM));
}

/*  ----------------------------------------------------------------------------
   Writes data to FF basic device
*/
Json::Value FFAdaptor::setItem(const Json::Value& data)
{
    // Invoke the appropriate method and return the response
    return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::SET_ITEM));
}

/*  ----------------------------------------------------------------------------
    Processes the subscription for specified item
    Authored By: Utthunga
*/
Json::Value FFAdaptor::processSubscription(const Json::Value& subReq)
{
    Json::Value subResp;

    if (subReq.isMember(JSON_ITEM_SUB_ACTION) && subReq[JSON_ITEM_SUB_ACTION].isString())
    {
        std::string itemAction = subReq[JSON_ITEM_SUB_ACTION].asString();

        if (0 == itemAction.compare(JSON_SUB_CREATE))
        {
            // creates subscription
            subResp = createSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_START))
        {
            // Starts subscription
            subResp = startSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_STOP))
        {
            // Stops subscription
            subResp = stopSubscription(subReq);
        }
        else if (0 == itemAction.compare(JSON_SUB_DELETE))
        {
            // deletes subscription
            subResp = deleteSubscription(subReq);
        }
        else
        {
            // Invalid subscription action type
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        // Invalid request type; return immediately with error!!
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(subResp);
}

/*  ----------------------------------------------------------------------------
    Creates subscription items
    Authored By: Utthunga
*/
Json::Value FFAdaptor::createSubscription(const Json::Value& createSubReq)
{
    Json::Value response;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;
    PMStatus status;

    unsigned int itemRefreshRate = 0;
    ParamInfo monitoredItems{};
    ITEM_ID itemId = 0;
    uint subscriptionId = 0;
    std::string blockTag {""};

    // verifies json keys are present or not
    if (createSubReq.isMember(JSON_ITEM_SUB_REFRESH_RATE) &&
        createSubReq[JSON_ITEM_SUB_REFRESH_RATE].isIntegral() &&
        (createSubReq[JSON_ITEM_SUB_REFRESH_RATE].asLargestInt() > 0) &&
        createSubReq.isMember(JSON_ITEM_LIST))
    {
        itemRefreshRate = createSubReq[JSON_ITEM_SUB_REFRESH_RATE].asUInt();
        for (auto &item : createSubReq[JSON_ITEM_LIST])
        {
            if ((SUCCESS == validateItemContTag(item, blockTag)) &&
                (SUCCESS == validateItemID(item, itemId)))
            {
                auto varType = new VariableType;
                varType->isMemAllocated = true;
                varType->itemType = ParamItemType::PARAM_VARIABLE;
                varType->itemID = itemId;
                varType->itemContainerTag.str = new char[blockTag.length() + 1]();
                varType->itemContainerTag.str[blockTag.length()] = '\0';
                strncpy(varType->itemContainerTag.str,
                        const_cast<char*>(blockTag.c_str()), blockTag.length());
                varType->itemContainerTag.len = blockTag.length();
                varType->itemContainerTag.flag = FREE_STRING;

                if( item[JSON_ITEM_INFO].isMember(JSON_VAR_BIT_INDEX))
                {
                	varType->bitIndex = item[JSON_ITEM_INFO][JSON_VAR_BIT_INDEX].asLargestUInt();
                }

                BLOCK_HANDLE blockHandle = m_pDDSController->getBlockHandleByBlockTag
                                          (varType->itemContainerTag.str);
                if (blockHandle >= 0)
                {
                    Json::Value subItem = item[JSON_ITEM_INFO];
                    if (subItem.isMember(JSON_VAR_CONT_ID) &&
                        subItem[JSON_VAR_CONT_ID].isIntegral() &&
                        (subItem[JSON_VAR_CONT_ID].asLargestInt() >= 0) &&
                        subItem.isMember(JSON_VAR_SUBINDEX) &&
                        subItem[JSON_VAR_SUBINDEX].isIntegral() &&
                        (subItem[JSON_VAR_SUBINDEX].asLargestInt() >= 0))
                    {
                        varType->containerId = subItem[JSON_VAR_CONT_ID].asLargestUInt();
                        varType->subIndex = subItem[JSON_VAR_SUBINDEX].asUInt();
                    }
                    else
                    {
                        //Json attribute missing
                        LOGF_ERROR(CPMAPP, "Missing or Invalid Variable Container ID / Sub Index");
                        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
                        error = Error::cpm.INVALID_JSON_REQUEST;
                        return JsonRpcUtil::setErrorObject(error.code,error.message);
                    }

                    varType->value.stdValue = {INIT_STD_VALUE};

                    status = getSubParamInfo(varType);

                    if (status.hasError())
                    {
                        LOGF_DEBUG(CPMAPP, "get parameter failed");
                        if (nullptr != varType)
                        {
                            delete varType;
                            varType = nullptr;
                        }
                        continue;
                    }

                    monitoredItems.paramList.push_back(*varType);
                }
                else
                {
                    if (nullptr != varType)
                    {
                        delete varType;
                        varType = nullptr;
                    }
                    LOGF_DEBUG(CPMAPP, "Invalid block handle");
                }
            }
            else
            {
                //Json attribute missing
                LOGF_ERROR(CPMAPP, "Missing or Invalid Item ID or Item Container Tag");
                status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
                error = Error::cpm.INVALID_JSON_REQUEST;
                return JsonRpcUtil::setErrorObject(error.code,error.message);
            }
        }
        DataChangedCallback callback = [this](uint subscriptionID,
                ParamInfo* monitorItem)
        {
            Json::Value subJsonData;
            PMStatus status;
            subJsonData[JSON_ITEM_TYPE] = JSON_VARIABLE_ITEM_TYPE;
            subJsonData[JSON_ITEM_SUB_ID] = subscriptionID;

            if (nullptr != monitorItem)
            {
                for (auto& paramItemRef : monitorItem->paramList)
                {
                    auto paramItem = &paramItemRef.get();
                    Json::Value tempJsonData;
                    auto varParam = dynamic_cast<VariableType*>(paramItem);

                    tempJsonData[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(varParam->itemID);

                    DDI_BLOCK_SPECIFIER blockSpec = {};
                    DDI_ITEM_SPECIFIER itemSpec = {};
                    ENV_INFO envInfo = {};
                    DDI_GENERIC_ITEM genericItem = {};

                    itemSpec.type = DDI_ITEM_ID;
                    itemSpec.item.id = varParam->itemID;
                    blockSpec.type = DDI_BLOCK_HANDLE;

                    blockSpec.block.handle = m_pDDSController->getBlockHandleByBlockTag(paramItem->itemContainerTag.str);

                    if (blockSpec.block.handle >= 0)
                    {
                        status = m_pDDSController->ddiGetItem(&blockSpec, &itemSpec, &envInfo, VAR_UNIT,
                                                                                           &genericItem);

                        if (!status.hasError())
                        {
                            FLAT_VAR* flatVar = static_cast<FLAT_VAR*>(genericItem.item);
                            if (nullptr != flatVar)
                            {
                                if (nullptr != flatVar->misc->unit.str && flatVar->misc->unit.len > 0)
                                {
                                    m_pFFParameterCache->assignStringValue(varParam->unit, flatVar->misc->unit);
                                }
                            }

                            fillVariableJsonInfo(varParam, tempJsonData, VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK);
                            subJsonData[JSON_ITEM_LIST].append(tempJsonData);

                            if (nullptr != varParam->unit.str && FREE_STRING == varParam->unit.flag)
                            {
                                // Deallocates memory allocated to string types
                                delete[] varParam->unit.str;
                                varParam->unit.str = nullptr;
                                varParam->unit.len = 0;
                                varParam->unit.flag = DONT_FREE_STRING;
                            }
                        }

                        // Clear the Generic Item memory allocated by ddiGetItem API
                        m_pDDSController->ddiCleanItem(&genericItem);
                    }
                    else
                    {
                        LOGF_DEBUG(CPMAPP, "Invalid block handle");
                    }
                }
            }

            // notify CPM with subscription response
            std::string subPubStr(ITEM_PUBLISHER);
            CommonProtocolManager::getInstance()->notifyMessage(subPubStr, subJsonData);
        };

        if (monitoredItems.paramList.empty())
        {
            LOGF_ERROR(CPMAPP, "create subscription failed or invalid subscriptionId");
            status.setError(FF_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
            error = Error::cpm.CREATE_SUBSCRIPTION_FAILED;
        }
        else
        {
            monitoredItems.requestInfo.maskType = ITEM_ATTR_SUBSCRIPTION_MASK;
            // creates subscription and generates subscription id.
            status = m_pFFParameterCache->createSubscription(monitoredItems,
                                                             itemRefreshRate,
                                                             false, //read from cache
                                                             callback,
                                                             subscriptionId,
                                                             SUBSCRIPTION_TYPE_EXTERNAL);

            if (!status.hasError())
            {
                response[JSON_ITEM_SUB_ID] = subscriptionId;
                // Add the static parameters to internal subscription table
                status = addSubscriptionCacheItems(subscriptionId);
            }
            else
            {
                LOGF_ERROR(CPMAPP, "create subscription failed or invalid subscriptionId");
                status.setError(FF_ADAPTOR_ERR_CREATE_SUBSCRIPTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
                error = Error::cpm.CREATE_SUBSCRIPTION_FAILED;
            }
        }
    }
    else
    {
        // Invalid refresh rate item, return error immediately
        LOGF_ERROR(CPMAPP,"Invalid Json req, Item Refresh Rate or ItemList is unavailable");
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    // clean the paramInfo members
    m_pFFParameterCache->cleanParamInfo(&monitoredItems);   

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code,error.message);
    }

    return response;
}

/*  ----------------------------------------------------------------------------
    Adds the Static parameters for the given subscription ID to the
    internal subscription table.
*/
PMStatus FFAdaptor::addSubscriptionCacheItems(unsigned int subscriptionID)
{
    PMStatus status;
    ParamInfo paramInfo{};

    if (nullptr == m_pSubscriptionController || nullptr == m_pFFParameterCache )
    {
        LOGF_ERROR(CPMAPP,"Subscription Controller or HART Parameter cache"
                       "instance is null");
        status.setError(HART_ADAPTOR_ERR_NULL_INSTANCE,
                        PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    // get the monitor items for the given subscription ID
    paramInfo = m_pSubscriptionController->getMonitorItems(subscriptionID);

    for (auto item :  paramInfo.paramList)
    {
        // gets the ItemInfo Type
        ItemInfoType* subItemInfo = &item.get();
        VariableType* varType = dynamic_cast<VariableType*>(subItemInfo);

        if (nullptr == varType)
        {
            continue;
        }


        // Don't add dynamic items to internal subscription table
        if (varType->misc.class_attr & DYNAMIC_CLASS)
        {
            continue;
        }

        bool isItemExists = false;
        // Update new items to the internal subscription table
        for (auto table : m_subscriptionCacheTable)
        {
            if (strcmp(table->BlockTag, varType->itemContainerTag.str) == 0)
            {
                unsigned long itemID = varType->itemID;

                if(table->cacheItems.size() > 0)
                {
                    auto it = std::find_if(begin(table->cacheItems),
                                           end(table->cacheItems),
                                           [&itemID](const subscriptionCacheItem& cacheItem)
                    {
                        return cacheItem.itemID == itemID;
                    });

                   isItemExists = (it != std::end(table->cacheItems));

                    // If the item already exists, just increment the reference counter
                    if (isItemExists)
                    {
                        subscriptionCacheItem &item =  *it;
                        item.refCounter++;
                    }
                }

                // if item not exists, add it to the internal subscription table
                if (false == isItemExists)
                {
                    subscriptionCacheItem cacheItem{};
                    cacheItem.varType = varType->clone();
                    cacheItem.itemID = varType->itemID;
                    cacheItem.refCounter = 1;
                    table->cacheItems.push_back(cacheItem);
                }

                   break;
            }
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Removes the Static parameters reference for the given subscription ID
    from the internal subscription table
*/
PMStatus FFAdaptor::removeSubscriptionCacheItems(unsigned int subscriptionID)
{
    PMStatus status;
    ParamInfo paramInfo{};

    if (nullptr == m_pSubscriptionController || nullptr == m_pFFParameterCache )
    {
        LOGF_ERROR(CPMAPP,"Subscription Controller or HART Parameter cache"
                       "instance is null");
        status.setError(HART_ADAPTOR_ERR_NULL_INSTANCE,
                        PM_ERR_CL_HART_ADAPTOR);
        return status;
    }

    std::vector<Subscription*> subscription = m_pSubscriptionController->getAllSubscription();
    std::vector<Subscription*>::iterator it;

    for (it = subscription.begin();it != subscription.end(); ++it)
    {
        /* if subscription ID is 0, remove parameters reference for all the subscriptions
           otherwise only specified subscription ID.
        */

        if (subscriptionID != 0)
        {
            if(subscriptionID == (*it)->getSubscriptionID())
            {
                paramInfo = (*it)->getMonitorItems();
            }
            else
            {
                continue;
            }
        }
        else
        {
            paramInfo = (*it)->getMonitorItems();
        }

        // get the monitor items for the given subscription ID
        paramInfo = m_pSubscriptionController->getMonitorItems((*it)->getSubscriptionID());

        // removes the items from the internal subscription table
        for (auto item :  paramInfo.paramList)
        {
            // gets the ItemInfo Type
            ItemInfoType* subItemInfo = &item.get();
            VariableType* variable = dynamic_cast<VariableType*>(subItemInfo);
            bool isItemExists = false;

            if (nullptr == variable)
                continue;

            // skip if it is dynamic parameters
            if (variable->misc.class_attr & DYNAMIC_CLASS)
            {
                continue;
            }

            for (auto table : m_subscriptionCacheTable)
            {
                if (strcmp(table->BlockTag, variable->itemContainerTag.str) == 0)
                {
                    unsigned long itemID = variable->itemID;

                    if(table->cacheItems.size() > 0)
                    {
                        auto it = std::find_if(begin(table->cacheItems),
                                               end(table->cacheItems),
                                               [&itemID](const subscriptionCacheItem& cacheItem)
                        {
                            return cacheItem.itemID == itemID;
                        });

                        isItemExists = (it != std::end(table->cacheItems));

                        // If the item exists, just decrement the reference counter
                        if (isItemExists)
                        {
                            subscriptionCacheItem &cacheItem =  *it;
                            cacheItem.refCounter--;
                        }
                    }

                     break;
                }
            }
        }

        if (subscriptionID != 0)
        {
            break;
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves the requested parameter info
    Authored By: Utthunga
*/
PMStatus FFAdaptor::getSubParamInfo(VariableType* varItem)
{
    PMStatus status;

    if (nullptr != varItem)
    {
        ParamInfo paramInfo{};
        paramInfo.blockTag.str = const_cast<char*>(varItem->itemContainerTag.str);
        paramInfo.paramList.push_back(*varItem);
        paramInfo.requestInfo.maskType = ITEM_ATTR_SUBSCRIPTION_MASK;

        ITEM_ID blkCharID = 0;
        status = m_pFFParameterCache->getBlockCharacteristicsID(paramInfo.blockTag.str,
                                                                blkCharID);
        if (blkCharID == varItem->containerId)
        {
             // Call getParameter and retrieve parameter information
             status = m_pFFParameterCache->getBlockCharacteristics(&paramInfo,
                                                                   varItem->subIndex);
        }
        else
        {
            // Call getParameter and retrieve parameter information
            status = m_pFFParameterCache->getParameter(&paramInfo);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Validates item for subscription if already subscribed or not
    Authored By: Utthunga
*/
bool FFAdaptor::isSubscriptionItemExists(ITEM_ID itemId, ParamInfo paramInfo)
{
    for (auto item : paramInfo.paramList)
    {
        // gets the ItemInfo Type
        ItemInfoType* itemInfo = &item.get();
        if (itemInfo->itemID == itemId)
        {
            return true;
        }
    }

    return false;
}

/*  ----------------------------------------------------------------------------
    Starts subscription
    Authored By: Utthunga
*/
Json::Value FFAdaptor::startSubscription(const Json::Value& startSubReq)
{
    Json::Value startSubResp("ok");
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    if (startSubReq.isMember(JSON_ITEM_SUB_ID) &&
        startSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (startSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionID = startSubReq[JSON_ITEM_SUB_ID].asUInt();
        // starts subscription
        status = m_pFFParameterCache->startSubscription(subscriptionID);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "invalid subscription id");
            status.setError(FF_ADAPTOR_ERR_START_SUBSCRIPTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
            error = Error::cpm.START_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return startSubResp;
}

/*  ----------------------------------------------------------------------------
    Stops subscription
    Authored By: Utthunga
*/
Json::Value FFAdaptor::stopSubscription(const Json::Value& stopSubReq)
{
    Json::Value response("ok");
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    if (stopSubReq.isMember(JSON_ITEM_SUB_ID) &&
        stopSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (stopSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionID = stopSubReq[JSON_ITEM_SUB_ID].asUInt();
        // Unsubscribe and stop the subscription thread
        status =  m_pFFParameterCache->unsubscribe(subscriptionID);

        if (status.hasError())
        {
            status.setError(FF_ADAPTOR_ERR_STOP_SUBSCRIPTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
            error = Error::cpm.STOP_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return response;
}

/*  ----------------------------------------------------------------------------
    Deletes subscription items
    Authored By: Utthunga
*/
Json::Value FFAdaptor::deleteSubscription(const Json::Value& delSubReq)
{
    Json::Value response("ok");
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    if (delSubReq.isMember(JSON_ITEM_SUB_ID) &&
        delSubReq[JSON_ITEM_SUB_ID].isIntegral() &&
        (delSubReq[JSON_ITEM_SUB_ID].asLargestInt() >= 0))
    {
        uint subscriptionID = delSubReq[JSON_ITEM_SUB_ID].asUInt();

        status = removeSubscriptionCacheItems(subscriptionID);

        if (0 == subscriptionID)
        {
            // Deletes all subscriptions
            status = m_pFFParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_EXTERNAL);
        }
        else
        {
            // Stop supcription and delete the subscription from the list
            status = m_pFFParameterCache->deleteSubscription(subscriptionID);
        }

        if (status.hasError())
        {
            status.setError(FF_ADAPTOR_ERR_DELETE_SUBSCRIPTION_FAILED, PM_ERR_CL_FF_ADAPTOR);
            error = Error::cpm.DELETE_SUBSCRIPTION_FAILED;
        }
    }
    else
    {
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return response;
}

/*  ----------------------------------------------------------------------------
    Creates internal subscription to update the cache table when the
    ST_REV counter increases
    Authored By: Utthunga
*/
PMStatus FFAdaptor::createsInternalSubscription()
{
    ParamInfo monitoredItems{};
    uint subscriptionId = 0;
    ITEM_ID symID = INVALID_ID_OR_HANDLE;

    APP_INFO appInfo{};
    ENV_INFO2 envInfo{};
    PMStatus status;
    int blkCount = 0;

    envInfo.app_info = &appInfo;
	
	// Get the Device information from connection manager
	const DeviceInfo & deviceInfo = m_pFFConnectionManager->getDeviceInfo();
	
    // Get the lsit of all the blocks in the device
    for (auto blkTag : deviceInfo.dd_tag)
    {
        ++blkCount;
        if (blkCount > deviceInfo.blk_count)
        {
            break;
        }

        envInfo.block_handle = m_pDDSController->getBlockHandleByBlockTag(blkTag);
        if (envInfo.block_handle < 0)
        {
            LOGF_DEBUG(CPMAPP, "Invalid block handle");
            continue;
        }

        status = m_pDDSController->ddiLocateSymbolId(&envInfo, ST_REV_DD_NAME, &symID);
        if (status.hasError())
        {
            // ST_REV item Id not found in symbol file
            continue;
        }

        // The below memory is deallocated in disconnect() API
        auto subscription = new SUBSCRIPTION_CACHE_TBL;
        subscription->BlockTag = blkTag;     
        m_subscriptionCacheTable.push_back(subscription);

       // The below memory is deallocated in disconnect() API
        auto varType = new VariableType();
        varType->itemContainerTag.str = blkTag;
        varType->itemID = symID;

        status = getSubParamInfo(varType);

        if (status.hasError())
        {
            LOGF_DEBUG(CPMAPP, "get parameter failed for st_rev");
            if (nullptr != varType)
            {
                delete varType;
                varType = nullptr;
            }
            continue;
        }

        monitoredItems.paramList.push_back(*varType);
    }

    DataChangedCallback callback = [this](uint subscriptionID,
            ParamInfo* monitorItem)
    {
        PMStatus status;
        if (nullptr != monitorItem)
        {
            for (auto& paramItemRef : monitorItem->paramList)
            {
                auto paramItem = &paramItemRef.get();
                auto stRevParam = dynamic_cast<VariableType*>(paramItem);
                if (nullptr == stRevParam )
                {
                    LOGF_ERROR(CPMAPP, "item is not a variable type ");
                    break;
                }

                for (auto table : m_subscriptionCacheTable)
                {
                    // gets the ItemInfo Type
                    if  (strcmp(table->BlockTag, stRevParam->itemContainerTag.str) == 0)
                    {
                        // Read Dynamic parameters always and static parameter once if it is not read
                        //  status = m_pFFParameterCache->updateCacheTable(item->paramList, true);

                       /* reads the static parameters and updates the cache
                           upon ST_REV value changed */
                        if (stRevParam->valueChanged)
                        {
                            ParamInfo paramInfo{};
                            paramInfo.blockTag.str = stRevParam->itemContainerTag.str;

                            for (auto item : table->cacheItems)
                            {
                                /* add it to the read list only if refCounter is greater
                                   than or equal to 1 */
                                if (item.refCounter >= 1)
                                {
                                    paramInfo.paramList.push_back(*item.varType);
                                }
                            }

                            status = m_pFFParameterCache->updateCacheTable(
                                        paramInfo, false);
                        }
                        break;
                    }
                }
            }
        }
    };

    // creates subscription and generates subscription id.
    status = m_pFFParameterCache->createSubscription(monitoredItems,
                                                     m_internalSubscriptionRate,
                                                     true, //read from device
                                                     callback,
                                                     subscriptionId,
                                                     SUBSCRIPTION_TYPE_INTERNAL);

    if (!status.hasError())
    {
        status = m_pFFParameterCache->startSubscription(subscriptionId);
    }

    // clears the list of parameter items, which are used to create the subscription
    for(auto item : monitoredItems.paramList)
    {
        auto varItem = dynamic_cast<VariableType*>(&item.get());
        if(nullptr != varItem)
        {
            delete varItem;
            varItem = nullptr;
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
     Start device scanning operation on H1 network
*/
Json::Value FFAdaptor::startScan(const Json::Value& data)
{
    (void) data;
    Json::Value response;

    // create scan thread only if no scan thread exists
    if (THREAD_STATE_INVALID == m_pBusinessLogic->getScanThreadState())
    {
        // create device scan thread
        m_pBusinessLogic->createScanThread();

        // reset the scan sequence id
        m_unScanSeqID = 0;

        response = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Scanning thread is already running");
        Error::Entry error = Error::cpm.START_SCAN_FAILED;
        response = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    return response;
}

/*  ----------------------------------------------------------------------------
    Stops device scanning operation on H1 network
*/
Json::Value FFAdaptor::stopScan()
{
    Json::Value response;

    PMStatus status = stopScanThread();

    if (!status.hasError())
    {
        response = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        LOGF_ERROR(CPMAPP, "No Scanning thread is available");
        Error::Entry error = Error::cpm.STOP_SCAN_FAILED;
        response = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    return response;
}

/*  ----------------------------------------------------------------------------
    Stops device scanning thread on H1 network
*/
PMStatus FFAdaptor::stopScanThread()
{
    PMStatus stopStatus;

    // Destroy scan thread only if the existing thread is in running state
    if (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
    {
        // Destroy scan thread
        m_pBusinessLogic->destroyScanThread();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "No Scanning thread is available");
        stopStatus.setError(FF_ADAPTOR_ERR_STOP_SCAN_FAILED,PM_ERR_CL_FF_ADAPTOR);
    }

    return stopStatus;
}
/*  ----------------------------------------------------------------------------
    Scans H1 network for basic devices
*/
PMStatus FFAdaptor::scanNetwork()
{
    // scan the network through ff stack APIs;
    ENV_INFO sEnvInfo{};
    APP_INFO sAppInfo{};
    m_sScanDevTable = {};

    sEnvInfo.app_info = &sAppInfo;

    PMStatus scanStatus;
    scanStatus =  m_pFFConnectionManager->scanDevices(&sEnvInfo, &m_sScanDevTable);

    // can the network and prepare the scan device table for device data population
    if (!scanStatus.hasError())
    {
        scanStatus = populateDeviceInfo();

        // Populate device failed
        if (scanStatus.hasError())
        {
            if (FF_ADAPTOR_ERR_SCAN_THREAD_STOPPED == scanStatus.getErrorCode())
            {
                // Scan thread has been stopped terminate thread immediately
                // Set state as scanning in progress
                CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_SCANNING_IN_PROGRESS);
            }
        }
    }
    else
    {
        // scan device failed; inform scan thread to finish scanning mechanism
        Error::Entry error = Error::cpm.DEV_SCAN_FAILED;

        // prepare Json error response
        Json::Value jsondata = JsonRpcUtil::setErrorObject(error.code, error.message);

        // notify CPM with error infomation
        std::string scanPubStr(SCAN_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(scanPubStr, jsondata);
    }

    return scanStatus;
}

/*  ----------------------------------------------------------------------------
    Connects to FF basic device
*/
Json::Value FFAdaptor::connect(const Json::Value& data)
{
    PMStatus status;
    Json::Value root;
    uint32_t deviceAddress{0};

    if (data.isMember(JSON_DEV_ADDR) && data[JSON_DEV_ADDR].isIntegral())
    {
        deviceAddress = data[JSON_DEV_ADDR].asUInt();
    }

    if (deviceAddress < MIN_VALID_ADDRESS || deviceAddress > MAX_VALID_ADDRESS)
    {
        LOGF_ERROR(CPMAPP,"Invalid Device Address: " << deviceAddress);
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Store the Json Data
    DeviceInfo devInfo{};

    // Fill the DeviceInfo Structure
    devInfo.node_address = deviceAddress;

    // Storing this node number for disconnection notification purpose
    m_nodeAddress = deviceAddress;

    // Setting this state for disconnect notification
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_CONNECTION_IN_PROGRESS );

    // Create a lock here for disconnectionNotification
    m_connectLock.lock();

    // Send Connect request to Connection Manager
    status = m_pFFConnectionManager->connectDevice(&devInfo);

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Could Not Connect to the Device: "
                   << status.getErrorCode());

        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        m_connectLock.unlock();
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        PMStatus createSubStatus;
        createSubStatus = createsInternalSubscription();
        
        // Prepare symbol table
        PMStatus prepSymTable;
        prepSymTable = prepareSymTable();

        if (!createSubStatus.hasError() && !prepSymTable.hasError())
        {
            IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

            if (nullptr != pAppConfigParser)
            {
                std::string imagePath = pAppConfigParser->getImagePath();
            
                // Creates a specific directory
                // If the directory already exists clear the content of the directory
                if (false == CPMUtil::createDirectory(imagePath))
                    CPMUtil::clearDirectory(imagePath);
            }

            // Get the top Level Menu for the device
            getTopLevelMenu(root);

            // Set state as connected
            CPMStateMachine::getInstance()->setCpmState(CPMSTATE_DEVICE_CONNECTED);
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_CONNECTION_IN_PROGRESS );
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Error in preparing connection compenents : "
                       << status.getErrorCode());

            // Disconnect the connected device in subscription failure case
            disconnect();

            Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
            m_connectLock.unlock();
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    m_connectLock.unlock();
    return JsonRpcUtil::getResponseObject(root);
}

/*  ----------------------------------------------------------------------------
    Disconnects from the FF basic device
*/
Json::Value FFAdaptor::disconnect()
{
    if (CPMSTATE_METHOD_EXEC_IN_PROGRESS == (CPMSTATE_METHOD_EXEC_IN_PROGRESS & CPMStateMachine::getInstance()->getCpmState()))
    {
        // Stop current method execution before disconenction
        stopMethodExec();
    }
    m_connectLock.lock();
    
    PMStatus status = disconnectDevice();
    
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Device Disconnect Failed: " << status.getErrorCode());
        Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
        m_connectLock.unlock();
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    m_connectLock .unlock();
    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Clears subscription and disconnects device
*/
PMStatus FFAdaptor::disconnectDevice()
{
    // Before disconnect, delete all running subcription
    PMStatus status = stopAndClearSubscription();

    if (!status.hasError())
    {
        status = m_pFFConnectionManager->disconnectDevice();
        
        // Clear symbol table 
        m_SymTable.clear();
        
        // Set state as Disconnected
        CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_DEVICE_CONNECTED);

        LOGF_DEBUG(CPMAPP, "Device Successfully Disconnected");
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Deletes all items from the internal subscription table
    Authored By: Utthunga
*/
void FFAdaptor::deleteSubscriptionTableItem()
{
    if (!m_subscriptionCacheTable.empty())
    {
        for (auto table : m_subscriptionCacheTable)
        {
            if (nullptr != table)
            {
                for (auto item : table->cacheItems)
                {
                    if (nullptr != item.varType)
                    {
                        delete item.varType;
                        item.varType = nullptr;
                    }
                }
                delete table;
                table = nullptr;
            }
        }
        m_subscriptionCacheTable.clear();
    }
}

/*  ----------------------------------------------------------------------------
    Populates the scanned device information
*/
PMStatus FFAdaptor::populateDeviceInfo()
{
    PMStatus populateStatus;

    // Get DeviceInformation from stack based on node address and prepare
    // corresponding device json information and call Notify()
    SCAN_TBL *pList = m_sScanDevTable.list;
    SCAN_TBL *temp = nullptr;

    m_unScanSeqID++;

    // check whether the scan device table is empty
    if (m_sScanDevTable.count > 0)
    {
        while(nullptr != pList)
        {
            // check whether scanning operation is cancelled
            if (THREAD_STATE_STOP == m_pBusinessLogic->getScanThreadState())
            {
                /* scanning has been cancelled intentionally;
                   clear the exising device list
                   stop populating the device information */
                cleanDeviceList(pList);
                populateStatus.setError(FF_ADAPTOR_ERR_SCAN_THREAD_STOPPED, PM_ERR_CL_FF_ADAPTOR);
                break;
            }
            DeviceInfo deviceInfo{};

            // fetch the device information
            populateStatus = m_pFFConnectionManager->getDeviceInfo(
                        pList->station_address,
                        &deviceInfo);

            if (!populateStatus.hasError())
            {
                // prepare device information in Json
                Json::Value jsondata;

                // prepare the corresponding JSON information and call notify
                jsondata[JSON_DEV_INFO][JSON_DEV_TAG] = m_pDDSController->getUTF8String(deviceInfo.pd_tag);
                jsondata[JSON_DEV_INFO][JSON_DEV_ADDR] = deviceInfo.node_address;
                jsondata[JSON_DEV_INFO][JSON_DEV_ID_INFO] = deviceInfo.dev_id;

                // Comission/decommission applicable only for the address range 0 - 251
                if (deviceInfo.node_address >= MIN_VALID_ADDRESS &&
                        deviceInfo.node_address <= MAX_DEFAULT_ADDRESS )
                {
                    jsondata[JSON_DEV_INFO][JSON_DEV_COMMISSIONED] =
                            deviceInfo.node_address <= MAX_VALID_ADDRESS ?
                                COMMISSIONED : DECOMMISSIONED;
                }
                else
                {
                    // Commission not applicable for visitor host address from 252-255
                    jsondata[JSON_DEV_INFO][JSON_DEV_COMMISSIONED] =
                            NOT_APPLICABLE;
                }
                jsondata[JSON_DEV_SCAN_SEQ_ID] = m_unScanSeqID;

                // notify CPM with device information
                std::string scanPubStr(SCAN_PUBLISHER);
                CommonProtocolManager::getInstance()->notifyMessage(scanPubStr,
                                                                    jsondata);
            }
            else
            {
                // fetching device information failed; set populate device info error and
                // continue populating the other devices in the list
                populateStatus.setError(FF_ADAPTOR_ERR_GET_DEV_INFO, PM_ERR_CL_FF_ADAPTOR);
            }

            // remove the device entry from the list; look for the remaining devices
            temp = pList;
            pList = pList->next;
            free(temp);
            temp = nullptr;
        }
    }
    else
    {
        // prepare device information in Json
        Json::Value jsondata;

        jsondata[JSON_DEV_SCAN_SEQ_ID] = m_unScanSeqID;

        // notify CPM with device information
        std::string scanPubStr(SCAN_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(scanPubStr, jsondata);
    }

    return populateStatus;
}

/*  ----------------------------------------------------------------------------
    Cleans the scanned device table list
*/
void FFAdaptor::cleanDeviceList(SCAN_TBL *pList)
{
    // clear all the entries from the device scanning list
    SCAN_TBL *temp = nullptr;
    while(nullptr != pList)
    {
        temp = pList;
        pList = pList->next;
        free(temp);
        temp = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Checks if FF bus params are available
*/
bool FFAdaptor::isBusParamAvailable(const Json::Value& jsonData)
{
    return jsonData.isMember(JSON_FF_BUS_PARAMS);
}

/*  ----------------------------------------------------------------------------
    Checks if FF Application Settings are available or not.
*/
bool FFAdaptor::isAppSettingsAvailable(const Json::Value& jsonData)
{
    // Futture usage, Does nothing for now.
    (void) jsonData;
    return true;
}

/*  ----------------------------------------------------------------------------
    Sets the H1 network bus parameters
*/
Json::Value FFAdaptor::setBusParameters(const Json::Value& jsonData)
{
    PMStatus status;
    Json::Value jsonResponse;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    FF_BUSPARAM_STATUS ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;

    ffStatus = setBusParams(jsonData);

    if (ffStatus != FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
    	///[ Fix: DP-827-Setting Invalid slot time.]As fix added new error codes.
        if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_OUT_OF_RANGE_VALUE_FOR_NUN_FUN_PARAMS)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Wrong input from USER for NUN and FUN Values");
            error = Error::cpm.BUS_PARAM_OUT_OF_RANGE_FOR_NUN_AND_FUN;
        } 
        
        else if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_WRONG_VALUE_FOR_SLOT_TIME)
		{
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Wrong input from USER for Slot time");
            error = Error::cpm.BUS_PARAM_INVALID_SLOT_TIME;
        } 
        
        else if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_DEFUALT_PARAM_FAILED)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Setting default params failed");
            error = Error::cpm.BUS_PARAM_SET_DEFUALT_PARAM_FAILED;
        } 
        
        else if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Invalid bus parameters");
            error = Error::cpm.INVALID_BUS_PARAMS;
        }
        
        else
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Error in storing parameters");
            error = Error::cpm.SET_BUS_PARAM_FAILED;
        }
        
        return JsonRpcUtil::setErrorObject(error.code,
                                           error.message);
    }

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Retrieves H1 network bus parameters
*/
Json::Value FFAdaptor::getBusParameters()
{
    Json::Value root;
    Json::Value jsonData;
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    FF_BUSPARAM_STATUS getStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;

    status = m_pBusinessLogic->prepareBusParams();

    // get the bus parameters from PS, update BUS_PARAM structure with UI values
    status = fetchBusParameters(static_cast<void*>(&m_busParams));
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Failed to fetch bus params from persistant storage file");
        error = Error::cpm.GET_BUS_PARAM_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
	    getStatus = getBusParams(jsonData);
	    if(getStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
		{
			if (jsonData.isMember(JSON_FF_BUS_PARAMS))
			{
				 root[JSON_FF_BUS_PARAMS] = jsonData[JSON_FF_BUS_PARAMS];
			}
			else
			{
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Pram is not member of list" );
				getStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_FAIL;
			}
		}
    }

    if (getStatus != FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Fetching bus param fail");
        error = Error::cpm.GET_BUS_PARAM_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(root);
}

/*  ----------------------------------------------------------------------------
    Sets FF communication failure
*/
void FFAdaptor::setCommunicationFailure()
{

}

/*  ----------------------------------------------------------------------------
    Processes device alerts
*/
void FFAdaptor::processAlerts(void * data)
{
    (void) data;
}

/*  ----------------------------------------------------------------------------
    Set the bus parameters value in the Persistence storage
*/
FF_BUSPARAM_STATUS FFAdaptor::setBusParams(const Json::Value& root)
{
    FF_BUSPARAM_STATUS ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;

    std::string rootStr;
    Json::Value busParamJson;
    PMStatus status;

    status = m_pBusinessLogic->prepareBusParams();
    // get the bus parameters from PS, update BUS_PARAM structure with UI values
    status = fetchBusParameters(static_cast<void*>(&m_busParams));
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Failed to fetch bus params from persistant storage file");
        ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_GET_BUS_PARAMS_FAIL;
        return ffStatus;
    }

    ffStatus = getBusParams(busParamJson);
    if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
    	/// checking if input json is valid bus parameter
        for (auto const& id: root["FF_BUS_PARAMS"].getMemberNames())
		{
			rootStr = id;

			if (!busParamJson["FF_BUS_PARAMS"].isMember(rootStr))
			{
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Invalid bus params");
				ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS;
				return ffStatus;
			} ///[Fix DP-823 : application return ok for invalid bus parmaters ]
			else if ((rootStr != SLOT_TIME) && (rootStr != FIRST_UNPOLLED_NODE) &&
					(rootStr != NUM_UNPOLLED_NODE) && (rootStr != SET_DEFAULT))
			{
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Unable to set default bus parameters");
				ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_DEFUALT_PARAM_FAILED;
				return ffStatus;
			}
		}
    }
    else
    {
    	ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_FAIL;
    	return ffStatus;
    }

    // set default bus parameters if root["FF_BUS_PARAMS"]["set_default"] is set to 1 from UI
    if (ROOT.isMember(SET_DEFAULT))
    {
        if (ROOT[SET_DEFAULT].isIntegral() && (ROOT[SET_DEFAULT].asLargestInt() >= 0))
        {
            if (DEFAULT_PARAMS == ROOT[SET_DEFAULT].asUInt())
            {
                setDefaultBusParams();
                LOGF_INFO(CPMAPP,"BUS_PARAM_INFO:As SET_DEFAULT =1, storing the default parameters in PS");
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Invalid bus params");
            ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS;
        }
    }
    else
    {
        // update BUS-PARAM with new UI_values and validate them
        ffStatus = fillValues(root);
    }

    if (ffStatus == FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
        // store bus params in PS
        ffStatus = storeBusParams(root);

        if (ffStatus != FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:storing Parameters in Persistence storage failed");
            // send an error message to UI/USER
            ffStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_GET_BUS_PARAMS_FAIL;
        }
    }

    return ffStatus;
}

/*  ----------------------------------------------------------------------------
    Assigns and validates bus parameters values
*/
FF_BUSPARAM_STATUS FFAdaptor::fillValues(const Json::Value& root)
{
    FF_BUSPARAM_STATUS fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;

    // Read & set Slot Time
    if (ROOT.isMember(SLOT_TIME))
    {
        if (ROOT[SLOT_TIME].isIntegral())
        {
            unsigned int slotTime = ROOT[SLOT_TIME].asUInt();
            if (SLOT_TIME_MIN == slotTime ||  SLOT_TIME_MAX == slotTime)
            {
                m_busParams.slot_time = slotTime;
            }
            else
            {
                fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_WRONG_VALUE_FOR_SLOT_TIME;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for slot_time, please use 8 or 16");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Set FUN & NUN values to the existing values by default
    unsigned int fun = m_busParams.fun;
    unsigned int nun = m_busParams.nun;

    // Store the user entered FUN value if available
    if (ROOT.isMember(FIRST_UNPOLLED_NODE))
    {
        if (ROOT[FIRST_UNPOLLED_NODE].isIntegral() &&
            (ROOT[FIRST_UNPOLLED_NODE].asLargestInt() >= 0))
        {
            fun = ROOT[FIRST_UNPOLLED_NODE].asUInt();
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Store the user entered NUN value if available
    if (ROOT.isMember(NUM_UNPOLLED_NODE))
    {
        if (ROOT[NUM_UNPOLLED_NODE].isIntegral() &&
            (ROOT[NUM_UNPOLLED_NODE].asLargestInt() >= 0))
        {
        	nun = ROOT[NUM_UNPOLLED_NODE].asUInt();
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Determine the Max Polling range
    unsigned int maxPollingRange = fun + nun;

    // Validate FUN & NUN values
    if (((fun >= FUN_MIN) && (fun <= FUN_MAX)) &&
    	((nun >= DFLT_NUM_UNPOLLED_NODE) && (nun <= NUN_MAX)) &&
		(POLLING_LIMIT > maxPollingRange))
    {
    	m_busParams.fun = fun;
    	m_busParams.nun = nun;
    }
    else
    {
    	fillValueStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_OUT_OF_RANGE_VALUE_FOR_NUN_FUN_PARAMS;
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Out of range values for NUN and FUN.FUN range 20-247."
                   "NUN range 0-228 and Total range below 248");
    }

    return fillValueStatus;
}

/*  ----------------------------------------------------------------------------
    Updates Json values with user i/p, so can be store in persistence storage
*/
void FFAdaptor::modifyParams(Json::Value &root)
{
    //modifying FF stack param with user/UI given values
    ROOT[THIS_LINK] 				= m_busParams.this_link;
    ROOT[SLOT_TIME] 				= m_busParams.slot_time;
    ROOT[PHL_OVERHEAD] 			    = m_busParams.phlo;
    ROOT[MAX_RPLY_DELAY]  	   		= m_busParams.mrd;
    ROOT[MIN_INTER_PDU_DELAY]		= m_busParams.mipd;
    ROOT[TIME_SYNC_CLASS]			= m_busParams.tsc;
    ROOT[MAX_INTER_CHAN_SIG_SKEW]	= m_busParams.phis_skew;
    ROOT[PHL_PREAMBLE_EXTN]		    = m_busParams.phpe;
    ROOT[PHL_GAP_EXTN]		   		= m_busParams.phge;
    ROOT[FIRST_UNPOLLED_NODE]		= m_busParams.fun;
    ROOT[NUM_UNPOLLED_NODE]		    = m_busParams.nun;
}

/*  ----------------------------------------------------------------------------
    Stores bus parameters in the persistence storage
*/
FF_BUSPARAM_STATUS FFAdaptor::storeBusParams(const Json::Value& root)
{
    FF_BUSPARAM_STATUS storeParamStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_FAIL;

    Json::Value busParams = root;

    // Get the bus paramaters already stored in persistent storage
    storeParamStatus = getBusParams(busParams);
    if (storeParamStatus != FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Get Bus param fail in storeBusParams");
        return storeParamStatus;
    }

    // Modify the existing bus parameters before save it to persistent storage
    modifyParams(busParams);

    // to save the parameters in PS
    PMStatus status = m_pBusinessLogic->saveConfigurationValues(busParams);
    if (!status.hasError())
    {
        storeParamStatus= FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;
    }

    return storeParamStatus;
}

/*  ----------------------------------------------------------------------------
    Fetches bus parameters from persistence storage
*/
FF_BUSPARAM_STATUS FFAdaptor::getBusParams(Json::Value& root)
{
    FF_BUSPARAM_STATUS getParamStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_FAIL;
    PMStatus status;

    status = m_pBusinessLogic->readConfigurationValues(root);
    if (!status.hasError())
    {
        getParamStatus = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;
    }

    return getParamStatus;
}

/*  ----------------------------------------------------------------------------
    Sets default bus parameters
*/
void FFAdaptor::setDefaultBusParams()
{
    m_busParams.this_link       = DFLT_THIS_LINK;
    m_busParams.slot_time       = DFLT_SLOT_TIME;
    m_busParams.phlo			= DFLT_PHL_OVERHEAD;
    m_busParams.mrd			    = DFLT_MAX_RPLY_DELAY;
    m_busParams.mipd			= DFLT_MIN_INTER_PDU_DELAY;
    m_busParams.tsc			    = DFLT_TIME_SYNC_CLASS;
    m_busParams.phis_skew		= DFLT_MAX_INTER_CHAN_SIG_SKEW;
    m_busParams.phpe			= DFLT_PHL_PREAMBLE_EXTN;
    m_busParams.phge			= DFLT_PHL_GAP_EXTN;
    m_busParams.fun			    = DFLT_FIRST_UNPOLLED_NODE;
    m_busParams.nun			    = DFLT_NUM_UNPOLLED_NODE;
}

/*  ----------------------------------------------------------------------------
    Provides interface function to connection manager to pass
    the bus to FF stack
*/
PMStatus FFAdaptor::fetchBusParameters(void* params)
{
    FF_BUSPARAM_STATUS ffret = FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS;
    PMStatus status;

    FF_BUS_PARAMS *parameters = static_cast<FF_BUS_PARAMS*>(params);

    // get bus params
    Json::Value root;
    ffret = getBusParams(root);

    if (ffret == FF_BUSPARAM_STATUS::FF_BUSPARAM_SUCCESS)
    {
        // updating the bus param structure with Bus param values in PS
        parameters->this_link       = ROOT[THIS_LINK].asUInt();
        parameters->slot_time       = ROOT[SLOT_TIME].asUInt();
        parameters->phlo			= ROOT[PHL_OVERHEAD].asUInt();
        parameters->mrd			    = ROOT[MAX_RPLY_DELAY].asUInt();
        parameters->mipd			= ROOT[MIN_INTER_PDU_DELAY].asUInt();
        parameters->tsc			    = ROOT[TIME_SYNC_CLASS].asUInt();
        parameters->phis_skew		= ROOT[MAX_INTER_CHAN_SIG_SKEW].asUInt();
        parameters->phpe			= ROOT[PHL_PREAMBLE_EXTN].asUInt();
        parameters->phge			= ROOT[PHL_GAP_EXTN].asUInt();
        parameters->fun			    = ROOT[FIRST_UNPOLLED_NODE].asUInt();
        parameters->nun			    = ROOT[NUM_UNPOLLED_NODE].asUInt();
    }
    else
    {
        status.setError(FF_ADAPTOR_ERR_BUSPARAM_GET_BUS_PARAMS_FAIL,
                        PM_ERR_CL_FF_ADAPTOR);

        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:failed to get ff bus params");
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Gets default FF bus parameters
*/
void FFAdaptor::getDefaultBusParameters(Json::Value &root)
{
    // updating json values stored in PS with default values
    ROOT[THIS_LINK] 				= DFLT_THIS_LINK;
    ROOT[SLOT_TIME] 				= DFLT_SLOT_TIME;
    ROOT[PHL_OVERHEAD] 			    = DFLT_PHL_OVERHEAD;
    ROOT[MAX_RPLY_DELAY]  	   		= DFLT_MAX_RPLY_DELAY;
    ROOT[MIN_INTER_PDU_DELAY]		= DFLT_MIN_INTER_PDU_DELAY;
    ROOT[TIME_SYNC_CLASS]			= DFLT_TIME_SYNC_CLASS;
    ROOT[MAX_INTER_CHAN_SIG_SKEW]	= DFLT_MAX_INTER_CHAN_SIG_SKEW;
    ROOT[PHL_PREAMBLE_EXTN]		    = DFLT_PHL_PREAMBLE_EXTN;
    ROOT[PHL_GAP_EXTN]		   		= DFLT_PHL_GAP_EXTN;
    ROOT[FIRST_UNPOLLED_NODE]		= DFLT_FIRST_UNPOLLED_NODE;
    ROOT[NUM_UNPOLLED_NODE]		    = DFLT_NUM_UNPOLLED_NODE;
    ROOT[SET_DEFAULT]               = DEFAULT_INITIAL;
}

/*  ----------------------------------------------------------------------------
    Retrieves basic field device information
    Authored By: Utthunga
*/
Json::Value FFAdaptor::getFieldDeviceConfig(const Json::Value& jsonData)
{
    PMStatus pmStatus;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;
    Json::Value resultData;
    unsigned int address = 0;

    if (jsonData.isMember(JSON_DEV_ADDR) &&
        jsonData[JSON_DEV_ADDR].isIntegral() &&
        (jsonData[JSON_DEV_ADDR].asLargestInt() > 0))
    {
        address = jsonData[JSON_DEV_ADDR].asInt();
        DeviceInfo deviceInfo{};
        UINT8 deviceClass{0};

        // gets the device information
        pmStatus = m_pFFConnectionManager->getDeviceInfo(address, &deviceInfo);

        if (!pmStatus.hasError())
        {
            resultData[JSON_DEV_ADDR] = address;
            resultData[JSON_DEV_TAG] = m_pDDSController->getUTF8String(deviceInfo.pd_tag);

            /* if device address range is in between 16 to 247
               then get the device class.
            */
            if (address >= MIN_VALID_ADDRESS && address <= MAX_VALID_ADDRESS)
            {
                //Retrieves the current device class
                pmStatus = m_pFFConnectionManager->getDeviceClass(address,
                                                                  deviceClass);

                if (!pmStatus.hasError())
                {
                    // prepare device information in Json
                    resultData[JSON_DEV_CLASS] = deviceClass;
                }
            }
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Invalid JSON request!");
        error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    if (pmStatus.hasError())
    {
        error = Error::cpm.GET_FIELD_DEVICE_CONFIG_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject (resultData);
}

/*  ----------------------------------------------------------------------------
    Assigns user configured node address, PD Tag, Device Class(Basic /LM) to
    the device.
    Authored By: Utthunga
*/
Json::Value FFAdaptor::setFieldDeviceConfig(const Json::Value& jsonData)
{
    Json::Value resultData;
    PMStatus status;
    Error::Entry error {};
    std::string configOption{""};
    unsigned int address{0};

    // verifies required member is present or not
    if (jsonData.isMember(JSON_DEV_CONFIG) &&
        jsonData.isMember(JSON_DEV_ADDR))
    {
        // validate Json data types
        if (jsonData[JSON_DEV_CONFIG].isString() &&
            jsonData[JSON_DEV_ADDR].isIntegral() &&
            (jsonData[JSON_DEV_ADDR].asLargestInt() >= 0))
        {
            configOption = jsonData[JSON_DEV_CONFIG].asString();
            address = jsonData[JSON_DEV_ADDR].asUInt();
        }

        // set Device Class
        if (configOption == JSON_DEVICE_CLASS)
        {
            uint8_t deviceClass{0};

            if (jsonData.isMember(JSON_DEV_CLASS) &&
                jsonData[JSON_DEV_CLASS].isIntegral() &&
                (jsonData[JSON_DEV_CLASS].asLargestInt() >= 0))
            {
                // gets the device class to write
                deviceClass = jsonData[JSON_DEV_CLASS].asUInt();

                CPMStateMachine::getInstance()->setCpmState(CPMSTATE_COMMSSION_IS_IN_PROGRESS);
                m_connectLock.lock();
                // sets field device class such as basic or LM
                status = m_pFFConnectionManager->setDeviceClass(deviceClass,
                                                                address);

               if (status.hasError())
                {
                     error = Error::cpm.DEVICE_CLASS_COMMISSION_FAILED;
                }

                m_connectLock.unlock();
                CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_COMMSSION_IS_IN_PROGRESS);
            }
            else
            {
                status.setError(FF_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL, PM_ERR_CL_FF_ADAPTOR);
                error = Error::cpm.INVALID_JSON_REQUEST;
            }
        }
        else if (configOption == JSON_DEVICE_COMMISSION)
        {
            unsigned int newAddress{0};
            std::string newTag{""};

            // Validate Json data types
            if (jsonData.isMember(JSON_NEW_DEV_ADDR) &&
                jsonData[JSON_NEW_DEV_ADDR].isIntegral() &&
                (jsonData[JSON_NEW_DEV_ADDR].asLargestInt() >= 0))
            {
                //gets the new device address
                newAddress = jsonData[JSON_NEW_DEV_ADDR].asUInt();

                if(jsonData.isMember(JSON_NEW_DEV_TAG) &&
                        jsonData[JSON_NEW_DEV_TAG].isString())
                {
                    // gets the new device tag
                    newTag = jsonData[JSON_NEW_DEV_TAG].asString();

                    // returns error if new device tag length is zero
                    if (newTag.length() == 0)
                    {
                        LOGF_ERROR(CPMAPP,"New device tag is empty!");
                        status.setError(FF_ADAPTOR_ERR_INVALID_TAG, PM_ERR_CL_FF_ADAPTOR);
                        error = Error::cpm.INVALID_TAG;
                    }
                }
				
                if(!status.hasError())
                {
                    // verifies device already commissioned or not
                    if (address > MAX_VALID_ADDRESS && address <= MAX_DEFAULT_ADDRESS)
                    {
                        // verifies if new address is valid or not
                        if (newAddress < MIN_VALID_ADDRESS ||
                                newAddress  > MAX_VALID_ADDRESS)
                        {
                            LOGF_WARN(CPMAPP, "Invalid new device address!");
                            status.setError(FF_ADAPTOR_ERR_INVALID_ADDRESS, PM_ERR_CL_FF_ADAPTOR);
                            error = Error::cpm.INVALID_ADDRESS;
                        }
                    }
                    else
                    {
                        LOGF_WARN(CPMAPP, "Device Already Commissioned!");
                        status.setError(FF_ADAPTOR_ERR_DEVICE_ALREADY_COMMISSIONED, PM_ERR_CL_FF_ADAPTOR);
                        error = Error::cpm.DEVICE_ALREADY_COMMISSIONED;
                    }
                }


                if (!status.hasError())
                {
                    /* creates commission thread and performs commissioning
                        and notifies commission info to upper layer
                    */
                    m_pFFConnectionManager->createCommissionThread(address,
                                                                   newAddress, newTag,
                                                                   [](std::string commissionInfo)
                    {
                        //inline callback function to notify commission status
                        Json::Value jsondata;
                        jsondata[JSON_STATUS_MESSAGE] = commissionInfo;

                        std::string publisher = STATUS_PUBLISHER;
                        CommonProtocolManager::getInstance()->notifyMessage(
                                    publisher, jsondata);
                    });

                    m_pFFConnectionManager->aquireLock();
                    FFConnectionManager::s_isCompleted = false;
                }
            }
            else
            {
                status.setError(FF_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL, PM_ERR_CL_FF_ADAPTOR);
                error = Error::cpm.INVALID_JSON_REQUEST;
            }
        }
        else if (configOption == JSON_DEVICE_DECOMMISSION)
        {
            //performs device decommissioning
            status = m_pFFConnectionManager->decommissionDevice(address);
            if (status.hasError())
            {
                error = Error::cpm.DEVICE_DECOMMISSION_FAILED;
            }
        }
        else
        {
            status.setError(FF_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL, PM_ERR_CL_FF_ADAPTOR);
            error = Error::cpm.INVALID_JSON_REQUEST;
        }
    }
    else
    {
        status.setError(FF_ADAPTOR_ERR_SET_DEVICE_CONFIG_FAIL, PM_ERR_CL_FF_ADAPTOR);
        error = Error::cpm.INVALID_JSON_REQUEST;
    }

    if (status.hasError())
    {
        resultData = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        if (FFConnectionManager::s_fldDevStatus.hasError())
        {
            Error::Entry connError;

            //Gives error code
            int errorCode = FFConnectionManager::s_fldDevStatus.getErrorCode();

            //Gives specific error mesages
            if(errorCode == FF_CM_ERR_SET_NEW_DEVICE_ADDRESS_FAILED)
            {
                connError = Error::cpm.SET_DEVICE_ADDRESS_FAILED;
            }
            else if(errorCode == FF_CM_ERR_SET_NEW_DEVICE_TAG_FAILED)
            {
                connError = Error::cpm.SET_DEVICE_TAG_FAILED;
            }
			
            resultData = JsonRpcUtil::setErrorObject(connError.code, connError.message);
            FFConnectionManager::s_fldDevStatus.clear();
        }
        else
        {
            resultData = JsonRpcUtil::getResponseObject(Json::Value("ok"));
        }
    }
	
    return resultData;
}

/*  ----------------------------------------------------------------------------
    Maps the processItem method function pointers to corresponding method key
*/
void FFAdaptor::mapItemMethods()
{
    //  Map the function pointer to respective ItemType String
    processMethodMap[JSON_VARIABLE_ITEM_TYPE] = &FFAdaptor::processVariable;
    processMethodMap[JSON_MENU_ITEM_TYPE]     = &FFAdaptor::processMenu;
    processMethodMap[JSON_BLOCK_ITEM_TYPE]    = &FFAdaptor::processBlock;
    processMethodMap[JSON_METHOD_ITEM_TYPE]   = &FFAdaptor::processMethod;
    processMethodMap[JSON_IMAGE_ITEM_TYPE]    = &FFAdaptor::processImage;
}

/*  ----------------------------------------------------------------------------
    Maps DDS Items to JSON item type strings
*/
void FFAdaptor::mapDDSItemTypes()
{
    // Map the DDS item types to the respective ItemType String
    m_itemTypeMap[VARIABLE_ITYPE]   = JSON_VARIABLE_ITEM_TYPE;
    m_itemTypeMap[MENU_ITYPE]       = JSON_MENU_ITEM_TYPE;
    m_itemTypeMap[METHOD_ITYPE]     = JSON_METHOD_ITEM_TYPE;
    m_itemTypeMap[BLOCK_ITYPE]      = JSON_BLOCK_ITEM_TYPE;
    m_itemTypeMap[IMAGE_ITYPE]      = JSON_IMAGE_ITEM_TYPE;
}

/* PROCESS ITEM METHODS *******************************************************/

/*  ----------------------------------------------------------------------------
    Processes Image information
    Authored By: Utthunga
*/
Json::Value FFAdaptor::processImage
(
    const Json::Value& imageRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value imageResponse = Json::Value("ok");

    ITEM_ID itemID{0};
    string itemContainerTag = "";

    if ((SUCCESS == validateItemID(imageRequest, itemID)) &&
        (SUCCESS == validateItemInDD(imageRequest, true)))
    {
        // Get the Block tag
        if (FAILURE == validateItemContTag(imageRequest, itemContainerTag))
        {
            LOGF_ERROR(CPMAPP, "Invalid item container Tag");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }

        if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
        {
            bool bIsValid = true;
            imageResponse = getImage(itemID, itemContainerTag, bIsValid);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid item query for the Image");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            imageResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        imageResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return imageResponse;
}

/*  ----------------------------------------------------------------------------
    Get Edit Display Items
*/
void FFAdaptor::getEditDisplay(const Json::Value & input, Json::Value & output)
{
    PMStatus status;

    // Get the Item ID
    ITEM_ID itemID = 0;
    if (FAILURE == validateItemID(input, itemID))
    {
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    // Get the Block tag
    std::string itemContainerTag;
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    DDI_BLOCK_SPECIFIER ddiBlockSpecifier{};
    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
    DDI_GENERIC_ITEM    genericItem{};
    ENV_INFO envInfo{};
    APP_INFO appInfo{};

    // Fill the Item Specifier
    ddiItemSpecifier.type = DDI_ITEM_ID;
    ddiItemSpecifier.item.id = itemID;

    if (itemContainerTag.empty())
    {
        /* Device Level Standard Menu's, are not associated with any blocks,
           needs to be handled seperately
        */
        ENV_INFO2 envInfo2{};
        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

        // Get the current Device handle
        status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
            Error::Entry error = Error::cpm.GET_MENU_FAILED;
            output = JsonRpcUtil::setErrorObject(error.code, error.message);
            return;
        }

        // Set the Environment Info for ddi_get_tem2
        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;

        envInfo2.app_info = &appInfo;
        appInfo.status_info = USE_EDIT_VALUE;

        // Deveic Level standard Menu's, hence use ddi_get_item2
        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                               ALL_ITEM_ATTR_MASK, &genericItem);
    }
    else
    {
        // Fill the Block Specifier
        ddiBlockSpecifier.type = DDI_BLOCK_TAG;
        ddiBlockSpecifier.block.tag = const_cast<char *>(itemContainerTag.c_str());
        envInfo.app_info = &appInfo;

        // Must be Block Level Menu's - Hence use ddi_get_item
        status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier,
                                              &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
    }

    if (!status.hasError())
    {
        FLAT_EDIT_DISPLAY * pEditDisplay = static_cast<FLAT_EDIT_DISPLAY *>(genericItem.item);

        if (nullptr != pEditDisplay)
        {
            ItemBasicInfo menuInfo;
            // Retrieve Item ID and corresponding block Tag of the item.
            ITEM_ID itemID{0};
            ITEM_TYPE itemType{0};
            std::string blockTag = itemContainerTag;

            // Fill the Menu attributes
            fillBasicMenuAttributes(input, menuInfo);

            // Add the menu information to output Json
            fillItemBasicJsonInfo(menuInfo, output);

            unsigned int itemIndex{0};
            // Now add all the menu items one by one
            while (itemIndex < pEditDisplay->edit_items.count)
            {
                // Retrieve Item ID and corresponding block Tag of the item.
                itemID     = pEditDisplay->edit_items.list[itemIndex].op_id;
                itemType   = pEditDisplay->edit_items.list[itemIndex].op_type;
                if (EDIT_DISP_ITYPE == itemType)
                {
                    itemType = MENU_ITYPE;
                }
                if (WAO_ITYPE == itemType)
                {
                    itemType = VARIABLE_ITYPE;
                }

                /* Get the actual tag using Block ID and block Instance for menu items
                   contained in device level menu. This is not required for Block Level
                   menu's as the menu items in the block menu's will always belong to the same block
                */
                if (itemContainerTag.empty())
                {
                    ITEM_ID blockID = pEditDisplay->edit_items.list[itemIndex].op_block_id;
                    ITEM_ID blockInstance = pEditDisplay->edit_items.list[itemIndex].op_block_instance;

                    // Get the block Tag block ID and instance is filled by DDS
                    if (blockID != 0)
                    {
                        blockTag = m_pDDSController->getBlockTagByBlockID(blockID, blockInstance);
                    }
                }

                // Fill the common information required for al the menu items
                Json::Value itemInfoJson;
                ItemBasicInfo sItemInfo;

                // Fill menu item attributes
                sItemInfo.itemID   = itemID;
                sItemInfo.itemType = m_itemTypeMap[itemType];
                sItemInfo.itemContainerTag = blockTag;
                sItemInfo.itemInDD = true;

                if (blockTag.empty())
                {
                    /* Device Level Standard Menu's, are not associated with any blocks,
                       needs to be handled seperately
                    */
                    ENV_INFO2 envInfo2{};
                    APP_INFO appInfo{};
                    DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
                    DDI_GENERIC_ITEM    ddiGenericItem2{};
                    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};

                    // Get the current Device handle
                    status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);
                    ddiItemSpecifier.type = DDI_ITEM_ID;
                    ddiItemSpecifier.item.id = itemID;

                    if (status.hasError())
                    {
                        LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                        Error::Entry error = Error::cpm.GET_MENU_FAILED;
                        output = JsonRpcUtil::setErrorObject(error.code, error.message);
                    }

                    // Set the Environment Info for ddi_get_tem2
                    envInfo2.device_handle = deviceHandle;
                    envInfo2.type = ENV_DEVICE_HANDLE;

                    envInfo2.app_info = &appInfo;
                    appInfo.status_info = USE_EDIT_VALUE;

                    // Deveic Level standard Menu's, hence use ddi_get_item2 to get further information
                    status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                                           ALL_ITEM_ATTR_MASK, &ddiGenericItem2);

                    if (!status.hasError())
                    {
                        if (fillMenuItemInfoByType(sItemInfo, ddiGenericItem2))
                        {
                            // Fill the Basic information as Json
                            fillItemBasicJsonInfo(sItemInfo, itemInfoJson);
                            // Append to the item List
                            output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                        }
                    }
                    else
                    {
                        LOG_ERROR(CPMAPP, "DDI Get Item 2 Failed with Error : " << status.getErrorCode());
                    }

                    // Clear the Generic Item memory allocated by ddiGetItem API
                    m_pDDSController->ddiCleanItem(&ddiGenericItem2);

                    if (status.hasError())
                    {
                        LOG_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
                    }
                }
                else
                {
                    bool bIsValid = true;

                    ParamItems varItems{};
                    varItems.paramItemID = itemID;
                    varItems.paramContainerTag = blockTag;
                    varItems.paramType = JSON_MENU_ITEM_TYPE;
                    // Get the minimum information for Variable to be shown in Menu
                    itemInfoJson = getVariable(varItems, bIsValid, VARIABLE_VAL_MASK::VAR_ATTR_DD_VAL_INFO);

                    if (true == bIsValid)
                    {
                        // Check for the Qualifiers and update the JSON accordingly
                        applyQualifiersForVariables(pEditDisplay->edit_items.list[itemIndex].bit_mask, itemInfoJson);
                        // Append to the item List
                        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                    }
                }
                // Move to the next item
                ++itemIndex;
            }

            itemIndex = {0};
            // Now add all the menu items one by one
            while (itemIndex < pEditDisplay->disp_items.count)
            {
                // Retrieve Item ID and corresponding block Tag of the item.
                itemID     = pEditDisplay->disp_items.list[itemIndex].op_id;
                itemType   = pEditDisplay->disp_items.list[itemIndex].op_type;
                if (EDIT_DISP_ITYPE == itemType)
                {
                    itemType = MENU_ITYPE;
                }
                if (WAO_ITYPE == itemType)
                {
                    itemType = VARIABLE_ITYPE;
                }
                blockTag   = itemContainerTag;

                /* Get the actual tag using Block ID and block Instance for menu items
                   contained in device level menu. This is not required for Block Level
                   menu's as the menu items in the block menu's will always belong to the same block
                */
                if (itemContainerTag.empty())
                {
                    ITEM_ID blockID = pEditDisplay->disp_items.list[itemIndex].op_block_id;
                    ITEM_ID blockInstance = pEditDisplay->disp_items.list[itemIndex].op_block_instance;

                    // Get the block Tag block ID and instance is filled by DDS
                    if (blockID !=0)
                    {
                        blockTag = m_pDDSController->getBlockTagByBlockID(blockID, blockInstance);
                    }
                }

                // Fill the common information required for al the menu items
                Json::Value itemInfoJson;
                ItemBasicInfo sItemInfo;

                // Fill menu item attributes
                sItemInfo.itemID   = itemID;
                sItemInfo.itemType = m_itemTypeMap[itemType];
                sItemInfo.itemContainerTag = blockTag;
                sItemInfo.itemInDD = true;

                if (blockTag.empty())
                {
                    /* Device Level Standard Menu's, are not associated with any blocks,
                       needs to be handled seperately
                    */
                    ENV_INFO2 envInfo2{};
                    APP_INFO appInfo{};
                    DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
                    DDI_GENERIC_ITEM    ddiGenericItem2{};
                    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};

                    // Get the current Device handle
                    status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);
                    ddiItemSpecifier.type = DDI_ITEM_ID;
                    ddiItemSpecifier.item.id = itemID;

                    if (status.hasError())
                    {
                        LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                        Error::Entry error = Error::cpm.GET_MENU_FAILED;
                        output = JsonRpcUtil::setErrorObject(error.code, error.message);
                    }

                    // Set the Environment Info for ddi_get_tem2
                    envInfo2.device_handle = deviceHandle;
                    envInfo2.type = ENV_DEVICE_HANDLE;

                    envInfo2.app_info = &appInfo;
                    appInfo.status_info = USE_EDIT_VALUE;

                    // Deveic Level standard Menu's, hence use ddi_get_item2 to get further information
                    status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                                           ALL_ITEM_ATTR_MASK, &ddiGenericItem2);

                    if (!status.hasError())
                    {
                        if (fillMenuItemInfoByType(sItemInfo, ddiGenericItem2))
                        {
                            // Fill the Basic information as Json
                            fillItemBasicJsonInfo(sItemInfo, itemInfoJson);
                            // Append to the item List
                            output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                        }
                    }
                    else
                    {
                        LOG_ERROR(CPMAPP, "DDI Get Item 2 Failed with Error : " << status.getErrorCode());
                    }

                    // Clear the Generic Item memory allocated by ddiGetItem API
                    m_pDDSController->ddiCleanItem(&ddiGenericItem2);

                    if (status.hasError())
                    {
                        LOG_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
                    }
                }
                else
                {
                    bool bIsValid = true;

                    ParamItems varItems{};
                    varItems.paramItemID = itemID;
                    varItems.paramContainerTag = blockTag;
                    varItems.paramType = JSON_MENU_ITEM_TYPE;
                    // Get the minimum information for Variable to be shown in Menu
                    itemInfoJson = getVariable(varItems, bIsValid, VARIABLE_VAL_MASK::VAR_ATTR_DD_VAL_INFO);

                    if (true == bIsValid)
                    {
                        // Check for the Qualifiers and update the JSON accordingly
                        applyQualifiersForVariables(pEditDisplay->disp_items.list[itemIndex].bit_mask, itemInfoJson);
                        // Append to the item List
                        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                    }
                }
                // Move to the next item
                ++itemIndex;
            }
        }
    }
}

/*  ----------------------------------------------------------------------------
    Processes variable Write-As-One list information
*/
Json::Value FFAdaptor::getVarWaoListItems
(
    const Json::Value& varRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value varResponse;
    PMStatus status;

    ITEM_ID itemID = 0;
    std::string itemContainerTag;
    ITEM_ID itemContainerID = 0;
    unsigned short itemSubIndex = 0;

    if ( (FAILURE == validateItemContTag(varRequest, itemContainerTag)) ||
         (FAILURE == validateItemID(varRequest, itemID)))
    {
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        return varResponse;
    }

    if (varRequest.isMember(JSON_ITEM_INFO))
    {
        if ((FAILURE == validateItemContID(varRequest[JSON_ITEM_INFO], itemContainerID)) ||
             (FAILURE == validateItemSubIndex(varRequest[JSON_ITEM_INFO], itemSubIndex)))
        {
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }
    }

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
    {
        if (varRequest.isMember(JSON_ITEM_INFO))
        {
            if (FAILURE == validateItemInDD(varRequest, true))
            {
                Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
                varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                return varResponse;
            }
        }

        DDI_BLOCK_SPECIFIER blockSpecifier{};
        blockSpecifier.type = DDI_BLOCK_TAG;
        blockSpecifier.block.tag = const_cast<char*>(itemContainerTag.c_str());

        DDI_PARAM_SPECIFIER paramSpecifier{};
        paramSpecifier.type = DDI_PS_ITEM_ID;
        paramSpecifier.item.id = itemID;

        ITEM_ID waoID;
        int errorCode = m_pDDSController->ddiGetWao(&blockSpecifier, &paramSpecifier, &waoID);
        if (SUCCESS == errorCode)
        {
            Json::Value output;
            DDI_GENERIC_ITEM genericItem{};

            DDI_BLOCK_SPECIFIER blockSpec{};
            blockSpec.type = DDI_BLOCK_TAG;
            blockSpec.block.tag = const_cast<char*>(itemContainerTag.c_str());

            DDI_ITEM_SPECIFIER itemSpec{};
            itemSpec.type = DDI_ITEM_ID;
            itemSpec.item.id = waoID;

            ENV_INFO envInfo{};
            APP_INFO appInfo{};

            envInfo.app_info = &appInfo;
            appInfo.status_info = USE_EDIT_VALUE;

            // Get the block information from the DDS
            status = m_pDDSController->ddiGetItem(&blockSpec, &itemSpec,
                                                  &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
            if (!status.hasError())
            {
                FLAT_WAO * pMenu = static_cast<FLAT_WAO *>(genericItem.item);

                if (nullptr != pMenu)
                {
                    ItemBasicInfo menuInfo;
                    // Retrieve Item ID and corresponding block Tag of the item.
                    ITEM_ID itemID{0};
                    ITEM_TYPE itemType{0};
                    std::string blockTag = itemContainerTag;

                    // Fill the Menu attributes
                    fillBasicMenuAttributes(varRequest, menuInfo);

                    menuInfo.itemType = m_itemTypeMap[MENU_ITYPE];
                    output[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;

                    // Add the menu information to output Json
                    fillItemBasicJsonInfo(menuInfo, output);

                    unsigned int itemIndex{0};
                    // Now add all the menu items one by one
                    while (itemIndex < pMenu->items.count)
                    {
                        // Retrieve Item ID and corresponding block Tag of the item.
                        itemID   = pMenu->items.list[itemIndex].op_id;
                        itemType = pMenu->items.list[itemIndex].op_type;

                        /* Get the actual tag using Block ID and block Instance for menu items
                           contained in device level menu. This is not required for Block Level
                           menu's as the menu items in the block menu's will always belong to the same block
                        */
                        if (itemContainerTag.empty())
                        {
                            ITEM_ID blockID = pMenu->items.list[itemIndex].op_block_id;
                            ITEM_ID blockInstance = pMenu->items.list[itemIndex].op_block_instance;

                            // Get the block Tag block ID and instance is filled by DDS
                            if (blockID != 0)
                            {
                                blockTag = m_pDDSController->getBlockTagByBlockID(blockID, blockInstance);
                            }
                        }

                        // Fill the common information required for al the menu items
                        Json::Value itemInfoJson;
                        ItemBasicInfo sItemInfo;

                        // Fill menu item attributes
                        sItemInfo.itemID   = itemID;
                        sItemInfo.itemType = m_itemTypeMap[itemType];
                        sItemInfo.itemContainerTag = blockTag;
                        sItemInfo.itemInDD = true;

                        if (blockTag.empty())
                        {
                            /* Device Level Standard Menu's, are not associated with any blocks,
                               needs to be handled seperately
                            */
                            ENV_INFO2 envInfo2{};
                            DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
                            DDI_GENERIC_ITEM    ddiGenericItem2{};
                            DDI_ITEM_SPECIFIER  ddiItemSpecifier{};

                            // Get the current Device handle
                            status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);
                            ddiItemSpecifier.type = DDI_ITEM_ID;
                            ddiItemSpecifier.item.id = itemID;

                            if (status.hasError())
                            {
                                LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                                Error::Entry error = Error::cpm.GET_WAO_FAILED;
                                output = JsonRpcUtil::setErrorObject(error.code, error.message);
                            }

                            // Set the Environment Info for ddi_get_tem2
                            envInfo2.device_handle = deviceHandle;
                            envInfo2.type = ENV_DEVICE_HANDLE;

                            envInfo2.app_info = &appInfo;
                            appInfo.status_info = USE_EDIT_VALUE;

                            // Deveic Level standard Menu's, hence use ddi_get_item2 to get further information
                            status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                                                   ALL_ITEM_ATTR_MASK, &ddiGenericItem2);

                            if (!status.hasError())
                            {
                                if (fillMenuItemInfoByType(sItemInfo, ddiGenericItem2))
                                {
                                    // Fill the Basic information as Json
                                    fillItemBasicJsonInfo(sItemInfo, itemInfoJson);
                                    // Append to the item List
                                    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                                }
                            }
                            else
                            {
                                LOG_ERROR(CPMAPP, "DDI Get Item 2 Failed with Error : " << status.getErrorCode());
                            }

                            // Clear the Generic Item memory allocated by ddiGetItem API
                            m_pDDSController->ddiCleanItem(&ddiGenericItem2);

                            if (status.hasError())
                            {
                                LOG_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
                            }
                        }
                        else
                        {
                            bool bIsValid = true;

                            ParamItems varItems{};
                            varItems.paramItemID = itemID;
                            varItems.paramContainerTag = blockTag;
                            varItems.paramType = JSON_VARIABLE_ITEM_TYPE;
                            // Get the minimum information for Variable to be shown in Menu
                            itemInfoJson = getVariable(varItems, bIsValid, VARIABLE_VAL_MASK::VAR_ATTR_DD_VAL_INFO);

                            if (true == bIsValid)
                            {
                                // Check for the Qualifiers and update the JSON accordingly
                                applyQualifiersForVariables(pMenu->items.list[itemIndex].bit_mask, itemInfoJson);
                                // Append to the item List
                                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                            }
                        }
                        // Move to the next item
                        ++itemIndex;
                    }
                }
            }
            return output;
        }
        else
        {
            LOG_ERROR(CPMAPP, "There is no Write-As-One relation for : " << itemID);
            Error::Entry error = Error::cpm.GET_WAO_FAILED;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid item query for VAR_WAO_LIST");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return varResponse;
}

/*  ----------------------------------------------------------------------------
    Processes variable information
*/
Json::Value FFAdaptor::processVariable
(
    const Json::Value& varRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value varResponse;
    PMStatus status;

    ITEM_ID itemID = 0;
    std::string itemContainerTag;
    std::string reqType{""};
    ITEM_ID itemContainerID = 0;
    unsigned short itemSubIndex = 0;
    unsigned long itemBitIndex = 0;

    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    DDI_GENERIC_ITEM	 genericItem{};
	DDI_BLOCK_SPECIFIER  blockSpecifier{};
	DDI_ITEM_SPECIFIER   itemSpecifier{};

    if ((FAILURE == validateItemContTag(varRequest, itemContainerTag)) || 
        (FAILURE == validateItemID(varRequest, itemID)))
    {
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        return varResponse;
    }
    
    if (varRequest.isMember(JSON_ITEM_INFO))
    {
        if ((FAILURE == validateItemContID(varRequest[JSON_ITEM_INFO], itemContainerID)) ||
            (FAILURE == validateItemSubIndex(varRequest[JSON_ITEM_INFO], itemSubIndex)) ||
            (FAILURE == validateItemBitIndex(varRequest[JSON_ITEM_INFO], itemBitIndex)))
        {
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }
    }

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
    {
        if ((false == varRequest.isMember(JSON_ITEM_INFO)) && (FAILURE == validateItemInDD(varRequest, true)))
        {
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }

        bool itemHasRelation = false;
        validateItemHasRelation(varRequest[JSON_ITEM_INFO], itemHasRelation);
        if (itemHasRelation)
        {
            return getVarWaoListItems(varRequest, ITEMQUERY);
        }
        else
        {
            ParamItems varItems{};
            bool bIsValid = true;
            varItems.paramItemID = itemID;
            varItems.paramContainerTag = itemContainerTag;
            varItems.paramContainerID = itemContainerID;
            varItems.paramSubIndex = itemSubIndex;
        	varItems.paramBitIndex = itemBitIndex;

            varResponse = getVariable(varItems, bIsValid);
        }
    }
    else
    {
        if (FAILURE == validateItemReqType(varRequest, reqType))
        {
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
            return varResponse;
        }

        (void)memset((char *)&genericItem, 0, sizeof(DDI_GENERIC_ITEM));
		envInfo.app_info = &appInfo;
		appInfo.status_info = USE_EDIT_VALUE;

		blockSpecifier.type = DDI_BLOCK_TAG;
		blockSpecifier.block.tag = const_cast<char*>(itemContainerTag.c_str());

		itemSpecifier.type = DDI_ITEM_ID;
		itemSpecifier.item.id = itemID;


		status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier,
											  &envInfo,
                                              VAR_POST_EDIT_ACT|VAR_PRE_EDIT_ACT ,&genericItem);

        FLAT_VAR *flatVar = static_cast<FLAT_VAR*>(genericItem.item);


        if (!reqType.empty() && (0 == reqType.compare(JSON_VAR_TYPE_EDIT)))
        {
            // check if any pre-edit function is present
			if((flatVar->actions->pre_edit_act.count) > NO_ACTION)
			{
				editData.itemId = flatVar->id;
				editData.blockTag = std::string(blockSpecifier.block.tag);
				editData.actionTypes  = EditActionTypes::PRE_EDIT_ACTION;
                editData.actionItemTypes = EditActionItemTypes::VARIABLE_ACTION;

				 // create edit action thread
				if (THREAD_STATE_INVALID == getEditActionThreadState())
				{
					// command response publisher with message.
					Error::Entry error = Error::cpm.PRE_EDIT_ACTION_IS_IN_PROGRESS;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

					// create edit action thread
					createEditActionThread();
				}
				else
				{
                    LOGF_ERROR(CPMAPP, "Failed to start pre edit Action thread, already running");
					status.setError(FF_ADAPTOR_ERR_INVALID_STATE_OF_PRE_EDIT_ACTION_THREAD,
									PM_ERR_CL_FF_ADAPTOR);
					Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
				}
			}
			else
			{
                LOGF_INFO(CPMAPP,"Variable has no pre Edit actions with Item ID : " <<itemID);
				// command response publisher  with no edit action.
				Error::Entry error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
				varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
			}
        }
        else if (!reqType.empty() && (0 == reqType.compare(JSON_VAR_TYPE_UPDATE)))
        {
            // Requested set item action is update variable
            if (varRequest.isMember(JSON_ITEM_INFO))
            {
            	// check if any post edit function is present
                if((flatVar->actions->post_edit_act.count) > NO_ACTION)
                {
                    // Write the edited variable values to parameter cache for post edit action validation
                    ParamInfo editParamInfo{};
                    editParamInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());
                    VariableType editVarType(itemID);
                    editVarType.containerId = itemContainerID;
                    editVarType.subIndex = itemSubIndex;
                    editVarType.bitIndex = itemBitIndex;
                    editParamInfo.paramList.push_back(editVarType);

                    // Get the parameter information of the variable for post edit action
                    PMStatus getParamStatus = m_pFFParameterCache->getParameter(&editParamInfo);
                    if (getParamStatus.hasError())
                    {
                        // clear the paramInfo structure and return error response
                        m_pFFParameterCache->cleanParamInfo(&editParamInfo);
                        LOGF_ERROR(CPMAPP, "Getting Parameter Info Failed");
                        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                        return varResponse;
                    }
                    
                    // Set the value entered in IC
                    Json::Value itemInfo = varRequest[JSON_ITEM_INFO];
                    PMStatus setValStatus = setVariableValue(itemInfo,editVarType);
                    if (setValStatus.hasError())
                    {
                        // clear the paramInfo structure and return error response
                        m_pFFParameterCache->cleanParamInfo(&editParamInfo);
                        LOGF_ERROR(CPMAPP, "Setting Variable Value failed");
                        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                        return varResponse;
                    }
                    
                    // Update the parameter cache with IC value
                    PMStatus editVarStatus = m_pFFParameterCache->writeEditValue(&editParamInfo);
                    if (editVarStatus.hasError())
                    {
                        // clear the paramInfo structure and return error response
                        m_pFFParameterCache->cleanParamInfo(&editParamInfo);
                        LOGF_ERROR(CPMAPP, "Write to Cache of Edited Values Failed");
                        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
                        varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                        return varResponse;
                    }
                	
                    editData.itemId = flatVar->id;
					editData.blockTag = std::string(blockSpecifier.block.tag);
					editData.actionTypes  = EditActionTypes::POST_EDIT_ACTION;
                    editData.actionItemTypes = EditActionItemTypes::VARIABLE_ACTION;

					// Create edit action  thread
					if (THREAD_STATE_INVALID == getEditActionThreadState())
					{
						// command response publisher with message "POST_EDIT_ACTION_IN_PROGRESS"
						Error::Entry error = Error::cpm.POST_EDIT_ACTION_IS_IN_PROGRESS;
						varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);

						// createThread for edit actions					
						createEditActionThread();
					}
					else
					{
                        LOGF_ERROR(CPMAPP, "Failed to start post edit Action thread, already running");
						status.setError(FF_ADAPTOR_ERR_INVALID_STATE_OF_POST_EDIT_ACTION_THREAD,
										PM_ERR_CL_FF_ADAPTOR);
						Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
						varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
					}
                }
                else
                {
                    LOGF_INFO(CPMAPP,"Variable has no post Edit actions with Item ID : " <<itemID);
                    // command response publisher  with no edit action.
					Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
					varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
                }
            }
        }
		else if (!reqType.empty() && (0 == reqType.compare(JSON_VAR_TYPE_WRITE)))
		{
			Json::Value itemInfo = varRequest[JSON_ITEM_INFO];

			ParamItems varItems;
			varItems.paramItemID = itemID;
			varItems.paramContainerTag = itemContainerTag;
			varItems.paramContainerID = itemContainerID;
			varItems.paramSubIndex = itemSubIndex;
			varItems.paramBitIndex = itemBitIndex;

			//setting the variable
			varResponse = setVariable(varItems, itemInfo);
		}
		else
		{
        	LOGF_ERROR(CPMAPP, "ItemInfo is unavailable in setItem request");
			// ItemInfo is unavailable; return error
			Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
			varResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        } // !-- else if (UPDATE)

        // Clear the Generic Item memory allocated by ddiGetItem API
        m_pDDSController->ddiCleanItem(&genericItem);
    } // !--else (ITEM_QUERY::SET_ITEM == ITEMQUERY)

    return varResponse;
}

/*  ----------------------------------------------------------------------------
    Creates Edit Action timer thread
*/
void FFAdaptor::createEditActionThread()
{
    m_threadIdLock.lock();

    std::function<void(const void *)> methodFuncPtr =
                         std::bind(&FFAdaptor::editActionThreadProc, this,this);
    m_editActionThreadId = ThreadManager::getInstance()->createThread(methodFuncPtr,this);
    m_threadIdLock.unlock();
}

/*  ----------------------------------------------------------------------------
    Destroys edit action thread
*/
void FFAdaptor::destroyEditActionThread()
{
	setEditActionThreadState(THREAD_STATE_STOP);
    ThreadManager::getInstance()->destroyThread(m_editActionThreadId);
    m_editActionThreadId = 0;
}

/*  ----------------------------------------------------------------------------
    Implements edit action thread
*/
void FFAdaptor::editActionThreadProc(const void* arg)
{
    m_threadIdLock.lock();

    FFAdaptor* pInstance = static_cast<FFAdaptor*>(const_cast<void*>(arg));
    m_threadIdLock.unlock();
    pInstance->processEditAction();
    destroyEditActionThread();
}


/*  ----------------------------------------------------------------------------
    Processes menu information
*/
Json::Value FFAdaptor::processMenu
(
    const Json::Value& menuRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value menuResponse;
    (void)ITEMQUERY;

    if (SUCCESS == validateItemInDD(menuRequest, true))
    {
        ITEM_ID itemID;
        string blockTag;

        // Validate the Item ID and Item Container Tag
        if (menuRequest.isMember(JSON_ITEM_ID) &&
            menuRequest[JSON_ITEM_ID].isIntegral() &&
            (menuRequest[JSON_ITEM_ID].asLargestInt() >= 0) &&
            menuRequest.isMember(JSON_ITEM_CONT_TAG) &&
            menuRequest[JSON_ITEM_CONT_TAG].isString())
        {
            itemID = menuRequest[JSON_ITEM_ID].asLargestUInt();
            blockTag = menuRequest[JSON_ITEM_CONT_TAG].asString();

            ITEM_TYPE itemType = m_pDDSController->getItemType(itemID, blockTag);

            if (MENU_ITYPE == itemType)
            {
                // gives menu information
                getDDMenu(menuRequest, menuResponse);
            }
            else if(EDIT_DISP_ITYPE == itemType)
            {
                // gives edit display information
                getEditDisplay(menuRequest, menuResponse);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Invalid Item ID or Item Container Tag");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
            menuResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        getCustomMenu(menuRequest, menuResponse);
    }

    return menuResponse;
}

/*  ----------------------------------------------------------------------------
    Processes Block information
*/
Json::Value FFAdaptor::processBlock
(
    const Json::Value& blockRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value blockResponse;
    (void)ITEMQUERY;

    // Get the Block information
    getBlockMenu(blockRequest, blockResponse);

    return blockResponse;
}

/*  ----------------------------------------------------------------------------
    Processes method information
    Authored By: Utthunga
*/
Json::Value FFAdaptor::processMethod
(
    const Json::Value& methodRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value methodResponse;

    if (ITEM_QUERY::GET_ITEM == ITEMQUERY)
    {
        methodResponse = processGetMethodExecution(methodRequest);
    }
    else if (ITEM_QUERY::SET_ITEM == ITEMQUERY)
    {
        methodResponse = processSetMethodExecution(methodRequest);
    }

    return methodResponse;
}

/*  ----------------------------------------------------------------------------
    Executes respective mapped method through function pointer based on item type
*/
Json::Value FFAdaptor::executeMappedMethod
(
    const Json::Value& itemRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value response;

    // Fetch the corresponding function pointer from the map
    std::string itemType = itemRequest[JSON_ITEM_TYPE].asString();

    auto methodItr = processMethodMap.find(itemType);

    if (methodItr != processMethodMap.end())
    {
        // Method available inside map; execute it
        response = (*this.*(methodItr->second))(itemRequest, ITEMQUERY);
    }
    return response;
}

/* GET ITEM METHODS ***********************************************************/
/*  ----------------------------------------------------------------------------
    Retrieves Variable value with attributes
*/
Json::Value FFAdaptor::getVariable
(
    ParamItems& varItems,
    bool& bIsValid,
    VARIABLE_VAL_MASK varValMask
)
{
    Json::Value getVarResp;
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;
    VariableType readVar{};

    ParamInfo paramInfo;
    paramInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
    paramInfo.requestInfo.maskType = varItems.paramMaskType;

    ITEM_TYPE itemType = m_pDDSController->getItemType(varItems.paramItemID,
                                                       varItems.paramContainerTag);

    switch (itemType)
    {
        case VARIABLE_ITYPE:
        {
            readVar.itemID = varItems.paramItemID;
            readVar.subIndex = varItems.paramSubIndex;
            readVar.containerId = varItems.paramContainerID;
            readVar.itemContainerTag.str = paramInfo.blockTag.str;
            if (varItems.paramBitIndex > 0)
            {
                readVar.bitIndex = varItems.paramBitIndex;
            }

            paramInfo.paramList.push_back(readVar);

            if (varItems.paramItemID == BLOCK_TAG_ID)
            {
                status = m_pFFParameterCache->getBlockCharacteristics(&paramInfo,
                                                                      DEFAULT_PARAMS);
            }
            else
            {
                // Call getParameter and retrieve parameter information
                status = m_pFFParameterCache->getParameter(&paramInfo);
            }
        }
        break;

        case RECORD_ITYPE:
        {
            ParamInfo recParamInfo{};
            recParamInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
            recParamInfo.requestInfo.maskType = static_cast<MaskType>(varItems.paramMaskType);

            if (varItems.paramItemID == BLOCK_TAG_ID)
            {
                status = m_pFFParameterCache->getBlockCharacteristics(&recParamInfo,
                                                                      DEFAULT_PARAMS);

                if (!status.hasError())
                {
                    // In Characteristics record, first element is Block Tag, so
                    // no need to query for it 
                    ItemInfoType& blockItem = recParamInfo.paramList.at(0);
                    readVar = dynamic_cast<VariableType&>(blockItem);
                    paramInfo.paramList.push_back(readVar);
                }
            }
            else
            {
                RecordType recType{varItems.paramContainerID};
                recType.subIndex = varItems.paramSubIndex;
                recParamInfo.paramList.push_back(recType);

                // Call getParameter and retrieve parameter information
                status = m_pFFParameterCache->getParameter(&recParamInfo);

                if (!status.hasError())
                {
                    ItemInfoType& recType = recParamInfo.paramList.at(0);
                    RecordType& recItem = dynamic_cast<RecordType&>(recType);

                    if (!recItem.members.empty())
                    {
                        ItemInfoType& item = recItem.members.at(0);
                        readVar = dynamic_cast<VariableType&>(item);
                        paramInfo.paramList.push_back(readVar);
                    }
                }
                
                else
                {
                    LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
                    error = Error::cpm.GET_PARAMETER_FAILED;
                }
            }
        }
        break;

        case ARRAY_ITYPE:
        {
            ParamInfo arrParamInfo{};
            arrParamInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
            arrParamInfo.requestInfo.maskType = static_cast<MaskType>(varItems.paramMaskType);

            ArrayType arrType{varItems.paramContainerID};
            arrParamInfo.paramList.push_back(arrType);

            // Call getParameter and retrieve parameter information
            status = m_pFFParameterCache->getParameter(&arrParamInfo);

            if (!status.hasError())
            {
                ItemInfoType& arrType = arrParamInfo.paramList.at(0);
                ArrayType& arrItem = dynamic_cast<ArrayType&>(arrType);

                if (!arrItem.members.empty())
                {
                    ItemInfoType& item = arrItem.members.at(0);
                    readVar = dynamic_cast<VariableType&>(item);
                    paramInfo.paramList.push_back(readVar);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
                error = Error::cpm.GET_PARAMETER_FAILED;
            }
        }
        break;

        default:
        {
            error = Error::cpm.GET_PARAMETER_FAILED;
            status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
            LOGF_ERROR(CPMAPP, "Item type not supported!!" << itemType);
        }
        break;
    }

    if (!status.hasError())
    {
        if (readVar.validity)
        {
            // Fill itemBasicInfo structure
            VarBasicInfo varBasicInfo{};
            varBasicInfo.basicInfo.itemID = readVar.itemID;
            
            if (readVar.bitIndex > 0)
            {
                BitEnumItemStr bitEnumItem;
                bitEnumItem.bitIndex = readVar.bitIndex; 
                getBitEnumItemStr(&readVar,bitEnumItem);
                varBasicInfo.basicInfo.itemHelp = bitEnumItem.helpStr;
                varBasicInfo.basicInfo.itemLabel = bitEnumItem.labelStr;
            }
            else
            {
                varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(readVar.help.str, readVar.help.len);
                varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(readVar.label.str, readVar.label.len);
            }

            varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
            varBasicInfo.basicInfo.itemContainerTag = varItems.paramContainerTag;
            if (READ_HANDLING != readVar.misc.handling)
            {
                varBasicInfo.itemReadOnly = false;
            }
            varBasicInfo.itemClass = readVar.misc.class_attr;

            // Fill the basic information in JsonResponse
            fillVarBasicJsonInfo(varBasicInfo, getVarResp);

            // Fill the variable attribute information
            fillVariableJsonInfo(&readVar, getVarResp);
        }
        else
        {
            // If DD item validity is not valid, dont show as part of menu
            bIsValid = (0 != readVar.validity);
            LOGF_INFO(CPMAPP, "Validity of item " << readVar.itemID << " is false");
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
        error = Error::cpm.GET_PARAMETER_FAILED;
    }

    // Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&paramInfo);

    if (status.hasError())
    {
        getVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    return getVarResp;
}

/*  ----------------------------------------------------------------------------
    Retrieves Block Parameters with attributes and value
    Authored By: Utthunga
*/
void FFAdaptor::getBlockParams
(
    std::string itemContainerTag,
    Json::Value& menuJsonResp
)
{
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;
    /* Set the required informtion such as container tag and mask type into
       ParamInfo structure to retrieve all parameters from specific block
    */
    ParamInfo blockParamInfo{};
    blockParamInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());

    // Call getParameter and retrieve parameter information
    PMStatus status;
    status = m_pFFParameterCache->getParameter(&blockParamInfo);

    if (!status.hasError())
    {
        // Fill Json response with parameter inforation
        fillBlockParamsJson(&blockParamInfo, menuJsonResp);
    }

    else
    {
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
        error = Error::cpm.GET_PARAMETER_FAILED;
    }

    // Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&blockParamInfo);

    if (status.hasError())
    {
        menuJsonResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Retrieves Record parameters with attributes and value
    Authored By: Utthunga
*/
void FFAdaptor::getRecord
(
    ParamItems& recReqItems,
	Json::Value& recordJsonResp,
    unsigned short qualifier    
)
{
    /* Set the required informtion such as itemID,container tag and mask type into
       ParamInfo structure to retrieve all records from specific block
    */
    ParamInfo recParamInfo{};
    recParamInfo.requestInfo.maskType = recReqItems.paramMaskType;
    recParamInfo.blockTag.str = const_cast<char*>(recReqItems.paramContainerTag.c_str());

    RecordType recType{recReqItems.paramItemID};
    recType.subIndex = recReqItems.paramSubIndex;
    recParamInfo.paramList.push_back(recType);
    unsigned long bitIndex = recReqItems.paramBitIndex;

    // Call getParameter and retrieve parameter information
    PMStatus status;
    status = m_pFFParameterCache->getParameter(&recParamInfo);

    if (!status.hasError())
    {
        ItemInfoType& type = recParamInfo.paramList.at(0);
        RecordType& recItemType = dynamic_cast<RecordType&>(type);
        if (0 == recReqItems.paramSubIndex)
        {
            // SubIndex 0, retrieve all variable members of Record
            if (true == recItemType.validity)
            {
            	// Fill Json response with record members inforation
                fillRecordJsonInfo(recItemType, recordJsonResp, recReqItems.paramContainerTag);
            }
            else
            {
                LOGF_INFO(CPMAPP, "Validity of item " << recItemType.itemID << " is false");
            }
        }
        else
        {
            if (recItemType.members.size() > 0)
            {
                // SubIndex is not 0, retrieve corresponding variable member of Record
				Json::Value varJsonResp;
                std::string recordName = validateAndGetStringValue(recItemType.label.str, recItemType.label.len);
                ItemInfoType& type = recItemType.members.at(0);
                VariableType& varItemType = dynamic_cast<VariableType&>(type);

	            if (true == varItemType.validity)
                {   
                    // Fill Record header structure
                    VarBasicInfo varBasicInfo{};
                    varBasicInfo.basicInfo.itemID = varItemType.itemID;
					varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varItemType.help.str, varItemType.help.len);
                    if (bitIndex > 0) 
                    {
                        BitEnumItemStr bitEnumItem;
                        bitEnumItem.bitIndex = bitIndex; 
                        getBitEnumItemStr(&varItemType,bitEnumItem);    
                        varBasicInfo.basicInfo.itemHelp = bitEnumItem.helpStr;
                        varBasicInfo.basicInfo.itemLabel = bitEnumItem.labelStr;
						varItemType.bitIndex = recReqItems.paramBitIndex;
                    }    
                    else 
                    {    
                        if (recordName.empty())
		                {
                    		varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(varItemType.label.str, varItemType.label.len);
		                }
		                else
		                {
		                    varBasicInfo.basicInfo.itemLabel = recordName + " " + validateAndGetStringValue(varItemType.label.str, varItemType.label.len);
		                }
                    }  
                    varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
                    varBasicInfo.basicInfo.itemContainerTag = recReqItems.paramContainerTag;

                    varItemType.bitIndex = recReqItems.paramBitIndex;

                    if (READ_HANDLING != varItemType.misc.handling)
                    {
                        varBasicInfo.itemReadOnly = false;
                    }
                    varBasicInfo.itemClass = varItemType.misc.class_attr;

	                // Fill the basic information in JsonResponse
	                fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

	                fillVariableJsonInfo(&varItemType, varJsonResp);
					
					// Append the Variable Json response into ItemList of Record response
			        if (!varJsonResp.empty())
			        {
                        // Check for the Qualifiers and update the JSON accordingly
                        applyQualifiersForVariables(qualifier, varJsonResp);
			            recordJsonResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(varJsonResp);
			        }
	            }
	            else
	            {
	                // If DD item validity is not valid, dont show as part of menu
                    LOGF_INFO(CPMAPP, "Validity of item " << varItemType.itemID << " is false");
	            }
			}
        }
    }

    else
    {
        // get parameter failed!!
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
    }

    // Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&recParamInfo);
}

/*  ----------------------------------------------------------------------------
    Retrieves Record Variable parameters with attributes and value
*/
void FFAdaptor::getRecordVar
(
    ParamItems& recVarItems,
    Json::Value& recVarJsonResp,
    unsigned short qualifier    
)
{
    std::string recordName;
    // Call getParameter and retrieve parameter information
    PMStatus status;
	// Special handling for retreiving the record name for the (Characteristics) Block Tag parameter
	DDI_BLOCK_SPECIFIER ddiBlockSpecifier{};
	DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
	DDI_GENERIC_ITEM    ddiGenericItem{};
	ENV_INFO envInfo{};
	APP_INFO appInfo{};
	// Fill the Item Specifier
	ddiItemSpecifier.type = DDI_ITEM_ID;
	ddiItemSpecifier.item.id = recVarItems.paramContainerID;

    // Fill the Block Specifier
    ddiBlockSpecifier.type = DDI_BLOCK_TAG;
    ddiBlockSpecifier.block.tag = const_cast<char*>(recVarItems.paramContainerTag.c_str());
    envInfo.app_info = &appInfo;

    // Get the Record Name by calling ddi_get_item on the container ID
    status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier,
                                          &envInfo, ALL_ITEM_ATTR_MASK, &ddiGenericItem);
	if (!status.hasError())
	{	
		FLAT_RECORD* pRecord = static_cast<FLAT_RECORD*>(ddiGenericItem.item);
	    if (pRecord != nullptr)
		{
			if (pRecord->valid == true)
        	{
				// Store the Record Name
		    	recordName = validateAndGetStringValue(pRecord->label.str, pRecord->label.len);
			}
			else
			{
				// If DD item validity is not valid, dont show as part of menu
				LOGF_INFO(CPMAPP, "Validity of the Record ID = " << recVarItems.paramContainerID << " is false");
				// Clear the Generic Item memory allocated by ddiGetItem API
		        m_pDDSController->ddiCleanItem(&ddiGenericItem);
				return;
			}
	    }
	}
	else                
    {   
       	// get parameter failed!!
	    LOGF_ERROR(CPMAPP, "DDI get Item Failed for the Record !! Item ID = " << recVarItems.paramContainerID);
		// Clear the Generic Item memory allocated by ddiGetItem API
        m_pDDSController->ddiCleanItem(&ddiGenericItem);
        return;
    }

	// Record is valid, now get the variable information
    ParamInfo varParamInfo{};
    varParamInfo.blockTag.str = const_cast<char*>(recVarItems.paramContainerTag.c_str());
    VariableType varType{recVarItems.paramItemID};
    varType.containerId = recVarItems.paramContainerID;
    varType.subIndex = recVarItems.paramSubIndex;
	unsigned long bitIndex = recVarItems.paramBitIndex;
    varParamInfo.paramList.push_back(varType);
    
    PMStatus varStatus;
    if (recVarItems.paramItemID == BLOCK_TAG_ID)
    {
        varStatus = m_pFFParameterCache->getBlockCharacteristics(&varParamInfo,
                                                                      DEFAULT_PARAMS);
    }
    else
    {
    	varStatus = m_pFFParameterCache->getParameter(&varParamInfo);
    }
    
    if (!varStatus.hasError())
    {

	    ItemInfoType& type = varParamInfo.paramList.at(0);
	    VariableType& varItemType = dynamic_cast<VariableType&>(type);

	    if (true == varItemType.validity)
	    {   
			Json::Value varJsonResp;
			VarBasicInfo varBasicInfo{};
			varBasicInfo.basicInfo.itemID = varItemType.itemID;
			varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varItemType.help.str, varItemType.help.len);
		
            if (bitIndex > 0) 
            {
                BitEnumItemStr bitEnumItem;
                bitEnumItem.bitIndex = bitIndex; 
                getBitEnumItemStr(&varItemType,bitEnumItem);    
                varBasicInfo.basicInfo.itemHelp = bitEnumItem.helpStr;
                varBasicInfo.basicInfo.itemLabel = bitEnumItem.labelStr;
				varItemType.bitIndex = bitIndex;
            }
			else
			{    
				if (recordName.empty())
				{
					varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(varItemType.label.str, varItemType.label.len);
				}
				else
				{
					varBasicInfo.basicInfo.itemLabel = recordName + " " + validateAndGetStringValue(varItemType.label.str, varItemType.label.len);
				}
			}

			varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
			varBasicInfo.basicInfo.itemContainerTag = recVarItems.paramContainerTag;

			if (READ_HANDLING != varItemType.misc.handling)
			{
			    varBasicInfo.itemReadOnly = false;
			}
			varBasicInfo.itemClass = varItemType.misc.class_attr;

			// Fill the basic information in JsonResponse
			fillVarBasicJsonInfo(varBasicInfo, varJsonResp);
			fillVariableJsonInfo(&varItemType, varJsonResp);
					
			// Append the Variable Json response into ItemList of Record response
			if (!varJsonResp.empty())
			{
			    // Check for the Qualifiers and update the JSON accordingly
		    	applyQualifiersForVariables(qualifier, varJsonResp);
			    recVarJsonResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(varJsonResp);
			}
	    }
	    else
	    {
			// If DD item validity is not valid, dont show as part of menu
			LOGF_INFO(CPMAPP, "Validity of item " << varItemType.itemID << " is false");
	    }
    }

    else
    {
        // get parameter failed!!
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
    }

	// Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&varParamInfo);
}
/*  ----------------------------------------------------------------------------
    Retrieves Array parameters with attributes and value
*/
void FFAdaptor::getArray
(
    ParamItems& arrReqItems,
	Json::Value& arrayJsonResp,
    unsigned short qualifier    
)
{
    /* Set the required informtion such as container tag and mask type into
       ParamInfo structure to retrieve array parameters from specific block
    */
    ParamInfo arrParamInfo{};

    ArrayType arrType{arrReqItems.paramItemID};
    arrParamInfo.requestInfo.maskType = arrReqItems.paramMaskType;
    arrParamInfo.blockTag.str = const_cast<char*>(arrReqItems.paramContainerTag.c_str());
    arrParamInfo.paramList.push_back(arrType);
    arrType.subIndex = arrReqItems.paramSubIndex;
    unsigned int bitIndex = arrReqItems.paramBitIndex;

    // Call getParameter and retrieve array information
    PMStatus status;
    status = m_pFFParameterCache->getParameter(&arrParamInfo);

    if (!status.hasError())
    {
        ItemInfoType& type = arrParamInfo.paramList.at(0);
        ArrayType& arrItemType = dynamic_cast<ArrayType&>(type);

        if (0 == arrReqItems.paramSubIndex)
        {
            // SubIndex 0, retrieve all variable members of array
            if (true == arrItemType.validity)
            {
            	// Fill Json response with array members inforation
                fillArrayJsonInfo(arrItemType, arrayJsonResp, arrReqItems.paramContainerTag);
            }
            else
            {
                LOGF_INFO(CPMAPP, "Validity of item " << arrItemType.itemID << " is false");
            }
        }
        else
        {
            // SubIndex is not 0, retrieve only correspnding array variable
            if (arrItemType.members.size() > 0) 
            {
                ItemInfoType& type = arrItemType.members.at((0));
                VariableType& varItemType = dynamic_cast<VariableType&>(type);

                if (true == varItemType.validity)
                {
                    Json::Value varJsonResp;
                    VarBasicInfo varBasicInfo{};
                    varBasicInfo.basicInfo.itemID = varItemType.itemID;
					
	                if (bitIndex > 0) 
	                {
	                    BitEnumItemStr bitEnumItem;
	                    bitEnumItem.bitIndex = bitIndex; 
	                    getBitEnumItemStr(&varItemType,bitEnumItem);    
	                    varBasicInfo.basicInfo.itemHelp = bitEnumItem.helpStr;
	                    varBasicInfo.basicInfo.itemLabel = bitEnumItem.labelStr;
	                    varItemType.bitIndex = bitIndex;
	                }    
	                else 
	                {
						unsigned int subIndex = varItemType.subIndex;
                   		if (subIndex > 0)
	                    {
	                        // According to FCG_TS61804-4_2.0__PR3_EDD-Interpretation.pdf
	                        // Array labels must be prefixed with Array_Label[Array_Index]
	                        varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(arrItemType.label.str, arrItemType.label.len) + getArrayIndex(subIndex);
	                    }    
	                    varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varItemType.help.str, varItemType.help.len);
	                }  
	                varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
	                varBasicInfo.basicInfo.itemContainerTag = arrReqItems.paramContainerTag;

                    if (READ_HANDLING != varItemType.misc.handling)
                    {
                        varBasicInfo.itemReadOnly = false;
                    }
                    varBasicInfo.itemClass = varItemType.misc.class_attr;

                    // Fill the basic information in JsonResponse
                    fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

                    fillVariableJsonInfo(&varItemType, varJsonResp);
					
                    // Append the Variable Json response into ItemList of Record response
			        if (!varJsonResp.empty())
			        {
                        // Check for the Qualifiers and update the JSON accordingly
                        applyQualifiersForVariables(qualifier, varJsonResp);
			            arrayJsonResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(varJsonResp);
			        }
                }
                else
                {
                    // If DD item validity is not valid, dont show as part of menu
                    LOGF_INFO(CPMAPP, "Validity of item " << varItemType.itemID << " is false");
                }
            }
        }
    }
    else
    {
        // get parameter failed!!
        LOGF_ERROR(CPMAPP, "GetParameter Failed!!");
    }

    // Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&arrParamInfo);
}
/*  ----------------------------------------------------------------------------
    Retrieves Image value with attributes
    Authored By: Utthunga
*/
Json::Value FFAdaptor::getImage
(
    ITEM_ID& itemID,
    string   itemContainerTag,
    bool& bIsValid
)
{
    Json::Value getVarResp;

    DDI_BLOCK_SPECIFIER ddiBlockSpecifier{};
    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
    DDI_GENERIC_ITEM    ddiGenericItem{};
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    PMStatus status;

    // Fill the Item Specifier
    ddiItemSpecifier.type = DDI_ITEM_ID;
    ddiItemSpecifier.item.id = itemID;

    if (itemContainerTag.empty())
    {
        /* Device Level Standard Menu's, are not associated with any blocks,
           needs to be handled separately
        */
        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

        // Get the current Device handle
        status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
            Error::Entry error = Error::cpm.GET_ITEM_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }

        // Set the Environment Info for ddi_get_tem2
        ENV_INFO2 envInfo2{};
        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;

        envInfo2.app_info = &appInfo;
        appInfo.status_info = USE_EDIT_VALUE;

        // Device Level standard Menu's, hence use ddi_get_item2
        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                               ALL_ITEM_ATTR_MASK, &ddiGenericItem);
    }
    else
    {
        // Fill the Block Specifier
        ddiBlockSpecifier.type = DDI_BLOCK_TAG;
        ddiBlockSpecifier.block.tag = const_cast<char*>(itemContainerTag.c_str());
        envInfo.app_info = &appInfo;

        // Must be Block Level Menu's - Hence use ddi_get_item
        status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier,
                                              &envInfo, ALL_ITEM_ATTR_MASK, &ddiGenericItem);
    }

    if (!status.hasError())
    {
        FLAT_IMAGE* pImage = static_cast<FLAT_IMAGE *>(ddiGenericItem.item);
        if (pImage != nullptr)
        {
            if (pImage->valid)
            {
                ItemBasicInfo varBasicInfo{};
                varBasicInfo.itemID = itemID;
                varBasicInfo.itemInDD = true;
                varBasicInfo.itemType = JSON_IMAGE_ITEM_TYPE;
                varBasicInfo.itemContainerTag = itemContainerTag;
                varBasicInfo.itemLabel = validateAndGetStringValue(pImage->label.str, pImage->label.len);
                varBasicInfo.itemHelp = validateAndGetStringValue(pImage->help.str, pImage->help.len);

                ImageType imageParam(itemID);
                imageParam.data = pImage->entry.data;
                imageParam.length = pImage->entry.length;

                // Fill the basic information in JsonResponse
                fillItemBasicJsonInfo(varBasicInfo, getVarResp);

                // Fill the variable attribute information
                fillImageJsonInfo(&imageParam, getVarResp);
            }
            else
            {
                bIsValid = (0 != pImage->valid);
                LOGF_INFO(CPMAPP, "Validity of item " << itemID << " is false");
            }
        }
    }

    // Clear the Generic Item memory allocated by ddiGetItem API
    m_pDDSController->ddiCleanItem(&ddiGenericItem);

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_ITEM_FAILED;
        getVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    
    return getVarResp;
}

/*  ----------------------------------------------------------------------------
    Creates image file by using image raw data
    Authored By: Utthunga
*/
const char* FFAdaptor::storeImage(ImageType* imageParam)
{
    char* imagePath = nullptr;
    if (nullptr != imageParam && nullptr != imageParam->data
        && imageParam->length > 0)
    {
        char oBuffer[MAX_IMAGE_FILENAME_LEN];
        snprintf(oBuffer, sizeof(oBuffer), "%ld", imageParam->itemID);
        strcat(oBuffer, REPLACE_RANDOM_STRING);
        
        IAppConfigParser *pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();
        if (nullptr != pAppConfigParser)
        {
            std::string imageFile;     
            imageFile = pAppConfigParser->getImagePath() + "/" + std::string(oBuffer);

            memset (oBuffer, '\0', MAX_IMAGE_FILENAME_LEN);
            strncpy (oBuffer, imageFile.c_str(), imageFile.length());
        }
    
        char* ouputFileName = oBuffer;

        // Create a image file with trailing "XXXXXX" character at the end of file name
        int fd = mkstemp(ouputFileName);
        // Write the image content to the file
        write(fd, imageParam->data, imageParam->length);
        // Close the file descriptor
        close(fd);
        // Get the complete image file name
        imagePath = realpath(ouputFileName, NULL);
    }
    return imagePath;
}

/*  ----------------------------------------------------------------------------
    Processes Get Item for Method execution
    Authored By: Utthunga
*/
Json::Value FFAdaptor::processGetMethodExecution(const Json::Value& data)
{
    PMStatus status;
    Json::Value jsonResponse = Json::Value("ok");

    if (CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_METHOD_EXEC_IN_PROGRESS)
    {
        LOGF_ERROR(CPMAPP, "Failed to start thread, already running");
        Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    ITEM_ID itemID = 0;
    std::string itemContainerTag = "";

    if (SUCCESS == validateItemID(data, itemID) &&
        SUCCESS == validateItemContTag(data, itemContainerTag))
    {
        ITEM_ID blkId = 0;
        DDI_ITEM_SPECIFIER   itemSpecifier{};
        DDI_GENERIC_ITEM     genericItem{};

        if (!itemContainerTag.empty())
        {
            BLOCK_HANDLE blockHandle = m_pDDSController->getBlockHandleByBlockTag(
                                       itemContainerTag.c_str());
            if (blockHandle < 0)
            {
                LOGF_ERROR(CPMAPP, "Failed to get block handle");
                Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }

            status = m_pDDSController->getBlockIDByBlockHandle(blockHandle, &blkId);
            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "Failed to get block id");
                Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        DDSController::s_pMethodExecutionData = {};
        DDSController::s_pMethodExecutionData.blockID = blkId;
        DDSController::s_pMethodExecutionData.methodID = itemID;

        // Fill the item Specifier information
        itemSpecifier.type = DDI_ITEM_ID;
        itemSpecifier.item.id = itemID;

        if (blkId > 0)
        {
            DDI_BLOCK_SPECIFIER  blockSpecifier{};
            ENV_INFO envInfo{};
            APP_INFO appInfo{};

            envInfo.app_info = &appInfo;
            appInfo.status_info = USE_EDIT_VALUE;

            // Fill the Block Specifier Attributes
            blockSpecifier.type = DDI_BLOCK_TAG;
            blockSpecifier.block.tag = const_cast<char*>(itemContainerTag.c_str());

            // Get the block information from the DDS
            status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier,
                                                  &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
        }
        else
        {
            ENV_INFO2 envInfo2{};
            APP_INFO appInfo{};

            DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
            status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }

            // Set the Environment Info for ddi_get_tem2
            envInfo2.device_handle = deviceHandle;
            envInfo2.type = ENV_DEVICE_HANDLE;
            envInfo2.app_info = &appInfo;

            status = m_pDDSController->ddiGetItem2(&itemSpecifier,
                                                   &envInfo2, ALL_ITEM_ATTR_MASK, &genericItem);
        }

        if (!status.hasError())
        {
            FLAT_METHOD *pMethod = static_cast<FLAT_METHOD*>(genericItem.item);
            if (nullptr != pMethod)
            {
                DDSController::s_pMethodExecutionData.methodLabel = validateAndGetStringValue(pMethod->label.str, pMethod->label.len);
                DDSController::s_pMethodExecutionData.methodHelp  = validateAndGetStringValue(pMethod->help.str, pMethod->help.len);
            }

            // create scan thread only if no MethodExecution exists
            if (THREAD_STATE_INVALID == getMethodExecutionThreadState())
            {
                // create MethodExecution thread
                createMethodExecutionThread();
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Failed to start thread, already running");
                Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Failed to fetch the method data");
            Error::Entry error = Error::cpm.METHOD_EXEC_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }

        // Clear the Generic Item memory allocated by ddiGetItem API
        m_pDDSController->ddiCleanItem(&genericItem);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID or Item Container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResponse;
}

/*  ----------------------------------------------------------------------------
    Execute pre/post actions
    Authored By: Utthunga
*/
PMStatus FFAdaptor::executeEditMethod(ITEM_ID methodID, ITEM_ID blockID)
{
	PMStatus status;

    DDI_ITEM_SPECIFIER   itemSpecifier{};
    DDI_GENERIC_ITEM     genericItem{};

    DDSController::s_pMethodExecutionData = {};
    DDSController::s_pMethodExecutionData.blockID = blockID;
    DDSController::s_pMethodExecutionData.methodID = methodID;

    // Fill the item Specifier information
    itemSpecifier.type = DDI_ITEM_ID;
    itemSpecifier.item.id = methodID;

    if (blockID > 0)
    {
        DDI_BLOCK_SPECIFIER  blockSpecifier{};
        ENV_INFO envInfo{};

        // Fill the Block Specifier Attributes
        blockSpecifier.type = DDI_BLOCK_TAG;
        blockSpecifier.block.tag = m_pDDSController->getBlockTagByBlockID(blockID, 0);

        // Get the block information from the DDS
        status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier,
                                              &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
    }
    else
    {
        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
        status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
            status.setError(FF_ADAPTOR_ERR_INVALID_DEVICE_HANDLE, PM_ERR_CL_FF_ADAPTOR);
            return status;
        }

        // Set the Environment Info for ddi_get_tem2
        ENV_INFO2 envInfo2{};
        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;

        status = m_pDDSController->ddiGetItem2(&itemSpecifier,
                                               &envInfo2, ALL_ITEM_ATTR_MASK, &genericItem);
    }

    if (!status.hasError())
    {
        FLAT_METHOD *pMethod = static_cast<FLAT_METHOD*>(genericItem.item);
        if (nullptr != pMethod)
        {
            DDSController::s_pMethodExecutionData.methodLabel = validateAndGetStringValue(pMethod->label.str, pMethod->label.len);
            DDSController::s_pMethodExecutionData.methodHelp  = validateAndGetStringValue(pMethod->help.str, pMethod->help.len);
        }

        // create  thread
        if (THREAD_STATE_RUN == getEditActionThreadState())
        {
        	status = executePrePostEditMethod();
        	if(status.hasError())
        	{
                        LOGF_ERROR(CPMAPP, "Failed to start thread, already running");
				status.setError(FF_ADAPTOR_ERR_INVALID_STATE_OF_ME_THREAD,
								PM_ERR_CL_FF_ADAPTOR);
        	}
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Failed to start thread, fetching generic data failed");
            status.setError(FF_ADAPTOR_ERR_DATA_EDIT_ACTION,
                            PM_ERR_CL_FF_ADAPTOR);
        }
    }
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "DDI get item failed. Recheck variable inputs");
    }

    // Clear the Generic Item memory allocated by ddiGetItem API
    m_pDDSController->ddiCleanItem(&genericItem);

    return status;
}

/* ---------------------------------------------------------------------------
    Process pre/post actions
    Authored By: Utthunga
*/
PMStatus FFAdaptor::processEditAction()
{
    PMStatus status;
    ITEM_ID blockID;
    Json::Value jsondata;
    Json::Value jsonResultObject = JsonRpcUtil::getResponseObject(Json::Value("ok")) ;

    DDI_GENERIC_ITEM	genericItem{};
    DDI_BLOCK_SPECIFIER blockSpecifier{};
    DDI_ITEM_SPECIFIER  itemSpecifier{};

    ITEM_ID* actList;
    ENV_INFO envInfo{};
    APP_INFO appInfo{};

    envInfo.app_info = &appInfo;
    appInfo.status_info = USE_EDIT_VALUE;

    blockSpecifier.type = DDI_BLOCK_TAG;
    blockSpecifier.block.tag = const_cast<char*>(editData.blockTag.c_str());

    itemSpecifier.type = DDI_ITEM_ID;
    itemSpecifier.item.id = editData.itemId;
    EditActionTypes actionType = editData.actionTypes;
    EditActionItemTypes actionItemType = editData.actionItemTypes;

    status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier, &envInfo,
                                          ALL_ITEM_ATTR_MASK ,&genericItem);

    int count = 0;
    if (actionItemType == EditActionItemTypes::VARIABLE_ACTION)
    {
        FLAT_VAR *flatVar = static_cast<FLAT_VAR*>(genericItem.item);
        if (nullptr != flatVar)
        {
            if (actionType == EditActionTypes::PRE_EDIT_ACTION)
            {
                count = flatVar->actions->pre_edit_act.count;
                actList = flatVar->actions->pre_edit_act.list;
            }
            else if (actionType == EditActionTypes::POST_EDIT_ACTION)
            {
                count = flatVar->actions->post_edit_act.count;
                actList = flatVar->actions->post_edit_act.list;
            }
        }
    }
    else if (actionItemType == EditActionItemTypes::EDIT_DISPLAY_ACTION)
    {
        FLAT_EDIT_DISPLAY *flatEditDisplay = static_cast<FLAT_EDIT_DISPLAY*>(genericItem.item);
        if (nullptr != flatEditDisplay)
        {
            if (actionType == EditActionTypes::PRE_EDIT_ACTION)
            {
                count = flatEditDisplay->pre_edit_act.count;
                actList = flatEditDisplay->pre_edit_act.list;
            }
            else if (actionType == EditActionTypes::POST_EDIT_ACTION)
            {
                count = flatEditDisplay->post_edit_act.count;
                actList = flatEditDisplay->post_edit_act.list;
            }
        }
    }

	BLOCK_HANDLE blockHandle = m_pDDSController->getBlockHandleByBlockTag(
            editData.blockTag.c_str());

    if (blockHandle < 0)
    {
        LOGF_ERROR(CPMAPP,"Falied to fetch block handle by block tag");
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                        PM_ERR_CL_FF_ADAPTOR);
        Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
        jsonResultObject = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

	// handling the number of actions
    for (int itemList = 0; itemList < count; itemList++)
    {
    	status = m_pDDSController->getBlockIDByBlockHandle(blockHandle, &blockID);
    	if(!status.hasError())
    	{
    		status = executeEditMethod(actList[itemList], blockID);
            if (status.hasError())
    		{
    			status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
    			                                PM_ERR_CL_FF_ADAPTOR);
    			Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
    			jsonResultObject = JsonRpcUtil::setErrorObject(error.code, error.message);

    			// if any action fails, don't process with next pre/post edit action
    			break;
    		}
    	}
    	else
    	{
            LOGF_ERROR(CPMAPP,"Falied to fetch block ID by block handle");
    		status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
											PM_ERR_CL_FF_ADAPTOR);
			Error::Entry error = Error::cpm.EDIT_ACTION_FAILED;
			jsonResultObject = JsonRpcUtil::setErrorObject(error.code, error.message);
    	}
    }

    // creating json and sending info using status publisher
	VariableType varType(editData.itemId);

    std::string publisher = STATUS_PUBLISHER;

	jsondata[JSON_ITEM_STATUS_CLASS] = JSON_EDIT_ACTION;
	jsondata[JSON_ITEM_INFO][JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(editData.itemId);
	jsondata[JSON_ITEM_INFO][JSON_VAR_CONT_ID] = static_cast<Json::Value::LargestUInt>(varType.containerId);
	jsondata[JSON_ITEM_INFO][JSON_VAR_SUBINDEX] = varType.subIndex;
	jsondata[JSON_ITEM_STATUS] = jsonResultObject;

    CommonProtocolManager::getInstance()->notifyMessage(publisher, jsondata);

    m_pDDSController->ddiCleanItem(&genericItem);
    return status;
}

/*
    Sets the edit action thread state
*/
void FFAdaptor::setEditActionThreadState(THREAD_STATE eThreadState)
{
    ThreadManager::getInstance()->setThreadState(m_editActionThreadId, eThreadState);
}

/*
    Retrieves the current state of edit action thread
*/
THREAD_STATE FFAdaptor::getEditActionThreadState()
{
    return ThreadManager::getInstance()->getThreadState(m_editActionThreadId);
}

/*  ----------------------------------------------------------------------------
    Processes Set Item for Method execution
    Authored By: Utthunga
*/
Json::Value FFAdaptor::processSetMethodExecution(const Json::Value& data)
{
    Json::Value jsonResponse = Json::Value("ok");

    if (data.isMember(JSON_ITEM_REQ_TYPE) && data[JSON_ITEM_REQ_TYPE].isString())
    {
        std::string itemReqType = data[JSON_ITEM_REQ_TYPE].asString();
        if (itemReqType.compare(JSON_METHOD_NOTIFY_ABORT) == 0)
        {
            // Abort the execution of method
            DDSController::s_pMethodExecutionData.isAborted = true;
        }
        else
        {
            if (data.isMember(JSON_ITEM_INFO) &&
                data[JSON_ITEM_INFO].isMember(JSON_VAR_VALUE))
            {
                std::string varValue = data[JSON_ITEM_INFO][JSON_VAR_VALUE].asString();
				unsigned int milliSec = 0;
                unsigned int microSec = 0;
				if (data[JSON_ITEM_INFO].isMember(JSON_VAR_VAL_MILLISEC) && 
                        data[JSON_ITEM_INFO][JSON_VAR_VAL_MILLISEC].isIntegral())
                {
                    unsigned int milliSec = data[JSON_ITEM_INFO][JSON_VAR_VAL_MILLISEC].asUInt();
                }

                if (data[JSON_ITEM_INFO].isMember(JSON_VAR_VAL_MICROSEC) && 
                        data[JSON_ITEM_INFO][JSON_VAR_VAL_MICROSEC].isIntegral())
                {
                    unsigned int microSec = data[JSON_ITEM_INFO][JSON_VAR_VAL_MICROSEC].asUInt();
                }
				
                PMStatus status = setMethodData(data[JSON_ITEM_INFO], milliSec, microSec);
                if (status.hasError())
                {
                    LOGF_ERROR(CPMAPP, "Invalid Item Info : Var Value");
                    Error::Entry error = Error::cpm.INVALID_VARIABLE_VALUE;
                    return JsonRpcUtil::setErrorObject(error.code, error.message);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Invalid Item Info : Var Value");
                Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
                return JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        DDSController::s_pMethodExecutionData.isItemSet = true;

        // Unlocks the ME thread and notify that data has arrived
        m_pDDSController->releaseLock();
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid request type");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        jsonResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResponse;
}

/*  ----------------------------------------------------------------------------
    Delivers JSON notification to Instrument controller
    Authored By: Utthunga
*/
void FFAdaptor::sendMethodUIMessage(string response, string notificationType)
{
    Json::Value jsondata;

    DDSController::s_pMethodExecutionData.currentInputType = 0;
    jsondata[JSON_ITEM_LABEL] = DDSController::s_pMethodExecutionData.methodLabel;
    jsondata[JSON_ITEM_HELP] = DDSController::s_pMethodExecutionData.methodHelp;
    jsondata[JSON_ITEM_TYPE] = JSON_METHOD_ITEM_TYPE;
    if (DDSController::s_pMethodExecutionData.blockID == 0)
    {
        jsondata[JSON_ITEM_CONT_TAG] = "";
    }
    else
    {
        jsondata[JSON_ITEM_CONT_TAG] = m_pDDSController->getBlockTagByBlockID(
                    DDSController::s_pMethodExecutionData.blockID, 0);
    }
    jsondata[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(DDSController::s_pMethodExecutionData.methodID);
    jsondata[JSON_ITEM_IN_DD] = true;

    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] = notificationType;
    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = response;

    if (JSON_METHOD_NOTIFY_START == notificationType)
    {
        m_pFFParameterCache->pauseSubscription();
    }
    else if (JSON_METHOD_NOTIFY_COMPLETED == notificationType)
    {
        m_pFFParameterCache->resumeSubscription();
    }

    // Sends method state for Abort status to UI
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        jsondata[JSON_METHOD_STATE] = TRUE;
    }

    // notify CPM with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr, jsondata);
}

/*  ----------------------------------------------------------------------------
    getMethodData function is responsible to get data from to UI layer or User
    and it will fill the default data for local reference
    Authored By: Utthunga
*/
void FFAdaptor::getMethodDefaultData(std::string msg, EVAL_VAR_VALUE* varValue)
{
    if (nullptr != DDSController::s_pMethodExecutionData.userInputVal)
    {
        DDSController::s_pMethodExecutionData.currentInputType =
        DDSController::s_pMethodExecutionData.userInputVal->type;

        Json::Value methJsonResp;

        // Fill the basic attribute information into json response
        methJsonResp[JSON_ITEM_LABEL] = DDSController::s_pMethodExecutionData.methodLabel;
        methJsonResp[JSON_ITEM_HELP] = DDSController::s_pMethodExecutionData.methodHelp;
        methJsonResp[JSON_ITEM_TYPE] = JSON_METHOD_ITEM_TYPE;
        methJsonResp[JSON_ITEM_CONT_TAG] = m_pDDSController->getBlockTagByBlockID(
                    DDSController::s_pMethodExecutionData.blockID, 0);
        methJsonResp[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(DDSController::s_pMethodExecutionData.methodID);
        methJsonResp[JSON_ITEM_IN_DD] = true;

        // Fill information required for a method
        methJsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = 0;
        methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] = JSON_METHOD_NOTIFY_GETVALUE;
        methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = msg;

        Json::Value varJsonResp;
        varJsonResp[JSON_ITEM_LABEL] = "";
        varJsonResp[JSON_ITEM_HELP] = "";
        varJsonResp[JSON_ITEM_TYPE] = JSON_VARIABLE_ITEM_TYPE;
        varJsonResp[JSON_ITEM_CONT_TAG] = m_pDDSController->getBlockTagByBlockID(
                    DDSController::s_pMethodExecutionData.blockID, 0);
        varJsonResp[JSON_ITEM_ID] = 0;
        varJsonResp[JSON_ITEM_IN_DD] = true;
        varJsonResp[JSON_ITEM_INFO][JSON_ITEM_READONLY] = false;
        varJsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = 0;

        VariableType varParam{};
        m_pFFParameterCache->setValuesToParamInfo(varValue, &varParam);

        // Fill the variable attribute information
        fillVariableJsonInfo(&varParam, varJsonResp);

        if (!varJsonResp.empty())
        {
            methJsonResp[JSON_ITEM_INFO][JSON_METHOD_VAR_INFO] = varJsonResp;
        }

        // Sends method state for Abort status to UI
        if (DDSController::s_pMethodExecutionData.isAborted)
        {
            methJsonResp[JSON_METHOD_STATE] = TRUE;
        }

        // notify CPM with method execution response
        std::string methPubStr(ITEM_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(methPubStr,
                                                            methJsonResp);
    }
    else
    {
        LOGF_ERROR(CPMAPP, "failed to fetch user data");

        // Abort the execution of method
        DDSController::s_pMethodExecutionData.isAborted = true;
        DDSController::s_pMethodExecutionData.isItemSet = true;

        // Unlocks the ME thread and notify that data has arrived
        m_pDDSController->releaseLock();
    }
}

/*  ----------------------------------------------------------------------------
    getMethodData function is responsible to get data from to UI layer or User
    Authored By: Utthunga
*/
void FFAdaptor::getMethodData(std::string msg, DDI_GENERIC_ITEM* item, unsigned short subIndex)
{
    PMStatus status;
    DDSController::s_pMethodExecutionData.currentInputType = 0;

    if (nullptr != item && nullptr !=
        DDSController::s_pMethodExecutionData.userInputVal)
    {
        DDSController::s_pMethodExecutionData.currentInputType =
        DDSController::s_pMethodExecutionData.userInputVal->type;

        std::string itemContainerTag = "";
        if (DDSController::s_pMethodExecutionData.blockID > 0)
        {
            itemContainerTag = m_pDDSController->getBlockTagByBlockID(
                        DDSController::s_pMethodExecutionData.blockID, 0);
        }

        FLAT_VAR* flatVar = static_cast<FLAT_VAR *>(item->item);

        Json::Value methJsonResp;
        auto paramInfo = new ParamInfo;

        VariableType varType{};
        varType.itemType = PARAM_VARIABLE;

        std::string recordName;

        if (subIndex > 0)
        {
            ParamInfo recParamInfo{};
            recParamInfo.requestInfo.maskType = ITEM_ATTR_ALL_MASK;
            recParamInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());

            RecordType recType{flatVar->id};
            recType.subIndex = subIndex;
            recParamInfo.paramList.push_back(recType);

            // Call getParameter and retrieve parameter information
            status = m_pFFParameterCache->getParameter(&recParamInfo);

            if (!status.hasError())
            {
                ItemInfoType& rtype = recParamInfo.paramList.at(0);
                RecordType& recItemType = dynamic_cast<RecordType&>(rtype);
                recordName = validateAndGetStringValue(recItemType.label.str, recItemType.label.len);

                ItemInfoType& type = recItemType.members.at(0);
                VariableType& varItemType = dynamic_cast<VariableType&>(type);

                varType.itemID = varItemType.itemID;
                varType.subIndex = varItemType.subIndex;
                varType.containerId = varItemType.containerId;
                varType.itemContainerTag.str = const_cast<char*>(itemContainerTag.c_str());

                paramInfo->blockTag.str = const_cast<char*>(itemContainerTag.c_str());
                paramInfo->requestInfo.maskType = MaskType::ITEM_ATTR_ALL_MASK;
                paramInfo->paramList.push_back(varType);
            }

            // Delete the dynamically allocated memory from parameter cache
            m_pFFParameterCache->cleanParamInfo(&recParamInfo);
        }
        else
        {
            varType.itemID = flatVar->id;
            varType.itemContainerTag.str = const_cast<char*>(itemContainerTag.c_str());

            paramInfo->blockTag.str = const_cast<char*>(itemContainerTag.c_str());
            paramInfo->requestInfo.maskType = MaskType::ITEM_ATTR_ALL_MASK;
            paramInfo->paramList.push_back(varType);
        }

        // Call getParameter and retrieve parameter information
        status = m_pFFParameterCache->getParameter(paramInfo);
        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Failed to get parameter");

            // Abort the execution of method
            DDSController::s_pMethodExecutionData.isAborted = true;
            DDSController::s_pMethodExecutionData.isItemSet = true;

            // Unlocks the ME thread and notify that data has arrived
            m_pDDSController->releaseLock();
            
            Error::Entry error = Error::cpm.GET_PARAMETER_FAILED;
            methJsonResp = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
        else
        {
            status = m_pFFParameterCache->readValue(paramInfo);
            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "Failed to read values");

                // Abort the execution of method
                DDSController::s_pMethodExecutionData.isAborted = true;
                DDSController::s_pMethodExecutionData.isItemSet = true;

                // Unlocks the ME thread and notify that data has arrived
                m_pDDSController->releaseLock();

                Error::Entry error = Error::cpm.READ_VALUE_FAILED;
                methJsonResp = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            else
            {
                auto type = &paramInfo->paramList.at(0).get();
                VariableType* varParam = dynamic_cast<VariableType*>(type);

                // Fill itemBasicInfo structure
                MethodBasicInfo methBasicInfo{};
                methBasicInfo.basicInfo.itemID = DDSController::s_pMethodExecutionData.methodID;
                methBasicInfo.basicInfo.itemHelp = DDSController::s_pMethodExecutionData.methodHelp;
                methBasicInfo.basicInfo.itemLabel = DDSController::s_pMethodExecutionData.methodLabel;
                methBasicInfo.basicInfo.itemType = JSON_METHOD_ITEM_TYPE;
                methBasicInfo.basicInfo.itemContainerTag = itemContainerTag;
                
                if (READ_HANDLING != varParam->misc.handling)
                {
                    methBasicInfo.itemReadOnly = false;
                }
                methBasicInfo.itemClass = varParam->misc.class_attr;

                // Fill the basic information in JsonResponse
                fillItemBasicJsonInfo(methBasicInfo.basicInfo, methJsonResp);

                // Fill information required for a method
                methJsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = static_cast<Json::Value::LargestUInt>(methBasicInfo.itemClass);
                methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE] = JSON_METHOD_NOTIFY_GETVALUE;
                methJsonResp[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = msg;

                Json::Value varJsonResp;

                // Fill itemBasicInfo structure
                VarBasicInfo varBasicInfo{};
                varBasicInfo.basicInfo.itemID = flatVar->id;
                varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(flatVar->help.str, flatVar->help.len);
                if (recordName.empty())
                {
                	varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(flatVar->label.str, flatVar->label.len);
                }
                else
                {
                    varBasicInfo.basicInfo.itemLabel = recordName + " " + validateAndGetStringValue(flatVar->label.str, flatVar->label.len);
                }
                varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
                if (nullptr != flatVar->misc && 0 == subIndex)
                {
                    if (READ_HANDLING != flatVar->misc->write_mode)
                    {
                        varBasicInfo.itemReadOnly = false;
                    }
                }
                varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;
                varBasicInfo.itemClass = flatVar->class_attr;
                
                // Fill the basic information in JsonResponse
                fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

                // Fill the variable attribute information
                fillVariableJsonInfo(varParam, varJsonResp);

                if (!varJsonResp.empty())
                {
                    methJsonResp[JSON_ITEM_INFO][JSON_METHOD_VAR_INFO] = varJsonResp;
                }

                // Copy the device string value in to the method data
                if ((varParam->type == DDS_ASCII)       || (varParam->type == DDS_PACKED_ASCII)  ||
                    (varParam->type == DDS_PASSWORD)    || (varParam->type == DDS_BITSTRING)     ||
                    (varParam->type == DDS_OCTETSTRING) || (varParam->type == DDS_VISIBLESTRING) ||
                    (varParam->type == DDS_EUC))
                {
                    DDSController::s_pMethodExecutionData.userInputVal->val.s.len = varParam->value.strData.len;
                    DDSController::s_pMethodExecutionData.userInputVal->val.s.str = (char*)malloc(varParam->value.strData.len);
                    strncpy(DDSController::s_pMethodExecutionData.userInputVal->val.s.str,
                            varParam->value.strData.str, varParam->value.strData.len);
                }

                // Sends method state for Abort status to UI
                if (DDSController::s_pMethodExecutionData.isAborted)
                {
                    methJsonResp[JSON_METHOD_STATE] = TRUE;
                }

                // notify CPM with method execution response
                std::string methPubStr(ITEM_PUBLISHER);
                CommonProtocolManager::getInstance()->notifyMessage(methPubStr,
                                                                    methJsonResp);

                // Delete the dynamically allocated memory from parameter cache
                m_pFFParameterCache->cleanParamInfo(paramInfo);
            }
        }

        delete paramInfo;
        paramInfo = nullptr;
    }
    else
    {
        LOGF_ERROR(CPMAPP, "failed to fetch user data\n");

        // Abort the execution of method
        DDSController::s_pMethodExecutionData.isAborted = true;
        DDSController::s_pMethodExecutionData.isItemSet = true;

        // Unlocks the ME thread and notify that data has arrived
        m_pDDSController->releaseLock();
    }
}

/*  ----------------------------------------------------------------------------
    setMethodData function is responsible to get data from to UI layer or User
    Authored By: Utthunga
*/
PMStatus FFAdaptor::setMethodData
(
    Json::Value itemInfo, 
    unsigned int milliSec, 
    unsigned int microSec
)
{
    PMStatus status;
    int errorCode = FAILURE;

    if (itemInfo.isNull() && !(itemInfo.isMember(JSON_VAR_VALUE)))
    {
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        return status;
    }
    switch (DDSController::s_pMethodExecutionData.currentInputType)
    {
        case DDS_INTEGER:
        {
            long long tempValue = 0;
            try
            {
                // For validation of range, verified with value -62362362889
                tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }

            if (validateIntegerRange<long long, long long>(tempValue))
            {
                DDSController::s_pMethodExecutionData.userInputVal->val.i = tempValue;
                errorCode = SUCCESS;
            }
        }
        break;

		case DDS_UNSIGNED:      //FALLTHROUGH
        case DDS_INDEX:         //FALLTHROUGH
        case DDS_ENUMERATED:    //FALLTHROUGH
        case DDS_BIT_ENUMERATED://FALLTHROUGH
        case DDS_ACK_TYPE:      //FALLTHROUGH
        case DDS_MAX_TYPE:      //FALLTHROUGH
        case DDS_BOOLEAN_T:     //FALLTHROUGH
        {
            unsigned long bitIndex = 0;
            if (true == itemInfo.isMember(JSON_VAR_BIT_INDEX) &&
                itemInfo[JSON_VAR_BIT_INDEX].isIntegral())
            {
                bitIndex = itemInfo[JSON_VAR_BIT_INDEX].asLargestUInt();
            }

            if (bitIndex > 0)
            {
                bool bitEnumItemVal = itemInfo[JSON_VAR_VALUE].asBool();
                unsigned long devBitEnumVal = DDSController::s_pMethodExecutionData.userInputVal->val.u;
                setBitEnumItemVal(devBitEnumVal, bitEnumItemVal, bitIndex);
            }
            else
            {
                unsigned long long tempValue = 0;
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                              PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }

                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    DDSController::s_pMethodExecutionData.userInputVal->val.u = tempValue;
                    errorCode = SUCCESS;
                }
            }
        }
        break;

        case DDS_FLOAT:
        {
            double tempValue = 0;
            try
            {
                // For validation of range, verified with value 632732323652361251236156125126351236.3843282643643;
                tempValue = itemInfo[JSON_VAR_VALUE].asDouble();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            DDSController::s_pMethodExecutionData.userInputVal->val.f = tempValue;
            errorCode = SUCCESS;
        }
        break;

        case DDS_DOUBLE:
        {
            double tempValue = 0;
            try
            {
                // no validation required for double variable
                tempValue = itemInfo[JSON_VAR_VALUE].asDouble();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            DDSController::s_pMethodExecutionData.userInputVal->val.d = tempValue;
            errorCode = SUCCESS;
        }
        break;

        // String types
        case DDS_ASCII:         //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        case DDS_EUC:           //FALLTHROUGH
        {
            if (itemInfo[JSON_VAR_VALUE].isString())
            {
                std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                // validation:check if string is NULL
                if (!tempStr.empty())
                {
                    if ((DDS_ASCII == DDSController::s_pMethodExecutionData.currentInputType) ||
                        (DDS_PASSWORD == DDSController::s_pMethodExecutionData.currentInputType))
                    {
                        // Convert UTF8 string to ISO-Latin-1 before writing the value to device
                        size_t utfStrLen = tempStr.length();
                        char* utf8Str = (char *) calloc ((utfStrLen + 1), sizeof(char));
                        if (nullptr == utf8Str)        
                        {
                            LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                            break;
                        }

                        // Since the string from user is in utf8 format, its better to allocate twice the 
                        // memory for accomodating decoded string data
                        size_t isoLatinStrLen = (utfStrLen * 2) + 1;
                        char* isoLatinStr = (char *) calloc (isoLatinStrLen, sizeof(char));
                        if (nullptr == isoLatinStr)        
                        {
                            LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                            break;
                        }

                        strncpy (utf8Str, tempStr.c_str(), tempStr.length());

                        char* tempUtf8Str = utf8Str;
                        char* tempIsoLatinStr = isoLatinStr;
                        bool convRetVal = CPMUtil::convertUtf8ToIsolatin1 (&tempUtf8Str, &utfStrLen,     
                                                        &tempIsoLatinStr, &isoLatinStrLen);

                        if (true == convRetVal)
                        {
                            tempStr = isoLatinStr;
                            free (utf8Str);
                            free (isoLatinStr);
                        }
                        else
                        {
                            LOGF_ERROR (CPMAPP, "Cannot convert UTF8 string to ISO-Latin-1");
                            free (utf8Str);
                            free (isoLatinStr);
                            break;
                        }
                    }

                    int length = tempStr.length();
                    if (length > DDSController::s_pMethodExecutionData.userInputVal->val.s.len)
                    {
                        length = DDSController::s_pMethodExecutionData.userInputVal->val.s.len;
                    }
                    else
                    {
                        int spaceCount = DDSController::s_pMethodExecutionData.userInputVal->val.s.len - tempStr.length();
                        tempStr.append(spaceCount, ' ');
                        length = tempStr.length();
                    }
                    strncpy(DDSController::s_pMethodExecutionData.userInputVal->val.s.str,
                            const_cast<char *>(tempStr.c_str()), length);
                    errorCode = SUCCESS;
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Can not set Empty String: " << DDSController::s_pMethodExecutionData.currentInputType);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Not a valid input for String type " << DDSController::s_pMethodExecutionData.currentInputType);
            }
        }
        break;

        case DDS_BITSTRING:
        {
            if (itemInfo[JSON_VAR_VALUE].isString())
            {
                std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                // validation:check if string is NULL
                if (!tempStr.empty())
                {
                    if (tempStr.length() == DDSController::s_pMethodExecutionData.userInputVal->val.s.len)
                    {
                        strncpy(DDSController::s_pMethodExecutionData.userInputVal->val.s.str, tempStr.c_str(), tempStr.length());
                        DDSController::s_pMethodExecutionData.userInputVal->val.s.len = tempStr.length();
                        errorCode = SUCCESS;
                    }
                }
            }
        }
        break;

        case DDS_TIME:
        {
            unsigned long long timeStamp = 0;
            try
            {
                // For validation of range, verified with value 712376526151635
                timeStamp = itemInfo[JSON_VAR_VALUE].asLargestUInt();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            if (validateIntegerRange<unsigned int, unsigned long long>(timeStamp))
            {
                int* dateArray = new int[DATENTIME_ARRAY_LENGTH];
                std::string dateString = m_pDDSController->convertsEpochTimeToUserTime(
                            timeStamp, DATENTIME_DATA_FORMAT);
                sscanf(dateString.c_str(), DATENTIME_TYPE_FORMAT, &dateArray[DATE], &dateArray[MONTH],
                       &dateArray[YEAR], &dateArray[HOUR], &dateArray[MINUTE], &dateArray[SECOND]);
            	dateArray[MILLISECOND] = milliSec;

                if (m_pFFParameterCache->validateDateNTime(dateArray, DATENTIME_ARRAY_LENGTH))
                {
                    m_pDDSController->convUserTimeToDDSTime(dateArray,
                                      DDSController::s_pMethodExecutionData.userInputVal->val.ca);
                    if (nullptr != dateArray)
                    {
                        delete[] dateArray;
                        dateArray = nullptr;
                    }
                    errorCode = SUCCESS;
                }
            }
        }
        break;

        case DDS_TIME_VALUE:
        {
            unsigned long long timeStamp = 0;
            try
            {
                // For validation of range, verified with value 712376526151635
                timeStamp = itemInfo[JSON_VAR_VALUE].asLargestUInt();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            if (validateIntegerRange<unsigned int, unsigned long long>(timeStamp))
            {
            	auto dateArray = new int[DATENTIME_ARRAY_LENGTH];
                        
            	dateArray[MILLISECOND] = milliSec;
            	dateArray[MICROSECOND] = microSec;

                m_pDDSController->convUserTimeValToDDSTimeVal(timeStamp, dateArray,
                                  DDSController::s_pMethodExecutionData.userInputVal->val.ca);
            
            	if (nullptr != dateArray)
            	{
                	delete[] dateArray;
                	dateArray = nullptr;
				}
                errorCode = SUCCESS;
            }
        }
        break;

        case DDS_DATE_AND_TIME:
        {
            long long timeStamp = 0;
            try
            {
                // For validation of range, verified with value 712376526151635
                timeStamp = itemInfo[JSON_VAR_VALUE].asLargestInt();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            if (validateIntegerRange<long long, long long>(timeStamp))
            {
                int* dateArray = new int[DATENTIME_ARRAY_LENGTH];
                std::string dateString = m_pDDSController->convertsEpochTimeToUserTime(
                            timeStamp, DATENTIME_DATA_FORMAT);
                sscanf(dateString.c_str(), DATENTIME_TYPE_FORMAT, &dateArray[DATE], &dateArray[MONTH],
                       &dateArray[YEAR], &dateArray[HOUR], &dateArray[MINUTE], &dateArray[SECOND]);
                if (m_pFFParameterCache->validateDateNTime(dateArray, DATENTIME_ARRAY_LENGTH))
                {
                    m_pDDSController->convUserDateNTimeToDDSDateNTime(dateArray,
                                      DDSController::s_pMethodExecutionData.userInputVal->val.ca);
                    if (nullptr != dateArray)
                    {
                        delete[] dateArray;
                        dateArray = nullptr;
                    }
                    errorCode = SUCCESS;
                }
            }
        }
        break;

        case DDS_DURATION:
        {
            unsigned long long timeStamp = 0;
            try
            {
                // For validation of range, verified with value 712376526151635
                timeStamp = itemInfo[JSON_VAR_VALUE].asLargestUInt();
            }
            catch(...)
            {
                status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                            PM_ERR_CL_FF_ADAPTOR);
                return status;
            }
            if (validateIntegerRange<unsigned int, unsigned long long>(timeStamp))
            {
                m_pDDSController->convUserDurationToDDSDuration(timeStamp,
                                  DDSController::s_pMethodExecutionData.userInputVal->val.ca);
                errorCode = SUCCESS;
            }
        }
        break;

        default:
        {
			// Its success, Expected user acknowledgement in Default case
            errorCode = SUCCESS;
        }
        break;
    } //!-- switch case

    if (SUCCESS != errorCode)
    {
        LOGF_ERROR(CPMAPP, "Invalid input value for variable: " <<
                   DDSController::s_pMethodExecutionData.currentInputType);
        status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                    PM_ERR_CL_FF_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    GetData function is responsible to fill the enumeration values
    Authored By: Utthunga
*/
void FFAdaptor::fillMethodEnumValAsJson(string msg, std::map<int, std::string> enumMap, int selection)
{
    Json::Value jsondata;

    DDSController::s_pMethodExecutionData.currentInputType = DDS_INDEX;
    jsondata[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(DDSController::s_pMethodExecutionData.methodID);
    jsondata[JSON_ITEM_LABEL] = DDSController::s_pMethodExecutionData.methodLabel;
    jsondata[JSON_ITEM_TYPE] = JSON_METHOD_ITEM_TYPE;
    jsondata[JSON_ITEM_HELP] = DDSController::s_pMethodExecutionData.methodHelp;
    jsondata[JSON_ITEM_CONT_TAG] = "";

    jsondata[JSON_ITEM_READONLY] = false;
    jsondata[JSON_ITEM_IN_DD] = true;

    jsondata[JSON_ITEM_INFO][JSON_ITEM_CLASS] = 0;
    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_TYPE]
            = JSON_METHOD_NOTIFY_SELECTION;
    jsondata[JSON_ITEM_INFO][JSON_METHOD_NOTIFY_MSG] = msg;

    Json::Value itemdata;
    itemdata[JSON_ITEM_ID] = "";
    itemdata[JSON_ITEM_LABEL] = "";
    itemdata[JSON_ITEM_TYPE] = JSON_VARIABLE_ITEM_TYPE;
    itemdata[JSON_ITEM_HELP] = "";

    itemdata[JSON_ITEM_READONLY] = false;
    itemdata[JSON_ITEM_IN_DD] = true;

    itemdata[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ENUM;
    itemdata[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VAL_TYPE] = DDS_INDEX;

    // Increment by 1, because index value to match the enumeration
    itemdata[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VALUE] = selection + 1;

    std::map<int, std::string>::const_iterator it;
    for (it = enumMap.begin(); it != enumMap.end(); ++it)
    {
        Json::Value enumdata;
        enumdata[JSON_VAR_ENUM_VALUE] = it->first;
        enumdata[JSON_VAR_ENUM_LABEL] = it->second;
        enumdata[JSON_VAR_ENUM_HELP] = "";
        itemdata[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_ENUM_INFO]
                .append(enumdata);
    }

    jsondata[JSON_ITEM_INFO][JSON_METHOD_VAR_INFO] = itemdata;

    // Sends method state for Abort status to UI
    if (DDSController::s_pMethodExecutionData.isAborted)
    {
        jsondata[JSON_METHOD_STATE] = TRUE;
    }

    // notify CPM with method execution response
    std::string methPubStr(ITEM_PUBLISHER);
    CommonProtocolManager::getInstance()->notifyMessage(methPubStr,
                                                        jsondata);
}

/*  ---------------------------------------------------------------------------
    Creates Method Execution timer thread
    Authored By: Utthunga
*/
void FFAdaptor::createMethodExecutionThread()
{
    m_threadIdLock.lock();
    std::function<void(const void *)> methodFuncPtr = 
                     std::bind(&FFAdaptor::methodExecutionThread, this, this);
    m_methodThreadId = ThreadManager::getInstance()->createThread(methodFuncPtr, this);
    m_threadIdLock.unlock();
}

/*  ---------------------------------------------------------------------------
    Destroyes Method Execution thread
    Authored By: Utthunga
*/
void FFAdaptor::destroyMethodExecutionThread()
{
    setMethodExecutionThreadState(THREAD_STATE_STOP);
    ThreadManager::getInstance()->destroyThread(m_methodThreadId);
    m_methodThreadId = 0;
}

/*  ---------------------------------------------------------------------------
    Implements Method Execution thread
    Authored By: Utthunga
*/
void FFAdaptor::methodExecutionThread(const void* arg)
{
    m_threadIdLock.lock();
    FFAdaptor* pInstance = static_cast<FFAdaptor*>(const_cast<void*>(arg));
    m_threadIdLock.unlock();
    pInstance->executeMethod();
}

/*  ---------------------------------------------------------------------------
    Executes the method
    Authored By: Utthunga
*/
void FFAdaptor::executeMethod()
{
    // Set state as method execution is in progress
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);

    PMStatus status;
    char* blockTag = m_pDDSController->getBlockTagByBlockID(
                     DDSController::s_pMethodExecutionData.blockID, 0);
    if (nullptr == blockTag)
    {
        status = m_pDDSController->executeMethod("",
                          DDSController::s_pMethodExecutionData.methodID);
    }
    else
    {
        std::string tag(blockTag);
        status = m_pDDSController->executeMethod(tag,
                          DDSController::s_pMethodExecutionData.methodID);
    }
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Failed to execute method");
        destroyMethodExecutionThread();
    }

    // Clear state as method execution is in progress
    CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);
}

/*  ---------------------------------------------------------------------------
    Execute post and pre edit action
*/
PMStatus FFAdaptor::executePrePostEditMethod()
{
    // Set state as method execution is in progress
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);

    PMStatus status;
    char* blockTag = m_pDDSController->getBlockTagByBlockID(
                     DDSController::s_pMethodExecutionData.blockID, 0);
    if (nullptr == blockTag)
    {
    	 status = m_pDDSController->executeMethod("",
                          DDSController::s_pMethodExecutionData.methodID);
    }
    else
    {
    	std::string tag(blockTag);
        status = m_pDDSController->executeMethod(tag,
                          DDSController::s_pMethodExecutionData.methodID);
    }
    if ((DDSController::s_pMethodExecutionData.m_methodResult == METH_METHOD_ABORT) ||
    		(DDSController::s_pMethodExecutionData.m_methodResult == METH_INTERNAL_ERROR) ||
    		status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Failed to execute method");
        status.setError(FF_ADAPTOR_ERR_EDIT_ACTION_FAILED,
                                        PM_ERR_CL_FF_ADAPTOR);

        //destroyMethodExecutionThread();
       // destroyEditActionThread();
    }

    // Clear state as method execution is in progress
    CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_METHOD_EXEC_IN_PROGRESS);

    return status;
}

/*  ---------------------------------------------------------------------------
    Sets the Method Execution thread state
    Authored By: Utthunga
*/
void FFAdaptor::setMethodExecutionThreadState(THREAD_STATE eThreadState)
{
    ThreadManager::getInstance()->setThreadState(m_methodThreadId, eThreadState);
}

/*  ---------------------------------------------------------------------------
    Retrieves the current state of Method Execution thread
    Authored By: Utthunga
*/
THREAD_STATE FFAdaptor::getMethodExecutionThreadState()
{
    return ThreadManager::getInstance()->getThreadState(m_methodThreadId);
}

/* FILL JSON INFO METHODS *****************************************************/
/*  ----------------------------------------------------------------------------
    Fills Block Parameters with attributes and value in Json
*/
void FFAdaptor::fillBlockParamsJson
(
    ParamInfo* paramInfo,
    Json::Value& blkParamResp
)
{
    std::string itemContainerTag = paramInfo->blockTag.str;
    // Loop through the paramList and prepare minimum Json information
    for (auto& paramItemRef : paramInfo->paramList)
    {
        Json::Value blockParamJson;

        auto paramItem = &paramItemRef.get();

        if (DDL_INVALID_PARAM != paramItem->response_code)
        {
            switch(paramItem->itemType)
            {
                case PARAM_VARIABLE:
                {
                    // Clear Json data if any
                    blockParamJson.clear();
                    auto varParam = dynamic_cast<VariableType*>(paramItem);

                    if (true == varParam->validity)
                    {
                        VarBasicInfo varBasicInfo{};
                        // Fill ItemBasicInfo structure
                        varBasicInfo.basicInfo.itemID = varParam->itemID;
                        varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(varParam->label.str, varParam->label.len);
                        varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varParam->help.str, varParam->help.len);
                        varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;
                        varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;

                        if (varParam->containerId != 0 && varParam->subIndex != 0)
                        {
                       		varBasicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
                        }
                        if (READ_HANDLING != varParam->misc.handling)
                        {
                            varBasicInfo.itemReadOnly = false;
                        }
                        varBasicInfo.itemClass = varParam->misc.class_attr;

                        // Fill the basic information in JsonResponse
                        fillVarBasicJsonInfo(varBasicInfo, blockParamJson);

                        // Fill variable information into Block param Json response
                        fillVariableJsonInfo(varParam, blockParamJson);

                        if (!blockParamJson.empty())
                        {
                   		    blkParamResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(blockParamJson);
                        }
                    }
                    else
                    {
                        LOGF_INFO(CPMAPP, "Validity of item " << varParam->itemID << " is false");
                    }
                }
                break;

                case PARAM_RECORD:
                {
                    blockParamJson.clear();
                    auto recParam = dynamic_cast<RecordType*>(paramItem);

                    if (true == recParam->validity)
                    {
	                    std::string recordName = validateAndGetStringValue(recParam->label.str, recParam->label.len);
	                    for (auto& recordMemListRef : recParam->members)
	                    {
	                        auto recordMemList = &recordMemListRef.get();
	                        switch(recordMemList->itemType)
	                        {
	                            case PARAM_VARIABLE:
	                            {
	                                auto varParam = reinterpret_cast<VariableType*>(recordMemList);

	                                VarBasicInfo varBasicInfo{};

	                                // Fill ItemBasicInfo structure
	                                varBasicInfo.basicInfo.itemID = varParam->itemID;
                                    if (recordName.empty())
                                    {
	                                    varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(varParam->label.str, varParam->label.len);
                                    }
                                    else
                                    {
	                                    varBasicInfo.basicInfo.itemLabel = recordName + " " + validateAndGetStringValue(varParam->label.str, varParam->label.len);
                                    }
	                                varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varParam->help.str, varParam->help.len);
	                                varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
	                                varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;
                                
	                                varBasicInfo.itemClass = varParam->misc.class_attr;
	                                if (READ_HANDLING != varParam->misc.handling)
	                                {
	                                    varBasicInfo.itemReadOnly = false;
	                                }
	                                // Fill the basic information in JsonResponse
	                                fillVarBasicJsonInfo(varBasicInfo, blockParamJson);

	                                // Fill the variable Json response into record Json response
	                                fillVariableJsonInfo(varParam, blockParamJson);
	                            }
	                            default:
	                            {
	                                LOGF_ERROR(CPMAPP, "Invalid member type inside Record " << recordMemList->itemType);
	                            }
	                        }
                            if (!blockParamJson.empty())
                            {
	                            blkParamResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(blockParamJson);
                            }
	                    }    
                	}
                    else
                    {
                        LOGF_INFO(CPMAPP, "Validity of item " << recParam->itemID << " is false");
                    }
                }
                break;

                case PARAM_ARRAY:
                {
                    blockParamJson.clear();
                    auto arrParam = dynamic_cast<ArrayType*>(paramItem);
                    ItemBasicInfo arrBasicInfo{};

                    if (true == arrParam->validity)
                    {
                        // Iterate through the list of array members and prepare Json response
                        for (auto& arrayMemListRef : arrParam->members)
                        {
                            auto arrayMemList = &arrayMemListRef.get();

                            if (PARAM_VARIABLE == arrayMemList->itemType)
                            {
                                auto varParam = reinterpret_cast<VariableType*>(arrayMemList);

                                VarBasicInfo varBasicInfo{};

                                Json::Value arrVarJsonResp;

                                // Fill ItemBasicInfo structure
                                varBasicInfo.basicInfo.itemID = varParam->itemID;
                                unsigned int subIndex = varParam->subIndex;
                                if (subIndex > 0)
                                {
                                    // According to FCG_TS61804-4_2.0__PR3_EDD-Interpretation.pdf
                                    // Array labels must be prefixed with Array_Label[Array_Index]
                                    varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(arrParam->label.str, arrParam->label.len) + getArrayIndex(subIndex);
                                }
                                varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varParam->help.str, varParam->help.len);
                                varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
                                varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;

                                if (READ_HANDLING != varParam->misc.handling)
                                {
                                    varBasicInfo.itemReadOnly = false;
                                }
                                varBasicInfo.itemClass = varParam->misc.class_attr;

                                // Fill the basic information in JsonResponse
                                fillVarBasicJsonInfo(varBasicInfo, blockParamJson);

                                // Fill the variable Json response into array Json response
                                fillVariableJsonInfo(varParam, blockParamJson);
                            }
                            else
                            {
                                LOGF_ERROR(CPMAPP, "Member ItemType " << arrayMemList->itemType
                                           << " is not supported inside Array");
                            }

                            if (!blockParamJson.empty())
                            {
	                            blkParamResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(blockParamJson);
                            }
                        }
                    }
                    else
                    {
                        LOGF_INFO(CPMAPP, "Validity of item " << arrParam->itemID << " is false");
                    }
                }
                break;

                case PARAM_IMAGE:
                {
                    // Clear Json data if any
                    blockParamJson.clear();
                    auto imgParam = dynamic_cast<ImageType*>(paramItem);

                    if (true == imgParam->validity)
                    {
                        ItemBasicInfo imageBasicInfo{};
                        // Fill ItemBasicInfo structure
                        imageBasicInfo.itemID = imgParam->itemID;
                        imageBasicInfo.itemLabel = validateAndGetStringValue(imgParam->label.str, imgParam->label.len);
                        imageBasicInfo.itemHelp = validateAndGetStringValue(imgParam->help.str, imgParam->help.len);
                        imageBasicInfo.itemContainerTag = itemContainerTag;
                        imageBasicInfo.itemType = JSON_IMAGE_ITEM_TYPE;

                        // Fill the basic information in JsonResponse
                        fillItemBasicJsonInfo(imageBasicInfo, blockParamJson);

                        // Fill image information into Block param Json response
                        fillImageJsonInfo(imgParam, blockParamJson);
                        blkParamResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(blockParamJson);
                    }
                    else
                    {
                        LOGF_INFO(CPMAPP, "Validity of item " << imgParam->itemID << " is false");
                    }
                }
                break;

                default:
                {
                    /* There are many items which are yet to be supported
                     * Print corresponding error text and proceed
                     * Ignoring the un-supported artifacts instead return error
                     */
                    LOGF_ERROR(CPMAPP, "Invalid Item Type: "<< paramItem->itemType);
                }
                break;
            } // !-- swith case
        } // !-- if (paramItem->response_code == DDI_INVALID_PARAM)
    }
}

/*  ----------------------------------------------------------------------------
    Fills item basic attribute information into Json response
*/
void FFAdaptor::fillItemBasicJsonInfo
(
    ItemBasicInfo& itemBasicInfo,
    Json::Value& jsonResp
)
{
    // Fill the basic attribute information into json response
    jsonResp[JSON_ITEM_LABEL] = itemBasicInfo.itemLabel;
    jsonResp[JSON_ITEM_HELP] = itemBasicInfo.itemHelp;
    jsonResp[JSON_ITEM_TYPE] = itemBasicInfo.itemType;
    jsonResp[JSON_ITEM_CONT_TAG] = itemBasicInfo.itemContainerTag;
    jsonResp[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(itemBasicInfo.itemID);
    jsonResp[JSON_ITEM_IN_DD] = itemBasicInfo.itemInDD;
}

/*  ----------------------------------------------------------------------------
    Fills variable attribute information into Json response
*/
void FFAdaptor::fillVarBasicJsonInfo
(
    VarBasicInfo& varBasicInfo,
    Json::Value& jsonResp
)
{
    // Fill the default minimum information required for a variable
    fillItemBasicJsonInfo(varBasicInfo.basicInfo, jsonResp);
    // Fill information required for a variable
    jsonResp[JSON_ITEM_INFO][JSON_ITEM_READONLY] = varBasicInfo.itemReadOnly;
    jsonResp[JSON_ITEM_INFO][JSON_ITEM_CLASS] = static_cast<Json::Value::LargestUInt>(varBasicInfo.itemClass);
}

/*  ----------------------------------------------------------------------------
    Fills Variable information into Json response
*/
void FFAdaptor::fillVariableJsonInfo
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType    
)
{
    if (varParam == nullptr)
    {
        LOGF_ERROR(CPMAPP,"GET Variable ERROR:null pointer");
        return;
    }

    if ((varParam->type == DDS_INTEGER) ||
        (varParam->type == DDS_UNSIGNED) ||
        (varParam->type == DDS_FLOAT) ||
        (varParam->type == DDS_DOUBLE) ||
        (varParam->type == DDS_BOOLEAN_T))
    {
        fillVarArithmeticJson(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == DDS_ENUMERATED) ||
         (varParam->type == DDS_BIT_ENUMERATED))
    {
        fillVarEnumerationJson(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == DDS_ASCII) ||
         (varParam->type == DDS_PACKED_ASCII)||
         (varParam->type == DDS_PASSWORD)||
         (varParam->type == DDS_BITSTRING) ||
         (varParam->type == DDS_OCTETSTRING) ||
         (varParam->type == DDS_VISIBLESTRING) ||
         (varParam->type == DDS_EUC))
    {
        fillVarStringJson(varParam, varJsonResp, varMaskType);
    }
    else if ((varParam->type == DDS_TIME)||
         (varParam->type == DDS_DATE_AND_TIME) ||
         (varParam->type == DDS_DURATION) ||
                 (varParam->type == DDS_TIME_VALUE))
    {
        fillVarDateTimeJson(varParam, varJsonResp, varMaskType);
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Invalid Variable Type: " << varParam->type);
        varJsonResp.clear();
        varJsonResp = Json::ValueType::nullValue;
        return;
    }

    if (nullptr != varParam->itemContainerTag.str)
    {
        fillVariableHasRelation(varParam, varJsonResp);
    }

    varJsonResp[JSON_ITEM_INFO][JSON_VAR_CONT_ID] = static_cast<Json::Value::LargestUInt>(varParam->containerId);
    varJsonResp[JSON_ITEM_INFO][JSON_VAR_SUBINDEX] = varParam->subIndex;
}

/*  ----------------------------------------------------------------------------
    Check the Write As One relation for the variable and fills it in variable
    JSON structure
*/
void FFAdaptor::fillVariableHasRelation(VariableType* varParam,
                                        Json::Value& varJsonResp)
{
    DDI_BLOCK_SPECIFIER blockSpecifier{};
    blockSpecifier.type = DDI_BLOCK_TAG;
    blockSpecifier.block.tag = const_cast<char*>(varParam->itemContainerTag.str);

    DDI_PARAM_SPECIFIER paramSpecifier{};
    paramSpecifier.type = DDI_PS_ITEM_ID;
    paramSpecifier.item.id = varParam->itemID;

    ITEM_ID waoID = INVALID_ID_OR_HANDLE;
    m_pDDSController->ddiGetWao(&blockSpecifier, &paramSpecifier, &waoID);
    if (waoID != INVALID_ID_OR_HANDLE)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_ITEM_HAS_RELATION] = true;
    }
}

/*  ----------------------------------------------------------------------------
    Fills Variable Arithmetic information into Json response
*/
void FFAdaptor::fillVarArithmeticJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ARITHMETIC;
        (*tempJsonObj)[JSON_DISPLAY_FORMAT] = validateAndGetStringValue(varParam->displayFormat.str, varParam->displayFormat.len);
        (*tempJsonObj)[JSON_EDIT_FORMAT] = validateAndGetStringValue(varParam->editFormat.str, varParam->editFormat.len);
        (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);

        // Assign min and max values
        assignMinMaxValues(varParam, varJsonResp);
    }

    (*tempJsonObj)[JSON_VAR_UNIT] = validateAndGetStringValue(varParam->unit.str, varParam->unit.len);
    
    fillVarValue(varParam, *tempJsonObj);

    if (DDS_FLOAT == varParam->type) 
    {
        (*tempJsonObj)[JSON_VAR_VAL_IS_NAN] = isNAN(varParam->value.stdValue.f);
        (*tempJsonObj)[JSON_VAR_VAL_IS_INF] = isINF(varParam->value.stdValue.f);
        
        if ((*tempJsonObj)[JSON_VAR_VAL_IS_NAN].asBool() || 
           (*tempJsonObj)[JSON_VAR_VAL_IS_INF].asBool())
        {
            (*tempJsonObj)[JSON_VAR_VALUE] = std::numeric_limits<float>::max();
        }
    }

    if (DDS_DOUBLE == varParam->type)
    {
        (*tempJsonObj)[JSON_VAR_VAL_IS_NAN] = isNAN(varParam->value.stdValue.d);
        (*tempJsonObj)[JSON_VAR_VAL_IS_INF] = isINF(varParam->value.stdValue.d);
        
        if ((*tempJsonObj)[JSON_VAR_VAL_IS_NAN].asBool() || 
           (*tempJsonObj)[JSON_VAR_VAL_IS_INF].asBool())
        {
            (*tempJsonObj)[JSON_VAR_VALUE] = std::numeric_limits<double>::max();
        }
    }
}

/*  ----------------------------------------------------------------------------
    Fills Enumeration information into Json response
*/
void FFAdaptor::fillVarEnumerationJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_ENUM;
        switch (varParam->type)
        {
            case DDS_ENUMERATED:
            {
                (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_TYPE_ENUM;
            }
            break;

            case DDS_BIT_ENUMERATED:
            {
        	    if (varParam->bitIndex > 0)
			    {
				    (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_BITENUM_ITEM;
				    (*tempJsonObj)[JSON_VAR_BIT_INDEX] = static_cast<Json::Value::LargestUInt>(varParam->bitIndex);
			    }
			    else
			    {
				    (*tempJsonObj)[JSON_VAR_VAL_TYPE] = JSON_VAR_VAL_TYPE_BITENUM;
			    }
            } 
            break;

            default:
            LOGF_ERROR(CPMAPP,"Invalid Enumeration type "<< varParam->type);
            break;
        }//!-- switch

	   if (0 == varParam->bitIndex)
	   {
	        int countEnum = varParam->enums.size();

	        for (int index = 0; index < countEnum; index++)
	        {
	            Json::Value enumJsonObj;

            	enumJsonObj[JSON_VAR_ENUM_VALUE] = static_cast<Json::Value::LargestUInt>(varParam->enums.at(index).value);
            	enumJsonObj[JSON_VAR_ENUM_LABEL] = validateAndGetStringValue(varParam->enums.at(index).label.str, varParam->enums.at(index).label.len);
            	enumJsonObj[JSON_VAR_ENUM_HELP] =  validateAndGetStringValue(varParam->enums.at(index).help.str, varParam->enums.at(index).help.len);

	            (*tempJsonObj)[JSON_VAR_ENUM_INFO].append(enumJsonObj);
	        }
	   } 
	}//!--if
	
    if ((varParam->type == DDS_BIT_ENUMERATED) && (varParam->bitIndex > 0))
    {
       // variable is Bit ENUMERATION ITEM type, fill only corresponding bit<boolean> value
       unsigned int bitPos = CPMUtil::getBitPos(varParam->bitIndex);
	   (*tempJsonObj)[JSON_VAR_VALUE] = (varParam->value.stdValue.ul >> bitPos) & BIT_ON ? true : false;
    }
    else
    {
       (*tempJsonObj)[JSON_VAR_VALUE] = static_cast<Json::Value::LargestUInt>(varParam->value.stdValue.ul);
    }
}

/*  ----------------------------------------------------------------------------
    Fills String Information into Json response
*/
void FFAdaptor::fillVarStringJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE]= JSON_VAR_TYPE_STRING;

        Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
        (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);
        (*tempJsonObj)[JSON_VAR_VAL_LENGTH] = varParam->value.strData.len;
        (*tempJsonObj)[JSON_VAR_CHAR_SET] = getCharSetType(varParam->value.strData.ucType);
    }
    
    (*tempJsonObj)[JSON_VAR_VALUE] = validateAndGetStringValue(varParam->value.strData.str, varParam->value.strData.len);
}

/*  ----------------------------------------------------------------------------
    Fills Date-Time Information into Json response
*/
void FFAdaptor::fillVarDateTimeJson
(
    VariableType* varParam,
    Json::Value& varJsonResp,
    VAR_JSON_ATTR_MASK varMaskType
)
{
    Json::Value *tempJsonObj = &varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO];
    if (VAR_JSON_ATTR_MASK::VAR_JSON_VALUE_MASK != varMaskType)
    {
        varJsonResp[JSON_ITEM_INFO][JSON_VAR_TYPE] = JSON_VAR_TYPE_DATE_TIME;

        (*tempJsonObj)[JSON_DISPLAY_FORMAT] = validateAndGetStringValue(varParam->displayFormat.str, varParam->displayFormat.len);
        (*tempJsonObj)[JSON_VAR_VAL_TYPE] = getVarValType(varParam->type);

        // Assign min and max values
        assignMinMaxValues(varParam, varJsonResp);
    }

    fillVarValue(varParam, *tempJsonObj);
}

/*  ----------------------------------------------------------------------------
    Assign min and max range values
*/
void FFAdaptor::assignMinMaxValues
(
    VariableType* varParam,
    Json::Value& varJsonResp
)
{
    if (!varParam->misc.minMaxRange.empty())
    {
        for (unsigned index = 0 ; index < varParam->misc.minMaxRange.size(); index++)
        {
            Json::Value tempJsonObj;

            switch(varParam->type)
            {
                case DDS_INTEGER:
                {
                    if (varParam->misc.isMinPresent)
                    {
#ifdef CPM_64_BIT_OS
                        tempJsonObj[JSON_VAR_VAL_MIN] = static_cast<int>(varParam->misc.minMaxRange.at(index).min.l);
#else
                        tempJsonObj[JSON_VAR_VAL_MIN] = static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).min.l);
#endif
                    }
                    if (varParam->misc.isMaxPresent)
                    {
#ifdef CPM_64_BIT_OS
                        tempJsonObj[JSON_VAR_VAL_MAX] = static_cast<int>(varParam->misc.minMaxRange.at(index).max.l);
#else
                        tempJsonObj[JSON_VAR_VAL_MAX] = static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).max.l);
#endif
                    }
                }
                break;

                case DDS_DATE_AND_TIME:
                {
                    if (varParam->misc.isMinPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MIN] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).min.l);
                    }
                    if (varParam->misc.isMaxPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MAX] =
                                static_cast<Json::Value::LargestInt>(varParam->misc.minMaxRange.at(index).max.l);
                    }
                }
                break;

                case DDS_UNSIGNED:
                case DDS_TIME:          //FALLTHROUGH
                case DDS_DURATION:      //FALLTHROUGH
                case DDS_TIME_VALUE:    //FALLTHROUGH
                {
                    if (varParam->misc.isMinPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MIN] =
                                static_cast<Json::Value::LargestUInt>(varParam->misc.minMaxRange.at(index).min.ul);
                    }
                    if (varParam->misc.isMaxPresent)
                    {
                    	tempJsonObj[JSON_VAR_VAL_MAX] = 
                                static_cast<Json::Value::LargestUInt>(varParam->misc.minMaxRange.at(index).max.ul);
                    }
                }
                break;

                case DDS_FLOAT:
                {
                    if (varParam->misc.isMinPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MIN] =
                                varParam->misc.minMaxRange.at(index).min.f;
                    }
                    if (varParam->misc.isMaxPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MAX] =
                                varParam->misc.minMaxRange.at(index).max.f;
                    }
                }
                 break;

                case DDS_DOUBLE:
                {
                    if (varParam->misc.isMinPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MIN] =
                                varParam->misc.minMaxRange.at(index).min.d;
                    }
                    if (varParam->misc.isMaxPresent)
                    {
                        tempJsonObj[JSON_VAR_VAL_MAX] =
                                varParam->misc.minMaxRange.at(index).max.d;
                    }
                }
                break;

                default:
                {
                    LOGF_ERROR(CPMAPP,"Invalid Type: "<< varParam->type);
                }
                break;
            }
            varJsonResp[JSON_ITEM_INFO][JSON_VAR_INFO][JSON_VAR_VAL_RANGE].append(tempJsonObj);
        }
    }
}

/*  ----------------------------------------------------------------------------
    Retrieves variable value type from ddsVarValMap
*/
std::string FFAdaptor::getVarValType(unsigned int varType)
{
    auto it = ddsVarValMap.find(varType);

    return it->second;
}

/*  ----------------------------------------------------------------------------
    Retrieves character set type form map
*/
std::string FFAdaptor::getCharSetType(unsigned int charSetVal)
{
    auto it = charSetMap.find(charSetVal);

    return it->second;
}

/*  ----------------------------------------------------------------------------
    Fills Record information into Json response
*/
void FFAdaptor::fillRecordJsonInfo
(
    RecordType recParam,
    Json::Value& recordJsonResp,
    string itemContainerTag
)
{
    Json::Value varJsonResp;
    std::string recordName = validateAndGetStringValue(recParam.label.str, recParam.label.len);

    // Iterate through the list of record members and prepare Json response
    for (auto& recordMemListRef : recParam.members)
    {
        auto recordMemList = &recordMemListRef.get();
        switch(recordMemList->itemType)
        {
            case PARAM_VARIABLE:
            {
                varJsonResp.clear();
                auto varParam = reinterpret_cast<VariableType*>(recordMemList);

                VarBasicInfo varBasicInfo{};

                // Fill ItemBasicInfo structure
                varBasicInfo.basicInfo.itemID = varParam->itemID;

                if (recordName.empty())
                {
                    varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(varParam->label.str, varParam->label.len);
                }
                else
                {
                    varBasicInfo.basicInfo.itemLabel = recordName + " "+ validateAndGetStringValue(varParam->label.str, varParam->label.len);
                }
                varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varParam->help.str, varParam->help.len);
                varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
                varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;
                
                varBasicInfo.itemClass = varParam->misc.class_attr;
                if (READ_HANDLING != varParam->misc.handling)
                {
                    varBasicInfo.itemReadOnly = false;
                }
                // Fill the basic information in JsonResponse
                fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

                // Fill the variable Json response into record Json response
                fillVariableJsonInfo(varParam, varJsonResp);

                // Append the Variable Json response into ItemList of Record response
                if (!varJsonResp.empty())
                {
                    recordJsonResp[JSON_ITEM_INFO][JSON_ITEM_LIST].append(varJsonResp);
                }
            }
            break;

            default:
            {
                LOGF_ERROR(CPMAPP,"Member ItemType " <<recordMemList->itemType
                            << " is not supported inside Record");
            }
            break;
        }//!-- switch

    } //!-- for
}

/*  ----------------------------------------------------------------------------
    Fills Array information into Json response
*/
void FFAdaptor::fillArrayJsonInfo
(
    ArrayType arrParam,
    Json::Value& blockParamJson,
    string itemContainerTag
)
{
    // Iterate through the list of array members and prepare Json response
    for (auto& arrayMemListRef : arrParam.members)
    {
        auto arrayMemList = &arrayMemListRef.get();

        if (PARAM_VARIABLE == arrayMemList->itemType)
        {
            auto varParam = reinterpret_cast<VariableType*>(arrayMemList);

            VarBasicInfo varBasicInfo{};

            Json::Value varJsonResp;

            // Fill ItemBasicInfo structure
            varBasicInfo.basicInfo.itemID = varParam->itemID;
            unsigned int subIndex = varParam->subIndex;
            if (subIndex > 0)
            {
                // According to FCG_TS61804-4_2.0__PR3_EDD-Interpretation.pdf
                // Array labels must be prefixed with Array_Label[Array_Index]
                varBasicInfo.basicInfo.itemLabel = validateAndGetStringValue(arrParam.label.str, arrParam.label.len) + getArrayIndex(subIndex);
            }
            varBasicInfo.basicInfo.itemHelp = validateAndGetStringValue(varParam->help.str, varParam->help.len);
            varBasicInfo.basicInfo.itemType = JSON_VARIABLE_ITEM_TYPE;
            varBasicInfo.basicInfo.itemContainerTag = itemContainerTag;

            if (READ_HANDLING != varParam->misc.handling)
            {
                varBasicInfo.itemReadOnly = false;
            }
            varBasicInfo.itemClass = varParam->misc.class_attr;

            // Fill the basic information in JsonResponse
            fillVarBasicJsonInfo(varBasicInfo, varJsonResp);

            // Fill the variable Json response into array Json response
            fillVariableJsonInfo(varParam, varJsonResp);

            // Append the Variable Json response into ItemList of Array response
            if (!varJsonResp.empty())
            {
                blockParamJson[JSON_ITEM_INFO][JSON_ITEM_LIST].append(varJsonResp);
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP,"Member ItemType " << arrayMemList->itemType
                       << " is not supported inside Array");
        }
    }
}

/*  ----------------------------------------------------------------------------
    Fills Image information into Json response
    Authored By: Utthunga
*/
void FFAdaptor::fillImageJsonInfo
(
    ImageType* imageParam,
    Json::Value& imageParamJson
)
{
    char* imagePath = const_cast<char*>(storeImage(imageParam));
    if (nullptr == imagePath)
    {
        LOGF_ERROR(CPMAPP, "Failed to get Image : " << imageParam->itemID);
        return;
    }

    imageParamJson[JSON_ITEM_INFO][JSON_IMAGE_PATH] = imagePath;

    if (nullptr != imagePath)
    {
        free(imagePath);
        imagePath = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Returns top level menu for the given device
*/
void FFAdaptor::getTopLevelMenu(Json::Value & root)
{
    PMStatus status;
    ENV_INFO2 envInfo2{};
    APP_INFO appInfo{};
    ItemBasicInfo sMenu;

    std::list<ItemBasicInfo> itemList;

    // Extract the Label information for the menu's
    DDI_GENERIC_ITEM    ddiGenericItem{};
    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};

    // Get the current Device handle
    DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
    status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        root = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    // Set the Environment Info for ddi_get_tem2
    envInfo2.device_handle = deviceHandle;
    envInfo2.type = ENV_DEVICE_HANDLE;
    envInfo2.app_info = &appInfo;

    // Find the Top Level Root  Menu ID's from the symbol file
    vector<unsigned long> topMenu;
    ITEM_ID menuID{};

    for (auto & menuName : gRootMenus)
    {
        status = m_pDDSController->ddiLocateSymbolId(&envInfo2, menuName, &menuID);
        if (status.hasError())
        {
            // Root Menu not found in the Symbol File
            continue;
        }
        // Root Menu found in symbol file, store the ID
        topMenu.push_back(menuID);
    }

    for (auto& menuID : topMenu)
    {
        memset(&ddiGenericItem, 0, sizeof(DDI_GENERIC_ITEM));
        memset(&ddiItemSpecifier, 0, sizeof(DDI_ITEM_SPECIFIER));

        ddiItemSpecifier.type = DDI_ITEM_ID;
        ddiItemSpecifier.item.id = menuID;

        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier,
                     &envInfo2, ALL_ITEM_ATTR_MASK, &ddiGenericItem);

        if (status.hasError())
        {
            // Unable to retrieve top level menu information from DD
            // Clear the Generic Item memory allocated by ddiGetItem API
            m_pDDSController->ddiCleanItem(&ddiGenericItem);
            continue;
        }

        FLAT_MENU *pMenu = static_cast<FLAT_MENU*>(ddiGenericItem.item);

        if (nullptr != pMenu)
        {
            sMenu.itemLabel = validateAndGetStringValue(pMenu->label.str, pMenu->label.len);
            sMenu.itemID    = pMenu->id;
            sMenu.itemType  = JSON_MENU_ITEM_TYPE;
            sMenu.itemInDD  = TRUE;
            /* Since the deveice level menu's doeas nt belong to any blocks
               the item container tag shall be empty */
            sMenu.itemContainerTag = "";

            // Add the menu item to the list
            itemList.push_back(sMenu);
        }
        // Clear the Generic Item memory allocated by ddiGetItem API
        m_pDDSController->ddiCleanItem(&ddiGenericItem);
    }

    // Add the Advanced option to the Top Level Menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.help;
    sMenu.itemContainerTag = "";

    itemList.push_back(sMenu);

    // Set the attributes of the top level menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU.help;
    sMenu.itemContainerTag = "";
    // Now prepare the Json for top level menu
    getMenuAsJson(sMenu, itemList, root);

    // Clear the PM Status object
    status.clear();
}

/*  ----------------------------------------------------------------------------
    Returns menu in Json format for the given item list
*/
void FFAdaptor::getMenuAsJson(ItemBasicInfo & sMenuInfo,
                              list<ItemBasicInfo> & menuItems,
                              Json::Value & output)
{
    // Fill the attributes for the Menu
    fillItemBasicJsonInfo(sMenuInfo, output);

    // Fill the Menu Items one by one
    for (auto & menuItem : menuItems)
    {
        Json::Value itemNode;
        fillItemBasicJsonInfo(menuItem, itemNode);

        // Append to the item List
        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemNode);
    }
}

/*  ----------------------------------------------------------------------------
    Returns DD Menu information
*/
void FFAdaptor::getDDMenu(const Json::Value & input, Json::Value & output)
{
    DDI_BLOCK_SPECIFIER ddiBlockSpecifier{};
    DDI_ITEM_SPECIFIER  ddiItemSpecifier{};
    DDI_GENERIC_ITEM    ddiGenericItem{};
    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    PMStatus status;

    // Get the Block tag
    std::string itemContainerTag;
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    // Fill the Item Specifier
    ddiItemSpecifier.type = DDI_ITEM_ID;
    ITEM_ID itemID = input[JSON_ITEM_ID].asLargestUInt();
    ddiItemSpecifier.item.id = itemID;

    if (itemContainerTag.empty())
    {
        /* Device Level Standard Menu's, are not associated with any blocks,
           needs to be handled seperately
        */
        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};

        // Get the current Device handle
        status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);

        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
            Error::Entry error = Error::cpm.GET_MENU_FAILED;
            output = JsonRpcUtil::setErrorObject(error.code, error.message);
            return;
        }

        // Set the Environment Info for ddi_get_tem2
        ENV_INFO2 envInfo2{};
        envInfo2.device_handle = deviceHandle;
        envInfo2.type = ENV_DEVICE_HANDLE;

        envInfo2.app_info = &appInfo;
        appInfo.status_info = USE_EDIT_VALUE;

        // Deveic Level standard Menu's, hence use ddi_get_item2
        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                               ALL_ITEM_ATTR_MASK, &ddiGenericItem);
    }
    else
    {
        // Fill the Block Specifier
        ddiBlockSpecifier.type = DDI_BLOCK_TAG;
        ddiBlockSpecifier.block.tag = const_cast<char*>(itemContainerTag.c_str());
        envInfo.app_info = &appInfo;

        // Must be Block Level Menu's - Hence use ddi_get_item
        status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier,
                                              &envInfo, ALL_ITEM_ATTR_MASK, &ddiGenericItem);
    }

    if (status.hasError())
    {
        // Log the Error
        LOGF_ERROR(CPMAPP, "DDI Get Item Failed for the Item ID : "  << ddiItemSpecifier.item.id);
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        FLAT_MENU* pMenu = static_cast<FLAT_MENU*>(ddiGenericItem.item);

        if (nullptr != pMenu)
        {
            ItemBasicInfo menuInfo;
            // Retrieve Item ID and corresponding block Tag of the item.
            ITEM_ID itemID{0};
            ITEM_TYPE itemType{0};
            std::string blockTag = itemContainerTag;

            // Fill the Menu attributes
            fillBasicMenuAttributes(input, menuInfo);

            // Help is not available in FLAT_MENU, hence filled only label
            if (nullptr != pMenu->label.str)
            {
                menuInfo.itemLabel = validateAndGetStringValue(pMenu->label.str, pMenu->label.len);
            }

            // Add the menu information to output Json
            fillItemBasicJsonInfo(menuInfo, output);

            unsigned int itemIndex{0};
            // Now add all the menu items one by one
            while (itemIndex < pMenu->items.count)
            {
                // Retrieve Item ID and corresponding block Tag of the item.
                itemID     = pMenu->items.list[itemIndex].ref.op_id;
                itemType   = pMenu->items.list[itemIndex].ref.op_type;
                if (EDIT_DISP_ITYPE == itemType)
                {
                    itemType = MENU_ITYPE;
                }
                if (WAO_ITYPE == itemType)
                {
                    itemType = VARIABLE_ITYPE;
                }

                if (itemID > 0)
                {
                    /* Get the actual tag using Block ID and block Instance for menu items
                       contained in device level menu. This is not required for Block Level
                       menu's as the menu items in the block menu's will always belong to the same block
                    */
                    if (itemContainerTag.empty())
                    {
                        ITEM_ID blockID = pMenu->items.list[itemIndex].ref.op_block_id;
                        ITEM_ID blockInstance = pMenu->items.list[itemIndex].ref.op_block_instance;

                        // Get the block Tag block ID and instance is filled by DDS
                        if (blockID != 0)
                        {
                            blockTag = m_pDDSController->getBlockTagByBlockID(blockID, blockInstance);
                        }
                    }

                    // Fill the common information required for all the menu items
                    Json::Value itemInfoJson;
                    ItemBasicInfo sItemInfo;

                    // Fill menu item attributes
                    sItemInfo.itemID   = itemID;
                    sItemInfo.itemType = m_itemTypeMap[itemType];
                    sItemInfo.itemContainerTag = blockTag;
                    sItemInfo.itemInDD = true;

                    if (blockTag.empty())
                    {
                        /* Device Level Standard Menu's, are not associated with any blocks,
                           needs to be handled seperately
                        */
                        ENV_INFO2 envInfo2{};
                        DeviceHandle deviceHandle{INVALID_ID_OR_HANDLE};
                        DDI_GENERIC_ITEM    ddiGenericItem2{};

                        // Get the current Device handle
                        status = m_pDDSController->getActiveDeviceHandle(&deviceHandle);
                        ddiItemSpecifier.item.id = itemID;

                        if (status.hasError())
                        {
                            LOGF_ERROR(CPMAPP,"Invalid Device Handle : " << deviceHandle);
                            Error::Entry error = Error::cpm.GET_MENU_FAILED;
                            output = JsonRpcUtil::setErrorObject(error.code, error.message);
                            return;
                        }

                        // Set the Environment Info for ddi_get_tem2
                        envInfo2.device_handle = deviceHandle;
                        envInfo2.type = ENV_DEVICE_HANDLE;

                        envInfo2.app_info = &appInfo;
                        appInfo.status_info = USE_EDIT_VALUE;

                        // Deveic Level standard Menu's, hence use ddi_get_item2 to get further information
                        status = m_pDDSController->ddiGetItem2(&ddiItemSpecifier, &envInfo2,
                                                               ALL_ITEM_ATTR_MASK, &ddiGenericItem2);

                        if (!status.hasError())
                        {
                            if (fillMenuItemInfoByType(sItemInfo, ddiGenericItem2))
                            {
                                // Fill the Basic information as Json
                                fillItemBasicJsonInfo(sItemInfo, itemInfoJson);
                                // Append to the item List
                                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                            }
                        }
                        else
                        {
                            LOGF_ERROR(CPMAPP, "DDI Get Item 2 Failed with Error : " << status.getErrorCode());
                        }

                        // Clear the Generic Item memory allocated by ddiGetItem API
                        m_pDDSController->ddiCleanItem(&ddiGenericItem2);

                        if (status.hasError())
                        {
                            LOGF_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
                        }
                    }
                    else
                    {
                        bool bIsValid = true;

                        // Fill the menu item according to the item type
                        switch (itemType)
                        {
                            case VARIABLE_ITYPE:
                            {
                                ParamItems varItems{};
                                varItems.paramItemID = itemID;
                                varItems.paramContainerTag = blockTag;
								varItems.paramSubIndex = pMenu->items.list[itemIndex].ref.op_subindex;
								varItems.paramBitIndex  = pMenu->items.list[itemIndex].ref.bit_mask;

                                // Get the complete information for Variable to be shown in Menu
                                itemInfoJson = getVariable(varItems, bIsValid, VARIABLE_VAL_MASK::VAR_ATTR_DD_VAL_INFO);

                                if (true == bIsValid && false == itemInfoJson.isNull())
                                {
                                    // Check for the Qualifiers and update the JSON accordingly
                                    applyQualifiersForVariables(pMenu->items.list[itemIndex].qual, itemInfoJson);
                                    // Append to the item List
                                    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                                }
                            }
                            break;
                            case RECORD_ITYPE:
                            {
                                if (0 == pMenu->items.list[itemIndex].ref.op_subindex)
								{
								    ParamItems recItems{};
                                    recItems.paramItemID = itemID;
                                    recItems.paramContainerTag = blockTag;
                                    recItems.paramSubIndex = 0;
									recItems.paramSubIndex = pMenu->items.list[itemIndex].ref.op_subindex;
									recItems.paramBitIndex  = pMenu->items.list[itemIndex].ref.bit_mask;
								    getRecord(recItems, output, pMenu->items.list[itemIndex].qual);
								}
								else
								{
								    ParamItems recVarItems{};
                                    recVarItems.paramItemID = pMenu->items.list[itemIndex].ref.desc_id;
                                    recVarItems.paramContainerTag = blockTag;
								    recVarItems.paramContainerID = itemID;
				                	recVarItems.paramSubIndex = pMenu->items.list[itemIndex].ref.op_subindex;
									recVarItems.paramBitIndex  = pMenu->items.list[itemIndex].ref.bit_mask;
								    getRecordVar(recVarItems, output, pMenu->items.list[itemIndex].qual);
								}				
                            }
                            break;
                            case IMAGE_ITYPE:
                            {
                                // Get the minimum information for image to be shown in Menu
                                itemInfoJson = getImage(itemID, blockTag, bIsValid);

                                if (true == bIsValid)
                                {
                                    // Append to the item List
                                    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                                    // Apply Qualifier for Image
                                    // if (pMenu->items.list[itemIndex].qual && INLINE_ITEM)
                                    //{
					// TODO(Vinay) - POST MAP
                                        // Add Inline Item Qualifier - Not Approved for MAP
                                    //}
                                }
                            }
                            break;
                            case ARRAY_ITYPE:
                            {
                                ParamItems arrItems{};
                                arrItems.paramItemID = itemID;
                                arrItems.paramContainerTag = blockTag;
                                arrItems.paramSubIndex = pMenu->items.list[itemIndex].ref.op_subindex;
                                if( (pMenu->items.list[itemIndex].ref.bit_mask) > 0)
								{
                                	arrItems.paramBitIndex  = pMenu->items.list[itemIndex].ref.bit_mask ;
								}
                                getArray(arrItems, output, pMenu->items.list[itemIndex].qual);
                            }
                            break;
                            case MENU_ITYPE:
                            {
                                //if (pMenu->items.list[itemIndex].qual && REVIEW_ITEM)
                                //{
				    // TODO(Vinay) - POST MAP
                                    // Add Review Item Qualifier - Not Approved for MAP
                                //}
                            }
                            // Intentional Fall through
                            case METHOD_ITYPE:
                            case EDIT_DISP_ITYPE:
                            /*Other item types to be added here*/
                            {
                                DDI_GENERIC_ITEM    ddiGenericItem1{};
                                memset(&ddiItemSpecifier, 0, sizeof(DDI_ITEM_SPECIFIER));
                                memset(&ddiBlockSpecifier, 0, sizeof(DDI_BLOCK_SPECIFIER));
                                memset(&envInfo, 0, sizeof(ENV_INFO));

                                envInfo.app_info = &appInfo;

                                // Fill the item Specifier
                                ddiItemSpecifier.type = DDI_ITEM_ID;
                                ddiItemSpecifier.item.id = sItemInfo.itemID;

                                // Fill the Block Specifier
                                ddiBlockSpecifier.type = DDI_BLOCK_TAG;
                                ddiBlockSpecifier.block.tag = const_cast<char *>(sItemInfo.itemContainerTag.c_str());

                                // Get the item from DD
                                status = m_pDDSController->ddiGetItem(&ddiBlockSpecifier, &ddiItemSpecifier, &envInfo, ALL_ITEM_ATTR_MASK, &ddiGenericItem1);

                                if (!status.hasError())
                                {
                                    if (fillMenuItemInfoByType(sItemInfo, ddiGenericItem1))
                                    {
                                        // Fill the information as Json
                                        fillItemBasicJsonInfo(sItemInfo, itemInfoJson);
                                        // Append to the item List
                                        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
                                    }
                                }
                                else
                                {
                                    LOGF_ERROR(CPMAPP, "DDI Get Item Failed. Error Code = " << status.getErrorCode() << ". Item Type = " << itemType);
                                }

                                // Clear the Generic Item memory allocated by ddiGetItem API
                                status = m_pDDSController->ddiCleanItem(&ddiGenericItem1);

                                if (status.hasError())
                                {
                                    LOGF_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
                                }
                            }
                            break;
                            case RESERVED_ITYPE1:
                            // //////////////////// DO NOT DELETE THE COMMENT //////////////////////
                            /* All the items such as ROWBREAK, COLUMNBREAK, SEPARATOR & STRING Items
                               are classified as RESERVED_ITYPE1 item type. The qualifier field
                               differentiates if it is a ROWBREAK or a COLUMNBREAK.
                               As these are not suported for a handheld, all these items are
                               skipped and will not be given to Instrument Controller - Starsky*/
                            // //////////////////// DO NOT DELETE THE COMMENT //////////////////////
                            default:
                                LOGF_ERROR(CPMAPP, " This Item Type is not Supported. Item Type : " << itemType);
                            break;
                        }
                    }
                }
                // Move to the next item
                ++itemIndex;
            }
        }
    }

    // Clear the PM Status object
    status.clear();

    // Clear the Generic Item memory allocated by ddiGetItem API
    m_pDDSController->ddiCleanItem(&ddiGenericItem);

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
    }
}

/*  ----------------------------------------------------------------------------
    Returns Custom Menu information
*/
void FFAdaptor::getCustomMenu(const Json::Value & input, Json::Value & output)
{
    ITEM_ID itemID = input[JSON_ITEM_ID].asLargestUInt();
    if (itemID < CUSTOM_MENU_COUNT)
    {
        (this->*customMenu[itemID])(input, output);
    }
    else
    {
        // Invalid Request type; return immediately with error!!
        LOGF_ERROR(CPMAPP, "Invalid Item ID" << itemID);
        
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Get Device Menu information
*/
void FFAdaptor::getDeviceMenu(const Json::Value & input, Json::Value & output)
{
    (void)input; // To remove Warning
    getTopLevelMenu(output);
}

/*  ----------------------------------------------------------------------------
    Get Advanced Device Menu information
    Authored By: Utthunga
*/
void FFAdaptor::getAdvancedDeviceMenu(const Json::Value& input,
                                      Json::Value& output)
{
    PMStatus status;
    ITEM_ID blockID = INVALID_ID_OR_HANDLE;
    BLOCK_HANDLE blockHandle = INVALID_ID_OR_HANDLE;

    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;

    // To Remove Clang Warning
    (void)input;
	
	// Get the Device information from connection manager
    const DeviceInfo & deviceInfo = m_pFFConnectionManager->getDeviceInfo();

    // Get the lsit of all the blocks in the device
    for (int index = 0; index < deviceInfo.blk_count; ++index)
    {
        blockHandle = m_pDDSController->getBlockHandleByBlockTag(deviceInfo.dd_tag[index]);
        if (blockHandle < 0)
        {
            LOGF_DEBUG(CPMAPP, "Invalid block handle");
            continue;
        }

        status = m_pDDSController->getBlockIDByBlockHandle(blockHandle, &blockID);
        if (status.hasError())
        {
            LOGF_DEBUG(CPMAPP, "Get Block ID Failed with Error : " << status.getErrorCode());
            continue;
        }
        
        // Fill the Block information as menu items - Item Info
        sMenu.itemLabel = deviceInfo.blk_tag[index];
        sMenu.itemID    = blockID ;
        sMenu.itemType  = JSON_BLOCK_ITEM_TYPE;
        sMenu.itemInDD  = TRUE;
        sMenu.itemContainerTag = deviceInfo.dd_tag[index];

        itemList.push_back(sMenu);
    }
    // Set the attributs of the Menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.help;
    sMenu.itemContainerTag = "";

    // Now prepare the Json for the menu
    getMenuAsJson(sMenu, itemList, output);
}

/*  ----------------------------------------------------------------------------
    Get Block Level Menu information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockMenu(const Json::Value& input, Json::Value& output)
{
    DDI_ITEM_SPECIFIER   itemSpecifier{};
    DDI_GENERIC_ITEM     genericItem{};
    DDI_BLOCK_SPECIFIER  blockSpecifier{};

    ENV_INFO envInfo{};
    APP_INFO appInfo{};
    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;
    std::vector<ITEM_ID> menuList;
    std::vector<ITEM_ID> rootMenuList;
    std::string blockTag;
    PMStatus status;
    
    envInfo.app_info = &appInfo;

    // Validate & retrieve the block container tag
    if (FAILURE == validateItemContTag(input, blockTag))
    {
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    // Fill the Block Specifier Attributes
    blockSpecifier.type = DDI_BLOCK_TAG;
    blockSpecifier.block.tag = const_cast<char*>(blockTag.c_str());

    // Fill the item Specifier information
    itemSpecifier.type = DDI_ITEM_BLOCK;

    // Get the block information from the DDS
    status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier,
                                          &envInfo, ALL_ITEM_ATTR_MASK, &genericItem);
    if (genericItem.item != NULL)
    {
    	if (!status.hasError())
		{
			FLAT_BLOCK *pBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
			if (nullptr != pBlock)
			{
				for (int index = 0; index < pBlock->menu.count; ++index)
				{
					// Fetch menu ID from pBlock->menu.list and compare with symbol table menu entries
					// Look for word Menu_Top, if available ddiGetItem for that particular menu ID
					// Else display all menus under the block
					std::string blockName = fetchMenuNameFromMap(pBlock->menu.list[index]);

					// Check for Root Menu magic string in suffix
					if (!blockName.empty() && blockName.find(HH_MAGIC_STRING_1) != string::npos)
					{
						// Root menu is available inside the block
						rootMenuList.push_back(pBlock->menu.list[index]);
					}
					// Check for root menu magic string in prefix
					else if (!blockName.empty() && blockName.find(HH_MAGIC_STRING_2) != string::npos)
					{
						// Root menu is available inside the block
						rootMenuList.push_back(pBlock->menu.list[index]);
					}
					else
					{
						// Push the menu item ID into the menuList vector
						menuList.push_back(pBlock->menu.list[index]);
					}
				}

				// Check whether root menu is available in the block
				if (!rootMenuList.empty())
				{
					menuList.clear();
					menuList = rootMenuList;
                                        LOGF_INFO(CPMAPP, "Root menu is available inside the block");
				}
				else
				{
                                        LOGF_ERROR(CPMAPP, "No root menu available inside the block");
				}

				for(auto index : menuList)
				{
					// One more generic item info structure is needed for getting menu information
					DDI_GENERIC_ITEM genericMenuItem{};

					// Fill the Item Specifier information
					itemSpecifier.type = DDI_ITEM_ID;
					itemSpecifier.item.id = index;

					status = m_pDDSController->ddiGetItem(&blockSpecifier, &itemSpecifier,
														  &envInfo, ALL_ITEM_ATTR_MASK, &genericMenuItem);
					if (!status.hasError())
					{
						FLAT_MENU * pMenu = static_cast<FLAT_MENU*>(genericMenuItem.item);
						if (nullptr != pMenu)
						{
							if (nullptr != pMenu->label.str)
							{
								sMenu.itemLabel = validateAndGetStringValue(pMenu->label.str, pMenu->label.len);
							}
							sMenu.itemID    = index;
							sMenu.itemType  = JSON_MENU_ITEM_TYPE;
							sMenu.itemInDD  = TRUE;
							sMenu.itemContainerTag = blockTag;

							itemList.push_back(sMenu);
						}
					}
					else
					{
                        LOGF_ERROR(CPMAPP, "DDI Get Item Failed with Error : " << status.getErrorCode());
						status.clear();
					}

					// Clear the generic item memory allocated by ddiGetItem API
					status = m_pDDSController->ddiCleanItem(&genericMenuItem);
					if (status.hasError())
					{
                        LOGF_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
						status.clear();
					}
				}
			}
		}
		else
		{
            LOGF_ERROR(CPMAPP, "Block Not Found" << status.getErrorCode());
			status.clear();
		}

        // Add the Advanced option as a menu item to the Block Level Menu
        sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.label;
        sMenu.itemID    = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.id;
        sMenu.itemType  = JSON_MENU_ITEM_TYPE;
        sMenu.itemInDD  = FALSE;
        sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.help;
        sMenu.itemContainerTag = blockTag;

        itemList.push_back(sMenu);

        // Set the attributs of the menu
        sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_MENU.label;
        sMenu.itemID    = CustomMenu::info.CUSTOM_BLOCK_MENU.id;
        sMenu.itemType  = JSON_MENU_ITEM_TYPE;
        sMenu.itemInDD  = FALSE;
        sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_MENU.help;
        sMenu.itemContainerTag = blockTag;

        // Now prepare the Json for top level menu
        getMenuAsJson(sMenu, itemList, output);

		// Clear the generic item memory allocated by ddiGetItem API
		status = m_pDDSController->ddiCleanItem(&genericItem);
		if (status.hasError())
		{
            LOGF_ERROR(CPMAPP, "DDI Clean Item Failed with Error : " << status.getErrorCode());
		}
    }
	else
	{
        LOGF_ERROR(CPMAPP, "No Item in Block");
        Error::Entry error = Error::cpm.NO_BLOCK_ITEM_FOUND;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Get Advaced Block Menu information
    Authored By: Utthunga
*/
void FFAdaptor::getAdvancedBlockMenu(const Json::Value& input,
                                     Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);

    // Add the inidividual menu items

    // Add the Block Parameter List option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.help;
    itemList.push_back(sMenu);

    // Add the Block Views menu option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_VIEWS.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_VIEWS.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_VIEWS.help;
    itemList.push_back(sMenu);

    // Add the Block Methods menu option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_METHODS.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_METHODS.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_METHODS.help;
    itemList.push_back(sMenu);

    // Add the Block Configuration Menu option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_CONFIG.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_CONFIG.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_CONFIG.help;
    itemList.push_back(sMenu);

    // Add the Block Alarms Menu option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_ALARMS.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_ALARMS.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_ALARMS.help;
    itemList.push_back(sMenu);

    // Add the Block Status Menu option
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_STATUS.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_STATUS.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_STATUS.help;
    itemList.push_back(sMenu);

    // Set the attributs of the menu
    sMenu.itemID    	= CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.id;
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.help;

    // Now prepare the Json for top level menu
    getMenuAsJson(sMenu, itemList, output);
}

/*  ----------------------------------------------------------------------------
    Get Block Parameter List Menu Information
*/
void FFAdaptor::getBlockParamListMenu(const Json::Value& input,
                                      Json::Value& output)
{
    ItemBasicInfo sMenu;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get Block Param List
    getBlockParams(sMenu.itemContainerTag, output);
}

/*  ----------------------------------------------------------------------------
    Get Block Views Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockViewsMenu(const Json::Value& input,
                                  Json::Value& output)
{
    ItemBasicInfo sMenu;
    PMStatus status;
    DDI_BLOCK_SPECIFIER blockSpec{};
    ENV_INFO            envInfo{};
    FLAT_BLOCK*         flatBlock = nullptr;
    DDI_GENERIC_ITEM    genericItem{};
    DDI_ITEM_SPECIFIER  itemSpec{};
    APP_INFO            appInfo;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  	= CustomMenu::info.CUSTOM_BLOCK_VIEWS.label;
    sMenu.itemHelp  	= CustomMenu::info.CUSTOM_BLOCK_VIEWS.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    //gives block handle
    envInfo.block_handle = m_pDDSController->getBlockHandleByBlockTag(sMenu.itemContainerTag.c_str());
    if (envInfo.block_handle >= 0)
    {
        envInfo.app_info = &appInfo;
        appInfo.status_info = USE_EDIT_VALUE;

        blockSpec.type = DDI_BLOCK_HANDLE;
        blockSpec.block.handle = envInfo.block_handle;

        // Gets the requested block item.
        status = m_pDDSController->ddiGetItem(&blockSpec, &itemSpec, &envInfo,
                                              ALL_ITEM_ATTR_MASK, &genericItem);
        if (!status.hasError())
        {
            // Typecasted generic item to flat block.
            flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
            if (nullptr == flatBlock)
            {
                status.setError(FF_ADAPTOR_ERR_GET_ADVANCED_BLOCK_VIEW_FAILED,
                                PM_ERR_CL_FF_ADAPTOR);
                Error::Entry error = Error::cpm.GET_MENU_FAILED;
                output = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
    }
    else
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    if (!status.hasError())
    {
        MEMBER* memList = flatBlock->param_list.list;
        int paramCount = flatBlock->param_list.count;
        int staticMenuIndex = CustomMenu::info.CUSTOM_BLOCK_VIEW1.id;

        for (int index = 1; index <= paramCount; index++, memList++)
        {
            DDI_GENERIC_ITEM listGenericItem{};

            envInfo.app_info = &appInfo;
            appInfo.status_info = USE_EDIT_VALUE;

            blockSpec.type = DDI_BLOCK_HANDLE;
            blockSpec.block.handle = envInfo.block_handle;

            itemSpec.type = DDI_ITEM_ID;
            itemSpec.item.id = memList->ref.id;

            // Gets the requested block item.
            status = m_pDDSController->ddiGetItem(&blockSpec, &itemSpec, &envInfo,
                                                  ALL_ITEM_ATTR_MASK, &listGenericItem);

            if (!status.hasError())
            {
                FLAT_VAR_LIST* varList = static_cast<FLAT_VAR_LIST*>(listGenericItem.item);
                if (nullptr != varList)
                {
                    ItemBasicInfo itemBasicInfo{};
                    Json::Value params;

                    itemBasicInfo.itemLabel = validateAndGetStringValue(varList->label.str, varList->label.len);
                    itemBasicInfo.itemHelp = validateAndGetStringValue(varList->help.str, varList->help.len);
                    itemBasicInfo.itemID = staticMenuIndex++;
                    itemBasicInfo.itemContainerTag = sMenu.itemContainerTag;
                    itemBasicInfo.itemType = JSON_MENU_ITEM_TYPE;
                    itemBasicInfo.itemInDD = false;

                    //fills each view information
                    fillItemBasicJsonInfo(itemBasicInfo, params);

                    //appends each view information to output json
                    output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(params);
                }
            }

            //cleans generic item
            m_pDDSController->ddiCleanItem(&listGenericItem);
        }
    }

    //cleans generic item
    m_pDDSController->ddiCleanItem(&genericItem);
}

/*  ----------------------------------------------------------------------------
    Get Block Methods Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockMethodsMenu(const Json::Value& input,
                                    Json::Value& output)
{
    ItemBasicInfo sMenu{};
    PMStatus status;
    std::list<ItemBasicInfo> itemList;

    if (!input.isNull())
    {
        // Get the Block tag
        std::string itemContainerTag;
        if (FAILURE == validateItemContTag(input, itemContainerTag))
        {
            LOGF_ERROR(CPMAPP, "Invalid item container Tag");
            Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
	        output = JsonRpcUtil::setErrorObject(error.code, error.message);
            return;
        }

        // Fill the standard atributes for the menu
        sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_METHODS.label;
        sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_METHODS.help;
        sMenu.itemID    = CustomMenu::info.CUSTOM_BLOCK_METHODS.id;
        sMenu.itemType  = JSON_MENU_ITEM_TYPE;
        sMenu.itemInDD  = FALSE;
        sMenu.itemContainerTag = itemContainerTag;

        ITEM_ID blkId = -1;
        BLOCK_HANDLE blockHandle = m_pDDSController->getBlockHandleByBlockTag(
                                   itemContainerTag.c_str());
        if (blockHandle < 0)
        {
            LOGF_ERROR(CPMAPP, "Failed to get block handle");
            Error::Entry error = Error::cpm.GET_MENU_FAILED;
            output = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
        else
        {
            status = m_pDDSController->getBlockIDByBlockHandle(blockHandle, &blkId);
            if (status.hasError())
            {
                LOGF_ERROR(CPMAPP, "Failed to get block id");
                Error::Entry error = Error::cpm.GET_MENU_FAILED;
                output = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
            else
            {
                uint64_t attrMask = ALL_ITEM_ATTR_MASK;
                FLAT_BLOCK* flatBlock = nullptr;
                DDI_ITEM_SPECIFIER itemSpec{};
                APP_INFO appInfo{};
                ENV_INFO envInfo{};
                DDI_GENERIC_ITEM genericItem{};
                DDI_BLOCK_SPECIFIER blockSpec{};

                envInfo.app_info = &appInfo;
                appInfo.status_info = USE_EDIT_VALUE;

                // This API gives block handle
                envInfo.block_handle = m_pDDSController->getBlockHandleByBlockTag(
                                       itemContainerTag.c_str());

                if (envInfo.block_handle >= 0)
                {
                    blockSpec.type = DDI_BLOCK_HANDLE;
                    blockSpec.block.handle = envInfo.block_handle;

                    // Gets the requested item.
                    status = m_pDDSController->ddiGetItem(
                                &blockSpec, &itemSpec, &envInfo, attrMask, &genericItem);

                    if (!status.hasError())
                    {
                        // Typecasted generic item to flat block.
                        flatBlock = static_cast<FLAT_BLOCK*>(genericItem.item);
                        for (int index = 0; index < flatBlock->method.count; index++)
                        {
                            ITEM_ID itemID = flatBlock->method.list[index];

                            DDI_ITEM_SPECIFIER   itemSpecifier{};
                            DDI_GENERIC_ITEM     genericMethodItem{};

                            // Fill the item Specifier information
                            itemSpecifier.type = DDI_ITEM_ID;
                            itemSpecifier.item.id = itemID;

                            // Get the block information from the DDS
                            status = m_pDDSController->ddiGetItem(&blockSpec, &itemSpecifier,
                                                                  &envInfo, ALL_ITEM_ATTR_MASK, &genericMethodItem);

                            if (!status.hasError())
                            {
                                FLAT_METHOD *pMethod = static_cast<FLAT_METHOD*>(genericMethodItem.item);
                                if (nullptr != pMethod)
                                {
                                    ItemBasicInfo sMethod;
                                    sMethod.itemLabel = validateAndGetStringValue(pMethod->label.str, pMethod->label.len);
                                    sMethod.itemID    = pMethod->id;
                                    sMethod.itemType  = JSON_METHOD_ITEM_TYPE;
                                    sMethod.itemInDD  = TRUE;
                                    sMethod.itemHelp  = validateAndGetStringValue(pMethod->help.str, pMethod->help.len);
                                    sMethod.itemContainerTag = itemContainerTag;
                                    itemList.push_back(sMethod);
                                }
                            }

                            // Clear the Generic Item memory allocated by ddiGetItem API
                            m_pDDSController->ddiCleanItem(&genericMethodItem);
                        }

                        // Now prepare the Json for top level menu
                        getMenuAsJson(sMenu, itemList, output);
                    }

                    // Clear the Generic Item memory allocated by ddiGetItem API
                    m_pDDSController->ddiCleanItem(&genericItem);
                }
            }
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Json request");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Get Block Configuration Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockConfigurationMenu(const Json::Value& input,
                                          Json::Value& output)
{
    ItemBasicInfo sMenu;
    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_CONFIG.label;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_CONFIG.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    PMStatus status;

    ParamInfo paramInfo{};
    paramInfo.blockTag.str = const_cast<char*>(
                sMenu.itemContainerTag.c_str());

    // Add variable type elements here
    ITEM_ID varItemIDs[]{
        CHANNEL_ID,
                L_TYPE_ID};

    // Add record type elements here
    ITEM_ID recordItemIDs[]{
        MODE_BLOCK_ID,
                XD_SCALE_ID,
                OUT_SCALE_ID};

    // Get no of elements in varItemIDs array
    int varMaxSize = sizeof(varItemIDs) / sizeof(ITEM_ID);
    VariableType varParam[varMaxSize];
    int index = 0;

    // Add variable type items to the paramInfo list
    for (auto& itemID: varItemIDs)
    {
        varParam[index].itemID = itemID;
        paramInfo.paramList.push_back(varParam[index]);
        index++;
    }

    // Get no of elements in recordItemIDs array
    int recMaxSize = sizeof(recordItemIDs) / sizeof(ITEM_ID);
    RecordType recordParam[recMaxSize];
    index = 0;

    // Add record type items to the paramInfo list
    for (auto& itemID: recordItemIDs)
    {
        recordParam[index].itemID = itemID;
        paramInfo.paramList.push_back(recordParam[index]);
        index++;
    }

    // Get the requested item info from block
    status = m_pFFParameterCache->getParameter(&paramInfo);

    if (!status.hasError())
    {
        ParamInfo validParamInfo{};
        validParamInfo.blockTag.str = const_cast<char*>(
                    sMenu.itemContainerTag.c_str());

        for (auto &iteminfo : paramInfo.paramList)
        {
            auto paramItem = &iteminfo.get();

            /* All DDs may not support all the requested parameters above.
               Hence, send only valid parameters to UI */
            if (DDI_INVALID_PARAM != paramItem->response_code)
            {
                validParamInfo.paramList.push_back(iteminfo);
            }
        }

        ParamInfo charParamInfo{};
        charParamInfo.blockTag.str = const_cast<char*>(
                    sMenu.itemContainerTag.c_str());

        // Get Block characteristics to get the Block Tag info
        status = m_pFFParameterCache->getBlockCharacteristics(&charParamInfo,
                                                              DEFAULT_PARAMS);

        if (!status.hasError())
        {
            ItemInfoType& itemInfo = charParamInfo.paramList.at(0);
            validParamInfo.paramList.push_back(itemInfo);

            // Get requested Block Param List as JSON data
            fillBlockParamsJson(&validParamInfo, output);
        }

        // Clear the param list
        m_pFFParameterCache->cleanParamInfo(&charParamInfo);

    }

    // Clear the param list
    m_pFFParameterCache->cleanParamInfo(&paramInfo);

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/* ----------------------------------------------------------------------------
    Get Block Alarms Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockAlarmsMenu(const Json::Value& input,
                                   Json::Value& output)
{
    ItemBasicInfo sMenu;
    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_ALARMS.label;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_ALARMS.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    PMStatus status;

    // Allocate memory for ParamInfo structure
    ParamInfo paramInfo{};
    // assign the block tag to ParamInfo structure
    paramInfo.blockTag.str = const_cast<char*>(
                sMenu.itemContainerTag.c_str());

    // adding the standard alarm parameters as per FF Spec AN-014.pdf
    // Add VARIABLE TYPE type elements here
    ITEM_ID varItemIDs[]{ SET_FSTATE, FAULT_STATE, CLR_FSTATE, MAX_NOTIFY,
        LIM_NOTIFY, CONFIRM_TIME, ACK_OPTION_RES2, ACK_OPTION, ACK_OPTION,
        ACK_OPTION_RES, WRITE_PRI, FEATURES, FEATURES_SEL, FD_CHECK_ACTIVE,
        FD_CHECK_MASK, FD_CHECK_PRI, FD_CHECK_MAP, FD_EXTENDED_ACTIVE_n,
        FD_EXTENDED_MAP_n, FD_FAIL_ACTIVE, FD_FAIL_MAP, FD_FAIL_PRI,
        FD_FAIL_MASK, FD_OFFSPEC_MAP, FD_OFFSPEC_MASK, FD_OFFSPEC_PRI,
        FD_OFFSPEC_ACTIVE, FD_MAINT_ACTIVE, FD_MAINT_MAP, FD_MAINT_MASK,
        FD_MAINT_PRI, FD_RECOMMEND_ACT, FD_VER, HI_HI_PRI, HI_HI_LIM,
        HI_PRI, HI_LIM, LO_LO_PRI, LO_LO_LIM, LO_PRI, LO_LIM, DV_LO_PRI,
        DV_LO_LIM, DV_HI_PRI, DV_HI_LIM };

    ITEM_ID recordItemIDs[]{ BLOCK_ALM, ALARM_SUM_RES2, ALARM_SUM, ALARM_SUM,
        ALARM_SUM_RES, UPDATE_EVT, FD_CHECK_ALM, FD_FAIL_ALM, FD_OFFSPEC_ALM,
        FD_MAINT_ALM, FD_SIMULATE, HI_HI_ALM, HI_ALM, LO_LO_ALM, LO_ALM,
        DV_LO_ALM, DV_HI_ALM };

    int varMaxSize = sizeof(varItemIDs) / sizeof(ITEM_ID);
    VariableType varParam[varMaxSize];
    int index = 0;

    // Add variable type items to the paramInfo list
    for (auto& itemID: varItemIDs)
    {
        varParam[index].itemID = itemID;
        paramInfo.paramList.push_back(varParam[index]);
        index++;
    }

    // Get no of elements in recordItemIDs array
    int recMaxSize = sizeof(recordItemIDs) / sizeof(ITEM_ID);
    RecordType recordParam[recMaxSize];
    index = 0;

    // Add record type items to the paramInfo list
    for (auto& itemID: recordItemIDs)
    {
        recordParam[index].itemID = itemID;
        paramInfo.paramList.push_back(recordParam[index]);
        index++;
    }

    // Retrieves the requested item information from the specified block.
    status = m_pFFParameterCache->getParameter(&paramInfo);

    if (!status.hasError())
    {
        ParamInfo validParamInfo{};
        validParamInfo.blockTag.str = const_cast<char*>(
                    sMenu.itemContainerTag.c_str());

        for (auto &iteminfo : paramInfo.paramList)
        {
            auto paramItem = &iteminfo.get();
            /* All DDs may not support all the requested parameters.
               Hence, sending only valid parameters to UI.*/
            if (DDI_INVALID_PARAM != paramItem->response_code)
            {
                validParamInfo.paramList.push_back(iteminfo);
            }
        }

        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&validParamInfo, output);
    }

    // paramlist is stack memory just clear the list
    
    m_pFFParameterCache->cleanParamInfo(&paramInfo);

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/* ----------------------------------------------------------------------------
    Get Block Status Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockStatusMenu(const Json::Value& input,
                                   Json::Value& output)
{

    ItemBasicInfo sMenu;
    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_STATUS.label;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_STATUS.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    PMStatus status;

    // Allocate memory for ParamInfo Structure
    ParamInfo paramInfo{};
    // assign the block tag to ParamInfo structure
    paramInfo.blockTag.str = const_cast<char*>(
                sMenu.itemContainerTag.c_str());

    /* add block error parameter which contains the 16 block
       error condition
    */
    VariableType varParam{BLOCK_ERROR};
    paramInfo.paramList.push_back(varParam);

    // Retrieves the requested item information from the specified block
    status = m_pFFParameterCache->getParameter(&paramInfo);

    if (!status.hasError())
    {
        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&paramInfo, output);
    }

    // Clear the param list
    paramInfo.blockTag.str = nullptr;
    paramInfo.paramList.clear();

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Get Block View 1 Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockView1Menu(const Json::Value& input,
                                  Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::string itemContainerTag{};
    ParamInfo paramInfo{};
    PMStatus status;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  = CustomMenu::info.CUSTOM_BLOCK_VIEW1.label;
    sMenu.itemHelp   = CustomMenu::info.CUSTOM_BLOCK_VIEW1.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get the Block tag
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    paramInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());
    paramInfo.requestInfo.maskType = MaskType::ITEM_ATTR_MIN_MASK;

    // retrieves parameters from block view based on view index
    status = m_pFFParameterCache->getParameterFromBlockView(VIEW_1, &paramInfo);
    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&paramInfo, output);
    }

    // Clear the param list
    m_pFFParameterCache->cleanParamInfo(&paramInfo);
}

/* ----------------------------------------------------------------------------
    Get Block View 2 Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockView2Menu(const Json::Value& input,
                                  Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::string itemContainerTag{};
    ParamInfo paramInfo{};
    PMStatus status;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  = CustomMenu::info.CUSTOM_BLOCK_VIEW2.label;
    sMenu.itemHelp   = CustomMenu::info.CUSTOM_BLOCK_VIEW2.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get the Block tag
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    paramInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());
    paramInfo.requestInfo.maskType = MaskType::ITEM_ATTR_MIN_MASK;

    // retrieves parameters from block view based on view index
    status = m_pFFParameterCache->getParameterFromBlockView(VIEW_2, &paramInfo);
    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&paramInfo, output);
    }

    // Clear the param list
    m_pFFParameterCache->cleanParamInfo(&paramInfo);

}

/* ----------------------------------------------------------------------------
   Get Block View 3 Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockView3Menu(const Json::Value& input,
                                  Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::string itemContainerTag{};
    ParamInfo paramInfo{};
    PMStatus status;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  = CustomMenu::info.CUSTOM_BLOCK_VIEW3.label;
    sMenu.itemHelp   = CustomMenu::info.CUSTOM_BLOCK_VIEW3.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get the Block tag
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    paramInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());
    paramInfo.requestInfo.maskType = MaskType::ITEM_ATTR_MIN_MASK;

    // retrieves parameters from block view based on view index
    status = m_pFFParameterCache->getParameterFromBlockView(VIEW_3, &paramInfo);
    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&paramInfo, output);
    }

    // Clear the param list
    m_pFFParameterCache->cleanParamInfo(&paramInfo);
}

/*  ----------------------------------------------------------------------------
    Get Block View 4 Menu Information
    Authored By: Utthunga
*/
void FFAdaptor::getBlockView4Menu(const Json::Value& input,
                                  Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::string itemContainerTag{};
    ParamInfo paramInfo{};
    PMStatus status;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel  = CustomMenu::info.CUSTOM_BLOCK_VIEW4.label;
    sMenu.itemHelp   = CustomMenu::info.CUSTOM_BLOCK_VIEW4.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get the Block tag
    if (FAILURE == validateItemContTag(input, itemContainerTag))
    {
        LOGF_ERROR(CPMAPP, "Invalid item container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }

    paramInfo.blockTag.str = const_cast<char*>(itemContainerTag.c_str());
    paramInfo.requestInfo.maskType = MaskType::ITEM_ATTR_MIN_MASK;

    // retrieves parameters from block view based on view index
    status = m_pFFParameterCache->getParameterFromBlockView(VIEW_4, &paramInfo);
    if (status.hasError())
    {
        Error::Entry error = Error::cpm.GET_MENU_FAILED;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        // Get requested Block Param List as JSON data
        fillBlockParamsJson(&paramInfo, output);
    }

    // Clear the param list
    m_pFFParameterCache->cleanParamInfo(&paramInfo);
}

/*  ---------------------------------------------------------------------------
    Fills all the required Json attributes for Custom Menu's
*/
void FFAdaptor::fillBasicMenuAttributes(const Json::Value& input,
                                        ItemBasicInfo& output)
{
    // Initialise all the common attributes for Menu
    output.itemType 		= input[JSON_ITEM_TYPE].asString();
    output.itemInDD 		= input[JSON_ITEM_IN_DD].asBool();
    output.itemID   		= input[JSON_ITEM_ID].asLargestUInt();
    output.itemContainerTag = input[JSON_ITEM_CONT_TAG].asString();
}

/* SET ITEM METHODS ***********************************************************/

/*  ----------------------------------------------------------------------------
    Sets value into a device variable parameter
*/
Json::Value FFAdaptor::setVariable
(
    ParamItems& varItems,
    Json::Value& itemInfo
)
{
    Json::Value setVarResp = Json::Value("ok");
    PMStatus status;

    ParamInfo setParamInfo{};
    setParamInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
    setParamInfo.requestInfo.maskType = varItems.paramMaskType;
    VariableType writeVar{};

    ITEM_TYPE itemType = m_pDDSController->getItemType(varItems.paramItemID,
                                                       varItems.paramContainerTag);

    switch (itemType)
    {
        case VARIABLE_ITYPE:
        {
            writeVar.itemID = varItems.paramItemID;
            writeVar.subIndex = varItems.paramSubIndex;
            writeVar.containerId = varItems.paramContainerID;
            writeVar.itemContainerTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());

            // For a single BITENUMERATTION_ITEM type
            writeVar.bitIndex = varItems.paramBitIndex;

            setParamInfo.paramList.push_back(writeVar);

            if (varItems.paramItemID == BLOCK_TAG_ID)
            {
                status = m_pFFParameterCache->getBlockCharacteristics(&setParamInfo,
                                                                      DEFAULT_PARAMS);
            }
            else
            {
                // Call getParameter and retrieve parameter information
                status = m_pFFParameterCache->getParameter(&setParamInfo);
            }
        }
        break;

        case RECORD_ITYPE:
        {
            ParamInfo recParamInfo{};
            recParamInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
            recParamInfo.requestInfo.maskType = varItems.paramMaskType;

            if (varItems.paramItemID == BLOCK_TAG_ID)
            {
                status = m_pFFParameterCache->getBlockCharacteristics(&recParamInfo,
                                                                      DEFAULT_PARAMS);

                if (!status.hasError())
                {
                    /* In Characteristics record, first element is Block Tag, so
                       no need to query for it */
                    ItemInfoType& blockItem = recParamInfo.paramList.at(0);
                    writeVar = dynamic_cast<VariableType&>(blockItem);
                    setParamInfo.paramList.push_back(writeVar);
                }
            }
            else
            {
                RecordType recType{varItems.paramContainerID};
                recType.subIndex = varItems.paramSubIndex;
                recParamInfo.paramList.push_back(recType);

                // Call getParameter and retrieve parameter information
                status = m_pFFParameterCache->getParameter(&recParamInfo);

                if (!status.hasError())
                {
                    ItemInfoType& recType = recParamInfo.paramList.at(0);
                    RecordType& recItem = dynamic_cast<RecordType&>(recType);

                    ItemInfoType& item = recItem.members.at(0);
                    writeVar = dynamic_cast<VariableType&>(item);
                    setParamInfo.paramList.push_back(writeVar);
                }
            }
        }
        break;

        case ARRAY_ITYPE:
        {
            ParamInfo arrParamInfo{};
            arrParamInfo.blockTag.str = const_cast<char*>(varItems.paramContainerTag.c_str());
            arrParamInfo.requestInfo.maskType = varItems.paramMaskType;

            ArrayType arrType{varItems.paramContainerID};
            arrParamInfo.paramList.push_back(arrType);

            // Call getParameter and retrieve parameter information
            status = m_pFFParameterCache->getParameter(&arrParamInfo);

            if (!status.hasError())
            {
                ItemInfoType& arrType = arrParamInfo.paramList.at(0);
                ArrayType& arrItem = dynamic_cast<ArrayType&>(arrType);

                ItemInfoType& item = arrItem.members.at(0);
                writeVar = dynamic_cast<VariableType&>(item);
                setParamInfo.paramList.push_back(writeVar);
            }
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Item type not supported!!" << itemType);
        }
        break;
    }

    if (!status.hasError())
    {
        if (READ_HANDLING != writeVar.misc.handling)
        {
            // Read the value from the cache to compare with new value
            m_pFFParameterCache->readValue(&setParamInfo);

            // Set the user modified value into paramInfo structure
            status = setVariableValue(itemInfo, writeVar);

            if (!status.hasError())
            {
                // Write the value to the device and parameter cache
                status = m_pFFParameterCache->writeValue(&setParamInfo);
                if (status.hasError())
                {
                    // Reset the previous value when device rejects the value to write
                    m_pFFParameterCache->readValue(&setParamInfo);
                    m_pFFParameterCache->writeValue(&setParamInfo);

                    // Writing value to device falied
                    LOGF_ERROR(CPMAPP, "Writing Value to device failed");
                    Error::Entry error = Error::cpm.SET_ITEM_FAILED;
                    setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
                }
                else
                {
                    // TODO(Bharath): Intimate UI upon changing the Block Tag
                    if (varItems.paramItemID == BLOCK_TAG_ID)
                    {
						// Get the Device information from connection manager
						const DeviceInfo & deviceInfo = m_pFFConnectionManager->getDeviceInfo();

                        // Update block tag in block list
                        for (int index = 0; index < deviceInfo.blk_count; ++index)
                        {
                            if (strcmp(deviceInfo.dd_tag[index], varItems.paramContainerTag.c_str()) == 0)
                            {
                                strcpy(deviceInfo.blk_tag[index], writeVar.value.strData.str);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                // invalid user input
                LOGF_ERROR(CPMAPP, "Invalid input for for variable");
                Error::Entry error = Error::cpm.INVALID_VARIABLE_VALUE;
                setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
            }
        }
        
        else
        {
            LOGF_ERROR(CPMAPP, "Parameter is readonly!, cannot be written");
            Error::Entry error = Error::cpm.PARAM_READONLY;
            setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        // Getting parameter information failed
        Error::Entry error = Error::cpm.SET_ITEM_FAILED;
        setVarResp = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Delete the dynamically allocated memory from parameter cache
    m_pFFParameterCache->cleanParamInfo(&setParamInfo);

    return setVarResp;
}

/*  ----------------------------------------------------------------------------
    Sets variable value into parameter info structure
*/
PMStatus FFAdaptor::setVariableValue
(
    const Json::Value& itemInfo,
    VariableType& varParam 
)
{
    PMStatus status;
    int errorCode = FAILURE;

    if (itemInfo.isNull() && !(itemInfo.isMember(JSON_VAR_VALUE)))
    {
        status.setError(FF_ADAPTOR_INVALID_JSON_REQUEST, PM_ERR_CL_FF_ADAPTOR);
        return status;
    }

    switch(varParam.type)
    {
        case DDS_BOOLEAN_T:
        {
            unsigned long long tempVal = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
                try
                {
                    tempVal = itemInfo[JSON_VAR_VALUE].asLargestUInt(); 
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE, PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }
                
                if ((0 == tempVal) || (255 == tempVal))
                {
                    varParam.value.stdValue.ul = tempVal;
                    errorCode = SUCCESS;
                }
            }
        }
        break;

        // Integer types
        case DDS_INTEGER:
        {
            long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral() && Json::ValueType::intValue == itemInfo[JSON_VAR_VALUE].type())
            {
                try
                {
            	    tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();
                }
                
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }

            	// For validation of range, verified with value -62362362889
                if (validateIntegerRange<long long, long long>(tempValue))
            	{
            		errorCode = validateVariableLimit(itemInfo,varParam);
            		if (errorCode == SUCCESS)
                    {
                        varParam.value.stdValue.l = itemInfo[JSON_VAR_VALUE].asLargestInt();
                    }
            	}
            }
        }
        break;

        // unsigned Integer and enumeration types
        case DDS_UNSIGNED:      //FALLTHROUGH
        case DDS_ENUMERATED:    //FALLTHROUGH
        case DDS_MAX_TYPE:      //FALLTHROUGH
        case DDS_ACK_TYPE:
        {
            unsigned long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
				/* converting a negative value to largest UInt may crash the application
				* handle the exception and return corresponding error
				*/
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }

            	if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
            	{
            		errorCode = validateVariableLimit(itemInfo,varParam);

            		if (errorCode == SUCCESS)
                    {
            			varParam.value.stdValue.ul = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                    }
            	}
            }
        }
        break;

        case DDS_BIT_ENUMERATED:
		{
            // BIT_ENUMERATION_ITEM shall accept only boolean values
            if (varParam.bitIndex > 0)
            {
                bool tempVal = false;
                if (itemInfo[JSON_VAR_VALUE].isBool())
                {
                    try
                    {
                        tempVal = static_cast<bool>(itemInfo[JSON_VAR_VALUE].asBool()); 
                    }
                    catch(...)
                    {
                        status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE, PM_ERR_CL_FF_ADAPTOR);
                        return status;
                    }
                    
                    if ((0 == tempVal) || (1 == tempVal))
                    {
                        // Set the modified BIT_ENUMERATION value
                        setBitEnumItemVal(varParam.value.stdValue.ul, tempVal, varParam.bitIndex);
                        errorCode = SUCCESS;
                    }
                }
            }

            // BIT_ENUMERATION shall accept only unsigned integer values
            else
            {
                unsigned long long tempValue = 0;
                if (itemInfo[JSON_VAR_VALUE].isIntegral())
                {
                    /* converting a negative value to largest UInt may crash the application
                    * handle the exception and return corresponding error
                    */
                    try
                    {
                        // For validation of range, verified with value 712376526151635
                        tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                    }
                    catch(...)
                    {
                        status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE, PM_ERR_CL_FF_ADAPTOR);
                        return status;
                    }

                    if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                    {
                        errorCode = validateVariableLimit(itemInfo,varParam);

                        if (errorCode == SUCCESS)
                        {
                            varParam.value.stdValue.ul = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                        }
                    }
                }
            }
		}
        break;

        case DDS_FLOAT:
        {
            if (itemInfo[JSON_VAR_VALUE].isNumeric() && Json::ValueType::realValue == itemInfo[JSON_VAR_VALUE].type())
            {
                // For validation of range, verified with value 632732323652361251236156125126351236.3843282643643;
                double tempValue = itemInfo[JSON_VAR_VALUE].asDouble();

                if ((tempValue >= std::numeric_limits<float>::lowest())
            	    	&& (tempValue <= std::numeric_limits<float>::max()))
                {
            	    varParam.value.stdValue.f = itemInfo[JSON_VAR_VALUE].asFloat();
                    errorCode = SUCCESS;
                }
            }
        }
        break;

        case DDS_DOUBLE:
        {
            if (itemInfo[JSON_VAR_VALUE].isIntegral() && Json::ValueType::realValue == itemInfo[JSON_VAR_VALUE].type())
            {
                // no validation required for double variable
                varParam.value.stdValue.d = itemInfo[JSON_VAR_VALUE].asDouble();
                errorCode = SUCCESS;
            }
        }
        break;

        // String types
        case DDS_ASCII:         //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        {
            if (itemInfo[JSON_VAR_VALUE].isString())
            {
                std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                // validation:check if string is NULL
                if (!tempStr.empty())
                {
                    // Convert UTF8 string to ISO-Latin-1 before writing the value to device
                    size_t utfStrLen = tempStr.length();
                    char* utf8Str = (char *) calloc ((utfStrLen + 1), sizeof(char));
                    if (nullptr == utf8Str)        
                    {
                        LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                        break;
                    }

                    // Since the string from user is in utf8 format, its better to allocate twice the 
                    // memory for accomodating decoded string data
                    size_t isoLatinStrLen = (utfStrLen * 2) + 1;
                    char* isoLatinStr = (char *) calloc (isoLatinStrLen, sizeof(char));
                    if (nullptr == isoLatinStr)        
                    {
                        LOGF_ERROR (CPMAPP, "Cannot allocate memory for string conversion");
                        break;
                    }

                    strncpy (utf8Str, tempStr.c_str(), tempStr.length());

                    char* tempUtf8Str = utf8Str;
                    char* tempIsoLatinStr = isoLatinStr;
                    bool convRetVal = CPMUtil::convertUtf8ToIsolatin1 (&tempUtf8Str, &utfStrLen,     
                                                    &tempIsoLatinStr, &isoLatinStrLen);

                    if (true == convRetVal)
                    {
                        std::string isoLatinString(isoLatinStr);
                        if (isoLatinString.length() < varParam.value.strData.len)
                        {
                            int spaceCount = varParam.value.strData.len - isoLatinString.length();
                            isoLatinString.append(spaceCount, ' ');
                        }
                        if (isoLatinString.length() > varParam.value.strData.len)
                        {
                            LOGF_INFO(CPMAPP, "Can not set the string which is more than "
                                          << varParam.value.strData.len << " characters");
                        }
                        else
                        {
                            strncpy(varParam.value.strData.str, isoLatinString.c_str(), varParam.value.strData.len);
                            errorCode = SUCCESS;
                        }
                    }
                    else
                    {
                        LOGF_ERROR(CPMAPP, "Failed to convert user input utf8 string to iso-latin-1");
                    }

                    free (isoLatinStr);
                    free (utf8Str);
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Can not set Empty String: " << varParam.type);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Not a valid input for String type " << varParam.type);
            }
        }
        break;

        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_EUC:           //FALLTHROUGH
        {
            if (itemInfo[JSON_VAR_VALUE].isString())
            {
                std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                // validation:check if string is NULL
                if (!tempStr.empty())
                {
                    if (tempStr.length() < varParam.value.strData.len)
                    {
                        int spaceCount = varParam.value.strData.len - tempStr.length();
                        tempStr.append(spaceCount, ' ');
                    }
                    if (tempStr.length() > varParam.value.strData.len)
                    {
                        LOGF_INFO(CPMAPP, "Can not set the string which is more than "
                                      << varParam.value.strData.len << " characters");
                    }
                    else
                    {
                        strncpy(varParam.value.strData.str, tempStr.c_str(), varParam.value.strData.len);
                        errorCode = SUCCESS;
                    }
                }
                else
                {
                    LOGF_ERROR(CPMAPP, "Can not set Empty String: " << varParam.type);
                }
            }
            else
            {
                LOGF_ERROR(CPMAPP, "Not a valid input for String type " << varParam.type);
            }
        }
        break;

        case DDS_BITSTRING:     //FALLTHROUGH
        {
            if (itemInfo[JSON_VAR_VALUE].isString())
            {
                std::string tempStr = itemInfo[JSON_VAR_VALUE].asString();

                // validation:check if string is NULL
                if (!tempStr.empty())
                {
                    if (tempStr.length() == varParam.value.strData.len)
                    {
                        strncpy(varParam.value.strData.str, tempStr.c_str(), tempStr.length());
                        varParam.value.strData.len = tempStr.length();
                        errorCode = SUCCESS;
                    }
                }
            }
        }
        break;

        // Date and time values
        case DDS_TIME:
        {
            unsigned long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
			    /* converting a negative value to largest UInt may crash the application
				* handle the exception and return corresponding error
				*/
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    errorCode = validateVariableLimit(itemInfo,varParam);

                    if (errorCode == SUCCESS)
                    {
                        unsigned long time = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                        unsigned int milliSec = 0;

                        if (itemInfo.isMember(JSON_VAR_VAL_MILLISEC))
                        {
                            milliSec = itemInfo[JSON_VAR_VAL_MILLISEC].asUInt();
                        }

                        int* dateArray = new int[DATENTIME_ARRAY_LENGTH];
                        std::string dateString = m_pDDSController->convertsEpochTimeToUserTime(time, DATENTIME_DATA_FORMAT);
                        sscanf(dateString.c_str(), DATENTIME_TYPE_FORMAT, &dateArray[DATE], &dateArray[MONTH],
                            &dateArray[YEAR], &dateArray[HOUR], &dateArray[MINUTE], &dateArray[SECOND]);

                        dateArray[MILLISECOND] = milliSec;

                        m_pDDSController->convUserTimeToDDSTime(dateArray,varParam.value.CA);
            
                        if (nullptr != dateArray)
                        {
                            delete[] dateArray;
                            dateArray = nullptr;
                        }
                    }
                }
            }
        }
        break;

        case DDS_DATE_AND_TIME:
        {
            long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
			    /* converting a negative value to largest UInt may crash the application
				* handle the exception and return corresponding error
				*/
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<long long, long long>(tempValue))
                {
                    errorCode = validateVariableLimit(itemInfo,varParam);

                    if (errorCode == SUCCESS)
                    {
                        long long date_time_val = itemInfo[JSON_VAR_VALUE].asLargestInt();
                        unsigned int milliSec = 0;

                        if (itemInfo.isMember(JSON_VAR_VAL_MILLISEC))
                        {
                            milliSec = itemInfo[JSON_VAR_VAL_MILLISEC].asUInt();
                        }
                        
                        int* dateArray = new int[DATENTIME_ARRAY_LENGTH];
                        std::string dateString = m_pDDSController->convertsEpochTimeToUserTime(date_time_val, DATENTIME_DATA_FORMAT);
                        sscanf(dateString.c_str(), DATENTIME_TYPE_FORMAT, &dateArray[DATE], &dateArray[MONTH],
                                &dateArray[YEAR], &dateArray[HOUR], &dateArray[MINUTE], &dateArray[SECOND]);
            
                        dateArray[MILLISECOND] = milliSec;

                        m_pDDSController->convUserDateNTimeToDDSDateNTime(dateArray, varParam.value.CA);
            
                        if (nullptr != dateArray)
                        {
                            delete[] dateArray;
                            dateArray = nullptr;
                        }
                    }
                }
            }
        }
        break;

        case DDS_DURATION: 
        {
            unsigned long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
			    /* converting a negative value to largest UInt may crash the application
				* handle the exception and return corresponding error
				*/
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    errorCode = validateVariableLimit(itemInfo,varParam);

                    if (errorCode == SUCCESS)
                    {
                        unsigned long duration = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                        m_pDDSController->convUserDurationToDDSDuration(duration,varParam.value.CA);
                    }
                }
            }
        }
        break;

        case DDS_TIME_VALUE:
        {
            unsigned long long tempValue = 0;
            if (itemInfo[JSON_VAR_VALUE].isIntegral())
            {
			    /* converting a negative value to largest UInt may crash the application
				* handle the exception and return corresponding error
				*/
                try
                {
                    // For validation of range, verified with value 712376526151635
                    tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();
                }
                catch(...)
                {
                    status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE,
                                                PM_ERR_CL_FF_ADAPTOR);
                    return status;
                }
                // For validation of range, verified with value 712376526151635
                if (validateIntegerRange<unsigned int, unsigned long long>(tempValue))
                {
                    errorCode = validateVariableLimit(itemInfo,varParam);

                    if (errorCode == SUCCESS)
                    {
                        unsigned long epochTime = 0;
                        unsigned int milliSec = 0;
                        unsigned int microSec = 0;
                        
                        epochTime = itemInfo[JSON_VAR_VALUE].asLargestUInt();

                        if (itemInfo.isMember(JSON_VAR_VAL_MILLISEC))
                        {
                            milliSec = itemInfo[JSON_VAR_VAL_MILLISEC].asUInt();
                        }
                        
                        if (itemInfo.isMember(JSON_VAR_VAL_MICROSEC))
                        {
                            microSec = itemInfo[JSON_VAR_VAL_MICROSEC].asUInt();
                        }

                        int* dateArray = new int[DATENTIME_ARRAY_LENGTH];
                        
                        dateArray[MILLISECOND] = milliSec;
                        dateArray[MICROSECOND] = microSec;

                        m_pDDSController->convUserTimeValToDDSTimeVal(epochTime,dateArray,varParam.value.CA);
            
                        if (nullptr != dateArray)
                        {
                            delete[] dateArray;
                            dateArray = nullptr;
                        }
                    }
                }
            }
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid Variable Type: " << varParam.type);
        }
        break;
    } //!-- switch case

    if (FAILURE == errorCode)
    {
        LOGF_ERROR(CPMAPP, "Invalid input value for variable: " << varParam.type);
    	status.setError(FF_ADAPTOR_ERR_INVALID_VARIABLE_VALUE, PM_ERR_CL_FF_ADAPTOR);
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Fills Json value key with variable value
*/
void FFAdaptor::fillVarValue(VariableType* varParam, Json::Value& varValueJson)
{
    switch(varParam->type)
    {
        case DDS_INTEGER:
        {
#ifdef CPM_64_BIT_OS
            varValueJson[JSON_VAR_VALUE]  = static_cast<int>(varParam->value.stdValue.l);
#else
            varValueJson[JSON_VAR_VALUE]  = static_cast<Json::Value::LargestInt>(varParam->value.stdValue.l);
#endif
        }
        break;

        case DDS_UNSIGNED:      //FALLTHROUGH
        case DDS_ENUMERATED:    //FALLTHROUGH
        case DDS_BIT_ENUMERATED://FALLTHROUGH
        case DDS_BOOLEAN_T:     //FALLTHROUGH
      //  case DDS_INDEX:         //FALLTHROUGH
        case DDS_MAX_TYPE:      //FALLTHROUGH
        case DDS_ACK_TYPE:
        {
#ifdef CPM_64_BIT_OS
            varValueJson[JSON_VAR_VALUE]  = static_cast<unsigned long>(varParam->value.stdValue.ul);
#else
            varValueJson[JSON_VAR_VALUE]  = static_cast<Json::Value::LargestUInt>(varParam->value.stdValue.ul);
#endif
        }
        break;

        case DDS_FLOAT:
        {
            varValueJson[JSON_VAR_VALUE]  = varParam->value.stdValue.f;
        }
        break;

        case DDS_DOUBLE:
        {
            varValueJson[JSON_VAR_VALUE]  = varParam->value.stdValue.d;
        }
        break;

        case DDS_ASCII:         //FALLTHROUGH
        case DDS_OCTETSTRING:   //FALLTHROUGH
        case DDS_VISIBLESTRING: //FALLTHROUGH
        case DDS_PACKED_ASCII:  //FALLTHROUGH
        case DDS_PASSWORD:      //FALLTHROUGH
        case DDS_EUC:           //FALLTHROUGH
        case DDS_BITSTRING:     //FALLTHROUGH
        {
            std::string strValue;
            strValue.assign(varParam->value.strData.str);
            varValueJson[JSON_VAR_VALUE] = strValue;
        }
        break;

        case DDS_TIME: 
        {
            auto dateArray = new int[DATENTIME_ARRAY_LENGTH];
            m_pDDSController->convDDSTimeToUserTime(varParam->value.CA, dateArray);
            auto dateNTime = new char[DATENTIME_STR_LENGTH]();
            sprintf(dateNTime, DATENTIME_TYPE_FORMAT, dateArray[DATE], dateArray[MONTH], dateArray[YEAR],
                    dateArray[HOUR], dateArray[MINUTE], dateArray[SECOND]);

            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestUInt>(m_pDDSController->convertsUserTimeToEpochTime(dateNTime, DATENTIME_DATA_FORMAT));
            varValueJson[JSON_VAR_VAL_INFO][JSON_VAR_VAL_MILLISEC] = dateArray[MILLISECOND];

            if (nullptr != dateArray)
            {
                delete[] dateArray;
            }

            if (nullptr != dateNTime)
            {
                delete[] dateNTime;
            }
        }
        break;

        case DDS_DATE_AND_TIME:
        {
            auto dateArray = new int[DATENTIME_ARRAY_LENGTH];
            m_pDDSController->convDDSDateNTimeToUserDateNTime(varParam->value.CA, dateArray);
            auto dateNTime = new char[DATENTIME_STR_LENGTH]();
            sprintf(dateNTime, DATENTIME_TYPE_FORMAT, dateArray[DATE], dateArray[MONTH], dateArray[YEAR],
                    dateArray[HOUR], dateArray[MINUTE], dateArray[SECOND]);

            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestInt>(m_pDDSController->convertsUserTimeToEpochTime(dateNTime, DATENTIME_DATA_FORMAT));
            varValueJson[JSON_VAR_VAL_INFO][JSON_VAR_VAL_MILLISEC] = dateArray[MILLISECOND];

            if (nullptr != dateArray)
            {
                delete[] dateArray;
            }

            if (nullptr != dateNTime)
            {
                delete[] dateNTime;
            }
        }
        break;

        case DDS_DURATION:
        {
            unsigned long long mSec = 0;
            m_pDDSController->convDDSDurationToUserDuration(varParam->value.CA, &mSec);
            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestUInt>(mSec);
        }
        break;

        case DDS_TIME_VALUE:
        {
            auto dateArray = new int[DATENTIME_ARRAY_LENGTH];
            m_pDDSController->convDDSTimeValToUserTimeVal(varParam->value.CA, dateArray);
            auto dateNTime = new char[DATENTIME_STR_LENGTH]();
            sprintf(dateNTime, DATENTIME_TYPE_FORMAT, dateArray[DATE], dateArray[MONTH], dateArray[YEAR],
                    dateArray[HOUR], dateArray[MINUTE], dateArray[SECOND]);

            varValueJson[JSON_VAR_VALUE] = static_cast<Json::Value::LargestUInt>(m_pDDSController->convertsUserTimeToEpochTime(dateNTime, DATENTIME_DATA_FORMAT));
            varValueJson[JSON_VAR_VAL_INFO][JSON_VAR_VAL_MILLISEC] = dateArray[MILLISECOND];
            varValueJson[JSON_VAR_VAL_INFO][JSON_VAR_VAL_MICROSEC] = dateArray[MICROSECOND];

            if (nullptr != dateArray)
            {
                delete[] dateArray;
            }

            if (nullptr != dateNTime)
            {
                delete[] dateNTime;
            }
        }
        break;

        default:
        {
            LOGF_ERROR(CPMAPP, "Invalid Variable Type: " << varParam->type);
        }
        break;
    } //!-- switch case
}

/*   ----------------------------------------------------------------------------
     Returns string extracted from StringType parameter
*/
std::string FFAdaptor::validateAndGetStringValue(const char* strData, unsigned int strLength)
{
    std::string retStr("");

    if ((nullptr != strData) &&
        (0 != strLength) &&
        ('\0' != (*(strData))))
    {
    	m_pDDSController->getLanguageString(strData, retStr, strLength);
    }

    return retStr;
}

/*  ----------------------------------------------------------------------------
    Set application settings to persistent storage
    Authored By: Utthunga
*/
Json::Value FFAdaptor::setAppSettings(const Json::Value &jsonData)
{
    (void) jsonData;

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Apply the qualifiers for Variables
*/
void FFAdaptor::applyQualifiersForVariables(unsigned short qualifier, Json::Value & itemJsonInfo)
{
    // Check for Read Only qualifier
    if (qualifier & READ_ONLY_ITEM)
    {
        itemJsonInfo[JSON_ITEM_READONLY] = true;
    }
    // Check for Label Qualifier
    if (qualifier & NO_LABEL_ITEM)
    {
        itemJsonInfo[JSON_ITEM_LABEL] = "";
    }
    // Check for Unit Qualifier
    if (qualifier & NO_UNIT_ITEM)
    {
        itemJsonInfo[JSON_VAR_UNIT] = "";
    }
}

/*  ----------------------------------------------------------------------------
    Get application settings from persistent storage
    Authored By: Utthunga
*/
Json::Value FFAdaptor::getAppSettings()
{
    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
   to get the default HART settings if the file is not present in persistent
   storage
    Authored By: Utthunga
*/
void FFAdaptor::getDefaultAppSettings(Json::Value &jsonData)
{
    (void) jsonData;
}

/*  ----------------------------------------------------------------------------
   To fill specific information about each menu item based on the type
*/
bool FFAdaptor::fillMenuItemInfoByType(ItemBasicInfo& sMenuItemInfo, DDI_GENERIC_ITEM& ddiGenericItem)
{
    bool status = true;

    switch(ddiGenericItem.item_type)
    {
        case RECORD_ITYPE:
        {
            FLAT_RECORD* pRecord = static_cast<FLAT_RECORD *>(ddiGenericItem.item);
            if (nullptr != pRecord)
            {
                if (0 == pRecord->valid)
				{
                    return false;
                }
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pRecord->label.str, pRecord->label.len);
                sMenuItemInfo.itemHelp = validateAndGetStringValue(pRecord->help.str, pRecord->help.len);
            }
        }
        break;
        case METHOD_ITYPE:
        {
            FLAT_METHOD* pMethod = static_cast<FLAT_METHOD *>(ddiGenericItem.item);
            if (nullptr != pMethod)
            {
                if (0 == pMethod->valid)
				{
                	return false;
				}
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pMethod->label.str, pMethod->label.len);
                sMenuItemInfo.itemHelp = validateAndGetStringValue(pMethod->help.str, pMethod->help.len);
            }
        }
        break;
        case ARRAY_ITYPE:
        {
            FLAT_ARRAY* pArray = static_cast<FLAT_ARRAY *>(ddiGenericItem.item);
            if (nullptr != pArray)
            {
                if (0 == pArray->valid)
				{
                	return false;
				}
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pArray->label.str, pArray->label.len);
                sMenuItemInfo.itemHelp = validateAndGetStringValue(pArray->help.str, pArray->help.len);
            }
        }
        break;
        case EDIT_DISP_ITYPE:
        {
            FLAT_EDIT_DISPLAY* pEditDisplay = static_cast<FLAT_EDIT_DISPLAY *>(ddiGenericItem.item);
            if (nullptr != pEditDisplay)
            {
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pEditDisplay->label.str, pEditDisplay->label.len);
            }
        }
        break;
        case MENU_ITYPE:
        {
            FLAT_MENU* pMenu = static_cast<FLAT_MENU *>(ddiGenericItem.item);
            if (nullptr != pMenu)
            {
                if (0 == pMenu->valid)
				{
                	return false;
				}
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pMenu->label.str, pMenu->label.len);
            }
        }
        break;
        case VARIABLE_ITYPE:
        {
            FLAT_VAR* pVar = static_cast<FLAT_VAR *>(ddiGenericItem.item);
            if (pVar != nullptr)
            {
                if (0 == pVar->misc->valid)
				{
                 	return false;
				}
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pVar->label.str, pVar->label.len);
                sMenuItemInfo.itemHelp = validateAndGetStringValue(pVar->help.str, pVar->help.len);
            }
        }
        break;
        case IMAGE_ITYPE:
        {
            FLAT_IMAGE* pImage = static_cast<FLAT_IMAGE *>(ddiGenericItem.item);
            if (pImage != nullptr)
            {
                if (0 == pImage->valid)
                {
                    return false;
                }
                sMenuItemInfo.itemLabel = validateAndGetStringValue(pImage->label.str, pImage->label.len);
                sMenuItemInfo.itemHelp = validateAndGetStringValue(pImage->help.str, pImage->help.len);
            }
        }
        break;
        default:
        {
            LOGF_ERROR(CPMAPP, " This Item Type is not Supported : " << m_itemTypeMap[ddiGenericItem.item_type]);
            status = false;
        }
        break;
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Prepares symbol table of DD items by parsing symbol file
*/
PMStatus FFAdaptor::prepareSymTable()
{
    PMStatus prepSymTable;
    ifstream symFileStream;

    DD_DEVICE_ID ddDeviceID{};
    char symbolFileName[MAX_LEN] = {};

    prepSymTable = m_pDDSController->getDeviceIDInfo(ddDeviceID);
    std::string ddFileExtn = m_pDDSController->getDDFileExtn();
    std::string symFileExtn = "";

    if (0 == ddFileExtn.compare(DD5_PROF_DD_EXT))
    {
        symFileExtn = DD5_PROF_SYM_EXT;
    }
    else if (0 == ddFileExtn.compare(DD4_PROF_DD_EXT))
    {
        symFileExtn = DD4_PROF_SYM_EXT; 
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Incorrect DD File extention, not handled!!");
    }

    if (prepSymTable.hasError() && symFileExtn.empty())
    {
        LOGF_ERROR(CPMAPP, "Failed to get device information and symbol file extension");
        prepSymTable.clear();
        prepSymTable.setError( FF_ADAPTOR_ERR_PREP_SYM_TABLE_FAILED,
                               PM_ERR_CL_FF_ADAPTOR);
        return prepSymTable;
    } 

    (void)sprintf(symbolFileName, "%s/%06X/%04X/%02X%02X.%s", m_ffDDBinaryPath.c_str(),
                  ddDeviceID.ddid_manufacturer,
                  ddDeviceID.ddid_device_type,
                  ddDeviceID.ddid_device_rev,
                  ddDeviceID.ddid_dd_rev, 
                  symFileExtn.c_str());

    if (-1 == access(symbolFileName, F_OK)) 
    {
	memset (&symbolFileName, '\0', MAX_LEN);
	(void)sprintf(symbolFileName, "%s/%06x/%04x/%02x%02x.%s", m_ffDDBinaryPath.c_str(),
                  ddDeviceID.ddid_manufacturer,
                  ddDeviceID.ddid_device_type,
                  ddDeviceID.ddid_device_rev,
                  ddDeviceID.ddid_dd_rev, 
                  symFileExtn.c_str());
    }
                                
    symFileStream.open(symbolFileName, ifstream::in);

    // Parse the symbol file and create a symbol map table for it
    if (symFileStream.good())
    {
        char symFileData[MAX_LEN] = {0};
        while (symFileStream.getline(symFileData, MAX_LEN))
        {
            vector<string> symVectData;
            PARAM_DATA sParamData;
            string strData;
            std::stringstream strStream (symFileData);

            while (strStream >> strData)
            {
                symVectData.push_back(strData);
            }

            int nIndex = 0;
            string symTableKey = symVectData.at(nIndex++);
            sParamData.symParamName = symVectData.at(nIndex++);

            if (symVectData.size() > NUM_OF_COLUMN)
                sParamData.symParamType =  symVectData.at(nIndex++);

            sParamData.symParamID = (ITEM_ID) strtoul(symVectData.at(nIndex).c_str(), 0, 16);

            m_SymTable.insert(pair<string, PARAM_DATA>(symTableKey, sParamData));
         }
         LOGF_INFO(CPMAPP, "Parsing symbol file: " << symbolFileName);
     }

    else
    {
        prepSymTable.setError(FF_ADAPTOR_ERR_PREP_SYM_TABLE_FAILED,PM_ERR_CL_FF_ADAPTOR);
    }
    return prepSymTable;
}

/*  ----------------------------------------------------------------------------
    Returns Block menu name from symbol table
*/
std::string FFAdaptor::fetchMenuNameFromMap(ITEM_ID menuID)
{
    std::string blockMenuName;

    // Map holding device menus against menu ID
    std::pair<std::map<string, PARAM_DATA>::iterator, std::map<string, PARAM_DATA>::iterator> menuMap;

    menuMap = m_SymTable.equal_range(SYM_TABLE_MENU);

    for (auto menuItem = menuMap.first; menuItem != menuMap.second; ++menuItem)
    {
        PARAM_DATA sParamData = menuItem->second;

        if (menuID == sParamData.symParamID)
        {
            blockMenuName = sParamData.symParamName;
            break;
        }
    }
    return blockMenuName;
}

/*  ----------------------------------------------------------------------------
    Utility function for input validation for the variables
*/
template<typename RangeType, typename ValueType>
bool FFAdaptor::validateIntegerRange(ValueType value)
{
	// checking if variable is float and double type
    if (!numeric_limits<RangeType>::is_integer)
    {
        return false;
    }

    // checking for signed integer
    if (numeric_limits<RangeType>::is_signed ==
         numeric_limits<ValueType>::is_signed )
    {
        return ((value >= numeric_limits<RangeType>::min()) &&
               (value <= numeric_limits<RangeType>::max()));
    }
    else if (numeric_limits<RangeType>::is_signed )
    {
        return (value <= numeric_limits<RangeType>::max());
    }
    else
    {
        return ((value >= 0) && (value <= numeric_limits<RangeType>::max()));
    }
}


/*  ----------------------------------------------------------------------------
    Utility function for input validation with variable size
*/
int FFAdaptor::validateVariableLimit
(
    const Json::Value& itemInfo,
    VariableType& varParam
)
{
	int errorCode = SUCCESS;

	// check if var size is less than and equal to 2
	if (varParam.size <= VAR_SIZE_TWO)
	{
		switch (varParam.type)
		{
			case DDS_INTEGER:
			{
				long tempValue = itemInfo[JSON_VAR_VALUE].asLargestInt();

				if (varParam.size == VAR_SIZE_ONE)
				{
					if (!((tempValue >= SCHAR_MIN) &&
							(tempValue <= SCHAR_MAX)))
					{
						errorCode = FAILURE;
					}
				}
				else if (varParam.size == VAR_SIZE_TWO)
				{
					if (!((tempValue >= SHRT_MIN) &&
						(tempValue <= SHRT_MAX)))
					{
						errorCode = FAILURE;
					}
				}
			}
			break;

			case DDS_UNSIGNED:
			case DDS_ENUMERATED:
			case DDS_BIT_ENUMERATED:
			case DDS_BOOLEAN_T:
			case DDS_MAX_TYPE:
			case DDS_ACK_TYPE:
			{
				unsigned long long tempValue = itemInfo[JSON_VAR_VALUE].asLargestUInt();

				if (varParam.size == VAR_SIZE_ONE)
				{
					if (!((tempValue >= MIN_LIMIT) &&
							(tempValue <= UCHAR_MAX)))
					{
						errorCode = FAILURE;
					}
				}
				else if (varParam.size == VAR_SIZE_TWO)
				{
					if (!((tempValue >= MIN_LIMIT) &&
						(tempValue <= USHRT_MAX)))
					{
						errorCode = FAILURE;
					}
				}
			}
			break;

			default:
			{
                LOGF_INFO (CPMAPP,"SET_VARIABLE: VAR size is not in this scope");
			}
			break;
		}
	}
	return errorCode;
}


/*  ----------------------------------------------------------------------------
    Validates Item Container Tag input
*/
int FFAdaptor::validateItemContTag
(
    const Json::Value& jsonRequest, 
    std::string& itemContainerTag
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_ITEM_CONT_TAG) &&
        jsonRequest[JSON_ITEM_CONT_TAG].isString())
    {
        // Empty tag is also valid for local and device variables
        itemContainerTag = jsonRequest[JSON_ITEM_CONT_TAG].asString();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates bit Index  input
*/
int FFAdaptor::validateItemBitIndex
(
    const Json::Value& jsonRequest,
	unsigned long& itemBitIndex
)
{
	int status = FAILURE;

	if (jsonRequest.isMember(JSON_VAR_BIT_INDEX))
    {
        if (jsonRequest[JSON_VAR_BIT_INDEX].isIntegral())
        {
		    itemBitIndex = jsonRequest[JSON_VAR_BIT_INDEX].asLargestUInt();
            // check if number is power of 2
            if (CPMUtil::isPow2(itemBitIndex))
            {
                status = SUCCESS;
            }
        }
    }
    else
    {
        status = SUCCESS;
    } 
	return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item ID input
*/
int FFAdaptor::validateItemID
(
    const Json::Value& jsonRequest, 
    ITEM_ID& itemID
)
{
    int status = FAILURE;

    if ( jsonRequest.isMember(JSON_ITEM_ID) &&
         jsonRequest[JSON_ITEM_ID].isIntegral() &&
         (jsonRequest[JSON_ITEM_ID].asLargestInt() > 0))
    {
        itemID = jsonRequest[JSON_ITEM_ID].asLargestUInt();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item Container ID input
*/
int FFAdaptor::validateItemContID
(
    const Json::Value& jsonRequest, 
    ITEM_ID& containerID
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_VAR_CONT_ID))
    {
        if (jsonRequest[JSON_VAR_CONT_ID].isIntegral() &&
            (jsonRequest[JSON_VAR_CONT_ID].asLargestInt() >= 0))
        {
            containerID = jsonRequest[JSON_VAR_CONT_ID].asLargestUInt();
            status = SUCCESS;
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item SubIndex input
*/
int FFAdaptor::validateItemSubIndex
(
    const Json::Value& jsonRequest, 
    unsigned short& subIndex
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_VAR_SUBINDEX))
    {
        if (jsonRequest[JSON_VAR_SUBINDEX].isIntegral() &&
            (jsonRequest[JSON_VAR_SUBINDEX].asLargestInt() >= 0))
        {
            subIndex = jsonRequest[JSON_VAR_SUBINDEX].asUInt();
            status = SUCCESS;
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item in DD input
*/
int FFAdaptor::validateItemInDD
(
    const Json::Value& jsonRequest, 
    bool requiredState
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_ITEM_IN_DD) &&
        jsonRequest[JSON_ITEM_IN_DD].isBool())
    {
        if (requiredState == jsonRequest[JSON_ITEM_IN_DD].asBool())
        {
            status = SUCCESS;
        }
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item has relation
*/
int FFAdaptor::validateItemHasRelation
(
    const Json::Value& jsonRequest,
    bool& itemHasRelation
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_ITEM_HAS_RELATION) &&
        jsonRequest[JSON_ITEM_HAS_RELATION].isBool())
    {
        itemHasRelation = jsonRequest[JSON_ITEM_HAS_RELATION].asBool();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item Request Type
*/
int FFAdaptor::validateItemReqType
(
    const Json::Value& jsonRequest, 
    std::string& reqType
)
{
    int status = FAILURE;

    if ( jsonRequest.isMember(JSON_ITEM_REQ_TYPE) &&
         jsonRequest[JSON_ITEM_REQ_TYPE].isString())
    {
        reqType = jsonRequest[JSON_ITEM_REQ_TYPE].asString();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item Type
*/
int FFAdaptor::validateItemType
(
    const Json::Value& jsonRequest,
    std::string& itemType
)
{
    int status = FAILURE;

    if ( jsonRequest.isMember(JSON_ITEM_TYPE) &&
         jsonRequest[JSON_ITEM_TYPE].isString())
    {
        itemType = jsonRequest[JSON_ITEM_TYPE].asString();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Checks whether float value is not a number     
*/
bool FFAdaptor::isNAN(float floatVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isnan(floatVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isnan(floatVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;
}

/*  ----------------------------------------------------------------------------
    Checks whether double value is not a number     
*/
bool FFAdaptor::isNAN(double doubleVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isnan(doubleVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isnan(doubleVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;
}

/*  ----------------------------------------------------------------------------
    Checks whether float value is infinity     
*/
bool FFAdaptor::isINF(float floatVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isinf(floatVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isinf(floatVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;

}

/*  ----------------------------------------------------------------------------
    Checks whether double value is infinity     
*/
bool FFAdaptor::isINF(double doubleVal)
{
    bool bRetVal = false;
// CPM : 64bit to 32 bit conversion
#ifdef CPM_64_BIT_OS
        if (std::isinf(doubleVal))
        {
            bRetVal = true;
        }
#else       
        if (__builtin_isinf(doubleVal))
        {
            bRetVal = true;
        }
#endif
        return bRetVal;

}


/*  ----------------------------------------------------------------------------
    Returns array index in required format for Array Label     
*/
std::string FFAdaptor::getArrayIndex(unsigned short subIndex)
{
    std::string arrayIndex = "[";
    arrayIndex += (std::to_string(subIndex));
    arrayIndex += "]";
    return arrayIndex;
}

/*  ----------------------------------------------------------------------------
    Get Bit Enumeration Item Label and Help     
*/
void FFAdaptor::getBitEnumItemStr
(
    VariableType* varType, 
    BitEnumItemStr& bitEnumItem
)
{
    std::map <unsigned long, BitEnumItemStr> bitEnumStrMap;
    for (EnumValue enumItem : varType->enums)
    {
        BitEnumItemStr tmpBitEnumItem;
        tmpBitEnumItem.labelStr = validateAndGetStringValue(enumItem.label.str, enumItem.label.len); 
        tmpBitEnumItem.helpStr = validateAndGetStringValue(enumItem.help.str, enumItem.help.len); 
        bitEnumStrMap.emplace(enumItem.value, tmpBitEnumItem);
    }

    bitEnumItem.labelStr = bitEnumStrMap.find(bitEnumItem.bitIndex)->second.labelStr;
    bitEnumItem.helpStr = bitEnumStrMap.find(bitEnumItem.bitIndex)->second.helpStr;
}

/*  ----------------------------------------------------------------------------
    Get Bit Enumeration Item Label and Help     
*/
void FFAdaptor::setBitEnumItemVal
(
    unsigned long& devBitEnumVal,
    bool bitEnumItemVal, 
    unsigned long bitIndex
)
{
	// Read bit index value and convert it to bit position
	unsigned int bitPos = CPMUtil::getBitPos(bitIndex);

	if(bitEnumItemVal)
	{
        // Set the corresponding bit in existing variable value
		devBitEnumVal |= (1 << bitPos);
	}
	else
	{
        // Unset the corresponding bit in existing variable value
		devBitEnumVal &= ~(1 << (bitPos));
	}
}


/*  ----------------------------------------------------------------------------
    Cleanup function after device disconnection     
*/
PMStatus FFAdaptor::doCleanUP(unsigned char node_number)
{
	PMStatus status;
	Json::Value value, jsonData, jsonResultObject;

	if ((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_CONNECTION_IN_PROGRESS)  ||
			((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_DEVICE_CONNECTED)) ||
			((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_COMMSSION_IS_IN_PROGRESS)))
	{
		if (( node_number == m_nodeAddress)
				||((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_COMMSSION_IS_IN_PROGRESS)) )
		{
			CPMStateMachine::getInstance()->setCpmState(CPMSTATE_DISCONNECTION_IN_PROGRESS );

			// Lockig the connection mutex
			m_connectLock.lock();

			if ((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_DEVICE_CONNECTED))
			{
				status = m_pFFParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_NONE);

				// Deletes the internal subscription table and items
				deleteSubscriptionTableItem();

				while((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_METHOD_EXEC_IN_PROGRESS))
				{
					DDSController::s_pMethodExecutionData.isItemSet = true;
					DDSController::s_pMethodExecutionData.isAborted = true;

					if((true == DDSController::s_pMethodExecutionData.isWaitingForUserInput))
					{
						// Unlocks the ME thread and notify that data has arrived
						m_pDDSController->releaseLock();
						DDSController::s_pMethodExecutionData.isWaitingForUserInput = false;
					}
                    // Sleep and allow other threads to run
                    std::this_thread::sleep_for (std::chrono::milliseconds(METHOD_EXEC_SLEEP_INTERVAL)); 
				}

				if (!status.hasError())
				{
					status = m_pFFConnectionManager->disconnectDevice();
				}

				// Clear symbol table
				m_SymTable.clear();

				if (status.hasError())
				{
					LOGF_ERROR(CPMAPP, "Device Disconnect Failed: " << status.getErrorCode());
				}
				CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_DEVICE_CONNECTED);
			}

			// Clear state as connected
			CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_CONNECTION_IN_PROGRESS );
			CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_DISCONNECTION_IN_PROGRESS );

			// Notify to IC about device disconnection
			std::string publisher = STATUS_PUBLISHER;
			jsonData[JSON_ITEM_STATUS_CLASS] = JSON_CONNECT_NOTIFICATION;

			Error::Entry error = Error::cpm.DEVICE_DISCONNECTED;
			jsonResultObject = JsonRpcUtil::setErrorObject(error.code, error.message);
			jsonData[JSON_ITEM_STATUS] = jsonResultObject;

			CommonProtocolManager::getInstance()->notifyMessage(
										publisher, jsonData);
			m_connectLock.unlock();
			return status;
		}
		else
		{
            LOGF_TRACE(CPMAPP, "Additional Info: Removed node: " << node_number << " from network does not match with connected device: " << m_nodeAddress << endl);
			status.setError(FF_ADAPTOR_ERR_NOT_CURRENT_NODE_ADDRESS, PM_ERR_CL_FF_ADAPTOR);
			return status;
		}
	}
	return status;
}

/*  ----------------------------------------------------------------------------
    Gets connection manager instance
*/
FFConnectionManager* FFAdaptor::getConnMangerInstance()
{
	return m_pFFConnectionManager;
}

/*  ----------------------------------------------------------------------------
    Stops method execution
*/
void FFAdaptor::stopMethodExec()
{
	while((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_METHOD_EXEC_IN_PROGRESS))
	{
        LOGF_DEBUG(CPMAPP, "Stopping current method execution");
		DDSController::s_pMethodExecutionData.isItemSet = true;
		DDSController::s_pMethodExecutionData.isAborted = true;

		if((DDSController::s_pMethodExecutionData.isWaitingForUserInput == true))
		{
			// Unlocks the ME thread and notify that data has arrived
			m_pDDSController->releaseLock();
			DDSController::s_pMethodExecutionData.isWaitingForUserInput = false;
		}
        // Sleep and allow other threads to run
        std::this_thread::sleep_for (std::chrono::milliseconds(METHOD_EXEC_SLEEP_INTERVAL)); 
	}
    LOGF_DEBUG(CPMAPP, "Method execution stopped!!");
}

/*  ----------------------------------------------------------------------------
    Stops  and clears all active external/internal subscriptions 
*/
PMStatus FFAdaptor::stopAndClearSubscription()
{
    PMStatus status;
	if ((CPMStateMachine::getInstance()->getCpmState() & CPMSTATE_DEVICE_CONNECTED)
            && (nullptr != m_pFFParameterCache))
	{
		status = m_pFFParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_NONE);

		// Deletes the internal subscription table and items
		if (!status.hasError())
        {
            deleteSubscriptionTableItem();
            LOGF_DEBUG(CPMAPP, "Subscription stopped and cleared"); 
        }
    }
    return status;
}
