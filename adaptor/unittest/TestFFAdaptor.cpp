/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://aravindnagaraj@bitbucket.org/flukept/digipro.git
    Authored By:       Aravind Nagaraj
    Origin:            ProStar
*/

/** @file
    TestFFAdaptor class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////
#include <string>
#include "json/json.h"
#include "gtest/gtest.h"
#include "BusinessLogic.h"
#include "FFAdaptor.h"

using namespace Json;
/////////////////////////////////////////////////////////////////////////////
// Global data
/////////////////////////////////////////////////////////////////////////////
static FFAdaptor* g_pFFAdaptor =  new FFAdaptor();

/////////////////////////////////////////////////////////////////////////////
// Following are the declarations of classes
/////////////////////////////////////////////////////////////////////////////
class TestFFAdaptor : public ::testing::Test
{
public:
  TestFFAdaptor()
  {
    /// perform initialization
	  FF_BUS_PARAMS busParam;
  }

  ~TestFFAdaptor()
  {
    /// Perform cleanup
  }
  void readJsonData(Json::Value& UIData);
protected:
  virtual void SetUp()
  {
    /// Called just before the test executes
  }

  virtual void TearDown()
  {
    /// Called just after test execution
  }
};


//==============================================================================
/**
*       testStartScan
*/
//==============================================================================
TEST_F(TestFFAdaptor, testStartScan)
{
    g_pFFAdaptor->initialise();

    Json::Value root = g_pFFAdaptor->startScan("");
    
    EXPECT_EQ("ok", root["result"].asString()); 
}

//==============================================================================
/**
*       testStopScan
*/
//==============================================================================
TEST_F(TestFFAdaptor, testStopScan)
{
    Json::Value root = g_pFFAdaptor->stopScan();
 
    EXPECT_EQ("ok", root["result"].asString());

    delete g_pFFAdaptor;
}


//==============================================================================
/**
*       readJsonData : function to create Json dtata for test
*/
//==============================================================================
void TestFFAdaptor::readJsonData(Json::Value& root)
{
	root["FF_BUS_PARAMS"]["this_link"]   = 0;
	root["FF_BUS_PARAMS"]["slot_time"]   = 8;
	root["FF_BUS_PARAMS"]["phlo"]        = 6;
	root["FF_BUS_PARAMS"]["mrd"]         = 10;
	root["FF_BUS_PARAMS"]["mipd"]        = 8;
	root["FF_BUS_PARAMS"]["tsc"]         = 3;
	root["FF_BUS_PARAMS"]["phis_skew"]   = 0;
	root["FF_BUS_PARAMS"]["phpe"]        = 2;
	root["FF_BUS_PARAMS"]["phge"]        = 0;
	root["FF_BUS_PARAMS"]["fun"]         = 248;
	root["FF_BUS_PARAMS"]["nun"]         = 0;
	root["FF_BUS_PARAMS"]["set_default"] = 0;
}

//==============================================================================
/**
*       setBusParameters
*/
//==============================================================================

TEST_F (TestFFAdaptor, setBusParameters)
{
	Json::Value root;
    Json::Value actualJsonData, actualJsonStrData;
    std::string expectedJsonStrData("ok") ;

	readJsonData(root);

	EXPECT_NO_THROW(actualJsonData = g_pFFAdaptor->setBusParameters(root));

	EXPECT_EQ(expectedJsonStrData, actualJsonData.asString());
}

//==============================================================================
/**
*       getBusParameters
*/
//==============================================================================

TEST_F (TestFFAdaptor, getBusParameters)
{
    Json::FastWriter fastWriter;

    Json::Value expectedJsonData;

	expectedJsonData["FF_BUS_PARAMS"]["this_link"]   = 0;
	expectedJsonData["FF_BUS_PARAMS"]["slot_time"]   = 8;
	expectedJsonData["FF_BUS_PARAMS"]["phlo"]        = 6;
	expectedJsonData["FF_BUS_PARAMS"]["mrd"]         = 10;
	expectedJsonData["FF_BUS_PARAMS"]["mipd"]        = 8;
	expectedJsonData["FF_BUS_PARAMS"]["tsc"]         = 3;
	expectedJsonData["FF_BUS_PARAMS"]["phis_skew"]   = 0;
	expectedJsonData["FF_BUS_PARAMS"]["phpe"]        = 2;
	expectedJsonData["FF_BUS_PARAMS"]["phge"]        = 0;
	expectedJsonData["FF_BUS_PARAMS"]["fun"]         = 248;
	expectedJsonData["FF_BUS_PARAMS"]["nun"]         = 0;

    std::string expectedJsonStrData = fastWriter.write(expectedJsonData);

    /// Get the actual bus paramaters
    Json::Value actualJsonData;
	EXPECT_NO_THROW(actualJsonData = g_pFFAdaptor->getBusParameters());
    std::string actualJsonStrData = fastWriter.write(actualJsonData);
    
	EXPECT_EQ(expectedJsonStrData, actualJsonStrData);
}

//==============================================================================
/**
*       fetchBusParameters
*/
//==============================================================================
TEST_F (TestFFAdaptor, fetchBusParameters)
{
	FF_BUS_PARAMS params;
	PMStatus stackStatus;

	int errorCheck = 0;

	EXPECT_NO_THROW(stackStatus = g_pFFAdaptor->fetchBusParameters(&params));
	if(stackStatus.hasError())
	{
	    	errorCheck = 1;
	}

	EXPECT_EQ(errorCheck,stackStatus.getErrorCode());
}

//==============================================================================
/**
*       getDefaultBusParameters
*/
//==============================================================================

TEST_F (TestFFAdaptor, getDefaultBusParameters)
{
    Json::FastWriter fastWriter;
	Json::Value root;

	root["FF_BUS_PARAMS"]["this_link"]   = 0;
	root["FF_BUS_PARAMS"]["slot_time"]   = 8;
	root["FF_BUS_PARAMS"]["phlo"]        = 6;
	root["FF_BUS_PARAMS"]["mrd"]         = 10;
	root["FF_BUS_PARAMS"]["mipd"]        = 16;
	root["FF_BUS_PARAMS"]["tsc"]         = 3;
	root["FF_BUS_PARAMS"]["phis_skew"]   = 0;
	root["FF_BUS_PARAMS"]["phpe"]        = 2;
	root["FF_BUS_PARAMS"]["phge"]        = 0;
	root["FF_BUS_PARAMS"]["fun"]         = 248;
	root["FF_BUS_PARAMS"]["nun"]         = 0;
	root["FF_BUS_PARAMS"]["set_default"] = 1;

	/// setting default paramters
	g_pFFAdaptor->setBusParameters(root);
    
    /// Get default bus parameters and stores data internally within adaptor 
    /// instance
    Json::Value defaultBusParam;
	g_pFFAdaptor->getDefaultBusParameters(defaultBusParam);

    /// Fetch the parameters which is set earlier in the previous step
    Json::Value actualJsonData;
    std::string actualJsonStrData;  
	actualJsonData = g_pFFAdaptor->getBusParameters();
    actualJsonStrData = fastWriter.write(actualJsonData);

    /// Store the expected json data
    Json::Value expectedJsonData;
	expectedJsonData["FF_BUS_PARAMS"]["this_link"]   = 0;
	expectedJsonData["FF_BUS_PARAMS"]["slot_time"]   = 8;
	expectedJsonData["FF_BUS_PARAMS"]["phlo"]        = 6;
	expectedJsonData["FF_BUS_PARAMS"]["mrd"]         = 10;
	expectedJsonData["FF_BUS_PARAMS"]["mipd"]        = 16;
	expectedJsonData["FF_BUS_PARAMS"]["tsc"]         = 3;
	expectedJsonData["FF_BUS_PARAMS"]["phis_skew"]   = 0;
	expectedJsonData["FF_BUS_PARAMS"]["phpe"]        = 2;
	expectedJsonData["FF_BUS_PARAMS"]["phge"]        = 0;
	expectedJsonData["FF_BUS_PARAMS"]["fun"]         = 248;
	expectedJsonData["FF_BUS_PARAMS"]["nun"]         = 0;
    
    /// Convert the json data to json rpc data in string format
    std::string expectedJsonStrData;
    expectedJsonStrData = fastWriter.write(expectedJsonData);

    EXPECT_EQ(expectedJsonStrData, actualJsonStrData);
}

//==============================================================================
TEST_F (TestFFAdaptor, getAlarmMenu)
{
    Json::Value response;

    g_pFFAdaptor->initialise();  

    DeviceInfo *devInfo = new DeviceInfo();
    devInfo->node_address = 28;

    g_pFFAdaptor->m_pFFCm->connectDevice(devInfo);
    cout<<"Block Lists:" <<endl;

    for (int index = 0; index < devInfo->blk_count; ++index)
    {
        std::string tag;
        tag = devInfo->blk_tag[index];
        cout<<tag<<endl;

        // Adding 32 spaces for constructing block tag
        std::string dupTag = "                                ";
        dupTag.replace(0, tag.length(), tag);

//        response = g_pFFAdaptor->getAlarmMenu(tag);

        Json::FastWriter writer;
        cout<< "JSON Response : " << writer.write(response);

	/* Since item validation is being doing in 
	   getAlarmMenu API, checking only ItemLists available 
           and not performing further validation */

	EXPECT_TRUE(response.isMember("ItemLists"));	
    }
}


//==============================================================================
TEST_F (TestFFAdaptor, getStatusMenu)
{
    
    Json::Value response;
   
    g_pFFAdaptor->initialise();
   
    DeviceInfo *devInfo = new DeviceInfo();
    devInfo->node_address = 28;

    g_pFFAdaptor->m_pFFCm->connectDevice(devInfo);
    cout<<"Block Lists:" <<endl;

    for (int index = 0; index < devInfo->blk_count; ++index)
    {
        std::string tag;
        tag = devInfo->blk_tag[index];
        cout<<tag<<endl;

	// Adding 32 spaces for constructing block tag
        std::string dupTag = "                                ";
        dupTag.replace(0, tag.length(), tag);

//        response = g_pFFAdaptor->getStatusMenu(dupTag);

        Json::FastWriter writer;
        cout<< "JSON Response : " << writer.write(response);

        EXPECT_TRUE(response.isMember("ItemLists"));
    }
}

TEST_F (TestFFAdaptor, getFieldDeviceConfig)
{
    Json::Value jsonData;
    EXPECT_NO_FATAL_FAILURE(g_pFFAdaptor->initialise());
    jsonData["DeviceNodeAddress"] = 25;
    Json::Value root = g_pFFAdaptor->getFieldDeviceConfig(jsonData);
    int devClass = root["DeviceClass"].asInt();
    EXPECT_EQ(2, devClass);
}
TEST_F (TestFFAdaptor, deviceClass)
{
    Json::Value jsonData;
    EXPECT_NO_FATAL_FAILURE(g_pFFAdaptor->initialise());
    jsonData["DeviceConfig"] = "DEVICECLASS";
    jsonData["DeviceClass"] = 1;
    jsonData["DeviceNodeAddress"] = 25;
    Json::Value roots = g_pFFAdaptor->setFieldDeviceConfig(jsonData);
    EXPECT_EQ("ok", roots.asString());
}
TEST_F (TestFFAdaptor, deCommission)
{
    Json::Value jsonData;
    EXPECT_NO_FATAL_FAILURE(g_pFFAdaptor->initialise());
    jsonData["DeviceConfig"] = "DECOMMISSION";
    jsonData["DeviceNodeAddress"] = 30;
    Json::Value roots = g_pFFAdaptor->setFieldDeviceConfig(jsonData);
    EXPECT_EQ("ok", roots.asString());
}
TEST_F (TestFFAdaptor, deviceCommission)
{
    Json::Value jsonData;
    EXPECT_NO_FATAL_FAILURE(g_pFFAdaptor->initialise());
    jsonData["DeviceConfig"] = "COMMISSION";
    jsonData["NewDeviceNodeAddress"] = 22;
    jsonData["DeviceNodeAddress"] = 249;
    jsonData["NewDeviceTag"] = "2051FF_TAG";
    Json::Value roots = g_pFFAdaptor->setFieldDeviceConfig(jsonData);
    sleep(5);
    EXPECT_EQ("ok", roots.asString());
}


