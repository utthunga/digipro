/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://vihg@bitbucket.org/flukept/digipro.git
    Authored By:       Vinay Shreyas
    Origin:            ProStar
*/

/** @file
    Unit Test case for HARTAdaptor class
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/// Include Files
#include"gtest/gtest.h"


#include "HARTAdaptor.h"
#include "json/json.h"
#include "BusinessLogic.h"
#include "PMStatus.h"
#include "PMErrorCode.h"
#include "error/CpmErrors.h"

using namespace std;

static HARTAdaptor* s_pHartAdaptor =  new HARTAdaptor();

/// Unit Test class definition for HARTAdaptor
class TestHARTAdaptor : public ::testing::Test
{
public:
    TestHARTAdaptor()
    {
        /// perform initialization
    }

    ~TestHARTAdaptor()
    {
        /// Perform cleanup
    }

protected:
    virtual void SetUp()
    {

    }

    virtual void TearDown()
    {
        /// Called just after test execution
    }
};

// busparam setting area
TEST_F (TestHARTAdaptor, setBusParameters_defaultOne)
{
    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    jsonData[SET_DEFLT] = 1;

    EXPECT_NO_THROW(actualJsonData = s_pHartAdaptor->setBusParameters(jsonData));

    EXPECT_EQ(expectedJsonStrData, actualJsonData.asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pHartAdaptor->getBusParameters());

    int readmasterType = readJsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asInt();
    int readpreamble = readJsonData[HART_BUS_PARAMS_KEY][PREAMBLE].asInt();

    //default master type always Zero
    EXPECT_EQ(0, readmasterType);
    //default preamble always Five
    EXPECT_EQ(5, readpreamble);
}


TEST_F (TestHARTAdaptor, setBusParameters_defaultZero)
{
    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    HartMasterType masterType = HartMasterType::HART_PRIMARY_MASTER;

    jsonData[SET_DEFLT] = 0;

    uint8_t mType = static_cast<uint8_t>(masterType);

    jsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE] = mType;

    // Set the bus parameters and return the result
    EXPECT_NO_THROW(actualJsonData =
            s_pHartAdaptor->setBusParameters(jsonData));

    EXPECT_EQ(expectedJsonStrData, actualJsonData.asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pHartAdaptor->getBusParameters());

    int readmasterType = readJsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asInt();
    int readpreamble = readJsonData[HART_BUS_PARAMS_KEY][PREAMBLE].asInt();

    EXPECT_EQ(mType, readmasterType);
    EXPECT_EQ(5, readpreamble);
}


TEST_F (TestHARTAdaptor, getBusParameters)
{
    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    /// Get the actual bus paramaters
    Json::Value actualJsonData;

    //gets bus param
    EXPECT_NO_THROW(actualJsonData = s_pHartAdaptor->getBusParameters());

    //verifies member is present or not
    EXPECT_EQ(true, actualJsonData.isMember(HART_BUS_PARAMS_KEY));

    int masterType = actualJsonData[HART_BUS_PARAMS_KEY][HART_MASTER_TYPE].asInt();
    int preamble = actualJsonData[HART_BUS_PARAMS_KEY][PREAMBLE].asInt();

    //checks for valid master type
    EXPECT_EQ(masterType, 1);

    //checks for valid preamble
    EXPECT_EQ(preamble, 5);


}

//app setting area
TEST_F (TestHARTAdaptor, setAppSettings_setDefaultOne)
{
    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    jsonData[SET_DEFLT] = 1;

    EXPECT_NO_THROW(actualJsonData = s_pHartAdaptor->setAppSettings(jsonData));

    EXPECT_EQ(expectedJsonStrData, actualJsonData.asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pHartAdaptor->getAppSettings());

    int readPollAddr = readJsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].asInt();

    //default poll address always 1 (0-15)
    EXPECT_EQ(1, readPollAddr);
}


TEST_F (TestHARTAdaptor, setAppSettings_setDefaultZero)
{
    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    int pollOption = 2;

    jsonData[SET_DEFLT] = 0;
    jsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION] = pollOption;

    EXPECT_NO_THROW(actualJsonData = s_pHartAdaptor->setAppSettings(jsonData));

    EXPECT_EQ(expectedJsonStrData, actualJsonData.asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pHartAdaptor->getAppSettings());

    int readPollAddr = readJsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].asInt();

    //expecting user configured poll option
    EXPECT_EQ(pollOption, readPollAddr);
}


TEST_F (TestHARTAdaptor, getAppSettings)
{
    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    Json::Value actualJsonData;

    //gets app settings
    EXPECT_NO_THROW(actualJsonData = s_pHartAdaptor->getAppSettings());

    //verifies member is present or not
    EXPECT_EQ(true, actualJsonData.isMember(HART_APP_SET_KEY));

    int pollOption = actualJsonData[HART_APP_SET_KEY][POLL_BY_ADDR_OPTION].asInt();

    //expect correct pollOption
    EXPECT_EQ(pollOption, 2);
}

//scan area
TEST_F(TestHARTAdaptor, testStartScan)
{
    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    Json::Value root;

    EXPECT_NO_FATAL_FAILURE(root = s_pHartAdaptor->startScan(""));

    EXPECT_EQ(root.asString(), "ok");
}

TEST_F(TestHARTAdaptor, testStopScan)
{

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    Json::Value root;

    //starts scanning
    EXPECT_NO_FATAL_FAILURE(root = s_pHartAdaptor->startScan(""));
    sleep(5);

    EXPECT_EQ(root.asString(), "ok");

    Json::Value jsonResult;
    std::string strData = "ok";

    //stops scanning
    EXPECT_NO_FATAL_FAILURE(jsonResult = s_pHartAdaptor->stopScan());

    EXPECT_EQ(jsonResult.asString(), strData);

    Json::Value jsonResultOne;
    std::string strDataOne = "ok";

    //again call stop scanning
    EXPECT_NO_FATAL_FAILURE(jsonResultOne = s_pHartAdaptor->stopScan());
    Json::FastWriter writer;

    EXPECT_NE(writer.write(jsonResultOne), strDataOne);
}

TEST_F(TestHARTAdaptor, testScanNetwork)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();
    //sleep(60);
    EXPECT_EQ(status.hasError(), false);
}

TEST_F(TestHARTAdaptor, testRootMenu)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 15;

    result = s_pHartAdaptor->connect(jsonData);

    //verifies member is present or not
    EXPECT_EQ(true, result.isMember(JSON_ITEM_ID));

    unsigned int id = result[JSON_ITEM_ID].asLargestUInt();

    //expecting 1000 as root menu id
    EXPECT_EQ(1000, id);
    cout<<"root menu id     :  "<<id<<endl;

    for(auto &item : result[JSON_ITEM_INFO][JSON_ITEM_LIST])
    {
        int id = item[JSON_ITEM_ID].asLargestUInt();
        EXPECT_TRUE(id > 0);
        cout<<"id   :  "<<id<<endl;
    }
}

TEST_F(TestHARTAdaptor, testNonRootMenu)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 15;

    result = s_pHartAdaptor->connect(jsonData);

    result.clear();

    Json::Value jsonReq;
    jsonReq[JSON_ITEM_ID] = 16913;
    jsonReq[JSON_ITEM_TYPE] = "MENU";
    jsonReq[JSON_ITEM_IN_DD] = TRUE;

    result = s_pHartAdaptor->getItem(jsonReq);
    unsigned int id = result[JSON_ITEM_ID].asLargestUInt();

    EXPECT_TRUE(id > 0);

    for(auto &item : result[JSON_ITEM_INFO][JSON_ITEM_LIST])
    {
        int id = item[JSON_ITEM_ID].asLargestUInt();
        string itemType = item[JSON_ITEM_TYPE].asString();
        EXPECT_TRUE(id > 0);
        cout<<"id   :  "<<id <<"   itemType   :    " << itemType<<endl;
    }

    result.clear();
    jsonData.clear();
    jsonReq.clear();
}

TEST_F(TestHARTAdaptor, testNonRootMenuWithInvalidId)
{
    PMStatus status;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    status = s_pHartAdaptor->scanNetwork();

    EXPECT_EQ(status.hasError(), false);

    Json::Value jsonData;
    Json::Value result;

    jsonData["DeviceNodeAddress"] = 15;

    result = s_pHartAdaptor->connect(jsonData);

    result.clear();

    Json::Value jsonReq;
    jsonReq[JSON_ITEM_ID] = 777777;
    jsonReq[JSON_ITEM_TYPE] = "MENU";
    jsonReq[JSON_ITEM_IN_DD] = TRUE;

    result = s_pHartAdaptor->getItem(jsonReq);

    EXPECT_EQ(result[JSON_ITEM_INFO][JSON_ITEM_LIST].size(), 0);

    result.clear();
    jsonData.clear();
    jsonReq.clear();
}

TEST_F (TestHARTAdaptor, ExecuteMethod)
{
    Json::Value jsonData;
    int rs = SUCCESS;
    Json::Value response;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    jsonData["DeviceNodeAddress"] = 0;

    response = s_pHartAdaptor->connect(jsonData);

    //verifies member is present or not
    EXPECT_EQ(true, response.isMember(JSON_ITEM_ID));

    if (nullptr != s_pHartAdaptor)
    {
        // Execute METHOD_show_dd_rev for 644 device
        jsonData["ItemID"] = 17113;
        jsonData["ItemType"] = "METHOD";
        jsonData["ItemContainerTag"] = "";
        jsonData["ItemInDD"] = true;


        response = s_pHartAdaptor->getItem(jsonData);

        EXPECT_EQ(response.asString(), "ok");
    }
    else
    {
        rs = FAILURE;
    }

    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestHARTAdaptor, ExecuteMethod_Invalid_Method)
{
    Json::Value jsonData;
    int rs = SUCCESS;
    Json::Value response;

    EXPECT_NO_FATAL_FAILURE(s_pHartAdaptor->initialise());

    jsonData["DeviceNodeAddress"] = 0;

    response = s_pHartAdaptor->connect(jsonData);

    //verifies member is present or not
    EXPECT_EQ(true, response.isMember(JSON_ITEM_ID));

    if (nullptr != s_pHartAdaptor)
    {
        // Send invalid method ID
        jsonData["ItemID"] = 27114;
        jsonData["ItemType"] = "METHOD";
        jsonData["ItemContainerTag"] = "";
        jsonData["ItemInDD"] = true;

        Json::Value response;

        response = s_pHartAdaptor->getItem(jsonData);
        EXPECT_EQ(response["code"].asUInt(), Error::cpm.METHOD_EXEC_FAILED.code);
    }
    else
    {
        rs = FAILURE;
    }
    EXPECT_EQ(SUCCESS, rs);

}

