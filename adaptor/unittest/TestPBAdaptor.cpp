/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    git clone https://sadanandugare@bitbucket.org/flukept/digipro.git
    Authored By:       Sadanand Ugare
    Origin:            ProStar
*/

/** @file
    TestPBAdaptor class implementation
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////
#include <string>
#include "json/json.h"
#include "gtest/gtest.h"
#include "BusinessLogic.h"
#include "PBAdaptor.h"
#include "IFakController.h"
#include "PBParameterCache.h"
#include "PBConnectionManager.h"
#include "CommonProtocolManager.h"
#include "YAMLAppConfigParser.h"

using namespace Json;

#define PB_BUSPARAM "PB_BUS_PARAMS"
#define RESULT "result"
/////////////////////////////////////////////////////////////////////////////
// Global data
/////////////////////////////////////////////////////////////////////////////
static PBAdaptor* s_pPBAdaptor =  new PBAdaptor();

/////////////////////////////////////////////////////////////////////////////
// Following are the declarations of classes
/////////////////////////////////////////////////////////////////////////////
class TestPBAdaptor : public ::testing::Test
{
public:
  TestPBAdaptor()
  {
    /// perform initialization
      PB_BUS_PARAMS busParam;
  }

  ~TestPBAdaptor()
  {
    /// Perform cleanup
  }
  void readJsonData(Json::Value& UIData);
protected:
  virtual void SetUp()
  {
    /// Called just before the test executes
  }

  virtual void TearDown()
  {
    /// Called just after test execution
  }
};

// configures the default busparams
TEST_F (TestPBAdaptor, setBusParameters_defaultOne)
{

    Json::Value root, result;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    result =  CommonProtocolManager::getInstance()->setProtocolType(root);

    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    jsonData["PB_BUS_PARAMS"][SET_DEFAULT] = 1;

    actualJsonData = s_pPBAdaptor->setBusParameters(jsonData);

    EXPECT_EQ(expectedJsonStrData, actualJsonData["result"].asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pPBAdaptor->getBusParameters());

    uint32_t slotTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSL].asUInt();
    uint32_t minTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MIN_TSDR].asUInt();
    uint32_t maxTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_TSDR].asUInt();
    uint32_t quietTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TQUI].asUInt();
    uint32_t setTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSET].asUInt();
    uint32_t ttr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TTR].asUInt();
    uint32_t gapUFactor = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_G].asUInt();
    uint32_t HighStationAddr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_HSA].asUInt();
    uint32_t maxRetryLimit = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_RETRY_LIMIT].asUInt();

    //verifies the default bus parameters
    EXPECT_EQ(100, slotTime);
    EXPECT_EQ(11, minTsdr);
    EXPECT_EQ(60, maxTsdr);
    EXPECT_EQ(0, quietTime);
    EXPECT_EQ(1, setTime);
    EXPECT_EQ(65000, ttr);
    EXPECT_EQ(10, gapUFactor);
    EXPECT_EQ(126, HighStationAddr);
    EXPECT_EQ(1, maxRetryLimit);
}

// configures the BusParameters
TEST_F (TestPBAdaptor, setBusParameters_defaultZero)
{

    Json::Value root, result;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    result =  CommonProtocolManager::getInstance()->setProtocolType(root);

    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    jsonData[PB_BUSPARAM][SET_DEFAULT] = 0;
    jsonData[PB_BUSPARAM][PB_BUS_TSL]              = 600;
    jsonData[PB_BUSPARAM][PB_BUS_MIN_TSDR]         = 11;
    jsonData[PB_BUSPARAM][PB_BUS_MAX_TSDR]         = 60;
    jsonData[PB_BUSPARAM][PB_BUS_TQUI]             = 0;
    jsonData[PB_BUSPARAM][PB_BUS_TSET]             = 1;
    jsonData[PB_BUSPARAM][PB_BUS_TTR]              = 60000;
    jsonData[PB_BUSPARAM][PB_BUS_G]                = 0;
    jsonData[PB_BUSPARAM][PB_BUS_HSA]              = 2;
    jsonData[PB_BUSPARAM][PB_BUS_MAX_RETRY_LIMIT]  = 3;

    actualJsonData = s_pPBAdaptor->setBusParameters(jsonData);

    EXPECT_EQ(expectedJsonStrData, actualJsonData["result"].asString());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pPBAdaptor->getBusParameters());

    uint32_t slotTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSL].asUInt();
    uint32_t minTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MIN_TSDR].asUInt();
    uint32_t maxTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_TSDR].asUInt();
    uint32_t quietTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TQUI].asUInt();
    uint32_t setTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSET].asUInt();
    uint32_t ttr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TTR].asUInt();
    uint32_t gapUFactor = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_G].asUInt();
    uint32_t HighStationAddr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_HSA].asUInt();
    uint32_t maxRetryLimit = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_RETRY_LIMIT].asUInt();

    //verifies the default bus parameters
    EXPECT_EQ(600, slotTime);
    EXPECT_EQ(11, minTsdr);
    EXPECT_EQ(60, maxTsdr);
    EXPECT_EQ(0, quietTime);
    EXPECT_EQ(1, setTime);
    EXPECT_EQ(60000, ttr);
    EXPECT_EQ(0, gapUFactor);
    EXPECT_EQ(2, HighStationAddr);
    EXPECT_EQ(3, maxRetryLimit);
}

// configures the BusParameters
TEST_F (TestPBAdaptor, invalid_setBusParameters)
{

    Json::Value root, result;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    result =  CommonProtocolManager::getInstance()->setProtocolType(root);

    Json::Value jsonData, actualJsonData;
    std::string expectedJsonStrData("ok");

    jsonData[PB_BUSPARAM][SET_DEFAULT] = 0;
    jsonData[PB_BUSPARAM][PB_BUS_TSL]              = 600;
    jsonData[PB_BUSPARAM][PB_BUS_MIN_TSDR]         = 11;
    jsonData[PB_BUSPARAM][PB_BUS_MAX_TSDR]         = 60;
    jsonData[PB_BUSPARAM][PB_BUS_TQUI]             = 0;
    jsonData[PB_BUSPARAM][PB_BUS_TSET]             = 1;
    jsonData[PB_BUSPARAM][PB_BUS_TTR]              = 60000;
    jsonData[PB_BUSPARAM][PB_BUS_G]                = 120;
    jsonData[PB_BUSPARAM][PB_BUS_HSA]              = 2;
    jsonData[PB_BUSPARAM][PB_BUS_MAX_RETRY_LIMIT]  = 8;

    actualJsonData = s_pPBAdaptor->setBusParameters(jsonData);

    EXPECT_NE(expectedJsonStrData, actualJsonData["result"].asString());
}


// Retreives the BusParameters
TEST_F (TestPBAdaptor, getBusParameters)
{
    Json::Value root, result;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    result =  CommonProtocolManager::getInstance()->setProtocolType(root);

    EXPECT_NO_FATAL_FAILURE(s_pPBAdaptor->initialise());

    Json::Value readJsonData;
    EXPECT_NO_FATAL_FAILURE(readJsonData = s_pPBAdaptor->getBusParameters());

    uint32_t slotTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSL].asUInt();
    uint32_t minTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MIN_TSDR].asUInt();
    uint32_t maxTsdr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_TSDR].asUInt();
    uint32_t quietTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TQUI].asUInt();
    uint32_t setTime = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TSET].asUInt();
    uint32_t ttr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_TTR].asUInt();
    uint32_t gapUFactor = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_G].asUInt();
    uint32_t HighStationAddr = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_HSA].asUInt();
    uint32_t maxRetryLimit = readJsonData[RESULT][PB_BUSPARAM][PB_BUS_MAX_RETRY_LIMIT].asUInt();

    //verifies the default bus parameters
    EXPECT_EQ(100, slotTime);
    EXPECT_EQ(11, minTsdr);
    EXPECT_EQ(60, maxTsdr);
    EXPECT_EQ(0, quietTime);
    EXPECT_EQ(1, setTime);
    EXPECT_EQ(65000, ttr);
    EXPECT_EQ(10, gapUFactor);
    EXPECT_EQ(126, HighStationAddr);
    EXPECT_EQ(1, maxRetryLimit);
}


// fetch BusParameters
TEST_F (TestPBAdaptor, fetchBusParameters)
{
    PB_BUS_PARAMS params;
    PMStatus stackStatus;

    Json::Value root, result;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    result =  CommonProtocolManager::getInstance()->setProtocolType(root);

    EXPECT_NO_THROW(stackStatus = s_pPBAdaptor->fetchBusParameters(&params));

    EXPECT_EQ(false, stackStatus.hasError());
}


TEST_F(TestPBAdaptor, testStartScan)
{
    Json::Value root, result;
    Json::Value jsonData;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    CommonProtocolManager::getInstance()->setProtocolType(root);

    EXPECT_NO_FATAL_FAILURE(s_pPBAdaptor->initialise());

    result = s_pPBAdaptor->startScan(jsonData);

    sleep(5);

    EXPECT_EQ("ok", result["result"].asString());

}

TEST_F(TestPBAdaptor, testStopScan)
{

    Json::Value root, result;
    Json::Value jsonData;
    root[JSON_ITEM_PROTOCOL] = "PROFIBUS";
    CommonProtocolManager::getInstance()->setProtocolType(root);

    EXPECT_NO_FATAL_FAILURE(s_pPBAdaptor->initialise());

    //starts scanning
    result = s_pPBAdaptor->startScan(jsonData);
    sleep(5);

    EXPECT_EQ("ok", result["result"].asString());

    Json::Value jsonResult;

    //stops scanning
    EXPECT_NO_FATAL_FAILURE(jsonResult = s_pPBAdaptor->stopScan());

    EXPECT_EQ("ok", jsonResult["result"].asString());
}

// scan for the device
TEST_F (TestPBAdaptor, testScanDevice)
{
    PB_BUS_PARAMS params;
    PMStatus stackStatus;

    params.g = 10;
    params.hsa = 126;
    params.maxRetryLimit = 2;
    params.maxTsdr = 60;
    params.minTsdr = 11;
    params.tqui = 0;
    params.tset = 1;
    params.tsl = 500;
    params.ttr = 65000;


    PBConnectionManager pbInstance;
    PBParameterCache paramCache;
    PBAdaptor pbAdaptor;
    IFakController iFakInstance;

    pbInstance.initialize(&iFakInstance, &paramCache);

    iFakInstance.initialize(&pbAdaptor, &pbInstance, &paramCache);

    //initialize stack
    stackStatus = pbInstance.initStack(&params, "", "/dev/ttyUSB0");
    EXPECT_EQ(false, stackStatus.hasError());

    stackStatus.clear();

    SCAN_DEV_TBL scanDevTable;

    //scan for profibus devices
    stackStatus = pbInstance.scanDevices(nullptr, &scanDevTable);

    EXPECT_EQ(false, stackStatus.hasError());
    EXPECT_EQ(false, scanDevTable.list.empty());

    //Check wheteher the device addresses are within the range
    for(auto item : scanDevTable.list)
    {
        EXPECT_LT(item.stationAddress, 126);
        EXPECT_GT(item.stationAddress, 0);
    }

    //clears scan list
    scanDevTable.list.clear();
}

