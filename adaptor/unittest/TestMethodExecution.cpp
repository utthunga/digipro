/*****************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro.git
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    Method Execution Unit Test
*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#include <iostream>
#include "gtest/gtest.h"
#include "CommonProtocolManager.h"
#include "IProtocolAdaptor.h"
#include "ConnectionManager.h"
#include "FFAdaptor.h"
#include "IConnectionManager.h"
#include "FFConnectionManager.h"
#include "DDSController.h"
#include "FFParameterCache.h"
#include "PMErrorCode.h"
#include "PMStatus.h"

using namespace std;

typedef struct _devinfo_TBL {
    DeviceInfo  deviceInfo;
    DeviceInfo* next;
} DEVICEINFO_TBL;

typedef struct _SCAN_DEVICEINFO_TBL{
    int             count;
    DEVICEINFO_TBL* list;

} SCAN_DEVICEINFO_TBL;

FFAdaptor *ffAdaptor;
DDSController* ddsController;
FFConnectionManager* ffConnectionManager;
FFParameterCache* ffParameterCache;

DeviceInfo ffDevices[10];
SCAN_DEV_TBL scanDevTable;
ParamInfo* pInfo;
int ScanCount;
DeviceInfo device;
PMStatus status;

class TestMethodExecution : public :: testing :: Test
{
public:

    TestMethodExecution()
    {
    }

    ~TestMethodExecution()
    {
    }

    void SetUp()
    {
        ffAdaptor = new FFAdaptor();
    }

    void TearDown()
    {
    }
};


TEST_F (TestMethodExecution, Initialize)
{
    Json::Value data;
    data = ffAdaptor->initialise();

    // Instantiate the Connection Manager, Parameter Cache & DDS Controller
    ffConnectionManager = ffAdaptor->m_pFFCm;
    ffParameterCache	= ffAdaptor->m_pFFPc;
    ddsController 	= ffAdaptor->m_pDds;
}

TEST_F (TestMethodExecution, Setup)
{
    ENV_INFO env_info;
    APP_INFO  app_info;

    memset(&scanDevTable, 0, sizeof(SCAN_DEV_TBL));
    memset(&app_info, 0, sizeof(APP_INFO));
    memset(&env_info, 0, sizeof(ENV_INFO));

    env_info.app_info = &app_info;
    int rs = 0;

    EXPECT_NO_THROW(status = ffConnectionManager->scanDevices(&env_info, &scanDevTable));
    if (status.hasError())
    {
        rs = FAILURE;
    }
    EXPECT_EQ(0, rs);

    SCAN_TBL* pList = scanDevTable.list;
    SCAN_TBL* temp = NULL;

    if (scanDevTable.count > 0)
    {
        ScanCount = 0;
        while(pList != NULL )
        {
            DeviceInfo deviceInfo;
            memset(&deviceInfo, 0, sizeof(DeviceInfo));

            EXPECT_NO_THROW(status = ffConnectionManager->getDeviceInfo(pList->station_address, &deviceInfo));
            if (status.hasError())
            {
                rs = FAILURE;
            }
            EXPECT_EQ(0, rs);
            if (rs == SUCCESS)
            {
                ffDevices[ScanCount++] = deviceInfo;
                device = deviceInfo;
            }
            temp = pList;
            pList = pList->next;
            free(temp);
        }
    }

    if (0 < ScanCount)
    {
        EXPECT_NO_THROW(status = ffConnectionManager->connectDevice(&ffDevices[0]));
        sleep(5);
        if (status.hasError())
        {
            rs = FAILURE;
        }
        EXPECT_EQ(SUCCESS, rs);
    }
}

TEST_F (TestMethodExecution, FetchMethods)
{
    int rs = SUCCESS;
    Json::Value jsonObject;

    if (nullptr != ffAdaptor)
    {
        jsonObject["ItemID"] = 0;
        jsonObject["ItemType"] = "METHOD";
        jsonObject["ItemContainerTag"] = "RES";
        jsonObject["ItemInDD"] = true;

        Json::Value response;

        response = ffAdaptor->getItem(jsonObject);

        Json::FastWriter writer;
        std::string str = writer.write(response);
        cout << str << endl;
    }
    else
    {
        rs = FAILURE;
    }
    EXPECT_EQ(SUCCESS, rs);
}

TEST_F (TestMethodExecution, ExecuteMethod)
{
    int rs = SUCCESS;
    Json::Value jsonObject;

    if (nullptr != ffAdaptor)
    {
        jsonObject["ItemID"] = "show_dd_version";
        jsonObject["ItemType"] = "METHOD";
        jsonObject["ItemContainerTag"] = "RES";
        jsonObject["ItemInDD"] = true;

        Json::Value response;

        response = ffAdaptor->getItem(jsonObject);
    }
    else
    {
        rs = FAILURE;
    }
    EXPECT_EQ(SUCCESS, rs);
}
