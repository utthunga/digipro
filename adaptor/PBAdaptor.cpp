/*******************************************************************************
    Copyright (c) 2017 Fluke Corporation, Inc. All rights reserved.
********************************************************************************

    Repository URL:    https://bitbucket.org/flukept/digipro
    Authored By:       Bharath SR
    Origin:            ProStar - Utthunga Technologies
*/

/** @file
    PB Adaptor class implementation
*/

/*******************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*******************************************************************************/

/* INCLUDE FILES **************************************************************/
#include <string>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include "PBAdaptor.h"
#include "json/json.h"
#include "JsonRpcUtil.h"
#include "CPMUtil.h"
#include "CommonProtocolManager.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"
#include "error/CpmErrors.h"
#include "edd_ast.h"

/* ========================================================================== */

/* Namespaces *****************************************************************/
using namespace std;

/* ========================================================================== */

/* Constants and Macros *******************************************************/

/* ========================================================================== */

/* Global Variables ***********************************************************/


std::vector<std::string> gRootMenu = { "device_root_menu",
                                      "diagnostic_root_menu",
                                      "maintenance_root_menu",
                                      "process_variables_root_menu"};

std::vector<std::string> gFallBackMenu = { "Menu_Main_Specialist",
                                          "Menu_Main_Maintenance" };

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*  ----------------------------------------------------------------------------
    Constructs PBAdaptor Class
*/
PBAdaptor::PBAdaptor()
    // TODO(Santhosh): Need to handle with threading mechanism implementation
   :m_pIFakController(nullptr),
    m_pPBConnectionManager(nullptr),
    m_pPBParameterCache(nullptr),
    m_pBusinessLogic(nullptr),
    m_devInfo(nullptr),
    m_unScanSeqID(0)
{
    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    // Set the Protocol Type
    m_Protocol = CPMPROTOCOL::CPMPROTOCOL_PROFIBUS;
}

/*  ----------------------------------------------------------------------------
    Destructor for PBAdaptor Class
*/
PBAdaptor::~PBAdaptor()
{
    if (nullptr != m_pIFakController)
    {
        delete m_pIFakController;
        m_pIFakController = nullptr;
    }

    if (nullptr != m_pPBConnectionManager)
    {
        delete m_pPBConnectionManager;
        m_pPBConnectionManager = nullptr;
    }

    if (nullptr != m_pPBParameterCache)
    {
        delete m_pPBParameterCache;
        m_pPBParameterCache = nullptr;
    }

    if (nullptr != m_pBusinessLogic)
    {
        delete m_pBusinessLogic;
        m_pBusinessLogic = nullptr;
    }
}

/*  ----------------------------------------------------------------------------
    Initializes Parameter Cache, Connection Manager and DDS Controller
*/
Json::Value PBAdaptor::initialise()
{
    PMStatus status;

    /* Creates all the required instances of DDS Controller, Parameter
       Cache and Connection manager and injects them to the required objects
    */
    Json::Value jsonResult;

    // Instantiate the Connection Manager, Parameter Cache & DDS Controller
    if (nullptr == m_pPBConnectionManager)
    {
        m_pPBConnectionManager = new PBConnectionManager;
    }

    if (nullptr == m_pPBParameterCache)
    {
        m_pPBParameterCache	= new PBParameterCache;
    }

    if (nullptr == m_pIFakController)
    {
        m_pIFakController = new IFakController;
    }

    // Check for improper initialisation
    if ((nullptr == m_pPBConnectionManager) ||
        (nullptr == m_pPBParameterCache) ||
        (nullptr == m_pIFakController))
    {
        Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;

        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Initialise IFak Controller with Connection Manger & Parameter cache
    m_pIFakController->initialize(this, m_pPBConnectionManager, m_pPBParameterCache);

     // Initialise Connection Manager with IFak Controller & Parameter Cache
    m_pPBConnectionManager->initialize(m_pIFakController, m_pPBParameterCache);

    // Initialise Parameter Cache with IFak Controller & Connection Manager
    m_pPBParameterCache->initialize(m_pIFakController, m_pPBConnectionManager, this);

    // Get subscripiton controller instance
    m_pSubscriptionController = m_pPBParameterCache->getSubscriptionController();

    // Prepare Bus Parameters
    status = m_pBusinessLogic->prepareBusParams();

    if (!status.hasError())
    {
        // get the stack configurable parameters
        status = getStackConfiguration();
        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP,"Failed to fetch bus params from Adaptor");
            Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }

        // Read PB attribue information such as dd path, port
        IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

        if (nullptr != pAppConfigParser)
        {
            m_pbDDLSourcePath = pAppConfigParser->getDeviceDescriptionPath();
            m_pbDeviceFile = pAppConfigParser->getDevicePort();
            m_internalSubscriptionRate = pAppConfigParser->getSubscriptionRate();
        }

        status = m_pPBConnectionManager->initStack(&m_busParams, &m_appSetting,
                                                   ((m_pbDDLSourcePath.length() > 0) ? m_pbDDLSourcePath.c_str() : nullptr),
                                                   ((m_pbDeviceFile.length() > 0) ? m_pbDeviceFile.c_str() : nullptr));

        if (status.hasError())
        {
            Error::Entry error = Error::cpm.PROTOCOL_INIT_FAILED;
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }

      
    mapItemMethods();

    // Set the protocol state as initialized
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_PROTOCOL_INITIALIZED);
    // Set language code state
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_LANGUAGE_CODE_SET);

    // Get the positive result json value
    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Shutsdown PB Adaptor
*/
Json::Value PBAdaptor::shutDown()
{
    PMStatus status;
    Json::Value jsonResult;

    if (nullptr != m_pPBConnectionManager)
    {
        status = m_pPBConnectionManager->shutDownStack();

        // For success condition
        if (!status.hasError())
        {
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_PROTOCOL_INITIALIZED);
            // Clear language code set state
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_LANGUAGE_CODE_SET);

            // Get the positive result json value
            jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
        }
        else
        {
            Error::Entry error = Error::cpm.PROTOCOL_SHUTDOWN_FAILED;
            jsonResult =  JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }
    else
    {
        Error::Entry error = Error::cpm.PROTOCOL_NOT_INITIALIZED;
        jsonResult =  JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Retrieves current active protocol
*/
CPMPROTOCOL PBAdaptor::getActiveProtocol()
{
    return m_Protocol;
}

/*  ----------------------------------------------------------------------------
    Gets device description header information
*/
Json::Value PBAdaptor::getDDHeaderInfo()
{
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Executes Pre Edit Action for Edit Display
*/
Json::Value PBAdaptor::execPreEditAction(const Json::Value& data)
{
    Json::Value jsonResult;
    
    Error::Entry error = Error::cpm.NO_PRE_EDIT_ACTION_CONFIG;
    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
   
    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Executes Post Edit Action for Edit Display
*/
Json::Value PBAdaptor::execPostEditAction(const Json::Value& data)
{
    Json::Value jsonResult;

    Error::Entry error = Error::cpm.NO_POST_EDIT_ACTION_CONFIG;
    jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Updates device description data
*/
Json::Value PBAdaptor::updateDD(const Json::Value& data)
{
    (void) data;
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Reads PB basic device data
*/
Json::Value PBAdaptor::getItem(const Json::Value& data)
{
    // Invoke the appropriate method and return the response
	return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::GET_ITEM));
}

/*  ----------------------------------------------------------------------------
   Writes data to PB basic device
*/
Json::Value PBAdaptor::setItem(const Json::Value& data)
{
    // Invoke the appropriate method and return the response
	return JsonRpcUtil::getResponseObject(executeMappedMethod(data, ITEM_QUERY::SET_ITEM));
}

/*  ----------------------------------------------------------------------------
    Executes respective mapped method through function pointer based on item type
*/
Json::Value PBAdaptor::executeMappedMethod
(
    const Json::Value& itemRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value response;

    // Fetch the corresponding function pointer from the map
    std::string itemType = itemRequest[JSON_ITEM_TYPE].asString();

    auto methodItr = processMethodMap.find(itemType);

    if (methodItr != processMethodMap.end())
    {
        // Method available inside map; execute it
        response = (*this.*(methodItr->second))(itemRequest, ITEMQUERY);
    }

    return response;
}

/*  ----------------------------------------------------------------------------
    Maps the processItem method function pointers to corresponding method key
*/
void PBAdaptor::mapItemMethods()
{
    //  Map the function pointer to respective ItemType String
    processMethodMap[JSON_MENU_ITEM_TYPE]     = &PBAdaptor::processMenu;
    processMethodMap[JSON_BLOCK_ITEM_TYPE]    = &PBAdaptor::processBlock;
}

/*  ----------------------------------------------------------------------------
    Processes menu information
*/
Json::Value PBAdaptor::processMenu(const Json::Value& menuRequest,
                                    const ITEM_QUERY ITEMQUERY)
{
    Json::Value menuResponse;
    (void)ITEMQUERY;

    // Validate the Item ID and Item Container Tag
    if (menuRequest.isMember(JSON_ITEM_ID) &&
        menuRequest[JSON_ITEM_ID].isIntegral() &&
        menuRequest[JSON_ITEM_ID].asLargestInt() >= 0)
    {

        if (SUCCESS == validateItemInDD(menuRequest, true))
        {
            unsigned long itemID;
            
            itemID = menuRequest[JSON_ITEM_ID].asLargestUInt();
            
            getDDMenu(itemID, menuResponse);

        }
        else
        {
            // For custom menu
            getCustomMenu(menuRequest, menuResponse);
        }
    }
    else
    {
        LOGF_ERROR(CPMAPP, "Invalid Item ID or Item Container Tag");
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        menuResponse = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return menuResponse;
}

/*  ----------------------------------------------------------------------------
    Processes the subscription for specified item
    Authored By: Utthunga
*/
Json::Value PBAdaptor::processSubscription(const Json::Value& subReq)
{
     Json::Value jsonResult;
     
     jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

     return jsonResult;     
}

/*  ----------------------------------------------------------------------------
    Returns Custom Menu information
*/
void PBAdaptor::getCustomMenu(const Json::Value& input, Json::Value& output)
{
    unsigned long itemID = input[JSON_ITEM_ID].asLargestUInt();
    if (itemID < CUSTOM_MENU_COUNT)
    {
        (this->*customMenu[itemID])(input, output);
    }
    else
    {
        // Invalid Request type; return immediately with error!!
        LOGF_ERROR(CPMAPP, "Invalid Item ID" << itemID);

        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
}

/*  ----------------------------------------------------------------------------
    Get Device Menu information
*/
void PBAdaptor::getDeviceMenu(const Json::Value & input, Json::Value & output)
{
    (void)input; // To remove Warning
    getTopLevelMenu(output);
}

/*  ----------------------------------------------------------------------------
    Get Advanced Device Menu information
*/
void PBAdaptor::getAdvancedDeviceMenu(const Json::Value& input,
                                      Json::Value& output)
{
    PMStatus status;

    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;

    // To Remove Clang Warning
    (void)input;

    auto blockItems = m_pIFakController->getItemByType(EDD_BLOCK);
	
    // Get the lsit of all the blocks in the device
    for (auto & blockItem : blockItems)
    {
        // Fill the Block information as menu items - Item Info
        sMenu.itemLabel = blockItem->label(nullptr);
        sMenu.itemID    = blockItem->id();
        sMenu.itemType  = JSON_BLOCK_ITEM_TYPE;
        sMenu.itemInDD  = TRUE;
        sMenu.itemContainerTag = blockItem->ident().str();

        itemList.push_back(sMenu);
    }

    // Set the attributs of the Menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.help;
    sMenu.itemContainerTag = "";

    // Now prepare the Json for the menu
    getMenuAsJson(sMenu, itemList, output);
}

/*  ----------------------------------------------------------------------------
    Get Block Level Menu information
*/
void PBAdaptor::getBlockMenu(const Json::Value& input, Json::Value& output)
{
    std::string blockTag;
    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;

    // Validate & retrieve the block container tag
    if (FAILURE == validateItemContTag(input, blockTag))
    {
        Error::Entry error = Error::cpm.INVALID_JSON_REQUEST;
        output = JsonRpcUtil::setErrorObject(error.code, error.message);
        return;
    }
    
    // Add the Advanced option as a menu item to the Block Level Menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_MENU_ADVANCED.help;
    sMenu.itemContainerTag = blockTag;

    itemList.push_back(sMenu);

    // Set the attributs of the menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_BLOCK_MENU.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_BLOCK_MENU.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_BLOCK_MENU.help;
    sMenu.itemContainerTag = blockTag;

    // Now prepare the Json for top level menu
    getMenuAsJson(sMenu, itemList, output);
}

/*  ----------------------------------------------------------------------------
    Get Advaced Block Menu information
*/
void PBAdaptor::getAdvancedBlockMenu(const Json::Value& input,
                                     Json::Value& output)
{
    ItemBasicInfo sMenu;
    std::list<ItemBasicInfo> itemList;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);

    // Add the inidividual menu items

    // Add the Block Parameter List option
    sMenu.itemID        = CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.id;
    sMenu.itemLabel     = CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.label;
    sMenu.itemHelp      = CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.help;
    itemList.push_back(sMenu);

    // Now prepare the Json for top level menu
    getMenuAsJson(sMenu, itemList, output);
}

/*  ---------------------------------------------------------------------------
    Fills all the required Json attributes for Custom Menu's
*/
void PBAdaptor::fillBasicMenuAttributes(const Json::Value& input,
                                        ItemBasicInfo& output)
{
    // Initialise all the common attributes for Menu
    output.itemType         = input[JSON_ITEM_TYPE].asString();
    output.itemInDD         = input[JSON_ITEM_IN_DD].asBool();
    output.itemID           = input[JSON_ITEM_ID].asLargestUInt();
    output.itemContainerTag = input[JSON_ITEM_CONT_TAG].asString();
}

/*  ----------------------------------------------------------------------------
    Get Block Parameter List Menu Information
*/
void PBAdaptor::getBlockParamListMenu(const Json::Value& input,
                                      Json::Value& output)
{
    ItemBasicInfo sMenu;

    // Fill the standard atributes for the menu
    fillBasicMenuAttributes(input, sMenu);
    sMenu.itemLabel     = CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.label;
    sMenu.itemHelp      = CustomMenu::info.CUSTOM_BLOCK_PARAM_LIST.help;

    // Add the menu information to output Json
    fillItemBasicJsonInfo(sMenu, output);

    // Get Block Param List
   // getBlockParams(sMenu.itemContainerTag, output);
}

/*  ----------------------------------------------------------------------------
     Start device scanning operation on Profibus network
*/
Json::Value PBAdaptor::startScan(const Json::Value& data)
{
    (void) data;
    Json::Value response;

    // create scan thread only if no scan thread exists
    if (THREAD_STATE_INVALID == m_pBusinessLogic->getScanThreadState())
    {
        // reset the scan sequence id
        m_unScanSeqID = 0;

        // create device scan thread
        m_pBusinessLogic->createScanThread();

        response = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        LOGF_ERROR(CPMAPP,"Scanning thread is already running");
        Error::Entry error = Error::cpm.START_SCAN_FAILED;
        response = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    return response;
}

/*  ----------------------------------------------------------------------------
    Stops device scanning operation on Profibus network
*/
Json::Value PBAdaptor::stopScan()
{
    Json::Value response;

    // destroy scan thread only if the existing thread is in running state
    if (THREAD_STATE_RUN == m_pBusinessLogic->getScanThreadState())
    {
        // destroy scan thread
        m_pBusinessLogic->destroyScanThread();
        response = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }
    else
    {
        LOGF_ERROR(CPMAPP, "No Scanning thread is available");
        Error::Entry error = Error::cpm.STOP_SCAN_FAILED;

        response = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return response;
}

/*  ----------------------------------------------------------------------------
    Scans Profibus network for PA devices
*/
PMStatus PBAdaptor::scanNetwork()
{
    // scan the network through pb stack APIs;
    m_sScanDevTable = {};

    PMStatus scanStatus;
    scanStatus = m_pPBConnectionManager->scanDevices(nullptr, &m_sScanDevTable);

    // can the network and prepare the scan device table for device data population
    if (!scanStatus.hasError())
    {
        scanStatus = populateDeviceInfo();

        // Populate device failed
        if (scanStatus.hasError())
        {
            if (PB_ADAPTOR_ERR_SCAN_THREAD_STOPPED == scanStatus.getErrorCode())
            {
                // Scan thread has been stopped terminate thread immediately
                // Clear scanning in progress state
                CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_SCANNING_IN_PROGRESS);
            }
        }
    }
    else
    {
        // scan device failed; inform scan thread to finish scanning mechanism
        Error::Entry error = Error::cpm.DEV_SCAN_FAILED;

        // prepare Json error response
        Json::Value jsondata = JsonRpcUtil::setErrorObject(error.code, error.message);

        // notify CPM with error infomation
        std::string scanPubStr(SCAN_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(scanPubStr, jsondata);
    }

    return scanStatus;
}

/*  ----------------------------------------------------------------------------
    Connects to PB basic device
*/
Json::Value PBAdaptor::connect(const Json::Value& data)
{
    PMStatus status;
    Json::Value root;
    uint32_t deviceAddress{0};

    if (data.isMember(JSON_DEV_ADDR) && data[JSON_DEV_ADDR].isIntegral())
    {
        deviceAddress = data[JSON_DEV_ADDR].asUInt();
    }

    if (deviceAddress < MIN_VALID_ADDRESS || deviceAddress > MAX_VALID_ADDRESS)
    {
        LOGF_ERROR(CPMAPP,"Invalid Device Address: " << deviceAddress);
        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Fill the DeviceInfo Structure
    m_devInfo = new DeviceInfo;
    m_devInfo->node_address = deviceAddress;

    // Storing this node number for disconnection notification purpose
    m_nodeAddress = deviceAddress;

    // Setting this state for disconnect notification
    CPMStateMachine::getInstance()->setCpmState(CPMSTATE_CONNECTION_IN_PROGRESS);

    // Create a lock here for disconnection Notification
    m_connectLock.lock();

    // Send Connect request to Connection Manager
    status = m_pPBConnectionManager->connectDevice(m_devInfo);

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Could Not Connect to the Device: "
                   << status.getErrorCode());

        // Clear the connection in progress state
        CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_CONNECTION_IN_PROGRESS);

        Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
        m_connectLock.unlock();
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        PMStatus createSubStatus;
        createSubStatus = createsInternalSubscription();

        if (!createSubStatus.hasError())
        {
            IAppConfigParser* pAppConfigParser = CommonProtocolManager::getInstance()->getAppConfigParser();

            if (nullptr != pAppConfigParser)
            {
                std::string imagePath = pAppConfigParser->getImagePath();

                // Creates a specific directory
                // If the directory already exists clear the content of the directory
                if (false == CPMUtil::createDirectory(imagePath))
                    CPMUtil::clearDirectory(imagePath);
            }

#ifdef HACK_CPM
            loadCustomDDBinaryFile(pAppConfigParser);
#endif

            // Get the top Level Menu for the device
            getTopLevelMenu(root);

            // Set state as connected
            CPMStateMachine::getInstance()->setCpmState(CPMSTATE_DEVICE_CONNECTED);
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_CONNECTION_IN_PROGRESS);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Error in preparing connection compenents : "
                       << status.getErrorCode());

            // Clear the connection in progress state
            CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_CONNECTION_IN_PROGRESS);

            // Disconnect the connected device in subscription failure case
            disconnect();

            Error::Entry error = Error::cpm.CONNECT_DEVICE_FAILED;
            m_connectLock.unlock();
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
    }

    m_connectLock.unlock();

    return JsonRpcUtil::getResponseObject(root);
}

#ifdef HACK_CPM
/* ----------------------------------------------------------------------------
    This function loads custom dd binary file
*/
void PBAdaptor::loadCustomDDBinaryFile(IAppConfigParser* pAppConfigParser)
{
     //#ifdef MICROCYBER
        std::string ddFile("TT106.ddl");
        std::string dictFile("MCdct.dct");
        std::string device("TT106");
    //#endif

    #ifdef FISHER
        std::string ddFile("001310370101_main.ddl");
        std::string dictFile("");
        std::string device("FISHER_CONTROLS");
    #endif

        std::string absoluteDDFile = pAppConfigParser->getDeviceDescriptionPath() + "/" + device + "/" +  ddFile;
        std::string absoluteDictFile = pAppConfigParser->getDeviceDescriptionPath() + "/" + device + "/" +  dictFile;
        std::vector<std::string> includes;
        std::vector<std::string> defines;

        std::string includePath;
        std::string definePath;

        definePath = includePath = pAppConfigParser->getDeviceDescriptionPath() + "/" + device;

        includes.push_back(includePath);
        defines.push_back(definePath);

        LOGF_INFO(CPMAPP, "DD Include Path: " << includePath);
        LOGF_INFO(CPMAPP, "Load dictionary file: " << absoluteDictFile);
        LOGF_INFO(CPMAPP, "Load dd file: " << absoluteDDFile);

        m_pIFakController->getIFakInterpreter()->set_pb_path(includePath);
        m_pIFakController->getIFakInterpreter()->load_dictionary(absoluteDictFile);
          //m_pIFakController->getIFakInterpreter()->parse(absoluteDDFile, includes, defines, edd_tree::PROFIBUS, edd_tree::PDM_MODE);
        m_pIFakController->getIFakInterpreter()->parse(absoluteDDFile, includes, defines);

        size_t errCount = m_pIFakController->getIFakInterpreter()->getErrorCount();

        if (errCount)
        {
            LOGF_ERROR (CPMAPP, "Errors/warnings during parsing - errorCount: " << errCount);
            return;
        }

        const edd::IDENTIFICATION_tree *identObj =
                        m_pIFakController->getIFakInterpreter()->identification();

        if (nullptr != identObj)
        {
            LOGF_INFO (CPMAPP, "Manufacturer_ID:  " << identObj->manufacturer());
            LOGF_INFO (CPMAPP, "Manufacturer_Name:  " << identObj->manufacturer_name());
            LOGF_INFO (CPMAPP, "DD_Revision:  " << identObj->dd_revision());
            LOGF_INFO (CPMAPP, "Device_Type:  " << std::hex<<(int)identObj->device_type());
            LOGF_INFO (CPMAPP, "Device_Type_Name:  " << identObj->device_type_name());
            LOGF_INFO (CPMAPP, "Device_Revision:  " << identObj->device_revision());
        }
}
#endif

/*  ----------------------------------------------------------------------------
    Disconnects from the PB basic device
*/
Json::Value PBAdaptor::disconnect()
{
    PMStatus status;

    m_connectLock.lock();
    //Before disconnect, delete all running subcription
    status = m_pPBParameterCache->deleteAllSubscription(SUBSCRIPTION_TYPE_NONE);

    if (!status.hasError())
    {
        // deletes the internal subscription table and items
        deleteSubscriptionTableItem();

        status = m_pPBConnectionManager->disconnectDevice();
        if (status.hasError())
        {
            LOGF_ERROR(CPMAPP, "Device Disconnect Failed: " << status.getErrorCode());
            Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
            m_connectLock.unlock();
            return JsonRpcUtil::setErrorObject(error.code, error.message);
        }
        if (nullptr != m_devInfo)
        {
            delete m_devInfo;
            m_devInfo = nullptr;
        }
    }

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP, "Device Disconnect Failed: " << status.getErrorCode());
        Error::Entry error = Error::cpm.DISCONNECT_DEVICE_FAILED;
        m_connectLock.unlock();
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    // Set state as Disconnected
    CPMStateMachine::getInstance()->clearCpmState(CPMSTATE_DEVICE_CONNECTED);

    // Clear edd tree objects from memory
    m_pIFakController->clearEDDTree();

    m_connectLock .unlock();
    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Populates the scanned device information
*/
PMStatus PBAdaptor::populateDeviceInfo()
{
    PMStatus populateStatus;

    // Get DeviceInformation from stack based on node address and prepare
    // corresponding device json information and call Notify()
    m_unScanSeqID++;

    // check whether the scan device table is empty
    if (m_sScanDevTable.list.size() > 0)
    {
        for (auto pList : m_sScanDevTable.list)
        {
            // check whether scanning operation is cancelled
            if (THREAD_STATE_STOP == m_pBusinessLogic->getScanThreadState())
            {
                /* scanning has been cancelled intentionally;
                   clear the exising device list
                   stop populating the device information */
                
                m_sScanDevTable.list.clear();
                populateStatus.setError(PB_ADAPTOR_ERR_SCAN_THREAD_STOPPED, PM_ERR_CL_PB_ADAPTOR);
                break;
            }
            DeviceInfo deviceInfo{};

            // fetch the device information
            populateStatus = m_pPBConnectionManager->getDeviceInfo(
                        pList.stationAddress,
                        &deviceInfo);

            if (!populateStatus.hasError())
            {
                // prepare device information in Json
                Json::Value jsondata;

                // prepare the corresponding JSON information and call notify
                jsondata[JSON_DEV_INFO][JSON_DEV_TAG] = deviceInfo.pd_tag;
                jsondata[JSON_DEV_INFO][JSON_DEV_ADDR] = deviceInfo.node_address;
                jsondata[JSON_DEV_INFO][JSON_DEV_ID_INFO] = deviceInfo.dev_id;

                jsondata[JSON_DEV_SCAN_SEQ_ID] = m_unScanSeqID;

                // notify CPM with device information
                std::string scanPubStr(SCAN_PUBLISHER);
                CommonProtocolManager::getInstance()->notifyMessage(scanPubStr,
                                                                    jsondata);
            }
            else
            {
                // fetching device information failed; set populate device info error and
                // continue populating the other devices in the list
                LOGF_ERROR(CPMAPP,"Failed to get device information");
                populateStatus.setError(PB_ADAPTOR_ERR_GET_DEV_INFO, PM_ERR_CL_PB_ADAPTOR);
            }
        }
    }
    else
    {
        // prepare device information in Json
        Json::Value jsondata;

        jsondata[JSON_DEV_SCAN_SEQ_ID] = m_unScanSeqID;

        // notify CPM with device information
        std::string scanPubStr(SCAN_PUBLISHER);
        CommonProtocolManager::getInstance()->notifyMessage(scanPubStr, jsondata);
    }

    return populateStatus;
}

/*  ----------------------------------------------------------------------------
    check if PB bus params are available
*/
bool PBAdaptor::isBusParamAvailable(const Json::Value& jsonData)
{
    return jsonData.isMember(JSON_PB_BUS_PARAMS);
}

/*  ----------------------------------------------------------------------------
    check if PB Application Settings are available or not.
*/
bool PBAdaptor::isAppSettingsAvailable(const Json::Value& jsonData)
{
    return jsonData.isMember(JSON_PB_APP_SETTINGS);
}

/*  ----------------------------------------------------------------------------
    Sets the Profibus network bus parameters
*/
Json::Value PBAdaptor::setBusParameters(const Json::Value& jsonData)
{
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    PB_BUSPARAM_STATUS pbStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;

    pbStatus = setBusParams(jsonData);

    if (pbStatus != PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
    {
        if (pbStatus == PB_BUSPARAM_STATUS::PB_BUSPARAM_DEFUALT_PARAM_FAILED)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Setting default params failed");
            error = Error::cpm.BUS_PARAM_SET_DEFUALT_PARAM_FAILED;
        }
        else if (pbStatus == PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Invalid bus parameters");
            error = Error::cpm.INVALID_BUS_PARAMS;
        }
        else if (pbStatus == PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_TSL)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Wrong input from USER for Slot time");
            error = Error::cpm.BUS_PARAM_INVALID_SLOT_TIME;
        }
        else
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Error in storing parameters");
            error = Error::cpm.SET_BUS_PARAM_FAILED;
        }

        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(Json::Value("ok"));
}

/*  ----------------------------------------------------------------------------
    Retrieves Profibus network bus parameters
*/
Json::Value PBAdaptor::getBusParameters()
{
    Json::Value root;
    Json::Value jsonData;
    PMStatus status;
    Error::Entry error = Error::cpm.UNKNOWN_ERROR;

    PB_BUSPARAM_STATUS getStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;

    // Gets the bus parameters from the PS in business logic layer.
    if (nullptr != m_pBusinessLogic)
    {
        status = m_pBusinessLogic->prepareBusParams();
    }

    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Failed to fetch bus params from persistant storage file");
        error = Error::cpm.GET_BUS_PARAM_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        getStatus = getBusParams(jsonData);
        if (getStatus == PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
        {
            if (jsonData.isMember(JSON_PB_BUS_PARAMS))
            {
                 root[JSON_PB_BUS_PARAMS] = jsonData[JSON_PB_BUS_PARAMS];
            }
            else
            {
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Pram is not member of list" );
                getStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_FAIL;
            }
        }
    }

    if (getStatus != PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Fetching bus param fail");
        error = Error::cpm.GET_BUS_PARAM_FAILED;
        return JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return JsonRpcUtil::getResponseObject(root);
}

/*  ----------------------------------------------------------------------------
    Sets PB communication failure
*/
void PBAdaptor::setCommunicationFailure()
{

}

/*  ----------------------------------------------------------------------------
    Processes device alerts
*/
void PBAdaptor::processAlerts(void * data)
{
    (void) data;
}

/*  ----------------------------------------------------------------------------
    to get the default PB bus parameters
*/
void PBAdaptor::getDefaultBusParameters(Json::Value &root)
{
    // updating json values stored in PS with default values
    ROOT[PB_BUS_TSL]             = DEFAULT_TSL;
    ROOT[PB_BUS_MIN_TSDR]        = DEFAULT_MIN_TSDR;
    ROOT[PB_BUS_MAX_TSDR]        = DEFAULT_MAX_TSDR;
    ROOT[PB_BUS_TQUI]            = DEFAULT_TQUI;
    ROOT[PB_BUS_TSET]            = DEFAULT_TSET;
    ROOT[PB_BUS_TTR]             = DEFAULT_TTR;
    ROOT[PB_BUS_G]               = DEFAULT_G;
    ROOT[PB_BUS_HSA]             = DEFAULT_HSA;
    ROOT[PB_BUS_MAX_RETRY_LIMIT] = DEFAULT_MAX_RETRY_LIMIT;
}

/*  ----------------------------------------------------------------------------
    Gives PB stack configuaration parameters
*/
PMStatus PBAdaptor::getStackConfiguration()
{
    PMStatus status;
    Json::Value root;

    // Gets the bus parameters from the PS.
    if (nullptr != m_pBusinessLogic)
    {
        status = m_pBusinessLogic->readConfigurationValues(root);
    }
    else
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: m_pBusinessLogic object is"
                              "null in fetchBusParameters");
        status.setError(PB_ADAPTOR_ERR_FETCHING_STACK_CONFIGURATION, PM_ERR_CL_PB_ADAPTOR);
    }

    if (!status.hasError())
    {
        if (root.isMember(PB_BUS_PARM_KEY) && isAppSettingsAvailable(root))
        {
            if (ROOT.isMember(PB_BUS_TSL))
            {
                m_busParams.tsl = ROOT[PB_BUS_TSL].asUInt();
            }

            if (ROOT.isMember(PB_BUS_MIN_TSDR))
            {
                m_busParams.minTsdr = ROOT[PB_BUS_MIN_TSDR].asUInt();
            }

            if (ROOT.isMember(PB_BUS_MAX_TSDR))
            {
                m_busParams.maxTsdr = ROOT[PB_BUS_MAX_TSDR].asUInt();
            }

            if (ROOT.isMember(PB_BUS_TQUI))
            {
                m_busParams.tqui = ROOT[PB_BUS_TQUI].asUInt();
            }

            if (ROOT.isMember(PB_BUS_TSET))
            {
                m_busParams.tset = ROOT[PB_BUS_TSET].asUInt();
            }

            if (ROOT.isMember(PB_BUS_TTR))
            {
                m_busParams.ttr = ROOT[PB_BUS_TTR].asUInt();
            }

            if (ROOT.isMember(PB_BUS_G))
            {
                m_busParams.g = ROOT[PB_BUS_G].asUInt();
            }

            if (ROOT.isMember(PB_BUS_HSA))
            {
                m_busParams.hsa = ROOT[PB_BUS_HSA].asUInt();
            }

            if (ROOT.isMember(PB_BUS_MAX_RETRY_LIMIT))
            {
                m_busParams.maxRetryLimit = ROOT[PB_BUS_MAX_RETRY_LIMIT].asUInt();
            }

            if (root[PB_APP_SET_KEY].isMember(PB_LOC_ADDR))
            {

                m_appSetting.address = root[PB_APP_SET_KEY][PB_LOC_ADDR].asUInt();
            }

        }
        else
        {
            status.setError(PB_ADAPTOR_ERR_GET_BUS_PARAMS_FAIL, PM_ERR_CL_PB_ADAPTOR);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Retrieves basic field device information
    Authored By: Utthunga
*/
Json::Value PBAdaptor::getFieldDeviceConfig(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Assigns user configured node address and Tag.
*/
Json::Value PBAdaptor::setFieldDeviceConfig(const Json::Value& jsonData)
{
    Json::Value jsonResult;

    jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Set application settings to persistent storage
*/
Json::Value PBAdaptor::setAppSettings(const Json::Value &jsonData)
{
    PMStatus status;
    Json::Value jsonResult;

    if (jsonData.isNull() || nullptr == m_pBusinessLogic)
    {
        LOGF_ERROR(CPMAPP,"APP_SET_ERROR: json or BusinessLogic object is"
                          " null in setAppSettings");
        status.setError(PB_ADAPTOR_ERR_JSON_PARSE, PM_ERR_CL_PB_ADAPTOR);
    }

    if (!status.hasError())
    {
        /* Set default app settings if root["set_default"] is
           set to 1 from UI
        */

        if (jsonData.isMember(PB_APP_SET_KEY) &&
                jsonData[PB_APP_SET_KEY].isMember(SET_DEFAULT) &&
                jsonData[PB_APP_SET_KEY][SET_DEFAULT].isIntegral() &&
                jsonData[PB_APP_SET_KEY][SET_DEFAULT].asLargestInt() > 0)
        {
            if (DEFAULT_PARAMS == jsonData[PB_APP_SET_KEY][SET_DEFAULT].asUInt())
            {
                setDefaultAppSettings();
            }
            else
            {
                status.setError(PB_ADAPTOR_ERR_INVALID_JSON_REQUEST, PM_ERR_CL_PB_ADAPTOR);
            }
        }
        else if (jsonData.isMember(PB_APP_SET_KEY) && jsonData[PB_APP_SET_KEY].isMember(PB_LOC_ADDR))
        {
            /* Assign and validating the address recived from
               user/UI.
            */
            if (jsonData[PB_APP_SET_KEY][PB_LOC_ADDR].isIntegral() &&
                jsonData[PB_APP_SET_KEY][PB_LOC_ADDR].asLargestInt() >= 0)
            {
                m_appSetting.address = jsonData[PB_APP_SET_KEY][PB_LOC_ADDR].asUInt();

                //Validates app settings
                status = validateAppSettings();
            }
            else
            {
                status.setError(PB_ADAPTOR_ERR_INVALID_JSON_REQUEST, PM_ERR_CL_PB_ADAPTOR);
            }
        }
        else
        {
            status.setError(PB_ADAPTOR_ERR_INVALID_JSON_REQUEST, PM_ERR_CL_PB_ADAPTOR);
        }
    }

    if (!status.hasError() && nullptr != m_pBusinessLogic)
    {
        Json::Value param;
        // Fetch App setting from persistence storage.
        status = m_pBusinessLogic->readConfigurationValues(param);
        if (!status.hasError())
        {
            // Update Json values with user address.
            param[PB_APP_SET_KEY][PB_LOC_ADDR] = m_appSetting.address;

            // To save the parameters in PS
            status = m_pBusinessLogic->saveConfigurationValues(param);
        }
    }

    if (status.hasError())
    {
        Error::Entry error = Error::cpm.SET_APP_SETTINGS_FAILED;
        jsonResult = JsonRpcUtil::setErrorObject(error.code, error.message);
    }
    else
    {
        jsonResult = JsonRpcUtil::getResponseObject(Json::Value("ok"));
    }

    return jsonResult;
}

/*  ----------------------------------------------------------------------------
    Get application settings from persistent storage
*/
Json::Value PBAdaptor::getAppSettings()
{
    PMStatus status;
    Json::Value returnData;
    Json::Value appSettings;

    if (nullptr == m_pBusinessLogic)
    {
        m_pBusinessLogic = new BusinessLogic(this);
    }

    // Gets the app settings from the PS in business logic layer.
    if (nullptr != m_pBusinessLogic)
    {
        //Preparing App settings and Bus parameters
        m_pBusinessLogic->prepareBusParams();

        status = m_pBusinessLogic->readConfigurationValues(returnData);
    }

    if (!status.hasError() )
    {
        /* Send only PB App settings Json structure to UI and ignore others
           as we use single Json file to store HART, FF and PB Bus Param & App settings.
        */
        for (Json::ValueConstIterator it = returnData.begin(); it != returnData.end(); ++it)
        {
            if (!it.key().isNull() && it.key().isString())
            {
                std::string key = it.key().asString().c_str();

                if (key == PB_APP_SET_KEY)
                {
                    appSettings[PB_APP_SET_KEY] = *it;
                    break;
                }
            }
        }

        // check if PB_APP_SETTINGS exists, otherwise set error
        if (!appSettings.isMember(PB_APP_SET_KEY))
        {
            LOGF_ERROR(CPMAPP,"PB_APP_SETTINGS Json structure is not available");
            status.setError(PB_ADAPTOR_ERR_APP_SETTINGS_NOT_AVAIL, PM_ERR_CL_PB_ADAPTOR);
        }
    }

    // return error if status is failure.
    if (!status.hasError())
    {
         returnData = JsonRpcUtil::getResponseObject(appSettings);
    }
    else
    {

        Error::Entry error = Error::cpm.GET_APP_SETTINGS_FAILED;
        returnData = JsonRpcUtil::setErrorObject(error.code, error.message);
    }

    return returnData;
}

/*  ----------------------------------------------------------------------------
   to get the default PB settings if the file is not present in persistent
   storage
*/
void PBAdaptor::getDefaultAppSettings(Json::Value &jsonData)
{
    // Assign default values to the PB app settings.
    jsonData[PB_APP_SET_KEY][PB_LOC_ADDR] = DEFAULT_LOC_ADDR;
}

/*  ----------------------------------------------------------------------------
    provide interface function to connection manager to pass
    the bus to PB stack
*/
PMStatus PBAdaptor::fetchBusParameters(void* params)
{
    Json::Value root;
    PMStatus status;

    PB_BUSPARAM_STATUS pbret = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;

    PB_BUS_PARAMS *parameters = static_cast<PB_BUS_PARAMS*>(params);

    // get bus params
    pbret = getBusParams(root);

    if (pbret != PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
    {
        status.setError(PB_ADAPTOR_ERR_GET_BUS_PARAMS_FAIL,
                        PM_ERR_CL_PB_ADAPTOR);

        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:failed to get PB bus params");
    }

    if (!status.hasError())
    {
        // updating the bus param structure with Bus param values in PS
        if (ROOT.isMember(PB_BUS_TSL))
        {
            parameters->tsl = ROOT[PB_BUS_TSL].asUInt();
        }
        if (ROOT.isMember(PB_BUS_MIN_TSDR))
        {
            parameters->minTsdr = ROOT[PB_BUS_MIN_TSDR].asUInt();
        }
        if (ROOT.isMember(PB_BUS_MAX_TSDR))
        {
            parameters->maxTsdr = ROOT[PB_BUS_MAX_TSDR].asUInt();
        }
        if (ROOT.isMember(PB_BUS_TQUI))
        {
            parameters->tqui = ROOT[PB_BUS_TQUI].asUInt();
        }
        if (ROOT.isMember(PB_BUS_TSET))
        {
            parameters->tset = ROOT[PB_BUS_TSET].asUInt();
        }
        if (ROOT.isMember(PB_BUS_TTR))
        {
            parameters->ttr = ROOT[PB_BUS_TTR].asUInt();
        }
        if (ROOT.isMember(PB_BUS_G))
        {
            parameters->g = ROOT[PB_BUS_G].asUInt();
        }
        if (ROOT.isMember(PB_BUS_HSA))
        {
            parameters->hsa = ROOT[PB_BUS_HSA].asUInt();
        }
        if (ROOT.isMember(PB_BUS_MAX_RETRY_LIMIT))
        {
            parameters->maxRetryLimit = ROOT[PB_BUS_MAX_RETRY_LIMIT].asUInt();
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    fetch bus parameters from persistence storage
*/
PB_BUSPARAM_STATUS PBAdaptor::getBusParams(Json::Value& root)
{
    PB_BUSPARAM_STATUS getParamStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;
    PMStatus status;
    int errorCode = SUCCESS;

    status = m_pBusinessLogic->readConfigurationValues(root);
    if (status.hasError())
    {
        errorCode = status.getErrorCode();
    }

    if ((errorCode == PB_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL) ||
       (errorCode == PB_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_OPEN_FAIL) ||
       (errorCode == PB_ADAPTOR_ERR_BUSPARAM_BL_GET_PARAM_PSI_READ_FAIL) ||
       (errorCode == PB_ADAPTOR_ERR_BUSPARAM_BL_JSON_PARSE_FAIL))
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:Get BUS Parameters fail from BL, CODE = " << errorCode)
        getParamStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_FAIL;
    }

    return getParamStatus;
}

/*  ----------------------------------------------------------------------------
    set the bus parameters value in the Persistence storage
*/
PB_BUSPARAM_STATUS PBAdaptor::setBusParams(const Json::Value& root)
{
    PB_BUSPARAM_STATUS pbStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;
    PMStatus status;

    //prepare bus parameters
    status = m_pBusinessLogic->prepareBusParams();

    //get the bus parameters from PS, update BUS_PARAM structure with UI values
    status = fetchBusParameters(static_cast<void*>(&m_busParams));
    if (status.hasError())
    {
        LOGF_ERROR(CPMAPP,"Failed to fetch bus params from persistant storage file");
        pbStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_GET_BUS_PARAMS_FAIL;
        return pbStatus;
    }


    // set default bus parameters if root["PB_BUS_PARAMS"]["set_default"] is set to 1 from UI
    if (ROOT.isMember(SET_DEFAULT) && ROOT[SET_DEFAULT].asLargestInt() > 0)
    {
        if (DEFAULT_PARAMS == ROOT[SET_DEFAULT].asUInt())
        {
            setDefaultBusParams();
            LOGF_INFO(CPMAPP,"BUS_PARAM_INFO:As SET_DEFAULT = 1, storing the default parameters in PS");
        }
    }
    else
    {
        // update BUS-PARAM with new UI_values and validate them
        pbStatus = validateBusParameters(root);
    }

    if (pbStatus == PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
    {
        // store bus params in PS
        pbStatus = storeBusParams(root);

        if (pbStatus != PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
        {
            LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:storing Parameters in Persistence storage failed");
            // send an error message to UI/USER
            pbStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_GET_BUS_PARAMS_FAIL;
        }
    }

    return pbStatus;
}

/*  ----------------------------------------------------------------------------
    validating and assigning the bus parameters values
*/
PB_BUSPARAM_STATUS PBAdaptor::validateBusParameters(const Json::Value& root)
{
    PB_BUSPARAM_STATUS fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;

    // Read & set Slot Time
    if (ROOT.isMember(PB_BUS_TSL))
    {
        if (ROOT[PB_BUS_TSL].isIntegral() &&
                (ROOT[PB_BUS_TSL].asLargestInt() >= 0))
        {
            unsigned int pbTsl = ROOT[PB_BUS_TSL].asUInt();
            if (pbTsl >= PB_BUS_TSL_MIN && pbTsl <= PB_BUS_TSL_MAX)
            {
                m_busParams.tsl = pbTsl;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_TSL;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_TSL, please use 37 or 16383");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Minimum station delay time response
    if (ROOT.isMember(PB_BUS_MIN_TSDR))
    {
        if (ROOT[PB_BUS_MIN_TSDR].isIntegral() &&
                (ROOT[PB_BUS_MIN_TSDR].asLargestInt() >= 0))
        {
            unsigned int minTsdr = ROOT[PB_BUS_MIN_TSDR].asUInt();
            if (minTsdr >= PB_BUS_MIN_TSDR_MIN && minTsdr <= PB_BUS_MIN_TSDR_MAX)
            {
                m_busParams.minTsdr = minTsdr;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_MIN_TSDR;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_MIN_TSDR, please use 11 or 1023");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Maximum station delay time response
    if (ROOT.isMember(PB_BUS_MAX_TSDR))
    {
        if (ROOT[PB_BUS_MAX_TSDR].isIntegral() &&
                (ROOT[PB_BUS_MAX_TSDR].asLargestInt() >= 0))
        {
            unsigned int maxTsdr = ROOT[PB_BUS_MAX_TSDR].asUInt();
            if (maxTsdr >= PB_BUS_MAX_TSDR_MIN  && maxTsdr <= PB_BUS_MAX_TSDR_MAX)
            {
                m_busParams.maxTsdr = maxTsdr;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_MAX_TSDR;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_MAX_TSDR, please use 37 or 65535");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_TQUI))
    {
        if (ROOT[PB_BUS_TQUI].isIntegral() &&
                (ROOT[PB_BUS_TQUI].asLargestInt() >= 0))
        {
            unsigned int tqui = ROOT[PB_BUS_TQUI].asUInt();
            if (tqui >= PB_BUS_TQUI_MIN && tqui <= PB_BUS_TQUI_MAX)
            {
                m_busParams.tqui = tqui;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_TQUI;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_TQUI, please use 0 or 493");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_TSET))
    {
        if (ROOT[PB_BUS_TSET].isIntegral() &&
                (ROOT[PB_BUS_TSET].asLargestInt() >= 0))
        {
            unsigned int tset = ROOT[PB_BUS_TSET].asUInt();
            if (tset >= PB_BUS_TSET_MIN && tset <= PB_BUS_TSET_MAX)
            {
                m_busParams.tset = tset;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_TSET;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_TSET, please use 1 or 494");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_TTR))
    {
        if (ROOT[PB_BUS_TTR].isIntegral() &&
               (ROOT[PB_BUS_TTR].asLargestInt() >= 0))
        {
            unsigned int ttr = ROOT[PB_BUS_TTR].asUInt();
            if (ttr >= PB_BUS_TTR_MIN && ttr <= PB_BUS_TTR_MAX)
            {
                m_busParams.ttr = ttr;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_TTR;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_TTR, please use 256 or 16776960");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_G))
    {
        if (ROOT[PB_BUS_G].isIntegral() &&
                (ROOT[PB_BUS_G].asLargestInt() >= 0))
        {
            unsigned int g = ROOT[PB_BUS_G].asUInt();
            if (g >= PB_BUS_G_MIN && g <= PB_BUS_G_MAX)
            {
                m_busParams.g = g;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_G;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_G, please use 0 or 100");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_HSA))
    {
        if (ROOT[PB_BUS_HSA].isIntegral() &&
                (ROOT[PB_BUS_HSA].asLargestInt() >= 0))
        {
            unsigned int hsa = ROOT[PB_BUS_HSA].asUInt();
            if (hsa >= PB_BUS_HSA_MIN  && hsa <= PB_BUS_HSA_MAX)
            {
                m_busParams.hsa = hsa;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_HSA;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_HSA, please use 1 or 126");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    // Read & set Quite time
    if (ROOT.isMember(PB_BUS_MAX_RETRY_LIMIT))
    {
        if (ROOT[PB_BUS_MAX_RETRY_LIMIT].isIntegral() &&
                (ROOT[PB_BUS_MAX_RETRY_LIMIT].asLargestInt() >= 0))
        {
            unsigned int maxRetryLimit = ROOT[PB_BUS_MAX_RETRY_LIMIT].asUInt();
            if (maxRetryLimit >= PB_BUS_MAX_RETRY_LIMIT_MIN && maxRetryLimit <= PB_BUS_MAX_RETRY_LIMIT_MAX)
            {
                m_busParams.maxRetryLimit = maxRetryLimit;
            }
            else
            {
                fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_WRONG_VALUE_FOR_MAX_RETRY_LIMIT;
                LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR:wrong value entered for PB_BUS_MAX_RETRY_LIMIT, please use 0 or 7");
                return fillValueStatus;
            }
        }
        else
        {
            LOGF_ERROR(CPMAPP, "BUS_PARAM_ERROR:Invalid bus param value");
            fillValueStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_INVALID_BUS_PARAMS;
            return fillValueStatus;
        }
    }

    return fillValueStatus;
}

/*  ----------------------------------------------------------------------------
    update Json values with user i/p, so can be store in persistence storage
*/
void PBAdaptor::modifyParams(Json::Value &root)
{
    //modifying PB stack param with user/UI given values
    ROOT[PB_BUS_TSL]             = m_busParams.tsl;
    ROOT[PB_BUS_MIN_TSDR]        = m_busParams.minTsdr;
    ROOT[PB_BUS_MAX_TSDR]        = m_busParams.maxTsdr;
    ROOT[PB_BUS_TQUI]            = m_busParams.tqui;
    ROOT[PB_BUS_TSET]            = m_busParams.tset;
    ROOT[PB_BUS_TTR]             = m_busParams.ttr;
    ROOT[PB_BUS_G]               = m_busParams.g;
    ROOT[PB_BUS_HSA]             = m_busParams.hsa;
    ROOT[PB_BUS_MAX_RETRY_LIMIT] = m_busParams.maxRetryLimit;
}

/*  ----------------------------------------------------------------------------
    store bus parameters in the persistence storage
*/
PB_BUSPARAM_STATUS PBAdaptor::storeBusParams(const Json::Value& root)
{
    PB_BUSPARAM_STATUS storeParamStatus = PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS;

    Json::Value busParams = root;

    // Get the bus paramaters already stored in persistent storage
    storeParamStatus = getBusParams(busParams);
    if (storeParamStatus != PB_BUSPARAM_STATUS::PB_BUSPARAM_SUCCESS)
    {
        LOGF_ERROR(CPMAPP,"BUS_PARAM_ERROR: Get Bus param fail in storeBusParams");
        storeParamStatus =  PB_BUSPARAM_STATUS::PB_BUSPARAM_FAIL;
        return storeParamStatus;
    }

    // Modify the existing bus parameters before save it to persistent storage
    modifyParams(busParams);

    // to save the parameters in PS
    PMStatus status = m_pBusinessLogic->saveConfigurationValues(busParams);
    if (status.hasError())
    {
        int errorCode = status.getErrorCode();
        if ((errorCode == PB_ADAPTOR_ERR::PB_ADAPTOR_ERR_BUSPARAM_BL_SAVE_PARAM_PSI_WRITE_FAIL) ||
            (errorCode == PB_ADAPTOR_ERR::PB_ADAPTOR_ERR_BUSPARAM_BL_INIT_PSI_FAIL))
        {
            storeParamStatus= PB_BUSPARAM_STATUS::PB_BUSPARAM_FAIL;
        }
    }

    return storeParamStatus;
}

/*  ----------------------------------------------------------------------------
    set/copy default app settings
*/
void PBAdaptor::setDefaultAppSettings()
{
    // updating json values stored in PS with default values
    m_appSetting.address = DEFAULT_LOC_ADDR;
}

/*  ----------------------------------------------------------------------------
    validates app settings
*/
PMStatus PBAdaptor::validateAppSettings()
{
    PMStatus status;

    // Validates the configuration against the user modified app settings
    if(m_appSetting.address < PB_APP_LOC_ADD_MIN || m_appSetting.address > PB_APP_LOC_ADD_MAX)
    {
        status.setError(PB_ADAPTOR_ERR_INVALID_LOCAL_MASTER_NODE_ADDRESS,
                        PM_ERR_CL_PB_ADAPTOR);
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    set/copy default bus parameters
*/
void PBAdaptor::setDefaultBusParams()
{
    // updating json values stored in PS with default values
    m_busParams.tsl           = DEFAULT_TSL;
    m_busParams.minTsdr       = DEFAULT_MIN_TSDR;
    m_busParams.maxTsdr       = DEFAULT_MAX_TSDR;
    m_busParams.tqui          = DEFAULT_TQUI;
    m_busParams.tset          = DEFAULT_TSET;
    m_busParams.ttr           = DEFAULT_TTR;
    m_busParams.g             = DEFAULT_G;
    m_busParams.hsa           = DEFAULT_HSA;
    m_busParams.maxRetryLimit = DEFAULT_MAX_RETRY_LIMIT;
}

/*  ----------------------------------------------------------------------------
    Creates internal subscription to update the cache table
*/
PMStatus PBAdaptor::createsInternalSubscription()
{
    PMStatus status;

    return status;
}

/*  ----------------------------------------------------------------------------
    Deletes all items from the internal subscription table
*/
PMStatus PBAdaptor::deleteSubscriptionTableItem()
{
    for (auto table : m_subscriptionCacheTable)
    {
        if (nullptr != table)
        {
            for (auto item : table->cacheItems)
            {
                if (nullptr != item.varType)
                {
                    delete item.varType;
                    item.varType = nullptr;
                }
            }

            delete table;
            table = nullptr;
        }
    }

    m_subscriptionCacheTable.clear();
}

/*  ----------------------------------------------------------------------------
    Processes Block information
*/
Json::Value PBAdaptor::processBlock
(
    const Json::Value& blockRequest,
    const ITEM_QUERY ITEMQUERY
)
{
    Json::Value blockResponse;
    (void)ITEMQUERY;

    // Get the Block information
    getBlockMenu(blockRequest, blockResponse);

    return blockResponse;
}

/*------------------------------------------------------------------------*/
/** Fills menu items Basic information into Json response

    @param locMenuList contains the list of menu items information

    @param jsonResp Json response to be filled

*/
PMStatus PBAdaptor::fillMenuItemsJson(
        edd::Interpreter::menuitems_t& locMenuList, Json::Value &output)
{
    PMStatus status;
    ItemBasicInfo menuInfo;
    Json::Value itemInfoJson;

    auto getJSONType = [](auto itemType)
     {
         switch(itemType) {
             case edd::EDD_VARIABLE:
               return  JSON_VARIABLE_ITEM_TYPE;
             case edd::EDD_MENU:
                return JSON_MENU_ITEM_TYPE;
             case edd::EDD_METHOD:
                return JSON_METHOD_ITEM_TYPE;
             case edd::EDD_IMAGE:
                return JSON_IMAGE_ITEM_TYPE;
             case edd::EDD_EDIT_DISPLAY:
                return JSON_MENU_ITEM_TYPE;
             default:
                LOGF_ERROR(CPMAPP, "invalid item type");
                return "";
            }
     };


    // For every item in the menu...
    for (auto &menu : locMenuList) {

        MENU_ITEM_tree* menuItem = menu.item;
            
        EDD_OBJECT_tree* eddObjTree = 
            dynamic_cast<EDD_OBJECT_tree *>(menu.ref);

        if ( menuItem == nullptr || eddObjTree == nullptr)
        {
            LOGF_ERROR(CPMAPP, "EDD object tree or Menu item instance is null!!");
            continue;
        }

        /* if Menu Item is a format like ROW_BREAK, COLBREAK, SEPARATOR,
           ignore the item */
        if (menuItem->is_format())
            continue;
 
        // if Menu Item qualifier is a HIDDEN, ignore the item
        if (menuItem->is_hidden())
        {
           LOGF_DEBUG(CPMAPP,
                   "Menu Item qualifier is HIDDEN, so skipping this menu Item ID : "
                   << eddObjTree->id()); 
           continue; 
        }
       
        // if menu item has validitiy and if it is not valid, ignore the item 
        if (eddObjTree->have_validity())
        {
            if(!eddObjTree->is_valid(nullptr))
            {
                continue;
            }
            else
            {
                LOGF_DEBUG(CPMAPP, "Validity is false, so skipping this menu Item : "
                        << eddObjTree->id());
            }
        }

        menuInfo.itemLabel = menuItem->label(nullptr);
        menuInfo.itemID = eddObjTree->id();
        menuInfo.itemHelp = menuItem->help(nullptr);

        menuInfo.itemType  = getJSONType(eddObjTree->kind);

        if (menuInfo.itemType == JSON_VARIABLE_ITEM_TYPE)
        {
            ParamItems varItems{};

            varItems.paramItemID = menuInfo.itemID;

            //TODO (Santhosh) : To enable the below line once the Variable is implemented
            //status = getVariable(varItems, itemInfoJson);

            if (!status.hasError())
            {
                fillItemBasicJsonInfo(menuInfo, itemInfoJson);
                // apply qualifiers for variables
                applyQualifiersForVariables(menuItem->qualifier(), itemInfoJson);
                // Append to the item List
                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
            }
        }
        else if (menuInfo.itemType == JSON_IMAGE_ITEM_TYPE)
        {           
            //TODO (Bharath) : To enable the below list once the Image is implemented
            //status = getImage( menuInfo.itemID, itemInfoJson);

            if (!status.hasError())
            {
                fillItemBasicJsonInfo(menuInfo, itemInfoJson);
                output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
            }
        }
        else if (menuInfo.itemType == JSON_MENU_ITEM_TYPE ||
                menuInfo.itemType == JSON_METHOD_ITEM_TYPE)
        {
            fillItemBasicJsonInfo(menuInfo, itemInfoJson);
            // Append to the item List
            output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemInfoJson);
        }
    }

    return status;
}

/*  ----------------------------------------------------------------------------
    Apply the qualifiers for Variables
*/
void PBAdaptor::applyQualifiersForVariables(unsigned short qualifier,
                                            Json::Value & itemJsonInfo)
{
    // Check for Read Only qualifier
    if (qualifier & EDD_MENU_ITEM_QUALIFIER_READ_ONLY)
    {
        itemJsonInfo[JSON_ITEM_READONLY] = true;
    }
    // Check for Label Qualifier
    if (qualifier & EDD_MENU_ITEM_QUALIFIER_NO_LABEL)
    {
        itemJsonInfo[JSON_ITEM_LABEL] = "";
    }
    // Check for Unit Qualifier
    if (qualifier & EDD_MENU_ITEM_QUALIFIER_NO_UNIT)
    {
        itemJsonInfo[JSON_VAR_UNIT] = "";
    }
}

/*  ----------------------------------------------------------------------------
    Returns DD Menu information
*/
PMStatus PBAdaptor::getDDMenu(unsigned long itemID, Json::Value & output)
{
    PMStatus status;
    ItemBasicInfo menuInfo;

     // get edd menu tree
    edd::MENU_tree* pMenuTree = m_pIFakController->getEDDMenuTree(itemID);

    if (NULL == pMenuTree)
    {
        LOGF_ERROR(CPMAPP, "DD Menu not available for ItemID : " << itemID);
        status.setError(PB_ADAPTOR_GET_DD_MENU_FAILED, PM_ERR_CL_PB_ADAPTOR);
        return status;
    }

    menuInfo.itemLabel = pMenuTree->label(nullptr);
    menuInfo.itemID = itemID;
    menuInfo.itemType = JSON_MENU_ITEM_TYPE;
    menuInfo.itemHelp = pMenuTree->help(nullptr);

    edd::Interpreter::menuitems_t menuItems;
    status = m_pIFakController->getEDDMenuItems(itemID, menuItems);

    if (!status.hasError())
    {
        fillMenuItemsJson(menuItems, output);
    }

    // Fill the attributes for the Menu
    fillItemBasicJsonInfo(menuInfo, output);
    
    return status;
}

/*  ----------------------------------------------------------------------------
    Returns top level menu for the given device
*/
void PBAdaptor::getTopLevelMenu(Json::Value &root)
{
    ItemBasicInfo sMenu;
    std::vector<std::string> availableRootMenu, availableFallBackMenu;
    std::list<ItemBasicInfo> itemList;
    std::vector<std::string> topMenuList;
    
    for(auto & menuName : gRootMenu)
    {
        if(nullptr != m_pIFakController->getItemByIdent(menuName))
        {
            availableRootMenu.push_back(menuName);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Root Menu not available : " << menuName);
        }
    }

    for(auto & menuName :  gFallBackMenu)
    {
       if(nullptr != m_pIFakController->getItemByIdent(menuName))
       {
           availableFallBackMenu.push_back(menuName);
       }
       else
       {
           LOGF_ERROR(CPMAPP, "Fall back Menu not available : " << menuName);
       }         
    }

    (availableRootMenu.size() > 0) ? topMenuList = availableRootMenu
            : topMenuList = availableFallBackMenu;

    for (auto & menuIdent : topMenuList)
    {
        edd::MENU_tree *menuTree = 
            dynamic_cast<edd::MENU_tree *>(m_pIFakController->getItemByIdent(menuIdent));

        if(nullptr != menuTree)
        {
            sMenu.itemLabel = menuTree->label(nullptr);
            sMenu.itemHelp = menuTree->help(nullptr);
            sMenu.itemID    = menuTree->is_edd_object()->id();
            sMenu.itemType  = JSON_MENU_ITEM_TYPE;
            sMenu.itemInDD  = TRUE;
            /* Since the device level menu's doesn't belong to any blocks
               the item container tag shall be empty */
            sMenu.itemContainerTag = "";

            // Add the menu item to the list
            itemList.push_back(sMenu);
        }
        else
        {
            LOGF_ERROR(CPMAPP, "Fetch Menu tree failed :" << menuIdent);
        }
    }
    // Add the Advanced option to the Top Level Menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU_ADVANCED.help;
    sMenu.itemContainerTag = "";
    itemList.push_back(sMenu);

    // Set the attributes of the top level menu
    sMenu.itemLabel = CustomMenu::info.CUSTOM_DEVICE_MENU.label;
    sMenu.itemID    = CustomMenu::info.CUSTOM_DEVICE_MENU.id;
    sMenu.itemType  = JSON_MENU_ITEM_TYPE;
    sMenu.itemInDD  = FALSE;
    sMenu.itemHelp  = CustomMenu::info.CUSTOM_DEVICE_MENU.help;
    sMenu.itemContainerTag = "";

    // Now prepare the Json for top level menu
    getMenuAsJson(sMenu, itemList, root);

}

/*  ----------------------------------------------------------------------------
    Returns menu in Json format for the given item list
*/
void PBAdaptor::getMenuAsJson(ItemBasicInfo & sMenuInfo,
                              list<ItemBasicInfo> & menuItems,
                              Json::Value & output)
{
    // Fill the attributes for the Menu
    fillItemBasicJsonInfo(sMenuInfo, output);

    // Fill the Menu Items one by one
    for (auto & menuItem : menuItems)
    {
        Json::Value itemNode;
        fillItemBasicJsonInfo(menuItem, itemNode);

        // Append to the item List
        output[JSON_ITEM_INFO][JSON_ITEM_LIST].append(itemNode);
    }
}

/*  ----------------------------------------------------------------------------
    Fills item basic attribute information into Json response
*/
void PBAdaptor::fillItemBasicJsonInfo(ItemBasicInfo& itemBasicInfo,
                                      Json::Value&   jsonResp)
{
    // Fill the basic attribute information into json response
    jsonResp[JSON_ITEM_LABEL] = itemBasicInfo.itemLabel;
    jsonResp[JSON_ITEM_HELP] = itemBasicInfo.itemHelp;
    jsonResp[JSON_ITEM_TYPE] = itemBasicInfo.itemType;
    jsonResp[JSON_ITEM_CONT_TAG] = itemBasicInfo.itemContainerTag;
    jsonResp[JSON_ITEM_ID] = static_cast<Json::Value::LargestUInt>(itemBasicInfo.itemID);
    jsonResp[JSON_ITEM_IN_DD] = itemBasicInfo.itemInDD;
}

/*  ----------------------------------------------------------------------------
    Validates Item in DD input
*/
int PBAdaptor::validateItemInDD
(   
    const Json::Value& jsonRequest,
    bool requiredState
)
{       
    int status = FAILURE;
    
    if (jsonRequest.isMember(JSON_ITEM_IN_DD) &&
        jsonRequest[JSON_ITEM_IN_DD].isBool())
    {
        if (requiredState == jsonRequest[JSON_ITEM_IN_DD].asBool())
        {   
            status = SUCCESS;
        }
    }
   
    return status;
}


/*  ----------------------------------------------------------------------------
    Validates Item Container Tag input
*/
int PBAdaptor::validateItemContTag
(
    const Json::Value& jsonRequest,
    std::string& itemContainerTag
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_ITEM_CONT_TAG) &&
        jsonRequest[JSON_ITEM_CONT_TAG].isString())
    {
        // Empty tag is also valid for local and device variables
        itemContainerTag = jsonRequest[JSON_ITEM_CONT_TAG].asString();
        status = SUCCESS;
    }
    return status;
}

/*  ----------------------------------------------------------------------------
    Validates Item ID input
*/
int PBAdaptor::validateItemID
(
    const Json::Value& jsonRequest,
    unsigned long& itemID
)
{
    int status = FAILURE;

    if ( jsonRequest.isMember(JSON_ITEM_ID) &&
         jsonRequest[JSON_ITEM_ID].isIntegral() &&
         (jsonRequest[JSON_ITEM_ID].asLargestInt() > 0))
    {
        itemID = jsonRequest[JSON_ITEM_ID].asLargestUInt();
        status = SUCCESS;
    }
    return status;
}


/*  ----------------------------------------------------------------------------
    Validates Item Container ID input
*/
int PBAdaptor::validateItemContID
(
    const Json::Value& jsonRequest,
    unsigned long& containerID
)
{
    int status = FAILURE;

    if (jsonRequest.isMember(JSON_VAR_CONT_ID))
    {
        if (jsonRequest[JSON_VAR_CONT_ID].isIntegral() &&
            (jsonRequest[JSON_VAR_CONT_ID].asLargestInt() >= 0))
        {
            containerID = jsonRequest[JSON_VAR_CONT_ID].asLargestUInt();
            status = SUCCESS;
        }
    }
    return status;
}
