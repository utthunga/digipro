
//#include "MethodInterpreterTests.ddl"

/*************************/
/* Unsupported Test Menu */
/*************************/

MENU the_unsupported_features
{
    STYLE MENU;
    LABEL "Unsupported Features";
    ITEMS
    {
        unsupported_variables,
        unsupported_charts,
        unsupported_sources,
        unsupported_graphs,
        unsupported_waveforms,
        unsupported_grids,
        unsupported_images,
        unsupported_string_literals,
        unsupported_edit_displays,
        unsupported_menus
    }
}

/* Unsupported Variables */

MENU unsupported_variables
{
    STYLE WINDOW;
    LABEL "Unsupported Variable Attributes";
    ITEMS
    {
        var_default_value,
        var_refresh_actions
    }
}

VARIABLE var_default_value
{
    LABEL "Var: DEFAULT_VALUE";
    HELP  "A Variable Testing the 'DEFAULT_VALUE' Attribute. The value should not be '375'.";
    TYPE INTEGER;
    CLASS LOCAL;
    DEFAULT_VALUE 375;
}

VARIABLE var_refresh_actions
{
    LABEL "Var: REFRESH_ACTIONS";
    HELP "A Variable Testing the 'REFRESH_ACTIONS' Attribute. You should not get an acknowledgement stating that 'A very simple method was called'.";
    TYPE INTEGER;
    CLASS LOCAL;
    REFRESH_ACTIONS {simple_method}
}


/* Unsupported Charts */

MENU unsupported_charts
{
    STYLE WINDOW;
    LABEL "Unsupported Charts Attributes";
    ITEMS
    {
        chart_height_width,
        chart_validity_true,
        chart_validity_false
    }
}

CHART chart_height_width
{
    LABEL "Chart: HEIGHT and WIDTH";
    HELP "A Chart Testing the HEIGHT and WIDTH Attributes.";
    HEIGHT X_LARGE;
    WIDTH XX_SMALL;
    TYPE STRIP;
    MEMBERS
    {
        PV_CHART1, source_pv_digitalvalue;
    }
}

CHART chart_validity_true
{
    LABEL "Chart: VALIDITY=TRUE";
    HELP "A Chart Testing the VALIDITY Attribute. VALIDITY = TRUE";
    VALIDITY TRUE;
    TYPE STRIP;
    MEMBERS
    {
        PV_CHART1, source_pv_digitalvalue;
    }
}

CHART chart_validity_false
{
    LABEL "Chart: VALIDITY=FALSE";
    HELP "A Chart Testing the VALIDITY Attribute. VALIDITY = FALSE";
    VALIDITY FALSE;
    TYPE STRIP;
    MEMBERS
    {
        PV_CHART1, source_pv_digitalvalue;
    }
}

/* Unsupported Sources */

MENU unsupported_sources
{
    STYLE WINDOW;
    LABEL "Unsupported Sources";
    ITEMS
    {
        chart_unsupported_sources
    }
}

CHART chart_unsupported_sources
{
    LABEL "Unsupported Source Attributes";
    HELP "Objective: Create or update DD with Sources attributes that are not supported.";
    TYPE STRIP;
    MEMBERS
    {
        PV_CHART1, source_emphasis;
        PV_CHART2, source_line_color;
        PV_CHART3, source_line_type;
        PV_CHART4, source_validity_true;
        PV_CHART5, source_validity_false;
        PV_CHART6, source_array_element;
    }
}

SOURCE source_emphasis
{
    LABEL "Source: EMPHASIS";
    HELP "A Source Testing the EMPHASIS Attribute.";
    EMPHASIS TRUE;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

SOURCE source_line_color
{
    LABEL "Source: LINE_COLOR";
    HELP "A Source Testing the LINE_COLOR Attribute.";
    LINE_COLOR 0xFFFFFF;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

SOURCE source_line_type
{
    LABEL "Source: LINE_TYPE";
    HELP "A Source Testing the LINE_TYPE Attribute.";
    LINE_TYPE DATA;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

SOURCE source_validity_true
{
    LABEL "Source: VALIDITY=TRUE";
    HELP "A Source Testing the VALIDITY Attribute. VALIDITY = TRUE";
    VALIDITY TRUE;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

SOURCE source_validity_false
{
    LABEL "Source: VALIDITY=FALSE";
    HELP "A Source Testing the VALIDITY Attribute. VALIDITY = FALSE";
    VALIDITY FALSE;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

SOURCE source_array_element
{
    LABEL "Source: VALIDITY=FALSE";
    HELP "A Source Testing the VALIDITY Attribute. VALIDITY = FALSE";
    VALIDITY FALSE;
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, unsupported_float_array[1], "array element source", "array element source Help" ;
    }
}

/* Unsupported Graphs */

MENU unsupported_graphs
{
    STYLE WINDOW;
    LABEL "Unsupported Graphs Attributes";
    ITEMS
    {
        unsupported_graphs_page1,
        unsupported_graphs_page2,
        unsupported_graphs_page3,
        unsupported_graphs_page4,
        unsupported_graphs_page5,
        unsupported_graphs_page6
    }
}
MENU unsupported_graphs_page1
{
    STYLE PAGE;
    LABEL "Height and Width";
    ITEMS
    {
        graph_height_width
    }
}
MENU unsupported_graphs_page2
{
    STYLE PAGE;
    LABEL "Validity";
    ITEMS
    {
        graph_validity_true,
        graph_validity_false
    }
}
MENU unsupported_graphs_page3
{
    STYLE PAGE;
    LABEL "File in Waveform";
    ITEMS
    {
        unsupported_graph_from_file
    }
}
MENU unsupported_graphs_page4
{
    STYLE PAGE;
    LABEL "List in WaveForm";
    ITEMS
    {
        unsupported_graph_from_list
    }
}

MENU unsupported_graphs_page5
{
    STYLE PAGE;
    LABEL "Array in WaveForm";
    ITEMS
    {   
	    unsupported_graph_from_array1
    }
}

MENU unsupported_graphs_page6
{
    STYLE PAGE;
    LABEL "ArrayElem in WaveForm";
    ITEMS
    {   
	    unsupported_graph_from_array2
    }
}

GRAPH graph_height_width
{
    LABEL "Graph: HEIGHT and WIDTH";
	HELP "A Graph Testing the HEIGHT and WIDTH Attributes.";
    HEIGHT XX_LARGE;
    WIDTH XX_SMALL;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, wave_xy;
	}
}

GRAPH graph_validity_true
{
    LABEL "Graph: VALIDITY=TRUE";
	HELP "A Graph Testing the VALIDITY Attribute. VALIDITY = TRUE";
    VALIDITY TRUE;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, wave_xy;
	}
}

GRAPH graph_validity_false
{
    LABEL "Graph: VALIDITY=FALSE";
	HELP "A Graph Testing the VALIDITY Attribute. VALIDITY = FALSE";
    VALIDITY FALSE;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, wave_xy;
	}
}

/* Unsupported Waveforms */

MENU unsupported_waveforms
{
    STYLE WINDOW;
    LABEL "WaveForms";
    ITEMS
    {
        unsupported_waveforms_graph
    }
}

GRAPH unsupported_waveforms_graph
{
    LABEL "Unsupported Waveforms Attributes";
    HELP "Objective: Create or update DD with Waveforms attributes that are not supported.";
    X_AXIS axis_0_3;
    MEMBERS
    {
        WAVE1, wave_emphasis;
        WAVE2, wave_handling;
        WAVE3, wave_key_points;
        WAVE4, wave_line_color;
        WAVE5, wave_line_type;
        WAVE6, wave_refresh_actions;
    }
}

WAVEFORM wave_emphasis
{
    LABEL "Wave: EMPHASIS";
    HELP "A Waveform Testing the EMPHASIS Attribute.";
    EMPHASIS TRUE;
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

WAVEFORM wave_handling
{
    LABEL "Wave: HANDLING";
    HELP "A Waveform Testing the HANDLING Attribute.";
    HANDLING READ&WRITE;
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

WAVEFORM wave_key_points
{
    LABEL "Wave: KEY_POINTS";
    HELP "A Waveform Testing the KEY_POINTS Attribute.";
    KEY_POINTS
	{
		X_VALUES { 1, 5, 9, 14}
		Y_VALUES { 1, 5, 9, 14}
	}
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

WAVEFORM wave_line_color
{
    LABEL "Wave: LINE_COLOR";
    HELP "A Waveform Testing the LINE_COLOR Attribute.";
    LINE_COLOR 0xC0C0C0C0;
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

WAVEFORM wave_line_type
{
    LABEL "Wave: LINE_TYPE";
    HELP "A Waveform Testing the LINE_TYPE Attribute.";
    LINE_TYPE DATA;
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

WAVEFORM wave_refresh_actions
{
    LABEL "Wave: REFRESH_ACTIONS";
    HELP "A Waveform Testing the REFRESH_ACTIONS Attribute.";
    REFRESH_ACTIONS {simple_method}
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

/* Unsupported Grids */

MENU unsupported_grids
{
    STYLE WINDOW;
    LABEL "Unsupported Grids Attributes";
    ITEMS
    {
        unsupported_grids_page1,
        unsupported_grids_page2
    }
}
MENU unsupported_grids_page1
{
    STYLE PAGE;
    LABEL "HeightWidth";
    ITEMS
    {
        grid_height_width
    }
}
MENU unsupported_grids_page2
{
    STYLE PAGE;
    LABEL "Array,List,File";
    ITEMS
    {
        grid_array_list_file
    }
}

GRID grid_height_width
{
	LABEL "Grid: HEIGHT and WIDTH";
    HEIGHT XX_SMALL;
    WIDTH XX_LARGE;
    VECTORS
	{
		{ "Column 1", 1, 4, 7 },
		{ "Column 2", 2, 5, 8 },
        { "Column 3", 3, 6, 9 }
	}
}
GRID grid_array_list_file
{
	LABEL "Grid: Invalid sources";
    HEIGHT MEDIUM;
    WIDTH MEDIUM;
    VECTORS
	{
		{ "Array", unsupported_float_array, unsupported_float_array[0] },
		{ "List", unsupported_x_float_list, unsupported_x_float_list[1] },
        { "File", unsupported_file_list, unsupported_file_list.XVALS }
	}
}

/* Unsupported Images */

MENU unsupported_images
{
    STYLE WINDOW;
    LABEL "Unsupported Images Attributes";
    ITEMS
    {
        image_link
    }
}

IMAGE image_link
{
	LABEL "Image: LINK";
    HELP "An Image Testing the LINK Attribute.";
    PATH "Smiley.GIF";
    LINK menu_bad;
}

/* Unsupported String Literals */

MENU unsupported_string_literals
{
	STYLE WINDOW;
    LABEL "Unsupported String Literals Attributes|en zz|You shouldn't see this!";
	ITEMS
	{
		var_zz
	}
}

VARIABLE var_zz
{
	LABEL "String Literals: ZZ Country Code|en zz|You should not see this.";
	HELP "The name is the String Literal.  The variable is nothing special.|en zz|You should not see this.";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{9, "If you see this, all is good.|en zz|You should not see this."}
	}
}

/* Unsupported Edit Displays */

MENU unsupported_edit_displays
{
	STYLE WINDOW;
    LABEL "Unsupported Edit Display Attributes";
	ITEMS
	{
		edit_display_items,
        edit_pre_edit_actions,
        edit_post_edit_actions
	}
}

EDIT_DISPLAY edit_display_items
{
     LABEL "Edit Display: DISPLAY_ITEMS";
     HELP "An Edit Display Testing the DISPLAY_ITEMS Attribute.";
     DISPLAY_ITEMS { var_zz }
     EDIT_ITEMS { var_misc, var_default_value, var_refresh_actions }
}

EDIT_DISPLAY edit_pre_edit_actions
{
     LABEL "Edit Display: PRE_EDIT_ACTIONS";
     HELP "An Edit Display Testing the PRE_EDIT_ACTIONS Attribute.";
     PRE_EDIT_ACTIONS { simple_method }
     EDIT_ITEMS { var_misc }
}

EDIT_DISPLAY edit_post_edit_actions
{
     LABEL "Edit Display: POST_EDIT_ACTIONS";
     HELP "An Edit Display Testing the POST_EDIT_ACTIONS Attribute.";
     POST_EDIT_ACTIONS { simple_method }
     EDIT_ITEMS { var_misc }
}

/* Unsupported Menus */

MENU unsupported_menus
{
    STYLE MENU;
    LABEL "Unsupported Menu Attributes";
    ITEMS
    {
        menu_style,
        menu_validity_true,
        menu_validity_false,
        menu_review ( REVIEW ),
        menu_display_value,
        menu_no_label,
        menu_no_unit,
        menu_inline,
        menu_columnbreak,
        menu_rowbreak,
	    menu_array_list_file,
	    menu_bad_command
    }
}

MENU menu_bad_command
{
    LABEL "Menu: Bad Command";
    STYLE WINDOW;
    ITEMS
    {
		unsupported_device_variable_response
    }
}
MENU menu_array_list_file
{
    LABEL "Menu: Array,List,and File Elements";
    STYLE WINDOW;
    ITEMS
    {
        unsupported_float_array[0],	/* ARRAY Element */
	unsupported_x_float_list[0],/* LIST Element */
	unsupported_file_list.XVALS, /* FILE Element */
        unsupported_array_validity,
        unsupported_list_validity
    }
}
MENU menu_style
{
    LABEL "Menu: STYLE";
    STYLE WINDOW;
    ITEMS
    {
        var_misc
    }
}
MENU menu_validity_true
{
    STYLE WINDOW;
    LABEL "Menu: VALIDITY=TRUE";
    VALIDITY TRUE;
    ITEMS
    {
        var_misc
    }
}
MENU menu_validity_false
{
    STYLE WINDOW;
    LABEL "Menu: VALIDITY=FALSE";
    VALIDITY FALSE;
    ITEMS
    {
        menu_bad
    }
}
MENU menu_review
{
    STYLE WINDOW;
    LABEL "Menu: REVIEW";
    ITEMS
    {
        var_misc
    }
}
MENU menu_display_value
{
    STYLE WINDOW;
    LABEL "Menu: DISPLAY_VALUE";
    ITEMS
    {
        var_misc (DISPLAY_VALUE),
	var_bit_enumerated (DISPLAY_VALUE)
    }
}
MENU menu_no_label
{
    STYLE WINDOW;
    LABEL "Menu: NO_LABEL";
    ITEMS
    {
        var_misc (NO_LABEL)
    }
}
MENU menu_no_unit
{
    STYLE WINDOW;
    LABEL "Menu: NO_UNIT";
    ITEMS
    {
        var_misc (NO_UNIT)
    }
}
MENU menu_inline
{
    STYLE WINDOW;
    LABEL "Menu: INLINE";
    ITEMS
    {
        image_misc (INLINE)
    }
}
MENU menu_columnbreak
{
    STYLE WINDOW;
    LABEL "Menu: COLUMNBREAK";
    ITEMS
    {
        var_misc,
        COLUMNBREAK,
        image_misc
    }
}
MENU menu_rowbreak
{
    STYLE WINDOW;
    LABEL "Menu: ROWBREAK";
    ITEMS
    {
        var_misc,
        ROWBREAK,
        image_misc
    }
}

/* Used Constructs */

MENU menu_misc
{
    STYLE WINDOW;
/*    LABEL "Miscellaneous Menu";*/
    ITEMS
    {
        var_misc
    }
}

MENU menu_bad
{
    STYLE WINDOW;
    LABEL "This is a bad menu.";
    ITEMS
    {
        var_misc
    }
}

IMAGE image_misc
{
    LABEL "Miscellaneous Image";
    PATH "Smiley.GIF";
}

METHOD simple_method
{
    LABEL "A Very Simple Method";
    DEFINITION
    {
        ACKNOWLEDGE("A very simple method was called.");
    }
}

VARIABLE var_misc
{
    LABEL "Local Float";
    CLASS LOCAL;
    TYPE FLOAT
    {
		MIN_VALUE -666.0;
		MAX_VALUE 10000.0;
	}
}

VARIABLE var_bit_enumerated
{
    LABEL "Bit_Enumerated Variable";
    CLASS LOCAL;
    TYPE BIT_ENUMERATED (4) {
        { 0x00000001, "Bit 1", "" },
	{ 0x00000002, "Bit 2", "" },
	{ 0x00000004, "Bit 4", "" },
	{ 0x00000100, "Bit 100", "" },
	{ 0x00000200, "Bit 200", "" },
	{ 0x00000400, "Bit 400", "" },
	{ 0x00000800, "Bit 800", "" },
	{ 0x00001000, "Bit 1000", "" },
	{ 0x00008000, "Bit 8000", "" }
    }
}

SOURCE source_pv_digitalvalue
{
    LABEL "PV Source";
    HELP "A source that reads the Digital Value of the PV.";
    Y_AXIS axis_pv_lrv_urv;
    MEMBERS
    {
        PV_SOURCE1, PV.DIGITAL_VALUE, "PVSource Descr", "PVSource Help" ;
    }
}

AXIS axis_pv_lrv_urv
{
    MIN_VALUE PV.LOWER_RANGE_VALUE - 1;
    MAX_VALUE PV.UPPER_RANGE_VALUE + 1;
}

AXIS axis_0_3
{
	MIN_VALUE 0;
    MAX_VALUE 3;
}

WAVEFORM wave_xy
{
    Y_AXIS axis_0_3;
    TYPE XY
	{
        X_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        Y_VALUES{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
        NUMBER_OF_POINTS 10;
    }
}

ARRAY unsupported_float_array
{
	LABEL "Unsupported Float Arrray";
	TYPE unsupported_float_value;
	NUMBER_OF_ELEMENTS 5;
}

VARIABLE unsupported_list_validity
{
    LABEL "Unsupported list validity";
    HELP  "The validity of this parameter is based on a list";
    VALIDITY IF( unsupported_x_float_list[0] == 0 ){TRUE;} ELSE {FALSE;}
    TYPE FLOAT;
    CLASS LOCAL;
    DEFAULT_VALUE 1.0;
}

VARIABLE unsupported_array_validity
{
    LABEL "Unsupported array validity";
    HELP  "The validity of this parameter is based on an array";
    VALIDITY IF( unsupported_float_array[0] == 0 ){TRUE;} ELSE {FALSE;}
    TYPE FLOAT;
    CLASS LOCAL;
    DEFAULT_VALUE 1.0;
}
VARIABLE unsupported_float_value
{
    LABEL "Unsupported float value";
    HELP  "A floating point value used in a list";
    TYPE FLOAT;
    CLASS LOCAL;
    DEFAULT_VALUE 1.0;
}
VARIABLE unsupported_int_value
{
    LABEL "Unsupported integer value";
    HELP  "A integer value used in a file";
    TYPE INTEGER;
    CLASS LOCAL;
    DEFAULT_VALUE 1;
}

LIST unsupported_x_float_list
{
    CAPACITY 3;
    TYPE unsupported_float_value;
}

LIST unsupported_y_float_list
{
    CAPACITY 3;
    TYPE unsupported_float_value;
}

WAVEFORM unsupported_float_array1_waveform
{
	LABEL "Array Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_float_array }
		Y_VALUES { unsupported_float_array }
		NUMBER_OF_POINTS 2;
	}
}

WAVEFORM unsupported_float_array2_waveform
{
	LABEL "Array Element Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_float_array[0], unsupported_float_array[1] }
		Y_VALUES { unsupported_float_array[0], unsupported_float_array[1] }
		NUMBER_OF_POINTS 2;
	}
}

WAVEFORM unsupported_float_list_waveform
{
	LABEL "List Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_x_float_list }
		Y_VALUES { unsupported_y_float_list }
		NUMBER_OF_POINTS 2;
	}
}

WAVEFORM unsupported_float_list2_waveform
{
	LABEL "List Elements Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_x_float_list[0], unsupported_x_float_list[1] }
		Y_VALUES { unsupported_y_float_list[0], unsupported_y_float_list[1] }
		NUMBER_OF_POINTS 2;
	}
}

FILE unsupported_file_list
{
	MEMBERS
	{
		XVALS, unsupported_float_value;
		YVALS, unsupported_float_value;
		NUMPTS, unsupported_int_value;
	}
}

WAVEFORM unsupported_float_file_waveform
{
	LABEL "File Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_file_list }
		Y_VALUES { unsupported_file_list }
		NUMBER_OF_POINTS unsupported_file_list.NUMPTS;
	}
}

WAVEFORM unsupported_float_file2_waveform
{
	LABEL "File Element Waveform";
	TYPE XY
	{
		X_VALUES { unsupported_file_list.XVALS }
		Y_VALUES { unsupported_file_list.YVALS }
		NUMBER_OF_POINTS unsupported_file_list.NUMPTS;
	}
}


GRAPH unsupported_graph_from_array1
{
    LABEL "Graph: With array waveform";
    HELP "A Graph Testing the use of a list in the waveform";
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    X_AXIS axis_0_3;
    MEMBERS
	{
		
		WAVE1, unsupported_float_array1_waveform;
	}
}
GRAPH unsupported_graph_from_array2
{
    LABEL "Graph: With array waveform";
    HELP "A Graph Testing the use of a list in the waveform";
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, unsupported_float_array2_waveform;
	}
}

GRAPH unsupported_graph_from_list
{
    LABEL "Graph: With list waveform";
	HELP "A Graph Testing the use of a list in the waveform";
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, unsupported_float_list_waveform;
		WAVE2, unsupported_float_list2_waveform;
	}
}

GRAPH unsupported_graph_from_file
{
    LABEL "Graph: With file waveform";
	HELP "A Graph Testing the use of a file in the waveform";
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    X_AXIS axis_0_3;
    MEMBERS
	{
		WAVE1, unsupported_float_file_waveform;
		WAVE2, unsupported_float_file2_waveform;
	}
}

VARIABLE unsupported_device_variable_response
{
    LABEL "Var: Bad Command";
    HELP  "The command associated with this parameter contains an array element";
    TYPE INTEGER;
    CLASS DEVICE;
}

COMMAND unsupported_command
{
	NUMBER 666;
	OPERATION READ;
	TRANSACTION
	{
		REQUEST
		{
			78
		}
		REPLY
		{
			unsupported_device_variable_response
			unsupported_float_array[0]
		}
	}
	RESPONSE_CODES
	{
		0, SUCCESS, [no_command_specific_errors];
		2, DATA_ENTRY_ERROR, [invalid_selection];
		5, MISC_ERROR, [too_few_data_bytes_recieved];
		7, MODE_ERROR, [in_write_protect_mode];
	}
}