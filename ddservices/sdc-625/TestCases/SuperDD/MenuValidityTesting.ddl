
/********************************************/
/*          EDDL VALIDITY TESTS             */
/********************************************/
MENU items_validity_test_menu
{
	STYLE MENU;
	LABEL "Validity Tests";
	ITEMS
	{
		validity_items_test_window,
		validity_child_window_menu
	}
}
MENU validity_items_test_window
{
	STYLE WINDOW;
	LABEL "Set Validity on Items";
	ITEMS
	{
		EDDLDiagnosticsValidity,
		EDDLPVValidity,
		EDDLMenuValidity,
		WindowsValidity,
		PagesValidity,
		GroupValidity,
		ParameterValidity
	}
}
VARIABLE EDDLMenuValidity
{
	LABEL "EDDLMenuValidity";
	HELP "Use this to display, or hide, the Conditional Menu";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "Show"}
		{1, "Hide"}
	}
}
VARIABLE WindowsValidity
{
	LABEL "WindowsValidity";
	HELP "Use this to display, or hide, the Conditional Window";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "Show"}
		{1, "Hide"}
	}
}
VARIABLE PagesValidity
{
	LABEL "2nd Page Validity";
	HELP "Use this to change the number of tabs on the 'Conditional Pages' window";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "Show"}
		{1, "Hide"}
	}
}
VARIABLE GroupValidity
{
	LABEL "GroupValidity";
	HELP "Use this to change the number of groups on the 'Variables' tab";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "Show"}
		{1, "Hide"}
	}
}
VARIABLE ParameterValidity
{
	LABEL "ParameterValidity";
	HELP "Use this to hide parameters on the 'Variables' tab";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "Valid"}
		{1, "Invalid"}
	}
}



MENU validity_child_window_menu
{
	STYLE MENU;
	VALIDITY IF ( EDDLMenuValidity == 0){ TRUE; } ELSE { FALSE; }
	LABEL "Conditional Child Window";
	ITEMS
	{
		has_validity_pages_window
	}
}
MENU has_validity_pages_window
{
	STYLE WINDOW;
	VALIDITY IF ( WindowsValidity == 0){ TRUE; } ELSE { FALSE; }
	LABEL "Conditional Pages";
	ITEMS
	{
		validity_variables_page,
		validity_methods_page
	}
}
MENU validity_variables_page
{
	STYLE PAGE;
	LABEL "Variables";
	ITEMS
	{
		non_validity_variables_group1,
		COLUMNBREAK,
		non_validity_variables_group2,
		COLUMNBREAK,
		validity_variables_group
	}
}
MENU non_validity_variables_group1
{
	STYLE GROUP;
	LABEL "Non-Conditional1";
	ITEMS
	{
		"This group should always be here"                  /* variable */
	}
}
MENU non_validity_variables_group2
{
	STYLE GROUP;
	LABEL "Non-Conditional2";
	ITEMS
	{
		"The following parameters may be invalid",                  /* variable */
		VariableTestingParameter
	}
}
VARIABLE VariableTestingParameter
{
	LABEL "Conditional Parameter";
	HELP "This Parameters validity is set on the first tab";
	VALIDITY IF ( ParameterValidity == 0){ TRUE; } ELSE { FALSE; }
	CLASS LOCAL;
	TYPE INTEGER;
}
MENU validity_variables_group
{
	STYLE GROUP;
	VALIDITY IF ( GroupValidity == 0){ TRUE; } ELSE { FALSE; }
	LABEL "Conditional Variables";
	ITEMS
	{
		"This group is conditional on GroupValidity"        /* variable */
	}
}
MENU validity_methods_page
{
	STYLE PAGE;
	VALIDITY IF ( PagesValidity == 0){ TRUE; } ELSE { FALSE; }
	LABEL "Conditional Methods";
	HELP "Pages Depend on previous page";
	ITEMS
	{
		"This Page Depends on previous page"
	}
}

