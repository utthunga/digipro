
/********************************************/
/*    EDDL CHART SIZE TESTS               */
/********************************************/
MENU variable_size_chart_test_window
{
	STYLE WINDOW;
	LABEL "Change Chart Sizes";
	ITEMS
	{
		chart_size_test_page
	}
}
MENU chart_size_test_page
{
	STYLE PAGE;
	LABEL "Change Chart Sizes";
	ITEMS
	{
		VariableSizeChartHeightCondition,
		VariableSizeChartWidthCondition,
		VariableSizeChartChartedValue,
		variable_size_show_chart_window
	}
}

VARIABLE VariableSizeChartChartedValue
{
	LABEL "Charted Value";
	HELP "Local Charted Value";
	VALIDITY TRUE;
	HANDLING READ & WRITE;
	CLASS LOCAL;
	TYPE FLOAT;
}

SOURCE VariableSizeChartChartedSource 
{
    LABEL "PV Source";
	HELP "PV Source help";
	VALIDITY TRUE;
/*    Y_AXIS pv_axis; */
	EMPHASIS TRUE;
	LINE_TYPE DATA;
	LINE_COLOR 0xFF00FF ;
/* one dynamic value referenced by a collection member name */
    MEMBERS 
    { 
		PV_SOURCE, VariableSizeChartChartedValue, "PVSource Descr", "PVSource Help" ;
	}
}

VARIABLE VariableSizeChartHeightCondition
{
	LABEL "ChartHeight";
	HELP "Change the height of the PV chart";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "XSMALL"}
		{1, "SMALL"}
		{2, "MEDIUM"}
		{3, "LARGE"}
		{4, "XLARGE"}
	}
}
VARIABLE VariableSizeChartWidthCondition
{
	LABEL "ChartWidth";
	HELP "Change the width of the PV chart";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{0, "X_SMALL"}
		{1, "SMALL"}
		{2, "MEDIUM"}
		{3, "LARGE"}
		{4, "X_LARGE"}
	}
}
MENU variable_size_show_chart_window
{
	STYLE WINDOW;
	LABEL "Change Chart Sizes";
	ITEMS
	{
		variable_size_chart_page_0_0,
		variable_size_chart_page_0_1,
		variable_size_chart_page_0_2,
		variable_size_chart_page_0_3,
		variable_size_chart_page_0_4,
		variable_size_chart_page_1_0,
		variable_size_chart_page_1_1,
		variable_size_chart_page_1_2,
		variable_size_chart_page_1_3,
		variable_size_chart_page_1_4,
		variable_size_chart_page_2_0,
		variable_size_chart_page_2_1,
		variable_size_chart_page_2_2,
		variable_size_chart_page_2_3,
		variable_size_chart_page_2_4,
		variable_size_chart_page_3_0,
		variable_size_chart_page_3_1,
		variable_size_chart_page_3_2,
		variable_size_chart_page_3_3,
		variable_size_chart_page_3_4,
		variable_size_chart_page_4_0,
		variable_size_chart_page_4_1,
		variable_size_chart_page_4_2,
		variable_size_chart_page_4_3,
		variable_size_chart_page_4_4
	}
}
MENU variable_size_chart_page_0_0
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 0 && VariableSizeChartWidthCondition ==0 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_0_0
    }     
}
MENU variable_size_chart_page_0_1
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 0 && VariableSizeChartWidthCondition == 1 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_0_1
    }     
}
MENU variable_size_chart_page_0_2
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 0 && VariableSizeChartWidthCondition == 2 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_0_2
    }     
}
MENU variable_size_chart_page_0_3
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 0 && VariableSizeChartWidthCondition == 3 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_0_3
    }     
}
MENU variable_size_chart_page_0_4
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 0 && VariableSizeChartWidthCondition == 4 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_0_4
    }     
}
MENU variable_size_chart_page_1_0
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 1 && VariableSizeChartWidthCondition ==0 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_1_0
    }     
}
MENU variable_size_chart_page_1_1
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 1 && VariableSizeChartWidthCondition == 1 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_1_1
    }     
}
MENU variable_size_chart_page_1_2
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 1 && VariableSizeChartWidthCondition == 2 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_1_2
    }     
}
MENU variable_size_chart_page_1_3
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 1 && VariableSizeChartWidthCondition == 3 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_1_3
    }     
}
MENU variable_size_chart_page_1_4
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 1 && VariableSizeChartWidthCondition == 4 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_1_4
    }     
}
MENU variable_size_chart_page_2_0
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 2 && VariableSizeChartWidthCondition ==0 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_2_0
    }     
}
MENU variable_size_chart_page_2_1
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 2 && VariableSizeChartWidthCondition == 1 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_2_1
    }     
}
MENU variable_size_chart_page_2_2
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 2 && VariableSizeChartWidthCondition == 2 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_2_2
    }     
}
MENU variable_size_chart_page_2_3
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 2 && VariableSizeChartWidthCondition == 3 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_2_3
    }     
}
MENU variable_size_chart_page_2_4
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 2 && VariableSizeChartWidthCondition == 4 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_2_4
    }     
}
MENU variable_size_chart_page_3_0
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 3 && VariableSizeChartWidthCondition ==0 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_3_0
    }     
}
MENU variable_size_chart_page_3_1
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 3 && VariableSizeChartWidthCondition == 1 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_3_1
    }     
}
MENU variable_size_chart_page_3_2
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 3 && VariableSizeChartWidthCondition == 2 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_3_2
    }     
}
MENU variable_size_chart_page_3_3
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 3 && VariableSizeChartWidthCondition == 3 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_3_3
    }     
}
MENU variable_size_chart_page_3_4
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 3 && VariableSizeChartWidthCondition == 4 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_3_4
    }     
}
MENU variable_size_chart_page_4_0
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 4 && VariableSizeChartWidthCondition ==0 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_4_0
    }     
}
MENU variable_size_chart_page_4_1
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 4 && VariableSizeChartWidthCondition == 1 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_4_1
    }     
}
MENU variable_size_chart_page_4_2
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 4 && VariableSizeChartWidthCondition == 2 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_4_2
    }     
}
MENU variable_size_chart_page_4_3
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 4 && VariableSizeChartWidthCondition == 3 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_4_3
    }     
}
MENU variable_size_chart_page_4_4
{
    LABEL "Chart";
	STYLE PAGE;
	VALIDITY IF( VariableSizeChartHeightCondition == 4 && VariableSizeChartWidthCondition == 4 ){TRUE;} ELSE {FALSE;}
    ITEMS 
    { 
		variable_size_chart_4_4
    }     
}
CHART variable_size_chart_0_0
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_SMALL;
    WIDTH X_SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_0_1
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_SMALL;
    WIDTH SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_0_2
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_SMALL;
    WIDTH MEDIUM;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_0_3
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_SMALL;
    WIDTH LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_0_4
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_SMALL;
    WIDTH X_LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}

CHART variable_size_chart_1_0
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH X_SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_1_1
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_1_2
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH MEDIUM;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_1_3
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_1_4
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH X_LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_2_0
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT MEDIUM;
    WIDTH X_SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_2_1
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT MEDIUM;
    WIDTH SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_2_2
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT MEDIUM;
    WIDTH MEDIUM;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_2_3
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT MEDIUM;
    WIDTH LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_2_4
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT MEDIUM;
    WIDTH X_LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_3_0
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT LARGE;
    WIDTH X_SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_3_1
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT LARGE;
    WIDTH SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_3_2
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT LARGE;
    WIDTH MEDIUM;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_3_3
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT LARGE;
    WIDTH LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_3_4
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT LARGE;
    WIDTH X_LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_4_0
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_LARGE;
    WIDTH X_SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_4_1
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_LARGE;
    WIDTH SMALL;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_4_2
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_4_3
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_LARGE;
    WIDTH LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}
CHART variable_size_chart_4_4
{
    LABEL "Variable Size Chart";
	HELP "This chart is of variable sizes";
    TYPE SWEEP;
    HEIGHT X_LARGE;
    WIDTH X_LARGE;
	VALIDITY TRUE;
    MEMBERS 
    { 
		PV_CHART1, VariableSizeChartChartedSource, "PV Chart Desc",  "PV Chart Help";
    }     
}

