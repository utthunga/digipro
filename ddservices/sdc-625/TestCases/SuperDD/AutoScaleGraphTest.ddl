


/********************************************/
/*      EDDL Auto Scaling Graph Tests       */
/********************************************/
MENU graph_autoscaling_graph_tests_window
{
	STYLE WINDOW;
	LABEL "Auto Scaling Graph Tests";
	ITEMS
	{
		graph_autoscaling_graph_tests_page1
	}
}


/*  PAGE 1   */

MENU graph_autoscaling_graph_tests_page1
{
	STYLE PAGE;
	LABEL "Variables";
	ITEMS
	{
		"This is used for Graphs 1,3&4",
		graphautoscale_point11,
		graphautoscale_point12,
		graphautoscale_point13,
		COLUMNBREAK,
		"This is used for Graphs 2&3",
		graphautoscale_point21,
		graphautoscale_point22,
		graphautoscale_point23,
		ROWBREAK,
		graph_autoscaling_graph_tests_execute
	}
}


VARIABLE graphautoscale_point11
{
	LABEL "Axis 1 Pt 1";
	HELP "First Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}
VARIABLE graphautoscale_point12
{
	LABEL "Axis 1 Pt 2";
	HELP "Second Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}
VARIABLE graphautoscale_point13
{
	LABEL "Axis 1 Pt 3";
	HELP "Third Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}
VARIABLE graphautoscale_point21
{
	LABEL "Axis 2 Pt 1";
	HELP "First Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}
VARIABLE graphautoscale_point22
{
	LABEL "Axis 2 Pt 2";
	HELP "Second Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}
VARIABLE graphautoscale_point23
{
	LABEL "Axis 2 Pt 3";
	HELP "Third Test point";
	CLASS LOCAL;
	TYPE FLOAT;
}



MENU graph_autoscaling_graph_tests_execute
{
	STYLE WINDOW;
	LABEL "Auto Scaling Graph Tests";
	ITEMS
	{
		graph_autoscaling_graph_tests_page2,
		graph_autoscaling_graph_tests_page3,
		graph_autoscaling_graph_tests_page4,
		graph_autoscaling_graph_tests_page5
	}
}


/*  PAGE 2   */

MENU graph_autoscaling_graph_tests_page2
{
	STYLE PAGE;
	LABEL "No Y Axis";
	ITEMS
	{
		graph_autoscale_no_y_axis
	}
}

GRAPH graph_autoscale_no_y_axis 
{
    LABEL "Graph Test with No Y Axis";
    HELP "This tests a graph with No-Axis";
    VALIDITY TRUE;
    X_AXIS graph_autoscale_x_axis;
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    MEMBERS 
	{ 
		WAVE, wave_autoscale_no_axis;
	}
}


WAVEFORM wave_autoscale_no_axis 
{
	LABEL "AutoScaled Graph - No Y Axis";
	HELP "XY Graph with no Y Axis";
    TYPE XY 
	{
		X_VALUES { 1, 2, 3 } 
		Y_VALUES { graphautoscale_point11, graphautoscale_point12, graphautoscale_point13 }
		NUMBER_OF_POINTS 3;
    }
}



/*  PAGE 3   */	
	
MENU graph_autoscaling_graph_tests_page3
{
	STYLE PAGE;
	LABEL "Single Y Axis";
	ITEMS
	{
		graph_autoscale_single_axis_no_min_max
	}
}	


GRAPH graph_autoscale_single_axis_no_min_max 
{
    LABEL "AutoScaled Graph single Y Axis";
    HELP "This tests a graph with a single Axis that has no min or max defined";
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    X_AXIS graph_autoscale_x_axis;
    MEMBERS 
	{ 
		WAVE1, wave_autoscale_single_axis_no_min_max;
	}
}


WAVEFORM wave_autoscale_single_axis_no_min_max 
{
	LABEL "AutoScaled Graph - No Min Max";
	HELP "Y Axis with No min or max defined";
	Y_AXIS wave_autoscale_no_min_max;
    TYPE XY 
	{
		X_VALUES { 1, 2, 3 } 
		Y_VALUES { graphautoscale_point21, graphautoscale_point22, graphautoscale_point23 }
		NUMBER_OF_POINTS 3;
    }
}

AXIS wave_autoscale_no_min_max 
{
	LABEL "No Min/Max";
	CONSTANT_UNIT "psi";
}

AXIS graph_autoscale_x_axis
{
	MIN_VALUE 0;
	MAX_VALUE 4 ;
}



/*  PAGE 4   */	

MENU graph_autoscaling_graph_tests_page4
{
	STYLE PAGE;
	LABEL "Multi Y Axis";
	ITEMS
	{
		graph_autoscale_multi_axis
	}
}


GRAPH graph_autoscale_multi_axis 
{
    LABEL "Graph Multi Axis AutoScaling";
    VALIDITY TRUE;
    X_AXIS graph_autoscale_x_axis;
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    MEMBERS 
	{ 
		WAVE1, wave_autoscale_no_axis;
		WAVE2, wave_autoscale_single_axis_no_min_max;
		WAVE3, wave_autoscale_multiaxis_1;
	}
}


WAVEFORM wave_autoscale_multiaxis_1 
{
	LABEL "Hard Coded 0-4 YAxis";
	HELP "Hard Coded waveform with values 1,1 2,2 & 2,3";
	Y_AXIS wave_autoscale_hardcoded_0_4;
    TYPE XY 
	{
		X_VALUES { 1, 2, 3 } 
		Y_VALUES { 1, 2, 3 }
		NUMBER_OF_POINTS 3;
    }
}

AXIS wave_autoscale_hardcoded_0_4 
{
	LABEL "Scaled 0-4";
	CONSTANT_UNIT "psi";
	MIN_VALUE 0;
	MAX_VALUE 4 ;
}

/*  PAGE 5   */	

MENU graph_autoscaling_graph_tests_page5
{
	STYLE PAGE;
	LABEL "No X or Y Axis";
	ITEMS
	{
		graph_autoscale_no_x_or_y_axis
	}
}

GRAPH graph_autoscale_no_x_or_y_axis 
{
    LABEL "Graph Test with No X or Y Axis";
    VALIDITY TRUE;
    HEIGHT X_LARGE;
    WIDTH MEDIUM;
    MEMBERS 
	{ 
		WAVE, wave_autoscale_no_axis;
	}
}



