/**********************/
/*GAUGE CHART VALIDITY*/
/**********************/

MENU EGRC39XXX_menu
{
	STYLE MENU;
	LABEL "EGRC39XXX_menu";
	VALIDITY TRUE;
	ITEMS
	{
		EGRC390XX_window
	}
}

/*********/
/*WINDOWS*/
/*********/

MENU EGRC390XX_window
{
	STYLE WINDOW;
	LABEL "EGRC390XX_window";
	ITEMS
	{
		EGRC39000_page
	}
}

MENU EGRC3901X_window
{
	STYLE WINDOW;
	LABEL "EGRC3901X_window";
	ITEMS
	{
		EGRC39010_page
	}
}

/*******/
/*PAGES*/
/*******/

MENU EGRC39000_page
{
	STYLE PAGE;
	LABEL "EGRC39000_page";
	HELP "Gauge Chart Validity";
	ITEMS
	{
		variable_gauge_validity,
		variable_gauge_source_valid,
		variable_gauge_value_validity,
		variable_gauge_value,
		variable_gauge_source_color_validity,
		variable_gauge_source_color,
		variable_gauge_green_band,
		variable_gauge_yellow_band,
		variable_gauge_red_band,
		variable_axis_max,
		variable_axis_min,
		EGRC3901X_window
	}
}

MENU EGRC39010_page
{
	STYLE PAGE;
	LABEL "EGRC39010_page";
	ITEMS
	{
		chart_gauge
	}
}

/********/
/*CHARTS*/
/********/

CHART chart_gauge
{
	LABEL "Gauge Chart";
	TYPE GAUGE;
	HEIGHT MEDIUM;
	WIDTH MEDIUM;
	VALIDITY
	IF(variable_gauge_validity == 0)
	{
		FALSE;
	}	
	ELSE
	{
		TRUE;
	}
	MEMBERS
	{
		member1, source_gauge_value;
		member2, source_gauge_red_zone;
		member3, source_gauge_yellow_zone;
		member4, source_gauge_green_zone;
	}
}

/***********/
/*VARIABLES*/
/***********/

VARIABLE variable_gauge_validity
{
	LABEL "Gauge Validity";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{1,"TRUE"},
		{0,"FALSE"}
	}
}

VARIABLE variable_gauge_source_valid
{
	LABEL "Source Validity";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{1,"TRUE"},
		{0,"FALSE"}
	}
}

VARIABLE variable_gauge_value_validity
{
	LABEL "Source Variable Validity";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{1,"TRUE"},
		{0,"FALSE"}
	}
}

VARIABLE variable_gauge_value
{
	LABEL "Gauge Chart Value:";
	CLASS LOCAL;
	TYPE FLOAT;
	VALIDITY
	IF (variable_gauge_value_validity == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
}


VARIABLE variable_gauge_green_band
{
	LABEL "Green Zone";
	CLASS LOCAL;
	TYPE FLOAT;
	VALIDITY
	IF (variable_gauge_value_validity == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
}
VARIABLE variable_gauge_yellow_band
{
	LABEL "Yellow Zone";
	CLASS LOCAL;
	TYPE FLOAT;
	VALIDITY
	IF (variable_gauge_value_validity == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
}
VARIABLE variable_gauge_red_band
{
	LABEL "Red Zone";
	CLASS LOCAL;
	TYPE FLOAT;
	VALIDITY
	IF (variable_gauge_value_validity == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
}

VARIABLE variable_gauge_source_color
{
	LABEL "Source Color:";
	CLASS LOCAL;
	VALIDITY
	IF (variable_gauge_source_color_validity == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
	TYPE ENUMERATED (4)
	{
		{0x000000,"BLACK"},
		{0xC0C0C0,"SILVER"},
		{0x808080,"GRAY"},
		{0xFFFFFF,"WHITE"},
		{0x800000,"MAROON"},
		{0xFF0000,"RED"},
		{0xFFA500,"ORANGE"},
		{0x800080,"PURPLE"},
		{0xFF00FF,"FUCHSIA"},
		{0x008000,"GREEN"},
		{0x00FF00,"LIME"},
		{0x808000,"OLIVE"},
		{0xFFFF00,"YELLOW"},
		{0x000080,"NAVY"},
		{0x0000FF,"BLUE"},
		{0x008080,"TEAL"},
		{0x00FFFF,"AQUA"}		
	}
}

VARIABLE variable_gauge_source_color_validity
{
	LABEL "Source Color Validity:";
	CLASS LOCAL;
	TYPE ENUMERATED
	{
		{1,"TRUE"},
		{0,"FALSE"}
	}
}

VARIABLE variable_axis_min
{
	LABEL "Axis Min";
	CLASS LOCAL;
	TYPE INTEGER;
}

VARIABLE variable_axis_max
{
	LABEL "Axis Max";
	CLASS LOCAL;
	TYPE INTEGER;
}

/*********/
/*SOURCES*/
/*********/

SOURCE source_gauge_value
{
	LABEL "Gauge Chart Value:";
	LINE_COLOR variable_gauge_source_color;
	Y_AXIS axis_gauge;
	VALIDITY
	IF (variable_gauge_source_valid == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
	MEMBERS
	{
		source2, variable_gauge_value;
	}
}


SOURCE source_gauge_red_zone
{
	LABEL "Red Zone";
	LINE_COLOR 0x008000;/*0xFF0000;red*/
	LINE_TYPE HIGH_LIMIT;
	Y_AXIS axis_gauge;
	VALIDITY
	IF (variable_gauge_source_valid == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
	MEMBERS
	{
		source, variable_gauge_red_band;
	}
}

SOURCE source_gauge_yellow_zone
{
	LABEL "Yelow Zone";
	LINE_COLOR 0xFF0000; /*0xFFFF00;yellow*/
	LINE_TYPE HIGH_LIMIT;
	Y_AXIS axis_gauge;
	VALIDITY
	IF (variable_gauge_source_valid == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
	MEMBERS
	{
		source, variable_gauge_yellow_band;
	}
}

SOURCE source_gauge_green_zone
{
	LABEL "Green Zone";
	LINE_COLOR 0xFFFF00;/*0x008000;green*/
	LINE_TYPE HIGH_LIMIT;
	Y_AXIS axis_gauge;
	VALIDITY
	IF (variable_gauge_source_valid == 0)
	{
		FALSE;
	}
	ELSE
	{
		TRUE;
	}
	MEMBERS
	{
		source, variable_gauge_green_band;
	}
}

/******/
/*Axes*/
/******/

AXIS axis_gauge 
{
    MIN_VALUE variable_axis_min;
	MAX_VALUE variable_axis_max;        
}