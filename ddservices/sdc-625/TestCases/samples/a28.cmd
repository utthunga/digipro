set ROOT=\work\engineering_testlibrary\trunk\Source\Lib

\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader -R -A %ROOT%A%1%.fmA
mv _RdOutW.log ddta-fma-R.out

\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader -R -8 %ROOT%A%1%.fm8
mv _RdOutW.log ddta-fm8-R.out

\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader  -A %ROOT%A%1%.fmA
mv _RdOutW.log ddta-fma.out
mv _RdLogW.log ddta-fma.log
mv _RdErrW.log ddta-fma.err

\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader  -8 %ROOT%A%1%.fm8
mv _RdOutW.log ddta-fm8.out
mv _RdLogW.log ddta-fm8.log
mv _RdErrW.log ddta-fm8.err
