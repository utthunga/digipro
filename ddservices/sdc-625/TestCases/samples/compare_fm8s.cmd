REM compare FM8 reader output from Ddt8 vs DdtA

set READER=h:\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader
set BASE=h:\Work\testlibrary\trunk\Source


set BIN=006025\e0c2\0104.fm8


%READER% -8 %BASE%\Lib8\%BIN%
mv _RdOutW.log ddt8-fm8-release.log

%READER% -8  %BASE%\LibA\%BIN%
mv _RdOutW.log ddta-fm8-release.log
