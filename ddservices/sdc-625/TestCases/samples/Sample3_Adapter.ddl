/*****************************************************************************
 *
 * File: Sample3_Adapter.ddl
 * Version: 4.1.0
 * Date: 04/24/10
 *
 *****************************************************************************
 * Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: This file is a sample DD intended to demonstrate the 
 *              development of a simple wireless adapter DD.  It supports HART 7, 
 *              Universal Commands, a few Common Practice Commands, and all the 
 *              WirelessHART commands.
 *
 *****************************************************************************
 */

/*****************************************************************************
 * The manufacturer line
 *****************************************************************************
 */
// Replace following line with your manufacturer and device type as defined in Devices.cfg
MANUFACTURER HCF, DEVICE_TYPE _SAMPLE3_ADAPTER, DEVICE_REVISION 1, DD_REVISION 1

/*****************************************************************************
 * Include Files
 *****************************************************************************
 */

#include "macros.h"
#include "methods.h"

/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */

/* 
 * Because the number of bytes in the response of event commands
 * is not pre-determined, the event commands should not be imported
 * at this time. Set to 1 to include all event items. 
 */
#define INCLUDE_EVENTS 0

/*****************************************************************************
 * Common Tables
 */

IMPORT STANDARD _TABLES, DEVICE_REVISION 20, DD_REVISION 4
{
    // Import all so that we get all device types for subdevices attached to the adapter.
    EVERYTHING;

    REDEFINITIONS
    {
        VARIABLE device_type
        {
            /* 
             * The type must be redefined if the Device Type is not listed
             * in Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0xf9f7 with your Expanded Device Type code and appropriate label
                { 0xf9f7,  "Sample3 - Wireless Adapter" }
            }
        }

        VARIABLE manufacturer_id
        {
            /*
             * The type must be redefined if the Manufacturer code is not listed
             * in the Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0x00f9 with your Manufacturer code and appropriate label
                { 0x00f9,  [HART_Communication_Foundation] } 
            }
        }

        VARIABLE private_label_distributor
        {
            /*
             * The type must be redefined if the Private Label code is not listed
             * in the Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0x00f9 with your manufactures id and appropriate label
                { 0x00f9,  [HART_Communication_Foundation] } 
            }
        }

        /* 
     * Remove the following if the device supports trends.  Corresponding
         * commands will also need to be imported from the COMMON_PRACTICE DD.
     */
        DELETE VARIABLE trend_digital_units;
        DELETE VARIABLE trend_classification;
        DELETE VARIABLE trend_control_codes;
        DELETE VARIABLE trend_control;

        // Commands 821,822 are optional for wireless devices - remove if device does not support
        DELETE VARIABLE network_access_mode;

        // Command 64,512 is optional for wireless devices - remove if device does not support
        DELETE VARIABLE wireless_module_device_type;
        DELETE VARIABLE wireless_module_manufacturer_id;

        // Command 89 is not typically supported by wireless adapters
        DELETE VARIABLE time_set;

#if !INCLUDE_EVENTS
        DELETE VARIABLE event_nofication_control_codes;
        DELETE VARIABLE event_status_codes;
        DELETE VARIABLE event_status;
        DELETE VARIABLE event_notification_control_n;
        DELETE VARIABLE device_status_mask;
        DELETE VARIABLE extended_fld_device_status_mask;
        DELETE VARIABLE standardized_status_0_mask;
        DELETE VARIABLE standardized_status_1_mask;
        DELETE VARIABLE standardized_status_2_mask;
        DELETE VARIABLE standardized_status_3_mask;
        DELETE VARIABLE analog_channel_saturated1_mask;
        DELETE VARIABLE analog_channel_fixed1_mask;
        DELETE VARIABLE device_status_latched_value;
        DELETE VARIABLE dev_operating_mode_mask;
        DELETE VARIABLE extended_fld_device_status_latched_value;
        DELETE VARIABLE standardized_status_0_latched_value;
        DELETE VARIABLE standardized_status_1_latched_value;
        DELETE VARIABLE standardized_status_2_latched_value;
        DELETE VARIABLE standardized_status_3_latched_value;
        DELETE VARIABLE analog_channel_saturated1_latched_value;
        DELETE VARIABLE analog_channel_fixed1_latched_value;
        DELETE VARIABLE dev_operating_mode_latched_value;
#endif
    }
}

/*****************************************************************************
 * Universal Commmands
 */

IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 7, DD_REVISION 2
{
    // All HART devices must support all Universal Commands
    EVERYTHING;

    REDEFINITIONS
    {
        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 3) 
         */
        COMMAND read_dynamic_variables_and_pv_current
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DAQ.ANALOG_VALUE,
                    PV.DEVICE_VARIABLE.DIGITAL_UNITS, PV.DEVICE_VARIABLE.DIGITAL_VALUE
                }
            }
        }

        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 8)
         */
        COMMAND read_dynamic_variable_classification
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DEVICE_VARIABLE.CLASSIFICATION,
                    250,
                    250,
                    250
                }
            }
        }

        // Remove the command 48 status bytes that are not supported by the device
        // See also command 48 related items in the common practice imports.
        DELETE VARIABLE device_specific_status_14;
        DELETE VARIABLE device_specific_status_15;
        DELETE VARIABLE device_specific_status_16;
        DELETE VARIABLE device_specific_status_17;
        DELETE VARIABLE device_specific_status_18;
        DELETE VARIABLE device_specific_status_19;
        DELETE VARIABLE device_specific_status_20;
        DELETE VARIABLE device_specific_status_21;
        DELETE VARIABLE device_specific_status_22;
        DELETE VARIABLE device_specific_status_23;
        DELETE VARIABLE device_specific_status_24;

        /* 
         * Redefine the command 48 status to match
         * the device implementation.  The same should
         * also be done for device_specific_status_0_mask
         * and device_specific_status_0_latched_value
         * example for 1 byte follows: 
         */
        VARIABLE device_specific_status_0
        {
            REDEFINE LABEL "Status example 1";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }

        // Remove the command 48 status bytes that are not supported by the adapter
        // See also command 48 related items in the common practice imports.
        COMMAND read_additional_device_status
        {
            REDEFINE TRANSACTION 0
            {
                REQUEST
                {}
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0, device_specific_status_1, device_specific_status_2,
                    device_specific_status_3, device_specific_status_4, device_specific_status_5,
        
                    extended_fld_device_status, 0x00, 
                    
                    standardized_status_0, standardized_status_1, analog_channel_saturated1,
                    standardized_status_2, standardized_status_3
                }
            }

            REDEFINE TRANSACTION 1
            {
                REQUEST
                {
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),
        
                    extended_fld_device_status (INFO), 0x00, 
                    
                    standardized_status_0 (INFO), standardized_status_1 (INFO), analog_channel_saturated1 (INFO),
                    standardized_status_2 (INFO), standardized_status_3 (INFO)
                }
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),
        
                    extended_fld_device_status (INFO), 0x00, 
                    
                    standardized_status_0 (INFO), standardized_status_1 (INFO), analog_channel_saturated1 (INFO),
                    standardized_status_2 (INFO), standardized_status_3 (INFO)
                }
            }
        }
    }
}

/*****************************************************************************
 * PV, when the primary variable is not mappable we can import the PV
 * standard DD to take care of the dynamic variable array, loop current
 * and ranging functions
 */

IMPORT STANDARD _PV, DEVICE_REVISION 1, DD_REVISION 1
{
    EVERYTHING;

    REDEFINITIONS
    {
        /*
         * Remove the device variables not used in this DD.
         */
        DELETE COLLECTION secondary_variable;
        DELETE COLLECTION tertiary_variable;
        DELETE COLLECTION quaternary_variable;

        ARRAY OF COLLECTION dynamic_variables
        {
            ELEMENTS
            {
                DELETE SECONDARY;
                DELETE TERTIARY;
                DELETE QUATERNARY;
            }
        }

        /* 
         * Most devices use the same data item for range units (command 15) and the sensor units.
         * when this is true the following should be added.  if your device uses a different variable
         * for range units you should reference it here 
         */
        COLLECTION OF VARIABLE scaling
        {
            MEMBERS
            {
                ADD RANGE_UNITS, deviceVariables[0].DIGITAL_UNITS, [range_units], [range_units_help];
            } 
        }
    }
}

/*****************************************************************************
 * Common Practice Commands
 */

IMPORT STANDARD _COMMON_PRACTICE, DEVICE_REVISION 9, DD_REVISION 2
{
    // For a wireless adapter, these are the minimal commands required (in the device) for compliance.
    // Commented items are required in the device, but not modeled in a DD
    COMMAND perform_self_test;                          // Command 41 
    COMMAND perform_device_reset;                       // Command 42 
    COMMAND write_number_of_response_preambles;         // Command 59
    COMMAND read_io_system_capabilities;                // Command 74
//  COMMAND poll_subdevice;                             // Command 75
    COMMAND read_subdev_id_summary;                     // Command 84
    COMMAND read_io_channel_stats;                      // Command 85
    COMMAND read_subdevice_statistics;                  // Command 86
    COMMAND write_io_system_master_mode;                // Command 87
    COMMAND write_io_system_retry_count;                // Command 88
    COMMAND read_real_time_clock;                       // Command 90
    COMMAND read_device_communication_statistics;       // Command 95
    COMMAND read_subdevice_burst_map;                   // Command 101
    COMMAND write_subdevice_burst_map;                  // Command 102
    COMMAND write_burst_period;                         // Command 103
    COMMAND write_burst_trigger;                        // Command 104
    COMMAND read_burst_mode_configuration;              // Command 105
    COMMAND flush_delayed_responses;                    // Command 106
    COMMAND write_burst_device_variables;               // Command 107
    COMMAND write_burst_command_number;                 // Command 108
    COMMAND burst_mode_control;                         // Command 109

#if INCLUDE_EVENTS
    COMMAND read_event_notification_summary;            // Command 115
    COMMAND write_event_notification_bit_mask;          // Command 116
    COMMAND write_event_notification_timing;            // Command 117
    COMMAND event_notification_control;                 // Command 118
    COMMAND acknowledge_event_notification;             // Command 119
#endif

    COMMAND read_country_code;                          // Command 512
    COMMAND write_country_code;                         // Command 513

    VARIABLE max_io_cards;                              // for Command 74
    VARIABLE max_channels_per_io_card;                  // for Command 74
    VARIABLE max_sub_dev_per_channel;                   // for Command 74
    VARIABLE devices_detected;                          // for Command 74
    VARIABLE max_dr;                                    // for Command 74
    VARIABLE retry_count;                               // for Command 74, 88
    VARIABLE current_date;                              // for Command 90
    VARIABLE current_time;                              // for Command 90
    VARIABLE last_clock_date;                           // for Command 90
    VARIABLE last_clock_time;                           // for Command 90
    VARIABLE STX_count;                                 // for Command 95
    VARIABLE ACK_count;                                 // for Command 95
    VARIABLE BACK_count;                                // for Command 95
    VARIABLE burst_subdevice_index;                     // for Command 101, 102
    VARIABLE event_subdevice_index;                     // for Command 101, 102
    VARIABLE subdevice_number;                          // for Command 84, 86
    LIST     subdevice_list;                            // for Command 84, 86
    VARIABLE burst_message_number;                      // for Command 103, 104, 105, 107, 108, 109
    VARIABLE total_number_burst_messages;               // for Command 105
    VARIABLE subdev_IO_card;                            // for collection subdev_identity
    VARIABLE subdev_channel;                            // for collection subdev_identity
    VARIABLE subdev_device_id;                          // for collection subdev_identity
    VARIABLE subdev_universal_command_revision;         // for collection subdev_identity
    VARIABLE subdev_long_tag;                           // for collection subdev_identity
    VARIABLE stx_sent_to_subdev;                        // for collection subdev_stats
    VARIABLE ack_received_from_subdev;                  // for collection subdev_stats
    VARIABLE back_received_from_subdev;                 // for collection subdev_stats
    VARIABLE io_channel_stx_sent;                       // for collection io_stats
    VARIABLE io_channel_ack_received;                   // for collection io_stats
    VARIABLE io_channel_ostx_received;                  // for collection io_stats
    VARIABLE io_channel_oack_received;                  // for collection io_stats
    VARIABLE io_channel_back_received;                  // for collection io_stats
    VARIABLE burst_command_number;                      // for collection burst_message_collection
    VARIABLE update_period;                             // for collection burst_message_collection
    VARIABLE max_update_period;                         // for collection burst_message_collection
    VARIABLE trigger_level;                             // for collection burst_message_collection

#if INCLUDE_EVENTS
    VARIABLE number_events_supported;                   // for Command 115
    VARIABLE time_of_first_unack_event;                 // for collection event_control
    VARIABLE event_notification_retry_time_event;       // for collection event_control
    VARIABLE max_update_time_event;                     // for collection event_control
    VARIABLE event_debounce_interval_event;             // for collection event_control
    VARIABLE device_specific_status_0_mask;             // for collection event_mask
    VARIABLE device_specific_status_1_mask;             // for collection event_mask
    VARIABLE device_specific_status_2_mask;             // for collection event_mask
    VARIABLE device_specific_status_3_mask;             // for collection event_mask
    VARIABLE device_specific_status_4_mask;             // for collection event_mask
    VARIABLE device_specific_status_5_mask;             // for collection event_mask

    /*
     * Adapters handle events on behalf of subdevices,
     * so ALL status masks must be imported so that
     * subdevices with any number of status bytes can
     * be supported.
     */
    VARIABLE device_specific_status_14_mask;            // for collection event_mask
    VARIABLE device_specific_status_15_mask;            // for collection event_mask
    VARIABLE device_specific_status_16_mask;            // for collection event_mask
    VARIABLE device_specific_status_17_mask;            // for collection event_mask
    VARIABLE device_specific_status_18_mask;            // for collection event_mask
    VARIABLE device_specific_status_19_mask;            // for collection event_mask
    VARIABLE device_specific_status_20_mask;            // for collection event_mask
    VARIABLE device_specific_status_21_mask;            // for collection event_mask
    VARIABLE device_specific_status_22_mask;            // for collection event_mask
    VARIABLE device_specific_status_23_mask;            // for collection event_mask
    VARIABLE device_specific_status_24_mask;            // for collection event_mask
    VARIABLE device_specific_status_0_latched_value;    // for collection event_report
    VARIABLE device_specific_status_1_latched_value;    // for collection event_report
    VARIABLE device_specific_status_2_latched_value;    // for collection event_report
    VARIABLE device_specific_status_3_latched_value;    // for collection event_report
    VARIABLE device_specific_status_4_latched_value;    // for collection event_report
    VARIABLE device_specific_status_5_latched_value;    // for collection event_report

    /*
     * Adapters handle events on behalf of subdevices,
     * so ALL status masks must be imported so that
     * subdevices with any number of status bytes can
     * be supported.
     */
    VARIABLE device_specific_status_14_latched_value;   // for collection event_report
    VARIABLE device_specific_status_15_latched_value;   // for collection event_report
    VARIABLE device_specific_status_16_latched_value;   // for collection event_report
    VARIABLE device_specific_status_17_latched_value;   // for collection event_report
    VARIABLE device_specific_status_18_latched_value;   // for collection event_report
    VARIABLE device_specific_status_19_latched_value;   // for collection event_report
    VARIABLE device_specific_status_20_latched_value;   // for collection event_report
    VARIABLE device_specific_status_21_latched_value;   // for collection event_report
    VARIABLE device_specific_status_22_latched_value;   // for collection event_report
    VARIABLE device_specific_status_23_latched_value;   // for collection event_report
    VARIABLE device_specific_status_24_latched_value;   // for collection event_report
    VARIABLE config_change_counter_latched_value;       // for collection event_report
    COLLECTION event_control;                           // for collection event
    COLLECTION event_mask;                              // for collection event
    COLLECTION event_report;                            // for collection event
    COLLECTION event;                                   // for ARRAY events
    VARIABLE eventNumber;                               // for Command 115, 116, 117, 118, 119
    ARRAY events;                                       // for Command 115, 116, 117, 118, 119
#endif

    VARIABLE constOne;                                  // for command 101,102
    VARIABLE constZero;                                 // for command 101,102
    VARIABLE subdevice_missing;                         // for command 101,102

    UNIT burst_trigger_units_relation;
    
    COLLECTION subdev_identity;                         // for collection sub_device
    COLLECTION subdev_stats;                            // for collection sub_device
    COLLECTION sub_device;                              // for LIST subdevice_list
    COLLECTION io_stats;                                // for LIST channel_list
    COLLECTION burst_message_collection;                // for ARRAY burst_messages

    ARRAY card0_channels;                               // for ARRAY io_card_list
    ARRAY OF ARRAY io_cards;                            // for Command 85
    VARIABLE IO_card_number;                            // for Command 85
    VARIABLE IO_channel_number;                         // for Command 85
    ARRAY burst_messages;                               // for Command 103, 104, 105, 107, 108, 109
    VARIABLE burst_variable_code;                       // for array burst_variable_codes
    ARRAY burst_variable_codes;                         // for Command 105, 107
    
    METHOD return_to_normal;
    METHOD warning_message;
    METHOD device_self_test;
    METHOD device_master_reset;

    REDEFINITIONS
    {
        // Define how many burst messages this adapter supports
        ARRAY burst_messages
        {
            // A minimum of 4 are required
            REDEFINE NUMBER_OF_ELEMENTS 4;
        }

        // Define how many events this adapter supports
#if INCLUDE_EVENTS
        ARRAY events
        {
            // A minimum of 2 are required
            REDEFINE NUMBER_OF_ELEMENTS 2;
        }
#endif

        ARRAY card0_channels
        {
            // How many IO channels exist on IO card
            REDEFINE NUMBER_OF_ELEMENTS 1;
        }

        // Define the list of commands that can be burst by the adapter (add any device specific commands too)
        VARIABLE burst_command_number
        {
            REDEFINE TYPE ENUMERATED (2)
            {
                {1,  [read_primary_variable], [pv_help] }, 
                {2,  [read_percent_range_and_current], [percent_rnge_help] }, 
                {3,  [read_dyn_vars_and_current], [all_process_var_help] }, 
                {9,  [read_device_variables_with_status], [read_device_variables_with_status_help] }, 
                {33, [read_device_variables],[read_device_variables_help] },
                {48, "Cmd 48: Read Additional Device Status", "Read Additional Device - This command reads additional device status information"}
                {178,"Cmd 178: My device specific command", "Command that performs a device specific action"}
            }
        }

        // If the time is not directly settable via command 89, make sure to mark these READ_ONLY
        VARIABLE last_clock_date
        {
            REDEFINE HANDLING READ;
        }
        VARIABLE last_clock_time
        {
            REDEFINE HANDLING READ;
        }

#if INCLUDE_EVENTS
        /* The event masks and reported values should be
           redefined similar to the command 48 byte
           redefinitions in the UNIVERSAL imports. */
        VARIABLE device_specific_status_0_mask
        {
            REDEFINE LABEL "Status example 1 Mask";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }

        VARIABLE device_specific_status_0_latched_value
        {
            REDEFINE LABEL "Latched Status example 1";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }
#endif
    }
}

IMPORT STANDARD _WIRELESS, DEVICE_REVISION 1, DD_REVISION 2
{
    EVERYTHING;

    REDEFINITIONS
    {
        // Commands 821,822 are optional for adapters - remove if device does not support
        DELETE COMMAND write_network_access_mode;
        DELETE COMMAND read_network_access_mode;

        // Commands 64,512 are optional for adapters - remove if device does not support
        DELETE VARIABLE wireless_module_transmitter_revision;
        DELETE VARIABLE wireless_module_software_revision;
        DELETE VARIABLE wireless_module_hardware_revision;
        DELETE COMMAND read_wireless_module_revision;
    }
}

// Common items for a wireless adapter (modify as needed for your particular device)

// Make sure the burst and event subdevice indicies are updated when the subdevice count changes
REFRESH subdev_refresh_relation
{
    devices_detected:
        burst_messages[0].SUB_DEVICE_MISSING,
        burst_messages[1].SUB_DEVICE_MISSING,
        burst_messages[2].SUB_DEVICE_MISSING,
        burst_messages[3].SUB_DEVICE_MISSING,
        burst_messages[0].SUB_DEVICE_MAPPING,
        burst_messages[1].SUB_DEVICE_MAPPING,
        burst_messages[2].SUB_DEVICE_MAPPING,
        burst_messages[3].SUB_DEVICE_MAPPING
#if INCLUDE_EVENTS
       ,events[0].EVENT_CONTROL.SUB_DEVICE_MISSING,
        events[1].EVENT_CONTROL.SUB_DEVICE_MISSING
        events[0].EVENT_CONTROL.SUB_DEVICE_MAPPING,
        events[1].EVENT_CONTROL.SUB_DEVICE_MAPPING
#endif
}

VARIABLE fl_batteryLife
{
    LABEL "Battery Life";
    HELP [digital_value_help];
    CLASS DEVICE & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryUSL
{
    LABEL [usl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryLSL
{
    LABEL [lsl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryDamping
{
    LABEL [damping_value];
    HELP [seconds_damping_value_help];
    HANDLING READ;
    CONSTANT_UNIT "s";
    TYPE FLOAT;
}

VARIABLE batteryMinSpan
{
    LABEL [minimum_span];
    HELP [minimum_span_help];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryUnits
{
    LABEL "Units";
    HELP [digital_units_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // Battery life only supports units of Days
        {53, "Days"}        // Standard table for [days] is d.  We want "Days".
    }
}

UNIT battery_relation
{
    batteryUnits:
        fl_batteryLife,
        batteryUSL,
        batteryLSL,
        batteryMinSpan
}

VARIABLE battery_data_quality
{
    LABEL [process_data_quality];
    HELP [process_data_quality_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        PROCESS_DATA_STATUS_CODES
    }
}

VARIABLE battery_limit_status
{
    LABEL [limit_status];
    HELP [limit_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        LIMIT_STATUS_CODES
    }
}

VARIABLE battery_family_status
{
    LABEL [family_status];
    HELP [family_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // Battery life is not part of a device family
        { 0x08, [more_device_family_status_available]}
    }
}

VARIABLE battery_classification
{   
    LABEL [classification];
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // Battery Life is not classified
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0)
    }
}

VARIABLE battery_family
{   
    LABEL [device_family];
    HELP [device_family_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // Battery Life is not in a family
        DEVICE_VARIABLE_FAMILY_CODE(250)
    }
}

VARIABLE battery_serialNum
{   
    LABEL [sensor_serial_number];
    HELP [sensor_serial_number_help];
    TYPE UNSIGNED_INTEGER(3); 
}

VARIABLE battery_update_period 
{
    LABEL "Update Period";
    TYPE TIME_VALUE;
    HANDLING READ;
}

VARIABLE battery_write_dev_var_code
{
    LABEL "Battery write dev var code";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

COLLECTION OF VARIABLE batteryLife
{
    LABEL "Battery Life";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              fl_batteryLife;
        DIGITAL_UNITS,              batteryUnits;
        DATA_QUALITY,               battery_data_quality;
        LIMIT_STATUS,               battery_limit_status;
        DEVICE_FAMILY_STATUS,       battery_family_status;

        CLASSIFICATION,             battery_classification;
        DEVICE_FAMILY,              battery_family;

        UPPER_SENSOR_LIMIT,         batteryUSL;
        LOWER_SENSOR_LIMIT,         batteryLSL;
        MINIMUM_SPAN,               batteryMinSpan;
        DAMPING_VALUE,              dummy_variable_float;
        UPDATE_TIME_PERIOD,         battery_update_period;
        SIMULATED,                  battery_write_dev_var_code;

        // Device Information
        SENSOR_SERIAL_NUMBER,       battery_serialNum;
    }
}

VARIABLE dummy_variable
{
    HANDLING READ;
    TYPE UNSIGNED_INTEGER;
}

VARIABLE dummy_variable_float
{
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE dummy_variable_unsigned_3
{
    HANDLING READ;
    TYPE UNSIGNED_INTEGER (3);
}

VARIABLE dummy_variable_time
{
    HANDLING READ;
    TYPE TIME_VALUE;
}

/* This collection is used to point to for all the
   device variables that are really not used, but
   required to be defined so that the burst variables
   indexes are valid. Referring to any items in this
   collection is not recommended. */
COLLECTION OF VARIABLE scratchData
{
    LABEL "Scratch Data";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              dummy_variable_float;
        DIGITAL_UNITS,              dummy_variable;
        DATA_QUALITY,               dummy_variable;
        LIMIT_STATUS,               dummy_variable;
        DEVICE_FAMILY_STATUS,       dummy_variable;

        CLASSIFICATION,             dummy_variable;
        DEVICE_FAMILY,              dummy_variable;

        UPPER_SENSOR_LIMIT,         dummy_variable_float;
        LOWER_SENSOR_LIMIT,         dummy_variable_float;
        MINIMUM_SPAN,               dummy_variable_float;
        DAMPING_VALUE,              dummy_variable_float;
        UPDATE_TIME_PERIOD,         dummy_variable_time;
        SIMULATED,                  dummy_variable;

        // Device Information
        SENSOR_SERIAL_NUMBER,       dummy_variable_unsigned_3;
    }
}

// Now model any device specific information contained in your adapter

/* 
 * This is the minimum set of members that any device variable collection must have
 * Define these for each device variable contained in your device, and then add to
 * the deviceVariables array.
 */
VARIABLE var0_value
{
    LABEL "Dev Var 0";
    HELP [digital_value_help];
    CLASS DEVICE & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_usl
{
    LABEL [usl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_lsl
{
    LABEL [lsl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_damping
{
    LABEL [damping_value];
    HELP [seconds_damping_value_help];
    HANDLING READ;
    CONSTANT_UNIT "s";
    TYPE FLOAT;
}

VARIABLE var0_minSpan
{
    LABEL [minimum_span];
    HELP [minimum_span_help];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_units
{
    LABEL "Units";
    HELP [digital_units_help];
    TYPE ENUMERATED
    {
        // List Units code that apply to your device variable
        UNITS_CODE(32),
        UNITS_CODE(33)
    }
}

UNIT var0_relation
{
    var0_units:
        var0_value,
        var0_usl,
        var0_lsl,
        var0_minSpan
}

VARIABLE var0_data_quality
{
    LABEL [process_data_quality];
    HELP [process_data_quality_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        PROCESS_DATA_STATUS_CODES
    }
}

VARIABLE var0_limit_status
{
    LABEL [limit_status];
    HELP [limit_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        LIMIT_STATUS_CODES
    }
}

VARIABLE var0_family_status
{
    LABEL [family_status];
    HELP [family_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // List applicable family status if appropriate
        { 0x08, [more_device_family_status_available]}
    }
}

VARIABLE var0_classification
{   
    LABEL [classification];
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // List the proper classification code
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0)
    }
}

VARIABLE var0_family
{   
    LABEL [device_family];
    HELP [device_family_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // List proper family code
        DEVICE_VARIABLE_FAMILY_CODE(250)
    }
}

VARIABLE var0_serialNum
{   
    LABEL [sensor_serial_number];
    HELP [sensor_serial_number_help];
    TYPE UNSIGNED_INTEGER(3); 
}

VARIABLE var0_update_period 
{
    LABEL "Update Period";
    TYPE TIME_VALUE;
    HANDLING READ;
}

VARIABLE var0_write_dev_var_code
{
    LABEL "Dev Var 0 write dev var code";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

COLLECTION OF VARIABLE devVar0
{
    LABEL "Device Var 0";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              var0_value;
        DIGITAL_UNITS,              var0_units;
        DATA_QUALITY,               var0_data_quality;
        LIMIT_STATUS,               var0_limit_status;
        DEVICE_FAMILY_STATUS,       var0_family_status;

        CLASSIFICATION,             var0_classification;
        DEVICE_FAMILY,              var0_family;

        UPPER_SENSOR_LIMIT,         var0_usl;
        LOWER_SENSOR_LIMIT,         var0_lsl;
        MINIMUM_SPAN,               var0_minSpan;
        DAMPING_VALUE,              var0_damping;
        UPDATE_TIME_PERIOD,         var0_update_period;
        SIMULATED,                  var0_write_dev_var_code;

        // Device Information
        SENSOR_SERIAL_NUMBER,       var0_serialNum;
    }
}

/* Because IO Adapters do not know what device variables are
   supported in the subdevice that it will be attached to,
   all entries from 0-249 need to be declared so that all
   possible burst variable codes will be valid. For the
   variable indexes not used for this device, the other
   indexes will point to a global data holder to save size. */
/* Variables 243-249 are predefined variables that should be
   supported if applicable to the device. */
ARRAY OF COLLECTION deviceVariables 
{
    ELEMENTS
    {
        0, devVar0,               "Device Variable 0";    // Shouldn't use names specific to this device because these could be for subdevices
        1, scratchData,           "Device Variable 1";
        2, scratchData,           "Device Variable 2";
        3, scratchData,           "Device Variable 3";
        4, scratchData,           "Device Variable 4";
        5, scratchData,           "Device Variable 5";
        6, scratchData,           "Device Variable 6";
        7, scratchData,           "Device Variable 7";
        8, scratchData,           "Device Variable 8";
        9, scratchData,           "Device Variable 9";
        10, scratchData,          "Device Variable 10";
        11, scratchData,          "Device Variable 11";
        12, scratchData,          "Device Variable 12";
        13, scratchData,          "Device Variable 13";
        14, scratchData,          "Device Variable 14";
        15, scratchData,          "Device Variable 15";
        16, scratchData,          "Device Variable 16";
        17, scratchData,          "Device Variable 17";
        18, scratchData,          "Device Variable 18";
        19, scratchData,          "Device Variable 19";
        20, scratchData,          "Device Variable 20";
        21, scratchData,          "Device Variable 21";
        22, scratchData,          "Device Variable 22";
        23, scratchData,          "Device Variable 23";
        24, scratchData,          "Device Variable 24";
        25, scratchData,          "Device Variable 25";
        26, scratchData,          "Device Variable 26";
        27, scratchData,          "Device Variable 27";
        28, scratchData,          "Device Variable 28";
        29, scratchData,          "Device Variable 29";
        30, scratchData,          "Device Variable 30";
        31, scratchData,          "Device Variable 31";
        32, scratchData,          "Device Variable 32";
        33, scratchData,          "Device Variable 33";
        34, scratchData,          "Device Variable 34";
        35, scratchData,          "Device Variable 35";
        36, scratchData,          "Device Variable 36";
        37, scratchData,          "Device Variable 37";
        38, scratchData,          "Device Variable 38";
        39, scratchData,          "Device Variable 39";
        40, scratchData,          "Device Variable 40";
        41, scratchData,          "Device Variable 41";
        42, scratchData,          "Device Variable 42";
        43, scratchData,          "Device Variable 43";
        44, scratchData,          "Device Variable 44";
        45, scratchData,          "Device Variable 45";
        46, scratchData,          "Device Variable 46";
        47, scratchData,          "Device Variable 47";
        48, scratchData,          "Device Variable 48";
        49, scratchData,          "Device Variable 49";
        50, scratchData,          "Device Variable 50";
        51, scratchData,          "Device Variable 51";
        52, scratchData,          "Device Variable 52";
        53, scratchData,          "Device Variable 53";
        54, scratchData,          "Device Variable 54";
        55, scratchData,          "Device Variable 55";
        56, scratchData,          "Device Variable 56";
        57, scratchData,          "Device Variable 57";
        58, scratchData,          "Device Variable 58";
        59, scratchData,          "Device Variable 59";
        60, scratchData,          "Device Variable 60";
        61, scratchData,          "Device Variable 61";
        62,  scratchData,         "Device Variable 62";
        63,  scratchData,         "Device Variable 63";
        64,  scratchData,         "Device Variable 64";
        65,  scratchData,         "Device Variable 65";
        66,  scratchData,         "Device Variable 66";
        67,  scratchData,         "Device Variable 67";
        68,  scratchData,         "Device Variable 68";
        69,  scratchData,         "Device Variable 69";
        70,  scratchData,         "Device Variable 70";
        71,  scratchData,         "Device Variable 71";
        72,  scratchData,         "Device Variable 72";
        73,  scratchData,         "Device Variable 73";
        74,  scratchData,         "Device Variable 74";
        75,  scratchData,         "Device Variable 75";
        76,  scratchData,         "Device Variable 76";
        77,  scratchData,         "Device Variable 77";
        78,  scratchData,         "Device Variable 78";
        79,  scratchData,         "Device Variable 79";
        80,  scratchData,         "Device Variable 80";
        81,  scratchData,         "Device Variable 81";
        82,  scratchData,         "Device Variable 82";
        83,  scratchData,         "Device Variable 83";
        84,  scratchData,         "Device Variable 84";
        85,  scratchData,         "Device Variable 85";
        86,  scratchData,         "Device Variable 86";
        87,  scratchData,         "Device Variable 87";
        88,  scratchData,         "Device Variable 88";
        89,  scratchData,         "Device Variable 89";
        90,  scratchData,         "Device Variable 90";
        91,  scratchData,         "Device Variable 91";
        92,  scratchData,         "Device Variable 92";
        93,  scratchData,         "Device Variable 93";
        94,  scratchData,         "Device Variable 94";
        95,  scratchData,         "Device Variable 95";
        96,  scratchData,         "Device Variable 96";
        97,  scratchData,         "Device Variable 97";
        98,  scratchData,         "Device Variable 98";
        99,  scratchData,         "Device Variable 99";
        100, scratchData,         "Device Variable 100";
        101, scratchData,         "Device Variable 101";
        102, scratchData,         "Device Variable 102";
        103, scratchData,         "Device Variable 103";
        104, scratchData,         "Device Variable 104";
        105, scratchData,         "Device Variable 105";
        106, scratchData,         "Device Variable 106";
        107, scratchData,         "Device Variable 107";
        108, scratchData,         "Device Variable 108";
        109, scratchData,         "Device Variable 109";
        110, scratchData,         "Device Variable 110";
        111, scratchData,         "Device Variable 111";
        112, scratchData,         "Device Variable 112";
        113, scratchData,         "Device Variable 113";
        114, scratchData,         "Device Variable 114";
        115, scratchData,         "Device Variable 115";
        116, scratchData,         "Device Variable 116";
        117, scratchData,         "Device Variable 117";
        118, scratchData,         "Device Variable 118";
        119, scratchData,         "Device Variable 119";
        120, scratchData,         "Device Variable 120";
        121, scratchData,         "Device Variable 121";
        122, scratchData,         "Device Variable 122";
        123, scratchData,         "Device Variable 123";
        124, scratchData,         "Device Variable 124";
        125, scratchData,         "Device Variable 125";
        126, scratchData,         "Device Variable 126";
        127, scratchData,         "Device Variable 127";
        128, scratchData,         "Device Variable 128";
        129, scratchData,         "Device Variable 129";
        130, scratchData,         "Device Variable 130";
        131, scratchData,         "Device Variable 131";
        132, scratchData,         "Device Variable 132";
        133, scratchData,         "Device Variable 133";
        134, scratchData,         "Device Variable 134";
        135, scratchData,         "Device Variable 135";
        136, scratchData,         "Device Variable 136";
        137, scratchData,         "Device Variable 137";
        138, scratchData,         "Device Variable 138";
        139, scratchData,         "Device Variable 139";
        140, scratchData,         "Device Variable 140";
        141, scratchData,         "Device Variable 141";
        142, scratchData,         "Device Variable 142";
        143, scratchData,         "Device Variable 143";
        144, scratchData,         "Device Variable 144";
        145, scratchData,         "Device Variable 145";
        146, scratchData,         "Device Variable 146";
        147, scratchData,         "Device Variable 147";
        148, scratchData,         "Device Variable 148";
        149, scratchData,         "Device Variable 149";
        150, scratchData,         "Device Variable 150";
        151, scratchData,         "Device Variable 151";
        152, scratchData,         "Device Variable 152";
        153, scratchData,         "Device Variable 153";
        154, scratchData,         "Device Variable 154";
        155, scratchData,         "Device Variable 155";
        156, scratchData,         "Device Variable 156";
        157, scratchData,         "Device Variable 157";
        158, scratchData,         "Device Variable 158";
        159, scratchData,         "Device Variable 159";
        160, scratchData,         "Device Variable 160";
        161, scratchData,         "Device Variable 161";
        162, scratchData,         "Device Variable 162";
        163, scratchData,         "Device Variable 163";
        164, scratchData,         "Device Variable 164";
        165, scratchData,         "Device Variable 165";
        166, scratchData,         "Device Variable 166";
        167, scratchData,         "Device Variable 167";
        168, scratchData,         "Device Variable 168";
        169, scratchData,         "Device Variable 169";
        170, scratchData,         "Device Variable 170";
        171, scratchData,         "Device Variable 171";
        172, scratchData,         "Device Variable 172";
        173, scratchData,         "Device Variable 173";
        174, scratchData,         "Device Variable 174";
        175, scratchData,         "Device Variable 175";
        176, scratchData,         "Device Variable 176";
        177, scratchData,         "Device Variable 177";
        178, scratchData,         "Device Variable 178";
        179, scratchData,         "Device Variable 179";
        180, scratchData,         "Device Variable 180";
        181, scratchData,         "Device Variable 181";
        182, scratchData,         "Device Variable 182";
        183, scratchData,         "Device Variable 183";
        184, scratchData,         "Device Variable 184";
        185, scratchData,         "Device Variable 185";
        186, scratchData,         "Device Variable 186";
        187, scratchData,         "Device Variable 187";
        188, scratchData,         "Device Variable 188";
        189, scratchData,         "Device Variable 189";
        190, scratchData,         "Device Variable 190";
        191, scratchData,         "Device Variable 191";
        192, scratchData,         "Device Variable 192";
        193, scratchData,         "Device Variable 193";
        194, scratchData,         "Device Variable 194";
        195, scratchData,         "Device Variable 195";
        196, scratchData,         "Device Variable 196";
        197, scratchData,         "Device Variable 197";
        198, scratchData,         "Device Variable 198";
        199, scratchData,         "Device Variable 199";
        200, scratchData,         "Device Variable 200";
        201, scratchData,         "Device Variable 201";
        202, scratchData,         "Device Variable 202";
        203, scratchData,         "Device Variable 203";
        204, scratchData,         "Device Variable 204";
        205, scratchData,         "Device Variable 205";
        206, scratchData,         "Device Variable 206";
        207, scratchData,         "Device Variable 207";
        208, scratchData,         "Device Variable 208";
        209, scratchData,         "Device Variable 209";
        210, scratchData,         "Device Variable 210";
        211, scratchData,         "Device Variable 211";
        212, scratchData,         "Device Variable 212";
        213, scratchData,         "Device Variable 213";
        214, scratchData,         "Device Variable 214";
        215, scratchData,         "Device Variable 215";
        216, scratchData,         "Device Variable 216";
        217, scratchData,         "Device Variable 217";
        218, scratchData,         "Device Variable 218";
        219, scratchData,         "Device Variable 219";
        220, scratchData,         "Device Variable 220";
        221, scratchData,         "Device Variable 221";
        222, scratchData,         "Device Variable 222";
        223, scratchData,         "Device Variable 223";
        224, scratchData,         "Device Variable 224";
        225, scratchData,         "Device Variable 225";
        226, scratchData,         "Device Variable 226";
        227, scratchData,         "Device Variable 227";
        228, scratchData,         "Device Variable 228";
        229, scratchData,         "Device Variable 229";
        230, scratchData,         "Device Variable 230";
        231, scratchData,         "Device Variable 231";
        232, scratchData,         "Device Variable 232";
        233, scratchData,         "Device Variable 233";
        234, scratchData,         "Device Variable 234";
        235, scratchData,         "Device Variable 235";
        236, scratchData,         "Device Variable 236";
        237, scratchData,         "Device Variable 237";
        238, scratchData,         "Device Variable 238";
        239, scratchData,         "Device Variable 239";
        240, scratchData,         "Device Variable 240";
        241, scratchData,         "Device Variable 241";
        242, scratchData,         "Device Variable 242";
        243, batteryLife,         device_variable_code_codes(243);
        244, scratchData,         device_variable_code_codes(244);
        245, scratchData,         device_variable_code_codes(245);
        246, PV.DEVICE_VARIABLE,  device_variable_code_codes(246);
        247, scratchData,         device_variable_code_codes(247);
        248, scratchData,         device_variable_code_codes(248);
        249, scratchData,         device_variable_code_codes(249);
        250, scratchData,         [not_used];

        /* 
     * Uncomment each variable if used in this device, and remove from above list
     */
        //247, SV.DEVICE_VARIABLE,  device_variable_code_codes(247);
        //248, TV.DEVICE_VARIABLE,  device_variable_code_codes(248);
        //249, QV.DEVICE_VARIABLE,  device_variable_code_codes(249);
    }
}

/***************************************************************************
 * below is an example menu for a wireless adapter.  Modify to fit the needs
 * of your device.
 */

MENU root_menu
{
    LABEL [bus_devices];
    ITEMS
    {
        device_setup,                                               // menu
        PV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE)
    }
}

MENU device_setup
{
    LABEL [device_setup];
    ITEMS
    {
        process_variables,                                          // menu
        diag_service,                                               // menu
        basic_setup,                                                // menu
        detailed_setup,                                             // menu
        review (REVIEW)                                             // menu
    }
}

MENU process_variables
{
    LABEL [process_variables];
    ITEMS
    {
        deviceVariables[0].DIGITAL_VALUE (DISPLAY_VALUE),

        /* 
         * Add your device variables here
         */
        deviceVariables[243].DIGITAL_VALUE (DISPLAY_VALUE)          // Battery life if applicable
    }
}

MENU diag_service
{
    LABEL [diagnostics_and_service];
    ITEMS
    {
        test_device                                                 // menu
    }
}

MENU test_device
{
    LABEL [test_device];
    ITEMS
    {
        status_display,                                             // menu
        wireless_status,                                            // menu
#if INCLUDE_EVENTS
        event_status_menu,                                          // menu
#endif
        device_self_test,                                           // method
        device_master_reset                                         // method
    }
}

MENU status_display
{
    LABEL "Device Status";
    ITEMS
    {
        device_status (DISPLAY_VALUE),
        extended_fld_device_status (DISPLAY_VALUE),
        standardized_status_0 (DISPLAY_VALUE),
        standardized_status_1 (DISPLAY_VALUE),
        standardized_status_2 (DISPLAY_VALUE),
        standardized_status_3 (DISPLAY_VALUE),
        device_specific_status_0 (DISPLAY_VALUE),
        device_specific_status_1 (DISPLAY_VALUE),
        device_specific_status_2 (DISPLAY_VALUE),
        device_specific_status_3 (DISPLAY_VALUE),
        device_specific_status_4 (DISPLAY_VALUE),
        device_specific_status_5 (DISPLAY_VALUE),

    /*
     * Add any additional device specific status here
     */
        config_change_counter (DISPLAY_VALUE),
        battery_life (DISPLAY_VALUE),
        device_power_status (DISPLAY_VALUE),
        message_statistics,                                         // menu
        time                                                        // menu
    }
}

MENU basic_setup
{
    LABEL [basic_setup];
    ITEMS
    {
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        PV.DEVICE_VARIABLE.DIGITAL_UNITS,
        device_info                                                 // menu
    }
}

MENU detailed_setup
{
    LABEL [detailed_setup];
    ITEMS
    {
        output_conditioning,                                        // menu
        device_info,                                                // menu
        wireless_info,                                              // menu
        subdevice_info                                              // menu
    }
}

MENU device_info
{
    LABEL [device_information];
    ITEMS
    {
        private_label_distributor (DISPLAY_VALUE),
        device_type (DISPLAY_VALUE),
        device_id (DISPLAY_VALUE),
        config_change_counter (DISPLAY_VALUE),
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        date (DISPLAY_VALUE),
        write_protect (DISPLAY_VALUE),
        descriptor (DISPLAY_VALUE),
        message (DISPLAY_VALUE),
        final_assembly_number (DISPLAY_VALUE),
        country_code (DISPLAY_VALUE),
        si_control (DISPLAY_VALUE),
        physical_signaling_code (DISPLAY_VALUE),
        power_source (DISPLAY_VALUE),
        device_flags (DISPLAY_VALUE),
        device_profile (DISPLAY_VALUE),
        device_capabilities,                                        // menu
        device_revisions                                            // menu
    }
}

MENU device_revisions
{
    LABEL [revision_numbers];
    ITEMS
    {
        universal_revision (DISPLAY_VALUE),
        transmitter_revision (DISPLAY_VALUE),
        software_revision (DISPLAY_VALUE),
        hardware_revision (DISPLAY_VALUE)
    }
}

MENU output_conditioning
{
    LABEL [output_condition];
    ITEMS
    {
        hart_output,                                                // menu
        wireless_output                                             // menu
    }
}

MENU hart_output
{
    LABEL [HART_output];
    ITEMS
    {
        polling_address (DISPLAY_VALUE),
        request_preambles (DISPLAY_VALUE),
        response_preambles (DISPLAY_VALUE),
        master_mode (DISPLAY_VALUE),
        retry_count (DISPLAY_VALUE),
        burst_config                                                // menu
#if INCLUDE_EVENTS
       ,event_config                                                // menu
#endif
    }
}

MENU review
{
    LABEL [review];
    ITEMS
    {
        device_type (DISPLAY_VALUE, READ_ONLY),
        private_label_distributor (DISPLAY_VALUE, READ_ONLY),
        write_protect (DISPLAY_VALUE, READ_ONLY),
        device_id (DISPLAY_VALUE, READ_ONLY),
        device_nickname_address (DISPLAY_VALUE, READ_ONLY),
        config_change_counter (DISPLAY_VALUE, READ_ONLY),
        max_num_device_variables  (DISPLAY_VALUE, READ_ONLY),
        tag (DISPLAY_VALUE, READ_ONLY),
        longTag (DISPLAY_VALUE, READ_ONLY),
        descriptor (DISPLAY_VALUE, READ_ONLY),
        message (DISPLAY_VALUE, READ_ONLY),
        date (DISPLAY_VALUE, READ_ONLY),
        final_assembly_number (DISPLAY_VALUE),
        universal_revision (DISPLAY_VALUE, READ_ONLY),
        transmitter_revision (DISPLAY_VALUE, READ_ONLY),
        software_revision (DISPLAY_VALUE, READ_ONLY),
        polling_address (DISPLAY_VALUE, READ_ONLY),
        loop_current_mode (DISPLAY_VALUE, READ_ONLY),
        request_preambles (DISPLAY_VALUE, READ_ONLY),
        response_preambles (DISPLAY_VALUE, READ_ONLY)
    }
}

MENU wireless_status
{
    LABEL "Wireless Status";
    ITEMS
    {
        wireless_mode (DISPLAY_VALUE),
        join_status (DISPLAY_VALUE),
        advertisements (DISPLAY_VALUE),
        neighbor_count (DISPLAY_VALUE),
        join_attempts (DISPLAY_VALUE),
        network_search_timer (DISPLAY_VALUE),
        wireless_statistics                                         // menu
    }
}

MENU wireless_statistics
{
    LABEL "Wireless Statistics";
    ITEMS
    {
        generated_packets (DISPLAY_VALUE),
        terminated_packets (DISPLAY_VALUE),
        detected_crc_errors (DISPLAY_VALUE),
        detected_dll_mic_failures (DISPLAY_VALUE),
        detected_nl_mic_failures (DISPLAY_VALUE),
        not_received_nonce_counter_values (DISPLAY_VALUE)
    }
}

#if INCLUDE_EVENTS
MENU event_status_menu
{
    LABEL "Event Status";
    ITEMS
    {
        event1_status,
        event2_status
    }
}

MENU event1_status
{
    LABEL "Event 1";
    ITEMS
    {
        subdevice_list[events[0].EVENT_CONTROL.SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.SUB_DEVICE_MISSING (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_STATUS (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (DISPLAY_VALUE)
    }
}
#endif

MENU time
{
    LABEL "Time";
    ITEMS
    {
        current_date(DISPLAY_VALUE),
        current_time(DISPLAY_VALUE),
        last_clock_date(DISPLAY_VALUE),
        last_clock_time(DISPLAY_VALUE),
        real_time_clock_flag (DISPLAY_VALUE)
    }
}

MENU device_capabilities
{
    LABEL "Device Capabilities";
    ITEMS
    {
        total_number_burst_messages (DISPLAY_VALUE),
#if INCLUDE_EVENTS
        number_events_supported (DISPLAY_VALUE),
#endif
        max_io_cards (DISPLAY_VALUE),
        max_channels_per_io_card (DISPLAY_VALUE),
        max_sub_dev_per_channel (DISPLAY_VALUE),
        max_dr (DISPLAY_VALUE)
    }
}

MENU wireless_info
{
    LABEL "Wireless Info";
    ITEMS
    {
        /*wireless_module_manufacturer_id (DISPLAY_VALUE),*/        // Optional
        /*wireless_module_device_type (DISPLAY_VALUE),*/            // Optional
        /*wireless_module_transmitter_revision (DISPLAY_VALUE),*/   // Optional
        /*wireless_module_software_revision (DISPLAY_VALUE),*/      // Optional
        /*wireless_module_hardware_revision (DISPLAY_VALUE),*/      // Optional
        device_nickname_address (DISPLAY_VALUE),
        join_retry_timer (DISPLAY_VALUE),
        keep_alive_min_time (DISPLAY_VALUE),
        wireless_capabilities                                       // menu
    }
}

MENU wireless_capabilities
{
    LABEL "Wireless Capabilities";
    ITEMS
    {
        max_neighbors (DISPLAY_VALUE),
        max_packet_buffers (DISPLAY_VALUE),
        receive_signal_level (DISPLAY_VALUE),
        recover_time (DISPLAY_VALUE),
        peak_packet_load (DISPLAY_VALUE),
        peak_packets_per_second (DISPLAY_VALUE)
    }
}

MENU burst_config
{
    LABEL "Burst Configuration";
    ITEMS
    {
        burst_menu_1,                                               // menu
        burst_menu_2,                                               // menu
        burst_menu_3,                                               // menu
        burst_menu_4                                                // menu
    }
}

MENU burst_menu_1
{
    LABEL "Burst Message 1";
    ITEMS
    {
        change_burst1_subdevice,                                    // method
        subdevice_list[burst_messages[0].SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        burst_messages[0].SUB_DEVICE_MISSING (DISPLAY_VALUE),
        burst_messages[0].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[0].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[0].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[0].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[0].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[0].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[0].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[0].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars1                                                 // menu
    }
}

MENU burst_vars1
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[0].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[0].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[0].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[7] (DISPLAY_VALUE)
    }
}

MENU burst_menu_2
{
    LABEL "Burst Message 2";
    ITEMS
    {
        change_burst2_subdevice,                                    // method
        subdevice_list[burst_messages[1].SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        burst_messages[1].SUB_DEVICE_MISSING (DISPLAY_VALUE),
        burst_messages[1].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[1].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[1].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[1].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[1].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[1].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[1].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[1].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars2                                                 // menu
    }
}

MENU burst_vars2
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[1].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[1].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[1].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[7] (DISPLAY_VALUE)
    }
}

MENU burst_menu_3
{
    LABEL "Burst Message 3";
    ITEMS
    {
        change_burst3_subdevice,                                    // method
        subdevice_list[burst_messages[2].SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        burst_messages[2].SUB_DEVICE_MISSING (DISPLAY_VALUE),
        burst_messages[2].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[2].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[2].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[2].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[2].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[2].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[2].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[2].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars3                                                 // menu
    }
}

MENU burst_vars3
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[2].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[2].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[2].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[7] (DISPLAY_VALUE)
    }
}

MENU burst_menu_4
{
    LABEL "Burst Message 4";
    ITEMS
    {
        change_burst4_subdevice,                                    // method
        subdevice_list[burst_messages[3].SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        burst_messages[3].SUB_DEVICE_MISSING (DISPLAY_VALUE),
        burst_messages[3].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[3].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[3].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[3].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[3].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[3].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[3].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[3].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars4                                                 // menu
    }
}

MENU burst_vars4
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[3].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[3].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[3].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[3].BURST_VAR[7] (DISPLAY_VALUE)
    }
}

#if INCLUDE_EVENTS
MENU event_config
{
    LABEL "Event Configuration";
    ITEMS
    {
        event_menu_1,                                               // menu
        event_menu_2                                                // menu
    }
}

MENU event_menu_1
{
    LABEL "Event 1";
    ITEMS
    {
        change_event1_subdevice,                                    // method
        subdevice_list[events[0].EVENT_CONTROL.SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.SUB_DEVICE_MISSING (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.MAX_UPDATE_TIME (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL (DISPLAY_VALUE),
        event_mask_1                                                // menu
    }
}

MENU event_mask_1
{
    LABEL "Event Mask";
    ITEMS
    {
        events[0].EVENT_MASK.DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_0_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_2_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_3_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.ANALOG_CHANNEL_FIXED1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEV_OPERATING_MODE_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_14_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_15_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_16_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_17_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_18_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_19_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_20_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_21_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_22_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_23_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_24_MASK (DISPLAY_VALUE)
    }
}

MENU event_menu_2
{
    LABEL "Event 2";
    ITEMS
    {
        change_event2_subdevice,
        subdevice_list[events[1].EVENT_CONTROL.SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.SUB_DEVICE_MISSING (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.MAX_UPDATE_TIME (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL (DISPLAY_VALUE),
        event_mask_2
    }
}

MENU event_mask_2
{
    LABEL "Event Mask";
    ITEMS
    {
        events[1].EVENT_MASK.DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.STANDARDIZED_STATUS_0_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.STANDARDIZED_STATUS_1_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.STANDARDIZED_STATUS_2_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.STANDARDIZED_STATUS_3_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.ANALOG_CHANNEL_FIXED1_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEV_OPERATING_MODE_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_14_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_15_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_16_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_17_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_18_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_19_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_20_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_21_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_22_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_23_MASK (DISPLAY_VALUE),
        events[1].EVENT_MASK.DEVICE_SPECIFIC_STATUS_24_MASK (DISPLAY_VALUE)
    }
}
#endif

MENU wireless_output
{
    LABEL "WirelessHART Output";
    ITEMS
    {
        join_network,                                               // method
        network_id (DISPLAY_VALUE),
        join_mode (DISPLAY_VALUE),
        join_shed_time (DISPLAY_VALUE),
        radio_power_output (DISPLAY_VALUE)
    }
}

MENU subdevice_info
{
    LABEL "Subdevices by Index";
    ITEMS 
    {
        devices_detected (DISPLAY_VALUE),
        refreshSubDeviceMapping,                                    // method
        sub_device_menu_0,
        sub_device_menu_1,
        sub_device_menu_2

        /*
     * Add more as needed
     */
    }

}

MENU sub_device_menu_0
{
    LABEL "I/O Adapter";
    ITEMS
    {
        subdevice_list[0].__IDENTITY.IO_CARD (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.CHANNEL (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.MANUFID (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.EXP_DEV_TYPE (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.DEV_ID (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.UNIV_CMD_REV (DISPLAY_VALUE),
        subdevice_list[0].__IDENTITY.LONG_TAG (DISPLAY_VALUE)
    }
}

MENU sub_device_menu_1
{
    LABEL "Subdevice 1";
    VALIDITY  IF ( devices_detected > 0x0001 )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        subdevice_list[1].__IDENTITY.IO_CARD (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.CHANNEL (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.MANUFID (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.EXP_DEV_TYPE (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.DEV_ID (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.UNIV_CMD_REV (DISPLAY_VALUE),
        subdevice_list[1].__IDENTITY.LONG_TAG (DISPLAY_VALUE)
    }
}

MENU sub_device_menu_2
{
    LABEL "Subdevice 2";
    VALIDITY  IF ( devices_detected > 0x0002 )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        subdevice_list[2].__IDENTITY.IO_CARD (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.CHANNEL (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.MANUFID (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.EXP_DEV_TYPE (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.DEV_ID (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.UNIV_CMD_REV (DISPLAY_VALUE),
        subdevice_list[2].__IDENTITY.LONG_TAG (DISPLAY_VALUE)
    }
}

#if INCLUDE_EVENTS
MENU event2_status
{
    LABEL "Event 2";
    ITEMS
    {
        subdevice_list[events[1].EVENT_CONTROL.SUB_DEVICE_MAPPING].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.SUB_DEVICE_MISSING (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.EVENT_STATUS (DISPLAY_VALUE),
        events[1].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (DISPLAY_VALUE)
    }
}
#endif

MENU message_statistics
{
    LABEL "Message Statistics";
    ITEMS
    {
       AdapterStats,                 // menu
       MasterStats,                  // menu
       subDeviceStats                // menu
    }
}

MENU AdapterStats
{
    LABEL "IO Slave statistics";
    ITEMS
    {
       STX_count(DISPLAY_VALUE),
       ACK_count(DISPLAY_VALUE),
       BACK_count(DISPLAY_VALUE)
    }
}

MENU MasterStats
{
    LABEL "IO Master statistics";
    ITEMS
    {
        Card0Stats

        /* 
     * DD currently supports 1 IO card - add additional cards here in future as needed
     */
    }
}

MENU Card0Stats
{
    LABEL "IO Card 1";
    ITEMS
    {
        Channel0Stats

        /*
     * Add menus for each additional channel supported!
     */
    }
}

/* 
 * Add more of these for each card/channel combination supported by the adapter
 */
MENU Channel0Stats
{
    LABEL "IO Channel 1";
    ITEMS
    {
       io_cards[0][0].STX_SENT(DISPLAY_VALUE),
       io_cards[0][0].ACK_RECEIVED(DISPLAY_VALUE),
       io_cards[0][0].OSTX_RECEIVED(DISPLAY_VALUE),
       io_cards[0][0].OACK_RECEIVED(DISPLAY_VALUE),
       io_cards[0][0].BACK_RECEIVED(DISPLAY_VALUE)
    }
}

MENU subDeviceStats
{
    LABEL "Subdevice Statistics";
    ITEMS
    {
        subdev_0_stats,             // menu
        subdev_1_stats,             // menu
        subdev_2_stats              // menu

        /*
     * Add more as needed
     */
    }
}

MENU subdev_0_stats
{
    LABEL "IO Adapter";
    ITEMS
    {
        subdevice_list[0].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        subdevice_list[0].STATS.STAT_STX_CNT (DISPLAY_VALUE),
        subdevice_list[0].STATS.STAT_ACK_CNT (DISPLAY_VALUE),
        subdevice_list[0].STATS.STAT_BACK_CNT (DISPLAY_VALUE)
    }
}

MENU subdev_1_stats
{
    LABEL "Subdevice 1";
    ITEMS
    {
        subdevice_list[1].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        subdevice_list[1].STATS.STAT_STX_CNT (DISPLAY_VALUE),
        subdevice_list[1].STATS.STAT_ACK_CNT (DISPLAY_VALUE),
        subdevice_list[1].STATS.STAT_BACK_CNT (DISPLAY_VALUE)
    }
    VALIDITY  IF ( devices_detected > 0x0001 )   {TRUE;}   ELSE  {FALSE;}
}

MENU subdev_2_stats
{
    LABEL "Subdevice 2";
    ITEMS
    {
        subdevice_list[2].__IDENTITY.LONG_TAG (DISPLAY_VALUE),
        subdevice_list[2].STATS.STAT_STX_CNT (DISPLAY_VALUE),
        subdevice_list[2].STATS.STAT_ACK_CNT (DISPLAY_VALUE),
        subdevice_list[2].STATS.STAT_BACK_CNT (DISPLAY_VALUE)
    }
    VALIDITY  IF ( devices_detected > 0x0002 )   {TRUE;}   ELSE  {FALSE;}
}

METHOD forceJoinMode
{
    LABEL "Force Join";
    HELP  "";
    DEFINITION 
    {
        char status[STATUS_SIZE];

        do
        {
            GET_DEV_VAR_VALUE("Enter Join Mode ", join_mode);
            GET_DEV_VAR_VALUE("Enter Join Shed Time", join_shed_time);

            send(771,status);
    
            if (status[STATUS_RESPONSE_CODE])
            {
                display_response_status(771, status[STATUS_RESPONSE_CODE]);
            }
    
        }while(status[STATUS_RESPONSE_CODE]);

        send_command(769);
    }
}

METHOD join_network
{
    LABEL "Join Network";
    HELP  "";
    DEFINITION 
    {
        char status[STATUS_SIZE];
        int iJoin;
 
        IGNORE_ALL_RESPONSE_CODES();
        IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_DEVICE_STATUS();

        GET_DEV_VAR_VALUE("Enter Network ID", network_id);

        GET_DEV_VAR_VALUE("Enter Join Key 1", join_key_1);
        GET_DEV_VAR_VALUE("Enter Join Key 2", join_key_2);
        GET_DEV_VAR_VALUE("Enter Join Key 3", join_key_3);
        GET_DEV_VAR_VALUE("Enter Join Key 4", join_key_4);

        send(773,status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(773, status[STATUS_RESPONSE_CODE]);
        }

        send(768,status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(768, status[STATUS_RESPONSE_CODE]);
        }
    

       iassign(join_mode, 1);     // join now
       iassign(join_shed_time, 115200000); // search for 1 hour

       send(771,status);
       if (status[STATUS_RESPONSE_CODE])
       {
           display_response_status(771, status[STATUS_RESPONSE_CODE]);
       }


        if (ivar_value(join_mode) == 1 ) // only loop if we told device to join now
        {
            do
            {
                send_command(769);
                iJoin = ivar_value(join_status);
                if (iJoin&0x400)
                {
                    DELAY (5, "Join Status - Join complete");
                }
                else if (iJoin&0x040)
                {
                    DELAY (5, "Join Status - Join failed");
                }
                else if (iJoin&0x020)
                {
                    DELAY (5, "Join Status - Join retrying");  
                }
                else if (iJoin&0x200)
                {
                    DELAY (5, "Join Status - Network bandwidth requested");  
                }
                else if (iJoin&0x100)
                {
                    DELAY (5, "Join Status - Network joined");  
                }
                else if (iJoin&0x080)
                {
                    DELAY (5, "Join Status - Network security clearance granted");  
                }
                else if (iJoin&0x010)
                {
                    DELAY (5, "Join Status - Network admission requested");  
                }
                else if (iJoin&0x008)
                {
                    DELAY (5, "Join Status - WirelessHart signal identified");  
                }
                else if (iJoin&0x004)
                {
                    DELAY (5, "Join Status - Wireless time synchronized");  
                }
                else if (iJoin&0x002)
                {
                    DELAY (5, "Join Status - Wireless signal identified");  
                }
                else if (iJoin&0x001)
                {
                    DELAY (5, "Join Status - Network packets heard");  
                }
                else if (iJoin==0)
                {
                    DELAY (5, "Join Status - Network not found");  
                }
                else
                {
                    DELAY (5, "Join Status - Unknown");
                    iJoin = 0;
                } 
                
            } while (!((iJoin & 0x040)||(iJoin & 0x400)));
        }
    }
}

METHOD change_burst1_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the burst message is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;

        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        GET_DEV_VAR_VALUE("Select sub-device to use for burst message 1", burst_messages[0].SUB_DEVICE_MAPPING);

        iassign(burst_message_number, 0);
        send_trans(102, 0, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}

METHOD change_burst2_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the burst message is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;

        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        GET_DEV_VAR_VALUE("Select sub-device to use for burst message 2", burst_messages[1].SUB_DEVICE_MAPPING);

        iassign(burst_message_number, 1);
        send_trans(102, 0, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}

METHOD change_burst3_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the burst message is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;
        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        iassign(burst_message_number, 2);
        GET_DEV_VAR_VALUE("Select sub-device to use for burst message 3", burst_messages[2].SUB_DEVICE_MAPPING);

        send_trans(102, 0, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}

METHOD change_burst4_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the burst message is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;
        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        iassign(burst_message_number, 3);
        GET_DEV_VAR_VALUE("Select sub-device to use for burst message 4", burst_messages[3].SUB_DEVICE_MAPPING);

        send_trans(102, 0, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}

#if INCLUDE_EVENTS
METHOD change_event1_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the event is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;

        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        GET_DEV_VAR_VALUE("Select sub-device to use for Event 1", events[0].EVENT_CONTROL.SUB_DEVICE_MAPPING);

        iassign(eventNumber, 0);
        send_trans(102, 1, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}

METHOD change_event2_subdevice
{
    LABEL "Change sub-device";
    HELP "Changes the sub-device that the event is transmitted for";
    DEFINITION
    {
        char status[STATUS_SIZE];
        char disp_string1[140];
        int slen1;

        slen1 = 140;

        IGNORE_ALL_DEVICE_STATUS();
        IGNORE_ALL_RESPONSE_CODES();
        XMTR_IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_RESPONSE_CODES();

        GET_DEV_VAR_VALUE("Select sub-device to use for Event 2", events[1].EVENT_CONTROL.SUB_DEVICE_MAPPING);

        iassign(eventNumber, 1);
        send_trans(102, 1, status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(102, status[STATUS_RESPONSE_CODE]);

            get_dictionary_string(abort_last_error, disp_string1, slen1);
            DELAY(5, disp_string1);
            process_abort();
        }
    }
}
#endif

METHOD refreshSubDeviceMapping
{
    LABEL "Refresh SubDevice Mapping";
    HELP  "This method forces the sub-device mapping information to be re-read from the field device for all supported burst and event messages.";
    DEFINITION 
    {
        char status[STATUS_SIZE];
        int i;

        IGNORE_ALL_RESPONSE_CODES();
        IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_DEVICE_STATUS();

        for(i=0; i<ivar_value(total_number_burst_messages); i++)
        {
            iassign(burst_message_number, i);
            send_trans(101, 0, status);
            if (status[STATUS_RESPONSE_CODE])
            {
                display_response_status(101, status[STATUS_RESPONSE_CODE]);
            }
        }

#if INCLUDE_EVENTS
        for(i=0; i<ivar_value(number_events_supported); i++)
        {
            iassign(eventNumber, i);
            send_trans(101, 1, status);
            if (status[STATUS_RESPONSE_CODE])
            {
                display_response_status(101, status[STATUS_RESPONSE_CODE]);
            }
        }
#endif

        ACKNOWLEDGE("Subdevice mapping has been refreshed for all Burst and Event messages.");
    }
}

