
set LIB=K:\Hcf_tok.2013.4_10.3.11\hartddl\Release
set ARGS=-i -R %LIB%  -k 615 -k 335 -k 336 -k 333 3051CR32.DDL
rem set ARGS=-i -R %LIB%  -k 615 -k 335 -k 336 -k 333 _Super.ddl
rem ARGS=-i -R %LIB%    -V  -d LibA/standard.dc8  -i -R LibA -k 615 -k 602 -k 614 -k 622 -k 710 -k 674 -k 641 -k 532 -k 333 -k 335 -k 336 -k 672 -k 313 -k 232 -k 513 -k 640 -k 673 -k 509 13030205__dvc6000.ddl

pushd ..\SuperDD

set BIN=%LIB%\000026\0006\0302
rem set BIN=%LIB%\0000f9\00f9\0101

set READER=k:\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader

set DDT=k:\Work\TokA_x\Ddt
rem set DDT=C:\Work\TokA\tags\Release_V10-3-10\Ddt
rem set DDT=DdtA.3.09.exe


rem ==========================================================================================
rem ddta release


%DDT%\Release\DdtA.exe  %ARGS%

rem Ddt8 %ARGS%

rem %READER%  %BIN%.fm8
rem mv _RdOutW.log ddta-fm8-release.log

%READER%  -A  %BIN%.fma
mv _RdOutW.log ddta-fma-release.log


rem ==========================================================================================
rem ddta debug

%DDT%\Debug\DdtA.exe %ARGS%

rem %READER%    %BIN%.fm8
rem mv _RdOutW.log ddta-fm8-debug.log

%READER%  -A  %BIN%.fma
mv _RdOutW.log ddta-fma-debug.log

popd