/*****************************************************************************
 *
 * File: Sample2_wireless.ddl
 * Version: 4.1.0
 * Date: 04/24/10
 *
 *****************************************************************************
 * Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: This file is a sample DD intended to demonstrate the 
 *              development of a simple wireless device.  It supports HART 7, 
 *              Universal Commands, a few Common Practice Commands, and all the 
 *              WirelessHART commands.
 *
 *****************************************************************************
 */

/*****************************************************************************
 * The manufacturer line
 *****************************************************************************
 */
// Replace following line with your manufacturer and device type as defined in Devices.cfg
// This is a HART 7 device so we will need treat the device type differently for legacy
// hosts (ie, .fms and .fm6)
MANUFACTURER HCF, 
#if __TOKVER__ >= 800
DEVICE_TYPE _SAMPLE2_WIRELESS_EXP,
#else
DEVICE_TYPE _SAMPLE2_WIRELESS,
#endif
DEVICE_REVISION 1, 
DD_REVISION 1

/*****************************************************************************
 * Include Files
 *****************************************************************************
 */

#include "macros.h"
#include "methods.h"

/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */

/*****************************************************************************
 * Common Tables
 */

IMPORT STANDARD _TABLES, DEVICE_REVISION 20, DD_REVISION 4
{
    HART_DEVICE_TABLES;  // Pulls in all the items for typical HART devices.
    BURST_TABLES;        // Pulls in all the items for HART devices supporting Burst Mode.
    WIRELESS_TABLES;     // Pulls in all the items for WirelessHART devices.

    /*
     * Uncomment the following if the device supports trends.  Corresponding
     * commands will also need to be imported from the COMMON_PRACTICE DD.
     */
    //VAR_TREND_TABLES;   // Pulls in all the items for devices that support TRENDS.

    /* 
     * If this device supports command 64,512 - you may want to pull in the
     * wireless module vendors' device type codes. Following is one possible
     *  example...
     */
    // Uncomment and change vendor to the one this device uses for its wireless radio
    //VARIABLE DUST_NETWORKS_model_code;

    REDEFINITIONS
    {
        VARIABLE device_type
        {
            /* 
             * The type must be redefined if the Device Type is not listed
             * in Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0xf9f8 with your Expanded Device Type code and appropriate label
                { 0xf9f8,  "Sample2 - Wireless Device" }
            }
        }

        VARIABLE manufacturer_id
        {
            /*
             * The type must be redefined if the Manufacturer code is not listed
             * in the Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0x00f9 with your Manufacturer code and appropriate label
                { 0x00f9,  [HART_Communication_Foundation] } 
            }
        }

        VARIABLE private_label_distributor
        {
            /*
             * The type must be redefined if the Private Label code is not listed
             * in the Common Tables DD
             */
            REDEFINE TYPE ENUMERATED (2)
            {
                // Replace the 0x00f9 with your manufactures id and appropriate label
                { 0x00f9,  [HART_Communication_Foundation] } 
            }
        }

        // Commands 821,822 are optional for wireless devices - remove if device does not support
        DELETE VARIABLE network_access_mode;

        // Command 64,512 is optional for wireless devices - remove if device does not support
        DELETE VARIABLE wireless_module_device_type;
        DELETE VARIABLE wireless_module_manufacturer_id;

        // Command 89 is not typically supported by wireless devices (they get time from the network manager)
        DELETE VARIABLE time_set;

        /* 
         * Wireless device MUST support at least the first 13 bytes in
         * command 48. Remove the command 48 bytes that are not used
         * for this device (cmd48 is variable length).  Note that some
         * of the variables come from common9 import, remove the tables
         * ones here, and the rest in the common practice IMPORT section.
         * 
         * The event mask and latched value should also represent the
         * same bytes as defined in command 48.
         */
        DELETE VARIABLE analog_channel_fixed1;
        DELETE VARIABLE analog_channel_fixed1_mask;
        DELETE VARIABLE analog_channel_fixed1_latched_value;
    }
}

/*****************************************************************************
 * Universal Commmands
 */

IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 7, DD_REVISION 2
{
    // All HART devices must support all Universal Commands
    EVERYTHING;

    REDEFINITIONS
    {
        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 3) 
         */
        COMMAND read_dynamic_variables_and_pv_current
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DAQ.ANALOG_VALUE,
                    PV.DEVICE_VARIABLE.DIGITAL_UNITS, PV.DEVICE_VARIABLE.DIGITAL_VALUE
                }
            }
        }

        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 8)
         */
        COMMAND read_dynamic_variable_classification
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DEVICE_VARIABLE.CLASSIFICATION,
                    250,
                    250,
                    250
                }
            }
        }

        // Remove the command 48 status bytes that are not supported by the device
        // See also command 48 related items in the common practice imports.
        DELETE VARIABLE device_specific_status_14;
        DELETE VARIABLE device_specific_status_15;
        DELETE VARIABLE device_specific_status_16;
        DELETE VARIABLE device_specific_status_17;
        DELETE VARIABLE device_specific_status_18;
        DELETE VARIABLE device_specific_status_19;
        DELETE VARIABLE device_specific_status_20;
        DELETE VARIABLE device_specific_status_21;
        DELETE VARIABLE device_specific_status_22;
        DELETE VARIABLE device_specific_status_23;
        DELETE VARIABLE device_specific_status_24;

        /* 
         * Redefine the command 48 status to match
         * the device implementation.  The same should
         * also be done for device_specific_status_0_mask
         * and device_specific_status_0_latched_value
         * example for 1 byte follows: 
         */
        VARIABLE device_specific_status_0
        {
            REDEFINE LABEL "Status example 1";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }

        // Remove the command 48 status bytes that are not supported by the adapter
        // See also command 48 related items in the common practice imports.
        COMMAND read_additional_device_status
        {
            REDEFINE TRANSACTION 0
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0, device_specific_status_1, device_specific_status_2,
                    device_specific_status_3, device_specific_status_4, device_specific_status_5,

                    extended_fld_device_status, 0x00,

                    standardized_status_0, standardized_status_1, analog_channel_saturated1,
                    standardized_status_2, standardized_status_3
                }
            }

            REDEFINE TRANSACTION 1
            {
                REQUEST
                {
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),

                    extended_fld_device_status (INFO), 0x00,

                    standardized_status_0 (INFO), standardized_status_1 (INFO), analog_channel_saturated1 (INFO),
                    standardized_status_2 (INFO), standardized_status_3 (INFO)
                }
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0 (INFO), device_specific_status_1 (INFO), device_specific_status_2 (INFO),
                    device_specific_status_3 (INFO), device_specific_status_4 (INFO), device_specific_status_5 (INFO),

                    extended_fld_device_status (INFO), 0x00,

                    standardized_status_0 (INFO), standardized_status_1 (INFO), analog_channel_saturated1 (INFO),
                    standardized_status_2 (INFO), standardized_status_3 (INFO)
                }
            }
        }
    }
}

/*****************************************************************************
 * PV, when the primary variable is not mappable we can import the PV
 * standard DD to take care of the dynamic variable array, loop current
 * and ranging functions
 */

IMPORT STANDARD _PV, DEVICE_REVISION 1, DD_REVISION 1
{
    EVERYTHING;

    REDEFINITIONS
    {
        /*
         * Remove the device variables not used in this DD.
         */
        DELETE COLLECTION secondary_variable;
        DELETE COLLECTION tertiary_variable;
        DELETE COLLECTION quaternary_variable;

        ARRAY OF COLLECTION dynamic_variables
        {
            ELEMENTS
            {
                DELETE SECONDARY;
                DELETE TERTIARY;
                DELETE QUATERNARY;
            }
        }

        /* 
         * Most devices use the same data item for range units (command 15) and the sensor units.
         * when this is true the following should be added.  if your device uses a different variable
         * for range units you should reference it here 
         */
        COLLECTION OF VARIABLE scaling
        {
            MEMBERS
            {
                ADD RANGE_UNITS, deviceVariables[0].DIGITAL_UNITS, [range_units], [range_units_help];
            } 
        }
    }
}

/*****************************************************************************
 * Common Practice Commands
 */

IMPORT STANDARD _COMMON_PRACTICE, DEVICE_REVISION 9, DD_REVISION 2
{
    // For a wireless device, these are the minimal commands required (in the device) for compliance.
    // Commented items are required in the device, but not modeled in a DD
    COMMAND perform_self_test;                          // Command 41
    COMMAND perform_device_reset;                       // Command 42
    COMMAND write_number_of_response_preambles;         // Command 59
    COMMAND write_device_variable;                      // Command 79
                                                        // 
#if __TOKVER__ >= 800 
    COMMAND read_real_time_clock;                       // Command 90
#endif

    COMMAND write_burst_period;                         // Command 103
    COMMAND write_burst_trigger;                        // Command 104
    COMMAND read_burst_mode_configuration;              // Command 105
    COMMAND flush_delayed_responses;                    // Command 106
    COMMAND write_burst_device_variables;               // Command 107
    COMMAND write_burst_command_number;                 // Command 108
    COMMAND burst_mode_control;                         // Command 109

#if __TOKVER__ >= 600
    COMMAND read_event_notification_summary;            // Command 115
    COMMAND write_event_notification_bit_mask;          // Command 116
    COMMAND write_event_notification_timing;            // Command 117
    COMMAND event_notification_control;                 // Command 118
    COMMAND acknowledge_event_notification;             // Command 119
#endif
                                                        
    COMMAND read_country_code;                          // Command 512
    COMMAND write_country_code;                         // Command 513

    VARIABLE device_variable_code;                      // for Command 79

#if __TOKVER__ >= 800
    VARIABLE current_date;                              // for Command 90
    VARIABLE current_time;                              // for Command 90
    VARIABLE last_clock_time;                           // for Command 90
    VARIABLE last_clock_date;                           // for Command 90
    VARIABLE burst_message_number;                      // for Command 103, 104, 105, 107, 108, 109
    VARIABLE eventNumber;                               // for Command 115, 116, 117, 118, 119
    VARIABLE update_period;                             // for collection burst_message_collection
    VARIABLE max_update_period;                         // for collection burst_message_collection
    VARIABLE burst_variable_code;                       // for collection burst_message_collection
    ARRAY    burst_variable_codes;                      // for collection burst_message_collection
    VARIABLE time_of_first_unack_event;                 // for collection event_control
    VARIABLE event_notification_retry_time_event;       // for collection event_control
    VARIABLE max_update_time_event;                     // for collection event_control
    VARIABLE event_debounce_interval_event;             // for collection event_control
    ARRAY burst_messages;                               // for Command 103, 104, 105, 107, 108, 109
    ARRAY events;                                       // for Command 115, 116, 117, 118, 119

#else
    //VARIABLE burst_message_number_int;                  // for Command 103, 104, 105, 107, 108, 109
    //VARIABLE eventNumber_int;                           // for Command 115, 116, 117, 118, 119
    VARIABLE update_period_int;                         // for collection burst_message_collection
    VARIABLE max_update_period_int;                     // for collection burst_message_collection
    VARIABLE burst_variable_code_1;                     // for Command 105, 107
    VARIABLE burst_variable_code_2;                     // for Command 105, 107                                                        // 
    VARIABLE burst_variable_code_3;                     // for Command 105, 107
    VARIABLE burst_variable_code_4;                     // for Command 105, 107
    VARIABLE burst_variable_code_5;                     // for Command 105, 107
    VARIABLE burst_variable_code_6;                     // for Command 105, 107
    VARIABLE burst_variable_code_7;                     // for Command 105, 107
    VARIABLE burst_variable_code_8;                     // for Command 105, 107
    VARIABLE time_of_first_unack_event_int;             // for collection event_control
    VARIABLE event_notification_retry_time_event_int;   // for collection event_control
    VARIABLE max_update_time_event_int;                 // for collection event_control
    VARIABLE event_debounce_interval_event_int;         // for collection event_control
#endif
 

    VARIABLE total_number_burst_messages;               // for Command 105

    VARIABLE number_events_supported;                   // for Command 115

    VARIABLE burst_command_number;                      // for collection burst_message_collection




    VARIABLE trigger_level;                             // for collection burst_message_collection




    VARIABLE device_specific_status_0_mask;             // for collection event_mask
    VARIABLE device_specific_status_1_mask;             // for collection event_mask
    VARIABLE device_specific_status_2_mask;             // for collection event_mask
    VARIABLE device_specific_status_3_mask;             // for collection event_mask
    VARIABLE device_specific_status_4_mask;             // for collection event_mask
    VARIABLE device_specific_status_5_mask;             // for collection event_mask

    /*
     * The event uses the same number of command 48 mask bytes as command 48 has for status bytes
     * Import the same ones as were imported in the Universal DD.
     */
//  VARIABLE device_specific_status_14_mask;            // for collection event_mask
//  VARIABLE device_specific_status_15_mask;            // for collection event_mask
//  VARIABLE device_specific_status_16_mask;            // for collection event_mask
//  VARIABLE device_specific_status_17_mask;            // for collection event_mask
//  VARIABLE device_specific_status_18_mask;            // for collection event_mask
//  VARIABLE device_specific_status_19_mask;            // for collection event_mask
//  VARIABLE device_specific_status_20_mask;            // for collection event_mask
//  VARIABLE device_specific_status_21_mask;            // for collection event_mask
//  VARIABLE device_specific_status_22_mask;            // for collection event_mask
//  VARIABLE device_specific_status_23_mask;            // for collection event_mask
//  VARIABLE device_specific_status_24_mask;            // for collection event_mask

    VARIABLE device_specific_status_0_latched_value;    // for collection event_report
    VARIABLE device_specific_status_1_latched_value;    // for collection event_report
    VARIABLE device_specific_status_2_latched_value;    // for collection event_report
    VARIABLE device_specific_status_3_latched_value;    // for collection event_report
    VARIABLE device_specific_status_4_latched_value;    // for collection event_report
    VARIABLE device_specific_status_5_latched_value;    // for collection event_report

    // The event uses the same number of command 48 latched bytes as command 48 has for status bytes
    // Import the same ones as were imported in the Universal DD.

//  VARIABLE device_specific_status_14_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_15_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_16_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_17_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_18_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_19_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_20_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_21_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_22_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_23_latched_value;   // for collection event_report
//  VARIABLE device_specific_status_24_latched_value;   // for collection event_report

    VARIABLE config_change_counter_latched_value;       // for collection event_report

    UNIT burst_trigger_units_relation;
    
    COLLECTION burst_message_collection;                // for ARRAY burst_message_array
    COLLECTION event_control;                           // for ARRAY event_array
    COLLECTION event_mask;                              // for ARRAY event_array
    COLLECTION event_report;                            // for ARRAY event_array
    COLLECTION event;                                   // for ARRAY event_array
  
    METHOD return_to_normal;
    METHOD warning_message;
    METHOD device_self_test;
    METHOD device_master_reset;

    REDEFINITIONS
    {
        // Define how many burst messages this device supports
#if __TOKVER__ >= 800
        ARRAY burst_messages
        {
            // A minimum of 3 are required
            REDEFINE NUMBER_OF_ELEMENTS 3;
        }

        // Define how many events this device supports
        ARRAY events
        {
            // A minimum of 1 is required
            REDEFINE NUMBER_OF_ELEMENTS 1;
        }
#endif

        // Define the list of commands that can be burst by the adapter (add any device specific commands too)
        VARIABLE burst_command_number
        {
            REDEFINE TYPE ENUMERATED (2)
            {
                {1,  [read_primary_variable], [pv_help] }, 
                {2,  [read_percent_range_and_current], [percent_rnge_help] }, 
                {3,  [read_dyn_vars_and_current], [all_process_var_help] }, 
                {9,  [read_device_variables_with_status], [read_device_variables_with_status_help] }, 
                {33, [read_device_variables],[read_device_variables_help] },
                {48, "Cmd 48: Read Additional Device Status", "Read Additional Device - This command reads additional device status information"}
                {178,"Cmd 178: My device specific command", "Command that performs a device specific action"}
            }
        }

        // If the time is not directly settable via command 89, make sure to mark these READ_ONLY
#if __TOKVER__ >= 800
        VARIABLE last_clock_date
        {
            REDEFINE HANDLING READ;
        }
        VARIABLE last_clock_time
        {
            REDEFINE HANDLING READ;
        }
#endif

        /* The event masks and reported values should be
           redefined similar to the command 48 byte
           redefinitions in the UNIVERSAL imports. */
        VARIABLE device_specific_status_0_mask
        {
            REDEFINE LABEL "Status example 1 Mask";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }

        VARIABLE device_specific_status_0_latched_value
        {
            REDEFINE LABEL "Latched Status example 1";
            REDEFINE TYPE BIT_ENUMERATED
            {
                { 0x01, "Example condition 1", MISC },
                { 0x02, "Example condition 2", MISC },
                { 0x04, "Example condition 3", MISC },
                { 0x08, "Example condition 4", MISC },
                { 0x10, "Example condition 5", MISC },
                { 0x20, "Example condition 6", MISC },
                { 0x40, "Example condition 7", MISC },
                { 0x80, "Example condition 8", MISC }
            }
        }

        COLLECTION OF VARIABLE burst_message_collection
        {
            MEMBERS
            {
                // Remove the following members for non-IO devices (do not support commands 101 and 102)
                DELETE SUB_DEVICE_MISSING;
                DELETE SUB_DEVICE_MAPPING;
            }
        }

        COLLECTION OF VARIABLE event_control
        {
            MEMBERS
            {
                // Remove the following members for non-IO devices (do not support for commands 101 and 102)
                DELETE SUB_DEVICE_MISSING;
                DELETE SUB_DEVICE_MAPPING;
            }
        }

        COLLECTION OF VARIABLE event_mask
        {
            MEMBERS
            {
                // Event masks are only used for each command 48 byte supported by the device
                // Remove the masks for the command 48 bytes that are not supported by the device.
                DELETE ANALOG_CHANNEL_FIXED1_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_14_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_15_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_16_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_17_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_18_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_19_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_20_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_21_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_22_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_23_MASK;
                DELETE DEVICE_SPECIFIC_STATUS_24_MASK;
            }
        }

        COLLECTION OF VARIABLE event_report
        {
            MEMBERS
            {
                // Latched Event values are only used for each command 48 byte supported by the device
                // Remove the latched values for the command 48 bytes that are not supported by the device.
                DELETE ANALOG_CHANNEL_FIXED1_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_14_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_15_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_16_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_17_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_18_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_19_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_20_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_21_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_22_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_23_LATCHED_VALUE;
                DELETE DEVICE_SPECIFIC_STATUS_24_LATCHED_VALUE;
            }
        }

#if __TOKVER__ >= 600
        // Redefine the 3 event commands to match the proper number of command 48 bytes supported by the device
        // Command 115
        COMMAND read_event_notification_summary
        {
            REDEFINE TRANSACTION
            {
#if __TOKVER__ >= 800
                REQUEST
                {
                    eventNumber (INFO, INDEX)
                }
                REPLY
                {
                    response_code, device_status,
                    eventNumber (INFO, INDEX),
                    number_events_supported,
                    events[eventNumber].EVENT_CONTROL.EVENT_STATUS<0xf0>, 
                    events[eventNumber].EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL<0x0f>,
                    events[eventNumber].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT,
                    events[eventNumber].EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME,
                    events[eventNumber].EVENT_CONTROL.MAX_UPDATE_TIME,
                    events[eventNumber].EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL,
                    events[eventNumber].EVENT_MASK.DEVICE_STATUS_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK, 
                    events[eventNumber].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK, 
                    events[eventNumber].EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    events[eventNumber].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_3_MASK
                }
#else
                REQUEST
                {
                    0
                }
                REPLY
                {
                    response_code, device_status,
                    0,
                    number_events_supported,
                    event.EVENT_CONTROL.EVENT_STATUS<0xf0>, 
                    event.EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL<0x0f>,
                    event.EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT,
                    event.EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME,
                    event.EVENT_CONTROL.MAX_UPDATE_TIME,
                    event.EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL,
                    event.EVENT_MASK.DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK, 
                    event.EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    event.EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    event.EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_3_MASK 
                }
#endif
            }
        }
#endif

#if __TOKVER__ >= 600        
        // Command 116
        COMMAND write_event_notification_bit_mask
        {
            REDEFINE TRANSACTION
            {
#if __TOKVER__ >= 800
                REQUEST
                {
                    eventNumber (INFO, INDEX),
                    events[eventNumber].EVENT_MASK.DEVICE_STATUS_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK,
                    events[eventNumber].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK,
                    events[eventNumber].EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    events[eventNumber].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_3_MASK
                }
                REPLY
                {
                    response_code, device_status,
                    eventNumber (INFO, INDEX),
                    events[eventNumber].EVENT_MASK.DEVICE_STATUS_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    events[eventNumber].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK, 
                    events[eventNumber].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK, 
                    events[eventNumber].EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    events[eventNumber].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    events[eventNumber].EVENT_MASK.STANDARDIZED_STATUS_3_MASK
                }
#else
                REQUEST
                {
                    0,
                    event.EVENT_MASK.DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK,
                    event.EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    event.EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    event.EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_3_MASK 
              
                }
                REPLY
                {
                    response_code, device_status,
                    0,
                    event.EVENT_MASK.DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK, 
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK,  
                    event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK, 
                    event.EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK, 
                    event.EVENT_MASK.DEV_OPERATING_MODE_MASK,
                    event.EVENT_MASK.STANDARDIZED_STATUS_0_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_1_MASK, 
                    event.EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_2_MASK, 
                    event.EVENT_MASK.STANDARDIZED_STATUS_3_MASK
                }
            
#endif
            }
        }
#endif

#if __TOKVER__ >= 600
        // Command 119
        COMMAND acknowledge_event_notification
        {
            REDEFINE TRANSACTION 0  // Used to get the latest outstanding event
            {
#if __TOKVER__ >= 800
                REQUEST
                {
                    eventNumber (INFO, INDEX)
                }
                REPLY
                {
                    response_code, device_status,
                    eventNumber (INFO, INDEX),
                    events[eventNumber].EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE
                    events[eventNumber].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT,
                    events[eventNumber].EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE,  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE,  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE,  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE,  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE,
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE, 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE
                }
#else
                /* For older tokenizers - a single event is defined so that METHODS can
                   send command 119 and read the response from the device. */
                REQUEST
                {
                    0
                }
                REPLY
                {
                    response_code, device_status,
                    0,
                    event.EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE
                    event.EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT,
                    event.EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE, 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE,  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE,  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE, 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE,  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE,  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE, 
                    event.EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE, 
                    event.EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE,
                    event.EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE, 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE, 
                    event.EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE, 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE, 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE
                }
#endif
            }
            REDEFINE TRANSACTION 1 // Used to ACK an outstanding event
            {
#if __TOKVER__ >= 800
                REQUEST
                {
                    eventNumber (INFO, INDEX),
                    events[eventNumber].EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE (INFO),
                    events[eventNumber].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (INFO),
                    events[eventNumber].EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE (INFO),
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE (INFO)
                }
                REPLY
                {
                    response_code, device_status,
                    eventNumber (INFO, INDEX),
                    events[eventNumber].EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE (INFO),
                    events[eventNumber].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (INFO),
                    events[eventNumber].EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE (INFO),  
                    events[eventNumber].EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE (INFO),
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE (INFO), 
                    events[eventNumber].EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE (INFO)
                }
#else
                REQUEST
                {
                    0,
                    event.EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE (INFO),
                    event.EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (INFO),
                    event.EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE (INFO),
                    event.EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE (INFO)  
                }
                REPLY
                {
                    response_code, device_status,
                    0,
                    event.EVENT_REPORT.CONFIG_CHANGE_COUNTER_LATCHED_VALUE (INFO),
                    event.EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (INFO),
                    event.EVENT_REPORT.DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_0_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_1_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_2_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_3_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_4_LATCHED_VALUE (INFO),  
                    event.EVENT_REPORT.DEVICE_SPECIFIC_STATUS_5_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.EXTENDED_FLD_DEVICE_STATUS_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.DEV_OPERATING_MODE_LATCHED_VALUE (INFO),
                    event.EVENT_REPORT.STANDARDIZED_STATUS_0_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_1_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.ANALOG_CHANNEL_SATURATED1_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_2_LATCHED_VALUE (INFO), 
                    event.EVENT_REPORT.STANDARDIZED_STATUS_3_LATCHED_VALUE (INFO)
                }
#endif
            }
        }
#endif
    }
}

IMPORT STANDARD _WIRELESS, DEVICE_REVISION 1, DD_REVISION 2
{
    EVERYTHING;

    REDEFINITIONS
    {
        // Commands 821,822 are optional for wireless devices - remove if device does not support
        DELETE COMMAND write_network_access_mode;
        DELETE COMMAND read_network_access_mode;

        // Commands 64,512 are optional for wireless devices - remove if device does not support
        DELETE VARIABLE wireless_module_transmitter_revision;
        DELETE VARIABLE wireless_module_software_revision;
        DELETE VARIABLE wireless_module_hardware_revision;
        DELETE COMMAND read_wireless_module_revision;
    }
}

// Common items for a wireless device (modify as needed for your particular device)
VARIABLE fl_batteryLife
{
    LABEL "Battery Life";
    HELP [digital_value_help];
    CLASS DEVICE & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryUSL
{
    LABEL [usl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryLSL
{
    LABEL [lsl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryDamping
{
    LABEL [damping_value];
    HELP [seconds_damping_value_help];
    HANDLING READ;
    CONSTANT_UNIT "s";
    TYPE FLOAT;
}

VARIABLE batteryMinSpan
{
    LABEL [minimum_span];
    HELP [minimum_span_help];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE batteryUnits
{
    LABEL "Units";
    HELP [digital_units_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // Battery life only supports units of Days
        {53, "Days"}        // Standard table for [days] is d.  We want "Days".
    }
}

UNIT battery_relation
{
    batteryUnits:
        fl_batteryLife,
        batteryUSL,
        batteryLSL,
        batteryMinSpan
}

VARIABLE battery_data_quality
{
    LABEL [process_data_quality];
    HELP [process_data_quality_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        PROCESS_DATA_STATUS_CODES
    }
}

VARIABLE battery_limit_status
{
    LABEL [limit_status];
    HELP [limit_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        LIMIT_STATUS_CODES
    }
}

VARIABLE battery_family_status
{
    LABEL [family_status];
    HELP [family_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // Battery life is not part of a device family
        { 0x08, [more_device_family_status_available]}
    }
}

VARIABLE battery_classification
{   
    LABEL [classification];
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // Battery Life is not classified
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0)
    }
}

VARIABLE battery_family
{   
    LABEL [device_family];
    HELP [device_family_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // Battery Life is not in a family
        DEVICE_VARIABLE_FAMILY_CODE(250)
    }
}

VARIABLE battery_serialNum
{   
    LABEL [sensor_serial_number];
    HELP [sensor_serial_number_help];
    TYPE UNSIGNED_INTEGER(3); 
}

#if __TOKVER__ >= 800
VARIABLE battery_update_period 
{
    LABEL "Update Period";
    TYPE TIME_VALUE;
    HANDLING READ;
}
#endif

VARIABLE battery_write_dev_var_code
{
    LABEL "Battery write dev var code";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

COLLECTION OF VARIABLE batteryLife
{
    LABEL "Battery Life";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              fl_batteryLife;
        DIGITAL_UNITS,              batteryUnits;
        DATA_QUALITY,               battery_data_quality;
        LIMIT_STATUS,               battery_limit_status;
        DEVICE_FAMILY_STATUS,       battery_family_status;

        CLASSIFICATION,             battery_classification;
        DEVICE_FAMILY,              battery_family;

        UPPER_SENSOR_LIMIT,         batteryUSL;
        LOWER_SENSOR_LIMIT,         batteryLSL;
        MINIMUM_SPAN,               batteryMinSpan;
        DAMPING_VALUE,              dummy_variable_float;

        #if __TOKVER__ >= 800
        UPDATE_TIME_PERIOD,         battery_update_period;
        #endif

        SIMULATED,                  battery_write_dev_var_code;

        // Device Information
        SENSOR_SERIAL_NUMBER,       battery_serialNum;
    }
}

VARIABLE dummy_variable
{
    HANDLING READ;
    TYPE UNSIGNED_INTEGER;
}

VARIABLE dummy_variable_float
{
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE dummy_variable_unsigned_3
{
    HANDLING READ;
    TYPE UNSIGNED_INTEGER (3);
}

#if __TOKVER__ >= 800
VARIABLE dummy_variable_time
{
    HANDLING READ;
    TYPE TIME_VALUE;
}
#endif

/* This collection is used to point to for all the
   device variables that are really not used, but
   required to be defined so that the burst variables
   indexes are valid. Referring to any items in this
   collection is not recommended. */
COLLECTION OF VARIABLE scratchData
{
    LABEL "Scratch Data";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              dummy_variable_float;
        DIGITAL_UNITS,              dummy_variable;
        DATA_QUALITY,               dummy_variable;
        LIMIT_STATUS,               dummy_variable;
        DEVICE_FAMILY_STATUS,       dummy_variable;

        CLASSIFICATION,             dummy_variable;
        DEVICE_FAMILY,              dummy_variable;

        UPPER_SENSOR_LIMIT,         dummy_variable_float;
        LOWER_SENSOR_LIMIT,         dummy_variable_float;
        MINIMUM_SPAN,               dummy_variable_float;
        DAMPING_VALUE,              dummy_variable_float;

        #if __TOKVER__ >= 800
        UPDATE_TIME_PERIOD,         dummy_variable_time;
        #endif

        SIMULATED,                  dummy_variable;

        // Device Information
        SENSOR_SERIAL_NUMBER,       dummy_variable_unsigned_3;
    }
}

// Now model any device specific information contained in your device

/* 
 * This is the minimum set of members that any device variable collection must have
 * Define these for each device variable contained in your device, and then add to
 * the deviceVariables array.
 */
VARIABLE var0_value
{
    LABEL "Dev Var 0";
    HELP [digital_value_help];
    CLASS DEVICE & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_usl
{
    LABEL [usl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_lsl
{
    LABEL [lsl];
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE var0_damping
{
    LABEL [damping_value];
    HELP [seconds_damping_value_help];
    HANDLING READ;
    CONSTANT_UNIT "s";
    TYPE FLOAT;
}

VARIABLE var0_minSpan
{
    LABEL [minimum_span];
    HELP [minimum_span_help];
    // stevev temp     HANDLING READ;
    TYPE FLOAT
    {
      // the following is a temp test of encoding
    MIN_VALUE -115;
    MAX_VALUE  115;
    }
}

VARIABLE var0_units
{
    LABEL "Units";
    HELP [digital_units_help];
    TYPE ENUMERATED
    {
        // List Units code that apply to your device variable
        UNITS_CODE(32),
        UNITS_CODE(33)
    }
}

UNIT var0_relation
{
    var0_units:
        var0_value,
        var0_usl,
        var0_lsl,
        var0_minSpan
}

VARIABLE var0_data_quality
{
    LABEL [process_data_quality];
    HELP [process_data_quality_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        PROCESS_DATA_STATUS_CODES
    }
}

VARIABLE var0_limit_status
{
    LABEL [limit_status];
    HELP [limit_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        LIMIT_STATUS_CODES
    }
}

VARIABLE var0_family_status
{
    LABEL [family_status];
    HELP [family_status_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // List applicable family status if appropriate
        { 0x08, [more_device_family_status_available]}
    }
}

VARIABLE var0_classification
{   
    LABEL [classification];
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // List the proper classification code
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0)
    }
}

VARIABLE var0_family
{   
    LABEL [device_family];
    HELP [device_family_help];
    HANDLING READ;
    TYPE ENUMERATED 
    {
        // List proper family code
        DEVICE_VARIABLE_FAMILY_CODE(250)
    }
}

VARIABLE var0_serialNum
{   
    LABEL [sensor_serial_number];
    HELP [sensor_serial_number_help];
    TYPE UNSIGNED_INTEGER(3); 
}

#if __TOKVER__ >= 800
VARIABLE var0_update_period 
{
    LABEL "Update Period";
    TYPE TIME_VALUE;
    HANDLING READ;
}
#endif

VARIABLE var0_write_dev_var_code
{
    LABEL "Dev Var 0 write dev var code";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

COLLECTION OF VARIABLE devVar0
{
    LABEL "Device Var 0";
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              var0_value;
        DIGITAL_UNITS,              var0_units;
        DATA_QUALITY,               var0_data_quality;
        LIMIT_STATUS,               var0_limit_status;
        DEVICE_FAMILY_STATUS,       var0_family_status;

        CLASSIFICATION,             var0_classification;
        DEVICE_FAMILY,              var0_family;

        UPPER_SENSOR_LIMIT,         var0_usl;
        LOWER_SENSOR_LIMIT,         var0_lsl;
        MINIMUM_SPAN,               var0_minSpan;
        DAMPING_VALUE,              var0_damping;

        #if __TOKVER__ >= 800
        UPDATE_TIME_PERIOD,         var0_update_period;
        #endif

        SIMULATED,                  var0_write_dev_var_code;

        // Device Information
        SENSOR_SERIAL_NUMBER,       var0_serialNum;
    }
}

ARRAY OF COLLECTION deviceVariables 
{
    ELEMENTS
    {
          0, devVar0,             "Device Variable 0";
        243, batteryLife,         device_variable_code_codes(243);
        246, devVar0,             device_variable_code_codes(246);
        250, scratchData,         [not_used];
    }
}

/***************************************************************************
 * below is an example menu for a wireless device  Modify to fit the needs
 * of your device.
 */

MENU root_menu
{
    LABEL [bus_devices];
    ITEMS
    {
        device_setup,                                               // menu
        PV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE)
    }
}

MENU device_setup
{
    LABEL [device_setup];
    ITEMS
    {
        process_variables,                                          // menu
        diag_service,                                               // menu
        basic_setup,                                                // menu
        detailed_setup,                                             // menu
        review (REVIEW)                                             // menu,
        var0_minSpan  // stevev test min max
    }
}

MENU process_variables
{
    LABEL [process_variables];
    ITEMS
    {
        deviceVariables[0].DIGITAL_VALUE (DISPLAY_VALUE),

        /* 
         * Add your device variables here
         */
        deviceVariables[243].DIGITAL_VALUE (DISPLAY_VALUE)          // Battery life if applicable
    }
}

MENU diag_service
{
    LABEL [diagnostics_and_service];
    ITEMS
    {
        test_device                                                 // menu
    }
}

MENU test_device
{
    LABEL [test_device];
    ITEMS
    {
        status_display,                                             // menu
        wireless_status,                                            // menu
#if __TOKVER__ >= 600
        event_status_menu,                                          // menu
#endif
        device_self_test,                                           // method
        device_master_reset                                         // method
    }
}

MENU status_display
{
    LABEL "Device Status";
    ITEMS
    {
        device_status (DISPLAY_VALUE),
        extended_fld_device_status (DISPLAY_VALUE),
        standardized_status_0 (DISPLAY_VALUE),
        standardized_status_1 (DISPLAY_VALUE),
        standardized_status_2 (DISPLAY_VALUE),
        standardized_status_3 (DISPLAY_VALUE),
        device_specific_status_0 (DISPLAY_VALUE),
        device_specific_status_1 (DISPLAY_VALUE),
        device_specific_status_2 (DISPLAY_VALUE),
        device_specific_status_3 (DISPLAY_VALUE),
        device_specific_status_4 (DISPLAY_VALUE),
        device_specific_status_5 (DISPLAY_VALUE),

    /*
     * Add any additional device specific status here
     */
        config_change_counter (DISPLAY_VALUE),
        battery_life (DISPLAY_VALUE),
        device_power_status (DISPLAY_VALUE)
#if __TOKVER__ >= 800
        time                                                        // menu
#endif
    }
}

MENU basic_setup
{
    LABEL [basic_setup];
    ITEMS
    {
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        PV.DEVICE_VARIABLE.DIGITAL_UNITS,
        device_info                                                 // menu
    }
}

MENU detailed_setup
{
    LABEL [detailed_setup];
    ITEMS
    {
        output_conditioning,                                        // menu
        device_info,                                                // menu
        wireless_info                                               // menu
    }
}

MENU device_info
{
    LABEL [device_information];
    ITEMS
    {
        private_label_distributor (DISPLAY_VALUE),
        device_type (DISPLAY_VALUE),
        device_id (DISPLAY_VALUE),
        config_change_counter (DISPLAY_VALUE),
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        date (DISPLAY_VALUE),
        write_protect (DISPLAY_VALUE),
        descriptor (DISPLAY_VALUE),
        message (DISPLAY_VALUE),
        final_assembly_number (DISPLAY_VALUE),
        country_code (DISPLAY_VALUE),
        si_control (DISPLAY_VALUE),
        physical_signaling_code (DISPLAY_VALUE),
        power_source (DISPLAY_VALUE),
        device_flags (DISPLAY_VALUE),
        device_profile (DISPLAY_VALUE),
        device_capabilities,                                        // menu
        device_revisions                                            // menu
    }
}

MENU device_revisions
{
    LABEL [revision_numbers];
    ITEMS
    {
        universal_revision (DISPLAY_VALUE),
        transmitter_revision (DISPLAY_VALUE),
        software_revision (DISPLAY_VALUE),
        hardware_revision (DISPLAY_VALUE)
    }
}

MENU output_conditioning
{
    LABEL [output_condition];
    ITEMS
    {
        hart_output,                                                // menu
        wireless_output                                             // menu
    }
}

MENU hart_output
{
    LABEL [HART_output];
    ITEMS
    {
        polling_address (DISPLAY_VALUE),
        request_preambles (DISPLAY_VALUE),
        response_preambles (DISPLAY_VALUE),
        burst_config                                                // menu
#if __TOKVER__ >= 800
        event_config                                                // menu
#endif
    }
}

MENU review
{
    LABEL [review];
    ITEMS
    {
        device_type (DISPLAY_VALUE, READ_ONLY),
        private_label_distributor (DISPLAY_VALUE, READ_ONLY),
        write_protect (DISPLAY_VALUE, READ_ONLY),
        device_id (DISPLAY_VALUE, READ_ONLY),
        device_nickname_address (DISPLAY_VALUE, READ_ONLY),
        config_change_counter (DISPLAY_VALUE, READ_ONLY),
        max_num_device_variables  (DISPLAY_VALUE, READ_ONLY),
        tag (DISPLAY_VALUE, READ_ONLY),
        longTag (DISPLAY_VALUE, READ_ONLY),
        descriptor (DISPLAY_VALUE, READ_ONLY),
        message (DISPLAY_VALUE, READ_ONLY),
        date (DISPLAY_VALUE, READ_ONLY),
        final_assembly_number (DISPLAY_VALUE),
        universal_revision (DISPLAY_VALUE, READ_ONLY),
        transmitter_revision (DISPLAY_VALUE, READ_ONLY),
        software_revision (DISPLAY_VALUE, READ_ONLY),
        polling_address (DISPLAY_VALUE, READ_ONLY),
        loop_current_mode (DISPLAY_VALUE, READ_ONLY),
        request_preambles (DISPLAY_VALUE, READ_ONLY),
        response_preambles (DISPLAY_VALUE, READ_ONLY)
    }
}

MENU wireless_status
{
    LABEL "Wireless Status";
    ITEMS
    {
        wireless_mode (DISPLAY_VALUE),
        join_status (DISPLAY_VALUE),
        advertisements (DISPLAY_VALUE),
        neighbor_count (DISPLAY_VALUE),
        join_attempts (DISPLAY_VALUE),
#if __TOKVER__ >= 800
        network_search_timer (DISPLAY_VALUE),
#else
        network_search_timer_int (DISPLAY_VALUE),
#endif
        wireless_statistics                                         // menu
    }
}

MENU wireless_statistics
{
    LABEL "Wireless Statistics";
    ITEMS
    {
        generated_packets (DISPLAY_VALUE),
        terminated_packets (DISPLAY_VALUE),
        detected_crc_errors (DISPLAY_VALUE),
        detected_dll_mic_failures (DISPLAY_VALUE),
        detected_nl_mic_failures (DISPLAY_VALUE),
        not_received_nonce_counter_values (DISPLAY_VALUE)
    }
}

#if __TOKVER__ >= 600
MENU event_status_menu
{
    LABEL "Event Status";
    ITEMS
    {
        event1_status
    }
}
#endif

#if __TOKVER__ >= 600
MENU event1_status
{
    LABEL "Event 1";
    ITEMS
    {
#if __TOKVER__ >= 800
        events[0].EVENT_CONTROL.EVENT_STATUS (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (DISPLAY_VALUE)
#else
        event.EVENT_CONTROL.EVENT_STATUS (DISPLAY_VALUE),
        event.EVENT_CONTROL.TIME_OF_FIRST_UNACK_EVENT (DISPLAY_VALUE)
#endif
    }
}
#endif

#if __TOKVER__ >= 800
MENU time
{
    LABEL "Time";
    ITEMS
    {
        current_date(DISPLAY_VALUE),
        current_time(DISPLAY_VALUE),
        last_clock_date(DISPLAY_VALUE),
        last_clock_time(DISPLAY_VALUE),
        real_time_clock_flag (DISPLAY_VALUE)
    }
}
#endif

MENU device_capabilities
{
    LABEL "Device Capabilities";
    ITEMS
    {
        total_number_burst_messages (DISPLAY_VALUE),
        number_events_supported (DISPLAY_VALUE)
    }
}

MENU wireless_info
{
    LABEL "Wireless Info";
    ITEMS
    {
        /*wireless_module_manufacturer_id (DISPLAY_VALUE),*/        // Optional
        /*wireless_module_device_type (DISPLAY_VALUE),*/            // Optional
        /*wireless_module_transmitter_revision (DISPLAY_VALUE),*/   // Optional
        /*wireless_module_software_revision (DISPLAY_VALUE),*/      // Optional
        /*wireless_module_hardware_revision (DISPLAY_VALUE),*/      // Optional
        device_nickname_address (DISPLAY_VALUE),
#if __TOKVER__ >= 800
        join_retry_timer (DISPLAY_VALUE),
        keep_alive_min_time (DISPLAY_VALUE),
#else
        join_retry_timer_int (DISPLAY_VALUE),
        keep_alive_min_time_int (DISPLAY_VALUE),
#endif
        wireless_capabilities                                       // menu
    }
}

MENU wireless_capabilities
{
    LABEL "Wireless Capabilities";
    ITEMS
    {
        max_neighbors (DISPLAY_VALUE),
        max_packet_buffers (DISPLAY_VALUE),
        receive_signal_level (DISPLAY_VALUE),

#if __TOKVER__ >= 800
        recover_time (DISPLAY_VALUE),
        peak_packet_load (DISPLAY_VALUE),
#else
        recover_time_int (DISPLAY_VALUE),
        peak_packet_load_int (DISPLAY_VALUE),
#endif
        peak_packets_per_second (DISPLAY_VALUE)
    }
}

MENU burst_config
{
    LABEL "Burst Configuration";
    ITEMS
    {
        burst_menu_1                                                // menu

#if __TOKVER__ >= 800
        burst_menu_2,                                               // menu
        burst_menu_3                                                // menu
#endif
    }
}

MENU burst_menu_1
{
    LABEL "Burst Message 1";
    ITEMS
    {
#if __TOKVER__ >= 800
        burst_messages[0].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[0].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[0].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[0].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[0].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[0].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[0].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[0].TRIGGER_LEVEL (DISPLAY_VALUE),
#else
        burst_message_collection.BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_message_collection.COMMAND_NUMBER (DISPLAY_VALUE),
        burst_message_collection.UPDATE_PERIOD (DISPLAY_VALUE),
        burst_message_collection.MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_message_collection.BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_message_collection.BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_message_collection.BURST_UNIT (DISPLAY_VALUE),
        burst_message_collection.TRIGGER_LEVEL (DISPLAY_VALUE),
#endif
        burst_vars1                                                 // menu
    }
}

MENU burst_vars1
{
    LABEL "Burst Variables";
#if __TOKVER__ >= 800
    VALIDITY  IF ( ( burst_messages[0].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[0].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
#elif __TOKVER__ >= 600
    VALIDITY  IF ( ( burst_message_collection.COMMAND_NUMBER == 33 ) ||
                   ( burst_message_collection.COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
#endif
    ITEMS
    {
#if __TOKVER__ >= 800
        burst_messages[0].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[0].BURST_VAR[7] (DISPLAY_VALUE)
#else 
        burst_variable_code_1 (DISPLAY_VALUE),
        burst_variable_code_2 (DISPLAY_VALUE),
        burst_variable_code_3 (DISPLAY_VALUE),
        burst_variable_code_4 (DISPLAY_VALUE),
        burst_variable_code_5 (DISPLAY_VALUE),
        burst_variable_code_6 (DISPLAY_VALUE),
        burst_variable_code_7 (DISPLAY_VALUE),
        burst_variable_code_8 (DISPLAY_VALUE)
#endif
    }
}

#if __TOKVER__ >= 800
MENU burst_menu_2
{
    LABEL "Burst Message 2";
    ITEMS
    {
        burst_messages[1].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[1].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[1].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[1].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[1].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[1].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[1].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[1].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars2                                                 // menu
    }
}


MENU burst_vars2
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[1].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[1].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[1].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[1].BURST_VAR[7] (DISPLAY_VALUE)
    }
}

MENU burst_menu_3
{
    LABEL "Burst Message 3";
    ITEMS
    {
        burst_messages[2].BURST_MODE_SELECT (DISPLAY_VALUE),
        burst_messages[2].COMMAND_NUMBER (DISPLAY_VALUE),
        burst_messages[2].UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[2].MAX_UPDATE_PERIOD (DISPLAY_VALUE),
        burst_messages[2].BURST_TRIGGER_MODE (DISPLAY_VALUE),
        burst_messages[2].BURST_CLASSIFICATION (DISPLAY_VALUE),
        burst_messages[2].BURST_UNIT (DISPLAY_VALUE),
        burst_messages[2].TRIGGER_LEVEL (DISPLAY_VALUE),
        burst_vars3                                                 // menu
    }
}

MENU burst_vars3
{
    LABEL "Burst Variables";
    VALIDITY  IF ( ( burst_messages[2].COMMAND_NUMBER == 33 ) ||
                   ( burst_messages[2].COMMAND_NUMBER == 9  )  )   {TRUE;}   ELSE  {FALSE;}
    ITEMS
    {
        burst_messages[2].BURST_VAR[0] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[1] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[2] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[3] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[4] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[5] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[6] (DISPLAY_VALUE),
        burst_messages[2].BURST_VAR[7] (DISPLAY_VALUE)
    }
}
#endif

#if __TOKVER__ >= 600
MENU event_config
{
    LABEL "Event Configuration";
    ITEMS
    {
        event_menu_1                                                // menu
    }
}
#endif

#if __TOKVER__ >= 600
MENU event_menu_1
{
    LABEL "Event 1";
    ITEMS
    {
#if __TOKVER__ >= 800
        events[0].EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.MAX_UPDATE_TIME (DISPLAY_VALUE),
        events[0].EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL (DISPLAY_VALUE),
#else
        event.EVENT_CONTROL.EVENT_NOTIFICATION_CONTROL (DISPLAY_VALUE),
        event.EVENT_CONTROL.EVENT_NOTIFICATION_RETRY_TIME (DISPLAY_VALUE),
        event.EVENT_CONTROL.MAX_UPDATE_TIME (DISPLAY_VALUE),
        event.EVENT_CONTROL.EVENT_DEBOUNCE_INTERVAL (DISPLAY_VALUE),
#endif
        event_mask_1                                                // menu
    }
}
#endif

#if __TOKVER__ >= 600
MENU event_mask_1
{
    LABEL "Event Mask";
    ITEMS
    {
#if __TOKVER__ >= 800
        events[0].EVENT_MASK.DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_0_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_2_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.STANDARDIZED_STATUS_3_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEV_OPERATING_MODE_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK (DISPLAY_VALUE),
        events[0].EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK (DISPLAY_VALUE)
#else
        event.EVENT_MASK.DEVICE_STATUS_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.EXTENDED_FLD_DEVICE_STATUS_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.STANDARDIZED_STATUS_0_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.STANDARDIZED_STATUS_1_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.STANDARDIZED_STATUS_2_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.STANDARDIZED_STATUS_3_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.ANALOG_CHANNEL_SATURATED1_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEV_OPERATING_MODE_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_0_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_1_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_2_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_3_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_4_MASK (DISPLAY_VALUE),
        event.EVENT_MASK.DEVICE_SPECIFIC_STATUS_5_MASK (DISPLAY_VALUE)
#endif
    }
}
#endif

MENU wireless_output
{
    LABEL "WirelessHART Output";
    ITEMS
    {
        join_network,                                               // method
        network_id (DISPLAY_VALUE),
        join_mode (DISPLAY_VALUE),
#if __TOKVER__ >= 800
        join_shed_time (DISPLAY_VALUE),
#else
        join_shed_time_int (DISPLAY_VALUE),
#endif
        radio_power_output (DISPLAY_VALUE)
    }
}

METHOD forceJoinMode
{
    LABEL "Force Join";
    HELP  "";
    DEFINITION 
    {
        char status[STATUS_SIZE];

        do
        {
            GET_DEV_VAR_VALUE("Enter Join Mode ", join_mode);

#if __TOKVER__ >= 800
            GET_DEV_VAR_VALUE("Enter Join Shed Time", join_shed_time);
#else
            GET_DEV_VAR_VALUE("Enter Join Shed Time", join_shed_time_int);
#endif

            send(771,status);
    
            if (status[STATUS_RESPONSE_CODE])
            {
                display_response_status(771, status[STATUS_RESPONSE_CODE]);
            }
    
        }while(status[STATUS_RESPONSE_CODE]);

        send_command(769);
    }
}

METHOD join_network
{
    LABEL "Join Network";
    HELP  "";
    DEFINITION 
    {
        char status[STATUS_SIZE];
        int iJoin;
 
        IGNORE_ALL_RESPONSE_CODES();
        IGNORE_ALL_DEVICE_STATUS();
        XMTR_IGNORE_ALL_DEVICE_STATUS();

        GET_DEV_VAR_VALUE("Enter Network ID", network_id);

        GET_DEV_VAR_VALUE("Enter Join Key 1", join_key_1);
        GET_DEV_VAR_VALUE("Enter Join Key 2", join_key_2);
        GET_DEV_VAR_VALUE("Enter Join Key 3", join_key_3);
        GET_DEV_VAR_VALUE("Enter Join Key 4", join_key_4);

        send(773,status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(773, status[STATUS_RESPONSE_CODE]);
        }

        send(768,status);
        if (status[STATUS_RESPONSE_CODE])
        {
            display_response_status(768, status[STATUS_RESPONSE_CODE]);
        }
    

       iassign(join_mode, 1);     // join now

#if __TOKVER__ >= 800
       iassign(join_shed_time, 115200000); // search for 1 hour
#else
       iassign(join_shed_time_int, 115200000); // search for 1 hour
#endif

       send(771,status);
       if (status[STATUS_RESPONSE_CODE])
       {
           display_response_status(771, status[STATUS_RESPONSE_CODE]);
       }


        if (ivar_value(join_mode) == 1 ) // only loop if we told device to join now
        {
            do
            {
                send_command(769);
                iJoin = ivar_value(join_status);
                if (iJoin&0x400)
                {
                    DELAY (5, "Join Status - Join complete");
                }
                else if (iJoin&0x040)
                {
                    DELAY (5, "Join Status - Join failed");
                }
                else if (iJoin&0x020)
                {
                    DELAY (5, "Join Status - Join retrying");  
                }
                else if (iJoin&0x200)
                {
                    DELAY (5, "Join Status - Network bandwidth requested");  
                }
                else if (iJoin&0x100)
                {
                    DELAY (5, "Join Status - Network joined");  
                }
                else if (iJoin&0x080)
                {
                    DELAY (5, "Join Status - Network security clearance granted");  
                }
                else if (iJoin&0x010)
                {
                    DELAY (5, "Join Status - Network admission requested");  
                }
                else if (iJoin&0x008)
                {
                    DELAY (5, "Join Status - WirelessHart signal identified");  
                }
                else if (iJoin&0x004)
                {
                    DELAY (5, "Join Status - Wireless time synchronized");  
                }
                else if (iJoin&0x002)
                {
                    DELAY (5, "Join Status - Wireless signal identified");  
                }
                else if (iJoin&0x001)
                {
                    DELAY (5, "Join Status - Network packets heard");  
                }
                else if (iJoin==0)
                {
                    DELAY (5, "Join Status - Network not found");  
                }
                else
                {
                    DELAY (5, "Join Status - Unknown");
                    iJoin = 0;
                } 
                
            } while (!((iJoin & 0x040)||(iJoin & 0x400)));
        }
    }
}
