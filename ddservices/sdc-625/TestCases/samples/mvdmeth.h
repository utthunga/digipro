/******************************************************************
**	   FILE NAME: mvdmeth.h
**	
**	   DESCRIPTION: This file contains #defines for the various MMI
**                 method strings that were implemented. The file
**                 contains the definitions for the Gemini Series devices.
**
**	   PROGRAMMER: Jeffrey S. Walker
**
**	   CREATION DATE: Dec-18-2000
**
**	   Copyright (c) 2000 by Micro Motion, Inc.
**          All Rights Reserved.
**
**	   This software is furnished under a license and may be used,
**	   copied, or disclosed only in accordance with the terms of
**    such license and with the inclusion of the above copyright
**    notice. This software or any other copies thereof may not
**    be provided or otherwise made available to any other person.
**    No title to the ownership of the software is hereby transferred.
**
*******************************************************************
*	CHANGE LOG
*------------------------------------------------------------------
*	DATE	   NAME		DESCRIPTION
* 12-18-00   jsw      Original
******************************************************************/

/************************************************
                MVD Defines
 ***********************************************/

/* Defines for Validity Statements*/

#define ENABLED         1

/* Sensor Type Codes */
#define CURVED_TUBE    0
#define STRAIGHT_TUBE  1

/* FO Scaling Methods */
#define FREQ_EQ_FLOW    0
#define PULSES_PER_UNIT 1
#define UNITS_PER_PULSE 2

/* Channel A Assignments */
#define MAO1   0

/* Channel B Assignments */
#define MAO2   3
#define FO1    1
#define DO1    4

/* Channel C Assignments */
#define FO2    1
#define DO2    4
#define DI     5

/* Power Source Codes */
#define EXTERNAL 0   /* passive */
#define INTERNAL 1   /* active */

/* Polling Codes */
#define DO_NOT_POLL   0
#define HART_PRI_POLL 1
#define HART_SEC_POLL 2

/* AO Fault Codes */
#define AO_FLT_UPSCALE     0
#define AO_FLT_DOWNSCALE   1
#define AO_FLT_ZERO        3

/* DO Assignment */
#define DO_ASSIGN_FLOW_SWITCH 101

/* Update Rate Codes */
#define UPDATE_20_HZ  0
#define UPDATE_100_HZ 2

/*****************

#define str_pv_assign "NOTE-Send PV assignment before modifying range values."\
"|de|HINWEIS-Vor Einstellung des Messbereiches Messvariable 1 zuweisen/senden."\
"|fr|REMARQUE-Envoyer l'affectation de la var. 1 au transmetteur avant de modifier les valeurs d'�chelle."

#define str_sv_assign "NOTE-Send SV assignment before modifying range values."\
"|de|HINWEIS-Vor Einstellung des Messbereiches Messvariable 2 zuweisen/senden."\
"|fr|REMARQUE-Envoyer l'affectation de la var. 2 au transmetteur avant de modifier les valeurs d'�chelle."

#define str_tv_assign "NOTE-Send TV assignment before modifying frequency scaling."\
"|de|HINWEIS-Vor Einstellung des Messbereiches Messvariable 3 zuweisen/senden."\
"|fr|REMARQUE-Envoyer l'affectation de la var. 3 au transmetteur avant de modifier l'�chelle de fr�quence."

#define str_tot_res "ERROR: Totalizer reset not allowed when flow is not zero."\
"|de|FEHLER: Zaehler rueckstellen bei Durchflu_ nicht moeglich."\
"|fr|ERREUR: R.A.Z. impossible car d�bit non nul."

#define str_stat_no_prob "No Device Problems seen during Selftest"\
"|de|Keine Geraetefehler waehrend des Selbsttests"\
"|fr|Aucun d�faut d�tect� lors de l'auto-test"

#define str_enter_dens "Enter Density"\
"|de|Dichte eingeben"\
"|fr|Entrer la masse volumique"

#define str_cal_in_prog "Calibration in Progress..."\
"|de|Kalibration laeuft..."\
"|fr|Etalonnage en cours..."

#define str_zero_fail "Auto Zero Failed"\
"|de| Fehler Nullpunktabgleich"\
"|fr|Echec de l'auto-z�ro"

#define str_perform_cal "Perform calibration?"\
"|de|Kalibration durchfuehren?"\
"|fr|Proc�der � l'�talonnage?"

#define str_dens_cal_complete "Density Calibration Complete"\
"|de|Dichtekalibration beendet"\
"|fr|Etalonnage en masse vol. termin�"

#define str_auto_loop "WARN-Loop should be removed from automatic control"\
"|de|WARNUNG-Regelkreis evtl. auf Handbetrieb schalten"\
"|fr|ATTENTION-Mettre la boucle en mode manuel"

#define str_freq_level "Choose frequency output level"\
"|de|Frequenzwert auswaehlen"\
"|fr|Choisir une fr�quence"

#define str_freq_choice "10 kHz;Other;End"\
"|de|10 kHz;Anderer Wert;Ende"\
"|fr|10 kHz;Autre;Termin�"

#define str_output_is "Output"\
"|de|Ausgang"\
"|fr|Sortie"

#define str_abort_error "Aborting due to field device error"\
"|de|Abbruch durch Geraetefehler"\
"|fr|Arr�t pr�matur� d� � une erreur du transmetteur"

#define str_freq_fixed "Frequency output is fixed at %0 Hz"\
"|de|Frequenzausgang gesetzt auf %0 Hz"\
"|fr|La sortie impulsions est forc�e � %0 Hz"

#define str_anal_level "Choose analog output level"\
"|de|Stromausgangswert auswaehlen"\
"|fr|Choisir un niveau pour la sortie analogique"

#define str_anal_choice "4 mA;20 mA;Other;End"\
"|de|4mA;20mA;Anderer Wert;Ende"\
"|fr|4 mA;20 mA;Autre;Termin�"

#define str_out_level "Output value:"\
"|de|Ausgangswert:"\
"|fr|Niveau en sortie:"

#define str_ao_fixed "Analog Output is fixed at %0"\
"|de|Stromausg. gesetzt auf %0"\
"|fr|La sortie analogique est forc�e � %0"

#define str_ao1_fixed "Analog Output 1 is fixed at %0"\
"|de|Stromausg.1 gesetzt auf %0"\
"|fr|La sortie analogique 1 est forc�e � %0"

#define str_ao2_fixed "Analog Output 2 is fixed at %0"\
"|de|Stromausg.2 gesetzt auf %0"\
"|fr|La sortie analogique 2 est forc�e � %0"

#define str_conn_ref "Connect reference meter"\
"|de|Referenzanzeige anschl."\
"|fr|Brancher un appareil de mesure de r�f�rence"

#define str_ao_to_4 "About to set analog output to 4.0 mA"\
"|de|Stromausg. wird auf 4.0 mA gesetzt"\
"|fr|La sortie analogique va �tre forc�e � 4,0 mA"

#define str_ao_to_20 "About to set analog output to 20.0 mA"\
"|de|Stromausg. wird auf 20.0 mA gesetzt"\
"|fr|La sortie analogique va �tre forc�e � 20,0 mA"

#define str_ao1_to_n "About to set analog output 1 to %0 mA"\
"|de|Stromausg.1 wird gesetzt auf %0 mA"\
"|fr|La sortie analogique 1 va �tre forc�e � %0 mA"

#define str_ao2_to_n "About to set analog output 2 to %0 mA"\
"|de|Stromausg.2 wird gesetzt auf %0 mA"\
"|fr|La sortie analogique 2 va �tre forc�e � %0 mA"

#define str_enter_val "Enter meter value"\
"|de|Referenzwert eingeben"\
"|fr|Entrer la valeur indiqu�e par l'app. de mesure"

#define str_at_4ma "Is fld dev 4.00 mA output equal to reference meter?"\
"|de|Ausg.Transmitter (4.00 mA) = Ref.Anzeige?"\
"|fr|Est-ce que l'appareil de mesure indique 4,00 mA?"

#define str_at_20ma "Is fld dev 20.00 mA output equal to reference meter?"\
"|de|Ausg.Transmitter (20.00 mA) = Ref.Anzeige?"\
"|fr|Est-ce que l'appareil de mesure indique 20,00 mA?"

#define str_yes_no "Yes;No"\
"|de|Ja;Nein"\
"|fr|Oui;Non"

#define str_selftest_comp "Selftest complete"\
"|de|Selbsttest beendet"\
"|fr|Auto-test termin�"

#define str_return_out "Returning fld dev to original output"\
"|de|Ausgang geht auf aktuell. Me_w. zurck"\
"|fr|La sortie indique � nouveau la valeur instantan�e de la variable"

#define str_temp_off_cal_comp "Temperature Offset Calibration Complete"\
"|de|Kalibration Temp.Offset beendet"\
"|fr|Etalon. du d�calage en temp. termin�"

#define str_temp_slop_cal_comp "Temperature Slope Calibration Complete"\
"|de|Kalibration Temp.Steilheit beendet"\
"|fr|Etalon. de la pente de temp. termin�"

#define str_xmtr_stat_comp "Transmitter status complete"\
"|de|Statusabfrage beendet"\
"|fr|Interrogation �tat transmetteur termin�e"

#define str_xmtr_stat_ok "Transmitter status OK"\
"|de|Status OK"\
"|fr|Etat: OK"

#define str_unit_msg "NOTE-Variables which use this as the units code will be in the previous units until this value is sent to the device"\
"|de|Die Variablen werden solange in der vorherigen Einheit dargestellt, bis diese Einheit dem Gert �bermittelt wurde."\
"|fr|REMARQUE-les variables qui utilisent cette unit� garderons l'unit� ant�rieure tant que la nouvelle unit� n'aura pas �t� envoy�e au transmetteur"

#define str_total_stop_msg "WARN-Stopping totalizer disables frequency output"\
"|de|WARNUNG-Stoppen des Zaehlers sperrt Frequenzausgang"\
"|fr|ATTENTION-L'arr�t des totalisateurs provoque l'arr�t du comptage sur la sortie impulsions"

#define str_fo_scaling_msg "NOTE-Send scaling method selection to the device prior to performing frequency output scaling."\
"|de|ACHTUNG, senden Sie die ausgew�hlte Skalierungs Methode zum Ger�t bevor Sie die Frequenzausg�nge skalieren."\
"|fr|REMARQUE-Envoyer le mode de r�glage s�lectionn� au transmetteur avant de r�gler l'�chelle de la sortie impulsions."

#define str_fd_cal_minimum "NOTE: Flow rate must be greater than 50% of the nominal flow for the meter."\
"|de|ACHTUNG, der Durchfluss muss h�her sein als 50 % vom Nenndurchfluss des Sensors."\
"|fr|REMARQUE-le d�bit doit �tre sup�rieur � 50% du d�bit nominal du capteur."

#define str_sens_type_choice "T-Series;Other"\
"|de|T-Sensor; Anderer"\
"|fr|S�rie T; Autre"

#define str_sens_type "Sensor type:"\
"|de|Sensor Typ:"\
"|fr|Type capteur:"

#define str_abort_error "Aborting due to field device error"\
"|de|Abbruch durch Geraetefehler"\
"|fr|Arr�t pr�matur� d� � une erreur du transmetteur"

#define str_prob_detected "A problem was detected"\
"|de|Fehler gefunden"\
"|fr|Un probl�me a �t� d�tect�"

#define str_abort_fix_retry "Abort;Fix and Retry"\
"|de|Abbruch;Korrigieren und erneut versuchen"\
"|fr|Arr�t pr�matur�; corriger et recommencer"

#define str_return_auto "NOTE-Loop may be returned to automatic control"\
"|de|HINWEIS-Regelkreis evtl. auf Automatik zurckschalten"\
"|fr|REMARQUE-La boucle peut �tre remise en mode automatique"

****************/

/******************************
* Can't do in MMI Dictionary 
******************************/

#define str_flow_zero_do_cal "Flow must be zero\n\nPerform calibration?"\
"|de|Durchflu_ mu_  Null sein\n\nKalibration durchfuehren?"\
"|fr|Le d�bit doit �tre nul\n\nEffectuer l'auto-z�ro?"

#define str_offset_cal "Offset Calibration\nEnter Temp in %{0}"\
"|de|Kalibration Offset\nEingabe Temp. in %{0}"\
"|fr|Etalon. du d�calage\nEntrer la temp. en %{0}"

#define str_slope_cal "Slope Calibration\nEnter Temp in %{0}"\
"|de|Kalibration Steilheit\nEingabe Temp. in %{0}"\
"|fr|Etalon. de la pente\nEntrer la temp. en %{0}"

#define str_cal_zero_std "Calibration in Progress\nZero: %{0}\nStd dev: %{1}"\
"|de|Nullpunktabgleich laeuft\nNullpunkt: %{0}\nStandardabweichg: %{1}"\
"|fr|Auto-z�ro en cours\nZ�ro: %{0}\nEcart type: %{1}"

#define str_zero_comp "Auto Zero Complete\nZero: %{0}\nStd dev: %{1}"\
"|de|Nullpunktabgleich beendet\nNullpunkt: %{0}\nStandardabweichg: %{1}"\
"|fr|Auto-z�ro termin�\nZ�ro: %{0}\nEcart type: %{1}"

#define str_ev1_yes "\nEv1 Triggered: Yes"\
"|de|\nAlarm1 aktiv: Ja"\
"|fr|\nEv�nmt1 activ�: Oui"

#define str_ev1_no "\nEv1 Triggered: No"\
"|de|\nAlarm1 aktiv: Nein"\
"|fr|\nEv�nmt1 activ�: Non"

#define str_ev2_yes "\nEv2 Triggered: Yes"\
"|de|\nAlarm2 aktiv: Ja"\
"|fr|\nEv�nmt2 activ�: Oui"

#define str_ev2_no "\nEv2 Triggered: No"\
"|de|\nAlarm2 aktiv: Nein"\
"|fr|\nEv�nmt2 activ�: Non"

#define str_do1_yes "\nDO1 Triggered: Yes"\
"|de|\nBin�r 1 getriggert: JA"\
"|fr|\nSTOR1 activ�e"

#define str_do1_no "\nDO1 Triggered: No"\
"|de|\nBin�r 1 getriggert: NEIN"\
"|fr|\nSTOR1 d�sactiv�e"
