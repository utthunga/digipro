##############################################################################
#
# File: user.mk
# Version: 4.1.0
# Date: 04/24/10
#
##############################################################################
# Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
##############################################################################
#
# Description: This is an additional makefile included with the DD-IDE. 
#              
# It currently contains everything needed to rebuild all the DDs supplied 
# with the DD-IDE. There are 3 targets of key interest in this makefile.  
#
# It contains entry points for addditional makefile options so that the DD
# Developer can make unique preferences for more control over the DD IDE
#
#  USER_OPTION	Additional Tokenizer dash '-' options
#
#  DD_DEPEND    Dependent DDL include (.dd) and/or #define files (.h) 
#
#  USER_DD      Additional DDL Projects
#
# These preferences are intended to be preserved in future DD IDE upgrades.
#
##############################################################################

##############################################################################
#
# USER		Add any additional tokenizer dash '-' options here

USER_OPTION =

##############################################################################
#
# Dependencies
#
# DD_DEPEND.  Place your dependent DDL include (.dd) and/or #defines files(.h)
#             here to make sure that your main DDL file will tokenize every 
#             time there is a modification to your .dd or .h files.

DD_DEPEND =

##############################################################################
#
# If necessary, you can add your rules for building other DD files which are
# necessary to exist prior to the DD you are tokenizing. 
#
# This is especially important if you are importing a device revision 1 and
# you need that one built prior to tokenizing your device revision 2. 
#
# Use the rules for the sample DDs as a guide for developing 
# you rules.
#
# SAMPLE = $(RELEASE)\0000f9
# SAMPLE1_1	= $(SAMPLE)\00f9\0101$(DD_EXT)
# SAMPLE1_2	= $(SAMPLE)\00f9\0201$(DD_EXT)
# SAMPLES = $(SAMPLE1_1) $(SAMPLE1_2)

# USER_DD = $(SAMPLES)

USER_DD =
