REM compare FM8 reader output from Ddt8 vs DdtA

set READER=c:\work\SDC_hcf007_hatchWorks\APPS\FMA_reader\Release\FMA_reader
set LIB=C:\work\engineering_testlibrary\trunk\Source\LibA



%READER% -R %LIB%\%1%.fm8
mv _RdOutW.log __%2%_8.txt

%READER% -R %LIB%\%1%.fma
mv _RdOutW.log __%2%_A.txt
