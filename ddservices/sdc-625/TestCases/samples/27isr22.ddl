/******************************************************************
**	FILE NAME: 27isr22.ddl
**	
**	DESCRIPTION: This file contains all of the import definitions
**              used in the creation of the DD for the 2000 Series 
**              IS output option.
**              This is the file to be used as the 
**              argument for the 'tokenize' command since it 
**              includes all other files that it needs (see end 
**              of file).
**
**	PROGRAMMER: Dawn Kelsch
**
**	CREATION DATE: April-24-2003
**
**	Copyright (c) 2003 by Micro Motion, Inc.
**       All Rights Reserved.
**
**	This software is furnished under a license and may be used,
**	copied, or disclosed only in accordance with the terms of
** such license and with the inclusion of the above copyright
** notice. This software or any other copies thereof may not
** be provided or otherwise made available to any other person.
** No title to the ownership of the software is hereby transferred.
**
*******************************************************************
*	CHANGE LOG
*------------------------------------------------------------------
*	DATE	   NAME		DESCRIPTION
* 04-24-03  dmk      Original
******************************************************************/

#include "bi_codes.h"
#include "macros.ddl"
#include "mvdmeth.h"

MANUFACTURER MICRO_MOTION, DEVICE_TYPE _2000_IS, DEVICE_REVISION 3, DD_REVISION 2

IMPORT MICRO_MOTION _2000_IS, DEVICE_REVISION 3, DD_REVISION 1
{
	EVERYTHING;

   REDEFINITIONS
   {

	  /* ECR 12931 - API Average Density (19) and API Average Temperature (20) 
	     is missing in PV and SV arrays.  Found in Config I/O DD.  Check other DDs.

         Change: Added API avg density and temperature to PV and SV arrays.
      */

	   COLLECTION OF VARIABLE api_avg_density
       {
         MEMBERS
         {

         /* PV, SV Output Information */
         ADD ANALOG_UNITS,         analog_units,                   [analog_output_units];
         ADD ANALOG_VALUE,         api_avg_dens_analog_value,      [analog_output_value];
         ADD ANALOG_DAMPING,       dens_additional_analog_damping, [analog_output_add_damping];
         ADD UPPER_ENDPOINT_VALUE, dens_analog_upper_limit,        [analog_output_upper_endpoint];
         ADD LOWER_ENDPOINT_VALUE, dens_analog_lower_limit,        [analog_output_lower_endpoint];
         ADD UPPER_RANGE_VALUE,    dens_upper_range_value,         [urv];
         ADD LOWER_RANGE_VALUE,    dens_lower_range_value,         [lrv];
         ADD RANGE_UNITS,          dens_digital_units,             [range_units];
         ADD PERCENT_RANGE,        dens_percent_range,             [percent_of_range];
         ADD ALARM_CODE,           alarm_select,                   [alarm_select];
         ADD TRANSFER_FUNCTION,    mmi_transfer_function,          [transfer_function];
         ADD AO1_CUTOFF,           unknown_float,                  [blank];
         }
       }

	   COLLECTION OF VARIABLE api_avg_temperature
       {
         MEMBERS
         {
         /* PV, SV Output Information */
         ADD ANALOG_UNITS,         analog_units,                   [analog_output_units];
         ADD ANALOG_VALUE,         api_avg_temp_value,             [analog_output_value];
         ADD ANALOG_DAMPING,       temp_additional_analog_damping, [analog_output_add_damping];
         ADD UPPER_ENDPOINT_VALUE, temp_analog_upper_limit,        [analog_output_upper_endpoint];
         ADD LOWER_ENDPOINT_VALUE, temp_analog_lower_limit,        [analog_output_lower_endpoint];
         ADD UPPER_RANGE_VALUE,    temp_upper_range_value,         [urv];
         ADD LOWER_RANGE_VALUE,    temp_lower_range_value,         [lrv];
         ADD RANGE_UNITS,          temp_digital_units,             [range_units];
         ADD PERCENT_RANGE,        temp_percent_range,             [percent_of_range];
         ADD ALARM_CODE,           alarm_select,                   [alarm_select];
         ADD TRANSFER_FUNCTION,    mmi_transfer_function,          [transfer_function];
         ADD AO1_CUTOFF,           unknown_float,                  [blank];
         }
       }

       ARRAY OF VARIABLE pv_array
       {
           ELEMENTS
           {
		        ADD 19, transmitter_variables[19],  [mmi_api_avg_density_digital_value];
		        ADD 20, transmitter_variables[20],  [mmi_api_avg_temperature_digital_value];
           }
       }

       ARRAY OF VARIABLE sv_array
       {
           ELEMENTS
           {
    	        ADD 19, transmitter_variables[19],  [mmi_api_avg_density_digital_value];
		        ADD 20, transmitter_variables[20],  [mmi_api_avg_temperature_digital_value];
           }
       }

       ARRAY OF COLLECTION transmitter_variables_1
       {
           ELEMENTS  
           {     

		        REDEFINE 19, api_avg_density_sv,          [mmi_api_avg_density_digital_value];      
		        REDEFINE 20, api_avg_temperature_sv,      [mmi_api_avg_temperature_digital_value];
           }     
       }

	  /* ECR 12019 - Under test points, the Tseries temperature is displayed.
         The temperature being displayed is -260.  This has caused concern
         to several customers to see this even though they know they don't
         have a T-series.

         Change: Added validity statement to board temp var
      */
      VARIABLE case_temp
		{
         REDEFINE VALIDITY IF (sens_type_code == 1)
           { TRUE; } ELSE { FALSE; }
      }

	   /* ECR 12416 - For Analog and IS units, temperature is not a choice
         for polling var.

         Change: Fixed enum.
      */
      VARIABLE poll_var_1
      {

        REDEFINE TYPE ENUMERATED
        {
             { 251, [none] },
             { 53,  [pressure] },
             { 55,  [temperature] }
        }
      }

      VARIABLE poll_var_2
      {

        REDEFINE TYPE ENUMERATED
        {
             { 251, [none] },
             { 53,  [pressure] },
             { 55,  [temperature] }
        }
      }

	   /* ECR 12470 - 100 Hz var selection list in 275 is wrong

         Change: Updated enumeration with correct list.
      */
      VARIABLE update_rate_var
      {
         REDEFINE TYPE ENUMERATED
         {
             { 0,  [mmi_mass_flow_digital_value]           },
             { 1,  [mmi_temperature_digital_value]         },
             { 3,  [mmi_density_digital_value]             },
             { 5,  [mmi_volume_flow_digital_value]         },
             { 10, [mmi_event1_label]                      },
             { 11, [mmi_event2_label]                      },
             { 15, [mmi_api_density_digital_value]         },
             { 16, [mmi_api_volume_flow_digital_value]     },
             { 19, [mmi_api_avg_density_digital_value]     },
             { 20, [mmi_api_avg_temperature_digital_value] }
         }
      }

	   /* ECR 10567 - When changing mao1 fault level, loop warning screen
         appears.  When changing mao2 fault level, it does not.

         Change: Added mao2 fault vars to loop warning array.
      */
      ARRAY OF VARIABLE loop_warning_variables
      {
           ELEMENTS
           {
               ADD 95, ao_2_fault_code,    [none];
               ADD 96, ao_2_fault_level,   [none];
           }
      }

	   /* ECR 10570 - 275 displays the TC volume flow in L instead of L/min.

         Change: Updated volume unit relations.  API volume was in total
         relation instead of flow relation.
      */
      REDEFINE UNIT vol_flow_units_relation
      {
         vol_flow_digital_units :
           vol_flow_digital_value,
	        vol_flow_digital_cutoff,
	        vol_flow_upper_range_value,
	        vol_flow_lower_range_value,
	        vol_flow_upper_sensor_limit,
	        vol_flow_lower_sensor_limit,
	        vol_flow_minimum_span,
	        vol_flow_hzrate,
	        api_vol_flow_digital_value
      }

      REDEFINE UNIT vtot_units_relation
      {
         vtot_digital_units :
	        vtot_digital_value,
	        vinv_digital_value,
           api_vtot_digital_value,
           api_vinv_digital_value
      }

      /* ECR 10571 - API Setup method has a warning to remove unit from
         automatic control but not a message to return unit to
         automatic control.

         Change: Added message to end of method
      */
      METHOD api_setup
      {
	     REDEFINE DEFINITION
	     {
		    char status[STATUS_SIZE];
		    int numberChoice, letterChoice, enableApi;
		    long var_id[1];
		    char disp_string[100];             
		    char disp_string1[100];             
		    int slen;             
		    int slen1; 

		    slen1 = 100;
		    slen = 100;            

		    XMTR_IGNORE_ALL_DEVICE_STATUS();
		    XMTR_IGNORE_ALL_RESPONSE_CODES();
		    IGNORE_ALL_DEVICE_STATUS();     
		    IGNORE_ALL_RESPONSE_CODES();

          get_dictionary_string(mmi_str_auto_loop,disp_string,slen);
		    ACKNOWLEDGE(disp_string); 

		    add_abort_method(abort_send);

          /* Read the API parameters */
		    send(216, status);  /* read_api_configuration */

          /* Ask user to choose an API table number */
          get_dictionary_string(mmi_choose_api_table_number,disp_string,slen);
          get_dictionary_string(mmi_api_table_number_choice,disp_string1,slen1);
		    numberChoice = SELECT_FROM_LIST(disp_string, disp_string1);

          /* if the table number selected was odd, ask for table letter.
		       even number tables use letter C */
		    if ( (numberChoice == 0) ||    /* number 5  */
		         (numberChoice == 2) ||    /* number 23 */
		         (numberChoice == 4) )     /* number 53 */
		    {
             get_dictionary_string(mmi_choose_api_table_letter,disp_string,slen);
             get_dictionary_string(mmi_api_table_letter_choice,disp_string1,slen1);
		       letterChoice = SELECT_FROM_LIST(disp_string, disp_string1);
		    }
		    else
		    {
             /* must be an even number table so ask user for TEC.
		    	    odd number tables don't use TEC */
             get_dictionary_string(mmi_str_enter_tec,disp_string,slen);
		       GET_DEV_VAR_VALUE(disp_string, api_thermal_exp_coeff);
	    	 }

	    	 /* determine reference temp.  Tables 5, 6, 23 and 24 use 60 degF.
		      Tables 53 and 54 ask the user in degC.  Also change the
		    	user temperature units */
		    if ( (numberChoice == 4) ||    /* number 53 */
		         (numberChoice == 5) )     /* number 54 */
		    {
 		    	 /* change user temp units to degC */
		    	 /* command 53, xmtr var code = 1  xmtr var units code = 32 */
             assign_int(transmitter_variable_code, 1);
		    	 assign_int(transmitter_variables[1].DIGITAL_UNITS, 32);
		       send(53, status); /* write_transmitter_variable_units */

             /* ask user for ref temp in degC */
			    get_dictionary_string(mmi_str_enter_ref_temp,disp_string,slen);
			    assign_float(api_ref_temp, 15.0);
		       GET_DEV_VAR_VALUE(disp_string, api_ref_temp);


		    	 /* refresh temperature units */
		       assign_int(transmitter_variable_code_1, 1);
	          send(33, status); /* read_transmitter_variables */
	          send(54, status); /* read_transmitter_variable_information */

		    }
		    else
		    {
		    	 /* change user temp units to degF */
		    	 /* command 53, xmtr var code = 1  xmtr var units code = 33 */
             assign_int(transmitter_variable_code, 1);
		    	 assign_int(transmitter_variables[1].DIGITAL_UNITS, 33);
		       send(53, status); /* write_transmitter_variable_units */

			    /* refresh temperature units */
		       assign_int(transmitter_variable_code_1, 1);
	          send(33, status); /* read_transmitter_variables */
	          send(54, status); /* read_transmitter_variable_information */

             /* set ref temp to 60 degF */
			    assign_float(api_ref_temp,60.0);
		    }

		    /* change the user density units.  Tables 5 and 6 use degAPI,
		       Tables 23 and 24 use SGU and Tables 53 and 54 use kg/m3 */
		    if ( (numberChoice == 0) ||    /* number 5 */
		         (numberChoice == 1) )     /* number 6 */
		    {
		    	 /* change user density units to degAPI */
		       /* command 53, xmtr var code = 3  xmtr var units code = 104 */
             assign_int(transmitter_variable_code, 3);
		    	 assign_int(transmitter_variables[3].DIGITAL_UNITS, 104);
		    }
		    else if ( (numberChoice == 2) ||   /* number 23 */
		          (numberChoice == 3) )    /* number 24 */
		    {
			    /* change user density units to SGU */
		       /* command 53, xmtr var code = 3  xmtr var units code = 90 */
             assign_int(transmitter_variable_code, 3);
			    assign_int(transmitter_variables[3].DIGITAL_UNITS, 90);
		    }
		    else /* number 53 and 54 */
		    {
		    	/* change user density units to kg/m3 */
		      /* command 53, xmtr var code = 3  xmtr var units code = 92 */
             assign_int(transmitter_variable_code, 3);
			    assign_int(transmitter_variables[3].DIGITAL_UNITS, 92);
		    }
	       send(53, status);  /* write_transmitter_variable_units */

          /* refresh density units and other info */
		    assign_int(transmitter_variable_code_1, 3);
	       send(33, status); /* read_transmitter_variables */
	       send(54, status); /* read_transmitter_variable_information */

          /* write api_ctl_table_type */
		    switch ( numberChoice ) {
		    case 0:  /* number 5 */
             switch ( letterChoice ) {
		       case 0:  /* letter A */
			       assign_int(api_ctl_table_type,17);  /* Table 5A */
                break;

		       case 1:  /* letter B */
			       assign_int(api_ctl_table_type,18);  /* Table 5B */
                break;

		       case 2:  /* letter D */
			       assign_int(api_ctl_table_type,19);  /* Table 5D */
                break;

		       default:  /* invalid letter choice */
			       process_abort();
                break;
		    	}
             break;

		    case 1:  /* number 6 */
		    	assign_int(api_ctl_table_type,36);     /* Table 6C */
             break;

	    	case 2:  /* number 23 */
            switch ( letterChoice ) {
		       case 0:  /* letter A */
			       assign_int(api_ctl_table_type,49);  /* Table 23A */
                break;

		       case 1:  /* letter B */
			       assign_int(api_ctl_table_type,50);  /* Table 23B */
                break;

		       case 2:  /* letter D */
			       assign_int(api_ctl_table_type,51);  /* Table 23D */
                break;

		       default:  /* invalid letter choice */
			       process_abort();
                break;
			    }
             break;

		    case 3:  /* number 24 */
			    assign_int(api_ctl_table_type,68);     /* Table 24C */
             break;

		    case 4:  /* number 53 */
             switch ( letterChoice ) {
		       case 0:  /* letter A */
			       assign_int(api_ctl_table_type,81);  /* Table 53A */
                break;

		       case 1:  /* letter B */
			       assign_int(api_ctl_table_type,82);  /* Table 53B */
                break;

		       case 2:  /* letter D */
			       assign_int(api_ctl_table_type,83);  /* Table 53D */
                break;

		       default:  /* invalid letter choice */
			       process_abort();
                break;
			    }
			    break;

		    case 5:  /* number 54 */
			    assign_int(api_ctl_table_type,100);     /* Table 54C */
             break;

		    default:  /* invalid number choice */
	          process_abort();
             break;
          }

		    /* ask user if they want to enable the API calculations */
          get_dictionary_string(mmi_str_enable_api,disp_string,slen);
          get_dictionary_string(mmi_str_yes_no,disp_string1,slen1);
		    enableApi = SELECT_FROM_LIST(disp_string, disp_string1);
		    if (enableApi == 0)  /* yes */
		      assign_int(enable_api, 1);
		    else                 /* no */
		      assign_int(enable_api, 0);

          /* write the API parameters */		
		    send(217, status);  /* write_api_configuration */
		    if ( status[STATUS_RESPONSE_CODE] )
	    	{
		    	display_response_status(217,status[STATUS_RESPONSE_CODE]); 
             get_dictionary_string(mmi_str_abort_error,disp_string,slen);
		    	DELAY(2, disp_string);
		    	process_abort();
		    }

          get_dictionary_string(mmi_str_return_auto,disp_string,slen);
          ACKNOWLEDGE(disp_string);
	     }
      }

      /* Change: Removed while loop around sending cmd 184.  This was causing
         an infinite loop when the transmitter was in custody transfer mode.
         The command would get an acess restricted response and keep sending
         the command.
      */
      METHOD abort_auto_zero
      {

	      REDEFINE DEFINITION
	      {
		      char status[STATUS_SIZE];
		      long var_id[1];
		      char disp_string[100];             
		      int slen;             

		      slen = 100;


		      XMTR_IGNORE_ALL_DEVICE_STATUS();
		      XMTR_IGNORE_ALL_RESPONSE_CODES();
		      IGNORE_ALL_DEVICE_STATUS();     
		      IGNORE_ALL_RESPONSE_CODES(); 

		      send(184,status);
            get_dictionary_string(mmi_str_return_auto,disp_string,slen);
            ACKNOWLEDGE(disp_string);

	      }
      }


   } /* REDEFINITIONS */

} /* IMPORT */


/*********************************
* New Additions 
*********************************/


VARIABLE api_avg_dens_analog_value
{
  CLASS DYNAMIC&ANALOG_OUTPUT;
  LABEL [mmi_api_avg_density_digital_value];
  TYPE FLOAT
    {
      DISPLAY_FORMAT "4.2f";
    }
  HANDLING READ&WRITE;
}

VARIABLE api_avg_dens_analog_value_1
{
  CLASS DYNAMIC&ANALOG_OUTPUT;
  LABEL [mmi_api_avg_density_digital_value];
  TYPE FLOAT
    {
      DISPLAY_FORMAT "4.2f";
    }
  HANDLING READ&WRITE;
}


COLLECTION OF VARIABLE api_avg_density_sv
{
  LABEL [density];
  MEMBERS
  {
/* Measurement */
  DIGITAL_UNITS, dens_digital_units, [units];
  DIGITAL_VALUE, api_avg_dens_digital_value, [blank];
  DIGITAL_CUTOFF, dens_digital_cutoff, [cutoff];
  DAMPING_VALUE, dens_digital_damping, [damping_value];

/* Sensor Information */
  SENSOR_SERIAL_NUMBER, serial_number, [sensor_serial_number];
  SENSOR_UNITS, dens_digital_units, [sensor_unit];
  UPPER_SENSOR_LIMIT, dens_upper_sensor_limit, [usl];
  LOWER_SENSOR_LIMIT, dens_lower_sensor_limit, [lsl];
  MINIMUM_SPAN, dens_minimum_span, [minimum_span];

/* PV, SV Output Information */
  ANALOG_UNITS, analog_units, [analog_output_units];
  ANALOG_VALUE, api_avg_dens_analog_value_1, [analog_output_value];
  ANALOG_DAMPING, dens_additional_analog_damping_1, [analog_output_add_damping];
  UPPER_ENDPOINT_VALUE, dens_analog_upper_limit, [analog_output_upper_endpoint];
  LOWER_ENDPOINT_VALUE, dens_analog_lower_limit, [analog_output_lower_endpoint];
  UPPER_RANGE_VALUE, dens_upper_range_value_1, [urv];
  LOWER_RANGE_VALUE, dens_lower_range_value_1, [lrv];
  RANGE_UNITS, dens_digital_units, [range_units];
  PERCENT_RANGE, dens_percent_range_1, [percent_of_range];
  ALARM_CODE, alarm_select, [alarm_select];
  TRANSFER_FUNCTION, mmi_transfer_function, [transfer_function];
  AO1_CUTOFF, unknown_float, [blank];
  AO2_CUTOFF, unknown_float, [blank];

/* TV Output Information */
  }
}

VARIABLE api_avg_temp_value
{
  CLASS DYNAMIC&ANALOG_OUTPUT;
  LABEL [mmi_api_avg_temperature_digital_value];
  TYPE FLOAT
    {
      DISPLAY_FORMAT "4.2f";
    }
  HANDLING READ&WRITE;
}

VARIABLE api_avg_temp_value_1
{
  CLASS DYNAMIC&ANALOG_OUTPUT;
  LABEL [mmi_api_avg_temperature_digital_value];
  TYPE FLOAT
    {
      DISPLAY_FORMAT "4.2f";
    }
  HANDLING READ&WRITE;
}


COLLECTION OF VARIABLE api_avg_temperature_sv
{
  LABEL [temperature];
  MEMBERS
  {
/* Measurement */
  DIGITAL_UNITS, temp_digital_units, [units];
  DIGITAL_VALUE, api_avg_temp_digital_value, [blank];
  DIGITAL_CUTOFF, unknown_float, [cutoff];
  DAMPING_VALUE, temp_digital_damping, [damping_value];

/* Sensor Information */
  SENSOR_SERIAL_NUMBER, serial_number, [sensor_serial_number];
  SENSOR_UNITS, temp_digital_units, [sensor_unit];
  UPPER_SENSOR_LIMIT, temp_upper_sensor_limit, [usl];
  LOWER_SENSOR_LIMIT, temp_lower_sensor_limit, [lsl];
  MINIMUM_SPAN, temp_minimum_span, [minimum_span];

/* PV, SV Output Information */
  ANALOG_UNITS, analog_units, [analog_output_units];
  ANALOG_VALUE, api_avg_temp_value_1, [analog_output_value];
  ANALOG_DAMPING, temp_additional_analog_damping_1, [analog_output_add_damping];
  UPPER_ENDPOINT_VALUE, temp_analog_upper_limit, [analog_output_upper_endpoint];
  LOWER_ENDPOINT_VALUE, temp_analog_lower_limit, [analog_output_lower_endpoint];
  UPPER_RANGE_VALUE, temp_upper_range_value_1, [urv];
  LOWER_RANGE_VALUE, temp_lower_range_value_1, [lrv];
  RANGE_UNITS, temp_digital_units, [range_units];
  PERCENT_RANGE, temp_percent_range_1, [percent_of_range];
  ALARM_CODE, alarm_select, [alarm_select];
  TRANSFER_FUNCTION, mmi_transfer_function, [transfer_function];
  AO1_CUTOFF, unknown_float, [blank];
  AO2_CUTOFF, unknown_float, [blank];

/* TV Output Information */
  }
}



