/*****************************************************************************
 *
 * File:  Macros.ddl $
 *
 *****************************************************************************
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: HART Device Description Macro Definitions.  This file is
 *      targeted for older HART 5 DDs (ie, DDs that use univ5.ddl)
 *
 *****************************************************************************
 */

#define USER_INTERFACE LOCAL
#define LOCAL_INTERFACE LOCAL_DISPLAY

#define I_NO_COMMAND_SPECIFIC_ERRORS     0
#define I_INVALID_SELECTION              2
#define I_PARAMETER_TOO_LARGE            3
#define I_PARAMETER_TOO_SMALL            4
#define I_TOO_FEW_DATA_BYTES             5
#define I_TRANSMITTER_SPECIFIC_ERROR     6
#define I_IN_WRITE_PROTECT_MODE          7
#define I_ACCESS_RESTRICTED             16

/*
 * Strings for the single definition response codes.
 */

#define S_NO_COMMAND_SPECIFIC_ERRORS [no_command_specific_errors]
#define S_INVALID_SELECTION [invalid_selection]
#define S_PARAMETER_TOO_LARGE [passed_parameter_too_large]
#define S_PARAMETER_TOO_SMALL [passed_parameter_too_small]
#define S_TOO_FEW_DATA_BYTES [too_few_data_bytes_recieved]
#define S_TRANSMITTER_SPECIFIC_ERROR [xmtr_specific_command_error]
#define S_IN_WRITE_PROTECT_MODE [in_write_protect_mode]
#define S_ACCESS_RESTRICTED [access_restricted]

#define PRIMARY     1
#define SECONDARY   2
#define TERTIARY    3
#define FOURTH      4

#ifndef QUATERNARY
#define QUATERNARY FOURTH
#endif

#define PV  dynamic_variables[PRIMARY]
#define SV  dynamic_variables[SECONDARY]
#define TV  dynamic_variables[TERTIARY]
#define QV  dynamic_variables[QUATERNARY]

/*
 * Common table references.
 */

#define ROSEMOUNT_MODEL_CODE(N) { N, rosemount_model_code(N) }
#define ROSEMOUNT_ANALYTICAL_MODEL_CODE(N) { N, rosemount_analytical_model_code(N) }
#define KAYRAY_MODEL_CODE(N) { N, kayray_model_code(N) }
#define MICRO_MOTION_MODEL_CODE(N) { N, micro_motion_model_code(N) }
#define UNITS_CODE(N) { N, units_code(N) }
#define TRANSFER_FUNCTION_CODE(N) { N, transfer_function_code(N) }
#define MATERIAL_CODE(N) { N, material_code(N) }
#define ALARM_SELECTION_CODE(N) { N, alarm_selection_code(N) }
#define WRITE_PROTECT_CODE(N) { N, write_protect_code(N) }
#define COMPANY_IDENTIFICATION_CODE(N) { N, company_identification_code(N) }
#define BURST_MODE_CONTROL_CODE(N) { N, burst_mode_control_code(N) }
#define PHYSICAL_SIGNALING_CODE(N) { N, physical_signaling_codes(N) }
#define FLAG_ASSIGNMENT(N) { N, flag_assignment(N) }
#define OPERATING_MODE_CODE(N) { N, operating_mode_code(N) }
#define ANALOG_OUTPUT_NUMBERS_CODE(N) { N, analog_output_numbers_code(N) }

/*
 * Miscellaneous macros.
 */
#ifdef __GNUC__
#define LOCALVAR( x) #x     /*this is used by the get_local_var commands*/
#else
#define LOCALVAR( x) "x"
#endif


/*
 * 1) Display Message, Value, and Menu Builtins
 */
#define get_dev_var_value(a,b,c)    _get_dev_var_value((a),(b),METHODID(c))
#define GET_DEV_VAR_VALUE(a,b)      _get_dev_var_value((a),0,METHODID(b))
#define get_local_var_value(a,b,c)  _get_local_var_value((a),(b),LOCALVAR(c))
#define GET_LOCAL_VAR_VALUE(a,b)    _get_local_var_value((a),0,LOCALVAR(b))
#define display_comm_status(a)      _display_xmtr_status(METHODID(comm_status),a)
#define display_device_status(a)    _display_xmtr_status(METHODID(device_status),a)
#define display_xmtr_status(a,b)    _display_xmtr_status(METHODID(a),b)

/*
 * 2) Variable Access Builtins (Non-scaling)
 */
#define assign(a,b)                 _vassign(METHODID(a),METHODID(b))
#define vassign(a,b)                _vassign(METHODID(a),METHODID(b))

#define assign_double(a,b)          _dassign(METHODID(a),(b))
#define dassign(a,b)                _dassign(METHODID(a),(b))

#define assign_float(a,b)           _fassign(METHODID(a),(b))
#define fassign(a,b)                _fassign(METHODID(a),(b))

#define assign_int(a,b)             _iassign(METHODID(a),(b))
#define iassign(a,b)                _iassign(METHODID(a),(b))

#define assign_long(a,b)            _lassign(METHODID(a),(b))
#define lassign(a,b)                _lassign(METHODID(a),(b))

#define float_value(a)              _fvar_value(METHODID(a))
#define fvar_value(a)               _fvar_value(METHODID(a))

#define int_value(a)                _ivar_value(METHODID(a))
#define ivar_value(a)               _ivar_value(METHODID(a))

#define long_value(a)               _lvar_value(METHODID(a))
#define lvar_value(a)               _lvar_value(METHODID(a))

/*
 * 3) Communications Builtins
 */
#define get_status_code_string(a,b,c,d)     \
                    _get_status_code_string(METHODID(a),(b),(c),(d))

/*
 * 4) Variable Table Value State Change Builtins
 *      Relevant to Method Termination
 */

/*
 * 5) Abort Methods Builtins
 */
#define add_abort_method(a)         _add_abort_method(METHODID(a))
#define remove_abort_method(a)      _remove_abort_method(METHODID(a))

/*
 * 6) Name to ID Translation Builtins
 */
#define get_dictionary_string(a,b,c)\
                    _get_dictionary_string(DICT_ID(a),(b),(c))
#define VARID(a)                    METHODID(a)
#define array_reference(a,b)        resolve_array_ref(METHODID(a),(b))
#define collection_reference(a,b)   resolve_record_ref(METHODID(a),(b))

/*
 * 7) Scaling Builtins
 */

/*
 * ABORT, IGNORE, RETRY Builtins
 */

#define ABORT_ON_COMM_ERROR()           _set_comm_status(0xFF,__ABORT__)
#define IGNORE_COMM_ERROR()             _set_comm_status(0xFF,__IGNORE__)
#define RETRY_ON_COMM_ERROR()           _set_comm_status(0xFF,__RETRY__)

#define ABORT_ON_ALL_COMM_STATUS()      _set_comm_status(0x7F,__ABORT__)
#define IGNORE_ALL_COMM_STATUS()        _set_comm_status(0x7F,__IGNORE__)
#define RETRY_ON_ALL_COMM_STATUS()      _set_comm_status(0x7F,__RETRY__)

#define ABORT_ON_COMM_STATUS(comm_stat) _set_comm_status((comm_stat),__ABORT__)
#define IGNORE_COMM_STATUS(comm_stat)   _set_comm_status((comm_stat),__IGNORE__)
#define RETRY_ON_COMM_STATUS(comm_stat) _set_comm_status((comm_stat),__RETRY__)


#define ABORT_ON_ALL_DEVICE_STATUS()    _set_device_status(0xFF,__ABORT__)
#define IGNORE_ALL_DEVICE_STATUS()      _set_device_status(0xFF,__IGNORE__)
#define RETRY_ON_ALL_DEVICE_STATUS()    _set_device_status(0xFF,__RETRY__)

#define ABORT_ON_DEVICE_STATUS(dev_st)  _set_device_status((dev_st),__ABORT__)
#define IGNORE_DEVICE_STATUS(dev_st)    _set_device_status((dev_st),__IGNORE__)
#define RETRY_ON_DEVICE_STATUS(dev_st)  _set_device_status((dev_st),__RETRY__)


#define ABORT_ON_ALL_RESPONSE_CODES()   _set_all_resp_code(__ABORT__)
#define IGNORE_ALL_RESPONSE_CODES()     _set_all_resp_code(__IGNORE__)
#define RETRY_ON_ALL_RESPONSE_CODES()   _set_all_resp_code(__RETRY__)

#define ABORT_ON_RESPONSE_CODE(rsp_cod) _set_resp_code((rsp_cod),__ABORT__)
#define IGNORE_RESPONSE_CODE(rsp_cod)   _set_resp_code((rsp_cod),__IGNORE__)
#define RETRY_ON_RESPONSE_CODE(rsp_cod) _set_resp_code((rsp_cod),__RETRY__)


#define ABORT_ON_NO_DEVICE()            _set_no_device(__ABORT__)
#define IGNORE_NO_DEVICE()              _set_no_device(__IGNORE__)
#define RETRY_ON_NO_DEVICE()            _set_no_device(__RETRY__)


/*
 *  XMTR - Abort, Ignore, Retry functions
 */


#define XMTR_ABORT_ON_COMM_ERROR()      _set_xmtr_comm_status(0xFF,__ABORT__)
#define XMTR_IGNORE_COMM_ERROR()        _set_xmtr_comm_status(0xFF,__IGNORE__)
#define XMTR_RETRY_ON_COMM_ERROR()      _set_xmtr_comm_status(0xFF,__RETRY__)

#define XMTR_ABORT_ON_ALL_COMM_STATUS() _set_xmtr_comm_status(0x7F,__ABORT__)
#define XMTR_IGNORE_ALL_COMM_STATUS()   _set_xmtr_comm_status(0x7F,__IGNORE__)
#define XMTR_RETRY_ON_ALL_COMM_STATUS() _set_xmtr_comm_status(0x7F,__RETRY__)

#define XMTR_ABORT_ON_COMM_STATUS(comm_st)  _set_xmtr_comm_status((comm_st),__ABORT__)
#define XMTR_IGNORE_COMM_STATUS(comm_st)    _set_xmtr_comm_status((comm_st),__IGNORE__)
#define XMTR_RETRY_ON_COMM_STATUS(comm_st)  _set_xmtr_comm_status((comm_st),__RETRY__)


#define XMTR_ABORT_ON_ALL_DEVICE_STATUS()   _set_xmtr_device_status(0xFF,__ABORT__)
#define XMTR_IGNORE_ALL_DEVICE_STATUS()     _set_xmtr_device_status(0xFF,__IGNORE__)
#define XMTR_RETRY_ON_ALL_DEVICE_STATUS()   _set_xmtr_device_status(0xFF,__RETRY__)

#define XMTR_ABORT_ON_DEVICE_STATUS(dev_st) _set_xmtr_device_status((dev_st),__ABORT__)
#define XMTR_IGNORE_DEVICE_STATUS(dev_st)   _set_xmtr_device_status((dev_st),__IGNORE__)
#define XMTR_RETRY_ON_DEVICE_STATUS(dev_st) _set_xmtr_device_status((dev_st),__RETRY__)


#define XMTR_ABORT_ON_ALL_RESPONSE_CODES()  _set_xmtr_all_resp_code(__ABORT__)
#define XMTR_IGNORE_ALL_RESPONSE_CODES()    _set_xmtr_all_resp_code(__IGNORE__)
#define XMTR_RETRY_ON_ALL_RESPONSE_CODES()  _set_xmtr_all_resp_code(__RETRY__)

#define XMTR_ABORT_ON_RESPONSE_CODE(rsp_cd) _set_xmtr_resp_code((rsp_cd),__ABORT__)
#define XMTR_IGNORE_RESPONSE_CODE(rsp_cd)   _set_xmtr_resp_code((rsp_cd),__IGNORE__)
#define XMTR_RETRY_ON_RESPONSE_CODE(rsp_cd) _set_xmtr_resp_code((rsp_cd),__RETRY__)


#define XMTR_ABORT_ON_NO_DEVICE()       _set_xmtr_no_device(__ABORT__)
#define XMTR_IGNORE_NO_DEVICE()         _set_xmtr_no_device(__IGNORE__)
#define XMTR_RETRY_ON_NO_DEVICE()       _set_xmtr_no_device(__RETRY__)


#define XMTR_ABORT_ON_ALL_DATA()        _set_xmtr_all_data(__ABORT__)
#define XMTR_IGNORE_ALL_DATA()          _set_xmtr_all_data(__IGNORE__)
#define XMTR_RETRY_ON_ALL_DATA()        _set_xmtr_all_data(__RETRY__)

#define XMTR_ABORT_ON_DATA(byte,bit)    _set_xmtr_data((byte),(bit),__ABORT__)
#define XMTR_IGNORE_DATA(byte,bit)      _set_xmtr_data((byte),(bit),__IGNORE__)
#define XMTR_RETRY_ON_DATA(byte,bit)    _set_xmtr_data((byte),(bit),__RETRY__)


/* ADDED BY KIM */
#define assign_var(a,b)                     _vassign(METHODID(a),METHODID(b))
#define NaN                                 NaN_value()
/* Begin CPK */
#define remove_all_abort_methods()          remove_all_abort()
/* End CPK */

#ifndef PI
#define PI      3.14159265358
#endif

#define BLACK   0x000000                        
#define SILVER  0xC0C0C0                        
#define GRAY    0x808080                        
#define WHITE   0xFFFFFF                        
#define MAROON  0x800000                        
#define RED     0xFF0000                        
#define ORANGE  0xFFA500                        
#define PURPLE  0x800080                        
#define FUCHSIA 0xFF00FF                                
#define GREEN   0x008000
#define LIME    0x00FF00
#define OLIVE   0x808000
#define YELLOW  0xFFFF00
#define NAVY    0x000080
#define BLUE    0x0000FF
#define TEAL    0x008080
#define AQUA    0x00FFFF

