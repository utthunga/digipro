/*****************************************************************************
 *
 * File: test DD for installation of new items
 * Version: 4.1.0
 * Date: 10sep12
 *
 *****************************************************************************
 * Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: This file is a sample DD intended to demonstrate the 
 *              development of a simple pressure transmitter with temperature 
 *              compensation DD.  It supports HART 6, Universal Commands
 *              and a few Common Practice Commands.
 *
 *****************************************************************************
 */

/*****************************************************************************
 * The manufacturer line
 *****************************************************************************
 */
// Replace following line with your manufacturer and device type as defined in Devices.cfg
MANUFACTURER HCF, DEVICE_TYPE _SAMPLE1_WIRE, DEVICE_REVISION 1, DD_REVISION 1

/*****************************************************************************
 * Include Files
 *****************************************************************************
 */

#include "macros.h"
#include "methods.h"

/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */

/*****************************************************************************
 * Common Tables
 */

IMPORT STANDARD _TABLES, DEVICE_REVISION 20, DD_REVISION 4
{
    VARIABLE device_status;
    VARIABLE units_code;
    VARIABLE transfer_function_code;
    VARIABLE material_code;
    VARIABLE alarm_selection_code;
    VARIABLE write_protect_code;
    VARIABLE company_identification_code;
    VARIABLE burst_mode_control_code;
    VARIABLE physical_signaling_codes;
    VARIABLE flag_assignment;
    VARIABLE operating_mode_code;
    VARIABLE analog_output_numbers_code;
    VARIABLE loop_current_mode_codes;
    VARIABLE extended_device_status_codes;
    VARIABLE lock_device_codes;
    VARIABLE write_device_variable_codes;
    VARIABLE device_variable_family_codes;
    VARIABLE device_variable_classification_codes;
    VARIABLE trim_point_codes;
    VARIABLE capture_mode_codes;
    VARIABLE physical_layer_type_codes;
    VARIABLE lock_device_status_codes;
    VARIABLE analog_channel_flags;
    VARIABLE process_data_status;
    VARIABLE limit_status;
    VARIABLE device_family_status;
    VARIABLE manufacturer_id;
    VARIABLE private_label_distributor;
    VARIABLE device_type;
    VARIABLE write_protect;
    VARIABLE physical_signaling_code;
    VARIABLE operatingMode;
    VARIABLE extended_fld_device_status;
    VARIABLE loop_current_mode;
    VARIABLE device_flags;
    VARIABLE analog_channel_saturated1;
    VARIABLE analog_channel_fixed1;

    REDEFINITIONS
    {
        /*
         * The entire variable must be redefined since Device Type is defined as a two-byte 
         * Enumerated and the Tokenizer will not allow the type to be redefined from size 2 to 1.
         */
        REDEFINE VARIABLE device_type
        {
            LABEL [device_type];
            HELP [manufacturers_device_type_help];
            CLASS HART;
            HANDLING READ;
            TYPE ENUMERATED
            {
                // Replace the 0xf9 with your one-byte Device Type code and appropriate label
                { 0xf9,  "Sample 1 - HART 6" }
            }
        }

        /*
         * The entire variable must be redefined since Manufacturer ID is defined as a two-byte
         * Enumerated and the Tokenizer will not allow the type to be redefined from size 2 to 1.
         */
        REDEFINE VARIABLE manufacturer_id
        {
            LABEL [manufacturer_id];
            HELP [manufacturer_id_code_help];
            CLASS HART;
            HANDLING READ;
            TYPE ENUMERATED
            {
                // Replace the 0xf9 with your one-byte Manufacturer code and appropriate label
                { 0xf9,  [HART_Communication_Foundation] } 
            }
        }

        /*
         * The entire variable must be redefined since Private Label is defined as a two-byte
         * Enumerated and the Tokenizer will not allow the type to be redefined from size 2 to 1.
         */
        REDEFINE VARIABLE private_label_distributor
        {
            LABEL [private_label_distributor];
            HELP [private_label_distributor_help];
            CLASS DEVICE;
            HANDLING READ;
            TYPE ENUMERATED
            {
                // Replace the 0xf9 with your manufactures id and appropriate label
                { 0xf9,  [HART_Communication_Foundation] } 
            }
        }
    }
}

/*****************************************************************************
 * Universal Commmands
 */

IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 6, DD_REVISION 2
{
    // All HART devices must support all Universal Commands
    EVERYTHING;

    REDEFINITIONS
    {
        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 3) 
         */
        COMMAND read_dynamic_variables_and_pv_current
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DAQ.ANALOG_VALUE,
                    PV.DEVICE_VARIABLE.DIGITAL_UNITS, PV.DEVICE_VARIABLE.DIGITAL_VALUE,
                    SV.DEVICE_VARIABLE.DIGITAL_UNITS, SV.DEVICE_VARIABLE.DIGITAL_VALUE
                }
            }
        }

        /* 
         * The transaction will need to be redefined to show the correct
         * number of dynamic variables returned in this command 
         * (Command 8)
         */
        COMMAND read_dynamic_variable_classification
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status,
                    PV.DEVICE_VARIABLE.CLASSIFICATION,
                    SV.DEVICE_VARIABLE.CLASSIFICATION,
                    250,
                    250
                }
            }
        }
    }
}

/*****************************************************************************
 * PV, when the primary variable is not mappable we can import the PV
 * standard DD to take care of the dynamic variable array, loop current
 * and ranging functions
 */

IMPORT STANDARD _PV, DEVICE_REVISION 1, DD_REVISION 1
{
    EVERYTHING;

    REDEFINITIONS
    {
        /*
         * Remove the device variables not used in this DD.
         */
        DELETE COLLECTION tertiary_variable;
        DELETE COLLECTION quaternary_variable;

        ARRAY OF COLLECTION dynamic_variables
        {
            ELEMENTS
            {
                DELETE TERTIARY;
                DELETE QUATERNARY;
            }
        }

        /* 
         * Most devices use the same data item for range units (command 15) and the sensor units.
         * when this is true the following should be added.  if your device uses a different variable
         * for range units you should reference it here 
         */
        COLLECTION OF VARIABLE scaling
        {
            MEMBERS
            {
                ADD RANGE_UNITS, deviceVariables[0].DIGITAL_UNITS, [range_units], [range_units_help];
            } 
        }
    }
}

/*****************************************************************************
 * Common Practice Commands
 */

IMPORT STANDARD _COMMON_PRACTICE, DEVICE_REVISION 8, DD_REVISION 2
{
    COMMAND write_pv_range_values;                      // Command 35
    COMMAND set_pv_upper_range_value;                   // Command 36
    COMMAND set_pv_lower_range_value;                   // Command 37
    COMMAND reset_configuration_change_flag;            // Command 38
    COMMAND enter_exit_fixed_pv_current_mode;           // Command 40
    COMMAND perform_self_test;                          // Command 41
    COMMAND perform_device_reset;                       // Command 42
    COMMAND set_pv_zero;                                // Command 43
    COMMAND write_pv_units;                             // Command 44
    COMMAND trim_pv_current_dac_zero;                   // Command 45
    COMMAND trim_pv_current_dac_gain;                   // Command 46
    COMMAND read_additional_device_status;              // Command 48
    COMMAND read_device_variable_information;           // Command 54
    COMMAND write_number_of_response_preambles;         // Command 59
    COMMAND squawk;                                     // Command 72
    COMMAND write_device_variable;                      // Command 79
    COMMAND read_device_variable_trim_point;            // Command 80
    COMMAND read_device_variable_trim_guidelines;       // Command 81
    COMMAND write_device_variable_trim_point;           // Command 82
    COMMAND reset_device_variable_trim;                 // Command 83

    METHOD applied_rerange;
    METHOD zero_trim;
    METHOD return_to_normal;
    METHOD warning_message;
    METHOD device_self_test;
    METHOD device_master_reset;
    METHOD transmitter_loop_test;
    METHOD leave_fixed_current_mode;
    METHOD transmitter_dac_trim;
    METHOD makeItSquawk;
    METHOD device_variable_trim;
    METHOD trim_warning;
    METHOD device_variable_trim_reset;

    VARIABLE device_specific_status_0;
    VARIABLE device_specific_status_1;
    VARIABLE device_specific_status_2;
    VARIABLE device_specific_status_3;
    VARIABLE device_specific_status_4;
    VARIABLE device_specific_status_5;
    VARIABLE device_variable_code;
    VARIABLE device_variable_trim_code;

    REDEFINITIONS
    {
        COMMAND read_additional_device_status
        {
            REDEFINE TRANSACTION
            {
                REQUEST {}
                REPLY
                {
                    response_code, device_status, 
                    device_specific_status_0, device_specific_status_1, device_specific_status_2,
                    device_specific_status_3, device_specific_status_4, device_specific_status_5,

                    extended_fld_device_status, 0x00, 

                    0x00, 0x00,  analog_channel_saturated1,
                    0x00, 0x00,  analog_channel_fixed1
                }
            }
        }

        VARIABLE device_variable_trim_code
        {
        /* REDEFINE CLASS LOCAL; */
            REDEFINE TYPE INDEX trimDeviceVariables; 
        }
    }
}

/*
 * Dynamic Variables Configuration
 */
VARIABLE pressureValue
{
    LABEL [pressure_value];
    HELP [digital_value_pressure_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureUnits
{
    LABEL [pressure_value_unit];
    HELP [digital_units_pressure_help];
    HANDLING READ & WRITE;
    TYPE ENUMERATED
    {
        { 0x01,   [InH2O],                [inches_of_water_68_degrees_F_help] },
        { 0x02,   [InHg],                 [inches_of_mercury_0_degrees_C_help] },
        { 0x03,   [FtH2O],                [feet_of_water_68_degrees_F_help] },
        { 0x04,   [mmH2O],                [millimeters_of_water_68_degrees_F_help] },
        { 0x05,   [mmHg],                 [millimeters_of_mercury_0_degrees_C_help] },
        { 0x06,   [psi],                  [pounds_per_square_inch_help] },
        { 0x07,   [bar],                  [bars_help] },
        { 0x08,   [mbar],                 [millibars_help] },
        { 0x09,   [g_SqCm],               [grams_per_square_centimeter_help] },
        { 0x0A,   [kg_SqCm],              [kilograms_per_square_centimeter_help] },
        { 0x0B,   [PA],                   [pascals_help] },
        { 0x0C,   [kPA],                  [kilopascals_help] },
        { 0x0D,   [torr],                 [torr_help] },
        { 0x0E,   [ATM],                  [atmospheres_help] },
        { 0x91,   [in_H2O_60_degrees_F],  [inches_of_water_60_degrees_F_help] },
        { 0xAA,   [cm_H20_4_degrees_C],   [centimeters_of_water_4_degrees_C_help] },
        { 0xAB,   [m_H20_4_degrees_C],    [meters_of_water_4_degrees_C_help] },
        { 0xAC,   [cm_Hg_0_degrees_C],    [centimeters_of_mercury_0_degrees_C_help] },
        { 0xAD,   [lb_SqFt],              [pounds_square_foot_help] },
        { 0xAE,   [hPa],                  [hPa_help] },
        { 0xAF,   [lb_SqIn_abs],          [pounds_square_inch_absolute_help] },
        { 0xB0,   [kg_Sqm],               [kilograms_per_square_meter_help] },
        { 0xB1,   [ft_H20_4_degrees_C],   [feet_of_water_4_degrees_C_help] },
        { 0xB2,   [ft_H2O_60_degrees_F],  [feet_of_water_60_degrees_F_help] },
        { 0xB3,   [m_Hg_0_degrees_C],     [meters_of_mercury_0_degrees_C_help] },
        { 0xED,   [mega_pascals],         [megapascals_help] },
        { 0xEE,   [in_H2O_4_degrees_C],   [inches_of_water_4_degrees_C_help] },
        { 0xEF,   [mm_H2O_4_degrees_C],   [millimeters_of_water_4_degrees_C_help] }
    }
}

VARIABLE pressureFamily
{
    LABEL "Pres Dev fmly";
    HELP [device_family_help];
    CLASS DEVICE;
    HANDLING READ;
    TYPE ENUMERATED
    {
        /*
         * We have declared that the pressure device variable supports the Pressure Family,
         * however, this DD is not complete because there are no Pressure Family commands in it
         */
        DEVICE_VARIABLE_FAMILY_CODE(5)      // This is the macro from above macros.h
    }
}

VARIABLE pressureClassification
{
    LABEL "Pres Class";
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        // This is the macro from above macros.h
        DEVICE_VARIABLE_CLASSIFICATION_CODE(65)
    }
}

VARIABLE pressurePDQ
{
    LABEL "Pres PDQ";
    HELP [process_data_quality_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 , process_data_status(0)},
        {1 , process_data_status(1)},
        {2 , process_data_status(2)},
        {3 , process_data_status(3)}

        // PROCESS_DATA_STATUS_CODES      // if preferred, use this macro */         
    }
}

VARIABLE pressureLS
{
    LABEL "Pres LS";
    HELP [limit_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        // This is the macro from above macros.h
        LIMIT_STATUS_CODES
    }
}

VARIABLE pressureStatus
{
    LABEL "Pres Fmly stat";
    HELP [family_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE BIT_ENUMERATED
    {
        { 0x08, device_family_status(0x08), MORE }
    }
}

VARIABLE pressureUSL
{
    LABEL [pressure_upper_sensor_limit];
    HELP [upper_sensor_limit_pressure_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureLSL
{
    LABEL [pressure_lower_sensor_limit];
    HELP [lower_sensor_limit_pressure_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureMinimumSpan
{
    LABEL [pressure_minimum_span];
    HELP [minimum_span_pressure_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureDampingValue
{
    LABEL [pressure_damping_value];
    HELP  [seconds_damping_value_pressure_help];
    // You will probably add a write command for this later
    // Usualy command 34 for Write Primary Variable Damping Value
    HANDLING READ;
    CONSTANT_UNIT [sec];
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
        EDIT_FORMAT "8.3f";
    }
}

VARIABLE pressureSerialNumber
{
    LABEL [pressure_sensor_serial_number];
    HELP [sensor_serial_number_help];
    CLASS DEVICE;
    HANDLING READ;
    TYPE UNSIGNED_INTEGER (3)
    {
        DISPLAY_FORMAT  "7d";
    }
}

VARIABLE pressureLowerTrimPoint
{
    LABEL "Pres Lower trim";
    HELP [lower_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureUpperTrimPoint
{
    LABEL "Pres Upper trim";
    HELP [upper_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureTrimSupport
{
    LABEL "Pres Trim support";
    HELP [trim_support_help];
    HANDLING READ;
    TYPE ENUMERATED (1)
    {
        TRIM_POINT_CODE(1),   // Lower Trim Point Supported  
        TRIM_POINT_CODE(2),   // Upper Trim Point Supported
        TRIM_POINT_CODE(3)    // Lower and Upper Trim Point Supported
    }
}

VARIABLE pressureMinimumLTP
{
    LABEL "Pres Min lower trim";
    HELP [minimum_lower_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureMaximumLTP
{
    LABEL "Pres Max lower trim";
    HELP [maximum_lower_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureMinimumUTP
{
    LABEL "Pres Min upper trim";
    HELP [minimum_upper_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureMaximumUTP
{
    LABEL "Pres Max upper trim";
    HELP [maximum_upper_trim_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureMinimumTrimDiff
{
    LABEL "Pres Min trim diff";
    HELP [minimum_trim_diff_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE pressureSimulated
{
    LABEL "Simulate";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

/* 
 * This is the minimum set of members that any device variable collection must have 
 */
COLLECTION OF VARIABLE press
{
    LABEL [pressure];
    HELP [pressure_help];
    MEMBERS
    {   
        // Sensor Correction Information
        DIGITAL_VALUE,              pressureValue,              [blank],                [digital_value_help];
        DIGITAL_UNITS,              pressureUnits,              [units],                [digital_units_help];
        DATA_QUALITY,               pressurePDQ,                [process_data_quality], [process_data_quality_help];
        LIMIT_STATUS,               pressureLS,                 [limit_status],         [limit_status_help];
        DEVICE_FAMILY_STATUS,       pressureStatus,             [family_status],        [family_status_help];

        CLASSIFICATION,             pressureClassification,     [classification],       [classification_help];
        DEVICE_FAMILY,              pressureFamily,             [device_family],        [device_family_help];

        UPPER_SENSOR_LIMIT,         pressureUSL,                [usl],                  [upper_sensor_limit_help];
        LOWER_SENSOR_LIMIT,         pressureLSL,                [lsl],                  [lower_sensor_limit_help];
        MINIMUM_SPAN,               pressureMinimumSpan,        [minimum_span],         [minimum_span_help];
        DAMPING_VALUE,              pressureDampingValue,       [damping_value],        [seconds_damping_value_help];
        SIMULATED,                  pressureSimulated,          "Simulate",             "";

        // Device Information
        SENSOR_SERIAL_NUMBER,       pressureSerialNumber,       [sensor_serial_number], [sensor_serial_number_help];

        // Calibration Information
        TRIM_POINT_UNITS,           pressureUnits,              [trim_unit],            [sensor_trim_unit_help];
        LOWER_TRIM_POINT,           pressureLowerTrimPoint,     [lower_trim],           [lower_trim_help];
        UPPER_TRIM_POINT,           pressureUpperTrimPoint,     [upper_trim],           [upper_trim_help];
        TRIM_POINT_SUPPORT,         pressureTrimSupport,        [trim_support],         [trim_support_help];
        MINIMUM_LOWER_TRIM_POINT,   pressureMinimumLTP,         [minimum_lower_trim],   [minimum_lower_trim_help];
        MAXIMUM_LOWER_TRIM_POINT,   pressureMaximumLTP,         [maximum_lower_trim],   [maximum_lower_trim_help];
        MINIMUM_UPPER_TRIM_POINT,   pressureMinimumUTP,         [minimum_upper_trim],   [minimum_upper_trim_help];
        MAXIMUM_UPPER_TRIM_POINT,   pressureMaximumUTP,         [maximum_upper_trim],   [maximum_upper_trim_help];
        MINIMUM_TRIM_DIFFERENTIAL,  pressureMinimumTrimDiff,    [minimum_trim_diff],    [minimum_trim_diff_help];
    }
}

VARIABLE temperatureValue
{
    LABEL [sensor_temperature];
    HELP [sensor_temperature_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE temperatureUnits
{
    LABEL "Snsr temp unit"
          "|de|Einh Snsr Temp"
          "|fr|Unité Température de sonde."
          "|kt|sennsa- onndo tanni";

    HELP "Sensor Temperature Units- Engineering unit to be displayed with Sensor Temperature."
         "|de|Einheit Sensortemperatur- Temperatureinheit mit der die Sensortemperatur angezeigt wird."
         "|fr|Unité de temperature de sonde - Unité physique pour exprimer la température du module."
         "|kt|sennsa- onndo tanni - sennsa- onndono hyouzini siyousareru tannidesu";
    CLASS CORRECTION;
    HANDLING READ & WRITE;
    TYPE ENUMERATED
    {
        { 0x20,  [degC],                  [degrees_celsius_help] },
        { 0x21,  [degF],                  [degrees_fahrenheit_help] },
        { 0x22,  [degR],                  [degrees_rankine_help] },
        { 0x23,  [Kelvin],                [degrees_kelvin_help] }
    }
}

VARIABLE temperatureFamily
{
    LABEL [device_family];
    CLASS DEVICE;
    HANDLING READ;
    TYPE ENUMERATED
    {
        /*
         * We have declared that the temperature device variable supports the Temperature Family,
         * however, this DD is not complete because there are no Temperature Family commands in it
         */
        DEVICE_VARIABLE_FAMILY_CODE(4)      // This is the macro from above macros.h
    }
}

VARIABLE temperatureClassification
{
    LABEL "Temp Class";
    HANDLING READ;
    TYPE ENUMERATED
    {
        // This is the macro from above macros.h
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0)  // classification is not used here
    }
}

VARIABLE temperaturePDQ
{
    LABEL "Temp PDQ";
    HELP "";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        // This is the macro from above macros.h
        PROCESS_DATA_STATUS_CODES
    }
}

VARIABLE temperatureLS
{
    LABEL "Temp LS";
    HELP "";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        // This is the macro from above macros.h
        LIMIT_STATUS_CODES
    }
}

VARIABLE temperatureStatus
{
    LABEL "Temp Dev Stat";
    HELP "";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE BIT_ENUMERATED
    {
        { 0x08, device_family_status(0x08), MORE }
    }
}

VARIABLE temperatureUSL
{
    LABEL "Temp USL";
    HELP [upper_sensor_limit_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE temperatureLSL
{
    LABEL "Temp LSL";
    HELP [lower_sensor_limit_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE temperatureMinimumSpan
{
    LABEL "Temp min span";
    HELP [minimum_span_help];
    HANDLING READ;
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
    }
}

VARIABLE temperatureDampingValue
{
    LABEL [temperature_damping_value];
    HELP  [seconds_damping_value_help];
    // You will probably add a write command for this later
    // Usualy command 34 for Write Primary Variable Damping Value
    HANDLING READ;
    CONSTANT_UNIT [sec];
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
        EDIT_FORMAT "8.3f";
    }
}

VARIABLE temperatureSerialNumber
{
    LABEL "Temp snsr s/n";
    HELP [sensor_serial_number_help];
    CLASS DEVICE;
    HANDLING READ;
    TYPE UNSIGNED_INTEGER (3)
    {
        DISPLAY_FORMAT  "7d";
    }
}

VARIABLE temperatureSimulated
{
    LABEL "Simulate";
    HELP "";
    CLASS DEVICE & SERVICE;
    HANDLING WRITE;
    TYPE ENUMERATED
    {
        WRITE_DEVICE_VARIABLE_CODE(0),  /* Normal */
        WRITE_DEVICE_VARIABLE_CODE(1)   /* Fix Value */
    }
}

COLLECTION OF VARIABLE temperature
{
    LABEL [temperature];
    HELP "Secondary Variable - Temperature Compensation";
    MEMBERS
    {
        // Sensor Correction Information
        DIGITAL_VALUE,              temperatureValue,           [blank],                [digital_value_help];
        DIGITAL_UNITS,              temperatureUnits,           [units],                [digital_units_help];
        DATA_QUALITY,               temperaturePDQ,             [process_data_quality], [process_data_quality_help];  
        LIMIT_STATUS,               temperatureLS,              [limit_status],         [limit_status_help];  
        DEVICE_FAMILY_STATUS,       temperatureStatus,          [family_status],        [family_status_help]; 

        CLASSIFICATION,             temperatureClassification,  [classification],       [classification_help];
        DEVICE_FAMILY,              temperatureFamily,          [device_family],        [device_family_help]; 

        UPPER_SENSOR_LIMIT,         temperatureUSL,             [usl],                  [upper_sensor_limit_help];
        LOWER_SENSOR_LIMIT,         temperatureLSL,             [lsl],                  [lower_sensor_limit_help];
        MINIMUM_SPAN,               temperatureMinimumSpan,     [minimum_span],         [minimum_span_help];
        DAMPING_VALUE,              temperatureDampingValue,    [damping_value],        [seconds_damping_value_help];
        SIMULATED,                  temperatureSimulated,       "Simulate",             "";

	// Device Information
        SENSOR_SERIAL_NUMBER,       temperatureSerialNumber,    [sensor_serial_number], [sensor_serial_number_help];
     }
}

ARRAY OF COLLECTION deviceVariables 
{
    ELEMENTS
    {
        0, press,       "Pressure";
        1, temperature, "Temperature";
    }
}

ARRAY OF COLLECTION trimDeviceVariables 
{
    ELEMENTS
    {
        0, press, [pv];
    }
}

/*
 * Relationships
 */

UNIT pressureUnitsRelation
{
    pressureUnits:
        pressureValue,
        pressureUSL,
        pressureLSL,
        pressureMinimumSpan,
        pressureLowerTrimPoint,
        pressureUpperTrimPoint,
        pressureMinimumLTP,
        pressureMaximumLTP,
        pressureMinimumUTP,
        pressureMaximumUTP,
        pressureMinimumTrimDiff
}

UNIT temperatureUnitRelation
{
    temperatureUnits :
        temperatureValue
        temperatureUSL,
        temperatureLSL,
        temperatureMinimumSpan
}

/***************************************************************************
 * below is list of menu declaritions
 * they where take directly from menu.ddl an interoperable file
 * this is a recommended display format derived by the HART communicator
 * developers in conjunction with slave developers and end users.
 */

MENU root_menu
{
    LABEL [bus_devices];
    ITEMS
    {
        device_setup,                                               // menu
        PV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE),
        SV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE),

        PV.DAQ.ANALOG_VALUE (DISPLAY_VALUE, READ_ONLY),
        PV.RANGING.LOWER_RANGE_VALUE (DISPLAY_VALUE, READ_ONLY),
        PV.RANGING.UPPER_RANGE_VALUE (DISPLAY_VALUE, READ_ONLY)
    }
}

MENU device_setup
{
    LABEL [device_setup];
    ITEMS
    {
        process_variables,                                          // menu
        diag_service,                                               // menu
        basic_setup,                                                // menu
        detailed_setup,                                             // menu
        review (REVIEW)                                             // menu
    }
}

MENU process_variables
{
    LABEL [process_variables];
    ITEMS
    {
        PV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE),
        PV.RANGING.PERCENT_RANGE (DISPLAY_VALUE),
        PV.DAQ.ANALOG_VALUE (DISPLAY_VALUE),
        SV.DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE)
    }
}

MENU diag_service
{
    LABEL [diagnostics_and_service];
    ITEMS
    {
        test_device,                                                // menu
        calibration                                                 // menu
    }
}

MENU test_device
{
    LABEL [test_device];
    ITEMS
    {
        status_display,                                             // menu
        device_self_test,                                           // method
        device_master_reset,                                        // method
        makeItSquawk                                                // method
    }
}

MENU calibration
{
    LABEL [calibration];
    ITEMS
    {
        menu_rerange,                                               // menu
        analog_output,                                              // menu
        menu_sensor_trim                                            // menu
    }
}

MENU menu_rerange
{
    LABEL "Re-range";
    ITEMS
    {
        rerange,                                                    // edit display
        applied_rerange                                             // method
    }
}

MENU menu_sensor_trim
{
    LABEL [sensor_trim];
    ITEMS
    {
        zero_trim,                                                  // method
        device_variable_trim,                                       // method
        device_variable_trim_reset,                                 // method
        menu_sensor_trim_points (REVIEW)                            // menu
    }
}

MENU menu_sensor_trim_points
{
    LABEL "Sensor trim points"
          "|fr|Points d'etalonnage du capteur"
          "|de|Abgl.punkte Sensor"
          "|kt|sennsa-torimu poinnto";
    ITEMS
    {
        deviceVariables[0].LOWER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].UPPER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].TRIM_POINT_SUPPORT (DISPLAY_VALUE),
        deviceVariables[0].TRIM_POINT_UNITS (DISPLAY_VALUE),
        deviceVariables[0].MINIMUM_LOWER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].MAXIMUM_LOWER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].MINIMUM_UPPER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].MAXIMUM_UPPER_TRIM_POINT (DISPLAY_VALUE),
        deviceVariables[0].MINIMUM_TRIM_DIFFERENTIAL (DISPLAY_VALUE)
    }
}

MENU status_display
{
    LABEL [status];
    ITEMS
    {
        device_specific_status_0 (DISPLAY_VALUE),
        device_specific_status_1 (DISPLAY_VALUE),
        device_specific_status_2 (DISPLAY_VALUE),
        device_specific_status_3 (DISPLAY_VALUE),
        device_specific_status_4 (DISPLAY_VALUE),
        device_specific_status_5 (DISPLAY_VALUE),
        extended_fld_device_status (DISPLAY_VALUE),
        operatingMode (DISPLAY_VALUE)
    }
}

MENU basic_setup
{
    LABEL [basic_setup];
    ITEMS
    {
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        PV.DEVICE_VARIABLE.DIGITAL_UNITS,
        PV.RANGING.TRANSFER_FUNCTION,
        PV.DEVICE_VARIABLE.DAMPING_VALUE,
        rerange,                                                    // edit display
        device_info                                                 // menu
    }
}

MENU detailed_setup
{
    LABEL [detailed_setup];
    ITEMS
    {
        measuring_elements,                                         // menu
        signal_conditioning,                                        // menu
        output_conditioning,                                        // menu
        device_info                                                 // menu
    }
}

MENU device_info
{
    LABEL [device_information];
    ITEMS
    {
        private_label_distributor (DISPLAY_VALUE),
        device_type (DISPLAY_VALUE),
        device_id (DISPLAY_VALUE),
        config_change_counter (DISPLAY_VALUE),
        tag (DISPLAY_VALUE),
        longTag (DISPLAY_VALUE),
        date (DISPLAY_VALUE),
        write_protect (DISPLAY_VALUE),
        descriptor (DISPLAY_VALUE),
        message (DISPLAY_VALUE),
        final_assembly_number (DISPLAY_VALUE),
        device_revisions                                            // menu
    }
}

MENU device_revisions
{
    LABEL [revision_numbers];
    ITEMS
    {
        universal_revision (DISPLAY_VALUE),
        transmitter_revision (DISPLAY_VALUE),
        software_revision (DISPLAY_VALUE),
        hardware_revision (DISPLAY_VALUE)
    }
}

MENU measuring_elements
{
    LABEL [sensors];
    ITEMS
    {
        menu_pres_sensor,                                           // menu
        menu_temp_sensor,                                           // menu
        max_num_device_variables (DISPLAY_VALUE)
    }
}

MENU menu_pres_sensor
{
    LABEL [pressure_sensor];
    ITEMS
    {
        deviceVariables[0].DIGITAL_VALUE (DISPLAY_VALUE),
        deviceVariables[0].DIGITAL_UNITS (DISPLAY_VALUE),
        menu_sensor_trim,                                           // menu
        menu_sensor_info (REVIEW)                                   // menu
    }
}

MENU menu_temp_sensor
{
    LABEL "Temperature sensor";
    ITEMS
    {
        deviceVariables[1].DIGITAL_VALUE (DISPLAY_VALUE),
        deviceVariables[1].DIGITAL_UNITS (DISPLAY_VALUE),
        menu_sensor2_info (REVIEW)                                  // menu
    }
}

MENU menu_sensor_info
{
    LABEL [sensor_info];
    ITEMS
    {
        deviceVariables[0].DEVICE_FAMILY (DISPLAY_VALUE),
        deviceVariables[0].CLASSIFICATION (DISPLAY_VALUE),
        deviceVariables[0].DATA_QUALITY (DISPLAY_VALUE),
        deviceVariables[0].LIMIT_STATUS (DISPLAY_VALUE),
        deviceVariables[0].DEVICE_FAMILY_STATUS (DISPLAY_VALUE),
        deviceVariables[0].LOWER_SENSOR_LIMIT (DISPLAY_VALUE),
        deviceVariables[0].UPPER_SENSOR_LIMIT (DISPLAY_VALUE),
        deviceVariables[0].MINIMUM_SPAN (DISPLAY_VALUE),
        deviceVariables[0].DAMPING_VALUE (DISPLAY_VALUE),
        deviceVariables[0].SENSOR_SERIAL_NUMBER (DISPLAY_VALUE)
    }
}

MENU menu_sensor2_info
{
    LABEL [sensor_info];
    ITEMS
    {
        deviceVariables[1].DEVICE_FAMILY (DISPLAY_VALUE),
        deviceVariables[1].CLASSIFICATION (DISPLAY_VALUE),
        deviceVariables[1].DATA_QUALITY (DISPLAY_VALUE),
        deviceVariables[1].LIMIT_STATUS (DISPLAY_VALUE),
        deviceVariables[1].DEVICE_FAMILY_STATUS (DISPLAY_VALUE),
        deviceVariables[1].LOWER_SENSOR_LIMIT (DISPLAY_VALUE),
        deviceVariables[1].UPPER_SENSOR_LIMIT (DISPLAY_VALUE),
        deviceVariables[1].MINIMUM_SPAN (DISPLAY_VALUE),
        deviceVariables[1].DAMPING_VALUE (DISPLAY_VALUE),
        deviceVariables[1].SENSOR_SERIAL_NUMBER (DISPLAY_VALUE)
    }
}

MENU signal_conditioning
{
    LABEL [signal_condition];
    ITEMS
    {
        PV.RANGING.LOWER_RANGE_VALUE (DISPLAY_VALUE),
        PV.RANGING.UPPER_RANGE_VALUE (DISPLAY_VALUE),
        PV.RANGING.PERCENT_RANGE (DISPLAY_VALUE),
        PV.RANGING.TRANSFER_FUNCTION (DISPLAY_VALUE),
        PV.DEVICE_VARIABLE.DAMPING_VALUE (DISPLAY_VALUE)
    }
}

MENU output_conditioning
{
    LABEL [output_condition];
    ITEMS
    {
        analog_output,                                              // Menu
        hart_output                                                 // Menu
    }
}

MENU analog_output
{
    LABEL [analog_output];
    ITEMS
    {
        PV.DAQ.ANALOG_VALUE (DISPLAY_VALUE,READ_ONLY),
        PV.DAQ.ALARM_CODE (DISPLAY_VALUE),
        PV.DAQ.ANALOG_CHANNEL_FLAGS (DISPLAY_VALUE),
        loop_current_mode  (DISPLAY_VALUE),
        transmitter_loop_test,                                      // method
        transmitter_dac_trim                                        // method
    }
}

MENU hart_output
{
    LABEL [HART_output];
    ITEMS
    {
        polling_address (DISPLAY_VALUE),
        request_preambles (DISPLAY_VALUE),
        response_preambles (DISPLAY_VALUE)
    }
}

MENU review
{
    LABEL [review];
    ITEMS
    {
        device_type (DISPLAY_VALUE, READ_ONLY),
        private_label_distributor (DISPLAY_VALUE, READ_ONLY),
        write_protect (DISPLAY_VALUE, READ_ONLY),
        device_id (DISPLAY_VALUE, READ_ONLY),
        config_change_counter (DISPLAY_VALUE, READ_ONLY),
        max_num_device_variables  (DISPLAY_VALUE, READ_ONLY),
        tag (DISPLAY_VALUE, READ_ONLY),
        longTag (DISPLAY_VALUE, READ_ONLY),
        descriptor (DISPLAY_VALUE, READ_ONLY),
        message (DISPLAY_VALUE, READ_ONLY),
        date (DISPLAY_VALUE, READ_ONLY),
        final_assembly_number (DISPLAY_VALUE),
        universal_revision (DISPLAY_VALUE, READ_ONLY),
        transmitter_revision (DISPLAY_VALUE, READ_ONLY),
        software_revision (DISPLAY_VALUE, READ_ONLY),
        polling_address (DISPLAY_VALUE, READ_ONLY),
        loop_current_mode (DISPLAY_VALUE, READ_ONLY),
        request_preambles (DISPLAY_VALUE, READ_ONLY),
        response_preambles (DISPLAY_VALUE, READ_ONLY)
    }
}

EDIT_DISPLAY rerange
{
    LABEL [range_values];
    EDIT_ITEMS
    { 
        scaling_wao, 
        PV.DEVICE_VARIABLE.DIGITAL_UNITS
    }
    DISPLAY_ITEMS
    {
        PV.DEVICE_VARIABLE.LOWER_SENSOR_LIMIT,
        PV.DEVICE_VARIABLE.UPPER_SENSOR_LIMIT
    }
}
