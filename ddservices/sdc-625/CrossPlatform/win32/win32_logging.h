/**********************************************************************************************
 *
 * $Workfile: logging.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *********************************************************************************************
 * This file was generated for the logging.cpp file
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *		logging.h : external interface to the error logging functions
 *
 * #include "logging.h"
 *
 */


#ifndef _XPLATFORM_WIN32_LOGGING_H
#define _XPLATFORM_WIN32_LOGGING_H


//#include "args.h"
//#include "errors.h" /* return codes , here for convienience */

#pragma warning (disable : 4786) 

/***** ENTRY PREP *****/
int  logout(int channel, const wchar_t* format, ...);
int  logout(int channel,    const char* format, ...);

/*** NOTE: error number calls only have extra parameters if they are required 
 **        in the errorDesc.h file's specific error formatting.
 ***/
int  logout(int channel, int errNumber, ...);

#define LOGIT	::logout
/* Usage:
 * LOGIT(CERR_LOG | CLOG_LOG, "ERROR: Failed to find %d in %s.\n", 42, "The answer");
 */
/* Args requires
 *	'O' , "outputFileName"
 *	'E' , "errorFileName"
 *  'G' , "logFileName"
 *
 * if any are the empty string then that log path will go into the bit bucket.
 */


#define LOGIF( a )	if ( (logPer & a ) != 0 )	::logout
/* Usage:
 * LOGIF(LOGP_DEPENDENCY)(CERR_LOG | CLOG_LOG, 
 *								"ERROR: Failed to find %d in %s.\n", 42, "The answer");
 */

#if defined(_MSC_VER)

#if _MSC_VER < 1400	/* these are defined in VS 2005 */
/* these two are located in DDParser/PrintData.cpp and resolve the error:
   error C2593: 'operator <<' is ambiguous                             */
extern std::ostream& operator<<(std::ostream& os, __int64 i );
extern std::ostream& operator<<(std::ostream& os, UINT64 i );
#endif

#endif // _MSC_VER

extern volatile unsigned int logPer;


// if LOGTHIS(LOGP_DEPENDENCY) {}
#define LOGTHIS( a ) ( (logPer & a ) != 0 ) /* means ok2logit */
#ifdef _DEBUG
#define DEBUGLOG	::logout
#define DBGLOGIF( V )  if( V ) ::logout
//#define LOGTHIS( a ) ( (logPer & a ) != 0 ) /* means ok2logit */
#else


#define DBGLOGIF( V )  ((void)0)
/* 13aug10 note that warning C4353 says to use __noop. __noop is a microsoft specific thingy
                it is NOT ansi standard so it won't be included in the cvs code of sdc 
				That warning has been disabled in the current projects                     */

#define DEBUGLOG	((void)0)


//#define LOGTHIS( a ) (false) /* should optimize out the entire statement*/
#endif
/**** end permission ***/

#ifdef IS_SDC
 #define STATUS_BAR_DEFAULT      L"SDC-625"
 #define SDCLOG      ::logout  
#else
 #define SDCLOG      ((void)0)
 #ifdef IS_LIBR
  #define STATUS_BAR_DEFAULT	  "Device Librarian"
 #else /* xmtr???*/
  #define STATUS_BAR_DEFAULT	  L"Xmtr-DD"
 #endif

#endif

class DDSargs;


/***** ENTRY PREP *****/
#ifdef _UNICODE
int  logSetup(DDSargs  &rArgs, wchar_t * cmdLine = NULL);
#else
int  logSetup(DDSargs  &rArgs, char * cmdLine = NULL);
#endif
#if defined TOK || defined FMA_PARSER_LOG
int  logSetup(void);
#endif



/***** INFO *****/
bool isClogging(void);
bool isCerring(void);
bool isOuting(void);

/***** USER INTERFACE FUNCTIONS *****/
// no longer valid - use LOGIT  void LogMessage(char * strMessage,...);
bool GetLogMsgFromLog( int nLine, char *pLogMsg, size_t bufLen );
void GetNoOfLogMsgs( int *pnTotalLogMsgCnt );
void ClearLog(); //Added by ANOOP 03MAR2004

/* removed 22aug08 ... 
void LogMessageV(char *strMessage,va_list varArgList); 
													/x/ specifically for ddbDevice::systemLog
  ddbDevice was modified... */

/***** STATUS BAR MAINTAINENCE ******/
void GetStatusString(wchar_t statusRetStr[]);

#ifdef _WIN32_WCE
void ShowTime(bool bEnabled,char *pLogMsg);

int ChkSupportedDevice(LPCTSTR filename,int manuf,int dev,int rev1,BYTE* rev2);
int Tokenise(char* str,const char* delim,int& manuf,int& dev,int& rev1,int& rev2);
#endif


#endif // _XPLATFORM_WIN32_LOGGING_H
