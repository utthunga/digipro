#include "stdafx.h" // to allow precompiled headers
#include <basetsd.h>
#include <windef.h>
#include <string>


using namespace std;

#include "xplatform_thread.h"
#include "shared/shared_thread.h"

#include "logging.h"



static DWORD WINAPI __WaitThreadFunction(LPVOID lpThreadParameter)
{
	oneWait* pWait = (oneWait*)lpThreadParameter;
	return (*(pWait->WaitFunction))(pWait);
}

THREAD_HANDLE_TYPE Thread_CreateThread(
	oneWait* pWrkWait,
	ULONG dwFlags,
	THREAD_ID_TYPE* pThreadId
)
{
	if (dwFlags & WT_INIT_COM_MULTI & WT_INIT_COM_SINGLE)
	{
		if (dwFlags & WT_INIT_COM_MULTI)
		{
			pWrkWait->enableCOM = (BOOL)1;
		}
		else
		{
			pWrkWait->enableCOM = (BOOL)2;
		}
	}
	else
	{
		pWrkWait->enableCOM = FALSE;
	}

	THREAD_HANDLE_TYPE threadHandle = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) __WaitThreadFunction, // the func
		(LPVOID) (pWrkWait),			 // pass the comm
		0,						// starts not suspended
		pThreadId
	);			// returns threadId

	if(NULL == threadHandle)	
	{
		DWORD dwErr = GetLastError();
		LOGIT(CERR_LOG,"Failed to Create Thread (err = %d)\n",dwErr);
	}

	return threadHandle;
}

void Thread_ExitThread(DWORD exitCode)
{
	ExitThread(exitCode);
}

bool Thread_TerminateThread(
	THREAD_HANDLE_TYPE threadHandle,
	DWORD exitCode
)
{
	return (0 != TerminateThread(threadHandle, exitCode));
}

void Thread_CloseHandle(THREAD_HANDLE_TYPE threadHandle)
{
    CloseHandle(threadHandle);
}

void Thread_HandleWaitSetUp(oneWait* pWait)
{
	if (pWait->enableCOM)
	{
		HRESULT hR = 0;
#if !(defined(XMTRDD)) && !(defined(TOK)) && !(defined(ANALYS))
		if ( (int)(pWait->enableCOM) == 2)
			hR = CoInitializeEx(NULL, COINIT_MULTITHREADED);
		else
			hR = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#endif
		if (  FAILED( hR ) )
		{
			LOGIT(CLOG_LOG|CERR_LOG,"ERROR: HandleWait could not initialize COM\n");
    		pWait->enableCOM = FALSE;
    	}
	}
}

void Thread_HandleWaitCleanUp(oneWait* pWait)
{
#if !(defined(XMTRDD)) && !(defined(TOK))
	if (pWait->enableCOM)
	{
		CoUninitialize();
	}
#endif
}


THREAD_ID_TYPE Thread_GetCurrentThreadId()
{
	return GetCurrentThreadId();
}


bool Thread_IsThreadSame(
	THREAD_ID_TYPE threadOneId,
	THREAD_ID_TYPE threadTwoId
)
{
	return (threadOneId == threadTwoId);
}

THREAD_HANDLE_TYPE Thread_GetCurrentThread()
{
	return GetCurrentThread();
}

int Thread_GetThreadPriority(THREAD_HANDLE_TYPE threadHandle)
{
	return GetThreadPriority(threadHandle);
}

void Thread_SetThreadPriority(
	THREAD_HANDLE_TYPE threadHandle, 
	int priorityValue
)
{
	(void) SetThreadPriority(threadHandle, priorityValue);
}


void Thread_SetThreadPriorityFromMask(
	THREAD_HANDLE_TYPE threadHandle, 
	ULONG dwFlagsPriorityMask
)
{
	int PRI = 0;
	switch ((dwFlagsPriorityMask & WT_NEW_PRIORITY))
	{
#ifndef _WIN32_WCE// use CE thread priorities with CeSetThreadPriority!  PAW 24/07/09
	case WT_NORM_PLUS_ONE: PRI = THREAD_PRIORITY_ABOVE_NORMAL;	break; // 1 point below norm. 
	case WT_NORM_PLUS_TWO: PRI = THREAD_PRIORITY_HIGHEST;		break; // 2 point below norm 
	case WT_NORM_LESS_ONE: PRI = THREAD_PRIORITY_LOWEST;		break; // 3 point below norm 
	case WT_NORM_LESS_TWO: PRI = THREAD_PRIORITY_IDLE;			break; // 4 point below norm 
	default:PRI = THREAD_PRIORITY_NORMAL;		break;
	}
	Thread_SetThreadPriority(threadHandle,PRI);
#else
	case WT_NORM_PLUS_ONE: PRI = CE_THREAD_PRIORITY_ABOVE_NORMAL;	break; // 1 point below norm. 
	case WT_NORM_PLUS_TWO: PRI = CE_THREAD_PRIORITY_HIGHEST;		break; // 2 point below norm 
	case WT_NORM_LESS_ONE: PRI = CE_THREAD_PRIORITY_LOWEST;		break; // 3 point below norm 
	case WT_NORM_LESS_TWO: PRI = CE_THREAD_PRIORITY_IDLE;			break; // 4 point below norm 
	default:PRI = CE_THREAD_PRIORITY_NORMAL;		break;
	}
	Thread_SetThreadPriority(threadHandle, PRI);
#endif
}
