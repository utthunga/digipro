#ifndef _XPLATFORM_WIN32_THREAD_H
#define _XPLATFORM_WIN32_THREAD_H

#define CALLBACK __stdcall


// Wait or Timer event call back handler.
typedef void (CALLBACK * THREAD_WAITORTIMERCALLBACK) (PVOID, waitStatus_t);



#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS 7 >> warning C4005: 'WT_EXECUTEINPERSISTENTTHREAD' : macro redefinition
#undef WT_EXECUTEONLYONCE	/*  REGISTERED_WAIT */
#endif

// stevev 31Aug05
#define WT_INIT_COM_MULTI               0x00000800	// do a CoInitializeEx(NULL,MULTI_THRESDED)
#define WT_INIT_COM_SINGLE				0x00000400  //
// end stevev 31aug05
//defined in winnt.h  #define WT_EXECUTEINWAITTHREAD		0x00000004	// not implemented
//defined in winnt.h  #define WT_EXECUTEINIOTHREAD			0x00000001	// not implemented
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS 7 >> warning C4005: 'WT_EXECUTEINPERSISTENTTHREAD' : macro redefinition
#undef WT_EXECUTEINPERSISTENTTHREAD			/*  REGISTERED_WAIT */
#undef WT_EXECUTELONGFUNCTION
#endif

#define WT_EXECUTEINPERSISTENTTHREAD	0x00000000	// not implemented
#define WT_EXECUTELONGFUNCTION			0x00000000	// not implemented

/*  flags 
WT_EXECUTEDEFAULT			By default, the callback function is queued to a non-I/O worker thread 
WT_EXECUTEINWAITTHREAD		The callback function is invoked by the wait thread itself. This flag 
		should be used only for short tasks or it could affect other wait operations. 
		Deadlocks can occur if some other thread acquires an exclusive lock and calls the 
		Thread_UnregisterWait or UnregisterWaitEx function while the callback function is trying to 
		acquire the same lock. 
 
WT_EXECUTEINIOTHREAD		The callback function is queued to an I/O worker thread. This flag 
		should be used if the function should be executed in a thread that waits in an alertable 
		state. 
		The callback function is queued as an APC. Be sure to address reentrancy issues if the 
		function performs an alertable wait operation. 
 
WT_EXECUTEINPERSISTENTTHREAD The callback function is queued to a thread that never terminates. 
		This flag should be used only for short tasks or it could affect other wait operations. 
		Note that currently no worker thread is persistent, although no worker thread will 
		terminate if there are any pending I/O requests.
 
WT_EXECUTELONGFUNCTION		Specifies that the callback function can perform a long wait. This 
		flag helps the system to decide if it should create a new thread.  
WT_EXECUTEONLYONCE			The thread will no longer wait on the handle after the callback 
		function has been called once. Otherwise, the timer is reset every time the wait 
		operation completes until the wait operation is canceled.  
*/

#ifdef _WIN32_WCE
// PAW thread priorities for CE 24/07/09
#define CE_THREAD_PRIORITY_ABOVE_NORMAL	250 // 1 point above norm. 
#define CE_THREAD_PRIORITY_HIGHEST		249 // 2 point above norm 
#define CE_THREAD_PRIORITY_LOWEST		253 // 3 point below norm 
#define CE_THREAD_PRIORITY_IDLE			255	// 4 point below norm 
#define CE_THREAD_PRIORITY_NORMAL		251	// normal
unsigned char delay_send = FALSE;	// debugging 24/08/09 PAW
// PAW end 24/07/09
#endif


typedef DWORD THREAD_ID_TYPE;
typedef HANDLE THREAD_HANDLE_TYPE;
#define THREAD_HANDLE_INVALID ((THREAD_HANDLE_TYPE)0xCf000008L)

/*
	Zero is an invalid thread ID
	See:
		http://blogs.msdn.com/b/oldnewthing/archive/2004/02/23/78395.aspx
		http://msdn.microsoft.com/en-us/library/ms683233(VS.85).aspx
*/
#define THREAD_ID_INVALID ((THREAD_ID_TYPE)0L)
#define THREAD_ID_NONE (THREAD_ID_INVALID)
#define THREAD_ID_OOR ((THREAD_ID_TYPE)-1L)

// define the assumed handle definition  >>>>>>>>>>>>>>> OS DEPENDENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define OS_HANDLE             void*
#define OS_INVALID_HANDLE   ((OS_HANDLE)0xCf000008L) 


#endif // _XPLATFORM_WIN32_THREAD_H
