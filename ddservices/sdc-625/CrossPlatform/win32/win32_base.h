#ifndef _XPLATFORM_WIN32_BASE_H
#define _XPLATFORM_WIN32_BASE_H

#include <basetsd.h>

typedef  unsigned __int64  UINT64;


/* from HOMZ' stdafx.h - too easy to slip windows in and/or know which stdafx.h is being used */  
// _MSC_VER == 1200  for VC 6
// _MSC_VER == 1300  for VS 7
// _MSC_VER == 1310  for VS 7.1

#if _MSC_VER >= 1300  // 
       // By defining ULONG_PTR, we remove error C2146, missing ';' before identifier 'KSPIN_LOCK' 
//  typedef unsigned long ULONG_PTR, *PULONG_PTR; These already exist in VS2005 PAW

       // Prevent error C2146: syntax error : missing ';' before identifier 'LPARAM'
  typedef long LONG_PTR, *PLONG_PTR;

       // Prevent C2061: syntax error : identifier 'DWORD_PTR' and 'PDWORD_PTR'
//  typedef ULONG_PTR DWORD_PTR, *PDWORD_PTR;These already exist in VS2005 PAW

       // Prevent C2061: syntax error : identifier 'HANDLE_PTR'
  typedef unsigned long HANDLE_PTR;
#endif



#if _MSC_VER < 1300  // HOMZ - port to 2003, VS 7
typedef           char  INT8;
#else
//////  HOMZ 
//////  error C2371: redefinition
#endif
/////////////////////////////////////////////////////////////////


// windows defined:: typedef  unsigned   int    UINT32;
// windows defined:: typedef             int    INT32;



#include "shared/shared_base.h"

#endif // _XPLATFORM_WIN32_BASE_H

