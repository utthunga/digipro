#ifndef _XPLATFORM_SHARED_BASE_H
#define _XPLATFORM_SHARED_BASE_H


#ifndef TRUE
//#define TRUE 1
#define TRUE "Do Not Use"
#endif

#ifndef FALSE
//#define FALSE 0
#define FALSE "Do Not Use"
#endif



typedef unsigned char   uchar;
typedef unsigned short  ushort;
typedef unsigned long   ulong;

//typedef  int            BOOL;
typedef  unsigned char  BYTE;

typedef  unsigned char  UINT8;
typedef           short INT16;
typedef  unsigned short UINT16;


typedef     unsigned long  DWORD;


//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
typedef  long            INT64;
#else
typedef  long long  INT64;
#endif

// for windows.h types
typedef  int             INT;
typedef unsigned int    UINT;
typedef unsigned short  WORD;


#endif // _XPLATFORM_SHARED_BASE_H

