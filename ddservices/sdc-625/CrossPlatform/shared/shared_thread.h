#ifndef _XPLATFORM_SHARED_THREAD_H
#define _XPLATFORM_SHARED_THREAD_H


class oneWait
{
public:
	HANDLE eventHandle;
	THREAD_HANDLE_TYPE hThread;	
	THREAD_WAITORTIMERCALLBACK Callback;
	PVOID Context;
	ULONG dwTimeout;
	BOOL  onceOnly;
	BOOL  Die;
	THREAD_ID_TYPE dwThreadID;
	WAIT_FUNCTION_TYPE WaitFunction;

#if !defined(__GNUC__)
	BOOL  enableCOM;
#endif // __GNUC__

#ifdef _DEBUG
	string waitName;
#endif
	
	oneWait():
		eventHandle(NULL),
		hThread(NULL),
		Callback(NULL),
		Context(NULL),
		dwTimeout(INFINITE),
		onceOnly(FALSE),
		Die(FALSE),
		dwThreadID(0),
		WaitFunction(NULL)
#if !defined(__GNUC__)
		, enableCOM(FALSE)
#endif // __GNUC__
#ifdef _DEBUG
		, waitName()
#endif
	{
	};
			  
	oneWait(const oneWait& src)
	{	eventHandle = src.eventHandle;	Callback = src.Callback; 
		Context     = src.Context;		dwTimeout= src.dwTimeout; 
		onceOnly    = src.onceOnly;		hThread  = src.hThread;
		dwThreadID  = src.dwThreadID;   Die      = src.Die;

#if !defined(__GNUC__)
		enableCOM   = src.enableCOM;
#endif // __GNUC__

#ifdef _DEBUG
		waitName    = src.waitName;
#endif
	};
};



#endif // _XPLATFORM_SHARED_THREAD_H