#ifndef _XPLATFORM_SHARED_LOGGING_H
#define _XPLATFORM_SHARED_LOGGING_H

/* --- Shared Internal Types ---------------------------------------------- */

typedef struct err_s
{
	int		 err_number;
	wchar_t* err_str;
} errStruct_t;

#endif // _XPLATFORM_SHARED_LOGGING_H
