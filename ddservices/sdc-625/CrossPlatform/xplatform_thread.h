#ifndef _XPLATFORM_THREAD_H
#define _XPLATFORM_THREAD_H


#include "xplatform_base.h"

/*
	NOTE: This is used in the callback type definition in platform and hence 
	needs to be defined before including those headers.
*/
typedef enum waitStatus_e
{
	evt_fired,		// 0 - false
	evt_timeout,	// event timed out
	evt_lastcall	// this is the last call, cleanup to die.
}
/*typedef*/ waitStatus_t;


#ifndef VOID

#define VOID void
typedef char CHAR;
typedef short SHORT;
typedef long LONG;

typedef unsigned long ULONG;
typedef void *  HANDLE;
typedef HANDLE* PHANDLE;
typedef void *  PVOID;
typedef unsigned char  BOOLEAN;  

#endif // VOID


#if defined(__GNUC__)

#include "linux/linux_thread.h"
#include "linux/linux_mutex.h"

#else // !__GNUC__

#include "win32/win32_thread.h"

#endif // __GNUC__


#ifndef INFINITE
#define INFINITE            0xFFFFFFFF  // Infinite timeout
#endif // INFINITE

/*  
	Wait 'dwFlags' masks: Thread execution option masks.

	NOTE: Additional Windows option masks are defined in win32/thread.h

	WT_EXECUTEDEFAULT: By default, the callback function is queued to a 
		non-I/O worker thread 

	WT_EXECUTEONLYONCE: The thread will no longer wait on the handle after the 
		callback function has been called once. Otherwise, the timer is reset 
		every time the wait operation completes until the wait operation is canceled.  
*/
#define WT_EXECUTEDEFAULT				0x00000000
#define WT_EXECUTEONLYONCE				0x00000001

/*  
	Wait 'dwFlags' masks: Thread priority masks.

	WT_NEW_PRIORITY: Priority bit mask (for internal use).
	WT_NORMAL_PRI: Use normal, default priority.
	WT_NORM_PLUS_ONE: One level above normal.
	WT_NORM_PLUS_TWO: Two levels above normal.
	WT_NORM_LESS_ONE: One level below normal.
	WT_NORM_LESS_TWO: Two levels below normal.
*/
#define WT_NEW_PRIORITY					0x00000070	/* 3 bits mask 0 (normal) to 4 (idle) */
#define WT_NORMAL_PRI					0x00000000
#define WT_NORM_PLUS_ONE				0x00000010
#define WT_NORM_PLUS_TWO				0x00000020
#define WT_NORM_LESS_ONE				0x00000030
#define WT_NORM_LESS_TWO				0x00000040


// Wait thread function
class oneWait;
typedef DWORD (* WAIT_FUNCTION_TYPE)(oneWait*);

THREAD_HANDLE_TYPE Thread_CreateThread(
	oneWait* pWrkWait,
	ULONG dwFlags,
	THREAD_ID_TYPE* pThreadId
);

void Thread_ExitThread(DWORD exitCode);

bool Thread_TerminateThread(
	THREAD_HANDLE_TYPE threadHandle,
	DWORD exitCode
);


void Thread_CloseHandle(THREAD_HANDLE_TYPE threadHandle);

void Thread_HandleWaitSetUp(oneWait* pWait);

void Thread_HandleWaitCleanUp(oneWait* pWait);


THREAD_ID_TYPE Thread_GetCurrentThreadId();


bool Thread_IsThreadSame(
	THREAD_ID_TYPE threadOneId,
	THREAD_ID_TYPE threadTwoId
);


THREAD_HANDLE_TYPE Thread_GetCurrentThread();

int Thread_GetThreadPriority(THREAD_HANDLE_TYPE threadHandle);

void Thread_SetThreadPriority(
	THREAD_HANDLE_TYPE threadHandle, 
	int priorityValue
);

void Thread_SetThreadPriorityFromMask(
	THREAD_HANDLE_TYPE threadHandle, 
	ULONG dwFlagsPriorityMask
);

// generate background task to translate the xmit packets and send 'em thru to the pipe
// using RegisterWait ForSingleObject(QueueEvent)
// use Thread_UnregisterWait(hNewWaitObject) to kill it
bool Thread_RegisterWaitForSingleObject( 
	  PHANDLE phNewWaitObject,       // wait handle-----------------returned value
	  HANDLE hObject,                // handle to object------------anEvent
	  THREAD_WAITORTIMERCALLBACK Callback,  // timer callback function-----write this
	  PVOID Context,                 // callback function parameter- a ptr 2 this class
	  ULONG dwMilliseconds,          // time-out interval ----------INFINITY
	  ULONG dwFlags                  // options---------------------continuous wait
#ifdef _DEBUG
	  , char* debugWaitName
#endif
); 

bool Thread_UnregisterWait(
  HANDLE WaitHandle,      // wait handle
  ULONG dwMilliseconds = 10000
);

THREAD_ID_TYPE Thread_GetWaitThreadID(HANDLE WaitHandle);	// stevev 31aug05



#endif // _XPLATFORM_THREAD_H
