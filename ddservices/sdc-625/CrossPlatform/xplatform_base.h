#ifndef _XPLATFORM_BASE_H
#define _XPLATFORM_BASE_H

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <limits>


#if defined(__GNUC__)



#include "linux/linux_base.h"

// Functions and types for compatibility with VC++ CRT equivalents.
#include "linux/vccrtcompat.h"


#else // !__GNUC__


#include "win32/win32_base.h"


#endif // __GNUC__



#endif // _XPLATFORM_BASE_H

