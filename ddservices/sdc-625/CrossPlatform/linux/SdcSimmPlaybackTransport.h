/*****************************************************************************
    Copyright (c) 2013 Fluke Corporation. All rights reserved.
******************************************************************************

    Authored By    - Dileepa Prabhakar
*/

/** @file
    Transport to play back recorded SDC625 Windows simulation transactions.

*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#ifndef _SDC_SIMM_PLAYBACK_TRANSPORT_H
#define _SDC_SIMM_PLAYBACK_TRANSPORT_H

/* INCLUDE FILES ************************************************************/

#include <iostream>
#include <fstream>
#include <string>


/* CLASS DECLARATIONS *******************************************************/


/*==========================================================================*/
/** SdcSimmPlaybackTransport definition.

    The playback transport uses a user-specified recording file to 
    playback previously recorded transactions.

    It matches the command being 'transmitted' against a command in the 
    recording file and then returns the next response as the response
    to that command.

    It is not designed to be intelligent. It does not know anything about
    HART packets. It's a dumb, sub-string matching implementation that 
    is designed to help test the SDC625 threading and event object model
    under Linux.
*/

class SdcSimmPlaybackTransport 
{
public:

    /* Public Types ========================================================*/

    /* Public Constants ====================================================*/

    /* Construction and Destruction ========================================*/

    /*----------------------------------------------------------------------*/
    /** Construct a SdcSimmPlaybackTransport.

        @param pPlaybackFilePath - Path to recorded playback file.
    */
    explicit SdcSimmPlaybackTransport(const char *pPlaybackFilePath);


    /*----------------------------------------------------------------------*/
    /** Destruct a SdcSimmPlaybackTransport.
    */
    virtual ~SdcSimmPlaybackTransport();


    /* Public Member Functions =============================================*/

    /*----------------------------------------------------------------------*/
    /** Transmit command.

        @param command - Command to transmit.

        @return True if command was successfully sent.
    */
    bool transmit(const std::string& command);

    /*----------------------------------------------------------------------*/
    /** Receive response.

        @param pResponse - Pointer to string to store response.
            If no match is found, the string is cleared.
        
        @return True if response received, false otherwise.
    */
    bool receive(std::string* pResponse);

    /*----------------------------------------------------------------------*/
    /** Transmit command and wait and receive command-response.

        @param command - Command to transmit.
        @param pResponse - Pointer to string to store response.
            If no match is found, the string is cleared.

        @return True on success transmit/receive, false otherwise.
    */
    bool getResponse(
        const std::string& command, 
        std::string* pResponse
    );

protected:

    /* Protected Member Functions ==========================================*/

    // None.


private:

    /* Private Types =======================================================*/


    /* Private Constants ===================================================*/

    /// Header for recorded command.
    static const char TX_COMMAND_HEADER[];

    /// Header for recorded responses.
    static const char RX_RESPONSE_HEADER[];


    /* Private Member Functions ============================================*/

    /*----------------------------------------------------------------------*/
    /** Find specified command in playback file.

        @param command - Command to find in playback file.

        @note Performs a circular search from current position through
            EoF back to current position.

        @return True if command was found, false otherwise.
    */
    bool findPlaybackCommand(const std::string& command);

    /*----------------------------------------------------------------------*/
    /** Get playback response.

        @note Following a successful call to findPlaybackCommand, a 
            call to this method returns the corresponding response.

        @param pResponse - Pointer to string to store matching response.
            If no match is found, the string is cleared.

        @return True if response was found, false otherwise.
    */
    bool getPlaybackResponse(std::string* pResponse);

    /*----------------------------------------------------------------------*/
    /** Is the playback file still alright to read?

        @return True if OK to read, false otherwise.
    */
    bool isPlaybackFileOkToRead() const;


    /* Private Data Members ================================================*/

    /// Stream associated with recorded file.
    std::ifstream m_playbackFile;


    /*= Explicitly Disabled Methods ========================================*/

    /// Assignment operator (disabled).
    SdcSimmPlaybackTransport& operator=(const SdcSimmPlaybackTransport& rhs);

    /// Copy constructor (disabled).
    SdcSimmPlaybackTransport(const SdcSimmPlaybackTransport& rhs);
};


#endif // _SDC_SIMM_PLAYBACK_TRANSPORT_H

