#ifndef _XPLATFORM_LINUX_THREAD_H
#define _XPLATFORM_LINUX_THREAD_H

#include <pthread.h>

/*
	For pthreads, thread ID is the thread handle.

	Note: 
	  pthread IDs (pthread_t objects) are considered 'opaque' and 
	  should not be assumed to be of a native type.

*/
typedef pthread_t THREAD_ID_TYPE;
typedef pthread_t THREAD_HANDLE_TYPE;

#define THREAD_HANDLE_INVALID ((THREAD_HANDLE_TYPE)0)

/*
	TODO(LINUX_PORT, "There is no reliable way to detect invalid thread "
		"IDs using pthreads. Using getpid() is not an option since "
		"it always returns the process ID for NTPL threads. Also "
		"can't use gettid() since that is Linux specific -- not "
		"available on Cygwin. The THREAD_ID_OOR value is used in "
		"hCcountingMutex to control state. hCcountingMutex should be "
		"updated to not depend on thread ID value itself for state.");

	Note: 
		pthread IDs (pthread_t objects) are considered 'opaque' and 
		should not be assumed to be of a native type.
*/
#define THREAD_ID_INVALID ((THREAD_ID_TYPE)0)
#define THREAD_ID_NONE (THREAD_ID_INVALID)
#define THREAD_ID_OOR ((THREAD_ID_TYPE)-1)

#define OS_HANDLE             void*
#define OS_INVALID_HANDLE   ((OS_HANDLE)NULL) 


// Wait or Timer event call back handler.
typedef void (* THREAD_WAITORTIMERCALLBACK) (PVOID, waitStatus_t);


#define CALLBACK 

/*
    Thread priority levels.

    Note:
        * Priority level names match Windows names.
          http://msdn.microsoft.com/en-us/library/windows/desktop/ms685100(v=vs.85).aspx
        * In Linux, only root (administrator) users have the ability
          to switch scheduling policies and change priorities.
*/
const int THREAD_PRIORITY_IDLE = 0;
const int THREAD_PRIORITY_LOWEST = 1;
const int THREAD_PRIORITY_BELOW_NORMAL = 2;
const int THREAD_PRIORITY_NORMAL = 3;
const int THREAD_PRIORITY_ABOVE_NORMAL = 4;
const int THREAD_PRIORITY_HIGHEST = 5;
const int THREAD_PRIORITY_TIME_CRITICAL = 15;

#endif // _XPLATFORM_LINUX_THREAD_H
