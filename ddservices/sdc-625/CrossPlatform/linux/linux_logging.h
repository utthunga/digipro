/**********************************************************************************************
 *
 * $Workfile: logging.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *********************************************************************************************
 * This file was generated for the logging.cpp file
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *		logging.h : external interface to the error logging functions
 *
 * #include "logging.h"
 *
 */


#ifndef _XPLATFORM_LINUX_LOGGING_H
#define _XPLATFORM_LINUX_LOGGING_H

#include <inttypes.h>


extern bool isLogPermitted(uint32_t perm);

// Log a standard string to the specified channels.
int logout(
    const char* filename, 
    const char* funcname, 
    int line, 
    int channel, 
    const char* format, 
    ...
);

// Log a wide string to the specified channels.
int logout(
    const char* filename, 
    const char* funcname, 
    int line, 
    int channel, 
    const wchar_t* format, 
    ...
);

// Log error information mapping to error number to the specified channels.
int logout(
    const char* filename, 
    const char* funcname, 
    int line, 
    int channel, 
    int errNumber, 
    ...
);


#define LOGIT(...) ::logout(__FILE__, __func__, __LINE__, __VA_ARGS__)


/* Usage:
 * LOGIT(CERR_LOG | CLOG_LOG, "ERROR: Failed to find %d in %s.\n", 42, "The answer");
 */
/* Args requires
 *	'O' , "outputFileName"
 *	'E' , "errorFileName"
 *  'G' , "logFileName"
 *
 * if any are the empty string then that log path will go into the bit bucket.
 */


#define SDCLOG(...) LOGIT(__VA_ARGS__)

#define LOGIF(perm)    if (isLogPermitted(perm)) LOGIT
/* Usage:
 * LOGIF(LOGP_DEPENDENCY)(CERR_LOG | CLOG_LOG, 
 *								"ERROR: Failed to find %d in %s.\n", 42, "The answer");
 */


// if LOGTHIS(LOGP_DEPENDENCY) {}
#define LOGTHIS(perm) (isLogPermitted(perm)) /* means ok2logit */

#ifdef _DEBUG

#define DEBUGLOG    LOGIT(__VA_ARGS__)
#define DBGLOGIF(V)(...)    if(V) LOGIT

#else // !_DEBUG


/*
 * Because of the way DBGLOGIF is used in ddbReference.cpp and because of how GCC CPP
 * pre-processes this, redirect DBGLOGIF to another varargs macro that expands to void.
 */
#define DUMMY_DBGLOGIF( ... ) ((void)0)
#define DBGLOGIF( V )  DUMMY_DBGLOGIF

#define DEBUGLOG( L, ... )	((void)0)

//#define LOGTHIS( a ) (false) /* should optimize out the entire statement*/
#endif // _DEBUG


// TODO(LINUX_PORT, "Add a logSetup function if required.");


#endif // _XPLATFORM_LINUX_LOGGING_H
