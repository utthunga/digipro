#ifndef _LINUX_MUTEX_H
#define _LINUX_MUTEX_H

#include <pthread.h>


// A Mutex implementation using pthread_mutex.
class Mutex
{
public:
    // Construct a mutex.
    Mutex();

    /*
        Destruct a mutex.

        Note: The behavior is undefined if a locked mutex is destructed.
    */
    virtual ~Mutex();

    // Lock this mutex.
    void lock();

    // Unlock this mutex.
    void unlock();

protected:

    // pthread mutex object.
    pthread_mutex_t m_mutex;


private:

    /* Explicitly Disabled Methods */

    // Assignment operator (disabled).
    Mutex& operator=(const Mutex& rhs);

    // Copy constructor (disabled).
    Mutex(const Mutex& rhs);
};


/*
    A recursive mutex implementation using pthread_mutex.

    A recursive mutex can be reacquired in the same calling thread.
*/
typedef Mutex BASE_OF_RecursiveMutex;
class RecursiveMutex : public BASE_OF_RecursiveMutex
{
public:
    // Construct a recursive mutex.
    RecursiveMutex();

    /*
        Destruct a recursive mutex.

        Note: The behavior is undefined if a locked mutex is destructed.
    */
    virtual ~RecursiveMutex();

private:

    /* Explicitly Disabled Methods */

    // Assignment operator (disabled).
    RecursiveMutex& operator=(const RecursiveMutex& rhs);

    // Copy constructor (disabled).
    RecursiveMutex(const RecursiveMutex& rhs);
};


/*
    MutexLocker - a helper class that acquires lock on specified mutex object
    on construction and releases the lock on destruction.
*/
class MutexLocker
{
public:
    /*
        Construct a MutexLocker and acquire lock on mutex.

        Params:
            mutex - Mutex (or sub-classes) to acquire lock on.
    */
    explicit MutexLocker(Mutex& mutex);
    

    // Destruct a MutexLocker and release lock on mutex.    
    ~MutexLocker();

private:

    /// Reference to mutex under lock.
    Mutex& m_mutex;

    /* Explicitly Disabled Methods */

    // Assignment operator (disabled).
    MutexLocker& operator=(const MutexLocker& rhs);

    // Copy constructor (disabled).
    MutexLocker(const MutexLocker& rhs);
};


#endif // _LINUX_MUTEX_H
