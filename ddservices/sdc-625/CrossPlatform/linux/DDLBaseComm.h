/*************************************************************************************************
 *
 * Workfile : DDLBaseComm.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "DDLBaseComm.h"
 */


#ifndef _DDLBASECOMM_H
#define _DDLBASECOMM_H


#include "pvfc.h"
#include "ddbCommInfc.h"
#include "DDLCommErr.h"
#include "messageUI.h"

////////////////////////////////////////////////////////////////
typedef enum commStatus_e
{
	commInitialize = 0,
	commOK,			// normal
	commNoDev,		// device did not respond to the last command
	commNC,			// not connected
	commShuttingDown,// begin shutdown handshake
	commShutDown	// complete shutdown handshake
}
/*typedef*/ commStatus_t;

////////////////////////////////////////////////////////////////
class HartException  
{
public:
	int completionCode;
	HartException(int code) {completionCode = code;};
	virtual ~HartException(){};

	static const TCHAR* except1020;
	static const TCHAR* except1021;
	static const TCHAR* except1022;
	static const TCHAR* except1023;
	static const TCHAR* except1024;
	const
	TCHAR* errorStr(void) {if (completionCode==1020) return except1020; else
						   if (completionCode==1021) return except1021; else
						   if (completionCode==1022) return except1022; else
						   if (completionCode==1023) return except1023; else
						   if (completionCode==1024) return except1024; else
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
							   return M_UI_UNK_DLL_EXCEPTION;
#else				// M_UI_UNK_DLL_EXCEPTION is defined in messageUI.h
					// which shall be used by the GUI app of Windows
							   ;	// TODO: Equivalent has to be found.
#endif
	};
};

class CMainFrame;
class CDDIdeDoc;

class CbaseComm : public hCcommInfc
{
protected:	
	CDDIdeDoc*   m_pDoc;
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
	CRITICAL_SECTION BaseCommNotifySection; // we need to stop task switches during notify
#else
	// TODO: Linux equivalent has to be added
	// CRITICAL_SECTION : A non-reentrant mutex which is explicitly aware of the Concurrency Runtime.
#endif

public:			
		bool  m_pending;// stevev 31jul08 - try to prevent wait-forever lockup in all drivers

public:
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
	CbaseComm() {m_pending = false;
	InitializeCriticalSectionAndSpinCount(&BaseCommNotifySection, 0x80000400);
	};
#else
	CbaseComm();
	// TODO: Equivalent has to be found. InitializeCriticalSectionAndSpinCount
	// initializes a critical section object and sets the spin count for the critical section.
#endif
	virtual ~CbaseComm(){
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
		DeleteCriticalSection(&BaseCommNotifySection);
#else
		// TODO: Equivalent has to be found. DeleteCriticalSection
		// releases all resources used by an unowned critical section object.
#endif
	};

	/// pass thru as pure virtual
	virtual											// 'PUSH INTERFACE' no longer == 0 - 25jan06
		void  notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged = NO_change, long aTyp = 0);	
	virtual // call back for windows actions in response to response code, device status and cmd 48
         void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle) PVFC( "CbaseComm1" );

    virtual
         short setNumberOfRetries( short newVal )  RPVFC( "hCcommInfc_C",0 );// returns previous val (for methods)
	
	virtual
		hPkt* getMTPacket(bool pendOnMT) RPVFC( "CbaseComm2",NULL );// gets an empty packet for generating a command 
									//		(gives caller ownership of the packet*)
	virtual
		void  putMTPacket(hPkt*)PVFC( "CbaseComm3" );	// returns the packet memory back to the interface 
									//		(this takes ownership of the packet*)
	virtual
		RETURNCODE
			SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "CbaseComm4",0 );
						   // puts a packet into the send queue  (takes ownership of the packet*)
	virtual
		RETURNCODE 
			SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "CbaseComm5",0 );
			// puts a packet into the front of the send queue(takes ownership of the pkt*)
	virtual
		RETURNCODE		
			SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "CbaseComm6",0 );
	virtual
		int sendQsize(void)RPVFC( "CbaseComm7",0 ); 	        // how many are in the send queue ( 0 == empty )
	virtual
		int	sendQmax(void) RPVFC( "CbaseComm8",0 );		    // how many will fit
    virtual
		RETURNCODE  initComm(void) RPVFC( "CbaseComm9",0 );  // setup connection (called after instantiation of device)
	virtual
		RETURNCODE  shutdownComm(void)RPVFC( "CbaseComma",0 );
	virtual
		RETURNCODE GetIdentity(Indentity_t& retIdentity, BYTE pollAddr) RPVFC( "CbaseCommb",0 );
    virtual
		void  disableAppCommRequests(void) PVFC( "CbaseComme" );
	virtual		
		void  enableAppCommRequests (void) PVFC( "CbaseCommf" );
	virtual
		bool  appCommEnabled (void) RPVFC( "CbaseCommg",false ); 

virtual	
RETURNCODE ZeroPkt2Identity(hPkt* pPkt, Indentity_t& retIdentity)
{// parse the data to an identity structure 
	RETURNCODE rc = FAILURE;	
	retIdentity.clear();
	if (pPkt->dataCnt < 14 || pPkt->cmdNum != 0 )  // probably an error 
	{
		clog << "Identity Too short ("<< (int)pPkt->dataCnt << " bytes)"<<endl;
		rc = DDL_COMM_EXTRACTION_FAILED;
	}
	else//      5                                 6                        7
	{	// break it up
		//ST RC DATA
		//DATA	FE
		//		HH  Mfg						  HH   Mfg				HHHH   Expanded DevType
		//		HH	DevType					  HH   DevType			  
		//		HH	PreAmb					  HH   Reqst-PreAmb		  HH   Reqst-PreAmb
		//		HH  CmdRev-- 5				  HH   CmdRev-- 6		  HH   CmdRev-- 7
		//		HH	DevRev					  HH   DevRev			  HH   DevRev
		//		HH	SoftRev					  HH   SoftRev			  HH   SoftRev
		//		HH	top 5 bits - Hardwr Rev	  5b   Hdwr Rev			  5b   Hdwr Rev
		//			low 3 bits - Signal Code  3b   SignalCd			  3b   SignalCd
		//		HH	FuncFlags				  HH   FuncFlags		  HH   FuncFlags
		//		HHHHHH DeviceID				HHHHHH DeviceID			HHHHHH DeviceID
		//									  HH   Resp-PreAmbl		  HH   Resp-PreAmbl
		//									  HH   Num Dev-Vars		  HH   Num Dev-Vars
		//									HHHH   Config-Chng-Cnt	HHHH   Config-Chng-Cnt
		//									  HH   Extend-Dev Stat    HH   Extend-Dev Stat
		//															HHHH   Mfg Id Code
		if (pPkt->theData[2] != 254)
		{
			cerr << "ERROR: Zero cmd Constant is not in the right place."<<endl;
			rc = DDL_COMM_EXTRACTION_FAILED;
		}
		else
		if (pPkt->theData[6] == 5 )
		{	rc = SUCCESS;
			retIdentity.wManufacturer = pPkt->theData[ 3]; 
			retIdentity.wDeviceType   = pPkt->theData[ 4];
			retIdentity.cReqPreambles = pPkt->theData[ 5];  
			retIdentity.cUniversalRev = pPkt->theData[ 6];
			retIdentity.cDeviceRev    = pPkt->theData[ 7];
			retIdentity.cSoftwareRev  = pPkt->theData[ 8];
			retIdentity.cHardwareRev  = pPkt->theData[ 9];
			retIdentity.cZeroFlags    = pPkt->theData[10];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = pPkt->theData[11];
			pID[1] = pPkt->theData[12];
			pID[0] = pPkt->theData[13];
		}
		else
		if (pPkt->theData[6] == 6 )
		{	rc = SUCCESS;
			retIdentity.wManufacturer = pPkt->theData[ 3]; 
			retIdentity.wDeviceType   = pPkt->theData[ 4];
			retIdentity.cReqPreambles = pPkt->theData[ 5];  
			retIdentity.cUniversalRev = pPkt->theData[ 6];
			retIdentity.cDeviceRev    = pPkt->theData[ 7];
			retIdentity.cSoftwareRev  = pPkt->theData[ 8];
			retIdentity.cHardwareRev  = pPkt->theData[ 9];
			retIdentity.cZeroFlags    = pPkt->theData[10];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = pPkt->theData[11];
			pID[1] = pPkt->theData[12];
			pID[0] = pPkt->theData[13];
			retIdentity.cRespPreambles= pPkt->theData[14];
			retIdentity.cMaxDevVars   = pPkt->theData[15];
			retIdentity.wCfgChngCnt   =(pPkt->theData[16]<<8)+pPkt->theData[17];
			retIdentity.cExtDevStatus = pPkt->theData[18];
		}
		else
		if (pPkt->theData[6] >= 7 )/* this is 'forward compatability'  ooook */
		{	rc = SUCCESS;
			retIdentity.wDeviceType   =(pPkt->theData[ 3]<<8)+pPkt->theData[ 4];  
			retIdentity.cReqPreambles = pPkt->theData[ 5];
			retIdentity.cUniversalRev = pPkt->theData[ 6];
			retIdentity.cDeviceRev    = pPkt->theData[ 7];
			retIdentity.cSoftwareRev  = pPkt->theData[ 8];
			retIdentity.cHardwareRev  = pPkt->theData[ 9];
			retIdentity.cZeroFlags    = pPkt->theData[10];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = pPkt->theData[11];
			pID[1] = pPkt->theData[12];
			pID[0] = pPkt->theData[13];
			retIdentity.cRespPreambles= pPkt->theData[14];
			retIdentity.cMaxDevVars   = pPkt->theData[15];
			retIdentity.wCfgChngCnt   =(pPkt->theData[16]<<8)+pPkt->theData[17];
			retIdentity.cExtDevStatus = pPkt->theData[18];
			retIdentity.wManufacturer =(pPkt->theData[19]<<8)+pPkt->theData[20]; 
		}
		else
		{// unknown universal rev
			rc = DDL_COMM_UNKNOWN_UNIV_REV;
		}
	}
	return rc;
};

// L&T Modifications : Real Tx. Support - start
#ifdef __GNUC__
public:

#endif
// L&T Modifications : Real Tx. Support - end

};


#endif//_DDLBASECOMM_H

 /*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
 
/*************************************************************************************************
 *
 *   $History:  $
 *  
 *************************************************************************************************
 */
