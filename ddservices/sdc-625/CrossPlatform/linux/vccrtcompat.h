#ifndef _XPLATFORM_LINUX_VC_CRT_COMPAT_H
#define _XPLATFORM_LINUX_VC_CRT_COMPAT_H


#include <cstdlib>
#include <wchar.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <errno.h>

#include "linux_base.h"

/*
    Windows Sleep(...) sleeps for specified milli seconds. Use Linux usleep
    to port functionality.
*/
#define Sleep(time_msec) usleep(time_msec * 1000)

/*
    Windows GetLastError() returns a thread's last-error code value.

    'errno' does the same in Linux.
*/
#define GetLastError() errno

/*
    "struct _timeb and function _ftime in Windows are struct timeb and ftime 
    in Linux. 
*/
#define _timeb timeb
#define _ftime ftime


/* 
    '_isnan(...)' is used in varient.cpp and is available in Linux 
    as 'isnan(...)'.
*/
#define _isnan isnan


/*
    '_gcvt(...)' is used in varient.cpp and is available in Linux as 
    'gcvt(...)'.
*/
#define _gcvt gcvt


// Convert wide-string to long integer.
#define _wtol(s) wcstol(s, (wchar_t **) NULL, 10)

// Convert wide-string to integer.
#define _wtoi(s) ((int) _wtol(s))

// Convert string to 64-bit unsigned integer.
#define _strtoui64 strtoull


// Convert int to string.
char* _itoa(int value, char* string, int radix);

// Convert unsigned long to string.
char *_ultoa(unsigned long value, char *string, int radix);

// Convert int to wide-string.
wchar_t* _itow(int value, wchar_t* string, int radix);



// Convert string to upper case.
char* _strupr(char* s);

// Convert wide string to upper case.
wchar_t *_wcsupr(wchar_t *str);

// Convert wide string to lower case.
wchar_t *_wcslwr(wchar_t * str);




/*
    Get tick count - time, in milliseconds, from an undefined point since 
    start-up.
*/
unsigned long GetTickCount();


/*
    Induce breakpoint when debugging.

    This is a built-in VC++ feature used in several places. There may not be 
    a Linux equivalent. See implementation for more.
*/
void _CrtDbgBreak();

#endif // _XPLATFORM_LINUX_VC_CRT_COMPAT_H
