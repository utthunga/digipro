#ifndef _EVENT_OBJECTS_POSIX_H 
#define _EVENT_OBJECTS_POSIX_H


/**
 * Description:
 * A simple, lightweight function library for using event objects in POSIX
 *
 * Author: Lars Melander, lars dot melander at gmail dot com
 *
 * Version: 0.5
 *
 * Web:
 *	- http://win32eoposix.sourceforge.net/
 *	- http://sourceforge.net/projects/win32eoposix/
 *
 */


typedef unsigned long  DWORD;


typedef int BOOL;
typedef void *  HANDLE;


/**
 * Constants needed for POSIX
 */

#define TRUE 1
#define FALSE 0

// #define INFINITE -1
#define WAIT_OBJECT_0 0
#define WAIT_TIMEOUT 64
#define WAIT_FAILED 96
#define WAIT_ABANDONED 128
#define WAIT_ABANDONED_0 WAIT_ABANDONED

#define MIL 1000000L
#define BIL 1000000000L
#ifndef ETIMEDOUT
#define ETIMEDOUT 110 // Timeout value
#endif


HANDLE CreateEvent(void *attribute, BOOL manual_reset, BOOL initial_state, void *name);

BOOL SetEvent(HANDLE event_object);

BOOL ResetEvent(HANDLE event_object);

BOOL CloseHandle(HANDLE event_object);

DWORD WaitForMultipleObjects(DWORD count, HANDLE *h_event_object, BOOL wait_all, int timeout_ms);

DWORD WaitForSingleObject(HANDLE event_object, int timeout_ms);

#endif // _EVENT_OBJECTS_POSIX_H
