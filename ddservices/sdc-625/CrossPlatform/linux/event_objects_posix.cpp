#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>

#include "event_objects_posix.h"
#include "../xplatform_thread.h"

/**
 * Description:
 * A simple, lightweight function library for using event objects in POSIX
 *
 * Author: Lars Melander, lars dot melander at gmail dot com
 *
 * Version: 0.5
 *
 */


/**
 * Each thread waiting for an event object will add a list element
 * containing a conditional variable and then wait for the variable
 * to be signalled.
 */
typedef struct t_list_element
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    struct t_list_element *prev, *next;
} *list_element;

/**
 * The event object. Contains pointers for the linked list of
 * conditional variables, a mutex for handlind reading and writing,
 * and a flag to be set or reset.
 *
 * The linked list does not own its elements.
 */
typedef struct t_HANDLE
{
    list_element start, end;
    pthread_mutex_t mutex;
    BOOL flag;
    BOOL manual_reset;
} *EHANDLE;

/**
 * Each time a thread starts waiting for an event object, a
 * conditional variable is added to the linked list in the
 * event object.
 *
 * Parameters:
 * event_object, the pointer to the event object in question.
 *
 * le, the pointer to be added.
 */
static void AddElement(HANDLE h_event_object, list_element le)
{
    EHANDLE event_object = (EHANDLE) h_event_object;
    if (event_object->start == NULL)
    {
        event_object->start = event_object->end = le;
    }
    else
    {
        event_object->end->next = le;
        le->prev = event_object->end;
        event_object->end = le;
    }
}

/**
 * When a thread has finished waiting, the conditional variable
 * is removed from the list.
 *
 * Parameters:
 * event_object, the pointer to the event object in question.
 *
 * le, the pointer to be removed.
 */
static void RemoveElement(HANDLE h_event_object, list_element le)
{
    EHANDLE event_object = (EHANDLE) h_event_object;
    list_element ptr;

    if (event_object->start == event_object->end)
    {
        event_object->start = event_object->end = NULL;
    }
    else if (le == event_object->start)
    {
        event_object->start = event_object->start->next;
    }
    else if (le == event_object->end)
    {
        event_object->end = event_object->end->prev;
    }
    else
    {
        for (ptr = event_object->start->next; ; ptr = ptr->next)
        {
            if (ptr == le)
            {
                ptr->prev->next = ptr->next;
                ptr->next->prev = ptr->prev;
                break;
            }
        }
    }
}

/**
 * Create an event object and return a pointer to it.
 *
 * Parameters:
 * attribute, not used but kept to retain compatibility
 * with the win32 function.
 *
 * manual_reset, note: was always treated as true in original
 * implementation. See SetEvent and WaitForMultipleObjects 
 * for changes to support auto reset.
 *
 *
 * initial_state, if the event object will initally be set
 * or reset.
 *
 * name, not used.
 *
 * Return value:
 * Pointer to the event object.
 */
HANDLE CreateEvent(void *attribute, BOOL manual_reset, BOOL initial_state, void *name)
{
    EHANDLE event_object = (EHANDLE) malloc(sizeof (struct t_HANDLE));
    event_object->start = event_object->end = NULL;
    pthread_mutex_init(&event_object->mutex, NULL);
    event_object->flag = initial_state;
    event_object->manual_reset = manual_reset;
    return (HANDLE) event_object;
}

/**
 * Set an event object and signal any threads that are waiting.
 *
 * Parameters:
 * event_object, the object to be set.
 *
 * Returns:
 * Should return whether function succeeds or fails, but returns
 * only true for the time being.
 */
BOOL SetEvent(HANDLE h_event_object)
{
    EHANDLE event_object = (EHANDLE) h_event_object;
    list_element ptr;

    // Make sure only this thread is the only one using the event object
    pthread_mutex_lock(&event_object->mutex);

    event_object->flag = TRUE;

    // Go through the list of variables and signal them
    for (ptr = event_object->start; ptr != NULL; ptr = ptr->next)
    {
        pthread_mutex_lock(&ptr->mutex);
        pthread_cond_signal(&ptr->cond);
        pthread_mutex_unlock(&ptr->mutex);

        /*
            If this is an auto reset event, exit after signaling only one thread.

            Note: The signal state is reset after the thread has been woken up.

            More info on events and auto-reset:
            http://msdn.microsoft.com/en-us/library/windows/desktop/ms682396(v=vs.85).aspx
            http://www.codeproject.com/Articles/39040/Auto-and-Manual-Reset-Events-Revisited
            http://stackoverflow.com/questions/10276750/is-win32-event-object-recursive-mutexes
        */
        if (FALSE == event_object->manual_reset)
        {
            break;
        }

        if (ptr == event_object->end)
        {
            break;
        }
    }
    pthread_mutex_unlock(&event_object->mutex);

    return TRUE;
}

/**
 * Reset an event object.
 *
 * Parameters:
 * event_object, the object to be reset.
 *
 * Returns:
 * Should return whether function succeeds or fails, but returns
 * only true for the time being.
 */
BOOL ResetEvent(HANDLE h_event_object)
{
    EHANDLE event_object = (EHANDLE) h_event_object;
    // Only this thread accessing the event object
    pthread_mutex_lock(&event_object->mutex);

    event_object->flag = FALSE;
    pthread_mutex_unlock(&event_object->mutex);

    return TRUE;
}

/**
 * Delete an event object.
 *
 * Parameters:
 * event_object, the object to be deleted.
 *
 * Returns:
 * True if the object is deleted successfully, false otherwise.
 */
BOOL CloseHandle(HANDLE h_event_object)
{
    EHANDLE event_object = (EHANDLE) h_event_object;
    // If any thread is still waiting for this event object,
    // then it can't be deleted
    if (event_object->start != NULL)
    {
        return FALSE;
    }

    pthread_mutex_destroy(&event_object->mutex);
    free(event_object);
    return TRUE;
}

/**
 * Make the thread wait for an event object to be set, if it is not
 * already set. This is done by converting the event object pointer
 * into a list of one and calling WAitForMultipleObjects.
 *
 * Parameters:
 * event_object, the object to wait for.
 *
 * timeout_ms, set to the number of milliseconds the thread will wait,
 * or set to INFINITE.
 *
 * Returns:
 * See WaitForMultipleObjects.
 */
DWORD WaitForMultipleObjects(DWORD, HANDLE *, BOOL, int);
DWORD WaitForSingleObject(HANDLE h_event_object, int timeout_ms)
{
    return WaitForMultipleObjects(1, &h_event_object, FALSE, timeout_ms);
}

/**
 * Make the thread wait for one or more event objects to be set/
 *
 * Parameters:
 * count, the number of objects to wait for.
 *
 * event_object, an array of pointers to event objects.
 *
 * wait_all, whether the thread should wait for all objects (true) or
 * if it should continue after only one has been set (false). For now,
 * this is always treated as false.
 *
 * timeout_ms, make the thread continue after a number of milliseconds,
 * otherwise set to INFINITE.
 */
DWORD WaitForMultipleObjects(DWORD count, HANDLE *h_event_object, BOOL wait_all, int timeout_ms)
{
    EHANDLE* event_object = (EHANDLE *) h_event_object;
    struct t_list_element le;
    DWORD i, return_value;
    int check_value = -1;
    struct timespec t;
    struct timeval tv;

    // No event objects to wait for, failed.
    if (count == 0)
    {
        return WAIT_FAILED;
    }

    // First, get exclusive access to all event objects.
    for (i = 0; i < count; ++i)
    {
        pthread_mutex_lock(&event_object[i]->mutex);
    }

    // Initialise the condition variable that this thread will wait for
    pthread_mutex_init(&le.mutex, NULL);
    pthread_cond_init(&le.cond, NULL);

    // Add the variable to all event objects
    for (i = 0; i < count; ++i)
    {
        AddElement(event_object[i], &le);

        // Is any object already set?
        if (event_object[i]->flag)
        {
            check_value = 1;
        }
    }

    // If no object was set, continue with the waiting
    if (check_value == -1)
    {
        // Time to lock the condition variable.
        pthread_mutex_lock(&le.mutex);

        // If the thread is to wait only for a fixed time, convert
        // the number to something the variable can use.
        if (timeout_ms != INFINITE)
        {
            gettimeofday(&tv, NULL);
            t.tv_nsec = tv.tv_usec * 1000  + ((long) timeout_ms) * MIL;
            t.tv_sec = tv.tv_sec;
            if (t.tv_nsec >= BIL)
            {
                t.tv_sec += t.tv_nsec / BIL;
                t.tv_nsec %= BIL;
            }
        }

        // Unlock all event objects before the wait.
        for (i = 0; i < count; ++i)
        {
            pthread_mutex_unlock(&event_object[i]->mutex);
        }

        // Wait.
        if (timeout_ms == INFINITE)
        {
            check_value = pthread_cond_wait(&le.cond, &le.mutex);
        }
        else
        {
            check_value = pthread_cond_timedwait(&le.cond, &le.mutex, &t);
        }

        // What was the reason for continuing?
        if (check_value == ETIMEDOUT)
        {
            return_value = WAIT_TIMEOUT;
        }
        else if (check_value != 0)
        {
            return_value = WAIT_FAILED;
        }

        // Unlock the variable now, in case any thread is waiting for it.
        pthread_mutex_unlock(&le.mutex);

        // Get exclusive access to all event objects before proceeding.
        for (i = 0; i < count; ++i)
        {
            pthread_mutex_lock(&event_object[i]->mutex);
        }
    }

    // No longer using the condition variable, remove it from all event
    // objects.
    for (i = 0; i < count; ++i)
    {
        RemoveElement(event_object[i], &le);

        // If an object was set (which is probably the case), return
        // the index of it.
        if (event_object[i]->flag)
        {
            /*
                If this is an auto reset event, reset signal state.

                Also see SetEvent.

                TODO(LINUX_PORT, "There is presently no support for wait_all when "
                    "waiting on multiple objects. See if there is a need to "
                    "update this function to support wait_all.");

                http://msdn.microsoft.com/en-us/library/windows/desktop/ms687025%28v=vs.85%29.aspx
            */
            if (FALSE == event_object[i]->manual_reset)
            {
                event_object[i]->flag = FALSE;
            }

            return_value = WAIT_OBJECT_0 + i;
        }
    }

    // Finished.
    for (i = 0; i < count; ++i)
    {
        pthread_mutex_unlock(&event_object[i]->mutex);
    }

    // Cleaning up.
    pthread_mutex_destroy(&le.mutex);
    pthread_cond_destroy(&le.cond);

    return return_value;
}

