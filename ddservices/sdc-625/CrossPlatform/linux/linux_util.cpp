/*****************************************************************************
    Copyright (c) 2013 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Authored By    - Dileepa Prabhakar
*/

/** @file
    Linux utility functions.

*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/

#include <cstdlib>

#include "linux_util.h"
#include "logging.h"

/* NON-MEMBER FUNCTION IMPLEMENTATIONS **************************************/

/*---------------------------------------------------------------------------
    Looks up the specified environment variable & returns its value as a 
    string.
*/
std::string getEnvironmentVariable(char* pEnvVar)
{
    // Look up the specified environment variable
    char* pValue = getenv(pEnvVar);

    // If a value was found for the environment variable...
    if((pValue != NULL) && (strlen(pValue)))
    {
        return pValue;
    }
    else
    {
        LOGIT(
            CERR_LOG, 
            "%s environment variable not set or value not available!\n",
            pEnvVar
        );
    }

    return ""; 
}

/*---------------------------------------------------------------------------
    Log time difference between specified end and start time with an 
    optional message.
*/
void logTimeDiff(
    struct timeval startTime, 
    struct timeval endTime, 
    char *message
)
{
    double time1, time2;
    char printMessage[] = "Time difference (s)";
    char *msgPtr = printMessage;

    if (NULL != message)
    {
        msgPtr = message;
    }

    time1 = (double) startTime.tv_sec + ((double) startTime.tv_usec/1000000);
    time2 = (double) endTime.tv_sec + ((double) endTime.tv_usec/1000000);
    LOGIT(CLOG_LOG, "%s: %lf seconds\n", msgPtr, time2 - time1);
}
