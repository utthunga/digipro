#include "linux/linux_mutex.h"


// Construct a mutex.
Mutex::Mutex():
    m_mutex()
{
    pthread_mutex_init(&m_mutex, NULL);
}

// Destruct a mutex.
Mutex::~Mutex()
{
    pthread_mutex_destroy(&m_mutex);
}

// Lock this mutex.
void Mutex::lock()
{
    pthread_mutex_lock(&m_mutex);
}

// Unlock this mutex.
void Mutex::unlock()
{
    pthread_mutex_unlock(&m_mutex);
}





// Construct a recursive mutex.
RecursiveMutex::RecursiveMutex():
    BASE_OF_RecursiveMutex()
{
    // Destroy base class mutex initialization/configuration.
    pthread_mutex_destroy(&m_mutex);

    // Set up reentrant (recursive) mutex.
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(
        &attr,
        PTHREAD_MUTEX_RECURSIVE
    );


    // Create a recursive mutex.
    pthread_mutex_init(&m_mutex, &attr);

    // Destroy attribute object (OK to do after mutex initialization).
    pthread_mutexattr_destroy(&attr);
}


// Destruct a recursive mutex.
RecursiveMutex::~RecursiveMutex()
{
    // Empty (see base class dtor).
}






// Construct a MutexLocker and acquire lock on mutex.
MutexLocker::MutexLocker(Mutex& mutex):
    m_mutex(mutex)
{
    m_mutex.lock();
}


// Destruct a MutexLocker and release lock on mutex.    
MutexLocker::~MutexLocker()
{
    m_mutex.unlock();
}

