#ifndef _XPLATFORM_LINUX_BASE_H
#define _XPLATFORM_LINUX_BASE_H

#include <stdint.h>
#include <cstdlib>
#include <cfloat>
#include <cstring>



/*
    Defining _BASETSD_H_ is required to prevent re-declaration of types in 
    std.h. 
*/
#ifndef _BASETSD_H_
#define _BASETSD_H_

typedef int BOOL;

typedef unsigned long  DWORD;


typedef char INT8;
typedef int32_t INT32;
typedef uint32_t UINT32;
typedef int64_t __int64;
typedef uint64_t UINT64;

#endif // _BASETSD_H_



/*
    CrossPlatform/shared/shared_base.h defines TRUE and FALSE to unusable
    constants if they are not already defined (that code was originally 
    in ddbGeneral.h).

    It looks like TRUE, FLASE definition on Windows is expected from 
    a Windows header file, may be MFC related -- see
    http://msdn.microsoft.com/en-us/library/hh184276(v=vs.88).aspx 


    So provide an equivalent definition for Linux here ahead of including 
    shared_base.h.
*/

#define TRUE 1
#define FALSE 0

#define _UI8_MAX UINT8_MAX
#define _UI16_MAX UINT16_MAX
#define _I32_MAX INT32_MAX
#define _I64_MIN INT64_MIN
#define _I64_MAX INT64_MAX
#define _UI64_MAX UINT64_MAX


#include "shared/shared_base.h"

#endif // _XPLATFORM_LINUX_BASE_H

