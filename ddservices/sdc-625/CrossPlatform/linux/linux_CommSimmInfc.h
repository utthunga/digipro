#ifndef _XPLATFORM_LINUX_COMM_SIMM_INFC_H
#define _XPLATFORM_LINUX_COMM_SIMM_INFC_H

#include "HARTsupport.h"
#include "pvfc.h"
#include "ddbCommInfc.h"
#include "messageUI.h"

#include "linux/SdcSimmPlaybackTransport.h"

// TODO(LINUX_PORT, "Copied from DDLBaseComm.h -- de-duplicate later.");
typedef enum commStatus_e
{
	commInitialize = 0,
	commOK,			// normal
	commNoDev,		// device did not respond to the last command
	commNC,			// not connected
	commShuttingDown,// begin shutdown handshake
	commShutDown	// complete shutdown handshake
}
/*typedef*/ commStatus_t;


// TODO(LINUX_PORT, "Copied from NamedPipe.h");
#define PIPEBUFFERSIZE 512

#define DDL_COMM_ERROR_BASE  3000

// TODO(LINUX_PORT, "Copied from DDLCommErr.h -- de-duplicate later.");
#define DDL_COMM_PARAMETER_ERROR	(DDL_COMM_ERROR_BASE +  0)
#define DDL_COMM_CONBYPOLL_FAILED	(DDL_COMM_ERROR_BASE +  1)
#define DDL_COMM_DEVICE_NOT_AT_POLL	(DDL_COMM_ERROR_BASE +  2)
#define DDL_COMM_DISCONNECT_FAILED	(DDL_COMM_ERROR_BASE +  3)
#define DDL_COMM_SEARCH_FAILED		(DDL_COMM_ERROR_BASE +  4)
#define DDL_COMM_MEMORY_FAILURE		(DDL_COMM_ERROR_BASE +  5)
#define DDL_COMM_UTIL_FAILED		(DDL_COMM_ERROR_BASE +  6)
#define DDL_COMMUNICATIONS_FAILED	(DDL_COMM_ERROR_BASE +  7)
#define DDL_COMM_EXTRACTION_FAILED	(DDL_COMM_ERROR_BASE +  8)
#define DDL_COMM_NO_RESPONSE		(DDL_COMM_ERROR_BASE +  9)
#define DDL_COMM_PROGRAMMER_ERROR	(DDL_COMM_ERROR_BASE + 10)
#define DDL_COMM_QUEUE_FAILURE		(DDL_COMM_ERROR_BASE + 11)
#define DDL_COMM_SHUTDOWNINPROGRESS	(DDL_COMM_ERROR_BASE + 12)
#define DDL_COMM_CONBYTAG_FAILED	(DDL_COMM_ERROR_BASE + 13)  // timj added 4/6/04
#define DDL_COMM_UNKNOWN_UNIV_REV	(DDL_COMM_ERROR_BASE + 14)	// stevev added 12feb07

// TODO(LINUX_PORT, "Copied from HartTrans.h -- de-duplicate later.");
#define ST_OK                      0                    //SUCCESS
#define APP_ST_FALSE            (APP_ERROR_BASE +200)
#define APP_ST_TRUE             (APP_ERROR_BASE +201)
#define APP_ST_CHANGED          (APP_ERROR_BASE +202)
#define APP_ST_NOT_CHANGED      (APP_ERROR_BASE +203)
#define APP_ST_CANCELED         (APP_ERROR_BASE +204)   // User canceled
#define APP_ST_ABORTED          (APP_ERROR_BASE +205)
#define APP_ST_WORKING          (APP_ERROR_BASE +206)
#define APP_ER_FAILED           (APP_ERROR_BASE +207)   // COM worked,but result was not succesful
#define APP_ER_OLE_FAILED       (APP_ERROR_BASE +208)
#define APP_ER_OLE_NOT_FOUND    (APP_ERROR_BASE +209)   // COM error, method not excuted
#define APP_ER_TIMEOUT          (APP_ERROR_BASE +210)
#define APP_ER_CMD_FAILED       (APP_ERROR_BASE +211)
#define APP_ER_COMM_FAILURE     (APP_ERROR_BASE +212)   // Parity error, etc. from device
#define APP_ER_NORESPONSE       (APP_ERROR_BASE +213)   // Device did not respond
#define APP_ER_PARAMETER        (APP_ERROR_BASE +214)
#define APP_ER_BUF_FULL         (APP_ERROR_BASE +215)   // Buffer size was not big enough


typedef hCcommInfc BASE_OF_CommSimmInfc;

class CommSimmInfc : public BASE_OF_CommSimmInfc
{
protected:

	Indentity_t thisIdentity;

	bool m_hadConfigChanged;

	// How many we have skipped
	int m_skipCount;

	bool m_bMoreStatusSent;

	// Enabling all rolled into one
	commStatus_t m_CommStatus;

	BYTE hstAddr;

	// TODO(LINUX_PORT, "Turn this into a pluggable transport interface so that we can use the same communication interface with runtime switchable transport.");
	SdcSimmPlaybackTransport m_transport;

public:
	CommSimmInfc();
	virtual ~CommSimmInfc();

	// TODO(LINUX_PORT, This notifyAppVarChange interface is in hCcommInfc and implemented in CbaseComm on the Windows side.);
	virtual											// 'PUSH INTERFACE' no longer == 0 - 25jan06
		void  notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged = NO_change, long aTyp = 0);	
	
	virtual void notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);

	/*
		Gets an empty packet for generating a command 
		(gives caller ownership of the packet*)
	*/
	virtual hPkt* getMTPacket(bool pendOnMT=false);

	/*
		Returns the packet memory back to the interface 
		(this takes ownership of the packet*)
	*/
	virtual void putMTPacket(hPkt*);

	/*
		Puts a packet into the send queue  
		(takes ownership of the packet*)
	*/
	virtual RETURNCODE SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);

	/* 
		Puts a packet into the front of the send queue
		(takes ownership of the pkt*)
	*/
	virtual	RETURNCODE SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
			
	virtual RETURNCODE SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

	// How many are in the send queue ( 0 == empty )
	virtual int sendQsize(void);

	// How many will fit
	virtual int	sendQmax(void);

	virtual void clearQueue(void);

	// Setup connection (called after instantiation of device)
	virtual RETURNCODE initComm(void);

	virtual RETURNCODE shutdownComm(void);

	void disableAppCommRequests(void);

	void enableAppCommRequests(void);

	virtual bool appCommEnabled(void);

	virtual RETURNCODE GetIdentity(Indentity_t& retIdentity, BYTE pollAddr);

	// TODO(LINUX_PORT, "Do we need this interface? Not in base class and used by SDC625 UI comm infc.");
	virtual void enableComm(void);


	// TODO(LINUX_PORT, "Do we need this interface? See CdocCommInfc::setHostAddress(...));
	// nop in simulation
	virtual void setHostAddress(BYTE ha);
	

// Attributes
public:
	// TODO(LINUX_PORT, "Why is this public?");
	hCPktQueue thePktQueue;


// Implementation
protected:

	// format conversion
	RETURNCODE  pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt);
	RETURNCODE  pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen);

	// fills thisIdentity
	RETURNCODE  Array2Identity(int dataCnt, char* DataStr);

};

#endif // _XPLATFORM_LINUX_COMM_SIMM_INFC_H
