 
#include "linux_CommSimmInfc.h"

#include "linux/linux_util.h"

#include "registerwaits.h"
#include "logging.h"

#include "HARTsupport.h"


//#define DEBUG_TRANSACTION
//#define MSG_IS_BINARY


// TODO(LINUX_PORT, "This implementation exists in DDLBaseComm.cpp in the Windows portion of SDC625. Figure out what we want to do with this later. Also see related TODO in CommSimmInfc in this file about initializing base class members.");
hCcommInfc::hCcommInfc()
{
    cpRcvService=NULL;
    moreStatusRepression=50;                 

    // TODO(LINUX_PORT, "This is a file global in DDLBaseComm.cpp and used in CbaseComm::notifyAppVarChange. Figure out what we want to do with this later.");
#if 0
    pstActPktList.clear();
#endif

    inActionPkt=false; 
    MutexWaitTime=REASONABLE_TIME;
    LongMutexWaitTime = 2*REASONABLE_TIME;
    MessagePendTime = 3 * REASONABLE_TIME;

     /* stevev 23jan06 - try to stack the action packets */
    ActionPktEntryCnt = 0;  
    MethodPktEntryCnt=0;
    inMethodPkt=false; 
};

CommSimmInfc::CommSimmInfc():
	BASE_OF_CommSimmInfc(),
	thisIdentity(),
	m_hadConfigChanged(false),
	m_skipCount(0),
	m_bMoreStatusSent(false),
	m_CommStatus(commNC), // all of comm state rolled into one
	thePktQueue(), 
	hstAddr(2),
	m_transport(getEnvironmentVariable("SDC_PLAYBACK_FILE_PATH").c_str())
{
	// TODO(LINUX_PORT, "Add code to initialize base class public and protected member variables");

	thisIdentity.clear();
	thePktQueue.initialize(4);
}


CommSimmInfc::~CommSimmInfc()
{
	thePktQueue.destroy();
	LOGIT(CLOG_LOG, "CommSimmInfc delete called.\n");
}


void CommSimmInfc::notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged, long aTyp)
{ 
	// TODO(LINUX_PORT, "Implement this based on how we want to handle this.");
}


/*
	This is an asycronous callback from the dispatcher.
    The dispatcher handles the internal data acquasition and then calls this function.
*/
void  CommSimmInfc::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
	// TODO(LINUX_PORT, "Implement this based on how we want to handle this.");
}


/*
	Gets an empty packet for generating a command 
	(gives caller ownership of the packet*)
	pass back ownership with Sendxxx OR putMTPacket if command send aborted
*/
hPkt* CommSimmInfc::getMTPacket(bool pendOnMT)
{
	hPkt* pR = thePktQueue.GetEmptyObject(pendOnMT);
	//clog <<"got  MT   Pkt "<<hex << pR   << dec << endl;
	return pR;
}


/*
	Returns the packet memory back to the interface 
	(caller relinguishes ownership of the packet*)
*/						
void CommSimmInfc::putMTPacket(hPkt* pPkt)
{	
	//clog <<"return MT Pkt "<<hex << pPkt << dec << endl;
    thePktQueue.ReturnMTObject( pPkt );
}


// Puts a packet into the send queue (takes ownership of the packet*)
RETURNCODE CommSimmInfc::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = FAILURE;
	if ( pPkt == NULL)
	{
		cerr <<"ERROR: SendPkt has a null for a packet - discarding."<<endl;
		//return rc;
	}
	else
	{
		cmdStatus.transactionID = pPkt->transactionID;
		rc = SendCmd(pPkt->cmdNum,pPkt->theData,pPkt->dataCnt,timeout,cmdStatus);
	}
	putMTPacket(pPkt);
	return rc;
}


// Puts a packet into the send queue (takes ownership of the packet*)
RETURNCODE CommSimmInfc::SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	// does nothing for now
	return SendPkt(pPkt, timeout, cmdStatus);
}


/* The pipe interface will run in sycronous mode */
RETURNCODE CommSimmInfc::SendCmd(
	int cmdNum, 
	BYTE* pData, 
	BYTE& dataCnt, 
	int timeout, 
	cmdInfo_t& cmdStatus
)
{
	RETURNCODE rc = SUCCESS, trc;

	// pipe format:
	// CC_BB_DDDDDDDD
	// where CC is ascii hex value of the command number
	//   and BB is ascii hex value of the byte count
	//   and DD is ascii hex values for each data byte in bytecount
	//   and _  is an ascii space

	DWORD  cbRead;
	BOOL   isSuccess;	
	BYTE writeBuffer[PIPEBUFFERSIZE];
	BYTE read_Buffer[PIPEBUFFERSIZE];
	hPkt* pPkt = NULL;
	BYTE* pBuf = NULL;;
	BYTE  cntBuf;

	if (cmdNum < 0 || pData == NULL )
	{
		rc = APP_PARAMETER_ERR;
	}
	else
	{
		pPkt = getMTPacket();
		if ( pPkt == NULL )
		{
			cerr <<"ERROR: CdocCommInfc::SendCmd could NOT get an empty packet."<<endl;
			return APP_MEMORY_ERROR;
		}
		// else continue working

		// convert back to packet
		pPkt->cmdNum = cmdNum;
		pPkt->dataCnt= dataCnt;
		memcpy(&(pPkt->theData[0]),pData,dataCnt);

		DWORD cb ;

		trc = pkt2pipe(pPkt, writeBuffer, cb);// cb returns ascii length

		writeBuffer[cb] = '\0';
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Sending  |%s|\n",writeBuffer);  // was=>  << writeBuffer << "|"<<endl;
		cntBuf = (BYTE)(cb & 0xff);// max length is 255!

		if ( trc == SUCCESS )
		{				
			isSuccess = m_transport.transmit(
				std::string(
					reinterpret_cast<const char *>(writeBuffer), 
					static_cast<size_t>(cntBuf)
				)   
			);
			if (!isSuccess)
			{
				cerr << "ERROR: Transmit failed."<<endl;
				rc = APP_ER_FAILED;
			}
			else
			{
				rc = SUCCESS;
			}
		}
		else
		{
			rc = trc;
		}

		/*
		 * TODO(LINUX_PORT, "The call to systemSleep was inside a conditional on a "
		 *			"window handle being available. Since the window handle "
		 *			"was always available, I am assuming that we still want the "
		 *			"delay before we go on to receive a packet in the simulatino "
		 *			"interface");
		 */
		systemSleep(2);

	}// end else - params ok
	if ( rc == SUCCESS) //write went OK
	{	// we have apPkt

        std::string receivePacket;

		do 
		{ // Read from the pipe. 

			isSuccess = m_transport.receive(&receivePacket);
			cbRead = receivePacket.size() >= PIPEBUFFERSIZE ? 
				PIPEBUFFERSIZE - 1 : 
				receivePacket.size();
			memcpy(read_Buffer, receivePacket.c_str(), cbRead);

			if (!isSuccess)
			{
				cerr <<"ERROR: Receive failed."<<endl;
				cbRead = 0;
				rc = APP_ER_FAILED;
				break; 
			}
			else
			{
				//dataCnt = cbRead;
				pPkt->dataCnt = (BYTE)(cbRead & 0x00ff);// char count - max 255
				trc = pipe2pkt(read_Buffer, cbRead, pPkt);
				rc  = SUCCESS; // force an abort
			}
			read_Buffer[cbRead] = '\0'; // be sure it's terminated
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"RECEIVED |%s| message.\n",read_Buffer); // was =>  <<read_Buffer<<"| message."<<endl;

		} while (! isSuccess);  // repeat loop if ERROR_MORE_DATA 

		// parse the packet
		if (cpRcvService != NULL && trc == SUCCESS)
		{
			/*  stevev 01mar05 - prevent a cmd38 following a cmd 38 in simulation 
			the user can't change it fast enough */
			if (pPkt->cmdNum == 38)
			{
				pPkt->theData[1] &= 0xBF; // clr the cnfg chng bit
			}	
			pPkt->transactionID = cmdStatus.transactionID;
			cpRcvService->serviceReceivePacket(pPkt,&cmdStatus);
		}
		else
		if(cpRcvService == NULL)// from '!='  on 19may07 sjv
		{
		   cerr <<"ERROR: no service function to the receive packet."<<endl;
		   rc = APP_PROGRAMMER_ERROR;
		}
		else
		{
			rc = trc;// to return the packet parsing error
		}
	}
	// else - write failed, no reason to try a read...

	if ( pPkt != NULL )
	{
		putMTPacket(pPkt);
	}

	return rc;

}


// how many are in the send queue ( 0 == empty )
int CommSimmInfc::sendQsize(void)
{
	cerr << "CommSimmInfc::sendQsize needs work..." << endl;
	return 9999;
}

// how many will fit		
int	CommSimmInfc::sendQmax(void)
{
	cerr << "CommSimmInfc::sendQmax needs work..." << endl;
	return 2;
}


/*************************************************************************************************
* clearQueue is used where FlushQueue is needed but all threads pending on done-event must be
* released.
*  we have an issue with the msgCycle queue too.  There is one there for each of these.
*  we have an issue with all those variables left in the PENDING state too.
*
* >>>>caller should check m_CommStatus (for transition to ShuttingDown) when this function returns
*/
void CommSimmInfc::clearQueue(void)
{
    hPkt*           pSendPkt;

	while (thePktQueue.GetQueueState() > 0)// while not empty
	{
		pSendPkt = thePktQueue.GetNextObject(12);
		if ( pSendPkt != NULL)
		{
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Stop)
			{// de-init
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a Stop.\n");
				m_CommStatus = commShuttingDown;
			}
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_RawSnd) // identity interface
			{// ptr is event and we return data in the packet and the caller returns it
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a RawSend.\n");
				if (pSendPkt->m_thrdCntl.threadLPARAM != NULL)
				{
					hCevent::hCeventCntrl* pEc =
						((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
					if (pEc)
					{
						pEc->triggerEvent();
						delete pEc;
						delete ((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM));
						pSendPkt->m_thrdCntl.threadLPARAM = NULL;
					}// error - do nothing
				}// else no-op
			}// else we b done
			thePktQueue.ReturnMTObject( pSendPkt );
		}// else an error, skip it
	}//wend more packets left
    thePktQueue.FlushQueue();// just in case

	// TODO(LINUX_PORT, "This appears to be dealing with something in the UI. Figure out how this applies to us later.");
#if 0
	/**** Now deal with all those variables that are out there pending ***/
#ifndef _CONSOLE  // this will probably be required
    if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
	{
		CVarList* pVlist = (CVarList*)(m_pDoc->pCurDev->getListPtr(iT_Variable));
		if (pVlist != NULL)
		{
			for(CVarList::iterator VLIT = pVlist->begin(); VLIT != pVlist->end(); VLIT++)
			{// ptr 2a ptr 2a hCVar ---- for each device variable
				hCVar* pVar = (hCVar*)(*VLIT);
				if (pVar == NULL)
					continue;// should never happen
				INSTANCE_DATA_STATE_T tSt;
				tSt = pVar->getDataState();
				if ( tSt == IDS_PENDING )
				{
					pVar->markItemState(IDS_INVALID);// force a new read
				}// else, leave it alone
			}//next
		}//else list is unavailable
	}//we are in bas shape, just leave
#endif // _CONSOLE
#endif // if 0


	/**** and, finally, clear the msgQueue too ***/
	cpRcvService->serviceMsgCycleQue(0);// clear it all
}


// setup connection (called after instantiation of device)	
RETURNCODE CommSimmInfc::initComm(void)
{
	RETURNCODE rc = SUCCESS;

	// TODO(LINUX_PORT, "Implement this -- initialize transport.");

	return rc;
}

RETURNCODE CommSimmInfc::shutdownComm(void)
{
	RETURNCODE rc = SUCCESS;

	DEBUGLOG(CLOG_LOG,L"< doc-comm infc shutting down.\n");
	if (m_CommStatus > commNC )	// shuttingdown|shutdown  was:  ! m_enableComm)
	{// we're already shutdown, just leave
		DEBUGLOG(CLOG_LOG,L"< communications interface already gone (*)\n");
		return rc;
	}// else do the sequence

	// TODO(LINUX_PORT, "Shutdown transport.");

	thePktQueue.destroy();
	
	m_CommStatus = commShuttingDown;

	DEBUGLOG(CLOG_LOG,L"< communications interface is done. (19)\n");
	return rc;
}


void CommSimmInfc::disableAppCommRequests(void)
{
	// TODO(LINUX_PORT, "Implement this based on how we want to handle this.");
}


void CommSimmInfc::enableAppCommRequests(void)
{
	// TODO(LINUX_PORT, "Implement this based on how we want to handle this.");
}


bool CommSimmInfc::appCommEnabled (void)
{
	// TODO(LINUX_PORT, "Implement this based on how we want to handle this.");
	return true;
}


RETURNCODE CommSimmInfc::GetIdentity(Indentity_t& retIdentity, BYTE pollAddr)
{
	// TODO(LINUX_PORT, "Either add fake device identity or extract identity from recorded file.");

#if 1
	RETURNCODE rc = FAILURE;


    RETURNCODE status = ST_OK;
	DWORD lstErr = 0;
	TCHAR errMsg[128];


	DWORD  cbRead, dwMode;
	CHAR   chBuf[PIPEBUFFERSIZE];
	BOOL   isSuccess;	
 
	char message[] = "00 00"; // command zero with no data

    memset(chBuf,0,PIPEBUFFERSIZE);// prevent uninitialized memory warnings

	isSuccess = m_transport.transmit(
		std::string(
			reinterpret_cast<const char *>(message), 
			strlen(message)
		)   
	);


	if (! isSuccess) 
	{		

        // TODO(LINUX_PORT, "Handle failure");
	  cerr << "ERROR: Transmit failed.\n       "<<errMsg<<endl;
	  status = DDL_COMM_NO_RESPONSE;
	}
	else
	{ 
		/*
		 * TODO(LINUX_PORT, "The call to systemSleep was inside a conditional on a "
		 *			"window handle being available. Since the window handle "
		 *			"was always available, I am assuming that we still want the "
		 *			"delay before we go on to receive a packet in the simulatino "
		 *			"interface");
		 */
       systemSleep(1);

		std::string receivePacket;
	   do 
	   { // Read from the pipe. 

			isSuccess = m_transport.receive(&receivePacket);
			cbRead = receivePacket.size() >= PIPEBUFFERSIZE ? 
				PIPEBUFFERSIZE - 1 : 
				receivePacket.size();
			memcpy(chBuf, receivePacket.c_str(), cbRead);

		  if (! isSuccess ) 
		  {		
			  cerr <<"ERROR: Receive failed.\n       "<<endl;
			 break; 
		  }

		  // Reply from the pipe is written to STDOUT. 


		  chBuf[cbRead] = '\0'; // be sure it's terminated
		  LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Received |%s| Identity message. (%d bytes)\n",chBuf, cbRead);
								// was =>    <<chBuf<<"| message. ("<< cbRead << " bytes)"<<endl;
			
            hPkt locPkt;
#ifdef MSG_IS_BINARY
			//0 == 0, 1=cmdNumHi, 2=cmdNum, 3=Bcnt, 4 = status, 5 = respcd,
			locPkt.cmdNum = (chBuf[1]<<8 )|chBuf[2];
			locPkt.dataCnt= chBuf[3];
			memcpy(&(locPkt.theData[0]),&(chBuf[4]),cbRead-4);
#else // msg is ascii

			BYTE tmpDat[6];
			char* ptd = (char*) &(tmpDat[0]);
			/*BYTE PAW 03/03/09*/int tmp, cnt = 0, inS = 6;;// PAW 03/03/09 resolve stack errors with using a BYTE

#define	dat2tmp( x ) tmpDat[0] = chBuf[x];tmpDat[1] = chBuf[x+1];tmpDat[2] = '\0';\
					 sscanf(ptd, "%x" ,&tmp);

			dat2tmp( 0 );
			locPkt.cmdNum = tmp;
			dat2tmp( 3 );// skip space
			locPkt.dataCnt= tmp;

			while (cnt < locPkt.dataCnt)
			{
				dat2tmp( inS );
				locPkt.theData[cnt++] = tmp;
				inS+= 2;
			}			
#endif

	   } while (! isSuccess);  // repeat loop if ERROR_MORE_DATA 

	   rc = Array2Identity(cbRead, chBuf); // fills thisIdentity
	   if ( rc == SUCCESS )
	   {
		   //retIdentity = thisIdentity;
		   memcpy(&retIdentity, &thisIdentity, sizeof(Indentity_t));
	   }
	}// endif no errors so far


    return rc;
#endif
}


void CommSimmInfc::enableComm(void)
{
	m_CommStatus = commOK;
};


void CommSimmInfc::setHostAddress(BYTE ha)// nop in simulation
{
	hstAddr = ha;
}


// conversion routines
RETURNCODE CommSimmInfc::pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt)
{
	RETURNCODE rc = SUCCESS;
	BYTE* pB = pipeBuf;

	int  tmpInt = -1, c = 0, L;
	char shortStr[5] = {'\0','\0','\0','\0','\0'};

	if ( pipeBuf == NULL || pPkt == NULL || pipeLen < 5 )// 5 is cmd & 00 byte count
	{
		cerr <<"ERROR: parameter is null in parseStr2Msg."<<endl;
		rc = APP_PARAMETER_ERR;
	}
	else
	{// parameters OK
		memset(&(pPkt->theData[0]),0,255);

		if ( pB[pipeLen-1] == '\0' )
		{	 pipeLen--;				}

		//if ( pB[2] == ' ' )
		
		if ( pB[2] == ' ' )
		{	
			L = 2;	}
		else
		if ( pB[3] == ' ' )
		{	
			L = 3;	}
		else
		if ( pB[4] == ' ' )
		{	
			L = 4;	}
		else
		{	
			L = 0;	}
		if ( L > 0 )
		{ 
			shortStr[0] = pB[0]; shortStr[1] = pB[1];
			//c = sscanf(shortStr,"%02x",&tmpInt);
			if ( L > 2 )
			{
				shortStr[2] = pB[2];
				if ( L > 3 )
				{
					shortStr[3] = pB[3];
					c = sscanf(shortStr,"%04x",&tmpInt);
				}
				else
				{
					c = sscanf(shortStr,"%03x",&tmpInt);
				}
			}
			else
			{
				c = sscanf(shortStr,"%02x",&tmpInt);
			}
			if ( c == 1 )
			{
				pPkt->cmdNum = tmpInt;
			}
			else
			{
				pPkt->cmdNum = -1;
				cerr << "ERROR: scanf of |" << shortStr << "| failed with " <<
																	c << " scanned."<<endl;
				rc = APP_COMMAND_ERROR;
			}
			L++;
			pB = &(pB[L]);
			pipeLen -= L;
			if ( pipeLen > 2 )
			{
				if ( pB[2] != ' ' )
				{
					cerr << "ERROR: message buffer has incorrect delimiting. |"<<
																			pB<< "|"<<endl;
					rc = APP_COMMAND_ERROR;
				}
				// else fall through
			}
			else // it had to be 5 or we wouldn't get here
			{
				pipeLen++; // to match later(needs 6)
			}
			shortStr[0] = pB[0]; shortStr[1] = pB[1];

			if ( rc == SUCCESS )// we didn't have a commend read error
			{// continue
				sscanf(shortStr,"%02x",&tmpInt);
				pPkt->dataCnt = tmpInt;
				pB = &(pB[3]);
				pipeLen -= 3;
				if ( pipeLen != tmpInt * 2) // 2 ascii bytes per data byte
				{
					cerr << "ERROR: message buffer has incorrect size. BufSize="<<pipeLen<<
						 " ByteCnt="<< tmpInt << endl;
					rc = APP_COMMAND_ERROR;
				}
				else
				{// good length match, do the data
					int y,z;
					for (y = 0, z = 0; y < pPkt->dataCnt; y++, z+=2)
					{
						shortStr[0] = pB[z]; shortStr[1] = pB[z+1];
						sscanf(shortStr,"%02x",&tmpInt);
						pPkt->theData[y] = tmpInt;
					}
				}
			}
			// else just return the rc
		}
		else
		{
			cerr << "ERROR: message buffer has incorrect delimiting. |"<< pB<< "|"<<endl;
			rc = APP_COMMAND_ERROR;
		}
	}

	return rc;
}


RETURNCODE CommSimmInfc::pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen)
{
	RETURNCODE retVal = FAILURE;// deal with failure first
	int bufLoc = 0;
	int y, i, L;
	pipeLen = 0;

	if ( pipeBuf != NULL && pPkt != NULL)
	{
		L = y = sprintf((char*)pipeBuf, "%02x %02x ", pPkt->cmdNum, pPkt->dataCnt);// y == 6
		if ( pPkt->cmdNum < 256 && y != 6 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 6, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		if ( pPkt->cmdNum > 255 && pPkt->cmdNum < 4096 && y != 7 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 7, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		if ( pPkt->cmdNum > 4095 && pPkt->cmdNum < 65536 && y != 8 )
		{
			cerr << "ERROR: sprintf error in pkt2pipe. (expected 8, got "<< y <<")"<<endl;
			pipeBuf[0] = '\0';
			retVal     = APP_PARAMETER_ERR;
		}
		else
		{
			for ( i = 0; i < pPkt->dataCnt; i++ )
			{
				y += sprintf( (char*)(&(pipeBuf[y])),"%02x",pPkt->theData[i]);
			}
			if ( y != ((pPkt->dataCnt*2)+L) )
			{
				LOGIT(CERR_LOG,L"ERROR: sprintf error in pkt2pipe. (expected %d got %d)\n",
																((pPkt->dataCnt*2)+L), y );
				pipeBuf[0] = '\0';
				retVal = APP_PARAMETER_ERR;
			}
			else
			{// OK, send it				
				pipeLen = y;
				retVal = SUCCESS;
			}
		}
	}
	// else return failure

	return retVal;
}


RETURNCODE CommSimmInfc::Array2Identity(int dataCnt, char* DataStr)// fils thisIdentity
{
	RETURNCODE rc = FAILURE;

	// parse the data to an identity structure -- m_currIdentity
	thisIdentity.clear();

#ifdef MSG_IS_BINARY
#pragma message( "CommSimmInfc is Binary." )

	if (dataCnt < 18 || DataStr[0] != '\0' )  // probably an error packet
	{
		clog << "Identity |"<<hex<<DataStr<<dec<<"| message. ("<< dataCnt << " bytes)"<<endl;
		TRACE(_T("Identity |%s| message.\n"),DataStr);
		rc = APP_CMD_RESPCODE_ERR;
	}
	else//      5                                 6                        7
	{	// break it up
		//00 BC ST RC DATA
		//DATA	FE
		//		HH  Mfg						  HH   Mfg				HHHH   Expanded DevType
		//		HH	DevType					  HH   DevType			  
		//		HH	PreAmb					  HH   Reqst-PreAmb		  HH   Reqst-PreAmb
		//		HH  CmdRev-- 5				  HH   CmdRev-- 6		  HH   CmdRev-- 7
		//		HH	DevRev					  HH   DevRev			  HH   DevRev
		//		HH	SoftRev					  HH   SoftRev			  HH   SoftRev
		//		HH	top 5 bits - Hardwr Rev	  5b   Hdwr Rev			  5b   Hdwr Rev
		//			low 3 bits - Signal Code  3b   SignalCd			  3b   SignalCd
		//		HH	FuncFlags				  HH   FuncFlags		  HH   FuncFlags
		//		HHHHHH DeviceID				HHHHHH DeviceID			HHHHHH DeviceID
		//									  HH   Resp-PreAmbl		  HH   Resp-PreAmbl
		//									  HH   Num Dev-Vars		  HH   Num Dev-Vars
		//									HHHH   Config-Chng-Cnt	HHHH   Config-Chng-Cnt
		//									  HH   Extend-Dev Stat    HH   Extend-Dev Stat
		//															HHHH   Mfg Id Code
		int offset  = 4;
		BYTE st     = DataStr[4];
		BYTE respCd = DataStr[5];
		BYTE* pID   = (BYTE*) &(thisIdentity.dwDeviceId);

		if (DataStr[2+offset] != 254)
		{
			cerr << "ERROR: Zero cmd Constant is not in the right place."<<endl;
			rc = DDL_COMM_EXTRACTION_FAILED;
		}
		else
		if (DataStr[6+offset] == 5 || DataStr[6+offset] == 6)
		{	rc = SUCCESS;
			retIdentity.wManufacturer = DataStr[ 3+offset]; 
			retIdentity.wDeviceType   = DataStr[ 4+offset];  
			retIdentity.cUniversalRev = DataStr[ 6+offset];
			retIdentity.cDeviceRev    = DataStr[ 7+offset];
			retIdentity.cSoftwareRev  = DataStr[ 8+offset];
			retIdentity.cHardwareRev  = DataStr[ 9+offset];
			retIdentity.cZeroFlags    = DataStr[10+offset];
			retIdentity.cReqPreambles = DataStr[ 5+offset];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = DataStr[11+offset];
			pID[1] = DataStr[12+offset];
			pID[0] = DataStr[13+offset];

			if (DataStr[6+offset] == 6 )
			{	rc = SUCCESS;
				retIdentity.cRespPreambles= DataStr[14+offset];
				retIdentity.cMaxDevVars   = DataStr[15+offset];
				retIdentity.wCfgChngCnt   =(DataStr[16+offset]<<8)+DataStr[17+offset];
				retIdentity.cExtDevStatus = DataStr[18+offset];
			}
		}
		else
		if (DataStr[6+offset] == 7 )
		{	rc = SUCCESS;
			retIdentity.wDeviceType   =(DataStr[ 3+offset]<<8)+DataStr[ 4+offset];  
			retIdentity.cUniversalRev = DataStr[ 6+offset];
			retIdentity.cDeviceRev    = DataStr[ 7+offset];
			retIdentity.cSoftwareRev  = DataStr[ 8+offset];
			retIdentity.cHardwareRev  = DataStr[ 9+offset];
			retIdentity.cZeroFlags    = DataStr[10+offset];
			retIdentity.cReqPreambles = DataStr[ 5+offset];
			BYTE* pID   = (BYTE*) &(retIdentity.dwDeviceId);   
			pID[2] = DataStr[11+offset];
			pID[1] = DataStr[12+offset];
			pID[0] = DataStr[13+offset];
			retIdentity.cRespPreambles= DataStr[14+offset];
			retIdentity.cMaxDevVars   = DataStr[15+offset];
			retIdentity.wCfgChngCnt   =(DataStr[16+offset]<<8)+DataStr[17+offset];
			retIdentity.cExtDevStatus = DataStr[18+offset];
			retIdentity.wManufacturer =(DataStr[19+offset]<<8)+DataStr[20+offset]; 
		}
		else
		{// unknown universal rev
			rc = DDL_COMM_UNKNOWN_UNIV_REV;
		}
	}
#else /* message is ascii */

	if (dataCnt < 34 || DataStr[0] != '0' || DataStr[1] != '0')  // probably an error packet
	{
		clog << "Identity |"<< DataStr<<"| message. ("<< dataCnt << " bytes)"<<endl;
		LOGIT(CERR_LOG, _T("Identity |%s| message.\n"),DataStr);
		rc = APP_CMD_RESPCODE_ERR;
	}
	else
	{
		// break it up - ascii hex
		//00 BC STRCDATA
		//DATA	FE
		//		HH  Mfg
		//		HH	DevType
		//		HH	PreAmb
		//		HH  CmdRev
		//		HH	DevRev
		//		HH	SoftRev
		//		HH	top 5 bits - Hardware Revision
		//			low 3 bits - Signal Code
		//		HH	FuncFlags
		//		HHHHHH DeviceID
		BYTE tmpDat[6];
		char* ptd = (char*) &(tmpDat[0]);
		/*BYTE PAW 03/03/09*/ int bc, st, /*tmp,*/ htmp;	// using byte causes stack issues PAW
		/*BYTE PAW*/ int respCd;				// using byte causes stack issues PAW
		BYTE* pID   = (BYTE*) &(thisIdentity.dwDeviceId);
		unsigned tmp = 0;

		tmpDat[0] = DataStr[3];tmpDat[1] = DataStr[4];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&bc);

		tmpDat[0] = DataStr[6];tmpDat[1] = DataStr[7];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&st);

		tmpDat[0] = DataStr[8];tmpDat[1] = DataStr[9];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&respCd);

		tmpDat[0] = DataStr[10];tmpDat[1] = DataStr[11];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&tmp);

		if (tmp != 254)
		{
			cerr << "ERROR: Zero cmd Constant is not in the right place."<<endl;
			rc = APP_COMMAND_ERROR;
		}
		else
		{	rc = SUCCESS;
		}
		
		tmpDat[0] = DataStr[18];tmpDat[1] = DataStr[19];tmpDat[2] = '\0';
		sscanf(ptd, "%x" ,&tmp);
		thisIdentity.cUniversalRev = tmp;

		if (thisIdentity.cUniversalRev <= 6)// 5 & 6 & uninitialized xmtr-dd's 0x01
		{		
			tmpDat[0] = DataStr[12];tmpDat[1] = DataStr[13];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wManufacturer = tmp; 

			tmpDat[0] = DataStr[14];tmpDat[1] = DataStr[15];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wDeviceType   = tmp; 

			tmpDat[0] = DataStr[16];tmpDat[1] = DataStr[17];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp); 
			thisIdentity.cReqPreambles = tmp;

			tmpDat[0] = DataStr[20];tmpDat[1] = DataStr[21];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cDeviceRev    = tmp;

			tmpDat[0] = DataStr[22];tmpDat[1] = DataStr[23];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cSoftwareRev  = tmp;

			tmpDat[0] = DataStr[24];tmpDat[1] = DataStr[25];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cHardwareRev  = tmp;

			tmpDat[0] = DataStr[26];tmpDat[1] = DataStr[27];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cZeroFlags    = tmp;

			tmpDat[0] = DataStr[28];tmpDat[1] = DataStr[29];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[2] = tmp;
			
			tmpDat[0] = DataStr[30];tmpDat[1] = DataStr[31];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[1] = tmp;

			tmpDat[0] = DataStr[32];tmpDat[1] = DataStr[33];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[0] = tmp;
			
			if (thisIdentity.cUniversalRev == 6)
			{			
				tmpDat[0] = DataStr[34];tmpDat[1] = DataStr[35];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cRespPreambles= tmp; 

				tmpDat[0] = DataStr[36];tmpDat[1] = DataStr[37];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cMaxDevVars   = tmp; 

				tmpDat[0] = DataStr[38];tmpDat[1] = DataStr[39];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp); 
				tmpDat[0] = DataStr[40];tmpDat[1] = DataStr[41];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&htmp); 
				thisIdentity.wCfgChngCnt   = (tmp << 8) | htmp;


				tmpDat[0] = DataStr[42];tmpDat[1] = DataStr[43];tmpDat[2] = '\0';
				sscanf(ptd, "%x" ,&tmp);
				thisIdentity.cExtDevStatus = tmp;
			}
		}
		else
		if (thisIdentity.cUniversalRev == 7)
		{			
			tmpDat[0] = DataStr[12];tmpDat[1] = DataStr[13];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);		
			tmpDat[0] = DataStr[14];tmpDat[1] = DataStr[15];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&htmp);
			thisIdentity.wDeviceType   = ( tmp << 8 ) | htmp; 

			tmpDat[0] = DataStr[16];tmpDat[1] = DataStr[17];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp); 
			thisIdentity.cReqPreambles = tmp;

			tmpDat[0] = DataStr[20];tmpDat[1] = DataStr[21];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cDeviceRev    = tmp;

			tmpDat[0] = DataStr[22];tmpDat[1] = DataStr[23];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cSoftwareRev  = tmp;

			tmpDat[0] = DataStr[24];tmpDat[1] = DataStr[25];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cHardwareRev  = tmp;

			tmpDat[0] = DataStr[26];tmpDat[1] = DataStr[27];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cZeroFlags    = tmp;

			tmpDat[0] = DataStr[28];tmpDat[1] = DataStr[29];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);  
			pID[2] = tmp;
			
			tmpDat[0] = DataStr[30];tmpDat[1] = DataStr[31];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[1] = tmp;

			tmpDat[0] = DataStr[32];tmpDat[1] = DataStr[33];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			pID[0] = tmp;

			tmpDat[0] = DataStr[34];tmpDat[1] = DataStr[35];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);  
			thisIdentity.cRespPreambles = tmp;
			
			tmpDat[0] = DataStr[36];tmpDat[1] = DataStr[37];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cMaxDevVars = tmp;

			tmpDat[0] = DataStr[38];tmpDat[1] = DataStr[39];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);			
			tmpDat[0] = DataStr[40];tmpDat[1] = DataStr[41];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&htmp);
			thisIdentity.wCfgChngCnt = ( tmp << 8 ) | htmp;

			tmpDat[0] = DataStr[42];tmpDat[1] = DataStr[43];tmpDat[2] = '\0';
			sscanf(ptd, "%x" ,&tmp);
			thisIdentity.cExtDevStatus = tmp;

			tmpDat[0] = DataStr[44];tmpDat[1] = DataStr[45];tmpDat[2] = '\0';			
			tmpDat[2] = DataStr[46];tmpDat[3] = DataStr[47];tmpDat[4] = '\0';
			sscanf(ptd, "%x" ,&tmp);			
		//	tmpDat[0] = DataStr[46];tmpDat[1] = DataStr[47];tmpDat[2] = '\0';
		//	sscanf(ptd, "%x" ,&tmp);
			thisIdentity.wManufacturer = tmp;
		}
		else
		{// unknown universal rev
			rc = DDL_COMM_UNKNOWN_UNIV_REV;
			LOGIT(CERR_LOG|CLOG_LOG|UI_LOG|STAT_LOG,"Unknown Universal Revision 0x%02x\n",
																thisIdentity.cUniversalRev);
		}
	}
#endif

	return rc;
}
