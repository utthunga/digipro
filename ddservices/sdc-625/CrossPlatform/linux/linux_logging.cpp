#include <inttypes.h>
#include <cstdlib>
#include <ctime>
#include <map>
#include <string>

#include "Char.h"
#include "vccrtcompat.h"

#include "linux/linux_mutex.h"
#include "xplatform_logging.h"
#include "shared/shared_logging.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"


/*
    Return a 'global' lock for use with logging.

    Note: Wrapping this in a function insures against the static 
    initialization order issue (i.e., ensures construct-on-first-use). 
    
    See:
      * http://www.parashift.com/c++-faq/static-init-order.html
      * http://www.parashift.com/c++-faq/static-init-order-on-first-use.html

*/
Mutex& getGlobalLogMutex()
{
    // TODO(LINUX_PORT, "Probably not thread safe");
    static RecursiveMutex globalLogMutex;
    return globalLogMutex;
}


// TODO(LINUX_PORT, "Implement these");
void logExit(void){return;};
void captureStartTime(void){};
void logTime(void){};

#ifdef CPM_LOGS

// Log specified message to specified channels.
void log(const char* filename, const char* funcname, int line, int channel, const std::string& message)
{
    // Automatically unlocks on context-exit.
    MutexLocker lock(getGlobalLogMutex());

    string logMessage = "";
    logMessage.append(filename);
    logMessage.append(":");
    logMessage.append(to_string(line));
    logMessage.append(" - ");
    logMessage.append(funcname);
    logMessage.append(":");

    if (channel & CERR_LOG)
    {
        logMessage.append(message);
        LOG_ERROR(CPM_HART_SDC625, logMessage);
    }
    else if (channel & CLOG_LOG)
    {
        // clog is buffered (unlike cerr), but also goes to stderr)
        logMessage.append(message);
        LOG_INFO(CPM_HART_SDC625, logMessage);
    }
    else if (channel & COUT_LOG)
    {
        logMessage.append(message);
        LOG_DEBUG(CPM_HART_SDC625, logMessage);
    }
    else
    {
        if (channel & (UI_LOG | STAT_LOG | USRACK_LOG | TRACE_LOG | TOKERR))
        {        
            logMessage.append("Unsupported channel(s): 0x");
            LOG_TRACE(CPM_HART_SDC625, logMessage <<std::hex << channel);
        }
        else if (!(channel & (CERR_LOG | CLOG_LOG | COUT_LOG)))
        {
            logMessage.append("UNKNOWN CHANNEL: 0x");
            LOG_TRACE(CPM_HART_SDC625, logMessage <<std::hex << channel);
        }
    }
}

#else
// Log specified message to specified channels.
void log(const char* filename, const char* funcname, int line, int channel, const std::string& message)
{
    // Automatically unlocks on context-exit.
    MutexLocker lock(getGlobalLogMutex());

    std::time_t timeNow = time(NULL);

    /*
    
        Retrieve current time as string through ctime and strip out the last 
        character, which is a new-line, from the timestmap.
    */

    // Erase last character from ctime, which is a new-line.
    static std::string timeStamp = "";
    timeStamp = ctime(&timeNow);
    timeStamp.erase(timeStamp.size() - 1);

    std::ostream* pLogOutStream = NULL;
    if (channel & CERR_LOG)
    {
        pLogOutStream = &std::cout;
    }
    else if (channel & CLOG_LOG)
    {
        // clog is buffered (unlike cerr), but also goes to stderr)
        pLogOutStream = &std::clog;
    }
    else if (channel & COUT_LOG)
    {
        pLogOutStream = &std::cout;
    }
    else
    {
        if (channel & (UI_LOG | STAT_LOG | USRACK_LOG | TRACE_LOG | TOKERR))
        {
            std::cerr <<
                timeStamp << " - " <<
                __FILE__ << " - " << 
                __func__ << " - " << 
                __LINE__ << " - " << 
                "Unsupported channel(s): 0x" <<
                std::hex << channel << std::dec << 
                "; logging to CERR_LOG\n";
            pLogOutStream = &std::cerr;
        }
        else if (!(channel & (CERR_LOG | CLOG_LOG | COUT_LOG)))
        {
            std::cerr << 
                timeStamp << " - " <<
                __FILE__ << " - " << 
                __func__ << " - " << 
                __LINE__ << " - " << 
                "UNKNOWN CHANNEL: 0x" <<
                std::hex << channel << std::dec <<
                "; logging to CERR_LOG\n";
            pLogOutStream = &std::cerr;
        }
    }

    *pLogOutStream << 
        timeStamp << " - " <<
        filename << " - " << 
        funcname << " - " << 
        line << " - " << 
        message;
    if (pLogOutStream == &std::cerr)
    {
        pLogOutStream->flush();
    }
}
#endif //CPM_LOGS
// Log a standard string to the specified channels.
int logout(const char* filename, const char *funcname, int line, int channel, const char* format, ...)
{
    va_list vMarker;
    va_start(vMarker, format);
    
    char buffer[LOG_BUF_SIZE];
    int r = vsnprintf(buffer, LOG_BUF_SIZE, format, vMarker);
    log(filename, funcname, line, channel, buffer);

    va_end(vMarker);

    return r;
}

/*
    Log a wide string to the specified channels.

    Note: 
        The wide string is converted to a normal, UTF-8 string before 
        logging the output.

        Once a stream orientation is established for a particular stream,
        say stdout or a file, attempts to use a different stream orientation
        on that stream will fail. 
        
        See http://stackoverflow.com/a/7496243 for more.

    TODO(LINUX_PORT, "Improve error handling -- use info from both vswprintf and TStr2AStr to determine return value");
*/
int logout(const char* filename, const char *funcname, int line, int channel, const wchar_t* format, ...)
{

    va_list vMarker;
    va_start(vMarker, format);
    
    wchar_t buffer[LOG_BUF_SIZE];
    int r = vswprintf(buffer, LOG_BUF_SIZE, format, vMarker);
    std::string normalStr = TStr2AStr(buffer);

    log(filename, funcname, line, channel, normalStr);

    va_end(vMarker);

    return normalStr.size();
}

/*
    Log error information mapping to error number to the specified channels.

    Note: This code is mostly a straight lift from the corresponding 
    logout(...) and logoutV(...) implementations in COMMON/logging.cpp.
*/
int logout(const char* filename, const char *funcname, int line, int channel, int errNumber, ...)
{
    int r = 0;
    va_list vMarker;

    // Error number to error information mapping.
    static const errStruct_t errList[] = { ERR_DESC_LIST_00 ERR_DESC_LIST_END };

    va_start(vMarker, errNumber);

    int fndIdx = 0;
    for (int k = 0; errList[k].err_number != 0; k++)
    {
        if ( errList[k].err_number == errNumber )
        {
            fndIdx = k;
            break; // out of for loop
        }
    }


    if ( errList[fndIdx].err_number != 0 )
    {
        wstring tmpStr;
        if (errNumber >= WARNSTRT && errNumber < ERR_STRT)
        {
            tmpStr = L"WARNING ";
        }
        else 
        if (errNumber >= ERR_STRT)
        {
            tmpStr = L"ERROR ";
        }
        else
        {
            tmpStr = L"PROBLEM ";
        }       
        wchar_t tmpWar[32];
        _itow( errNumber, tmpWar, 10 );
        tmpStr += tmpWar;
        tmpStr += L": ";
        tmpStr += errList[fndIdx].err_str;

        r = logout(filename, funcname, line, channel, tmpStr.c_str() , vMarker);
    }
    else
    {
        r = logout(filename, funcname, line, channel, L"ERROR: Unknown error number.\n", vMarker);
    }
    
    va_end(vMarker);

    return r;
}


/*----------------------------------------------------------------------------
    Initialize loggers.

    This function scans the user's environment to determine which
    loggers permissions are to be enabled. The environmental strings are 
    of the form:

        SDC_LOG_PERMS=perm1,perml,...permN

    where:

     - permN = logger permission names (same as the LOGP_* permission macro 
       names).

    The following example enables logger permissions for DD info and 
    communication packet loggers.

        export SDC_LOG_PERMS=LOGP_DD_INFO;LOGP_COMM_PKTS

    A special logger permission name of "ALL" can be used to enable all
    logger permissions.

        export SDC_LOG_PERMS=ALL

    This can be useful when investigating a problem when you want to
    enable as much debugging output as possible.

    @note When SDC_LOG_PERMS is undefined or an empty string, then 
    TODO(LINUX_PORT, "Define defaults when no permissions are specified")


    Note: Permissions are stored to the 'logPer' global variable.

    Return: None.
*/

#define PERM_STR(perm) #perm

static uint32_t getLoggerPermissions()
{
    static uint32_t logPer = 0;

    static RecursiveMutex permissionInitMutex;
    static bool arePermissionsInitialized = false;

    /*
        Use Double-checked locking to insure thread safety
        http://en.wikipedia.org/wiki/Double-checked_locking
    */
    if (!arePermissionsInitialized)
    {
        // Automatically unlocks on context-exit.
        MutexLocker lock(permissionInitMutex);

        if (!arePermissionsInitialized)
        {
            const char* const SEP1 = ",;";

            std::map<std::string, uint32_t> permNameToPermMap;
            permNameToPermMap[PERM_STR(LOGP_DEPENDENCY)] = LOGP_DEPENDENCY;
            permNameToPermMap[PERM_STR(LOGP_RAW_DUMP)] = LOGP_RAW_DUMP;
            permNameToPermMap[PERM_STR(LOGP_START_STOP)] = LOGP_START_STOP;
            permNameToPermMap[PERM_STR(LOGP_COMM_PKTS)] = LOGP_COMM_PKTS;
            permNameToPermMap[PERM_STR(LOGP_MISC_CMTS)] = LOGP_MISC_CMTS;
            permNameToPermMap[PERM_STR(LOGP_DD_INFO)] = LOGP_DD_INFO;
            permNameToPermMap[PERM_STR(LOGP_NOTIFY)] = LOGP_NOTIFY;
            permNameToPermMap[PERM_STR(LOGP_TRANSFER)] = LOGP_TRANSFER;
            permNameToPermMap[PERM_STR(LOGP_WEIGHT)] = LOGP_WEIGHT;
            permNameToPermMap[PERM_STR(LOGP_MUTEX)] = LOGP_MUTEX;
            permNameToPermMap[PERM_STR(LOGP_NOT_TOK)] = LOGP_NOT_TOK;

            // Read environment for dynamic logger enable and levels.
            const char* const SDC_LOG_PERMS = "SDC_LOG_PERMS";
            const char* const PERMIT_ALL_LOGGERS = "ALL";
            char* pPerms = getenv(SDC_LOG_PERMS);


            // If at least one logger permission is specified.
            if (pPerms)
            {
                // Parse logger permission names
                fprintf(stdout, "\n%s=%s\n", SDC_LOG_PERMS, pPerms);
                char* pName = pPerms;
                while (true)
                {
                    // Get first or next logger permission name.
                    pName = strtok_r(pName, SEP1, &pPerms);

                    // Quit if no more logger permission names.
                    if (!pName)
                    {
                        break;
                    }

                    // If a specific logger permission is designated.
                    if (0 != strcmp(PERMIT_ALL_LOGGERS, pName))
                    {
                        if (permNameToPermMap.end() != 
                                permNameToPermMap.find(pName)
                        )
                        {
                            logPer |= permNameToPermMap[pName];
                        }
                        else
                        {
                            fprintf(
                                stderr, 
                                "\nUnrecognized permission: %s\n", 
                                pName
                            );
                        }
                    }
                    else
                    {
                        std::map<std::string, uint32_t>::iterator perm;
                        for (
                            perm = permNameToPermMap.begin();
                            perm != permNameToPermMap.end();
                            perm++
                        )
                        {
                            logPer |= (*perm).second;
                        }

                        break;
                    }


                    // Move to next pair.
                    pName = NULL;
                }
            }

            arePermissionsInitialized = true;
        }
    }

    return logPer;
}


// LOGP permission (logger enable/disable) bit mask.
bool isLogPermitted(uint32_t perm)
{
    return (0 != getLoggerPermissions());
}
