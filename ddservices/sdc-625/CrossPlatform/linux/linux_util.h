/*****************************************************************************
    Copyright (c) 2013 Fluke Corporation. All rights reserved.
******************************************************************************

    Authored By    - Dileepa Prabhakar
*/

/** @file
    Linux utility functions.

*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/
#ifndef _XPLATFORM_LINUX_UTIL_H
#define _XPLATFORM_LINUX_UTIL_H

/* INCLUDE FILES ************************************************************/

#include <string>


/* NON-MEMBER FUNCTION DECLARATIONS *****************************************/

/*-------------------------------------------------------------------------*/
/** Looks up the specified environment variable & returns its value as a 
    string.

    @param pEnvVar - Name of environment variable to look-up.

    @return Environment variable value when available, an empty string
        both when the environment variable is not available and when
        the variable does not have a value specified.
*/
std::string getEnvironmentVariable(char* pEnvVar);

/*-------------------------------------------------------------------------*/
/** Log time difference between specified end and start time with an 
    optional message.

    @param startTime - First time stamp.
    @param endTime - Second, later time stamp.
    @param message - Optional message; specify NULL to use default.

    @return None.
*/
void logTimeDiff(
    struct timeval startTime, 
    struct timeval endTime, 
    char *message
);
#endif // _XPLATFORM_LINUX_UTIL_H
