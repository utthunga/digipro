#include <locale>
#include <wctype.h>
#include <signal.h>
#include <time.h>

#include "vccrtcompat.h"

/*
    Convert int to string.
    
    Ref: 
        * http://www.daniweb.com/software-development/c/threads/148080/itoa-function-or-similar-in-linux
        * http://stackoverflow.com/a/12714086

    Note: 
        This implementation matches with the implementation in VC++
        by returning 2's complement representation for non-base 10
        negative numbers. See example in link below:
        http://msdn.microsoft.com/en-us/library/yakksftt.aspx
*/
char* _itoa(int value, char* str, int radix) 
{
    static char dig[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    int n = 0, neg = 0;
    unsigned int v;
    char* p, *q;
    char c;

    if (radix == 10 && value < 0) 
    {
        value = -value;
        neg = 1;
    }
    
    v = value;
    do 
    {
        str[n++] = dig[v%radix];
        v /= radix;
    } while (v);

    if (neg)
    {
        str[n++] = '-';
    }

    str[n] = '\0';
    
    // Reverse string to get actual representation.
    for (p = str, q = p + (n-1); p < q; ++p, --q)
    {
        c = *p, *p = *q, *q = c;
    }

    return str;
}


/*
    Convert unsigned long to string.
    Used in ddbFormat.cpp - hCformatBase::insertInt

    Ref: 
        * http://www.daniweb.com/software-development/c/threads/148080/itoa-function-or-similar-in-linux
        * http://stackoverflow.com/a/12714086
*/
char *_ultoa(unsigned long value, char *str, int radix)
{
    static char dig[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    int n = 0;
    unsigned long v;
    char* p, *q;
    char c;
    
    v = value;
    do 
    {
        str[n++] = dig[v%radix];
        v /= radix;
    } while (v);

    str[n] = '\0';

    // Reverse string to get actual representation.
    for (p = str, q = p + (n-1); p < q; ++p, --q)
    {
        c = *p, *p = *q, *q = c;
    }

    return str;
}


/*
    Convert int to wide-string.
    
    Used in MathBuiltins.cpp - CHart_Builtins::itoa.

    Ref: 
        * http://www.daniweb.com/software-development/c/threads/148080/itoa-function-or-similar-in-linux
        * http://stackoverflow.com/a/12714086

    Note: 
        This implementation matches with the implementation in VC++
        by returning 2's complement representation for non-base 10
        negative numbers. See example in link below:
        http://msdn.microsoft.com/en-us/library/yakksftt.aspx
*/
wchar_t* _itow(int value, wchar_t* str, int radix) 
{
    static wchar_t dig[] = L"0123456789abcdefghijklmnopqrstuvwxyz";
    int n = 0, neg = 0;
    unsigned int v;
    wchar_t* p, *q;
    wchar_t c;

    if (radix == 10 && value < 0) 
    {
        value = -value;
        neg = 1;
    }
    
    v = value;
    do 
    {
        str[n++] = dig[v%radix];
        v /= radix;
    } while (v);

    if (neg)
    {
        str[n++] = L'-';
    }

    str[n] = L'\0';
    
    // Reverse string to get actual representation.
    for (p = str, q = p + (n-1); p < q; ++p, --q)
    {
        c = *p, *p = *q, *q = c;
    }

    return str;
}


/* 
    Convert string to upper case.
    Copied from here:
    http://www.cplusplus.com/forum/general/21215/
*/
char* _strupr(char* str)
{
    char* p = str;
    while ((*p = toupper(*p))) p++;
    return str;
}


/* 
    Convert wide string to upper case.

    Used in StringBuiltins.cpp CHart_Builtins::STRUPR

    Note:
        Both '_wcsupr' VC++ documentation and the man page of 'towupper' in 
        Linux mention that the behavior is dependent on LC_CTYPE category 
        of the current locale.


    Refs:
        * http://stackoverflow.com/a/1614670
        * http://msdn.microsoft.com/en-us/library/hkxwh33z.aspx
        * ctype<wchar_t>::toupper
        * http://stackoverflow.com/questions/11491/string-to-lower-upper-in-c
*/
wchar_t *_wcsupr(wchar_t *str)
{
    wchar_t* p = str;
    while ((*p = towupper(*p))) p++;
    return str;
}


/* 
    Convert wide string to lower case.

    Used in StringBuiltins.cpp CHart_Builtins::STRLWR

    Note:
        Both '_wcslwr' VC++ documentation and the man page of 'towlower' in 
        Linux mention that the behavior is dependent on LC_CTYPE category 
        of the current locale.


    Refs:
        * http://stackoverflow.com/a/1614670
        * http://msdn.microsoft.com/en-us/library/sch3dy08.aspx
        * ctype<wchar_t>::tolower
        * http://stackoverflow.com/questions/11491/string-to-lower-upper-in-c
*/
wchar_t *_wcslwr(wchar_t * str)
{
    wchar_t* p = str;
    while ((*p = towlower(*p))) p++;
    return str;
}


/*
    Get tick count - time, in milliseconds, from an undefined point since 
    start-up.

    Ref:
      * http://msdn.microsoft.com/en-us/library/ms724408(v=VS.85).aspx
      * http://stackoverflow.com/questions/2958291/equivalent-to-gettickcount-on-linux
      * http://linux.die.net/man/3/clock_gettime
      * http://www.devx.com/tips/Tip/30141
*/
unsigned long GetTickCount()
{
    struct timespec ts;
    if(0 == clock_gettime(CLOCK_MONOTONIC,&ts))
    {
        /*
            Convert and return value in milliseconds to match Windows
            implementation.
        */
        return (ts.tv_sec * 1000) + (ts.tv_nsec / 1000);
    }
    else
    {
        return -1;
    }
}


/*
    Induce breakpoint when debugging.

    This is a built-in VC++ feature used in several places. There may not be 
    a Linux equivalent. Raise SIGINT instead.

    Ref:
        http://www.linuxquestions.org/questions/programming-9/how-to-programmatically-break-into-gdb-from-gcc-c-source-230854/

    Note: 
        Calls to this function are expected to be restricted to Debug builds.
*/
void _CrtDbgBreak()
{
#ifndef NDEBUG
    raise(SIGINT);
#endif // NDEBUG
}

