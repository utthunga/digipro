 /***********************************************************************************************
  *
  * $Workfile: DDLBaseComm.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
  **
  ***********************************************************************************************
  * The content of this file is the 
  *     Proprietary and Confidential property of the HART Communication Foundation
  * Copyright (c) 2006, HART Communication Foundation, All Rights Reserved 
  ***********************************************************************************************
  *
  * Description:
  *		DDLBaseComm.cpp : common code implementation file
  *	
  *		compile on the 'UI side' to get windows & documents 
  */

#ifndef __GNUC__	// L&T Modifications : LIN_CONS
    #include "stdafx.h"
#endif
   // #include "SDC625.h"
	#include "messageUI.h"
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
    #include "Mainfrm.h"
#else				// Linux treats the names Mainfrm and MainFrm differently.
   // #include "MainFrm.h"
#endif				// Hence in Linux add the exact file name.

    //#include "SDC625Doc.h"

    #include "DDLBaseComm.h"
    
    #include "logging.h"
    
    
    /*Vibhor 040204: End of Code*/
    
    #ifdef _DEBUG
    #define new DEBUG_NEW
    #undef THIS_FILE
    static char THIS_FILE[] = __FILE__;    
#define LOG_NOTIFY		/* turn it off here by undefing/commenting */
    #endif


	/** moved here from DDLCommInfc 25jan06 since the HartException is defined in our .h file **/    
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
    const TCHAR* HartException::except1020 = M_UI_UNABLE_2_INIT_COM;
    const TCHAR* HartException::except1021 = M_UI_NO_HART_UTIL;
    const TCHAR* HartException::except1022 = M_UI_NO_MODEM_CONN;
    const TCHAR* HartException::except1023 = M_UI_NO_HART_SERVER_CONN;
    const TCHAR* HartException::except1024 = M_UI_NO_HART_SELECT;
#else
	const TCHAR* HartException::except1020 = (TCHAR*)M_UI_UNABLE_2_INIT_COM;
    const TCHAR* HartException::except1021 = (TCHAR*)M_UI_NO_HART_UTIL;
    const TCHAR* HartException::except1022 = (TCHAR*)M_UI_NO_MODEM_CONN;
    const TCHAR* HartException::except1023 = (TCHAR*)M_UI_NO_HART_SERVER_CONN;
    const TCHAR* HartException::except1024 = (TCHAR*)M_UI_NO_HART_SELECT;
#endif

/* stevev 18nov05 - handle structure changes made static 31may11*/
	static itemIDlist_t pstActPktList;	// hold value pkts
	static itemIDlist_t pstActStrList; // hold structure  pkts

// debug state
static unsigned state = 0;
	

	hCcommInfc::hCcommInfc()
	{cpRcvService=NULL;moreStatusRepression=50;                 
	 pstActPktList.clear();
	 inActionPkt=false; 
	 MutexWaitTime=REASONABLE_TIME;
	LongMutexWaitTime = 2*REASONABLE_TIME;     
	MessagePendTime = 3 * REASONABLE_TIME;
	 /* stevev 23jan06 - try to stack the action packets */
	 ActionPktEntryCnt = 0;  MethodPktEntryCnt=0;inMethodPkt=false; };

int lastSize = 0;
#ifdef _DEBUG
void* pLast_MyPtr = NULL;
#endif

#ifdef __GNUC__			// L&T Modifications : LIN_CONS
CbaseComm::CbaseComm()	// If the implementation is provided in header file,
{						// compiler throws error : undefined reference to
	m_pending = false;	// CbaseComm:CbaseComm(). Hence provided the implementation
}						// here (for linux only).
#endif

/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc - required functions  hCcommInfc
// stevev 25jan06 - moved from DDLCommInfc.cpp so it could be used in all SDC functions
// The PUSH INTERFACE - see notes at end of "ddbCommInfc.h"
//  - Handles the holding of notifications during different method execution cycles
//virtual
void  CbaseComm::notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged, long aTyp )
{
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
    hCitemBase* pIB = NULL;
    hCVar*      pIV = NULL;
	itemIDlist_t::iterator itmIT;
	/* aType is unused at this  time.  The attribute type may need to be part of the message */
	UNUSED( aTyp );
	
//    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd(); // PAW 13/03/09 
//make generic    CMainFrame * pMainWnd = (CMainFrame *) AfxGetApp ()->GetMainWnd (); 
	CWnd * pMainWnd = AfxGetApp ()->GetMainWnd ();
	
	HWND hwndMainWindow   = pMainWnd->GetSafeHwnd();		// where to Post messages
// moved here temporarily
EnterCriticalSection(&BaseCommNotifySection);
// debug
if ( state == 9 ) assert(0);
state = 1;
	/***********************
	stevev 14apr10 - this part of the notification has been moved to the serviceReceivePacket
	to get the update/signalling/notify sequence right....
	if ((m_pDoc != NULL) && (m_pDoc->pCurDev != NULL) &&	// we have pointers to work with 
		 (SUCCESS == (m_pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB))) && 
		 (pIB != NULL)  )
	{// we need that part in a couple of places
		if ((isChanged == IS_changed)  && ( pIB->getIType() == iT_Variable) )
		{
			pIV = (hCVar*)pIB;
			pIV->CancelIt();
		}
	}
	****************************/

	/* deal with the copy-over first
	if ( (isChanged == IS_changed)  &&						/// value change 
		 (m_pDoc != NULL) && (m_pDoc->pCurDev != NULL) &&	/// we have pointers to work with
		 (SUCCESS == (m_pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB))) && // item
		 (pIB != NULL) && ( pIB->getIType() == iT_Variable)  ) // only variables are copied
	**/

	switch (isChanged)
    {
	case METH_changed:/* sjv 6jun07 new one to preclude the above copy, all else is the same */
    case IS_changed: 
    	{
			if (inActionPkt)
			{// stevev 23jan06 - added insertUnique
TRACE(_T("    NotifyAppVarChanged----(0x%x) CHANGED while in Action Packet\n"),Thread_GetCurrentThreadId());
// debug
state = 2;
				for ( itmIT = pstActPktList.begin(); itmIT != pstActPktList.end(); ++itmIT)
				{ if ( (*itmIT) == changedItemNumber) break;  }

				if ( itmIT == pstActPktList.end() )// not found
				{
    				pstActPktList.push_back(changedItemNumber);
					TRACE("ActionPkt stores 0x%04x and now has %d items\n",
													changedItemNumber,pstActPktList.size());
				}
			}
			else // not inActionPkt
			{				 
    			if (hwndMainWindow != NULL)
    			{
  				::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, changedItemNumber,NULL);
// debug
state = 3;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
					L"Notify: Value Changed; 0x%04x\n",changedItemNumber);
#ifndef XMTRDD
				g_chWriteEnable = FALSE;
#endif
  				}// else - nop
			}
    	} 
		break;
	case STR_changed:
    	{
			if (inActionPkt || inMethodPkt)
			{// stevev 23jan06 - added insertUnique
TRACE(_T("    NotifyAppVarChanged----(0x%x) CHANGED while in Action/Method Packet\n"),
Thread_GetCurrentThreadId());
// debug
state = 4;
				for ( itmIT = pstActStrList.begin(); itmIT != pstActStrList.end(); ++itmIT)
				{ if ( (*itmIT) == changedItemNumber) break;  }
				if ( itmIT == pstActStrList.end() )// not found
				{    				
    				pstActStrList.push_back(changedItemNumber);
					TRACE("ActionStr stores 0x%04x and now has %d items\n",
													changedItemNumber,pstActStrList.size());
				}
			}
			else // not in any packet
			{
    			if (hwndMainWindow != NULL)
    			{
  				::PostMessage(hwndMainWindow,WM_SDC_STRUCTURE_CHANGED,changedItemNumber, NULL);
// debug
state = 5;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
										L"Notify: Struct Changed: 0x%04x\n",changedItemNumber);
  				}	
			}
    	} 
		break;
	case STRT_actPkt:
    	{
			if (inActionPkt)
			{
// debug
state = 6;
				/* stevev 23jan06 - try to stack the action packets */
				ActionPktEntryCnt++;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
								L"   ActionPkt lockout re-entry(now %d)\n",ActionPktEntryCnt);
				TRACE("ActionPkt Start again..%d entries now\n",ActionPktEntryCnt);
			}
			else // not in actionpkt
			{
TRACE(_T("    NotifyAppVarChanged----(0x%x) START Action Packet\n"),Thread_GetCurrentThreadId());
// debug
state = 7;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"   ActionPkt lockout Entry\n",ActionPktEntryCnt);
				/* stevev 23jan06 - try to stack the action packets */
				ActionPktEntryCnt = 1;
				inActionPkt = TRUE; 
    			pstActPktList.clear();
				TRACE("ActionPkt Start clears ActPktList");
				if ( ! inMethodPkt)
				{
					pstActStrList.clear();;
					TRACE(" and clears ActStrList");
				}// else leave it be, method is using it
				TRACE(".\n");
			}
    	} 
		break;
	case END_actPkt:
    	{
TRACE(_T("    NotifyAppVarChanged----(0x%x)  END  Action Packet\n"),Thread_GetCurrentThreadId());
			if (inActionPkt)
			{/* stevev 23jan06 - try to stack the action packets */
// debug
state = 8;
				ActionPktEntryCnt--;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
								L"   ActionPkt lockout re-moval(now %d)\n",ActionPktEntryCnt);
				TRACE("ActionPkt End Message leaves %d entries\n",ActionPktEntryCnt);
				// Stevev - stop task switching during notify flush			
// move to larger scope temporarily
//				EnterCriticalSection(&BaseCommNotifySection);
TRACE(_T("    NotifyAppVarChanged----(0x%x) ENTER CRITICAL SECTION Action Packet\n"),Thread_GetCurrentThreadId()); 

				if ( ! ActionPktEntryCnt )// reached zero
				{
    				if (hwndMainWindow != NULL)
					{
// debug
state = 9;
						if ( ! inMethodPkt)
						{	// structure first - meth not using it
							for ( itmIT = pstActStrList.begin();
							     itmIT != pstActStrList.end();    ++itmIT)
							{
								::PostMessage(hwndMainWindow,WM_SDC_STRUCTURE_CHANGED,
																				(*itmIT),NULL);
								LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"Notify: Struct Changed: 0x%04x"
														   L"- Post action Packet\n",(*itmIT));
								TRACE("ActionPkt Notify Structure Change 0x%04x\n", (*itmIT));
							}
							pstActStrList.clear();
							TRACE("ActionStr cleared at End of actpkt\n");
						}// else, just leave it
						// then values
						itemID_t Itm = 0;
						int cntIdx = 0;// for debugging
						unsigned db= 0, da = 0;// for debugging 
						for (itmIT = pstActPktList.begin();
							itmIT != pstActPktList.end();
							++itmIT  )
						{// 26jul1-crashed here with a bad iterator value to be incremented
						 //       the ++itmIT detected it on first increment
						 // 18aug11 - still haven't figured it out..there's always a Windows
						 // message traced right before this crash (X46 i think).  There is no
						 // such message when there is a successful execution of this code.
#if defined(_DEBUG) && ( _MSC_VER < 1700)  // _MyPtr not in VS2012
pLast_MyPtr = (void*)itmIT._Myptr;
da = (unsigned)(*itmIT._Myptr);
// these are pushed as symbol numbers including 0xe00000001
if ( itmIT._Myptr > ((unsigned long*)0x90000000) || da > ((unsigned)0xf0000000))
{
	assert(0);
}

#define MYPTR_CHECK      if ((*itmIT._Myptr) != da)	assert(0);
#else
#define MYPTR_CHECK
#endif
							Itm = *itmIT;// dereference operator
MYPTR_CHECK
							if ( Itm != DEVICE_RESPONSECODE   &&
  								 Itm != DEVICE_COMM48_STATUS	&&
								 Itm != DEVICE_COMM_STATUS    )
							::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, Itm, NULL);
MYPTR_CHECK
							LOGIF(LOGP_NOTIFY) (CLOG_LOG,
							    L"Notify: Value Changed: 0x%04x - Post action Packet\n", Itm);
MYPTR_CHECK
							TRACE("ActionPkt Notify Value Change 0x%04x\n", Itm);
MYPTR_CHECK
							// else do not send the message
							cntIdx++;
#if defined(_DEBUG) && ( _MSC_VER < 1700)  // _MyPtr not in VS2012
// 26jul1-crashed with a bad iterator value to be incremented
//       the ++itmIT detected it on first increment
db = (unsigned)(*itmIT._Myptr);
// these are pushed as symbol numbers including 0xe00000001
if ( itmIT._Myptr > ((unsigned long*)0x90000000)  || db > ((unsigned)0xf0000000))
{
	assert(0);
}
#endif
						}
						TRACE("ActionPkt Posted at End of actpkt\n");
					}// else - no window to send to - just exit
					else
					{
						TRACE("NO MAIN WINDOW all notifications discarded.\n");
					}

// debug
state = 0xA;
    				inActionPkt = FALSE; // redundant
    				pstActPktList.clear();  
					TRACE("ActionPkt cleared at End of actpkt\n");
				}// else not zero yet, keep going	
				else
				{
					TRACE("Action Pkt Still has re-entries, go around again.\n");
				}
				
TRACE(_T("    NotifyAppVarChanged----(0x%x)  EXIT CRITICAL SECTION Action Packet\n"),Thread_GetCurrentThreadId()); 
// move to larger scope temporarily
//				LeaveCriticalSection(&BaseCommNotifySection); // Stevev - stop task switching
			}
			else // not in actionpkt
			{

// debug
state = 0xB;
    			LOGIT(CERR_LOG|CLOG_LOG,
									L"ERROR: action packet lockout termination w/o start.\n");
    			pstActPktList.clear();// leave the structure queue
				TRACE("ActionPkt cleared at End of actpkt while never Starting\n");
				/* stevev 23jan06 - try to stack the action packets */
				ActionPktEntryCnt = 0;
			}
    	} 
		break;
	case STRT_methPkt:
    	{
TRACE(_T("    NotifyAppVarChanged----(0x%x) START Method* Packet\n"),Thread_GetCurrentThreadId());
			if (inMethodPkt)
			{

// debug
state = 0xC;
				/* stevev 23jan06 - try to stack the action packets */
				MethodPktEntryCnt++;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
								L"   MethodPkt lockout re-entry(now %d)\n",MethodPktEntryCnt);
			}
			else // not in methodpkt
			{

// debug
state = 0xD;
				MethodPktEntryCnt = 1;
				inMethodPkt = TRUE; 

				if ( ! inActionPkt)
				{
					pstActStrList.clear(); 
					TRACE("ActionStr cleared at Start of methpkt\n");
				}// else leave it be, action is using it
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
								  L"   MethodPkt lockout entry (now %d)\n",MethodPktEntryCnt);
			}
    	} 
		break;
	case END_methPkt:
    	{
TRACE(_T("    NotifyAppVarChanged----(0x%x)  END  Method* Packet\n"),Thread_GetCurrentThreadId());
			if (inMethodPkt)
			{/* stevev 23jan06 - try to stack the action packets */
				MethodPktEntryCnt--;
				LOGIF(LOGP_NOTIFY)(CLOG_LOG,
								L"   MethodPkt lockout re-moval(now %d)\n",MethodPktEntryCnt);
				if ( ! MethodPktEntryCnt )// reached zero
				{

// debug
state = 0xE;
					// Stevev - stop task switching during notify flush			
					EnterCriticalSection(&BaseCommNotifySection); 
TRACE(_T("    NotifyAppVarChanged----(0x%x) ENTER CRITICAL SECTION Method* Packet\n"),Thread_GetCurrentThreadId());
    				if (hwndMainWindow != NULL)
					{
						if ( ! inActionPkt)
						{	// send structure - action not using it
							for ( itmIT = pstActStrList.begin(); 
								  itmIT != pstActStrList.end();                ++itmIT    )
							{
								::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED,
																			   (*itmIT), NULL);
								LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"Notify: Struct Changed: 0x%04x -"
 															L" Post method Packet\n",(*itmIT));
							}
							pstActStrList.clear();
						}// else, just leave it, action is using it
					}// else - no window to send to - just exit

    				inMethodPkt = FALSE; 
TRACE(_T("    NotifyAppVarChanged----(0x%x)  EXIT CRITICAL SECTION Method* Packet\n"),Thread_GetCurrentThreadId());
					// Stevev - stop task switching				
					LeaveCriticalSection(&BaseCommNotifySection); 
				}// else not zero yet, keep going
			}
			else // not in methodpkt
			{

// debug
state = 0xF;
    			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: method packet termination w/o start.\n");
				MethodPktEntryCnt = 0;
			}
    	} 
		break;
		
	case ABORT_dd:      /* stevev 11apr11 - UI has to kill dev obj so obj has to tell */
		{// no questions asked... just kill it		
TRACE(_T("    NotifyAppVarChanged----(0x%x)  Abort DD Notification\n"),Thread_GetCurrentThreadId());
// debug
state = 0x10;	
			::PostMessage(hwndMainWindow, WM_SDC_CLOSEDEVICE, (WPARAM)pIB, NULL);
			// but tell it its going to die
			if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
			{
				m_pDoc->pCurDev->setDeviceState( ds_Closing );
			}
		}
		break;
	case DEL_element:
		{// added notify item disappearance 23may11
			TRACE(_T("    NotifyAppVarChanged----(0x%x)  DELETE Element Notification\n"),
																		Thread_GetCurrentThreadId());
// debug
state = 0x11;	;	
			if (hwndMainWindow != NULL)
			{
			::PostMessage(hwndMainWindow, WM_SDC_ELEMENT_DELETED, changedItemNumber, NULL);
			LOGIF(LOGP_NOTIFY)(CLOG_LOG,"Notify: Element Deleted: 0x%04x\n",changedItemNumber);
			}// else - nop
		}
		break;
	case STAT_changed:	// J.U. State is changed 17.02.11 - used when offline
    case NO_change:  /* 18nov05 - no longer a legal input. */
	default:
    	{// nothing is done (the message is consumed)
// too many...TRACE(_T("    NotifyAppVarChanged----(0x%x)  NO CHANGE Notification\n"),Thread_GetCurrentThreadId());	
// debug
state = 0x12;	
		LOGIF(LOGP_NOTIFY)(CLOG_LOG,"No change call in Notify: 0x%04x\n",changedItemNumber);
    	} 
		break;
	}// endswitxh
// moved here temorarily
LeaveCriticalSection(&BaseCommNotifySection); // Stevev - stop task switching
#else
	// As this method is not required for the console application,
	// it is not implemented for linux.
#endif
}
