#include <errno.h>

#include "xplatform_thread.h"
#include "linux_thread.h"
#include "shared/shared_thread.h"

#include "logging.h"


static void * __WaitThreadFunction(void* pThreadParameter)
{
    /*
        Make the thread cancelable at any time and at not just p re-defined
        cancellation points.
    */
    pthread_setcanceltype(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    oneWait* pWait = (oneWait*)pThreadParameter;
    (*(pWait->WaitFunction))(pWait);

    return pThreadParameter;
}



THREAD_HANDLE_TYPE Thread_CreateThread(
    oneWait* pWrkWait,
    ULONG dwFlags,
    THREAD_ID_TYPE* pThreadId
)
{
    /*
        Create threads in detached state (i.e., not joinable).

        The use model in SDC625 is to start a thread and have it run 
        independently (detached) from the thread that started it -- 
        i.e., the calling thread does not wait on the new thread to 
        finish execution before doing something else.

        See:
          * http://www.cs.cf.ac.uk/Dave/C/node30.html
    */
    pthread_attr_t threadAttr;
    if (0 == pthread_attr_init(&threadAttr))
    {
        if (0 != pthread_attr_setdetachstate(
                &threadAttr,
                PTHREAD_CREATE_DETACHED
            )
        )
        {
            LOGIT(
                CERR_LOG,
                "Failed to create thread; detached state config errno: %d\n", 
                errno
            );
            return 0;
        }
    }
    else
    {
        LOGIT(
            CERR_LOG,
            "Failed to create thread; attribute initialization errno: %d\n", 
            errno
        );
        return 0;
    }


    /*
       Create and start the thread.
       All threads call our _WaitThreadFunction() function and that 
       in turn calls the wait handler function through the wait object
       that the thread parameter will be set up to point to.
    */
    THREAD_ID_TYPE handle = 0;
    int status = pthread_create(
        &handle, 
        &threadAttr,
        &__WaitThreadFunction, 
        pWrkWait
    );

    *pThreadId = handle;

    if (0 != status)
    {
        handle = 0;
        LOGIT(
            CERR_LOG,
            "Failed thread creation with status: %d; errno: %d",
            status,
            errno
        );
    }

    pthread_attr_destroy(&threadAttr);

    return static_cast<THREAD_HANDLE_TYPE>(handle);
}


void Thread_ExitThread(DWORD exitCode)
{
    // TODO(LINUX_PORT, "Figure out if we need to set up clean-up handlers vai pthread_cleanup_push);
    pthread_exit(&exitCode);
}

bool Thread_TerminateThread(
    THREAD_HANDLE_TYPE threadHandle,
    DWORD exitCode
)
{
    // TODO(LINUX_PORT, "Figure out if we need to set up clean-up handlers vai pthread_cleanup_push);
    //
    /*
        It looks like the return value from TerminateThread in Windows 
        actually indicates if the thread was terminated or not while 
        pthread_cancel only indicates if the cancellation request was 
        successfully queued.

        http://linux.die.net/man/3/pthread_cancel
        http://msdn.microsoft.com/en-us/library/ms686717(VS.85).aspx
    */
    return (0 == pthread_cancel(threadHandle));
}


void Thread_CloseHandle(THREAD_HANDLE_TYPE threadHandle)
{
    // Do nothing.
    return;
}

void Thread_HandleWaitSetUp(oneWait* pWait)
{
}

void Thread_HandleWaitCleanUp(oneWait* pWait)
{
}


THREAD_ID_TYPE Thread_GetCurrentThreadId()
{
    return pthread_self();
}

bool Thread_IsThreadSame(
    THREAD_ID_TYPE threadOneId,
    THREAD_ID_TYPE threadTwoId
)
{
    // pthread_equal returns a non-zero value if threads are the same.
    return (0 != pthread_equal(threadOneId, threadTwoId));
}

/*
    Get handle/ID to current thread.

    Ref:
      * http://msdn.microsoft.com/en-us/library/aa908745.aspx
      * http://www.ibm.com/developerworks/systems/library/es-MigratingWin32toLinux.html
      * http://www.kernel.org/doc/man-pages/online/pages/man3/pthread_self.3.html
*/
THREAD_HANDLE_TYPE Thread_GetCurrentThread()
{
    return pthread_self();
}

int Thread_GetThreadPriority(THREAD_HANDLE_TYPE threadHandle)
{
    // Get present scheduling policy and priority.
    struct sched_param sched;
    int policy;
    int priority = 0;
    if (0 == pthread_getschedparam(threadHandle, &policy, &sched))
    {
        priority = sched.sched_priority;
    }

    return priority;
}

void Thread_SetThreadPriority(
    THREAD_HANDLE_TYPE threadHandle, 
    int priorityValue
)
{
    /*
        Change thread priority/scheduling policy only on the ARM.

        We are typically not be able to change priority or scheduling policy on 
        on i386 for non-root users. There are workarounds, but aren't typically
        easy.

        See
         * "Privileges and resource limits" for sched_setscheduler(...)
           http://kernel.org/doc/man-pages/online/pages/man2/sched_setscheduler.2.html
         * http://stackoverflow.com/questions/3649281/how-to-increase-thread-priority-in-pthreads
         * http://stackoverflow.com/questions/2138756/windows-and-linux-thread-priority-equivalence


        TODO(LINUX_PORT, "Do we really need to change priority/policy? This 
            code is in here as a port of what SDC625 does on the Windows
            side.");
    */
#if defined(BUILD_ARCH_ARM)

    // Get present scheduling policy and priority.
    struct sched_param sched;
    int prevPolicy;
    int prevPriority = 0;
    if (0 == pthread_getschedparam(threadHandle, &prevPolicy, &sched))
    {
        prevPriority = sched.sched_priority;

        // Scheduler policy differs when priority is boosted.
        sched.sched_priority = priorityValue;
        int policy = (0 < priorityValue) ?
            SCHED_RR : SCHED_OTHER;

        // If priority or scheduling policy is changing.
        if (
            (sched.sched_priority != prevPriority) ||
            (policy != prevPolicy)
        )
        {
            if (0 != pthread_setschedparam(threadHandle, policy, &sched))
            {
                LOGIT(
                    CERR_LOG,
                    "Failed to set new scheduler parameters; errno: %d\n",
                    errno
                );
            }
        }
    }
    else
    {
        LOGIT(
            CERR_LOG,
            "Failed to retrieve existing scheduler parameters; errno: %d\n",
            errno
        );
    }
#endif // BUILD_ARCH_ARM
}

void Thread_SetThreadPriorityFromMask(
    THREAD_HANDLE_TYPE threadHandle, 
    ULONG dwFlagsPriorityMask
)
{
    int newPriority = 0;
    switch ((dwFlagsPriorityMask & WT_NEW_PRIORITY))
    {
    case WT_NORM_PLUS_ONE: 
        // 1 point below norm. 
        newPriority = THREAD_PRIORITY_ABOVE_NORMAL;
        break; 

    case WT_NORM_PLUS_TWO: 
        // 2 points below norm 
        newPriority = THREAD_PRIORITY_HIGHEST;
        break; 

    case WT_NORM_LESS_ONE: 
        // 3 points below norm 
        newPriority = THREAD_PRIORITY_LOWEST;
        break; 

    case WT_NORM_LESS_TWO: 
        // 4 points below norm 
        newPriority = THREAD_PRIORITY_IDLE;
        break; 

    default:
        newPriority = THREAD_PRIORITY_NORMAL;
        break;
    }

    Thread_SetThreadPriority(threadHandle,newPriority);
}

