/*****************************************************************************
    Copyright (c) 2013 Fluke Corporation, Inc. All rights reserved.
******************************************************************************

    Authored By    - Dileepa Prabhakar
*/

/** @file
    Transport to play back recorded SDC624 Windows simulation transactions.

*/

/*****************************************************************************
    Use of the software source code and warranty disclaimers are
    identified in the Software Agreement associated herewith.
*****************************************************************************/

/* INCLUDE FILES ************************************************************/

#include <cstring>
#include <cassert>

#include "SdcSimmPlaybackTransport.h"

#include "logging.h"


/* CLASS CONSTANT DEFINITIONS ***********************************************/

/*
    Transmit and receive packet headers in the record-playback file.

    Note: This is the same header used by the Windows simulation interface 
    to record transactions. If we ever get to the grand goal of sharing 
    core SDC625 communication interface functionality between Windows 
    and Linux (and that will require considerable refactoring), it would be
    wise to move this header definition to a common place.
*/
const char SdcSimmPlaybackTransport::TX_COMMAND_HEADER[] = "TX: ";
const char SdcSimmPlaybackTransport::RX_RESPONSE_HEADER[] = "RX: ";

// http://stackoverflow.com/a/6089413
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

/* CLASS MEMBER FUNCTION IMPLEMENTATIONS ************************************/

/*------------------------------------------------------------------------
    Construct a SdcSimmPlaybackTransport.
*/
SdcSimmPlaybackTransport::SdcSimmPlaybackTransport(
    const char *pPlaybackFilePath
):
    m_playbackFile()
{
    if (pPlaybackFilePath)
    {
        m_playbackFile.open(
            pPlaybackFilePath, 
            std::ios_base::in | std::ios_base::binary
        );
    }
}

/*------------------------------------------------------------------------
    Destruct a SdcSimmPlaybackTransport.
*/
SdcSimmPlaybackTransport::~SdcSimmPlaybackTransport()
{
    if (m_playbackFile.is_open())
    {
        m_playbackFile.close();
    }
}


/*------------------------------------------------------------------------
    Transmit command.
*/
bool SdcSimmPlaybackTransport::transmit(const std::string& command)
{
    assert(command.size());

    return findPlaybackCommand(command);
}

/*------------------------------------------------------------------------
    Receive response.
*/
bool SdcSimmPlaybackTransport::receive(std::string* pResponse)
{
    assert(NULL != pResponse);

    return getPlaybackResponse(pResponse);
}

/*------------------------------------------------------------------------
    Transmit command and wait and receive command-response.
*/
bool SdcSimmPlaybackTransport::getResponse(
    const std::string& command, 
    std::string* pResponse
)
{
    assert(command.size());
    assert(NULL != pResponse);

    if (transmit(command))
    {
        return receive(pResponse);
    }

    return false;
}

/*------------------------------------------------------------------------
    Find specified command in playback file.
*/
bool SdcSimmPlaybackTransport::findPlaybackCommand(
    const std::string& command
)
{
    if (!isPlaybackFileOkToRead())
    {
        return false;
    }

    std::streampos fsearchStartPos = m_playbackFile.tellg();
    std::string line;

    bool wasCommandFound = false;
    while (!wasCommandFound)
    {
        safeGetline(m_playbackFile, line);
        if (!m_playbackFile.fail() && line.size())
        {
            if (m_playbackFile.tellg() == fsearchStartPos)
            {
                LOGIT(
                    CERR_LOG, 
                    "Command %s not found in recording.\n",
                    command.c_str()
                );


                break;
            }
            else 
            {
                // Do we have the right header?
                bool isTxHeader = (0 == line.substr(
                        0, 
                        strlen(TX_COMMAND_HEADER)
                    ).compare(TX_COMMAND_HEADER)
                );

                if (isTxHeader)
                {
                    // Do we  have the right command?
                    wasCommandFound = (0 == line.substr(
                            strlen(TX_COMMAND_HEADER)
                        ).compare(command) 
                    );
                }
                // else { continue searching };
            }

        }
        else if (m_playbackFile.eof())
        {
            /*
                The fail bit may also be set because getline might have 
                attempted to read past EoF. Clear it here.
            */
            if (m_playbackFile.rdstate() & std::ifstream::failbit)
            {
                m_playbackFile.clear();
            }

            // Continue search from the beginning.
            m_playbackFile.seekg(0);

        }
        else
        {
            break;
        }
    }

    return wasCommandFound;
}

/*------------------------------------------------------------------------
    Get playback response.
*/
bool SdcSimmPlaybackTransport::getPlaybackResponse(std::string* pResponse)
{
    bool wasResponseFound = false;

    if (!isPlaybackFileOkToRead())
    {
        return false;
    }
    else if (m_playbackFile.eof())
    {
        LOGIT(
            CERR_LOG,
            "Unexpected end of file status on playback file." 
            " Was findPlaybackCommand successfully invoked?.\n"
        );

        return false;
    }
    else
    {
        std::string line;
        safeGetline(m_playbackFile, line);
        if (!m_playbackFile.fail() && !m_playbackFile.eof() && line.size())
        {
            wasResponseFound = (
                0 == line.substr(
                    0, 
                    strlen(RX_RESPONSE_HEADER)
                ).compare(RX_RESPONSE_HEADER)
            );

            // Found what we wanted.
            if (wasResponseFound)
            {
                *pResponse = line.substr(strlen(RX_RESPONSE_HEADER));
            }
            else 
            {
                LOGIT(
                    CERR_LOG,
                    "Unexpected header in response line: %s"
                    "; expected header: %s\n",
                    line.c_str(),
                    RX_RESPONSE_HEADER
                );
            }
        }
        else
        {
            LOGIT(
                CERR_LOG,
                "Response not available; Playback failure?: %d "
                "; Playback end-of-file?: %d\n",
                m_playbackFile.fail(),
                m_playbackFile.eof()
            );


            wasResponseFound = false;
        }
    }

    return wasResponseFound;
}


/*------------------------------------------------------------------------
    Is the playback file still alright to read?
*/
bool SdcSimmPlaybackTransport::isPlaybackFileOkToRead() const
{
    if (!m_playbackFile.is_open() || m_playbackFile.fail())
    {
        LOGIT(
            CERR_LOG,
            "Playback file failed to open during construction "
            "or an error occurred during a previous read.\n"
        );

        return false;
    }

    return true;
}

