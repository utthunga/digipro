#*****************************************************************************
#    Copyright (c) 2009 Fluke Corporation. All rights reserved.
#*****************************************************************************

"""
CrossPlatform sources build rules.
"""

#=============================================================================
# Imports
#=============================================================================

import os

# Import the construction environment
Import("env")


#=============================================================================
# Customize environment.
#=============================================================================

# Operate on a cloned environment (any modifications done to the cloned 
# environment will not affect the original environment)
env = env.Clone()
env.setUpDevServicesBuildConfig()

#=============================================================================
# Module name
# The module name is used for the following:
#   - As the name of the build directory for this module under 
#     the main build directory.
#   - As an identifier for names of targets for this module in the 
#     following format: arm.<moduleName>
# The module name should be unique across all ARM modules.
#
# IMPORTANT: Update 'moduleSources' with sources relative to 
# 'variantSourceRoot'. The source tree under 'variantSouceRoot' is replicated 
# under 'buildPath' when building sources listed in 'moduleSources'.

moduleName = 'libcrossplatform'


#=============================================================================
# Determine the build path for this module.
# E.g.: build/arm/i386/development/msl
buildPath = Dir(
    '#' + os.path.join(
        env['BUILD_BASE_PATH'].path, 
        env['BUILD_ARCH'],
        env['BUILD_TYPE'],
        moduleName 
    )
)

# Set build directory using SCons VariantDir(...)
variantSourceRoot = Dir('#')
env.VariantDir(buildPath, variantSourceRoot, duplicate = 0)

# Read and store this directory's path. (Have to use srcnode()'s path
# because of the variant_dir option. Will get the build directory
# if we access Dir('.').path)
sourcePath = Dir('#' + Dir('.').srcnode().path)

#=============================================================================
# SCons targets, target binary
sconsTargetName = moduleName

Help(""" %s : Build Cross Platform sources static library.\n""" % sconsTargetName)


# Name of the module binary.
moduleBinary = moduleName + '.a'

target = File(
    os.path.join(
        buildPath.abspath, moduleBinary
    )
)
# SCons command-line build alias for this target.
env.Alias(sconsTargetName, target)


#=============================================================================
# Include paths
# env.AppendUnique(CPPPATH = [])
# env.AppendUnique(LIBPATH = [])

#=============================================================================
# Preprocessor defines.
# env.AppendUnique(CPPDEFINES = [])

#=============================================================================
# Compiler flags
# env.AppendUnique(CFLAGS = [])

#=============================================================================
# Linker flags
# env.AppendUnique(LINKFLAGS = [])


#=============================================================================
# List libraries to link against 
# Specify only the library name, excluding the 'lib' prefix or the 
# '.so' suffix.
# env.AppendUnique(LIBS = [])


#=============================================================================
# Source files and sub SCons scripts
#=============================================================================

# List source files relative to the variantSourceRoot
moduleSources = Split("""
    linux/event_objects_posix.cpp
    linux/linux_CommSimmInfc.cpp
    linux/linux_mutex.cpp
    linux/linux_thread.cpp 
    linux/linux_logging.cpp 
    linux/linux_util.cpp
    linux/vccrtcompat.cpp
    linux/SdcSimmPlaybackTransport.cpp
""")

moduleSources = [os.path.join(buildPath.abspath, sourcePath.path, s) for s in moduleSources]

#=============================================================================
# Generate targets.
#=============================================================================

env.createStaticLibrary(
    target,
    sconsTargetName,
    moduleSources
)

# Uncomment to print cloned environment
# print env.Dump()

# Return target node
Return('target')

