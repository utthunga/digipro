#ifndef _XPLATFORM_LOGGING_H
#define _XPLATFORM_LOGGING_H

#include <stdarg.h>
#include <stdio.h>
#include <string.h> // stevev 14mar07 to get strncpy
#include <ostream>

#include "ErrorList.h"	/* logged errors                      */


/*********************************************************************************************/
/*  Logging channels:   use channel | channel... to log to multiple destinations
**********************************************************************************************/

#define CERR_LOG	1
#define CLOG_LOG	2
#define COUT_LOG	4
#define UI_LOG		8
#define STAT_LOG   16

#define USRACK_LOG	32	// user acknowledge (popup) - does not return till modal acknowledged
#define TRACE_LOG   64
#define TOKERR		128 /* if not in the tokenizer/ does nothing, else generates a Tok error */

/* future *
#define USRCCNLLOG	64	// user OK/CANCEL (popup) - returns which button was pressed
**********************************************************************************************/


/* PErmissions - see ln 293 in SDC625.cpp*/
#define LOGP_DEPENDENCY 0x00000001
#define LOGP_RAW_DUMP   0x00000002
#define LOGP_START_STOP 0x00000004
#define LOGP_COMM_PKTS  0x00000008

#define LOGP_MISC_CMTS  0x00000010
#define LOGP_DD_INFO	0x00000020	/* use for stuff like critical parameter dump */
#define LOGP_NOTIFY     0x00000040	/* used to track UI notification of value change */
#define LOGP_TRANSFER   0x00000080  /* for logging transfer operations (111 & 112) */

#define LOGP_WEIGHT		0x00000100  /* for logging all things having to do with cmd weighing*/
#define LOGP_MUTEX      0x00000200  /* mutex transaction logging */
#define LOGP_LAYOUT     0x00000400  /* extended info for menu layout, including pixel locs  */
#define LOGP_NOT_TOK    0x80000000  /* turn off messages in the tokenizer(this is set in !tok*/
									// set it to enable all those not-yet-numbered-errors



#define LOG_BUF_SIZE	6144	/* gotta be big for methods */
#define LOG_MAX_PATH     300    /* similar to windows MAX_PATH)*/

#define wSpace( m ) m, L" "
#define Space( m )  m, " "
		/* pre 31aug06 was::>( strncpy(spBuf,sStr, m ) )*/ /* returns ptr2 spBuf */
// LOGIT(COUT_LOG,"%*s",Space(indent));


/***** EXIT CLEANUP *****/
void logExit(void);

void captureStartTime(void);
void logTime(void);// is a LOGP_START_STOP  with a trailing \n


#if defined(__GNUC__)

#include "linux/linux_logging.h"

#else // !__GNUC__

#include "win32/win32_logging.h"

#endif // __GNUC__


#endif // _XPLATFORM_LOGGING_H
