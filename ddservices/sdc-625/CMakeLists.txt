#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    compiler options ######################################################
#############################################################################
# Disable warning into error for unknown pragmas: -Wno-error=unknown-pragmas
# Disable warning of unknown pragmas: -Wno-unknown-pragmas
set (CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wno-unknown-pragmas")

#############################################################################
#    macro definitions ######################################################
#############################################################################
add_definitions(-D_UNICODE)
add_definitions(-D_FULL_RULE_ENGINE)
add_definitions(-DIS_SDC)
add_definitions(-DXMTR)

#############################################################################
#    directories needed to be built as part of the project ##################
#############################################################################
add_subdirectory(APPS)
add_subdirectory(sdc625controller)
add_subdirectory(COMMON)
add_subdirectory(CrossPlatform)
add_subdirectory(CommonClasses/SoftFloat)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(sdc625 STATIC $<TARGET_OBJECTS:sdc625controller>
                          $<TARGET_OBJECTS:COMMON>
                          $<TARGET_OBJECTS:BuiltinLib>
                          $<TARGET_OBJECTS:linux>
                          $<TARGET_OBJECTS:DDParser>
                          $<TARGET_OBJECTS:DevServices>
                          $<TARGET_OBJECTS:Interpreter>
                          $<TARGET_OBJECTS:MEE>
                          $<TARGET_OBJECTS:MethodSupport>
                          $<TARGET_OBJECTS:ParserInfc>
                          $<TARGET_OBJECTS:SoftFloat>)
