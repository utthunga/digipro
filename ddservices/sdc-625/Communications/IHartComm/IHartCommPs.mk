# ------------------------------------------------------------------------
# SourceSafe Header
#
# $Workfile: makefile $
# $Archive: /SwRICode/HWin/IHartComm/makefile $
# $Revision: 4 $
# $Date: 9/10/99 1:45p $
# ------------------------------------------------------------------------

# ------------------------------------------------------------------------
# Copyright 1999, Southwest Research Institute.
# ------------------------------------------------------------------------

# ------------------------------------------------------------------------
# File Description
#
# NMAKE Make file for the IHartComm Interface.
# This file processes the IHartComm.idl file to produce the appropriate
# Header files, Type Library, and Proxy Stub.
# ------------------------------------------------------------------------

# -------------------
# RULES
# -------------------

.c.obj:
     cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL $<

# -------------------
# Targets
# -------------------

ALL: IHartCommPs.dll IHartComm.tlb

IHartComm.tlb: IHartComm.idl IHartCommPs.mk
     midl IHartComm.idl

IHartCommPs.dll: IHartComm.tlb dlldata.obj IHartComm_p.obj IHartComm_i.obj IHartCommPs.def 
     link /dll /out:IHartCommPs.dll /def:IHartCommPs.def /entry:DllMain \
          dlldata.obj IHartComm_p.obj IHartComm_i.obj \
          kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib
     copy IHartCommPS.dll ..\bin
     copy IHartComm_i.c ..\include
     copy IHartComm.h ..\include
     copy IHartComm.tlb ..\include
     copy IHartComm.idl ..\include
     copy IHartCommCP.h ..\include
     regsvr32 ..\bin\IHartCommPS.dll

clean:
     regsvr32 /u ..\bin\IHartCommPS.dll
     @del IHartCommPs.dll
     @del IHartCommPs.lib
     @del IHartCommPs.exp
     @del dlldata.obj
     @del IHartComm_p.obj
     @del IHartComm_i.obj
