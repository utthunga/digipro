/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Oct 06 11:53:33 2009
 */
/* Compiler settings for C:\Work\HART\SDC_HEAD_hcf006\HART Tools\src\HSelect\HSelect.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __HSelect_h__
#define __HSelect_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IHartSelect2_FWD_DEFINED__
#define __IHartSelect2_FWD_DEFINED__
typedef interface IHartSelect2 IHartSelect2;
#endif 	/* __IHartSelect2_FWD_DEFINED__ */


#ifndef __IHartSelect3_FWD_DEFINED__
#define __IHartSelect3_FWD_DEFINED__
typedef interface IHartSelect3 IHartSelect3;
#endif 	/* __IHartSelect3_FWD_DEFINED__ */


#ifndef __HartSelectDevice_FWD_DEFINED__
#define __HartSelectDevice_FWD_DEFINED__

#ifdef __cplusplus
typedef class HartSelectDevice HartSelectDevice;
#else
typedef struct HartSelectDevice HartSelectDevice;
#endif /* __cplusplus */

#endif 	/* __HartSelectDevice_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "ihartcomm.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IHartSelect2_INTERFACE_DEFINED__
#define __IHartSelect2_INTERFACE_DEFINED__

/* interface IHartSelect2 */
/* [unique][helpstring][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IHartSelect2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("127A41BB-9C0A-4002-979F-14F23E96F6F4")
    IHartSelect2 : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectTag( 
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out] */ BYTE __RPC_FAR *pcPollAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectOpcTag( 
            /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
            /* [in] */ BSTR strFilter,
            /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
            /* [retval][out] */ long __RPC_FAR *plStatus) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHartSelect2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IHartSelect2 __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IHartSelect2 __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IHartSelect2 __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectTag )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out] */ BYTE __RPC_FAR *pcPollAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectOpcTag )( 
            IHartSelect2 __RPC_FAR * This,
            /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
            /* [in] */ BSTR strFilter,
            /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        END_INTERFACE
    } IHartSelect2Vtbl;

    interface IHartSelect2
    {
        CONST_VTBL struct IHartSelect2Vtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHartSelect2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHartSelect2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHartSelect2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHartSelect2_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHartSelect2_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHartSelect2_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHartSelect2_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHartSelect2_SelectTag(This,pComm,strAccess,pcPollAddress,plStatus)	\
    (This)->lpVtbl -> SelectTag(This,pComm,strAccess,pcPollAddress,plStatus)

#define IHartSelect2_SelectOpcTag(This,pOpcIUnk,strFilter,pstrSelectedTag,plStatus)	\
    (This)->lpVtbl -> SelectOpcTag(This,pOpcIUnk,strFilter,pstrSelectedTag,plStatus)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartSelect2_SelectTag_Proxy( 
    IHartSelect2 __RPC_FAR * This,
    /* [in] */ IHartComm __RPC_FAR *pComm,
    /* [in] */ BSTR strAccess,
    /* [out] */ BYTE __RPC_FAR *pcPollAddress,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartSelect2_SelectTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartSelect2_SelectOpcTag_Proxy( 
    IHartSelect2 __RPC_FAR * This,
    /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
    /* [in] */ BSTR strFilter,
    /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartSelect2_SelectOpcTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHartSelect2_INTERFACE_DEFINED__ */


#ifndef __IHartSelect3_INTERFACE_DEFINED__
#define __IHartSelect3_INTERFACE_DEFINED__

/* interface IHartSelect3 */
/* [unique][helpstring][oleautomation][dual][uuid][object] */ 


EXTERN_C const IID IID_IHartSelect3;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7c84b09b-37dc-4c87-af0b-db056553bd06")
    IHartSelect3 : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectTag( 
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out] */ BYTE __RPC_FAR *pcPollAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectOpcTag( 
            /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
            /* [in] */ BSTR strFilter,
            /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
            /* [retval][out] */ long __RPC_FAR *plStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SelectTagRange( 
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out][in] */ BYTE __RPC_FAR *pcPollAddress,
            /* [out][in] */ BYTE __RPC_FAR *pcEndAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHartSelect3Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IHartSelect3 __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IHartSelect3 __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IHartSelect3 __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectTag )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out] */ BYTE __RPC_FAR *pcPollAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectOpcTag )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
            /* [in] */ BSTR strFilter,
            /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SelectTagRange )( 
            IHartSelect3 __RPC_FAR * This,
            /* [in] */ IHartComm __RPC_FAR *pComm,
            /* [in] */ BSTR strAccess,
            /* [out][in] */ BYTE __RPC_FAR *pcPollAddress,
            /* [out][in] */ BYTE __RPC_FAR *pcEndAddress,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        END_INTERFACE
    } IHartSelect3Vtbl;

    interface IHartSelect3
    {
        CONST_VTBL struct IHartSelect3Vtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHartSelect3_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHartSelect3_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHartSelect3_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHartSelect3_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHartSelect3_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHartSelect3_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHartSelect3_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHartSelect3_SelectTag(This,pComm,strAccess,pcPollAddress,plStatus)	\
    (This)->lpVtbl -> SelectTag(This,pComm,strAccess,pcPollAddress,plStatus)

#define IHartSelect3_SelectOpcTag(This,pOpcIUnk,strFilter,pstrSelectedTag,plStatus)	\
    (This)->lpVtbl -> SelectOpcTag(This,pOpcIUnk,strFilter,pstrSelectedTag,plStatus)

#define IHartSelect3_SelectTagRange(This,pComm,strAccess,pcPollAddress,pcEndAddress,plStatus)	\
    (This)->lpVtbl -> SelectTagRange(This,pComm,strAccess,pcPollAddress,pcEndAddress,plStatus)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartSelect3_SelectTag_Proxy( 
    IHartSelect3 __RPC_FAR * This,
    /* [in] */ IHartComm __RPC_FAR *pComm,
    /* [in] */ BSTR strAccess,
    /* [out] */ BYTE __RPC_FAR *pcPollAddress,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartSelect3_SelectTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartSelect3_SelectOpcTag_Proxy( 
    IHartSelect3 __RPC_FAR * This,
    /* [in] */ IUnknown __RPC_FAR *pOpcIUnk,
    /* [in] */ BSTR strFilter,
    /* [out] */ BSTR __RPC_FAR *pstrSelectedTag,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartSelect3_SelectOpcTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartSelect3_SelectTagRange_Proxy( 
    IHartSelect3 __RPC_FAR * This,
    /* [in] */ IHartComm __RPC_FAR *pComm,
    /* [in] */ BSTR strAccess,
    /* [out][in] */ BYTE __RPC_FAR *pcPollAddress,
    /* [out][in] */ BYTE __RPC_FAR *pcEndAddress,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartSelect3_SelectTagRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHartSelect3_INTERFACE_DEFINED__ */



#ifndef __HSelectLib_LIBRARY_DEFINED__
#define __HSelectLib_LIBRARY_DEFINED__

/* library HSelectLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_HSelectLib;

EXTERN_C const CLSID CLSID_HartSelectDevice;

#ifdef __cplusplus

class DECLSPEC_UUID("2D148E71-6752-435e-AE46-986C36A974DA")
HartSelectDevice;
#endif
#endif /* __HSelectLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
