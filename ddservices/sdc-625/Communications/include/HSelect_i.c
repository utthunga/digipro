/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Tue Oct 06 11:53:33 2009
 */
/* Compiler settings for C:\Work\HART\SDC_HEAD_hcf006\HART Tools\src\HSelect\HSelect.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IHartSelect2 = {0x127A41BB,0x9C0A,0x4002,{0x97,0x9F,0x14,0xF2,0x3E,0x96,0xF6,0xF4}};


const IID IID_IHartSelect3 = {0x7c84b09b,0x37dc,0x4c87,{0xaf,0x0b,0xdb,0x05,0x65,0x53,0xbd,0x06}};


const IID LIBID_HSelectLib = {0xD7F74801,0x4A45,0x4bff,{0x8F,0xC2,0x9D,0x07,0x31,0x8B,0xF0,0x2F}};


const CLSID CLSID_HartSelectDevice = {0x2D148E71,0x6752,0x435e,{0xAE,0x46,0x98,0x6C,0x36,0xA9,0x74,0xDA}};


#ifdef __cplusplus
}
#endif

