/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Jun 07 12:54:33 2002
 */
/* Compiler settings for D:\HART Server\Server\OPCServer\Src\HartOpc\HartServer.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __HartServer_h__
#define __HartServer_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __OPCServer_FWD_DEFINED__
#define __OPCServer_FWD_DEFINED__

#ifdef __cplusplus
typedef class OPCServer OPCServer;
#else
typedef struct OPCServer OPCServer;
#endif /* __cplusplus */

#endif 	/* __OPCServer_FWD_DEFINED__ */


#ifndef __HartComm_FWD_DEFINED__
#define __HartComm_FWD_DEFINED__

#ifdef __cplusplus
typedef class HartComm HartComm;
#else
typedef struct HartComm HartComm;
#endif /* __cplusplus */

#endif 	/* __HartComm_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "ihartcomm.h"
#include "opcda.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 


#ifndef __HartServerLib_LIBRARY_DEFINED__
#define __HartServerLib_LIBRARY_DEFINED__

/* library HartServerLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_HartServerLib;

EXTERN_C const CLSID CLSID_OPCServer;

#ifdef __cplusplus

class DECLSPEC_UUID("E31E5782-636C-11D2-B326-0000A0080AC8")
OPCServer;
#endif

EXTERN_C const CLSID CLSID_HartComm;

#ifdef __cplusplus

class DECLSPEC_UUID("9AF50C9C-D462-11D2-97A4-0000A001344B")
HartComm;
#endif
#endif /* __HartServerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
