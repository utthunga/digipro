/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Jul 26 08:32:47 2000
 */
/* Compiler settings for IHartComm.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "pvfc.h"
#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __IHartComm_h__
#define __IHartComm_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IHartComm_FWD_DEFINED__
#define __IHartComm_FWD_DEFINED__
typedef interface IHartComm IHartComm;
#endif 	/* __IHartComm_FWD_DEFINED__ */


#ifndef ___IHartCommEvents_FWD_DEFINED__
#define ___IHartCommEvents_FWD_DEFINED__
typedef interface _IHartCommEvents _IHartCommEvents;
#endif 	/* ___IHartCommEvents_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IHartComm_INTERFACE_DEFINED__
#define __IHartComm_INTERFACE_DEFINED__

/* interface IHartComm */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHartComm;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9AF50C9B-D462-11D2-97A4-0000A001344B")
    IHartComm : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsInstrumentAtTag( 
            /* [in] */ BSTR szwAccess,
            /* [in] */ BSTR szwTag,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent) PVFC( "IHartComm.1" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsInstrumentAtPollAddress( 
            /* [in] */ BSTR szwAccess,
            /* [in] */ BYTE cPollAddr,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent) PVFC( "IHartComm.2" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectByTag( 
            /* [in] */ BSTR szwAccess,
            /* [in] */ BSTR szwTag,
            /* [in] */ VARIANT_BOOL bExclusive,
            /* [retval][out] */ long __RPC_FAR *plHandle) PVFC( "IHartComm.3" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ConnectByPoll( 
            /* [in] */ BSTR szwAccess,
            /* [in] */ BYTE cPollAddr,
            /* [in] */ VARIANT_BOOL bExclusive,
            /* [retval][out] */ long __RPC_FAR *plHandle) PVFC( "IHartComm.4" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ long lHandle) PVFC( "IHartComm.5" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Send( 
            /* [in] */ long lHandle,
            /* [in] */ VARIANT varCommand,
            /* [out] */ VARIANT __RPC_FAR *pvarReply,
            /* [retval][out] */ long __RPC_FAR *plStatus) PVFC( "IHartComm.6" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AsyncSend( 
            /* [in] */ long lHandle,
            /* [in] */ VARIANT varCommand,
            /* [retval][out] */ long __RPC_FAR *plStatus) PVFC( "IHartComm.7" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetId( 
            /* [in] */ long lHandle,
            /* [out] */ short __RPC_FAR *pnMfid,
            /* [out] */ short __RPC_FAR *pnDevid,
            /* [out] */ short __RPC_FAR *pnRevision,
            /* [out] */ long __RPC_FAR *plUid,
            /* [retval][out] */ long __RPC_FAR *plStatus) PVFC( "IHartComm.8" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloneConnection( 
            /* [in] */ long lHandle,
            /* [retval][out] */ long __RPC_FAR *plNewHandle) PVFC( "IHartComm.9" );
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_nRetries( 
            /* [retval][out] */ short __RPC_FAR *pVal) PVFC( "IHartComm.10" );
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_nRetries( 
            /* [in] */ short newVal) PVFC( "IHartComm.11" );
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bPrimaryMaster( 
            /* [retval][out] */ BOOL __RPC_FAR *pVal) PVFC( "IHartComm.12" );
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bPrimaryMaster( 
            /* [in] */ BOOL newVal) PVFC( "IHartComm.13" );
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_bIgnoreBurst( 
            /* [retval][out] */ BOOL __RPC_FAR *pVal) PVFC( "IHartComm.14" );
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_bIgnoreBurst( 
            /* [in] */ BOOL newVal) PVFC( "IHartComm.15" );
        
    };
    
#else 	/* C style interface */

    typedef struct IHartCommVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IHartComm __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IHartComm __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IHartComm __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *IsInstrumentAtTag )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BSTR szwAccess,
            /* [in] */ BSTR szwTag,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *IsInstrumentAtPollAddress )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BSTR szwAccess,
            /* [in] */ BYTE cPollAddr,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectByTag )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BSTR szwAccess,
            /* [in] */ BSTR szwTag,
            /* [in] */ VARIANT_BOOL bExclusive,
            /* [retval][out] */ long __RPC_FAR *plHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectByPoll )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BSTR szwAccess,
            /* [in] */ BYTE cPollAddr,
            /* [in] */ VARIANT_BOOL bExclusive,
            /* [retval][out] */ long __RPC_FAR *plHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ long lHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Send )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ long lHandle,
            /* [in] */ VARIANT varCommand,
            /* [out] */ VARIANT __RPC_FAR *pvarReply,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AsyncSend )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ long lHandle,
            /* [in] */ VARIANT varCommand,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetId )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ long lHandle,
            /* [out] */ short __RPC_FAR *pnMfid,
            /* [out] */ short __RPC_FAR *pnDevid,
            /* [out] */ short __RPC_FAR *pnRevision,
            /* [out] */ long __RPC_FAR *plUid,
            /* [retval][out] */ long __RPC_FAR *plStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CloneConnection )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ long lHandle,
            /* [retval][out] */ long __RPC_FAR *plNewHandle);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_nRetries )( 
            IHartComm __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_nRetries )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_bPrimaryMaster )( 
            IHartComm __RPC_FAR * This,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_bPrimaryMaster )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_bIgnoreBurst )( 
            IHartComm __RPC_FAR * This,
            /* [retval][out] */ BOOL __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_bIgnoreBurst )( 
            IHartComm __RPC_FAR * This,
            /* [in] */ BOOL newVal);
        
        END_INTERFACE
    } IHartCommVtbl;

    interface IHartComm
    {
        CONST_VTBL struct IHartCommVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHartComm_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHartComm_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHartComm_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHartComm_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHartComm_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHartComm_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHartComm_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHartComm_IsInstrumentAtTag(This,szwAccess,szwTag,pbIsPresent)	\
    (This)->lpVtbl -> IsInstrumentAtTag(This,szwAccess,szwTag,pbIsPresent)

#define IHartComm_IsInstrumentAtPollAddress(This,szwAccess,cPollAddr,pbIsPresent)	\
    (This)->lpVtbl -> IsInstrumentAtPollAddress(This,szwAccess,cPollAddr,pbIsPresent)

#define IHartComm_ConnectByTag(This,szwAccess,szwTag,bExclusive,plHandle)	\
    (This)->lpVtbl -> ConnectByTag(This,szwAccess,szwTag,bExclusive,plHandle)

#define IHartComm_ConnectByPoll(This,szwAccess,cPollAddr,bExclusive,plHandle)	\
    (This)->lpVtbl -> ConnectByPoll(This,szwAccess,cPollAddr,bExclusive,plHandle)

#define IHartComm_Disconnect(This,lHandle)	\
    (This)->lpVtbl -> Disconnect(This,lHandle)

#define IHartComm_Send(This,lHandle,varCommand,pvarReply,plStatus)	\
    (This)->lpVtbl -> Send(This,lHandle,varCommand,pvarReply,plStatus)

#define IHartComm_AsyncSend(This,lHandle,varCommand,plStatus)	\
    (This)->lpVtbl -> AsyncSend(This,lHandle,varCommand,plStatus)

#define IHartComm_GetId(This,lHandle,pnMfid,pnDevid,pnRevision,plUid,plStatus)	\
    (This)->lpVtbl -> GetId(This,lHandle,pnMfid,pnDevid,pnRevision,plUid,plStatus)

#define IHartComm_CloneConnection(This,lHandle,plNewHandle)	\
    (This)->lpVtbl -> CloneConnection(This,lHandle,plNewHandle)

#define IHartComm_get_nRetries(This,pVal)	\
    (This)->lpVtbl -> get_nRetries(This,pVal)

#define IHartComm_put_nRetries(This,newVal)	\
    (This)->lpVtbl -> put_nRetries(This,newVal)

#define IHartComm_get_bPrimaryMaster(This,pVal)	\
    (This)->lpVtbl -> get_bPrimaryMaster(This,pVal)

#define IHartComm_put_bPrimaryMaster(This,newVal)	\
    (This)->lpVtbl -> put_bPrimaryMaster(This,newVal)

#define IHartComm_get_bIgnoreBurst(This,pVal)	\
    (This)->lpVtbl -> get_bIgnoreBurst(This,pVal)

#define IHartComm_put_bIgnoreBurst(This,newVal)	\
    (This)->lpVtbl -> put_bIgnoreBurst(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_IsInstrumentAtTag_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BSTR szwAccess,
    /* [in] */ BSTR szwTag,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent);


void __RPC_STUB IHartComm_IsInstrumentAtTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_IsInstrumentAtPollAddress_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BSTR szwAccess,
    /* [in] */ BYTE cPollAddr,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsPresent);


void __RPC_STUB IHartComm_IsInstrumentAtPollAddress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_ConnectByTag_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BSTR szwAccess,
    /* [in] */ BSTR szwTag,
    /* [in] */ VARIANT_BOOL bExclusive,
    /* [retval][out] */ long __RPC_FAR *plHandle);


void __RPC_STUB IHartComm_ConnectByTag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_ConnectByPoll_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BSTR szwAccess,
    /* [in] */ BYTE cPollAddr,
    /* [in] */ VARIANT_BOOL bExclusive,
    /* [retval][out] */ long __RPC_FAR *plHandle);


void __RPC_STUB IHartComm_ConnectByPoll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_Disconnect_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ long lHandle);


void __RPC_STUB IHartComm_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_Send_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ long lHandle,
    /* [in] */ VARIANT varCommand,
    /* [out] */ VARIANT __RPC_FAR *pvarReply,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartComm_Send_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_AsyncSend_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ long lHandle,
    /* [in] */ VARIANT varCommand,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartComm_AsyncSend_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_GetId_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ long lHandle,
    /* [out] */ short __RPC_FAR *pnMfid,
    /* [out] */ short __RPC_FAR *pnDevid,
    /* [out] */ short __RPC_FAR *pnRevision,
    /* [out] */ long __RPC_FAR *plUid,
    /* [retval][out] */ long __RPC_FAR *plStatus);


void __RPC_STUB IHartComm_GetId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartComm_CloneConnection_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ long lHandle,
    /* [retval][out] */ long __RPC_FAR *plNewHandle);


void __RPC_STUB IHartComm_CloneConnection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IHartComm_get_nRetries_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IHartComm_get_nRetries_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IHartComm_put_nRetries_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IHartComm_put_nRetries_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IHartComm_get_bPrimaryMaster_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IHartComm_get_bPrimaryMaster_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IHartComm_put_bPrimaryMaster_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IHartComm_put_bPrimaryMaster_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IHartComm_get_bIgnoreBurst_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [retval][out] */ BOOL __RPC_FAR *pVal);


void __RPC_STUB IHartComm_get_bIgnoreBurst_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IHartComm_put_bIgnoreBurst_Proxy( 
    IHartComm __RPC_FAR * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IHartComm_put_bIgnoreBurst_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHartComm_INTERFACE_DEFINED__ */



#ifndef __IHartCommLib_LIBRARY_DEFINED__
#define __IHartCommLib_LIBRARY_DEFINED__

/* library IHartCommLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_IHartCommLib;

#ifndef ___IHartCommEvents_DISPINTERFACE_DEFINED__
#define ___IHartCommEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IHartCommEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IHartCommEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("9AF50C9D-D462-11D2-97A4-0000A001344B")
    _IHartCommEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IHartCommEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IHartCommEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IHartCommEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IHartCommEvents __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            _IHartCommEvents __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            _IHartCommEvents __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            _IHartCommEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            _IHartCommEvents __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        END_INTERFACE
    } _IHartCommEventsVtbl;

    interface _IHartCommEvents
    {
        CONST_VTBL struct _IHartCommEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IHartCommEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IHartCommEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IHartCommEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IHartCommEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IHartCommEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IHartCommEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IHartCommEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IHartCommEvents_DISPINTERFACE_DEFINED__ */

#endif /* __IHartCommLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
