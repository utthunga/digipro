/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Jul 28 10:24:23 2000
 */
/* Compiler settings for E:\HCF\HART Tools\src\HModemSvr\HModemSvr.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID LIBID_HModemSvrLib = {0x3C5ED4E7,0x67E0,0x43E0,{0x9D,0xC4,0x6D,0xF9,0x95,0x74,0x8A,0x53}};


const CLSID CLSID_HartModemComm = {0x7AC9A031,0x27E4,0x4BF4,{0xAF,0xAC,0x76,0x5A,0xDA,0x37,0xAC,0xD2}};


#ifdef __cplusplus
}
#endif

