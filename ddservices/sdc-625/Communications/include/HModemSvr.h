/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Jul 28 10:24:23 2000
 */
/* Compiler settings for E:\HCF\HART Tools\src\HModemSvr\HModemSvr.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __HModemSvr_h__
#define __HModemSvr_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __HartModemComm_FWD_DEFINED__
#define __HartModemComm_FWD_DEFINED__

#ifdef __cplusplus
typedef class HartModemComm HartModemComm;
#else
typedef struct HartModemComm HartModemComm;
#endif /* __cplusplus */

#endif 	/* __HartModemComm_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "ihartcomm.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 


#ifndef __HModemSvrLib_LIBRARY_DEFINED__
#define __HModemSvrLib_LIBRARY_DEFINED__

/* library HModemSvrLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_HModemSvrLib;

EXTERN_C const CLSID CLSID_HartModemComm;

#ifdef __cplusplus

class DECLSPEC_UUID("7AC9A031-27E4-4BF4-AFAC-765ADA37ACD2")
HartModemComm;
#endif
#endif /* __HModemSvrLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
