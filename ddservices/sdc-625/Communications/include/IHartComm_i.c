/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Jul 26 08:32:47 2000
 */
/* Compiler settings for IHartComm.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IHartComm = {0x9AF50C9B,0xD462,0x11D2,{0x97,0xA4,0x00,0x00,0xA0,0x01,0x34,0x4B}};


const IID LIBID_IHartCommLib = {0xF4EDB651,0xBBB4,0x42e0,{0x8B,0x2B,0x43,0x40,0x69,0xBE,0x3C,0x07}};


const IID DIID__IHartCommEvents = {0x9AF50C9D,0xD462,0x11D2,{0x97,0xA4,0x00,0x00,0xA0,0x01,0x34,0x4B}};


#ifdef __cplusplus
}
#endif

