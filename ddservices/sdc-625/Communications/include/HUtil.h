/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Sat Jul 29 11:58:19 2000
 */
/* Compiler settings for E:\HCF\HART Tools\src\HUtil\HUtil.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "pvfc.h"
#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __HUtil_h__
#define __HUtil_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IHartUtilities_FWD_DEFINED__
#define __IHartUtilities_FWD_DEFINED__
typedef interface IHartUtilities IHartUtilities;
#endif 	/* __IHartUtilities_FWD_DEFINED__ */


#ifndef __HartUtilities_FWD_DEFINED__
#define __HartUtilities_FWD_DEFINED__

#ifdef __cplusplus
typedef class HartUtilities HartUtilities;
#else
typedef struct HartUtilities HartUtilities;
#endif /* __cplusplus */

#endif 	/* __HartUtilities_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "ihartcomm.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IHartUtilities_INTERFACE_DEFINED__
#define __IHartUtilities_INTERFACE_DEFINED__

/* interface IHartUtilities */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHartUtilities;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AE4CA416-50DF-11D3-BAD6-0090275D5324")
    IHartUtilities : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AllocateCommandSpace( 
            /* [out] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ BYTE cCommand,
            /* [in] */ BYTE cDataLength) PVFC( "IHartUtilities.1" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertByte( 
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ BYTE cData) PVFC( "IHartUtilities.2" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertWord( 
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nData) PVFC( "IHartUtilities.3" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertFloat( 
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ float rData) PVFC( "IHartUtilities.4" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Insert3ByteLong( 
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ long lData) PVFC( "IHartUtilities.5" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertPackedASCII( 
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nPackedBytes,
            /* [in] */ BSTR strText) PVFC( "IHartUtilities.6" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckReply( 
            /* [in] */ VARIANT varReply,
            /* [out] */ BYTE __RPC_FAR *pcCmd,
            /* [out] */ BYTE __RPC_FAR *pcDataCount,
            /* [out] */ BYTE __RPC_FAR *pHdwStatus,
            /* [retval][out] */ BYTE __RPC_FAR *pCmdStatus) PVFC( "IHartUtilities.7" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExtractByte( 
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ BYTE __RPC_FAR *pcData) PVFC( "IHartUtilities.8" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExtractWord( 
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ short __RPC_FAR *pnData) PVFC( "IHartUtilities.9" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExtractFloat( 
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [out] */ float __RPC_FAR *prData,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsNan) PVFC( "IHartUtilities.10" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Extract3ByteLong( 
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ long __RPC_FAR *plData) PVFC( "IHartUtilities.11" );
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExtractPackedASCII( 
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nPackedBytes,
            /* [retval][out] */ BSTR __RPC_FAR *pstrText) PVFC( "IHartUtilities.12" );
        
    };
    
#else 	/* C style interface */

    typedef struct IHartUtilitiesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IHartUtilities __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IHartUtilities __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IHartUtilities __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AllocateCommandSpace )( 
            IHartUtilities __RPC_FAR * This,
            /* [out] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ BYTE cCommand,
            /* [in] */ BYTE cDataLength);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertByte )( 
            IHartUtilities __RPC_FAR * This,
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ BYTE cData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertWord )( 
            IHartUtilities __RPC_FAR * This,
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertFloat )( 
            IHartUtilities __RPC_FAR * This,
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ float rData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Insert3ByteLong )( 
            IHartUtilities __RPC_FAR * This,
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ long lData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertPackedASCII )( 
            IHartUtilities __RPC_FAR * This,
            /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nPackedBytes,
            /* [in] */ BSTR strText);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CheckReply )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [out] */ BYTE __RPC_FAR *pcCmd,
            /* [out] */ BYTE __RPC_FAR *pcDataCount,
            /* [out] */ BYTE __RPC_FAR *pHdwStatus,
            /* [retval][out] */ BYTE __RPC_FAR *pCmdStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExtractByte )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ BYTE __RPC_FAR *pcData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExtractWord )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ short __RPC_FAR *pnData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExtractFloat )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [out] */ float __RPC_FAR *prData,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsNan);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Extract3ByteLong )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [retval][out] */ long __RPC_FAR *plData);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExtractPackedASCII )( 
            IHartUtilities __RPC_FAR * This,
            /* [in] */ VARIANT varReply,
            /* [in] */ short nStartDataByte,
            /* [in] */ short nPackedBytes,
            /* [retval][out] */ BSTR __RPC_FAR *pstrText);
        
        END_INTERFACE
    } IHartUtilitiesVtbl;

    interface IHartUtilities
    {
        CONST_VTBL struct IHartUtilitiesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHartUtilities_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHartUtilities_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHartUtilities_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHartUtilities_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHartUtilities_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHartUtilities_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHartUtilities_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHartUtilities_AllocateCommandSpace(This,pvarCommand,cCommand,cDataLength)	\
    (This)->lpVtbl -> AllocateCommandSpace(This,pvarCommand,cCommand,cDataLength)

#define IHartUtilities_InsertByte(This,pvarCommand,nStartDataByte,cData)	\
    (This)->lpVtbl -> InsertByte(This,pvarCommand,nStartDataByte,cData)

#define IHartUtilities_InsertWord(This,pvarCommand,nStartDataByte,nData)	\
    (This)->lpVtbl -> InsertWord(This,pvarCommand,nStartDataByte,nData)

#define IHartUtilities_InsertFloat(This,pvarCommand,nStartDataByte,rData)	\
    (This)->lpVtbl -> InsertFloat(This,pvarCommand,nStartDataByte,rData)

#define IHartUtilities_Insert3ByteLong(This,pvarCommand,nStartDataByte,lData)	\
    (This)->lpVtbl -> Insert3ByteLong(This,pvarCommand,nStartDataByte,lData)

#define IHartUtilities_InsertPackedASCII(This,pvarCommand,nStartDataByte,nPackedBytes,strText)	\
    (This)->lpVtbl -> InsertPackedASCII(This,pvarCommand,nStartDataByte,nPackedBytes,strText)

#define IHartUtilities_CheckReply(This,varReply,pcCmd,pcDataCount,pHdwStatus,pCmdStatus)	\
    (This)->lpVtbl -> CheckReply(This,varReply,pcCmd,pcDataCount,pHdwStatus,pCmdStatus)

#define IHartUtilities_ExtractByte(This,varReply,nStartDataByte,pcData)	\
    (This)->lpVtbl -> ExtractByte(This,varReply,nStartDataByte,pcData)

#define IHartUtilities_ExtractWord(This,varReply,nStartDataByte,pnData)	\
    (This)->lpVtbl -> ExtractWord(This,varReply,nStartDataByte,pnData)

#define IHartUtilities_ExtractFloat(This,varReply,nStartDataByte,prData,pbIsNan)	\
    (This)->lpVtbl -> ExtractFloat(This,varReply,nStartDataByte,prData,pbIsNan)

#define IHartUtilities_Extract3ByteLong(This,varReply,nStartDataByte,plData)	\
    (This)->lpVtbl -> Extract3ByteLong(This,varReply,nStartDataByte,plData)

#define IHartUtilities_ExtractPackedASCII(This,varReply,nStartDataByte,nPackedBytes,pstrText)	\
    (This)->lpVtbl -> ExtractPackedASCII(This,varReply,nStartDataByte,nPackedBytes,pstrText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_AllocateCommandSpace_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ BYTE cCommand,
    /* [in] */ BYTE cDataLength);


void __RPC_STUB IHartUtilities_AllocateCommandSpace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_InsertByte_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ short nStartDataByte,
    /* [in] */ BYTE cData);


void __RPC_STUB IHartUtilities_InsertByte_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_InsertWord_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ short nStartDataByte,
    /* [in] */ short nData);


void __RPC_STUB IHartUtilities_InsertWord_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_InsertFloat_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ short nStartDataByte,
    /* [in] */ float rData);


void __RPC_STUB IHartUtilities_InsertFloat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_Insert3ByteLong_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ short nStartDataByte,
    /* [in] */ long lData);


void __RPC_STUB IHartUtilities_Insert3ByteLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_InsertPackedASCII_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [out][in] */ VARIANT __RPC_FAR *pvarCommand,
    /* [in] */ short nStartDataByte,
    /* [in] */ short nPackedBytes,
    /* [in] */ BSTR strText);


void __RPC_STUB IHartUtilities_InsertPackedASCII_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_CheckReply_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [out] */ BYTE __RPC_FAR *pcCmd,
    /* [out] */ BYTE __RPC_FAR *pcDataCount,
    /* [out] */ BYTE __RPC_FAR *pHdwStatus,
    /* [retval][out] */ BYTE __RPC_FAR *pCmdStatus);


void __RPC_STUB IHartUtilities_CheckReply_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_ExtractByte_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [in] */ short nStartDataByte,
    /* [retval][out] */ BYTE __RPC_FAR *pcData);


void __RPC_STUB IHartUtilities_ExtractByte_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_ExtractWord_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [in] */ short nStartDataByte,
    /* [retval][out] */ short __RPC_FAR *pnData);


void __RPC_STUB IHartUtilities_ExtractWord_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_ExtractFloat_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [in] */ short nStartDataByte,
    /* [out] */ float __RPC_FAR *prData,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pbIsNan);


void __RPC_STUB IHartUtilities_ExtractFloat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_Extract3ByteLong_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [in] */ short nStartDataByte,
    /* [retval][out] */ long __RPC_FAR *plData);


void __RPC_STUB IHartUtilities_Extract3ByteLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHartUtilities_ExtractPackedASCII_Proxy( 
    IHartUtilities __RPC_FAR * This,
    /* [in] */ VARIANT varReply,
    /* [in] */ short nStartDataByte,
    /* [in] */ short nPackedBytes,
    /* [retval][out] */ BSTR __RPC_FAR *pstrText);


void __RPC_STUB IHartUtilities_ExtractPackedASCII_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHartUtilities_INTERFACE_DEFINED__ */



#ifndef __HUtilLib_LIBRARY_DEFINED__
#define __HUtilLib_LIBRARY_DEFINED__

/* library HUtilLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_HUtilLib;

EXTERN_C const CLSID CLSID_HartUtilities;

#ifdef __cplusplus

class DECLSPEC_UUID("AE4CA417-50DF-11D3-BAD6-0090275D5324")
HartUtilities;
#endif
#endif /* __HUtilLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
