/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Sat Jul 29 11:58:19 2000
 */
/* Compiler settings for E:\HCF\HART Tools\src\HUtil\HUtil.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IHartUtilities = {0xAE4CA416,0x50DF,0x11D3,{0xBA,0xD6,0x00,0x90,0x27,0x5D,0x53,0x24}};


const IID LIBID_HUtilLib = {0xAE4CA408,0x50DF,0x11D3,{0xBA,0xD6,0x00,0x90,0x27,0x5D,0x53,0x24}};


const CLSID CLSID_HartUtilities = {0xAE4CA417,0x50DF,0x11D3,{0xBA,0xD6,0x00,0x90,0x27,0x5D,0x53,0x24}};


#ifdef __cplusplus
}
#endif

