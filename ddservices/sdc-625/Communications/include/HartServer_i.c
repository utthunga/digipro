/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Jun 07 12:54:33 2002
 */
/* Compiler settings for D:\HART Server\Server\OPCServer\Src\HartOpc\HartServer.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID LIBID_HartServerLib = {0x9AF50C8F,0xD462,0x11D2,{0x97,0xA4,0x00,0x00,0xA0,0x01,0x34,0x4B}};


const CLSID CLSID_OPCServer = {0xE31E5782,0x636C,0x11D2,{0xB3,0x26,0x00,0x00,0xA0,0x08,0x0A,0xC8}};


const CLSID CLSID_HartComm = {0x9AF50C9C,0xD462,0x11D2,{0x97,0xA4,0x00,0x00,0xA0,0x01,0x34,0x4B}};


#ifdef __cplusplus
}
#endif

