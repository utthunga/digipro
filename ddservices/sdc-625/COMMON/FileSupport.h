/*************************************************************************************************
 *
 * $Workfile: FileSupport.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		FileSupport.h Supplies the OS interface to the device object
 *
 * #include "FileSupport.h"
 */


#ifndef _SDC_FILE_SUPPORT_H
#define _SDC_FILE_SUPPORT_H

#ifndef __GNUC__	// L&T Modifications : LIN_CONS
#include "stdafx.h"
#endif
#include "CmdLineInfo.h"
#include "ddbFileSupportInfc.h"
#include "logging.h"

// include in the .cpp ...#include "SDC625.h"

class CsdcFileSupport : public hCFileSupport
{
	cmdLineInfo_t   m_copyOfCmdLine;
	tstring			m_strFilePath;
	HANDLE			hFile; 
 
	uchar* pBigBuffer;
	int    BigBuffPoint;
protected:
	tstring getFileSupPath(void);
	tstring createFileName(Indentity_t* deviceID, itemID_t fileID);


public:	
#ifndef __GNUC__	//L&T Modifications : LIN_CONS
	CsdcFileSupport(cmdLineInfo_t& rCmd):m_copyOfCmdLine(rCmd),
						hFile(INVALID_HANDLE_VALUE),pBigBuffer(NULL),BigBuffPoint(0){};
#else
	// TODO : Equivalent has to be found.
#endif
	virtual
		~CsdcFileSupport();

/**** not currently used.... to be used later for DD directory reading ****/
	//function to be implemented in derived class that handles directory handling 
	RETURNCODE FindFile(string& pattern, StrVector_t& returnedFiles){ return FAILURE;};
	//function to be implemented in derived class that handles initializations 
	RETURNCODE OpenDDFile(Indentity_t* deviceID){ return FAILURE;};
/**** end not used ****/

	/** Note - only one file may opened at a time (DD or File) through this interface **/

	//function to be implemented in derived class that handles initializations 
	RETURNCODE OpenFileFile(Indentity_t* deviceID, itemID_t fileID, bool clearOnOpen=FALSE);

	//function to be implemented in derived class that handles cleanup 
	RETURNCODE CloseFile(void);/* whichever type is opened */

	//function to be implemented in derived class that actually reads the file info
	RETURNCODE ReadFile(uchar* buffer, int& buffLen, ulong location = 0);

	//function to be implemented in derived class that actually writes the file info
	RETURNCODE WriteFile(uchar* buffer, int buffLen);

};

#endif/*_SDC_FILE_SUPPORT_H*/
