/*************************************************************************************************
 *
 * $Workfile: DDselect.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 This is the DD Selection class for Xmtr-DD
 */

#include "stdafx.h"
#include "ddbGeneral.h"
#ifdef XMTRDD
#include "XmtrDD.h"
#else
#include "SDC625.h"
//#include "SDC625Doc.h"
#endif
#include "DDselect.h"


#include "ddb.h"
#include "ddbParserInfc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CDDselect dialog

/* stevev 10jul09 - return to original /Vibhor 080404: Added new parameter pDoc
CDDselect::CDDselect(CDDIdeDoc *pDoc,CWnd* pParent)
	: CDDselectSplitter(CDDselect::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDDselect)
	m_SelectedString = _T("");
	//}}AFX_DATA_INIT
/x*Vibhor 080404: Start of Code*x/	
	m_pDoc = pDoc;
/x*Vibhor 080404: End of Code*x/		
	pDDkey = NULL;
*****************************************************/
CDDselect::CDDselect(CWnd* pParent /*=NULL*/)
	: CDDselectSplitter(CDDselect::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDDselect)
	m_SelectedString = _T("");
	//}}AFX_DATA_INIT
	pDDkey = NULL;
	pStdDev = NULL;
}// end of stevev 10jul09 - return to original


void CDDselect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDDselect)
#ifdef XMTRDD
	DDX_Control(pDX, IDOK, m_ApplyButtonCtrl);
#endif /* apply not used for sdc */
	DDX_Control(pDX, IDC_TREE1, m_LeftTreeCtrl);
	DDX_Control(pDX, IDC_LIST1, m_RgtListCtrl);
	DDX_Text(pDX, IDC_STAT_SELECTED, m_SelectedString);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDDselect, CDDselectSplitter)
	//{{AFX_MSG_MAP(CDDselect)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkListItem)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//stevev 10jul09 blikeXmtr
void CDDselect::setResp(hCddbDevice *pStandardDev,DD_Key_t* returnedDDkey)
{
	pDDkey = returnedDDkey;	
#ifndef XMTRDD
	m_LeftTreeCtrl.DDKey = *pDDkey;
#else
	m_LeftTreeCtrl.pDDKey = pDDkey;
#endif
	pStdDev = pStandardDev;
}

/////////////////////////////////////////////////////////////////////////////
// CDDselect message handlers

BOOL CDDselect::OnInitDialog() 
{
	CDDselectSplitter::OnInitDialog();
	//stevev 10jul09 blikeXmtrDWORD dwDummyDDKey;
	//stevev 10jul09 blikeXmtrsetResp(&dwDummyDDKey);
	// Controls information                               minSize      HorizOffset, VertOffset, from  
	LPCTRLINFO pInfoLeft = new CTRLINFO( &m_LeftTreeCtrl, CSize(  50, 100 ),  2,     50, TOP );
	LPCTRLINFO pInfoRight= new CTRLINFO( &m_RgtListCtrl,  CSize( 100, 100 ),  2,     50, TOP );
	LPCTRLINFO pInfoDisp = new CTRLINFO( NULL       ,     CSize( 100,  40 ),  2,      2, TOP );

	m_LeftTreeCtrl.pStrPath = &m_SelectedString;

	// Initialize of the splitter dialog
	InitDialog( pInfoLeft, pInfoRight, pInfoDisp, 50 );	
	m_LeftTreeCtrl.setDlg((CDialog*)this);

	// Added by Deepak
	m_LeftTreeCtrl.setListCtrl (&m_RgtListCtrl);

// Example ///////////////////////////////////////////////////////////////////
// From here is code that fill the controls: don't depend from CSplitterDialog
	m_imgList.Create( IDB_SELECTBITMAP, 16, 3, RGB(192,192,192) );
	m_LeftTreeCtrl.SetImageList( &m_imgList, TVSIL_NORMAL );
	m_RgtListCtrl. SetImageList( &m_imgList, LVSIL_SMALL );
	
////////////////////// End Example /////////////////////////////////////////////
//class aCentry
//
//public:
//   int     number;
//   string  name;
//   string  childTypeName;
//   vector<class aCentry> children;
/*                  unselected image index, selected image index*/
#define IMAGES      BMI_UNSELECTEDFOLDER,  BMI_SELECTEDFOLDER	
#define BASEIMAGES  BMI_UNSELECTEDDOCUMENT,BMI_SELECTEDDOCUMENT
#define SIX_IMAGES  BMI_UNSELECTED_SIX,    BMI_SELECTED_SIX
#define EIGHTIMAGS  BMI_UNSELECTED_EIGHT,  BMI_SELECTED_EIGHT

	RETURNCODE arc = SUCCESS;
	aCentry* pRoot = NULL;

	HTREEITEM hRoot, mfgParent, devtypeParent, devrevParent; // ddrevParent-nobody down there
	CString str;
	vector<class aCentry>::iterator mfgIT, devtypeIT, devrevIT, ddrevIT; //stevev 10jul09 blikeXmtrDWORD 

	// we will put the numeric value into the lparam of the item
	// level is encoded into the third nibble
	// x000 root x100 mfg x200 devType x300 devrev x400 ddrev
	const UINT mymask = TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE | TVIF_TEXT;

	BeginWaitCursor();
	if( NULL != pStdDev)
	{
		arc =  allocDeviceList(pStdDev,&pRoot );
	}
	else
	{
		arc = FAILURE;
	}	
/*//stevev 10jul09 blikeXmtrd  Vibhor 080404: Start of Code*x/

	CDocument *pDoc = m_pDoc;
	
    hCddbDevice *stdTbl = (hCddbDevice*)(m_pDoc->pDevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE));


	arc =  allocDeviceListForSDC(stdTbl, &pRoot );
/xx*Vibhor 080404: End of Code - end of blike */	
	if (arc == SUCCESS && pRoot != NULL  )
	{// load the display
		//hRoot = m_LeftTreeCtrl.InsertItem( pRoot->name.c_str(), BMI_SELECTEDFOLDER, BMI_SELECTEDFOLDER );
		// stevev - WIDE2NARROW char interface
		CString lStr;lStr = pRoot->name.c_str();
		hRoot = m_LeftTreeCtrl.InsertItem(mymask, lStr, BMI_SELECTEDFOLDER, BMI_SELECTEDFOLDER, 
									  0,0, /* state, state-mask */
									  TLF_ISROOT | 0, /* lparam */
									  0, TVI_LAST  );
		m_LeftTreeCtrl.rootPath = pRoot->help.c_str();
		for (mfgIT = pRoot->children.begin(); mfgIT < pRoot->children.end(); mfgIT++)
		{// IT points to a aCentry ---- MFG
			
			str.Format(_T("%s (0x%02X)"),mfgIT->name.c_str(),mfgIT->number);   
			//mfgParent = m_LeftTreeCtrl.InsertItem( str, IMAGES, hRoot );

			//UINT nMask, LPCTSTR lpszItem, int nImage, int nSelectedImage, UINT nState, UINT nStateMask, LPARAM lParam, HTREEITEM hParent, HTREEITEM hInsertAfter );

			mfgParent = m_LeftTreeCtrl.InsertItem(mymask, str, IMAGES, 
									  0,0, /* state */
									  TLF_ISMFG | (mfgIT->number & 0xFFFF), 
									  hRoot, TVI_LAST  );

			for (devtypeIT = mfgIT->children.begin(); devtypeIT < mfgIT->children.end(); devtypeIT++)
			{// IT points to a aCentry ---- DEVTYPE
				/* i can't see this working for unicode
				char *ptrDev_type=NULL;
				char szDev_type[56]={0},buf[56]={0};

				// stevev - WIDE2NARROW char interface
				string locStr; locStr = TStr2AStr(devtypeIT->name);

				strcpy(szDev_type,locStr.c_str());//was>  devtypeIT->name.c_str());

				ptrDev_type=szDev_type;
				if(szDev_type[0] == '_')  //remove '_' character if the name of the device type is coming as _STT250
				{
					ptrDev_type++;
					strcpy(buf,ptrDev_type);
					str.Format(_T("%s (0x%02X)"),buf,devtypeIT->number);
				}
				else
				{
					str.Format(_T("%s (0x%02X)"),devtypeIT->name.c_str(),devtypeIT->number);   
				}
				*** end can't see this working ***/
				wstring locName = devtypeIT->name; // we're about to muck with it...
				if ( locName[0] == L'_' ) locName.erase(0,1);
				str.Format(_T("%s (0x%02X)"),locName.c_str(),devtypeIT->number); 
		
				//devtypeParent = m_LeftTreeCtrl.InsertItem( str, IMAGES, mfgParent );
				devtypeParent = m_LeftTreeCtrl.InsertItem(mymask, str, IMAGES, 
									  0,0, /* state */
									  TLF_ISDEVTYPE | (devtypeIT->number & 0xFFFF), 
									  mfgParent, TVI_LAST  );

				for (devrevIT = devtypeIT->children.begin(); devrevIT < devtypeIT->children.end(); devrevIT++)
				{// IT points to a aCentry ---- DEVTYPE	
/*  stevev 10jul09 - this is an excercise in futility..............		
					char szDev_rev[32]={0};
//					str.Format("0x%02X", devrevIT->number);   
					BYTE byDevRev = 0;
					byDevRev = devrevIT->number & 0xFF;
					sprintf(szDev_rev,"Dev Rev %02d",byDevRev);
 end futility ......................................................*/
 					int y = 0;

 					str.Format(_T("0x%02X"), devrevIT->number);
 					y = 0x00ff;
 
					//devrevParent = m_LeftTreeCtrl.InsertItem( str, IMAGES, devtypeParent );
					
					devrevParent = m_LeftTreeCtrl.InsertItem(mymask, str, IMAGES,
								  0,0, // state 
								  TLF_ISDEVREV | (devrevIT->number & y), 
								  devtypeParent, TVI_LAST  );

									  
// in the future these should go on the right hand list window
// in the interest of speed, we'll put 'em here for now
					for (ddrevIT = devrevIT->children.begin(); ddrevIT < devrevIT->children.end(); ddrevIT++)
					{// IT points to a aCentry ---- DEVTYPE

						str.Format(_T("0x%02X"), (ddrevIT->number & 0xFF));   
/* stevev 25jun09 - add file type in the icon
						//m_LeftTreeCtrl.InsertItem( str, IMAGES, devrevParent );
						m_LeftTreeCtrl.InsertItem(mymask, str, BASEIMAGES, 
									  0,0, // state 
									  TLF_ISDDREV | (ddrevIT->number & 0x0fff), // added the file type 0x00FF), 
									  devrevParent, TVI_LAST  );
 ** substitute with below **/
						
						int unsel, sel, ver;

						if ( (ddrevIT->number >> 8) == 6 ) // .fm6
						{	unsel = BMI_UNSELECTED_SIX;			sel = BMI_SELECTED_SIX;		ver = BFF_ISV_6;
						}
						else
						if ( (ddrevIT->number >> 8) == 8 ) // .fm8
						{	unsel = BMI_UNSELECTED_EIGHT;		sel = BMI_SELECTED_EIGHT;	ver = BFF_ISV_8;
						}
						else // .fms and unknown
						{	unsel = BMI_UNSELECTEDDOCUMENT;		sel = BMI_SELECTEDDOCUMENT;	ver = BFF_ISUNK;
						}
						m_LeftTreeCtrl.InsertItem(mymask, str, unsel, sel, 
									  0,0, // state 
									  ver | TLF_ISDDREV | (ddrevIT->number & 0x00FF), 
									  devrevParent, TVI_LAST  );
					}
				}
			}
		}
		
		m_LeftTreeCtrl.Expand( hRoot, TVE_EXPAND );
	}
	releaseDevList ( &pRoot );

	m_RgtListCtrl.InsertColumn(0,_T("DD-Revision"),LVCFMT_LEFT,300);

	EndWaitCursor();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
// Added By Deepak
// This method handles the double click on List Item
void CDDselect::OnDblclkListItem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// We have the rquired DD Rev Value in m_SelectedString 
	// Just Exit from the dialog and transfer the control to DDListdata.cpp
	EndDialog(IDOK);

	*pResult = 0;
}
// END Modification

/*************************************************************************************************
 *
 *   $History: DDselect.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */


