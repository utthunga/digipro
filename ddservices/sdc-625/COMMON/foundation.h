/*************************************************************************************************
 *
 * $Workfile: foundation.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of those tiny little classes that everything is built on
 *		4/25/02	sjv	created
 *
 * #include "foundation.h"
 */

#ifndef _FOUNDATION_H
#define _FOUNDATION_H

#pragma warning (disable : 4786) 
#ifndef FMAPARSER
#include "ddbGeneral.h"

#include "DDLDEFS.H"
#include "ddbdefs.h"
#endif

#include <ostream>
#include <iostream>

// Utility functions for UTF8 and wstring
using namespace std;

extern wstring UTF82Unicode(const char* src);
extern string ws2as(const wstring& src);
//extern wstring as2ws(const string& str );
extern wstring c2w(char *str );
extern char *w2c(wstring& str );
extern char *latin2utf8(char* s, char *buf, int bufsiz);
extern int latin2utf8size(char* s);
extern string addlinebreaks(char *s);
extern string trim(const std::string& str, const std::string& whitespace = " \t");

extern wchar_t *	/* compiled and linked from the UI strptime.cpp */
	strptime(const wchar_t *buf, const wchar_t *fmt, struct tm *tm);

#include "logging.h"

#ifndef FMAPARSER /***************************************************************************/
extern const  /* referenceTpe = id2refTbl[itemType]; */
referenceType_t id2refTbl[]; 

extern double uint64TOdbl(UINT64 uint64value);


class CrefType
{
#define MAX_REFTYPE_ENUM  ((int)rT_NotA_RefType) /* MAX_REFTYPE is NOT-A-REF */

	unsigned int refTyVal;		// has to be bigger than a char - see  referenceType_t
	static char  refTyName[][18];
	char*  pName;
	char returnString[128];

public:
	CrefType() {refTyVal = (MAX_REFTYPE_ENUM);};
	CrefType(unsigned int initVal) 
	{	refTyVal=initVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-value constructor set the value out of range00.(%d)\n",refTyVal);}
		#endif
	};
	CrefType (const CrefType& src)
	{	refTyVal = src.refTyVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-copy constructor set the value out of range01.(%d)\n", refTyVal);}
		#endif
	};
	CrefType& operator=(const CrefType& src)
	{	refTyVal = src.refTyVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-equal operator set the value out of range.02(%d)\n",refTyVal);
			    refTyVal=MAX_REFTYPE_ENUM;}
		#endif
		return *this;
	};
	CrefType& operator=(const unsigned short& src)
	{	refTyVal = src;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-int equal operator set the value out of range03.(%d)\n",refTyVal);
			    refTyVal=MAX_REFTYPE_ENUM;}
		#endif
		return *this;
	};

	bool operator==(const int otherValue) { return ( otherValue == refTyVal );};
	bool operator==(const referenceType_t otherValue) { return (otherValue == (referenceType_t)refTyVal );};

	bool operator!=(const int otherValue) { return !( otherValue == refTyVal );};
	bool operator!=(const referenceType_t otherValue) { return !(otherValue == (referenceType_t)refTyVal );};


	unsigned int getType(void){ return refTyVal;};
	void         setType(unsigned int newType){if (newType>=0 && newType<=(MAX_REFTYPE_ENUM)) refTyVal = newType; else refTyVal = MAX_REFTYPE_ENUM; };
	char*        getTypeStr(void)
				{
					if (refTyVal>=0 && refTyVal<=MAX_REFTYPE_ENUM)
					{	return refTyName[refTyVal];
					}
					else
					{
#if _MSC_VER >= 1400 && ! defined( _WIN32_WCE )	/* WS 9apr07 VS2005 checkin */					
						sprintf_s( returnString, 128, "InvalidRefType (%x)", refTyVal);
#else
						sprintf(returnString,"InvalidRefType (%x)",refTyVal);
#endif
						return returnString;
					}
				};
	int/*boolean*/ isValidRef(void){ if (refTyVal >= 0 && refTyVal < MAX_REFTYPE_ENUM) return 1; else return 0;};

	void clear(void){refTyVal=MAX_REFTYPE_ENUM;pName = NULL;};
};

inline ostream &operator<<(ostream& ostr, CrefType& it )
{
	ostr << it.getTypeStr();
	return ostr;
};


/*******************************
#define	RESERVED_ITYPE1			0
#define VARIABLE_ITYPE 	       	1
#define COMMAND_ITYPE           2
#define MENU_ITYPE              3
#define EDIT_DISP_ITYPE         4
#define METHOD_ITYPE            5
#define REFRESH_ITYPE           6
#define UNIT_ITYPE              7
#define WAO_ITYPE 	       		8
#define ITEM_ARRAY_ITYPE        9
#define COLLECTION_ITYPE        10
#define	RESERVED_ITYPE2			11
#define BLOCK_ITYPE             12
#define PROGRAM_ITYPE           13
#define RECORD_ITYPE            14
#define ARRAY_ITYPE             15
#define VAR_LIST_ITYPE          16
#define RESP_CODES_ITYPE        17
#define DOMAIN_ITYPE            18
#define MEMBER_ITYPE            19
#define MAX_ITYPE				20	// must be last in list 
//
//	Special object type values
//
#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130
 // these additional ITYPES are used by resolve to build resolve trails 
#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202
************************************/
class CitemType
{
	unsigned int itmTyVal;		// actually an itemType_t but left an int so load & read can share
	static char  itmTyName[MAX_ITYPE][16];
	static const int ref2idTbl[];
public:
	operator long &()		{ return (long &)itmTyVal ;      };
	operator itemType_t ()	{ return ((itemType_t)itmTyVal); };

	unsigned int getType(void){ return itmTyVal;};

	char*        getTypeStr(void)
				{return (itmTyVal>0 && itmTyVal<MAX_ITYPE)?itmTyName[itmTyVal]:itmTyName[0];};

	void clear(void){itmTyVal=0;};




	CitemType() {itmTyVal = 0xFFFFFFFF;};
	CitemType(unsigned int initVal) 
	{	itmTyVal=initVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal> MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-value constructor set the value out of range.(%d)\n",itmTyVal);}
		#endif
	};
	CitemType (const CitemType& src)
	{	itmTyVal = src.itmTyVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-copy constructor set the value out of range.(%d)\n",itmTyVal);}
		#endif
	};

	CitemType& operator=(const CitemType& src)
	{	itmTyVal = src.itmTyVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-equal operator set the value out of range.(%d)\n",itmTyVal);}
		#endif
		return *this;
	};
	//CitemType& operator=(const unsigned short& src)
	//{	itmTyVal = src;
	//	#ifdef _DEBUG
	//		if (itmTyVal<0 && itmTyVal>=MAX_ITYPE)
	//		{	cerr << "ERROR: CitemType-short equal operator set the value out of range.(" << itmTyVal << ")" << endl;}
	//	#endif
	//	return *this;
	//};
	CitemType& operator=(const long& src)
	{	itmTyVal = (unsigned int) src;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-long equal operator set the value out of range.(%d)\n",itmTyVal);}
		#endif
		return *this;
	};
	CitemType& operator=( CrefType& ref)
	{
		if (ref.getType() >= MAX_REFTYPE_ENUM )
			itmTyVal = MAX_REFTYPE_ENUM;
		else
			itmTyVal = ref2idTbl[ref.getType()];
		return *this;
	};
};

inline ostream &operator<<(ostream& ostr, CitemType& it )
{
	ostr << it.getTypeStr();
	return ostr;
};


/* stevev 20oct06 - added for device-object layout */
/*  this is defined in case we need to change to a float location value later */
#ifndef DDSizeLocType	/* it can be over-ridden */
#define DDSizeLocType  long 
#endif /* else, leave it as it came in */

typedef struct hv_point_s
{
	DDSizeLocType	xval;
	DDSizeLocType	yval;
#if !defined(__GNUC__)
	struct
#endif
	hv_point_s(){xval=yval=0;};
#if !defined(__GNUC__)
	struct
#endif
	hv_point_s(DDSizeLocType x,DDSizeLocType y){xval=x;yval=y;};
	bool operator==(const struct hv_point_s& p){ return ( (xval==p.xval)&&(yval==p.yval));};
	void clear(void){xval=yval=0;};
	bool isEmpty(void){ return ((xval==0)&&(yval==0));};
	hv_point_s& operator=(const struct hv_point_s& s){xval=s.xval;yval=s.yval;return *this;};
}
/* typedef */  hv_point_t; /* end-add 20oct06*/

/* stevev 28oct10
   leaf data extracted from menu to this struct so location and tree could do operator= */
struct leafData_s
{
	hv_point_t       topleft;
	hv_point_t		 hv_size;  // the horiz-vert size
	hv_point_t       da_size;  // the draw-as size, different only in pages
	DDSizeLocType    rowSize;  // stevev 20oct10-width of our row  (in unit-blocks)
	DDSizeLocType    canvasSize;
	/* note that rowSize is the number of columns used on the row we will be painted on.
		canvas size is the width of the canvas that rowSize items will be painted.
		so the spacing will be rowSize/canvasSize.
	The canvas size that a group's contents will be painted on is its da_size.xval.
	While the canvas size the group will be painted onto is it's canvasSize        */

	leafData_s():topleft(0,0),hv_size(0,0),da_size(0,0), rowSize(0), canvasSize(0){};
	leafData_s& operator=(const leafData_s& src){
		topleft = src.topleft;    hv_size = src.hv_size;    da_size = src.da_size;
		rowSize = src.rowSize; canvasSize = src.canvasSize; return *this;          };
	bool isEmpty() { return (topleft.isEmpty() && hv_size.isEmpty() && da_size.isEmpty() &&
			rowSize == 0 && canvasSize == 0);};
	bool operator==(const leafData_s& p) { return(
		topleft == p.topleft &&    hv_size == p.hv_size &&  da_size == p.da_size &&
		rowSize == p.rowSize && canvasSize == p.canvasSize );   };
	void clear(void) { 
		topleft.clear(); hv_size.clear(); da_size.clear(); rowSize = 0; canvasSize = 0;  };
};

/* stevev 28oct10 -- replace with full blown version below
typedef struct hv_location_s
{
	hv_point_t	topLeftPt;
	hv_point_t	rectSize;

	struct hv_location_s(){};//they each default to zero-zero
	struct hv_location_s(hv_point_t loc,hv_point_t siz){topLeftPt=loc;rectSize=siz;};

	void setValue(hv_point_t& tlPt, hv_point_t& recSz){topLeftPt=tlPt;rectSize=recSz;};
	bool operator==(const struct hv_location_s& p)
							{ return ( (topLeftPt==p.topLeftPt)&&(rectSize==p.rectSize));};
	bool operator!=(const struct hv_location_s& p)
							{ return !( (topLeftPt==p.topLeftPt)&&(rectSize==p.rectSize));};
	void clear(void)		{topLeftPt.clear();rectSize.clear();};
	bool isEmpty(void)		{ return ((topLeftPt.isEmpty())&&(rectSize.isEmpty()));};
	hv_location_s& operator=(const struct hv_location_s& s)
							{topLeftPt=s.topLeftPt;rectSize=s.rectSize;return *this;};
}// typedef 
hv_location_t; // end-add 20oct06
*************************************/
class hv_location_t : public leafData_s
{
public:
	hv_location_t(){};//they each default to zero-zero
	hv_location_t(leafData_s src){leafData_s::operator=(src);};
	hv_location_t(hv_point_t loc,hv_point_t siz, DDSizeLocType row,DDSizeLocType canvas){
		topleft = loc; hv_size=da_size=siz; rowSize = row; canvasSize = canvas;   };

	void setValue(hv_point_t loc,hv_point_t siz, DDSizeLocType row,DDSizeLocType canvas){
		topleft = loc; hv_size=da_size=siz; rowSize = row; canvasSize = canvas;   };

	bool operator==(const hv_location_t& p)   {
		return ( leafData_s::operator==(p) ); };
		bool operator!=(const hv_location_t& p) {
			return ( ! (leafData_s::operator==(p))); };
	// use leaf's void clear(void)		{topLeftPt.clear();rectSize.clear();};
	// use leaf's bool isEmpty(void)		{ return ((topLeftPt.isEmpty())&&(rectSize.isEmpty()));};
	hv_location_t& operator=(const hv_location_t& s){ leafData_s::operator=(s); return *this;  };
};

#endif // FMAPARSER **************************************************************************

//ws 12mar08 - added from the unicode website (in lieu of .h file)
typedef unsigned long	UTF32;	/* at least 32 bits */
typedef unsigned short	UTF16;	/* at least 16 bits */
typedef unsigned char	UTF8;	/* typically 8 bits */
// stevev 26jan11 -at inventsys prompting   typedef unsigned char	Boolean; 
typedef bool			Boolean; 

/* Some fundamental constants */
#define UNI_REPLACEMENT_CHAR (UTF32)0x0000FFFD
#define UNI_MAX_BMP (UTF32)0x0000FFFF
#define UNI_MAX_UTF16 (UTF32)0x0010FFFF
#define UNI_MAX_UTF32 (UTF32)0x7FFFFFFF
#define UNI_MAX_LEGAL_UTF32 (UTF32)0x0010FFFF

typedef enum {
	conversionOK, 		/* conversion successful */
	sourceExhausted,	/* partial character in source, but hit end */
	targetExhausted,	/* insuff. room in target for conversion */
	sourceIllegal		/* source sequence is illegal/malformed */
} ConversionResult;

typedef enum {
	strictConversion = 0,
	lenientConversion
} ConversionFlags;

/* This is for C++ and does no harm in C */
#ifdef __cplusplus
extern "C" {
#endif


#if defined(__GNUC__)
ConversionResult ConvertUTF8toUTF32 (
		        const UTF8** sourceStart, const UTF8* sourceEnd, 
		                UTF32** targetStart, UTF32* targetEnd, ConversionFlags flags);

ConversionResult ConvertUTF32toUTF8 (
		        const UTF32** sourceStart, const UTF32* sourceEnd, 
		                UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags);
#else // !__GNUC__
ConversionResult ConvertUTF8toUTF16 (
		const UTF8** sourceStart, const UTF8* sourceEnd, 
		UTF16** targetStart, UTF16* targetEnd, ConversionFlags flags);

ConversionResult ConvertUTF16toUTF8 (
		const UTF16** sourceStart, const UTF16* sourceEnd, 
		UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags);
#endif // __GNUC__



bool isLegalUTF8Sequence(const UTF8 *source, const UTF8 *sourceEnd);

#ifdef __cplusplus
}
#endif
//end ws 12mar08

#endif//_FOUNDATION_H

/*************************************************************************************************
 *
 *   $History: foundation.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
