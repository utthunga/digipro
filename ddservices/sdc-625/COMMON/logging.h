/**********************************************************************************************
 *
 * $Workfile: logging.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *********************************************************************************************
 * This file was generated for the logging.cpp file
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *		logging.h : external interface to the error logging functions
 *
 * #include "logging.h"
 *
 */

#ifndef _LOGGING_H
#define _LOGGING_H

#include "xplatform_logging.h"

/* 

NOTE: See CrossPlatform/xplatform_logging.h for permission bits, log buffer sizes and more.


- LOGGING CONFIGURATION AND MACROS:

-- Windows-only features:

    // ENTRY PREP - Set up logging.
    int logSetup(DDSargs  &rArgs, wchar_t * cmdLine = NULL);

    // INFO - Logging status informational routines.
    bool isClogging(void);
    bool isCerring(void);
    bool isOuting(void);

    // USER INTERFACE FUNCTIONS
    bool GetLogMsgFromLog( int nLine, char *pLogMsg );
    void GetNoOfLogMsgs( int *pnTotalLogMsgCnt );
    void ClearLog(); //Added by ANOOP 03MAR2004

    // STATUS BAR MAINTAINENCE 
    void GetStatusString(wchar_t statusRetStr[]);


-- Logging macros and helpers - cross-platform:
   IMPORTANT: See platform specific headers for implementation variations.

    // MACRO - Standard logging
    LOGIT(logChannels, format, ...);

    // MACRO - SDC logging.
    SDCLOG(...) LOGIT(__VA_ARGS__)


    // MACRO - Conditional logging
    LOGIF(perm)(logChannels, format, ...)


    // MACRO - Is logging enabled for this bitmap of permissions?
    LOGTHIS(perm) 

    // MACRO - LOGIT and LOGIF equivalents when _DEBUG is defined.
    DEBUGLOG(logChannels, format, ...)
    DBGLOGIF(perm)(logChannels, format, ...)


    // TIME DURATION HELPERS
    void captureStartTime(void);
    void logTime(void);

    // EXIT CLEANUP - Clean-up and exit logging.
    void logExit(void);

*/

#endif // _LOGGING_H
