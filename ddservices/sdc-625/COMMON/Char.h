/*************************************************************************************************
 *
 * Workfile: Char.h
 * 11Dec07 - stevev
 *     Revision, Date and Author are not in the file!
 *
 *************************************************************************************************
 * The content of this file is the
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2007, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		HANDLES THE DIFFERENCES BETWEEN WIDE AND NARROW CHARACTERS
 *
 * Component History:
 *
 *
 * #include "Char.h"
 */

#ifndef _CHAR_H
#define _CHAR_H

#pragma warning (disable : 4786)

#include <string>
#include <iostream>
#include <iomanip>

using namespace std;
#pragma warning (disable : 4786)

#ifdef _UNICODE
#define tstring wstring

#ifndef tcerr
#define tcerr	wcerr
#define tclog	wclog
#define tcout	wcout
#endif

#ifndef  _T
#define _T(s) L##s
#define _ultot( a, b, c ) _ultow ( a, b, c )
#endif /*not IS_UI */

typedef wchar_t tchar;
#define TCHAR   wchar_t
#define _ttof( a ) _wtof( a )
#define _tsscanf( a, b, c ) swscanf( a, b, c )
//#define _tsprintf( a, b, c ) swprintf( a, b, c )
#define _tsprintf            swprintf
#define lmemcpy( a, b, c )   wmemcpy( a, b, c )
#define ttoi( a ) _wtoi ( a )
//double _wtof( const wchar_t* str );
#define _itoT( a , b , c )   _itow(  a , b , c  )
#define tctime( a )          _wctime ( a )
#define _tatoi				 _wtoi
#define _tatol				 _wtol

#define tisalpha			  iswalpha
#define tisdigit			  iswdigit
#define _tstrcpy( a, b )      wcscpy( a , b )
#define _tstrncpy( a, b, c )  wcsncpy( a, b, c )
#define _tstrlen			  wcslen
#define _tscanf				  wscanf
#define _tstrchar			  wcschr
#define _tstrcmp			  wcscmp
#define _tstrncmp			  wcsncmp
#define _tstrcat              wcscat
// os #define _tfullpath( a, b, c )  _wfullpath( a, b, c )
#define _tvsprintf			  vswprintf

#else

#ifndef tcerr
#define tcerr	cerr
#define tclog	clog
#define tcout	cout
#endif


#define tstring string
#ifndef _T
#define _T(s) s
#define _ultot( a, b, c ) _ultoa ( a, b, c )
#endif

typedef  char   tchar;
#define TCHAR   char
#define _ttof( a ) atof( a )
#define _itoT(  a , b , c  )  _itoa(  a , b , c  )
#define _tatoi				  atoi
#define _tatol				  atol
#define _tsprintf             sprintf

#define tisalpha			  isalpha
#define tisdigit			  isdigit
#define _tstrcpy( a, b )      strcpy( a , b )
#define _tstrncpy( a, b, c )  strncpy( a, b, c )
#define _tstrlen              strlen
#define _tscanf				  scanf
#define _tstrchar			  strchr
#define _tstrcmp			  strcmp
#define _tstrncmp			  strncmp
#define _tstrcat              strcat
//os  #define _tfullpath( a, b, c )  _fullpath( a, b, c )
#define _tvsprintf			  vsprintf

#endif //USINGTCHAR
// stevev - WIDE2NARROW char interface
#define mt_String	_T("")
string TStr2AStr(const  wstring& str );
wstring AStr2TStr(const string& str );
#define _tDestroyTheStr() theStr.~wstring()

tchar char2wchar(char *);
#if _MSC_VER >= 1400	/* VS8 (2005) */
_CRTIMP __checkReturn double __cdecl _wtof(__in_z const wchar_t *_Str);
#else
extern double _wtof(const wchar_t* a );
#endif

#endif /* _CHAR_H  */
