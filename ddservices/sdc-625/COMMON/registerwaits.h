/*************************************************************************************************
 *
 * $Workfile: registerwaits.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is an implementation of a OS function set available on win 200 and above
 *		
 * #include "registerWaits.h"
 */
 

#ifndef _REGISTERWAITS_H
#define _REGISTERWAITS_H

// we need to make the dispatcher capable of tasking

#include "xplatform_thread.h"

#endif _REGISTERWAITS_H

/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
