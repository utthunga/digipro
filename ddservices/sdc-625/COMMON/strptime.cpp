//#include "misc.h"

/* modified for wide characters and Visual Studio 6 by Steve Vreeland 27may08*/
/*
 * Ported from NetBSD to Windows by Ron Koenderink, 2007
 */

/*      $NetBSD: strptime.c,v 1.25 2005/11/29 03:12:00 christos Exp $   */

/*-
 * Copyright (c) 1997, 1998, 2005 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code was contributed to The NetBSD Foundation by Klaus Klein.
 * Heavily optimised by David Laight
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *        This product includes software developed by the NetBSD
 *        Foundation, Inc. and its contributors.
 * 4. Neither the name of The NetBSD Foundation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_WIN32)
#include <sys/cdefs.h>
#endif

#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: strptime.c,v 1.25 2005/11/29 03:12:00 christos Exp $");
#endif

#if !defined(_WIN32) && !defined(__GNUC__)
#include "namespace.h"
#include <sys/localedef.h>
#else

#if defined(_WIN32)
#include <tChar.h>
#else // ! _WIN32
#include <wchar.h>
#include "Char.h"
#endif // _WIN32

#include <stdio.h>
typedef wchar_t t_char;
typedef unsigned int uint;


#if defined(_WIN32)
#include <Windows.h>
#endif // _WIN32

#endif


#include <ctype.h>
#include <locale.h>
#include <string.h>
#include <time.h>

#if !defined(_WIN32) && !defined(__GNUC__)
#include <tzfile.h>
#endif

#ifdef __weak_alias
__weak_alias(strptime,_strptime)
#endif

#if !defined(_WIN32) && !defined(__GNUC__)
#define _ctloc(x)               (_CurrentTimeLocale->x)
#define PREF		const	
#else
#ifdef XMTR
#define _ctloc(x)               (_CurrentTimeLocale->x)
#define PREF						
#else
#define _ctloc(x)   (x)
#define PREF		const				
#endif
PREF wchar_t *abday[] = {
        _T("Sun"), _T("Mon"), _T("Tue"), _T("Wed"),
        _T("Thu"), _T("Fri"), _T("Sat")
};
PREF wchar_t *day[] = {
        _T("Sunday"), _T("Monday"), _T("Tuesday"), _T("Wednesday"),
        _T("Thursday"), _T("Friday"), _T("Saturday")
};
PREF wchar_t *abmon[] =   {
        _T("Jan"), _T("Feb"), _T("Mar"), _T("Apr"), _T("May"), _T("Jun"),
        _T("Jul"), _T("Aug"), _T("Sep"), _T("Oct"), _T("Nov"), _T("Dec")
};
PREF wchar_t *mon[] = {
        _T("January"), _T("February"), _T("March"), _T("April"), _T("May"), _T("June"),
        _T("July"), _T("August"), _T("September"), _T("October"), _T("November"), _T("December")
};
PREF wchar_t *am_pm[] = {
        _T("AM"), _T("PM")
};
wchar_t *d_t_fmt = _T("%a %Ef %T %Y");
wchar_t *t_fmt_ampm = _T("%I:%M:%S %p");
wchar_t *t_fmt = _T("%H:%M:%S");
wchar_t *d_fmt = _T("%m/%d/%y");

//#define STRX     _T("                    ")
#define STRX     _T(" ")
#define STRXLen  20

typedef
struct CurrentTimeLocale_s
{
	wchar_t *abday[7];
	wchar_t *day  [7];
	wchar_t *abmon[13];
	wchar_t *mon  [13];
	wchar_t *am_pm[2];

	wchar_t *d_t_fmt;
	wchar_t *t_fmt_ampm;
	wchar_t *t_fmt;
	wchar_t *d_fmt;
} CurrentTimeLocale_t;

CurrentTimeLocale_t CurrentTimeLocale = { 
 { STRX,STRX,STRX,STRX,STRX,STRX,STRX  },
 { STRX,STRX,STRX,STRX,STRX,STRX,STRX  },
 { STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX  },
 { STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX,STRX  },
 { STRX,STRX  },
 STRX,
 STRX,
 STRX,
 STRX
};
CurrentTimeLocale_t* _CurrentTimeLocale = &CurrentTimeLocale;
bool    areSet   = false;

#define TM_YEAR_BASE 1900
#define __UNCONST(x) ((wchar_t *)(((const wchar_t *)(x) - (const wchar_t *)0) + (wchar_t *)0))


#if defined(_WIN32)
#define strncasecmp  _wcsnicmp
#else // !_WIN32
#define strncasecmp wcsncasecmp
#endif // _WIN32

static void fill_local(void);// at the end of the file

#endif
/*
 * We do not implement alternate representations. However, we always
 * check whether a given modifier is allowed for a certain conversion.
 */
#define ALT_E                   0x01
#define ALT_O                   0x02
#define LEGAL_ALT(x)            { if (alt_format & ~(x)) return NULL; }


static const t_char *conv_num(const wchar_t *, int *, uint, uint);
static const t_char *find_string(const t_char *, int *, const wchar_t * const *,
        const wchar_t * const *, int);


wchar_t *
strptime(const wchar_t *buf, const wchar_t *fmt, struct tm *tm)
{
        wchar_t c;
        const wchar_t *bp;
        int alt_format, i, split_year = 0;
        const wchar_t *new_fmt;

		if ( ! areSet ) fill_local();

        bp = (const t_char *)buf;

        while (bp != NULL && (c = *fmt++) != '\0') 
		{
                /* Clear `alternate' modifier prior to new conversion. */
                alt_format = 0;
                i = 0;

                /* Eat up white-space. */
                if (isspace(c)) {
                        while (isspace(*bp))
                                bp++;
                        continue;
                }

                if (c != '%')
                        goto literal;


again:          switch (c = *fmt++) {
                case '%':       /* "%%" is converted to "%". */
literal:
                        if (c != *bp++)
                                return NULL;
                        LEGAL_ALT(0);
                        continue;

                /*
                 * "Alternative" modifiers. Just set the appropriate flag
                 * and start over again.
                 */
                case 'E':       /* "%E?" alternative conversion modifier. */
                        LEGAL_ALT(0);
                        alt_format |= ALT_E;
                        goto again;

                case 'O':       /* "%O?" alternative conversion modifier. */
                        LEGAL_ALT(0);
                        alt_format |= ALT_O;
                        goto again;

                /*
                 * "Complex" conversion rules, implemented through recursion.
                 */
                case 'c':       /* Date and time, using the locale's format. */
                        new_fmt = _ctloc(d_t_fmt);
                        goto recurse;

                case 'D':       /* The date as "%m/%d/%y". */
                        new_fmt = _T("%m/%d/%y");
                        LEGAL_ALT(0);
                        goto recurse;

                case 'R':       /* The time as "%H:%M". */
                        new_fmt = _T("%H:%M");
                        LEGAL_ALT(0);
                        goto recurse;

                case 'r':       /* The time in 12-hour clock representation. */
                        new_fmt =_ctloc(t_fmt_ampm);
                        LEGAL_ALT(0);
                        goto recurse;

                case 'T':       /* The time as "%H:%M:%S". */
                        new_fmt = _T("%H:%M:%S");
                        LEGAL_ALT(0);
                        goto recurse;

                case 'X':       /* The time, using the locale's format. */
                        new_fmt =_ctloc(t_fmt);
                        goto recurse;

                case 'x':       /* The date, using the locale's format. */
                        new_fmt =_ctloc(d_fmt);
                    recurse:
                        bp = (const t_char *)strptime((const wchar_t *)bp,
                                                            new_fmt, tm);
                        LEGAL_ALT(ALT_E);
                        continue;

                /*
                 * "Elementary" conversion rules.
                 */
                case 'A':       /* The day of week, using the locale's form. */
                case 'a':
                        bp = find_string(bp, &tm->tm_wday, _ctloc(day),
                                        _ctloc(abday), 7);
                        LEGAL_ALT(0);
                        continue;

                case 'B':       /* The month, using the locale's form. */
                case 'b':
                case 'h':
                        bp = find_string(bp, &tm->tm_mon, _ctloc(mon),
                                        _ctloc(abmon), 12);
                        LEGAL_ALT(0);
                        continue;

                case 'C':       /* The century number. */
                        i = 20;
                        bp = conv_num(bp, &i, 0, 99);

                        i = i * 100 - TM_YEAR_BASE;
                        if (split_year)
                                i += tm->tm_year % 100;
                        split_year = 1;
                        tm->tm_year = i;
                        LEGAL_ALT(ALT_E);
                        continue;

                case 'd':       /* The day of month. */
                case 'e':
                        bp = conv_num(bp, &tm->tm_mday, 1, 31);
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'k':       /* The hour (24-hour clock representation). */
                        LEGAL_ALT(0);
                        /* FALLTHROUGH */
                case 'H':
                        bp = conv_num(bp, &tm->tm_hour, 0, 23);
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'l':       /* The hour (12-hour clock representation). */
                        LEGAL_ALT(0);
                        /* FALLTHROUGH */
                case 'I':
                        bp = conv_num(bp, &tm->tm_hour, 1, 12);
                        if (tm->tm_hour == 12)
                                tm->tm_hour = 0;
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'j':       /* The day of year. */
                        i = 1;
                        bp = conv_num(bp, &i, 1, 366);
                        tm->tm_yday = i - 1;
                        LEGAL_ALT(0);
                        continue;

                case 'M':       /* The minute. */
                        bp = conv_num(bp, &tm->tm_min, 0, 59);
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'm':       /* The month. */
                        i = 1;
                        bp = conv_num(bp, &i, 1, 12);
                        tm->tm_mon = i - 1;
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'p':       /* The locale's equivalent of AM/PM. */
					{	
						wchar_t *targetStr[2], tempStrAM[12], tempStrPM[12];
						targetStr[0] = tempStrAM;
						targetStr[1] = tempStrPM;
#if defined(_WIN32)
						if ( GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S1159, tempStrAM, 12) |
							 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S2359, tempStrPM, 12) )//success
						{
							bp = find_string(bp, &i, targetStr, NULL, 2);
						}
						else
#endif // _WIN32
						{
							bp = find_string(bp, &i, _ctloc(am_pm), NULL, 2);
						}

                        if (tm->tm_hour > 11)
                                return NULL;
                        tm->tm_hour += i * 12;
                        LEGAL_ALT(0);
                        continue;
					}

                case 'S':       /* The seconds. */
                        bp = conv_num(bp, &tm->tm_sec, 0, 61);
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'U':       /* The week of year, beginning on sunday. */
                case 'W':       /* The week of year, beginning on monday. */
                        /*
                         * XXX This is bogus, as we can not assume any valid
                         * information present in the tm structure at this
                         * point to calculate a real value, so just check the
                         * range for now.
                         */
                         bp = conv_num(bp, &i, 0, 53);
                         LEGAL_ALT(ALT_O);
                         continue;

                case 'w':       /* The day of week, beginning on sunday. */
                        bp = conv_num(bp, &tm->tm_wday, 0, 6);
                        LEGAL_ALT(ALT_O);
                        continue;

                case 'Y':       /* The year. */
                        i = TM_YEAR_BASE;       /* just for data sanity... */
                        bp = conv_num(bp, &i, 0, 9999);
                        tm->tm_year = i - TM_YEAR_BASE;
                        LEGAL_ALT(ALT_E);
                        continue;

                case 'y':       /* The year within 100 years of the epoch. */
                        /* LEGAL_ALT(ALT_E | ALT_O); */
                        bp = conv_num(bp, &i, 0, 99);

                        if (split_year)
                                /* preserve century */
                                i += (tm->tm_year / 100) * 100;
                        else {
                                split_year = 1;
                                if (i <= 68)
                                        i = i + 2000 - TM_YEAR_BASE;
                                else
                                        i = i + 1900 - TM_YEAR_BASE;
                        }
                        tm->tm_year = i;
                        continue;

                /*
                 * Miscellaneous conversions.
                 */
                case 'n':       /* Any kind of white-space. */
                case 't':
                        while (isspace(*bp))
                                bp++;
                        LEGAL_ALT(0);
                        continue;


                default:        /* Unknown/unsupported conversion. */
                        return NULL;
                }
        }

        return __UNCONST(bp);
}


static const t_char *
conv_num(const  wchar_t *buf, int *dest, uint llim, uint ulim)
{
        uint result = 0;
        wchar_t ch;

        /* The limit also determines the number of valid digits. */
        uint rulim = ulim;

        ch = *buf;
        if (ch < '0' || ch > '9')
                return NULL;

        do {
                result *= 10;
                result += ch - '0';
                rulim /= 10;
                ch = *++buf;
        } while ((result * 10 <= ulim) && rulim && ch >= '0' && ch <= '9');

        if (result < llim || result > ulim)
                return NULL;

        *dest = result;
        return buf;
}

static const t_char *
find_string(const t_char *bp, int *tgt, const wchar_t * const *n1,
                const wchar_t * const *n2, int c)

{
        int i;
        unsigned int len;

        /* check full name - then abbreviated ones */
        for (; n1 != NULL; n1 = n2, n2 = NULL) {
                for (i = 0; i < c; i++, n1++) {
                        len = wcslen(*n1);
                        if (strncasecmp(*n1, (const wchar_t *)bp, len) == 0) {
                                *tgt = i;
                                return bp + len;
                        }
                }
        }

        /* Nothing matched */
        return NULL;
}


// this will fill the strings
static void 
fill_local(void)
{
	// set the default then set locale so any error will leave the default
	int i;
	for ( i = 0; i < 7; i++)  CurrentTimeLocale.abday[i] = abday[i];
	for ( i = 0; i < 7; i++)  CurrentTimeLocale.day[i]   =   day[i];
	for ( i = 0; i < 12;i++)  CurrentTimeLocale.abmon[i] = abmon[i];
	for ( i = 0; i < 12;i++)  CurrentTimeLocale.mon[i]   =   mon[i]; 
	CurrentTimeLocale.am_pm[0] = am_pm[0];
	CurrentTimeLocale.am_pm[1] = am_pm[1];
	CurrentTimeLocale.d_t_fmt  =  d_t_fmt;
	CurrentTimeLocale.t_fmt_ampm=t_fmt_ampm;
	CurrentTimeLocale.t_fmt =     t_fmt;
	CurrentTimeLocale.d_fmt =     d_fmt;
#ifdef x_DEBUG /* to see what we get...*/
	wchar_t langname[9] = STRX;
	wchar_t ym[] = STRX;
	GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SISO639LANGNAME, langname, 9);
// not on 2000::	GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SYEARMONTH,      ym,       STRXLen);
#endif 
#ifdef USE_LOCALE_INFO
//_ctloc(x)               (_CurrentTimeLocale->am_pm)[0]	
//	GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S1159, _ctloc(am_pm)[0], STRXLen);
GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S1159, CurrentTimeLocale.am_pm[0], STRXLen);
//	GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S2359, _ctloc(am_pm)[1], STRXLen);
GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_S2359, CurrentTimeLocale.am_pm[1], STRXLen);
/*GetLocaleInfoW(__in LCID Locale,  __in LCTYPE LCType,__out_ecount_opt(cchData) PWSTR  lpLCData, __in int      cchData);*/
	/* We'll load the strings but we're not going to mess with the dividers and stuff*/
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME1         , _ctloc(day)[0], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME2         , _ctloc(day)[1], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME3         , _ctloc(day)[2], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME4         , _ctloc(day)[3], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME5         , _ctloc(day)[4], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME6         , _ctloc(day)[5], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SDAYNAME7         , _ctloc(day)[6], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME1   , _ctloc(abday)[0], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME2   , _ctloc(abday)[1], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME3   , _ctloc(abday)[2], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME4   , _ctloc(abday)[3], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME5   , _ctloc(abday)[4], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME6   , _ctloc(abday)[5], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVDAYNAME7   , _ctloc(abday)[6], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME1       , _ctloc(mon)[0], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME2       , _ctloc(mon)[1], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME3       , _ctloc(mon)[2], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME4       , _ctloc(mon)[3], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME5       , _ctloc(mon)[4], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME6       , _ctloc(mon)[5], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME7       , _ctloc(mon)[6], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME8       , _ctloc(mon)[7], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME9       , _ctloc(mon)[8], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME10      , _ctloc(mon)[9], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME11      , _ctloc(mon)[10], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME12      , _ctloc(mon)[11], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SMONTHNAME13      , _ctloc(mon)[12], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME1 , _ctloc(abmon)[0],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME2 , _ctloc(abmon)[1],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME3 , _ctloc(abmon)[2],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME4 , _ctloc(abmon)[3],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME5 , _ctloc(abmon)[4],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME6 , _ctloc(abmon)[5],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME7 , _ctloc(abmon)[6],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME8 , _ctloc(abmon)[7],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME9 , _ctloc(abmon)[8],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME10, _ctloc(abmon)[9],  STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME11, _ctloc(abmon)[10], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME12, _ctloc(abmon)[11], STRXLen);
	 GetLocaleInfo(LOCALE_SYSTEM_DEFAULT,LOCALE_SABBREVMONTHNAME13, _ctloc(abmon)[12], STRXLen);
#endif
	areSet = true;
	return;
}
//LOCALE_IDATE	 		short date fmt  mdy / dmy / ymd     
//LOCALE_ILDATE 		long  date fmt  mdy / dmy / ymd                  
//LOCALE_ITIME			12 / 24
//LOCALE_ITIMEMARKPOSN  am/pm front or back of rest of string
//LOCALE_ICENTURY       short date century 4 or 2 digits
//LOCALE_ITLZERO        leading zeros in time fields bool
//LOCALE_IDAYLZERO      leading zero for day in short date bool
//LOCALE_IMONLZERO      leading zero for mon in short date bool

//LOCALE_IFIRSTDAYOFWEEK   
//LOCALE_IFIRSTWEEKOFYEAR  

