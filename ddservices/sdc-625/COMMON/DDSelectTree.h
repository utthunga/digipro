/*************************************************************************************************
 *
 * $Workfile: SelectTree.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "SelectTree.h"
 */
#if !defined(AFX_SELECTTREE_H__9F64A2D5_058B_4FCF_9F4D_662D27C422A1__INCLUDED_)
#define AFX_SELECTTREE_H__9F64A2D5_058B_4FCF_9F4D_662D27C422A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef enum treelevelFlag_e
{
	TLF_ISROOT    = 0x00000,
	TLF_ISMFG     = 0x10000,
	TLF_ISDEVTYPE = 0x20000,
	TLF_ISDEVREV  = 0x30000,
	TLF_ISDDREV   = 0x40000
} treelevelFlag_t;

typedef enum binaryFormat_e
{
	BFF_ISUNK    = 0x000000,	// fms, version 5 and all unknowns
	BFF_ISV_6    = 0x600000,	// fm6, version 6
	BFF_ISV_8    = 0x800000		// fm8, version 8
} binaryFormat_t;

/////////////////////////////////////////////////////////////////////////////
// CSelectTree window

class CSelectTree : public CTreeCtrl
{
// Construction
public:
	CSelectTree();

// Attributes
public:
	CString  rootPath;// sdc compatibility
	__int64    DDKey;// sdc compatibility

	CString* pStrPath;
	DD_Key_t*   pDDKey;
	//CEdit*   pDispCtrl;
	CDialog* pMyDlg;
// Added By Deepak
// This willbe used for updating DDRev value in List Control
	CListCtrl *pMyListCtrl;
// END Modification

	void setDlg(CDialog* daDlg) {pMyDlg = daDlg;};
// Added by Deepak
// This method should be called to set the value of pMyListCtrl
	void setListCtrl(CListCtrl *pListCtrl){pMyListCtrl = pListCtrl;};
//END modification

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectTree)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSelectTree();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSelectTree)
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTTREE_H__9F64A2D5_058B_4FCF_9F4D_662D27C422A1__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: SelectTree.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
