/*************************************************************************************************
 *
 * $Workfile: sysEnum.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of the system enumeration template class
 *		9/02/04	sjv	created
 *
 * #include "sysEnum.h"
 */

#ifndef _SYSENUM_H
#define _SYSENUM_H

#pragma warning (disable : 4786) 

#include "ddbGeneral.h"
#include "DDLDEFS.H"
#include "ddbdefs.h"
#include "DDBpayload.h"
#include "ddbGlblSrvInfc.h"
#include "dllapi.h"

#include "ddbConditional.h"  // includes reference
#include "ddbPrimatives.h"

#include <ostream>
#include <iostream>

#if !defined(__GNUC__)
#include <basetsd.h>
#endif // __GUNC__

#include <string>
#include <vector>

using namespace std;

class enumStr : public vector<string>
{ public:	
	enumStr(int strCnt, int maxStrLen, char* parr)
	{  if (parr == NULL)return;
       for( int i = 0; i < strCnt; i++, parr +=maxStrLen)	{ push_back(string(parr)); } 
	};
	enumStr& operator=(enumStr& s)
	{ 
		//(*this) = s;
	
		clear();
		
		for( int i = 0; i < (int)s.size(); i++)	// J.U. warning C4717
		{
			push_back(string(s[i])); 
		} 
		
		return *this;
	};
	enumStr( enumStr& src ){ clear(); (*this)  =  src; };// copy vector
};

/* MINVALUE is first (0) string value, 
 * MAXVALUE is last stringValue (assumed to be the Illegal-Value)             
 * BASETYPE MUST have a hCddlLong as a base class.
 */
// use this class instead of hCconditional<hCddlLong> condEnumValue;
template<class ENUMTYPE, class BASETYPE, int MINVALUE, int MAXVALUE, int MAXSTRLEN>
class hCsystemEnum : public hCpayload, public hCobject, public enumStr
{
protected:
	hCconditional<BASETYPE> condEnumValue;
	ENUMTYPE systemEnum;
    // pStr     is really a pointer to an array (maxvalue+1 long) of strings that are MAXSTRLEN long
	char *pStr;

public:	
	hCsystemEnum(DevInfcHandle_t h, char* pStr)
		: hCobject(h), condEnumValue(h), pStr(pStr), enumStr(MAXVALUE-MINVALUE+1,MAXSTRLEN, pStr)
	{	systemEnum = (ENUMTYPE)MAXVALUE; };
	
	hCsystemEnum(DevInfcHandle_t h,int initVal, char* pStr) : 
				hCobject(h), condEnumValue(h), pStr(pStr), enumStr(MAXVALUE-MINVALUE+1,MAXSTRLEN, pStr)
	{	if (initVal >= MINVALUE && initVal <= MAXVALUE)  systemEnum = (ENUMTYPE)initVal;
												else     systemEnum = (ENUMTYPE)(MAXVALUE+1);
		#ifdef _DEBUG
			if (initVal<MINVALUE || initVal>MAXVALUE)
			{	LOGIT(CERR_LOG,"ERROR: hCsystemEnum constructor set the value out of range00.(" 
			         "%d)\n",initVal);}
		#endif
	};

	hCsystemEnum (const hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE,MAXSTRLEN>& src)
		: hCobject( src.devHndl() ), 
		  condEnumValue(src.devHndl()),enumStr(MAXVALUE-MINVALUE+1,MAXSTRLEN, src.pStr)
	{	systemEnum = src.systemEnum; condEnumValue=src.condEnumValue; pStr = src.pStr;
		#ifdef _DEBUG
			if (systemEnum<MINVALUE || systemEnum>MAXVALUE) 
			{	LOGIT(CERR_LOG,"ERROR: hCsystemEnum constructor set the value out of range01.(" 
					 "%d)\n", systemEnum);}
		#endif
	};
	
	hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>& 
		operator= 
		(const hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>& src)
	{	systemEnum = src.systemEnum; condEnumValue=src.condEnumValue; pStr = src.pStr;
		#ifdef _DEBUG
			if (systemEnum<MINVALUE || systemEnum>MAXVALUE) 
			{	LOGIT(CERR_LOG,"ERROR: hCsystemEnum constructor set the value out of range02.(" 
					 "%d)\n",systemEnum);}
		#endif
		return *this;
	};

	BASETYPE procureBase(void)
	{	BASETYPE   Lng(devHndl());
		RETURNCODE rc = condEnumValue.resolveCond(&Lng);
		if ( rc != SUCCESS ) Lng.clear();
		return Lng;
	};

	virtual
	ENUMTYPE getSysEnum(void) 
/*	{ 
		BASETYPE Lng(devHndl());
		// int r;
		RETURNCODE rc = condEnumValue.resolveCond(&Lng);
		if ( rc == SUCCESS )
		{			
			return (systemEnum=(ENUMTYPE) Lng.getDdlLong()); 
		}
		else
		{
			return (ENUMTYPE)MAXVALUE;
		} 

	}*/;
	virtual
	void    setSysEnum(ENUMTYPE newVal)
/*	{	BASETYPE Lng = NULL;
		RETURNCODE rc;
		if ( condEnumValue.priExprType == aT_Unknown)
		{
			condEnumValue.setPayload(Lng);
		}
		rc= condEnumValue.resolveCond(&Lng);
		if ( rc != SUCCESS )
		{
			return ;// error condition
		} 
		
		if (newVal >= MINVALUE && newVal <= MAXVALUE)  
				systemEnum = newVal;
		else	systemEnum = (ENUMTYPE)MAXVALUE;
		Lng.setDdlLong((ulong)systemEnum);
	}*/;

	virtual
	void	setCondValue(ENUMTYPE newVal);
	virtual
	operator int()		{ return (int)getSysEnum();};    // cast to a int
	virtual
	operator string()	{ return at((int)getSysEnum());};// cast to a string

	virtual
	char*   getSysEnumStr(void)
	{
		// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		if ( (systemEnum >= 0) && (systemEnum < (ENUMTYPE)size()) ) 
			return ( (char*)(at(systemEnum).c_str()) );

		return "OUT_OF_RANGE";
	};
	
	int   getEnumStrList(vector<string>& retList) // returns 0 at no error
	{retList = *((vector<string>*)this);};

	RETURNCODE destroy(void) {return SUCCESS;};
	void clear(void){systemEnum = (ENUMTYPE)(MAXVALUE + 1); 
					 enumStr::clear();};
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		{ /*COUTSPACE*/LOGIT(COUT_LOG,"SystemEnum is 0x%x  (%d)\n", ((int)systemEnum) 
		  ,at((int)systemEnum).c_str() );return SUCCESS;};


	/* Payload required methods */
	virtual
	void  setEqual(void* pAclass) // must be a aCattrCondLong or similar conditioanal
	{	if (pAclass )
			condEnumValue.makeEqual((aCconditional*) &(((aCattrCondLong*)pAclass)->condLong));

#ifdef _DEBUG
		else
			LOGIT(CERR_LOG,"ERROR: system enum passed a non-long aC class.\n");
#endif
	};
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0)
	{	if (pPayLd != NULL )
			condEnumValue.setDuplicate(
			((hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>*)pPayLd)->
																				  condEnumValue,replicate );
	};
	// each payload must tell if the abstract payload type is correct for itself
	virtual
	bool validPayload(payloadType_t ldType)
	{ if (ldType != pltddlLong) return false; else return true;};
};


template<class ENUMTYPE, class BASETYPE, int MINVALUE, int MAXVALUE, int MAXSTRLEN>	
ENUMTYPE 
hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>
::getSysEnum(void) 
{ 
/* this trashes the stack in releasemode utf-8 version
	BASETYPE Lng(devHndl());
	// int r;
	RETURNCODE rc = condEnumValue.resolveCond(&Lng);
	if ( rc == SUCCESS )
	{			
		return (systemEnum=(ENUMTYPE) Lng.getDdlLong()); 
	}
	else
	{
		return (ENUMTYPE)MAXVALUE;
	} 
*/// this didn't help the stack issue
	BASETYPE* pLng = new BASETYPE(devHndl());
	RETURNCODE rc = condEnumValue.resolveCond(pLng);
	if ( rc == SUCCESS )
		systemEnum = (ENUMTYPE) pLng->getDdlLong();
	else
		systemEnum = (ENUMTYPE)MAXVALUE;

	delete pLng;
	return systemEnum;
};


template<class ENUMTYPE, class BASETYPE, int MINVALUE, int MAXVALUE, int MAXSTRLEN>	
void 
hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>
::setSysEnum(ENUMTYPE newVal)
{	BASETYPE Lng(devHndl()); Lng  = NULL;
	RETURNCODE rc;
	if ( condEnumValue.priExprType == aT_Unknown)//aT_Direct
	{
		condEnumValue.setPayload(Lng);
	}
	rc= condEnumValue.resolveCond(&Lng);
	if ( rc != SUCCESS )
	{
		return ;// error condition
	} 
	
	if (newVal >= MINVALUE && newVal <= MAXVALUE)  
			systemEnum = newVal;
	else	systemEnum = (ENUMTYPE)MAXVALUE;
	Lng.setDdlLong((ulong)systemEnum);
};
/* stevev 27sep11 - I need to set the conditional.  The above makes little sense to me 10 years
   on.  It's used and its working so I'm making a similar function that will set the cond and
   systemEnum.
**/
template<class ENUMTYPE, class BASETYPE, int MINVALUE, int MAXVALUE, int MAXSTRLEN>	
void 
hCsystemEnum<ENUMTYPE, BASETYPE, MINVALUE, MAXVALUE, MAXSTRLEN>
::setCondValue(ENUMTYPE newVal)
{	RETURNCODE rc;

	if (newVal >= MINVALUE && newVal <= MAXVALUE)  
		systemEnum = newVal;
	else	
		systemEnum = (ENUMTYPE)MAXVALUE;

	// this always resolves to zero....BASETYPE Lng = (BASETYPE)systemEnum;
	ulong x = (ulong)systemEnum;
	BASETYPE Lng(devHndl()); Lng  = x;
	
	condEnumValue.setPayload(Lng);

	rc= condEnumValue.resolveCond(&Lng);	
};
/*
 * HALFPOINT types.

#define X_VALUE				1
#define Y_VALUE				2
 */

#endif//_SYSENUM_H

/*************************************************************************************************
 *
 *   $History:  $
 * 
 *************************************************************************************************
 */
