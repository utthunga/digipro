/*************************************************************************************************
 *
 * $Workfile: CmdLineInfo.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		CmdLineInfo.h Supplies the OS interface to the device object
 *
 * #include "CmdLineInfo.h"
 */

#ifndef _CMDLINEINFO_H
#define _CMDLINEINFO_H

#include "Arguments.h"
#include "ddbDeviceMgr.h" // for max supported version

#ifdef __GNUC__		// L&T Modifications : LIN_CONS
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
#endif
// timj begin 04/05/04  default polladdr is -1 so that we can detect no -p on cmd line
#define	EMPTY_POLL_ADDR			0xff
// timj end 04/05/04

typedef struct cmdLineInfo_s
{
#ifdef __GNUC__		// L&T Modifications : LIN_CONS
	typedef std::string CString;
#endif

	CString       cl_Url;		// comPortString;
	CString       cl_DbP;        // database path-release dir
	CString		  cl_Lang;
	CString		  cl_HelpDir;	// where lit 18 is kept
	CString         cl_AppDir;    // the directory holding the application
	CString		  cl_Modem;		// Communication Modem /* VMKP added */
	CString       cl_dirName;	// undoc'd - where to put the release dump
	CString		    cl_verString;	// the complete version information string
	// key removed 07feb07 for hart 7  unsigned long cl_Key;
	// mini Identity instead
	BYTE		  cl_Hrev;		// HART revision (5/6/7)*
	WORD		  cl_Mfg;		// manufacturer - 08feb07
	WORD          cl_DevType;	// device type  - 08feb07
	BYTE		  cl_DevRev;	// Device Revision08feb07
	BYTE          cl_PollAddr;  // where we can find our device
	BYTE          cl_MaxEFFver; // where our search will start
	CString		  cl_DevID;		// device unique id

	BYTE          cl_HstAddr;   // Pri/Sec valid values are 1 or 2
	bool          cl_IsSim;
	bool		  cl_IsFcPlt;
	bool		  cl_IsDirDump;
	CString       cl_Restriction;
	bool          cl_UseServer;	// HARTServerFirstChoice::false means use Modem server if it exists
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
	VS_FIXEDFILEINFO cl_VersionInfo;
#else
	// TODO : Equivalent has to be found.
#endif

	 Arguments::optionSOurce_t srcUrl;
	 Arguments::optionSOurce_t srcDbP;
	 Arguments::optionSOurce_t srcLang;
	 Arguments::optionSOurce_t srcHlpDir;
	 Arguments::optionSOurce_t srcModem;
	 Arguments::optionSOurce_t srcPollAddr;
	 Arguments::optionSOurce_t srcHstAddr;
	Arguments::optionSOurce_t srcKey;	// cl_Mfg & cl_DevType & cl_DevRev
	Arguments::optionSOurce_t srcDevID;
	Arguments::optionSOurce_t srcHrev;
	Arguments::optionSOurce_t srcIsSim;
	Arguments::optionSOurce_t srcFcPlt;
	Arguments::optionSOurce_t srcRestrict;
	Arguments::optionSOurce_t srcUseSrvr;
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
	void clear(void){	
		cl_Url.Empty();	cl_DbP.Empty(); cl_Lang.Empty(); cl_DevID.Empty();
		cl_HelpDir.Empty(); cl_AppDir.Empty(); cl_HstAddr = 2;cl_MaxEFFver = SUPPORTED_VERSION_MAX;
		// timj begin 04/05/04  default polladdr is -1 so that we can detect no -p on cmd line
		cl_Modem.Empty();cl_Mfg = cl_DevType= 0;cl_DevRev=0; cl_PollAddr= EMPTY_POLL_ADDR;	
		// timj end 04/05/04
		cl_Hrev = 5; cl_IsSim   =    cl_IsFcPlt = cl_IsDirDump = false; 
		cl_dirName.Empty(); /* stevev 10/4/4*/
		cl_Restriction = 'S'; /*stevev 28jan05 */cl_verString.Empty();
		srcUrl = srcDbP = srcLang = srcDevID =  srcHlpDir = srcModem = srcKey = srcPollAddr =
		srcHstAddr = srcHrev = srcIsSim = srcFcPlt = srcRestrict =Arguments::src_initVal;/* end 10/4/4*/
	};
#else
	void clear(void){	
		cl_Url.empty();	cl_DbP.empty(); cl_Lang.empty(); cl_DevID.empty();
		cl_HelpDir.empty(); cl_AppDir.empty(); cl_HstAddr = 2;
		// timj begin 04/05/04  default polladdr is -1 so that we can detect no -p on cmd line
		cl_Modem.empty();cl_Mfg = cl_DevType= 0;cl_DevRev=0; cl_PollAddr= EMPTY_POLL_ADDR;	
		// timj end 04/05/04
		cl_Hrev = 5; cl_IsSim   =    cl_IsFcPlt = cl_IsDirDump = false; 
		cl_dirName.empty(); /* stevev 10/4/4*/
		cl_Restriction = 'S'; /*stevev 28jan05 */cl_verString.empty();
		srcUrl = srcDbP = srcLang = srcDevID =  srcHlpDir = srcModem = srcKey = srcPollAddr =
		srcHstAddr = srcHrev = srcIsSim = srcFcPlt = srcRestrict =Arguments::src_initVal;/* end 10/4/4*/
	};
#endif

	struct cmdLineInfo_s&
		operator=(struct cmdLineInfo_s& src){	
				cl_Url   = src.cl_Url;	
#ifdef GE_BUILD	
				cl_DbP      = "Hard Disk" + src.cl_DbP;      // added "Hard Disk" PAW 21/05/09
#else
				cl_DbP   = src.cl_DbP;      
#endif				
				cl_Lang  = src.cl_Lang; 
				cl_DevID = src.cl_DevID;cl_PollAddr = src.cl_PollAddr;	
				cl_Mfg = src.cl_Mfg;cl_DevType = src.cl_DevType;cl_DevRev = src.cl_DevRev;// 08feb07
				cl_Hrev  = src.cl_Hrev; cl_IsSim = src.cl_IsSim;cl_IsFcPlt  = src.cl_IsFcPlt;  
				cl_HelpDir=src.cl_HelpDir;cl_HstAddr = src.cl_HstAddr;cl_AppDir=src.cl_AppDir;
				cl_Modem = src.cl_Modem; srcUrl = src.srcUrl; srcDbP = src.srcDbP; 
				cl_IsDirDump = src.cl_IsDirDump;cl_dirName=src.cl_dirName;cl_Restriction=src.cl_Restriction;
				srcLang = src.srcLang;srcRestrict=src.srcRestrict;cl_verString=src.cl_verString;
				srcDevID = src.srcDevID;srcHlpDir = src.srcHlpDir;srcModem = src.srcModem;
				srcKey = src.srcKey;srcPollAddr = src.srcPollAddr;srcHstAddr = src.srcHstAddr;
				srcHrev = src.srcHrev;srcIsSim = src.srcIsSim;srcFcPlt = src.srcFcPlt;
#ifndef __GNUC__	// L&T Modifications : LIN_CONS
				memcpy(&cl_VersionInfo,&(src.cl_VersionInfo),sizeof(VS_FIXEDFILEINFO));
#else
	// TODO : Equivalent has to be found.
#endif
				return (*this);      };
}
/*typedef*/ cmdLineInfo_t;

#endif
