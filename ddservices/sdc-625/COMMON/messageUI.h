/*************************************************************************************************
 *
 * Workfile: messageUI.h 
 * 02Jan08 - stevev
 *     Revision, Date and Author are not in the file!
 *
 *************************************************************************************************
 * The content of this file is the
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2008 HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		Holds string definitions for the NON-Device Object areas.
 *
 * Component History:
 * 
 *
 * #include "messageUI.h"
 */

#ifndef _MESSAGEUI_H
#define _MESSAGEUI_H

#define M_UI_MT_STR					_T("")

#define M_UI_CONTINUE_QUEST			_T("\nContinue?")
#define M_UI_NO_RESP_CD				_T("No response code defined.")
#define M_UI_NO_RESP_CD_FORMAT		_T("  (Cmd: %d  RC: %d)")
#define M_UI_NO_RESP_CD_4_BIT		_T("Comm Error : No Description Available for bit value: 0x%02X for Command: %d")
#define M_UI_SEARCH_POLL_FAIL		_T("Connection to poll address (search) failed.")
#define M_UI_CONNECT_POLL_FAIL		_T("Connection to poll address %d failed.")
#define M_UI_NO_RESP_CONTINUE_QUEST	_T("No Response to resend after %d seconds. \n  Continue trying? ")
#define M_UI_DELAYED_RESP			_T("Delayed Response")
#define M_UI_COMM_ERROR				_T("Error In Communication with Device, Closing the Device...")

#define M_UI_UNABLE_2_INIT_COM		_T("Unable to initialize communications COM")
#define M_UI_NO_HART_UTIL			_T("Unable to get Hart Utilities dll")
#define M_UI_NO_MODEM_CONN			_T("Unable to get Hart Modem Connection")
#define M_UI_NO_HART_SERVER_CONN	_T("Unable to get Hart Server Connection")
#define M_UI_NO_HART_SELECT			_T("Unable to get Hart Select dll")
#define M_UI_UNK_DLL_EXCEPTION		_T("Unknown DLL exception.")
#define M_UI_NO_TIMER				_T("Cannot install timer.  Timers are being used by other applications")

#define M_UI_ARG_USAGE01	_T("\nUsage: ")

#define M_UI_FORMAT_001		_T("0x%c0%dX")

#define M_UI_NO_SAVE		_T("Saving a Device data snapshot is not yet supported.")
#define M_UI_NO_COMMS		_T("Communications have NOT been instantiated.")
#define M_UI_GENERIC		_T("This should not happen")
#define M_UI_SRC_CHANGED	_T("The DD file has changed on the disk. Reload?   ")

#define M_UI_CLOSE_DEVICE	_T("Close the current device?")
#define M_UI_NO_OPEN_SAVED	_T("Opening a previously saved Device is not yet supported.")
#define M_UI_NO_TABLE_STYLE	_T("The Table Style is not yet supported.")
#define M_UI_BAD_SN			_T("Invalid Serial Number")
#define M_UI_NOT_REGISTERED _T("This application is not registered.   ")
#define M_UI_GENERATING		_T("Generating Device")
#define M_UI_PLEASE_RESTART _T("Please restart the application to implement changes")
#define M_UI_POLLING_RNG	_T("Polling address must be a number from 0 to %d.")

#define M_UI_SCOPE_NO_CWND		_T("Could not instantiate the Cwnd object")
#define M_UI_SCOPE_CHART_FAIL	_T("Failed to instantiate Chart Control")
#define M_UI_SCOPE_STCHART_FAIL	_T("Failed to instantiate Strip Chart Control")
#define M_UI_SCOPE_SCCHART_FAIL	_T("Failed to instantiate Scope Chart Control")
#define M_UI_SCOPE_SWCHART_FAIL	_T("Failed to instantiate Sweep Chart Control")
#define M_UI_SCOPE_ANGUAGE_FAIL	_T("Failed to instantiate Angular Guage Control")
#define M_UI_SCOPE_LNGUAGE_FAIL	_T("Failed to instantiate Linear Guage Control")
#define M_UI_SCOPE_NO_IUNKNOWN	_T("Could not get IUnKnown pointer")
#define M_UI_SCOPE_LEGEND		_T("Legend")

#define M_UI_INVALID_ENTRY	_T("Value out of range.  Please enter a valid value")
#define M_UI_INVALID_TYPE   _T("Edit type is not recognized. Please notify the programmer.")
#define M_UI_NON_ASCII_STR  _T("ASCII type variables require ascii input strings.")
#define M_UI_PARSE_NUM_FAIL _T("The string could not be parsed into a number.")
#define M_UI_INPUT_FAILURE  _T("The input failed for an unknown reason.")


#define M_UI_XMTR_NO_NUMBER		_T("FFFFFFFF")
#define M_UI_XMTR_BAD_INI		_T("INI file may be corrupt,Loading default values")
#define M_UI_XMTR_NO_DB			_T("No database specification found.")
#define M_UI_XMTR_ZERO_ILLEGAL	_T("Zero DD key is illegal.")
#define M_UI_XMTR_DB_NOT_AVAIL	_T("The Database is unavailable.")


#define M_METHOD_ABORTED		_T("Method aborted")
#define M_METHOD_SEND_ERROR	    _T("Exiting Method: Send Error")
#define M_METHOD_RC_ABORT		_T("Aborting method due to response code or device status")

#define M_ARGS_NO_COMMAND		_T("no command name")

 
#endif /*_MESSAGEUI_H*/
