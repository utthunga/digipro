/*************************************************************************************************
 *
 * $Workfile: DDselectSplitter.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 The splitter class for the Xmtr-DD DD selection dialog
 */

#include "stdafx.h"
//#include "XmtrDD.h"
#include "DDselectSplitter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDDselectSplitter dialog

// Initialize of the static variable
WNDPROC CDDselectSplitter::m_MFCWndProc = 0;
CTypedPtrMap<CMapPtrToPtr, HWND, CDDselectSplitter*> CDDselectSplitter::m_staticMap;


CDDselectSplitter::CDDselectSplitter(UINT idd, CWnd* pParent)
	: m_bMovingSplitter( FALSE ), m_infoLeft( NULL ), m_infoRight( NULL ), m_infoDisp( NULL ),
		CDialog(idd, pParent)
{
	//{{AFX_DATA_INIT(CSplitterDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}



CDDselectSplitter::~CDDselectSplitter()
{
	delete m_infoLeft;
	delete m_infoRight;
	delete m_infoDisp;
	m_staticMap.RemoveKey( GetSafeHwnd() );
}


void CDDselectSplitter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDDselectSplitter)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDDselectSplitter, CDialog)
	//{{AFX_MSG_MAP(CDDselectSplitter)
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDDselectSplitter message handlers


void CDDselectSplitter::InitDialog( LPCTRLINFO infoLeft, LPCTRLINFO infoRight, LPCTRLINFO infoDisp, UINT nPerc )
{
	// Set control info
	m_infoLeft	= infoLeft;
	m_infoRight	= infoRight;
	m_infoDisp  = infoDisp;

	CRect rect;
	GetClientRect( &rect );

	m_LeftPercent  = nPerc;

	ReSize(rect.Width(), rect.Height());

	// Changing the window proc
	WNDPROC temp = reinterpret_cast<WNDPROC>(::SetWindowLong( 
		GetSafeHwnd(), GWL_WNDPROC, reinterpret_cast<long>(NewWndProc) ));
	if ( !m_MFCWndProc ) m_MFCWndProc = temp;

	// Add map element of the current instance
	m_staticMap[GetSafeHwnd()] = this;
}

void CDDselectSplitter::OnSize(UINT nType, int cx, int cy) // new width, new height
{
	// filter a too small setting here
	CDialog::OnSize(nType, cx, cy);

	ReSize(cx,cy);
	return;
}

// common sizing routine
void CDDselectSplitter::ReSize(int cx, int cy) // new width, new height
{
	// If the controls are not setted the function quit
	if ( !m_infoRight || !m_infoLeft || !m_infoDisp ) return;

	int nPixelPerc = int(cx*m_LeftPercent/100);
	int iTop, iBot;

	// Change the controls position and dimension
	SetCtrl( 
		m_infoLeft->m_pWnd, 
		m_rectLeft,				// return value
		m_infoLeft->m_nOffX,									// left end of left cntrl
		(m_infoLeft->m_nHowY == TOP) ? m_infoLeft->m_nOffY : m_infoLeft->m_nOffX,   // top calcs
		nPixelPerc - SPLITTERWIDTH - m_infoLeft->m_nOffX,  // width calc
		cy - m_infoLeft->m_nOffY -	m_infoLeft->m_nOffX       // height calc,, 
		//  total - both offsets use the x offset for the non spec'd y 
	);
			//m_infoLeft->m_pWnd, m_rectLeft, 
			//m_rectLeft.left,    m_rectLeft.top,
			//m_rectLeft.right,   cy - m_infoLeft->m_nOffY 
			//);
//	SetCtrl( m_infoRight->m_pWnd, m_rectRight, m_rectRight.left, m_rectRight.top, 
//		cx - m_rectSplitter.right - m_infoRight->m_nOffX, cy - m_infoRight->m_nOffY );
	iTop = (m_infoRight->m_nHowY == TOP) ? m_infoRight->m_nOffY : m_infoRight->m_nOffX; // top
	iBot = cy - m_infoRight->m_nOffY  - m_infoRight->m_nOffX; // height
	SetCtrl( 
		m_infoRight->m_pWnd, 
		m_rectRight, 				// return value
		nPixelPerc + SPLITTERWIDTH,                             // left
		iTop,                                                   // top
		cx - nPixelPerc - SPLITTERWIDTH - m_infoRight->m_nOffX, // width
		iBot                                                    // height
	);

	// Adjust the splitter height
	//m_rectSplitter.bottom = cy;
	//                              left                top   right                    bot
	m_rectSplitter.SetRect( nPixelPerc - SPLITTERWIDTH, iTop, nPixelPerc + SPLITTERWIDTH, iBot );
}


void CDDselectSplitter::OnMouseMove(UINT nFlags, CPoint point) 
{
	CClientDC dc( this );
	dc.DPtoLP( &point );

	// If the point is over the splitter change the cursor
	if ( m_rectSplitter.PtInRect( point ) )
		SetCursor( LoadCursor( NULL, MAKEINTRESOURCE(IDC_SIZEWE) ) );

	// If we moving the splitter move the controls
	if ( m_bMovingSplitter )
		MoveCtrls( point.x );
	
	CDialog::OnMouseMove(nFlags, point);
}

void CDDselectSplitter::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CClientDC dc( this );
	dc.DPtoLP( &point );

	// If the point is over the splitter ...
	if ( m_rectSplitter.PtInRect( point ) )
	{
		// ... capture the mouse, change the cursor and set the moving splitter flag
		SetCapture();
		SetCursor( LoadCursor( NULL, MAKEINTRESOURCE(IDC_SIZEWE) ) );
		m_bMovingSplitter = TRUE;
	}
	
	CDialog::OnLButtonDown(nFlags, point);
}

void CDDselectSplitter::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// If we are don't moving the splitter quit
	if ( !m_bMovingSplitter ) return;
	
	CClientDC dc( this );
	dc.DPtoLP( &point );

	// Move the controls to the new position
	MoveCtrls( point.x );
	// Reset the moving splitter falg
	m_bMovingSplitter = FALSE;
	// Release the mouse
	ReleaseCapture();
	
	CDialog::OnLButtonUp(nFlags, point);
}

void CDDselectSplitter::MoveCtrls( int point )
{
	CRect rect;
	GetClientRect( &rect );

	// Verify that the new size of the controls are correct
	if ( point < m_infoLeft->m_sizeMin.cx + (int)m_infoLeft->m_nOffX ) 
		point = m_infoLeft->m_sizeMin.cx + m_infoLeft->m_nOffX;
	if ( point > rect.Width() - m_infoRight->m_sizeMin.cx - (int)m_infoRight->m_nOffX ) 
		point = rect.Width() - m_infoRight->m_sizeMin.cx - m_infoRight->m_nOffX;

	// Change the splitter bound
	m_rectSplitter.SetRect( point-SPLITTERWIDTH, m_rectSplitter.top, point+SPLITTERWIDTH, m_rectSplitter.bottom );

	// Change the position and dimension of the controls
	SetCtrl( m_infoLeft->m_pWnd, m_rectLeft, m_rectLeft.left, m_rectLeft.top, 
		point - SPLITTERWIDTH - m_infoLeft->m_nOffX, m_rectLeft.bottom );
	SetCtrl( m_infoRight->m_pWnd, m_rectRight, point + SPLITTERWIDTH, m_rectRight.top, 
		rect.Width() - point - SPLITTERWIDTH - m_infoRight->m_nOffX, m_rectRight.bottom );
}

void CDDselectSplitter::SetCtrl( CWnd* pWnd, CRect& rect, int left, int top, int width, int height )
{
	// Resize and move the control
	pWnd->MoveWindow( left, top, width, height );
	// Reset the control bounding rect
	rect.SetRect( left, top, width, height );
}

LRESULT CALLBACK CDDselectSplitter::NewWndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// If the message is WM_SIZING ...
	if ( message == WM_SIZING )
	{
		CDDselectSplitter* pDlg;
		m_staticMap.Lookup( hWnd, pDlg );
		if ( !pDlg ) return FALSE;

		// Verify that the controls size are correct: if are not correct set the value
		// to make it correct
		LPRECT rect = (LPRECT)lParam;
		if ( rect->right - rect->left - pDlg->m_rectSplitter.right - 
			(int)pDlg->m_infoRight->m_nOffX - 8 < pDlg->m_infoRight->m_sizeMin.cx )
			rect->right = pDlg->m_infoRight->m_sizeMin.cx + rect->left + 
			pDlg->m_rectSplitter.right + pDlg->m_infoRight->m_nOffX + 8;

		int max = max( 
			pDlg->m_infoLeft->m_sizeMin.cy + pDlg->m_infoLeft->m_nOffY, 
			pDlg->m_infoRight->m_sizeMin.cy + pDlg->m_infoRight->m_nOffY);
		if ( rect->bottom - rect->top - 8 < max )
			rect->bottom = max + rect->top + 8;

		// Quit function without call the standard MFC window proc
		return FALSE;

	}

	// Call the standard MFC window proc
	return ::CallWindowProc( m_MFCWndProc, hWnd, message, wParam, lParam );
}


/*************************************************************************************************
 *
 *   $History: DDselectSplitter.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */