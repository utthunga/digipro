#ifndef DICTIONARY_H
#define DICTIONARY_H

#include "DDlConditional.h"// includes--"Attributes.h"
#include "Table.h"
#include "Char.h"
#include <string>
#include <map>

//#define	MAX_DICT_TABLE_SIZE		8000 //Vibhor 140404 Changing to 8000 from10000
//#define		MAX_DICT_TABLE_SIZE		10000 //stevev 20aug07 Changing to 10000 from 8000(these are going to grow!)
										 // finding this type of error is difficult - please give a reason 
										 // for the change when a number like this is modified.
#define		MAX_DICT_TABLE_SIZE		16384	// stevev 28apr08 - got a 'dictionary too big' error

#define TOK_ERROR 0
#define TOK_DONE 1
#define TOK_NUMBER 2
#define TOK_NAME 3
#define TOK_TEXT 4
#define TOK_COMMA 5

#define DEF__LANG__CTRY		_T("|en|")	/* Of the form "|LL|" or "|LL CC|" */
#define DEF_LANG_CTRY		   "|en|"	/* Of the form "|LL|" or "|LL CC|" */

#define COUNTRY_CODE_MARK 	_T('|')

typedef map<string,int> nameLookup_t;

class CDictionary
{
// L&T Modifications : LanguageChange - start
/* The preprocesser LANGUAGECHANGE should be defined for
	enabling multiple language support. If this feature is
	not required then, LANGUAGECHANGE is undefined.
	This is not possible in case of SDC625 GUI and avialabel
	only for Console application. */
// If Windows
#ifndef __GNUC__
// If SDC625 GUI
#ifndef _CONSOLE
#undef LANGUAGECHANGE
// If SDC-Console
#else //_CONSOLE
//Define if Multiple languange support is required in Windows console
#undef LANGUAGECHANGE
#endif //_CONSOLE
// If Linux
#else //__GNUC__
//Define if Multiple languange support is required in Linux console
#undef LANGUAGECHANGE
#endif //__GNUC__
// L&T Modifications : LanguageChange - end

	//MODIFIED: Deepak changed size to 10
// L&T Modifications : LanguageChange - start
/* The data member languageCode is used from the SDCW class
	for updating the language used by the user during multiple
	lanuage support. Hence this data member is made public in
	order to access it from the SDCW class. */
#ifndef __GNUC__
// If Windows
#ifndef _CONSOLE
// SDC625 GUI
/* In order to access the below data member from ddbVar.cpp,
	it is made as public. */
public:
	wchar_t languageCode[10];		// timj 7jan08
private:
#else //_CONSOLE
#ifndef LANGUAGECHANGE
	wchar_t languageCode[10];		// timj 7jan08
#else //LANGUAGECHANGE
public:
	wchar_t languageCode[10];		// timj 7jan08
private:
#endif //LANGUAGECHANGE
#endif //_CONSOLE
// If Linux
#else //__GNUC__
#ifndef LANGUAGECHANGE
/* In order to access the below data member from ddbVar.cpp,
	it is made as public. */
public:
	wchar_t languageCode[10];		// timj 7jan08
private:
#else //LANGUAGECHANGE
public:
	wchar_t languageCode[10];		// timj 7jan08
private:
#endif //LANGUAGECHANGE
#endif //__GNUC__
// L&T Modifications : LanguageChange - end

#ifdef DIRECT_DICT_REFERENCE
	nameLookup_t        nameMap;	// stevev 20sep05 - map the name string to index 4 methods
#endif


#if 1    // stevev 11feb10 - move is disallowed at check-in, destination is shown
// PAW 02/06/09 place these outside the class as new and malloc are tramping all over them ! */
	struct DICT_TABLE_ENTRY{
		unsigned long	 ref;
		char			*name;		// timj 13dec07
		unsigned short	 len;
		char			*str;
		bool			used;		// timj 14jan08
		};
	DICT_TABLE_ENTRY 	dict_table[MAX_DICT_TABLE_SIZE]; /*This array holds the dictionary*/
	
	char *pchDictData;
	int iIndex;

	unsigned long 		num_dict_table_entries;
#endif

	void dict_table_install (unsigned long ref, char *name, char *str);
	

	void dicterr(char *msg);

	void inschar(int c);

	int comment();

	int lex();

	void parse();

	static int compdict (const void *dict_ent1,const void *dict_ent2);
	
	static int dict_compare(void const *ptr_a, void const *ptr_b);

	static char* strdup(const char* instr);

public:	

	int makedict(char *file,char **addnl_file_array);
	int makedict(DICT_REF_TBL *dict_ref_tbl);

	void dumpdict(bool fm6);
	
	/*This version to be used by DD_Parser*/
	int get_dictionary_string(unsigned long index, ddpSTRING *str);
	
	/*This one to be used by Builtin Library & SDC*/
	int get_dictionary_string(unsigned long index, wstring& str);
#ifdef DIRECT_DICT_REFERENCE
	int get_dictionary_string(string        index, string& str);
#endif	
 	int get_string_translation(const wstring &instr, wstring &outstr);
 	int get_string_translation(wchar_t *string,wchar_t *outbuf,int outbuf_size);
	int get_string_translation(char *string,char *outbuf,int outbuf_size);
	
	CDictionary(const char *pchLangCode);
	CDictionary(const CDictionary & dict);
	~CDictionary();
// PAW start 09/04/09
void *binary_search(const void *key, const char/*void*/ *base,size_t nmemb, size_t size, int (*compar)(const void *, const void *));
// PAW end
#ifdef DICT_REF_DEBUG
// this is for pre-UTF8 debugging
bool dict_ok();// false on problem
bool isDictPtr(char* t);
#endif
};

#endif/*DICTIONARY_H*/
