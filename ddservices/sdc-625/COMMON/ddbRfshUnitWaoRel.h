/*************************************************************************************************
 *
 * $Workfile: $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		all of the Relations classes
 * #include "ddbRfshUnitWaoRel.h"
 */


#ifndef _DDB_RFSH_UNIT_WAO_H
#define _DDB_RFSH_UNIT_WAO_H

#include "pvfc.h"
#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "logging.h"

typedef hCcondList<hCreferenceList>	 condReferenceList_t;

class hCrelation : public hCitemBase  /* currently just for dependent handling */
{
public:
	hCrelation(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual
		~hCrelation(){};

	virtual
	RETURNCODE MarkDependentsStale(void) RPVFC( "hCrelation-0",0 );
	virtual
	bool isIndependent(itemID_t varID)   RPVFC( "hCrelation-1",0 ); // see if varID is in dominant part

	RETURNCODE MarkListStale(varPtrList_t& listOvarPtrs);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};
};

class hCrefresh  : public hCrelation  /* itemType 6 REFRESH_ITYPE  */
{
//	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Crefresh");return NULL;};
	condReferenceList_t watchList;
	condReferenceList_t updateList;
public:
//	Crefresh(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
//	Crefresh(struct itemTbl&  itmR) : CitemBase(itmR) {};
	hCrefresh(DevInfcHandle_t h, aCitemBase* paItemBase);
	~hCrefresh();
	RETURNCODE destroy(void);

	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*No accessable attributes*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: refresh Attribute information is not accessable.\n");
		return rV;
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	RETURNCODE Label(wstring& retStr){return baseLabel(NO_LABEL, retStr);};
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(NO_LABEL, l); };

	RETURNCODE getWatchVarPtrs (varPtrList_t& listOvarPtrs);
	RETURNCODE getUpdateVarPtrs(varPtrList_t& listOvarPtrs);

	RETURNCODE MarkDependentsStale(void);

	RETURNCODE getWatchPayLdPtrs(vector<hCreferenceList *>& retList)
	{return(watchList.aquirePayloadPtrList(retList));};// don't need the dependents at this time
	bool isIndependent(itemID_t varID);// added 30nov06
};


class hCunitRel  : public hCrelation /* itemType 7 UNIT_ITYPE     */
{
	hCconditional<hCreference>   indepVar;     // cond reference
//	hCcondList<HreferenceList_t> dependentVars;// cond list of reference lists(unit lists)
	condReferenceList_t          dependentVars;
public:
//	CunitRel(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
//	CunitRel(struct itemTbl&  itmR) : CitemBase(itmR) {};
//	hCunitRel(DevInfcHandle_t h, ITEM_TYPE item_type, itemID_t item_id, unsigned long DDkey);
	hCunitRel(DevInfcHandle_t h, aCitemBase* paItemBase);
	~hCunitRel();
	RETURNCODE destroy(void);
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*No accessable attributes*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: Unit Attribute information is not accessable.\n");
		return rV;
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	RETURNCODE Label(wstring& retStr){return baseLabel(NO_LABEL, retStr);};
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(NO_LABEL, l); };

	RETURNCODE getIndependVarPtr(hCVar*& pIndependVar);
	RETURNCODE getDependVarPtrs (varPtrList_t& listOvarPtrs);

	RETURNCODE MarkDependentsStale(void);

	RETURNCODE getIndepdPayLdPtrs(vector<hCreference *>& retList)
				{return(indepVar.aquirePayloadPtrList(retList));};
	RETURNCODE getDependPayLdPtrs(vector<hCreferenceList *>& retList)
				{return(dependentVars.aquirePayloadPtrList(retList));};
	bool isIndependent(itemID_t varID);// added 30nov06
};



class hCwao      : public hCitemBase /* itemType 8 WAO_ITYPE      */
{
//	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Cwao");return NULL;};
	condReferenceList_t  waoList;
public:
//	Cwao(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
//	Cwao(struct itemTbl&  itmR) : CitemBase(itmR) {};
//	hCwao(DevInfcHandle_t h, ITEM_TYPE item_type, itemID_t item_id, unsigned long DDkey);
	hCwao(DevInfcHandle_t h, aCitemBase* paItemBase);
	~hCwao();
	RETURNCODE destroy(void){return (waoList.destroy() | hCitemBase::destroy());};
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*No accessable attributes*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: WAO Attribute information is not accessable.\n");
		return rV;
	};

	RETURNCODE dumpSelf(int indent, char* typeName );
	RETURNCODE Label(wstring& retStr){ return baseLabel(NO_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(NO_LABEL, l); };

	RETURNCODE getWAOVarPtrs (varPtrList_t& listOvarPtrs);// resolved

	RETURNCODE getWaoPayLdPtrs(vector<hCreferenceList *>& retList)// all possible
				{return(waoList.aquirePayloadPtrList(retList));};
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};
};


/*-*-* Supporting classes *-*-*-*-*-*/
// added stevev 30nov06
// this sits in var or axis and supplies information about the Unit Relationships
// it is a list of Unit Relation item numbers that the owner might possibly be a dependent in
class hCunitRelList : public itemIDlist_t
{
	itemID_t ownerID;
	hCdeviceSrvc* pdev;

public:
	hCunitRelList():ownerID(0){};// placeholder
	virtual ~hCunitRelList(){};// ditto

	// initializer
	void       setOwner(itemID_t ownersID, hCdeviceSrvc* pD){ownerID=ownersID;pdev=pD;};

	//implementation
	void       setUnit_Rel(itemID_t unitRelationID);
	hCunitRel* getUnit_Rel(void);   // returns INVALID_ITEM_ID if there is none
		
	wstring    getUnit_Str(void);   // will return empty ("") string in worst case
	RETURNCODE getUnit_String(wstring & str);
	itemID_t   getUnit_VarId(void); // returns INVALID_ITEM_ID if there is none
};

#endif //_DDB_RFSH_UNIT_WAO_H
/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
