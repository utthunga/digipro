#ifndef DEVICELIB_H
#define DEVICELIB_H

/* RDM 5.0.0 */


/* database DeviceLib record/key structure declarations */

struct db_info {
   long next_dbKey;
   short dataBaseSchemaVersion;
   short last_modified_year;
   short last_modified_month;
   short last_modified_day;
   short last_modified_hour;
   short last_modified_minute;
};

struct mfg {
   short mfgNum;
   char mfgName[80];
};

struct dev_type {
   short devType;
   char devTypeName[80];
};

struct dev_rev {
   short devRev;
};

struct dd_rev {
   long DDmasterKey;
   short ddRev;
   char fileName[80];
   char filePath[832];
   char fileDate[24];
   char tokMajorVer;
   char tokMinorVer;
   char tokProfile;
};

struct itemTbl {
   struct {
      long DDkey;
      long itemID;
   } item_Key;
   long itemType;
   long itemSize;
   long itemSubtype;
   long itemAttrMask;
   char itemName[80];
};

struct itemAttrTbl {
   long attrType;
   long attrId;
};

struct referenceEntry {
   short isaRef;
   short refType;
   long refItemID;
   short refSubindex;
   short refItemType;
   short optionType;
   long which;
};

struct expressionRec {
   short exprType;
};

struct expressionElement {
   short exprContent;
   short valType;
   short valSize;
   short vIsValid;
   long intVar;
   double fltVar;
};

struct condList {
   unsigned long expected;
};

struct condDest {
   unsigned long destSet;
};

struct ddlongRec {
   unsigned long uddlng;
};

struct bitStrRec {
   unsigned long bsRbitstringVal;
};

struct ddlStrRec {
   unsigned long strDDKey;
   unsigned long strDDItem;
   unsigned long strEnumVal;
   unsigned long strTag;
   unsigned short strFlags;
};

struct typeType {
   unsigned short ttType;
   unsigned short ttSize;
};

struct enumDesc {
   unsigned long val;
   unsigned long status_class;
   unsigned short evaled;
};

struct outputClass {
   unsigned short ocKind;
   unsigned short ocWhich;
   unsigned short oclass;
};

struct respCodeRec {
   unsigned long rcVal;
   unsigned long rcType;
   unsigned short rcEvaled;
};

struct memberRec {
   unsigned long memName;
   unsigned short memEvaled;
};

struct itmArrElement {
   unsigned long iaindex;
   unsigned short iaevaled;
};

struct methodBlob {
   unsigned long blobLen;
};

struct Blob_1024 {
   char bytes_1024[1000][1];
};

struct dataItemRec {
   unsigned long diType;
   unsigned long diFlags;
   unsigned long diWidth;
   unsigned long diIntConst;
   float diFltConst;
};

struct transactionRec {
   unsigned long transNum;
};

/* record, field and set table entry definitions */

/* File Id Constants */

/* Record Name Constants */
#define DB_INFO 10000
#define MFG 10001
#define DEV_TYPE 10002
#define DEV_REV 10003
#define DD_REV 10004
#define ITEMTBL 10006
#define ITEMATTRTBL 10007
#define REFERENCEENTRY 10008
#define REFERENCELIST 10009
#define EXPRESSIONREC 10010
#define EXPRESSIONELEMENT 10011
#define CONDLIST 10012
#define CONDREC 10013
#define CONDDEST 10014
#define DDLONGREC 10015
#define BITSTRREC 10016
#define DDLSTRREC 10017
#define TYPETYPE 10018
#define ENUMDESC 10019
#define ENUMLIST 10020
#define OUTPUTCLASS 10021
#define MENUITEM 10022
#define MENULIST 10023
#define RESPCODEREC 10024
#define RESPCODELIST 10025
#define EDITDISPLIST 10026
#define WAOLIST 10027
#define UNITLIST 10028
#define MEMBERREC 10029
#define MEMBERLIST 10030
#define ITMARRELEMENT 10031
#define ITEMARRAYELEMENTLIST 10032
#define METHODBLOB 10033
#define BLOB_1024 10034
#define DATAITEMREC 10035
#define DATAITEMLIST 10036
#define TRANSACTIONREC 10037
#define TRANSACTIONLIST 10038

/* Field Name Constants */
#define NEXT_DBKEY 0L
#define DATABASESCHEMAVERSION 1L
#define LAST_MODIFIED_YEAR 2L
#define LAST_MODIFIED_MONTH 3L
#define LAST_MODIFIED_DAY 4L
#define LAST_MODIFIED_HOUR 5L
#define LAST_MODIFIED_MINUTE 6L
#define MFGNUM 1000L
#define MFGNAME 1001L
#define DEVTYPE 2000L
#define DEVTYPENAME 2001L
#define DEVREV 3000L
#define DDMASTERKEY 4000L
#define DDREV 4001L
#define FILENAME 4002L
#define FILEPATH 4003L
#define FILEDATE 4004L
#define TOKMAJORVER 4005L
#define TOKMINORVER 4006L
#define TOKPROFILE 4007L
#define ITEM_KEY 6000L
#define DDKEY 6001L
#define ITEMID 6002L
#define ITEMTYPE 6003L
#define ITEMSIZE 6004L
#define ITEMSUBTYPE 6005L
#define ITEMATTRMASK 6006L
#define ITEMNAME 6007L
#define ATTRTYPE 7000L
#define ATTRID 7001L
#define ISAREF 8000L
#define REFTYPE 8001L
#define REFITEMID 8002L
#define REFSUBINDEX 8003L
#define REFITEMTYPE 8004L
#define OPTIONTYPE 8005L
#define WHICH 8006L
#define EXPRTYPE 10000L
#define EXPRCONTENT 11000L
#define VALTYPE 11001L
#define VALSIZE 11002L
#define VISVALID 11003L
#define INTVAR 11004L
#define FLTVAR 11005L
#define EXPECTED 12000L
#define DESTSET 14000L
#define UDDLNG 15000L
#define BSRBITSTRINGVAL 16000L
#define STRDDKEY 17000L
#define STRDDITEM 17001L
#define STRENUMVAL 17002L
#define STRTAG 17003L
#define STRFLAGS 17004L
#define TTTYPE 18000L
#define TTSIZE 18001L
#define VAL 19000L
#define STATUS_CLASS 19001L
#define EVALED 19002L
#define OCKIND 21000L
#define OCWHICH 21001L
#define OCLASS 21002L
#define RCVAL 24000L
#define RCTYPE 24001L
#define RCEVALED 24002L
#define MEMNAME 29000L
#define MEMEVALED 29001L
#define IAINDEX 31000L
#define IAEVALED 31001L
#define BLOBLEN 33000L
#define BYTES_1024 34000L
#define DITYPE 35000L
#define DIFLAGS 35001L
#define DIWIDTH 35002L
#define DIINTCONST 35003L
#define DIFLTCONST 35004L
#define TRANSNUM 37000L

/* Set Name Constants */
#define MFGS 20000
#define MFG2DEVTYPE 20001
#define DEVTYPE2DEVREV 20002
#define DEVREV2DDREV 20003
#define DDREV2ITEM 20004
#define ITEM2ITEMATTR 20005
#define REF2REF 20006
#define REFLIST2REF 20007
#define EXPRREC2ELEMENTS 20008
#define EXPRELEMENT2REF 20009
#define REF2EXPR 20010
#define CLIST2COND 20011
#define COND2EXPR 20012
#define COND2DEST 20013
#define DEST2EXPR 20014
#define DEST2COND 20015
#define DEST2CONDLIST 20016
#define ATTR2COND 20017
#define ATTR2CONDLIST 20018
#define ATTR2SECONDCONDLIST 20019
#define DDLSTR2REF 20020
#define COND2PAYTYPETYPE 20021
#define TYPE2CONDLIST 20022
#define ELIST2EDESC 20023
#define ENUM2OCLASSLIST 20024
#define ENUM2BITSTR 20025
#define ENUM2DESC 20026
#define ENUM2HELP 20027
#define ENUM2METH 20028
#define MITEM2BITSTR 20029
#define MITEM2REF 20030
#define MLIST2MITEM 20031
#define RREC2DESCSTR 20032
#define RREC2HELPSTR 20033
#define RLIST2RREC 20034
#define REDDISP2RREF 20035
#define RWAO2RREF 20036
#define RUNIT2RREF 20037
#define MEM2DESCSTR 20038
#define MEM2HELPSTR 20039
#define MEM2REF 20040
#define MEMLIST 20041
#define IAELEM2DESCSTR 20042
#define IAELEM2HELPSTR 20043
#define IAELEM2REF 20044
#define IAELEMTLIST 20045
#define METH2BLOB 20046
#define ATTR2METHBLOB 20047
#define DIREC2REF 20048
#define DILIST2DIREC 20049
#define TRREC2REQLST 20050
#define TRREC2REPLST 20051
#define TRREC2RESPCDS 20052
#define TRLIST2TRREC 20053
#define COND2TRLIST 20054
#define COND2DILIST 20055
#define COND2RCLIST 20056
#define COND2PAYMLIST 20057
#define COND2PAYENUMLIST 20058
#define COND2PAYDDLONG 20059
#define COND2PAYDDLSTR 20060
#define COND2PAYDDLBSTR 20061
#define COND2PAYEDDISPLIST 20062
#define COND2PAYELIST 20063
#define COND2PAYMEMLIST 20064
#define COND2PAYWAOLIST 20065
#define COND2PAYUNITLIST 20066
#define COND2PAYREFERENCE 20067
#define COND2PAYREFLIST 20068

/* Field Sizes */
#define SIZEOF_NEXT_DBKEY 4
#define SIZEOF_DATABASESCHEMAVERSION 2
#define SIZEOF_LAST_MODIFIED_YEAR 2
#define SIZEOF_LAST_MODIFIED_MONTH 2
#define SIZEOF_LAST_MODIFIED_DAY 2
#define SIZEOF_LAST_MODIFIED_HOUR 2
#define SIZEOF_LAST_MODIFIED_MINUTE 2
#define SIZEOF_MFGNUM 2
#define SIZEOF_MFGNAME 80
#define SIZEOF_DEVTYPE 2
#define SIZEOF_DEVTYPENAME 80
#define SIZEOF_DEVREV 2
#define SIZEOF_DDMASTERKEY 4
#define SIZEOF_DDREV 2
#define SIZEOF_FILENAME 80
#define SIZEOF_FILEPATH 832
#define SIZEOF_FILEDATE 24
#define SIZEOF_TOKMAJORVER 1
#define SIZEOF_TOKMINORVER 1
#define SIZEOF_TOKPROFILE 1
#define SIZEOF_DDKEY 4
#define SIZEOF_ITEMID 4
#define SIZEOF_ITEMTYPE 4
#define SIZEOF_ITEMSIZE 4
#define SIZEOF_ITEMSUBTYPE 4
#define SIZEOF_ITEMATTRMASK 4
#define SIZEOF_ITEMNAME 80
#define SIZEOF_ATTRTYPE 4
#define SIZEOF_ATTRID 4
#define SIZEOF_ISAREF 2
#define SIZEOF_REFTYPE 2
#define SIZEOF_REFITEMID 4
#define SIZEOF_REFSUBINDEX 2
#define SIZEOF_REFITEMTYPE 2
#define SIZEOF_OPTIONTYPE 2
#define SIZEOF_WHICH 4
#define SIZEOF_EXPRTYPE 2
#define SIZEOF_EXPRCONTENT 2
#define SIZEOF_VALTYPE 2
#define SIZEOF_VALSIZE 2
#define SIZEOF_VISVALID 2
#define SIZEOF_INTVAR 4
#define SIZEOF_FLTVAR 8
#define SIZEOF_EXPECTED 4
#define SIZEOF_DESTSET 4
#define SIZEOF_UDDLNG 4
#define SIZEOF_BSRBITSTRINGVAL 4
#define SIZEOF_STRDDKEY 4
#define SIZEOF_STRDDITEM 4
#define SIZEOF_STRENUMVAL 4
#define SIZEOF_STRTAG 4
#define SIZEOF_STRFLAGS 2
#define SIZEOF_TTTYPE 2
#define SIZEOF_TTSIZE 2
#define SIZEOF_VAL 4
#define SIZEOF_STATUS_CLASS 4
#define SIZEOF_EVALED 2
#define SIZEOF_OCKIND 2
#define SIZEOF_OCWHICH 2
#define SIZEOF_OCLASS 2
#define SIZEOF_RCVAL 4
#define SIZEOF_RCTYPE 4
#define SIZEOF_RCEVALED 2
#define SIZEOF_MEMNAME 4
#define SIZEOF_MEMEVALED 2
#define SIZEOF_IAINDEX 4
#define SIZEOF_IAEVALED 2
#define SIZEOF_BLOBLEN 4
#define SIZEOF_BYTES_1024 1000
#define SIZEOF_DITYPE 4
#define SIZEOF_DIFLAGS 4
#define SIZEOF_DIWIDTH 4
#define SIZEOF_DIINTCONST 4
#define SIZEOF_DIFLTCONST 4
#define SIZEOF_TRANSNUM 4

#endif    /* DEVICELIB_H */
