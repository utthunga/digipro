/*********************************************************************************************
 *
 * $Workfile: ddbMenuLayout.h $
 * 20Oct06 - stevev	generated
 *
 *********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002-2006 HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *		Information/constanstants needed to layout a menu screen.
 *		This is supposed to hold all the constants to change how the layout functions
 *
 * #include "ddbMenuLayout.h"
 */

#ifndef _MENULAYOUT_H
#define _MENULAYOUT_H

#ifdef ALLOC_DEBUGGING
#include "CustomAlloc.h" //not in cvs
#endif

/**********  menu errors *******************************************************************/
//  LAYOUT_RETURN_VALUE_BASE is in ddbGeneral
#define LAYOUT_SUCCESS          0
#define LAYOUT_SKIP_BRANCH		(LAYOUT_RETURN_VALUE_BASE  +   1)
#define LAYOUT_RESET_PAGES		(LAYOUT_RETURN_VALUE_BASE  +   2)  
/* the following are layout errors */
#define LAYOUT_PARAM_ERROR		(LAYOUT_RETURN_VALUE_BASE  +  10)

#define ARRANGE_IT  /* when defined, we use devObj to do the layout */
// stevev 13oct10--spec500 rev13 is NOT golden.... #define GOLDENRECTANGLE		/* use height/width ratios that depend on the golden rectangle*/

/*stevev 29mar07 - put here so measure and create uses the same stuff */
// stevev 18mar09 - try multichar again   #define CONST_STRING_FONT	_T("Arial")	// I needed a multi-charset font!!!   _T("MS Sans Serif") /*was _T("Courier New")*/
#define CONST_STRING_FONT	_T("MS Sans Serif")
#define CONST_STRING_HIGT	8                   /* was 15  */

#ifdef _DEBUG
#define LOGBIRTH	/* comment out to stop creation logging */
extern int ldepth;  // global- can only log one birth at a time
#endif

// a menu will have a list of hCmenuItem s
#define COLS_SUPPORTED  5 
#define MINIMUM_CANVAS  3

#define DFLTHGT		16	// scope, medium, number of rows    // stevev  Updated to rev 13 spec 13oct10
#define DFLTWID		COLS_SUPPORTED	// scope, medium, number of columns // stevev  Updated to rev 13 spec 13oct10

#define DFLTHGT_INDX  4 //Vibhor Added 030105 - scope height default 
#define DFLTWID_INDX  4 //Vibhor Added 030105 - scope width  default

#define SEP_SIZE      0 /* separator is 0 X 0 size */
#define STR_SIZE      1 /* constant string is 1 X 1 size */

#define BITSIZE_X		1	/* bit control size - one column */
#define BITSIZE_Y		1   /* bit control size - by one row */

#define BUTSIZE_X		1
#define BUTSIZE_Y		1

#define INLINEIMAGE_X
#define INLINEIMAGE_Y

/* modify these to change the layout according to your screen 
   - be careful: if you change 'em, some dds may start looking odd */
#define TAB_HGT_ROWS	1	/* number of rows the tab-control overhead (tab) takes   */
#define GRP_HGT_ROWS	1	/* number of rows the group overhead (label & box) takes */

/*  moved from Dyn_Controls.h PIXEL UNITS */
#define ROWSTEPSIZE	     14//12 
#define FIRSTROW1		 10
#define FIRSTROW2		 37
#define FIXEDCOL1		 10
#define FIXEDCOL2		 65//120
#define INPUTCOL		  0//150
#define GROWLIMIT		  6
#define COLUMSTEPSIZE    70
#define ROWGAP            6
#ifdef _USE_COL_GAP
#define COLUMNGAP		 12
#else
#define COLUMNGAP		  0
#endif
#define BIT_SIZE         17  // was 18 POB - 31 March 2005
#define GRP_LEFT_ADJ     -3  // was -5 sjv -29nov10
#define GRP_RGHT_ADJ     -3	 // separated from botrght 30nov10-made same as left gap 08jun12
#define GRP_BOTM_ADJ	 -2 
#define TAB_LEFT_ADJ     -8

#define NORMAL_DROP_ROWS  5 // found imperically

//following need to be made into equations using the above defines !!!!/
#define INITIAL_ROWHEIGHT		23	/* first row has top & bottom margins */ 
#define ADDITIONAL_ROWHEIGHT	33	/* other rows are added as-is */

#define COL_WIDTH				333 /* pixels per column */
/* * * * * * * * * end moved * * * * * * * * */

extern const int horizSizes[];
extern const int vert_Sizes[];

#ifndef GOLDENRECTANGLE		// stevev 13oct10 rev 13 spec500 is not golden
	/* map horiz sizes to columns */// stevev  Updated to rev 13 spec500 13oct10
	#define HORIZSCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						DFLTWID, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,\
	/*	gS_Small,			 3	SMALL_DISPSIZE		*/	2,\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	3,\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	4,\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	5,\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	5,\
	/*	gS_Unknown			 8	MAX_SIZE			*/	DFLTWID }

	/* map vertc sizes to rows */// stevev  Updated to rev 13 spec500 13oct10
	#define VERT_SCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						DFLTHGT, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	3, \
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	5,\
	/*	gS_Small,			 3	SMALL_DISPSIZE		*/	7,\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	8,\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	9,\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	11,\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	13,\
	/*	gS_Unknown			 8	MAX_SIZE			*/	DFLTHGT }
	/******************************************************************************/
#else  /* have GOLDENRECTANGLE  */
	#if ( COLS_SUPPORTED == 3 )
	/* map horiz sizes to columns  - stevev - remapped 09jan07 - see notes at end of file*/
	#define HORIZSCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						3, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,/*7*/\
	/*	gS_Small,			 3	SMALL_DISPSIZE		*/	2,\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	2,/*13*/\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	3,\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	3,\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	3,/*20*/\
	/*	gS_Unknown			 8	MAX_SIZE			*/	3 }

	/* map vertc sizes to rows '<<<' indicates that this is a golden ratio (with the same width size)*/
	#define VERT_SCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						19, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	2, /*was  1 before 9jan07*/\
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	4, /*was  3 before 9jan07*/\
	/*	gS_Small,			 3	SMALL_DISPSIZ		*/	6, /*<<< was  7 before 9jan07*/\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	12,/*<<< was 13 before 9jan07*/\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	14,/*was 16 before 9jan07*/\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	17,/*was 18 before 9jan07*/\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	19,/*<<< was 20 before 9jan07*/\
	/*	gS_Unknown			 8	MAX_SIZE			*/	19 }
	#endif

	#if ( COLS_SUPPORTED == 5 )	
	/* map horiz sizes to columns*/
	#define HORIZSCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						5, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,/* 7*/\
	/*	gS_Small,			 3	SMALL_DISPSIZE		*/	2,/*13*/\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	3,/*20*/\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	4,/*26*/\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	5,\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	5,/*33*/\
	/*	gS_Unknown			 8	MAX_SIZE			*/	5 }

	/* map vertc sizes to rows '<<<' indicates that this is a golden ratio (with the same width size) */	
	#define VERT_SCOPESIZES  { \
	/*	gS_Undefined,		 0	*/						31, \
	/*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	3, /*    was 03 before 9jan07*/\
	/*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	6, /*<<< was  7 before 9jan07*/\
	/*	gS_Small,			 3	SMALL_DISPSIZE		*/	12,/*<<< was 13 before 9jan07*/\
	/*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	19,/*<<< was 20 before 9jan07*/\
	/*	gS_Large,			 5	LARGE_DISPSIZE		*/	25,/*<<< was 26 before 9jan07*/\
	/*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	28,/*    was 29 before 9jan07*/\
	/*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	31,/*<<< was 33 before 9jan07*/\
	/*	gS_Unknown			 8	MAX_SIZE			*/	31 }
	#endif
#endif

/*********************************************************************************************
 * classes needed outside of the device object
 *
 ********************************************************************************************/

//helper macro -*- assumes we are copying into a freshly 'newed' structure
#define COPY_QUALIFIER(fr,to) if (fr->IsReadOnly())	to->m_bIsReadOnly = TRUE;/*else leave it false*/\
					if (fr->IsNoLabel() )	to->m_bIsNoLabel  = TRUE;/*else leave it false*/\
					if (fr->IsNoUnit()  )	to->m_bIsNoUnit   = TRUE;/*else leave it false*/\
					if (fr->IsInline()  )	to->m_bIsInline   = TRUE;/*else leave it false*/\
					if (fr->IsDisplayValue())to->m_bIsDisplayVal= TRUE;/*else leave it false*/\
					if (fr->IsReview()  )	to->m_bIsReview   = TRUE;/*else leave it false*/

#define IS_QUALIFIER(a) {if (qualifier.getBitStr() & a) return true;else return false;}

// this is used to facilitate menu layout by keeping all the state together
typedef struct layState_s
{
	hv_point_t    cursor;
	hv_point_t    maxPage;// Size
	hv_point_t     firstPg;
	DDSizeLocType  firstMx; // width of widest page
	DDSizeLocType  currentRowMaxWidth;

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	layState_s() { clear(); };

	void clear(void) {  cursor = hv_point_t(1,1);
		maxPage = firstPg = hv_point_t(0,0);
		firstMx  = currentRowMaxWidth = 0;
	};
}/*typedef */ layState_t;


// lightweight class
 /*define the layout history - The history will determine the restrictions 
*/
typedef struct menuHist_s
{
	bool dlgInPath;		// if we are in a dialog, all must be modal (no windows/tables/menus)
    bool wndInPath;     // if we are in a WINDOW, DIALOG, PAGE, GROUP - No TABLE,POB-6 Aug 08
    					// see notes in bug report 13aug08 on menu states
	bool tblInPath;		// 28aug11 - new spec:only tables in tables

	bool inMethDlg;
	bool grpDepthHit;	// groups only allowed 2 deep
	bool inPopMenu;		// many things are different in a pop-up menu child
	bool inReview;		// stevev- 31aug07 - can't believe its never been here
	menuItemDrawingStyles_t parentStyle;

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	menuHist_s(){clear();};
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	menuHist_s(menuItemDrawingStyles_t pS)
					{inPopMenu=dlgInPath=wndInPath=grpDepthHit=inMethDlg=inReview=false;parentStyle=pS;};
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	menuHist_s(const struct menuHist_s& src){ operator=(src);};//stevev 28feb07 added
	void   clear(void) 
		{dlgInPath=wndInPath=grpDepthHit=inPopMenu=inMethDlg=inReview=false;parentStyle=mds_do_not_use;};
	struct menuHist_s& operator=(const struct menuHist_s& src){dlgInPath=src.dlgInPath;
	wndInPath=src.wndInPath;inMethDlg=src.inMethDlg;grpDepthHit=src.grpDepthHit;inPopMenu=src.inPopMenu;
	inReview=src.inReview;parentStyle=src.parentStyle;  return (*this); };
} // Added "wndInPath" to make sure that TABLE is not invoked when underneath a WINDOW, POB - 6 Aug 2008
/*typedef*/ menuHist_t;



class hCmenu;
class hCmenuTree;
class hCcollection;

// one item in the menu ... NOT a dds item 
class hCmenuItem : public hCobject
{
public:
    // the payload - real data..
	hCreference      itemRef;
// L&T Modifications : ReadNWriteSupport - start
/* The below data member qualifier is made as public, so as to
    access this from SDCW class. And then added keyword private
	to keep the rest private. */

	hCbitString      qualifier;	
private:
// L&T Modifications : ReadNWriteSupport - end
	menuItemDrawingStyles_t	draw_as;
	menuHist_t       myHistory;		// to pass-down to my children


// qualifiers
//#define READ_ONLY_ITEM 					0x01
//#define DISPLAY_VALUE_ITEM 				0x02
//#define REVIEW_ITEM 						0x04
	// eDDL
//#define NO_LABEL_ITEM						0x08
//#define NO_UNIT_ITEM						0x10
//#define INLINE_ITEM						0x20
	
public:
	/* stevev - 24oct06 - added so we don't keep looking up the pointer */
	hCitemBase* pItemBase; // null on constant
#if 1
	/* * * * * * * * * * * * * CmenuItem * * * * * * * * * * * * * * * * * * * * */
//		DDItemBase &item;			// the item on the menu
//
// FOR NOW:
// get a pointer to the actual item and then query it.
//		you must be at a high enough level to know what the device is 
//		(ie an itembase derived class | the device)
// getDevPtr()->procure(MenuItem.getRef(), pointer2returnPointer )  
//		will give you a pointer to an item.  
//		Once you get there, you can test if its dynamic....etc.
/*
		bool IsDynamic() 
		{ 	CitemBase* pItem = NULL;
			if ( FILL_ITEM_PTR(&pItem) == SUCCESS )
			{    return pItem->IsDynamic(); };	// interesting qualifiers.
			else return false;
		};
		
		bool IsReadOnly()
		{ 	CitemBase* pItem = NULL;
			if ( FILL_ITEM_PTR(&pItem) == SUCCESS )
			{  return ( pItem->IsReadOnly() ||  (qualifier & READ_ONLY_ITEM));}
			else
			{  return true;// default?????????????			
			}
		};
*/
// TEMPORARY - use it with the item read only to get the real value
	bool IsReadOnly()         IS_QUALIFIER(READ_ONLY_ITEM);

	bool IsDisplayValue(void) IS_QUALIFIER(DISPLAY_VALUE_ITEM);

	bool IsReview(void)       IS_QUALIFIER(REVIEW_ITEM);// only difference about REVIEW menu is 
												// all items are DISPLAY_VALUE & READ_ONLY_ITEM
	bool IsNoLabel(void)      IS_QUALIFIER(NO_LABEL_ITEM);

	bool IsNoUnit(void)       IS_QUALIFIER(NO_UNIT_ITEM);

	bool IsInline(void)       IS_QUALIFIER(INLINE_ITEM); 
/*		
Removed 9/7/4 by sjv - cleanup
*/
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#endif
	// no default constructor (we MUST have the handle)
	hCmenuItem(DevInfcHandle_t h);
	hCmenuItem(const hCmenuItem& meD);
	virtual ~hCmenuItem();
	RETURNCODE destroy(void);

	hCmenuItem& operator=(const hCmenuItem& meD);
	hCmenuItem& operator=(const hCmenu& meN);// load it from a menu - no code yet..

	void  setEqual(void* pAitem);
	void setDuplicate(hCmenuItem& rMi);


	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	void clear(void)
	{
		qualifier.clear();
		itemRef.clear();
		myHistory.clear();
		pItemBase = NULL; //we don't own this memory
	};

	// accessors
	hCreference& getRef(void) { return itemRef;};
	menuItemDrawingStyles_t getDrawAs(void){ return draw_as;};
	void                    setDrawAs(menuItemDrawingStyles_t dr_style) {draw_as = dr_style;}; //Vibhor 200904: Added
	void         setHistory(menuHist_t s) { myHistory = s; };
	menuHist_t   getHistory(void)         { return myHistory; };
	/* stevev 30mar05 - adding collections as groups...*/
	void         setReference(hCreference& ref){itemRef=ref;};
	void         fillDrawType(hCitemBase* pItemBase, const menuHist_t& history);
	void         setQualifier(hCbitString& q){qualifier=q;};
	hCbitString& getQualifier(void){return(qualifier);};

	friend class hCmenuTree;
};

// a menuList is the destination of the conditionals
//             is a list of lists/items
// menu has 
//		list of conditionals 
//			each conditional resolves to a list of menu lists <CmenuList>
//				which contain a list of items <CmenuItem>
//
//

#ifdef ALLOC_DEBUGGING
typedef vector<hCmenuItem, CustomAlloc<hCmenuItem> >       menuItemList_t;
#else
typedef vector<hCmenuItem>       menuItemList_t;
#endif
typedef menuItemList_t::iterator menuItmLstIT_t;


/************************************************************************************************/
// test extraction to common struct


class hCmenuTree;

typedef vector<hCmenuTree*>         menuTreePtrList_t;
typedef menuTreePtrList_t::iterator menuTrePtrLstIT_t;

class hCmenuTree : public hCmenuItem, public leafData_s
{
public:
	void setInternalCanvas(DDSizeLocType cSize);// this one is re-entrant for all sub-menus

public:
		/* stevev 20oct10 - added rowSize...see notes at the end of this file       */
		/* stevev 20oct06 - added for device-object internal layout                 */
	/* ALL of these are in unit-block coordinates w/ 1,1 being the top left corner */
	/* a unit-block is the size of a float VARIABLE with label and unit */
	/* the UI must translate these to pixels with canvas, frames & titles */
	int              idx;		// normally not used - specifically for bitenum
/* temp try extraction to common struct----------------
	hv_point_t       topleft;
	hv_point_t		 hv_size;  // the horiz-vert size
	hv_point_t       da_size;  // the draw-as size, different only in pages
	DDSizeLocType    rowSize;  // stevev 20oct10-width of our row  (in unit-blocks)
	DDSizeLocType    canvasSize;
********/
	hCmenuTree*      pParentTree; // stevev 11dec06-we gotta be able to replace self at update

	menuTreePtrList_t   branchList;// list of hCmenuTrees in this menu/collection item, 
								   // empty for simple types (eg FLOAT)
	/// data in hCmenuItem //////////////	
	//hCreference      itemRef;
	//hCbitString      qualifier;	
	//menuItemDrawingStyles_t	draw_as;
	//menuHist_t       myHistory;	
	// +
	//hCitemBase* pItemBase; // null on constant
	static int  canvas_EntryCnt;

public:
	hCmenuTree(DevInfcHandle_t h):hCmenuItem(h),idx(0), pParentTree(NULL)
	{	branchList.clear();		}; //leafdata clears in ctor
	// partial copy constuctor
	hCmenuTree(hCmenuItem* pMI):hCmenuItem(*pMI),idx(0), pParentTree(NULL) 
	{	branchList.clear();		}; //leafdata clears in ctor
	hCmenuTree(const hCmenuTree& mT):hCmenuItem(mT.devHndl())
	{  operator=(mT);  };
	virtual
		~hCmenuTree() {topleft.clear(); hv_size.clear();/*this clears down the tree::clear();*/
		if(branchList.size()) branchList.clear();  };

	void destroy(void) // remove memory pointed to in the list
	{for(menuTrePtrLstIT_t it=branchList.begin();it!=branchList.end();++it)
	{ if ((*it) != NULL){(*it)->destroy();delete (*it);(*it) = NULL;}  }; branchList.clear();};

	void clear(void)// NOTICE THAT THIS DOES NOT DESTROY!! you may want to do that first
	{for(menuTrePtrLstIT_t it=branchList.begin();it!=branchList.end();++it)
		{  if ( (*it) != NULL ){ (*it)->clear(); (*it)=NULL;}/*else-skip*/};
		 branchList.clear();idx=0;topleft.clear();hv_size.clear();
		 da_size.clear();pParentTree=NULL;hCmenuItem::clear();rowSize=canvasSize=0;};

	hCmenuTree& operator=(const hCmenu& meN);
	hCmenuTree& operator=(const hCmenuItem& meD) { hCmenuItem::operator =(meD);return *this;};
	hCmenuTree& operator=(const leafData_s& ldS) { leafData_s::operator =(ldS);return *this;};
	hCmenuTree& operator=(const hCmenuTree& meD)
	{	hCmenuItem::operator =((hCmenuItem)meD);
		idx       = meD.idx;
		topleft   = meD.topleft;
		hv_size   = meD.hv_size;
		da_size   = meD.da_size;
		rowSize   = meD.rowSize;
		canvasSize= meD.canvasSize;
		//branchList= meD.branchList;
		menuTreePtrList_t* pl = (menuTreePtrList_t*)&(meD.branchList);
		for(menuTrePtrLstIT_t it = pl->begin(); it != pl->end(); ++it)
		{	hCmenuTree* p = (hCmenuTree*)(*it);  branchList.push_back(p);// just copy pointer
		}
		pParentTree=meD.pParentTree;
		return *this;
	};
	//hCmenuTree& operator=(const hCmenu& meN){ hCmenuItem::operator=(meN); branchList.clear();
	//return *this;
	//};
	void setRowSize(unsigned w);// sub menues must be set as well
	void setCanvas(void)  {  setInternalCanvas(canvasSize);   };
	// helpers
	void setPreviousPages(hv_point_t sz); // set last page and previous to size passed in
	hCmenuTree& fill4Collection(hCcollection& coll,const menuHist_t& hist);//similar to operation=
	hCmenuTree& fill4Menu(hCmenu& menu,const menuHist_t& hist);
	void replace(hCmenuTree* p2B_removed, hCmenuTree* p2Replacemant);


	void dumpSelf(int indent=0,char* typeName = NULL);

};

/*  the golden rectangle has a ratio of 1:1.618033989
assuming 333 pixels per column and 33 pixels per row
and we want the box to be normally wider than tall, we have

			at ratio of pixels
col pixels  height:pixels	height:rows		rounded to row count
1	333		205.8053183		 6.236524796	6
2	666		411.6106365		12.47304959		12
3	999		617.4159548		18.70957439		19
4	1332	823.221273		24.94609918		25
5	1665	1029.026591		31.18262398		31

So the vertical values are set to these as the largest size.
The 'equal column counts' smaller than this are interpolated to the next smallest size.

===============================================================================================
rowSize is set to the number of columns used on the row we are in.
When this item is to be painted, the 'canvas' row size must be known.
At that point the pixel width of a column in this row will be known: 
(canvasSize/rowSize)* pixelsPerStdColumn => pixelsPerRowColumn (in this row)

*/
#endif // _MENULAYOUT_H
