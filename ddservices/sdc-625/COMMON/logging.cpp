/************************************************************************************************
 *
 * $Workfile: logging.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 ************************************************************************************************
 * This file was generated from the donated 'errorlog.cpp' file.
 * It is now
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003-2008, HART Communication Foundation, All Rights Reserved
 * with complete rights to the donated errorlog.cpp file remaining with the originator.
 ************************************************************************************************
 *
 * Description:
 *		       implementation of the logging support functions
 * Major modifications:
 *			removed ISDLL and IS_LIBR 22aug08
 *			Removed files.  24oct08 Wide wclog and wcout streams which are always LOGIT examples 
 *				will be converted to narrow and output there.  cerr will be converted in the code
 *				to wcerr everywhere and cerr will be redirected to the bit bucket. SO all 
 *				LOGIT(CERR_LOG will go to a wide character stream.  All LOGIT(COUT_LOG|CLOG_LOG 
 *				will go to a narrow character stream into a file. Status and UI log streams will 
 *				remain wide only.	
 */

#pragma warning (disable : 4786)
#ifdef _IS_STATIC
	#define FMA_PARSER_LOG
#endif

/* --- Standard Includes ------------------------------------------------- */
//#include <windows.h>
#ifndef FMA_PARSER_LOG
#include "stdafx.h"
#endif

#ifndef _WIN32_WCE
#include <time.h>
#include <io.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
/* --- Module Includes ------------------------------------------------- */

#ifndef SKEL
 #ifndef XMTRDD
	#ifndef TOK
      #ifndef FMA_PARSER_LOG
        #include "ddbDevice.h"
      #endif
	#endif
 #endif
 #ifndef FMA_PARSER_LOG
   #include"resource.h"
 #endif
#endif

#if !defined(TOK) && !defined(_CONSOLE)
  #ifndef FMA_PARSER_LOG
    #include "args.h"
  #endif
#endif


#include "logging.h"
#include <list>
#include < fstream >
#include < map >

using namespace std;
#pragma warning (disable : 4786)

//#include "errorDesc.h"
#ifndef _WIN32_WCE
#include <sys/timeb.h>
#else
#include "time_CE.h"	// PAW 30/04/09 see above
#endif

#include "Char.h"

#ifdef TOK
#include "err_gen.h"
#endif

#include "shared/shared_logging.h"

/* --- Module Definitions ------------------------------------------------- */
#ifndef RAZE
#define RAZE(p)		  if((p)!=NULL){ delete (p); (p) = NULL;}
#define RAZE_ARRAY(p) if((p)!=NULL){ delete[] (p); (p) = NULL;}
#endif

/* The maximum size of the log file in Mega Bytes */
#define FILE_SIZE 3
#define MB (1024*1024)

/* Size of the trace string the module will hold in BYTEs */
#define TRACE_SIZE	20000
#define STATUS_LEN  1000


#ifdef IS_SDC
#include "MainFrm.h"
 #pragma message("Is_SDC log message")
 #define	ERROR_MESSAGE_CAPTION	"SDC-625"
#define  WERROR_MESSAGE_CAPTION		_T(ERROR_MESSAGE_CAPTION)
 #define ERROR_LOG_MAIN_NAME     "SDC-625.log"
 #define ERROR_LOG_BACK_NAME     "SDC-625.bak"

CMainFrame* mainAppWindow = NULL;

#define UPDATE_STATUS(a)		mainAppWindow->UpdateBar( a )
#define GET_UPDATEE(t)			mainAppWindow = ( t *) ((AfxGetApp()->m_pMainWnd))  
																		/*not-AfxGetMainWnd()*/
#else /* NOT IS_SDC */

#ifndef TOK  /* XMTR?? FMA_PARSER*/
 #ifdef FMA_PARSER_LOG
  #pragma message("Is_Reader log message")
  #define	ERROR_MESSAGE_CAPTION	"DDreader"
	#define  WERROR_MESSAGE_CAPTION		_T(ERROR_MESSAGE_CAPTION)
	  #define ERROR_LOG_MAIN_NAME     "DDreader.log"
	  #define ERROR_LOG_BACK_NAME     "DDreader.bak"
 #define ERR_FILE_WW L"_RdErrW.log"
 #define ERR_FILE_W   "_RdErrW.log"
 #define ERR_FILE     "_RdErr.log"
//
 #define LOG_FILE_WW L"_RdLogW.log"
 #define LOG_FILE_W   "_RdLogW.log"
 #define LOG_FILE     "_RdLog.log"
//
 #define OUT_FILE_WW L"_RdOutW.log"
 #define OUT_FILE_W   "_RdOutW.log"
 #define OUT_FILE     "_RdOut.log"

 #else /* gotta be xmtr */
  #pragma message("Is_XMTR log message")
  #define	ERROR_MESSAGE_CAPTION	"Xmtr-DD"
	#define  WERROR_MESSAGE_CAPTION		_T(ERROR_MESSAGE_CAPTION)
	  #define ERROR_LOG_MAIN_NAME     "Xmtr-DD.log"
	  #define ERROR_LOG_BACK_NAME     "Xmtr-DD.bak"
 #define ERR_FILE_WW L"_XmtrErrW.log"
 #define ERR_FILE_W   "_XmtrErrW.log"
 #define ERR_FILE     "_XmtrErr.log"
//
 #define LOG_FILE_WW L"_XmtrLogW.log"
 #define LOG_FILE_W   "_XmtrLogW.log";
 #define LOG_FILE     "_XmtrLog.log";
//
 #define OUT_FILE_WW L"_XmtrOutW.log"
 #define OUT_FILE_W   "_XmtrOutW.log"
 #define OUT_FILE     "_XmtrOut.log"
 #endif
#else  /* is TOK */
  #pragma message("Is_TOK log message")
  #define	ERROR_MESSAGE_CAPTION	"Tokenizer"
	#define  WERROR_MESSAGE_CAPTION		_T(ERROR_MESSAGE_CAPTION)
	  #define ERROR_LOG_MAIN_NAME     "Tokenizer.log"
	  #define ERROR_LOG_BACK_NAME     "Tokenizer.bak"
 #define ERR_FILE_WW L"_TokErrW.log"
 #define ERR_FILE_W   "_TokErrW.log"
 #define ERR_FILE     "_TokErr.log"
//
 #define LOG_FILE_WW L"_TokLogW.log"
 #define LOG_FILE_W   "_TokLogW.log";
 #define LOG_FILE     "_TokLog.log";
//
 #define OUT_FILE_WW L"_TokOutW.log"
 #define OUT_FILE_W   "_TokOutW.log"
 #define OUT_FILE     "_TokOut.log"
#endif

#ifndef FMA_PARSER_LOG
	CWnd* mainAppWindow = NULL;
	#define UPDATE_STATUS(a)	/* no-op */
	#define GET_UPDATEE(t)		mainAppWindow = NULL
#else

#ifdef _IS_STATIC
  #if !defined(_68K_) && !defined(_MPPC_) && !defined(_X86_) && !defined(_IA64_) && !defined(_AMD64_) && defined(_M_IX86)
    #define _X86_
  #endif
  #include <WinDef.h>
  #include <BaseTsd.h>
  #include <Winbase.h>
#else
  #include <windows.h>
#endif

#endif

#endif // IS_SDC

#pragma warning (disable : 4786)
/* --- Global Variables --------------------------------------------------- */

errStruct_t errList[] = { ERR_DESC_LIST_00 ERR_DESC_LIST_END };

/* --- Module Variables --------------------------------------------------- */
static char strTrace[TRACE_SIZE];
static wchar_t wstrTrace[TRACE_SIZE];
// stevev 10jul06 - make non-static 
volatile unsigned int logPer = 0;

static wchar_t statusString[STATUS_LEN];

// stevev - WIDE2NARROW char interface
wchar_t glbWbuf[_MAX_PATH];
char    gblBuf [_MAX_PATH];
#ifdef GE_BUILD// PAW start 30/04/09
	char LogOUTCSDebugInfo = NULL;
	char LogERRCSDebugInfo = NULL;
	char LogLOGCSDebugInfo = NULL;
	char LogMsgCSDebugInfo = NULL;
	char StatusBufCSDebugInfo = NULL;
	char CSDebugInfo = NULL;
#endif // PAW end 30/04/09

#ifdef _UNICODE
char* convertToChar(const wchar_t *inPtr)
{
	//UnicodeToASCII(inPtr, gblBuf, _MAX_PATH);
	wstring lws(inPtr);
	string  ls;
	ls = TStr2AStr(lws);

	strcpy_s(gblBuf,_MAX_PATH,ls.c_str());
	return gblBuf;
}
wchar_t* Ascii2Unicode(const char* inPtr)
{
	//AsciiToUnicode(inPtr,  glbWbuf, _MAX_PATH);
	wstring lws;
	string  ls(inPtr);
	lws = AStr2TStr(ls);
	wcscpy_s(glbWbuf,_MAX_PATH,lws.c_str());

	return glbWbuf;
}
#else
char* convertToChar(const char *inPtr)
{
	strncpy(gblBuf, inPtr, _MAX_PATH);
	return gblBuf;
}
char*    Ascii2Unicode(const char* inPtr)
{
	return (char*)inPtr;
}
#endif


/*** List for storing Log messages into a list ***/
typedef struct stLogMessage
{
	char szBuf[500];
	stLogMessage()
	{
		memset(szBuf, 0, 500 );
	}
}LOGMESSAGE;
typedef std::list<LOGMESSAGE> LOGMESSAGE_LIST;
LOGMESSAGE_LIST LogMessageList;

typedef struct errorItem_s
{
	string format;
	string name;
	string desc;
}/*typedef*/ errorItem_t;

#pragma warning (disable : 4786)
typedef map<int,errorItem_t> errorMap_t;


#define MAX_LOGMSG_LIST_SIZE		1000
/***** NOTE:   Bill's 'critical section' allows reentry into itself
 *****         within the same thread!!!!!!!!!  BEWARE
 *****        eg inside a critical section and the OS blocks for some reason
 *****           it will then go process other windows messages which can get
 *****           into that same critical section - my case crashed the second 
 *****           time.   Bill doesn't think it's THAT critical...
 ************************************************************************************/
CRITICAL_SECTION			LogMsgCriticalSection;// LogMessage list
// added stevev 28jul05
CRITICAL_SECTION			StatusBufCriticalSection;// status bar
// added stevev 03sep09
CRITICAL_SECTION			UI_log_criticalSection;// UI logging func

CRITICAL_SECTION			criticalSection;// LogMessage function

CRITICAL_SECTION	LogOUTCriticalSection;
CRITICAL_SECTION	LogERRCriticalSection;
CRITICAL_SECTION	LogLOGCriticalSection;

/***************************************************/

static wofstream* plogwRedir = NULL;//stevev 10jul08
static wofstream* poutwRedir = NULL;
static wofstream* perrwRedir = NULL;

static wstreambuf* pHldwOutBuf = NULL;
static wstreambuf* pHldwErrBuf = NULL;
static wstreambuf* pHldwLogBuf = NULL;

//wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

static ofstream* plogRedir = NULL;
static ofstream* poutRedir = NULL;
static ofstream* perrRedir = NULL;

static streambuf* pHldOutBuf = NULL;
static streambuf* pHldErrBuf = NULL;
static streambuf* pHldLogBuf = NULL;

//////////////////////////////////////////////////////

bool haveOutfile = false;
bool haveErrfile = false;
bool haveLogfile = false;
bool haveUI_file = false;

errorMap_t errorList;


/***** INFO *****/
bool isClogging(void) { return (haveLogfile);};
bool isCerring(void) { return (haveErrfile);};
bool isOuting(void) { return (haveOutfile);};

/* ------------------------------------------------------------------------- */
void ShowUserErrorDialog(char * strMessage,...);
bool GetLogMsgFromLog( int nLine, char *pLogMsg, size_t bufLen );
void GetNoOfLogMsgs( int *pnTotalLogMsgCnt );
void ClearLog();	//Added by ANOOP 03MAR2004
int  setupErrorDB(void); // stevev 16may08

// these are left in to not break what's working
static void LogMessage(char * strMessage,...);
static void LogMessageV(char    *strMessage,va_list varArgList);
static void LogMessageV(wchar_t *strMessage,va_list varArgList);

static int  logoutV(int channel, int errNumber, va_list vL);

static int  logoutV(int channel, const char* format,  va_list vL);
static int  logoutV(int channel, const wchar_t* format, va_list vL);


void verifyCriticalSections(void)
{
#ifndef GE_BUILD  // removed PAW 30/04/09 DebugInfo does not exist in Win CE
	if (LogOUTCriticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&LogOUTCriticalSection);
	}
	if (LogERRCriticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&LogERRCriticalSection);
	}
	if (LogLOGCriticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&LogLOGCriticalSection);
	}
	
	if (LogMsgCriticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&LogMsgCriticalSection);
	}	
	if (StatusBufCriticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&StatusBufCriticalSection);
	}	
	if (UI_log_criticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&UI_log_criticalSection);
	}
	if (criticalSection.DebugInfo == NULL)
	{
		InitializeCriticalSection(&criticalSection);
	}		
#else // it is GE_BUILD
	if (LogOUTCSDebugInfo == NULL)
	{
		InitializeCriticalSection(&LogOUTCriticalSection);
		LogOUTCSDebugInfo =1;
	}
	if (LogERRCSDebugInfo == NULL)
	{
		InitializeCriticalSection(&LogERRCriticalSection);
		LogERRCSDebugInfo = 1;
	}
	if (LogLOGCSDebugInfo == NULL)
	{
		InitializeCriticalSection(&LogLOGCriticalSection);
		LogLOGCSDebugInfo = 1;
	}
	
	if (LogMsgCSDebugInfo == NULL)
	{
		InitializeCriticalSection(&LogMsgCriticalSection);
		LogMsgCSDebugInfo = 1;
	}	
	if (StatusBufCSDebugInfo == NULL)
	{
		InitializeCriticalSection(&StatusBufCriticalSection);
		StatusBufCSDebugInfo = 1;
	}
	if (CSDebugInfo == NULL)
	{
		InitializeCriticalSection(&criticalSection);
		CSDebugInfo = 1;
	}			
#endif // GE_BUILD
}

/* remove trailing newline and spaces , returns true if a newline was removed */
static wchar_t* remTrailing(const wchar_t* fmt)
{
	int k, y ;
	wchar_t* pstr = (wchar_t*)fmt;
	wcscpy_s(wstrTrace, TRACE_SIZE, fmt);
	y = wcslen(wstrTrace);

	for ( k = y-1; k >= 0; k++ )
	{
		if (wstrTrace[k] == _T('\n') || wstrTrace[k] == _T('\r'))
		{
			wstrTrace[k] = _T('\0');
			pstr = wstrTrace;
			if ( k > 0 && (wstrTrace[k-1] != _T('\n') && wstrTrace[k-1] != _T('\r')))
				break; // out of for loop
		}
		else
		if ( wstrTrace[k] != _T(' ') && wstrTrace[k] != _T('\t') )
		{
			break; // out of for loop
		}
		// else skip trailing spaces and tabs
	}
	return pstr;	//= entry at not modified OR strTrace if modified
}


/*F*/
/***************************************************************************
** FUNCTION NAME: LogMessage
**
** PURPOSE		: Logs the messages into a file specified in the registry
**	configuration or a default file. If the file size exceeds the maximum
**	specified then it is saved as a .bak file and a new file generated. If
**	a .bak file already existed, then its contents are overwritten.
**		This function is reentrant.
**
** PARAMETER	: The format string and Variable parameters to be logged..
**			similar to parameters of printf !
**
** RETURN VALUE	: none
****************************************************************************/
void LogMessage(char * strMessage,...)
{
	////  UI_LOG  ////
//  HOMZ - TODO :  Currently, Logging is ON always.  In the future,
//                 If logging is TURNED OFF, then return NOW
//                 Retrieve the setting from the registry if necessary
// stevev merge 19feb07 - Non-Error Logging will morph to be off always with sections turned on
//						- disable error logging at your own risk.
//  if (m_FetchLogLevel == false)
//  {
//      *** get the Log Level from the System Registry
//      m_FetchLogLevel = true;
//  }
//  if (m_LogLevel == 0)
//  {
//      return;
//  }

//	char strPath[_MAX_PATH];
	char strDefaultLogFileName[_MAX_FNAME];
	char strDefaultBackupName[_MAX_FNAME];

	static char strLogFilePath[_MAX_PATH];
	static char strBackupFilePath[_MAX_PATH];

	// HOMZ - replaced check for "if(!bCriticalSectionInit)" with the
	//        actual determination as to whether the critical section has been initialized.
	//        Also made criticalSection at the same level as Log & Status sections.
	verifyCriticalSections();

	static unsigned uMaxFileSize = FILE_SIZE * MB;

	EnterCriticalSection(&criticalSection);

	/* If this is the first time we are called... we need to read some
	configurations before we are ready to go !*/
	if(!haveUI_file)
	{
		/* Initialise the trace buffer */
		strcpy_s (strTrace, TRACE_SIZE, "");

		/* set the default file names */
		strcpy_s(strDefaultLogFileName,_MAX_FNAME,ERROR_LOG_MAIN_NAME);
		strcpy_s(strDefaultBackupName, _MAX_FNAME,ERROR_LOG_BACK_NAME);

		char *strPtr;
		char strCommandLine[_MAX_PATH];


		/* Try to get the path of our executable */
// stevev - WIDE2NARROW char interface
		TCHAR* pCmdLn = GetCommandLine();
//was		strcpy(strCommandLine, GetCommandLine());
#ifdef _UNICODE
		//UnicodeToASCII((LPCWSTR)pCmdLn, strCommandLine, _MAX_PATH);
		strcpy_s(strCommandLine, _MAX_PATH, convertToChar((LPCWSTR)pCmdLn) );
#else
		strcpy(strCommandLine, pCmdLn);
#endif
		/* Remove the "" in this string  */
		do
		{
			strPtr = strchr(strCommandLine, '"');

			if(strPtr != NULL)
			{
				if (strPtr > strCommandLine)
				{
					int iCopySize = strPtr - strCommandLine;
					strncpy_s(
							strLogFilePath,_MAX_PATH
							, strCommandLine
							, iCopySize
							);
					strcpy_s
						(
						strLogFilePath + iCopySize, _MAX_PATH-iCopySize
						, strCommandLine + iCopySize + 1
						);
				}
				else
				{
					strcpy_s(
							strLogFilePath,_MAX_PATH
							, strPtr + 1
							);
				}
				strcpy_s(strCommandLine,_MAX_PATH,  strLogFilePath);
			}
			else
			{
				strcpy_s(strLogFilePath, _MAX_PATH, strCommandLine);
				break;
			}
		}while (strPtr != NULL);
		_strlwr_s(strLogFilePath,_MAX_PATH);

		//COMMENTED by Deepak
		//	strPtr = strstr(strLogFilePath,"ddides.exe");

	// Added By Deepak
	// The following lines would replace above statement
		int i=0;
		int nLastBackSlash =0;

		while(strLogFilePath[i] && (strLogFilePath[i] != ' '))
		{
			i++;
			if (strLogFilePath[i] == '\\')
				nLastBackSlash =i;
		}

		strLogFilePath [nLastBackSlash+1] ='\0';

		//COMMENTED by Deepak
		//strcpy(strPtr, "");
	//END MOD
		strcpy_s(strBackupFilePath, _MAX_PATH,strLogFilePath);
		strcat_s(strLogFilePath,_MAX_PATH,strDefaultLogFileName);
		strcat_s(strBackupFilePath,_MAX_PATH,strDefaultBackupName);

		/* Ensures we do the configuration only once !*/
		haveUI_file = TRUE;
	}


#ifndef GE_BUILD	// PAW 30/04/09
	/* Now check if the size */
	_finddata_t fileData;
	long handle;

	if(-1L != (handle =_findfirst(strLogFilePath,&fileData)))
	{
		_findclose(handle);
		/* If the sie of the file is greater than the max size... just
		take a back up...*/
		if(fileData.size >= uMaxFileSize)
		{

			remove(strBackupFilePath);
			if(rename(strLogFilePath,strBackupFilePath))
			{
				LeaveCriticalSection(&criticalSection);
				return ;
			}
		}
	}

#else
		LPWIN32_FIND_DATA lpFindFileData;
		HANDLE tempHandle;
		tempHandle = FindFirstFileW((LPCWSTR)strLogFilePath, lpFindFileData);
		if(tempHandle!= INVALID_HANDLE_VALUE)
		{
			FindClose(tempHandle);
			if(lpFindFileData->nFileSizeLow >= uMaxFileSize)
			{
				DeleteFile ((LPCWSTR)strBackupFilePath);
				if(DeleteAndRenameFile((LPCWSTR)strLogFilePath,(LPCWSTR)strBackupFilePath))
				{
					LeaveCriticalSection(&criticalSection);
					return ;
				}
			}	
		}
#endif

   	FILE *file;

	char strTime[15];
	char strDate[15];

#ifndef _WIN32_WCE	// PAW function missing 06/05/09
	_strtime_s(strTime,15);
	_strdate_s(strDate,15);
#else
	struct tm * tmbuffer;
	SYSTEMTIME	st;
	GetSystemTime(&st);
  #ifdef USED	// remove for now place debugging here to find later - causes a data mis-alignment exception
	SystemTimeToTm(&st, tmbuffer);
	strftime_ce(strDate, 9/* no chars*/, "%m/%d/%y", tmbuffer);
	strftime_ce(strTime, 9/* no chars*/, "%H:%M:%S", tmbuffer);
  #endif
#endif

	/* Open the file for logging */
	errno_t fe = fopen_s(&file,strLogFilePath,"a");
	if(NULL == file || fe != 0)
	{
		char strActual[400];
		va_list vMarker;

		va_start(vMarker, strMessage);
		vsprintf_s(strActual,400, strMessage, vMarker);
		va_end(vMarker);

		LeaveCriticalSection(&criticalSection);
		return ;
	}

	/* Write the date and the time */
	fwrite(strDate,sizeof(char),strlen(strDate),file);
	fwrite("  ",sizeof(char),2,file);

	fwrite(strTime,sizeof(char),strlen(strTime),file);
	fwrite("  ",sizeof(char),2,file);

	char strActual[400];
	va_list vMarker;

	va_start(vMarker, strMessage);
	vsprintf_s(strActual, 400, strMessage, vMarker);
	va_end(vMarker);

	// strip embedded newline chars
	char * pC = &(strActual[0]);
	while (*pC)
	{
		if ( *pC == '\r' || *pC == '\n' )
		{
			*pC = ' ';
		}
		pC++;
	}
	/* Log the actual string */
	fwrite(strActual,sizeof(char),strlen(strActual),file);
	fwrite("\r\n",sizeof(char),2,file);

	/* Add the log message to Queue */
	LOGMESSAGE LogMsg;
	EnterCriticalSection(&LogMsgCriticalSection);
	/* Get size of the list, if list size is > 5000, delet the last element of the list */
	if( LogMessageList.size() > MAX_LOGMSG_LIST_SIZE )
	{
		LOGMESSAGE_LIST::iterator	iterateLogMessage;
		iterateLogMessage = LogMessageList.begin();
		LogMessageList.erase( iterateLogMessage );
	}
	// Get Current time;
	SYSTEMTIME sysTempTime;
	GetLocalTime(&sysTempTime);
	/* Format the log message with current date */
	sprintf_s(LogMsg.szBuf, 500, "%2d/%2d/%2d %02d:%02d:%02d  %s", 
			sysTempTime.wMonth,  sysTempTime.wDay,    sysTempTime.wYear, sysTempTime.wHour,
			sysTempTime.wMinute, sysTempTime.wSecond, strActual );

	if ( strlen(strActual) < 4 )
	{
		//assert(0);
	}
	LogMessageList.push_back(LogMsg);
//	DEBUGLOG(CLOG_LOG,"ECHO:  Pushed '%s' to get %d entries.\n",LogMsg,LogMessageList.size());
	
/* fixed the echo
	string   dStr(LogMsg.szBuf);
	wstring dwStr;
	dwStr = AStr2TStr(dStr);// to unicode
	wclog << L"ECHO:  Pushed '" << dwStr <<L"' to get " << LogMessageList.size() 
		  <<L" entries.\n";
****/
	LeaveCriticalSection(&LogMsgCriticalSection);
	/************/

	/* Close the file */
    fclose(file);

	/* Copy the message to the local trace buffer */
	if ((strlen(strActual) + strlen(strTrace)) > TRACE_SIZE)
	{
		strcpy_s(strTrace, TRACE_SIZE, "");
	}
	strcat_s(strTrace, TRACE_SIZE, strActual);

	LeaveCriticalSection(&criticalSection);
	return ;
}/* End of Function: LogMessage() */

/*F*/
/***************************************************************************
** FUNCTION NAME: ShowUserErrorDialog
**
** PURPOSE		: Shows a message box with the error string passed as
**	parameter
**
** PARAMETER	: The format string and Variable parameters to be logged..
**			similar to parameters of printf !
**
** RETURN VALUE	: none
****************************************************************************/
/* this has been rolled into LOGIT------------------------------------------
void ShowUserErrorDialog(wchar_t * strMessage,...)
{
	char strActual[400];
	va_list vMarker;

	va_start(vMarker, strMessage);
	vsprintf(strActual, strMessage, vMarker);
	va_end(vMarker);

	// stevev - WIDE2NARROW char interface
	//CString theMsg;
	string   lStr(strActual);
	wstring lwStr;
#ifdef _UNICODE
	//AsciiToUnicode(strActual, theMsg);
	lwStr = AStr2TStr(lStr);
	MessageBox(
			NULL
			, lwStr.c_str() // was 24mar08 , theMsg // was 10jan08 , strActual // was 29jun07 - strMessage
			, WERROR_MESSAGE_CAPTION
			, MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
#else
	//theMsg = strActual;

	MessageBox(
			NULL
			, lStr.c_str() // was 24mar08, theMsg // was 10jan08 , strActual // was 29jun07 - strMessage
			, WERROR_MESSAGE_CAPTION
			, MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
#endif

	return;
}// End of Function: ShowUserErrorDialog() 
-------------- end of rolled into logit ----------------------------------*/

/*F*/
/***************************************************************************
** FUNCTION NAME: GetTrace
**
** PURPOSE		: Copies the trace buffer to the buffer pointed to by the
**	parameter.
**
** PARAMETER	: Pointer to a buffer to which memory is allocated! If the
**	call is successful then the buffer trace buffer is cleared.
**
** RETURN VALUE	: none
****************************************************************************/
/* unused after 5 years, commented out stevev 19sep07
void GetTrace(char strTraceParam[])
{
	try
	{
		memcpy(strTraceParam, strTrace, strlen(strTrace));
		strcpy(strTrace,"");
	}
	catch(...)
	{
		LogMessage("Call to GetTrace function without allocating enough memory");
		return;
	}
	return;
}*//* End of Function: GetTrace() */

bool GetLogMsgFromLog( int nLine, char *pLogMsg, size_t bufLen )
{
	EnterCriticalSection(&LogMsgCriticalSection);
	LOGMESSAGE_LIST::iterator	iterateLogMessage;
	int nCnt = 0;
	LOGMESSAGE LogMsg;
	for( iterateLogMessage = LogMessageList.begin(); iterateLogMessage != LogMessageList.end();
		iterateLogMessage++)
	{
		LogMsg = (*iterateLogMessage);
		strcpy_s( pLogMsg, bufLen, LogMsg.szBuf );
		nCnt++;
		if(nCnt == nLine)
			break;

	}
	LeaveCriticalSection(&LogMsgCriticalSection);
	if( nCnt == 0)
		return FALSE;
	return TRUE;

}


void GetNoOfLogMsgs( int *pnTotalLogMsgCnt )
{
	EnterCriticalSection(&LogMsgCriticalSection);
	*pnTotalLogMsgCnt = LogMessageList.size();
	LeaveCriticalSection(&LogMsgCriticalSection);
}
void LogMessageV(wchar_t *strMessage,va_list varArgList)
{
	// stevev 08apr11, this has to sprinf wide char first, then translate
	wchar_t strActual[TRACE_SIZE];
	verifyCriticalSections();

	int r = vswprintf(strActual, TRACE_SIZE, strMessage, varArgList);
	va_end(varArgList);

	string   lStr;
	wstring  lwStr(strActual);
	lStr = TStr2AStr(lwStr);// to unicode

	LogMessage( ((char *)lStr.c_str()) );
	//LogMessageV((char*)lStr.c_str(),varArgList);
}

void LogMessageV(char *strMessage,va_list varArgList)
{
	//static bCriticalSectionInit = FALSE;
	//static CRITICAL_SECTION criticalSection;

	char strActual[TRACE_SIZE];

	verifyCriticalSections();

// stevev 19sep07-LogMessage gets this...	EnterCriticalSection(&criticalSection);

//		va_start(varArgList, strMessage);
		vsprintf_s(strActual, TRACE_SIZE, strMessage, varArgList);
		va_end(varArgList);
		LogMessage(strActual);
// stevev 19sep07-LogMessage gets this...	LeaveCriticalSection(&criticalSection);
	/************/
	return ;
}
/* End of Function: LogMessageV() */
/* End of Function: OutputToTrace() */

#ifdef _DEBUG
string logfileone, logfiletwo;
string outfileone, outfiletwo;
string errfileone, errfiletwo;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// external interface:
// L&T Modifications : MethodSupport - start
#ifdef _CONSOLE
int logSetup(DDSargs &rArgs, unsigned short * cmdLine)
{
	std::cout << " added this method for METHOD SUPPORT \n" << 
		"in order to remove unresolved external symbol int __cdecl logSetup error" <<
		" have added the arg as unsigned short" << std::endl;
	return 0;
}
#endif
// L&T Modifications : MethodSupport - end

#if ! defined TOK  && ! defined _CONSOLE  && ! defined FMA_PARSER_LOG
/* TOK doesn't redirect any outputs -------------------------------------------*/
/* we don't want the parser (by itself) to redefine outputs */
int logSetup(DDSargs &rArgs, char * cmdLine)
{
	wstring locWstr;
	string  loc_str, plusStr;

	verifyCriticalSections(); //be sure they are all ready for operation
	
	EnterCriticalSection(&LogOUTCriticalSection);//++++++++++++++++++++++++++++++++++++++++++++

	if ( rArgs[_T('O')] && ! haveOutfile && ! (rArgs[_T('O')][_T("outputFileName")].empty()))
	{// reset the output file
		locWstr = rArgs[_T('O')][_T("outputFileName")];
		loc_str = convertToChar(locWstr.c_str());

       #ifdef _DEBUG
		cout << ERROR_MESSAGE_CAPTION " output to " << loc_str.c_str() << endl;
       #endif

		// we do this to take care of all those cout<<... calls that are out there
/* was commented out, re instate the cout redirect */
		poutRedir = new ofstream;
		poutRedir->open( loc_str.c_str() );

		if ( poutRedir->is_open() )
		{
			pHldOutBuf = cout.rdbuf();
			cout.rdbuf(poutRedir->rdbuf());
			haveOutfile = true;
/* end of re-instated redirect */

			//  this is the real LOGIT target file - everything is wide here..
			poutwRedir = new wofstream;

			// we will use a narrow filename till told otherwise
			plusStr  = "w";
			plusStr += loc_str.c_str();
			poutwRedir->open(plusStr.c_str());

			if ( poutwRedir->is_open() )
			{
				pHldwOutBuf = wcout.rdbuf();
				wcout.rdbuf(poutwRedir->rdbuf());
				//leave haveOutfile = true;
//was not commented				pHldOutBuf = cout.rdbuf();
		//		cout.rdbuf(poutwRedir->rdbuf()); //poutRedir->rdbuf());
//was not commented				haveOutfile = true;
			}
			else
			{
				cerr << "ERROR: output redirection failed." << endl;
				RAZE(poutwRedir);

				if (pHldOutBuf != NULL ) 
					cout.rdbuf(pHldOutBuf); 
				pHldOutBuf = NULL;
/* was commented out, re instate the cout redirect */
				if ( poutRedir )
				{
					if ( poutRedir->is_open() )
						poutRedir->close(); 
					RAZE(poutRedir);	
				}
/* end reinstated out file */
				haveOutfile = false;
			}
		}
		else // not open
		{
			cerr << "ERROR: output redirection failed." << endl;
			RAZE(poutRedir);	
		}
/* was commented out, re instate the cout redirect */
	}
	else // user does not want to log cout & wcout
	{
		haveOutfile = false; // be sure to disable
	}
/* end of was commented out */
#ifdef _DEBUG	// PAW 31/07/09
	cout << ERROR_MESSAGE_CAPTION << " output file." << endl;
#endif	
	LeaveCriticalSection(&LogOUTCriticalSection);//===============================================


	EnterCriticalSection(&LogERRCriticalSection);//+++++++++++++++++++++++++++++++++++++++++++++++

	if ( rArgs[_T('E')] && ! haveErrfile && ! (rArgs[_T('E')][_T("errorFileName")].empty()))
	{// reset the error stream
		locWstr = rArgs[_T('E')][_T("errorFileName")];
		loc_str = convertToChar(locWstr.c_str());

       #ifdef _DEBUG
		cout << ERROR_MESSAGE_CAPTION " errors to "  << loc_str.c_str() << endl;
	   #endif

		// we do this only to take care of all those cerr<<... calls that are out there
		//
		// Note that the exception
		//             "First-chance exception at 0x00000000 in SDC625.exe: 0xC0000005:..."
		// is deep inside the OS CreateFile() function and cannot be traced to anything passed
		// to it.  Appears to be a MS programming error.
		perrRedir = new ofstream;
		perrRedir->open( loc_str.c_str() );

		if ( perrRedir->is_open() )
		{
			pHldErrBuf = cerr.rdbuf();
			cerr.rdbuf(perrRedir->rdbuf());
			haveErrfile = true;

			//  this is the real LOGIT target file - everything is wide here..
			perrwRedir = new wofstream;

			// we will use a narrow filename till told otherwise
			plusStr  = "w";
			plusStr += loc_str.c_str();
			perrwRedir->open(plusStr.c_str());

			if ( perrwRedir->is_open() )
			{
				pHldwErrBuf = wcerr.rdbuf();
				wcerr.rdbuf(perrwRedir->rdbuf());
				//leave haveErrfile = true;
			}
			else
			{
				cerr << "ERROR: error file redirection failed." << endl;
				RAZE(perrwRedir);

				if (pHldErrBuf != NULL ) 
					cerr.rdbuf(pHldErrBuf); 
				pHldErrBuf = NULL;

				if ( perrRedir )
				{
					if ( perrRedir->is_open() )
						perrRedir->close(); 
					RAZE(perrRedir);	
				}
				haveErrfile = false;
			}
		}
		else
		{
			haveErrfile = false; // be sure to disable
			cerr << "ERROR: error file redirection failed." << endl;
		}
	}
	else
	{
		haveErrfile = false; // be sure to disable
	}
	//cerr << ERROR_MESSAGE_CAPTION << " error file." << endl;
	wcerr<<WERROR_MESSAGE_CAPTION<< L" error file." << endl;

	if ( cmdLine != NULL )
	{		
		string   lStr(cmdLine);
		wstring lwStr,lwSpec;
		lwStr = AStr2TStr(lStr);// to unicode
		lwSpec = remTrailing(lwStr.c_str() );

		wcerr << lwSpec << endl << endl;
	}	

	LeaveCriticalSection(&LogERRCriticalSection);//===============================================



	EnterCriticalSection(&LogLOGCriticalSection);//+++++++++++++++++++++++++++++++++++++++++++++++

	if ( rArgs[_T('G')] && ! haveLogfile && ! (rArgs[_T('G')][_T("logFileName")].empty()))
	{// reset the logging stream
		locWstr = rArgs[_T('G')][_T("logFileName")];
		loc_str = convertToChar(locWstr.c_str());

       #ifdef _DEBUG
		cout << ERROR_MESSAGE_CAPTION " logging to "<< loc_str.c_str() << endl;
	   #endif

		// we do this only to take care of all those clog<<... calls that are out there
		plogRedir = new ofstream;
		plogRedir->open( loc_str.c_str() );

		if ( plogRedir->is_open() )
		{
			pHldLogBuf = clog.rdbuf();
			clog.rdbuf(plogRedir->rdbuf());
			haveLogfile = true;

			//  this is the real LOGIT target file - everything is wide here..
			plogwRedir = new wofstream;

			// we will use a narrow filename till told otherwise
			plusStr  = "w";
			plusStr += loc_str.c_str();
			plogwRedir->open(plusStr.c_str());

			if ( plogwRedir->is_open() )
			{
				pHldwLogBuf = wclog.rdbuf();
				wclog.rdbuf(plogwRedir->rdbuf());
				//leave haveOutfile = true;
			}
			else
			{
				cerr << "ERROR: log file redirection failed." << endl;
				RAZE(plogwRedir);

				if (pHldLogBuf != NULL ) 
					clog.rdbuf(pHldLogBuf); 
				pHldLogBuf = NULL;

				if ( plogRedir )
				{
					if ( plogRedir->is_open() )
						plogRedir->close(); 
					RAZE(plogRedir);	
				}
				haveLogfile = false;
			}
		}
		else // not open
		{
			cerr << "ERROR: log file redirection failed." << endl;
			RAZE(plogRedir);	
		}
	}
	else
	{
		haveLogfile = false; // be sure to disable
	}
	clog  << ERROR_MESSAGE_CAPTION << " logging file." << endl;
	wclog << WERROR_MESSAGE_CAPTION << L" logging file." << endl;

	LeaveCriticalSection(&LogLOGCriticalSection);//===============================================

	if ( rArgs[_T('T')] && ! (rArgs[_T('T')][_T("Log_Permission")].empty()))
	{// set the logging permissions // '0x0000ffff'
		unsigned int tmp;
		char sTmp[32];
		strncpy(sTmp,convertToChar(rArgs[_T('T')][_T("Log_Permission")].c_str()),32);
		if ( sscanf(sTmp,"0x%x",&tmp) == 1)
		{
			logPer = tmp;
		}
		// else - don't change it
	}
	else // no command line
	{
		logPer = 0;
	}
#ifndef TOK
	logPer |= LOGP_NOT_TOK;// enable all those error messages
#endif
#ifdef _DEBUG
	logPer |= LOGP_NOT_TOK;// enable all those error messages
#endif


	logout(CLOG_LOG, L">>LogPermission: 0x%04x\n", logPer);


	//mainAppWindow = (CMainFrame *) AfxGetMainWnd();
	GET_UPDATEE(CMainFrame);

	/* initialize the error string list */
//	LIB_ERR_INIT;

#ifndef _WIN32_WCE
	wcscpy(statusString,STATUS_BAR_DEFAULT);
#endif
	return 0; // show all OK even if it's not (defaults will be used)
}
	
int  logSetup(DDSargs  &rArgs, wchar_t * cmdLine)
{
	if (cmdLine)
	{
		int y = wcslen(cmdLine);
		char *narrowCmd = new char[y+1];
		memset(narrowCmd,0,(y+1));
		UnicodeToASCII(cmdLine, narrowCmd, y+1);
		y = logSetup(rArgs, narrowCmd);
		delete[] narrowCmd;
		return y;
	}
	else
	{
		return logSetup(rArgs, (char*)NULL);
	}
}

#else /* is TOK or Parser **********************/
	/* the tokenizer doesn't redefine anything */

/* NOTE: outputs are sent to screen when in TOK release mode */
	
int logSetup(void)
{	
	wstring locWstr;
	string  loc_str, plusStr;

	verifyCriticalSections();

	EnterCriticalSection(&LogOUTCriticalSection);

	poutRedir = NULL;
	poutwRedir= NULL;

	LeaveCriticalSection(&LogOUTCriticalSection);


	EnterCriticalSection(&LogERRCriticalSection);

#if (! defined(_DEBUG) && ! defined(FMA_PARSER_LOG) )
/// #ifndef _DEBUG
	perrRedir = NULL;
#else
	locWstr = ERR_FILE_WW;
	plusStr = ERR_FILE_W;
	loc_str = ERR_FILE;

	perrRedir = new ofstream;
	perrRedir->open( loc_str.c_str() );

	if ( perrRedir->is_open() )
	{
		pHldErrBuf = cerr.rdbuf();
		cerr.rdbuf(perrRedir->rdbuf());
		haveErrfile = true;
//cerr<<"    Err File opened as "<<loc_str<<endl;

		//  this is the real LOGIT target file - everything is wide here..
		perrwRedir = new wofstream;

		// we will use a narrow filename till told otherwise
		perrwRedir->open(plusStr.c_str());

		if ( perrwRedir->is_open() )
		{
			pHldwErrBuf = wcerr.rdbuf();
			wcerr.rdbuf(perrwRedir->rdbuf());
			//leave haveErrfile = true;
		}
		else
		{
			cerr << "ERROR: error file redirection failed." << endl;
			RAZE(perrwRedir);

			if (pHldErrBuf != NULL ) 
				cerr.rdbuf(pHldErrBuf); 
			pHldErrBuf = NULL;

			if ( perrRedir )
			{
				if ( perrRedir->is_open() )
					perrRedir->close(); 
				RAZE(perrRedir);	
			}
			haveErrfile = false;
		}
	}
	else
	{
		haveErrfile = false; // be sure to disable
		cerr << "ERROR: error file redirection failed." << endl;
	}	
#endif

	LeaveCriticalSection(&LogERRCriticalSection);


	EnterCriticalSection(&LogLOGCriticalSection);

#if (! defined(_DEBUG) && ! defined(FMA_PARSER_LOG) )
////#ifndef _DEBUG
	plogRedir = NULL;
	plogwRedir= NULL;//stevev 10jul08
#else
	locWstr = LOG_FILE_WW;
	plusStr = LOG_FILE_W;
	loc_str = LOG_FILE;

	plogRedir = new ofstream;
	plogRedir->open( loc_str.c_str() );

	if ( plogRedir->is_open() )
	{
		pHldLogBuf = clog.rdbuf();
		clog.rdbuf(plogRedir->rdbuf());
		haveLogfile = true;
//cerr<<"    Err File opened as "<<loc_str<<endl;

		//  this is the real LOGIT target file - everything is wide here..
		plogwRedir = new wofstream;

		// we will use a narrow filename till told otherwise
		plogwRedir->open(plusStr.c_str());

		if ( plogwRedir->is_open() )
		{
			pHldwLogBuf = wclog.rdbuf();
			wclog.rdbuf(plogwRedir->rdbuf());
			//leave haveErrfile = true;
//cerr<<"WideErr File opened as "<<plusStr<<endl;
		}
		else
		{
			cerr << "ERROR: logging file redirection failed." << endl;
			RAZE(plogwRedir);

			if (pHldLogBuf != NULL ) 
				clog.rdbuf(pHldLogBuf); 
			pHldLogBuf = NULL;

			if ( plogRedir )
			{
				if ( plogRedir->is_open() )
					plogRedir->close(); 
				RAZE(plogRedir);	
			}
			haveLogfile = false;
		}
	}
	else
	{
		haveLogfile = false; // be sure to disable
		cerr << "ERROR: logging file redirection failed." << endl;
	}	
#endif

	LeaveCriticalSection(&LogLOGCriticalSection);

	EnterCriticalSection(&LogOUTCriticalSection);

#if (! defined(_DEBUG) && ! defined(FMA_PARSER_LOG) )
////#ifndef _DEBUG
	poutRedir = NULL;
	poutwRedir= NULL;//stevev 10jul08
#else
	locWstr = OUT_FILE_WW;
	plusStr = OUT_FILE_W;
	loc_str = OUT_FILE;

	poutRedir = new ofstream;
	poutRedir->open( loc_str.c_str() );

	if ( poutRedir->is_open() )
	{
		pHldOutBuf = cout.rdbuf();
		cout.rdbuf(poutRedir->rdbuf());
		haveOutfile = true;

		//  this is the real LOGIT target file - everything is wide here..
		poutwRedir = new wofstream;

		// we will use a narrow filename till told otherwise
		poutwRedir->open(plusStr.c_str());

		if ( poutwRedir->is_open() )
		{
			pHldwOutBuf = wcout.rdbuf();
			wcout.rdbuf(poutwRedir->rdbuf());
			//leave haveErrfile = true;
		}
		else
		{
			cerr << "ERROR: output file redirection failed." << endl;
			RAZE(poutwRedir);

			if (pHldOutBuf != NULL ) 
				cout.rdbuf(pHldOutBuf); 
			pHldOutBuf = NULL;

			if ( poutRedir )
			{
				if ( poutRedir->is_open() )
					poutRedir->close(); 
				RAZE(poutRedir);	
			}
			haveOutfile = false;
		}
	}
	else
	{
		haveOutfile = false; // be sure to disable
		cerr << "ERROR: output file redirection failed." << endl;
	}	
#endif

	LeaveCriticalSection(&LogOUTCriticalSection);

#if (! defined(_DEBUG) && ! defined(FMA_PARSER_LOG) )
////#ifdef _DEBUG
	logPer = LOGP_MISC_CMTS;
#else
	logPer = 0;
#endif

	/* initialize the error string list */
	setupErrorDB();
	return 0; // show all OK even if it's not (defaults will be used)
}
#endif /* TOK or not to TOK */


void logExit(void)
{	
#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (LogLOGCriticalSection.DebugInfo != NULL)
#else
	if (LogLOGCSDebugInfo != NULL)
#endif
	{	
		EnterCriticalSection(&LogLOGCriticalSection);
		wclog.flush();	
		clog.flush();
#ifndef TOK
 #ifdef _DEBUG	// PAW 31/07/09
		clog<<"-- logExit is closing this file."<<endl;
		
		wclog<<"-- logExit is closing this file."<<endl;
 #endif
#endif;	
		if (pHldLogBuf != NULL ) 
			clog.rdbuf(pHldLogBuf); 
		pHldLogBuf = NULL;			
		if ( plogRedir && plogRedir->is_open() )
		{	 
			plogRedir->close(); 
			RAZE(plogRedir);	
		}	
		
		if (pHldwLogBuf != NULL ) 
			wclog.rdbuf(pHldwLogBuf); 
		pHldwLogBuf = NULL;		
		if ( plogwRedir && plogwRedir->is_open() )
		{	 
			plogwRedir->close(); 
			RAZE(plogwRedir);	
		}

		haveLogfile = false;
		LeaveCriticalSection(&LogLOGCriticalSection);
		DeleteCriticalSection(&LogLOGCriticalSection);
#ifndef GE_BUILD	// PAW 30/04/09 function missing
		LogLOGCriticalSection.DebugInfo = NULL;
#else
		LogLOGCSDebugInfo = NULL;
#endif
#ifndef TOK
 #ifdef _DEBUG	// PAW 31/07/09
		cerr<<"-- logExit has closed the clog file."<<endl;
 #endif
#endif
	}	
#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (LogOUTCriticalSection.DebugInfo != NULL)
#else
	if (LogOUTCSDebugInfo != NULL)
#endif
	{
		EnterCriticalSection(&LogOUTCriticalSection);
#ifndef TOK
 #ifdef _DEBUG	// PAW 31/07/09
		cout<<"-- logExit is closing this file."<<endl;
 #endif
#endif
		cout.flush();	
		if (pHldOutBuf != NULL ) 
			cout.rdbuf(pHldOutBuf); 
		pHldOutBuf = NULL;
		if ( poutRedir && poutRedir->is_open())
		{	
			poutRedir->close(); 
			RAZE(poutRedir);	
		}
		
		wcout.flush();	
		if (pHldwOutBuf != NULL ) 
			wcout.rdbuf(pHldwOutBuf); 
		pHldwOutBuf = NULL;		
		if ( poutwRedir && poutwRedir->is_open() )
		{	 
			poutwRedir->close(); 
			RAZE(poutwRedir);	
		}

		haveOutfile = false;
		LeaveCriticalSection(&LogOUTCriticalSection);
		DeleteCriticalSection(&LogOUTCriticalSection);
#ifndef TOK
 #ifdef _DEBUG	// PAW 31/07/09
		cerr<<"-- logExit has closed the cout file."<<endl;
 #endif
#endif
	}
#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (LogERRCriticalSection.DebugInfo != NULL)
#else
	if (LogERRCSDebugInfo != NULL)
#endif
	{	
		EnterCriticalSection(&LogERRCriticalSection);
#ifndef TOK
#ifdef _DEBUG	// PAW 31/07/09
		cerr<<"-- logExit is closing this file."<<endl;
#endif
#endif
		cerr.flush();	
		if (pHldErrBuf != NULL ) 
			cerr.rdbuf(pHldErrBuf); 
		pHldErrBuf = NULL;
		if ( perrRedir && perrRedir->is_open())
		{	
			perrRedir->close(); 
			RAZE(perrRedir);	
		}

		wcerr.flush();	
		if (pHldwErrBuf != NULL ) 
			wcerr.rdbuf(pHldwErrBuf); 
		pHldwErrBuf = NULL;		
		if ( perrwRedir && perrwRedir->is_open() )
		{	 
			perrwRedir->close(); 
			RAZE(perrwRedir);	
		}

		haveErrfile = false;
		LeaveCriticalSection(&LogERRCriticalSection);
		DeleteCriticalSection(&LogERRCriticalSection);
	}

#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (criticalSection.DebugInfo != NULL)
#else
	if (CSDebugInfo != NULL)
#endif
	{
		DeleteCriticalSection(&criticalSection);
	}
#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (LogMsgCriticalSection.DebugInfo != NULL)
#else
	if (LogMsgCSDebugInfo != NULL)
#endif
	{
		DeleteCriticalSection(&LogMsgCriticalSection);
	}
#ifndef GE_BUILD	// PAW 30/04/09 function missing
	if (StatusBufCriticalSection.DebugInfo != NULL)
#else
	if (StatusBufCSDebugInfo != NULL)
#endif
	{
		DeleteCriticalSection(&StatusBufCriticalSection);
	}
	if (UI_log_criticalSection.DebugInfo != NULL)
	{
		DeleteCriticalSection(&UI_log_criticalSection);
	}
}


//////////////////////////////////////////////////
//int __cdecl logout(int,char *,...)
//{
//	return 1;
//}

int logout(int channel, const char* format, ...)
{
	int r = 0;
	va_list vMarker;

	va_start(vMarker, format);
	r = logoutV(channel, format, vMarker);
	va_end(vMarker);
#ifdef TOK
	if (channel & TOKERR) 
	{
		error(DEVICE_OBJECT_ERROR,format);
	}
#endif
	return r;
}
int logout(int channel, const wchar_t* format, ...)
{
	int r = 0;
	va_list vMarker;

	va_start(vMarker, format);
	r = logoutV(channel, format, vMarker);
	va_end(vMarker);

	return r;
}

#ifdef TOK
#define can_output( Ch, Fl )    (channel & Ch )
#else
#define can_output( Ch, Fl )    (channel & Ch ) && Fl 
#endif

// common code for outputting wide strings
int wideOutput(int channel, wstring& lwStr, wstring& wspecial)
{
	// initialize if they haven't been yet.
	verifyCriticalSections();
#ifndef FMA_PARSER_LOG
	if ( ! mainAppWindow)
	{
		GET_UPDATEE(CMainFrame);
	}
#endif// now get down to the output
	
	if (can_output( CERR_LOG, haveErrfile) )
	{	EnterCriticalSection(&LogERRCriticalSection);

		wcerr << lwStr << flush;

		LeaveCriticalSection(&LogERRCriticalSection);
	}
		
	if (can_output( CLOG_LOG, haveLogfile ))
	{	EnterCriticalSection(&LogLOGCriticalSection);
	 
//temp#ifndef _DEBUG
#ifndef NDEBUG
		wclog << lwStr; 
		wclog.flush();
#else
		wclog << lwStr << flush;
#endif

		LeaveCriticalSection(&LogLOGCriticalSection);
	}


	if (can_output( COUT_LOG,  haveOutfile) )
	{	EnterCriticalSection(&LogOUTCriticalSection);

		wcout << lwStr << flush;

		LeaveCriticalSection(&LogOUTCriticalSection);
	}


#ifndef FMA_PARSER_LOG
	if (channel & TRACE_LOG)
	{
		TRACE(lwStr.c_str());
	}

#if !defined(TOK) && !defined(_CONSOLE)	// the tok can't handle these
	if (channel & STAT_LOG)
	{
		wstring locW;// trim length if required
		if (wspecial.length() >= STATUS_LEN)
		{
			locW = wspecial.substr(0,STATUS_LEN);
		}
		else
		{
			locW = wspecial;
		}
		EnterCriticalSection(&StatusBufCriticalSection);
		wcscpy(statusString, wspecial.c_str());
		LeaveCriticalSection(&StatusBufCriticalSection);
		if (mainAppWindow)
		{
			UPDATE_STATUS(statusString);
		}// else do nothing
	}
	if (channel & USRACK_LOG)
	{	
	//temp	MessageBox(
	// stevev 08oct10:was:>	AfxGetMainWnd()->MessageBox(
	GETMAINFRAME->MessageBox(
			/*NULL,*/
			wspecial.c_str() 
			, WERROR_MESSAGE_CAPTION
			, MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
	}
	if (channel & UI_LOG) 
	{		
		EnterCriticalSection(&UI_log_criticalSection);
		string   lStr;
		lStr = TStr2AStr(wspecial);// trimmed to unicode
		char * pNewStr = (char*)lStr.c_str();
		LogMessage(pNewStr);
		LeaveCriticalSection(&UI_log_criticalSection);
	}
#endif
#endif

	return 0;
}
// We are going to have to do the entire conversion in one routine avoid trashing the stack
//
// WARNING: logout(1,"%s format string", "any char pointer");
//			will link to the va_list routine!!!! and give a memory fault!!!
static int logoutV(int channel, const char* format, va_list vL)// va_list is a char*  - for god's sake
{
	if ( format == NULL ) return 1;// failure
	char strActual[LOG_BUF_SIZE];

	vsprintf_s(strActual, LOG_BUF_SIZE, format, vL);

	string   lStr(strActual);
	wstring lwStr,lwSpec;
	lwStr = AStr2TStr(lStr);// to unicode
	lwSpec = remTrailing(lwStr.c_str() );

//?????????????caller should be doing this??????	va_end(tL); // stevev 5aug08 - end stack usage

	return wideOutput(channel, lwStr, lwSpec);
}

int logoutV(int channel, const wchar_t* format, va_list vL)// va_list is a char*-for god's sake
{
	//static bCriticalSectionInit = FALSE;
#ifndef _CONSOLE // Unreferenced in console application
	wchar_t strActual[LOG_BUF_SIZE];
#endif
	wstring value, special;

	if ( format == NULL ) return 1;// failure

	va_list tL = vL;

#ifdef TOK
	// we need Unicode to UTF8 to get it printed in the TOK
	value = format;
	string  ls;
	ls = TStr2AStr(value);
	vsprintf((char*)strActual, ls.c_str(), tL);
	value   = strActual;
	special = L"";
#endif


#if !defined(TOK) && !defined(_CONSOLE)

	vswprintf(strActual, LOG_BUF_SIZE, format, tL);
	value = strActual;

	tL = vL;  // has to be done to reset the stack before each conversion
	wstring sFmt(remTrailing(format));
	vswprintf(strActual, LOG_BUF_SIZE, sFmt.c_str(), tL);
	special = strActual;

	// Instead of rewriting the sizable code for the UI log screen, we'll just handle it here
/********* this must be in wideOutput if we're going to do it	
	if ((channel & UI_LOG) && haveUI_file)
	{
		va_list tL = vL;  // has to be done to reset the stack before each conversion

		int iFormatLen = wcslen(format);
		wchar_t *pchNewFormat = new wchar_t[iFormatLen+1];
		wcscpy(pchNewFormat,format);
		for(int i = 0; i < iFormatLen ; i++)
		{
			if(pchNewFormat[i] == _T('\n') || pchNewFormat[i] == _T('\r')) //If its a newline
			{
				pchNewFormat[i] = _T(' '); //Overwrite with a blank!!
			}

		}
		LogMessageV(pchNewFormat, tL);

		if(pchNewFormat)
		{
			delete [] pchNewFormat;
			pchNewFormat = NULL;
		}
		vL = tL;
		return 0; // stevev 08apr11 - exit here, don't echo via wideOutput...
	}
*********************/

#endif

	//???? should be called in the caller????? va_end(tL); // stevev 5aug08 - end stack usage
	vL = tL;

	return wideOutput(channel, value, special);
	//va_end(vL);// this appears to clean the stack...be careful
}

int  setupErrorDB(void)
{
	return 0; // static init right now...may change
}

int  logout(int channel, int errNumber, ...)
{
	int r = 0;
	va_list vMarker;

	va_start(vMarker, errNumber);
	r = logoutV(channel, errNumber, vMarker);
	va_end(vMarker);

	return r;
}

int  logoutV(int channel, int errNumber, va_list vL)
{
	errorMap_t::iterator pos;

	int fndIdx = 0;
	for (int k = 0; errList[k].err_number != 0; k++)
	{
		if ( errList[k].err_number == errNumber )
		{
			fndIdx = k;
			break; // out of for loop
		}
	}


	if ( errList[fndIdx].err_number != 0 )
	{
		wstring tmpStr;
		if (errNumber >= WARNSTRT && errNumber < ERR_STRT)
		{
			tmpStr = L"WARNING ";
		}
		else 
		if (errNumber >= ERR_STRT)
		{
			tmpStr = L"ERROR ";
		}
		else
		{
			tmpStr = L"PROBLEM ";
		}		
		wchar_t tmpWar[32];
		_itow_s( errNumber, tmpWar, 32, 10 );
		tmpStr += tmpWar;
		tmpStr += L": ";
		tmpStr += errList[fndIdx].err_str;

		return logoutV(channel, (wchar_t *)tmpStr.c_str() , vL);
	}
	else
	{
		return logoutV(channel, L"ERROR: Unknown error number.\n", vL);
	}
}

/* stevev 28jul05 */
void GetStatusString(wchar_t statusRetStr[])
{
	try
	{
		memcpy(statusRetStr, statusString, (wcslen(statusString)+1)*sizeof(wchar_t));
	}
	catch(...)
	{
		LogMessage("Call to GetStatusString function without allocating enough memory");
		return;
	}
	return;
}

/*<START> Added by ANOOP for cleaning up the error log	*/
void ClearLog()
{
	LogMessageList.clear();
}
/*<END> Added by ANOOP for cleaning up the error log	*/
#ifndef _WIN32_WCE			// PAW 24/04/09
struct _timeb baseTime, lastTime, wrkTm;
#else
time_t_ce baseTime;
time_t_ce baseTimeMS;
time_t_ce lastTime;
time_t_ce lastTimeMS;
time_t_ce wrkTm;
time_t_ce wrkTmMS;
time_t_ce start_ce;	
#endif
/** see hi res time info below the time routines */	
void captureStartTime(void)
{
#ifndef _WIN32_WCE			// PAW 24/04/09
	_ftime_s( &baseTime );
	memcpy(&lastTime, &baseTime, sizeof(struct _timeb));
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	baseTimeMS = st.wMilliseconds;
	start_ce = time_ce(&baseTime);	
	lastTime = baseTime;
	lastTimeMS = baseTimeMS;
#endif
}

void logTime(void)
{
	if LOGTHIS(LOGP_START_STOP)
	{
#ifndef _WIN32_WCE			// PAW 24/04/09
		_ftime_s( &wrkTm );
		int eMillitime = wrkTm.millitm - baseTime.millitm;
		INT64 eTime    = wrkTm.time    - baseTime.time;
		if (eMillitime < 0) 
		{	eMillitime+=1000; eTime -= 1;
		}

		int sMillitime = wrkTm.millitm - lastTime.millitm;
		INT64 sTime    = wrkTm.time    - lastTime.time;
		if (sMillitime < 0) 
		{	sMillitime+=1000; sTime -= 1;
		}	

		

		LOGIT(CLOG_LOG," Elapsed time = %I64d.%d secs Split: %I64d.%d\n",	
													eTime,eMillitime,  sTime,sMillitime);
			
		memcpy(&lastTime, &wrkTm, sizeof(struct _timeb));
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	wrkTmMS = st.wMilliseconds;
	start_ce = time_ce(&wrkTm);	// time

	int eMillitime = wrkTmMS - baseTimeMS;
	INT64 eTime    = wrkTm    - baseTime;
	if (eMillitime < 0) 
	{	eMillitime+=1000; eTime -= 1;
	}
	int sMillitime = wrkTmMS - lastTimeMS;
	INT64 sTime    = wrkTm    - lastTime;
	if (sMillitime < 0) 
	{	sMillitime+=1000; sTime -= 1;
	}	

	LOGIT(CLOG_LOG," Elapsed time = %I64d.%d secs Split: %I64d.%d\n",	
													eTime,eMillitime,  sTime,sMillitime);
	lastTime = wrkTm;
	lastTimeMS = wrkTmMS;
#endif
		
	}
	// else it's an empty call
}

LARGE_INTEGER freq = { 0 };// counts/sec

void logCurrentTime(void)
{
	if ( freq.QuadPart == 0 )
		QueryPerformanceFrequency( &freq );
	if LOGTHIS(LOGP_START_STOP)
	{
		if ( freq.QuadPart == 0 )
		{
			_ftime_s( &wrkTm );
			LOGIT(CLOG_LOG," Current time = %d.%d secs\n", wrkTm.time, wrkTm.millitm);		
		}
		else // we have a hi-res
		{
			LARGE_INTEGER t;
			QueryPerformanceCounter(&t);
			double k = ((double)t.QuadPart / (double)freq.QuadPart);
			LOGIT(CLOG_LOG," Current time = %f secs\n", k);
		}
	}
	// else it's an empty call
}



#ifdef GE_BUILD

void ShowTime(bool bEnabled,char *pLogMsg)
{
	if(!bEnabled)
		return;
static CStopWatch stpWtch;
__int64 elapsed = stpWtch.Time();
	LOGIT(LOG_DEBUG,"%s %d\n",pLogMsg,elapsed);
	stpWtch.Reset();
}




//////////////////////////////////////////////////////////////////////////
//
//	Here to avoid putting CFile in the .h file
//
//
BOOL ReadAnsiLine(CFile * pFile, char* pBuf, int maxLen)		{
	int i = 0;
	char ch;	
	memset(pBuf,0,maxLen);
	while( pFile->Read(&ch,1)>0 && ch!='\n' && i < maxLen )		{
		*pBuf++ = ch;
		i++;
	}
	if(i > 0 || i >= maxLen)	{
		return TRUE;
	}
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////
//
//	Checks in a file for supported devices
//	allows us to ship a full DD but only use it for devices which actually work
//
//	returns 0 to continue
//  which can be a result of no file, a match or "all" at the start of the file
//
//	
//	Mod to force a higher software revision to a tested revision
//	
//	
//	
//	
int ChkSupportedDevice(LPCTSTR filename,int manuf,int dev,int rev1,BYTE *rev2)	{
	int lnManuf,lnDev,lnRev1,lnRev2;
	CString text;
	CFile file;	// destructor does the file close

	char buff[24];
	if ( file.Open ( filename, CFile::modeRead ) )	{
		if(ReadAnsiLine(&file,buff,sizeof(buff))!= FALSE)	{
			// using ansi
			if(_strnicmp("ALL",buff,3) == 0)	{
				// use all
				return 0;
			}
		}

		int maxSwRevInFile = -1;
		do	{
			Tokenise(buff,",\\/.",lnManuf,lnDev,lnRev1,lnRev2);
			if(lnManuf == manuf && lnDev == dev && lnRev1 == rev1)	{
	
				// software revision does not always have a dd and uses the next one down
				if(lnRev2 == *rev2)	{
					return 0;
				}
				else	{
					// record the highest found, but not higher than requested
					if(maxSwRevInFile <= lnRev2 && maxSwRevInFile < *rev2 )	{
						maxSwRevInFile = lnRev2;
					}
				}
				// found
			}
		}	while(ReadAnsiLine(&file,buff,sizeof(buff))!= FALSE);

		// end of file reached
		// did we find another sw rev?
		if(maxSwRevInFile >=0)	{
			*rev2 = maxSwRevInFile;	// pass back the new sw rev
			return 0;
		}			
		return -1;
	}
	return 0;
}

int Tokenise(char* str,const char* delim,int& manuf,int& dev,int& rev1,int& rev2)
{
	char* token;
	char* inString = str;	

	token = strtok(inString, delim );
	int i=0;
	int val;
	while( token != NULL && i < 4 )   {      /* While there are tokens in "string" */
		sscanf(token,"%x",&val);
		switch(i)	{
			case 0:
				manuf = val;
				break;
			case 1:
				dev = val;
				break;
			case 2:
				rev1 = val;
				break;
			case 3:
				rev2 = val;
				break;
			default:
				break;
		}
		i++;
		token = strtok( NULL, delim);
	}
	return i;
}

#endif // GE_BUILD

/* ------------------------ End Of File --------------------------------- */
/************************************************************************************************
 *
 *   $History: logging.cpp $
 *
 ************************************************************************************************
 */

