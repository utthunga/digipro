/*************************************************************************************************
 *
 * $Workfile: errors.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * This file is the
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		       error list
 *
 * #include "errors.h"
 *
 */


#ifndef _ERRORS_H
#define _ERRORS_H

#ifndef RETURNCODE

#define RETURNCODE  int

#define SUCCESS			(0)
#define FAILURE			(1)

#endif
/*************************************************************************************************
 * NOTES:
 *		The descriptions and usage should be in errorDesc.h
 *		Any changes here should be extended to that file.
 *		
 *		ddbGeneral.h holds 5000, 6000, 7000, 8000 another 10000, 11000, 12000
 */

//#define LIBRARIANERRORS		10000

//#define LIB_NOMEMORY		(LIBRARIANERRORS +  1)
//#define LIB_PROGRAMMER		(LIBRARIANERRORS +  2)


//#define LIB_TEST		                          (LIBRARIANERRORS + 10)





/* error codes */
//#define RDR_ERROR_BASE  4000

//#define PARAMERR		(RDR_ERROR_BASE +  1)
//#define FILEOPENERROR	(RDR_ERROR_BASE +  2)
//#define FILEREADERROR   (RDR_ERROR_BASE +  3)




#endif // _ERRORS_H

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: errors.h $
 *
 *************************************************************************************************
 */
