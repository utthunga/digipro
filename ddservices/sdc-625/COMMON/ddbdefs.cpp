/**********************************************************************************************
 *
 * $Workfile: ddbdefs.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		holds global definitions and instantiates string helpers
 *		
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */
// stevev 20feb07 - merge- noway...use the general   #include "stdafx.h"  // HOMZ Port to 2003

#include "ddbdefs.h"
#include "ddbGeneral.h"
#include "DDLDEFS.H"
#include "logging.h"
#ifndef FMAPARSER
#include "dllapi.h"

#include "softfloat.h"
#endif // FMAPARSER

#ifndef _T
#define _T( a )  L ## a
#endif

#if defined(__GNUC__)
#include <cmath>
#endif // __GNUC__

/* * * * * global strings * * * * */
char itemStrings   [iT_MaxType+8]   [ITMAXLEN]         = {ITEMTYPESTRINGS};
char  varTypeStrings[VARTYPESTRCNT]  [VARTYPESTRMAXLEN] = {VARTYPESTRINGS};
char  varAttrStrings[VARATTRSTRCOUNT][VARATTRSTRMAXLEN]  = {VARATTRSTRINGS};
wchar_t  varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN]     = {CLASSSTRINGS};
char  axiomStrings  [5]              [AXIOMSTRMAXLEN]    = {AXIOMSTRINGS};
char  clauseStrings [8]              [CLAUSESTRMAXLEN]   = {CLAUSESTRINGS};
char  payloadStrings[PAYLDSTRCOUNT]  [PAYLDSTRMAXLEN]    = {PAYLDSTRINGS};
char  refTypeStrings[REFSTRCOUNT]    [REFMAXLEN]        = {REFTYPESTRINGS};
char  cmdAttrStrings[CMDATRSTRCOUNT] [CMDATRMAXLEN]    = {CMDATRTYPESTRINGS};
char  menuAttrStrings[MENUATRSTRCOUNT] [MENUATRMAXLEN]  = {MENUATRTYPESTRINGS};
char  edDispTypeStrings[EDDISPSTRCOUNT][EDDISPMAXLEN]    = {EDDISPTYPESTRINGS};
char  methodTypeStrings[METHODSTRCOUNT][METHODMAXLEN]    = {METHODTYPESTRINGS};
char   refreshTypeStrings[3][15] = {{"Refresh Items"},{"Debug Info"},{"Unknown"}};
char   unitTypeStrings[5][20]    = {{"Unit Relation Items"},{"Debug Info"},{"Unknown"},{"Watch Var"},{"Update List"}};
char   waoTypeStrings[3][20]     = {{"Write-As-One Items"},{"Debug Info"},{"Unknown"}};
//char  collAttrStrings[COLLATRSTRCOUNT] [COLLATRMAXLEN]     = {COLLATRTYPESTRINGS};
//char  itmArrAttrStrings[ITMARRATRSTRCOUNT] [ITMARRATRMAXLEN]     = {ITMARRATRTYPESTRINGS};
char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN] = {GRPITMATRTYPESTRINGS};
char exprElemStrings[EXPELEMSTRCNT][EXPELEMSTRMAXLEN] = {EXPELEMSTRINGS};
char arrAttrStrings[ARRATRSTRCOUNT] [ARRATRMAXLEN]     = {ARRATRTYPESTRINGS};
char fileAttrStrings[FILEATRSTRCOUNT] [FILEATRMAXLEN]     = {FILEATRTYPESTRINGS};
char chartAttrStrings[CHRTATRSTRCOUNT] [CHRTATRMAXLEN]     = {CHRTATRTYPESTRINGS};
char graphAttrStrings[GRPHTATRSTRCOUNT] [GRPHTATRMAXLEN]     = {GRPHATRTYPESTRINGS};
char axisAttrStrings[AXISTATRSTRCOUNT] [AXISTATRMAXLEN]     = {AXISATRTYPESTRINGS};
char waveformAttrStrings[WAVEFORMTATRSTRCOUNT] [WAVEFORMTATRMAXLEN] = {WAVEFORMATRTYPESTRINGS};
char sourceAttrStrings[SOURCETATRSTRCOUNT] [SOURCETATRMAXLEN]     = {SOURCEATRTYPESTRINGS};
const char listAttrStrings[LISTTATRSTRCOUNT] [LISTTATRMAXLEN]     = {LISTATRTYPESTRINGS};
const char gridAttrStrings[GRIDTATRSTRCOUNT] [GRIDTATRMAXLEN]     = {GRIDATRTYPESTRINGS};
const char imageAttrStrings[IMAGETATRSTRCOUNT] [IMAGETATRMAXLEN]     = {IMAGEATRTYPESTRINGS};
const char blobAttrStrings[BLOBATTRSTRCOUNT] [BLOBATTRMAXLEN]     = {BLOBATRTYPESTRINGS};
const char templAttrStrings[TEMPLATTRSTRCOUNT] [TEMPLATTRMAXLEN]  = {TEMPLATRTYPESTRINGS};
const char pluginAttrStrings[PLUGINATTRSTRCOUNT] [PLUGINATTRMAXLEN]= {PLUGINATRTYPESTRINGS};

char instanceDataStStrings[6][INSTANCEDATASTATEMAXLEN] = {INSTANCEDATASTATESTRINGS};
char dataQualityStrings[4][DATAQUALITYMAXLEN] = {DATAQUALITYSTRINGS};


#ifndef FMAPARSER
char varientTypeStrings[VARIENTYPECOUNT][VARIENTTYPE_LEN] = {VARIENTTYPESTRING};
char methodVarTypeStrings[METHODVARSTRCOUNT][METHODVARMAXLEN] = {METHODVARTYPESTRINGS};

char varTimeScaleStrings[TIMESCALECOUNT][TIMESCALEMAXLEN] = {TIMESCALESTRINGS};

/* * * * * global helper functions * * * * */

axiomType_t  axiomFrom(expressionType_t e)
{
	if (e < eT_Unknown || e == eT_CASE_expr || e > eT_Direct)
	{
		LOGIT(CERR_LOG,"ERROR: non-axiom expression converting to axiom.\n");
		return aT_Unknown;
	}
	else
	{
		return (axiomType_t)e;
	}
}

clauseType_t clauseFrom(expressionType_t e)
{
	if (e < eT_Unknown || e == eT_IF_expr || e == eT_SEL_expr || e > et_SEL_isDEFAULT )
	{
		LOGIT(CERR_LOG,"ERROR: non-clause expression converting to clause.\n");
		return cT_Unknown;
	}
	else
	{// leaves 0 & 3thru7
		return (clauseType_t)e;
	}
}


varAttrType_t getVarAttrType(ulong aMask)
{
	switch (aMask)
	{
	case VAR_CLASS			:	return varAttrClass;break;
	case VAR_HANDLING		:	return varAttrHandling;break;
	case VAR_UNIT			:	return varAttrConstUnit;break;
	case VAR_LABEL			:	return varAttrLabel;break;

	case VAR_HELP			:	return varAttrHelp;break;
	case VAR_WIDTHSIZE		:	return varAttrWidth;break;
	case VAR_HEIGHTSIZE		:	return varAttrHeight;break;

	case VAR_VALID			:	return varAttrValidity;break;

	case VAR_PRE_READ_ACT	:	return varAttrPreReadAct;break;
	case VAR_POST_READ_ACT	:	return varAttrPostReadAct;break;
	case VAR_PRE_WRITE_ACT	:	return varAttrPreWriteAct;break;
	case VAR_POST_WRITE_ACT	:	return varAttrPostWriteAct;break;

	case VAR_PRE_EDIT_ACT	:	return varAttrPreEditAct;break;
	case VAR_POST_EDIT_ACT	:	return varAttrPostEditAct;break;
	case VAR_RESP_CODES		:	return varAttrResponseCodes;break;
	case VAR_TYPE_SIZE		:	return varAttrTypeSize;break;

	case VAR_DISPLAY		:	return varAttrDisplay;break;
	case VAR_EDIT			:	return varAttrEdit;break;
	case VAR_MIN_VAL		:	return varAttrMinValue;break;
	case VAR_MAX_VAL		:	return varAttrMaxValue;break;

	case VAR_SCALE			:	return varAttrScaling;break;
	case VAR_ENUMS			:	return varAttrEnums;break;
	case VAR_INDEX_ITEM_ARRAY:	return varAttrIndexItemArray;break;
	case VAR_DEFAULT_VALUE	:   return varAttrDefaultValue; break;
	case VAR_REFRESH_ACT	:   return varAttrRefreshAct; break;
	case VAR_DEBUG			:   return varAttrDebugInfo; break;		// 25
	case VAR_PRIVATE		:	return varAttrPrivate; break;		// 31
#ifdef XMTR	
	case VAR_POST_RQST_ACT	:   return varAttrPostRequestAct; break;
	case VAR_POST_USER_ACT	:   return varAttrPostUserAct; break;
#endif
	}
	return varAttrLastvarAttr;
}

genericTypeEnum_t getAttrType4Mask(CitemType& itmType, ulong aMask)
{
	genericTypeEnum_t rt;
	itemType_t        it = (itemType_t)itmType.getType();
//LOGIT(CERR_LOG,"getAttrType4Mask %08x \n",aMask);
	switch(it)
	{
	case iT_Variable:
		rt.gAttrVar = getVarAttrType(aMask);
		break;
	case iT_Command:
		if ( aMask > (1<<cmdAttrUnknown) || aMask < 0 )
		{
			rt.gAttrCmd = cmdAttrUnknown;
			LOGIT(CERR_LOG,"ERROR: Command attribute unknown. (0x%x)\n", aMask);
		}
		else
		{
			if (aMask == (1<<cmdAttrNumber))
			{	rt.gAttrCmd = cmdAttrNumber;}
			else
			if  (aMask == (1<<cmdAttrOperation))
			{	rt.gAttrCmd = cmdAttrOperation;}
			else
			if  (aMask == (1<<cmdAttrTransaction))
			{	rt.gAttrCmd = cmdAttrTransaction;}
			else
			if  (aMask == (1<<cmdAttrRespCodes))
			{	rt.gAttrCmd = cmdAttrRespCodes;}
			else
			if  (aMask == (1<<cmdAttrDebugInfo))
			{	rt.gAttrCmd = cmdAttrDebugInfo;}
			else
			{	rt.gAttrCmd = cmdAttrUnknown; }
		}
		break;
	case iT_Menu:
		if ( aMask > (1<<menuAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrMenu = menuAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Menu attribute unknown. (0x%x)\n", aMask);
		}
		else
		{
			if (aMask == (1<<menuAttr_Label))
			{	rt.gAttrMenu = menuAttr_Label;}
			else
			if  (aMask == (1<<menuAttr_Items))
			{	rt.gAttrMenu = menuAttr_Items;}
			else
			if  (aMask == (1<<menuAttr_Help))
			{	rt.gAttrMenu = menuAttr_Help;}
			else
			if  (aMask == (1<<menuAttr_Validity))
			{	rt.gAttrMenu = menuAttr_Validity;}
			else
			if  (aMask == (1<<menuAttr_Style))
			{	rt.gAttrMenu = menuAttr_Style;}
			else
			if  (aMask == (1<<menuAttrDebugInfo))
			{	rt.gAttrMenu = menuAttrDebugInfo;}
			else
			{	rt.gAttrMenu = menuAttr_Unknown; }
		}
		break;
	case iT_EditDisplay:
		if ( aMask > (1<<eddispAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrEdDisp = eddispAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Edit Display attribute unknown. (0x%x)\n", aMask);
		}
		else
		{
			if (aMask == (1<<eddispAttrLabel))
			{	rt.gAttrEdDisp = eddispAttrLabel;}
			else
			if  (aMask == (1<<eddispAttrEditItems))
			{	rt.gAttrEdDisp = eddispAttrEditItems;}
			else
			if (aMask == (1<<eddispAttrDispItems))
			{	rt.gAttrEdDisp = eddispAttrDispItems;}
			else
			if  (aMask == (1<<eddispAttrPreEditAct))
			{	rt.gAttrEdDisp = eddispAttrPreEditAct;}
			else
			if  (aMask == (1<<eddispAttrPostEditAct))
			{	rt.gAttrEdDisp = eddispAttrPostEditAct;}
			else
			if  (aMask == (1<<eddispAttrHelp))
			{	rt.gAttrEdDisp = eddispAttrHelp;}
			else
			if  (aMask == (1<<eddispAttrValidity))
			{	rt.gAttrEdDisp = eddispAttrValidity;}
			else
			if  (aMask == (1<<eddispAttrDebugInfo))
			{	rt.gAttrEdDisp = eddispAttrDebugInfo;}
			else
			{	rt.gAttrEdDisp = eddispAttr_Unknown; }
		}
		break;
	case iT_Method:
		if ( aMask > (1<<methodAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrMethod = methodAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Method attribute unknown. (0x%x)\n", aMask );
		}
		else
		{
			if (aMask == (1<<methodAttrClass))
			{	rt.gAttrMethod = methodAttrClass;}
			else
			if  (aMask == (1<<methodAttrLabel))
			{	rt.gAttrMethod = methodAttrLabel;}
			else
			if  (aMask == (1<<methodAttrHelp))
			{	rt.gAttrMethod = methodAttrHelp;}
			else
			if  (aMask == (1<<methodAttrDefinition))
			{	rt.gAttrMethod = methodAttrDefinition;}
			else
			if  (aMask == (1<<methodAttrValidity))
			{	rt.gAttrMethod = methodAttrValidity;}
			else
			if  (aMask == (1<<methodAttrScope))
			{	rt.gAttrMethod = methodAttrScope;}
			else
			if (aMask == (1<<methodAttrType))
			{	rt.gAttrMethod = methodAttrType;}
			else
			if  (aMask == (1<<methodAttrParams))
			{	rt.gAttrMethod = methodAttrParams;}
			else
			if  (aMask == (1<<methodAttrDebugInfo))
			{	rt.gAttrMethod = methodAttrDebugInfo;}
			else
			if (aMask == (1 << methodAttrVisible))
			{	rt.gAttrMethod = methodAttrVisible;	}
			else
			if (aMask == (1 << methodAttrPrivate))
			{	rt.gAttrMethod = methodAttrPrivate;	}
			else
			{	rt.gAttrMethod = methodAttr_Unknown; }	
		}
		break;
	case iT_Refresh:
		if ( aMask > (1<<refreshAttr_Watch) || aMask < 0 )
		{
			rt.gAttrRefresh = refreshAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Unit attribute unknown. ( %08x )\n", aMask);
		}
		else
		{	
			if  (aMask == (1<<refreshAttrItems))
			{	rt.gAttrRefresh = refreshAttrItems;}
			else
			if  (aMask == (1<<refreshAttrDebugInfo))
			{	rt.gAttrRefresh = refreshAttrDebugInfo;}
			else
			if  (aMask == (1<<refreshAttr_Update))
			{	rt.gAttrRefresh = refreshAttr_Update;}
			else
			if  (aMask == (1<<refreshAttr_Watch))
			{	rt.gAttrRefresh = refreshAttr_Watch;}
			else
			{	rt.gAttrRefresh = refreshAttr_Unknown; }
		}
		break;
	case iT_Unit:
		if ( aMask > (1<<unitAttr_Update) || aMask < 0 )
		{
			rt.gAttrUnit = unitAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Refresh attribute unknown. ( %08x )\n", aMask);
		}
		else
		{	
			if  (aMask == (1<<unitAttrItems))
			{	rt.gAttrUnit = unitAttrItems;}
			else
			if  (aMask == (1<<unitAttrDebugInfo))
			{	rt.gAttrUnit = unitAttrDebugInfo;}
			else
			if  (aMask == (1<<unitAttr_Var))
			{	rt.gAttrUnit = unitAttr_Var;}
			else
			if  (aMask == (1<<unitAttr_Update))
			{	rt.gAttrUnit = unitAttr_Update;}
			else
			{	rt.gAttrUnit = unitAttr_Unknown; }
		}
		break;
	case iT_WaO:
		if ( aMask > (1<<waoAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrWao = waoAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Write-As-One attribute unknown. ( %08x )\n", aMask);
		}
		else
		{	
			if  (aMask == (1<<waoAttrItems))
			{	rt.gAttrWao = waoAttrItems;}
			else
			if  (aMask == (1<<waoAttrDebugInfo))
			{	rt.gAttrWao = waoAttrDebugInfo;}
			else
			{	rt.gAttrWao = waoAttr_Unknown; }
		}
		break;
	case iT_ItemArray:
		{
			if (aMask == (1<< ITEM_ARRAY_ELEMENTS_ID))
			{	rt.gAttrGrpItm = grpItmAttrElements;}
			else
			if  (aMask == (1<< ITEM_ARRAY_LABEL_ID))
			{	rt.gAttrGrpItm = grpItmAttrLabel;}
			else
			if (aMask == (1<< ITEM_ARRAY_HELP_ID))
			{	rt.gAttrGrpItm = grpItmAttrHelp;}
			else
			if (aMask == (1<< ITEM_ARRAY_VALIDITY_ID))
			{	rt.gAttrGrpItm = grpItmAttrValidity;}
			else
			if (aMask == (1<< ITEM_ARRAY_DEBUG_ID))
			{	rt.gAttrGrpItm = grpItmAttrDebugInfo;}
			else
			if (aMask == (1<< ITEM_ARRAY_PRIVATE_ID))
			{	rt.gAttrGrpItm = grpItmAttrPrivate;}
			else
			{	
				rt.gAttrGrpItm = grpItmAttrUnknown; 
				LOGIT(CERR_LOG, "ERROR: Item Array attribute unknown. ( %08x )\n", aMask);
			}
		}
		break;
	case iT_Collection:
		{
			if (aMask == (1<< COLLECTION_MEMBERS_ID))
			{	rt.gAttrGrpItm = grpItmAttrElements;}
			else
			if  (aMask == (1<< COLLECTION_LABEL_ID))
			{	rt.gAttrGrpItm = grpItmAttrLabel;}
			else
			if (aMask == (1<< COLLECTION_HELP_ID))
			{	rt.gAttrGrpItm = grpItmAttrHelp;}
			else
			if (aMask == (1<< COLLECTION_VALID_ID))
			{	rt.gAttrGrpItm = grpItmAttrValidity;}
			else
			if (aMask == (1<< COLLECTION_DEBUG_ID))
			{	rt.gAttrGrpItm = grpItmAttrDebugInfo;}
			else
			if (aMask == (1<< COLLECTION_PRIVATE_ID))
			{	rt.gAttrGrpItm = grpItmAttrPrivate;}
			else
			{	
				rt.gAttrGrpItm = grpItmAttrUnknown; 
				LOGIT(CERR_LOG, "ERROR: Collection attribute unknown. ( %08x )\n", aMask);
			}
		}
		break;

	case iT_Block:
	case iT_Record:
	case iT_ResponseCodes://??
	case iT_Member:	//??
		// unsupported
		rt.gAttrVar = varAttrLastvarAttr;
		break;

	case iT_Array:
		if ( aMask > (1<<arrayAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrArray = arrayAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Array attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<arrayAttrLabel))
			{	rt.gAttrArray = arrayAttrLabel;}
			else
			if  (aMask == (1<<arrayAttrHelp))
			{	rt.gAttrArray = arrayAttrHelp;}
			else
			if (aMask == (1<<arrayAttrValidity))
			{	rt.gAttrArray = arrayAttrValidity;}
			else
			if (aMask == (1<<arrayAttrType))
			{	rt.gAttrArray = arrayAttrType;}
			else
			if (aMask == (1<<arrayAttrElementCnt))
			{	rt.gAttrArray = arrayAttrElementCnt;}
			else
			if (aMask == (1<<arrayAttrDebugInfo))
			{	rt.gAttrArray = arrayAttrDebugInfo;}
			else
			if (aMask == (1<<arrayAttrPrivate))
			{	rt.gAttrArray = arrayAttrPrivate;}
			else
			{	rt.gAttrArray = arrayAttr_Unknown; }
		}
		break;
	case iT_File:				//20
		{
			if (aMask == (1<< FILE_MEMBERS_ID))
			{	rt.gAttrGrpItm = grpItmAttrElements;}//FILE_MEMBERS_ID
			else
			if  (aMask == (1<< FILE_LABEL_ID))
			{	rt.gAttrGrpItm = grpItmAttrLabel;}//FILE_LABEL_ID
			else
			if (aMask == (1<< FILE_HELP_ID))
			{	rt.gAttrGrpItm = grpItmAttrHelp;}//FILE_HELP_ID
			else
			if (aMask == (1<< FILE_DEBUG_ID))
			{	rt.gAttrGrpItm = grpItmAttrDebugInfo;}// FILE_DEBUG_ID
			else
			if  (aMask == (1<< FILE_HANDLING_ID))
			{	rt.gAttrGrpItm = grpItmAttrHandling;}//FILE_HANDLING_ID
			else
			if (aMask == (1<< FILE_UPDATE_ACTIONS_ID))
			{	rt.gAttrGrpItm = grpItmAttrUpdateSActions;}//FILE_UPDATE_ACTIONS_ID
			else
			if (aMask == (1<< FILE_IDENTITY_ID))
			{	rt.gAttrGrpItm = grpItmAttrIdentity;}//FILE_IDENTITY_ID
			else
			if (aMask == (1 << FILE_PRIVATE_ID))
			{	rt.gAttrGrpItm = grpItmAttrPrivate;}// FILE_PRIVATE_ID
			else 
			if (aMask == (1 << FILE_SHARED_ID))
			{	rt.gAttrGrpItm = grpItmAttrShared;	}// FILE_SHARED_ID
			else
			{	rt.gAttrGrpItm = grpItmAttrUnknown; }
		}
		break;
	case iT_Chart:				//21
		if ( aMask > (1<<chartAttrUnknown) || aMask < 0 )
		{
			rt.gAttrChart = chartAttrUnknown;
			LOGIT(CERR_LOG,"ERROR: Chart attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<chartAttrLabel))
			{	rt.gAttrChart = chartAttrLabel;}
			else
			if  (aMask == (1<<chartAttrHelp))
			{	rt.gAttrChart = chartAttrHelp;}
			else
			if (aMask == (1<<chartAttrValidity))
			{	rt.gAttrChart = chartAttrValidity;}
			else
			if (aMask == (1<<chartAttrHeight))
			{	rt.gAttrChart = chartAttrHeight;}
			else
			if (aMask == (1<<chartAttrWidth))
			{	rt.gAttrChart = chartAttrWidth;}
			else
			if  (aMask == (1<<chartAttrType))
			{	rt.gAttrChart = chartAttrType;}
			else
			if  (aMask == (1<<chartAttrLength))
			{	rt.gAttrChart = chartAttrLength;}
			else
			if  (aMask == (1<<chartAttrCycleTime))
			{	rt.gAttrChart = chartAttrCycleTime;}
			else
			if  (aMask == (1<<chartAttrMembers))
			{	rt.gAttrChart = chartAttrMembers;}
			else
			if  (aMask == (1<<chartAttrDebugInfo))
			{	rt.gAttrChart = chartAttrDebugInfo;}
			else
			{	rt.gAttrChart = chartAttrUnknown; }
		}
		break;
	case iT_Graph:				//22
		if ( aMask > (1<<graphAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrGraph = graphAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Graph attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<graphAttrLabel))
			{	rt.gAttrGraph = graphAttrLabel;}
			else
			if  (aMask == (1<<graphAttrHelp))
			{	rt.gAttrGraph = graphAttrHelp;}
			else
			if (aMask == (1<<graphAttrValidity))
			{	rt.gAttrGraph = graphAttrValidity;}
			else
			if (aMask == (1<<graphAttrHeight))
			{	rt.gAttrGraph = graphAttrHeight;}
			else
			if (aMask == (1<<graphAttrWidth))
			{	rt.gAttrGraph = graphAttrWidth;}
			else
			if  (aMask == (1<<graphAttrXAxis))
			{	rt.gAttrGraph = graphAttrXAxis;}
			else
			if  (aMask == (1<<graphAttrMembers))
			{	rt.gAttrGraph = graphAttrMembers;}
			else
			if  (aMask == (1<<graphAttrDebugInfo))
			{	rt.gAttrGraph = graphAttrDebugInfo;}
			else
			if  (aMask == (1<<graphAttrCycleTime))
			{	rt.gAttrGraph = graphAttrCycleTime;}
			else
			{	rt.gAttrGraph = graphAttr_Unknown; }
		}
		break;
	case iT_Axis:				//23
		if ( aMask > (1<<axisAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrAxis = axisAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Axis attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<axisAttrLabel))
			{	rt.gAttrAxis = axisAttrLabel;}
			else
			if  (aMask == (1<<axisAttrHelp))
			{	rt.gAttrAxis = axisAttrHelp;}
			else
			//if (aMask == (1<<axisAttrValidity))	no longer a valid attribute
			//{	rt.gAttrAxis = axisAttrValidity;}
			//else
			if (aMask == (1<<axisAttrMinVal))
			{	rt.gAttrAxis = axisAttrMinVal;}
			else
			if (aMask == (1<<axisAttrMaxVal))
			{	rt.gAttrAxis = axisAttrMaxVal;}
			else
			if  (aMask == (1<<axisAttrScale))
			{	rt.gAttrAxis = axisAttrScale;}
			else
			if  (aMask == (1<<axisAttrUnit))
			{	rt.gAttrAxis = axisAttrUnit;}
			else
			if  (aMask == (1<<axisAttrDebugInfo))
			{	rt.gAttrAxis = axisAttrDebugInfo;}
			else
			{	rt.gAttrAxis = axisAttr_Unknown; }
		}
		break;
	case iT_Waveform:			//24
		if ( aMask > (1<<waveformAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrWaveform = waveformAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Waveform attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<waveformAttrLabel))
			{	rt.gAttrWaveform = waveformAttrLabel;}
			else
			if  (aMask == (1<<waveformAttrHelp))
			{	rt.gAttrWaveform = waveformAttrHelp;}
			else
			if (aMask == (1<<waveformAttrHandling))
			{	rt.gAttrWaveform = waveformAttrHandling;}
			else
			if (aMask == (1<<waveformAttrEmphasis))
			{	rt.gAttrWaveform = waveformAttrEmphasis;}
			else
			if (aMask == (1<<waveformAttrLineType))
			{	rt.gAttrWaveform = waveformAttrLineType;}
			else
			if  (aMask == (1<<waveformAttrLineColor))
			{	rt.gAttrWaveform = waveformAttrLineColor;}
			else
			if  (aMask == (1<<waveformAttrYAxis))
			{	rt.gAttrWaveform = waveformAttrYAxis;}
			else
			if  (aMask == (1<<waveformAttrKeyPtX))
			{	rt.gAttrWaveform = waveformAttrKeyPtX;}
			else
			if  (aMask == (1<<waveformAttrKeyPtY))
			{	rt.gAttrWaveform = waveformAttrKeyPtY;}
			else
			if  (aMask == (1<<waveformAttrType))
			{	rt.gAttrWaveform = waveformAttrType;}
			else
			if  (aMask == (1<<waveformAttrXVals))
			{	rt.gAttrWaveform = waveformAttrXVals;}
			else
			if  (aMask == (1<<waveformAttrYVals))
			{	rt.gAttrWaveform = waveformAttrYVals;}
			else
			if  (aMask == (1<<waveformAttrXinitial))
			{	rt.gAttrWaveform = waveformAttrXinitial;}
			else
			if  (aMask == (1<<waveformAttrXIncr))
			{	rt.gAttrWaveform = waveformAttrXIncr;}
			else
			if  (aMask == (1<<waveformAttrPtCount))
			{	rt.gAttrWaveform = waveformAttrPtCount;}
			else
			if  (aMask == (1<<waveformAttrInitActions))
			{	rt.gAttrWaveform = waveformAttrInitActions;}
			else
			if  (aMask == (1<<waveformAttrRfshActions))
			{	rt.gAttrWaveform = waveformAttrRfshActions;}
			else
			if  (aMask == (1<<waveformAttrExitActions))
			{	rt.gAttrWaveform = waveformAttrExitActions;}
			else
			if  (aMask == (1<<waveformAttrDebugInfo))
			{	rt.gAttrWaveform = waveformAttrDebugInfo;}
			else
			if  (aMask == (1<<waveformAttrValidity))
			{	rt.gAttrWaveform = waveformAttrValidity;}
			else
			{	rt.gAttrWaveform = waveformAttr_Unknown; }
		}
		break;
	case iT_Source:				//25
		if ( aMask > (1<<sourceAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrSource = sourceAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Source attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<sourceAttrLabel))
			{	rt.gAttrSource = sourceAttrLabel;}
			else
			if  (aMask == (1<<sourceAttrHelp))
			{	rt.gAttrSource = sourceAttrHelp;}
			else
			if (aMask == (1<<sourceAttrValidity))
			{	rt.gAttrSource = sourceAttrValidity;}
			else
			if (aMask == (1<<sourceAttrEmphasis))
			{	rt.gAttrSource = sourceAttrEmphasis;}
			else
			if (aMask == (1<<sourceAttrLineType))
			{	rt.gAttrSource = sourceAttrLineType;}
			else
			if  (aMask == (1<<sourceAttrLineColor))
			{	rt.gAttrSource = sourceAttrLineColor;}
			else
			if  (aMask == (1<<sourceAttrYAxis))
			{	rt.gAttrSource = sourceAttrYAxis;}
			else
			if  (aMask == (1<<sourceAttrMembers))
			{	rt.gAttrSource = sourceAttrMembers;}
			else
			if  (aMask == (1<<sourceAttrInitActions))
			{	rt.gAttrSource = sourceAttrInitActions;}
			else
			if  (aMask == (1<<sourceAttrRfshActions))
			{	rt.gAttrSource = sourceAttrRfshActions;}
			else
			if  (aMask == (1<<sourceAttrExitActions))
			{	rt.gAttrSource = sourceAttrExitActions;}
			else
			if  (aMask == (1<<sourceAttrDebugInfo))
			{	rt.gAttrSource = sourceAttrDebugInfo;}
			else
			{	rt.gAttrSource = sourceAttr_Unknown; }
		}
		break;
	case iT_List:				//26
		if ( aMask > (1<<listAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrList = listAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: List attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<listAttrLabel))
			{	rt.gAttrList = listAttrLabel;}
			else
			if (aMask == (1<<listAttrHelp))
			{	rt.gAttrList = listAttrHelp;}
			else
			if  (aMask == (1<<listAttrValidity))
			{	rt.gAttrList = listAttrValidity;}
			else
			if (aMask == (1<<listAttrType))
			{	rt.gAttrList = listAttrType;}
			else
			if (aMask == (1<<listAttrCount))
			{	rt.gAttrList = listAttrCount;}
			else
			if  (aMask == (1<<listAttrCapacity))
			{	rt.gAttrList = listAttrCapacity;}
			else
			if (aMask == (1<<listAttrDebugInfo))
			{	rt.gAttrList = listAttrDebugInfo;}
			else
			if (aMask == (1 << listAttrPrivate))
			{	rt.gAttrList = listAttrPrivate; }
			else
			{	rt.gAttrList = listAttr_Unknown; }
		}
		break;

	case iT_Grid:				// 27
		if ( aMask > (1<<gridAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrGrid = gridAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Grid attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<gridAttrLabel))
			{	rt.gAttrGrid = gridAttrLabel;}
			else
			if (aMask == (1<<gridAttrHelp))
			{	rt.gAttrGrid = gridAttrHelp;}
			else
			if  (aMask == (1<<gridAttrValidity))
			{	rt.gAttrGrid = gridAttrValidity;}
			else
			if (aMask == (1<<gridAttrHeight))
			{	rt.gAttrGrid = gridAttrHeight;}
			else
			if (aMask == (1<<gridAttrWidth))
			{	rt.gAttrGrid = gridAttrWidth;}
			else
			if (aMask == (1<<gridAttrOrient))
			{	rt.gAttrGrid = gridAttrOrient;}
			else
			if  (aMask == (1<<gridAttrHandling))
			{	rt.gAttrGrid = gridAttrHandling;}
			else
			if (aMask == (1<<gridAttrMembers))
			{	rt.gAttrGrid = gridAttrMembers;}
			else
			if  (aMask == (1<<gridAttrDebugInfo))
			{	rt.gAttrGrid = gridAttrDebugInfo;}
			else
			{	rt.gAttrGrid = gridAttr_Unknown; }
		}
		break;

	case iT_Image:				//28
		if ( aMask > (1<<imageAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrImage = imageAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Image attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<imageAttrLabel))
			{	rt.gAttrImage = imageAttrLabel;}
			else
			if (aMask == (1<<imageAttrHelp))
			{	rt.gAttrImage = imageAttrHelp;}
			else
			if  (aMask == (1<<imageAttrValidity))
			{	rt.gAttrImage = imageAttrValidity;}
			else
			if (aMask == (1<<imageAttrLink))
			{	rt.gAttrImage = imageAttrLink;}
			else
			if (aMask == (1<<imageAttrPath))
			{	rt.gAttrImage = imageAttrPath;}
			else
			if (aMask == (1<<imageAttrDebugInfo))
			{	rt.gAttrImage = imageAttrDebugInfo;}
			else
			{	rt.gAttrImage = imageAttr_Unknown; }
		}
		break;

	case iT_Blob:				//29
		if ( aMask > (1<<blobAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrBlob = blobAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Blob attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<blobAttrLabel))
			{	rt.gAttrBlob = blobAttrLabel;}
			else
			if (aMask == (1<<blobAttrHelp))
			{	rt.gAttrBlob = blobAttrHelp;}
			else
			if (aMask == (1<<blobAttrHandling))
			{	rt.gAttrBlob = blobAttrHandling;}
			else
			if (aMask == (1<<blobAttrIdentity))
			{	rt.gAttrBlob = blobAttrIdentity;}
			else
			if (aMask == (1<<blobAttrDebugInfo))
			{	rt.gAttrBlob = blobAttrDebugInfo;}
			else
			{	rt.gAttrBlob = blobAttr_Unknown; }
		}
		break;

	case iT_PlugIn:		// 30
		if ( aMask > (1<<pluginAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrPlugin = pluginAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Plugin attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<pluginAttrLabel))
			{	rt.gAttrPlugin = pluginAttrLabel;}
			else
			if (aMask == (1<<pluginAttrHelp))
			{	rt.gAttrPlugin = pluginAttrHelp;}
			else
			if (aMask == (1<<pluginAttrValidity))
			{	rt.gAttrPlugin = pluginAttrValidity;}
			else
			if (aMask == (1<<pluginAttrVisibility))
			{	rt.gAttrPlugin = pluginAttrVisibility;}
			else
			if (aMask == (1<<pluginAttrUUID))
			{	rt.gAttrPlugin = pluginAttrUUID;}
			else
			if (aMask == (1<<pluginAttrDebugInfo))
			{	rt.gAttrPlugin = pluginAttrDebugInfo;}
			else
			{	rt.gAttrPlugin = pluginAttr_Unknown; }
		}
		break;

	case iT_Template:		//31
		if ( aMask > (1<<templAttr_Unknown) || aMask < 0 )
		{
			rt.gAttrTemplate = templAttr_Unknown;
			LOGIT(CERR_LOG,"ERROR: Template attribute unknown. ( %08x )\n", aMask);
		}
		else
		{
			if (aMask == (1<<templAttrLabel))
			{	rt.gAttrTemplate = templAttrLabel;}
			else
			if (aMask == (1<<templAttrHelp))
			{	rt.gAttrTemplate = templAttrHelp;}
			else
			if (aMask == (1<<templAttrValidity))
			{	rt.gAttrTemplate = templAttrValidity;}
			else
			if (aMask == (1<<templAttrDefaultValues))
			{	rt.gAttrTemplate = templAttrDefaultValues;}
			else
			if (aMask == (1<<templAttrDebugInfo))
			{	rt.gAttrTemplate = templAttrDebugInfo;}
			else
			{	rt.gAttrTemplate = templAttr_Unknown; }
		}
		break;

	case iT_ReservedZeta:
	case iT_ReservedOne:
	case iT_Program:
	case iT_VariableList:
	case iT_Domain:
	case iT_MaxType:
	case iT_FormatObject:
	case iT_DeviceDirectory:
	case iT_BlockDirectory:
	case iT_Parameter:
	case iT_ParameterList:
	case iT_BlockCharacteristic:
	default:
		// erroneous
		return ((genericTypeEnum_t)-1);
		break;
	}// end switch

	return rt;
}
#endif // FMAPARSER

// double
// SXXX XXXX XXXX MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM 
// float
// SXXX XXXX XMMM MMMM MMMM MMMM MMMM MMMM 

/*********************************************************************************************
 * Usable AlmostEqual function
 * by Bruce Dawson
 *
 * parameters:	two floats to compare
 *				maxUlps - number of FLT_EPSILONs between the two floats where they
 *							will be considered equal
 *				
 * returns:		true  on floats almost equal (within maxUlps * FLT_EPSILON of each other)
 *				false when floats are > (maxUlps * FLT_EPSILON apart)
 *
 *********************************************************************************************/
bool AlmostEqual2sComplement(float A, float B, int maxUlps)
{   // Make sure maxUlps is non-negative and small enough that the

    // default NAN won't compare as equal to anything.

    if (! (maxUlps > 0 && maxUlps < 4 * 1024 ) )// make sure we are sane
		throw 4;

    int aInt = *(int*)&A;	// float to int (see bit pattern above)

    // Make aInt lexicographically ordered using a twos-complement int
    if (aInt < 0) // 2's complement abs(aInt) via aInt = -0 - aInt;
        aInt = 0x80000000 - aInt;

    int bInt = *(int*)&B;	// float to int (see bit pattern above)

    // Make bInt lexicographically ordered using a twos-complement int
    if (bInt < 0)
        bInt = 0x80000000 - bInt;

	// aInt and bInt are conceptually the number of FLT_EPSILONs in the values
    int intDiff = abs(aInt - bInt);// absolute value of the difference

    if (intDiff <= maxUlps)// number of FLT_EPSILONs difference is within spec'ed difference
        return true;

    return false;
}

// 0x36a0000000000000 is one float...1.40130e-045...3,936,146,074,321,813,504 double Ulps
// 4.9406564584125e-323 is 10 double epsilons
bool AlmostEqual2sComplement(double A, double B, int maxUlps)
{   // Make sure maxUlps is non-negative and small enough that the
 
    // default NAN won't compare as equal to anything.

    if ( !(maxUlps > 0 && maxUlps < 4 * 1024 ) )// make sure we are sane
		throw 5;

    __int64 aInt = *(__int64*)&A;	// float to int (see bit pattern above)

    // Make aInt lexicographically ordered using a twos-complement int
    if (aInt < 0)
        aInt = 0x8000000000000000ULL - aInt; // L&T Modifications : PortToAM335

    __int64 bInt = *(__int64*)&B;	// float to int (see bit pattern above)

    // Make bInt lexicographically ordered using a twos-complement int
    if (bInt < 0)
        bInt = 0x8000000000000000ULL - bInt; // L&T Modifications : PortToAM335

	// aInt and bInt are conceptually the number of FLT_EPSILONs in the values

#if 1 /*sjv 09may07  was:: _MSC_VER >= 1300  // HOMZ - port to 2003, VS 7 */
	// error C2668: 'abs' : ambiguous call to overloaded function
	// SOLUTION - added double logic
	double  dblDiff = double(aInt - bInt);// WS - 9apr07 - VS2005 checkin
    __int64 intDiff = (__int64)fabs(dblDiff);  // sjv 09may07 abs to fabs.
											   // ...absolute value of the difference
#else
	__int64 intDiff = abs(aInt - bInt);
#endif

    if (intDiff <= maxUlps)// number of FLT_EPSILONs difference is within spec'ed difference
        return true;

    return false;
}



//AlmostEqual2sComplement with NaN == NaN
bool isEqual(float A, float B)
{
	if ( _isnan(A) || _isnan(B) )
	{
		if ( _isnan(A) && _isnan(B) )
		{
			return true;// NOTE: this is different from the math...but useful to us.
		}
		else
		{
			return false;// NaN to a number cannot be equal
		}
	}
	else// neither is a NaN
	{
		return AlmostEqual2sComplement(A,B,3);
	}
}

//AlmostEqual2sComplement with NaN == NaN
bool isEqual(double A, double B)
{
	if ( _isnan(A) || _isnan(B) )
	{
		if ( _isnan(A) && _isnan(B) )
		{
			return true;// NOTE: this is different from the math...mut usefor us.
		}
		else
		{
			return false;// NaN to a number cannot be equal
		}
	}
	else// neither is a NaN
	{
		return AlmostEqual2sComplement(A,B,3);
	}
}

/**********************************************************************************************
 *  roundDbl   round a double to a double
 *
 *  numberOfPlaces	-10 to 10 decimal places remaining when done
					zero rounds to an int
					< 0 tells how many trailing zeros before the decimal point
						if more than the number of digits - 1, nothing happens
					> 0 tells how many digits to the right of the decimal point
 *	26apr11 - another general math function
 *
 *********************************************************************************************/
static double roundHalfup( const double value )
{
	return floor( value + 0.5 );
}
static double roundUp( const double value )
{    
	double result = roundHalfup( fabs( value ) );
    return (value < 0.0) ? -result : result;
}

double roundDbl(double toBrounded, int numberOfPlaces)
{
	if ( numberOfPlaces > 10 || numberOfPlaces < -10 )
	{
		LOGIT(CERR_LOG,"Error: round size out of range.\n");
		return toBrounded;
	}
	double left, right, wrk, ret, wwrk;
	double plier = pow(10.0,numberOfPlaces);
	if (numberOfPlaces < 0 && toBrounded < plier )
	{// too small to be rounded to that...
		return toBrounded;
	}

	right = modf(toBrounded, &left);

	if ( numberOfPlaces == 0 )
	{
		ret = roundHalfup(toBrounded);
	}
	else
	if ( numberOfPlaces > 0 )
	{
#if 1   /* these two techniques give EXACTLY the same answers */
		wrk = right * plier;
		wrk = roundUp(wrk);
		wwrk= wrk/plier;
		ret = wwrk * plier;
		ret = left + wwrk;
#else
		float128 Right, Plier, Work, Work2, Work3, Constant, Constant10;
		float64  lf64x;
		double powSize = pow(10.0,(numberOfPlaces));// was +1

		Constant10 = int32_to_float128(10);
		Constant = int32_to_float128(5);
		Right = float64_to_float128( *( (float64*) &right ) );
		Plier = float64_to_float128( *( (float64*) &powSize ) );

		Work = float128_mul(Right, Plier);
		//Work2= float128_add(Work, Constant);
		Work3 = float128_round_to_int(Work);// was work 2
		Work2= float128_div(Work3, Plier);

		lf64x = float128_to_float64(Work2);
		wrk = *((double*) &lf64x);
		ret = left + wrk;

#endif
	}
	else // numberOfPlaces < 0
	{
		wrk = left * plier;//aka  left / abs(plier)
		wrk = roundUp(wrk);
		ret = (wrk / plier);
	}
	return ret;
}


#ifndef FMAPARSER
/**********************************************************************************************
 *	19apr11 - implement some of dllapi.h code here precuding a near-total rebuild 4 each change
 *********************************************************************************************/
outStatusList_t& outStatusList_t::operator=(const outStatusList_t& src)
{
	outStatusList_td::const_iterator osIT;
	outStatusList_td::clear();
	if (src.size() > 0 )
	{
		for (osIT = src.begin(); osIT != src.end(); ++ osIT)
		{// ptr2outputStatus_t
			push_back(*osIT);
		}
	}// else, just clear and leave
	return (*this);
}

void outStatusList_t::clear(void)
{ 
	outStatusList_td::const_iterator osIT;
	outStatusList_td::iterator beg,dne;
	outputStatus_t* pOSC = NULL;
	int k = size();
	if ( ! k ) return;
	beg = begin();
	dne = end();
	for (osIT = beg; osIT != dne; ++ osIT)
	{
		pOSC = (outputStatus_t*) &(*osIT);
		pOSC->clear();
	}
	//erase(const_cast<struct oStatus_s *>(beg),const_cast<struct oStatus_s *>(dne));
	outStatusList_td::clear();
	erase(begin(),end());
}
#endif // FMAPARSER
/**********************************************************************************************
 *
 *   $History: ddbdefs.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/Common
 * updated header and footer as per HART coding spec.
 * 
 **********************************************************************************************
 */
