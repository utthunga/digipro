/*************************************************************************************************
 *
 * $Workfile: SDCthreadSync.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the api to an OS specific syncronization functions.
 *		These are implemented in ddbEvent.cpp - stored in the device object area but compiled in 
 *		the UI section in order to use the OS specific functionality.  ddbEvent.h is the only 
 *		interface available to the device object.  This file is available to the rest of the 
 *		application in order to overcome the 'User Interface Thread' complications introduced by 
 *		the Windows interface.
 *		
 *
 *		
 * #include "SDCthreadSync.h"
 */

#ifndef _SDCSYNCTHREAD_H
#define _SDCSYNCTHREAD_H

#include "ddbGeneral.h" // includes vector

#define slotTime	100		/* time between msg queue checks in UI waits */

#ifndef EVER
#define EVER (;;)  /* as in for EVER */
#endif

// replace WaitForSingleObject
DWORD systemWaitSingleObject(HANDLE hHandle, DWORD mSecs);

// replace WaitForMultipleObjects
DWORD systemWaitMultipleObjects(DWORD nCount, HANDLE *lpHandles,
								bool bWaitAll,DWORD dwMilliseconds)	;
 // replace Sleep
void  systemSleep(DWORD mSecs);	

// extra help
void systemFlush(void);

#endif // _SDCSYNCTHREAD_H
