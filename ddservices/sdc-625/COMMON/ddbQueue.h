/*************************************************************************************************
 *
 * $Workfile: ddbQueue.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		From my '97 MFC class CQueue (from Q_coll.h)
 *		Interprocess queue handling template class
 *		This class owns ALL memory it uses: return all buffers to the class before destruction.
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003 
 * 09 Jan 2012 - see Pure Virtual Function Call discussion at the end of this file.
 *		
 * #include "ddbQueue.h".
 */

//*/*/* see notes at the end of the file */*/*/

#ifndef _DDBQUEUE_H
#define _DDBQUEUE_H

#include "ddbGeneral.h" // includes vector
#include <deque>
#include "ddbMutex.h"
#include "ddbEvent.h"

#include "registerwaits.h"
#include "logging.h"
#include  <assert.h>

// this may be overridden by defining it before entry here
#ifndef Q_INIT_EMPTY_SIZE
#define Q_INIT_EMPTY_SIZE		8
#endif

/* if there is no consumer, we must stop before the system memory is exhuasted */
#ifndef Q_MAX_QUE_OBJECTS	/* the number of blocks, each containig (Growby) objects */
#define Q_MAX_QUE_OBJECTS		8 // total = (Q_INIT_EMPTY_SIZE*Q_MAX_QUE_OBJECTS) # of objects
#endif						/* VMKP Modified on 270104 ( changed Q_MAX_QUE_OBJECTS
								size from 4 to 8 */

/* default timeout --- very long */
#if defined(_MSC_VER) && _MSC_VER < 1300  // HOMZ - port to 2003, VS 7
#define Q_DFLT_WAIT_TIME		hCEvent::FOREVER
#else
                                              /// HOMZ prevent error C2653: 'hCEvent' : is not a class or namespace name
#define Q_DFLT_WAIT_TIME		INFINITE      /// ddb_EventMutex.cpp(91):const unsigned long hCevent::FOREVER = INFINITE;// 0xffffffff;	
											// stevev - merge-19feb07 - this is a problem since ddbEvent.h was included above!
#endif

// remove def to turn off debug logging
// #define QDBG 1


#ifdef QDBG
extern int Qhandle;
#endif
// interprocess queue handling
// default construction is for a FIFO queue of LOADTYPE objects.
//
template<class LOADTYPE>
class hCqueue
{
public: // data types
	// for debugging but always here
	struct identity
	{
		LOADTYPE *pLd;
		int       id;
	};
protected:
	deque< LOADTYPE *>     loadDeque;
	hCevent                haveObjectEvt;
	hCevent::hCeventCntrl* pEvtCtrl;
	hCmutex                listProtMutex;// since this class straddles tasks, protect the memory
	hCmutex::hCmutexCntrl* pMtxCtrl;
	hCevent                haveMTObjectEvt;
	hCevent::hCeventCntrl* pMTEvtCtrl;

	int GrowSize;	// how to grow the Empty Queue
	vector< LOADTYPE * > EmptyList;
	vector< LOADTYPE * > OwnedBlks;
#ifdef _DEBUG
	vector< struct identity > MtMIAList;
	vector< struct identity > RdMIAList;
#endif
#ifdef QDBG
	int     thisQhndl;
#endif
	virtual int  AddEmpties() ;
	// the following have to be pure virtual because only the payload knows how to clear itself
	virtual void EmptyItem(LOADTYPE * pItem) = 0;
	virtual void InitItem(LOADTYPE * pItem)  = 0;

public:
	// Construction
	hCqueue(void);
	void initialize(int Growby= Q_INIT_EMPTY_SIZE);

	/*** Data Consumer services ***/
	virtual
	int        GetQueueState(void);		// currently gives number in the queue
	virtual
    LOADTYPE * GetNextObject(int id = 0);
	virtual
    int        ReturnMTObject( LOADTYPE * pMTobject );
	virtual
	int        FlushQueue();

	virtual		// waits for and fetches object
	LOADTYPE * Pend4Object( DWORD timeout = Q_DFLT_WAIT_TIME, int id = 0);
	virtual
	int        Wait4Arrival(DWORD timeout = Q_DFLT_WAIT_TIME);// waits for arrival, no fetch
	virtual
	OS_HANDLE  GetArrivalEvent(void); // allows a consumer to wait for more than just an arrival

	/*** Data Generator Services ****/
	virtual
	LOADTYPE * GetEmptyObject(bool pendOnEmpty = false, int id = 0);
	virtual
	int        PutObject2Que (LOADTYPE * pNewObject, bool toTail = true);
	virtual
		int    PutObject2Head(LOADTYPE * pNewObject){ return PutObject2Que (pNewObject, false);};

	// Destructors
	virtual ~hCqueue();
	void destroy(void) { 
#ifdef USED	// PAW revise logic to stop assertion error
		for(vector< LOADTYPE * >::iterator iT = OwnedBlks.begin(); iT < OwnedBlks.end();iT++)
		{//error	if ( (*iT) != NULL ){ delete ((LOADTYPE *)(*iT)); *iT = NULL;}
	//VMKP 260304
			LOADTYPE* plt = NULL;
			for ( vector<LOADTYPE*>::iterator pos = OwnedBlks.begin(); pos < OwnedBlks.end(); pos++)
			{// pos is a ptr2ptr2LOADTYPE
				plt = (*pos);
				if ( plt != NULL )
				{
		//clog<<"Q delMT:"<<hex<<plt<<dec<<endl;
				//Generate a memory leak in order not to get an assertion//	
					try {
// _CONSOLE					delete plt;
					}
					catch(...) {}
					*pos = NULL;
				}
			}
			loadDeque.clear();
			OwnedBlks.clear();	
			EmptyList.clear();
		#ifdef _DEBUG
			MtMIAList.clear();
			RdMIAList.clear();
		#endif
			RAZE(pEvtCtrl);
			RAZE(pMtxCtrl);
			RAZE(pMTEvtCtrl);
			
	//VMKP 260304
		}
#else	// PAW
		typename vector< LOADTYPE * >::iterator iT;
		iT = OwnedBlks.begin(); 
		do
		{
    //VMKP 260304
            // CPMHACK: Clear the allocated memory inside vector
            typename vector<LOADTYPE*>::iterator pos;
            for (pos = OwnedBlks.begin(); pos < OwnedBlks.end(); pos++)
            {
                if (*pos != NULL)
                {
                    RAZE_ARRAY(*pos);
                }
            }
			loadDeque.clear();
			OwnedBlks.clear();	
			EmptyList.clear();
		#ifdef _DEBUG
			MtMIAList.clear();
			RdMIAList.clear();
		#endif
			RAZE(pEvtCtrl);
			RAZE(pMtxCtrl);
			RAZE(pMTEvtCtrl);
			
			if (OwnedBlks.size() != 0)
				iT++;
			else
				continue;	//OwnedBlks is empty so terminate loop 
		}while (OwnedBlks.size() != 0);
#endif	// PAW
	};// end of destroy()
};// end of queue class

/*-*-*-*-* Construction *-*-*-*-*/

template<class LOADTYPE>
hCqueue<LOADTYPE>::hCqueue(void)
{
	GrowSize  = 0;
	pEvtCtrl  = NULL;
	pMtxCtrl  = NULL;
	pMTEvtCtrl= NULL;
};

template<class LOADTYPE>
void
hCqueue<LOADTYPE>::initialize( int Growby )
{
	GrowSize  = Growby;
#ifdef QDBG
	if (Qhandle < 0 || Qhandle > 100 )
	{
		clog << "Q: ???? resetting Qhandle from " << Qhandle <<endl;
		Qhandle = 0;
	}
	thisQhndl = ++Qhandle;
	clog << "Q:" << thisQhndl << " initialized Growby to "<< Growby<<endl;
#endif
	AddEmpties();
	pEvtCtrl  = haveObjectEvt.getEventControl();

	pMtxCtrl  = listProtMutex.getMutexControl("Queue");// string unused in release mode

	pMTEvtCtrl= haveMTObjectEvt.getEventControl();
};

/*** Destruction ***/
/*  The class remembers all memory blocks allocated 
 *  It deletes all of this memory during destruction, 
 *     no matter who happens to be using it at the time.
 *  You should return all buffers to the empty lists before destruction
 */
template<class LOADTYPE>
hCqueue<LOADTYPE>::~hCqueue()                            
{// assume we are only deleted in one place - no mutex required
#ifdef QDBG
	clog << "Q:" << thisQhndl << " destroying " << OwnedBlks.size() << " blocks."<<endl;
#endif
    // CPMHACK: Clear the allocated memory inside vector
    typename vector<LOADTYPE*>::iterator pos;
    for (pos = OwnedBlks.begin(); pos < OwnedBlks.end(); pos++)
    {
        if (*pos != NULL)
        {
            RAZE_ARRAY(*pos);
        }
    }
	loadDeque.clear();
	OwnedBlks.clear();	
	EmptyList.clear();
#ifdef _DEBUG
	MtMIAList.clear();
	RdMIAList.clear();
#endif
	RAZE(pEvtCtrl);
	RAZE(pMtxCtrl);
	RAZE(pMTEvtCtrl);
};

/*-*-*-*-* Data Consumer services *-*-*-*-*/

/***
 *	GetQueueState
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::GetQueueState() 
{//	We may add a state structure parameter someday
 //		to get empty count and/or checked out counts
	if (pMtxCtrl==NULL)
	{
		LOGIT(CLOG_LOG|CERR_LOG,"No Mutex ptr in que state.\n");
		return 0;
	}
	pMtxCtrl->aquireMutex();
	int r = (int)loadDeque.size();// WS - 9apr07 - VS2005 checkin
	pMtxCtrl->relinquishMutex();
	return r;
};

/***
 *	GetNextObject
 *		returns the next ready element or NULL if none are ready
 ***/
template<class LOADTYPE>
LOADTYPE * hCqueue<LOADTYPE>::GetNextObject(int id) 
{ 
	LOADTYPE * t = NULL;
	if (pMtxCtrl==NULL)
	{
		LOGIT(CLOG_LOG|CERR_LOG,"No Mutex ptr in que get.\n");
		return NULL;
	}
	pMtxCtrl->aquireMutex();
	if ( loadDeque.size() > 0 )
	{
		t = loadDeque.front();
		loadDeque.pop_front();
#ifdef _DEBUG
#ifdef QDBG
		clog << "Q:" << thisQhndl << " dequed, added to RdMIA: "<< hex<<t<<dec<<endl;
#endif
		struct identity rdId = { t, id };
		RdMIAList.push_back( rdId );
#endif
	}
	// else	return NULL
	pMtxCtrl->relinquishMutex();

	return t;
};

/***
 *	ReturnMTObject
 *		Data consumer returns the empty buffer after the data has been used
 *		Method returns the number of empty buffers or false on error
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::ReturnMTObject ( LOADTYPE * pMTobject ) 
{ 
	int r = 0;
	if (pMtxCtrl==NULL || pMTobject == NULL || pMTEvtCtrl == NULL)
	{
/* VMKP Added on 270104 
		(To generate event in case the packet return by GetEmptyObject is NULL) */
#ifdef NO_GOVERNOR
		pMTEvtCtrl->triggerEvent();
#endif
/* VMKP Added on 270104 */
		return 0;
	}
	pMtxCtrl->aquireMutex();
#ifdef _DEBUG
	bool found = false;
	struct identity t;
	vector<struct identity>::iterator pos;// WS - 9apr07 - VS2005 checkin
	for ( pos = RdMIAList.begin(); pos < RdMIAList.end(); pos++)
	{// pos is a ptr2ptr2identity
		t = *pos;
		if ( t.pLd == pMTobject )
		{
			found = true;
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " ReturnMTObject RdMIA List found: "<<
			hex<<pMTobject<<dec<< " in " << RdMIAList.size() <<" RdMIA's"<<endl;
  #endif
			break; // out of for loop with correct location
		}
	}
	if ( found != true ) // not found
	{		
		for ( pos = MtMIAList.begin(); pos < MtMIAList.end(); pos++)
		{// pos is a ptr2identity
			t = *pos;
			if ( t.pLd == pMTobject )
			{
				found = true;
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " ReturnMTObject MtMIA List found: "<<
			hex<<pMTobject<<dec<<" in " << MtMIAList.size() <<" MtMIA's"<<endl;
  #endif
				break; // out of for loop with correct location
			}
		}
		if ( found != true ) // not found
		{
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " ReturnMTObject did NOT find: "<<
			hex<<pMTobject<<dec<<" On the RdMIA or the MtMIA lists."<<endl;
  #endif
			LOGIT(CERR_LOG, 
			 "ReturnMTObject's object to be queued has not been gotten as Empty OR Ready!\n");
			r = 0;//return false;	// FAILURE
		}
		else
		{
			MtMIAList.erase(pos); // gets rid of the pointer, not the object
			r = 0; //return false;	// FAILURE
		}
	}
	else
	{
		RdMIAList.erase(pos); // gets rid of the pointer, not the object
	}
	// added stevev 10jan11 - see if we have a duplicate
	vector< LOADTYPE * >::iterator ploc;
	for ( ploc = EmptyList.begin(); ploc != EmptyList.end(); ++ploc )
	{// pos is a ptr2ptr2LOADTYPE
		if ( (*ploc) == pMTobject )
		{
			found = true;
			LOGIT(CLOG_LOG|CERR_LOG,"Returning a returned packet.\n");
			assert(0);
		}
	}
#endif /* _DEBUG */
	EmptyItem( pMTobject );
	EmptyList.push_back( pMTobject ); 
	pMTEvtCtrl->triggerEvent();
	/*   set event here if we implement empty pend */
	r = (int)EmptyList.size();// WS - 9apr07 - VS2005 checkin
#ifdef QDBG
		clog << "Q:" << thisQhndl << " ReturnMTObject added to empties: "<<
			hex<<pMTobject<<dec<< " makes "<< r << " empties."<<endl;
#endif

	pMtxCtrl->relinquishMutex();
	return r;
};

/***
 *	FlushQueue
 *		Empties the Ready Queue, fills the empty queue
 *		Method returns the number of Full buffers when done (should be ZERO)
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::FlushQueue()                         
{
	LOADTYPE * pT;
	int s = 0;
	if (pMtxCtrl==NULL)
	{
		return 0;
	}
	pMtxCtrl->aquireMutex();
	while ( loadDeque.size() > 0 )
	{	
		pT = loadDeque.front();
#ifdef QDBG
		clog << "Q:" << thisQhndl << " FlushQueue flushing: " <<hex<<pT<<dec<<endl;
#endif	
		loadDeque.pop_front();

		EmptyItem( pT );	
		EmptyList.push_back(pT);
	}
#ifdef _DEBUG
	s = (int)(MtMIAList.size() + RdMIAList.size() + loadDeque.size());// WS - 9apr07 - VS2005 checkin
	if (s>0) 
	{
		LOGIT(CERR_LOG|CLOG_LOG,"FlushQueue shows memory outstanding in a MIA list.!"
			"(%d MT,%dRd)\n",MtMIAList.size(),RdMIAList.size());
		vector< struct identity >::iterator idIT;
		struct identity *pId = NULL;
		for ( idIT = MtMIAList.begin(); idIT != MtMIAList.end(); ++idIT )
		{
			pId = &(*idIT);
			LOGIT(CLOG_LOG,"    Missing in Action Empty for id %d\n",pId->id);
		}
		for ( idIT = RdMIAList.begin(); idIT != RdMIAList.end(); ++idIT )
		{
			pId = &(*idIT);
			LOGIT(CLOG_LOG,"    Missing in Action Read for id %d\n",pId->id);
		}
	}
#else
	s = loadDeque.size();
#endif
		
	/* stevev 01-28-04 	- prevent deadlock if this done outside of descruction */	
	if ( EmptyList.size() > 0 )
	{		
		pMTEvtCtrl->triggerEvent();// trigger the next task, or leave ready
	}
	/* end  stevev 01-28-04 */

	pMtxCtrl->relinquishMutex();
	return s;
};

/***
 *	Pend4Object
 *		Waits for the haveObject event, gets the first object on the queue and
 *		returns a pointer to it.  Returns NULL on timeout or error.
 *
 *	NOTE: you should service ALL of the Queue once before pending again
 *
 ***/
template<class LOADTYPE>
LOADTYPE *  hCqueue<LOADTYPE>::Pend4Object(DWORD timeout, int id)                         
{
	RETURNCODE rc = SUCCESS;
	LOADTYPE * pR = NULL;

	if (pEvtCtrl == NULL || pMtxCtrl==NULL)
	{
		LOGIT(CERR_LOG,"ERROR: Pend4Object has no control of its syncronization objects.\n");
		return NULL; // error exit
	}
	pMtxCtrl->aquireMutex();
	if (loadDeque.size() > 0) // one snuck in
	{
		pR = (GetNextObject(id));
	}
	else // no objects currently available
	{
			
		pMtxCtrl->relinquishMutex();        // do not pend while holding the mutex
		rc = pEvtCtrl->pendOnEvent(timeout);		
		pMtxCtrl->aquireMutex();

		if ( rc != SUCCESS) // APP_WAIT_TIMEOUT || APP_WAIT_ERROR
		{ 
			if (loadDeque.size() == 0) // and nothing there
			{
				pR = NULL; // error / timeout exit
			}
			else
			{
				pR = (GetNextObject(id));
			}
		}
		else  //success - continue
		{	
			if (loadDeque.size() == 0) // and nothing there - an error
			{
				pR = NULL; // error  exit
			}
			else
			{
				pR = (GetNextObject(id));
			}
		}
	}
#ifdef QDBG
		clog << "Q:" << thisQhndl<<" Pend4Object returning: "<<hex<<pR<<dec<<" from queue."<<endl;
#endif	
	pMtxCtrl->relinquishMutex();
	return pR;
};

/***
 *	Wait4Arrival
 *		Waits for the haveObject event and returns when it arrives. (does not get object)
 *		Returns the number of objects in the queue...will be zero on timeout or error.
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::Wait4Arrival(DWORD timeout)                         
{
	RETURNCODE rc = SUCCESS;
	int r = 0;
	
	if (pEvtCtrl == NULL || pMtxCtrl==NULL)
	{
		return 0; // error exit
	}
	pMtxCtrl->aquireMutex();
	if (loadDeque.size() > 0) // one snuck in
	{
		r = (int)(loadDeque.size());
	}
	else // no objects currently available
	{
		pMtxCtrl->relinquishMutex();		// do not pend while holding the mutex
		rc = pEvtCtrl->pendOnEvent(timeout);
		pMtxCtrl->aquireMutex();

		if ( rc != SUCCESS  &&  // APP_WAIT_TIMEOUT || APP_WAIT_ERROR
			loadDeque.size() == 0) // and nothing there
		{ 
			r =  0; // error || timeout exit
		}
		else  // success - continue
		{
			r = loadDeque.size();// WS - 9apr07 - VS2005 checkin
		}
	}
	pMtxCtrl->relinquishMutex();
	return (r); // could return zero on error
};

/***
 *	GetArrivalEvent
 *		Returns the event handle to allow a consumer to execute a wait-for-multiple-events.
 *		
 ***/
template<class LOADTYPE>
OS_HANDLE hCqueue<LOADTYPE>::GetArrivalEvent(void)                         
{
	if ( pEvtCtrl != NULL )
	{
		return (pEvtCtrl->getEventHandle());
	}
	else
	{
		return OS_INVALID_HANDLE;
	}
}


     
/*-*-*-*-* Data Generator Services *-*-*-*-*/

/***
 *	GetEmptyObject
 *		Returns an empty object pointer
 *			Since we are allocating more on the fly, this will return
 *			NULL when memory is exhausted.( see Q_MAX_QUE_OBJECTS above)
 ***/
template<class LOADTYPE>
LOADTYPE *hCqueue<LOADTYPE>::GetEmptyObject(bool pendOnEmpty, int id)             
{
	LOADTYPE * pT = NULL;
	if (pMtxCtrl==NULL)
	{
		return NULL;
	}
	pMtxCtrl->aquireMutex();
	if (EmptyList.size() == 0)
	{
		if ( AddEmpties() == 0 )
		{
#ifdef QDBG
		clog << "Q:" << thisQhndl << " NO EMPTIES AVAILABLE";
#endif
			if (pendOnEmpty && pMTEvtCtrl != NULL)
			{
#ifdef QDBG
		clog << " - PENDING"<< endl;
#endif
				pMtxCtrl->relinquishMutex();
				pMTEvtCtrl->pendOnEvent();
				pMtxCtrl->aquireMutex();
			}
			
#ifdef QDBG
			else
			{
		clog << " - NO PEND REQUESTED"<< endl;
			}
#endif
		}
	}
	if ( EmptyList.size() > 0 )
	{
		pT = EmptyList.back(); // it was cleared going into the empty list (list of empty unused)
		EmptyList. pop_back();
#ifdef _DEBUG
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " got MT, added to MtMIA: "<< hex<<pT<<dec<<endl;
  #endif
		struct identity myId = { pT, id };
		MtMIAList.push_back( myId );
#endif
	}
	else
	{
//		cerr << "hCqueue - No Empties Available!"<< endl; // leave return null
#ifdef QDBG
		clog << "Q:" << thisQhndl << " NO EMPTIES AVAILABLE - Returning NULL."<< endl;
#endif
	}

	/* stevev 01-28-04 	- prevent deadlock if more than one packet returned empty at a time */	
	if ( EmptyList.size() > 0 )
	{		
		pMTEvtCtrl->triggerEvent();// trigger the next task, or leave ready
	}
	/* end  stevev 01-28-04 */
	pMtxCtrl->relinquishMutex();
	return pT; // NULL on error
};

/***
 *	PutObject2Que
 *		Places the passed-in object on the Ready Queue & signals event
 *		Method returns the number of Ready buffers.
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::PutObject2Que(LOADTYPE * pNewObject, bool toTail ) 
{
	int r = 0;
	if (pMtxCtrl==NULL)
	{
		return 0;
	}
	pMtxCtrl->aquireMutex();
#ifdef _DEBUG
	bool found   = false;
	struct identity t;
	vector< identity >::iterator pos;// WS - 9apr07 - VS2005 checkin
	for ( pos = MtMIAList.begin(); pos < MtMIAList.end(); pos++)
	{// pos is a ptr2ptr2LOADTYPE
		t = *pos;
		if ( t.pLd == pNewObject )
		{
			found = true;
			break; // out of for loop with correct location
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " PutObject2Que found adder in MtMIA: "<<
			hex<<pNewObject<<dec<<endl;
  #endif
		}
	}
	if ( found != true ) // not found
	{
		LOGIT(CERR_LOG,"PutObject2Que's object to be queued has not been gotten as Empty!\n");
		r = 0;//return false;	// FAILURE
  #ifdef QDBG
		clog << "Q:" << thisQhndl << " PutObject2Que DID NOT FIND adder in MtMIA: "<<
			hex<<pNewObject<<dec<<" -- OBJECT NOT! QUEUED!!--"<< endl;
  #endif
	}
	else
	{
		MtMIAList.erase(pos); // gets rid of the pointer, not the object
#endif
	if (toTail)
	{
		loadDeque.push_back( pNewObject ); 
	}
	else
	{
		loadDeque.push_front( pNewObject ); 
	}
	pEvtCtrl->triggerEvent();
	r = (int)loadDeque.size();// WS - 9apr07 - VS2005 checkin
#ifdef QDBG
		clog << "Q:" << thisQhndl << " PutObject2Que queued: "<<
			hex<<pNewObject<<dec<<" now has "<< r << " entries."<<endl;
#endif

#ifdef _DEBUG
	}
#endif
	pMtxCtrl->relinquishMutex();
	return r;
};



/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *  virtual function defaults
 *     not mutex protected - caller must handle 
 *__________________________________________*/

/***
 *	AddEmpties
 *		Allocates and initializes memory for buffers into the Empty list
 *		Method returns the number of Empty buffers.
 ***/
template<class LOADTYPE>
int hCqueue<LOADTYPE>::AddEmpties() 
{   
	int i = 0;
	LOADTYPE*  pT = NULL;
	LOADTYPE* top = NULL;
	if (OwnedBlks.size() < Q_MAX_QUE_OBJECTS)
	{	// alloc on the heap in a single chunk
		top = new LOADTYPE[GrowSize];
		if ( top != NULL )
		{	
			OwnedBlks.push_back(top);	// register memory block for later destruction
//clog<<"Q addMT:"<<hex<<top<<dec<<endl;
			// insert each one into the list
			for (i = 0, pT = top; i < GrowSize; i++, pT++ )
			{// first we do bit-wise zero initialization
				//EmptyItem(pT);				
				InitItem(pT);// like EmptyItem but slightly different
				EmptyList.push_back(pT); // make it available
#ifdef QDBG
		clog << "Q:" << thisQhndl << " AddEmpties added to empties: "<<hex<<pT<<dec
			<<" now own " << OwnedBlks.size()<<" with a maximum of "<< Q_MAX_QUE_OBJECTS<<endl;
#endif
			}			
		}
		// else  memory exhaustion - return size
	}
	// else return the size

	return ((int)EmptyList.size());// WS - 9apr07 - VS2005 checkin
};

/***
 *	EmptyItem
 *		Executed right befor the Empty Buffer is placed into the Empty List 
 ***/
//template<class LOADTYPE>
//void hCqueue<LOADTYPE>::EmptyItem(LOADTYPE * pItem)
//{
	// default is to set all to zero, overload if you need something else
	// note: memset to a class fails in VS2010
	// memset(pItem, 0, sizeof(LOADTYPE));
	// this has to be pure virtual because only loadtype knows how to clear itself
//};

/***
 *	InitItem same as above
 *		
 ***/
//template<class LOADTYPE>
//void hCqueue<LOADTYPE>::InitItem(LOADTYPE * pItem)
//{
	// default is to set all to zero, overload if you need something else
//	memset(pItem, 0, sizeof(LOADTYPE));
//};


#endif // _DDBQUEUE_H
/**********************************************************************************************
 * NOTES:
 *       hCqueue class
 *_____________________________________________________________________________________________
 *  This class is designed to have a single consumer. If the consumer does not empty the ready
 *  queue when it is signaled to do so, it will not get another event to tell it that there is
 *  stuff still there.  The pend/wait methods try to prevent this when they are entered but may 
 *  may not be successful if there are several there.
 *_____________________________________________________________________________________________
 *
 *		default construction is for a FIFO queue of LOADTYPE objects.
 *		Growby is also initial queue depth.
 *
 *	During DEBUG the class tracks buffers gotten as Empty and as Ready.
 *  Currently nothing is done with this information.
 *_____________________________________________________________________________________________
 * Pure Virtual Function Calls - 09Jan2011
 *	There is an issue with the initial clearing of a LOADTYPE class in AddEmpties().  The first
 *	implementation used a memset on the pointer.  This is a bad idea for clearing classes and
 *  is known to crash VS2010 apps in a vector<> environment.  This and the desire to flag an 
 *	initialized class differently from a used but emptied class gave rise to a pure virtual
 *	function for EmptyItem() and InitItem() since the decendent class would know how to clear
 *  parametertized class.
 *	Pure virtual functions cannot be called from a base class constructor or destructor when 
 *	compiling with Visual Studio (GCC handles it correctly according to the web).
 *	My current answer to this dilemma is to use a two stage construction scenario much like
 *	MFC's.  There will be a instantiation then an initialization done by the owner of the que.
 *
 **********************************************************************************************
*/

/**********************************************************************************************
 * 05dec12-stevev - added ids to the fetch classes.  while these are only used in debug mode
 *			they are maintained all the time.  They let us know who loses a LOADTYPE.
 *   $History: ddbEvent.h $
 * 
 **********************************************************************************************
 */
