/*************************************************************************************************
 *
 * $Workfile: ddbGeneral.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		holds useful info for all users of the db - beware of change, this is in almost all .cpp's
 *		5/6/2	sjv	started creation
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *		
 * #include "ddbGeneral.h"
 */

#ifndef _DDBGENERAL_H
#define _DDBGENERAL_H
#pragma once

#pragma warning (disable : 4786) 

#include "xplatform_base.h"

using namespace std;
#pragma warning (disable : 4786)


#define DBDD__PATH   L"\\hcf\\ddl\\library"	/* default path */
#define DBDD_PATH     "\\hcf\\ddl\\library"	/* default path */


#define RAZE(p)		  if((p)!=NULL){ delete (p); (p) = NULL;}
#define RAZE_ARRAY(p) if((p)!=NULL){ delete[] (p); (p) = NULL;}
#define DESTROY(q)	  if((q)!=NULL){ (q) ->destroy(); delete (q); (q) = NULL;}
#define DE_ALLOC(p)	  if((p)!=NULL){ free( (p) ); (p) = NULL;}


#define FOR_iT(lt,lst)  for(lt::iterator iT = (lst).begin();iT!=(lst).end();++iT)
#define FOR_p_iT(lt,lst)  for(lt::iterator iT = (lst)->begin();iT!=(lst)->end();++iT)
#define FOR_this_iT(lt,lst)  for(lt::iterator iT = begin();iT!=end();++iT)

typedef float          FLOAT;

// definition
typedef DWORD  itemID_t;
typedef vector<DWORD> itemIDlist_t;
typedef itemIDlist_t::iterator  itemIDlistIT_t;

#define symbolNumb_t itemID_t
typedef int DevInfcHandle_t;

#define InvalidItemID     0

typedef UINT64   DD_Key_t;

/* for construction of psuedo-items:  */
typedef struct itemIdentity_s
{
	symbolNumb_t   newID;
	string         newName;
}
/*typedef*/ itemIdentity_t;
/* end new identy for psuedo-items 21jun05 */
typedef struct elementID_s
{
	int       which; // the identifier for which element: member-name, index number..(-1 is MT)
	itemID_t  contID;// the symbol number of the container (array, collection, list..)
}
/*typedef*/ elementID_t;
/* typedef for identifying a psuedo element */


typedef enum cmdOriginator_e
{
	  co_INITED    = 0x00,
      co_READ_IMD  = 0x01,	    // From ReadImd (was co_DEV_LOADING)
      co_METHOD    = 0x02,		// From sendMethCmd
      co_WRITEIMD  = 0x04,		// From WriteImd (was co_WRITE)
	  co_ASYNC_READ= 0x08,	// From Async Read (ServiceReads)
	  co_ASYNC_WRITE=0x10,	// From Async Write (ServiceWrites)
	  co_TRANSPORT = 0x20,   // command 112 cycles
	  co_DEVICE    = 0x40,	// called from sendCommand in the ddbDevice
      co_UNDEFINED = 0x80      // I haven't a clue where I came from
} 
/*typedef*/ cmdOriginator_t;


#define INT_CONST_SIZE   1		/* as per DD spec */
#define FLT_CONST_SIZE	 4		/* not a double */

typedef vector<wstring> WstrVector_t;
typedef vector<string>	StrVector_t;
typedef vector<int>     IntList_t;
typedef vector<UINT32>  UIntList_t;

#ifndef RETURNCODE
/* error codes */
   #define RETURNCODE  int

   #define SUCCESS			(0)
   #define FAILURE			(1)
   #define VIRTUALCALLERR   (2)	

#endif
  
#define DB_ERROR_BASE     5000
#define  DB_ATTRIBUTE_NOT_FOUND		(DB_ERROR_BASE +  1)
#define  DB_ELEMENT_NOT_FOUND		(DB_ERROR_BASE +  2)
#define  DB_NO_LIST_ELEMENT			(DB_ERROR_BASE +  3) /* aborts DD */

#define APP_ERROR_BASE    7000

#define  APP_DEVICE_NOT_FOUND		(APP_ERROR_BASE +  2)
#define  APP_COMMAND_ERROR			(APP_ERROR_BASE +  3)
#define  APP_CMD_COMM_ERR			(APP_ERROR_BASE +  4)
#define  APP_CMD_RESPCODE_ERR		(APP_ERROR_BASE +  5)  
#define  APP_NEEDS_REINIT           (APP_ERROR_BASE +  6)  
#define  APP_NO_ATTR_TYPE           (APP_ERROR_BASE +  7)  
#define  APP_MEMORY_ERROR           (APP_ERROR_BASE +  8) 
#define  APP_DB_UNAVAILABLE         (APP_ERROR_BASE +  9) 
#define  APP_DD_NOT_FOUND           (APP_ERROR_BASE + 10) 
#define  APP_PROGRAMMER_ERROR       (APP_ERROR_BASE + 11)
#define  APP_DB_KEY_FAILURE			(APP_ERROR_BASE + 12)
#define  APP_EXPRESSION_ERR			(APP_ERROR_BASE + 13)
#define  APP_EXPR_TYPE_ERR			(APP_ERROR_BASE + 14)
#define  APP_EXPR_DIV_BY_ZERO		(APP_ERROR_BASE + 15)
#define  APP_TYPE_UNKNOWN			(APP_ERROR_BASE + 16)
#define  APP_CONSTRUCT_ERR          (APP_ERROR_BASE + 17)
#define  APP_DD_ERROR               (APP_ERROR_BASE + 18)
#define  APP_PARAMETER_ERR			(APP_ERROR_BASE + 19)
#define  APP_AGAINST_POLICY			(APP_ERROR_BASE + 20)
#define  APP_WAIT_TIMEOUT			(APP_ERROR_BASE + 21)
#define  APP_WAIT_ABANDONED			(APP_ERROR_BASE + 22)
#define  APP_WAIT_ERROR				(APP_ERROR_BASE + 23)
#define  APP_RESOLUTION_ERROR		(APP_ERROR_BASE + 24)
#define  APP_CMD_VALIDITY_RULE_ERR  (APP_ERROR_BASE + 25)
#define  APP_OUT_OF_RANGE_ERR       (APP_ERROR_BASE + 26)
#define  APP_USER_CANCELED			(APP_ERROR_BASE + 27)
#define  APP_USER_OKED				(APP_ERROR_BASE + 28)
#define  APP_EXPR_INT_AS_INDEX      (APP_ERROR_BASE + 29)
//Vibhor 111004: Added
/*NOTE: The following rc would be returned if an attribute, which wasn't filled by JIT Parser for
a. Attribute not defined in DD &&
b. No specification for default in the specs
*/
#define	 APP_ATTR_NOT_SUPPLIED		(APP_ERROR_BASE + 29) 
#define	 APP_EXPR_BIN_WO_VALID		(APP_ERROR_BASE + 30) 
/* stevev 10Jun05 - for all the new typedef functionality */
#define  APP_TYPE_MISMATCH			(APP_ERROR_BASE + 31)
#define  APP_STRING_NOT_ASCII		(APP_ERROR_BASE + 32)
#define  APP_PARSE_FAILURE			(APP_ERROR_BASE + 33)

#define  APP_LIST_IS_EMPTY			(APP_ERROR_BASE + 34) /* stevev 27jan09 - not always an error */
#define  APP_DICTIONARY_MISSING		(APP_ERROR_BASE + 35) /* stevev 28dec10 */

#define  APP_INDEX_SPECIAL_VALUE    (APP_ERROR_BASE + 36)
#define  APP_INDEX_MISMATCH			(APP_ERROR_BASE + 37)

#define API_ERROR_BASE	6000

#define  API_FAIL_READ_ONLY			(API_ERROR_BASE +  5)  
#define  API_FUNCTION_NOT_SUPPORTED	(API_ERROR_BASE +  6) 

#define MEE_ERROR_BASE  8000		/* see MEE.h for defs */

#define DATA_QUALITY_NOT_GOOD	10000

#define DATA_QUALITY_NOT_INITIALIZED (DATA_QUALITY_NOT_GOOD + 1)
#define DATA_QUALITY_NOT_VALID       (DATA_QUALITY_NOT_GOOD + 2)
#define DATA_QUALITY_NOT_WRITABLE    (DATA_QUALITY_NOT_GOOD + 3)

#define COND_RETURN_VALUE_BASE	11000

#define COND_RESOLVED_TO_INVALID     (COND_RETURN_VALUE_BASE + 1)
#define COND_IF_WITH_NULL            (COND_RETURN_VALUE_BASE + 2)
#define COND_SLCT_WITH_BAD_DEST      (COND_RETURN_VALUE_BASE + 3)
#define COND_BAD_PRI_AXIOM_TYPE      (COND_RETURN_VALUE_BASE + 4)
#define COND_DIRECT_W_NULL_PAYLOAD   (COND_RETURN_VALUE_BASE + 5)
#define COND_ISCOND_ERROR            (COND_RETURN_VALUE_BASE + 6)

#define COND_VAR_USE_INVALID		 (COND_RETURN_VALUE_BASE + 7) /* tokenizer use*/

#define FILE_RETURN_VALUE_BASE	12000
/* elements defined in ddbFileSupportInfc.h */

#define LAYOUT_RETURN_VALUE_BASE  13000

// from OPC server::
/* used to move around instrument identity information */
/* currently unsupported */
typedef struct Identity_s
{
//   BYTE cManufacturer; //this
//   BYTE cDeviceType;   //and this
   WORD wManufacturer; //this		upgraded for hart 7 07feb07
   WORD wDeviceType;   //and this	upgraded for hart 7 07feb07
   BYTE cUniversalRev;
   BYTE cDeviceRev;
   BYTE cSoftwareRev;
   BYTE cHardwareRev;// and Signalling Code!
   BYTE cZeroFlags;
/* VMKP added on 311203*/
   BYTE PollAddr;
/* VMKP added on 311203*/
   BYTE cReqPreambles;
   DWORD dwDeviceId;   //and this; gives a unique instance ID
//Added for HART 7 capability
   BYTE cRespPreambles;
   BYTE cMaxDevVars;
   WORD wCfgChngCnt;
   BYTE cExtDevStatus;
// end 07feb07 additions
   BYTE cInternalFlags; // used during identify to differentiate phases stevev 29jun07
   BYTE cUnused;
   std::string sTag; //Gives a device tag (short tag)
/* VMKP added on 030404 */
   Identity_s(){ clear(); };
/* VMKP added on 030404 */
   void clear(void){ wManufacturer = wDeviceType = wCfgChngCnt = 0; cUniversalRev = PollAddr = 0;
             cDeviceRev = cSoftwareRev  = cHardwareRev = cZeroFlags = cReqPreambles = 0;
            dwDeviceId = 0L; cRespPreambles = cMaxDevVars = cExtDevStatus = 0;cInternalFlags=0; sTag = "";};
   bool isEmpty(void) { return((wManufacturer|wDeviceType|cUniversalRev|cDeviceRev|
      cSoftwareRev|cHardwareRev|cZeroFlags|cReqPreambles|PollAddr|dwDeviceId|
          cRespPreambles|cMaxDevVars|wCfgChngCnt|cExtDevStatus/*|cInternalFlags*/) == 0L && sTag.empty());};//stevev - 24oct07 - fix the selection window value not found issues
	struct Identity_s& operator = (const struct Identity_s& s)
	{ wManufacturer=s.wManufacturer; wDeviceType=s.wDeviceType;  cUniversalRev=s.cUniversalRev;
      cDeviceRev=s.cDeviceRev;   cSoftwareRev=s.cSoftwareRev;   cHardwareRev=s.cHardwareRev;
      cZeroFlags=s.cHardwareRev; PollAddr=s.PollAddr;   cReqPreambles=s.cReqPreambles;
      dwDeviceId=s.dwDeviceId;   cRespPreambles=s.cRespPreambles;   cMaxDevVars=s.cMaxDevVars;
      wCfgChngCnt=s.wCfgChngCnt; cExtDevStatus=s.cExtDevStatus; cInternalFlags=s.cInternalFlags;
      sTag = s.sTag;
	  return (*this);};
}/*typedef*/Indentity_t;

// debug dump helpers

#define space(m)  std::setw((m>0)?m:1) << std::setfill(' ') << "" 
#define COUTSPACE cout<< space(indent)
#define CLOGSPACE clog<< space(indent)

/* stevev 02/26/04 - moved here to resolve include conflicts */
typedef enum notifyUpdateAction_e
{
	NO_change,	/* 0 :: 18nov05 - no longer a valid input (filtered out) */
	IS_changed, /* 18nov05 - now defined as value changed (not structure */
	STR_changed,/* added 18nov05 - generate a structure changed message  */
	STRT_actPkt,
	END_actPkt,
	STRT_methPkt,/* added 24jan06 - to restrict notifications from methods*/
	END_methPkt  /*               - ditto                                 */
   ,METH_changed /* sjv 6jun07-we must preclude copying in notifyAppVarChange()*/
   ,STAT_changed	// J.U. State is changed 17.02.11
   ,ABORT_dd	 /* stevev 11apr11 - UI has to kill dev obj so we have to tell */
   ,DEL_element  /* stevev 23may11 - list element no longer exists             */
}
/*typedef*/ NUA_t;   

/* stevev 10/15/04 - may never be used */
typedef enum devMode_e
{
	dm_Standard,
	dm_275compatible
	/* more later */
}
/*typedef*/ devMode_t;

/* stevev 19oct09 - we need to pass more information to newPsuedoItem */
typedef enum psuedoItemFlags // a bit-enum
{
	sif_None,
	sif_RegisterNew,		// 0x01
	sif_PreserveValue,		// 0x02
	sif_registerANDpreserve	// 0x03
}
/* typedef */ psuedoItemFlags_t;

#if 0 // the following is no longer true.  There is no differnce between missing and empty.
/* stevev 17sep10 - new base structure is needed
 *	Due to the complexity of the concatenated naming conventions, we
 *  have to be able to tell the difference between an empty string
 *  and a non-existent string.  
 *  eg a collection member description that was not supplied:
 *		DEVICE_VALUE, pressure_value;
 *  or a collection member description that was supplied empty:
 *		DEVICE_VALUE, temperature_value, "";
 *  so far, we have done the work by just testing for empty. It didn't work
 *	That will no lonfer work and pass the Host tests.
 *  The following 'light-weight class' is supplied to handle this situation.  
 *  It is structured so that you can cast the pointer to a wstring and 
 *  have the same access as before.
 *  ALL hCddlString functions will expect these and return these in parameters.
 *
 *****************************************************************************/
typedef struct sbSflags
{
	unsigned notMT : 1;// true at filled with something (even "")
}
/* typedef */ sbSflags_t;

typedef struct swStringEx
{
	wstring	    s;
	sbSflags_t sf;

	struct swStringEx() {sf.notMT = 0;};

	struct swStringEx& operator=(const wstring& src ) 
		{s = src; 
		 sf.notMT = 1; 
		 return *this;        };
	struct swStringEx& operator=(const struct swStringEx& src ) 
		{s = src.s; 
		 sf.notMT =((s.length()>0)||(src.sf.notMT>0))? 1:0;  
		 return *this;};
	void clear(void) 
		{sf.notMT = 0; // not.notMT=isMT
		 s.erase(); };
	 operator wstring& ()	{ return (s); };
}
/*typedef*/ wstringEx_t;
//   wstringEx_t strStruct;
//	*((wstring*)(&strStruct))  isa wstring
#endif // zero - no longer true...........
#endif//_DDBGENERAL_H

/*************************************************************************************************
 *
 *   $History: ddbGeneral.h $
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 5/05/03    Time: 6:06a
 * Updated in $/DD Tools/DDB/Common
 * added signalling code comment
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/28/03    Time: 11:48a
 * Updated in $/DD Tools/DDB/Common
 * Added an error def
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/28/03    Time: 10:30a
 * Updated in $/DD Tools/DDB/Common
 * added default float and int size definitions
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
