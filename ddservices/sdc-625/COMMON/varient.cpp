/*************************************************************************************************
 *
 * $Workfile: varient.cpp $
 * 25jun08 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This holds the implementation code for the HCF variant CValueVarient 
 *	
 *		25jun08	sjv	creation from the varient.h file
*/


#if defined(__GNUC__)
#include <cmath>
#endif // __GNUC__

#include "varient.h"


#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
  #ifndef _WIN32_WCE
    #define SCAN_FUNC  		sscanf_s
  #else
    #define SCAN_FUNC  		sscanf
  #endif
#else
    #define SCAN_FUNC  		sscanf
#endif

#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
    #define FORTH_PARAM  	,sizeof(char)
#else
    #define FORTH_PARAM  
#endif    


CValueVarient::CValueVarient() : vSize(0),vIsValid(false),vType(invalid),vIsUnsigned(false),
												   vIsDouble(false),vIndex(0),vIsBit(false)	
{ vValue.fFloatConst=0.0;
};
CValueVarient::CValueVarient(const CValueVarient& src) : vSize(0),vIsValid(false),vType(invalid),
			vIsUnsigned(false), vIsDouble(false),vIndex(0),vIsBit(false)	 { (*this) = src; };
CValueVarient::~CValueVarient(){clear();};

/*----------------------------------------------------------------------------------------------*/

void CValueVarient::clear(void)
{
	vType     = invalid;	vValue.fFloatConst = 0.0;
	vSize     = 0;			vIsValid           = false;		vIsUnsigned = false;
	vIsDouble = false;		vIndex             = 0;			vIsBit      = false;
	sWideStringVal.erase();	sStringVal.erase();
}

bool CValueVarient::valueIsZero(void) 
{   
	if      (vType == isBool)       return ((vValue.bIsTrue)          ?false:true);
	else if (vType == isIntConst)   return ((vValue.iIntConst   == 0) ? true:false);
	else if (vType == isVeryLong)   return ((vValue.longlongVal == 0) ? true:false);
	else if (vType == isFloatConst) 
					return (AlmostEqual2sComplement(vValue.fFloatConst,(double)0.0, 4)?true:false);
	else if (vType == isSymID)      return ((vValue.varSymbolID == 0) ? true:false);
	else return (true);
}

bool CValueVarient::isNumeric() const
{	
	if ( (vType == isBool)       || (vType == isIntConst)  || 
		 (vType == isFloatConst) || (vType == isVeryLong)  )
	{	return true;
	}
	else
	{	return false;
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////
//  CAST operators
/////////////////////////////////////////////////////////////////////////////////////////////////
CValueVarient::operator bool(void)
{	bool bVal;

	switch(vType)
	{
	case isBool:
		{	bVal = vValue.bIsTrue;  }							break;
	case isIntConst:
		{	bVal = ((vValue.iIntConst)? true : false);}			break;
	case isVeryLong:
		{	bVal = ((vValue.longlongVal)? true : false); }		break;
	case isFloatConst:
		{		bVal = ((vValue.fFloatConst)? true : false); }	break;
	case isString:
		{	bVal = ((sStringVal.size())? true : false); }		break;
	case isWideString:
		{	bVal = ((sWideStringVal.size())? true : false); }	break;
	case isSymID:	/* aka unsigned long */
		{	bVal = ((vValue.varSymbolID)? true : false);}		break;
	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			bVal = false; // error.
		}
	}
	return bVal;
}

CValueVarient::operator char(void)
{	
	char cVal;

	switch(vType)
	{
	case isBool:
		{	cVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				cVal = ((unsigned int)vValue.iIntConst);      // auto cast to char
			}
			else
			{
				cVal = vValue.iIntConst;      // auto cast to char
			}
		} break;
	case isVeryLong:
		{
			if ( vIsUnsigned )
			{
				cVal = (unsigned int)vValue.longlongVal;  // auto cast to char
			}
			else
			{
				cVal = (int) vValue.longlongVal;    // auto cast to char
			}
		}	
		break;
	case isFloatConst:
		{	cVal =  (char) vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%d", &cVal FORTH_PARAM ) <= 0) 
			{  cVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%d",&cVal) <= 0) {  cVal = 0;  }
		} break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	case isSymID:
	default:
		{
			cVal = 0; // error.
		}
	}
	return cVal;
}

CValueVarient::operator unsigned char(void)
{	
	unsigned char uVal;

	switch(vType)
	{
	case isBool:
		{	uVal = ((vValue.bIsTrue)? 1 : 0); }			break;
	case isIntConst:
		{	uVal = (unsigned char)  vValue.iIntConst;}	break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				uVal = (unsigned int)vValue.longlongVal; // auto cast to unsigned char
			}
			else
			{
				uVal = (int) vValue.longlongVal;  // auto cast to unsigned
			}
		}
		break;
	case isFloatConst:
		{	uVal =  (unsigned char) vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%u", &uVal FORTH_PARAM ) <= 0) 
			{  uVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%u",&uVal) <= 0) {  uVal = 0;  }
		} break;
	case isSymID:
		{	uVal = (unsigned char)  vValue.varSymbolID;  } break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			uVal = 0; // error.
		}
	}
	return uVal;
}

CValueVarient::operator short(void)
{	
	short cVal;

	switch(vType)
	{
	case isBool:
		{	cVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				cVal = ((unsigned int)vValue.iIntConst);      // auto cast to short
			}
			else
			{
				cVal = vValue.iIntConst;      // auto cast to short
			}
		} break;
	case isVeryLong:
		{
			if ( vIsUnsigned)
			{
				cVal = (unsigned int)vValue.longlongVal;  // auto cast to short
			}
			else
			{
				cVal = (int) vValue.longlongVal;    // auto cast to short
			}
		}
		break;
	case isFloatConst:
		{	cVal =  (short) vValue.fFloatConst;} break;
	case isString:
		{ 
			if ( SCAN_FUNC( sStringVal.c_str(), "%d", &cVal FORTH_PARAM ) <= 0) 
			{  cVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%d",&cVal) <= 0) {  cVal = 0;  }
		} break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	case isSymID:
	default:
		{
			cVal = 0; // error.
		}
	}
	return cVal;
}

CValueVarient::operator unsigned short(void)
{	
	unsigned short uVal;

	switch(vType)
	{
	case isBool:
		{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{	uVal = (unsigned short)  vValue.iIntConst;  } break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				uVal = (unsigned int)vValue.longlongVal; // auto cast to unsigned short
			}
			else
			{
				uVal = (int) vValue.longlongVal;  // auto cast to unsigned
			}
		}
		break;
	case isFloatConst:
		{	uVal =  (unsigned short) vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%u", &uVal FORTH_PARAM ) <= 0) 
			{  uVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%u",&uVal) <= 0) {  uVal = 0;  }
		} break;

	case isSymID:
		{	uVal = (unsigned short)  vValue.varSymbolID;  } break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			uVal = 0; // error.
		}
	}
	return uVal;
}

CValueVarient::operator double(void)
{	
	double fVal;

	switch(vType)
	{
	case isBool:
		{	fVal = ((vValue.bIsTrue)? 1.0 : 0.0); } break;
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				fVal = (double) ((unsigned int)vValue.iIntConst);  
			}
			else
			{
				fVal = (double) vValue.iIntConst;  
			}
		} 	break;
	case isVeryLong:
		{
			if ( vIsUnsigned)
			{// bill doesn't support unsigned long long to double conversion (in VS6)
				fVal  = (double) ( (__int64)(vValue.longlongVal & 0x7fffffffffffffffULL) ); // L&T Modifications : PortToAM335

				if ( (vValue.longlongVal & 0x8000000000000000ULL) != 0 ) // L&T Modifications : PortToAM335
				{
					fVal += 0x7fffffffffffffffULL;// highest positive value // L&T Modifications : PortToAM335
					fVal += 1;// but we need another...
				}
			}
			else
			{
				fVal = (double) vValue.longlongVal;  
			}
		}
		break;
	case isFloatConst:
		{	fVal =          vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%f", &fVal FORTH_PARAM ) <= 0) 
			{  fVal = 0.0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%f",&fVal) <= 0) {  fVal = 0;  }
		} break;


	case invalid:
	case isOpcode:
	case isDepIndex:
	case isSymID:
	default:
		{
			fVal = 0.0; // error.
		}
	}
	return fVal;
}

CValueVarient::operator float(void)
{	
	float fVal;

	switch(vType)
	{
	case isBool:
		{	fVal = (float)((vValue.bIsTrue)? 1.0 : 0.0); } break; // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				fVal = (float) ((unsigned int)vValue.iIntConst);  
			}
			else
			{
				fVal = (float) vValue.iIntConst;  
			}
		}	break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				fVal = (float) vValue.longlongVal;  
				if ( (vValue.longlongVal & 0x8000000000000000ULL) && fVal < 0 ) // L&T Modifications : PortToAM335
				{	fVal *= -1;    fVal += 0x8000000000000000ULL;   } // L&T Modifications : PortToAM335
			}
			else
			{
				fVal = (float) vValue.longlongVal;  
			}
		}
		break;
	case isFloatConst:
		{	fVal =  (float)vValue.fFloatConst;} break;  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	case isString:
		{// never works::	if ( sscanf(sStringVal.c_str(),"%f",&fVal) <= 0) {  fVal = 0.0;  }
			fVal = (float)atof(sStringVal.c_str()); // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		} break;
	case isWideString:
		{
			fVal = (float)_wtof(sWideStringVal.c_str());
		} break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	case isSymID:
	default:
		{
			fVal = 0.0; // error.
		}
	}
	return fVal;
}

CValueVarient::operator int(void)
{	
	int iVal;

	switch(vType)
	{
	case isBool:
		{	iVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				iVal = ((unsigned int)vValue.iIntConst);  
			}
			else
			{
				iVal = vValue.iIntConst;  
			}
		} break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				iVal = (unsigned int)vValue.longlongVal;  
			}
			else
			{
				iVal = (int) vValue.longlongVal;  
			}
		}
		break;
	case isSymID:	/* aka unsigned long */
		{	iVal = (unsigned int)  vValue.varSymbolID;  } break;
	case isFloatConst:
		{	iVal =  (int) vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%d", &iVal FORTH_PARAM ) <= 0) 
			{  iVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%d",&iVal) <= 0) {  iVal = 0;  }
		} break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			iVal = 0; // error.
		}
	}
	return iVal;
}

CValueVarient::operator unsigned int(void)
{	
	unsigned int uVal;

	switch(vType)
	{
	case isBool:
		{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			uVal = (unsigned int)  vValue.iIntConst;  
		} break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				uVal = (unsigned int)vValue.longlongVal;  
			}
			else
			{
				uVal = (int) vValue.longlongVal;  // auto cast to unsigned
			}
		}
		break;
	case isFloatConst:
		{	uVal =  (unsigned int) vValue.fFloatConst;} break;
	case isString:
		{
			if ( SCAN_FUNC( sStringVal.c_str(), "%u", &uVal FORTH_PARAM ) <= 0) 
			{  uVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%u",&uVal) <= 0) {  uVal = 0;  }
		} break;
	case isSymID:	/* aka unsigned long */
		{	uVal = (unsigned int)  vValue.varSymbolID;  } break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			uVal = 0; // error.
		}
	}
	return uVal;
}

CValueVarient::operator long(void)
{	
	long uVal = (int)(*this);
	return uVal;
}

CValueVarient::operator unsigned long(void)
{	
	unsigned long uVal = (unsigned int)(*this);
	return uVal;
}

// CPMHACK : Added for 32bit ARM compilation
#ifdef CPM_32_BIT_OS
CValueVarient::operator long long(void)
{	
	long long uVal = (int)(*this);
	return uVal;
}

CValueVarient::operator unsigned long long(void)
{	
	unsigned long long uVal = (unsigned int)(*this);
	return uVal;
}
#endif

/*CValueVarient::operator __int64(void)
{
	__int64 iVal;

	switch(vType)
	{
	case isBool:
		{	iVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			if (vIsUnsigned)
			{
				iVal = ((unsigned int)vValue.iIntConst);   // autocast 2 longlong
			}
			else
			{
				iVal = vValue.iIntConst;    // autocast
			}
		} break;
	case isVeryLong:
		{	iVal = vValue.longlongVal;  // autocast
		}
		break;
	case isFloatConst:
		{	iVal =  (__int64) vValue.fFloatConst;} break;
	case isString:
		{	if ( sscanf(sStringVal.c_str(),"%d",&iVal) <= 0) {  iVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%d",&iVal) <= 0) {  iVal = 0;  }
		} break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	case isSymID:
	default:
		{
			iVal = 0; // error.
		}
	}
	return iVal;
}

CValueVarient::operator UINT64(void)
{	
	UINT64 uVal;

	switch(vType)
	{
	case isBool:
		{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
	case isIntConst:
		{
			uVal = vValue.iIntConst;  
			//stevev 26jan09 - ALWAYS sign extend when up converting (even when unsigned)
			if (vValue.iIntConst & 0x80000000)
			{
				uVal |= 0xffffffff00000000ULL; // L&T Modifications : PortToAM335
			}
		} break;
	case isVeryLong:
		{
			if (vIsUnsigned)
			{
				uVal = ((UINT64)vValue.longlongVal);   // autocast 2 longlong
			}
			else
			{
				uVal = vValue.longlongVal;    // autocast
			}
		}
		break;

	case isFloatConst:
		{	uVal =  (UINT64) vValue.fFloatConst;} break;
	case isString:
		{	if ( sscanf(sStringVal.c_str(),"%u",&uVal) <= 0) {  uVal = 0;  }
		} break;
	case isWideString:
		{
			if ( wscanf(sWideStringVal.c_str(),L"%u",&uVal) <= 0) {  uVal = 0;  }
		} break;
	case isSymID:
		{	uVal = (unsigned int)  vValue.varSymbolID;  } break;

	case invalid:
	case isOpcode:
	case isDepIndex:
	default:
		{
			uVal = 0; // error.
		}
	}
	return uVal;
}*/


CValueVarient::operator string(void)
{
	string sVal;
	sVal = "";
	if (vType == isString)
	{
		sVal = sStringVal;
		// we won't deal with converting numerics to strings right now
	}
	// ws 12mar08
	else if (vType == isWideString)
	{
		sVal = TStr2AStr(sWideStringVal);
		// we won't deal with converting numerics to strings right now
	}
	// end ws 
	return sVal;
}

CValueVarient::operator wstring(void)
{
	wstring sVal;
	sVal = L"";
	if (vType == isWideString)
	{
		sVal = sWideStringVal;
		// we won't deal with converting numerics to strings right now
	}
	else if (vType == isString)
	{
		sVal = AStr2TStr(sStringVal);// UTF8 to Unicode conversion
		// we won't deal with converting numerics to strings right now
	}
	else
	{
		sVal = getAsWString();
	}
	return sVal;
}


wstring CValueVarient::getAsWString()
{
	wstring wS;
	const size_t TMPBUFF_SIZE = 68;
	wchar_t tmpBuff[TMPBUFF_SIZE];

	if (vIsValid)
	{
		switch (vType)
		{
		case CValueVarient::invalid:
			wS = L"*INVALID*";	                    break;
		case CValueVarient::isBool:
			wS =  (vValue.bIsTrue)?L"TRUE":L"FALSE";	break;
		case CValueVarient::isOpcode:
			{
				switch ((expressElemType_t)(vValue.iOpCode))
				{
				case eet_NOT:			//_OPCODE 1
					wS =  L"NOT";		break;
				case eet_NEG:			//_OPCODE 2
					wS =  L"NEG";		break;
				case eet_BNEG:			//_OPCODE 3
					wS =  L"BNEG";		break;
				case eet_ADD:			//_OPCODE 4
					wS =  L"ADD";		break;
				case eet_SUB:			//_OPCODE 5
					wS =  L"SUB";		break;
				case eet_MUL:			//_OPCODE 6
					wS =  L"MUL";		break;
				case eet_DIV:			//_OPCODE 7
					wS =  L"DIV";		break;
				case eet_MOD:			//_OPCODE 8
					wS =  L"MOD";		break;
				case eet_LSHIFT:		//_OPCODE 9
					wS =  L"LSHIFT";		break;
				case eet_RSHIFT:		//_OPCODE 10
					wS =  L"RSHIFT";		break;
				case eet_AND:			//_OPCODE 11
					wS =  L"AND";		break;
				case eet_OR:			//_OPCODE 12
					wS =  L"OR";		break;
				case eet_XOR:			//_OPCODE 13
					wS =  L"XOR";		break;
				case eet_LAND:			//_OPCODE 14
					wS =  L"LOGICALAND";		break;
				case eet_LOR:			//_OPCODE 15
					wS =  L"LOGICALOR";		break;
				case eet_LT:			//_OPCODE 16
					wS =  L"LESSTHAN";		break;
				case eet_GT:			//_OPCODE 17
					wS =  L"GREATERTHAN";		break;
				case eet_LE:			//_OPCODE 18
					wS =  L"LESSTHANOREQUAL";		break;
				case eet_GE:			//_OPCODE 19
					wS =  L"GREATERTHANOREQUAL";		break;
				case eet_EQ:			//_OPCODE 20
					wS =  L"EQUAL";		break;
				case eet_NEQ:			//_OPCODE 21
					wS =  L"NOTEQUAL";		break;
				default:
					LOGIT(CERR_LOG,L"VARIENT: Tried to output an unknown OpCode: %d\n" 
						 ,vValue.iOpCode);
					wS =  L"*UNKNOWN_OPCODE*";	
					break;
				}
			}
			break;
		case CValueVarient::isIntConst:
			swprintf(tmpBuff,
#if defined(__GNUC__)
				TMPBUFF_SIZE,
#endif // __GNUC__
				L"const %d (0x%02x)",vValue.iIntConst,vValue.iIntConst);
			wS  = tmpBuff ;
			break;
		case CValueVarient::isFloatConst:
			swprintf(tmpBuff,
#if defined(__GNUC__)
				TMPBUFF_SIZE,
#endif // __GNUC__
				L"const %f (0x%02x)",vValue.fFloatConst,vValue.iIntConst);
			wS  = tmpBuff ;
			break;
		case CValueVarient::isDepIndex:
			swprintf(tmpBuff,
#if defined(__GNUC__)
				TMPBUFF_SIZE,
#endif // __GNUC__
				L"*DEPIDX( %d (0x%02x))*",vValue.depIndex);
			wS  = tmpBuff ;
			break;
		case CValueVarient::isString:
			wS  =  L"String const |";
			wS +=  AStr2TStr(sStringVal);
			wS +=  L"|";
			break;
		case CValueVarient::isSymID:
			swprintf(tmpBuff,
#if defined(__GNUC__)
				TMPBUFF_SIZE,
#endif // __GNUC__
				L"Symbol(0x%04x)",vValue.varSymbolID);
			wS  = tmpBuff ;
			if (vIsBit)
			{
				swprintf(tmpBuff,
#if defined(__GNUC__)
					TMPBUFF_SIZE,
#endif // __GNUC__
					L"[0x%02x]",vValue.varSymbolID);
				wS  += tmpBuff ;
			}
			break;
		case CValueVarient::isVeryLong:	// added 2sept08 stevev
			swprintf(tmpBuff,
#if defined(__GNUC__)
				TMPBUFF_SIZE,
#endif // __GNUC__
				L"const %I64d(0x%04I64x)",vValue.longlongVal,vValue.longlongVal);
			wS  = tmpBuff ;
			break;
		case CValueVarient::isWideString:						// added 2sept08 stevev
			// wouldn't take a straight wstring, We'll just output the first char for now
			wS  =  L"WideString const |";
			wS += sWideStringVal.c_str();
			wS += L"|" ;
			break;
		default:
			LOGIT(CERR_LOG,"VARIENT: Tried to output an unknown type:%d\n",vType);
			wS =  L"*UNKNOWN_TYPE*";	
			break;
		}
	}
	else
	{
		wS = L"*FLAGGED_INVALID*";
	}
	return wS;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
// EQUAL Operators
/////////////////////////////////////////////////////////////////////////////////////////////////
CValueVarient& CValueVarient::operator=(const CValueVarient& src) 
{
	vType       = src.vType;		vValue    = src.vValue; 
	vIsUnsigned = src.vIsUnsigned;	vIsDouble = src.vIsDouble;

	if ( ! src.sStringVal.empty() ) 
	{ 
		sStringVal = (src.sStringVal.c_str());
	}
	else
	{
		sStringVal.erase();
	}

	if ( ! src.sWideStringVal.empty() )
	{ 
		sWideStringVal = (src.sWideStringVal.c_str());
	}
	else
	{
		sWideStringVal.erase();
	}

	vSize  = src.vSize;		vIsValid = src.vIsValid;
	vIndex = src.vIndex;	vIsBit   = src.vIsBit;
	return *this;
}// end operator=()



CValueVarient& CValueVarient::operator=(const unsigned char src) 
{// sign extend all for storage
	int l = 0; l |= src;
// this throws off the sizeandScale routines	if ( src & 0x80 ) l |= 0xffffff00;
	vType=isIntConst;
	vValue.iIntConst= l; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 1; 
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}


CValueVarient& CValueVarient::operator=(const char src) 
{// sign extend all for storage
	int l = 0; l |= src;
	if ( src & 0x80 ) l |= 0xffffff00;
	vType=isIntConst;
	vValue.iIntConst= l; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 1;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const unsigned short src) 
{// sign extend all for storage
	int l = 0; l |= src;
	if ( src & 0x8000 ) l |= 0xffff0000;
	vType=isIntConst;
	vValue.iIntConst= l; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 2;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}


CValueVarient& CValueVarient::operator=(const short src) 
{// sign extend all for storage
	int l = 0; l |= src;
	if ( src & 0x80 ) l |= 0xffff0000;
	vType=isIntConst;
	vValue.iIntConst= l; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 1;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const unsigned int src) 
{
	vType=isIntConst;
	vValue.iIntConst= (int)src; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 4;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

/*CValueVarient& CValueVarient::operator=(const itemID_t src)
{
	vType=isSymID;
	vValue.varSymbolID= src; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = sizeof(itemID_t);  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}*/

CValueVarient& CValueVarient::operator=(const int src) 
{
	vType = isIntConst;
	vValue.iIntConst= src; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 4;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const double src) 
{
	vType = isFloatConst;
	vValue.fFloatConst= src; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = true;
	vIsValid    = true;
	vSize       = 8;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const float src) 
{
	vType = isFloatConst;
//		vValue.fFloatConst= src; 
	// stevev 16feb06 - the only way I have seen to cast a float to a double
	//	without introducing an error that is smaller than FLT_EPSILON but
	//	much greater than DBL_EPSILON
	// stevev 24feb06 - the following will NOT convert nans!!!!!!!!!!!!!
	if (_isnan(src))
	{
		vValue.fFloatConst = src;
	}
	else
	if ( src <= -numeric_limits<float>::infinity() )
	{
		vValue.fFloatConst = -numeric_limits<double>::infinity();
	}
	else
	if ( src >= numeric_limits<float>::infinity() )
	{
		vValue.fFloatConst = numeric_limits<double>::infinity();
	}
	else
	{	  
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */	
  #ifndef _WIN32_WCE
		_gcvt_s( tmpBuf, _countof(tmpBuf), src, FLT_DIG+1);	
  #else
		_gcvt(src,FLT_DIG+1,tmpBuf);	// PAW 24/04/09
  #endif
#else			
		_gcvt(src,FLT_DIG+1,tmpBuf);
#endif			
		vValue.fFloatConst = atof(tmpBuf);
	}
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 4;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

// CPMHACK : Added for 32bit ARM compilation
#ifdef CPM_32_BIT_OS
CValueVarient& CValueVarient::operator=(const unsigned long long src) 
{
	vType=isVeryLong;
	vValue.longlongVal= (unsigned long long)src; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 8;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const long long src) 
{
	vType=isVeryLong;
	vValue.longlongVal= src; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 8;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}
#endif

//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
CValueVarient& CValueVarient::operator=(const UINT64 src) 
{
	vType=isVeryLong;
	vValue.longlongVal= (__int64)src; 
	sStringVal.erase();
	vIsUnsigned = true;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 8;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const __int64 src) 
{
	vType=isVeryLong;
	vValue.longlongVal= src; 
	sStringVal.erase();
	vIsUnsigned = false;
	vIsDouble   = false;
	vIsValid    = true;
	vSize       = 8;  
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}
#endif

CValueVarient& CValueVarient::operator=(const string& src)
{
	vType = isString;
	sStringVal= src; 
	vIsUnsigned = false;
	vIsDouble   = false;
	vSize       = (int)sStringVal.size(); // WS 9apr07 VS2005 checkin 
	vIsValid    =true; 
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}

CValueVarient& CValueVarient::operator=(const wstring& src)
{
	vType = isWideString;
	sWideStringVal= src;
	vIsUnsigned = false;
	vIsDouble   = false;
	vSize       = (int)sWideStringVal.size(); // WS 9apr07 VS2005 checkin
	vIsValid    =true;
	vIndex      = 0;
	vIsBit      = false;
	return *this;
}



/////////////////////////////////////////////////////////////////////////////////////////////////
//  Relational operators
/////////////////////////////////////////////////////////////////////////////////////////////////

bool CValueVarient::operator<=(const CValueVarient& src) // are we <= src
{
	CValueVarient newMe, newSrc;
	valueType_t workingType = promote(*this, newMe, src, newSrc);

	switch(workingType)
	{
	case isBool:
		{
			return (newMe.vValue.bIsTrue == newSrc.vValue.bIsTrue);// there is no relativity
		}
		break;
	case isIntConst:
		{
			if (newMe.vIsUnsigned)// modified 24jan06 - previously, 0 was never < 0xffffffff
			{
				return ( ((unsigned int)newMe. vValue.iIntConst) <= 
					     ((unsigned int)newSrc.vValue.iIntConst));
			}
			else
			{
				return (newMe. vValue.iIntConst <= newSrc.vValue.iIntConst);
			}
		}
		break;
	case isFloatConst:
		{			
			return (newMe. vValue.fFloatConst <= newSrc.vValue.fFloatConst);
		}
		break;
	case isVeryLong:
		{
			if ( newMe.vIsUnsigned )
			{// we are unsigned - compare magnitudes
				return ( ((UINT64)newMe.vValue.longlongVal) <= 
						 ((UINT64)newSrc.vValue.longlongVal) );
			}
			else
				return (newMe.vValue.longlongVal <= newSrc.vValue.longlongVal); 
		}
		break;
	default:
		return false; // an error
		break;
	}// endswitch
/* original code::::>>>>> removed on the hcf_UTF8 branch 25jun08 <<<<<<::::************/
}

	
bool CValueVarient::operator>=(const CValueVarient& src) 
{
	CValueVarient newMe, newSrc;
	valueType_t workingType = promote(*this, newMe, src, newSrc);

	switch(workingType)
	{
	case isBool:
		{
			return (newMe.vValue.bIsTrue == newSrc.vValue.bIsTrue);// there is no relativity
		}
		break;
	case isIntConst:
		{
			if (newMe.vIsUnsigned)// modified 24jan06 - previously, 0 was never < 0xffffffff
			{
				return ( ((unsigned int)newMe. vValue.iIntConst) >= 
					     ((unsigned int)newSrc.vValue.iIntConst));
			}
			else
			{
				return (newMe. vValue.iIntConst >= newSrc.vValue.iIntConst);
			}
		}
		break;
	case isFloatConst:
		{			
			return (newMe. vValue.fFloatConst >= newSrc.vValue.fFloatConst);
		}
		break;
	case isVeryLong:
		{
			if ( newMe.vIsUnsigned )
			{// we are unsigned - compare magnitudes
				return ( ((UINT64)newMe.vValue.longlongVal) >= 
						 ((UINT64)newSrc.vValue.longlongVal) );
			}
			else
				return (newMe.vValue.longlongVal >= newSrc.vValue.longlongVal); 
		}
		break;
	default:
		return false; // an error
		break;
	}// endswitch
/* original code::::>>>>> removed on the hcf_UTF8 branch 25jun08 <<<<<<::::************/
}

bool CValueVarient::operator==(const CValueVarient& src) 
{	
	bool ret = false;
	CValueVarient old_Me = *this, oldSrc = src, local;

	if (/*my*/vType != isOpcode && /*my*/vType != isDepIndex && /*my*/vType != invalid 
	 &&   src.vType != isOpcode &&   src.vType != isDepIndex &&   src.vType != invalid  )
	{
		if (old_Me.vType == isSymID)
		{
			local.clear();	local = (unsigned int)old_Me;
			old_Me.clear();	old_Me = local;
		}
		if (oldSrc.vType == isSymID)
		{
			local.clear();	local = (unsigned int)oldSrc;
			oldSrc.clear();	oldSrc = local;
		}

		if ( old_Me.isNumeric() && oldSrc.isNumeric() )
		{
			CValueVarient newMe, newSrc, oldMe = *this;
			valueType_t workingType = promote(old_Me, newMe, oldSrc, newSrc);

			switch (workingType)
			{
			case isBool:
				{
					ret = (newMe.vValue.bIsTrue   == newSrc.vValue.bIsTrue);
				}
				break;
			case isIntConst:
				{
					ret = (newMe.vValue.iIntConst == newSrc.vValue.iIntConst);
				}
				break;
			case isFloatConst:
				{
					ret = AlmostEqual2sComplement(newSrc.vValue.fFloatConst, 
												  newMe. vValue.fFloatConst, 4);
				}
				break;
			case isVeryLong:
				{
					ret = (newMe.vValue.longlongVal == newSrc.vValue.longlongVal);
				}
				break;
			default:
				ret = false;  // very much an error
				break;
			}//endswitch

		}
		else
		if ( (old_Me.vType == isString || old_Me.vType == isWideString) &&
			 (oldSrc.vType == isString || oldSrc.vType == isWideString)   )
		{//isString || isWideString or unknown error
			if (old_Me.vType == isString && oldSrc.vType == isString)
			{
				ret = (old_Me.sStringVal == oldSrc.sStringVal);
			}
			else
			if (old_Me.vType == isWideString && oldSrc.vType == isWideString)
			{
				ret = (old_Me.sWideStringVal == oldSrc.sWideStringVal);
			}
			else // they are strings but mismatched
			{
				wstring oneSide;
				if (old_Me.vType == isString)//oldSrc MUST be isWideString
				{
					oneSide = (wstring)old_Me;
					ret = (oldSrc.sWideStringVal == oneSide);
				}
				else // oldSrc MUST be isString and old_Me MUST be isWideString
				{
					oneSide = (wstring)oldSrc;
					ret = (old_Me.sWideStringVal == oneSide);
				}
			} 
		}
		else
		{// unknown type or mixed string and numeric types	
			ret = false; // not comparable
		}
	}
	// else these (invalid, isOpcode, isDepIndex) cannot be compared so cannot equal ever..
	// leave ret false
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Helper (private) Functions
//////////////////////////////////////////////////////////////////////////////////////////////////

/*** stevev 25jun08 - add promotions to cut down on the number of cases in the math portion *****/
struct INTEGER_RANK{ int rank; bool is_unsigned; };

/* promotes the two types according to C rules, returns type of result */
CVV::valueType_t CVV::promote(  const CVV& inOne, CVV& outOne, const CVV& inTwo, CVV& outTwo)
{
	valueType_t  retType = invalid;
	CVV          local;

	outOne.clear(); outOne = inOne;
	outTwo.clear(); outTwo = inTwo;

	// they both have to be numeric to be here
	if ( (! inOne.isNumeric()) || (! inTwo.isNumeric()) )
	{	
		return retType; // an error 
	}

	INTEGER_RANK oneRank, twoRank;
	INTEGER_RANK oneCnvt={0,false},twoCnvt= {0,false};
	
/*	First, if the corresponding real type of either operand is long double, the other
operand is converted, without change of type domain, to a type whose
corresponding real type is long double.   */
	/*
	--- We don't support long double at this time 
	*/ 
/*  Otherwise, if the corresponding real type of either operand is double, the other
operand is converted, to a double. */
	if ( (outOne.vType == isFloatConst &&  outOne.vIsDouble) && 
		 (outTwo.vType != isFloatConst || !outTwo.vIsDouble)   )
	{	
		local.clear();  
		local = (double)outTwo;  
		outTwo.clear(); 
		outTwo = local;
		retType = isFloatConst;
	}
	else 
	if ( (outOne.vType != isFloatConst || !outOne.vIsDouble) && 
		 (outTwo.vType == isFloatConst &&  outTwo.vIsDouble)    )
	{	
		local.clear(); 
		local = (double)outOne;  
		outOne.clear(); 
		outOne = local;
		retType = isFloatConst;
	}
	else 
	if ( (outOne.vType == isFloatConst &&  outOne.vIsDouble) && 
		 (outTwo.vType == isFloatConst &&  outTwo.vIsDouble)   )
	{	
		local.clear(); 
		local = (double)outOne;  
		outOne.clear(); 
		outOne = local;
		local.clear(); 
		local = (double)outTwo;  
		outTwo.clear(); 
		outTwo = local;
		retType = isFloatConst;
	}

/*  Otherwise, if the corresponding real type of either operand is float, the other
operand is converted to a float.  */
	else 
	if ( (outOne.vType == isFloatConst && !outOne.vIsDouble) && 
		 (outTwo.vType != isFloatConst ||  outTwo.vIsDouble)   )
	{	local.clear(); local = (float)outTwo;  outTwo.clear(); outTwo = local;
		retType = isFloatConst;
	}
	else 
	if ( (outOne.vType != isFloatConst ||  outOne.vIsDouble) && 
		 (outTwo.vType == isFloatConst && !outTwo.vIsDouble)   )
	{	local.clear(); local = (float)outOne;  outOne.clear(); outOne = local;
		retType = isFloatConst;
	}
	else 
	if ( (outOne.vType == isFloatConst && !outOne.vIsDouble) && 
		 (outTwo.vType == isFloatConst && !outTwo.vIsDouble)   )
	{	local.clear(); local = outOne;  outOne.clear(); outOne = local;
		local.clear(); local = outTwo;  outTwo.clear(); outTwo = local;
		retType = isFloatConst;
	}
	// else: neither is double nor float so fall thru to int handling

/*  Otherwise, the integer promotions are performed on both operands. Then the
following rules are applied to the promoted operands:   */
	if ( retType == invalid ) // no float types...
	{
		if (outOne.vType == isBool )
		{	
			oneRank.is_unsigned = false; oneRank.rank = 1;
		}
		else 
		if ( outOne.vType == isIntConst )
		{
			if ( outOne.vIsUnsigned ) 
				oneRank.is_unsigned = true;
			else // not unsigned => is signed
				oneRank.is_unsigned = false;

			switch (outOne.vSize)
			{
			case 1:		// char & unsigned char
				oneRank.rank = 2;
				break;
			case 2:		// short & unsigned short
				oneRank.rank = 3;
				break;
			case 4:		// int & unsigned int
				oneRank.rank = 4;
				break;
			default:		//0,3
				return invalid; // error return
				break;
			}
		}
		else 
		if ( outOne.vType == isVeryLong)
		{
			if ( outOne.vIsUnsigned ) 
				oneRank.is_unsigned = true;
			else // not unsigned => is signed
				oneRank.is_unsigned = false;

			if (outOne.vSize == 8)
			{
				oneRank.rank = 5;
			}
			else // size 5,6,7 & > 8
			{	return invalid; // error return
			}
		}
		else
		{	return invalid; // error return
		}

		
		if (outTwo.vType == isBool )
		{	
			twoRank.is_unsigned = false; twoRank.rank = 1;
		}
		else 
		if ( outTwo.vType == isIntConst )
		{
			if ( outTwo.vIsUnsigned ) 
				twoRank.is_unsigned = true;
			else // not unsigned => is signed
				twoRank.is_unsigned = false;

			switch (outTwo.vSize)
			{
			case 1:		// char & unsigned char
				twoRank.rank = 2;
				break;
			case 2:		// short & unsigned short
				twoRank.rank = 3;
				break;
			case 4:		// int & unsigned int
				twoRank.rank = 4;
				break;
			default:		//0,3
				return invalid; // error return
				break;
			}
		}
		else 
		if ( outTwo.vType == isVeryLong)
		{
			if ( outTwo.vIsUnsigned ) 
				twoRank.is_unsigned = true;
			else // not unsigned => is signed
				twoRank.is_unsigned = false;

			if (outTwo.vSize == 8)
			{
				twoRank.rank = 5;
			}
			else // size 5,6,7 & > 8
			{	return invalid; // error return
			}
		}
		else
		{	return invalid; // error return
		}

	/* If both operands have the same type, then no further conversion is needed. */
		if ( outOne.vType       == outTwo.vType        &&  
			 outOne.vIsUnsigned == outTwo.vIsUnsigned  &&  
			 outOne.vIsDouble   == outTwo.vIsDouble      )
		{	return outTwo.vType;// done
		}

	/* Otherwise, if both operands have signed integer types or both have unsigned
	integer types, the operand with the type of lesser integer conversion rank is
	converted to the type of the operand with greater rank. */
		if ( (( oneRank.is_unsigned) && ( twoRank.is_unsigned)) || 
			 ((!oneRank.is_unsigned) && (!twoRank.is_unsigned))  )
		{// lower to higher
			//oneCnvt,twoCnvt;
			if (oneRank.rank > twoRank.rank)
			{
				twoCnvt = oneRank;// other stays empty
			}
			else
			{
				oneCnvt = twoRank;// other stays empty
			}
		}
		else // one is signed, the other is unsigned
	/* Otherwise, if the operand that has unsigned integer type has rank greater or
	equal to the rank of the type of the other operand, then the operand with
	signed integer type is converted to the type of the operand with unsigned
	integer type.*/
		if ( oneRank.is_unsigned && oneRank.rank >= twoRank.rank )
		{// two converted to one's type
			twoCnvt = oneRank;// other stays empty
		}
		else
		if ( twoRank.is_unsigned && twoRank.rank >= oneRank.rank )
		{// one converted to two's type
			oneCnvt = twoRank;// other stays empty
		}
		else
	/* Otherwise, if the type of the operand with signed integer type can represent
	all of the values of the type of the operand with unsigned integer type, then
	the operand with unsigned integer type is converted to the type of the
	operand with signed integer type.  */
		if ( (!oneRank.is_unsigned) && oneRank.rank > twoRank.rank)
		{//two converted to one's type
			twoCnvt = oneRank;// other stays empty
		}
		else
		if ( (!twoRank.is_unsigned) && twoRank.rank > oneRank.rank )
		{// one converted to two's type
			oneCnvt = twoRank;// other stays empty
		}
		else
	/* Otherwise, both operands are converted to the unsigned integer type
	corresponding to the type of the operand with signed integer type.	*/
		if ( oneRank.is_unsigned )// two is SIGNED
		{//both to twoRank.rank and unsigned
			twoCnvt = oneCnvt   = twoRank;// other stays empty
			twoCnvt.is_unsigned = true;
			oneCnvt.is_unsigned = true;
		}
		else // one is SIGNED
		{//both to oneRank.rank and unsigned
			twoCnvt = oneCnvt   = oneRank;// other stays empty
			twoCnvt.is_unsigned = true;
			oneCnvt.is_unsigned = true;
		}

		// do the conversion(s)
		if (oneCnvt.rank > 0 )
		{// convert oneOut to oneCnvt type
			switch (oneCnvt.rank)
			{
			case 1:	// bool
				{	local.clear();  local  = (bool)outOne;  
					outOne.clear(); outOne = local;
					retType = isBool;
				}
				break;
			case 2:	// char
				{
					if (oneCnvt.is_unsigned)
					{	local.clear();  local = (unsigned char)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_UNSIGNED_CHAR;
					}
					else//signed
					{	local.clear();  local = (char)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_CHAR;
					}
				}
				break;
			case 3:	// short
				{
					if (oneCnvt.is_unsigned)
					{	local.clear();  local = (unsigned short)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_USHORT;
					}
					else//signed
					{	local.clear();  local = (short)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_SHORT;
					}
				}
				break;
			case 4:	// int
				{
					if (oneCnvt.is_unsigned)
					{	local.clear();  local = (unsigned int)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_UINT;
					}
					else//signed
					{	local.clear();  local = (int)outOne;  
						outOne.clear(); outOne = local;
						retType = isIntConst;//RUL_INT;
					}
				}
				break;
			case 5:	// long long
				{
					if (oneCnvt.is_unsigned)
					{	local.clear();  local = (UINT64)outOne;  
						outOne.clear(); outOne = local;
						retType = isVeryLong;//RUL_ULONGLONG;
					}
					else//signed
					{	local.clear();  local = (INT64)outOne;  
						outOne.clear(); outOne = local;
						retType = isVeryLong;//RUL_LONGLONG;
					}
				}
				break;
			default:
				outOne.clear();// error
				retType = invalid;//RUL_NULL;
				break;
			}// endswitch
		}// else no conversion on one

		if (twoCnvt.rank > 0 )
		{// convert twoOut to twoCnvt type
			switch (twoCnvt.rank)
			{
			case 1:	// bool
				{	local.clear();   local = (bool)outTwo;  
					outTwo.clear(); outTwo = local;
					retType = isBool;//RUL_BOOL;
				}
				break;
			case 2:	// char
				{
					if (twoCnvt.is_unsigned)
					{	local.clear();  local = (unsigned char)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_UNSIGNED_CHAR;
					}
					else//signed
					{	local.clear();  local = (char)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_CHAR;
					}
				}
				break;
			case 3:	// short
				{
					if (twoCnvt.is_unsigned)
					{	local.clear();  local = (unsigned short)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_USHORT;
					}
					else//signed
					{	local.clear();  local = (short)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_SHORT;
					}
				}
				break;
			case 4:	// int
				{
					if (twoCnvt.is_unsigned)
					{	local.clear();  local = (unsigned int)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_UINT;
					}
					else//signed
					{	local.clear();  local = (int)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isIntConst;//RUL_INT;
					}
				}
				break;
			case 5:	// long long
				{
					if (twoCnvt.is_unsigned)
					{	local.clear();  local = (UINT64)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isVeryLong;//RUL_ULONGLONG;
					}
					else//signed
					{	local.clear();  local = (INT64)outTwo;  
						outTwo.clear(); outTwo = local;
						retType = isVeryLong;//RUL_LONGLONG;
					}
				}
				break;
			default:
				outTwo.clear();// error
				retType = invalid;//RUL_NULL;
				break;
			}// endswitch
		}// else no conversion on two

	}//else let the float types stay
	return retType;
}// end of promote
