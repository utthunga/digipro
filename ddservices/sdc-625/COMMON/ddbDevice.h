/**********************************************************************************************
 *
 * $Workfile: ddbDevice.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the Device class
 *		4/30/2	sjv	started creation
 *
 *		
 * #include "ddbDevice.h"
 */

#ifndef _DDBDEVICE_H
 #define _DDBDEVICE_H

//#define DBDD_PATH   "C:\\Work\\HART\\DDDB"      /* default path */
// moved to ddbGeneral.h    #define DBDD_PATH   "\\hcf\\ddl\\library"	
/* "C:\\hcf\\ddl\\dev"  / * default path */
// moved to ddbdefs.h 18jan06 #define ROOT_MENU   1000   /* the item id of the root menu */
// moved to ddbdefs.h 18jan06 #define VIEW_MENU   1009
#include "ddbdefs.h"

#include "ddbGeneral.h"

#ifdef INI_OK
extern bool openIni();
extern bool closeIni();
#endif // INI_OK

//#include "ddbSysKey.h"

#include "ddbVar.h"
#include "dllapi.h"

//#include "itemBase.h"
//#include "ddbReference.h"
#include "ddbItemBase.h"
#include "ddbItems.h"
#include "ddbItemLists.h"
/*Vibhor 041203: Start of Code*/
#include "ddbDeviceMgr.h"
/*Vibhor 041203: End of Code*/
#include "Dictionary.h"
#include "LitStringTable.h"

//#include "ddbVariable.h"

//prashant for methods support.oct 2003
#include "ddbMethSupportInfc.h"

#include <map>

#include "ddbCmdDispatch.h"
// was #include "HartTrans.h"	/* comm object */
//#include "ddItem.h"
#include "ddbFileSupportInfc.h"
#include "ddbPublish.h"

#include "ddbCritical.h"//stevev 20jan05

extern const elementID_t MTelem;// in ddbDevice.cpp

// device's portion of the Method return code use | with HOST_INTERNALERR
// to designate these
#define DEVICE_NO_DISPATCH		0x0100
#define DEVICE_NO_COMMANDCLASS	0x0200

#define registerIt    ( ((si_Flags &   sif_RegisterNew)>0)?true:false )
#define preserveValue ( ((si_Flags & sif_PreserveValue)>0)?true:false )

#define FIRSTPSUEDOID  0xE0000000	/* stevev 30mar05 */

typedef map<ulong, hCitemBase*> itemMap_t;  // item id 2 item ptr map
											// the lists hold the actual items
typedef map<string,hCitemBase*> nameMap_t;  // item symbol Name 2 item ptr map

typedef struct elemDescriptor_s
{
	unsigned long elem_Idx;
	string        elem_name;
	itemIDlist_t  elem_users;
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	elemDescriptor_s() {elem_Idx=0;elem_name.erase();elem_users.clear();};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	elemDescriptor_s(const struct elemDescriptor_s& eDes){operator=(eDes);};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	elemDescriptor_s& operator=(const struct elemDescriptor_s& ed)
	{elem_Idx = ed.elem_Idx; elem_name = (ed.elem_name.c_str()); elem_users = ed.elem_users;
	 return *this;  };
} elemDesc_t;

typedef map<unsigned long,elemDesc_t> elemMap_t;  // element symbol number 2 element info map


/*********************************************************************************************/
/*********************************************************************************************/
//class hCddbDevice : public CddbBaseDevice, public hCdeviceSrvc   
//    has hCobject because it has things to do too
class hCddbDevice : public hCobject, public hCdeviceSrvc      // the device instance class
{
protected:		/* we need to be subclassed to get the CommAPI in      */
				/* protected will allow those subs to get to this info */
#define SVRSTATUS(a)  (a>>16) 
#define RTNSTATUS(a)  (a&0xFFFF)

// now commifc	CHartTrans*   pHartComm;  // the communications interface - mem owned by others

//	CsysKey       systemKey;  // unique ID of one particular device instance 
						      // use getDDkey to aquire that info
	DD_Key_t      devDDkey;
	Indentity_t   ddbDeviceID;// supporting data
	// added for file sniffing support 5/11/04
	FILE_INFO_T   devFileInfo;
	devMode_t	  compatabilityMode;

	// ItemLists of component instances
	CVarList			varInstanceLst; // list of variable instances
	CCmdList			cmdInstanceLst; // list of command instances
	CMenuList			menuInstanceLst;
	CEditDispList		edDispInstanceList;
	CmethodList			methodInstanceList;
	CRefreshList		refreshInstanceList;
	CunitRelList		unitRelInstanceList;
	CwaoList			waoInstanceList;
	CitemArrayList		itemArrayInstanceList;
	CcollectionList		collectionInstanceList;

	CvalueArrayList		valArrayInstanceList;
	CfileList			fileInstanceList;
	CchartList			chartInstanceList;
	CgraphList			graphInstanceList;
	CaxisList			axisInstanceList;
	CwaveformList		waveformInstanceList;
	CsourceList			sourceInstanceList;
	ClistList			listInstanceList;
	CgridList			gridInstanceList;
	CimageItemList		imageItemInstanceList;

	Image_PtrList_t     imageInstanceList;

	hCmutex				itemMutex;		//we own it
	hCmutex::
		hCmutexCntrl*	pItemMutxCntrl;	// AND we control it
	hCFileSupport*      m_pFileSupport;   // for use by glblInfc Only
	hCcritical          m_criticalItems;	// stevev 20jan05

// should be in cmd dispatcher....	RETURNCODE  processCmdStatus(BYTE cmdNum, UINT16 cStatus);

	RETURNCODE collectXrefInfo(void);//catchall for extracting info from data(assumes lists OK)

	int nLastMethodsReturnCode;

	/* The master list must be public so the lists can insert themselves */

	int weightPerQuery;	// stevev 2/17/04

	itemID_t nextPsuedoID; // stevev 30mar05

// stevev 06mar07 - added a place for common execution code
protected:
	RETURNCODE 
		   doExecute(hCmethodCall oneMethod, CValueVarient& returnValue, bool isMultipleEntry);
// end 06mar07 ----

public:
	
	bool          isDDKeyPresent;
	
	itemMap_t     masterMap;      // the master cross reference of all the 
								  //     components in the instance lists
	nameMap_t     masterNameMap;  // device's symbol lookup

	elemMap_t     masterElementMap;// only used in tokenizer

	MEE* pMEE;	// stevev 07mar07 made public so MethodSupport could execute

	hCcmdDispatcher* pCmdDispatch;
	bool   enableDispatchDeletion;

	/* Dictinary (Get Dicitionary pointer from device manager )*/
	CDictionary *   dictionary;
	LitStringTable* literalStringTbl;

	hCsubscriptions   subscriptionHandler;

	int  updateCnt; /* stevev - 2/16/04 - added for helping timer track the time */

	__int64 idBase; /* stevev - 28aug09 - application id 1/2 for message id */

#ifdef _DEBUG
	void dmpVarNotification();
	void dmpVarDependency();
#endif

public:
	/////// DATA ////////
	StrVector_t DeviceDescStrings;
	//prashant .oct 2003
	hCMethSupport* m_pMethSupportInterface;

	/*Vibhor 041203: Start of Code*/

	hCDeviceManger *pDevMgr; /*We need this one to get a device ptr giving its handle*/

	bool     devIsExecuting; // stevev 04apr10 - added to know when we are in a method
	dState_t devState;		// stevev 12jul07 made it public
#define IS_ONLINE   ((devState & ds_OnLine)?true:false)  /* stevev 5sep07 */

	OperatingMode_t OpMode;
	OperatingMode_t getOpMode(void);
	void  setOpMode(OperatingMode_t g)
	{	OpMode = g;	};
	void  clrOpMode(OperatingMode_t c)
	{
		int y = (OpMode & (~c));
		OpMode = (OperatingMode_t)y;
	};

	/*Vibhor 041203: Start of Code*/

	/////// CON/DIS ////////
	//hCddbDevice(CHartTrans* pIn = NULL, const char* pDbdir=DBDD_PATH);
	hCddbDevice(DevInfcHandle_t h); // we now do a 2 step construction 'new' then 'instantiate'
	hCddbDevice(const hCddbDevice& src);
	~hCddbDevice();    // we do a little more now; { closeIni();};
	void destroy(void);// totally empties the device

	/////// REAL WORK ////////
	// a key parameter will override the serial connection for identity
// use the new device factory 
//				 	    'instantiate'RETURNCODE instantiate(unsigned long incomingDDkey = 0); 
	RETURNCODE 
	instantiate(DD_Key_t DDkey, DevInfcHandle_t newHndl, aCdevice* pADev, hCcmdDispatcher* pCD,
					hCMethSupport* pMethSupportInterface, hCFileSupport*  pFileSupport,
					bool devOwnsDispatch = false); // will delete it when true
// now in the device dispatcher...	RETURNCODE connect(	unsigned char deviceAddress = 0)
									//	{	return(pHartComm->Connect(deviceAddress)); };

	/*Vibhor 051203: Start of Code*/

	RETURNCODE 
		populateStandard(DD_Key_t DDkey, DevInfcHandle_t newHndl, aCdevice* pADev);

	/*Vibhor 051203: End of Code*/

	// accessors --- the following interface is being phased out - use get item by id method
	RETURNCODE     get(int itemID, hCitemBase** returnItem);// unresolved item & subitems <raw>
	RETURNCODE   fetch(int itemID, hCitemBase** returnItem);// resolved item w/ raw subitems
	RETURNCODE  aquire(int itemID, hCitemBase** returnItem);// a fully resolved item & subitems
	RETURNCODE procure(int itemID, hCitemBase** returnItem); // a fully resolved item	
	RETURNCODE procure(hCreference& itmRef, hCitemBase** returnItem); // a fully resolved item

/*
	hCpayload*         resolveCond (hCconditional<hCpayload>* pPc) // returns payload* or NULL
	{	cerrxx<< "No body for device's resolveCond." << endl;
		return NULL;
	};
	vector<hCpayload>* resolveCondList(hCcondList<hCpayload>* pcL) // returns payload* or NULL
	{	cerrxx<< "No body for device's resolveCondList." << endl;
		return NULL;
	};
	void  insert2map(pair<int, hCitemBase*>& insertionData)
	{
		masterMap.insert( insertionData );
	};
	// * * end base device * * 
*/

	// * *  hCregisterItem  *  * pure virtual function
	//void  insert2map(pair<int, hCitemBase*>& insertionData)
//	void insert2map(symbolNumb_t synNum, hCitemBase* pNewItempointer)
//	{
//		pair<int, hCitemBase*> insertionData = {synNum,pNewItempointer};
//		masterMap.insert( insertionData );
//	};		
	// * * end register virtual implementation * * 

	// * *  hCdeviceSrvc  *  * pure virtual functions
	// assume unused till proven otherwise         RETURNCODE InitComm(void);
	RETURNCODE getItemBySymNumber(itemID_t symNum, hCitemBase** ppReturnedItempointer);
	RETURNCODE getItemBySymName  ( const string& symName,hCitemBase** ppReturnedItempointer);
	/* stevev 20sep06
	   this is needed to force a read of a variable with a default value if it is critical */
	RETURNCODE ReadCritical(hCitemBase* itemPtr,  CValueVarient& ppReturnedDataVal);
	void       readCriticalparams(int behaviour = 0);

	RETURNCODE ReadImd(itemID_t           itemID,   CValueVarient& ppReturnedDataItem,
							bool isAtest=false);
	RETURNCODE ReadImd(hCitemBase*        itemPtr,  CValueVarient& ppReturnedDataVal,
							bool isAtest=false);

	RETURNCODE WriteImd(itemID_t          itemID,   CValueVarient* dataVal);	// -> to RTDB
	RETURNCODE WriteImd(hCitemBase*       itemPtr,  CValueVarient* dataVal);	// ->

	RETURNCODE Read(vector<hCitemBase*>&  itemList); // <-
	RETURNCODE Write(vector<hCitemBase*>& itemList); // ->
	
	DD_Key_t   getDDkey(void);//{ return systemKey.getDDkey();};
	void       getStartDate(dmDate_t& dt);

	RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue);
	RETURNCODE executeMethod(vector<hCmethodCall>& actionList,CValueVarient& returnValue);
	RETURNCODE executeMethod(vector<hCmethodCall>& actionList);

	RETURNCODE sendMethodCmd(int commandNumber,int transactionNumber = -1, 
				indexUseList_t* pI=NULL);// added indexes 29nov11(dynamics weren't updating)
	// stevev 2/17/04
	RETURNCODE sendCommand(int commandNumber,int transactionNumber = -1 );

	int  queryWeight(void)     { return weightPerQuery;};
	void setQueryWeight(int qw){ weightPerQuery = qw;  };
	// end 2/17/04
	
	bool isItemCritical(itemID_t  itm);/* stevev 10/13/04 */

	/* stevev 08nov04 - added file interface */
	RETURNCODE fillme(hCfile* fileClass);
	RETURNCODE saveme(hCfile* fileClass);
	void         setIdentity(Indentity_t& id);// moved to .cpp 26jun15 {ddbDeviceID=id;};
	Indentity_t& getIdentity(void){return (ddbDeviceID); };
	/* end stevev 08nov04*/
	/* stevev 09dec04 */
	RETURNCODE subscribe(int& returnedHandle, ddbItemList_t SubscribedList,
					hCpubsub* pPublishTo,	// pointer to the callback class
					unsigned long cycleTime = 1000,	// requested update interval
					int cyclesPerPublish    = 1  
					);

	RETURNCODE unsubscribe(int handle);
	/* endstevev 09dec04 */
	/* stevev 20jan05 */
	hCcritical* getCriticalPtr(void) { return &m_criticalItems;};
	/* endstevev 20jan05 */
	 /* stevev 30mar05 newPsuedoItem - see note at the end of this file */
	hCitemBase* newPsuedoItem(hCitemBase* pTypeDef, hCitemBase* pValueDef, 
				psuedoItemFlags_t si_Flags = sif_RegisterNew, elementID_t thisElement= MTelem);
	hCitemBase* newPsuedoMenu(hCmenuItem* pContent);// stevev 09sep11 - added. See Notes below
	RETURNCODE  destroyPsuedoItem(hCitemBase* pPsuedoItem);// destroys,unregisters,unlists same
	itemID_t    getAPsuedoID(void) { return (nextPsuedoID++); };
	
	devMode_t  whatCompatability(void){ return compatabilityMode;};// 10/15/04
	void       setCompatability(devMode_t newMode){compatabilityMode=newMode;};
	bool	   sizeImage(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInLn);
	bool	   sizeString(string* c_str, unsigned& xCols, unsigned& yRows, 
										int hgtLim=(int)gS_Unknown);// ask the UI for service
	__int64    getIDbase(void)  {  return idBase; };
	
	/* stevev 11apr11 - abort from anywhere..mainly no list element */
	void abortDD(int retCode, hCitemBase* pList);
	/* stevev 10apr13 - see if we are in amethod */
	bool isInMethod(void) { return devIsExecuting;};


	/* stevev 15Nov10 - let the strings get a string in a conditional */
	wstring   getLiteralString(unsigned stringIndex)
	{         return (literalStringTbl->get_lit_string(stringIndex));    };


	//prashant .oct 2003
	hCMethSupport*   getMethodSupportPtr();
	unsigned int getMEEdepth();
	RETURNCODE GetLastMethodsReturnCode(void); // this returns the error code from the method
	void       cacheDispValues(bool generateNotify = false);
	void       uncacheDispVals(bool saveValuesCalled);

	void	systemLog(int channel, char* format, ...) ;
	void	systemLog(int channel, int errNumber,...) ;

	//         required by constructors
	void       registerItem(itemID_t newItemID, hCitemBase* pNewItem, string& symName); 
	void       unRegister(hCitemBase* rmvItem);
	hCobject*  getListPtr(CitemType  it);//must be cast to proper list type( like new() does )
	
	void       registerElement(unsigned long elemIdx,string& elemName, itemID_t grpID);
	RETURNCODE getElemNameByIndex(unsigned long elemIdx, string& returnedName);

	void       notifyVarUpdate(itemID_t i,  NUA_t isChanged = NO_change, long aTyp = 0L);
#ifdef _DEBUG
	void       aquireItemMutex(char* fileNm, int LineNum);	
	void       returnItemMutex(void);	
	RETURNCODE aquireIndexMutex(int millis, char* fileNm, int LineNum);		
#else
	void       aquireItemMutex(void);	
	void       returnItemMutex(void);	
	RETURNCODE aquireIndexMutex(int millis);		
#endif
	void       returnIndexMutex(void);	
	
	int			getDeviceState(void) { return devState;};
	int			setDeviceState(dState_t dSt) 
	{int i = devState;
	devState = dSt;
#ifdef _DEBUG
	if (i == ds_Closing && dSt != ds_Closing)
	{
		_CrtDbgBreak();
	}
#endif
	return i;};
	// * * end device service virtual implementation * * 


	RETURNCODE get(int itemID, vector<hCitemBase*>& itemList);// generic get
	RETURNCODE get(vector<hCVar>& variableList);
//	UINT32     getDDkey(void){ return systemKey.getDDkey();};

#define \
	GET_ROOT_MENU(ppitem)   procure(ROOT_MENU,ppitem)


	// misc stuff
	PFILE_INFO_T getFileInfoPtr(void) {return (&devFileInfo);};
	void displayVars(void);
	void dumpSelf(int indent=0);
	void fillDepTree(void);

	hSdispatchPolicy_t getPolicy(void) 
	{	hSdispatchPolicy_t newP; 
		if ( pCmdDispatch ) 
			return (pCmdDispatch->getPolicy());else return newP;};
	hSdispatchPolicy_t setPolicy(hSdispatchPolicy_t newP)
	{	return(pCmdDispatch->setPolicy(newP));	};

	bool isTypeCompatible(hCitemBase* pDest, hCitemBase* pSorc);// stevev 27oct11 for ListInser

// L&T Modifications : MethodSupport - start
public:
	RETURNCODE executeMethodForMS(hCmethodCall oneMethod, CValueVarient& returnValue, hCddbDevice *pCurDev);
	RETURNCODE doExecuteForMS(hCmethodCall oneMethod, CValueVarient& returnValue, bool isMultipleEntry, hCddbDevice *pCurDev);
// L&T Modifications : MethodSupport - end

// L&T Modifications : VariableSupport - start
	RETURNCODE executeMethodForVS(methodCallList_t& actionList,CValueVarient&returnValue, hCddbDevice *pCurDev);
// L&T Modifications : VariableSupport - end

// L&T Modifications : ABB Tx. Support - start
	RETURNCODE executeMethodForMS(vector<hCmethodCall>& actionList, hCddbDevice *pCurDev);
// L&T Modifications : ABB Tx. Support - end
};

#endif//_DDBDEVICE_H

/********************************* NOTES ******************************************************
 *	newPsuedoItem  - this is a 100% copy constructor for Typedef duplication.
 *		The first parameter is the Typedef template we are going to copy.
 *		The second is the original item where we are going to get the values for the copy.
 *		This will make a duplicate of the first and update it's value to that of the second.
 *		It is different from our copy constructors because it duplicates everything, even
 *		those classes that are only pointed to.
 *		The initial version of this will not copy items that cannot hold values, like 
 *		methods and commands and various relations.  Note that collections and item-arrays
 *		can get big in a hurry.
 *		If flagged to registerIt, this routine puts the pointer in the instance list & 
 *		registers it in the symbol map and in the symbol name map
 * * * 26sep12 - added the element ID to the call so the psuedoitem whill know its place and
 *		parent.  This is needed for 'resolved references' in the tokenizer.
 *
 *  destroyPsuedoItem - this will UNregister the item (if it's registered) from the symbol map
 *		and the symbol name map.  It will remove the item from the instance list and then it  
 *		will do a destroy on the item which will remove/delete all pointed-to memory.  The  
 *		caller should do a delete on the item itself.
 *
 *  new 09sep11 - to support eddl items from the table view.
 *  newPsuedoMenu - this generates a dialog menu with a single item (pContent) in it.
 *		It will always register the menu.  Use destroyPsuedoItem() to get rid of it.
 *
 **********************************************************************************************
 */

/**********************************************************************************************
 *
 *   $History: ddbDevice.h $
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/Common
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 **********************************************************************************************
 */
