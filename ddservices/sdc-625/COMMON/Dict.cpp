/*	Dict.cpp

  created		3/18/02	sjv	separated out as a base class


  */

#include "dict.h"
#ifndef READDICTIONARY
#endif

#ifndef TASKDEFINED
#define TASKDEFINED
DB_TASK Currtask;
#endif

using namespace std;

Cdict::Cdict(string dfltLang) : defaultLang(dfltLang),dictDB(-1)
{
#ifndef TASKDEFINED
	dt_opentask(&Currtask);
#endif
}

Cdict::~Cdict()
{
	defaultLang.erase();
#ifndef TASKDEFINED
	dt_closetask(&Currtask);
#endif
}

bool Cdict::setDefaultLang(string lang)
{ 
	if (lang.length() == 2) 
	{
		defaultLang = lang; 
		return FALSE; 
	}
	else
	{
		return TRUE;
	}
}


int Cdict::dumpDict(ostream &outputstream, int dictType /*=0*/)
{
	return 0;
}

string glblNotImp = "Virtual getDictString function is not implemented.";


// fetch string using specified language
// fetch a string from combined section/offset
string & Cdict::getDictString(int value, int dictType /*=0*/, string lang/*="USEDFLT"*/)
{
	return glblNotImp;
}
// fetch string using separate values, (this may be DDkey,ItemID)
string & Cdict::getDictString(int section, int offset, int dictType /*=0*/, string lang/*="USEDFLT"*/)
{
	return glblNotImp;
}

RETURNCODE Cdict::loadDictionaries(StrVector_t& vdictFiles)
{
	cerr << "ERROR: Cdict does not implement loadDictionaries."<<endl;
	return -1;
}
