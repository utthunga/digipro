
/* 
#include "pvfc.h"    
*/

#ifndef _PVFC_
#define _PVFC_

#include "logging.h"

/* for pure virtual function call debugging */
#ifdef DBG_PVFC
#define  PVFC( a )    {LOGIT(CLOG_LOG|CERR_LOG,L"ERROR: Pure Virtual Function Call.(%s)\n", a );}
#define RPVFC( a , r ) {LOGIT(CLOG_LOG|CERR_LOG,L"ERROR: Pure Virtual Function Call.(%s)\n", a );return r;}
#else // normal build
#define  PVFC( a )	   = 0
#define RPVFC( a , r ) = 0
#endif

#endif
