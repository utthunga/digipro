/*************************************************************************************************
 *
 * $Workfile: NamedPipe.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the common info on the named pipe from a client to XmtrDD
 *		
 *
 * #include "NamedPipe.h"
 */

/*
\\ServerName\pipe\PipeName

where ServerName is either the name of a remote computer or a period, 
to specify the local computer. The pipe name string specified by PipeName 
can include any character, including numbers and special characters. 
The entire pipe name string can be up to 256 characters long. Pipe names 
are not case-sensitive. 
*/

#define PIPENAME _T("\\\\.\\pipe\\XmtrDDpipe")

#define PIPEBUFFERSIZE 512

/* L&T Modifications : WINXMTR_01	- Define this Macro SERIAL_PORT_NOT_PIPES for the xmtrDD 
application to communicate in Serial port with the SDC625 Console application.
Undefine to work in pipes
*/
#define SERIAL_PORT_NOT_PIPES
/* L&T Modifications : WINXMTR_02	- Change the Number macro SERIAL_PORT_NUMBER which 
denotes the COM Port Number Eg 1 for "COM1" etc.,

SERIAL_PORT_BAUD_RATE	- For the Baud rate in Serial port Eg. 9600 bps

SERIAL_PORT_STOP_BITS	- For the Stop bits. Eg 1 for 1 stop bit, 
							2 for 2 Stop bit, 1.5 for 1.5 stop bit

Rest of the paramers are fixed as 8-N means Data Bits - 8, No Parity
*/
#define SERIAL_PORT_NUMBER		(1)
#define SERIAL_PORT_BAUD_RATE	(9600)
#define SERIAL_PORT_WORD_LEN	(8)
#define SERIAL_PORT_PARTIY		(0)
#define SERIAL_PORT_STOP_BITS	(2)

#if 0
// L&T Modifications : TODO List
#define TODO(WINXMTR_03, "The above serial port parameters has to be loaded dynamically
			using .ini files")
#define TODO(WINXMTR_04, "Instead of Polling method of Serial Port, serial events 
			will yield better result")
#endif

// common file for pipes
extern bool lE;	/* Make true to enable detailed pipe transaction logging (le logging enabled)*/
#define STARTLOG	if (lE){ clog
#define ENDLOG		endl;}
// use STARTLOG<<"log message"<<ENDLOG


/*  XmtrDD is the server and opens this pipe and then services it */

/*

  In the future: HART messages will be transacted accros this interface!
  For now we'll use this messaging system::>

note: the \' are not part of the message, they delineate the transmitted string
      from the informative narrative.
  Client writes a message :
  READ space
  the decimal number string of the symbol ID to be returned

  WRITE space
  the decimal number string of the symbol ID to be returned
  number-of-bytes in following string  space
  quote data-string quote (see discussion below)


  Server Responds:
  ResponseCode in decimal  space
  the decimal-number string of the symbol ID requested
  number-of-bytes in following string  space < zero on write response >
  quote data-string quote                    < "" on write response   >

  **special: 'READ 0' ** is the identity function
  **respond: '0 0 8 "00370201"'   the requested,status.len,ddKey for the device being served ***

  data string will be the value in whatever format fits
  ie
  float "-4.5673809"
  string "The string"
  double "2.3456E-3"
  etc.

client  -> / <- server
		  -> 'READ 0'
		  <- '0 0 8 "00370201"'
		  -> 'WRITE 1176 4 "1123"
		  <- '1176 0 0 ""'
		  -> 'READ 1176'
		  <- '1176 0 4 "1123"'

*/


/*************************************************************************************************
 *
 *   $History: NamedPipe.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Common
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */