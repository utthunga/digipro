// LitStringTable.cpp: implementation of the LitStringTable class.
//
//////////////////////////////////////////////////////////////////////

#include "foundation.h"
#include "LitStringTable.h"
#include "Dictionary.h"
#include "FMx_Dict.h"

extern CDictionary *pGlobalDict; /*The Global Dictionary object*/


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LitStringTable::LitStringTable()
{
}

LitStringTable::~LitStringTable()
{
	// stevev - 28sep11 - this stuff has to go away sometime
	unsigned long I;
	char* actualStr = NULL;
	map<unsigned long, char *, ulong_lt>::iterator pos;
	for (pos = table.begin(); pos != table.end(); pos++)
	{
		I = pos->first;
		actualStr = pos->second;
		if (actualStr)
			delete [] actualStr;
		pos->second = NULL;

		/* I'm not real sure what is being attempted here...it assumes consecutive indexes...
		actualStr = table[I];
		delete [] actualStr;
		table[I] = NULL;
		*/
	}
	table.clear();
}

void LitStringTable::install(unsigned long index, char *s)
{
	if ( s )
		table.insert( make_pair(index, s) );
}

unsigned LitStringTable::install(char* s)
{
	unsigned f = table.size();
	install(f, s);
	return f;
}

wstring LitStringTable::get_lit_string(unsigned long index)
{
	if ( table.find(index) != table.end() )
	{
		// use dictionary to localize language [3/31/2015 timj]
		wstring litstr, locstr;
		litstr = UTF82Unicode(table[index]);

		bool ifoundit = litstr.find(L"4 wire analysis device") != wstring::npos;

		pGlobalDict->get_string_translation(litstr, locstr);
		return locstr;
	}
	else
		return wstring(_T(""));// otherwise it would make a record.
}


char* LitStringTable::get_lit_char(unsigned long index)
{
	if ( table.find(index) != table.end() )
		return table[index];
	else
		return "";// otherwise it would make a record.
}
void LitStringTable::dump()
{
	cout << "\nLiteral String Table    Count: " << table.size() << "\n";
	cout << "\nFormat     Lit: [index]  [str]\n\n";

	map<unsigned long, char *, ulong_lt>::iterator pos;
	for (pos = table.begin(); pos != table.end(); pos++)
	{
		wstring litstr = get_lit_string(pos->first);

		cout   << "Lit: \t" << pos->first << "\t" << addlinebreaks(ws2s(litstr))
			   << endl;
	}

	cout << "\n\n" ;
}


// build lit string table from binary
//	copy all strings so that they are not de-allocated!
//	all lit string entries from the binary have been used by the DD or an import

int LitStringTable::makelit(STRING_TBL *string_tbl, bool isLatin1)
{
	ddpSTRING			*string = NULL;	 	/* temp pointer for the list */
	ddpSTRING			*end_string =NULL;	/* end pointer for the list */

	unsigned long index = 0;
	for (string = string_tbl->list, end_string = string + string_tbl->count;
		 string < end_string; string++) 
	{// a list of ddpSTRING

		char *hld = string->str;
		char *lit = NULL;
		string->str = NULL;// stevev 28sep11 - take ownership of the memory
		if (isLatin1)
		{
			//int iAllocLength = latin2utf8size(string->str) + 1;
			int iAllocLength = latin2utf8size(hld) + 1;
			lit = new char[iAllocLength];
			if (lit == (char *) 0)
			{
				LOGIT(CERR_LOG,L"Memory exhausted.\n");
				exit(-1);
			}
			
			//latin2utf8(string->str, lit, iAllocLength);
			latin2utf8(hld, lit, iAllocLength);
			delete[] hld;
		}
		else
		{
			lit = hld;	// before this change, valid utf8 strings caused a 
						// NULL to install in the table [2/27/2014 timj]
		}

		install(index++, lit);
	}
	
	return 0;	// dicterrs
}

// build lit strings from an FMx_Dict object.
// this occurs when building fma/8's with new 10 tokenizer
// copy strings into *this and ERASE the strings in the FMx_Dict
// to avoid too many string copies
int LitStringTable::makelit( FMx_Dict *fmxdict )
{
	//  change from FMx style dictionary (list of strings with one index) 
	// to SDC LitStringTable format ( (key, string) tuples)

	/* redesigned 12jan16
	StrList_t::iterator strIter;
	unsigned idx = 0;
	for (strIter = fmxdict->theStrList.begin(); strIter != fmxdict->theStrList.end(); strIter++)
	{
		install(idx, w2c(fmxdict->theStrList[idx]));

		// remove strings in the FMx_Dict to conserve space
		wstring *pS = const_cast<wstring*>(&(fmxdict->theStrList[idx]));
		pS->erase(pS->begin(),pS->end());

		idx++;
	}
	***/
	char * pLoc = NULL;
	string tmp;
	wstring tmpStr;
	int s = fmxdict->dictSize();
	for (int j = 0; j < s; j++)
	{
		fmxdict->retrieveStr(j, tmpStr);
		// this leaves a string c_str() in the table, not good
		//install(j, w2c(tmpStr) );
	//sjv try a cleanup	tmp = ws2s(tmpStr);
		tmp  = TStr2AStr(tmpStr);

		int k = tmp.length() + 1;
		if ( k )
		{
			pLoc = new char[ k ];
			memset(pLoc,0,k);
			strncpy(pLoc, tmp.c_str(), k-1 );
			install(j, pLoc );
			tmp.clear();
		}// else skip the empty ones
	}
	fmxdict->clearDict();// empty the source

	return 0;
}
