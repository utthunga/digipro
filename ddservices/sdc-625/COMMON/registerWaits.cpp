/*************************************************************************************************
 *
 * $Workfile: registerWaits.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is an implementation of a OS function set available on win 2000 and above
 *	As such, it cannot go into the library.  stdafx.h will have little effect.
 *		
 */ 

#if defined(__GNUC__)
#include "linux/event_objects_posix.h"
#else // !__GNUC__
#include "stdafx.h" // to allow precompiled headers
#endif // __GNUC__

//#include "DDIde.h"

#include "registerwaits.h"

#include "SDCthreadSync.h"

#include "logging.h"

using namespace std;
#pragma warning (disable : 4786) 

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <limits>


#include "shared/shared_thread.h"



// a re-entrant thread function
DWORD HandleWait(oneWait* pWait)
{
	BOOL wasDie    = FALSE;
	DWORD hr = 0;
	DWORD lstErr;
	waitStatus_t   wtStat = evt_fired;

	Thread_HandleWaitSetUp(pWait);
	// loop waiting
	do
	{
		hr = systemWaitSingleObject(pWait->eventHandle,pWait->dwTimeout);
		wasDie = pWait->Die;

		LOGIF(LOGP_START_STOP)(CLOG_LOG,"<-Current Running Registered Thread ID = 0x%X, "
			"pWait's Thread ID = 0x%X\n", 	Thread_GetCurrentThreadId(), pWait->dwThreadID);

		if ( hr == WAIT_TIMEOUT ) 
		{	
			wtStat = evt_timeout;
		}
		else if ( hr == WAIT_OBJECT_0 )
		{	wtStat = evt_fired;	
		}
		else if (hr == WAIT_ABANDONED)
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,">>>> HandleWait was abandoned.-killing self!\n");
			pWait->onceOnly = true;// force exit			
			wtStat = evt_lastcall;
		}
		else
		{			
			if ( hr == WAIT_FAILED )
			{
				lstErr = GetLastError();
				LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,">>>> HandleWait got WAIT_FAILED back from the wait."
																"(rcv'd a QUIT Windows message)\n");
				systemSleep(1000); // give it some time to re-instantiate
				continue;// loop w/o calling
			}
			else
			{
				LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,">>>> HandleWait got value 0x%04x back from the wait.\n",hr);
				continue;// loop w/o calling
			}
		}

		/* stevev 09/19/03 - always call to allow them to cleanup
		if ( ! pWait->Die )
		{*/
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"<-<-<-event in wait event. Thd:0x%04x\n",
																		pWait->dwThreadID);
		if (wasDie) 
		{
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"<-<-<-  HandleWait has a die event. (6)  ");
			if (wtStat == evt_timeout)
			{	LOGIF(LOGP_START_STOP)(CLOG_LOG," > via a TIMEOUT < "); }
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"\n");
			wtStat = evt_lastcall; // doesn't matter if it was timeout or event
		}//else don't print on a regular basis
		(*(pWait->Callback)) (pWait->Context, wtStat);
		/*}*/
	}while ( (! pWait->onceOnly) && (  (!wasDie) || (!pWait->Die)  )    );

	// delete as required 
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"> HandleWait back from final callback - "
							"clearing death flag.  Exit Thread/Task 0x%04x (0x%04x)\n", 
									Thread_GetCurrentThreadId(), pWait->dwThreadID);

	Thread_HandleWaitCleanUp(pWait);
	pWait->Die = FALSE;// handshake
	Thread_ExitThread(0);
	return 0;
}




bool Thread_RegisterWaitForSingleObject( 
	  PHANDLE phNewWaitObject,       // wait handle-----------------returned value
	  HANDLE hObject,                // handle to object------------anEvent
	  THREAD_WAITORTIMERCALLBACK Callback,  // timer callback function-----write this
	  PVOID Context,                 // callback function parameter- a ptr 2 this class
	  ULONG dwMilliseconds,          // time-out interval ----------INFINITY
	  ULONG dwFlags                  // options---------------------continuous wait
#ifdef _DEBUG
	  , char* debugWaitName
#endif
	)
{
	THREAD_ID_TYPE dwThreadID;
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> Registering a wait task.\n");
	oneWait* pWrkWait = new oneWait;
	if (pWrkWait == NULL)
	{
		return FALSE; // error
	}
              
#ifdef _DEBUG
	pWrkWait->waitName = debugWaitName;
#endif

	pWrkWait->Callback = Callback;
	pWrkWait->Context  = Context;
	pWrkWait->dwTimeout= dwMilliseconds;
	pWrkWait->eventHandle = hObject;
	pWrkWait->onceOnly = (dwFlags & WT_EXECUTEONLYONCE) ? TRUE : FALSE;
	pWrkWait->WaitFunction = HandleWait;

	pWrkWait->hThread = Thread_CreateThread(
		pWrkWait,
		dwFlags,
		&dwThreadID
	);

	pWrkWait->dwThreadID = dwThreadID;

	*phNewWaitObject = (HANDLE) pWrkWait;// return value

	int pri = (dwFlags & WT_NEW_PRIORITY) >> 4;
	if ( dwThreadID && pri )
	{
		Thread_SetThreadPriorityFromMask(pWrkWait->hThread, dwFlags);
	}

#ifdef _DEBUG
	LOGIT(CLOG_LOG,"< Registered Task 0x%04x\n",dwThreadID);
#endif
	return TRUE;
}



bool Thread_UnregisterWait( HANDLE WaitHandle, ULONG dwMilliseconds )     // wait handle
{
	oneWait* pWait = (oneWait*)WaitHandle;
	ULONG wtCnt = dwMilliseconds / 10;
	ULONG cnt = 0;  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: changed from int to ULONG>

	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"< Thread_UnregisterWait setting die flag and signalling event. (5)\n");
	pWait->Die = TRUE;
	SetEvent(pWait->eventHandle);
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"  Waiting for death of 0x%04x thread from 0x%04x thread\n",
											pWait->dwThreadID, Thread_GetCurrentThreadId());
	while ( pWait->Die && ( cnt < wtCnt ) )
	{
		systemSleep(10);
		cnt++;
	}
	
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"< Thread_UnregisterWait finished waiting for 0x%04x's death. ",
																			pWait->dwThreadID);
	if (cnt >= wtCnt) 
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"<<timed out before death. (%d loops) >>",cnt);
		if ( Thread_TerminateThread(pWait->hThread, -44) )// true on success
		{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"(TERMINATED THREAD!!!)");
		}
		else
		{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"(THREAD TERMINATION FAILED!!!)");
		}
	}
	else
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"(Success-closing thread handle)");
	}
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"\n");

	if (pWait->hThread != 0)   //// HOMZ - Memory / Resource leaks
	{
		Thread_CloseHandle(pWait->hThread);
		pWait->hThread = 0;
	}

//this should be done by the owner of the handle	CloseHandle(pWait->eventHandle);
// stevev 21feb07 - that handle is part of an event object and will be closed at event deletion.
	delete pWait; 

	return ((cnt < wtCnt)? TRUE : FALSE) ; // true == success
}

THREAD_ID_TYPE Thread_GetWaitThreadID( HANDLE WaitHandle) 
{
	oneWait* pWait = (oneWait*)WaitHandle;
	THREAD_ID_TYPE retVal = THREAD_ID_NONE;

	if ( pWait )
	{
		retVal = pWait->dwThreadID;
	}

	return retVal;
}


/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
