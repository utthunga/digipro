/*************************************************************************************************
 *
 * $Workfile: graphicInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		describes the data interface to the graphics classes
 *		9/29/4	sjv	created
 *
 * #include "graphicInfc.h"
 */

#ifndef _GRAPHIC_INFC_H
#define _GRAPHIC_INFC_H

#include "varient.h"
#include <time.h>

typedef struct pointValue_s
{
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	pointValue_s(){};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	pointValue_s(const struct pointValue_s& s):vx(s.vx),vy(s.vy){};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	pointValue_s& operator=(const struct pointValue_s& s){vx=s.vx;vy=s.vy;return *this;};
	CValueVarient vx;
	CValueVarient vy;
}
/* typedef*/pointValue_t;

typedef vector<pointValue_t> pointValueList_t;

typedef vector <pointValueList_t> listOfPointList_t;//Vibhor Added: 221204


typedef struct dataPoint_s
{
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataPoint_s():iID(0){};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataPoint_s(const struct dataPoint_s& s):iID(s.iID),vd(s.vd){};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataPoint_s& operator=(const struct dataPoint_s& s){iID=s.iID;vd=s.vd;return *this;};
	itemID_t      iID;
	CValueVarient vd;
}
/* typedef*/dataPoint_t;

typedef vector<dataPoint_t> dataPointList_t;


typedef struct dataStruct_s
{
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataStruct_s():timestamp(0){};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataStruct_s& operator=(const struct dataStruct_s& s)
	{	timestamp=s.timestamp;dpL=s.dpL;return *this;};

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	dataStruct_s(const struct dataStruct_s& s):timestamp(s.timestamp),dpL(s.dpL){};
	void clear(void){dpL.clear();timestamp = 0;};
	time_t           timestamp;
	dataPointList_t  dpL;
}
/* typedef*/dataStruct_t;

typedef vector<dataStruct_t> dataStructList_t;

/* anyone wanting to recieve published data MUST inherit from hCpubsub */
/* see ddbPublish.h */ 

#endif// _GRAPHIC_INFC_H

