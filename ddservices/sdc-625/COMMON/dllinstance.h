/*****************************************************************************
 *
 * $Workfile: dllinstance.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *****************************************************************************
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * 1/06/03	sjv	started creation
 *
 * Description:
 * This holds the classes that make up the instance data type derived from the
 *   information in the dllapi.h file.
 * These are designed to be ultra-lightweight classes (data only)
 * 
 */
 

#ifndef _DLLINSTANCE_H
#define _DLLINSTANCE_H

#include "dllapi.h"


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* ITEMS *=*=*=*=*=**=*=*=*=*=*=*=*=*/

//------------------------- Variable --------------------------

/*======================= BASE CLASSES ====================*/
class aCvarType //: public aCitemBase   //the variable base class
{
	aCvarTypeDescriptor mtErrorReturn; // has type & size
	aCitemBase* pOrigItemBase;

public:
	aCvarType(aCitemBase* pFromItemBase = NULL) : pOrigItemBase(pFromItemBase) 
	{ 
		if (pOrigItemBase != NULL) 
		{
			sTypeName = pOrigItemBase->itemType.getTypeStr();
		}
	};
	//int      actualTypesType; // required to cast base class
	//int      actualTypeSize;
	aCconditional/*<aCvarTypeDescriptor>*/ condTypeDescriptor; // not in attr list
	string   sTypeName;

	// call as global  aCvarType::newInstance(ptr2aCitemBase);
	aCvarType* newInstance(aCitemBase* pFromItemBase);// returns correct instance type

	// very useful
	unsigned int getType(void){ return pOrigItemBase->itemType.getType(); }
	/*
	aCvarTypeDescriptor& getType() // since it is not really conditional
	{
		if (condTypeDescriptor.priExprType != eT_Direct)
		{
			cerr << "ERROR: a aCvarType with a non-direct descriptor type." << endl;
		}
		if (condTypeDescriptor.destElements.size() > 0 )
		{
		return *(condTypeDescriptor.destElements.front().pPayload);
		}
		else
		{
			clog << "aCvarType:getType 'condTypeDescriptor' has no destinations!"<<endl;			
			return mtErrorReturn;
		}
	};
	*/
};

/*============= INTERMEDIATE BASE CLASSES ====================*/
class aCvarArithType  : public aCvarType
{};
class aCvarStringType : public aCvarType
{
public:
	char* theValue;

	aCvarStringType(){theValue = NULL;};
	~aCvarStringType(){if(theValue != NULL){delete theValue;theValue = NULL;}};
};
class aCvarEnumType   : public aCvarType
{
public:
	AcondEnumList_t condEnumDescriptor;
};

/*========================= CLASSES ==========================*/
class aCvTinteger		: public aCvarArithType	//#define INTEGER 			2
{
public:
	int theValue;
};
class aCvTunsigned		: public aCvarArithType	//#define UNSIGNED 			3
{
public:
	unsigned int theValue;
};
class aCvTfloat			: public aCvarArithType	//#define FLOATG_PT			4
{
public:
	float theValue;
};
class aCvTdouble		: public aCvarArithType	//#define DOUBLE			5
{
public:
	double theValue;
};
class aCvTenumerated	: public aCvarEnumType	//#define ENUMERATED 		6
{
public:
	unsigned int theValue;
};
class aCvTbitenumerated : public aCvarEnumType	//#define BIT_ENUMERATED 	7
{
public:
	unsigned int theValue;
};
class aCvTindex			: public aCvarType		//#define INDEX 			8
{
public:
	int theValue;
};
class aCvTascii			: public aCvarStringType//#define ASCII 			9
{
};
class aCvTpackedascii	: public aCvarStringType//#define PACKED_ASCII 		10
{
};
class aCvTpassword		: public aCvarStringType//#define PASSWORD 			11
{
};
class aCvTbitstring		: public aCvarStringType//#define BITSTRING 		12
{
};
class aCvThartdate		: public aCvarType		//#define HART_DATE_FORMAT 	13
{
public:
	int theValue;
};
class aCvTtime			: public aCvarType		//#define TIME 				14
{
public:
	unsigned int theValue;
};
class aCvTdateandtime	: public aCvarType		//#define DATE_AND_TIME 	15
{
public:
	unsigned int theDateValue;
	unsigned int theTimeValue;
};
class aCvTduration		: public aCvarType		//#define DURATION 			16
{
public:
	unsigned int theValue;
};
class aCvTeuc			: public aCvarType		//#define EUC				17
{
public:
	int theValue;//????????????????
};
class aCvToctetstring   : public aCvarStringType//#define OCTETSTRING		18
{
};
class aCvTvisiblestring : public aCvarStringType//#define VISIBLESTRING		19
{
};
class aCvTtimevalue		: public aCvarType		//#define TIME_VALUE		20
{
public:
	unsigned int theValue;
};
class aCvTboolean		: public aCvarType		//#define BOOLEAN_T			21
{
public:
	int theValue;
};

/* * * * * not yet * * * * * *
//-------------------- Command -----------------------------
class aCcommand : public aCitemBase
{// no contents,  base class has list of attributes
public:
};

//---------------------- Menu ------------------------------
class aCmenu : public aCitemBase
{// no contents, base class has list of attributes
public:
};

//------------------ Edit Display --------------------------
class aCeditDisplay : public aCitemBase
{// no content - base has attribute list
public:
};

/ /------------------ Method,				// 5
//------------------ Refresh,/ *relation* /	// 6
//------------------ Unit,/ *relation* /		// 7
//------------------ WaO,					// 8
//------------------ ItemArray,				// 9
//------------------ Collection,			//10
//------------------ Block,					//12
//------------------ Program,				//13
//------------------ Record,				//14
//------------------ Array,					//15
//------------------ VariableList,			//16
//------------------ ResponseCodes,			//17
//------------------ Domain,				//18
//------------------ Member,				//19

* * * * * * * * * * * * * * * */

/*=*=*=*=*=*=*=*=*=*=*=*=*=* END OF ITEMS =*=*=*=**=*=*=*=*=*=*=*=*/


#endif//_DLLINSTANCE_H
