/*************************************************************************************************
 *
 * $Workfile: HARTsupport.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		structures and enums to support the HART protocol
 *	
 * #include "HARTsupport.h"
 */

#ifndef _HARTSUPPORT_H
#define _HARTSUPPORT_H

#include "ddbQueue.h"

/*** HART Standard setting::***/

// strings or numbers //
#define DEFAULT_HART_MFG	0xf9
#define DEFAULT_HART_DT 	0x82
#define DEFAULT_HART_EDT 	0xf982
//  HART  5 //
#define DEFAULT_MFG_05		0xf9
#define DEFAULT_DEVTYPE_05	0x82
#define DEFAULT_DEV_REV_05	0x05
#define DEFAULT_DD__REV_05	0x01
//  HART  6 //
#define DEFAULT_MFG_06		0xf9
#define DEFAULT_DEVTYPE_06	0x82
#define DEFAULT_DEV_REV_06	0x06
#define DEFAULT_DD__REV_06	0x01
//  HART  7 //
#define DEFAULT_MFG_07		0x00f9
#define DEFAULT_DEVTYPE_07	0xf982	/* was 0x0082*/
#define DEFAULT_DEV_REV_07	0x07
#define DEFAULT_DD__REV_07	0x01
/*======= FALL_BACK_LOCATION =========*/
// strings or numbers //
#define DEFAULT_ROSE_MFG	    0x26
#define DEFAULT_ROSE_DT 	    0x80
#define DEFAULT_ROSE_EDT 	  0x2680
//  HART  5 //
#define RM_DEFAULT_MFG_05		0x26
#define RM_DEFAULT_DEVTYPE_05	0x80
#define RM_DEFAULT_DEV_REV_05	0x05
#define RM_DEFAULT_DD__REV_05	0x01
//  HART  6 //
#define RM_DEFAULT_MFG_06		0x26
#define RM_DEFAULT_DEVTYPE_06	0x80
#define RM_DEFAULT_DEV_REV_06	0x06
#define RM_DEFAULT_DD__REV_06	0x01
//  HART  7 //
#define RM_DEFAULT_MFG_07		0x0026
#define RM_DEFAULT_DEVTYPE_07	0x2680	/* was 0x0080 */
#define RM_DEFAULT_DEV_REV_07	0x07
#define RM_DEFAULT_DD__REV_07	0x01


/* moved from ddbCmdDispatch.h */
#define REASONABLE_TIME 5000 /* 5 seconds */
#define FOREVER_TIME    (hCevent::FOREVER)
//#define REASONABLE_TIME   FOREVER_TIME


/* spec 307: ERROR: 1-7,9-13,15,16-23,28,29,32-64, 65 -95
           WARNING:    8,   14,    24-27, 30,31, 96-111,112-127
**/
// RESPONSE CODES - Hi Bit is CLEAR
typedef enum response5Code_e
{
	rc_NoError,		// 0		   Error
	rc_Undefined,	// 1		   Error
	rc_InvalidSel,				// Error
	rc_Param2Big,				// Error
	rc_Param2Small,				// Error
	rc_TooFewBytes,	// 5		   Error
	rc_XmitrCmdErr,				// Error
	rc_InWrtProtect,			// Error
	rc_8_UpdateErr,				//Warning
	rc_9_ValTooHi,				// Error
	rc_10ValTooLo,	//10		   Error
	rc_11InvalidMode,			// Error
	rc_12InvalidUnit,			// Error
	rc_13BothRangeErr,			// Error
	rc_14Span2Small,			//Warning
	rc_15NotSpecd,	//15		   Error
	rc_Restricted,	//0x10		   Error
	rc_Busy = 32,	//0x20		   Error
	rc_DR_Initiate,	//0x21 - 33	   Error
	rc_DR_Running,	//0x22 - 34	   Error
	rc_DR_Dead,		//0x23 - 35	   Error
	rc_DR_Conflict,	//0x24 - 36	   Error
	rc_CmdNotImpl=64//0x40		   Error
}/*typedef*/ response5Code_t;

// COMM STATUS - Hi Bit is SET, use | to tie'em together
#define DEV_PARITYERR		0xC0	// hi bit and bit 6
#define DEV_OVERUNERR		0xA0	// hi bit and bit 5
#define DEV_FRAMINGERR		0x90	// hi bit and bit 4
#define DEV_CHKSUMERR		0x88	// hi bit and bit 3
#define DEV_BIT2RSVD		0x84	// hi bit and bit 2 - reserved
#define DEV_RXBUFFOVER		0x82	// hi bit and bit 1
#define DEV_BIT0RSVD		0x81	// hi bit and bit 0 - reserved

// HOST_COMM STATUS - Hi Bit is CLEAR, use | to tie'em together
#define HOST_INTERNALERR	0x80	// hi bit set - see 2nd byte for reason
#define HOST_PARITYERR		0x40	// bit 6
#define HOST_OVERUNERR		0x20	// bit 5
#define HOST_FRAMINGERR		0x10	// bit 4
#define HOST_CHKSUMERR		0x08	// bit 3
#define HOST_HARTFRAMEERR	0x04	// bit 2
#define HOST_RXBUFFOVER		0x02	// bit 1
#define HOST_NORESPONSE		0x01	// bit 0

// DEVICE STATUS - Second byte after a response code <non-comm status>
#define DS_DEVMALFUNCTION	0x80	// bit 7
#define DS_CONFIGCHANGED	0x40	// bit 6
#define DS_COLDSTART		0x20	// bit 5
#define DS_MORESTATUSAVAIL	0x10	// bit 4
#define DS_OUTCURRENTFIXED	0x08	// bit 3
#define DS_ANALGOUTSATURATE	0x04	// bit 2
#define DS_SNSRVAROUTOFRNG	0x02	// bit 1
#define DS_SNSRPRIVAROUTRNG	0x01	// bit 0
#define DS_NO_STATUS		0x00


// Refer requirements document for the details
#define DEVICE_RESPONSECODE     150
#define DEVICE_COMM48_STATUS	151  /* actually Device Status */
#define DEVICE_COMM_STATUS		152
#define DEVICE_MFG_ID			153
#define DEVICE_DEVICE_TYPE		154
#define DEVICE_XMTR_REVISION	157
#define DEVICE_HART_REV_LEVEL	156	/* 5 till six arrives */
#define DEVICE_REQST_PREAMBLES	155
#define DEVICE_SFTWR_REVISION	158
#define DEVICE_HDWR_REVISION	159
#define DEVICE_POLL_ADDRESS		162
#define DEVICE_TAG				163
#define DEVICE_DISTRIBUTOR		168
#define DEVICE_ID				210
#define DEVICE_PROCESS_VARIABLES 7003
#define DEVICE_FACE_PLATE		7024
/* VMKP added on 310304 */
#define DEVICE_BURST_MODE		0xFFF 
#define DEVICE_BURST_OPTION		0xFFD
/* VMKP added on 310304 */
#define MORE_STATUS_AVAIL       0x10	/* bit 4 set */


typedef enum frameType_e
{
	ft_UNK = 0, // 0x00 Unknown
	ft_BACK,	// 0x01 SlaveBurst->Master
	ft_STX,		// 0x02 Master->Slave
	ft_UDEF3,	// 0x03  Undefined
	ft_UDEF4,	// 0x04
	ft_UDEF5,	// 0x05
	ft_ACK,		// 0x06 Slave->Master
	ft_MAX		// 0x07
}
/*typedef*/ frameType_t;

typedef enum   phyType_e
{
	pt_UNK,	//Unknown
	pt_UDEF1,
	pt_UDEF2,
	pt_UDEF3
}
/*typedef*/ phyType_t;

typedef struct shortFrm_s
{
	BYTE	sfPollAddr;
	BYTE	twoBits;
}
/*typedef*/ shortFrm_t;

typedef struct long_Frm_s
{
	BYTE	mfgNumber;	// 6 bits
	BYTE	devType;	// 8 bits
	BYTE	devID[3];	// 3 bytes/24 bits
	void clear(void){mfgNumber=0;devType=0;devID[0]=devID[1]=devID[2]=0;};
}
/*typedef*/ long_Frm_t;

typedef union  address_u
{
	shortFrm_t	shortAddr;
	long_Frm_t	long_Addr;
}
/*typedef*/ address_t;

typedef struct cmdInfo_s
{
	bool			isLongFrame;
	unsigned char   expansionCnt;
	phyType_t		physicalType;
	frameType_t		frametype;
	__int64         transactionID; // simm/serial has to passeddown  - stevev 31aug09

	bool			isPri;
	bool			isBurstMsg;
	address_t		theAddress;
		
	BYTE			expByte[3];
	unsigned int    command;   // very long to support DD IDs (initial Xmtr-DD implementation)
	BYTE			byteCnt;
	BYTE			RespCd;
	BYTE			Devstatus;
	bool			crcOK;
	bool			discardMsg; // used during shutdown
	bool			clrNexit;
	void*           pOtherData;// someone else owns this pointer (and is responsible 4 deletion)
	void clear(void){
		isLongFrame = false;		expansionCnt = 0;
		physicalType= pt_UNK;		frametype = ft_UNK;
		isPri       = false;		isBurstMsg= false;  discardMsg=clrNexit=false;
		theAddress.long_Addr.clear();
		expByte[0]=expByte[1]=expByte[2]=0; command = 0;
		byteCnt=0;		RespCd=0;	Devstatus=0;
		crcOK=false;	pOtherData = NULL;            };
	struct cmdInfo_s& operator=(struct cmdInfo_s& src) {
		isLongFrame = src.isLongFrame;	expansionCnt = src.expansionCnt;
		physicalType= src.physicalType;	frametype = src.frametype;
		isPri       = src.isPri;		isBurstMsg= src.isBurstMsg;
		discardMsg=src.discardMsg;      clrNexit= src.clrNexit;
		memcpy(&theAddress, &(src.theAddress), sizeof(address_t));
		expByte[0]=src.expByte[0];expByte[1]=src.expByte[1];expByte[2]=src.expByte[2]; 
		command = src.command;			byteCnt=src.byteCnt;	
		RespCd=src.RespCd;				Devstatus=src.Devstatus;
		crcOK=src.crcOK;				pOtherData = src.pOtherData;            
			return *((struct cmdInfo_s*)(&isLongFrame));};
}
/*typedef*/ cmdInfo_t;

/* stevev 2sep05 - due to com apartments and host error actions, the identify function
   must be incorporated into the comminfc thread task.
   to do this, the packet must carry more information than merely the data packet.
   hence, the threadControl.
 */
class hThreadCntl
{
public:
	enum trdAction_e
	{
		thd_Init,	// 0 set it up
		thd_Send,	// 1 normal activity
		thd_RawSnd,	// just send and return resonse in same pkts(no msgcycle)
		thd_Connect,// do connect operation
		thd_DisCon, // disconnect
		thd_Stop	// shutdown
	}
	        threadAction;

	void*	threadLPARAM;// is hCevent when thd_RawSnd
	bool    fromMethod;  // communication is totally different when sending from a method

	hThreadCntl():threadAction(thd_Send),threadLPARAM(NULL),fromMethod(false) {};
	void clear(void){threadAction = thd_Send;threadLPARAM = NULL;fromMethod=false;};
};

class hPkt
{
public:
	long channelID; // pipe instance number or comm specific number (ie URL to device)
	int  timeout;   // a way to pass the time around, even if the modem server doesn't support it
	__int64  transactionID; // how to match the transmit to receive packet - stevev 15may09
	int  cmdNum;
	BYTE dataCnt; 
	BYTE theData[256];
/* VMKP added on 020404 */
	BYTE byIsBurstMode;
	BYTE byCmdNum;
/* VMKP added on 020404 */
	hThreadCntl m_thrdCntl;	// stevev 2sep05 

	void clear(void){channelID = -1; timeout=-1; cmdNum = -1;dataCnt = 0;theData[0] = '\0';
	byIsBurstMode = 0; byCmdNum = 0;transactionID = 0;m_thrdCntl.clear();};
	hPkt(){ clear(); };
	unsigned char checkSum(void) 
				{ unsigned char c = 0; for(int i =0; i < dataCnt; c ^= theData[i++] );return c;}
	void Log(void){LOGIT(CLOG_LOG,"Cmd: %d w/%02d |",cmdNum, dataCnt); if (dataCnt < 250)
		for (int y = 0; y<dataCnt;y++)LOGIT(CLOG_LOG,"%02x ",theData[y]);LOGIT(CLOG_LOG,"|\n");}
};

class hCPktQueue : public hCqueue<hPkt>	
{          
public://constructor
	// 09jan12 - hCPktQueue(int gb = 1):hCqueue<hPkt>(gb) {};
	hCPktQueue():hCqueue<hPkt>() {};
	void InitItem (hPkt * pItem){ if (pItem != NULL) pItem->clear(); };
	void EmptyItem(hPkt * pItem){ if (pItem != NULL) pItem->clear(); };
};


#endif // _HARTSUPPORT_H

/*************************************************************************************************
 *
 *   $History: HARTsupport.h $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/Common
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/Common
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
