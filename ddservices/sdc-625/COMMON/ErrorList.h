/*************************************************************************************************
 *
 * $Workfile: ErrorList.h $
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the central list of strings used in warnings and errors
 *
 *		
 * #include "ErrorList.h"
 */

#ifndef _ERRORLIST_H
#define _ERRORLIST_H

#ifndef  _T
//#define _T(s) L##s
#if !defined(__GNUC__)
#include "TChar.h"  // stevev 17aug10
#endif // __GNUC__
#endif 

#define sz( a )		#a

/*
 *		ddbGeneral.h holds 5000, 6000, 7000, 8000, 10000, 11000, 12000
 *		builtins use 1400 to 2700
 *		tokenizer uses 0 to 700
 */

#define WARNSTRT		3000	/* 3000 - 3999 are warnings */

#define ERR_NO_DEBUG_INFO	( WARNSTRT + 0 )
#define ERR_NO_DISP_ATTR	( WARNSTRT + 1 )
#define ERR_NO_EDIT_ATTR	( WARNSTRT + 2 )
#define ERR_MISNG_MAX		( WARNSTRT + 3 )
#define ERR_MISNG_MIN		( WARNSTRT + 4 )

#define ERR_STRT		4000	/* 4000 - 4999 are errors   */
#define ERR_IN_PROGRAM		( ERR_STRT + 0 )
#define ERR_MINMAX_MISMATCH ( ERR_STRT + 1 )


#define  ERR_DESC_LIST_00	\
   ERR_NO_DEBUG_INFO ,/*_T("WARN ")  */ _T(": debug information is not accessable.\n")		\
,  ERR_IN_PROGRAM    ,/*_T("ERROR ") */ _T(": program error %s line %d.\n")	\
,  ERR_NO_DISP_ATTR  ,/*_T("WARN ")  */ _T(": %s (0x%04x) Edit-Display has no display-items attribute\n") \
,  ERR_NO_EDIT_ATTR  ,/*_T("WARN ")  */ _T(": %s (0x%04x) Edit-Display has no edit-items attribute\n")\
,  ERR_MISNG_MAX     ,/*_T("WARN ")  */ _T(": %s has a numbered [%d] MIN_VALUE without a matching MAX.\n" )\
,  ERR_MISNG_MIN	 ,/*_T("WARN ")  */ _T(": %s has a numbered [%d] MAX_VALUE without a matching MIN.\n" )\
,  ERR_MINMAX_MISMATCH,/*_T("ERROR ")*/ _T(": %s has a mismatched MIN-MAX VALUE set MIN:%d, MAX:%d values.\n" )


#define ERR_DESC_LIST_END	,0,NULL	/* designation of end of list */

#endif /*_ERRORLIST_H*/
