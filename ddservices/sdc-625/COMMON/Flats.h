/**
 *		Copyright 1995 - HART Communication Foundation
 *		All rights reserved.
 */

/*
 *
 *	@(#)flats.h	40.1  40  07 Jul 1998
 * part of sdc
 */

/* Note: The Tokenizer needs data values in the FLATs, Others don't */
/*!!!!!   Take care of moderating the contents of this file		!!!!!*/

#ifndef FLATS_H
#define FLATS_H
  
#ifdef TOK
 #ifdef ATTRS_H
 #endif 
  #include "attrs.h"
 #else
 #include "Attributes.h"
#endif


/*
 *	FLAT structures
 */

typedef struct FLAT_MASKS
{
	unsigned long           bin_exists;		/* There is binary for this attribute */
	unsigned long           bin_hooked;		/* Binary is attached for this attribute */
	unsigned long           attr_avail;		/* The attribute has been evaluated	 */
	unsigned long           dynamic;		/* The attribute is dynamic */
}FLAT_MASKS;


/*
 * VARIABLE item
 */

typedef struct VAR_ACTIONS_DEPBIN
{
	DEPBIN         *db_pre_edit_act;
	DEPBIN         *db_post_edit_act;
	DEPBIN         *db_pre_read_act;
	DEPBIN         *db_post_read_act;
	DEPBIN         *db_pre_write_act;
	DEPBIN         *db_post_write_act;
	DEPBIN         *db_refresh_act;
#ifdef XMTR
	DEPBIN         *db_post_rqst_act;
	DEPBIN         *db_post_user_act;
#endif
}VAR_ACTIONS_DEPBIN;

typedef struct FLAT_VAR_ACTIONS
{
#ifdef TOK
	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;
	ITEM_ID_LIST    pre_read_act;
	ITEM_ID_LIST    post_read_act;
	ITEM_ID_LIST    pre_write_act;
	ITEM_ID_LIST    post_write_act;
	ITEM_ID_LIST    refresh_act;
#ifdef XMTR
	ITEM_ID_LIST    post_rqst_act;
	ITEM_ID_LIST    post_user_act;
#endif
#endif
	VAR_ACTIONS_DEPBIN *depbin;
}FLAT_VAR_ACTIONS;

typedef struct VAR_MISC_DEPBIN
{
	DEPBIN         *db_unit;
	DEPBIN         *db_height;	//db_read_time_out;
	DEPBIN         *db_width;	//db_write_time_out;
	DEPBIN         *db_min_val;
	DEPBIN         *db_max_val;
	DEPBIN         *db_scale;
	DEPBIN         *db_valid;
	DEPBIN         *db_debug_info;
	DEPBIN         *db_time_format;
	DEPBIN         *db_time_scale;
}VAR_MISC_DEPBIN;

typedef struct FLAT_VAR_MISC
{
#ifdef TOK
	STRING          unit;			/**/
	unsigned long   height; //  was  read_time_out;		/**/
	unsigned long   width;  //  was  write_time_out;	/**/
	RANGE_DATA_LIST min_val;		/**/
	RANGE_DATA_LIST max_val;		/**/
	EXPR            scale;			/* scale factor */
	unsigned long   valid;			/* validity */
#endif
	VAR_MISC_DEPBIN *depbin;
}FLAT_VAR_MISC;

typedef struct VAR_DEPBIN
{
	DEPBIN         *db_class;
	DEPBIN         *db_handling;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_type_size;
	DEPBIN         *db_display;
	DEPBIN         *db_edit;
	DEPBIN         *db_enums;
	DEPBIN         *db_index_item_array;
	DEPBIN         *db_resp_codes;
	DEPBIN         *db_default_value;
}VAR_DEPBIN;

typedef struct FLAT_VAR{
	ITEM_ID         id;	               /* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	unsigned long   class_attr;
	unsigned long   handling;
	STRING          help;
	STRING          label;
	TYPE_SIZE       type_size;
	STRING          display;
	STRING          edit;
	ENUM_VALUE_LIST enums;
	ITEM_ID         index_item_array;
	ITEM_ID         resp_codes;
	EXPR            default_value;
#endif
	VAR_DEPBIN     *depbin;
	FLAT_VAR_ACTIONS *actions;
	FLAT_VAR_MISC  *misc;
}FLAT_VAR;


/*
 * PROGRAM item
 */

typedef struct PROGRAM_DEPBIN{
	DEPBIN         *db_args;
	DEPBIN         *db_resp_codes;
}PROGRAM_DEPBIN;

typedef struct FLAT_PROGRAM{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	DATA_ITEM_LIST  args;
	ITEM_ID         resp_codes;
	PROGRAM_DEPBIN *depbin;
}FLAT_PROGRAM;

/*
 * MENU item
 */

typedef struct MENU_DEPBIN{
	DEPBIN         *db_label;
	DEPBIN         *db_items;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_style;/* TODO: add to clean_flat() */
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}MENU_DEPBIN;

typedef struct FLAT_MENU{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	STRING          label;
	STRING          help;
	unsigned long   valid;			/* validity */
	unsigned long   style;			/* type of menu */
	MENU_ITEM_LIST  items;
#endif
	MENU_DEPBIN    *depbin;
}FLAT_MENU;



/*
 * EDIT_DISPLAY item
 */

typedef struct EDIT_DISPLAY_DEPBIN{
	DEPBIN         *db_disp_items;
	DEPBIN         *db_edit_items;
	DEPBIN         *db_label;
	DEPBIN         *db_pre_edit_act;
	DEPBIN         *db_post_edit_act;
	DEPBIN		   *db_help;
	DEPBIN		   *db_valid;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}EDIT_DISPLAY_DEPBIN;

typedef struct FLAT_EDIT_DISPLAY{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	OP_REF_TRAIL_LIST disp_items;
	OP_REF_TRAIL_LIST edit_items;
	STRING          label;
	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;
#endif
	EDIT_DISPLAY_DEPBIN *depbin;
}FLAT_EDIT_DISPLAY;



/*
 * METHOD item
 */


typedef struct METHOD_DEPBIN{
	DEPBIN         *db_class;
	DEPBIN         *db_def;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_valid;
	DEPBIN         *db_scope;
	DEPBIN         *db_type;
	DEPBIN         *db_params;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}METHOD_DEPBIN;

typedef struct FLAT_METHOD{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	unsigned long           class_attr ;  
	DEFINITION      def;
	STRING          help;
	STRING          label;	 
	unsigned long           valid;
	unsigned long			scope;
#endif
	METHOD_DEPBIN  *depbin;
}FLAT_METHOD;

/*
 * REFRESH item
 */

typedef struct REFRESH_DEPBIN{
	DEPBIN         *db_items;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}REFRESH_DEPBIN;

typedef struct FLAT_REFRESH{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	REFRESH_RELATION items;
#endif
	REFRESH_DEPBIN *depbin;
}FLAT_REFRESH;



/*
 * UNIT item
 */

typedef struct UNIT_DEPBIN{
	DEPBIN         *db_items;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}UNIT_DEPBIN;

typedef struct FLAT_UNIT{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	UNIT_RELATION   items;
#endif
	UNIT_DEPBIN    *depbin;
} FLAT_UNIT;


/*
 * WRITE AS ONE item
 */

typedef struct WAO_DEPBIN{
	DEPBIN         *db_items;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}WAO_DEPBIN;

typedef struct FLAT_WAO{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	OP_REF_TRAIL_LIST items;
#endif
	WAO_DEPBIN     *depbin;
}FLAT_WAO;


/*
 * ITEM_ARRAY item
 */

typedef struct ITEM_ARRAY_DEPBIN{
	DEPBIN         *db_elements;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN		   *db_valid;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
} ITEM_ARRAY_DEPBIN;

typedef struct FLAT_ITEM_ARRAY{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
#ifdef TOK
	ITEM_ARRAY_ELEMENT_LIST elements;
	STRING          help;
	STRING          label;
#endif
	ITEM_ARRAY_DEPBIN *depbin;
}FLAT_ITEM_ARRAY;



/*
 * ARRAY item
 */

typedef struct ARRAY_DEPBIN{
	DEPBIN         *db_num_of_elements;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_type;
	DEPBIN         *db_valid;
	DEPBIN         *db_resp_codes;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}ARRAY_DEPBIN;


typedef struct FLAT_ARRAY{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	unsigned long   num_of_elements;
	STRING          help;
	STRING          label;
	ITEM_ID         type;
	unsigned long           validity;
	ITEM_ID         resp_codes;
#endif
	ARRAY_DEPBIN   *depbin;
}FLAT_ARRAY;



/*
 * COLLECTION item
 */

typedef struct COLLECTION_DEPBIN{
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_label;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}COLLECTION_DEPBIN;

typedef struct FLAT_COLLECTION{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	ITEM_TYPE		subtype;
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
#endif
	COLLECTION_DEPBIN *depbin;
}FLAT_COLLECTION;


/*
 * RECORD item
 */

typedef struct RECORD_DEPBIN{
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_resp_codes;
}RECORD_DEPBIN;

typedef struct FLAT_RECORD{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
#endif
	RECORD_DEPBIN  *depbin;
}FLAT_RECORD;


/*
 * VARIABLE	LIST item
 */

typedef struct VAR_LIST_DEPBIN{
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_resp_codes;
}VAR_LIST_DEPBIN;

typedef struct FLAT_VAR_LIST{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
#endif
	VAR_LIST_DEPBIN *depbin;
}FLAT_VAR_LIST;


/*
 * BLOCK item
 */

typedef struct BLOCK_DEPBIN{
	DEPBIN         *db_characteristic;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_param;
	DEPBIN         *db_param_list;
	DEPBIN         *db_item_array;
	DEPBIN         *db_collect;
	DEPBIN         *db_menu;
	DEPBIN         *db_edit_disp;
	DEPBIN         *db_method;
	DEPBIN         *db_unit;
	DEPBIN         *db_refresh;
	DEPBIN         *db_wao;
}BLOCK_DEPBIN;

/*Vibhor 131003: We have to parse the FLAT_BLOCK also
 so I am moidifying the structure to match with other 
 FLAT structures 
 We will allocate and parse the HART Block in the very same way as 
 all the other Items 
*/
typedef struct FLAT_BLOCK{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	ITEM_ID         characteristic;
	STRING          help;
	STRING          label;
	MEMBER_LIST     param;
	MEMBER_LIST     param_list;
	ITEM_ID_LIST    item_array;
	ITEM_ID_LIST    collect;
	ITEM_ID_LIST    menu;
	ITEM_ID_LIST    edit_disp;
	ITEM_ID_LIST    method;
	ITEM_ID_LIST    unit;
	ITEM_ID_LIST    refresh;
	ITEM_ID_LIST    wao;
#endif
	BLOCK_DEPBIN   *depbin;
}FLAT_BLOCK;


/*
 * RESPONSE CODE item
 */

typedef struct RESP_CODE_DEPBIN{
	DEPBIN         *db_member;
}RESP_CODE_DEPBIN;

typedef struct FLAT_RESP_CODE{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	RESPONSE_CODE_LIST member;
#endif
	RESP_CODE_DEPBIN *depbin;
}FLAT_RESP_CODE;


/*
 * DOMAIN item
 */

typedef struct DOMAIN_DEPBIN{
	DEPBIN         *db_handling;
	DEPBIN         *db_resp_codes;
}DOMAIN_DEPBIN;

typedef struct FLAT_DOMAIN{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	unsigned long           handling;
	ITEM_ID         resp_codes;
#endif
	DOMAIN_DEPBIN  *depbin;
}FLAT_DOMAIN;




/*
 * COMMAND item
 */

typedef struct COMMAND_DEPBIN{
	DEPBIN         *db_number;
	DEPBIN         *db_oper;
	DEPBIN         *db_trans;
	DEPBIN         *db_resp_codes;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}COMMAND_DEPBIN;

typedef struct FLAT_COMMAND{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	unsigned long           number;
	unsigned long           oper;
	TRANSACTION_LIST trans;
	RESPONSE_CODE_LIST resp_codes;
#endif
	COMMAND_DEPBIN *depbin;
}FLAT_COMMAND;

/*************************************************************************
 *  EXTENDED ITEM TYPES
 *************************************************************************
 */
/*
 * FILE item
 */

typedef struct FILE_DEPBIN
{
	DEPBIN         *db_members;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}FILE_DEPBIN;

typedef struct FLAT_FILE
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
	FILE_DEPBIN   *depbin;
}FLAT_FILE;

	
/*
 * CHART item
 */
typedef struct CHART_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_height;
	DEPBIN         *db_width;
	DEPBIN         *db_type;
	DEPBIN         *db_length;
	DEPBIN         *db_cytime;
	DEPBIN         *db_members;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}CHART_DEPBIN;

typedef struct FLAT_CHART
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
#ifdef TOK
	STRING          help;
	STRING          label;
	// other stuff TODO - define this correctly
	unsigned long           validity;
#endif
	CHART_DEPBIN   *depbin;
}FLAT_CHART;

/*
 * GRAPH item
 */
typedef struct GRAPH_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_height;
	DEPBIN         *db_width;
	DEPBIN         *db_x_axis;
	DEPBIN         *db_members;
	DEPBIN         *db_cytime;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}GRAPH_DEPBIN;

typedef struct FLAT_GRAPH
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// other stuff TODO - define this correctly
	GRAPH_DEPBIN   *depbin;
}FLAT_GRAPH;

/*
 * AXIS item
 */
typedef struct AXIS_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_minval;
	DEPBIN         *db_maxval;
	DEPBIN         *db_scaling;
	DEPBIN         *db_unit;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}AXIS_DEPBIN;

typedef struct FLAT_AXIS
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// other stuff	TODO - define this correctly
	AXIS_DEPBIN   *depbin;
}FLAT_AXIS;

/*
 * WAVEFORM item
 */
typedef struct WAVEFORM_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_handling;
	DEPBIN         *db_emphasis;
	DEPBIN         *db_linetype;
	DEPBIN         *db_linecolor;
	DEPBIN         *db_y_axis;
	DEPBIN         *db_x_keypts;
	DEPBIN         *db_y_keypts;
	DEPBIN         *db_type;
	DEPBIN         *db_x_values;
	DEPBIN         *db_y_values;
	DEPBIN         *db_x_initial;
	DEPBIN         *db_x_incr;
	DEPBIN         *db_pt_count;
	DEPBIN         *db_init_acts;
	DEPBIN         *db_rfrsh_acts;
	DEPBIN         *db_exit_acts;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
	DEPBIN         *db_valid;		/* 23jan07 sjv - spec change */
}WAVEFORM_DEPBIN;

typedef struct FLAT_WAVEFORM
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// TODO define the data instances correctly
	WAVEFORM_DEPBIN   *depbin;
}FLAT_WAVEFORM;

/*
 * SOURCE item
 */
typedef struct SOURCE_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_emphasis;
	DEPBIN         *db_linetype;
	DEPBIN         *db_linecolor;
	DEPBIN         *db_y_axis;
	DEPBIN         *db_init_acts;
	DEPBIN         *db_rfrsh_acts;
	DEPBIN         *db_exit_acts;
	DEPBIN         *db_members;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}SOURCE_DEPBIN;

typedef struct FLAT_SOURCE
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// TODO define the data instances correctly
	SOURCE_DEPBIN   *depbin;
}FLAT_SOURCE;

/*
 * LIST item
 */
typedef struct LIST_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_type;
	DEPBIN         *db_count;
	DEPBIN         *db_capacity;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}LIST_DEPBIN;

typedef struct FLAT_LIST
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// TODO define the data instances correctly
	LIST_DEPBIN   *depbin;
}FLAT_LIST;


/*
 * GRID item
 */
typedef struct GRID_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_height;
	DEPBIN         *db_width;
	DEPBIN         *db_orient;
	DEPBIN         *db_handling;
	DEPBIN         *db_members;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}GRID_DEPBIN;

typedef struct FLAT_GRID
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// other stuff TODO - define this correctly
	GRID_DEPBIN   *depbin;
}FLAT_GRID;


/*
 * IMAGE item
 */
typedef struct IMAGE_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_valid;
	DEPBIN         *db_link;
	DEPBIN         *db_path;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}IMAGE_DEPBIN;

typedef struct FLAT_IMAGE
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// TODO define the data instances correctly
	IMAGE_DEPBIN   *depbin;
}FLAT_IMAGE;

/*
 * BLOB item
 */
typedef struct BLOB_DEPBIN
{
	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_handling;
	DEPBIN         *db_identifier;
	DEPBIN		   *db_debug_info;/* TODO: add to clean_flat() */
}BLOB_DEPBIN;

typedef struct FLAT_BLOB
{
	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	// TODO define the data instances correctly
	IMAGE_DEPBIN   *depbin;
}FLAT_BLOB;
#ifdef TOK
#endif
#endif /* FLATS_H */
