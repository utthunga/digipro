/*************************************************************************************************
 *
 * $Workfile: DDselectSplitter.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include ".h"
 */
#if !defined(AFX_DDSELECTSPLITTER_H__93C95992_2CEF_4BB4_9567_7862E5415ED1__INCLUDED_)
#define AFX_DDSELECTSPLITTER_H__93C95992_2CEF_4BB4_9567_7862E5415ED1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDDselectSplitter dialog

class CDDselectSplitter : public CDialog
{
// Construction
public:
	CDDselectSplitter(UINT idd, CWnd* pParent = NULL);   // standard constructor 
	~CDDselectSplitter();

	enum {
		TOP,
		BOTTOM,
		SPLITTERWIDTH = 2	// Half width of the splitter
	};

	struct CTRLINFO {
		CWnd*	m_pWnd;		// Pointer to the control
		CSize	m_sizeMin;	// Min size of the control
		UINT	m_nOffX,	// Horizontal offset (non splitter side)
				m_nOffY,	// Vertcal offset  border size
				m_nHowY;	// Use of vertical offset Top/Bottom (other side uses X value)

		CTRLINFO( CWnd* pWnd, CSize sizeMin, UINT offX, UINT offY, UINT howY = TOP )
			: m_pWnd( pWnd ), m_sizeMin( sizeMin ), m_nOffX( offX ), 
				m_nOffY( offY ), m_nHowY( howY ) {}

	};

	typedef CTRLINFO* LPCTRLINFO;

	CSize	m_sizeMin; // dialog minimum
	int     m_LeftPercent; //

	// Initialize of the splitter dialog ---                                         percent used by left ctrl
	void InitDialog( LPCTRLINFO infoLeft, LPCTRLINFO infoRight, LPCTRLINFO infoDisp, UINT nPerc );

// Dialog Data
	//{{AFX_DATA(CDDselectSplitter)
	enum { IDD = 0 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDselectSplitter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	LPCTRLINFO	m_infoLeft,			// Left control information
				m_infoRight,		// Right control information
				m_infoDisp;			// Key Display at the top

	CRect		m_rectLeft,			// Left control current size
				m_rectRight,		// Right control current size
				m_rectDisp,			// Display control current size
				m_rectSplitter;		// Splitter bound

	BOOL		m_bMovingSplitter;	// Moving splitter flag
	static WNDPROC m_MFCWndProc;	// Standard MFC window proc

	// Map for the correct instance in my window proc
	static CTypedPtrMap<CMapPtrToPtr, HWND, CDDselectSplitter*> m_staticMap;

	// New window proc
	static LRESULT CALLBACK NewWndProc( HWND, UINT, WPARAM, LPARAM );

	// Change of the splitter position
	void MoveCtrls( int point );

	// Change of the control dimension and position
	void SetCtrl( CWnd* pWnd, CRect& rect, int left, int top, int right, int bottom );
	void ReSize(int cx, int cy); // common size routine   new width, new height

	// Generated message map functions
	//{{AFX_MSG(CDDselectSplitter)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDSELECTSPLITTER_H__93C95992_2CEF_4BB4_9567_7862E5415ED1__INCLUDED_)

/*************************************************************************************************
 *
 *   $History: DDselectSplitter.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
