/*************************************************************************************************
 *
 * $Workfile: ddb.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The dll method api
 *		The data api is in dllapi.h
 *
 *
 * we will use load-time dynamic linking since there is no need to unload 
 * ie - link to ddb.lib  in your project 
 *
 * #include "ddb.h"
 */

#ifndef _DDB_H
#define _DDB_H

#include "ddbGeneral.h"
#include "dllapi.h"

 #ifdef INTHEDLL
  #define DLLFUNC extern "C" __declspec( dllexport )
 #else
  #ifdef DLLFUNC
   #undef DLLFUNC
  #endif
  #define DLLFUNC extern "C" /* __declspec( dllimport ) */
 #endif

class CDictionary;
class LitStringTable;

// initialize using the dd directory string
DLLFUNC RETURNCODE initDB(const char* pDbdir);

/////////////////////////////////////////////////////////////////////////////
// Key lookup/conversion and handling routines
//

// NOTE:
//		DDkey is:
//      = ((mfgID & 0xFF)<<24) | ((dev_type & 0xFF)<<16) | ((dev_Rev & 0xFF)<<8) | (dd_rev & 0xFF)

// key from identity <with strings>...dd_rev of 0xff implies "use latest dd rev"
DLLFUNC RETURNCODE keyFromID(Indentity_t& sID, DWORD& retKey, StrVector_t** strList = NULL);
// identity from key <with strings>
DLLFUNC RETURNCODE idFromKey(DWORD& newKey, Indentity_t& sID, StrVector_t** strList = NULL);
// change the active ddkey
DLLFUNC RETURNCODE setDDFromKey(DWORD& newKey);

DLLFUNC RETURNCODE 	closeDB(void);

DLLFUNC RETURNCODE 	releaseDB(void);

// NOTES:
// term:  raw::> conditionals are returned as conditionals, all choices are included
//        evaluated::> conditionals are evaluated via data items &/| command calls(not used here)
//		  
//	getXXXXXX(yy)   obtains the raw information and the entire hierarchal tree under it
//	fetchXXXX(yy)   obtains the raw information only for a single item (lists are empty )
//
//              --- the dll cannot do evaluations so the following is for information only ---
//  aquireXXX(yy)   obtains the evaluated information and all the evaluated leaves under it
//  procureXX(yy)   obtains the evaluated information without any leaves 
//
//	Devices are describes by a list of items
//
//  Items are described by a list of (probably conditional) attributes
//
//  Attributes depend on DDkey + Item Type + Attribute type for their identity
//             are filled with lots of information that would be rather usless 
//             at any finer grain - 
//             so attributes are always completely filled
//

//////////////////////// System Level Functions ////////////////////////
//
// handle the mfg/devtype/devrev/ddrev/dev tree
//
//Vibhor 140404: Commenting the prototypr; The same is defined in ParserInfc.h now
//stevev 041504  Sorry, you don't change a principle interface like this.
 DLLFUNC RETURNCODE allocDeviceList(aCentry** ppEntryRoot );// alloc + fetch
 DLLFUNC RETURNCODE releaseDevList (aCentry** ppEntryRoot );// return the memory



//////////////////////// Device Level Functions ////////////////////////
// 
 DLLFUNC RETURNCODE allocDevice(DD_Key_t key, aCdevice** ppReturnDevice );// alloc + fetch
//DLLFUNC RETURNCODE getDevice  (UINT32 key, aCdevice*  pReturnDevice  );
 DLLFUNC RETURNCODE getDevice  (DD_Key_t wrkingKey, aCdevice* pAbstractDev,
				CDictionary* dictionary, LitStringTable* litStrings,bool isInTokenizer=false);
 DLLFUNC RETURNCODE fetchDevice(UINT32 key, aCdevice*  pReturnDevice  );
 DLLFUNC RETURNCODE releaseDev (aCdevice** ppAbstractDevice );          // return the memory



//////////////////////// Item level functions///////////////////////////
// 
 DLLFUNC RETURNCODE allocOneItem    ( aCitemBase ** ppAbstractItem );
 DLLFUNC RETURNCODE allocListOfItems(UINT32 key, UINT32 itemType, AitemList_t** ppReturnList  );// alloc & fetch
//                                             itemType 0 gets all items
 DLLFUNC RETURNCODE getListOfItems  (UINT32 key, UINT32 itemType, AitemList_t* pReturnList );
 DLLFUNC RETURNCODE fetchListOfItems(UINT32 key, UINT32 itemType, AitemList_t* pReturnList );

 DLLFUNC RETURNCODE releaseOneItem (aCitemBase ** ppAbstractItem     );// return the memory
 DLLFUNC RETURNCODE releaseItemList(AitemList_t** ppAbstractItemList );// return the memory


////////////////////////  Attribute Level functions ////////////////////
// 
// fills the attribute list in the item
//  you must use allocOneItem and releaseOneItem from above
DLLFUNC RETURNCODE getAttrList  (UINT32 key, aCitemBase*  pItem2fetch4);
DLLFUNC RETURNCODE fetchAttrList(UINT32 key, aCitemBase*  pItem2fetch4);

///////////////////////   String/dictionary functions  /////////////////
//
DLLFUNC RETURNCODE allocString(string** ppRemoteString); // use before the following

DLLFUNC RETURNCODE procureStringBySingleRemote (INT32 value,                 INT32 dictType, wchar_t* pCstr, wstring** ppString2BeFilled);
DLLFUNC RETURNCODE procureStringByDoubleRemote (DD_Key_t section, INT32 offset, INT32 dictType, wchar_t* pCstr, wstring** ppString2BeFilled);

DLLFUNC RETURNCODE releaseString(wstring** ppStringWasFilled); // return the memory
DLLFUNC RETURNCODE releaseStringVect(StrVector_t** ppStrList);// return the memory

///////////////////////   DD file information function(s)  /////////////////
//
DLLFUNC RETURNCODE getDDfileInfo(PFILE_INFO_T pFileInfo);

#endif//_DDB_H


/*************************************************************************************************
 *
 *   $History: ddb.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
