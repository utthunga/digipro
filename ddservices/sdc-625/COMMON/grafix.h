/*************************************************************************************************
 *
 * $Workfile: gragix.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of those common little classes that help out the structure
 *		9/01/04	sjv	created
 *
 * #include "grafix.h"
 */

#ifndef _GRAFIX_H
#define _GRAFIX_H

#pragma warning (disable : 4786) 

#include "ddbGeneral.h"
#include "DDLDEFS.H"
#include "ddbdefs.h"
#include "sysEnum.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include <ostream>
#include <iostream>

using namespace std;

/***************************************************************************************
 * graphics Size enum declaration
 ***************************************************************************************
 */
/* memory location of strings */
extern const char grafSizeStrings[GRAFSIZESTRCOUNT] [GRAFSIZEMAXLEN];// in grafix.cpp
//					  enum type,      zeroth string,  highest vale,    array string length
/* this enum type */
typedef  hCsystemEnum<grafixSize_t,hCddlLong,gS_Undefined,  gS_Unknown, GRAFSIZEMAXLEN>
		 graphicsSize_t;

/* stream io for the enum */
inline ostream& operator<<(ostream& ostr,	graphicsSize_t& it)\
{  ostr << it.getSysEnumStr();	return ostr; }	

/****************************************************************************************/



/***************************************************************************************
 * graphics Line Type enum declaration
 ***************************************************************************************
 */
/* memory location of strings */
extern const char lineTypeStrings[LINETYPESTRCOUNT] [LINETYPEMAXLEN];// in grafix.cpp

#define grafixAttrLineType	4 /* isa 4 enum in source & waveform */

class hCattrLineType : public hCattrBase
{ 
	hCconditional<hCddlIntWhich> condLineType;// holds the value(s) 
	//lineTypeStrings 

protected:
//	RETURNCODE    fetch(void);
	hCddlIntWhich Resolved;

public:
	// no default (we MUST have a handle)
	hCattrLineType(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grafixAttrLineType), condLineType(h),Resolved(h) {};
	// copy constructor
	hCattrLineType(const hCattrLineType& sae)
		:hCattrBase(sae), condLineType(sae.devHndl()),Resolved(sae.devHndl())
		{  condLineType   = sae.condLineType;	};
	// abstract constructor (from abstract class)
	hCattrLineType(DevInfcHandle_t h, const aCattrCondIntWhich*  aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grafixAttrLineType), condLineType(h),Resolved(h)
		{ condLineType.setEqualTo( (void*) &(aCa->condIntWhich));};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return ( Resolved.destroy() | condLineType.destroy());};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      LineType:                 ");
		return condLineType.dumpSelf(indent);};

	hCddlIntWhich procureBase(void)
	{	
		RETURNCODE rc = condLineType.resolveCond(&Resolved);
		if ( rc != SUCCESS ) Resolved.clear();
		return Resolved;
	};

	string getLineTypeString(void);
	int  getEnumStrList(vector<string>& retList); // returns 0 at no error


	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrLineType");};

	RETURNCODE setDefaultVals(RETURNCODE rrc)
	{	return FAILURE;};
};


/************************************************************************************************/




/*************************************************************************************************
 * graphics Scale enum declaration
 *************************************************************************************************
 */
/* memory location of strings */
extern const char scaleStrings[SCALESTRCOUNT] [SCALEMAXLEN];// in grafix.cpp
//					  enum type,      zeroth string,  highest vale,    array string length
/* this enum type */
typedef  hCsystemEnum<scale_t, hCddlLong, sT_Undefined,  sT_Uknown, SCALEMAXLEN> scale_type_t;

/* stream io for the enum */
inline ostream& operator<<(ostream& ostr,	scale_type_t& it)\
{  ostr << it.getSysEnumStr();	return ostr; }	

/****************************************************************************************/


/*
 * HALFPOINT types.

#define X_VALUE				1
#define Y_VALUE				2
 */


/*************************************************************************************************
 * Reference list attribute - needed for graphics
 *************************************************************************************************
 */

class hCattrRefList : public hCattrBase
{
	hCcondList<hCreferenceList>	condRefList;

protected :
	
	hCreferenceList m_refList;	//Vibhor 111004: Added

public:
	hCattrRefList(DevInfcHandle_t h, int attrNum, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,   attrNum    ),condRefList(h),m_refList(h)  {};

	hCattrRefList(DevInfcHandle_t h, int attrNum, 
										   const aCattrReferenceList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,    attrNum     ),condRefList(h),m_refList(h) 
		{condRefList.setEqualTo((aCcondList*) &(aCa->RefList));}//aCcondList/*<aCreferenceList>*/ 
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return (condRefList.destroy() & m_refList.destroy());};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrRefList");};

/*Vibhor 111004: Start of Code*/
	
	void * procure()
	{
		m_refList.clear();
		if ( condRefList.resolveCond(&m_refList) == SUCCESS )
		{	return &m_refList;	}
		else
		{	return NULL;		}
	};

	// modified as per vibhor's email of 06nov06
	RETURNCODE getResolvedList(hCreferenceList *rslvdList)
	{// stevev 09oct06 - need to keep m_refList up to date for isChanged comparison
        /*      RETURNCODE rc = condRefList.resolveCond(&m_refList);
                *rslvdList = m_refList;
                return rc;
                // pre 09oct06::> return condRefList.resolveCond(rslvdList);
		*/
                /*Vibhor 041106: -m_refList was getting appended every time with Steve's changes*/

                /*If at all the intention is to keep the m_RefList up to date, 
                following is the correct way to do it*/
				m_refList.clear();// stevev 09nov06 to prevent memory fault
                RETURNCODE rc = condRefList.resolveCond(rslvdList);
                m_refList = *rslvdList;

                return rc;

	}
/*Vibhor 111004: End of Code*/

	RETURNCODE getDepends(ddbItemList_t& retList); // stevev 12sep06
	void  fillContainList(ddbItemList_t&  iContain);//stevev 27sep06
	
	bool isConditional(void){ return (condRefList.isConditional());};
	bool  reCalc(void);// stevev 18nov05 - returns didChange

};

/************************************************************************************************/



/*************************************************************************************************
 * image storage structure & definitions
 *************************************************************************************************
 */

class hCframe
{
public:
	string			cLang;
	unsigned int	uSize;
	BYTE*			pbRawImage;
	unsigned int	uOffset;	// used for reference only

	hCframe(){clear();};
	hCframe(imgframe_t* pAframe){clear();	operator=(pAframe);};	
	hCframe(const hCframe& src) {	operator=(src);    };

	hCframe& operator=( const hCframe& s)
		{  cLang  = s.cLang;       uSize      = s.uSize;
		   uOffset= s.uOffset;	  
		   pbRawImage = (uSize > 0) ? s.pbRawImage : NULL;
		   return *this;
		};
	hCframe& operator=( imgframe_t* pAframe );
	void     clear(void);

	virtual
		~hCframe()
		{ RAZE_ARRAY(pbRawImage); };
	void destroy(void);

};
typedef vector<hCframe*>         FramePtrList_t;
typedef FramePtrList_t::iterator FramePtrList_it;//ptr2ptr2 hCframe

/************************************************************************************************
 *  The Device's image list is a list of hCimage  [imageInstanceList]
 *  The Device's list of image items (with symbol numbers) hold hCimageItem s which only have a 
 *	(conditional) index into the list of hCimages.[imageItemInstanceList]
 ************************************************************************************************
 */
class hCimage : public FramePtrList_t
{
public:
	hCimage(DevInfcHandle_t h)
	{
		clear();
	};
	hCimage(DevInfcHandle_t h, const hCimage& src);
	virtual ~hCimage();

	hCimage& operator=(const hCimage& s);

	void clear(void);
};


typedef vector<hCimage*>		   Image_PtrList_t;
typedef Image_PtrList_t::iterator  ImagePtrList_it;// ptr2ptr2hCimage

/************************************************************************************************/


#endif//_GRAFIX_H

/*************************************************************************************************
 *
 *   $History:  $
 * 
 *************************************************************************************************
 */
