/**********************************************************************************************
 *
 * $Workfile: dllapi.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		This holds the classes that make up the api that crosses the dll boundry.
 *		These are designed to be ultra-lightweight classes (data only)
 *		In the initial design, everything is public.
 *		
 *		7/30/2	sjv	started creation
 *		9/25/02  sjv converted from template class to standard abstract base classes
 *
 * #include "dllapi.h"
 */

#ifndef _DLLAPI_H
#define _DLLAPI_H

#include "pvfc.h"
#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbdefs.h"
#include "varient.h"

#if !defined(__GNUC__)
#include <CRTDBG.h>
#endif // !defined(__GNUC__)

/*replave boolT,isFALSE,isTRUE with bool	 06Jun06 - conform to new coding rules */

// a useful MACRO
#define maskFromInt(a) (1<<a)
#define maskLessOne(a) (a>>1)  /* some masks are formed (1<<(a-1))
								  ::use maskLessOne(maskFromInt(int))*/
/* NOTE: We use vectors of baseclass pointers because the vectors won't copy
 *	the child's virtual function table.  Using a vector of base classes will 
 *  lose all the child information.  This method allows a cast to the child class 
 *  with no loss of information.
 *
 * NOTE: It is assumed that the memory allocated in the DLL for ALL of these 
 *	classes will be deallocated by the DLL as well.  See the ddb.h for the 
 *	function interface.
 */
/************ removed 10/9/03 to prevent compliler confusion *******
#ifdef DBGHEAP
#define HEAPMARK   int heapMrk
#define NEWMARK    heapMrk = 0
 #ifdef ISDLL
  #define MARKIT     heapMrk = 0x0D11
 #else
  #define MARKIT     heapMrk = 0x0BAD
 #endif
#else
 ************ end remove **********/
#define HEAPMARK   /* not defined*/
#define MARKIT     /* not defined*/
#define NEWMARK    /* not defined*/
////remove too   #endif


#ifdef COUNT_MEMORY
#include <map>
#define DFLTCON 1
#define COPYCON 2
extern void UNREGCHUNK(void* p);
extern void REGISCHUNK(void* p, int src);
extern void CHECKCHUNK(void* p);
#else
#define UNREGCHUNK( a )
#define REGISCHUNK( b, c )
#define CHECKCHUNK( p )
#endif 


/*  stevev 5/11/04 - added for file information support */
// note that the beginning of FILE_INFO_T is exactly a FILETIME (just cast)
//      we use a struct so we can append more stuff later without changing the API
// NOTE this is different then the FILE_TYPE_ID item (it's for file sniffing)
typedef struct fileInfo_s
{
	DWORD LoFileTime;
	DWORD HiFileTime;
	// end of FILETIME look-alike

	tstring file_name;
} 
FILE_INFO_T, *PFILE_INFO_T;

/* stevev 10/13/04 - for system info */
typedef vector<ITEM_ID>	AnIdList_t;/* this will be more complex in later versions*/

/*=*=*=*=*=*=*=*=*=* SYSTEM FUNCTIONS  *=*=*=*=*=**=*=*=*=*=*=*=*=*/
class aCentry
{
public:
   int     number;
   wstring  name;
   wstring  help;
   wstring  childTypeName;
   vector<class aCentry> children;

   aCentry(){clear();};			// constructor
   aCentry(const aCentry& src)	// copy constructor
   {number = src.number;
	name   = src.name;help = src.help;
    childTypeName = src.childTypeName;
    children = src.children;  };
   virtual 
   ~aCentry(){clear();};			// destructor

   void clear(void)
   {
      number = 0;
      name.erase();help.erase();
      childTypeName.erase();
      children.clear();
   };
};



/*=*=*=*=*=*=*=*=* COMPLEX PRIMATIVES  *=*=*=*=*=**=*=*=*=*=*=*=*=*/
class aPayldType // a support primatives for the complex ones
{
protected:
	payloadType_t loadtype;
public:
	aPayldType(payloadType_t ld=pltUnknown	):loadtype(ld){};
	virtual 
	~aPayldType()  { 
	/* not needed	destroy(); */};
	payloadType_t whatPayload(void) 
	{ 	if (loadtype < pltUnknown || loadtype > pltLastLdType)
		{return pltLastLdType;}
		else{return loadtype;}  
	};
	//void set_Payload(payloadType_t p) { loadtype = p; };
	virtual void destroy(void) //PVFC( "aPayldType" );// must have one in a payload
	{
		LOGIT(CERR_LOG,"ERROR: aPayldType.destroy has been called.\n");
	};
};


class aCexpression;// forward reference

class aCreference : public aPayldType
{
public:
	HEAPMARK;
	//               null pointers are in case we change the clear() to delete these
	aCreference():aPayldType(pltReference){pExpr=NULL;pRef=NULL;clear();MARKIT;};
	/* try a copy constructor - solved the problem */
	aCreference(const aCreference& s);
	virtual ~aCreference();
	void destroy(void); // deletes pointer memory!!!   (defined after expression defined)
	CrefType        rtype;  // in foundation.h -> 0 - 24...1 == Item_id,  
							//                             3 = via item array, 
							//                            24 = response code id

	int             isRef; // used as bool:isRef else id is a valid item id...inited to ID (0)
	ITEM_ID         id;	   // ! isRef && refType == 0,1,2,11 to 24 

	// at this time item type may or may not be valid for the given ITEM_ID... 
	//															we need to check after a load
	CitemType		type;	// in foundation.h  -> 0 - 16...1 == variable, 
							//								9 = item_array, 19 = member
					SUB_INDEX       subindex;	// unknown usage????? 
												// may or may not be valid for hart

	// ----- used for via_xxx types (via_item_array, via_collection,etc. (types 3-10))
	int              iName;			// member-name,         parameter-name,
									// parameter-list-name, characteristic-name
	expressionType_t exprType;      // type <direct, if, select, case::> unknown==empty expr>
	// these are pointers since they are used (instantiated) only in particular circumstances
	aCexpression*    pExpr;			// array's (item-array & array) index only
	aCreference*     pRef;			// ItemArray_ref,   (array-ref, collection-ref, record-ref)

	void clear(void)
	{ rtype.clear();isRef=0;id=0;type.clear();subindex=0;
	  iName=0;exprType =eT_Unknown;pExpr=NULL;pRef=NULL;};
};

typedef vector<aCreference>        AreferenceList_t;
class aCreferenceList : public aPayldType, public AreferenceList_t
{				  
public:
	HEAPMARK;
	aCreferenceList():aPayldType(pltReferenceList){MARKIT;};
	aCreferenceList(const aCreferenceList& s):aPayldType(pltReferenceList)
	{
		for(AreferenceList_t::const_iterator  iT = /*(aCreference*)*/s.begin(); // loose cast & change to const_iterator PAW
	iT < s.end(); 
	++iT) 
		{  
		//push_back(*((aCreference*)iT));  // PAW see below
		push_back(*((aCreference*)&(*iT)));  // PAW see above
		}MARKIT;         	};
	//VMKP 260304
	virtual 
	~aCreferenceList(){
		for(AreferenceList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		AreferenceList_t::clear();
	};
	//VMKP 260304
	void destroy(void){		
		for(AreferenceList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		AreferenceList_t::clear();
	};
};

// After conditional list is defined::typedef aCcondList/*<aCreferenceList>*/ AcondReferenceList_t;

/*--------------------- expression ------------------------*/

class aCdepends
{
public:
	HEAPMARK;
	arithOp_t      useOptionVal;	// Max/Min/don't Care
	DDL_UINT       which;           // offset+1 into MIN | MAX range->list[]
	aCreference    depRef;			// a reference - resolves to a variable item id

	aCdepends(){MARKIT;};
	aCdepends(const aCdepends& s):useOptionVal(s.useOptionVal),which(s.which),
								  depRef(s.depRef) {MARKIT;};
	virtual 
	~aCdepends(){ 
		destroy(); clear(); };
	void destroy(void){ 
		depRef.destroy(); };

	void clear(void){useOptionVal=aOp_dontCare;which=0;depRef.clear();};
};

class aCexpressElement
{
public: 
	HEAPMARK;
	expressElemType_t   exprElemType;    // types 1 thru 21 are opcodes; 22-29 values
	CValueVarient       expressionData;  // holds opcode(1-21),const(22,23) - we don't use index type
	aCdepends			dependency;      // holds !isRef @ 24, holds dependency info (25-29)
	
	aCexpressElement(){MARKIT;};
	/* try a copy constructor */
	aCexpressElement(const aCexpressElement& s):exprElemType(s.exprElemType),
		expressionData(s.expressionData),dependency(s.dependency)  {MARKIT;};
	virtual 
	~aCexpressElement(){/*5653 replacement fixed::(see 5662) destroy();use:*/
		clear();  };
	void clear(void){exprElemType=eeT_Unknown;expressionData.clear();dependency.clear();};
	void destroy(void)
	{ 
		dependency.destroy(); 
	};
};

typedef vector<aCexpressElement>	RPNexpress_t;

class aCexpression
{	// the expression type always goes with the containing class.
public:
	RPNexpress_t	 expressionL;	// list of CexpressElement in RPN order  

	HEAPMARK;
	aCexpression(){MARKIT;};
	aCexpression(const aCexpression& s);// stevev 26apr07 - moved and copy from src, not self...

	virtual 
	~aCexpression(){
		if ( expressionL.size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
			expressionL.clear();
	};
	void clear(void) { expressionL.clear(); };
	void destroy(void){
		FOR_iT(RPNexpress_t, expressionL)
		{
			iT->destroy();
		}; 
		expressionL.clear();  
		if ( expressionL.size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
		{
/*
    LINUX_PORT - REVIEW_WITH_HCF: expressionL is a member and its destructor 
    is automatically invoked on aCexpression destruction. It is unclear as 
    to why this is explicitly called.
*/
#if !defined(__GNUC__)
			expressionL.~vector<aCexpressElement>();// stevev 09jul07 - attempt to plug a big leak
#endif // !__GNUC__
		}
	};
};

inline
aCexpression::aCexpression(const aCexpression& s)
{	
	aCexpressElement* pexe;
	RPNexpress_t* pL = (RPNexpress_t*) &(s.expressionL);
	for(RPNexpress_t::iterator iT = pL->begin(); iT != pL->end(); ++iT)
	{
		pexe = (aCexpressElement*)&(*iT);// PAW &(*) added
		expressionL.push_back( *pexe );
	} 
};

// reference destroy after expression defined.
inline void aCreference::destroy(void) 
{	if ( pExpr!=NULL)
	{ pExpr->destroy();} 
	RAZE(pExpr); 
	if (pRef!=NULL) 
	{ pRef->destroy(); } 
	RAZE(pRef);  
};

// reference destructor after expression defined.
inline aCreference::~aCreference() 
{	destroy();
	clear(); 
	RAZE(pExpr); 
CHECKCHUNK(pRef);
	RAZE(pRef); 
};

// reference copy constructor after expression defined
inline
aCreference::aCreference(const aCreference& s):aPayldType(((aPayldType*)&s)->whatPayload())
{	rtype=s.rtype;	isRef=s.isRef;	id = s.id;	type=s.type;	subindex=s.subindex;
	iName = s.iName;	exprType=s.exprType;
	if (s.pExpr) pExpr = new aCexpression(*s.pExpr); else  pExpr = s.pExpr;
	if (s.pRef)  pRef  = new  aCreference(*s.pRef);	 else   pRef = s.pRef;
	/*had::> ((aCreference*)&s)->pExpr = NULL;*/
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	the aCgenericConditional.aExpressDest must record the pointer types so that
		the client may instaniate the matching type during data consumption
	This is done by using two abstract base classes, one for the payload
	and one for the conditional destination.  This will allow each pointer to
	be queried for its type. 
	>> see next line---There is no 'set' method since types are not allowed to change.
	>>maybe::> A working payload may be used for more than one payload type
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

class aCondDestType
{
	bool isList;
public:
	HEAPMARK;
	aCondDestType(){MARKIT;};
	aCondDestType(bool isLst):isList(isLst){MARKIT;};//isTRUE, isFALSE;
	virtual ~aCondDestType(){};
	bool isAlist(void) { return isList; };
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	aCgenericConditional is a generic template for all conditionals
		see tutorial file for a detailed explaination
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


//was //template<class DESTTYPE, class PAYLDTYPE>  
                    /* desttype was a conditional OR conditionalList */
//		the reason that the types were removed (and this is no longer a template)
//		is that the these are ALWAYS populated and depopulated (consumed) by the correct
//		C or hC classes and they know know what abstract types are required so I no longer
//		carry that complexity through this api. 
class aCgenericConditional
{
public:
			class aCexpressDest
			{
			public:
				HEAPMARK;
				aCexpressDest(){clear();MARKIT;};
				aCexpressDest(const aCexpressDest& s):destType(s.destType),
													  destExpression(s.destExpression)
				{	pCondDest = s.pCondDest;  		pPayload  = s.pPayload;   
					;MARKIT; ((aCexpressDest*)&s)->clear(); // take memory ownership   
				};				
				virtual 
				~aCexpressDest(){ 
						if (pPayload) {
							delete pPayload; pPayload =  NULL;   }
						if (pCondDest) {
							delete pCondDest;pCondDest =  NULL;  }
						destExpression.destroy();
				};
	//VMKP 260304
				void destroy(void);

/*				void destroy(void)
				{	RAZE(pCondDest);RAZE(pPayload);destExpression.destroy();};*/

	//VMKP 260304
				expressionType_t   destType;//this destination type   - actually a clauseType_t  
									   //direct,then,else,case,default are the ONLY valid types

				aPayldType*      pPayload;	// destination type at is a direct
				aCondDestType*   pCondDest;	// destination type at is not direct 
											//              (all others:then,else,case,default)

				aCexpression destExpression;	// CASE type EXPRESSION only - otherwise Empty	
				void clear(void){destType = eT_Unknown; pCondDest=NULL;pPayload=NULL;
							destExpression.clear();     };
			};
	
			typedef vector<aCexpressDest> dest_List;


	expressionType_t priExprType;   // actually a axiomType_t <if,select,direct are the ONLY valid types>
									//   - direct implies there is a single payload item in the dest_list
	aCexpression     priExpression; // primary expression - empty on direct
	dest_List        destElements;  // a vector of destinations
								    //   - direct,then,else,case,default are the ONLY valid destinations
	aCexpressDest    aWrkingExprDest;//a working variable whose ptr may be returned to a caller

	HEAPMARK;
	aCgenericConditional(){clear();MARKIT;};
	aCgenericConditional(const aCgenericConditional& s) : priExpression(s.priExpression),
		destElements(s.destElements){ priExprType=s.priExprType;  MARKIT;};
    virtual
	~aCgenericConditional(){
		destroy();  clear(); };
	void clear(void){
		priExprType = eT_Unknown; 
		priExpression.clear(); 
		aWrkingExprDest.clear();
	    FOR_iT(dest_List, destElements){iT->clear();}; destElements.clear();   };
	void destroy(void){ 	
		FOR_iT(dest_List, destElements)	
		{
			aCexpressDest* pExDst = (aCexpressDest*)&(*iT);
			pExDst->destroy(); 
		} 
		if ( destElements.size() != 0)
			destElements.clear(); 
		priExpression.destroy();
		aWrkingExprDest.destExpression.destroy();
		};
		// aWrkingExprDest may need to be destroyed too
		//    assume aWrkingExprDest's pointer ownership has been transfered.
		//		then:aWrkingExprDest.destroy();};
};


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	Expressions are payloads in Variable ReadTime - WriteTime and Scaling attributes							
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class aCExpressionPayload : public aPayldType
{
public:
	//expressionType_t priExprType;   // actually a axiomType_t <if,select,direct are the ONLY valid types>
									//   - direct implies there is a single payload item in the dest_list
	aCexpression     theExpression; // primary expression - empty on direct
	//dest_List        destElements; 

	aCExpressionPayload():aPayldType(pltExpression){MARKIT;};
	virtual 
	~aCExpressionPayload(){ 
		destroy();};
	void destroy(void)
	{
		theExpression.destroy();
	}; 
};

// minmaxlists moved below----23aug06

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	a simple (sic) conditional							
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class aCconditional : public aCondDestType, public aCgenericConditional
{// no contents...parents have it all
public:
	HEAPMARK;
	aCconditional():aCondDestType(false){MARKIT;};// not a list
	virtual
	~aCconditional(){};
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	class aCcondList        is a list of conditionally included lists	
	NOTE UNIQUENESS: The PAYLOAD MUST be a VECTOR  ie
	     class baseList : public vector<itemListed> {};
	this allows some generic copies to occur!
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class aCcondList : public aCondDestType
{
public:
		// the dual typedefs are holdovers to when this was a template class
		typedef aCgenericConditional  oneGeneric_t;
		typedef	vector<oneGeneric_t>  listOconds_t;

	HEAPMARK;
	listOconds_t genericlist; // a list of genericConditionals 

	aCcondList(const aCcondList& src) : aCondDestType(true), genericlist( src.genericlist ){ };

	aCcondList(): aCondDestType(true){clear();MARKIT;};// is a list
	virtual
	~aCcondList(){
		destroy(); clear();};
	void clear(void){ genericlist.clear(); };
	void destroy(void){ FOR_iT(listOconds_t,genericlist){iT->destroy();} genericlist.clear();};
};
typedef aCcondList/*<aCreferenceList>*/   AcondReferenceList_t;

/*=*=*=*=*=*=*=* END COMPLEX PRIMATIVES  *=*=*=*=**=*=*=*=*=*=*=*=*/


/*=*=*=*=*=*=*=*=*=*=*=* PRIMATIVES  *=*=*=*=*=*=**=*=*=*=*=*=*=*=*/
class aCddlString : public aPayldType
{
public:
	HEAPMARK;
	wstring         theStr;	//18jan08 - timj
	bool            isSet;  // 30sep10-stevev-set when value in theStr is valid(for a valid "")
	unsigned short  len;	/* the length of the string */
	unsigned short  flags;	/* memory allocation flags *///5/29/02 -added a use-default flag
	unsigned long   strTag;
	//unsigned int    ddKey;  /* in case you wanna get another language*/	
	DD_Key_t        ddKey;  /* in case you wanna get another language*/
	unsigned int    ddItem;
	unsigned long   enumVal;// to help the enums

	aCreference*    pRef;	/* rarely needed, allocate as required */
aCreference*    pDBGRef;
public:
	/* calling clear() in the constructor didn't tell purify that these were initialized*/
	aCddlString():aPayldType(pltddlStr),len(0),flags(0),strTag(0),ddKey(0),ddItem(0),
							 enumVal(0),pRef(NULL),isSet(false) 
	{ theStr.erase(); 
pDBGRef = NULL;
	  MARKIT;       };
	aCddlString(const aCddlString& s):theStr(s.theStr),isSet(s.isSet),flags(s.flags),
		strTag(s.strTag),ddKey(s.ddKey),ddItem(s.ddItem),enumVal(s.enumVal)
	{	len = theStr.size();
		if ( s.pRef )
		{
			pRef = new aCreference(*(s.pRef));// make a copy of it
REGISCHUNK(pRef,2);
if (s.pRef != s.pDBGRef && strTag == 2)//ENUMERATION_STRING_TAG
{
#ifdef _DEBUG
        // TOOD(DP, "CrtDbgBreak is Windows specific -- stub out or implement Linux compat.")
	_CrtDbgBreak();
#endif
}
else
{pDBGRef = pRef;
}
		}
		else
		{
			pRef = NULL;
pDBGRef = NULL;
		}
	};
	virtual
	~aCddlString(){
		destroy();  clear();   };
	
	void destroy(void) 
	{ 
		if (pRef != NULL)
		{ pRef->destroy();
		delete pRef;
UNREGCHUNK(pRef);
if (pRef != pDBGRef)
{
#ifdef _DEBUG
	_CrtDbgBreak();
#endif
}
pDBGRef =
		pRef = NULL;   }

/*
    LINUX_PORT - REVIEW_WITH_HCF: theStr is a member variable and its 
    destructor is automatically invoked on aCddlString destruction. 
    It is unclear as to why this is explicitly called.");
*/
#if !defined(__GNUC__)
		theStr.~wstring();
#endif // !__GNUC__
	};
	// clear's caller is responsible for deleting/NULLing the reference 
	//                    - we assume that is done before we are called
	void clear(void) 
		{theStr.erase(); len =flags = 0;enumVal = strTag = 0L; ddKey=ddItem= 0; pRef = NULL;};
		//if (pRef==(aCreference*)0xcccccccc) pRef=NULL;else if (pRef != NULL) pRef->clear();};
};

class aCbitString : public aPayldType
{
public:
	ulong bitstringVal;
	aCbitString():aPayldType(pltbitStr){clear();};
	virtual
		~aCbitString(){clear();};
	void clear(void){bitstringVal = 0L;};
	void destroy(void){};// must have one in a payload
};

class aCddlLong : public aPayldType
{
public:
	// stevev 27aug10..convert to long long values   ulong ddlLong;
	UINT64   ddlLong;
	aCddlLong():aPayldType(pltddlLong){};
	virtual
		~aCddlLong(){clear();};
	void destroy(void){};
	void clear(void){ddlLong = 0L;};
};
/*=*=*=*=*=*=*=*=*=*=* END PRIMATIVES  *=*=*=*=*=**=*=*=*=*=*=*=*=*/




/*=*=*=*=*=*=*=*=*=*=* ENUM SUPPORT TYPES  *=*=*=**=*=*=*=*=*=*=*=*/
// part of the dd binary - data is stored and retrieved
typedef struct oStatus_s	// see varTypeEnum.h for explaination
{
	unsigned short  kind;
	unsigned short  which;
	unsigned short  oclass;
	
	struct oStatus_s& operator=(const struct oStatus_s& src)
	{	kind   = src.kind;
		which  = src.which;
		oclass = src.oclass;
		return *this;
	};
	// start stevev 2/24/04
	void clear(void){kind = which = oclass=0;};
#if !defined(__GNUC__)
	struct
#endif // __GUNC__
	oStatus_s(){clear();};
#if !defined(__GNUC__)
	struct
#endif // __GUNC__
	oStatus_s(const struct oStatus_s& src){operator=(src);};//memory issues
	// end stevev
}   outputStatus_t;
	
// 2008 library self destructs on copy constructor's outStatusList_t=src's outStatusList_t
//              ...at least when they are both empty...give 'iterator list corrupted'
//typedef vector<outputStatus_t>     outStatusList_t;
typedef vector<outputStatus_t>     outStatusList_td;

class outStatusList_t : public outStatusList_td
{
public:
	outStatusList_t& operator=(const outStatusList_t& src)
;/**** now implemented in ddbdefs.cpp
	{
		outStatusList_td::const_iterator osIT;
		outStatusList_td::clear();
		if (src.size() > 0 )
		{
			for (osIT = src.begin(); osIT != src.end(); ++ osIT)
			{// ptr2outputStatus_t
				push_back(*osIT);
			}
		}// else, just clear and leave
		return (*this);
	};
***/
	void clear(void)
;/**** now implemented in ddbdefs.cpp
	{ 
		outStatusList_td::const_iterator osIT;
		int k = size();
		if ( ! k ) return;
		for (osIT = begin(); osIT != end(); ++ osIT)
		{
			((outputStatus_t*) &(*osIT))->clear();
		}
		erase(begin(),end());
	};
***/
};

// the actual enum element
class aCenumDesc 
{
public:
	HEAPMARK;
	unsigned long   val;
	aCbitString     func_class;	/* functional class - a ulong bit pattern*/
	aCddlString     descS;
	aCddlString     helpS;	
	aCreference     actions;

	//BIT_ENUM_STATUS
	unsigned long   status_class;
	outStatusList_t oclasslist; // oclasses...Empty when kind_oclasses is OC_NORMAL

	aCenumDesc(){val = 0; func_class.clear(); descS.clear(); helpS.clear(); 
						actions.clear();status_class = 0;/*clear();*/MARKIT;};				
	aCenumDesc(const aCenumDesc& s):func_class(s.func_class),descS(s.descS),helpS(s.helpS),
	actions(s.actions), oclasslist(s.oclasslist) { val = s.val; status_class=s.status_class;};

	virtual 
	~aCenumDesc(){
	destroy();  clear();  };
	void clear(void) {val = 0; func_class.clear(); descS.clear(); helpS.clear(); 
						actions.clear();status_class = 0; oclasslist.clear();};
	void destroy(void){
		func_class.destroy();
		descS.destroy();
		helpS.destroy();
		actions.destroy();
		oclasslist.clear();
	};
};

typedef vector<aCenumDesc>         AenumDescList_t;


// the enum is described as a list of elements
class aCenumList : public aPayldType, public AenumDescList_t // descList;
{	 
public: 
	aCenumList():aPayldType(pltEnumList){};
	//VMKP 260304
	virtual
	~aCenumList(){		
		for(AenumDescList_t::iterator iT = begin();iT<end();iT++)
		{ // stevev 10oct11 - stop using the iterator as a pointer
			aCenumDesc* pED = (aCenumDesc*)&(*iT);
			pED->destroy();		} 
// WS - 9apr07 - VS2005 checkin
//		AenumDescList_t::clear();AenumDescList_t::~vector<aCenumDesc>();
		// The following line is not obsolete in VS2005
		AenumDescList_t::clear();

/*
    LINUX_PORT - REVIEW_WITH_HCF: Having to call STL container base class 
    destructor explicitly because they are not designed to be base classes 
    (no virtual destructor)
*/
		if ( size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
#if defined(__GNUC__)
			this->~AenumDescList_t();
#else // !__GNUC__
			AenumDescList_t::~vector();
#endif // __GNUC__

	};
	//VMKP 260304
	void clear(void) {
		FOR_this_iT(AenumDescList_t,this) {iT->clear();}//for(lt::iterator iT = begin();iT<end();iT++)
		AenumDescList_t::clear();         };
	void destroy(void){		
		for(AenumDescList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		AenumDescList_t::clear();    
		// 2005 convert 27jul10::>  AenumDescList_t::~vector<aCenumDesc>(); // stevev 09jul07 try to plug leak
/*
    LINUX_PORT - REVIEW_WITH_HCF: It's not clear why 'destroy' should call 
    base class destructor. This object continues to exist until it is 
    deleted.
*/
#if !defined(__GNUC__)
		if ( size() )// try to prevent deletion crash
			AenumDescList_t::~vector();
#endif // __GNUC__
	};
};

typedef aCcondList/*<aCenumList>*/	   AcondEnumList_t;

/*=*=*=*=*=*=*=*=* END ENUM SUPPORT TYPES  *=*=*=**=*=*=*=*=*=*=*=*/

/*========================= BASE CLASSES =========================*/
// common to all attributes
class aCattrBase 
{
public:
	aCattrBase() { clear(); }  // HOMZ - added constructor
    virtual ~aCattrBase(){};
        //the attribute type    #####define attr_mask valid values
	ulong      attr_mask; // DDLDEFS.H  see VAR_TYPE_SIZE

	virtual void clear(void) 
	{
		attr_mask = 0L;
    };
};

// vector of pointers so we can cast to actual type
typedef vector<aCattrBase*>		   AattributeList_t;



/*=*=*=*=*=*=*=*=*=* MEMBER SUPPORT TYPES  *=*=*=**=*=*=*=*=*=*=*=*/
class aCmemberElement 
{
public:
	HEAPMARK;
	unsigned long    locator;
	aCreference		 ref;
	string           mem_name;

	aCddlString      descS;
	aCddlString      helpS;
	
	aCmemberElement(){MARKIT;};
	aCmemberElement(const aCmemberElement& s):ref(s.ref),mem_name(s.mem_name),descS(s.descS),
		helpS(s.helpS) 	{locator = s.locator; MARKIT;};
	virtual
	~aCmemberElement(){
		ref.destroy();
		descS.destroy();helpS.destroy();
		clear();
	};
	void clear(void){locator = 0; ref.clear();descS.clear();helpS.clear();}  ;
};

typedef vector<aCmemberElement> AmemberElemList_t;
//								AitmArrElemList_t
//								AcollectionMemList_t

//    aCItmArrElementList
//    aCCollectionMemberList
class aCmemberElementList : public aPayldType, public	AmemberElemList_t 
{				  
public:
	HEAPMARK;
	aCmemberElementList():  aPayldType(pltGroupItemList){MARKIT;};
	aCmemberElementList(aCmemberElementList& s)	
	{	loadtype = ((aPayldType *) &s)->whatPayload();
		//aCdataitemList * pDIL = (AmemberElemList_t *) &sl;
		for(AmemberElemList_t::iterator iT = s.begin(); iT != s.end(); ++iT)
//		{	push_back(*((aCmemberElement*)iT) );  } // PAW see below
		{	push_back(*((aCmemberElement*)&(*iT)) );  } // PAW see above
	};
	virtual
	~aCmemberElementList(){ destroy();	clear();};

	void clear(void) {
		FOR_this_iT(AmemberElemList_t,this) {iT->clear();}  };

	void destroy(void){			    /* VMKP added on 0204040 */
		for(AmemberElemList_t::iterator iT = begin();iT<end();iT++)
		{ iT->ref.destroy();iT->descS.destroy();iT->helpS.destroy();} 
		AmemberElemList_t::clear();
	};
};

typedef aCcondList/*<aCmemberElementList>*/   AcondMemberElemList_t;
//											  AcondItmArrElemList_t
//											  AcondCollMemList_t


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	Min-Max List							
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

class aCminmaxVal
{
public:
	int  whichVal;
	// isa Conditional Expression, not::>> aCexpression pExpr;
	aCconditional/*<aCExpressionPayload>*/ condExpr;

	void destroy(void)	{condExpr.destroy();  };
	void clear(void)    {condExpr.clear(); whichVal = -1; };
};

typedef vector<aCminmaxVal>         AMinMaxList_t;

typedef AMinMaxList_t::iterator     ITminMaxList_t;


class aCminmaxList : public AMinMaxList_t //list of cond expr's;
{
public:
	aCminmaxList(){};

	~aCminmaxList(){  destroy(); };

	//VMKP Added 020404
	void destroy(void){		
		for(ITminMaxList_t iT = begin();iT<end();iT++){ iT->destroy();} 
		clear();
	};
	// stevev 23aug06 added
	void clear(void) {		
	    FOR_this_iT(AMinMaxList_t, NULL){iT->clear();}; 
		};
};

class aCattrVarMinMaxList  : public aCattrBase
{	// List of Int-Which + Conditional expression
public:
	aCminmaxList   ListOminMaxElements;

	void clear(void){aCattrBase::clear();
		     ListOminMaxElements.clear(); };
};

/* pre 23aug06 structure::>>
class aCminmaxVal
{
public:
	int  whichVal;
	aCexpression pExpr;
	void destroy(void)
	{pExpr.destroy();};
};

typedef vector<aCminmaxVal>         AMinMaxList_t;

// Min-Max List 
class aCminmaxList : public aPayldType, public AMinMaxList_t
{	 
public: 
	aCminmaxList():aPayldType(pltMinMax){};
	//VMKP 260304
	~aCminmaxList(){		
		for(AMinMaxList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		AMinMaxList_t::clear();
	};
	//VMKP 260304
	void clear(void) {
		FOR_this_iT(AMinMaxList_t,this) {iT->destroy();}
		AMinMaxList_t::clear();         };
	//VMKP 260304
	void destroy(void){		
		for(AMinMaxList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		AMinMaxList_t::clear();
	//VMKP 260304
	};
};
class aCattrVarMinMaxList  : public aCattrBase
{	// int which with conditional expression  min (int)	
public:
	aCconditional/x*<aCminmaxList>*x/ condMinMaxVal;

	void clear(void){aCattrBase::clear();
				   condMinMaxVal.clear(); };
};
**** end of pre 23aug06 ****/


/****************** GRID MEMBERS **********************/

class aCgridMemberElement 
{
public:
	HEAPMARK;
	aCddlString      header;
	aCreferenceList	 refLst;

	
	aCgridMemberElement(){MARKIT;};
	/* problems with stl push_back loosing elements - make a copy constructor */
	aCgridMemberElement(const aCgridMemberElement& s)
	{MARKIT; 
	header = s.header; 
	refLst = s.refLst;};

	void clear(void){header.clear();refLst.clear();}  ;
};

typedef vector<aCgridMemberElement> AGridMemberElemList_t;


class aCgridMemberList : public aPayldType, public	AGridMemberElemList_t 
{				  
public:
	HEAPMARK;
	aCgridMemberList():  aPayldType(pltGridMemberList){MARKIT;};
	~aCgridMemberList(){ destroy();	};

	void clear(void) {
		FOR_this_iT(AGridMemberElemList_t,this) {iT->clear();}
		};

	void destroy(void){			    /* VMKP added on 0204040 */
		for(AGridMemberElemList_t::iterator iT = begin();iT<end();iT++)
		{ iT->refLst.destroy();iT->header.destroy();} 
		AGridMemberElemList_t::clear();
	};
};

typedef aCcondList/*<aCgridMemberList>*/   AcondGridMemberList_t;

/*=*=*=*=*=*=*=*=* END MEMBER SUPPORT TYPES  *=*=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*  SOME COMMON ATTRIBUTES  =*=*=**=*=*=*=*=*=*=*=*/

/********************* MEMBERS ************************/
class aCattrMemberList : public aCattrBase
{
public:
	AcondMemberElemList_t condMemberElemListOlists;

	void clear(void){aCattrBase::clear();
					 condMemberElemListOlists.clear();        };
};


/****************** GRID MEMBERS **********************/
class aCattrGridMemberList : public aCattrBase
{
public:
	AcondGridMemberList_t condGridMemberListOlists;

	void clear(void){aCattrBase::clear();
					 condGridMemberListOlists.clear();        };
};

/*********************** LONG **************************/
class aCattrCondLong : public aCattrBase
{
public:
	aCconditional/*<aCddlLong>*/ condLong;// holds the value(s) 

	void clear(void){aCattrBase::clear();
					   condLong.clear();        };	
#ifdef TOKENIZER
	bool isConditional(void){ return condLong.isConditional(); };
#endif
};
class aCattrLong : public aCattrBase
{
public:
	aCddlLong theLong;// holds the value (boolean etc) 

	void clear(void){aCattrBase::clear();
					 theLong.clear();        };	
#ifdef TOKENIZER
	bool isConditional(void){ return false; };
#endif
};
#define aCattrBool  aCattrLong

class aCattrUuid : public aCattrBase
{
public:
	aCddlLong upper_half;// holds half the value -unsigned
	aCddlLong lower_half;// holds half the value -unsigned

	void clear(void){aCattrBase::clear();
			upper_half.clear();   lower_half.clear();     };	
#ifdef TOKENIZER
	bool isConditional(void){ return false; };
#endif
};

/******************** REFERENCE *************************/
class aCattrCondReference : public aCattrBase
{
public:
	aCconditional/*<aCreference>*/ condRef;// holds the value(s) 

	void clear(void){aCattrBase::clear();
					    condRef.clear();        };
};

/********************** STRING **************************/
class aCattrString : public aCattrBase		
{
public:
	aCconditional/*<aCddlString>*/ condString;

	void clear(void){aCattrBase::clear();
					  condString.clear();        };
};

/******************** ACTION LIST ************************
class aCattrActionList   : public aCattrBase
{	
public:
	AcondReferenceList_t    ;// reference list
	
	void clear(void){aCattrBase::clear(); 
	                  ActionList.clear(); };
};
*/
#define aCattrActionList	aCattrReferenceList
#define ActionList			RefList

/****************** REFERENCE LIST ************************/
class aCattrReferenceList   : public aCattrBase
{	
public:
	AcondReferenceList_t    RefList;
	
	void clear(void){aCattrBase::clear(); 
	                  RefList.clear(); };
};

/********************** BITSTRING ************************/
class aCattrBitstring : public aCattrBase
{
public:
	aCconditional/*<aCbitString>*/ condBitStr;// holds the value(s) 

	void clear(void){aCattrBase::clear();
					  condBitStr.clear();        };
};

/********************** EXPRESSION ***********************/
class aCattrCondExpr : public aCattrBase
{	
public:
	aCconditional/*<aCExpressionPayload>*/ condExpr;

	void clear(void){aCattrBase::clear();
					   condExpr.clear();        };
};

/********************** INTWHICH ***********************/
class aCintWhich : public aPayldType
{	

public:
	unsigned long value;
	int           which; // only valid when value == DATA_LINETYPE


	aCintWhich():  aPayldType(pltIntWhich){clear();};
	~aCintWhich(){ 	};

	void clear(void) {	value = 0; which = 0;};
	void destroy(void) {};
};

class aCattrCondIntWhich : public aCattrBase
{	
public:
	aCconditional/*<aCintWhich>*/ condIntWhich;

	void clear(void){aCattrBase::clear();
					   condIntWhich.clear();   };
};



/********************** PARAMETER ***********************/
class aCparameter : public aCattrBase
{	

public:
	unsigned long type;		 // variable type
	int           modifiers; // the type-modifiers (isRef,isArray,isConst)
	string		  paramName;


	aCparameter() { clear();};
	~aCparameter(){ 	};

	void clear(void)   { type = 0; modifiers = 0; paramName.erase(); };
	void destroy(void) { clear();};
};


typedef vector<aCparameter>         AparamList_t;


class aCparameterList : public aCattrBase, public AparamList_t 
{		 
public: 	
	HEAPMARK;
	aCparameterList(){MARKIT;};
	
	void destroy(void)
	{	for(AparamList_t::iterator iT = begin();iT<end();iT++)
		{	iT->destroy();			 } 
		AparamList_t::clear();
	};

	~aCparameterList()
	{	destroy();   };
	void clear(void) { aCattrBase::clear(); AparamList_t::clear();};
};


/*=============================== DEBUG INFO ================================*/
/****************** MEMBER DEBUG INFO *******************/
class aCdebugMember 
{
public:
	string		  symbolName;
	int			 memberValue;

	ulong		   use_flags;// not needed, carried temporarily

	aCdebugMember() { clear();};
	aCdebugMember(const aCdebugMember& s):symbolName(s.symbolName) 
	{	memberValue = s.memberValue;  use_flags = s.use_flags;   };
	~aCdebugMember(){ 	};

	void clear(void)   { memberValue = 0; use_flags = 0; symbolName.erase(); };
	void destroy(void) { clear();};
};


typedef vector<aCdebugMember>         AdebugMemberList_t;


/******************** ATTR DEBUG ************************/
class aCdebugAttr 
{
public:
	int			     attrTag;
	int			  lineNumber;
	aCddlString		fileName;

	AdebugMemberList_t memList;// empty except for member attrs

	aCdebugAttr() { clear();};
	aCdebugAttr(const aCdebugAttr& s):memList(s.memList),fileName(s.fileName) 
	{	attrTag = s.attrTag; lineNumber = s.lineNumber;  };
	~aCdebugAttr(){ 	};

	void clear(void)   { attrTag = lineNumber = 0; memList.clear(); fileName.clear();};
	void destroy(void) { clear();};
};


typedef vector<aCdebugAttr>         AdebugAttrList_t;

/************************ DEBUG *************************/
class aCdebugInfo : public aCattrBase
{	

public:
	string		  symbolName;
	aCddlString		fileName;
	int			  lineNumber;
	ulong		   use_flags;// not needed, carried temporarily
	
	AdebugAttrList_t   attrs;


	aCdebugInfo() { clear();};
	aCdebugInfo(const aCdebugInfo& s):
								   symbolName(s.symbolName),fileName(s.fileName),attrs(s.attrs)
	{	lineNumber=s.lineNumber; use_flags = s.use_flags;  };

	~aCdebugInfo(){ 	};

	void clear(void)   { symbolName.erase(); fileName.clear(); lineNumber = 0; use_flags = 0; 
							attrs.clear();};
	void destroy(void) { clear();};
};
/*=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*/
/*=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*/
/*=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*=*=*=*=*=*=**=*=*=*=*=*=*=*/

/*=============================== VARIABLES ================================*/

/***************** VAR TYPE-SIZE ************************/
// variable type class
class aCvarTypeDescriptor 
{// this is a NON-CONDITIONAL attribute
public:
	
	int       actualVarType;	// actually a variableType_t
	int       actualVarSize;    // in bytes, I believe

	aCvarTypeDescriptor& operator=(aCvarTypeDescriptor& src)
	{ actualVarType = src.actualVarType;
	  actualVarSize = src.actualVarSize; return *this;	
	};
};

// VAR_LABEL_ID		use aCattrString
// VAR_VALID_ID		use aCattrCondLong
// VAR_HELP_ID		use aCattrString

/********************* TYPE ******************************/
// VAR_TYPE_SIZE_ID use ::>
class aCattrTypeType : public aCattrBase 
{
public:
	aCvarTypeDescriptor notCondTypeType;
	// this is a non-conditional attribute
	// hcf decided that a variable's type will never be conditional
};


/******************* ENUMERATED **************************/
// VAR_ENUMS_ID		use::>
class aCattrEnum : public aCattrBase
{
public:
	AcondEnumList_t condEnumListOlists;
	
	void clear(void){aCattrBase::clear();
					 condEnumListOlists.clear();        };
};


// VAR_CLASS_ID				use aCattrBitstring
// VAR_HANDLING_ID			use aCattrBitstring
// VAR_UNIT_ID				use aCattrString	{ CONSTANT-UNIT }

// VAR_PRE_READ_ACT_ID	 	use aCattrActionList
// VAR_POST_READ_ACT_ID	 	use aCattrActionList
// VAR_PRE_WRITE_ACT_ID	 	use aCattrActionList
// VAR_POST_WRITE_ACT_ID 	use aCattrActionList	
// VAR_PRE_EDIT_ACT_ID	 	use aCattrActionList
// VAR_POST_EDIT_ACT_ID  	use aCattrActionList
// VAR_READ_TIME_OUT_ID		use aCattrCondExpr
// VAR_WRITE_TIME_OUT_ID	use aCattrCondExpr
// VAR_DISPLAY_ID			use aCattrString
// VAR_EDIT_ID				use aCattrString
// VAR_MIN_VAL_ID		    use aCattrVarMinMaxList
/*  This attribute was redefined by stevev 23aug06 
						use ::>
class aCattrVarMinMaxList  : public aCattrBase
{	// int which with conditional expression  min (int)	
public:
	aCconditional/x*<aCminmaxList>*x/ condMinMaxVal;

	void clear(void){aCattrBase::clear();
				   condMinMaxVal.clear(); };
};
*** See correct definition above ****/
// VAR_MAX_VAL_ID			use aCattrVarMinMaxList
// VAR_SCALE_ID				use aCattrCondExpr
// VAR_INDEX_ITEM_ARRAY_ID	use aCattrCondReference
// VAR DEFAULT_VALUE		use aCattrCondExpr


/*================================  MENUES =================================*/

/*=*=*=*=*=*=*=*=*=*=* MENU SUPPORT TYPES  *=*=*=**=*=*=*=*=*=*=*=*/
// a menu entry
class aCmenuItem 
{
public:
	HEAPMARK;
	aCmenuItem(){MARKIT;};
	virtual ~aCmenuItem() 
	{ itemRef.destroy();}
	aCreference      itemRef;
	aCbitString      qualifier;	

	void clear(void){itemRef.clear();qualifier.clear();};
};
// a vector of menu entries
typedef vector<aCmenuItem>         AmenuItemList_t;

// a list ( or sublist) of menu entries
class aCmenuList : public aPayldType, public AmenuItemList_t // itemList;
{		 
public: 
	HEAPMARK;	
	aCmenuList():aPayldType(pltMenuList){MARKIT;};
	/* VMKP added on 0204040 */
	virtual ~aCmenuList(){		
		for(AmenuItemList_t::iterator iT = begin();iT<end();iT++){ iT->itemRef.destroy();} 
// WS - 9apr07 - VS2005 checkin
//		AmenuItemList_t::clear(); AmenuItemList_t::~vector<aCmenuItem>();
		// The following line is not obsolete in VS2005
		AmenuItemList_t::clear();
		
/*
    LINUX_PORT - REVIEW_WITH_HCF: Having to call STL container base class 
    destructor explicitly because they are not designed to be base classes 
    (no virtual destructor).
*/
		if ( size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
#if defined(__GNUC__)
			this->~AmenuItemList_t();
#else // !__GNUC__
			AmenuItemList_t::~vector();
#endif // __GNUC__
	};
	/* VMKP added on 0204040 */

	void clear(void) {
		FOR_this_iT(AmenuItemList_t,this) {iT->clear();}
		clear();         };
	//VMKP 260304
	void destroy(void){		
		for(AmenuItemList_t::iterator iT = begin();iT<end();iT++){ iT->itemRef.destroy();} 
		// for warning removal...27jul10...AmenuItemList_t::clear();  AmenuItemList_t::~vector<aCmenuItem>(); // stevev 09jul07 - try to plug a leak
		AmenuItemList_t::clear();
/*
    LINUX_PORT - REVIEW_WITH_HCF: It's not clear why 'destroy' should call 
    base class destructor. This object continues to exist until it is 
    deleted.
*/
#if !defined(__GNUC__)
		if ( size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
			AmenuItemList_t::~vector(); // stevev 09jul07 - try to plug a leak
	//VMKP 260304
#endif // __GNUC__
	};

};

typedef aCcondList/*<aCmenuList>*/	   AcondMenuList_t;

/*=*=*=*=*=*=*=*=* END MENU SUPPORT TYPES  *=*=*=**=*=*=*=*=*=*=*=*/

// MENU_LABEL_ID			use aCattrString

/****************** MENU ITEMS  **************************/
// menu item attribute supplying the list of items
class aCattrMenuItems : public aCattrBase
{
public:
	AcondMenuList_t condMenuListOlists;
	//									- aCcondList<CmenuList> AcondMenuList_t;

	void clear(void){aCattrBase::clear();
					 condMenuListOlists.clear();        };
	void destroy(void){
		condMenuListOlists.destroy();
	};
};
// MENU_HELP_ID			use aCattrString
// MENU_VALID_ID		use aCattrCondLong
// MENU_STYLE_ID		use aCattrCondLong



/*================================ COMMANDS ================================*/

/*=*=*=*=*=*=*=*=*=* COMMAND SUPPORT TYPES *=*=*=**=*=*=*=*=*=*=*=*/
/******************* COMMAND RESPONSE CODES  **********************/

class aCrespCode
{
public:
	HEAPMARK;
	unsigned long   val;
	unsigned long   type;
	aCddlString     descS;
	aCddlString     helpS;
	
	aCrespCode(){MARKIT;};
};

typedef vector<aCrespCode>         ArespCodesList_t;	

class aCrespCodeList : public aPayldType, public ArespCodesList_t //responseCodes;
{		 
public: 	
	HEAPMARK;
	aCrespCodeList():aPayldType(pltRespCdList){MARKIT;};
	//VMKP Added 020404
	~aCrespCodeList(){		
		for(ArespCodesList_t::iterator iT = begin();iT<end();iT++)
		{	iT->descS.destroy();iT->helpS.destroy();			 } 
		ArespCodesList_t::clear();
	};
	//VMKP Added 020404
	void destroy(void){		
		for(ArespCodesList_t::iterator iT = begin();iT<end();iT++)
		{	iT->descS.destroy();iT->helpS.destroy();			 } 
		ArespCodesList_t::clear();
	};
};

typedef aCcondList/*<aCrespCodeList>*/ AcondRespCodeL_t;

/******************* COMMAND DATA ITEMS  **************************/
class aCdataItem
{
public:
	ulong  type;
	ulong  flags;
	/// changed 27jul10 - stevev    was::>  ulong  width;
	UINT64 width;// made it unsigned 20aug10
	// value::> only one used
	ulong  iconst;
	float  fconst;
	aCreference		ref;
	aCdataItem(): fconst(0.00),type(0),flags(0),
		width(0),iconst(0)	{ref.clear();	};
	aCdataItem(const aCdataItem& r): fconst(r.fconst),type(r.type),flags(r.flags),
		width(r.width),iconst(r.iconst),ref(r.ref)	{	};
	virtual ~aCdataItem()
	{  ref.destroy(); }
};

typedef vector<aCdataItem>         AdataItemList_t;

class aCdataitemList : public aPayldType, public AdataItemList_t //dataItems;
{
public:
	aCdataitemList():aPayldType(pltDataItmLst){};
	aCdataitemList(const aCdataitemList& sl)
	{	loadtype = ((aPayldType *) &sl)->whatPayload();
		aCdataitemList * pDIL = (aCdataitemList *) &sl;
		for(AdataItemList_t::iterator iT = pDIL->begin(); iT != pDIL->end(); ++iT)
//		{	push_back(*((aCdataItem*)iT );  }	// PAW see below
		{	push_back(*((aCdataItem*)&(*iT)) );  } // PAW see above
	};
	//VMKP Added 020404
	virtual ~aCdataitemList()		
	{	for(AdataItemList_t::iterator iT = begin();iT<end();iT++){ iT->ref.destroy();} 
		AdataItemList_t::clear();
	};
	//VMKP Added 020404
	void destroy(void)
	{	for(AdataItemList_t::iterator iT = begin();iT<end();iT++){ iT->ref.destroy();} 
		AdataItemList_t::clear();
	};
};

typedef aCcondList/*<aCdataitemList>*/ AcondDataItemL_t;

/******************* COMMAND TRANSACTIONS  **************************/
class aCtransaction
{	
public:
	ulong             transNum;
	AcondDataItemL_t  request;
	AcondDataItemL_t  response;
	AcondRespCodeL_t  respCodes;
#ifdef XMTR
	aCreferenceList   postRqstRcvAction;
#endif
	void clear(void)
	{	transNum = 0;
		request.clear(); response.clear(); respCodes.clear();//deletes the contents
	};
	virtual ~aCtransaction()
	{ request.destroy(); response.destroy(); respCodes.destroy(); };
	aCtransaction(){ clear(); };
	aCtransaction(const aCtransaction& s)// stevev added 10jul07
		: request(s.request),response(s.response),respCodes(s.respCodes)
#ifdef XMTR
		,postRqstRcvAction( s.postRqstRcvAction )
#endif
	{	transNum  = s.transNum;
	};
};

typedef vector<aCtransaction>      AtransactionL_t;

/*=*=*=*=*=*=*=*=* END COMMAND SUPPORT TYPES *=*=**=*=*=*=*=*=*=*=*/


// COMMAND_NUMBER_ID		use aCattrCondLong
// COMMAND_OPER_ID			use aCattrCondLong
// COMMAND_TRANS_ID
/**************** COMMAND TRANSACTION  *******************/
class aCattrCmdTrans : public aCattrBase
{  
public:
	AtransactionL_t transactionList; // not conditional

	void clear(void){aCattrBase::clear();
		FOR_iT(AtransactionL_t, transactionList) {iT->clear();}
		transactionList.clear();         };
};

// COMMAND_RESP_CODES_ID
/************* COMMAND RESPONSE CODES  *******************/
class aCattrCmdRspCd : public aCattrBase
{
public:
	AcondRespCodeL_t   respCodes; 

	void clear(void){aCattrBase::clear();
					 respCodes.clear();        };
};


/*============================= EDIT DISPLAYS ==============================*/

// EDIT_DISPLAY_LABEL_ID		use aCattrString 
// EDIT_DISPLAY_EDIT_ITEMS_ID	use aCattrReferenceList
// EDIT_DISPLAY_DISP_ITEMS_ID	use aCattrReferenceList
// EDIT_DISPLAY_PRE_EDIT_ACT_ID use aCattrActionList
// EDIT_DISPLAY_POST_EDIT_ACT_IDuse aCattrActionList
// EDIT_DISPLAY_HELP_ID			use aCattrString
// EDIT_DISPLAY_VALID_ID		use aCattrCondLong



/*============================== ITEM ARRAYS ===============================*/

// ITEM_ARRAY_LABEL_ID		use aCattrString
// ITEM_ARRAY_HELP_ID		use aCattrString 
// ITEM_ARRAY_ELEMENTS_ID	use aCattrMemberList
// ITEM_ARRAY_VALIDITY_ID	use aCattrCondLong


/*============================== COLLECTIONS ===============================*/

// COLLECTION_LABEL_ID	use  aCattrString
// COLLECTION_HELP_ID   use  aCattrString 
// COLLECTION_MEMBERS_ID use aCattrMemberList


/*================================ RECORDS =================================*/
// RECORD_LABEL_ID		use aCattrString
// RECORD_HELP_ID		use aCattrString
// RECORD_MEMBERS_ID	use aCattrMemberList
// RECORD_RESP_CODES_ID use::>
class aCattrRecRspCd : public aCattrBase
{
public:
	AcondRespCodeL_t   respCodes; 

	void clear(void){aCattrBase::clear();
					 respCodes.clear();        };
};


/*============================== WRITE AS ONE ==============================*/
// WAO_ITEMS_ID		use aCattrReferenceList



/*============================== UNIT RELATIONS ============================*/
// UNIT_ITEMS_ID	

/***************** UNIT RELATION ITEMS  ********************/
class aCattrUnititems : public aCattrBase
{
public:
	aCconditional			condVar;
	AcondReferenceList_t	condListOUnitLists;

	void clear(void){aCattrBase::clear();
					 condListOUnitLists.clear();  condVar.clear();};
};


/*=========================== REFRESH RELATIONS ============================*/

// REFRESH_ITEMS_ID 
/***************** REFRESH RELATION ITEMS  ********************/
class aCattrRefreshitems : public aCattrBase
{
public:
	AcondReferenceList_t condWatchList;
	AcondReferenceList_t condUpdateList;

	void clear(void){aCattrBase::clear();
					 condWatchList.clear(); condUpdateList.clear(); };
};


/*================================ METHODS =================================*/

// METHOD_LABEL_ID  use aCattrString
// METHOD_CLASS_ID	use aCattrBitstring
// METHOD_HELP_ID	use aCattrString 
// METHOD_VALID_ID	use aCattrCondLong
// METHOD_SCOPE_ID	use aCattrBitstring

class aCattrMethodDef : public aCattrBase
{
public:
	char* pBlob;   // this class owns this memory
	int   blobLen;

	aCattrMethodDef() : aCattrBase() { pBlob=NULL; blobLen = 0;};
	~aCattrMethodDef(){RAZE_ARRAY(pBlob);};//via DD@F 16aug13
};



/*================================ ARRAYS ==================================*/

// ARRAY_LABEL_ID	use aCattrString 
// ARRAY_HELP_ID	use aCattrString
// ARRAY_VALID_ID	use aCattrCondLong
// ARRAY_TYPE_ID	use aCattrCondReference
// ARRAY_NUM_OF_ELEMENTS_ID  use  aCattrCondLong
//					is non conditional (now) defined in a conditional


/*================================== AXIS ==================================*/
// AXIS_LABEL_ID		use aCattrString 
// AXIS_HELP_ID			use aCattrString
// AXIS_VALID_ID		use aCattrCondLong
// AXIS_MINVAL_ID		use aCattrCondExpr
// AXIS_MAXVAL_ID		use aCattrCondExpr
// AXIS_SCALING_ID		use aCattrCondLong		- as an enum
// AXIS_CONSTUNIT_ID	use aCattrString

/*================================ CHARTS ==================================*/
// CHART_LABEL_ID		use aCattrString 
// CHART_HELP_ID		use aCattrString
// CHART_VALID_ID		use aCattrCondLong
// CHART_HEIGHT_ID		use aCattrCondLong	- as enum
// CHART_WIDTH_ID		use aCattrCondLong	- as enum
// NOTE: height & width are not conditional at this time.
//		 They are represented in a conditional structure anyway.
// CHART_TYPE_ID		use aCattrCondLong	- as enum - Must not be conditional
// CHART_LENGTH_ID		use aCattrCondExpr
// CHART_CYCLETIME_ID	use aCattrCondExpr
// CHART_MEMBERS_ID		use aCattrMemberList

/*================================= FILES ==================================*/
// FILE_MEMBERS_ID		use aCattrMemberList
// FILE_LABEL_ID		use aCattrString 
// FILE_HELP_ID			use aCattrString

/*================================ GRAPHS ==================================*/
// GRAPH_LABEL_ID	use aCattrString 
// GRAPH_HELP_ID	use aCattrString
// GRAPH_VALID_ID	use aCattrCondLong
// GRAPH_HEIGHT_ID	use aCattrCondLong	- as enum
// GRAPH_WIDTH_ID	use aCattrCondLong	- as enum
// GRAPH_XAXIS_ID	use	aCattrCondReference
// GRAPH_MEMBERS_ID	use aCattrMemberList


/*================================= LISTS ==================================*/
// LIST_LABEL_ID	use aCattrString 
// LIST_HELP_ID		use aCattrString
// LIST_VALID_ID	use aCattrCondLong
// LIST_TYPE_ID		use aCattrCondReference
// LIST_COUNT_ID	use aCattrCondExpr  - cond expr
// LIST_CAPACITY_ID	use aCattrCondLong	- non-cond in a integer conditional 

/*=============================== SOURCES ==================================*/
// SOURCE_LABEL_ID		use aCattrString 
// SOURCE_HELP_ID		use aCattrString
// SOURCE_VALID_ID		use aCattrCondLong
// SOURCE_EMPHASIS_ID	use aCattrCondLong		- a conditional BOOLEAN [ T || F ]
// SOURCE_LINETYPE_ID	use	aCattrCondIntWhich	- as an intWhich
// SOURCE_LINECOLOR_ID	use aCattrCondExpr		- as a 24 bit uint
// SOURCE_YAXIS_ID		use aCattrCondReference
// SOURCE_MEMBERS_ID	use aCattrMemberList

/*=============================== WAVEFORMS ================================*/
// WAVEFORM_LABEL_ID		use aCattrString
// WAVEFORM_HELP_ID			use aCattrString 
// WAVEFORM_HANDLING_ID		use aCattrBitstring
// WAVEFORM_EMPHASIS_ID		use aCattrCondLong		- a conditional BOOLEAN [ T || F ]
// WAVEFORM_LINETYPE_ID		use aCattrCondIntWhich	- as an intWhich  
// WAVEFORM_LINECOLOR_ID	use aCattrCondExpr		- as a 24 bit uint	
// WAVEFORM_YAXIS_ID		use	aCattrCondReference
// WAVEFORM_KEYPTS_X_ID		use aCattrReferenceList - there are constant references now...
// WAVEFORM_KEYPTS_Y_ID		use aCattrReferenceList - there are constant references now...		
// WAVEFORM_TYPE_ID			use aCattrCondLong		- as an enum   
// WAVEFORM_X_VALUES_ID		use aCattrReferenceList - there are constant references now...		
// WAVEFORM_Y_VALUES_ID		use aCattrReferenceList - there are constant references now...
// WAVEFORM_X_INITIAL_ID	use aCattrCondExpr
// WAVEFORM_X_INCREMENT_ID	use aCattrCondExpr
// WAVEFORM_POINT_COUNT_ID	use aCattrCondExpr
// WAVEFORM_INIT_ACTIONS_ID	use aCattrActionList
// WAVEFORM_RFRSH_ACTIONS_ID use aCattrActionList	
// WAVEFORM_EXIT_ACTIONS_ID	use aCattrActionList


/*=*=*=*=*=*=*=*=*=*=*=* END OF ATTRIBUTES *=*=*=*=**=*=*=*=*=*=*=*/

/* note that instance types are in dllinstance.h */

/*==================== BASE ITEM CLASS ===========================*/

// common to all items
// - an item is really just an identity and a list of attributes
class aCitemBase 
{
public:
	HEAPMARK;
	/* for unitialized read from purify */
	aCitemBase():itemId(0),itemType(0),itemSize(0),itemSubType(0),attrMask(0), isConditional(0) 
	{
		MARKIT;
		attrLst.clear();
	};

	virtual ~aCitemBase()
	{	// HOMZ - port to 2003
#ifdef _DEBUG
		long listSize = (long)attrLst.size();// WS - 9apr07 - VS2005 checkin
#endif
		// #define FOR_iT(lt,lst)  for(lt::iterator iT = (lst).begin();iT<(lst).end();iT++)
		// FOR_iT(AattributeList_t, attrLst)
                for ( std::vector<aCattrBase*>::iterator iT = attrLst.begin(); iT < attrLst.end(); iT++)
                {
                    //			if ( (iT != NULL) && (*iT != NULL) )// PAW see below
                    if ( (&(*iT) != NULL) && (*iT != NULL) )// PAW see above
                    {
                        // HOMZ - For _MSC_VER 1300+, a CRASH was occuring here because this item has already been deleted in
                        //        releaseItem( *iT)  (called in releaseDev).  In releaseItem, the function
                        //        releaseOneAttrib is called, and deletes this item (making it invalid here).
                        // The aCitemBase has a member AattributeList_t attrLst and has a member whose address is 0xfeeefeee
                        //(*iT)->clear();
                    }
                    //CPMHACK:
                    if((*iT) != NULL)
                    {
                        RAZE(*iT);
                    }
                }  // FOR_iT
	};

	void clear()
	{ 
#ifdef _DEBUG
		long listSize = (long)attrLst.size();// WS - 9apr07 - VS2005 checkin
#endif
		FOR_iT(AattributeList_t, attrLst)
		{  
// stevev 20feb07 - check all the time  #if _MSC_VER >= 1300  // HOMZ - Resolve Memory Leaks
			if (*iT != NULL) // HOMZ
//#endif
			{
				(*iT)->clear(); 
			}
		} 
	};

	// ddKey is redundant - has been removed
	ITEM_ID         itemId;
	CitemType       itemType;

	ITEM_SIZE       itemSize;
	CitemType		itemSubType;
	UINT32          attrMask;   // one bit for each attribute in attrLst

	//UINT32			ddRef;		/* DD_REFERENCE - for dds compatability - removed*/
	string          itemName;
	
/* VMKP added on 291203 */
	bool			isConditional;
/* VMKP added on 291203 */

	// main data::				// vector of aCattrBase*
	AattributeList_t attrLst;	// if memory ownership removed, set ptr to NULL

	aCattrBase*     getaAttr(unsigned int attrType)
	{
		ulong m = maskFromInt(attrType);
		FOR_iT(AattributeList_t, attrLst)
		{ 
			if ((*iT) && (*iT)->attr_mask == m) return (*iT);
		}
		/*else */  return NULL;
	};
};


// vector of pointers so we can cast to actual type
typedef vector<aCitemBase*>		   AitemList_t;

/*====================== IMAGE SUPPORT ===========================*/
typedef struct imgframe_s
{
	unsigned int   size;		// in bytes
	unsigned char* pRawFrame;	// pointer to the raw image
	unsigned int   offset;		// for reference
	char           language[6]; // only the first 3 are used
} imgframe_t;

typedef vector<imgframe_t>    AframeList_t;
typedef vector<AframeList_t>  AimageList_t;

/*=================== END IMAGE SUPPORT ==========================*/

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*  DEVICE *=*=*=*=*=**=*=*=*=*=*=*=*=*/

//   a device is just an identity and a list of items
class aCdevice
{
public:
	// stevev 27aug10   unsigned long   DDkey;		// who  - the DDkey value <0xffffffff means no key>
	UINT64          DDkey;
	AitemList_t     AitemPtrList;
	/* 10/13/04 stevev added device overview */
	AnIdList_t      CritItemList;
	/* we'll add the item2command list later */
	AimageList_t    AimageList; // holds pointers to real DATA (only item that does)
};
/*=*=*=*=*=*=*=*=*=*=*=*=*=* END OF DEVICE *=*=*=**=*=*=*=*=*=*=*=*/


//VMKP 260304
inline void aCgenericConditional::aCexpressDest::destroy()
{
	RAZE(pCondDest);
	if ( pPayload == NULL ) return;
	payloadType_t loadtype = pPayload->whatPayload();

	switch(loadtype)
	{
		case pltddlStr:
			((aCddlString *)pPayload)->destroy();
			delete 	((aCddlString *)pPayload);
			break;
		case pltddlLong:
			((aCddlLong *)pPayload)->destroy();
			delete ((aCddlLong *)pPayload);
			break;
		case pltbitStr:
			((aCbitString *)pPayload)->destroy();
			delete (( aCbitString*)pPayload);
			break;
		case pltEnumList:
			((aCenumList *)pPayload)->destroy();
			delete ((aCenumList *)pPayload);
			break;
		case pltDataItmLst:
			((aCdataitemList *)pPayload)->destroy();
			delete ((aCdataitemList *)pPayload);
			break;
		case pltMenuList:
			((aCmenuList *)pPayload)->destroy();
			delete ((aCmenuList *)pPayload);
			break;
		case pltRespCdList:
			((aCrespCodeList *)pPayload)->destroy();
			delete ((aCrespCodeList *)pPayload);
			break;
//		case pltEdDispLst:
//			((aCreferenceList *)pPayload)->destroy();
			break;
		case pltGroupItemList:
			((aCmemberElementList *)pPayload)->destroy();
			delete ((aCmemberElementList *)pPayload);
			break;
//		case pltCollMemList:
//			((aCCollectionMemberList *)pPayload)->destroy();
//			break;
//		case pltWrtAoneList:
//			((aCWaOList *)pPayload)->destroy();
//			break;
//		case pltUnitList:
//			((aCunitList *)pPayload)->destroy();
//			break;
		case pltReferenceList:
			((aCreferenceList *)pPayload)->destroy();
			delete ((aCreferenceList *)pPayload);
			break;
		case pltReference:
			((aCreference *)pPayload)->destroy();
			delete ((aCreference *)pPayload);
			break;
		case pltExpression:
			((aCExpressionPayload *)pPayload)->destroy();
			delete ((aCExpressionPayload *)pPayload);
			break;
		case pltMinMax:
			((aCminmaxList *)pPayload)->destroy();
			delete ((aCminmaxList *)pPayload);
			break;
//		case pltMemListOLists:
//			((aCminmaxList *)pPayload)->destroy();
//			break;
		case pltIntWhich:
			((aCintWhich *)pPayload)->destroy();
			delete ((aCintWhich *)pPayload);
			break;
		case pltGridMemberList:	// stevev 07dec05
			((aCgridMemberList *)pPayload)->destroy();
			delete ((aCgridMemberList *)pPayload);
			break;
		default:
			break;
	}

/*	delete pPayload; */
	pPayload = NULL;
	destExpression.destroy();

};
//VMKP 260304



#endif // _DLLAPI_H


/*************************************************************************************************
 *
 *   $History: dllapi.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */


