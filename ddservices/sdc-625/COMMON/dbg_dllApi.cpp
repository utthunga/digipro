/*************************************************************************************************
 *
 * $Workfile: dbg_dllApi.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		a debugging tool... otherwise this file is empty - dump capabilities...
 *		
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003 
 */

// stevev 20feb07 - merge- noway...use the general #include "stdafx.h"   /// HOMZ - port to 2003, VS 7

#ifdef _DEBUG
// debugging tool... otherwise this file is empty

#include "dllapi.h"
#include "ddbdefs.h"
//#include "Dict.h"
#include "ddb.h"



#ifdef COUNT_MEMORY
unsigned maxSizeElem = 0;
map <void*,int> exElemMap;
void UNREGCHUNK(void* p)
{
	if ( ! p ) return;
	map <void*,int>::iterator miT;
	miT = exElemMap.find( p );
	if ( miT == exElemMap.end() )
	{
		LOGIT(CLOG_LOG,"EXELEM: Unreg With No Reg.\n 0x%p\n",p);
	}
	else // found
	{
		exElemMap.erase(miT);
	}
};
void REGISCHUNK(void* p, int src)
{
	exElemMap[ p ]=src;
	if (exElemMap.size() > maxSizeElem )
		maxSizeElem = exElemMap.size();
};
void DUMPCHUNK(void)
{
	map <void*,int>::iterator miT;
	for (miT = exElemMap.begin();miT != exElemMap.end();++miT)
	{
		LOGIT(CLOG_LOG,"EXELEM: Registered %p from %d\n",miT->first, miT->second);
	}
};

void CHECKCHUNK(void* p)
{
	if ( ! p ) return;
	map <void*,int>::iterator miT;
	miT = exElemMap.find( p );
	if ( miT != exElemMap.end() )
	{
		int p = miT->second;// for a breakpoint
	}
	// else it has never been registered
};

#endif 


/* Prototypes */
void dumpDev(aCdevice* pDev);
void dumpItem(aCitemBase* paItm);
void dumpAttr(CitemType& itmType, aCattrBase* paAttr);

void dumpReference(aCreference* pR, int indentLen=0);
void dumpCondList(aCcondList* paCondLst, int indentLen=0);
void dumpCond(aCconditional* paCond, int indentLen=0);

void dumpExpr(aCexpression* paExpr, int indentLen=0);
void dumpMinMaxList(AMinMaxList_t *pL,int indentLen=0);
void dumpDDLstring(aCddlString* ps, int indentLen=0 );
void dumpOutStatus(outStatusList_t* pOclassList,int indentLen=0);

void dumpVarAttr(varAttrType_t vatrT,aCattrBase* paAttr);
void dumpCmdAttr(cmdAttrType_t cmdrT,aCattrBase* paAttr);
void dumpMenuAttr(menuAttrType_t attrType, aCattrBase* paAttr);
void dumpEdDispAttr(eddispAttrType_t attrType, aCattrBase* paAttr);
void dumpMethodAttr(methodAttrType_t attrType, aCattrBase* paAttr);
void dumpRefreshAttr(refreshAttrType_t attrType, aCattrBase* paAttr);
void dumpUnitAttr(unitAttrType_t attrType, aCattrBase* paAttr);
void dumpWaOAttr(waoAttrType_t attrType, aCattrBase* paAttr);
//void dumpItmArrAttr(itemarrayAttrType_t attrType, aCattrBase* paAttr);
//void dumpCollAttr(collAttrType_t attrType, aCattrBase* paAttr);
void dumpGrpItmAttr(grpItmAttrType_t attrType, aCattrBase* paAttr);
void dumpArrayAttr(arrayAttrType_t attrType, aCattrBase* paAttr);
void dumpDebug( aCdebugInfo* paAttr, int indentLen=0);
void dumpVectors( aCgridMemberList* paAttr, int indentLen);


void dumpChartAttr(chartAttrType_t catrT,aCattrBase* paAttr);
void dumpGraphAttr(graphAttrType_t gatrT,aCattrBase* paAttr);
void dumpAxisAttr(axisAttrType_t aatrT,aCattrBase* paAttr);
void dumpWaveformAttr(waveformAttrType_t watrT,aCattrBase* paAttr);
void dumpSourceAttr(sourceAttrType_t satrT,aCattrBase* paAttr);
void dumpListAttr(listAttrType_t catrT,aCattrBase* paAttr);

/* stevev 1Apr05 */
void dumpImageAttr(imageAttrType_t attrType,aCattrBase* paAttr);
void dumpGridAttr(gridAttrType_t attrType,aCattrBase* paAttr);
/* end stevev */

varAttrType_t getVarAttrType(ulong aMask);
genericTypeEnum_t getAttrType4Mask(CitemType& itmType, ulong aMask);


extern char varTypeStrings[VARTYPESTRCNT]  [VARTYPESTRMAXLEN];
extern char varAttrStrings[VARATTRSTRCOUNT][VARATTRSTRMAXLEN];
extern wchar_t varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN];
extern char axiomStrings  [5]              [AXIOMSTRMAXLEN]  ;
extern char clauseStrings [8]              [CLAUSESTRMAXLEN] ;
extern char payloadStrings[PAYLDSTRCOUNT]  [PAYLDSTRMAXLEN]  ;
extern char refTypeStrings[REFSTRCOUNT]    [REFMAXLEN]       ;
extern char cmdAttrStrings[CMDATRSTRCOUNT] [CMDATRMAXLEN];
extern char  menuAttrStrings[MENUATRSTRCOUNT] [MENUATRMAXLEN] ;
extern char edDispTypeStrings[EDDISPSTRCOUNT][EDDISPMAXLEN] ;
extern char methodTypeStrings[METHODSTRCOUNT][METHODMAXLEN];
extern char refreshTypeStrings[1][15];
extern char unitTypeStrings[1][20];
extern char waoTypeStrings[1][20];
//extern char collAttrStrings[COLLATRSTRCOUNT] [COLLATRMAXLEN];
//extern char itmArrAttrStrings[ITMARRATRSTRCOUNT] [ITMARRATRMAXLEN];
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];
extern char exprElemStrings[EXPELEMSTRCNT][EXPELEMSTRMAXLEN];
extern char arrAttrStrings[ARRATRSTRCOUNT] [ARRATRMAXLEN];
extern char fileAttrStrings[FILEATRSTRCOUNT] [FILEATRMAXLEN];
extern char chartAttrStrings[CHRTATRSTRCOUNT] [CHRTATRMAXLEN];
extern char graphAttrStrings[GRPHTATRSTRCOUNT] [GRPHTATRMAXLEN];
extern char axisAttrStrings[AXISTATRSTRCOUNT] [AXISTATRMAXLEN];
extern char waveformAttrStrings[WAVEFORMTATRSTRCOUNT] [WAVEFORMTATRMAXLEN];
extern char sourceAttrStrings[SOURCETATRSTRCOUNT] [SOURCETATRMAXLEN];
extern const char listAttrStrings[LISTTATRSTRCOUNT] [LISTTATRMAXLEN];

#if _MSC_VER < 1400	/* these are defined in VS 2005 */
extern std::ostream& operator<<(std::ostream& os, __int64 i );
extern std::ostream& operator<<(std::ostream& os, UINT64 i );
#endif


void dumpParam(aCparameter* paAttr, int indentLen);
void dumpParamList(aCparameterList* paAttr,int indentLen);


#ifndef USEDEFAULT
#define USEDEFAULT          L"USEDFLT"
#endif

#define SP_ACE "    "
// ulong daDDkey = 0;
UINT64 daDDkey = 0;

void dumpDev(aCdevice* pDev)
{
	daDDkey = pDev->DDkey;
//string localS;
//if (SUCCESS != procureStringByDouble (daDDkey, 0, 0, USEDEFAULT, &localS) )
//{localS = "Not found in the dictionary.";
//}
//cout<<" Test String ZERO:"<<localS<<"|"<<endl;


	cout << ">Device:     DDkey: 0x" <<hex<< pDev->DDkey <<dec<<endl;
	cout << ">Device: ItemCount: " << pDev->AitemPtrList.size() <<endl;

		/* Arrange all the items in ascending order according to the Item IDs */
//		unsigned int PrintItem = 96;
		aCitemBase *swapItem;
		AitemList_t::iterator itr1;
		AitemList_t::iterator itr2;
		AattributeList_t::iterator itr3;
		AattributeList_t::iterator itr4;

		aCattrBase *swapaCattr;

		for(itr1 = (pDev->AitemPtrList).begin();itr1<(pDev->AitemPtrList).end();itr1++)
		{
			for(itr2 = itr1 + 1;itr2<(pDev->AitemPtrList).end();itr2++)
			{
				if((*itr2)->itemId  < (*itr1)->itemId)
				{
					swapItem = *itr1;
					*itr1 = *itr2;
					*itr2 = swapItem;
					
					for(itr3 = ((*itr1)->attrLst).begin();itr3<((*itr1)->attrLst).end();itr3++)
					{
						for(itr4 = itr3 + 1;itr4<((*itr1)->attrLst).end();itr4++)
						{
							if((*itr4) != NULL && (*itr3) != NULL )
							{
								if((*itr4)->attr_mask  < (*itr3)->attr_mask)
								{
									swapaCattr = *itr3;
									*itr3 = *itr4;
									*itr4 = swapaCattr;
								}
							}
							else
							{
								cout<<"***************** ERROR null attribute pointer ***************"<<endl;
							}
						}
					}
				}
			}
		}

	FOR_iT(AitemList_t,pDev->AitemPtrList)
	{//iT is a ptr2ptr to aCitemBase
		dumpItem(*iT);
		cout << endl;
	}

}


void dumpItem(aCitemBase* paItm)
{
	cout << SP_ACE << ">Item:      itemId	: 0x" <<hex<< paItm->itemId  <<dec       << endl;
	cout << SP_ACE << ">Item:    itemType	: " << paItm->itemType.getTypeStr()    <<"  (" << paItm->itemType.getType() << ")"<<endl;
	cout << SP_ACE << ">Item:    itemSize	: " << paItm->itemSize       << endl;
	cout << SP_ACE << ">Item: itemSubType	: " << paItm->itemSubType.getTypeStr() <<"  (" << paItm->itemSubType.getType() << ")"<<endl;
	cout << SP_ACE << ">Item:    attrMask	: 0x" <<hex<< paItm->attrMask   <<dec    << endl;
	cout << SP_ACE << ">Item:    itemName	: " << paItm->itemName       << endl;
	cout << SP_ACE << ">Item:Is Conditional	: " << paItm->isConditional << endl;
	cout << SP_ACE << ">Item:AttributeCnt	: " << paItm->attrLst.size() << endl;

	FOR_iT(AattributeList_t,paItm->attrLst)
	{//iT is a ptr2ptr2 aCattrBase;
		//if (((paItm->itemId)>>4) == 0x8000ff0  &&  (*iT)->attr_mask == VAR_LABEL  )
		//{
		//	cout<<" MungeLabel ";
		//}
		dumpAttr(paItm->itemType,*iT);
	}
}

#define COUTATTRTYPE(k)  cout << SP_ACE << SP_ACE << ">Attribute      Type: " << k << endl

void dumpAttr(CitemType& itmType, aCattrBase* paAttr)
{
	cout << SP_ACE << SP_ACE << ">Attribute: BaseMask: 0x"<< hex << paAttr->attr_mask  <<dec << endl;
	genericTypeEnum_t git = getAttrType4Mask(itmType, paAttr->attr_mask);
	char* pStr= NULL;

	switch((itemType_t)itmType)
	{
	case iT_Variable:
		{
			COUTATTRTYPE(varAttrStrings[git.gAttrVar]);
 			dumpVarAttr(git.gAttrVar, paAttr);
		}
		break;
	case iT_Command:
		{
			COUTATTRTYPE(cmdAttrStrings[git.gAttrCmd]);
 			dumpCmdAttr(git.gAttrCmd, paAttr);
		}
		break;
	case iT_Menu:
		{
			COUTATTRTYPE( menuAttrStrings[git.gAttrMenu]);
			dumpMenuAttr(git.gAttrMenu, paAttr);
		}
		break;
	case iT_EditDisplay:
		{
			COUTATTRTYPE( edDispTypeStrings[git.gAttrEdDisp]);
			dumpEdDispAttr(git.gAttrEdDisp, paAttr);
		}
		break;
	case iT_Method:
		{
			COUTATTRTYPE( methodTypeStrings[git.gAttrMethod]);
			dumpMethodAttr(git.gAttrMethod, paAttr);
		}
		break;
	case iT_Refresh:
		{
			COUTATTRTYPE( refreshTypeStrings[git.gAttrRefresh]);
			dumpRefreshAttr(git.gAttrRefresh, paAttr);
		}
		break;
	case iT_Unit:	
		{
			COUTATTRTYPE( unitTypeStrings[git.gAttrUnit]);
			dumpUnitAttr(git.gAttrUnit, paAttr);
		}
		break;
	case iT_WaO:	
		{
			COUTATTRTYPE( waoTypeStrings[git.gAttrWao]);
			dumpWaOAttr(git.gAttrWao, paAttr);
		}
		break;
	case iT_ItemArray:	
		{
			COUTATTRTYPE( grpItmAttrStrings[git.gAttrGrpItm]);
			dumpGrpItmAttr(git.gAttrGrpItm, paAttr);
		}
		break;
	case iT_Collection:		
		{
			COUTATTRTYPE( grpItmAttrStrings[git.gAttrGrpItm]);
			dumpGrpItmAttr(git.gAttrGrpItm, paAttr);
		}
		break;
	case iT_Array:	
		{
			COUTATTRTYPE( arrAttrStrings[git.gAttrArray]);
			dumpArrayAttr(git.gAttrArray, paAttr);
		}
		break;




	case iT_Block:
		COUTATTRTYPE("Block Unsupported");
		break;
	case iT_Record:
		COUTATTRTYPE("Record Unsupported");
		break;
	case iT_ResponseCodes://??
		COUTATTRTYPE("Response Codes Unsupported");
		break;
	case iT_Member:	//??
		// unsupported
		COUTATTRTYPE("Member Unsupported");
		break;

	case iT_File:				//20
		{
			COUTATTRTYPE( fileAttrStrings[git.gAttrGrpItm]);
			dumpGrpItmAttr(git.gAttrGrpItm, paAttr);
		}
		break;
	case iT_Chart:				//21
		{
			COUTATTRTYPE( chartAttrStrings[git.gAttrChart]);
			dumpChartAttr(git.gAttrChart, paAttr);
		}
		break;
	case iT_Graph:				//22
		{
			COUTATTRTYPE( graphAttrStrings[git.gAttrGraph]);
			dumpGraphAttr(git.gAttrGraph, paAttr);
		}
		break;
	case iT_Axis:				//23
		{
			COUTATTRTYPE( axisAttrStrings[git.gAttrAxis]);
			dumpAxisAttr(git.gAttrAxis, paAttr);
		}
		break;
	case iT_Waveform:			//24
		{
			COUTATTRTYPE( waveformAttrStrings[git.gAttrWaveform]);
			dumpWaveformAttr(git.gAttrWaveform, paAttr);
		}
		break;
	case iT_Source:				//25
		{
			COUTATTRTYPE( sourceAttrStrings[git.gAttrSource]);
			dumpSourceAttr(git.gAttrSource, paAttr);
		}
		break;
	case iT_List:				//26
		{
			COUTATTRTYPE( listAttrStrings[git.gAttrList]);
			dumpListAttr(git.gAttrList, paAttr);
		}
		break;
	case iT_Grid:				//27
		{
			COUTATTRTYPE( gridAttrStrings[git.gAttrGrid]);
			dumpGridAttr(git.gAttrGrid, paAttr);
		}
		break;
	case iT_Image:				//28
		{
			COUTATTRTYPE( imageAttrStrings[git.gAttrImage]);
			dumpImageAttr(git.gAttrImage, paAttr);
		}
		break;

	case iT_ReservedZeta:
	case iT_ReservedOne:
	case iT_Program:
	case iT_VariableList:
	case iT_Domain:
	case iT_MaxType:
	case iT_FormatObject:
	case iT_DeviceDirectory:
	case iT_BlockDirectory:
	case iT_Parameter:
	case iT_ParameterList:
	case iT_BlockCharacteristic:
	default:
		// erroneous
		COUTATTRTYPE("Erroneous");
		break;
	}// end switch


	cout << endl;
}

#define COUTSPC(c) for(i=0;i<c;i++){cout<<SP_ACE;}



void dumpReference(aCreference* pR, int indentLen)
{
	int i;
	if (pR->rtype.getType() == MAX_REFTYPE_ENUM && pR->id==0 && pR->type.getType() == 0 && pR->subindex == 0 && pR->pExpr == NULL)
	{
		COUTSPC(indentLen);cout << "Reference:    No Reference" << endl;
		return;
	}
	// else do the whole thing
	COUTSPC(indentLen);cout << "Reference:    type:" << pR->rtype.getTypeStr() << endl;
	COUTSPC(indentLen);cout << "Reference: " << ((pR->isRef)? "is a Ref":"is a ID" ) << endl;
	COUTSPC(indentLen);cout << "Reference:  id val:0x" <<hex<< pR->id<<dec << endl;
	COUTSPC(indentLen);cout << "Reference: id type:" << pR->type.getTypeStr() << endl;
	COUTSPC(indentLen);cout << "Reference:subindex:" << pR->subindex << endl;
	COUTSPC(indentLen);cout << "Reference:   iname:0x" <<hex<< pR->iName<<dec << endl;
	
	axiomType_t axT = axiomFrom(pR->exprType);
	COUTSPC(indentLen);cout << "Reference:   Axiom:" << axiomStrings[axT] << endl;
	//if ( axT != aT_Unknown )
	//{
		if ( pR->pExpr != NULL )
		{
			dumpExpr(pR->pExpr, indentLen+1);
		}
		else if (axT != aT_Unknown)
		{
			COUTSPC(indentLen);cout << "***ERROR Non Empty expression type with no expression ptr."<< endl;
		}
		else //- we have no expression
		{
			COUTSPC(indentLen);cout << "Reference: no Expression attached." << endl;
		}
	//}
	if ( pR->pRef != NULL )
	{
		COUTSPC(indentLen);cout << "Reference: Has a reference::> "<< endl;
		dumpReference(pR->pRef, indentLen+1);
	}
}


void dumpExpr(aCexpression* paExpr, int indentLen)
{
	int i, y = 1;;
	COUTSPC(indentLen);
	cout << "Expression: has "<< paExpr->expressionL.size() << " Elements (in Reverse-Polish-Notation order)"<<endl;

	FOR_iT(RPNexpress_t,paExpr->expressionL)
	{// iT points to a aCexpressElement
		COUTSPC(indentLen);cout << "Expression "   << y   << " element type: "<<exprElemStrings[(int)(iT->exprElemType)] << endl;
		COUTSPC(indentLen);cout << "Expression "   << y   << "        value: "<< iT->expressionData << endl;
		COUTSPC(indentLen);cout << "Expression "   << y++ << "   Dependency: "<< endl;
		
		COUTSPC(indentLen+1);cout << "Dependency " << "   Arith Option: "<< ARITHSTR(iT->dependency.useOptionVal) << endl;
		COUTSPC(indentLen+1);cout << "Dependency " << "      Which one: "<< (unsigned long)(iT->dependency.which) << endl;// cast added PAW
		dumpReference(&(iT->dependency.depRef),indentLen+2);
	}
	//cout << endl;// empty space
}

void dumpParam(aCparameter* paAttr, int indentLen)
{	
	cout << " "   << methodVarTypeStrings[paAttr->type] ;
	cout << "   '"   << paAttr->paramName << "'";

	if (paAttr->modifiers)
	{
		if (paAttr->modifiers & CONSTFLAG)
		{
			cout << "   - Const - ";
		}
		if (paAttr->modifiers & REFECFLAG)
		{
			cout << "   - As Reference - ";
		}
		if (paAttr->modifiers & ARRAYFLAG)
		{
			cout << "   - As Array - ";
		}
	}
	cout << endl;
		
}
void dumpParamList(aCparameterList* paAttr,int indentLen)
{
	int i,y;

	if ( paAttr->size() )
	{	y = 1;
		for(AparamList_t::iterator iT = paAttr->begin();iT<paAttr->end();iT++)
		{
			COUTSPC(indentLen);cout << "Parameter " <<y++<<")";

#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7 - error C2440: 'type cast' : cannot convert from...
			dumpParam(&(*iT), indentLen + 3);
#else
			dumpParam((aCparameter*) iT, indentLen + 3);
#endif
		} 
	}// else keep going
}

/* stevev 23aug06 - min-max no longer a payload 
*void dumpMinMaxList(AMinMaxList_t *pL,int indentLen)
*{
*	int i;
*
*	for(AMinMaxList_t::iterator ix = pL->begin();ix<pL->end();ix++)
*	{//ix is a ptr2 AMinMaxList_t
*		COUTSPC(indentLen);cout << "Which " << ix->whichVal << endl;
*		COUTSPC(indentLen);cout << "Value: " << endl;
*		dumpExpr(&ix->pExpr,indentLen+1);
*	}
*}
*/

void dumpDDLstring(aCddlString* ps, int indentLen )
{
	int i;
	wchar_t* pName = L"String";
	wstring wrkStr;
	if (ps->theStr.empty() && ps->ddItem != 0)
	{
		wstring localS;
		wstring*  pStr = NULL;
		wrkStr = ps->theStr;
// try this::
		if ( ps->strTag == 0xffff)
		{			
			if (SUCCESS == procureStringBySingleRemote (ps->ddItem, 0, USEDEFAULT, &pStr/*&localS*/) )
			{
				localS = (*pStr);
				//ps->theStr = "'"+localS+"'(*)";// looked up value
				//wrkStr = "'"+localS+"'(*)";// looked up value
				wrkStr = L"'"+localS+L"'(*)";// looked up value
				releaseString(&pStr);
			}
			else
			{
				//ps->theStr = "'  [not in Sdictionary]  '";
				wrkStr = L"'  [not in Sdictionary]  '";
			}
		}
		else
		{
			if (SUCCESS == procureStringByDoubleRemote (daDDkey, ps->ddItem, 0, USEDEFAULT, &pStr/*&localS*/) )
			{
				localS = (*pStr);
				//ps->theStr = "'"+localS+"'(*)";// looked up value
				wrkStr = L"'"+localS+L"'(*)";// looked up value
				releaseString(&pStr);
			}
			else
			{
				//ps->theStr = "'  [not in Ddictionary]  '";
				wrkStr = L"'  [not in Ddictionary]  '";
			}
		}
	}
	COUTSPC(indentLen);cout<<">"<<pName<<": String '";
#ifndef _WIN32_CE	
	wcout<<ps->theStr;
	cout<<"'" <<endl;	
#else // is CE
	cout<<TStr2AStr(ps->theStr) <<"'" <<endl;	
#endif	
//	COUTSPC(indentLen);cout<<">"<<pName<<": String '"<<wrkStr <<"'" <<endl;	
	COUTSPC(indentLen);cout<<">"<<pName<<":    len " <<ps->len    <<endl;		
	COUTSPC(indentLen);cout<<">"<<pName<<":  flags "<<hex<<ps->flags<<dec<<endl;		
	COUTSPC(indentLen);cout<<">"<<pName<<":    tag 0x"<<hex<<ps->strTag <<dec<<" ("<<ps->strTag<<")"<<endl;		
	COUTSPC(indentLen);cout<<">"<<pName<<":    key 0x"<<hex<<ps->ddKey  <<dec<<" ("<<ps->ddKey<<")" <<endl;		
	COUTSPC(indentLen);cout<<">"<<pName<<":   item 0x"<<hex<<ps->ddItem <<dec<< endl;		
	COUTSPC(indentLen);cout<<">"<<pName<<":EnumVal "<<ps->enumVal <<endl;
	if (ps->pRef != NULL)
	{
		dumpReference(ps->pRef,indentLen+1);
	}
	else
	{
		COUTSPC(indentLen);
		cout << ">"<<pName<<": -No reference included-"<<endl;
	}
}

void dumpOutStatus(outStatusList_t* pOclassList,int indentLen)
{
	int i,p = 1;
//	COUTSPC(indentLen);    cout<<"# Out Status List has " << pOclassList->size() << " elements."<<endl;
	for(outStatusList_t::iterator ix = pOclassList->begin();ix<pOclassList->end();ix++)
	{//ix is a ptr2 outputStatus_t
		COUTSPC(indentLen+1);cout<<"#"<< p <<"   kind:"<<hex<<ix->kind<<dec   << ((ix->kind == 1)?" DV":((ix->kind==2)?" TV":((ix->kind==3)?" AO":""))) << endl;
		COUTSPC(indentLen+1);cout<<"#"<< p <<"  which:"<<hex<<ix->which<<dec  << endl;
		COUTSPC(indentLen+1);cout<<"#"<<p++<<" oclass:"<<hex<<ix->oclass<<dec << ((ix->oclass == 1)?" MANUAL & GOOD":((ix->oclass==2)?" AUTO & BAD":((ix->oclass==3)?" MANUAL & BAD":" AUTO & GOOD"))) << endl;
	}

}
void dumpPayload(aPayldType* pP, int indentLen=0, char* pName = "Payload")
{
	int i;
	COUTSPC(indentLen);cout<<">"<<pName<<": ("<<payloadStrings[pP->whatPayload()]<<")" << endl;
	switch(pP->whatPayload())
	{
	case pltddlStr:
		{
			dumpDDLstring((aCddlString*) pP, indentLen );			
		}
		break;
	case pltddlLong:
		{
			aCddlLong* pl = (aCddlLong*)pP;
			COUTSPC(indentLen);cout<<">"<<pName<<":  long " <<pl->ddlLong    <<endl;	
		}
		break;
	case pltbitStr:
		{
			aCbitString* pbs = (aCbitString*)pP;
			COUTSPC(indentLen);cout<<">"<<pName<<":bitString 0x" <<hex<<pbs->bitstringVal<<dec <<endl;	
		}
		break;
	case pltEnumList:
		{	
			aCenumList* pL = (aCenumList*)pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Enum Descriptors." << endl;
			FOR_p_iT(AenumDescList_t,pL)
			{// iT ptr2 aCenumDesc
				COUTSPC(indentLen);cout<<">Enum Value:"<<iT->val<<endl;

				dumpPayload(((aPayldType*) &(iT->func_class)), indentLen+1,"func  class");
				dumpPayload(((aPayldType*) &(iT->descS)),      indentLen+1,"description");
				dumpPayload(((aPayldType*) &(iT->helpS)),      indentLen+1,"help string");
				COUTSPC(indentLen+1);cout<<">actions:"<<endl;
				dumpReference(&(iT->actions), indentLen+2);

				COUTSPC(indentLen+1);cout<<">status class:0x"<<hex<<iT->status_class<<dec<<endl;

				if (iT->oclasslist.size() > 0 )
				{
					COUTSPC(indentLen+1);cout<<">Output Class List has "<<iT->oclasslist.size()<<" elements."<<endl;
					dumpOutStatus(&(iT->oclasslist),indentLen+2);	
				}
				else
				{
					COUTSPC(indentLen+1);cout<<">Output Class List is empty."<<endl;
				}

			}// next iT
		}
		break;
	case pltDataItmLst:
		{
			aCdataitemList* pL = (aCdataitemList*)pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Data Items." << endl;
			FOR_p_iT(AdataItemList_t,pL)
			{// iT ptr2 aCdataItem				
				COUTSPC(indentLen);cout<<">"<<pName<<":   type "<< iT->type  <<endl;
				COUTSPC(indentLen);cout<<">"<<pName<<":  flags "<<hex<<iT->flags<<dec<<endl;				
				COUTSPC(indentLen);cout<<">"<<pName<<":  width "<<iT->width<<endl;				
				COUTSPC(indentLen);cout<<">"<<pName<<": iconst "<<setw(8)<<iT->iconst <<" OR fconst "<< iT->fconst <<endl;		

				COUTSPC(indentLen+1);cout<<">Reference:"<<endl;
				dumpReference(&(iT->ref), indentLen+2);
			}
		}
		break;
	case pltMenuList:
		{
			int y = 1;
			aCmenuList* pL = (aCmenuList*)pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Menu Items" << endl;
			FOR_p_iT(AmenuItemList_t,pL)
			{// iT ptr2 aCmenuItem	
				dumpPayload(((aPayldType*) &(iT->qualifier)),      indentLen+1,"Qualifier");
				COUTSPC(indentLen+1);cout<<">Menu Item "<<y++<<" :"<<endl;
				dumpReference(&(iT->itemRef), indentLen+2);
			}
		}
		break;
	case pltRespCdList:
		{
			aCrespCodeList* pL = (aCrespCodeList*)pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Response Codes." << endl;
			FOR_p_iT(ArespCodesList_t,pL)
			{// iT ptr2 aCdataItem	
				COUTSPC(indentLen);cout<<">"<<pName<<":  value 0x"<<hex<< iT->val <<dec<<"  ("<<iT->val<<")" <<endl;
				COUTSPC(indentLen);cout<<">"<<pName<<":   type 0x"<<hex<< iT->type <<dec<<"  ("<<iT->type<<")    <";
				switch(iT->type)
				{
				case 1:	cout << "success";				break;
				case 2:	cout << "misc-warning";			break;
				case 3:	cout << "data-entry-warning";	break;
				case 4:	cout << "data-entry-error";		break;
				case 5:	cout << "mode-error";			break;
				case 6:	cout << "process-error";		break;
				case 7:	cout << "misc-error";			break;
				default:	cout << "Undefined Type!";	break;
				}
				cout<<">"<<endl;
				dumpPayload(((aPayldType*) &(iT->descS)),      indentLen+1,"description");
				dumpPayload(((aPayldType*) &(iT->helpS)),      indentLen+1,"help string");
			}
		}
		break;
	case pltGroupItemList:
		{
			int y = 1;
			aCmemberElementList* pL = (aCmemberElementList*) pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Members/Elements Items" << endl;
			FOR_p_iT(AmemberElemList_t,pL)
			{// iT ptr2 aCmemberElement	
				COUTSPC(indentLen);cout<<y++  << "  index: " << iT->locator << endl;
				dumpReference(&(iT->ref), indentLen+1);
				dumpPayload(((aPayldType*) &(iT->descS)),      indentLen+1,"description");
				dumpPayload(((aPayldType*) &(iT->helpS)),      indentLen+1,"help string");
			}
		}
		break;
	case pltReferenceList:
		{
			int y = 1;
			aCreferenceList* pL = (aCreferenceList*)pP;
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Reference Items." << endl;
			FOR_p_iT(AreferenceList_t,pL)
			{// iT ptr2 aCreference	
				COUTSPC(indentLen);cout<<y++<<endl;
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				dumpReference(&(*iT), indentLen+1);
#else
				dumpReference(iT, indentLen+1);
#endif
			}
		}
		break;
	case pltReference:
		{
			aCreference* pL = (aCreference*)pP;
			dumpReference(pL, indentLen+1);
		}
		break;
	case pltExpression:
		{
			aCexpression* pL = &((aCExpressionPayload*)pP)->theExpression;
			dumpExpr(pL, indentLen+1);
		}
		break;
	case pltMinMax:
		{
		/* stevev 23aug06 - no longer a payload
		  *	aCminmaxList* pL = (aCminmaxList*) pP;
		  *	COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Min-Max Items" << endl;
		  *	dumpMinMaxList((AMinMaxList_t *)pL,indentLen+1);
		  */
		}
		break;
	case pltGridMemberList:
		{
			int y = 1;
			aCgridMemberList* pL = (aCgridMemberList*) pP;
			
			COUTSPC(indentLen);cout<<">Payload: has "<<pL->size()<<" Grid Members" << endl;
			FOR_p_iT(AGridMemberElemList_t,pL)
			{// iT ptr2 aCgridMemberElement	
				COUTSPC(indentLen);cout<<y++  << "  index: " << y++ << endl;
				dumpPayload(((aPayldType*) &(iT->header)), indentLen+1,"Header");
				dumpPayload(((aPayldType*) &(iT->refLst)), indentLen+1, "Value List");
			}
		}
		break;
	case pltUnknown:
	case pltLastLdType:
	default:
		COUTSPC(indentLen);cout<<">"<<pName<<":  *** Illegal Payload Type." <<endl;	
	}//endswitch
}

void dumpConditional(aCondDestType*   pCondDest,int indentLen=0)
{
	if (pCondDest->isAlist())
	{
		dumpCondList( (aCcondList*) pCondDest, indentLen +1);
	}
	else
	{
		dumpCond(((aCconditional*)pCondDest), indentLen+1);		
	}
}

void dumpCondDest(aCgenericConditional::aCexpressDest* paeD,int indentLen=0)
{
	int i;
	COUTSPC(indentLen);

	clauseType_t cT = clauseFrom(paeD->destType);
	cout << "Destination: "<< clauseStrings[cT] << endl;
	if (cT == cT_CASE_expr)
	{
		dumpExpr(&(paeD->destExpression), indentLen+1);
	}
	else
	{
		COUTSPC(indentLen);
		cout << "Destination: -No CASE expression required-"<<endl;
	}

	if ( paeD->pCondDest != NULL)
	{//aCondDestType*   pCondDest
		dumpConditional(paeD->pCondDest,indentLen);
		if (paeD->pPayload != NULL)
		{	COUTSPC(indentLen);
		   cout << "ERROR: has BOTH conditional Destination and payload destination." << endl;
		}
	}
	else if (paeD->pPayload != NULL)
	{//aPayldType*  payloadStrings   whatPayload
		dumpPayload(paeD->pPayload , indentLen+1);
	}
	else
	{
		cout << "ERROR: has NO conditional Destination and NO payload destination." << endl;
	}
}

void dumpGCond(aCgenericConditional* paCond, int indentLen=0)
{
	int i;

	axiomType_t axT = axiomFrom(paCond->priExprType);
	COUTSPC(indentLen);cout << "Conditional: Axiom: " << axiomStrings[axT] << endl;
	if ( axT > aT_Unknown && axT < aT_Direct )
	{
		dumpExpr(&(paCond->priExpression),indentLen+1);
	}// else - no expression required
	COUTSPC(indentLen);
	cout << "Conditional:       "<< paCond->destElements.size() <<" Destination List Elements"<<endl;

	FOR_iT(aCgenericConditional::dest_List,paCond->destElements)
	{// iT points to a aCgenericConditional::aCexpressDest
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7 - Remove error C2664 - cannot convert parameter 1 from ...*
		dumpCondDest(&(*iT), indentLen+1);
#else
		dumpCondDest(iT,indentLen+1);
#endif
	}
	
	cout << endl;// empty space
}

void dumpCond(aCconditional* paCond, int indentLen)
{
	int i;
	if (paCond->isAlist())
	{
		COUTSPC(indentLen);
		cout << "***ERROR*** conditional called for a cond LIST."<<endl;
		return;
	}// else continue

	dumpGCond((aCgenericConditional*) paCond, indentLen);
}

void dumpCondList(aCcondList* paCondLst, int indentLen)// number of leading SP_ACE 's
{
	int i;
	if ( ! paCondLst->isAlist())
	{
		COUTSPC(indentLen);
		cout << "***ERROR*** Conditional List called for a conditional."<<endl;
		return;
	}// else continue
	COUTSPC(indentLen);cout<<">Conditional List has "<< paCondLst->genericlist.size() << " Generic Conditionals in it."<<endl;

	FOR_iT(aCcondList::listOconds_t, paCondLst->genericlist)
	{// iT isa ptr2 aCcondList::oneGeneric_t which isa aCgenericConditional
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7 - Remove error C2664 - cannot convert parameter 1 from ...
		dumpGCond(&(*iT), indentLen+1);
#else
		dumpGCond(iT, indentLen+1);
#endif
	}
}


void dumpMethod( char* pBlob,  int len )
{
	cout << pBlob << endl;//"+++++ Method text goes here ++++++" << endl;
}

void dumpCmdAttr(cmdAttrType_t cmdrT,aCattrBase* paAttr)
{
	int i;
	switch(cmdrT)
	{		
	case cmdAttrNumber:
		COUTSPC(3);cout<<">      Command Number:";
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);		
		break;
	case cmdAttrOperation:
		COUTSPC(3);cout<<">   Command Operation:";
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),  3);	
		break;
	case cmdAttrTransaction:
		{
			FOR_iT(AtransactionL_t,(((aCattrCmdTrans*)paAttr)->transactionList))
			{//iT pts 2 aCtransaction
				COUTSPC(3);cout<<">  Transaction Number:"<< iT->transNum <<endl;
				COUTSPC(3);cout<<">Transaction Requests:" <<endl;
				dumpCondList(&(iT->request),  3+1);
				COUTSPC(3);cout<<">Transaction Responses:" <<endl;
				dumpCondList(&(iT->response), 3+1);
				COUTSPC(3);cout<<">Transaction Response Codes:" <<endl;
				dumpCondList(&(iT->respCodes), 3+1);
#ifdef XMTRDD
				if ( iT->postRqstRcvAction.size() > 0 )
				{
					COUTSPC(3);cout<<">Transaction Post-Request Receive Actions:" <<endl;
					dumpPayload((aPayldType*)(&(iT->postRqstRcvAction)), 3+1, "Post Request Actions");
				}
#endif
			}
		}
		break;
	case cmdAttrRespCodes:
		dumpCondList(&(((aCattrCmdRspCd*)paAttr)->respCodes),  3);	
		break;
	case cmdAttrDebugInfo:		
		dumpDebug((aCdebugInfo*)paAttr, 3);		
		break;
	case cmdAttrUnknown:
	default:
		COUTSPC(3);cout<<"***ERROR: Unknown Command Attribute type. (0x"<<hex<<cmdrT<<dec<<")"<<endl;
		break;
	}
}

void dumpMenuAttr(menuAttrType_t attrType, aCattrBase* paAttr)
{
	int i = 0;
	switch(attrType)
	{	
	case menuAttr_Label	:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case menuAttr_Items :;
		dumpCondList(&(((aCattrMenuItems*)paAttr)->condMenuListOlists), 3);
		break;
	case menuAttr_Help:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case menuAttr_Validity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);		
		break;
	case menuAttr_Style:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);		
		break;
	case menuAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}
void dumpEdDispAttr(eddispAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{		
	case eddispAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case eddispAttrEditItems:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);
		break;
	case eddispAttrDispItems:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);
		break;
	case eddispAttrPreEditAct:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);	
		break;
	case eddispAttrPostEditAct:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);	
		break;		
	case eddispAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);		
		break;		
	case eddispAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);			
		break;	
	case eddispAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;	
	default:
		break;
	}//endswitch
}
void dumpMethodAttr(methodAttrType_t attrType, aCattrBase* paAttr)
{
	int i;
	switch(attrType)
	{
	case methodAttrClass:
		dumpCond(&(((aCattrBitstring*)paAttr)->condBitStr), 3);	
		break;
	case methodAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);
		break;
	case methodAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case methodAttrDefinition:
		dumpMethod( ((aCattrMethodDef*)paAttr)->pBlob,  ((aCattrMethodDef*)paAttr)->blobLen );	
		break;
	case methodAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);	
		break;
	case methodAttrScope:
		dumpCond(&(((aCattrBitstring*)paAttr)->condBitStr), 3);	
		break;
	case methodAttrType:
		COUTSPC(8);
		dumpParam((aCparameter*)paAttr, 3);
		break;
	case methodAttrParams:
		dumpParamList((aCparameterList*)paAttr,3);
		break;
	case methodAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);	
		break;
	default:
		break;
	}//endswitch
}


void dumpDebug( aCdebugInfo* paAttr, int indentLen)
{
	int i;

	COUTSPC(indentLen);cout << "    Name:'" << paAttr->symbolName << "'"<<endl;;
	COUTSPC(indentLen);cout << "    Line: " << paAttr->lineNumber <<endl;;
	COUTSPC(indentLen);cout << "FileName:" << endl;;
	dumpDDLstring(&(paAttr->fileName), indentLen + 1);

	if (paAttr->attrs.size() > 0)
	{
		AdebugAttrList_t::iterator it;
		for ( it = paAttr->attrs.begin(); it != paAttr->attrs.end();++it)
		{
			COUTSPC(indentLen+1);cout << "Attribute: 0x" <<hex<<it->attrTag << dec<<endl;;
			COUTSPC(indentLen+1);cout << "     Line: " << it->lineNumber <<endl;;
			COUTSPC(indentLen+1);cout << " FileName:" << endl;;
			dumpDDLstring(&(it->fileName), indentLen + 2);
			if (it->memList.size() > 0 )
			{
				AdebugMemberList_t::iterator IT;
				int mCnt = 0;
				for ( IT = it->memList.begin(); IT != it->memList.end();++IT)
				{
					COUTSPC(indentLen+2);
						cout << "           Member: ("<<mCnt<<") 0x" <<hex<<IT->memberValue<< dec<<endl;
					COUTSPC(indentLen+2);
						cout << "Member SymbolName: " << IT->symbolName <<endl;
				}// next member
			}
		}// next attr
	}
}

void dumpVectors( aCattrGridMemberList* paAttr, int indentLen)
{
	int /*i,*/y=0,k=0;

	dumpCondList(&(paAttr->condGridMemberListOlists), 3);

/*	if (paAttr != NULL && paAttr->size() > 0)
	{
		AGridMemberElemList_t::iterator it;
		for ( it = paAttr->begin(); it != paAttr->end();++it)
		{
			COUTSPC(indentLen);cout << "Vector: " << k++ << endl;;
			dumpDDLstring(&(it->header),indentLen);
			FOR_iT(AreferenceList_t, (it->refLst) )
			{// iT ptr2 aCreference	
				COUTSPC(indentLen+1);cout<<"Element: "<<y++<<endl;
				dumpReference(iT, indentLen+2);
			}
		}// vector
	}// else - no content
*/
}

void dumpRefreshAttr(refreshAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{		
	case refreshAttrItems:
		dumpCondList(&(((aCattrRefreshitems*)paAttr)->condWatchList), 3);
		dumpCondList(&(((aCattrRefreshitems*)paAttr)->condUpdateList), 3);
		break;	
	case refreshAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}
void dumpUnitAttr(unitAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{		
	case unitAttrItems:
		dumpCond(&(((aCattrUnititems*)paAttr)->condVar), 3);
		dumpCondList(&(((aCattrUnititems*)paAttr)->condListOUnitLists), 3);	
		break;	
	case unitAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}
void dumpWaOAttr(waoAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{		
	case waoAttrItems:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);
		break;	
	case waoAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}

void dumpGrpItmAttr(grpItmAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{		
	case grpItmAttrElements:
		dumpCondList(&(((aCattrMemberList*)paAttr)->condMemberElemListOlists), 3);
		break;
	case grpItmAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case grpItmAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case grpItmAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),  3);	
		break;
	case grpItmAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}

void dumpArrayAttr(arrayAttrType_t attrType, aCattrBase* paAttr)
{
	switch(attrType)
	{	
	case arrayAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case arrayAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case arrayAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),     3);	
		break;	
	case arrayAttrType:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);
		break;
	case arrayAttrElementCnt:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),     3);
		break;
	case arrayAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;
	}//endswitch
}

void dumpVarAttr(varAttrType_t vatrT,aCattrBase* paAttr)
{
	switch(vatrT)
	{		
	case varAttrClass:
	case varAttrHandling:
	case varAttrTimeScale:			// added 03jun08
		dumpCond(&(((aCattrBitstring*)paAttr)->condBitStr), 3);	
		break;

	case varAttrConstUnit:
	case varAttrLabel:
	case varAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case varAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong),       3);	
		break;
	case varAttrTypeSize:
		if (((aCattrTypeType*)paAttr)->notCondTypeType.actualVarType >=0 && 
			((aCattrTypeType*)paAttr)->notCondTypeType.actualVarType < VARTYPESTRCNT)
		{
		cout << SP_ACE << SP_ACE << SP_ACE << ">Variable Type: " << \
				varTypeStrings[((aCattrTypeType*)paAttr)->notCondTypeType.actualVarType] << endl;
		}
		else
		{
		cout << SP_ACE << SP_ACE << SP_ACE << ">Variable Type: " << \
				"UNKNOWN ("<<hex<< ((aCattrTypeType*)paAttr)->notCondTypeType.actualVarType<<dec <<")"<< endl;
		}
		cout << SP_ACE << SP_ACE << SP_ACE << ">Variable Size: " << \
				((aCattrTypeType*)paAttr)->notCondTypeType.actualVarSize << endl;
		break;
	case varAttrEnums:
		dumpCondList(&(((aCattrEnum*)paAttr)->condEnumListOlists), 3);		
		break;

//	case varAttrRdTimeout:
//	case varAttrWrTimeout:
//		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);
//		break;

	case varAttrWidth:
	case varAttrHeight:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);
		break;

	case varAttrPreReadAct:
	case varAttrPostReadAct:
	case varAttrPreWriteAct:
	case varAttrPostWriteAct:
	case varAttrPreEditAct:
	case varAttrPostEditAct:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;

	case varAttrResponseCodes:
		break;// an error

	case varAttrDisplay:
	case varAttrEdit:
	case varAttrTimeFormat:			// added 03jun08
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;

	case varAttrMinValue:
		//dumpCond(&(((aCattrVarMinMaxList*)paAttr)->condMinMaxVal), 3);
		{
			aCminmaxList* pMML = &(((aCattrVarMinMaxList*)paAttr)->ListOminMaxElements);
			for( ITminMaxList_t it = pMML->begin(); it != pMML->end(); ++it)
			{// ptr 2a aCminmaxVal
				int i; COUTSPC(3);
				cout<<"MinValue"<<it->whichVal <<endl;
				dumpCond(&(it->condExpr),6);
			}
		}
		break;
	case varAttrMaxValue:
		//dumpCond(&(((aCattrVarMinMaxList*)paAttr)->condMinMaxVal), 3);
		{
			aCminmaxList* pMML = &(((aCattrVarMinMaxList*)paAttr)->ListOminMaxElements);
			for( ITminMaxList_t it = pMML->begin(); it != pMML->end(); ++it)
			{// ptr 2a aCminmaxVal
				int i; COUTSPC(3);
				cout<<"MaxValue"<<it->whichVal <<endl;
				dumpCond(&(it->condExpr),6);
			}
		}	
		break;
	case varAttrScaling:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case varAttrIndexItemArray:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);	
		break;
	case varAttrDefaultValue:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);
		break;
		
	case varAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	case varAttrRefreshAct:
	case varAttrPostRequestAct:
	case varAttrPostUserAct:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);
		break;

	case varAttrLastvarAttr	:
	default:
		break;;
	}//endswitch
}



void dumpChartAttr(chartAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case chartAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case chartAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case chartAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;
	case chartAttrHeight:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case chartAttrWidth:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum
		break;
	case chartAttrType:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case chartAttrLength:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case chartAttrCycleTime:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case chartAttrMembers:
		dumpCondList(&(((aCattrMemberList*)paAttr)->condMemberElemListOlists), 3);
		break;
	case chartAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}

void dumpGraphAttr(graphAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case graphAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case graphAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case graphAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;
	case graphAttrHeight:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case graphAttrWidth:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case graphAttrXAxis:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);
		break;
	case graphAttrMembers:
		dumpCondList(&(((aCattrMemberList*)paAttr)->condMemberElemListOlists), 3);
		break;
	case graphAttrCycleTime:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);
		break;
	case graphAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}

void dumpAxisAttr(axisAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case axisAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case axisAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case axisAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// an error	
		break;
	case axisAttrMinVal:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case axisAttrMaxVal:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case axisAttrScale:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case axisAttrUnit:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case axisAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}

void dumpWaveformAttr(waveformAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case waveformAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case waveformAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case waveformAttrHandling:
		dumpCond(&(((aCattrBitstring*)paAttr)->condBitStr), 3);	
		break;
	case waveformAttrEmphasis:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case waveformAttrLineType:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case waveformAttrLineColor:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case waveformAttrYAxis:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);
		break;
	case waveformAttrKeyPtX:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrKeyPtY:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrType:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case waveformAttrXVals:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrYVals:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrXinitial:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case waveformAttrXIncr:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case waveformAttrPtCount:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case waveformAttrInitActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrRfshActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrExitActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case waveformAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	case waveformAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);		
		break;
	default:
		break;;
	}//endswitch
}

void dumpSourceAttr(sourceAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case sourceAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case sourceAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case sourceAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;
	case sourceAttrEmphasis:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case sourceAttrLineType:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case sourceAttrLineColor:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case sourceAttrYAxis:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);
		break;
	case sourceAttrMembers:
		dumpCondList(&(((aCattrMemberList*)paAttr)->condMemberElemListOlists), 3);
		break;
	case sourceAttrInitActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case sourceAttrRfshActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case sourceAttrExitActions:
		dumpCondList(&(((aCattrReferenceList*)paAttr)->RefList), 3);		
		break;
	case sourceAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}

void dumpListAttr(listAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case listAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case listAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case listAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;
	case listAttrType:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);
		break;
	case listAttrCount:
		dumpCond(&(((aCattrCondExpr*)paAttr)->condExpr), 3);	
		break;
	case listAttrCapacity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);
		break;
	case listAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}


void dumpGridAttr(gridAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case gridAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case gridAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case gridAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;
	case gridAttrHeight:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case gridAttrWidth:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum
		break;
	case gridAttrOrient:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// enum	
		break;
	case gridAttrHandling:
		dumpCond(&(((aCattrBitstring*)paAttr)->condBitStr), 3);	
		break;
	case gridAttrMembers:
/*TODO:  this encoding is WRONG!!!!!!!!!!!!!!!!!!!!!!!!*************/
//		dumpCondList(&(((aCattrMemberList*)paAttr)->condMemberElemListOlists), 3);	
		dumpVectors((aCattrGridMemberList*)paAttr, 3);
		break;
	case gridAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}



void dumpImageAttr(imageAttrType_t attrType,aCattrBase* paAttr)
{
	switch(attrType)
	{
	case imageAttrLabel:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case imageAttrHelp:
		dumpCond(&(((aCattrString*)paAttr)->condString), 3);	
		break;
	case imageAttrValidity:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);	
		break;

	case imageAttrLink:
		dumpCond(&(((aCattrCondReference*)paAttr)->condRef), 3);// ref/string
		break;
	case imageAttrPath:
		dumpCond(&(((aCattrCondLong*)paAttr)->condLong), 3);// index	
		break;
	case imageAttrDebugInfo:
		dumpDebug((aCdebugInfo*)paAttr, 3);
		break;
	default:
		break;;
	}//endswitch
}

#endif // _DEBUG

/*************************************************************************************************
 *
 *   $History: dbg_dllApi.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/Common
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
