/*************************************************************************************************
*
* Workfile: globals.h
* 28mar16 - stevev
**
*************************************************************************************************
* The content of this file is the
*     Proprietary and Confidential property of the HART Communication Foundation
* Copyright (c) 2016, FieldComm Group, All Rights Reserved
*************************************************************************************************
*
* Description:
*		1/27/2016 	sjv	created to handle Thread Local Storage, specifically for the new Parser
*
*
* #include "globals.h"
*/
#pragma once
#ifndef _GLOBALS_H
#define _GLOBALS_H

#ifdef INC_DEBUG
#pragma message("In globals.h") 
#endif

#include <sys/types.h>
#include <sys/timeb.h>
#if _MSC_VER >=1900
#include <mutex>
#endif
#include "TLStorage.h"
#include "FMx_Dict.h"
#ifdef MEMSNAPS

#include <crtdbg.h>
#endif
#ifdef INC_DEBUG
#pragma message("    Finished Includes::globals.h") 
#endif

extern unsigned mainThreadID;
extern unsigned currentThreadID(void);
class FMx_Dict;
class FM8_Dict;
class FMx;

class parseGlbl
{
public:
	unsigned  threadID;
	long long ddKey;
	int       Q_and_A;
	FMx_Dict* StringDict;
	FMx_Dict* DictionaryDict;
	wchar_t * pFilename;// do not delete, points to arg[1]
	FMx     * pFile;

#ifdef PROFILE
	struct _timeb baseTime, lastTime;
#endif

public:
	parseGlbl():threadID(0),ddKey(0) {};
	void initialize(void) 
	{
		threadID   = currentThreadID();
		StringDict = DictionaryDict = NULL;
		pFilename  = NULL;
		pFile = NULL;
		Q_and_A = 0;
#ifdef PROFILE
		memset(&baseTime, 0, sizeof(struct _timeb));
		memset(&lastTime, 0, sizeof(struct _timeb));
#endif
	};
};

#define TLSsize  ( sizeof parseGlbl )
extern void* TLSmainThread;

extern FM8_Dict *pStandardDICTSTR;	// holds entire standard dictionary

#ifdef _DEBUG
 extern volatile parseGlbl* pTLS;

 #define THREAD_ID   ((pTLS=(parseGlbl*)getTLS())->threadID)
 #define DD_KEY      ((pTLS=(parseGlbl*)getTLS())->ddKey)
 #define pLIT_STR    ((pTLS=(parseGlbl*)getTLS())->StringDict)
 #define pDICTSTR    ((pTLS=(parseGlbl*)getTLS())->DictionaryDict)
 #define pWfileNAME  ((pTLS=(parseGlbl*)getTLS())->pFilename)
 #define  Q_AND_A    ((pTLS=(parseGlbl*)getTLS())->Q_and_A)
 #define  pFILE		 ((pTLS=(parseGlbl*)getTLS())->pFile)

#define pWrkLIT_STR    ((pTLS=(parseGlbl*)TLSmainThread)->StringDict)
#define pWrkDICTSTR    ((pTLS=(parseGlbl*)TLSmainThread)->DictionaryDict)
 #ifdef PROFILE
  #define LASTTIME    ((pTLS=(parseGlbl*)getTLS())->lastTime)
  #define BASETIME    ((pTLS=(parseGlbl*)getTLS())->baseTime)
 #endif
#else
#define THREAD_ID   (((parseGlbl*)getTLS())->threadID)
#define DD_KEY      (((parseGlbl*)getTLS())->ddKey)
#define pLIT_STR    (((parseGlbl*)getTLS())->StringDict)
#define pDICTSTR    (((parseGlbl*)getTLS())->DictionaryDict)
#define pWfileNAME  (((parseGlbl*)getTLS())->pFilename)
#define  Q_AND_A    (((parseGlbl*)getTLS())->Q_and_A)


#define  pFILE		(((parseGlbl*)getTLS())->pFile)

#define pWrkLIT_STR    (((parseGlbl*)TLSmainThread)->StringDict)
#define pWrkDICTSTR    (((parseGlbl*)TLSmainThread)->DictionaryDict)


#endif

#define  IS_MAIN_THREAD (THREAD_ID == mainThreadID)



// PROFILE is expected in the build preprocessor defs
#ifdef PROFILE
extern mutex fmaTimeMtx;
#define startPROFILE(s) {struct _timeb myTm;char* S = s;fmaTimeMtx.lock(); _ftime_s( &myTm );\
			LOGIT(CLOG_LOG, "%s [%6d] Start Timing.\n",s,THREAD_ID);\
		memcpy(&LASTTIME, &myTm, sizeof(struct _timeb));\
		memcpy(&BASETIME, &myTm, sizeof(struct _timeb));   fmaTimeMtx.unlock(); }

#define logPROFILE(s)  {struct _timeb myTm;char* S = s;fmaTimeMtx.lock(); _ftime_s( &myTm );\
		int eMillitime = myTm.millitm - BASETIME.millitm;\
		INT64 eTime    = myTm.time    - BASETIME.time;\
		if (eMillitime < 0) {	eMillitime+=1000; eTime -= 1;}\
		int sMillitime = myTm.millitm - LASTTIME.millitm;\
		INT64 sTime    = myTm.time    - LASTTIME.time;\
		if (sMillitime < 0) {	sMillitime+=1000; sTime -= 1;}\
		if (S == NULL) S = " ";\
		LOGIT(CLOG_LOG,"%s [%6d]Elapsed time = \t%I64d.%03d \tsecs \tSplit: \t%I64d.%03d\n",\
								s,THREAD_ID,eTime,eMillitime,  sTime,sMillitime);\
		memcpy(&LASTTIME, &myTm, sizeof(struct _timeb));   fmaTimeMtx.unlock(); }
#else
#define logPROFILE(s)  
#define startPROFILE(S)
#endif // PROFILE

#ifdef _DEBUG
extern struct _timeb mainLast;
#define mainStartPROFILE(s) {char* S = s; _ftime_s( &mainLast );\
			LOGIT(CLOG_LOG, "    Main Start Timing.\n",THREAD_ID); startPROFILE( s ) ; }
#define mainEndPROFILE(s) { logPROFILE(s); struct _timeb myTm;char* S = s; _ftime_s( &myTm );\
		int eMillitime = myTm.millitm - mainLast.millitm;\
		INT64 eTime    = myTm.time    - mainLast.time;\
		if (eMillitime < 0) {	eMillitime+=1000; eTime -= 1;}\
		if (S == NULL) S = " ";\
		LOGIT(CLOG_LOG,"    Main Elapsed time = \t%I64d.%03d \tsecs\n",eTime,eMillitime ); }
 #ifdef MEMSNAPS
  extern _CrtMemState ms[16];
  extern _CrtMemState diffstate;
  #define TakeSNAP( a )   _CrtMemCheckpoint( &(ms[a]) );
  #define DiffSNAP( a, b )   _CrtMemCheckpoint( &(ms[b]) );\
	  if ( _CrtMemDifference( &diffstate, &(ms[a]), &(ms[b]) ) )  _CrtMemDumpStatistics( &diffstate );
  #define NewSnap( a )     _CrtMemCheckpoint( &(ms[a]) );_CrtMemDumpStatistics( &(ms[a]) );
 #else
  #define StartSNAP( a )
  #define NewSnap( a )
  #define TakeSNAP( a )
  #define DiffSNAP( a, b )
 #endif
#else
  #define mainStartPROFILE(s) 
  #define mainEndPROFILE(s)
  #define TakeSNAP( a )
  #define DiffSNAP( a, b )
  #define NewSnap( a ) 
  #define TakeSNAP( a )
  #define DiffSNAP( a, b )
  #define NewSnap( a ) 
#endif

#endif //_GLOBALS_H