//#include <options.h>
#include "sign.h"
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#ifndef IS_SDC 
#include <err_gen.h>
#endif

static unsigned long crc;	// current checksum

// stevev 29mar11 - for Tim - this is CRC-32 IEEE 802.3 
// polynomial coeffs
// [0] for production
// [1] for debug

static unsigned long p[2] = {0x04C11DB7, 0x401CD17B} ;


static int firstblocksize = 27; // first 27 bytes of file are data
								// 28th byte is CRC checksum

static void init_signature()
{ 
	crc = 0xFFFFFFFF; 
}

// hash n bytes of data using key, return 4 byte signature
// you can call once or multiple times

/*********************************************************************
 *
 *  Name: get_signature
 *
 *  ShortDesc: create 4-byte digest of buffer area
 *
 *  Description:
 *      Form a digital signature from a n byte buffer
 *
 *  Inputs:
 *      buffer to be scanned
 *		flg == 1, debug crc, flg == 0, production crc
 *
 *  Outputs:
 *      crc holds the digest
 *
 *  Returns:
 *      crc
 *
 *  Author:
 *      Tim Johnston
 *
 ********************************************************************/

static unsigned long get_signature(const void* data, unsigned int count, int flag)
{
	const unsigned char* ptr;
	unsigned long poly;

	ptr = (const unsigned char *) data;

	// select correct polynomial
	if (flag)
		poly = p[1];
	else
		poly = p[0];

	while (count--)
	{
		int i;
		unsigned char value = *ptr++;

		crc ^= ((unsigned long)value << 24);

		for (i = 0; i < 8; i++)
		{
			if (crc & 0x80000000)
				crc = (crc << 1) ^ poly;

			else
				crc <<= 1;
		}
	}

	return crc;
}


/*********************************************************************
 *
 *  Name: signfile
 *
 *  ShortDesc: stamp file with 4-byte file digest
 *
 *  Description:
 *      Form a digital signature from a DD binary
 *		apply it to the file
 *
 *  Inputs:
 *      path to file to be stamped
 *		flg == 1, debug crc, flg == 0, production crc
 *
 *  Outputs:
 *      None.
 *
 *  Returns:
 *      true if successful, false message if not
 *
 *  Author:
 *      Tim Johnston
 *
 ********************************************************************/

bool signfile(char *path, int flg)
{
	verifyfile(path, flg);	// calculates crc as a byproduct

	// open DDOD file to be signed
	// NOTE:  "rb+" mode is essential to get file opened for 
	//	binary writing without creating the file and allowing fseek
	//	to reposition properly
	FILE *fp = fopen(path, "rb+");
	if (!fp)
	{
#ifndef IS_SDC 
		fatal(105, path);
#endif
		return false;
	}

	// seek to 4 byte area we want to write to
	if (fseek(fp, firstblocksize, SEEK_SET))
	{
#ifndef IS_SDC 
		fatal(101, path);
#endif
		fclose(fp);
		return false;
	}

	if (fwrite(&crc, 4, 1, fp) != 1)
	{
#ifndef IS_SDC 
		fatal(102, path);
#endif
		fclose(fp);
		return false;
	}

	fclose(fp);
	
	return true;
}


/*********************************************************************
 *
 *  Name: verifyfile
 *
 *  ShortDesc: verify that DD binary matches the 4-byte file digest
 *
 *  Description:
 *      Form a digital signature from a DD binary
 *		check the result against the 4 byte value stored in the file
 *
 *  Inputs:
 *      path to file to be checked
 *		flg == 1, debug crc, flg == 0, production crc
 *
 *  Outputs:
 *      global crc contains checksum value.
 *
 *  Returns:
 *      true if successful, false message if not
 *
 *  Author:
 *      Tim Johnston
 *
 ********************************************************************/

bool verifyfile(char *path, int flg)
{
	unsigned char *buf;				//  entire file store at this memory location
	long flen;				// length of file
	unsigned long checksum;	// crc value stored in file;
#ifndef _WIN32_WCE	
	unsigned long *pchecksum;// causes a data type alignment error PAW 29/05/09
#endif	
	FILE *fp;
	
	// open DDOD file to be verified
	fp = fopen(path, "rb");
	if (!fp)
	{
#ifndef IS_SDC 
		fatal(105, path);
#endif
		return false;
	}

	// find file length in bytes
	if (fseek(fp, 0, SEEK_END))
	{
#ifndef IS_SDC 
		fatal(101, path);
#endif
		fclose(fp);
		return false;
	}

	flen = ftell(fp);

	if (fseek(fp, 0, SEEK_SET))
	{
#ifndef IS_SDC 
		fatal(101, path);
#endif
		fclose(fp);
		return false;
	}


	// read entire file into RAM
	buf = (unsigned char *)malloc(flen+100);
	if (buf == NULL)
	{
#ifndef IS_SDC 
		fatal(100);
#endif
		fclose(fp);
		return false;
	}

	if (fread(buf, flen, 1, fp) != 1)
	{
#ifndef IS_SDC 
		fatal(103, path);
#endif
		fclose(fp);
		return false;
	}
	fclose(fp);

	// we form the signature on two blocks of data, surrounding
	// the stored signature itself, which is necessarily excluded


#ifndef _WIN32_WCE		// PAW causes datatype alignment error 29/05/09
	pchecksum = (unsigned long *)(buf + firstblocksize);
	checksum = *pchecksum;
#else
	unsigned char *pchecksum;		
	unsigned char checksum_byte;	
	pchecksum = (unsigned char *)(buf + firstblocksize);
	checksum = 0;
	checksum_byte = *(pchecksum++);
	checksum += checksum_byte;				// LSB
	checksum_byte = *(pchecksum++);
	checksum += checksum_byte << 8;
	checksum_byte = *(pchecksum++);
	checksum += checksum_byte << 16;
	checksum_byte = *(pchecksum++);
	checksum += checksum_byte<<24;			// MSB
#endif

	init_signature();

	get_signature(buf, firstblocksize, flg);

	// 4 bytes are reserved for the unsigned long checksum
	get_signature(buf + firstblocksize + 4, flen - firstblocksize - 4,flg);

	free(buf);

	if (crc == checksum)
		return true;
	else
		return false;
}
