/*************************************************************************************************
 *
 * $Workfile: ddbdefs.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      This holds the definitions that support various classes.
 *      7/31/2  sjv started creation
 *
 *      
 * #include "ddbDefs.h"
 */

#ifndef _DDBDEFS_H
#define _DDBDEFS_H

// this file needs to be a primative - no includes

bool AlmostEqual2sComplement(float A, float B, int maxUlps); 
bool AlmostEqual2sComplement(double A, double B, int maxUlps);

// numberOfPlaces, 0 => to Int
//                >0 => digits right of decimal point
//                <0 => number of zeros before decimal point
double roundDbl(double toBrounded, int numberOfPlaces);


#define x_T( a )  a
/*=*=*=*=*=*=*=*=*=*=*=* General Support =*=**=*=*=*=*=*=*=*=*/

#ifndef SUB_INDEX
#define SUB_INDEX int
#endif 

//typedef enum bool_e
//{
//  isFALSE,
//  isTRUE
//}
///*typedef*/boolT;

/* so some files will not have to be modified textually */
#define boolT   forceError
#define isFALSE false_ERROR
#define isTRUE  true_ERROR

#define RESPONSECODE_SYMID   150
#define DEVICESTATUS_SYMID   151
#define COMMSTATUS_SYMID     152
#define MANUFACTURER_SYMID   153
#define DEVICETYPE_SYMID     154
#define UNIVERSALREV_SYMID   156

#define DEVICEID_SYMID       161
#define POLLINGADDRESS_SYMID 162

#define CONFIG_CHANGED_BIT   0x40

/****** standard symbols - moved from device 18jan06 **/
#define ROOT_MENU                   1000   /* the item id of the root menu */
#define VIEW_MENU                   1009
#define OFFLINE_ROOT_MENU           1002  // Added for Style Table Tree view, POB - 5/23/2014
#define DIAGNOSTIC_ROOT_MENU        1018  // Added for Style Table Tree view, POB - 5/23/2014
#define PROCESS_VARIABLES_ROOT_MENU 1019  // Added for Style Table Tree view, POB - 5/23/2014
#define DEVICE_ROOT_MENU            1020  // Added for Style Table Tree view, POB - 5/23/2014
#define MAINTENANCE_ROOT_MENU       1026  // Added for Style Table Tree view, POB - 5/23/2014

/*=*=*=*=*=*=*=*=*=*=* Expression Support *=**=*=*=*=*=*=*=*=*/

typedef enum  arithOp_e
{       
		aOp_dontCare,   // 0
		aOp_wantMax,    // MaxVal arithOption
		aOp_wantMin     // MinVal arithOption
	#ifdef REALLYSYMETRICALL /* if the language was symetrical*/
	,   aOp_wantScale,
		// aOp_wantDisp,
		// aOp_wantEdit
	#endif
} /*typedef*/ arithOp_t;
#define ARITHSTR(a) ((a==aOp_dontCare)?"No Option":((a==aOp_wantMax)?"Maximum":((a==aOp_wantMin)?"Minimum":"Illegal Option")))

typedef enum expressionType_e
{
		eT_Unknown = 0,
		eT_IF_expr,     // 1
		eT_SEL_expr,    // 2
		eT_CASE_expr,   // 3
		eT_Direct,      // 4
		// the rest imply that: expressionL & exprDependsL are MT
		eT_IF_isTRUE,    // THEN
		eT_IF_isFALSE,   // ELSE
		et_SEL_isDEFAULT // always TRUE
} /*typedef*/ expressionType_t;

/*****---- data status moved from ddbVar.cpp ----****/

typedef enum instanceDataState {
	IDS_UNINITIALIZED, IDS_CACHED, IDS_STALE, IDS_PENDING, IDS_INVALID, IDS_NEEDS_WRITE
} INSTANCE_DATA_STATE_T ;


//usage: char instanceDataStStrings[6][INSTANCEDATASTATEMAXLEN] = {INSTANCEDATASTATESTRINGS};
#define INSTANCEDATASTATEMAXLEN 14
#define INSTANCEDATASTATESTRINGS {"Uninitialized"},{"Cached"},{"Stale"},{"Pending"},{"Invalid"},{"Needs-Write"}

typedef enum data_avail  {
	DA_HAVE_DATA, DA_NOT_VALID, DA_STALE_OK, DA_STALEUNK
} DATA_QUALITY_T;
//usage: char dataQualityStrings[4][DATAQUALITYMAXLEN] = {DATAQUALITYSTRINGS};
#define DATAQUALITYMAXLEN 14
#define DATAQUALITYSTRINGS {"data-OK"},{"not-valid"},{"stale-OK"},{"stale-Unknown"}

#define QUERY_INC   1
#define VISIBLE_MUL 10

/*****---- expressions are now broken down into finer grain definitions----****/
typedef enum axiomType_e
{
		aT_Unknown = 0,
		aT_IF_expr,     // 1
		aT_SEL_expr,    // 2
		aT_Direct  = 4
}
/*typedef*/axiomType_t;

axiomType_t axiomFrom(expressionType_t e);
//usage: char axiomStrings[5][AXIOMSTRMAXLEN] = {AXIOMSTRINGS};
#define AXIOMSTRMAXLEN  19  /* maximum string length */
#define AXIOMSTRINGS \
	{"Unknown"},\
	{"IF expression"},\
	{"SELECT expression"},\
	{"ILLEGAL axiom"},\
	{"DIRECT expression"}

typedef enum clauseType_e
{
		cT_Unknown   = 0,
		cT_CASE_expr = 3,   // implies there is an expressionL
		cT_Direct,  // 4
		// the rest imply that: expressionL & exprDependsL are MT
		cT_IF_isTRUE,    // 5 THEN
		cT_IF_isFALSE,   // 6 ELSE
		ct_SEL_isDEFAULT // 7 always TRUE
}
/*typedef*/clauseType_t;

clauseType_t clauseFrom(expressionType_t e);
//usage: char clauseStrings[8][CLAUSESTRMAXLEN] = {CLAUSESTRINGS};
#define CLAUSESTRMAXLEN  16 /* maximum string length */
#define CLAUSESTRINGS \
	{"Unknown"},\
	{"ILLEGAL"},\
	{"ILLEGAL"},\
	{"CASE clause"},\
	{"DIRECT clause"},\
	{"THEN clause"},\
	{"ELSE clause"},\
	{"DEFAULT clause"}

/* this must be global so varient and expression can use it */
typedef enum expressionElementType_e
{/* note that these are equivalent to the tag values */
	eeT_Unknown = 0,
	eet_NOT,            //_OPCODE 1         bool
	eet_NEG,            //_OPCODE 2         Value
	eet_BNEG,           //_OPCODE 3         Value
	eet_ADD,            //_OPCODE 4         Value
	eet_SUB,            //_OPCODE 5         Value
	eet_MUL,            //_OPCODE 6         Value
	eet_DIV,            //_OPCODE 7         Value
	eet_MOD,            //_OPCODE 8         Value
	eet_LSHIFT,         //_OPCODE 9         Value
	eet_RSHIFT,         //_OPCODE 10        Value
	eet_AND,            //_OPCODE 11        Value
	eet_OR,             //_OPCODE 12        Value
	eet_XOR,            //_OPCODE 13        bool
	eet_LAND,           //_OPCODE 14        bool
	eet_LOR,            //_OPCODE 15        bool
	eet_LT,             //_OPCODE 16        bool
	eet_GT,             //_OPCODE 17        bool
	eet_LE,             //_OPCODE 18        bool
	eet_GE,             //_OPCODE 19        bool
	eet_EQ,             //_OPCODE 20        bool
	eet_NEQ,            //_OPCODE 21        bool

	eet_INTCST,			//_CONST  22		Value
	eet_FPCST,          //_CONST  23        Value

	eet_VARID,          //_VARVAL 24    isRef = F
	eet_MAXVAL,         //_VARVAL 25    isRef = F,isMax = T
	eet_MINVAL,         //_VARVAL 26    isRef = F,isMax = F

	eet_VARREF,         //_VARVAL 27    isRef = T
	eet_MAXREF,         //_VARVAL 28    isRef = T,isMax = T
	eet_MINREF          //_VARVAL 29    isRef = T,isMax = F

//#ifdef NOTUSED4HART
,   eet_BLOCK,          //_VARVAL 30    isRef = F ????
	eet_BLOCKID,        //_VARVAL 31    isRef = F
	eet_BLOCKREF        //_VARVAL 32    isRef = T
//#endif
	
,   eet_STRCONST,       //STRCST_OPCODE  33
	eet_SYSENUM,        //SYSTEMENUM_OPCODE 34
	eet_COUNTREF,       //CNTREF_OPCODE 35
	eet_CAPACITYREF,    //CAPREF_OPCODE 36
	eet_FIRSTREF,       //FSTREF_OPCODE 37 // stevev 17aug06 - this is a VARREF
	eet_LASTREF         //LSTREF_OPCODE 38 // stevev 17aug06 - this is a VARREF
/* stevev 17aug06 - rest of legal types - not in Tokenizer yet all reference.xxx*/
	,eet_DFLT_VALREF    //DFLTVAL_OPCODE 39
	,eet_VIEW_MINREF    //VMIN_OPCODE    40
	,eet_VIEW_MAXREF    //VMAX_OPCODE    41

	,eet_INVALID = -1
} /*typedef*/ expressElemType_t;

//usage: char exprElemStrings[EXPELEMSTRCNT][EXPELEMSTRMAXLEN] = {EXPELEMSTRINGS};

#define EXPELEMSTRMAXLEN  18    /* maximum string length */
#define EXPELEMSTRCNT     42
#define EXPELEMSTRINGS \
	{"UnknownOPcode"},\
	{"NOT"},\
	{"NEG"},\
	{"BNEG"},\
	{"ADD"},\
	{"SUBTRACT"},\
	{"MULTIPLY"},\
	{"DIVIDE"},\
	{"MODULO"},\
	{"SHIFT-LEFT"},\
	{"SHIFT-RIGHT"},\
	{"AND"},\
	{"OR"},\
	{"XOR"},\
	{"LOGICAL-AND"},\
	{"LOGICAL-OR"},\
	{"LESS-THEN"},\
	{"GREATER-THEN"},\
	{"LESS-OR-EQUAL"},\
	{"GREATER-OR-EQUAL"},\
	{"EQUAL"},\
	{"NOT-EQUAL"},\
	{"INT-CONST"},\
	{"FLOAT-CONST"},\
	{"-VAR_ID-"},\
	{"-MAX_VAL-"},\
	{"-MIN_VAL-"},\
	{"-VAR_REF-"},\
	{"-MAX_REF-"},\
	{"-MIN_REF-"},\
	{"BLOCK"},\
	{"BLOCK ID"},\
	{"BLOCK_REF"},\
	{"STRING CONST"},\
	{"STSTEM ENUM"},\
	{"COUNT_REF"},\
	{"CAPACITY_REF"},\
	{"FIRST_LISTREF"},\
	{"LAST_LISTREF"}, \
	{"DEFAULT_VALUEREF"},\
	{"VIEW_MIN_REF"},\
	{"VIEW_MAX_REF"}

/*=*=*=*=*=*=*=*=* End Expression Support *=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*= DDLString Support *=**=*=*=*=*=*=*=*=*/
/*
 * Types of ddl strings.
 */
// L&T Modifications : LanguageChange - start
/* Changing this preprocessor from 1024 to 4096 to avoid the warning
    DDLSTRING LENGTH (1435) EXCEEDS MAXIMUM(1024) from the file
    sdc625\APPS\APPsupport\DevServices\ddbPrimatives.h.
    This warning will be displayed when the multiple language support
    is used. During this, the length of the string will be greater
    than 1024.*/
#define MAX_LEGAL_STRING_LEN  4096
// L&T Modifications : LanguageChange - end
 /* stevev 11nov08 - as per WAP - no limit to string sizes, let the hosts work it out */
#define STR_OF_LAST_RESORT  L"Undefined String"     /* when no other string can be found */

/* these have to match the defines in TAGS_SA.h and in tokvals.h:TOK_STRING types*/
typedef enum ddlstringType_e
{
	ds_DevSpec      = 0,    //0 DEV_SPEC_STRING_TAG     /* string device specific id */
	ds_Var,                 //1 VARIABLE_STRING_TAG     /* string variable id */
	ds_Enum,                //2 ENUMERATION_STRING_TAG  /* enumeration string information */
	ds_Dict,                //3 DICTIONARY_STRING_TAG   /* dictionary string id */
	ds_VarRef,              //4 VAR_REF_STRING_TAG      /* variable_reference_string */
	ds_EnumRef,             //5 ENUM_REF_STRING_TAG     /* enumerated reference string */
	ds_AttrRef,				//6 ATTR_REF_STRING_TAG     /* added 17jul17 attribute reference */
	ds_MaxValid,

	ds_NoString     = 0xfffe,// NULL/No String
	ds_Undefined    = 0xffff // UNDEFINED_TAG   

}  /*typedef*/ ddlstringType_t;


//usage: char ddlStringStrings[DDLSTRCOUNT][DDLSTRMAXLEN] = {DDLSTRINGS};
//  add " string" to all these
#define DDLSTRMAXLEN  23    /* maximum string length */
#define DDLSTRCOUNT   10
#define DDLSTRINGS \
	{"Device Specific"},\
	{"Variable"},\
	{"Enumeration"},\
	{"Dictionary"},\
	{"Variable Reference"},\
	{"Enumeration Reference"},\
	{"Attribute Reference"},\
	{"-last-"},\
	{"Empty/Null"},\
	{"Undefined"}



/*=*=*=*=*=*=*=*=*= End DDLstring Support *=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*=*= Type Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 * Types of payloads.
 */
typedef enum payloadType_e
{
	pltUnknown      = 0,
	pltddlStr,          // 1    COND2PAYDDLSTR
	pltddlLong,         // 2    COND2PAYDDLONG
	pltbitStr,          // 3    COND2PAYDDLBSTR
	pltEnumList,        // 4    COND2PAYENUMLIST
	pltDataItmLst,      // 5    COND2DILIST
	pltMenuList,            // 6    COND2PAYMLIST
	pltRespCdList,      // 7    COND2RCLIST
//  pltEdDispLst,       // 8    COND2PAYEDDISPLIST

//  pltTransList,       // 9    COND2TRLIST
//  pltMenuList,        //10    COND2PAYMENU
//  pltRespCdList,      //11    COND2PAYRESPCD
//  pltTypeType,        //12    COND2PAYTYPETYPE
//  pltEnumList,        //13    COND2PAYENUM
//  pltItmArrElemLst,   //14    COND2PAYELIST
//  pltCollMemList,     //15    COND2PAYMEMLIST
	pltGroupItemList,
//  pltRecMemList,      //16    COND2PAYRECLIST
//  pltWrtAoneList,     //17    COND2PAYWAOLIST
//  pltUnitList,        //18    COND2PAYUNITLIST
	//pltRefreshList,     //19    COND2PAYREFRESHLIST
	pltReferenceList,   //19    COND2PAYREFLIST
	pltReference,       //20    COND2PAYREFERENCE
	pltExpression,      //21    post DB.JITParser - expression payload for min/max/scale
	pltMinMax,          //22    
	pltIntWhich,
	pltGridMemberList,  // Unique 'members' - NOT true members w/ desc & help
//see groupitem pltMemberElemList,  // 23 - generic array/collection,graph member list
//  pltMemListOLists,   // 24 - list of the above
			// COND2PAYTRANSACTION
			// COND2PAYDATAITEM
//    DEST2COND
//    DEST2CONDLIST
//    COND
	pltExpressionList,    // this is for default values list in templates(added 15mar2016)
	pltLastLdType

	, pltBoolean			/* tokenizer only - when long   */
	, pltActionList			/* tokenizer only - when reflist*/
	, pltRefArrayList		/* tokenizer only - memberlist without encoding the member symbol index */
	, pltTimeScale			/* TOKENIZER ONLY - BITSTRING IN VERSION 8, INT IN VERSION 10       */
	, pltAxisScale			/* TOKENIZER ONLY - cond int in VERSION 8, cond int wrapped with structure tag IN VERSION 10       */
}  /*typedef*/ payloadType_t;


//usage: char payloadStrings[PAYLDSTRCOUNT][PAYLDSTRMAXLEN] = {PAYLDSTRINGS};
#define PAYLDSTRMAXLEN  23  /* maximum string length */
#define PAYLDSTRCOUNT   16
#define PAYLDSTRINGS \
	{"Unknown"},\
	{"DDL string"},\
	{"DDL long"},\
	{"Bit String"},\
	{"Enum List"},\
	{"Data Item List"},\
	{"Menu List"},\
	{"Resp Code List"},\
	/*{"Edit Disp List"},*/\
	/*{"Transaction List"},*/\
	/*{"Menu List"},*/\
	/*{"Resp Code List (2)"},*/\
	/*{"Type Type"},*/\
	/*{"Enum List"},*/\
	/*{"ItemArray Elem List"},*/\
	/*{"Collection Member List"},*/\
	{"Group-Item List"},\
	/*{"Record Member List"},*/\
	/*{"Write As One List"},*/\
	/*{"Unit List"},*/\
	/*{"Refresh List"},*/\
	{"Reference List"},\
	{"Reference"},\
	{"Expression"},\
	{"Min-Max List"},\
	{"Int-Which"},\
	{"Vector List"},\
	{"Out-of-range Payload"}


/*=*=*=*=*=*=*=*=*=*=*=* End Type Support *=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*= Variable Support =*=**=*=*=*=*=*=*=*=*/
/*
 * Types of variables.
 */
typedef enum variableType_e
{
	vT_unused = 0,
	vT_undefined,       // 1
	vT_Integer,         // 2
	vT_Unsigned,        // 3
	vT_FloatgPt,        // 4
	vT_Double,          // 5
	vT_Enumerated,      // 6
	vT_BitEnumerated,   // 7
	vT_Index,           // 8
	vT_Ascii,           // 9
	vT_PackedAscii,     //10 - HART only
	vT_Password,        //11
	vT_BitString,       //12
	vT_HartDate,        //13 - HART only
	vT_Time,            //14 - not HART
	vT_DateAndTime,     //15 - not HART
	vT_Duration,        //16 - not HART
	vT_EUC,             //17 - not HART
	vT_OctetString,     //18 - not HART
	vT_VisibleString,   //19 - not HART
	vT_TimeValue,       //20
	vT_Boolean,         //21 - not HART
	vT_MaxType          //  22
}  /*typedef*/ variableType_t;


//usage: char varTypeStrings[VARTYPESTRCNT][VARTYPESTRMAXLEN] = {VARTYPESTRINGS};
#define VARTYPESTRMAXLEN  16    /* maximum string length */
#define VARTYPESTRCNT     23
#define VARTYPESTRINGS \
	{"UnusedType"},\
	{"Undefined"},\
	{"Integer"},\
	{"Unsigned"},\
	{"Float"},\
	{"Double"},\
	{"Enum"},\
	{"Bit-Enum"},\
	{"Index"},\
	{"Ascii"},\
	{"Packed"},\
	{"Password"},\
	{"BitString"},\
	{"Hart-Date"},\
	{"Time"},\
	{"Date & Time"},\
	{"Duration"},\
	{"EUC"},\
	{"Octet String"},\
	{"Visible String"},\
	{"Time Value"},\
	{"Boolean"},\
	{"Max-illegal"}


/**** variable attribute types ****/
/* from DDLDEFS.H but converted to an enum */
/* mask conversion to enum:  en = (varAttrType_t)mask2value(attr_mask); */
/* enum conversion to mask   msk= 0x00000001<< ((unsigned long)en)      */
/*                           see maskFromInt(a) macro in dllapi.h       */
typedef enum varAttrType_e
{                                      // default values
	 varAttrClass         = 0,  //*- none - required??
	 varAttrHandling,           //*READ_HANDLING | WRITE_HANDLING
	 varAttrConstUnit,          //*len=0, flags=0
	 varAttrLabel,              //*BASE:  DEFAULT_STD_DICT_LABEL
	 varAttrHelp,               //*BASE:  DEFAULT_STD_DICT_HELP
	 varAttrWidth,	// 5	// 0 
	 varAttrHeight,			// 0
	 varAttrValidity,           //*BASE:  TRUE
	 varAttrPreReadAct,         // count = 0
	 varAttrPostReadAct,        // count = 0
	 varAttrPreWriteAct,    //10// count = 0
	 varAttrPostWriteAct,       // count = 0
	 varAttrPreEditAct,         // count = 0
	 varAttrPostEditAct,        // count = 0
			varAttrResponseCodes,		// 0---not supported by hart
	 varAttrTypeSize,      //15 //*- none - required
	 varAttrDisplay,            //*DEFAULT_STD_DICT_DISP_INT
									   // DEFAULT_STD_DICT_DISP_UINT
									   // DEFAULT_STD_DICT_DISP_FLOAT
									   // DEFAULT_STD_DICT_DISP_DOUBLE
	 varAttrEdit,               //*DEFAULT_STD_DICT_EDIT_INT
									   // DEFAULT_STD_DICT_EDIT_UINT
									   // DEFAULT_STD_DICT_EDIT_FLOAT
									   // DEFAULT_STD_DICT_EDIT_DOUBLE
	 varAttrMinValue,           // count = 0
	 varAttrMaxValue,           // count = 0
	 varAttrScaling,        //20//*1 byte int with value of 1
	 varAttrEnums,              // count = 0
	 varAttrIndexItemArray,     // 22
	 varAttrDefaultValue,
	 varAttrRefreshAct,
	 varAttrDebugInfo,          // 25

	varAttrPostRequestAct,
	varAttrPostUserAct,

	varAttrTimeFormat,          // added 02jun08
	varAttrTimeScale,
	 varAttrPrivate,
	 varAttrVisibility,
	varAttrLastvarAttr  //30     /* must be last in list for scanning*/
}
/*typedef*/ varAttrType_t;

#define IS_VARDBG(a) ( a==((iT_Variable<<8)+varAttrDebugInfo) )

//usage: char varAttrStrings[VARATTRSTRCOUNT][VARATTRSTRMAXLEN] = {VARATTRSTRINGS};
#define VARATTRSTRMAXLEN  26    /* maximum string length */
#define VARATTRSTRCOUNT   33

#define VARATTRSTRINGS \
	{"Class"},\
	{"Handling"},\
	{"Const Unit"},\
	{"Label"},\
	{"Help"},\
	{"Variable Width"},\
	{"Variable Height"},\
	{"Validity"},\
	{"Pre-Read Action"},\
	{"PostRead Action"},\
	{"Pre-Write Action"},\
	{"PostWrite Action"},\
	{"Pre-Edit Action"},\
	{"PostEdit Action"},\
	{"Response Codes"},\
	{"Var Type & Size"},\
	{"Display Format"},\
	{"Edit Format"},\
	{"Min Value"},\
	{"Max Value"},\
	{"Scaling"},\
	{"Enumerations"},\
	{"Array Index"},\
	{"Default Value"}, \
	{"Refresh Action"},\
	{"Debug Information"},\
	{"Post-Request Action"},\
	{"Post-User-Modified Action"},\
	{"Time Format"},\
	{"Time Scale"},\
	{"Private"},\
	{"Visiblity"},\
	{"Last-illegal"}


/************** variable attribute supporting types **********/
/**** CLASS ****/
/* for variables and methods - derived from DDLDEFS.H */
/* note that the class attribute is a bitstring so multiple
 * classes may be set - the masks follow
 * 1 << (attrClass_t-1)  will give the mask bit for that class
 */
typedef enum attrClass_e
{
	classNoClass        =  0,
	classDiagnostic,    // =1
	classDynamic,       //  2
	classService,       //  3   4
	classCorrection,    //  4   8
	classComputation,
	classInputBlock,
	classAnalogOut,
	classHART,          //  8   80
	classLocalDisplay,          
	classFrequency,
	classDiscrete,
	classDevice,        // 12   800
	classLocal,
	classInput,
	classBackground,
	/* new classes from spec 500 03Dec07 */
	classAlarm,         // 16   8000
	classMode,
	classTune,
	/* new class 1mar10 */
	classConfig,        //       40000

	classFactory        = 21,   /*0x100000   historical usage       */

	class_Error         = 32    /*0x80000000 used in the tok parser */

}
/*typedef*/ attrClass_t;

typedef enum maskClass_e
{
	maskNoClass         =  0x00000000,
	maskDiagnostic      =  0x00000001,
	maskDynamic         =  0x00000002,
	maskService         =  0x00000004,
	maskCorrection      =  0x00000008,
	maskComputation     =  0x00000010,
	maskInputBlock      =  0x00000020,
	maskAnalogOut       =  0x00000040,
	maskHART            =  0x00000080,
	maskLocalDisplay    =  0x00000100,
	maskFrequency       =  0x00000200,
	maskDiscrete        =  0x00000400,
	maskDevice          =  0x00000800,
	maskLocal           =  0x00001000,
	maskInput           =  0x00002000,
	maskFactory         =  0x00100000   // originator proprietary I imagine
}
/*typedef*/ maskClass_t;

#define SINGLETON_CLASSES (maskCorrection | maskComputation | maskInputBlock | maskAnalogOut | maskHART | \
                           maskLocalDisplay | maskFrequency | maskFactory    | maskDiscrete  | maskDevice )

//usage: char varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN] = {CLASSSTRINGS};
#define CLASSSTRMAXLEN  21	/* maximum string length */
#define CLASSSTRCOUNT   28


/*	{_T("No-Class")},*/
#define CLASSSTRINGS \
	{_T("Diagnostic")},\
	{_T("Dynamic")},\
	{_T("Service")},\
	{_T("Correction")},\
	{_T("Computation")},\
	{_T("Input-Block")},\
	{_T("Analog-Block")},\
	{_T("HART-class")},\
	{_T("Local-Display")},\
	{_T("Frequency")},\
	{_T("Discrete")},\
	{_T("Device")},\
	{_T("Local")},\
	{_T("Input")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("Factory")},\
	{_T("Device-Status-View")},\
	{_T("System-Engineering")},\
	{_T("Device-Engineering")},\
	{_T("Device-Commisioning")},\
	{_T("Device-Maintenance")},\
	{_T("Condition-Monitoring")}

/**** HANDLING ****/
/* note that these are both values AND masks */
typedef enum attrHandling_e
{
	handlingUnknown = 0,
	handlingRead    = 1,
	handlingWrite   = 2,
	handlingRdWr    = 3
#ifdef XMTR
	, handlingConfigUnknown = 4,
	handlingConfigRead    = 5,
	handlingConfigWrite   = 6,
	handlingConfigRdWr    = 7
#endif
}
/*typedef*/ attrHandling_t;




/**** BIT-ENUMERATED VARIABLE'S STATUS CLASSES ****/
/* note that the status class is a bitstring so multiple
 * status classes may be set - the masks follow
 * 1 << (statusClass_t-1)  will give the mask bit for that class
 */

typedef enum statusClass_e
{
	statusNormal             = 0,   // is OC_NORMAL
	statusHardware           = 1,
	statusSoftware,
	statusProcess,
	statusMode,
	statusData,             // 5
	statusMisc,
	statusEvent,
	statusState,
	statusSelfCorrect,
	statusCorrectable,      //10
	statusNOTcorrectable,
	statusSummary,
	statusDetail,
	statusMore,
	statusCommError,        //15
	statusIgnoreInTemp,
	statusBadOutput         //17
}
/*typedef */ statusClass_t;

typedef enum maskStatus_e
{
	maskStHardware          =   0x000001,
	maskStSoftware          =   0x000002,
	maskStProcess           =   0x000004,
	maskStMode              =   0x000008,
	maskStData              =   0x000010,
	maskStMisc              =   0x000020,
	maskStEvent             =   0x000040,
	maskStState             =   0x000080,
	maskStSelfCorrect       =   0x000100,
	maskStCorrectable       =   0x000200,
	maskStNOTcorrectable    =   0x000400,
	maskStSummary           =   0x000800,
	maskStDetail            =   0x001000,
	maskStMore              =   0x002000,
	maskStCommError         =   0x004000,
	maskStIgnoreInTemp      =   0x008000, /* IGNORE_IN_HANDHELD */
	maskStBadOutput         =   0x010000, /* added next 4 stevev 04aug16 */
	maskStIgnoreInHost      =   0x020000,
	maskStError             =   0x040000,
	maskStWarning           =   0x080000,
	maskStInfo              =   0x100000
}
/*typedef*/maskStatus_t;

typedef enum outStatusClass_e
{
	outStatusDV     = 1,
	outStatusTV,
	outStatusAO,
	outStatusAll
}
/*typedef*/ outStatusClass_t;


typedef enum outClass_e
{
							/*      Good(0) + Auto(0)  = 0*/
	outClassManual  = 1,	/* also Good(0) + Manual(1)= 1*/
	outClassBad     = 2,	/* also Bad(2)  + auto(0)  = 2*/
	outClassMan_Bad = 3,	/*      bad(2)  + Manual(1)= 3*/
	outClassMarginal= 4,	/* also Marginal+ Auto(0)  = 4*/
	outClassMarg_Man= 5		/* Marginal(4)  + Manual(1)= 5*/
}
/*typedef*/ outClass_t;

/**** TIME VALUE VARIABLE'S TIME SCALE CLASSES ****/
/* note that the time scale is a bitstring so multiple
 * scales may be set - thats an error, uses highest bit
 * This is now used to determine what structure values are active to support partial value
 * updates. stevev 20jan12.
 */
typedef enum timeScale_e
{
	tsNo_Scale          =  0x00,
	tsSecScale          =  0x01,
	tsMinScale          =  0x02,
	ts_MSScale          =  0x03,    // 3 is Min & Sec
	tsHr_Scale          =  0x04,
	tsH_SScale          =  0x05,
	tsHM_Scale          =  0x06,
	tsHMSScale          =  0x07,
	ts_I_Scale          =  0x08,    // Fixed defect #4511, POB - 4/21/2014
	ts_p_Scale          =  0x10
}
/*typedef*/ timeScale_t;

#define TS__NO_SCALE_FACTOR             1
#define TS_SEC_SCALE_FACTOR         32000
#define TS_MIN_SCALE_FACTOR       1920000
#define TS_HOURSCALE_FACTOR     115200000

#define TS_SEC_PER_HR                3600
#define TS_SEC_PER_MIN                 60
#define TS_MIN_PER_HR                  60
#define TS_HRS_PER_DAY                 24

#define TS__NO_UNIT             L""
#define TS_SEC_UNIT             L"s"
#define TS_MIN_UNIT             L"min"
#define TS_HOURUNIT             L"h"

//usage: char varTimeScaleStrings[TIMESCALECOUNT][TIMESCALEMAXLEN] = {TIMESCALESTRINGS};
#define TIMESCALEMAXLEN  14 /* maximum string length */
#define TIMESCALECOUNT    4
#define TIMESCALESTRINGS \
	{x_T("No-Time-Scale")},\
	{x_T("Second Scale")},\
	{x_T("Minute Scale")},\
	{x_T("Hour Scale")}



/*=*=*=*=*=*=*=*=*=*=*= Command Support =*=**=*=*=*=*=*=*=*=*/

/***** COMMAND attributes *****/
typedef enum cmdAttrType_e
{
	cmdAttrNumber       = 0,
	cmdAttrOperation,
	cmdAttrTransaction,
	cmdAttrRespCodes,
	cmdAttrDebugInfo,
	cmdAttrUnknown
}
/*typedef */ cmdAttrType_t;

#define IS_CMDDBG(a) ( a==((iT_Command<<8)+cmdAttrDebugInfo) )

//usage: char cmdAttrStrings[CMDATRSTRCOUNT] [CMDATRMAXLEN]     = {CMDATRTYPESTRINGS};
#define CMDATRMAXLEN     27
#define CMDATRSTRCOUNT    6
#define CMDATRTYPESTRINGS \
	{"Command Number"},\
	{"Command Operation"},\
	{"Command Transaction"},\
	{"Command Response Codes"},\
	{"Command Debug Information"},\
	{"Command Unknown Attribute"}


/*
 * HART command operations.
 */
typedef enum cmdOperationType_e
{
	cmdOpNone        = 0,
	cmdOpRead,
	cmdOpWrite,
	cmdOpCmdCmd     // 3
}
/*typedef */ cmdOperationType_t;


/*
 * Data item types.
 */
typedef enum cmdDataItemType_e  /*  note that in the Interface Library 0,1,5 are only ones used */
{
	cmdDataConst        = 0,/* 0    integer constant - only (flags & width set to 0)            */
	cmdDataReference,       /* 1 reference <others are converted to this >                      */
	cmdDataFlags,           /* converted to (1) with non zero flags (width set to 0)            */
	cmdDataWidth,           /* converted to (1) with flags set to WIDTH_PRESENT (width set)     */
	cmdDataFlagWidth,       /* converted to (1) with flags set + flgWIDTH_PRESENT set(width set)*/
	cmdDataFloat            /* 5    float constant - only (flags & width set to 0)              */
}                           /*    SEE cmdDataItemFlags_t below for valid flag values            */
/*typedef */ cmdDataItemType_t;
/* due to legacy implementation of the aCclasses encoding in parserinfc.cpp, the 
	cmdDataItemType_t is abbreviated to 0,1 & 5..5 being the unspecified(illegal but exists) float constant.
	The x8000 bit of cmdDataItemFlags_t is considered a "width-present" flag taking the info
	of enums 3 & 4 and setting that bit so that under normal circumstances (no float constant)
	this cmdDataItemType_t will become 0,false at constant & 1,true at reference and
	the  cmdDataItemFlags_t becomes bit 0=>info, bit 1=>index, bit 15=>width-present.
*/

/*
 * Response code types.
 */
typedef enum cmdRespCdType_e
{
	cmdRespCdNone               = 0,
	cmdRespCdSuccess,
	cmdRespCdMiscWarning,
	cmdRespCdDataEntryWarn,
	cmdRespCdDataEntryErr,
	cmdRespCdModeError,
	cmdRespCdProcessError,
	cmdRespCdMiscError,
	cmdRespCdInvalid
}
/*typedef */ cmdRespCdType_t;


/*
 * HART data item flags.
 */
typedef enum cmdDataItemFlags_e
{
	cmdDataItemFlg_None         =  0x00,
	cmdDataItemFlg_Info,        // 0x01
	cmdDataItemFlg_Index,       // 0x02
	cmdDataItemFlg_indexNinfo,  // 0x03
	cmdDataItemFlg_WidthPresent = 0x8000
}
/*typedef */ cmdDataItemFlags_t;
/* the x8000 bit is used to hold the cmdDataItemType_t values of 3 & 4. see above */
	


/*=*=*=*=*=*=*=*=*=*=*= Menu Support =*=**=*=*=*=*=*=*=*=*/
/*
 * Menu item flags.
 */
/* MENU attributes */
typedef enum menuAttrType_e
{
	menuAttr_Label      = 0,
	menuAttr_Items,
	menuAttr_Help,
	menuAttr_Validity,
	menuAttr_Style,
	menuAttrDebugInfo,
	menuAttr_Unknown
}
/*typedef */ menuAttrType_t;

#define IS_MENDBG(a) ( a==((iT_Menu<<8)+menuAttrDebugInfo) )

//usage: char menuAttrStrings[MENUATRSTRCOUNT] [MENUATRMAXLEN]     = {MENUATRTYPESTRINGS};
#define MENUATRMAXLEN     27
#define MENUATRSTRCOUNT    7
#define MENUATRTYPESTRINGS \
	{"Menu Label"},\
	{"Menu Items"},\
	{"Menu Help"},\
	{"Menu Validity"},\
	{"Menu Style"},\
	{"Menu Debug Information"},\
	{"Menu Unknown Attribute"}


typedef enum menuItemType_e
{
	menuItemNone        = 0,
	menuItemReadOnly,       // 0x01
	menuItemDispValue,      // 0x02
	menuItemROdisp,         
	menuItemReview,         // 0x04
	menuItemReviewRO,       // 5
	menuItemReviewDisp,
	menuItemReviewRODisp,   // 7
// no label
	menuItemNoLabel,        // 0x08
	menuItemNLReadOnly,     // 0x09
	menuItemNLDispValue,    // 0x0a
	menuItemNLROdisp,           
	menuItemNLReview,       // 0x0c
	menuItemNLReviewRO,     // d
	menuItemNLReviewDisp,
	menuItemNLReviewRODisp, // 0x0f
// no unit
	menuItemNoUnit,         // 0x10
	menuItemNUReadOnly,     // 0x11
	menuItemNUDispValue,    // 0x12
	menuItemNUROdisp,           
	menuItemNUReview,       // 0x14
	menuItemNUReviewRO,     // 0x15
	menuItemNUReviewDisp,
	menuItemNUReviewRODisp, // 0x17
// no labal or unit
	menuItemNoLabelNorUnit, // 0x18
	menuItemNLNUReadOnly,   // 0x19
	menuItemNLNUDispValue,  // 0x1a
	menuItemNLNUROdisp,         
	menuItemNLNUReview,     // 0x1c
	menuItemNLNUReviewRO,   // 0x1d
	menuItemNLNUReviewDisp,
	menuItemNLNUReviewRODisp// 0x1f
// inline - added late,just raw for now 01nov06
   ,menuItemInLine          // 0x20
}
/*typedef */ menuItemType_t;


typedef enum menuStyleType_e
{
	menuStyleNone       = 0,
	menuStyleWindow,    // 0x01 WINDOW_STYLE_TYPE   
	menuStyleDialog,    // 0x02 DIALOG_STYLE_TYPE   
	menuStylePage,      //      PAGE_STYLE_TYPE     
	menuStyleGroup,     // 0x04 GROUP_STYLE_TYPE    
	menuStyleMenu,      // 5    MENU_STYLE_TYPE     
	menuStyleTable,     //      TABLE_STYLE_TYPE     - default
	menuStyleUnknown    // 7
	
}
/*typedef */ menuStyleType_t;


//usage: char menuStyleStrings[MENUSTYLESTRCOUNT] [MENUSTYLEMAXLEN] = {MENUSTYLESTRINGS};
#define MENUSTYLEMAXLEN     10
#define MENUSTYLESTRCOUNT   28
#define MENUSTYLESTRINGS    {"Window"},{"Dialog"},\
{"  Page"},{" Group"},{"  Menu"},{" Table"},{"MenuDisp"},{" var"},{" index"},{" edDisp"},\
{" enum"},{" bitenum"},{" bit"},{" meth"},{" img"},{" scope"},{" sep"},{" newLn"}\
	,{" str"},{" tab"},{" grid"},{" template"},{" par"},{"blank"}, {" NOT"}, {"UNKNOWN"} 


typedef enum menuItemDrawingStyles_e
{// keep the menu style types on the top!
	mds_window,
	mds_dialog,
	mds_page,
	mds_group,
	mds_menu,
	mds_table,
	mds_methDialog,

	mds_var,
	mds_index,
	mds_edDisp,
	mds_enum,
	mds_bitenum,
	mds_bit,
	mds_method,
	mds_image,
	mds_scope,      // charts and graphs
	mds_separator,
	mds_newline,    // rowbreak
	mds_constString,
	mds_tab,        // psuedo style encompassing a list of pages
	mds_grid,       // added stevev 27jun05
	mds_template,   // added stevev 13oct10
	mds_parent,// a pseudo drawing type
	mds_blank, // a placeholder - always empty for CB & invisible
	mds_do_not_use
}
/*typedef */ menuItemDrawingStyles_t;

/*=*=*=*=*=*=*=*=*=*=*= Edit Display Support =*=**=*=*=*=*=*=*=*=*/
/* EDIT_DISPLAY attributes */
typedef enum eddispAttrType_e
{
	eddispAttrLabel     = 0,
	eddispAttrEditItems,
	eddispAttrDispItems,
	eddispAttrPreEditAct,
	eddispAttrPostEditAct,
	eddispAttrHelp,
	eddispAttrValidity,
	eddispAttrDebugInfo,
	eddispAttr_Unknown
}
/*typedef */ eddispAttrType_t;

#define IS_EDDDBG(a) ( a==((iT_EditDisplay<<8)+eddispAttrDebugInfo) )

//usage: char edDispTypeStrings[EDDISPSTRCOUNT][EDDISPMAXLEN] = {EDDISPTYPESTRINGS};
#define EDDISPMAXLEN     32
#define EDDISPSTRCOUNT    9
#define EDDISPTYPESTRINGS \
	{"Edit Display Label"},\
	{"Edit Display Edits"},\
	{"Edit Display Displays"},\
	{"Edit Display Pre Edit Actions"},\
	{"Edit Display PostEdit Actions"},\
	{"Edit Display Help"},\
	{"Edit Display Validity"},\
	{"Edit Display Debug Information"},\
	{"Edit Display Attr_Unknown"}


/*=*=*=*=*=*=*=*=*=*=*= Method Support =*=**=*=*=*=*=*=*=*=*/
/* METHOD attributes */
typedef enum methodAttrType_e
{
	methodAttrClass         = 0,
	methodAttrLabel,
	methodAttrHelp,
	methodAttrDefinition,
	methodAttrValidity,
	methodAttrScope,
	methodAttrType,
	methodAttrParams,
	methodAttrDebugInfo,
	methodAttrVisible,
	methodAttrPrivate,
	methodAttr_Unknown
}
/*typedef */ methodAttrType_t;

#define IS_METHDBG(a) ( a==((iT_Method<<8)+methodAttrDebugInfo) )

//usage: char methodTypeStrings[METHODSTRCOUNT][METHODMAXLEN] = {METHODTYPESTRINGS};
#define METHODMAXLEN     20
#define METHODSTRCOUNT    12
#define METHODTYPESTRINGS \
	{"Method Class"},\
	{"Method Label"},\
	{"Method Help"},\
	{"Method Definition"},\
	{"Method Validity"},\
	{"Method Scope"},\
	{"Method Type"},\
	{"Method Parameters"},\
	{"Method Debug Info"},\
	{"Method Visible"},\
	{"Method Private"},\
	{"Method AttrUnknown"}

/*
 * Method type flags
 */
typedef enum methodFlags_e
{
	methodFlag_Scaling      =   0x00000001,
	methodFlag_User         =   0x00000010
}
/*typedef */ methodFlags_t;


/*
 * Method type flags
 */
typedef enum methodTypeFlag_e
{
	methodType_NonFlaged    =   0x00000000,
	methodType_Array        =   0x00000001,
	methodType_Reference    =   0x00000002,
	methodType_Const        =   0x00000004  // unsupported
}
/*typedef */ methodTypeFlag_t;


typedef enum methodVarType_e
{
	methodVarVoid           = 0,
	methodVarChar,
	methodVarShort,
	methodVarLongInt,
	methodVarFloat,
	methodVarDouble,        // 5
	methodVar_U_Char,
	methodVar_U_Short,
	methodVar_U_LongInt,
	methodVarInt64,
	methodVar_U_Int64,      //10
	methodVarDDString,
	methodVarDDItem,
	methodVar_Unknown
}
/*typedef */ methodVarType_t;
//usage: char methodVarTypeStrings[METHODVARSTRCOUNT][METHODVARMAXLEN] = {METHODVARTYPESTRINGS};
#define METHODVARMAXLEN     30
#define METHODVARSTRCOUNT    15
#define METHODVARTYPESTRINGS \
	{"Void"},\
	{"Char"},\
	{"Short"},\
	{"Integer"},\
	{"Float"},\
	{"Double"},\
	{"Unsigned Char"},\
	{"Unsigned Short"},\
	{"Unsigned Integer"},\
	{"DoubleInteger"},\
	{"Unsigned DoubleInteger"},\
	{"DDL String"},\
	{"DDL Item"},\
	{"Method Variable Type Unknown"}

extern char methodVarTypeStrings[METHODVARSTRCOUNT][METHODVARMAXLEN];

/*=*=*=*=*=*=*=*=*=*=*= Refresh Support =*=**=*=*=*=*=*=*=*=*/
/* REFRESH attributes */
typedef enum refreshAttrType_e
{
	refreshAttrItems        = 0,
	refreshAttrDebugInfo,
	refreshAttr_Unknown,
	refreshAttr_Update,
	refreshAttr_Watch
}
/*typedef */ refreshAttrType_t;

#define IS_RFSHDBG(a) ( a==((iT_Refresh<<8)+refreshAttrDebugInfo) )

/*usage: char refreshTypeStrings[1][15] = {{"Refresh Items"},{"Unknown"}};*/

/*=*=*=*=*=*=*=*=*=*=*= Unit Support =*=**=*=*=*=*=*=*=*=*/
/* UNIT attributes */
typedef enum unitAttrType_e
{
	unitAttrItems           = 0,
	unitAttrDebugInfo,
	unitAttr_Unknown,
	unitAttr_Var,
	unitAttr_Update
}
/*typedef */ unitAttrType_t;

#define IS_UNITDBG(a) ( a==((iT_Unit<<8)+unitAttrDebugInfo) )

/*usage: char unitTypeStrings[1][20] = {{"Unit Relation Items"},{"Unknown"}};*/

/*=*=*=*=*=*=*=*=*=*= WriteAsOne Support =**=*=*=*=*=*=*=*=*/
/* WRITE AS ONE attributes */
typedef enum waoAttrType_e
{
	waoAttrItems            = 0,
	waoAttrDebugInfo,
	waoAttr_Unknown
}
/*typedef */ waoAttrType_t;

#define IS_WAODBG(a) ( a==((iT_WaO <<8)+waoAttrDebugInfo ) )

/*usage: char waoTypeStrings[1][20] = {{"Write-As-One Items"},{"Unknown"}};*/


/*=*=*=*=*=*= Collection/ItemArray/File Support =*=**=*=*=*=*/
/* Group Item attributes */
typedef enum grpItmAttrType_e
{
	grpItmAttrElements      = 0,
	grpItmAttrLabel,
	grpItmAttrHelp,
	grpItmAttrValidity,		/* NOTE: files and collections are always valid - NOT - */
	grpItmAttrDebugInfo,
	grpItmAttrHandling,		// FILE
	grpItmAttrUpdateSActions,//FILE
	grpItmAttrIdentity,		// FILE
	grpItmAttrPrivate,
	grpItmAttrShared,		// FILE
	grpItmAttrUnknown
}
/*typedef */ grpItmAttrType_t;


#define		FILE_MEMBERS_ID				0
#define		FILE_LABEL_ID				1
#define		FILE_HELP_ID				2
#define		FILE_NO_VALIDITY			3
#define		FILE_DEBUG_ID				4
#define		FILE_HANDLING_ID			5	/* 5,6&7 added 17nov08 */
#define		FILE_UPDATE_ACTIONS_ID      6
#define		FILE_IDENTITY_ID			7
#define		FILE_PRIVATE_ID             8   /* 8&9 added 30sep2015-stevev */
#define		FILE_SHARED_ID              9
#define		MAX_FILE_ID				   10


#define IS_COLLDBG(a) ( a==((iT_Collection<<8)+ grpItmAttrDebugInfo) )
#define IS_RARRDBG(a) ( a==((iT_ItemArray <<8)+ grpItmAttrDebugInfo) )

//usage: char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN] = {GRPITMATRTYPESTRINGS};
#define GRPITMATRMAXLEN     24
#ifdef TOK80XXX
// see explanatory comment in DDLDEFS.H [3/17/2016 tjohnston]
#define GRPITMATRSTRCOUNT    8
#define GRPITMATRTYPESTRINGS \
	{"Group Item Elements"},\
	{"Group Item Label"},\
	{"Group Item Help"},\
	{"Group Item Validity"},\
	{"Group Item Unknown"}, \
	{"Group Item Private"}, \
	{"Group Item Debug Info"},\
	{"Group Item Unknown"}
#else
#define GRPITMATRSTRCOUNT    11
#define GRPITMATRTYPESTRINGS \
	{"Group Item Elements"},\
	{"Group Item Label"},\
	{"Group Item Help"},\
	{"Group Item Validity"},\
	{"Group Item Debug Info"},\
	{"Group Item Handling"}, \
	{"Group Item Update Acts"}, \
	{"Group Item Identity"}, \
	{"Group Item Private"}, \
	{"Group Item Shared"}, \
	{"Group Item Unknown"}
#endif

/*=*=*=*=*=*=*=*=*=*=*= Block Support =*=**=*=*=*=*=*=*=*=*/
/* BLOCK attributes */
typedef enum blockAttrType_e
{
	blockAttrCharacteristic = 0,
	blockAttrLabel,
	blockAttrHelp,
	blockAttrParameter,
	blockAttrMenu,
	blockAttrEditDisp,
	blockAttrMethod,
	blockAttrRefresh,
	blockAttrUnit,
	blockAttrWao,
	blockAttrCollect,
	blockAttrItemArray,
	blockAttrParameterList
}
/*typedef */ blockAttrType_e;


/*=*=*=*=*=*=*=*=*=*=*= Program Support =*=**=*=*=*=*=*=*=*=*/
/* PROGRAM attributes */
typedef enum progAttrType_e
{
	progAttrArgs            = 0,
	progAttrRespCodes
}
/*typedef */ progAttrType_t;



/*=*=*=*=*=*=*=*=*=*=*= Record Support =*=**=*=*=*=*=*=*=*=*/
/* RECORD attributes */
typedef enum recordAttrType_e
{
	recordAttrMembers       = 0,
	recordAttrLabel,
	recordAttrHelp,
	recordAttrRespCodes
}
/*typedef */ recordAttrType_t;



/*=*=*=*=*=*=*=*=*=*=*= Array Support =*=**=*=*=*=*=*=*=*=*/
/* ARRAY attributes */
typedef enum arrayAttrType_e
{
	arrayAttrLabel          = 1,/* stevev 18jan05 - to match ddldefs.h */
	arrayAttrHelp,
	arrayAttrValidity,
	arrayAttrType,
	arrayAttrElementCnt,
	arrayAttrDebugInfo,
	arrayAttrPrivate,
	arrayAttr_Unknown
}
/*typedef */ arrayAttrType_t;

#define IS_ARRDBG(a) ( a==((iT_Array <<8)+ arrayAttrDebugInfo) )

//usage: char arrAttrStrings[ARRATRSTRCOUNT] [ARRATRMAXLEN]     = {ARRATRTYPESTRINGS};
#define ARRATRMAXLEN     22
#define ARRATRSTRCOUNT    9
#define ARRATRTYPESTRINGS \
	{"ArrayZero is illegal"},\
	{"Array Label"},\
	{"Array Help"},\
	{"Array Validity"},\
	{"Array Type"},\
	{"Array Element Count"},\
	{"Array Debug Info"},\
	{"Array Private"},\
	{"Array Unknown"}


/*=*=*=*=*=*=*=*=*= Variable List Support *=*=*=*=*=*=*=*=*/
/* VARIABLE LIST attributes */
typedef enum varlistAttrType_e
{
	varlistAttrMembers      = 0,
	varlistAttrLabel,
	varlistAttrHelp,
	varlistAttrRespCodes
}
/*typedef */ varlistAttrType_t;


/*=*=*=*=*=*=*=*= Response Code Support =**=*=*=*=*=*=*=*=*/
/* RESPONSE CODE attributes */
typedef enum respcodeAttrType_e
{
	respcodeAttrMembers     = 0
}
/*typedef */ respcodeAttrType_e;


/*=*=*=*=*=*=*=*=*=*=*= Domain Support =*=**=*=*=*=*=*=*=*=*/
/* DOMAIN attributes */
typedef enum domainAttrType_e
{
	domainAttrHandling      = 0,
	domainAttrRespCodes
}
/*typedef */ domainAttrType_t;


/*=*=*=*=*=*=*=*=*=*=*= Member Support =*=**=*=*=*=*=*=*=*=*/
typedef enum memberType_e
{
	memberCollection    = 1,
	memberParameter,
	memberParameterList,
	memberVarList,
	memberRecord,
	memberFile,         //6
	memberChart,
	memberSource,
	memberGraph,
	memberCharacteristic//10
}
/*typedef*/ memberType_t;



/*=*=*=*=*=*=*=*=*=*=*=*= File Support =*=**=*=*=*=*=*=*=*=*/
typedef enum fileAttrType_e
{
	fileMembers ,   //0 (see ddldefs.h
	fileLabel,
	fileHelp,
	fileNoValidity,
	fileDebugInfo,
	filePrivate,
	fileShared,
	file_unknown
}
/*typedef*/ fileAttrType_t;

#define IS_FILEDBG(a) ( a==((iT_File <<8)+ fileDebugInfo) )

//usage: char fileAttrStrings[FILEATRSTRCOUNT] [FILEATRMAXLEN]     = {FILEATRTYPESTRINGS};
#define FILEATRMAXLEN     22
#define FILEATRSTRCOUNT    11
#define FILEATRTYPESTRINGS \
	{"File Members"},\
	{"File Label"},\
	{"File Help"},\
	{"File ERROR attr Type"},\
	{"File Debug Info"},\
	{"File Handling"},\
	{"File Update Actions"},\
	{"File Identity"},\
	{"File Private"},\
	{"File Shared"},\
	{"File Unknown"}


/*=*=*=*=*=*=*=*=*=*=*= Chart Support =*=**=*=*=*=*=*=*=*=*/
/* CHART attributes */
typedef enum chartAttrType_e
{
	chartAttrLabel          = 0,
	chartAttrHelp,
	chartAttrValidity,
	chartAttrHeight,        /* be sure this always matches  graphAttrHeight*/
	chartAttrWidth,
	chartAttrType,
	chartAttrLength,
	chartAttrCycleTime,
	chartAttrMembers,
	chartAttrDebugInfo,
	chartAttrUnknown
}
/*typedef */ chartAttrType_t;

#define IS_CHRTDBG(a) ( a==((iT_Chart <<8)+ chartAttrDebugInfo) )

//usage: char chartAttrStrings[CHRTATRSTRCOUNT] [CHRTATRMAXLEN]     = {CHRTATRTYPESTRINGS};
#define CHRTATRMAXLEN     18
#define CHRTATRSTRCOUNT   11
#define CHRTATRTYPESTRINGS \
	{"Chart Label"},\
	{"Chart Help"},\
	{"Chart Validity"},\
	{"Chart Height"},\
	{"Chart Width"},\
	{"Chart Type"},\
	{"Chart Length"},\
	{"Chart Cycle Time"},\
	{"Chart Members"},\
	{"Chart Debug Info"},\
	{"Chart Unknown"}


/*=*=*=*=*=*=*=*=*=*=*= Graph Support =*=**=*=*=*=*=*=*=*=*/
/* GRAPH attributes */
typedef enum graphAttrType_e
{
	graphAttrLabel          = 0,
	graphAttrHelp,
	graphAttrValidity,
	graphAttrHeight,        /* be sure this always matches  chartAttrHeight*/
	graphAttrWidth,
	graphAttrXAxis,
	graphAttrMembers,
	graphAttrDebugInfo,
	graphAttrCycleTime,     /* added 23jan07 - sjv - spec change */
	graphAttr_Unknown
}
/*typedef */ graphAttrType_t;

#define IS_GRPHDBG(a) ( a==((iT_Graph <<8)+ graphAttrDebugInfo) )

//usage: char graphAttrStrings[GRPHTATRSTRCOUNT] [GRPHTATRMAXLEN]     = {GRPHATRTYPESTRINGS};
#define GRPHTATRMAXLEN     18
#define GRPHTATRSTRCOUNT    10
#define GRPHATRTYPESTRINGS \
	{"Graph Label"},\
	{"Graph Help"},\
	{"Graph Validity"},\
	{"Graph Height"},\
	{"Graph Width"},\
	{"Graph X Axis"},\
	{"Graph Members"},\
	{"Graph Debug Info"},\
	{"Graph Cycletime"},\
	{"Graph Unknown"}
	

/*=*=*=*=*=*=*=*=*=*=*= Axis Support =*=**=*=*=*=*=*=*=*=*/
/* AXIS attributes */
typedef enum axisAttrType_e
{
	axisAttrLabel           = 0,
	axisAttrHelp,
	axisAttrValidity,       /* no longer there, we'll leave this as a placeholder*/
	axisAttrMinVal,
	axisAttrMaxVal,
	axisAttrScale,
	axisAttrUnit,
	axisAttrDebugInfo,
	axisAttrViewMinVal,
	axisAttrViewMaxVal,
	axisAttr_Unknown
}
/*typedef */ axisAttrType_t;

#define IS_AXISDBG(a) ( a==((iT_Axis <<8)+ axisAttrDebugInfo) )

//usage: char axisAttrStrings[AXISTATRSTRCOUNT] [AXISTATRMAXLEN]     = {AXISATRTYPESTRINGS};
#define AXISTATRMAXLEN     22
#define AXISTATRSTRCOUNT    9
#define AXISATRTYPESTRINGS \
	{"Axis Label"},\
	{"Axis Help"},\
	{"Axis Validity-Error-"},\
	{"Axis Min Value"},\
	{"Axis Max Value"},\
	{"Axis Scale"},\
	{"Axis Unit"},\
	{"Axis Debug Info"},\
	{"Axis Unknown"}
	

/*=*=*=*=*=*=*=*=*=*=*= Waveform Support =*=**=*=*=*=*=*=*=*=*/
/* WAVEFORM attributes */
typedef enum waveformAttrType_e
{
	waveformAttrLabel           = 0,
	waveformAttrHelp,
	waveformAttrHandling,
	waveformAttrEmphasis,
	waveformAttrLineType,
	waveformAttrLineColor,
	waveformAttrYAxis,
	waveformAttrKeyPtX,
	waveformAttrKeyPtY,
	waveformAttrType,
	waveformAttrXVals,
	waveformAttrYVals,
	waveformAttrXinitial,
	waveformAttrXIncr,
	waveformAttrPtCount,
	waveformAttrInitActions,
	waveformAttrRfshActions,
	waveformAttrExitActions,
	waveformAttrDebugInfo,
	waveformAttrValidity,       /* added 23jan07 - sjv - spec change */
	waveformAttr_Unknown
}
/*typedef */ waveformAttrType_t;

#define IS_WAVEDBG(a) ( a==((iT_Waveform <<8)+ waveformAttrDebugInfo) )

//usage: char waveformAttrStrings[WAVEFORMTATRSTRCOUNT] [WAVEFORMTATRMAXLEN]     = {WAVEFORMATRTYPESTRINGS};
#define WAVEFORMTATRMAXLEN     28
#define WAVEFORMTATRSTRCOUNT   21
#define WAVEFORMATRTYPESTRINGS \
	{"Waveform Label"},\
	{"Waveform Help"},\
	{"Waveform Handling"},\
	{"Waveform Emphasis"},\
	{"Waveform Line Type"},\
	{"Waveform Line Color"},\
	{"Waveform Y Axis"},\
	{"Waveform X Keypoints"},\
	{"Waveform Y Keypoints"},\
	{"Waveform Type"},\
	{"Waveform X Values"},\
	{"Waveform Y Values"},\
	{"Waveform Initial X"},\
	{"Waveform X Increment"},\
	{"Waveform Number of Points"},\
	{"Waveform Initial Actions"},\
	{"Waveform Refresh Actions"},\
	{"Waveform Exit Actions"},\
	{"Waveform Debug Info"},\
	{"Waveform Validity"},\
	{"Waveform Unknown"}

		

/*=*=*=*=*=*=*=*=*=*=*= Source Support =*=**=*=*=*=*=*=*=*=*/
/* SOURCE attributes */
typedef enum sourceAttrType_e
{
	sourceAttrLabel         = 0,
	sourceAttrHelp,
	sourceAttrValidity,
	sourceAttrEmphasis,
	sourceAttrLineType,
	sourceAttrLineColor,
	sourceAttrYAxis,
	sourceAttrMembers,
	sourceAttrDebugInfo,
	sourceAttrInitActions,      /* added 23jan07 - sjv - spec change */
	sourceAttrRfshActions,      /* added 23jan07 - sjv - spec change */
	sourceAttrExitActions,      /* added 23jan07 - sjv - spec change */
	sourceAttr_Unknown
}
/*typedef */ sourceAttrType_t;

#define IS_SRCDBG(a) ( a==((iT_Source <<8)+ sourceAttrDebugInfo) )

//usage: char sourceAttrStrings[SOURCETATRSTRCOUNT] [SOURCETATRMAXLEN]     = {SOURCEATRTYPESTRINGS};
#define SOURCETATRMAXLEN     24
#define SOURCETATRSTRCOUNT   13
#define SOURCEATRTYPESTRINGS \
	{"Source Label"},\
	{"Source Help"},\
	{"Source Validity"},\
	{"Source Emphasis"},\
	{"Source Line Type"},\
	{"Source Line Color"},\
	{"Source Y Axis"},\
	{"Source Members"},\
	{"Source Debug Info"},\
	{"Source Initial Actions"},\
	{"Source Refresh Actions"},\
	{"Source Exit Actions"},\
	{"Source Unknown"}



/*=*=*=*=*=*=*=*=*=*=*= List Support =*=**=*=*=*=*=*=*=*=*/
/* LIST attributes */
typedef enum listAttrType_e
{
	listAttrLabel           = 0,
	listAttrHelp,
	listAttrValidity,
	listAttrType,
	listAttrCount,
	listAttrCapacity,
	listAttrDebugInfo,
	listAttrFirst,
	listAttrLast,
	listAttrPrivate,
   listAttr_Unknown
}
/*typedef */ listAttrType_t;

#define IS_LISTDBG(a) ( a==((iT_List <<8)+ listAttrDebugInfo) )

//usage: char listAttrStrings[LISTTATRSTRCOUNT] [LISTTATRMAXLEN]     = {LISTATRTYPESTRINGS};
#define LISTTATRMAXLEN     17
#define LISTTATRSTRCOUNT   (listAttr_Unknown+1)
#define LISTATRTYPESTRINGS \
	{"List Label"},\
	{"List Help"},\
	{"List Validity"},\
	{"List Type"},\
	{"List Count"},\
	{"List Capacity"},\
	{"List Debug Info"},\
	{"List First"},\
	{"List Last"},\
	{"List Private"},\
	{"List Unknown"}    
		
extern const char listAttrStrings[LISTTATRSTRCOUNT] [LISTTATRMAXLEN];

/*=*=*=*=*=*=*=*=*=*=*= Grid Support =*=**=*=*=*=*=*=*=*=*/
/* GRID attributes */
typedef enum gridAttrType_e
{
	gridAttrLabel           = 0,
	gridAttrHelp,
	gridAttrValidity,
	gridAttrHeight,
	gridAttrWidth,
	gridAttrOrient,
	gridAttrHandling,
	gridAttrMembers,
	gridAttrDebugInfo,
	gridAttr_Unknown
}
/*typedef */ gridAttrType_t;

#define IS_GRIDDBG(a) ( a==((iT_Grid <<8)+ gridAttrDebugInfo) )

//usage: char gridAttrStrings[GRIDTATRSTRCOUNT] [GRIDTATRMAXLEN]     = {GRIDATRTYPESTRINGS};
#define GRIDTATRMAXLEN     18
#define GRIDTATRSTRCOUNT   10
#define GRIDATRTYPESTRINGS \
	{"Grid Label"},\
	{"Grid Help"},\
	{"Grid Validity"},\
	{"Grid Height"},\
	{"Grid Width"},\
	{"Grid Orientation"},\
	{"Grid Handling"},\
	{"Grid Members"},\
	{"Grid Debug Info"},\
	{"Grid Unknown"}    
extern const char gridAttrStrings[GRIDTATRSTRCOUNT] [GRIDTATRMAXLEN];

typedef enum gridOrient_e
{
	gd_Unknown,     // 0 - Not defined
	gd_Vertical,    // 1 - By Column
	gd_Horizontal   // 2 - By Row
}
/*typedef*/ gridOrient_t;

//usage: char gridOrientStrings[GRIDORIENTSTRCOUNT] [GRIDORIENTMAXLEN]     = {GRIDORIENTSTRINGS};
#define GRIDORIENTMAXLEN     23
#define GRIDORIENTSTRCOUNT    3
#define GRIDORIENTSTRINGS \
	{"Orientation Unknown"},\
	{"Orientation By Column"},\
	{"Orientation By Row"}


/*=*=*=*=*=*=*=*=*=*=*= Image Support =*=**=*=*=*=*=*=*=*=*/
/* IMAGE attributes */
typedef enum imageAttrType_e
{
	imageAttrLabel          = 0,
	imageAttrHelp,
	imageAttrValidity,
	imageAttrLink,
	imageAttrPath,
	imageAttrDebugInfo,
	imageAttr_Unknown
}
/*typedef */ imageAttrType_t;

#define IS_IMGDBG(a) ( a==((iT_Image <<8)+ imageAttrDebugInfo) )

//usage: char imageAttrStrings[IMAGETATRSTRCOUNT] [IMAGETATRMAXLEN]     = {IMAGEATRTYPESTRINGS};
#define IMAGETATRMAXLEN     18
#define IMAGETATRSTRCOUNT    7
#define IMAGEATRTYPESTRINGS \
	{"Image Label"},\
	{"Image Help"},\
	{"Image Validity"},\
	{"Image Link"},\
	{"Image Path"},\
	{"Image Debug Info"},\
	{"Image Unknown"}   

extern const char imageAttrStrings[IMAGETATRSTRCOUNT] [IMAGETATRMAXLEN];

/*=*=*=*=*=*=*=*=*=*=*= Blob Support =*=**=*=*=*=*=*=*=*=*/
/* BLOB attributes */
typedef enum blobAttrType_e
{
	blobAttrLabel           = 0,
	blobAttrHelp,
	blobAttrHandling,
	blobAttrIdentity,
	blobAttrDebugInfo,
	blobAttr_Unknown
}
/*typedef */ blobAttrType_t;

#define IS_BLOBDBG(a) ( a==((iT_Blob <<8)+ blobAttrDebugInfo) )

//usage: char blobAttrStrings[BLOBATTRSTRCOUNT] [BLOBATTRMAXLEN]     = {BLOBATRTYPESTRINGS};
#define BLOBATTRMAXLEN     18
#define BLOBATTRSTRCOUNT    7
#define BLOBATRTYPESTRINGS \
	{"Blob Label"},\
	{"Blob Help"},\
	{"Blob Handling"},\
	{"Blob Identity"},\
	{"Blob Debug Info"},\
	{"Blob Unknown"}    

extern const char blobAttrStrings[BLOBATTRSTRCOUNT] [BLOBATTRMAXLEN];


/*=*=*=*=*=*=*=*=*=*=*= Template Support =*=**=*=*=*=*=*=*=*=*/
/* TEMPLATE attributes */
typedef enum templateAttrType_e
{
	templAttrLabel			= 0,
	templAttrHelp,
	templAttrValidity,
	templAttrDefaultValues,
	templAttrDebugInfo,
	templAttr_Unknown
}
/*typedef */ templAttrType_t;

#define IS_TEMPDBG(a) ( a==((iT_Template <<8)+ templAttrDebugInfo) )

//usage: char templAttrStrings[TEMPLATTRSTRCOUNT] [TEMPLATTRMAXLEN]    = {TEMPLATRTYPESTRINGS};
#define TEMPLATTRMAXLEN     25
#define TEMPLATTRSTRCOUNT    7
#define TEMPLATRTYPESTRINGS \
	{"Template Label"},\
	{"Template Help"},\
	{"Template Validity"},\
	{"Template Default Values"},\
	{"Template Debug Info"},\
	{"Template Unknown"}	

extern const char templAttrStrings[TEMPLATTRSTRCOUNT] [TEMPLATTRMAXLEN];

/*=*=*=*=*=*=*=*=*=*=*= Plugin Support =*=**=*=*=*=*=*=*=*=*/
/* PLUGIN attributes */
typedef enum pluginAttrType_e
{
	pluginAttrLabel			= 0,
	pluginAttrHelp,
	pluginAttrValidity,
	pluginAttrVisibility,
	pluginAttrUUID,
	pluginAttrDebugInfo,
	pluginAttr_Unknown
}
/*typedef */ pluginAttrType_t;

#define IS_PLUGDBG(a) ( a==((iT_PlugIn <<8)+ pluginAttrDebugInfo) )

//usage: char pluginAttrStrings[PLUGINATTRSTRCOUNT] [PLUGINATTRMAXLEN]= {PLUGINATRTYPESTRINGS};
#define PLUGINATTRMAXLEN     18
#define PLUGINATTRSTRCOUNT    7
#define PLUGINATRTYPESTRINGS \
	{"Plugin Label"},\
	{"Plugin Help"},\
	{"Plugin Validity"},\
	{"Plugin Visibility"},\
	{"Plugin GUUID"},\
	{"Plugin Debug Info"},\
	{"Plugin Unknown"}	

extern const char pluginAttrStrings[PLUGINATTRSTRCOUNT] [PLUGINATTRMAXLEN];


/*=*=*=*=*=*=*=*=*=*=*=*= Attr Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 *  generic usage attribute enum
 *
 */

typedef union genericAttrTypeEnum_u
{
	varAttrType_t       gAttrVar;       // largest enum..used for conversions
	cmdAttrType_t       gAttrCmd;
	menuAttrType_t      gAttrMenu;
	eddispAttrType_t    gAttrEdDisp;
	methodAttrType_t    gAttrMethod;
	refreshAttrType_t   gAttrRefresh;
	unitAttrType_t      gAttrUnit;
	waoAttrType_t       gAttrWao;
	grpItmAttrType_t    gAttrGrpItm;//collections,itemarrays,files
	blockAttrType_e     gAttrBlk;
	progAttrType_t      gAttrProg;
	recordAttrType_t    gAttrRec;
	arrayAttrType_t     gAttrArray;
	varlistAttrType_t   gAttrVarLst;
	respcodeAttrType_e  gAttrRespCd;
	domainAttrType_t    gAttrDomain;
	memberType_t        gAttrMember;
//  fileAttrType_t      gAttrFile;
	chartAttrType_t     gAttrChart;
	graphAttrType_t     gAttrGraph;
	axisAttrType_t      gAttrAxis;
	waveformAttrType_t  gAttrWaveform;
	sourceAttrType_t    gAttrSource;
	listAttrType_t      gAttrList;
	gridAttrType_t      gAttrGrid;
	imageAttrType_t     gAttrImage;
	blobAttrType_t      gAttrBlob;
	templAttrType_t     gAttrTemplate;
	pluginAttrType_t    gAttrPlugin;
	// conversion to and from int
	operator unsigned int() {return ((unsigned int)gAttrVar);};
//	union genericAttrTypeEnum_u(const unsigned int& i=-1){gAttrVar=(varAttrType_t)i;};	
	genericAttrTypeEnum_u(const unsigned int& i=-1){gAttrVar=(varAttrType_t)i;};
}
/*typedef*/genericTypeEnum_t;


#define IS_DEBUG(K) (IS_VARDBG(K) ||IS_CMDDBG(K) ||IS_MENDBG(K) ||IS_EDDDBG(K)||IS_METHDBG(K)||\
	  IS_RFSHDBG(K)||IS_UNITDBG(K)||IS_COLLDBG(K)||IS_RARRDBG(K)||IS_ARRDBG(K)||IS_FILEDBG(K)||\
	  IS_CHRTDBG(K)||IS_GRPHDBG(K)||IS_AXISDBG(K)||IS_WAVEDBG(K)||IS_SRCDBG(K)||IS_LISTDBG(K)||\
	  IS_GRIDDBG(K)||IS_IMGDBG(K) ||IS_BLOBDBG(K)||IS_PLUGDBG(K)||IS_TEMPDBG(K)||IS_WAODBG(K)  )

/*=*=*=*=*=*=*=*=*=*=*=*= Item Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 *  Types of items
 *
 */
typedef enum itemType_e
{
	iT_ReservedZeta,        // 0
	iT_Variable,            // 1
	iT_Command,             // 2
	iT_Menu,                // 3
	iT_EditDisplay,         // 4
	iT_Method,              // 5
	iT_Refresh,/*relation*/ // 6
	iT_Unit,/*relation*/    // 7
	iT_WaO,                 // 8
	iT_ItemArray,           // 9
	iT_Collection,          //10
	iT_ReservedOne,             // specially used to get the image list
	iT_Block,               //12
	iT_Program,             //13
	iT_Record,              //14
	iT_Array,               //15
	iT_VariableList,        //16
	iT_ResponseCodes,       //17
	iT_Domain,              //18
	iT_Member,              //19
	iT_File,                //20
	iT_Chart,               //21
	iT_Graph,               //22
	iT_Axis,                //23
	iT_Waveform,            //24
	iT_Source,              //25
	iT_List,                //26
	iT_Grid,                //27
	iT_Image,               //28
	iT_Blob,                //29
	iT_PlugIn,				//30
	iT_Template,			//31
	iT_ReservedTwo,			//30
	iT_NotHartOne,
	iT_NotHartTwo,
	iT_NotHartThre,
	iT_NotHartFour,
	iT_ReservedThre,		//35
	iT_MaxType,				//  36
/*
 *  Special object type values
 */
	iT_AmorphousArray = 63,

	iT_FormatObject = 128,
	iT_DeviceDirectory,     //129
	iT_BlockDirectory,      //130
/* these additional ITYPES are used by DDS
 *       'resolve' function to build resolve trails 
 */
	iT_Parameter = 200,
	iT_ParameterList,       //201
	iT_BlockCharacteristic, //202

	//it_AmorphousArray	= 250, // a transitory type used in the tokenizer

	iT_NotAnItemType = 255, // probably a constant
	iT_Critical_Item = 256

}  /*typedef*/ itemType_t;


//usage: itemStrings[ITMSTRCOUNT][ITMAXLEN] = {ITEMTYPESTRINGS};
#define ITMAXLEN     24
#define ITMSTRCOUNT  46
#define ITEMTYPESTRINGS \
	{"Reserved Zeta"},\
	{"Variable Type"},\
	{"Command Type"},\
	{"Menu Type"},\
	{"Edit Display Type"},\
	{"Method Type"},\
	{"Refresh Type"},\
	{"Unit Type"},\
	{"Write as One Type"},\
	{"Item Array Type"},\
	{"Collection Type"},\
	{"Reserved One"},\
	{"Block Type"},\
	{"Program Type"},\
	{"Record Type"},\
	{"Array Type"},\
	{"Variable List Type"},\
	{"Response Codes Type"},\
	{"Domain Type"},\
	{"Member Type"},\
	{"File Type"},\
	{"Chart Type"},\
	{"Graph Type"},\
	{"Axis Type"},\
	{"Waveform Type"},\
	{"Source Type"},\
	{"List Type"},\
	{"Grid Type"},\
	{"Image Type"},\
	{"Blob Type"},\
	{"PlugIn Type"},\
	{"Template Type"},\
	{"Reserved Two"},\
	{"Not HART One"},\
	{"Not HART Two"},\
	{"Not HART Three"},\
	{"Not HART Four"},\
	{"Reserved Three"},\
	{"MAXIMUM TYPE"},/*38*/\
	{"Format Type"},/*128*/\
	{"Dev Dir Type"},/*129*/\
	{"Block Dir Type"},/*130*/\
	{"Parameter Type"},/*200*/\
	{"Parameter List Type"},/*201*/\
	{"Block Char Type"},/*202*/\
	{"Crit Item Type"}/*256*/


bool isUsableItemType(itemType_t it);

typedef enum referenceType_e
{   
	rT_Item_id,         // 0___undocumented                 // to ZERO
	rT_ItemArray_id,        // 1                                // to ITEM_ARRAY_ITYPE
	rT_Collect_id,      // 2                                // to COLLECTION_ITYPE
	rT_viaItemArray,    // 3  exp-index, ref-array          // to  ITEM_ARRAY_ITYPE
	rT_via_Collect,     // 4  member-itemId, ref-collection // to  COLLECTION_ITYPE
	rT_via_Record,      // 5  member-itemId, ref-record     // to  RECORD_ITYPE,
	rT_via_Array,       // 6  exp-index, ref-array          // to  ARRAY_ITYPE,
	rT_via_VarList,     // 7  member-itemId, ref-var list   // to  VAR_LIST_ITYPE,
	rT_via_Param,       // 8  int: parameter member id      // to  PARAM_ITYPE,-------special  200
	rT_via_ParamList,   // 9  int: param-list-name          // to  PARAM_LIST_ITYPE---special  201
	rT_via_Block,       //10  int: member name              // to  BLOCK_ITYPE,<BLOCK_CHAR_ITYPE in resolve>-special 202
	rT_Block_id,        //11                                // to BLOCK_ITYPE,
	rT_Variable_id,     //12                                // to VARIABLE_ITYPE,
	rT_Menu_id,         //13                                // to MENU_ITYPE,
	rT_Edit_Display_id, //14                                // to EDIT_DISP_ITYPE,
	rT_Method_id,       //15                                // to METHOD_ITYPE,
	rT_Refresh_id,      //16                                // to REFRESH_ITYPE,
	rT_Unit_id,         //17                                // to UNIT_ITYPE,
	rT_WAO_id,          //18                                // to WAO_ITYPE,
	rT_Record_id,       //19                                // to RECORD_ITYPE,
	rT_Array_id,        //20___undocumented                 // to ARRAY_ITYPE,
	rT_VarList_id,      //21___undocumented                 // to VAR_LIST_ITYPE,
	rT_Program_id,      //22___undocumented                 // to PROGRAM_ITYPE,
	rT_Domain_id,       //23___undocumented                 // to DOMAIN_ITYPE,
	rT_ResponseCode_id, //24___undocumented                 // to RESP_CODES_ITYPE

	/* new reference types */
	rT_File_id,
	rT_Chart_id,
	rT_Graph_id,
	rT_Axis_id,
	rT_Waveform_id,
	rT_Source_id,       //30
	rT_List_id,

	rT_Image_id,
	rT_Separator,       /* == colbreak */
	rT_Constant,
	rT_via_File,        // 35
	rT_via_List,
	rT_via_BitEnum,
	rT_Grid_id,
	rT_Row_Break,
	rT_via_Chart,       // 40
	rT_via_Graph,
	rT_via_Source,
	rT_via_Attr,
	rT_Blob_id,
	rT_via_Blob,        // 45
	rT_Template_id,		// 46
	rT_Plugin_id,		// 47
	rT_NotA_RefType     // 48

	// for tokenizer - intermediate reference types
   ,rT_SymbolRef		= 501,	// TOK_S_REF 501 x1f5
	rT_CollectRef,				// TOK_C_REF 502 x1f6... dot reference
	rT_ArrayRef,				// TOK_A_REF 503 x1f7... bracket reference
	rT_ParamRef,								// TOK_P_REF Parameter
	rT_ParamLstRef,								// TOK_L_REF ParameterList
	rT_BlockRef,								// TOK_B_REF Block
	rT_ConstEnumRef,			// TOK_N_REF 507 x1fB
	rT_ConstIntRef,				// TOK_I_REF 508 x1fC
	rT_ConstFloatRef,			// TOK_F_REF 509 x1fD
	rT_ConstStrRef,				// TOK_R_REF 510 x1fE
	rT_ImageRef,				// TOK_M_REF 511 x1fF
	rT_OneBitRef,				// TOK_T_REF 512 x200
	rT_AttrRef,					// TOK_D_REF 513 x201
	rT_MethLocalRef				// TOK_V_REF -- 514 x202
}
/*typedef*/referenceType_t;

//usage: char refTypeStrings[REFSTRCOUNT][REFMAXLEN] = {REFTYPESTRINGS};
#define REFMAXLEN     24
#define REFSTRCOUNT   47
#define REFTYPESTRINGS \
	{"<Item Id>"},\
	{"Item Array ID"},\
	{"Collection ID"},\
	{"via Item Array"},\
	{"via Collection"},\
	{"via Record"},/*5*/\
	{"via Array"},\
	{"via Variable List"},\
	{"via Parameter"},\
	{"via Parameter List"},\
	{"via Block"},/*10*/\
	{"Block ID"},\
	{"Variable ID"},\
	{"Menu ID"},\
	{"Edit Display ID"},\
	{"Method ID"},/*15*/\
	{"Refresh ID"},\
	{"Unit ID"},\
	{"WaO ID"},\
	{"Record ID"},\
	{"Array ID"},/*20*/\
	{"<Variable List ID>"},\
	{"<Program ID>"},\
	{"<Domain ID>"},\
	{"<Response Code ID>"},\
{"File ID"},/*25*/\
{"Chart ID"},\
{"Graph ID"},\
{"Axis ID"},\
{"Waveform ID"},\
{"Source ID"},/*30*/\
{"List ID"},\
{"Image"},\
{"Separator"},\
{"Constant"},\
{"via File"},/*35*/\
{"via List"},\
{"via Bit Enum"},\
{"Grid ID"},\
{"Row Break"},\
{"via Chart"},/*40*/\
{"via Graph"},\
{"via Source"},\
{"via Attribute"},\
{"Blob ID"},\
{"via Blob"},/*45*/\
	{"<Not a Reference Type>"}/*44*/


typedef enum refRequirment_e
{   
	rr_mustBeRef,		// 0	// no requirement
	rr_mustBeNumeric,	// 1    // final result must be a number type
	rr_mustBeString		// 2    // final result must be a string type
}
/*typedef*/refRequirment_t;

/*=*=*=*=*=*=*=*=*=*=*=*= Graphics Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 *  Types of ...stuff
 *
 */
typedef enum grafixSize_e
{
	gS_Undefined,       // 0    
	gS_xx_Small,        // 1    XX_SMALL_DISPSIZE
	gS__xSmall,         // 2    X_SMALL_DISPSIZE
	gS_Small,           // 3    SMALL_DISPSIZE
	gS_Medium,          // 4    MEDIUM_DISPSIZE
	gS_Large,           // 5    LARGE_DISPSIZE
	gS__x_Large,        // 6    X_LARGE_DISPSIZE
	gS_xx_Large,        // 7    XX_LARGE_DISPSIZE
	gS_xxxSmall,        // 8    XXX_SMALL
	gS_Unknown          // 9    MAX_SIZE
}
/*typedef*/  grafixSize_t;

//usage: char grafSizeStrings[GRAFSIZESTRCOUNT] [GRAFSIZEMAXLEN] = {GRAFSIZESTRINGS};
#define GRAFSIZEMAXLEN     10
#define GRAFSIZESTRCOUNT    9
#define GRAFSIZESTRINGS     {"UNDEFINED"},{"XX_SMALL"},{"X_SMALL"},{"SMALL"},{"MEDIUM"},\
							{"LARGE"},{"X_LARGE"},{"XX_LARGE"},{"XXX_SMALL"}


typedef enum waveformType_e
{
	wT_Undefined,       // 0    
	wT_Y_Time,          // 1    YT_WAVEFORM_TYPE
	wT_Y_X,             // 2    XY_WAVEFORM_TYPE
	wT_Horiz,           // 3    HORZ_WAVEFORM_TYPE
	wT_Vert,            // 4    VERT_WAVEFORM_TYPE
	wT_Unknown          // 8    MAX_SIZE
}
/*typedef*/  waveformType_t;

//usage: char waveTypeStrings[WAVETYPESTRCOUNT] [WAVETYPEMAXLEN] = {WAVETYPESTRINGS};
#define WAVETYPEMAXLEN     12
#define WAVETYPESTRCOUNT    6
#define WAVETYPESTRINGS     {"UNDEFINED"},{"Y vs T"},{"Y vs X"},{"Horiz Line"},{"Vert Line"},\
							{"UNKNOWN"}


typedef enum chartType_e
{
	cT_Undefined,       // 0    
	cT_Gauge,           // 1    GAUGE_CTYPE     
	cT_Horiz,           // 2    HORIZ_BAR_CTYPE 
	cT_Scope,           // 3    SCOPE_CTYPE     
	cT_Strip,           // 4    STRIP_CTYPE 
	cT_Sweep,           // 5    SWEEP_CTYPE 
	cT_Vert,            // 5    VERT_BAR_CTYPE  
	cT_UNKNOWN          // 8            
}
/*typedef*/  chartType_t;

//usage: char chartTypeStrings[CHARTTYPESTRCOUNT] [CHARTTYPEMAXLEN] = {CHARTTYPESTRINGS};
#define CHARTTYPEMAXLEN     10
#define CHARTTYPESTRCOUNT    8
#define CHARTTYPESTRINGS    {"UNDEFINED"},{"Gauge"},{"Horiz Bar"},{"Scope"},\
{"Strip"},{"Sweep"},{"Vert Bar"},{"UNKNOWN"}

#define lineTypeOffset (10)
typedef enum lineType_e
{
	lT_Undefined,       // 0    
	lT_Data,            // 1    DATA_LINETYPE       
	lT_LowLow,          // 2    LOWLOW_LINETYPE     
	lT_Low,             // 3    LOW_LINETYPE        
	lT_High,            // 4    HIGH_LINETYPE       
	lT_HighHigh,        // 5    HIGHHIGH_LINETYPE   
	lT_Trans,           // 6    TRANSPARENT_LINETYPE
	lT_Unknown,         // 7
	lT_Data1 = ( lineTypeOffset + 1),
	lT_Data2,
	lT_Data3,
	lT_Data4,
	lT_Data5,
	lT_Data6,
	lT_Data7,
	lT_Data8,
	lT_Data9,
	lT_END
}
/*typedef*/  lineType_t;

//usage: char lineTypeStrings[LINETYPESTRCOUNT] [LINETYPEMAXLEN] = {LINETYPESTRINGS};
#define LINETYPEMAXLEN     12
#define LINETYPESTRCOUNT   20
#define LINETYPESTRINGS     {"UNDEFINED"},{"Data"},{"Low-Low"},{"Low"},\
	{"High"},{"High-High"},{"Transparent"},{"UNKNOWN"},{""},{""},{""},\
	{"Data1"},{"Data2"},{"Data3"},{"Data4"},{"Data5"},{"Data6"},{"Data7"},{"Data8"},{"Data9"}


typedef enum scale_e
{
	sT_Undefined,       // 0    
	sT_Linear,          // 1    LINEAR_SCALE        
	sT_Log,             // 2    LOG_SCALE           
	sT_Uknown           // 3            
}
/*typedef*/  scale_t;

//usage: char scaleStrings[SCALESTRCOUNT] [SCALEMAXLEN] = {SCALESTRINGS};
#define SCALEMAXLEN     12
#define SCALESTRCOUNT    4
#define SCALESTRINGS    {"UNDEFINED"},{"Linear"},{"Logrithmic"},{"UNKNOWN"}

/*Vibhor 071204: Start of code*/
typedef enum actionType_e
{
	 eT_actionInit = 0
	,eT_actionRefresh
	,eT_actionExit
}   actionType_t;

/*Vibhor 071204: End of code*/
#ifdef MOVED_THESE_TO_DDLDEFS_H_FILE
/* names for A version attributes */
//usage: char AattrNameStrings[ATTRNAMESTRCOUNT] [ATTRNAMEMAXLEN] = {ATTRNAMESTRINGS};
#define ATTRNAMEMAXLEN     12
#define ATTRNAMESTRCOUNT   20
#define ATTRNAMESTRINGS     {"item_information"},{"label"},{"help"},{"validity"},\
{"members"},{"handling"},{"response_codes"},{"height"},{"post_edit_actions"},\
{"pre_edit_actions"},{"refresh_actions"},{"relation_update_list"},{"relation_watch_list"},\
{"relation_watch_variable"},{"width"},{"access"},{"class"},{"constant_unit"},\
{"cycle_time"},{"emphasis"},{"exit_actions"},{"identity"},{"init_actions"},\
{"line_color"},{"line_type"},{"max_value"},{"min_value"},{"command_number"},\
{"post_read_actions"},{"post_write_actions"},{"pre_read_actions"},\
{"pre_write_actions"},{"type_definition"},{"y_axis"},{"appinstance"},\
{"axis_items"},{"block_b"},{"block_b_type"},{"capacity"},{"characteristics"},\
{"chart_items"},{"chart_type"},{"charts"},{"collection_items"},{"block_number"},\
{"count"},{"default_value"},{"default_values"},{"definition"},{"display_format"},\
{"display_items"},{"edit_display_items"},{"edit_format"},{"edit_items"},{"elements"},\
{"enumerations"},{"file_items"},{"first"},{"graph_items"},{"graphs"},{"grid_items"},\
{"grids"},{"image_items"},{"image-table-index"},{"index"},{"indexed"},{"initial_value"},\
{"items"},{"keypoints-x-values"},{"keypoints-y-values"},{"last"},{"length"},{"link"},\
{"list_items"},{"lists"},{"local_parameters"},{"menu_items"},{"menus"},{"method_items"},\
{"method_parameters"},{"method_type"},{"methods"},{"number_of_elements"},\
{"number_of_points"},{"on_update_actions"},{"operation"},{"orientation"},\
{"parameter_lists"},{"parameters"},{"post-user-actions"},{"post-rqst-actions"},\
{"universally-unique-id"},{"reference_array_items"},{"refresh_items"},{"scaling"},\
{"scaling_factor"},{"slot"},{"source_items"},{"style"},{"sub_slot"},{"time-format"},\
{"time-scale"},{"transactions"},{"unit_items"},{"variable_type"},{"vectors"},\
{"waveform_items"},{"waveform_type"},{"write_as_one_items"},{"component_byte_order"},\
{"x_axis"},{"x-increment"},{"x-initial"},{"x-values"},{"y-values"},{"view-min"},{"view-max"},\
{"reserved117"},{"reserved118"},{"reserved119"},{"addressing"},{"can_delete"},\
{"check_configuration"},{"classification"},{"component_parent"},{"component_path"},\
{"component_relations"},{"components"},{"declaration"},{"detect"},{"device_revision"},\
{"device_type"},{"edd"},{"manufacturer"},{"maximum_number"},{"minimum_number"},{"protocol"},\
{"redundancy"},{"relation_type"},{"component_connect_point"},{"scan"},{"scan_list"},\
{"private"},{"visibility"},{"product_uri"},{"api"},{"header"},{"write_mode"},{"plugin_items"},\
{"files"},{"plugins"},{"initial_values"},{"post-rqstreceive-actions"},{"shared"},{"variable_status"}
#endif // MOVED
#endif // _DDBDEFS_H
 /*programming note:
   pStyleStr isa pointer to an array of const char arrays 
   const char (*pStyleStr)[MENUSTYLESTRCOUNT][MENUSTYLEMAXLEN] = &menuStyleStrings;
*/
/*************************************************************************************************
 *
 *   $History: ddbdefs.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/Common
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */

