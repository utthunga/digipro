/*************************************************************************************************
*
* Workfile: TLStorage.h
* 28mar16 - stevev
**
*************************************************************************************************
* The content of this file is the
*     Proprietary and Confidential property of the HART Communication Foundation
* Copyright (c) 2016, FieldComm Group, All Rights Reserved
*************************************************************************************************
*
* Description:
*		1/27/2016 	sjv	created to handle Thread Local Storage, specifically for the new Parser
*
*
* #include "TLStorage.h"
*/
#pragma once
#ifndef _TLSTORAGE_H
#define _TLSTORAGE_H

#ifdef INC_DEBUG
#pragma message("In TLStorage.h") 
#endif


#ifdef INC_DEBUG
#pragma message("    Finished Includes::TLStorage.h") 
#endif

extern unsigned TLSindex;  // we'll all use the same slot
extern void* TLSmainThread;

// main thread functions
void prepTLS(size_t allocSize); // sets the index, allocs data main thread of allocSize
void exitTLS(void);  //deletes the main thread's memory and frees the index


// thread functions
void* allocTLS(size_t allocSize);// at thread entry; use returned pointer to initialize
void  deAllocTLS(void);// at thread exit...this deallocates the pointers, NOT the memory therein

void* getTLS(void);  // get the pointer for use, cast it to your type


#endif  //_TLSTORAGE_H