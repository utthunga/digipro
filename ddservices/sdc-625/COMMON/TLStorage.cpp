/*************************************************************************************************
*
* Workfile: TLStorage.cpp
* 28mar16- stevev
**
*************************************************************************************************
* The content of this file is the
*     Proprietary and Confidential property of the HART Communication Foundation
* Copyright (c) 2016, FieldComm Group Inc, All Rights Reserved
*************************************************************************************************
*
* Description:
*		3/28/16	sjv	started creation
*/


#include "TLStorage.h"
#include "logging.h"

#include <windows.h> 

unsigned TLSindex;// we're using one...it's a class that can hold whatever globals you need
#pragma message("------Compiling TLStorage.cpp")

void* TLSmainThread = NULL;
#ifdef _DEBUG
class parseGlbl;
volatile parseGlbl* pTLS;
#endif


void prepTLS(size_t allocSize)// main thread only
{
	if ((TLSindex = TlsAlloc()) == TLS_OUT_OF_INDEXES)
	{
		LOGIT(CLOG_LOG | CERR_LOG, "TlsAlloc failed\n");
		TLSindex = 0;
		return;
	}
	TLSmainThread = (void*)LocalAlloc(LPTR, allocSize);
	if (!TlsSetValue(TLSindex, TLSmainThread))
	{
		LOGIT(CLOG_LOG | CERR_LOG, "TlsSetValue failed in prep\n");
		TlsSetValue(TLSindex, 0);
		return;
	}
}


void exitTLS(void)
{
	if (TLSmainThread)
	{
		//delete TLSmainThread;
		deAllocTLS();

		TLSmainThread = NULL;
	}
	TlsFree(TLSindex);
}


// get the pointer for use, cast it to your type
void* getTLS(void)
{
	void* pLocal = TlsGetValue(TLSindex);
	if ((pLocal == 0) && (GetLastError() != ERROR_SUCCESS))
	{
		LOGIT(CLOG_LOG | CERR_LOG, "TlsGetValue error\n");
		return NULL;
	}
	return pLocal;
}

// start of thread - returned pointer is to initialize the internals of whatever is being pointed to
void*  allocTLS(size_t allocSize)
{
	void* pLocal =	(void*)LocalAlloc(LPTR, allocSize);
	if (!TlsSetValue(TLSindex, pLocal))
	{
		LOGIT(CLOG_LOG | CERR_LOG, "TlsSetValue failed\n");
		TlsSetValue(TLSindex, 0);
		return NULL;
	}
	return pLocal;
}

// end of thread
void deAllocTLS(void)
{
	void* pLocal = TlsGetValue(TLSindex);
	if (pLocal != 0)
	{
		LocalFree((HLOCAL)pLocal);
		TlsSetValue(TLSindex, 0);
	}
}
