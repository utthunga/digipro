/*************************************************************************************************
 *
 * $Workfile: SelectTree.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */

#include "stdafx.h"
//#include "XmtrDD.h"
#include "ddbGeneral.h"
#include "DDSelectTree.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectTree

CSelectTree::CSelectTree()
{
	pStrPath = NULL;
	pDDKey   = NULL;
	pMyDlg   = NULL;
// Added By Deepak
	pMyListCtrl = NULL;
// END modification
}

CSelectTree::~CSelectTree()
{
}


BEGIN_MESSAGE_MAP(CSelectTree, CTreeCtrl)
	//{{AFX_MSG_MAP(CSelectTree)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnSelchanging)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectTree message handlers

void CSelectTree::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	//
	
	DD_Key_t longValue, tmp;			
	UINT nMask = GetItemState((pNMTreeView -> itemNew).hItem,0xFF);
	HTREEITEM hTIwrk, hTI = (pNMTreeView -> itemNew).hItem;

	if(!(nMask & TVIS_EXPANDEDONCE))  //the item is first time expanding
	{	
		if( hTI != GetRootItem())
		{	//BeginWaitCursor();
			
			  // fill the opening item with some information here (if we do JIT filling)

			//EndWaitCursor();			
		}
	}

//	if(pNMTreeView -> action == TVE_COLLAPSE)
//		  SetItemImage((pNMTreeView -> itemNew).hItem,m_nClsFolderImage,m_nClsFolderImage);
	//
//	if(pNMTreeView -> action == TVE_EXPAND)
//		SetItemImage((pNMTreeView -> itemNew).hItem,m_nOpenFolderImage,m_nOpenFolderImage);

//	if(pNMTreeView -> action == TVE_EXPAND)
//		SetItemImage((pNMTreeView -> itemNew).hItem,m_nOpenFolderImage,m_nOpenFolderImage);

	if ( pStrPath == NULL || pDDKey == NULL )
	{
		*pResult = 0;
		return;
	}
	if( (nMask & TVIS_EXPANDED)  || (nMask & TVIS_SELECTED) || (nMask & TVIS_EXPANDEDONCE) )
	{
		int i;

		*pStrPath = _T("");
		CString s;
		longValue = GetItemData(hTI);
		hTIwrk = hTI;

		while ( (longValue & 0x0F0000) > 0 )// not the root
		{
			i = ( (TLF_ISDDREV - (longValue & 0x0F0000)) >> 16) * 16;
			tmp = 0xFFFF;
			*pDDKey &= ( ~( tmp << i) );// clear the bits
			*pDDKey |= (  (longValue & 0xFFFF) << i );// set the new value

			if ( (longValue & 0x0F0000) == TLF_ISDDREV )
			{
				s.Format(_T("%02x"),longValue & 0xFFFF);
			}
			else
			{
				//s.Format("%02x\\%s",(longValue & 0xFFFF),*pStrPath);				
				s.Format(_T("%02x\\"),(longValue & 0xFFFF));
				s += *pStrPath;
			}

			hTIwrk    = GetParentItem( hTIwrk );
			longValue = GetItemData  ( hTIwrk );
			*pStrPath = s;
		}

	} //else nop (I wonder why we were called)
	// strPath holds the current path

	*pResult = 0;
}

void CSelectTree::OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	DD_Key_t longValue, tmp;		// max shift left is too small if not long long	
	UINT nMask = GetItemState((pNMTreeView -> itemNew).hItem,0xFF);
	HTREEITEM hTIwrk, hTI = (pNMTreeView -> itemNew).hItem;

	if ( pStrPath == NULL || pDDKey == NULL )
	{
		*pResult = 0;
		return;
	}
	if( 1 ) //(nMask & TVIS_EXPANDED)  || (nMask & TVIS_SELECTED) || (nMask & TVIS_EXPANDEDONCE) )
	{
		int i;

		*pStrPath = _T("");
		CString s;
		longValue = GetItemData(hTI);
		hTIwrk = hTI;
		binaryFormat_t ver = (binaryFormat_t)(longValue & 0xF00000);


		while ( (longValue & 0x0F0000) > 0 )// not the root
		{
			i = ( (TLF_ISDDREV - (longValue & 0x0F0000)) >> 16) * 16;
			tmp = 0xFFFF;
			*pDDKey &= ( ~( tmp << i) );// clear the bits
			*pDDKey |= (  (longValue & 0xFFFF) << i );// set the new value

			if ( (longValue & 0x0F0000) == TLF_ISDDREV )
			{
				s.Format(_T("%02x"),longValue & 0xFFFF);

// Added By Deepak 
// We have reached the DDRev Node. 
//This shall be displayed in the List Control 				
				CString str;
				if ( ver == BFF_ISV_6 )
				{
					str.Format (_T("%02x (enhanced)"),longValue & 0x00FF);
				}
				else
				if ( ver == BFF_ISV_8 )
				{
					str.Format (_T("%02x (enhanced with UTF8)"),longValue & 0x00FF);
				}
				else // catch all
				{
					str.Format (_T("%02x (unenhanced or unknown)"),longValue & 0x00FF);
				}

				if(pMyListCtrl)
				{
					pMyListCtrl->DeleteAllItems ();
					pMyListCtrl->InsertItem(0,str);
				}
//END modification
			}
			else
			{
				//s.Format("%02x\\%s",(longValue & 0xFFFF),*pStrPath);
				s.Format(_T("%02x\\"),(longValue & 0xFFFF));
				s += *pStrPath;
								
			}

			hTIwrk    = GetParentItem( hTIwrk );
			longValue = GetItemData  ( hTIwrk );
			*pStrPath = s;
		}
	
/*		LV_ITEM lvi;
		lvi.mask = LVIF_TEXT � LVIF_IMAGE � LVIF_PARAM; 
		lvi.iItem = 0; // this is only item we can have in list ctrl
		lvi.iSubItem = 0; 
		lvi.iImage = m_imgList.GetImage(0);
		
		lvi.pszText = str; 
		lvi.lParam = (LPARAM) hTI;
*/

		if ( pMyDlg != NULL )
		{
			pMyDlg->UpdateData(FALSE);
		}

	} //else nop (I wonder why we were called)
	// strPath holds the current path
	
	*pResult = 0;
}


/*************************************************************************************************
 *
 *   $History: SelectTree.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
