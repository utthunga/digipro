/*   Dict.h

  03/18/02	sjv	created from 2 classes (dictload & dictdump)

*/

#ifndef _DICT_H_
#define _DICT_H_

#pragma warning (disable : 4786) 

#include "vista.h"
#include "deviceDict.h"
//#include "rtnCode.h"
//<<temp<< #include "dbBase.h"

#include "ddbGeneral.h"

#include < basetsd.h >
#include < string >
#include < vector >
#include < iostream >

//gen using namespace std;

//gen typedef  unsigned char	UINT8;
//gen typedef  unsigned short UINT16;


//gen typedef vector<string>	StrVector_t;

#define DFLT_LANG           "en"
#define USEDEFAULT          "USEDFLT"
#define MAX_LEGAL_STRING_LEN  1024  /*was 512 */  /* was: 256*/

#define DICTNAME              "deviceDict"  /* must match db in the dictDB.ddl file */ 

#ifdef READDICTIONARY
#include "dictTok.h"
#endif
//////////////////////////////////////////////////////////////////////////////
// the dictionary - using a database
//
class Cdict //: public CdbBase
{
protected:
	long     dictDB;
	string   defaultLang;// without the delimiters

public:
	Cdict(string dfltLang = "en");
	~Cdict();

	virtual RETURNCODE loadDictionaries(StrVector_t& vdictFiles);// load file(s)

	bool    setDefaultLang(string lang);
	string  getDefaultLang(void){ return defaultLang; };
	// fetch string using specified language <or default>
	virtual string &getDictString(int value, int dictType = 0, string lang = (USEDEFAULT));// fetch a string from combined section/offset
	virtual string &getDictString(int section, int offset, int dictType = 0, string lang = (USEDEFAULT));// fetch string using separate values

	virtual int dumpDict(ostream &outputstream, int dictType = 0);

private: // utility functions
#ifdef READDICTIONARY
	RETURNCODE readdict(ifstream& incomingFile, int dictType = 0);/* defaults to unknown */
#endif

};

#endif  // _DICT_H_