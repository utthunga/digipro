/*************************************************************************************************
 *
 * $Workfile: DDSelect.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "DDselect.h"
 */
#if !defined(AFX_DDSELECT_H__9DA87799_8648_4614_911C_D8F15164F7A7__INCLUDED_)
#define AFX_DDSELECT_H__9DA87799_8648_4614_911C_D8F15164F7A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDselectSplitter.h"
#include "DDSelectTree.h"
#include "ddbDevice.h"	/* stevev 10jul09 - make like xmtr */

typedef enum bitmapIndex_e
{
	BMI_UNSELECTEDFOLDER,
	BMI_SELECTEDFOLDER,
	BMI_UNSELECTEDDOCUMENT,
	BMI_SELECTEDDOCUMENT,
	BMI_UNSELECTED_SIX,
	BMI_SELECTED_SIX,
	BMI_UNSELECTED_EIGHT,
	BMI_SELECTED_EIGHT
} bitmapIndex_t;


/////////////////////////////////////////////////////////////////////////////
// CDDselect dialog

class CDDselect : public CDDselectSplitter
{
// Construction
public: //Vibhor 080404: Adding document pointer as a new parameter
	//CDDselect(CDDIdeDoc *pDoc,CWnd* pParent = NULL);   // standard constructor
	// stevev 10jul09 - removed it to match xmtr's implementation
	CDDselect(CWnd* pParent = NULL);   // standard constructor

	// void setResp(DWORD* returnedDDkey);
	// stevev 10jul09 added stdDevciece to be like xmtr
	void setResp(hCddbDevice *pStandardDev,DD_Key_t* returnedDDkey);
	
// Dialog Data
	//{{AFX_DATA(CDDselect)
	enum { IDD = IDD_DDSLCTDIALOG };
	CButton	m_ApplyButtonCtrl;
	CSelectTree	m_LeftTreeCtrl;
	CListCtrl	m_RgtListCtrl;
	CString	m_SelectedString;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDselect)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	// stevev 10jul09...blikeXmtr
	hCddbDevice *pStdDev; // We want to pass it down to the parser Infc


// Implementation
protected:
	CImageList m_imgList;
	/* stevev 10jul09...blikeXmtr
	DWORD*     pDDkey;
	CDDIdeDoc *m_pDoc; //Vibhor 080404
	/_/ stevev 10jul09...blikeXmtr */
	DD_Key_t*     pDDkey;

	// Generated message map functions
	//{{AFX_MSG(CDDselect)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkListItem(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDSELECT_H__9DA87799_8648_4614_911C_D8F15164F7A7__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: DDSelect.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
