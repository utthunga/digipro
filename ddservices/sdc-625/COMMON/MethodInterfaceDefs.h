/*************************************************************************************************
 *
 * $Workfile: MethodInterfacedefs.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This file gives the data structures used to pass information between device object 
 *		and methods user interface.
 *	
 */

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version


#ifndef _METHODINTERFACEDEFS_H
#define _METHODINTERFACEDEFS_H

#pragma warning (disable : 4786) 

#include "ddbGeneral.h" // includes vector
#include "Char.h"
#include <deque>      // in its not in the above
#include "varient.h"	//stevev 28may09

//#include <cstringt.h> // to get CString class from mfc  stevev 10aug10

//class CString;
class hCVar;

/*The types of data that will need to be shown by the UI*/
enum UI_DATA_TYPE
{
	UNKNOWN_UI_DATA_TYPE = 0
	, TEXT_MESSAGE
	, EDIT
	, COMBO
	, HARTDATE
	, VARIABLES_CHANGED_MSG
	, PLOT   //Vibhor 071204: Chart / Graph
	, MENU	 // stevev 09aug05 - displayMenu() - button Names in combo list
	, TIME   // TIME_VALUE could be int,float, or tod
	
};

/*<START>06/01/2004 Added by ANOOP for validating the list of ranges*/
typedef struct
{
	__int64 iMinval;
	__int64 iMaxval;
}IntMinMaxVal_t;

typedef struct
{
	double fMinval;
	double fMaxval;
}FloatMinMaxVal_t;


typedef union
{
	IntMinMaxVal_t IntMinMaxVal;
	FloatMinMaxVal_t FloatMinMaxVal;
}MinMaxVal;		


typedef vector <MinMaxVal> MinMaxVal_t;

/*<END>06/01/2004 Added by ANOOP for validating the list of ranges*/

/*This class  represents the string message to be displayed*/
class UI_DATA_TYPE_TEXT_MESSAGE
{
public:
	int		iTextMessageLength;
	wchar_t	*pchTextMessage;


	UI_DATA_TYPE_TEXT_MESSAGE(){  clear();  };
	~UI_DATA_TYPE_TEXT_MESSAGE(){};
	void clear(){iTextMessageLength=0;pchTextMessage=NULL;};
};

/*The edit box type .see class UI_DATA_EDIT_BOX*/
enum EDIT_BOX_TYPE
{
	UNKNOWN_EDIT_BOX_TYPE
	, EDIT_BOX_TYPE_INTEGER
	, EDIT_BOX_TYPE_FLOAT
	, EDIT_BOX_TYPE_STRING
	, EDIT_BOX_TYPE_PASSWORD
	, EDIT_BOX_TYPE_TIME
	, EDIT_BOX_TYPE_DATE
};

/*This class  represents the edit box type and the value to be displayed and other related info */
/* stevev 28may09 - converted to a) use the HARTvariant, b) use item services to maximum */
class UI_DATA_EDIT_BOX
{
public:
	EDIT_BOX_TYPE		editBoxType;

	CValueVarient       editBoxValue;
	//__int64		iValue;
	//float	fValue;
	unsigned int nSize;
/*<START>06/01/2004 ANOOP for validating the list of ranges*/
/*	int		iValue, iMinValue, iMaxValue;
	float	fValue, fMinValue, fMaxValue;	*/
	MinMaxVal_t MinMaxVal;
	wstring     strEdtFormat;
/*<END>06/01/2004 ANOOP for validating the list of ranges*/
	//int		iDefaultStringLength;
	//int     iMaxStringLength;
	//char	*pchDefaultValue;
	//wchar_t *pwcDefaultValue;

	UI_DATA_EDIT_BOX() {  clear();   };

	void clear()
	{	editBoxType = UNKNOWN_EDIT_BOX_TYPE;
		editBoxValue.clear();
		nSize=0;  
		MinMaxVal.clear();
		strEdtFormat.clear();
	};

	~UI_DATA_EDIT_BOX()	{};
};

/* WS:EPM 30apr07 enum added to support multiple selection of bit enums, see UI_COM SF:EPM*/
enum COMBO_BOX_TYPE
{
	UNKNOWN_COMBO_BOX_TYPE,
	COMBO_BOX_TYPE_SINGLE_SELECT,
	COMBO_BOX_TYPE_MULTI_SELECT
};

#define MAXIMUM_NUMBER_OF_BITS_IN_BITENUM 32
/*This class  represents the combo box type and the value to be displayed and other related info */
class UI_DATA_TYPE_COMBO
{
public:
	COMBO_BOX_TYPE comboBoxType;//WS:EPM 30apr07 
	int		       iNumberOfComboElements;
	wchar_t	       *pchComboElementText; //This will be a list of options separated by semi colon
	unsigned long  m_lBitValues[MAXIMUM_NUMBER_OF_BITS_IN_BITENUM];
	//each selection in a bit enum has a value and a string associated with it. 
	//These values are not necessarily in binary order and values may be skipped 
	//so for each string in pchComboElementText, we will keep a bit value in m_lBitValues
	// The list should be in the order the DD has them in - NOT reordered to be numerically 
	//	(on value) or aphabetically (on string) in order.

	unsigned long nCurrentIndex; //Added By Anil October 25 2005 for fixing the PAR 5436

	UI_DATA_TYPE_COMBO()  {  clear();  };

	void clear()
	{
		comboBoxType = UNKNOWN_COMBO_BOX_TYPE;//WS:EPM 30apr07 
		iNumberOfComboElements = 0;
		pchComboElementText = NULL;
		nCurrentIndex = 0;//Added By Anil October 25 2005 for fixing the PAR 5436
		memset(m_lBitValues,0,sizeof(unsigned long)*MAXIMUM_NUMBER_OF_BITS_IN_BITENUM);
	};

	~UI_DATA_TYPE_COMBO(){};
};

/*This class  represents the date time type and the value to be displayed and other related info 
class UI_DATA_TYPE_DATETIME
{
public:
	wchar_t	*pchHartDate;
	
	UI_DATA_TYPE_DATETIME():pchHartDate(NULL){};	
	~UI_DATA_TYPE_DATETIME(){};
};
it's an edit box for gods sake...*/

/*Vibhor 091204: Start of Code*/
//This class represents the scope control (graph/chart)

// no longer needed....class hCitemBase;

/*Vibhor 091204: End of Code*/

/*This is the structure used to send the display info and values from the builtin(device object)
 to the SDC methods UI.
*/
struct ACTION_UI_DATA
{
	UI_DATA_TYPE userInterfaceDataType;

	UI_DATA_TYPE_TEXT_MESSAGE	textMessage;// has its own ctor
	UI_DATA_TYPE_COMBO			ComboBox;   // has its own ctor
	UI_DATA_EDIT_BOX			EditBox;	
//	UI_DATA_TYPE_DATETIME		datetime;

	bool        bMethodAbortedSignalToUI;
	bool		bUserAcknowledge;
  	bool		bDisplayDynamic;	//Added by Prashant 20FEB2004

	// There are certain cases where we need just the abort button enabled
	bool		bEnableAbortOnly;	// Vibhor 030304: 
	unsigned	uDelayTime;			// in MilliSecs. Vibhor 040304:
	long int	DDitemId;			//Added By Anil September 26 2005 as suggested by Steve
	hCVar*      pVar4ItemID;		// just save the re-lookup

	/* stevev 26jan06 - constructor */
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	ACTION_UI_DATA() {clear();};
	void clear() {userInterfaceDataType = UNKNOWN_UI_DATA_TYPE; textMessage.clear();
	ComboBox.clear();EditBox.clear();bMethodAbortedSignalToUI=false;bUserAcknowledge=false;
	bDisplayDynamic=false;bEnableAbortOnly=false;uDelayTime=0xffffffff;DDitemId=0;
	pVar4ItemID=NULL;};
};																				


/*This is the structure used to send the user input from the SDC methods UI to the device object
(and then to the builtins)*/
struct ACTION_USER_INPUT_DATA
{
	UI_DATA_TYPE				userInterfaceDataType;
	UI_DATA_TYPE_COMBO			ComboBox;//WS:EPM 30apr07 
	UI_DATA_EDIT_BOX			EditBox;
	//UI_DATA_TYPE_DATETIME		datetime; 

	unsigned int				nComboSelection;

	ACTION_USER_INPUT_DATA():userInterfaceDataType(UNKNOWN_UI_DATA_TYPE),nComboSelection(0){};
};


#endif // _METHODINTERFACEDEFS_H
