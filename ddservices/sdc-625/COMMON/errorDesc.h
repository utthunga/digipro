/*************************************************************************************************
 *
 * $Workfile: errorDesc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * This file is the
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		       the description of errors listed in errors.h
 *
 * #include "errorDesc.h"
 *
 */


#ifndef _ERRORDESC_H
#define _ERRORDESC_H

#include "errors.h"

/*************************************************************************************************
 * NOTES:
 *		The descriptions and usage is included here for errors in errors.h
 *		Any changes here should reflect the content of errors.h.
 *
 * custom format strings:
 *	the first  '%s' (with no '%' characters before it)
 *											will be filled with the contents of errorList[x].name
 *  the second '%s' (with no intervening %'s)
 *									 will always be filled with the contents of errorList[x].desc
 * the rest of variable arguement list will be passed to a printf type routine
 *  ie format = "ERROR: %s problem %-6s with %s problems of %d type.\n", and
 *																name="BigError", desc="Description"
 *     calling:   logout(channel,thisError,"XXX","qwerty",99);
 * prints to channel:
 * ERROR: BigError problem XXX    with qwerty problems of 99 type.
 *
 * while the same call with format="ERROR: %s problem %s with %s problems of %s type.\n"
 * prints to channel:
 * ERROR: BigError problem Description with XXX problems of qwerty type.
 *
 */

//#define ELMAC(a,n,d) errorList[a].format="%s ERROR (%s)\n";errorList[a].name=n;errorList[a].desc=d;

	//ELMAC(LIB_NOMEMORY,		"Memory",		"Memory allocation failed."		)
	//ELMAC(LIB_PROGRAMMER,	"Programmer",	"Internal programming error"	)
//#define LIB_ERR_INIT    \
	//ELMAC(PARAMERR,			"Parameter",	"Bad parameter passed in"       )\
	//ELMAC(FILEOPENERROR,	"FileOpen",     "Error opening file"            )\
	//ELMAC(FILEREADERROR,  	"FileRead",     "Error reading file"         )

//    errorList[LIB_TEST].format="%s ERROR (%s) with %d\n";errorList[LIB_TEST].name="Testing";\
//											errorList[LIB_TEST].desc="Test sting description";

#endif // _ERRORDESC_H

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: errorDesc.h $
 *
 *************************************************************************************************
 */
