
/*
 *   sign.h
 *
 *  Copyright 2007 - HART Communication Foundation
 *  All rights reserved.
 */

#if !defined(_SIGN_INCLUDED)
#define _SIGN_INCLUDED

// hash n bytes of data using key, return 4 byte signature
bool signfile(char *path, int flg);
bool verifyfile(char *path, int flg);

#endif		// _SIGN_INCLUDED
