/**********************************************************************************************
 *
 * $Workfile: QA+Debug.h $
 * 05aug15 - timj
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2015 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		extract the common information used to suppress or change output for debugging or Q&A
 *      for comparing 8vsA dump output
 *
 * #include "QA+Debug.h"
 *
 */

#ifndef _Q_AND_A_PLUS_DEBUG_H
#define _Q_AND_A_PLUS_DEBUG_H

//#define _QANDA			// uncomment this line for Q and A

#ifdef INC_DEBUG
#ifndef TOKENIZER
#pragma message("    No Includes in QA+Debug.h") 
#endif
#endif
#ifdef TOKENIZER
#include "globals.h"
#endif

#ifdef INC_DEBUG
#pragma message("    Finished       QA+Debug.h") 
#endif

// 07apr16 - to have this work across threads, we need a different definition

// QandA contains options for V&V comparisons of dump files:
	// 0x00 - QandA mode switched off
	// 0x01 - 8v8 - old 8 parser vs new 8 parser
	// 0x02 - AvA - old A parser vs new A parser (not used currently)
	// 0x03 - 8vA - 8 parser vs A parser (which 8 parser determined by bit 3
	// 0x04 - bit set => use the old binary parser
#ifndef TOKENIZER
extern int QandA;

#define SET_QnA(q)          (QandA = q)

#define QANDA				((QandA & 0x0003) > 0)

#define QANDA_8v8			((QandA & 0x0001) == 1)	// old 8.1 parser vs new 8.3 parser
#define QANDA_AvA			((QandA & 0x0002) == 2) // unused
#define QANDA_8vA			((QandA & 0x0003) == 3)	// new 8.3 parser vs version A parser
#define QANDA_81PARSER		((QandA & 0x0004) > 0)	// use the old 8.1 parser to read an fm8 DD
#define QANDA_8Vold8		((QandA & 0x0008) == 8)	// added stuff to the 1 setting (use 9)

#else

#define SET_QnA(q)          (Q_AND_A = q)

#define QandA				bad-usage /* designed to trap direct usage*/

#define QANDA				((Q_AND_A & 0x0003) > 0)

#define QANDA_8v8			((Q_AND_A & 0x0001) == 1)	// old 8.1 parser vs new 8.3 parser
#define QANDA_AvA			((Q_AND_A & 0x0002) == 2) // unused
#define QANDA_8vA			((Q_AND_A & 0x0003) == 3)	// new 8.3 parser vs version A parser
#define QANDA_81PARSER		((Q_AND_A & 0x0004) > 0)	// use the old 8.1 parser to read an fm8 DD
#define QANDA_8Vold8		((Q_AND_A & 0x0008) == 8)	// added stuff to the 1 setting (use 9)
#endif


#ifdef _QANDA
extern unsigned qaAttrType;	// see FMx_Attribute:attrType
							// in DDLDEFS.H see _ITYPE defines and attr type defines
extern unsigned qaAttrMask;	// see aCattrBase::attr_mask
extern unsigned qaItemType;	// see aCitemBase::itemType
extern unsigned qaItemId;
#endif

#endif //_Q_AND_A_PLUS_DEBUG_H