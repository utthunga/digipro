/*************************************************************************************************
 *
 * SDC625  dd
 *
 * Description:  This holds the description of the host device: SDC625.
 *    The intial implementation is small but it will grow in time.
 *
 * by steve vreeland
 *
 *	created: 01may08
 *
 * Note: This dd was created as a way to leverage the multiple languages of the standard
 * dictionary into the sdc.  We will be working to get 100% of our strings into this DD.
 * Other items like menu structure and window structure (like the preferences window) will
 * be added as we can implement their realization in the sdc.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2008-2009 HART Communication Foundation, All Rights Reserved 
 ************************************************************************************************/



MANUFACTURER HCF, DEVICE_TYPE _SDC625, DEVICE_REVISION 1,
DD_REVISION 1

/* this variable exists merely to force the load of these dictionary
   strings into the dictionary.  They are used by their dictionary number
   and not via an enum-string lookup
 */
VARIABLE default_dict_entries
{
   TYPE ENUMERATED
   {
      { 0,  [dict_str_not_found] },
      { 1,  [lit_str_not_found] },
      { 2,  [default_help] },
      { 3,  [default_label] },
      { 4,  [default_desc] },
      { 5,  [default_display_format_int] },
      { 6,  [default_display_format_uint] },
      { 7,  [default_display_format_float] },
      { 8,  [default_display_format_double] },
      { 9,  [default_edit_format_int] },
      {10,  [default_edit_format_uint] },
      {11,  [default_edit_format_float] },
      {12,  [default_edit_format_double] }
   }
}

/* used as default_string_entries(1) in a string reference
 */
VARIABLE default_string_entries
{
   TYPE ENUMERATED
   {
      { 0,  "Missing_String" },
      { 1,  "++EnumString++"},
      { 2,  "++Key-SymID lookup failed++" },
      { 3,  "++Variable String++"},
      { 4,  "++Variable Reference String++"},
      { 5,  "++Enum Reference String++" },
      { 6,  "Dictionary string could not be found." }
   }
}
