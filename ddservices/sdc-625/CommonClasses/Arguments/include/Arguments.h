/*************************************************************************************************
 *
 * $Workfile: Arguments.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface for the Arguments class.
 *		
 *
 * #include "Arguments.h"
 */

//
// (C) 2001 NOVACOM GmbH, Berlin, www.novacom.net
// Author: Patrick Hoffmann, 03/21/2001
//
/*  ******** 5-6-4 stevev *********************
  Yet another -   totally different argument call interface

  Option.AddArgument() is gone and replaced by:

  Option.AddRequiredArgument(stringName, stringDescription, stringDefault, 
				envLocationString,iniLocationString)
  Option.AddOptionalArgument(stringName, stringDescription, stringDefault, 
				envLocationString,iniLocationString)
	and
  Arguments.AddArgument() is gone and replaced by
  Arguments.AddRequiredArgument(stringName, stringDescription, stringDefault, 
				envLocationString,iniLocationString)
  Arguments.AddOptionalArgument(stringName, stringDescription, stringDefault, 
				envLocationString,iniLocationString)

SEE NOTES AT THE END OF THE FILE
*/
//////////////////////////////////////////////////////////////////////

#pragma warning (disable : 4786) 

#if !defined(AFX_ARGUMENTS_H__1F0E328D_C72D_4709_8A74_7654888217A6__INCLUDED_)
#define AFX_ARGUMENTS_H__1F0E328D_C72D_4709_8A74_7654888217A6__INCLUDED_

#ifndef TOK

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In Arguments.h") 
#endif

#define ARGUMENTS_H
#pragma warning (disable : 4786) 
#ifdef _CONSOLE
#define INI_FILE_SECTION _T( "Settings"	)/* location in app.ini file ie: [DEFAULT_VALUES] */
#else
#include "args.h"		/* only for the definition of INI_FILE_SECTION */
#endif
#include "messageUI.h"

#ifndef INI_FILE_SECTION
#define INI_FILE_SECTION  "ARGS"
#endif

#pragma warning (disable : 4786) 

#include <string>
#include <vector>
#include <map>

using namespace std;

#include "ddbDeviceMgr.h"	// to get max version

#ifndef TCHAR
#include <tchar.h>
// typedef char TCHAR;
// #define _T(a) a
#endif
#ifndef _UNICODE
#ifndef tstring
 typedef string tstring;
#endif
#define Outstream ostream
#else
 //typedef basic_string<TCHAR> tstring;
#ifndef tstring
 typedef wstring tstring;
#endif
#define Outstream wostream
#endif

#ifdef INC_DEBUG
#pragma message("    Finished Includes::Arguments.h") 
#endif

// RETURN CODES
 // note that zero (false) is success

#define PARSE_OK			0
#define UNKNOWN_OPTION		1
#define TOO_FEW_OPTION_ARGS 2
#define TOO_MANY_ARGS		3
#define TOO_FEW_ARGS		4
#define DEFAULT_FAILED		5	/* failure in the default settings */

// MACRO
#define STR(a) tstring a=_T("")

inline Outstream &operator<<(Outstream& ostr, tstring& tstr )
{
	ostr << tstr.c_str();
	return ostr;
};

// See notes at the end of this file

class Arguments  
{
	tstring	m_strOptionmarkers; // what makes an Option (ie '-|\'}
	tstring	m_strDescription;	// command help
	tstring	m_strCommandName;	// parsed .exe name from cmd line
	tstring m_strAppDir;		// parsed .exe path from cmd line

public:
	static tstring	UnknownArgument; 
	static tstring	IniFile_Section; 
	tstring			m_errString;		// where we store the error string before leaving

	class Option;
	class Argument;
	typedef vector<Argument>	ArgVector;

	typedef enum 
	{
		src_initVal,
		src_CmdLine,	/* inserted by user - priority 1 */
		src_IniFile,	/* or registry      - priority 2 */
		src_EnvVar,		/* stored for reuse - priority 3 */
		src_Default,	/* user unspecified - priority 4 */
		src_External    /* value changed programatically */
	} optionSOurce_t;

	class Argument
	{
		friend class Arguments;
		friend class Option;

		tstring	m_strName;
		tstring	m_strDescription;
		tstring	m_envLocString;
		tstring m_iniLocString;
		tstring m_strDefault;
		tstring	m_strValue;	
		optionSOurce_t m_argSrc;
		bool	m_bSet;				// set when this option encountered in command line		
		bool	m_bOptional;

	public:
		Argument( tstring stringName, STR(stringDescription),STR(iniLocationString), 
									  STR(envLocationString),STR(stringDefault),
									  bool isOptional = false);
		bool FillEmpty(void);	// true on success
		tstring getIniKey(void) { return (m_iniLocString);};
		tstring getStrValue(void) { return (m_strValue);};
	};

	class Option
	{
		friend class Arguments;
		static Option	Empty;
	
		tstring			m_chName;
		ArgVector		m_vOpArguments;
		tstring			m_strDescription;
		optionSOurce_t  m_optSrc;
		bool			m_bSet;				// set when this option encountered in command line
		bool            m_isUsage;
		
	public:
		Option( tstring chName, tstring strDescription=_T(""), bool thisIsUsage=false );
		//bool AddArgument(tstring strName,tstring strDescription=_T(""), 
		//								  tstring strDefault = _T(""), tstring strDefVal=_T(""));
		bool AddRequiredArgument(tstring stringName,STR(stringDescription), 
							 STR(iniLocationString),STR(envLocationString),STR(stringDefault));
		bool AddOptionalArgument(tstring stringName,STR(stringDescription), 
							 STR(iniLocationString),STR(envLocationString),STR(stringDefault));
		bool FillEmptyArgs(void);	// true on success

		tstring &operator[]( int n );
		tstring &operator[]( tstring strArgumentName );
		tstring &operator[]( const TCHAR *pszArgumentName );
		operator bool();
		optionSOurce_t getSrc(tstring strArgumentName);
		void Set( bool bSet = true );
		tstring GetName();

		int getArgCnt(void) { return m_vOpArguments.size(); };
		Argument* getFirstArg(void) { if (getArgCnt()) return &(m_vOpArguments[0]); else return NULL;};
	};

protected:
	typedef map<tstring,Option,less<tstring>,allocator<Option> > OptionMap;
	

	OptionMap::iterator findOption(TCHAR chOptionName);

	OptionMap			m_mOptions;
	ArgVector			m_vArguments;
	tstring     getEnvironment(tstring envLocString);
	tstring     getIniFile    (tstring iniLocLocation,   //file:   Location=value,  [Key]
							   tstring iniLocKey =  INI_FILE_SECTION );

public:
	Arguments( tstring strCommandName, STR(strDescription), tstring strOptionmarkers=_T("-/") );

	bool IsOption( TCHAR chOptionName );
	bool Usage();

	bool AddOption( tstring chOptionName, STR(strDescription),bool thisIsUsage=false );
	bool AddOption( Option &option );
	//bool AddArgument( tstring strName,tstring strDescription=_T(""),tstring strDefault = _T(""), 
	//																  tstring strDefVal=_T("") );																  tstring strDefVal=_T("") );
	bool AddRequiredArgument( Arguments::Argument Argue );
	bool AddRequiredArgument( tstring stringName,    STR(stringDescription), 
							 STR(iniLocationString),STR(envLocationString),STR(stringDefault) );
	bool AddOptionalArgument( Arguments::Argument Argue  );
	bool AddOptionalArgument( tstring stringName,    STR(stringDescription), 
							 STR(iniLocationString),STR(envLocationString),STR(stringDefault) );

	int  Parse(int argc, TCHAR* argv[]);
#ifdef WIN32
	int  Parse(TCHAR *pszCommandLine);
	int  Parse();
#endif	

	tstring &getAppDir(void)  { return m_strAppDir; };
	tstring &getAppName(void) { return m_strCommandName; };
	tstring &operator[]( int n );
	tstring &operator[]( tstring strArgumentName );
	Option  &operator[]( TCHAR chOptionName );

	virtual
		void CopyFromRegistry(void) { return; };/* over-ride to do this */
	
	virtual ~Arguments();
};

#pragma warning (disable : 4786) 

#undef STR	/* make sure this is a local macro */
#endif /* TOK */
#endif // !defined(AFX_ARGUMENTS_H__1F0E328D_C72D_4709_8A74_7654888217A6__INCLUDED_)

/****NOTES****************************************************************************************
the args.h file generates the arguments class and inserts the options

  optionMarker CommandName 
  - command line
  - ini file value
  - environment variable
  - default value

    add a -D (or -d) option for loading a dictionary
	following the -D there is one required parameter and four optional ones
	  the optionals end at end-of-command-line or another -|\ option

  // generate one option
Arguments::Option	cOpt( "D|d", "Load Standard Dictionary" );

  // add a non-optional option argument 
				// Non optional argument - You must have at least 1 if -D supplied
			        cOpt.AddArgument( "standardDictName", 
						"file name with optional path\n",
						_T(""), _T("value_When-D_isn't_input"));
						NON-Optional due to empty Default Value
  // add an optional option argument 
				// plus 1 optional argument
			        cOpt.AddArgument( "ExtStandardDictNameOne", 
						"Extended Standard dictionary name\n",
						_T("notSupplied"),_T("notUsedWhenOptional"));
						******** optional when the Default value is NOT empty
  // insert the new option into the Arguments' Option list
AddOption( cOpt );

  this gives
  [-(D|d) requiredArg [optionalArg]]
  --- note that programmatically you must access the option by the first char in the optionName.
		( ie ['D']["standardDictName"] works, ['d']["standardDictName"] does NOT)
examples:
1) commandLine>application.exe -D valueOne valueTwo
gives:
  Args['D'] is true
  Args['D']["standardDictName"] is 'valueOne'
  Args['D']["ExtStandardDictNameOne"] is 'valueTwo'


2) commandLine>application.exe -D valueOne
gives:
  Args['D'] is true
  Args['D']["standardDictName"] is 'valueOne'
  Args['D']["ExtStandardDictNameOne"] is 'notSupplied'

3) commandLine>application.exe -D 
gives:
application.exe error: Too few arguments for option D.
and aborts

4) commandLine>application.exe 
gives:
  Args['D'] is false
  Args['D']["standardDictName"] is 'value_When-D_isn't_input'
  Args['D']["ExtStandardDictNameOne"] is 'notSupplied'

 **** end notes **********************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: Arguments.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Common Classes/Arguments/include
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
