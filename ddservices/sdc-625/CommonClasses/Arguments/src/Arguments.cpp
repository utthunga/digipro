/*************************************************************************************************
 *
 * $Workfile: Arguments.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */

// Arguments.cpp: implementation of the Arguments class.
//
// (C) 2001 NOVACOM GmbH, Berlin, www.novacom.net
// Author: Patrick Hoffmann, 03/21/2001
// http://www.codeproject.com/cpp/arguments.asp
//
// "Just use it, feel fine and thank me "  from a chat room response 	dated 12:45 29 Jan. '02
//				 Response to to the license clarification request.
//	'You have retained the copyright to the class but there is no license.  
//	 Under what conditions is this class permitted to be reused?'
// 
// sjv modified 2/6/02 to handle multiple names for a single option
//     -NOTE: '|' is now an illegal option (ie -| will never be found)
// sjv modified 2/7/02 to handle a default value for optional option args
//
//////////////////////////////////////////////////////////////////////

#pragma warning (disable : 4786) 

#include "stdafx.h"
#include "char.h"
#include "Arguments.h"
#include <iostream>

#ifdef WIN32
int __cdecl _ConvertCommandLineToArgcArgv( LPTSTR pszSysCmdLine );
extern LPTSTR _ppszArgv[];
#endif

#define SCRATCH_STR_LEN	512

#ifdef TOK
struct TOKtmp 
{
	char m_pszProfileName[5] ;
};
TOKtmp locTokTmp = { "DDT" };
#endif

#include "QA+Debug.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

using namespace std;
#pragma warning (disable : 4786) 


int QandA;  //moved here to be consistent over sdc & xmtr-dd


Arguments::Option	Arguments::Option::Empty(_T(""));
tstring				Arguments::UnknownArgument(_T("<UnKnOwN>"));
tstring				Arguments::IniFile_Section(INI_FILE_SECTION);


/*================================================================================================
 *  The  Arguments' Argument class
 *===============================================================================================*/
/*************************************************************************************************
 Argument:construct a single argument 
 *************************************************************************************************/
Arguments::Argument::Argument( tstring stringName,       tstring stringDescription,
							   tstring iniLocationString,tstring envLocationString,
							   tstring stringDefault,    bool    isOptional        )
: m_strName( stringName )
, m_strDescription( stringDescription )
, m_iniLocString(iniLocationString)
, m_envLocString(envLocationString)
, m_strDefault( stringDefault)
, m_strValue                      ( UnknownArgument )
, m_bSet(false)
, m_bOptional(isOptional)
, m_argSrc(src_initVal)
{		
	// this is used in the beginning instantiation - no lookups here
}

/*************************************************************************************************
 Argument:fill self from ini/registry or environment or default  returns true on error
 *************************************************************************************************/
bool Arguments::Argument::FillEmpty(void)
{
	bool r = false;// no error
//	CString localStr;
	tstring  locStr;
	TCHAR*   pEnvStr = NULL;
	TCHAR tmp[SCRATCH_STR_LEN];

	if ( m_strValue != UnknownArgument )
	{
//		TRACE(" Cmd");
		r = false;//filled - no error
		m_argSrc = src_CmdLine;
	}
	else
	{// fill it, a) ini file, b) environment, c) default value
#ifdef TOK
		TOKtmp* pApp = &locTokTmp;
#else
		CWinApp *pApp = AfxGetApp();
#endif
		if (pApp == NULL)
			r = true;// error
		else
		{
//			localStr = pApp->GetProfileString(Arguments::IniFile_Section.c_str(), 
//																	m_iniLocString.c_str(),"");
#ifndef _WIN32_WCE	// PAW function missing 28/04/09
			DWORD rv = GetPrivateProfileString(Arguments::IniFile_Section.c_str(),
				  m_iniLocString.c_str(),   // key
				  _T(""),					// default string
				  tmp,						// destination buffer
				  SCRATCH_STR_LEN,              // size of destination buffer
				  pApp->m_pszProfileName        // initialization file name
				);

	//		localStr = tmp;
			locStr   = tmp;
	//		if ( localStr.IsEmpty() )
			if ( locStr.empty() )
			{// try for the environment
				TCHAR tmpEnv[SCRATCH_STR_LEN];
				DWORD tmpCnt = 0;
				tmpCnt = GetEnvironmentVariable(m_envLocString.c_str(), tmpEnv, SCRATCH_STR_LEN);

				if ( tmpCnt != 0 )
				{// set to environment
	//				TRACE(" Env");
	//				localStr = tmpEnv;
					locStr = tmpEnv;
					m_argSrc = src_EnvVar;
				}
				else
				{// set to default
	//				TRACE(_T(" Dfl"));
					m_argSrc = src_Default;
	//				localStr = m_strDefault.c_str();
					locStr = m_strDefault.c_str();// the '.c_str()' should force a copy
				}
			}// else we got the ini file version
			else
			{
	//			TRACE(_T(" Ini"));
				m_argSrc = src_IniFile;
			}

	//		if ( ! localStr.IsEmpty() )// if filled from anywhere...
			if ( ! locStr.empty() )
			{
	//			m_strValue = localStr.GetBuffer(SCRATCH_STR_LEN);
	//			localStr.ReleaseBuffer();
				m_strValue = locStr.c_str();
			}
			// else - just leave it Unknown
#else
// set to default
				m_argSrc = src_Default;

				locStr = m_strDefault.c_str();// the '.c_str()' should force a copy
#endif
		}
	}// endelse - already filled
	//TRACE(_T("    Arg:%s %c%s%c holds:|%s| from %s\n"),m_strName.c_str(),
	//	((m_bOptional)?_T('['):_T('(')),m_strDescription.c_str(),
	//	((m_bOptional)?_T(']'):_T(')')),m_strValue.c_str(),
	//	((m_argSrc==src_CmdLine)?_T("Command Line"):
	//                          (m_argSrc==src_IniFile)?_T("Ini File"):
	//						                        (m_argSrc==src_EnvVar)?_T("Environment"):
	//						                                              (m_argSrc==src_Default)?_T("Default Value"):
	//																	                            _T("NOT FILLED"))  );
	return r;
}



/*================================================================================================
 *  The  Arguments' Option class
 *===============================================================================================*/
/*************************************************************************************************
 Option:constructor
 *************************************************************************************************/
Arguments::Option::Option( tstring chName, tstring strDescription, bool thisIsUsage )
: m_chName( chName )
, m_strDescription( strDescription )
, m_bSet( false )
, m_isUsage( thisIsUsage )
, m_optSrc( src_initVal )
{
}
/*************************************************************************************************
 Option:Add an argument to  the option
 *************************************************************************************************/
bool  Arguments::Option::AddRequiredArgument(tstring stringName,        tstring stringDescription, 
				  tstring iniLocationString, tstring envLocationString, tstring stringDefault )
{
	m_vOpArguments.push_back( 
	 Argument(stringName,stringDescription,iniLocationString,envLocationString,stringDefault,false) 
	);
	return true;
}

bool  Arguments::Option::AddOptionalArgument(tstring stringName,        tstring stringDescription, 
				  tstring iniLocationString, tstring envLocationString, tstring stringDefault )
{
	m_vOpArguments.push_back( 
	 Argument(stringName,stringDescription,iniLocationString,envLocationString,stringDefault,true) 
	);
	return true;
}

Arguments::Option::operator bool()
{
	return m_bSet;
}

void Arguments::Option::Set( bool bSet )
{
	m_bSet = bSet;
}

bool Arguments::Option::FillEmptyArgs(void)// true on failure
{
	bool r = false;

//	TRACE(_T("Option %s (%s) %s %s\n"),                  m_chName.c_str(),m_strDescription.c_str(),
//		(m_bSet)?_T("WasSet"):_T("NotSet"),(m_isUsage)?_T("IsUsage"):_T(""));
	for ( ArgVector::iterator it = m_vOpArguments.begin(); it<m_vOpArguments.end() && !r; it++)
	{// it pts to an Argument
		r = it->FillEmpty();
	}
	return r;
}

/*************************************************************************************************
 Option:dereference - get value of Option's argument by number
 *************************************************************************************************/
tstring &Arguments::Option::operator[]( int n )
{
	return m_vOpArguments[n].m_strValue;
}

/*************************************************************************************************
 Option:dereference - get value of Option's argument by char string
 *************************************************************************************************/
tstring &Arguments::Option::operator[]( const TCHAR *pszArgumentName )
{
	return operator[]( (tstring)pszArgumentName );
}

/*************************************************************************************************
 Option:dereference - get value of Option's argument by std string
 *************************************************************************************************/
tstring &Arguments::Option::operator[]( tstring strArgumentName )
{
	for( ArgVector::iterator it = m_vOpArguments.begin(); it != m_vOpArguments.end(); it++ ) 
	{
		if( it->m_strName == strArgumentName )
			return it->m_strValue;
	}

	return UnknownArgument;
}

tstring Arguments::Option::GetName()
{
	return m_chName;//str;
}

Arguments::optionSOurce_t 
Arguments::Option::getSrc(tstring strArgumentName)
{
	for( ArgVector::iterator it = m_vOpArguments.begin(); it != m_vOpArguments.end(); it++ ) 
	{
		if( it->m_strName == strArgumentName )
			return it->m_argSrc;
	}

	return src_initVal;
};

/*================================================================================================
 *  The  Arguments class
 *===============================================================================================*/

/*************************************************************************************************
 Arguments:constructor
 *************************************************************************************************/
Arguments::Arguments(tstring strCommandName, tstring strDescription, tstring strOptionmarkers)
: m_strCommandName( strCommandName ) 
, m_strDescription( strDescription )
, m_strOptionmarkers( strOptionmarkers )
, m_strAppDir(_T("./"))
, m_errString(_T(""))
{
}
/*************************************************************************************************
 Arguments:destructor
 *************************************************************************************************/
Arguments::~Arguments()
{
}

#ifdef WIN32
/*************************************************************************************************
 Arguments:Windows command line parsers
 *************************************************************************************************/
int Arguments::Parse(LPTSTR pszCommandLine)
{
	int argc = _ConvertCommandLineToArgcArgv(pszCommandLine);
	
	return Parse( argc, _ppszArgv );
}

int Arguments::Parse()
{
/*<START>Added by ANOOP 09APR2004 to fix the memory leaks	*/
	

	LPTSTR	lpszCmdLine=GetCommandLine();
	TCHAR	*ptrCmdLine=NULL;

	int nCmdLine = lstrlen( lpszCmdLine );
	ptrCmdLine=new TCHAR[nCmdLine + 1];
	_tcscpy(ptrCmdLine,lpszCmdLine);
	int argc = _ConvertCommandLineToArgcArgv(ptrCmdLine);
	
	int nTmp = Parse( argc, _ppszArgv );

	if(NULL != ptrCmdLine)
	{
		delete[] ptrCmdLine;
		ptrCmdLine=NULL;
	}

	return nTmp; 
/*<END>Added by ANOOP 09APR2004 to fix the memory leaks	*/	
}
#endif
/*************************************************************************************************
 Arguments:findOption - find option char in list of options
 *************************************************************************************************/
Arguments::OptionMap::iterator Arguments::findOption(TCHAR chOptionName)
{
	tstring mapStr;

	if (chOptionName == '|')// an illegal option
	{
		return m_mOptions.end();
	}
	OptionMap::iterator it;	// PAW 03/03/09 see below
	for( /*OptionMap::iterator PAW 03/03/09 see above */it = m_mOptions.begin(); it != m_mOptions.end(); it++ )
	{
		mapStr = it->second.GetName() ;
		if (mapStr.find(chOptionName) != tstring::npos )
		{
			return it;
		}
	}		
		
	return it; 
}

/*************************************************************************************************
 Arguments:parse the standard command line
 *************************************************************************************************/
int Arguments::Parse(int argc, TCHAR *argv[])
{
	// get the command name (.exe file name) and its path
	if( m_strCommandName.empty() )
	{
		TCHAR tstr[_MAX_PATH];
#ifndef _WIN32_WCE		// PAW function missing 28/04/09
		if( _tfullpath( tstr, argv[0], _MAX_PATH ) == NULL )
			_tcscpy(tstr,argv[0]);

		tstring locStr(tstr);
#else
		tstring locStr(L"d:\PAW\AMC\HART\sandbox\PCVS05HCF6\SDC\APPS\SDC625\Debug\SDC625.exe");
#endif


		int iLoc = locStr.rfind(_T("/"));
		if ( iLoc == tstring::npos )
		{// not found
			iLoc = locStr.rfind(_T("\\"));
		}
		if ( iLoc != tstring::npos )
		{
			m_strCommandName = locStr.substr(++iLoc);
			m_strAppDir      = locStr.substr(0,iLoc);
//			TRACE(_T("Dir: %s\n"),m_strAppDir.c_str());
//			TRACE(_T("Cmd: %s\n"),m_strCommandName.c_str());
		}
		else
		{
			m_strCommandName = locStr;
		}
	}

	//17mar11...i think we're beyond this now...
	//CopyFromRegistry();/* normally empty... moves registry values to ini file */

	int nArg = 0;
	bool hasHelp = false;
	TCHAR scrStr[SCRATCH_STR_LEN];	
	int nNonOptionalArgs = 0, nOptionalArgs = 0;
	ArgVector::iterator it;	// PAW 03/03/09 see below
	// count the number of non-optional, main (not -x option) arguments
	for( /*ArgVector::iterator PAW see above */ it = m_vArguments.begin(); it != m_vArguments.end(); it++ ) 
	{
		if( !it->m_bOptional )
			nNonOptionalArgs++;
		else
			nOptionalArgs++;
	}


	for( int i=1; i<argc; i++ )
	{
		tstring strArgument = argv[i];
//		TRACE(_T("\nParsing Arg:%s| "),strArgument.c_str());
		// Option...?
		if( m_strOptionmarkers.find(strArgument.substr(0,1)) != tstring::npos )
		{// a option delimiter ( ie '-|\')
			TCHAR chOptionName = strArgument[1]; // the char following the delimiter

			OptionMap::iterator it = findOption(chOptionName);// see if it's a valid option char

			if( it == m_mOptions.end() )
			{// not a valid input ... (like -Z unk)
				_stprintf(scrStr,_T("%s  error: Unknown option %s.\n"),
													m_strCommandName.c_str(),strArgument.c_str());
				m_errString = scrStr;
//				TRACE(_T("ERROR:%s \n"),scrStr);
				return UNKNOWN_OPTION;
			}
			else
			{// valid option, we have a pointer to it in it
				it->second.m_bSet = true;			// encountered
				if (it->second.m_isUsage == true)
				{
					hasHelp = true;
				}

				i++; // point to the argument after the delimChar
				{ 
					int nNonOptionalArgs = 0;
					
					{
						for( ArgVector::iterator itOptArg = it->second.m_vOpArguments.begin(); 
						      itOptArg != it->second.m_vOpArguments.end();         itOptArg++ ) 
						{
							if( !itOptArg->m_bOptional )
								nNonOptionalArgs++;
						}
					}// end of program block - itOptArg out of scope 
					
					for(int nOptArg=0; nOptArg < (int)it->second.m_vOpArguments.size(); i++, nOptArg++ ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
					{// for each argument of the Option's argument list
						if( i >= argc || 
							m_strOptionmarkers.find(tstring(argv[i]).substr(0,1))!=tstring::npos)
						{// end of Option's arguments on cmd line 
						 //                             (end of line or a delimiter ('-|\')
							if( nOptArg < nNonOptionalArgs )
							{// the number we found was smaller than we needed
								_stprintf(scrStr,_T("%s  error: Too few arguments for option %s.\n"),
													m_strCommandName.c_str(),strArgument.c_str());
								m_errString = scrStr;
//								TRACE(_T("ERROR:%s \n"),scrStr);
								return TOO_FEW_OPTION_ARGS;
							}
							else
							{
								break;// all is well
							}
						}
						// else - set the Option's Arguemnt value to cmd line value						
						it->second.m_vOpArguments[nOptArg].m_strValue = argv[i];					
//						TRACE(_T(" Value set 2 :%s| "), argv[i]);
					}
				}// end of program block
				i--;
			}// endelse isa valid option character
		}
		else	// ...standard Argument - no delimiter (no '-|\')
		{
			if( nArg >= nNonOptionalArgs)
			{// check the Optional arguments
				// if any Optionals
				if ( (nArg - nNonOptionalArgs) <= nOptionalArgs )
				{	// fill the next open optional
					for( it = m_vArguments.begin(); it != m_vArguments.end(); it++ ) 
					{
						if( it->m_bOptional && ( ! it->m_bSet) )
						{// fill it
							it->m_strValue = strArgument;
							it->m_bSet     = true;
							nArg++;
//							TRACE(_T(" Optional Value set to:%s|\n"),strArgument.c_str());
							break;// out of for loop
						}// else - unfillable, keep looking
					}// next cmd line arg
				}
				else //  too many arguments
				{
					_stprintf(scrStr,_T("%s  error: Too many arguments.\n"),m_strCommandName.c_str());
					m_errString = scrStr;
//					TRACE(_T("ERROR:%s \n"),scrStr);
					return TOO_MANY_ARGS;
				}				
			}
			else // we haven't done the required yet
			{// fill the next open required
				// else set the non-option argument list
				for (ArgVector::iterator i=m_vArguments.begin();i<m_vArguments.end();i++)
				{
					if ( !(i->m_bOptional) && !(i->m_bSet) )
					{// non-optional and hasn't been set
						i->m_strValue = strArgument;
						i->m_bSet     = true;
						nArg++;
//						TRACE(_T(" Value set to:%s|\n"),strArgument.c_str());
					}
				}
			}
		}// endelse argument type
	}// next command line argument
//	TRACE(_T("\n Parsing complete\n"));
	{// all are filled from command line
		
		if( nArg < nNonOptionalArgs  && !hasHelp )
		{// the read-in arguments are less than the needed
			_stprintf(scrStr,_T("%s  error: Too few arguments.\n"),m_strCommandName.c_str());
			m_errString = scrStr;
			return TOO_FEW_ARGS;
		}
		// not an error return...  fill all non-included values with defaults
		// for each option
		for( OptionMap::iterator it = m_mOptions.begin(); it != m_mOptions.end(); it++ )
		{// it points to a pair<string, Option>
			if ( it->second.FillEmptyArgs() )
			{// the fill had an error
				_stprintf(scrStr,_T("%s  error: Default fill failed\n"),it->second.GetName().c_str());
				m_errString = scrStr;
//				TRACE(_T("ERROR:%s \n"),scrStr);
				return DEFAULT_FAILED;
			}
		}
		for( ArgVector::iterator iT = m_vArguments.begin(); iT != m_vArguments.end(); iT++ ) 
		{
			if ( iT->FillEmpty() )
			{// the fill had an error
//				TRACE(_T("ERROR:Argument fill failed \n"));
				return DEFAULT_FAILED;
			}
		}
	}// end of program block

	return PARSE_OK;
}

#pragma warning (disable : 4786) 

/*************************************************************************************************
 Arguments:AddOption
 *************************************************************************************************/
bool Arguments::AddOption(tstring chOption, tstring strDescription,bool thisIsUsage)
{
	m_mOptions.insert( pair<tstring,Option>(chOption,Option(chOption,strDescription,thisIsUsage)) );

	return true;
}

bool Arguments::AddOption( Option &option )
{
	m_mOptions.insert( pair<tstring,Option>(option.m_chName,option) );
	
	return true;
}

/*************************************************************************************************
 Arguments:output the usage screen
 *************************************************************************************************/
bool Arguments::Usage()
{
	tcerr << M_UI_ARG_USAGE01 << m_strCommandName;
	// cout << "Usage information sent to cerr"<<endl;
	OptionMap::iterator it; // PAW 03/03/09 see below
	for( /*OptionMap::iterator PAW see above */it = m_mOptions.begin(); it != m_mOptions.end(); it++ )
	{
		tcerr << _T(" [") << m_strOptionmarkers[0] << it->second.GetName();
		
		for( ArgVector::iterator itArg = it->second.m_vOpArguments.begin(); 
			 itArg != it->second.m_vOpArguments.end();                itArg++ )
		{
			if( itArg->m_bOptional )
				tcerr << _T(" [") << itArg->m_strName << _T("]");
			else
				tcerr << _T(" ") << itArg->m_strName;
		}
		tcerr << "]";
	}

	ArgVector::iterator itArg;	// PAW 03/03/09
	for( itArg = m_vArguments.begin(); itArg != m_vArguments.end(); itArg++ )
	{
		if( itArg->m_bOptional )
			tcerr << _T(" [") << itArg->m_strName << _T("]");
		else
			tcerr << _T(" ") << itArg->m_strName;
	}
	
	cerr << endl;

	if( !m_mOptions.empty() )
		cerr << endl << "Options:" << endl;
		
	for( it = m_mOptions.begin(); it != m_mOptions.end(); it++ )
	{
		tcerr << _T("\t-") << it->second.GetName() << _T("\t  ") << it->second.m_strDescription << endl;
		
		for( itArg = it->second.m_vOpArguments.begin(); 
		     itArg != it->second.m_vOpArguments.end(); itArg++ )
		{
			tcerr << _T("\t ") << itArg->m_strName << _T("\t= ") << itArg->m_strDescription << endl;

			if( itArg->m_bOptional )
				tcerr << "\t\t  optional argument (default='" << itArg->m_strDefault << "')" << endl;
		}
	}
	
	if( !m_vArguments.empty() )
		tcerr << endl << "Arguments:" << endl;

	for( itArg = m_vArguments.begin(); itArg != m_vArguments.end(); itArg++ )
	{
		tcerr << "\t" << itArg->m_strName << _T("\t= ") << itArg->m_strDescription << endl;

		if( itArg->m_bOptional )
			tcerr << "\t\t  optional argument (default='" << itArg->m_strDefault << "')" << endl;
	}
	
	tcerr << endl;
	
	tcerr << m_strDescription << endl;

	return true;
}

/*************************************************************************************************
 Arguments:Add an argument to the main list
 *************************************************************************************************/
bool Arguments::AddRequiredArgument(Arguments::Argument Argue)
{
	Argue.m_bOptional = false;
	m_vArguments.push_back( Argue );
	return true;
}
bool Arguments::AddRequiredArgument(tstring stringName,       tstring stringDescription,
		tstring iniLocationString,  tstring envLocationString,tstring stringDefault    )
{
	m_vArguments.push_back( 
	 Argument(stringName,stringDescription,iniLocationString,envLocationString,stringDefault,false)
    );
	return true;
}
bool Arguments::AddOptionalArgument(Arguments::Argument Argue)
{
	Argue.m_bOptional = true;
	m_vArguments.push_back( Argue );
	return true;
}
bool Arguments::AddOptionalArgument(tstring stringName,       tstring stringDescription,
		tstring iniLocationString,  tstring envLocationString,tstring stringDefault    )
{
	m_vArguments.push_back( 
	 Argument(stringName,stringDescription,iniLocationString,envLocationString,stringDefault,true)
    );
	return true;
}


/*************************************************************************************************
 Arguments:see if character is an option character
 *************************************************************************************************/
bool Arguments::IsOption(TCHAR chOptionName)
{
	OptionMap::iterator it = findOption(chOptionName);//m_mOptions.find(chOptionName);
	
	if( it == m_mOptions.end() )
		return false;
	else 
		return it->second.m_bSet;
}

tstring &Arguments::operator[]( int n )
{
	return m_vArguments[n].m_strValue;
}

tstring &Arguments::operator[]( tstring strArgumentName )
{
	for( ArgVector::iterator it = m_vArguments.begin(); it != m_vArguments.end(); it++ ) 
	{
		if( it->m_strName == strArgumentName )
			return it->m_strValue;
	}

	return UnknownArgument;
}

/*************************************************************************************************
 Arguments:dereference - get (filled) Option by char string
 *************************************************************************************************/
Arguments::Option &Arguments::operator[]( TCHAR chOptionName )
{
	OptionMap::iterator it = findOption(chOptionName);//m_mOptions.find(chOptionName);
	
	if( it == m_mOptions.end() )
		return Option::Empty;
	else 
		return it->second;
}



/*************************************************************************************************
 *
 *   $History: Arguments.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/Arguments/src
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
