/*************************************************************************************************
 *
 * $Workfile: ChkListCtrl.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface of the CChkListCtrl class
 * This class provedes a full row selection mode for the report
 * mode list view control.
 *
 * This was a part of the Microsoft Foundation Classes C++ library.
 * Dramatically modified to be a base class without icons
 *  
 * 03/07/99		sjv creation
 *		
 *
 * #include "ChkListCtrl.h"
 */

#ifndef _CHKLISTCTRL_H
#define _CHKLISTCTRL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In ChkListCtrl.h") 
#endif

/*Added By deepak */
// the data structure
#ifdef LISTDATATYPE
#undef LISTDATATYPE
#undef LISTDATANAME
#endif

#define LISTDATATYPE	hCVar*
#define LISTDATANAME   "hCVar Pointer"
//END modification

#include <afxcview.h>
#include "ListData.h"
// Added by Deepak 
#include "ExtendedInfoDlg.h"
// END modification
//#include "resource.h"  // to get the icons

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ChkListCtrl.h") 
#endif

#define STD_BLACK    m_stdColors[1]
#define STD_BLUE     m_stdColors[6]
#define STD_GREEN    m_stdColors[12]
#define STD_RED      m_stdColors[3]
#define STD_BROWN    m_stdColors[11]
#define STD_MAROON   m_stdColors[7]
#define STD_HOTGRN   m_stdColors[4]
#define STD_WHITE    m_stdColors[2]
#define STD_YELLOW   m_stdColors[5]
#define STD_TURQUOIS m_stdColors[8]
#define STD_DRKGREY  m_stdColors[9]
#define STD_LGTGREY  m_stdColors[10]
#define STD_GOLD     m_stdColors[13]
#define STD_DRKBLU   m_stdColors[14]
#define STD_DRKCYAN  m_stdColors[15]
#define STD_TURQBLU  m_stdColors[16]

#define THE_DOCUMENTCLASS	ownerOdListData

class CChkListCtrl : public CListCtrl
{
	DECLARE_DYNCREATE(CChkListCtrl)

// Construction
public:
	CChkListCtrl();

// Attributes
protected:
	BOOL m_bFullRowSel;// currently always false - select items - not entire row
	BOOL m_bHaveCols;  // set if we painted the columns
	CImageList m_StateImageList;
	CListData * p_MyListDataSet; 
// Added by Deepak
	// This points to ViewList which is maintained in the CPropertyList class 
	CArray <int,int> *pMyView;
	// This is used switching between direction of Sort Ascending or Descending
	bool m_bSortDirection;
// END modification
	COLORREF  *m_stdColors;
    HDC       coloredHDC;

	CString   oldString,newString;
	unsigned  oldEnum,  newEnum;// used for enum and bit enum
public:

	BOOL SetFullRowSel(BOOL bFillRowSel);
	BOOL GetFullRowSel();

	BOOL m_bClientWidthSel;
	BOOL m_bStateIconEnable;
	UINT m_StateIconBitmapID;
	
// Overrides
protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChkListCtrl)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CChkListCtrl();
#ifdef _DEBUG
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual CListData * GetListDataPtr();

	void CheckItem(int nNewCheckedItem); // check/uncheck an item
// Changed By Deepak
	// At the time of initialization we need a way to pass ViewList information 
	// available in CPropertyList class Second Variable was added to cater for this need.
//	void initialize(CListData * pNewCLD);
	void initialize(CListData * pNewCLD, CArray<int,int> *pView);
// END modification
	void*  EditSubItem (int Item, int subItm, datumInfo_t* pInfo = NULL);

	CComboBox* ShowInPlaceList( int nItem, int nCol, 
                                     CStringList &lstItems,   int nSel, int nLen );
	CComboBox* ShowInPlaceList( int nItem, int nCol, 
                                     SUBITEMLIST_T &lstItems, int nSel, int nLen  );
	CComboBox* ShowInPlaceCheckList( int nItem, int nCol, 
                                     SUBITEMLIST_T &lstItems, int nSel, int nLen  );
// Added By Deepak
	// This function sorts items in a given column 
	int SortFunc (LPARAM lParamSort);
	// This function is used for showing extended information in a new dialog
	int ShowExtendedDlg(int nRow,int nColumn);
// End Modification
protected:
	static LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);
	void   RepaintSelectedItems();
	LPTSTR AddPool(CString* pstr);
    void   setStdCols(HDC hdc);
// newer stuff doesn't need this::> int    HitTestEx(CPoint &point, int *col);

// Implementation - unique edit box info
	int m_editItem;
	int m_editSubItem;
	SUBITEMLIST_T subDataList;// for enums

// Implementation - client area width
	int m_cxClient;
	int m_valueByteWidth;

    CString m_strCPool[3];
    LPTSTR  m_pstrPool[3];
    int     m_nNextFree;

// Implementation - state icon width
	int m_cxStateImageOffset;
	afx_msg LRESULT OnSetImageList(WPARAM wParam, LPARAM lParam);

// Implementation - list view colors
	COLORREF m_clrText;
	COLORREF m_clrTextBk;
	COLORREF m_clrBkgnd;
	afx_msg LRESULT OnSetTextColor(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetTextBkColor(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetBkColor(WPARAM wParam, LPARAM lParam);

// Generated message map functions
protected:
	//{{AFX_MSG(CChkListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult);
// Added By Deepak
	afx_msg void OnColumnClick(NMHDR* pNMHDR, LRESULT* pResult);
// END modification
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif //_CHKLISTCTRL_H
