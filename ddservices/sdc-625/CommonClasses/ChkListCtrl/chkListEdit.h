/*************************************************************************************************
 *
 * $Workfile: chkListEdit.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		describes edi8t types and  maps edit types to edit controls
 *		
 *
 * #include "chkListEdit.h"
 */

#ifndef _CHKLISTEDIT_H
#define _CHKLISTEDIT_H
#ifdef INC_DEBUG
#pragma message("In chkListEdit.h") 
#endif


#include "stdafx.h"
#ifndef __AFXTEMPL_H__
#include <afxtempl.h>
#endif

#include "ddbVar.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::chkListEdit.h") 
#endif


// NOTES:  0x0 is read-only text, the default
typedef enum editType_e
{
	et_asciiEdit,		// 0x00 - default
	et_floatEdit,
	et_intEdit,
	et_hexIntEdit,
	et_pkdAsciiEdit,	// 0x04
	et_passwrdEdit,	
	et_dateEdit,
	et_enumEdit,
	et_bitEnumEdit,		// 0x08
	et_booleanEdit,
	et_Reserved0A,
	et_Reserved0B,
	et_Reserved0C,
	et_Reserved0D,
	et_Reserved0E,
	et_Reserved0F
}/*typedef*/ editType_t;


// currently only used for enums (bit & straight)
typedef struct subListData_s
{
	CString elementString;
	CString elementHelp; // future use as tooltips
	unsigned int elementValue;// normally a bit mask || bit value
	struct subListData_s& operator=(struct subListData_s& src)
	{elementString = src.elementString;
	 elementHelp   = src.elementHelp;
	 elementValue  = src.elementValue;  return (*this);  };
}
/*typedef*/ subListData_t;

//#define SUBLISTDATATYPE subListData_t
#define SUBLISTDATATYPE EnumTriad_t
/*  unsigned long   val;
	string          descS;
	string          helpS;
*/

#define SUBITEMLIST_T   CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&> 

#endif // _CHKLISTEDIT_H

/*************************************************************************************************
 *
 *   $History: chkListEdit.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */