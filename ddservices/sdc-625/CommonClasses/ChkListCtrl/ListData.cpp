/*************************************************************************************************
 *
 * $Workfile: ListData.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 This is part of the chkListView/ChkListCtrl reusable class set
 */

#include "stdafx.h"
#include <afxtempl.h>

#include "ListData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define NUM_COLUMNS	8
#define NUM_SAMPLEROWS 7

static _TCHAR *_gszItem[NUM_SAMPLEROWS][NUM_COLUMNS]=
{
	_T("Yellow"),  _T("255"), _T("255"), _T("0"),   _T("40"),  _T("240"), _T("120"), _T("Neutral"),
	_T("Red"),     _T("255"), _T("0"),   _T("0"),   _T("0"),   _T("240"), _T("120"), _T("Warm"),
	_T("Green"),   _T("0"),   _T("255"), _T("0"),   _T("80"),  _T("240"), _T("120"), _T("Cool"),
	_T("Magenta"), _T("255"), _T("0"),   _T("255"), _T("200"), _T("240"), _T("120"), _T("Warm"),
	_T("Cyan"),    _T("0"),   _T("255"), _T("255"), _T("120"), _T("240"), _T("120"), _T("Cool"),
	_T("Blue"),    _T("0"),   _T("0"),   _T("255"), _T("160"), _T("240"), _T("120"), _T("Cool"),
	_T("Gray"),    _T("192"), _T("192"), _T("192"), _T("160"), _T("0"),   _T("181"), _T("Neutral")
};

/////////////////////////////////////////////////////////////////////////////
// Column Description

static _TCHAR *_gszColumnLabel[NUM_COLUMNS]=
{
	_T("Color"), _T("Red"),   _T("Green"),  _T("Blue"),  _T("Hue"),   _T("Sat"),    _T("Lum"),     _T("Type")
};

static int _gnColumnFmt[NUM_COLUMNS]=
{
   LVCFMT_LEFT, LVCFMT_RIGHT, LVCFMT_RIGHT, LVCFMT_RIGHT,LVCFMT_RIGHT, LVCFMT_RIGHT, LVCFMT_RIGHT, LVCFMT_CENTER
};

static int _gnColumnWidth[NUM_COLUMNS]=
{
	150,        50,           50,           50,           100,          50,          50,            150
};


/////////////////////////////////////////////////////////////////////////////
// CListData
CListData::CListData()
{
	NumColumns = NUM_COLUMNS;
}

CListData::~CListData()
{	
#if defined RowListing
	for (int i = 0; i < RowList.GetSize(); i++)
	{
		delete RowList[i];
	}
	RowList.RemoveAll;
#endif
}

int CListData::PreFillListData()
{	
	/* OVerload this with your data type*/
#if defined RowListing
	LISTDATATYPE  * rl;
	for (int i = 0; i < NUM_SAMPLEROWS; i++)
	{
		rl = new TYPE;

		rl->Zero  = _gszItem[i][0];
		rl->Valid = TRUE;
	
		RowList.Add(rl);
	}
	return i-1;
#else
	return 0;
#endif
}

BOOL CListData::QueryValidity(int nRow)
{
	if (nRow >= RowList.GetSize())
		return FALSE;// could be true...

    LISTDATATYPE * rl = RowList[nRow];
    if (!rl)
		return FALSE;
	else
		return rl->Valid;
}

void CListData::SetItemValidity(int nRow, BOOL nValid)
{// sets the validity as above to match the display
	if (nRow >= RowList.GetSize())
		return;// could be true...
   else
      RowList[nRow]->Valid = nValid;
	return;
}

datumInfo_t CListData::QueryItemInfo(int nRow, int nCol)
{
   datumInfo_t dI = {0,0,1,0,1};

   return dI;
}

CString CListData::QueryItemText(int nRow, int nCol, BOOL getNumber/*= FALSE*/)
{
    CString strField = _T("");

	if (nRow >= RowList.GetSize() || nCol > NUM_COLUMNS)
		return strField;

    LISTDATATYPE * rl = RowList[nRow];
    if (!rl)
	{
		strField = _T("N/A");
		return strField;
	}

#if defined RowListing
    switch (nCol) 
	{
	case 0: 	strField = rl->Zero;   break;
	case 1: 	strField = rl->One;    break;
	case 2: 	strField = rl->Two;    break;
	case 3: 	strField = rl->Three;  break;
	case 4: 	strField = rl->Four;   break;
	case 5: 	strField = rl->Five;   break;
	case 6: 	strField = rl->Six;    break;
	case 7: 	strField = rl->Seven;  break;
	default: strField = _T("Bad");

    }
#else
	strField = _T("Undefined in Parent.");
#endif
    return strField;
}

sColDesc& CListData::GetColDesc(int ColNum, sColDesc& nColDesc)
{
	if (ColNum < NumColumns)
	{
		_tcscpy(nColDesc.ColLabel, _gszColumnLabel[ColNum]);
		nColDesc.ColWidth = _gnColumnWidth[ColNum];
		nColDesc.ColFmt   = _gnColumnFmt[ColNum];
	}
	else
	{
		_tcscpy(nColDesc.ColLabel,_T("UNK"));
		nColDesc.ColWidth = 50;
		nColDesc.ColFmt   = LVCFMT_CENTER;
	}
	return nColDesc;
}

int CListData::FillColumnHead(CListCtrl * p_MyCtrl)
{
	return 0;
}


inline int CListData::GetSize(void)
{	return RowList.GetSize();}



/*************************************************************************************************
 *
 *   $History: ListData.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */