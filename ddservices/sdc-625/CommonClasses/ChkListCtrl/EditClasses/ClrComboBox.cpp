// ClrComboBox.cpp : implementation file

#include "stdafx.h"
#include "ClrComboBox.h"
#include "SDC625.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClrComboBox

const COLORREF CClrComboBox::m_WHITE = RGB(255, 255, 255);
const COLORREF CClrComboBox::m_YELLOW = RGB(255, 255, 0);

IMPLEMENT_DYNAMIC( CClrComboBox, CComboBox )

CClrComboBox::CClrComboBox()
{
	m_color = m_WHITE;
	m_state = FALSE;
    m_isEnabled = TRUE;
}

CClrComboBox::~CClrComboBox()
{
}


BEGIN_MESSAGE_MAP(CClrComboBox, CComboBox)
	//{{AFX_MSG_MAP(CClrComboBox)
	ON_WM_RBUTTONDOWN()
    ON_WM_LBUTTONDOWN()
    ON_WM_MOUSEWHEEL()
    ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClrComboBox message handlers


BOOL CClrComboBox::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (message != WM_CTLCOLOREDIT)
	{
		return CComboBox::OnChildNotify(message, wParam, lParam, pLResult);

	}
	
	HDC hdcChild = (HDC) wParam;
	SetBkColor(hdcChild, m_color);
	HBRUSH brush = (HBRUSH)m_brush;
	brush = CreateSolidBrush(m_color);

	//Send what would have been the return value of OnCtlColor() - the 
	// brush handle - back in pLResult;
	*pLResult = (LRESULT)(brush);

	// Return TRUE to indicate the message was handled
	return TRUE;
}

void CClrComboBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)// sjv 07jul2017
{//is no base class
	CString fontName;
	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);
	dc.GetTextFace(fontName);
	
	CRect rc = lpDrawItemStruct->rcItem;
	
	if (lpDrawItemStruct->itemState & ODS_FOCUS)
		dc.DrawFocusRect(&rc);
	
	if (lpDrawItemStruct->itemID == -1)
		return;

	int nIndexDC = dc.SaveDC();

// which one are we working on?
	CString csCurFontName;
	GetLBText(lpDrawItemStruct->itemID, csCurFontName);
	
	// try::
	wchar_t *pTemp = csCurFontName.GetBuffer();

	CSize sz;
	int iPosY = 0;	
	int iOffsetX = 10;
	
			sz = dc.GetTextExtent(csCurFontName);
			iPosY = (rc.Height() - sz.cy) / 2;
			dc.TextOut(rc.left+iOffsetX, rc.top + iPosY,csCurFontName);

	
	dc.RestoreDC(nIndexDC);

	dc.Detach();

}

void CClrComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{// there is no base class

	// it has no font at this time
	LOGFONT LogFont;
	memset(&LogFont, 0x00, sizeof(LogFont));
	wcscpy(LogFont.lfFaceName, CONST_STRING_FONT);
	LogFont.lfHeight = 18;

	CFont * m_pFont = new CFont;
	m_pFont->CreateFontIndirect(&LogFont);
	SetFont(m_pFont);
	///////////////////////////////////////////////////

	CFont* pOrigFont = GetFont(); // returns empty
					
	LOGFONT lof;
	if (pOrigFont)
		pOrigFont->GetLogFont(&lof);

	//CString csFontName;
	//// this returns an empty string  GetLBText(lpMeasureItemStruct->itemID, csFontName);

	//CFont cf;
	//if (!cf.CreateFont(16,0,0,0,FW_NORMAL,FALSE, FALSE, FALSE,DEFAULT_CHARSET ,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,ANTIALIASED_QUALITY,DEFAULT_PITCH, csFontName))
	//{
	//	ASSERT(0);
	//	return;
	//}

	LOGFONT lf;
	//cf.GetLogFont(&lf);
	m_pFont->GetLogFont(&lf);

	//if ((m_style == NAME_ONLY) || (m_style == SAMPLE_ONLY) || (m_style == NAME_GUI_FONT))
	//{
	//	m_iMaxNameWidth = 0;
	//	m_iMaxSampleWidth = 0;
	//}
	//else
	//{
	//	CClientDC dc(this);

	//	// measure font name in GUI font
	//	HFONT hFont = ((HFONT)GetStockObject( DEFAULT_GUI_FONT ));
	//	HFONT hf = (HFONT)dc.SelectObject(hFont);
	//	CSize sz = dc.GetTextExtent(csFontName);
	//	m_iMaxNameWidth = max(m_iMaxNameWidth, sz.cx);
	//	dc.SelectObject(hf);

	//	// measure sample in cur font
	//	hf = (HFONT)dc.SelectObject(cf);
	//	  if (hf)
	//	  {
	//		   sz = dc.GetTextExtent(m_csSample);
	//		   m_iMaxSampleWidth = max(m_iMaxSampleWidth, sz.cx);
	//		   dc.SelectObject(hf);
	//	  }
	//}

	lpMeasureItemStruct->itemHeight = lf.lfHeight + 12;
	lpMeasureItemStruct->itemWidth  = 0x5A;
}

void CClrComboBox::Highlight(BOOL state)
{
    // The control should not be able to be changed by the user
    // while its disable so prevent any 'yellow' highlighting, POB - 28 Aug 2008
    if (m_isEnabled)
    {
	m_state = state;

	if (m_state == TRUE)
        {
		m_color = m_YELLOW;
        }
	    else
        {
            m_color = m_WHITE;
        }
	
	Invalidate();
}
}

void CClrComboBox::OnRButtonDown(UINT nFlags, CPoint point) 
{
    // Ignore CPoint value.  It will not satisfy CdrawBase::HitTest
    // Get a clean version, POB - 4/14/2014
    DWORD dwPosition = GetMessagePos();
    
    GetParent()->PostMessage(WM_RBUTTONDOWN, nFlags, dwPosition);
	
    CComboBox::OnRButtonDown(nFlags, point);
}

void CClrComboBox::OnLButtonDown(UINT nFlags, CPoint point) 
{
    // The drop-down list box will only be shown for 'left clicks' 
    // when the combo box is enabled, POB, 28 Aug 2008
    if (m_isEnabled)
    {
	    CComboBox::OnLButtonDown(nFlags, point);
    }
}

void CClrComboBox::OnLButtonDblClk (UINT nFlags, CPoint point) 
{
    if (m_isEnabled)
    {
        // The drop-down list box will only be shown for 'double left clicks' 
        // when the combo box is enabled, POB, 28 Aug 2008
	    CComboBox::OnLButtonDblClk(nFlags, point);
    }
}

BOOL CClrComboBox::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    if (m_isEnabled)
    {
        // The moust wheel will only allow for selection of enumerations
        // when the combo box is enabled, POB, 28 Aug 2008
	    return CComboBox::OnMouseWheel(nFlags, zDelta, pt);
    }

    return TRUE;
}


BOOL CClrComboBox::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			int id = GetDlgCtrlID();
			WPARAM wParam = MAKEWPARAM(id, VK_RETURN);

			GetParent()->PostMessage(WM_COMMAND,wParam, NULL );
			return TRUE;
		}
		if (pMsg->wParam == VK_ESCAPE)
		{
			int id = GetDlgCtrlID();
			WPARAM wParam = MAKEWPARAM(id, VK_ESCAPE);

			GetParent()->PostMessage(WM_COMMAND,wParam, NULL );
			return TRUE;
		}
	}
	return CComboBox::PreTranslateMessage(pMsg);
}

// This funtion allows for both the enabling and disabling of key strokes by the user.
// When a function is disabled, it is still allowed to receive, EN_SETFOCUS and EN_KILLFOCUS, POB - 28 Aug 2008
void CClrComboBox::Enable(BOOL bEnable)
{
    m_isEnabled = bEnable;  
}

// This funtion gives information on whether the control is enabled or disabled, POB - 28 Aug 2008
BOOL CClrComboBox::IsEnabled()
{
    return m_isEnabled;
}