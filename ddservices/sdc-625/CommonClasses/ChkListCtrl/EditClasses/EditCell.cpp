/*************************************************************************************************
 *
 * $Workfile: EditCell.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 This is the ascii editable cell of the chklistctrl
 */

#include "stdafx.h"
//#include "ListEditor.h"
//#include "ListCtrl.h"
//#include "FrameView.h"

#ifndef IS_IN_FRAME
#include "ChkListCtrl.h"
// which includes  #include "ListData.h"
#include "chkListEdit.h "

#endif
#include "EditCell.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef IS_IN_FRAME
#define PDATALIST    pLV->m_pDataList
#define LISTCTRL     pLV->GetListCtrl().
#define EDITSUBITEM  pLV->EditSubItem
#else
//#define PDATALIST    pLV
#define CHKLSTCTRL	 ((CChkListCtrl*)pLV)
#define PDATALIST    ((CListData/*CDDlistData*/*)(CHKLSTCTRL->GetListDataPtr()))
#define LISTCTRL     pLV->
#define EDITSUBITEM  CHKLSTCTRL->EditSubItem
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditCell

CEditCell::CEditCell () :bEscape (FALSE)
{
    pLV      = NULL;
    Item     = -1;
    SubItem  = -1;
    InitText = "";
	theEditType = et_Reserved0F;// invalid
}

//CEditCell::CEditCell (CFrameView *pLstVw, int iItem, int iSubItem, CString sInitText)
//:   bEscape (FALSE)
//{
//    pLV      = pLstVw;
//    Item     = iItem;
//    SubItem  = iSubItem;
//    InitText = sInitText;
//}

CEditCell::CEditCell (CListCtrl *pListCtrl, int iItem, int iSubItem, CString sInitText)
:   bEscape (FALSE)
{
    pLV      = pListCtrl;
    Item     = iItem;
    SubItem  = iSubItem;
    InitText = sInitText;
	theEditType = et_Reserved0F;// invalid
}

CEditCell::~CEditCell()
{
}

BEGIN_MESSAGE_MAP(CEditCell, CEdit)
    //{{AFX_MSG_MAP(CEditCell)
    ON_WM_KILLFOCUS()
    ON_WM_NCDESTROY()
    ON_WM_CHAR()
    ON_WM_CREATE()
    ON_WM_GETDLGCODE()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditCell message handlers

void 
CEditCell::SetListText()
{
    CString Text;
    GetWindowText (Text);

//    LISTCTRL SetItemText(Item, SubItem, Text);

     // Send Notification to parent of ListView ctrl
     LV_DISPINFO dispinfo;
     dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
     dispinfo.hdr.idFrom   = GetDlgCtrlID();
     dispinfo.hdr.code     = LVN_ENDLABELEDIT;

     dispinfo.item.mask       = LVIF_TEXT;
     dispinfo.item.iItem      = Item;
     dispinfo.item.iSubItem   = SubItem;
     dispinfo.item.pszText    = LPTSTR((LPCTSTR)Text);// m_bESC ? NULL : LPTSTR((LPCTSTR)str);
     dispinfo.item.cchTextMax = Text.GetLength();
//?	 if ( m_valLen > 4 ) // won't fit in the state variable
//?	 {// won't fit in the m_bitValue either...
//?	 }
//?	 dispinfo.item.state      = m_bitValue; //UINT caller must lookup in type list
//?
     GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo );

     PostMessage( WM_CLOSE );
}

BOOL 
CEditCell::PreTranslateMessage (MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE ||  
			pMsg->wParam == VK_BACK   || pMsg->wParam == VK_END    ||
			pMsg->wParam == VK_HOME   || pMsg->wParam == VK_LEFT   ||
			pMsg->wParam == VK_RIGHT  || pMsg->wParam == VK_SELECT ||
			pMsg->wParam == VK_CLEAR  || pMsg->wParam == VK_INSERT || 
			pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_TAB    ||
			pMsg->wParam == VK_UP     || pMsg->wParam == VK_DOWN   || GetKeyState (VK_CONTROL))
			{
				::TranslateMessage (pMsg);
				::DispatchMessage (pMsg);
				return TRUE;		    	// DO NOT process further
			}
    }
    return CEdit::PreTranslateMessage (pMsg);
}

void 
CEditCell::OnKillFocus (CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);

    SetListText();

    LISTCTRL RedrawItems( Item, Item );

    DestroyWindow();

}

void 
CEditCell::OnNcDestroy() 
{
    CEdit::OnNcDestroy();
    
    delete this;
}

/* fills the params with the next editable cell in the direction desired*/
int CEditCell::NextCell(int &item, int& subitem, direction_t dir)
{
   datumInfo_t cellInfo;
   int nMaxColumn = (LISTCTRL GetHeaderCtrl()->GetItemCount())-1;
   int nMaxRow    = (LISTCTRL GetItemCount())-1;  // highest valid rows
   int nRowPerPg  =  LISTCTRL GetCountPerPage(); //current + 1 page of items
	int nItem    = item, 
       nSubItem = subitem;
   int r = 1;

   if (dir == goFarRight)
   {  
      nSubItem = nMaxColumn;
      dir = goLeft; // backup till we can edit
   }
   if (dir == goFarLeft)
   {  
      nSubItem = 0;
      dir = goRight; // find one we can edit
   }

   while (r) // while location not found
   {
      switch (dir)
      {
      case goRight: 
         {
            nSubItem++;
            if (nSubItem <= nMaxColumn)
            {
               break; // from switch
            }
            else // prepare to go down
            {
               nSubItem = 0;
            }// fall through
         }
      case goDown:
         {
            nItem++;
            if (nItem > nMaxRow)
            {
               nItem --;
               r = 0;// time to go
            }
            // else exit to see if its editable
         }break;
      case goLeft:
         {
            nSubItem--;
            if (nSubItem >= 0)
            {
               break; // from switch
            }
            else // prepare to go down
            {
               nSubItem = nMaxColumn;
            }// fall through
         }
      case goUp:
         {
            nItem--;
            if (nItem < 0)
            {
               nItem = 0;
               r = 0;// time to go
            }
            // else exit to see if its editable
         }break;
      case goPgUp:
         {// assume current subitem is editable
            nItem -= nRowPerPg; //current - 1 page of items
			   if (nItem < 0)
            {
               nItem = 0;
            }
            // else use it
         }break;
      case goPgDwn:
         {// assume current subitem is editable
            nItem += nRowPerPg; //current + 1 page of items
			   if (nItem > nMaxRow)
            {
               nItem = nMaxRow;
            }
            // else use it
         }break;
      default: // stay
         {
            if ( r > 1 )
            {
               return 1; // needs to change
            }// else see if its OK
            r++; // leave the params & exit
         }
      }// endswitch
      // determine if it's editable
//cellInfo = ((CHKLSTCTRL->GetListDataPtr()))->QueryItemInfo(nItem, nSubItem);//(CListData*)
      cellInfo = PDATALIST->QueryItemInfo(nItem, nSubItem);
      if (cellInfo.isEdit)
      {
         r = 0;// we've got one
      }
      // else loop to go do it again
   }//wend

   if (nItem != item || nSubItem != subitem)
   {
      item = nItem;
      subitem = nSubItem;
      return 1; // change
   }
   else
   {
      return 0;// no change
   }
}

void 
CEditCell::OnKeyDown (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    // Up and down are in the OnKeyDown so that the user can hold down the arrow
    // keys to scroll through the entries.
    BOOL Control = GetKeyState (VK_CONTROL) < 0;
    direction_t d = dontgo;

    switch (nChar)
    {
		case VK_UP :
		{
         d = goUp;
		}break;
		case VK_DOWN :
		{
         d = goDown;// if we move
		}break;
		case VK_HOME :
		{
			if (Control)
            d = goFarLeft;
		}break;
		case VK_END :
		{
			if (Control)				
            d = goFarRight;
		}break;
    }
    if ( d != dontgo)
    {
      if ( NextCell(Item, SubItem, d) )// if we move
	      EDITSUBITEM (Item, SubItem);// generates a new EditCtrl
      return;
    }
	  
    CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
return;
}

void 
CEditCell::OnKeyUp (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
   int It = Item, Si = SubItem;
    switch (nChar)
    {
		case VK_NEXT :
		{
         if ( NextCell(It, Si, goPgDwn) )// if we move
	         EDITSUBITEM (It, Si);// generates a new EditCtrl
         return;
		}
		case VK_PRIOR :
		{
         if ( NextCell(It, Si, goPgUp) )// if we move
	         EDITSUBITEM (It, Si);// generates a new EditCtrl
         return;
		}
    }
    
    CEdit::OnKeyUp (nChar, nRepCnt, nFlags);
}

void 
CEditCell::OnChar (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    BOOL Shift = GetKeyState (VK_SHIFT) < 0;
    // find the mx cols
    CHeaderCtrl* pHeader = LISTCTRL GetHeaderCtrl();
    int nColumnCount = pHeader->GetItemCount();

    direction_t d = dontgo;
    int It = Item,Si = SubItem;

    switch (nChar)
    {
		case VK_ESCAPE :
		{
			if (nChar == VK_ESCAPE)
				bEscape = TRUE;
			GetParent()->SetFocus();
			return;
		}
		case VK_RETURN :
		{
	//		SetListText();
         if ( NextCell(It, Si, goDown) )// if we move
	         EDITSUBITEM (It, Si);// generates a new EditCtrl
         else
            GetParent()->SetFocus();// remove edit
         return;
		}
		case VK_TAB :
		{
			if (Shift)  // shift tab
			{
            d = goLeft;
			}
			else        // straight tab
			{
            d = goRight;
			}
         if ( NextCell(It, Si, d) )// if we move
         {
   //         SetListText();

	         EDITSUBITEM (It, Si);// generates a new EditCtrl
         }
         else
            GetParent()->SetFocus();// remove edit
         return;
		}

		// CASES TO SKIP VALIDATION
		case VK_BACK:
		case VK_CLEAR:
		case VK_END:
		case VK_HOME:
		case VK_LEFT:
		case VK_RIGHT:
		case VK_SELECT:
		case VK_INSERT:
		case VK_DELETE:
			// no validation
			break;
	
		default:
		{
			string validChars;
			string::size_type stIdx;
			switch (theEditType)
			{
			case et_floatEdit:
				validChars = "0123456789-+Ee.NnaA";
				break;
			case et_intEdit:
				validChars = "0123456789-+";
				break;
			case et_hexIntEdit:
				validChars = "0123456789AaBbCcDdEeFfxX";
				break;
			case et_pkdAsciiEdit:	// 0x04
				// stevev - added style UPPERCASE so we made the lower case legal
				validChars = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\}^_abcdefghijklmnopqrstuvwxyz";
				break;
			}// endswitch

			switch (theEditType)
			{
			case et_floatEdit:
			case et_intEdit:
			case et_hexIntEdit:
			case et_pkdAsciiEdit:	// 0x04
				stIdx = validChars.find((char)nChar);
				if (stIdx == std::string::npos) // char not in acceptable set
				{
					return;// stop processing
				}
				break;
			case et_asciiEdit:		// 0x00 - default
			case et_passwrdEdit:	
			case et_dateEdit:
			case et_enumEdit:
			case et_bitEnumEdit:		// 0x0
			case et_booleanEdit:
				{
					; // nop
				}
				break;
			}// endswitch
		}
		break;
    }// endswitch on char

/*
to translate chars::>

if (nChar == 'a' || nChar == 'A')
     nChar = 'X';
  DefWindowProc(WM_CHAR, nChar, MAKELONG(nRepCnt, nFlags));

in lieu of following OnChar call

*/

    CEdit::OnChar (nChar, nRepCnt, nFlags);

    // Resize edit control if needed

    // Get text extent
    CString Text;

    GetWindowText (Text);
    CWindowDC DC (this);
    CFont *pFont = GetParent()->GetFont();
    CFont *pFontDC = DC.SelectObject (pFont);
    CSize Size = DC.GetTextExtent (Text);
    DC.SelectObject (pFontDC);
    Size.cx += 5;			   	// add some extra buffer

    // Get client rect
    CRect Rect, ParentRect;
    GetClientRect (&Rect);
    GetParent()->GetClientRect (&ParentRect);

    // Transform rect to parent coordinates
    ClientToScreen (&Rect);
    GetParent()->ScreenToClient (&Rect);

    // Check whether control needs to be resized and whether there is space to grow
    if (Size.cx > Rect.Width())
    {
		if (Size.cx + Rect.left < ParentRect.right )
			Rect.right = Rect.left + Size.cx;
		else
			Rect.right = ParentRect.right;
		MoveWindow (&Rect);
    }
}

int 
CEditCell::OnCreate (LPCREATESTRUCT lpCreateStruct) 
{
    if (CEdit::OnCreate (lpCreateStruct) == -1)
		return -1;
// get the type into the var
	
	theEditType = ((editType_t)PDATALIST->QueryItemInfo(Item, SubItem).editType) ;
	
	
    // Set the proper font
    CFont* Font = GetParent()->GetFont();
    SetFont (Font);

    SetWindowText (InitText);
    SetFocus();
    SetSel (0, -1);
    return 0;
}

UINT 
CEditCell::OnGetDlgCode() 
{
   return CEdit::OnGetDlgCode() | DLGC_WANTARROWS | DLGC_WANTTAB;
}



/*************************************************************************************************
 *
 *   $History: EditCell.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */