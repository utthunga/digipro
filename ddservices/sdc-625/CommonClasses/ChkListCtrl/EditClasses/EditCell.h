/*************************************************************************************************
 *
 * $Workfile: EditCell.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "EditCell.h"
 */
#if !defined(_EDIT_CELL_H)
#define _EDIT_CELL_H

//#define		
#undef	\
IS_IN_FRAME  

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#ifdef INC_DEBUG
#pragma message("No Includes::EditCell.h") 
#endif

// EditCell.h : header file
//
#define IDC_EDITCELL  104567


typedef enum direction_e
{
   dontgo,
   goRight,
   goLeft,
   goUp,
   goDown,
   goFarRight, // Cntrl-end
   goFarLeft,  // Cntrl-home
   goPgUp,
   goPgDwn
}
/*typedef*/direction_t;

class CFrameView;

/////////////////////////////////////////////////////////////////////////////
// CEditCell window		
class CEditCell : public CEdit	
//class CEditCell : public CRichEditCtrl
{
public:
   CEditCell();
	 // CEditCell (gxListCtrl* pCtrl, int iItem, int iSubItem, CString sInitText);
#ifdef IS_IN_FRAME
    CEditCell(CFrameView *pLstVw, int iItem, int iSubItem, CString sInitText);
#else
    CEditCell (CListCtrl *pListCtrl, int iItem, int iSubItem, CString sInitText);
#endif
    virtual ~CEditCell();
    void    SetListText();

    int NextCell(int &item, int& subitem, direction_t dir);

    //{{AFX_VIRTUAL(gxEditCell)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

protected:
#ifdef IS_IN_FRAME
    CFrameView *pLV;
#else
    CListCtrl* pLV;
#endif
	editType_t  theEditType;
   
    int			Item;
    int			SubItem;
    CString		InitText;
    BOOL		   bEscape;
    
    //{{AFX_MSG(CEditCell)
    afx_msg void OnKillFocus(CWnd* pNewWnd);
    afx_msg void OnNcDestroy();
    afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg UINT OnGetDlgCode();
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_EDIT_CELL_H)


/*************************************************************************************************
 *
 *   $History: EditCell.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */