/*************************************************************************************************
 *
 * $Workfile: CheckComboBox.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "CheckComboBox.h"
 */
#if !defined(AFX_CHECKCOMBOBOX_H__66750D93_95DB_11D3_9325_444553540000__INCLUDED_)
#define AFX_CHECKCOMBOBOX_H__66750D93_95DB_11D3_9325_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In CheckComboBox.h") 
#endif

#include "comboBox.h"
#include "chkListEdit.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::CheckComboBox.h") 
#endif


class CCheckComboBox : public CInPlaceList // public CComboBox
{
	DECLARE_DYNAMIC(CCheckComboBox)

public:
	void SetValue(int bitValue);
	// iItem, iSubItem, ::> unknown at this time...
	// SUBITEMLIST_T *plstItems ::> ptr to list of template type
	// nSel, nLen   ::> number of bytes total (1 to 4)
	// bKillonFocus ::> TRUE==Delete/Kill the box in OnKillFocus()<required by xmtr-dd>
	CCheckComboBox(int iItem, int iSubItem, SUBITEMLIST_T *plstItems, int nSel, int nLen, BOOL bKillonFocus = TRUE) ;

	virtual ~CCheckComboBox();

	// Selects all/unselects the specified item
	INT SetCheck(INT nIndex, BOOL bFlag);

	// Returns checked state
	BOOL GetCheck(INT nIndex);

	// Selects all/unselects all
	void SelectAll(BOOL bCheck = TRUE);

	unsigned int GetSelectedValue();

	void ModifyListStrings(SUBITEMLIST_T *plstItems);

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCheckComboBox)
	protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCheckComboBox)	
    afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg LRESULT OnCtlColorListBox(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetText(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetTextLength(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnDropDown();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

protected:
	// the data
	int     m_iItem;
	int     m_iSubItem;
	SUBITEMLIST_T  m_lstItems; // we DO own this memory!!
	unsigned int   m_bitValue;  // if we need more than 32 we'll have to go to another data type
	int            m_valLen;
	CString        m_strFmt;
	BOOL		   m_bKillOnFocus;

	// Routine to update the text
	void RecalcText();

	// The subclassed COMBOLBOX window (notice the 'L')
	HWND m_hListBox;

	// The string containing the text to display
	CString m_strText;
	BOOL m_bTextUpdated;

	// A flag used in MeasureItem, see comments there
	BOOL m_bItemHeightSet;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHECKCOMBOBOX_H__66750D93_95DB_11D3_9325_444553540000__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: CheckComboBox.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
