#if !defined(AFX_CLRCOMBOBOX_H__0104F304_EB8D_11D3_A109_0000E8D6C4AD__INCLUDED_)
#define AFX_CLRCOMBOBOX_H__0104F304_EB8D_11D3_A109_0000E8D6C4AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ClrComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CClrComboBox window

class CClrComboBox : public CComboBox
{
	DECLARE_DYNAMIC(CClrComboBox)

// Construction
public:
	CClrComboBox();

// Attributes
public:

// Operations
public:
	void Highlight(BOOL state = TRUE);
    void Enable(BOOL bEnable);
    BOOL IsEnabled();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClrComboBox)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);// sjv 07jul2017	
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);// sjv 07jul2017
	protected:
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CClrComboBox();


	// Generated message map functions
protected:
	//{{AFX_MSG(CClrComboBox)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	BOOL m_state;
    BOOL m_isEnabled;
	COLORREF m_color;
	CBrush m_brush;
	static const COLORREF m_YELLOW;
	static const COLORREF m_WHITE;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLRCOMBOBOX_H__0104F304_EB8D_11D3_A109_0000E8D6C4AD__INCLUDED_)

