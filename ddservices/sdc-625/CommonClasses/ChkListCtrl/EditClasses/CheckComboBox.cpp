/*************************************************************************************************
 *
 * $Workfile: CheckComboBox.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */
// CheckComboBox.cpp 
//
// Written by Magnus Egelberg (magnus.egelberg@lundalogik.se)
//
// Copyright (C) 1999, Lundalogik AB, Sweden. All rights reserved.
// 
// http://www.codeproject.com/combobox/checkcombo_src.zip
//
// Code Project submission guidelines:
// If you post to CodeProject then you retain copyright of your article and code. You also give 
// CodeProject permission to use it in a fair manner and also permit all developers to freely use 
// the code in their own applications.

#include "stdafx.h"
// #include "CheckCombo.h"
#include "CheckComboBox.h"
#include "messageUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static WNDPROC m_pWndProc = 0;
static CCheckComboBox *m_pComboBox = 0;


//BEGIN_MESSAGE_MAP(CCheckComboBox, CComboBox)
BEGIN_MESSAGE_MAP(CCheckComboBox, CInPlaceList)
	//{{AFX_MSG_MAP(CCheckComboBox)
    ON_WM_CREATE()
    ON_WM_KILLFOCUS()
	ON_MESSAGE(WM_CTLCOLORLISTBOX, OnCtlColorListBox)
	ON_MESSAGE(WM_GETTEXT, OnGetText)
	ON_MESSAGE(WM_GETTEXTLENGTH, OnGetTextLength)
	ON_CONTROL_REFLECT_EX(CBN_DROPDOWN, OnDropDown)
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//
// The subclassed COMBOLBOX message handler
//
extern "C" LRESULT FAR PASCAL ComboBoxListBoxProc(HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{

	switch (nMsg) {

		
		case WM_RBUTTONDOWN: {
			// If you want to select all/unselect all using the
			// right button, remove this ifdef. Personally, I don't really like it
			#if FALSE

				if (m_pComboBox != 0) {
					INT nCount = m_pComboBox->GetCount();
					INT nSelCount = 0;

					for (INT i = 0; i < nCount; i++) {
						if (m_pComboBox->GetCheck(i))
							nSelCount++;
					}


					m_pComboBox->SelectAll(nSelCount != nCount);

					// Make sure to invalidate this window as well
					InvalidateRect(hWnd, 0, FALSE);
					m_pComboBox->GetParent()->SendMessage(WM_COMMAND, MAKELONG(GetWindowLong(m_pComboBox->m_hWnd, GWL_ID), CBN_SELCHANGE), (LPARAM)m_pComboBox->m_hWnd);

				}
			#endif

			break;
		}

		// Make the combobox always return -1 as the current selection. This
		// causes the lpDrawItemStruct->itemID in DrawItem() to be -1
		// when the always-visible-portion of the combo is drawn
		case LB_GETCURSEL: {
			return -1;
		}


		case WM_CHAR: {
			if (wParam == VK_SPACE) {
				// Get the current selection
#ifndef _WIN32_WCE	// PAW function missing 01/05/09
				INT nIndex = CallWindowProcA(m_pWndProc, hWnd, LB_GETCURSEL, wParam, lParam);

				CRect rcItem;
				SendMessage(hWnd, LB_GETITEMRECT, nIndex, (LONG)(VOID *)&rcItem);
				InvalidateRect(hWnd, rcItem, FALSE);

				// Invert the check mark
				m_pComboBox->SetCheck(nIndex, !m_pComboBox->GetCheck(nIndex));
#endif
				// Notify that selection has changed
				m_pComboBox->GetParent()->SendMessage(WM_COMMAND, 
					                                  MAKELONG(GetWindowLong(m_pComboBox->m_hWnd, GWL_ID), 
													           CBN_SELCHANGE ), 
													  (LPARAM)m_pComboBox->m_hWnd);
				return 0;
			}

			break;
		}


		case WM_LBUTTONDOWN: {

			CRect rcClient;
			GetClientRect(hWnd, rcClient);

			CPoint pt;
			pt.x = LOWORD(lParam);
			pt.y = HIWORD(lParam);


			if (PtInRect(rcClient, pt)) {
				INT nItemHeight = SendMessage(hWnd, LB_GETITEMHEIGHT, 0, 0);
				INT nTopIndex   = SendMessage(hWnd, LB_GETTOPINDEX, 0, 0);

				// Compute which index to check/uncheck
				INT nIndex = nTopIndex + pt.y / nItemHeight;

				CRect rcItem;
				SendMessage(hWnd, LB_GETITEMRECT, nIndex, (LONG)(VOID *)&rcItem);

				if (PtInRect(rcItem, pt)) {
					// Invalidate this window
					InvalidateRect(hWnd, rcItem, FALSE);
					m_pComboBox->SetCheck(nIndex, !m_pComboBox->GetCheck(nIndex));

					// Notify that selection has changed
					m_pComboBox->GetParent()->SendMessage(WM_COMMAND, MAKELONG(GetWindowLong(m_pComboBox->m_hWnd, GWL_ID), CBN_SELCHANGE), (LPARAM)m_pComboBox->m_hWnd);


				}
			}

			// Do the default handling now (such as close the popup
			// window when clicked outside)
			break;
		}

		case WM_LBUTTONUP: {
			// Don't do anything here. This causes the combobox popup
			// windows to remain open after a selection has been made
			return 0;
		}
	}

	return CallWindowProc(m_pWndProc, hWnd, nMsg, wParam, lParam);
}



IMPLEMENT_DYNAMIC( CCheckComboBox, CInPlaceList )

CCheckComboBox::CCheckComboBox(int iItem, int iSubItem, SUBITEMLIST_T *plstItems, int nSel, int nLen, BOOL bKillonFocus) 
			   : CInPlaceList(iItem, iSubItem, NULL, nSel, nLen)               
{
	m_hListBox       = 0;
	m_bTextUpdated   = FALSE;
	m_bItemHeightSet = FALSE;
	m_bitValue       = (unsigned int) nSel;
	m_bKillOnFocus   = bKillonFocus;

     m_iItem    = iItem;
     m_iSubItem = iSubItem;
	 m_valLen   = (nLen > 4)?4:nLen;

	SUBLISTDATATYPE localStruct;

	if (plstItems)
	{
		for ( int i = 0; i < plstItems->GetSize(); i++)
		{
			localStruct = plstItems->ElementAt(i);
			m_lstItems.Add(localStruct);     // copy 'em in (source list is on the creator's stack)
		}
	}

	m_strFmt.Format( M_UI_FORMAT_001, _T('%'),(nLen*2) );
}

CCheckComboBox::~CCheckComboBox()
{
}


int CCheckComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	TRACE(_T("CCheckComboBox.OnCreate Entry.\n"));
     if (CComboBox::OnCreate(lpCreateStruct) == -1)
             return -1;
     
     // Set the proper font
     CFont* font = GetParent()->GetFont();
     SetFont(font);

	 if ( m_lstItems.GetSize() > 0 )
	 {
		 for( int i = 0; i < m_lstItems.GetSize(); i++ )
		 {
			AddString( (LPCTSTR) (m_lstItems.ElementAt( i ).descS.c_str() ) );
			if (m_lstItems.ElementAt( i ).val & m_bitValue)
			{
				SetCheck(i, TRUE);
			}
			else
			{
				SetCheck(i, FALSE);
			}
		 }
		 //SetCurSel( /*m_nSel*/ );
	 }
     SetFocus();
	TRACE(_T("CCheckComboBox.OnCreate Exit.\n"));
     return 0;
}
void CCheckComboBox::OnKillFocus(CWnd* pNewWnd) 
{
     CComboBox::OnKillFocus(pNewWnd);
     
	 // This is an indication set by the caller not to destroy the control.
	 // Otherwise the control gets destroyed (Xmtr requirement)
	 if (m_bKillOnFocus == FALSE) 
		 return;

     CString str;
     GetWindowText(str);

     // Send Notification to parent of ListView ctrl
     LV_DISPINFO dispinfo;
     dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
     dispinfo.hdr.idFrom   = GetDlgCtrlID();
     dispinfo.hdr.code     = LVN_ENDLABELEDIT;

     dispinfo.item.mask       = LVIF_TEXT;
     dispinfo.item.iItem      = m_iItem;
     dispinfo.item.iSubItem   = m_iSubItem;
     dispinfo.item.pszText    = LPTSTR((LPCTSTR)str);// m_bESC ? NULL : LPTSTR((LPCTSTR)str);
     dispinfo.item.cchTextMax = str.GetLength();
	 if ( m_valLen > 4 ) // won't fit in the state variable
	 {// won't fit in the m_bitValue either...
	 }
	 dispinfo.item.state      = m_bitValue; //UINT caller must lookup in type list

     GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo );

     PostMessage( WM_CLOSE );
}
LRESULT CCheckComboBox::OnCtlColorListBox(WPARAM wParam, LPARAM lParam) 
{
	// If the listbox hasn't been subclassed yet, do so...
	if (m_hListBox == 0) {
		HWND hWnd = (HWND)lParam;

		if (hWnd != 0 && hWnd != m_hWnd) {
			// Save the listbox handle
			m_hListBox = hWnd;

			// Do the subclassing
			m_pWndProc = (WNDPROC)GetWindowLong(m_hListBox, GWL_WNDPROC);
			SetWindowLong(m_hListBox, GWL_WNDPROC, (LONG)ComboBoxListBoxProc);
		}
	}

	
	return DefWindowProc(WM_CTLCOLORLISTBOX, wParam, lParam);
}


void CCheckComboBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	HDC dc = lpDrawItemStruct->hDC;

	CRect rcBitmap = lpDrawItemStruct->rcItem;
	CRect rcText   = lpDrawItemStruct->rcItem;

	CString strText;

	// 0 - No check, 1 - Empty check, 2 - Checked
	INT nCheck = 0;

	// Check if we are drawing the static portion of the combobox
	if ((LONG)lpDrawItemStruct->itemID < 0) {
		// Make sure the m_strText member is updated
		RecalcText();

		// Get the text
		strText = m_strText;

		// Don't draw any boxes on this item
		nCheck = 0;
	}

	// Otherwise it is one of the items
	else {
		GetLBText(lpDrawItemStruct->itemID, strText);
		nCheck = 1 + (GetItemData(lpDrawItemStruct->itemID) != 0);

		TEXTMETRIC metrics;
		GetTextMetrics(dc, &metrics);

		rcBitmap.left    = 0;
		rcBitmap.right   = rcBitmap.left + metrics.tmHeight + metrics.tmExternalLeading + 6;
		rcBitmap.top    += 1;
		rcBitmap.bottom -= 1;

		rcText.left = rcBitmap.right;
	}
	
	 

	if (nCheck > 0) {
		SetBkColor(dc, GetSysColor(COLOR_WINDOW));
		SetTextColor(dc, GetSysColor(COLOR_WINDOWTEXT));

		UINT nState = DFCS_BUTTONCHECK;

		if (nCheck > 1)
			nState |= DFCS_CHECKED;

		// Draw the checkmark using DrawFrameControl
		DrawFrameControl(dc, rcBitmap, DFC_BUTTON, nState);
	}

	if (lpDrawItemStruct->itemState & ODS_SELECTED) {
		SetBkColor(dc, GetSysColor(COLOR_HIGHLIGHT));
		SetTextColor(dc, GetSysColor(COLOR_HIGHLIGHTTEXT));
	}
	else {
//		SetBkColor(dc, GetSysColor(COLOR_WINDOW));     // Modified so as to not to override CClrComboBox - POB, 27 Sep 2004
//		SetTextColor(dc, GetSysColor(COLOR_WINDOWTEXT)); // This will use the CClrComboBox::OnChildNotify instead
	}

	// Erase and draw
	ExtTextOut(dc, 0, 0, ETO_OPAQUE, &rcText, 0, 0, 0);
	DrawText(dc, ' ' + strText, strText.GetLength() + 1, &rcText, DT_SINGLELINE|DT_VCENTER|DT_END_ELLIPSIS);

	if ((lpDrawItemStruct->itemState & (ODS_FOCUS|ODS_SELECTED)) == (ODS_FOCUS|ODS_SELECTED))
		DrawFocusRect(dc, &rcText);
	
}


void CCheckComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	CClientDC dc(this);
	CFont *pFont = dc.SelectObject(GetFont());

	if (pFont != 0) {

		TEXTMETRIC metrics;
		dc.GetTextMetrics(&metrics);

		lpMeasureItemStruct->itemHeight = metrics.tmHeight + metrics.tmExternalLeading;

		// An extra height of 2 looks good I think. 
		// Otherwise the list looks a bit crowded...
		lpMeasureItemStruct->itemHeight += 2;


		// This is needed since the WM_MEASUREITEM message is sent before
		// MFC hooks everything up if used in i dialog. So adjust the
		// static portion of the combo box now
		if (!m_bItemHeightSet) {
			m_bItemHeightSet = TRUE;
			SetItemHeight(-1, lpMeasureItemStruct->itemHeight);
		}

		dc.SelectObject(pFont);
	}
}


//
// Make sure the combobox window handle is updated since
// there may be many CCheckComboBox windows active
//
BOOL CCheckComboBox::OnDropDown() 
{
	m_pComboBox = this;
/*  this does absolutely nothing---------------------------------------------------------
    don't know why--
	// added 01feb12 - get a taller drop down
	// Make sure the drop rect for this combobox is at least tall enough to
	// show 2 items and a scrollbar in the dropdown list.
	int nHeight = 0;
	int nItemsToShow = min(3, GetCount());
	nItemsToShow = max(nItemsToShow, GetCount()/3);
	for (int i = 0; i < nItemsToShow; i++)
	{
		int nItemH = GetItemHeight(i);
		nHeight += nItemH;
	}

	nHeight += (4 * ::GetSystemMetrics(SM_CYBORDER));
	nHeight += (    ::GetSystemMetrics(SM_CYHSCROLL));

	// Set the height if necessary -- save current size first
	COMBOBOXINFO cmbxInfo;
	cmbxInfo.cbSize = sizeof(COMBOBOXINFO);
	if (GetComboBoxInfo(&cmbxInfo))
	{
		CRect rcListBox;
		::GetWindowRect(cmbxInfo.hwndList, &rcListBox);

		int hgt = rcListBox.Height();
		if ( hgt < nHeight)
		{
			::SetWindowPos(cmbxInfo.hwndList, 0, 0, 0, rcListBox.Width(),
			nHeight, SWP_NOMOVE | SWP_NOZORDER);
		}
	}
********** end absolutly nothing */
	return FALSE; // let mfc continue to route the message
}


//
// Selects/unselects all items in the list
//
void CCheckComboBox::SelectAll(BOOL bCheck)
{
	INT nCount = GetCount();

	for (INT i = 0; i < nCount; i++)
		SetCheck(i, bCheck);

}


//
// By adding this message handler, we may use CWnd::GetText()
//
LRESULT CCheckComboBox::OnGetText(WPARAM wParam, LPARAM lParam)
{
	// Make sure the text is updated
	RecalcText();

	if (lParam == 0)
		return 0;

	// Copy the 'fake' window text, str1, str2,maxlen
	lstrcpyn((LPTSTR)lParam, m_strText, (INT)wParam);
	return m_strText.GetLength();
}


//
// By adding this message handler, we may use CWnd::GetTextLength()
//
LRESULT CCheckComboBox::OnGetTextLength(WPARAM, LPARAM)
{
	// Make sure the text is updated
	RecalcText();
	return m_strText.GetLength();
}


//
// This routine steps thru all the items and builds
// a string containing the checked items
//
// modified to handle bit enums 12-16-02
// assumes the values in the struct list are a single bit
//
void CCheckComboBox::RecalcText()
{
	INT i;
	if (!m_bTextUpdated) {
		CString strText;
		UINT newValue = 0;
		
		// Get the list count
		INT nCount    = GetCount();

		for ( i = 0; i < nCount; i++) 
		{
			if (GetItemData(i))  // if checked...
			{
				if ( (i >= 0 &&  m_lstItems.GetUpperBound() >= 0 ) && i < m_lstItems.GetSize() )
				{
					newValue |= m_lstItems.ElementAt(i).val;	
				}
				else
				{
					TRACE(_T("    Recalc with %d,  Max is %d.\n"),i,m_lstItems.GetSize());
				}
			}
		}
		//strText.Format("0x%02X",newValue);
		strText.Format(m_strFmt,newValue);

		// Set the text
		m_strText = strText;
		m_bitValue= newValue;

		m_bTextUpdated = TRUE;
	}
}

INT CCheckComboBox::SetCheck(INT nIndex, BOOL bFlag)
{
	INT nResult = SetItemData(nIndex, bFlag);

	if (nResult < 0)
		return nResult;

	// Signal that the text need updating
	m_bTextUpdated = FALSE;

	// Redraw the window
	Invalidate(FALSE);

	return nResult;
}

BOOL CCheckComboBox::GetCheck(INT nIndex)
{
	return GetItemData(nIndex);
}
unsigned int CCheckComboBox::GetSelectedValue()
{
	RecalcText();
	return m_bitValue;
}

void CCheckComboBox::SetValue(int bitValue)
{
	m_bitValue = bitValue;

	if ( m_lstItems.GetSize() > 0 )
	{
		 for( int i = 0; i < m_lstItems.GetSize(); i++ )
		 {
			if (m_lstItems.ElementAt( i ).val & m_bitValue)
			{
				SetCheck(i, TRUE);
			}
			else
			{
				SetCheck(i, FALSE);
			}
		 }
	 }
}

void CCheckComboBox::ModifyListStrings(SUBITEMLIST_T *plstItems)
{
	int i; // PAW 03/03/09
	if (!plstItems)
	{
		return;
	}

	m_lstItems.RemoveAll();
	ResetContent();

	SUBLISTDATATYPE localStruct;
	for ( i = 0; i < plstItems->GetSize(); i++)
	{
		localStruct = plstItems->ElementAt(i);
		m_lstItems.Add(localStruct);     // copy 'em in (source list is on the creator's stack)
	}
	if ( m_lstItems.GetSize() > 0 )
	{
		 for( i = 0; i < m_lstItems.GetSize(); i++ )
		 {
			AddString( (LPCTSTR) (m_lstItems.ElementAt( i ).descS.c_str() ) );
/*			if (m_lstItems.ElementAt( i ).val & m_bitValue)
			{
				SetCheck(i, TRUE);
			}
			else
			{
				SetCheck(i, FALSE);
			}*/
		 }
		 //SetCurSel( /*m_nSel*/ );
	}

//	SetFocus();

	 // Modify width of dropdown portion
	 // Find the longest string in the combo box.
	CString str;
	CSize   sz;
	int     dx=0;
	CDC*    pDC = GetDC();
	for (i=0;i < GetCount();i++)
	{
	   GetLBText( i, str );
	   sz = pDC->GetTextExtent(str);

	   if (sz.cx > dx)
		  dx = sz.cx;
	}
	ReleaseDC(pDC);

	// Adjust the width for the vertical scroll bar and the left and right border.
	dx += ::GetSystemMetrics(SM_CXVSCROLL) + 2*::GetSystemMetrics(SM_CXEDGE);

	// Set the width of the list box so that every item is completely visible.
	SetDroppedWidth(dx);


}

void CCheckComboBox::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	GetParent()->PostMessage(WM_RBUTTONDOWN);
	CInPlaceList::OnRButtonDown(nFlags, point);
}

/*************************************************************************************************
 *
 *   $History: CheckComboBox.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
