/*************************************************************************************************
 *
 * $Workfile: comboBox.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */
 
/* //////////////////////////NOTES///////////////////////////////////////

The listing of the implementation file now follows. The CInPlaceList constructor 
simply saves the values passed through its arguments and initializes m_bESC to 
false. 
  The OnCreate() function creates the drop down list and initializes it with 
the proper values.  The overridden PreTranslateMessage() is to ascertain that the 
escape and the enter key strokes do make it to the combobox control. The escape 
key and the enter key are normally pre-translated by the CDialog or the CFormView 
object, we therefore specifically check for these and pass it on to the combobox. 

  OnKillFocus() sends of the LVN_ENDLABELEDIT notification and destroys the combobox 
control. The notification is sent to the parent of the list view control and not 
to the list view control itself. When sending the notification, the m_bESC member 
variable is used to determine whether to send a NULL string. 

  The OnNcDestroy() function is the appropriate place to destroy the C++ object. 
The OnChar() function ends the selection if the escape or the enter key is pressed. 
It does this by setting focus to the list view control which force the OnKillFocus() 
of the combobox control to be called. For any other character, the OnChar() function 
lets the base class function take care of it. 

  The OnCloseup() function is called when the user has made a selection from the 
drop down list. This function sets the input focus to its parent thus terminating 
the item selection. 
///////////////////////////////////////////////////////////////////////////// */

// InPlaceList.cpp : implementation file
//

#include "stdafx.h"
//#include "InPlaceList.h"
#include "comboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInPlaceList

IMPLEMENT_DYNAMIC( CInPlaceList, CClrComboBox )

CInPlaceList::CInPlaceList(int iItem, int iSubItem, CStringList *plstItems, int nSel, int selLen)
{
     m_iItem = iItem;
     m_iSubItem = iSubItem;

	 if (plstItems)
	 {
		m_lstItems.AddTail( plstItems );
	 }
     m_nSel = nSel;
     m_bESC = FALSE;
	 // size doesn't matter
}


CInPlaceList::~CInPlaceList()
{
}


BEGIN_MESSAGE_MAP(CInPlaceList, CComboBox)
     //{{AFX_MSG_MAP(CInPlaceList)
     ON_WM_CREATE()
     ON_WM_KILLFOCUS()
     ON_WM_CHAR()
     ON_WM_NCDESTROY()
     ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCloseup)
     //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInPlaceList message handlers

int CInPlaceList::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	TRACE(_T("InPlaceList.OnCreate Entry.\n"));
     if (CComboBox::OnCreate(lpCreateStruct) == -1)
             return -1;
     
     // Set the proper font
     CFont* font = GetParent()->GetFont();
     SetFont(font);

     for( POSITION pos = m_lstItems.GetHeadPosition(); pos != NULL; )
     {
		AddString( (LPCTSTR) (m_lstItems.GetNext( pos )) );
     }
     SetCurSel( m_nSel );
     SetFocus();
	TRACE(_T("InPlaceList.OnCreate Exit.\n"));
     return 0;
}

BOOL CInPlaceList::PreTranslateMessage(MSG* pMsg) 
{
     if( pMsg->message == WM_KEYDOWN )
     {
             if(pMsg->wParam == VK_RETURN 
                             || pMsg->wParam == VK_ESCAPE
                             )
             {
                     ::TranslateMessage(pMsg);
                     ::DispatchMessage(pMsg);
                     return TRUE;                            // DO NOT process further
             }
     }
     
     return CComboBox::PreTranslateMessage(pMsg);
}

void CInPlaceList::OnKillFocus(CWnd* pNewWnd) 
{
     CComboBox::OnKillFocus(pNewWnd);
     
     CString str;
     GetWindowText(str);

     // Send Notification to parent of ListView ctrl
     LV_DISPINFO dispinfo;
     dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
     dispinfo.hdr.idFrom = GetDlgCtrlID();
     dispinfo.hdr.code = LVN_ENDLABELEDIT;

     dispinfo.item.mask = LVIF_TEXT;
     dispinfo.item.iItem = m_iItem;
     dispinfo.item.iSubItem = m_iSubItem;
     dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
     dispinfo.item.cchTextMax = str.GetLength();
	 dispinfo.item.state = m_bESC ? m_nSel : GetCurSel()  ; //caller must lookup in disp list

     GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo );

     PostMessage( WM_CLOSE );
}

void CInPlaceList::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
     if( nChar == VK_ESCAPE || nChar == VK_RETURN)
     {
             if( nChar == VK_ESCAPE )
                     m_bESC = TRUE;
             GetParent()->SetFocus();
             return;
     }
     
     CComboBox::OnChar(nChar, nRepCnt, nFlags);
}

void CInPlaceList::OnNcDestroy() 
{
     CComboBox::OnNcDestroy();
     
 //    delete this;  // POB, 12 Oct 2004 - causes crash in CDynDialogItemEx in destructor
}

void CInPlaceList::OnCloseup() 
{
     GetParent()->SetFocus();
}


/*************************************************************************************************
 *
 *   $History: comboBox.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
