/*************************************************************************************************
 *
 * $Workfile: comboBox.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "combobox.h"
 */
 /* ////////////////////////////////////////
Step 7: Subclass the CComboBox class
We need to subclass the CComboBox class to provide for our special requirement. The main 
requirements placed on this class is that 

  It should send the LVN_ENDLABELEDIT message when the user finishes selecting an item 
  It should destroy itself when the edit is complete 
  The edit should be terminated when the user presses the Escape or the Enter key or 
    when the user selects an item or when the control loses the input focus. 

The listing of the header file precedes that of the implementation file. The CInPlaceList 
declares five private variables. These are used for initializing the drop down list and 
when the control sends the LVN_ENDLABELEDIT notification. 
///////////////////////////////////////////*/

#if !defined(_COMBOBOX_H)
#define _COMBOBOX_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In dataPropertyList.h") 
#endif

#include "chkListEdit.h"
#include "ClrComboBox.h"
#ifdef INC_DEBUG
#pragma message("    Finished Includes::dataPropertyList.h") 
#endif


/////////////////////////////////////////////////////////////////////////////
// CInPlaceList window

class CInPlaceList : public CClrComboBox
{
	DECLARE_DYNAMIC(CInPlaceList)

// Construction
public:
     CInPlaceList(int iItem, int iSubItem, CStringList   *plstItems, int nSel, int selLen);
     
// Attributes
public:

// Operations
public:

// Overrides
     // ClassWizard generated virtual function overrides
     //{{AFX_VIRTUAL(CInPlaceList)
     public:
     virtual BOOL PreTranslateMessage(MSG* pMsg);
     //}}AFX_VIRTUAL

// Implementation
public:
     virtual ~CInPlaceList();

     // Generated message map functions
protected:
     //{{AFX_MSG(CInPlaceList)
     afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
     afx_msg void OnKillFocus(CWnd* pNewWnd);
     afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
     afx_msg void OnNcDestroy();
     afx_msg void OnCloseup();
     //}}AFX_MSG

     DECLARE_MESSAGE_MAP()
private:
     int     m_iItem;
     int     m_iSubItem;
     CStringList m_lstItems;
     int     m_nSel;
     BOOL    m_bESC;                         // To indicate whether ESC key was pressed
};

#endif

/*************************************************************************************************
 *
 *   $History: comboBox.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl/EditClasses
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
