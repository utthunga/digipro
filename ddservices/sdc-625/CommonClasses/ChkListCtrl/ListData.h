/*************************************************************************************************
 *
 * $Workfile: ListData.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "ListData.h"
 */

#ifndef _LISTDATA_H
#define _LISTDATA_H
#ifdef INC_DEBUG
#pragma message("In ListData.h") 
#endif

#include "StdAfx.h"

#ifndef __AFXTEMPL_H__
#include <afxtempl.h>
#endif

#include "chkListEdit.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ListData.h") 
#endif


// As much as we would like to, we can't templateize this class
// It is used in the ChkListView which can't handle temples as inputs
//#include "InputFile.h"

#ifndef LISTDATATYPE
 #define LISTDATATYPE  RowListing
 #define LISTDATANAME "RowListing"
	class RowListing : public CObject
	{
	public:
		CString Zero;
		BOOL   Valid; //TRUE on not changed 
	};
#endif

#ifndef SUBLISTDATATYPE
typedef struct dfltSubListData_s
{
	CString elementString;
	unsigned int elementValue;// normally a bit mask || bit value
}
/*typedef*/ dfltSubListData_t;
#define SUBLISTDATATYPE  dfltSubListData_t
#endif

struct sColDesc
{
	TCHAR   ColLabel[COLSTRLEN];
	int     ColWidth;
	int     ColFmt;
};

// redefine for unique data status
typedef struct datumInfo_s
{
   unsigned short txtColor  :4;/* 0=Blk,1=Blu,2=Grn,3=Rd,4=Org,5=Maroon,6=FlGrn,7=Wht*/
   unsigned short bkgndColor:4;
   unsigned short editType  :4;/* editType_t  */
   unsigned short isEdit    :1;/* is editable */
   unsigned short isHex     :1;
   unsigned short isOK      :1;
   unsigned short isChanged :1;
}
/*typedef*/ datumInfo_t;

#define DI_TXTCOLORVALID 0x0001
#define DI_BKGCOLORVALID 0x0002
#define DI_EDITTYPEVALID 0x0004
#define DI_ISEDIT_VALID  0x0008
#define DI_IS_HEX_VALID  0x0010
#define DI_IS_OK__VALID  0x0020
#define DI_ISCHANGEVALID 0x0040


// Added By Deepak 
// Required to fetch additional info into  ExtendedDlgbox 
// Used in QueryItemText method
// If u set the nCol value to this you would receive these additional information
#define COL_ITEM_TYPE  3333
#define COL_ITEM_SIZE  3334
#define COL_VARIABLE_TYPE 3335
// END Modification

/////////////////////////////////////////////////////////////////////////////
// CListData window

class CListData : public CObject
{
// Construction
public:
	CListData();

// Attributes
public:
	//CArray<RowListing*, RowListing*> RowList;
	int NumColumns;
// #pragma message("--- CArray for " LISTDATANAME )

	CArray< LISTDATATYPE *, LISTDATATYPE * > RowList; // when LISTDATATYPE is hCVar* 
													//  it's an array of pointers to pointers
	// Added By deepak
	maskClass_t enumClassFilter;	// Acts as filter, Varaible is shown in corressponding classTab only
	bool	m_bAllClass;			// if true Variable is being shown in ALL Class tab
	bool	m_bShowLocClass;		//Added by deepak for Show Local class option
	CArray<int,int> *pViewList;		//View pointer
//END Modification

// Operations
	virtual int         PreFillListData();
	virtual CString     QueryItemText(int nRow, int nCol, BOOL getNumber = FALSE);
    virtual datumInfo_t QueryItemInfo(int nRow, int nCol);
	virtual sColDesc&   GetColDesc(int ColNum, sColDesc& nColDesc);
	virtual BOOL	    QueryValidity(int nRow);
    virtual void        SetItemValidity(int nRow, BOOL nValid);
	virtual int		    FillColumnHead(CListCtrl * p_MyCtrl);
	virtual int         SetItemValue(int nRow, int nCol, BOOL isChanged, void* pValue = NULL) // type by location
						{ return -1;}
	virtual int         QuerySubListData(int nRow, int nCol, CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&>& datArray)
						{	return 0;};

	virtual int       GetSize(void);

// Implementation
public:
	virtual ~CListData();

};

/////////////////////////////////////////////////////////////////////////////
#endif //_LISTDATA_H

/*************************************************************************************************
 *
 *   $History: ListData.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */