/*************************************************************************************************
 *
 * $Workfile: ChkListCtrl.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 implementation of the CChkListView class
 * This was a part of the Microsoft Foundation Classes C++ library.
 * Modified 
 * 03/07/99		sjv	to be a base class for may others
 * 12/04/02    sjv   convert to a control
 */

#include "stdafx.h"
#ifndef __AFXTEMPL_H__
#include <afxtempl.h>
#endif

#include <math.h>
#include "resource.h"

#include "ChkListCtrl.h"
#include "comboBox.h"
#include "CheckComboBox.h"
#include "DDlistData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChkListCtrl   


IMPLEMENT_DYNCREATE(CChkListCtrl, CListCtrl )

BEGIN_MESSAGE_MAP(CChkListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CChkListCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_NOTIFY_REFLECT(LVN_GETDISPINFO, OnGetdispinfo)

// Added by deepak
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnClick)
//Added by deepak

	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndlabeledit)
	//}}AFX_MSG_MAP
	ON_MESSAGE(LVM_SETIMAGELIST, OnSetImageList)
	ON_MESSAGE(LVM_SETTEXTCOLOR, OnSetTextColor)
	ON_MESSAGE(LVM_SETTEXTBKCOLOR, OnSetTextBkColor)
	ON_MESSAGE(LVM_SETBKCOLOR, OnSetBkColor)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChkListCtrl construction/destruction

CChkListCtrl::CChkListCtrl()
{
	TRACE(_T("-+-+-+- CChkListCtrl: Instantiation - Entry.\n"));

	m_bFullRowSel    =FALSE;
	m_bClientWidthSel=TRUE;

	m_cxClient          =0;
	m_cxStateImageOffset=0;
	m_nNextFree         =0;
	m_valueByteWidth    =0;

	m_editItem    = -1;
	m_editSubItem = -1;

	m_clrText  =::GetSysColor(COLOR_WINDOWTEXT);
	m_clrTextBk=::GetSysColor(COLOR_WINDOW);
	m_clrBkgnd =::GetSysColor(COLOR_WINDOW);
	
	p_MyListDataSet    = NULL;  //this should be set by child to reflect data
	pMyView			   = NULL;	//this should be set by child to reflect data

	m_bStateIconEnable = FALSE;		// unique to this project
	// set image list...IDB_ is an int
	//BOOL Create(     UINT nBitmapID, int cx, int nGrow, COLORREF crMask );
//120402	m_StateImageList.Create(IDB_STATEICONS,16, 1, RGB(255,0,0));
// in this scenario it is <uncked boc; chcked box; dropdownOpenArrow; dropdownSolidArrow>
	m_StateImageList.Create(IDB_BITMAP1,16, 0, RGB(255,0,0));
	m_bHaveCols = FALSE;

   //setStdCols();
   m_stdColors = NULL;
   coloredHDC  = 0;

   TRACE(_T("-+-+-+- CChkListCtrl: Instantiation - Exit.\n"));
}

//void CChkListCtrl::initialize(CListData * pNewCLD)
void CChkListCtrl::initialize(CListData * pNewCLD, CArray<int,int> *pView)
{
	//CWnd* pParent = GetParent( );
	//CWnd* pParentsParent = GetParentOwner();
	// force 'em to tell us where the data is (generic for many types of interface)
#ifdef _DEBUG
	if ( p_MyListDataSet != NULL)
	{	TRACE(_T("* * * * WARNING: changing the CListData destination!\n")); }
	if ( pNewCLD == NULL)
	{	TRACE(_T("* * * * WARNING: clearing the CListData destination!\n")); }
	if ( pView == NULL)
	{	TRACE(_T("* * * * WARNING: Received NULL view!\n")); }
#endif
	p_MyListDataSet = pNewCLD;
	pMyView= pView;
}


int MyEnumProc(LPVOID lp, LPBYTE lpb)
{  
   LPLOGPEN lopn;    
   COLORREF *aColors;
   int iColor;    

   lopn    = (LPLOGPEN)lp;    
   aColors = (COLORREF *)lpb;

   if (lopn->lopnStyle==PS_SOLID)     
   {        // Check for too many colors.
      iColor = (int)(aColors[0]);
        if (iColor <= 0)
           return 0;
        aColors[iColor] = lopn->lopnColor;
////TRACE(_T("%d) Color: %d %d %d\n"),(int)aColors[0],
////      GetRValue(lopn->lopnColor),GetGValue(lopn->lopnColor),GetBValue(lopn->lopnColor));
        aColors[0]--;    
   }
   return 1;
}


void CChkListCtrl::setStdCols(HDC hdc)
{

#ifdef FIXED_STACK_SCREWUP 
// GetTheColors - returns the count and color values of solid colors
// Returns a pointer to the array containing colors
// hdc - handle to device contextCOLORREF *GetTheColors(HDC hdc){
   if (coloredHDC != hdc )
      coloredHDC = hdc;
   else
      return ; // already done

    int cColors;        // Get the number of colors.
    cColors = GetDeviceCaps(hdc, NUMCOLORS);    // Allocate space for the array.
    if (m_stdColors != NULL)
    {
       //called in draw, will usually have existed.TRACE(_T("m_stdColors holds a value !!!!!!!!!!!!!\n"));
       delete m_stdColors;
       m_stdColors = NULL;
    }
    COLORREF* tmpColors = new COLORREF[cColors+1];
    COLORREF pPassColors[40];
	//= tmpColors;
    m_stdColors = new COLORREF[cColors+1];// *)LocalAlloc(LPTR, sizeof(COLORREF) * 
                                             //(cColors+1));    // Save the count of colors in first element.
 
   if ( m_stdColors == NULL )
   {
      AfxMessageBox("Standard Memory was not allocated.");
      return;
   }
COLORREF* hldColors = m_stdColors;
CListData * hldLstPtr = p_MyListDataSet; 
HDC       hldHDC = coloredHDC;

   tmpColors[0] = m_stdColors[0] = (COLORREF)cColors;
    // Enumerate all pens and save solid colors in the array.
pPassColors[0] = (COLORREF)cColors;
   if (hdc != NULL)   
   { 
      CDC dc;      
      dc.Attach(hdc);
      VERIFY(dc.EnumObjects(OBJ_PEN, (GOBJENUMPROC)MyEnumProc, /*(COLORREF)*/(LPARAM)(&pPassColors[0])/*tmpColors*/));   
   }


//   if (
//      EnumObjects(hdc, OBJ_PEN, (GOBJENUMPROC)MyEnumProc, /*(COLORREF)*/(LPARAM)pPassColors/*tmpColors*/)
//      == 0 )  AfxMessageBox("EnumObjects returned zero.");


   if ( m_stdColors == NULL )
   {
      AfxMessageBox("Standard Memory was deleted.");
      if (hldLstPtr != p_MyListDataSet)
      {
         AfxMessageBox("List pointer trashed too.");
      }
      if (hldHDC != coloredHDC)
      {
         AfxMessageBox("HDC trashed too.");
      }

      return;
   }
   if ( tmpColors == NULL)
   {
      AfxMessageBox("Temporary Memory was deleted.");
      return;
   } 
   if ( pPassColors == NULL)
   {
      AfxMessageBox("Passed pointer NULLED.");
      return;
   }
   if ( m_stdColors != hldColors)
   {
      AfxMessageBox("Standard pointer changed.");
      return;
   }

    // Refresh the count of colors.    
    m_stdColors[0] = (COLORREF)cColors;
    int u,y;
    for ( u = 1, y = m_stdColors[0]; y > 0; y--,u++)
    {
       m_stdColors[u] = pPassColors[y]; //tmpColors[y];
    }
    delete tmpColors;
#else /* still screwed up - do a hard coded work around */

	CDC* pDC =CDC::FromHandle(hdc);


	int cColors;        // Get the number of colors.
	if (coloredHDC != hdc )
      coloredHDC = hdc;
    else
      return ; // already done

    cColors = GetDeviceCaps(hdc, NUMCOLORS);    // Allocate space for the array.
int dColors = pDC->GetDeviceCaps(NUMCOLORS);

int dBitsInAPixel = GetDeviceCaps(hdc, PLANES) *
                    GetDeviceCaps(hdc, BITSPIXEL); 
//To determine how many colors are available, you can raise 2 to the power of dBitsInAPixel 
//	and this will return the maximum number of colors that are displayable at a given time 
//	in the specified display context (DC). 

double f = dBitsInAPixel;
double g = 2.0;
double h = pow(2.0,dBitsInAPixel);
int eColors = 0;

if ( h > 0 )
{
	eColors = (int)h;
}
    if (m_stdColors != NULL)
    {
       //called in draw, will usually have existed.TRACE(_T("m_stdColors holds a value !!!!!!!!!!!!!\n"));
       delete m_stdColors;
       m_stdColors = NULL;
    }

	if (cColors < 0)
	{
		if (eColors > 0)
		{
			cColors = eColors;
		}
		else
		{
			cColors = 20;
		}
	}
cColors = min(cColors,20);


    m_stdColors = new COLORREF[cColors+1];// *)LocalAlloc(LPTR, sizeof(COLORREF) * 
                                             //(cColors+1));    // Save the count of colors in first element.
//     0           1        2        3          4        5        6          7         8
//	00000000  00A4A0A0  00F0FBFF  00F0C8A4  00C0DCC0  00808000  00800080  00800000  00008080  
//	   9           10       11       12         13       14       15        16         17
//	00008000  00000080  00C0C0C0  0031C3C0  00808080  00FFFF00  00FF00FF  00FF0000  0000FFFF
//	  18          19       20
//	0000FF00  000000FF  00FFFFFF

	COLORREF tmpColors[40];

	if (cColors >= 0)
		tmpColors[0] = (COLORREF)cColors;;
	if (cColors >= 1)
		tmpColors[1] = 0x00A4A0A0;
	if (cColors >= 2)
		tmpColors[2] = 0x00F0FBFF;
	if (cColors >= 3)
		tmpColors[3] = 0x00F0C8A4;
	if (cColors >= 4)
		tmpColors[4] = 0x00C0DCC0;
	if (cColors >= 5)
		tmpColors[5] = 0x00808000;
	if (cColors >= 6)
		tmpColors[6] = 0x00800080;
	if (cColors >= 7)
		tmpColors[7] = 0x00800000;
	if (cColors >= 8)
		tmpColors[8] = 0x00008080;
	if (cColors >= 9)
		tmpColors[9] = 0x00008000;
	if (cColors >=10)
		tmpColors[10] = 0x00000080;
	if (cColors >= 11)
		tmpColors[11] = 0x00C0C0C0;
	if (cColors >= 12)
		tmpColors[12] = 0x00808080;
	if (cColors >= 13)
		tmpColors[13] = 0x00FFFF00;
	if (cColors >= 14)
		tmpColors[14] = 0x00FF00FF;
	if (cColors >= 15)
		tmpColors[15] = 0x00FF0000;
	if (cColors >= 16)
		tmpColors[16] = 0x0000FFFF;
	if (cColors >= 17)
		tmpColors[17] = 0x0000FF00;
	if (cColors >= 18)
		tmpColors[18] = 0x000000FF;
	if (cColors >= 19)
		tmpColors[19] = 0x00FFFFFF;
	if (cColors >= 20)
		tmpColors[20] = 0x00000000;

	m_stdColors[0] = (COLORREF)cColors;
    int u,y;
    for ( u = 1, y = min(cColors,20); y > 0; y--,u++)
    {
       m_stdColors[u] = tmpColors[y]; //tmpColors[y];
    }
	TRACE(_T("----Colors set to hardcoded values.\n"));
#endif
    return ;
}

CChkListCtrl::~CChkListCtrl()
{
//	CListCtrl &clc =  GetListCtrl( );
//	if (clc)
//		clc.DeleteAllItems( );
	if (m_hWnd)
		DeleteAllItems( );
   if (m_stdColors!=NULL)
   {   delete m_stdColors;m_stdColors=NULL;}
}
BOOL CChkListCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	TRACE(_T("-+-+-+- CChkListCtrl: PreCreateWindow - Entry.\n"));
	// default is report view and full row selection
	cs.style &= ~LVS_TYPEMASK;
	cs.style |= LVS_REPORT | LVS_OWNERDRAWFIXED;
	//m_bFullRowSel=TRUE;
	m_bFullRowSel = FALSE;

	return(CListCtrl::PreCreateWindow(cs));
}

/////////////////////////////////////////////////////////////////////////////
// currently never full row <just the items>
BOOL CChkListCtrl::SetFullRowSel(BOOL bFullRowSel) 
{
	// no painting during change
	LockWindowUpdate();

	m_bFullRowSel=bFullRowSel;

	BOOL bRet;

	if(m_bFullRowSel)
		bRet=ModifyStyle(0L,LVS_OWNERDRAWFIXED);
	else
		bRet=ModifyStyle(LVS_OWNERDRAWFIXED,0L);

	// repaint window if we are not changing view type
	if(bRet && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		Invalidate();

	// repaint changes
	UnlockWindowUpdate();

	return(bRet);
}
BOOL CChkListCtrl::GetFullRowSel()
{
	return(m_bFullRowSel);
}

/////////////////////////////////////////////////////////////////////////////
// CChkListCtrl drawing

// offsets for first and other columns
#define OFFSET_FIRST	2
#define OFFSET_OTHER	6

void CChkListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
//*	TRACE(_T("------- Draw Entry "));
//	CListCtrl& ListCtrl =GetListCtrl();
	
	p_MyListDataSet ->pViewList = this->pMyView; 

	CDC* pDC            =CDC::FromHandle(lpDrawItemStruct->hDC);
	UINT uiFlags        =ILD_TRANSPARENT;
	int nItem           =lpDrawItemStruct->itemID;
	BOOL bFocus         =(GetFocus()==this);
	COLORREF clrImage   =m_clrBkgnd;
	CImageList* pImageList;
	COLORREF clrTextSave, clrBkSave;
	CRect rcItem(lpDrawItemStruct->rcItem);
	static _TCHAR szBuff[MAX_PATH];
	LPCTSTR pszText;
// we have to fend for ourself when getting data
	CListData *  p_LD = GetListDataPtr();
	datumInfo_t subItemInfo;
	setStdCols(lpDrawItemStruct->hDC);


// get item data
	LV_ITEM lvi;
	lvi.mask              =LVIF_TEXT /*| LVIF_IMAGE*/ | LVIF_STATE ;
	lvi.iItem             =nItem;
	lvi.iSubItem          =0;
	lvi.pszText           =szBuff;// buffer to be filled...nope
	lvi.cchTextMax        =sizeof(szBuff);
	lvi.stateMask         =0xFFFFFFFF;		// get all state flags
	GetItem(&lvi);

	BOOL bSelected=(bFocus    || (GetStyle() & LVS_SHOWSELALWAYS)) && lvi.state & LVIS_SELECTED;
	bSelected     = bSelected || (lvi.state  & LVIS_DROPHILITED);

// set colors if item is selected
	CRect rcAllLabels;
	GetItemRect(nItem,rcAllLabels,LVIR_BOUNDS);
	CRect rcLabel;
/* try with no icon mods::
	ListCtrl.GetItemRect(nItem,rcLabel,LVIR_LABEL);
	rcAllLabels.left=rcLabel.left;
	if(m_bClientWidthSel && rcAllLabels.right<m_cxClient)
		rcAllLabels.right=m_cxClient;
*/
   
   // Get a handle to the font used in the list box
    CFont *pFont;
    pFont = GetFont();

    // Select the list box font into the temporary DC
    CFont *pFontOld = pDC->SelectObject(pFont);
#if 0
	clipped from a mfc src code file
		oldFont = dc.SelectObject( &SampleFont );
	crText = dc.SetTextColor(GetSysColor(COLOR_WINDOWTEXT));
	bkMode = dc.SetBkMode(TRANSPARENT);

	// Calculate the position of the text
	dc.GetTextMetrics( &tm );

	len = strSample.GetLength();
	TextExtent = dc.GetTextExtent(strSample, len);
	TextExtent.cy = tm.tmAscent - tm.tmInternalLeading;

	if ((TextExtent.cx >= (rcText.right - rcText.left)) ||
			(TextExtent.cx <= 0))
		x = rcText.left;
	else
		x = rcText.left + ((rcText.right - rcText.left) - TextExtent.cx) / 2;
#endif
	if(bSelected && m_bFullRowSel)
	{
		//clrTextSave=pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
      clrTextSave=pDC->SetTextColor(STD_BLACK);
		clrBkSave  =pDC->SetBkColor  (STD_LGTGREY/*::GetSysColor(COLOR_HIGHLIGHT)*/);

		pDC->FillRect(rcAllLabels,&CBrush(STD_LGTGREY/*::GetSysColor(COLOR_HIGHLIGHT)*/));
	}
	else
	{
			clrTextSave=pDC->SetTextColor(STD_BLACK);
			clrBkSave  =pDC->SetBkColor  (STD_WHITE);
		pDC->FillRect(rcAllLabels,&CBrush(m_clrTextBk));
	}

	if (m_bStateIconEnable)
	{
// set color and mask for the icon
		if(lvi.state & LVIS_CUT)
		{
			clrImage=m_clrBkgnd;
			uiFlags|=ILD_BLEND50;
		}
		else if(bSelected)
		{
			clrImage=::GetSysColor(COLOR_HIGHLIGHT);
			uiFlags|=ILD_BLEND50;
		}

	// draw state icon
	/* This is (state & 0x0F000) >> 12;   this gives image# 0 - 15
	 * currently bit 0x01000 ON is OK (image 1), OFF is empty box  (image 0)
	 * see state image map in resources/bitmap/IDB_STATEICONS
	*/
		UINT nStateImageMask=lvi.state & LVIS_STATEIMAGEMASK;
		int nImage=(nStateImageMask>>12);
		pImageList=GetImageList(LVSIL_STATE);
		if(pImageList)
			pImageList->Draw(pDC,nImage,CPoint(rcItem.left,rcItem.top),ILD_NORMAL/*ILD_TRANSPARENT*/);
	}

// draw item label-column Zero - next to icon
	GetItemRect(nItem,rcItem,LVIR_LABEL);
	if (m_bStateIconEnable)
	{	rcItem.right-=m_cxStateImageOffset;// offset is set at imagelist load below
	}
	else
	{	rcItem.left -=m_cxStateImageOffset;
	}

	pszText=MakeShortString(pDC,lvi.pszText/*szBuff*/,rcItem.right-rcItem.left,2*OFFSET_FIRST);

	rcLabel =rcItem;
	rcLabel.left +=OFFSET_FIRST;
	rcLabel.right-=OFFSET_FIRST;
/* draw col 0 text */
    pDC->SetTextColor(STD_BLACK);
	pDC->DrawText(pszText,-1,rcLabel,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);

// draw labels(individual strings) for extra columns
	LV_COLUMN lvc;
	lvc.mask=LVCF_FMT | LVCF_WIDTH;
	CString aStrBuff;
	if (m_bStateIconEnable)
		rcItem.right+=m_cxStateImageOffset;// put back the offset we subtracted earlier for width

	for(int nColumn=1; GetColumn(nColumn,&lvc); nColumn++)
	{
		rcItem.left  =rcItem.right;
		rcItem.right+=lvc.cx;
		//int nRetLen    =ListCtrl.GetItemText(nItem,nColumn,szBuff,sizeof(szBuff));
		aStrBuff = p_LD->QueryItemText(nItem, nColumn);
//	  aStrBuff.Format("I:%d  S:%i",lvi.iItem ,nColumn);
		subItemInfo = p_LD->QueryItemInfo(nItem, nColumn);


//stevev 13jun05       aStrBuff = AddPool(&aStrBuff);
		if(aStrBuff.GetLength() ==0) continue;

		pszText=MakeShortString(pDC,LPCTSTR(aStrBuff),rcItem.right-rcItem.left,2*OFFSET_OTHER);

		UINT nJustify=DT_LEFT;

		if(pszText==aStrBuff)
		{
			switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
			{
			case LVCFMT_RIGHT:
				nJustify=DT_RIGHT;
				break;
			case LVCFMT_CENTER:
				nJustify=DT_CENTER;
				break;
			default:
				nJustify=DT_LEFT;
				break;
			}
		}

		rcLabel=rcItem;
		rcLabel.left +=OFFSET_OTHER;
		rcLabel.right-=OFFSET_OTHER;
//temp stub      subItemInfo = p_LD->QueryItemInfo(nItem, nColumn);
//		subItemInfo.txtColor = 0;//blk
//		subItemInfo.isHex = 0;
//		subItemInfo.isEdit= 0;
//		subItemInfo.isOK  = 1;
		



      COLORREF tColor;
      //if (subItemInfo.Color != 0)  
      //   TRACE(_T("%d (%d) ---color %d\n"),nItem,nColumn,subItemInfo.Color);

      switch (subItemInfo.txtColor)
      {  case  0:  tColor = STD_BLACK; break;// 0=Blk
         case  1:  tColor = STD_BLUE;  break;// 1=Blu
         case  2:  tColor = STD_GREEN; break;// 2=Grn
         case  3:  tColor = STD_RED;   break;// 3=Rd
         case  4:  tColor = STD_BROWN; break;// 4=Brn
         case  5:  tColor = STD_MAROON;break;// 5=Maroon
         case  6:  tColor = STD_HOTGRN;break;// 6=FlGrn
         case  7:  tColor = STD_WHITE; break;// 7=Wht
         case  8:  tColor = STD_YELLOW   ;break;
         case  9:  tColor = STD_TURQUOIS ;break;
         case 10:  tColor = STD_DRKGREY  ;break;
         case 11:  tColor = STD_LGTGREY  ;break;
         case 12:  tColor = STD_GOLD     ;break;
         case 13:  tColor = STD_DRKBLU   ;break;
         case 14:  tColor = STD_DRKCYAN  ;break;
         case 15:  tColor = STD_TURQBLU  ;break;

         default: tColor = STD_BLACK; break;
      }
//tColor = m_stdColors[nColumn+7];
      pDC->SetTextColor(/*::GetSysColor(*/tColor/*)*/);
	  switch (subItemInfo.bkgndColor)
      {  case  0:  tColor = STD_BLACK; break;// 0=Blk
         case  1:  tColor = STD_BLUE;  break;// 1=Blu
         case  2:  tColor = STD_GREEN; break;// 2=Grn
         case  3:  tColor = STD_RED;   break;// 3=Rd
         case  4:  tColor = STD_BROWN; break;// 4=Brn
         case  5:  tColor = STD_MAROON;break;// 5=Maroon
         case  6:  tColor = STD_HOTGRN;break;// 6=FlGrn
         case  7:  tColor = STD_WHITE; break;// 7=Wht
         case  8:  tColor = STD_YELLOW   ;break;
         case  9:  tColor = STD_TURQUOIS ;break;
         case 10:  tColor = STD_DRKGREY  ;break;
         case 11:  tColor = STD_LGTGREY  ;break;
         case 12:  tColor = STD_GOLD     ;break;
         case 13:  tColor = STD_DRKBLU   ;break;
         case 14:  tColor = STD_DRKCYAN  ;break;
         case 15:  tColor = STD_TURQBLU  ;break;

         default: tColor = STD_WHITE; break;
      }

	  pDC->FillRect(rcItem,&CBrush(tColor));

		pDC->SetBkMode( OPAQUE );
		pDC->SetBkColor (tColor);

		pDC->DrawText(pszText,-1,rcLabel,nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER);
		pDC->SetBkMode( TRANSPARENT );
		

	}// next column

// draw focus rectangle if item has focus
	if(lvi.state & LVIS_FOCUSED && bFocus)
		//pDC->DrawFocusRect(rcAllLabels);
		pDC->DrawFocusRect(rcLabel);

// set original colors if item was selected
	if(bSelected)
	{
	    pDC->SetTextColor(clrTextSave);
		pDC->SetBkColor  (clrBkSave);
	}

//*	TRACE(_T("---- Exit\n"));
}

LPCTSTR CChkListCtrl::MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
	static const _TCHAR szThreeDots[]=_T("...");

	int nStringLen=lstrlen(lpszLong);
	
   
   
   if(nStringLen==0 )
		return(lpszLong);   
	SIZE size;
#if 1
   if (pDC->m_hAttribDC != NULL)
   {
       // NOTE: use GetTextExtentPoint32 in Win32 for better results
       //CSize size;
       //GetTextExtentPoint(pDC->GetSafeHdc(),
       //  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
       //  52, &size);


		//if ( ::GetTextExtentPoint32(/*pDC->GetSafeHdc()*/pDC->m_hAttribDC, lpszLong, nStringLen, &size) == 0 )
   if ( GetTextExtentExPoint(/*pDC->GetSafeHdc()*/pDC->m_hAttribDC, lpszLong, nStringLen,0,NULL,NULL, &size) == 0 )
      {//error
		TRACE(_T("TextExtent ------- Failure.\n"));
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();
//CSize  extSz= pDC->GetViewportExt( ) ;
//CSize  strSz= pDC->GetOutputTextExtent( lpszLong, nStringLen ) ;
TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif
		   return lpszLong;
      }
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();
CSize  extSz= pDC->GetViewportExt( ) ;
CSize  strSz= pDC->GetOutputTextExtent( lpszLong, nStringLen ) ;
TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif

   //  TRACE(_T("TextExtent Success.\n"));
   }
   if(size.cx + nOffset <=nColumnLen )
		return(lpszLong);
#else
	if(nStringLen==0 || ((pDC->GetTextExtent(lpszLong,nStringLen).cx + nOffset) <=nColumnLen) )
		return(lpszLong);
#endif

	static _TCHAR szShort[MAX_PATH];

	lstrcpy(szShort,lpszLong);
	int nAddLen=pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

	for(int i=nStringLen-1; i>0; i--)
	{
		szShort[i]=0;
		if(pDC->GetTextExtent(szShort,i).cx+nOffset+nAddLen<=nColumnLen)
			break;
	}

	lstrcat(szShort,szThreeDots);

	return(szShort);
}

void CChkListCtrl::RepaintSelectedItems()
{
//	CListCtrl& ListCtrl=GetListCtrl();
	CRect rcItem, rcLabel;
	TRACE(_T("-+-+-+- CChkListCtrl: RepaintSelectedItems - Entry.\n"));

// invalidate focused item so it can repaint properly

	int nItem=GetNextItem(-1,LVNI_FOCUSED);

	if(nItem!=-1)
	{
		GetItemRect(nItem,rcItem,LVIR_BOUNDS);
		GetItemRect(nItem,rcLabel,LVIR_LABEL);
		rcItem.left=rcLabel.left;

		InvalidateRect(rcItem,FALSE);
	}

// if selected items should not be preserved, invalidate them

	if(!(GetStyle() & LVS_SHOWSELALWAYS))
	{
		for(nItem=GetNextItem(-1,LVNI_SELECTED);
			nItem!=-1; nItem=GetNextItem(nItem,LVNI_SELECTED))
		{
			GetItemRect(nItem,rcItem,LVIR_BOUNDS);
			GetItemRect(nItem,rcLabel,LVIR_LABEL);
			rcItem.left=rcLabel.left;

			InvalidateRect(rcItem,FALSE);
		}
	}

// update changes 

	UpdateWindow();
}

/////////////////////////////////////////////////////////////////////////////
// CChkListCtrl diagnostics

#ifdef _DEBUG

void CChkListCtrl::Dump(CDumpContext& dc) const
{
	CListCtrl::Dump(dc);

	dc << "m_bFullRowSel = " << (UINT)m_bFullRowSel;
	dc << "\n";
	dc << "m_cxStateImageOffset = " << m_cxStateImageOffset;
	dc << "\n";
}

// should be overridden
//CListData * CChkListCtrl::GetDocListData()
//   { return p_MyListDataSet; }

#else
//inline CListData * CChkListCtrl::GetDocListData()
//   { return ((CAfileReaderDoc*)m_pDocument)->p_RowListDataSet; }

#endif //_DEBUG
CListData * CChkListCtrl::GetListDataPtr()
   { return p_MyListDataSet; }

/////////////////////////////////////////////////////////////////////////////
// CChkListCtrl message handlers

LRESULT CChkListCtrl::OnSetImageList(WPARAM wParam, LPARAM lParam)
{
	if((int)wParam==LVSIL_STATE)
	{
		int cx, cy;

		if(::ImageList_GetIconSize((HIMAGELIST)lParam,&cx,&cy))
			m_cxStateImageOffset=cx;
		else
			m_cxStateImageOffset=0;
	}

	return(Default());
}

LRESULT CChkListCtrl::OnSetTextColor(WPARAM wParam, LPARAM lParam)
{
	m_clrText=(COLORREF)lParam;
	return(Default());
}

LRESULT CChkListCtrl::OnSetTextBkColor(WPARAM wParam, LPARAM lParam)
{
	m_clrTextBk=(COLORREF)lParam;
	return(Default());
}

LRESULT CChkListCtrl::OnSetBkColor(WPARAM wParam, LPARAM lParam)
{
	m_clrBkgnd=(COLORREF)lParam;
	return(Default());
}

void CChkListCtrl::OnSize(UINT nType, int cx, int cy) 
{
	m_cxClient=cx;
	CListCtrl::OnSize(nType, cx, cy);
}

void CChkListCtrl::OnPaint() 
{
	TRACE(_T("-+-+-+- CChkListCtrl: OnPaint - Entry."));
	// in full row select mode, we need to extend the clipping region
	// so we can paint a selection all the way to the right
	if(m_bClientWidthSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT && GetFullRowSel())
	{
		CRect rcAllLabels;
		//GetListCtrl().
		GetItemRect(0,rcAllLabels,LVIR_BOUNDS);

		if(rcAllLabels.right<m_cxClient)
		{
			// need to call BeginPaint (in CPaintDC c-tor)
			// to get correct clipping rect
			CPaintDC dc(this);

			CRect rcClip;
			dc.GetClipBox(rcClip);

			rcClip.left=min(rcAllLabels.right-1,rcClip.left);
			rcClip.right=m_cxClient;

			InvalidateRect(rcClip,FALSE);
			// EndPaint will be called in CPaintDC d-tor
		}
	}

	CListCtrl::OnPaint();
	TRACE(_T("-+-+-+- Exit.\n"));
}

void CChkListCtrl::OnSetFocus(CWnd* pOldWnd) 
{
	CListCtrl::OnSetFocus(pOldWnd);

	// check if we are getting focus from label edit box
	if(pOldWnd!=NULL && ::IsWindow(pOldWnd->m_hWnd) && pOldWnd->GetParent()==this)
		return;

	// repaint items that should change appearance
	if(m_bFullRowSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		RepaintSelectedItems();
}

void CChkListCtrl::OnKillFocus(CWnd* pNewWnd) 
{
	CListCtrl::OnKillFocus(pNewWnd);

	// check if we are losing focus to label edit box
	if(pNewWnd!=NULL && pNewWnd->GetParent()==this)
		return;

	// repaint items that should change appearance
	if(m_bFullRowSel && (GetStyle() & LVS_TYPEMASK)==LVS_REPORT)
		RepaintSelectedItems();
}

void CChkListCtrl::OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	CListData  * p_LD      = p_MyListDataSet/*GetDocListData()*/;
	
    if (pDispInfo->item.mask & LVIF_TEXT)
    {
        int DataIndex = (int) pDispInfo->item.lParam;

		CString strField;
		strField = p_LD->QueryItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem);
//		strField.Format("I:%d  S:%i",pDispInfo->item.iItem,pDispInfo->item.iSubItem);

        LPTSTR pstrBuffer = AddPool(&strField); 

        pDispInfo->item.pszText = pstrBuffer;
    } 
	*pResult = 0;
}

///////////////////////////////////////////////////////////////////////////////////////
//    Added By Deepak
//   This function is called to sort the list ctrl items
///////////////////////////////////////////////////////////////////////////////////////
int CChkListCtrl::SortFunc(LPARAM lParamSort/* Column Number*/)
{
    int nResult=0;
	
	CListData *pLD =NULL;
	CArray <int,int> *pVw =NULL;

	pLD = GetListDataPtr();
	pVw =pMyView;
	
//_T("Symbol Name"), _T("Symbol-Number"),   _T("Label"),  _T("Class"),  _T("Value"),   _T("Other")
	
	CString str1;
	CString str2;

	hCattrLabel* pLabel1 =NULL; 
	hCattrLabel* pLabel2 =NULL; 

	hCattrClass* paC1 =NULL;
	hCattrClass* paC2 =NULL;

	hCbitString* pClassBitStr1 = NULL;
	hCbitString* pClassBitStr2 = NULL;


	hCEnum* pE1 =NULL;
	hCEnum* pE2 =NULL;
	hCBitEnum* pBE1 =NULL; 
	hCBitEnum* pBE2 =NULL; 

	string t;
// We need to wait till sorting is Over	
	BeginWaitCursor();

	// Bubble sort
	// max limit val is  no of elements-1
	int nMaxLimit = pVw->GetUpperBound();
	for (int i=0; i < (nMaxLimit); i++) 
		for (int j=0; j < (nMaxLimit-i); j++)
		{  
			/* compare the two neighbors */
			hCVar* pFirstItem = *(pLD ->RowList.ElementAt(pVw ->ElementAt (j))); 
			hCVar* pSecItem = *(pLD ->RowList.ElementAt (pVw ->ElementAt (j+1))); 

			switch (lParamSort) {

			case 0: // Symbol Name
					str1 = pFirstItem->getName ().data();
					str2 = pSecItem->getName ().data ();
					nResult = str1 .CompareNoCase (str2);
					break;

			case 1: // Symbol-Number
					str1.Format(_T("%#010X"), pFirstItem->getID ());
					str2.Format(_T("%#010X"), pSecItem->getID ());	
					nResult = str1 .CompareNoCase (str2);
					break;

			case 2: // Label
					pLabel1 = pFirstItem->getLabelAttr();  // pIm->getaAttr(varAttrLabel);
					pLabel2 = pSecItem->getLabelAttr ();
					
					str1 ="No Label";
					str2 ="No Label";

					if (pLabel1 != NULL)
					{	
						wstring s;
						if ( pLabel1->getString(s) != SUCCESS ) 
						{
							str1 = "Unresolved Conditional Label";
						}
						else
						{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
							str1 = s.c_str();
#else
							int y = s.size();
							UnicodeToASCII(s.c_str(), str1.GetBuffer(y+1), y+1);
#endif
						}
					}
					
					if (pLabel2 != NULL)
					{	
						wstring s;
						if ( pLabel2->getString(s) != SUCCESS ) 
						{
							str2 = "Unresolved Conditional Label";
						}
						else
						{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
							str2 = s.c_str();
#else
							int y = s.size();
							UnicodeToASCII(s.c_str(), str2.GetBuffer(y+1), y+1);
#endif
						}
					}

					nResult = str1 .CompareNoCase (str2);

					break;
			
			case 3:	
					//Class
					paC1 = pFirstItem->getClassAttr();
					paC2 = pSecItem->getClassAttr();
					
					str1 =" No Class";
					str2 = "No Class";

					if (paC1 != NULL)
					{ 
						pClassBitStr1 = (hCbitString*)(paC1->procure());
						if (pClassBitStr1 == NULL)
						{
							str1 = "Unresolved Conditional";
						}
						else
						{
							str1= paC1->translateBstring(pClassBitStr1->getBitStr()).data();
						}
					}
					if (paC2 != NULL)
					{ 
						pClassBitStr2 = (hCbitString*)(paC2->procure());
						if (pClassBitStr2== NULL)
						{
							str2 = "Unresolved Conditional";
						}
						else
						{
							str2= paC2->translateBstring(pClassBitStr2->getBitStr()).data();
						}
					}
					nResult = str1 .CompareNoCase (str2);
					break;
			
			case 4:	//Value
						str1 ="No Value";
						str2 ="No Value";
						if ( pFirstItem->VariableType() == vT_Enumerated )
						{
							pE1 = (hCEnum*)pFirstItem;
							pE1->procureString(pE1->getDispValue(),t) ; // error string if not found
							str1 = t.c_str();
						}
						else
							if(pFirstItem->VariableType() == vT_BitEnumerated)
							{
								pBE1 = (hCBitEnum*)pFirstItem;
								pBE1->procureString(pBE1->getDispValue(),t) ; // error string if not found
								str1 = t.c_str();
							}
							else
							{
								str1 = pFirstItem->getDispValueString().c_str();
							}
						

						if ( pSecItem->VariableType() == vT_Enumerated )
						{
							pE2 = (hCEnum*)pSecItem;
							pE2->procureString(pE2->getDispValue(),t) ; // error string if not found
							str2 = t.c_str();
						}
						else
							if(pSecItem->VariableType() == vT_BitEnumerated)
							{
								pBE2 = (hCBitEnum*)pSecItem;
								pBE2->procureString(pBE2->getDispValue(),t) ; // error string if not found
								str2 = t.c_str();
							}
							else
							{
								str2 = pSecItem->getDispValueString().c_str();
							}

						nResult = str1 .CompareNoCase (str2);
						break;
						
			default:
						nResult=0;
					//break;	
			};
			
		/* swap a[j] and a[j+1]      */
		switch(m_bSortDirection)
		{
		case true:	// Ascending order
					if(nResult > 0)
					{
						int tmp = pVw ->ElementAt (j);
						pVw ->SetAt(j,pVw ->ElementAt (j+1));
						pVw ->SetAt(j+1,tmp);
					}
					break;

		case false:	// Descending order
					if(nResult < 0)
					{
						int tmp = pVw ->ElementAt (j);
						pVw ->SetAt(j,pVw ->ElementAt (j+1));
						pVw ->SetAt(j+1,tmp);
					}
					break;
		};
	}

// At last sorting is complete
	EndWaitCursor();

    return nResult;
}
void CChkListCtrl::OnColumnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*) pNMHDR;

// Added By Deepak
// Set the List Data view to current view
	p_MyListDataSet ->pViewList = this->pMyView; 
// Every column click would change the direction of sorting
	m_bSortDirection = !m_bSortDirection;
// Call the sort func with column number which we are looking for sorting
	SortFunc(pNMListView->iSubItem);
//Call redraw to reflect the change in rows 
	RedrawItems(0,pMyView->GetUpperBound ());
	// SortItems (CompareFunc, pNMListView->iSubItem);
    *pResult = 0;
}

LPTSTR CChkListCtrl::AddPool(CString* pstr)
{

    LPTSTR pstrRetVal;
    int nOldest = m_nNextFree;

    m_strCPool[m_nNextFree] = *pstr;
    pstrRetVal = m_strCPool[m_nNextFree].LockBuffer();
    m_pstrPool[m_nNextFree++] = pstrRetVal;
    m_strCPool[nOldest].ReleaseBuffer();

    if (m_nNextFree == 3)
            m_nNextFree = 0;
    return pstrRetVal;
}





void CChkListCtrl::OnInitialUpdate() 
{
	TRACE(_T("-+-+-+- CChkListCtrl: OnInitialUpdate - Entry.\n"));
//	CListCtrl::
//don't call self!!!! 	OnInitialUpdate();
	
//	CListCtrl& ListCtrl=GetListCtrl();

	CListData * p_ListData = GetListDataPtr();// fetch our data class
	
//	int NumRows = p_ListData->GetSize();
//	int NumCols = p_ListData->NumColumns;

//	sColDesc ThisCol;

	SetImageList(&m_StateImageList,LVSIL_STATE);
    // kills owner draw... SetFullRowSel(m_bFullRowSel);

//	p_ListData->FillColumnHead(ListCtrl);
//	int i;//, j;

/*/ insert columns from our data class

	LV_COLUMN lvc;

	lvc.mask=LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	//for(i=0; i<NumCols; i++)
	//	ListCtrl.DeleteColumn( i );

	if ( !m_bHaveCols)
	{
		for(i=0; i<NumCols; i++)
		{
			p_ListData->GetColDesc(i, ThisCol);
			lvc.iSubItem=i;
			lvc.pszText=ThisCol.ColLabel;
			lvc.cx     =ThisCol.ColWidth;
			lvc.fmt    =ThisCol.ColFmt;
			ListCtrl.InsertColumn(i,&lvc);
		}
		m_bHaveCols = TRUE;
	}
// insert items

	LV_ITEM lvi;

	for(i=0; i<NumRows; i++)
	{
		lvi.mask     =LVIF_TEXT / *| LVIF_IMAGE* / | LVIF_STATE | LVIF_PARAM;
		lvi.iItem    =i;
		lvi.iSubItem =0;
		lvi.pszText  =LPSTR_TEXTCALLBACK;
		lvi.iImage   =i;
		lvi.stateMask=LVIS_STATEIMAGEMASK;
		lvi.state    = (p_ListData->QueryValidity(i)) 
						? INDEXTOSTATEIMAGEMASK(1) : 0x0000;// ok, 0 is boxed
		lvi.lParam   = (LPARAM) i;// index of data == row in this view
		ListCtrl.InsertItem(&lvi);
	}
// Do NOT set item text for additional columns
		//	for(i=0; i<NUM_ITEMS; i++) for(j=1; j<NUM_COLUMNS; j++)
		//		ListCtrl.SetItemText(i,j,_gszItem[i][j]);
*/
	TRACE(_T("-+-+-+- CChkListCtrl: OnInitialUpdate - Exit.\n"));

}

void CChkListCtrl::CheckItem(int nNewCheckedItem)
{
//	CListCtrl& ListCtrl=GetListCtrl();
	CListData  * p_LD  = GetListDataPtr();
	BOOL wasChecked    = FALSE;
	
// OK if bit set - NOT (empty box) if clr

	if (GetItemState(nNewCheckedItem,LVIS_STATEIMAGEMASK) 
		                                              & INDEXTOSTATEIMAGEMASK(1))
	{
		wasChecked = TRUE;
	}
// we need to verify that these toggle the main set correctly!!!!!!!!!!!!!!!!!!!!!
	if (wasChecked)
	{// this view ALWAYS shows the state icons!
		
		// turns off the check mark  eg toX
		SetItemState(nNewCheckedItem/*m_nCheckedItem*/,
				INDEXTOSTATEIMAGEMASK(0),LVIS_STATEIMAGEMASK);
		p_LD->SetItemValidity(nNewCheckedItem, FALSE );
	}

// check new item 
	if ( !wasChecked && nNewCheckedItem !=-1 )
	{// this view ALWAYS shows the state icons!
		SetItemState(nNewCheckedItem,
				INDEXTOSTATEIMAGEMASK(1),LVIS_STATEIMAGEMASK);
		p_LD->SetItemValidity(nNewCheckedItem, TRUE );		
	}
}

/////////////////////////////////////////////////////////////////////////////
//
//   ShowExtended Dialog Implementation
// Added by Deepak
/////////////////////////////////////////////////////////////////////////////
int CChkListCtrl::ShowExtendedDlg (int nRow, int nColumn)
{
	int nResult=0;
	CExtendedInfoDlg MyExtendedInfoDlg;

	CListData *pLD= NULL;
	
	pLD = GetListDataPtr();
	
	pLD->pViewList = this->pMyView;

	if(pLD)
	{
		MyExtendedInfoDlg.m_ItemName = pLD->QueryItemText (nRow,0); // Get the Item Name
		MyExtendedInfoDlg.m_ItemID =pLD->QueryItemText (nRow,1); // Get the Item ID
		MyExtendedInfoDlg.m_ItemSize = pLD->QueryItemText (nRow,COL_ITEM_SIZE); // Get the Item ItemSize
		MyExtendedInfoDlg.m_ItemType = pLD->QueryItemText (nRow,COL_ITEM_TYPE); // Get Item Type
		MyExtendedInfoDlg.m_VariableType = pLD->QueryItemText (nRow,COL_VARIABLE_TYPE);// Get Variable Type
		
		if(MyExtendedInfoDlg.DoModal () ==IDOK)
		{	
			//For future Use	
		}
		
	}
	
	return nResult;
}

/////////////////////////////////////////////////////////////////////////////
// CButton/key message handlers

void CChkListCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{// one one click
 // one followed by ON_DBLCLK at double click
	UINT uFlags=0;
	LVHITTESTINFO stLVHITTESTINFO;

	int nHitItem=HitTest(point,&uFlags);
// Added By Deepak
// We need to identify the column which was clicked 	
	int nHitItem1=0;
	stLVHITTESTINFO.pt = point;
 	nHitItem1 = SubItemHitTest(&stLVHITTESTINFO);
// END modification
	
	// we need additional checking in owner-draw mode
	// because we only get LVHT_ONITEM
	BOOL bHit=FALSE;
	if(uFlags==LVHT_ONITEM && (GetStyle() & LVS_OWNERDRAWFIXED))
	{
		CRect rect;
		GetItemRect(nHitItem,rect,LVIR_ICON);

		// check if hit was on a state icon
		if(point.x<rect.left)
			bHit=TRUE;

	// Added By Deepak
	// Check if the Extened Info Column is clicked if so Show the extended information
	// If Click Here is clicked or 5 th column is clicked
		if(stLVHITTESTINFO.iSubItem == 5)	
			ShowExtendedDlg(stLVHITTESTINFO.iItem ,stLVHITTESTINFO.iSubItem);
	// END modification
	}
	else if(uFlags & LVHT_ONITEMSTATEICON)
		bHit=TRUE;

	if(bHit)
		CheckItem(nHitItem);
	else		
		CListCtrl::OnLButtonDown(nFlags, point);



#if 0
	///////////////////////////////
     int index;//, subRows;
     CListCtrl::OnLButtonDown(nFlags, point);

	 CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&> subDataList;

     int colnum;
//     if( ( index = HitTestEx( point, &colnum )) != -1 ) // gives pt & column
//     {
         UINT flag = LVIS_FOCUSED;
         if( (GetItemState( index, flag ) & flag) == flag )
         {
             // Add check for LVS_EDITLABELS
             //if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
             if( GetExStyle() & LVS_EDITLABELS )
             {
               //  CStringList lstItems;
			//	 CListData  * p_LD = GetListDataPtr();
			//	 subRows    = p_LD->QuerySubListData(index,colnum,subDataList);
			//	 for ( int r = 0; r < subRows; r++)
			//	 {
			//		 lstItems.AddTail( subDataList[r].elementString );
			//	 }
            //     ShowInPlaceList( index, colnum, lstItems, 2 );
             }
         }
         else
             SetItemState( index, LVIS_SELECTED | LVIS_FOCUSED , 
                             LVIS_SELECTED | LVIS_FOCUSED);
//     }
	///////////////////////////////
#endif
}


/*
{     POINT pt;     
      UINT flags;     
      int iItem;     
      int iSubItem;
}
lvhti.flags::
LVHT_ABOVE           The position is above the control's client area. 
LVHT_BELOW           The position is below the control's client area. 
LVHT_NOWHERE         The position is inside the list view control's client window, but it is not over a list item. 
LVHT_ONITEMICON      The position is over a list view item's icon. 
LVHT_ONITEMLABEL     The position is over a list view item's text. 
LVHT_ONITEMSTATEICON The position is over the state image of a list view item. 
LVHT_TOLEFT          The position is to the left of the list view control's client area. 
LVHT_TORIGHT         The position is to the right of the list view control's client area. 
*/
void CChkListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
   /* nFlags:
   MK_CONTROL   Set if the CTRL key is down.
   MK_LBUTTON   Set if the left mouse button is down.
   MK_MBUTTON   Set if the middle mouse button is down.
   MK_RBUTTON   Set if the right mouse button is down.
   MK_SHIFT     Set if the SHIFT key is down. 
   */
	
   LVHITTESTINFO lvhti;
   lvhti.pt = point;
   SubItemHitTest(&lvhti);

   int subRows;
   //CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&> subDataList;
   // now held in the class ::>>> SUBITEMLIST_T subDataList;

   if (lvhti.flags & LVHT_ONITEMLABEL)
   {
      //pmyListCtrl->SetItemText(lvhti.iItem, lvhti.iSubItem, NULL);
      // edit this guy
      //TRACE(_T("DblClk: item %d, subi %d\n"),lvhti.iItem,lvhti.iSubItem);
      //datumInfo_t iInfo = GetListDataPtr()->QueryItemInfo(lvhti.iItem,lvhti.iSubItem);
	  datumInfo_t	varInfo = ((CDDlistData*)GetListDataPtr())->QueryItemInfo(lvhti.iItem,lvhti.iSubItem);
      if (varInfo.isEdit)
      {
//         CEdit * localEdit = EditSubItem(lvhti.iItem,lvhti.iSubItem);
		    CStringList lstItems;
			m_editItem    = lvhti.iItem;
			m_editSubItem = lvhti.iSubItem;			
			
			editType_t  varEditType = (editType_t)varInfo.editType;
			switch(varEditType)
			{
			case et_enumEdit:		// 0x07
				{
					ulong curVal = 0;
					int   curSel = -1;
					subRows    = ((CDDlistData*)GetListDataPtr())->QuerySubListData(lvhti.iItem,lvhti.iSubItem, subDataList, curVal, m_valueByteWidth);			

					for (int i=0;  i < subDataList.GetSize(); i++)
					{
					   if (subDataList.ElementAt(i).val == curVal)
					   {
						   curSel = i;
						   break; // out of for loop
					   }
					}
					
					oldEnum = newEnum;
					newEnum = curSel;

					ShowInPlaceList( lvhti.iItem, lvhti.iSubItem, subDataList, curSel, m_valueByteWidth );
				}
				break;
			case et_bitEnumEdit:	// 0x08
				{
					ulong curVal = 0;
					subRows    = ((CDDlistData*)GetListDataPtr())->QuerySubListData(lvhti.iItem,lvhti.iSubItem, subDataList, curVal, m_valueByteWidth);	
					
					oldEnum = newEnum;
					newEnum = curVal;
					
					ShowInPlaceCheckList( lvhti.iItem, lvhti.iSubItem, subDataList, curVal/*current value*/, m_valueByteWidth );
				}
				break;
			case et_asciiEdit:		// 0x00 - default
			case et_floatEdit:
			case et_intEdit:
			case et_hexIntEdit:
			case et_pkdAsciiEdit:	// 0x04
			case et_passwrdEdit:	// 0x05	
			case et_dateEdit:
			case et_booleanEdit:
				{
					EditSubItem(lvhti.iItem,lvhti.iSubItem);// need to add varEditType
				}
				break;
			case et_Reserved0A:
			case et_Reserved0B:
			case et_Reserved0C:
			case et_Reserved0D:
			case et_Reserved0E:
			case et_Reserved0F:
			default:
				{
					// nothing - return
				}
				break;
			}//endswitch
      }
   }

//	CChkListView::OnLButtonDblClk(nFlags, point);
}


void CChkListCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{	
    // Up and down are in the OnKeyDown so that the user can hold down the arrow
    // keys to scroll through the entries.
    BOOL Control = (GetKeyState (VK_CONTROL) < 0);
    Control |= GetKeyState (VK_SHIFT) < 0;

    BOOL d = FALSE;

    if ( ! Control )
    {
       switch (nChar)
       {
		   case VK_DELETE :
		   {
//            d = DeleteRow();
		   }break;
		   case VK_INSERT :
		   {
//           d = InsertRow();
		   }break;
       }
    }
    if ( d ) // we did something to the screen
    { 
      // Invalidate window so entire client area 
      // is redrawn when UpdateWindow is called.   
       Invalidate();   
      // Update Window to cause View to redraw.   
       UpdateWindow();

       return;
    }
    //else we did nothing...handle default
	
	CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}



// The returned pointer should not be saved - for reference only - an edit type
void*  CChkListCtrl::EditSubItem (int Item, int subItm, datumInfo_t* pInfo)
{
    // Make sure that nCol is valid
    CHeaderCtrl* pHeader = GetHeaderCtrl();
    int nColumnCount = pHeader->GetItemCount();
    int nColumnWidth = GetColumnWidth (subItm);
    if (subItm >= nColumnCount || nColumnWidth < 5)
    {
       TRACE(_T("--subItem(%d) is larger than column count OR Width too small.\n"),subItm);

		 return NULL;
    }

	datumInfo_t vInfo;
	if (pInfo == NULL)
	{
		vInfo = ((CDDlistData*)GetListDataPtr())->QueryItemInfo(Item,subItm);
		pInfo = &vInfo;
	}

    CRect Rect,r2;

    //!!! GetSubItemRect() WORKS DIFFERENTLY WHEN IN COLUMN ZERO
    //   BOUNDS gets the entire item rect, 
    //   ICON gets the icon rect and 
    //   LABEL gets the non-icon string rect

    if(! GetSubItemRect( Item, subItm, LVIR_BOUNDS, Rect ))
    {
       TRACE(_T(" Failure to get SubItemRect(%d, %d ...) <(%d,%d) (%d,%d)>\n"),Item,subItm,
          Rect.left,Rect.top,Rect.right,Rect.bottom);
       return NULL;
    }
    else
    {
       if ( subItm == 0 )
       {
          //TRACE(_T(" BOUNDS %d - %d  \n"), Rect.left,Rect.right);
          Rect.right = Rect.left + nColumnWidth;
       }
    }

    // Now scroll if we need to expose the column
    CRect ClientRect;
    GetClientRect (&ClientRect);

    r2 = Rect;
    if (Rect.left < ClientRect.left || Rect.right > ClientRect.right )
    {
		CSize Size;
      if (Rect.left < ClientRect.left)
      {
         Size.cx = (Rect.left - ClientRect.left);//neg
         r2.left = ClientRect.left;
         r2.right= r2.left + (Rect.right - Rect.left);
      }
		else
      {
         Size.cx = Rect.right - ClientRect.right;//pos
         //r2.left = Rect.left + Size.cx;
         //r2.right= Rect.right + Size.cx;
      }
		Size.cy = 0;
// we really need to unpaint the current box befor scrolling to the new onw
      RedrawItems( Item, Item );
	  Scroll (Size);
		//Rect.left -= Size.cx;
      //GetListCtrl().GetSubItemRect( Item, subItm, LVIR_LABEL, Rect ) ;
    }
   // do it again
    if(! GetSubItemRect( Item, subItm, LVIR_BOUNDS, Rect ))
    {
       TRACE(_T(" Failure to get SubItemRect(%d, %d ...) <(%d,%d) (%d,%d)>\n"),Item,subItm,
          Rect.left,Rect.top,Rect.right,Rect.bottom);
       return NULL;
    }
    else
    {
       if ( subItm == 0 )
       {
          TRACE(_T(" BOUNDS %d - %d  \n"), Rect.left,Rect.right);
          Rect.right = Rect.left + nColumnWidth;
       }
    }

//GetListCtrl().Update(Item);

    // Get Column alignment
    LV_COLUMN lvCol;
    lvCol.mask = LVCF_FMT;
    GetColumn (subItm, &lvCol);
    DWORD dwStyle;
    if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
    else if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
    else dwStyle = ES_CENTER;

//    Rect.left += Offset+4;
//    Rect.right = Rect.left + GetColumnWidth (subItm) - 3;
    if (Rect.right > ClientRect.right)
		Rect.right = ClientRect.right;

	editType_t edTypOfCell = 
		((editType_t)GetListDataPtr()->QueryItemInfo(Item, subItm).editType);

    dwStyle |= WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL;

	if (edTypOfCell == et_passwrdEdit)
	{	// Turn on the password mode.
		dwStyle |= ES_PASSWORD;
	}
	if (edTypOfCell == et_pkdAsciiEdit)
	{	// Turn on the password mode.
		dwStyle |= ES_UPPERCASE;
	}

	hCVar* pVarItem  =  ((CDDlistData*)GetListDataPtr())->getRowsVar( Item );
	CString editString;
	wstring wstr; 

	if (pVarItem)
	{
		if (pVarItem->IsNumeric())
		{
			wstr = ((hCNumeric*)pVarItem)->getEditValueString();
		}
		else
		{
			wstr = pVarItem->getEditValueString();// is going to hCVar not integer
		}
		editString = wstr.c_str();
	}
	else
	{
		editString = ((CDDlistData*)GetListDataPtr())->QueryItemText(Item, subItm, TRUE);
	}
	// stevev 17jun09, the new box we are going to edit is processed here before the other box is 
	//					exited thru OnEndlabeledit().
	oldString = newString;
	newString = editString;
    CEdit *pEdit = NULL;

    //aStrBuff = p_LD->QueryItemText(nItem, nColumn);
    pEdit = new CEditCell (this, Item, subItm, editString);

    pEdit->Create (dwStyle, Rect, this, IDC_EDITCELL);

	// repaint item (exiting editcell may have changed somthing)
    RedrawItems( Item, Item );//////////// we need to do the previous item

    return pEdit;
}

/********************************************/
// ShowInPlaceList              - Creates an in-place drop down list for any 
//                              - cell in the list view control
// Returns                      - A temporary pointer to the combo-box control
// nItem                        - The row index of the cell
// nCol                         - The column index of the cell
// lstItems                     - A list of strings to populate the control with
// nSel                         - Index of the initial selection in the drop down list
CComboBox* CChkListCtrl::ShowInPlaceList( int nItem, int nCol, 
                                     SUBITEMLIST_T &lstItems, int nSel, int nLen )
{
	int r, rowCnt;
	rowCnt = lstItems.GetSize();
	CStringList lstOitems;

	for ( r = 0; r < rowCnt; r++)
	{
		lstOitems.AddTail( lstItems.ElementAt(r).descS.c_str() );
	}
	return ( ShowInPlaceList(nItem, nCol, lstOitems, nSel, nLen) );
}

CComboBox* CChkListCtrl::ShowInPlaceList( int nItem, int nCol, 
                                     CStringList &lstItems, int nSel, int nLen )// nSel is zero based index of value
{
     // The returned pointer should not be saved

     // Make sure that the item is visible
     if( !EnsureVisible( nItem, TRUE ) ) return NULL;

     // Make sure that nCol is valid 
     CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
     int nColumnCount = pHeader->GetItemCount();
     if( nCol >= nColumnCount || GetColumnWidth(nCol) < 10 ) 
             return NULL;

     // Get the column offset
     int offset = 0;
     for( int i = 0; i < nCol; i++ )
             offset += GetColumnWidth( i );

     CRect rect;
     GetItemRect( nItem, &rect, LVIR_BOUNDS );

     // Now scroll if we need to expose the column
     CRect rcClient;
     GetClientRect( &rcClient );
     if( offset + rect.left < 0 || offset + rect.left > rcClient.right )
     {
             CSize size;
             size.cx = offset + rect.left;
             size.cy = 0;
             Scroll( size );
             rect.left -= size.cx;
     }

     rect.left   += offset ;      // +4;
     rect.right   = rect.left + GetColumnWidth( nCol ) + 2;// - 3 ;
     int height   = rect.bottom-rect.top   - 4 ;
     rect.bottom += 5*height;
     if( rect.right > rcClient.right) rect.right = rcClient.right;

     DWORD dwStyle = WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL	/*|WS_HSCROLL*/
                                     |CBS_DROPDOWNLIST|CBS_DISABLENOSCROLL;
     CComboBox *pList = new CInPlaceList(nItem, nCol, &lstItems, nSel, nLen);
     pList->Create( dwStyle, rect, this, 1/*IDC_IPEDIT*/ );
     pList->SetItemHeight( -1, height);
     pList->SetHorizontalExtent( GetColumnWidth( nCol ));


     return pList;
}

CComboBox* CChkListCtrl::ShowInPlaceCheckList( int nItem, int nCol, 
                                     SUBITEMLIST_T &lstItems, int nSel, int nLen )
{// The returned pointer should not be saved

     // Make sure that the item is visible
     if( !EnsureVisible( nItem, TRUE ) ) return NULL;

     // Make sure that nCol is valid 
     CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
     int nColumnCount = pHeader->GetItemCount();
     if( nCol >= nColumnCount || GetColumnWidth(nCol) < 10 ) 
             return NULL;

     // Get the column offset
     int offset = 0;
     for( int i = 0; i < nCol; i++ )
             offset += GetColumnWidth( i );

     CRect rect;
     GetItemRect( nItem, &rect, LVIR_BOUNDS );

     // Now scroll if we need to expose the column
     CRect rcClient;
     GetClientRect( &rcClient );
     if( offset + rect.left < 0 || offset + rect.left > rcClient.right )
     {
             CSize size;
             size.cx = offset + rect.left;
             size.cy = 0;
             Scroll( size );
             rect.left -= size.cx;
     }

     rect.left   += offset ;      // +4;
     rect.right   = rect.left + GetColumnWidth( nCol ) + 2;// - 3 ;
     int height   = rect.bottom-rect.top   - 4 ;
     rect.bottom += 5*height;
     if( rect.right > rcClient.right) rect.right = rcClient.right;

     DWORD dwStyle = WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL	/*|WS_HSCROLL*/
                                     |CBS_DROPDOWNLIST|CBS_DISABLENOSCROLL;
	 dwStyle |= (CBS_OWNERDRAWVARIABLE | CBS_HASSTRINGS);
     CCheckComboBox *pList = new CCheckComboBox(nItem, nCol, &lstItems, nSel, nLen);
     pList->Create( dwStyle, rect, this, 1/*IDC_IPEDIT*/ );
     pList->SetItemHeight( -1, height);
     pList->SetHorizontalExtent( GetColumnWidth( nCol ) );

     return pList;
}

void CChkListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
     if( GetFocus() != this ) SetFocus();
     CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CChkListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
     if( GetFocus() != this ) SetFocus();
     CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CChkListCtrl::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult)
{
     LV_DISPINFO  *plvDispInfo = (LV_DISPINFO *)pNMHDR;
     LV_ITEM          *plvItem = &plvDispInfo->item;
// checkbox:
	 // mask     = LVIF_TEXT
	 // iItem    = item #
	 // iSubItem = subitem#
	 // pszText  = the display text
	 // cchTextMax= length thereof
	 // state    = bit value
// enum box
	 // mask     = LVIF_TEXT
	 // iItem    = item #
	 // iSubItem = subitem#
	 // pszText  = the display text - NULL if an escape
	 // cchTextMax= length thereof
	 // state    = zero-based index into list of sel, passed-in value if esc
	 //            we have to look it up here

	 // determine the type
	 if (plvItem->iItem == m_editItem && plvItem->iSubItem == m_editSubItem)
	 {
		datumInfo_t	varInfo     = ((CDDlistData*)GetListDataPtr())->QueryItemInfo(m_editItem,m_editSubItem);
		editType_t  varEditType = (editType_t)varInfo.editType;
		BOOL newValue;
		switch(varEditType)
		{
		case et_enumEdit:		// 0x07
			{	// send the index - index 2 value lookup
				UINT32 y = 0;
				if ( plvItem->state < (unsigned)(subDataList.GetSize()) )
				{
					y = subDataList[plvItem->state].val;
				}
				
				newValue = (oldEnum != y);
				// else leave it zero
				((CDDlistData*)GetListDataPtr())->SetItemValue
								(m_editItem, m_editSubItem, newValue, &(y));// type by location
				subDataList.RemoveAll(); // we're done with it
			}
			break;
		case et_bitEnumEdit:	// 0x08
			{
				UINT32 y = plvItem->state;
				newValue = (oldEnum != y);
				((CDDlistData*)GetListDataPtr())->SetItemValue(m_editItem, m_editSubItem, newValue, &(y));
			}
			break;
		case et_asciiEdit:		// 0x00 - default
		case et_pkdAsciiEdit:	// 0x04
		case et_passwrdEdit:	// 0x05	
		case et_dateEdit:
			/* deepak's changes discard fields with spaces, commas or '\' - 
			   stevev - make the discard for numeric's only (these chars should be killed on entry) */
			{	
				tstring strTemp;
				strTemp =plvItem->pszText;
				CString t; t = strTemp.c_str();

				newValue = (oldString != t);
				((CDDlistData*)GetListDataPtr())->SetItemValue(m_editItem, m_editSubItem, newValue, plvItem->pszText );
			}
			break;
		case et_floatEdit:
		case et_intEdit:
		case et_hexIntEdit:
		case et_booleanEdit:
			{
				// Modified by Deepak
				tstring strTemp;
				strTemp =plvItem->pszText;
				CString t; t = strTemp.c_str();

				int nPos =-1;
				nPos = strTemp.find_first_of (_T(", \\"));

				if(-1 ==nPos)
				{
					newValue = (oldString != t);
					((CDDlistData*)GetListDataPtr())->SetItemValue(m_editItem, m_editSubItem, newValue, plvItem->pszText );
				}// END MOD
			}
			break;
		case et_Reserved0A:
		case et_Reserved0B:
		case et_Reserved0C:
		case et_Reserved0D:
		case et_Reserved0E:
		case et_Reserved0F:
		default:
			{	// nothing - return
			}
			break;
		}//endswitch
		
	 }
	 else
	 {
		 cerr << "ERROR: finished editing a different item/subitem then started."<<endl;
	 }
	 // obtain the new value
	 // set the new value in the device (datalist)
     if (plvItem->pszText != NULL)
     {
	//	SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);//doesn't do much in an ownerdraw...
	//	p_MyListDataSet->SetItemValue(plvItem->iItem,plvItem->iSubItem, TRUE, &(plvItem->state) );//UINT state;
     }
     *pResult = FALSE;
}



#if 0 /* newer mscommonctrls have this built in */
// HitTestEx    - Determine the row index and column index for a point
// Returns      - the row index or -1 if point is not over a row
// point        - point to be tested.
// col          - to hold the column index
int CChkListCtrl::HitTestEx(CPoint &point, int *col)
{
     int colnum = 0;
     int row = HitTest( point, NULL );
     
     if( col ) *col = 0;

     // Make sure that the ListView is in LVS_REPORT
     if( (GetWindowLong(m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT )
             return row;

     // Get the top and bottom row visible
     row = GetTopIndex();
     int bottom = row + GetCountPerPage();
     if( bottom > GetItemCount() )
             bottom = GetItemCount();
     
     // Get the number of columns
     CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
     int nColumnCount = pHeader->GetItemCount();

     // Loop through the visible rows
     for( ;row <= bottom;row++)
     {
         // Get bounding rect of item and check whether point falls in it.
         CRect rect;
         GetItemRect( row, &rect, LVIR_BOUNDS );
         if( rect.PtInRect(point) )
         {
             // Now find the column
             for( colnum = 0; colnum < nColumnCount; colnum++ )
             {
                 int colwidth = GetColumnWidth(colnum);
                 if( point.x >= rect.left 
                         && point.x <= (rect.left + colwidth ) )
                 {
                         if( col ) *col = colnum;
                         return row;
                 }
                 rect.left += colwidth;
             }
         }
     }
     return -1;
}



#endif // remove


/*************************************************************************************************
 *
 *   $History: ChkListCtrl.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Common Classes/ChkListCtrl
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
