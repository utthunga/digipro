/* xima_sdc.cpp
   
   holds the implementation of various operations needed by the SDC & Tokenizer

*/


#include "ximage.h"		/* includes xima_sdc.h */

#if CXIMAGE_SUPPORT_ENCODE
#pragma  message("Support Encoding")

////////////////////////////////////////////////////////////////////////////////
// For UNICODE support: char -> TCHAR
/**
 * Saves to disk the image in a specific format.
 * \param filename: file name
 * \param imagetype: file format, see ENUM_CXIMAGE_FORMATS
 * \return true if everything is ok
 */
bool CxImage::Save(FILE* hFile, DWORD imagetype)
{
	fseek(hFile, 0L, SEEK_END);
	outFileSize = ftell(hFile);

	bool bOK = Encode(hFile,imagetype);

	fseek(hFile, 0L, SEEK_END);
	outFileSize = ftell(hFile) - outFileSize;

	return bOK;
};

// return the size of the image just saved
long CxImage::GetOutSize() 
{	
	return outFileSize; 
};

#endif

////////////////////////////////////////////////////////////////////////////////

void CxImage::AppendDrawMode(DRAWMODES newMode)/* or the setting */
{
	info.drawModeFlags = (DRAWMODES)(((long)(info.drawModeFlags))|((long)newMode));
}

void CxImage::SetDrawMode(DRAWMODES newMode)   /* set absolutely */
{
	info.drawModeFlags = newMode;
}

long CxImage::GetDrawMode(void)
{
	return (long)(info.drawModeFlags);
}

////////////////////////////////////////////////////////////////////////////////
/**
 * Draws the image in the specified device context, with support for alpha channel, alpha palette, transparency, opacity.
 * \param hdc : destination device context
 * \param x,y : (optional) offset
 * \param cx,cy : (optional) size.
 *                 - If cx or cy are not specified (or less than 0), the normal width or height will be used
 *                 - If cx or cy are different than width or height, the image will be stretched
 *
 * \param pClipRect : limit the drawing operations inside a given rectangle in the output device context.
 * \param bSmooth : activates a bilinear filter that will enhance the appearence for zommed pictures.
 *                   Quite slow. Needs CXIMAGE_SUPPORT_INTERPOLATION.
 * \return true if everything is ok
 */
#if CXIMAGE_SUPPORT_WINDOWS
long CxImage::Draw(HDC hdc, long x, long y, long cx, long cy, RECT* pClipRect, bool bSmooth)
{
	RECT locRect = {x,y,x+cx,y+cy};
	RECT cb; 
	RECT* pR = pClipRect;
	if ( pClipRect == NULL )
	{
		GetClipBox(hdc,&cb);
		pR = & cb;
	}
	return DrawB(hdc, locRect, pR, bSmooth);
}


long CxImage::DrawB(HDC hdc, const RECT& SrcRect, RECT* pDestRect, bool bSmooth)
{
	RECT DST, SRC = SrcRect;

	bool bTransparent = info.nBkgndIndex >= 0;
	bool bAlpha = pAlpha != 0;

	//required for MM_ANISOTROPIC, MM_HIENGLISH, and similar modes [Greg Peatfield]
	int hdc_Restore = ::SaveDC(hdc);
	if (!hdc_Restore) 
		return 0;


	long DSTdx, DSTdy;
	if (pDestRect == NULL)
	{
		GetClipBox(hdc,&DST);
	}
	else
	{
		DST = *pDestRect;
#if !defined (_WIN32_WCE)
	/*  temporary do not clip...	
		RECT mainbox; // (experimental) 
		if (pDestRect){
			GetClipBox(hdc,&mainbox);// box in logical coords
			HRGN rgn = CreateRectRgnIndirect(pDestRect);
			ExtSelectClipRgn(hdc,rgn,RGN_AND);
			DeleteObject(rgn);
		}
	*/
#endif
	}
	DSTdx = DST.right  - DST.left;		// dest width
	DSTdy = DST.bottom - DST.top;		// dest height
 

	long SRCdx, SRCdy;
	if (SrcRect.right < 0)
	{
		SRC.right = head.biWidth;
	}
	SRCdx = SRC.right - SRC.left;		// src width

	if (SrcRect.bottom < 0)
	{
		SRC.bottom = head.biHeight;
	}
	SRCdy = SRC.bottom  - SRC.top;		// src height

	if((pDib==0)||(hdc==0)||(DSTdx==0)||(DSTdy==0)||(!info.bEnabled)) 
		return 0;


	//find the smallest area to paint
	RECT clipbox,paintbox(DST);

/** stevev 27jul12 - the GetClipBox generates a box the size of the SHOWING rect
    We don't want to size to that, we to size to the rect we sent in.
	eg big image is barely scrolled into view, you have a wide, short clip box
	scaling to that gives you a tiny image instead of the clipped one you wanted

	GetClipBox(hdc,&clipbox);
instead, we start with what was passed in  ****/
	clipbox = DST;// appears to fix our problem


// NOTE we can use DST for this (post debugging)
	paintbox.top    = min(clipbox.bottom,max(clipbox.top,   DST.top   ));
	paintbox.left   = min(clipbox.right, max(clipbox.left,  DST.left  ));
	paintbox.right  = max(clipbox.left,  min(clipbox.right, DST.left + DSTdx));
	paintbox.bottom = max(clipbox.top,   min(clipbox.bottom,DST.top  + DSTdy));

	bool isStretch = (GetDrawMode() & 
		 ((long)DM_STRETCH2FITASPECT|(long)DM_STRETCH2FITVERT|(long)DM_STRETCH2FITHORZ)) != 0;

	if( (GetDrawMode() & (long)DM_STRETCH2FITASPECT) != 0)
	{		
		Reset2Aspect(paintbox);
	}
	else
	{
		paintbox.top    = min(clipbox.bottom,max(clipbox.top,   SRC.top   ));
		paintbox.left   = min(clipbox.right, max(clipbox.left,  SRC.left  ));
		paintbox.right  = max(clipbox.left,  min(clipbox.right, SRC.left + SRCdx));
		paintbox.bottom = max(clipbox.top,   min(clipbox.bottom,SRC.top  + SRCdy));
	}

	if( (GetDrawMode() & (long)DM_STRETCH2FITVERT) != 0)
	{	
		paintbox.top    = min(clipbox.bottom,max(clipbox.top,   DST.top   ));
		paintbox.bottom = max(clipbox.top,   min(clipbox.bottom,DST.top  + DSTdy));
	}
	if( (GetDrawMode() & (long)DM_STRETCH2FITHORZ) != 0)
	{		
		paintbox.left   = min(clipbox.right, max(clipbox.left,  DST.left  ));
		paintbox.right  = max(clipbox.left,  min(clipbox.right, DST.left + DSTdx));
	}
	if( (GetDrawMode() & (long)DM_CENTERVERT) != 0)
	{		
		if ((paintbox.bottom - paintbox.top) < DSTdy )
		{
			int y = ( DSTdy - (paintbox.bottom - paintbox.top) ) / 2;
			paintbox.bottom += y;
			paintbox.top    += y;
		}
	}
	if( (GetDrawMode() & (long)DM_CENTERHORZ) != 0)
	{		
		if ((paintbox.right - paintbox.left) < DSTdx )
		{
			int y = ( DSTdx - (paintbox.right - paintbox.left) ) / 2;
			paintbox.left  += y;
			paintbox.right += y;
		}
	}

	DST   = paintbox;
	DSTdx = DST.right  - DST.left;		// dest width
	DSTdy = DST.bottom - DST.top;		// dest height

	if (!(bTransparent || bAlpha || info.bAlphaPaletteEnabled))
	{
		if ( (DSTdx==head.biWidth && DSTdy==head.biHeight) || ! isStretch )
		{ //DEFAULT
#if !defined (_WIN32_WCE)
			SetStretchBltMode(hdc,COLORONCOLOR);
#endif
			SetDIBitsToDevice(hdc, DST.left, DST.top, head.biWidth, head.biHeight, 0, 0, 0, SRCdy,
						info.pImage,(BITMAPINFO*)pDib,DIB_RGB_COLORS);
		}
		else 
		{ //STRETCH the source Rect to Dest Rect
			//pixel informations
			RGBQUAD c={0,0,0,0};
			//Preparing Bitmap Info
			BITMAPINFO bmInfo;
			memset(&bmInfo.bmiHeader,0,sizeof(BITMAPINFOHEADER));
			bmInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
			bmInfo.bmiHeader.biWidth   = DSTdx;
			bmInfo.bmiHeader.biHeight  = DSTdy;
			bmInfo.bmiHeader.biPlanes  = 1;
			bmInfo.bmiHeader.biBitCount=24;
	/*	The bitmap has a maximum of 2^24 colors, and the bmiColors member of BITMAPINFO is NULL. 
		Each 3-byte triplet in the bitmap array represents the relative intensities of blue, 
		green, and red, respectively, for a pixel. The bmiColors color table is used for 
		optimizing colors used on palette-based devices, and must contain the number of entries 
		specified by the biClrUsed member of the BITMAPINFOHEADER. 
	*/
			BYTE *pbase;	//points to the final dib
			BYTE *pdst;		//current pixel from pbase
			BYTE *ppix;		//current pixel from image
			//get the background
			HDC TmpDC=CreateCompatibleDC(hdc);
			HBITMAP TmpBmp=CreateDIBSection(hdc,&bmInfo,DIB_RGB_COLORS,(void**)&pbase,0,0);
			HGDIOBJ TmpObj=SelectObject(TmpDC,TmpBmp);


			if (pbase)
			{
				long DSTcol, DSTrow;//xx,yy;
				long SRCcol, SRCrow;// sx,sy;
				float Sy,Sx;

//				float dx,dy;
				BYTE *psrc;

				long ew = ((((24 * DSTdx) + 31) / 32) * 4);	// effective dest row length(in bytes)

				float SRCperDSTx =(float)SRCdx/(float)DSTdx; 
				float SRCperDSTy =(float)SRCdy/(float)DSTdy;

				for(DSTrow=0; DSTrow < DSTdy; DSTrow++)
				{
					//calc source row
/*try				dy = head.biHeight-((ymax-y)-yy)*fy;
*/						
					Sy     = SRC.bottom - (((DSTdy - DSTrow)*SRCperDSTy)+SRC.top);
					SRCrow = max(0L,(long)floor(Sy));

					psrc = info.pImage+(SRCrow*info.dwEffWidth);
					pdst = pbase+(DSTrow*ew);

					for(DSTcol=0;DSTcol<DSTdx;DSTcol++)
					{
						Sx     = (DSTcol*SRCperDSTx) + SRC.left;
						SRCcol = max(0L,(long)floor(Sx));
#if CXIMAGE_SUPPORT_INTERPOLATION
						if (bSmooth)
						{
// stevev 08jan16 - not using this addition		if (fx > 1 && fy > 1) 
//							{ 
//								c = GetAreaColorInterpolated(dx - 0.5f, dy - 0.5f, fx, fy, CxImage::IM_BILINEAR, CxImage::OM_REPEAT); 
//							} 
//							else
							{
								c = GetPixelColorInterpolated(Sx - 0.5f, Sy - 0.5f, CxImage::IM_BILINEAR, 
															CxImage::OM_REPEAT);
							}
						} 
						else
#endif //CXIMAGE_SUPPORT_INTERPOLATION
						{
							if (head.biClrUsed)
							{
								c=GetPaletteColor(GetPixelIndex(SRCcol,SRCrow));
							} 
							else 
							{
								ppix = psrc + SRCcol*3; // 3 bytes per pixel color
								c.rgbBlue = *ppix++;
								c.rgbGreen= *ppix++;
								c.rgbRed  = *ppix;
							}
						}
						*pdst++=c.rgbBlue;
						*pdst++=c.rgbGreen;
						*pdst++=c.rgbRed;
					}// next col (x)
				}// next row (y)
			}// if we have a dest pointer
			//paint the image & cleanup
			SetDIBitsToDevice(hdc,DST.left,DST.top,DSTdx,DSTdy,  0,       0,        0,          DSTdy,         pbase,&bmInfo,0);
			//                  xdest,ydest,dibWid,dibHgt,leftDIBx,lowerDIBy,firstDIBrow,linesINpbase,ptr2DIB, info,palletref
			DeleteObject(SelectObject(TmpDC,TmpObj));
			DeleteDC(TmpDC);
		}//endelse - stretch mode
	}// endif transparent or alpha or alphaPallet 
	else // 0ne of bTransparent , bAlpha , info.bAlphaPaletteEnabled is TRUE
	{	// draw image with transparent/alpha blending
	//////////////////////////////////////////////////////////////////
		//Alpha blend - Thanks to Florian Egel

		//pixel informations
		RGBQUAD c={0,0,0,0};
		RGBQUAD ct = GetTransColor();
		long* pc = (long*)&c;
		long* pct= (long*)&ct;
		long cit = GetTransIndex();
		long ci;

		//Preparing Bitmap Info
		BITMAPINFO bmInfo;
		memset(&bmInfo.bmiHeader,0,sizeof(BITMAPINFOHEADER));
		bmInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
		bmInfo.bmiHeader.biWidth   =DSTdx;
		bmInfo.bmiHeader.biHeight  =DSTdy;
		bmInfo.bmiHeader.biPlanes  = 1;
		bmInfo.bmiHeader.biBitCount=24;

		BYTE *pbase;	//points to the final dib
		BYTE *pdst;		//current pixel from pbase
		BYTE *ppix;		//current pixel from image

		//get the background
		HDC TmpDC=CreateCompatibleDC(hdc);
		HBITMAP TmpBmp=CreateDIBSection(hdc,&bmInfo,DIB_RGB_COLORS,(void**)&pbase,0,0);
		HGDIOBJ TmpObj=SelectObject(TmpDC,TmpBmp);
		BitBlt(TmpDC,0,0,DSTdx,DSTdy,hdc,DST.left,DST.top,SRCCOPY);

		if (pbase)
		{
			long DSTcol, DSTrow;//xx,yy;
			long SRCcol, SRCrow;// sx,sy;
			float Sy,Sx;

			long alphaoffset;//,ix,iy;
			BYTE a,a1,*psrc;

			long ew = ((((24 * DSTdx) + 31) / 32) * 4);

	//		long ymax = paintbox.bottom;
	//		long xmin = paintbox.left;

			// always::: if (cx!=head.biWidth || cy!=head.biHeight)
			{
				//STRETCH
				//float fx=(float)head.biWidth/(float)destw; // 12jan05-sjv was::> cx;
				//float fy=(float)head.biHeight/(float)desth;// 12jan05-sjv was::> cy;
				
				float SRCperDSTx =(float)SRCdx/(float)DSTdx; 
				float SRCperDSTy =(float)SRCdy/(float)DSTdy;

	//			float dx,dy;
	//			long sx,sy;
				
				//for(yy=0;yy<desth;yy++)
				for(DSTrow=0; DSTrow < DSTdy; DSTrow++)
				{
					//dy = head.biHeight-(ymax-yy-y)*fy;
					//sy = max(0L,(long)floor(dy));	
					Sy     = SRC.bottom - (((DSTdy - DSTrow)*SRCperDSTy)+SRC.top);
					SRCrow = max(0L,(long)floor(Sy));


					//alphaoffset = sy*head.biWidth;
					alphaoffset = SRCrow * head.biWidth;// offset into original
					psrc = info.pImage + (SRCrow*info.dwEffWidth);//sy*info.dwEffWidth;
					pdst = pbase + (DSTrow*ew);//yy*ew;

					//for(xx=0;xx<destw;xx++)
					for(DSTcol=0;DSTcol<DSTdx;DSTcol++)
					{
						Sx     = (DSTcol*SRCperDSTx) + SRC.left;
						SRCcol = max(0L,(long)floor(Sx));

						//dx = (xx+xmin-x)*fx;
						//sx = max(0L,(long)floor(dx));

						//if (bAlpha) a=pAlpha[alphaoffset+sx]; else a=255;
						if (bAlpha) a=pAlpha[alphaoffset+SRCcol]; else a=255;
						a =(BYTE)((a*(1+info.nAlphaMax))>>8);

						if (head.biClrUsed)
						{
							ci = GetPixelIndex(SRCcol,SRCrow);//(sx,sy);
#if CXIMAGE_SUPPORT_INTERPOLATION
							if (bSmooth)
							{
// stevev 08jan16 - not using this change	if (fx > 1 && fy > 1) 
//								{ 
//									c = GetAreaColorInterpolated(dx - 0.5f, dy - 0.5f, fx, fy, CxImage::IM_BILINEAR, CxImage::OM_REPEAT); 
//								} 
//								else 
								{ 
									//c = GetPixelColorInterpolated(dx - 0.5f, dy - 0.5f, 
									//						CxImage::IM_BILINEAR, CxImage::OM_REPEAT);
									c = GetPixelColorInterpolated(Sx - 0.5f, Sy - 0.5f, 
															CxImage::IM_BILINEAR, CxImage::OM_REPEAT);
								}
							} 
							else
#endif //CXIMAGE_SUPPORT_INTERPOLATION
							{
								c = GetPaletteColor(GetPixelIndex(SRCcol,SRCrow));//(sx,sy));
							}
							if (info.bAlphaPaletteEnabled)
							{
								a = (BYTE)((a*(1+c.rgbReserved))>>8);
							}
						} 
						else // NOT ClrUsed
						{
#if CXIMAGE_SUPPORT_INTERPOLATION
							if (bSmooth)
							{
// stevev 08jan16 - not using this change	if (fx > 1 && fy > 1) 
//								{
//									c = GetAreaColorInterpolated(dx - 0.5f, dy - 0.5f, fx, fy, CxImage::IM_BILINEAR, CxImage::OM_REPEAT); 
//								} 
//								else 
								{ 
									//c = GetPixelColorInterpolated(dx - 0.5f, dy - 0.5f, 
									//						CxImage::IM_BILINEAR, CxImage::OM_REPEAT);
									c = GetPixelColorInterpolated(Sx - 0.5f, Sy - 0.5f, 
															CxImage::IM_BILINEAR, CxImage::OM_REPEAT);
								}
							} 
							else
#endif //CXIMAGE_SUPPORT_INTERPOLATION
							{
								ppix = psrc + SRCcol*3;//sx*3;
								c.rgbBlue = *ppix++;
								c.rgbGreen= *ppix++;
								c.rgbRed  = *ppix;
							}
						}
						//if (*pc!=*pct || !bTransparent){
						//if ((head.biClrUsed && ci!=cit) || ((!head.biClrUsed||bSmooth) && 
						//											*pc!=*pct) || !bTransparent)
						if ((head.biClrUsed  && ci!=cit)   || 
							(!head.biClrUsed && *pc!=*pct) || 
							(!bTransparent)  )
						{
							// DJT, assume many pixels are fully transparent or opaque and 
							//		thus avoid multiplication
							if (a == 0)
							{			// Transparent, retain dest 
								pdst+=3; 
							} 
							else 
							if (a == 255) 
							{	// opaque, ignore dest 
								*pdst++= c.rgbBlue; 
								*pdst++= c.rgbGreen; 
								*pdst++= c.rgbRed; 
							} 
							else 
							{				// semi transparent 
								a1=(BYTE)~a;
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbBlue)>>8); 
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbGreen)>>8); 
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbRed)>>8); 
							} 
						} 
						else 
						{
							pdst+=3;// skip transparent
						}
					}
				}
			} 
#if 0
			else 
			{
				//NORMAL
				iy=head.biHeight-ymax+y;
				for(yy=0;yy<desth;yy++,iy++)
				{
					alphaoffset=iy*head.biWidth;
					ix=xmin-x;
					pdst=pbase+yy*ew;
					ppix=info.pImage+iy*info.dwEffWidth+ix*3;
					for(xx=0;xx<destw;xx++,ix++)
					{
						if (bAlpha) a=pAlpha[alphaoffset+ix]; else a=255;
						a = (BYTE)((a*(1+info.nAlphaMax))>>8);

						if (head.biClrUsed)
						{
							ci = GetPixelIndex(ix,iy);
							c = GetPaletteColor((BYTE)ci);
							if (info.bAlphaPaletteEnabled)
							{
								a = (BYTE)((a*(1+c.rgbReserved))>>8);
							}
						} 
						else 
						{
							c.rgbBlue = *ppix++;
							c.rgbGreen= *ppix++;
							c.rgbRed  = *ppix++;
						}

						//if (*pc!=*pct || !bTransparent){
						if ((head.biClrUsed && ci!=cit) || (!head.biClrUsed && *pc!=*pct) || !bTransparent)
						{
							// DJT, assume many pixels are fully transparent or opaque and thus avoid multiplication
							if (a == 0) 
							{			// Transparent, retain dest 
								pdst+=3; 
							} 
							else 
							if (a == 255) 
							{	// opaque, ignore dest 
								*pdst++= c.rgbBlue; 
								*pdst++= c.rgbGreen; 
								*pdst++= c.rgbRed; 
							} else {				// semi transparent 
								a1=(BYTE)~a;
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbBlue)>>8); 
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbGreen)>>8); 
								*pdst++=(BYTE)((*pdst * a1 + a * c.rgbRed)>>8); 
							} 
						} 
						else 
						{
							pdst+=3;
						}
					}
				}
			}
#endif
		}
		//paint the image & cleanup
		//SetDIBitsToDevice(hdc,paintbox.left,paintbox.top,destw,desth,0,0,0,desth,pbase,&bmInfo,0);
		SetDIBitsToDevice(hdc,DST.left,DST.top,DSTdx,DSTdy,  0,       0,        0,          DSTdy,         pbase,&bmInfo,0);
		DeleteObject(SelectObject(TmpDC,TmpObj));
		DeleteDC(TmpDC);
	}
/* temporary do not clip...	
	if (pDestRect){  // (experimental)
		HRGN rgn = CreateRectRgnIndirect(&mainbox);// return to original
		ExtSelectClipRgn(hdc,rgn,RGN_OR);
		DeleteObject(rgn);
	}
*/
	::RestoreDC(hdc,hdc_Restore);
	return 1;
}

#endif // CXIMAGE_SUPPORT_WINDOWS

////////////////////////////////////////////////////////////////////////////////

/* 12 jan05 - sjv
 * support for the DM_STRETCH2FITASPECT draw mode */
void CxImage::Reset2Aspect(RECT& pntBox)
{
	long bx,by, ix,iy;
	bx = pntBox.right  - pntBox.left;
	by = pntBox.bottom - pntBox.top;
	
	ix = head.biWidth;
	iy = head.biHeight;

	float xRatio = (float)bx / (float) ix;
	float yRatio = (float)by / (float) iy;

	if ( xRatio > yRatio ) { xRatio = yRatio; } /* xRatio = min(xRatio,yRatio); */
	float fx = (float)ix * xRatio;
	float fy = (float)iy * xRatio;

	
	bx = pntBox.left + (long)floor(fx);	pntBox.right   = min(pntBox.right, bx);
	by = pntBox.top  + (long)floor(fy); pntBox.bottom  = min(pntBox.bottom,by);
}