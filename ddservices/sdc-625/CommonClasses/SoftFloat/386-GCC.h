
/*----------------------------------------------------------------------------
| One of the macros `BIGENDIAN' or `LITTLEENDIAN' must be defined.
*----------------------------------------------------------------------------*/
#define LITTLEENDIAN

/*----------------------------------------------------------------------------
| The macro `BITS64' can be defined to indicate that 64-bit integer types are
| supported by the compiler.
*----------------------------------------------------------------------------*/
#define BITS64

/*----------------------------------------------------------------------------
| Bill has his own version of 64 bit integers.
| If you are aren't useing Bill's compiler, define as in comment
*----------------------------------------------------------------------------*/
//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
#define UINT64    unsigned __int64
#define  INT64    signed   __int64
#else
#define UINT64     unsigned long long
#define  INT64     long long
#endif

/*----------------------------------------------------------------------------
| Each of the following `typedef's defines the most convenient type that holds
| integers of at least as many bits as specified.  For example, `uint8' should
| be the most convenient type that can hold unsigned integers of as many as
| 8 bits.  The `flag' type must be able to hold either a 0 or 1.  For most
| implementations of C, `flag', `uint8', and `int8' should all be `typedef'ed
| to the same as `int'.
*----------------------------------------------------------------------------*/
typedef char flag;
typedef unsigned char uint8;
typedef signed char int8;
typedef int uint16;
typedef int int16;
typedef unsigned int uint32;
typedef signed int int32;

#ifdef BITS64

#if defined(__GNUC__)

#include <stdint.h>

typedef uint64_t uint64;
typedef int64_t int64;

#else // not __GNUC__ 

typedef UINT64 uint64;
typedef  INT64 int64;

#endif // __GNUC__

#endif // BITS64

/*----------------------------------------------------------------------------
| Each of the following `typedef's defines a type that holds integers
| of _exactly_ the number of bits specified.  For instance, for most
| implementation of C, `bits16' and `sbits16' should be `typedef'ed to
| `unsigned short int' and `signed short int' (or `short int'), respectively.
*----------------------------------------------------------------------------*/
typedef unsigned char bits8;
typedef signed char sbits8;
typedef unsigned short int bits16;
typedef signed short int sbits16;
typedef unsigned int bits32;
typedef signed int sbits32;

#ifdef BITS64

#if defined(__GNUC__)

typedef uint64_t bits64;
typedef int64_t sbits64;

#else // not __GNUC__

typedef UINT64 bits64;
typedef  INT64 sbits64;

#endif // __GNUC__

#endif // BITS64

#ifdef BITS64
/*----------------------------------------------------------------------------
| The `LIT64' macro takes as its argument a textual integer literal and
| if necessary ``marks'' the literal as having a 64-bit integer type.
| For example, the GNU C Compiler (`gcc') requires that 64-bit literals be
| appended with the letters `LL' standing for `long long', which is `gcc's
| name for the 64-bit integer type.  Some compilers may allow `LIT64' to be
| defined as the identity macro:  `#define LIT64( a ) a'.
*----------------------------------------------------------------------------*/

#if defined(__GNUC__)
#define LIT64( a ) a##LL
#else // not __GNUC__
#define LIT64( a ) a##ui64
#endif // __GNUC__


#endif

/*----------------------------------------------------------------------------
| The macro `INLINE' can be used before functions that should be inlined.  If
| a compiler does not support explicit inlining, this macro should be defined
| to be `static'.
*----------------------------------------------------------------------------*/
#define INLINE extern inline

