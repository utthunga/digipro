#if !defined(AFX_EXTENDEDINFODLG1_H__1692A1CE_D67F_4B66_A29E_08C868C6685E__INCLUDED_)
#define AFX_EXTENDEDINFODLG1_H__1692A1CE_D67F_4B66_A29E_08C868C6685E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtendedInfoDlg1.h : header file
//
#include "resource.h"
/////////////////////////////////////////////////////////////////////////////
// CExtendedInfoDlg dialog

class CExtendedInfoDlg : public CDialog
{
// Construction
public:
	CExtendedInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExtendedInfoDlg)
	enum { IDD = IDD_EXTENDEDINFO };
	CString	m_ItemID;
	CString	m_ItemName;
	CString	m_ItemSize;
	CString	m_ItemType;
	CString	m_VariableType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtendedInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExtendedInfoDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTENDEDINFODLG1_H__1692A1CE_D67F_4B66_A29E_08C868C6685E__INCLUDED_)
