/*************************************************************************************************
 *
 * $Workfile: DDlistData.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		12/05/02   sjv    merged cframedatalist and frameview to get list data handler
 *		
 *
 * #include "DDlistData.h"
 */

#ifndef _DDLISTDATA_H
#define _DDLISTDATA_H
#ifdef INC_DEBUG
#pragma message("In DDlistData.h") 
#endif


//#include "FrameListData.h"// before chklistview
//#include "ChkListView.h"
#include "EditCell.h"
//#include "dllinstance.h"


// the data structure
#ifdef LISTDATATYPE
#undef LISTDATATYPE
#undef LISTDATANAME
#endif

class CVarData;

///#define LISTDATATYPE	CVarData
///#define LISTDATANAME   "CVarData"
#define LISTDATATYPE	hCVar*
#define LISTDATANAME   "hCVar Pointer"

#include "ddbVar.h"
#include "ListData.h"		// include after correct definition of LISTDATATYPE
#include "ddbGeneral.h"
//#include "dllapi.h"
//#include "dllinstance.h"
//OLD #include "DDDDdevice.h"
#include "ddbDeviceMgr.h"
#include <list>
#ifdef INC_DEBUG
#pragma message("    Finished Includes::DDlistData.h") 
#endif



#define SYM_MFG_ID    153 /*manufacturer_id                  ENUMERATED       1*/
#define SYM_DEVTYP    154 /*device_type                      ENUMERATED       1*/
#define SYM_DEVREV    157 /*transmitter_revision             UNSIGNED         1*/

inline editType_t var2edit(variableType_t vt)
{
	switch(vt)
	{
	case vT_Integer:		// 2
		return et_intEdit;
	case vT_Unsigned:		// 3
		return et_hexIntEdit;
	case vT_FloatgPt:		// 4
		return et_floatEdit;
	case vT_Double:			// 5
		return et_floatEdit;
	case vT_Enumerated:		// 6
		return et_enumEdit;
	case vT_BitEnumerated:	// 7
		return et_bitEnumEdit;
	case vT_Index:			// 8
		return et_intEdit;
	case vT_Ascii:			// 9
		return et_asciiEdit;
	case vT_PackedAscii:	//10 - HART only
		return et_pkdAsciiEdit;
	case vT_Password:		//11
		return et_passwrdEdit;
	case vT_BitString:		//12
		return et_asciiEdit;
	case vT_HartDate:		//13 - HART only
		return et_dateEdit;
	case vT_Time:			//14
		return et_dateEdit;
	case vT_DateAndTime:	//15
		return et_dateEdit;
	case vT_Duration:		//16
		return et_dateEdit;
	case vT_EUC:			//17
		return et_asciiEdit;
	case vT_OctetString:	//18
		return et_asciiEdit;
	case vT_VisibleString:	//19
		return et_asciiEdit;
	case vT_TimeValue:		//20
		return et_dateEdit;
	case vT_Boolean:		//21
		return et_booleanEdit;
	case vT_unused:			// 0
	case vT_undefined:		// 1
	default:
		return et_Reserved0F;
	}//endswitch
};

/////////////////////////////////////////////////////////////////////////////
// Display setup

#define ROUNDL( d ) ((long)((d) + ((d) > 0 ? 0.5 : -0.5)))


// used to transfer the needed information to the listctrl
class CVarData : public CObject
{
public:
	CVarData(){TRACE(_T("Temporaray CVarData Constructor.\n"));};

};

/////////////////////////////////////////////////////////////////////////////
// CDDlistData view

// the list class (in this application) is equivelent to the device
class CDDlistData : public CListData
{
// Attributes
public:
	CDDlistData(DD_Key_t dbKey = 0); 
	CDDlistData(Indentity_t iD,DD_Key_t dbKey = 0);
	~CDDlistData(); 
	// we own the data and the database


// now in device	StrVector_t DeviceDescStrings;
// now in device	int         DBnotAvailable; // holds the return code of the initDB (FALSE is success)
	int         DDAvailable;    // TRUE if all the DB preliminaries are successfull

	
//SUBITEMLIST_T testDataStore;
	//OLD CddddDevice* pDevice;
	hCDeviceManger* pDevMgr;
	hCddbDevice*    pCurDev;

	// Operations
public:
	// we have to overload 'em all if we want to use the data types and defaults here
	int			PreFillListData( LPCTSTR lpszFileName );
	CString		QueryItemText  (int nRow, int nCol, BOOL getNumber = FALSE);
   datumInfo_t	QueryItemInfo  (int nRow, int nCol);
	sColDesc&	GetColDesc     (int ColNum, sColDesc& nColDesc);
	BOOL	    QueryValidity  (int nRow);
	void		SetItemValidity(int nRow, BOOL nValid);
	int			FillColumnHead (CListCtrl * p_ListControl);
	int         QuerySubListData(int nRow, int nCol, 
								 CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&>& datArray, 
								 ulong& value, int& valLen);
	int         SetItemValue(int nRow, int nCol, BOOL& isChanged, void* pValue = NULL);// type by location
	int         ApplyNow(void);
	int			CancelNow();
	int			GetSize(void);
	int			ReleaseAll(void);// disconnection imminent

	int			FillVarsFromINI(FILE *stream);// caller controls the open/close of the file
	int         FillINIfromVars(FILE *stream);
//???   void      SetItemText    (int nRow, int nCol, CString &str);

//???	void      ClrData        (void);

//	int			FillItems(CListCtrl& ListCtrl);
// Added By deepak: Property page class needs corressmponding view data
	int			FillItems(CListCtrl& ListCtrl, CArray<int,int> &MyView);	
// END Modification
//???	BOOL	    WriteItems(CFile * Out, CFileException * p_eF);


   CEdit*  EditSubItem (int Item, int subItm);
   BOOL    DeleteRow(void);
   BOOL    InsertRow(void);

   // other functions
public:
   // initialize 
	virtual void OnInitialUpdate();
   // open and startup the database
   // set which DD is required
   // load the variable list from that dd
   // special functions??

	DD_Key_t getActiveKey(void) { return activeDDkey;};
	// normally returns an hCVar*
	LISTDATATYPE getRowsVar( int nRow ) { return (*(RowList.ElementAt(pViewList->ElementAt(nRow)))); };
/************
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDlistData)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
*************/

// Implementation
protected:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	DD_Key_t    activeDDkey;
	Indentity_t entryIdentity;


//	AitemList_t* pVarItems;

/*************
	// Generated message map functions
protected:
	//{{AFX_MSG(CDDlistData)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnFixchksum();
	afx_msg void OnFixtime();
	afx_msg void OnUpdateFixchksum(CCmdUI* pCmdUI);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
**************/

};

#ifdef INC_DEBUG
#pragma message("    Finished Including::DDlistData.h") 
#endif

/////////////////////////////////////////////////////////////////////////////
#endif	//_DDLISTDATA_H


/*************************************************************************************************
 *
 *   $History: DDlistData.h $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 12:01p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Milestone: Xmtr handles commands via pipes, IDE sends Cmd Zero
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/08/03    Time: 8:58a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/08/03    Time: 7:53a
 * Created in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 *************************************************************************************************
 */
