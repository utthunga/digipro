/*************************************************************************************************
 *
 * $Workfile: ddl_ini.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */
 
 
/*****************************************************************************
 * Include Files
 *****************************************************************************
 */ 

#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include < basetsd.h >
#include < iostream >
#include < iomanip >

/*****************************************************************************
 * Global Variables
 *****************************************************************************
 */

/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */

/*****************************************************************************
 * Local Variables
 *****************************************************************************
 */
typedef struct DdlSymbol {
	char id[20];
	char label[50];
	char value[50];
	} DDLSYMBOL;
	
DDLSYMBOL *symbols = 0;
int nSymbols = 0;

#include <string>
#include <vector>
using namespace std;

typedef struct stdLibSym_s
{
	string id;
	string label;
	string value;
} STDLINSYMBOL;


vector<STDLINSYMBOL> extraSymbols;

/*****************************************************************************
 * Local Function Protypes
 *
 * 3 ENTRY POINTS: openIni() and closeIni(), getSymbolValue()
 *****************************************************************************
 */

//BOOL openIni();
bool openIni(char* pFname);
bool closeIni();
char *getSymbolValue(unsigned long id);
char *getSymbolValue(char *id);

static char openedFile[512];

//////////////////////////////////////////////////////////////////////
// save defs, free RAM, return true on error
bool closeIni() {

	if ( strlen(openedFile) == 0)
	{
		clog << "ERROR: no file to close."<<endl;
		return true;
	}
	if (symbols) {

		// save possibly edited symbols back to file
		FILE *fp = fopen(openedFile,"w");//"370402.ini", "w");
		if (!fp) {
			// problem opening file
			return true;
			}

		for (int i = 0; i < nSymbols; i++) {
			fprintf(fp, "%s,%s,%s\n", symbols[i].id,
symbols[i].label, symbols[i].value);
			}

		if ( extraSymbols.size() > 0 )
		{
			for (vector<STDLINSYMBOL>::iterator it = extraSymbols.begin();
				it < extraSymbols.end(); it++)
				{
					fprintf(fp, "%s,%s,%s\n", it->id.c_str(),
									it->label.c_str(), it->value.c_str());
				}
		}

		fclose(fp);


		free(symbols);
		symbols = 0;
		}
	

	return false;

	}


// open ini file, return true on error
bool openIni(char* iniFileNm)
{
	if ( iniFileNm == NULL || strlen(iniFileNm) ==0)
	{
		strcpy(openedFile, "");
		return true;
	}
	// free existing array of symbols if necessary
	closeIni();

	FILE *fp = fopen(iniFileNm, "r");//"370402.ini", "r");
	if (!fp) 
	{
		// problem opening file
		clog << "File did not open. ("<< iniFileNm<< ")"<<endl;

		fp = fopen(iniFileNm, "a+");//"370402.ini", "r");
		if (!fp) 
		{// problem opening file
			clog << "File did not create. ("<< iniFileNm<< ")"<<endl;
			return true;
		}
	}
	strcpy(openedFile, iniFileNm);
	// count lines in file
	const int bufsize = 200;
	char buf[bufsize];
	nSymbols = 0;
	while (fgets(buf, bufsize, fp)) {
		nSymbols++;
		}
	fclose(fp);

	// allocate array of DDLSYMBOLS
	int symbolsize = sizeof(DDLSYMBOL);
	symbols = (DDLSYMBOL *) malloc(nSymbols*symbolsize);


	// read symbols into array
	fp = fopen(iniFileNm, "r");//"370402.ini", "r");
	if (!fp) {
		// problem opening file
		return true;
		}

	// parse each line
	int i = 0; // line counter
	while (fgets(buf, bufsize, fp)) {
		char *seps = ",\r\n";
		char *p;
		int len = 0;
		char buf2[bufsize];
		strcpy(buf2, buf);
		
		// id
		p = strtok(buf, seps);
		strcpy(symbols[i].id, p);
		len += (strlen(p) + 1);
		
		// label
		p = strtok(NULL, seps);
		strcpy(symbols[i].label, p);
		len += (strlen(p) + 1);
		
		// value
		p = buf2 + len;				// find field in buf2, keeping commas embedded in 3rd field
		p[strlen(p) - 1] = '\0';	// erase newline
		strcpy(symbols[i].value, p);

		// bump line count
		i++;
		}

	fclose(fp);

	return false;
}

BOOL setSymbolValue(unsigned long id, char* sym)
{
	char buffer[256];

	sprintf(buffer, "%u", id);

	for (int i = 0; i < nSymbols; i++) 
	{
		if (strcmp(symbols[i].id, buffer) == 0) 
		{
			strcpy(symbols[i].value,sym);
			return FALSE; // no error
		}
	}
	// we didn't find it
	
	STDLINSYMBOL wrkSym;
	wrkSym.id    = buffer;
	wrkSym.label = "internally set symbol";
	wrkSym.value = sym;

	extraSymbols.push_back(wrkSym);
	return FALSE; // no error

}

// look up symbol id, return value or NULL if not found
char *getSymbolValue(char *id) {

	for (int i = 0; i < nSymbols; i++) {
		if (strcmp(symbols[i].id, id) == 0) {
			return symbols[i].value;
			}
		}

	// id not found! return null
	return 0;
	}

char *getSymbolValue(unsigned long id)  {
	char buffer[256];

	sprintf(buffer, "%u", id);
	return(getSymbolValue(buffer));
	
}



#if 0
//////////////////////////////////////////////////////////////////////
// EXCERCISE ENTRY POINTS


int main(int argc, char* argv[])
{
	openIni();


	while (true) {
		printf("\n\nEnter Symbol ID:  ");

		// get id from user
		char buf[200];
		gets(buf);
		if (strlen(buf) == 0) {
			// break on user carriage return
			break;
			}

		char *value = getSymbolValue(buf);

		if (value) {
			printf("ID <%s>  Value = <%s>\n", buf, value);
			}
		else {
			printf("ID <%s> not found.\n", buf);
			}
		}
		

	closeIni();

	return 0;
}

// EOF
//////////////////////////////////////////////////////////////////////

#endif


/*************************************************************************************************
 *
 *   $History: ddl_ini.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */