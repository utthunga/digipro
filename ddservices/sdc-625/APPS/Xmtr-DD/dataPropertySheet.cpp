/*************************************************************************************************
 *
 * $Workfile: dataPropertySheet.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */

//#ifndef _DEBUG
//15aug06 causes errors with the latest try:catch structures in the devObject::#define _IOSTREAM_
//#endif

#include "stdafx.h"
#include "logging.h"// to get cerr for new catch
#include "XmtrDD.h"
#include "dataPropertySheet.h"

#include "ddbDevice.h"
#include "ddbItemLists.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CdataPropertySheet

IMPLEMENT_DYNAMIC(CdataPropertySheet, CResizableSheet)//CPropertySheet)

CdataPropertySheet::CdataPropertySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	 : CResizableSheet(nIDCaption, pParentWnd, iSelectPage)
	//:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{	
	initVars();
}

CdataPropertySheet::CdataPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	: CResizableSheet(pszCaption, pParentWnd, iSelectPage)
	//:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{	//using this one
	initVars();
}

void CdataPropertySheet::initVars(void)
{
// Add all property pages	
	AddPage(&m_Allpage);

//Added by Deepak  We are a page for each class
	AddPage(&m_CHartClass);
	AddPage(&m_CDeviceClass);
	AddPage(&m_CDynamicClass);
	AddPage(&m_CLocalClass);
	
	AddPage(&m_CDiagnosticClass);
	AddPage(&m_CCorrectionClass);
		
	AddPage(&m_CInputBlockClass);
	AddPage(&m_CInputClass);
	AddPage(&m_CAnalogOutputClass);

	AddPage(&m_CComputationClass);
	AddPage(&m_CLogicalDisplayClass);
	
	AddPage(&m_CFrequencyClass);
	AddPage(&m_CDiscreteClass);

	AddPage(&m_CServiceClass);
	AddPage(&m_CFactoryClass);
	AddPage(&m_CUnAssignedClass);

// Set caption for each of pages
	m_CDiagnosticClass.m_psp.pszTitle =_T("Diagnostic Class");
	m_CDiagnosticClass.m_psp.dwFlags |= PSP_USETITLE; 
	
	m_CDynamicClass.m_psp.pszTitle =_T("Dynamic Class");
	m_CDynamicClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CServiceClass.m_psp.pszTitle =_T("Service Class");
	m_CServiceClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CCorrectionClass.m_psp.pszTitle =_T("Correction Class");
	m_CCorrectionClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CComputationClass.m_psp.pszTitle =_T("Computation Class");
	m_CComputationClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CInputBlockClass.m_psp.pszTitle =_T("InputBlock Class");
	m_CInputBlockClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CAnalogOutputClass.m_psp.pszTitle =_T("Analog Output Class");
	m_CAnalogOutputClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CHartClass.m_psp.pszTitle =_T("Hart Class");
	m_CHartClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CLogicalDisplayClass.m_psp.pszTitle =_T("LogicalDisplayClass");
	m_CLogicalDisplayClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CFrequencyClass.m_psp.pszTitle =_T("Frequency Class");
	m_CFrequencyClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CDiscreteClass.m_psp.pszTitle =_T("Discrete Class");
	m_CDiscreteClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CDeviceClass.m_psp.pszTitle =_T("Device Class");
	m_CDeviceClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CLocalClass.m_psp.pszTitle =_T("Local Class");
	m_CLocalClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CInputClass.m_psp.pszTitle =_T("Input Class");
	m_CInputClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CUnAssignedClass.m_psp.pszTitle =_T("UnAssigned Class");
	m_CUnAssignedClass.m_psp.dwFlags |= PSP_USETITLE;

	m_CFactoryClass.m_psp.pszTitle =_T("Factory Class");
	m_CFactoryClass.m_psp.dwFlags |= PSP_USETITLE;

// We dont want all the tabs to be shown at a time
	EnableStackedTabs(FALSE);

//END Modification

	m_pDataList = NULL;
	m_hIcon     = 0;
	theDDkey    = 0;
}

CdataPropertySheet::~CdataPropertySheet()
{
	if ( m_pDataList != NULL )
	{
		m_pDataList->ReleaseAll();
		delete m_pDataList;
		m_pDataList = NULL;
	}
}


BEGIN_MESSAGE_MAP(CdataPropertySheet, CResizableSheet)//CPropertySheet)
	//{{AFX_MSG_MAP(CdataPropertySheet)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_BNCHECK,OnCheckUnCheck)	// added by deepak
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
	

/////////////////////////////////////////////////////////////////////////////
// CdataPropertySheet message handlers

BOOL CdataPropertySheet::OnInitDialog() 
{
	
	BOOL bResult = CResizableSheet::OnInitDialog();
                // CPropertySheet::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	///////////////////////////////////////////////////////////////////////////////
	 // Load the app icons
     SetIcon( AfxGetApp()->LoadIcon( IDI_ICOND ), TRUE ); //32 X 32 (Windows Bar)
     SetIcon( AfxGetApp()->LoadIcon( IDI_ICONE ), FALSE );//16 X 16 (toolbar icon)

     // add the minimize button to the window
     ::SetWindowLong( m_hWnd, GWL_STYLE, GetStyle() | WS_MINIMIZEBOX );
     
     // add the minimize command to the system menu
     GetSystemMenu( FALSE )->InsertMenu( -1, MF_BYPOSITION | MF_STRING,
                                SC_ICON, _T("Minimize") );

     // activate the new sysmenu
    DrawMenuBar();
	// TODO: Add extra initialization here/////////////////////////////////////////
	if (m_pDataList == NULL )
	{
		// the data list here is equivelent to the device
		m_pDataList = new CDDlistData(entryIdent,theDDkey); // a new DB interface
	}
	m_pDataList->OnInitialUpdate();      // init the DB
	if (! m_pDataList->DDAvailable )
	{
		return FALSE;
	}
	CString s;
	DD_Key_t llK= m_pDataList->getActiveKey();
	int m,t,r,d; m = llK>>48;            t = (llK>>32) & 0x000000000000ffff;
	r = (llK>>16) & 0x00000000000000ff;  d = (llK>> 0) & 0x00000000000000ff;
	s.Format(_T("%04x.%04x.%02x.%02x"), m ,t, r, d);
	SetWindowText(s);
	

//	AddPage(&m_Allpage);
//	maskNoClass			
//	maskDiagnostic		
//	maskDynamic			
//	maskService			
//	maskCorrection		
//	maskComputation		
//	maskInputBlock		
//	maskAnalogOut		
//	maskHART			
//	maskLocalDisplay	
//	maskFrequency		
//	maskDiscrete		
//	maskDevice			
///	maskLocal			
//	maskInput			
//	maskFactory			


//	maskClass_t enumClassFilter;	// Acts as filter, Varaible is shown in corressponding classTab only
//	bool		bAllClass;			// if true Variable is being shown in ALL Class tab

// Added by Deepak : All classes are initialized here
// IMP: Order Of page init should be same as that of AddPage(m_page);

	int nPageCount=0;
			
// This is all classes tab as indicated by bAllClass variable
	SetActivePage(nPageCount++);
	m_pDataList->m_bAllClass =true;					// This is all class tab
	m_Allpage.initPage(m_pDataList);				// pass the db server to the page 
// These  are other classes as defined by HCF
	m_pDataList->m_bAllClass =false;					// Rest of the classes should use filter
													// All the variables having this bit will be shown			
	m_pDataList->enumClassFilter = maskHART;
	SetActivePage(nPageCount++);
	m_CHartClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskDevice;
	SetActivePage(nPageCount++);
	m_CDeviceClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskDynamic;		// Set the filter
	SetActivePage(nPageCount++);
	m_CDynamicClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskLocal;
	SetActivePage(nPageCount++);
	m_CLocalClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskDiagnostic;	
	SetActivePage(nPageCount++);
	m_CDiagnosticClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskCorrection;
    SetActivePage(nPageCount++);
	m_CCorrectionClass.initPage(m_pDataList);


	m_pDataList->enumClassFilter = maskInputBlock;
	SetActivePage(nPageCount++);
	m_CInputBlockClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskInput;
	SetActivePage(nPageCount++);
	m_CInputClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskAnalogOut;
	SetActivePage(nPageCount++);
	m_CAnalogOutputClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskComputation;
    SetActivePage(nPageCount++);
	m_CComputationClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskLocalDisplay;	//? Is filter correct: Suppose to be Logical display
	SetActivePage(nPageCount++);
	m_CLogicalDisplayClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskFrequency;
	SetActivePage(nPageCount++);
	m_CFrequencyClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskDiscrete;
	SetActivePage(nPageCount++);
	m_CDiscreteClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskService;
	SetActivePage(nPageCount++);
	m_CServiceClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskFactory;
	SetActivePage(nPageCount++);
	m_CFactoryClass.initPage(m_pDataList);

	m_pDataList->enumClassFilter = maskNoClass;	// Is this correct filter
	SetActivePage(nPageCount++);
	m_CUnAssignedClass.initPage(m_pDataList);
	
	SetActivePage(0); //All classes page
//END Modification

	SetTimer(12,1000,0);

// add the other sheets here

	// ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);	// enable sys menu and icon
	// set minimal size

//Added by Deepak
	//Read window size & posn data from the xmtr_dd.ini file
	FILE *fwstream = NULL;
	fwstream = fopen("xmtr_dd.ini","r");
	WINDOWPLACEMENT stWINDOWPLACEMENT;
	bool bReadFromFile=false;
	unsigned int uFlags=0;
	unsigned int uShowCmd=0;

	if (fwstream)
	{
		char chComma=',';
		CString strInfo;

		fscanf(fwstream,"%s %d %c %d %c %d %c %d %c %d %c %d %c %d %c %d %c %d %c %d %c %d\n",
				strInfo.GetBuffer (1024)
				,&uFlags
				,&chComma
				,&stWINDOWPLACEMENT.length
				,&chComma
				,&stWINDOWPLACEMENT.ptMaxPosition.x 
				,&chComma
				,&stWINDOWPLACEMENT.ptMaxPosition .y
				,&chComma
				,&stWINDOWPLACEMENT.ptMinPosition .x 
				,&chComma
				,&stWINDOWPLACEMENT.ptMinPosition .y 
				,&chComma
				,&stWINDOWPLACEMENT.rcNormalPosition .bottom 
				,&chComma
				,&stWINDOWPLACEMENT.rcNormalPosition .right 
				,&chComma
				,&stWINDOWPLACEMENT.rcNormalPosition .top 
				,&chComma
				,&stWINDOWPLACEMENT.rcNormalPosition .left 
				,&chComma
				,&uShowCmd
				);
		strInfo.TrimLeft ();
		strInfo.TrimRight ();

		if(strInfo == "WINDOW_INFORMATION")
		{
			bReadFromFile =true;
			stWINDOWPLACEMENT.showCmd =uShowCmd;
			stWINDOWPLACEMENT.flags = uFlags;
		}
		fclose(fwstream);	
	}
	if(bReadFromFile)
		SetWindowPlacement(&stWINDOWPLACEMENT);
	else
	{
		CRect rc;
		GetWindowRect(&rc);
		SetMinTrackSize(CSize(GetMinWidth(), (int)(rc.Height() * 0.5) ));
	}
	// enable save/restore, with active page
// The following line is commented as we dont want to use resizable lib for this
//	EnableSaveRestore(_T("dataPropertySheet"), TRUE, TRUE);
// END modification

// added by deepak
// Create ChkBox

	CRect rcOK,tabRect;
	GetDlgItem(IDOK)->GetWindowRect(&rcOK);
	ScreenToClient(&rcOK);

	//Get button sizes and positions
	GetTabControl()->GetWindowRect(&tabRect);
	ScreenToClient(&tabRect);

	int width = rcOK.Width();
	rcOK.left = tabRect.left;
	rcOK.right =tabRect.left + 2*width;

	m_LocalChkBox.Create (_T("Show Local Variable"),
							WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX|WS_TABSTOP,
							rcOK,
							this, 
							IDC_BNCHECK);
		
	m_LocalChkBox.SetCheck (true);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CdataPropertySheet::OnDestroy() 
{
	WinHelp(0L, HELP_QUIT);

	FILE *stream = NULL;
	stream = fopen("xmtr_dd.ini","w+");
	if (stream)
	{
		WINDOWPLACEMENT stWINDOWPLACEMENT;
		GetWindowPlacement(&stWINDOWPLACEMENT);
		fprintf (stream,"WINDOW_INFORMATION \t %d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
				// stWINDOWPLACEMENT.flags
				(unsigned int)stWINDOWPLACEMENT.flags
				,stWINDOWPLACEMENT.length
				,stWINDOWPLACEMENT.ptMaxPosition.x 
				,stWINDOWPLACEMENT.ptMaxPosition .y
				,stWINDOWPLACEMENT.ptMinPosition .x 
				,stWINDOWPLACEMENT.ptMinPosition .y 
				,stWINDOWPLACEMENT.rcNormalPosition .bottom 
				,stWINDOWPLACEMENT.rcNormalPosition .right 
				,stWINDOWPLACEMENT.rcNormalPosition .top 
				,stWINDOWPLACEMENT.rcNormalPosition .left 
				,(unsigned int)stWINDOWPLACEMENT.showCmd 
				);
		fclose(stream);	
	}

	CResizableSheet::OnDestroy();
	//CPropertySheet::OnDestroy();
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CdataPropertySheet::OnPaint() 
{	
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
		
	}
	else
	{
		CPaintDC dc(this); // device context for painting
	
		// TODO: Add your message handler code here

		// added by deepak  Draw ChkBox
		CRect rcOK,tabRect;
		GetDlgItem(IDOK)->GetWindowRect(&rcOK);
		ScreenToClient(&rcOK);

	//Get button sizes and positions
		GetTabControl()->GetWindowRect(&tabRect);
		ScreenToClient(&tabRect);
		int width = rcOK.Width();
		rcOK.left = tabRect.left;
		rcOK.right =tabRect.left + 2*width;

	// Added By Deepak
		if(GetDlgItem(IDC_BNCHECK)) //END Mod
			m_LocalChkBox.MoveWindow (rcOK);
		
		// Do not call CPropertySheet::OnPaint() for painting messages
		//	CPropertySheet::OnPaint();
	}
	
}

HCURSOR CdataPropertySheet::OnQueryDragIcon() 
{
	
	return (HCURSOR) m_hIcon;	
	// default mfc... return CPropertySheet::OnQueryDragIcon();
}

void CdataPropertySheet::OnSysCommand(UINT nID, LPARAM lParam) 
{
	
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CPropertySheet::OnSysCommand(nID, lParam);
	}
}

// added by deepak
//Check box control handler
void CdataPropertySheet::OnCheckUnCheck()
{
	
	int nActivePage = this->GetActiveIndex ();
	int nPageCount=0;	
	m_pDataList->m_bAllClass =true;						// This is all class tab

	if(m_LocalChkBox.GetCheck ())
		//Show Local var
		m_pDataList->m_bShowLocClass  =true;		// This is for showing local 
	else
		m_pDataList->m_bShowLocClass  =false;		// This is for not showing local 

	BeginWaitCursor ();								//Change the all tabs data
	
	// This is all classes tab as indicated by bAllClass variable
	SetActivePage(nPageCount++);
	m_Allpage.OnShowLocalClass (m_pDataList);			// pass the db server to the page 

	// These  are other classes as defined by HCF
	m_pDataList->m_bAllClass =false;						// Rest of the classes should use filter
														// All the variables having this bit will be shown			
	m_pDataList->enumClassFilter = maskHART;
	m_CHartClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskDevice;
	m_CDeviceClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskDynamic;		// Set the filter
	m_CDynamicClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskLocal;
	m_CLocalClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskDiagnostic;	
	m_CDiagnosticClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskCorrection;
	m_CCorrectionClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskInputBlock;
	m_CInputBlockClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskInput;
	m_CInputClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskAnalogOut;
	m_CAnalogOutputClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskComputation;
	m_CComputationClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskLocalDisplay;	//? Is filter correct: Suppose to be Logical display
	m_CLogicalDisplayClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskFrequency;
	m_CFrequencyClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskDiscrete;
	m_CDiscreteClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskService;
	m_CServiceClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskFactory;
	m_CFactoryClass.OnShowLocalClass(m_pDataList);

	m_pDataList->enumClassFilter = maskNoClass;	// Is this correct filter
	m_CUnAssignedClass.OnShowLocalClass(m_pDataList);
	
	SetActivePage(nActivePage);
	GetActivePage()->Invalidate();

	EndWaitCursor ();
}
	

void CdataPropertySheet::OnTimer(UINT nIDEvent) 
{
	CmethodList* pMlst = NULL;
	methodCallList_t   methLst;
	CValueVarient      retVal;

	// recalculate method list every time... validities may have changed
	if ( m_pDataList != NULL && m_pDataList->pCurDev != NULL)
	{
		pMlst = (CmethodList*) m_pDataList->pCurDev->getListPtr(iT_Method);
		if ( pMlst != NULL )
		{
			if ( pMlst->getBackgroundMethods(methLst) > 0 )
			{
				m_pDataList->pCurDev->executeMethod(methLst, retVal);
			}
		}
	}
	
	CResizableSheet::OnTimer(nIDEvent);
}

/*************************************************************************************************
 *
 *   $History: dataPropertySheet.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */

