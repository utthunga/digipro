/*************************************************************************************************
 *
 * $Workfile: pipeCommX.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the Xmtr class to receive the command / send reply..
 *		The communication interface for xmtr app.
 */
 

#include "stdafx.h"

#include "Hart.h"
#include "XmtrLlc.h"
#include "HartLlcMsg.h"
#include "RecvMsg.h"


#include "pipeCommX.h"

#define MAX_QUEUE_DEPTH	2


DWORD HandlePipe( void* lpvParam ) // param is a ptr 2 a CXmtrpipeComm 
{
   CXmtrpipeComm* pPipeCom = (CXmtrpipeComm*)lpvParam;

   pPipeCom->pipeThread();

   return 0;
}

//*************** NOTE - this class currently does nothing ***********************

CXmtrpipeComm::CXmtrpipeComm()
{
	// can't do much since the infrastructure is not guaranteed till initComm
	cpRcvService = NULL;
	// used as: cpRcvService->serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo);
	// in this app, will trigger a new send for the reply
	pMutexCtrl = pipeMutex.getMutexControl();
}

CXmtrpipeComm::~CXmtrpipeComm()
{
	if (hDieEvent != NULL)
	{// get rid of it
		SetEvent(hDieEvent);
		// we may need to wait for it to finish
		Sleep(100);
		CloseHandle(hDieEvent);
	}
}


// setup connection (called after instantiation of device)
RETURNCODE  CXmtrpipeComm::initComm(void)
{// starts the pipe handler

	// we spin off the thread to handle the comm
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) HandlePipe, // the func
		(LPVOID) (this),			 // pass the comm
		0,						// starts not suspended
		&dwThreadID);			// returns threadId
	if (hThread == NULL)
	{
clog << "Pipe thread creation failed."<<endl;
		return FAILURE;
	}
	else
	{
clog << "Pipe thread creation successful."<<endl;
		return SUCCESS;
	}
}

// gets an empty packet for generating a command
//		(gives caller ownership of the packet*)
hPkt* CXmtrpipeComm::getMTPacket(void) 
{
	clog << "CXmtrpipeComm::getMTPacket called."<<endl;
	return NULL;
}
// returns the packet memory back to the interface 
//		(this takes ownership of the packet*)
void CXmtrpipeComm::putMTPacket(hPkt*)
{
	clog << "CXmtrpipeComm::putMTPacket called."<<endl;
}


// puts a packet into the send queue  (takes ownership of the packet*)
RETURNCODE	CXmtrpipeComm::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	return (SendCmd(pPkt->cmdNum, &(pPkt->theData[0]),pPkt->dataCnt,timeout,cmdStatus) );
}

RETURNCODE	
CXmtrpipeComm::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
	clog << "CXmtrpipeComm::SendCommand called."<<endl;
	return FAILURE;
}

int CXmtrpipeComm::sendQsize(void) 	// how many are in the send queue ( 0 == empty )
{
	clog << "CXmtrpipeComm::sendQsize called."<<endl;
	return (MAX_QUEUE_DEPTH);
}

int	CXmtrpipeComm::sendQmax(void)		// how many will fit
{
	return (MAX_QUEUE_DEPTH);
}


void	CXmtrpipeComm::pipeThread(void)		//the infinite loop
{
   DWORD i, k, dwWait, cbBytes, dwErr; 
   BOOL fSuccess; 
   hPkt* wrtPkt = NULL;
   LPTSTR lpszPipename = PIPENAME; 
//	pMutexCtrl->aquireMutex();
//	pMutexCtrl->relinquishMutex();

	// create pipe(s)
// The initial loop creates several instances of a named pipe 
// along with an event object for each instance.  An 
// overlapped ConnectNamedPipe operation is started for 
// each instance. 
 
	pMutexCtrl->aquireMutex();//+++++++++++++++++++++++++++++++++++++
   for (i = 0; i < INSTANCES; i++) 
   {  // Create an event object for this instance. 
 
      hEvents[i] = CreateEvent( 
         NULL,    // no security attribute 
         TRUE,    // manual-reset event 
         TRUE,    // initial state = signaled 
         NULL);   // unnamed event object 

      if (hEvents[i] == NULL) 
         cerr << "ERROR: Event did not construct."<< endl;
	  else
	  {
clog << "Creating pipe."<<endl;
		  Pipe[i].oOverlap.hEvent = hEvents[i]; 
 
		  Pipe[i].hPipeInst = CreateNamedPipe( 
			 lpszPipename,            // pipe name 
			 PIPE_ACCESS_DUPLEX |     // read/write access 
			 FILE_FLAG_OVERLAPPED,    // overlapped mode 
			 PIPE_TYPE_MESSAGE |      // message-type pipe 
			 PIPE_READMODE_MESSAGE |  // message-read mode 
			 PIPE_WAIT,               // blocking mode 
			 INSTANCES,               // number of instances 
			 PIPEBUFFERSIZE,          // output buffer size 
			 PIPEBUFFERSIZE,          // input buffer size 
			 PIPE_TIMEOUT,            // client time-out 
			 NULL);                   // no security attributes 

		  if (Pipe[i].hPipeInst == INVALID_HANDLE_VALUE) 
		  { 	cerr << "ERROR: Named pipe did not create."<< endl;
				Pipe[i].dwState = ERROR_STATE;
clog << "Pipe creation failed."<<endl;
		  }
		  else
		  {// Call the subroutine to connect to the new client
 
clog << "Pipe ("<<i<<") creation successful.("<< lpszPipename <<")"<<endl;
			  Pipe[i].dwState = ConnectToNewClient( 
				 Pipe[i].hPipeInst, 
				 &Pipe[i].oOverlap); 
			  // Connect & rdidle need no action
			  if (Pipe[i].dwState == ERROR_STATE)
			  {
				  DisconnectAndReconnect(i); // if still in error_state...so be it
			  }
 
		  }// endif
	  }//endif
   } // next

   // setup the die event
   hDieEvent = CreateEvent( 
							 NULL,    // no security attribute 
							 TRUE,    // manual-reset event 
							 FALSE,   // initial state = NOTsignaled 
							 NULL);   // unnamed event object 
   hEvents[INSTANCES]   = hDieEvent;
   hEvents[INSTANCES+1] = pktRcvdQueue.GetArrivalEvent();// we need to write
	pMutexCtrl->relinquishMutex();//--------------------------------------

clog << "Entering infinite loop"<<endl;
   while (1)  /* thread's infinite loop * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   { // wait for a client 

   // Wait for the event object to be signaled, indicating 
   // completion of an overlapped read, write, or 
   // connect operation. (assume only one triggers this return, others will show up after loop)
 
      dwWait = WaitForMultipleObjects( 
         INSTANCES+2,  // number of event objects (+1, die only, +2 die and write queue )
         hEvents,      // array of event objects 
         FALSE,        // does not wait for all 
         INFINITE);    // waits indefinitely 
 
   // dwWait shows which pipe completed the operation. 
 
      i = dwWait - WAIT_OBJECT_0;  // determines which pipe 
	  if (i == (INSTANCES)) // the die scenario
	  {
		  clog << " Pipe Handler is dying." << endl;
		  break; // out of infinite loop
	  }
	  else
	  if (i == (INSTANCES+1)) // a write is ready to go
	  {	  
		  // assume there is only one packet at a time for now(no burst & reply at the same time)
		  wrtPkt = pktRcvdQueue.GetNextObject();
		  if ( wrtPkt == NULL )
		  {
			  cerr << "ERROR: empty transmit queue on transmit event."<<endl;
			  continue; // loop to try again
		  }
		  else
		  {
			  k = wrtPkt->channelID;
			  if (k < 0 || k > (INSTANCES-1) ) 
			  {
				  cerr <<"ERROR: Packet channel out of range (" << k << ")"<<endl;
				  pktRcvdQueue.ReturnMTObject(wrtPkt); 
				  wrtPkt = NULL;
				  continue; // loop to try again
			  }
			  else
			  {// we have a pkt & channel
				  if ( Pipe[k].dwState != READING_STATE)//only valid in the waiting for read state
				  {
					  // programmer error - do not change overlap state
					pktRcvdQueue.ReturnMTObject(wrtPkt); 
					wrtPkt = NULL;
					cerr <<"ERROR: Pipe not idle at Write (" << k 
														<< ", "<< Pipe[k].dwState<< ")"<<endl;
					continue; // loop to try again
				  }
				  else
				  {// cancelio, write, >> readIdle || writeex
						pMutexCtrl->aquireMutex();//+++++++++++++++++++++++++++++++++++++++++++++
						if ( CancelIo(Pipe[k].hPipeInst) ) // success - stops the overlapped Read
						{
							Pipe[k].dwNxtState = sendPkt(k,wrtPkt);
						}
						else
						{// cancelio failed  error
							dwErr = GetLastError();
							cerr << "ERROR: Pipe("<<k<<") failed to cancel io. errCode:"
																					<<dwErr<<endl;
							// don't change overlapped state
							Pipe[k].dwNxtState = CONNECTING_STATE;
						}// end-else cancelio success
						i = k; // for next state processing
						pMutexCtrl->relinquishMutex();//-----------------------------------------
				  }// endelse - currently in the idle-read state
				  // if we got here then we tried to cancelio
			  }// end-else we have a packet and channel
			  // if we get here, we tried to canel io			  
		  }// end-else we have a write packet
		  // if we get here - discard the packet
			pktRcvdQueue.ReturnMTObject(wrtPkt); 
			wrtPkt = NULL;
	  }// endif - the event was a write ready
	  else	  
      if (i < 0 || i > (INSTANCES- 1))// )) 
	  {
         cerr << "ERROR: Pipe channel (from event) out of range.("<<i<<")"<< endl;
		 // don't change overlapped state
		 Pipe[k].dwNxtState = CONNECTING_STATE;
	  }
	  else // not write, event channel valid
	  {// Get the result if the operation was pending. 	
		  
		pMutexCtrl->aquireMutex();//+++++++++++++++++++++++++++++++++++++
 
		 // we're ALWAYS PENDING: except error state
	
		if ( Pipe[i].dwState == ERROR_STATE )
		{// possible channel not connected
			Pipe[i].dwNxtState = Pipe[i].dwState;// do a discon/recon
		}
		else
		{
			fSuccess = GetOverlappedResult( 
				Pipe[i].hPipeInst, // handle to pipe 
				&Pipe[i].oOverlap, // OVERLAPPED structure 
				&cbBytes,          // bytes transferred 
				FALSE);            // do not wait 


			switch (Pipe[i].dwState) 
			{ // Pending connect operation 
			case CONNECTING_STATE: 
				if (! fSuccess) 
				{
				   cerr << "ERROR: Connect to name pipe failed.(" << i << ")" << endl;
					Pipe[i].dwNxtState = ERROR_STATE; // causes a disconnect/reconnect
				}
				else
				{
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			 // Pending read completion operation 
			case READING_STATE: 
				if (! fSuccess || cbBytes == 0) 
				{ 
				   cerr << "ERROR: ReadingEx state failed.("<<i<<")"<< endl;
					Pipe[i].dwNxtState = ERROR_STATE; 
				  //DisconnectAndReconnect(i); 
				  //continue; 
				} 
				else // success
				{
				   // preparse packet process
				   // execute RcvMsg in dispatch
					rcv_Msg(i);
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			 // Pending write completion operation 
			case WRITING_STATE: 
				if (! fSuccess || cbBytes != Pipe[i].cbToWrite) 
				{ 
				   cerr << "ERROR: WritingEx state failed.("<<i<<")"<< endl;
					Pipe[i].dwNxtState = ERROR_STATE; 
				  //DisconnectAndReconnect(i); 
				  //continue; 
				} 
				else // success
				{
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			case IDLE_RD_STATE: // this is a temporary, next state only
				// this occurs when a client is waiting during connect - fall to nxtstate
				Pipe[i].dwNxtState = Pipe[i].dwState;
			break;  // nxt state handler will deal with it

			case ERROR_STATE: // won't happen
			default: 
				// error loop from below
				Pipe[i].dwNxtState = ERROR_STATE; 
			break;
			}  // endswitch
			pMutexCtrl->relinquishMutex();//-----------------------------------------
		}// endelse not in error state
	  }// end-else all event handling

 
	  // HANDLE THE NxtState	    
 
	  switch (Pipe[i].dwNxtState) 
	  { 
	  case CONNECTING_STATE:		/* no-op in nxtState */
		  {
			  ;// no-op
		  }
		  break;
	  case READING_STATE:			/* readingEx -- illegal in next state      */
		  clog << "WARNING: Next State == READING_STATE."<<endl;// fall through
	  case WRITING_STATE:			/* writingEx -- extended             */
		  {
			  Pipe[i].dwState    = Pipe[i].dwNxtState;
			  Pipe[i].dwNxtState = CONNECTING_STATE;
		  }
		  break;

	  case ERROR_STATE:				/* forces disconnect/reconnect in nxtstate */
		  { 
				DisconnectAndReconnect(i); // state to connecting or error or idle read
		  }
		  break;
	  case IDLE_RD_STATE:			/* reading an internal temporary next state */
		  // The pipe instance is connected to the client 
		  // A new overlapped read is needed to pend on another msg. 
		  {
				pMutexCtrl->aquireMutex();//+++++++++++++++++++++++++++++++++++++
			  while (1) // until break
			  {
				fSuccess = ReadFile( 
				   Pipe[i].hPipeInst, 
				   Pipe[i].chBuf, 
				   PIPEBUFFERSIZE, 
				   &cbBytes, 
				   &Pipe[i].oOverlap); 

			 // The read operation completed successfully (msg was waiting).  
				if (fSuccess && cbBytes != 0) 
				{ 
					// preparse packet process
					// execute RcvMsg in dispatch
					rcv_Msg(i);
					continue; // loops through the while() to read any other messages in the pipe
				} 
				else 
				if (! fSuccess  && ((dwErr = GetLastError()) == ERROR_IO_PENDING ) )
				{
					// the normal pending state
					  Pipe[i].dwState    = READING_STATE;
					  Pipe[i].dwNxtState = CONNECTING_STATE;
					  // fall through to break out of read loop and loop to pend on read/write
				}
				else // ! success || bytes == 0
				{
					Pipe[i].dwState    = ERROR_STATE;
					Pipe[i].dwNxtState = ERROR_STATE;
					// fall through to break out of read loop and loop to disconnect/reconnect
				}
				break; // always break unless continue above
			  }// wend
			  
				pMutexCtrl->relinquishMutex();//-----------------------------------------
			}
			break;  
 
         default: 
            cerr << "ERROR: invalid pipe Next state (" << Pipe[i].dwNxtState << ")"<<endl;
		} // end switch  
  } // wend infinity


	// if it closes 
	// disconnect active pipes, close all handles
	for (i = 0; i < INSTANCES; i++)
	{
		if (Pipe[i].dwState != ERROR_STATE)
		{
			DisconnectNamedPipe(Pipe[i].hPipeInst);
			CloseHandle(Pipe[i].hPipeInst);
			CloseHandle(Pipe[i].oOverlap.hEvent);
		}
	}   
	CloseHandle(hDieEvent);
    // pktRcvdQueue.must close its own handles
	// exit thread
clog << "Exiting infinite loop (thread death)."<<endl;
	
}



// DisconnectAndReconnect(DWORD) 
// This function is called when an error occurs or when the client 
// closes its handle to the pipe. Disconnect from this client, then 
// call ConnectNamedPipe to wait for another client to connect. 
 
VOID CXmtrpipeComm::DisconnectAndReconnect(DWORD i) 
{ 
// Disconnect the pipe instance. 
 
	pMutexCtrl->aquireMutex();//+++++++++++++++++++++++++++++++++++++

//	TRACE("PipeHandler's Discon/Recon called.\n");
	clog << "PipeHandler's Discon/Recon called on " << i << " pipe." << endl;
   if (! DisconnectNamedPipe(Pipe[i].hPipeInst) ) 
   {
      cerr << "ERROR: Disconnect failed on pipe " << i <<"."<<endl;
   }
   else
   { // Call a subroutine to connect to the new client. 
 
	   Pipe[i].dwState = ConnectToNewClient( 
		  Pipe[i].hPipeInst, 
		  &Pipe[i].oOverlap); 
 
		clog << "  New Reconnect state: ";
		if ( Pipe[i].dwState == CONNECTING_STATE )
		{
			clog << "CONNECTING_STATE" << endl;
		}
		else
		{
			clog << "IDLE_RD_STATE" << endl;
		}
   }
	pMutexCtrl->relinquishMutex();//--------------------------------------
} 
 
// ConnectToNewClient(HANDLE, LPOVERLAPPED) 
// This function is called to start an overlapped connect operation. 
// It returns a new state:   
// CONNECTING_STATE @ overlapped in progress
// ERROR_STATE      @ error, >>>>  caller must handle this one. < suggest a discon/recon >
// IDLE_RD_STATE    @ client was waiting, we are connected!(the event was set so no action needed)
 
DWORD CXmtrpipeComm::ConnectToNewClient(HANDLE hPipe, LPOVERLAPPED lpo) 
{ 
   BOOL fConnected;
   DWORD exitstate, le; 
 
// Start an overlapped connection for this pipe instance. 
   fConnected = ConnectNamedPipe(hPipe, lpo); 
 
// Overlapped ConnectNamedPipe is zero on success. 
   if (fConnected) 
   {
	  cerr << "ERROR: connect to named pipe succeeded incorrctly." <<endl;
	  exitstate = ERROR_STATE; // will try again later
   }
   else // returned 0
   { 
	   switch ( le = GetLastError()) 
	   { 
	   // The overlapped connection in progress. 
	   case ERROR_IO_PENDING: 
		 exitstate = CONNECTING_STATE; // normal, nobody-connected wait state
		 break; 
		 // client closed it's handle but we haven't disconnected
	   case ERROR_NO_DATA:
		 exitstate = ERROR_STATE;      // force a disconnect/reconnect
		 break; 
 
	   // Client is already connected, so signal an event to force a read.  
	   case ERROR_PIPE_CONNECTED: 
		  {
			 exitstate = IDLE_RD_STATE;
		  }
		  break; 
 
	   // If an error occurs during the connect operation... 
	   default: 
		  {
			cerr <<"ERROR: connect to named pipe with unknown error.("<<le<<")"<<endl; 
			exitstate = ERROR_STATE;
		  }
	   } // endswitch
   }// end if-else connection

   if ( exitstate == IDLE_RD_STATE )
   {// make the handler wakeup and take action
	    if (! (SetEvent(lpo->hEvent)) )
		{
			exitstate = ERROR_STATE;
		}
   }
 
   return exitstate; 
} 


	// generate a real message from the pkt struct
	// normally done further down in the HARTinterface
	// pipes need to do it here
int   CXmtrpipeComm::pkt2Msg(hPkt* pPkt, BYTE* msgBuff)   // pkt to write to xmit byte string
{
	int msgBufSz = 0;


	return msgBufSz;
}

// handles the packet write function for the pipe < was a big section in the state machine above >
// calls pkt2Msg
// returns nxtState
DWORD CXmtrpipeComm::sendPkt(int pipeNum, hPkt* pPkt2Wrt) 
{
	DWORD returnState = IDLE_RD_STATE;// the normal continue state
	DWORD dwErr, cbBytes = 0;
	BOOL  fSuccess;

	// write Pkt to pipe, return pkt2Q
	
	cbBytes = pkt2Msg(pPkt2Wrt, Pipe[pipeNum].chBuf); // generate a message from the packet
	if ( cbBytes ) // success
	{
		Pipe[pipeNum].cbToWrite = cbBytes;

		fSuccess = WriteFile( 
		   Pipe[pipeNum].hPipeInst, 
		   Pipe[pipeNum].chBuf, 
		   Pipe[pipeNum].cbToWrite, 
		   &cbBytes,           // return value
		   &Pipe[pipeNum].oOverlap);  
		if ( fSuccess )
		{
			if (cbBytes == Pipe[pipeNum].cbToWrite)
			{
				STARTLOG << "*Sent packet of "<<cbBytes<<" bytes."<<ENDLOG;
				returnState = IDLE_RD_STATE;// we're done, and successful too
			}
			else // success but incorrectly written - odd
			{
				cerr << "ERROR: success but wanted to write "
									<< Pipe[pipeNum].cbToWrite << "...wrote "<< cbBytes << endl;
				returnState = ERROR_STATE;  // force a disconnect/reconnect
			}
		}
		else // write - not success
		{
			dwErr = GetLastError(); 
			if (dwErr == ERROR_IO_PENDING) 
			{ 
				clog << " WriteEx: want to write "<< Pipe[pipeNum].cbToWrite 
																<< "...wrote "<< cbBytes << endl;
				returnState = WRITING_STATE;// extended write
			} 
			else
			{
				returnState = ERROR_STATE;  // force a disconnect
				cerr<< "ERROR:  Write error: " << dwErr << endl;
			}
		}// endif write success (or not)
	}
	else
	{// pkt 2 msg failed for write -- should never happen 
		// (it'll generate an error msg)
		cerr << "ERROR: message from packet failed!."<<endl;
		returnState = IDLE_RD_STATE;// we cancelled so restart the read pend
	}// end else message generation

	return returnState;
}

// we have a message, 
// preparse to pkt
// call dispatch recieve function
int   CXmtrpipeComm::rcv_Msg(int pipeNum)         // zero == success, doesMsg2Pkt, Pkt to rcv
{
	int rc = 0;// success
	hPkt*     pPkt = NULL;
	cmdInfo_t CommandInfo;  CommandInfo.clear();

	pPkt = pktRcvdQueue.GetEmptyObject();// use same queue for receive packets
	parseMsg2PktNinfo(Pipe[pipeNum].chBuf, Pipe[pipeNum].cbToWrite, pPkt, &CommandInfo);

	if ( cpRcvService != NULL )
	{
		cpRcvService->serviceReceivePacket(pPkt,&CommandInfo);// dispatcher rcv service
		// we are done with the packet, get rid of it
		pktRcvdQueue.ReturnMTObject( pPkt );
		// save the stuff for reply transmit
		lstRcvdCmdInfo = CommandInfo;
	}
	else
	{
		cerr << "ERROR:  received a packet but had no dispatch service to handle it."<<endl;
		rc = 1;//failure
	}
	return rc;
}


int CXmtrpipeComm::parseMsg2PktNinfo(BYTE* msgBuf, DWORD bufLen, hPkt* pPkt, cmdInfo_t* pCmdInfo)
{
	int rc = 0;

CHartLlcMsg* pHartMsg;
CRecvMsg* pRecvMsg = new CRecvMsg;


//	pHartMsg = pRecvMsg->BuildMsg(bTimedOut, (WORD*) &dwBytesRead,
//						pSerial->m_stCommPort.cReadBuffer,
//						pSerial->m_stCommPort.dwCommError);

	bool bTimedOut = false; // no timeout during buffer fill
	pHartMsg = pRecvMsg->BuildMsg(bTimedOut, (WORD*) &bufLen, msgBuf, 0);


	if (pHartMsg) 
	{
	}
	else
	{
	}


	return rc;
}


/*************************************************************************************************
 * NOTES:
 *	This is actually a half duplex channel since a pend may only deal with the last overlapped
 *		request. So:
 *  
 *************************************************************************************************
 */


/*************************************************************************************************
 *
 *   $History: pipeCommX.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/16/03    Time: 12:03p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * I'm changing to xmtrComm because it is now a commInfc 2 XmtrLlc.dll
 * shim class.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */


