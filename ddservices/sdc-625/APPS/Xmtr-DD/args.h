/*************************************************************************************************
 *
 * $Workfile: args.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		sets up the arguments for this particular application
 *	created 2/6/2 from code from the code page - see original 
 *	modified from loader version for dialog display
 *		
 *
 * #include "args.h"
 */

#ifdef ARGUMENTS_H	/* we are included in Arguments.h just for the defines */

#define INI_FILE_SECTION	_T( "Modem Server Preferences" )	/* location in app.ini file ie: [DEFAULT_VALUES] */

#else  /* the rest of the file */

#ifdef INC_DEBUG
#pragma message("In args.h") 
#endif
#include "ddbGeneral.h"
#include "Arguments.h"

#pragma warning (disable : 4786) 
#include <iostream>

using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes::args.h") 
#endif

// default db directory
// in ddbgeneral.h  #define DBDD_PATH   _T("\\hcf\\ddl\\library")      /* default path */

/*************************not a command line option but an environment variable************/
#define FILE_STORE_ENV _T("SDC_FILE_DIR") /* DD File constricts are stored */
/* defaults to databaseDirectory\..\Files\*   **/

// the strings we use in the environment and/or the ini file
// default to _T("") if you want to undefine 'em
#define NOT_SUPPLIED	_T("NoT_SuPpLiEd")
// clog file
#define ARG_STDLOG_INI	_T("")
#define ARG_STDLOG_ENV	_T("XMTRDD_STDLOG_FILE")
#define ARG_STDLOG_DFLT	NOT_SUPPLIED
// cout file
#define ARG_STDOUT_INI	_T("")
#define ARG_STDOUT_ENV	_T("XMTRDD_STDOUT_FILE")
#define ARG_STDOUT_DFLT	NOT_SUPPLIED
// cerr file
#define ARG_STDERR_INI	_T("")
#define ARG_STDERR_ENV	_T("XMTRDD_STDERR_FILE")
#define ARG_STDERR_DFLT	NOT_SUPPLIED
// DD release Directory
#define ARG_RELEASE_INI		_T("RELEASE_DIR")			/* not defined now */
#define ARG_RELEASE_ENV		_T("TOK_RELEASE_DIR")
#ifdef _UNICODE
#define ARG_RELEASE_DFLT	DBDD__PATH
#else
#define ARG_RELEASE_DFLT	DBDD_PATH
#endif
// comm log file name 
#define ARG_COMLOG_INI		_T("COMLOGIN")
#define ARG_COMLOG_ENV      _T("XMTRDD_COMLOGIN")
#define ARG_COMLOG_DFLT     _T(".\\CommLog.txt")
/*  Not yet implemented */
// url 
#define ARG_COMM_INI		_T("XmtrDDSerialPort")			/* not defined now */
#define ARG_COMM_ENV		_T("XMTRDD_COMMPORT")
#define ARG_COMM_DFLT		_T("COM1")
// language
#define ARG_LANG_INI		_T("LANGUAGE_CODE")			/* not defined now */
#define ARG_LANG_ENV		_T("XMTRDD_LANG_CODE")
#define ARG_LANG_DFLT		_T("|en|")
// logging permissions
#define ARG_LOGGING_INI     _T("LOGGING_LEVEL")	
#define ARG_LOGGING_ENV     _T("SDC_LOGGING_LEV")
#define ARG_LOGGING_DFLT    _T("0x00000000")
// Q&A
#define ARG_QA_INI			_T("QA_LEVEL")	
#define ARG_QA_ENV			_T("QA_LEV")
#define ARG_QA_DFLT			_T("0x00000000")


/********************* used ****************
ABCDEFGHILMOPRSTUV          SDC sargs.h holds the definitive version of this
********************used in xmtr************
KYQ
*********************available**************
JNWXZ
********************************************/


class DDSargs : public Arguments
{
public:
	DDSargs( tstring strCommandName, tstring strDescription=_T(""), tstring strOptionmarkers=_T("-"))
										:Arguments( strCommandName, strDescription, strOptionmarkers )
	{	// Add option "-h" with explanation...
		AddOption(_T("h|H|?"), _T("display usage help"), true);		

		// Add option "-G" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("G|g"), _T("file spec for the logging information") );
			cOpt.AddRequiredArgument(	_T("logFileName"),				// index name
							_T("log file name with optional path\n\t\t\tNOTE: -G option is for testing only"),
										ARG_STDLOG_INI,					// ini
										ARG_STDLOG_ENV,					// env
										ARG_STDLOG_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-o" with non optional parameter
		{
			Arguments::Option	cOpt( _T("O|0|o"), _T("output filespec") );
			cOpt.AddRequiredArgument(	_T("outputFileName"),				// index name
										_T("file name with optional path"), // description (help)
										ARG_STDOUT_INI,					// ini
										ARG_STDOUT_ENV,					// env
										ARG_STDOUT_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-E" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("E|e"), _T("file spec for the error information") );
			cOpt.AddRequiredArgument(	_T("errorFileName"),				// index name
						_T("error log file name with optional path\n\t\t\tNOTE: -E option is for testing only"),
										ARG_STDERR_INI,					// ini
										ARG_STDERR_ENV,					// env
										ARG_STDERR_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-B" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("B|b"), _T("Device Library directory") );
			cOpt.AddRequiredArgument(	_T("databaseDirectory"),			// index name
						_T("the 'Release' directory (with dictionaries)"),	// help
										ARG_RELEASE_INI,				// ini
										ARG_RELEASE_ENV,				// env
										ARG_RELEASE_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}		
		// Add option "-U" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("U|u"), _T("communication port") );
			cOpt.AddRequiredArgument(	_T("CommPort"),					// index name
								_T("communications port ie.COM1|COM2|COMx"),// help
										ARG_COMM_INI,					// ini
										ARG_COMM_ENV,					// env
										ARG_COMM_DFLT );					// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-l" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("L|l|1"), _T("language") );
			cOpt.AddRequiredArgument(	_T("langCode"),			// index name
				_T("ISO 639 specified language descriptor string"),	// help
										ARG_LANG_INI,		// ini
										ARG_LANG_ENV,		// env
										ARG_LANG_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		}	
		// Add option "-K" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("K|k"), _T("file spec for a commLog input") );
			cOpt.AddRequiredArgument(	_T("commLogFileName"),				// index name
						_T("comm log file name with optional path\n"),
										ARG_COMLOG_INI,					// ini
										ARG_COMLOG_ENV,					// env
										ARG_COMLOG_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}	
		// Add option "-T" with a non optional parameter...---undocumented HART logging
		{
			Arguments::Option	cOpt( _T("T|t"), _T("Log Permission") );
			cOpt.AddOptionalArgument(	_T("Log_Permission"),		// index name
						_T("Bit-Enum for Logging Level (use 0x prefix) "),	// help
										ARG_LOGGING_INI,		// ini
		/*MUST use 0x...*/				ARG_LOGGING_ENV,		// env
										ARG_LOGGING_DFLT );	   // default value if not in ini||env
			AddOption( cOpt );
		}


		// Add option "-Y" with no parameters -does an aC dump and exits the program
		{
			AddOption(_T("Y|y"), _T("generate Dump if debug build"), false);	
		}
		// Add option "-Q" with a non optional parameter...---undocumented HART V&V work
		{
			Arguments::Option	cOpt( _T("Q|q"), _T("Q&A operation during a -Y dump") );
			cOpt.AddOptionalArgument(	_T("QA options"),		// index name
				_T("Bit-Enum for QA options (use 0x prefix) "),	// help
										ARG_QA_INI,		// ini
				/*MUST use 0x...*/		ARG_QA_ENV,		// env
										ARG_QA_DFLT );	   // default value if not in ini||env
			AddOption( cOpt );	
			// was AddOption(_T("Q|q"), _T("Q&A operation during a -Y dump"), false);	
		}

		// the following are filled in the order given //
		AddOptionalArgument(_T("MfgID"),  _T("DD's hex Mfg_ID  to simulate (e.g. EF)"),
									_T(""),			// ini
									_T(""),			// env
									NOT_SUPPLIED );	// default value if not in ini||env
		AddOptionalArgument( _T("DevType"),	_T("DD's hex DevType to simulate (e.g. EC)"),
									_T(""),			// ini
									_T(""),			// env
									NOT_SUPPLIED );	// default value if not in ini||env
		AddOptionalArgument( _T("DevRev"),	_T("DD's hex Device_Revision to simulate (e.g. 01)"),
									_T(""),			// ini
									_T(""),			// env
									NOT_SUPPLIED );	// default value if not in ini||env
		// if it exists and has the previous, it may include the following
		AddOptionalArgument( _T("DDRev"),	_T("DD's hex DD_Revision to simulate (e.g. 02)"),
									_T(""),			// ini
									_T(""),			// env
									_T("ff") );	// default value if not in ini||env - search for latest

/* replaced		
#ifdef REQUIREDDNAME
		// add non optional arguments "inspec"...
		AddArgument( "mfgID",  "DD to read (ie.Mfg_ID)" );
		AddArgument( "devType","DD to read (ie.DevType)" );
		AddArgument( "DevRev", "DD to read (ie.Device_Revision)" );
		AddArgument( "ddRev",  "DD to read (ie.DD_Revision)" );	
#else.
		AddOption("mfgID",  "DD to read (ie.Mfg_ID)", false);	

		AddOption( "devType","DD to read (ie.DevType)", false );
		AddOption( "DevRev", "DD to read (ie.Device_Revision)", false );
		AddOption( "ddRev",  "DD to read (ie.DD_Revision)", false );	
#endif
end replaced */
	};

};

#endif  /* ARGUMENTS_H */

/*************************************************************************************************
 *
 *   $History: args.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:40a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
