/*************************************************************************************************
 *
 * $Workfile: XmtrComm.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		This is the class to the send command / receive command..
 *	This class is now a shim between the comm interface and the XmtrLlc.dll interface.
 *	The XmtrPipe library mimics this interface for pipe operations.  It is designed to
 *	allow drop-in replacement with the original XmtrLlc.dll when _USEXMTR_DLL is defined
 *	and the .DLL is supplied.
 *
 * #include "XmtrComm.h"
 */



#ifndef _XMTRCOMM_H
#define _XMTRCOMM_H
#ifdef INC_DEBUG
#pragma message("In XmtrComm.h") 
#endif
;;;
// in lieu of stdafx.h

//#include <windows.h> 

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include "ddbCommInfc.h"
#include "ddbQueue.h"

#include "NamedPipe.h"
#include "WndMsgRcvr.h"
#include "Client.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes::XmtrComm.h") 
#endif

class hCddbDevice;

#define INI_MAC  L"new_name.ini"	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

class xPkt : public hCqueue<hPkt>
{
protected:
	void EmptyItem(hPkt* pItem) { pItem->clear();};
	void InitItem( hPkt* pItem) { pItem->clear();};
};

// the interface (supplied by the application) to the i/o channel to the device
class CXmtrComm : public hCcommInfc
{
//	hCqueue<hPkt> pktXmitQueue;    // instantiates with dflt size// write queue
	xPkt          pktXmitQueue;
	CWndMsgRcvr   msgRcvr;         // we gotta have something that rcvs windows msgs from dll
	HANDLE        hRegisteredTask; // the write translate task handle
	hCddbDevice*  pCurDev;

	CClient*      pCommClient;
	tstring        commloginputname;
	string   readInString(FILE *pF);
	int      string2Pkt(hPkt* pPkt, string& str);// returns byte count and packet filled
	bool     ourAddr(CHartMsg* pHrtMsg);
	CValueVarient getValue(unsigned symNum);


	int           portNumber; // 0 == use the pipe

public:	
	CXmtrComm(char* pCom = NULL); // CXmtrComm();
	
	virtual ~CXmtrComm();
	
	void readInCommLog(void);//stevev 17may07- special simulator function

	void  notifyAppVarChange(itemID_t changedItemNumber,NUA_t isChanged = NO_change, long aTyp = 0);//{/*unused*/};

	//use base class's::> void  setRcvService(hCsvcRcvPkt* cpRcvSvc);

	hPkt*       getMTPacket(bool pendOnMT);	// gets an empty packet for generating a command 
								//		(gives caller ownership of the packet*)
	void        putMTPacket(hPkt*);	// returns the packet memory back to the interface 
								//		(this takes ownership of the packet*)<used for cmd abort>
	RETURNCODE	SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
	RETURNCODE	SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);
					// puts a packet into the send queue  (takes ownership of the packet*)

	int         sendQsize(void); 	// how many are in the send queue ( 0 == empty )
	int			sendQmax(void);		// how many will fit
	RETURNCODE  initComm(void);		// setup connection (called after instantiation of device)
	RETURNCODE  shutdownComm(void){/*empty for now*/ return SUCCESS;};
	void  disableAppCommRequests(void) {};
	void  enableAppCommRequests (void) {};
	bool  appCommEnabled        (void) {return true;}; 

	// xmtr specific stuff
	void        sendXlateTask(void);// runs in its own background task 
									// event & pkt from Q 
									// to client reply string and sendevent in DLL (or pipe)
	void        msgToReceive(void); // run from CWndMsgRcvr  in its task time
	int         parseMsg2PktNinfo(CHartMsg* pHmsg, hPkt* pPkt, cmdInfo_t* pCI);
	RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr) ;

	// we may want to add this to the standard interface
	void sendErrorPkt(hPkt* pReqPkt, response5Code_t rc,UINT8 rStat);


/*Vibhor 220104:Start of Code*/

	RETURNCODE 
			SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
	
/*Vibhor 220104:End of Code*/
	// xmtr specific
	void setDev(hCddbDevice* pD) { pCurDev = pD;};
/*Vibhor 290104: Start of Code*/

	 void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);
	 short setNumberOfRetries( short newVal ) {return 0;};// returns previous val 

};

#endif //_XMTRCOMM_H


/*************************************************************************************************
 *
 *   $History: XmtrCommX.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/16/03    Time: 12:04p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Changing to different name - see the cpp.
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/08/03    Time: 7:53a
 * Created in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 *************************************************************************************************
 */
