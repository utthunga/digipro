/*************************************************************************************************
 *
 * $Workfile: xMethodSupport.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		Does the non-interface method stuff for Xmtr
 *
 *
 *		
 * #include "xMethodSupport.h" 
 */

#ifndef _XMETHODSUPPORT_H
#define _XMETHODSUPPORT_H

#pragma warning (disable : 4786) 

#include < basetsd.h >
#include < string >
#include < vector >
#include < deque >

using namespace std;

#include "ddbMethSupportInfc.h"
#include "methodinterfacedefs.h"
#include "ddbMethod.h"

class hCddbDevice;

class CMethodSupport : public hCMethSupport
{
private:

		//CMethodDlg* m_pMethodDlg;
		hCmethod*   m_pddbMeth;
		hCmethodCall* pCurrentMethodCall;
		
		//this is set to TRUE if the method is not invoked from the UI by the user. 
		BOOL m_bInvokedForNonUIAction;

		deque<hCmethodCall*> methodStack;
		int   clientCount;

	void pushMethod(hCmethodCall* newMeth);// adds a new current
	hCmethodCall* popMethod(); // removes and returns current
	int  stackSize() { return methodStack.size();};// test 4 stack errors
	HWND m_hwndMain;

public:	
	CMethodSupport();

	~CMethodSupport();

	//attributes
	hCddbDevice* pDev;// stevev - 25apr07 
	MEE*         pMEE;// stevev - 25apr07 

	//methods
	HWND getBaseHwnd();
	hCmethodCall* currentMethod();// sees current

		//function to be implemented in derived class that handles initializations
#ifdef _DEBUG
	void PreExecute(hCmethodCall& mc);
	//function to be implemented in derived class that handles cleanup to be done
	void PostExecute(hCmethodCall& mc);
#else
	void PreExecute();
	void PostExecute();
#endif
	
	//function that calls the method execute function
	RETURNCODE DoMethodSupportExecute(hCmethodCall& MethCall);


	bool hasClients(void) { return (clientCount != 0); };

	//function to be implemented in derived class that handles the display of methods UI 
	bool MethodDisplay(ACTION_UI_DATA structMethodsUIData,ACTION_USER_INPUT_DATA& structUserInputData);

	// function to be implemented in derived class that returns the number of methods currently running
	int  numberMethodsRunning(); // should check that the number is correct as well

	// should be unused in xmtr-dd
	bool UIimageSize(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline) { return false; };

	//   string ptr, UI extracts the string(it knows what lang), converts lines X chars then to rows X cols & returns
	bool UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit, bool isDialog = false);


};

#endif //_XMETHODSUPPORT_H