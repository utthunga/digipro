/*************************************************************************************************
 *
 * $Workfile: DDlistData.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 This is the Xmtr-DD specific listData implementation for the chklistctrl
 *		12/05/02   sjv    merged datalist and frameview to get list data handler
 */

#include "stdafx.h"

#include <ios>
using namespace std;

#include <stdio.h>

#include <afxtempl.h>
#pragma warning (disable : 4786) 

#include "args.h"

#include "ddb_XmtrDispatch.h"
#include "ddbDevice.h"
#include "ddbMethSupportInfc.h"

#include "FileSupport.h"

#include "chkListEdit.h "
#include "DDlistData.h" // includes "ListData.h"
#include "EditCell.h"

#include "XmtrDD.h" // the winapp with commandline info
#include "ddb.h"

#include "DDselect.h"

#include "ddbCommInfc.h"

#include "panic.h"

//#include "ddbItemLists.h"
//#include "ddbVarList.h"

//					#include "pipeCommX.h"
#include "WndMsgRcvr.h"
#include "XmtrComm.h"
#include "xMethodSupport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

//#define STARTTIME_CHECK
#endif


#ifdef STARTTIME_CHECK
DWORD startOfInstantiation = 0;
DWORD lastTickCnt = 0, thisCnt = 0;
#endif

#ifdef INC_DEBUG
#pragma message("Finished Includes::DDlistData.cpp") 
#endif
#pragma warning (disable : 4786) 

#define NUM_COLUMNS	6
#define NUM_SAMPLEROWS 2


static _TCHAR *_gstItem[NUM_SAMPLEROWS][NUM_COLUMNS]=
{
	_T("Sym-Nm 001"),  _T("24576001"), _T("Label - 001"), _T("Subclass - 001"),   _T("1"),  _T(""),
	_T("Sym-Nm 002"),  _T("24576002"), _T("Label - 002"), _T("Subclass - 002"),   _T("2"),  _T("")
};

extern bool DumpOnly; // in XmtrDD.cpp

/////////////////////////////////////////////////////////////////////////////
// Column Description
// Changed By Deepak
// Column Name changed from Other to Extended Info
static _TCHAR *_gstColumnLabel[NUM_COLUMNS]=
{
	_T("Symbol Name"), _T("Symbol-Number"),   _T("Label"),  _T("Class"),  _T("Value"), /*_T("Other")*/_T("Extended-Info")
};

// in the .h file::> #define COLSTRLEN	16

static int _gstColumnFmt[NUM_COLUMNS]=
{
       LVCFMT_LEFT,     LVCFMT_CENTER,      LVCFMT_LEFT,    LVCFMT_LEFT,   LVCFMT_LEFT,    LVCFMT_CENTER
};

static int _gstColumnWidth[NUM_COLUMNS]=
{
	      150,                  100,               150,          100,             250,             80
};

/////////////////////////////////////////////////////////////////////////////
// CDDlistData

//xxIMPLEMENT_DYNCREATE(CDDlistData, CChkListView)

CDDlistData::CDDlistData(DD_Key_t dbKey)
{
	NumColumns  = NUM_COLUMNS;
	activeDDkey = dbKey;
	DDAvailable = FALSE;
//	pVarItems   = NULL;
// OLD	pDevice     = NULL;
	pDevMgr     = NULL;
	pCurDev     = NULL;

// Added BY deepak for implementing the Show local variable option
	m_bShowLocClass =true;
//END MOD
	entryIdentity.clear(); // stevev 14feb07
//xx	CurColType = Undefined;
//	m_pDataList = new CFrameDataList;
// the document doesn't exist at this point, we have to wait for on initial update
//	CAfileReaderDoc * p_FRD = (CAfileReaderDoc *)GetDocument();
//	if (p_FRD)
//	{
//		p_FRD-> p_FrameView = this;
//	}// else we don't have a document and we're in trouble
//	p_MyListDataSet = m_pDataList;
}

// added 14feb07 to help support 16 bit hart 7
CDDlistData::CDDlistData(Indentity_t iD,DD_Key_t dbKey)
{
	NumColumns  = NUM_COLUMNS;
	activeDDkey = dbKey;
	DDAvailable = FALSE;
	pDevMgr     = NULL;
	pCurDev     = NULL;
	m_bShowLocClass =true;
	entryIdentity = iD;
}

CDDlistData::~CDDlistData()
{
//	if (m_pDataList) delete m_pDataList;
//?	if (dataPtrCache.size() > 0 )
//?	{
//?		aCvarType* pCV;
//?		for (vector<aCvarType*>::iterator iT = dataPtrCache.begin(); iT < dataPtrCache.end();iT++)
//?		{// iT is ptr 2 a aCvarType*
//?			if ( iT != NULL )
//?			{
//?				pCV = *iT;
//?				delete pCV;
//?			}
//?		}
//?		dataPtrCache.clear();
//?	}
	
	// ************************************ save the init values for the next initialization
	if (pDevMgr != NULL)
	{
		if (pCurDev != NULL)
		{
			hCdeviceSrvc* pDevSvc = (hCdeviceSrvc*)pCurDev;
			pDevMgr->deleteDevice(&pDevSvc);
			
		}
//		delete pDevMgr;
//		pDevMgr = NULL;
	}
}

/*********************
BEGIN_MESSAGE_MAP(CDDlistData, CChkListView)
	//{{AFX_MSG_MAP(CDDlistData)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(IDM_FIXCHKSUM, OnFixchksum)
	ON_COMMAND(IDM_FIXTIME, OnFixtime)
	ON_UPDATE_COMMAND_UI(IDM_FIXCHKSUM, OnUpdateFixchksum)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
**********************/

/////////////////////////////////////////////////////////////////////////////
// CDDlistData data handling

int	CDDlistData::GetSize(void)
{// return the number of rows to be on the display
	return 0;
}

int	CDDlistData::FillColumnHead (CListCtrl * p_ListControl)
{// this might should be elsewhere	
	sColDesc oneCol;
	LVCOLUMN aColumn;
	int i;

	for ( i = 0; i < NumColumns; i++)
	{
		GetColDesc(i, oneCol);

		//int InsertColumn( int nCol, const LVCOLUMN* pColumn );
		aColumn.cx = oneCol.ColWidth;            aColumn.mask  = LVCF_WIDTH;  // column width (pixels)
		aColumn.pszText = &(oneCol.ColLabel[0]); aColumn.mask |= LVCF_TEXT;  // column name
		aColumn.cchTextMax = _tcslen(oneCol.ColLabel) + 1;
		aColumn.fmt  = oneCol.ColFmt/*LVCFMT_CENTER*/; aColumn.mask |= LVCF_FMT;   // formating
		aColumn.iSubItem = 0;
		p_ListControl->InsertColumn( i, &aColumn );
	}
	return 0;
}


/***
 * FillItems
 *	Inserts a row into the display for each variable in the list
 *	Sets it to TEXTCALLBACK so that GetDispTest will handle the data access
 ***/
int	CDDlistData::FillItems(CListCtrl& ListCtrl, CArray<int,int> &MyView)
{
//SUBLISTDATATYPE tempDataElement;

	LV_ITEM lvi;
	int cnt = 0;
	if (pCurDev == NULL) { return 0;}


	CitemType varType(iT_Variable);
	CVarList* pVarPtrList = (CVarList*)(pCurDev->getListPtr(varType));
	if ( pVarPtrList == NULL )
	{
		return 0; // none done
	}

// Added BY deepak for implementing the Show local variable option
	ListCtrl.DeleteAllItems ();
//END mod

// Added By deepak : Clear MyView Array	
	if ( MyView.GetSize() > 0)
	{
		MyView.RemoveAll(); // clears the list
	}

	
/*	for ( int i = 0; i < pVarPtrList->size(); i++, cnt++ )*/

	int i = 0;	// This variable will hold the row number corresspnding to a view
				// cnt will have the current index of RowList 
				// Cnt is stored in MyView
	for (CVarList::iterator iT = pVarPtrList->begin(); iT < pVarPtrList->end();cnt++, iT++)
	{
		hCVar *pCVar = (*iT);

	// We need class bit string 	
		UINT64 lClassBitString=0;
		hCattrClass* pCattr=NULL;
		pCattr= pCVar->getClassAttr();
		
		if (pCattr)
		{
			hCbitString* pClassBitStr = (hCbitString*)(pCattr->procure());
			if (pClassBitStr != NULL)
			{
				lClassBitString = pClassBitStr->getBitStr();
			}
			else
			{
				cerr << "ERROR: could not procure class string for display of item 0x"
					<<hex << pCVar->getID()<<dec<<endl;
				lClassBitString = 0x0000;
			}
		}

//	enumClassFilter;	// Acts as filter, Varaible is shown in corressponding classTab only
//	bAllClass;			// if true Variable is being shown in ALL Class tab

		ulong uResult=0;
		uResult = enumClassFilter & lClassBitString;

// Added BY deepak for implementing the Show local variable option
		bool bLocal =false;
		if(maskLocal & lClassBitString)			// Determine if variable is local or not
			bLocal =true;

// Implement the Truth Table
//	   --------------------------------
//	   |bShowLocClass |bLocal | result |
//     |     0        |  0    |    1   |
//     |     0        |  1    |    0   |   // Dont show if its local
//     |     1        |  0    |    1   |
//     |     1        |  1    |    1   |  
//      --------------------------------
//     This is exp need to implement 
//     ______ 
//     bLocal  + bShowLocClass && bLocal
		if((m_bShowLocClass && bLocal) || (!bLocal))			// If Dont show local dlags are set then
		{													//check the current items class
			if (m_bAllClass || uResult)						// UI filter to show the variable in 
			{												// corressponding class tab	
				  // We satisfy class Filter Criteria 
				lvi.mask     =LVIF_TEXT /*| LVIF_IMAGE*/ | LVIF_STATE | LVIF_PARAM;
				lvi.iItem    =i;
				lvi.iSubItem =0;
				lvi.pszText  =LPSTR_TEXTCALLBACK;
				lvi.iImage   =i;
				lvi.stateMask=LVIS_STATEIMAGEMASK;
				lvi.state    =INDEXTOSTATEIMAGEMASK(1); 
				//(QueryValidity(i)) 
				//			? INDEXTOSTATEIMAGEMASK(1) : 0x0000;// ok, 0 is boxed
				lvi.lParam   = (LPARAM) cnt;

				MyView.Add(cnt);			// cnt is data index
				ListCtrl.InsertItem(&lvi);								
				i++;
			}
		}
// END MOD Show local variable option
		
//tempDataElement.elementValue = 1 << i;
//testDataStore.Add(tempDataElement);
	}

	return cnt;

/**
	for (AitemList_t::iterator iT = pVarItems->begin(); iT < pVarItems->end();iT++)
	{// iT is ptr 2 a aCitemBase*
		for (AattributeList_t::iterator ipT = iT->attrLst.begin(); ipT < iT->attrLst.end();ipT++)
		{// ipT is ptr 2 a aCattrBase*  (ptr2ptr)
			
			  // process here

  we proly want to put 'em into some kinda class with variable values
  see hCVar in the library
		}
	}
**/
}


/****
 *  FillVarsFromINI
 *		initializes the variable list from the INI file
 *      returns the number of vars filled from the file
 ****/
int	CDDlistData::FillVarsFromINI(FILE *stream)
{
	int rc = 0; 
	int lDevId, lVarType;
	CitemType varType(iT_Variable);
	CVarList* pVarPtrList = (CVarList*)pCurDev->getListPtr(varType);
	hCVar *pCVar = NULL;
	char     chComma;
	bool     notFnd;
	tstring   str;
	CString  strVar, strName, strDispVal;
	hCEnum       *pEnum = NULL;
	hCBitEnum *pBitEnum = NULL;
	INSTANCE_DATA_STATE_T tmpState = IDS_CACHED;
	unsigned long  symnum = 0;

	if ( pVarPtrList == NULL )
	{
		LOGIT(CERR_LOG,"ERROR: No log file.\n");
		return 0;//failure - no device
	}
	if ( stream == NULL )
	{
		CValueVarient v;
		pCVar = pCurDev->getVarPtrBySymNumber(SYM_MFG_ID);
		if ( pCVar != NULL )
		{	v = (int)((activeDDkey >> 48/*24*/) & 0xffff);
			pCVar->setDispValue(v);
			pCVar->markItemState(IDS_CACHED);
			
			pCVar->ApplyIt();	//To prevent Yellow cells when UI comes up
		}
		pCVar = pCurDev->getVarPtrBySymNumber(SYM_DEVTYP);
		if ( pCVar != NULL )
		{	v = (int)((activeDDkey >> 32/*16*/) & 0xffff);
			pCVar->setDispValue(v);
			pCVar->markItemState(IDS_CACHED);
			
			pCVar->ApplyIt();	//To prevent Yellow cells when UI comes up
		}
		pCVar = pCurDev->getVarPtrBySymNumber(SYM_DEVREV);
		if ( pCVar != NULL )
		{	v = (int)((activeDDkey >> 16/*8*/) & 0xff);
			pCVar->setDispValue(v);
			pCVar->markItemState(IDS_CACHED);
			
			pCVar->ApplyIt();	//To prevent Yellow cells when UI comes up
		}
		return 0;
	}
	char  inchar[256];
	char  inVal[1048];

	while (// name, symbolNum,Type,Value
		fscanf(stream, "%s %c %d %c %d %c %[^\n]",inchar/*strName.GetBuffer(1024)*/,&chComma,
											 &lDevId,                 &chComma,    
											 &lVarType,               &chComma,
											 inVal/*strDispVal.GetBuffer(1024)*/)
		> 0 )
	{// for each line in the .ini file
		notFnd   = true;
		string   tStr,instr,invalstr;
		instr    = inchar;
		invalstr = inVal;
		wstring  daVal;
		str      = _T("");
	// this gives utf8????	daVal = AStr2TStr(invalstr);
		daVal = UTF82Unicode(invalstr.c_str());
		for (CVarList::iterator iT = pVarPtrList->begin(); 
				iT < pVarPtrList->end() && notFnd ; iT++)
		{// iT is ptr 2 ptr  2 a hCVar class
			pCVar = (hCVar *)(*iT);
			// If the record is same as that read from file
			// Compare the names instead of device ID  as ID may change bec of Tokenzier
			
			// stevev 28apr08 strName.TrimLeft();
			// stevev 28apr08 strName.TrimRight();

			//CString strVar =(pCVar->getName()).c_str();
			// stevev 28apr08 tStr = (pCVar->getName().c_str());
			tStr = pCVar->getName();
			// move out of loop...CString strVar(tStr.c_str());
			// stevev 28apr08 strVar = tStr.c_str();

			// stevev 28apr08 strVar.TrimLeft();
			// stevev 28apr08 strVar.TrimRight();
			// stevev 28apr08 if(strName == strVar)
			symnum = pCVar->getID();
			if (tStr == instr)
			{
				rc++;
				notFnd = false;
				switch(pCVar->VariableType())
				{
				case  vT_Enumerated:			// 6
					pEnum = (hCEnum*) (*iT);
					//	RETURNCODE procureValue (const string& keyStr, UINT32& value);// input string, get a value

					if(pEnum) 
					{
						// 28apr08 if(strDispVal == M_UI_XMTR_NO_NUMBER)// from file
						if(daVal == M_UI_XMTR_NO_NUMBER)// from file
						{// should never happen
							str =M_UI_MT_STR;
							tmpState = IDS_INVALID;
						}
						else
						{
							str      = (LPCTSTR)strDispVal;
							str      = daVal.c_str();// force a copy
							tmpState = IDS_CACHED;
						}						
					}
					break;

				case  vT_BitEnumerated:			// 7
					pBitEnum = (hCBitEnum*) (*iT);
					if(pBitEnum )
					{
						// 28apr08 if(strDispVal == M_UI_XMTR_NO_NUMBER)
						if(daVal == M_UI_XMTR_NO_NUMBER)
						{// should never happen
							str =M_UI_MT_STR;
							tmpState = IDS_INVALID;
						}
						else
						{
							// 28apr08 str= (LPCTSTR)strDispVal;
							str= daVal.c_str();
							tmpState = IDS_CACHED;
						}
					}
					break;
				case  vT_Integer:				// 2
				case  vT_Unsigned:				// 3
				case  vT_FloatgPt:				// 4
				case  vT_Double:				// 5
				case  vT_Index:					// 8
				case  vT_Ascii:					// 9
				case  vT_PackedAscii:			//10 - HART only
				case  vT_Password:				//11
				case  vT_BitString:				//12
				case  vT_HartDate:				//13 - HART only
				case  vT_Time:					//14
				case  vT_DateAndTime:			//15
				case  vT_Duration:				//16
				case  vT_EUC:					//17
				case  vT_OctetString:			//18
				case  vT_VisibleString:			//19
				case  vT_TimeValue:				//20
				case  vT_Boolean:				//21
				case  vT_MaxType:				//  22
					//stevev 28apr08 if(strDispVal == M_UI_XMTR_NO_NUMBER)
					if(daVal == M_UI_XMTR_NO_NUMBER)
					{// should never occur
						str =M_UI_MT_STR;
						tmpState = IDS_INVALID;
					}
					else
					{
						//stevev 28apr08 str= (LPCTSTR) strDispVal;
						str= daVal;
						tmpState = IDS_CACHED;
					}
					break;
				default:
					break;//should not reach this					  
				};
				pCVar->setDisplayValueString(str);
				pCVar->markItemState(tmpState);
				
				pCVar->ApplyIt();	//To prevent Yellow cells when UI comes up
					// ************************************set init value in iT->
			} 
			//else - skip this one and go to the var
		} // FOR each variable (linear search)
		// Read data from INI file
		if(feof(stream))
			break;// out of while
	//	Clear the previous read Values
		strName.ReleaseBuffer();
		strName.Empty();
		strDispVal.ReleaseBuffer ();
		strDispVal.Empty();
		// fscanf(stream, "%s %c %d %c %d %c %s",strName.GetBuffer(1024),&chComma,&lDevId,&chComma,&lVarType,&chComma,strDispVal.GetBuffer(1024));
	} // END of While wend

	return rc;
}

int CDDlistData::FillINIfromVars(FILE *stream)
{
	RETURNCODE rc = 0;

	if(stream == NULL )
	{
		return 0;// failure
	}
	char cEOL ='\n';
	char cComma = ',';
  
	int    cnt = 0, unk = 0;
	hCVar* pIm = NULL;	

	hCEnum       *pEnum = NULL;
	hCBitEnum *pBitEnum = NULL;
	// stevev 28apr08
	// make this file ascii....CString strName, strDispVal;
	string strName, strDispVal;
	UINT uValue;
	CValueVarient vValue;
	
	// stevev 28apr08 strName.Empty ();
	// stevev 28apr08 strDispVal.Empty ();
#ifdef _DEBUG
	CVarList* pVL = (CVarList*)pCurDev->getListPtr(iT_Variable);
	if ( pVL && pVL->size() != RowList.GetSize() )
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: size mismatch Variables: %d, Display: %d\n",pVL->size(),RowList.GetSize());
	}
#endif

	for (int i = 0; i < RowList.GetSize(); i++)
	{// ptr 2 a hCVar class
		pIm = *(RowList.ElementAt(i));
		
		strName = pIm->getName();// 28apr08 .c_str();
		if(strName.empty() )// 28apr08 IsEmpty ())
		{	char  t[16];
			strName = "Unknown";
			 _itoa(unk++,t,10);
			 strName += t;
			//strName="FFFFFFFF";
			// 28apr08 break; // out of switch - we don't want to record unknowns
		}


		switch(pIm->VariableType())
		{			
		case  vT_Enumerated:			// 6
			pEnum = (hCEnum*)(pIm);
			if(pEnum) 
			{
				// 28apr08 strName = pIm->getName();// 28apr08 .c_str();
				// 28apr08 if(strName.empty() )// 28apr08 IsEmpty ())
				// 28apr08 	//strName="FFFFFFFF";
				// 28apr08 	break; // out of switch - we don't want to record unknowns
				
				// Added by Deepak
				// Store Integer value as getDispValuestring functionality is no longer returning IntegerValue
				// pEnum->Read(uValue);
				vValue = pEnum->getDispValue();
				uValue = vValue;
				//fprintf(stream, "%s %d %d %d",str,pIm->getID(),pIm->VariableType(),pEnum->getDispValue());
				//fprintf(stream, "%s %c %d %c %d %c %d %c",strName,cComma,pIm->getID(),cComma,pIm->VariableType(),cComma,pEnum->getDispValue(),cEOL);
				//fprintf(stream, "%s , %d , %d , %d\n",strName,pIm->getID(),pIm->VariableType(),pEnum->getDispValue());
				fprintf(stream, "%s , %d , %d , %u\n",strName.c_str(),pIm->getID(),pIm->VariableType(),uValue);
				cnt++;
				//END Mod
			}
			break;

		case  vT_BitEnumerated:			// 7
			pBitEnum = (hCBitEnum*) (pIm);
			if(pBitEnum )
			{
				// 28apr08 strName = pIm->getName().c_str() ;
				// 28apr08 if(strName.IsEmpty ())
				// 28apr08 	//strName="FFFFFFFF";
				// 28apr08 	break; // out of switch, don't store unknowns
				// Added by Deepak
				// Store Integer value as getDispValuestring functionality is no longer returning IntegerValue
				//uValue = pBitEnum->Read (uValue);				
				vValue = pBitEnum->getDispValue();
				uValue = vValue;
				
				//fprintf(stream, "%s %c %d %c %d %c %d %c",strName,cComma,pIm->getID(),cComma,pIm->VariableType(),cComma,pBitEnum->getDispValue(),cEOL);
				//fprintf(stream, "%s , %d , %d , %d\n",strName,pIm->getID(),pIm->VariableType(),pBitEnum->getDispValue());
				//fprintf(stream, "%s , %d , %d , %d\n",strName,pIm->getID(),pIm->VariableType(),pBitEnum->getDispValueString().c_str ());
				fprintf(stream, "%s , %d , %d , %d\n",strName.c_str(),pIm->getID(),pIm->VariableType(),uValue);
				cnt++;
				//END MOD
			}
			break;
		
		case  vT_Index:					// 8
			{
			/* <START> stevev 2/11/04 - we need to save the numeric!! */
			// 28apr08 strName = pIm->getName().c_str() ;
			// 28apr08 if(strName.IsEmpty ())
			// 28apr08 	break; // do not store unidentifiable variables
			
			CValueVarient vv;
			vv = pIm->getDispValue();
			int v ;
			v = vv;
			fprintf(stream, "%s , %d , %d , %d\n",strName.c_str(),pIm->getID(),pIm->VariableType(),v);
			cnt++;
			}
			break;

		case  vT_Integer:				// 2
		case  vT_Unsigned:				// 3
		case  vT_FloatgPt:				// 4
		case  vT_Double:				// 5
/*<REMOVE>*///		case  vT_Index:					// 8   
		case  vT_Ascii:					// 9
		case  vT_PackedAscii:			//10 - HART only
		case  vT_Password:				//11
		case  vT_BitString:				//12
		case  vT_HartDate:				//13 - HART only
		case  vT_Time:					//14
		case  vT_DateAndTime:			//15
		case  vT_Duration:				//16
		case  vT_EUC:					//17
		case  vT_OctetString:			//18
		case  vT_VisibleString:			//19
		case  vT_TimeValue:				//20
		case  vT_Boolean:				//21
		case  vT_MaxType:				//  22
			// 28apr08 strName = pIm->getName().c_str() ;
			// 28apr08 if(strName.IsEmpty ())
			// 28apr08 	//strName="FFFFFFFF";
			// 28apr08 	break; // do not store unidentifiable variables
			
			// 28apr08 strDispVal = (pIm->getDispValueString().c_str());
			// 28apr08 strDispVal = pIm->getDispValueString();
			strDispVal = TStr2AStr(pIm->getDispValueString());
			if(strDispVal.empty() ) // 28apr08 IsEmpty ())
			{	// strDispVal ="FFFFFFFF";
				clog << "Empty value while storing var to ini file."<<endl;
				break; // this should never happen
			}
			//fprintf(stream, "%s %c %d %c %d %c %s %c",strName,cComma,pIm->getID(),cComma,pIm->VariableType(),cComma,strDispVal ,cEOL);
			//fprintf(stream, "%s , %d , %d , %s\n",strName,pIm->getID(),pIm->VariableType(),strDispVal);
			fprintf(stream, "%s , %d , %d , %s\n",strName.c_str(),pIm->getID(),pIm->VariableType(),strDispVal.c_str());
			cnt++;
			break;
		default:
				break;//should not reach this					  
		};
	}// next row in list

	return cnt;


}



/****
 *  PreFillListData
 *		fills the local list (in ClistData) with the information from the device
 *      the local list is sortable and just points to device variable info
 ****/
int		CDDlistData::PreFillListData( LPCTSTR lpszFileName)
{// fetch from the dd

	CString strName, strDispVal;
	//int lDevId, lVarType;  	
	int initCnt;
	
// To Read from INI


	RETURNCODE rc = FAILURE;
	if (pCurDev == NULL) { return rc;}

	CitemType varType(iT_Variable);
	CVarList* pVarPtrList = (CVarList*)pCurDev->getListPtr(varType);


	// empty the local if it's not already
	if( RowList.GetSize() > 0)
	{
		RowList.RemoveAll(); // clears the list
	}
	// add a pointer for each row
	
// Code aded for reading the data from INI file.
// a CVarList::CitemListBase<var, hCVar>..a vector of hCVar
//	hCEnum *pEnum= NULL;
//	hCBitEnum *pBitEnum = NULL;
//	INSTANCE_DATA_STATE_T tmpState = IDS_CACHED;

	FILE *stream= NULL; 

	if(NULL != lpszFileName)
		stream = _tfopen(lpszFileName,_T("r")); //37040202.INI
	
	// These Variables will be used read from file
	// ************************************set init value in iT->
	initCnt = FillVarsFromINI(stream);

	// now fill the display
	for (CVarList::iterator iT = pVarPtrList->begin(); iT < pVarPtrList->end(); iT++)
	{
		hCEnum* pE = (hCEnum*)(*iT);
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
		RowList.Add(&(*iT));
#else
		RowList.Add((iT));// passed CArray of ptrs 2 ptrs as a ptr2ptr
#endif
	}
	
// close ini file
	if (stream)
		fclose( stream );

//	ApplyNow();	//Commented by Deepak pCVar->ApplyIt() is called instead of ApplyNow()
	// verify size match
	if (RowList.GetSize() == pVarPtrList->size() )
	{
		return SUCCESS;
	}
	else
	{
		cerr << "ERROR: RowList in display does not match the number of variables."<<endl;
	// Added By Deepak
		AfxMessageBox(M_UI_XMTR_BAD_INI);
		RowList.RemoveAll(); // clears the list
		for (CVarList::iterator iT = pVarPtrList->begin(); iT < pVarPtrList->end(); iT++)
		{
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
			RowList.Add(&(*iT));
#else
		RowList.Add((iT));
#endif
		}
	// END MOD
		return FAILURE;
	}
}
/****
 *  CancelNow
 *		removes all differences in the db
 *      returns the number actually changed <ie RealValue to DisplayValue>
 ****/
int CDDlistData::CancelNow(void)
{    
	int cnt = 0;
	hCVar* pIm = NULL;

	for (int i = 0; i < RowList.GetSize(); i++)
	{// ptr 2 a hCVar class
		pIm = *(RowList.ElementAt(i));
		if (pIm->CancelIt())// wasChanged
		{
			cnt++;
		}
	}// next
	return cnt;
}
/****
 *  ApplyNow
 *		applies all changes in the db
 *      returns the number actually changed
 ****/
/* Code added for storing the information in .INI file*/
int CDDlistData::ApplyNow(void)
{
	FILE *stream =NULL;   //37040202.INI

// Set	INI file name from where UI is populated
	//stevev 28apr08  CString sFileName;
	wchar_t  locName[32];
	//stevev 28apr08  sFileName.Format(_T("%016I64x%s"),this->getActiveKey(),L".INI");
	//stevev 28apr08  sFileName+=;
	swprintf(locName,_T("%016I64x%s"),this->getActiveKey(),_T(".INI"));
	//stevev 28apr08  stream = _tfopen(sFileName,_T("w+") );
	stream = _tfopen(locName,_T("w+") );
    
	int cnt = 0;
	hCVar* pIm = NULL;

	for (int i = 0; i < RowList.GetSize(); i++)
	{// ptr 2 a hCVar class
		pIm = *(RowList.ElementAt(i));
		if (pIm->ApplyIt())// wasChanged
		{
			cnt++;
		}
	}// next row
	if(stream != NULL )
	{
		cnt = FillINIfromVars(stream);
		fclose(stream);
	}
	return cnt;
}

/****
 *	QueryItemText
 *		the 'callback' from the display to aquire the text string for a box
 *		this is the data access portion of the UI
 ****/
CString		CDDlistData::QueryItemText  (int nRow, int nCol, BOOL getNumber)
{
	CString sf;
	//if (nCol == 4)
	//{
	//	sf.Format("0x%02X",testDataStore.GetAt(nRow).elementValue);
	//}
	//else
	//{
	//	sf.Format("I:%d  S:%i",nRow, nCol);
	//}

	//aCitemBase* pIm = pVarItems->at(nRow);

// Added By Deepak : Get the corressponding View Data
//	hCVar* pIm = *(RowList.ElementAt(nRow));
	ASSERT(pViewList->GetUpperBound () >= nRow);
	if ( RowList.GetUpperBound() < pViewList->ElementAt(nRow) )
	{
		sf = L"Row access error";
		return sf;
	}
	hCVar* pIm = *(RowList.ElementAt(pViewList->ElementAt(nRow)));
// END modification

	if ( pIm != NULL )
	{
		switch (nCol)
		{
		case 0: // symbol name
			sf = pIm->getName().data();
			break;
		case 1: // symbol number
			sf.Format(_T("%#010X"), pIm->getID());
			break;
		case 2: // label
			{
				hCattrLabel* paB = pIm->getLabelAttr();  // pIm->getaAttr(varAttrLabel);
				if (paB == NULL)
				{
					sf = "-No Label Found-";
				}
				else
				{ 
					wstring s;
					if ( paB->getString(s) != SUCCESS ) 
					{
						sf = _T("Unresolved Conditional Label");
					}
					else
					{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
						sf = s.c_str();
#else
					char* pS = new char[s.size()+1];
					UnicodeToASCII(s.c_str(), pS, s.size()+1);
					sf = pS;
					delete[] pS;
#endif
					}

/*
					hCddlString* pLabelStr = (hCddlString*)paB->procure();
					if (pLabelStr == NULL)
					{
						sf = "Unresolved Conditional";
					}
					else
					{
						sf = pLabelStr->procureVal().data();
					}
*/
/** done in procure!!!
					hCconditional* paC = &(((aCattrLabel*)paB)->condLabel);
					if (paC->priExprType == eT_Direct)
					{
						aPayldType* pPay = NULL;

						pPay = paC->destElements[0].pPayload; 
						if (pPay->whatPayload() == pltddlStr)
						{
							sf = ((aCddlString*)pPay)->theStr.data();
						}
						else
						{
							sf = "=Non String=";
						}
					}
					else
					{
						sf = "+Conditional+";
					}
**/
				}
			}
			break;

		case 3: // subclass
			{
				//aCattrBase* paB = pIm->getaAttr(varAttrClass);
				hCattrClass* paC = pIm->getClassAttr();
				if (paC == NULL)
				{
					sf = "-No Class Found-";
				}
				else
				{ 
					hCbitString* pClassBitStr = (hCbitString*)(paC->procure());
					if (pClassBitStr == NULL)
					{
						sf = "Unresolved Conditional";
					}
					else
					{
						sf = paC->translateBstring(pClassBitStr->getBitStr()).data();
					}

/** done in procure
					aCconditional* paC = &(((aCattrClass*)paB)->condClass);
					if (paC->priExprType == eT_Direct)
					{
						aPayldType* pPay = NULL;

						pPay = paC->destElements[0].pPayload; 
						if (pPay->whatPayload() == pltbitStr)
						{
							ulong bsV = ((aCbitString*)pPay)->bitstringVal;
							sf.Format("0x%04x", bsV);
						}
						else
						{
							sf = "=Non BitString=";
						}
					}
					else
					{
						sf = "+Conditional+";
					}
**/
				}
			}
			break;
		case 4: // value
			{// we need the string of the value
				string t;
				if ( pIm->VariableType() == vT_Enumerated )
				{
					hCEnum* pE = (hCEnum*)pIm;
					pE->procureString(pE->getDispValue(),t) ; // error string if not found
					sf = t.c_str();
				}
				else
				if(pIm->VariableType() == vT_BitEnumerated)
				{
					hCBitEnum* pE = (hCBitEnum*)pIm;
					pE->procureString(pE->getDispValue(),t) ; // error string if not found
					sf = t.c_str();
				}
				else
				{
					sf = pIm->getDispValueString().c_str();
				}
			}
			break;
// Added by Deepak To show Click Here message in the last column
		case 5:
				sf ="Click Here";
				break;
// These are pseudo columns which serves the purpose of providing extended information
		case COL_ITEM_TYPE: 
					sf = pIm->getTypeName (); 
					break;
		case COL_ITEM_SIZE:  
					sf.Format (_T("%d"),pIm->getSize ());
					break;
		case COL_VARIABLE_TYPE:
					sf = pIm->VariableTypeString ();
					break;
// END of modification
		default: // other and out of range
			sf.Format(_T("I:%d  S:%i"),nRow, nCol);
			break;
		}
	}
/*	 varAttrClass         = 0,
	 varAttrHandling,
	 varAttrConstUnit,
	 varAttrLabel,
	 varAttrHelp,
	 varAttrRdTimeout,	// 5
	 varAttrWrTimeout,
	 varAttrValidity,
	 varAttrPreReadAct,
	 varAttrPostReadAct,
	 varAttrPreWriteAct,	//10
	 varAttrPostWriteAct,
	 varAttrPreEditAct,
	 varAttrPostEditAct,
	 varAttrResponseCodes,
	 varAttrTypeSize,	//15
	 varAttrDisplay,
	 varAttrEdit,
	 varAttrMinValue,
	 varAttrMaxValue,
	 varAttrScaling,		//20
	 varAttrEnums,
	varAttrIndexItemArray,
	varAttrLastvarAttr	//23     /* must be last 
*/
	else
	{
		sf.Format(_T("I:%d  S:%i"),nRow, nCol);
	}

	return sf;
}


/****
 *	QueryItemInfo
 *		get the display supplemental information for the list item
 ****/
datumInfo_t	CDDlistData::QueryItemInfo  (int nRow, int nCol)
{

// Added By Deepak : Get the corressponding View Data
	ASSERT(pViewList->GetUpperBound () >= nRow);
//	hCVar* pIm = *(RowList.ElementAt(nRow));
	hCVar* pIm = *(RowList.ElementAt(pViewList ->ElementAt(nRow)));
//END modification


// Added By Deepak
// We need to show different colour for values based for ollowing category variables	
//	Normal			Black Text, White background
//	Class Dynamic		Blue Text       maskDynamic
//	Class Diagnostic	Green Text      maskDiagnostic
//	Class Service		Burgandy Text   maskService
//	Class Local			Purple Text     maskLocal


		UINT64 lClassBitString=0;
		hCattrClass* pCattr=NULL;
// We need class bit string
		pCattr= pIm->getClassAttr();
		if (pCattr)
		{
			hCbitString* pClassBitStr = (hCbitString*)(pCattr->procure());
			if (pClassBitStr != NULL)
			{
				lClassBitString = pClassBitStr->getBitStr();
			}
			else
			{
				lClassBitString = 0x0000;
				clog << "Still NO bit string for 0x"<<hex<<pIm->getID()<<dec<<endl;
			}
		}

	datumInfo_t yt = {0,7,0,0,0,1,0};
	if (nCol == 4)
	{
/* 0=Blk,1=Blu,2=Grn,3=Rd,4=Org,5=Maroon,6=FlGrn,7=Wht*/
		if(lClassBitString & maskDynamic)
			yt.txtColor = 1;
		else
			if(lClassBitString & maskDiagnostic)
				yt.txtColor = 2;
			else 
				if (lClassBitString & maskService)
					yt.txtColor = 5;
				else
					if(lClassBitString & maskLocal)
						yt.txtColor = 6;
					 else
						 yt.txtColor = 0; // all else at default

//		yt.txtColor = 2;
// END Modification
		yt.isEdit   = 1;
		yt.editType = (int)var2edit((variableType_t)pIm->VariableType());
		if (yt.editType == et_intEdit || yt.editType ==  et_hexIntEdit )
		{// refine this setting .... 
			wstring wEdtFmt = ((hCNumeric*)pIm)->getEditFormat();
			if ( wEdtFmt.find_first_of(L"eEfgG") != std::string::npos )
				yt.editType = et_floatEdit;
			else
			if( wEdtFmt.find_first_of(L"xX") != std::string::npos )
				yt.editType = et_hexIntEdit;
			// else - leave it like it is, we can't improve it.
		}
		if (pIm->isChanged()) { yt.bkgndColor  = 8/*yellow*/;}else { yt.bkgndColor = 7/*white*/;}
	}
	else
	{
		yt.txtColor = 0; // all else at default
	}
	return yt;
}


/****
 * SetItemValue
 *		this is the 'callback' for write part of the UI
 ****/
int	CDDlistData::SetItemValue(int nRow, int nCol, BOOL& isChanged, void* pValue)
{
	// determine the type
	// cast the value
	// write the value
	BOOL returnIsChanged = isChanged;

//--??? SUBLISTDATATYPE tempDataElement;

// Added By Deepak: get the Corressponding data row
//	hCVar* pIm = *(RowList.ElementAt(nRow));
	hCVar* pIm = *(RowList.ElementAt(pViewList->ElementAt(nRow)));
//END modification
	if (nCol == 4 )
	{
		datumInfo_t	thisItemInfo = QueryItemInfo(nRow,  nCol);
		editType_t  varEditType  = (editType_t)thisItemInfo.editType;
		switch(varEditType)
		{
		case et_enumEdit:		// 0x07
		case et_bitEnumEdit:	// 0x08
			{	returnIsChanged = pIm->SetIt(pValue);
			}
			break;
		case et_asciiEdit:		// 0x00 - default
		case et_floatEdit:
		case et_intEdit:
		case et_hexIntEdit:
		case et_pkdAsciiEdit:	// 0x04
		case et_passwrdEdit:	// 0x05	
		case et_dateEdit:
		case et_booleanEdit:
			{
				tstring t = (tchar*)pValue;
				pIm->setDisplayValueString(t);
				returnIsChanged = pIm->isChanged();
			}
			break;
		case et_Reserved0A:
		case et_Reserved0B:
		case et_Reserved0C:
		case et_Reserved0D:
		case et_Reserved0E:
		case et_Reserved0F:
		default:
			{	// nothing - return
			}
			break;
		}//endswitch

		// stevev 19sep07 - implement POST_USERCHANGE_ACTIONS
		// note 17jun09 - we need to make isChanged valid for more than int's
		if (isChanged)
			pIm->doPstUserActs();
		// end stevev 19sep07

		isChanged = returnIsChanged;

		//tempDataElement.val = *((UINT*)pValue);
//write the value another way		testDataStore.SetAt(nRow, tempDataElement);
	}
	return 0;
}

sColDesc&	CDDlistData::GetColDesc     (int ColNum, sColDesc& nColDesc)
{
	if (ColNum < NumColumns)
	{
		_tcscpy(nColDesc.ColLabel, _gstColumnLabel[ColNum]);
		nColDesc.ColWidth = _gstColumnWidth[ColNum];
		nColDesc.ColFmt   = _gstColumnFmt[ColNum];
	}
	else
	{
		_tcscpy(nColDesc.ColLabel,_T("UNK"));
		nColDesc.ColWidth = 50;
		nColDesc.ColFmt   = LVCFMT_CENTER;
	}
	return nColDesc;
}
BOOL	    CDDlistData::QueryValidity  (int nRow)
{
	return TRUE;
}
void		CDDlistData::SetItemValidity(int nRow, BOOL nValid)
{
}


/////////////////////////////////////////////////////////////////////////////
// CDDlistData drawing

//void CDDlistData::OnDraw(CDC* pDC)
//{
//	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
//}

/////////////////////////////////////////////////////////////////////////////
// CDDlistData diagnostics

#ifdef _DEBUG
void CDDlistData::AssertValid() const
{
	//CChkListView::AssertValid();
}

void CDDlistData::Dump(CDumpContext& dc) const
{
	//CChkListView::Dump(dc);
}
#endif //_DEBUG
/*********************
/////////////////////////////////////////////////////////////////////////////
// CDDlistData message handlers

void CDDlistData::OnLButtonDown(UINT nFlags, CPoint point) 
{// one one click
 // one followed by ON_DBLCLK at double click
	UINT uFlags=0;
	int nHitItem=GetListCtrl().HitTest(point,&uFlags);

	// we need additional checking in owner-draw mode
	// because we only get LVHT_ONITEM
	BOOL bHit=FALSE;
	if(uFlags==LVHT_ONITEM && (GetStyle() & LVS_OWNERDRAWFIXED))
	{
		CRect rect;
		GetListCtrl().GetItemRect(nHitItem,rect,LVIR_ICON);

		// check if hit was on a state icon
		if(point.x<rect.left)
			bHit=TRUE;
	}
	else if(uFlags & LVHT_ONITEMSTATEICON)
		bHit=TRUE;

	if(bHit)
		CheckItem(nHitItem);
	else		
		CChkListView::OnLButtonDown(nFlags, point);
}

***********************/

int	CDDlistData::ReleaseAll(void)// disconnection is imminent
{// release any memory and pointers we may own
 //	releaseItemList(& pVarItems );

	if ( (pDevMgr != NULL) && (! pDevMgr->IsdbNOTavailable()) ) // double negative...if available
	{
		releaseDB();
	}
	return 0;//SUCCESS;
}

void CDDlistData::OnInitialUpdate() 
{
	RETURNCODE rc = FAILURE;
	hCcommInfc* pXmtrComm = NULL;

	//CAscaffoldDoc * p_FRD = (CAscaffoldDoc *)GetDocument();
//	CAfileReaderDoc * p_FRD = (CAfileReaderDoc *)GetDocument();
	CDDdeviceDataApp* p_App = (	CDDdeviceDataApp*)AfxGetApp();
	if (pDevMgr != NULL)
	{
		if (pCurDev != NULL)
		{
				hCdeviceSrvc* pDevSvc = (hCdeviceSrvc*)pCurDev;
			pDevMgr->deleteDevice(&pDevSvc);
			//pDevMgr->deleteDevice(&pCurDev);			
		}
	}

	const TCHAR *pa;

	if (p_App != NULL)
	{
		if ( ! (p_App->p_Arguments))
		{ // check the default db exists
		  // if not - force user to browse for the DB
			CString mb;
			mb.Format(M_UI_XMTR_NO_DB);
			AfxMessageBox(mb);
			DDAvailable = FALSE;
            clog << "No App Arguments."<<endl;
		}
		else
		{// get the db path & open it
			// if db not found - force the user to find it or exit
			pa = (*(p_App->p_Arguments))[_T('B')][_T("databaseDirectory")].c_str();// entered or default
clog << "Got DB DIR:"<<pa<<"."<<endl;	

			const wchar_t* pLC = (*(p_App->p_Arguments))[_T('L')][_T("langCode")].c_str();// entered or default

			if (pLC && *pLC != '|' )
			{
				panic("Invalid language code specified on command line\n");
			}

			if ( pDevMgr == NULL )
			{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
				char* pS = new char[wcslen(pa)+1];
				UnicodeToASCII(pa, pS, wcslen(pa)+1);

				char* pL = new char[wcslen(pLC)+1];
				UnicodeToASCII(pLC, pL, wcslen(pLC)+1);


				pDevMgr = hCDeviceManger::instantiate(pS,pL);
				delete[] pS;
#else
				pDevMgr = hCDeviceManger::instantiate(pa);
#endif
			}

			// if we got one and the DB is OK
			if ( pDevMgr != NULL && (! pDevMgr->IsdbNOTavailable())) 
			{// generate a device

				if (activeDDkey == 0) // but no device
				{// browse the DB for a Key

				//*	RETURNCODE arc = SUCCESS;
				//*	aCentry* pRoot = NULL;
				//*	arc =  allocDeviceList( &pRoot );
					
					CDDselect dlg;
/*Vibhor 140404: Start of Code*/
					hCddbDevice *pStandardDev = (hCddbDevice*)(pDevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE));
					// getDevicePtr is ALWAYS successful
					if (pStandardDev->getDDkey() == 0)// the error device
					{// the standard device is seldom loaded at this point
						Indentity_t mtIdentity;
						pStandardDev = pDevMgr->newDevice(mtIdentity,NULL,NULL,NULL,NULL,0,TOK_NONE);
						// we should handle a load error here!
					}
					dlg.setResp(pStandardDev,&activeDDkey);
/*Vibhor 140404: End of Code*/
					dlg.DoModal();
				}
				// else continue
				if (activeDDkey != 0) // we succeeded
				{// continue with the device
//					Indentity_t oneIdentity;	oneIdentity.clear();
					// we will be using the key generation technique
					//		-- identity empty and a value in the key	
					pa = (*(p_App->p_Arguments))[_T('U')][_T("CommPort")].c_str();
					char* pS;
#ifdef _UNICODE
					pS = new char[wcslen(pa)+1];
					UnicodeToASCII(pa, pS, wcslen(pa)+1);
#else
					pS = new char[slen(pa)+1];
					strcpy(pS,pa);
#endif

					pXmtrComm = (hCcommInfc*)     new CXmtrComm(pS);
					delete pS;
					if ( pXmtrComm != NULL )
					{
						rc = pXmtrComm->initComm();
						if (rc != SUCCESS)
						{
							cerr <<"ERROR: CmtrComm failed to initialize."<<endl;
							delete ((CXmtrComm*)pXmtrComm);
							return;// rc;
						}
					}
					else
					{
						return;// APP_CONSTRUCT_ERR;
					}
					// to get here we have to have a working comm interface
					hCcmdDispatcher* pCD  = 
						(hCcmdDispatcher*)new CxmtrDispatcher(pXmtrComm,0);
					
					//prashant oct 2003
					//added parameter ddbDeviceService.h
					//This is just to fill in the required parameter.It does nothing here.
					hCMethSupport *m_pMethSupport = new CMethodSupport();


					CsdcFileSupport* pFile = NULL;//pDoc->GetDeviceFile();// instantiates only(NULL if fails)

				//	pCurDev = pDevMgr->newDevice(oneIdentity,pFile, m_pMethSupport, pXmtrComm, pCD, activeDDkey);
					TokenizerDescription tok__desc = TOK_EFF8;// 12may16 - make it 8 to avoid the FMA HACK!//= 10 foe fma
					pCurDev = pDevMgr->newDevice(entryIdentity,pFile, m_pMethSupport, pXmtrComm, pCD, activeDDkey, tok__desc);

// for dump testing only [2/24/2014 timj]
//exit(0);	// this is a good place to exit if you only want dump output

					if ( pCurDev == NULL )
					{
	clog << "Device did not instantiate.."<<endl;	
						if ( !DumpOnly )
						{
							CString mb;
							mb.Format(_T("ID key '%016I64x' would not instantiate."),activeDDkey);
							AfxMessageBox(mb);
						}
						else
						{
							exit(2);
						}
						DDAvailable = FALSE;
					}
					else
					{// we made it	
						closeDB();
						DDAvailable = TRUE;						
						((CXmtrComm*)pXmtrComm)->setDev(pCurDev);
						((CMethodSupport*)m_pMethSupport)->pDev = pCurDev;
						((CMethodSupport*)m_pMethSupport)->pMEE = pCurDev->pMEE;// stevev 25apr07 added 
						activeDDkey = pCurDev->getDDkey();//stevev5sep07-match what was instantiated(in case of generic)
	clog << "Device instantiated OK."<<endl;
#ifdef _DEBUG
	if (DumpOnly)
	{
		pCurDev->dumpSelf();
		hCdeviceSrvc* pDevSvc = dynamic_cast<hCdeviceSrvc*>(pCurDev);
		pDevMgr->deleteDevice(&pDevSvc);
		pCurDev = dynamic_cast<hCddbDevice*>(pDevSvc);
		exit(0);
	}
#endif
					}
				}
				else // activeDDkey == 0  // none found
				{// error completion
					CString mb;
					mb.Format(M_UI_XMTR_ZERO_ILLEGAL);
					AfxMessageBox(mb);
//					delete pDevMgr;
//					pDevMgr = NULL;
					DDAvailable = FALSE;
					return ;
				}
			}
			else
			{// db mgr failed or db not available --- can do nothing
				
				if (pDevMgr == NULL )
				{
					DDAvailable = FALSE;
clog << "Device Manager failed to instantiate."<<endl;
				}
				else // db not available
				{
clog << "Device Manager failed to aquire the database."<<endl;
					CString mb;
					mb.Format(M_UI_XMTR_DB_NOT_AVAIL);
					AfxMessageBox(mb);
//					delete pDevMgr;
//					pDevMgr = NULL;
					DDAvailable = FALSE;
				}
			}// endelse devMgr & DB	
		}// endelse  have arguements
	}
	else
	{
clog << "No App available."<<endl;
		TRACE(_T("ERROR: No application found!\n"));
		DDAvailable = FALSE;
		return;
	}


#if 0



	//else // we have a key
	systemKey.setSegment(0);
	systemKey.setDDkey(wrkingKey);
	systemKey.setDevID(0/*ddbDeviceID.dwDeviceId*/);/////temporary - system device zero

	//*this is fills the variable lists
	//- other setup stuff is accomplished (ie commands)
	rc = setup(wrkingKey/*was:incomingDDkey*/);

	
	

#endif
// Set	INI file name from where UI is populated
	CString s;
	wchar_t  locName[32];
	//stevev 28apr08  s.Format(_T("%16I64x"),this->getActiveKey());
	//stevev 28apr08  s+=".INI";
	swprintf(locName,_T("%016I64x%s"),this->getActiveKey(),_T(".INI"));
//
	s = locName;

	if ( PreFillListData(s) != SUCCESS ) // instantiates vars
	{
clog << "Prefill list failed."<<endl;
		TRACE(_T("++++PREFILLLISTDATA FAILED++++"));
	}
	else
	{
	}
	// do this last
	if (pXmtrComm != NULL)
		((CXmtrComm*)pXmtrComm)->readInCommLog();

/***************************                                Initialization work
	CListCtrl& ListCtrl = GetListCtrl();
	// calculate data
	CString s_PathNm = (GetDocument())->GetPathName( );
	m_pDataList->PreFillListData(LPCTSTR( s_PathNm ) );
   CurColType = m_pDataList->atFileType;

	// fill columns
	m_pDataList->FillColumnHead(this);
	// fill items
	m_pDataList->FillItems(ListCtrl);
	// display the result
	CChkListView::OnInitialUpdate();	
*****************************/	
}

/**********************
BOOL CDDlistData::PreCreateWindow(CREATESTRUCT& cs) 
{
	// the document doesn't exist here
	// CAfileReaderDoc * p_FRD = (CAfileReaderDoc *)GetDocument();

	cs.style|= LVS_SHOWSELALWAYS | LVS_REPORT;

	return CChkListView::PreCreateWindow(cs);
}
***********************/


/*
{     POINT pt;     
      UINT flags;     
      int iItem;     
      int iSubItem;
}
lvhti.flags::
LVHT_ABOVE           The position is above the control's client area. 
LVHT_BELOW           The position is below the control's client area. 
LVHT_NOWHERE         The position is inside the list view control's client window, but it is not over a list item. 
LVHT_ONITEMICON      The position is over a list view item's icon. 
LVHT_ONITEMLABEL     The position is over a list view item's text. 
LVHT_ONITEMSTATEICON The position is over the state image of a list view item. 
LVHT_TOLEFT          The position is to the left of the list view control's client area. 
LVHT_TORIGHT         The position is to the right of the list view control's client area. 
*/
/************************
void CDDlistData::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
   // nFlags:
   //MK_CONTROL   Set if the CTRL key is down.
   //MK_LBUTTON   Set if the left mouse button is down.
   //MK_MBUTTON   Set if the middle mouse button is down.
   //MK_RBUTTON   Set if the right mouse button is down.
   //MK_SHIFT     Set if the SHIFT key is down. 
   
	
   LVHITTESTINFO lvhti;
   lvhti.pt = point;
   GetListCtrl().SubItemHitTest(&lvhti);

   
   if (lvhti.flags & LVHT_ONITEMLABEL)
   {
      //pmyListCtrl->SetItemText(lvhti.iItem, lvhti.iSubItem, NULL);
      // edit this guy
      //TRACE(_T("DblClk: item %d, subi %d\n"),lvhti.iItem,lvhti.iSubItem);
      datumInfo_t iInfo = m_pDataList->QueryItemInfo(lvhti.iItem,lvhti.iSubItem);
      if (iInfo.isEdit)
      {
         CEdit * localEdit = EditSubItem(lvhti.iItem,lvhti.iSubItem);
      }
   }

//	CChkListView::OnLButtonDblClk(nFlags, point);
}
**********************/


CEdit*  CDDlistData::EditSubItem (int Item, int subItm)
{
    // The returned pointer should not be saved
/***********************************************************     Work on the list control::
    // Make sure that nCol is valid
    CHeaderCtrl* pHeader = GetListCtrl().GetHeaderCtrl();
    int nColumnCount = pHeader->GetItemCount();
    int nColumnWidth = GetListCtrl().GetColumnWidth (subItm);
    if (subItm >= nColumnCount || nColumnWidth < 5)
    {
       TRACE(_T("--subItem(%d) is larger than column count OR Width too small.\n"),subItm);

		 return NULL;
    }

    CRect Rect,r2;

    //!!! GetSubItemRect() WORKS DIFFERENTLY WHEN IN COLUMN ZERO
    //   BOUNDS gets the entire item rect, 
    //   ICON gets the icon rect and 
    //   LABEL gets the non-icon string rect

    if(! GetListCtrl().GetSubItemRect( Item, subItm, LVIR_BOUNDS, Rect ))
    {
       TRACE(_T(" Failure to get SubItemRect(%d, %d ...) <(%d,%d) (%d,%d)>\n"),Item,subItm,
          Rect.left,Rect.top,Rect.right,Rect.bottom);
       return NULL;
    }
    else
    {
       if ( subItm == 0 )
       {
          //TRACE(_T(" BOUNDS %d - %d  \n"), Rect.left,Rect.right);
          Rect.right = Rect.left + nColumnWidth;
       }
    }

    // Now scroll if we need to expose the column
    CRect ClientRect;
    GetListCtrl().GetClientRect (&ClientRect);

    r2 = Rect;
    if (Rect.left < ClientRect.left || Rect.right > ClientRect.right )
    {
		CSize Size;
      if (Rect.left < ClientRect.left)
      {
         Size.cx = (Rect.left - ClientRect.left);//neg
         r2.left = ClientRect.left;
         r2.right= r2.left + (Rect.right - Rect.left);
      }
		else
      {
         Size.cx = Rect.right - ClientRect.right;//pos
         //r2.left = Rect.left + Size.cx;
         //r2.right= Rect.right + Size.cx;
      }
		Size.cy = 0;
// we really need to unpaint the current box befor scrolling to the new onw
      GetListCtrl().RedrawItems( Item, Item );
		GetListCtrl().Scroll (Size);
		//Rect.left -= Size.cx;
      //GetListCtrl().GetSubItemRect( Item, subItm, LVIR_LABEL, Rect ) ;
    }
   // do it again
    if(! GetListCtrl().GetSubItemRect( Item, subItm, LVIR_BOUNDS, Rect ))
    {
       TRACE(_T(" Failure to get SubItemRect(%d, %d ...) <(%d,%d) (%d,%d)>\n"),Item,subItm,
          Rect.left,Rect.top,Rect.right,Rect.bottom);
       return NULL;
    }
    else
    {
       if ( subItm == 0 )
       {
          TRACE(_T(" BOUNDS %d - %d  \n"), Rect.left,Rect.right);
          Rect.right = Rect.left + nColumnWidth;
       }
    }

//GetListCtrl().Update(Item);

    // Get Column alignment
    LV_COLUMN lvCol;
    lvCol.mask = LVCF_FMT;
    GetListCtrl().GetColumn (subItm, &lvCol);
    DWORD dwStyle;
    if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
    else if ((lvCol.fmt & LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
    else dwStyle = ES_CENTER;

//    Rect.left += Offset+4;
//    Rect.right = Rect.left + GetColumnWidth (subItm) - 3;
    if (Rect.right > ClientRect.right)
		Rect.right = ClientRect.right;

    dwStyle |= WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL;
    CEdit *pEdit = NULL;
    //aStrBuff = p_LD->QueryItemText(nItem, nColumn);
    pEdit = new CEditCell (this, Item, subItm, 
                           m_pDataList->QueryItemText(Item, subItm, TRUE));
                           //GetListCtrl().GetItemText (Item, subItm));
    pEdit->Create (dwStyle, Rect, this, IDC_EDITCELL);

       // repaint item (exiting editcell may have changed somthing)
    GetListCtrl().RedrawItems( Item, Item );//////////// we need to do the previous item

    return pEdit;
********************** will be moved later *********************************/
	return NULL;
}







/************************************ not used ********************************
// returns TRUE - we handled it
//         FALSE- go on to main keydown routine
BOOL CDDlistData::DeleteRow()
{
   int nItem;
   CString msg, m;
   POSITION pos = GetListCtrl().GetFirstSelectedItemPosition();
   if (pos == NULL)
   {
      nItem = -1;
   }
   else
   {   
      nItem = GetListCtrl().GetNextSelectedItem(pos);
   }

   if ( nItem >= 0)
   {
      if ( CurColType == Aframe)
      {
         m.Format("%d",nItem);
         AfxFormatString1( msg, IDS_STRING129, LPCTSTR(m) ); 

         int r = AfxMessageBox( msg,MB_OKCANCEL | MB_APPLMODAL |  MB_ICONQUESTION  | MB_DEFBUTTON2 );

         if (r == IDOK)
         {
            // * do frame deletion 
            if (m_pDataList->DeleteFrame(nItem, GetListCtrl()))//error
               return FALSE;
            else
               return TRUE;
         }
         //else  r == IDCANCEL - fall out
      }
      else if ( CurColType == Abyte)
      {
         m.Format("%d",nItem);
         AfxFormatString1( msg, IDS_STRING131, LPCTSTR(m) ); 

         int r = AfxMessageBox( msg,MB_OKCANCEL | MB_APPLMODAL |  MB_ICONQUESTION  | MB_DEFBUTTON2 );

         if (r == IDOK)
         {
            // do byte deletion 
            if (m_pDataList->DeleteByte(nItem, GetListCtrl()))//error
               return FALSE;
            else
               return TRUE;
         }
         //else r == IDCANCEL - fall out
      }
   }
   return FALSE;
}

// returns TRUE - we handled it
//         FALSE- go on to main keydown routine
BOOL CDDlistData::InsertRow()
{
   int nItem;
   CString msg, m;
   POSITION pos = GetListCtrl().GetFirstSelectedItemPosition();
   if (pos == NULL)
   {
      nItem = -1;
   }
   else
   {   
      nItem = GetListCtrl().GetNextSelectedItem(pos);
   }

   if ( nItem >= 0)
   {
      if ( CurColType == Aframe)
      {
         AfxFormatString1( msg, IDS_STRING130, "" ); 

         int r = AfxMessageBox( msg,MB_OKCANCEL | MB_APPLMODAL |  MB_ICONQUESTION  | MB_DEFBUTTON2 );

         if (r == IDOK)
         {
            // do the frame insertion 
            if (m_pDataList->InsertFrame(nItem, GetListCtrl()))//error
               return FALSE;
            else
               return TRUE;
         }
         //else  r == IDCANCEL - fall out
      }
      else if ( CurColType == Abyte)
      {
         AfxFormatString1( msg, IDS_STRING132, "" ); 

         int r = AfxMessageBox( msg,MB_OKCANCEL | MB_APPLMODAL |  MB_ICONQUESTION  | MB_DEFBUTTON2 );

         if (r == IDOK)
         {
            // do the byte insertion 
            if (m_pDataList->InsertByte(nItem, GetListCtrl()))//error
               return FALSE;
            else
               return TRUE;
         }
         //else  r == IDCANCEL - fall out
      }
   }
   return FALSE;
}
******************************* end not used ********************/

/**************************
void CDDlistData::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{	
    // Up and down are in the OnKeyDown so that the user can hold down the arrow
    // keys to scroll through the entries.
    BOOL Control = (GetKeyState (VK_CONTROL) < 0);
    Control |= GetKeyState (VK_SHIFT) < 0;

    BOOL d = FALSE;

    if ( ! Control )
    {
       switch (nChar)
       {
		   case VK_DELETE :
		   {
            d = DeleteRow();
		   }break;
		   case VK_INSERT :
		   {
            d = InsertRow();
		   }break;
       }
    }
    if ( d ) // we did something to the screen
    { 
      // Invalidate window so entire client area 
      // is redrawn when UpdateWindow is called.   
       Invalidate();   
      // Update Window to cause View to redraw.   
       UpdateWindow();

       return;
    }
    //else we did nothing...handle default
	
	CChkListView::OnKeyDown(nChar, nRepCnt, nFlags);
}

***********************************/

// fills the array with pertainent information
int CDDlistData::QuerySubListData(int nRow, int nCol, 
								  CArray<SUBLISTDATATYPE,SUBLISTDATATYPE&>& datArray, 
								  ulong& value, int& valLen)
{
	SUBLISTDATATYPE localData;// triad

	datArray.RemoveAll();
// Added By deepak: Get the corressponding view Data
//	hCVar*  pIm = *(RowList.ElementAt(nRow));
	hCVar*  pIm = *(RowList.ElementAt(pViewList->ElementAt (nRow)));
//END Modification

	hCEnum* pEn = NULL;
	if ( pIm->VariableType() == vT_Enumerated )
	{
		pEn = (hCEnum*)pIm;
		value = pEn->getDispValue();
		hCenumList eList(pEn->devHndl());
		if ( pEn->procureList(eList) != SUCCESS )
		{
			cerr << "ERROR: No eList for enum."<<endl;
			valLen = 0;
			return 0;//no lines
		}
		else
		{
			valLen = pEn->VariableSize();
			for (hCenumList::iterator iT = eList.begin(); iT < eList.end(); iT++)
			{//iT isa ptr 2 hCenumDesc
				//iT->theComm(pDevice->getCommAPI());
				localData = *iT;					// a triad
				datArray.Add(localData);
			}
		}
		return datArray.GetSize();
	}
	else 
	if ( pIm->VariableType() == vT_BitEnumerated )
	{
		pEn = (hCEnum*)pIm;
		value = pEn->getDispValue();
		hCenumList eList(pEn->devHndl());
		if ( pEn->procureList(eList) != SUCCESS )
		{
			cerr << "ERROR: No eList for bit enum."<<endl;
			valLen = 0;
			return 0;//no lines
		}
		else
		{
			valLen = pIm->VariableSize();
			for (hCenumList::iterator iT = eList.begin(); iT < eList.end(); iT++)
			{
				//iT->theComm(pDevice->getCommAPI());
				localData = *iT;
				datArray.Add(localData);
			}
		}
		return datArray.GetSize();
	}
	else
	{// no lists available
		return 0;//no lines
	}
	// find the correct item
	// fill the array w/ description/help/value sets

/*	localData. elementString = "First Item "; localData.elementValue = 0x01; datArray.Add( localData );
	localData. elementString = "Second Item"; localData.elementValue = 0x02; datArray.Add( localData );
	localData. elementString = "Third Item "; localData.elementValue = 0x04; datArray.Add( localData );
	localData. elementString = "Fourth Item"; localData.elementValue = 0x08; datArray.Add( localData );
	localData. elementString = "Fifth Item "; localData.elementValue = 0x10; datArray.Add( localData );
	localData. elementString = "Sixth Item "; localData.elementValue = 0x20; datArray.Add( localData );
	return 6;
*/
}


/*************************************************************************************************
 *
 *   $History: DDlistData.cpp $
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:58a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Milestone: Xmtr handles commands via pipes, IDE sends Cmd Zero
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:15p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/08/03    Time: 9:12a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/08/03    Time: 7:53a
 * Created in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 *************************************************************************************************
 */
