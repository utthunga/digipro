/*************************************************************************************************
 *
 * $Workfile: xMethodSupport.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		xMethodSupport.cpp : implementation file for Xmtr
 */
#include "stdafx.h"
//1  #include "XmtrDD.h"  // "sdc625.h"
#ifdef _DEBUG
#include "ddbDevice.h"
#endif

#include "xMethodSupport.h"
//#include "MethodDlg.h"
#include "MEE.h" // 07mar07 to get execution specifics

//3  #include "SDCthreadSync.h"

const hCmethodCall mtMethCall;// an error return structure

//#include "SDC625Doc.h" // added 01nov06 to support layout
//4  #include "ddbMenuLayout.h"
//#include "ximage.h"		// for pixel stuff


//const hCmethodCall mtMethCall;// an error return structure

CMethodSupport::CMethodSupport():pMEE(NULL),pCurrentMethodCall(NULL)
{
	m_bInvokedForNonUIAction = TRUE;

	methodStack.clear();
	clientCount = 0;
} 

CMethodSupport::	~CMethodSupport()
{
}

/*  handle several methods */
hCmethodCall* CMethodSupport::currentMethod()
{
/*	if (methodStack.size() > 0 )
	{
		return (methodStack.front());
	}
	else
	{	
		return mtMethCall;
	}
*/	return pCurrentMethodCall;
}
void CMethodSupport::pushMethod(hCmethodCall* pNewMeth)
{
	methodStack.push_front(pNewMeth);
	TRACE(_T("Pushed on stack...now has %d entries.\n"),methodStack.size());
}
hCmethodCall* CMethodSupport::popMethod()
{
	hCmethodCall* r = NULL;
	if (methodStack.size() > 0)
	{
		r = methodStack.front();
		methodStack.pop_front();
	}
	// else - return the null	
	TRACE(_T("Popped off stack...now has %d entries.\n"),methodStack.size());

	return r;
}


/**********************************************************************************************
 # Function Name  : PreExecute

 # Prototype      :  void PreExecute()


 # Description    :  This function used to do any initializations.Currently it does nothing.

 # Parameters     :   none
**********************************************************************************************/
#ifdef _DEBUG
void CMethodSupport::PreExecute(hCmethodCall& mc)
#else
void CMethodSupport::PreExecute()
#endif
{
	clientCount++;
}

/**********************************************************************************************
 # Function Name  : DoMethodExecute

 # Prototype      :  DoMethodExecute(hCmethod* pM)


 # Description    :  This function called by the device object to execute pre/post actions and by
											DDL method to execute a user selected method.
											
 # Parameters     :  The method description class

 # Returns        :  SUCCESS (0) or failure code
**********************************************************************************************/
RETURNCODE CMethodSupport::DoMethodSupportExecute(hCmethodCall& MethCall)
{
	hCitemBase*   pIB  = NULL;
	hCVar*       pVar  = NULL;
	CValueVarient tempVariant;
//	int           iCurrDevStat;

	bool          bReturnSuccess;
	RETURNCODE    rc   = SUCCESS;
	
	CValueVarient retVal;

	if ( pMEE == NULL ) 
		return FAILURE;
	unsigned  int meeCnt = pMEE->GetOneMethRefNo();//pDev->getMEEdepth();
	//hCMethSupport* pMee  = pDev->getMethodSupportPtr();

	if(MethCall.m_pMeth == NULL )
		return false;

	// start method execution
	//	pushMethod(MethCall); replaced stevev 25apr07
	if ( meeCnt > 0 )// we are not the first ones in
	{
		if ( pCurrentMethodCall == NULL )
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d MEEs with NO currently running method.\n",meeCnt);
			return false;
		}
		//pCurrentMethodCall->m_pMethodDlg = m_pMethodDlg; // these are zero if we are the first entry
		//pCurrentMethodCall->m_pMenuDlg   = m_pMenuDlg;

		pushMethod(pCurrentMethodCall);
		pCurrentMethodCall = &MethCall;// we have a new current
TRACE(_T("DoMethodExecute: push the current dialogs.\n"));
		if (pCurrentMethodCall->m_pMenuDlg != NULL || pCurrentMethodCall->m_pMethodDlg != NULL)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"Warning: Method execution entered with active dialog(s).\n");
		}
#ifdef _DEBUG
		if (stackSize() != (clientCount-1))
	{
		LOGIT(CERR_LOG,"Method Stack Error: %d stack with %d clients.\n",stackSize(),clientCount);
	}
		if (stackSize() != meeCnt)
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with %d MEEs.\n",stackSize(),meeCnt);
		}
#endif
	}
	else
	{// we are the first one in
		pCurrentMethodCall = &MethCall;// we have a new current
#ifdef _DEBUG
		if (stackSize() != 0 )
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with no methods running.\n",stackSize());
		}
		if (  clientCount != 1 )// incremented from 0 in pre-execute...
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d clients with no methods running.\n",clientCount);
		}
#endif
	}
	
	//RETURNCODE rc =	MethCall.m_pMeth->selfExecute(MethCall,retVal);


	//if called from an action
	if(MethCall.source == msrc_ACTION || MethCall.source == msrc_CMD_ACT) 
	{
		if (MethCall.paramList.size() != 1 )
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to execute action not executed because "
					       "the item ID of the concerned variable is not provided.\n");
			rc = APP_PARAMETER_ERR;
		}
		else
		{
			LOGIT(CLOG_LOG,"Start Action method ===============================\n");
			//first element in the parameter list is the variable item ID of interest
			//bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,MethCall.methodID,
			//	                                            MethCall.paramList[0].vValue.varSymbolID);
			bReturnSuccess = pMEE->ExecuteMethod(pDev,MethCall.methodID,
				                                         MethCall.paramList[0].vValue.varSymbolID);
			
			LOGIT(CLOG_LOG,"End   Action method ===============================\n");
		}
	}
	else
	if (MethCall.source == msrc_EXTERN)
	{
#ifdef _DEBUG
		hCitemBase* pI;
		if (pDev->getItemBySymNumber(MethCall.methodID, &pI) == SUCCESS)
		{
		LOGIT(CLOG_LOG,"Start User method ===============================%s\n",pI->getName().c_str());
		}
#else
		LOGIT(CLOG_LOG,"Start User method ===============================\n");
#endif
		//bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,MethCall.methodID);	
		bReturnSuccess = pMEE->ExecuteMethod(pDev,MethCall.methodID);	
	}
	else
	{
		LOGIT(CLOG_LOG,"INVALID method === ===============================\n");
		rc = APP_TYPE_UNKNOWN;
	}
/**** NEVER DONE - WE are SIMULATOR!!! 
	// this is post execute activity::>
	if (MethCall.source != msrc_CMD_ACT)
	{// @ Not cmd action - handle command 38
		rc = pDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
		if(SUCCESS == rc && pIB != NULL)
		{
			pVar = (hCVar*)pIB;
			// modified for KROHNE device error 10/18/04 stevev
			tempVariant  = pVar->getRealValue();
			iCurrDevStat = (int) tempVariant;
			if(iCurrDevStat & DS_CONFIGCHANGED)
			{// Its assumed that Command 38 is being supported by the device
				rc = pDev->sendMethodCmd(38,-1);
			}
		}
	}// else command activity will deal with response code and status
*****************/
	
	if ( rc == SUCCESS && ! bReturnSuccess)
	{
		rc = FAILURE;
	}// if both success, leave it, if rc has an error code, leave it
	if ( meeCnt > 0 && stackSize() > 0 )// we pushed ' em on entry
	{
		pCurrentMethodCall = popMethod();
TRACE(_T("DoMethodExecute: pop previous dialogs.\n"));
		// put into pointers for post execute deletion
		//m_pMethodDlg = pCurrentMethodCall->m_pMethodDlg;
		//m_pMenuDlg   = pCurrentMethodCall->m_pMenuDlg;	
	}
	else
	{
		pCurrentMethodCall = NULL; // we be done
	}
	// popMethod();

	if(rc == FAILURE)
	{	//m_ctlStat.SetWindowText("Method Execution Failed");
		LOGIT(CERR_LOG,"Method execution failed.selfExecute() function has returned an error.\n");
		TRACE(_T("Method execution failed.selfExecute() function has returned an error.\n"));
		return false;
	}

	return MethCall.m_pMeth->GetLastMethodsReturnCode();
}


/*************************************************************************************************
 # Function Name  : PostExecute

 # Prototype      :  void PostExecute()


 # Description    :  This function is used to do the cleanup, if any.Currently this funcyion
										 doesn't do anything.

 # Parameters     :   none
**********************************************************************************************/

#ifdef _DEBUG
void CMethodSupport::PostExecute(hCmethodCall& mc)
#else
void CMethodSupport::PostExecute()
#endif
{
	// dec client count
	clientCount--;
	if ( clientCount == 0 )
	{// if zero, send close, delete
#ifdef _DEBUG
//		TRACE(_T(">>  Display PostExecute Zero clients.\n"));
		if (stackSize() != clientCount)
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with NO clients.\n",stackSize());
		}
	}
	else
	{
		TRACE(_T(">>  Method PostExecute %d clients.\n"),clientCount);
#endif
	}
}



/**********************************************************************************************
# Function Name	: MethodDisplay

# Prototype     :  void MethodDisplay(ACTION_UI_DATA structMethodsUIData,
												ACTION_USER_INPUT_DATA& structUserInputData)


# Description   : This function is called from the builtins to display messages to the user and
					get the user inputs.  It calls the function in MethodsDlg to diaplay the 
					information. It than waits for the event when the user clicks next.It then 
					calls the MethodsDlg function to get the user input values.
		
# Parameters    : ACTION_UI_DATA structMethodsUIData -  This structure contains all information
														about the data to be dispalyed.
				  ACTION_USER_INPUT_DATA& structUserInputDats - This structure contains all the
														information entered by the user.

  # return value:  false == ABORT button pressed true == not

*************************************************************************************************/ 

bool CMethodSupport::MethodDisplay(ACTION_UI_DATA structMethodsUIData,
													  ACTION_USER_INPUT_DATA& structUserInputData)
{
	return TRUE;
}


//   string ptr, UI extracts the string(it knows what lang), converts lines X chars then to rows X cols & returns
bool CMethodSupport::UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit, bool isDialog )
{ return true; /* just return an error for now */}

int  CMethodSupport::numberMethodsRunning() 
{
	return clientCount;
}
