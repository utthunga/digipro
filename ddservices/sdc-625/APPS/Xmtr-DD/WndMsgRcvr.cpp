// WndMsgRcvr.cpp : implementation file
//

#include "stdafx.h"
#include "XmtrDD.h"
#include "WndMsgRcvr.h"
#include "Client.h"
#include "XmtrComm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWndMsgRcvr

CWndMsgRcvr::CWndMsgRcvr()
{
	pComm = NULL;
}

CWndMsgRcvr::~CWndMsgRcvr()
{
}


BEGIN_MESSAGE_MAP(CWndMsgRcvr, CWnd)
	//{{AFX_MSG_MAP(CWndMsgRcvr)
	ON_MESSAGE(WM_HART_MSG,OnHartMsg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CWndMsgRcvr message handlers
// 

// xmtr LL DLL to Dispatcher receiver shim code
LRESULT CWndMsgRcvr::OnHartMsg(WPARAM wParam, LPARAM lParam)
{
	// incoming is client.m_pHartMsg
	CClient* pClient = (CClient*)lParam;
	// reply used to be in the  m_pSlaveReply

	if ( pComm != NULL)
	{
		pComm->msgToReceive();
		return 0;
	}
	else
	{
		cerr << "ERROR: no commInfc ptr when we got the windows message."<<endl;
		return 1;
	}
	// the receive service will que a new pkt
	// when we return and release the mutex, it will be sent out

}