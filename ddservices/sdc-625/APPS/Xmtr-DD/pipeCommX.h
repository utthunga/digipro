/*************************************************************************************************
 *
 * $Workfile: pipeCommX.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		This is the class to the send command / receive command..
 */




#ifndef _PIPECOMM_H
#define _PIPECOMM_H
#ifdef INC_DEBUG
#pragma message("In pipeComm.h") 
#endif

#include "ddbCommInfc.h"
#include "ddbQueue.h"

// in lieu of stdafx.h
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include "NamedPipe.h"

#include <windows.h> 

#ifdef INC_DEBUG
#pragma message("    Finished Includes::pipeComm.h") 
#endif


#define PIPE_TIMEOUT   15000  /* mS (15 S) */

 
#define CONNECTING_STATE 0		/* no-op in nxtState */
#define IDLE_RD_STATE    1	/* reading an internal temorary next state */
#define READING_STATE    2	/* readingEx */
#define WRITING_STATE    3	/* writingEx */
#define ERROR_STATE      0xf0	/* forces disconnect/reconnect in nxtstate */
#define INSTANCES     2  /* only two masters are allowed in HART */
 


typedef struct 
{ 
   OVERLAPPED oOverlap; 
   HANDLE     hPipeInst; 
   BYTE       chBuf[PIPEBUFFERSIZE]; 
   DWORD      cbToWrite; 
   DWORD      dwState; 
   DWORD      dwNxtState; 
} PIPEINST, *LPPIPEINST; 


// the interface (supplied by the application) to the i/o channel to the device
class CXmtrpipeComm : public hCcommInfc
{
	hCqueue<hPkt> pktRcvdQueue;    // instantiates with dflt size// write queue
	cmdInfo_t     lstRcvdCmdInfo;  // save address and type for the reply
	
	DWORD  dwThreadID;
	HANDLE hThread;			// track that other thread
	HANDLE hDieEvent;		// tell the pipe handler that it's time to go home
//	HANDLE hDataChngEvent;	// let us know when someone wrote our data
	
	// class  memory - one per pipe
	PIPEINST Pipe[INSTANCES]; 
	HANDLE   hEvents[INSTANCES+2]; // one per instance + writeQ & the die event

	// private prototypes
	VOID  DisconnectAndReconnect(DWORD); 
	DWORD ConnectToNewClient(HANDLE, LPOVERLAPPED); 
	//VOID  GetDataToWriteToClient(LPPIPEINST); 

	int   pkt2Msg(hPkt* pPkt, BYTE* msgBuff);   // pkt to write to xmit byte string
	DWORD sendPkt(int pipeNum, hPkt* pPkt2Wrt); // returns nxtState
	int   rcv_Msg(int pipeNum);                 // zero == success, doesMsg2Pkt, Pkt to rcv
	int   parseMsg2PktNinfo(BYTE* msgBuf, DWORD bufLen, hPkt* pPkt, cmdInfo_t* pCmdInfo);


	hCmutex  pipeMutex;
	hCmutex::hCmutexCntrl* pMutexCtrl;

public:	
	CXmtrpipeComm();
	
	~CXmtrpipeComm();

	//use base class's::> void  setRcvService(hCsvcRcvPkt* cpRcvSvc);

	hPkt*       getMTPacket(void);	// gets an empty packet for generating a command 
								//		(gives caller ownership of the packet*)
	void        putMTPacket(hPkt*);	// returns the packet memory back to the interface 
								//		(this takes ownership of the packet*)<used for cmd abort>
	RETURNCODE	SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
	RETURNCODE	SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);
					// puts a packet into the send queue  (takes ownership of the packet*)

	int         sendQsize(void); 	// how many are in the send queue ( 0 == empty )
	int			sendQmax(void);		// how many will fit
	RETURNCODE  initComm(void);		// setup connection (called after instantiation of device)

	void        pipeThread(void);
};

#endif //_PIPECOMM_H


/*************************************************************************************************
 *
 *   $History: pipeCommX.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/16/03    Time: 12:04p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Changing to different name - see the cpp.
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/08/03    Time: 7:53a
 * Created in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 *************************************************************************************************
 */