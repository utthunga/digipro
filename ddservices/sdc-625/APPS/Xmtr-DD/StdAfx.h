// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__2D950084_C6B4_4102_A67C_AE9792BBB6E1__INCLUDED_)
#define AFX_STDAFX_H__2D950084_C6B4_4102_A67C_AE9792BBB6E1__INCLUDED_
#pragma message("StdAfx apps/Xmtr-DD")
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In StdAfx.h") 
#endif

#pragma warning (disable : 4786)

#if _MSC_VER >= 1300
#define  WINVER      0x0500	/* 2008 only targets win2000 and above */
#define _WIN32_WINNT 0x0500
#endif

//#define GETMAINFRAME ((CMainFrame *)(AfxGetApp()->m_pMainWnd))
#define GETMAINFRAME ((CWnd*)(AfxGetApp()->m_pMainWnd))

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// application generic includables

#include "messageUI.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes DevData::StdAfx.h") 
#endif

#define COLSTRLEN	16	/* column header name string length */

#ifdef _UNICODE

#if _MSC_VER < 1300
// vs 2003 and above have their own version of this function
extern double _wtof(const TCHAR* a );
#endif

#ifndef __TCHAR_DEFINED
#define _ttof( a ) _wtof( a )
#endif
//#pragma message("Unicode defined")
#define tcerr	wcerr
#define tclog	wclog
#define tcout	wcout
#else
#define _ttof( a ) atof( a )
//#pragma message("Unicode NOT defined")
#define tcerr	cerr
#define tclog	clog
#define tcout	cout
#endif

#ifdef INC_DEBUG
#pragma message("In StdAfx.h") 
#endif

#include <string>
#include <algorithm>
using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes::StdAfx.h") 
#endif
// stevev - WIDE2NARROW char interface
int __fastcall UnicodeToASCII(LPCWSTR pszW, LPSTR pRetA, int lenRetA);
int __fastcall UnicodeToASCII(CString &cstr, LPSTR pRetA, int lenRetA);
int __fastcall AsciiToUnicode(LPCSTR pszA,   LPWSTR pszW, int lenRetW);
int __fastcall AsciiToUnicode(LPCSTR pszA, CString& retStr);
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__2D950084_C6B4_4102_A67C_AE9792BBB6E1__INCLUDED_)
