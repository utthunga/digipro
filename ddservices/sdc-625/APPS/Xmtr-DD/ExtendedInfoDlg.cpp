// ExtendedInfoDlg1.cpp : implementation file
//

#include "stdafx.h"
#include "XmtrDD.h"
#include "ExtendedInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtendedInfoDlg dialog


CExtendedInfoDlg::CExtendedInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExtendedInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExtendedInfoDlg)
	m_ItemID = _T("");
	m_ItemName = _T("");
	m_ItemSize = _T("");
	m_ItemType = _T("");
	m_VariableType = _T("");
	//}}AFX_DATA_INIT
}


void CExtendedInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtendedInfoDlg)
	DDX_Text(pDX, IDC_ITEMID, m_ItemID);
	DDX_Text(pDX, IDC_ITEMNAME, m_ItemName);
	DDX_Text(pDX, IDC_ITEMSIZE, m_ItemSize);
	DDX_Text(pDX, IDC_ITEMTYPE, m_ItemType);
	DDX_Text(pDX, IDC_VARAIBLETYPE, m_VariableType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExtendedInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CExtendedInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtendedInfoDlg message handlers

void CExtendedInfoDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
	CDialog::OnOK();
}

BOOL CExtendedInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
