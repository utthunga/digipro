/*************************************************************************************************
 *
 * $Workfile: dataPropertySheet.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "dataPropertySheet.h"
 */
#if !defined(AFX_DATAPROPERTYSHEET_H__5D114EAE_C9C1_421D_A03C_2DA2C67FA443__INCLUDED_)
#define AFX_DATAPROPERTYSHEET_H__5D114EAE_C9C1_421D_A03C_2DA2C67FA443__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In dataPropertySheet.h") 
#endif
// dataPropertySheet.h : header file
//

#include "dataPropertyList.h"
#include "DDaboutDlg.h"
#include "DDlistData.h"  // the sheet owns the data

#include "ResizableSheet.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::dataPropertySheet.h") 
#endif
/////////////////////////////////////////////////////////////////////////////
// CdataPropertySheet


class CdataPropertySheet : public CResizableSheet//: public CPropertySheet
{
	DECLARE_DYNAMIC(CdataPropertySheet)

// Construction
public:
	CdataPropertySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CdataPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
	// note that the sheet owns all the pages!
	CdataPropertyList m_Allpage; 
	// will add a list
	//vector<CdataPropertyList>  classPages  at some future date 
// Added By Deepak: Each member represents a page in property sheet
// Each page has information about specific class
	CdataPropertyList  m_CDiagnosticClass;
	CdataPropertyList  m_CDynamicClass;
	CdataPropertyList  m_CServiceClass;
	CdataPropertyList  m_CCorrectionClass;
	CdataPropertyList  m_CComputationClass;
	CdataPropertyList  m_CInputBlockClass;
	CdataPropertyList  m_CAnalogOutputClass;
	CdataPropertyList  m_CHartClass;
	CdataPropertyList  m_CLogicalDisplayClass;
	CdataPropertyList  m_CFrequencyClass;
	CdataPropertyList  m_CDiscreteClass;
	CdataPropertyList  m_CDeviceClass;
	CdataPropertyList  m_CLocalClass;
	CdataPropertyList  m_CInputClass;
	CdataPropertyList  m_CUnAssignedClass;
	CdataPropertyList  m_CFactoryClass;
	
//END modification	

	CButton m_LocalChkBox;
	// we own the data handler too
	CDDlistData*  m_pDataList;
// Operations
public:
	void setDDkey(DD_Key_t ulKey = 0){theDDkey = ulKey; };
	void setIdentity(Indentity_t& id){entryIdent = id;};

	BOOL handleGetDispInfo(LV_DISPINFO* pDispInfo);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CdataPropertySheet)
	public:
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CdataPropertySheet();

protected:
	HICON m_hIcon;
	DD_Key_t theDDkey;
	Indentity_t entryIdent;


	void initVars(void);


	// Generated message map functions
	//{{AFX_MSG(CdataPropertySheet)
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnCheckUnCheck();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#ifdef INC_DEBUG
#pragma message("    Finished Including::dataPropertySheet.h") 
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAPROPERTYSHEET_H__5D114EAE_C9C1_421D_A03C_2DA2C67FA443__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: dataPropertySheet.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
