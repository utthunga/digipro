/*************************************************************************************************
 *
 * $Workfile: XmtrComm.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the Xmtr class to receive the command / send reply..
 *		The communication interface for xmtr app.
 *		Created from the orignal pipeCommX...but changed when when it became a shim.
 *	This class is now a shim between the comm interface and the XmtrLlc.dll interface.
 *	The XmtrPipe library mimics this interface for pipe operations.  It is designed to
 *	allow drop-in replacement with the original XmtrLlc.dll when _USEXMTR_DLL is defined
 *	and the .DLL is supplied.
 */
 
#include "stdafx.h"


#include "Hart.h"
#include "XmtrLlc.h"

//#include "HartLlcMsg.h"
//#include "RecvMsg.h"


#include "registerWaits.h"


#include "WndMsgRcvr.h"
#include "XmtrComm.h"


#include "ddbGeneral.h"

#include "ddbItemBase.h"
#include "ddbDevice.h"

#ifdef _USEXMTR_DLL
 #ifndef LOGPIPES
  bool lE = FALSE;
 #else
  bool lE = TRUE;	
 #endif
#endif

#ifdef _DEBUG
CClient* pGlblPtr = 0;
#endif

#define MAX_QUEUE_DEPTH	2

wchar_t plibtest[] = { INI_MAC };//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//WAITORTIMERCALLBACK Callback,      // triggers on an object ready in the queue
VOID CALLBACK gfWaitOrTimerCallback(
      PVOID   lpParameter,      // thread data(Context)
      waitStatus_t TimerOrWaitFired)  
{	
	CXmtrComm* pPipeInfc = (CXmtrComm*)lpParameter;
	// execute this class's send event callback
	if (pPipeInfc != NULL)
	{
		pPipeInfc->sendXlateTask();// will send the packet in prper form out the dll or pipe
	}
	// loops to wait again forever
	return;

	// translates it into the client slavemsg
	// signals the client that it needs to write it.
}


CXmtrComm::CXmtrComm(char* pComm) : pCurDev(NULL), hRegisteredTask(0)
{
	// can't do much since the infrastructure is not guaranteed till initComm
	cpRcvService = NULL;
	STARTLOG << " - CXmtrComm instantiation."<<ENDLOG;
	// used as: cpRcvService->serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo);
	// in this app, will trigger a new send for the reply
	if ( pComm != NULL && pComm[0] == 'C' && pComm[1] == 'O' && pComm[2] == 'M')
	{
		portNumber = atoi( pComm+3 );
	}
	else
	{
		portNumber = 0;
	}
	msgRcvr.setComm(this);
	commloginputname.empty();

	pktXmitQueue.initialize(16); 
}

CXmtrComm::~CXmtrComm()
{
	if (pCommClient != NULL)
	{
		HSlaveDisconnect(pCommClient);// kills the pipe deletes as required
	}
	if ( hRegisteredTask != NULL )
	{
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
		UnregisterWaitEx(hRegisteredTask, 0);// REGISTERED_WAIT..changed to Ex version 18jul12
#else
		UnregisterWait(hRegisteredTask);// kills the background task
#endif
	}
}

#include "XmtrDD.h" // the winapp with commandline info

string tstr;// a temp string
string CXmtrComm::readInString(FILE *pF)
{
	char buf[255];
	int  len = 0;
	// length
	fread(&len,1,1,pF);// string length
	// text
	fread(buf,1,len,pF);// read name
	buf[len] = '\0';
	tstr = buf;
	return tstr;
}

void CXmtrComm::readInCommLog(void)
{
	if ( commloginputname.empty() || cpRcvService == NULL)
		return;
	// open the file
	FILE *fp = _tfopen(commloginputname.c_str(), _T("r"));
	if (!fp) 
	{	// problem opening file
		tclog << "File did not open. ("<< commloginputname << ")" <<endl;
		tcerr << "File did not open. ("<< commloginputname << ")" <<endl;
		return;
	}
	//size_t fread( void *buffer, size_t size, size_t count, FILE *stream );


	// read header
	int iIn = 0;
	char buf[255];
	 fread(&iIn,1,4,fp);
	 if ( !(iIn == 0x1ffff ) ) return;  // bad format
	 iIn = 0;
	fread(&iIn,2,1,fp);                // classname length
	fread(buf,1,iIn,fp);// read name
	buf[iIn] = '\0';
	iIn = 0;
	 fread(&iIn,1,4,fp);
	 if ( !(iIn == 0x1ffff) ) return;//bad format
	 iIn = 0;
	fread(&iIn,2,1,fp);// classname length
	fread(buf,1,iIn,fp);// read name
	buf[iIn] = '\0';
	iIn = 0;
	string seqNum, str;
	// first one is not prefaced
	hPkt stxPkt,ackPkt;

	int pktSt = 0;
	do
	{
		iIn = 0;
		// read lines
		seqNum = readInString(fp);
		str    = readInString(fp); 
clog << seqNum << " " << str << endl;	
		if ( str.find("Stx") != string::npos )
		{
			string2Pkt( &stxPkt, str);
			pktSt = 1; // if there is an ack, lose it
		}
		else
		if ( str.find("Ack") != string::npos )
		{
			string2Pkt( &ackPkt, str);
			if (ackPkt.cmdNum == stxPkt.cmdNum && pktSt == 1)
				pktSt = 3;
			// otherwise we be out of sync...keep going
		}
		else
		{// error - unknown packet type or shutting down
			break; // out of while loop
		}
		if ( pktSt == 3 )
		{//	handle Stx & Ack
			cpRcvService->servicePacketPair(&stxPkt,&ackPkt);
			pktSt = 0;
		}
	}
	while ( fread(&iIn,2,1,fp) == 1 && iIn == 0x8003);	// while not eof
	LOGIT(CLOG_LOG,"Read in file '%s'\n",commloginputname.c_str());
	fclose(fp);
}


// setup connection (called after instantiation of device)
RETURNCODE  CXmtrComm::initComm(void)
{// deal with the dll
	STARTLOG << " - XmtrComm. initComm  entry."<<ENDLOG;
	CONNECT_DATA localConnectData = {COM1,CT_DEVICE,msgRcvr.getHartMsgID(),NULL};
	
	// com1 default, use com2 on my desk machine
		
	localConnectData.ePort = (COMM_PORT)(portNumber - 1);// int to enum
	// stevev 1july10 - subtract one because all the xmtrllc's were opening one too high

	// set up the connect structure
	//		device (see CT_DEVICE ),notify event (see WM_HART_MSG), 
	//		port (see COMM_PORT),   pClient = NULL
	HRESULT hr = HSlaveConnect(&localConnectData);
	// save the Client ptr out of ConnectData 
	pCommClient = localConnectData.pClient;

	if (!SUCCEEDED(hr))
	{
		cerr << "ERROR: Connect:  Link Layer Connection Failed.("<< hr <<")"<<endl;
		return FAILURE;
	}
	_ASSERT(pCommClient);
#ifdef _DEBUG
pGlblPtr = pCommClient;
#endif

	CDDdeviceDataApp* p_App = (CDDdeviceDataApp*)AfxGetApp();
	if ( p_App != NULL )
	{
		if (! msgRcvr.Create(NULL,_T("msgrcvr"),WS_CHILD, CRect(0, 0, 2, 2), (CWnd*)p_App->pdDlg, 1234 ) )
		{//failure
			cerr << "ERROR: Failed to construct a hWnd in the message receiver."<<endl;
			HSlaveDisconnect(pCommClient);
			return FAILURE;
		}
		// see if we have a input commlog
		commloginputname = (*(p_App->p_Arguments))[_T('K')][_T("commLogFileName")];// entered or default
		Arguments::optionSOurce_t pclifnSrc = 
						   (*(p_App->p_Arguments))[_T('K')].getSrc(_T("commLogFileName"));
		if (pclifnSrc == Arguments::src_Default) commloginputname.erase();
	}

	hr = HSlaveConfigWnd(pCommClient, & msgRcvr /*CWnd* pWnd*/);

	// With the DLL we will also have to 
	//HSlaveConfigPoll
	//HSlaveConfigUid
	//HSlaveConfigTag
	//HSlaveConfigLongTag

	RegisterWaitForSingleObject(& hRegisteredTask, pktXmitQueue.GetArrivalEvent(), 
							gfWaitOrTimerCallback,(void*)this, INFINITE, WT_EXECUTEDEFAULT
#ifdef _DEBUG
							, "waitOrTimerCallback"
#endif
							);
	// generate background task to translate the xmit packets and send 'em thru to the pipe
	// using RegisterWait ForSingleObject(QueueEvent)

	STARTLOG << " - XmtrComm. initComm  exit."<<ENDLOG;
	return SUCCESS;
}

void  CXmtrComm::notifyAppVarChange(itemID_t changedItemNumber,NUA_t isChanged, long aTyp)
{/*unused*/
	if (pCurDev == NULL)
	{
		return;
	}
	hCitemBase* pItm = NULL;
	if (SUCCESS == pCurDev->getItemBySymNumber(changedItemNumber, &pItm) && pItm != NULL)
	{
		if ( pItm->IsVariable() )
		{
			((hCVar*)pItm)->CancelIt(); // copies real to display
		}
	}
};

// runs in its own background task 
// is an event and data translator for the reply write to the dll/pipe
// runs on a queue event detected
// translates the queue packet to a client slavereply packet
// triggers the send event in the dll or pipe handler
void CXmtrComm::sendXlateTask(void)
{
	hPkt* wrtPkt = pktXmitQueue.GetNextObject();
	if ( wrtPkt == NULL || pCommClient == NULL )
	{
		cerr << "ERROR: empty transmit queue or client on transmit event."<<endl;
	}
	else
	{// handle the packet
	
	//	  pktRcvdQueue.ReturnMTObject(wrtPkt); 
	//	  wrtPkt = NULL;
		STARTLOG <<"*got a xmit packet."<<ENDLOG;
		// the m_cAddr1 doubles as the pipe channel number in this app
		// overwrites mfg in reply pkt::> pCommClient->m_Msg.m_cAddr1     = wrtPkt->channelID;
		pCommClient->m_Msg.m_comm_channel = (unsigned char) wrtPkt->channelID;
	    pCommClient->m_Msg.m_cCommand     = wrtPkt->cmdNum;
		pCommClient->m_Msg.m_cByteCount   = wrtPkt->dataCnt;
		pCommClient->m_Msg.m_cDelimiter  |= 0x04;// be sure this is a response packet

		memcpy(pCommClient->m_Msg.m_caData, wrtPkt->theData, wrtPkt->dataCnt);

		pCommClient->m_pSlaveReply = &(pCommClient->m_Msg);
if (pCommClient->m_hLlcNotifyEvent == NULL)
{
	STARTLOG<<" -- no Notify event."<<ENDLOG;
#ifndef _USEXMTR_DLL
	STARTLOG <<" - sendXlateTask setting Pipe.Send event handle:"<<
												pCommClient->m_pPipe->hSendEvent<<ENDLOG;
	SetEvent(pCommClient->m_pPipe->hSendEvent); //Inform Link Layer that there is a reply
#else
	STARTLOG <<" - sendXlateTask setting Pipe.Send event handle:"<<
												pCommClient->m_hLlcNotifyEvent<<ENDLOG;
	SetEvent(pCommClient->m_hLlcNotifyEvent); //Inform Link Layer that there is a reply
#endif
}
else
{
	STARTLOG <<" - sendXlateTask setting Notify event handle:"<<pCommClient->m_hLlcNotifyEvent<<ENDLOG;
	SetEvent(pCommClient->m_hLlcNotifyEvent); //Inform Link Layer that there is a reply
}	

	}// endelse have a packet
	int  remaining = pktXmitQueue.ReturnMTObject(wrtPkt);
}


// gets an empty packet for generating a command
//		(gives caller ownership of the packet*)
hPkt* CXmtrComm::getMTPacket(bool pendOnMT) 
{
	return pktXmitQueue.GetEmptyObject(pendOnMT);
}
// returns the packet memory back to the interface 
//		(this takes ownership of the packet*)
void CXmtrComm::putMTPacket(hPkt* pPk)
{
	//clog << "CXmtrComm::putMTPacket called."<<endl;
	if (pPk != NULL)
	{
		pktXmitQueue.ReturnMTObject(pPk);
	}
}


// puts a packet into the send queue  (takes ownership of the packet*)
RETURNCODE	CXmtrComm::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	//return (SendCmd(pPkt->cmdNum, &(pPkt->theData[0]),pPkt->dataCnt,timeout,cmdStatus) );
	int r = pktXmitQueue.PutObject2Que(pPkt); // sends the que'd event
	return SUCCESS;
}

RETURNCODE	
CXmtrComm::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
	hPkt*  pPkt =  getMTPacket(true);//pend
	if ( pPkt == NULL )
	{
		return FAILURE;
	}

	pPkt->channelID = (int)cmdStatus.isPri; // Pri doubles as channel number
	pPkt->cmdNum = cmdNum;
	pPkt->dataCnt= dataCnt; 
	memcpy(pPkt->theData,pData,dataCnt);

	return SendPkt(pPkt, timeout, cmdStatus);
}

int CXmtrComm::sendQsize(void) 	// how many are in the send queue ( 0 == empty )
{
	STARTLOG << "CXmtrComm::sendQsize called."<<ENDLOG;
	return (MAX_QUEUE_DEPTH);
}

int	CXmtrComm::sendQmax(void)		// how many will fit
{
	return (MAX_QUEUE_DEPTH);
}


void CXmtrComm::msgToReceive()
{
	hPkt*     pPkt = NULL;
	cmdInfo_t CommandInfo;  CommandInfo.clear();
	/* stevev 06jul10 - the xmtrllc stack says it's our responsibility to check the address*/
	if ( ! ourAddr(pCommClient->m_pHartMsg) )
	{
		return;
	}


	pPkt = pktXmitQueue.GetEmptyObject();// use same queue for receive packets
	parseMsg2PktNinfo(pCommClient->m_pHartMsg, pPkt, &CommandInfo);

	if ( cpRcvService != NULL )
	{
		cpRcvService->serviceReceivePacket(pPkt,&CommandInfo);// dispatcher rcv service
		// we are done with the packet, get rid of it
		pktXmitQueue.ReturnMTObject( pPkt );
	}
	else
	{
		cerr << "ERROR:  received a packet but had no dispatch service to handle it."<<endl;
	}
}

/* stevev 06jul10 - the xmtrllc stack says it's our responsibility to check the address*/
bool CXmtrComm::ourAddr(CHartMsg* pHrtMsg) 
{
	CValueVarient PollAddr, mfgNumber, extDevType, uniqueID, univ;

	if ( pCurDev != NULL )
	{
		if ( pHrtMsg->IsLongAddress() )
		{// verify long address
			mfgNumber  = getValue(MANUFACTURER_SYMID);
			extDevType = getValue(DEVICETYPE_SYMID);
			uniqueID   = getValue(DEVICEID_SYMID);
			univ       = getValue(UNIVERSALREV_SYMID);
			if ( 	mfgNumber.vIsValid && extDevType.vIsValid && 
					uniqueID.vIsValid  && univ.vIsValid			)
			{
				unsigned unique[3] , uid = (int)uniqueID;
				unique[0] = (uid & 0xff0000)>>16;
				unique[1] = (uid & 0x00ff00)>>8;
				unique[2] =  uid & 0x0000ff;
				if ( ((int)univ) < 7 )	// assume 5 & 6 are mfg + devtype
				{
					if ( (pHrtMsg->m_cAddr1 & MFR_ADDR) == (((int)mfgNumber)&MFR_ADDR) )
					{
						if ( pHrtMsg->m_cDevType  == (((int)extDevType)&0xff) )
						{
							if ( (pHrtMsg->m_caDevID[0] == unique[0]) &&
								 (pHrtMsg->m_caDevID[1] == unique[1]) &&
								 (pHrtMsg->m_caDevID[2] == unique[2])   )
							{
								return true;
							}
							// all others return false
						}
					}
				}
				else // hart 7 or above
				{
					unsigned extended = 
					( (pHrtMsg->m_cAddr1 & MFR_ADDR) << 8) |  ( pHrtMsg->m_cDevType );
					
					if ( extended  == (((int)extDevType)&0x3fff) )
					{
						if ( (pHrtMsg->m_caDevID[0] == unique[0]) &&
							 (pHrtMsg->m_caDevID[1] == unique[1]) &&
							 (pHrtMsg->m_caDevID[2] == unique[2])   )
						{
							return true;
						}
						// all others return false
					}
				}
			}
		}
		else
		{// verify polling address
			PollAddr = getValue(POLLINGADDRESS_SYMID);
			if ( PollAddr.vIsValid )
			{
				if ( (pHrtMsg->m_cAddr1 & POLLING_ADDR) == ((int)PollAddr) )
				{// 63 place address matches the one set in UI
					return true;
				}
				// everything else returns false
			}
		}
	}
	//	else.... we don't exist yet
	return false;
}

CValueVarient  CXmtrComm::getValue(unsigned symNum)
{
	CValueVarient empty;
	hCVar* pV = NULL;
	pV = pCurDev->getVarPtrBySymNumber(symNum);
	if ( pV == NULL )
		return empty;
	else
		return ( pV->getDispValue() );
}

RETURNCODE  CXmtrComm::GetIdentity(Indentity_t& retIdentity, BYTE pollAddr) 
{
	cerr << "ERROR: call to unimplemented CXmtrComm::GetIdentity."<<endl;
	return FAILURE;
}

// move the data 
int CXmtrComm::parseMsg2PktNinfo(CHartMsg* pHmsg, hPkt* pPkt, cmdInfo_t* pCI)
{
	int rc = 0;
	pPkt->channelID = pHmsg->m_cAddr1;
	pPkt->cmdNum    = pHmsg->m_cCommand;
	pPkt->dataCnt   = pHmsg->m_cByteCount;

	memcpy(pPkt->theData, pHmsg->m_caData, pPkt->dataCnt);

	return rc;
}

//	long channelID; // pipe instance number or comm specific number (ie URL to device)
//	int  cmdNum;
//	BYTE dataCnt; 
//	BYTE theData[256];


void CXmtrComm::sendErrorPkt(hPkt* pReqPkt, response5Code_t rc,UINT8 rStat)
{
	if (pReqPkt == NULL)
	{
		LOGIT(CLOG_LOG,L" - - sendErrorPkt: No Xmit Packet available.\n");
		LOGIT(CERR_LOG,L"ERROR: an error packet was requested without a request pkt.\n");
	}
	else
	{
		LOGIT(CLOG_LOG,L" - - sendErrorPkt: cmd= 0x%04x rc= 0x%02x status = 0x%02x\n",
											pReqPkt->cmdNum, rc, (unsigned int)rStat);
		hPkt* pP = pktXmitQueue.GetEmptyObject();
		if ( pP == NULL ) // nasty error
		{// can't do anything about it
			LOGIT(CLOG_LOG,L" - - sendErrorPkt: No Xmit Packet available.\n");
			LOGIT(CERR_LOG,L"ERROR: sendErrorPkt could not get a packet to send.\n");
			return;
		}
		else
		{
			pP->channelID = pReqPkt->channelID;
			pP->cmdNum    = pReqPkt->cmdNum;
			pP->dataCnt   = 2;
			pP->theData[0]= (BYTE)rc;
			pP->theData[1]= (BYTE)rStat;

			LOGIT(CLOG_LOG,L" - - insert error packet to xmit queue \n");
			int r = pktXmitQueue.PutObject2Que(pP); // sends the que'd event
		}
	}	
	return;
}
/*Vibhor 220104:Start of Code*/

RETURNCODE CXmtrComm::SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	return SUCCESS;
}

/*Vibhor 220104:End of Code*/


/*Vibhor 300104:Start of Code*/

void CXmtrComm::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
	return;
}
/*Vibhor 300104:End of Code*/
/*************************************************************************************************
 * NOTES:
 *	This is actually a half duplex channel since a pend may only deal with the last overlapped
 *		request. 
 *  
 *************************************************************************************************
 */

//int sscanf( const char *buffer, const char *format [, argument ] ... );

// returns byte count and packet filled or -x for an error [ 0 is a viable byte cnt ]
int CXmtrComm::string2Pkt(hPkt* pPkt, string& str)
{
	int bytecnt =-2;
	int tmp     = 0, k = 0;
	int loc     =-1, i = 0;

	if (pPkt == 0 ) return -1;

	loc = str.find("Stx");// 0 based index
	if ( loc >0 && loc != string::npos )
	{// we have a request packet
		k = sscanf(str.substr(loc+5,4).c_str(),"%d",&tmp);
		if (k) 
			pPkt->cmdNum = tmp;
		else
			return -1;
		loc += 11;
		
		k = sscanf(str.substr(loc,2).c_str(),"%d",&tmp);
		if (k)
			pPkt->dataCnt = bytecnt = tmp;
		else
			return -1;
		loc += 10;

		while ( bytecnt > 0 )
		{
			k = sscanf(str.substr(loc,2).c_str(),"%x",&tmp);
			if (k)
			{	
				pPkt->theData[i++] = tmp;
				bytecnt--;
				loc += 2;
			}			
			else	// an error - should never happen
				return -1;
			if (((unsigned)loc) > str.length() && bytecnt > 0 )
			{
				LOGIT(CERR_LOG,"ERROR parsing comm log STX bytes.\n");
				return -1;
			}
			if ( bytecnt > 0 && 
				 str.substr(loc,1) == " " && 
				 str.length() >= (unsigned)(loc + 2)  )
			{
				loc += 2;
			}
		}//wend
	}
#ifdef _DEBUG
	string daSubstr;
#endif
	loc = str.find("Ack");// 0 based index
	if ( loc >0 && loc != string::npos )
	{// we have a reply packet
#ifdef _DEBUG
		daSubstr = str.substr(loc+5,4);
#endif
		k = sscanf(str.substr(loc+5,4).c_str(),"%d",&tmp);
		if (k) 
			pPkt->cmdNum = tmp;
		else
			return -1;
		loc += 11;
		
#ifdef _DEBUG
		daSubstr = str.substr(loc,2);
#endif
		k = sscanf(str.substr(loc,2).c_str(),"%d",&tmp);
		if (k)
			pPkt->dataCnt = bytecnt = tmp;
		else
			return -1;
		loc += 3;// get ready for response code
		
		while ( bytecnt > 0 )
		{
#ifdef _DEBUG
		daSubstr = str.substr(loc,2);
#endif
			k = sscanf(str.substr(loc,2).c_str(),"%x",&tmp);
			if (k)
			{	
				pPkt->theData[i++] = tmp;
				bytecnt--;
				loc += 2;
			}			
			else	// an error - should never happen
				return -1;
			if ((unsigned)loc > str.length() && bytecnt > 0 )
			{
				LOGIT(CERR_LOG,"ERROR parsing comm log STX bytes.\n");
				return -1;
			}
			while ( bytecnt > 0 && 
				    str.substr(loc,1) == " " && 
					str.length() >= (unsigned)(loc + 1))
			{
				loc ++;
			}
		}
	}
	if ( pPkt->dataCnt != i )
		LOGIT(CLOG_LOG|CERR_LOG,"ERROR commlog packet did not get the correct number of bytes.\n");
	return i;
}




/*************************************************************************************************
 *
 *   $History: XmtrComm.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:58a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Milestone: Xmtr handles commands via pipes, IDE sends Cmd Zero
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:15p
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * 
 *************************************************************************************************
 */


