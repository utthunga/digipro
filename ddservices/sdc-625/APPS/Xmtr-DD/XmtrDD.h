/*************************************************************************************************
 *
 * $Workfile: XmtrDD.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		main header file for the Xmtr-DD application
 *		
 *
 * #include "XmtrDD.h"
 */

#if !defined(AFX_DDDEVICEDATA_H__BDA9B89E_EDA1_49E0_A43A_77C2D346BCB4__INCLUDED_)
#define AFX_DDDEVICEDATA_H__BDA9B89E_EDA1_49E0_A43A_77C2D346BCB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif
#ifdef INC_DEBUG
#pragma message("In XmtrDD.h") 
#endif

#include "resource.h"		// main symbols
#include "args.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::XmtrDD.h") 
#endif

#define INI_MAC  L"new_name.ini"	/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/////////////////////////////////////////////////////////////////////////////
// CDDdeviceDataApp:
// See DDdeviceData.cpp for the implementation of this class
//

class CdataPropertySheet; // forward reference

class CDDdeviceDataApp : public CWinApp
{
public:
	CDDdeviceDataApp();
	~CDDdeviceDataApp();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDdeviceDataApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	CdataPropertySheet* pdDlg;
	DDSargs*            p_Arguments;

	//{{AFX_MSG(CDDdeviceDataApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDDEVICEDATA_H__BDA9B89E_EDA1_49E0_A43A_77C2D346BCB4__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: XmtrDD.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */