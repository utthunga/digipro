#if !defined(AFX_WNDMSGRCVR_H__E71CE038_8149_4F26_A639_D79762FCEF18__INCLUDED_)
#define AFX_WNDMSGRCVR_H__E71CE038_8149_4F26_A639_D79762FCEF18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WndMsgRcvr.h : header file
/* WM_USER ==  0x400 = 1024 */
#define WM_HART_MSG  WM_USER+1


class CXmtrComm;

/////////////////////////////////////////////////////////////////////////////
// CWndMsgRcvr window

class CWndMsgRcvr : public CWnd
{
// Construction
public:
	CWndMsgRcvr();

// Attributes
public:
	CXmtrComm* pComm;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWndMsgRcvr)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWndMsgRcvr();

	DWORD getHartMsgID(void) { return WM_HART_MSG;};
	void  setComm(CXmtrComm* pC){pComm = pC;};

	// Generated message map functions
protected:
	//{{AFX_MSG(CWndMsgRcvr)
	afx_msg LRESULT OnHartMsg(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WNDMSGRCVR_H__E71CE038_8149_4F26_A639_D79762FCEF18__INCLUDED_)
