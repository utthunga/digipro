# Microsoft Developer Studio Project File - Name="XmtrDD" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=XmtrDD - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "XmtrDD.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "XmtrDD.mak" CFG="XmtrDD - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "XmtrDD - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "XmtrDD - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "XmtrDD - Win32 Release Static" (based on "Win32 (x86) Application")
!MESSAGE "XmtrDD - Win32 ReleaseWithSymbols" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DD Tools/DDB/Apps/XmtrDD", VGNBAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Zi /Ot /I ".\\" /I "..\..\Common" /I "..\..\CommonClasses\Arguments\include" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\ddbRead\ddbLib" /I "..\..\CommonClasses\ResizableLib" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\APPsupport\XmtrPipe" /I "..\APPsupport\DevServices" /I "..\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\DevServices" /I "..\APPsupport\ParserInfc" /I "..\..\Communications\include" /D "_UNICODE" /D "UNICODE" /D "REQUIREDDNAME" /D "XMTR" /D "XMTRDD" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /FD /I /Zm999 /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386 /nodefaultlib:"nafxcw" /nodefaultlib:"uafxcw" /nodefaultlib:"mfc42" /nodefaultlib:"mfcs42" /nodefaultlib:"LIBC" /libpath:"kernel32.lib"
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /Zi /Od /I ".\\" /I "..\APPsupport\XmtrPipe" /I "..\..\Common" /I "..\..\CommonClasses\Arguments\include" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\ddbRead\ddbLib" /I "..\..\CommonClasses\ResizableLib" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\APPsupport\DevServices" /I "..\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\DevServices" /I "..\APPsupport\ParserInfc" /I "..\..\Communications\include" /D "_UNICODE" /D "UNICODE" /D "XMTR" /D "REQUIREDDNAME" /D "XMTRDD" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /FD /GZ /Zm800 /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /nodefaultlib:"nafxcwd" /nodefaultlib:"uafxcwd" /nodefaultlib:"mfc42d" /nodefaultlib:"mfcs42d" /nodefaultlib:"LIBCD" /nodefaultlib:"MSVCRT" /pdbtype:sept /libpath:"kernel32.lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "XmtrDD___Win32_Release_Static"
# PROP BASE Intermediate_Dir "XmtrDD___Win32_Release_Static"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "XmtrDD___Win32_Release_Static"
# PROP Intermediate_Dir "XmtrDD___Win32_Release_Static"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /Zi /O2 /I ".\\" /I "..\..\Common" /I "..\..\Common Classes\Arguments\include" /I "..\..\Common Classes\ChkListCtrl" /I "..\..\Common Classes\ResizableLib" /I "..\..\Common Classes\ChkListCtrl\EditClasses" /I "..\XmtrPipe" /I "..\..\ddbRead\ddbLib" /D "REQUIREDDNAME" /D "XMTRDD" /D "DBGHEAP" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /YX /FD /I /Zm2000 /c
# ADD CPP /nologo /Zp1 /MDd /W3 /GX /Zi /O2 /I ".\\" /I "..\..\Common" /I "..\..\Common Classes\Arguments\include" /I "..\..\Common Classes\ChkListCtrl" /I "..\..\Common Classes\ResizableLib" /I "..\..\Common Classes\ChkListCtrl\EditClasses" /I "..\XmtrPipe" /I "..\..\ddbRead\ddbLib" /I "..\..\Apps\Appsupport\DDParser" /D "REQUIREDDNAME" /D "XMTRDD" /D "DBGHEAP" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FD /I /Zm2000 /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/XmtrDD.exe"
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/XmtrDD.exe"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "XmtrDD___Win32_ReleaseWithSymbols"
# PROP BASE Intermediate_Dir "XmtrDD___Win32_ReleaseWithSymbols"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "XmtrDD___Win32_ReleaseWithSymbols"
# PROP Intermediate_Dir "XmtrDD___Win32_ReleaseWithSymbols"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /Zi /Ot /I ".\\" /I "..\..\Common" /I "..\..\CommonClasses\Arguments\include" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\ddbRead\ddbLib" /I "..\..\CommonClasses\ResizableLib" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\APPsupport\XmtrPipe" /I "..\APPsupport\DevServices" /I "..\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\DevServices" /I "..\APPsupport\ParserInfc" /D "REQUIREDDNAME" /D "XMTRDD" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /FD /I /Zm800 /c
# SUBTRACT BASE CPP /Ow
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Zi /Ot /I ".\\" /I "..\..\Common" /I "..\..\CommonClasses\Arguments\include" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\ddbRead\ddbLib" /I "..\..\CommonClasses\ResizableLib" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\APPsupport\XmtrPipe" /I "..\APPsupport\DevServices" /I "..\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\DevServices" /I "..\APPsupport\ParserInfc" /D "_UNICODE" /D "UNICODE" /D "REQUIREDDNAME" /D "XMTR" /D "XMTRDD" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /FD /I /Zm800 /c
# SUBTRACT CPP /Ow
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"uafxcw.lib"
# ADD LINK32 kernel32.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /nodefaultlib:"nafxcw" /nodefaultlib:"uafxcw" /nodefaultlib:"mfc42" /nodefaultlib:"mfcs42" /nodefaultlib:"libc"

!ENDIF 

# Begin Target

# Name "XmtrDD - Win32 Release"
# Name "XmtrDD - Win32 Debug"
# Name "XmtrDD - Win32 Release Static"
# Name "XmtrDD - Win32 ReleaseWithSymbols"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\CommonClasses\Arguments\src\argcargv.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\Arguments\src\Arguments.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\CheckComboBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\ChkListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\ClrComboBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\comboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\dataPropertyList.cpp
# End Source File
# Begin Source File

SOURCE=.\dataPropertySheet.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /Od

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD BASE CPP /Od
# ADD CPP /Od

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DDaboutDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\APPsupport\DevServices\ddb_EventMutex.cpp
# End Source File
# Begin Source File

SOURCE=..\APPsupport\DevServices\ddb_XmtrDisp.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /Ze /Od

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD BASE CPP /Ze /Od /D "INC_DEBUG"
# ADD CPP /Ze /Od

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ddl_ini.cpp
# End Source File
# Begin Source File

SOURCE=.\DDlistData.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /MD /Ze /Gi- /Od /Ob0
# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

# SUBTRACT BASE CPP /YX
# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD BASE CPP /MD /Ze /Gi- /Od /Ob0 /D "INC_DEBUG"
# SUBTRACT BASE CPP /YX
# ADD CPP /MD /Ze /Gi- /Od /Ob0
# SUBTRACT CPP /YX

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDselect.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDselectSplitter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDSelectTree.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\EditCell.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtendedInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\SDC625\FileSupport.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\ListData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\logging.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /vd1 /Od /Ob0

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD BASE CPP /vd1 /Od /Ob0
# ADD CPP /vd1 /Od /Ob0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\common\registerWaits.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# SUBTRACT CPP /YX /Yc

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

# ADD CPP /Yc"stdafx.h"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# SUBTRACT BASE CPP /YX /Yc
# SUBTRACT CPP /YX /Yc

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\COMMON\strptime.cpp
# End Source File
# Begin Source File

SOURCE=.\WndMsgRcvr.cpp
# End Source File
# Begin Source File

SOURCE=.\xMethodSupport.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /I "..\APPsupport\MEE" /I "..\APPsupport\Interpreter" /I "..\APPsupport\BuiltinLib"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

# ADD CPP /I "..\APPsupport\MEE" /I "..\APPsupport\Interpreter" /I "..\APPsupport\BuiltinLib"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

# ADD CPP /I "..\..\CommonClasses\Arguments\include" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\ResizableLib" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\APPsupport\XmtrPipe" /I "..\APPsupport\DevServices" /I "..\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\DevServices" /I "..\APPsupport\ParserInfc" /I "..\APPsupport\MEE" /I "..\APPsupport\Interpreter" /I "..\APPsupport\BuiltinLib"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD CPP /I "..\APPsupport\MEE" /I "..\APPsupport\Interpreter" /I "..\APPsupport\BuiltinLib"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XmtrComm.cpp

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# ADD CPP /Od

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# ADD BASE CPP /Od
# ADD CPP /Od

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\XmtrDD.cpp
# End Source File
# Begin Source File

SOURCE=.\XmtrDD.rc
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\SDC625\args.h

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\args.h
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\Arguments\include\Arguments.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\EditClasses\CheckComboBox.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\ChkListCtrl.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\chkListEdit.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\EditClasses\comboBox.h"
# End Source File
# Begin Source File

SOURCE=.\dataPropertyList.h
# End Source File
# Begin Source File

SOURCE=.\dataPropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\DDaboutDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddb.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddb_XmtrDispatch.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbAttributes.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbCollAndArr.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbCommand.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbCommInfc.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbConditional.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbCondResolution.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbdefs.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbDeviceMgr.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbDeviceService.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbEditDisplay.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbEvent.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbGeneral.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbGlblSrvInfc.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbItemBase.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbItemLists.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbItems.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbMenu.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbMethod.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbMsgQ.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbMutex.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\DDBpayload.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbPolicy.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbPrimatives.h
# End Source File
# Begin Source File

SOURCE=..\..\common\ddbQueue.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbReferenceNexpression.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbResponseCode.h
# End Source File
# Begin Source File

SOURCE=..\..\ddbRead\ddbLib\ddbVar.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\DDLDEFS.H
# End Source File
# Begin Source File

SOURCE=.\DDlistData.h
# End Source File
# Begin Source File

SOURCE=.\DDSelect.h
# End Source File
# Begin Source File

SOURCE=.\DDselectSplitter.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\dllapi.h
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\EditClasses\EditCell.h"
# End Source File
# Begin Source File

SOURCE=..\..\Common\foundation.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\HARTsupport.h
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\ListData.h"
# End Source File
# Begin Source File

SOURCE=..\..\Common\NamedPipe.h
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableGrip.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableLayout.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableMinMax.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableMsgSupport.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizablePage.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableSheet.h"
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ResizableLib\ResizableState.h"
# End Source File
# Begin Source File

SOURCE=.\SelectTree.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\varient.h
# End Source File
# Begin Source File

SOURCE=.\WndMsgRcvr.h
# End Source File
# Begin Source File

SOURCE=.\XmtrComm.h
# End Source File
# Begin Source File

SOURCE=.\XmtrDD.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00022.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icone.ico
# End Source File
# Begin Source File

SOURCE=.\res\XmtrDD.ico
# End Source File
# Begin Source File

SOURCE=.\res\XmtrDD.rc2
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# Begin Source File

SOURCE=.\hlp\AfxDlg.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\XmtrDD.cnt

!IF  "$(CFG)" == "XmtrDD - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\Release
InputPath=.\hlp\XmtrDD.cnt
InputName=XmtrDD

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\Debug
InputPath=.\hlp\XmtrDD.cnt
InputName=XmtrDD

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 Release Static"

# PROP BASE Ignore_Default_Tool 1
# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\XmtrDD___Win32_Release_Static
InputPath=.\hlp\XmtrDD.cnt
InputName=XmtrDD

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ELSEIF  "$(CFG)" == "XmtrDD - Win32 ReleaseWithSymbols"

# PROP BASE Ignore_Default_Tool 1
# PROP Ignore_Default_Tool 1
# Begin Custom Build - Copying contents file...
OutDir=.\XmtrDD___Win32_ReleaseWithSymbols
InputPath=.\hlp\XmtrDD.cnt
InputName=XmtrDD

"$(OutDir)\$(InputName).cnt" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	copy "hlp\$(InputName).cnt" $(OutDir)

# End Custom Build

!ENDIF 

# End Source File
# End Group
# End Target
# End Project
