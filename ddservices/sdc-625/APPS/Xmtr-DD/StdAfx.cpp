// stdafx.cpp : source file that includes just the standard includes
//	DDdeviceData.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "TCHAR.H"
#include "stdafx.h"




/* global generic code*/
// stevev - WIDE2NARROW char interface
int __fastcall UnicodeToASCII(LPCWSTR pszW, LPSTR pRetA, int lenRetA)
{
	ULONG cCharacters;
    DWORD dwError;

	if (pRetA == NULL || lenRetA <= 0 )
	{
		return E_INVALIDARG;
	}
    // If input is null then just return empty string.
    if (pszW == NULL)
    {
        pRetA = '\0';
        return NOERROR;
    }

    cCharacters = wcslen(pszW)+1;
    // Determine number of bytes to be allocated for ANSI string. 
    // Convert to ANSI.
    if (0 == WideCharToMultiByte(CP_ACP,			//UINT CodePage
									0,				//DWORD dwFlags
									pszW,			//LPCWSTR lpWideCharStr
									cCharacters,	//int cchWideChar
									pRetA,			//LPSTR lpMultiByteStr
									lenRetA,		//int cbMultiByte
									NULL,			//LPCSTR lpDefaultChar
									NULL)			//LPBOOL lpUsedDefaultChar
		)
    {
        dwError = GetLastError();
        pRetA = '\0';
        return HRESULT_FROM_WIN32(dwError);
    }
    return NOERROR;
}

int __fastcall UnicodeToASCII(CString &cstr, LPSTR pRetA, int lenRetA)
{
#ifdef _UNICODE
	return (UnicodeToASCII((LPCTSTR)cstr,pRetA,lenRetA));
#else  // NOT UNICODE
	return (E_NOINTERFACE);
#endif
}

int __fastcall AsciiToUnicode(LPCSTR pszA, LPWSTR pszW,  int lenRetW)
{
    ULONG cCharacters;
    DWORD dwError;

	if (pszW == NULL || lenRetW <= 0 )
	{
		return E_INVALIDARG;
	}
    // If input is null then just return empty string.
    if (pszA == NULL)
    {
        pszW = L'\0';
        return NOERROR;
    }

    // Determine number of wide characters to be allocated for the
    // Unicode string.
    cCharacters =  strlen(pszA)+1;

    // Covert to Unicode.
    if (0 == MultiByteToWideChar(CP_ACP, 0, pszA, cCharacters,
                  pszW, lenRetW))
    {
        dwError = GetLastError();
        pszW = L'\0';
        return HRESULT_FROM_WIN32(dwError);
    }

    return NOERROR;
}

int __fastcall AsciiToUnicode(LPCSTR pszA, CString& retStr)
{
    ULONG cCharacters;
    // If input is null then just return empty string.
    if (pszA == NULL)
    {
        retStr=L"";
        return NOERROR;
    }
    cCharacters =  strlen(pszA)+1;
#ifdef _UNICODE
	return (AsciiToUnicode(pszA,retStr.GetBuffer(cCharacters),cCharacters));
#else
	return (E_NOINTERFACE);
#endif
}