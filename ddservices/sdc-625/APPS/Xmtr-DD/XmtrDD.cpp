/*************************************************************************************************
 *
 * $Workfile: DDdeviceData.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 This si the Xmtr-DD main module
 */

#include "stdafx.h"
#include "resource.h"		// main symbols

#include "dataPropertySheet.h"
#include "XmtrDD.h"
#include "globals.h" // includes TLStorage.h
//#include "DDdeviceDataDlg.h"
//int QandA = 0;  see Arguments.cpp
#include "QA+Debug.h"

#include < conio.h >
#include < fstream >

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/* now in logging
static ofstream outRedir;
static ifstream in_Redir;
static ofstream errRedir;
static ofstream logRedir;

static streambuf* pHldOutBuf = NULL;
static streambuf* pHld_InBuf = NULL;
static streambuf* pHldErrBuf = NULL;
static streambuf* pHldLogBuf = NULL;


int resetFiles(void);
*/
/* now in logging.cpp
void LogMessageV(char *strMessage,va_list varArgList)
{
	CString tmpStr;
	tmpStr.FormatV(strMessage,varArgList);
	AfxMessageBox(tmpStr);
}
*/

// for affinity recordation
DWORD  processMask = 0;
DWORD  system_Mask = 0;
DWORD  newProcMask = 1;

// TEMP make this a global
BOOL m_doingIdle;
//extern wchar_t* plibtest;
//extern wchar_t* plibtest;
extern wchar_t plibtest[];

bool DumpOnly = false;
unsigned mainThreadID  = 0;

/////////////////////////////////////////////////////////////////////////////
// CDDdeviceDataApp

BEGIN_MESSAGE_MAP(CDDdeviceDataApp, CWinApp)
	//{{AFX_MSG_MAP(CDDdeviceDataApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDDdeviceDataApp construction

CDDdeviceDataApp::CDDdeviceDataApp()
{
	AfxEnableMemoryTracking(FALSE);
	// TODO: add construction code here,
	pdDlg = NULL; 
	p_Arguments = new DDSargs(_T("argclass"), _T(""), _T("-/") );
	// Place all significant initialization in InitInstance

	/* stevev 9-9-9 set the affinity for this process **/
	HANDLE thisProcessHandle = GetCurrentProcess();
	BOOL 
	success = GetProcessAffinityMask(thisProcessHandle,&processMask,&system_Mask);
	// calculate the the proper affinity here
	if ( (processMask & 0x0002) != 0 )
	{
		newProcMask = 2;// put xmtr on the other cpu if possible
	}
	success = SetProcessAffinityMask(thisProcessHandle,newProcMask);

	SetPriorityClass(thisProcessHandle, REALTIME_PRIORITY_CLASS);//HIGH_PRIORITY_CLASS); // to respond to serial requests
	HANDLE mainThread = GetCurrentThread();
	::SetThreadPriority(mainThread, THREAD_PRIORITY_HIGHEST);

}
CDDdeviceDataApp::~CDDdeviceDataApp()
{
	// TODO: add destruction code here,
	if (p_Arguments){ delete p_Arguments; p_Arguments= NULL;}


	
	cerr << "\nExiting.\n" << endl;
//	cout.flush();	if (pHldOutBuf != NULL ) cout.rdbuf(pHldOutBuf);
//					if (pHld_InBuf != NULL ) cin. rdbuf(pHld_InBuf);
//	cerr.flush();	if (pHldErrBuf != NULL ) cerr.rdbuf(pHldErrBuf);
//	clog.flush();	if (pHldLogBuf != NULL ) clog.rdbuf(pHldLogBuf);
	logExit();

}
/////////////////////////////////////////////////////////////////////////////
// The one and only CDDdeviceDataApp object

CDDdeviceDataApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDDdeviceDataApp initialization

BOOL CDDdeviceDataApp::InitInstance()
{
	prepTLS(sizeof(parseGlbl));// get the storage ready
	mainThreadID = THREAD_ID;
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	// check the arguments for the correct DD
	if  ( p_Arguments->Parse() )
	{// message box??
		CString msg("Command Line error.   \n");
		msg += p_Arguments->m_errString.c_str();
		AfxMessageBox(msg);
		p_Arguments->Usage();
		return FALSE;
	}
//	p_Arguments->Parse(m_lpCmdLine);
	// if there are no arguments or the specified DD was not found
	//		call the DDselector class
	//		if the selector returns failure, exit the application
	// load the variables from the specified DD
	// get a list of classes from the variables
	// generate a list of property pages for the found classes
	// generate an ALL property page
	// display the sheet

#ifdef _AFXDLL
	/*MSVC++ 9.0  _MSC_VER = 1500
	MSVC++ 8.0  _MSC_VER = 1400
	MSVC++ 7.1  _MSC_VER = 1310
	MSVC++ 7.0  _MSC_VER = 1300
	MSVC++ 6.0  _MSC_VER = 1200
	*/
#if _MSC_VER < 1300
	Enable3dControls();			// Call this when using MFC in a shared DLL
#endif /* else it isn't needed */
#else
#if _MSC_VER < 1300
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
#endif

	// stevev 19sep07 - now donw in logSetup()....was::> resetFiles();
	logSetup(*p_Arguments, GetCommandLine()) ;
	atexit( logExit ); // be sure we get rid of 'em 15aug08

	tstring arg, argMf, argDT, argDR, argdR;
	bool undefined = false;
	int  nUndef = 0;
	argMf = (*p_Arguments)[_T("MfgID")];  
	if (argMf == NOT_SUPPLIED)
	{ 
		undefined=true;
		nUndef++;
		argMf = _T("XX"); // for error string
	};
	// handle 6 digit manufacturer file path
	if (argMf.length() > 4 )
	{
		argMf = argMf.substr(argMf.length() - 4);
	}

	argDT = (*p_Arguments)[_T("DevType")];
	if (argDT == NOT_SUPPLIED)
	{ 
		undefined=true;
		nUndef++;
		argDT = _T("XX"); // for error string
	};
	argDR = (*p_Arguments)[_T("DevRev")]; 
	if (argDR == NOT_SUPPLIED)// length = 12
	{ 
		undefined=true;
		nUndef++;
		argDR = _T("XX"); // for error string
	};
	argdR = (*p_Arguments)[_T("DDRev")];  // default is actually ff to trigger 'get latest'

	char  caption[256];
	DD_Key_t    ulKey = 0, tmpLL;
	int   tmpint;
	Indentity_t initIdent;

	if ( undefined )
	{
		if ( nUndef < 3 )
		{// put up a message box 
			CString mb;
			mb.Format(_T("The Command Line DD spec '%s' was not complete.  "),arg.data());
			AfxMessageBox(mb);
		}
		//pick from list
		TRACE(_T("PICK FROM LIST\n"));// into ulKey
		ulKey = 0;
	}
	else
	{/*-------------------------------------------------------------------------------------*/

		if (argMf.length() == 4 && argDT.length() == 4 && (nUndef == 0))
		{ 
			if (_stscanf(argMf.substr(0,4).data(),_T("%x"),&tmpint) == 0 )
			{// didn't work
				ulKey = 0;
			}
			else
			{
				initIdent.wManufacturer = tmpint;
				if (_stscanf(argDT.substr(0,4).data(),_T("%x"),&tmpint) == 0 )
				{// didn't work
					ulKey = 0;
				}
				else
				{
					tmpLL = initIdent.wManufacturer;
					ulKey = tmpLL << 48;
					initIdent.wDeviceType = tmpint;// both worked
					//ulKey = (initIdent.wDeviceType << 16) +1; // add 1 incase mfg & devtype == 0
					tmpLL = initIdent.wDeviceType;
					ulKey |= ( tmpLL << 32) +1; // add 1 incase mfg & devtype == 0
					initIdent.cUniversalRev = 7;
					sprintf(caption,"%04x:%04x.",initIdent.wManufacturer,tmpint);
					caption[10] = '\0';
				}
			}
		}
		
		if (ulKey == 0)
		{
			if (_stscanf(argMf.substr(argMf.length()-2,2).data(),_T("%x"),&tmpint) == 0 )
			{// didn't work
				ulKey = 0;
			}
			else
			{
				initIdent.wManufacturer = tmpint;
				if (_stscanf(argDT.substr(argDT.length()-2,2).data(),_T("%x"),&tmpint) == 0 )
				{// didn't work
					ulKey = 0;
				}
				else
				{
					initIdent.wDeviceType = tmpint;// both worked
					//ulKey = ((initIdent.wManufacturer & 0x00ff) << 24) | 
					//		((initIdent.wDeviceType   & 0x00ff) << 16) | 1;// see note above
				/* doesn't work????
					ulKey = ((initIdent.wManufacturer & 0xffff) << 48) | 
							((initIdent.wDeviceType   & 0xffff) << 32) | 1;// see note above
				-- so try the following ****/
				// somehow this = 0x0000000000XX0000	ulKey = ((initIdent.wManufacturer & 0xffff) << 48);
					ulKey  = (initIdent.wManufacturer & 0xffff);
					ulKey  = ulKey << 48;
					DD_Key_t 
					tmpKy  = (initIdent.wDeviceType   & 0xffff);
					tmpKy  = tmpKy << 32;
					ulKey  = ulKey | tmpKy | 1;// see note above

					initIdent.cUniversalRev = 5;// assume 5 till told otherwise
					sprintf(caption,"%02x.%02x.",initIdent.wManufacturer,tmpint);
					caption[6] = '\0';
				}
			}
		}
		
		if (ulKey != 0) // we got the beginning
		{// all others:
	//	arg =	argMf.substr(argMf.length()-2,2)+
	//			argDT.substr(argDT.length()-2,2)+
	//			argDR.substr(argDR.length()-2,2)+
	//			argdR.substr(argdR.length()-2,2);

			if (_stscanf(argDR.substr(argDR.length()-2,2).data(),_T("%x"),&tmpint) == 0 )
			{// didn't work
				ulKey = 0;
			}
			else
			{
				initIdent.cDeviceRev = (tmpint & 0x00ff);// both worked
				//ulKey = (ulKey & 0xffff00ff) | ((initIdent.cDeviceRev   & 0x00ff) << 8);
				ulKey = (ulKey & 0xffffffff0000000f) | (initIdent.cDeviceRev << 16);
				sprintf(caption,"%s%02x.",caption,tmpint);
				
				if (_stscanf(argdR.substr(argdR.length()-2,2).data(),_T("%x"),&tmpint) == 0 )
				{// didn't work
					//ulKey = (ulKey & 0xffffff00) | (0x00ff);
					ulKey = (ulKey & 0xffffffff00ff0000) | (0x00ff);
					sprintf(caption,"%s..",caption);
				}
				else
				{// dd rev is not in identity, put it in the key
					//ulKey = (ulKey & 0xffffff00) | (tmpint & 0x00ff);
					ulKey = (ulKey & 0xffffffff00ff0000) | (tmpint & 0x00ff);
					initIdent.cSoftwareRev = (tmpint & 0x00ff);
					if (tmpint == 0xff)
					{sprintf(caption,"%s..",caption);}
					else
					{sprintf(caption,"%s%02x",caption,(tmpint & 0x00ff));}
				}
			}
		}
	}/*-------------------------------------------------------------------------------------*/

	if( (*p_Arguments)[_T('Y')]) 
	{
		DumpOnly = true;
	}
	else
	{
		DumpOnly = false;
	}

	Arguments::Option &qoption = (*p_Arguments)[_T('Q')];
	if( qoption) 
	{
		QandA = 0;
#ifndef _DEBUG
#pragma message("RELEASE version is disabling QandA variable!\n");
#else
		const tstring qa = qoption.getFirstArg()->getStrValue().c_str();
		unsigned tmp;
		if ( swscanf(qa.c_str()+2, L"%x", &tmp) == 1)
		{
			QandA = tmp;
		}
#endif
	}


// replaced with above processing
//	if ( undefined )
//	{
//		if ( nUndef < 3 )
//		{// put up a message box 
//			CString mb;
//			mb.Format("The Command Line DD spec '%s' was not complete.  ",arg.data());
//			AfxMessageBox(mb);
//		}
//		//pick from list
//		TRACE(_T("PICK FROM LIST\n"));// into ulKey
//		ulKey = 0;
//	}
//	else
//	{
//		if ( sscanf(arg.data(),"%x",&ulKey) == 0 ) // failed
//		{
//			ulKey = 0;
//		}
//	}
//clog <<"App init: key 0x" << hex << ulKey << dec << endl;	
LOGIT(CLOG_LOG,"App init: key 0x%016I64x \n",ulKey);

//#ifdef REQUIREDDNAME
#if 0

	if ( ulKey == 0 )
	{
		CString mb;
		mb.Format("The DD specification '%s' could not be converted to a number.",arg.data());
		AfxMessageBox(mb);
		return -1;
	}
#endif
// from ddbParserInfc.cpp
#define intFlgs_exactDDrevDesired 0x80	/* set to search for exact ddrev  (xmtr-dd)      */

	//                              caption
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
	CString lS;
	AsciiToUnicode(caption, lS);
	pdDlg = new CdataPropertySheet(lS);
#else
	pdDlg = new CdataPropertySheet(caption);
#endif
clog << "App init: Got a new dialog." << endl;	
	initIdent.cInternalFlags  = intFlgs_exactDDrevDesired;// we are xmtr-dd, we want what we ask for	
	initIdent.cInternalFlags |= intFlgs_Do_NOT_load_Dflt; // we are xmtr-dd -default does us no good what-so-ever
	pdDlg->setDDkey( ulKey );	
	pdDlg->setIdentity( initIdent );
//=++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	CString tmpStr;
	tmpStr.Format(L"got a define '%s'",plibtest);
	tmpStr = plibtest;
#ifdef X_DEBUG /* for now */
	AfxMessageBox(tmpStr);
#endif

	//m_pMainWnd = &dlg;
	m_pMainWnd = pdDlg;
	LOGIT(CLOG_LOG,L"App init: going to work.\n");	
	//int nResponse = dlg.DoModal();
	int nResponse = pdDlg->DoModal();// the application
//	if (nResponse == IDOK)
//	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
//	}
//	else if (nResponse == IDCANCEL)
//	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
//	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.

	return FALSE;
}

int CDDdeviceDataApp::ExitInstance() 
{
	LOGIT(CLOG_LOG,"< App::ExitInstance\n");
	logExit();
/*	if (pHld_InBuf != NULL )
	{
		cin. rdbuf(pHld_InBuf);
		RAZE(pHld_InBuf);
	}
*/
	return CWinApp::ExitInstance();
}

/* don't use -- isolate this functionality into logging.cpp - stevev 19sep07
//int resetFiles(DDSargs &rArgs)
int resetFiles(void)
{
	tstring  locStr;
	DDSargs* pArgs = ((CDDdeviceDataApp*)AfxGetApp())->p_Arguments;
	// * cin  will be the input file
	// * cout will be the output file <dflt stdout>
	// * cerr will be the error file  <dflt stderr>
	// * clog will be the logging file<dflt stderr>
	// * //
	//outRedir.open("dddd_out.txt" );
	outRedir.open((*pArgs)['O']["outputFileName"].c_str());
	if ( outRedir.is_open() )
	{
		pHldOutBuf = cout.rdbuf();
		cout.rdbuf(outRedir.rdbuf());
	}
	else
	{
		TRACE(_T("ERROR: output redirection failed.\n") );
	}	
cout << "This is a cout." << endl;

	//errRedir.open("dddd_err.txt");
	errRedir.open((*pArgs)['E']["errorFileName"].c_str());
	if ( errRedir.is_open() )
	{
		pHldErrBuf = cerr.rdbuf();
		cerr.rdbuf(errRedir.rdbuf());
	}
	else
	{
		TRACE(_T("ERROR: error file redirection failed.\n"));
	}
cerr << "---- Error file -----   (cerr)\n" << endl;

	//logRedir.open("dddd_log.txt");
	logRedir.open((*pArgs)['G']["logFileName"].c_str());
	if ( logRedir.is_open() )
	{
		pHldLogBuf = clog.rdbuf();
		clog.rdbuf(logRedir.rdbuf());
	}
	else
	{
		TRACE(_T("ERROR: logging file open failed.\n"));
	}
clog << "---- Log file -----   (clog)\n" << endl;
	return 0; // show all OK even if it's not (defaults will be used)
}
*** end don't use ***/

/*************************************************************************************************
 *
 *   $History: DDdeviceData.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
