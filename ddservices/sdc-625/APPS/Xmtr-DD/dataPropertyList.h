/*************************************************************************************************
 *
 * $Workfile: dataPropertyList.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		
 *
 * #include "dataPropertyList.h"
 */
#if !defined(AFX_DATAPROPERTYLIST_H__4AB2DE53_0C17_47CD_A191_72A46554DBD4__INCLUDED_)
#define AFX_DATAPROPERTYLIST_H__4AB2DE53_0C17_47CD_A191_72A46554DBD4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef INC_DEBUG
#pragma message("In dataPropertyList.h") 
#endif
// dataPropertyList.h : header file
//
#include "resource.h"
#include "ResizablePage.h"

#include "ChkListCtrl.h"
#include "DDlistData.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::dataPropertyList.h") 
#endif

/////////////////////////////////////////////////////////////////////////////
// CdataPropertyList dialog



class CdataPropertyList : public CResizablePage// : public CPropertyPage
{
	DECLARE_DYNCREATE(CdataPropertyList)

// Construction
public:
	CdataPropertyList();
	~CdataPropertyList();

// Dialog Data
	//{{AFX_DATA(CdataPropertyList)
	enum { IDD = IDD_PROPPAGE_ALL };
	CChkListCtrl m_cDataListCtrl;
	//}}AFX_DATA
//was
//	CListCtrl	m_cDataListCtrl;
	int initPage(CDDlistData* pDataList);

	//Added By Deepak : CAlled by property sheet chkbox handler
	int OnShowLocalClass(CDDlistData* pDDataList);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CdataPropertyList)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
//	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
protected:

	CDDlistData* pDataList;

	// Generated message map functions
	//{{AFX_MSG(CdataPropertyList)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedDataList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnApplyNow();
	afx_msg void OnCancelDiffs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
//Added by Deepak
// The lViewList will maintain index of pDataList so that only members which belong to this class are 
// shown in the UI The value to be displayed at nth  the row is obtained by
// RowList.GetElementAt(ViewList.GetElementAt(nRow))
//	This should be filled while FillItems Call and then on wards used subsequently
	CArray<int,int> ViewList;

//END Modification
};

#ifdef INC_DEBUG
#pragma message("    Finished Including::dataPropertyList.h") 
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAPROPERTYLIST_H__4AB2DE53_0C17_47CD_A191_72A46554DBD4__INCLUDED_)


/*************************************************************************************************
 *
 *   $History: dataPropertyList.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:39a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header & footer to HART standards
 * 
 *************************************************************************************************
 */
