/*************************************************************************************************
 *
 * $Workfile: dataPropertyList.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 
 */

#include "stdafx.h"
#include "XmtrDD.h"
#include "chkListEdit.h "
#include "DDlistData.h"  // the sheet owns the data
#include "dataPropertyList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CdataPropertyList property page

IMPLEMENT_DYNCREATE(CdataPropertyList, CResizablePage)//, CPropertyPage)

CdataPropertyList::CdataPropertyList() : CResizablePage(CdataPropertyList::IDD)//: CPropertyPage(CdataPropertyList::IDD)
{
	pDataList = NULL;

	//{{AFX_DATA_INIT(CdataPropertyList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CdataPropertyList::~CdataPropertyList()
{
}

void CdataPropertyList::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CdataPropertyList)
	DDX_Control(pDX, IDC_LIST1, m_cDataListCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CdataPropertyList, CPropertyPage)
	//{{AFX_MSG_MAP(CdataPropertyList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnItemchangedDataList)
	ON_WM_SIZE()
	ON_BN_CLICKED(ID_APPLY_NOW, OnApplyNow)
	ON_BN_CLICKED(ID_CANCEL_DIFFS, OnCancelDiffs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CdataPropertyList message handlers

BOOL CdataPropertyList::OnSetActive() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//return CPropertyPage::OnSetActive();
// Added by Deepak : This tab will only show the data belonging to this class
	if(pDataList)
		pDataList ->pViewList = &ViewList; 
//END Modification
	
	((CPropertySheet*)GetParent())->SetWizardButtons(PSWIZB_NEXT);
	
	return CResizablePage::OnSetActive();
}

BOOL CdataPropertyList::OnInitDialog() 
{// note that the page will be inited in a separate call from the caller of this guy
 // we may have to move the initPage call here in case the page doesn't reinit at a new show
	//CPropertyPage::OnInitDialog();
	CResizablePage::OnInitDialog();
#ifdef _DEBUG
	if ( pDataList == NULL)
	{	TRACE(_T("* * * * WARNING: initializing control without a data list!\n")); }
#endif

	// preset layout - note that sizes are in PERCENT (0 to 100)
	AddAnchor(IDC_LIST1,       TOP_LEFT, CSize(100,100));
	AddAnchor(ID_APPLY_NOW,    TOP_LEFT, CSize(50,0));
	AddAnchor(ID_CANCEL_DIFFS, TOP_CENTER, CSize(100,0));
//	AddAnchor(IDC_BUTTON2, CSize(33,105), CSize(63,125));
//	AddAnchor(IDC_BUTTON3, CSize(65,105), CSize(100,125));

	//SetDlgItemText(IDC_EDIT1, _T("Pages have been designed for a property"
	//	" sheet dialog, so they have bad margins in wizard mode.\r\n\r\n"
	//	"The active page can be saved together with dialog's size and"
	//	" position. Select another page and try to close and open again."));

	return TRUE;
	
	// TODO: Add extra initialization here
	
	//return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CdataPropertyList::initPage(CDDlistData* pDDataList)
{
	m_cDataListCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_OWNERDRAWFIXED  | LVS_REPORT | LVS_OWNERDATA | LVS_EDITLABELS);
	//LVS_EX_TRACKSELECT | LVS_EX_ONECLICKACTIVATE);

	ASSERT(pDDataList);
	pDataList = pDDataList;
	
// Changed By Deepak
//	(m_cDataListCtrl).initialize(pDataList);
	(m_cDataListCtrl).initialize(pDataList,&ViewList);
// END Modification

	// fill columns
	pDataList->FillColumnHead(&m_cDataListCtrl);
	// fill items
// Changed By deepak
	pDataList->FillItems(m_cDataListCtrl,ViewList);			
// END Modification
	// display the result
//	CChkListView::OnInitialUpdate();
	m_cDataListCtrl.OnInitialUpdate();

	return 0;//maybe m_cDataListCtrl.getSize();????
}

// Added By deepak Called by property sheet chkbox handler
int CdataPropertyList::OnShowLocalClass(CDDlistData* pDDataList)
{
	// fill the View list for new condition
	pDataList->FillItems(m_cDataListCtrl,ViewList);	
	return 0;
}
void CdataPropertyList::OnItemchangedDataList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CdataPropertyList::OnSize(UINT nType, int cx, int cy) 
{
	CPropertyPage::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	ArrangeLayout();
}
#if 0
BOOL CdataPropertyList::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)lParam;

	if (pDispInfo->hdr.code == LVN_GETDISPINFO)
	{
		if (pDispInfo->item.mask & LVIF_TEXT)
		{
			int DataIndex = (int) pDispInfo->item.lParam;

			CString strField;
	//		strField = p_LD->QueryItemText(pDispInfo->item.iItem, pDispInfo->item.iSubItem);
	//		strField.Format("I:%d  S:%i",pDispInfo->item.iItem,pDispInfo->item.iSubItem);

			LPTSTR pstrBuffer = NULL;//AddPool(&strField); 
			pDispInfo->item.pszText = pstrBuffer;
		} 
		*pResult = 0;
		return FALSE;// no reflection - contrary to MS documentation

	}
	else
	{
		return CPropertyPage::OnNotify(wParam, lParam, pResult);
	}
}
#endif


void CdataPropertyList::OnApplyNow() 
{
	// TODO: Add your control notification handler code here
	if (pDataList != NULL)
	{
		pDataList->ApplyNow();
	}
	Invalidate();   
}

void CdataPropertyList::OnCancelDiffs()
{
	if (pDataList != NULL)
	{
		pDataList->CancelNow();
	}
	Invalidate(); 
}

/*************************************************************************************************
 *
 *   $History: dataPropertyList.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
