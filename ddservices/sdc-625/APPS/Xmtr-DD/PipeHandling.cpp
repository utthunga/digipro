/*************************************************************************************************
 *
 * $Workfile: PipeHandling.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		 this is a pipe server - creates the pipe and services the requests
 * * * * *
 * device::commAPI has a pair (rd/wr) of functions that are called by the
 * communication thread on an asycronous basis.
 *
 * The thread (in the future-host) will have a pair of functions (in this module)
 * that would stimulate a read or write.
 *
 * We must use overlapped io so we can get the die event
 *
 * * * * */


#include "NamedPipe.h"

#include "ddbGeneral.h"
//#include "commAPI.h"

#define PIPE_TIMEOUT   15000  /* mS (15 S) */

#include <windows.h> 
 
#define CONNECTING_STATE 0 
#define READING_STATE 1 
#define WRITING_STATE 2 
#define ERROR_STATE   3
#define INSTANCES 4  /*4 */
 
typedef struct 
{ 
   OVERLAPPED oOverlap; 
   HANDLE hPipeInst; 
   CHAR chBuf[PIPEBUFFERSIZE]; 
   DWORD cbToWrite; 
   DWORD dwState; 
   BOOL fPendingIO; 
} PIPEINST, *LPPIPEINST; 

// module local prototypes
VOID DisconnectAndReconnect(DWORD); 
BOOL ConnectToNewClient(HANDLE, LPOVERLAPPED); 
VOID GetDataToWriteToClient(LPPIPEINST); 

// global memory - one per pipe
PIPEINST Pipe[INSTANCES]; 
HANDLE hEvents[INSTANCES+1]; // add the die event
 
// the thread
void HandlePipe( void* lpvParam ) // param is a ptr 2 a CcommAPI 
{
   DWORD i, dwWait, cbBytes, dwErr; 
   BOOL fSuccess; 
   LPTSTR lpszPipename = PIPENAME; 
 
//*************************************************   CcommAPI* pCAPI = (CcommAPI*)lpvParam;

	// create pipe(s)
// The initial loop creates several instances of a named pipe 
// along with an event object for each instance.  An 
// overlapped ConnectNamedPipe operation is started for 
// each instance. 
 
   for (i = 0; i < INSTANCES; i++) 
   {  // Create an event object for this instance. 
 
      hEvents[i] = CreateEvent( 
         NULL,    // no security attribute 
         TRUE,    // manual-reset event 
         TRUE,    // initial state = signaled 
         NULL);   // unnamed event object 

      if (hEvents[i] == NULL) 
         cerr << "ERROR: Event did not construct."<< endl;
	  else
	  {
clog << "Creating pipe."<<endl;
		  Pipe[i].oOverlap.hEvent = hEvents[i]; 
 
		  Pipe[i].hPipeInst = CreateNamedPipe( 
			 lpszPipename,            // pipe name 
			 PIPE_ACCESS_DUPLEX |     // read/write access 
			 FILE_FLAG_OVERLAPPED,    // overlapped mode 
			 PIPE_TYPE_MESSAGE |      // message-type pipe 
			 PIPE_READMODE_MESSAGE |  // message-read mode 
			 PIPE_WAIT,               // blocking mode 
			 INSTANCES,               // number of instances 
			 PIPEBUFFERSIZE,          // output buffer size 
			 PIPEBUFFERSIZE,          // input buffer size 
			 PIPE_TIMEOUT,            // client time-out 
			 NULL);                   // no security attributes 

		  if (Pipe[i].hPipeInst == INVALID_HANDLE_VALUE) 
		  { 	cerr << "ERROR: Named pipe did not create."<< endl;
				Pipe[i].dwState = ERROR_STATE;
clog << "Pipe creation failed."<<endl;
		  }
		  else
		  {// Call the subroutine to connect to the new client
 
clog << "Pipe creation successful.("<< lpszPipename <<")"<<endl;
			  Pipe[i].fPendingIO = ConnectToNewClient( 
				 Pipe[i].hPipeInst, 
				 &Pipe[i].oOverlap); 
 
			  Pipe[i].dwState = Pipe[i].fPendingIO ? 
				 CONNECTING_STATE : // still connecting 
				 READING_STATE;     // ready to read 
		  }// endif
	  }//endif
   } // next

   // set the die event
//*****************************************************	hEvents[INSTANCES] = pCAPI->hDieEvent;

clog << "Entering infinite loop"<<endl;
   while (1)  /* thread's infinite loop */
   { 
	   	// wait for a client < only one allowed in his version >

   // Wait for the event object to be signaled, indicating 
   // completion of an overlapped read, write, or 
   // connect operation. 
 
      dwWait = WaitForMultipleObjects( 
         INSTANCES+1,    // number of event objects 
         hEvents,      // array of event objects 
         FALSE,        // does not wait for all 
         INFINITE);    // waits indefinitely 
 
   // dwWait shows which pipe completed the operation. 
 
      i = dwWait - WAIT_OBJECT_0;  // determines which pipe 
	  if (i == INSTANCES) // the die scenario
	  {
		  //TRACE (" Pipe Handler is diying.\n");
		  clog << " Pipe Handler is diying." << endl;
		  break; // out of infinite loop
	  }
      if (i < 0 || i > (INSTANCES))// - 1)) 
         cerr << "ERROR: Event index out of range."<< endl;
	  else
	  {// Get the result if the operation was pending. 
 
		  if (Pipe[i].fPendingIO) 
		  { 
			 fSuccess = GetOverlappedResult( 
				Pipe[i].hPipeInst, // handle to pipe 
				&Pipe[i].oOverlap, // OVERLAPPED structure 
				&cbBytes,          // bytes transferred 
				FALSE);            // do not wait 
 
			 switch (Pipe[i].dwState) 
			 { 
			 // Pending connect operation 
				case CONNECTING_STATE: 
				   if (! fSuccess) 
				   {
					   cerr << "ERROR: Connect to name pipe failed."<< endl;
						Pipe[i].dwState = ERROR_STATE; 
				   }
				   else
				   {
//					   TRACE("Pipe Handler connected.\n");
						Pipe[i].dwState = READING_STATE; 
				   }
				   break; 
 
			 // Pending read operation 
				case READING_STATE: 
				   if (! fSuccess || cbBytes == 0) 
				   { 
					  DisconnectAndReconnect(i); 
					  continue; 
				   } 
				   Pipe[i].dwState = WRITING_STATE; 
				   break; 
 
			 // Pending write operation 
				case WRITING_STATE: 
				   if (! fSuccess || cbBytes != Pipe[i].cbToWrite) 
				   { 
					  DisconnectAndReconnect(i); 
					  continue; 
				   } 
				   Pipe[i].dwState = READING_STATE; 
				   break; 
 
				default: 
				   cerr << "invalid pipe state"<<endl;
			 }  // endswitch
		  }// endif 
 
	   // The pipe state determines which operation to do next. 
 
		// handle the i/o
		  switch (Pipe[i].dwState) 
		  { 
		  // READING_STATE: 
		  // The pipe instance is connected to the client 
		  // and is ready to read a request from the client. 
 
			case READING_STATE: 
			{
				fSuccess = ReadFile( 
				   Pipe[i].hPipeInst, 
				   Pipe[i].chBuf, 
				   PIPEBUFFERSIZE, 
				   &cbBytes, 
				   &Pipe[i].oOverlap); 
 
			 // The read operation completed successfully. 
 
				if (fSuccess && cbBytes != 0) 
				{ 
				   Pipe[i].fPendingIO = FALSE; 
				   Pipe[i].dwState    = WRITING_STATE; 
				   continue; 
				} 
 
			 // The read operation is still pending. 
 
				dwErr = GetLastError(); 
				if (! fSuccess && (dwErr == ERROR_IO_PENDING)) 
				{ 
				   Pipe[i].fPendingIO = TRUE; 
				   continue; 
				} 
 
			 // An error occurred; disconnect from the client. 
 
				DisconnectAndReconnect(i); 
			}
			break; 
 
      // WRITING_STATE: 
      // The request was successfully read from the client. 
      // Get the reply data and write it to the client. 
 
         case WRITING_STATE: 
			{
				//GetDataToWriteToClient( &(Pipe[i]) ); // from commAPI, in and out
				Pipe[i].chBuf[cbBytes] = '\0';
		//		TRACE("++++  READ:|%s|++++\n",Pipe[i].chBuf);
//*************************************				fSuccess = pCAPI->MsgRcvd(	Pipe[i].chBuf,
//*******											cbBytes,// Pipe[i].cbToWrite,
//*******											PIPEBUFFERSIZE);
				Pipe[i].cbToWrite = cbBytes;
		//		TRACE("++++ WRITE:|%s|++++",Pipe[i].chBuf);
				fSuccess = WriteFile( 
				   Pipe[i].hPipeInst, 
				   Pipe[i].chBuf, 
				   Pipe[i].cbToWrite, 
				   &cbBytes,           // return value
				   &Pipe[i].oOverlap);  
				if (Pipe[i].cbToWrite !=  cbBytes)
				{
			//	TRACE(" want to write %d ...wrote %d \n",Pipe[i].cbToWrite,cbBytes);
				clog << " want to write "<< Pipe[i].cbToWrite << "...wrote "<< cbBytes << endl;
				}

			 // The write operation completed successfully. 
 
				if (fSuccess && cbBytes == Pipe[i].cbToWrite) 
				{ 
				   Pipe[i].fPendingIO = FALSE; 
				   Pipe[i].dwState    = READING_STATE; 
				   continue; 
				} 
 
			 // The write operation is still pending. 
 
				dwErr = GetLastError(); 
				if (! fSuccess && (dwErr == ERROR_IO_PENDING)) 
				{ 
				   Pipe[i].fPendingIO = TRUE; 
				   continue; 
				} 
				else
				{
					clog << "  Write error: " << dwErr << endl;
				}
 
			 // An error occurred; disconnect from the client. 
 
				DisconnectAndReconnect(i); 
			}
			break; 
 
         default: 
            cerr << "ERROR: invalid pipe state (" << Pipe[i].dwState << ")"<<endl;
		} // end switch
	}// end if
  
  } // wend infinity


	// if it closes 
		  // exit thread

}// end of separate proccess thread


// DisconnectAndReconnect(DWORD) 
// This function is called when an error occurs or when the client 
// closes its handle to the pipe. Disconnect from this client, then 
// call ConnectNamedPipe to wait for another client to connect. 
 
VOID DisconnectAndReconnect(DWORD i) 
{ 
// Disconnect the pipe instance. 
 
//	TRACE("PipeHandler's Discon/Recon called.\n");
	clog << "PipeHandler's Discon/Recon called." << endl;
   if (! DisconnectNamedPipe(Pipe[i].hPipeInst) ) 
      cerr << "ERROR: Disconnect failed."<<endl;
   else
   { 
	// Call a subroutine to connect to the new client. 
 
	   Pipe[i].fPendingIO = ConnectToNewClient( 
		  Pipe[i].hPipeInst, 
		  &Pipe[i].oOverlap); 
 
	   Pipe[i].dwState = Pipe[i].fPendingIO ? 
		  CONNECTING_STATE : // still connecting 
		  READING_STATE;     // ready to read 
		clog << "  New Reconnect state: ";
		if ( Pipe[i].dwState = Pipe[i].fPendingIO )
		{
			clog << "CONNECTING_STATE" << endl;
		}
		else
		{
			clog << "READING_STATE" << endl;
		}
   }
} 
 
// ConnectToNewClient(HANDLE, LPOVERLAPPED) 
// This function is called to start an overlapped connect operation. 
// It returns TRUE if an operation is pending or FALSE if the 
// connection has been completed. 
 
BOOL ConnectToNewClient(HANDLE hPipe, LPOVERLAPPED lpo) 
{ 
   BOOL fConnected, fPendingIO = FALSE; 
 
// Start an overlapped connection for this pipe instance. 
   fConnected = ConnectNamedPipe(hPipe, lpo); 
 
// Overlapped ConnectNamedPipe should return zero. 
   if (fConnected) 
      cerr <<"ERROR: connect to named pipe failed."<<endl;
   else
   { 
	   switch (GetLastError()) 
	   { 
	   // The overlapped connection in progress. 
		  case ERROR_IO_PENDING: 
			 fPendingIO = TRUE; 
			 break; 
 
	   // Client is already connected, so signal an event. 
 
		  case ERROR_PIPE_CONNECTED: 
			 if (SetEvent(lpo->hEvent)) 
				break; 
 
	   // If an error occurs during the connect operation... 
		  default: 
			 cerr <<"ERROR: connect to named pipe with unknown error."<<endl; 
	   } 
   }
 
   return fPendingIO; 
} 


/*************************************************************************************************
 *
 *   $History: PipeHandling.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 10:55a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Remove CommAPI dependence
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/Apps/DDdeviceData
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */