//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by XmtrDD.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DDDEVICEDATA_DIALOG         102
#define IDD_PROPPAGE_ALL                107
#define IDR_MAINFRAME                   128
#define ZDO_NOT_USE_IDR_MAINFRAME       128
#define IDI_ICOND                       129
#define IDI_ICONE                       130
#define IDB_BITMAP1                     131
#define IDD_DDSLCTDIALOG                132
#define IDB_SELECTBITMAP                135
#define IDD_EXTENDEDINFO                137
#define IDC_LIST1                       1000
#define IDC_TREE1                       1005
#define IDC_DDSLCTEDITBX                1006
#define IDC_STAT_SELECTED               1007
#define IDC_CHECK1                      1009
#define IDC_ITEMID                      1010
#define IDC_ITEMSIZE                    1011
#define IDC_VARAIBLETYPE                1012
#define IDC_ITEMNAME                    1013
#define IDC_ITEMTYPE                    1014
#define IDC_BNCHECK                     1099
#define IDC_EDIT1                       1100
#define ID_CANCEL_DIFFS                 12322

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1101
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
