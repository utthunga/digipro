/*************************************************************************************************
 *
 * $Workfile: ddbParserInfc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *************************************************************************************************/

/*************************************************************************************************

 *************************************************************************************************
 *
 * Description:
 *		
 *	5/10/04 - stevev - the command line options have changed to make the database directory be the
 *						release directory.  We verify this only once.
 */

//#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
//stevev 20feb07-merge- contents moved to ddbGeneral #include "..\DevServices\stdafx.h"   // HOMZ
//#endif

/* #define GE_BUILD // define to enable GE specific extensions */

#include "ddbGeneral.h"	// stevev 20feb07 - merge to get rid of 'stdafx.h'
/* comutil.h uses Bill's TRUE/FALSE that the general erroneously defines on purpose */
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0
#ifdef GE_BUILD
 #define NOT_CURRENTLY_SUPPORTED 1			// 01.01.00
 #define DD_NOT_FOUND 2						// 01.01.00
 #define GENERIC_DD_NOT_FOUND 3						// 01.01.01
#endif

#if !defined(__GNUC__)

#include <basetsd.h>	// stevev 23feb10 port to vs8 - 3mar10 - had to put the VCInstall PlatformSDK include into the project to get this to work.
#include <atlbase.h>	// includes windows.h

#else // __GNUC__

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Directory searching.
#include <dirent.h>
#include <libgen.h>
#include <fnmatch.h>

#endif // __GNUC__

#pragma warning (disable : 4786) 

#include "ddbDeviceMgr.h"
#include "ddbDevice.h"
// no longer needed (it's empty)#include "ddb_UIInfc.h"

#define _DDPARSERINFC_
	#include "ddbParserInfc.h"
#undef _DDPARSERINFC_
#include "DDL6Items.h"

#include "HARTsupport.h"	// to get standard default DDs
#include "sign.h"			// show me a sign

#ifdef _DBGMIL
extern bool flagthisone;
extern MENU_ITEM_LIST* pGlblMIL;
#endif


/******
 * The large number of globals here implies that this functionality as designed 
 * to NOT be re-entrant.  That means you may only load One device at a time.
 * While internally re-entrant, things like the filename and entry count show
 * it may only be used once at a time.   stevev 21sep09
 ********/
static const char hartDicts[DICTCNT][DICTNAMELEN] = {DICTLIST};

// now in .h file....#define MAX_FILE_NAME_PATH 4096 //Vibhor 050405

char chInputFileName[MAX_FILE_NAME_PATH];
char chOutputFileName[MAX_FILE_NAME_PATH];
char chErrorFileName[MAX_FILE_NAME_PATH];
char chDbdir[MAX_FILE_NAME_PATH];
#ifdef GE_BUILD
unsigned char generic_device_found=0;	// 01.00.03
#endif
char FileType[50];
FILE_INFO_T readFileTime; // the time the file was created when read
UINT8 eFFVersionMajor = 0;
UINT8 eFFVersionMinor = 0;
int   VerificationLevel = 0; // 0 = none, 1 = Production, 2 = Debug
INT8  entryCount = 0;
#ifdef GE_BUILD
// PAW 00.01.06 start
unsigned char file_error_occured;
// PAW 00.01.06 end
#endif
/*Vibhor 010705: Modified the dump conditions. If you need it define it temporarily in 
project settings, BUT DON'T Checkin !!!*/

#ifdef _DEBUG /* debug versions can get command line control   _DUMP_ENABLE*/
#include <fstream>
static ofstream outRedir;
static streambuf* pHldOutBuf = NULL;
extern void dumpDev(aCdevice* pDev);
#else
#define dumpDev(a) 
#endif

bool fm6 = false;	// timj 10jan08 - true if the current binary file is BFF 6

int  unsupported = 0; // used to suppress the abundent Revision Not Supported messages
int maxVer = SUPPORTED_VERSION_MAX;// changed to single macro stevev 13apr15...in ddbDeviceMgr.h...was::> 10;// =((CDDIdeApp*)AfxGetApp())->m_CmdLine.cl_MaxEFFver; the app has to set this
//#include "DeviceLib.h"

#include "logging.h"

bool weDidThis = false;
extern bool DumpOnly;

#ifndef _UNICODE
#undef  OLE2A
#define OLE2A
#endif

#ifndef _DEBUG
#define SAFETYNET 1
#else   /* is _DEBUG */
#define x_STRMEMTESTING 1
#endif

// stevev 28dec10, this should not be used in this file
// the global is used in the DDParser, it should not be used in the parser interface
// extern CDictionary *pGlobalDict; /*The Global Dictionary object*/
// we will use the passed-in dictionary instead 
static CDictionary * pPIdict = NULL;

/*** memory debug ***/
aPayldType* pBuilt = NULL;
/*** mem debug ******/

wchar_t* GetWC(const char* c);

/* stevev 13may08 - moved identity flags to .h file to share */

DDlDevDescription  *devDesc;
/* commented prototypes removed 03aug07 - see earlier versions for content
*/
		void fill_Payload(aCgenericConditional::aCexpressDest *,DDlAttribute *);
	    void fill_var_item_attributes(aCitemBase *,ItemAttrList&);
	    void fill_command_attributes(aCitemBase *,ItemAttrList&);
	    void fill_menu_attributes(aCitemBase *,ItemAttrList&);
	    void fill_edit_display_attributes(aCitemBase *,ItemAttrList&);
		void fill_item_array_attributes(aCitemBase *tmpaCitemBase, ItemAttrList& alist);
	    void fill_collection_attributes(aCitemBase *,ItemAttrList&);
	    void fill_refresh_attributes(aCitemBase *,ItemAttrList&);
	    void fill_unit_relation(aCitemBase *,ItemAttrList&);
	    void fill_wao_relation(aCitemBase *,ItemAttrList&);
	    void fill_method_attributes(aCitemBase *,ItemAttrList&);

		void copy_ddlstring(aCddlString *,ddpSTRING *);
		void fill_reference(aCreference *,ddpREF *);
		void fill_expression(aCexpression *,ddpExpression *);
		void fill_conditional_attributes_list(aCcondList *paCond,DDlConditional *pCond);
		void fill_conditional_attributes(aCconditional *paCond,DDlConditional *pCond);
		void fill_enumlist(aCenumList *,ENUM_VALUE_LIST *);
		void fill_menu_item_list(aCmenuList *tmpmenuList,MENU_ITEM_LIST *menuList);
		void fill_transaction_list(aCattrCmdTrans *tmpCmdTrans,TRANSACTION_LIST* transList);
		void fill_data_item_list(aCcondList *tmpcondLst, DATA_ITEM_LIST* dataItemList);
		void fill_response_code_list(aCcondList *tmpcondLst,RESPONSE_CODE_LIST *respList);
		void fill_reference_list(AreferenceList_t *acreflist, REFERENCE_LIST *refList);
		void fill_CondReference_list(AcondReferenceList_t *acreflist, REFERENCE_LIST *refList);
		void fill_member_element_list(aCmemberElementList *, ITEM_ARRAY_ELEMENT_LIST *);
		void fill_grid_member_list(aCgridMemberList *tmpGrdList, GRID_SET_LIST *pGridSet);/*13may05*/

		/*stub*/void fill_refresh_relation(aCreferenceList *tmpRefrelation,REFERENCE_LIST *refList);
		void fill_min_max_list(aCminmaxList *,MIN_MAX_LIST *);
		void fill_conditional_chunks(aCcondList* paCond,DDlAttribute *pAttr);
		void resolve_record_ref(unsigned long resl_member,unsigned long resl_recordid,aCreference *tmpaCref);
// line 5417
	    void fill_array_attributes(aCitemBase *,ItemAttrList&);
	    void fill_file_attributes (aCitemBase *,ItemAttrList&);
	    void fill_chart_attributes(aCitemBase *,ItemAttrList&);
	    void fill_graph_attributes(aCitemBase *,ItemAttrList&);
	    void fill_axis_attributes (aCitemBase *,ItemAttrList&);
	    void fill_waveform_attributes(aCitemBase *,ItemAttrList&);
	    void fill_source_attributes(aCitemBase *,ItemAttrList&);
	    void fill_list_attributes (aCitemBase *,ItemAttrList&);
void fill_section_chunks(aCcondList* paCond,DDlSectionChunks *pSecChunk); //Vibhor 200105: Added

	    void fill_image_attributes(aCitemBase * paCitembase, ItemAttrList& List); /* stevev 1apr05 */
	    void fill_grid_attributes(aCitemBase * paCitembase, ItemAttrList& List); /* stevev 1apr05 */
	    void fill_blob_attributes(aCitemBase * paCitembase, ItemAttrList& List); /* stevev 17nov08 */

#ifdef _STRMEMTESTING
vector<ddpSTRING*> gblLstOfSTRING;
bool areSTRINGSgood();
#endif


#if defined(__GNUC__)

#define OS_PATH_SEPR "/"

/*
	Convert Unix time_t to Windows FILETIME format.

	Source:
	 * http://stackoverflow.com/a/4135003
	 * http://support.microsoft.com/kb/167296


	Parameters:
	  unixTime - time_t value representing Unix time.

	Return:
		Converted unix time in Windows FILETIME format (represented using custom
		FILE_INFO_T structure defined in dllapi.h).
*/
FILE_INFO_T unixTimeToFileTime(time_t unixTime) {
	/*
	 * Difference between Windows and Unix time epochs.
	 * Number of seconds from 1 Jan. 1601 00:00 to 1 Jan 1970 00:00 UTC
	 */
	static const long long EPOCH_DIFF = 11644473600LL;

	FILE_INFO_T windowsTime = {0};

	unsigned long long result = EPOCH_DIFF;
	result += unixTime;
	result *= 10000000LL;
	windowsTime.LoFileTime = (DWORD) result;
	windowsTime.HiFileTime = result >> 32;
	return windowsTime;
}


/*
	Get last write time to the file.

	On Unix, uses the "time of last modification" as the write time.

	Parameters:
	  filePath - Path to file to retrieve write-time info for.
	  pFileInfo - Pointer to FILE_INFO_T structure to store write-time in.

	Return: True on successfully retriving and storing write-time, false
		otherwise.
*/
bool getFileLastWriteTime(const string& filePath, PFILE_INFO_T pFileInfo)
{
	if ((!filePath.size()) || (NULL == pFileInfo))
	{
		return false;
	}

	struct stat fileData;
	if(-1 != stat(filePath.c_str(), &fileData))
	{
		*pFileInfo = unixTimeToFileTime(fileData.st_mtime);
		return true;
	}

	return false;
}


/*
	Get current working directory.

	Return: None.
*/
string getCurrentDirectory()
{
	char *pDirPath = getcwd(NULL, 0);
	string dirPath(pDirPath);
	free(pDirPath);

	return dirPath;
};


class dDSearchOps
{
public:
    /*
        Construct a dDSearchOps object.

        Parameters:
            dirBasePath - Base directory path to perform the search under.
            matchPattern - Directory or file name or file pattern to match.

        Return: None.
    */
    dDSearchOps(
        const string& dirBasePath,
        const string& matchPattern
    ):
        m_searchPath(dirBasePath),
        m_searchPattern(matchPattern),
        m_pSearchHandle(NULL),
        m_findFileData(),
        m_wasFindSuccessful(false)
    {
        // If search path is empty, search working directory.
        if (!m_searchPath.size())
        {
            m_searchPath = ".";
        }

        /*
           If search pattern is empty, use basename component of search path as
           pattern.
        */
        if (!m_searchPattern.size())
        {

            /*
                Note: When using dirname or basename, the passed-in character
                buffer may be modified. Operate on a copy of the data to work
                around that.

                Make a copy using <string>.c_str(). Else, copy is not actually
                deep and the usual copy-on-write won't kick in when dereferencing
                string through <string>.c_str() on the supposed copies.

                Note: GNU version of basename behaves differently - path is not modified 
                and trailing / is handled differently. Use that instead? 
                See "man 3 basename".
            */
            string dirnameBuffer = dirBasePath.c_str();
            m_searchPath = dirname(const_cast<char *>(
                    dirnameBuffer.c_str()
                )
            );

            string basenameBuffer = dirBasePath.c_str();
            m_searchPattern = basename(const_cast<char *>(
                    basenameBuffer.c_str()
                )
            );

            // If match pattern is still empty, match everything.
            if (!m_searchPattern.size())
            {
                m_searchPattern = "*";
            }
        }
    };

    /*
        Destruct a dDSearchOps object.
    */
    ~dDSearchOps()
    {

        if (m_pSearchHandle)
        {
            closedir(m_pSearchHandle);
        }
    };

    /*
        Find directory or file matching pattern and path specified during
        construction.

        Successive invocations find the next directory or file that matches
        the criteria.

        Source:
            http://www.codeproject.com/Messages/193514/Linux-equivalent-to-FindFirstFile-AP


        Returns: True on success, false otherwise.
    */
    bool find()
    {
        if (NULL == m_pSearchHandle)
        {
            m_pSearchHandle = opendir(m_searchPath.c_str());
        }

        m_wasFindSuccessful = false;
        bool isSearching = true;
        while (m_pSearchHandle && isSearching)
        {
            struct dirent *pSearchResult = NULL;
            if (
                0 == readdir_r(
                    m_pSearchHandle,
                    &m_findFileData,
                    &pSearchResult
                )
            )
            {

                if (pSearchResult)
                {
                    // printf("d_name: %s\n", m_findFileData.d_name);


                    /*
                        It's not very clear from the Windows FindFirstFile
                        documentation, but it looks like wild card
                        matching is only for the last part of the
                        path component. Will mimic that behavior here by
                        using fnmatch to match file/directory name against
                        search pattern.

                        Note: Performing case-insensitive search to match
                        Windows behavior. 
                    */
                    if (
                        0 == fnmatch(
                            m_searchPattern.c_str(),
                            m_findFileData.d_name,
                            FNM_CASEFOLD | FNM_NOESCAPE | FNM_PATHNAME
                        )
                    )
                    {
                        // printf("Found\n");
                        m_wasFindSuccessful = true;
                        isSearching = false;
                    }
                }
                else
                {
                    // End of stream reached - no result.
                    isSearching = false;
                }
            }
            else
            {
                // Directory read error.
                isSearching = false;
            }
        }

        return m_wasFindSuccessful;
    };

    /*
        Was the last find operation successful.

        Return: True if successful, false otherwise, including when nothing
        has matched.
    */
    bool wasFindSuccessful()
    {
        return m_wasFindSuccessful;
    };

    /*
        Get name of directory or file found.

        Return: Name of directory or file on a match; empty string otherwise,
        including when nothing has matched.
    */
    string getName()
    {
        return m_findFileData.d_name;
    };

    /*
        Is the current match a directory?

        Return: True if a directory, false otherwise, including when nothing
        has matched.
    */
    bool isDirectory()
    {
        // TODO(LINUX_PORT, "Handle symlink (using stat(...))");
        // TODO(LINUX_PORT, "JFFS2 may not support this feature (per man readdir). May be safest to use stat(...)");
        return (DT_DIR == m_findFileData.d_type);
    };


    /*
        Get last write time to the matched directory or file.

        Return: Last write time as FILE_INFO_T structure as defined in dllapi.h
    */
    FILE_INFO_T getLastWriteTime()
    {
        FILE_INFO_T fileInfo = {0};

        (void) getFileLastWriteTime(m_searchPath + "/" + getName(), &fileInfo);

        return fileInfo;
    };


    /*
        Construct a dDSearchOps object, perform a search operation and return
        pointer to object on successful search.

        Parameters:
            dirBasePath - Base directory path to perform the search under.
            matchPattern - Directory or file name or file pattern to match.

        Return:
          * Pointer to instantiated object.
          * NULL on instantiation error.
          * NULL on find failure.

        Note: When a valid pointer to the instantiated object is returned, it is
            responsibility of the client to delete the object after  use.
    */
    static dDSearchOps* instantiateAndFind(
        const string& dirBasePath,
        const string& matchPattern
    )
    {
        // Make new return NULL on failure instead of throwing and exception.
        dDSearchOps* pSearch =
            new(std::nothrow) dDSearchOps(dirBasePath, matchPattern);
        if (NULL != pSearch)
        {
            if (!pSearch->find())
            {
                delete pSearch;
                pSearch = NULL;
            }
        }

        return pSearch;
    }

private:
    // Combined search path.
    string m_searchPath;

    string m_searchPattern;

    // Search operation handle.
    DIR *m_pSearchHandle;

    // Search result data.
    struct dirent m_findFileData;

    // Was the last find operation successful.
    bool m_wasFindSuccessful;


    // Assignment operator (disabled).
    dDSearchOps& operator=(const dDSearchOps& rhs);

    // Copy constructor (disabled).
    dDSearchOps(const dDSearchOps& rhs);

};



#else // not __GNUC__


#define OS_PATH_SEPR "\\"

/*
	Get last write time to the file.

	Parameters:
	  filePath - Path to file to retrieve write-time info for.
	  pFileInfo - Pointer to FILE_INFO_T structure to store write-time in.
	              FILE_INFO_T structure as defined in dllapi.h

	Return: True on successfully retriving and storing write-time, false
		otherwise.
*/
bool getFileLastWriteTime(const string& filePath, PFILE_INFO_T pFileInfo)
{
	if ((!filePath.size()) || (NULL == pFileInfo))
	{
		return false;
	}

	USES_CONVERSION;
	CComBSTR combstr(filePath.c_str());
	LPTSTR szFilePath = OLE2T(combstr.m_str);

	FILETIME lastWriteTime = {0};

	HANDLE fileHandle = CreateFile(
		szFilePath,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	if (INVALID_HANDLE_VALUE != fileHandle)
	{
		if (::GetFileTime(fileHandle, NULL, NULL, &lastWriteTime))
		{
			memcpy(
				pFileInfo, 
				&(lastWriteTime), 
				sizeof(FILETIME)
			);

			return true;
		}

		::CloseHandle(fileHandle);
	}

	return false;
}


/*
	Get current working directory.

	Parameters:
		pDirName - Name of working directory is stored to this user-specified
			string.

	Return: None.
*/
string getCurrentDirectory()
{
	USES_CONVERSION;
	TCHAR szTemp[MAX_FILE_NAME_PATH] = _T("");
#ifndef _WIN32_WCE
	::GetCurrentDirectory(MAX_FILE_NAME_PATH,szTemp);
#endif
	return OLE2A(szTemp);
};


class dDSearchOps
{
public:
	/*
		Construct a dDSearchOps object.

		Parameters:
			dirBasePath - Base directory path to perform the search under.
			matchPattern - Directory or file name or file pattern to match.

		Return: None.
	*/
	dDSearchOps(
		const string& dirBasePath,
		const string& matchPattern
	): 
		c_searchPath(dirBasePath + matchPattern),
		m_searchHandle(INVALID_HANDLE_VALUE),
		m_findFileData(),
		m_wasFindSuccessful(false)
	{
	};

	/*
		Destruct a dDSearchOps object.
	*/
	~dDSearchOps()
	{
		if (INVALID_HANDLE_VALUE != m_searchHandle)
		{
			::FindClose(m_searchHandle);
		}
	};

	/*
		Find directory or file matching pattern and path specified during 
		construction.

		Successive invocations find the next directory or file that matches
		the criteria.

		Returns: True on success, false otherwise.
	*/
	bool find()
	{
		if (INVALID_HANDLE_VALUE == m_searchHandle)
		{
#ifdef UNICODE 
			USES_CONVERSION;
			CComBSTR combstr(c_searchPath.c_str());
			LPTSTR szTemp = OLE2T(combstr.m_str);
#else
			const string& szTemp = c_searchPath;
#endif
			m_searchHandle = ::FindFirstFile(szTemp, &m_findFileData);
			m_wasFindSuccessful = (INVALID_HANDLE_VALUE != m_searchHandle);
		}
		else
		{
			m_wasFindSuccessful = 
				(0 != ::FindNextFile(m_searchHandle, &m_findFileData));
		}

		return m_wasFindSuccessful;
	};

	/*
		Was the last find operation successful.

		Return: True if successful, false otherwise, including when nothing
		has matched.
	*/
	bool wasFindSuccessful()
	{
		return m_wasFindSuccessful;
	};

	/*
		Get name of directory or file found.

		Return: Name of directory or file on a match; empty string otherwise, 
		including when nothing has matched.
	*/
	string getName()
	{
		if (INVALID_HANDLE_VALUE != m_searchHandle)
		{
			USES_CONVERSION;
#ifdef UNICODE
			CComBSTR combstr3(m_findFileData.cFileName);
			return OLE2A(combstr3.m_str);
#else
			return FindFileData.cFileName;
#endif
		}
		else
		{
			return "";
		}
	};

	/*
		Is the current match a directory?

		Return: True if a directory, false otherwise, including when nothing
		has matched.
	*/
	bool isDirectory()
	{
		if (INVALID_HANDLE_VALUE != m_searchHandle)
		{
			return 
				(0 != (FILE_ATTRIBUTE_DIRECTORY & m_findFileData.dwFileAttributes));
		}
		else
		{
			return false;
		}
	};


	/*
		Get last write time to the matched directory or file.

		Return: Last write time as FILE_INFO_T structure as defined in dllapi.h
	*/
	FILE_INFO_T getLastWriteTime()
	{
		FILE_INFO_T fileInfo = {0};

		if (INVALID_HANDLE_VALUE != m_searchHandle)
		{
			memcpy(
				&fileInfo, 
				&(m_findFileData.ftLastWriteTime), 
				sizeof(FILETIME)
			);
		}

		return fileInfo;
	};


	/*
		Construct a dDSearchOps object, perform a search operation and return
		pointer to object on successful search.

		Parameters:
			dirBasePath - Base directory path to perform the search under.
			matchPattern - Directory or file name or file pattern to match.

		Return: 
		  * Pointer to instantiated object. 
		  * NULL on instantiation error.
		  * NULL on find failure.

		Note: When a valid pointer to the instantiated object is returned, it is 
			responsibility of the client to delete the object after  use.
	*/
	static dDSearchOps* instantiateAndFind(
		const string& dirBasePath,
		const string& matchPattern
	)
	{
		// Make new return NULL on failure instead of throwing and exception.
		dDSearchOps* pSearch = 
			new(std::nothrow) dDSearchOps(dirBasePath, matchPattern);
		if (NULL != pSearch)
		{
			if (!pSearch->find())
			{
				delete pSearch;
				pSearch = NULL;
			}
		}

		return pSearch;
	}

private:

	// Combined search path.
	const string c_searchPath;

	// Search operation handle.
	HANDLE m_searchHandle;

	// Search result data.
	WIN32_FIND_DATA m_findFileData;

	// Was the last find operation successful.	
	bool m_wasFindSuccessful;

	// Assignment operator (disabled).
	dDSearchOps& operator=(const dDSearchOps& rhs);

	// Copy constructor (disabled).
	dDSearchOps(const dDSearchOps& rhs);

};
#endif




#ifdef GE_BUILD
#define VERIFY_RELEASE_DIR_PATH "000000" OS_PATH_SEPR "0002" OS_PATH_SEPR "0502.fm6"
#else
#define VERIFY_RELEASE_DIR_PATH "0000f9" OS_PATH_SEPR "0083" OS_PATH_SEPR "01??.fm?"
#endif





/*
release prototypes commented here have been removed 03sep09
*/

/*Vibhor 140404: Start of Code*/
//Changed the signatures to pass a Standard Device
		RETURNCODE getSystem(hCddbDevice *stdTbl,aCentry* pEntryRoot);
		RETURNCODE getSystemForSDC(hCddbDevice *stdTbl,aCentry* pEntryRoot); //ANOOP For invoking the DD Library from SDC
/*Vibhor 140404: End of Code*/
		RETURNCODE buildDicitonary(CDictionary* dictionary);
//no longer used 13mar08 sjv
//		RETURNCODE getDevice  (UINT32 wrkingKey, aCdevice* pAbstractDev,CDictionary* dictionary);
/* VMKP Added on 040203 */
		RETURNCODE handle_XXX_Pre_Post_Commit_Items(char *,BYTE *,UINT *);
		void UpdateDevicePreWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId);
		void UpdateDevicePostWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId);
/* VMKP Added on 040203 */
/*=*=*=*=*=*=*=*=*=* SYSTEM FUNCTIONS  *=*=*=*=*=**=*=*=*=*=*=*=*=*/
bool verifyReleaseDir(void);	//stevev 5/10/04 -- a local function
//int  makePath(Indentity_t fromIdent, char* retPath, int MaxPathLen, DWORD& uniqueKey);//stevev09feb07
int  makePath(Indentity_t fromIdent, char* retPath, int MaxPathLen, DD_Key_t& uniqueKey);//stevev 26sep07
/*=*=*=*=*=*=*=*=*=* SYSTEM FUNCTIONS  *=*=*=*=*=**=*=*=*=*=*=*=*=*/


#ifdef _DEBUG			
void dumpDevice(aCdevice* pAbstractDev)
{
	/* Dump the Data base dd file */
	string afile="aCOutput.txt";
		
	if (weDidThis)
	{
		outRedir.open(afile.c_str(),  ios_base::out | ios_base::app);// append?
	}
	else
	{ 
		outRedir.open(afile.c_str(), ios_base::out | ios_base::trunc);//new file?
		weDidThis = true;
	}
		
	if ( outRedir.is_open() )
	{
		pHldOutBuf = cout.rdbuf();
		cout.rdbuf(outRedir.rdbuf());
	}
	else
	{
		LOGIT(CERR_LOG, "ERROR: output redirection failed.\n");
	}

	dumpDev(pAbstractDev);
		
	cout.rdbuf(pHldOutBuf);
	outRedir.close();	
}
#endif

class aCDevTypeEntry
{
public:
   int     number;
   wstring  name;
   wstring  help;// stevev 10jan06

   aCDevTypeEntry(){clear();};			// constructor
   aCDevTypeEntry(const aCDevTypeEntry& src)	// copy constructor
   {number = src.number;
	name   = src.name;help=src.help;/*sjv 10jan06*/};
  ~aCDevTypeEntry(){clear();};			// destructor

   void clear(void)
   {
      number = 0;
      name.erase();
	  help.erase();/*sjv 10jan06*/
   };
};


class aCManEntry
{
public:
   int     number;
   wstring  name;
   vector<class aCDevTypeEntry> children;

   aCManEntry(){clear();};			// constructor
   aCManEntry(const aCManEntry& src)	// copy constructor
   {number = src.number;
	name   = src.name;
    children = src.children;  };
  ~aCManEntry(){clear();};			// destructor

   void clear(void)
   {
      number = 0;
      name.erase();
      children.clear();
   };
};

typedef map<int,aCManEntry> aCManList;
typedef pair <int,aCManEntry> ManEntrypair;
/*** use the one in foundation.cpp *********************
wstring UTF82Unicode(const char* src)
{
	wstring dest;
	int sz = strlen(src);
    dest.resize(sz);//src.size());
    unsigned int utf_idx=0;
    unsigned int ws_idx=0;
    int c, c1, c2, c3;
    while (utf_idx<sz)
	{
        c = static_cast<unsigned char>(src[utf_idx++]);
		if ((c & 0x80) == 0)
			dest[ws_idx++] = c & 0x7F;
		else if ((c & 0xE0) == 0xC0){
			c1 = c;
			c2 = src[utf_idx++];
			dest[ws_idx++] = ((c1 & 0x1F) << 6) | (c2 & 0x3F);
		}
		else if ((c & 0xF0) == 0xE0){
			c1 = c;
			c2 = src[utf_idx++];
			c3 = src[utf_idx++];
			dest[ws_idx++] = ((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6) | (c3 & 0x3F);
		}
	}
    dest.resize(ws_idx);
	return dest;
}
*** use the one in foundation.cpp *********************/

/*Vibhor 140404: Start of Code*/

// stevev 25mar09 - massive changes to deal with new format in the standard tables
//                  all device types are 2 byte, some directories are 2 byte
//					Old code removed - see an earlier version for compare.
RETURNCODE  fillManufacturerList(hCddbDevice *stdTbl/*FILE *fdev*/ ,aCManList *manList)
{
	RETURNCODE rc = SUCCESS;
	aCManEntry     tmpManEntry;
	aCDevTypeEntry tmpDevType;
	bool  isWideMfg = false;

	hCitemBase* pItem = NULL;
	rc = stdTbl->getItemBySymNumber(2143,&pItem);//x85f - array device_types
	if(SUCCESS != rc || pItem == NULL)
		return FAILURE;
	hCitemArray *pDeviceArray = (hCitemArray*)pItem;

	pItem = NULL;
	rc = stdTbl->getItemBySymNumber(153,&pItem);//x99 - variable manufacturer_id enumerated
	if((SUCCESS == rc) && pItem != NULL)
	{
		hCEnum *pMfgEnum = (hCEnum*)pItem;
		triadList_t MfgList, DevList;
		rc = pMfgEnum->procureList( MfgList ); // get the resolved list
		if(SUCCESS != rc || MfgList.size() == 0)
		{
			return FAILURE ;//to be handled in the calling routine
		}
		triadList_t::iterator it;
		for(it = MfgList.begin(); it != MfgList.end(); ++it)
		{// isa ptr2a EnumTriad_t				
			EnumTriad_t* p3 = (EnumTriad_t*)&(*it);// convert for 2005
			tmpManEntry.number = p3->val;
			tmpManEntry.name   = p3->descS;
			if (tmpManEntry.number > 0xff)
				isWideMfg = true;
			else
				isWideMfg = false;
			// now get the device-type 'children'
			rc = pDeviceArray->getByIndex(p3->val, pItem);// mfg number
			if (rc==SUCCESS && pItem != NULL && ((hCVar*)pItem)->VariableType()==vT_Enumerated)
			{// we have a device type 'table'
				hCEnum *pDevTypeEnum = (hCEnum*)pItem;
				
				rc = pDevTypeEnum->procureList( DevList ); // get the resolved list
				if(SUCCESS == rc || DevList.size() > 0)
				{						
					triadList_t::iterator it;
					for(it = DevList.begin(); it != DevList.end(); ++it)
					{// isa ptr2a EnumTriad_t							
						EnumTriad_t* p3 = (EnumTriad_t*)&(*it);// convert for 2005
						
						tmpDevType.number = p3->val;
						tmpDevType.name   = p3->descS;
						tmpDevType.help   = p3->helpS;
						// debug only
						bool y = tmpDevType.help.empty();
						tmpManEntry.children.push_back(tmpDevType);
// now done in the library						if ( ! isWideMfg && tmpDevType.number < 0xff )
//						{// for the future... add a device type duplicate for 16 bits
//							tmpDevType.number |= (tmpManEntry.number << 8);
//							tmpManEntry.children.push_back(tmpDevType);
//						}
						tmpDevType.clear(); 
					}// next triad
					DevList.clear(); // stevev 21aug09 - so procure doesn't keep adding
				}// else the type list was empty, just continue w/ pushing the empty mfg
				else
				{
					DEBUGLOG(CLOG_LOG,L"ERROR: Tables Failed to procure a device list.\n"
						L"       0x%04x (%s) in 0x%04x (%s)\n",
						pDevTypeEnum->getID(), pDevTypeEnum->getWName().c_str(),p3->val,p3->descS.c_str());
				}
			}
			//else get by index failed...No device-types;  just continue w/ next mfg
			else
			{
				DEBUGLOG(CLOG_LOG,L"ERROR: Tables Failed to find a Mfg item.\n"
					L"       0x%04x (%s) \n",p3->val,p3->descS.c_str() );
			}
			manList->insert(ManEntrypair(tmpManEntry.number,tmpManEntry));	
			tmpManEntry.clear();
		}// next Mfg
		rc = SUCCESS; //stevev 21aug09 clear this or any misses will abort the listing
	}//else - the manufacturer_id variable was not in the standard tables, we can do nothing
	else
	{
		rc = FAILURE;
	}

	return rc;
}
/*Vibhor 140404: End of Code*/
/* VMKP Added on 040203 */
/*******************************************************************************************/
/* F */


/********************************************************************************************/


RETURNCODE handle_XXX_Pre_Post_Commit_Items(char *pchMethodDef,BYTE *pbyPreOrPostItem,UINT *pnItemId)
{
	char *ptmpMethodDef = pchMethodDef;
	UINT pntmpItemId;
	BYTE pbytmpPreOrPostItem;
	int loop = 0;


	/* Find all the Pre commit actions in the Method Definition */
	while(ptmpMethodDef)
	{
		ptmpMethodDef = strstr(ptmpMethodDef,"xXx_pre_commit = ");

		if(ptmpMethodDef)
		{
			ptmpMethodDef += strlen("xXx_pre_commit = ");
			sscanf(ptmpMethodDef,"%d",&pntmpItemId);
			pbytmpPreOrPostItem = XXX_PRE_COMMIT;
			*(pbyPreOrPostItem + loop) = pbytmpPreOrPostItem;
			*(pnItemId + loop) = pntmpItemId;
			loop++;
		}
	}

	ptmpMethodDef= pchMethodDef;

	/* Find all the Post commit actions in the Method Definition */
	while(ptmpMethodDef)
	{
		ptmpMethodDef = strstr(ptmpMethodDef,"xXx_post_commit = ");

		if(ptmpMethodDef)
		{
			ptmpMethodDef += strlen("xXx_post_commit = ");
			sscanf(ptmpMethodDef,"%d",&pntmpItemId);
			pbytmpPreOrPostItem = XXX_POST_COMMIT;
			*(pbyPreOrPostItem + loop) = pbytmpPreOrPostItem;
			*(pnItemId + loop) = pntmpItemId;
			loop++;
		}
	}

/*  return the total number of Pre and Post Commit actions found,
	if returns zero don't do anything */

	return loop;
}


/*******************************************************************************************/
/* F */


/********************************************************************************************/

void UpdateDevicePreWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId)
{
		AitemList_t::iterator itr1;
		BYTE byIsVarAttrPreWriteAct = 0;
			aCitemBase* pItem;

		for(itr1 = (pAbstractDev->AitemPtrList).begin();
			itr1 < (pAbstractDev->AitemPtrList).end();		++itr1)
		{
			pItem = *itr1;
			if ( pItem == NULL )
				continue;
			if(pItem->itemId == nVarItemId)
			{
				FOR_iT(AattributeList_t,pItem->attrLst)
				{			
					genericTypeEnum_t git = 
										  getAttrType4Mask(pItem->itemType,(*iT)->attr_mask);

					if(git.gAttrMethod == varAttrPreWriteAct)
					{
						/* Add the action to the existing list */
						aCattrBase* paAttr = NULL;
						paAttr = (aCattrActionList*)(*iT);

						aCgenericConditional &tmpaCgenCond = ((aCattrActionList*)paAttr)->ActionList.genericlist.back();
						aCgenericConditional::aCexpressDest &tempaCDest = tmpaCgenCond.destElements.back();

						REFERENCE_LIST refList;
						ddpREF ref;
						ddpREFERENCE reference;

						ref.type = METHOD_ID_REF;
						ref.val.id = nMethodItemId;
						reference.push_back(ref);
						refList.push_back(reference);

						fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),&refList);

						byIsVarAttrPreWriteAct = 1;
						break;
					}
				}

				/* If no Pre Write Action attribute is found add new 
				  attribute */
				if(!byIsVarAttrPreWriteAct)
				{
					aCattrBase* paAttr = NULL;
					paAttr =  new aCattrActionList;

					((aCattrActionList*)paAttr)->ActionList.genericlist.clear();

					aCgenericConditional tmpaCgenCond;
					tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
					tmpaCgenCond.destElements.clear();
					aCgenericConditional::aCexpressDest tempaCDest;

					tempaCDest.destType = eT_Direct;
					tempaCDest.pPayload = new aCreferenceList;
					
					REFERENCE_LIST refList;
					ddpREF ref;
					ddpREFERENCE reference;

					ref.type = METHOD_ID_REF;
					ref.val.id = nMethodItemId;
					reference.push_back(ref);
					refList.push_back(reference);

					fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),&refList);
					tmpaCgenCond.destElements.push_back(tempaCDest);
					((aCattrActionList*)paAttr)->ActionList.genericlist.push_back(tmpaCgenCond);
					paAttr->attr_mask = 0x400;
					pItem->attrLst.push_back(paAttr);
				}
			break;
			}
		}
}

/*******************************************************************************************/
/* F */


/********************************************************************************************/
void UpdateDevicePostWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId)
{
		AitemList_t::iterator itr1;
		BYTE byIsVarAttrPostWriteAct = 0;
		aCitemBase* pItem;

		for(itr1 = (pAbstractDev->AitemPtrList).begin();
			itr1 < (pAbstractDev->AitemPtrList).end();    ++itr1)
		{
			pItem = *itr1;
			if ( pItem == NULL )
				continue;
			if(pItem->itemId == nVarItemId)
			{
				FOR_iT(AattributeList_t,(*itr1)->attrLst)
				{			
					genericTypeEnum_t git = 
										 getAttrType4Mask(pItem->itemType, (*iT)->attr_mask);

					if(git.gAttrMethod == varAttrPostWriteAct)
					{
						/* Add the action to the existing list */
						aCattrBase* paAttr = NULL;
						paAttr = (aCattrActionList*)(*iT);

						aCgenericConditional &tmpaCgenCond = ((aCattrActionList*)paAttr)->ActionList.genericlist.back();
						aCgenericConditional::aCexpressDest &tempaCDest = tmpaCgenCond.destElements.back();

						REFERENCE_LIST refList;
						ddpREF ref;
						ddpREFERENCE reference;

						ref.type = METHOD_ID_REF;
						ref.val.id = nMethodItemId;
						reference.push_back(ref);
						refList.push_back(reference);

						fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),&refList);

						byIsVarAttrPostWriteAct = 1;
						break;
					}
				}

				/* If no Pre Write Action attribute is found add new 
				  attribute */
				if(!byIsVarAttrPostWriteAct)
				{
					aCattrBase* paAttr = NULL;
					paAttr =  new aCattrActionList;

					((aCattrActionList*)paAttr)->ActionList.genericlist.clear();

					aCgenericConditional tmpaCgenCond;
					tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
					tmpaCgenCond.destElements.clear();
					aCgenericConditional::aCexpressDest tempaCDest;

					tempaCDest.destType = eT_Direct;
					tempaCDest.pPayload = new aCreferenceList;
					
					REFERENCE_LIST refList;
					ddpREF ref;
					ddpREFERENCE reference;

					ref.type = METHOD_ID_REF;
					ref.val.id = nMethodItemId;
					reference.push_back(ref);
					refList.push_back(reference);

					fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),&refList);
					tmpaCgenCond.destElements.push_back(tempaCDest);
					((aCattrActionList*)paAttr)->ActionList.genericlist.push_back(tmpaCgenCond);
					paAttr->attr_mask = 0x800;
					pItem->attrLst.push_back(paAttr);
				}
			break;
			}
		}

}

/* VMKP Added on 040203 */
/*******************************************************************************************/
/* F */


/********************************************************************************************/

/* VMKP Modified on 091203 */
//RETURNCODE identLookup(DWORD& newKey, Indentity_t& sID, StrVector_t* strList)
RETURNCODE identLookup(DD_Key_t& newKey, Indentity_t& sID, StrVector_t* strList)
{ // is is an isolated function - it doesn't actually change the key or device
  // this means we don't care about the itemindexlist
	RETURNCODE rc = SUCCESS;

	if (strList != NULL && strList->size() > 0 )
	{	// empty strings
		for (StrVector_t::iterator 
			    pos = strList->begin();
				pos < strList->end();
				   )
		{
			pos->erase();
			strList->erase(pos);
			pos = strList->begin();
		}
	
	}
	char wrkStr[80];
	string wrkString;
	WORD wrkShrt;
		
		{ // ddrev
			
			wrkString = "    DD revision ";
			wrkShrt = (WORD)(newKey & 0xff); // warning C4244: '=' : conversion from 'DWORD' to 'BYTE' <HOMZ: added cast>
			sprintf(wrkStr,"%d (0x%02x)", wrkShrt,wrkShrt);
			wrkString += wrkStr;
			if ( strList != NULL )
				strList->push_back(wrkString);
		}

		{ // devrev
			wrkString = "Device revision ";
			wrkShrt = (WORD)((newKey >> 16) & 0xff);  // warning C4244: '=' : conversion from 'DWORD' to 'BYTE' <HOMZ: added cast>
			sprintf(wrkStr,"%d (0x%02x)", wrkShrt,wrkShrt);
			wrkString += wrkStr;
			if ( strList != NULL )
				strList->insert(strList->begin(),wrkString);
		}

		{ // dev type
			wrkString = "Device Type ";
			wrkShrt = (WORD)((newKey >> 32) & 0xffff);  // warning C4244: '=' : conversion from 'DWORD' to 'BYTE' <HOMZ: added cast>
			sprintf(wrkStr,"%d (0x%02x)", wrkShrt, wrkShrt);
			wrkString += wrkStr;
			if ( strList != NULL )
				strList->insert(strList->begin(),wrkString);
		}

		{ // mfg
			wrkString = "Manufacturer ";
			wrkShrt = (WORD)((newKey >> 48) & 0xffff);  // warning C4244: '=' : conversion from 'DWORD' to 'BYTE' <HOMZ: added cast>
			sprintf(wrkStr,"%d (0x%02x)", wrkShrt, wrkShrt);
			wrkString += wrkStr;
			if ( strList != NULL )
				strList->insert(strList->begin(),wrkString);
		}

		return rc;
}
/* VMKP Modified on 091203 */

/****************************************************************************************/
/* F */


/********************************************************************************************/
//Vibhor 140404: Added new parameter stdTbl
RETURNCODE allocDeviceList(hCddbDevice *stdTbl,aCentry** ppEntryRoot )
{
    if ( (*ppEntryRoot) != NULL )
	{
		delete (*ppEntryRoot);
	}
	(*ppEntryRoot) = new aCentry;
	if ((*ppEntryRoot) == NULL)
		return FAILURE;

	(*ppEntryRoot)->name   = L"System Root";
	(*ppEntryRoot)->number =  0;
/*Vibhor 140404: Start of Code*/
  return getSystem(stdTbl,*ppEntryRoot); // fill the lists
/*Vibhor 140404: End of Code*/  
};


/****************************************************************************************/
/*ANOOP<START> For invoking the DD Library from SDC	*/


/********************************************************************************************/
//Vibhor 140404: Added new parameter stdTbl
/* depricated - use allocDeviceList *********************************************************/
#if 0
RETURNCODE allocDeviceListForSDC(hCddbDevice *stdTbl,aCentry** ppEntryRoot )
{
    if ( (*ppEntryRoot) != NULL )
	{
		delete (*ppEntryRoot);
	}
	(*ppEntryRoot) = new aCentry;
	if ((*ppEntryRoot) == NULL)
		return FAILURE;

	(*ppEntryRoot)->name   = L"System Root";
	(*ppEntryRoot)->number =  0;
/*Vibhor 140404: Start of Code*/ 
  return getSystemForSDC(stdTbl,*ppEntryRoot); // fill the lists
/*Vibhor 140404: End of Code*/   
};
/*ANOOP<END> For invoking the DD Library from SDC	*/
#endif


#define SYSNAME   L"DD Library"
#define MFGCHILD  L"Manufacturer"
#define DEVCHILD  L"Device Type"
#define DRVCHILD  L"Device Revision"
#define DDRCHILD  L"DD Revision"

#define DEVREVSTR L"Dev Rev "
#define D_DREVSTR L" DD Rev "


/****************************************************************************************/
/* F */


/********************************************************************************************/
//Vibhor 140404: Added new parameter stdTbl
RETURNCODE getSystem(hCddbDevice *stdTbl,aCentry* pEntryRoot)
{
	RETURNCODE rc = SUCCESS;
	char wrkStr[MAX_FILE_NAME_PATH];
	aCManList manList;
	aCentry wrkMFG, wrkDT, wrkDR, wrkDDR;
	char tmpchDbdir[MAX_FILE_NAME_PATH];
//    HANDLE DirSearch;
    string DirName;

	if (chDbdir != NULL && chDbdir[0] != '\0')
	{
#if 0
		/* stevev 24mar09 - removed commented code, see earlier version for contents */
#endif
		{strcpy(tmpchDbdir,chDbdir);}
	}
	else
	{
		string fName = getCurrentDirectory();
		strcpy(tmpchDbdir,fName.c_str());
		strcat(tmpchDbdir, OS_PATH_SEPR);
		strcat(tmpchDbdir,"Release");
	}
			
	if (tmpchDbdir[strlen(tmpchDbdir)-1] != OS_PATH_SEPR[0])
/*Vibhor 140404: Start of Code*/
		strcat(tmpchDbdir,OS_PATH_SEPR);
	strcpy(wrkStr,tmpchDbdir);
/*	strcat(wrkStr,"DEVICES.CFG");
	FILE *fdev = fopen(wrkStr ,"r");

	if(fdev)
		fillManufacturerList(fdev,&manList); */

	rc = fillManufacturerList(stdTbl,&manList);
	if(SUCCESS != rc)
	{
		manList.clear();
		return rc;
	}
/*Vibhor 140404: End of Code*/
   // may need to set system as current???
	pEntryRoot->number = 0;
	pEntryRoot->name   = SYSNAME;
	pEntryRoot->childTypeName = MFGCHILD;

	string PathFind = tmpchDbdir;
	string ManuPath,DevTypePath;
	char tmpManuPath[MAX_FILE_NAME_PATH],tmpDevTypePath[MAX_FILE_NAME_PATH];
	string fName;
	map <int, aCManEntry> :: const_iterator m1_AcIter;
	string szTemp3;

	/* Identify the DD file name and the path from which it has to be loaded
	This is calucuated by using command line args and Manufacturer ID, Devic Type
	and Device Revision */
	dDSearchOps LibSearch(PathFind, "*");
	if(LibSearch.find())
	do
	{
		szTemp3 = LibSearch.getName();

		if((LibSearch.isDirectory()) &&
			 (strcmp(szTemp3.c_str(),".")) && (strcmp(szTemp3.c_str(),"..")))
		 {
			 sprintf(tmpManuPath,"%s%s%s",PathFind.c_str(),szTemp3.c_str(),OS_PATH_SEPR);
			 ManuPath = tmpManuPath;

			// insert
			sscanf(szTemp3.c_str(),"%x",&wrkMFG.number);
			wrkMFG.childTypeName = DEVCHILD;
			m1_AcIter = manList.find(wrkMFG.number);
			
			if(m1_AcIter != manList.end())
				wrkMFG.name = m1_AcIter->second.name;

			 dDSearchOps ManuSearch(ManuPath, "*");
			 ManuSearch.find();

			 do
			 {
				szTemp3 = ManuSearch.getName();

				if((ManuSearch.isDirectory()) &&
					(strcmp(szTemp3.c_str(),".")) && (strcmp(szTemp3.c_str(),"..")))
				 {

					 sprintf(tmpDevTypePath,"%s%s%s",ManuPath.c_str(),szTemp3.c_str(),OS_PATH_SEPR);
					 DevTypePath = tmpDevTypePath;

			 		 sscanf(szTemp3.c_str(),"%x",&wrkDT.number);
					 
					 if(m1_AcIter != manList.end())
					 {
						vector <aCDevTypeEntry>::const_iterator c1_Iter;
						
						for(c1_Iter = m1_AcIter->second.children.begin();
								c1_Iter < m1_AcIter->second.children.end();c1_Iter++)
						{
							if(c1_Iter->number == wrkDT.number)
							{
								wrkDT.name = c1_Iter->name;
								wrkDT.help = c1_Iter->help;
								break;// stevev 25jun09 - assume mfg/devType is unique
							}
						}
						// stevev 25jun09 - tables converted to mfdt 4 digit keys
						if ( wrkDT.name.empty() )
						{// we failed to get a match
							if ( (wrkMFG.number & 0xFF) == wrkMFG.number  &&//16 bit mfg
								 (wrkDT.number  & 0xFF) == wrkDT.number   )// 16 bit devtype
							{
								unsigned tmpNumber = (wrkMFG.number << 8) + wrkDT.number;
								// try again
								for(c1_Iter = m1_AcIter->second.children.begin();
										c1_Iter < m1_AcIter->second.children.end();c1_Iter++)
								{
									if(c1_Iter->number == tmpNumber)
									{
										wrkDT.name = c1_Iter->name;
										wrkDT.help = c1_Iter->help;
										break;// stevev 25jun09 - assume mfg/devType is unique
									}
								}// next
							}
						}
					 }

					 wrkDT.childTypeName = DRVCHILD;
					 dDSearchOps DevTypeSearch(DevTypePath, "*.fm?"); // "*.fms");//temp

					 if (DevTypeSearch.find()) // HOMZ - Check for invalid handle
					 {
						 szTemp3 = DevTypeSearch.getName();
						 strncpy(wrkStr,szTemp3.c_str(),2);
						 wrkStr[2] = 0;
						 sscanf(wrkStr,"%x",&wrkDR.number);
						 wrkDR.childTypeName = DDRCHILD;
						 do
						 {
							szTemp3 = DevTypeSearch.getName();
							if(strncmp(szTemp3.c_str(),wrkStr,2))
							{// don't match, push the old one & start anew
								strncpy(wrkStr,szTemp3.c_str(),2);
								wrkStr[2] = 0;
								wrkDT.children.push_back(wrkDR);
								wrkDR.clear();
								sscanf(wrkStr,"%x",&wrkDR.number);
								wrkDR.childTypeName = DDRCHILD;
							}
							char ddRev[3],type;
							//stevev strncpy(ddRev,szTemp3.c_str(),4);
							//stevev ddRev[0] = ddRev[2];
							//stevev ddRev[1] = ddRev[3];
							//stevev ddRev[2] = 0;
							ddRev[0] = szTemp3[2];
							ddRev[1] = szTemp3[3];
							ddRev[2] = 0;
							type = szTemp3[7];
							if ( type == 's' || type == 'S' ) type = '5';

							sscanf(ddRev,"%x",&wrkDDR.number);
							//stevev-- wtf???   wrkDDR.number  = atoi(ddRev);
							wrkDDR.number += (type - 0x30)<<8; // put the file version above dd rev
							wrkDDR.childTypeName = L"";
							wrkDR.children.push_back(wrkDDR);
							wrkDDR.clear();
						 }
						 while(DevTypeSearch.find());	

						wrkDT.children.push_back(wrkDR);// push the last one
						wrkDR.clear();
					 }  // if DevTypeSearch is not INVALID_HANDLE_VALUE					  


					wrkMFG.children.push_back(wrkDT);
					wrkDT.clear();
				 }  // if FILE_ATTRIBUTE_DIRECTORY
			 }
			 while(ManuSearch.find());

			pEntryRoot->children.push_back(wrkMFG);
			wrkMFG.clear();
		 }
	 }
	 while(LibSearch.find());
	// we be done

	return rc;
}


#if 0 /* obsoleted ***************************************************************************/
/****************************************************************************************/
/*ANOOP<START> For invoking the DD Library from SDC	*/


/********************************************************************************************/
//Vibhor 140404: Added new parameter stdTbl
// stevev 25mar09 - modified for bug 2552,2549, general cleanup, add comments
//                - For earlier code, see revisions before this date
RETURNCODE getSystemForSDC(hCddbDevice *stdTbl,aCentry* pEntryRoot)
{	

	// temporarily here
	bool showFMX = true;//true shows all file formats & dev revs and DD revs

	RETURNCODE rc = SUCCESS;	
    WIN32_FIND_DATA FindFileData;
	HANDLE   mfgSEARCH, devTypeSEARCH, fmxSEARCH;
	map <int, aCManEntry> :: const_iterator manEntryIt;

	aCentry wrkMfg, wrkDevTyp, wrkFMX;

	wchar_t* pWdChar = NULL;
	string  nStr;
	wstring rootName, dirName, srchName;
	wstring bkSlsh(L"\\");

	aCManList mfgList;
	rc = fillManufacturerList(stdTbl,&mfgList);// get lookup strings from stdTables dev
	if(rc != SUCCESS)
	{
		mfgList.clear();
		return rc;
	}

	if (chDbdir != NULL && chDbdir[0] != '\0')
	{
		nStr = chDbdir;
		rootName = AStr2TStr(nStr);//TStr2AStr
	}
	else
	{
		USES_CONVERSION;
		TCHAR szTemp[MAX_FILE_NAME_PATH];
		GetCurrentDirectory(MAX_FILE_NAME_PATH,szTemp);
		rootName = szTemp;
		rootName += L"\\Release";
	}
	if ( *((unsigned short*)&(*(rootName.rbegin()))) != bkSlsh[0])
	{
		rootName += bkSlsh;
	}
	
	pEntryRoot->number = 0;
	pEntryRoot->name   = SYSNAME;
	pEntryRoot->help   = rootName;// for system root only
	pEntryRoot->childTypeName = MFGCHILD;

	srchName  = rootName;
	srchName += L"??????";
	
	mfgSEARCH = ::FindFirstFile( srchName.c_str(), &FindFileData );
	if (mfgSEARCH == INVALID_HANDLE_VALUE)
	{	return FAILURE;	}
	// else start processing with first directory
	do
	{
		wstring dirNameMfg(FindFileData.cFileName);

		if( (FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
			(dirNameMfg != L".") && (dirNameMfg != L"..") )
		{
			wrkMfg.number  = wcstol( dirNameMfg.begin(), &pWdChar, 16 );
			wrkMfg.name    = L"";
			wrkMfg.help    = L"";
			wrkMfg.childTypeName = DEVCHILD;
			manEntryIt = mfgList.find(wrkMfg.number);			
			if(manEntryIt != mfgList.end())
				wrkMfg.name = manEntryIt->second.name;
			
			srchName  = rootName;
			srchName += dirNameMfg;
			srchName += L"\\????";
			
			devTypeSEARCH = ::FindFirstFile( srchName.c_str(), &FindFileData );
			if (devTypeSEARCH == INVALID_HANDLE_VALUE)
			{	continue;// no devices; continue with next mfg
			}
			// else start processing with first device type directory
			do
			{
				wstring dirNameDevTyp(FindFileData.cFileName);
				unsigned lastFile = 0;

				if( (FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
					(dirNameDevTyp != L".") && (dirNameDevTyp != L"..") )
				{
					wrkDevTyp.number  = wcstol( dirNameDevTyp.begin(), &pWdChar, 16 );
					wrkDevTyp.name    = L"";
					wrkDevTyp.help    = L"";
					wrkDevTyp.childTypeName = DRVCHILD;
					
					if(manEntryIt != mfgList.end())
					{
						vector <aCDevTypeEntry>::const_iterator devTypEntryIt;

						for(devTypEntryIt = manEntryIt->second.children.begin();
							devTypEntryIt < manEntryIt->second.children.end(); ++devTypEntryIt)
						{
							aCDevTypeEntry *pdtE = (aCDevTypeEntry *)&(*devTypEntryIt);
							if(pdtE->number == wrkDevTyp.number)
							{	wrkDevTyp.name = pdtE->name;   
								wrkDevTyp.help = pdtE->help;
							}// else keep looking
						}//next device type for this manufacturer
					}//else no mfg entry in the tables, won't be any dev type

					srchName  = rootName;
					srchName += dirNameMfg;
					srchName += L"\\";
					srchName += dirNameDevTyp;
					srchName += L"\\????.fm?";
					
					fmxSEARCH = ::FindFirstFile( srchName.c_str(), &FindFileData );
					if (fmxSEARCH == INVALID_HANDLE_VALUE)
					{	continue;// no DDs; continue with next devType
					}

					do
					{
						wstring fileNameFMX(FindFileData.cFileName);
						wstring sDevRev, sDDRev, sExt, sUse;

						sDevRev = fileNameFMX.substr(0,2);
						sDDRev  = fileNameFMX.substr(2,2);
						sExt    = fileNameFMX.substr(5,3);

						wrkFMX.number  = wcstol( fileNameFMX.begin(), &pWdChar, 16 );// goes to dot
						wrkFMX.name    = L"";
						wrkFMX.help    = sExt;
						wrkFMX.childTypeName = DDRCHILD;
						if ( (lastFile == wrkFMX.number && ! showFMX) || wrkFMX.number == 0 )
							continue;// don't show the different file types or error
						lastFile = wrkFMX.number;// under all conditions

						sUse  = DEVREVSTR;
						sUse += sDevRev;
						sUse += D_DREVSTR;
						sUse += sDDRev;

						if (showFMX)
						{
							sUse += L" (";
							sUse += sExt;
							sUse += L")";
						}

						wrkFMX.name    = sUse;
						wrkDevTyp.children.push_back(wrkFMX);
						wrkFMX.clear();
					}
					 while(::FindNextFile(fmxSEARCH, &FindFileData));
					::FindClose(fmxSEARCH);

					wrkMfg.children.push_back(wrkDevTyp);
					wrkDevTyp.clear();
				}// else not a directory or not one of interest, continue
			}	
			 while(::FindNextFile(devTypeSEARCH, &FindFileData));
			::FindClose(devTypeSEARCH);

			pEntryRoot->children.push_back(wrkMfg);
			wrkMfg.clear();
		}// else not a directory or not one of interest, continue
	}	
	 while(::FindNextFile(mfgSEARCH, &FindFileData));
	::FindClose(mfgSEARCH);
	// we be done

#if 0 /* turn it off */
//////////////////////////////////////////////////////////////////////////////////////
//x   RETURNCODE rc = SUCCESS;
   char wrkStr[MAX_FILE_NAME_PATH];
   aCManList manList;
   aCentry wrkMFG, wrkDT, wrkDR, wrkDDR;
   char tmpchDbdir[MAX_FILE_NAME_PATH];
//x   WIN32_FIND_DATA FindFileData;
//    HANDLE DirSearch;
    string DirName;

	if (chDbdir != NULL && chDbdir[0] != '\0')
	{
		strcpy(tmpchDbdir,chDbdir);
	}
	else
	{
		USES_CONVERSION;
		TCHAR szTemp[MAX_FILE_NAME_PATH];
		string fName;
#ifndef _WIN32_WCE		
		GetCurrentDirectory(MAX_FILE_NAME_PATH,szTemp);
#endif		
		fName = OLE2A(szTemp);
		strcpy(tmpchDbdir,fName.c_str());
		strcat(tmpchDbdir,"\\Release");
	}
	if (tmpchDbdir[strlen(tmpchDbdir)-1] != '\\')
		strcat(tmpchDbdir,"\\");
	strcpy(wrkStr,tmpchDbdir);

	rc = fillManufacturerList(stdTbl,&manList);
	if(SUCCESS != rc)
	{
		manList.clear();
		return rc;
	}
   // may need to set system as current???
	pEntryRoot->number = 0;
	pEntryRoot->name   = SYSNAME;
	pEntryRoot->help   = ( AStr2TStr( string( tmpchDbdir ) ) );// for system root only
	pEntryRoot->childTypeName = MFGCHILD;


	/* Identify the DD file name and the path from which it has to be loaded
	This is calculated by using command line args and Manufacturer ID, Devic Type
	and Device Revision */

	HANDLE ManuSearch,DevTypeSearch,LibSearch;
	string PathFind = tmpchDbdir;
	string ManuPath,DevTypePath;
	char tmpManuPath[MAX_FILE_NAME_PATH],tmpDevTypePath[MAX_FILE_NAME_PATH];
	string fName;
	map <int, aCManEntry> :: const_iterator m1_AcIter;
	char tmpPathFind[MAX_FILE_NAME_PATH];
	strcpy(tmpPathFind,PathFind.c_str());
	//strcat(tmpPathFind,"*");
	strcat(tmpPathFind,"??????");
	string szTemp3;
	USES_CONVERSION;
	CComBSTR combstr(tmpPathFind);
	LPTSTR szTemp = OLE2T(combstr.m_str);
	LibSearch=::FindFirstFile(szTemp, &FindFileData );
 
	if(LibSearch  != INVALID_HANDLE_VALUE)
	do
	{
#ifdef UNICODE	 
		CComBSTR combstr3(FindFileData.cFileName);
		szTemp3 = OLE2A(combstr3.m_str);
#else
		szTemp3 = FindFileData.cFileName;
#endif
		 if((FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
			 (strcmp(szTemp3.c_str(),".")) && (strcmp(szTemp3.c_str(),"..")))
		 {
			 sprintf(tmpManuPath,"%s%s\\",PathFind.c_str(),szTemp3.c_str());
			 ManuPath = tmpManuPath;

			// insert
			if ( ! sscanf(szTemp3.c_str(),"%x",&wrkMFG.number) )
				continue; // not numeric, go tot next
			wrkMFG.childTypeName = DEVCHILD;
			m1_AcIter = manList.find(wrkMFG.number);
			
			if(m1_AcIter != manList.end())
				wrkMFG.name = m1_AcIter->second.name;

			char tmpManuPath[MAX_FILE_NAME_PATH];

			strcpy(tmpManuPath,ManuPath.c_str());
			//strcat(tmpManuPath,"*");
			strcat(tmpManuPath,"????");

			CComBSTR combstr1(tmpManuPath);
			LPTSTR szTemp1 = OLE2T(combstr1.m_str);
			 ManuSearch = ::FindFirstFile(szTemp1, &FindFileData);
			 do
			 {
#ifdef UNICODE
				CComBSTR combstr4(FindFileData.cFileName);
				szTemp3 = OLE2A(combstr4.m_str);
#else
				szTemp3 = FindFileData.cFileName;
#endif
			 if((FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
					(strcmp(szTemp3.c_str(),".")) && (strcmp(szTemp3.c_str(),"..")))
				 {

					 sprintf(tmpDevTypePath,"%s%s\\",ManuPath.c_str(),szTemp3.c_str());
					 DevTypePath = tmpDevTypePath;

			 		 if ( ! sscanf(szTemp3.c_str(),"%x",&wrkDT.number) )
						 continue; // non-numeric - skip
					 
					 if(m1_AcIter != manList.end())
					 {
						vector <aCDevTypeEntry>::const_iterator c1_Iter;
						
						for(c1_Iter = m1_AcIter->second.children.begin();
								c1_Iter < m1_AcIter->second.children.end();c1_Iter++)
						{
							if(c1_Iter->number == wrkDT.number)
							{	wrkDT.name = c1_Iter->name;   
								wrkDT.help = c1_Iter->help;
							}
						}
					 }

					 wrkDT.childTypeName = DRVCHILD;
 					 strcpy(tmpPathFind,DevTypePath.c_str());
//					 strcat(tmpPathFind,"*.fms"); temp
					 //strcat(tmpPathFind,"*.fm?");
					 strcat(tmpPathFind,"????.fm?");
					 CComBSTR combstr2(tmpPathFind);
					 LPTSTR szTemp2 = OLE2T(combstr2.m_str);
					 DevTypeSearch = ::FindFirstFile( szTemp2, &FindFileData);
					 if ( DevTypeSearch == INVALID_HANDLE_VALUE )
						 continue; // no *.fm? files
					 szTemp3 = OLE2A(FindFileData.cFileName);
					 strncpy(wrkStr,szTemp3.c_str(),2);
					 wrkStr[2] = 0;
					sscanf(wrkStr,"%x",&wrkDR.number);
					 wrkDR.childTypeName = DDRCHILD;
					 string strDevRev = "Dev Rev ";
					 char tmpBuf[20]={0};
					 _itoa(wrkDR.number,tmpBuf,10);	// underscore added PAW 27/04/09
					 strDevRev += tmpBuf;
					 char ddRevOld[3] = {0};
					 bool bNextDevRev = true;
					 do
					 {
						szTemp3 = OLE2A(FindFileData.cFileName);
						if(strncmp(szTemp3.c_str(),wrkStr,2))
						{
							strncpy(wrkStr,szTemp3.c_str(),2);
							wrkStr[2] = 0;
							wrkDR.clear();
							sscanf(wrkStr,"%x",&wrkDR.number);
							strDevRev.empty();
							strDevRev = "Dev Rev ";
							 char tmpBuf1[20]={0};
							 _itoa(wrkDR.number,tmpBuf1,10);// underscore added PAW 27/04/09
							 strDevRev += tmpBuf1;
							 wrkDR.childTypeName = DDRCHILD;
							 bNextDevRev = true;
						}
						char ddRev[5];
						strncpy(ddRev,szTemp3.c_str(),4);
						ddRev[0] = ddRev[2];
						ddRev[1] = ddRev[3];
						ddRev[2] = 0;
						if(!bNextDevRev && !strcmp(ddRev,ddRevOld))
						{
							continue;
						}
						else
						{
							strcpy(ddRevOld,ddRev);
							bNextDevRev = false;
						}
						
						if ( ! sscanf(ddRev,"%x",&wrkDDR.number) )
							continue;// not a dd file

					//	wrkDDR.number  = atoi(ddRev);
						string strDDRev = strDevRev;
						strDDRev += " DD Rev ";
						char tmpBuf1[20]={0};
						_itoa(wrkDDR.number,tmpBuf1,10);// underscore added PAW 27/04/09
						strDDRev += tmpBuf1;
						int nNumber = wrkDDR.number + (wrkDR.number << 8);

						aCentry tmpwrkDR;
						tmpwrkDR.number = nNumber;
						tmpwrkDR.childTypeName = DDRCHILD;
						//TEMPORARY WORK_AROUND::  tmpwrkDR.name = strDDRev;
						tmpwrkDR.name = AStr2TStr(strDDRev);
						wrkDT.children.push_back(tmpwrkDR);
					 }
					 while(::FindNextFile(DevTypeSearch, &FindFileData));
				 	::FindClose(DevTypeSearch);

//					wrkDT.children.push_back(wrkDR);
					wrkDR.clear();

					wrkMFG.children.push_back(wrkDT);
					wrkDT.clear();
				 }
			 }
			 while(::FindNextFile(ManuSearch, &FindFileData));
		 	::FindClose(ManuSearch);

			pEntryRoot->children.push_back(wrkMFG);
			wrkMFG.clear();
		 }
	 }
	 while(::FindNextFile(LibSearch, &FindFileData));
	::FindClose(LibSearch);
	// we be done
#endif /* turned off */

	return rc;
}
/*ANOOP<START> For invoking the DD Library from SDC	*/

/****************************************************************************************/
/* F */

// end obsoleted duplicate code
#endif // if 0
/********************************************************************************************/

RETURNCODE releaseDevList (aCentry** ppEntryRoot )
{
	delete (*ppEntryRoot);
	*ppEntryRoot = NULL;
	return SUCCESS;
};

/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE initDB(const char *pDbdir)
{
	char szSrchPath[MAX_PATH]; 
	int len = strlen(pDbdir);

	if (len >= (MAX_FILE_NAME_PATH -32))
	{
		strncpy(chDbdir,pDbdir,(MAX_FILE_NAME_PATH -32));
		szSrchPath[MAX_FILE_NAME_PATH -32] = '\0';
	}
	else
	{
		strcpy(chDbdir,pDbdir);
	}

	//memcpy(chDbdir,pDbdir,len);
	if (!verifyReleaseDir())
	{
		chDbdir[0] = _T('\0');// clear the path if it is invalid
		return FAILURE;
	}

	return SUCCESS;
}


/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE allocDevice(DD_Key_t Key, aCdevice **pAbstractDev)
{
	*pAbstractDev = new aCdevice;

	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
RETURNCODE procureStringBySingleRemote(INT32 value,INT32 dictType, wchar_t* pCstr, wstring** ppString2BeFilled)
{
	return FAILURE;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE procureStringByDoubleRemote(DD_Key_t section,INT32 offset,INT32 dictType,wchar_t* pCstr, wstring** ppString2BeFilled)
{
	return FAILURE;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
RETURNCODE releaseStringVect(StrVector_t** ppStrList)// return the memory
{
	if(*ppStrList != NULL)
	{
		(*ppStrList)->clear();
		delete *ppStrList;
		*ppStrList =  NULL;
	}
	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE releaseDB()
{
	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE closeDB()
{
	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

// identity from key <with strings>
RETURNCODE idFromKey(DWORD& newKey, 
							 Indentity_t& sID, StrVector_t** ppStrList)
{ 
#ifdef _NO_LONGER_SUPPORTED_	// 8feb07
	BYTE DDRev = 0;
	char tmpchDbdir[MAX_FILE_NAME_PATH];

//    WIN32_FIND_DATA FindFileData;
//    HANDLE DirSearch;
    string DirName;

		if (chDbdir != NULL && chDbdir[0] != '\0')
		{
#if 0
			if (!strstr(chDbdir,"Release"))
			{
				if(!strstr(chDbdir,"release"))
				{
					strcpy(tmpchDbdir,chDbdir);
					strcat(tmpchDbdir,"\\*");

					USES_CONVERSION;
					CComBSTR combstr(tmpchDbdir);
					LPTSTR szTemp = OLE2T(combstr.m_str);
					DirSearch=::FindFirstFile(szTemp, &FindFileData );
 
					if(DirSearch  != INVALID_HANDLE_VALUE)
						do
						{	 
							 string strTemp = OLE2A(FindFileData.cFileName);

							 if((FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
								 (!strcmp(strTemp.c_str(),"Release")) || (!strcmp(strTemp.c_str(),"release")))
							 {
								DirName = strTemp.c_str();
								break;
							 }
						}
						while(::FindNextFile(DirSearch, &FindFileData));
		 			::FindClose(DirSearch);
				
					if(DirName.size())
					{
						strcpy(tmpchDbdir,chDbdir);
						strcat(tmpchDbdir,"\\");
						strcat(tmpchDbdir,DirName.c_str());
					}
					else
						return FAILURE;
				}
				else
				{strcpy(tmpchDbdir,chDbdir);}
			}
			else
#endif
			{strcpy(tmpchDbdir,chDbdir);}
		}
		else
		{
			USES_CONVERSION;
			TCHAR szTemp[MAX_FILE_NAME_PATH];
			string fName;
			GetCurrentDirectory(MAX_FILE_NAME_PATH,szTemp);
			fName = OLE2A(szTemp);
			strcpy(tmpchDbdir,fName.c_str());
			strcat(tmpchDbdir,"\\Release");
		}
		if (tmpchDbdir[strlen(tmpchDbdir)-1] != '\\')
			strcat(tmpchDbdir,"\\");
		
		DDRev = (BYTE) ( newKey & 0xFF ); // warning C4244: '=' : conversion from 'DWORD' to 'BYTE' <HOMZ: added cast>
		sID.cManufacturer = (BYTE)((newKey & 0xFF000000) >> 24);  // warning C4244
		sID.cDeviceType	  = (BYTE)((newKey & 0xFF0000) >> 16);    // warning C4244
		sID.cDeviceRev	  = (BYTE)((newKey & 0xFF00) >> 8);       // warning C4244

		if ( DDRev == 0xff )// signal to search for latest
		{
			return keyFromID(sID, newKey, ppStrList );
		}
		else
		{
			/*sprintf(tmpchDbdir,"%s%06x%s%04x%s%02x%02x.fms",tmpchDbdir,sID.cManufacturer,"\\",
					sID.cDeviceType,"\\",sID.cDeviceRev,DDRev);*/ //temp

			sprintf(tmpchDbdir,"%s%06x%s%04x%s%02x%02x.fm6",tmpchDbdir,sID.cManufacturer,"\\",
					sID.cDeviceType,"\\",sID.cDeviceRev,DDRev);
			strcpy(chInputFileName,tmpchDbdir);

			*ppStrList =  new StrVector_t;
			identLookup(newKey, sID, *ppStrList);
		}
#endif // no longer supported
	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

// key from identity <with strings>
RETURNCODE keyFromID(Indentity_t& newIdent, 
							 DWORD& wrkingKey, StrVector_t** strList )
{ 
#ifdef _NO_LONGER_SUPPORTED_	// 8feb07

	char tmpchDbdir[MAX_FILE_NAME_PATH];

	/* Read Key and pRemoteStr from File */
	wrkingKey = ((newIdent.cManufacturer)<<24) | 
	((newIdent.cDeviceType )<<16) | 
	((newIdent.cDeviceRev  )<< 8);
    WIN32_FIND_DATA FindFileData;
//    HANDLE DirSearch;
    string DirName;

		if (chDbdir != NULL && chDbdir[0] != '\0')
		{
#if 0	/* chDbdir now is the release directory */
			if (!strstr(chDbdir,"Release"))
			{
				if(!strstr(chDbdir,"release"))
				{
					strcpy(tmpchDbdir,chDbdir);
					strcat(tmpchDbdir,"\\*");

					USES_CONVERSION;
					CComBSTR combstr(tmpchDbdir);
					LPTSTR szTemp = OLE2T(combstr.m_str);
					DirSearch=::FindFirstFile(szTemp, &FindFileData );
 
					if(DirSearch  != INVALID_HANDLE_VALUE)
						do
						{	 
							 string strTemp = OLE2A(FindFileData.cFileName);

							 if((FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
								 (!strcmp(strTemp.c_str(),"Release")) || (!strcmp(strTemp.c_str(),"release")))
							 {
								DirName = strTemp.c_str();
								break;
							 }
						}
						while(::FindNextFile(DirSearch, &FindFileData));
		 			::FindClose(DirSearch);
					
					if(DirName.size())
					{
						strcpy(tmpchDbdir,chDbdir);
						strcat(tmpchDbdir,"\\");
						strcat(tmpchDbdir,DirName.c_str());
					}
					else
						return FAILURE;
				}
				else
				{strcpy(tmpchDbdir,chDbdir);}
			}
			else
#endif
			{strcpy(tmpchDbdir,chDbdir);}
		}
		else
		{
			USES_CONVERSION;
			TCHAR szTemp[MAX_FILE_NAME_PATH];
			string fName;
			GetCurrentDirectory(MAX_FILE_NAME_PATH,szTemp);
			fName = OLE2A(szTemp);
			strcpy(tmpchDbdir,fName.c_str());
			strcat(tmpchDbdir,"\\Release");
		}

		if (tmpchDbdir[strlen(tmpchDbdir)-1] != '\\')
			strcat(tmpchDbdir,"\\");
		sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,newIdent.cManufacturer,"\\",
				newIdent.cDeviceType,"\\");

		//sprintf(FileType,"%02x*.fms",newIdent.cDeviceRev); //temp
		sprintf(FileType,"%02x*.fm?",newIdent.cDeviceRev);

		/* Identify the DD file name and the path from which it has to be loaded
		This is calucuated by using command line args and Manufacturer ID, Devic Type
		and Device Revision */

		int next,DDrev;
		HANDLE FileSearching;
		string PathFind = tmpchDbdir;
		string fName;	 //Latest file name
		string fNamePrev;//Name of file we got just before previous //Vibhor 011004: Added
		char tmpPathFind[MAX_FILE_NAME_PATH];
		strcpy(tmpPathFind,PathFind.c_str());
		strcat(tmpPathFind,FileType);
		USES_CONVERSION;
		CComBSTR combstr(tmpPathFind);
		LPTSTR szTemp = OLE2T(combstr.m_str);

		FileSearching=::FindFirstFile(szTemp, &FindFileData );
 
		 if(next=(FileSearching  != INVALID_HANDLE_VALUE))
		 do
		 {
#ifdef UNICODE
			  CComBSTR combstr(FindFileData.cFileName);
			  fName = OLE2A(combstr.m_str);
#else
			  fName = FindFileData.cFileName;
#endif
			  next=::FindNextFile(  FileSearching, &FindFileData  ); 
			  if(next)
			  {
				   fNamePrev = fName; //Store prev file name
			  }
		 }while(next);
 
		::FindClose(FileSearching);

  /*Now we have last two latest files in the directory.
  If the latest is not an fm6 and 
  If both files have same names with different (fm6 and fms)
  extensions, we need to take one with fm6*/
		if(!(strstr(fName.c_str(),"fm6")) && fNamePrev.size())
		{//The latest one is an fms and one before it has something too
			int iDDNameLatest, iDDNamePrev;
			sscanf(fName.c_str(),"%4x.fms",&iDDNameLatest);
			sscanf(fNamePrev.c_str(),"%4x.fm6",&iDDNamePrev);
			if(iDDNameLatest == iDDNamePrev)
			{
				fName = fNamePrev; // set fm6 as the latest DD
			}
		}
		if(fName.size())
		{
			USES_CONVERSION;
#ifdef UNICODE
			CComBSTR combstr(fName.c_str());
			string szTemp = OLE2A(combstr.m_str);
#else
			string szTemp( fName );
#endif
			strcat(tmpchDbdir,szTemp.c_str());
			strcpy(chInputFileName,tmpchDbdir);
			sscanf(szTemp.c_str(),"%4x.fm?",&DDrev);
			DDrev = DDrev & 0xff; //Get the Latest DD revision
			wrkingKey += DDrev;
		}
		else
		{
			OUTPUT(CERR_LOG | UI_LOG, "ERROR: No DD File present for the given device \'%s\'",
				tmpPathFind);
			return FAILURE; // error exit		
		}

		*strList =  new StrVector_t;
		identLookup(wrkingKey, newIdent, *strList);
#endif  // no longer supported 8feb07
	return SUCCESS;
}

/************************************************************************************************/
/* F */
/*This function returns the latest DD ie the DD with highest Dev Rev & therein highest DD Rev

*************************************************************************************************/

// key from identity <with strings>
//RETURNCODE getStdTableKey(Indentity_t& newIdent,DWORD& wrkingKey)
RETURNCODE getStdTableKey(Indentity_t& newIdent,DD_Key_t& wrkingKey)
{ 
   char tmpchDbdir[MAX_FILE_NAME_PATH];

	if ( makePath(newIdent,tmpchDbdir,MAX_FILE_NAME_PATH,wrkingKey) <= 0 )
	{
	   return FAILURE;// i don't think this is possible...
	}
   /**  code in this section was moved/replaced by the above makePath call
    **  it may be found by examining a version before 09feb07 
	**/

	string DirName;
	sprintf(FileType,"*.fm?");

	/* Identify the DD file name and the path from which it has to be loaded
	This is calucuated by using command line args and Manufacturer ID, Devic Type
	and Device Revision */
	int DDName,DevRev,DDrev;
//	string PathFind = tmpchDbdir;
	string fName;	 //Latest file name
	string fNamePrev;//Name of file we got just before previous //Vibhor 011004: Added

	char tmpPathFind[MAX_FILE_NAME_PATH];
	strcpy(tmpPathFind,tmpchDbdir);
	strcat(tmpPathFind,FileType);

	dDSearchOps FileSearching(tmpchDbdir, FileType);
	bool next = FileSearching.find();
	if( next )
	do
	{
		fName = FileSearching.getName();
		next=FileSearching.find(); //FAT in time written order, others in alphabetical order
		if(next)
		{
		   fNamePrev = fName; //Store prev file name
		}
	}while(next);

/*Now we have last two latest files in the directory.
  If the latest is not an fm6 and 
  If both files have same names with different (fm6 and fms)
  extensions, we need to take one with fm6*/
	if(!(strstr(fName.c_str(),"fm6")) && fNamePrev.size())
	{//The latest one is an fms and one before it has something too
		int iDDNameLatest, iDDNamePrev;
		sscanf(fName.c_str(),"%4x.fms",&iDDNameLatest);
		sscanf(fNamePrev.c_str(),"%4x.fm6",&iDDNamePrev);
		if(iDDNameLatest == iDDNamePrev)
		{
			fName = fNamePrev; // set fm6 as the latest DD
		}
	}

		if(fName.size())
		{
/*
	LINUX_PORT - REVIEW_WITH_HCF: Not sure why a std::string is converted 
	to CComBSTR and converted back to std::string. May be a copy-paste error?
*/
#if !defined(__GNUC__)
			USES_CONVERSION;
#ifdef UNICODE
			CComBSTR combstr(fName.c_str());
			string szTemp = OLE2A(combstr.m_str);
#else
			string szTemp(fName);
#endif

#else // __GNUC__
			string szTemp(fName);
#endif // __GNUC__

			strcat(tmpchDbdir,szTemp.c_str());
			strcpy(chInputFileName,tmpchDbdir);
			sscanf(szTemp.c_str(),"%4x.fm?",&DDName); //Vibhor 011004: Changed
			DevRev = (DDName  >> 8);  //Get the Latest Device Revision
			DDrev  = (DDName & 0xff); //Get the Latest DD revision
			wrkingKey &= 0xffffffff00000000ULL; // L&T Modifications : PortToAM335
			wrkingKey |= (DevRev << 16);
			wrkingKey += DDrev;
			// 22aug08 OUTPUT(CLOG_LOG,"getStdTableKey set identity to 0x%016x \n\t\tin '%s'\n",wrkingKey,chInputFileName);
			LOGIT(CLOG_LOG,"getStdTableKey set identity to 0x%016x \n\t\tin '%s'\n",wrkingKey,chInputFileName);
		}
		else
		{
			// 22aug08 OUTPUT(CERR_LOG | UI_LOG, "ERROR: No Std DD File present for the given device \'%s\'",
			LOGIT(CERR_LOG | UI_LOG, "ERROR: No Std DD File present for the given device \'%s\'",
				tmpPathFind);
			return FAILURE; // error exit		
		}

	return SUCCESS;
}

/*********************************************************************************************** /
	stevev 13feb07 - holds the entire file find function
	- release dir was verified in initDB from DevMgr::instantiate
	-  identity's device revision is 0xff to key a search for the latest (std tables)
	- fills chInputFileName with a usable filename on SUCCESS
	-											also fills the key with what was actually opened
	- 03sep09 - stevev - rewrote to not use the encoded file format the main key
	-           also genericized it for future format numbers: 9,a,b etc
/ *******************************************************************************************/

typedef struct fileInfo
{
	int           version;
	vector<struct fileInfo> infoList;
	
	// return int   is best match version from this list or -1 if none found
	// pRetFileInfo is the returned fileInfo_t ptr of best match or NULL if none found
	// targetVersion is the number we want to get as close to as possible
	// exactMatch tells if the version must exactly match the target or just get as close
	//
	int 
	versionMatch(struct fileInfo*& pRetFileInfo, int targetVersion, bool exactMatch = false)
	{	struct fileInfo* pFile;
		int maxVersion = -1;
		pRetFileInfo = NULL; 
		vector<struct fileInfo>::iterator dirIT;
		for (dirIT = infoList.begin(); dirIT != infoList.end(); ++dirIT)
		{
			pFile = (struct fileInfo *)&(*dirIT);
			if ( pFile == NULL ) continue;
			if (  ( exactMatch && pFile->version == targetVersion ) ||
				  (!exactMatch && 
				   ( pFile->version >= maxVersion && pFile->version <= targetVersion) ) )
			{
				maxVersion   = pFile->version;
				pRetFileInfo = pFile;
			}
		}// next fileInfo
		return maxVersion;// -1 at none found
	};

	void clear(void) 
	{ for(vector<struct fileInfo>::iterator iT = infoList.begin();
	  iT != infoList.end(); ++iT){iT->clear();} infoList.clear(); version = 0; };

} fileInfo_t;
typedef fileInfo_t*				pVerInfo;
typedef vector<fileInfo_t>      verInfoList_t;
typedef verInfoList_t::iterator verInfoListITr_t;

class dirlist : public fileInfo_t// verInfoList_t
{
public:
//	int versionMatch(pVerInfo& pRetFileInfo, int targetVersion, bool exactMatch = false)
//	{
//		return fileInfo_t::versionMatch(pRetFileInfo,targetVersion,exactMatch);
//	};
	void addAfile(int devrev,int ddrev,int effVer)
	{
		fileInfo_t * pFile = NULL;
		verInfoListITr_t dirIT;
//		for (dirIT = begin(); dirIT != end(); ++dirIT)
		for (dirIT = infoList.begin(); dirIT != infoList.end(); ++dirIT)
		{
			pFile = (fileInfo_t *)&(*dirIT);
			if ( pFile == NULL ) continue;
			if ( pFile->version == devrev )
				break;
			else
				pFile = NULL;
		}// next file
		if (pFile != NULL)
		{// we have seen this device rev before
			fileInfo_t * pDev = NULL;
			verInfoListITr_t devIT;
			for (devIT = pFile->infoList.begin(); devIT != pFile->infoList.end(); ++devIT)
			{
				pDev = (fileInfo_t *)&(*devIT);
				if ( pDev == NULL ) continue;
				if ( pDev->version == ddrev )
					break;
				else
					pDev = NULL;
			}// next file
			
			if (pDev != NULL)
			{// we have seen this device rev & ddrev before
				fileInfo_t *     pEff = NULL;
				verInfoListITr_t effIT;
				for (effIT = pDev->infoList.begin(); effIT != pDev->infoList.end(); ++effIT)
				{
					pEff = (fileInfo_t *)&(*effIT);
					if ( pEff == NULL ) continue;
					if ( pEff->version == effVer )
						break;
					else
						pEff = NULL;
				}// next file
				if ( pEff == NULL )
				{// add it
					fileInfo_t thisfile;
					thisfile.version = effVer;
					pDev->infoList.push_back(thisfile);
				}
				else
				{// error - we have seen this file before
					LOGIT(CERR_LOG|CLOG_LOG,"DirectoryError: %d%d.fm%d is a duplicate.\n",
						devrev,ddrev,effVer);
				}
			}
			else // insert the rest
			{	
				fileInfo_t thisDD;
				fileInfo_t thisfile;
				thisfile.version = effVer;
				thisDD.infoList.push_back(thisfile);
				thisDD.version = ddrev;
				pFile->infoList.push_back(thisDD);
			}
		}
		else //-do the whole thing
		{
			fileInfo_t thisDev;
			fileInfo_t thisDD;
			fileInfo_t thisfile;
			thisfile.version = effVer;
			thisDD.infoList.push_back(thisfile);
			thisDD.version = ddrev;
			thisDev.infoList.push_back(thisDD);
			thisDev.version = devrev;
			infoList.push_back(thisDev);
		}
		return;
	};	
};

// uses the handle and find data dDSearchOpes first find call to generate a DD 
// directory tree for a Device-Type directory
// the returned directory tree will usually be scanned to find a usable DD file
// Caller's responsibility to verify the DirTreeReturned is empty when calling this function
//
RETURNCODE makeDirTree(dirlist& DirTreeReturned,  dDSearchOps& FFH)
{
	RETURNCODE rc = FAILURE;

	string fileName, fileBase, fileExt;
	int    devrev,   ddrev,    bffVersion;
	bool   foundOne = false;

	if (!FFH.wasFindSuccessful())
	{
		return rc;// failed
	}

	do
	{
		fileName = FFH.getName();
		// process filename into list
		if (fileName.length() != 8 || fileName[4] != '.')
		{
			LOGIT(CLOG_LOG,"Bad Name from search '%s'\n",fileName.c_str());
		}
		else
		{//bffVersion
			rc = SUCCESS;// we have one to work with
			fileBase = fileName.substr(0,4);
			fileExt  = fileName.substr(5,3);

			sscanf(fileBase.substr(0,2).c_str(), "%x",&devrev);
			sscanf(fileBase.substr(2,2).c_str(), "%x",&ddrev);

			char fe2 = fileExt[2];
			if (fe2 == 's' || fe2 == 'S') 
			{	bffVersion = 5;
			}
			else 
			if (fe2 == '6')
			{	bffVersion = 6;
			}
			else 
			if (fe2 == '8')
			{	bffVersion = 8;
			}
			else 
			if (fe2 == 'Y' || fe2 == 'y')
			{	bffVersion = 8;				// FMAHACK eff 8 file suffix of .psy temporarily [6/19/2014 timj]
			}
			else 
			if (fe2 == 'A' || fe2 == 'a')
			{	bffVersion = 10;
			}
			else 
			if (fe2 == 'B')
			{	bffVersion = 11;
			}
			else 
			if (fe2 == 'C')
			{	bffVersion = 12;
			}
			else 
			if (fe2 == 'D')
			{	bffVersion = 13;
			}
			else 
			if (fe2 == 'E')
			{	bffVersion = 14;
			}
			else 
			if (fe2 == 'F')
			{	bffVersion = 15;
			}

			DirTreeReturned.addAfile(devrev, ddrev, bffVersion);
		}// endelse
		if (FFH.find())
		{
			foundOne = true;
		}
		else
		{
			foundOne = false;
		} 
		// leave rc the value it is
		//FAT in time written order, others in alphabetical order
	}while(foundOne);

	return rc;
}


struct DevTypStruct
{
	int devrev;
	int ddrev;
	int bffver;
	string ext;// file Extension

	DevTypStruct(){clear();}
	void setVer(int newBffVer){ 
		if (newBffVer<0) { ext = ""; return; }
		bffver=newBffVer; 
		if (bffver==10){ext = "fma";}else 
		if (bffver==8)
		{
			ext = "fm8";
#ifndef TOKENIZER
			if (maxVer == 10)
				ext = "psy";	// FMAHACK
#endif
		}else 
		if (bffver==6){ext = "fm6";}else
		              {ext = "fms";}    };
		void clear(){ devrev = ddrev = bffver=-1; ext="";}
};
// DirTree2Scan is a directory tree of a Device-Type directory made with makeDirTree()
// the retStruct is what was found as best match
// targetDev is what we want to get close to or exactly match (depending on ExactMatch)
//
RETURNCODE scanDirTree(DevTypStruct& retStruct,  dirlist& DirTree2Scan, 
					   Indentity_t& targetDev,   bool     ExactMatch = false)
{
	retStruct.clear();
	fileInfo_t * pfoundFile    = NULL;
	fileInfo_t * pfoundDevice  = NULL;
	fileInfo_t * pfoundDDrev   = NULL;
	int ver = -1;
	

	// get the highest device revision first
	retStruct.devrev = DirTree2Scan.versionMatch(pfoundFile, targetDev.cDeviceRev, ExactMatch);

	if ( retStruct.devrev >= 0  && pfoundFile != NULL )// we have at usable Device Revision
	{// get the highest DD revision
		if ( ExactMatch )
			retStruct.ddrev = pfoundFile->versionMatch(pfoundDevice,
															targetDev.cSoftwareRev,ExactMatch);
		else
			retStruct.ddrev = pfoundFile->versionMatch(pfoundDevice,  INT_MAX, false);
	}// else no usable Device revision

	if ( retStruct.ddrev >= 0 && pfoundDevice != NULL ) // we have a devrev & dd revision
	{// get the highest encoded file format version
#ifdef TOKENIZER
	retStruct.setVer( pfoundDevice->versionMatch(pfoundDDrev,maxVer,ExactMatch));
#else
		retStruct.setVer( pfoundDevice->versionMatch(pfoundDDrev,maxVer,false));
#endif
	}// else something was not found
	return (retStruct.bffver < 0)?FAILURE:SUCCESS;
}


//RETURNCODE identifyDDfile(Indentity_t& deviceID, DWORD& deviceKey, StrVector_t** ppStrList)
// refactored and amended to handle 16bit ->> 8 bit device-type backoff
RETURNCODE identifyDDfile(Indentity_t& deviceID, DD_Key_t& deviceKey, StrVector_t** ppStrList)
{
	RETURNCODE r = SUCCESS;

	char localdir[MAX_FILE_NAME_PATH];
	char targetFileSpec[MAX_FILE_NAME_PATH];
	char localFileType[50];

	dirlist thisDirectory;
	DevTypStruct foundDeviceInfo;
	
	string fileName, fileBase, fileExt;
	int    foundVer = 0;
	bool   matchOnlyOne = (  ((deviceID.cInternalFlags & intFlgs_exactDDrevDesired) != 0) &&
						     (deviceID.cDeviceRev < 0xFF ) && (deviceID.cSoftwareRev < 0xFF) );
			
	Indentity_t defaultID;
	DD_Key_t    defaultKey, tmp64;

	entryCount++;

	while ( 1 )
	{
		// make initial path - just fill the path string and key
		if ( makePath(deviceID,localdir,MAX_FILE_NAME_PATH,deviceKey) <= 0)// failed
		{
			entryCount--;
			return FAILURE;// several reasons possible
		}
		//else success 
		thisDirectory.clear();
		
		// make initial filename
		sprintf(localFileType,"*.fm?");
		// get directory contents to tuple list	deviceID
		strcpy(targetFileSpec,localdir);
		strcat(targetFileSpec,localFileType);

		/*
			LINUX_PORT - REVIEW_WITH_HCF: Was there a memory leak  here before 
			switching to dDSearchOps? LastFileHandle obtained from FindFirstFile 
			was never closed.
		*/
		dDSearchOps LastFileHandle(localdir, localFileType);
		LastFileHandle.find();

		r = makeDirTree(thisDirectory, LastFileHandle);
		if ( r == SUCCESS ) // we have a directory and a tree for it
		{
			r = scanDirTree(foundDeviceInfo, thisDirectory, deviceID, matchOnlyOne);
		}
		// r = failure on a) no device type dir, b) device type but no usable file

		if ( (r == SUCCESS)                 || // found a usable file or
			 (deviceID.wManufacturer > 255)	|| // we can't back off   or
			 (deviceID.wDeviceType   < 256)	   // nothing to back off to
		   )
		{
			break;// out of while
		}
		else
		{                                                        // take the device-type
			deviceID.wDeviceType = deviceID.wDeviceType & 0x00FF;// from extended device-type
		}// and loop to try again
	}//wend


	// we still haven't found one and we are supposed to use default if available
	if ( r != SUCCESS && !(deviceID.cInternalFlags & intFlgs_Do_NOT_load_Dflt) )
	{			
		if ( !(deviceID.cInternalFlags & intFlgs_run_silent) )
		{
		DEBUGLOG(CLOG_LOG,"No dd found for %04x/%02x/%02x/anyDDrev\n",
				   deviceID.wManufacturer,deviceID.wDeviceType,deviceID.cDeviceRev);
        LOGIT(CERR_LOG|UI_LOG,"'%s' failed to find a usable file.\n",targetFileSpec);
		}

		r = SUCCESS;
		defaultID.cUniversalRev  = deviceID.cUniversalRev;
					 
		if (deviceID.cUniversalRev >= 7)
		{
			defaultID.cDeviceRev    = DEFAULT_DEV_REV_07;
		}
		else 
		if (deviceID.cUniversalRev == 6)
		{
			defaultID.cDeviceRev    = DEFAULT_DEV_REV_06;
		}
		else // hart 5 and any unknowns
		{
			defaultID.cDeviceRev    = DEFAULT_DEV_REV_05;
		}
		switch(deviceID.cInternalFlags & intFlgs_looking4defaults)// use the three bits
		{
		case 0x00:
		case 0x10: // intFlgs_lookExpandedDT ( treat like a 0 )
			{// look in hart/generic/extDevType::: intFlgs_lookExpandedDT   & 
			 //                                    intFlgs_look4HARTdflt 				
				defaultID.cInternalFlags = intFlgs_look4HARTdflt | intFlgs_lookExpandedDT;
				defaultID.wManufacturer  =   DEFAULT_HART_MFG;
				defaultID.wDeviceType    =   DEFAULT_HART_EDT;
			}
			break;
		case 0x04: // intFlgs_look4HARTdflt
			{// look  in rose/Rgeneric/extDevType::: intFlgs_lookExpandedDT & 
			 //                                      intFlgs_look4RoseMntDflt 				
				defaultID.cInternalFlags = intFlgs_lookExpandedDT | intFlgs_look4RoseMntDflt ; 
				defaultID.wManufacturer  =   DEFAULT_ROSE_MFG;
				defaultID.wDeviceType    =   DEFAULT_ROSE_EDT;
			}
			break;
		case 0x08: // intFlgs_look4RoseMntDflt
			{// we have no place else to look - error exit.
                LOGIT(CERR_LOG|UI_LOG,"'%s' failed to find a usable file.\n", targetFileSpec);
				r = APP_DD_NOT_FOUND;
				deviceKey = 0;
			}
			break;
		case 0x14: // intFlgs_lookExpandedDT   & intFlgs_look4HARTdflt
			{// look  in hart/generic/DevType::: intFlgs_look4HARTdflt
				
				defaultID.cInternalFlags = intFlgs_look4HARTdflt; 
				defaultID.wManufacturer  =   DEFAULT_HART_MFG;
				defaultID.wDeviceType    =    DEFAULT_HART_DT;
			}
			break;
		case 0x18: // intFlgs_lookExpandedDT   & intFlgs_look4RoseMntDflt
			{// look  in rose/Rgeneric/DevType::: intFlgs_look4RoseMntDflt 
				
				defaultID.cInternalFlags = intFlgs_look4RoseMntDflt ; 
				defaultID.wManufacturer  =   DEFAULT_ROSE_MFG;
				defaultID.wDeviceType    =    DEFAULT_ROSE_DT;
			}
			break;
		case 0x0c: //intFlgs_look4HARTdflt & intFlgs_look4RoseMntDflt (can't look in 2 places)
		case 0x1c: //intFlgs_lookExpandedDT& intFlgs_look4HARTdflt  & intFlgs_look4RoseMntDflt
		default:
			{// error exit
                LOGIT(CERR_LOG|UI_LOG,"'%s' failed to find a usable file.\n", targetFileSpec);
				r = APP_DD_NOT_FOUND;
				deviceKey = 0;
			}
			break;
		}//end-switch

		if ( deviceID.cInternalFlags & intFlgs_run_silent )// bitwise or!
		{ // set, pass it down
			defaultID.cInternalFlags |= intFlgs_run_silent;
		}
			
		if ( r == SUCCESS )// we haven't been signaled as done yet
		{// re-enter to do a new search
			r = identifyDDfile(defaultID,defaultKey,ppStrList);
			if ( r == SUCCESS )
			{// have the key reflect the real DD
				deviceKey = defaultKey;//   we found the RM or the HART version
				deviceID.cInternalFlags |= intFlgs_foundDflt; // flag a generic was found
			}
			else
			{// show the error, leave r failure
				deviceKey = 0l;
			}
		}// else it's a failure in the switch, just continue
	}// else found one (or not supposed to fall back to generic) so keep going

// NOTES:  at r  = SUCCESS -  found a DD,
//         intFlgs_looking4defaults - means the global file name & strings have been set
	if ( r == SUCCESS &&  (deviceID.cInternalFlags & intFlgs_foundDflt) == 0x00 )
	{// build the info to return  implies (deviceID.cInternalFlags & 0x0c) == 0x00
		sprintf(targetFileSpec,"%s%02x%02x.%s",
			localdir,foundDeviceInfo.devrev,foundDeviceInfo.ddrev,foundDeviceInfo.ext.c_str());
		strcpy(&(chInputFileName[0]),targetFileSpec);

		deviceKey  = (deviceKey     & 0xffffffff00000000ULL); // L&T Modifications : PortToAM335
		tmp64      = foundDeviceInfo.devrev;
		deviceKey |= (tmp64 << 16) & 0x0000000000ff0000;
		tmp64      = foundDeviceInfo.ddrev;
		deviceKey |= (tmp64 <<  0) & 0x00000000000000ff;

		if (ppStrList != NULL)
		{
			*ppStrList =  new StrVector_t;
			identLookup(deviceKey, deviceID, *ppStrList);
		}
	}

//	if ((deviceID.cInternalFlags & intFlgs_looking4defaults) == 0x00  &&
//		(deviceID.cInternalFlags & intFlgs_Do_NOT_load_Dflt) == 0x00) //we are at the top level
//	{	// not looking (at the top level) and we are supposed to load one if we find it
	if ( (entryCount == 1 ) && (deviceID.cInternalFlags & intFlgs_looking4defaults) == 0x00 )
	{	
		if ( r != SUCCESS ) // nothing could be found (requested nor generic)
		{
			if ( !(deviceID.cInternalFlags & intFlgs_run_silent) )
			{
				LOGIT(CERR_LOG|UI_LOG|USRACK_LOG,
					"*** No usable file found ***    \n*** in %s ***\n",targetFileSpec);
				sprintf(targetFileSpec,"%s%02x??.fm?",localdir,foundDeviceInfo.devrev);
                LOGIT(CLOG_LOG|UI_LOG,"Trying to locate %s\n",targetFileSpec);
			}
#ifdef GE_BUILD
			if ( ((deviceID.cInternalFlags & intFlgs_Do_NOT_load_Dflt) == 0 ) )
			{
				generic_device_found = DD_NOT_FOUND;	//01.01.01
			}
#endif
			r = APP_DD_NOT_FOUND;
			deviceKey = 0;		
		}
		else // r == success
		if (  (deviceID.cInternalFlags & intFlgs_foundDflt) == intFlgs_foundDflt)
		{
			if ( !(deviceID.cInternalFlags & intFlgs_run_silent) )
			{
			LOGIT(UI_LOG|CLOG_LOG|USRACK_LOG,"Requested DD had an error or was not found.\n"
					                       "A generic DD will be loaded.\n%s",chInputFileName);
			}
#ifdef GE_BUILD
			generic_device_found = DD_NOT_FOUND;	//01.01.00
#endif
		}
		else 
		{// found correct dd
			FILE* fp = (FILE*)fopen(chInputFileName , "rb");

			if(!fp)
			{
			LOGIT(CERR_LOG|UI_LOG,"- Could not open requested DD file '%s'\n",chInputFileName);
				r = APP_DD_NOT_FOUND;
				deviceKey = 0;
			}
			else
			if (foundDeviceInfo.bffver == 5)// stevev 15oct10 - 
				              //      add special section for version 5 backward compatability
			{
				eFFVersionMajor = 5;
				eFFVersionMinor = 0;//just guess

				VerificationLevel = 0;// this version won't verify
			}
			else
			{
				DDOD_HEADER header;
				if (! DDlDevDescription::ReadHeader(header, fp) )// a static function
				{// error
					fclose(fp);
					entryCount--;
					return FAILURE;
				}
				fclose(fp);

				eFFVersionMajor = header.tok_rev_major;
				eFFVersionMinor = header.tok_rev_minor;

				int mfg = header.byManufacturer[0]<<16 | 
					      header.byManufacturer[1]<< 8 | 
						  header.byManufacturer[2];
				header.device_type = ((header.device_type & 0x00ff) << 8 ) |
					                 ((header.device_type & 0xff00) >> 8 ) ;

				if ( (eFFVersionMajor != foundDeviceInfo.bffver)   ||
					 ( mfg != deviceID.wManufacturer)              ||
					 ( header.device_type != deviceID.wDeviceType) ||
					 ( header.device_revision != foundDeviceInfo.devrev)           ||
					 ( header.dd_revision != foundDeviceInfo.ddrev)                )
				{// header info does not match filename
					// error message - 
					LOGIT(CERR_LOG|UI_LOG|CLOG_LOG,
						  "Error: DD File header information does not match file name.\n");
					LOGIT(
						CERR_LOG,
						"eFFVersionMajor : %d; foundDeviceInfo.bffver : %d\n",
						eFFVersionMajor,
						foundDeviceInfo.bffver
					);
					LOGIT(
						CERR_LOG,
						"mfg : %d; deviceID.wManufacturer : %d\n",
						mfg,
						deviceID.wManufacturer
					);
					LOGIT(
						CERR_LOG,
						"header.device_type : %d; deviceID.wDeviceType : %d\n",
						header.device_type,
						deviceID.wDeviceType
					);
					LOGIT(
						CERR_LOG,
						"header.device_revision : %d; foundDeviceInfo.devrev : %d\n",
						header.device_revision,
						foundDeviceInfo.devrev
					);
					LOGIT(
						CERR_LOG,
						"header.dd_revision : %d; foundDeviceInfo.ddrev : %d\n",
						header.dd_revision,
						foundDeviceInfo.ddrev
					);
					entryCount--;
					return FAILURE;
				}

				if (eFFVersionMajor < 8)
				{
					VerificationLevel = 0;
					if ( eFFVersionMajor == 6 )
					{
						fm6 = true;
					}
				}
				else // >= eff version 8	
				if ( ! verifyfile(chInputFileName, 0) ) // if not production
				{
					if (  ! verifyfile(chInputFileName, 1) ) // if not debug
					{// mark file as unverified (tampered with)
						VerificationLevel = 0;
						// tampered file error message
					}
					else
					{// mark file as verified debug
						VerificationLevel = 2;
					}
				}
				else
				{// mark file as verified production
					VerificationLevel = 1;
				}
			}
		}//end if found correct dd
	}// endif top level
	entryCount--;
	return r;
}
/****************************************************************************************/
/* F */


/************************************************************************************************/

RETURNCODE setDDFromKey(DWORD& newKey)
{

	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

RETURNCODE releaseString(wstring** ppStringWasFilled)
{

	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
RETURNCODE buildDicitonary(CDictionary* dictionary)
{
	char tmpchDbdir[MAX_FILE_NAME_PATH];
	char dictfile[MAX_FILE_NAME_PATH];
	char extraDict[DICTCNT][MAX_FILE_NAME_PATH];
	// changed technique 12feb07...,draegerfile[255],mmifile[255]/*temporary:*/,EnHfile[255];

//    WIN32_FIND_DATA FindFileData;
//    HANDLE DirSearch;
    string DirName;

	if (chDbdir != NULL && chDbdir[0] != '\0')
	{
#if 0
		if (!strstr(chDbdir,"Release"))
		{
			if(!strstr(chDbdir,"release"))
			{
				strcpy(tmpchDbdir,chDbdir);
				strcat(tmpchDbdir,"\\*");

				USES_CONVERSION;
				CComBSTR combstr(tmpchDbdir);
				LPTSTR szTemp = OLE2T(combstr.m_str);
				DirSearch=::FindFirstFile(szTemp, &FindFileData );
 
				if(DirSearch  != INVALID_HANDLE_VALUE)
					do
					{	 
						 string strTemp = OLE2A(FindFileData.cFileName);

						 if((FILE_ATTRIBUTE_DIRECTORY & FindFileData.dwFileAttributes) &&
							 (!strcmp(strTemp.c_str(),"Release")) || (!strcmp(strTemp.c_str(),"release")))
						 {
							DirName = strTemp.c_str();
							break;
						 }
					}
					while(::FindNextFile(DirSearch, &FindFileData));
		 	    ::FindClose(DirSearch);

				if(DirName.size())
				{
					strcpy(tmpchDbdir,chDbdir);
					strcat(tmpchDbdir,"\\");
					strcat(tmpchDbdir,DirName.c_str());
				}
				else
					return FAILURE;
			}
			else
			{	strcpy(tmpchDbdir,chDbdir);}
		}
		else
#endif
		{	strcpy(tmpchDbdir,chDbdir);}
	}
	else
	{
		string fName = getCurrentDirectory();
		strcpy(tmpchDbdir,fName.c_str());
		strcat(tmpchDbdir, OS_PATH_SEPR);
		strcat(tmpchDbdir,"Release");
	}
	if (tmpchDbdir[strlen(tmpchDbdir)-1] != OS_PATH_SEPR[0])
		strcat(tmpchDbdir,OS_PATH_SEPR);
	// changed technique 12feb07...strcpy(dictfile,tmpchDbdir);
	// changed technique 12feb07...strcat(dictfile,"standard.dct");
	// changed technique 12feb07...strcpy(draegerfile,tmpchDbdir);
	// changed technique 12feb07...strcat(draegerfile,"draeger.dct");
	// changed technique 12feb07...strcpy(mmifile,tmpchDbdir);
	// changed technique 12feb07...strcat(mmifile,"mmi.dct");
	// changed technique 12feb07.../*temporary:*/
	// changed technique 12feb07...strcpy(EnHfile,tmpchDbdir);
	// changed technique 12feb07...strcat(EnHfile,"endress_hauser.dct");

	// changed technique 12feb07...char* chDictionaryExtensions [] 
	// changed technique 12feb07...	= {
	// changed technique 12feb07...		draegerfile,
	// changed technique 12feb07...		mmifile,
	// changed technique 12feb07...		EnHfile,
	// changed technique 12feb07...			NULL
	// changed technique 12feb07...	}; 
//static const char hartDicts[DICTCNT][DICTNAMELEN] = { DICTLIST };
	//
	//char extraDict[DICTCNT][MAX_FILE_NAME_PATH];

	char* chDictionaryExtensions [DICTCNT+1];
	for (int k = 0; k < DICTCNT; k++)
	{
		if ( k == 0 )// first
		{
			strcpy(dictfile,tmpchDbdir);
			strcat(dictfile,hartDicts[k]);
		}
		else
		{
			strcpy(extraDict[k-1],tmpchDbdir);
			strcat(extraDict[k-1],hartDicts[k]);
			chDictionaryExtensions[k-1] = &(extraDict[k-1][0]);
		}
		if ( (k+1) == DICTCNT )// last
		{
			chDictionaryExtensions[k] = NULL;
		}
	}
	
	return(dictionary->makedict(dictfile,chDictionaryExtensions));
}

RETURNCODE getDDfileInfo(PFILE_INFO_T pFileInfo)
{
    memcpy(pFileInfo, &readFileTime, sizeof(FILE_INFO_T));
    tstring locStr;
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
    locStr = AStr2TStr( string(chInputFileName) );
#else
    locStr = chInputFileName;
#endif
// pre 10jan08::>	pFileInfo->file_name = chInputFileName;
   // pFileInfo->file_name = locStr;
	return SUCCESS;
}

/********************************************************************************************/
/* F */


/********************************************************************************************/

DDlBaseItem *pBaseItem; // stevev 01aug07 made global for debugging the catch
// make long-long key RETURNCODE getDevice  (UINT32 wrkingKey, aCdevice* pAbstractDev,CDictionary* dictionary) // recurse down the entire hierarchy
RETURNCODE getDevice  (DD_Key_t wrkingKey, aCdevice* pAbstractDev,
		CDictionary* dictionary, LitStringTable* litStrings, bool isInTokenizer) // recurse down the entire hierarchy
{
	if ( dictionary != NULL )
	{
		pPIdict = dictionary;
	}
	else
	{
		pPIdict = NULL;
		return APP_DICTIONARY_MISSING;
	}
	// stevev 13mar08 - literal strings have to stay around to allow method string lookup
	// they now belong to the device and we are to only fill them in in this routine.
	// if they weren't passed in, this HAS TO BE a pre fm8 file load
		
#ifdef SAFETYNET
	try{
#endif

		bool bRetVal;
		devDesc = new DDlDevDescription(chDbdir);
        string fileName(chInputFileName);
        bRetVal = devDesc->Initialize(fileName,"|en|",dictionary, litStrings);
/*Vibhor 291004: Start of Code*/
/* Input file has a defualt extension of fm6 now, so if the initialize failed because there
   is no fm6 available, we'll try for a fms*/
	/* stevev - for better error reporting */
		char erInputFileName[MAX_FILE_NAME_PATH];

		if(false == bRetVal )// can't be null... && NULL != chInputFileName)
		{
			char tmpFileName [MAX_FILE_NAME_PATH];
			unsigned filelen = strlen(chInputFileName) - 4;
			strncpy(erInputFileName,chInputFileName,filelen+5);/* stevev-for better reporting */
			strncpy(tmpFileName,    chInputFileName,filelen);
			tmpFileName[filelen] = '\0';
			if(tmpFileName)
			{
				strcat(tmpFileName,".fms");
				tmpFileName[filelen + 4] = '\0';
				strcpy(chInputFileName,tmpFileName);
				chInputFileName[filelen + 4] = '\0';
                bRetVal = devDesc->Initialize(fileName,"|en|",dictionary);
			}
		}
/*Vibhor 291004: End of Code*/ 
		if(!bRetVal)
		{
			// 22aug08  OUTPUT(CERR_LOG|UI_LOG,
			LOGIT(CERR_LOG|UI_LOG,
				"ERROR: DD Initialize failed for '%s'\n  and '%s'",chInputFileName,erInputFileName);
		/* VMKP added on 030404*/
			delete devDesc;
		/* VMKP added on 030404*/			
			pPIdict = NULL;
			return FAILURE;
		}
		else
		{	
			if ( ! isInTokenizer )
            LOGIT(COUT_LOG|UI_LOG,"Initialized using '%s'\n",chInputFileName);
		}

		int iDevItemListSize = devDesc->ItemsList.size();

		bRetVal = devDesc->LoadDeviceDescription(isInTokenizer);

		iDevItemListSize = devDesc->ItemsList.size();

		if(!bRetVal)
		{
			// 22aug08  OUTPUT(CERR_LOG|UI_LOG,"ERROR: DD Load failed for '%s'",chInputFileName);
			LOGIT(CERR_LOG|UI_LOG,"ERROR: DD Load failed for '%s'\n",chInputFileName);
		/* VMKP added on 030404*/
			delete devDesc;
		/* VMKP added on 030404*/						
			pPIdict = NULL;
			return FAILURE;
		}

		// stevev 5/11/04 - added for support of file sniffing
		(void) getFileLastWriteTime(chInputFileName, &readFileTime);

#ifdef TURNED_OFF_DEBUG
		WORD Dt,Tm;
		int  Dy,Mt,Yr,Hr,Mn,Sc;
		// takes a ptr2FILETIME
#define FT2Date(PFT) {FileTimeToDosDateTime((PFT),&Dt,&Tm);Dy=(Dt&0x1f);Mt=((Dt>>5)&0x0f);\
		Yr=(((Dt>>9)&0x7f)+1980);Sc=((Tm&0x1f)*2);Mn=((Tm>>5)&0x3f);Hr=((Tm>>11)&0x1f);}
		
		clog << "File:\n    " << chInputFileName << endl;
		FT2Date(&(FindFileData.ftLastAccessTime));
		clog << "     Access: "<<Mt<<"/"<<Dy<<"/"<<Yr<<"   "<<Hr<<":"<<Mn<<":"<<Sc<<endl;
		FT2Date(&(FindFileData.ftCreationTime));
		clog << "    Created: "<<Mt<<"/"<<Dy<<"/"<<Yr<<"   "<<Hr<<":"<<Mn<<":"<<Sc<<endl;
		FT2Date(&(FindFileData.ftLastWriteTime));
		clog << "    Written: "<<Mt<<"/"<<Dy<<"/"<<Yr<<"   "<<Hr<<":"<<Mn<<":"<<Sc<<endl;
#endif
		// end stevev 

		vector <DDlBaseItem *>:: iterator p;
//made global for debugging the catch...		DDlBaseItem *pBaseItem;
		
		/* Got the complete DD file data, Populate On to aCclasses */
		pAbstractDev->DDkey = wrkingKey;

		for(p = devDesc->ItemsList.begin(); p != devDesc->ItemsList.end();p++)
		{
			pBaseItem = *p;
			
			aCitemBase *tmpaCItemBase;
			tmpaCItemBase = new aCitemBase;

			tmpaCItemBase->itemId = pBaseItem->id;
			tmpaCItemBase->itemName = pBaseItem->strItemName;
			tmpaCItemBase->itemType = (unsigned long) pBaseItem->byItemType;
			tmpaCItemBase->itemSubType = (unsigned long) pBaseItem->byItemSubType;
			tmpaCItemBase->attrMask = pBaseItem->ulItemMasks;
			
			DDlAttribute *pAttr = NULL;
#ifdef _DEBUG
if (tmpaCItemBase->itemId == 0x4075)//0x4117)
{
// test register empty string	
	LOGIT(CLOG_LOG,"Got required Item{dbg only msg}\n");
}
#endif
			switch(pBaseItem->byItemType)
			{
				case	VARIABLE_ITYPE:
					 fill_var_item_attributes(tmpaCItemBase,((DDlVariable*)pBaseItem)->attrList);
					break;
				case	COMMAND_ITYPE:
					 fill_command_attributes(tmpaCItemBase,((DDlCommand*)pBaseItem)->attrList);
					break;
				case	MENU_ITYPE: 
					 fill_menu_attributes(tmpaCItemBase,((DDlMenu*)pBaseItem)->attrList);
					break;
				case	EDIT_DISP_ITYPE:  
					fill_edit_display_attributes(tmpaCItemBase,((DDlEditDisplay*)pBaseItem)->attrList);
					break;
				case	METHOD_ITYPE:     
					fill_method_attributes(tmpaCItemBase,((DDlMethod*)pBaseItem)->attrList);
					break;
				case	REFRESH_ITYPE:    
					fill_refresh_attributes(tmpaCItemBase,((DDlRefresh*)pBaseItem)->attrList);
					break;
				case	UNIT_ITYPE:       
					fill_unit_relation(tmpaCItemBase,((DDlUnit*)pBaseItem)->attrList);
					break;
				case	WAO_ITYPE: 	
					fill_wao_relation(tmpaCItemBase,((DDlWao*)pBaseItem)->attrList);
					break;
				case	ITEM_ARRAY_ITYPE: 
					fill_item_array_attributes(tmpaCItemBase,((DDlItemArray*)pBaseItem)->attrList);
					break;
				case	COLLECTION_ITYPE:
					fill_collection_attributes(tmpaCItemBase,((DDlCollection*)pBaseItem)->attrList);
					break;
				case	BLOCK_ITYPE: 
				case	RECORD_ITYPE: 
				case	PROGRAM_ITYPE:
				case	VAR_LIST_ITYPE  :
				case	RESP_CODES_ITYPE:
				case	DOMAIN_ITYPE    :
				case	MEMBER_ITYPE    :
					LOGIF(LOGP_MISC_CMTS) (CLOG_LOG,"Unknown item type coming out of JIT parser.\n");
					tmpaCItemBase->attrMask = 0; //we aren't doing attributes, no reason to show a mask
					break;// unused
				case	ARRAY_ITYPE     :
					fill_array_attributes(tmpaCItemBase,((DDl6Array*)pBaseItem)->attrList);
                    break;
				case	LIST_ITYPE		:
					fill_list_attributes(tmpaCItemBase,((DDl6List*)pBaseItem)->attrList);
                    break;
				case	IMAGE_ITYPE	:
					fill_image_attributes(tmpaCItemBase,((DDl6Image*)pBaseItem)->attrList);
                    break;
                /* CPMHACK: The below artifacts are not yet supported in CPM,
                 * temporarily its commenting out. This needs to be uncommented
                 * and memory leaks should be taken care once the below artifacts
                 * are implemented in CPM application.
                 */
                /**************************************************************
                case	GRID_ITYPE		:
                    fill_grid_attributes(tmpaCItemBase,((DDl6Grid*)pBaseItem)->attrList);
                    break;
                case	FILE_ITYPE		:
                    fill_file_attributes(tmpaCItemBase,((DDl6File*)pBaseItem)->attrList);
                    break;
                case	CHART_ITYPE		:
                    fill_chart_attributes(tmpaCItemBase,((DDl6Chart*)pBaseItem)->attrList);
                    break;
                case	GRAPH_ITYPE		:
                    fill_graph_attributes(tmpaCItemBase,((DDl6Graph*)pBaseItem)->attrList);
                    break;
                case	AXIS_ITYPE		:
                    fill_axis_attributes(tmpaCItemBase,((DDl6Axis*)pBaseItem)->attrList);
                    break;
                case	WAVEFORM_ITYPE	:
                    fill_waveform_attributes(tmpaCItemBase,((DDl6Waveform*)pBaseItem)->attrList);
                    break;
                case	SOURCE_ITYPE	:
                    fill_source_attributes(tmpaCItemBase,((DDl6Source*)pBaseItem)->attrList);
                    break;
				case	BLOB_ITYPE	:
					fill_blob_attributes(tmpaCItemBase,((DDl6Blob*)pBaseItem)->attrList);
                    break;
                **************************************************************/
				default:
					break;
			
			}/*End switch*/
#ifdef _STRMEMTESTING
assert(areSTRINGSgood());
#endif

			pAbstractDev->AitemPtrList.push_back(tmpaCItemBase);
		}

		vector <unsigned long>:: iterator q;

		for(q = devDesc->CriticalParamList.begin(); q != devDesc->CriticalParamList.end();q++)
		{
			pAbstractDev->CritItemList.push_back(*q);
		}

		/* stevev 07Jan05 - add image transport */
		// NOTE: the memory passed WILL BE RETURNED
		//       the host is expected to read the data like a file, use the info and 
		//		 NOT delete the memory pointed to.
		BIimageList_it iK;
		BIframeList_it iF;

		imgframe_t   ifi;
		AframeList_t ifL;

		for(iK = devDesc->ImagesList.begin(); iK != devDesc->ImagesList.end();iK++)
		{
			for ( iF = iK->begin(); iF < iK->end(); ++iF)
			{// iF isa ptr2a IMAGEFRAME_S
				//memcpy(&ifi,&(IMAGEFRAME_S *)iF, sizeof(imgframe_t));
				ifi.size		= iF->ifs_size;
				ifi.pRawFrame	= iF->ifs_pRawFrame;
				ifi.offset		= iF->ifs_offset;
				memcpy( &(ifi.language[0]), &(iF->ifs_language[0]),6); 

				ifL.push_back(ifi);
			}
			pAbstractDev->AimageList.push_back(ifL);
			ifL.clear();
		}

		/* stevev end image transport addition */

#ifdef _DEBUG /* debug versions can get command line control   _DUMP_ENABLE*/
//Vibhor 1308904: //Enabling both the dumps simultaneously:
		if LOGTHIS(LOGP_RAW_DUMP)
		{
			string bfile = "JITOutput.txt";
			outRedir.open(bfile.c_str());
			if ( outRedir.is_open() )
			{

				pHldOutBuf = cout.rdbuf();
				cout.rdbuf(outRedir.rdbuf());
			}
			else
			{
				LOGIT(CERR_LOG, "ERROR: output redirection failed.\n");
			}
 #ifdef _STRMEMTESTING
    assert(areSTRINGSgood());
 #endif
			dump_items(devDesc);
			cout.rdbuf(pHldOutBuf);
			outRedir.close();
 #ifdef _STRMEMTESTING
    assert(areSTRINGSgood());
 #endif


	
#ifdef _DBGMIL
	if (pGlblMIL)
	{
		LOGIT(COUT_LOG,"------ post dump_items() -----");
		pGlblMIL->dumpList();
	}
#endif
			/* Dump the Data base dd file */
			string afile="aCOutput.txt";
	//		string afile="output.txt";
			outRedir.open(afile.c_str() );
			if ( outRedir.is_open() )
			{
				pHldOutBuf = cout.rdbuf();
				cout.rdbuf(outRedir.rdbuf());
			}
			else
			{
				cerr << "ERROR: output redirection failed." << endl;
			}

			dumpDev(pAbstractDev);
	//		dump_items(devDesc);
			cout.rdbuf(pHldOutBuf);
			outRedir.close();	
#ifdef _DBGMIL
	if (pGlblMIL)
	{
		LOGIT(COUT_LOG,"------ post Abstract Dev dump -----");
		pGlblMIL->dumpList();
		pGlblMIL = NULL;

	}
#endif
		}// else - don't log it
 #ifdef _STRMEMTESTING
    assert(areSTRINGSgood());
 #endif

#endif //ifdef DEBUG

	delete devDesc;

#ifdef SAFETYNET
}
catch(...)
{
	LOGIT(CERR_LOG,"ParserInfc::getDevice() CRASH   into a  catch(...)\n");
}
#endif //SAFETYNET			
	pPIdict = NULL;
	return SUCCESS;
}

/****************************************************************************************/
/* F */


/********************************************************************************************/

void fill_min_max_list(aCminmaxList *minmaxLst,MIN_MAX_LIST *minMaxList)
{
	MIN_MAX_LIST :: iterator p;
	MIN_MAX_VALUE *pminMax;
	int i = 0;

	for(p = minMaxList->begin();p != minMaxList->end(); ++p)
	{// ptr2a MIN_MAX_VALUE
		aCminmaxVal minmaxVal;

		pminMax = (MIN_MAX_VALUE *)&(*p);// PAW 03/03/09
		minmaxVal.whichVal = pminMax->which;
//always		if (pminMax->isMinMaxConditional)
		if (pminMax->pCond != NULL && pminMax->pCond->listOfChilds.size() > 0)
		{//condExpr	
//			fill_conditional_attributes(&(minmaxVal.condExpr),pminMax->pCond);
			fill_conditional_attributes(&(minmaxVal.condExpr),pminMax->pCond->listOfChilds.at(0));
		}
		else // direct
		{		
			minmaxVal.condExpr.priExprType = eT_Direct;

			aCgenericConditional::aCexpressDest tempaCDest;
			tempaCDest.destType = eT_Direct;
			//fill_Payload(&tempaCDest, pAttr);
			tempaCDest.pPayload =  new aCExpressionPayload();
			fill_expression(&(((aCExpressionPayload	*)tempaCDest.pPayload)->theExpression),
							pminMax->pCond->Vals.at(0).pExpr);
			// end fillPayload simulator
			minmaxVal.condExpr.destElements.clear();
			minmaxVal.condExpr.destElements.push_back(tempaCDest);
		}

		//fill_expression(&minmaxVal.pExpr,&minMax.value);
		minmaxLst->push_back(minmaxVal);
	}/*End for p*/
}
/****************************************************************************************/
/* F */

/************************************************************************************************/
aCattrBase* fill_bitstring_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrBitstring* prPtr =  new aCattrBitstring;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condBitStr.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condBitStr.destElements.clear();
		prPtr->condBitStr.destElements.push_back(tempaCDest);
	}
	else
	if(pAttr->bIsAttributeConditional     == TRUE && 
		   pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condBitStr), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:bitstring flagged as Error or List.\n");
	}

	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_string_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrString* prPtr =  new aCattrString;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condString.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condString.destElements.clear();
		prPtr->condString.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condString), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:string flagged as Error or List.\n");
	}
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_expression_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrCondExpr* prPtr =  new aCattrCondExpr;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condExpr.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condExpr.destElements.clear();
		prPtr->condExpr.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condExpr), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:expression flagged as Error or List.\n");
	}
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}
/************************************************************************************************/


/************************************************************************************************/
aCattrBase* fill_ulong_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrCondLong* prPtr =  new aCattrCondLong;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condLong.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condLong.destElements.clear();
		prPtr->condLong.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condLong), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:long flagged as Error or List.\n");
	}
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_reference_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrCondReference* prPtr =  new aCattrCondReference;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condRef.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condRef.destElements.clear();
		prPtr->condRef.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condRef), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:reference flagged as Error or List.\n");
	}
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_actions_attribute(DDlAttribute* pAttr, ulong attrID)
{	
	aCattrActionList* paAttr =  new aCattrActionList;

	REFERENCE_LIST *refList;

	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		refList = pAttr->pVals->refList;

		paAttr->ActionList.genericlist.clear();

		aCgenericConditional tmpaCgenCond;
		tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
		tmpaCgenCond.destElements.clear();

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		tempaCDest.pPayload = new aCreferenceList;

		fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),refList);
		tmpaCgenCond.destElements.push_back(tempaCDest);

		paAttr->ActionList.genericlist.push_back(tmpaCgenCond);

	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE)
	{// this is actually an error???
		fill_conditional_attributes_list(&(paAttr->ActionList), pAttr->pCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == FALSE && 
		pAttr->bIsAttributeConditionalList == TRUE  )
	{
		fill_conditional_chunks(&(paAttr->ActionList), pAttr);
	}
	else // TRUE TRUE 
	{
		LOGIT(CERR_LOG,"ERROR:actions flagged as both cond and condList.\n");
	}
	// else both true - error condition if reached
	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_reflist_attribute(DDlAttribute* pAttr, ulong attrID)
{	
	aCattrReferenceList* paAttr =  new aCattrReferenceList;

	REFERENCE_LIST *refList;

	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		refList = pAttr->pVals->refList;

		paAttr->RefList.genericlist.clear();

		aCgenericConditional tmpaCgenCond;
		tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
		tmpaCgenCond.destElements.clear();

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		tempaCDest.pPayload = new aCreferenceList;

		fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),refList);
		tmpaCgenCond.destElements.push_back(tempaCDest);

		paAttr->RefList.genericlist.push_back(tmpaCgenCond);

	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE)
	{// this is actually an error???
		fill_conditional_attributes_list(&(paAttr->RefList), pAttr->pCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == FALSE && 
		pAttr->bIsAttributeConditionalList == TRUE  )
	{
		fill_conditional_chunks(&(paAttr->RefList), pAttr);
	}
	else // TRUE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:reference list flagged as not-List and List.\n");
	}
	// else both true - error condition if reached
	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_minmaxlist_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrVarMinMaxList* prPtr =  new aCattrVarMinMaxList;

	/* stevev 24aug06 - this is not a conditional attribute
	   it is a non-conditional list of conditional values  */
	fill_min_max_list (&(prPtr->ListOminMaxElements),pAttr->pVals->minMaxList);
	
	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);


#if 0 /*********************************************************************/
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condMinMaxVal.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condMinMaxVal.destElements.clear();
		prPtr->condMinMaxVal.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condMinMaxVal), pAttr->pCond);
	}
	/* stevev 23aug06 */
	else
	if( pAttr->bIsAttributeConditional     == FALSE && 
		pAttr->bIsAttributeConditionalList == TRUE)
	{
		fill_conditional_chunks(&(prPtr->condMinMaxVal), pAttr);
	}
	else // TRUE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:min-maxList flagged as both not-List and List.\n");
	}// end stevev 23aug06 
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
#endif /**********************************************************************/
}
/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_memberlist_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrMemberList* paAttr =  new aCattrMemberList;

	ITEM_ARRAY_ELEMENT_LIST *itemArray;
	//COLLECTION_MEMBER_LIST

	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE)
	{
		itemArray = pAttr->pVals->itemArrElmnts;// same as memberList

		paAttr->condMemberElemListOlists.genericlist.clear();

		aCgenericConditional tmpaCgenCond;
		tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
		tmpaCgenCond.destElements.clear();

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		tempaCDest.pPayload = new aCmemberElementList;
		fill_member_element_list(((aCmemberElementList *)tempaCDest.pPayload),itemArray);

		tmpaCgenCond.destElements.push_back(tempaCDest);
		paAttr->condMemberElemListOlists.genericlist.push_back(tmpaCgenCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE)
	{// this should be an error??
		fill_conditional_attributes_list(&(paAttr->condMemberElemListOlists),pAttr->pCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == FALSE && 
		pAttr->bIsAttributeConditionalList == TRUE)
	{
		fill_conditional_chunks(&(paAttr->condMemberElemListOlists), pAttr);
	}
	else // TRUE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:memberlist flagged as both not-List and List.\n");
	}

	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}

// stevev 12may05

/************************************************************************************************/
aCattrBase* fill_gridmemberlist_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrGridMemberList* paAttr =  new aCattrGridMemberList;

	GRID_SET_LIST *pGrdMembers;

	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE)
	{
		pGrdMembers = pAttr->pVals->gridMemList;

		paAttr->condGridMemberListOlists.genericlist.clear();

		aCgenericConditional tmpaCgenCond;
		tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
		tmpaCgenCond.destElements.clear();

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		tempaCDest.pPayload = new aCgridMemberList;
		fill_grid_member_list(((aCgridMemberList *)tempaCDest.pPayload),pGrdMembers);

		tmpaCgenCond.destElements.push_back(tempaCDest);
		paAttr->condGridMemberListOlists.genericlist.push_back(tmpaCgenCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE)
	{// this should be an error??
		fill_conditional_attributes_list(&(paAttr->condGridMemberListOlists),pAttr->pCond);
	}
	else
	if( pAttr->bIsAttributeConditional     == FALSE && 
		pAttr->bIsAttributeConditionalList == TRUE)
	{
		fill_conditional_chunks(&(paAttr->condGridMemberListOlists), pAttr);
	}
	else // TRUE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:grid member list flagged as both cond and condList.\n");
	}

	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}



/*Vibhor 041004: Start of Code*/

/************************************************************************************************/



/************************************************************************************************/
aCattrBase* fill_linetype_attribute(DDlAttribute* pAttr, ulong attrID)
{
	aCattrCondIntWhich* prPtr =  new aCattrCondIntWhich;

	
	if( pAttr->bIsAttributeConditional     == FALSE &&
		pAttr->bIsAttributeConditionalList == FALSE  )
	{
		prPtr->condIntWhich.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		fill_Payload(&tempaCDest, pAttr);

		prPtr->condIntWhich.destElements.clear();
		prPtr->condIntWhich.destElements.push_back(tempaCDest);
	}
	else
	if( pAttr->bIsAttributeConditional     == TRUE && 
		pAttr->bIsAttributeConditionalList == FALSE )
	{
		fill_conditional_attributes(&(prPtr->condIntWhich), pAttr->pCond);
	}
	else // TRUE TRUE - or list FALSE TRUE
	{
		LOGIT(CERR_LOG,"ERROR:linetype flagged as Error or List.\n");
	}
	// else 
	//  error ( cond list (or both))	

	prPtr->attr_mask = attrID;

	return ((aCattrBase*)prPtr);
}

/*Vibhor 041004: End of Code*/
/************************************************************************************************/
 // stevev 11may05 
 
aCattrBase* fill_debug_attribute(DDlAttribute* pAttr, ulong attrID)
{	
	aCdebugInfo*   paAttr =  new aCdebugInfo;

	if (pAttr->pVals->debugInfo)
	{
		paAttr->symbolName = pAttr->pVals->debugInfo->symbol_name;
	}
	copy_ddlstring(&(paAttr->fileName), &(pAttr->pVals->debugInfo->file_name) );
	paAttr->lineNumber = pAttr->pVals->debugInfo->lineNo;
	paAttr->use_flags  = pAttr->pVals->debugInfo->flags;// not needed, carry temporarily(for tokenizer)

	if ( pAttr->pVals->debugInfo->attr_list.size() > 0 )
	{
	/* ********* TODO **********************	
	AdebugAttrList_t   attrs;
	****************************************/
	}

	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}

void fill_oneParam(aCparameter& localParam, METHOD_PARAM* pmp)
{
	localParam.type      = pmp->param_type;
	localParam.modifiers = pmp->param_modifiers;
	if (pmp->param_name != NULL)
		localParam.paramName = pmp->param_name;
}
 
aCattrBase* fill_methodType_attribute(DDlAttribute* pAttr, ulong attrID)
{		
	aCparameter*   paAttr =  new aCparameter;
	paAttr->clear();

	fill_oneParam(*paAttr, pAttr->pVals->methodType);

	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}

 
aCattrBase* fill_param_list_attribute(DDlAttribute* pAttr, ulong attrID)
{		
	aCparameterList*   paAttr =  new aCparameterList;
	aCparameter        wrkParam;
	METHOD_PARAM_LIST* pParams;

	METHOD_PARAM_LIST :: iterator mp;
	
	pParams = pAttr->pVals->paramList;

	paAttr->clear();

	for ( mp = pParams->begin(); mp != pParams->end(); mp++)
	{
		fill_oneParam(wrkParam, (METHOD_PARAM*)&(*mp));
		paAttr->push_back(wrkParam);
		wrkParam.clear();
	}

	paAttr->attr_mask = attrID;

	return ((aCattrBase*)paAttr);
}

// stevev 11may05 -end 
/************************************************************************************************/

/************************************************************************************************/
void fill_var_item_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
#ifdef _DEBUG
	if (tmpaCitemBase->itemId == 0x40c3 )
	{
		LOGIT(CLOG_LOG," got 0x40c3\n");
	}
	ActiveVarID = tmpaCitemBase->itemId;
#endif
	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);
		aCattrBase* paAttr = NULL;

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
		// next two lines are unique for var items and definitely a bug [5/5/2014 timj]
		//else
		//{	tmpaCitemBase->isConditional = FALSE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{

			case	VAR_CLASS_ID:
				paAttr =  fill_bitstring_attribute(pAttr, VAR_CLASS);
				break;

			case	VAR_HANDLING_ID:
				paAttr =  fill_bitstring_attribute(pAttr, VAR_HANDLING);
				break;

			case	VAR_UNIT_ID :
				paAttr =  fill_string_attribute(pAttr, VAR_UNIT);
				break;

			case	VAR_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, VAR_LABEL);
				break;

			case  	VAR_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, VAR_HELP);
				break;
/* removed 15oct12
			case	VAR_READ_TIME_OUT_ID:
				paAttr =  fill_expression_attribute(pAttr, VAR_READ_TIME_OUT);
				break;

			case	VAR_WRITE_TIME_OUT_ID:
				paAttr =  fill_expression_attribute(pAttr, VAR_WRITE_TIME_OUT);
				break;
****/
			case	VAR_WIDTHSIZE_ID:
				paAttr =  fill_ulong_attribute(pAttr, VAR_WIDTH);
				break;

			case	VAR_HEIGHTSIZE_ID:
				paAttr =  fill_ulong_attribute(pAttr, VAR_HEIGHT);
				break;

			case	VAR_VALID_ID:
				paAttr =  fill_ulong_attribute(pAttr, VAR_VALID);
				break;

			case	VAR_PRE_READ_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_PRE_READ_ACT);
				break;

			case	VAR_POST_READ_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_POST_READ_ACT);
				break;

			case	VAR_PRE_WRITE_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_PRE_WRITE_ACT);
				break;

			case	VAR_POST_WRITE_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_POST_WRITE_ACT);
				break;

			case	VAR_PRE_EDIT_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_PRE_EDIT_ACT);
				break;

			case	VAR_POST_EDIT_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_POST_EDIT_ACT);
				break;

			case	VAR_REFRESH_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_REFRESH_ACT);
				break;

			case	VAR_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, VAR_DEBUG);
				break;

			case	VAR_POST_RQST_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_POST_RQST_ACT);
				break;

			case	VAR_POST_USER_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, VAR_POST_USER_ACT);
				break;

			case	VAR_TYPE_SIZE_ID:
				{	
					paAttr =  new aCattrTypeType;
					tmpaCitemBase->itemSize = pAttr->pVals->typeSize.size;
					((aCattrTypeType*)paAttr)->notCondTypeType.actualVarSize = pAttr->pVals->typeSize.size;
					((aCattrTypeType*)paAttr)->notCondTypeType.actualVarType = pAttr->pVals->typeSize.type;
					paAttr->attr_mask = VAR_TYPE_SIZE;
				}
				break;

			case	VAR_DISPLAY_ID:
				paAttr =  fill_string_attribute(pAttr, VAR_DISPLAY);
				break;

			case	VAR_EDIT_ID:
				paAttr =  fill_string_attribute(pAttr, VAR_EDIT);
				break;

			case	VAR_MIN_VAL_ID:
				paAttr =  fill_minmaxlist_attribute(pAttr, VAR_MIN_VAL);
				break;
				
			case	VAR_MAX_VAL_ID:
				paAttr =  fill_minmaxlist_attribute(pAttr, VAR_MAX_VAL);
				break;

			case	VAR_SCALE_ID:
				paAttr =  fill_expression_attribute(pAttr, VAR_SCALE);
				break;

			case	VAR_ENUMS_ID:
				{	
					paAttr =  new aCattrEnum;
					ENUM_VALUE_LIST *enmList;

					if( pAttr->bIsAttributeConditional     == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						enmList = pAttr->pVals->enmList;

						((aCattrEnum*)paAttr)->condEnumListOlists.genericlist.clear();

						aCgenericConditional tmpaCgenCond;
						tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
						tmpaCgenCond.destElements.clear();

						aCgenericConditional::aCexpressDest tempaCDest;
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCenumList;

						fill_enumlist((aCenumList *)tempaCDest.pPayload, enmList);

						tmpaCgenCond.destElements.push_back(tempaCDest);
						((aCattrEnum*)paAttr)->condEnumListOlists.genericlist.push_back(tmpaCgenCond);
						RAZE( tempaCDest.pPayload );// stevev 11sep09 - stop a major memory leak

					}
					else 
					if( pAttr->bIsAttributeConditional     == TRUE && 
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						fill_conditional_attributes_list(&(((aCattrEnum*)paAttr)->condEnumListOlists),pAttr->pCond);
					}
					else 
					if( pAttr->bIsAttributeConditional     == FALSE && 
						pAttr->bIsAttributeConditionalList == TRUE)
					{
						fill_conditional_chunks(&(((aCattrEnum*)paAttr)->condEnumListOlists),pAttr);
					}
					else // TRUE TRUE
					{
						LOGIT(CERR_LOG,"ERROR:enumList flagged as both not-List and List.\n");
					}
					// else - both - an error

					paAttr->attr_mask = VAR_ENUMS;
				}
				break;				
			case	VAR_INDEX_ITEM_ARRAY_ID:
				paAttr =  fill_reference_attribute(pAttr, VAR_INDEX_ITEM_ARRAY);
				break;
				
			case	VAR_DEFAULT_VALUE_ID:
				paAttr =  fill_expression_attribute(pAttr, VAR_DEFAULT_VALUE);
				break;
			/* stevev 30may08 added time attributes */
			case	VAR_TIME_FORMAT_ID:
				paAttr =  fill_string_attribute(pAttr, VAR_TIME_FORMAT);
				break;

			case	VAR_TIME_SCALE_ID:
				paAttr =  fill_bitstring_attribute(pAttr, VAR_TIME_SCALE);
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;

		}
        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }
	}/*End for p*/
}/*End fill_var_item_attributes */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_method_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
			case	METHOD_CLASS_ID:
				paAttr =  fill_bitstring_attribute(pAttr, METHOD_CLASS);
				break;

			case	METHOD_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, METHOD_LABEL);
				break;

			case  	METHOD_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, METHOD_HELP);
				break;

			case	METHOD_DEF_ID:
				{	
					paAttr =  new aCattrMethodDef;

					DEFINITION *def = &(pAttr->pVals->defData);
					((aCattrMethodDef *)paAttr)->blobLen = def->size;

					((aCattrMethodDef *)paAttr)->pBlob = new char[def->size + 1];
					memcpy(((aCattrMethodDef *)paAttr)->pBlob,def->data,def->size);
				}
					paAttr->attr_mask = METHOD_DEF;
				break;

			case	METHOD_VALID_ID:
				paAttr =  fill_ulong_attribute(pAttr, METHOD_VALID);
				break;

			case	METHOD_SCOPE_ID:
				{// eat the scope attribute, it's worthless and not in the fma files
				 // paAttr =  fill_bitstring_attribute(pAttr, METHOD_SCOPE);
				tmpaCitemBase->attrMask &= ~METHOD_SCOPE;
				continue;// don't push the a bad attribute 
				}
				break;

			case	METHOD_TYPE_ID:
				paAttr =  fill_methodType_attribute(pAttr, METHOD_TYPE);
				break;
			case	METHOD_PARAMS_ID:
				paAttr =  fill_param_list_attribute(pAttr, METHOD_PARAMS);
				break;
			case	METHOD_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, METHOD_DEBUG);
				break;

			case	MAX_METHOD_ID:
				{	
					paAttr->attr_mask = 0x40;// an error?? 
				}
				break;			
			default:
				/*Should Never Reach here!!!!*/
				break;

		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }
	}/*End for p*/

}/*End fill_method_attributes */

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_command_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{

			case	COMMAND_NUMBER_ID:
				paAttr =  fill_ulong_attribute(pAttr, COMMAND_NUMBER);
				break;

			case	COMMAND_OPER_ID:
				paAttr =  fill_ulong_attribute(pAttr, COMMAND_OPER);
				break;

			case	COMMAND_TRANS_ID :
				{	
					paAttr =  new aCattrCmdTrans;
					
					if( pAttr->bIsAttributeConditional     == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{

						fill_transaction_list((aCattrCmdTrans *)paAttr,pAttr->pVals->transList);
					}
					else // TRUE TRUE - or list FALSE TRUE
					{
						LOGIT(CERR_LOG,"ERROR:command transaction shown conditional.\n");
					}
					// else - transaction list not allowed to be conditional - error
					
					paAttr->attr_mask = COMMAND_TRANS;
					
				}
				break;

			case	COMMAND_RESP_CODES_ID:
				{
					paAttr =  new aCattrCmdRspCd;
	
					if( pAttr->bIsAttributeConditional     == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						fill_response_code_list(&(((aCattrCmdRspCd *)paAttr)->respCodes),pAttr->pVals->respCdList);
					}
					else
					if( pAttr->bIsAttributeConditional     == TRUE && 
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						fill_conditional_attributes_list(&(((aCattrCmdRspCd*)paAttr)->respCodes),pAttr->pCond);
					}
					else
					if( pAttr->bIsAttributeConditional     == FALSE && 
						pAttr->bIsAttributeConditionalList == TRUE)
					{
						fill_conditional_chunks(&(((aCattrCmdRspCd *)paAttr)->respCodes),pAttr);
					}
					else // TRUE TRUE - or list FALSE TRUE
					{
						LOGIT(CERR_LOG,"ERROR:response codes flagged as both not-List and List.\n");
					}
					// else - error

					paAttr->attr_mask = COMMAND_RESP_CODES;
	
				}
				break;
			case	COMMAND_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, COMMAND_DEBUG);
				break;
			case  	MAX_COMMAND_ID:
				{//an error
					paAttr->attr_mask = (1 << MAX_COMMAND_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_command_attributes */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_menu_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
			case	MENU_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, MENU_LABEL);
				break;

			case	MENU_ITEMS_ID:
				{
					paAttr =  new aCattrMenuItems;

					MENU_ITEM_LIST *menuList;

					if( pAttr->bIsAttributeConditional     == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						menuList = pAttr->pVals->menuItemsList;

						((aCattrMenuItems*)paAttr)->condMenuListOlists.genericlist.clear();

						aCgenericConditional tmpaCgenCond;
						tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
						tmpaCgenCond.destElements.clear();

						aCgenericConditional::aCexpressDest tempaCDest;
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCmenuList;

						fill_menu_item_list((aCmenuList *)tempaCDest.pPayload,menuList);

						tmpaCgenCond.destElements.push_back(tempaCDest);
						((aCattrMenuItems*)paAttr)->condMenuListOlists.genericlist.push_back(tmpaCgenCond);

					}
					else if(pAttr->bIsAttributeConditional     == TRUE && 
						    pAttr->bIsAttributeConditionalList == FALSE)
					{
						fill_conditional_attributes_list(&(((aCattrMenuItems*)paAttr)->condMenuListOlists),pAttr->pCond);
					}
					else if(pAttr->bIsAttributeConditional     == FALSE && 
						    pAttr->bIsAttributeConditionalList == TRUE)
					{
						fill_conditional_chunks(&(((aCattrMenuItems*)paAttr)->condMenuListOlists),pAttr);
					}
					else // TRUE TRUE - or list FALSE TRUE
					{
						LOGIT(CERR_LOG,"ERROR:menu item list flagged as both List and not-List.\n");
					}

					paAttr->attr_mask = 0x2;
				}
				break;

			case	MENU_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, MENU_HELP);
				break;

			case	MENU_VALID_ID:
				paAttr =  fill_ulong_attribute(pAttr, MENU_VALID);
				break;

			case	MENU_STYLE_ID:	
				paAttr =  fill_ulong_attribute(pAttr, MENU_STYLE);
				break;

			case	MENU_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, MENU_DEBUG);
				break;
				
			default:/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_menu_attributes */

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_edit_display_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (DDlAttribute *)(*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
		case	EDIT_DISPLAY_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, EDIT_DISPLAY_LABEL);
				break;

		case	EDIT_DISPLAY_EDIT_ITEMS_ID:
				paAttr =  fill_reflist_attribute(pAttr, EDIT_DISPLAY_EDIT_ITEMS);
				break;

		case	EDIT_DISPLAY_DISP_ITEMS_ID :
				paAttr =  fill_reflist_attribute(pAttr, EDIT_DISPLAY_DISP_ITEMS);
				break;

		case	EDIT_DISPLAY_PRE_EDIT_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, EDIT_DISPLAY_PRE_EDIT_ACT);
				break;

		case	EDIT_DISPLAY_POST_EDIT_ACT_ID:
				paAttr =  fill_actions_attribute(pAttr, EDIT_DISPLAY_POST_EDIT_ACT);
				break;

		case	EDIT_DISPLAY_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, EDIT_DISPLAY_HELP);
				break;

		case	EDIT_DISPLAY_VALID_ID:
				paAttr =  fill_ulong_attribute(pAttr, EDIT_DISPLAY_VALID);
				break;
		case	EDIT_DISPLAY_DEBUG_ID:
			paAttr =  fill_debug_attribute(pAttr, EDIT_DISPLAY_DEBUG);
			break;

		case  	MAX_EDIT_DISPLAY_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_EDIT_DISPLAY_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_edit_display_attributes */

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_item_array_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& alist)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = alist.begin(); p!= alist.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
		case	ITEM_ARRAY_ELEMENTS_ID:
				paAttr = fill_memberlist_attribute(pAttr, ITEM_ARRAY_ELEMENTS);
				break;

		case	ITEM_ARRAY_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, ITEM_ARRAY_LABEL);
				break;

		case	ITEM_ARRAY_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, ITEM_ARRAY_HELP);
				break;

		case	ITEM_ARRAY_VALIDITY_ID:
				paAttr =  fill_ulong_attribute(pAttr, ITEM_ARRAY_VALIDITY);
				break;

		case	ITEM_ARRAY_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, ITEM_ARRAY_DEBUG);
				break;

		case  	MAX_ITEM_ARRAY_ID:
				{// an error
					paAttr->attr_mask = (1<<MAX_ITEM_ARRAY_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_item_array_attributes */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_collection_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
		case	COLLECTION_MEMBERS_ID:
			/* TODO: do the resolve_record_ref() demunge elsewhere before we get here */
				paAttr = fill_memberlist_attribute(pAttr, COLLECTION_MEMBERS);
				break;

			case	COLLECTION_LABEL_ID:
				paAttr =  fill_string_attribute(pAttr, COLLECTION_LABEL);
				break;

			case	COLLECTION_HELP_ID:
				paAttr =  fill_string_attribute(pAttr, COLLECTION_HELP);
				break;

			case	COLLECTION_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, COLLECTION_DEBUG);
				break;

			case	COLLECTION_VALID_ID:
				paAttr =  fill_ulong_attribute(pAttr, COLLECTION_VALIDITY);
				break;

			case  	MAX_COLLECTION_ID:
				{// error
					paAttr->attr_mask = (1<<MAX_COLLECTION_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_collection_attributes */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_refresh_attributes(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
			case	REFRESH_ITEMS_ID:
				{
					paAttr =  new aCattrRefreshitems;

					REFRESH_RELATION* refReln;

					if( pAttr->bIsAttributeConditional     == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						refReln = pAttr->pVals->refrshReln;

						((aCattrRefreshitems*)paAttr)->condUpdateList.genericlist.clear();
						((aCattrRefreshitems*)paAttr)->condWatchList.genericlist.clear();

						/* Fill watch List */
						aCgenericConditional tmpaCgenCond;
						tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
						tmpaCgenCond.destElements.clear();
						aCgenericConditional::aCexpressDest tempaCDest;
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCreferenceList;

						fill_refresh_relation(((aCreferenceList *)tempaCDest.pPayload),&refReln->watch_list);
						tmpaCgenCond.destElements.push_back(tempaCDest);
						((aCattrRefreshitems*)paAttr)->condWatchList.genericlist.push_back(tmpaCgenCond);

						tmpaCgenCond.clear();
                        //CPMHACK: Delete the payload memory before re-initialize
                        if(nullptr != tempaCDest.pPayload )
                        {
                            delete tempaCDest.pPayload;
                        }
						tempaCDest.pPayload = NULL;

						/* Fill Update List */
						tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
						tmpaCgenCond.destElements.clear();
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCreferenceList;
						
						fill_refresh_relation(((aCreferenceList *)tempaCDest.pPayload),&refReln->update_list);
						tmpaCgenCond.destElements.push_back(tempaCDest);
						((aCattrRefreshitems*)paAttr)->condUpdateList.genericlist.push_back(tmpaCgenCond);

					}
					else // TRUE TRUE - or list FALSE TRUE
					{
						LOGIT(CERR_LOG,"ERROR:refresh list flagged as conditional.\n");
					}
					// else - the attribute is not allowed to be conditional ( the reference list is though )

					paAttr->attr_mask = REFRESH_ITEMS;
				}
				break;

			case	REFRESH_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, REFRESH_DEBUG);
				break;

			case  	MAX_REFRESH_ID:
				{
					paAttr->attr_mask = (1<<MAX_REFRESH_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_refresh_attributes */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_unit_relation(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
			case	UNIT_ITEMS_ID:
				{
/*Vibhor 210904: Start of Code*/
/*Restoring the old code as this is different than a reflist !!*/
					//	paAttr =  fill_reflist_attribute(pAttr, UNIT_ITEMS);  //Vibhor :WRONG
					paAttr =  new aCattrUnititems;

					UNIT_RELATION *unitReln;

					if(pAttr->bIsAttributeConditional == FALSE &&
						pAttr->bIsAttributeConditionalList == FALSE)
					{
						unitReln = pAttr->pVals->unitReln;

						((aCattrUnititems*)paAttr)->condVar.clear();
						((aCattrUnititems*)paAttr)->condListOUnitLists.genericlist.clear();

						/* Fill Conditional Var */
						((aCattrUnititems *)paAttr)->condVar.priExprType = (expressionType_e) eT_Direct;
						((aCattrUnititems *)paAttr)->condVar.destElements.clear();
						aCgenericConditional::aCexpressDest tempaCDest;
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCreference;

						ddpREFERENCE :: iterator q;
						aCreference *tmpaCref = (aCreference *)(tempaCDest.pPayload);
						ddpREFERENCE	*unit_var = &unitReln->unit_var;
						BYTE makeTmpNull = 0;
						for(q = unit_var->begin();q != unit_var->end();q++)
						{
							/* If not the first time allocate memory */
							if(makeTmpNull)
							{
								tmpaCref->pRef = new aCreference;
								tmpaCref = tmpaCref->pRef;
							}

                            // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                            // when its destructor is called and cleans own data up (included the content of the pointed vector)
                            /* 
							#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
								ddpREF myRef;
								myRef.type = q->type;
								myRef.val  = q->val;
								fill_reference(tmpaCref, &myRef);
							#else
							fill_reference(tmpaCref,q);
							#endif
                            */

                            // CodeWrights [PC 22] replaced with:
							fill_reference(tmpaCref, (ddpREF *)&(*q));

							makeTmpNull = 1;
						}	

						((aCattrUnititems*)paAttr)->condVar.destElements.push_back(tempaCDest);

                        //CPMHACK: Delete the payload memory before re-initialize
                        if(nullptr != tempaCDest.pPayload )
                        {
                            delete tempaCDest.pPayload;
                        }
						tempaCDest.pPayload = NULL;

						/* Fill Update List */
						aCgenericConditional tmpaCgenCond;
						tmpaCgenCond.priExprType = (expressionType_e) eT_Direct;
						tmpaCgenCond.destElements.clear();
						tempaCDest.destType = eT_Direct;
						tempaCDest.pPayload = new aCreferenceList; //was aCunitList;
						
						fill_refresh_relation(((aCreferenceList *)tempaCDest.pPayload),&unitReln->var_units);
						tmpaCgenCond.destElements.push_back(tempaCDest);
						((aCattrUnititems*)paAttr)->condListOUnitLists.genericlist.push_back(tmpaCgenCond);
					}
					else
					{
						LOGIT(CERR_LOG,"ERROR:unit relation flagged as Conditional.\n");
						/* stevev - 23aug06 - unit relation is not allowed to be conditional....
						if(pAttr->bIsAttributeConditional == TRUE && pAttr->bIsAttributeConditionalList == FALSE)
						{  //Needs to be taken care
							//fill_conditional_attributes_list(&(((aCattrUnititems*)paAttr)->condVar),pAttr->pCond);
							//fill_conditional_attributes_list(&(((aCattrUnititems*)paAttr)->condListOUnitLists),pAttr->pCond);
						}
						end 23aug06*/
					}

					paAttr->attr_mask = (1<<UNIT_ITEMS_ID);
/*Vibhor 210904: End of Code*/
				}
				break;

			case	UNIT_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, UNIT_DEBUG);
				break;

			case  	MAX_UNIT_ID:
				{
					paAttr->attr_mask = (1<<MAX_UNIT_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_unit_relation */


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_wao_relation(aCitemBase *tmpaCitemBase,ItemAttrList& List)
{
	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase* paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

/* VMKP added on 291203 */
		if( (pAttr->bIsAttributeConditional == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	tmpaCitemBase->isConditional = TRUE; }
/* VMKP added on 291203 */

		switch(pAttr->byAttrID)
		{
			case	WAO_ITEMS_ID:
				paAttr =  fill_reflist_attribute(pAttr, WAO_ITEMS);
				break;

			case	WAO_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, WAO_DEBUG);
				break;

			case  	MAX_WAO_ID:
				{
					paAttr->attr_mask = (1<<MAX_WAO_ID);
				}
				break;

			default:
				/*Should Never Reach here!!!!*/
				break;
		}

        if (NULL != paAttr) {
            tmpaCitemBase->attrLst.push_back(paAttr);
        }

	}/*End for p*/

}/*End fill_wao_relation */

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_conditional_attributes(aCconditional* paCond,DDlConditional *pCond)
{
	aCgenericConditional  *pgenCond = (aCgenericConditional  *) paCond;
	aCgenericConditional::aCexpressDest tmpaCexpressDest;
	pgenCond->destElements.clear();

	int iSectionIndex = 0; /*We will use this index to traverse the sections of the conditional*/
	int iChildIndex = 0;
	int	iValueIndex = 0;

	if(pCond->condType == DDL_COND_TYPE_UNDEFINED)
		pgenCond->priExprType = eT_Unknown;
	else if (pCond->condType == DDL_COND_TYPE_IF)
		pgenCond->priExprType = eT_IF_expr;
	else if (pCond->condType == DDL_COND_TYPE_SELECT)
		pgenCond->priExprType = eT_SEL_expr;
	else if (pCond->condType == DDL_COND_TYPE_DIRECT)
		pgenCond->priExprType = eT_Direct;

	if (pgenCond->priExprType == eT_IF_expr)
	{
		fill_expression(&pgenCond->priExpression,&pCond->expr);
		
		tmpaCexpressDest.destType = eT_IF_isTRUE;
		
		aCgenericConditional  *caseGenCond;
		aCgenericConditional::aCexpressDest tmpaCexpDest;

		if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
		{
			tmpaCexpressDest.pCondDest = new aCconditional;
			caseGenCond = (aCconditional *) tmpaCexpressDest.pCondDest;
			caseGenCond->priExprType = eT_Direct;
			tmpaCexpDest.destType = eT_Direct;

			/*switch to the appropriate data type & simply dump it!!*/
			switch(pCond->attrDataType)
			{

				case 	DDL_ATTR_DATA_TYPE_INT:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
					break;

				case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_FLOAT:
				case	DDL_ATTR_DATA_TYPE_DOUBLE:
					break;

				case	DDL_ATTR_DATA_TYPE_BITSTRING :

					tmpaCexpDest.pPayload = new aCbitString();
					((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
						(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_STRING:

					tmpaCexpDest.pPayload = new aCddlString();
					copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
					break;

				case	DDL_ATTR_DATA_TYPE_ITEM_ID:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					{
						tmpaCexpDest.pPayload = new aCenumList;
						fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE :
					{
						tmpaCexpDest.pPayload = new aCreference;

                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = ((pCond->Vals).at(iValueIndex).ref->begin())->type;
							myRef.val  = ((pCond->Vals).at(iValueIndex).ref->begin())->val;
							fill_reference((aCreference*)tmpaCexpDest.pPayload, &myRef);
						#else
						ddpREFERENCE :: iterator p;
						tmpaCexpDest.pPayload = new aCreference;
						p = (ddpREF*)((pCond->Vals).at(iValueIndex).ref->begin());
						fill_reference((aCreference *) tmpaCexpDest.pPayload,p);//(ddpREF*)(pCond->Vals).at(iValueIndex).ref->begin()));
						#endif
                        */

                        // CodeWrights [PC 22] replaced with:
						fill_reference((aCreference*)tmpaCexpDest.pPayload, 
                            (ddpREF *)&(*((pCond->Vals).at(iValueIndex).ref->begin())));
					}
					break;

				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
					break;

				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_DEFINITION:
					break;
				
				case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
					break;

				case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
					break;

				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_EXPRESSION:
					{
						tmpaCexpDest.pPayload = new aCExpressionPayload();
						fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
					}
					break;

				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					{
// error					tmpaCexpDest.pPayload = new aCminmaxList();
//    						fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
					{
						tmpaCexpDest.pPayload = new aCintWhich ();
						((aCintWhich*)tmpaCexpDest.pPayload)->value = pCond->Vals.at(iValueIndex).lineType.type;
						((aCintWhich*)tmpaCexpDest.pPayload)->which = pCond->Vals.at(iValueIndex).lineType.qual;
					}
					break;	
				case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
				case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
				case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
				case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
				case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
				case	DDL_ATTR_DATA_TYPE_PARAM:
				case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
					LOGIT(CERR_LOG, "fill_conditional_attributes (IF) - New (unimplemented) type\n");
					break;
				default:
					LOGIT(CERR_LOG, "fill_conditional_attributes (IF) - Unknown data type\n");
					/*Surprise!!!*/
					break;
			}/*End switch*/

			caseGenCond->destElements.push_back(tmpaCexpDest);
			pgenCond->destElements.push_back(tmpaCexpressDest);
			tmpaCexpressDest.clear();
		}
		else
		{
			tmpaCexpressDest.pCondDest =  new aCconditional;
			fill_conditional_attributes(((aCconditional *)(tmpaCexpressDest.pCondDest)),
					pCond->listOfChilds.at(iChildIndex));
			pgenCond->destElements.push_back(tmpaCexpressDest);
			tmpaCexpressDest.clear();
			iChildIndex++;
		}


		if (pCond->byNumberOfSections == 2)
		{
			iSectionIndex++; /*increment the section index*/
			iValueIndex++;
		    tmpaCexpressDest.destType = eT_IF_isFALSE;

			if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
			{
				tmpaCexpressDest.pCondDest = new aCconditional;
				caseGenCond = (aCconditional *) tmpaCexpressDest.pCondDest;
				caseGenCond->priExprType = eT_Direct;
				tmpaCexpDest.destType = eT_Direct;

			/*switch to the appropriate data type & simply dump it!!*/
				switch(pCond->attrDataType)
				{

					case 	DDL_ATTR_DATA_TYPE_INT:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
						break;
					case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_FLOAT:
					case	DDL_ATTR_DATA_TYPE_DOUBLE:
						break;

					case	DDL_ATTR_DATA_TYPE_BITSTRING :
						tmpaCexpDest.pPayload = new aCbitString();
						((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
							(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_STRING:
						tmpaCexpDest.pPayload = new aCddlString();
						copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ID:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
						{
							tmpaCexpDest.pPayload = new aCenumList;
							fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE :
						{
						// CodeWrights [PC 22] no longer used	ddpREFERENCE :: iterator p;
							tmpaCexpDest.pPayload = new aCreference;

                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = ((pCond->Vals).at(iValueIndex).ref->begin())->type;
							myRef.val  = ((pCond->Vals).at(iValueIndex).ref->begin())->val;
							fill_reference((aCreference*)tmpaCexpDest.pPayload, &myRef);
						#else
							p = (ddpREF*)((pCond->Vals).at(iValueIndex).ref->begin());
							fill_reference((aCreference *) tmpaCexpDest.pPayload,p);//(ddpREF*)(pCond->Vals).at(iValueIndex).ref->begin()));
						#endif
                        */

                        // CodeWrights [PC 22] replaced with:
						fill_reference((aCreference*)tmpaCexpDest.pPayload, 
                            (ddpREF *)&(*((pCond->Vals).at(iValueIndex).ref->begin())));

						}
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
						break;

					case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_DEFINITION:
						break;
					
					case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_EXPRESSION:
						{
							tmpaCexpDest.pPayload = new aCExpressionPayload();
							fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MIN_MAX:
						{
//error							tmpaCexpDest.pPayload = new aCminmaxList();
//   							fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
					{
						tmpaCexpDest.pPayload = new aCintWhich ();//();
						((aCintWhich*)tmpaCexpDest.pPayload)->value = pCond->Vals.at(iValueIndex).lineType.type;
						((aCintWhich*)tmpaCexpDest.pPayload)->which = pCond->Vals.at(iValueIndex).lineType.qual;
					}
					break;	
				case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
				case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
				case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
				case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
				case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
				case	DDL_ATTR_DATA_TYPE_PARAM:
				case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
					LOGIT(CLOG_LOG,"fill_conditional_attributes (ELSE) - New (unimplemented) type\n");
					break;

				default:
					LOGIT(CERR_LOG, "fill_conditional_attributes (ELSE) - Unknown data type\n");
							/*Surprise!!!*/
						break;
				}/*End switch*/

				caseGenCond->destElements.push_back(tmpaCexpDest);
				pgenCond->destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
				tmpaCexpDest.clear();// 12feb14 - leak repair
			}
			else
			{
				tmpaCexpressDest.pCondDest =  new aCconditional;
				fill_conditional_attributes(((aCconditional *)(tmpaCexpressDest.pCondDest)),
								pCond->listOfChilds.at(iChildIndex));
				pgenCond->destElements.push_back(tmpaCexpressDest);
                //CPMHACK: Delete the conditional destination memory before clearing it
                if(nullptr != tmpaCexpressDest.pCondDest)
                {
                    delete tmpaCexpressDest.pCondDest;
                    tmpaCexpressDest.pCondDest = nullptr;
                }
				tmpaCexpressDest.clear();
			}
		}

	}
	else if (pgenCond->priExprType == eT_SEL_expr)
	{
		fill_expression(&pgenCond->priExpression,&pCond->expr);

		while( iSectionIndex < pCond->byNumberOfSections)
		{
/* stevev 05jun14 - there is NO requirement that a DEFAULT has to be at the end!!!!!
			if(iSectionIndex == pCond->caseVals.size())
			{
				tmpaCexpressDest.destType = et_SEL_isDEFAULT;
				tmpaCexpressDest.destExpression.clear();
			}
			else
			{
				tmpaCexpressDest.destType = eT_CASE_expr;
				tmpaCexpressDest.destExpression.clear();
				fill_expression(&tmpaCexpressDest.destExpression,&pCond->caseVals.at(iSectionIndex));
			}
***/
			tmpaCexpressDest.destExpression.clear();
			ddpExpression *pExpr = &pCond->caseVals.at(iSectionIndex);
			if (pExpr->size() == 0)
			{
				tmpaCexpressDest.destType = et_SEL_isDEFAULT;
			}
			else
			{
				tmpaCexpressDest.destType = eT_CASE_expr;
				fill_expression(&tmpaCexpressDest.destExpression, pExpr);
			}
/* end stevev 05jun14 **/

			aCgenericConditional  *caseGenCond;
			aCgenericConditional::aCexpressDest tmpaCexpDest;

			if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
			{
				tmpaCexpressDest.pCondDest = new aCconditional;
				caseGenCond = (aCconditional *) tmpaCexpressDest.pCondDest;
				caseGenCond->priExprType = eT_Direct;
				tmpaCexpDest.destType = eT_Direct;

				/*switch to the appropriate data type & simply dump it!!*/
				switch(pCond->attrDataType)
				{

					case 	DDL_ATTR_DATA_TYPE_INT:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
						break;

					case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_FLOAT:
					case	DDL_ATTR_DATA_TYPE_DOUBLE:
						break;

					case	DDL_ATTR_DATA_TYPE_BITSTRING :

						tmpaCexpDest.pPayload = new aCbitString();
						((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
								(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_STRING:

						tmpaCexpDest.pPayload = new aCddlString();
						copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ID:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
						{
							tmpaCexpDest.pPayload = new aCenumList;
							fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_REFERENCE :
						{
							tmpaCexpDest.pPayload = new aCreference;

                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = ((pCond->Vals).at(iValueIndex).ref->begin())->type;
							myRef.val  = ((pCond->Vals).at(iValueIndex).ref->begin())->val;
							fill_reference((aCreference*)tmpaCexpDest.pPayload, &myRef);
						#else
							ddpREFERENCE :: iterator p;
							p = (ddpREF*)((pCond->Vals).at(iValueIndex).ref->begin());
							fill_reference((aCreference *) tmpaCexpDest.pPayload,p);//(ddpREF*)(pCond->Vals).at(iValueIndex).ref->begin()));
						#endif
                        */

                        // CodeWrights [PC 22] replaced with:
						fill_reference((aCreference*)tmpaCexpDest.pPayload, 
                            (ddpREF *)&(*((pCond->Vals).at(iValueIndex).ref->begin())));

						}
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
						break;

					case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_DEFINITION:
						break;

					case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_EXPRESSION:
						{
							tmpaCexpDest.pPayload = new aCExpressionPayload();
							fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MIN_MAX:
						{
//error							tmpaCexpDest.pPayload = new aCminmaxList();
//   							fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
					{
						tmpaCexpDest.pPayload = new aCintWhich ();//();
						((aCintWhich*)tmpaCexpDest.pPayload)->value = pCond->Vals.at(iValueIndex).lineType.type;
						((aCintWhich*)tmpaCexpDest.pPayload)->which = pCond->Vals.at(iValueIndex).lineType.qual;
					}
					break;
				case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
				case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
				case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
				case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
				case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
				case	DDL_ATTR_DATA_TYPE_PARAM:
				case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
					LOGIT(CLOG_LOG,"fill_conditional_attributes (CASE) - New (unimplemented) type\n");
					break;

				default:
					LOGIT(CERR_LOG, "fill_conditional_attributes (CASE) - Unknown data type\n");
						/*Surprise!!!*/
						break;
				}/*End switch*/

				caseGenCond->destElements.push_back(tmpaCexpDest);
				pgenCond->destElements.push_back(tmpaCexpressDest);

                //CPMHACK:
                if(nullptr != tmpaCexpressDest.pCondDest)
                {
                    delete tmpaCexpressDest.pCondDest;
                    tmpaCexpressDest.pCondDest = nullptr;
                }
				tmpaCexpressDest.clear();
			}
			else
			{
				tmpaCexpressDest.pCondDest = new aCconditional;
				fill_conditional_attributes(((aCconditional *)(tmpaCexpressDest.pCondDest)),
						pCond->listOfChilds.at(iChildIndex));
				pgenCond->destElements.push_back(tmpaCexpressDest);
                //CPMHACK:
                if(nullptr != tmpaCexpressDest.pCondDest)
                {
                    delete tmpaCexpressDest.pCondDest;
                    tmpaCexpressDest.pCondDest = nullptr;
                }
				tmpaCexpressDest.clear();
				iChildIndex++;
			}
		iSectionIndex++;
		iValueIndex++;
		}

	}
	
	/* End of secondary conditional*/
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_conditional_attributes_list(aCcondList *paCond,DDlConditional *pCond)
{
	int iValueIndex = 0; /*We will use this to index into Vals vector*/
	int iChildIndex = 0; /*We will use this to index into  listOfChilds*/
	int iSectionIndex = 0; /*We will use this index to traverse the sections of the conditional*/
	int iChunkIndex = 0; /*We will use this to index into listOfChunks*/ //Vibhor 200105: Added

	aCgenericConditional  pgenCond;
	pgenCond.destElements.clear();
	aCgenericConditional::aCexpressDest tmpaCexpressDest;

	if (pCond->condType == DDL_COND_TYPE_IF)
	{
		pgenCond.priExprType = eT_IF_expr;

		fill_expression(&pgenCond.priExpression,&pCond->expr);
		
		tmpaCexpressDest.destType = eT_IF_isTRUE;

		aCgenericConditional  caseGenCond;
		aCgenericConditional::aCexpressDest tmpaCexpDest;
		tmpaCexpressDest.pCondDest = new aCcondList;

		if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
		{
			caseGenCond.priExprType = eT_Direct;
			tmpaCexpDest.destType = eT_Direct;

			/*switch to the appropriate data type & simply dump it!!*/
			switch(pCond->attrDataType)
			{

				case 	DDL_ATTR_DATA_TYPE_INT:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
					break;

				case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_FLOAT:
				case	DDL_ATTR_DATA_TYPE_DOUBLE:
					break;

				case	DDL_ATTR_DATA_TYPE_BITSTRING :

					tmpaCexpDest.pPayload = new aCbitString();
					((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
						(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_STRING:

					tmpaCexpDest.pPayload = new aCddlString();
					copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
					break;

				case	DDL_ATTR_DATA_TYPE_ITEM_ID:
					tmpaCexpDest.pPayload = new aCddlLong();
					((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
					break;

				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					{
						tmpaCexpDest.pPayload = new aCenumList;
						fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE :
					break;

				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					{
						tmpaCexpDest.pPayload = new aCreferenceList;
						fill_reference_list((aCreferenceList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).refList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					break;

				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					{
						tmpaCexpDest.pPayload = new aCrespCodeList;
						RESPONSE_CODE_LIST :: iterator p;
						RESPONSE_CODE	*pRespCode;
						RESPONSE_CODE_LIST *respList = pCond->Vals.at(iValueIndex).respCdList;

						if(respList->size() == 0)
							return;

						aCrespCodeList pL;
						aCrespCode pI;
						
						for(p = respList->begin();p != respList->end();p++)
						{
							pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
							pI.type = pRespCode->type;
							pI.val  = pRespCode->val;
							copy_ddlstring(&pI.descS,&(pRespCode->desc));

							if(pRespCode->evaled & RS_HELP_EVALED)
							{
								copy_ddlstring(&pI.helpS,&(pRespCode->help));

							}/*End if*/

							((aCrespCodeList *)(tmpaCexpDest.pPayload))->push_back(pI);

						}/*End for*/
					}
					break;

				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					{
						tmpaCexpDest.pPayload = new aCmenuList;
						fill_menu_item_list((aCmenuList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).menuItemsList);
					}
					break;

				case	DDL_ATTR_DATA_TYPE_DEFINITION:
					break;
				
				case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
					break;

				case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
					break;

				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					{
						tmpaCexpDest.pPayload = new aCmemberElementList;
						fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).itemArrElmnts);
					}
					break;

				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					{
						tmpaCexpDest.pPayload = new aCmemberElementList;
						fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload), pCond->Vals.at(iValueIndex).itemArrElmnts);
					}
					break;

				case	DDL_ATTR_DATA_TYPE_EXPRESSION:
						{
							tmpaCexpDest.pPayload = new aCExpressionPayload();
							fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
						}
					break;

				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					{
//error						tmpaCexpDest.pPayload = new aCminmaxList();
//   						fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
					{
						tmpaCexpDest.pPayload = new aCgridMemberList;
						fill_grid_member_list((aCgridMemberList *) (tmpaCexpDest.pPayload), pCond->Vals.at(iValueIndex).gridMemList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
				case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
				case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
				case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
				case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
				case	DDL_ATTR_DATA_TYPE_PARAM:
				case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
					LOGIT(CERR_LOG, "fill_conditional_attributes_list (IF) - New (unimplemented) type\n");
					break;

				default:
					LOGIT(CERR_LOG, "fill_conditional_attributes_list (IF) - Unknown data type\n");
					/*Surprise!!!*/
					break;
			}/*End switch*/
	
			caseGenCond.destElements.push_back(tmpaCexpDest);
			((aCcondList *)tmpaCexpressDest.pCondDest)->genericlist.push_back(caseGenCond);
			pgenCond.destElements.push_back(tmpaCexpressDest);
			tmpaCexpressDest.clear();
		}
		else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL) //Vibhor 200105: Added
		{ //Nested Conditional
			fill_conditional_attributes_list(((aCcondList *)(tmpaCexpressDest.pCondDest)),
					pCond->listOfChilds.at(iChildIndex));
			pgenCond.destElements.push_back(tmpaCexpressDest);
			tmpaCexpressDest.clear();
			iChildIndex++;
		}  
		else  //Vibhor 200105: Added
		{//this section of conditional has chunks
			fill_section_chunks(((aCcondList *)(tmpaCexpressDest.pCondDest)),
						 pCond->listOfChunks.at(iChunkIndex));
			pgenCond.destElements.push_back(tmpaCexpressDest);
			tmpaCexpressDest.clear();
			iChunkIndex++;
		}

		if (pCond->byNumberOfSections == 2)
		{
			tmpaCexpressDest.destType = eT_IF_isFALSE;
			iSectionIndex++; /*increment the section index*/
			iValueIndex++;
			
			aCgenericConditional  caseGenCond;
			aCgenericConditional::aCexpressDest tmpaCexpDest;
			tmpaCexpressDest.pCondDest = new aCcondList;

			if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
			{
				caseGenCond.priExprType = eT_Direct;
				tmpaCexpDest.destType = eT_Direct;

				/*switch to the appropriate data type & simply dump it!!*/
				switch(pCond->attrDataType)
				{

					case 	DDL_ATTR_DATA_TYPE_INT:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
						break;
					case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_FLOAT:
					case	DDL_ATTR_DATA_TYPE_DOUBLE:
						break;

					case	DDL_ATTR_DATA_TYPE_BITSTRING :
						tmpaCexpDest.pPayload = new aCbitString();
						((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
								(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_STRING:
						tmpaCexpDest.pPayload = new aCddlString();
						copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ID:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
						{
							tmpaCexpDest.pPayload = new aCenumList;
							fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE :
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
						{
							tmpaCexpDest.pPayload = new aCreferenceList;
							fill_reference_list((aCreferenceList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).refList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
					case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
						break;

					case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
						{
							tmpaCexpDest.pPayload = new aCrespCodeList;
							RESPONSE_CODE_LIST :: iterator p;
							RESPONSE_CODE	*pRespCode;
							RESPONSE_CODE_LIST *respList = pCond->Vals.at(iValueIndex).respCdList;

							if(respList->size() == 0)
								return;

							aCrespCodeList pL;
							aCrespCode pI;
							
							for(p = respList->begin();p != respList->end();p++)
							{
								pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
								pI.type = pRespCode->type;
								pI.val  = pRespCode->val;
								copy_ddlstring(&pI.descS,&(pRespCode->desc));

								if(pRespCode->evaled & RS_HELP_EVALED)
								{
									copy_ddlstring(&pI.helpS,&(pRespCode->help));

								}/*End if*/

								((aCrespCodeList *)(tmpaCexpDest.pPayload))->push_back(pI);

							}/*End for*/
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
						{
							tmpaCexpDest.pPayload = new aCmenuList;
							fill_menu_item_list((aCmenuList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).menuItemsList);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_DEFINITION:
						break;
					
					case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
						{
							tmpaCexpDest.pPayload = new aCmemberElementList;
							fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).itemArrElmnts);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
						{
							tmpaCexpDest.pPayload = new aCmemberElementList;
							fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).itemArrElmnts);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_EXPRESSION:
						{
							tmpaCexpDest.pPayload = new aCExpressionPayload();
							fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MIN_MAX:
						{
//error							tmpaCexpDest.pPayload = new aCminmaxList();
//  							fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
					case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
					case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
					case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
					case	DDL_ATTR_DATA_TYPE_GRID_SET:
					case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
					case	DDL_ATTR_DATA_TYPE_PARAM:
					case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
						LOGIT(CLOG_LOG,"fill_conditional_attributes_list (ELSE) - New (unimplemented) type\n");
						break;

					default:
						LOGIT(CERR_LOG, "fill_conditional_attributes_list (ELSE) - Unknown data type\n");
							/*Surprise!!!*/
						break;
				}/*End switch*/

				caseGenCond.destElements.push_back(tmpaCexpDest);
				((aCcondList *)tmpaCexpressDest.pCondDest)->genericlist.push_back(caseGenCond);
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
			}
			else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL) //Vibhor 200105: Added
			{ //Nested Conditional
				fill_conditional_attributes_list(((aCcondList *)(tmpaCexpressDest.pCondDest)),
						pCond->listOfChilds.at(iChildIndex));
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
				//iChildIndex++;
			}  
			else  //Vibhor 200105: Added
			{//this section of conditional has chunks
				fill_section_chunks(((aCcondList *)(tmpaCexpressDest.pCondDest)),
							 pCond->listOfChunks.at(iChunkIndex));
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
				iChunkIndex++;
			}
		}//endif pCond->byNumberOfSections == 2

	}
	else if (pCond->condType == DDL_COND_TYPE_SELECT)
	{
		pgenCond.priExprType = eT_SEL_expr;

		fill_expression(&pgenCond.priExpression,&pCond->expr);

		while( iSectionIndex < pCond->byNumberOfSections)
		{
			// there is NO requirement that a DEFAULT has to be at the end!!!!! [7/14/2014 timj]
			tmpaCexpressDest.destExpression.clear();
			ddpExpression *pExpr = &pCond->caseVals.at(iSectionIndex);
			if(pExpr->size() == 0)
			{
				tmpaCexpressDest.destType = et_SEL_isDEFAULT;
			}
			else
			{
				tmpaCexpressDest.destType = eT_CASE_expr;
				fill_expression(&tmpaCexpressDest.destExpression,&pCond->caseVals.at(iSectionIndex));
			}

			aCgenericConditional  caseGenCond;
			aCgenericConditional::aCexpressDest tmpaCexpDest;
			tmpaCexpressDest.pCondDest = new aCcondList;

			if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)
			{
				caseGenCond.priExprType = eT_Direct;
				tmpaCexpDest.destType = eT_Direct;

				/*switch to the appropriate data type & simply dump it!!*/
				switch(pCond->attrDataType)
				{

					case 	DDL_ATTR_DATA_TYPE_INT:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).llVal;
						break;

					case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_FLOAT:
					case	DDL_ATTR_DATA_TYPE_DOUBLE:
						break;

					case	DDL_ATTR_DATA_TYPE_BITSTRING :

						tmpaCexpDest.pPayload = new aCbitString();
						((aCbitString *)tmpaCexpDest.pPayload)->bitstringVal = // bitstring is limited to 32 bits
								(unsigned long)pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_STRING:

						tmpaCexpDest.pPayload = new aCddlString();
						copy_ddlstring(((aCddlString *)tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).strVal);
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ID:
						tmpaCexpDest.pPayload = new aCddlLong();
						((aCddlLong *)tmpaCexpDest.pPayload)->ddlLong = pCond->Vals.at(iValueIndex).ullVal;
						break;

					case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
						{
							tmpaCexpDest.pPayload = new aCenumList;
							fill_enumlist((aCenumList *) tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).enmList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_REFERENCE :
						break;

					case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
						{
							tmpaCexpDest.pPayload = new aCreferenceList;
							fill_reference_list((aCreferenceList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).refList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
					case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
						break;


					case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
						{
							tmpaCexpDest.pPayload = new aCrespCodeList;
							RESPONSE_CODE_LIST :: iterator p;
							RESPONSE_CODE*	pRespCode;
							RESPONSE_CODE_LIST *respList = pCond->Vals.at(iValueIndex).respCdList;

							if(respList->size() == 0)
								return;

							aCrespCodeList pL;
							aCrespCode pI;
							
							for(p = respList->begin();p != respList->end();p++)
							{
								pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
								pI.type = pRespCode->type;
								pI.val  = pRespCode->val;
								copy_ddlstring(&pI.descS,&(pRespCode->desc));

								if(pRespCode->evaled & RS_HELP_EVALED)
								{
									copy_ddlstring(&pI.helpS,&(pRespCode->help));

								}/*End if*/

								((aCrespCodeList *)(tmpaCexpDest.pPayload))->push_back(pI);

							}/*End for*/
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
						{
							tmpaCexpDest.pPayload = new aCmenuList;
							fill_menu_item_list((aCmenuList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).menuItemsList);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_DEFINITION:
						break;

					case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
						break;

					case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
						{
							tmpaCexpDest.pPayload = new aCmemberElementList;
							fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).itemArrElmnts);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
						{
							tmpaCexpDest.pPayload = new aCmemberElementList;
							fill_member_element_list((aCmemberElementList *) (tmpaCexpDest.pPayload),pCond->Vals.at(iValueIndex).itemArrElmnts);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_EXPRESSION:
						{
							tmpaCexpDest.pPayload = new aCExpressionPayload();
							fill_expression(&(((aCExpressionPayload *)tmpaCexpDest.pPayload)->theExpression),pCond->Vals.at(iValueIndex).pExpr);
						}
						break;

					case	DDL_ATTR_DATA_TYPE_MIN_MAX:
						{
//error							tmpaCexpDest.pPayload = new aCminmaxList();
//   							fill_min_max_list((aCminmaxList *)tmpaCexpDest.pPayload,pCond->Vals.at(iValueIndex).minMaxList);
						}
						break;
					case	DDL_ATTR_DATA_TYPE_GRID_SET:
						{
							tmpaCexpDest.pPayload = new aCgridMemberList;
							fill_grid_member_list((aCgridMemberList *) (tmpaCexpDest.pPayload), pCond->Vals.at(iValueIndex).gridMemList);
						}
					break;
					case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
					case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
					case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
					case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
					case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
					case	DDL_ATTR_DATA_TYPE_PARAM:
					case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
						LOGIT(CLOG_LOG,"fill_conditional_attributes_list (CASE) - New (unimplemented) type\n");
						break;

					default:
						LOGIT(CERR_LOG, "fill_conditional_attributes_list (CASE) - Unknown data type\n");
						/*Surprise!!!*/
						break;
				}/*End switch*/

				caseGenCond.destElements.push_back(tmpaCexpDest);
				((aCcondList *)tmpaCexpressDest.pCondDest)->genericlist.push_back(caseGenCond);
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
			}
			else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL) //Vibhor 200105: Added
			{ //Nested Conditional
				fill_conditional_attributes_list(((aCcondList *)(tmpaCexpressDest.pCondDest)),
						pCond->listOfChilds.at(iChildIndex));
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
				iChildIndex++;
			}  
			else  //Vibhor 200105: Added
			{//this section of conditional has chunks
				fill_section_chunks(((aCcondList *)(tmpaCexpressDest.pCondDest)),
							 pCond->listOfChunks.at(iChunkIndex));
				pgenCond.destElements.push_back(tmpaCexpressDest);
				tmpaCexpressDest.clear();
				iChunkIndex++;
			}
			iSectionIndex++;
			iValueIndex++;
		}

	}
		paCond->genericlist.push_back(pgenCond);
	
	/* End of secondary conditional*/
}


void fill_conditional_chunks(aCcondList* paCond,DDlAttribute *pAttr)
{
	int iDirectValIndex = 0;
	int iCondValIndex = 0;

	for(int i = 0; i < pAttr->byNumOfChunks; i++)
	{
		if(pAttr->isChunkConditionalList.at(i) == DDL_SECT_TYPE_CONDNL) /*If conditional chunk*/
		{
			fill_conditional_attributes_list(paCond,pAttr->conditionalVals.at(iCondValIndex));
			iCondValIndex++;
		}/*Endif Conditional chunk */
		else
		{/*Direct chunk*/
			aCgenericConditional pgenCond;
			pgenCond.destElements.clear();
			pgenCond.priExprType = eT_Direct;
			aCgenericConditional::aCexpressDest tempaCDest;
			tempaCDest.destType = eT_Direct;

			switch(pAttr->attrDataType)
			{
				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					{
						tempaCDest.pPayload = new aCenumList;
						fill_enumlist((aCenumList *)tempaCDest.pPayload,pAttr->directVals.at(iDirectValIndex).enmList);
						pgenCond.destElements.push_back(tempaCDest);

					}
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					{
						tempaCDest.pPayload = new aCreferenceList;
						fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),pAttr->directVals.at(iDirectValIndex).refList);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					//dump_transaction_list(fout,pAttr->directVals.at(iDirectValIndex).transList);
					break;
				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					{
						tempaCDest.pPayload = new aCrespCodeList;
						RESPONSE_CODE_LIST :: iterator p;
						RESPONSE_CODE*	pRespCode;
						RESPONSE_CODE_LIST *respList = pAttr->directVals.at(iDirectValIndex).respCdList;

						if(respList->size() == 0)
							return;

						aCrespCodeList pL;
						aCrespCode pI;
						
						for(p = respList->begin();p != respList->end();p++)
						{
							pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
							pI.type = pRespCode->type;
							pI.val  = pRespCode->val;
							copy_ddlstring(&pI.descS,&(pRespCode->desc));

							if(pRespCode->evaled & RS_HELP_EVALED)
							{
								copy_ddlstring(&pI.helpS,&(pRespCode->help));

							}/*End if*/

							((aCrespCodeList *)(tempaCDest.pPayload))->push_back(pI);

						}/*End for*/

						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					{
						tempaCDest.pPayload = new aCmenuList;
						fill_menu_item_list(((aCmenuList *)tempaCDest.pPayload),pAttr->directVals.at(iDirectValIndex).menuItemsList);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					{
						tempaCDest.pPayload = new aCmemberElementList;
						fill_member_element_list(((aCmemberElementList *)tempaCDest.pPayload),pAttr->directVals.at(iDirectValIndex).itemArrElmnts);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;	
				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					{
						tempaCDest.pPayload = new aCmemberElementList;
						fill_member_element_list(((aCmemberElementList *)tempaCDest.pPayload),pAttr->directVals.at(iDirectValIndex).itemArrElmnts);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					{
//error						tempaCDest.pPayload = new aCminmaxList();
//  						fill_min_max_list((aCminmaxList *)tempaCDest.pPayload,pAttr->directVals.at(iDirectValIndex).minMaxList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
					{
						tempaCDest.pPayload = new aCgridMemberList();
						fill_grid_member_list((aCgridMemberList *)tempaCDest.pPayload,
												pAttr->directVals.at(iDirectValIndex).gridMemList);
						pgenCond.destElements.push_back(tempaCDest);// stevev 20may05
					}
					break;
				default:
					LOGIT(CERR_LOG, "fill_conditional_chunks - Unknown data type\n");
					break;
			
			}/*End switch*/	
			
			iDirectValIndex++;
		
			paCond->genericlist.push_back(pgenCond);
		}/*End else*/

	}/*Endfor i*/
	
}/*End fill_conditional_chunks*/

/*Vibhor 200105: Start of Code*/
/*Adding this function to handle conditional chunks lists in sections of conditionals*/

void fill_section_chunks(aCcondList* paCond,DDlSectionChunks *pSecChunk)
{
	int iDirectValIndex = 0;
	int iCondValIndex = 0;

	for(int i = 0; i < pSecChunk->byNumOfChunks; i++)
	{
		if(pSecChunk->isChunkConditionalList.at(i) == DDL_SECT_TYPE_CONDNL) /*If conditional chunk*/
		{
			fill_conditional_attributes_list(paCond,pSecChunk->conditionalVals.at(iCondValIndex));
			iCondValIndex++;
		}/*Endif Conditional chunk */
		else
		{/*Direct chunk*/
			aCgenericConditional pgenCond;
			pgenCond.destElements.clear();
			pgenCond.priExprType = eT_Direct;
			aCgenericConditional::aCexpressDest tempaCDest;
			tempaCDest.destType = eT_Direct;

			switch(pSecChunk->attrDataType)
			{
				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					{
						tempaCDest.pPayload = new aCenumList;
						fill_enumlist((aCenumList *)tempaCDest.pPayload,pSecChunk->directVals.at(iDirectValIndex).enmList);
						pgenCond.destElements.push_back(tempaCDest);

					}
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					{
						tempaCDest.pPayload = new aCreferenceList;
						fill_reference_list(((aCreferenceList *)tempaCDest.pPayload),pSecChunk->directVals.at(iDirectValIndex).refList);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					//dump_transaction_list(fout,pSecChunk->directVals.at(iDirectValIndex).transList);
					break;
				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					{
						tempaCDest.pPayload = new aCrespCodeList;
						RESPONSE_CODE_LIST :: iterator p;
						RESPONSE_CODE*	pRespCode;
						RESPONSE_CODE_LIST *respList = pSecChunk->directVals.at(iDirectValIndex).respCdList;

						if(respList->size() == 0)
							return;

						aCrespCodeList pL;
						aCrespCode pI;
						
						for(p = respList->begin();p != respList->end();p++)
						{
							pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
							pI.type = pRespCode->type;
							pI.val  = pRespCode->val;
							copy_ddlstring(&pI.descS,&(pRespCode->desc));

							if(pRespCode->evaled & RS_HELP_EVALED)
							{
								copy_ddlstring(&pI.helpS,&(pRespCode->help));

							}/*End if*/

							((aCrespCodeList *)(tempaCDest.pPayload))->push_back(pI);

						}/*End for*/

						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					{
						tempaCDest.pPayload = new aCmenuList;
						fill_menu_item_list(((aCmenuList *)tempaCDest.pPayload),pSecChunk->directVals.at(iDirectValIndex).menuItemsList);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					{
						tempaCDest.pPayload = new aCmemberElementList;
						fill_member_element_list(((aCmemberElementList *)tempaCDest.pPayload),pSecChunk->directVals.at(iDirectValIndex).itemArrElmnts);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;	
				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					{
						tempaCDest.pPayload = new aCmemberElementList;
						fill_member_element_list(((aCmemberElementList *)tempaCDest.pPayload),pSecChunk->directVals.at(iDirectValIndex).itemArrElmnts);
						pgenCond.destElements.push_back(tempaCDest);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					{
//error						tempaCDest.pPayload = new aCminmaxList();
//  						fill_min_max_list((aCminmaxList *)tempaCDest.pPayload,pSecChunk->directVals.at(iDirectValIndex).minMaxList);
					}
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:	/* added - stevev 20may05 */
					{
						tempaCDest.pPayload = new aCgridMemberList();
						fill_grid_member_list((aCgridMemberList *)tempaCDest.pPayload,
												pSecChunk->directVals.at(iDirectValIndex).gridMemList);
						pgenCond.destElements.push_back(tempaCDest);// stevev 20may05
					}
					break;
				default:
					LOGIT(CERR_LOG, "fill_section_chunks - Unknown data type\n");
					break;
			
			}/*End switch*/	
			
			iDirectValIndex++;
		
			paCond->genericlist.push_back(pgenCond);
		}/*End else*/

	}/*Endfor i*/
	
}/*End fill_section_chunks()*/


/*Vibhor 200105: End of Code*/

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_Payload(aCgenericConditional::aCexpressDest *tmpaCexpressDest,DDlAttribute * paAttr)
{
	/*switch to the appropriate data type & simply dump it!!*/
	switch(paAttr->attrDataType)
	{
		case 	DDL_ATTR_DATA_TYPE_INT:
			tmpaCexpressDest->pPayload = new aCddlLong();
			((aCddlLong *)tmpaCexpressDest->pPayload)->ddlLong = paAttr->pVals->ullVal;
			break;

		case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
		case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
		case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
		case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
		case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
			tmpaCexpressDest->pPayload = new aCddlLong();
			((aCddlLong *)tmpaCexpressDest->pPayload)->ddlLong = paAttr->pVals->ullVal;
			break;

		case	DDL_ATTR_DATA_TYPE_FLOAT:
		case	DDL_ATTR_DATA_TYPE_DOUBLE:
			break;

		case	DDL_ATTR_DATA_TYPE_BITSTRING :

			tmpaCexpressDest->pPayload = new aCbitString();
			// bitstring is limited to 32 bits
			((aCbitString *)tmpaCexpressDest->pPayload)->bitstringVal = 
				(unsigned long)paAttr->pVals->ullVal;
			break;

		case	DDL_ATTR_DATA_TYPE_STRING:
		
			tmpaCexpressDest->pPayload = new aCddlString();
			copy_ddlstring(((aCddlString *)tmpaCexpressDest->pPayload),
															   paAttr->pVals->strVal);
			break;

		case	DDL_ATTR_DATA_TYPE_ITEM_ID:
			tmpaCexpressDest->pPayload = new aCddlLong();
			((aCddlLong *)tmpaCexpressDest->pPayload)->ddlLong = paAttr->pVals->ullVal;
			break;

		case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_REFERENCE :
			{
				tmpaCexpressDest->pPayload = new aCreference;
				aCreference *tmpaCref = (aCreference *) (tmpaCexpressDest->pPayload);
				ddpREFERENCE :: iterator p;
				BYTE makeTmpNull = 0;
				
				for(p = paAttr->pVals->ref->begin();p != paAttr->pVals->ref->end();p++)
				{
						/* If not the first time allocate memory */
					if(makeTmpNull)
					{
						tmpaCref->pRef = new aCreference;
						tmpaCref = tmpaCref->pRef;
					}

					// CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
					// when its destructor is called and cleans own data up (included the 
					//                                           content of the pointed vector)
					/* 
					#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
						ddpREF myRef;
						myRef.type = p->type;
						myRef.val  = p->val;
						fill_reference(tmpaCref, &myRef);
					#else
					fill_reference(tmpaCref,p);
					#endif
					*/

					// CodeWrights [PC 22] replaced with:
					fill_reference(tmpaCref, (ddpREF *)&(*p));

					makeTmpNull = 1;
				}//next
			}
			break;

		case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
			break;

		case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_DEFINITION:
			break;
		
		case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
			break;

		case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
			break;

		case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
			break;

		case	DDL_ATTR_DATA_TYPE_EXPRESSION:
			{
				tmpaCexpressDest->pPayload = new aCExpressionPayload();
				if ( pBuilt == NULL )
					pBuilt = tmpaCexpressDest->pPayload;

				fill_expression(&(
					((aCExpressionPayload*)(tmpaCexpressDest->pPayload))->theExpression
								 )
								 ,paAttr->pVals->pExpr);
			}
			break;

		case	DDL_ATTR_DATA_TYPE_MIN_MAX:
			{
//error			tmpaCexpressDest->pPayload = new aCminmaxList();
//     fill_min_max_list((aCminmaxList *)tmpaCexpressDest->pPayload,paAttr->pVals->minMaxList);
			}
			break;
		case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
			{
				tmpaCexpressDest->pPayload = new aCintWhich ();//();
				((aCintWhich*)tmpaCexpressDest->pPayload)->value = paAttr->pVals->lineType.type;
				((aCintWhich*)tmpaCexpressDest->pPayload)->which = paAttr->pVals->lineType.qual;
			}
		break;
		case	DDL_ATTR_DATA_TYPE_GRID_SET:// list
		case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
		case	DDL_ATTR_DATA_TYPE_PARAM:
		case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
			LOGIT(CLOG_LOG,"fill_Payload - New (unimplemented) type\n");
			break;	
		default:
			LOGIT(CERR_LOG, "fill_Payload - Unknown data type\n");
			/*Surprise!!!*/
			break;
	}/*End switch*/
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_expression(aCexpression *aExpr,ddpExpression *pExpr)
{	
	ddpExpression :: iterator e;
	Element element;
	int i = 0;
	
	for(e = pExpr->begin(); e != pExpr->end(); e++)
	{
		aCexpressElement pexpressElmnt;
		pexpressElmnt.clear();
		element = *e;
		pexpressElmnt.exprElemType = (expressionElementType_e) element.byElemType;
		pexpressElmnt.dependency.which = 0;
		pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
		pexpressElmnt.dependency.depRef.isRef = FALSE;
		switch(pexpressElmnt.exprElemType)
		{

			case	NOT_OPCODE:				/* 1 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_NOT;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	NEG_OPCODE:				/* 2 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_NEG;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	BNEG_OPCODE:			 /* 3 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_BNEG;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	ADD_OPCODE:				/* 4 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_ADD;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	SUB_OPCODE:				 /* 5 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_SUB;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	MUL_OPCODE:				/* 6 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_MUL;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	DIV_OPCODE:				/* 7 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_DIV;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	MOD_OPCODE:				/* 8 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_MOD;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	LSHIFT_OPCODE:			/* 9 */ 
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_LSHIFT;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	RSHIFT_OPCODE:			/* 10 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_RSHIFT;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	AND_OPCODE:				/* 11 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_AND;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	OR_OPCODE:				/* 12 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_OR;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	XOR_OPCODE:				/* 13 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_XOR;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	LAND_OPCODE:			/* 14 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_LAND;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	LOR_OPCODE:				/* 15 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_LOR;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	LT_OPCODE:				/* 16 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_LT;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	GT_OPCODE:				/* 17 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_GT;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	LE_OPCODE:				/* 18 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_LE;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	GE_OPCODE:				/* 19 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_GE;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	EQ_OPCODE:				/* 20 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_EQ;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	NEQ_OPCODE:				/* 21 */
				pexpressElmnt.expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt.expressionData.vValue.iOpCode = eet_NEQ;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				break;
			case	INTCST_OPCODE:			/* 22 */
				pexpressElmnt.expressionData = element.elem.ulConst;
				break;
			case	FPCST_OPCODE:			/* 23 */
				pexpressElmnt.expressionData = element.elem.fConst;
				break;
			case	VARID_OPCODE:			/* 24 */
				pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt.expressionData.vValue.depIndex = 0;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				pexpressElmnt.dependency.depRef.rtype = 0;
				pexpressElmnt.dependency.depRef.id =	element.elem.varId;
				pexpressElmnt.dependency.depRef.type = 1;
				break;
			case	MAXVAL_OPCODE:			/* 25 */
// VMKP Added on 100404
				pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt.expressionData.vValue.depIndex = 0;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				pexpressElmnt.dependency.useOptionVal = aOp_wantMax;
				pexpressElmnt.dependency.which = element.elem.minMax->which;
				pexpressElmnt.dependency.depRef.rtype = 0;
				pexpressElmnt.dependency.depRef.id =	element.elem.minMax->variable.id;
				pexpressElmnt.dependency.depRef.type = 1;
// VMKP Added on 100404
				break;
			case	MINVAL_OPCODE:			/* 26 */
// VMKP Added on 100404
				pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt.expressionData.vValue.depIndex = 0;
				pexpressElmnt.expressionData.sStringVal.erase();
				pexpressElmnt.expressionData.vIsUnsigned = false;
				pexpressElmnt.expressionData.vSize       =4; 
				pexpressElmnt.expressionData.vIsValid    =true;
				pexpressElmnt.dependency.useOptionVal = aOp_wantMin;
				pexpressElmnt.dependency.which = element.elem.minMax->which;
				pexpressElmnt.dependency.depRef.rtype = 0;
				pexpressElmnt.dependency.depRef.id =	element.elem.minMax->variable.id;
				pexpressElmnt.dependency.depRef.type = 1;
// VMKP Added on 100404
				break;
			case	VARREF_OPCODE:			/* 27 */
				{
// VMKP Added on 100404
					pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt.expressionData.vValue.depIndex = 0;
					pexpressElmnt.expressionData.sStringVal.erase();
					pexpressElmnt.expressionData.vIsUnsigned = false;
					pexpressElmnt.expressionData.vSize       =4; 
					pexpressElmnt.expressionData.vIsValid    =true;
					aCreference *tmpaCref = &pexpressElmnt.dependency.depRef;
					ddpREFERENCE :: iterator p;
					BYTE makeTmpNull = 0;

					for(p = element.elem.varRef->begin();p != element.elem.varRef->end();p++)
					{
					/* If not the first time allocate memory */
						if(makeTmpNull)
						{
							pexpressElmnt.dependency.depRef.isRef = TRUE;
							tmpaCref->pRef = new aCreference;
							tmpaCref = tmpaCref->pRef;
						}
                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = p->type;
							myRef.val  = p->val;
							fill_reference(tmpaCref, &myRef);
						#else
						fill_reference(tmpaCref,p);
						#endif
                        */

                        // CodeWrights [PC 22] replaced with:
						fill_reference(tmpaCref, (ddpREF *)&(*p));

						makeTmpNull = 1;
					}	
				}
// VMKP Added on 100404
				break;
			case	MAXREF_OPCODE:	
				/* 28 */
				{
// VMKP Added on 100404
					pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt.expressionData.vValue.depIndex = 0;
					pexpressElmnt.expressionData.sStringVal.erase();
					pexpressElmnt.expressionData.vIsUnsigned = false;
					pexpressElmnt.expressionData.vSize       =4; 
					pexpressElmnt.expressionData.vIsValid    =true;
					pexpressElmnt.dependency.which = element.elem.minMax->which;
					pexpressElmnt.dependency.useOptionVal = aOp_wantMax;

					aCreference *tmpaCref = &pexpressElmnt.dependency.depRef;
					ddpREFERENCE :: iterator p;
					BYTE makeTmpNull = 0;

					for(p = element.elem.minMax->variable.ref->begin();
					    p != element.elem.minMax->variable.ref->end();p++)
					{
					/* If not the first time allocate memory */
						if(makeTmpNull)
						{
							pexpressElmnt.dependency.depRef.isRef = TRUE;
							tmpaCref->pRef = new aCreference;
							tmpaCref = tmpaCref->pRef;
						}

                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = p->type;
							myRef.val  = p->val;
							fill_reference(tmpaCref, &myRef);
						#else
						fill_reference(tmpaCref,p);
						#endif
*/
                        // CodeWrights [PC 22] replaced with:
						fill_reference(tmpaCref, (ddpREF *)&(*p));

						makeTmpNull = 1;
					}	
				}
// VMKP Added on 100404
				break;
			case	MINREF_OPCODE:			/* 29 */
				{
// VMKP Added on 100404
					pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt.expressionData.vValue.depIndex = 0;
					pexpressElmnt.expressionData.sStringVal.erase();
					pexpressElmnt.expressionData.vIsUnsigned = false;
					pexpressElmnt.expressionData.vSize       =4; 
					pexpressElmnt.expressionData.vIsValid    =true;
					pexpressElmnt.dependency.which = element.elem.minMax->which;
					pexpressElmnt.dependency.useOptionVal = aOp_wantMin;

					aCreference *tmpaCref = &pexpressElmnt.dependency.depRef;
					ddpREFERENCE :: iterator p;
					BYTE makeTmpNull = 0;

					for(p = element.elem.minMax->variable.ref->begin();
					    p != element.elem.minMax->variable.ref->end();p++)
					{
					/* If not the first time allocate memory */
						if(makeTmpNull)
						{
							pexpressElmnt.dependency.depRef.isRef = TRUE;
							tmpaCref->pRef = new aCreference;
							tmpaCref = tmpaCref->pRef;
						}

                        // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                        // when its destructor is called and cleans own data up (included the content of the pointed vector)
                        /* 
						#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
							ddpREF myRef;
							myRef.type = p->type;
							myRef.val  = p->val;
							fill_reference(tmpaCref, &myRef);
						#else
						fill_reference(tmpaCref,p);
						#endif
                        */

                        // CodeWrights [PC 22] replaced with:
						fill_reference(tmpaCref, (ddpREF *)&(*p));

						makeTmpNull = 1;
					}	
				}
// VMKP Added on 100404
				break;
			case	BLOCK_OPCODE:			/* 30 */
			case	BLOCKID_OPCODE:			/* 31 */
			case	BLOCKREF_OPCODE:		/* 32 */
				break;
			case	STRCST_OPCODE:			/* 33 */
				{
			
					pexpressElmnt.expressionData = element.elem.pSTRCNST->str;//.ulConst;
			
				}
				break;
/** these are now handled through a reference by attribute 
			case	CNTREF_OPCODE:			/x* 35 *x/
			case	CAPREF_OPCODE:			/x* 36 *x/
			case	FSTREF_OPCODE:			/x* 37 *x/
			case	LSTREF_OPCODE:			/x* 38 *x/
			// added by stevev 18aug06 
				eet_DFLT_VALREF			//DFLTVAL_OPCODE 39
				eet_VIEW_MINREF			//VMIN_OPCODE    40
				eet_VIEW_MAXREF			//VMAX_OPCODE    41
			// end added by sjv
				{				
					pexpressElmnt.expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt.expressionData.vValue.depIndex = 0;
					pexpressElmnt.expressionData.sStringVal.erase();
					pexpressElmnt.expressionData.vIsUnsigned = false;
					pexpressElmnt.expressionData.vSize       =4; 
					pexpressElmnt.expressionData.vIsValid    =true;
					aCreference *tmpaCref = &pexpressElmnt.dependency.depRef;
					ddpREFERENCE :: iterator p;
					BYTE makeTmpNull = 0;

					for(p = element.elem.varRef->begin();p != element.elem.varRef->end();p++)
					{
					/x* If not the first time allocate memory *x/
						if(makeTmpNull)
						{
							pexpressElmnt.dependency.depRef.isRef = TRUE;
							tmpaCref->pRef = new aCreference;
							tmpaCref = tmpaCref->pRef;
						}
						fill_reference(tmpaCref,p);
						makeTmpNull = 1;
					}	
				}
				break;
*** end 21apr05 stevev move to reference ***/
			case	SYSTEMENUM_OPCODE:		/* 34 */
				/* the string constant was the image filename - should not be here */
				break;
			default:
				break;

		}/*End switch*/
		
		aExpr->expressionL.push_back(pexpressElmnt);
	}
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
void copy_ddlstring(aCddlString *tmpaCddlstrng,ddpSTRING *strng)
{
	tmpaCddlstrng->ddKey = 0;
	
//	if(strng->strType == DEV_SPEC_STRING_TAG)
//		tmpaCddlstrng->flags = 0;
//	else
	tmpaCddlstrng->flags = strng->flags;

	tmpaCddlstrng->len   = strng->len;
	tmpaCddlstrng->strTag = strng->strType;

//	if(tmpaCddlstrng->strTag == ENUMERATION_STRING_TAG\)
	tmpaCddlstrng->enumVal = strng->enumStr.enumValue;

//	if(tmpaCddlstrng->strTag == VARIABLE_STRING_TAG)
	tmpaCddlstrng->ddItem = strng->varId;

	tmpaCddlstrng->pRef   = NULL;//stevev
#ifdef _DEBUG
	tmpaCddlstrng->pDBGRef   = NULL;
#endif

	if (strng->str != NULL)
	{
		//tmpaCddlstrng->theStr = strng->str;
		// timj 9jan08  convert the str to wide char, then filter for current language
		pPIdict->get_string_translation(UTF82Unicode(strng->str), tmpaCddlstrng->theStr);
		tmpaCddlstrng->flags |=  0x01;

		// stevev 30apr08 - these will never be the same - there is no longer any use to compare
		//if (tmpaCddlstrng->theStr.length() !=  strng->len)
		//{
		//	LOGIT(CLOG_LOG,"ERROR: string length mismatch. incoming= 0x%02x, actual 0x%02x\n",
		//											   strng->len,tmpaCddlstrng->theStr.length());
			tmpaCddlstrng->len = tmpaCddlstrng->theStr.length();
		//}		

		if (strng->str[0] == '\0')
		{// stevev added to get a non-null empty string 14nov05
			tmpaCddlstrng->theStr = L" ";// stevev
			tmpaCddlstrng->theStr.erase();// stevev - not a NULL string
		}
	}
	else
	if (tmpaCddlstrng->strTag == ds_Dict)
	{
		tmpaCddlstrng->theStr = L" ";// stevev
		tmpaCddlstrng->theStr.erase();// stevev - not a NULL string
	}
	else
	{
		tmpaCddlstrng->theStr = L"";// stevev
	}
	
	if (strng->strType ==DEV_SPEC_STRING_TAG)
	{
//		tmpaCddlstrng->ddKey = pAbstractDev->DDkey;
	}
	else if (strng->strType == ENUMERATION_STRING_TAG)
	{
			aCreference *tmpaCref = new aCreference;
REGISCHUNK(tmpaCref, 3);// supposed to be leaking this reference
#ifdef _DEBUG
tmpaCddlstrng->pDBGRef = tmpaCref;
#endif
			tmpaCddlstrng->pRef = tmpaCref;
			tmpaCref->rtype = 0x00; //ITEM_ID
			tmpaCref->id = strng->enumStr.enmVar.iD;
			tmpaCddlstrng->flags |= 0x4000;
			const size_t ENMSTR_LEN = 100;
			wchar_t enmstr[ENMSTR_LEN];
			swprintf(
				enmstr,
#if defined(__GNUC__)
				ENMSTR_LEN,
#endif
				L"++Enum String++id:0x%x  val:0x%x",
				strng->enumStr.enmVar.iD,
				strng->enumStr.enumValue
			);
			tmpaCddlstrng->theStr = enmstr; // stevev 23jan08...not...UTF82Unicode(enmstr);
	}
	else if (strng->strType == ENUM_REF_STRING_TAG)
	{
		aCreference *tmpaCref = new aCreference;
REGISCHUNK(tmpaCref, 4);
		ddpREFERENCE :: iterator p;
		tmpaCddlstrng->pRef = tmpaCref;
		BYTE makeTmpNull = 0;

		for(p = strng->enumStr.enmVar.ref->begin();p != strng->enumStr.enmVar.ref->end();p++)
		{
		/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}

            // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
            // when its destructor is called and cleans own data up (included the content of the pointed vector)
            /* 
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				ddpREF myRef;
				myRef.type = p->type;
				myRef.val  = p->val;
				fill_reference(tmpaCref, &myRef);
			#else
			fill_reference(tmpaCref,p);
			#endif
            */

            // CodeWrights [PC 22] replaced with:
			fill_reference(tmpaCref, (ddpREF *)&(*p));

			makeTmpNull = 1;
		}	
	}
	else if (strng->strType == VAR_REF_STRING_TAG)
	{
		aCreference *tmpaCref = new aCreference;
REGISCHUNK(tmpaCref, 5);
		ddpREFERENCE :: iterator p;
		tmpaCddlstrng->pRef = tmpaCref;
		BYTE makeTmpNull = 0;

		for(p = strng->varRef.begin();p != strng->varRef.end();p++)
		{
		/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}

            // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
            // when its destructor is called and cleans own data up (included the content of the pointed vector)
            /* 
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				ddpREF myRef;
				myRef.type = p->type;
				myRef.val  = p->val;
				fill_reference(tmpaCref, &myRef);
			#else
			fill_reference(tmpaCref,p);
			#endif
            */

            // CodeWrights [PC 22] replaced with:
			fill_reference(tmpaCref, (ddpREF *)&(*p));

			makeTmpNull = 1;
		}	
	}
	tmpaCddlstrng->isSet = true;
#ifdef _DEBUG
	if(tmpaCddlstrng->theStr.length() <0 || tmpaCddlstrng->theStr.length() > 2048)
	{
		LOGIT(CERR_LOG,"ERROR: string length incorrect.(0x%02x)(debug only msg)\n",tmpaCddlstrng->theStr.length());
	}
#endif

}

/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_reference(aCreference *aCref,ddpREF *p)
{
		aCref->rtype = (unsigned int) p->type;
		switch(p->type)
		{
			case	ITEM_ID_REF:
					aCref->type = VARIABLE_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case 	ITEM_ARRAY_ID_REF:
					aCref->type = ITEM_ARRAY_ITYPE; // stevev 24feb06 changed from:: VARIABLE_ITYPE;
					aCref->isRef = 1;
					aCref->id = p->val.id;
				break;
			case	COLLECTION_ID_REF:
					aCref->type = COLLECTION_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	BLOCK_ID_REF:
					aCref->type = BLOCK_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	VARIABLE_ID_REF:
					aCref->type = VARIABLE_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	MENU_ID_REF:
					aCref->type = MENU_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	EDIT_DISP_ID_REF:
					aCref->type = EDIT_DISP_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	METHOD_ID_REF:
					aCref->type = METHOD_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	REFRESH_ID_REF:
					aCref->type = REFRESH_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	UNIT_ID_REF:
					aCref->type = UNIT_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	WAO_ID_REF:
					aCref->type = WAO_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	RECORD_ID_REF:
					aCref->type = RECORD_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	ARRAY_ID_REF:
					aCref->type = ARRAY_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	BLOB_ID_REF:
					aCref->type = BLOB_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	RESP_CODES_ID_REF:
					aCref->type = RESP_CODES_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	VIA_ARRAY_REF:
			case	VIA_ITEM_ARRAY_REF:
			case	VIA_LIST_REF:
				{
					if(p->type == VIA_ARRAY_REF)
					{
						aCref->type = ARRAY_ITYPE;
					}
					else if(p->type == VIA_ITEM_ARRAY_REF)
					{
						aCref->type = ITEM_ARRAY_ITYPE;
					}
					else //VIA_LIST_REF
					{
						aCref->type = VIA_LIST_REF;
					}
 					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					fill_expression(aCref->pExpr,p->val.index);
				}
				break;
			case	VIA_BITENUM_REF:
			case	VIA_COLLECTION_REF:
			case    VIA_FILE_REF:
				{
					if(p->type == VIA_BITENUM_REF)
					{
						aCref->type = VARIABLE_ITYPE;
					}
					else if(p->type == VIA_COLLECTION_REF)
					{
						aCref->type = COLLECTION_ITYPE;
					}
					else  //VIA_FILE_REF
					{
						aCref->type = FILE_ITYPE;
					}
					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					pexpressElmnt.expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);
				}

				break;
			case	VIA_RECORD_REF:	
					aCref->type = RECORD_ITYPE;
					aCref->isRef = 0;
					aCref->iName = p->val.member;
				break;
			case	VIA_PARAM_REF:
					aCref->type = VARIABLE_ITYPE;
					aCref->isRef = 0;
					aCref->iName = p->val.member;
				break;
			case	VIA_PARAM_LIST_REF:
					aCref->type = VAR_LIST_ITYPE;
					aCref->isRef = 0;
					aCref->iName = p->val.member;
				break;
			case	VIA_BLOCK_REF:
					aCref->type = BLOCK_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	FILE_ID_REF:	
					aCref->type = FILE_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	CHART_ID_REF:
					aCref->type = CHART_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	GRAPH_ID_REF:
					aCref->type = GRAPH_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	AXIS_ID_REF:
					aCref->type = AXIS_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	WAVEFORM_ID_REF:
					aCref->type = WAVEFORM_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	SOURCE_ID_REF:
					aCref->type = SOURCE_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	LIST_ID_REF	:
					aCref->type = LIST_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	GRID_ID_REF:
					aCref->type = GRID_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	IMAGE_ID_REF:
					aCref->type = IMAGE_ITYPE;
					aCref->isRef = 0;
					aCref->id = p->val.id;
				break;
			case	CONSTANT_REF:	
					aCref->type = MAX_ITYPE;
					aCref->isRef = 0;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					fill_expression(aCref->pExpr,p->val.index);
				break;
			case	SEPARATOR_REF:
					aCref->type = MAX_ITYPE;
					aCref->isRef = 0;
					aCref->id    = 0;//p->val.id; // should be zero
					aCref->iName = 0;//p->val.member; // should be NULL
					aCref->exprType = eT_Unknown;
					aCref->pExpr = NULL;
				break;
			case	ROWBREAK_REF:
					aCref->type = MAX_ITYPE;
					aCref->isRef = 0;
					aCref->id    = 0;
					aCref->iName = 0;
					aCref->exprType = eT_Unknown;
					aCref->pExpr = NULL;
				break;
				
			case	VIA_CHART_REF:
				{
					aCref->type = SOURCE_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					pexpressElmnt.expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);
				}
				break;
			case	VIA_GRAPH_REF:
				{
					aCref->type = WAVEFORM_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					pexpressElmnt.expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);
				}
				break;
			case	VIA_SOURCE_REF:
				{
					aCref->type = VARIABLE_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					pexpressElmnt.expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);
				}
				break;
			case    VIA_BLOB_REF:
				{
					// TODO: implement this to get to atributes 
				}
				break;
			case	VIA_ATTR_REF:
				{
					aCref->type = MAX_ITYPE; /* type cannot be accerned */
				
					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
/* stevev 21aug06 - now that the tokenizer is putting 'em in right, get 'em out right
					pexpressElmnt.expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
*/					pexpressElmnt.expressionData   = (unsigned int) (p->val.member & 0x0000ffff);
					pexpressElmnt.dependency.which = (unsigned int) ((p->val.member & 0xffff0000)>>16);

					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);
				}
				break;

	  		default:
				break;
		}/*End switch*/
}


/****************************************************************************************/
/* F */


/********************************************************************************************/


void fill_enumlist(aCenumList *tmpenumList,ENUM_VALUE_LIST *enmList)
{
	ENUM_VALUE_LIST :: iterator p;
	
	aCenumDesc tmpaCenumDesc;
	for(p = enmList->begin(); p != enmList->end(); p++)
	{
			ENUM_VALUE		enmVal = *p;
			// move it outside  aCenumDesc tmpaCenumDesc;
			tmpaCenumDesc.clear();
			tmpaCenumDesc.val = enmVal.val;
			tmpaCenumDesc.func_class.bitstringVal = enmVal.func_class;
#ifdef _DEBUG
if (enmVal.desc.strType == DEV_SPEC_STRING_TAG || enmVal.desc.strType == DICTIONARY_STRING_TAG)
{
	if (enmVal.desc.len == 0 || (enmVal.desc.str != NULL && strlen(enmVal.desc.str) == 0) ) 
	{
		LOGIT(CLOG_LOG,"WARN Enum description string is empty coming out of the parser.\n");
	}
	if (enmVal.desc.len != strlen(enmVal.desc.str) )
	{
		LOGIT(CLOG_LOG,"Enum description length mismatch.\n");
	}
}
else if (enmVal.desc.str != NULL )
{
		LOGIT(CLOG_LOG,"Enum description type.vs.strPtr mismatch.\n");
}
#endif
			copy_ddlstring(&tmpaCenumDesc.descS,&enmVal.desc);
			copy_ddlstring(&tmpaCenumDesc.helpS,&enmVal.help);
#ifdef _STRMEMTESTING
			gblLstOfSTRING.push_back(&(p->desc));
			gblLstOfSTRING.push_back(&(p->help));
#endif

			tmpaCenumDesc.actions.id = enmVal.actions;
			tmpaCenumDesc.status_class = enmVal.status.status_class;

			OUTPUT_STATUS_LIST :: iterator n;
			OUTPUT_STATUS*     pOStat;
			outputStatus_t outputstatus;
			
			if(enmVal.status.oclasses.size() > 0)
			{
				for (n = enmVal.status.oclasses.begin(); n != enmVal.status.oclasses.end(); n++)
				{
					pOStat = (OUTPUT_STATUS*)&(*n);
					outputstatus.kind = pOStat->kind;
					outputstatus.which = pOStat->which;
					outputstatus.oclass = pOStat->oclass;
					tmpaCenumDesc.oclasslist.push_back(outputstatus);
					outputstatus.clear();// stevev
				}
			}
			else
			{
				tmpaCenumDesc.oclasslist.clear();//stevev
			}

			tmpenumList->push_back(tmpaCenumDesc);
#ifdef _DEBUG
if (enmVal.desc.strType == DEV_SPEC_STRING_TAG || enmVal.desc.strType == DICTIONARY_STRING_TAG)
{
	if (enmVal.desc.len == 0 || (enmVal.desc.str != NULL && strlen(enmVal.desc.str) == 0) ) 
	{// normally done by the dd writer - a bad habit...
		LOGIT(CLOG_LOG,"WARN: Enum description string is empty coming out of the parser.\n");
	}
	if (enmVal.desc.len != strlen(enmVal.desc.str) )
	{
		LOGIT(CLOG_LOG,"Enum description len mismatch.\n");
	}
}
else if (enmVal.desc.str != NULL )
{
		LOGIT(CLOG_LOG,"Enum description type.vs.strPtr mismatch.\n");
}
#endif
		///- the push makes a copy of the reference so a clear by itself generates a sizable leak
		tmpaCenumDesc.destroy();
		tmpaCenumDesc.clear();// add here to try and stop deleted-reference crash
	}// next
#ifdef _STRMEMTESTING
assert(areSTRINGSgood());
#endif
}

#ifdef _STRMEMTESTING
bool areSTRINGSgood()
{
	vector<ddpSTRING*>::iterator p2p2STR;
	ddpSTRING* pSTR = NULL;
	bool retval  = true;
	int itemCnt = 0;

	for (p2p2STR = gblLstOfSTRING.begin(); p2p2STR != gblLstOfSTRING.end(); ++p2p2STR,itemCnt++)
	{
		pSTR = *( (ddpSTRING**) p2p2STR);

		if (pSTR->strType == DEV_SPEC_STRING_TAG || pSTR->strType == DICTIONARY_STRING_TAG)
		{
			if (pSTR->len == 0 || (pSTR->str != NULL && strlen(pSTR->str) == 0) ) 
			{// help usually hits this
				//LOGIT(CERR_LOG,"Enum description/help empty in the hold list.\n");
				//retval = false;// for now
			}
			if (pSTR->str != NULL && pSTR->len != strlen(pSTR->str) )
			{
				LOGIT(CLOG_LOG,"Enum description len mismatch.\n");
				retval = false;
			}
		}
		else if (pSTR->str != NULL )
		{
				LOGIT(CLOG_LOG,"Enum description type.vs.strPtr mismatch.\n");
				retval = false;
		}
	}// next
	return retval;
}
#endif
/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_menu_item_list(aCmenuList *tmpmenuList,MENU_ITEM_LIST *menuList)
{
	MENU_ITEM_LIST :: iterator p;// ptr 2a MENU_ITEM
	MENU_ITEM*  pMenuItem = NULL;// stevev 06aug07 X- use pointers, not copy

	for(p = menuList->begin(); p != menuList->end(); p++)
	{
		pMenuItem = (MENU_ITEM*)&(*p);// PAW 03/03/09
		aCmenuItem tmpmenuItem;

		ddpREFERENCE :: iterator q;// ptr 2a ddpREF
		ddpREF*      pREF = NULL;
		aCreference *tmpaCref = &(tmpmenuItem.itemRef);
		BYTE makeTmpNull = 0;

		for(q = pMenuItem->item.begin();q != pMenuItem->item.end();q++)
		{
			pREF = (ddpREF*)&(*q);// PAW 03/03/09
			/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}
			/* stevev 19feb07 - odd but ok  */
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
			//	ddpREF myRef;
			//	myRef.type = q->type;
			//	myRef.val  = q->val;
				fill_reference(tmpaCref, pREF);// was::>  &myRef);
			#else
			fill_reference(tmpaCref,pREF);//q);
			#endif
			makeTmpNull = 1;
		}	

		tmpmenuItem.qualifier.bitstringVal = (unsigned long) pMenuItem->qual;
	
		tmpmenuList->push_back(tmpmenuItem);
	}

}

/****************************************************************************************/
/* F */


/********************************************************************************************/

void fill_transaction_list(aCattrCmdTrans *tmpCmdTrans,TRANSACTION_LIST* transList)
{
	TRANSACTION_LIST :: iterator p;// a ptr 2a TRANSACTION
	TRANSACTION*  pTrans = NULL;// stevev 07aug07 X- use pointers instead of copying
	int r, s = transList->size();


	for(r = 0, p = transList->begin();p != transList->end();p++,r++)
	{
		if ( r >= s )
		{
			LOGIT(CERR_LOG,"Transaction iterator went past the list size.\n");
			return;
		}
		else
		{
			pTrans = (TRANSACTION*)&(*p);// PAW 03/03/09
			aCtransaction tmpaCtrans;
			tmpaCtrans.clear();

			tmpaCtrans.transNum = pTrans->number;
			fill_data_item_list(&tmpaCtrans.request,&(pTrans->request));
			fill_data_item_list(&tmpaCtrans.response,&(pTrans->reply));
			fill_response_code_list(&tmpaCtrans.respCodes,&(pTrans->rcodes));

#ifdef XMTR
			fill_reference_list(&(tmpaCtrans.postRqstRcvAction),&(pTrans->post_rqst_rcv_act));
#endif
			tmpCmdTrans->transactionList.push_back(tmpaCtrans);
		}

	}/*Endfor p*/

}/*End fill_transaction_list*/

/****************************************************************************************/
/* F */


/********************************************************************************************/

void fill_data_item_list(aCcondList *tmpcondLst, DATA_ITEM_LIST* pDataItemList)
{
	DATA_ITEM_LIST :: iterator p;// ptr 2a DATA_ITEM
	DATA_ITEM*  pDataItem = NULL;// stevev 07aug07 X- use pointers instead of copying
	
	if(pDataItemList == NULL || pDataItemList->size() == 0)
		return;

	aCdataitemList pL;

	aCgenericConditional tmpgnCond;
	tmpgnCond.priExprType = eT_Direct;
	aCgenericConditional::aCexpressDest tmpaCexpressDest;
	tmpaCexpressDest.destType = eT_Direct;
	tmpaCexpressDest.pPayload =  new aCdataitemList;
	aCdataItem pI;
	char byFractWidth = 7;

	for(p = pDataItemList->begin();p != pDataItemList->end(); p++)
	{
		pDataItem = (DATA_ITEM*)&(*p);// PAW 03/03/09

		pI.fconst =  0.0f;
		pI.iconst =  0;
		pI.ref.clear();

		switch(pDataItem->type)
		{
			case 	DATA_CONSTANT:
				pI.iconst =  pDataItem->data.iconst;
				pI.type        = DATA_CONSTANT;
				pI.flags       = 0;
				pI.width       = 0;
				break;

			case	DATA_FLOATING:
				pI.fconst =  pDataItem->data.fconst;
				pI.type        = DATA_FLOATING;
				pI.flags       = 0;
				pI.width       = 0;
				break;

			case	DATA_REFERENCE:
				pI.type = DATA_REFERENCE;
				pI.flags = 0;
				pI.width = 0;
				break;

			case	DATA_REF_FLAGS:
				pI.type = DATA_REFERENCE;
				pI.flags = pDataItem->flags;
				pI.width = 0;
				break;

			case	DATA_REF_WIDTH:
				pI.type = DATA_REFERENCE;
				//pI.width = pDataItem->width;
				pI.width = pDataItem->mask;
				pI.flags = WIDTH_PRESENT;
				break;

			case	DATA_REF_FLAGS_WIDTH:
				pI.type = DATA_REFERENCE;
				pI.flags = pDataItem->flags | WIDTH_PRESENT;
				//pI.width = pDataItem->width;
				pI.width = pDataItem->mask;
				break;
				
			default:
				/*Should not come here!!!*/
				break;
		}/*End switch*/


/* VMKP added on 240104*stevev - removed 18jun09, this is the recontruction of the
 mask that the parser was deconstructiong - parser leaves it alone now, so we have to as well
 - this was wrong anyway - limited masks to 8 bits which is an invalid assumption    /
		if(pI.flags & WIDTH_PRESENT)
		{
			BYTE byMask = 0x00;
			
			for(int nloop=0; nloop < (int)(pI.width); nloop++)
			{
				byMask |= (1 << byFractWidth);
				byFractWidth--;

				if(byFractWidth == -1)
					byFractWidth = 7;
			}
			
			pI.width = byMask;
		}
/---------------------------------------end remove * VMKP added on 240104*/

		ddpREFERENCE :: iterator q;
		ddpREF*  pREF = NULL;
		aCreference *tmpaCref = &(pI.ref);
		BYTE makeTmpNull = 0;

		if((pDataItem->type == DATA_REFERENCE) || (pDataItem->type == DATA_REF_FLAGS)
		|| (pDataItem->type == DATA_REF_WIDTH) || (pDataItem->type == DATA_REF_FLAGS_WIDTH))
		{

			for(q = pDataItem->data.ref->begin();q != pDataItem->data.ref->end();q++)
			{
				pREF = (ddpREF*)&(*q);// PAW 03/03/09
/* VMKP removed on 240104*/
#if 0
				if( q->val.id == 0x9f )
					pI.width = 248;

				if( q->val.id == 0xab )
					pI.width = 7;
#endif
/* VMKP removed on 240104*/
				
				/* If not the first time allocate memory */
				if(makeTmpNull)
				{
					tmpaCref->pRef = new aCreference;
					tmpaCref = tmpaCref->pRef;
				}
				/* stevev 19feb07 - odd but ok  */
				#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
					//ddpREF myRef;
					//myRef.type = q->type;
					//myRef.val  = q->val;
					fill_reference(tmpaCref, pREF);// stevev 7aug7..was:  &myRef);
				#else
				fill_reference(tmpaCref,pREF);
				#endif
				makeTmpNull = 1;
#ifdef _DEBUG
	if ( tmpaCref->pExpr && tmpaCref->pExpr->expressionL.size() > 0)
	{
		if (tmpaCref->pExpr->expressionL[0].exprElemType == eet_VARID)
		{
			if (tmpaCref->pExpr->expressionL[0].dependency.depRef.id == 0xcdcdcdcd)
			{
				LOGIT(CERR_LOG,"Error: no ID.\n");
			}
		}
	}
#endif
			}
		}

		((aCdataitemList *)(tmpaCexpressDest.pPayload))->push_back(pI);
		// stevev 11jul07 - reference pointers are fully copied in the copy constructor
		pI.ref.destroy();


		/*Go to the next line*/
	}/*End for*/
	
	tmpgnCond.destElements.push_back(tmpaCexpressDest);
	tmpcondLst->genericlist.push_back(tmpgnCond);

}/*End fill_data_item_list*/


/****************************************************************************************/
/* F */


/********************************************************************************************/
void fill_response_code_list(aCcondList *tmpcondLst,RESPONSE_CODE_LIST *respList)
{
	RESPONSE_CODE_LIST :: iterator p;// ptr 2a RESPONSE_CODE
	RESPONSE_CODE	*pRespCode = NULL;// stevev 6aug07 X- use pointers instead of copying

	if(respList->size() == 0)
		return;


	aCrespCodeList pL;
	aCrespCode pI;
	
	aCgenericConditional tmpgnCond;
	tmpgnCond.priExprType = eT_Direct;
	aCgenericConditional::aCexpressDest tmpaCexpressDest;
	tmpaCexpressDest.destType = eT_Direct;
	tmpaCexpressDest.pPayload =  new aCrespCodeList;

	for(p = respList->begin();p != respList->end();++p)
	{
		pRespCode = (RESPONSE_CODE*)&(*p);// PAW 03/03/09
		pI.type = pRespCode->type;
		pI.val  = pRespCode->val;
		copy_ddlstring(&pI.descS,&(pRespCode->desc));

		if(pRespCode->evaled & RS_HELP_EVALED)
		{
			copy_ddlstring(&pI.helpS,&(pRespCode->help));

		}/*End if*/

		((aCrespCodeList *)(tmpaCexpressDest.pPayload))->push_back(pI);

		pI.helpS.clear();

	}/*End for*/

	tmpgnCond.destElements.push_back(tmpaCexpressDest);
	tmpcondLst->genericlist.push_back(tmpgnCond);

}/*End fill_response_code_list*/


/****************************************************************************************/
/* F */


/************************************************************************************************/

void fill_reference_list(AreferenceList_t *acreflist, REFERENCE_LIST *refList)
{
	REFERENCE_LIST :: iterator p;// ptr 2a reference
	ddpREFERENCE*	pRef = NULL;// stevev 7aug07 X- use pointers instead of copying
	
	if(refList->size() == 0)
		return;

	for(p = refList->begin();p != refList->end();++p)
	{
		pRef = (ddpREFERENCE*)&(*p);// PAW 03/03/09
		aCreference aCref;

		ddpREFERENCE :: iterator q;// ptr 2a ddpREF
		ddpREF*        pREF = NULL;
		
		aCreference *tmpaCref = &aCref;
		BYTE makeTmpNull = 0;

		for(q = pRef->begin();q != pRef->end();++q)
		{
			pREF = (ddpREF*)&(*q);// PAW 03/03/09
		/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}
		
            if((pREF->type == VIA_COLLECTION_REF) && (((pREF+1)->type == COLLECTION_ID_REF)) &&
                                                      ((pREF+1)->val.id > 0x80000000))
            {
                unsigned long resl_member = pREF->val.member;
                unsigned long resl_recordid = (pREF+1)->val.id;

                resolve_record_ref(resl_member,resl_recordid,tmpaCref);

                break;
            }
			/* stevev 19feb07 - odd but ok  */
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				//ddpREF myRef;
				//myRef.type = q->type;
				//myRef.val  = q->val;
				fill_reference(tmpaCref, pREF);// stevev 7aug07...was::> &myRef);
			#else
			fill_reference(tmpaCref,pREF);
			#endif
			makeTmpNull = 1;
		}/*End for*/

		acreflist->push_back(aCref);

	}/*End for*/
}/*End fill_reference_list*/


void fill_CondReference_list(AcondReferenceList_t *acCondReflist, REFERENCE_LIST *refList)
{
	REFERENCE_LIST :: iterator p;// ptr 2a reference
	ddpREFERENCE*	pRef = NULL;// stevev 7aug07 X- use pointers instead of copying
	aCreferenceList* pacreflist =  new aCreferenceList;
	
	if(refList->size() == 0)
		return;

	for(p = refList->begin();p != refList->end();++p)
	{
		pRef = (ddpREFERENCE*)&(*p);// PAW 03/03/09
		aCreference aCref;

		ddpREFERENCE :: iterator q;// ptr 2a ddpREF
		ddpREF*        pREF = NULL;
		
		aCreference *tmpaCref = &aCref;
		BYTE makeTmpNull = 0;

		for(q = pRef->begin();q != pRef->end();++q)
		{
			pREF = (ddpREF*)&(*q);// PAW 03/03/09
		/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}
		
			if((pREF->type == VIA_COLLECTION_REF) && ((pREF+1)->val.id > 0x80000000))
			{
				unsigned long resl_member = pREF->val.member;
				unsigned long resl_recordid = (pREF+1)->val.id;

				resolve_record_ref(resl_member,resl_recordid,tmpaCref);

				break;
			}
			/* stevev 19feb07 - odd but ok  */
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				//ddpREF myRef;
				//myRef.type = q->type;
				//myRef.val  = q->val;
				fill_reference(tmpaCref, pREF);// stevev 7aug07...was::> &myRef);
			#else
			fill_reference(tmpaCref,pREF);
			#endif
			makeTmpNull = 1;
		}/*End for*/

		pacreflist->push_back(aCref);

	}/*End for*/
	aCgenericConditional::aCexpressDest localDest;
	localDest.pPayload = pacreflist;
	localDest.destType = eT_Direct;
	aCgenericConditional localgennCond;
	localgennCond.priExprType = eT_Direct;
	localgennCond.destElements.push_back(localDest);

	acCondReflist->genericlist.push_back(localgennCond);

}/*End fill_cond_reference_list*/

/****************************************************************************************/
/* F */


/********************************************************************************************/

void resolve_record_ref(unsigned long resl_member,unsigned long resl_recordid,aCreference *aCref)
{
	vector <DDlBaseItem *>:: iterator p;
	DDlBaseItem *pBaseItem;
	
	for(p = devDesc->ItemsList.begin(); p != devDesc->ItemsList.end();p++)
	{
		pBaseItem = *p;

		switch(pBaseItem->byItemType)
		{
		case	COLLECTION_ITYPE: 
		{
		
			if( pBaseItem->id != resl_recordid)
			{	continue;
			}
			else
			{
				ItemAttrList :: iterator q;
				DDlAttribute *pAttr = NULL;
				ItemAttrList itmAttrList;

				itmAttrList = ((DDlCollection*)pBaseItem)->attrList;


				for(q = itmAttrList.begin(); q!= itmAttrList.end(); q++)
				{
					pAttr = (*q);

					switch(pAttr->byAttrID)
					{
					case	COLLECTION_MEMBERS_ID:
					{
						MEMBER_LIST *memberList;

						memberList = pAttr->pVals->memberList;

						MEMBER_LIST	:: iterator r;
						MEMBER member;

						if(memberList->size() == 0)
							return;

						for(r = memberList->begin();r != memberList->end();r++)
						{
							member = *r;

							if (member.name == resl_member)
							{
								// CodeWrights [PC 22] no longer used  ddpREFERENCE :: iterator s;
								aCreference *tmpaCref = aCref;

                                // CodeWrights [PC 22] using the ddpREF object damages a dynamic memory 
                                // when its destructor is called and cleans own data up (included the content of the pointed vector)
                                /* 
								#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
									ddpREF myRef;
									myRef.type = (member.item.begin())->type;
									myRef.val  = (member.item.begin())->val;
									fill_reference(tmpaCref, &myRef);
								#else
								s = member.item.begin();
								fill_reference(tmpaCref,s);
								#endif
                                */

                                // CodeWrights [PC 22] replaced with:
							    fill_reference(tmpaCref, (ddpREF *)&(*(member.item.begin())));

								tmpaCref->exprType = eT_Direct;
								tmpaCref->pExpr = new aCexpression; 
								ddpExpression	pExpr;
								Element elmnt;
								elmnt.byElemType = INTCST_OPCODE;
								elmnt.elem.ulConst = resl_member;
								pExpr.push_back(elmnt);
								fill_expression(tmpaCref->pExpr,&pExpr);

								break;
							}
						}/*next member*/
					}/* CASE */
					break;
					default:
						break;
					}/*switch*/
				}/* FOR */
			}/* ELSE*/
		}/*CASE*/
		default:
			break;
		
		}/*End switch*/
	} /*End For */
}


/****************************************************************************************/
/* F */



/********************************************************************************************/

void fill_refresh_relation(aCreferenceList *tmpRefrelation,REFERENCE_LIST *refList)
{
	fill_reference_list((AreferenceList_t *) tmpRefrelation,refList);
}

/****************************************************************************************/
/* F */


/********************************************************************************************/
/* note that we will use the element list even though the member list has the same structure*/
void fill_member_element_list(aCmemberElementList *tmpItmArray, ITEM_ARRAY_ELEMENT_LIST *memElemArray)
{
	ITEM_ARRAY_ELEMENT_LIST	:: iterator p; //a ptr2 ITEM_ARRAY_ELEMENT
	ITEM_ARRAY_ELEMENT* pElement = NULL;// stevev 6aug07 X- use pointers not copy

	if(memElemArray->size() == 0)
		return;

	AmemberElemList_t *tmpMembElemLst = (AmemberElemList_t *) tmpItmArray;
	aCmemberElement    tmpMemElement;

	for(p = memElemArray->begin(); p != memElemArray->end(); p++)
	{
		pElement = (ITEM_ARRAY_ELEMENT*)&(*p);// PAW 03/03/09
		tmpMemElement.clear();
		tmpMemElement.locator = pElement->index;
		BYTE makeTmpNull = 0;

		ddpREFERENCE :: iterator q;		// a ptr2a ddpREF
		ddpREF*    pREF = NULL;
		
		aCreference *tmpaCref = &tmpMemElement.ref;

		for(q = pElement->item.begin(); q != pElement->item.end(); q++)
		{
			pREF = (ddpREF*)&(*q);// PAW 03/03/09
			/* If not the first time allocate memory */
			if(makeTmpNull)
			{
				tmpaCref->pRef = new aCreference;
				tmpaCref = tmpaCref->pRef;
			}
			/* stevev 19feb07 - odd but ok  */
			#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
				//ddpREF myRef;
				//myRef.type = q->type;
				//myRef.val  = q->val;	// stevev 3aug07 - I don't know if this'll handle ptr right
				fill_reference(tmpaCref, pREF);// was::> &myRef);
			#else
			fill_reference(tmpaCref,pREF);
			#endif
			makeTmpNull = 1;
		}/*next ddpREF in reference*/
		tmpMemElement.mem_name = pElement->mem_name;

		if(pElement->evaled & MEMBER_DESC_EVALED)
			copy_ddlstring(&tmpMemElement.descS,&(pElement->desc));
		else
			tmpMemElement.descS.isSet = false;

		if(pElement->evaled & MEMBER_HELP_EVALED)
			copy_ddlstring(&tmpMemElement.helpS,&(pElement->help));
		else
			tmpMemElement.helpS.isSet = false;

		tmpMembElemLst->push_back(tmpMemElement);

	}/* next pElement*/
}/* end func */



/********************************************************************************************/
/* grid members are DIFFERENT!!           fill <<<<<------- this direction                  */
void fill_grid_member_list(aCgridMemberList *tmpGrdList, GRID_SET_LIST *pGridSetList)
{
	GRID_SET_LIST :: iterator p;
	GRID_SET*          pGridSet = NULL;// stevev 6aug07 X- use pointers not copy

	if(pGridSetList->size() == 0)
		return;

	AGridMemberElemList_t *tmpMembLst = (AGridMemberElemList_t *) tmpGrdList;
	aCgridMemberElement    tmpMemElement;

	for(p = pGridSetList->begin(); p != pGridSetList->end(); p++)
	{
		pGridSet = (GRID_SET*)&(*p);// PAW 03/03/09
		tmpMemElement.clear();// does a vector clear
		
		// HOMZ - port to 2003, VS7
		copy_ddlstring(&(tmpMemElement.header),&(pGridSet->desc));
		fill_reference_list(&(tmpMemElement.refLst),&(pGridSet->values));

		tmpGrdList->push_back(tmpMemElement);
		tmpMemElement.clear();// stevev 01feb06 - repair crash in dev object
	}/*End for*/

}/* end func */



#if 0 /* no longer used */   /* old code removed 3aug07 - see earlier versions for content*/
#endif /* no longer used */

/********************************************************************************************************/

/********************************************************************************************************/

/********************************************************************************************************/

void fill_array_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{
		case	ARRAY_LABEL_ID://	use aCattrString 
				paAttr =  fill_string_attribute(pAttr, ARRAY_LABEL);
				break;

		case	ARRAY_HELP_ID://	use aCattrString
				paAttr =  fill_string_attribute(pAttr, ARRAY_HELP);
				break;

		case	ARRAY_VALID_ID://	use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, ARRAY_VALID);
				break;

		case	ARRAY_TYPE_ID://	use aCattrCondReference
				paAttr =  fill_reference_attribute(pAttr, ARRAY_TYPE);
				break;

		case	ARRAY_NUM_OF_ELEMENTS_ID://  use  aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, ARRAY_NUM_OF_ELEMENTS);
				break;

		case	ARRAY_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, ARRAY_DEBUG);
				break;

		case  	MAX_ARRAY_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_ARRAY_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}


/********************************************************************************************************/


/********************************************************************************************************/
void fill_file_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	FILE_MEMBERS_ID://		use aCattrMemberList
				paAttr =  fill_memberlist_attribute(pAttr, FILE_MEMBERS);
				break;

		case	FILE_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, FILE_LABEL);
				break;

		case	FILE_HELP_ID://			use aCattrString
				paAttr =  fill_string_attribute(pAttr, FILE_HELP);
				break;

		case	FILE_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, FILE_DEBUG);
				break;

		case  	MAX_FILE_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_FILE_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_chart_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{
		case	CHART_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, CHART_LABEL);
				break;

		case	CHART_HELP_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, CHART_HELP);
				break;

		case	CHART_VALID_ID://		use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, CHART_VALID);
				break;

		case	CHART_HEIGHT_ID://		use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, CHART_HEIGHT);
				break;

		case	CHART_WIDTH_ID://		use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, CHART_WIDTH);
				break;

		case	CHART_TYPE_ID://		use aCattrCondLong	- as enum - Must not be conditional
				paAttr =  fill_ulong_attribute(pAttr, CHART_TYPE);
				break;

		case	CHART_LENGTH_ID://		use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, CHART_LENGTH);
				break;

		case	CHART_CYCLETIME_ID://	use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, CHART_CYCLETIME);
				break;

		case	CHART_MEMBERS_ID://		use aCattrMemberList
				paAttr =  fill_memberlist_attribute(pAttr, CHART_MEMBERS);
				break;

		case	CHART_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, CHART_DEBUG);
				break;
				

		case  	MAX_CHART_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_CHART_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_graph_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	GRAPH_LABEL_ID://	use aCattrString 
				paAttr =  fill_string_attribute(pAttr, GRAPH_LABEL);
				break;

		case	GRAPH_HELP_ID://	use aCattrString
				paAttr =  fill_string_attribute(pAttr, GRAPH_HELP);
				break;

		case	GRAPH_VALID_ID://	use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, GRAPH_VALID);
				break;

		case	GRAPH_HEIGHT_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRAPH_HEIGHT);
				break;

		case	GRAPH_WIDTH_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRAPH_WIDTH);
				break;

		case	GRAPH_XAXIS_ID://	use	aCattrCondReference
				paAttr =  fill_reference_attribute(pAttr, GRAPH_XAXIS);
				break;

		case	GRAPH_CYCLETIME_ID://	use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, GRAPH_CYCLETIME);
				break;

		case	GRAPH_MEMBERS_ID://	use aCattrMemberList
				paAttr =  fill_memberlist_attribute(pAttr, GRAPH_MEMBERS);
				break;

		case	GRAPH_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, GRAPH_DEBUG);
				break;


		case  	MAX_GRAPH_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_GRAPH_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_axis_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{
		case	AXIS_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, AXIS_LABEL);
				break;

		case	AXIS_HELP_ID://			use aCattrString
				paAttr =  fill_string_attribute(pAttr, AXIS_HELP);
				break;

		case	AXIS_VALID_ID://		use aCattrCondLong
				//paAttr =  fill_ulong_attribute(pAttr, AXIS_VALID);
				// an error  23jan07 - spec change
				paAttr->attr_mask = (1 << MAX_AXIS_ID);
				break;

		case	AXIS_MINVAL_ID://		use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, AXIS_MINVAL);
				break;

		case	AXIS_MAXVAL_ID://		use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, AXIS_MAXVAL);
				break;

		case	AXIS_SCALING_ID://		use aCattrCondLong		- as an enum
				paAttr =  fill_ulong_attribute(pAttr, AXIS_SCALING);
				break;

		case	AXIS_CONSTUNIT_ID://	use aCattrString
				paAttr =  fill_string_attribute(pAttr, AXIS_CONSTUNIT);
				break;

		case	AXIS_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, AXIS_DEBUG);
				break;


		case  	MAX_AXIS_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_AXIS_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_waveform_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{
		case	WAVEFORM_LABEL_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, WAVEFORM_LABEL);
				break;

		case	WAVEFORM_HELP_ID://			use aCattrString 
				paAttr =  fill_string_attribute(pAttr, WAVEFORM_HELP);
				break;

		case	WAVEFORM_HANDLING_ID://		use aCattrBitstring
				paAttr =  fill_bitstring_attribute(pAttr, WAVEFORM_HANDLING);
				break;

		case	WAVEFORM_EMPHASIS_ID://		use aCattrCondLong		- a conditional BOOLEAN [ T || F ]
				paAttr =  fill_ulong_attribute(pAttr, WAVEFORM_EMPHASIS);
				break;

		case	WAVEFORM_LINETYPE_ID://		use aCattrCondLong		- as an enum   
				paAttr =  fill_linetype_attribute(pAttr, WAVEFORM_LINETYPE); //Vibhor 041004: Changed
				break;

		case	WAVEFORM_LINECOLOR_ID://	use aCattrCondLong		- as a 24 bit uint	
				//paAttr =  fill_ulong_attribute(pAttr, WAVEFORM_LINECOLOR);
				paAttr =  fill_expression_attribute(pAttr, WAVEFORM_LINECOLOR);
				break;

		case	WAVEFORM_YAXIS_ID://		use	aCattrCondReference
				paAttr =  fill_reference_attribute(pAttr, WAVEFORM_YAXIS);
				break;

		case	WAVEFORM_KEYPTS_X_ID://		use aCattrReferenceList - there are constant references now...
				paAttr =  fill_reflist_attribute(pAttr, WAVEFORM_KEYPTS_X);
				break;

		case	WAVEFORM_KEYPTS_Y_ID://		use aCattrReferenceList - there are constant references now...		
				paAttr =  fill_reflist_attribute(pAttr, WAVEFORM_KEYPTS_Y);
				break;

		case	WAVEFORM_TYPE_ID://			use aCattrCondLong		- as an enum   
				paAttr =  fill_ulong_attribute(pAttr, WAVEFORM_TYPE);
				break;

		case	WAVEFORM_X_VALUES_ID://		use aCattrReferenceList - there are constant references now...		
				paAttr =  fill_reflist_attribute(pAttr, WAVEFORM_X_VALUES);
				break;

		case	WAVEFORM_Y_VALUES_ID://		use aCattrReferenceList - there are constant references now...
				paAttr =  fill_reflist_attribute(pAttr, WAVEFORM_Y_VALUES);
				break;

		case	WAVEFORM_X_INITIAL_ID://	use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, WAVEFORM_X_INITIAL);
				break;

		case	WAVEFORM_X_INCREMENT_ID://	use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, WAVEFORM_X_INCREMENT);
				break;

		case	WAVEFORM_POINT_COUNT_ID://	use aCattrCondExpr
				paAttr =  fill_expression_attribute(pAttr, WAVEFORM_POINT_COUNT);
				break;

		case	WAVEFORM_INIT_ACTIONS_ID://	use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, WAVEFORM_INIT_ACTIONS);
				break;

		case	WAVEFORM_RFRSH_ACTIONS_ID:// use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, WAVEFORM_RFRSH_ACTIONS);
				break;
	
		case	WAVEFORM_EXIT_ACTIONS_ID://	use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, WAVEFORM_EXIT_ACTIONS);
				break;

		case	WAVEFORM_VALID_ID://	use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, WAVEFORM_VALID);
				break;

		case	WAVEFORM_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, WAVEFORM_DEBUG);
				break;

		case  	MAX_WAVEFORM_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_WAVEFORM_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_source_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	SOURCE_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, SOURCE_LABEL);
				break;

		case	SOURCE_HELP_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, SOURCE_HELP);
				break;

		case	SOURCE_VALID_ID://		use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, SOURCE_VALID);
				break;

		case	SOURCE_EMPHASIS_ID://	use aCattrCondLong		- a conditional BOOLEAN [ T || F ]
				paAttr =  fill_ulong_attribute(pAttr, SOURCE_EMPHASIS);
				break;

		case	SOURCE_LINETYPE_ID://	use	aCattrCondLong		- as an enum
				paAttr =  fill_linetype_attribute(pAttr, SOURCE_LINETYPE); //Vibhor 041004: Changed
				break;

		case	SOURCE_LINECOLOR_ID://	use aCattrCondLong		- as a 24 bit uint
				paAttr =  fill_expression_attribute(pAttr, SOURCE_LINECOLOR);
				break;

		case	SOURCE_YAXIS_ID://		use aCattrCondReference
				paAttr =  fill_reference_attribute(pAttr, SOURCE_YAXIS);
				break;

		case	SOURCE_MEMBERS_ID://	use aCattrMemberList
				paAttr =  fill_memberlist_attribute(pAttr, SOURCE_MEMBERS);
				break;

		case	SOURCE_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, SOURCE_DEBUG);
				break;

		case	SOURCE_INIT_ACTIONS_ID://	use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, SOURCE_INIT_ACTIONS);
				break;

		case	SOURCE_RFRSH_ACTIONS_ID:// use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, SOURCE_RFRSH_ACTIONS);
				break;
	
		case	SOURCE_EXIT_ACTIONS_ID://	use aCattrActionList
				paAttr =  fill_actions_attribute(pAttr, SOURCE_EXIT_ACTIONS);
				break;


		case  	MAX_SOURCE_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_SOURCE_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_list_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{
		case	LIST_LABEL_ID://	use aCattrString 
				paAttr =  fill_string_attribute(pAttr, LIST_LABEL);
				break;

		case	LIST_HELP_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, LIST_HELP);
				break;

		case	LIST_VALID_ID://	use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, LIST_VALID);
				break;

		case	LIST_TYPE_ID://		use aCattrCondReference
				paAttr =  fill_reference_attribute(pAttr, LIST_TYPE);
				break;

		case	LIST_COUNT_ID://	use aCattrCondExpr  - cond expr
				paAttr =  fill_expression_attribute(pAttr, LIST_COUNT);
				break;

		case	LIST_CAPACITY_ID://	use aCattrCondLong	- non-cond in a integer conditional 
				paAttr =  fill_ulong_attribute(pAttr, LIST_CAPACITY);
				break;

		case	LIST_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, LIST_DEBUG);
				break;

		case  	MAX_LIST_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_LIST_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_image_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	IMAGE_LABEL_ID://	use aCattrString 
				paAttr =  fill_string_attribute(pAttr, IMAGE_LABEL);
				break;

		case	IMAGE_HELP_ID://	use aCattrString
				paAttr =  fill_string_attribute(pAttr, IMAGE_HELP);
				break;

		case	IMAGE_VALID_ID://	use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, IMAGE_VALID);
				break;

		case	IMAGE_LINK_ID://	use aCattrCondReference	- ref or const string
				paAttr =  fill_reference_attribute(pAttr, IMAGE_LINK);
				break;

		case	IMAGE_PATH_ID://	use aCattrCondLong	- index into image table
				paAttr =  fill_ulong_attribute(pAttr, IMAGE_PATH);
				break;

		case	IMAGE_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, IMAGE_DEBUG);
				break;

		case  	MAX_IMAGE_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_IMAGE_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }

	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void fill_grid_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	GRID_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, GRID_LABEL);
				break;

		case	GRID_HELP_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, GRID_HELP);
				break;

		case	GRID_VALID_ID://		use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, GRID_VALID);
				break;

		case	GRID_HEIGHT_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_HEIGHT);
				break;

		case	GRID_WIDTH_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_WIDTH);
				break;

		case	GRID_ORIENT_ID://	use aCattrCondLong	- as Non-Conditional enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_ORIENT);
				break;

		case	GRID_HANDLING_ID://		use aCattrCondReference
				paAttr =  fill_bitstring_attribute(pAttr, GRID_HANDLING);
				break;

		case	GRID_MEMBERS_ID://	use aCattrMemberList
				paAttr =  fill_gridmemberlist_attribute(pAttr, GRID_MEMBERS);
				break;

		case	GRID_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, GRID_DEBUG);
				break;

		case  	MAX_GRID_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_GRID_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }
	}/*Next p*/
}

/********************************************************************************************************/


/********************************************************************************************************/

void 
fill_blob_attributes(aCitemBase * paCitembase, ItemAttrList& List)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;
	aCattrBase*  paAttr = NULL;

	for(p = List.begin(); p!= List.end(); p++)
	{
		pAttr = (*p);

		if( (pAttr->bIsAttributeConditional     == TRUE) || 
			(pAttr->bIsAttributeConditionalList == TRUE))
		{	paCitembase->isConditional = TRUE; }

		switch(pAttr->byAttrID)
		{

		case	GRID_LABEL_ID://		use aCattrString 
				paAttr =  fill_string_attribute(pAttr, GRID_LABEL);
				break;

		case	GRID_HELP_ID://		use aCattrString
				paAttr =  fill_string_attribute(pAttr, GRID_HELP);
				break;

		case	GRID_VALID_ID://		use aCattrCondLong
				paAttr =  fill_ulong_attribute(pAttr, GRID_VALID);
				break;

		case	GRID_HEIGHT_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_HEIGHT);
				break;

		case	GRID_WIDTH_ID://	use aCattrCondLong	- as enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_WIDTH);
				break;

		case	GRID_ORIENT_ID://	use aCattrCondLong	- as Non-Conditional enum
				paAttr =  fill_ulong_attribute(pAttr, GRID_ORIENT);
				break;

		case	GRID_HANDLING_ID://		use aCattrCondReference
				paAttr =  fill_bitstring_attribute(pAttr, GRID_HANDLING);
				break;

		case	GRID_MEMBERS_ID://	use aCattrMemberList
				paAttr =  fill_gridmemberlist_attribute(pAttr, GRID_MEMBERS);
				break;

		case	GRID_DEBUG_ID:
				paAttr =  fill_debug_attribute(pAttr, GRID_DEBUG);
				break;

		case  	MAX_GRID_ID:
			{// an error
				paAttr->attr_mask = (1 << MAX_GRID_ID);
			}
			break;

		default:
			/*Should Never Reach here!!!!*/
			break;
		}// endswitch

        if (NULL != paAttr)
        {
            paCitembase->attrLst.push_back(paAttr);
        }
	}/*Next p*/
}


/********************************************************************************************************/




/********************* RELEASE FUNCTIONS ****************************************************************/
// Note that all release functions merely do away with internal pointers
// The ownwer must do the deletion 

RETURNCODE releaseCond(aCconditional* paCond)
{
	RETURNCODE rc = FAILURE;

	if ( ! (paCond->isAlist()) )
	{
		rc = SUCCESS;

	//VMKP 260304
		if (paCond->priExprType == eT_Direct)
		{
			paCond->destroy();
		}
		else
		{
			FOR_iT(aCgenericConditional::dest_List,paCond->destElements)
			{
//				((aCconditional *)(*iT).pCondDest)->destroy();
				if (iT->pCondDest != NULL)
				{
					releaseCond(((aCconditional *)(iT->pCondDest)));
					RAZE((iT->pCondDest));
				}
				if (iT->pPayload)
				{
					/*** memory debug ***/
					aPayldType* pX = iT->pPayload;
					if (pX == pBuilt)
					{
						_CrtDbgBreak();
					}

					/********************/

					iT->pPayload->destroy();
					RAZE((iT->pPayload));
				}
				iT->destExpression.destroy();//07dec05-sjv-do the rest

			}
			paCond->destElements.clear();
			paCond->priExpression.destroy();//07dec05-sjv-do the rest
			paCond->aWrkingExprDest.destExpression.destroy();//07dec05-sjv-do the rest
		}
	}
	else
	{
		LOGIT(CERR_LOG, "       passed a list when releasing a conditional.(%s)\n", ( paCond->isAlist())?"isAlist":"isNOTaList" );
	}
	//VMKP 260304
	return rc;
}

#define FOR_jT(lt,lst)  for(lt::iterator jT = (lst).begin();jT<(lst).end();jT++)

RETURNCODE releaseCondList(aCcondList* paCondLst)
{
	RETURNCODE rc = FAILURE;
	
	if ( paCondLst->isAlist() )
	{
		rc = SUCCESS;

	//VMKP 260304

		FOR_iT(aCcondList::listOconds_t, paCondLst->genericlist)
		{// ptr2a aCgenericConditional
//			iT->destroy();

			if (iT->priExprType == eT_Direct)
			{
				iT->destroy();// handles destlist,both expressions
			}
			else
			{
				FOR_jT(aCgenericConditional::dest_List, (iT->destElements))
				{// ptr2a aCexpressDest
					releaseCondList( ((aCcondList *)(jT->pCondDest)) );
					RAZE(jT->pCondDest);
					jT->destExpression.destroy();//07dec05-sjv-do the rest
				}
				iT->destElements.clear();
				iT->priExpression.destroy();//07dec05-sjv-do the rest
				iT->aWrkingExprDest.destExpression.destroy();//07dec05-sjv-do the rest
			}

		} 
		paCondLst->genericlist.clear();

//		paCondLst->destroy();

	//VMKP 260304
	}
	else //leave failure
	{
		LOGIT(CERR_LOG, "       NOT passed a list when releasing a conditional List.\n");
	}
	return rc;
}

RETURNCODE releaseDebug(aCdebugInfo*pAbAttr)
{
	RETURNCODE rc = SUCCESS;
	
	pAbAttr->symbolName.erase(); 
	pAbAttr->fileName.destroy();
	
	AdebugAttrList_t  ::  iterator aI; 
	for ( aI = pAbAttr->attrs.begin(); aI != pAbAttr->attrs.end(); ++ aI)
	{	aI->destroy();	}

	return rc;
}

RETURNCODE releaseMinMax( aCminmaxList* pMML )
{
	if ( pMML != NULL )
		pMML->destroy();
	else
		return FAILURE;
	return SUCCESS;
}

RETURNCODE releaseVarAttr(varAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = FAILURE;

    //CPMHACK: switch case modified
	switch(eAt)
	{		
	case varAttrClass:
	case varAttrHandling:
	case varAttrTimeScale:		// added 3jun08
        rc = releaseCond(&(((aCattrBitstring*)pAbAttr)->condBitStr));
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrBitstring*)pAbAttr);
        }
		break;
	case varAttrConstUnit:
	case varAttrLabel:
	case varAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrString*)pAbAttr);
        }
		break;
	case varAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));			
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrCondLong*)pAbAttr);
        }
		break;
    case varAttrTypeSize:
        //CPMHACK: Delete the var type size attribute
        if (NULL != pAbAttr)
        {
            delete ((aCattrTypeType*)pAbAttr);
        }
		// nothing to do here
		rc = SUCCESS;
		break;
	case varAttrEnums:
		rc = releaseCondList(&(((aCattrEnum*)pAbAttr)->condEnumListOlists));			
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrEnum*)pAbAttr);
        }
		break;
	case varAttrWidth:
	case varAttrHeight:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));				
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrCondLong*)pAbAttr);
        }
		break;
	case varAttrScaling:
	case varAttrDefaultValue:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));					
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrCondExpr*)pAbAttr);
        }
		break;
	case varAttrPreReadAct:
	case varAttrPostReadAct:
	case varAttrPreWriteAct:
	case varAttrPostWriteAct:
	case varAttrPreEditAct:
	case varAttrPostEditAct:
	case varAttrRefreshAct:
	case varAttrPostUserAct:
	case varAttrPostRequestAct:
		rc = releaseCondList(&(((aCattrActionList*)pAbAttr)->ActionList));						
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrActionList*)pAbAttr);
        }
		break;

	case varAttrDisplay:
	case varAttrEdit:
	case varAttrTimeFormat:		// added 3jun08
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));							
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrString*)pAbAttr);
        }
		break;

	case varAttrMinValue:
	case varAttrMaxValue:
		//rc = releaseCond(&(((aCattrVarMinMaxList*)pAbAttr)->condMinMaxVal));	
		rc = releaseMinMax(&((aCattrVarMinMaxList*)pAbAttr)->ListOminMaxElements );
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrVarMinMaxList*)pAbAttr);
        }
		break;

	case varAttrIndexItemArray:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));	
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCattrCondReference*)pAbAttr);
        }
		break;

	case varAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            delete ((aCdebugInfo*)pAbAttr);
        }
		break;

	case varAttrResponseCodes:
	case varAttrLastvarAttr	:
	default:
		break;// failure
	}//endswitch
	return rc;
}
RETURNCODE releaseCmdAttr(cmdAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = FAILURE;

	switch(eAt)
	{		
	case cmdAttrNumber:
	case cmdAttrOperation:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));							
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);		
		break;
	case cmdAttrTransaction:
		{
			FOR_iT(AtransactionL_t,(((aCattrCmdTrans*)pAbAttr)->transactionList))
			{//iT pts 2 aCtransaction
				rc = releaseCondList(&(iT->request));
				rc = releaseCondList(&(iT->response));
				rc = releaseCondList(&(iT->respCodes));
			}							
            if (rc == SUCCESS && NULL != pAbAttr)
				delete ((aCattrCmdTrans*)pAbAttr);	
		}
		break;
	case cmdAttrRespCodes:
		rc = releaseCondList(&(((aCattrCmdRspCd*)pAbAttr)->respCodes));								
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCmdRspCd*)pAbAttr);	
		break;
	case cmdAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);								
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;
	case cmdAttrUnknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       command attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}		
RETURNCODE releaseMenuAttr(menuAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case menuAttr_Items:
		rc = releaseCondList(&(((aCattrMenuItems*)pAbAttr)->condMenuListOlists));									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMenuItems*)pAbAttr);		
		break;
	case menuAttr_Label:
	case menuAttr_Help:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);	
		break;
	case menuAttr_Validity:
	case menuAttr_Style:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);	
		break;

	case menuAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;

	case menuAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "      menu attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseEdDispAttr(eddispAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = FAILURE;

	switch (eAt)
	{
	case eddispAttrLabel:
	case eddispAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));										
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);	
		break;
	case eddispAttrEditItems:
	case eddispAttrDispItems:
		rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrReferenceList*)pAbAttr);	
		break;
	case eddispAttrPreEditAct:
	case eddispAttrPostEditAct:
		rc = releaseCondList(&(((aCattrActionList*)pAbAttr)->ActionList));									
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrActionList*)pAbAttr);	
		break;
	case eddispAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));										
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);	
		break;

	case eddispAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);										
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;

	case eddispAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "      Edit Display attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}
RETURNCODE releaseMethodAttr(methodAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case methodAttrDefinition:
		// no-op --- has a destructor!		
		/* VMKP added on 180204 (This is really getting deleted in the
		  aCattrMethodDef destructor with a RAZE function, but when i checked with 
		  Boundschecker for memory leak,  it is showing as leak present, I don't 
		  know why? But i added this code,  which eliminates this leak */
		if(((aCattrMethodDef *)pAbAttr)->pBlob)
		{
			delete[] ((aCattrMethodDef *)pAbAttr)->pBlob;//via DD@F 16aug13
			((aCattrMethodDef *)pAbAttr)->pBlob = NULL;
		}
        //CPMHACK: Release method definition attribute memory
        if(nullptr != pAbAttr && NULL != pAbAttr)
        {
            delete ((aCattrMethodDef *)pAbAttr);
            pAbAttr = nullptr;
        }
		/* End of VMKP added on 180204 */
		break;

	case methodAttrClass:
	case methodAttrScope:
		rc = releaseCond(&(((aCattrBitstring*)pAbAttr)->condBitStr));										
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrBitstring*)pAbAttr);		
		break;

	case methodAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));											
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);	
		break;

	case methodAttrHelp:
	case methodAttrLabel:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));											
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);	
		break;
		
	case methodAttrType:
        rc = SUCCESS;
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            ((aCparameter*)pAbAttr)->destroy();
            delete ((aCparameter*)pAbAttr);
        }
		break;
	case methodAttrParams:
        rc = SUCCESS;
        if (rc == SUCCESS && NULL != pAbAttr)
        {
            ((aCparameterList*)pAbAttr)->destroy();
            delete ((aCparameterList*)pAbAttr);
        }
		break;

	case methodAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);											
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;

	case methodAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "      method attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}
RETURNCODE releaseRefreshAttr(refreshAttrType_t eAt, aCattrBase* pAbAttr)
{// 0 'refreshAttrItems' only one
	RETURNCODE rc = FAILURE;
	if ( eAt == refreshAttrItems)
	{
	//	rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));

		rc  = releaseCondList(&(((aCattrRefreshitems*)pAbAttr)->condUpdateList));
		rc |= releaseCondList(&(((aCattrRefreshitems*)pAbAttr)->condWatchList));
		if (rc)
		{
			LOGIT(CERR_LOG, "       error %d returned when releasing refresh items.\n",rc);
		}
		else
		{
            if (NULL != pAbAttr)
                delete ((aCattrRefreshitems*)pAbAttr);
		}
	}
	else
	if ( eAt == refreshAttrDebugInfo)
	{
        rc = releaseDebug((aCdebugInfo*)pAbAttr);
        if (NULL != pAbAttr)
            delete ((aCdebugInfo*)pAbAttr);
	}
	else
	{
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       unsupported Refresh attribute.((0x%02x)\n", ((int)eAt) );
	}
	return rc;
}
RETURNCODE releaseUnitAttr(unitAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = FAILURE;
	if (pAbAttr == NULL) 
		return SUCCESS;

	if ( eAt == unitAttrItems ) // one and only
	{
		rc  = releaseCond(&(((aCattrUnititems*)pAbAttr)->condVar));
		rc |= releaseCondList(&(((aCattrUnititems*)pAbAttr)->condListOUnitLists));
		if (rc)
		{
			LOGIT(CERR_LOG, "       error %d returned when releasing unit items.\n",rc);
		}
		else
		{
            if (NULL != pAbAttr)
                delete ((aCattrUnititems*)pAbAttr);
		}
	}
	else
	if ( eAt == unitAttrDebugInfo)
	{
		rc = releaseDebug((aCdebugInfo*)pAbAttr);
        if (NULL != pAbAttr)
            delete ((aCdebugInfo*)pAbAttr);
	}
	else
	{
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       unknown Unit attribute.((0x%02x)\n", ((int)eAt) );
	}
	return rc;
}	
RETURNCODE releaseWaOAttr(waoAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = FAILURE;
	if (pAbAttr == NULL) 
		return SUCCESS;

	if ( eAt == waoAttrItems ) // one and only
	{
		rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));
        if (NULL != pAbAttr)
            delete ((aCattrReferenceList*)pAbAttr);
	}
	else
	if ( eAt == waoAttrDebugInfo)
	{
		rc = releaseDebug((aCdebugInfo*)pAbAttr);
        if (NULL != pAbAttr)
            delete ((aCdebugInfo*)pAbAttr);
	}
	else
	{
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       unknown WaO attribute.((0x%02x)\n", ((int)eAt) );
	}
	return rc;
}

RETURNCODE releaseGrpItmAttr(grpItmAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;
	if ( pAbAttr == NULL )
		return SUCCESS;

	switch(eAt)
	{		
	case grpItmAttrElements:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));										
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);				
		break;

	case grpItmAttrLabel:
	case grpItmAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));											
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);		
		break;

	case grpItmAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));											
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);		
		break;

	case grpItmAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;

	case grpItmAttrUnknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       group item attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}

/* use the generic releaseGrpItmAttr()		
RETURNCODE releaseItmArrAttr(itemarrayAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case itemarrayAttrElements:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));		
		break;

	case itemarrayAttrLabel:
	case itemarrayAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
		break;

	case itemarrayAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));	
		break;

	case itemarrayAttrUnknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       item array attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}
RETURNCODE releaseCollAttr(collAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case collAttrMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));		
		break;

	case collAttrLabel:
	case collAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
		break;

	case collAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       collection attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
end -use generic instead */
/*Vibhor 041004: Start of Code*/

RETURNCODE releaseArrayAttr(arrayAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{
	case arrayAttrLabel:
	case arrayAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case arrayAttrValidity:
	case arrayAttrElementCnt:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case arrayAttrType:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);
		break; 

	case arrayAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;

	case arrayAttr_Unknown:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "      array attribute is unknown when releasing.((0x%02x)\n", ((int)eAt) );
		break;
	}

	return rc;
}

/*Vibhor 041004: End of Code*/

RETURNCODE releaseFileAttr(fileAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case fileMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);		
		break;

	case fileLabel:
	case fileHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;

	case fileDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;

	case fileNoValidity:
	case file_unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "      file attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseChartAttr(chartAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case chartAttrLabel:
	case chartAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case chartAttrValidity:
	case chartAttrHeight:
	case chartAttrWidth:
	case chartAttrType:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case chartAttrLength:
	case chartAttrCycleTime:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);
		break;
	case chartAttrMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);		
		break;
	case chartAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case chartAttrUnknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       chart attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseGraphAttr(graphAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{	
	case graphAttrLabel		:
	case graphAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case graphAttrValidity:
	case graphAttrHeight:
	case graphAttrWidth:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case graphAttrXAxis:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);	
		break;
	case graphAttrMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);		
		break;
	case graphAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;
	case graphAttrCycleTime:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);
		break;
	case graphAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       graph attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseAxisAttr(axisAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{
	case axisAttrLabel	:
	case axisAttrHelp:
	case axisAttrUnit:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);	
		break;
	case axisAttrValidity:
	case axisAttrScale:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case axisAttrMinVal:
	case axisAttrMaxVal:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);
		break;
	case axisAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case axisAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       axis attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseWaveformAttr(waveformAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{	
	case waveformAttrLabel	:
	case waveformAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case waveformAttrHandling:
		rc = releaseCond(&(((aCattrBitstring*)pAbAttr)->condBitStr));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrBitstring*)pAbAttr);	
		break;
	case waveformAttrEmphasis:
	case waveformAttrType:
	case waveformAttrValidity:	// added 23jan07 - sjv - spec change
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case waveformAttrLineType:
		rc = releaseCond(&(((aCattrCondIntWhich*)pAbAttr)->condIntWhich));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondIntWhich*)pAbAttr);
		break;
	case waveformAttrYAxis:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);
		break;
	case waveformAttrKeyPtX:
	case waveformAttrKeyPtY:
	case waveformAttrXVals:
	case waveformAttrYVals:
		rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrReferenceList*)pAbAttr);
		break;
	case waveformAttrXinitial:
	case waveformAttrXIncr:
	case waveformAttrPtCount:
	case waveformAttrLineColor:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);
		break;
	case waveformAttrInitActions:
	case waveformAttrRfshActions:
	case waveformAttrExitActions:
		rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrReferenceList*)pAbAttr);
		break;
	case waveformAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;

	case waveformAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       waveform attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseSourceAttr(sourceAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{
	case sourceAttrLabel		:
	case sourceAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);	
		break;
	case sourceAttrValidity:
	case sourceAttrEmphasis:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case sourceAttrLineType:
		rc = releaseCond(&(((aCattrCondIntWhich*)pAbAttr)->condIntWhich));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondIntWhich*)pAbAttr);
		break;
	case sourceAttrLineColor:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);
		break;
	case sourceAttrYAxis:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);
		break;
	case sourceAttrMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);		
		break;
	case sourceAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case sourceAttrInitActions:
	case sourceAttrRfshActions:
	case sourceAttrExitActions:
		rc = releaseCondList(&(((aCattrReferenceList*)pAbAttr)->RefList));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrReferenceList*)pAbAttr);
		break;
	case sourceAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       source attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseListAttr(listAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{
	case listAttrLabel	:
	case listAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case listAttrValidity:
	case listAttrCapacity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case listAttrType:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);
		break;
	case listAttrCount:
		rc = releaseCond(&(((aCattrCondExpr*)pAbAttr)->condExpr));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondExpr*)pAbAttr);	
		break;
	case listAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);	
		break;
	case listAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       list attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseGridAttr(gridAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case gridAttrLabel:
	case gridAttrHelp:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case gridAttrValidity:
	case gridAttrHeight:
	case gridAttrWidth:
	case gridAttrOrient:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case gridAttrHandling:
		rc = releaseCond(&(((aCattrBitstring*)pAbAttr)->condBitStr));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrBitstring*)pAbAttr);
		break;
	case gridAttrMembers:
		rc = releaseCondList(&(((aCattrMemberList*)pAbAttr)->condMemberElemListOlists));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrMemberList*)pAbAttr);	
		break;
	case gridAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case gridAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       grid attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}	
RETURNCODE releaseImageAttr(imageAttrType_t eAt, aCattrBase* pAbAttr)
{
	RETURNCODE rc = SUCCESS;

	switch(eAt)
	{		
	case imageAttrLabel:
	case imageAttrHelp:
	case imageAttrPath:
		rc = releaseCond(&(((aCattrString*)pAbAttr)->condString));		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrString*)pAbAttr);
		break;
	case imageAttrValidity:
		rc = releaseCond(&(((aCattrCondLong*)pAbAttr)->condLong));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondLong*)pAbAttr);
		break;
	case imageAttrLink:
		rc = releaseCond(&(((aCattrCondReference*)pAbAttr)->condRef));	
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCattrCondReference*)pAbAttr);
		break;
	case imageAttrDebugInfo:
		rc = releaseDebug((aCdebugInfo*)pAbAttr);		
        if (rc == SUCCESS && NULL != pAbAttr)
			delete ((aCdebugInfo*)pAbAttr);
		break;
	case imageAttr_Unknown:
	default:
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       image attribute is unknown when releasing.(0x%02x)\n", ((int)eAt) );
		break;
	}// endswitch

	return rc;
}		


// in dbg_dllApi.cpp
extern genericTypeEnum_t getAttrType4Mask(CitemType& itmType, ulong aMask);


RETURNCODE releaseAttrib(CitemType& itmType, aCattrBase* pAbstractAttribute )
{//	
	RETURNCODE rc = SUCCESS;
	genericTypeEnum_t git = getAttrType4Mask(itmType, pAbstractAttribute->attr_mask);

	switch((itemType_t)itmType)
	{
	case iT_Variable:		rc = releaseVarAttr(git.gAttrVar, pAbstractAttribute);			break;
	case iT_Command:		rc = releaseCmdAttr(git.gAttrCmd, pAbstractAttribute);			break;
	case iT_Menu:			rc = releaseMenuAttr(git.gAttrMenu, pAbstractAttribute);		break;
	case iT_EditDisplay:	rc = releaseEdDispAttr(git.gAttrEdDisp, pAbstractAttribute);	break;
	case iT_Method:			rc = releaseMethodAttr(git.gAttrMethod, pAbstractAttribute);	break;
	case iT_Refresh:		rc = releaseRefreshAttr(git.gAttrRefresh, pAbstractAttribute);	break;
	case iT_Unit:			rc = releaseUnitAttr(git.gAttrUnit, pAbstractAttribute);		break;
	case iT_WaO:			rc = releaseWaOAttr(git.gAttrWao, pAbstractAttribute);			break;
	case iT_ItemArray:		rc = releaseGrpItmAttr(git.gAttrGrpItm, pAbstractAttribute);	break;
	case iT_Collection:		rc = releaseGrpItmAttr(git.gAttrGrpItm, pAbstractAttribute);	break;


	case iT_File:			rc = releaseGrpItmAttr(git.gAttrGrpItm, pAbstractAttribute);	break;
	case iT_Chart:			rc = releaseChartAttr(git.gAttrChart, pAbstractAttribute);		break;
	case iT_Graph:			rc = releaseGraphAttr(git.gAttrGraph, pAbstractAttribute);		break;
	case iT_Axis:			rc = releaseAxisAttr(git.gAttrAxis, pAbstractAttribute);		break;
	case iT_Waveform:		rc = releaseWaveformAttr(git.gAttrWaveform, pAbstractAttribute);break;
	case iT_Source:			rc = releaseSourceAttr(git.gAttrSource, pAbstractAttribute);	break;
	case iT_List:			rc = releaseListAttr(git.gAttrList, pAbstractAttribute);		break;
	case iT_Array:			rc = releaseArrayAttr(git.gAttrArray, pAbstractAttribute);		break;
	case iT_Grid:			rc = releaseGridAttr(git.gAttrGrid, pAbstractAttribute);		break;
	case iT_Image:			rc = releaseImageAttr(git.gAttrImage, pAbstractAttribute);		break;

	case iT_Block:
	case iT_Record:
	case iT_ResponseCodes:
	case iT_Member:	
		// unsupported
		rc = APP_NO_ATTR_TYPE;
		LOGIT(CERR_LOG, "       unsupported Item Type in release.(0x%02x)\n", ((long)itmType));
		break;

	case iT_ReservedZeta:
	case iT_ReservedOne:
	case iT_Program:
	case iT_VariableList:
	case iT_Domain:
	case iT_MaxType:
	case iT_FormatObject:
	case iT_DeviceDirectory:
	case iT_BlockDirectory:
	case iT_Parameter:
	case iT_ParameterList:
	case iT_BlockCharacteristic:
	default:
		// erroneous
		rc = APP_PROGRAMMER_ERROR;
		LOGIT(CERR_LOG, "       unsupported Item Type in release.(0x%02x)\n", ((long)itmType));
		break;
	}// end switch

	return rc;	
}


RETURNCODE releaseOneAttrib(CitemType& itmType, aCattrBase* pAttribute )
{
	RETURNCODE rc = FAILURE;
	if ( pAttribute == NULL ) return FAILURE;

	rc = releaseAttrib(itmType,  pAttribute );

// stevev 21feb07 merge - #if _MSC_VER < 1300  // HOMZ - port to 2003, VS7 - this is causing problems in ~aCitemBase()
// stevev 21feb07 merge - 	if ( rc == SUCCESS )
// stevev 21feb07 merge - 	{
// stevev 21feb07 merge - 		delete (*ppAttribute);
// stevev 21feb07 merge - 		(*ppAttribute) = NULL;
// stevev 21feb07 merge - 	}
// stevev 21feb07 merge - 	else
// stevev 21feb07 merge -	{
// stevev 21feb07 merge - 		// unknown failure, force a memory leak...dont'::> delete (*ppAttribute);
// stevev 21feb07 merge - 		(*ppAttribute) = NULL;
// stevev 21feb07 merge - 	}
// stevev 21feb07 merge - #endif
	return SUCCESS;	
}


/* LOCAL */
RETURNCODE releaseItem(aCitemBase* pAbstractItem )
{//	for each attribute..destroy, delete
	RETURNCODE rc = FAILURE;

	FOR_iT(AattributeList_t,pAbstractItem->attrLst)
	{//iT isa ptr2ptr2aCattrBase
		
// stevev - 21feb07 - merge - all the time #if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
		aCattrBase* attBase = *iT;
		//rc = releaseOneAttrib(pAbstractItem->itemType, attBase);
		if (attBase) rc = releaseAttrib(pAbstractItem->itemType, attBase );// deletes it
// stevev - 21feb07 - merge - all the time #else
// stevev - 21feb07 - merge - all the time 		rc = releaseOneAttrib(pAbstractItem->itemType,  iT);
// stevev - 21feb07 - merge - all the time #endif
		if (rc != SUCCESS)
		{
//			LOGIT(CERR_LOG, "ERROR: %d when releasing attribute type.\n", rc);
		}
		else
			*iT = NULL;
	}
	pAbstractItem->attrLst.clear();
	return SUCCESS;
}


RETURNCODE releaseDev(aCdevice** ppAbstractDevice)
{
	RETURNCODE rc = FAILURE;;
	//aCitemBase* pItem;

	if ( ppAbstractDevice == NULL || (*ppAbstractDevice) == NULL )
	{
		return SUCCESS;
	}
	aCitemBase* pIB =  NULL;
	// for each item, release
	FOR_iT(AitemList_t,(*ppAbstractDevice)->AitemPtrList)// list of pointers
	{//iT isa ptr2ptr2aCitemBase
		pIB = *iT; // don't use iterator as a pointer
		if ( pIB != NULL )
		{
			rc = releaseItem( pIB );
			if (rc != SUCCESS)
			{
				LOGIT(CERR_LOG, "ERROR: %d when releasing item type %d.\n", rc, (long)((*iT)->itemType));
			}
			//else
			//{
//#if _MSC_VER < 1300 // HOMZ - Causes a crash in  ~aCitemBase()  dllapi.h during (*iT)->clear(); 
					   // The aCitemBase has a member AattributeList_t attrLst
					   // When the list is parsed and the item aCattrBase is cleared, a crash occurs.
					   // It has a member whose address is 0xfeeefeee
				delete (pIB);
//#endif
				//*iT = NULL;
			//}
		}// else, just leave it alone
	}
	// get rid of the graphics
	
	if ( (*ppAbstractDevice)->AimageList.size() > 0 )
	{// find offset zero and delete it
		AimageList_t::iterator iTI;
		AframeList_t::iterator iTF;
		BOOL gotiT = FALSE;

		for ( iTI  = (*ppAbstractDevice)->AimageList.begin(); 
			  iTI != (*ppAbstractDevice)->AimageList.end() ; ++ iTI)
		{
			for ( iTF = iTI->begin(); iTF != iTI->end(); ++ iTF)
			{
				if ( iTF->offset == 0 && iTF->size > 0 && !gotiT)// should only happen once!
				{
					delete[] iTF->pRawFrame;//via DD@F
					gotiT = true;
				}
			}
			iTI->clear();
		}
		(*ppAbstractDevice)->AimageList.clear();
	}

	delete (*ppAbstractDevice);
	(*ppAbstractDevice) = NULL;
	
	return SUCCESS;
}

/* stevev 15may08 - we no longer need stnd tables or dictionaries to load the DD 
	- all we need is the sdc file.  We will use that to verify the diretory. */
bool verifyReleaseDir(void)
{
	bool ret = false; // assume bad till proven otherwise
	/* requirement for a valid release directory is
	 * a) has the '000000' directory, and
	 * b) has 'Standard.dct' in it
	 */
	dDSearchOps *hSearch = NULL;

	char szSrchPath[MAX_PATH]; 
#ifdef GE_BUILD
// PAW 00.01.06 start
	file_error_occured = FALSE;
// PAW 00.01.06 end
#endif
	//build up szSrchPath to have 000000
	strcpy(szSrchPath,chDbdir);
#ifdef USE_ORIGINAL_PATH	// PAW 03/03/09
	if (szSrchPath[strlen(chDbdir)-1] != OS_PATH_SEPR[0])
	{
		strcat(szSrchPath,OS_PATH_SEPR);// the hart directory
		strcat(szSrchPath,"0000f9");// the hart directory
	}
	else
	{
		strcat(szSrchPath,"0000f9");// the hart directory
	}
#else
	if (szSrchPath[strlen(chDbdir)-1] != OS_PATH_SEPR[0])
	{
		strcat(szSrchPath,OS_PATH_SEPR);
		strcat(szSrchPath,"000000");
	}
	else
	{
		strcat(szSrchPath,"000000");
	}
#endif

	hSearch = dDSearchOps::instantiateAndFind(szSrchPath, "");

#ifdef UNICODE

#ifdef GE_BUILD
// PAW handle SD Card 29/06/09
	if (NULL == hSearch)
	{
		unsigned char length, loop;
		char copy_string[50];
		length = strlen(szSrchPath);
		for (loop = 0; loop < length; loop++)
			if (szSrchPath[loop] == OS_PATH_SEPR[0])
			{
				strcpy(&copy_string[0], &szSrchPath[loop]);
				break;
			}
		strcpy(&szSrchPath[0], "SDCard");
		strcpy(&chDbdir[0], "SDCard");
		strncpy(&szSrchPath[6], &copy_string[0], length-8);
		strncpy(&chDbdir[6], &copy_string[0], length-8-7);
		chDbdir[length+6-8-8] = '\0';

		hSearch = dDSearchOps::instantiateAndFind(szSrchPath, "");
	}
// PAW 29/06/09 end
#endif

#ifndef GE_BUILD
////NOTE::  (reenabled 12aug10, worked in v6)  #ifdef USED	// PAW 00_01_06 removed as causes a crash
#ifdef _DEBUG
	if (NULL == hSearch)
	{
		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
		);
		// Process any inserts in lpMsgBuf.
		// ...
		// Display the string.
		//MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION 
		//			| MB_TASKMODAL );// stevev - 19jun07 - fix for PAR 324 - crash on no dir
		wstring tmpws((LPTSTR)lpMsgBuf);
		string  tmprs;
		tmprs = TStr2AStr(tmpws);
		if (tmprs[tmprs.length()-1] == 0x0a) 
		{
			tmprs[tmprs.length()-1] = ' ';
			tmprs[tmprs.length()-2] = ' ';
		}
		tmprs += " (";
		tmprs += szSrchPath;
		tmprs += ")\n";
		/* logit checked  8/22/08 */
		LOGIT(USRACK_LOG|CLOG_LOG,(char*)tmprs.c_str());// convert to standard logging interface 15may08

		// Free the buffer.
		LocalFree( lpMsgBuf );
	}
#endif /* _DEBUG*/
////NOTE::  (reenabled 12aug10, worked in v6)    #endif // PAW 00.01.06
#endif  // not GE_BUILD
#endif // unicode

	if ( (NULL != hSearch)  &&		// we found something named 000000
		(hSearch->isDirectory()) ) // it is a directory
	{ 
		delete hSearch;
		hSearch = NULL;

		//build up szSrchPath to have Standard.dct
		strcpy(szSrchPath,chDbdir);
		if (szSrchPath[strlen(chDbdir)-1] != OS_PATH_SEPR[0])
		{
			// stevev 18apr08 - we no longer require anything in the library
			//    i'm going to require something for a while....
			//was   strcat(szSrchPath,"\\?tandard.dct");
			// stevev 15may08 - we require the sdc DD, we'll use that one
			strcat(szSrchPath, OS_PATH_SEPR);
			strcat(szSrchPath, VERIFY_RELEASE_DIR_PATH);// replaced see above PAw25/03/09
		}
		else
		{
			// ditto above     strcat(szSrchPath,"\\000000\\0002\\0502.fm6");
			strcat(szSrchPath,VERIFY_RELEASE_DIR_PATH);
		}

		hSearch = dDSearchOps::instantiateAndFind(szSrchPath, "");

		if ( (NULL != hSearch)  &&		// we found sdc file of some sort
			!(hSearch->isDirectory()) ) // it is not a directory
		{ 
			ret = true; // it meets the criteria
		}// else has no standard.dct - return false - not valid	
		else
		{// stevev 15may08 - try to inform the user of what's wrong	
			string msgStr;

#ifdef UNICODE	
			wstring tmpS; tmpS = ((wchar_t*)szSrchPath);
			msgStr = TStr2AStr(tmpS);
#else
			msgStr = szSrchPath;
#endif
			/* logit checked 8/22/08*/
		LOGIT(USRACK_LOG|CLOG_LOG,"WARNING: Required File '%s' \n"/* use non -long constant string to invoke conversion in logit*/
		"                        was not detected. (expect 'String Not Found' errors)\n",szSrchPath);
#ifdef GE_BUILD
// PAW 00.01.06	start
		LOGIT(STAT_LOG|CLOG_LOG,"Warning DD Files missing\n");
		file_error_occured = TRUE;
// PAW 00.01.06	end
#endif
		ret = true; // stevev 15may08 we're going to let this slide in order to tokenize the SDC DD
					//                other DDs will get a bunch of string errors.
		}
	} 
	// else has no 000000 - return false - not valid
#ifdef GE_BUILD
// PAW 00.01.06	start
		LOGIT(STAT_LOG|CLOG_LOG,"Warning DD Files missing\n");
		file_error_occured = TRUE;
// PAW 00.01.06	end
#endif
	if (hSearch)
	{
		delete hSearch;
		hSearch = NULL;
	}
	return ret;
}



/****************************************************************************************/

/*This function returns a FilePath & Key according to Identity (including HART Version)
 * stevev 09feb07 - generic path from identity, No filename added, key is filled as unique key
 *                   returns len of path string or 0 on error                               
*************************************************************************************************/
//int  makePath( Indentity_t fromIdent, char* retPath, int MaxPathLen, DWORD& uniqueKey)
int  makePath( Indentity_t fromIdent, char* retPath, int MaxPathLen, DD_Key_t& uniqueKey)
{
	int  stl;
	char tmpchDbdir[MAX_FILE_NAME_PATH];

	if (chDbdir != NULL && chDbdir[0] != '\0')
	{
		strncpy(tmpchDbdir,chDbdir,MAX_FILE_NAME_PATH);
	}
	else
	{
		strcat(tmpchDbdir,OS_PATH_SEPR);
		strcat(tmpchDbdir,"Release");
	}
	if (tmpchDbdir[strlen(tmpchDbdir)-1] != OS_PATH_SEPR[0])
		strcat(tmpchDbdir,OS_PATH_SEPR);

	if ( fromIdent.cUniversalRev < 7 )// 5 & 6 & unitialized xmtr-dd's 0x01
	{// legacy handling		
	//	uniqueKey = ((fromIdent.wManufacturer & 0x00FF)<<24) | 
	//				((fromIdent.wDeviceType   & 0x00FF)<<16) | 
	//				((fromIdent.cDeviceRev    & 0x00FF)<< 8);//  ddrev 2bDetermined 	
		DD_Key_t llM, llT, llR;
		llM = fromIdent.wManufacturer;
		llT = fromIdent.wDeviceType;
		llR = fromIdent.cDeviceRev;

		uniqueKey = ((llM & 0xFFFF)<<48) | ((llT & 0xFFFF)<<32) | ((llR& 0x00FF)<<16);   //  ddrev 2bDetermined 
	//	sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,(fromIdent.wManufacturer & 0x00FF),
	//										"\\",(fromIdent.wDeviceType   & 0x00FF),"\\");
		sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,(fromIdent.wManufacturer & 0xFFFF),
				OS_PATH_SEPR,(fromIdent.wDeviceType   & 0xFFFF),OS_PATH_SEPR);
	}
	else
	// if (fromIdent.cUniversalRev >= 7)
	{// 16 bit handling
		DD_Key_t llM, llT, llR;
		llM = fromIdent.wManufacturer;
		llT = fromIdent.wDeviceType;
		llR = fromIdent.cDeviceRev;
		if (fromIdent.cUniversalRev > 7 && fromIdent.cUniversalRev > unsupported)
		{
		    LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"Universal Command Revision %d is not supported.\n",
						fromIdent.cUniversalRev);
			unsupported = fromIdent.cUniversalRev;
		}

		uniqueKey = ((llM & 0xFFFF)<<48) | ((llT & 0xFFFF)<<32) | ((llR& 0x00FF)<<16 | 0xFF);  
		//uniqueKey = ((fromIdent.wDeviceType   & 0xFFFF)<<16) | 
		//			((fromIdent.cDeviceRev    & 0x00FF)<< 8);// ddrev 2bDetermined 
		// as per Paul Baker's email of 7/3/7
		//*** indicates change made 28jan08 as per Wally's phone call to 'remove conditional'
		//***if ( fromIdent.wManufacturer > 0xff )
		//***{
        sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,(fromIdent.wManufacturer & 0xFFFF),
                OS_PATH_SEPR,(fromIdent.wDeviceType   & 0xFFFF),OS_PATH_SEPR);
		//***}
		//***else
		//***{
		//***sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,(fromIdent.wManufacturer & 0x00FF),
		//***									"\\",(fromIdent.wDeviceType   & 0x00FF),"\\");
		//***}
	}
//	else
//	{// error handling
//		uniqueKey = 0;
//		LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"Universal Command Revision %d is not supported.\n",
//			fromIdent.cUniversalRev);
//		return 0; // error return
//		//sprintf(tmpchDbdir,"%s%06x%s%04x%s",tmpchDbdir,
//		//									DEFAULT_MFG_05,"\\",DEFAULT_DEVTYPE_05,"\\");
//	}
	stl = strlen(tmpchDbdir);

	if (stl)
	{
		strncpy( retPath, tmpchDbdir, MaxPathLen-1);
		retPath[MaxPathLen-1] = '\0';// just-in-case
	}
	// else - MT, return zero as error length
	
	return (stl);
}
