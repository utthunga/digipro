#ifndef _PARSER_INFC /* FOR DDParser Integration */
#define _PARSER_INFC

#include "DDLDEFS.H"

#ifndef UINT
#define UINT unsigned int
#endif

/*************************************Already defined in DDLDEFS.H************/
#define XXX_PRE_COMMIT				1
#define XXX_POST_COMMIT				2
#define DEFAULT_ATTR_ID				255


#define MAX_FILE_NAME_PATH 255
extern UINT8 eFFVersionMajor;
extern UINT8 eFFVersionMinor;

extern int maxVer;

/*
 *	Macros used to limit the availability of some of the type subattributes
 *	based on type in eval_item_var().
 */
/*** these are in DDLDEFS.H *********
#define INVALID_VAR_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

#define INVALID_ARITH_TYPE_SUBATTR_MASK ~(VAR_ENUMS | VAR_INDEX_ITEM_ARRAY)

#define INVALID_ENUM_TYPE_SUBATTR_MASK ~(VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_INDEX_ITEM_ARRAY )

#define INVALID_STRING_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

#define INVALID_INDEX_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS )

#define INVALID_DATE_TIME_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

**********************************************************************************/

#include "ddbDeviceMgr.h"
#include "ddbDevice.h"
// no longer needed (it's empty)#include "ddb_UIInfc.h"

#include "Retn_Code.h"
#include "DDLItems.h"
#include "DDlDevDescription.h"
#include "DDlConditional.h"


/* HART supported Dictionaries                usage: char var[DICTCNT][DICTNAMELEN] = {DICTLIST};*/
/* standard MUST be zeroth string, buildDicitonary() should handle whatever you put here  */
#define DICTLIST {"standard.dct"},{"draeger.dct"},{"mmi.dct"},{"endress_hauser.dct"},{"siemens.dct"}
#define DICTCNT      5
#define DICTNAMELEN 20	

extern void dump_items(DDlDevDescription *pDevDesc);

extern genericTypeEnum_t getAttrType4Mask(CitemType& itmType, ulong aMask);


#ifdef _NEVER__ //#ifndef _DDPARSERINFC_
extern void fill_Payload(aCgenericConditional::aCexpressDest *,DDlAttribute *);
extern void fill_var_item_attributes(aCitemBase *,ItemAttrList&);
extern void fill_command_attributes(aCitemBase *,ItemAttrList&);
extern void fill_menu_attributes(aCitemBase *,ItemAttrList&);
extern void fill_edit_display_attributes(aCitemBase *,ItemAttrList&);
extern void fill_item_array_attributes(aCitemBase *,ItemAttrList&);
extern void fill_collection_attributes(aCitemBase *,ItemAttrList&);
extern void fill_refresh_attributes(aCitemBase *,ItemAttrList&);
extern void fill_unit_relation(aCitemBase *,ItemAttrList&);
extern void fill_wao_relation(aCitemBase *,ItemAttrList&);
extern void fill_method_attributes(aCitemBase *,ItemAttrList&);
extern void fill_record_attributes(aCitemBase *,ItemAttrList);
extern void fill_block_attributes(aCitemBase *,ItemAttrList);

extern void copy_ddlstring(aCddlString *,ddpSTRING *);
extern void fill_reference(aCreference *,REF *);
extern void fill_expression(aCexpression *,ddpExpression *);
extern void fill_conditional_attributes_list(aCcondList *paCond,DDlConditional *pCond);
extern void fill_conditional_attributes(aCconditional *paCond,DDlConditional *pCond);
extern void fill_enumlist(aCenumList *,ENUM_VALUE_LIST *);
extern void fill_menu_item_list(aCmenuList *tmpmenuList,MENU_ITEM_LIST *menuList);
extern void fill_transaction_list(aCattrCmdTrans *tmpCmdTrans,TRANSACTION_LIST* transList);
extern void fill_data_item_list(aCcondList *tmpcondLst, DATA_ITEM_LIST dataItemList);
extern void fill_response_code_list(aCcondList *tmpcondLst,RESPONSE_CODE_LIST *respList);
//extern void fill_eddisplaylist_list(aCedDispList *tmpeddisplst,REFERENCE_LIST *refList);
extern void fill_reference_list(AreferenceList_t *acreflist, REFERENCE_LIST *refList);
//extern void fill_item_array_element_list(aCItmArrElementList *tmpItmArray, ITEM_ARRAY_ELEMENT_LIST *itemArray);
//extern void fill_member_list(aCCollectionMemberList *tmpCollMemLst, MEMBER_LIST *memberList);
extern void fill_refresh_relation(aCreferenceList *tmpRefrelation,REFERENCE_LIST *refList);
extern void resolve_record_ref(unsigned long resl_member,unsigned long resl_recordid,aCreference *tmpaCref);

#endif

extern RETURNCODE buildDicitonary(CDictionary* dictionary);
//extern RETURNCODE getDevice  (UINT32 wrkingKey, aCdevice* pAbstractDev,CDictionary* dictionary); // recurse down the entire hierarchy
extern RETURNCODE getDevice  (DD_Key_t wrkingKey, aCdevice* pAbstractDev,
							  CDictionary* dictionary, LitStringTable* litStrings); // recurse down the entire hierarchy
//extern RETURNCODE getStdTableKey(Indentity_t& newIdent,DWORD& wrkingKey);
extern RETURNCODE getStdTableKey(Indentity_t& newIdent,DD_Key_t& wrkingKey);// widened key 26sep07 stevev
//extern RETURNCODE identifyDDfile(Indentity_t& deviceID, DWORD& deviceKey, StrVector_t** ppStrList = NULL);
extern RETURNCODE identifyDDfile(Indentity_t& deviceID, DD_Key_t& deviceKey, StrVector_t** ppStrList = NULL);
/*Vibhor 140404: Start of Code*/
/* depricated...use allocDeviceList 
extern RETURNCODE allocDeviceListForSDC(hCddbDevice *stdTbl,aCentry** ppEntryRoot );//ANOOP Added for invoking the DD Library from SDC
*/
extern RETURNCODE allocDeviceList(hCddbDevice *stdTbl,aCentry** ppEntryRoot );
/*Vibhor 140404: End of Code*/
/* VMKP Added on 040203 */
extern RETURNCODE handle_XXX_Pre_Post_Commit_Items(char *,BYTE *,UINT *);
extern void UpdateDevicePreWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId);
extern void UpdateDevicePostWriteAction(aCdevice* pAbstractDev,ITEM_ID nVarItemId,ITEM_ID nMethodItemId);
/* VMKP Added on 040203 */
extern	DDlDevDescription*  devDesc;


#endif
