//* Component History: 
//* 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003

#pragma warning (disable : 4786)

//sjv06feb06 #include <windows.h>
#include "ddbGeneral.h"

//#include <iostream>
//#include <fstream>
//#include <iomanip> 

//#include <algorithms>
#include "Char.h"

using namespace std;
#include "PrintData.h"
#include <algorithm>
#include "ddbdefs.h"
	
/*Vibhor 210105: Adding this Global var for tracking the item ID for conditional chunks*/
unsigned long item_id = 0;

extern CDictionary *pGlobalDict; /*The Global Dictionary object*/
extern LitStringTable *pLitStringTable; 
extern bool fm6;	// timj 10jan08 - true if the current binary file is BFF 6

extern char     itemStrings[ITMSTRCOUNT]  [ITMAXLEN];
extern wchar_t varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN];
extern char  varTypeStrings[VARTYPESTRCNT][VARTYPESTRMAXLEN];

/******************************************************************************************/

#if defined(_MSC_VER)
#if _MSC_VER < 1400	/* these are defined in VS 2005 */
std::ostream& operator<<(std::ostream& os, __int64 i )
{
    char buf[32];
    sprintf(buf,"%I64d", i );
    os << buf;
    return os;
}
std::ostream& operator<<(std::ostream& os, UINT64 i )
{
    char buf[32];
    sprintf(buf,"%I64u", i );
    os << buf;
    return os;
}


#endif
#endif // _MSC_VER

/******************************************************************************************/
void dump_blank(int i)
{
	while(i)
	{
		cout<<" ";
		i--;
	}

}

void dump_reference(ddpREFERENCE& ref)
{
	
	ddpREFERENCE :: iterator p;
	for(p = ref.begin();p != ref.end();p++)
	{
		if(p > ref.begin())
		{
			/*We are looping !!* Print on the next line now */
			
			cout<<"\n\t";
		}/*Endif*/
		switch(p->type)
		{
			case	ITEM_ID_REF:
				cout<< "\t Ref Type :ITEM_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case 	ITEM_ARRAY_ID_REF:
				cout<< "\t Ref Type :ITEM_ARRAY_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	COLLECTION_ID_REF:
				cout<< "\t Ref Type :COLLECTION_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	BLOCK_ID_REF:
				cout<< "\t Ref Type :BLOCK_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	VARIABLE_ID_REF:
				cout<< "\t Ref Type :VARIABLE_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	MENU_ID_REF:
				cout<< "\t Ref Type :MENU_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	EDIT_DISP_ID_REF:
				cout<< "\t Ref Type :EDIT_DISP_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	METHOD_ID_REF:
				cout<< "\t Ref Type :METHOD_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	REFRESH_ID_REF:
				cout<< "\t Ref Type :REFRESH_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	UNIT_ID_REF:
				cout<< "\t Ref Type :UNIT_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	WAO_ID_REF:
				cout<< "\t Ref Type :WAO_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	RECORD_ID_REF:
				cout<< "\t Ref Type :*RECORD_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case 	ARRAY_ID_REF:
				cout<< "\t Ref Type :ARRAY_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case 	VAR_LIST_ID_REF:
				cout<< "\t Ref Type :**VAR_LIST_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case 	PROGRAM_ID_REF:
				cout<< "\t Ref Type :**PROGRAM_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case 	DOMAIN_ID_REF:
				cout<< "\t Ref Type :**DOMAIN_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	RESP_CODES_ID_REF:
				cout<< "\t Ref Type :*RESP_CODES_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	VIA_ITEM_ARRAY_REF:
				cout<< "\t Ref Type :VIA_ITEM_ARRAY_REF \t\t Index:";
				dump_expression(p->val.index);
				break;
			case	VIA_COLLECTION_REF:
				cout<< "\t Ref Type :VIA_COLLECTION_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_RECORD_REF:	
				cout<< "\t Ref Type :*VIA_RECORD_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_ARRAY_REF:
				cout<< "\t Ref Type :VIA_ARRAY_REF \t\t Index:";
				dump_expression(p->val.index);
				break;
			case	VIA_VAR_LIST_REF:
				cout<< "\t Ref Type :**VIA_VAR_LIST_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_PARAM_REF:
				cout<< "\t Ref Type :**VIA_PARAM_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_PARAM_LIST_REF:
				cout<< "\t Ref Type :**VIA_PARAM_LIST_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_BLOCK_REF:
				cout<< "\t Ref Type :*VIA_BLOCK_REF";
				cout<< "\t Characteristic Rec. ID: " << p->val.id << "   (0x"<<hex<<p->val.id<<dec <<")";
				break;

				
			case	FILE_ID_REF		:
				cout<< "\t Ref Type :FILE_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	CHART_ID_REF	:
				cout<< "\t Ref Type :CHART_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	GRAPH_ID_REF	:
				cout<< "\t Ref Type :GRAPH_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	AXIS_ID_REF		:
				cout<< "\t Ref Type :AXIS_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	WAVEFORM_ID_REF	:
				cout<< "\t Ref Type :WAVEFORM_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	SOURCE_ID_REF	:
				cout<< "\t Ref Type :SOURCE_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	LIST_ID_REF		:
				cout<< "\t Ref Type :LIST_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;

			case	IMAGE_ID_REF		:
				cout<< "\t Ref Type :IMAGE_ID_REF";
				cout<< "\t Table Index: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	SEPARATOR_REF	:
				cout<< "\t Ref Type :SEPARATOR_REF \t\t -*-*-*-*-*-*-*-*-*-*-";
				break;
			case	CONSTANT_REF	:
				cout<< "\t Ref Type :CONSTANT_REF";
				cout<<"\tConstant:";
				dump_expression(p->val.index);
				break;
			case	VIA_FILE_REF	:
				cout<< "\t Ref Type :VIA_FILE_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_LIST_REF	:
				cout<< "\t Ref Type :VIA_LIST_REF\t\t Index:";
				dump_expression(p->val.index);
				break;
			case	VIA_BITENUM_REF	:
				cout<< "\t Ref Type :VIA_BITENUM_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	GRID_ID_REF	:
				cout<< "\t Ref Type :GRID_ID_REF";
				cout<< "\tRef ID: " << p->val.id <<"   (0x" << hex << p->val.id << dec<< ")";
				break;
			case	ROWBREAK_REF	:
				cout<< "\t Ref Type :NEWLINE_REF \t\t -+-+-+-+-+-+-+-+-+-+-";
				break;

			case	VIA_CHART_REF:
				cout<< "\t Ref Type :VIA_CHART_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_GRAPH_REF:
				cout<< "\t Ref Type :VIA_GRAPH_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_SOURCE_REF:
				cout<< "\t Ref Type :VIA_SOURCE_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;
			case	VIA_ATTR_REF:
				cout<< "\t Ref Type :VIA_ATTR_REF";
				cout<< "\t Member: " << p->val.member << "   (0x"<<hex<<p->val.member<<dec <<")";
				break;

			default:
				cout<< "\t Ref Type :UNKNOWN \t\t Type:"<<p->type<<"  (0x"<<hex<<p->type<<dec <<")";
				break;
		}/*End switch*/
	}/*End for*/
			
}/*End dump_reference*/


void dump_string(ddpSTRING * Str)
{
	//ADDED By Deepak
	if(NULL== Str)
		return;

	switch(Str->strType)
	{
		case 	DEV_SPEC_STRING_TAG:
		case	DICTIONARY_STRING_TAG: 
			cout<<"\t\""<< ((Str->str != NULL) ? Str->str : "NULL") << "\" ";
			break;
		case	VARIABLE_STRING_TAG:  
			cout<<"\t\"Var String ID : "<<Str->varId <<"   (0x" <<hex<<Str->varId<<dec<< ")\" ";
			break;
		case	VAR_REF_STRING_TAG:
			cout<<"\"";
			dump_reference(Str->varRef);
			cout<<"\"";
			break;
		case	ENUMERATION_STRING_TAG:
			cout<<"\t\" Enum String ID : "<<Str->enumStr.enmVar.iD<<"   (0x"<<hex<<Str->enumStr.enmVar.iD<<dec<<")";
			cout<<" \tValue: "<<Str->enumStr.enumValue<<"   (0x"<<hex<<Str->enumStr.enumValue<<dec<<")\"";
			break;
		case	ENUM_REF_STRING_TAG:
			cout<<"\" Enum Ref:";
			dump_reference(*(Str->enumStr.enmVar.ref));
			cout<<" \tValue: "<<Str->enumStr.enumValue<<"   (0x"<<hex<<Str->enumStr.enumValue<<dec<<")\"";
			break;
		default:
			cout<<"* Unknown String Type *";
			break;
		
	}/*End switch*/
	
}/*End dump_string */


void dump_type_size(TYPE_SIZE typeSize)
{
	switch(typeSize.type)
	{
		case 	INTEGER: 
			cout<<"\t INTEGER  , size : "<<typeSize.size<<endl;
			break;
		case	UNSIGNED:
			cout<<"\t UNSIGNED  , size : "<<typeSize.size<<endl;
			break;
		case	FLOATG_PT:
			cout<<"\t FLOAT  , size : "<<typeSize.size<<endl;
			break;
		case	DOUBLE_FLOAT: 	
			cout<<"\t DOUBLE  , size : "<<typeSize.size<<endl;
			break;
		case	ENUMERATED:
			cout<<"\t ENUMERATED  , size : "<<typeSize.size<<endl;
			break;
		case	BIT_ENUMERATED:
			cout<<"\t BIT_ENUMERATED  , size : "<<typeSize.size<<endl;
			break;
		case	INDEX:
			cout<<"\t INDEX  , size : "<<typeSize.size<<endl;
			break;
		case	ASCII:
			cout<<"\t ASCII  , size : "<<typeSize.size<<endl;
			break;
		case	PACKED_ASCII:
			cout<<"\t PACKED_ASCII  , size : "<<typeSize.size<<endl;
			break;
		case	PASSWORD:
			cout<<"\t PASSWORD  , size : "<<typeSize.size<<endl;
			break;
		case	HART_DATE_FORMAT:
			cout<<"\t HART_DATE_FORMAT  , size : "<<typeSize.size<<endl;
			break;
#ifdef FF  // 12dec07 timj
		case	BITSTRING:
			cout<<"\t BITSTRING  , size : "<<typeSize.size<<endl;
			break;
		case	TIME:
			cout<<"\t TIME  , size : "<<typeSize.size<<endl;
			break;
		case	DATE_AND_TIME:
			cout<<"\t DATE_AND_TIME  , size : "<<typeSize.size<<endl;
			break;
		case	DURATION:
			cout<<"\t DURATION  , size : "<<typeSize.size<<endl;
			break;
		case 	EUC:
			cout<<"\t EUC  , size : "<<typeSize.size<<endl;
			break;
		case	OCTETSTRING:
			cout<<"\t OCTETSTRING  , size : "<<typeSize.size<<endl;
			break;
		case	VISIBLESTRING:	
			cout<<"\t VISIBLESTRING  , size : "<<typeSize.size<<endl;
			break;
#endif
		case	TIME_VALUE:		
			cout<<"\t TIME_VALUE  , size : "<<typeSize.size<<endl;
			break;
		case	BOOLEAN_T:
			cout<<"\t BOOLEAN  , size : "<<typeSize.size<<endl;
			break;
		default:
			cout<<"* Unknown Variable Type *";
		/*surprise!!!*/
			break;
	}/*End switch*/

}/*End dump_type_size */

void dump_debug_info(ITEM_DEBUG_INFO* pDbg)
{
	if ( pDbg == NULL )
	{
		cout<<" * No debug information."<<endl;
		return ;
	}

	cout<<"\t\""<<pDbg->symbol_name.c_str()<<"\""<<endl;
	cout<<"\t File:";
	dump_string(&(pDbg->file_name));
	cout<<"\tLine: "<<pDbg->lineNo<<"\tFlags: 0x"<<hex<<pDbg->flags<<dec<<endl;
	/* TODO: decode flags: imp,std,man etc */
	if (pDbg->attr_list.size() > 0 )
	{
		ATTR_DEBUG_LIST::iterator attrDbgIT;
		for (attrDbgIT = pDbg->attr_list.begin(); attrDbgIT != pDbg->attr_list.end();++attrDbgIT)
		{// ptr2a ATTR_DEBUG_INFO_T ptr
			cout<<"\t\tAttribute 0x"<<hex<<(*attrDbgIT)->attr_tag<<dec;
			cout<<"\tFile:";
			dump_string(&((*attrDbgIT)->attr_filename));
			cout<<"\tLine: "<<(*attrDbgIT)->attr_lineNo<<endl;

			if ((*attrDbgIT)->attr_member_list.size() > 0 )
			{
				int cnt = 0;
				MEMBER_DEBUG_LIST::iterator memDbgIT;
				for (memDbgIT  = (*attrDbgIT)->attr_member_list.begin(); 
					 memDbgIT != (*attrDbgIT)->attr_member_list.end();		++memDbgIT, cnt++)
				{// ptr2a MEMBER_DEBUG_T
					cout<<"\t\t"<<cnt<<")\t0x"<<hex<<memDbgIT->member_value<<dec;
					cout<<"\t"<<memDbgIT->symbol_name.c_str();
					if (memDbgIT->flags != 0)
					{
						cout<<"\tFlags: 0x"<<hex<<memDbgIT->flags<<dec;
						/* TODO: decode flags: imp,std,man etc */
					}
					cout<<endl;
				}// next member
			}// else - no members, do nothing
		}// next attribute
	}// else - no attribute data, do nothing else
	return;
}


void dump_enumlist(ENUM_VALUE_LIST *enmList)
{
	ENUM_VALUE_LIST :: iterator p;
	ENUM_VALUE		*enmVal;

	//Addded by Deepak
	if(NULL==enmList)
		return;

	cout<<"\t\t Enum Count: "<<enmList->size()<<endl;

	for(p = enmList->begin(); p != enmList->end(); p++)
	{
		enmVal = (ENUM_VALUE*)&(*p);// PAW &(*) 03/03/09
		cout<<"Val:  0x"<<hex<<enmVal->val<<dec<<"\tDesc:  \"";
		dump_string(&(enmVal->desc));
		cout<<"\"\n";
		cout<<"Help:\"";
		dump_string(&(enmVal->help));
		cout<<"\"\n";
		cout<<"Func_Class:  0x"<<hex<<enmVal->func_class<<dec<<"\t";
		cout<<"Action_Method:  "<<enmVal->actions<<"   (0x"<<hex<<enmVal->actions<<dec<<")\n";
		cout<<"Status_Class:  0x"<<hex<<enmVal->status.status_class<<dec<<"\t";
		cout<<"  OutputClass_Count: "<<enmVal->status.oclasses.size()<<endl;
		if(enmVal->status.oclasses.size() > 0)
		{
			OUTPUT_STATUS_LIST stList = enmVal->status.oclasses;
			OUTPUT_STATUS_LIST :: iterator it;
			for(it = stList.begin();it != stList.end();it++)
			{
				switch(it->kind)
				{
					case OC_DV:
						cout<<"\"Kind_nWhich : DV"<<it->which<<"\t";
						break;
					case OC_TV:
						cout<<"\"Kind_nWhich : TV"<<it->which<<"\t";
						break;
					case OC_AO:
						cout<<"\"Kind_nWhich : AO"<<it->which<<"\t";
						break;
					default:
						cout<<"\"Kind_nWhich : ALL\t";
						break;
				}/*End Switch*/

				switch(it->oclass)
				{
					case 00:
						cout<<"Mode_nReliability: AUTO & GOOD\"\n";
						break;
					case 01:
						cout<<"Mode_nReliability: MANUAL & GOOD\"\n";
						break;
					case 02:
						cout<<"Mode_nReliability: AUTO & BAD\"\n";
						break;
					case 03:
						cout<<"Mode_nReliability: MANUAL & BAD\"\n";
						break;
					/* stevev add marginal types */
					case 04:
						cout<<"Mode_nReliability: AUTO & MARGINAL\"\n";
						break;
					case 05:
						cout<<"Mode_nReliability: MANUAL & MARGINAL\"\n";
						break;
					case 06:
						cout<<"Mode_nReliability: AUTO & BAD & MARGINAL (ILLEGAL)\"\n";
						break;
					case 07:
						cout<<"Mode_nReliability: MANUAL &  BAD & MARGINAL (ILLEGAL)\"\n";
						break;
					/* end marginal types */
					default:
						cout<<"\"\n";
						break;
				}/*End switch*/

			}/*Endfor it*/
			
		}/*Endif size*/

	cout<<"\t\t\t\t------------------------\n";
	}/*End for iterator p*/
	
}/* End dump_enumlist */


void dump_reference_list(REFERENCE_LIST *refList)
{
	REFERENCE_LIST :: iterator p;
	ddpREFERENCE	ref;
	
	cout<<"\t\t List Size: "<<refList->size()<<endl;
	int i = 0;
	for(p = refList->begin();p != refList->end();p++)
	{
		// stevev 23mar09 try a different approach...  ref = *p;
		cout<<i++<<")";
//		dump_reference(ref); steve 23mar09
		dump_reference( *((ddpREFERENCE*)&(*p)) );
		cout<<endl;
	}/*End for*/
}/*End dump_reference_list*/

void dump_data_item_list(DATA_ITEM_LIST& dataItemList)
{
	DATA_ITEM_LIST :: iterator p;
	DATA_ITEM dataItem;
	
	cout<<"\t\t Item Count: "<<dataItemList.size()<<endl;

	int i = 0;
	for(p = dataItemList.begin();p != dataItemList.end(); p++)
	{
		cout<<i++<<")";
		dataItem = *p;
		switch(dataItem.type)
		{
			case 	DATA_CONSTANT:
				cout<<"\tIntConst:  "<<dataItem.data.iconst<<"   (0x"<<hex<<dataItem.data.iconst<<dec<<")";
				break;
			case	DATA_REFERENCE:
				dump_reference(*(dataItem.data.ref));
				break;
			case	DATA_REF_FLAGS:
				dump_reference(*(dataItem.data.ref));
				cout<<"\t Flags:   0x"<<dataItem.flags;
				break;
			case	DATA_REF_WIDTH:
				dump_reference(*(dataItem.data.ref));
				cout << "\t Width:   " << dataItem.mask;  // width;
				break;
			case	DATA_REF_FLAGS_WIDTH:
				dump_reference(*(dataItem.data.ref));
				cout<<"\t Flags:   0x"<<dataItem.flags;
				cout<<"\t Width:   "<<dataItem.mask;  // width;
				break;
			case	DATA_FLOATING:
				cout<<"\t FltConst:  "<<dataItem.data.fconst;
			default:
				/*Should not come here!!!*/
					cout<<"* Unknown DataItem Type 8*";
				break;
		}/*End switch*/
		/*Go to the next line*/
		cout<<endl;
	}/*End for*/

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_data_item_list*/

void dump_response_code_list(RESPONSE_CODE_LIST *respList)
{
	RESPONSE_CODE_LIST :: iterator p;
	RESPONSE_CODE	respCode;
	int i =0;

	cout<<"\t\t List Size: "<<respList->size()<<endl;
	//Added By Deepak 
	if(NULL ==respList)
		return;

	for(p = respList->begin();p != respList->end();p++)
	{
		cout<<i++<<")";
		respCode = *p;
		cout<<"\tValue:  0x"<<respCode.val;
		cout<<"\tType:  0x"<<respCode.type<<endl;
		cout<<"\tDesc:  \"";
		dump_string(&respCode.desc);
		cout<<"\"\n";
		if(respCode.evaled & RS_HELP_EVALED)
		{
			cout<<"\tHelp:  \"";
			dump_string(&respCode.help);
			cout<<"\"\n";

		}/*End if*/

	}/*End for*/
	cout<<"\t\t\t\t------------------------\n";

}/*End dump_response_code_list*/



void dump_transaction_list(TRANSACTION_LIST* transList)
{
	TRANSACTION_LIST :: iterator p;
	TRANSACTION trans;
	int i = 0;
	
	cout<<"\t\t List Size: "<<transList->size()<<endl;

	if(NULL ==transList)
		return;
	for(p = transList->begin();p != transList->end();p++)
	{
		cout<<i++<<")";
		trans = *p;
		cout<<"\tNumber:  "<<trans.number<<endl;
		cout<<"\tRquest: ";
		dump_data_item_list(trans.request);
		cout<<"\tReply:  ";
		dump_data_item_list(trans.reply);
		cout<<"\tResponse Codes:  ";
		dump_response_code_list(&trans.rcodes);
#ifdef XMTR
		if (trans.post_rqst_rcv_act.size())
		{
			cout<<"\tPost Request Actions:  ";
			dump_reference_list(&trans.post_rqst_rcv_act);
		}
#endif
	}/*Endfor p*/

}/*End dump_transaction_list*/


void dump_menu_item_list(MENU_ITEM_LIST* menuList)
{
	MENU_ITEM_LIST :: iterator p;
	MENU_ITEM menuItem;
	int i = 0;
	
	cout<<"\t\t Item Count: "<<menuList->size()<<endl;

	if(NULL ==menuList)
	return;

	for(p = menuList->begin(); p != menuList->end(); p++)
	{
		cout<<i++<<")";
		// stevev 23mar09 try a different approach...  menuItem = *p;
		//dump_reference(menuItem.item);
		dump_reference( ((MENU_ITEM*)&(*p))->item ); // stevev 23mar09
		cout<<"\n\t Qualifier:  0x"<<menuItem.qual<<endl;
	}/*End for*/

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_transaction_list*/


void dump_param(METHOD_PARAM* pParam)
{	
	
	if ( pParam->param_modifiers & CONSTFLAG )
	{
		cout<<" const ";
	}
	if ( pParam->param_modifiers & REFECFLAG )
	{
		cout<<" reference ";
	}

	switch (pParam->param_type)
	{
	case TYPE_VOID:		cout<<" void  ";		break;
	case TYPE_INT__8:	cout<<" char  ";		break;
	case TYPE_INT_16:	cout<<" short ";		break;
	case TYPE_INT_32:	cout<<" int   ";		break;
	case TYPE_FLOAT	:	cout<<" float ";		break;
	case TYPE_DOUBLE:	cout<<" double ";		break;
	case TYPE_UINT__8 :	cout<<" uchar ";		break;
	case TYPE_UINT_16 :	cout<<" ushort ";		break;
	case TYPE_UINT_32:	cout<<" uint  ";		break;
	case TYPE_INT_64 :	cout<<" int64 ";		break;
	case TYPE_UINT_64:	cout<<" uint64 ";		break;
	case TYPE_DDSTRING:	cout<<" ddString ";		break;
	case TYPE_DD_ITEM :	cout<<" ddItem ";		break;
	default:
		cout<<" Unknown ";
		break;
	}//endswitch
	

	if ( pParam->param_name != NULL )
	{
		cout<<"\t"<<pParam->param_name;
	}
	else
	{
		cout<<"\tERROR: no variable name.";
	}

	if ( pParam->param_modifiers & ARRAYFLAG )
	{
		cout<<" array \n";
	}
	else
	{
		cout<<"\n";
	}

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_param_list*/


void dump_param_list(METHOD_PARAM_LIST* pParamList)
{
	METHOD_PARAM_LIST :: iterator p;
	
	cout<<"\t\t Item Count: "<<pParamList->size()<<endl;

	int i = 0;
	for(p = pParamList->begin();p != pParamList->end(); p++)
	{
		cout<<i++<<")";
//#if _MSC_VER >= 1300   // HOMZ - port to 2003 / VS 7
					   // Removed due to error C2440:
					   // dump_parm expects p to be a METHOD_PARAM*, not a list	
		//dump_param(&(*p));
		dump_param((METHOD_PARAM*)&(*p));
//#else /* was */
//		dump_param((METHOD_PARAM*)p);
//#endif

	}/*End for*/

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_param_list*/


void dump_refresh_relation(REFRESH_RELATION* refReln)
{
	
	if(NULL ==refReln)
	return;

	cout<<endl;
	cout<<"Watch Items:";
	dump_reference_list(&(refReln->watch_list) );
	cout<<"Update Items:";
	dump_reference_list(&(refReln->update_list));

}/*End dump_refresh_relation*/


void dump_unit_relation(UNIT_RELATION* unitReln)
{
	if(NULL ==unitReln)
	return;

	cout<<endl;
	cout<<"Unit Variable:";
	dump_reference(unitReln->unit_var);
	cout<<endl;
	cout<<"Variables with Above Units:";
	dump_reference_list(&unitReln->var_units);
}/*End dump_unit_relation*/


void dump_item_array_element_list(ITEM_ARRAY_ELEMENT_LIST *itemArray)
{
	ITEM_ARRAY_ELEMENT_LIST	:: iterator p;
	ITEM_ARRAY_ELEMENT* pElement;// stevev 23mar09
	// stevev 23mar09 -- all references to element have been modified to pElement
	//                   with requisite changes to references/dereferences
	int i = 0;

	cout<<"\t\t List Count: "<<itemArray->size()<<endl;

	if(NULL ==itemArray)
	return;

	for(p = itemArray->begin();p != itemArray->end();p++)
	{
		pElement = (ITEM_ARRAY_ELEMENT*)&(*p);// stevev 23mar09
		cout<<i++<<")";
//		element = *p;// stevev 23mar09
		cout<<"\tIndex:  "<<pElement->index<<"   (0x"<<hex<<pElement->index<<dec<<")\n";
		cout<<"\tItem:";
		dump_reference(pElement->item);
		cout<<endl;
		if(pElement->evaled & IA_DESC_EVALED)
		{
			cout<<"\tDesc:  \"";
			dump_string(&(pElement->desc));
			cout<<"\"\n";
		}
		if(pElement->evaled & IA_HELP_EVALED)
		{
			cout<<"\tHelp:  \"";
			dump_string(&(pElement->help));
			cout<<"\"\n";
		}

	}/*End for*/
	cout<<"\t\t\t\t------------------------\n";

}/*End dump_item_array*/


void dump_member_list(MEMBER_LIST *memberList)
{
	MEMBER_LIST	:: iterator p;
	MEMBER * pMember; // stevev 23mar09 - all subsequebt references to member are
					  // changed to pMember with requisite changes to references/dereferences
	int i = 0;
	
	cout<<"\t\t List Count: "<<memberList->size()<<endl;

	if(NULL ==memberList)
	return;

	for(p = memberList->begin();p != memberList->end();p++)
	{
		cout<<i++<<")";
		pMember = (MEMBER *)&(*p);
		cout<<"\tName:  "<<pMember->name<<"   (0x"<<hex<<pMember->name<<dec<<")";
		if ( ! pMember->member_name.empty() )
		{
			cout << "      '" << pMember->member_name << "' \n";  //timj
		}
		else
		{
			cout << "\n";
		}
		cout<<"\tItem:";
		dump_reference(pMember->item);
		cout<<endl;
		if(pMember->evaled & MEM_DESC_EVALED)
		{
			cout<<"\tDesc:  \"";
			dump_string(&(pMember->desc));
			cout<<"\"\n";
		}
		if(pMember->evaled & MEM_HELP_EVALED)
		{
			cout<<"\tHelp:  \"";
			dump_string(&(pMember->help));
			cout<<"\"\n";
		}
	}/*End for*/
	cout<<"\t\t\t\t------------------------\n";


}/*End dump_member_list*/


void dump_grid_list(GRID_SET_LIST* pGridList)
{
	GRID_SET_LIST :: iterator p;
	GRID_SET* gridSet;
	int i = 0;
	

	if(NULL ==pGridList)
		return;
	cout<<"\t\t Item Count: "<<pGridList->size()<<endl;

	for(p = pGridList->begin(); p != pGridList->end(); p++)
	{
		cout<<i++<<")";
		gridSet = (GRID_SET*)&(*p);// PAW &(*) 03/03/09
		dump_string(&(gridSet->desc));
		dump_reference_list(&(gridSet->values));
	}/*next*/

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_grid_list*/


void dump_expression(ddpExpression *pExpr)
{
	ddpExpression :: iterator p;
	Element * pElement;// stevev 23mar09 -- all references to element have been modified to 
	//                           pElement with requisite changes to references/dereferences
	int i = 0;

	//Added By Deepak
	if(NULL ==pExpr)
	return;

	for(p = pExpr->begin(); p != pExpr->end(); p++)
	{
//		element = *p;// stevev 23mar09
		pElement = (Element *)&(*p);// see above
		cout<<"\t <"<<i++<<">  ";
		switch(pElement->byElemType)
		{

			case	NOT_OPCODE:				/* 1 */
				cout<<"NOT";
				break;
			case	NEG_OPCODE:				/* 2 */
				cout<<"NEG";
				break;
			case	BNEG_OPCODE:			 /* 3 */
				cout<<"BNEG";
				break;
			case	ADD_OPCODE:				/* 4 */
				cout<<"ADD";
				break;
			case	SUB_OPCODE:				 /* 5 */
				cout<<"SUB";
				break;
			case	MUL_OPCODE:				/* 6 */
				cout<<"MULT";
				break;
			case	DIV_OPCODE:				/* 7 */
				cout<<"DIV";
				break;
			case	MOD_OPCODE:				/* 8 */
				cout<<"MOD";
				break;
			case	LSHIFT_OPCODE:			/* 9 */ 
				cout<<"LEFT_SHIFT";
				break;
			case	RSHIFT_OPCODE:			/* 10 */
				cout<<"RIGHT_SHIFT";
				break;
			case	AND_OPCODE:				/* 11 */
				cout<<"AND";
				break;
			case	OR_OPCODE:				/* 12 */
				cout<<"OR";
				break;
			case	XOR_OPCODE:				/* 13 */
				cout<<"XOR";
				break;
			case	LAND_OPCODE:			/* 14 */
				cout<<"LOGICAL_AND";
				break;
			case	LOR_OPCODE:				/* 15 */
				cout<<"LOGICAL_OR";
				break;
			case	LT_OPCODE:				/* 16 */
				cout<<"LESSTHAN";
				break;
			case	GT_OPCODE:				/* 17 */
				cout<<"GREATERTHAN";
				break;
			case	LE_OPCODE:				/* 18 */
				cout<<"LESSTHAN_OR_EQUALTO";
				break;
			case	GE_OPCODE:				/* 19 */
				cout<<"GREATERTHAN_OR_EQUALTO";
				break;
			case	EQ_OPCODE:				/* 20 */
				cout<<"EQUALTO";
				break;
			case	NEQ_OPCODE:				/* 21 */
				cout<<"NOT_EQUALTO";
				break;
			case	INTCST_OPCODE:			/* 22 */
				char buf[32];
				sprintf(buf,"%I64u", pElement->elem.ulConst );
				cout<<"INTCONST:\t "<< buf;// PAW 03/03/09
//				cout<<"INTCONST:\t "<< element.elem.ulConst;// PAW see above
				break;
			case	FPCST_OPCODE:			/* 23 */
				cout<<"FPCONST:\t "<<pElement->elem.fConst;
				break;
			case	VARID_OPCODE:			/* 24 */
				cout<<"VARID:\t "<<pElement->elem.varId<<"  (0x"<<hex<<pElement->elem.varId<<dec<<")";
				break;
			case	MAXVAL_OPCODE:			/* 25 */
				cout<<"MAX"<<pElement->elem.minMax->which<<" OF (0x"<<pElement->elem.minMax->variable.id<<")";
				break;
			case	MINVAL_OPCODE:			/* 26 */
				cout<<"MIN"<<pElement->elem.minMax->which<<" OF (0x"<<pElement->elem.minMax->variable.id<<")";
				break;
			case	VARREF_OPCODE:			/* 27 */
				cout<<"VARREF:";
				dump_reference(*(pElement->elem.varRef));
				break;
			case	MAXREF_OPCODE:			/* 28 */
				cout<<"MAX"<<pElement->elem.minMax->which<<" OF \t";
				if ( pElement->elem.minMax->isID )
					cout<<"(0x"<<pElement->elem.minMax->variable.id<<")";
				else
					dump_reference(*(pElement->elem.minMax->variable.ref));
				break;
			case	MINREF_OPCODE:			/* 29 */
				cout<<"MIN"<<pElement->elem.minMax->which<<" OF \t";
				dump_reference(*(pElement->elem.minMax->variable.ref));
				break;
			case	BLOCK_OPCODE:			/* 30 */
			case	BLOCKID_OPCODE:			/* 31 */
			case	BLOCKREF_OPCODE:		/* 32 */
				cout<<"UNSUPPORTED-BLOCK-OPCODE:\t *******************";
				break;
			case	STRCST_OPCODE:			/* 33 */
				if(pElement->elem.pSTRCNST)
				{
					cout<<"STRCONST:";
				  dump_string(pElement->elem.pSTRCNST);
				}  // #|"<<element.elem.pSTRCNST<<"|";
				else
					cout<<"STRCONST:***ERROR*** NULL string pointer in constant";
				break;
			case	SYSTEMENUM_OPCODE:		/* 34 */
				cout<<"SYSTEMENUM_OPCODE:\t *******************";
				break;
/*** these are now a reference by attribute ***
			case	CNTREF_OPCODE:			/x* 35 *x/
			case	CAPREF_OPCODE:			/x* 36 *x/
			case	FSTREF_OPCODE:			/x* 37 *x/
			case	LSTREF_OPCODE:			/x* 38 *x/
				cout<<"LIST-SPECIFIC-OPCODE:\t *******************";
				break;
*** end 21apr05 stevev */
			default:
				cout<<"UNKNOWN-OPCODE:\t *******************";
				break;

		}/*End switch*/

		cout<<endl;

	}/*End for */

}/*End dump_expression*/


/* stevev 24aug06 - min-max is not conditional, values(expressions) are  **/
void dump_min_max_list(MIN_MAX_LIST *minMaxList)
{
	MIN_MAX_LIST :: iterator p;
	MIN_MAX_VALUE minMax;
	int i = 0;
	
	//Added By Deepak
	if(NULL ==minMaxList)
	return;

	cout<<"\t\t List Size: "<<minMaxList->size()<<endl;

	for(p = minMaxList->begin();p != minMaxList->end(); p++)
	{
		//minMax = *p;
		//cout<<i++<<")";
		//cout<<"Which:  "<<minMax.which<<endl;
		//cout<<"Value: \n";
		//dump_expression(&minMax.value);
		cout<<i++<<")";
		cout<<"Which:  "<<p->which<<endl;
		cout<<"Value: \n";
// not doable at this time...		dump_conditional(p->pCond);
		if (p->pCond->listOfChilds.size() > 0)
			cout<<"Conditional - No output available at this time."<<endl;
		else
			cout<<"Is direct value."<<endl;
	}/*End for p*/

	cout<<"\t\t\t\t------------------------\n";

}/*End dump_min_max_list*/

void dump_line_type(LINE_TYPE lineType)
{
	switch(lineType.type)
	{
	
		case	DATA_LINETYPE:
			{
				if(lineType.qual > 0)
				{
					cout<<"DATA"<<lineType.qual ;
				}
				else
				{
					cout<<"DATA"<<endl;;
				}
			}
			break;
		case	LOWLOW_LINETYPE:
			{
				cout<<"LOW_LOW_LIMIT"<<endl;;
			}
			break;
		case	LOW_LINETYPE:
			{
				cout<<"LOW_LIMIT"<<endl;;
			}
			break;
		case	HIGH_LINETYPE:
			{
				cout<<"HIGH_LIMIT"<<endl;;
			}
			break;
		case	HIGHHIGH_LINETYPE:
			{
				cout<<"HIGH_HIGH_LIMIT"<<endl;;
			}
			break;
		case	TRANSPARENT_LINETYPE:
			{
				cout<<"TRANSPARENT"<<endl;;
			}
			break;
		default:
			cout<<"UNKNOWN_LINE_TYPE"<<endl;;
			break;
	
	}/*End switch*/

}/*End dump_line_type*/

void dump_conditional_attribute(DDlConditional *pCond, int indent)
{
	int iSectionIndex = 0; /*We will use this to index into isSectionConditionalList */
	int iChildIndex = 0; /*We will use this to index into  listOfChilds*/
	int iValueIndex = 0; /*We will use this to index into Vals vector*/
	int iChunkIndex = 0; /*We will use this to index into listOfChunks*/
	
	//Added By Deepak
	if(NULL ==pCond)
	return;

	switch(pCond->condType)
	{
		case DDL_COND_TYPE_IF:
			{
				dump_blank(indent);
				cout<<"IF\n";
				dump_expression(&pCond->expr);
				dump_blank(indent+5);
				cout<<"THEN  {\n";
				if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)//Vibhor 200105: Changed
				{
					dump_blank(indent+7);
					switch(pCond->attrDataType)
					{
						case 	DDL_ATTR_DATA_TYPE_INT:
// PAW start 03/03/09
							char buf[32], buf1[32];
							sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).llVal );
							sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).llVal );
							cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW 03/03/09 see below
//							cout<<"	"<<pCond->Vals.at(iValueIndex).llVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).llVal<<dec<<")\n"; PAW 03/03/09 see above
// PAW end 03/03/09
							break;
						case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
// PAW start 03/03/09
							sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
							sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
							cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW 03/03/09 see below
//							cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n"; 
// PAW end 03/03/09
							break;
						case	DDL_ATTR_DATA_TYPE_FLOAT:
						case	DDL_ATTR_DATA_TYPE_DOUBLE:
							cout<<" "<<pCond->Vals.at(iValueIndex).fVal<<endl;
							break;
						case	DDL_ATTR_DATA_TYPE_BITSTRING :
// PAW start 03/03/09
							sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
							sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
							cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//							cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n";
							break;
						case	DDL_ATTR_DATA_TYPE_STRING:
							dump_string(pCond->Vals.at(iValueIndex).strVal);
							cout<<endl; /*Go to the next line for the next attribute!!*/
							break;
						case	DDL_ATTR_DATA_TYPE_ITEM_ID:
							cout<<"	"<<pCond->Vals.at(iValueIndex).id<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).id<<dec<<")\n";
							break;
						case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
							dump_enumlist(pCond->Vals.at(iValueIndex).enmList);
							break;

						case	DDL_ATTR_DATA_TYPE_REFERENCE :
							dump_reference(*(pCond->Vals.at(iValueIndex).ref));
							cout<<endl;
							break;
						case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
							dump_reference_list(pCond->Vals.at(iValueIndex).refList);
							break;
						case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
							dump_response_code_list(pCond->Vals.at(iValueIndex).respCdList);
							break;
						case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
							dump_menu_item_list(pCond->Vals.at(iValueIndex).menuItemsList);
							break;
						case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
							dump_refresh_relation(pCond->Vals.at(iValueIndex).refrshReln);
							break;
						case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
							dump_unit_relation(pCond->Vals.at(iValueIndex).unitReln);
							break;
						case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
							dump_item_array_element_list(pCond->Vals.at(iValueIndex).itemArrElmnts);
							break;
						case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
							dump_member_list(pCond->Vals.at(iValueIndex).memberList);
							break;
						case	DDL_ATTR_DATA_TYPE_EXPRESSION:
							cout<<endl;
							dump_expression(pCond->Vals.at(iValueIndex).pExpr);
							break;
						case	DDL_ATTR_DATA_TYPE_MIN_MAX:
							dump_min_max_list(pCond->Vals.at(iValueIndex).minMaxList);
							break;
						case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
							dump_line_type(pCond->Vals.at(iValueIndex).lineType);
							break;
						case	DDL_ATTR_DATA_TYPE_GRID_SET:
							dump_grid_list(pCond->Vals.at(iValueIndex).gridMemList);
							break;
						default:
							cout<<"* Unknown Data Type 4*";
					/*Surprise!!!*/
						break;
					}/*End switch*/
					
				}/*End if */
				else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL)//Vibhor 200105: Changed
				{
					dump_conditional_attribute(pCond->listOfChilds.at(iChildIndex),(indent+7));
					iChildIndex++;
					
				}/*End else*/
				else
				{ //this section of conditional has chunks
					dump_section_chunks(pCond->listOfChunks.at(iChunkIndex));
					iChunkIndex++;
					clog<<"Item  0x" <<hex<<item_id <<dec<<"  has section chunks" <<endl;
				}

				cout<<endl;
				dump_blank(indent+7);
				cout<<"}/*End IF*/\n";

				/*Just check if we have ELSE part of IF, if yes then dump it too*/
				if(pCond->byNumberOfSections == 2)
				{
					iSectionIndex++;
					iValueIndex++;
					dump_blank(indent+5);
					cout<<"ELSE  {\n";
					if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)//Vibhor 200105: Changed
					{
						/*increment the section index*/
						
						dump_blank(indent+7);
						switch(pCond->attrDataType)
						{
							case 	DDL_ATTR_DATA_TYPE_INT:
// PAW start 03/03/09
								char buf[32], buf1[32];
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).llVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).llVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).llVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).llVal<<dec<<")\n";
// PAW end 03/03/09
								break;
							case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
// PAW start 03/03/09
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n";
// PAW end 03/03/09

								break;
							case	DDL_ATTR_DATA_TYPE_FLOAT:
							case	DDL_ATTR_DATA_TYPE_DOUBLE:
								cout<<" "<<pCond->Vals.at(iValueIndex).fVal<<endl;
								break;
							case	DDL_ATTR_DATA_TYPE_BITSTRING :
// PAW start 03/03/09
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n";
// PAW end 03/03/09

								break;
							case	DDL_ATTR_DATA_TYPE_STRING:
								dump_string(pCond->Vals.at(iValueIndex).strVal);
								cout<<endl; /*Go to the next line for the next attribute!!*/
								break;
							case	DDL_ATTR_DATA_TYPE_ITEM_ID:
								cout<<"	"<<pCond->Vals.at(iValueIndex).id<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).id<<dec<<")\n";
								break;
							case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
								dump_enumlist(pCond->Vals.at(iValueIndex).enmList);
								break;

							case	DDL_ATTR_DATA_TYPE_REFERENCE :
								dump_reference(*(pCond->Vals.at(iValueIndex).ref));
								cout<<endl;
								break;
							case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
								dump_reference_list(pCond->Vals.at(iValueIndex).refList);
								break;
							case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
								dump_response_code_list(pCond->Vals.at(iValueIndex).respCdList);
								break;
							case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
								dump_menu_item_list(pCond->Vals.at(iValueIndex).menuItemsList);
								break;
							case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
								dump_refresh_relation(pCond->Vals.at(iValueIndex).refrshReln);
								break;
							case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
								dump_unit_relation(pCond->Vals.at(iValueIndex).unitReln);
								break;
							case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
								dump_item_array_element_list(pCond->Vals.at(iValueIndex).itemArrElmnts);
								break;
							case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
								dump_member_list(pCond->Vals.at(iValueIndex).memberList);
								break;
							case	DDL_ATTR_DATA_TYPE_EXPRESSION:
								cout<<endl;
								dump_expression(pCond->Vals.at(iValueIndex).pExpr);
								break;
							case	DDL_ATTR_DATA_TYPE_MIN_MAX:
								dump_min_max_list(pCond->Vals.at(iValueIndex).minMaxList);
								break;
							case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
								dump_line_type(pCond->Vals.at(iValueIndex).lineType);
								break;
							case	DDL_ATTR_DATA_TYPE_GRID_SET:
								dump_grid_list(pCond->Vals.at(iValueIndex).gridMemList);
								break;
							default:
								cout<<"* Unknown Data Type 3*";
						/*Surprise!!!*/
							break;
						}/*End switch*/
						
					}/*End if */
					else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL)//Vibhor 200105: Changed
					{
						dump_conditional_attribute(pCond->listOfChilds.at(iChildIndex),(indent+7));
						iChildIndex++;
					
					}/*End else*/
					else
					{ //this section of conditional has chunks
						dump_section_chunks(pCond->listOfChunks.at(iChunkIndex));
						iChunkIndex++;
						clog<<"Item  0x" <<hex<<item_id <<dec<<"  has section chunks" <<endl;
					}
					cout<<endl;
					dump_blank(indent+7);
					cout<<"}/*End ELSE*/\n";

				}/*End ifpCond->byNumberOfSections == 2*/

			}
			break;
		case DDL_COND_TYPE_SELECT:
			{
				dump_blank(indent);
				cout<<"SELECT\n";
				dump_expression(&pCond->expr);
				dump_blank(indent+5);
				cout<<"{\n";
				while( iSectionIndex < pCond->byNumberOfSections)
				{
					dump_blank(indent+10);
					if(iSectionIndex == pCond->caseVals.size())
					{
						cout<<"DEFAULT :\n";
					}
					else
					{
						cout<<"CASE";
						dump_expression(&pCond->caseVals.at(iSectionIndex));
					}
					dump_blank(indent+10);
					cout<<"{\n";
					if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_DIRECT)//Vibhor 200105: Changed
					{
						dump_blank(indent+20);
						switch(pCond->attrDataType)
						{
							case 	DDL_ATTR_DATA_TYPE_INT:
// PAW start 03/03/09
								char buf[32], buf1[32];
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).llVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).llVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).llVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).llVal<<dec<<")\n";
// PAW end 03/03/09

								break;
							case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
// PAW start 03/03/09
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n";
// PAW end 03/03/09

								break;
							case	DDL_ATTR_DATA_TYPE_FLOAT:
							case	DDL_ATTR_DATA_TYPE_DOUBLE:
								cout<<" "<<pCond->Vals.at(iValueIndex).fVal<<endl;
								break;
							case	DDL_ATTR_DATA_TYPE_BITSTRING :
// PAW start 03/03/09
								sprintf(buf,"%I64d", pCond->Vals.at(iValueIndex).ullVal );
								sprintf(buf1,"%I64dx", pCond->Vals.at(iValueIndex).ullVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pCond->Vals.at(iValueIndex).ullVal<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).ullVal<<dec<<")\n";
// PAW end 03/03/09

								break;
							case	DDL_ATTR_DATA_TYPE_STRING:
								dump_string(pCond->Vals.at(iValueIndex).strVal);
								cout<<endl; /*Go to the next line for the next attribute!!*/
								break;
							case	DDL_ATTR_DATA_TYPE_ITEM_ID:
								cout<<"	"<<pCond->Vals.at(iValueIndex).id<<"  (0x"<<hex<<pCond->Vals.at(iValueIndex).id<<dec<<")\n";
								break;
							case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
								dump_enumlist(pCond->Vals.at(iValueIndex).enmList);
								break;

							case	DDL_ATTR_DATA_TYPE_REFERENCE :
								dump_reference(*(pCond->Vals.at(iValueIndex).ref));
								cout<<endl;
								break;
							case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
								dump_reference_list(pCond->Vals.at(iValueIndex).refList);
								break;
							case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
								dump_response_code_list(pCond->Vals.at(iValueIndex).respCdList);
								break;
							case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
								dump_menu_item_list(pCond->Vals.at(iValueIndex).menuItemsList);
								break;
							case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
								dump_refresh_relation(pCond->Vals.at(iValueIndex).refrshReln);
								break;
							case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
								dump_unit_relation(pCond->Vals.at(iValueIndex).unitReln);
								break;
							case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
								dump_item_array_element_list(pCond->Vals.at(iValueIndex).itemArrElmnts);
								break;
							case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
								dump_member_list(pCond->Vals.at(iValueIndex).memberList);
								break;
							case	DDL_ATTR_DATA_TYPE_EXPRESSION:
								cout<<endl;
								dump_expression(pCond->Vals.at(iValueIndex).pExpr);
								break;
							case	DDL_ATTR_DATA_TYPE_MIN_MAX:
								dump_min_max_list(pCond->Vals.at(iValueIndex).minMaxList);
								break;
							case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
								dump_line_type(pCond->Vals.at(iValueIndex).lineType);
								break;
							case	DDL_ATTR_DATA_TYPE_GRID_SET:
								dump_grid_list(pCond->Vals.at(iValueIndex).gridMemList);
								break;
							default:
								cout<<"* Unknown Data Type 2*";
						/*Surprise!!!*/
							break;
						}/*End switch*/
						
					
					}/*Endif*/
					else if(pCond->isSectionConditionalList.at(iSectionIndex) == DDL_SECT_TYPE_CONDNL)//Vibhor 200105: Changed
					{
						dump_conditional_attribute(pCond->listOfChilds.at(iChildIndex),(indent+7));
						iChildIndex++;
					
					}/*End else*/
					else
					{ //this section of conditional has chunks
						dump_section_chunks(pCond->listOfChunks.at(iChunkIndex));
						iChunkIndex++;
						clog<<"Item  0x" <<hex<<item_id <<dec<<"  has section chunks" <<endl;
					}

					cout<<endl;
					dump_blank(indent+10);
					cout<<"}/*End CASE*/\n";

				iSectionIndex++;
				iValueIndex++;

				}/*End while iSectionIndex*/

				dump_blank(indent+5);
				cout<<"}/*End SELECT*/\n";

			}
			break;		
		default:
			cout<<"* Unknown Conditional Type *";
			break;

	}/*End switch*/

}/*End dump_conditional_attribute*/


void dump_conditional_chunks(DDlAttribute *pAttr)
{
	int iDirectValIndex = 0;
	int iCondValIndex = 0;
	//Added By Deepak
	if(NULL ==pAttr)
	return;

	for(int i = 0; i < pAttr->byNumOfChunks; i++)
	{
		if(pAttr->isChunkConditionalList.at(i) == DDL_SECT_TYPE_CONDNL) /*If conditional chunk*///Vibhor 200105: Changed
		{
			dump_conditional_attribute(pAttr->conditionalVals.at(iCondValIndex),5);
			iCondValIndex++;
		}/*Endif Conditional chunk */
		else
		{/*Direct chunk*/
			switch(pAttr->attrDataType)
			{
				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					dump_enumlist(pAttr->directVals.at(iDirectValIndex).enmList);
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					dump_reference_list(pAttr->directVals.at(iDirectValIndex).refList);
					break;
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					dump_transaction_list(pAttr->directVals.at(iDirectValIndex).transList);
					break;
				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					dump_response_code_list(pAttr->directVals.at(iDirectValIndex).respCdList);
					break;
				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					dump_menu_item_list(pAttr->directVals.at(iDirectValIndex).menuItemsList);
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					dump_item_array_element_list(pAttr->directVals.at(iDirectValIndex).itemArrElmnts);
					break;	
				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					dump_member_list(pAttr->directVals.at(iDirectValIndex).memberList);
					break;
				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					dump_min_max_list(pAttr->directVals.at(iDirectValIndex).minMaxList);
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
					dump_grid_list(pAttr->directVals.at(iDirectValIndex).gridMemList);
					break;
				default:
					cout<<"* Unknown Data Type 1*";
					break;
			
			}/*End switch*/	
			
			iDirectValIndex++;
		
		}/*End else*/

	}/*Endfor i*/
	
}/*End dump_conditional_chunks*/

/*Vibhor 190105: Start of Code*/

void dump_section_chunks(DDlSectionChunks *pSecChunk)
{
	int iDirectValIndex = 0;
	int iCondValIndex = 0;
	//Added By Deepak
	if(NULL == pSecChunk)
	return;
	
	//dump_blank(indent);

	for(int i = 0; i < pSecChunk->byNumOfChunks; i++)
	{
		if(pSecChunk->isChunkConditionalList.at(i) == DDL_SECT_TYPE_CONDNL) /*If conditional chunk*/
		{
			dump_conditional_attribute(pSecChunk->conditionalVals.at(iCondValIndex),5);
			iCondValIndex++;
		}/*Endif Conditional chunk */
		else
		{/*Direct chunk*/
			switch(pSecChunk->attrDataType)
			{
				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					dump_enumlist(pSecChunk->directVals.at(iDirectValIndex).enmList);
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					dump_reference_list(pSecChunk->directVals.at(iDirectValIndex).refList);
					break;
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					dump_transaction_list(pSecChunk->directVals.at(iDirectValIndex).transList);
					break;
				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					dump_response_code_list(pSecChunk->directVals.at(iDirectValIndex).respCdList);
					break;
				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					dump_menu_item_list(pSecChunk->directVals.at(iDirectValIndex).menuItemsList);
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					dump_item_array_element_list(pSecChunk->directVals.at(iDirectValIndex).itemArrElmnts);
					break;	
				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					dump_member_list(pSecChunk->directVals.at(iDirectValIndex).memberList);
					break;
				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					dump_min_max_list(pSecChunk->directVals.at(iDirectValIndex).minMaxList);
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
					dump_grid_list(pSecChunk->directVals.at(iDirectValIndex).gridMemList);
					break;
				default:
					cout<<"* Unknown Data Type 6*";
					break;
			
			}/*End switch*/	
			
			iDirectValIndex++;
		
		}/*End else*/

	}/*Endfor i*/
}

/*Vibhor 190105: End of Code*/



bool AttrSortPredicate(const DDlAttribute* /*&*/ lhs,  const DDlAttribute* /*&*/ rhs)// remove references PAW 03/03/09
{
  return lhs->byAttrID < rhs->byAttrID;
}

void dump_attributes(ItemAttrList& alist)
{	
	ItemAttrList :: iterator p;
	DDlAttribute *pAttr = NULL;

	// all items come out in sorted order - timj 20dec07
	std::sort(alist.begin(), alist.end(), AttrSortPredicate);

	for(p = alist.begin(); p!= alist.end(); p++)
	{
		pAttr = (*p);
		cout<<pAttr->attrName.c_str()<<" :";
		if(pAttr->bIsAttributeConditional == false &&
			pAttr->bIsAttributeConditionalList == false) /*We have a direct attribute */
		{
			/*switch to the appropriate data type & simply dump it!!*/
			switch(pAttr->attrDataType)
			{

				case 	DDL_ATTR_DATA_TYPE_INT:
// PAW start 03/03/09
								char buf[32], buf1[32];
								sprintf(buf,"%I64d", pAttr->pVals->llVal );
								sprintf(buf1,"%I64dx", pAttr->pVals->llVal );
								cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//								cout<<"	"<<pAttr->pVals->llVal<<"  (0x"<<hex<<pAttr->pVals->llVal<<dec<<")\n";
// PAW end 03/03/09

					break;
				case 	DDL_ATTR_DATA_TYPE_UNSIGNED_LONG:
				case	DDL_ATTR_DATA_TYPE_WAVEFORM_TYPE:
				case	DDL_ATTR_DATA_TYPE_CHART_TYPE:
				case	DDL_ATTR_DATA_TYPE_MENU_STYLE:
				case	DDL_ATTR_DATA_TYPE_SCOPE_SIZE:
// PAW start 03/03/09
				sprintf(buf,"%I64d", pAttr->pVals->ullVal );
				sprintf(buf1,"%I64dx", pAttr->pVals->ullVal );
				cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW see below
//				cout<<"	"<<pAttr->pVals->ullVal<<"  (0x"<<hex<<pAttr->pVals->ullVal<<dec<<")\n";
// PAW end 03/03/09

					break;
				case	DDL_ATTR_DATA_TYPE_FLOAT:
				case	DDL_ATTR_DATA_TYPE_DOUBLE:
					cout<<"	"<<pAttr->pVals->fVal<<endl;
					break;
				case	DDL_ATTR_DATA_TYPE_BITSTRING :
// PAW start 03/03/09
				sprintf(buf,"%I64d", pAttr->pVals->ullVal );
				sprintf(buf1,"%I64dx", pAttr->pVals->ullVal );
				cout<<"	"<< buf <<"  (0x"<<buf1<<")\n"; // PAW 03/03/09 see below
//				cout<<"	0x"<<hex<<pAttr->pVals->ullVal<<dec<<endl;
// PAW end 03/03/09

					break;
				case	DDL_ATTR_DATA_TYPE_STRING:
					dump_string(pAttr->pVals->strVal);
					cout<<endl; /*Go to the next line for the next attribute!!*/
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ID:
					cout<<"	"<<pAttr->pVals->id<<"  (0x"<<hex<<pAttr->pVals->id<<dec<<")\n";
					break;
				case	DDL_ATTR_DATA_TYPE_ENUM_LIST:
					dump_enumlist(pAttr->pVals->enmList);
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE :
					dump_reference(*(pAttr->pVals->ref));
					cout<<endl;
					break;
				case	DDL_ATTR_DATA_TYPE_REFERENCE_LIST:
					dump_reference_list(pAttr->pVals->refList);
					break;
				case	DDL_ATTR_DATA_TYPE_TYPE_SIZE:
					dump_type_size(pAttr->pVals->typeSize);
					break;
				case	DDL_ATTR_DATA_TYPE_TRANSACTION_LIST:
					dump_transaction_list(pAttr->pVals->transList);
					break;
				case	DDL_ATTR_DATA_TYPE_RESPONSE_CODE_LIST:
					dump_response_code_list(pAttr->pVals->respCdList);
					break;
				case	DDL_ATTR_DATA_TYPE_MENU_ITEM_LIST:
					dump_menu_item_list(pAttr->pVals->menuItemsList);
					break;
				case	DDL_ATTR_DATA_TYPE_DEFINITION:
					cout<<endl;
					for(unsigned i=0;i < pAttr->pVals->defData.size;i++)
					{
						cout<<pAttr->pVals->defData.data[i];
					}
					cout<<endl;
					break;
				case	DDL_ATTR_DATA_TYPE_REFRESH_RELATION:
					dump_refresh_relation(pAttr->pVals->refrshReln);
					break;
				case	DDL_ATTR_DATA_TYPE_UNIT_RELATION:
					dump_unit_relation(pAttr->pVals->unitReln);
					break;
				case	DDL_ATTR_DATA_TYPE_ITEM_ARRAY_ELEMENT_LIST:
					dump_item_array_element_list(pAttr->pVals->itemArrElmnts);
					break;
				case	DDL_ATTR_DATA_TYPE_MEMBER_LIST:
					dump_member_list(pAttr->pVals->memberList);
					break;
				case	DDL_ATTR_DATA_TYPE_EXPRESSION:
					cout<<endl;
					dump_expression(pAttr->pVals->pExpr);
					break;
				case	DDL_ATTR_DATA_TYPE_MIN_MAX:
					dump_min_max_list(pAttr->pVals->minMaxList);
					break;
				case	DDL_ATTR_DATA_TYPE_LINE_TYPE:
					dump_line_type(pAttr->pVals->lineType);
					break;
				case	DDL_ATTR_DATA_TYPE_GRID_SET:
					dump_grid_list(pAttr->pVals->gridMemList);
					break;
					
				case	DDL_ATTR_DATA_TYPE_DEBUG_DATA:
					dump_debug_info(pAttr->pVals->debugInfo);
					break;
				case	DDL_ATTR_DATA_TYPE_PARAM:
					dump_param(pAttr->pVals->methodType);
					break;
				case	DDL_ATTR_DATA_TYPE_PARAM_LIST:
					dump_param_list(pAttr->pVals->paramList);
					break;
				default:
					/*Surprise!!!*/
					cout<<"* Unknown Data Type 7*" << ((int)pAttr->attrDataType) <<" *";
					break;
			}/*End switch*/


		}/*Endif */
		else /*dump the conditional attribute*/
		{
			cout<<endl;
			if(pAttr->bIsAttributeConditional == true && pAttr->bIsAttributeConditionalList == false
				)
			{
				dump_conditional_attribute(pAttr->pCond,5);
			}
			else
			{
				dump_conditional_chunks(pAttr);
			}


		}/*End else*/

	
	}/*End for p*/

}/*End dump_attributes */





// all items come out in sorted order - timj 20dec07
bool ItemSortPredicate(const DDlBaseItem* /*&*/ lhs,  const DDlBaseItem* /*&*/ rhs)// PAW 03/03/09 removed references
{
  return lhs->id < rhs->id;
}


void dump_items(DDlDevDescription* pDevDesc)
{
	vector <DDlBaseItem *>:: iterator p;
	DDlBaseItem *pBaseItem=NULL;
	int iVarCnt,iCmdCnt,iMenuCnt,iEdDispCnt,iMethCnt,iRfrshCnt,
	 iUnitCnt,iWaoCnt,iIArrCnt,iCollnCnt,iBlkCnt,iRecCnt,iImgCnt,iGrdCnt,
	 iArrayCnt,iFileCnt,iChrtCnt,iGrphCnt,iAxsCnt,iWavCnt,iSrcCnt,iLstCnt,iErrCnt;

	iVarCnt = iCmdCnt = iMenuCnt = iEdDispCnt = iMethCnt = iRfrshCnt = 
	 iUnitCnt = iWaoCnt = iIArrCnt = iCollnCnt = iBlkCnt = iRecCnt = 
	 iArrayCnt = iFileCnt = iChrtCnt = iGrphCnt = iAxsCnt = iWavCnt = 
	 iSrcCnt = iLstCnt = iImgCnt = iGrdCnt = iErrCnt = 0;
	
	//Added by deepak
	if(NULL == pDevDesc)
		return;

	// all items come out in sorted order - timj 20dec07
	std::sort(pDevDesc->ItemsList.begin(), pDevDesc->ItemsList.end(), ItemSortPredicate); // was error here PAW 03/03/09

	for(p = pDevDesc->ItemsList.begin(); p != pDevDesc->ItemsList.end();p++)
	{
		pBaseItem = *p;
		item_id = pBaseItem->id;
		switch(pBaseItem->byItemType)
		{

			case	VARIABLE_ITYPE:
				cout<<"\n\n*********************************Variable Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName << endl;	//timj
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlVariable*)pBaseItem)->attrList);
				iVarCnt++;
				break;
			case	COMMAND_ITYPE:
				cout<<"\n\n*********************************Command Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName << endl; //timj
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlCommand*)pBaseItem)->attrList);
				iCmdCnt++;
				break;
			case	MENU_ITYPE: 
				cout<<"\n\n*********************************Menu Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName << endl;  //timj
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlMenu*)pBaseItem)->attrList);
				iMenuCnt++;
				break;
			case	EDIT_DISP_ITYPE:  
				cout<<"\n\n*********************************Edit Display Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlEditDisplay*)pBaseItem)->attrList);
				iEdDispCnt++;
				break;
			case	METHOD_ITYPE:     
				cout<<"\n\n*********************************Method Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlMethod*)pBaseItem)->attrList);
				iMethCnt++;
				break;
			case	REFRESH_ITYPE:    
				cout<<"\n\n*********************************Refresh Relation Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlRefresh*)pBaseItem)->attrList);
				iRfrshCnt++;
				break;
			case	UNIT_ITYPE:       
				cout<<"\n\n*********************************Unit Relation Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlUnit*)pBaseItem)->attrList);
				iUnitCnt++;
				break;
			case	WAO_ITYPE: 	
				cout<<"\n\n*********************************WAO Relation Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlWao*)pBaseItem)->attrList);
				iWaoCnt++;
				break;
			case	ITEM_ARRAY_ITYPE: 
				cout<<"\n\n*********************************Item Array Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				cout<<"Item Sub Type:	"<<(int)pBaseItem->byItemSubType<<endl;
				dump_attributes(((DDlItemArray*)pBaseItem)->attrList);
				iIArrCnt++;
				break;
			case	COLLECTION_ITYPE:
				cout<<"\n\n*********************************Collection Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				cout<<"Item Sub Type:	"<<(int)pBaseItem->byItemSubType<<endl;
				dump_attributes(((DDlCollection*)pBaseItem)->attrList);
				iCollnCnt++;
				break;
			case	BLOCK_ITYPE:      
				cout<<"\n\n----------------------------------Block Item---------------------------------\n";
				cout<<"-Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"-Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"-Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDlBlock*)pBaseItem)->attrList);
				iBlkCnt++;
				break;
			case	RECORD_ITYPE: 
				cout<<"\n\n---------------------------------Record Item---------------------------------\n";
				cout<<"-Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"-Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"-Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				cout<<"-Item Sub Type:	"<<(int)pBaseItem->byItemSubType<<endl;
				dump_attributes(((DDlRecord*)pBaseItem)->attrList);
				iRecCnt++;
				break;
				
			case	ARRAY_ITYPE     :
				cout<<"\n\n**********************************Array Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Array*)pBaseItem)->attrList);
				iArrayCnt++;
				break;
			case	VAR_LIST_ITYPE  :
				cout<<"\n\n--------------------------------List Item------------------------------------\n";
				break;
			case	RESP_CODES_ITYPE:
				cout<<"\n\n------------------------------Resp Code Item---------------------------------\n";
				break;
			case	DOMAIN_ITYPE    :
				cout<<"\n\n---------------------------------Domain Item---------------------------------\n";
				break;
			case	MEMBER_ITYPE    :
				cout<<"\n\n---------------------------------Member Item---------------------------------\n";
				break;
			case	FILE_ITYPE		:
				cout<<"\n\n***********************************File Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6File*)pBaseItem)->attrList);
				iFileCnt++;
				break;
			case	CHART_ITYPE		:
				cout<<"\n\n**********************************Chart Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Chart*)pBaseItem)->attrList);
				iChrtCnt++;
				break;
			case	GRAPH_ITYPE		:
				cout<<"\n\n**********************************Graph Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Graph*)pBaseItem)->attrList);
				iGrphCnt++;
				break;
			case	AXIS_ITYPE		:
				cout<<"\n\n***********************************Axis Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Axis*)pBaseItem)->attrList);
				iAxsCnt++;
				break;
			case	WAVEFORM_ITYPE	:
				cout<<"\n\n*******************************Waveform Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Waveform*)pBaseItem)->attrList);
				iWavCnt++;
				break;
			case	SOURCE_ITYPE	:
				cout<<"\n\n*********************************Source Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Source*)pBaseItem)->attrList);
				iSrcCnt++;
				break;
			case	LIST_ITYPE		:
				cout<<"\n\n***********************************List Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6List*)pBaseItem)->attrList);
				iLstCnt++;
				break;
			case	IMAGE_ITYPE		:
				cout<<"\n\n***********************************Image Item*********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6List*)pBaseItem)->attrList);
				iImgCnt++;
				break;
			case	GRID_ITYPE		:
				cout<<"\n\n***********************************Grid Item**********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				dump_attributes(((DDl6Grid*)pBaseItem)->attrList);
				iGrdCnt++;
				break;
			default:
				cout<<"\n\n********************************** Error Item**********************************\n";
				cout<<"Item ID:   "<<pBaseItem->id<<"  (0x"<<hex<<pBaseItem->id<<dec<<")\t\t";
				cout<<"Item Name:  "<<pBaseItem->strItemName.c_str() << endl;
				cout<<"Item Masks:   "<<pBaseItem->ulItemMasks<<"  (0x"<<hex<<pBaseItem->ulItemMasks<<dec<<")\n";
				iErrCnt++;
				break;
			
		}/*End switch*/

	}/*End for p*/

	cout <<"***************SUMMARY*******************" <<endl;
	cout <<endl;
	cout <<"Variable   Count   :" <<iVarCnt<<endl;
	cout <<"Command    Count   :" <<iCmdCnt<<endl;
	cout <<"Menu       Count   :" <<iMenuCnt<<endl;
	cout <<"EditDisp   Count   :" <<iEdDispCnt<<endl;
	cout <<"Method     Count   :" <<iMethCnt<<endl;
	cout <<"Refresh    Count   :" <<iRfrshCnt<<endl;
	cout <<"Unit       Count   :" <<iUnitCnt<<endl;
	cout <<"WAO        Count   :" <<iWaoCnt<<endl;
	cout <<"ItemArray  Count   :" <<iIArrCnt<<endl;
	cout <<"Collection Count   :" <<iCollnCnt<<endl;
	cout <<"Block      Count   :" <<iBlkCnt<<endl;
	cout <<"Record     Count   :" <<iRecCnt<<endl;
	cout <<"Array	   Count   :" <<iArrayCnt<<endl;
	cout <<"File 	   Count   :" <<iFileCnt<<endl;
	cout <<"Chart	   Count   :" <<iChrtCnt<<endl;
	cout <<"Graph	   Count   :" <<iGrphCnt<<endl;
	cout <<"Axis	   Count   :" <<iAxsCnt<<endl;
	cout <<"Waveform   Count   :" <<iWavCnt<<endl;
	cout <<"Source	   Count   :" <<iSrcCnt<<endl;
	cout <<"List	   Count   :" <<iLstCnt<<endl;
	cout <<"Image      Count   :" <<iImgCnt<<endl;
	cout <<"Grid	   Count   :" <<iGrdCnt<<endl;
	cout <<"\nUnknown Item Count :" <<iErrCnt<<endl;


}/*End dump_items*/




void dump_devdir_tables(FLAT_DEVICE_DIR_6& fDD)
{
	unsigned i, j;
	int k;
	cout<<"\n\n========================== Device Directory ================================"<<endl;
	cout<<"    Availability Mask: 0x" << hex << fDD.attr_avail << dec<< endl;
	cout<<"    Item Table has "<< fDD.item_tbl.count <<" items"<<endl;
	for ( i = 0; i < fDD.item_tbl.count; i++)
	{
		fDD.item_tbl.list[i].dd_ref.object_index;
		fDD.item_tbl.list[i].item_id;
		k = fDD.item_tbl.list[i].item_type;
		if ( k > iT_MaxType )
		{
			k = k - 100 + 2;  // 128 -> 28 -> 30,31,32
			if ( k > 32 )
			{// 200->100->102...
				k -= 69;//        ->33
			}
			if ( k > 35 )
			{
				k -= 53;
			}
			if ( k > 36 ) k = 0;
		}
		cout<<"        Item "<< i <<" with ID:"<<fDD.item_tbl.list[i].item_id <<":(0x";
		cout<<hex<< fDD.item_tbl.list[i].item_id <<dec <<") and index ";
		cout<<fDD.item_tbl.list[i].dd_ref.object_index<<" is a"<< itemStrings[k] <<endl;          
	}// next item in table

	pGlobalDict->dumpdict(fm6);
	pLitStringTable->dump();

	cout<<"\n    Command Number Table has "<< fDD.cmd_num_id_tbl.count <<" items"<<endl;
	for ( i = 0; i < fDD.cmd_num_id_tbl.count; i++)
	{
		fDD.cmd_num_id_tbl.list[i].number;
		fDD.cmd_num_id_tbl.list[i].item_id;
		
		cout<<"        "<< i <<") Command " <<fDD.cmd_num_id_tbl.list[i].item_id<<":(0x";
		cout<<hex<< fDD.cmd_num_id_tbl.list[i].item_id <<dec <<") is command number: ";
		cout<<fDD.cmd_num_id_tbl.list[i].number<<endl;          
	}// next item in table

	cout<<"\n    Image Table has "<< fDD.image_tbl.count <<" images"<<endl;
	char langStr[8]={0,0,0,0,0,0,0,0};
	for ( i = 0; i < fDD.image_tbl.count; i++)
	{
		if ( fDD.image_tbl.list[i].num_langs > 1 )
		{
			cout<<"        Image "<<i<<" has "<<fDD.image_tbl.list[i].num_langs<<" languages."<<endl;
			for(j = 0; j < fDD.image_tbl.list[i].num_langs; j++ )
			{
				memcpy(langStr,&(fDD.image_tbl.list[i].img_list[j].lang_code),6);
				cout<<"              Lang "<<j+1<<"] "<< langStr << "  Offset:";
				cout<<				fDD.image_tbl.list[i].img_list[j].img_file.offset;
				cout<<"   Size:"<<	fDD.image_tbl.list[i].img_list[j].img_file.uSize<<endl;
				memset(langStr,0,8);
/* for debugging    char* tmp = (char*) fDD.image_tbl.list[i].img_list[j].p2Graphik;
				this is a pointer to the actual graphic stored on the computer */
			}// next lang			
		}
		else
		{// only one language
			memcpy(langStr,&(fDD.image_tbl.list[i].img_list[0].lang_code),6);
			cout<<"              Lang 1] "<< langStr << "  Offset:";
			cout<<				fDD.image_tbl.list[i].img_list[0].img_file.offset;
			cout<<"   Size:"<<	fDD.image_tbl.list[i].img_list[0].img_file.uSize<<endl;
			memset(langStr,0,8);			
		}
	}// next item in table
}

void dump_blkdir_tables(FLAT_BLOCK_DIR_6&  fBD)//stevev 22feb08
{	
	int i, k;
	cout<<"\n\n========================== Block Directory ================================"<<endl;
	cout<<"    Availability Mask: 0x" << hex << fBD.attr_avail << dec<< endl;
  
	cout<<"\n    Block-Item Table has "<< fBD.blk_item_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.blk_item_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tBlock-Item " <<fBD.blk_item_tbl.list[i].blk_item_id<<":(0x";
		cout<<hex<< fBD.blk_item_tbl.list[i].blk_item_id <<dec <<") maps 1:1 to Block-Item-Name-Table: ";
		cout<<fBD.blk_item_tbl.list[i].blk_item_name_tbl_offset<<endl;          
	}// next item in table


	cout<<"\n    Block-Item-Name Table has "<< fBD.blk_item_name_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.blk_item_name_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tBlock-Item-Name " <<fBD.blk_item_name_tbl.list[i].blk_item_name<<":(0x";
		cout<<hex<< fBD.blk_item_name_tbl.list[i].blk_item_name <<dec <<")"<<endl;
		
		cout<<"           \t                 maps 1:1 to Device-Item-Table: ";
		cout<<fBD.blk_item_name_tbl.list[i].item_tbl_offset<<endl;        
		
		cout<<"           \t                 maps 1:1 to Block-Param-Table: ";
		cout<<fBD.blk_item_name_tbl.list[i].param_tbl_offset<<endl;            
		
		cout<<"           \t                 maps 1:1 to    Relation-Table: ";
		cout<<fBD.blk_item_name_tbl.list[i].rel_tbl_offset<<endl;      
		
		cout<<"           \t                 maps to           R-Cmd-Table: ";
		cout<<fBD.blk_item_name_tbl.list[i].read_cmd_tbl_offset;
		cout<<" for "<< fBD.blk_item_name_tbl.list[i].read_cmd_count<<" Commands."<<endl;              
		
		cout<<"           \t                 maps to           W-Cmd-Table: ";
		cout<<fBD.blk_item_name_tbl.list[i].write_cmd_tbl_offset;
		cout<<" for "<< fBD.blk_item_name_tbl.list[i].write_cmd_count<<" Commands."<<endl;     
	}// next item in table

	cout<<"\n    Parameter Table has "<< fBD.param_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.param_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tParameter entry:"<<endl;
		
		cout<<"           \t                 maps 1:1 to  Block-Item-Name Table: ";
		cout<<fBD.param_tbl.list[i].blk_item_name_tbl_offset<<endl;       
		
		cout<<"           \t                 maps to      Array-Elem-Item Table: ";
		cout<<fBD.param_tbl.list[i].array_elem_item_tbl_offset;
		cout<<" for "<< fBD.param_tbl.list[i].array_elem_count<<" Elements."<<endl;   
		

		k = fBD.param_tbl.list[i].array_elem_type_or_var_type;
		cout<<"           \t                 with elem/var Type: "<< k <<" ("<<varTypeStrings[k]<<") ";
		cout<<"Size: "<< fBD.param_tbl.list[i].array_elem_size_or_var_size;
		cout<<" and Class 0x"<<hex<< fBD.param_tbl.list[i].array_elem_class_or_var_class <<dec<< " \t{";
		unsigned int y = fBD.param_tbl.list[i].array_elem_class_or_var_class;
		int h = 0, notfirst = 0;
		while (y)
		{
			if ( (1<<h) & y )
			{// bit is set
				y &= ~(1<<h) ;
				if ( notfirst )
				cout<<"|"<< TStr2AStr(wstring(varClassStrings[h+1]));
				else
				cout<<TStr2AStr(wstring(varClassStrings[h+1]));
				notfirst = 1;
			}
			h++;
		}
		cout<<"}"<<endl; 
	}// next item in table
	

	cout<<"\n    Relation Table has "<< fBD.rel_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.rel_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tRelation entry:"<<endl;
		
		cout<<"           \t          WAO_item_table_offset: ";
		cout<<fBD.rel_tbl.list[i].wao_item_tbl_offset<<endl;       
		
		cout<<"           \t         UNIT_item_table_offset: ";
		cout<<fBD.rel_tbl.list[i].unit_item_tbl_offset;
		cout<<" for "<< fBD.rel_tbl.list[i].unit_count <<" Units."<<endl; 
		
		cout<<"           \t            Update_table_offset: ";
		cout<<fBD.rel_tbl.list[i].update_tbl_offset;
		cout<<" for "<< fBD.rel_tbl.list[i].update_count <<" Updates."<<endl;  
	}// next item in table

	cout<<"\n    Update Table has "<< fBD.update_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.update_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tUpdate entry:"<<endl;  
		
		cout<<"           \t              Item_table_offset: ";
		cout<<fBD.update_tbl.list[i].desc_it_offset<<endl;     
		
		cout<<"           \t  Operational_item_table_offset: ";
		cout<<fBD.update_tbl.list[i].op_it_offset;
		cout<<"   with "<< fBD.update_tbl.list[i].op_subindex <<" Subindex."<<endl; 
	}// next item in table
	/*
	COMMAND_TBL					command_tbl;
					int					count;
					COMMAND_TBL_ELEM	*list;
									ulong				number;
									ulong				transaction;
									SUBINDEX			subindex;
									ushort				weight;
									int					count;
									COMMAND_INDEX		*index_list;
*/
	if (fBD.command_tbl.count && ! fBD.command_to_var_tbl.count)
	{
		cout<<"\n    Parameter to Command Table has "<< fBD.command_tbl.count <<" items"<<endl;
		for ( i = 0; i < fBD.command_tbl.count; i++)
		{		
			cout<<"        "<< i <<")\tPTOC entry: " <<endl;  
			
			cout<<"           \t  Subindex:"<<fBD.command_tbl.list[i].subindex <<endl;
			cout<<"           \t    Number: ";
			cout<<fBD.command_tbl.list[i].number;
			cout<<"  Transaction: "<< fBD.command_tbl.list[i].transaction ;
			cout<<"  w/wgt "<<fBD.command_tbl.list[i].weight<<endl; 

			if (fBD.command_tbl.list[i].count)
			{
				for ( int t = 0; t < fBD.command_tbl.list[i].count; t++)
				{
					cout<<"           \t             ID: ";
					cout<< fBD.command_tbl.list[i].index_list[t].id  <<"   Value: ";
					cout<< fBD.command_tbl.list[i].index_list[t].value<<endl;
				}
			}
		}// next item in table
	}
	else
	if(fBD.command_to_var_tbl.count /*&& ! fBD.command_tbl.count*/)
	{
		cout<<"\n    Parameter to Command Table has "<< fBD.command_to_var_tbl.count <<" items"<<endl;
		for ( i = 0; i < fBD.command_to_var_tbl.count; i++)
		{		
			cout<<"        "<< i <<")\tPTOC entry:  0x"<<hex<<fBD.command_to_var_tbl.list[i].item_id<<dec<<endl;  
			
			cout<<"           \t"<<fBD.command_to_var_tbl.list[i].rd_count<<"          Read Commands: "<<endl;
			for( int R = 0; R < fBD.command_to_var_tbl.list[i].rd_count; R++)
			{
				cout<<"           \t\t  Subindex: "<<fBD.command_to_var_tbl.list[i].rd_list[R].subindex <<endl;
				cout<<"           \t\t    Number: ";
				cout<<fBD.command_to_var_tbl.list[i].rd_list[R].number;
				cout<<"  Transaction: "<< fBD.command_to_var_tbl.list[i].rd_list[R].transaction ;
				cout<<"  w/wgt "<<fBD.command_to_var_tbl.list[i].rd_list[R].weight<<endl; 

				if (fBD.command_to_var_tbl.list[i].rd_list[R].count)
				{
					for ( int t = 0; t < fBD.command_to_var_tbl.list[i].rd_list[R].count; t++)
					{
						cout<<"           \t             use index ID: 0x"<<hex;
						cout<< fBD.command_to_var_tbl.list[i].rd_list[R].index_list[t].id  <<dec<<"   Value: ";
						cout<< fBD.command_to_var_tbl.list[i].rd_list[R].index_list[t].value<<endl;
					}
				}
			}

			
			cout<<"           \t"<<fBD.command_to_var_tbl.list[i].wr_count<<"         Write Commands: "<<endl;
			for( int W = 0; W < fBD.command_to_var_tbl.list[i].wr_count; W++)
			{
				cout<<"           \t  Subindex: "<<fBD.command_to_var_tbl.list[i].wr_list[W].subindex <<endl;
				cout<<"           \t    Number: ";
				cout<<fBD.command_to_var_tbl.list[i].wr_list[W].number;
				cout<<"  Transaction: "<< fBD.command_to_var_tbl.list[i].wr_list[W].transaction ;
				cout<<"  w/wgt "<<fBD.command_to_var_tbl.list[i].wr_list[W].weight<<endl; 

				if (fBD.command_to_var_tbl.list[i].wr_list[W].count)
				{
					for ( int t = 0; t < fBD.command_to_var_tbl.list[i].wr_list[W].count; t++)
					{
						cout<<"           \t             use index ID: 0x"<<hex;
						cout<< fBD.command_to_var_tbl.list[i].wr_list[W].index_list[t].id  <<dec<<"   Value: ";
						cout<< fBD.command_to_var_tbl.list[i].wr_list[W].index_list[t].value<<endl;
					}
				}
			}
		}// next item in table
	}
	/*else
	if(fBD.command_to_var_tbl.count || fBD.command_tbl.count)
	{
		cout<<"\n    Parameter to Command Table has data for both an fm6 and an fm8"<<endl;
	}*/
	else
	{
		cout<<"\n    Parameter to Command Table has no entries for either file type."<<endl;
	}
	/*

    CRIT_PARAM_TBL              crit_param_tbl;
					int	     count;
					ITEM_ID	 *list;
									typedef unsigned long	ITEM_ID;
*/
	cout<<"\n    Critical Item Table has "<< fBD.crit_param_tbl.count <<" items"<<endl;
	for ( i = 0; i < fBD.crit_param_tbl.count; i++)
	{		
		cout<<"        "<< i <<")\tCritical Item: 0x";		
		cout<<hex<<fBD.crit_param_tbl.list[i]<<dec<<endl;  
	}// next item in table

}
