//#include <windows.h>
//#include <stdio.h>
//#include "std.h"
//#include <tchar.h>


#include <list>
#include < fstream >

#include "ddbGeneral.h"

#pragma warning (disable : 4786)

//#include "Dict.h"
#include "Retn_Code.h"
#include "PrintData.h"

#include "DDlDevDescription.h"
#include "ddbdefs.h"

extern CDictionary *pGlobalDict; /*The Global Dictionary object*/
extern LitStringTable *pLitStringTable; 

extern bool v_and_v;	// timj 17jan08  exe is "ParserVerify.exe"

extern FLAT_DEVICE_DIR_6 device_dir_6; 
extern FLAT_BLOCK_DIR_6  block_dir_6;

bool fm6;	// timj 10jan08 - true if the current binary file is BFF 6


static ofstream* plogRedir = NULL;
static ofstream* perrRedir = NULL;

static ofstream* poutRedir = NULL;
static streambuf* pHldOutBuf = NULL;

static streambuf* pHldErrBuf = NULL;
static streambuf* pHldLogBuf = NULL;


//#define _DDPARSERINFC_
//	#include "ddbParserInfc.h"
//#undef _DDPARSERINFC_
extern RETURNCODE buildDicitonary(CDictionary* dictionary);
extern RETURNCODE getStdTableKey(Indentity_t& newIdent,DWORD& wrkingKey);
int iNumberOfNoClassVars = 0;

//FILE * fout;
FILE * ferr;

#define myprintf fprintf

volatile unsigned int logPer = 0;

void translatePTOC(void);

#ifdef CLASSSTRMAXLEN
#undef CLASSSTRMAXLEN
#endif
#ifdef CLASSSTRCOUNT
#undef CLASSSTRCOUNT
#endif
#ifdef CLASSSTRINGS
#undef CLASSSTRINGS
#endif

#define CLASSSTRMAXLEN  16	/* maximum string length */
#define CLASSSTRCOUNT   22

#define CLASSSTRINGS \
	{_T("No-Class")},\
	{_T("Diagnostic")},\
	{_T("Dynamic")},\
	{_T("Service")},\
	{_T("Correction")},\
	{_T("Computation")},\
	{_T("Input-Block")},\
	{_T("Analog-Block")},\
	{_T("HART-class")},\
	{_T("Local-Display")},\
	{_T("Frequency")},\
	{_T("Discrete")},\
	{_T("Device")},\
	{_T("Local")},\
	{_T("Input")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("*UNDEFINED*")},\
	{_T("Factory")}


char     itemStrings[ITMSTRCOUNT]  [ITMAXLEN] = {ITEMTYPESTRINGS};
wchar_t varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN] = {CLASSSTRINGS};
char  varTypeStrings[VARTYPESTRCNT][VARTYPESTRMAXLEN] = {VARTYPESTRINGS};


/*Vibhor 05102003 : Moving dictionary loading to the DDlDevDescription class ******/
// WARNING: logout(1,"%s format string", "any char pointer");
//			will link to the va_list routine!!!! and give a memory fault!!!
int logoutV(int channel, char* format, va_list vL)// va_list is a char*  - for god's sake
{
	//static bCriticalSectionInit = FALSE;
	char strActual[LOG_BUF_SIZE];
	if ( format == NULL ) return 1;// failure

	va_list tL = vL;

	vsprintf(strActual, format, tL);

	if ((channel & CERR_LOG) )
	{	
		cerr << strActual << flush;
		
	}
	if ((channel & CLOG_LOG))
	{	
		clog << strActual << flush;
		
	}
	if ((channel & COUT_LOG))
	{	
		cout << strActual << flush;
		
	}
	va_end(vL);// this appears to clean the stack...be careful

	return 0;
}


int logout(int channel, char* format, ...)
{
	int r = 0;
	va_list vMarker;

	va_start(vMarker, format);
	r = logoutV(channel, format, vMarker);
	va_end(vMarker);

	return r;
}


#define MAX_FILE_NAME_PATH 255

char chInputFileName[MAX_FILE_NAME_PATH];
char chOutputFileName[MAX_FILE_NAME_PATH];
char chErrorFileName[MAX_FILE_NAME_PATH];

// also in critSect.cpp if that is included, pick one
unsigned long ddbGetTickCount()
{
	//return GetTickCount();
	return 0;
}

int main(int argc,char* argv[])
{
	DDlDevDescription  devDesc;
	bool bRetVal;
//	int iRet;


	fprintf(stdout,"Test01 to StdOut\n");
	printf("test02 to out\n");
	fflush(stdout);
	cout<<"test03 to out"<<endl;

	if(argc != 4)
	{
		printf("\n Usage : ParseDD <Inputfile (fmx) name> <Outputfile name> <Errorfile name>\n");
		exit(1);
	}

	strcpy(chInputFileName,argv[1]); /*If the input & output files are not in the same*/
	strcpy(chOutputFileName,argv[2]); /*location as the executable, then absolute path is required*/
	strcpy(chErrorFileName,argv[3]); /*Same for the Error File too*/	


	// SET TO TRUE TO COMPARE FM8 AND FM6
	v_and_v = true;


	poutRedir = new ofstream;
	poutRedir->open(chOutputFileName);
	if ( poutRedir->is_open() )
	{
		pHldOutBuf = cout.rdbuf();
		cout.rdbuf(poutRedir->rdbuf());
	}

	
//	fprintf(stdout,"Test04 to StdOut\n");
//	printf("test05 to out\n");
//	fflush(stdout);
//	cout<<"test06 to out"<<endl;

	/*Build the standard dictionary, If this doesn't succeed , there's no point in going ahead
	*/	
//	void (*dict_install_fptr)(unsigned long, char * , char*);
//	dict_install_fptr = dict_table_install;
//	printf("\n Start Time = %ul ",(unsigned long)dwStartTime);

	RETURNCODE rc = 0;
	DWORD tempKey = 0;			
	Indentity_t stdTblIdentity;
	CDictionary* dictionary = new CDictionary("|en|");
	LitStringTable *pLit    = new LitStringTable();

	// detect BFF version as last character of the file name
	char fmx = chInputFileName[strlen(chInputFileName)-1];
	fm6 = (fmx == '6' || fmx == 's' || fmx == 'S');

	if (fm6)
	{
		char *std = "c:\\hcf\\ddl\\library\\standard.dct";
		char *rest[] = {
					"c:\\hcf\\ddl\\library\\draeger.dct",
					"c:\\hcf\\ddl\\library\\mmi.dct",
					"c:\\hcf\\ddl\\library\\endress_hauser.dct",
					"c:\\hcf\\ddl\\library\\siemens.dct",
					NULL
					};

		int iRet = dictionary->makedict(std, rest);
		if(iRet != SUCCESS)
		{
			printf("\n Error Building the Standard Dictionary");
			exit(1);
		} 
	}


#ifdef PRELOAD
			rc = buildDicitonary(dictionary);
#endif // PRELOAD
			if(rc != SUCCESS)
			{
#ifdef PRELOAD
//				OUTPUT(CERR_LOG | UI_LOG,
//				"ERROR: Device manager could not load the dictionary '%s'",chDbdir);
				//cerr << "ERROR: +++ database did not start up +++"<< pDbdir << endl;
				// we can do nothing in this situation
//				delete pSelf;
//				pSelf = NULL;
#endif // PRELOAD
				// null will be returned
			}
			else/*Vibhor 041203: Start of Code*/
			{
				/*We will load standard tables as the first device*/
				stdTblIdentity.wManufacturer = 0x00;
				stdTblIdentity.wDeviceType = 0x01;
				stdTblIdentity.cDeviceRev = 0x00;

#ifdef PRELOAD
				LOGIT(STAT_LOG|CLOG_LOG,"Loading Standards");
				rc = getStdTableKey(stdTblIdentity,tempKey);
#endif // PRELOAD
				if(rc == SUCCESS)
				{
#ifdef PRELOAD
					cout<< "Loading Standard Device "<<endl;
					hCddbDevice *stdDev = stdDevice(stdTblIdentity,NULL,NULL,NULL,NULL,tempKey);
					if(NULL == stdDev)
					{
						OUTPUT(CERR_LOG | UI_LOG,
						"ERROR: Device manager could not load the Standard Tables");

						delete pSelf;
						pSelf = NULL;
					}
					cout<< "Standard Device Complete"<<endl;
#endif // PRELOAD
				}
				else
				{
#ifdef PRELOAD
					OUTPUT(CERR_LOG | UI_LOG,
						"ERROR: Device manager could not get the Standard Table Key");
					delete pSelf;
					pSelf = NULL;
#endif // PRELOAD
				}
			  	
			}



	bRetVal = devDesc.Initialize(chInputFileName,"|en|",dictionary, pLit);
 
	if(!bRetVal)
	{
		exit(1);
	}

//#ifdef _PARSER_DEBUG
//	fout = fopen(chOutputFileName,"w");
//	if(!fout)
//		exit(1);
//#endif
	ferr = fopen(chErrorFileName,"a");
	if(!ferr)
		exit(1);

#ifdef _PARSER_DEBUG
//	cout << sprintf(buf,"\n Start Time = %ul ",(unsigned long)dwStartTime);
//	myprintf(ferr,"FileName: %s\n",chInputFileName);
#endif
	cout << "Load DD from '"<<chInputFileName<<"'"<<endl;
	bRetVal = devDesc.LoadDeviceDescription();

	int iDevItemListSize = devDesc.ItemsList.size();

//	myprintf(ferr,"Number of variables which do not have CLASS defined: %d\n",iNumberOfNoClassVars);
	
	if(!bRetVal)
	{
		fprintf(ferr,"Parsing Failed for FileName: %s\n",chInputFileName);
		goto error_exit;
	}
#ifdef _PARSER_DEBUG
	FILE *fdone = fopen("Success.dat","a");
	fprintf(fdone,"Successfully Parsed   %s  [List size = %d]\n",chInputFileName,iDevItemListSize);
	fclose(fdone);
#endif
	unsigned a,o;
	devDesc.getTokVer(a, o);
	if ( a < 8 )
	{
		translatePTOC();// make 6 ptoc look like 8 ptoc 
	}

	cout<<"by Tokenizer V"<<a<<"."<<o<<endl;
	dump_devdir_tables(device_dir_6);
	dump_blkdir_tables(block_dir_6);

//	pGlobalDict->dumpdict(fm6);
//	pLitStringTable->dump();
	dump_items(&devDesc);



error_exit:
//	fclose(fout);
	fclose(ferr);

	/**
	char buf[80];
	printf("Press any ENTER to continue..\n");
	while (getc() != '\n')
		;
	//gets(buf);
	*/
	if (pHldOutBuf != NULL ) 
		cout.rdbuf(pHldOutBuf); 
	pHldOutBuf = NULL;
	if ( poutRedir && poutRedir->is_open() )
	{	 poutRedir->close(); RAZE(poutRedir);	}
	cout<<endl;
exit (0);
	return 0;

}




struct CMDTBL_ELEM
{
	SUBINDEX			subindex;
	ulong				number;
	ulong				transaction;
	ushort				weight;
	vector<COMMAND_INDEX> idxList;
	void clear(void){idxList.clear();number=transaction = 0;weight = 0;subindex=0;};
};

struct PTOC_ELEM
{
	ITEM_ID				item_id;
	vector<CMDTBL_ELEM>	rd_list;
	vector<CMDTBL_ELEM>	wr_list;
	void clear(void){rd_list.clear();wr_list.clear();item_id = 0;};
};



void translatePTOC(void)
{
	int i,bintIdx;
	COMMAND_INDEX cmdidx; 
	CMDTBL_ELEM   cmdelem;cmdelem.clear();
	PTOC_ELEM     ptocelem;ptocelem.clear();

	vector<PTOC_ELEM> tempCmdTbl8;

	int k,ptocIdx, j,h;

	for ( i = 0; i < block_dir_6.blk_item_tbl.count; i++)//FLAT_BLOCK_DIR_6
	{// for each block-item
		ptocelem.item_id = block_dir_6.blk_item_tbl.list[i].blk_item_id;
		bintIdx = block_dir_6.blk_item_tbl.list[i].blk_item_name_tbl_offset;

		for (k = 0, ptocIdx = block_dir_6.blk_item_name_tbl.list[bintIdx].read_cmd_tbl_offset;
				    k <       block_dir_6.blk_item_name_tbl.list[bintIdx].read_cmd_count;
			 k++,   ptocIdx++)
		{// for each read ptoc entry
			cmdelem.number		= block_dir_6.command_tbl.list[ptocIdx].number;
			cmdelem.transaction	= block_dir_6.command_tbl.list[ptocIdx].transaction;
			cmdelem.subindex	= block_dir_6.command_tbl.list[ptocIdx].subindex;
			cmdelem.weight		= block_dir_6.command_tbl.list[ptocIdx].weight;
			if (block_dir_6.command_tbl.list[ptocIdx].count)
			{
				for (j = 0; j < block_dir_6.command_tbl.list[ptocIdx].count; j++ )
				{
					cmdidx.id	= block_dir_6.command_tbl.list[ptocIdx].index_list[j].id;
					cmdidx.value= block_dir_6.command_tbl.list[ptocIdx].index_list[j].value;
					cmdelem.idxList.push_back(cmdidx);
				}
			}
			// cmdelem is filled
			// TODO: if this cmd.trans exists, just push the indexes into its indexes
			ptocelem.rd_list.push_back(cmdelem);
			cmdelem.clear();
		}

		for (k = 0, ptocIdx = block_dir_6.blk_item_name_tbl.list[bintIdx].write_cmd_tbl_offset;
				    k <       block_dir_6.blk_item_name_tbl.list[bintIdx].write_cmd_count;
			 k++,   ptocIdx++)
		{// for each read ptoc entry
			cmdelem.number		= block_dir_6.command_tbl.list[ptocIdx].number;
			cmdelem.transaction	= block_dir_6.command_tbl.list[ptocIdx].transaction;
			cmdelem.subindex	= block_dir_6.command_tbl.list[ptocIdx].subindex;
			cmdelem.weight		= block_dir_6.command_tbl.list[ptocIdx].weight;
			if (block_dir_6.command_tbl.list[ptocIdx].count)
			{
				for (j = 0; j < block_dir_6.command_tbl.list[ptocIdx].count; j++ )
				{
					cmdidx.id	= block_dir_6.command_tbl.list[ptocIdx].index_list[j].id;
					cmdidx.value= block_dir_6.command_tbl.list[ptocIdx].index_list[j].value;
					cmdelem.idxList.push_back(cmdidx);
				}
			}
			// cmdelem is filled
			// TODO: if this cmd.trans exists, just push the indexes into its indexes
			ptocelem.wr_list.push_back(cmdelem);
			cmdelem.clear();
		}
		// ptocelem filled
		tempCmdTbl8.push_back(ptocelem);
		ptocelem.clear();
	}// next block-item


	COMMAND_TBL_8_ELEM* pAsList;
	//COMMAND_INDEX cmdidx; 

	// tempCmdTbl8 is filled, copy to the flat dir then destroy
	block_dir_6.command_to_var_tbl.count = tempCmdTbl8.size();
	block_dir_6.command_to_var_tbl.list  = new PTOC_TBL_8_ELEM[tempCmdTbl8.size()];
	for ( k = 0; k < tempCmdTbl8.size(); k++)
	{
		block_dir_6.command_to_var_tbl.list[k].item_id = tempCmdTbl8[k].item_id;
		if (tempCmdTbl8[k].rd_list.size() || tempCmdTbl8[k].wr_list.size() )
		{
			if (tempCmdTbl8[k].rd_list.size())
			{
				block_dir_6.command_to_var_tbl.list[k].rd_count = tempCmdTbl8[k].rd_list.size();
				block_dir_6.command_to_var_tbl.list[k].rd_list  = 
								pAsList = new COMMAND_TBL_8_ELEM[tempCmdTbl8[k].rd_list.size()];
				for (j = 0; j < tempCmdTbl8[k].rd_list.size(); j++)
				{
					pAsList[j].number      = tempCmdTbl8[k].rd_list[j].number;
					pAsList[j].transaction = tempCmdTbl8[k].rd_list[j].transaction;
					pAsList[j].subindex    = tempCmdTbl8[k].rd_list[j].subindex;
					pAsList[j].weight      = tempCmdTbl8[k].rd_list[j].weight;
					if(tempCmdTbl8[k].rd_list[j].idxList.size())
					{
						pAsList[j].count = tempCmdTbl8[k].rd_list[j].idxList.size();
						pAsList[j].index_list = new COMMAND_INDEX[pAsList[j].count];
						for (h = 0; h < pAsList[j].count; h++)
						{
							pAsList[j].index_list[h] = tempCmdTbl8[k].rd_list[j].idxList[h];
						}// next index
					}
					else
					{
						pAsList[j].count      = 0;
						pAsList[j].index_list = NULL;
					}
					// may as well cleanup while we're down here
					tempCmdTbl8[k].rd_list[j].idxList.clear();
				}// next rd cmd
				// keep it clean
				tempCmdTbl8[k].rd_list.clear();
			}
			else
			{	
				block_dir_6.command_to_var_tbl.list[k].rd_count = 0;
				block_dir_6.command_to_var_tbl.list[k].rd_list  = NULL;
			}

			if (tempCmdTbl8[k].wr_list.size())
			{
				block_dir_6.command_to_var_tbl.list[k].wr_count = tempCmdTbl8[k].wr_list.size();
				block_dir_6.command_to_var_tbl.list[k].wr_list  = 
								pAsList = new COMMAND_TBL_8_ELEM[tempCmdTbl8[k].wr_list.size()];
				for (j = 0; j < tempCmdTbl8[k].wr_list.size(); j++)
				{
					pAsList[j].number      = tempCmdTbl8[k].wr_list[j].number;
					pAsList[j].transaction = tempCmdTbl8[k].wr_list[j].transaction;
					pAsList[j].subindex    = tempCmdTbl8[k].wr_list[j].subindex;
					pAsList[j].weight      = tempCmdTbl8[k].wr_list[j].weight;
					if(tempCmdTbl8[k].wr_list[j].idxList.size())
					{
						pAsList[j].count = tempCmdTbl8[k].wr_list[j].idxList.size();
						pAsList[j].index_list = new COMMAND_INDEX[pAsList[j].count];
						for (h = 0; h < pAsList[j].count; h++)
						{
							pAsList[j].index_list[h] = tempCmdTbl8[k].wr_list[j].idxList[h];
						}// next index
					}
					else
					{
						pAsList[j].count      = 0;
						pAsList[j].index_list = NULL;
					}
					// may as well cleanup while we're down here
					tempCmdTbl8[k].wr_list[j].idxList.clear();
				}// next wr cmd
				// keep it clean
				tempCmdTbl8[k].wr_list.clear();
			}
			else
			{	
				block_dir_6.command_to_var_tbl.list[k].wr_count = 0;
				block_dir_6.command_to_var_tbl.list[k].wr_list  = NULL;
			}
		}
		// else (no read or write commands)  skip it (should never happen)
	}// next command table entry
	tempCmdTbl8.clear();
}
