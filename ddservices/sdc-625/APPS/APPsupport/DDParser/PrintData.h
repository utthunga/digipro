#ifndef PRINTDATA_H
#define PRINTDATA_H

#include "DDlDevDescription.h"
#include "DDLItems.h"
#include "DDL6Items.h"



void dump_blank(int i);

void dump_reference(ddpREFERENCE& ref);

void dump_string(ddpSTRING * str);

void dump_enumlist(ENUM_VALUE_LIST *enmList);

void dump_type_size(TYPE_SIZE typeSize);

void dump_reference_list(REFERENCE_LIST *refList);

void dump_data_item_list(DATA_ITEM_LIST *dataItemList);

void dump_response_code_list(RESPONSE_CODE_LIST *respList);

void dump_transaction_list(TRANSACTION_LIST* transList);

void dump_menu_item_list(MENU_ITEM_LIST* menuList);

void dump_refresh_relation(REFRESH_RELATION* refReln);

void dump_unit_relation(UNIT_RELATION* unitReln);

void dump_item_array_element_list(ITEM_ARRAY_ELEMENT_LIST *itemArray);

void dump_member_list(MEMBER_LIST *memberList);

void dump_expression(ddpExpression *pExpr);

void dump_min_max_list(MIN_MAX_LIST *minMaxList);

void dump_conditional_attribute(DDlConditional *pCond, int indent);

void dump_conditional_chunks(DDlAttribute *pAttr);

void dump_attributes(ItemAttrList& alist);

void dump_items(DDlDevDescription* pDevDesc);

void dump_line_type(LINE_TYPE lineType);

void dump_section_chunks(DDlSectionChunks *pSecChunk); //Vibhor 190105: Added

void dump_devdir_tables(FLAT_DEVICE_DIR_6& fDD);//stevev 22feb08
void dump_blkdir_tables(FLAT_BLOCK_DIR_6&  fBD);//stevev 22feb08

#endif /*PRINTDATA_H*/