# Microsoft Developer Studio Project File - Name="DDParser" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=DDParser - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DDParser.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DDParser.mak" CFG="DDParser - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DDParser - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DDParser - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DDParser - Win32 ReleaseWithSymbols" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DDParser - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\..\COMMON" /D "_UNICODE" /D "DBG_PVFC" /D "XMTR" /D "WIN32" /D "NDEBUG" /D "_MBCS" /YX /FD /Zm400 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DDParser - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /Zi /Od /I "..\..\..\COMMON" /D "_UNICODE" /D "XMTR" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Fr /FD /GZ /Zm200 /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DDParser - Win32 ReleaseWithSymbols"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DDParser___Win32_ReleaseWithSymbols"
# PROP BASE Intermediate_Dir "DDParser___Win32_ReleaseWithSymbols"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DDParser___Win32_ReleaseWithSymbols"
# PROP Intermediate_Dir "DDParser___Win32_ReleaseWithSymbols"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\..\COMMON" /D "XMTR" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "_AFXDLL" /YX /FD /Zm400 /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Zi /Od /I "..\..\..\COMMON" /D "_UNICODE" /D "DBG_PVFC" /D "XMTR" /D "WIN32" /D "NDEBUG" /D "_MBCS" /FR /YX /FD /Zm400 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "DDParser - Win32 Release"
# Name "DDParser - Win32 Debug"
# Name "DDParser - Win32 ReleaseWithSymbols"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Attributes.cpp
# End Source File
# Begin Source File

SOURCE=.\DDI_Lang.cpp
# End Source File
# Begin Source File

SOURCE=.\DDL6Eval.cpp
# End Source File
# Begin Source File

SOURCE=.\DDL6FetchNattach.cpp
# End Source File
# Begin Source File

SOURCE=.\DDl6Items.cpp
# End Source File
# Begin Source File

SOURCE=.\DDl_Attr_Parse.cpp
# End Source File
# Begin Source File

SOURCE=.\DDlConditional.cpp
# End Source File
# Begin Source File

SOURCE=.\DDlDevDesc6.cpp
# End Source File
# Begin Source File

SOURCE=.\DDlDevDescription.cpp
# End Source File
# Begin Source File

SOURCE=.\DDlItems.cpp
# End Source File
# Begin Source File

SOURCE=.\DDS_UPCL.CPP
# End Source File
# Begin Source File

SOURCE=.\Dictionary.cpp
# End Source File
# Begin Source File

SOURCE=.\Endian.cpp
# End Source File
# Begin Source File

SOURCE=.\Eval_Item.cpp
# End Source File
# Begin Source File

SOURCE=.\Evl_dir.cpp
# End Source File
# Begin Source File

SOURCE=.\Fetch_item.cpp
# End Source File
# Begin Source File

SOURCE=.\LitStringTable.cpp
# End Source File
# Begin Source File

SOURCE=.\Panic.cpp
# End Source File
# Begin Source File

SOURCE=.\Parse_Base.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintData.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Attributes.h
# End Source File
# Begin Source File

SOURCE=.\DD_Header.h
# End Source File
# Begin Source File

SOURCE=.\DDL6Items.h
# End Source File
# Begin Source File

SOURCE=.\DDl_Attr_Parse.h
# End Source File
# Begin Source File

SOURCE=.\DDLBaseItem.h
# End Source File
# Begin Source File

SOURCE=.\DDlConditional.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\DDLDEFS.H
# End Source File
# Begin Source File

SOURCE=.\DDlDevDescription.h
# End Source File
# Begin Source File

SOURCE=.\DDLItems.h
# End Source File
# Begin Source File

SOURCE=.\Dictionary.h
# End Source File
# Begin Source File

SOURCE=.\Endian.h
# End Source File
# Begin Source File

SOURCE=.\Eval_Item.h
# End Source File
# Begin Source File

SOURCE=.\evl_lib.h
# End Source File
# Begin Source File

SOURCE=.\evl_loc.h
# End Source File
# Begin Source File

SOURCE=.\Fetch_item.h
# End Source File
# Begin Source File

SOURCE=.\Flats.h
# End Source File
# Begin Source File

SOURCE=.\Globaldefs.h
# End Source File
# Begin Source File

SOURCE=.\LitStringTable.h
# End Source File
# Begin Source File

SOURCE=.\panic.h
# End Source File
# Begin Source File

SOURCE=.\Parse_Base.h
# End Source File
# Begin Source File

SOURCE=.\Retn_Code.h
# End Source File
# Begin Source File

SOURCE=.\Table.h
# End Source File
# End Group
# End Target
# End Project
