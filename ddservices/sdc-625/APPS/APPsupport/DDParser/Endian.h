/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2002 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/***************************************************************************
**
** APPLICATION: HARTSoftwareMux 
** HEADER NAME: Endian.h
**
** PURPOSE:
**   This file contains definitions of functions to read and write values in 
**   different multi-byte data representation (big endian, little endian).
**
** REVISION HISTORY:
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 16-Jul-02  TSR Prasad  initial release
**
** Date...: 16-Jul-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
****************************************************************************/

/**************************************************************************/
#ifndef ENDIAN_H
#define ENDIAN_H

#define FORMAT_BIG_ENDIAN		1
#define FORMAT_LITTLE_ENDIAN	2

bool write_word(void *dest, WORD *source, int format);
bool write_dword(void *dest, DWORD *source, int format);
bool write_float(void *dest, FLOAT *source, int format);
bool read_word(WORD *dest, void *source, int format);
bool read_dword(DWORD *dest, void *source, int format);
bool read_dword_spl(DWORD *dest, void *source, int size, int format);
bool read_float(FLOAT *dest, void *source, int format);


//Copied from COMMON/Endian.h
#define USS(a) ((unsigned short)(a))
#define ULL(b) ((unsigned long)(b))
#define UHH(c) ((UINT64) (c))

#ifdef IS_BIG_ENDIAN /* NOT INTEL */
#else				 /* IS  INTEL */

#define REVERSE_S(s) (  ( USS(s) << 8 ) | ( USS(s) >> 8 )  )

#define REVERSE_L(l) (  ( ULL(REVERSE_S(ULL(l))) << 16) | ( REVERSE_S(ULL(l) >> 16) &0xFFFF)  )
//                                   note: the mask in the second half is due to compiler error
#define REVERSE_H(L) (  (( UHH(REVERSE_L(UHH(L))) << 32) & 0xFFFFFFFF00000000ULL) | (REVERSE_L( UHH(L) >> 32) & 0xFFFFFFFF )) // L&T Modifications : PortToAM335
//                                   note: masks are to protect against Bill-isims.


#endif



























#endif /* ENDIAN_H */
