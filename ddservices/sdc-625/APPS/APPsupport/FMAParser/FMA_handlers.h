/**********************************************************************************************
 *
 * $Workfile: FMA_handlers.h $
 * 07sep11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_handlers.h : These are the content handlers
 *
 * #include "FMA_handlers.h"
 *
 */

#ifndef _FMA_HANDLERS_H
#define _FMA_HANDLERS_H

#ifdef INC_DEBUG
#pragma message("In FMA_handlers.h") 
#endif

#include <vector>
#include "varient.h"
#include "FMx_general.h"
#include "FMA_defs.h"
#include "FMx_handler.h"
#include "FMA_Support_tables.h"
#include "FMA_Attributes.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_handlers.h") 
#endif

/**** docs *****
SEQUENCE  {
	header-info				Header_Info,		FMx_header  same for all
	DDOD-description		DDOD_ODES,			similar to meta data
	DDOD-static-objects		DDOD_SOD,			the tables and objects
	ddod-object-data		DDOD_Object_Data	
	ddod-image-data			DDOD_Image_Data
}
********************/

class FMA_Object;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 Defines
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#define _TCHAR_DEFINED	// avoids issue in winnt.h

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  These classes hold the FMA interface definition 
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMA_Description_handler : public FMx_handler
{
public:	// construct/destruct
	FMA_Description_handler(FMA_Object* pOwner);
	virtual ~FMA_Description_handler();

public:	// data
	unsigned /* 14jan16 short*/ firstItemIndex;// in both fm8 & fma
	unsigned /* 14jan16 short*/ item_count;// in both fm8 & fma
	unsigned /* 14jan16 short*/ block_count;

public: // workers
	int evaluateExtension();// breaks up the extension part
	int findOffset(unsigned offset){ return -1;};// tells what to do with the current location

	int out (void);		// extension data to stdout
	void out2(void) {};	// no-operation in FMA
	void out3(void) {};	// no class data here
};

class FMA_DevTables_handler : public FMx_handler
{
public:	// construct/destruct
	FMA_DevTables_handler(FMA_Object* pOwner);
	virtual ~FMA_DevTables_handler();

public: // Table Data
	unsigned        deviceTableCount; // first byte of Device_Table_Set_Extension;
	unsigned        dtcOffset;
	unsigned        dtcLength; // always 1
	unsigned char*  pTblCnt;

	FMA_Table*		deviceTables[DEV_TBL_CNT];// an array of  pointers2table
	unsigned		deviceTblOffset[DEV_TBL_CNT];// file offset of raw table start

public: // workers
	int evaluateExtension();		// breaks up the extension part
	int findOffset(unsigned offset);// tells what to do with the current location
    int deviceTablesCount();		// count the number of non-null pointers in the array-12-1-11 timj

	int out(void);		// extension data to stdout(fixed done in the object this is outpayload)
	void out2(void) {};		// no operation in FMA
	void out3(void);	// class data to stdout
	void out5(TableType_e tableType, bool showRandom=true);	// reconcile out operation in FMA
};

class FMA_BlkTables_handler : public FMx_handler
{
public:	// construct/destruct
	FMA_BlkTables_handler(FMA_Object* pOwner);
	virtual ~FMA_BlkTables_handler();

public: // Table Data
	unsigned        blockTableCount; // first byte of Block_Table_Set_Extension;
	unsigned        btcOffset;
	unsigned        btcLength; // always 1
	unsigned char*  pTblCnt;	// raw data 

	FMA_Table*		block_Tables[BLK_DIR_TBL_CNT_A];// an array of table pointers
	unsigned		blockTblOffset[BLK_DIR_TBL_CNT_A];

public: // workers
	int evaluateExtension();// breaks up the extension part
	int findOffset(unsigned offset);// tells what to do with the current location
	int blockTablesCount(void);

	int  out (void); // extension data to stdout
	void out2(void){};// no-operation in FMA
	void out3(void);  // class data to stdout
	void out5 (TableType_e tableType, bool showRandom=true); // reconcile operation	
};

class FMA_Item_handler : public FMx_handler
{
public:	// construct/destruct
	FMA_Item_handler(FMA_Object* pOwner);
	virtual ~FMA_Item_handler() {}

public:	// data
	unsigned char	item_type;
	unsigned char	item_subtype;
	unsigned        item_number;//aka symbol ID
	unsigned		item_symbol;	// index into symbol table
	unsigned		item_attr_cnt;

	unsigned        symbolTableIndex;

	// attribute list
	FMA_AttributeList items_attributes;

	// other helpful information
	unsigned        length2firstAttr;
//	aCitemBase*		protoItem;

public: // workers
	int evaluateExtension();// breaks up the extension part

	void generatePrototype();
	void generatePrototypeUnitRel();		// specialized behavior for 
	void generatePrototypeRefreshRel();		// places where 8 and A attributes
	void generatePrototypeAllOther();		// don't align 0ne-to-one
	virtual void generatePrototypeDefaultAttrs(vector <FMx_Attribute *> &attrlist);	// 
	void patchPrototypeAttrs(aCitemBase* acitem);
	//bool hasAttrType(int atype);
	//void generateDefaultPrototype(FMx_Attribute *pattr);

	int findOffset(unsigned offset);// tells what to do with the current location

	int out(void);  // extension data to stdout
	void out2(void){};// no-operation in FMA
	void out3(void);  // class data to stdout
};

#endif //_FMA_HANDLERS_H
