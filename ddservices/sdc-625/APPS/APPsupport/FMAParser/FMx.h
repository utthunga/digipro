/**********************************************************************************************
 *
 * $Workfile: FMx.h $
 * 20Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx.h : FMx class description file...
 *
 * #include "FMx.h"
 *
 */

#ifndef _FMX_
#define _FMX_

#include <fstream>		// files as streams

#include "FMx_general.h"
#include "FMx_defs.h"
#include "FMx_header.h"
#include "FMx_Dict.h"
#include "FMx_visitor.h"
#include "FMx_Symbol.h"

class FMx_meta;
class FMx_ObjectSet;

extern unsigned currentObjectIndex;		// debugging use only for setting breakpoints;

// 28mar16 - moved to FMx extern unsigned char *	pImageBuffer;	// image binaries in contiguous block
using namespace std;

/*********************************************************************************************
 * FMx
 ********************************************************************************************/
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the *.fm? file, it handle file stuff but instatiatiates sub parts to
 *			match the version number of the encoded file format. - Is the DD-File!
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMx
{
public:		
	FMx(); // constructor
	virtual ~FMx();

public:	// data - leave public since we are supposed to know what we're doing	
	bool    saveBinary;

//	static FMx_Dict* DictionaryDict;	  // could be version 8 or a fill with table pointer
//	static FMx_Dict* StringDict;   // could be version 8 or a fill with table pointer when we get it

	unsigned char *	pImageBuffer;	// image binaries in contiguous block

	FMx_Symbol* daSymbolTbl;  // usually v A

//	wchar_t* pFilename;// this is a pointer to argv[1], do not delete
	FILE*    fp;
	unsigned long fileSize;
	wchar_t  langCode[LANGCD_LEN];

public: //data
	unsigned idx;// we keep count of the object number index here

public:	// data
	FMx_header		headerData;// full up class with data and function
	FMx_meta*		pMetaData; // could be version 8 or a
	FMx_ObjectSet*	pObj_Data; // could be version 8 or a

	virtual RETURNCODE open(wchar_t* pFilename,const wchar_t* pLangCode, FMx_Dict *pDict, FMx_Dict *pLit);

public:	// workers
	virtual RETURNCODE VisitAll(FMx_visitor* pVistor);
	virtual RETURNCODE indexFile(void) = 0; // scans the file building pointer indexes, rewinds when done
	virtual RETURNCODE readIn_File(void) = 0;// fill payloads from index
	virtual RETURNCODE process_File(void) = 0;
	
	virtual void release(void) = 0;// loose the payload memory
	virtual void out(void) = 0;// dump all the information to a file
	virtual void generate(void) = 0;

	// helpers
	void getLiteralString(int strInt,   wstring& retVal);
	void getDictionString(int strInt,   wstring& retVal);
	void getDictionString(wstring name, wstring& retVal);
	void getSymbolName(unsigned symNum,  string& retVal, int& itmType);
	void getSymbolNameByIndex(unsigned index,  string& retVal, int& itmType);

	// accessors
	unsigned getObjectCount(void);
	unsigned getFirstItemIndex(void);
	unsigned getNumberBlocks(void);
};

#endif //_FMX_
