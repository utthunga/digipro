/**********************************************************************************************
 *
 * $Workfile: FMx_handler.h $
 * 07sep11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_handler.h : This is the abstract base class for the content handlers
 *  It defines the interface to the DD-Object's specificity class (its handler).
 *
 * #include "FMx_handler.h"
 *
 */

#ifndef _FMA_FMx_HANDLER_H
#define _FMA_FMx_HANDLER_H

#ifdef INC_DEBUG
#pragma message("In FMx_handler.h") 
#endif

#include "FMx_general.h"
#include "FMx_Attribute.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMx_handler.h") 
#endif

class FMx_Base;
class aCitemBase;	// the item's prototype base class

typedef struct Segment_s
{
	unsigned    offset;
	unsigned    actualoffset;// the ftell from the read start
	unsigned    length;
} segment_t;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class holds the abstract interface definition for all the handlers
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMx_handler
{
public:	// construct/destruct
	FMx_handler(FMx_Base* p0) : pBObj(p0), protoItem(NULL) {};
	virtual ~FMx_handler() {};

public:	// data
	FMx_Base* pBObj;//owner
	aCitemBase*		protoItem;

public: // workers
	virtual int evaluateExtension() = 0;// breaks up the extension part <aka Interpret_Payload>
	virtual void generatePrototype() {}	// make template class (nop on non-item)
	virtual int readChunk(unsigned& sz) { return 0; };// done if findOffset successful...

	virtual void generatePrototypeDefaultAttrs(vector <FMx_Attribute *> &attrlist);	// 
	void generateDefaultPrototype(FMx_Attribute *pattr);
	bool hasAttrType(vector<FMx_Attribute *>&attrlist, unsigned atype);

	// A&8 - outputs extension data to stdout
	// returns 0 if it was handled OK, !0 if error or not supported
	virtual int out(void) = 0; 

	// 8 - outputs segment data to stdout
	// A - no-op
	virtual void out2(void) = 0;

	// A&8 - outputs class information to stdout - nop on non-item 
	virtual void out3(void) = 0;
	
	// no table output for reconcile mode
	int reconcileOut(void);

protected:
	int countSetBits(unsigned msk);
};

#endif //_FMA_FMx_HANDLER_H