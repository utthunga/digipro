/**********************************************************************************************
 *
 * $Workfile: FM8_Attributes.h $
 * 20mar12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Attributes.h : These are the content handlers
 *
 * #include "FM8_Attributes.h"
 *
 */

#ifndef _FM8_ATTRIBUTES_H
#define _FM8_ATTRIBUTES_H

#include "FMx_Attribute.h"
#include "v_N_v+Debug.h"


class FM8_AttributeList : public vector<FMx_Attribute*>
{	
public: // construct / destruct
	FM8_AttributeList() : attr_count(0), item_type(), objHeapStartOffset(0) {};
	virtual ~FM8_AttributeList();			

public: // data
	UCHAR			attr_count;		// how many attributes are expected
	unsigned		objectIndex;	// the file-based index number for identification
	unsigned char	item_type;		// the version 8 has to have this

	unsigned        objHeapStartOffset;// this item's annex offset from beginning of file

#ifdef _DEBUG
	int remainingLen;
	int remainingCnt;
#endif

public: // methods
	int evaluateAttributePayload(UCHAR* pData, int length, unsigned offset, UCHAR& count, bool isheap = false);
	int evaluateAttributeHeap(UCHAR* pData, int length, unsigned offset, UCHAR& count);
	int out(void); // payload data to stdout	

public:
	FMx_Attribute* createAttr(unsigned char attr_type); // factory

protected:
	void insertAttr(FMx_Attribute*);// inserts 'em in numerical order
};

typedef vector<FMx_Attribute *>::iterator fm8AttrIT;

#endif //_FM8_ATTRIBUTES_H