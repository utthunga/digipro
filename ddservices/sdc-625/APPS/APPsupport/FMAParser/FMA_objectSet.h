/**********************************************************************************************
 *
 * $Workfile: FMA_objects.h $
 * 09aug11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_objects.h : FMA's basic OBJECT class description file and its list
 *
 * #include "FMA_objects.h"
 *
 */

#ifndef _FMA_FMA_OBJECTS_H
#define _FMA_FMA_OBJECTS_H

#include "FMx_objectSet.h"		// holds defs and general
#include "FMA_object.h"

#include <stdio.h>
#include <vector>

using namespace std;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class holds all the objects in an FMA file
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMA_ObjectSet : public FMx_ObjectSet
{
public:	// construct/destruct
	FMA_ObjectSet(FMx* pTC, FILE* pF, bool sb) : FMx_ObjectSet(pTC, pF, sb, 0x83) {};
	virtual ~FMA_ObjectSet();

public:	// data
	FMA_pObjectList_t objList;

public: // workers
	virtual RETURNCODE index_ObjectData(unsigned& size);
	
public:	// required
	RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor);
};


#endif // _FMA_FMA_OBJECTS_H
