/**********************************************************************************************
 *
 * $Workfile: FMA_object.cpp $
 * 21Jul11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Object class implementation...the model of One File Object
 *		
 *
 */

#include "FMA_object.h"

FMA_Object::FMA_Object(FMx* pTC, FILE* pf, bool saveBin) : FMx_Base(pTC, pf,saveBin, 0x82)
{
	obj_Fixed   = 0;
	obj_Payload = 0;

	object_offset     = 0;

	obj_Index   = 0;
	obj_type    = obj_Unknown; // 0
	obj_Length  = 0;// in UCHARs

	pHandler    = NULL;
}


FMA_Object::~FMA_Object()
{
	if (obj_Fixed)
	{
		delete[] obj_Fixed; obj_Fixed = 0;
	}

	if (obj_Payload != 0)
	{
		delete[] obj_Payload; obj_Payload = 0;
	}

	delete pHandler;
}

/*********************************************************************************************
 *	read_Object_Info   at current file pointer
 *
 *	in: we normally seek over the extension.  When we are used as a piece of reading an
 *      entire object, we read our part and leave the file pointer on the next byte
 *
 *	returns: err code or success 
 *            if skipExtensionRead the file pointer will be on the first byte of the payload
 *          else the file pointer  will be just after last byte of the payload
 *
 *	read in the object fixed and then read in the extension if asked or skip the extension
 *	Skip is used when indexing a file for future read-in
 *		
 ********************************************************************************************/
RETURNCODE FMA_Object::read_Object_Info(bool skipExtensionRead/*=true*/)
{
	if (fp == 0 )
		return FMx_BADPARAM;

	object_offset = ftell(fp);

	obj_Fixed = new UCHAR[OBJ_FIXED_SIZE];
	int returns = fread(obj_Fixed, 1, OBJ_FIXED_SIZE, fp);
	if (returns != OBJ_FIXED_SIZE)
	{
		delete[] obj_Fixed; obj_Fixed = 0;
		return FMx_READFAIL;
	}// else continue working
	
	obj_Index  = REVERSE_L( *((unsigned int*) &obj_Fixed[0]) );
	obj_type   = (obj_type_t)obj_Fixed[4];  
	obj_Length = REVERSE_L( *((unsigned int*) &obj_Fixed[5]) );

	if (skipExtensionRead)// the default, normal condition
	{		
		if ( fseek(fp,obj_Length,SEEK_CUR) )// zero on success
		{
			return FMx_REPOSFAIL;
		}// we're now pointing to the first byte of next object		 
	}
	else
	{	assert(0);// should never happen - for now
		obj_Payload  = new UCHAR[obj_Length];
		returns = fread(obj_Payload, 1, obj_Length, fp);
		if (returns != obj_Length)
		{
			delete[] obj_Payload; 
			obj_Payload = 0;
			return FMx_READFAIL;
		}// else continue to the exit
		// we're now pointing to the first byte of next object	
	}
	if ( ! saveBinary )
	{
		delete[] obj_Fixed; obj_Fixed = 0;
		if ( obj_Payload )
		{
			delete[] obj_Payload; 
			obj_Payload = 0;
		}
	}

	return SUCCESS;
}

/*********************************************************************************************
 *	read_ObjectPayload   at recorded offset
 *
 *	in: nothing
 *
 *	returns: err code or success with the file pointer just after last byte of the payload
 *
 *	seek to recorded payload location, read in the extension as payload
 *  called by read payload visitor since the object is better suited to do this errand
 *		
 ********************************************************************************************/
RETURNCODE FMA_Object::read_ObjectPayload()
{
	if (obj_Payload != NULL)// we've already read it somewhere
		return SUCCESS;
	if (fp == 0 || objA_payload_offset() == 0)// no payload is at zero offset
		return FMx_BADPARAM;

	if (fseek(fp, objA_payload_offset(), SEEK_SET))// zero on success
	{
		return FMx_REPOSFAIL;
	}

	if (obj_Length)
	{
		obj_Payload = new UCHAR[obj_Length];
		int returns = fread(obj_Payload, 1, obj_Length, fp);
		if (returns != obj_Length)
		{
			delete[] obj_Payload; 
			obj_Payload = 0;
			return FMx_READFAIL;
		}// else continue working
	}// else nothing to read

	// the saveBinary will only come to play when the data is extracted from the binary

	return SUCCESS;
}
