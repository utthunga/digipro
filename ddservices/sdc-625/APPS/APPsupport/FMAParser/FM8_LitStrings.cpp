/**********************************************************************************************
 *
 * $Workfile: FM8_LitStrings.cpp $
 * 29oct12 - stevev - from fma
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_LitStrings class implemantation...the model of the file
 *		
 *
 */

#include "Convert.h"
#include "FM8_LitStrings.h"
#include "FM8_Support_tables.h"
#include "logging.h"



FM8_LitStrings::FM8_LitStrings()
{
}


FM8_LitStrings::~FM8_LitStrings()
{
}


void FM8_LitStrings::setTheTable( FMx_Table* pTbl)
{
	pTable = pTbl;
	if (pTbl == NULL)
	{
		LOGIT(CERR_LOG,L"FM8_LitStrings::setTheTable passed a NULL table.\n");
		return;
	}
	FM8_LitStringTable* pT = dynamic_cast<FM8_LitStringTable*>( pTbl );

	FM8_LitStringTable::iterator vLIT;

	wstring locStr;

	for (vLIT = pT->begin(); vLIT != pT->end(); ++vLIT )
	{
		locStr = *vLIT;

#ifdef TOKENIZER
		/*
		* In tokenizing, we want all the full string with all
		* languages, as these will be stored in the final
		* encoded DD
		*/
#else
		ConvertToString::filterString(locStr);
#endif
		addStr(locStr);

		if (! saveBinary)
		{// empty the original (DD) string
			(*vLIT).clear();// try to prevent having a bunch of string copies
		}	
	}// next row
}


int FM8_LitStrings::retrieveStr(int  str,   wstring& retVal)
{
	int rtn = FAILURE;
#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	if (str >= (int)dictStrList.size() || str < 0 )
	{
		retVal = DICT_STRING_NOT_FOUND;
	}
	else
	{
		retVal = dictStrList[str].StringItself;
		rtn = SUCCESS;
	}
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif

	return rtn;
}
