/**********************************************************************************************
 *
 * FM8_conditional.h 
 * 24oct13 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_conditional.h : Based on FMx_Conditional.h
 *
 * #include "FM8_conditional.h"
 *
 */

#ifndef _FM8_FMx_CONDITIONAL_8_H 
#define _FM8_FMx_CONDITIONAL_8_H

#ifdef INC_DEBUG

 #pragma message("In FM8_conditional.h") 

#endif

#include "FMx_Conditional.h"


#include "QA+Debug.h"

#ifdef INC_DEBUG

 #pragma message("    Finished includes from FM8_conditional.h") 

#endif


#include "FMx_defs.h"
#include "FMx_conditional.h"
#include "FM8_Attributes_Ref&Expr.h"
#include "logging.h"
#include "FM8_Attributes.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	fC_8_baseConditional is based on the fCbaseConditional but overrides its eval & hydrate
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            
template<class CLAUSECLASS_8, class PAYLOADCLASS_8>
class fC_8_baseConditional :  
						public fCbaseConditional<CLAUSECLASS_8,abCExpression8,PAYLOADCLASS_8>
{
public: // override the contained class
    class  fC_8_clauseItem : public
                fCbaseConditional<CLAUSECLASS_8,abCExpression8,PAYLOADCLASS_8>::fCclauseItem
    {
        
	public: /* methods */
	    // virtual evaluate attribute
	    // virtual hydrate prototype
	
    }; // end of fC_8_clauseItem



public: /* methods */
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void hydratePrototype(aCondDestType* p8condDest /*aCgenericConditional* cond*/, bool coalesce);
protected:
	void hydrateme(aCgenericConditional *cond, bool coalesce);

};// end of fC_8_baseConditional



/*===========================================================================================*
  fC_8_baseConditional  implementation
  ===========================================================================================*/
template<class CLAUSECLASS_8, class PAYLOADCLASS_8>
int fC_8_baseConditional<CLAUSECLASS_8, PAYLOADCLASS_8>::
                         evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{	// note that the caller is responsible for setting the conditional type
	// note that the incoming length is the length of this conditional item so it
	// should be zero on exit.
	RETURNCODE ret = 0;	
	exitValues_t    holdExit;
	unsigned   tagValue = 0;
	unsigned   tagLength;

//	clauseElement* pClause;

//	int      exitLength = 0;
//	unsigned exitOffset = offset + length;
//	UCHAR*   exitPdata  = (*pData) + length;
//	unsigned entryOffset = offset;// for debugging only

	pushExitValues(holdExit, pData, length, offset);

	if (axiomType == aT_IF_expr)//1 (was 0)
	{// we are the IF conditional
		ret = axiomExpr.evaluateAttribute(pData, length, offset);
		if (ret == SUCCESS && length > 1)
		{//	then-clause		Conditional[structure],
			fC_8_clauseItem thenClause;

			thenClause.thisClause = cT_IF_isTRUE; // then clause
			ret = thenClause.evaluateAttribute(pData, length, offset);
			if (ret == SUCCESS)
			{
				clauseList.push_back(thenClause);
				thenClause.destroy();
			}
			else
			{
				popExitValues(holdExit, pData, length, offset);	// then exit
			}
		}// end of then clause
		if (ret == SUCCESS && length > 1)// ie we still have some length
		{//	else-clause		Conditional[structure]  (OPTIONAL on explicit-tag.length)
			fC_8_clauseItem elseClause;

			elseClause.thisClause = cT_IF_isFALSE; // else clause
			ret = elseClause.evaluateAttribute(pData, length, offset);
			if (ret == SUCCESS)
			{
				clauseList.push_back(elseClause);
				elseClause.destroy();
			}
			else
			{
				popExitValues(holdExit, pData, length, offset);	// then exit
			}
		}// end of else clause
	 // if conditional complete
	}
	else 
		
	if (axiomType == aT_SEL_expr)//2 (was 1)
	{// we are the SELECT conditional
		ret = axiomExpr.evaluateAttribute(pData, length, offset);
		if ( ret == SUCCESS && length > 1 )// ie we have a case/default clause
		{//	list of case/default-clauses	Expression -Conditional[structure],			
			fC_8_clauseItem caseClause;

			while ( length > 1 )// get clauses till out of length
			{	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
				ret = parseTag(pData, length,   tagValue,   tagLength, offset );
				if ( ret != SUCCESS || tagLength != 0 || tagValue > 1) //defaultx/)
				{		
					LOGIT(CERR_LOG,"Error: SELECT clause Type is invalid. (0x%02x)\n)", tagValue);
					break;// out of while loop
				}

				caseClause.thisClause = (tagValue) ? ct_SEL_isDEFAULT :  cT_CASE_expr;  

				if (caseClause.thisClause == cT_CASE_expr)
				{// this one has an expression
					ret = caseClause.clauseExpr.evaluateAttribute(pData,length,offset);
					if (ret != SUCCESS || length < 2)
					{
						ret = FMx_BAD_PARSE;
						break; // out of while loop
					}// else do the conditional
				}// else default has no expression

				ret = caseClause.evaluateAttribute(pData, length, offset);
				if (ret == SUCCESS)
				{//  so
					clauseList.push_back(caseClause);
					caseClause.clear();
				}
				else // eval error
				{
					break;// out of while loop
				}
			}// wend there are more clauses to do

			if (ret != SUCCESS)// an error exit
			{
				popExitValues(holdExit, pData, length, offset);	// then exit
			}
		}// else // error in expression - just exit
	}
	else 
		
		
	if (axiomType == aT_Direct)//4 ( was 2 )
	{//		on direct
		//			sequence of PAYLOADCLASS...vector< FMx_Attribute* > pPayloadList;
		// the new type is a single list-based payload that will evaluate iteself and
		// parse the elements, putting them into it's list

		fCclauseItem directClause;
		directClause.directClass = new PAYLOADCLASS_8;

		directClause.thisClause = cT_Direct; // added stevev 20feb14 
		ret = directClause.directClass->evaluateAttribute(pData, length, offset);

		if (ret != SUCCESS || length > 1)
		{
			delete directClause.directClass; directClause.directClass = NULL;
			popExitValues(holdExit, pData, length, offset);	// then exit
		}
		else
		{			
			clauseList.push_back(directClause);
		}
	}
	else //cdt_INVALID
	{
		LOGIT(CERR_LOG,"Error: Conditional List Type is unknown.\n");
		ret = FMx_BAD_PARSE;
	}

	return ret;	
};


template<class CLAUSECLASS_8, class PAYLOADCLASS_8>
void fC_8_baseConditional<CLAUSECLASS_8, PAYLOADCLASS_8>::
	hydratePrototype(aCondDestType* p8condDest, bool coalesce = false)
{
	aCgenericConditional* cond = NULL;
	aCconditional *acond = NULL;
	if (p8condDest->isAlist())
	{
		cond = (aCgenericConditional *) p8condDest;
	}
	else
	{
		acond = dynamic_cast<aCconditional *> (p8condDest);
		assert(acond);// can't be null
		cond = dynamic_cast<aCgenericConditional *> (acond);
		assert(cond);// can't be null
	}

	hydrateme(cond, coalesce);
};

template<class CLAUSECLASS_8, class PAYLOADCLASS_8>
void fC_8_baseConditional<CLAUSECLASS_8, PAYLOADCLASS_8>::
hydrateme(aCgenericConditional* cond, bool coalesce = false)
{
	if (coalesce == false)
	{
		cond->priExprType = (expressionType_t)axiomType;
		axiomExpr.hydratePrototype(&(cond->priExpression));
	}

	fCclauseItem* pCI;
	for(iTclause iT = clauseList.begin(); iT != clauseList.end(); ++iT)
	{
		pCI = &(*iT);
		if (coalesce)
		{
			pCI->hydratePrototype(cond->destElements.back(), coalesce);
		}
		else
		{
			aCgenericConditional::aCexpressDest edest;
			pCI->hydratePrototype(edest, coalesce);
			cond->destElements.push_back(edest);
			edest.destroy(); // we're assuming the push did a deep copy of the dest
		}
	}

};
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	fC_8_Conditional is based on the fC_8_baseConditional but overrides its eval & hydrate 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
template<class PAYLOADCLASS_8C>
class fC_8_Conditional : public
      //fC_8_baseConditional<fC_8_Conditional<PAYLOADCLASS_8C>,EXPRESSIONCLASS_8C,PAYLOADCLASS_8C>
      fC_8_baseConditional<fC_8_Conditional<PAYLOADCLASS_8C>, PAYLOADCLASS_8C>, fCbase
{// base class holds all the data & functionality
public: /* housekeeping */
    fC_8_Conditional() {};
    fC_8_Conditional(const fC_8_Conditional& s) { operator=(s); };
    virtual ~fC_8_Conditional()                 { destroy();    };
    fC_8_Conditional& operator=(const fC_8_Conditional& s)
	{   fC_8_baseConditional<fC_8_Conditional<PAYLOADCLASS_8C>, PAYLOADCLASS_8C> :: operator=(s);
        return *this;
    };
    // let    int destroy()  fall thru to the parent

    
public: /* methods */
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{ 
		RETURNCODE ret = 0;	
		//exitValues_t    holdExit;
		unsigned   tagValue = 0;
		unsigned   tagLength;
		unsigned   exitOffset;
		int		   exitLength, workingLength;
		uchar *exitData;

		if (forceDirect)
		{
			tagValue = cdt_DIRECT;
			tagLength= length;
		}
		else	// normal path
		{
			// explicit tag 
			ret = parseTag(pData, length,  tagValue,  tagLength, offset );
		}

		if (tagValue > cdt_DIRECT )
		{// then illegal
			LOGIT(CERR_LOG,"Error: Conditional Type is wrong. (%u)\n", tagValue);
			return -1;
		}

		axiomType = (axiomType_t) ConvertToDDL::ClauseType((condType_t)tagValue);

		// what things should be when we get out of eval
		if (tagLength == 0)
		{
			LOGIT(CERR_LOG,"Error: Conditional Item expected tagLength > 0.\n");
			ret = -1;
		}
		else
		{
			exitLength = length  - tagLength;
			exitOffset = offset   + tagLength;
			workingLength = tagLength;// so we can get an int reference
			exitData = *pData + tagLength;

			ret = fC_8_baseConditional<fC_8_Conditional<PAYLOADCLASS_8C>,PAYLOADCLASS_8C> 
				:: evaluateAttribute(pData, workingLength, offset);

			if ( ret == SUCCESS && workingLength == 0 && exitOffset == offset )
			{
				// oh happy day!
			}
			else
			{
				LOGIT(CERR_LOG,"Error: Conditional Item failed to parse.\n");
			}
			*pData = exitData;
			length = exitLength;
			offset = exitOffset;
		}

		return ret;
	};
	
	virtual void hydratePrototype(aCondDestType* p8condDest)
	{   return 
		fC_8_baseConditional<fC_8_Conditional<PAYLOADCLASS_8C>,PAYLOADCLASS_8C> 
			:: hydratePrototype(p8condDest, false);
	};


	virtual aCondDestType *makeaCclass() 
	{ 
		return (aCondDestType *) new aCconditional();
	};

	//virtual int out(void);
};




/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    fC_8_condList is based on the parent class with a unique  handling methods
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

template<class PAYLOADCLASS_8L>
class fC_8_condList : public fCbase
{
typedef fC_8_baseConditional<
       fC_8_condList<PAYLOADCLASS_8L>, PAYLOADCLASS_8L>
        cond8List_t;
typedef  typename vector< cond8List_t *>::iterator iTcondList;
typedef  typename vector< cond8List_t *>::const_iterator iTconstCondList;
// resolves to a ptr to a fC_8_baseConditional<fC_A_condList<..>..>
        
public: // data
    vector< cond8List_t *>  listOfConditionalPtrs;
	// requiredDirect handles an infrequent situation where an FMA specifier is conditional, but
	// the FM8 specifier is not.  In these cases, the FMA text output must match the FM8 output,
	// so the base class out() method goes right to the payload::out() method.
	// see FMx_Conditional::out(), above.
// should be false in FM8
	bool            requiredDirect;


	// forceDirect handles the couple of V8 attributes that need to come out as conditional
	// but are encoded as non-conditional.  This makes the top level conditional infer it is
	// a ct_Direct type (without decoding anything) then go directly to the payload.
// should be false in FM8
	bool			forceDirect;
	
	int             actualTag;	// the raw tag value parsed from the file

	// copy/paste from FMx_Conditional_List: this class will be refactored soon so we will 
	// do the expedient thing for now [4/25/2014 timj]
	// coalescePayload is for bringing multiple Direct payload lists together into a single
	// output list.  This is done to match the legacy parser and make the outputs match
// should be false in FM8
	bool			coalescePayload;

public: /* housekeeping */
    fC_8_condList():forceDirect(false), requiredDirect(false), coalescePayload(false)  {};
    fC_8_condList(const fC_8_condList& s) { operator=(s); };
    virtual ~fC_8_condList()           { destroy();    };
	fC_8_condList& operator=(const fC_8_condList& s) {
		for (iTconstCondList iT =s.listOfConditionalPtrs.begin();	//  [3/13/2014 timj]
			iT != s.listOfConditionalPtrs.end(); ++iT)
        {// ptr2aPtr2acond8List_t   
			cond8List_t *pLst = new cond8List_t(*((cond8List_t *)(*iT))); //deep copy
			listOfConditionalPtrs.push_back(pLst);
        };
//		forceDirect    = s.forceDirect;
//		actualTag      = s.actualTag;
//		requiredDirect = s.requiredDirect;
        return *this;
    };
    int destroy()
    { 
		for (iTcondList iT = listOfConditionalPtrs.begin(); 
			            iT != listOfConditionalPtrs.end(); ++iT)
        {// ptr2aPtr2acond8List_t 
			cond8List_t * pAL = *iT;
			pAL->destroy();
			delete pAL;
			(*iT) = NULL;
		}  
		listOfConditionalPtrs.clear();
        return 0;
    };

public: /* methods */
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void hydratePrototype(aCondDestType* p8condDest /*aCgenericConditional* cond*/);

	virtual aCondDestType *makeaCclass() 
	{ 
		return (aCondDestType*) new aCcondList();
	};

	virtual int out(void)
	{	
		// tok 8.2 vs 8.3 V&V [12/15/2015 tjohnston]
		// this output was useful to us while we were chasing conditional
		// list differences, but it does not reveal any substantive difference in the
		// content or shape of a conditional list.  permanently delete this block
		// if it not useful a year or more after this date.
		// see also fC_A_condList::out()
		 if (requiredDirect == false)
		{
			if (reconcile==false)
			{
				// remove this because it reveals a difference only in the requiredDirect
				// variable.  Any difference in the actual structure of the list will be revealed
				// in the rest of the out() output.
				//LOGIT(COUT_LOG,"    Conditional List:\n");
			}
		}

		vector< cond8List_t *>::const_iterator iT;
		int Itm = 0;
		for (/*iTcondList*/ iT = listOfConditionalPtrs.begin();
			                iT != listOfConditionalPtrs.end(); ++iT)
        {   
			cond8List_t *pLst = (cond8List_t *)(*iT); 
//			pLst->requiredDirect = requiredDirect;
			if ( Itm != 0)
			{
				//pLst->collapseItemsOnOut = collapseItemsOnOut;
			}
			LOGIT(COUT_LOG,"    Conditional %3d: ",++Itm);
			pLst->out();
        };
		return SUCCESS;
	};

	bool isConditional(void)
	{	
		vector< cond8List_t *>::const_iterator iT;
		for (/*iTcondList*/ iT = listOfConditionalPtrs.begin();
			                iT != listOfConditionalPtrs.end(); ++iT)
        {   
			cond8List_t *pLst = (cond8List_t *)(*iT); 
			if (pLst->axiomType != aT_Direct)
				return true;
        };

		return false;
	};

};// end of fC_8_condList





/*===========================================================================================*
  fC_8_condList  implementation
  ===========================================================================================*/
template<class PAYLOADCLASS_8L>
int fC_8_condList<PAYLOADCLASS_8L>::
                 evaluateAttribute(UCHAR** pData, int& length, unsigned int& offset)
{
	RETURNCODE ret    = 0;	
//	RETURNCODE status = 0;	
	exitValues_t holdExit;
	unsigned   tagValue = 0;
	unsigned   tagLength;
	unsigned   entryLen;
	int      attrLen, exitLength, workingLength;
	unsigned exitOffset;
//	UCHAR*   exitPdata;
//	unsigned entryOffset = offset;// for debugging only

	pushExitValues(holdExit, pData, length, offset);

	// explict tag must == ATTR_TAG	
	ret = parseTag(pData, length,  tagValue, tagLength, offset);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: Conditional List Attribute Type is wrong. (%u)\n",tagValue);
		popExitValues(holdExit, pData, length, offset);
		return -1;
	}
	actualTag = tagValue;
	attrLen   = tagLength;

	
	// seq of cond8List_t till no more length
	cond8List_t *pThisCond; // the working conditional
	while ( attrLen > 0 )
	{//	each cond8List_t
		entryLen = attrLen; // used to calculate length of length
		// explicit tag 
		if (forceDirect)
		{
			tagValue = cdt_DIRECT;
			tagLength= length;
		}
		else	// normal path
		{// explicit tag 
			ret = parseTag(pData, attrLen,  tagValue,  tagLength, offset );
		}

		if (tagValue > cdt_DIRECT )
		{// then illegal
			LOGIT(CERR_LOG,"Error: Conditional List Type is wrong.... (%u)\n",tagValue);
			popExitValues(holdExit, pData, length, offset);
			return -1;
		}
		if (ret != SUCCESS ||  tagLength > (unsigned)attrLen )
		{
			LOGIT(CERR_LOG,"Error: Conditional List Item Length is bigger than available.\n");
			popExitValues(holdExit, pData, length, offset);
			return -2;
		}
		// what things should be when we get out of eval
		entryLen   = entryLen - attrLen;	
		exitLength = attrLen  - tagLength;
		exitOffset = offset   + tagLength;
		workingLength = tagLength;// so we can get an int reference

		pThisCond = new cond8List_t; 
		pThisCond->axiomType = (axiomType_t) ConvertToDDL::ClauseType((condType_t)tagValue);

		if (QANDA)
		{
			// during QandA, mimic fm8 coalesce logic for conditional lists:  
			// adjacent DIRECT lists are coalesced until an IF or SELECT item
			// is encountered.  the list is not coalesced after that point
			//  [5/7/2015 timj]
			if (pThisCond->axiomType != aT_Direct)
				coalescePayload = false;
		}

		ret = pThisCond->evaluateAttribute(pData, workingLength, offset);
		listOfConditionalPtrs.push_back(pThisCond);
		pThisCond = NULL;

		if ( ret == SUCCESS && workingLength == 0 && exitOffset == offset )
		{
			attrLen = exitLength;// we assume that pData is correct if the others are
		}
		else
		{
			LOGIT(CERR_LOG,"Error: Conditional List Item failed to parse.\n");
			popExitValues(holdExit, pData, length, offset);
			return ret;
		}

		length -= (tagLength + entryLen); // works better
	}// wend - next List Item

	return ret;
};// end of fC_A_condList::evaluateAttribute



template<class PAYLOADCLASS_8L>
void fC_8_condList<PAYLOADCLASS_8L>::hydratePrototype(aCondDestType* p8condDest)
{
	aCcondList* pCondList = (aCcondList*) p8condDest;

	// make sure we are empty
	pCondList->clear();
	bool lastWasDirect = false;
	aCgenericConditional* ddlCond = NULL;
	aCconditional* pCcond =  NULL;

	// mimic fm8 coalesce logic for conditional lists:  
	// do not coalesce if any there are any IF or SELECT items on the list
	//  [5/7/2015 timj]
	// do this always, not just V&V  //  [7/21/2015 timj]
	coalescePayload = true;
	for (iTcondList liIT =listOfConditionalPtrs.begin(); liIT != listOfConditionalPtrs.end(); ++liIT)
	{
		if ((*liIT)->axiomType != aT_Direct)
			coalescePayload = false;
	}

	// list of fC_A_baseConditional<fC_A_condList<PAYLOADCLASS_AL>, PAYLOADCLASS_AL>	
	for (iTcondList liIT =listOfConditionalPtrs.begin(); liIT != listOfConditionalPtrs.end(); ++liIT)
	{
		cond8List_t* pLstItm = (*liIT);

		bool coalesce = coalescePayload && lastWasDirect;

		if (coalesce && pLstItm->axiomType == aT_Direct)
		{// it's a list of generics
			ddlCond = &(pCondList->genericlist.back());
			// we just pushed this, so pop it and (since a deep copy was made to push it)
			// make a new Conditional with the generic information (ie add a isList to a copy of the generic)
			pCcond = new aCconditional(ddlCond);
			// since the copy constructor takes the payload pointer memory
			// the listed generic is no longer of use
			pCondList->genericlist.pop_back();// removes it (i assume it deletes it since it doesn't return)
			pLstItm->hydratePrototype((aCondDestType*)pCcond, coalescePayload);
			// now we need to put the last one back with more stuff in it.
			pCondList->genericlist.push_back(*((aCcondList::oneGeneric_t*)pCcond));
			// we did a deep copy to push it so we'll delete it later when we're done with it
		}
		else
		{
			pCcond = new aCconditional;
			pCcond->priExprType = (expressionType_t)(pLstItm->axiomType);
			// we don't need to dynamic cast- we just new'd this and know it's type	
			pLstItm->hydratePrototype(pCcond, false);		
			// a aCcondtional isa aCgenericConditional, push it as such
			pCondList->genericlist.push_back(*((aCcondList::oneGeneric_t*)pCcond));
			// we did a deep copy to push it so we'll delete it later when we're done with it
		}
		lastWasDirect = (pCcond->priExprType == eT_Direct) ? true : false;
		delete pCcond;// push makes a deep copy so we'll try and save some memory
	}

};

#endif // _FM8_FMx_CONDITIONAL_8_H
