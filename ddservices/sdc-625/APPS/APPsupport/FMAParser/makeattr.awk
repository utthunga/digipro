# usage:  gawk   -f makeattr.awk   XXXX=axisScal   xxxx.cpp
# note: this file must have unix line endings
# note: axisScal above is an example


{
    gsub(/xxxx/, XXXX, $0)
}

/ FILE / { 
    fname = $3 
    print $0
    print "\r" > fname
    $0 = ""
    }

{
    if (fname != "")
    {
        print $0  "\r" >> fname
    }
}

END {
    close(fname)
}