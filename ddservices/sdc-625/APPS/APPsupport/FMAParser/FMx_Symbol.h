/**********************************************************************************************
 *
 * $Workfile: FMx_Symbol.h $
 * 
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_Symbol.h : FMA class description file
 *
 * #include "FMx_Symbol.h"
 *
 */

#ifndef _FMX_SYMBOL_
#define _FMX_SYMBOL_

#ifdef INC_DEBUG
#pragma message("In FMx_Symbol.h") 
#endif

#include <vector>
#include <string>
#include <map>

using namespace std;
#pragma warning (disable : 4786) 


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMx_Symbol.h") 
#endif


class FMx_Table;   // in lieu of including the whole file

// 10jan13 - this is now the baseclass for the dictionary and the literal string dictionary

class FMx_Symbol
{	
protected:
	FMx_Table* pTable;  // can be a 8 or A table, manipulation of the table MUST be handled
						// in the A or 8 code.
public: //data
	bool saveBinary;

public:
	FMx_Symbol();
	virtual ~FMx_Symbol();
	
public: // implementation
	virtual void retrieveStr(unsigned str, string& retVal, int& itmType);
	virtual void retrieveStrByIndex(unsigned index, string& retVal, int& itmType);
	virtual void setTheTable( FMx_Table* pTbl) = 0;
};

#endif //_FMX_SYMBOL_