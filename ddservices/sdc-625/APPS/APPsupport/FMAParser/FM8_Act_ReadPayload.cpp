/**********************************************************************************************
 *
 * $Workfile: FM8_Act_ReadPayload.cpp $
 * 17may12 - stevev - created from FMA version
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Act_ReadPayload Visitor Operation on all object classes
 *		
 *
 */

#include "FM8_defs.h"
#include "FM8_Activities.h"
#include "FM8_Object.h"
#include "logging.h"

// // required function
//	> x41 disregard, x81 won't be seen
//	> x43 disregard, x83 won't be seen
//	> x42 disregard for now, x82 -- lots of work to do
int FM8_ReadPayloadVisitor::Operate( FMx_Base* pVisited )
{
	if ( pVisited == NULL )
		return FAILURE;

	unsigned ct = pVisited->classType;
	if ( (ct & (FM8_type|FMA_type)) == FMA_type )
	{// we are going to disregard all FMA for now
		return SUCCESS;
	}
	if ( (ct & 0x0f) == __Meta )
	{
		LOGIT(CERR_LOG,"ERROR: FM8_ReadPayloadVisitor should never see classtype %#02x\n",ct);
		return FAILURE;
	}

	// we have to have class type FM8_Object
	FM8_Object *obj = ((FM8_Object*)pVisited);

	// for now, use the object's read
	return (obj->read_ObjectPayload());

};



