/**********************************************************************************************
 *
 * $Workfile: FMx_Attributes_Ref&Expr.cpp $
 * 23mar12 - steveh
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_Attribute Expression and Reference class implementations
 */

#include "FMx_general.h"
#include "FMx_AttributeClass.h"
#include "FMx_Attributes_Ref&Expr.h"
#include "FMx_Support_primatives.h"
#include "Convert.h"
#include "AttributeString.h"
#include "tags_sa.h"
#include "v_N_v+Debug.h"


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

void expressionElement::destroy(void)
{ 
	if (eepRef)
	{
		eepRef->destroy();   
		delete eepRef; // delete will destroy
		eepRef = NULL;
	}   
	if (eepString)
	{
		delete eepString; 
		eepString = NULL;
	}   
}

int expressionElement::out(void)
{
	char buf[ELEM_WIDTH + 1]; memset(buf, 0, ELEM_WIDTH + 1);
	int len, m, n = 0;
	
	exprElemType_t ee_type = ConvertToA::ExpressionElementType(exprElemType);

	m = len = strlen(exprType_Str[ee_type]);

	while(m < ELEM_WIDTH)
	{
		buf[n++] = ' ';
		m++;
	}// wend

	m = 0;
	strncpy(&(buf[n]),exprType_Str[ee_type],(ELEM_WIDTH-n));
	buf[ELEM_WIDTH] = '\0';
	LOGIT(COUT_LOG,"        %s",buf);

	if (ee_type == eet_conditional)
	{
		LOGIT(COUT_LOG,": a tertiary operator, uses 3 stack elements",buf);
	}
	else if(ee_type == eet_integer_constant)
	{
		if ( eeIsUnsigned )
		{
			LOGIT(COUT_LOG,": %I64u { unsigned for signed constant}\n",eeInt);
		}
		else
		{
			LOGIT(COUT_LOG,": %I64d\n",(__int64)eeInt);
		}
	}
	else if(ee_type == eet_unsigned_constant)
	{
		if ( eeIsUnsigned )
		{
			LOGIT(COUT_LOG,": %I64u\n",eeInt);
		}
		else
		{
			LOGIT(COUT_LOG,": %I64d { signed for unsigned constant}\n",(__int64)eeInt);
		}
	}
	else if(ee_type == eet_floating_constant)
	{
		// in reconciling FM8 vs FMA, floating constants in FMA expressions are 8-byte precision
		// while in FM8 they are 4-bytes.  Internally, both are stored as doubles for consistency.
		// To guarantee a match during reconcile mode, "downcast" the double to a float to get
		// a precision match.
		double x = (reconcile) ? ((double) ((float) eeFloat)) : eeFloat;// FLOATCHANGE no change required [1/20/2016 tjohnston]
		LOGIT(COUT_LOG,": %12.8f\n", x);
	}
	else if(ee_type == eet_string_constant)
	{
		if ( eepString == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No string exists.\n");
			return -6;
		}
		else
		{
			eepString->out();
		}
		
	}
	else if(ee_type == eet_numeric_value)
	{//29 8] Reference,-- Must resolve to a numeric value
		LOGIT(COUT_LOG,": Numeric Reference>>\n");
		if ( eepRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -2;
		}
		else
		{
			eepRef->out();
		}
	}
	else if(ee_type == eet_all_value)
	{//30 29] Reference	-- Must resolve to a Unicode String	
		LOGIT(COUT_LOG,": A value Reference>>\n");
		if ( eepRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -3;
		}
		else
		{
			eepRef->out();
		}
	}
	else if(ee_type == eet_attr_value)
	{//31 0]                attr-name	byte; item-ref Reference
		LOGIT(COUT_LOG,": Attribute Reference>>\n");
//		LOGIT(COUT_LOG,"        %s in the following reference>>\n", attr8Type_Str[eeAttrType >>8][eeAttrType & 0xff]);   // TODO: 'A' header used in '8' class
		// was  attrType_Str[eeAttrType]);
		if ( eepRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -4;
		}
		else
		{
			eepRef->out();
		}
	}
	else if(ee_type == eet_min_max_value)
	{//32 1] which	INTEGER; attr-name	byte; item-ref Reference
		LOGIT(COUT_LOG,": MIN/MAX Reference>>\n");
//		LOGIT(COUT_LOG,"        %s[%I64d] in the following reference>>\n", attr8Type_Str[eeAttrType >>8][eeAttrType &0xff],eeInt);  // TODO: 'A' header used in '8' class
		//was				attrType_Str[eeAttrType],eeInt);
		if ( eepRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -5;
		}
		else
		{
			eepRef->out();
		}
	}
	else if(ee_type == eet_method_value)
	{//33 2] Reference	-- to a method
		LOGIT(COUT_LOG,": Method Reference>>");
		LOGIT(COUT_LOG,"  UNSUPPORTED in HART\n");
		if ( eepRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -6;
		}
		else
		{
			eepRef->out();
		}
	}
	else if (ee_type == eet_Undefined || 
			 ee_type == eet_current_role || 
			 ee_type == eet_array_index)
	{
		LOGIT(COUT_LOG,": Unsupported by HART. Terminating expression.\n",buf);
		return -1;
	}
	else
	{
		LOGIT(COUT_LOG,"\n");
	}

	return 0;
}

void expressionElement::hydratePrototype(aCexpressElement* pexpressElmnt)
{
	pexpressElmnt->clear();

	pexpressElmnt->exprElemType = exprElemType;
	pexpressElmnt->dependency.which = 0;
	pexpressElmnt->dependency.useOptionVal = aOp_dontCare;
	pexpressElmnt->dependency.depRef.isRef = 0;
	pexpressElmnt->expressionData.sStringVal.erase();
	pexpressElmnt->expressionData.vIsUnsigned = eeIsUnsigned;
	pexpressElmnt->expressionData.vSize       = 4; 
	pexpressElmnt->expressionData.vIsValid    = true;

	switch (pexpressElmnt->exprElemType)
	{
		case	NOT_OPCODE:				/* 1 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_NOT;
				break;

		case	NEG_OPCODE:				/* 2 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_NEG;
				break;

		case	BNEG_OPCODE:			 /* 3 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_BNEG;
				break;

		case	ADD_OPCODE:				/* 4 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_ADD;
				break;

		case	SUB_OPCODE:				 /* 5 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_SUB;
				break;

		case	MUL_OPCODE:				/* 6 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_MUL;
				break;

		case	DIV_OPCODE:				/* 7 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_DIV;
				break;

		case	MOD_OPCODE:				/* 8 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_MOD;
				break;

		case	LSHIFT_OPCODE:			/* 9 */ 
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_LSHIFT;
				break;

		case	RSHIFT_OPCODE:			/* 10 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_RSHIFT;
				break;

		case	AND_OPCODE:				/* 11 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_AND;
				break;

		case	OR_OPCODE:				/* 12 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_OR;
				break;

		case	XOR_OPCODE:				/* 13 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_XOR;
				break;

		case	LAND_OPCODE:			/* 14 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_LAND;
				break;

		case	LOR_OPCODE:				/* 15 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_LOR;
				break;

		case	LT_OPCODE:				/* 16 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_LT;
				break;

		case	GT_OPCODE:				/* 17 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_GT;
				break;

		case	LE_OPCODE:				/* 18 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_LE;
				break;

		case	GE_OPCODE:				/* 19 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_GE;
				break;

		case	EQ_OPCODE:				/* 20 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_EQ;
				break;

		case	NEQ_OPCODE:				/* 21 */
				pexpressElmnt->expressionData.vType = CValueVarient::isOpcode;
				pexpressElmnt->expressionData.vValue.iOpCode = eet_NEQ;
				break;

		case	INTCST_OPCODE:			/* 22 */
				pexpressElmnt->expressionData = eeInt;
				break;

		case	FPCST_OPCODE:			/* 23 */
				pexpressElmnt->expressionData = eeFloat;
				break;

		case	VARID_OPCODE:			/* 24 */
				pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt->expressionData.vValue.depIndex = 0;
				pexpressElmnt->dependency.depRef.rtype = 0;
				//pexpressElmnt->dependency.depRef.id =	element.elem.varId;
				pexpressElmnt->dependency.depRef.type = 1;
				break;

		case	MAXVAL_OPCODE:			/* 25 */
				pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt->expressionData.vValue.depIndex = 0;
				pexpressElmnt->dependency.useOptionVal = aOp_wantMax;
				pexpressElmnt->dependency.which = eeInt;
				pexpressElmnt->dependency.depRef.rtype = 0;
				//pexpressElmnt->dependency.depRef.id =	element.elem.minMax->variable.id;
				pexpressElmnt->dependency.depRef.type = 1;
				break;

		case	MINVAL_OPCODE:			/* 26 */
				pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
				pexpressElmnt->expressionData.vValue.depIndex = 0;
				pexpressElmnt->dependency.useOptionVal = aOp_wantMin;
				pexpressElmnt->dependency.which = eeInt;
				pexpressElmnt->dependency.depRef.rtype = 0;
				//pexpressElmnt->dependency.depRef.id =	element.elem.minMax->variable.id;
				pexpressElmnt->dependency.depRef.type = 1;
				break;

		case	VARREF_OPCODE:			/* 27 */
				{
					pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt->expressionData.vValue.depIndex = 0;
					aCreference *tmpaCref = &(pexpressElmnt->dependency.depRef);
					if ( eepRef == NULL )
					{
						LOGIT(CERR_LOG,"VARREF expression opcode has no reference.\n");
					}
					else
					{
						eepRef->hydratePrototype(tmpaCref);

						// added eet_BLOCK to this test to get dump output to align with fm8 [4/22/2014 timj]
						if ( (rawElemType == eet_VARID || rawElemType == eet_BLOCK) && exprElemType == eet_VARREF )
						{// a covertion has been done, modify reference to match legacy tokenizer
							if (pexpressElmnt->dependency.depRef.pExpr == NULL && 
								pexpressElmnt->dependency.depRef.pRef == NULL)
							{
								pexpressElmnt->exprElemType = eet_VARID;
								tmpaCref->rtype = rT_Item_id;
							}
						}//eepRef->hydratePrototype(tmpaCref);
					}
					//BYTE makeTmpNull = 0;

					//for (ddpREFERENCE :: iterator p = element.elem.varRef->begin();
					//                              p != element.elem.varRef->end();p++)
					//{
					//	if (makeTmpNull)
					//	{
					//		pexpressElmnt->dependency.depRef.isRef = TRUE;
					//		tmpaCref->pRef = new aCreference;
					//		tmpaCref = tmpaCref->pRef;
					//	}
					//	
					//	eepRef->hydratePrototype(tmpaCref)

					//	makeTmpNull = 1;
					//}	
				}
				break;

		case	MAXREF_OPCODE:		/* 28 */
				{
					pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt->expressionData.vValue.depIndex = 0;
					pexpressElmnt->dependency.which = eeInt;
					pexpressElmnt->dependency.useOptionVal = aOp_wantMax;

					aCreference *tmpaCref = &pexpressElmnt->dependency.depRef;
					//BYTE makeTmpNull = 0;

					//for (ddpREFERENCE :: iterator p = element.elem.minMax->variable.ref->begin(); p != element.elem.minMax->variable.ref->end();p++)
					//{
					//	if (makeTmpNull)
					//	{
					//		pexpressElmnt->dependency.depRef.isRef = TRUE;
					//		tmpaCref->pRef = new aCreference;
					//		tmpaCref = tmpaCref->pRef;
					//	}

					//	eepRef->hydratePrototype(tmpaCref)

					//	makeTmpNull = 1;
					//}	
				}
				break;

		case	MINREF_OPCODE:			/* 29 */
				{
					pexpressElmnt->expressionData.vType = CValueVarient::isDepIndex;
					pexpressElmnt->expressionData.vValue.depIndex = 0;
					pexpressElmnt->dependency.which = eeInt;
					pexpressElmnt->dependency.useOptionVal = aOp_wantMin;

					aCreference *tmpaCref = &pexpressElmnt->dependency.depRef;
					//BYTE makeTmpNull = 0;

					//for (ddpREFERENCE :: iterator p = element.elem.minMax->variable.ref->begin(); p != element.elem.minMax->variable.ref->end();p++)
					//{
					//	if (makeTmpNull)
					//	{
					//		pexpressElmnt->dependency.depRef.isRef = TRUE;
					//		tmpaCref->pRef = new aCreference;
					//		tmpaCref = tmpaCref->pRef;
					//	}

					//	eepRef->hydratePrototype(tmpaCref)

					//	makeTmpNull = 1;
					//}	
				}
				break;

		case	BLOCK_OPCODE:			/* 30 */
		case	BLOCKID_OPCODE:			/* 31 */
		case	BLOCKREF_OPCODE:		/* 32 */
				break;

		case	STRCST_OPCODE:			/* 33 */	
				{
					aCddlString s;
					eepString->hydratePrototype(&s);
					pexpressElmnt->expressionData = s.theStr;
#ifdef TOKENIZER
					/* the tokenizer needs to record more information*/

					switch ( s.strTag )
					{
					case ds_DevSpec:	// device specific - ie Literal
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Lit;
							pexpressElmnt->dependency.which         = s.enumVal;
							pexpressElmnt->dependency.depRef.clear();
						}
						break;
					case ds_Var:
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Ascii;
							pexpressElmnt->dependency.which         = 0;
							pexpressElmnt->dependency.depRef.rtype  = 1; // itemID
							pexpressElmnt->dependency.depRef.isRef  = 0; // just an id
							pexpressElmnt->dependency.depRef.id     = s.ddItem;
						}
						break;
					case ds_Enum:   
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Enum;
							pexpressElmnt->dependency.which         = s.enumVal;
							if (s.pRef != NULL)
							{// this may need to fill the id like Var above
								pexpressElmnt->dependency.depRef    = *(s.pRef);
							}
						}
						break;
					case ds_Dict:   
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Dict;
							pexpressElmnt->dependency.which         = s.enumVal;
							pexpressElmnt->dependency.depRef.clear();
						}
						break;
					case ds_VarRef: 
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Ascii;
							pexpressElmnt->dependency.which         = 0;
							if (s.pRef != NULL)
							{
								pexpressElmnt->dependency.depRef    = *(s.pRef);
								if ( s.pRef->rtype == rT_via_Attr )
								{
									pexpressElmnt->dependency.useOptionVal = aOp_Attr;
								}
							}
							else
							{
								LOGIT(CERR_LOG,"Internal ERROR: a Var ref did not have a reference.\n");
							}
						}
						break;
					case ds_EnumRef:
						{
							pexpressElmnt->dependency.useOptionVal  = aOp_Enum;
							pexpressElmnt->dependency.which         = s.enumVal;
							if (s.pRef != NULL)
							{
								pexpressElmnt->dependency.depRef    = *(s.pRef);
							}
							else
							{
								LOGIT(CERR_LOG,"Internal ERROR: an Enum ref did not have a reference.\n");
							}
						}
						break;
					case ds_MaxValid:
					default:
						{
							LOGIT(CERR_LOG,"Internal ERROR: ExprElement with unknown type.\n");
						}
						break;
					}// endswitch
#endif
				}
				break;

		case	SYSTEMENUM_OPCODE:		/* 34 */
				break;

		default:
				break;

	}
}



expressionElement& expressionElement::operator=(const expressionElement& s)
{
	FMx_Attribute::operator=(*((FMx_Attribute *)(&s)));
	rawElemType = s.rawElemType;	
	exprElemType = s.exprElemType;
	eeInt       = s.eeInt;			
	eeIsUnsigned = s.eeIsUnsigned;  
	eeFloat = s.eeFloat;
	if (s.eepString) eepString = new faCString(*(s.eepString)); else eepString = NULL;
	if (s.eepRef)    eepRef    = new abCReference(*(s.eepRef)); else eepRef = NULL;
	return *this;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

void abCExpression::destroy(void)
{
	expressionElement* peE;
	eepIt_t pIT;

	for (pIT = exprList.begin(); pIT != exprList.end(); ++pIT)
	{
		peE = *pIT;
		peE->destroy();
		delete peE;
	}
	exprList.clear();
}

int abCExpression::out(void)
{
	int y = 0;	
	LOGIT(COUT_LOG,"Expression with %d entries:\n",exprList.size());
	expressionElement* p1Elem;
	eepIt_t elstIT;

	for (elstIT = exprList.begin(); elstIT != exprList.end(); ++elstIT, y++)
	{
		p1Elem = *elstIT;
		assert(p1Elem);

		if (p1Elem->out())// false on no eror
		{
			LOGIT(COUT_LOG,"        Expression element %d reported an error.\n", y);
			break; // out of for loop
		}
	}

	return 0;
}

void abCExpression::hydratePrototype(aCexpression* exp)
{
	exp->clear();

	for (eepIt_t it = exprList.begin(); it != exprList.end(); ++it)
	{
		expressionElement* elem = *it;

		aCexpressElement* aCelem = new aCexpressElement;
		elem->hydratePrototype(aCelem);

		exp->expressionL.push_back(*aCelem);
	}
}
abCExpression& abCExpression::operator=(const abCExpression& src)
{
	FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
	eepLst_t::const_iterator iT;// ptr2ptr2expressionElement
	expressionElement* peE;
	for ( iT = src.exprList.begin(); iT != src.exprList.end(); ++iT)
	{	// deep copy it's a ptr2aptr2a expressionElement
		peE = new expressionElement( *(*iT) );
		exprList.push_back( peE );
	}
	return *this;
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

void abCReference::destroy(void)
{
	if (pExpr != NULL)
	{
		pExpr->destroy();
		delete pExpr;
		pExpr = NULL;
	}

	if (pRef != NULL)
	{
		pRef->destroy();
		delete pRef;
		pRef = NULL;
	}

	if (argumentList.size() > 0)
	{
		LOGIT(CERR_LOG,"ERROR: reference arguement list has contents.\n");// not in HART
		argumentList.clear();
	}
}

int abCReference::out(void)
{
	if (reconcile && attrType == ag_List_type)
	{	
		FMx_Attribute::out();
		// in this one corner case, 8 uses a ref while A uses a refSpec, just make 'em match up [10/29/2013 tim]
		LOGIT(COUT_LOG,"                  Non-Conditional: (Direct) attribute.\n");
	}
	if (requiredDirect)
	{
		// in this case, an FM8 specifer is not conditional, while it companion  FMA specifiers is
		// so we must put out the attribute header info
		int ret = FMx_Attribute::out();
		if (ret != SUCCESS)
		{
			return ret;
		}
	}

	LOGIT(COUT_LOG,"Reference Type: %s\n", refTypeName(refType));

	// filter non-HART
	if (!reconcile)
	{
		// timj 16jul2013 - this block of code can give false reports for an FM8 file because
		//		refType and rawRefType are the same.  example: a method type (15) matches an 
		//		FMA response_codes_id (15) tripping the error message.  turn this off for
		//		reconcile so output matches
		if ( refType ==	rT_via_Record || refType ==	rT_via_VarList || refType == rT_via_Block
			/*refType == rT_via_Xblock_member || 
			refType ==	rT_param            || refType ==	rT_param_list         ||
			refType ==	rT_block_char        || refType ==	rT_local_param        ||
			refType ==	rT_ref_with_arguments|| refType ==	rT_invalid            */  )
		{
			LOGIT(COUT_LOG,"        Reference type %d not supported by HART.\n", refType);
			return 0;
		}

		// use raw ref type here where there is no mapping to FM8
		if ( /*refType ==	rT_block_b_id             || refType ==	rT_block_a_id            ||  */
			refType == rT_Record_id              || /*refType ==	rT_var_list_id           || */
			refType ==	rT_ResponseCode_id      /*refType ==	rT_undefined26           || */
			/*refType ==	rT_component_id           || refType == rT_component_folder_id	 ||
			refType ==	rT_component_reference_id || refType ==	rT_component_relation_id ||
			refType ==	rT_interface_id           || refType ==  rT_constant_ref */          )
		{
			LOGIT(COUT_LOG,"        Reference type %d not supported by HART.\n", rawRefType);
			return 0;
		}
		// on success we need to output the rest of the information
	}


	// VIA - expression + reference
	int via[] = { rT_viaItemArray,
		          rT_via_Collect,
				//stevev     rT_via_Record,// not HART
				  rT_via_Array,
				//stevev     rT_via_VarList,// not HART
				//stevev     rT_via_Param,// not HART
				//stevev	 rT_via_ParamList,// not HART
				//stevev	 rT_via_Block,// not HART
				  rT_via_File,
				  rT_via_List,
				  rT_via_BitEnum,				  
				//stevev  rT_Grid_id,
				//stevev  rT_Row_Break,				  
				//stevev  rT_via_Attr,				  
				//stevev  rT_Blob_id,
				  
				  rT_via_Blob, -1};
	
	for (int i=0; via[i] >= 0; i++)
	{
		if ( refType ==	via[i] )
		{
			LOGIT(COUT_LOG,"        Expression portion:\n");
			
			if ( pExpr == NULL )
			{
				/* A version these are encoded as int constants 
				*/
				if ( refType ==	rT_via_Collect || refType ==	rT_via_File )
				{// these are coded as int constants
					int y = (int)intValue;
					LOGIT(COUT_LOG,"    Symbol # 0x%04x.\n",y);
					// fall thru to do the reference
				}
				else
				if ( refType ==  rT_via_BitEnum )
				{// these are coded as int constants
					int y = (int)intValue;
					LOGIT(COUT_LOG,"    mask value # 0x%04x.\n",y);
					// fall thru to do the reference
				}
				else
				{
				LOGIT(COUT_LOG,"       }}}Error: No expression exists.\n");
				return -2;
			}
			}
			else
			{
				pExpr->out();
			}

			LOGIT(COUT_LOG,"         Reference portion:\n");
			
			if ( pRef == NULL )
			{
				LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
				return -3;
			}
			else
			{
				return pRef->out();
			}
		}
		// this only allows the first array entry to be checked???wtf???return -4;
	}

	if (refType ==	rT_via_Attr)
	{
		LOGIT(COUT_LOG,"        Attribute '%s' of:\n", AttributeString::get8((int)(intValue >> 8),
														(int)(intValue & 0xff)));
		
		LOGIT(COUT_LOG,"        Reference portion:\n");
		
		if ( pRef == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No reference exists.\n");
			return -4;
		}
		else
		{
			return pRef->out();
		}		
	}

	// VIA - not done yet

	if ( refType ==	rT_via_Chart || refType ==	rT_via_Graph || refType ==  rT_via_Source )
	{
		LOGIT(COUT_LOG,"        Reference not yet supported.\n");
		return 0;	 // don't stop the tree
	}

	if ( refType ==	rT_Row_Break || refType ==	rT_Separator )
	{// nothing else is required
		return 0;
	}

	if ( refType ==	rT_Constant )
	{// nothing else is required
		
		LOGIT(COUT_LOG,"        Const Expression :\n");
		
		if ( pExpr == NULL )
		{
			LOGIT(COUT_LOG,"       }}}Error: No expression exists.\n");
			return -2;
		}
		else
		{
			return pExpr->out();
		}
	}
	// IDs

	int ids[] = {
				rT_Item_id,
				rT_ItemArray_id,
				rT_Collect_id,
				rT_viaItemArray,
				rT_via_Collect,
				//stevev  rT_via_Record,
				rT_via_Array,
				//stevev  rT_via_VarList,
				//stevev  rT_via_Param,
				//stevev  rT_via_ParamList,
				//stevev  rT_via_Block,
				//stevev  rT_Block_id,
				rT_Variable_id,
				rT_Menu_id,
				rT_Edit_Display_id,
				rT_Method_id,
				rT_Refresh_id,
				rT_Unit_id,
				rT_WAO_id,
				//stevev  rT_Record_id,
				rT_Array_id,
				//stevev  rT_VarList_id,
				//stevev  rT_Program_id,
				//stevev  rT_Domain_id,
				//stevev  rT_ResponseCode_id,
				rT_File_id,
				rT_Chart_id,
				rT_Graph_id,
				rT_Axis_id,
				rT_Waveform_id,
				rT_Source_id,
				rT_List_id,
				rT_Image_id, 

				rT_Grid_id,//added stevev
				rT_Blob_id,//added stevev
		-1};

	bool inhibit = false;
	int inhibited[] = {rT_Chart_id,    rT_Graph_id, rT_Source_id, 
		               rT_Waveform_id, rT_Axis_id,  rT_List_id, rT_Image_id, -1};
	for (int i=0; inhibited[i] >= 0; i++)
	{
		if (refType == inhibited[i])
			inhibit = true;
	}
	
	for (int i=0; ids[i] >= 0; i++)
	{
		if (refType == ids[i])//added stevev
		{
			if (false == (reconcile  &&  inhibit))
			{
				int y = (int)intValue;
				LOGIT(COUT_LOG,"        References Symbol # 0x%04x.\n",y);
			}
			return 0; //added stevev
		}
	}

	// catch-all
	LOGIT(COUT_LOG,"        Reference type %d not supported by HART.\n", refType);

	return 0;
}

void abCReference::hydratePrototype(aCreference* aCref)
{
	aCref->clear();
	aCref->rtype = refType;

	switch(refType)
	{
		case	ITEM_ID_REF:
				aCref->type = VARIABLE_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case 	ITEM_ARRAY_ID_REF:
				aCref->type = ITEM_ARRAY_ITYPE; // stevev 24feb06 changed from:: VARIABLE_ITYPE;
				aCref->isRef = 1;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	COLLECTION_ID_REF:
				aCref->type = COLLECTION_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	BLOCK_ID_REF:
				aCref->type = BLOCK_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	VARIABLE_ID_REF:
				aCref->type = VARIABLE_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	MENU_ID_REF:
				aCref->type = MENU_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	EDIT_DISP_ID_REF:
				aCref->type = EDIT_DISP_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	METHOD_ID_REF:
				aCref->type = METHOD_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	REFRESH_ID_REF:
				aCref->type = REFRESH_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	UNIT_ID_REF:
				aCref->type = UNIT_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	WAO_ID_REF:
				aCref->type = WAO_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	RECORD_ID_REF:
				aCref->type = RECORD_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	ARRAY_ID_REF:
				aCref->type = ARRAY_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	BLOB_ID_REF:
				aCref->type = BLOB_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	RESP_CODES_ID_REF:
				aCref->type = RESP_CODES_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	VIA_ARRAY_REF:
		case	VIA_ITEM_ARRAY_REF:
		case	VIA_LIST_REF:
				{
					if(refType == VIA_ARRAY_REF)
					{
						aCref->type = ARRAY_ITYPE;
					}
					else if(refType == VIA_ITEM_ARRAY_REF)
					{
						aCref->type = ITEM_ARRAY_ITYPE;
					}
					else //VIA_LIST_REF
					{
						aCref->type = LIST_ITYPE; // stevev 31jan17 - just found this error...was: VIA_LIST_REF;
					}
					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 

					if (pExpr)
					{
						pExpr->hydratePrototype(aCref->pExpr);
					}

					aCref->pRef = new aCreference; 
					if (pRef)
					{
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{
						delete aCref->pRef;
						LOGIT(CERR_LOG,"Reference with expression but no wefewence.\n");
					}
				}
				break;

		case	VIA_BITENUM_REF:
		case	VIA_COLLECTION_REF:
		case    VIA_FILE_REF:
				{
					if(refType == VIA_BITENUM_REF)
					{
						aCref->type = VARIABLE_ITYPE;
					}
					else if(refType == VIA_COLLECTION_REF)
					{
						aCref->type = COLLECTION_ITYPE;
					}
					else  //VIA_FILE_REF
					{
						aCref->type = FILE_ITYPE;
					}
					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e)INTCST_OPCODE;
					pexpressElmnt.expressionData = (unsigned int) intValue;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);

					aCref->pRef = new aCreference; 
					if (pRef)
					{
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{
						delete aCref->pRef;
						LOGIT(CERR_LOG,"Reference with expression but no reference.\n");
					}
				}
				break;

		case	VIA_RECORD_REF:	
				aCref->type = RECORD_ITYPE;
				aCref->isRef = 0;
				//aCref->iName = p->val.member;
				break;

		case	VIA_PARAM_REF:
				aCref->type = VARIABLE_ITYPE;
				aCref->isRef = 0;
				//aCref->iName = p->val.member;
				break;

		case	VIA_PARAM_LIST_REF:
				aCref->type = VAR_LIST_ITYPE;
				aCref->isRef = 0;
				//aCref->iName = p->val.member;
				break;

		case	VIA_BLOCK_REF:
				aCref->type = BLOCK_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	FILE_ID_REF:	
				aCref->type = FILE_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	CHART_ID_REF:
				aCref->type = CHART_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	GRAPH_ID_REF:
				aCref->type = GRAPH_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	AXIS_ID_REF:
				aCref->type = AXIS_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	WAVEFORM_ID_REF:
				aCref->type = WAVEFORM_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	SOURCE_ID_REF:
				aCref->type = SOURCE_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	LIST_ID_REF	:
				aCref->type = LIST_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	GRID_ID_REF:
				aCref->type = GRID_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	IMAGE_ID_REF:
				aCref->type = IMAGE_ITYPE;
				aCref->isRef = 0;
				aCref->id = (ITEM_ID)intValue;
				break;

		case	CONSTANT_REF:	
				aCref->type = MAX_ITYPE;
				aCref->isRef = 0;
				aCref->exprType = eT_Direct;
				aCref->pExpr = new aCexpression; 
				if (pExpr)
				{
					pExpr->hydratePrototype(aCref->pExpr);
				}
				break;

		case	SEPARATOR_REF:
				aCref->type = MAX_ITYPE;
				aCref->isRef = 0;
				aCref->id    = 0;
				aCref->iName = 0;
				aCref->exprType = eT_Unknown;
				aCref->pExpr = NULL;
				break;

		case	ROWBREAK_REF:
				aCref->type = MAX_ITYPE;
				aCref->isRef = 0;
				aCref->id    = 0;
				aCref->iName = 0;
				aCref->exprType = eT_Unknown;
				aCref->pExpr = NULL;
				break;
			
		case	VIA_CHART_REF:
				{
					aCref->type = SOURCE_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					//pexpressElmnt->expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);

					if (pRef)
					{
						aCref->pRef = new aCreference; 
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{;
						LOGIT(CERR_LOG,"Chart Reference with expression but no reference.\n");
					}
				}
				break;

		case	VIA_GRAPH_REF:
				{
					aCref->type = WAVEFORM_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					//pexpressElmnt->expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);

					if (pRef)
					{
						aCref->pRef = new aCreference; 
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{;
						LOGIT(CERR_LOG,"Graph Reference with expression but no reference.\n");
					}
				}
				break;

		case	VIA_SOURCE_REF:
				{
					aCref->type = VARIABLE_ITYPE;

					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					//pexpressElmnt->expressionData = (unsigned int) p->val.member;
					pexpressElmnt.dependency.which = 0;
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);

					if (pRef)
					{
						aCref->pRef = new aCreference; 
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{;
						LOGIT(CERR_LOG,"Source Reference with expression but no reference.\n");
					}
				}
				break;

		case    VIA_BLOB_REF:
				{
					// TODO: implement this to get to atributes 
				}
				break;

		case	VIA_ATTR_REF:
				{
					aCref->type = MAX_ITYPE; /* type cannot be accerned */
				
					aCref->isRef = 1;
					aCref->exprType = eT_Direct;
					aCref->pExpr = new aCexpression; 
					aCexpressElement pexpressElmnt;
					pexpressElmnt.exprElemType = (expressionElementType_e) INTCST_OPCODE;
					//pexpressElmnt->expressionData   = (unsigned int) (p->val.member & 0x0000ffff);
					//pexpressElmnt->dependency.which = (unsigned int) ((p->val.member & 0xffff0000)>>16);
					pexpressElmnt.dependency.useOptionVal = aOp_dontCare;
					aCref->pExpr->expressionL.push_back(pexpressElmnt);

					if (pRef)
					{
						aCref->pRef = new aCreference; 
						pRef->hydratePrototype(aCref->pRef);
					}
					else
					{;
						LOGIT(CERR_LOG,"Attr Reference with expression but no reference.\n");
					}
				}
				break;

  		default:
				break;
	}
}


char *abCReference::refTypeName(int id)		// fm8 reftype
{
	typedef struct {int type; char *name;} MAP;
	static MAP map[] = {
		{ ITEM_ID_REF,	"ID - Item" },
		{ ITEM_ARRAY_ID_REF,	"ID - Ref-Array" },
		{ COLLECTION_ID_REF,	"ID - Collection" },
		{ VIA_ITEM_ARRAY_REF,	"Via- Ref-Array" },
		{ VIA_COLLECTION_REF,	"Via- Collection" },
		{ VIA_RECORD_REF,	"Via- Record" },
		{ VIA_ARRAY_REF,	"Via- Val-Array" },
		{ VIA_VAR_LIST_REF,	"Via- Var-List" },
		{ VARIABLE_ID_REF,	"ID - Variable" },
		{ MENU_ID_REF,	"ID - Menu" },
		{ EDIT_DISP_ID_REF,	"ID - EditDisplay" },
		{ METHOD_ID_REF,	"ID - Method" },
		{ REFRESH_ID_REF,	"ID - Refresh" },
		{ UNIT_ID_REF,	"ID - Unit" },
		{ WAO_ID_REF,	"ID - WAO" },
		{ RECORD_ID_REF,	"ID - Record" },
		{ ARRAY_ID_REF,	"ID - Val-Array" },
		{ VAR_LIST_ID_REF,	"ID - Var-List" },
		{ FILE_ID_REF,	"ID - File" },
		{ CHART_ID_REF,	"ID - Chart" },
		{ GRAPH_ID_REF,	"ID - Graph" },
		{ AXIS_ID_REF,	"ID - Axiz" },
		{ WAVEFORM_ID_REF,	"ID - Waveform" },
		{ SOURCE_ID_REF,	"ID - Source" },
		{ LIST_ID_REF,	"ID - List" },
		{ IMAGE_ID_REF,	"ID - Image" },
		{ SEPARATOR_REF,	"ColBreak" },
		{ CONSTANT_REF,	"Constant" },
		{ VIA_FILE_REF,	"Via- File" },
		{ VIA_LIST_REF,	"Via- List" },
		{ VIA_BITENUM_REF,	"Via- Bit-Enum" },
		{ GRID_ID_REF,	"ID - Grid" },
		{ VIA_CHART_REF,	"Via- Chart" },
		{ VIA_GRAPH_REF,	"Via- Graph" },
		{ VIA_SOURCE_REF,	"Via- Source" },

		{ -1, "not found" }
	};


	for (int i=0; map[i].type >= 0; i++)
		if (map[i].type == id)
		{
			return map[i].name;
			break; 
		}

	return "Unknown Reference Type";
}

abCReference& abCReference::operator=(const abCReference& src)
{
	FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
	rawRefType = src.rawRefType;
	refType    = src.refType; 
	intValue   = src.intValue;
	if (src.pExpr) pExpr = new abCExpression(*(src.pExpr)); else pExpr = NULL;
	if (src.pRef)  pRef  = new abCReference(*(src.pRef));   else pRef  = NULL;
	return *this;
};