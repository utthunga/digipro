/**********************************************************************************************
 *
 * $Workfile: FM8_objectSet.cpp $
 * 31aug11 - steveh
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_objectSet class implementation...
 *		
 *
 */

#include "FM8_defs.h"
#include "FM8_objectSet.h"
#include "logging.h"

FM8_ObjectSet::~FM8_ObjectSet()
{
	for (FM8_pObjectList_t::iterator oIT = objList.begin(); oIT != objList.end(); ++oIT)
	{
		delete (FM8_Object*)(*oIT);
	}
}

RETURNCODE FM8_ObjectSet::index_ObjectData(unsigned& size)
{
	int i, x = pTopClass->getObjectCount();
	FM8_Object *pObj;

	//Read 'em
	for ( i = 0; i < x; i++)
	{
		pObj = new FM8_Object(pTopClass,fp,saveBinary);
		if (pObj == NULL)
		{
			return FMx_MEMFAIL;
		}
		pObj->obj_type = obj_8_DD_item;
		// no longer 		read_Object(size);
		if (pObj->read_Object_Info( size, true ))// read the object, skip the payload
		{
			break;// out of for loop..size error will show it
		}
		objList.push_back(pObj);
		pObj = NULL;
	}// next object

	if ( size != 0 )
	{
		LOGIT(CLOG_LOG,"Object Read Ending size is %#04x not zero.\n", size);
		return FMx_OPPFAIL;
	}

	return SUCCESS;
}

RETURNCODE FM8_ObjectSet::ExecuteVisitor( FMx_visitor* pVisitor )
{
	RETURNCODE r = FAILURE;	
	FM8_Object* pObj;

	try
	{
		for ( FM8_pObjectList_t::iterator oIT = objList.begin(); oIT != objList.end(); ++oIT )
		{
			pObj = (FM8_Object*)(*oIT);

			currentObjectIndex = pObj->obj_Index;

			r = pObj->ExecuteVisitor( pVisitor );
			if ( r != SUCCESS )
			{
				currentObjectIndex = 0;
				return r;
			}
		}//next

		currentObjectIndex = 0;
	}
	catch (...)
	{
	}

	return r;
}

void FM8_ObjectSet::out(unsigned idx, unsigned size)
{
	FM8_Object *pObj;
	int x = objList.size();

	LOGIT(COUT_LOG," FM8_ObjectSet\n");
	// print it out
	for (int i = 1; i <= x; i++)
	{
		pObj = objList[i-1];		
		pObj->out(i,size);;
	} 
	
	LOGIT(COUT_LOG," FM8_Object Set's Heap\n");

	// print it out
	for (int i = 1; i <= x; i++)
	{
		pObj = objList[i-1];		
		pObj->out2(i,size);;
	}  

	LOGIT(COUT_LOG,"======================================================================\n");
} 
