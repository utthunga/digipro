#include "Convert.h"
#include "tags_sa.h"
#include "ddldefs.h"
#include "v_N_v+Debug.h"


exprElemType_t ConvertToA::v8_Expr_2_V10[] = EXPR_TYPE_CONVERT;

wchar_t* GetWC(const char* c)
{
    const size_t cSize = strlen(c)+1;
    wchar_t* wc = new wchar_t[cSize];
    mbstowcs(wc, c, cSize);

    return wc;
}

/***
expressionType_t ConvertToDDL::ConditionType(condType_t type)
{
	switch (type)
	{
		case cdt_IF:
			return eT_IF_expr;

		case cdt_SELECT:
			return eT_SEL_expr;

		case cdt_DIRECT:
			return eT_Direct;

		case cdt_THEN:	// if is TRUE
			return eT_IF_isTRUE;

		case cdt_ELSE:	// if is FALSE
			return eT_IF_isFALSE;

		case cdt_CASE:
			return eT_CASE_expr;

		case cdt_DEFAULT:
			return et_SEL_isDEFAULT;

		case cdt_INVALID:
		default:
			return eT_Unknown; // TODO: need to throw some kind of error here
	}
}
***/
expressionType_t ConvertToDDL::ClauseType(condType_t type)
{
	switch (type)
	{
	case cdt_IF:		 // 0///
		return eT_IF_expr;	// == axiomType_t aT_IF_expr..just cast
	case cdt_SELECT:
		return eT_SEL_expr; // == axiomType_t aT_SEL_expr..just cast
	case cdt_DIRECT:
		return eT_Direct;   // == axiomType_t aT_Direct == clauseType_t cT_Direct..just cast
	case cdt_INVALID:
		return eT_Unknown;  // == axiomType_t aT_Unknown == clauseType_t cT_Unknown..just cast
	case cdt_THEN:
		return eT_IF_isTRUE;// == clauseType_t cT_IF_isTRUE (THEN)  ..just cast 
	case cdt_ELSE:
		return eT_IF_isFALSE;// == clauseType_t cT_IF_isFALSE (ELSE)  ..just cast 
	case cdt_CASE:
		return eT_CASE_expr; // == clauseType_t cT_CASE_expr...just cast
	case cdt_DEFAULT:
		return et_SEL_isDEFAULT;// == clauseType_t ct_SEL_isDEFAULT...just cast
	default:
		DEBUGLOG(CERR_LOG|CLOG_LOG,"Convert unknown type %d to DDL attempted.\n",type);
		return eT_Unknown;
	}//end switch
}


// fma to fm8 expression conversion
expressElemType_t ConvertToDDL::ExpressionElementType(exprElemType_t type)
{
	switch (type)
	{
		case eet_Undefined:			//00
			return eeT_Unknown;

		case eet_logical_not:		//01
			return eet_NOT; 		//_OPCODE 1			bool

		case eet_negation:			//02
			return eet_NEG;			//_OPCODE 2			Value

		case eet_complement:		//03
			return eet_BNEG;		//_OPCODE 3			Value

		case eet_addition:			//04
			return eet_ADD;			//_OPCODE 4			Value

		case eet_subtraction:		//05
			return eet_SUB;			//_OPCODE 5			Value

		case eet_multiplication:	//06
			return eet_MUL;			//_OPCODE 6			Value

		case eet_division:			//07
			return eet_DIV;			//_OPCODE 7			Value

		case eet_modulus:			//08
			return eet_MOD;			//_OPCODE 8			Value

		case eet_left_shift:		//09
			return eet_LSHIFT;		//_OPCODE 9			Value

		case eet_right_shift:		//10
			return eet_RSHIFT;		//_OPCODE 10		Value

		case eet_bitwise_and:		//11
			return eet_AND;			//_OPCODE 11		Value

		case eet_bitwise_or:		//12
			return eet_OR;			//_OPCODE 12		Value

		case eet_bitwise_xor:		//13
			return eet_XOR;			//_OPCODE 13		bool

		case eet_logical_and:		//14
			return eet_LAND;		//_OPCODE 14		bool

		case eet_logical_or:		//15
			return eet_LOR;			//_OPCODE 15		bool

		case eet_less_than:			//16
			return eet_LT;			//_OPCODE 16		bool

		case eet_greater_than:		//17
			return eet_GT;			//_OPCODE 17		bool

		case eet_less_or_equal:		//18
			return eet_LE;			//_OPCODE 18		bool

		case eet_greater_or_equal:	//19
			return eet_GE;			//_OPCODE 19		bool

		case eet_equal:				//20
			return eet_EQ;			//_OPCODE 20		bool

		case eet_not_equal:			//21
			return eet_NEQ;			//_OPCODE 21		bool

		case eet_conditional:		//22		-- ()?: operator-� The Only Tertiary operation 
			return eeT_Unknown;		//			-- HART tokenizer doesn't use this

		case eet_number_to_string:	//23		-- unary conversion to string
			return eeT_Unknown;		//			-- not valid in version 10.3

		case eet_concatenation:		//24		-- of strings (the only string operator)
			return eeT_Unknown;		//			-- not valid in version 10.3

		case eet_integer_constant:	//25] INTEGER,  -- signed integer 
			return eet_INTCST;		// changed from unknown stevev 09jan13

		case eet_unsigned_constant:	//26] INTEGER,  -- unsigned integer
			return eet_INTCST;//changed from unknown stevev 09jan13-should never happen in HART

		case eet_floating_constant:	//27 6] Floating, 
			return eet_FPCST;

		case eet_string_constant:	//28 7] String, 
			return eet_STRCONST;	//STRCST_OPCODE  33

		case eet_numeric_value:		//29 8] Reference,-- Must resolve to a numeric value
			if (reconcile)
				return eet_VARREF;		//FDI version 10.3 opcodes this is boolean: unused in HART
			else
				return eeT_Unknown;		// 08mar13 timj - FDI spev version 10.3 opcodes

		case eet_all_value:		//30 29] Reference	-- Must resolve to a Unicode String	
			return eet_VARREF;		// FDI spec version 10.3 opcodes: this is all references

		case eet_attr_value:		//31 0]                attr-name	byte; item-ref Reference
			//return eeT_Unknown;
			return eet_VARREF;		// 08mar13 timj -  - FDI spev version 3 opcodes make attrRef

		case eet_min_max_value:		//32 1] which	INTEGER; attr-name	byte; item-ref Reference
			return eeT_Unknown;		// code for eet_MAXREF or eet_MINREF

		case eet_method_value:		//33 2] Reference	-- to a method
			return eeT_Unknown;

		case eet_current_role:		//34 3
			return eeT_Unknown;

		case eet_array_index: 		//35 4  -- only useable in value array or list (string function)
			return eeT_Unknown;


		// version 8 types, convertible to version ten types	
		case eet_variable_id:		//40 INTEGER -> eet_numeric_value & reference
			return eet_VARID;		//_VARVAL 24	isRef = F

		case eet_maxVal:			//41 INTEGER whch, INTEGER id -> eet_min_max_value & which & MAX & ref
			return eet_MAXVAL;		//_VARVAL 25	isRef = F,isMax = T

		case eet_minVal:			//42 INTEGER whch, INTEGER id -> eet_min_max_value & which & MIN & ref
			return eet_MINVAL;		//_VARVAL 26	isRef = F,isMax = F

		case eet_maxValRef:			//43 INTEGER whch, REF var    -> eet_min_max_value & which & MAX & ref
			return eet_MAXREF;		//_VARVAL 28	isRef = T,isMax = T

		case eet_minValRef:			//44 INTEGER whch, REF var    -> eet_min_max_value & which & MIN & ref 
			return eet_MINREF;		//_VARVAL 29	isRef = T,isMax = F

		case eet_count_ref:			//45 Reference	-> eet_attr_value & COUNT & reference
			return eet_COUNTREF;	//CNTREF_OPCODE	35

		case eet_capacity_ref:		//46 Reference	-> eet_attr_value & CAPACITY & reference
			return eet_CAPACITYREF;	//CAPREF_OPCODE	36

		case eet_first_ref:			//47 Reference	-> eet_attr_value & FIRST & reference
			return eet_FIRSTREF;	//FSTREF_OPCODE	37 // stevev 17aug06 - this is a VARREF

		case eet_last_ref:			//48 Reference	-> eet_attr_value & LAST & reference
			return eet_LASTREF;		//LSTREF_OPCODE	38 // stevev 17aug06 - this is a VARREF

		case eet_dflt_val_ref:		//49 Reference	-> eet_attr_value & DFLT & reference
			return eet_DFLT_VALREF;	//DFLTVAL_OPCODE 39

		case eet_view_min_ref:		//50 Reference  -> eet_attr_value & MIN  & reference
			return eet_VIEW_MINREF;	//VMIN_OPCODE    40

		case eet_view_max_ref:		//51 Reference  -> eet_attr_value & MAX  & reference
			return eet_VIEW_MAXREF;	//VMAX_OPCODE    41

		case eet_error:				//45
			return eeT_Unknown;

		default:
			return eeT_Unknown;
	}
}



void ConvertToString::fetchDictionString(unsigned strNumber, wstring& returnedStr)
{
	if (pFILE == NULL)
	{
		returnedStr = L"No file is active.\n";
	}
	else
	{
		pFILE->getDictionString(strNumber, returnedStr);
	}
	return;
}

void ConvertToString::fetchDictionString(wstring  keyString, wstring& returnedStr)
{
	if (pFILE == NULL)
	{
		returnedStr = L"No file is active.\n";
	}
	else
	{
		pFILE->getDictionString(keyString, returnedStr);
	}
	return;
}

void ConvertToString::fetchLiteralString(unsigned strNumber, wstring& returnedStr)
{
	if (pFILE == NULL)
	{
		returnedStr = L"No file is active.\n";
	}
	else
	{
		pFILE->getLiteralString(strNumber, returnedStr);
	}
	return;
}

void ConvertToString::filterString(wstring& inAndOutString)
{
	wstring locStr(inAndOutString);
	wstring locLangStr;

	if (pFILE != NULL)
	{
		locLangStr = pFILE->langCode;
	}
	//else leave it empty - means leave all the strings (out == in)
	get_string_translation(inAndOutString,inAndOutString);

	// no-op right now (out == in)
	return;
}

//from CDictionary function of the same name
int ConvertToString ::get_string_translation(wstring &instr, wstring &outstr)
{
	int n = (int)instr.size();
	wchar_t *in = (wchar_t *)instr.c_str();
	wchar_t *out = new wchar_t[n+1];
	memset( out, 0, sizeof(wchar_t)*(n+1) );

	int result = get_string_translation(in, out, n+1);

	if (result == 0)
	{
		wstring temp(out);
		outstr = temp;
	}

	delete [] out;

	return result;
}


//from CDictionary function of the same name
int ConvertToString ::get_string_translation(wchar_t *string,wchar_t *outbuf,int outbuf_size)
{
	//ADDED By Deepak , initializing all vars
	wchar_t 	*ci=NULL;			/* input character pointer */
	wchar_t 	*co=NULL;			/* output character pointer */
	wchar_t 	*first_phrp=NULL;		/* first phrase pointer */
	wchar_t 	*lang_cntry_phrp=NULL;	/* language + country phrase pointer */
	wchar_t 	*lang_only_phrp=NULL;	/* language-only phrase pointer */
	wchar_t 	lang_only[10];//[5];	/* language-only pulled from language/country code */
	wchar_t 	*new_def_phrp=NULL;	/* new-style default phrase pointer */
	wchar_t 	*old_def_phrp=NULL;	/* old-style language-only phrase pointer */
	wchar_t 	*out_phrp=NULL;		/* output phrase pointer */
	int		code_length=0;	/* length of language/country code, in characters */
	int     n = 0;          /* working value ofstring length */
	
	wchar_t  lang_cntry[10];
	// stevev 2sept08 - outbuf is never guranteed to hold the entire in buf string!
	wchar_t* pBuf = new wchar_t[wcslen(string) + 2];
	pBuf[0] = pBuf[1] = 0;

	//wchar_t  langCode[LANGCD_LEN];
	/*if(languageCode)
		wcscpy(lang_cntry,languageCode);
	else
		wcscpy(lang_cntry,DEF__LANG__CTRY);
	*/
	if (pFILE && pFILE->langCode )
	{
		wcscpy(lang_cntry,pFILE->langCode);
	}
	else
	{
		wcscpy(lang_cntry,DEF__LANG__CTRY);
	}
	/*
	 *	Make all the phrase pointers null pointers.
	 */
	first_phrp = 0;
	lang_cntry_phrp = 0;
	lang_only_phrp = 0;
	new_def_phrp = 0;
	old_def_phrp = 0;
	out_phrp = 0;

	/*
	 *	If the input string is a null string (which is legal), we can
	 *	skip all the string processing and return the output string,
	 *	which we set to a null string.
	 */
	if (string[0] == 0) 
	{
		outbuf[0] = 0;
		delete[] pBuf;
		return(0);
	}

	/*
	 *	If the input country_code is full-size (i.e., seven characters),
	 *	extract the language code from the language/country code.
	 *	Otherwise, make the language-only code a null string.
	 */
	if (wcslen(lang_cntry) == 7) 
	{
		(void)wcsncpy(lang_only, lang_cntry, (size_t)3);
		lang_only[3] = COUNTRY_CODE_MARK;
		lang_only[4] = _T('\0');
	} 
	else 
	{
		lang_only[0] = _T('\0');
	}

	/*
	 *	Check to see if the input string begins with a COUNTRY_CODE_MARK.
	 *	If it does not, set the first-phrase pointer, then enter the loop.
	 */
	if (string[0] != COUNTRY_CODE_MARK) 
	{
		// stevev - this default string but points to an empty return buf :first_phrp = outbuf;
		first_phrp = string; // stevev - changed 29nov11 from above
	}

	/*
	 *	The Loop:
	 *		On a character-by-character basis, check for any of the
	 *	possible language or language/country codes, or escape sequences.
	 *	Look for the specified language/country code in the input string
	 *	in this order:
	 *
	 *		- the complete language/country code
	 *		- the language-only code (new style)
	 *		- the language-only code (old style)
	 *
	 *	If one of the language/country codes matches, and the corresponding
	 *	phrase pointer is not yet set, save the address of that phrase.  In
	 *	any case that a substring in the form of a language/country code is
	 *	found, even if it's not one we're looking for, insert an end-of-string
	 *	character in the output buffer, then move the input string pointer
	 *	beyond the language/country code.  If no language/country code is
	 *	found, look for escape sequences.  Do this this until the input
	 *	string's end-of-string is encountered.
	 */

	for (co = pBuf, ci = string; *ci; ci++) 
	{
		n = wcslen(ci);
	/*
	 *	Look for the complete language/country code.
	 */
		if (  n>=7 &&   (ci[0] == COUNTRY_CODE_MARK) && iswalpha(ci[1]) &&
				iswalpha(ci[2]) && (ci[3] == _T(' ')) && iswalpha(ci[4])
				&& iswalpha(ci[5]) && (ci[6] == COUNTRY_CODE_MARK)) 
		{
			code_length = 7;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) 
			{
				lang_cntry_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
					(wcsncmp(ci, DEF__LANG__CTRY, code_length) == 0)) 
			{
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (new style).
	 */
		} 
		else if ( n>=4 && (ci[0] == COUNTRY_CODE_MARK) && iswalpha(ci[1]) &&
				iswalpha(ci[2]) && (ci[3] == COUNTRY_CODE_MARK)) 
		{
			code_length = 4;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) {
				lang_cntry_phrp = co + 1;
			}

			if ((lang_only_phrp == 0) && (lang_only[0] != _T('\0')) &&
					(wcsncmp(ci, lang_only, code_length) == 0)) {
				lang_only_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
					(wcsncmp(ci, DEF__LANG__CTRY, code_length) == 0)) {
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (old style); default only.
	 */
		} else if ( n >= 4 && (ci[0] == COUNTRY_CODE_MARK) && iswdigit(ci[1]) &&
				iswdigit(ci[2]) && iswdigit(ci[3])) 
		{
			code_length = 4;

			if ((old_def_phrp == 0) &&
					(wcsncmp(ci, L"|001", code_length) == 0)) 
			{
				old_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	If the escape sequence character (\) is encountered, convert
	 *	the following character as required.  These are the escape
	 *	sequences required by the DDL Spec.
	 */

		} else if (*ci == _T('\\')) 
		{

			switch (*(ci + 1)) 
			{

				case _T('a'):
					*co++ = _T('\a');
					ci++;
					break;

				case _T('f'):
					*co++ = _T('\f');
					ci++;
					break;

				case _T('n'):
					*co++ = _T('\n');
					ci++;
					break;

				case _T('r'):
					*co++ = _T('\r');
					ci++;
					break;

				case _T('t'):
					*co++ = _T('\t');
					ci++;
					break;

				case _T('v'):
					*co++ = _T('\v');
					ci++;
					break;

				default:
					*co++ = *(ci + 1);
					ci++;
					break;
			}

	/*
	 *	This is the 'normal' case; this character has no special
	 *	significance, so just copy it to the output pointer.
	 */
		} 
		else 
		{
			*co++ = *ci;
		}
	}// next wchar_t


	/*
	 *	Tack an end-of-string character onto the final phrase.
	 */
	*co++ = _T('\0');

	/*
	 *	We may have found a phrase to output.  Copy the highest priority
	 *	phrase into the holding buffer.  Priority is determined in this 
	 *	order, depending upon which string pointers have been assigned 
	 *	non-null values:
	 *
	 *		- the phrase specified by the complete language/country code,
	 *		- the phrase specified by just the language in the
	 *		  language/country code,
	 *		- the phrase specified by the new-style default
	 *		  language/country code,
	 *		- the phrase specified by the old-style default
	 *		  language/country code,
	 *		- the first phrase encountered in the input string.
	 */

	if (lang_cntry_phrp) {
		out_phrp = lang_cntry_phrp;
	} else if (lang_only_phrp) {
		out_phrp = lang_only_phrp;
	} else if (new_def_phrp) {
		out_phrp = new_def_phrp;
	} else if (old_def_phrp) {
		out_phrp = old_def_phrp;
	} else {
		out_phrp = first_phrp;
	}

	/*
	 *	Check the length of the output buffer.  If the phrase to be output
	 *	is longer than the output buffer, return an error code.  Otherwise,
	 *	copy the phrase in the holding buffer into the output buffer.  
	 */
	if ((size_t) outbuf_size < wcslen(out_phrp)) 
	{// do the best we can
		wcsncpy(outbuf, out_phrp, outbuf_size-1);
		outbuf[outbuf_size-1] = _T('\0');
		delete[] pBuf;
		return -5;
	} 
	else 
	{
		(void)wcscpy(outbuf, out_phrp);
	}

	delete[] pBuf;
	return 0;// success
}


void ConvertToString::fetchSymbolNameNtype(unsigned symNumber,  string& retString, int& Stype)
{
	Stype = NOT_AN_ITEM_TYPE;
	if (pFILE == NULL)
	{
		retString = "No FILE is active.";
	}
	else
	{
		pFILE->getSymbolName(symNumber, retString, Stype);
	}
	return;
}

void ConvertToString::fetchSymbolNameByIndex(unsigned index, string& retString, int& Stype)
{
	if (pFILE == NULL)
	{
		retString = "No FILE is active.";
	}
	else
	{
		pFILE->getSymbolNameByIndex(index, retString, Stype);
	}
	return;
}


int ConvertTo8::refType(int fmaType)
{
	typedef struct {int typeA; int type8;} MAP;
	static MAP map[] = {
		{   ITEM_ID_REF_A,	ITEM_ID_REF },
		{   ITEM_ARRAY_ID_REF_A,	ITEM_ARRAY_ID_REF },
		{   COLLECTION_ID_REF_A,	COLLECTION_ID_REF },
		{   VIA_ITEM_ARRAY_REF_A,	VIA_ITEM_ARRAY_REF },
		{   VIA_COLLECTION_REF_A,	VIA_COLLECTION_REF },
		{   VIA_RECORD_REF_A,	VIA_RECORD_REF },
		{   VIA_ARRAY_REF_A,	VIA_ARRAY_REF },
		{   VIA_VAR_LIST_REF_A,	VIA_VAR_LIST_REF },
		{   VARIABLE_ID_REF_A,	VARIABLE_ID_REF },
		{   MENU_ID_REF_A,	MENU_ID_REF },
		{   EDIT_DISP_ID_REF_A,	EDIT_DISP_ID_REF },
		{   METHOD_ID_REF_A,	METHOD_ID_REF },
		{   REFRESH_ID_REF_A,	REFRESH_ID_REF },
		{   UNIT_ID_REF_A,	UNIT_ID_REF },
		{   WAO_ID_REF_A,	WAO_ID_REF },
		{   RECORD_ID_REF_A,	RECORD_ID_REF },
		{   ARRAY_ID_REF_A,	ARRAY_ID_REF },
		{   VAR_LIST_ID_REF_A,	VAR_LIST_ID_REF },
		{   FILE_ID_REF_A,	FILE_ID_REF },
		{   CHART_ID_REF_A,	CHART_ID_REF },
		{   GRAPH_ID_REF_A,	GRAPH_ID_REF },
		{   AXIS_ID_REF_A,	AXIS_ID_REF },
		{   WAVEFORM_ID_REF_A,	WAVEFORM_ID_REF },
		{   SOURCE_ID_REF_A,	SOURCE_ID_REF },
		{   LIST_ID_REF_A,	LIST_ID_REF },
		{   IMAGE_ID_REF_A,	IMAGE_ID_REF },
		{   SEPARATOR_REF_A,	SEPARATOR_REF },
		{   CONSTANT_REF_A,	CONSTANT_REF },
		{   VIA_FILE_REF_A,	VIA_FILE_REF },
		{   VIA_LIST_REF_A,	VIA_LIST_REF },
		{   VIA_BITENUM_REF_A,	VIA_BITENUM_REF },
		{   GRID_ID_REF_A,	GRID_ID_REF },
		{   VIA_CHART_REF_A,	VIA_CHART_REF },
		{   VIA_GRAPH_REF_A,	VIA_GRAPH_REF },
		{   VIA_SOURCE_REF_A,	VIA_SOURCE_REF },
		/* added stevev 03oct13 **/
		{   BLOB_ID_REF_A,      BLOB_ID_REF },
		{   PLUGIN_ID_REF_A,    PLUGIN_ID_REF },
		{   TEMPLATE_ID_REF_A,  TEMPLATE_ID_REF },
		{   ROWBREAK_REF_A,     ROWBREAK_REF },
		{   VIA_ATTR_REF_A,     VIA_ATTR_REF },//47 --> 43
		{ -1, -1 }
	};
		/* these are not translated ***
        BLOCK_B_REF_A			10
        BLOCK_A_REF_A			11
        RESP_CODES_ID_REF_A		15
        COMPONENT_A				28
        COMPFOLDER_A			29
        COMPREF_A				30
        COMPREL_A				31
        INTERFACE_A				32
        ****/
	/* these need special activity ***
        VIA_ATTR_REF_A			47
        VIA_RESOLVED_REF_A		48
	****/
	if ( fmaType == VIA_RESOLVED_REF_A)
	{
		LOGIT(CERR_LOG|CLOG_LOG, "Reftype translator got a via Resolved-Reference type.\n");
	}

	int ret = -1;
	for (int i=0; map[i].typeA >= 0; i++)
	{
		if (map[i].typeA == fmaType)
		{
			ret = map[i].type8;
			break; 
		}
	}// next

	return ret;
}

int ConvertTo8::enumerationTag(int tag)
{
	int map[] = {0, 3, 4, 1, 2, 5};
	assert(0<= tag  &&  tag <= 5);
	return map[tag];
}

int ConvertTo8::baseClass(int status)
{
	BASE_CLASS_MAP;

	for (int i=0; map[i].fma >= 0; i++)
		if (map[i].fma == status)
		{
			status = map[i].fmx;
			break; 
		}

	return status;
	
}

int ConvertTo8::parameterType(int type)
{
	PARAMETER_TYPE_MAP;

	for (int i=0; map[i].fma >= 0; i++)
		if (map[i].fma == type)
		{
			type = map[i].fmx;
			break; 
		}

	return type;
}

// class bits are set at different positions in FM8 vs FMA
// given the position of a bit in FMA, return the corresponding
// position in FM8

int ConvertTo8::classBitA28(int bitposA)
{
	// Note: only differing bit positions are mapped
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
	//  FM8-FMA  bit positions for Class
		{5,  13},\
		{9,  26},\
		{10, 24},\
		{17, 22},\
		{20, 31},\
		{21, 32},\
		{22, 33},\
		{23, 34},\
		{24, 35},\
		{25, 36},\
		{26, 37},\
		{-1, -1}
	};

	int bitpos8 = bitposA;		// most of the positions are the same

	assert(0 <= bitposA && bitposA < 64);

	for (int i = 0; map[i].fma >= 0; i++)
	{
		if (map[i].fma == bitposA)
			bitpos8 = map[i].fm8;
	}

	return bitpos8;
}

// return the FMA class bitstring converted to an FM8 class bitstring
UINT64 ConvertTo8::classA28(UINT64 bsA)
{
	// if bit at position n of bsA is set at FMA position in the map
	//	then set the bit in bs8 at the corresponding FM8 position from the map
	// else if bit at postion n of bsA is set
	//	then set the bit at the same position in bs8

	UINT64 bs8 = 0;			// resulting FM8 class bitstring
	UINT64 onebit = 0x01;
	for (int n=0; n < 64; n++)
	{
		UINT64 bsn = (onebit << n);	// bitstring with bit set at posion n
		if ( bsn & bsA )
		{
			// bit at position n of bsA is set
			bs8 |= (onebit << classBitA28(n));
		}
	}

	return bs8;
}

unsigned ConvertTo8::chartType(unsigned typeA)
{
	unsigned type8 = -1;
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
		/* 0 {INVALID},*/			{0, 124},// very invalid
		/* 1 {GAUGE_CTYPE},*/		{1, 5},	// GAUGE_ATYPE
		/* 2 {HORIZ_BAR_CTYPE},*/	{2, 3},	// HORIZ_BAR_ATYPE
		/* 3 {SCOPE_CTYPE},*/		{3, 2},  // SCOPE_ATYPE
		/* 4 {STRIP_CTYPE},*/		{4, 0},	// STRIP_ATYPE
		/* 5 {SWEEP_CTYPE},*/		{5, 1},	// SWEEP_ATYPE
		/* 6 {VERT_BAR_CTYPE},*/	{6, 4},	// VERT_BAR_ATYPE
		/* 7 { -1}*/				{-1, -1}	// eol
	};
	
	for (int i=0; map[i].fm8 != -1; i++)
	{
		if (map[i].fma == typeA)
			type8 = map[i].fm8;
	}

	return type8;
}

unsigned ConvertTo8::axisScaling(unsigned scaleA)
{
	return scaleA + 1;
}

unsigned ConvertTo8::waveType(unsigned typeA)
{

	unsigned type8 = -1;
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
		/* 0 {INVALID},*/				{0, 124},// very invalid
		/* 1 {YT_WAVEFORM_TYPE},*/		{1, 1},	// yt
		/* 2 {XY_WAVEFORM_TYPE},*/		{2, 0},	// xy
		/* 3 {HORZ_WAVEFORM_TYPE},*/	{3, 2},  // HORIZ
		/* 4 {VERT_WAVEFORM_TYPE},*/	{4, 3},	// VERT
		/* 5 { -1}*/					{-1, -1}	// eol
	};
	
	for (int i=0; map[i].fm8 != -1; i++)
	{
		if (map[i].fma == typeA)
			type8 = map[i].fm8;
	}

	return type8;
}


unsigned ConvertTo8::orient(unsigned typeA)
{
	unsigned fm8 = gd_Unknown;		// invalid

	if (typeA == 0)
		fm8 = gd_Horizontal;

	else if (typeA == 1)
		fm8 = gd_Vertical;

	return fm8;
}

unsigned ConvertTo8::outputClass(unsigned classA)
{
	unsigned class8 = -1;
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
		/* 0 AUTO-GOOD */				{0, 0},	
		/* 1 MANUAL-GOOD */				{1, 3},	
		/* 2 AUTO-BAD */				{2, 1},	
		/* 3 MANUAL-BAD */				{3, 4},  
		/* 4 AUTO-MARGINAL */			{4, 2},	
		/* 5 MANUAL-MARGINAL */			{5, 5},
		/* 6 EOL */						{-1, -1}	// eol
	};
	
	for (int i=0; map[i].fm8 != -1; i++)
	{
		if (map[i].fma == classA)
			class8 = map[i].fm8;
	}

	return class8;
}


unsigned ConvertTo8::responseCodeType(unsigned typeA)
{
	// FMA resp codes are zero-based, FM8 are not
	return typeA + 1;
}

// note a valid fmA op == exchange(3) will return a -1 as exchange is not defined for fm8
unsigned ConvertTo8::operation(unsigned opA)
{
	unsigned op8 = -1;
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
		{0, -1},	// undefined 	
		{1, 1},		// read
		{2, 2},		// write
		{3, 0},		// command
		{-1, -1}	// eol
	};
	
	for (int i=1; map[i].fm8 != -1; i++)
	{
		if (map[i].fma == opA)
			op8 = map[i].fm8;
	}

	return op8;
}

unsigned ConvertTo8::style(unsigned styleA)
{
	// FMA resp codes are zero-based, FM8 are not
	return styleA + 1;
}

unsigned ConvertTo8::timeScale(unsigned typeA)
{
	unsigned type8 = -1;
	typedef struct {int fm8; int fma;} MAP;	
	static MAP map[] = { \
		{1, 1},		// seconds
		{2, 2},		// minutes
		{4, 3},		// hours
		{-1, -1}	// eol
	};
	
	for (int i=0; map[i].fm8 != -1; i++)
	{
		if (map[i].fma == typeA)
			type8 = map[i].fm8;
	}

	return type8;
}

unsigned ConvertTo8::displaySize(unsigned typeA)
{
	return (typeA + 1);	// FM8 is 1-based
}
