/**********************************************************************************************
 *
 * $Workfile: FMA_objectSet.cpp $
 * 21Jul11 - steveh
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_ObjectSet class implementation...the model of One File Object
 *		
 *
 */

#include "FMA_objectSet.h"

FMA_ObjectSet::~FMA_ObjectSet()
{
	for (FMA_pObjectListIT_t oIT = objList.begin(); oIT != objList.end(); ++oIT )
	{
		delete (FMA_Object*)(*oIT);
	}

	objList.clear();
}

/*********************************************************************************************
 *	index_ObjectData   
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		
 ********************************************************************************************/
RETURNCODE FMA_ObjectSet::index_ObjectData(unsigned& size)
{
// ** this doesn't work because the ddef desc hasn't been decoded
//	int i, x = pTopClass->getObjectCount();
//	int i, x;
	FMA_Object *pObj;
	//Read 'em, end by size, not count...we'll verify count later
// **	for ( i = 0; i < x; i++)
	while (size > 0)
	{
		pObj = new FMA_Object(pTopClass, fp, saveBinary);
		if (pObj == NULL)
		{
			return FMx_MEMFAIL;
		}

		pObj->read_Object_Info();// skip-payload

		size -= pObj->obj_size();
		objList.push_back(pObj);
		pObj = NULL;
	}// next object

	if ( size != 0 )
	{
		LOGIT(CLOG_LOG,"Object Read Ending size is %#04x not zero.\n", size);
		return FMx_OPPFAIL;
	}

	return SUCCESS;
}

RETURNCODE FMA_ObjectSet::ExecuteVisitor(FMx_visitor* pVisitor)
{
	RETURNCODE r = FAILURE;	
	FMA_Object* pObj;
currentObjectIndex = 0;

	if (objList.size() == 0)
	{
		return SUCCESS;		// allow reader to continue processing
	} // else nothing

	for (FMA_pObjectListIT_t oIT = objList.begin(); oIT != objList.end(); ++oIT )
	{
		pObj = (FMA_Object*)(*oIT);
currentObjectIndex = pObj->obj_Index;
		r = pObj->ExecuteVisitor(pVisitor);

		if ( r != SUCCESS )
		{
currentObjectIndex = 0;
			return r;
		}
	}//next

	return r;
}

