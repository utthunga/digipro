#############################################################################
#    Copyright (c) 2017 Fluke Corporation. All rights reserved.
#############################################################################

#############################################################################
#    Compiler Options #######################################################
#############################################################################
add_compile_options (${FLUKE_IGNORE_COMPILE_WARNINGS})

#############################################################################
#    include the source files ###############################################
#############################################################################
set (COMMON_SRC	${CMAKE_CURRENT_SOURCE_DIR}/FM8.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA.cpp  
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA_LitStrings.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FM8_LitStrings.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA_Dict.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FM8_Dict.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FM8_objectSet.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA_objectSet.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA_Symbol.cpp 
    ${CMAKE_CURRENT_SOURCE_DIR}/FMA_Support_rows.cpp)

#############################################################################
#    include the header files ###############################################
#############################################################################

include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/FMAParser)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/BuiltinLib)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/DDParser)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/DevServices)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/COMMON)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/CrossPlatform)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/Interpreter)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/MEE)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/sdc625Interface)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/APPS/APPsupport/ParserInfc)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/ddservices/sdc-625/CommonClasses/SoftFloat)
include_directories(${CMAKE_PROSTAR_SRC_DIR}/stack/hart/include)

#############################################################################
#    library definition #####################################################
#############################################################################
add_library(FMAParser OBJECT ${COMMON_SRC})

