/**********************************************************************************************
 *
 * $Workfile: FM8_Support_tables.cpp $
 * 19mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_handler support classes for tables
 *		
 *
 */

#include "FMx.h"
#include "FMx_Support_primatives.h"
#include "FM8_meta.h"
#include "FM8_Support_Tables.h"
#include "v_N_v+Debug.h"
#include <assert.h>

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );//global in fma_reader.cpp
extern string filter2print(const wstring& src); //in FMx_Support_primatives.cpp

/* ==========================================================================================*/

int FM8_Table::parse(UCHAR** pp_Location, INT& remainingSize) 
{
	return 0;
}

int FM8_Table::out(unsigned offset)
{
	return 0;
}

int FM8_Table::reconcileOut(unsigned offset)
{
	return 0;
}

int FM8_DictTable::parse(unsigned char** pp_Location, int& remainingSize)
{
	dictString_entry_s locDictItem;
	UINT64             length;
	int                sLen = 8, cnt = 0, expected;
	unsigned           strLen;
	wstring            strDev;
	unsigned		   offset = 0;

	parseInteger(sLen, pp_Location, remainingSize, length);
	expected = (int)length;

	while (remainingSize > 0 && cnt < expected )
	{
		parseInteger(sLen,pp_Location, remainingSize, length);
		locDictItem.number = (int)length;

		parseInteger(sLen, pp_Location, remainingSize, length);
		strLen = (unsigned)length;
		parseWideString(pp_Location, remainingSize, strLen, offset, locDictItem.name);

		parseInteger(sLen, pp_Location, remainingSize, length);
		strLen = (unsigned)length;
		parseWideString(pp_Location, remainingSize, strLen, offset, locDictItem.actualStr);

		push_back(locDictItem);
		cnt++;
	}//wend
	if (remainingSize != 0 )
	{
		LOGIT(CERR_LOG,"Error: %d left instead of zero.\n", remainingSize);
	}
	if (cnt != expected )
	{
	LOGIT(CERR_LOG,"Error: Expected %d dictionary strings but got %d.\n", expected,cnt);
	}
	return SUCCESS;
}

int FM8_DictTable::out(unsigned offset, UCHAR* pPayload, unsigned payldLength)
{
	unsigned rcnt = size();
	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,
		" ------->Dictionary Device Table<------(%d rows, %d parsed) %#04x payload size\n",
			rcnt, rcnt, payldLength);	
		outHex(pPayload, offset, payldLength );// hex dump of table
	}

	LOGIT(COUT_LOG,
	"- * - * - * -> Dictionary Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");

	vector<dictString_entry_s>::iterator rowIT;
	dictString_entry_s * pRow;
	string   locAscii, locName; 
	int y = 0;
	for (rowIT = begin(); rowIT != end(); ++rowIT, y++ )
	{
		pRow = &(*rowIT);

		// timj 20jan14 row numbers hurt when comparing output to trace dictionary changes
		if (SUPPRESS_STRINGINDEX == false)
		{
			LOGIT(COUT_LOG,"%4d",y);
		}

		int hi,lo;
		hi = ((pRow->number) >> 16) & 0xffff;
		lo =  (pRow->number)        & 0xffff;

		// we can't print all the characters that are in here
		//LOGIT(COUT_LOG,L" [%4d,%4d] |%s|    :>'%s'.\n",hi,lo,
		//										pRow->name.c_str(), pRow->actualStr.c_str());
		locName = TStr2AStr(pRow->name);	
		locAscii = filter2print(pRow->actualStr);


		if (reconcile && locName.compare("blank")==0)
		{
			int x = 0;
		}
		else
		{
			LOGIT(COUT_LOG," [%4d,%4d] |%s|    :>'%s'.\n",hi,lo,locName.c_str(), locAscii.c_str());
		}
	}
	return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

int FM8_LitStringTable::parse(unsigned char** pp_Location, int& remainingSize)
{
	UINT64             length;
	int                sLen = 8, cnt = 0, expected;
	unsigned           strLen;
	wstring            strDev;
	unsigned		   offset=0;
	
	parseInteger(sLen, pp_Location, remainingSize, length);
	expected = (int)length;
	cnt = 0;

	while (remainingSize > 0 && cnt < expected )
	{
		parseInteger(sLen, pp_Location, remainingSize, length);
		strLen = (unsigned)length;
		parseWideString(pp_Location, remainingSize, strLen, offset, strDev);

		push_back(strDev);
		cnt++;
	}//wend
	if (remainingSize != 0 )
	{
		LOGIT(CERR_LOG,"Error: %d left instead of zero.\n", remainingSize);
	}
	if (cnt != expected )
	{
	LOGIT(CERR_LOG,"Error: Expected %d literal strings but got %d.\n", expected,cnt);
	}
	return SUCCESS;
}

int FM8_LitStringTable::out(unsigned offset, UCHAR* pPayload, unsigned payldLength)
{
	unsigned rcnt = size();

	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,
		" ------->String Device Table<------(%d rows, %d parsed) %#04x payload size\n",
			rcnt,rcnt,payldLength);	
		outHex(pPayload, offset, payldLength );// hex dump of table
	}

	if (!reconcile || (reconcile && rcnt>0))
	{
		LOGIT(COUT_LOG,
			"- * - * - * -> String Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");
	}

	string   locAscii;
	vector<wstring>::iterator rowIT;
	wstring * pRow;
	int y = 0;
	for (rowIT = begin(); rowIT != end(); ++rowIT, y++ )
	{
		pRow = &(*rowIT);		

		// timj 20jan14 row numbers hurt when comparing output to trace lit string changes
		if (SUPPRESS_STRINGINDEX == false)
		{
			LOGIT(COUT_LOG,"%4d",y);
		}
	
		//locAscii = TStr2AStr(*pRow);	stevev - 30apr13 we can't print all the characters...
		locAscii = filter2print(*pRow);

		LOGIT(COUT_LOG,"  '%s'.\n",locAscii.c_str());

	}
	return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

int FM8_CriticalParameterTable::parse(UCHAR** pp_Location, INT& remainingSize)
{
	UINT64				tempLongLong;
	unsigned			id;

	pPayload = *pp_Location;

	payldLength = remainingSize;

	parseInteger(8, pp_Location, remainingSize, tempLongLong);
	rowCount = (unsigned)tempLongLong;
	
	unsigned cnt = 0;
	while (remainingSize > 0 && cnt < rowCount )
	{
		parseInteger(8, pp_Location, remainingSize, tempLongLong);
		id = (unsigned)tempLongLong;
		push_back(id);
		cnt++;
	}

	if (remainingSize != 0 )
	{
		LOGIT(CERR_LOG,"Error: %d left instead of zero.\n", remainingSize);
	}
	if (cnt != rowCount )
	{
		LOGIT(CERR_LOG,"Error: Expected %d resolved fres but got %d.\n", rowCount, cnt);
	}

	return SUCCESS;
}

int FM8_CriticalParameterTable::out(unsigned offset, UCHAR* pPayload, unsigned payldLength)
{
	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,
			" ------->Critical Item Table<------(%d rows, %d parsed) %#04x payload size\n",
			rowCount, rowCount, payldLength);	
		outHex(pPayload, offset, payldLength );// hex dump of table
	}

	if (size() > 0)
	{
			LOGIT(COUT_LOG,
				"- * - * - * -> Critical Item Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");
	
			vector<unsigned>::iterator rowIT;
			unsigned u = 0;
			int y = 0;
			for (rowIT = begin(); rowIT != end(); ++rowIT, y++ )
			{
				u = (*rowIT);	
	
				LOGIT(COUT_LOG,"%4d]  Resolved Reference: SymbolID: 0x%04x\n", y, u);
		}
	}

	return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// private
int FM8_RRptocTable::getRR(RR_ref* refOut, unsigned char ** pRaW, int& sz)
{	
	UINT64				tempLongLong;
	UCHAR               ttag;

	assert( pRaW!=NULL  );
	assert((*pRaW)!=NULL);
	assert(refOut != NULL);
	if (refOut->src) delete refOut->src;
	refOut->src = 0;
	//implicit-tag	Implicit_Tag, :only item-id and via-resolved allowed here
	parseInteger(8, pRaW, sz, tempLongLong);
	ttag = (tempLongLong & 0xff);
	assert((ttag &0x80) == 0 && ttag != 0x7f );// implicit short
	//CHOICE{	<as designated by implicit-tag.implicit-value>
	//	item_id		    [ 0]	INTEGER,	-- symbol number
	if (ttag == 0)	
	{
		parseInteger(8, pRaW, sz, tempLongLong);
		refOut->symNum = (unsigned)tempLongLong;
	}
	else
	//	via-resolved	[48]	SEQUENCE 
	if (ttag == 48)	
	{//			selection	INTEGER,
		parseInteger(8, pRaW, sz, tempLongLong);
		refOut->expr   = (unsigned)tempLongLong;
		refOut->symNum = 0x00000000;
	 //			group-item	RESOLVED_REFERENCE
		refOut->src = new RR_ref;
		getRR(refOut->src, pRaW, sz);
	}
	else // error
	{
		LOGIT(CERR_LOG,"Error: getRR got a %d illegal tag.\n", ttag);
		(*pRaW) -= sz;
		sz = 0;
		return -4;
	}
	return SUCCESS;
}

// private
int FM8_RRptocTable::getCmdDesc(RRcmdTrans* pCmdOut, unsigned char ** pRaw, int& sz)
{
	UINT64	 tempLongLong;
	unsigned idxCnt = 0;
	RRindex  idx;

	assert( pRaw!=NULL  );
	assert((*pRaw)!=NULL);
	assert(pCmdOut != NULL);
	assert(sz > 0);	
		//	subindex				INTEGER,
	parseInteger(8, pRaw, sz, tempLongLong);
	pCmdOut->subIdx = (unsigned)tempLongLong;
		//	command-number			INTEGER,
	parseInteger(8, pRaw, sz, tempLongLong);
	pCmdOut->cmdNum = (unsigned)tempLongLong;
		//	transaction-number		INTEGER,
	parseInteger(8, pRaw, sz, tempLongLong);
	pCmdOut->trnNum = (unsigned)tempLongLong;
		//	weight					INTEGER,
	parseInteger(8, pRaw, sz, tempLongLong);
	pCmdOut->weight = (unsigned)tempLongLong;
		//	number-of-indices		INTEGER,
	parseInteger(8, pRaw, sz, tempLongLong);
	idxCnt = (unsigned)tempLongLong;
		//	SEQUENCE OF SEQUENCE	-- empty if count is zero
	while (idxCnt && sz)
	{	//		index-item-id			INTEGER,
		parseInteger(8, pRaw, sz, tempLongLong);
		idx.indxID = (unsigned)tempLongLong;
		//		index					INTEGER
		parseInteger(8, pRaw, sz, tempLongLong);
		idx.index = (unsigned)tempLongLong;

		pCmdOut->idxList.push_back(idx);
		idxCnt--;
		idx.clear();
	}
	return SUCCESS;
}

int FM8_RRptocTable::parse(UCHAR** pp_Location, INT& remainingSize)
{
	RR_ptoc_row_s rrRow;
	RR_ref        rrRef; // a resolved reference
	RRindex       rrIdx;
	RRcmdTrans    rrCmd;

	UINT64				 tempLongLong;
	unsigned			 cnt, cmdCnt;
	int                  remaining;
	UCHAR*               pData;

	pPayload    = pData     = *pp_Location;
	payldLength = remaining = remainingSize;

	parseInteger(8, &pData, remaining, tempLongLong);
	rowCount = (unsigned)tempLongLong;
	cnt = 0;
	while (remaining > 0 && cnt < rowCount )
	{
	//	element_ref				RESOLVED_REFERENCE,	
		getRR(&(rrRow.param_name), &pData, remaining);

	//	number-of-read-commands		INTEGER,	
		parseInteger(8, &pData, remaining, tempLongLong);
		cmdCnt = (unsigned)tempLongLong;

	//	SEQUENCE OF SEQUENCE	-- empty if count is zero
		while ( cmdCnt && remaining )
		{
			if ( ! getCmdDesc(&rrCmd,&pData, remaining) )// success
			{
				rrRow.redList.push_back(rrCmd);
				rrCmd.clear();
				cmdCnt--;
			}
			else
			{
				LOGIT(CERR_LOG,"Error: getCmdDesc READ failed, exit table.\n");
				return -5;
			}
		}

		//number-of-write-commands	INTEGER,	
		parseInteger(8, &pData, remaining, tempLongLong);
		cmdCnt = (unsigned)tempLongLong;
		//SEQUENCE OF SEQUENCE	-- empty if count is zero
		while ( cmdCnt && remaining )
		{
			if ( ! getCmdDesc(&rrCmd,&pData, remaining) )// success
			{
				rrRow.wrtList.push_back(rrCmd);
				rrCmd.clear();
				cmdCnt--;
			}
			else
			{
				LOGIT(CERR_LOG,"Error: getCmdDesc WRT failed, exit table.\n");
				return -5;
			}
		}
		push_back(rrRow);
		rrRow.clear();
		cnt++;
	}
	if (cnt != rowCount )
	{
		LOGIT(CERR_LOG,"Error: Expected %d resolved RRptocs but got %d.\n", rowCount, cnt);
	}

	if (remaining != 0 )
	{
		LOGIT(CERR_LOG,"Error: %d bytes left in RRptoc instead of zero.\n", remaining);
	}
	remainingSize = remaining;
	*pp_Location = pData;

	return SUCCESS;
}

int FM8_RRptocTable::out(unsigned offset, UCHAR* pPayload, unsigned payldLength)
{
/*
	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,
			" ------->Critical Item Table<------(%d rows, %d parsed) %#04x payload size\n",
			rowCount, rowCount, payldLength);	
		outHex(pPayload, offset, payldLength );// hex dump of table
	}
*/
	if (SUPPRESS_PTOC)
		return 0;

	if ( rowCount != size() )
	{
		LOGIT(COUT_LOG|CERR_LOG,"RRptoc size = %d while count = %d\n",size(),rowCount);
	}

	if (size() > 0)
	{
		LOGIT(COUT_LOG,
			"- * - * - * -> RR PTOC Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");

		vector<RR_ptoc_row_s>::iterator rowIT;
		RR_ptoc_row_s* pRoe;
		int y = 1;

		for (rowIT = begin(); rowIT != end(); ++rowIT )
		{
			pRoe = (RR_ptoc_row_s*)&(*rowIT);

			LOGIT(COUT_LOG,"%4d] ", y);
			pRoe->out(0);
			LOGIT(COUT_LOG,"\n");
			y++;
		}
	}
	else
	{
		LOGIT(COUT_LOG,
			"- * - * - * -> RR PTOC Table is Empty <- * - * - * - * - * - * - * - * - * - * - * -\n");
	}

	return 0;
}

////////////////////////////////////////////////
void RR_ref::out(int sp)
{
	if ( symNum == 0 && src != NULL )
	{
		LOGIT(COUT_LOG,"%s Element number %d of:\n",S_pace(sp),expr);
		src->out(sp+8);
	}
	else
	if ( symNum != 0 && src == NULL )
	{
		LOGIT(COUT_LOG,"%s item 0x%04x:\n",S_pace(sp),symNum);
	}
	else
	if ( symNum != 0 && src != NULL )// both full
	{
		LOGIT(COUT_LOG,"%s R_Reference has 0x%04x Symbol# and a RRef.\n",S_pace(sp),symNum);
		LOGIT(COUT_LOG,"%s      the %d element in:\n",S_pace(sp),expr);
		src->out(sp+8);
	}
	else // both empty
	{
		LOGIT(COUT_LOG," R_Reference has no content.\n");
	}
	//char* S_pace(sp)
}

void RRcmdTrans::out(int sp)
{
	vector<RRindex>::iterator idxIT;
	RRindex* pDex;
	if (subIdx)
	{
		LOGIT(COUT_LOG," Cmd:%4d Trans:%4d with %d wgt and a Subindex of %d\n",
			cmdNum,trnNum,weight,subIdx);
	}
	else // normal
	{
		LOGIT(COUT_LOG," Cmd:%4d Trans:%4d with %d wgt\n",
			cmdNum,trnNum,weight);
	}

	if (idxList.size())
	{
		LOGIT(COUT_LOG,"%s Using:\n", S_pace(sp-3));
		for (idxIT = idxList.begin(); idxIT != idxList.end(); ++ idxIT)
		{
			pDex = &(*idxIT);
			pDex->out(sp);
		}
	}
	else
	{
		LOGIT(COUT_LOG,"%s and Has no indexes Listed.\n",S_pace(sp));
	}
}

void RRindex::out(int sp)
{
	LOGIT(COUT_LOG,"%s Index 0x%04x with value:%3d\n",S_pace(sp),indxID,index);
}


void RR_ptoc_row_s::out(int sp)
{
	param_name.out(sp);
	sp+= 8;

	vector<RRcmdTrans>::iterator cmdIT;
	RRcmdTrans* pCmd;
	int k = 1;

	if (redList.size())
	{
		LOGIT(COUT_LOG,"%s >>>> READ Command List <<<<\n",S_pace(sp));

		for (cmdIT = redList.begin(); cmdIT != redList.end(); ++cmdIT )
		{
			LOGIT(COUT_LOG,"%s %2d} ",S_pace(sp),k);
			pCmd = &(*cmdIT);
			pCmd->out(sp+8);
			k++;
		}
	}
	else
	{
		LOGIT(COUT_LOG,"%s ---- READ Command List is Empty. ----\n",S_pace(sp));
	}

	k = 0;
	if (wrtList.size())
	{
		LOGIT(COUT_LOG,"%s >>>> WRITE Command List <<<<\n",S_pace(sp));

		for (cmdIT = wrtList.begin(); cmdIT != wrtList.end(); ++cmdIT )
		{
			LOGIT(COUT_LOG,"%s %2d} ",S_pace(sp),k);
			pCmd = &(*cmdIT);
			pCmd->out(sp+8);
			k++;
		}
	}
	else
	{
		LOGIT(COUT_LOG,"%s ---- WRITE Command List is Empty. ----\n",S_pace(sp));
	}
}


int FM8_ImageTable::parse(UCHAR** pp_Location, INT& remainingSize)
{
	UINT64				tempLongLong;

	pPayload    = *pp_Location;// base class FM8_Table
	payldLength = remainingSize;

	parseInteger(8, pp_Location, remainingSize, tempLongLong);
	rowCount = (unsigned)tempLongLong;
	
	img_table_elem wrk_elem;

	unsigned cnt = 0;
	while (remainingSize > 0 && cnt < rowCount )
	{//for each element 
		// -- parse the number of languages
		parseInteger(8, pp_Location, remainingSize, tempLongLong);
		wrk_elem.num_langs = (unsigned)tempLongLong;

		if (tempLongLong == 0 )
		{
			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Image table row shows zero languages for image %d\n",cnt);
			return FMx_BAD_PARSE;
		}// else process the image item

		// generate the array
		wrk_elem.imageArray = (img_item_s*)new img_item_s[((size_t)(tempLongLong))];
		if (wrk_elem.imageArray == NULL)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Image table exhausted memory\n");
			return FMx_MEMFAIL;
		}

		// parse the image list to fill the array
		img_item_s *pImageItem;
		// for each language
		for ( unsigned y = 0; y < wrk_elem.num_langs && remainingSize > 0; y++)
		{
			pImageItem = &(wrk_elem.imageArray[y]);
			// get byte string (6)  (define is same as used in tokenizer)
			strncpy((char*)(pImageItem->langCode), (char*)(*pp_Location), CNTRYCDSTRLEN);

			remainingSize  -= CNTRYCDSTRLEN;        /* decrement size */
			(*pp_Location) += CNTRYCDSTRLEN;        /* increment the root pointer */

			// get datapart segment
			//		get 	offset	Long_Offset,
			parseInteger(8, pp_Location, remainingSize, tempLongLong);	
			pImageItem->img_location.offset = (unsigned)tempLongLong;
			
			//		get		size	Unsigned32				
			parseInteger(8, pp_Location, remainingSize, tempLongLong);
			pImageItem->img_location.length = (unsigned)tempLongLong;

		}// next language

		push_back(wrk_elem); // assume a memcpy; the array memory is transfered
		memset(&wrk_elem, 0, sizeof(wrk_elem));// clear for next use
		cnt++;
	}// next image row

	if (remainingSize != 0 )
	{
		LOGIT(CERR_LOG,"Error: %d remaining instead of zero.\n", remainingSize);
	}
	if (cnt != rowCount )
	{
		LOGIT(CERR_LOG,"Error: Expected %d image entries but got %d.\n", rowCount, cnt);
	}

	return SUCCESS;
}

int FM8_ImageTable::out(unsigned offset, UCHAR* pPayload, unsigned payldLength)
{
	CValueVarient lV, oV;
	string        locAscii;
	int retval = SUCCESS;
	
	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,
		" ------->Image Device Table<-----------(%d rows, %d parsed) %#04x payload size\n",
			rowCount, rowCount, payldLength);	
		outHex(pPayload, offset, payldLength );// hex dump of table
	}

LOGIT(COUT_LOG,"- * - * - * -> Image Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");

	vector<img_table_elem>::iterator iteIT;
	img_table_elem *pWrk_elem;
	int rowNumber = 0;
	img_item_s *pImageItem;
	char* pLang;
	unsigned mV,nV;

	for (iteIT = begin(); iteIT != end(); ++iteIT)
	{
		pWrk_elem = &(*iteIT);


		LOGIT(COUT_LOG,"%4d ",rowNumber++);// row numbers are zero based
	
		// push order lang cnt {lang-string,  unsigned 32 offset , unsigned 32 length}
		int t = 0, st = 0;
	
		t = (unsigned)pWrk_elem->num_langs;
		if(t<=0)
		{
			LOGIT(COUT_LOG,"        }}} Error, bad language count.\n");
			continue;// next row
		}
	
		LOGIT(COUT_LOG," IMAGE has %2d languages/images defined\n",t);
		for (int y = 0; y < t; y++)
		{
			pImageItem = &(pWrk_elem->imageArray[y]);

			pLang = (char *)(pImageItem->langCode);

			mV = pImageItem->img_location.offset;
			nV = pImageItem->img_location.length;
			
			LOGIT(COUT_LOG,"            Language:'%6s'   Offset: 0x%06x   Length: 0x%04x\n", 
				  pLang, mV, nV);
		}// next lang
	}// next row

	return SUCCESS;
}

FM8_Table* get_8_TablePtr(FMx* fmx, TableType_t t)
{
	FM8_Table* pTbl = NULL;

	if (t == string_table_type      || 
		t == dictionary_table_type  || 
		t == image_table_type         )// a known device table
	{
		if (fmx->pMetaData == NULL ||
			((FM8_meta*)fmx->pMetaData)->pDevDir == NULL ||
			((FM8_meta*)fmx->pMetaData)->pDevDir->pHandler == NULL )
		{
			return NULL;
		}

		FM8_DevDir_handler* pDDHandler = (FM8_DevDir_handler*)((FM8_meta*)fmx->pMetaData)->pDevDir->pHandler;
		if (t == string_table_type)
		{
			pTbl = &(pDDHandler->literalStringList);
		}
		else
		if (t == dictionary_table_type)
		{
			pTbl = &(pDDHandler->dictStringList);
		}
		else
		if (t == image_table_type)
		{
			pTbl = &(pDDHandler->imageList);
		}
		// we don't support other tables at this time
	}
	else
	if (t == critical_item_table_type   || 
		t == item_RR_to_cmd_table_type   )// a known block table
	{
		if (fmx->pMetaData == NULL ||
			((FM8_meta*)fmx->pMetaData)->pBlockDir == NULL ||
			((FM8_meta*)fmx->pMetaData)->pBlockDir->pHandler == NULL )
		{
			return NULL;
		}
		
		FM8_BlkDir_handler *pBlkHandler = 
			(FM8_BlkDir_handler *)((FM8_meta*)fmx->pMetaData)->pBlockDir->pHandler;

		if (t == critical_item_table_type)
		{
			pTbl = &(pBlkHandler->critParamTable);
		}
		else
		if (t == image_table_type)
		{
			pTbl = &(pBlkHandler->RRptocTable);
		}
		// we don't support other tables at this time
	}

	return pTbl;
}


void img_table_elem::generate(AframeList_t *framelist, unsigned char *pBufferStart)
{
	CValueVarient lV, mV, nV, oV;
	string        locAscii;

	assert(framelist);
	assert(pBufferStart);

	if(num_langs<=0)
	{
		LOGIT(COUT_LOG," Error, bad language count in image.\n");
		return;
	}

	struct img_item_s *pImgItem;
	for (unsigned y = 0; y < num_langs; y++)
	{
		pImgItem = &(imageArray[y]);

		struct imgframe_s frame;
		frame.size   = pImgItem->img_location.length;
		frame.offset = pImgItem->img_location.offset;
		frame.pRawFrame = pBufferStart + frame.offset;
		strcpy(frame.language, (char*)(&(pImgItem->langCode[0]))  );
		framelist->push_back(frame);
	}

	return;
}