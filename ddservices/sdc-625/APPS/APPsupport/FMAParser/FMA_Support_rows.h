/**********************************************************************************************
 *
 * $Workfile: FMA_Support_rows.h $
 * 05oct12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Support_rows.h : These are the data handlers
 *
 * #include "FMA_Support_rows.h"
 *
 */

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _FMA_SUPP_ROWS_H
#define _FMA_SUPP_ROWS_H

#ifdef INC_DEBUG
#pragma message("In FMA_Support_rows.h") 
#endif

#include "FMA_Support_tables.h"
#include "FMA_ResolvedRef.h"
#include "dllapi.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_Support_rows.h") 
#endif


//  NOTE:  FMA_Row  is in FMA_support_tables.h


class FMA_DevRow_ItemInfo : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_ItemInfo();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_String : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_String();
	int parse(FMx_Table *table, unsigned char** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_Dictionary : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_Dictionary();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_Symbol : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_Symbol();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_CommandNumber : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_CommandNumber();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_BlockInfo : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_BlockInfo();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_DevRow_Image : public FMA_Row // hasa vector<CValueVarient>
{
public:
	FMA_DevRow_Image();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
	void generate(AframeList_t *framelist, const unsigned char *pBufferStart);
};

/* * * * PTOC * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
class command_index
{
public://data
	unsigned idx_Sym_number;
	unsigned idx_value;

public://the big three:::
	command_index& operator=(const command_index& src)
	{idx_Sym_number=src.idx_Sym_number;idx_value=src.idx_value;	return *this;};
	command_index(const command_index& s){ operator=(s); };
	~command_index(){};//we own no memory
	// plus two
	command_index(){  clear(); };
	void clear(){ idx_Sym_number=idx_value=0; };

public: // implementation
	int parse(UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};
typedef vector<command_index>	cmdIdxLst_t;
typedef cmdIdxLst_t::iterator   cmdIdxIt_t;

class command_entry
{
public://data
	unsigned cmdNumber;
	unsigned cmdItemInfoIndex;
	unsigned transactionNumber;
	unsigned weight;
	cmdIdxLst_t Indexes;

public://the big three:::
	command_entry& operator=(const command_entry& src)
	{	cmdNumber        = src.cmdNumber;
		cmdItemInfoIndex = src.cmdItemInfoIndex;
		transactionNumber= src.transactionNumber;
		weight           = src.weight;
		Indexes          = src.Indexes;
		return *this;
	};
	command_entry(const command_entry& s){ operator=(s); };
	~command_entry(){ Indexes.clear(); };
	// plus two
	command_entry(){ clear();	};
	void clear(){ cmdNumber=cmdItemInfoIndex=transactionNumber=weight = 0;Indexes.clear();}

public: // implementation
	int parse(UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
	int ptocReconcileOut(int rowNumber);
};
typedef vector<command_entry>	cmdLst_t;
typedef cmdLst_t::iterator      cmdIt_t;

class FMA_DevRow_Item2Cmd : public FMA_Row // hasa vector<CValueVarient>
{
public://data
	resolved_reference  rr;	// the data item these commands are for
	cmdLst_t     rdCmdList;	// readcommand list
	cmdLst_t     wrCmdList; // writcommand list

public:
	FMA_DevRow_Item2Cmd();
	~FMA_DevRow_Item2Cmd();

	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
	int reconcileOut(int rowNumber);
};

/*********************************************************************************************
 *
 * Block Directory Table Rows
 *
 *********************************************************************************************/

class FMA_BlkRow_CriticalItem : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	resolved_reference  rr;	// the data item that is critical

public:
	FMA_BlkRow_CriticalItem();
	~FMA_BlkRow_CriticalItem();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};


class FMA_BlkRow_BlockItem : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	

public:
	FMA_BlkRow_BlockItem();
	~FMA_BlkRow_BlockItem();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};

class FMA_BlkRow_BlockItemName : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	

public:
	FMA_BlkRow_BlockItemName();
	~FMA_BlkRow_BlockItemName();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};

class FMA_BlkRow_Parameter : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	

public:
	FMA_BlkRow_Parameter();
	~FMA_BlkRow_Parameter();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};

class FMA_BlkRow_Relations : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	

public:
	FMA_BlkRow_Relations();
	~FMA_BlkRow_Relations();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
};

class FMA_BlkRow_Update : public FMA_Row // hasa vector<CValueVarient>
{
public:	
	resolved_reference  rr;	// the data item being updated
	

public:
	FMA_BlkRow_Update();
	FMA_BlkRow_Update(const FMA_BlkRow_Update& s);
	~FMA_BlkRow_Update();
	int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
	//int parse(UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);

	FMA_BlkRow_Update& operator=(const FMA_BlkRow_Update& s);
};



#endif //_FMA_SUPP_ROWS_H