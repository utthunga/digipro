#ifndef _FM8_ATTRIBUTECLASS_
#define _FM8_ATTRIBUTECLASS_

#include "FMx_AttributeClass.h"
#include "FM8_Defs.h"
#include "FM8_Attributes_Ref&Expr.h"
#include "FM8_conditional.h"
#include "FM8_Attributes.h"

#include "FM8_Attributes\faCByteString8.h"
#include "FM8_Attributes\asCasciiStr8.h"
#include "FM8_Attributes\asCdisplaySize8.h"
#include "FM8_Attributes\asCintegerConst8.h"
#include "FM8_Attributes\asCwaveType8.h"
#include "FM8_Attributes\asCtimeScaleSpec8.h"
#include "FM8_Attributes\asCclassSpec8.h"
#include "FM8_Attributes\asCchartTypeSpec8.h"
#include "FM8_Attributes\asCaxisScalSpec8.h"
#include "FM8_Attributes\asChandlingSpec8.h"
#include "FM8_Attributes\asCintegerSpec8.h"
#include "FM8_Attributes\faCMember8.h"
#include "FM8_Attributes\icCoperation8.h"
#include "FM8_Attributes\asCorientSpec8.h"
#include "FM8_Attributes\asClineTypeSpec8.h"
#include "FM8_Attributes\icCmethType8.h"
#include "FM8_Attributes\icCstyle8.h"
#include "FM8_Attributes\asCstrSpec8.h"
#include "FM8_Attributes\asCvarType8.h"
#include "FM8_Attributes\asCnotHart8.h"
#include "FM8_Attributes\asCrefSpec8.h"
#include "FM8_Attributes\asCrefListSpec8.h"
#include "FM8_Attributes\asCactionListSpec8.h"
#include "FM8_Attributes\asCminmaxSpec8.h"
#include "FM8_Attributes\asCmenuItmSpec8.h"
#include "FM8_Attributes\asCmethParams8.h"
#include "FM8_Attributes\asCvectorSpec8.h"
#include "FM8_Attributes\asCexprSpec8.h"
#include "FM8_Attributes\asCenumLstSpec8.h"
#include "FM8_Attributes\asCelementsSpec8.h"
#include "FM8_Attributes\asCunknownType8.h"
#include "FM8_Attributes\asCreserved8.h"
#include "FM8_Attributes\asCattrRefOnly8.h"
#include "FM8_Attributes\asCboolSpec8.h"
#include "FM8_Attributes\asCmemberLst8.h"
#include "FM8_Attributes\asCrespCodesSpec8.h"
#include "FM8_Attributes\asTransactionLst8.h"
#include "FM8_Attributes\asCuuid8.h"
#include "FM8_Attributes\asCdefaultValuesList8.h"
#include "FM8_Attributes\asCRefreshLst8.h"
#include "FM8_Attributes\asCUnitRelationLst8.h"
#include "FM8_Attributes\asCoperationSpec8.h"
#include "FM8_Attributes\asCmethDef8.h"
#include "FM8_Attributes\asCscopeSpec8.h"

class asCitemInfo8 : public asCitemInfo
{
public: // no reason to be private about it 
	string	symbolName;
	int		len;

public:
	asCitemInfo8(int tag) : asCitemInfo(tag) {};

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{
		RETURNCODE  ret = SUCCESS;
		UCHAR* pStore;
		unsigned tagValue, tagLength;
		UINT64 tempLongLong;
		
		exitValues_t holdExit; 
		pushExitValues(holdExit, pData, length, offset);

		// explicit
		ret = parseExpectedTag(pData, length, offset);// conditioned on expected tag exists
		if (ret != SUCCESS)
		{
			return ret;
		}

		// ascii-string
		// implicit tag // ReturnLen is zero (with no error) on an implicit tag
		ret = parseTag(pData, length,   tagValue,   tagLength, offset);

		if (tagLength > 0)
		{
			LOGIT(CERR_LOG,"Error: asciiStr attribute with should have implicit tag.\n)");
			popExitValues(holdExit, pData, length, offset);
			return -1;
		}

		if (tagValue != NAME_STR_TAG)// 57
		{
			LOGIT(CERR_LOG,"Error: asciiStr attribute ASCII string explicit tag is not 57.\n)");
			popExitValues(holdExit, pData, length, offset);
			return -2;
		}

		// string length
		if (parseInteger(4, pData, length, tempLongLong, offset) != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Unsigned Long attribute error in size extraction.\n)");
			popExitValues(holdExit, pData, length, offset);
			return -3;
		}
		else
		{
			len = (unsigned long)tempLongLong;
		}

		// the string
		ret = parseByteString(pData, length, len, offset, &pStore);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Item Info ASCII String has an error.\n)");
			return ret;
		}

		symbolName = (char *) pStore;
		offset += len;

		// that's all we care about for now
		popExitValues(holdExit, pData, length, offset);
		return 0;
	}

	virtual int out()
	{
		if (! reconcile)
		{
			LOGIT(COUT_LOG,"       Symbol Name: %s\n", symbolName.c_str()); 
		}
		FMx_Attribute::out();

		return 0;
	}
	virtual aCattrBase* generatePrototype()
	{
		aCdebugInfo* attr = new aCdebugInfo;
		if ( !attr )
			return NULL;
		attr->symbolName = symbolName;

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
	virtual bool inhibitOut() { return (reconcile) ? true : false; }
};

#endif //_FM8_ATTRIBUTECLASS_