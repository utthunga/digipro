/**********************************************************************************************
 *
 * $Workfile: FMA_Attributes.cpp $
 * 20mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Attribute classes implementations
 *		
 *
 */

#include <memory.h>
#include "logging.h"
#include "endian.h"
#include "AttributeString.h"

#include "FMx_general.h"
#include "FMA_AttributeClass.h"

#include "FMA_defs.h"
#include "FMx_Base.h"
#include "FMA_Attributes.h"
#include "FMx_Support_primatives.h"
#include "v_N_v+Debug.h"

stringRefType_t convert8_2_10[6] = CONVERT_STR_TYPE;
// usage:  version10Type = convert8_2_10[version8type];

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );

extern unsigned currentItemSymbolID;

char fmaTypeStrings[(int)at_invalid_value][FMAATTRTYPMAXLEN] = { FMAATTRTYPSTRINGS };

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute list class
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
FMA_AttributeList::~FMA_AttributeList() // has to destroy all the attributes
{
	FMx_Attribute* pAttr = NULL;
	fmaAttrIT mIT;

	for (mIT = begin(); mIT != end(); ++mIT)
	{
		pAttr = *mIT;
		pAttr->destroy();
		delete pAttr;
	}// next

	clear();
}

int FMA_AttributeList::	// caller must assume all bytes are used
evaluateAttributePayload(UCHAR* pData, int length, unsigned offset, unsigned item_type, UCHAR count)
{
	int status = SUCCESS;

	FMx_Attribute* pAttr = NULL;
	UINT64 tempLongLong = 0;
	UCHAR localAttrType = 0;
	unsigned localSize  = 0;
	unsigned entryOffset= 0, entryLen = 0;
	UCHAR*   entryPtr   = NULL;
	UCHAR*   calcdPtr   = NULL;
	int      calcdLen   = 0;
	unsigned calcdOff   = 0;
	int      wrkingLen  = 0;

	attr_count = count;
	while( count > 0 && length > 1 )
	{
		entryOffset   = offset;
		entryPtr      = pData;

		// read type
		localAttrType = *pData;
		pData++;  offset++; length--;

		// read size
		entryLen = length;  // used to calculate length of length
		status = parseInteger(4, &pData, length, tempLongLong, offset);		
		if (status != SUCCESS)
		{// we're out of sync and have to leave
			return status;
		}
		localSize = (unsigned)tempLongLong;

		if (localSize > (unsigned)length) // error, can't happen
		{
			LOGIT(CERR_LOG,
				"Error: [IDX:%4u] attribute size (%u) longer  than remaining length (%d)\n",
				objectIndex,localSize, length);
			localSize = length;
		}

		calcdPtr = pData  + localSize;
		calcdLen = length - localSize;
		calcdOff = offset + localSize;
		wrkingLen= localSize;

		pAttr = createAttr((attr_t)localAttrType);
		if (pAttr == NULL)
		{// unsupported or unknown
			pData  = calcdPtr;
			offset = calcdOff;
			length = calcdLen;
		}
		else
		{
			pAttr->startOffset = entryOffset; // offset of the attrType
			pAttr->length      = localSize;   // attribute itself
			pAttr->lenOfLength = entryLen - length;
			pAttr->pBytes      = entryPtr;
			pAttr->origType    = localAttrType;
			pAttr->attrType    = conversionAto8(map_AttrType,item_type,localAttrType);

			status = pAttr->evaluateAttribute(&pData, wrkingLen, offset);

			if (status != SUCCESS)
			{
				if ( status != FMx_UNSUPPORTED)
				{
					if (((int)pAttr->attrType) > 0)												
						LOGIT(CERR_LOG,"       %s Attribute @ 0x%08x failed to evaluate.\n", 
						AttributeString::get8(pAttr->attrType>>8, pAttr->attrType&0xff), offset);
					else
						LOGIT(CERR_LOG,"       Attribute %d.%d @ 0x%08x failed to evaluate.\n", 
						item_type, pAttr->origType, offset);
				}

				pAttr->status = status;// save for debugging
				pData  = calcdPtr;
				offset = calcdOff;
				length = calcdLen;

				status = 0;// allow it to continue after we leave
			}
			else
			{
				if (wrkingLen != 0)
				{					
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd length mismatch.(%d)\n",
					entryOffset,wrkingLen);
				}
				length = calcdLen;  // unlike offset and pData, length is not passed to the evaluateAttribute method.
									// it must be updated manually, whether successful or not
				if (offset != calcdOff )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd offset mismatch.\n",
					entryOffset);
					offset = calcdOff;
				}
				if ( pData != calcdPtr )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd pointer mismatch.\n",
					entryOffset);
					pData  = calcdPtr;
				}
			}

			push_back(pAttr); // save even the bad ones
			pAttr = NULL;
		}

		count--;
	}//wend count > 0
#ifdef _DEBUG
	if ( length != 0 || count != 0 )
	{
		LOGIT(CERR_LOG,"       Attribute length mismatch: Len; %d Cnt:%d\n",length,count);
	}
	remainingLen = length;
	remainingCnt = count;
#endif
	return status;
}

int FMA_AttributeList::out (void) // payload data to stdout	
{
	FMx_Attribute* pAttr = NULL;
	

	int count = attr_count;

	fmaAttrIT mIT;
	for( mIT = begin(); mIT != end(); ++ mIT )
	{
		pAttr = *mIT;
		if (pAttr  &&  pAttr->inhibitOut())
		{
			count--;	// don't count inhibited attrs
		}
	}

	LOGIT(COUT_LOG,"   Item's Attr Count: 0x%04x (%u)\n\n", count, count);

	if (reconcile)// we don't need to sort unless we want 'em to look the same
	{
		sort( begin(), end(), attrTypeLess);
	}

	for( mIT = begin(); mIT != end(); ++ mIT )
	{
		pAttr = *mIT;
		if (pAttr)
		{
			if (pAttr->inhibitOut() == false)
			{
				LOGIT(COUT_LOG,
				"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
				if ( ! reconcile &&  !maintenance)
				{
					outHex(pAttr->pBytes, pAttr->startOffset, pAttr->length+pAttr->lenOfLength+1);// hex of this attribute
				}
				if (pAttr->attrType >= ag_Last_Generic_Attr_Type)
				{
					// in this situation, the FMA attr type does not map directly to the FM8 type.
					// so use the original type with the FMA type strings
					int fmaType = pAttr->origType;
					if ( reconcile )
					{
						LOGIT(COUT_LOG," Attribute: %s\n\n", fmaTypeStrings[fmaType]);
					}
					else // not reconcile
					if (fmaType >= 0)
					{
						LOGIT(COUT_LOG," Attribute: 0x%02x (%u) %s\n\n",fmaType,fmaType,
							fmaTypeStrings[fmaType]);
					}
				}
				

				if (pAttr->attrType == ag_Wave_x_values)
				{
					LOGIT(COUT_LOG," Attribute: X Values\n\n");
					if (reconcile)
						LOGIT(COUT_LOG,"    Conditional   1: "); // fma is a asCrefListA (not spec) 
																//while fm8 is a asCrefListSpec8 
																// simply mimic the Conditional 1: for 8-A reconciliation
				}

				if (pAttr->attrType == ag_Wave_y_values)
				{
					LOGIT(COUT_LOG," Attribute: Y Values\n\n");
					if (reconcile)
						LOGIT(COUT_LOG,"    Conditional   1: ");	// see above [6/24/2015 timj]
				}

				if (pAttr->attrType == ag_Wave_key_points_x_values)
				{
					LOGIT(COUT_LOG," Attribute: Key X Values\n\n");
					if (reconcile)
						LOGIT(COUT_LOG,"    Conditional   1: ");	// see above [6/24/2015 timj]
				}

				if (pAttr->attrType == ag_Wave_key_points_y_values)
				{
					LOGIT(COUT_LOG," Attribute: Key Y Values\n\n");
					if (reconcile)
						LOGIT(COUT_LOG,"    Conditional   1: ");	// see above [6/24/2015 timj]
				}

				pAttr->out();
				
			}
		}
	}// next
	return 0;
}

// factory method for attributes
FMx_Attribute* FMA_AttributeList::createAttr(attr_t attr_type) 
{
	FMx_Attribute* pAttrClass = NULL;

	switch (attr_type)
	{
		case at_item_information:			pAttrClass = new	asCitemInfoA(50);		break;
		case at_label:						pAttrClass = new	asCstrSpecA(0);			break;
		case at_help:						pAttrClass = new	asCstrSpecA(0);			break;
		case at_validity:					pAttrClass = new	asCboolSpecA(0);		break;
		case at_members:					pAttrClass = new	asCmemberLstA(0);		break;
		case at_handling:					pAttrClass = new	asChandlingSpecA(0);	break;
		case at_response_codes:				pAttrClass = new	asCrespCodesSpecA(0);	break;
		case at_height:						pAttrClass = new	asCdisplaySizeA(0);		break;
		case at_post_edit_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_pre_edit_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_refresh_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_relation_update_list:		pAttrClass = new	asCrefListSpecA(0);		break;
		case at_relation_watch_list:		pAttrClass = new	asCrefListSpecA(0);		break;
		case at_relation_watch_variable:	pAttrClass = new	asCrefSpecA(0, true, true);			break;
		case at_width:						pAttrClass = new	asCdisplaySizeA(0);		break;
		case at_access:						pAttrClass = new	asCnotHartA(0);			break;
		case at_class:						pAttrClass = new	asCclassSpecA(0);		break;
		case at_constant_unit:				pAttrClass = new	asCstrSpecA(0);			break;
		case at_cycle_time:					pAttrClass = new	asCexprSpecA(0);		break;
		case at_emphasis:					pAttrClass = new	asCboolSpecA(0);		break;
		case at_exit_actions:				pAttrClass = new	asCactionListSpecA(0);	break;
		case at_identity:					pAttrClass = new	asCasciiStrA(0);		break;
		case at_init_actions:				pAttrClass = new	asCactionListSpecA(0);	break;
		case at_line_color:					pAttrClass = new	asCexprSpecA(0);		break;
		case at_line_type:					pAttrClass = new	asClineTypeSpecA(0);	break;
		case at_max_value:					pAttrClass = new	asCminmaxSpecA(0);	break;
		case at_min_value:					pAttrClass = new	asCminmaxSpecA(0);	break;
		case at_number:						pAttrClass = new	asCintegerConstA(0);	pAttrClass->attname = "CmdNumber"; break;
		case at_post_read_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_post_write_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_pre_read_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_pre_write_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_type_definition:			pAttrClass = new	asCrefSpecA(0, false, true);		break;
		case at_y_axis:						pAttrClass = new	asCrefSpecA(0);			break;
		case at_appinstance:				pAttrClass = new	asCnotHartA(0);			break;
		case at_axis_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_block_b:					pAttrClass = new	asCnotHartA(0);			break;
		case at_block_b_type:				pAttrClass = new	asCnotHartA(0);			break;
		case at_capacity:					pAttrClass = new	asCintegerConstA(0);	break;
		case at_characteristics:			pAttrClass = new	asCnotHartA(0);			break;
		case at_chart_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_chart_type:					pAttrClass = new	asCchartTypeSpecA(0);	break;
		case at_charts:						pAttrClass = new	asCnotHartA(0);			break;
		case at_collection_items:			pAttrClass = new	asCnotHartA(0);			break;
		case at_reserved44:					pAttrClass = new	asCreservedA(0);		break;
		case at_count:						pAttrClass = new	asCexprSpecA(0);		break;
		case at_default_value:				pAttrClass = new	asCexprSpecA(0);		break;
		case at_default_values:				pAttrClass = new	asCdefaultValuesListA();	break;
		case at_definition:					pAttrClass = new	faCByteStringA();		break;
		case at_display_format:				pAttrClass = new	asCstrSpecA(0);			break;
		case at_display_items:				pAttrClass = new	asCrefListSpecA(0);		break;
		case at_edit_display_items:			pAttrClass = new	asCnotHartA(0);			break;
		case at_edit_format:				pAttrClass = new	asCstrSpecA(0);			break;
		case at_edit_items:					pAttrClass = new	asCrefListSpecA(0);		break;
		case at_elements:					pAttrClass = new	asCelementsSpecA(0);	break;
		case at_enumerations:				pAttrClass = new	asCenumLstSpecA(0);		break;
		case at_file_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_first:						pAttrClass = new	asCattrRefOnlyA(0);		break;
		case at_graph_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_graphs:						pAttrClass = new	asCnotHartA(0);			break;
		case at_grid_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_grids:						pAttrClass = new	asCnotHartA(0);			break;
		case at_image_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_image_table_index:			pAttrClass = new	asCintegerSpecA(0);		break;
		case at_index:						pAttrClass = new	asCnotHartA(0);			break;
		case at_indexed:					pAttrClass = new	asCrefSpecA(0, true);			break;
		case at_initial_value:				pAttrClass = new	asCexprSpecA(0);		break;
		case at_items:						pAttrClass = new	asCmenuItmSpecA(0);		break;
		case at_keypoints_x_values:			pAttrClass = new	asCrefListA();          break;
		case at_keypoints_y_values:			pAttrClass = new	asCrefListA();          break;
		case at_last:						pAttrClass = new	asCattrRefOnlyA(0);		break;
		case at_length:						pAttrClass = new	asCexprSpecA(0);		break;
		case at_link:						pAttrClass = new	asCrefSpecA(0);			break;
		case at_list_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_lists:						pAttrClass = new	asCnotHartA(0);			break;
		case at_local_parameters:			pAttrClass = new	asCnotHartA(0);			break;
		case at_menu_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_menus:						pAttrClass = new	asCnotHartA(0);			break;
		case at_method_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_method_parameters:			pAttrClass = new	asCmethParamsA(0);		break;
		case at_method_type:				pAttrClass = new	icCmethTypeA();			break;
		case at_methods:					pAttrClass = new	asCnotHartA(0);			break;
		case at_number_of_elements:			pAttrClass = new	asCexprA(0);			break;
		case at_number_of_points:			pAttrClass = new	asCexprSpecA(0);		break;
		case at_on_update_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_operation:					pAttrClass = new	icCoperationA(0);		break;
		case at_orientation:				pAttrClass = new	asCorientSpecA(0);		break;
		case at_parameter_lists:			pAttrClass = new	asCnotHartA(0);			break;
		case at_parameters:					pAttrClass = new	asCnotHartA(0);			break;
		case at_post_user_actions:			pAttrClass = new	asCactionListSpecA(0);	break;
		case at_reserved90:					pAttrClass = new	asCreservedA(0);		break;
		case at_uuid:						pAttrClass = new	asCuuidA();			break;
		case at_reference_array_items:		pAttrClass = new	asCnotHartA(0);			break;
		case at_refresh_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_scaling:					pAttrClass = new	asCaxisScalSpecA(0);	break;
		case at_scaling_factor:				pAttrClass = new	asCexprSpecA(0);		break;
		case at_slot:						pAttrClass = new	asCnotHartA(0);			break;
		case at_source_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_style:						pAttrClass = new	icCstyleA();			break;
		case at_sub_slot:					pAttrClass = new	asCnotHartA(0);			break;
		case at_time_format:				pAttrClass = new	asCstrSpecA(0);			break;
		case at_time_scale:					pAttrClass = new	asCtimeScaleSpecA(0);	break;
		case at_transaction:				pAttrClass = new	asTransactionLstA(0);	break;
		case at_unit_items:					pAttrClass = new	asCnotHartA(0);			break;
		case at_variable_type:				pAttrClass = new	asCvarTypeA(0);			break;
		case at_vectors:					pAttrClass = new	asCvectorSpecA(0);		break;
		case at_waveform_items:				pAttrClass = new	asCnotHartA(0);			break;
		case at_waveform_type:				pAttrClass = new	asCwaveTypeA(0);		break;
		case at_write_as_one_items:			pAttrClass = new	asCnotHartA(0);			break;
		case at_reserved109:				pAttrClass = new	asCreservedA(0);		break;
		case at_x_axis:						pAttrClass = new	asCrefSpecA(0);			break;
		case at_x_increment:				pAttrClass = new	asCexprSpecA(0);		break;
		case at_x_initial:					pAttrClass = new	asCexprSpecA(0);		break;
		case at_x_values:					pAttrClass = new	asCrefListA();          break;
		case at_y_values:					pAttrClass = new	asCrefListA();          break;
		case at_view_min:					pAttrClass = new	asCattrRefOnlyA(0);		break;
		case at_view_max:					pAttrClass = new	asCattrRefOnlyA(0);		break;
		case at_min_axis:					pAttrClass = new	asCexprSpecA(0);		break;
		case at_max_axis:					pAttrClass = new	asCexprSpecA(0);		break;
		case at_default_reference:			pAttrClass = new	asCnotHartA(0);			break;
		case at_addressing:					pAttrClass = new	asCnotHartA(0);			break;
		case at_can_delete:					pAttrClass = new	asCnotHartA(0);			break;
		case at_check_configuration:		pAttrClass = new	asCnotHartA(0);			break;
		case at_classification:				pAttrClass = new	asCnotHartA(0);			break;
		case at_component_parent:			pAttrClass = new	asCnotHartA(0);			break;
		case at_component_path:				pAttrClass = new	asCnotHartA(0);			break;
		case at_component_relations:		pAttrClass = new	asCnotHartA(0);			break;
		case at_components:					pAttrClass = new	asCnotHartA(0);			break;
		case at_declaration:				pAttrClass = new	asCnotHartA(0);			break;
		case at_detect:						pAttrClass = new	asCnotHartA(0);			break;
		case at_device_revision:			pAttrClass = new	asCnotHartA(0);			break;
		case at_device_type:				pAttrClass = new	asCnotHartA(0);			break;
		case at_edd:						pAttrClass = new	asCnotHartA(0);			break;
		case at_manufacturer:				pAttrClass = new	asCnotHartA(0);			break;
		case at_maximum_number:				pAttrClass = new	asCnotHartA(0);			break;
		case at_minimum_number:				pAttrClass = new	asCnotHartA(0);			break;
		case at_protocol:					pAttrClass = new	asCnotHartA(0);			break;
		case at_redundancy:					pAttrClass = new	asCnotHartA(0);			break;
		case at_relation_type:				pAttrClass = new	asCnotHartA(0);			break;
		case at_unused03:					pAttrClass = new	asCnotHartA(0);			break;
		case at_scan:						pAttrClass = new	asCnotHartA(0);			break;
		case at_scan_list:					pAttrClass = new	asCnotHartA(0);			break;
		case at_visibility:					pAttrClass = new	asCboolSpecA(0);		break;
		case at_private:					pAttrClass = new	icCBoolA();				break;
		default:							pAttrClass = new	asCunknownTypeA(0);		break;

	}// end switch

	return pAttrClass;
}

