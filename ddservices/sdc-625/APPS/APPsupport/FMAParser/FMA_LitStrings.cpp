/**********************************************************************************************
 *
 * $Workfile: FMA_LitStrings.cpp $
 * 20Jul11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_LitStrings class implemantation...the model of the file
 *		
 *
 */

#include "FMA_LitStrings.h"
#include "FMA_Support_tables.h"
#include "varient.h"
#include "logging.h"
#include "Convert.h"



FMA_LitStrings::FMA_LitStrings()
{
}


FMA_LitStrings::~FMA_LitStrings()
{
}


void FMA_LitStrings::setTheTable( FMx_Table* pTbl)
{
	pTable = pTbl;
	if (pTbl == NULL)
	{
		LOGIT(CERR_LOG,L"FMA_LitStrings::setTheTable passed a NULL table.\n");
		return;
	}
	FMA_Table* pT = dynamic_cast<FMA_Table*>( pTbl );

	// verify
#ifdef _DEBUG
	if ( pT->tableType != string_table_type)
	{
		LOGIT(CERR_LOG,L"FMA_LitStrings::setTheTable passed a non-literal string table.\n");
		return;
	}
#endif

	vector<FMA_Row *>    ::iterator rowIT;
	FMA_Row * pRow;

	vector<CValueVarient>::iterator vLIT;
	CValueVarient  nV;
	wstring        locStr;
	unsigned       rowCnt = 0;

	for (rowIT = pT->begin(); rowIT != pT->end(); ++rowIT )
	{
		pRow = *rowIT;
		rowCnt++;
#ifdef _DEBUG
		assert( pRow->tt == string_table_type);
#endif
		if ( pRow->size() <= 0 )
		{
			LOGIT(CLOG_LOG,"setLiteralStringTable: Row %4d is empty.\n",rowCnt);
			continue;
		}
		vLIT = pRow->begin();
		nV = *vLIT;  // actual string..the only row

		locStr = (std::wstring)nV;

#ifdef TOKENIZER
		/*
		 * In tokenizing, we want all the full string with all
		 * languages, as these will be stored in the final
		 * encoded DD
		 */
#else
		// localize the string to the preferred lang
		// and strip the language code
		ConvertToString::filterString(locStr);
#endif

		addStr(locStr);

		if (! saveBinary)
		{// empty the original (DD) string
			(*vLIT).clear();// prevents having a bunch of string copies
		}
		vLIT++;
#ifdef _DEBUG
		if ( vLIT != pRow->end() )
		{// something is wrong
			LOGIT(CERR_LOG," End of LitString row is not at the end(). (%d)\n",pRow->size());
		}
#endif		
	}// next row
}


int FMA_LitStrings::retrieveStr(int  str,   wstring& retVal)
{
	int rtn = FAILURE;

#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	if (str >= (int)dictStrList.size() || str < 0 )
	{
		retVal = L"****FMA_LitStrings did not find String.\n";
	}
	else
	{
		retVal = dictStrList[str].StringItself;
		rtn = SUCCESS;
	}
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif

	return rtn;
}
