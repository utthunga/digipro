/**********************************************************************************************
 *
 * $Workfile: FMx_Table.h $
 * 10Jan13 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_Table.h : base class for A and 8 tables so we can treat them the same w/ same infc
 *
 * #include "FMx_Table.h"
 *
 */

#ifndef _FMX_TABLE_H
#define _FMX_TABLE_H

#ifdef INC_DEBUG
#pragma message("In FMx_Table.h") 
#endif
#include "FMx_general.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMx_Table.h") 
#endif


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 Types
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// from spec - all defined table types
typedef enum TableType_e
{	
	string_table_type,						//( 0), FM8-string_tbl_idx, // 1
	dictionary_table_type,					//( 1), FM8-dictionary_idx, // 2
	image_table_type,						//( 2), FM8-image_tbl_idx   // 4
	item_to_cmd_table_type,					//( 3), FM8-cmd_table_idx,  // 3 param_cmd_idx,// 5
	symbol_table_type, 						//( 4),
	item_info_table_type,					//( 5), FM8-item_table_idx,	// 0
	block_info_table_type,					//( 6),
	command_number_table_type,				//( 7),
	item_RR_to_cmd_table_type,				//( 8), 
	reserved_future_device_2,				//( 9),

	/* * Block table types * */
	critical_item_table_type,				//(10), FM8-critical_idx    // 6
	block_item_table_type,					//(11), FM8-blk_item_idx,	// 0
	block_item_name_table_type,				//(12), FM8-blk_item_nm_idx,// 1
	parameter_table_type,					//(13), FM8-parameter_idx,  // 2 
	parameter_member_table_type, 			//(14),
	parameter_member_name_table_type,		//(15),
	parameter_element_table_type,			//(16),
	parameter_list_table_type,				//(17),
	parameter_list_member_table_type,		//(18),
	parameter_list_member_name_table_type,	//(19),
	characteristic_table_type,				//(20),
	characteristic_member_name_table_type,	//(21),
	relation_table_type,					//(22), FM8-relation_idx,	// 3
	update_table_type						//(23)  FM8-update_idx,     // 4

}/*def*/ TableType_t;

//usage: char tableTypeStrings[TBLTYPESTRCNT][TBLTYPEMAXLEN] = {TBLTYPESTRINGS};
#define TBLTYPESTRCNT  24
#define TBLTYPEMAXLEN  27	/* maximum string length */
#define TBLTYPESTRINGS \
	{"String"},\
	{"Dictionary"},\
	{"Image"},\
	{"Item2Command"},\
	{"Symbol"},\
	{"Item Info"},\
	{"Block Info"},\
	{"Command Number"},\
	{"Reserved 01"},\
	{"Reserved 02"},\
	\
	{"Critical Item"},\
	{"Block-Item"},\
	{"Block-Item-Name"},\
	{"Parameter"},\
	{"Parameter-Member"},\
	{"Parameter-Member-Name"},\
	{"Parameter-Element"},\
	{"ParameterList"},\
	{"ParameterList-Member"},\
	{"ParameterList-Member-Name"},\
	{"Characteristic"},\
	{"Characteristic-Member-Name"},\
	{"Relation"},\
	{"Update"}

extern char tableTypeStrings[TBLTYPESTRCNT][TBLTYPEMAXLEN];

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 Defines
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#define DEV_TBL_CNT 8


#define string_table	(deviceTables[string_table_type])
#define dict_table		(deviceTables[dictionary_table_type])
#define image_table		(deviceTables[image_table_type])
#define item2cmd_table	(deviceTables[item_to_cmd_table_type])
#define symbol_table    (deviceTables[symbol_table_type])
#define item_Info_table	(deviceTables[item_info_table_type])
#define blockInfo_table	(deviceTables[block_info_table_type])
#define command_table	(deviceTables[command_number_table_type])


#define BLK_DIR_TBL_CNT_A 24 /* * >>>>>>> first 10 are not used */    // TODO: why redef'ed to another value

#define criticalItem_Table			(blockTables[critical_item_table_type])
#define block_item_Table			(blockTables[block_item_table_type])
#define block_item_nm_Table			(blockTables[block_item_name_table_type])
#define parameter_Table				(blockTables[parameter_table_type])
#define parameter_mem_Table			(blockTables[parameter_member_table_type])
#define parameter_mem_nm_tTable		(blockTables[parameter_member_name_table_type])
#define parameter_elem_Table		(blockTables[parameter_element_table_type])
#define parameter_list_Table		(blockTables[parameter_list_table_type])
#define parameter_list_mem_Table	(blockTables[parameter_list_member_table_type])
#define parameter_list_mem_nm_Table	(blockTables[parameter_list_member_name_table_type])
#define characteristic_Table		(blockTables[characteristic_table_type])
#define characteristic_mem_nm_Table	(blockTables[characteristic_member_name_table_type])
#define relations_Table				(blockTables[relation_table_type])
#define update_Table				(blockTables[update_table_type])


class FMx_Table;
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 Generic, common classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// Row is the base class for all rows in all tables
// column type depends on the table type, defaults to CValueVarient
// all rows are lists of columns 

// note that template classed need to be fully defined in the .h file
template<class COLUMNTYPE> 
class FMx_Row : public vector<COLUMNTYPE>
{
public:	// data
	TableType_t  tt; // the row has to know what kind of table he's in

public:// construct/destruct
	FMx_Row(TableType_t  t):tt(t){};
	FMx_Row(const FMx_Row& s) { operator=(s); };
	virtual ~FMx_Row();

public:// workers
	virtual FMx_Row& operator=(const FMx_Row& s);
//	virtual int parse(unsigned char** pp_Location, int& remainingSize)=0;// aka evaluate
	virtual int parse(FMx_Table *table, unsigned char** pp_Location, int& remainingSize)=0;// aka evaluate
	virtual int out(int rowNumber) = 0;

};

template<class COLUMNTYPE> 
FMx_Row<COLUMNTYPE>
:: ~FMx_Row()
{
	tt = (TableType_t) -1;
	
    typename vector<COLUMNTYPE>::iterator vLIT;
	COLUMNTYPE* pCT;
	for ( vLIT = begin(); vLIT != end(); ++vLIT )
	{
		pCT = &(*vLIT);
		pCT->~COLUMNTYPE();// call column's destructor
	}
	clear();
}


template<class COLUMNTYPE> 
FMx_Row<COLUMNTYPE>& FMx_Row<COLUMNTYPE>
:: operator=(const FMx_Row& s)
{
	clear(); // self to start with an empty list
    typename vector<COLUMNTYPE>::iterator vLIT;
	COLUMNTYPE* pCT;
	COLUMNTYPE  kt; // must have an operator=
	for ( vLIT = begin(); vLIT != end(); ++vLIT )
	{
		pCT = &(*vLIT);
		kt = *pCT;
		push_back(kt);
		kt.clear();
	}

	return *this;
}

/* ==========================================================================================*/

class FMx_Table 
{
public:		
	FMx_Table();  // constructor
	virtual ~FMx_Table();

private:	// data

public:		// data

public:		// workers
	// this has to be overloaded for the A or 8 class for supported
	static bool supported(TableType_t tt);// true if our row decoder is in place

protected:	// helpers

};



#endif	// _FMX_TABLE_H
