#include "FMx_Symbol.h"

FMx_Symbol::FMx_Symbol() : saveBinary(false), pTable(NULL)
{
}

FMx_Symbol::~FMx_Symbol()
{
	pTable = NULL;	// the table is not ours to delete
}


void FMx_Symbol::retrieveStr(unsigned str, string& retVal, int& itmType)
{
	retVal = "retrieveStr by symbol number needed for the symbol table.";
}

void FMx_Symbol::retrieveStrByIndex(unsigned index, string& retVal, int& itmType)
{
	retVal = "retrieveStrByIndex needed for the symbol table.";
}
