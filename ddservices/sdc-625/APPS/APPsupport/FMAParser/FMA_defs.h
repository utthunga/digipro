/**********************************************************************************************
 *
 * $Workfile: FMA_defs.h $
 * 20Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_defs.h : holds definitions that define the FMA encoded file format
 *
 * #include "FMA_defs.h"
 *
 */

#ifndef _FMA_DEFS_H
#define _FMA_DEFS_H

#ifdef INC_DEBUG
#pragma message("FMA_defs.h") 
#endif

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_defs.h") 
#endif

#include "FMx_general.h"

/*********************************************************************************************
 * general definitions
 ********************************************************************************************/
//  FIXED portion of an object
#define OBJFIX_INDEX_SIZE	4
#define OBJFIX_OBJECT_CODE  1
#define OBJFIX_PAYLD_SIZE   4
//                        ____
#define OBJ_FIXED_SIZE      9	/* we're including the payload size too */

#define SIZEOF_DATE         3
/*********************************************************************************************
 * STRUCTURE TAGS
 ********************************************************************************************/
#define STRUCTURE_TAG_EXPRESSION	52
#define STRUCTURE_TAG_BOOL			60
#define STRUCTURE_TAG_ARGUMENT		62
#define STRUCTURE_TAG_STRING		63
#define STRUCTURE_TAG_MEMBER        83
#define STRUCTURE_TAG_MEMBER_LIST   84
#define STRUCTURE_TAG_REFERENCE     89


/*********************************************************************************************
 * 
 ********************************************************************************************/
#define DESC_ITMCNT_OFFSET  0
#define DESC_BLKCNT_OFFSET  4 
#define DESC_FRSTITMIDX_OFFSET  (DESC_BLKCNT_OFFSET + 2) 

/*********************************************************************************************
 * Item Type
 ********************************************************************************************/
typedef enum item_type_e
{
	it_Undefined,	// 0
	it_Variable,
	it_Command,
	it_Menu,
	it_EditDisplay,
	it_Method,		// 5
	it_RefreshRel,
	it_UnitRel,
	it_WAORel,
	it_Ref_Array,
	it_Collection,	//10
	it__BlockB,
	it__BlockA,
	it___Unused1,
	it__Record,
	it_Val_Array,	//15
	it__VarList,
	it__RespCodes,
	it___Unused2,
	it_Member,		// used for symbol table
	it_File,		//20
	it_Chart,
	it_Graph,
	it_Axis,
	it_Waveform,
	it_Source,		//25
	it_List,
	it_Grid,
	it_Image,
	it_Blob,
	it_Plugin,	//30
	it_Template,
	it___Unused3,
	it__Component,
	it__Comp_folder,
	it__Comp_desc,	//35
	it__Comp_Rel,
	it___Unused4
}/*typedef*/ item_type_t;

#define FMAITMTYPECOUNT  (((int)it___Unused4)  + 1)
//usage: char itemType_Str[FMAITMTYPECOUNT][FMAITMTYPMAXLEN] = {FMAITMTYPSTRINGS};
#define FMAITMTYPMAXLEN  22	/* maximum string length */
#define FMAITMTYPSTRINGS \
	{"Undefined   "},\
	{"Variable    "},\
	{"Command     "},\
	{"Menu        "},\
	{"Edit Disp   "},\
	{"Method      "},\
	{"Refresh Rel "},\
	{"Unit Rel    "},\
	{"Write As One"},\
	{"Ref Array   "},\
	{"Collection  "},\
	{"Non-HARTa   "},\
	{"Block typeA "},\
	{"Not-Used    "},\
	{"Non-HARTb   "},\
	{"Value Array "},\
	{"Non-HARTc   "},\
	{"Non-HARTd   "},\
	{"Not-Used    "},\
	{"Member      "},\
	{"File        "},\
	{"Chart       "},\
	{"Graph       "},\
	{"Axis        "},\
	{"Waveform    "},\
	{"Source      "},\
	{"List        "},\
	{"Grid        "},\
	{"Image       "},\
	{"Blob        "},\
	{"Plugin      "},\
	{"Template    "},\
	{"Not-Used    "},\
	{"Non-HARTe   "},\
	{"Non-HARTf   "},\
	{"Non-HARTg   "},\
	{"Non-HARTh   "},\
	{"Not-Used    "}

extern char itemType_Str[FMAITMTYPECOUNT][FMAITMTYPMAXLEN];


/*********************************************************************************************
 * Attribute Type
 ********************************************************************************************/
typedef enum attr_e
{
	at_item_information,		//  0	-- debug info
	at_label,
	at_help,
	at_validity,
	at_members,
	at_handling,				//  5
	at_response_codes,					// hart resonse codes
	at_height,	 
	at_post_edit_actions,	
	at_pre_edit_actions, 	
	at_refresh_actions,			// 10 	
	at_relation_update_list,	
	at_relation_watch_list,
	at_relation_watch_variable, 
	at_width, 	
	at_access,					// 15/////
	at_class, 	
	at_constant_unit,	
	at_cycle_time, 
	at_emphasis,	
	at_exit_actions,			// 20
	at_identity,
	at_init_actions,	 
	at_line_color,	
	at_line_type,
	at_max_value,				// 25
	at_min_value, 
	at_number,						 
	at_post_read_actions, 	
	at_post_write_actions, 	
	at_pre_read_actions, 		// 30
	at_pre_write_actions, 	
	at_type_definition,						// aka  typedef
	at_y_axis, 
	at_appinstance,					 /////
	at_axis_items,				// 35/////
	at_block_b, 					 /////
	at_block_b_type, 				 /////
	at_capacity,
	at_characteristics,				 /////
	at_chart_items,				// 40/////
	at_chart_type,
	at_charts,						 ///// 
	at_collection_items, 			 /////
	at_reserved44,					 /////
	at_count,					// 45
	at_default_value,
	at_default_values, 
	at_definition,
	at_display_format, 
	at_display_items,			// 50		// - part of edit display
	at_edit_display_items,			 /////	
	at_edit_format, 
	at_edit_items,							// - part of edit display
	at_elements,
	at_enumerations,			// 55
	at_file_items,					  ////
	at_first,								// - needed for attribute reference
	at_graph_items,					 /////
	at_graphs,						 ////
	at_grid_items,				// 60////
	at_grids, 						 ////
	at_image_items, 				 ////
	at_image_table_index, 
	at_index,						 ////	//  - Profibus command addressing
	at_indexed,					// 65		//  - the item indexed by the variable
	at_initial_value, 
	at_items, 								//  - aka menu-items specifier
	at_keypoints_x_values,
	at_keypoints_y_values,
	at_last,					// 70		//  - needed for attribute reference
	at_length,
	at_link,
	at_list_items,					 ////
	at_lists,						 ////
	at_local_parameters,		// 75////
	at_menu_items,					 ////	//	- part of block-a (not menu)
	at_menus,						 ////
	at_method_items,				 ////
	at_method_parameters,					//  - signature
	at_method_type,				// 80		//	- aka return type
	at_methods, 					 ////
	at_number_of_elements,
	at_number_of_points,
   	at_on_update_actions,
	at_operation,				// 85
	at_orientation,		
	at_parameter_lists,				 ////
	at_parameters,					 ////	//	- part of block-a
	at_post_user_actions,
	at_reserved90,				// 90////
	at_uuid, 					 ////
	at_reference_array_items,		 ////
	at_refresh_items,				 ////	//	- part of block-a
	at_scaling,								//  - axis scaling
	at_scaling_factor,			// 95
	at_slot,						 //// 
	at_source_items,				 ////	
	at_style, 
	at_sub_slot,  					 //// 
	at_time_format,				//100
	at_time_scale, 
	at_transaction,
	at_unit_items,					 ////
	at_variable_type,
	at_vectors,					//105
	at_waveform_items,				 ////
	at_waveform_type,
	at_write_as_one_items,			 ////	//	- part of block-a
	at_reserved109,
	at_x_axis,					//110
	at_x_increment, 
	at_x_initial, 
	at_x_values,
	at_y_values,
	at_view_min,				//115		//	- needed for attribute reference
	at_view_max,							//	- needed for attribute reference
	at_min_axis,							//	- smallest axis value
	at_max_axis,
	at_default_reference,			 ////	//  - part of Object_Reference item
	at_addressing,				//120////
	at_can_delete,					 ////
	at_check_configuration,			 ////
	at_classification,				 ////
	at_component_parent,			 ////
	at_component_path,			//125////
	at_component_relations, 		 ////
	at_components, 					 ////
	at_declaration, 				 ////
	at_detect,						 ////
	at_device_revision,			//130////
	at_device_type,					 ////
	at_edd, 						 ////
	at_manufacturer, 				 ////
	at_maximum_number, 				 ////
	at_minimum_number, 			//135////
	at_protocol, 					 ////
	at_redundancy, 					 ////
	at_relation_type, 				 ////
	at_unused03,					 ////	// -- was  �required_interface�	
	at_scan,					//140////
	at_scan_list,					 //// 
	at_private,						 //// 
	at_visibility,					 //// 
	at_product_uri,					 //// 
	at_api,						//145//// 
	at_header,						 //// 
	at_write_mode,					 //// 
	at_plugin_items,				 //// 
	at_files,						 //// 
	at_plugins,					//150//// 
	at_initial_values				 //// 
	,at_invalid_value // always last
}/*typedef*/ attr_t;

//usage: char attrType_Str[(int)at_invalid_value][FMAATTRTYPMAXLEN] = {FMAATTRTYPSTRINGS};
#define FMAATTRTYPMAXLEN  20	/* maximum string length */
#define FMAATTRTYPSTRINGS \
	{"Item Information"},\
	{"Label"},\
	{"Help"},\
	{"Validity"},\
	{"Members"},\
	{"Handling"},\
	{"Response Codes"},\
	{"Height"},\
	{"Post Edit Actions"},\
	{"Pre Edit Actions"},\
	{"Refresh Actions"},/* 10 */\
	{"Update List"},\
	{"Watch List"},\
	{"Watch Variable"},\
	{"Width"},\
	{"NH:Access"},\
	{"Class"},\
	{"Constant Unit"},\
	{"Cycle Time"},\
	{"Emphasis"},\
	{"Exit Actions"},/* 20*/\
	{"Identity"},\
	{"Init Actions"},\
	{"Line Color"},\
	{"Line Type"},\
	{"Max-Value"},\
	{"Min-Value"},\
	{"Number"},\
	{"Post Read Actions"},\
	{"Post Write Actions"},\
	{"Pre Read Actions"},/* 30*/\
	{"Pre Write Actions"},\
	{"Type Definition"},\
	{"Axis"},\
	{"NH:AppInstance"},\
	{"NH:AxisItems"},\
	{"NH:BlockB"},\
	{"NH:BlockBType"},\
	{"Capacity"},\
	{"NH:Characteristics"},\
	{"NH:ChartItems"},/* 40*/\
	{"Chart Type"},\
	{"NH:Charts"},\
	{"NH:CollectionItems"},\
	{"--Reserved-44--"},\
	{"Count"},\
	{"Default-Value"},\
	{"Default Values"},\
	{"Definition"},\
	{"Display Format"},\
	{"DisplayItems"},/* 50*/\
	{"NH:EditDisplayItems"},\
	{"Edit Format"},\
	{"EditItems"},\
	{"Elements"},\
	{"Enumerations"},\
	{"NH:FileItems"},\
	{"First"},\
	{"NH:GraphItems"},\
	{"NH:Graphs"},\
	{"NH:GridItems"},/* 60*/\
	{"NH:Grids"},\
	{"NH:ImageItems"},\
	{"Image Table Index"},\
	{"NH:Index"},\
	{"Indexed Item"},\
	{"Initial Value"},\
	{"Items"},/*menuItems*/\
	{"Key X Values"},\
	{"Key Y Values"},\
	{"Last"},/* 70*/\
	{"Length"},\
	{"Link"},\
	{"NH:ListItems"},\
	{"NH:Lists"},\
	{"NH:LocalParameters"},\
	{"NH:MenuItems"},\
	{"NH:Menus"},\
	{"NH:MethodItems"},\
	{"Method Parameters"},\
	{"Method Type"},/* 80*/\
	{"NH:Methods"},\
	{"Number Of Elements"},\
	{"Number of Points"},\
	{"On Update Actions"},\
	{"Operation"},\
	{"Orientation"},\
	{"NH:ParameterLists"},\
	{"NH:Parameters"},\
	{"Post User Action"},\
	{"--Reserved-90--"},/* 90*/\
	{"--Reserved-91--"},\
	{"NH:RefArrayItems"},\
	{"NH:RefreshItems"},\
	{"Scaling"},\
	{"Scaling Factor"},\
	{"NH:Slot"},\
	{"NH:SourceItems"},\
	{"Style"},\
	{"NH:SubSlot"},\
	{"Time Format"},/*100*/\
	{"Time Scale"},\
	{"Transaction"},\
	{"NH:UnitItems"},\
	{"Variable Type"},\
	{"Vectors"},\
	{"NH:WaveformItems"},\
	{"Waveform Type"},\
	{"NH:WAOItems"},\
	{"--Reserved109--"},\
	{"X Axis"},/*110*/\
	{"X Increment"},\
	{"X Initial"},\
	{"X Values"},\
	{"Y Values"},\
	{"View-MIN"},\
	{"View-MAX"},\
	{"MinAxis"},\
	{"MaxAxis"},\
	{"NH:DefaultRef"},\
	{"NH:Addressing"},/*120*/\
	{"NH:CanDelete"},\
	{"NH:ChkConfig"},\
	{"NH:Classification"},\
	{"NH:CompParent"},\
	{"NH:CompPath"},\
	{"NH:CompRelations"},\
	{"NH:Comps"},\
	{"NH:Declaration"},\
	{"NH:Detect"},\
	{"NH:DevRev"},/*130*/\
	{"NH:DevType"},\
	{"NH:EDD"},\
	{"NH:Mfgr"},\
	{"NH:MaxNumb"},\
	{"NH:MinNumb"},\
	{"NH:Protocol"},\
	{"NH:Redund"},\
	{"NH:RelType"},\
	{"--Unused-139 --"},\
	{"NH:Scan"},/*140*/\
	{"NH:ScanList"}

// we're using version 8 types...extern char attrType_Str[ATTR_TYPE_COUNT][FMAATTRTYPMAXLEN];


/*********************************************************************************************
 * Variable Type
 ********************************************************************************************/
typedef enum var_e
{
	vt_no_type,		 // 0///
	vt_undefined,		 ///
	vt_integer,				// INTEGER  size	A & 8
	vt_unsigned,			// INTEGER  size	A & 8
	vt_float,		 // 4						A & 8
	vt_double,		 // 5						A & 8
	vt_enum,				// INTEGER  size	A & 8
	vt_bitEnum,				// INTEGER  size	A & 8
	vt_index,				// INTEGER  size	A & 8
	vt_ascii,				// INTEGER  size	A & 8				
	vt_pkdAscii,	 //10	// INTEGER  size	A & 8				
	vt_password,			// INTEGER  size	A & 8				 
	vt_bit_string,		 ///// INTEGER  size	A & 8				
	vt_date,			 //    HART date		A & 8		 
	vt_time,			 ///					Not HART
	vt_date_and_time,//15///					Not HART
	vt_duration,		 ///					Not HART
	vt_euc,				 ///					Not HART
	vt_octet_string,	 ///// INTEGER  size	Not HART				
	vt_visible_string,	 ///// INTEGER  size	Not HART				
	vt_time_value,	 //20	// INTEGER  size	A & 8
	vt_object_reference  ///					Not HART
}/*typedef*/ var_t;

#define VAR_TYPE_COUNT	22
//usage: char varType_Str[VAR_TYPE_COUNT][FMAVARTYPMAXLEN] = {FMAVARTYPSTRINGS};
#define FMAVARTYPMAXLEN  20	/* maximum string length */
#define FMAVARTYPSTRINGS \
	{"No Type"},\
	{"Undefined"},\
	{"Integer"},\
	{"Unsigned"},\
	{"Float"},\
	{"Double"},\
	{"Enumeration"},\
	{"Bit-Enumeration"},\
	{"Index"},\
	{"ASCII"},\
	{"Packed-ASCII"},\
	{"Password"},\
	{"NH:Bit-String"},\
	{"Date"},\
	{"NH:Time"},\
	{"NH:Date&Time"},\
	{"NH:Duration"},\
	{"NH:EUC"},\
	{"NH:Octet-String"},\
	{"NH:Visible-String"},\
	{"Time-Value"},\
	{"NH:Object-reference"},\

extern char varType_Str[VAR_TYPE_COUNT][FMAVARTYPMAXLEN];


/********************************************************************************************/

typedef enum stringRefType_e
{
	st_Literal,		 // 0///
	st_Dictionary,
	st_Reference,
	st_EnumDescRef,  // 3///
	st_AttributeRef, // not used in version 8
	st_VarDirect,	// 5 // for version 8 type 1, just a symbol number
	st_EnumDirect	// 6 // for version 8 type 2, just a symbol number
}stringRefType_t;

#define STR_TYPE_COUNT	5
#define STR8TYPE_COUNT	6

//usage: char strType_Str[STR_TYPE_COUNT][FMASTRTYPMAXLEN] = {FMASTRTYPSTRINGS};
#define FMASTRTYPMAXLEN  21	/* maximum string length */
#define FMASTRTYPSTRINGS \
	{"Literal String"},\
	{"Dictionary String"},\
	{"String Reference"},\
	{"Enum Description Ref"},\
	{"Attribute Reference"}

extern char strType_Str[STR_TYPE_COUNT][FMASTRTYPMAXLEN];

// usage:   stringRefType_t convert8_2_10[6] = CONVERT_STR_TYPE;
//          version10Type = convert8_2_10[version8type];
#define CONVERT_STR_TYPE {\
	/*0*/ st_Literal,\
	/*1*/ st_VarDirect,\
	/*2*/ st_EnumDirect,\
	/*3*/ st_Dictionary,\
	/*4*/ st_Reference,\
	/*5*/ st_EnumDescRef }


//#include "ddbdefs.h"
//extern	char clauseStrings[8][CLAUSESTRMAXLEN];// from ddbdefs
#ifdef INC_DEBUG
#pragma message("    Exit Include file: FMA_defs.h") 
#endif
#endif //_FMA_DEFS_H