#ifndef _FMA_
#define _FMA_

#include "FMx.h"

class FMA : public FMx
{
public:
	FMA() {}
	virtual ~FMA() {}

public:
	virtual RETURNCODE indexFile(void); // scans the file building pointer indexes, rewinds when done
	virtual RETURNCODE readIn_File(void);// fill payloads from index
	virtual RETURNCODE process_File(void);
	
	virtual void release(void);// loose the payload memory
	virtual void out(void);// dump all the information to a file
	virtual void generate(void);
};

#endif //_FMA_
