/**********************************************************************************************
 *
 * $Workfile: FMA_Support_primatives.cpp $
 * 19mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_handler support classe's support functions
 */

#include "FMA.h"
#include "FMA_Support_primatives.h"
#include "FMA_Support_tables.h"
#include "FMA_meta.h"

FMA_Table* get_A_TablePtr(FMx* fmx, TableType_t t)
{
	FMA_Table* pTbl = NULL;
	if (fmx->pMetaData == NULL ||
		((FMA_meta*)fmx->pMetaData)->pDevTables == NULL ||
		((FMA_meta*)fmx->pMetaData)->pDevTables->pHandler == NULL )
	{
		return NULL;
	}
	if (t < critical_item_table_type)// a device table
	{
		if (fmx->pMetaData == NULL ||
			((FMA_meta*)fmx->pMetaData)->pDevTables == NULL ||
			((FMA_meta*)fmx->pMetaData)->pDevTables->pHandler == NULL )
		{
			return NULL;
		}
		pTbl = ((FMA_DevTables_handler*)((FMA_meta*)fmx->pMetaData)->pDevTables->pHandler)->deviceTables[t];
	}
	else
	{
		if (fmx->pMetaData == NULL ||
			((FMA_meta*)fmx->pMetaData)->pBlockTable == NULL ||
			((FMA_meta*)fmx->pMetaData)->pBlockTable->pHandler == NULL )
		{
			return NULL;
		}

		pTbl = ((FMA_BlkTables_handler*)((FMA_meta*)fmx->pMetaData)->pBlockTable->pHandler)->block_Tables[t];
	}

	return pTbl;
}