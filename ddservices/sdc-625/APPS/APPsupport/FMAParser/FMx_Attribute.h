/**********************************************************************************************
*
* $Workfile: FMx_Attribute_Base.h $
* 26mar12 - stevev
*     Revision, Date and Author are never in these files
*
**********************************************************************************************
* The content of this file is the 
*     Proprietary and Confidential property of the HART Communication Foundation
* Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
**********************************************************************************************
*
* Description:
*		FMx_Attribute_Base.h : This common base class had to separated so we get out of catch22
*
* #include "FMx_Attribute.h"
*
*/

#ifndef _FMX_ATTRIBUTE_
#define _FMX_ATTRIBUTE_

#include "dllapi.h"
#include "FMx_general.h"

// all attribute types are converted to version 8 generic types!!!!
#include "FM8_defs.h"  // TODO: Get rid of this header include
#include "v_N_v+Debug.h"

extern unsigned currentItemSymbolID;

typedef struct exitValues_s
{
	UCHAR*   pExitData;
	int      ExitLen;
	unsigned ExitOff;
}
/*typedef*/ exitValues_t;


inline void pushExitValues(exitValues_t& hldValues, UCHAR** pD, int len, unsigned off)
{	hldValues.pExitData = (*pD)+len; hldValues.ExitLen = 0; hldValues.ExitOff = off+len;}
inline void popExitValues(exitValues_t& hldValues, UCHAR** pD, int& len, unsigned& off)
{	(*pD) = hldValues.pExitData; len = hldValues.ExitLen; off = hldValues.ExitOff;}
// usage:  exitValues_t holdExit; pushExitValues(holdExit, pData, length, offset);
//         popExitValues(holdExit, pData, length, offset);


struct attr_ID_s
{
	UCHAR	attrType;	// aka attribute-tag
	UCHAR   RefType;	// aka RI <reference indicator> 0=immediate, 1=on the heap, 2=external
	// we'll use the base class length 
};

class FMx_Attribute
{	
public: // construct / destruct
	FMx_Attribute(int tag = 0, bool direct = false) : startOffset(0), length(0), lenOfLength(0), 
		pBytes(NULL), attrType(-1), origType(-1), status(0),heapOffset(0), fileOffset(0),
		externalIndex(0), pHeapBytes(NULL), isConditional(false)
	{	
		attrGuid = classGuid++;
		requiredDirect = direct;
		id.attrType = id.RefType = 0xff; 
		expectedTag = tag; 
		attname = NULL;
	};
	// NOTE: default copy constructor will copy everything.Only the debug attrGuid has an issue

	virtual ~FMx_Attribute() {/* attname is always a const string, you can't delete it */
		                      /* if (attname) delete attname; */ };

	FMx_Attribute(FMx_Attribute &src) { operator=(src);};
	FMx_Attribute &operator=(FMx_Attribute &src);

public: // data
	static unsigned	classGuid;		// debugging only
	unsigned		attrGuid;		// every instance gets a guid for debugging only

	unsigned		startOffset;	// from beginning of the file to the attribute type (extension)
	unsigned		length;			// in number of bytes (either extension or heap)
	unsigned		lenOfLength;	// extention attr length with RI == 1
	UCHAR*			pBytes;			// pointer into the data for this attribute (we do not own it)
									// points to the attribute type

	unsigned		attrType;		// aka  attribute_name or generic attributeName
									// actual attribute data will be in derived class
	int             origType;       // A types are converted to 8 types, this is -1 in a version 8
									// it will hold the original value of the attribute for FMA

	int				status;			// the last returned status value (zero on all success)
	attr_ID_s		id;				// only valid for version 8 attributes

	unsigned		heapOffset;		// the 'local-address-offset' <only valid @id.RefType==1>
	unsigned        fileOffset;     // file offset to start of this attr in the heap <refType=1>
	UCHAR*			pHeapBytes; 
	unsigned /* 14jan16 short*/	externalIndex;	// <only valid @id.RefType==2>
	unsigned		expectedTag;
	char			*attname;

	// requiredDirect handles an infrequent situation where an FMA specifier is conditional, but
	// the FM8 specifier is not.  In these cases, the FMA text output must match the FM8 output,
	// so the base class out() method goes right to the payload::out() method.
	// In this case, the underlying type must call FMx_Attribute::out() because the specifier::out()
	// is not called
	bool            requiredDirect;

	bool			isConditional;	// true if the attribute is conditional and not direct

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
	{ 
		LOGIT(CLOG_LOG,"------ evaluateAttribute going to Baseclass. "
							"Type 0x%04x, origType %d, %s\n",attrType,origType,attname);
		return -1; 
	}
	virtual aCattrBase* generatePrototype() 
	{ 
		LOGIT(CLOG_LOG,"+++++ generatePrototype going to Baseclass. "
							"Type 0x%04x, origType %d, %s\n",attrType,origType,attname);
		return NULL; 
	};
	virtual aPayldType* generatePayload(void* pV = NULL) 
	{ 
		LOGIT(CLOG_LOG,"===== generatePayload going to Baseclass. "
							"Type 0x%04x, origType %d, %s\n",attrType,origType,attname);
		return NULL; 
	};
	virtual void* generateElement() 
	{
		LOGIT(CLOG_LOG,"##### generateElement going to Baseclass. "
								"Type 0x%04x, origType %d, %s\n",attrType,origType,attname);
		return NULL; 
	};
	virtual void destroy(void);
	virtual void clear();
	virtual int out(void);

	virtual bool inhibitOut() { return false; }	// in some reconcile situations, it is necessary to inhibit the output of an attr
												// and have it not counted in the attr counts

protected:
	int parseExpectedTag(UCHAR** pData, int& length, unsigned& offset);	// parse expectedTag if nonzero, return 0 for success, -1 for wrong tag, +other errors
	int parseExpectedExplicitTag(UCHAR** pData, int& length, unsigned& offset, unsigned expected, unsigned &tagLength);
	int parseExpectedImplicitTag(UCHAR** pData, int& length, unsigned& offset, unsigned expected);
	int parseUnsigned(UCHAR** pData, int& length, unsigned& offset, unsigned &x);
	int parseUINT64(UCHAR** pData, int& length, unsigned& offset, UINT64 &x);
	aCattrBase* createLongAttribute(ulong attrID);
	aCattrBase* createBitstringAttribute(ulong attrID);
	aCattrBase* createStringAttribute(ulong attrID);
	aCattrBase* createIntWitchAttribute(ulong attrID);
	aCattrBase* createReferenceAttribute(ulong attrID);

	//aCreferenceList* createReferenceList();
};

inline bool attrTypeLess(const FMx_Attribute* a1, const FMx_Attribute* a2)
{
	return a1->attrType < a2->attrType;
};

// functor used to sort attributes by attribute type
struct compareAttributes 
{
	bool operator()(FMx_Attribute *lhs, FMx_Attribute *rhs) { return lhs->attrType  <  rhs->attrType; }
};

#endif //_FMX_ATTRIBUTE_