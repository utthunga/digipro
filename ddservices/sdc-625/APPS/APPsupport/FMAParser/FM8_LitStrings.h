/**********************************************************************************************
 *
 * $Workfile: FM8_LitStrings.h $
 * 29oct12 - stevev-from fma version
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_LitStrings.h : strings and their list class descriptions file
 *
 * #include "FMA_LitStrings.h"
 *
 */

#ifndef _FM8_LITSTRINGS_H
#define _FM8_LITSTRINGS_H

#ifdef INC_DEBUG
#pragma message("In FM8_LitStrings.h") 
#endif

#include "FMx_Dict.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FM8_LitStrings.h") 
#endif


class FM8_LitStrings : public FMx_Dict
{
	// use the string list in the base class....StrList_t		theStrList;

public:		
	FM8_LitStrings();  // constructor
	virtual ~FM8_LitStrings();

private:	// data

public:		// data

public:		// workers
	int retrieveStr(int  str,   wstring& retVal);	
	void setTheTable( FMx_Table* pTbl);

protected:	// helpers

};



#endif	// _FM8_LITSTRINGS_H