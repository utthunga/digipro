/**********************************************************************************************
 *
 * $Workfile: FM8_Attributes_Ref&Expr.cpp $
 * 23mar12 - steveh
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Attribute Expression and Reference class implementations
 */

#include "FMx_general.h"
#include "FM8_AttributeClass.h"
#include "FM8_defs.h"
#include "FM8_Attributes_Ref&Expr.h"
#include "FMx_Support_primatives.h"
#include "FM8_Attributes.h"
#include "tags_sa.h"

extern void outHex(UCHAR* pS, unsigned offset, unsigned len);

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

int expressionElement8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;

	unsigned tagValue  = 0;
	unsigned tagLength = 0;
	unsigned entryOffset = offset; // for debugging only
	
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length, tagValue, tagLength, offset);

	if (ret != SUCCESS)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x with error parsing tag.\n", entryOffset);
		return ret;
	}

	if (tagLength != 0)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x  implicit tag with length %d.\n", entryOffset, tagLength);
		return ret;
	}

	if (tagValue > (unsigned)eet_array_index)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x with invalid Value &u.\n", entryOffset, tagValue);
		return ret;
	}

	if (tagValue >= (unsigned)eet_error || ConvertToA::ExpressionElementType((expressElemType_t)tagValue) >= (unsigned)eet_error )
	{
		LOGIT(CERR_LOG,"Error: Expression8  element @ 0x%08x with invalid Value &u.\n", entryOffset, tagValue);
		return ret;
	}

	rawElemType = eet_INVALID;
	exprElemType = (expressElemType_t)tagValue;
	exprElemType_t ee_type = ConvertToA::ExpressionElementType(exprElemType);

	if (ee_type > eet_array_index)// if we're a version 8 special type
	{// I split these out so this method didn't expand unmanagably
		return evalSpecial(pData, length, offset);
	}

	if (ee_type < eet_integer_constant || ee_type > eet_method_value )
	{
		return SUCCESS;// operator, has no parameters
	}
	// else, parse some more stuff

	if (ee_type == eet_integer_constant || ee_type == eet_unsigned_constant)
	{
		ret = parseInteger(8, pData, length, eeInt, offset);

		if ( ee_type == eet_unsigned_constant)
		{
			LOGIT(CERR_LOG,"Error: Expression element with non-hart unsigned const.\n");
			return FMx_BAD_PARSE;
		}

		eeIsUnsigned = false;
	}
	if (ee_type == eet_floating_constant)
	{
		ret = parseFLOAT(pData, length, eeFloat, offset);
	}

	if (ee_type == eet_string_constant)
	{
		eepString = (faCString*)new faCString8;

		if (eepString == NULL)
		{
			return FMx_MEMFAIL;
		}

			ret = eepString->evaluateAttribute(pData, length, offset);
	}

	if (ee_type==eet_numeric_value || ee_type==eet_all_value || ee_type==eet_method_value)
	{//Reference
		eepRef = (abCReference*)new abCReference8;

		if (eepRef == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = eepRef->evaluateAttribute(pData, length, offset);
	}

	//if (ee_type == eet_attr_value)   // TODO: 'A' headers used in '8' class ???
	//{//enum1 + Reference		
	//	eeAttrType = (attr_t)*(*pData); // straight up 1 byte enum	
	//	(*pData)++; length--; offset ++;
	//	eepRef = (abCReference*)new abCReference8;

	//	if (eepRef == NULL)
	//	{
	//		return FMx_MEMFAIL;
	//	}

	//	ret = eepRef->evaluateAttribute(pData, length, offset);
	//}

	//if ( ee_type == eet_min_max_value )
	//{//whichinteger + enum1 + reference		
	//	ret = parseinteger(8, pdata, length, eeint, offset);

	//	if ( ret != success )
	//	{
	//		return ret;
	//	}	

	//	eeAttrType = (attr_t)*(*pData); // straight up 1 byte enum	
	//	(*pData)++; length--; offset ++;
	//	eepRef = (abCReference*)new abCReference8;

	//	if (eepRef == NULL)
	//	{
	//		return FMx_MEMFAIL;
	//	}

	//	ret = eepRef->evaluateAttribute(pData, length, offset);
	//}

	return ret;
}

int expressionElement8::evalSpecial(UCHAR** pData, int& length, unsigned& offset)
{
	int retVal = SUCCESS;
	//unsigned __int64 localLongLong;

	rawElemType = exprElemType; // record for tracability
	exprElemType_t ee_type = ConvertToA::ExpressionElementType(exprElemType);

	if (ee_type == eet_variable_id)
	{	//36 INTEGER -> eet_numeric_value & reference
		retVal = parseInteger(8, pData, length, eeInt, offset);

		if (retVal != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Expression eval 8 Var ID error.\n");
			return FMx_BAD_PARSE;
		}		

		eepRef = new abCReference8;

		if (eepRef == NULL)
		{
			return FMx_MEMFAIL;
		}

		eepRef->intValue = eeInt;
		eepRef->refType  = VARIABLE_ID_REF;

		exprElemType = eet_VARREF;

		return SUCCESS;
	}

	//if ( ee_type == eet_maxVal || ee_type == eet_minVal)
	//{	//37 INTEGER whch, INTEGER id -> eet_min_max_value & which & MAX & ref
	//	//38 INTEGER whch, INTEGER id -> eet_min_max_value & which & MIN & ref
	//	eeAttrType = (ee_type == eet_maxVal) ? at_max_value : at_min_value;
	//	ee_type    = eet_min_max_value;

	//	retVal = parseInteger(8, pData, length, eeInt, offset);// which

	//	if (retVal != SUCCESS)
	//	{
	//		LOGIT(CERR_LOG,"Error: Expression eval 8 which error.\n");
	//		return FMx_BAD_PARSE;
	//	}		

	//	retVal = parseInteger(8, pData, length, localLongLong, offset);

	//	if (retVal != SUCCESS)
	//	{
	//		LOGIT(CERR_LOG,"Error: Expression eval 8 minmax ID error.\n");
	//		return FMx_BAD_PARSE;
	//	}		

	//	eepRef = new abCReference8;

	//	if (eepRef == NULL)
	//	{
	//		return FMx_MEMFAIL;
	//	}

	//	eepRef->intValue = localLongLong;
	//	eepRef->refType  = rT_variable_id;

	//	return SUCCESS;
	//}

	//if (ee_type == eet_maxValRef || ee_type == eet_minValRef)
	//{	//39 INTEGER whch, REF var    -> eet_min_max_value & which & MAX & ref
	//	//40 INTEGER whch, REF var    -> eet_min_max_value & which & MIN & ref
	//	eeAttrType = (ee_type == eet_maxValRef) ? at_max_value : at_min_value;
	//	ee_type    = eet_min_max_value;

	//	retVal = parseInteger(8, pData, length, eeInt, offset);

	//	if (retVal != SUCCESS)
	//	{
	//		LOGIT(CERR_LOG,"Error: Expression eval 8 which error.\n");
	//		return FMx_BAD_PARSE;
	//	}				

	//	eepRef = new abCReference8;

	//	if (eepRef == NULL)
	//	{
	//		return FMx_MEMFAIL;
	//	}

	//	return eepRef->evaluateAttribute(pData, length, offset);
	//}

	if (ee_type == eet_error)
	{	//45
		LOGIT(CERR_LOG,"Error: UNKNOWN EXPRESSION TYPE 8.\n");
		return FMx_BAD_DECODE;
	}

	attr_t theAttr = at_item_information;

	if (ee_type == eet_count_ref)
	{	//41 Reference	-> eet_attr_value & COUNT & reference
		theAttr = at_count;
	}

	if (ee_type == eet_capacity_ref)
	{	//42 Reference	-> eet_attr_value & CAPACITY & reference
		theAttr = at_capacity;
	}

	if (ee_type == eet_first_ref)
	{	//43 Reference	-> eet_attr_value & FIRST & reference
		theAttr = at_first;
	}

	if (ee_type == eet_last_ref)
	{	//44 Reference	-> eet_attr_value & LAST & reference
		theAttr = at_last;
	}	

	if (theAttr == at_item_information)
	{
		LOGIT(CERR_LOG,"Error: UNFOUND EXPRESSION TYPE 8.\n");
		return FMx_BAD_DECODE;
	}

	exprElemType = ConvertToDDL::ExpressionElementType(eet_attr_value);
	eepRef = new abCReference8;

	if (eepRef == NULL)
	{
		return FMx_MEMFAIL;
	}

	return eepRef->evaluateAttribute(pData, length, offset);
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

int abCExpression8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{	
	RETURNCODE  ret = 0;
	unsigned tagValue;
	unsigned tagLength;
	expressionElement8* pExprElem;
	int exitLength, workingLength;
	unsigned exitOffset;
	UCHAR* exitPdata;
	unsigned entryOffset = offset;// for debugging only
	
	// explicit tag 
	ret = parseTag(pData, length, tagValue, tagLength, offset);

	if (tagLength > (unsigned)length || tagLength == 0)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute @ 0x%08x with incorrect tag's length.\n", 
																				  entryOffset);
		return -1;
	}

	if (tagValue != STRUCT8_TAG_EXPRESSION)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute @ 0x%08x with incorrect tag (%u).\n", 
																		entryOffset, tagValue);
		return -2;
	}

	workingLength = tagLength;
	exitLength = length   - tagLength;
	exitOffset = offset   + tagLength;
	exitPdata  = (*pData) + tagLength;

	while (workingLength > 0 && ret == SUCCESS)// do elements as long as we can
	{
		pExprElem = new expressionElement8;

		if (pExprElem == NULL)
			return FMx_MEMFAIL;

		ret = pExprElem->evaluateAttribute(pData, workingLength, offset);

		if ( ret == SUCCESS )
		{
			exprList.push_back(pExprElem);
			pExprElem = NULL;
		}
	}//		wend

	if (workingLength != 0)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval length mismatch.\n");
	}

	if (offset != exitOffset)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval offset mismatch.\n");
	}

	if ((*pData) != exitPdata)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval pointer mismatch.\n");
	}

	length = exitLength;

	return ret;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

aCattrBase* abCReference8::generatePrototype()
{
	if (attrType == 0x0116) // arrayName
	{
		return createReferenceAttribute(attrType);
	}
	else
	{
		return abCReference::generatePrototype();// base class'
	}
}

int abCReference8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;
	unsigned tagValue;
	unsigned tagLength;
	
	// steveh - this may need to go in the asC wrapper??
	ret = parseExpectedTag(pData, length, offset);//conditional on expected tag exists
	if (ret != SUCCESS)
		return ret;

	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset );

	unsigned reflen = tagLength;
	unsigned offset0 = offset;

	if (ret != SUCCESS || tagLength != 0 || tagValue >= (unsigned)rT_invalid )
	{		
		LOGIT(CERR_LOG,"Error: Reference Type is invalid. (0x%02x)\n)",tagValue);
		return ret;
	}

	rawRefType = -1;		// unused for FM8, 
	refType = tagValue;		// see comment where they are defined

	switch (tagValue)
	{
		case ITEM_ID_REF		:
		case ITEM_ARRAY_ID_REF	:
		case COLLECTION_ID_REF	:
		case BLOCK_ID_REF		:
		case VARIABLE_ID_REF	:
		case MENU_ID_REF		:
		case EDIT_DISP_ID_REF	:
		case METHOD_ID_REF		:
		case REFRESH_ID_REF		:
		case UNIT_ID_REF		:
		case WAO_ID_REF			:
		case RECORD_ID_REF		:
		case ARRAY_ID_REF		:
		case VAR_LIST_ID_REF	:
		case PROGRAM_ID_REF		:
		case DOMAIN_ID_REF		:
		case RESP_CODES_ID_REF	:
		case FILE_ID_REF		:
		case CHART_ID_REF		:
		case GRAPH_ID_REF		:
		case AXIS_ID_REF		:
		case WAVEFORM_ID_REF	:
		case SOURCE_ID_REF		:
		case LIST_ID_REF		:
		case IMAGE_ID_REF		:
		case GRID_ID_REF		:
		{// integer			
			ret = parseInteger(8, pData, length, intValue, offset);
		}
		break;

		case SEPARATOR_REF:
		case ROWBREAK_REF:
		{// nothing more - check length
//#ifdef _DEBUG
			// was: assert(length == 0);
			// check that the ref was consumed
			// note that the length may be legitimately > 0 at this point
			int len = reflen - (offset - offset0);
			assert(len == 0);
//#endif
		}
		break;

		case CONSTANT_REF:				//[35]
		{// 35 expression			
			pExpr = (abCExpression*)new abCExpression8;
			if (pExpr == NULL)
				return FMx_MEMFAIL;
			ret = pExpr->evaluateAttribute(pData, length, offset);
		}
		break;

		case VIA_ITEM_ARRAY_REF:
		case VIA_ARRAY_REF:
		case VIA_LIST_REF:
		case VIA_BLOCK_REF:
		{// 36,39,45, 50 expression,  reference			
			pExpr = (abCExpression*)new abCExpression8;

			if (pExpr == NULL)
				return FMx_MEMFAIL;

			ret = pExpr->evaluateAttribute(pData, length, offset);

			if ( ret == SUCCESS)
			{		
				pRef = (abCReference*)new abCReference8;

				if (pRef == NULL)
					return FMx_MEMFAIL;

				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case VIA_COLLECTION_REF:
		case VIA_RECORD_REF:	
		case VIA_VAR_LIST_REF:				//[40]
		case VIA_FILE_REF:
		case VIA_CHART_REF:
		case VIA_GRAPH_REF:
		case VIA_SOURCE_REF:						
		case VIA_BITENUM_REF:			//[46]
		//case rT_via_Xblock_member:
		{// 37,38, 40,41,42,43,44,46, 49 integer, reference		
			ret = parseInteger(8, pData, length, intValue, offset);
			if ( ret == SUCCESS)
			{		
				pRef = new abCReference8;
				if (pRef == NULL)
					return FMx_MEMFAIL;
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case VIA_ATTR_REF:
		{// 47 enum1, reference				
			intValue = *(*pData);
			(*pData)++; length--; offset++;
			if ( ret == SUCCESS)
			{		
				pRef = new abCReference8;
				if (pRef == NULL)
					return FMx_MEMFAIL;
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case rT_ref_with_arguments:			//[55] 
assert(false);	// how are we here?
#if 0
		{//explicit tag 62, args are references till taglength gone
			unsigned localTemp;
			int      localLen;
			abCReference8* p_ref;
			ret = parseTag(pData, length,   tagValue,   localTemp,   offset );
			if (tagValue != STRUCTURE_TAG_ARGUMENT)
			{
				LOGIT(CERR_LOG,"Error: Reference w/Args has bad tag. (0x%02x)\n)",tagValue);
				ret = FMx_BAD_PARSE;
			}
			else
			{
				localLen = (int)localTemp;
				length -= localLen;
				while(localLen > 0)
				{
					p_ref = new abCReference8;
					if ( p_ref == NULL )
					{
						return FMx_MEMFAIL;
					}
					ret = p_ref->evaluateAttribute(pData, localLen, offset);
					if ( ret == SUCCESS )
					{
						argumentList.push_back(p_ref);
					}
				}//wend
				assert( length > 0 );//we have more to do
				pRef = new abCReference8;
				if ( pRef == NULL )
				{
					return FMx_MEMFAIL;
				}
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
#endif
		break;

		//case rT_reserved_1:
		//case rT_Invalid:
		case ERR_REF_A:
		default:
		{
			LOGIT(CERR_LOG,"Error: Reference Type is incorrect. (0x%02x)\n)",tagValue);
			ret = FMx_BAD_PARSE;
		}
		break;
	}//endswitch on ref type
	return ret;	
}
