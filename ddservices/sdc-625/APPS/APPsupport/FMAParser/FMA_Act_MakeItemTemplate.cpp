/**********************************************************************************************
 *
 * $Workfile: FMA_Act_OutputPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Act_ReadPayload Visitor Operation on all object classes
 */

#include "FMA_Activities.h"
#include "FMx.h"
#include "FMA_object.h"
#include "logging.h"

// // required function
//	> x41 disregard, x81 won't be seen
//	> x43 disregard, x83 won't be seen
//	> x42 disregard for now, x82 -- lots of work to do
//
// this is required to output the entire object (fixed and extension)
int FMA_makeItemTemplateVisitor::Operate(FMx_Base* pVisited)
{
	if (pVisited == NULL)
	{
		return FAILURE;
	}

	unsigned ct = pVisited->classType;

	if ((ct & FMA_type) != FMA_type)
	{
		LOGIT(CERR_LOG,"ERROR: FMA_makeItemTemplateVisitor should never see classtype %#02x\n", ct);
		return FAILURE;
	}

	if (ct & __Meta)
	{// we are not supposed to see x81 nor x83
		LOGIT(CERR_LOG,"ERROR: FMA_makeItemTemplateVisitor should never see classtype %#02x\n", ct);
		return FAILURE;
	}

	FMA_Object* pObj = (FMA_Object*)pVisited;

	if (pObj && pObj->pHandler)
	{
		pObj->pHandler->generatePrototype();
	}

	return SUCCESS;
};


