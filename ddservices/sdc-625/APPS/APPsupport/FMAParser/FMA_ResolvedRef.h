/**********************************************************************************************
 *
 * $Workfile: FMA_ResolvedRef.h $
 * 08oct12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_ResolvedRef.h : This describes the resolved reference item
 *
 * #include "FMA_ResolvedRef.h"
 *
 */

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _FMA_RESOLVEDREF_H
#define _FMA_RESOLVEDREF_H

#ifdef INC_DEBUG
#pragma message("In FMA_ResolvedRef.h") 
#endif


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_ResolvedRef.h") 
#endif

#define RESOLVED_REF_TYPEID   48
#define RESOLVED_REF_ITEMID    0

class resolved_reference
{
public://data
	int					type;  // only valid are 0 (item id ) and 48 (resolved ref)
	unsigned			whichItem;// type==48:which element; type==0: symbolID
	resolved_reference* resRef;// null if type 0 (memory allocated only fo type 48)
	
public://the big three:::
	resolved_reference& operator=(const resolved_reference& src)
	{	type = src.type; whichItem=src.whichItem;
		if (src.resRef)
		{	resRef = new resolved_reference(*(src.resRef));
		}else 
			resRef=NULL;
	};
	resolved_reference(resolved_reference& s){ operator=(s); };
	~resolved_reference() { if(resRef) {delete resRef, resRef=NULL;} };
	// plus one
	resolved_reference():type(-1),whichItem(0), resRef(NULL){};

public:
	int parse(UCHAR** pp_Location, INT& remainingSize);
	int out(int rowNumber);
	int ptocReconcileOut(int rowNumber);
	// add the aCreference generator here...
};


#endif //_FMA_RESOLVEDREF_H