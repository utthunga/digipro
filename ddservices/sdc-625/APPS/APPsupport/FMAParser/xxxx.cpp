//   FILE FMx_Attributes/icCxxxx.h - no accompanying .cpp file
//   ___xxxx___         add data, code out() and destroy() in the header

#ifndef _icCxxxx_
#define _icCxxxx_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCxxxx: public FMx_Attribute
{
public: // construct 
	icCxxxx() : value(0L) { attname = "xxxx"; };   // initialize underlying class data
    
public: // data
	unsigned long value;        // whatever data is included in the underlying class
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
    };
    
	virtual int out(void) 
    {
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Value", value, value);
        return 0;
    };
};
#endif //_icCxxxx_




//   FILE FMA_Attributes/icCxxxxA.h
//   ___xxxx___

#ifndef _icCxxxxA_
#define _icCxxxxA_

#include "..\FMx_Attributes\icCxxxx.h"
#include "..\FMA_Conditional.h"

class icCxxxxA : public icCxxxx
{
public: // construct / destruct
	icCxxxxA() {}
	virtual ~icCxxxxA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCxxxxA_



//   FILE FMA_Attributes/asCxxxxSpecA.h
//   ___xxxx___        code generatePrototype() in the header

#ifndef _ASCxxxxSPECA_
#define _ASCxxxxSPECA_

#include "..\FMA_Attributes\icCxxxxA.h"
#include "..\FMA_Conditional.h"

class asCxxxxSpecA : public FMx_Attribute
{
public:
	asCxxxxSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	FMA_Conditional<icCxxxxA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
        return NULL;
    };
};
#endif //_ASCxxxxSPECA_


//  FILE FMA_Attributes/icCxxxxA.cpp
//   ___xxxx___         code evaluateAttribute()

#include "icCxxxxA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
*/

int icCxxxxA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

    // parse tag
    // check tag
	// parsing - eg. parseInteger(4, pData, length, tempLongLong, offset);
    // length checking
    // return status
    
    return 0;
}


//   FILE FM8_Attributes/icCxxxx8.h
//   ___xxxx___

#ifndef _icCxxxx8_
#define _icCxxxx8_

#include "..\FMx_Attributes\icCxxxx.h"
#include "..\FM8_Conditional.h"

class icCxxxx8 : public icCxxxx
{
public: // construct / destruct
	icCxxxx8() {}
	virtual ~icCxxxx8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCxxxx8_



//   FILE FM8_Attributes/asCxxxxSpec8.h
//   ___xxxx___        code generatePrototype() in the header

#ifndef _ASCxxxxSPEC8_
#define _ASCxxxxSPEC8_

#include "..\FM8_Attributes\icCxxxx8.h"
#include "..\FM8_Conditional.h"

class asCxxxxSpec8 : public  FMx_Attribute
{
public:
	asCxxxxSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	FM8_Conditional<icCxxxx8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
    
	virtual aCattrBase* generatePrototype()
    {
        return NULL;
    };
};
#endif //_ASCxxxxSPEC8_


//  FILE FM8_Attributes/icCxxxx8.cpp
//   ___xxxx___         code evaluateAttribute()

#include "icCxxxx8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
*/

int icCxxxx8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
    UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

    // parse tag
    // check tag
	// parsing - eg. parseInteger(4, pData, length, tempLongLong, offset);
    // length checking
    // return status
    
    return 0;
}



