/**********************************************************************************************
 *
 * $Workfile: FM8_Attributes.cpp $
 * 20mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Attribute classes implementations
 *		
 *
 */

#include <memory.h>
#include "logging.h"
#include "endian.h"
#include "tags_sa.h"
#include "AttributeString.h"
#include "FMx_general.h"
#include "FMx_Support_primatives.h"
#include "FM8_AttributeClass.h"

#include "FM8_defs.h"
#include "FM8_Attributes.h"	// these are polymorphic - different evaluators(includes FM8)

#include "FM8_Attributes\asCrefListSpec8.h"
#include "FM8_Attributes\asCrefSpec8.h"

extern void outHex(UCHAR* pS, unsigned offset, unsigned len);
extern unsigned currentItemSymbolID;

#define HEAP_ATTR_FILLED  111	// a randomly chosen number

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute list class
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
FM8_AttributeList::~FM8_AttributeList() // has to destroy all the attributes
{
	FMx_Attribute* pAttr = NULL;
	fm8AttrIT mIT;
	for( mIT = begin(); mIT != end(); ++ mIT )
	{
		pAttr = *mIT;
		pAttr->destroy();
		delete(pAttr);	// don't worry about the pointer, we'll get rid of it in a bit
	}// next
	clear();
}


// called for extension (!isheap) AND heap chunks (isheap)
int FM8_AttributeList::	// caller must assume all bytes are used
evaluateAttributePayload(UCHAR* pData, int length, unsigned offset, UCHAR& count, bool isheap)
{
	int status = SUCCESS;
	RETURNCODE rc = SUCCESS;
	if (pData == NULL || length == 0)
	{
		return status;// we were called with nothing to do
	}
	if ( isheap )
	{
		return evaluateAttributeHeap(pData, length, offset, count);
	}

	FMx_Attribute* pAttr = NULL;
	UINT64 tempLongLong = 0;
	UCHAR localAttrType = 0;
	unsigned localSize  = 0;
	unsigned entryOffset= offset;
	unsigned entryLen   = length;
	UCHAR*   entryPtr   = pData;
	UCHAR*   calcdPtr   = pData +length;
	int      calcdLen   = 0;
	unsigned calcdOff   = offset+length;
	int      wrkingLen  = 0;
	unsigned wrkOffset  = 0;
	unsigned wrkLenLen  = 0;
	unsigned wrkIndex   = 0;
	unsigned RIvalue    = 0xff;
	unsigned immedCnt   = 0;

/* version 8
series of:

	attrID
		RI      byte >> 6
		AttrTag byte & 0x3f		aka attr name
		length  encoded integer
	@ RI == 2   Object index			unsigned16
	@ RI == 1	Local-address-offset	encoded integer
	@ RI == 0	AttributeSpecifier
		as determined by itemType + AttrTag
		ie variable:label  1:3 String Specifier
**/

	// fm8 version set at bit count location   attr_count = count;
	while( count > 0 && length > 1 )
	{
		entryOffset   = offset;
		entryPtr      = pData;
	
		// read type
		localAttrType = (*pData) & 0x3f;
		// RIvalue: 0 = Immediate, 1 = Local, 2 = External, 3 = External-All
		RIvalue       = ((*pData)>>6) & 0x03;
		pData++;  offset++; length--;
		
		// read size
		entryLen = length;  // used to calculate length of length
		status   = parseInteger(4, &pData, length, tempLongLong, offset);		
		// Immediate => following Length bytes
		// Local     => next INTEGER is the length of attribute in heap
		// External  => next unsigned 16 is object-index of attribute :Length of other attr 

		if (status != SUCCESS)
		{// we're out of sync and have to leave
			DEBUGLOG(CLOG_LOG,"Error: [IDX:%4u] Length unreadable for RI = %d\n",
																	objectIndex, RIvalue);
			pData  = calcdPtr;
			offset = calcdOff;
			length = calcdLen;
			return status;
		}
		
		wrkingLen = (int)tempLongLong;

		if ( RIvalue == 0 )// immediate
		{
			if (wrkingLen > length) //immediate only
			{
				LOGIT(CERR_LOG,
				"Error: [IDX:%4u] attribute size (%u) longer than remaining length (%d)\n",
														objectIndex, pAttr->length, length);
				pData  = calcdPtr;
				offset = calcdOff;
				length = calcdLen;
				//delete pAttr;
				return -5;
			}
			
			calcdPtr = pData  + wrkingLen;
			calcdLen = length - wrkingLen;
			calcdOff = offset + wrkingLen;
		}
		else
		if ( RIvalue == 1 )// local
		{//next INTEGER to local-address for offset into ddod:Length heap byte	

			status   = parseInteger(4, &pData, length, tempLongLong, offset);
			if (status != SUCCESS)
			{// we're out of sync and have to leave
				DEBUGLOG(CLOG_LOG,"Error: [IDX:%4u] Offset unreadable for RI = %d\n",
																	objectIndex, RIvalue);
				pData  = calcdPtr;
				offset = calcdOff;
				length = calcdLen;
				return status;
			}	
			wrkOffset = (unsigned)tempLongLong;
			
			calcdPtr = pData  + length;
			calcdLen = length - length;
			calcdOff = offset + length;

			wrkLenLen = entryLen - length; // amount of extension bytes used
		}
		else
		if ( RIvalue == 2 )
		{// get unsigned 16 object index of target
			wrkIndex = REVERSE_S( *((unsigned short*)pData) );
			pData  += sizeof(unsigned short);
			offset += sizeof(unsigned short); 
			length -= sizeof(unsigned short);

			calcdPtr = pData  + length;
			calcdLen = length - length;
			calcdOff = offset + length;
		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,"Error:[IDX:%4u] Reference Indicator is unknown (%d)\n"
																	,objectIndex,RIvalue);
			pData  = calcdPtr;
			offset = calcdOff;
			length = calcdLen;
			return -6;
		}

		// get an attribute as soon as we have the type and size
		pAttr = createAttr(localAttrType);
		if (pAttr == NULL)
		{// unsupported or unknown
			pData  = calcdPtr;
			offset = calcdOff;
			length = calcdLen;
			return FMx_MEMFAIL;// out of memory
		}
		// RI=0; bytes in extension.  RI=1; bytes in heap;  RI=2; bytes in other attribute
		pAttr->length       = (unsigned)wrkingLen;
		pAttr->heapOffset   = wrkOffset;
		pAttr->lenOfLength  = wrkLenLen;
		pAttr->externalIndex= wrkIndex;

		pAttr->startOffset = entryOffset; // offset of the attrType
		pAttr->lenOfLength = entryLen - length;
		pAttr->pBytes      = entryPtr;
		//this has been set to the generic attribute type:
		//											 pAttr->attrType = localAttrType;
		pAttr->id.RefType  = RIvalue;
		pAttr->id.attrType = localAttrType;

		if ( RIvalue != 1)
		{
			status = pAttr->evaluateAttribute(&pData, wrkingLen, offset);
			if (status != SUCCESS)
			{
				if ( status != FMx_UNSUPPORTED)
					LOGIT(CERR_LOG,"      %s Attribute @ 0x%08x failed to evaluate.\n",
					AttributeString::get8(pAttr->attrType>>8, pAttr->attrType&0xff));
				pAttr->status = status;// save for debugging
				pData  = calcdPtr;
				offset = calcdOff;
				length = calcdLen;

				status = 0;// allow it to continue after we leave
			}
			else
			{
				if (wrkingLen != 0)
				{					
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd len mismatch.(%d)\n",
					entryOffset,wrkingLen);
				}
				if (offset != calcdOff )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd offset mismatch.\n",
					entryOffset);
				}
				if ( pData != calcdPtr )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd pointer mismatch.\n",
					entryOffset);
				}				
				pData  = calcdPtr;
				offset = calcdOff;
				length = calcdLen;
			}
			count--;
		}
		else			
			immedCnt++;

		//push_back(pAttr); // save even the bad ones
		insertAttr(pAttr); // save even the bad ones
		pAttr = NULL;
	}//wend count > 0
#ifdef _DEBUG
	if ( length != 0 || (count-immedCnt) != 0  )
	{
		LOGIT(COUT_LOG|CERR_LOG,"       Attribute length mismatch: Len: %d Cnt:%d\n",
																				length,count);
	}
	remainingLen = length;
	remainingCnt = count;
#endif
	return rc;
}


int FM8_AttributeList::	// caller must assume all bytes are used
evaluateAttributeHeap(UCHAR* pData, int length, unsigned offset, UCHAR& count)
{
	int status = SUCCESS;

	FMx_Attribute* pAttr = NULL;
	unsigned entryOffset= offset;
	unsigned entryLen   = length;// total length of this object's use of the heap (all attrs)
	UCHAR*   entryPtr   = pData; // beginning of item's heap (all attrs)
	UCHAR*   calcdPtr   = pData +length;// has no use
	int      calcdLen   = 0;
	unsigned calcdOff   = offset+length;// has no use
	int      wrkingLen  = 0;
	UCHAR localAttrType = 0;
/*
	UINT64 tempLongLong = 0;
	unsigned localSize  = 0;
	unsigned RIvalue    = 0xff;
	unsigned immedCnt   = 0;
*/

	// fm8 version set at bit count location   attr_count = count;
	int attr_count = count;
	while( count > 0 && length > 1 )
	{// pdata is pointing into the local heap
		vector<FMx_Attribute *>::iterator abIT;
		for (abIT = begin(); abIT != end(); ++ abIT)
		{
			pAttr = (FMx_Attribute *)*abIT;
			if (pAttr->id.RefType == 1 && pAttr->status == 0)// local & unprocessed
			{
				break;// next unprocessed annex attribute
			}
		}
		// stevev 18sep13 -attributes can be any quantity, in any order
		if ( abIT != end() )
		{
			wrkingLen = pAttr->length;
			pData     = entryPtr    + pAttr->heapOffset;// stevev 18sep13 - this attr start pt
			offset    = entryOffset + pAttr->heapOffset;// stevev 18sep13 - ditto
			
			calcdPtr = pData  + wrkingLen;// has no use
			calcdLen = length - wrkingLen;// tracking total bytes available
			calcdOff = offset + wrkingLen;// has no use

			pAttr->fileOffset = offset;
			pAttr->pHeapBytes = pData;
			status = pAttr->evaluateAttribute(&pData, wrkingLen, offset);
			if (status != SUCCESS)
			{
				if ( status != FMx_UNSUPPORTED)
					LOGIT(CERR_LOG,"       %s Attribute @ offset 0x%08x failed to evaluate.\n",
					AttributeString::get8(pAttr->attrType>>8, pAttr->attrType&0xff), offset);
				pAttr->status = status;// save for debugging
				pAttr->heapOffset = 0;
				length = calcdLen;

				status = 0;// allow it to continue after we leave
			}
			else
			{
				if (wrkingLen != 0)
				{					
				LOGIT(CERR_LOG,"Error: Attribute @ offset 0x%08x eval'd length mismatch.(%d)\n",
					entryOffset + pAttr->heapOffset,wrkingLen);
					length = calcdLen;
				}
				if (offset != calcdOff )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd offset mismatch.\n",
					entryOffset + pAttr->heapOffset);
					offset = calcdOff;
				}
				if ( pData != calcdPtr )
				{
					LOGIT(CERR_LOG,"Error: Attribute @ 0x%08x eval'd pointer mismatch.\n",
					entryOffset + pAttr->heapOffset);
					pData  = calcdPtr;
				}
				length = calcdLen;
				pAttr->status = HEAP_ATTR_FILLED;
			}
			count--;
		}
#ifdef _DEBUG
		else
		{
			//LOGIT(CERR_LOG,"       Attribute not found in attribute list\n");
			count--;
		}
#endif
	}//wend count > 0
#ifdef _DEBUG
	if ( length != 0 || count != 0  )
	{
		LOGIT(CERR_LOG,"       Attribute heaplength mismatch: Len: %d Cnt:%d\n",length,count);
	}
	remainingLen = length;
	remainingCnt = count;
#endif
	return status;
}

	
int FM8_AttributeList::out (void) // payload data to stdout	
{
	fm8AttrIT mIT;
	FMx_Attribute* pAttr = NULL;

	int count = attr_count;
	if (reconcile)
	{
		for( mIT = begin(); mIT != end(); ++ mIT )
		{
			pAttr = *mIT;
			if (pAttr)
			{
				if (pAttr->inhibitOut())
				{
					count--;	// don't count inhibited attrs
				}
			}
		}// next
		if (pAttr->attrType>>8 == UNIT_ITYPE  ||  pAttr->attrType>>8 == REFRESH_ITYPE)
		{
			count++;	// emulate two FMA attrs for unit & refresh relations
		}
	}

	LOGIT(COUT_LOG,"   Item's Attr Count: 0x%04x (%u)\n\n", count, count);

	if ( reconcile )
	{
		sort( begin(), end(), attrTypeLess);
	}

	for( mIT = begin(); mIT != end(); ++ mIT )
	{
		pAttr = *mIT;
		if (pAttr)
		{
			string atype = AttributeString::get8(pAttr->attrType>>8, pAttr->attrType&0xff);
			if (reconcile && atype=="Watch Variable")
			{
				LOGIT(COUT_LOG,
				"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
				((asCUnitRelationLst8 *)pAttr)->reconcile_out();
			}
			else if (reconcile && atype=="Watch List" && pAttr->attrType == REFRESH_ITYPE<<8 )
			{
				LOGIT(COUT_LOG,
					"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
				((asCRefreshLst8 *)pAttr)->reconcile_out();
			}
			else if (pAttr->inhibitOut() == false)
			{
				LOGIT(COUT_LOG,
					"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
				pAttr->out();
			}
		}
	}// next

	return 0;
}
 
// inserts 'em in numerical order
void FM8_AttributeList::insertAttr(FMx_Attribute* pA)
{
	//push_back(pAttr); // initial technique
	unsigned newattr = pA->attrType;
	FMx_Attribute *pE;

	if ( size() < 1 )
	{
		push_back(pA);
		return;
	}
	// else, put it in order
	for ( vector<FMx_Attribute*>::iterator IT = begin(); IT != end(); ++IT)
	{
		pE = *IT;
		if (pE->attrType > newattr)
		{
			insert(IT, pA);
			return;
		}
	}
	// if we get here, we belong in the back
	push_back(pA);
	return;
}


// factory method for attributes
// passed in value is expected tag..hi bit set for explicit eg x23 for Edit Format,xd2 Elements
FMx_Attribute* FM8_AttributeList::createAttr(unsigned char attr_type) 
{
	FMx_Attribute* pAttrClass = NULL;
	unsigned  genericAttr = (item_type << 8) | attr_type;

	switch (genericAttr)
	{
		case ag_Var_class:					pAttrClass = new  asCclassSpec8(0);			break;
		case ag_Var_handling:				pAttrClass = new  asChandlingSpec8(0);		break;
		case ag_Var_constant_units:			pAttrClass = new  asCstrSpec8(0);			break;
		case ag_Var_label:					pAttrClass = new  asCstrSpec8(0);			break;
		case ag_Var_help:					pAttrClass = new  asCstrSpec8(0);			break;
		case ag_Var_width:
			{
				if (SUPPRESS_TIMEOUTS)
				{
					pAttrClass = new  asCnotHart8(0);	
				}
				else
				{
					pAttrClass = new  asCdisplaySize8(0);			
				}
			}
			break;
		case ag_Var_height:
			{
				if (SUPPRESS_TIMEOUTS)
				{
					pAttrClass = new  asCnotHart8(0);	
				}
				else
				{
					pAttrClass = new  asCdisplaySize8(0);	
				}
			}
			break;
		case ag_Var_validity:				pAttrClass = new  asCboolSpec8(0);			break;
		case ag_Var_pre_read_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_post_read_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_pre_write_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_post_write_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_pre_edit_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_post_edit_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_response_codes:			pAttrClass = new  asCnotHart8(0);			break;
		case ag_Var_type:					pAttrClass = new  asCvarType8(0);			break;
		case ag_Var_display_format:			pAttrClass = new  asCstrSpec8(34);			break;
		case ag_Var_edit_format:			pAttrClass = new  asCstrSpec8(35);			break;
		case ag_Var_min_values:				pAttrClass = new  asCminmaxSpec8(0xa5);		break;
		case ag_Var_max_values:				pAttrClass = new  asCminmaxSpec8(0xa4);		break;
		case ag_Var_scaling_factor:			pAttrClass = new  asCexprSpec8(38);			break;
		case ag_Var_enumerations:			pAttrClass = new  asCenumLstSpec8(0);		break;
		case ag_Var_array_name:				pAttrClass = new  abCReference8(50, true);
											pAttrClass->attname = "VarArrayName";		break;
		case ag_Var_default_value:			pAttrClass = new  asCexprSpec8(0);			break;
		case ag_Var_refresh_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_item_information:		pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Var_post_request_action: 	pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_post_user_action:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Var_time_format:			pAttrClass = new  asCstrSpec8(61);			break;
		case ag_Var_time_scale:				pAttrClass = new  asCtimeScaleSpec8(0);		break;
		case ag_Var_visible:				pAttrClass = new  asCboolSpec8(0);		break;
		case ag_Var_private:				pAttrClass = new  icCBool8();				break;

		case ag_Cmd_number:					pAttrClass = new  asCintegerConst8(0x82);
											pAttrClass->attname = "CmdNumber";			break;
//		case ag_Cmd_operation:				pAttrClass = new  icCoperation8(0);
//											pAttrClass->attname = "CmdOperation";		break;
		case ag_Cmd_operation:				pAttrClass = new  asCoperationSpec8(0, (reconcile?true:false));
											pAttrClass->attname = "CmdOperation";		break;
		case ag_Cmd_transaction:			pAttrClass = new  asTransactionLst8(0);
											pAttrClass->attname = "CmdTransList";		break;
		case ag_Cmd_response_codes:			pAttrClass = new  asCrespCodesSpec8(0);
											pAttrClass->attname = "RespCodes";			break;
		case ag_Cmd_item_information:		pAttrClass = new  asCitemInfo8(186);
											pAttrClass->attname = "CmdDbgInfo";			break;
		
		case ag_Menu_label:					pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Menu) << 8,//[0] String_Specifier,
		case ag_Menu_items:					pAttrClass = new  asCmenuItmSpec8(0);		break;			//	[1] Menu_Items_Specifier,
		case ag_Menu_help:					pAttrClass = new  asCstrSpec8(0);			break;			//	[2] String_Specifier,
		case ag_Menu_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//	[3] Boolean_Specifier,
		case ag_Menu_style:					pAttrClass = new  icCstyle8();				break; 			//	[4] Style_Specifier
		case ag_Menu_item_information:		pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Menu_visible:				pAttrClass = new  asCboolSpec8(0);			break;
		case ag_Menu_pre_read_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Menu_post_read_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Menu_pre_write_actions:		pAttrClass = new  asCrefListSpec8(0);		break;
		case ag_Menu_post_write_actions:	pAttrClass = new  asCrefListSpec8(0);	break;

		case ag_EdDisp_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_EditDisplay) << 8,//[0] String_Specifier,
		case ag_EdDisp_edit_items:			pAttrClass = new  asCrefListSpec8(0);		break;			//[1] Reference_List_Specifier,
		case ag_EdDisp_display_items:		pAttrClass = new  asCrefListSpec8(0);		break;			//[2] Reference_List_Specifier,
		case ag_EdDisp_pre_edit_actions:	pAttrClass = new  asCrefListSpec8(0);		break;			//[3] Reference_List_Specifier,
		case ag_EdDisp_post_edit_actions:	pAttrClass = new  asCrefListSpec8(0);		break;			//[4] Reference_List_Specifier,
		case ag_EdDisp_help:				pAttrClass = new  asCstrSpec8(0);			break;					//[5] String_Specifier,
		case ag_EdDisp_validity:			pAttrClass = new  asCboolSpec8(0);			break; 			//[6] Boolean_Specifier,
		case ag_EdDisp_item_information:	pAttrClass = new  asCitemInfo8(186);		break;
		case ag_EdDisp_visible:				pAttrClass = new  asCboolSpec8(0);		break;

		case ag_Meth_class:					pAttrClass = new  asCclassSpec8(0);			break;		//= ((int)it_Method) << 8,//[0] Class_Specifier,
		case ag_Meth_label:					pAttrClass = new  asCstrSpec8(0);			break;		//[1] String_Specifier,
		case ag_Meth_help:					pAttrClass = new  asCstrSpec8(0);			break;		//[2] String_Specifier,
		case ag_Meth_definition:			pAttrClass = new  asCmethDef8();
											pAttrClass->attname = "MethDef";			break;		//[3] BYTE STRING, 
		case ag_Meth_validity:				pAttrClass = new  asCboolSpec8(0);			break; 		//[4] Boolean_Specifier
		case ag_Meth_scope:					pAttrClass = new  asCscopeSpec8(0);break;	// [5] scope - unsupported
		case ag_Meth_type:					pAttrClass = new  icCmethType8();			break;		//[6] Method_Type_Specifier
		case ag_Meth_parameters:			pAttrClass = new  asCmethParams8(0);		break;		//[7] Method_Parameters_Specifier
		case ag_Meth_item_information:		pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Meth_visible:				pAttrClass = new  asCboolSpec8(0);
											pAttrClass->attname = "MethVisibility";		break;
		case ag_Meth_private:				pAttrClass = new  icCBool8();
											pAttrClass->attname = "MethPrivate";		break;


		case ag_Refresh_list:				pAttrClass = new  asCRefreshLst8(0);		break;			// = ((int)it_RefreshRel) << 8,//[0] Refresh_Specifier 
		case ag_Refresh_item_information:	pAttrClass = new  asCitemInfo8(186);		break;
			
   
		case ag_Refresh_max_id:				pAttrClass = new  asCunknownType8(0);		break;	// [2] - remains for backward compatability
		case ag_Refresh_updateList:			pAttrClass = new  asCRefreshLst8(0);		break;	//???? [3] REFRESH_UPDATE_LIST_ID -new format, works with 4
		case ag_Refresh_watchList:			pAttrClass = new  asCRefreshLst8(0);		break;	//???? [4] REFRESH_WATCH_LIST_ID - new format, works with 3

		case ag_UnitRel_list:				pAttrClass = new  asCUnitRelationLst8(0);	break;			// = ((int)it_UnitRel) << 8,// [0] Unit_Specifier 

		case ag_Unit_item_information:		pAttrClass = new  asCitemInfo8(186);		break;			// [1] REFRESH_DEBUG_ID
		case ag_Unit_max_id:				pAttrClass = new  asCunknownType8(0);		break;	// [2] - remains for backward compatability
		case ag_UnitRel_var:				pAttrClass = new  abCReference8(50, true);	break;	//???? [3] UNIT_VAR_ID - new format - 4 is the update list
		case ag_Unit_updateList:			pAttrClass = new  asCRefreshLst8(0);		break;	//???? [4] UNIT_UPDATE_LIST_ID -new format, works with 3

		case ag_WAO_list:					pAttrClass = new  asCrefListSpec8(0);		break;			// = ((int)it_WAORel) << 8,// [0] Reference_List_Specifier 
		case ag_WAO_item_information:		pAttrClass = new  asCitemInfo8(186);		break;	// [1] WAO_DEBUG_ID

		case ag_RefArr_elements:			pAttrClass = new  asCelementsSpec8(0);		break;			//= ((int)it_Ref_Array) << 8,//	[0] Elements_Specifier,
		case ag_RefArr_label:				pAttrClass = new  asCstrSpec8(0);			break;			//	[1] String_Specifier,
		case ag_RefArr_help:				pAttrClass = new  asCstrSpec8(0);			break;			//	[2] String_Specifier,
		case ag_RefArr_validity:			pAttrClass = new  asCboolSpec8(0);			break; 			//	[3] Boolean_Specifier
		case ag_RefArr_item_information:	pAttrClass = new  asCitemInfo8(186);			break;
		case ag_RefArr_private:				pAttrClass = new  icCBool8();				break;

		case ag_Coll_members:				pAttrClass = new  asCmemberLst8(0);			break;			//= ((int)it_Collection) << 8,//[0] Members_Specifier,
		case ag_Coll_label:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Coll_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[3] String_Specifier
		case ag_Coll_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//	[3] Boolean_Specifier
		case ag_Coll_item_information:		pAttrClass = new  asCitemInfo8(186);			break;
		case ag_Coll_visible:				pAttrClass = new  asCboolSpec8(0);		break;
		case ag_Coll_private:				pAttrClass = new  icCBool8();				break;

		case ag_Block_Char:					pAttrClass = new  abCReference8();			break;			//= ((int)it__BlockA) << 8,//characteristics	[ 0] Reference,
		case ag_Block_label:				pAttrClass = new  asCstrSpec8(0);			break;			//[ 1] String_Specifier,
		case ag_Block_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[ 2] String_Specifier,
		case ag_Block_parameters:			reconcile ? (pAttrClass = new asCnotHart8(0)) : (pAttrClass = new  asCmemberLst8(0));			break;			//[ 3] Members_Specifier,

		case ag_ValArr_unused:				pAttrClass = new  asCunknownType8(0);		break;			//= ((int)it_Val_Array) << 8,//[0] 
		case ag_ValArr_label:				pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier, 
		case ag_ValArr_help:				pAttrClass = new  asCstrSpec8(0);			break;			//[2] String_Specifier, 
		case ag_ValArr_validity:			pAttrClass = new  asCboolSpec8(0);			break;			//[3] Boolean_Specifier
		// the ag_ValArr_type is shown in lit80 as a Reference but is actully encoded as a
		//		conditional reference that is always direct.
		case ag_ValArr_type:				pAttrClass = new  asCrefSpec8(0x82,false, true);		break;			//[4] Reference,
		case ag_ValArr_length:				pAttrClass = new  asCintegerConst8(0);		break;			//[5] INTEGER, 
		case ag_ValArr_item_information:	pAttrClass = new  asCitemInfo8(186);		break;
		case ag_ValArr_private:				pAttrClass = new  icCBool8();				break;		//[7] Boolean - NOT conditional
	
		case ag_File_members:				pAttrClass = new  asCmemberLst8(0);			break;	// =((int)it_File) << 8,//[0]Members_Specifier,
		case ag_File_label:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_File_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[2] String_Specifier
		//ag_File_unused
		case ag_File_item_information:		pAttrClass = new  asCitemInfo8(186);		break;			
		case ag_File_handling:				pAttrClass = new  asChandlingSpec8(0);		break;
		case ag_File_update_actions:		pAttrClass = new  asCrefListSpec8(0);		break; 
		case ag_File_identity:				pAttrClass = new  asCasciiStr8(0);			break;			//[  3] ASCII_String

		case ag_Chart_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Chart) << 8,//[0] String_Specifier,
		case ag_Chart_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Chart_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//[2] Boolean_Specifier,
		case ag_Chart_height:				pAttrClass = new  asCdisplaySize8(0);		break;			//[3] Size_Specifier,
		case ag_Chart_width:				pAttrClass = new  asCdisplaySize8(0);		break;			//[4] Size_Specifier,
		case ag_Chart_type:					pAttrClass = new  asCchartTypeSpec8(0);		break;			//[5] Chart_Type_Specifier,
		case ag_Chart_length:				pAttrClass = new  asCexprSpec8(0);			break;			//[6] Expression_Specifier,
		case ag_Chart_cycle_time:			pAttrClass = new  asCexprSpec8(0);			break;			//[7] Expression_Specifier,
		case ag_Chart_members:				pAttrClass = new  asCmemberLst8(0);			break; 			//[8] Members_Specifier
		case ag_Chart_item_information:		pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Chart_visible:				pAttrClass = new  asCboolSpec8(0);		break;

		case ag_Graph_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Graph) << 8,//[0] String_Specifier,
		case ag_Graph_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Graph_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//[2] Boolean_Specifier,
		case ag_Graph_height:				pAttrClass = new  asCdisplaySize8(0);		break;			//[3] Size_Specifier,
		case ag_Graph_width:				pAttrClass = new  asCdisplaySize8(0);		break;			//[4] Size_Specifier,
		case ag_Graph_x_axis:				pAttrClass = new  asCrefSpec8(0);			break;			//[5] Reference_Specifier,
		case ag_Graph_members:				pAttrClass = new  asCmemberLst8(0);			break; 			//[6] Members_Specifier
		case ag_Graph_item_information:		pAttrClass = new  asCitemInfo8(186);			break;
		case ag_Graph_cycle_time:			pAttrClass = new  asCexprSpec8(0);			break;			
		case ag_Graph_visible:				pAttrClass = new  asCboolSpec8(0);		break;

		case ag_Axis_label:					pAttrClass = new  asCstrSpec8(0);			break;			// = ((int)it_Axis) << 8,//[0] String_Specifier, 
		case ag_Axis_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Axis_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//[2] Boolean_Specifier,
		case ag_Axis_min_value:				pAttrClass = new  asCexprSpec8(0);			break;			//[3] Expression_Specifier,
		case ag_Axis_max_value:				pAttrClass = new  asCexprSpec8(0);			break;			//[4] Expression_Specifier,
		case ag_Axis_scaling:				pAttrClass = new  asCaxisScalSpec8(0);		break;			//[5] Axis_Scaling_Specifier,
		case ag_Axis_constant_units:		pAttrClass = new  asCstrSpec8(0);			break;			//[6] String_Specifier
		case ag_Axis_item_information:		pAttrClass = new  asCitemInfo8(186);			break;
		case ag_Axis_view_min_value:		pAttrClass = new  asCunknownType8(0);			break;			
		case ag_Axis_view_max_value:		pAttrClass = new  asCunknownType8(0);			break;			

		case ag_Wave_label:					pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Waveform) << 8,//[ 0] String_Specifier,
		case ag_Wave_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[ 1] String_Specifier, 
		case ag_Wave_handling:				pAttrClass = new  asChandlingSpec8(0);		break;			//[ 2] Handling_Specifier,
		case ag_Wave_emphasis:				pAttrClass = new  asCboolSpec8(0);			break;			//[ 3] Boolean_Specifier,
		case ag_Wave_line_type:				pAttrClass = new  asClineTypeSpec8(0);		break;			//[ 4] Line_Type_Specifier,
		case ag_Wave_line_color:			pAttrClass = new  asCexprSpec8(0);			break;			//[ 5] Expression_Specifier,
		case ag_Wave_y_axis:				pAttrClass = new  asCrefSpec8(0);			break;			//[ 6] Reference_Specifier,
		case ag_Wave_key_points_x_values:	pAttrClass = new  asCrefListSpec8(0, (reconcile?true:false));		break;			//[ 7] Reference_List_Specifier,
		case ag_Wave_key_points_y_values:	pAttrClass = new  asCrefListSpec8(0, (reconcile?true:false));		break;			//[ 8] Reference_List_Specifier,
		case ag_Wave_waveform_type:			pAttrClass = new  asCwaveType8(0);			break;			//[ 9] Waveform_Type_Specifier,
		case ag_Wave_x_values:				pAttrClass = new  asCrefListSpec8(0, (reconcile?true:false));		break;			//[10] Reference_List_Specifier,
		case ag_Wave_y_values:				pAttrClass = new  asCrefListSpec8(0, (reconcile?true:false));		break;			//[11] Reference_List_Specifier,
		case ag_Wave_x_initial:				pAttrClass = new  asCexprSpec8(0);			break;			//[12] Expression_Specifier,
		case ag_Wave_x_increment:			pAttrClass = new  asCexprSpec8(0);			break;			//[13] Expression_Specifier,
		case ag_Wave_number_of_points:		pAttrClass = new  asCexprSpec8(0);			break;			//[14] Expression_Specifier,
		case ag_Wave_init_actions:			pAttrClass = new  asCrefListSpec8(0);		break;			//[15] Reference_List_Specifier,
		case ag_Wave_refresh_actions:		pAttrClass = new  asCrefListSpec8(0);		break;			//[16] Reference_List_Specifier,
		case ag_Wave_exit_actions:			pAttrClass = new  asCrefListSpec8(0);		break;			//[17] Reference_List_Specifier,
		case ag_Wave_item_information:		pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Wave_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//	[3] Boolean_Specifier

		case ag_Source_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Source) << 8,//[0] String_Specifier,
		case ag_Source_help:				pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Source_validity:			pAttrClass = new  asCboolSpec8(0);			break;			//[3] Boolean_Specifier,
		case ag_Source_emphasis:			pAttrClass = new  asCboolSpec8(0);			break;			//[3] Boolean_Specifier,
		case ag_Source_line_type:			pAttrClass = new  asClineTypeSpec8(0);		break;			//[4] Line_Type_Specifier,	
		case ag_Source_line_color:			pAttrClass = new  asCexprSpec8(0);			break;			//[5] Expression_Specifier,
		case ag_Source_y_axis:				pAttrClass = new  asCrefSpec8(0);			break;			//[6] Reference_Specifier,
		case ag_Source_members:				pAttrClass = new  asCmemberLst8(0);			break;			//[7] Members_Specifier
		case ag_Source_item_information:	pAttrClass = new  asCitemInfo8(186);		break;
		case ag_Source_init_actions:		pAttrClass = new  asCrefListSpec8(0);		break;			//[3] Reference_List_Specifier,
		case ag_Source_refresh_actions:		pAttrClass = new  asCrefListSpec8(0);		break;			//[3] Reference_List_Specifier,
		case ag_Source_exit_actions:		pAttrClass = new  asCrefListSpec8(0);		break;			//[3] Reference_List_Specifier,

		case ag_List_label:					pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_List) << 8,//[0] String_Specifier,
		case ag_List_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_List_validity:				pAttrClass = new  asCboolSpec8(0);			break;			//[2] Boolean_Specifier,			
		// the ag_List_type is shown in lit80 as a Reference but is actully encoded as a
		//		conditional reference that is always direct.
		case ag_List_type:					pAttrClass = new  abCReference8(0x82);		break;			//[3] Reference,
		case ag_List_count:					pAttrClass = new  asCexprSpec8(0);			break;			//[4] Expression_Specifier,
		case ag_List_capacity:				pAttrClass = new  asCintegerConst8(0x82);	break;	 		//[5] INTEGER  (OBJECT_TAG not shown in spec)
		case ag_List_item_information:		pAttrClass = new  asCitemInfo8(186);			break;			
		case ag_List_first:					pAttrClass = new  asCunknownType8(0);		break;			// internal use/not in a dd
		case ag_List_last:					pAttrClass = new  asCunknownType8(0);		break;			// internal use/not in a dd
		case ag_List_private:				pAttrClass = new  icCBool8();				break;	

		case ag_Grid_label:					pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Grid) << 8,//[0] String_Specifier,
		case ag_Grid_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Grid_validity:				pAttrClass = new  asCboolSpec8(0);			break;			//[2] Boolean_Specifier,
		case ag_Grid_height:				pAttrClass = new  asCdisplaySize8(0);		break;			//[3] ,
		case ag_Grid_width:					pAttrClass = new  asCdisplaySize8(0);		break;			//[4] ,
		case ag_Grid_orientation:			pAttrClass = new  asCorientSpec8(0);		break; 			//[5] ,
		case ag_Grid_handling:				pAttrClass = new  asChandlingSpec8(0);		break;			//[6] ,
		case ag_Grid_members:				pAttrClass = new  asCvectorSpec8(0);		break;			//[7] ,
		case ag_Grid_item_info:				pAttrClass = new  asCitemInfo8(186);			break;			//[8] Item_Information		
		case ag_Grid_visible:				pAttrClass = new  asCboolSpec8(0);		break;

		case ag_Image_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Image) << 8,//[0] String_Specifier,
		case ag_Image_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[1] String_Specifier,
		case ag_Image_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//[2] Boolean_Specifier,
		case ag_Image_link:					pAttrClass = new  asCrefSpec8(0);			break;          //[3] Reference_Specifier,     
		case ag_Image_image_table_index:	pAttrClass = new  asCintegerSpec8(0);		break;			//[4] Integer_Specifier,
		case ag_Image_item_info:			pAttrClass = new  asCitemInfo8(186);			break;			//[5] Item_Information		
		case ag_Image_visible:				pAttrClass = new  asCboolSpec8(0);		break;

		case ag_Blob_label:					pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Blob)<< 8,//[  0] String_Specifier,
		case ag_Blob_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[  1] String_Specifier, 
		case ag_Blob_handling:				pAttrClass = new  asChandlingSpec8(0);		break;			//[  2] Handling_Specifier, 
		case ag_Blob_identity:				pAttrClass = new  asCasciiStr8(0);			break;			//[  3] ASCII_String
		case ag_Blob_item_info:				pAttrClass = new  asCitemInfo8(186);			break;			//[  4] Item_Information
			// ag_Blob_count  is an internal attribute and not found in a dd

		case ag_Temp_label:					pAttrClass = new  asCstrSpec8(0);			break;	//= ((int)it_Template) << 8,//[  0] String_Specifier,
		case ag_Temp_help:					pAttrClass = new  asCstrSpec8(0);			break;			//[  1] String_Specifier, 
		case ag_Temp_validity:				pAttrClass = new  asCboolSpec8(0);			break; 			//[  2] Boolean_Specifier,
		case ag_Temp_default_values:		pAttrClass = new  asCdefaultValuesList8();	break;			//[  2] Default_Values_List 
		case ag_Temp_item_info:				pAttrClass = new  asCitemInfo8(186);			break;			//[  3] Item_Information, -- debug only

		case ag_Plugin_label:				pAttrClass = new  asCstrSpec8(0);			break;			//= ((int)it_Blob) << 8,//[  0] String_Specifier,
		case ag_Plugin_help:				pAttrClass = new  asCstrSpec8(0);			break;			//[  1] String_Specifier, 
		case ag_Plugin_validity:			pAttrClass = new  asCboolSpec8(0);			break;			//[  2] Boolean_Specifier, 
		case ag_Plugin_visible:				pAttrClass = new  asCboolSpec8(0);			break;
		case ag_Plugin_uuid:				pAttrClass = new  asCuuid8();				break;			//[4] universally_unique_identifier
		case ag_Plugin_item_info:			pAttrClass = new  asCitemInfo8(186);			break;			

		default:							pAttrClass = new  asCunknownType8(0);		break;
	}// end switch

	pAttrClass->attrType   = genericAttr;

	return pAttrClass;
}
