/**********************************************************************************************
 *
 * $Workfile: FMx_handler.cpp $
 * 31aug11 - steveh
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_handler class implementation...
 *		
 *
 */

#include "FMx_handler.h"
#include "FMx_general.h"
#include "logging.h"

#include "FMx_Support_primatives.h"
#include "FMA_defs.h"
#include "FMx_handler.h"
#include "FMA_Support_tables.h"
#include "FMA_Attributes.h"
#include "FMA_Attributes\asCboolSpecA.h"
#include "FMA_Attributes\asChandlingSpecA.h"
#include "FMA_Attributes\asCstrSpecA.h"
#include "FMA_Attributes\asCchartTypeSpecA.h"
#include "FMA_Attributes\asCdisplaySizeA.h"
#include "FMA_Attributes\asCexprSpecA.h"
#include "FMA_Attributes\asCorientSpecA.h"
#include "FMA_Attributes\asCaxisScalSpecA.h"


int FMx_handler::countSetBits(unsigned msk)
{
	int c = 0;

	while ( msk )   // not zero
	{
		if ( msk & 0x01 ) c++;	// low bit set, count it
		msk>>= 1;				//              shift it
	}//wend                                     next

	return c;
}

bool FMx_handler::hasAttrType( vector<FMx_Attribute *>&attrlist, unsigned atype )
{
	vector<FMx_Attribute *>::iterator at;
	for (at = attrlist.begin(); at != attrlist.end(); at++)
	{
		FMx_Attribute* pObj = (FMx_Attribute*)(*at);

		if (pObj->attrType == atype)
			return true;
	}

	return false;
}

void FMx_handler::generateDefaultPrototype( FMx_Attribute *pattr )
{
	aCattrBase *abase = pattr->generatePrototype();
	if (abase)
	{
		abase->attr_mask = maskFromInt(pattr->attrType & 0xff);// we don't want the item type
		protoItem->attrMask |= abase->attr_mask;
		protoItem->attrLst.push_back(abase);		
	}
	DESTROY(pattr);
}

void FMx_handler::generatePrototypeDefaultAttrs(vector <FMx_Attribute *> &attrlist)
{
	if (protoItem->itemType == VARIABLE_ITYPE)
	{
		if ((protoItem->attrMask & VAR_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Var_validity, true));

		if ((protoItem->attrMask & VAR_HANDLING) == 0)
			generateDefaultPrototype(asChandlingSpecA::generateDefault(ag_Var_handling));
	}

	if (protoItem->itemType == MENU_ITYPE)
	{
		if ((protoItem->attrMask & MENU_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Menu_validity, true));
	}

	if (protoItem->itemType == EDIT_DISP_ITYPE)
	{
		if ((protoItem->attrMask & EDIT_DISPLAY_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_EdDisp_validity, true));
	}

	if (protoItem->itemType == METHOD_ITYPE)
	{
		if ((protoItem->attrMask & METHOD_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Meth_validity, true));

		if ((protoItem->attrMask & METHOD_HELP) == 0)
			generateDefaultPrototype(asCstrSpecA::generateDefault(ag_Meth_help));
	}

	if (protoItem->itemType == ITEM_ARRAY_ITYPE)
	{
		if ((protoItem->attrMask & ITEM_ARRAY_VALIDITY) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_RefArr_validity, true));
	}

	if (protoItem->itemType == COLLECTION_ITYPE)
	{
		if ((protoItem->attrMask & COLLECTION_VALIDITY) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Coll_validity, true));
	}

	if (protoItem->itemType == ARRAY_ITYPE)
	{
		if ((protoItem->attrMask & ARRAY_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_ValArr_validity, true));
	}

	if (protoItem->itemType == CHART_ITYPE)
	{
		if ((protoItem->attrMask & CHART_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Chart_validity, true));

		if ((protoItem->attrMask & CHART_TYPE) == 0)
			generateDefaultPrototype(asCchartTypeSpecA::generateDefault(ag_Chart_type));

		if ((protoItem->attrMask & CHART_HEIGHT) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Chart_height));

		if ((protoItem->attrMask & CHART_WIDTH) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Chart_width));

		if ((protoItem->attrMask & CHART_LENGTH) == 0)
			generateDefaultPrototype(asCexprSpecA::generateDefault(ag_Chart_length, 600000));

		if ((protoItem->attrMask & CHART_CYCLETIME) == 0)
			generateDefaultPrototype(asCexprSpecA::generateDefault(ag_Chart_cycle_time, 1000));
	}

	if (protoItem->itemType == GRAPH_ITYPE)
	{
		if ((protoItem->attrMask & GRAPH_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Graph_validity, true));

		if ((protoItem->attrMask & GRAPH_HEIGHT) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Graph_height, MEDIUM_DISPSIZE));

		if ((protoItem->attrMask & GRAPH_WIDTH) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Graph_width, MEDIUM_DISPSIZE));
	}

	if (protoItem->itemType == AXIS_ITYPE)
	{
		if (!QANDA)
		{
			// the old parser did not generate axis validity and neither did the new FMA parser
			//  so this default is new [2/19/2016 tjohnston]
			if ((protoItem->attrMask & AXIS_VALID) == 0)
				generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Axis_validity, true));
		}

		if ((protoItem->attrMask & AXIS_SCALING) == 0)
			generateDefaultPrototype(asCaxisScalSpecA::generateDefault(ag_Axis_scaling, LINEAR_SCALE));
	}

	if (protoItem->itemType == WAVEFORM_ITYPE)
	{
		if ((protoItem->attrMask & WAVEFORM_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Wave_validity, true));

		if ((protoItem->attrMask & WAVEFORM_HANDLING) == 0)
			generateDefaultPrototype(asChandlingSpecA::generateDefault(ag_Wave_handling));

		if ((protoItem->attrMask & WAVEFORM_EMPHASIS) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Wave_emphasis, true));
	}

	if (protoItem->itemType == LIST_ITYPE)
	{
		if ((protoItem->attrMask & LIST_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_List_validity, true));
	}

	if (protoItem->itemType == SOURCE_ITYPE)
	{
		if ((protoItem->attrMask & SOURCE_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Source_validity, true));

		if ((protoItem->attrMask & SOURCE_EMPHASIS) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Source_emphasis, true));
	}

	if (protoItem->itemType == IMAGE_ITYPE)
	{
		if ((protoItem->attrMask & IMAGE_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Image_validity, true));
	}

	if (protoItem->itemType == GRID_ITYPE)
	{
		if ((protoItem->attrMask & GRID_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Grid_validity, true));

		if ((protoItem->attrMask & GRID_HEIGHT) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Grid_height, MEDIUM_DISPSIZE));

		if ((protoItem->attrMask & GRID_WIDTH) == 0)
			generateDefaultPrototype(asCdisplaySize::generateDefault(ag_Grid_width, MEDIUM_DISPSIZE));

		if ((protoItem->attrMask & GRID_ORIENT) == 0)
			generateDefaultPrototype(asCorientSpecA::generateDefault(ag_Grid_orientation, ORIENT_VERT));

		if ((protoItem->attrMask & GRID_HANDLING) == 0)
			generateDefaultPrototype(asChandlingSpecA::generateDefault(ag_Grid_handling));
	}

	if (protoItem->itemType == PLUGIN_ITYPE)
	{
		if ((protoItem->attrMask & PLUGIN_VALID) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Plugin_validity, true));
	}

	if (protoItem->itemType == TEMPLATE_ITYPE)
	{
		if ((protoItem->attrMask & TEMPLATE_VALIDITY) == 0)
			generateDefaultPrototype(asCboolSpecA::generateDefault(ag_Temp_validity, true));
	}
}

int FMx_handler::reconcileOut(void)
{
	LOGIT(COUT_LOG,"    Table Output Inhibited for -R reconcile option\n");
	return 0;
}
