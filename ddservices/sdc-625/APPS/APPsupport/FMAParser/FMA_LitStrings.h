/**********************************************************************************************
 *
 * $Workfile: FMA_LitStrings.h $
 * 20Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_LitStrings.h : strings and their list class descriptions file
 *
 * #include "FMA_LitStrings.h"
 *
 */

#ifndef _FMA_LITSTRINGS_H
#define _FMA_LITSTRINGS_H

#ifdef INC_DEBUG
#pragma message("In FMA_LitStrings.h") 
#endif

#include "FMx_Dict.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_LitStrings.h") 
#endif


class FMA_LitStrings : public FMx_Dict
{
public:		
	FMA_LitStrings();  // constructor
	virtual ~FMA_LitStrings();

public:		// workers
	virtual int retrieveStr(int  str,   wstring& retVal);
	virtual void setTheTable( FMx_Table* pTbl);

protected:	// helpers

};



#endif	// _FMA_LITSTRINGS_H