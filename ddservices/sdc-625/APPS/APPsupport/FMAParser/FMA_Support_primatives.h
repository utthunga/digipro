/**********************************************************************************************
 *
 * $Workfile: FMA_Support_primatives.h $
 * 19mar12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Support_primatives.h : These are basic functions (mostly global) like parse int
 *
 * #include "FMA_Support_primatives.h"
 *
 */

#ifndef _FMA_SUPP_PRIMATIVES_H
#define _FMA_SUPP_PRIMATIVES_H

#ifdef INC_DEBUG
#pragma message("In FMA_Support_primatives.h") 
#endif

#include "varient.h"
#include "foundation.h"

#include "FMx_general.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_Support_primatives.h") 
#endif

#endif //_FMA_SUPP_PRIMATIVES_H