/**********************************************************************************************
 *
 * $Workfile: FM8_handlers.cpp $
 * 07sep11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_handlers class implementation
 *		
 *
 */

#include <memory.h>
#include "logging.h"

#include "FM8_handlers.h"
#include "FM8_Object.h"
#include "endian.h"
#include "FMx_Support_primatives.h"
#include "FM8_Attributes\asCvarType8.h"
#include "FM8_AttributeClass.h"
#include "FM8_handlers.h"
#include "v_N_v+Debug.h"

//#include "ddbPrimatives.h"  // to get default string descriptions
// temporary till the string handling gets fixed
#define DEFAULT_STD_DICT_HELP           (unsigned long)((400 << 16) + 2)


#include "dllapi.h"

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );

extern unsigned currentItemSymbolID;

/* 
 * in reconcile mode only, the PTOC table must go out with the device tables.  They are owned by different
 * objects and rigging communication between the 2 objects is difficult, so we have this pathological 
 * connection the out function of reconcile mode only.  The 
 */
static segment_t *pPtocSegment = 0;
static UCHAR **ppPtocData = 0;

void outptoclist(UCHAR* pTOC, unsigned len);
void fm8PtocReconcileOut(UCHAR* pTOC, unsigned len);

#define  LOG_OFFSET_AND_LEN( s,o,l )    LOGIT(COUT_LOG,"\t%s (offset:0x%06x   size:0x%04x)\n", s,\
	(SUPPRESS_HEXDUMP)?0:o, (SUPPRESS_HEXDUMP)?0:l);
#define LOG_OFF_ACT_AND_LEN( s,o,a,l )  LOGIT(COUT_LOG,"\t%s (offset:0x%06x [0x%08x] size:0x%04x)\n", s,\
	(SUPPRESS_HEXDUMP)?0:o, (SUPPRESS_HEXDUMP)?0:a, (SUPPRESS_HEXDUMP)?0:l);

/*********************************************************************************************
 *	readSegment
 *
 *	in: the segment offset,length
 *      pointer to the pointer that will hold the raw data
 *      the offset of the first byte in the heap...reference to be updated
 *
 *	returns: error code or zero on success 
 *		// allocate memory, read the segment, update the offset
 ********************************************************************************************/
int FM8_handler::readSegment(segment_t seg, UCHAR** ppTbl, unsigned& offset)
{
	unsigned  r = 0;
	UCHAR* pTbl = * ppTbl;
	if(seg.length == 0 || ppTbl == NULL )
	{
		return FMx_BADPARAM;
	}
	
	pTbl = *ppTbl = new UCHAR[seg.length];

	r = fread(pTbl, 1, seg.length, pBObj->fp);
	if ( r != seg.length)
	{
		return FMx_READFAIL;
	}
	offset += r;
	return 0;//success
}



/*============================================================================================
  = ODES handler
  ==========================================================================================*/

/*********************************************************************************************
 *	evaluateExtension
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// breaks up the extension part
 ********************************************************************************************/

int FM8_ODES_handler::evaluateExtension()
{// this does nothing now since the evaluation was done at index time so we could use the data
	return 0;
}

int FM8_ODES_handler::evaluate__Extension()
{
	FM8_ODES* pObj = (FM8_ODES*)pBObj;// up-cast
	int shSz = sizeof(unsigned short);
	int lgSz = sizeof(int);
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 10 )
	{
		return FMx_BADPARAM;
	}
	unsigned char* pC = pObj->obj_payload;	

	index   = REVERSE_S( *((WORD*)(&(pC[DD_ODES_INDEX_OFFSET]))) ); 
	version = REVERSE_S( *((WORD*)(&(pC[VERSION_OFFSET]))) );  

	local_address_ODES = REVERSE_L(*((DWORD*)(&(pC[LOC_ADDR_ODES_OFFSET]))));

	firstItemIndex = REVERSE_S( *((WORD*)(&(pC[SOD_FIRST_INDX_OFFSET]))) ); 
	item_count     = REVERSE_S( *((WORD*)(&(pC[SOD_LENGTH_OFFSET]))) );     

	local_address_sod = REVERSE_L(*((DWORD*)(&(pC[LOC_ADDR_SOD_OFFSET])))); 
	item_count     = (unsigned) REVERSE_S( *((WORD*)(&(pC[SOD_LENGTH_OFFSET]))) );     

	if (firstItemIndex != 100)
	{
		LOGIT(CERR_LOG|CLOG_LOG,"Failure: incorrect starting index number '%u'\n",firstItemIndex);
		delete[] pObj->obj_payload;
		pObj->obj_payload = NULL;
		return FMx_OPPFAIL;
	}

	if (item_count >= MAX_SOD)  // 0x4000, well short of a short
	{// document this
		LOGIT(CERR_LOG|CLOG_LOG,"Failure: Too many SOD items in the binary.(%d)\n",item_count);
		delete[] pObj->obj_payload;
		pObj->obj_payload = NULL;
		return FMx_OPPFAIL;
	}

	if (reconcile == false)
	{
		DEBUGLOG(COUT_LOG,"Number of SOD Objects: %d\n",item_count);
		DEBUGLOG(COUT_LOG,"Local Address SOD: 0x%04x\n",local_address_sod);
	}
	
	pObj->pTopClass->idx = firstItemIndex;// the others know their number.

	return SUCCESS;
}

int FM8_ODES_handler::out(void)
{
	FM8_ODES* pObj = (FM8_ODES*)pBObj;// up-cast
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 41 )
	{
		return FMx_BADPARAM;
	}
	
	if ( pObj->obj_payload != NULL )
	{
		outHex(pObj->obj_payload, local_address_ODES, pObj->obj_payload_size );
	}

	
	LOGIT(COUT_LOG,"           index: %4hu         [normally 0]\n",index);
	LOGIT(COUT_LOG,"    DDOD version: %4hu         [normally 2]\n",version,version); 
	LOGIT(COUT_LOG,"offset to item 0: %4u  (0x%x) [file offset to this section]\n", 
													  local_address_ODES, local_address_ODES);  
	LOGIT(COUT_LOG,"\n"); 	
	LOGIT(COUT_LOG," first SOD index: %4hu         [normally 100]\n",firstItemIndex);
	LOGIT(COUT_LOG,"SOD object count: %4hu  (0x%hx)\n",item_count,item_count);
	LOGIT(COUT_LOG,"   offset to SOD: %4hu  (0x%hx) [file offset to first SOD object]\n",
													  local_address_sod,local_address_sod);
	LOGIT(COUT_LOG,"\n* The rest of the DDOD_OD items in a HART DD are set to default.\n");   

	return 0;// success
}


void FM8_ODES_handler::out3(void)
{
	// we haven't got this far yet
}



/*============================================================================================
  = Format handler
  ==========================================================================================*/

/*********************************************************************************************
 *	evaluateExtension
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// breaks up the extension part
 ********************************************************************************************/
int FM8_Format_handler::evaluateExtension()
{
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 10 )
	{
		return FMx_BADPARAM;
	}
	unsigned char* pC = pObj->obj_payload;
	
	objCode			= *pC++; // will be 128, x80
	major_rev		= *pC++; 
	minor_rev		= *pC++;
	dd_rev			= *pC++;
	
	profile		= REVERSE_S( *((unsigned short*)pC) ); pC += sizeof(unsigned short);
	import_count= REVERSE_S( *((unsigned short*)pC) ); pC += sizeof(unsigned short);
	like_count	= REVERSE_S( *((unsigned short*)pC) ); pC += sizeof(unsigned short); 
	
	pObj->itemIndex = 1;
	return 0;
}
/*********************************************************************************************
 *	out
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// sends text version to out
 ********************************************************************************************/

int FM8_Format_handler::out(void)
{
	if (reconcile == false)
	{
		LOGIT(COUT_LOG,"----Extension section of Format Object----\n");
	
		LOGIT(COUT_LOG,"\tobject code: %d\n",(int) objCode);// will be 128 0x80
		if (!SUPPRESS_SIGNATURE)
		{
			LOGIT(COUT_LOG,"\t  major rev: %d\n",(int) major_rev);// will be eight 
			LOGIT(COUT_LOG,"\t  minor rev: %d\n",(int) minor_rev);
		}
		LOGIT(COUT_LOG,"\t     DD rev: %d\n",(int) dd_rev); 
		LOGIT(COUT_LOG,"\t    Profile: %d\n",(int) profile); 
		LOGIT(COUT_LOG,"\t    Imports: %d\n",(int) import_count); 
		LOGIT(COUT_LOG,"\t      Likes: %d\n",(int) like_count); 
	}
	return 0; // success
}


void FM8_Format_handler::out3(void)
{
	// no-op, format has no class information
}


/*============================================================================================
  = Device Directory handler
  ==========================================================================================*/

FM8_DevDir_handler::
FM8_DevDir_handler(FM8_Object* pOwner) : pObj(pOwner), FM8_handler((FMx_Base*)pOwner) 
 {
	 memset(devDirTblInfo,   0, sizeof(devDirTblInfo));
	 memset(devDirTables,    0, sizeof(devDirTables));
     memset(devDirTblOffset, 0, sizeof(devDirTblOffset));
 };
 FM8_DevDir_handler::~FM8_DevDir_handler()
 {
	 for (int i = 0; i < BLK_DIR_TBL_CNT; i++)
	 {
		 if (devDirTables[i] != 0)
		 {
			 delete devDirTables[i];
			 devDirTables[i] = 0;
		 }
	 }
 };
/*********************************************************************************************
 *	readExtension
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// breaks up the extension part
 ********************************************************************************************/
#define evaluateSegment(i,p) \
	devDirTblInfo[i].offset = REVERSE_L( *((DWORD *)p) ); p += sizeof(unsigned);\
	devDirTblInfo[i].length = REVERSE_L( *((DWORD *)p) ); p += sizeof(unsigned)

int FM8_DevDir_handler::evaluateExtension()
{
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 42 )
	{
		return FMx_BADPARAM;
	}
	if (  pObj->extProcessed ) // then we're on the second pass
	{		
		pObj->itemIndex = 2;

		// we'll do dictionary here
		int                left = dict_segment.length;
		unsigned char *    pData= dict_data;
		dictStringList.parse(&pData, left);
	

		//  and do the literal string table too 
		left = string_segment.length;
		pData= string_data;

		literalStringList.parse(&pData, left);
		
		//  and do the image table next
		left = image_segment.length;
		pData= image_data;

		imageList.parse(&pData, left);

	}
	else // first pass to do the extension only
	{
		unsigned char* pC = pObj->obj_payload;
		
		objCode			= *pC++; // will be 129, x81
		fmtCode			= *pC++;;// will be zero 
		evaluateSegment( item_table_idx, pC );
		evaluateSegment( string_tbl_idx, pC );
		evaluateSegment( dictionary_idx, pC );
		evaluateSegment(  cmd_table_idx, pC );
		evaluateSegment(  image_tbl_idx, pC );

		 pObj->extProcessed = true;
	}
	
	return 0;
}

/*********************************************************************************************
 *	readChunk
 *
 *	in: nothing
 *
 *	returns: bytes read or zero on error 
 *		// read item at the current location, it's one of ours
 ********************************************************************************************/
int FM8_DevDir_handler::readChunk(unsigned& sz)
{
	int r = 0, k;
	unsigned heapOffset = ftell(pObj->fp)-(pObj->pTopClass->headerData.getOffset2Heap());

	// find offset actually finds the correct able offset out of the set from fixed portion
	k = findOffset(heapOffset); // k is a handle/index
	if (k < 0)
	{// error, exit
		return k;
	}	
	devDirTblInfo[k].actualoffset = ftell(pObj->fp);
	r = readSegment(devDirTblInfo[k],&(devDirTables[k]),heapOffset);
	if ( r )
	{// erro exit
		return r;
	}
	else
	{
		sz -= devDirTblInfo[k].length;
	}

	k = findOffset(heapOffset); // k is a handle
	if (k < 0)
	{// exit
		return k;
	}	
	devDirTblInfo[k].actualoffset = ftell(pObj->fp);	
	r = readSegment(devDirTblInfo[k],&(devDirTables[k]),heapOffset);
	if ( r )
	{// erro exit
		return r;
	}
	else
	{
		sz -= devDirTblInfo[k].length;
	}

	k = findOffset(heapOffset); // k is a handle
	if (k < 0)
	{// exit
		return k;
	}	
	devDirTblInfo[k].actualoffset = ftell(pObj->fp);	
	r = readSegment(devDirTblInfo[k],&(devDirTables[k]),heapOffset);
	if ( r )
	{// erro exit
		return r;
	}
	else
	{
		sz -= devDirTblInfo[k].length;
	}

	k = findOffset(heapOffset); // k is a handle
	if (k < 0)
	{// exit
		return k;
	}	
	devDirTblInfo[k].actualoffset = ftell(pObj->fp);	
	r = readSegment(devDirTblInfo[k],&(devDirTables[k]),heapOffset);
	if ( r )
	{// erro exit
		return r;
	}
	else
	{
		sz -= devDirTblInfo[k].length;
	}

	k = findOffset(heapOffset); // k is a handle
	if (k < 0)
	{// exit
		return k;
	}	
	devDirTblInfo[k].actualoffset = ftell(pObj->fp);	
	r = readSegment(devDirTblInfo[k],&(devDirTables[k]),heapOffset);
	if ( r )
	{// erro exit
		return r;
	}
	else
	{
		sz -= devDirTblInfo[k].length;
	}


	return 0;
}

/*********************************************************************************************
 *	findOffset
 *
 *	in: nothing
 *
 *	returns: location or zero if not found 
 *		// tells what to do with the current location
 ********************************************************************************************/
int FM8_DevDir_handler::findOffset(unsigned offset)
{
	if (offset == devDirTblInfo[item_table_idx].offset)  return  (int)item_table_idx;
	else
	if (offset == devDirTblInfo[string_tbl_idx].offset)  return  (int)string_tbl_idx;
	else
	if (offset == devDirTblInfo[dictionary_idx].offset)  return  (int)dictionary_idx;
	else
	if (offset == devDirTblInfo[ cmd_table_idx].offset)  return  (int) cmd_table_idx;
	else
	if (offset == devDirTblInfo[ image_tbl_idx].offset)  return  (int) image_tbl_idx;
	else
		return -1;
}

/*********************************************************************************************
 *	getFm8TableRows
 *
 *	in: p - pointer into the item data
 *
 *	returns: the number of rows in the data
 *		// no side effects
 ********************************************************************************************/

int FM8_handler::getTableRows( UCHAR* p)
{
	// number of rows is the encoded integer at the beginning of the table
	int remaining = 4;
	UINT64 tempLongLong;
	unsigned status = parseInteger(4, &p, remaining, tempLongLong);
	return (int) tempLongLong;
}

void FM8_handler::out5( UCHAR *p, char *stype )
{
	if (reconcile==false)
	{
		if (p)
		{
			int n = reconcile ? 0 : getTableRows(p);	//  [6/24/2015 timj]
			LOGIT(COUT_LOG,"%23s: (rows:0x%04x)\n", stype, n); 
		}
		else
		{
			string t = stype;
			int r = reconcile ? 0 : rand();
			if (reconcile &&  t=="Critical Item")
				LOGIT(COUT_LOG,"%23s: (rows:0x%04x)\n", stype, 0); 
			else
				LOGIT(COUT_LOG,"%23s: not found %d\n", stype, r);
		}
	}
}
/*********************************************************************************************
 *	out
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// sends text version to out
 ********************************************************************************************/

int FM8_DevDir_handler::out(void)
{
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 42 )
	{
		return FMx_BADPARAM;
	}
	if ( reconcile ||  maintenance)
	{
		out5(string_data, "String");
		out5(dict_data, "Dictionary"); 
		out5(image_data, "Image");
		out5(item_data, "Item Info");
		out5(*ppPtocData, "Item2Command");

		if(ppPtocData  &&  *ppPtocData  &&  pPtocSegment)
		{
			LOGIT(COUT_LOG,"%23s:\n", "PTOC table found");				// use this line for 8vA [7/1/2015 timj]
			// fm8PtocReconcileOut(*ppPtocData, pPtocSegment->length);	// use thie line for 8v8 or AvA [7/1/2015 timj] 
		}
		else if (!ppPtocData)
		{
			LOGIT(COUT_LOG,"%23s:\n", "PTOC table *not* found");		// use this line for 8vA [7/1/2015 timj]
		}
	}	
	else
	{
		LOGIT(COUT_LOG,"----Extension section of Device Directory Object----\n");

		LOGIT(COUT_LOG,"\tobject code: %d\n",(int) objCode);// will be 129, x81
		LOGIT(COUT_LOG,"\tformat code: %d\n",(int) fmtCode);// will be zero 
		LOGIT(COUT_LOG,"\t   Item Table: (offset:0x%06x  size:0x%04x)\n\n",   /*usually zero*/
													item_segment.offset,   item_segment.length);
		outHex( item_data, item_segment.actualoffset, item_segment.length );
		LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

		LOGIT(COUT_LOG,"\t String Table: (offset:0x%06x  size:0x%04x)\n",
													string_segment.offset, string_segment.length); 
		//outHex( string_data, string_segment.actualoffset, string_segment.length );
		LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

		LOGIT(COUT_LOG,"\t   Dictionary: (offset:0x%06x  size:0x%04x)\n",
													dict_segment.offset,   dict_segment.length); 
		//outHex( dict_data, dict_segment.actualoffset, dict_segment.length );
		LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");


		LOGIT(COUT_LOG,"\tCommand Table: (offset:0x%06x  size:0x%04x)\n",
													command_segment.offset,command_segment.length);
		outHex( command_data, command_segment.actualoffset, command_segment.length );
		LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

		LOGIT(COUT_LOG,"\t  Image Table: (offset:0x%06x  size:0x%04x)\n",
													image_segment.offset,  image_segment.length);
		outHex( image_data, image_segment.actualoffset, image_segment.length );
	}

	literalStringList.out(string_segment.actualoffset, string_data, string_segment.length);
	dictStringList   .out(dict_segment.actualoffset,   dict_data,   dict_segment.length );


	return 0;// success
}
void FM8_DevDir_handler::out2(void)// does the annex
{
	LOGIT(COUT_LOG,"   Item Table:   size = 0x%04x \n", item_segment.length);
		outHex(devDirTables[item_table_idx], item_segment.actualoffset, item_segment.length);
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n");

	LOGIT(COUT_LOG," String Table:   size = 0x%04x\n",string_segment.length);
		outHex(devDirTables[string_tbl_idx],string_segment.actualoffset,string_segment.length);
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n");

	LOGIT(COUT_LOG,"   Dictionary:   size = 0x%04x\n", dict_segment.length);
		outHex(devDirTables[dictionary_idx], dict_segment.actualoffset, dict_segment.length);
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n");

	LOGIT(COUT_LOG,"Command Table:   size = 0x%04x \n",command_segment.length);
	   outHex(devDirTables[cmd_table_idx],command_segment.actualoffset,command_segment.length);
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n");

	LOGIT(COUT_LOG,"  Image Table:   size = 0x%04x\n", image_segment.length);
	   outHex(devDirTables[image_tbl_idx], image_segment.actualoffset, image_segment.length);
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n");
};

void FM8_DevDir_handler::out3(void)
{
	// we haven't got this far yet
}



/*============================================================================================
  = Block Directory handler
  ==========================================================================================*/

FM8_BlkDir_handler::
FM8_BlkDir_handler(FM8_Object* pOwner) : pObj(pOwner), FM8_handler((FMx_Base*)pOwner) 
 {
	 memset(blkDirTblInfo,   0, sizeof(blkDirTblInfo));
	 memset(blkDirTables,    0, sizeof(blkDirTables));
     memset(blkDirTblOffset, 0, sizeof(blkDirTblOffset));

	 // for reconcile output
	 pPtocSegment = &(blkDirTblInfo[param_cmd_idx]);
	 ppPtocData = &(blkDirTables[param_cmd_idx]);
 };

 FM8_BlkDir_handler::~FM8_BlkDir_handler()
 {
	 for (int i = 0; i < BLK_DIR_TBL_CNT; i++)
	 {
		 if (blkDirTables[i] != 0)
		 {
			 delete blkDirTables[i];
			 blkDirTables[i] = 0;
		 }
	 }
 };

/*********************************************************************************************
 *	readExtension
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// breaks up the extension part
 ********************************************************************************************/
#define evaluateBlkSegment(i,p) \
	blkDirTblInfo[i].offset = REVERSE_L( *((DWORD *)p) ); p += sizeof(unsigned);\
	blkDirTblInfo[i].length = REVERSE_L( *((DWORD *)p) ); p += sizeof(unsigned)

 int FM8_BlkDir_handler::evaluateExtension()
{
	int                left; 
	unsigned char *    pData;
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 42 )
	{
		return FMx_BADPARAM;
	}
	if (  pObj->extProcessed ) // then we're on the second pass
	{
		pObj->itemIndex = 3;

		LOGIT(CLOG_LOG,">>> Need to implement Block Directory Heap data evaluation.\n");
		
		left = critical_param_segment.length;
		pData= critical_param_data;
		if (pData)
		{
			critParamTable.parse(&pData, left);
		}
		
		left = rslvdRef_cmd_segment.length;
		pData= rslvdRef_cmd_data;
		if (pData)
		{
			RRptocTable.parse(&pData, left);
		}
	}
	else // first pass to do the extension only
	{
		unsigned char* pC = pObj->obj_payload;
		unsigned int   sz = pObj->obj_payload_size;
		
		objCode			= *pC++; sz--;// will be 130, x82
		fmtCode			= *pC++; sz--;// will be zero 
int z = BLK_DIR_TBL_CNT;
		for (int k = blk_item_idx; 
			 k < BLK_DIR_TBL_CNT && sz >= (sizeof(unsigned) * 2); k++)
		{
			evaluateBlkSegment( k, pC );// one at a time in blkDirTblIndex_e order
			sz -=(sizeof(unsigned) * 2);
		}

		 pObj->extProcessed = true;
	}
	
	return 0;
}
 
/*********************************************************************************************
 *	readChunk
 *
 *	in: nothing
 *
 *	returns: bytes read or zero on error 
 *		// read item at the current location, it's one of ours
 ********************************************************************************************/
int FM8_BlkDir_handler::readChunk(unsigned& sz)
{
	int r = 0, k = 0, c = 0;
	unsigned heapOffset = ftell(pObj->fp)-(pObj->pTopClass->headerData.getOffset2Heap());

	while ( (k = findOffset(heapOffset - pObj->obj_annex_offset)) >= 0 )
	{																// lookup, assign and test
		blkDirTblInfo[k].actualoffset = ftell(pObj->fp);
		r = readSegment(blkDirTblInfo[k],&(blkDirTables[k]),heapOffset);
		if ( r )
		{// erro exit
			return -1;
		}
		else
		{
			sz -= blkDirTblInfo[k].length;
		}
		c++;
	}// wend


	return 0;
}
/*********************************************************************************************
 *	findOffset
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// tells what to do with the current location
 ********************************************************************************************/
int FM8_BlkDir_handler::findOffset(unsigned offset)
{
	if (offset == blkDirTblInfo[blk_item_idx].offset)  return  (int)blk_item_idx;
	else
	if (offset == blkDirTblInfo[blk_item_nm_idx].offset)  return  (int)blk_item_nm_idx;
	else
	if (offset == blkDirTblInfo[parameter_idx].offset)  return  (int)parameter_idx;
	else
	if (offset == blkDirTblInfo[ relation_idx].offset)  return  (int) relation_idx;
	else
	if (offset == blkDirTblInfo[ update_idx].offset)  return  (int) update_idx;
	else
	if (offset == blkDirTblInfo[ param_cmd_idx].offset)  return  (int) param_cmd_idx;
	else
	if (offset == blkDirTblInfo[ critical_idx].offset)  return  (int) critical_idx;
	else
	if (offset == blkDirTblInfo[ resolvedRef_cmd_idx].offset)  return  (int) resolvedRef_cmd_idx;
	else
		return -1;
}

/*********************************************************************************************
 *	out
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// sends text version to out
 ********************************************************************************************/
void fm8PtocReconcileOut(UCHAR* pTOC, unsigned len)
{
	UINT64 tempLongLong;
	UCHAR* pLoc    = pTOC;
	int remaining  = (int)len;
	int status;
	int entryCnt = 0;
	int iD, cmdCnt, subidx,cnum,tnum,wgt,idxs,  idx,idxval;

	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return;// status;
	}
	//LOGIT(COUT_LOG,"\tReconcile PTOC Table: %d rows\n",(int) tempLongLong);
	LOGIT(COUT_LOG,
		"\n\n- * - * - * -> Reconcile PTOC Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");

	while (remaining > 0)
	{
		status = parseInteger(4, &pLoc, remaining, tempLongLong);// itemID
		if (status != SUCCESS)
		{
			return;// status;
		}
		iD = (int)tempLongLong;

		status = parseInteger(4, &pLoc, remaining, tempLongLong);// rdcmd cnt
		if (status != SUCCESS)
		{
			return;// status;
		}
		cmdCnt = (int)tempLongLong;
		LOGIT(COUT_LOG,"%4d]     SymbolID: 0x%04x\n",entryCnt++,iD);

		LOGIT(COUT_LOG,"        %d Read Commands:\n",cmdCnt);
		for (int i = 0; i < cmdCnt; i++)
		{
			status = parseInteger(4, &pLoc, remaining, tempLongLong);// subindex
			if (status != SUCCESS)
			{
				return;// status;
			}
			subidx = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// cmd #
			if (status != SUCCESS)
			{
				return;// status;
			}
			cnum = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// trans #
			if (status != SUCCESS)
			{
				return;// status;
			}
			tnum = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// weight
			if (status != SUCCESS)
			{
				return;// status;
			}
			wgt = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// index cnt
			if (status != SUCCESS)
			{
				return;// status;
			}
			idxs = (int)tempLongLong;
			if (reconcile) wgt=0;	// 8.1 v 8.2 compare [12/18/2014 timj]
			LOGIT(COUT_LOG,"\t\t%4d> Cmd %3u:%u\tWeight: %4u\n", i+1/*subidx*/, cnum, tnum, wgt);

			idx,idxval;
			for ( int k = 0; k < idxs; k++)
			{
				status = parseInteger(4, &pLoc, remaining, tempLongLong);// weight
				if (status != SUCCESS)
				{
					return;// status;
				}
				idx = (int)tempLongLong;

				status = parseInteger(4, &pLoc, remaining, tempLongLong);// index cnt
				if (status != SUCCESS)
				{
					return;// status;
				}
				idxval = (int)tempLongLong;

				if (reconcile == false)	// 8.1 v 8.2 compare [12/18/2014 timj]
				{
					LOGIT(COUT_LOG,"\t\t\t%2d} Index SymbolID: 0x%04x   Index Value: %3d \n",
						k+1, idx, idxval);
				}
			}

		}

		status = parseInteger(4, &pLoc, remaining, tempLongLong);// wrcmd cnt
		if (status != SUCCESS)
		{
			return;// status;
		}
		cmdCnt = (int)tempLongLong;
		LOGIT(COUT_LOG,"        %d Write Commands:\n",cmdCnt);

		for (int i = 0; i < cmdCnt; i++)
		{
			status = parseInteger(4, &pLoc, remaining, tempLongLong);// subindex
			if (status != SUCCESS)
			{
				return;// status;
			}
			subidx = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// cmd #
			if (status != SUCCESS)
			{
				return;// status;
			}
			cnum = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// trans #
			if (status != SUCCESS)
			{
				return;// status;
			}
			tnum = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// weight
			if (status != SUCCESS)
			{
				return;// status;
			}
			wgt = (int)tempLongLong;

			status = parseInteger(4, &pLoc, remaining, tempLongLong);// index cnt
			if (status != SUCCESS)
			{
				return;// status;
			}
			idxs = (int)tempLongLong;

			if (reconcile) wgt=0;	// 8.1 v 8.2 compare [12/18/2014 timj]
			LOGIT(COUT_LOG,"\t\t%4d> Cmd %3u:%u\tWeight: %4u\n", i+1/*subidx*/, cnum, tnum, wgt);

			idx,idxval;
			for ( int k = 0; k < idxs; k++)
			{
				status = parseInteger(4, &pLoc, remaining, tempLongLong);// weight
				if (status != SUCCESS)
				{
					return;// status;
				}
				idx = (int)tempLongLong;

				status = parseInteger(4, &pLoc, remaining, tempLongLong);// index cnt
				if (status != SUCCESS)
				{
					return;// status;
				}
				idxval = (int)tempLongLong;
				if (reconcile == false)	// 8.1 v 8.2 compare [12/18/2014 timj]
				{
					LOGIT(COUT_LOG,"\t\t\t%2d} Index SymbolID: 0x%04x   Index Value: %3d \n",
						k+1, idx, idxval);
				}
			}

		}
	}// end remaining loop

}


static const	char spaceone[] = " ";
static const	char spaceout[] = "                     ";

void outptocrow(UCHAR** pLoc, int &remaining, int rowNumber)
{
	UINT64 tempLongLong;
	int status;
	int subidx,cnum,tnum,wgt,idxcnt,  idx,idxval;

	
	status = parseInteger(4, pLoc, remaining, tempLongLong);// subindex
	if (status != SUCCESS)
	{
		return;// status;
	}
	if (SUPPRESS_EXTRAIDX)
	{
		subidx = 0;
	}
	else // normal
	{
		subidx = (int)tempLongLong;
	}

	status = parseInteger(4, pLoc, remaining, tempLongLong);// cmd #
	if (status != SUCCESS)
	{
		return;// status;
	}
	cnum = (int)tempLongLong;

	status = parseInteger(4, pLoc, remaining, tempLongLong);// trans #
	if (status != SUCCESS)
	{
		return;// status;
	}
	tnum = (int)tempLongLong;

	status = parseInteger(4, pLoc, remaining, tempLongLong);// weight
	if (status != SUCCESS)
	{
		return;// status;
	}
	wgt = (int)tempLongLong;

	status = parseInteger(4, pLoc, remaining, tempLongLong);// index cnt
	if (status != SUCCESS)
	{
		return;// status;
	}
	idxcnt = (int)tempLongLong;


	if ( SUPPRESS_WEIGHT )
	{
		wgt = 0;
	}
	LOGIT(COUT_LOG,"%s%2d> Cmd %3u  Trans: %3u Weight: %4u   Cmd Item Info Index 0x%04x \n",
		(rowNumber == 1)?spaceone:spaceout, 
		rowNumber, cnum, tnum, wgt, subidx);// subindex is always zero, I put it out to verify that

	for ( int k = 0; k < idxcnt; k++)
	{
		status = parseInteger(4, pLoc, remaining, tempLongLong);// indexID
		if (status != SUCCESS)
		{
			return;// status;
		}
		idx = (int)tempLongLong;

		status = parseInteger(4, pLoc, remaining, tempLongLong);// index value
		if (status != SUCCESS)
		{
			return;// status;
		}
		idxval = (int)tempLongLong;

		if ( ! SUPPRESS_EXTRAIDX  )// stevev 15jan16 - using '&& k>0' always suppresses the first index
		{
		LOGIT(COUT_LOG,"%s         %2d} Index SymbolID: 0x%04x   Index Value: %3d \n",
							spaceout, (k+1), idx, idxval);
		}
	}
}


void outptoclist(UCHAR* pTOC, unsigned len)
{
	UINT64 tempLongLong;
	UCHAR* pLoc    = pTOC;
	int remaining  = (int)len;
	int status;
	int entryCnt = 0;
	int iD, cmdCnt;

	if ( pTOC == 0 || len == 0 )
	{
		LOGIT(COUT_LOG,"\t>>>> PTOC is EMPTY <<<<\n");
		return;
	}
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return;// status;
	}
	LOGIT(COUT_LOG,"\tParam 2 Cmd Count: %d\n",(int) tempLongLong);

//	UCHAR   *IDstart  = NULL, *IDend = NULL;// debugging
//	unsigned IDlength = 0;

	while (remaining > 0)
	{
/*		if ( ! reconcile && ! maintenance)
		{
			IDend = pLoc;
			IDlength = IDend - IDstart;
			outHex( IDstart, 0, IDlength );
		}
		IDstart = pLoc;
*/
		status = parseInteger(4, &pLoc, remaining, tempLongLong);// itemID
		if (status != SUCCESS)
		{
			return;// status;
		}
		iD = (int)tempLongLong;

		status = parseInteger(4, &pLoc, remaining, tempLongLong);// rdcmd cnt
		if (status != SUCCESS)
		{
			return;// status;
		}
		cmdCnt = (int)tempLongLong;
	
		LOGIT(COUT_LOG,"%4d] ", entryCnt++);

		LOGIT(COUT_LOG," Resolved Reference: SymbolID: 0x%04x", iD);

		LOGIT(COUT_LOG,"\n %3d  Read Commands:", cmdCnt);
		
		if (cmdCnt < 1)
		{
			LOGIT(COUT_LOG," -- NONE --\n");
		}
		else
		{
			for (int i = 1; i <= cmdCnt; i++)
			{
				outptocrow(&pLoc, remaining, i);// output one row
			}
		}
		
	
		status = parseInteger(4, &pLoc, remaining, tempLongLong);// wrcmd cnt
		if (status != SUCCESS)
		{
			return;// status;
		}
		cmdCnt = (int)tempLongLong;
		LOGIT(COUT_LOG," %3d Write Commands:", cmdCnt);
		
		if (cmdCnt < 1)
		{
			LOGIT(COUT_LOG," -- NONE --\n");
		}
		else	
		{
			for (int i = 1; i <= cmdCnt; i++)
			{
				outptocrow(&pLoc, remaining, i);// output one row
			}
		}
	}// end remaining loop

}

int FM8_BlkDir_handler::out(void)
{
	if (reconcile ||  maintenance)
	{
		out5(critical_param_data, "Critical Item");

		critParamTable.out(0, 0, 0);

		return 0;
	}

	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 42 )
	{
		return FMx_BADPARAM;
	}
	LOGIT(COUT_LOG,"----Extension section of Block Directory Object----\n");
	outHex( pObj->obj_payload, pObj->obj_payload_offset(), pObj->obj_payload_size );

	LOGIT(COUT_LOG,"\tobject code: %d\n",(int) objCode);// will be 130, x82
	LOGIT(COUT_LOG,"\tformat code: %d\n",(int) fmtCode);// will be zero 

	LOGIT(COUT_LOG,"\t         Block Item Table: (offset:0x%06x [0x%08x] size:0x%04x)\n", 
			blk_item_segment.offset,   blk_item_segment.actualoffset, blk_item_segment.length); 
	//outHex( blk_item_data, blk_item_segment.actualoffset, blk_item_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\t    Block Item Name Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			blk_item_nm_segment.offset, blk_item_nm_segment.actualoffset, blk_item_nm_segment.length);  
	//outHex( blk_item_nm_data, blk_item_nm_segment.actualoffset, blk_item_nm_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\t                Parameter: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			param_segment.offset,  param_segment.actualoffset,  param_segment.length);  
	//outHex( param_data, param_segment.actualoffset, param_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\t           Relation Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			relation_segment.offset, relation_segment.actualoffset, relation_segment.length);  
	//outHex( relation_data, relation_segment.actualoffset, relation_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\t             Update Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			update_segment.offset,  update_segment.actualoffset, update_segment.length);  
	//outHex( update_data, update_segment.actualoffset, update_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\tParameter 2 Command Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			param_cmd_segment.offset, param_cmd_segment.actualoffset, param_cmd_segment.length);  
	//outHex( param_cmd_data, param_cmd_segment.actualoffset, param_cmd_segment.length );
	//outptoclist(param_cmd_data, param_cmd_segment.length);
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");

	LOGIT(COUT_LOG,"\t Critical Parameter Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			critical_param_segment.offset,  critical_param_segment.actualoffset, critical_param_segment.length); 
	//outHex( critical_param_data, critical_param_segment.actualoffset, critical_param_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");



	if (rslvdRef_cmd_segment.offset) // use 'if exist' not 'if has contents'..rslvdRef_cmd_segment.length)
	{
	LOGIT(COUT_LOG,"\t  ResolvedRef 2 Cmd Table: (offset:0x%06x [0x%08x] size:0x%04x)\n",
			rslvdRef_cmd_segment.offset,  rslvdRef_cmd_segment.actualoffset, rslvdRef_cmd_segment.length); 
	//outHex( rslvdRef_cmd_data, rslvdRef_cmd_segment.actualoffset, rslvdRef_cmd_segment.length );
	//LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");
	}
	LOGIT(COUT_LOG,"\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n\n");
	out2(); // annex section in 
	return 0;
}

void FM8_BlkDir_handler::out2(void)// does the annex
{
	LOGIT(COUT_LOG,"----  Annex section of Block Directory Object ----\n");

	
	LOGIT(COUT_LOG,"\t         Block Item Table: (offset:0x%06x  {size:0x%04x)\n", 
									blk_item_segment.offset,   blk_item_segment.length); 
	outHex( blk_item_data, blk_item_segment.actualoffset, blk_item_segment.length );
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	LOGIT(COUT_LOG,"\t    Block Item Name Table: (offset:0x%06x  size:0x%04x)\n",
									blk_item_nm_segment.offset, blk_item_nm_segment.length);  
	outHex( blk_item_nm_data, blk_item_nm_segment.actualoffset, blk_item_nm_segment.length );
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	LOGIT(COUT_LOG,"\t                Parameter: (offset:0x%06x  size:0x%04x)\n",
			param_segment.offset,  param_segment.actualoffset,  param_segment.length);  
	outHex( param_data, param_segment.actualoffset, param_segment.length );
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	LOGIT(COUT_LOG,"\t           Relation Table: (offset:0x%06x  size:0x%04x)\n",
			relation_segment.offset, relation_segment.length);  
	outHex( relation_data, relation_segment.actualoffset, relation_segment.length );
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	LOGIT(COUT_LOG,"\t             Update Table: (offset:0x%06x  size:0x%04x)\n",
									update_segment.offset,  update_segment.length);  
	outHex( update_data, update_segment.actualoffset, update_segment.length );
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	
	if (SUPPRESS_PTOC == false)
	{
		LOGIT(COUT_LOG,"\tParameter 2 Command Table: (offset:0x%06x  size:0x%04x)\n",
										param_cmd_segment.offset, param_cmd_segment.length);  
		outHex( param_cmd_data, param_cmd_segment.actualoffset, param_cmd_segment.length );
		outptoclist(param_cmd_data, param_cmd_segment.length);
		LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");
	}

	LOGIT(COUT_LOG,"\t Critical Parameter Table: (offset:0x%06x  size:0x%04x)\n",
								critical_param_segment.offset,  critical_param_segment.length); 
	outHex( critical_param_data, critical_param_segment.actualoffset, critical_param_segment.length );
	
	out5(critical_param_data, "Critical Item");
	critParamTable.out(0, 0, 0);
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");

	if (rslvdRef_cmd_segment.offset) // use 'if exist' not 'if has contents'..rslvdRef_cmd_segment.length)
	{
	LOGIT(COUT_LOG,"\t  ResolvedRef 2 Cmd Table: (offset:0x%06x  size:0x%04x)\n",
										rslvdRef_cmd_segment.offset,   rslvdRef_cmd_segment.length); 
	outHex( rslvdRef_cmd_data, rslvdRef_cmd_segment.actualoffset, rslvdRef_cmd_segment.length );
	RRptocTable.out(0, 0, 0);
	LOGIT(COUT_LOG,"\t . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n");
	}
	
	return ;	
};

void FM8_BlkDir_handler::out3(void)
{
	// we haven't got this far yet
}

/*============================================================================================
  = Item Object handler
  ==========================================================================================*/


FM8_Item_handler::FM8_Item_handler(FM8_Object* pOwner) : pObj(pOwner), 
   FM8_handler((FMx_Base*)pOwner),item_type(0),item_subtype(0),item_number(0),item_attr_mask(0)
{
	attrCntRemaining = 0;
};

FM8_Item_handler::~FM8_Item_handler()
{
};
/*********************************************************************************************
 *	readExtension
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// breaks up the extension part on the first pass
 *		// breaks up the annex part on the second pass
 ********************************************************************************************/
int FM8_Item_handler::evaluateExtension()
{
	int status = 0;
	unsigned acnt = attrCntRemaining; // for dgugginh purposes
	if ( pObj->obj_payload == NULL || pObj->obj_payload_size < 10 )
	{
		return FMx_BADPARAM;
	}
	if (  pObj->extProcessed ) // then we're on the second pass
	{
currentItemSymbolID = item_number;
		if (pObj->obj_annex_size > 0 || pObj->obj_annex_offset != 0xffffffff )
		{
			//LOGIT(CLOG_LOG,">>> Need to implement Item Heap data evaluation.\n");
		status =
		items_attributes.evaluateAttributePayload(pObj->obj_annex_payload,pObj->obj_annex_size,
											pObj->objHeapStartOffset, attrCntRemaining, true );
		//was								  pObj->obj_annex_offset, attrCntRemaining, true );
			if (status == FMx_IS_ITMINFO)// we encountered an item information attribute
			{	//	in reconcile mode and removed it
				item_attr_cnt--;
			}
		}
	}
	else // first pass to do the extension only
	{
		int szUINT = sizeof(unsigned);
		int offset = pObj->obj_payload_offset();
		unsigned char* pC = pObj->obj_payload;
		unsigned int   sZ = pObj->obj_payload_size;
		
		item_type	   = *pC++;	offset++;	sZ--;
		item_subtype   = *pC++;	offset++;	sZ--;
		
		item_number	   = REVERSE_L(*((DWORD *)pC));pC += szUINT;offset += szUINT;sZ-=szUINT;
currentItemSymbolID = item_number;
		item_attr_mask = REVERSE_L(*((DWORD *)pC));pC += szUINT;offset += szUINT;sZ-=szUINT;
		item_attr_cnt  = attrCntRemaining  = 
									items_attributes.attr_count = countSetBits(item_attr_mask);
		length2firstAttr= offset - pObj->obj_payload_offset();
		items_attributes.objectIndex = pObj->obj_Index;// for identity
		items_attributes.item_type   = item_type;

		if (item_type > ITEM_TYPE_MAX)
		{
			LOGIT(CERR_LOG,"ERROR: Item type is unknown <%d>\n",item_type);
		}
		if (item_subtype > ITEM_TYPE_MAX)
		{
			LOGIT(CERR_LOG,"ERROR: Item sub type is unknown <%d>\n",item_subtype);
		}
		pObj->itemIndex = pObj->pTopClass->idx++;

		status =
		//items_attributes.evaluateAttributePayload(pObj->obj_payload, pObj->obj_payload_size,
		items_attributes.evaluateAttributePayload(pC, sZ, offset,      attrCntRemaining    );
		if (status == FMx_IS_ITMINFO)// we encountered an item information attribute
		{	//	in reconcile mode and removed it
			item_attr_cnt--;
		}

		pObj->extProcessed = true;
	}

	return 0;
}

/*********************************************************************************************
 *	generatePrototype
 *
 *	in: nothing
 *
 *	returns: returns an aC item class that has all the prototype info and attributes
 *		
 ********************************************************************************************/
void FM8_Item_handler::generatePrototype()
{
	protoItem = new aCitemBase;

	protoItem->itemId = (ITEM_ID)item_number;
	protoItem->itemType = (CitemType)item_type;
currentItemSymbolID = item_number;	 
	protoItem->itemSubType = (CitemType)item_subtype;
	protoItem->attrMask = item_attr_mask;
	int unused;
	ConvertToString::fetchSymbolNameNtype(item_number, protoItem->itemName, unused);

	//FM8_Table* table = get_A_TablePtr(this->pBObj->pTopClass, symbol_table_type);
	//FMA_Row* row = table->at(item_symbol);  // this won't work.. see daSymbolTbl in FMx
	//CValueVarient v = row->at(1);
	//protoItem->itemName = v.sStringVal;

	// We need to use the variable's size to set the overall size
	unsigned targetType = (iT_Variable << 8) | varAttrTypeSize;
	// We need to use the debug info to get a symbol name
	unsigned targetName = (iT_Variable << 8) | varAttrDebugInfo;

	unsigned targetValid = 0;
	if ( item_type == iT_Variable )
	{
		targetValid = (item_type << 8) | varAttrValidity;
	}
	if ( item_type == iT_Collection )
	{
		targetValid = (item_type << 8) | grpItmAttrValidity;
	}	
	if ( item_type == iT_ItemArray )
	{
		targetValid = (item_type << 8) | grpItmAttrValidity;
	}
	if ( item_type == iT_Method )
	{
		targetValid = (item_type << 8) | methodAttrValidity;
	}	
	if ( item_type == iT_Menu )
	{
		targetValid = (item_type << 8) | menuAttr_Validity;
	}
	if ( item_type == iT_EditDisplay )
	{
		targetValid = (item_type << 8) | eddispAttrValidity;
	}	
	if ( item_type == iT_File )
	{
		targetValid = (item_type << 8) | fileNoValidity;
	}	
	if ( item_type == iT_Array )
	{
		targetValid = (item_type << 8) | arrayAttrValidity;
	}	
	if ( item_type == iT_Chart )
	{
		targetValid = (item_type << 8) | chartAttrValidity;
	}		
	if ( item_type == iT_Graph )
	{
		targetValid = (item_type << 8) | graphAttrValidity;
	}		
	if ( item_type == iT_Axis )
	{
		targetValid = (item_type << 8) | axisAttrValidity;
	}		
	if ( item_type == iT_Waveform )
	{
		targetValid = (item_type << 8) | waveformAttrValidity;
	}		
	if ( item_type == iT_Source )
	{
		targetValid = (item_type << 8) | sourceAttrValidity;
	}	
	if ( item_type == iT_List )
	{
		targetValid = (item_type << 8) | listAttrValidity;
	}	
	if ( item_type == iT_Grid )
	{
		targetValid = (item_type << 8) | gridAttrValidity;
	}	
	if ( item_type == iT_Image )
	{
		targetValid = (item_type << 8) | imageAttrValidity;
	}	
	if ( item_type == iT_Template )
	{
		targetValid = (item_type << 8) | templAttrValidity;
	}	
	if ( item_type == iT_PlugIn )
	{
		targetValid = (item_type << 8) | pluginAttrValidity;
	}
	bool haveTargetValid = false;
	unsigned targetHandl = (iT_Variable << 8) | varAttrHandling;
	bool haveTargetHandl = false;
	unsigned targetHelp  = (iT_Method << 8)   | methodAttrHelp;
	bool haveMethHelp    = false;

	protoItem->attrLst.clear();
	
	for (fm8AttrIT at = items_attributes.begin(); at != items_attributes.end(); at++)
	{
		FMx_Attribute* pAttr = (FMx_Attribute*)(*at);

		if ( item_type == iT_Block && pAttr->attrType > 0xc03)
		{// supress errors for unimplemented block attributes
			continue;
		}
#ifdef _QANDA
		qaAttrType = pAttr->attrType;
#endif
		aCattrBase* ptype = pAttr->generatePrototype();

		if (pAttr->isConditional)
		{
			protoItem->isConditional = true;
		}

		if ( pAttr->attrType == targetHelp )//
		{
			haveMethHelp = true;
		}
		if ( pAttr->attrType == targetType )//
		{
			protoItem->itemSize = ((asCvarType8*)pAttr)->varSize;
		}
		if ( IS_DEBUG( pAttr->attrType) )
		{
			protoItem->itemName = ((asCitemInfo8*)pAttr)->symbolName;
		}
		if ( pAttr->attrType == targetValid )
		{
			haveTargetValid = true;
		}
		if ( pAttr->attrType == targetHandl )
		{
			haveTargetHandl = true;
		}

		if (ptype != NULL && item_type != iT_Block)
		{
			protoItem->attrLst.push_back(ptype);
		}
	}// next


	if ( item_type == iT_Block )
	{
		protoItem->itemName = "Block";
		protoItem->attrMask = 0;	//we aren't doing attributes, no reason to show a mask
	}

	generatePrototypeDefaultAttrs(items_attributes);

} 


/*********************************************************************************************
 *	findOffset
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// tells what to do with the current location
 ********************************************************************************************/
int FM8_Item_handler::findOffset(unsigned offset)
{
	// should always be zero
	if (offset != 0)
	{
		LOGIT(CERR_LOG,"ERROR item offset mismatch: %d is not zero.\n",offset);
		return -1;
	}
	return 0;// we aren't doing the annex yet
}

/*********************************************************************************************
 *	readChunk
 *
 *	in: nothing
 *
 *	returns: bytes read or zero on error 
 *		// read item at the current location, it's one of ours
 ********************************************************************************************/
int FM8_Item_handler::readChunk(unsigned& sz)
{
	pObj->obj_annex_payload = new UCHAR[pObj->obj_annex_size];

	unsigned returns = fread(pObj->obj_annex_payload, 1, pObj->obj_annex_size, pObj->fp);

	if (returns != pObj->obj_annex_size)
	{
		delete[] pObj->obj_annex_payload; 
		pObj->obj_annex_payload = 0;
		return FMx_READFAIL;
	}// else continue to the exit

	sz -= returns;
	return 0; // we aren't do annex now
}
/*********************************************************************************************
 *	out
 *
 *	in: nothing
 *
 *	returns: err code or success 
 *		// sends text version to out
 ********************************************************************************************/

int FM8_Item_handler::out(void)
{
	// debugging usage only
	extern unsigned currentItemSymbolID;
	currentItemSymbolID = item_number;

	if (!reconcile  &&  !maintenance)
	{
		outHex(pObj->obj_payload, pObj->obj_payload_offset(), length2firstAttr );
	}
	
	
	LOGIT(COUT_LOG,"         Item Type: 0x%02x %s\n",(int)item_type,iType_8[item_type]);
	LOGIT(COUT_LOG,"      Item SubType: 0x%02x %s\n",
												(int)item_subtype,iType_8[item_subtype]);
	LOGIT(COUT_LOG,"    Item Symbol ID: 0x%04x (%u)\n",(int)item_number,(int)item_number);
	if (!reconcile)
	{
		LOGIT(COUT_LOG,"    Attribute Mask: 0x%04x\n",(int) item_attr_mask); 		
	}

	if ( items_attributes.size() )
	{
		items_attributes.out();
	}
	else
	{
		// temp till above works:
		outHex(pObj->obj_payload + length2firstAttr, 
			   pObj->obj_payload_offset() + length2firstAttr, 
			   pObj->obj_payload_size - length2firstAttr );
	}
	return 0; // success
}


void FM8_Item_handler::out2(void)
{
	// we aren't dealing with annexes yet
}

void FM8_Item_handler::out3(void)
{
	// we haven't got this far yet
}