#include "FM8_defs.h"
#include "logging.h"

#include <wchar.h>
#include "endian.h"

#include "FM8_objectSet.h"
#include "FM8_meta.h"
#include "FM8_Activities.h"
#include "FM8.h"
#include "FM8_Support_Tables.h"

char iType_8[ITEM_TYPE_STR_CNT][ITEM_TYPE_MAX_LEN] = ITEM_TYPE_STRINGS;

FM8::FM8()
{
}

FM8::~FM8()
{
}

/*********************************************************************************************
 *	indexFile
 *
 *	in: nada
 *
 *	returns: err code or success with the file open & rewound to the start of the file
 *
 *  reads the header and then scans the file building pointer indexes, rewinds when done
 *        note that this skips all payloads. memory for the fixed portion only is stored
 *
 ********************************************************************************************/
RETURNCODE FM8::indexFile(void)  
{
	RETURNCODE r = -1;
	unsigned  currentSize;

	r = headerData.readHeader(fp, saveBinary);
	if ( r != SUCCESS)
		return r;

	if (headerData.EFF_rev_major != 8)
	{
		return FAILURE;
	}

	// we're at the beginning of the ddef_meta data

	pMetaData = (FMx_meta*)new FM8_meta(this, fp, saveBinary);
	pObj_Data = (FMx_ObjectSet*)new FM8_ObjectSet(this, fp, saveBinary);

	if (pMetaData == NULL || pObj_Data == NULL)
		return FMx_MEMFAIL;
	
	// read images binary from end of DD file
	long savepos = ftell(fp);							// save position
	unsigned imageOffset = headerData.getOffset2Imag();
	unsigned imageBufferSize = fileSize - imageOffset;	// calc size of image data
	if (imageBufferSize > 0)
	{
		pImageBuffer = (unsigned char *) malloc(imageBufferSize);// allocate that much heap
		fseek(fp, imageOffset, SEEK_SET);	// seek to beginning of image data
		fread(pImageBuffer, imageBufferSize, 1, fp);		// read images from disk
	}
	fseek(fp, savepos, SEEK_SET);						// return to saved position


	currentSize = headerData.ddef_meta_size;// aka ddod-objects-size (size of statics)

	r = pMetaData->index_MetaData(currentSize); // Object description Only
	
	if ( r != SUCCESS || currentSize == 0 )
		return r;

	r = pObj_Data->index_ObjectData( currentSize );// all of it

	if ( r != SUCCESS)
		return r;

	return SUCCESS;	
}

RETURNCODE FM8::readIn_File(void)
{
	RETURNCODE r = FAILURE;

	FM8_ReadPayloadVisitor ReadFromFile;
	r = VisitAll(&ReadFromFile);

	return r;
}

RETURNCODE FM8::process_File(void)
{// interpret the payload
	RETURNCODE r = FAILURE;

	FM8_InterpretPayloadVisitor ProcessFile;
	ProcessFile.heapSize = headerData.ddef_obj_size;// aka ddod-data-size
	r = VisitAll(&ProcessFile);

	//TODO: capture string tables

	if (ProcessFile.heapSize)
	{
		LOGIT(CERR_LOG,"Heap reads failed to zero (from %d to %d)\n",
								(int)headerData.ddef_obj_size, (int)ProcessFile.heapSize);
	}
	else
	{
		DEBUGLOG(CLOG_LOG,"Heap read  in ProcessFile ended with  %d\n",(int)ProcessFile.heapSize);
	}

	FM8_meta* pMeta = dynamic_cast<FM8_meta *>(pMetaData);
	assert(pMeta);
	FM8_DevDir_handler* pHand = dynamic_cast<FM8_DevDir_handler *>(pMeta->pDevDir->pHandler);
	assert(pHand);

	// capture the string tables
	if (pLIT_STR)
	{
		pLIT_STR->setTheTable( &(pHand->literalStringList) );
	}
	if (pDICTSTR)
	{
		pDICTSTR->setTheTable( &(pHand->dictStringList) );
	}


	return r;
}

void FM8::release(void)
{
	FM8_ReleasePayloadVisitor releasePayload;

	VisitAll(&releasePayload);

	// added 23sep13 by stevev
	if( fp != NULL )
	{
		if ( fclose( fp ) )
		{
			wprintf( L"The file '%s' was not closed\n", pWfileNAME);
		}
		fp = NULL;
    }
}

void FM8::generate(void)
{
	FM8_makeItemTemplateVisitor makeItem;

	VisitAll(&makeItem);
}

void FM8::out(void)
{
	int retVal = 0;// for debugging right now
	unsigned index = 0;
	headerData.out();

	FM8_OutputObjectVisitor outObject;

	if (pMetaData)
		retVal = pMetaData->ExecuteVisitor(&outObject);
	if (retVal == SUCCESS && pObj_Data)
	{
		retVal = pObj_Data->ExecuteVisitor(&outObject);
	}
}
