/**********************************************************************************************
 *
 * $Workfile: FMA_Dict.cpp $
 * 20Jul11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Dict class implementation...the model of the file
 *		
 *
 */

#include "FMA_Dict.h"
#include "FMA_Support_tables.h"
#include "varient.h"
#include "logging.h"
#include "Convert.h"


	
FMA_Dict::FMA_Dict()  // constructor
{
}


FMA_Dict::~FMA_Dict()
{
}


void FMA_Dict::setTheTable( FMx_Table* pTbl)
{
	pTable = pTbl;
	if (pTbl == NULL)
	{
		LOGIT(CERR_LOG,L"FMA_Dict::setTheTable passed a NULL table.\n");
		return;
	}
	FMA_Table* pT = dynamic_cast<FMA_Table*>( pTbl );

	// verify
#ifdef _DEBUG
	if (pT->tableType != dictionary_table_type)
	{
		LOGIT(CERR_LOG,L"FMA_Dict::setTheTable passed a non-dictionary table.\n");
		return;
	}
#endif

	vector<FMA_Row *>    ::iterator rowIT;
	FMA_Row * pRow;

	vector<CValueVarient>::iterator vLIT;
	CValueVarient lV, mV, nV;
	wstring locNm, locStr;
	unsigned long strKey, rowCnt=0;

	for (rowIT = pT->begin(); rowIT != pT->end(); ++rowIT )
	{
		pRow = *rowIT;
		rowCnt++;
#ifdef _DEBUG
		assert( pRow->tt == dictionary_table_type);
#endif
		
		if ( pRow->size() <= 0 )
		{
			LOGIT(CLOG_LOG,"setDictionaryTable: Row %4d is empty.\n",rowCnt);
			continue;
		}
		vLIT = pRow->begin();
		lV = *vLIT++;  // key
		mV = *vLIT++;  // name
		nV = *vLIT;  // actual string

		strKey = (unsigned long)lV;
		locNm  = (std::wstring)mV;
		locStr = (std::wstring)nV;

			
#ifdef TOKENIZER
		/*
		* In tokenizing, we want all the full string with all
		* languages, as these will be stored in the final
		* encoded DD
		*/
#else
		ConvertToString::filterString(locStr);
#endif
		addStr(locStr, locNm, strKey);	//  [6/8/2016 tjohnston]

		if (! saveBinary)
		{// empty the original (DD) string
			(*vLIT).clear();// prevents having a bunch of string copies
		}
		vLIT++;
#ifdef _DEBUG
		if ( vLIT != pRow->end() )
		{// something is wrong
			LOGIT(CERR_LOG," End of Dictionary row is not at the end(). (%d)\n",pRow->size());
		}
#endif
		
	}// next row

}


/* 12jan16 - stevev, let's do this correctly and fall thru to the parent
void FMA_Dict::retrieveStr(int  str,   wstring& retVal)
{
	StrByNumber_t::iterator dictStrIT;

	dictStrIT = strByKey.find(str);

	if (dictStrIT == strByKey.end() )
	{
		retVal = L"****FMA_Dict did not find String by number.\n";
	}
	else
	{
		retVal = theStrList[dictStrIT->second];
	}
}


void FMA_Dict::retrieveStr(wstring nm, wstring& retVal)
{
	StrByString_t::iterator dictStrIT;

	dictStrIT = strByName.find(nm);

	if (dictStrIT == strByName.end() )
	{
		retVal = L"****FMA_Dict did not find String by Name.\n";
	}
	else
	{
		retVal = theStrList[dictStrIT->second];
	}
}
****/