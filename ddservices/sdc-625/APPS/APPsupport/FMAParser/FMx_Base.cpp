#include "FMx_Base.h"
#include "DDLdefs.h"
#include "ddbDefs.h"

bool FMx_Base::get_int(unsigned short* dest,void* src, int ret_fmt)
{
	int i=0;
	unsigned char *source_ptr = (unsigned char *)src;
	unsigned short tmp=0;

	tmp = 0;

	switch (ret_fmt)
	{
		case FORMAT_BIG_ENDIAN:
			for (i = 0; i < sizeof(tmp); i++)
			{
				tmp <<= 8;
				tmp |= *source_ptr++;
			}
		break;

		case FORMAT_LITTLE_ENDIAN:
			source_ptr += sizeof(tmp);
			for (i = 0; i < sizeof(tmp); i++)
			{
				tmp <<= 8;
				tmp |= *(--source_ptr);
			}
		break;

		default:
			return false;
	}

	*dest = tmp;

	return true;
}

bool FMx_Base::get_int(unsigned long *dest, void* src, int ret_fmt)
{
	int i=0;
	unsigned char *source_ptr = (unsigned char *)src;
	unsigned long tmp= 0;

	tmp = 0;

	switch (ret_fmt)
	{
		case FORMAT_BIG_ENDIAN:
			for (i = 0; i < sizeof(tmp); i++)
			{
				tmp <<= 8;
				tmp |= *source_ptr++;
			}
		break;

		case FORMAT_LITTLE_ENDIAN:
			source_ptr += sizeof(tmp);
			for (i = 0; i < sizeof(tmp); i++)
			{
				tmp <<= 8;
				tmp |= *(--source_ptr);
			}
		break;

		default:
			return false;
	}

	*dest = tmp;

	return true;
}


/*=========================================================================================== /
 *    MAPPING FUNCTIONS
 *===========================================================================================*/


int map_StringType_A28(int AstringType)
{ 
/*	from version A string types
	string-literal		  [0] INTEGER 			-- index into string-table
	dictionary-string	  [1] INTEGER				-- dictionary key		
	string-reference	  [2] Reference			-- string variable 
	enum-desc-reference	[3] SEQUENCE				-- description reference
	{	
	enum-value			INTEGER, 
	enum-reference		Reference
	}
	attribute-reference [4] SEQUENCE		-- must resolve to a string
	{
	attribute-name		Attribute_Name, 
	item-reference		Reference
	}
	string-expression [5] Expression	-- must resolve to a string
*/
	static ddlstringType_t map[] = {
		ds_DevSpec,	// A - string-literal
		ds_Dict,	// A - dictionary-string
		ds_VarRef,	// A - string-reference 
		ds_EnumRef,	// A - enum-desc-reference
		ds_AttrRef	// A - attribute-reference
	};// ds_Var, and ds_Enum aren't used in A...they're just another reference
	if ( AstringType < 0 || AstringType > 4 )
	{
		return -2;
	}
	return map[AstringType];
}



// given a item type and redefinition attribute type 
// return the FM8 attribute number (for that item type)
int map_RedefType_2_8(int itemType, int RedefattrType)
{
	ID_MAP_STRUCT;

	for (int i=0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].redefType == RedefattrType)
		{
			return  map[i].fm8 ;
		}
	}

	return -1;		// should never reach here!!
}



// given a item type and FMA attribute type 
// return the RedefAttrType attribute number (for that item type)
TOKATTR_T map_FMAtype_2_RedefType(int itemType, int FMAattrType)
{
	ID_MAP_STRUCT;

	for (int i = 0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].fma == FMAattrType)
		{
			return (TOKATTR_T) map[i].redefType;
		}
	}

	return I_TYPE_NONE;		// should never reach here!!
}


// given a item type and FM8 attribute type  (ie mask to number)
// return the RedefAttrType attribute number (for that item type)
TOKATTR_T map_FM8type_2_RedefType(int itemType, int FM8maskNumber)
{
	ID_MAP_STRUCT;

	for (int i = 0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].fm8 == FM8maskNumber)
		{
			return (TOKATTR_T)map[i].redefType;
		}
	}

	return I_TYPE_NONE;		// should never reach here!!
}


// given a item type and redefinition attribute type 
// return the FM8 attribute number (for that item type)
// apparently unused [1/27/2016 tjohnston]
// used in bld_imp.cpp   
int map_RedefType_2_A(int itemType, int RedefattrType)
{
	ID_MAP_STRUCT;

	for (int i=0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].redefType == RedefattrType)
		{
			return  map[i].fma ;
		}
	}

	return -1;		// should never reach here!!
}



// given an fma attribute type, switch between 
// return the FM8 generic attribute type
int map_AttrType_A28(int itemType, int AattrType)
{
	ID_MAP_STRUCT;

	for (int i=0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].fma == AattrType)
		{
			return ((itemType << 8) | map[i].fm8 );
		}
	}

	return -1;		// should never reach here!!
}




// given an item type AND an fm8 attribute type, 
// return the FMA  attribute type
int map_AttrType_82A(int itemType, int attrType)
{
	ID_MAP_STRUCT;

	for (int i=0; map[i].fm8 >= 0; i++)
	{
		if (map[i].itype == itemType  &&  map[i].fm8 == attrType)
		{
			return ( map[i].fma );
		}
	}

	return -1;		// should never reach here!!
}


int conversionAto8(mappingTypeAto8_t mt, int param1, int param2 )
{
	switch (mt)
	{
	case map_AttrType:
		{
			return map_AttrType_A28( param1, param2 );
		}
		break;
	case map_Str_Type:
		{
			return map_StringType_A28(param2);
		}
		break;
	default:
		{
			return -1;
		}
		break;
	}// end switch
	return -2;
}

//int conversionAtoDDL(mappingTypeAtoDDL_t mt, int param1, int param2)
//{
//}

/*
getLiteralString(strInt, k);
*/