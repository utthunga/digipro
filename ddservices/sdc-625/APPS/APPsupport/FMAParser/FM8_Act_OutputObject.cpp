/**********************************************************************************************
 *
 * $Workfile: FM8_Act_OutputPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Act_ReadPayload Visitor Operation on all object classes
 *		
 *
 */

#include "FM8_defs.h"
#include "FM8_Activities.h"
#include "FM8_object.h"
#include "logging.h"
#include "v_N_v+Debug.h"

extern char objectTypeStrings[10][OBJTYPEMAXLEN];
extern void outHex( UCHAR* pS, unsigned offset, unsigned len );//global in FM8_reader.cpp


// // required function
//	> x41 disregard, x81 won't be seen
//	> x43 disregard, x83 won't be seen
//	> x42 disregard for now, x82 -- lots of work to do
//
// this is required to output the entire object (fixed and extension)
int FM8_OutputObjectVisitor::Operate( FMx_Base* pVisited )
{
	if ( pVisited == NULL )
		return FAILURE;
	FM8_Object* pObj = (FM8_Object*)pVisited;

	unsigned ct = pVisited->classType;
	if ( (ct & (FMA_type|FM8_type)) == FMA_type )
	{// we are going to disregard all FM8 for now
		return SUCCESS;
	}
	if ( ct & __Meta )
	{// we are not supposed to see x81 nor x83
	LOGIT(CERR_LOG,"ERROR: FM8_OutputObjectVisitor should never see classtype %#02x\n",ct);
		return FAILURE;
	}
	// we have to have class type FM8_Object

	if (reconcile == false  &&  maintenance == false)
	{
		if ( pObj->obj_type != obj_8_ODES && !reconcile )
		{// Output the FIXED portion of the object
			
			LOGIT(COUT_LOG,"Object [ %4u] \n", pVisited->itemIndex); 
			outHex(pObj->obj_Fixed, pObj->object_offset, V8_FIXED_SIZE );
			LOGIT(COUT_LOG,"Object Index: %4u (0x%04x)\n",pObj->obj_Index,pObj->obj_Index);
			unsigned ops = (SUPPRESS_HEXDUMP)?0:pObj->obj_payload_size;
	
			LOGIT(COUT_LOG,"Object  Type:> %s <       Object Size: %6u (0x%06x)\n",
												objectTypeStrings[(int)(pObj->obj_type & 0x7f)],
												ops,ops );
			if ( pObj->obj_annex_offset != 0xffffffff )
			{
				if (SUPPRESS_HEXDUMP)
				{
				LOGIT(COUT_LOG,"Object annex Offset: --  Size: -- \n");// supress it all
				}
				else
				{
				LOGIT(COUT_LOG,"Object annex Offset: 0x%08x (%7u)  Size: 0x%04x (%6u)\n",
		pObj->obj_annex_offset, pObj->obj_annex_offset,pObj->obj_annex_size, pObj->obj_annex_size);
				}
			}
		}
		else
		if (pObj->obj_type == obj_8_ODES)
		{
			LOGIT(COUT_LOG,"ODES object has no Fixed portion.\n");
		}
	}

	if ((reconcile || maintenance) && pObj->obj_type == obj_8_ODES)
	{
	}
	else
	{
		// Now output the Payload portion
		int isUnhandled = -1;
		if ( pObj->obj_payload != NULL)
		{
			LOGIT(COUT_LOG,
			"- - - - - - - - - - - - PAYLOAD - - - - - - - - - - - - - - - - - - - - - - \n");

			if (pObj->pHandler)
			{
				isUnhandled = pObj->pHandler->out();// 0 (false) on successfully handled
			}
			// else leave unhandled
			if ( isUnhandled )// error or not supported
			{// fallback position, just dump the payload hex	
				
			LOGIT(COUT_LOG,
			"   Object's out handler  did not handle all the object.\n");
		outHex( pObj->obj_payload, (pObj->object_offset + V8_FIXED_SIZE), pObj->obj_payload_size );
			}
		}
		else
		{
			LOGIT(COUT_LOG,
			"          x x x x x x NO PAYLOAD x x x x x \n");
		}
		LOGIT(COUT_LOG,
			"____________________________________________________________________________\n");
	}

	return SUCCESS;
};


