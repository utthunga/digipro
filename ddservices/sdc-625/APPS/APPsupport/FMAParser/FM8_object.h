/**********************************************************************************************
 *
 * $Workfile: FM8_Object.h $
 * 21Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Object.h : FMA's basic OBJECT class description file
 *
 * #include "FM8_Object.h"
 *
 */

#ifndef _FMA_FM8_OBJECT_H
#define _FMA_FM8_OBJECT_H

#include "FMx_Base.h"
#include "FM8_handlers.h"
#include "FM8_defs.h"
#include "ddbdefs.h" /* for uf_short */

using namespace std;
#pragma warning (disable : 4786) 


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the basic object that forms the fma format.  It reads the fixed portion
 *		and returns with the payload.  It doe NO payload processing.
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class FM8_Object : public FMx_Base
{
public:		
	 FM8_Object(FMx* pTC, FILE* pf, bool saveBinary = false);  // constructor
	~FM8_Object();

public:	// data 
	UCHAR*			obj_Fixed;			// raw binary of fixed portion

	unsigned		object_offset;		// first byte of fixed...from start of file

	/** contents of the Fixed portion of the object **/
	unsigned /* 14jan16 short*/	obj_Index;
	obj_type_t      obj_type;  // always a 2 in fm8s... re-coded for directories, format & ODES

	// EXTENSION
	unsigned		obj_payload_offset(void) { return (object_offset + V8_FIXED_SIZE);}; 
	unsigned char   obj_payload_size;    // extension length in uchars
	unsigned char   paddingZero[sizeof(unsigned)-1];
	UCHAR*			obj_payload;//the raw extension chunk
			
	// ANNEX (on the heap) FOR FM8 ONLY
	unsigned        obj_annex_offset; // heap payload offset from beginning of heap
    unsigned short  obj_annex_size;   // heap payload length in UCHARs (-1 means extended)
	UCHAR*          obj_annex_payload;// the part that is in the DDOD Heap @ offset

	unsigned		objHeapStartOffset;// disk location annex_offset from beginning of file

	// accessors
	virtual unsigned    obj_size(void)	{	return(obj_payload_size + V8_FIXED_SIZE);};

#define calcHeapStartOffset	(pTopClass->headerData.getOffset2Heap()+obj_annex_offset)

	// the changable handler class
	FMx_handler*    pHandler;
	bool			extProcessed;// set true on first pass of the handler's evaluateExtension

public:		// workers
	virtual RETURNCODE read_Object_Info(unsigned& size, bool skipExtensionRead = true);//at current loc	read info only then (normally) seek past payload
	virtual RETURNCODE read_ObjectPayload(void);// at recorded offset
	virtual FMx_handler* getObjectHandler(void);// overidden for FM8_ODES

	// reads obj_annex_size bytes from the Heap to obj_annex_payload
	RETURNCODE readHeapChunk(unsigned& size);

public:		// required
	RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor) { return (pVisitor->Operate( this )); };

// these need to be moved to the output visitor
	void out(unsigned idx = 0xffffffff, unsigned size = 0);
	void out2(unsigned idx, unsigned size);// does the heap part only
};

 /*************************************************************************************
 * stevev 21may12 - In order to make the version 8 run like the version 10, I'm adding
 *		a unique FM8_Object to deal with the DDOD_ODES
 **************************************************************************************/
class FM8_ODES : public FM8_Object
{
public:
	FM8_ODES(FMx* pTC, FILE* pf, bool saveBinary = false) : FM8_Object(pTC,pf,saveBinary) { obj_type = obj_8_ODES; };

public:	// data
	// for length, use object's extension length: obj_payload_size

public:
	RETURNCODE  read_Object_Info(unsigned& size, bool skipExtensionRead = true);
	RETURNCODE  read_ObjectPayload(void);// at recorded offset	
	FMx_handler* getObjectHandler(void);
	unsigned    obj_size(void)	{	return(obj_payload_size);  };
};

typedef vector<FM8_Object*> FM8_pObjectList_t;
typedef FM8_pObjectList_t::iterator FM8_pObjectListIT_t;

#endif //_FMA_FM8_OBJECT_H
