/**********************************************************************************************
 *
 * $Workfile: FMA_Act_OutputPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Act_ReadPayload Visitor Operation on all object classes
 */

#include "FMA_Activities.h"
#include "FMx.h"
#include "FMA_object.h"
#include "logging.h"
#include "v_N_v+Debug.h"

extern char objectTypeStrings[10][OBJTYPEMAXLEN];
extern void outHex( UCHAR* pS, unsigned offset, unsigned len );//global in fma_reader.cpp

FMA_OutputObjectVisitor::FMA_OutputObjectVisitor()
{// no data, no construction...as of now
};

FMA_OutputObjectVisitor::~FMA_OutputObjectVisitor()
{// no data, no construction...as of now
};



// // required function
//	> x41 disregard, x81 won't be seen
//	> x43 disregard, x83 won't be seen
//	> x42 disregard for now, x82 -- lots of work to do
//
// this is required to output the entire object (fixed and extension)
int FMA_OutputObjectVisitor::Operate( FMx_Base* pVisited )
{
	if ( pVisited == NULL )
		return FAILURE;
	FMA_Object* pObj = (FMA_Object*)pVisited;

	unsigned ct = pVisited->classType;
	if ( (ct & (FM8_type|FMA_type)) == FM8_type )
	{// we are going to disregard all FM8 for now
		return SUCCESS;
	}
	if ( ct & __Meta )
	{// we are not supposed to see x81 nor x83
		LOGIT(CERR_LOG,"ERROR: FMA_OutputObjectVisitor should never see classtype %#02x\n",ct);
		return FAILURE;
	}

	// we have to have class type FMA_Object

	// Output the FIXED portion of the object
	if (!reconcile  &&  !maintenance)
	{
		LOGIT(COUT_LOG,"Object [ %4u] \n", pVisited->itemIndex); 
		outHex(pObj->obj_Fixed, pObj->object_offset, OBJ_FIXED_SIZE );
		LOGIT(COUT_LOG,"Object Index: %4u (0x%04x)\n",pObj->obj_Index,pObj->obj_Index);

		if (SUPPRESS_PTOCSIZE==false)
		{
			LOGIT(COUT_LOG,"Object  Type:> %s <       Object Size: %6u (0x%06x)\n",
					objectTypeStrings[(int)(pObj->obj_type)], pObj->obj_Length,pObj->obj_Length );
		}
	}

	// Now output the Payload portion
	int isUnhandled = -1;
	if ( pObj->obj_Payload != NULL)
	{
		LOGIT(COUT_LOG,
		"- - - - - - - - - - - - PAYLOAD - - - - - - - - - - - - - - - - - - - - - - \n");
		if (pObj->pHandler)
		{
			isUnhandled = pObj->pHandler->out();// 0 (false) on successfully handled
		}
		// else leave unhandled
		if ( isUnhandled )// error or not supported
		{// fallback position, just dump the payload hex				
			outHex(pObj->obj_Payload, pObj->objA_payload_offset(), pObj->obj_Length );
		}
	}
	else
	{
		LOGIT(COUT_LOG,
		"          x x x x x x NO PAYLOAD x x x x x \n");
	}
	LOGIT(COUT_LOG,
		"____________________________________________________________________________\n");

	return SUCCESS;
};


