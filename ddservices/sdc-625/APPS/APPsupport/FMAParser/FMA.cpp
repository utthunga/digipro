#include "FMA_defs.h"
#include "logging.h"

#include <wchar.h>
#include "endian.h"

#include "FMA_objectSet.h"
#include "FMA_meta.h"
#include "FMA_Activities.h"
#include "FMA.h"
#include "FMx_Attributes_Ref&Expr.h"
#include "FMA_Support_tables.h"

char objectTypeStrings[10][OBJTYPEMAXLEN]   = {OBJTYPESTRINGS};
char itemType_Str[FMAITMTYPECOUNT][FMAITMTYPMAXLEN]  = {FMAITMTYPSTRINGS};
//char attrType_Str[ATTR_TYPE_COUNT][FMAATTRTYPMAXLEN] = {FMAATTRTYPSTRINGS};
char varType_Str[VAR_TYPE_COUNT][FMAVARTYPMAXLEN]    = {FMAVARTYPSTRINGS};
//char strType_Str[STR_TYPE_COUNT][FMASTRTYPMAXLEN]    = {FMASTRTYPSTRINGS};
char exprType_Str[EXPR_TYPE_COUNT][FMAEXPRTYPMAXLEN] = {FMAEXPRTYPSTRINGS};
char condType_Str[COND_TYPE_COUNT][FMACONDTYPMAXLEN] = {FMACONDTYPSTRINGS};
char tableTypeStrings[TBLTYPESTRCNT][TBLTYPEMAXLEN]  = {TBLTYPESTRINGS};

/*********************************************************************************************
 *	indexFile
 *
 *	in: nada
 *
 *	returns: err code or success with the file open & rewound to the start of the file
 *
 *  reads the header and then scans the file building pointer indexes, rewinds when done
 *        note that this skips all payloads. memory for the fixed portion only is stored
 *
 ********************************************************************************************/
RETURNCODE FMA::indexFile(void)  
{
	RETURNCODE r = -1;
	unsigned  currentSize;

	r = headerData.readHeader(fp, saveBinary);
	if ( r != SUCCESS)
		return r;

#ifdef _DEBUG
#define EXPECTED_VERSION  3 && headerData.EFF_rev_major != 10
#else
#define EXPECTED_VERSION 3
#endif

	//if (headerData.EFF_rev_major != EXPECTED_VERSION )
	////if (headerData.EFF_rev_major != 2  &&  headerData.EFF_rev_major != 3  &&  headerData.EFF_rev_major != 10 )  // allow 2's
	//{
	//	return FAILURE;
	//}

	// we're at the beginning of the ddef_meta data

	pMetaData = (FMx_meta*)new FMA_meta(this, fp, saveBinary );
	pObj_Data = (FMx_ObjectSet*)new FMA_ObjectSet(this, fp, saveBinary);

	if (pMetaData == NULL || pObj_Data == NULL)
		return FMx_MEMFAIL;

	// read images binary from end of DD file
	long savepos = ftell(fp);							// save position
	unsigned imageOffset = headerData.getOffset2Imag();
	unsigned imageBufferSize = fileSize - imageOffset;	// calc size of image data
	if (imageBufferSize > 0)
	{
		pImageBuffer = (unsigned char *) malloc(imageBufferSize);// allocate that much heap
		fseek(fp, imageOffset, SEEK_SET);	// seek to beginning of image data
		fread(pImageBuffer, imageBufferSize, 1, fp);		// read images from disk
	}
	fseek(fp, savepos, SEEK_SET);						// return to saved position


	currentSize = headerData.ddef_meta_size;// aka ddod-objects-size (size of statics)

	r = pMetaData->index_MetaData(currentSize); // Object description Only

	if ( r != SUCCESS || currentSize == 0 )
		return r;

	currentSize = headerData.ddef_obj_size;

	r = pObj_Data->index_ObjectData( currentSize );// all of it

	if (r != SUCCESS)
		return r;

	return SUCCESS;	
}

RETURNCODE FMA::readIn_File(void)
{// get the payload
	RETURNCODE r = FAILURE;

	FMA_ReadPayloadVisitor ReadFromFile;
	r = VisitAll(&ReadFromFile);

	return r;
}

RETURNCODE FMA::process_File(void)
{
	RETURNCODE r = FAILURE;

	FMA_InterpretPayloadVisitor ProcessFile;
	r = VisitAll(&ProcessFile);

	if ( r != SUCCESS )
		return r;

	// capture the string tables
	if (pLIT_STR)
	{
		pLIT_STR->setTheTable( get_A_TablePtr(this, string_table_type) );
	}
	if (pDICTSTR)
	{
		pDICTSTR->setTheTable( get_A_TablePtr(this, dictionary_table_type) );
	}
	if ( daSymbolTbl )
	{
		daSymbolTbl->setTheTable( get_A_TablePtr(this, symbol_table_type) );
	}

	return r;
}

void FMA::release(void)
{
	FMA_ReleasePayloadVisitor releasePayload;

	VisitAll(&releasePayload);

	// added 23sep13 by stevev
	if( fp != NULL )
	{
		if ( fclose( fp ) )
		{
			wprintf( L"The file '%s' was not closed\n", pWfileNAME);
		}
		fp = NULL;
    }
}

void FMA::generate(void)
{
	FMA_makeItemTemplateVisitor makeItem;

	VisitAll(&makeItem);
}

void FMA::out(void)
{
	int retVal = 0;// for debugging right now
	unsigned index = 0;
	headerData.out();

	FMA_OutputObjectVisitor outObject;

	if (pMetaData)
		retVal = pMetaData->ExecuteVisitor(&outObject);

	if (retVal == SUCCESS && pObj_Data)
	{
		retVal = pObj_Data->ExecuteVisitor(&outObject);
	}
}

