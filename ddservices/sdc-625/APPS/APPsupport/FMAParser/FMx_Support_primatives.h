/**********************************************************************************************
 *
 * $Workfile: FMx_Support_primatives.h $
 * 19mar12 - steveh
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_Support_primatives.h : These are basic functions (mostly global) like parse int
 *
 * #include "FMx_Support_primatives.h"
 */

#ifndef _FMX_SUPP_PRIMATIVES_H
#define _FMX_SUPP_PRIMATIVES_H

#include "varient.h"
#include "foundation.h"

#include "FMx_general.h"

extern RETURNCODE	 //parse encoded  INTEGER
parseInteger(int ReturnSize, unsigned char** pp_Location, int& remainingSize, UINT64 &value);

extern RETURNCODE	 //parse encoded  INTEGER, inc offset
parseInteger(int RetSize, unsigned char** pp_Loc, int& Size, UINT64 &value, unsigned& offset);

extern RETURNCODE	// ReturnLen is zero (with no error) on an implicit tag
parseTag(unsigned char** pp_Location, int& remainingSize, unsigned& ReturnTag, unsigned& ReturnLen);

extern RETURNCODE	//w/offset  ReturnLen is zero (with no error) on an implicit tag
parseTag(unsigned char** pp_Loc, int& Size,unsigned& RetTag,unsigned& RetLen, unsigned &offset);

extern RETURNCODE /* ALLOCATES MEMORY to *pp_value, copies from pp_Loc to value for length */ parseByteString(unsigned char** pp_Loc, int& remainingSize, unsigned length, unsigned & offset, UCHAR** pp_value);

void obfuscateByteString(bool onoff);	// true for FMA (default), false for FM8

extern RETURNCODE	//ALLOCATES MEMORY to value:  gets tag&len, copies data to value for len
parseAsciiString(unsigned char** pp_Location, int& remainingSize, unsigned & offset, char** value);

extern RETURNCODE	// parses utf8 byte string, converts to unicode and puts it in ws_value
parseWideString(unsigned char** pp_Loc, int& remainingSize, unsigned length, unsigned & offset, wstring& ws_value);

extern RETURNCODE	// parses 8 byte double from stream to df_value
parseDouble(unsigned char** pp_Loc, int& SizeLeft, double& df_value, unsigned& offset);

extern RETURNCODE	// parses 4 byte float from stream to df_value
parseFLOAT(unsigned char** pp_Loc, int& SizeLeft, double& df_value, unsigned& offset);

// encoded integer to value
extern int parseInteger(int size, FILE* fp, UINT64& value);
extern int parseInteger(FILE* fp, unsigned& value);// size 5?
extern int parseInteger(FILE* fp, UCHAR& value); // size 1
extern int parseAsciiString(FILE* fp, char** value); //including tag & len

// parse a non-encoded integer
extern int parse_unsigned32(unsigned char** pp_Location, int& remainingSize, unsigned& value);


extern int parseBitString(UCHAR** pData, int& size, UINT64 &value, unsigned& offset);

// Following macros are used to parse primitives with error checking in a uniform way

#define PARSE_EXPECTED_IMPLICIT_TAG(_tag, _ret) _ret = parseExpectedImplicitTag	\
																(pData, length, offset, _tag)
/*** stevev 25jun13 - see Docs\SDC_coding.doc for coding guidelines, including macros
// parse tag, check for implicit and expected value == _tag, log errors
#define PARSE_EXPECTED_IMPLICIT_TAG(_tag, _ret)								\
{																				\
  	unsigned _tagValue;															\
	unsigned _tagLength;														\
																				\
	(_ret) = parseTag(pData, length,   _tagValue,   _tagLength, offset);		\
																				\
	if (_tagLength > 0)															\
	{																			\
		LOGIT(CERR_LOG,"Error: %s implicit tag length should be 0.\n)", attname);\
		return -1;																\
	}																			\
																				\
	if (_tagValue != (_tag))													\
	{																			\
		LOGIT(CERR_LOG,"Error: %s tag is not %d.\n)", attname, (_tag));			\
		return -2;																\
	}																			\
}																				
****/
// parse tag, check for implicit, return tag, log errors
#define PARSE_IMPLICIT_TAG(_tag, _ret)											\
{																				\
  	unsigned _tagValue;															\
	unsigned _tagLength;														\
																				\
	/* ReturnLen is zero (with no error) on an implicit tag */					\
	(_ret) = parseTag(pData, length,   _tagValue,   _tagLength, offset);		\
																				\
	if (_tagLength > 0)															\
	{																			\
		LOGIT(CERR_LOG,"Error: %s implicit tag length should be 0.\n)", attname);\
		return -1;																\
	}																			\
																				\
	(_tag) = _tagValue;															\
}																				


// parse tag, check for expected value == _tag, return length, log errors
#define PARSE_EXPECTED_EXPLICIT_TAG(_tag, _len, _ret)								\
{																					\
  	unsigned _tagValue;																\
	unsigned _tagLength;															\
																					\
	(_ret) = parseTag(pData, length, _tagValue, _tagLength, offset);				\
	(_len) = _tagLength;															\
																					\
	if (_tagLength > (unsigned) length)												\
	{																				\
		LOGIT(CERR_LOG,"Error: %s attribute with tag's length bigger than available.\n)", attname);	\
		return -1;																	\
	}																				\
																					\
	if (_tagValue != (_tag))														\
	{																				\
		LOGIT(CERR_LOG,"Error: %s attribute tag is not %d.\n)", attname, (_tag));	\
		return -2;																	\
	}																				\
}					

// parse tag, return tag and length, log errors
#define PARSE_EXPLICIT_TAG(_tag, _len, _ret)										\
{																					\
  	unsigned _tagValue;																\
	unsigned _tagLength;															\
																					\
	(_ret) = parseTag(pData, length, _tagValue, _tagLength, offset);				\
																					\
	if (_tagLength > (unsigned) length)												\
	{																				\
		LOGIT(CERR_LOG,"Error: %s attribute with tag's length bigger than available.\n)", attname);	\
		return -1;																	\
	}																				\
																					\
	(_tag) = _tagValue;																\
	(_len) = _tagLength;															\
}

//#define PARSE_UNSIGNED(_x, _ret)														\
//{																						\
//	UINT64	_tempLongLong;																\
//	if (length > 0)																		\
//	{																					\
//		(_ret) = parseInteger(4, pData, length, _tempLongLong, offset);					\
//		if ((_ret) != SUCCESS)															\
//		{																				\
//			LOGIT(CERR_LOG,"Error: %s attribute error in size extraction.\n)", attname);\
//		}																				\
//		else																			\
//		{																				\
//			(_x) = (unsigned)_tempLongLong;												\
//		}																				\
//	}																					\
//	else																				\
//	{																					\
//		LOGIT(CERR_LOG,"Error: %s attribute needs size but has no length.\n)", attname);\
//		(_ret) = -3;																	\
//	}																					\
//}

#define PARSE_BITSTRING(_x, _ret)														\
{																						\
	if (length > 0)																		\
	{																					\
		(_ret) = parseBitString(pData, length, (_x), offset);							\
		if ((_ret) != SUCCESS)															\
		{																				\
			LOGIT(CERR_LOG,"Error: %s attribute error in size extraction.\n)", attname);\
		}																				\
	}																					\
	else																				\
	{																					\
		LOGIT(CERR_LOG,"Error: %s attribute needs size but has no length.\n)", attname);\
		(_ret) = -3;																	\
	}																					\
}

// is int x on int list?
#define intIsInList(x, l)	_intIsInList((x), (l), sizeof((l))/sizeof(int))
bool _intIsInList(int x, int list[], int n);

// find x on list 1, return the int at the same position from list 2
#define intLookupList(x, l1, l2)  _intLookupList((x), (l1), (l2), sizeof(l1)/sizeof(int), sizeof(l2)/sizeof(int))
int _intLookupList(int x, int list1[], int list2[], int n1, int n2);

#endif //_FMX_SUPP_PRIMATIVES_H