/**********************************************************************************************
 *
 * $Workfile: FMx_Dict.h $
 * 
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_Dict.h : FMA class description file
 *
 * #include "FMx_Dict.h"
 *
 */

#ifndef _FMX_DICT_
#define _FMX_DICT_

#ifdef INC_DEBUG
#pragma message("In FMx_Dict.h") 
#endif

#include <functional> 
#include <vector>
#include <string>
#include <map>

#if _MSC_VER >= 1900 || (__GNUC__)
#include <mutex>
#endif

using namespace std;
using namespace tr1;
#pragma warning (disable : 4786) 

// removed stevev 04nov15  #include "FMx.h"
#include "FMx_general.h"
#include "globals.h"

extern wstring c2w(char *str);

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMx_Dict.h") 
#endif

struct dictStr
{
	unsigned keyNumber;
	wstring  keyName;
	wstring  StringItself;
	bool	 used;
};

#define DICT_STRING_NOT_FOUND  L""

/* redesigned removed [6/8/2016 tjohnston]
 struct dictStrID
 {
 	unsigned index;
 	unsigned keyNumber;
 };
***/
/* redesigned 12jan16
typedef	map<unsigned, unsigned>  StrByNumber_t; // key::theStrList.index
typedef	map<wstring,  unsigned>  StrByString_t; //name::theStrList.index
typedef vector<wstring>          StrList_t;	// just a list of strings
***/
/* redesigned 08jun2016
typedef	map<unsigned, unsigned>   StrByNumber_t; // keyNumber::theStrList.index
typedef	map<wstring,  dictStrID>  StrByString_t; //   keyName::{theStrList.index & keyNumber}
typedef vector<wstring>           StrList_t;	//  just a list of strings
*/

typedef	map<unsigned, unsigned>   StrByNumber_t; // keyNumber::dictStrList.index
typedef	map<wstring, unsigned>	  StrByName_t;	//  keyName::dictStrList.index
typedef vector<dictStr>           StrList_t;	//  just a list of tuples

class FMx_Table;   // in lieu of including the whole file

// 10jan13 - this is now the baseclass for the dictionary and the literal string dictionary

class FMx_Dict
{/* redesigned 12jan16 - made protected and added iterator type functions */
	// redesigned to be a list of dictStr tuples [6/8/2016 tjohnston]
//public:			// changed to public to facilitate CDictionary copying out of this class hierarchy [7/29/2014 timj]
	//StrByName_t::iterator internalIT;	// by key
	StrByNumber_t::iterator internalIT;	// by key
	StrList_t::iterator litIT;			// by index
protected:
#if _MSC_VER >= 1900
	mutex                   strListmutex;
#endif
	StrList_t		dictStrList;
	StrByNumber_t	strByKey;	// keyNumber::dictStrList.index (unused for Lit Strings)
	StrByName_t		strByName;	// keyName::dictStrList.index	(unused for Lit Strings)

	FMx_Table* pTable;  // can be a 8 or a table, manipulation of the table MUST be handled
						// in the A or 8 code.
public: //data
	UINT64   fileKey;// where we were last loaded
	bool saveBinary;

public:
	FMx_Dict();
	virtual ~FMx_Dict();
	

protected:
	std::tr1::hash<std::wstring> hash_fn;

public: // implementation
	virtual unsigned retrieveKey(wstring nm);									// -1 for error
	virtual unsigned retrieveKey(char *nm) { return retrieveKey(c2w(nm)); };	// -1 for error
	virtual int retrieveStr(int  str,   wstring& retVal);						// SUCCESS or -1
	virtual int retrieveStr(wstring nm, wstring& retVal);						// SUCCESS or -1
	virtual int retrieveStr(char* nm, wstring& retVal) { return retrieveStr(c2w(nm), retVal); };
	virtual int retrieveIndex(int index, wstring& retVal);
	virtual void setTheTable( FMx_Table* pTbl) = 0;								// Pure Virtual Function
	virtual int addStr(wstring& newVal);										// new index
	virtual int addUniqStr(wstring& newVal);									// new index
	virtual int addStr(wstring& newVal, wstring& newName, unsigned newKey, 
													bool newUsed = false);		// new index
	virtual int findStr(wstring& val);											// existing index or -1
	virtual int dictSize(void){ return dictStrList.size(); };
	virtual void clearDict(void);// empty all the lists
	virtual int copyStrTo(int str, FMx_Dict *otherDict, bool markUsed = false);	// SUCCESS or FAILURE
	virtual void copyDictTo(FMx_Dict *otherDict);								// SUCCESS or FAILURE

	// iteration function on string name  (ie keyName)
	virtual void    setFirstString(void);//CAUTION-NOT thread safe!
	virtual dictStr getNextString(void); //CAUTION-NOT thread safe!

	// iteration function on literal strings  (by index)
	virtual void    setFirstLitString(void);		//CAUTION-NOT thread safe!
	virtual int		getNextLitString(dictStr &ds);	//CAUTION-NOT thread safe!
#ifdef TOKENIZER
	bool readFromFile(char *file, char *add_file);
#endif
};

#endif //_FMX_DICT_
