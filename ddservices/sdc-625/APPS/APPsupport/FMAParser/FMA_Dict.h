/**********************************************************************************************
 *
 * $Workfile: FMA_Dict.h $
 * 20Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA.h : FMA class description file
 *
 * #include "FMA_Dict.h"
 *
 */

#ifndef _FMA_DICT_H
#define _FMA_DICT_H

#ifdef INC_DEBUG
#pragma message("In FMA_Dict.h") 
#endif

#include "FMx_Dict.h"

using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_Dict.h") 
#endif


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the *.fma dictionary in two modes
 *	first -	Single language mode - only the start language is loaded
 *	second- All languiages, for changing languages on the fly
 *
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMA_Dict : public FMx_Dict
{	
public:		
	FMA_Dict();  // constructor
	virtual ~FMA_Dict();

public:		// workers
	//virtual void retrieveStr(int  str,   wstring& retVal);
	//virtual void retrieveStr(wstring nm, wstring& retVal);
	virtual void setTheTable( FMx_Table* pTbl);

protected:	// helpers

};



#endif	// _FMA_DICT_H