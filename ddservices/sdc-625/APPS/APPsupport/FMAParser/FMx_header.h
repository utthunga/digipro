#ifndef _FMX_HEADER_
#define _FMX_HEADER_

#include <stdio.h>
#include "FMx_general.h"

/*********************************************************************************************
 * FMx_header
 ********************************************************************************************/
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class is the generic header reader.  It supports the FMx file described later
 *			
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMx_header
{  
public:
	unsigned int	magic_number;	//0x8A45F533
	unsigned int	header_size;
	unsigned int	ddef_meta_size; // object size
	unsigned int	ddef_obj_size;  // post-object heap size
	unsigned char	byManufacturer[4];// added a forth to make structure even
	unsigned short	device_type;
	unsigned char	device_revision;
	unsigned char	dd_revision;
	unsigned char	EFF_rev_major;
	unsigned char	EFF_rev_minor;
	unsigned char	deviceProfile;	//-- X01 HART, X02 FF, X03 Profi
	unsigned char	reserved1;
	unsigned int	fileSignature;
	unsigned int	reserved3;
	unsigned int	reserved4;
	
	unsigned char*  pStart;// raw header info: magic number and header size 
	unsigned char*  pBody; // raw header buffer of everything not in pStart

public:	// construct/destruct
	FMx_header() : pStart(NULL), pBody(NULL) {};

	~FMx_header()
	{	
		if (pStart) { delete[] pStart; }
		if (pBody)  { delete[] pBody; }
	};

public:	// implementation
	RETURNCODE readHeader(FILE* filePtr, bool saveBinary);
	unsigned  getOffset2Heap(void) { return(header_size+ddef_meta_size); };
	unsigned  getOffset2Imag(void) { return(header_size+ddef_meta_size+ddef_obj_size); };

	void out(void);// print to out
};

#endif //_FMX_HEADER_