/**********************************************************************************************
 *
 * $Workfile: FMA_Act_ReadPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Act_ReadPayload Visitor Operation on all object classes
 *		
 *
 */

#include "FMA_Activities.h"
#include "FMx.h"
#include "FMA_object.h"
#include "logging.h"




FMA_ReleasePayloadVisitor::FMA_ReleasePayloadVisitor()
{// no data, no construction...as of now
};

FMA_ReleasePayloadVisitor::~FMA_ReleasePayloadVisitor()
{// no data, no construction...as of now
};



// // required function
//	> x41 disregard, x81 won't be seen
//	> x43 disregard, x83 won't be seen
//	> x42 disregard for now, x82 -- lots of work to do
int FMA_ReleasePayloadVisitor::Operate( FMx_Base* pVisited )
{
	if ( pVisited == NULL )
		return FAILURE;
	FMA_Object* pObj = (FMA_Object*)pVisited;

	unsigned ct = pVisited->classType;
	if ( (ct & (FM8_type|FMA_type)) == FM8_type )
	{// we are going to disregard all FM8 for now
		return SUCCESS;
	}
	if ( ct & __Meta )
	{// we are not supposed to see x81 nor x83
	LOGIT(CERR_LOG,"ERROR: FMA_ReleasePayloadVisitor should never see classtype %#02x\n",ct);
		return FAILURE;
	}
	// we have to have class type FMA_Object

	// for now, use the objects read
	if (pObj->obj_Payload)
	{
		delete[] pObj->obj_Payload;
		pObj->obj_Payload = NULL;
	}
	return SUCCESS;
};


