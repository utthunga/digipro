/**********************************************************************************************
 *
 * $Workfile: FMx.cpp $
 * 20Jul11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA class implementation...the model of the file
 *		
 *note: prelim fma file:
 *  G:\DD_Library\PaulsOfficialDDISE_4_0_1\HCF\DDL\Library\000050\000a\0001.fm8
 */


#include "FMx.h"
#include "FMx_meta.h"
#include "FMx_objectSet.h"
#include "globals.h"

#include "logging.h"

#include <wchar.h>
#include "endian.h"

/* globals */
error_string    fms_errors[] = { FMx_ERROR_CODES };

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );

/* these are used for various supression requirements */
bool compress = false;
bool reconcile= false;// drop the non compatible stuff like hex dumps
// delete bool printIndices = false;	// put index number in front of dictionary and literal strings in the tables
bool maintenance = false;	// true for DD library maintenance
unsigned long long suppress = 0;		// stevev 14jan15- add some ptoc debugging capability

// debugging use only for setting breakpoints;
unsigned currentObjectIndex = 0;	
unsigned currentItemSymbolID = 0;

// 28mar16 - move to the file...unsigned char *	pImageBuffer = NULL;	// image binaries in contiguous block

// no longer used, use globals.h   FMx_Dict* FMx::StringDict = NULL;
// no longer used, use globals.h   FMx_Dict* FMx::DictionaryDict = NULL;

FMx::FMx()
{	
	pImageBuffer = NULL;
	pWfileNAME = NULL;
	langCode[0] = 0;
	fp          = NULL;

	fileSize = 0;
	idx      = 1;// 0 is header
	pMetaData= NULL;
	pObj_Data= NULL;


	pLIT_STR = NULL;
	pDICTSTR = NULL;
	daSymbolTbl   = NULL;

#ifdef SAVE_BINARY
	saveBinary = true;
#else
	saveBinary = false;
#endif

/********************
	pDefDesc   = NULL;
	pDevTables = NULL;
	pBlockTable= NULL;

	item_count     = 0;
	block_count    = 0;
	firstItemIndex = 0;
**********************/
}

FMx::~FMx()
{
	if( fp != NULL )
	{
		if ( fclose( fp ) )
		{
			wprintf( L"The file '%s' was not closed\n", pWfileNAME);
		}
		fp = NULL;
    }

	if (this->pMetaData != NULL)
	{
		delete pMetaData;
	}

	if (this->pObj_Data != NULL)
	{
		delete this->pObj_Data;
	}
}

/*********************************************************************************************
 *	open
 *
 *	in: the filename, the language code, a dictionary and a literal string table
 *      A NULL language code will load all languages (no filter)
 *
 *	returns: err code or success with the file open
 *
 *  just initialized the dictionary pointer , litstring pointer and filename
 *		trys to open the file, measure its size and leaves it open on success
 ********************************************************************************************/
RETURNCODE FMx::open(wchar_t* pFileName, const wchar_t* pLangCode, FMx_Dict* pDict, FMx_Dict* pLit)
{
	if (pFileName == NULL || pDict == NULL || pLit == NULL)
		return FMx_BADPARAM;
/* original globals
	StringDict = pLit;
	DictionaryDict = pDict;	
	pFilename = pFileName;
	*/
	pLIT_STR = pLit;
	pDICTSTR = pDict;
	pWfileNAME = pFileName;


// we need to file the two tables...
	if(NULL == pLangCode)
	{
		wcscpy_s(langCode, LANGCD_LEN, DEFAULT_LANGUAGE_CODE);// filter all but one language
	}
	else
	{	
/* stevev 31may2017 - tokenizer works in this legacy scenario so I'm leaving it for now
    Apparently languages have never worlked in the SDC using the fma parser*/
#ifdef TOKENIZER
		// change to be filter free...wcscpy_s(langCode,LANGCD_LEN,pLangCode);
		langCode[0] = 0;// load all languages
#else
		wcscpy_s(langCode,LANGCD_LEN,pLangCode);
#endif
	}

	errno_t enf = _wfopen_s(&fp, pFileName , L"rb");

	if (enf)// zero on success
	{
		wchar_t errStr[ERROR_STR_BUFFER_LEN];
		enf = _wcserror_s(errStr, ERROR_STR_BUFFER_LEN, 0);

		if (enf) 
			errStr[0] = 0;

		fp = NULL;// so we don't try to close an error file
		LOGIT(CLOG_LOG,L"- Could not open DD file '%s' :%s\n", pWfileNAME,errStr);

		return FMx_FILEFAIL;
	}
	else
	{
		fseek(fp, 0, SEEK_END);
		fileSize = ftell(fp);
		rewind(fp);// back to the beginning
	}

	return SUCCESS;
}// end open()

RETURNCODE FMx::VisitAll(FMx_visitor* pVistor)
{ 
	RETURNCODE r = FAILURE;
	if (pVistor == NULL)
		return r;// fail

	if (pMetaData)
		r = pMetaData->ExecuteVisitor(pVistor);

//STEVEV TEMP	if (r == SUCCESS && pObj_Data)
	if (pObj_Data)
		r = pObj_Data->ExecuteVisitor(pVistor);

	return r;
}

unsigned FMx::getObjectCount(void) 
{ 
	if (pMetaData == 0) 
		return 0; 
	else                 
		return pMetaData->get_item_count();
}

unsigned FMx::getFirstItemIndex(void)
{
	if (pMetaData == 0) 
		return 0; 
	else                 
		return pMetaData->get_first_obj_index();
}

unsigned FMx::getNumberBlocks(void)
{
	if (pMetaData == 0) 
		return 0; 
	else                 
		return pMetaData->get_block_count();
}


void FMx::getLiteralString(int strNum, wstring& retVal)
{
	if (pLIT_STR == NULL)
	{
		retVal = L"No literal strings loaded.";
		return;
	}
	pLIT_STR->retrieveStr(strNum, retVal);
	return;
}

void FMx::getDictionString(int strNum, wstring& retVal)
{
	if (pDICTSTR == NULL)
	{
		retVal = L"No dictionary strings loaded.";
		return;
	}
	pDICTSTR->retrieveStr(strNum, retVal);
	return;
}

void FMx::getDictionString(wstring name, wstring& retVal)
{
	if (pDICTSTR == NULL)
	{
		retVal = L"No dictionary strings loaded.";
		return;
	}
	pDICTSTR->retrieveStr(name, retVal);
	return;
}

void FMx::getSymbolName(unsigned symNum,  string& retVal, int& itmType)
{
	if (daSymbolTbl == NULL)
	{
		retVal = "No symbols loaded.";
		return;
	}
	daSymbolTbl->retrieveStr(symNum, retVal, itmType);
	return;
}

void FMx::getSymbolNameByIndex(unsigned index,  string& retVal, int& itmType)
{
	if (daSymbolTbl == NULL)
	{
		retVal = "No symbols loaded.";
		return;
	}
	daSymbolTbl->retrieveStrByIndex(index, retVal, itmType);
	return;
}