#ifndef _ATTRIBUTESTRING_
#define _ATTRIBUTESTRING_

#define MAX_ITEM_TYPE    38
#define MAX_ATTR_TYPE	 40		/* max attributes per item */
#define FM8ATTRTYPLEN    22		/* maximum string length */

#define FM8ATTRTYPSTRINGS \
	/*0 unk*/	{	{"Undefined"}	},\
	/*1 var*/	{	{"Class"},\
					{"Handling"},\
					{"Constant Unit"},\
					{"Label"},\
					{"Help"},\
					{"Width"},	/*was ReadTimeOut*/\
					{"Height"},	/*was WriteTimeOut*/\
					{"Validity"},\
					{"Pre Read Actions"},\
					{"Post Read Actions"},\
					{"Pre Write Actions"},\
					{"Post Write Actions"},\
					{"Pre Edit Actions"},\
					{"Post Edit Actions"},\
					{"Response Codes"},\
					{"Variable Type"},\
					{"Display Format"},\
					{"Edit Format"},\
					{"Min-Value"},\
					{"Max-Value"},\
					{"Scaling Factor"},\
					{"Enumerations"},\
					{"Indexed Item"},\
					{"Default-Value"},\
					{"Refresh Actions"},\
					{"Item Information"},\
					{"Post Request Actions"},\
					{"Post User Action"},\
					{"Time Format"},\
					{"Time Scale"},\
					{"Visible"},\
					{"Private"}				},\
	/* 2 cmd*/	{	{"Number"},\
					{"Operation"},\
					{"Transaction"},\
					{"Response Codes"},\
					{"Item Information"}	},\
	/* 3 menu*/ {	{"Label"},\
					{"Items"},\
					{"Help"},\
					{"Validity"},\
					{"Style"},\
					{"Item Information"},\
					{"Visible"},\
					{"Post Read Actions"},\
					{"Post Write Actions"},\
					{"Pre Read Actions"},\
					{"Pre Write Actions"}				},\
	/* 4 edDis*/ {	{"Label"},\
					{"EditItems"},\
					{"DisplayItems"},\
					{"Pre Edit Actions"},\
					{"Post Edit Actions"},\
					{"Help"},\
					{"Validity"},\
					{"Item Information"},\
					{"Visible"}					},\
	/* 5 meth*/ {	{"Class"},\
					{"Label"},\
					{"Help"},\
					{"Definition"},\
					{"Validity"},\
					{"Scope"},\
					{"Method Type"},\
					{"Method Parameters"},\
					{"Item Information"},\
					{"Visible"},\
					{"Private"}					},\
	/* 6 refr*/ {	{"Watch List"},\
					{"Item Information"}, /*was Update List*/ \
					{"Unused Item-06"},\
					{"Watch List"},\
					{"Update List"}		},\
	/* 7 unit*/ {	{"Watch Variable"},\
					{"Item Information"}, /*was Update List*/	\
					{"Unused Item-07"},\
					{"Watch Variable"},\
					{"Update List"}		},\
	/* 8 wao */ {	{"Watch List"},\
					{"Item Information"} 		},\
	/* 9 rArr*/ {	{"Number Of Elements"},\
					{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Item Information"},\
					{"Private"}					},\
	/*10 coll*/ {	{"Members"},\
					{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Item Information"},\
					{"Visible"},\
					{"Private"}					},\
	/*11 res */ {	{"Reserved Item-11"}		},\
	/*12 bloc*/ {	{"Characteristics"},\
					{"Label"},\
					{"Help"},\
					{"Parameters"}				},\
	/*13 unus*/ {	{"Unused Item-13"}			},\
	/*14 recd*/ {	{"Unused Item-14"}			},\
	/*15 vArr*/ {	{"Unused Item-15"},\
					{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Type Definition"},\
					{"Length"},\
					{"Item Information"},\
					{"Private"}					},\
	/*16 unus*/ {	{"Unused Item-16"}			},\
	/*17 unus*/ {	{"Unused Item-17"}			},\
	/*18 unus*/ {	{"Unused Item-18"}			},\
	/*19 unus*/ {	{"Unused Item-19"}			},\
	/*20 file*/ {	{"Members"},\
					{"Label"},\
					{"Help"},\
					{"Unused attr-20"},\
					{"Item Information"},\
					{"Handling"},\
					{"Update Actions"},\
					{"Identity"}				},\
	/*21 chrt*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Height"},\
					{"Width"},\
					{"Chart Type"},\
					{"Length"},\
					{"Cycle Time"},\
					{"Members"}	,\
					{"Item Information"},\
					{"Visible"}					},\
	/*22 grph*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Height"},\
					{"Width"},\
					{"X Axis"},\
					{"Members"},\
					{"Item Information"},\
					{"Cycle Time"},\
					{"Visible"}					},\
	/*23 axis*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"MinAxis"},\
					{"MaxAxis"},\
					{"Scaling"},\
					{"Constant Unit"},\
					{"Item Information"},\
					{"View MinAxis"},\
					{"View MaxAxis"}			},\
	/*24 wave*/ {	{"Label"},\
					{"Help"},\
					{"Handling"},\
					{"Emphasis"},\
					{"Line Type"},\
					{"Line Color"},\
					{"Axis"},\
					{"Key X Values"},\
					{"Key Y Values"},\
					{"Waveform Type"},\
					{"X Values"},\
					{"Y Values"},\
					{"X Initial"},\
					{"X Increment"},\
					{"Number of Points"},\
					{"Init Actions"},\
					{"Refresh Actions"},\
					{"Exit Actions"},\
					{"Item Information"},\
					{"Validity"}			},\
	/*25 srce*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Emphasis"},\
					{"Line Type"},\
					{"Line Color"},\
					{"Axis"},\
					{"Members"},\
					{"Item Information"},\
					{"Init Actions"},\
					{"Refresh Actions"},\
					{"Exit Actions"}			},\
	/*26 list*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Type Definition"},\
					{"Count"},\
					{"Capacity"},\
					{"Item Information"},\
					{"First"},\
					{"Last"},\
					{"Private"}					},\
	/*27 grid*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Height"},\
					{"Width"},\
					{"Orientation"},\
					{"Handling"},\
					{"Vectors"},\
					{"Item Information"},\
					{"Visible"}		},\
	/*28 imag*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Link"},\
					{"Image Table Index"},\
					{"Item Information"},\
					{"Visible"}		},\
	/*29 blob*/ {	{"Label"},\
					{"Help"},\
					{"Handling"},\
					{"Identity"},\
					{"Item Information"}		},\
	/*30 plugin*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Visible"},\
					{"UUID"}		},\
	/*31 temp*/ {	{"Label"},\
					{"Help"},\
					{"Validity"},\
					{"Default Values"},\
					{"Item Information"}		},\
	/*32 unus*/ {	{"Unused Item-32"}			},\
	/*33 unus*/ {	{"Non-HARTe"}			},\
	/*34 unus*/ {	{"Non-HARTf"}			},\
	/*35 unus*/ {	{"Non-HARTg"}			},\
	/*36 unus*/ {	{"Non-HARTh"}			},\
	/*37 unus*/ {	{"Non-HARTi"}			}

class AttributeString
{
private:
	static char attr8Type_Str[MAX_ITEM_TYPE][MAX_ATTR_TYPE][FM8ATTRTYPLEN];

public:
	AttributeString(void) {}
	~AttributeString(void) {}

	static char* get8(int itemType, int attrType)
	{
		return attr8Type_Str[itemType][attrType];
	};
};

#endif //_ATTRIBUTESTRING_
