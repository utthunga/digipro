#ifndef _FMX_META_
#define _FMX_META_

#include "FMx_Base.h"

class FMx;

/*********************************************************************************************
 * FMx_meta
 ********************************************************************************************/
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class is the base class for reading the meta data section before the objects.
 *			It holds the common information from both FM8 & FMA formats.
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMx_meta : public FMx_Base
{  
public:	// construct/destruct
	FMx_meta(FMx* pTC, FILE* pF, bool sb, unsigned ct) : FMx_Base(pTC, pF,sb, ct) {};
	virtual ~FMx_meta(){};

public: // workers - these need to be virtual
	virtual RETURNCODE index_MetaData(unsigned& size) = 0;
	virtual unsigned  get_item_count(void) = 0;// accessor
	virtual unsigned  get_first_obj_index(void) = 0;
	virtual unsigned  get_block_count(void) = 0;
};

#endif //_FMX_META_
