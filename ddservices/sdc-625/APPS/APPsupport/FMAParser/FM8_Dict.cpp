/**********************************************************************************************
 *
 * $Workfile: FM8_Dict.cpp $
 * 29oct12 - stevev-from fma
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Dict class implementation...the model of the file
 *		
 *
 */

#include "Convert.h"
#include "FM8_Dict.h"
#include "FM8_Support_tables.h"
#include "logging.h"
#include "v_N_v+Debug.h"


	
FM8_Dict::FM8_Dict()  // constructor
{
}


FM8_Dict::~FM8_Dict()
{
}


void FM8_Dict::setTheTable( FMx_Table* pTbl)
{
	pTable = pTbl;
	if (pTbl == NULL)
	{
		LOGIT(CERR_LOG,L"FM8_Dict::setTheTable passed a NULL table.\n");
		return;
	}
	FM8_DictTable* pT = dynamic_cast<FM8_DictTable*>( pTbl );

	FM8_DictTable::iterator vLIT;
	dictString_entry_s *pDictEntry;

	wstring locNm, locStr;
	unsigned long strKey;//, newIdx;

	for (vLIT = pT->begin(); vLIT != pT->end(); ++vLIT )
	{
		pDictEntry = &(*vLIT);

		strKey = (unsigned long)pDictEntry->number;
		locNm  = (std::wstring)pDictEntry->name;
		locStr = (std::wstring)pDictEntry->actualStr;

#ifdef TOKENIZER
		/*
		* In tokenizing, we want all the full string with all
		* languages, as these will be stored in the final
		* encoded DD
		*/
#else
		ConvertToString::filterString(locStr);
#endif
		addStr(locStr, locNm, strKey);	//  [6/8/2016 tjohnston]

		if (! saveBinary)
		{// empty the original (DD) string
			(*vLIT).name.clear();// try to prevent having a bunch of string copies
			(*vLIT).actualStr.clear();
		}	
	}// next row
}



/* 12jan16, let's try it the right way::...fall thru to the parent
void FM8_Dict::retrieveStr(int  str,   wstring& retVal)
{
	StrByNumber_t::iterator dictStrIT;

	dictStrIT = strByKey.find(str);

	if (dictStrIT == strByKey.end() )
	{
		retVal = L""; // [6/22/2015 timj]  L"****FM8_Dict did not find String by number.\n";
	}
	else
	{
		retVal = theStrList[dictStrIT->second];
	}
}

void FM8_Dict::retrieveStr(wstring nm, wstring& retVal)
{
	StrByString_t::iterator dictStrIT;

	dictStrIT = strByName.find(nm);

	if (dictStrIT == strByName.end() )
	{
		retVal = L""; //  [6/22/2015 timj]  L"****FM8_Dict did not find String by Name.\n";
	}
	else
	{
		retVal = theStrList[dictStrIT->second];
	}
}
**/
