/**********************************************************************************************
 *
 * $Workfile: FM8_handlers.h $
 * 07sep11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_handlers.h : This is the abstract base class for the content handlers
 *
 * #include "FM8_handlers.h"
 *
 */

#ifndef _FMA_FM8_HANDLERS_H
#define _FMA_FM8_HANDLERS_H

#ifdef INC_DEBUG
#pragma message("In FM8_handlers.h") 
#endif

#include "FMx_general.h"
#include "FMx_handler.h"
#include "FM8_meta.h"	/* try this */
#include "FM8_Attributes.h"
#include "FM8_Support_Tables.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FM8_handlers.h") 
#endif

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 Defines
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class FM8_Object;
class FM8_ODES;

class FM8_handler : public FMx_handler
{
public:	// construct/destruct
	FM8_handler(FMx_Base* pOwner) : FMx_handler(pOwner) {};
	virtual ~FM8_handler(){};

public:
	// these three are used to deal with heap segments
	virtual int findOffset(unsigned offset) = 0;// tells what to do with the current location
	virtual int readChunk(unsigned& sz) { return 0; };// done if findOffset successful...
	virtual RETURNCODE readSegment(segment_t seg, UCHAR** ppTbl, unsigned& offset);

	int getTableRows( UCHAR* p);
	void out5( UCHAR *p, char *stype );
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  These classes hold the FM8 interface definition 
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FM8_ODES_handler : public FM8_handler
{
public:	// construct/destruct
	FM8_ODES_handler(FM8_ODES* pOwner) : FM8_handler((FMx_Base*)pOwner) {};
	virtual ~FM8_ODES_handler(){};

public:	// data
	unsigned /*short*/	index;// should always be zero
	unsigned /*short*/	version;// version number for DDOD??? usually 2
	unsigned long	local_address_sod;	/*long_offset; beginning of file to SOD objects*/ 
	unsigned long	local_address_ODES;	/*long_offset; beginning of file to this object*/
	// diff of above should be 41 0x29, the size of the ODES

	unsigned /*short*/	firstItemIndex;// in both fm8 & fma, normally 100
	unsigned /*short*/	item_count;    // in both fm8 & fma

public: // workers
	int evaluateExtension();// does nothing because it's already done
	
	int evaluate__Extension();		// breaks up the extension part
	int findOffset(unsigned offset) { return 0; };// this has no annex

	int out(void);// sends text version to out
	void out2(void){return;};// this has no annex
	void out3(void);
};



class FM8_Format_handler : public FM8_handler
{
public:	// construct/destruct
	FM8_Format_handler(FM8_Object* pOwner) : pObj(pOwner), FM8_handler((FMx_Base*)pOwner), objCode(0), major_rev(0), minor_rev(0), dd_rev(0), profile(0xffff) {};
	virtual ~FM8_Format_handler(){};

public:
	FM8_Object	*pObj;

public:	// data
	unsigned char	objCode;// will be 128, x80
	unsigned char	major_rev;
	unsigned char	minor_rev;
	unsigned char	dd_rev;
	unsigned /*short*/	profile;
	unsigned /*short*/	import_count;// should always be zero 
	unsigned /*short*/	like_count;  // should always be zero

public: // workers
	int evaluateExtension();		// breaks up the extension part
	int findOffset(unsigned offset){return 0;};// this has no annex

	int out(void);// sends text version to out
	void out2(void){return;};// this has no annex
	void out3(void);
};


class FM8_DevDir_handler : public FM8_handler
{
public:	// construct/destruct
	FM8_DevDir_handler(FM8_Object* pOwner);
	virtual ~FM8_DevDir_handler();

public:
	FM8_Object *pObj;

public:	// data
	unsigned char objCode;// will be 129, x81
	unsigned char fmtCode;// will be zero

public: // Table Data
	segment_t   devDirTblInfo[DEV_DIR_TBL_CNT];// offset and length
	UCHAR*		devDirTables[DEV_DIR_TBL_CNT];// an array of char ptrs-raw data
	unsigned	devDirTblOffset[DEV_DIR_TBL_CNT];

	//evaluated Tables
	FM8_LitStringTable			literalStringList;
	FM8_DictTable				dictStringList;
	FM8_ImageTable              imageList;

public: // workers
	int evaluateExtension();// breaks up the extension part
	int findOffset(unsigned offset);// tells what to do with the current location
	virtual int readChunk(unsigned& sz);

	int  out(void);// sends text version to out
	void out2(void);// sends text version to out
	void out3(void);

	// accessors
};


enum blkDirTblIndex_e
{
	blk_item_idx,	// 0
	blk_item_nm_idx,
	parameter_idx,
	relation_idx,
	update_idx,     // 4
	param_cmd_idx,
	critical_idx
	,resolvedRef_cmd_idx
};

#define BLK_DIR_TBL_CNT 8	// stevev 07apr15 - for extra ptoc table

#define blk_item_segment		(blkDirTblInfo[   blk_item_idx])
#define blk_item_nm_segment		(blkDirTblInfo[blk_item_nm_idx])
#define param_segment			(blkDirTblInfo[parameter_idx])
#define relation_segment		(blkDirTblInfo[ relation_idx])
#define update_segment			(blkDirTblInfo[   update_idx])
#define param_cmd_segment		(blkDirTblInfo[param_cmd_idx])
#define critical_param_segment	(blkDirTblInfo[ critical_idx])
#define rslvdRef_cmd_segment	(blkDirTblInfo[resolvedRef_cmd_idx])
	
#define blk_item_data		(blkDirTables[   blk_item_idx])
#define blk_item_nm_data	(blkDirTables[blk_item_nm_idx])
#define param_data			(blkDirTables[parameter_idx])
#define relation_data		(blkDirTables[ relation_idx])
#define update_data			(blkDirTables[   update_idx])
#define param_cmd_data		(blkDirTables[param_cmd_idx])
#define critical_param_data	(blkDirTables[ critical_idx])
#define rslvdRef_cmd_data	(blkDirTables[resolvedRef_cmd_idx])

class FM8_BlkDir_handler : public FM8_handler
{
public:	// construct/destruct
	FM8_BlkDir_handler(FM8_Object* pOwner);
	virtual ~FM8_BlkDir_handler();

public:
	FM8_Object *pObj;

public:	// data
	unsigned char	objCode;// will be 129, x81
	unsigned char	fmtCode;// will be zero

public: // Table Data
	segment_t   blkDirTblInfo[BLK_DIR_TBL_CNT];
	UCHAR*		blkDirTables[BLK_DIR_TBL_CNT];// an array of char ptrs
	unsigned	blkDirTblOffset[BLK_DIR_TBL_CNT];

	//evaluated Tables
	FM8_CriticalParameterTable	critParamTable;
	FM8_RRptocTable             RRptocTable;

public: // workers
	int evaluateExtension();// breaks up the extension part
	int findOffset(unsigned offset);// tells what to do with the current location
	virtual int readChunk(unsigned& sz);

	int  out(void);// sends text version to out
	void out2(void);// sends text version to out
	void out3(void);

	// accessors
};

class FM8_Item_handler : public FM8_handler
{
public:	// construct/destruct
	 FM8_Item_handler(FM8_Object* pOwner);
	 virtual ~FM8_Item_handler();

public:
	unsigned  char attrCntRemaining;// stevev made a char 03aug12

public:	// data
	FM8_Object *pObj;

	unsigned        item_number;//aka symbol ID
	unsigned        item_attr_mask;// should always be 4 long
	unsigned		item_attr_cnt;

	// attribute list
	FM8_AttributeList items_attributes;

	// other helpful information
	unsigned        length2firstAttr;
//	aCitemBase*		protoItem;
	unsigned char	item_type;		// moved these to the bottom to facilitate alignment
	unsigned char	item_subtype;

public: // workers
	int evaluateExtension();// breaks up the extension part
	void generatePrototype();
	int findOffset(unsigned offset);// temporary do-nothing
	virtual int readChunk(unsigned& sz);

	int out(void);// sends text version to out
	void out2(void);
	void out3(void);
};

#endif //_FMA_FM8_HANDLERS_H
