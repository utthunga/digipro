/**********************************************************************************************
 *
 * $Workfile: FMA_Attributes_Ref&Expr.cpp $
 * 23mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Attribute Expression and Reference class implementations
 */

#include "FMx_general.h"
#include "FMA_AttributeClass.h"
#include "FMA_defs.h"
#include "FMA_Attributes_Ref&Expr.h"

#include "FMx_Support_primatives.h"
#include "FMA_Attributes.h"
#include "Convert.h"

#include "tags_sa.h"

extern void outHex(UCHAR* pS, unsigned offset, unsigned len);

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

int expressionElementA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;

	unsigned tagValue  = 0;
	unsigned tagLength = 0;
	unsigned entryOffset = offset; // for debugging only
	
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length, tagValue, tagLength, offset);

	if (ret != SUCCESS)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x with error parsing tag.\n", 
																	entryOffset);
		return ret;
	}

	if (tagLength != 0)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x  implicit tag with length %d.\n", 
																	entryOffset, tagLength);
		return ret;
	}

	if (tagValue > (unsigned)eet_array_index)
	{		
		LOGIT(CERR_LOG,"Error: Expression element @ 0x%08x with invalid Value &u.\n", 
																	entryOffset, tagValue);
		return ret;
	}

	rawElemType = (expressElemType_t)tagValue;
	ee_type =     (exprElemType_t)tagValue;
	exprElemType = ConvertToDDL::ExpressionElementType(ee_type);

	if (ee_type < eet_integer_constant || ee_type > eet_method_value )
	{
		return SUCCESS;// operator, has no parameters
	}
	// else, parse some more stuff

	if (ee_type == eet_integer_constant || ee_type == eet_unsigned_constant)
	{
		ret = parseInteger(8, pData, length, eeInt, offset);

		if ( ee_type == eet_unsigned_constant)
		{
			LOGIT(CERR_LOG,"Error: Expression element with non-hart unsigned const.\n");
			return FMx_BAD_PARSE;
		}

		eeIsUnsigned = false;
	}

	if ( ee_type == eet_floating_constant )
	{
		if (length >= 8)
		{
			ret = parseDouble(pData, length, eeFloat, offset);
		}
		else
		{
			LOGIT(CERR_LOG,"Error: Expression element with FLOAT length coding.\n");
			ret = parseFLOAT(pData, length, eeFloat, offset);
		}
	}

	if ( ee_type == eet_string_constant )
	{
		eepString = new faCStringA;

		if (eepString == NULL)
		{
			return FMx_MEMFAIL;
		}

		ret = eepString->evaluateAttribute(pData, length, offset);
	}

	if (ee_type==eet_numeric_value || ee_type==eet_all_value || ee_type==eet_method_value)
	{//Reference
		eepRef = new abCReferenceA;

		if (eepRef == NULL)
		{
			return FMx_MEMFAIL;
		}

		ret = eepRef->evaluateAttribute(pData, length, offset);
	}

	if ( ee_type == eet_attr_value )
	{//enum1 + Reference		
		eeAttrType = (attr_t)*(*pData); // straight up 1 byte enum	
		(*pData)++; length--; offset ++;
		eepRef = new abCReferenceA;

		if (eepRef == NULL)
		{
			return FMx_MEMFAIL;
		}

		ret = eepRef->evaluateAttribute(pData, length, offset);
	}

	if ( ee_type == eet_min_max_value )
	{//whichINTEGER + enum1 + Reference		
		ret = parseInteger(8, pData, length, eeInt, offset);
		if ( ret != SUCCESS )
		{
			return ret;
		}	

		eeAttrType     = (attr_t)*(*pData); // straight up 1 byte enum	
		(*pData)++; length--; offset ++;
		eepRef = new abCReferenceA;

		if (eepRef == NULL)
		{
			return FMx_MEMFAIL;
		}

		ret = eepRef->evaluateAttribute(pData, length, offset);
	}

	return ret;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

int abCExpressionA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{	
	RETURNCODE  ret = 0;
	unsigned tagValue;
	unsigned tagLength;
	expressionElementA* pExprElem;
	int exitLength, workingLength;
	unsigned exitOffset;
	UCHAR* exitPdata;
	unsigned entryOffset = offset;// for debugging only
	
	if ( length <= 0 )
	{
		LOGIT(CERR_LOG,"Error: Expression attribute @ 0x%08x with ZERO length.\n", entryOffset);
		return -1;
	}

	// explicit tag 
	ret = parseTag(pData, length, tagValue, tagLength, offset);

	if (tagLength > (unsigned)length || tagLength == 0)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute @0x%08x with incorrect tag's length.\n", entryOffset);
		return -1;
	}
	if (tagValue != STRUCTURE_TAG_EXPRESSION)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute @0x%08x with incorrect tag (%u).\n", entryOffset, tagValue);
		return -2;
	}

	workingLength = tagLength;
	exitLength = length   - tagLength;
	exitOffset = offset   + tagLength;
	exitPdata  = (*pData) + tagLength;

	while ( workingLength > 0 && ret == SUCCESS )// do elements as long as we can
	{
		pExprElem = new expressionElementA;

		if (pExprElem == NULL)
			return FMx_MEMFAIL;

		ret = pExprElem->evaluateAttribute(pData, workingLength, offset);

		if ( ret == SUCCESS )
		{
			exprList.push_back(pExprElem);
		}
		else
			delete pExprElem;
		pExprElem = NULL;
	}//		wend

	if (workingLength != 0)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval length mismatch.\n");
	}

	if (offset != exitOffset)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval offset mismatch.\n");
	}

	if ((*pData) != exitPdata)
	{
		LOGIT(CERR_LOG,"Error: Expression attribute eval pointer mismatch.\n");
	}

	length = exitLength;

	return ret;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

int abCReferenceA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;
	unsigned tagValue;
	unsigned tagLength;


	// explicit tag 
	ret = parseTag(pData, length, tagValue, tagLength, offset);

	unsigned reflen = tagLength;
	unsigned offset0 = offset;

	if (tagLength > (unsigned)length || tagLength == 0)
	{
		LOGIT(CERR_LOG,"Error: Reference attribute with incorrect tag's length.\n)");
		return -1;
	}

	if ( tagValue != STRUCTURE_TAG_REFERENCE )
	{
		LOGIT(CERR_LOG,"Error: Reference attribute with incorrect tag (%u).\n)",tagValue);
		return -2;
	}

	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset );

	if ( ret != SUCCESS || tagLength != 0 || tagValue >= (unsigned)rT_invalid )
	{		
		LOGIT(CERR_LOG,"Error: Reference Type is invalid. (0x%02x)\n)", tagValue);
		return ret;
	}

	rawRefType = tagValue;						// the FMA ref type
	refType = ConvertTo8::refType(rawRefType);	// see comment where refType is defined

	switch (rawRefType)
	{
		case rT_item_id:					//[ 0] 
		case rT_variable_id:
		case rT_menu_id: 
		case rT_edit_display_id:
		case rT_method_id:
		case rT_refresh_id:					//[ 5]
		case rT_unit_id:
		case rT_write_as_one_id: 
		case rT_reference_array_id:
		case rT_collection_id:
		case rT_block_b_id:					//[10]
		case rT_block_a_id: 
		case rT_record_id:
		case rT_array_id: 
		case rT_var_list_id:		
		case rT_response_codes_id:			//[15]
		case rT_file_id:
		case rT_chart_id:
		case rT_graph_id:
		case rT_axis_id:
		case rT_waveform_id:				//[20]
		case rT_source_id:
		case rT_list_id: 
		case rT_grid_id: 
		case rT_image_id:
		case rT_blob_id:					//[25]
		case rT_undefined26:
		case rT_template_id: 
		case rT_component_id: 
		case rT_component_folder_id: 
		case rT_component_reference_id:		//[30]
		case rT_component_relation_id: 
		case rT_interface_id: 
		case rT_param:
		case rT_param_list:
		case rT_block_char:
		case rT_local_param:
		{// integer			
			ret = parseInteger(8, pData, length, intValue, offset);
		}
		break;

		case rT_rowbreak_ref:
		case rT_separator_ref:
		{// nothing more - check length
#ifdef _DEBUG
			// was: assert(length == 0);
			// check that the ref was consumed
			// note that the length may be legitimately > 0 at this point
			int len = reflen - (offset - offset0);
			assert(len == 0);
#endif
		}
		break;

		case rT_constant_ref:				//[35]
		{// 35 expression			
			pExpr = (abCExpression*)new abCExpressionA;

			if (pExpr == NULL)
				return FMx_MEMFAIL;

			ret = pExpr->evaluateAttribute(pData, length, offset);
		}
		break;

		case rT_via_reference_array:
		case rT_via_value_array:
		case rT_via_list:
		case rT_via_block_instance:
		{// 36,39,45, 50 expression,  reference			
			pExpr = (abCExpression*)new abCExpressionA;

			if (pExpr == NULL)
				return FMx_MEMFAIL;

			ret = pExpr->evaluateAttribute(pData, length, offset);

			if ( ret == SUCCESS)
			{		
				pRef = (abCReference*)new abCReferenceA;

				if (pRef == NULL)
					return FMx_MEMFAIL;

				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case rT_via_collection:
		case rT_via_record:	
		case rT_via_var_list:				//[40]
		case rT_via_file:
		case rT_via_chart:
		case rT_via_graph:
		case rT_via_source:						
		case rT_via_bit_enum:			//[46]
		case rT_via_Xblock_member:
		{// 37,38, 40,41,42,43,44,46, 49 integer, reference		
			ret = parseInteger(8, pData, length, intValue, offset);
			if ( ret == SUCCESS)
			{		
				pRef = (abCReference*)new abCReferenceA;
				if (pRef == NULL)
					return FMx_MEMFAIL;
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case rT_via_attribute:
		{// 47 enum1, reference				
			intValue = *(*pData);
			(*pData)++; length--; offset++;
			if ( ret == SUCCESS)
			{		
				pRef = (abCReference*)new abCReferenceA;
				if (pRef == NULL)
					return FMx_MEMFAIL;
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case rT_ref_with_arguments:			//[55] 
		{//explicit tag 62, args are references till taglength gone
			unsigned localTemp;
			int      localLen;
			abCReferenceA* p_ref;

			ret = parseTag(pData, length,   tagValue,   localTemp,   offset );

			if (tagValue != STRUCTURE_TAG_ARGUMENT)
			{
				LOGIT(CERR_LOG,"Error: Reference w/Args has bad tag. (0x%02x)\n)",tagValue);
				ret = FMx_BAD_PARSE;
			}
			else
			{
				localLen = (int)localTemp;
				length -= localLen;
				while(localLen > 0)
				{
					p_ref = new abCReferenceA;
					if ( p_ref == NULL )
					{
						return FMx_MEMFAIL;
					}
					ret = p_ref->evaluateAttribute(pData, localLen, offset);
					if ( ret == SUCCESS )
					{
						argumentList.push_back(p_ref);
					}
				}//wend
				assert( length > 0 );//we have more to do
				pRef = new abCReferenceA;
				if ( pRef == NULL )
				{
					return FMx_MEMFAIL;
				}
				ret = pRef->evaluateAttribute(pData, length, offset);
			}
		}
		break;

		case rT_reserved_1:
		case rT_invalid:
		default:
		{
			LOGIT(CERR_LOG,"Error: Reference Type is incorrect. (0x%02x)\n)",tagValue);
			ret = FMx_BAD_PARSE;
		}
		break;
	}//endswitch on ref type

	return ret;	
}

