/**********************************************************************************************
 *
 * $Workfile: FMA_Symbol.h $
 * 08mar13 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Symbol.h:  The index file for the symbol table, and associated accessors
 *
 * #include "FMA_Symbol.h"
 *
 */

#ifndef _FMA_SYMBOL_H
#define _FMA_SYMBOL_H

#ifdef INC_DEBUG
#pragma message("In FMA_Symbol.h") 
#endif

#include "FMx_Symbol.h"

using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_Symbol.h") 
#endif

struct symInfo
{
	int    symType;
	string symName;
};

typedef	map<unsigned, symInfo>    NameByNumber_t; // symbolNumber::asciiString+type


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the *.fma dictionary in two modes
 *	first -	Single language mode - only the start language is loaded
 *	second- All languiages, for changing languages on the fly
 *
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMA_Symbol : public FMx_Symbol
{	
public: // data	
	NameByNumber_t  theSymTable;// a map, the same, even if we implement one in FM8
	vector<unsigned> symbolId;	// symbol ID (aka symbol number) by symbol table index

#pragma message("FMA_Symbol is in")

public:		
	FMA_Symbol();  // constructor
	virtual ~FMA_Symbol();

public:		// workers
	virtual void retrieveStr(unsigned symNum, string& retVal, int& retType);
	virtual void retrieveStrByIndex(unsigned index, string& retVal, int& retType);
	virtual void setTheTable( FMx_Table*   pTbl);

protected:	// helpers

};



#endif	// _FMA_SYMBOL_H