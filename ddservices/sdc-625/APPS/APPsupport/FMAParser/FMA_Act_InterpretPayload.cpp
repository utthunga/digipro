/**********************************************************************************************
 *
 * $Workfile: FMA_Act_ReadPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Act_ReadPayload Visitor Operation on all object classes
 *		
 *
 */

#include "FMA_Activities.h"
#include "FMx.h"
#include "FMA_handlers.h"
#include "FMA_object.h"
#include "Endian.h"
#include "logging.h"

/************ now done in handler via visitors **********
class DDEF_DESC
{
public: // data
	FMA_DDdescription* pDesc;

public:  // construct/destruct
	DDEF_DESC(FMA_DDdescription* pObj):pDesc(pObj)
	{};
	~DDEF_DESC()
	{
		pDesc=NULL;
	};
public:  // workers

	RETURNCODE setDescVals()
	{
		if (pDesc == NULL)
			return FAILURE;
	
		// done in another step now:::>pDefDesc->read_ObjectPayload();
		// parse the description table
		if (pDesc->obj_Payload != 0 && pDesc->obj_type == obj_Description)
		{
			pDesc->item_count     
				= REVERSE_L(*((unsigned int*  )&(pDesc->obj_Payload[DESC_ITMCNT_OFFSET])));
			pDesc->block_count    
				= REVERSE_S(*((unsigned short*)&(pDesc->obj_Payload[DESC_BLKCNT_OFFSET])));
			pDesc->firstItemIndex 
				= REVERSE_L(*((unsigned int*  )&(pDesc->obj_Payload[DESC_FRSTITMIDX_OFFSET])));
			return SUCCESS;
		}
		else
		{// empty meta data
			return FAILURE;
		}
	};
};
*******************************************************/
/* +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ * +-+ */


FMA_InterpretPayloadVisitor::FMA_InterpretPayloadVisitor()
{// no data, no construction...as of now
};

FMA_InterpretPayloadVisitor::~FMA_InterpretPayloadVisitor()
{// no data, no construction...as of now
};



// // required function
//	> x41 disregard, x81 won't be seen(fm8-meta & fma-meta)
//	> x43 disregard, x83 won't be seen(fm8-objset * fma-objset)
//	> x42 disregard for now, x82 -- lots of work to do (fm8-obj, work on fma-obj)
int FMA_InterpretPayloadVisitor::Operate(FMx_Base* pVisited)
{
	RETURNCODE r = FAILURE;
	FMA_Object* pThisObject;
	if ( pVisited == NULL )
		return FAILURE;

	unsigned ct = pVisited->classType;
	if ( (ct & (FM8_type|FMA_type)) == FM8_type ) //(fm8-meta fm8-objset,fm8-obj)
	{// we are going to disregard all FM8 for now
		return SUCCESS;
	}
	if ( ct & __Meta )//( fma - meta & fma-objset )
	{// we are not supposed to see x81 nor x83
	LOGIT(CERR_LOG,"ERROR: FMA_InterpretPayloadVisitor should never see classtype %#02x\n",ct);
		return FAILURE;
	}
	// we have to have class type FMA_Object <all that's left>
	pThisObject = (FMA_Object*)pVisited;
	if (pThisObject->pHandler == NULL)// don't have one yet
	{// get one
		switch ( pThisObject->obj_type )
		{
		case obj_Description:
			{
				pThisObject->pHandler = new FMA_Description_handler(pThisObject);
				r = SUCCESS;
				//DDEF_DESC ddef((FMA_DDdescription*)pThisObject);
				//r = ddef.setDescVals();
			}
			break;
		case obj_Device_Tbls:
			{// we don't do this yet
				pThisObject->pHandler = new FMA_DevTables_handler(pThisObject);
				r = SUCCESS;
			}
			break;
		case obj_Block_Tbls:
			{// we don't do this yet
				pThisObject->pHandler = new FMA_BlkTables_handler(pThisObject);
				r = SUCCESS;
			}
			break;
		case obj_DD_Items:
			{// we don't do this yet
				pThisObject->pHandler = new FMA_Item_handler(pThisObject);
				r = SUCCESS;
			}
			break;
		case obj_Unknown:
		default:
			break;// leave r fail
		}
	}// else use the one it's got

	if ( pThisObject->pHandler != NULL )
	{
		r = pThisObject->pHandler->evaluateExtension();
	}
	return r;
};


