/**********************************************************************************************
 *
 * $Workfile: FMA_Support_tables.cpp $
 * 19mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_handler support classes for tables
 *		
 *
 */

#include "FMx_Support_primatives.h"
#include "FMA_Support_tables.h"
#include "FMA_Support_rows.h"
#include "FMA_defs.h"
#include "Endian.h"
#include "v_N_v+Debug.h"

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );//global in fma_reader.cpp


/* ==========================================================================================*/



/* ==========================================================================================*/
FMA_Table::FMA_Table()
{
//causes crash and is totally useless	erase(begin());
	pPayload = NULL;
	payldLength = 0;
	tableType = reserved_future_device_2;// unused/unknown
	rowCount = 0;
	rawByteLen = 0;
	lenLen = 0;
}



int FMA_Table::parse(UCHAR** pp_Location, INT& remainingSize)
{
	UINT64 tempLongLong;
	UCHAR *pLoc,     *estLoc;
	int    remaining, estRemaining;
	int length = 0;	// calculated
	int status;
	unsigned  preLength;

	assert(pp_Location != NULL && (*pp_Location) != NULL && remainingSize > 0);

	clear();// clear myself -vector
	pPayload  =              // for output later
	pLoc      = *pp_Location;// pLoc IS the location (the local pointer in caller)
	remaining = remainingSize;

	// table type is unsigned char
	unsigned char tt = *pLoc;
	tableType = (TableType_t)(tt);
	pLoc++;
	remaining--;

	// then comes encoded length of this table
	preLength = remaining;
	if ( (status = parseInteger(4, &pLoc, remaining, tempLongLong)) != SUCCESS)
	{
		return status;
	}
	rawByteLen   = (unsigned) tempLongLong;
	lenLen       = preLength - remaining;

	estRemaining = remaining - rawByteLen;// calc expected exit values
	estLoc       = pLoc      + rawByteLen;
	payldLength  = rawByteLen +     lenLen      + 1;
	//             spec'd len  length of Length   len of type

	length = rawByteLen;
	// then comes encoded row count
	if (status = parseInteger(4,  &pLoc, length, tempLongLong))
	{
		return status;
	}
	rowCount = (unsigned) tempLongLong;

	if ( FMA_Row::supported( tableType ) )
	{
		unsigned i;
		//for (unsigned i = 0; i < rowCount && remaining > 0; i++)
		for (i = 0; i < rowCount && length > 0; i++)
		{
			FMA_Row *row = newRow();
			if ( ! row)
			{	
				*pp_Location = estLoc;
				remainingSize= estRemaining;
				return FMx_MEMFAIL;
			}
			//if ((status = row->parse(this, &pLoc, remaining))  != SUCCESS )
			if ((status = row->parse(this, &pLoc, length))  != SUCCESS )
			{
				DEBUGLOG(CLOG_LOG,"    Parse Failure: %s @ 0x%06x  remain:0x%04x, row: %d\n",
											tableTypeStrings[tableType],pLoc, remaining, i);
				*pp_Location = estLoc;
				remainingSize= estRemaining;
				delete row;
				return status;
			}
			push_back(row);
		}
		if ( i < rowCount && length == 0)
		{			
			LOGIT(COUT_LOG,"ERROR: %s ran out of length before all rows were complete."
			"  Parsed %d of %d recorded rows.\n",tableTypeStrings[tableType],i,rowCount);
		}
		*pp_Location = pLoc;
		//remainingSize= remaining;		
		remainingSize= estRemaining;
	}
	else // we don't know these rows
	{
		*pp_Location = estLoc;
		remainingSize= estRemaining;
	}

	return SUCCESS;
}

void FMA_Table::release()
{
	vector<FMA_Row*>::iterator iT;
	FMA_Row* pRo;
	for (iT = begin(); iT != end(); ++iT)
	{
		pRo = *iT;
		delete pRo;
	}
	clear();
}

int FMA_Table::reconcileOut(unsigned offset)
{
	int n = reconcile ? 0 : rowCount;
	if (!reconcile)
		LOGIT(COUT_LOG,"%23s: (rows:0x%04x)\n", tableTypeStrings[tableType], n /*reconcileRowCount()*/);
	return 0;
}

int FMA_Table::out(unsigned offset)
	{
		if (SUPPRESS_PTOC  &&  tableType == item_to_cmd_table_type)
			return 0;

		// DEBUGLOG(COUT_LOG,"    Device Table Count: %4u (0x%04x)\n",devi
		if (!reconcile  &&  !maintenance)
		{
			if (SUPPRESS_PTOCSIZE==false && (tableType < critical_item_table_type /* Device Table */ ))
			{
				LOGIT(COUT_LOG," ------->%s %s Table<------(%d rows, %d parsed) %#04x payload size\n",
					tableTypeStrings[tableType],(tableType>= critical_item_table_type)?"Block":"Device",
					rowCount,size(),rawByteLen);	
			}
			outHex(pPayload, offset, payldLength );// hex dump of table
		}

	LOGIT(COUT_LOG,
		"- * - * - * -> %s Table Contents <- * - * - * - * - * - * - * - * - * - * - * -\n",
		tableTypeStrings[tableType]);
	vector<FMA_Row *>::iterator rowIT;
	FMA_Row * pRow;
	int y = 0;
	for (rowIT = begin(); rowIT != end(); ++rowIT, y++ )
	{
		pRow = *rowIT;
		pRow->out(y);
	}
	return 0;
}

// don't count resolved refs in reconcile mode
int FMA_ItemToCmdTable::reconcileRowCount(void)
{
	vector<FMA_Row *>::iterator rowIT;
	FMA_Row * pRow;
	int n = 0;
	for (rowIT = begin(); rowIT != end(); ++rowIT )
	{
		pRow = *rowIT;
		FMA_DevRow_Item2Cmd *p = (FMA_DevRow_Item2Cmd *)pRow;
		if (p->rr.type == RESOLVED_REF_ITEMID)
			n++;
	}
	return n;
}
int FMA_ItemToCmdTable::reconcileOut(unsigned offset)
{
	FMA_Table::reconcileOut(0);

	LOGIT(COUT_LOG,
		"\n\n- * - * - * -> Reconcile PTOC Contents <- * - * - * - * - * - * - * - * - * - * - * -\n");
	vector<FMA_Row *>::iterator rowIT;
	FMA_Row * pRow;
	int y = 0;
	for (rowIT = begin(); rowIT != end(); ++rowIT, y++ )
	{
		pRow = *rowIT;
		FMA_DevRow_Item2Cmd *p = (FMA_DevRow_Item2Cmd *)pRow;
		p->reconcileOut(y);
	}
	return 0;
}


// the row
FMA_Row::FMA_Row(TableType_t  t):FMx_Row<CValueVarient>(t)
{
	t = t;// breakpoint
};
FMA_Row::FMA_Row(const FMA_Row& s):FMx_Row<CValueVarient>(s.tt) 
{ 
	FMA_Row::operator=(s); 
};
// the row factory
FMA_Row* FMA_Table::newRow() // keep syncronized with FMA_Row::supported()
{
	FMA_Row* pRet = NULL;

	switch (tableType) // we are making a row for our table type
	{
	case string_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_String );
		break;
		
	case dictionary_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_Dictionary );
		break;

	case image_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_Image );
		break;

	case item_to_cmd_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_Item2Cmd );
		break;

	case symbol_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_Symbol );
		break;

	case item_info_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_ItemInfo );
		break;

	case block_info_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_BlockInfo );
		break;

	case command_number_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_DevRow_CommandNumber );
		break;

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	case critical_item_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_CriticalItem );
		break;
	case block_item_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_BlockItem );
		break;
	case block_item_name_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_BlockItemName );
		break;
	case parameter_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_Parameter );
		break;
	case relation_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_Relations );
		break;
	case update_table_type:
		pRet = dynamic_cast<FMA_Row*>( new FMA_BlkRow_Update );
		break;


	default:
		{// leave it a null pointer
		}
		break;
	}// endswitch
	return pRet;
}


/* ==========================================================================================*/
bool FMA_Row::supported(TableType_t tt)
{
	bool isReady = false;
	switch (tt) 
	{
	case string_table_type:
		isReady = true;
		break;
		
	case dictionary_table_type:
		isReady = true;
		break;

	case image_table_type:
		isReady = true;
		break;
	case item_to_cmd_table_type:
		isReady = true;
		break;

	case symbol_table_type:
		isReady = true;
		break;

	case item_info_table_type:
		isReady = true;
		break;

	case block_info_table_type:
		isReady = true;
		break;

	case command_number_table_type:
		isReady = true;
		break;

	// block tables
	case critical_item_table_type:
		isReady = true;
		break;
	case block_item_table_type:
		isReady = true;
		break;
	case block_item_name_table_type:
		isReady = true;
		break;
	case parameter_table_type:
		isReady = true;
		break;

	case parameter_member_table_type:
	case parameter_member_name_table_type:
	case parameter_element_table_type:
	case parameter_list_table_type:
	case parameter_list_member_table_type:
	case parameter_list_member_name_table_type:
	case characteristic_table_type:
	case characteristic_member_name_table_type:
											isReady = false;
		break;
	case relation_table_type:
		isReady = true;
		break;
	case update_table_type:
		isReady = true;
		break;
	default:
											isReady = false;
		break;
	}//endswitch
	return isReady;
}
//void FMA_Row::release()
//{
//	for (int i=0; i < size(); i++)
//	{
//		UCHAR *p = at(i);
//		if (p)
//			delete[] p;
//	}
//}

int FMA_Row::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	assert(pp_Location != NULL && (*pp_Location) != NULL && remainingSize > 0);
//temp	tt = table->tableType;
	return SUCCESS;
}

FMA_Row::~FMA_Row()
{
	vector<CValueVarient>::iterator vLIT;
	CValueVarient* pVar;
	for ( vLIT = begin(); vLIT != end(); ++vLIT )
	{
		pVar = &(*vLIT);
		pVar->clear();
	}
	clear();
}

/*
int FMA_Row::out(int rowNumber)
{	
	return FAILURE;//you shouldn't be here
}
*/

FMA_Row& FMA_Row::operator=(const FMA_Row& s)
{
	CValueVarient locVal;

	clear();
	tt = s.tt;
	
	//vector<CValueVarient>::iterator vLIT;
	FMx_Row<CValueVarient>::const_iterator vLIT;
	for ( vLIT = s.begin(); vLIT != s.end(); ++vLIT )
	{
		locVal = *vLIT;
		push_back(locVal);
		locVal.clear();
	}

	return *this;
}