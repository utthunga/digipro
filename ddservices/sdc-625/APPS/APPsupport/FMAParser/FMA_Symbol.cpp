/**********************************************************************************************
 *
 * $Workfile: FMA_Symbol.cpp $
 * 8mar13 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Symbol class implementation...data handling
 *		
 *
 */

#include "FMA_Symbol.h"
#include "FMA_Support_tables.h"
#include "logging.h"
#include "v_N_v+Debug.h"


	
FMA_Symbol::FMA_Symbol()  // constructor
{
}


FMA_Symbol::~FMA_Symbol()
{
	int breakHere = 1;
}


void FMA_Symbol::setTheTable( FMx_Table* pTbl)
{
	pTable = pTbl;
	if (pTbl == NULL)
	{
		LOGIT(CERR_LOG,L"FMA_Symbol::setTheTable passed a NULL table.\n");
		return;
	}
	FMA_Table* pT = dynamic_cast<FMA_Table*>( pTbl );

	// verify
#ifdef _DEBUG
	if (pT->tableType != symbol_table_type)
	{
		LOGIT(CERR_LOG,L"FMA_Symbol::setTheTable passed a non-symbol table.\n");
		return;
	}
#endif

	vector<FMA_Row *>    ::iterator rowIT;
	FMA_Row * pRow;

	vector<CValueVarient>::iterator vLIT;
	CValueVarient lV, mV, nV, oV, pV;
	string locNm, locStr;
	unsigned long strKey;
	struct symInfo  localInfo;

	for (rowIT = pT->begin(); rowIT != pT->end(); ++rowIT )
	{
		pRow = *rowIT;
#ifdef _DEBUG
		assert( pRow->tt == symbol_table_type);
#endif
		// push order: symID, symbolType, symSubType, idx2ItemInfoTbl, symName
		vLIT = pRow->begin();
		lV = *vLIT++;  // symbol-ID (the key)
		mV = *vLIT++;  // item-Type
		nV = *vLIT++;  // item-subType
		oV = *vLIT++;  // idx2InfoTbl
		pV = *vLIT;    // the actual string

		strKey             = (unsigned long)lV;
		localInfo.symType  = (int)mV;
		localInfo.symName  = (std::string)pV;

		theSymTable.insert(pair<unsigned, symInfo>(strKey,  localInfo));
		symbolId.push_back(strKey);

		if (! saveBinary)
		{// empty the original (DD) string
			(*vLIT).clear();// prevents having a bunch of string copies
		}
		vLIT++;
#ifdef _DEBUG
		if ( vLIT != pRow->end() )
		{// something is wrong
			LOGIT(CERR_LOG," End of symbol row is not at the end(). (%d)\n",pRow->size());
		}
#endif
		
	}// next row

}


void FMA_Symbol::retrieveStr(unsigned symNum, string& retVal, int& retType)
{
	NameByNumber_t::iterator symNameIT;

	symNameIT = theSymTable.find(symNum);

	if (symNameIT == theSymTable.end() )
	{
		retVal  = "****FMA_Symbol did not find Symbol by number.\n";
		retType = NOT_AN_ITEM_TYPE;
	}
	else
	{
		retVal  = symNameIT->second.symName;
		retType = symNameIT->second.symType;
	}
}

void FMA_Symbol::retrieveStrByIndex(unsigned index, string& retVal, int& retType)
{
	if (index == -1)
	{
		if (reconcile)
			retVal = "";
		else
			retVal = "<Unknown String>";
	} 
	else if (index+1 > symbolId.size())
	{
		LOGIT(CLOG_LOG,"Symbol trying to find index %d in a Symbol with %d elements.\n",
			index, (int)(symbolId.size()) );
	}
	else
	{
		retrieveStr(symbolId[index], retVal, retType);
	}
}