/**********************************************************************************************
 *
 * $Workfile: FMx_defs.h $
 * 01nov12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_defs.h : definitions used in both A and 8
 *
 * #include "FMx_defs.h"
 *
 */

#ifndef _FMA_FMx_DEFS_H
#define _FMA_FMx_DEFS_H

#define VERSION_MAJOR  1
#define VERSION_MINOR  1
#define VERSION_BETA   5
#define LANGCD_LEN	  10

#define FORMAT_BIG_ENDIAN		1
#define FORMAT_LITTLE_ENDIAN	2
#define THIS_ENDIAN				FORMAT_BIG_ENDIAN


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class is the base class for the basic file interface that all items need.
 *			It holds the common information from both FM8 & FMA formats.
 * FM8_Object, FMA_Object,						42, 82
 *  FMx_meta,FM8_meta,FMA_meta,					41,	81
 *  FMx_objectSet,FM8_ObjectSet,FMA_objectSet	43, 83
 **-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#define FMA_type    0x80
#define FM8_type	0x40
#define __Meta       0x01
#define _OneObj     0x02  /*_Object had a name conflict with the stl */
#define _ObjSet     0x03

/*******************************************************************************************
 * these appear to be exactly the same in 8 and A
 *******************************************************************************************/
typedef enum condType_e
{
	cdt_IF,		 // 0///
	cdt_SELECT,
	cdt_DIRECT,
	cdt_INVALID,
	cdt_THEN,	// if is TRUE
	cdt_ELSE,	// if is FALSE
	cdt_CASE,
	cdt_DEFAULT
}condType_t;


#define COND_TYPE_COUNT	8
//usage: char condType_Str[COND_TYPE_COUNT][FMACONDTYPMAXLEN] = {FMACONDTYPSTRINGS};
#define FMACONDTYPMAXLEN  17	/* maximum string length */
#define FMACONDTYPSTRINGS \
	{"If structure"},\
	{"Select structure"},\
	{"Direct"},\
	{"Invalid"},\
	{"Then"},\
	{"Else"},\
	{"Case"},\
	{"Default"}

extern char condType_Str[COND_TYPE_COUNT][FMACONDTYPMAXLEN];
/********************************************************************************************/

typedef enum exprElemType_e
{
	eet_Undefined,			//00
	eet_logical_not,		//01
	eet_negation,			//02
	eet_complement,			//03
	eet_addition,			//04
	eet_subtraction,		//05
	eet_multiplication,		//06
	eet_division,			//07
	eet_modulus,			//08
	eet_left_shift,			//09
	eet_right_shift,		//10
	eet_bitwise_and,		//11
	eet_bitwise_or,			//12
	eet_bitwise_xor,		//13
	eet_logical_and,		//14
	eet_logical_or,			//15
	eet_less_than,			//16
	eet_greater_than,		//17
	eet_less_or_equal,		//18
	eet_greater_or_equal,	//19
	eet_equal,				//20
	eet_not_equal,			//21
	eet_conditional,		//22		-- ()?: operator-� The Only Tertiary operation 
	eet_number_to_string,	//23		-- unary conversion to string
	eet_concatenation,		//24		-- of strings (the only string operator)
	eet_integer_constant,	//25] INTEGER,  -- signed integer 
	eet_unsigned_constant,	//26] INTEGER,  -- unsigned integer
	eet_floating_constant,	//27 6] Floating, 
	eet_string_constant,	//28 7] String, 
	eet_numeric_value,		//29 8] Reference,-- Must resolve to a numeric value
	eet_all_value,		//30 29] Reference	-- Must resolve to a Unicode String	
	eet_attr_value,			//31 0]                attr-name	byte; item-ref Reference
	eet_min_max_value,		//32 1] which	INTEGER; attr-name	byte; item-ref Reference
	eet_method_value,		//33 2] Reference	-- to a method
	eet_current_role,		//34 3
	eet_array_index, 		//35 4  -- only useable in value array or list (string function)
	eet_scaling_type,        //36 new:: value is is linear or logrithmic

	eet_unused1,			 //37 used to give space for expansion without changing the last
	eet_unused2,			 //38
	eet_unused3,			 //39

	// version 8 types, convertible to version ten types	
	// 22aug13 - never convert to v10...these should never be needed
	eet_variable_id,	//40 INTEGER -> eet_numeric_value & reference
	eet_maxVal,			//41 INTEGER whch, INTEGER id -> eet_min_max_value & which & MAX & ref
	eet_minVal,			//42 INTEGER whch, INTEGER id -> eet_min_max_value & which & MIN & ref
	eet_maxValRef,		//43 INTEGER whch, REF var    -> eet_min_max_value & which & MAX & ref
	eet_minValRef,		//44 INTEGER whch, REF var    -> eet_min_max_value & which & MIN & ref 
	eet_count_ref,		//45 Reference	-> eet_attr_value & COUNT & reference
	eet_capacity_ref,	//46 Reference	-> eet_attr_value & CAPACITY & reference
	eet_first_ref,		//47 Reference	-> eet_attr_value & FIRST & reference
	eet_last_ref,		//48 Reference	-> eet_attr_value & LAST & reference
	eet_dflt_val_ref,	//49 Reference	-> eet_attr_value & DFLT & reference
	eet_view_min_ref,	//50 Reference  -> eet_attr_value & MIN  & reference
	eet_view_max_ref,	//51 Reference  -> eet_attr_value & MAX  & reference
	eet_error			//52
}exprElemType_t;

#define EXPR_TYPE_COUNT	50
//usage: char exprType_Str[EXPR_TYPE_COUNT][FMAEXPRTYPMAXLEN] = {FMAEXPRTYPSTRINGS};
#define FMAEXPRTYPMAXLEN  22	/* maximum string length */
#define FMAEXPRTYPSTRINGS \
			{"UNDEFINED"},\
			{"NOT"},/*Logical Not*/\
			{"MINUS"},/*negation*/\
			{"COMP"},/*complement*/\
			{"ADD"},\
			{"SUB"},\
			{"MULT"},\
			{"DIV"},\
			{"MOD"},\
			{"<<"},\
			{">>"},\
			{"&"},\
			{"|"},\
			{"^"},\
			{"&&"},\
			{"||"},\
			{"<"},\
			{">"},\
			{"<="},\
			{">="},\
			{"=="},\
/* 21 */	{"!="},\
			{"()"},/*()?:*/\
			{"ntoa()"},/*number2string*/\
			{"concat"},/*str concat func*/\
			{"int-const"},\
			{"unsigned-const"},\
			{"float-const"},\
/* 28 */	{"string-const"},\
			{"valueRef"},\
			{"stringRef"},\
			{"attrRef"},\
			{"min/maxRef"},\
			{"methodRef"},\
			{"role"},\
/* 35 */	{"arrayIdx"},\
			{"eet_scaling_type"},\
			{"eet_unused1"},\
			{"eet_unused2"},\
			{"eet_unused3"},\
/* 40 */	{"8 Variable-ID"},\
			{"8 MaxValue"},\
			{"8 MinValue"},\
			{"8 MaxValue Ref"},\
			{"8 MinValue Ref"},\
			{"8 Count Attr Ref"},\
			{"8 Capacity Attr Ref"},\
			{"8 First Attr Ref"},\
			{"8 Last Attr Ref"},\
			{"8 ERROR unknown type"}

extern char exprType_Str[EXPR_TYPE_COUNT][FMAEXPRTYPMAXLEN];


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// version 10 ref types:

typedef enum refType_e
{
	//[ 0] 
	rT_item_id,					
	rT_variable_id,
	rT_menu_id, 
	rT_edit_display_id,
	rT_method_id,
	//[ 5]
	rT_refresh_id,					
	rT_unit_id,
	rT_write_as_one_id, 
	rT_reference_array_id,
	rT_collection_id,
	//[10]
	rT_block_b_id,					
	rT_block_a_id, 
	rT_record_id,
	rT_array_id, 
	rT_var_list_id,		
	//[15]
	rT_response_codes_id,			
	rT_file_id,
	rT_chart_id,
	rT_graph_id,
	rT_axis_id,
	//[20]
	rT_waveform_id,				
	rT_source_id,
	rT_list_id, 
	rT_grid_id, 
	rT_image_id,
	//[25]
	rT_blob_id,					
	rT_undefined26,
	rT_template_id, 
	rT_component_id, 
	rT_component_folder_id, 
	//[30]
	rT_component_reference_id,		
	rT_component_relation_id, 
	rT_interface_id,
	rT_rowbreak_ref,
	rT_separator_ref,
	//[35]
	rT_constant_ref,				
	rT_via_reference_array,
	rT_via_collection,
	rT_via_record,
	rT_via_value_array,
	//[40]
	rT_via_var_list,				
	rT_via_file,
	rT_via_chart,
	rT_via_graph,
	rT_via_source,
	//[45]
	rT_via_list,					
	rT_via_bit_enum,
	rT_via_attribute,
	rT_reserved_1,
	rT_via_Xblock_member,
	//[50] 
	rT_via_block_instance,			
	rT_param,
	rT_param_list,
	rT_block_char,
	rT_local_param,
	//[55] 
	rT_ref_with_arguments,
	rT_invalid
}refType_t;

#define REF_TYPE_COUNT	57

// we hit an issue with comparing the api contents 8 vs A [5/14/2014 timj]
// we had installed a "coalesce" feature in the A hydration that would take
// a list of DIRECTS and coalesce it into one DIRECT with a condition-less
// list.  We still could not match the 8-A output.  We will leave the coalesce
// code in place and use this define to switch it off or on.
#ifdef TOKENIZER
  #define COALESCEPAYLOAD false
#else  // all else
  #define COALESCEPAYLOAD true
#endif


#endif // _FMA_FMx_DEFS_H