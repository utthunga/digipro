/**********************************************************************************************
 *
 * $Workfile: FM8_meta.h $
 * 21Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_meta.h : FM8's basic OBJECT class description file
 *
 * #include "FM8_meta.h"
 *
 */

#ifndef _FMA_FM8_META_H
#define _FMA_FM8_META_H

#include "FM8_object.h"
#include "FMx_meta.h"

class FM8_ODES;
class FM8_Object;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class holds the meta objects for an FM8 file
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FM8_meta : public FMx_meta
{
public:	// construct/destruct                                     x41 
	FM8_meta(FMx* pTC, FILE* pF, bool sb) : FMx_meta (pTC, pF, sb, FM8_type|__Meta), handledObjects(0), pDefDesc(NULL), pFormat(NULL), pDevDir(NULL), pBlockDir(NULL) {}
	virtual ~FM8_meta();

private:
	unsigned handledObjects;

public:	// data
	FM8_ODES* pDefDesc;
	FM8_Object*	pFormat;  
	FM8_Object*	pDevDir;  
	FM8_Object*	pBlockDir;

	bool checkObjectType(UCHAR mask);	// true if object matches any of the bits in the mask
	bool isMetadataObject(void) { return checkObjectType(0x80);};

#ifdef _DEBUG
	unsigned    metaSize;
#endif

public: // workers
	void out(unsigned idx = 0xffffffff, unsigned size = 0);

public:	// required
	RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor);
	virtual RETURNCODE index_MetaData(unsigned& size);
	virtual unsigned get_item_count(void);	
	virtual unsigned get_first_obj_index(void);
	virtual unsigned get_block_count(void);
};

#endif // _FMA_FM8_META_H
