/**********************************************************************************************
 *
 * $Workfile: FMA_Attributes.h $
 * 20mar12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Attributes.h : These are the content handlers
 *
 * #include "FMA_Attributes.h"
 *
 */

#ifndef _FMA_ATTRIBUTES_H
#define _FMA_ATTRIBUTES_H

#ifdef INC_DEBUG
#pragma message("In FMA_Attributes.h") 
#endif

#include "FMx_Attribute.h"
#include "FMA_Attributes_Ref&Expr.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_Attributes.h") 
#endif

class FMA_AttributeList : public vector<FMx_Attribute*>
{	
public: // construct / destruct
	FMA_AttributeList() : attr_count(0) {};// nothing now
	virtual ~FMA_AttributeList(); // has to destroy all the attributes

public: // data
	UCHAR attr_count;// how many attributes are expected
	unsigned objectIndex;  // the file-based index number for identification

#ifdef _DEBUG
	int remainingLen;
	int remainingCnt;
#endif

public: // methods
	int evaluateAttributePayload(UCHAR* pData, int length, unsigned offset, unsigned iType, UCHAR count);
	int out(void); // payload data to stdout	

public: // static methods
	FMx_Attribute* createAttr(attr_t attr_type); // factory
};

typedef vector<FMx_Attribute*>::iterator fmaAttrIT;

#endif //_FMA_ATTRIBUTES_H