#ifndef _FMX_BASE_
#define _FMX_BASE_

#include "FMx.h"

class aCitemBase;

class FMx_Base
{
public:	// construct/destruct
	FMx_Base(FMx* pTC, FILE* pF, bool sb, unsigned ct) : pTopClass(pTC), fp(pF), saveBinary(sb), classType(ct), itemIndex(0), prototype(NULL) {};
	virtual ~FMx_Base() {};

public:	// data
	bool    saveBinary;
	FILE*   fp;// not ours...do not delete
	FMx*    pTopClass;

	unsigned classType;
	unsigned itemIndex; // like the FMA_Object's obj_Index but calc'd at read time
						// should be equal to FMA_Object's obj_Index

	aCitemBase* prototype;

public: // useful, near global, functions
	bool get_int(unsigned short* dest,void* src, int ret_fmt = THIS_ENDIAN);
	bool get_int(unsigned long* dest,void* src, int ret_fmt = THIS_ENDIAN);
	bool get_int(unsigned int* dest,void* src, int ret_fmt = THIS_ENDIAN) { return get_int((unsigned long*)(dest), src, ret_fmt); };

public: // required function(s)
	virtual RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor) = 0;	// everybody needs one
																	// usually { return (pVisitor->Operate( this )); };
};

/*********************************************************************************************
 * Object Types - AKA in the EFF as object-code
 ********************************************************************************************/
typedef enum obj_type_e
{
	obj_Unknown,	// unknown object code
	obj_Description,// 1)holds the DD descriptive items that don't appear in the header
	obj_Device_Tbls,// 2)holds the device tables, payload will each have a table type
	obj_Block_Tbls, // 3)holds the block tables, principly for FF
	obj_DD_Items,    // 4)holds an actual DD item.  There will be many of these
	obj_8_ODES     = 0x80, // specifically for this file section
	obj_8_Format,   //x81
	obj_8_Dev_Dir,	//x82 - Device Directory Object
	obj_8_Blk_Dir,	//x83 - Block Directory Object
	obj_8_DD_item 	//x84 - artificial, set by the app due to position
}obj_type_t;

//usage: char objectTypeStrings[10][OBJTYPEMAXLEN] = {OBJTYPESTRINGS};
#define OBJTYPEMAXLEN  14	/* maximum string length */
#define OBJTYPESTRINGS \
	{"Unknown"},\
	{"Description"},\
	{"Device Table"},\
	{"Block Table"},\
	{"DD Item"},\
	{"Description"},\
	{"Format"},\
	{"Device Table"},\
	{"Block Table"},\
	{"DD Item"}


extern char objectTypeStrings[10][OBJTYPEMAXLEN];


/*=========================================================================================== /
 *    MAPPING FUNCTIONS
 *===========================================================================================*/

typedef enum mappingTypeAto8_e
{
	map_Unknown,	/* 0 */
	map_AttrType,			/* A-ItemType, A-AttributType */
    map_Str_Type,			/* map A to 8 string types    */
}mappingTypeAto8_t;

//typedef enum mappingType8toDDL_e
//{
//	map_Unknown,	/* 0 */
//	map_elemType,			// 8-element type, DDL-element Type 
//}mappingTypeAtoDDL_t;

typedef struct {int itype; int fm8; int fma;} IDMAP;
typedef struct {int fm8; int ddl;} ELEMENT_TYPE_MAP;

extern int conversionAto8(mappingTypeAto8_t mt, int param1, int param2);
//extern int conversionAtoDDL(mappingTypeAtoDDL_t mt, int param1, int param2);


#endif //_FMX_BASE_
