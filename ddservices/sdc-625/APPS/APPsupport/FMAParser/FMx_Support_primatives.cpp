/**********************************************************************************************
 *
 * $Workfile: FMA_Support_primatives.cpp $
 * 19mar12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_handler support classe's support functions
 */

#include "FMx.h"
#include "FMx_Support_primatives.h"

int parseString(FILE* fp, unsigned len, UCHAR **value)
{
	UCHAR c;
	unsigned i = 0;
	UCHAR *buf = new UCHAR[len+1];
	do {
		if (fread(&c, sizeof(UCHAR), 1, fp) != 1) 
		{
			return FMx_READFAIL;
		}
		buf[i++] = c;
	} while (c && i < len);
	buf[i++] = 0;
	*value = buf;

	return SUCCESS;
	
}

/* parseInteger - parse an encoded integer from the chunk of memory
 *		increase the location pointer to just after the parse
 *      decrease the remaining size count by the bytes used
 *		verify that the value in the UINT64 is small enough to fit the Return size(bytes)
 *    return SUCCESS if all is well
 *           FMA_BAD_DECODE  if the value decoded OK but the value was bigger than ReturnSize
 *           FMx_OPPFAIL     if the decode is bad - we have lost our way...
 ****/
RETURNCODE parseInteger(int ReturnSize, unsigned char** pp_Location, int& remainingSize, UINT64& value)
{
	UINT		cnt=0;					/* temp ptr to size of binary */
	UINT64      val=0;					/* temp storage for parsed integer */
	UCHAR		c=0;					/* temp ptr to binary */
	UCHAR      *ptr=NULL;				/* local temp pouinter to a uchar in the stream*/
	int			another_byte_please=0;	/* need to parse another byte */
	
	// Read each character, building the long long until the high order bit is not set
	assert( pp_Location!=NULL  );
	assert((*pp_Location)!=NULL);
	//assert( remainingSize > 0 );

	if (remainingSize < 1)
	{
		remainingSize = 0;
	}

	val = 0;
	cnt = remainingSize;
	ptr = *pp_Location;
	
	int i = 0;
	do {
		if (cnt == 0 || ReturnSize == 0 || ReturnSize > 8) 
		{
			return FMx_OPPFAIL;
		}		
		
		c = *ptr++;
		another_byte_please = c & 0x80;
		c &= 0x7f;
		
		if (val > ( _UI64_MAX >> 7)) 
		{
			return FMx_OPPFAIL;
		}
		
		val <<= 7;
		if (val >   _UI64_MAX - c) 
		{
			return FMx_OPPFAIL;
		}
		
		val |= c;
		--cnt;
	} while (another_byte_please);

	UINT64 testMask = 0;
	for (int y = 0; y < ReturnSize; y++)
	{
		testMask <<= 8;
		testMask |= 0xff;
	}
	if ( val <= testMask )
	{		
		value         = val;		
		remainingSize = cnt;
		*pp_Location   = ptr;
		return SUCCESS;
	}
	else
	{// log error?
		return FMx_BAD_DECODE;
	}
}/* parseInteger */

/** we need to handle the offset so the caller doesn't have to duplicate code ***/
RETURNCODE parseInteger(int RetSize, unsigned char** pp_Loc, int& Size, UINT64& value, unsigned& offset)
{
	int entrySize = Size;
	RETURNCODE r = parseInteger(RetSize, pp_Loc, Size, value);
	if ( r != SUCCESS )
		return r;
	offset += (entrySize - Size);
	return r;
}

// ReturnLen is zero (with no error) on an implicit tag
RETURNCODE parseTag(unsigned char** pp_Location, int& remainingSize, unsigned& ReturnTag,   unsigned& ReturnLen)
{
	RETURNCODE status = SUCCESS;
	UCHAR tagStart = 0;
	UINT64 tempLongLong;
	UCHAR *pLoc;
	int    remaining;

	ReturnLen = 0; // will ruturn zero or set to a explicit value
	ReturnTag = 0; // an error tag

	//assert(pp_Location != NULL && (*pp_Location) != NULL && remainingSize > 0);
	pLoc      = *pp_Location;
	remaining = remainingSize;

	tagStart  = *pLoc++; remaining--;
	if ( tagStart & 0x80 ) // != 0 then explicit
	{
		tagStart &= 0x7f;// clr hi bit
		if(tagStart == 127)// long tag
		{// get the long value
			if ( (status = parseInteger(4, &pLoc, remaining, tempLongLong)) != SUCCESS)
			{
				return status;
			}
			ReturnTag = (unsigned)tempLongLong;
		}
		else // short tag
		{
			ReturnTag = tagStart;
		}
		// get the explicit length
		if ( (status = parseInteger(4, &pLoc, remaining, tempLongLong)) != SUCCESS)
		{
			return status;
		}
		ReturnLen = (unsigned)tempLongLong;
	}
	else//implicit (hi bit clear)
	if ( tagStart == 127 ) // long tag
	{// get the long value
		if ( (status = parseInteger(4, &pLoc, remaining, tempLongLong)) != SUCCESS)
		{
			return status;
		}
		ReturnTag = (unsigned)tempLongLong;
	}
	else // implicit, short tag
	{
		ReturnTag = tagStart;
	}
	// if we get here we have success
	*pp_Location = pLoc;
	remainingSize = remaining;
	return SUCCESS;
}

RETURNCODE		// ReturnLen is zero (with no error) on an implicit tag
parseTag(unsigned char** pp_Loc, int& Size,unsigned& RetTag, unsigned& RetLen, unsigned& offset)
{
	unsigned entry = Size;
	RETURNCODE r = parseTag(pp_Loc, Size, RetTag, RetLen);	
	if ( r != SUCCESS )
		return r;
	offset += (entry - Size);
	return r;
}

static bool obfuscate = true;			// default to true lest we break strings when we jettison FM8
static UCHAR DD_BYTESTR_XOR = 0xFF;		// XOR with this value

void obfuscateByteString(bool onoff)
{
	obfuscate = onoff;					// true if bytestrings are obfuscated
}

extern RETURNCODE /* ALLOCATES MEMORY to *pp_value, copies from pp_Loc to value for length */ 
parseByteString( unsigned char** pp_Loc, int& remainingSize, unsigned length, unsigned & offset, UCHAR** pp_value )
{
	UCHAR* pV;
	assert(pp_Loc != NULL && (*pp_Loc) != NULL && remainingSize > 0 && 
		   length > 0     && pp_value != NULL  );
	if ( ((int)length) > remainingSize)
	{
		return FMx_BADPARAM;
	}
	pV = new UCHAR[length];
	if (pV == NULL )
	{
		return FMx_MEMFAIL;
	}
	memcpy(pV, (*pp_Loc), length);

	// de-obfuscate the bytestring. FMA only.
	if (obfuscate)
	{
		for (unsigned i=0; i<length; i++)
		{
			pV[i] = pV[i] ^ DD_BYTESTR_XOR;
		}
	}

	(*pp_Loc)     += length;
	remainingSize -= length;
	offset		  += length;
	*pp_value      = pV;

	return SUCCESS;
}


RETURNCODE
parseAsciiString(unsigned char** pp_Location, int& remainingSize, unsigned & offset, char** value)
{
	unsigned tag, length;  // explicit tag
	int status = SUCCESS;

	assert(pp_Location != NULL &&(*pp_Location) != NULL && remainingSize > 0 && value != NULL);

	status = parseTag(pp_Location, remainingSize, tag, length );
	if ( length == 0 )
	if (status != SUCCESS) 
	{
		return status;
	}
	if ( tag != 61 )// tag for an ascii string
	{
		status = FMx_BAD_PARSE;
	}
	assert(length != 0);

	status = parseByteString(pp_Location, remainingSize, length, offset, (UCHAR **)value);

	return status;
}

RETURNCODE
parseWideString(unsigned char** pp_Loc, int& remainingSize, unsigned length, unsigned & offset, wstring& ws_value)
{
	RETURNCODE status = SUCCESS;
	UCHAR  *text   = NULL;
	UCHAR *pLoc;
	int    remaining;

	assert(pp_Loc != NULL && (*pp_Loc) != NULL && remainingSize > 0 && length > 0);
	pLoc      = *pp_Loc;
	remaining = remainingSize;

	status = parseByteString(&pLoc, remaining, length, offset, &text);
	if (status != SUCCESS)
	{
		return status;
	} 
	
	ws_value = UTF82Unicode( (char*)text);
	delete[] text; // allocated in parse bytestring
	*pp_Loc       = pLoc;
	remainingSize = remaining;

	return SUCCESS;
}


// parse an unsigned 32
int parse_unsigned32(unsigned char** pp_Location, int& remainingSize, unsigned& value)
{
	UCHAR *pL = (*pp_Location);
	if (remainingSize < 4)
		return FAILURE;
	value = 0;

	value |= (*pL++);   remainingSize--;
	value <<= 8;
	value |= (*pL++);   remainingSize--;
	value <<= 8;
	value |= (*pL++);   remainingSize--;
	value <<= 8;
	value |= (*pL++);   remainingSize--;

	*pp_Location = pL;
	return SUCCESS;
}

int parseInteger(int size, FILE* fp, UINT64& value)
{
	//ADDED By Deepak Initialize all vars
	UINT		cnt=0;	/* temp ptr to size of binary */
	UINT64      val=0;	/* temp storage for parsed integer */
	UCHAR		c=0;		/* temp ptr to binary */
	int			another_byte_please=0;	/* need to parse another byte */
	
	// Read each character, building the ulong until the high order bit is not set
	
	val = 0;
	cnt = size;
	
	int i = 0;
	do {
		if (cnt == 0) 
		{
			return FMx_READFAIL;
		}
		
		if (fread(&c, sizeof(UCHAR), 1, fp) != 1)
		{
			return FMx_READFAIL;
		}

		another_byte_please = c & 0x80;
		c &= 0x7f;
		
		//if (val > ( _UI64_MAX >> 7)) 
		//{
		//	return DDL_LARGE_VALUE;
		//}
		
		val <<= 7;

		//if (val >   _UI64_MAX - c) 
		//{
		//	return DDL_LARGE_VALUE;
		//}
		
		val |= c;
		--cnt;
	} while (another_byte_please);
		
	value = val;
	
	return SUCCESS;
	
}/* parseInteger */

int parseInteger(FILE* fp, unsigned& value)
{
	int status;
	UINT64 I;
	if (status = parseInteger(5, fp, I)) return status;
	
	value = (unsigned) I;
	return SUCCESS;
}

int parseInteger(FILE* fp, UCHAR& value)
{
	int status;
	UINT64 I;
	if (status = parseInteger(1, fp, I)) return status;
	
	value = (UCHAR) I;
	return SUCCESS;
}

int parseAsciiString(FILE* fp, char** value)
{
	unsigned tag;  // explicit tag
	int status = SUCCESS;

	if (status = parseInteger(fp, tag)) return status;
	unsigned len = tag - 61; // includes terminating null

	return parseString(fp, len, (UCHAR **)value);
}

//length always = 8
int parseDouble(unsigned char** pp_Loc, int& SizeLeft, double& df_value, unsigned& offset)
{// switch endian, make double
	
	assert(pp_Loc != NULL && (*pp_Loc) != NULL && SizeLeft > 7 );

	unsigned char buff[8];
	int i;
	for ( i = 7; i >= 0 ; i--)
	{
		buff[i] = *(*pp_Loc);
		(*pp_Loc)++;
		SizeLeft--; offset++;
	}
	df_value = *((double*) buff);
	return SUCCESS;
}

//length always = 4
int parseFLOAT(unsigned char** pp_Loc, int& SizeLeft, double& df_value, unsigned& offset)
{// switch endian, make float
	
	assert(pp_Loc != NULL && (*pp_Loc) != NULL && SizeLeft > 3 );

	unsigned char buff[4];
	int i;
	for ( i = 3; i >= 0 ; i--)
	{
		buff[i] = *(*pp_Loc);
		(*pp_Loc)++;
		SizeLeft--; offset++;
	}
	df_value = *((float*) buff);
	return SUCCESS;
}

int parseBitString(UCHAR** pData, int& size, UINT64 &value, unsigned& offset)
{
	RETURNCODE  ret = 0;

	unsigned char	*chunk=NULL;		/* temp ptr to the binary chunk */
	unsigned char	**chunkp=pData;
	UINT64   read_mask=0;		/* mask used to read bits from the binary */
	UINT64   write_mask=0;		/* mask used to store the bits  */
	unsigned long   count=0;			/* local value of binary size */
	int             more_indicator=0;	/* indicators more bits to be read */
	int             last_unused=0;		/* # of bits to ignore in last octet */
	int             bitlimit=0;			/* loop counter */
	unsigned char   c=0;
	UINT64	tempval;


	/*
	* Read the first character
	*/
	
	tempval = 0;

	chunk = *chunkp;
	count = size;
	count--;
	c = *chunk++;
	
	/*
	* Special case the situation where there is no bitstring (that is, the
	* bit string is zero).
	*/
	
	if (c == 0x80) {
		offset = offset + size - count;
		size = count;
		*chunkp = chunk;
		*pData = *chunkp;
		value = tempval;
		return 0;
	}
	
	/*
	* Pull out the number of bits to ignore in the last octet, and the
	* indicator of whether there are more octets.
	*/
	
	more_indicator = c & 0x10;
	last_unused = (c & 0xE0) >> 5;
	c &= 0xF;
	
	/*
	* Build up the return value.  Note that in each octet, the highest
	* order bit corresponds to the lowest order bit in the returned bit
	* mask.  That is, in the first octet, bit 3 corresponds to bit 0, bit
	* 2 to bit 1, etc.  In the next octet, bit 6 corresponds to bit 4, bit
	* 5 to bit 5, bit 4 to bit 6, ...  In the next octet, bit 6
	* corresponds to bit 11, bit 5 to bit 12, ...
	*/
	
	write_mask = 1;
	read_mask = 0x8;
	bitlimit = 4;
	
	while (more_indicator) 
	{
		if (!count) 
		{
			return -1;
		}
		
		for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {
			
			if (c & read_mask) 
			{	
				tempval |= write_mask;
			}
		}
		
		/*
		* Read the next octet.
		*/
		
		count--;
		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7F;
		read_mask = 0x40;
		bitlimit = 7;
	}
	
	/*
	* In the last octet, some of the bits may be ignored.  The number of
	* bits to ignore was specified in the very first octet, and remembered
	* in "last_unused".
	*/
	
	bitlimit -= last_unused;
	
	if (bitlimit <= 0) 
	{	
		return -2;
	}
	
	for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {
		
		if (!write_mask) 
		{	
			return -3;
		}
		if (c & read_mask) 
		{	
			tempval |= write_mask;
		}
	}
	
	offset = offset + size - count;
	size = count;
	*chunkp = chunk;
	*pData = *chunkp;
	value = tempval;
	return ret;
}

#define firstUnprintable	0x00b0
#define unknownChar         0x3f /* ? */

string filter2print(const wstring& src)
{
	string dest;
	if( src.size() )
	{
		char   *szdest = new  char[src.size()+2];
		unsigned val, loc = 0;

		memset(szdest,0,(src.size()+2) * sizeof(char)   );// fixed 12aug10

		wstring::const_iterator wChar;

		for (wChar = src.begin(); wChar != src.end(); ++wChar)
		{
			val = (unsigned)*wChar;
			if ( val >= firstUnprintable)
			{
				szdest[loc++] = (unsigned char)unknownChar;
			}
			else
			{
				szdest[loc++] = (unsigned char)val;
			}
		}
		dest = szdest;
		delete [] szdest;
	}
	return dest;
}

// return true if x is contained in the list of integers
bool _intIsInList(int x, int list[], int n)
{
	for (int i=0; i < n; i++)
	{
		if (list[i] == x)
			return true;
	}
	return false;
}


int _intLookupList(int x, int list1[], int list2[], int n1, int n2)
{
	assert(n1 == n2  && n1 > 0);
	for (int i=0; i < n1; i++)
	{
		if (list1[i] == x)
			return list2[i];
	}
	assert(false);	// cant be here!
	return -1;
}