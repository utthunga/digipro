#ifndef _FM8_
#define _FM8_

#include "FMx.h"

class FM8 : public FMx
{
public:
	FM8();
	virtual ~FM8();

public:
	virtual RETURNCODE indexFile(void); // scans the file building pointer indexes, rewinds when done
	virtual RETURNCODE readIn_File(void);// fill payloads from index
	virtual RETURNCODE process_File(void);
	
	virtual void release(void);// loose the payload memory
	virtual void out(void);// dump all the information to a file
	virtual void generate(void);
};

#endif //_FM8_
