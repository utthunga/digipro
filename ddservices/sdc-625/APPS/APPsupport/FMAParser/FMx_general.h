/**********************************************************************************************
 *
 * $Workfile: FMx_general.h $
 * 20Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_general.h : holds common definitions for the parser, including error codes
 *
 * #include "FMx_general.h"
 *
 */

#ifndef _FMx_GENERAL_H
#define _FMx_GENERAL_H

#ifdef INC_DEBUG
#pragma message("FMx_general.h") 
#endif

#include <assert.h>
#include <vector>
#include <algorithm>

#include "endian.h"

using namespace std;
#pragma warning (disable : 4786) 


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMx_general.h") 
#endif

#define SAVE_BINARY  1  /* enables the recordation of the raw Encoded file */
						/* undef this value to skip the binary stuff      */

/*********************************************************************************************
 * return codes
 ********************************************************************************************/
#define ERROR_STR_BUFFER_LEN  80

#define RETURNCODE int
#ifndef SUCCESS
#define SUCCESS   0
#define FAILURE  -1
#endif

#define HDRSTARTLEN 8
#define	FMx_HEADER_SIZE 41

#define FMx_CODE_START	7500

#define FMx_BADPARAM	FMx_CODE_START+ 1
#define FMx_STR001		{FMx_BADPARAM, L"Invalid parameter passed to function."}
#define FMx_FILEFAIL    FMx_CODE_START+ 2
#define FMx_STR002		{FMx_FILEFAIL, L"File failed to open."}
#define FMx_READFAIL    FMx_CODE_START+ 3
#define FMx_STR003		{FMx_READFAIL, L"File Read failed."}
#define FMx_REPOSFAIL   FMx_CODE_START+ 4
#define FMx_STR004		{FMx_REPOSFAIL, L"File Reposition failed."}
#define FMx_MEMFAIL     FMx_CODE_START+ 5
#define FMx_STR005		{FMx_MEMFAIL, L"Memory acquisition failed."}
#define FMx_UNSUPPORTED FMx_CODE_START+ 6
#define FMx_STR006		{FMx_UNSUPPORTED, L"Unsupported %s."}
#define FMx_OPPFAIL     FMx_CODE_START+ 7
#define FMx_STR007		{FMx_OPPFAIL, L"Operation failed."}
#define FMx_BAD_DECODE  FMx_CODE_START+ 8
#define FMx_STR008		{FMx_BAD_DECODE, L"Value decoded is invalid."/* decode OK, value bad */
#define FMx_BAD_PARSE   FMx_CODE_START+ 9
#define FMx_STR009		{FMx_BAD_PARSE, L"Parsing is invalid.");/* decode wrong(eg wrong tag)*/
#define FMx_IS_ITMINFO  FMx_CODE_START+10
#define FMx_STR010		{FMx_IS_ITMINFO, L"Not an error:Removed a Item Information attribute");
					/************************/

struct  error_string{ int codeNumber;wchar_t* pStr; };
#define FMx_ERROR_STRLEN 38
#define FMx_ERROR_CODES  FMx_STR001,FMx_STR002,FMx_STR003,FMx_STR004,FMx_STR005,FMx_STR006

/********************************************************************************************/

#define DEFAULT_LANGUAGE_CODE  L"|en|"
#define SYM_EXTN_LEN  4

/********************************************************************************************/

typedef unsigned char		UCHAR;
//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
typedef unsigned long	UINT64;
#else
typedef unsigned long long	UINT64;
#endif

#define ATTR_TYPE_COUNT	142


#endif //_FMx_GENERAL_H
