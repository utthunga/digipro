/**********************************************************************************************
 *
 * $Workfile: v_N_v+Debug.h $
 * 19jan15 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2015 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		extract the common information used to suppress or change output for debugging or v&v
 *
 * #include "v_N_v+Debug.h"
 *
 */

#ifndef _V_AND_V_PLUS_DEBUG_H
#define _V_AND_V_PLUS_DEBUG_H


#ifdef INC_DEBUG
#pragma message("    No Includes in v_N_v+Debug.h") 
#endif

#ifdef INC_DEBUG
#pragma message("    Finished       v_N_v+Debug.h") 
#endif




extern bool compress;
extern bool reconcile;
// delete extern bool printIndices;
extern bool maintenance;

extern  unsigned long long  suppress; // stevev 14jan15
#define SUPPRESS_WEIGHT		((suppress & 0x0001) != 0)
#define SUPPRESS_EXTRAIDX	((suppress & 0x0002) != 0)
#define SUPPRESS_IDXCNT		((suppress & 0x0004) != 0)
#define SUPPRESS_HEXDUMP	((suppress & 0x0008) != 0)

#define SUPPRESS_TIMEOUTS	((suppress & 0x0010) != 0)
#define SUPPRESS_SIGNATURE	((suppress & 0x0020) != 0)
#define SUPPRESS_PTOCSIZE	((suppress & 0x0040) != 0)
#define SUPPRESS_STRINGINDEX ((suppress &0x0080) != 0)

#define SUPPRESS_HEADER		((suppress & 0x0100) != 0)
#define SUPPRESS_INTEGER    ((suppress & 0x0200) != 0)	/* make into a short -> currently only transaction number 06jan16*/
#define SUPPRESS_PTOC		((suppress & 0x0400) != 0)
#define SUPPRESS_CLASSERROR	((suppress & 0x0800) != 0)

#define SUPPRESS_PTOCNAME	((suppress & 0x1000) != 0) /* A vs 8 ptoc ; 8 doesn't have this */
#define SUPPRESS_CIII		((suppress & 0x2000) != 0) /* A vs 8 ptoc ; 8 doesn't have this */
#define CONVERT_DFLT2BLANK  ((suppress & 0x4000) != 0) /* 'No Label Available'  to ""       */
#define SUPRESS_RR_PTOC     ((suppress & 0x8000) != 0) /* if it's empty - supress it        */


#define SUPPRESS_TABLES		((suppress & 0x10000) != 0)

#endif //_V_AND_V_PLUS_DEBUG_H