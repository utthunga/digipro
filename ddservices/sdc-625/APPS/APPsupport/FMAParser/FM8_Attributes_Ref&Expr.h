/**********************************************************************************************
 *
 * $Workfile: FMA_Attributes_Ref&Expr.h $
 * 23mar12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Attributes_Ref&Expr.h : These are the content handlers
 *
 * #include "FM8_Attributes_Ref&Expr.h"
 *
 */

#ifndef _FM8_ATTR_REF_N_EXPR_H
#define _FM8_ATTR_REF_N_EXPR_H

#include "FMx_Attributes_Ref&Expr.h"
#include "Convert.h"

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class expressionElement8 : public expressionElement
{
public: // construct
	expressionElement8() {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	int evalSpecial(UCHAR** pData, int& length, unsigned& offset);
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCExpression8 : public abCExpression
{
public: // construct
	abCExpression8() {};
	abCExpression8(UINT64 const_value)
	{
		expressionElement8* pExprElem = new expressionElement8;
		pExprElem->eeInt = const_value;
		pExprElem->exprElemType = ConvertToDDL::ExpressionElementType(eet_integer_constant);

		exprList.push_back(pExprElem);
	};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCReference8 : public abCReference
{
public: // construct
	abCReference8(int tag = 0, bool direct = false) : abCReference(tag) 
	{
		requiredDirect = direct;
	};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual aCattrBase* generatePrototype();
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#endif //_FMA_ATTR_REF_N_EXPR_H