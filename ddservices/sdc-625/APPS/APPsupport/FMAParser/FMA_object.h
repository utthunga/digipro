/**********************************************************************************************
 *
 * $Workfile: FMA_object.h $
 * 21Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA.h : FMA's basic OBJECT class description file
 *
 * #include "FMA_object.h"
 *
 */

#ifndef _FMA_FMA_OBJECT_H
#define _FMA_FMA_OBJECT_H

#include "FMx_general.h"
#include "FMA_defs.h"
#include "FMx_Base.h"
#include "FMA_handlers.h"

#include <stdio.h>
#include <vector>

using namespace std;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the basic object that forms the fma format.  It reads the fixed portion
 *		and returns with the payload.  It doe NO payload processing.
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class FMA_Object : public FMx_Base
{
public:	// construct / destruct	
	FMA_Object(FMx* pTC, FILE* pf, bool saveBinary = false);  // constructor
	virtual ~FMA_Object();

public:	// data
	UCHAR*      obj_Fixed;			// raw binary for fixed portion
	//          OBJ_FIXED_SIZE		// size of fixed

	unsigned	object_offset;		 // from start of file

	/** contents of the Fixed portion of the object **/
	unsigned	obj_Index;
	obj_type_t  obj_type;  // originally a UCHAR, 

	// EXTENSION
				/* raw payload offset from start of file */
	unsigned    objA_payload_offset() { return  (object_offset + OBJ_FIXED_SIZE);}; 
	unsigned    obj_Length; // also extension length
	UCHAR*      obj_Payload;// the raw extension chunk (after extension-byte-count)
	unsigned    obj_size(void) { return(obj_Length + OBJ_FIXED_SIZE); };
	
	// the changable handler class (contains specificity methods for this Object type)
	FMx_handler*    pHandler;

public:	// workers
	RETURNCODE read_Object_Info(bool skipExtensionRead = true);//at current loc,read info only
	RETURNCODE read_ObjectPayload();// at recorded offset (called by visitor for now)

public:	// required
	RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor)
	{ 
		return (pVisitor->Operate(this)); 
	};
};// end class FMA_Object


typedef vector<FMA_Object*>          FMA_pObjectList_t;
typedef FMA_pObjectList_t::iterator  FMA_pObjectListIT_t;


#endif //_FMA_FMA_OBJECT_H
