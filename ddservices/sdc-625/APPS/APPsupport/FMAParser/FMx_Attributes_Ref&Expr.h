#ifndef _FMX_ATTR_REF_N_EXPR_H
#define _FMX_ATTR_REF_N_EXPR_H

#include "FMx_Attribute.h"
#include "FMx_defs.h"

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


// forward reference
class abCReference;
class faCString;
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
#define ELEM_WIDTH  15

class expressionElement : public FMx_Attribute
{
public: // construct
	expressionElement() : eepString(NULL), eepRef(NULL) { clear(); };
	expressionElement(const expressionElement& src) { operator=(src); };
	expressionElement& operator=(const expressionElement& s);
	virtual ~expressionElement() { destroy(); };

public:	//data
	expressElemType_t   rawElemType;	// v8 or v10 type
	expressElemType_t   exprElemType;	// version 8 types.  not:  v10 with extra range(40-52) for v8 types
    UINT64	eeInt;			// holds WHICH for min-max value
	bool				eeIsUnsigned;
	double				eeFloat;
	faCString*			eepString;
	abCReference*		eepRef;

public: // methods
	virtual void clear() { destroy(); FMx_Attribute::clear();rawElemType=eeT_Unknown;eeInt = 0;
			exprElemType = eeT_Unknown; eeIsUnsigned = 0;eeFloat = 0.0;
	};
	virtual void hydratePrototype(aCexpressElement* elem);
	virtual void destroy(void);
	virtual int out(void);
};

typedef vector<expressionElement*>	eepLst_t;
typedef eepLst_t::iterator			eepIt_t;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCExpression : public FMx_Attribute
{
public: // construct
	abCExpression() {};
	abCExpression(const abCExpression& s){ operator=(s); };
	abCExpression& operator=(const abCExpression& src);
	virtual ~abCExpression() { destroy(); };

public: // data
	eepLst_t  exprList;

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) { return 0; }
	virtual void hydratePrototype(aCexpression* exp);
	virtual void destroy(void);
	virtual int out(void);
	virtual void clear() 
	{	destroy(); exprList.clear(); };
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCReference : public FMx_Attribute
{
public: // construct
	abCReference(int tag = 0) : FMx_Attribute(tag), rawRefType(0), refType(rT_NotA_RefType), // was -1), 
								intValue(0), pExpr(NULL), pRef(NULL) {attname = "Reference";};
	abCReference(const abCReference& s){ operator=(s); };
	abCReference& operator=(const abCReference& src);
	virtual ~abCReference() { destroy(); };

public: // data
	int						rawRefType;	// what was parsed out of the DD, used for debugging
	int						refType;	//				| FM8 DD			FMA	DD
										//	rawRefType	| 8 type			A type
										//  refType		| 8 type			8 type

    UINT64		intValue;// holds the int value of attribute Type on attribute reference
	abCExpression*			pExpr;
	abCReference*			pRef;
	vector<abCReference*>	argumentList;// this better not be used in HART

public: // methods
	virtual void hydratePrototype(aCreference* ref);
	virtual void destroy(void);
	virtual int out(void);

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCreference* r = new aCreference;
		hydratePrototype(r);

		return r;
	}

	virtual aCattrBase* generatePrototype() 
	{ 
		aCattrCondReference *ref = new aCattrCondReference();
		ref->condRef.priExprType = eT_Direct;

		aCgenericConditional::aCexpressDest tempaCDest;
		tempaCDest.destType = eT_Direct;
		aCreference *r = new aCreference();
		hydratePrototype(r);
		tempaCDest.pPayload = r;
		ref->condRef.destElements.clear();
		ref->condRef.destElements.push_back(tempaCDest);

		ref->attr_mask = maskFromInt(attrType);
		return ref;
	};

	char *refTypeName(int id);				// based on fm8 reftype
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#endif //_FMX_ATTR_REF_N_EXPR_H
