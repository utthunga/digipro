/*************************************************************************************************
 *
 * filename  faCStringA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A String Attribute
 *
 */

#include "logging.h"
#include "faCStringA.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"
#include "TAGS_SA.H"

int faCStringA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;
	UINT64		tempLongLong;
	unsigned	tagValue;
	unsigned	tagLength;
	unsigned	strLength = 0;

	// explicit tag 63// ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length, tagValue, tagLength, offset );//verify string,get length

	if (tagValue != STRUCTURE_TAG_STRING)
	{
		LOGIT(CERR_LOG,"Error: String Type has incorrect tag. (%u)\n",tagValue);
		return -1;
	}

	if (tagLength > (unsigned)length)
	{
		LOGIT(CERR_LOG,"Error: String with tag's length bigger than available.\n)");
		return -2;
	}

	strLength = tagLength;	// strLength is ready to go (may be the same as length)

	ret = parseTag(pData, length, tagValue, tagLength, offset);// verify string, get length

	if (ret != SUCCESS || tagLength != 0 || tagValue >= STR_TYPE_COUNT)// implicit tag error
	{
		LOGIT(CERR_LOG,"Error: String implicit tag has an error.\n)");
		return -3;
	}

	stringRefType_t AstrRefType = (stringRefType_t)tagValue;
	strRefType = (ddlstringType_t)conversionAto8(map_Str_Type, 0, AstrRefType);

	switch (AstrRefType)
	{
		case st_Literal:		//0
		case st_Dictionary:     //1 
		{// int			
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Dictionary table key extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;

			break;
		}

		case st_Reference:		//2
		{// ref		
			ret = strRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String reference extraction error.\n)");
				return ret;
			}
			if (strRef->refType < rT_rowbreak_ref)
			{
				strInt = (int)(strRef->intValue & 0xffffffff);// explicit downsize to remove warning
				strRefType = (ddlstringType_t) VARIABLE_STRING_TAG;
			}
			else
			{
				strRefType = (ddlstringType_t) VAR_REF_STRING_TAG;
			}
			break;
		}

		case st_EnumDescRef:    //3
		{// int + ref	;
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum number extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;// which enum to read it from	

			ret = strRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum reference extraction error.\n)");
				return ret;
			}
			strRefType = (ddlstringType_t)((strRef->refType < rT_rowbreak_ref) ? ENUMERATION_STRING_TAG : ENUM_REF_STRING_TAG);
			break;
		}

		case st_AttributeRef:   //4
		{// byte + ref
			// read attribute type			
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Attribute Name extraction error.\n)");
				return ret;
			}
			strInt = (int)(0xffffffff & tempLongLong);// changed to encoded int 17jul17
			//strInt = (UCHAR)(**pData);
			//pData++; offset++; length--;

			ret = strRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String attribute reference extraction error.\n)");
				return ret;
			}

			break;
		}

		default:
		{
			LOGIT(CERR_LOG,"Error: Totally unknown string type. (0x%04x)\n", tagValue);
			return -4;
		}
	}

	return 0;
}
