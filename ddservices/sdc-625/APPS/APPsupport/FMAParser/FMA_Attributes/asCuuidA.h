/*************************************************************************************************
 *
 * filename  asCuuidA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A UUID Attribute class
 *
 */

#ifndef _asCuuidA_
#define _asCuuidA_

#include "..\FMx_Attributes\asCuuid.h"

class asCuuidA : public asCuuid
{
public: // construct 
	asCuuidA() { };   // initialize underlying class data
	virtual ~asCuuidA() {}
    
public: // methods
 
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_asCuuidA_



