/*************************************************************************************************
 *
 * filename  asCenumLstSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A ENum List Specifier Attribute class
 *
 */

#ifndef _ASCenumLstSPECA_
#define _ASCenumLstSPECA_

#include "..\FMA_Attributes\icCenumLstA.h"
#include "..\FMA_conditional.h"

class asCenumLstSpecA : public FMx_Attribute
{
public:
	asCenumLstSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_condList<icCenumLstA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
        aCattrEnum* attr = new aCattrEnum;
		conditional.hydratePrototype(&attr->condEnumListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCenumLstSPECA_


