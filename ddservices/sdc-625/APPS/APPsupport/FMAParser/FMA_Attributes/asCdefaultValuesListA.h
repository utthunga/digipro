/*************************************************************************************************
 *
 * filename  asCdefaultValuesListA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Default Value List Attribute class
 * Definition for version A Expression Element List Attribute class
 * Definition for version A Expression List Attribute class
 *
 */

#ifndef _asCdefaultValuesListA_
#define _asCdefaultValuesListA_

#include "..\FMx_Attributes\asCdefaultValuesList.h"

class icCexpressionListElemA : public icCexpressionListElem
{
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCexpressionListA : public icCexpressionList
{
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class asCdefaultValuesListA : public asCdefaultValuesList
{
public: // construct / destruct
	asCdefaultValuesListA() {};
	virtual ~asCdefaultValuesListA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_asCdefaultValuesListA_



