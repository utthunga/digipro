/*************************************************************************************************
 *
 * filename  asCclassSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Class Specifier Attribute class
 *
 */

#ifndef _ASCclassSPECA_
#define _ASCclassSPECA_

#include "icCclassA.h"
#include "..\FMA_conditional.h"

class asCclassSpecA : public FMx_Attribute
{
public:
	asCclassSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icCclassA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
		FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCclassSPECA_


