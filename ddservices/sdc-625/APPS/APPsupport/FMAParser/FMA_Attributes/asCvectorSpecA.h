/*************************************************************************************************
 *
 * filename  asCvectorSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Vector Specifier Attribute class
 *
 */

#ifndef _ASCvectorSPECA_
#define _ASCvectorSPECA_

#include "..\FMA_Attributes\icCgridMemberA.h"

#include "..\FMA_conditional.h"
#include "tags_sa.h"


/*
Vector_Specifier ::= [STRUCTURE_TAG 82]
Conditional_List[ Grid_Member, 82 ]
*/

class asCvectorSpecA : public FMx_Attribute
{
public:
	asCvectorSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_A_condList<icCgridMemberListA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };

	virtual aCattrBase* generatePrototype()
	{
		aCattrGridMemberList* attr = new aCattrGridMemberList;
		conditional.hydratePrototype(&attr->condGridMemberListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCvectorSPECA_


