/*************************************************************************************************
 *
 * filename  asCintegerSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Integer Specifier Attribute class
 *
 */

#ifndef _ASCINTEGERSPECA_
#define _ASCINTEGERSPECA_

#include "..\FMA_conditional.h"
#include "faCLongA.h"

class asCintegerSpecA : public FMx_Attribute
{
public:
	asCintegerSpecA(int tag) : FMx_Attribute(tag) {};

public:
	fC_A_Conditional<faCLongA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCINTEGERSPECA_