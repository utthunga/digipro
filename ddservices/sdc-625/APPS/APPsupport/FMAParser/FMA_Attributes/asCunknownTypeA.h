/*************************************************************************************************
 *
 * filename  asCunknownTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Unknown Type Attribute class
 *
 */

#ifndef _ASCUNKNOWNTYPEA_
#define _ASCUNKNOWNTYPEA_

#include "..\FMx_Attributes\asCunknownType.h"

class asCunknownTypeA : public asCunknownType
{
public:
	asCunknownTypeA(int tag) : asCunknownType(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCUNKNOWNTYPEA_
