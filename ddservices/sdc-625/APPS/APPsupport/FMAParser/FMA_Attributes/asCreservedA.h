/*************************************************************************************************
 *
 * filename  asCreservedA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Reserved Attribute class
 *
 */

#ifndef _ASCRESERVEDTYPEA_
#define _ASCRESERVEDTYPEA_

#include "..\FMx_Attributes\asCreserved.h"

class asCreservedA : public asCreserved
{
public:
	asCreservedA(int tag) : asCreserved(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCRESERVEDTYPEA_
