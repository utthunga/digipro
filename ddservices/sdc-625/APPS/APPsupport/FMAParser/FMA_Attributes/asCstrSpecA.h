/*************************************************************************************************
 *
 * filename  asCstrSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A String Specifier Attribute class
 *
 */

#ifndef _ASCSTRSPECA_
#define _ASCSTRSPECA_

#include "..\FMA_conditional.h"
#include "faCStringA.h"

class asCstrSpecA : public FMx_Attribute
{
public:
	asCstrSpecA(int tag) : FMx_Attribute(tag) {};

public:
	fC_A_Conditional<faCStringA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrString* attr = new aCattrString;
		conditional.hydratePrototype(&attr->condString);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};

	static asCstrSpecA *generateDefault(unsigned atype)
	{
		asCstrSpecA *spec = new asCstrSpecA(0);
		faCStringA *ic = new faCStringA();
		if (spec && ic)
		{
			ic->strRefType = ds_NoString;// will cause default string to generate on hydrate
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);	//  [8/14/2015 timj]
		}
		return spec;
	};

};

#endif //_ASCSTRSPECA_
