/*************************************************************************************************
 *
 * filename  asCdisplaySizeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Display Size Attribute
 *
 */

#include "logging.h"
#include "asCdisplaySizeA.h"
#include "..\FMx_Support_primatives.h"
#include "..\Convert.h"

/*
Display_Size ::=  
ENUMERATED (size 1)
{
	XX_SMALL			(0),
	X_SMALL				(1),
	SMALL				(2),
	MEDIUM				(3),
	LARGE				(4),
	X_LARGE				(5),
	XX_LARGE			(6),
	XXX_SMALL			(7)
}
*/

int asCdisplaySizeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
	RETURNCODE ret = 0;

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: DisplaySize attribute error in size extraction.\n)");
		}
		else
		{
			rawValue = (int)tempLongLong;
			value = ConvertTo8::displaySize(rawValue);
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: DisplaySize attribute needs size but has no length.\n)");
		ret = -2;
	}

	return ret;
}
