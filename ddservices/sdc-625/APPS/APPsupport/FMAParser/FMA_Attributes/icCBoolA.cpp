/*************************************************************************************************
 *
 * filename  icCBoolA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A BOOL Attribute
 *
 */

#include "logging.h"
#include "icCBoolA.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"

int icCBoolA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
	RETURNCODE  ret = 0;
	unsigned	tagValue;
	unsigned	tagLength;
	unsigned	strLength = 0;

	// explicit tag 60// ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length, tagValue, tagLength, offset);//verify string,get length

	if (tagValue != STRUCTURE_TAG_BOOL)
	{
		LOGIT(CERR_LOG,"Error: Bool Type has incorrect tag. (%u)\n", tagValue);
		return -1;
	}

	if (tagLength > (unsigned)length)
	{
		LOGIT(CERR_LOG,"Error: String with tag's length bigger than available.\n)");
		return -2;
	}

	value = false;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Boolean attribute error in size extraction.\n)");
		}
		else
		{
			value = tempLongLong != 0;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Boolean attribute needs size but has no length.\n)");
		return -2;
	}

	return ret;
}
