/*************************************************************************************************
 *
 * filename  asCboolSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A BOOL Specifier Attribute class
 *
 */

#ifndef _ASCBOOLSPECA_
#define _ASCBOOLSPECA_

#include "..\FMA_conditional.h"
#include "icCBoolA.h"

class asCboolSpecA : public FMx_Attribute
{
public:
	asCboolSpecA(int tag) : FMx_Attribute(tag) {};

public:
	fC_A_Conditional<icCBoolA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();
	};

	virtual int out(void)
	{
		FMx_Attribute::out();
		conditional.out();
		return 0;
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();
		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};

	static asCboolSpecA *generateDefault(unsigned atype, bool val)
	{
		asCboolSpecA *spec = new asCboolSpecA(0);
		icCBoolA *ic = new icCBoolA();
		if (spec && ic)
		{
			ic->value = val;	// payload
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);
		}
		return spec;
	};

};

#endif // _ASCBOOLSPECA_
