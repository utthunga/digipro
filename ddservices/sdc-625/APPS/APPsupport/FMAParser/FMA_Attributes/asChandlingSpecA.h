/*************************************************************************************************
 *
 * filename  asChandlingSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Handling Specifier Attribute class
 *
 */

#ifndef _ASChandlingSPECA_
#define _ASChandlingSPECA_

#include "icChandlingA.h"
#include "..\FMA_conditional.h"

class asChandlingSpecA : public FMx_Attribute
{
public:
	asChandlingSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icChandlingA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrBitstring* attr = new aCattrBitstring;
		conditional.hydratePrototype(&attr->condBitStr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };

	static asChandlingSpecA *generateDefault(unsigned atype, int unsigned val = 3)
	{
		asChandlingSpecA *hspec = new asChandlingSpecA(0);
		icChandlingA *h = new icChandlingA();
		if (hspec && h)
		{
			h->value = val;	// payload
			hspec->attrType = atype;
			hspec->conditional.MakeDirect(h); 
		}
		return hspec;
	};
};
#endif //_ASChandlingSPECA_


