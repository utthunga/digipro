/*************************************************************************************************
 *
 * filename  asCattrRefOnlyA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Reference Only Attribute class
 *
 */

#ifndef _ASCATTRREFONLYA_
#define _ASCATTRREFONLYA_

class asCattrRefOnlyA : public asCattrRefOnly
{
public:
	asCattrRefOnlyA(int tag) : asCattrRefOnly(tag) {};

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{
		// not interested in content here, just scan over it all
		*pData += length;
		offset += length;
		length = 0;

		return 0;
	}
};


#endif // _ASCATTRREFONLYA_