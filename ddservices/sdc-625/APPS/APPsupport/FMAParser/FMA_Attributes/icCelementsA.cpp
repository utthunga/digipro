/*************************************************************************************************
 *
 * filename  icCelementsA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Element Attribute
 *
 */

#include "icCelementsA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"
#include "..\FMA_Attributes_Ref&Expr.h"
#include "faCStringA.h"

/*
Element ::= [STRUCTURE_TAG 77]
SEQUENCE  {
	explicit-tag	Explicit_Tag,	-- tag = 77
	SEQUENCE  {
		index				INTEGER,
		item				Reference,
		description			String  (OPTIONAL � on explicit-tag.length),
		help				String  (OPTIONAL � on explicit-tag.length)
		}
}
*/

int icCelementA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, ELEMENT_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, index))
		goto end;

	item = new abCReferenceA();
	if (item == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = item->evaluateAttribute(pData, workingLength, offset))
		goto end;


	if (workingLength > 0)
	{
		description = new faCStringA();
		if (description == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = description->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

	if (workingLength > 0)
	{
		help = new faCStringA();
		if (help == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = help->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}

icCelementA& icCelementA::operator=( const icCelementA& src )
{
	icCelement::operator=(src);
	return *this;
}


//=============================================================================================


int icCelementListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCelementA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCelementA *pElem;

	while(length > 4)
	{
		pElem = new icCelementA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCelementListA::destroy()
{
	icCelementA *pElem = NULL;
	vector<icCelementA *>::iterator iT;//ptr2ptr2icCekementA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCelementListA::out(void)
{
	icCelementA *pElem = NULL;
	vector<icCelementA *>::iterator iT;//ptr2ptr2icCelementA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCelementListA::generatePayload(aPayldType*payld)
{	
	aCmemberElementList  *pPlst;
	if (payld)
	{
		pPlst = (aCmemberElementList  *) payld;
	}
	else
	{
		pPlst = new aCmemberElementList;
	}

	aCmemberElement  locElem;
	icCelementA *pElem = NULL;
	vector<icCelementA *>::iterator iT;//ptr2ptr2icCelementA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCelementListA& icCelementListA::operator=(const icCelementListA& s)
{
	icCelementA *pElem = NULL, *pnewElem = NULL;
	vector<icCelementA *>::const_iterator iT;//ptr2ptr2icCelementA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCelementA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
