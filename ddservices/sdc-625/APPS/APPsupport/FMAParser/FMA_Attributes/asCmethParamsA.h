/*************************************************************************************************
 *
 * filename  asCmethParamsA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Method Param Attribute class
 *
 */

#ifndef _ASCmethParamsA_
#define _ASCmethParamsA_

#include "..\FMA_Attributes\icCmethParamA.h"
#include "..\FMx_Attributes\asCmethParams.h"
#include "tags_sa.h"

/*
Method_Parameters ::= [STRUCTURE_TAG 88]
SEQUENCE  
{
	explicit-tag	Explicit_Tag,		-- tag = 88 
	SEQUENCE OF		Method_Parameter
}
*/

class asCmethParamsA : public asCmethParams
{
public:
	asCmethParamsA(int tag) : asCmethParams(tag) {};
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		unsigned tagLength;
		RETURNCODE  ret = 0;

		if (ret = parseExpectedExplicitTag(pData, length, offset, METHOD_PARAMETERS_TAG_A, tagLength))
			return ret;

		int			exitLength		= length   - tagLength;
		unsigned	exitOffset		= offset   + tagLength;
		UCHAR	   *exitPdata		= (*pData) + tagLength;
		int			workingLength	= tagLength;

		while (workingLength > 0)
		{
			icCmethParamA *p = new icCmethParamA;
			if (p == 0)
			{
				LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
				ret = -3;
				goto end;
			}
			paramList.push_back(p);

			if (ret = p->evaluateAttribute(pData, workingLength, offset))
				goto end;
		}

end:
		*pData = exitPdata;
		length = exitLength;
		offset = exitOffset;

		return ret;
    };
};

#endif //_ASCmethParamsA_


