/*************************************************************************************************
 *
 * filename  icCdataElemA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Data Field Attribute
 *
 */

#include "icCdataElemA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"

#include "faCBitStringA.h"

int icCdataElemA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 

	if (ret = parseExpectedExplicitTag(pData, length, offset, LOCAL_PARAMETERS_ATT_NAME, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;


	ret = parseUnsigned(pData, length, offset, choice);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: Data Field attribute error in size extraction.\n)");
		goto end;
	}

	switch (choice)
	{
		case cmdDataConst:
		{
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataConst.\n)");
				goto end;
			}
			intRef = (unsigned int)tempLongLong;
			break;
		}
			
		case cmdDataReference:
		{
			ret = varRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataReference.\n)");
				goto end;
			}
			break;
		}

		case cmdDataFlags:
		{
			ret = varRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataFlags.\n)");
				goto end;
			}
			flags->evaluateAttribute(pData, length, offset);
			break;
		}

		case cmdDataWidth:
		{
			ret = varRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataWidth.\n)");
				goto end;
			}
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataWidth.\n)");
				goto end;
			}
			width = (int)tempLongLong;					
			// stevev 31march2016  moved to hydration generateElement()...flags->value = cmdDataItemFlg_WidthPresent;// the x8000 bit
			break;
		}

		case cmdDataFlagWidth:
		{
			ret = varRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataFlagWidth.\n)");
				goto end;
			}
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataFlagWidth.\n)");
				goto end;
			}
			width = (int)tempLongLong;
			ret = flags->evaluateAttribute(pData, length, offset);				
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataFlagWidth.\n)");
				goto end;
			}
			// stevev 31march2016  moved to hydration generateElement()...flags->value |= cmdDataItemFlg_WidthPresent;// the x8000 bit
			break;
		}
		
		case cmdDataFloat:
		{
			double dval = 0.0;
			ret = parseFLOAT(pData, length, dval, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: DataElem evaluation error in cmdDataFloat.\n)");
				goto end;
			}
			fvalue = (float)dval;	// FLOATCHANGE no change required [1/20/2016 tjohnston]
			break;
		}

		default:
		{
			LOGIT(CERR_LOG,"Error: Invalid choice: %d.\n)", choice);
		}
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}




//=============================================================================================


int icCdataElemListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCdataElemA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCdataElemA *pElem;

#ifdef _DEBUG
	int elemCnt = 0;
#endif

	while(length >= 4)
	{
#ifdef _DEBUG
		elemCnt++;
#endif
		pElem = new icCdataElemA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCdataElemListA::destroy()
{
	icCdataElemA *pElem = NULL;
	vector<icCdataElemA *>::iterator iT;//ptr2ptr2icCdataElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCdataElemListA::out(void)
{
	icCdataElemA *pElem = NULL;
	vector<icCdataElemA *>::iterator iT;//ptr2ptr2icCdataElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


aPayldType* icCdataElemListA::generatePayload(aPayldType*payld)
{	
	aCdataitemList  *pPlst;
	if (payld)
	{
		pPlst = (aCdataitemList  *) payld;
	}
	else
	{
		pPlst = new aCdataitemList;
	}

	icCdataElemA *pElem = NULL;
	vector<icCdataElemA *>::iterator iT;//ptr2ptr2icCdataElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			aCdataItem  locElem;
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
		}
	}//next

	return pPlst;
}


icCdataElemListA& icCdataElemListA::operator=(const icCdataElemListA& s)
{
	icCdataElemA *pElem = NULL, *pnewElem = NULL;
	vector<icCdataElemA *>::const_iterator iT;//ptr2ptr2icCdataElemA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCdataElemA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}