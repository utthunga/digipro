/*************************************************************************************************
 *
 * filename  asCtimeScaleSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Time Scale Specifier Attribute class
 *
 */

#ifndef _ASCtimeScaleSPECA_
#define _ASCtimeScaleSPECA_

#include "icCtimeScaleA.h"
#include "..\FMA_conditional.h"

class asCtimeScaleSpecA : public FMx_Attribute
{
public:
	asCtimeScaleSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icCtimeScaleA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrBitstring* attr = new aCattrBitstring;
		conditional.hydratePrototype(&attr->condBitStr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCtimeScaleSPECA_


