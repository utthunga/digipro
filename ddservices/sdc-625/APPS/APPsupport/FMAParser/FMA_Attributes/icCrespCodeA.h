/*************************************************************************************************
 *
 * filename  icCrespCodeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Response Code Attribute class
 *
 */

#ifndef _icCrespCodeA_
#define _icCrespCodeA_

#include "..\FMx_Attributes\icCrespCode.h"

class icCrespCodeA : public icCrespCode
{
public: // construct / destruct
	icCrespCodeA() {};
	icCrespCodeA(const icCrespCodeA& elem) {operator=(elem);};	// must have copy constructor 													
	virtual ~icCrespCodeA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};


class icCrespCodeListA : public vector<icCrespCodeA *>
{
public: // construct / destruct
	icCrespCodeListA() {};
	icCrespCodeListA(const icCrespCodeListA& src) { operator=(src); };
	virtual ~icCrespCodeListA() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCrespCodeListA&	operator=(const icCrespCodeListA& s);
};

#endif //_icCrespCodeA_



