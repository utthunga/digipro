/*************************************************************************************************
 *
 * filename  icCchartTypeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Chart Type Attribute
 *
 */

#include "icCchartTypeA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"
#include "..\convert.h"

/*
Chart_Type ::=	[STRUCTURE_TAG 58]
SEQUENCE {
	implicit-tag		Implicit_Tag,	-- tag = 58
	ENUMERATED (size 1)
	{
		strip				(0),
		sweep				(1),
		scope				(2),
		horizontal-bar		(3),
		vertical-bar		(4),
		gauge				(5)
	}
}
*/


int icCchartTypeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	PARSE_EXPECTED_IMPLICIT_TAG(CHART_TYPE_TAG_A, ret);

	rawValue = 0;
	ret = parseUnsigned(pData, length, offset, rawValue);

	value = ConvertTo8::chartType(rawValue);

	return ret;
}


