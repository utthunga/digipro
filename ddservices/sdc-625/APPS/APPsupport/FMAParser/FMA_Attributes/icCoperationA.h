/*************************************************************************************************
 *
 * filename  icCoperationA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Operation Attribute class
 *
 */

#ifndef _icCoperationA_
#define _icCoperationA_

#include "..\FMx_Attributes\icCoperation.h"

class icCoperationA : public icCoperation
{
public: // construct / destruct
	icCoperationA(int tag): icCoperation(tag) {}
	virtual ~icCoperationA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCoperationA_



