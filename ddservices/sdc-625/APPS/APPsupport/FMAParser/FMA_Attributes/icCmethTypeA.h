/*************************************************************************************************
 *
 * filename  icCmethTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Method Type Attribute class
 *
 */

#ifndef _icCmethTypeA_
#define _icCmethTypeA_

#include "..\FMx_Attributes\icCmethType.h"

class icCmethTypeA : public icCmethType
{
public: // construct / destruct
	icCmethTypeA() {}
	virtual ~icCmethTypeA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCmethTypeA_



