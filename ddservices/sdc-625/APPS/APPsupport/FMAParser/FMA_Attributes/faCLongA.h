/*************************************************************************************************
 *
 * filename  faCLongA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Long Attribute class
 *
 */

#ifndef _FACLONGA_
#define _FACLONGA_

#include "..\FMx_Attributes\faCLong.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class faCLongA : public faCLong
{
public: // construct / destruct
	faCLongA() {}
	virtual ~faCLongA() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACLONGA_
