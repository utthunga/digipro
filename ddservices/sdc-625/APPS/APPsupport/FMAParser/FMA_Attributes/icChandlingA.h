/*************************************************************************************************
 *
 * filename  icChandlingA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Handling Attribute class
 *
 */

#ifndef _icChandlingA_
#define _icChandlingA_

#include "..\FMx_Attributes\icChandling.h"

class icChandlingA : public icChandling
{
public: // construct / destruct
	icChandlingA() {};
	virtual ~icChandlingA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icChandlingA_



