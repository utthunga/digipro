/*************************************************************************************************
 *
 * filename  icCstyleA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Style Attribute
 *
 */

#include "icCstyleA.h"
#include "logging.h"
#include "..\Convert.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Style ::=  
ENUMERATED (size 1)
{
	window				(0),
	dialog				(1),
	page				(2),
	group				(3),
	menu				(4),
	table				(5)
	}
*/

int icCstyleA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	value = 0;
	ret = parseUnsigned(pData, length, offset, value);
    
	value = ConvertTo8::style(value);

    return ret;
}


