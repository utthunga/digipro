/*************************************************************************************************
 *
 * filename  icCclassA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Class Attribute
 *
 */

#include "icCclassA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

#include "faCBitStringA.h"

int icCclassA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagValue;
	RETURNCODE  ret = 0;
 
	PARSE_IMPLICIT_TAG(tagValue, ret);

	if (ret == SUCCESS)
			PARSE_BITSTRING(rawValue, ret);

	value = ConvertTo8::classA28(rawValue);

	return ret;
}

