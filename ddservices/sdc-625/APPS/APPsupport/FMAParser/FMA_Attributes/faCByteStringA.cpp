/*************************************************************************************************
 *
 * filename  faCByteStringA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Byte String Attribute
 *
 */

#include "logging.h"
#include "faCByteStringA.h"
#include "..\FMx_Support_primatives.h"

int faCByteStringA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;

	len = length;
	ret = parseByteString(pData, length, len, offset, &pStore);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: 8 ByteString has an error.\n)");
		return status;
	}

//	str = (char *) pStore;
//offset += (len - length);

	return 0;
}
