/*************************************************************************************************
 *
 * filename  icCdataElemA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Data Field Attribute class
 *
 */

#ifndef _ICCDATAELEMA_
#define _ICCDATAELEMA_

#include "..\FMx_Attributes\icCdataElem.h"
#include "..\FMA_Attributes_Ref&Expr.h"

#include "faCBitStringA.h"

class icCdataElemA: public icCdataElem
{
public: // construct 
	icCdataElemA() 
	{
		varRef = new abCReferenceA;
		flags  = new faCBitStringA;
	};
	icCdataElemA(const icCdataElemA& elem) { operator=( elem );};
	virtual ~icCdataElemA() {};
    
public: // methods
	int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCdataElemListA : public vector<icCdataElemA *>
{
public: // construct / destruct
	icCdataElemListA() {};
	icCdataElemListA(const icCdataElemListA& src) { operator=(src); };
	virtual ~icCdataElemListA() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCdataElemListA&	operator=(const icCdataElemListA& s);
};

#endif //_ICCDATAELEMA_
