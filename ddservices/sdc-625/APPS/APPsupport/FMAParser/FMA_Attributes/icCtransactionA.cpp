/*************************************************************************************************
 *
 * filename  icCtransactionA.cpp
 *
 * 27 June 2013 - stevev
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Transaction Item in a Transaction list
 *
 */

#include "icCtransactionA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"


int icCtransactionA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = SUCCESS;
	exitValues_t HOLD; pushExitValues(HOLD, pData, length, offset);
 
	ret = parseInteger(4, pData, length, tempLongLong, offset);// transaction number
	if ( ret )
	{
		LOGIT(CERR_LOG,"Error: Parsing the transaction number failed.\n");
		popExitValues(HOLD, pData, length, offset);
		return -1;
	}
	//else
	transactionNumber = (int)tempLongLong;

	while (length > 0)// the 4 possible fields are bounded by length
	{// implicit tag ::>  ReturnLen is zero (with no error) on an implicit tag
		ret = parseTag(pData, length,   tagValue,   tagLength, offset);

		if (tagLength > 0 || tagValue > 3 || ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Transaction attribute expected a valid implicit tag.\n)");
			popExitValues(HOLD, pData, length, offset);
			ret = -2;
		}

		switch (tagValue)
		{
			case 0:		// Request
				{	
					asCdataFieldSpec* ptr = new asCdataFieldSpecA;// was asCdataFieldSeqA //  [3/9/2015 timj]
					if ( ptr->evaluateAttribute(pData, length, offset) != SUCCESS )
					{
						popExitValues(HOLD, pData, length, offset);
						ptr->destroy();
						delete ptr;
						ret = -3;
					}
					else
					{
						//ptr->list.push_back(item);
						pRequestPkt = ptr;
					}
				}
				break;
				
			case 1:		// Reply
				{	
					asCdataFieldSpec* ptr = new asCdataFieldSpecA;	// was asCdataFieldSeqA //  [3/9/2015 timj]
					if (ptr->evaluateAttribute(pData, length, offset) != SUCCESS )
					{
						popExitValues(HOLD, pData, length, offset);
						ptr->destroy();
						delete ptr;
						ret = -4;
					}
					else
					{
						//ptr->list.push_back(item);
						pReplyPkt = ptr;
					}
				}
				break;

			case 2:		// Response Codes
				{	
					asCrespCodesSpec* ptr = new asCrespCodesSpecA(0);						
					if ( ptr->evaluateAttribute(pData, length, offset) != SUCCESS )
					{
						popExitValues(HOLD, pData, length, offset);
						delete ptr;
						ret = -5;
					}
					else
					{
						pRespCodes = ptr;
					}
				}
				break;

			case 3:		// Post Request Actions
				{	
					asCactionListSpecA* ptr = new asCactionListSpecA(0);						
					if ( ptr->evaluateAttribute(pData, length, offset) != SUCCESS )
					{
						popExitValues(HOLD, pData, length, offset);
						delete ptr;
						ret = -6;
					}
					else
					{
						pActions = ptr;
					}
				}
				break;

			default:
				LOGIT(CERR_LOG,"Error: Transaction got an unknown choice tag.\n");
				popExitValues(HOLD, pData, length, offset);
				ret = -7;
				break;
		}// endswitch on tag type
	}// wend length 

	return ret;
}
