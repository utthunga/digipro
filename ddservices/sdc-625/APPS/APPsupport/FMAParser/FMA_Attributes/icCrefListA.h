/*************************************************************************************************
 *
 * filename  icCrefListA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Reference List Attribute class
 *
 */

#ifndef _icCrefListA_
#define _icCrefListA_

#include "..\FMx_Attributes\icCrefList.h"

class icCrefListElemA : public icCrefListElem /* isa vector<abCReference*> */
{
public: // construct / destruct
	icCrefListElemA() {};
	virtual ~icCrefListElemA() {};
	icCrefListElemA(const icCrefListElemA& src) { operator=(src); };
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCrefListA : public FMx_Attribute, vector<icCrefListElemA*>
{
public: // construct / destruct
	icCrefListA() {};
	icCrefListA(int tag) : FMx_Attribute(tag) {};
	icCrefListA(const icCrefListA& src) { operator=(src); };
	virtual ~icCrefListA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual int out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCrefListA&	operator=(const icCrefListA& s);
};

class asCrefListA : public icCrefListA
{
public: // construct / destruct
	asCrefListA(int tag=0) : icCrefListA(tag) {};
	asCrefListA(const asCrefListA& src) 
	{ 
		const icCrefListA *base = &src;
		icCrefListA::operator=(*base); 
	};
	virtual ~asCrefListA() {};


	virtual aCattrBase* generatePrototype()
	{
		aCgenericConditional* ddlcond = new aCgenericConditional;
		ddlcond->priExprType = eT_Direct;

		// hydrate the conditional internal
		aCgenericConditional::aCexpressDest edest;
		edest.destType = eT_Direct;//this destination type   - actually a clauseType_t  
		edest.pPayload = generatePayload();
		edest.pCondDest = NULL;
		ddlcond->destElements.push_back(edest);
		edest.destroy(); // we're assuming the push did a deep copy of the dest

		ddlcond->priExprType = eT_Direct; // - direct implies there is a single payload item in the dest_list
		
		aCattrReferenceList *pCondList = new aCattrReferenceList;
		pCondList->RefList.genericlist.push_back(*((aCcondList::oneGeneric_t*)ddlcond));

		pCondList->attr_mask = maskFromInt(attrType);

		return pCondList;
	}

public: // workers
	icCrefListA&	operator=(const icCrefListA& s);
};


#endif //_icCrefListA_



