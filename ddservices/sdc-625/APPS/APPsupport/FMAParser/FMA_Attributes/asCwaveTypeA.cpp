/*************************************************************************************************
 *
 * filename  asCwaveTypeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Wave Type Attribute
 *
 */

#include "logging.h"
#include "asCwaveTypeA.h"
#include "..\FMx_Support_primatives.h"
#include "..\Convert.h"

/*
Waveform_Type ::= 
ENUMERATED size(1)
{
	xy				(0),
	yt				(1),
	horizontal		(2),
	vertical		(3)
}

*/

int asCwaveTypeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
	RETURNCODE ret = 0;

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: WaveType attribute error in size extraction.\n)");
		}
		else
		{
			rawValue = (int)tempLongLong;
			value = ConvertTo8::waveType(rawValue);
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: WaveType attribute needs size but has no length.\n)");
		ret = -2;
	}

	return ret;
}
