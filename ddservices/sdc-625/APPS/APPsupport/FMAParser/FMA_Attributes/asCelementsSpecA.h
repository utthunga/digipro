/*************************************************************************************************
 *
 * filename  asCelementsSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Element Specifier Attribute class
 *
 */

#ifndef _ASCelementsSPECA_
#define _ASCelementsSPECA_

#include "..\FMA_Attributes\icCelementsA.h"

#include "..\FMA_conditional.h"

class asCelementsSpecA : public FMx_Attribute
{
public:
	asCelementsSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_A_condList<icCelementListA> conditional;
 
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrMemberList* list = new aCattrMemberList;
		conditional.hydratePrototype(&list->condMemberElemListOlists);
		isConditional = conditional.isConditional();

		list->attr_mask = maskFromInt(attrType);

        return list;
    };
};

#endif //_ASCelementsSPECA_


