/*************************************************************************************************
 *
 * filename  icCmethTypeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Method Type Attribute
 *
 */

#include "icCmethTypeA.h"
#include "logging.h"
#include "..\Convert.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Method_Type ::= 
ENUMERATED 
{
	void			[ 0]
	char			[ 1],		-- is a int8
	short			[ 2],		-- is a int16
	long			[ 3],		-- is a int32
	float			[ 4],		-- is a single precision floating point
	double			[ 5], 		-- is a double precision floating point
	uchar			[ 6],		-- is a unsigned int8
	ushort			[ 7],		-- is a unsigned int16
	ulong			[ 8],		-- is a unsigned int32
	longlong 		[ 9],		-- is a int64
	ulonglong		[10],		-- is a unsigned int64
	ddstring  		[11],		-- is a DD specific string
	dditem			[12],		-- is a DD item		(not a return type)
	timet			[13]		-- is a time_t type
}
*/

int icCmethTypeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	value = 0;
	ret = parseUnsigned(pData, length, offset, value);

	value =  ConvertTo8::parameterType(value);
    
    return ret;
}


