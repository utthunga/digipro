/*************************************************************************************************
 *
 * filename  icCorientA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Orientation Attribute
 *
 */

#include "icCorientA.h"
#include "logging.h"
#include "..\Convert.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Orientation  ::=  [STRUCTURE_TAG 69] 
SEQUENCE  
{
	implicit-tag	Implicit_Tag,	-- tag = 69
	ENUMERATED (size 1)
	{
		HORIZONTAL	(0), 
		VERTICAL	(1)
	}
}
*/

int icCorientA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute expected implicit tag.\n)");
		return -1;
	}

	if (tagValue != ORIENTATION_TAG_A)
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute explicit tag is not 69.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Orientation attribute error in size extraction.\n)");
		}
		else
		{
			rawValue = (int)tempLongLong;
			value = ConvertTo8::orient(rawValue);
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}


