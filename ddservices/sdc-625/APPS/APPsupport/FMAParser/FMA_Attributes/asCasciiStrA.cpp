/*************************************************************************************************
 *
 * filename  asCasciiStrA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A ASCII String Attribute
 *
 */

#include "logging.h"
#include "asCasciiStrA.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
ASCII_String ::=  [STRUCTURE TAG 61]
SEQUENCE {
		Explicit-tag		Explicit_Tag,		-- tag value 61 		
		text				BYTE_STRING	 		-- length bytes long �holds trailing null
}
*/

int asCasciiStrA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned) length)
	{
		LOGIT(CERR_LOG,"Error: asCasciiStr attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != ASCII_STRING_TAG_A)
	{
		LOGIT(CERR_LOG,"Error: asCasciiStr attribute ASCII string explicit tag is not 61.\n)");
		return -1;
	}

	ret = parseByteString(pData, length, tagLength, offset, &pStore);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: ASCII String has an error.\n)");
		return status;
	}

	str = (char *) pStore;
	len = str.length()+1;		// length must include the trailing NULL

	return 0;
}
