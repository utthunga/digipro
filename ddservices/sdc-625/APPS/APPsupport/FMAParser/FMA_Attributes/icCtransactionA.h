/*************************************************************************************************
 *
 * filename  icCtransactionA.h
 *
 * 27 June 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Transaction Attribute class
 *
 * #include "icCtransactionA.h"
 *
 */

#ifndef _icCtransactionA_
#define _icCtransactionA_

#include "..\FMx_Attributes\icCtransaction.h"
#include "asCdataFieldSpecA.h"
#include "asCrespCodesSpecA.h"
#include "asCactionListSpecA.h"

class icCtransactionA : public icCtransaction
{
public:  // data

public: // construct / destruct
	icCtransactionA(){};

	virtual ~icCtransactionA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCtransactionA_



