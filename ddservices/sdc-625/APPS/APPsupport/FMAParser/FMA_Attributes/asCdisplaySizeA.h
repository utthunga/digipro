/*************************************************************************************************
 *
 * filename  asCdisplaySizeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Display Size Attribute class
 *
 */

#ifndef _ASCDISPLAYSIZEA_
#define _ASCDISPLAYSIZEA_

#include "..\FMx_Attributes\asCdisplaySize.h"

class asCdisplaySizeA : public asCdisplaySize
{
public:
	asCdisplaySizeA(int tag) : asCdisplaySize(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCINTEGERCONSTA_
