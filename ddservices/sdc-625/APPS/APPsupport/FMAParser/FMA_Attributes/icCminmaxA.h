/*************************************************************************************************
 *
 * filename  icMinMaxA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Min Max Attribute class
 *
 */

#ifndef _icCminmaxA_
#define _icCminmaxA_

#include "..\FMx_Attributes\icCminmax.h"
#include "asCexprSpecA.h"
#include "TAGS_SA.H"
#include "FMx_Support_primatives.h"

class icMinMaxA : public icMinMax<asCexprSpecA>
{
public:
	icMinMaxA() {};
	virtual ~icMinMaxA() {};// i have no memory
	icMinMaxA(const icMinMaxA &src) { icMinMax<asCexprSpecA>::operator =(src); };
	void destroy() { icMinMax<asCexprSpecA>::destroy(); };
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
	{
		unsigned tagLength;
		UINT64 tmpLongLong;
		int ret;

		if (ret = parseExpectedExplicitTag(pData, length, offset, MINMAX_VALUE_TAG_A, tagLength))
		{
			LOGIT(CERR_LOG,"Error: MinMaxValue attribute error in size extraction.\n)");
			return ret;
		}
		
		length -= tagLength;

		while (tagLength > 0)
		{
			minmaxValue<asCexprSpecA> Value;

			parseInteger(1, pData, (int&)tagLength, tmpLongLong, offset);
			Value.which = (int)tmpLongLong;

			ret = Value.Expression.evaluateAttribute(pData, (int&)tagLength, offset);

			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: MinMaxValue attribute error in size extraction.\n)");
			}

			values.push_back(Value);
			Value.clear();
		}

		return 0;
	}
};

#endif //_icCminmaxA_



