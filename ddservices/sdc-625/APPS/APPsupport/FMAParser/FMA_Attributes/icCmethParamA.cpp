/*************************************************************************************************
 *
 * filename  icCmethParamA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Method Param Attribute
 *
 */

#include "icCmethParamA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Method_Parameter ::= [STRUCTURE_TAG 87]
SEQUENCE  {
		explicit-tag	Explicit_Tag,					-- tag = 87 
		variable-type	Method_Type,					-- 0 (void) for no return
		type-modifiers	Type_Modifiers,				-- zero if no modifiers
	param-name		ASCII_String
}
*/

int icCmethParamA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, METHOD_PARAMETER_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;

	if (ret = parseUnsigned(pData, length, offset, methodType))
		goto end;

	methodType = ConvertTo8::parameterType(methodType);

	typeModifiers = new faCBitStringA();
	if (typeModifiers == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = typeModifiers->evaluateAttribute(pData, length, offset))
		goto end;


	paramName = new asCasciiStrA();
	if (paramName == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = paramName->evaluateAttribute(pData, length, offset))
		goto end;

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}


