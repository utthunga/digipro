/*************************************************************************************************
 *
 * filename  asCvarTypeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Variable Type Attribute
 *
 */

#include "logging.h"
#include "asCvarTypeA.h"
#include "..\FMx_Support_primatives.h"

int asCvarTypeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64   tempLongLong;
	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned)length)
	{
		LOGIT(CERR_LOG,"Error: VarType attribute with tag's length bigger than available.\n)");
		return -1;
	}

	varType = (var_t)tagValue;

	switch(varType)
	{
		case vt_integer:		// 2 INTEGER  size
		case vt_unsigned:		// 3 INTEGER  size
		case vt_enum:			// 6 INTEGER  size
		case vt_bitEnum:		// 7 INTEGER  size
		case vt_index:			// 8 INTEGER  size
		case vt_ascii:			// 9 INTEGER  size				
		case vt_pkdAscii:	    //10 INTEGER  size				
		case vt_password:		//11 INTEGER  size
		case vt_time_value:		//20 INTEGER  size		
		{// we need a size
			if (length > 0)
			{
				ret = parseInteger(4, pData, length, tempLongLong, offset);
				if (ret != SUCCESS)
				{
					LOGIT(CERR_LOG,"Error: VarType attribute error in size extraction.\n)");
				}
				else
				{
					varSize = (int)tempLongLong;
				}
			}
			else// error
			{
				LOGIT(CERR_LOG,"Error: VarType attribute needs size but has no length.\n)");
				ret = -2;
			}

			break;
		}

		case vt_float:			// 4 
		{// implicit size
			varSize = sizeof(float);	// FLOATCHANGE no change required [1/20/2016 tjohnston]

			break;
		}

		case vt_double:			// 5	 
		{// implicit size
			varSize = sizeof(double);

			break;	
		}

		case vt_date:			//13///		 
		{// implicit size
			if (length > 0)// this encoding has the size in it..stevev 13dec12
			{
				ret = parseInteger(4, pData, length, tempLongLong, offset);
				if (ret != SUCCESS)
				{
					LOGIT(CERR_LOG,"Error: VarType attribute error in size extraction.\n)");
				}
			}

			varSize = SIZEOF_DATE;// we set correctly regardless

			break;		
		}

		case vt_bit_string:		//12/// INTEGER  size
		case vt_octet_string:	//18/// INTEGER  size
		case vt_visible_string:	//19/// INTEGER  size	
		{// not supported, needs an integer
			notHART = true;
			if (length > 0)
			{
				ret = parseInteger(4, pData, length, tempLongLong, offset);
				if (ret != SUCCESS)
				{
					LOGIT(CERR_LOG,"Error: XXVarType attribute error in size extraction.\n)");
				}
				else
				{
					varSize = (int)tempLongLong;
				}
			}
			else// error
			{
				LOGIT(CERR_LOG,"Error: XXVarType attribute needs size but has no length.\n)");
				ret = -2;
			}
			ret = 5;

			break;	 
		}

		case vt_time:			//14///
		case vt_date_and_time:  //15///
		case vt_duration:		//16///
		case vt_euc:			//17///
		case vt_object_reference://21////
		{// not supported
			notHART = true;
			ret = 6;

			break;
		}

		case vt_no_type:		// 0///
		case vt_undefined:		// 1///
		default:
		{
			if (tagValue > 1)// default, bad type
			{
				LOGIT(CERR_LOG,"Error: VarType is not allowed (%d).\n)",tagValue);
				ret = -3;
			}
			else // illigal, unknown type
			{
				LOGIT(CERR_LOG,"Error: VarType is illegal (%d).\n)",tagValue);
				ret = -4;
			}

			break;
		}
	}

	return ret;
}
