/*************************************************************************************************
 *
 * filename  asCvarTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Variable Type Attribute class
 *
 */

#ifndef _ASCVARTYPEA_
#define _ASCVARTYPEA_

#include "..\FMx_Attributes\asCvarType.h"

class asCvarTypeA : public asCvarType
{
public: // construct
	asCvarTypeA(int tag) : asCvarType(tag) {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCVARTYPEA_
