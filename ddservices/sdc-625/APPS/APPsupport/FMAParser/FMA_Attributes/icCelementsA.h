/*************************************************************************************************
 *
 * filename  icCelementsA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Elements Attribute class
 *
 */

#ifndef _icCelementsA_
#define _icCelementsA_

#include "..\FMx_Attributes\icCelements.h"
#include "faCStringA.h"

class icCelementA : public icCelement
{
public: // construct / destruct
	icCelementA() : icCelement() {};
	icCelementA(const icCelementA& elem) { operator=(elem); };
	virtual ~icCelementA() {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCelementA& operator=(const icCelementA& src);
};


class icCelementListA : public vector<icCelementA *>
{
public: // construct / destruct
	icCelementListA() {};
	icCelementListA(const icCelementListA& src) { operator=(src); };
	virtual ~icCelementListA() { destroy(); };
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCelementListA&	operator=(const icCelementListA& s);
};

#endif //_icCelementsA_



