/*************************************************************************************************
 *
 * filename  icCmenuItmA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Menu Item Attribute
 *
 */

#include "icCmenuItmA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Attributes\faCBitStringA.h"
#include "tags_sa.h"

int icCmenuItmA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;
    
	//PARSE_EXPECTED_EXPLICIT_TAG(MENU_ITEMS_SPECIFIER_TAG_A, tagLength, ret);
	if (ret = parseExpectedExplicitTag(pData, length, offset, MENU_ITEM_TAG_A, tagLength))
		return ret;

	item = new abCReferenceA();  
	if (item == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		return -3;
	}
	ret = item->evaluateAttribute(pData, length, offset);
	if (ret != SUCCESS)
		return ret;

	qualifiers = new faCBitStringA();     
	if (qualifiers == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		return -4;
	}
	ret = qualifiers->evaluateAttribute(pData, length, offset);


	return ret;
}

icCmenuItmA& icCmenuItmA::operator=(const icCmenuItmA& src)
{
	icCmenuItm::operator=(src);
	return *this;
}

//=============================================================================================


int icCmenuItmListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCmenuItmA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCmenuItmA *pItm;

	while(length > 4)
	{
		pItm = new icCmenuItmA;
		if (pItm == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pItm->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pItm;
			return ret;
		}
		push_back(pItm);
		pItm = NULL;
	}// wend

	return ret;
}

void icCmenuItmListA::destroy()
{
	icCmenuItmA *pI = NULL;
	vector<icCmenuItmA *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			delete pI;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCmenuItmListA::out(void)
{
	icCmenuItmA *pI = NULL;
	vector<icCmenuItmA *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			pI->out();
		}
	}//next
}


//class aCmenuList : public aPayldType, public AmenuItemList_t // itemList;
aPayldType* icCmenuItmListA::generatePayload(aPayldType*payld)
{	
	aCmenuList  *pPlst;
	if (payld)
	{
		pPlst = (aCmenuList  *) payld;
	}
	else
	{
		pPlst = new aCmenuList;
	}

	aCmenuItem  locMI;
	icCmenuItmA *pI = NULL;
	vector<icCmenuItmA *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			pI->generateElement(&locMI);
			pPlst->push_back(locMI);
			locMI.clear();
		}
	}//next

	return pPlst;
}


icCmenuItmListA& icCmenuItmListA::operator=(const icCmenuItmListA& s)
{
	icCmenuItmA *pmI = NULL, *pnewI = NULL;
	vector<icCmenuItmA *>::const_iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pmI = (*iT);
		if ( pmI != NULL )
		{
			pnewI = new icCmenuItmA(*pmI);
			push_back(pnewI);pnewI = NULL;
		}
	}// next
	return *this;
}