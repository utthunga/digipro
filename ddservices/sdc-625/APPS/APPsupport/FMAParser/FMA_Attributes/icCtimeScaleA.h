/*************************************************************************************************
 *
 * filename  icCtimeScaleA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Time Scale Attribute class
 *
 */

#ifndef _icCtimeScaleA_
#define _icCtimeScaleA_

#include "..\FMx_Attributes\icCtimeScale.h"

class icCtimeScaleA : public icCtimeScale
{
public: // construct / destruct
	icCtimeScaleA() {}
	virtual ~icCtimeScaleA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCtimeScaleA_



