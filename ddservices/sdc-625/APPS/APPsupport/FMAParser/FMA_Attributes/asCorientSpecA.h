/*************************************************************************************************
 *
 * filename  asCorientSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Orientation Specifier Attribute class
 *
 */

#ifndef _ASCorientSPECA_
#define _ASCorientSPECA_

#include "icCorientA.h"
#include "..\FMA_conditional.h"

class asCorientSpecA : public FMx_Attribute
{
public:
	asCorientSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icCorientA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };

	static asCorientSpecA *generateDefault(unsigned atype, unsigned long val)
	{
		asCorientSpecA *spec = new asCorientSpecA(0);
		icCorientA *ic = new icCorientA();
		if (spec && ic)
		{
			ic->value = val;	// payload
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);	//  [8/14/2015 timj]
		}
		return spec;
	};

};

#endif //_ASCorientSPECA_


