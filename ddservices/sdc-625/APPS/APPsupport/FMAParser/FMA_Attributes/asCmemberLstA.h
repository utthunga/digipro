/*************************************************************************************************
 *
 * filename  asCmemberLstA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Member List Attribute class
 *
 */

#ifndef _ASCMEMBERLSTA_
#define _ASCMEMBERLSTA_

#include "faCMemberA.h"
#include "..\FMA_conditional.h"

class asCmemberLstA : public FMx_Attribute
{
public:
	asCmemberLstA(int tag=0) : FMx_Attribute(tag) {};

public:
	fC_A_condList<faCMemberListA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  

		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrMemberList* attr =  new aCattrMemberList;
		conditional.hydratePrototype(&attr->condMemberElemListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCMEMBERLSTA_
