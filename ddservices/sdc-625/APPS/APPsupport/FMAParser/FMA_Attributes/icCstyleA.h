/*************************************************************************************************
 *
 * filename  icCstyleA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Style Attribute class
 *
 */

#ifndef _icCstyleA_
#define _icCstyleA_

#include "..\FMx_Attributes\icCstyle.h"

class icCstyleA : public icCstyle
{
public: // construct / destruct
	icCstyleA() {};
	virtual ~icCstyleA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	
	virtual aCattrBase* generatePrototype() 
	{ 
		return createLongAttribute(attrType);
	};
};
#endif //_icCstyleA_



