/*************************************************************************************************
 *
 * filename  asCasciiStrA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A ASCII String Attribute class
 *
 */

#ifndef _ASCASCIISTRA_
#define _ASCASCIISTRA_

#include "..\FMx_Attributes\asCasciiStr.h"

class asCasciiStrA : public asCasciiStr
{
public: // construct / destruct
	asCasciiStrA(int y = 0) : asCasciiStr(y) {};

	virtual ~asCasciiStrA() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCASCIISTRA_
