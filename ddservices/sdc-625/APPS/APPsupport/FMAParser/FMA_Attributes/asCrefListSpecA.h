/*************************************************************************************************
 *
 * filename  asCrefListSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Reference List Specifier Attribute class
 *
 */

#ifndef _ASCrefListSPECA_
#define _ASCrefListSPECA_

#include "..\FMA_Attributes\icCrefListA.h"

#include "..\FMA_conditional.h"

class asCrefListSpecA : public FMx_Attribute
{
public:
	asCrefListSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
    
    fC_A_condList<icCrefListA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out(); 
		if (reconcile==false)
		{
			LOGIT(COUT_LOG,"%23s:\n","Reference List Spec");   // Attribute name
		}

        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrReferenceList* attr = new aCattrReferenceList;
		conditional.hydratePrototype(&attr->RefList);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCrefListSPECA_


