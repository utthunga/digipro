/*************************************************************************************************
 *
 * filename  icCenumLstA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A ENum List Attribute
 * Implementation for version A Status Classes Attribute
 *
 */

#include "..\FMx_Attribute.h"
#include "faCLongA.h"
#include "faCStringA.h"
#include "icCclassA.h"
#include "icCenumLstA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Attributes_Ref&Expr.h"
#include "tags_sa.h"

/*
Enumeration ::= [STRUCTURE_TAG 79]
SEQUENCE  
{
	explicit-tag	Explicit_Tag,			-- tag = 79
	SEQUENCE OF SEQUENCE 
	{
		entry-explicit-tag	Explicit_Tag,
		CHOICE	
		{< as designated by entry-explicit-tag.tag>
			*value			[0]	INTEGER,										
			*description	[1]	String,									
			*help			[2]	String      (OPTIONAL�on explicit-tag.length),
			status			[3]	Status_Class(OPTIONAL�on explicit-tag.length),	  
			actions			[4]	Reference   (OPTIONAL�on explicit-tag.length),	 
			function		[5]	Class       (OPTIONAL�on explicit-tag.length)	
		}			
	}
}
(*)  starred items indicate those that are allowed in non-bit enumerations.
*/

int icCstatusClassesA::out(void) 
    {
		if (reconcile)
		{
			return icCstatusClasses::out();
		}

		char *classStrings[] = {
			"AUTO-GOOD",
			"MANUAL-GOOD",
			"AUTO-BAD",
			"MANUAL-BAD",
			"AUTO-MARGINAL",
			"MANUAL-MARGINAL",
			NULL
		};

        //LOGIT(COUT_LOG,"%23s: %d %s\n",attname, classPairs.size(), " to follow");   // Attribute name
		//LOGIT(COUT_LOG,"%23s:\n", "Output Classes");
		for (unsigned i=0; i < classPairs.size(); i++)
		{
			int bitNum = classPairs[i].baseClass;
			outputClass_t oc = classPairs[i].oc;
			LOGIT(COUT_LOG,"%28s: %s  (%d)\n","base class", baseClassLabels[bitNum], bitNum);
			if (21 <= bitNum && bitNum <= 24)
			{
				LOGIT(COUT_LOG,"%33s: 0x%08x (%d)\n", "kind", oc.kind, oc.kind);
				LOGIT(COUT_LOG,"%33s: %-20s 0x%08x (%d)\n", "class", 
							  classStrings[oc.output_class], oc.output_class, oc.output_class);
				LOGIT(COUT_LOG,"%33s: 0x%08x (%d)\n", "which", oc.which, oc.which);
			}
		}

        return 0;
    }


int icCstatusClassesA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, STATUS_CLASS_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	baseClass = 0;

	// zero or more output classes
	while (workingLength > 0)
	{
		// base class
		unsigned bc = 0, bitNum = 0;
		if (ret = parseUnsigned(pData, workingLength, offset, bitNum))
			goto end;

		assert(bitNum <= 24);

		classPair_t pair;
		pair.baseClass = bitNum;

		if (21 <= bitNum  &&  bitNum <= 24)
		{
			// > 20 implies following output class
			outputClass_t oc;

			oc.kind = bitNum - 20;

			if (ret = parseUnsigned(pData, workingLength, offset, oc.output_class))	// a.k.a. Mode-Rel-Status
				goto end;

			oc.output_class = ConvertTo8::outputClass(oc.output_class);

			if (ret = parseUnsigned(pData, workingLength, offset, oc.which))
				goto end;

			outputClasses.push_back(oc);

			pair.oc = oc;
		}
		else
		{
			bc = 1 << bitNum;
			baseClass |= bc;
		}
		classPairs.push_back(pair);
	}


end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}

int icCenumElemA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	unsigned tagValue8, tagValueA;
    RETURNCODE  ret = 0;
	if (ret = parseExpectedExplicitTag(pData, length, offset, ENUMERATION_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;


	// parse one or more fields
	while (workingLength > 0)
	{
		FMx_Attribute *attr = 0;
		if (ret = parseTag(pData, workingLength,   tagValueA, tagLength, offset ))
			goto end;

		tagValue8 = ConvertTo8::enumerationTag(tagValueA);

		switch (tagValue8)// in version 8 order
		{
		case 0:
			attr = new faCLongA();// 8&A same
			break;

		case 1:
			attr = new icCstatusClassesA();// VA 3
			break;

		case 2:
			attr = new abCReferenceA();// VA 4
			break;

		case 3:
		case 4:
			attr = new faCStringA();// VA 1 & 2
			break;

		case 5:
			attr = new icCclassA();// VA 5
			break;

		default:
			assert(0);
			ret = -5;
			goto end;
		}// endswitch

		if (attr == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -6;
			goto end;
		}

		if (tagValue8 == 1)
		{
			// only status class has explicit length
			int len = tagLength;
			if (ret = attr->evaluateAttribute(pData, len, offset))
				goto end;
			workingLength -= tagLength;
		}
		else
		{
			if (ret = attr->evaluateAttribute(pData, workingLength, offset))
				goto end;
		}

		fields[tagValue8] = attr;
	} // wend

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}

icCenumElemA& icCenumElemA::operator=(const icCenumElemA& src)
{
	icCenumElem::operator=(src);
	return *this;
}

//=============================================================================================


int icCenumLstA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCenumElemA*>
	RETURNCODE  ret = 0;
	int			entryLength	= length;

	while (length > 0)
	{
		icCenumElemA *pElem = new icCenumElemA;
		if (pElem == NULL)
		{
			ret = FMx_MEMFAIL;
			goto end;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			goto end;
		}
		push_back(pElem);
		pElem = NULL;
	}  // wend

end:

	return ret;
}

void icCenumLstA::destroy()
{
	icCenumElemA *pElem = NULL;
	vector<icCenumElemA *>::iterator iT;//ptr2ptr2icCenumElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCenumLstA::out(void)
{
	icCenumElemA *pElem = NULL;
	vector<icCenumElemA *>::iterator iT;//ptr2ptr2icCenumElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmenuList : public aPayldType, public AmenuItemList_t // itemList;
aPayldType* icCenumLstA::generatePayload(aPayldType* payld)
{	
	aCenumList  *pPlst;
	if (payld)
	{
		pPlst = (aCenumList  *)payld;
	}
	else
	{
		pPlst = new aCenumList;
	}

	icCenumElemA *pElem = NULL;
	vector<icCenumElemA *>::iterator iT;//ptr2ptr2icCenumElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			aCenumDesc  locEnumDesc;

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			locEnumDesc.descS.enumVal = -1;
			locEnumDesc.helpS.enumVal = -1; 

			pElem->generateElement(&locEnumDesc);

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (pElem->fields[3])
			{
				if (((faCString*)pElem->fields[3])->strRefType == ds_Dict && locEnumDesc.descS.enumVal == 0)
				{
					locEnumDesc.descS.enumVal = -1;
				}
			}
			if (pElem->fields[4])
			{
				if (((faCString*)pElem->fields[4])->strRefType == ds_Dict && locEnumDesc.helpS.enumVal == 0)
				{
					locEnumDesc.helpS.enumVal = -1;
				}
			}

			pPlst->push_back(locEnumDesc);
		}
	}//next

	return pPlst;
}


icCenumLstA& icCenumLstA::operator=(const icCenumLstA& src)
{
	icCenumElemA *pElem = NULL;
	vector<icCenumElemA *>::const_iterator iT;//ptr2ptr2icCenumElemA
	for( iT = src.begin(); iT != src.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			push_back(new icCenumElemA(*pElem));
		}
	}// next
	return *this;
}