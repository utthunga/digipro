/*************************************************************************************************
 *
 * filename  asCmenuItmSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Menu Item Specifier Attribute class
 *
 */

#ifndef _ASCmenuItmSPECA_
#define _ASCmenuItmSPECA_

#include "..\FMA_Attributes\icCmenuItmA.h"

#include "..\FMA_conditional.h"

class asCmenuItmSpecA : public FMx_Attribute
{
public:
	asCmenuItmSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_A_condList<icCmenuItmListA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
		return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrMenuItems* attr = new aCattrMenuItems;
		conditional.hydratePrototype(&attr->condMenuListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCmenuItmSPECA_


