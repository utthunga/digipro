/*************************************************************************************************
 *
 * filename  icCaxisScalA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Axis Scale Attribute class
 *
 */

#ifndef _icCaxisScalA_
#define _icCaxisScalA_

#include "..\FMx_Attributes\icCaxisScal.h"

class icCaxisScalA : public icCaxisScal
{
public: // construct / destruct
	icCaxisScalA() {}
	virtual ~icCaxisScalA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCaxisScalA_



