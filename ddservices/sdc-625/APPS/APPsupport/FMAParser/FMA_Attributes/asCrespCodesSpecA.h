/*************************************************************************************************
 *
 * filename  asCrespCodesSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Response Code Specifier Attribute class
 *
 */

#ifndef _ASCrespCodesSPECA_
#define _ASCrespCodesSPECA_

#include "..\FMx_Support_primatives.h"
#include "..\FMA_Attributes\icCrespCodeA.h"

#include "..\FMA_conditional.h"
#include "..\FMx_Attributes\asCrespCodesSpec.h"

class asCrespCodesSpecA : public asCrespCodesSpec
{
public:
	asCrespCodesSpecA(int tag) : asCrespCodesSpec(tag) {};
    
public:
	
	fC_A_condList<icCrespCodeListA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
 		unsigned tagLength, tagValue;
		RETURNCODE  ret = 0;

		// explicit tag had better be value 2 - conditional-list:Response_Codes_List_Specifier

		ret = parseTag(pData, length,   tagValue,   tagLength, offset);

		if (tagValue != 2)
		{
			LOGIT(CERR_LOG,"Error: HART Response Codes Specifier only allows a tag value of 2.\n)");
			return -2;
		}
		
		conditional.coalescePayload = COALESCEPAYLOAD;
		return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        LOGIT(COUT_LOG,"%23s:\n","Response Codes Spec");   // Attribute name
        conditional.out(); 
        return 0; 
    };
    
	virtual void hydrateConditional(aCcondList* cond) 
	{
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	};
	
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrCmdRspCd* attr = new aCattrCmdRspCd;

		conditional.hydratePrototype(&attr->respCodes);// a aCcondList/*<aCrespCodeList>*/
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};
#endif //_ASCrespCodesSPECA_


