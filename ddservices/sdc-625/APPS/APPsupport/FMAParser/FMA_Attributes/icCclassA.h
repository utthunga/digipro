/*************************************************************************************************
 *
 * filename  icCclassA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Class Attribute class
 *
 */

#ifndef _icCclassA_
#define _icCclassA_

#include "..\FMx_Attributes\icCclass.h"

class icCclassA : public icCclass
{
public: // construct / destruct
	icCclassA() {}
	virtual ~icCclassA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCclassA_



