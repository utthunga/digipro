/*************************************************************************************************
 *
 * filename  asCaxisScalSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Axis Scale Specifier Attribute class
 *
 */

#ifndef _ASCaxisScalSPECA_
#define _ASCaxisScalSPECA_

#include "icCaxisScalA.h"
#include "..\FMA_conditional.h"

class asCaxisScalSpecA : public FMx_Attribute
{
public:
	asCaxisScalSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icCaxisScalA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };

	static asCaxisScalSpecA *generateDefault(unsigned atype, unsigned long val)
	{
		asCaxisScalSpecA *spec = new asCaxisScalSpecA(0);
		icCaxisScalA *ic = new icCaxisScalA();
		if (spec && ic)
		{
			ic->value = val;	// payload
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);	//  [8/14/2015 timj]
		}
		return spec;
	};

};
#endif //_ASCaxisScalSPECA_


