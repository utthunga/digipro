/*************************************************************************************************
 *
 * filename  asTransactionLstA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Transaction List Attribute class
 *
 */

#ifndef _ASTRANSACTIONLSTA_
#define _ASTRANSACTIONLSTA_

#include "..\FMx_Attributes\asTransactionLst.h"

class asTransactionLstA : public asTransactionLst
{
public:
	asTransactionLstA(int tag) : asTransactionLst(tag) { attname = "Transaction List";};

public:
	int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASTRANSACTIONLSTA_
