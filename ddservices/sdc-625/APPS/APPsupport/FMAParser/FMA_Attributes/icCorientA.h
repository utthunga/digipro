/*************************************************************************************************
 *
 * filename  icCorientA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Orientation Attribute class
 *
 */

#ifndef _icCorientA_
#define _icCorientA_

#include "..\FMx_Attributes\icCorient.h"

class icCorientA : public icCorient
{
public: // construct / destruct
	icCorientA() {}
	virtual ~icCorientA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCorientA_



