/*************************************************************************************************
 *
 * filename  faCStringA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A String Attribute class
 *
 */

#ifndef _FACSTRINGA_
#define _FACSTRINGA_

#include "..\FMA_Dict.h"
#include "..\FMx_Attributes\faCString.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class faCStringA : public faCString
{
public: // construct / destruct
	faCStringA()
	{
		strRef = new abCReferenceA;
	};

	virtual ~faCStringA()
	{
		//delete strRef;	// strRef is now deleted in base class ~faCString() [3/6/2014 timj]
	}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACSTRINGA_
