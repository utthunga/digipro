/*************************************************************************************************
 *
 * filename  faCMemberA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Member Attribute class
 *
 */

#ifndef _FACMEMBERA_
#define _FACMEMBERA_

#include "..\FMx_Attributes\faCMember.h"
#include "..\FMA_Attributes_Ref&Expr.h"
#include "faCStringA.h"

class faCMemberA : public faCMember
{
public: // construct 
	faCMemberA(int y = 0) : faCMember(y)	{};
	faCMemberA(const faCMemberA& elem) { operator=(elem); };
	faCMemberA& operator=(const faCMemberA& src);

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual ~faCMemberA() {};
	virtual void GetSymbolName(string &name);
};

class faCMemberListA : public vector<faCMemberA *>
{
public: // construct / destruct
	faCMemberListA() {}
	faCMemberListA(const faCMemberListA& src) { operator=(src); };
	virtual ~faCMemberListA() { destroy(); }
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 faCMemberListA&	operator=(const faCMemberListA& s);
};
#endif //_FACMEMBERA_
 