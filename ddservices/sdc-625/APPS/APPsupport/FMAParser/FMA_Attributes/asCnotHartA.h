/*************************************************************************************************
 *
 * filename  asCnotHartA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Not Hart Attribute class
 *
 */

#ifndef _ASCNOTHARTA_
#define _ASCNOTHARTA_

#include "..\FMx_Attributes\asCnotHart.h"

class asCnotHartA : public asCnotHart
{
public:
	asCnotHartA(int tag) : asCnotHart(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCNOTHARTA_
