/*************************************************************************************************
 *
 * filename  icCgridMemberA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Grid Member Attribute
 *
 */

#include "icCgridMemberA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Attributes\faCStringA.h"
#include "tags_sa.h"

/*
Grid_Member ::= [STRUCTURE_TAG 81]
SEQUENCE  {
	explicit-tag	Explicit_Tag, -- tag = 81
	description		String,
	values			Reference_List
	}
}
*/

int icCgridMemberA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, GRID_MEMBER_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;


	description = new faCStringA();
	if (description == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = description->evaluateAttribute(pData, length, offset))
		goto end;


	if (ret = parseExpectedExplicitTag(pData, length, offset, REFERENCE_LIST_TAG_A, tagLength))
		goto end;

	workingLength	= tagLength;
	while (workingLength > 0)
	{
		abCReferenceA *ref = new abCReferenceA();
		if (ref == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = ref->evaluateAttribute(pData, workingLength, offset))
			goto end;

		values.push_back(ref);
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}




//=============================================================================================


int icCgridMemberListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCgridMemberA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCgridMemberA *pElem;

	while(length > 4)
	{
		pElem = new icCgridMemberA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCgridMemberListA::destroy()
{
	icCgridMemberA *pElem = NULL;
	vector<icCgridMemberA *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCgridMemberListA::out(void)
{
	icCgridMemberA *pElem = NULL;
	vector<icCgridMemberA *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCgridMemberListA::generatePayload(aPayldType*payld)
{	
	aCgridMemberList  *pPlst;
	if (payld)
	{
		pPlst = (aCgridMemberList  *) payld;
	}
	else
	{
		pPlst = new aCgridMemberList;
	}

	aCgridMemberElement  locElem;
	icCgridMemberA *pElem = NULL;
	vector<icCgridMemberA *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCgridMemberListA& icCgridMemberListA::operator=(const icCgridMemberListA& s)
{
	icCgridMemberA *pElem = NULL, *pnewElem = NULL;
	vector<icCgridMemberA *>::const_iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCgridMemberA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}