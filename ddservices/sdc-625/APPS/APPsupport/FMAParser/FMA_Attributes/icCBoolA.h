/*************************************************************************************************
 *
 * filename  icCBoolA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A BOOL Attribute class
 *
 */

#ifndef _ICCBOOLA_
#define _ICCBOOLA_

#include "..\FMx_Attributes\icCBool.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class icCBoolA : public icCBool
{
public: // construct / destruct
	icCBoolA() {}
	virtual ~icCBoolA() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ICCBOOLA_
