/*************************************************************************************************
 *
 * filename  asCuuidA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A UUID Attribute
 *
 */

#include "asCuuidA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
UUIdentifier ::= [STRUCTURE_TAG 56]
SEQUENCE 
{
  implicit-tag 		Implicit_Tag, 	 -- tag = 56
  upper_half 		INTEGER,        -- leftmost 16 hex digits
  lower_half 		INTEGER         -- rightmost 16 hex digits
}
*/

int asCuuidA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	if (ret = parseExpectedImplicitTag(pData, length, offset, UUID_TAG_A))
		return ret;

	if (ret = parseUINT64(pData, length, offset, upper_half))
		goto end;

	if (ret = parseUINT64(pData, length, offset, lower_half))
		goto end;

end:

	return ret;
}


