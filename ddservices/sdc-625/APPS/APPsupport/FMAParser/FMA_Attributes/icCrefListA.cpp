/*************************************************************************************************
 *
 * filename  icCrefListA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Reference List Attribute
 *
 */

#include "icCrefListA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Attributes_Ref&Expr.h"
#include "tags_sa.h"

/*
Reference_List ::= [STRUCTURE_TAG 90]
SEQUENCE
{
	explicit_tag	Explicit_Tag, 		-- tag = 90
	SEQUENCE OF Reference
}
*/

int icCrefListElemA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{//isa vector<abCReference*>
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, REFERENCE_LIST_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	abCReferenceA *pElem;

	while(length > 4)
	{
		pElem = new abCReferenceA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			goto end;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend


end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}




//=============================================================================================


int icCrefListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCrefListElemA>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCrefListElemA *pElem;

	while(length > 4)
	{
		pElem = new icCrefListElemA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

    return ret;
}


void icCrefListA::destroy()
{
	icCrefListElemA *pElem = NULL;
	vector<icCrefListElemA *>::iterator iT;//ptr2ptr2icCrefListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	vector<icCrefListElemA*>::clear();//self vector
}

int icCrefListA::out(void)
{
	icCrefListElemA *pElem = NULL;
	vector<icCrefListElemA *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next

	return 0;
}


aPayldType* icCrefListA::generatePayload(aPayldType*payld)
{	
	aCreferenceList  *pPlst;
	if (payld)
	{
		pPlst = (aCreferenceList  *) payld;
	}
	else
	{
		pPlst = new aCreferenceList;
	}

	aCreference  locElem;
	icCrefListElemA *pElem = NULL;
	vector<icCrefListElemA *>::iterator iT;//ptr2ptr2icCrefListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generatePayload(pPlst);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCrefListA& icCrefListA::operator=(const icCrefListA& s)
{
	icCrefListElemA *pElem = NULL, *pnewElem = NULL;
	vector<icCrefListElemA *>::const_iterator iT;//ptr2ptr2icCrefListElemA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCrefListElemA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
