/*************************************************************************************************
 *
 * filename  asCintegerConstA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Integer Constant Attribute class
 *
 */

#ifndef _ASCINTEGERCONSTA_
#define _ASCINTEGERCONSTA_

#include "..\FMx_Attributes\asCintegerConst.h"

class asCintegerConstA : public asCintegerConst
{
public:
	asCintegerConstA(int tag) : asCintegerConst(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCINTEGERCONSTA_
