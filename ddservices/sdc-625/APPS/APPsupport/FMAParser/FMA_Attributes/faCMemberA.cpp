/*************************************************************************************************
 *
 * filename  faCMemberA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Member Attribute
 *
 */

#include "logging.h"
#include "faCMemberA.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"
#include "tags_sa.h"
#include "asCasciiStrA.h"
#include "..\FMA_Support_tables.h"
#include "..\FMA_Support_rows.h"
#include "..\FMA_defs.h"

/*
	Member ::= [STRUCTURE_TAG 83]
	SEQUENCE 
	{
	explicit-tag	Explicit_Tag,	-- tag = 83
	SEQUENCE  
	{
		name				INTEGER,  -- member�s numeric key
		member-symbol-index	INTEGER,  -- index into Symbol-Table (for name)
		item				Reference,-- reference is restricted by item-type
		description			String (OPTIONAL),-- required when help string 
		help				String (OPTIONAL) 
		}
	}
*/

void faCMemberA::GetSymbolName(string &name)
{
	int lt; // throw away
	ConvertToString::fetchSymbolNameByIndex(memberSymbolIndex, name, lt);
}

int faCMemberA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned tagLength;
	RETURNCODE  ret = 0;
	CValueVarient v;

	if (ret = parseExpectedExplicitTag(pData, length, offset, MEMBER_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, memberID))
		goto end;

	if (ret = parseUnsigned(pData, workingLength, offset, memberSymbolIndex))
		goto end;

	itemRef = new abCReferenceA();
	if (itemRef == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = itemRef->evaluateAttribute(pData, workingLength, offset))
		goto end;


	if (workingLength > 0)
	{
		strDesc = new faCStringA();
		if (strDesc == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = strDesc->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

	if (workingLength > 0)
	{
		strHelp = new faCStringA();
		if (strHelp == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = strHelp->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}


faCMemberA& faCMemberA::operator=( const faCMemberA& src )
{
	faCMember::operator=(src);
	return *this;
}



//=============================================================================================


int faCMemberListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<faCMemberA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	faCMemberA *pElem;

	while(length > 4)
	{
		pElem = new faCMemberA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void faCMemberListA::destroy()
{
	faCMemberA *pElem = NULL;
	vector<faCMemberA *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void faCMemberListA::out(void)
{
	faCMemberA *pElem = NULL;
	vector<faCMemberA *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* faCMemberListA::generatePayload(aPayldType*payld)
{	
	aCmemberElementList  *pPlst;
	if (payld)
	{
		pPlst = (aCmemberElementList  *) payld;
	}
	else
	{
		pPlst = new aCmemberElementList;
	}

	aCmemberElement  locElem;
	faCMemberA *pElem = NULL;
	vector<faCMemberA *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


faCMemberListA& faCMemberListA::operator=(const faCMemberListA& s)
{
	faCMemberA *pElem = NULL, *pnewElem = NULL;
	vector<faCMemberA *>::const_iterator iT;//ptr2ptr2faCMemberA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new faCMemberA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
