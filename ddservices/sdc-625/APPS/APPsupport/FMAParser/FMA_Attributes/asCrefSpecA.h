/*************************************************************************************************
 *
 * filename  asCrefSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Reference Specifier Attribute class
 *
 */

#ifndef _ASCrefSPECA_
#define _ASCrefSPECA_

#include "FMA_Attributes_Ref&Expr.h"
#include "..\FMA_conditional.h"

class asCrefSpecA : public FMx_Attribute
{
public:
	asCrefSpecA(int tag, bool direct = false, bool forceDir = false) : FMx_Attribute(tag) 
	{
		// if direct == true, then the companion FM8 attr is not conditional, so
		// the conditional.out() skips right to the payload.out()
		conditional.requiredDirect = direct;
		// forceDirect is true when the attribute has to be conditional but the encoding is not
		// it hard code sets the  type as DIRECT and then evaluates the payload.
		conditional.forceDirect    = forceDir;
		// not required ... forceDirect to be used::> force = forceDir;
	};
    
public:
	// not required ... forceDirect to be used::> bool force;
	fC_A_Conditional<abCReferenceA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		/* not required ... forceDirect to be used::>
		if (force == true)
		{
			conditional.conditionalType = cdt_DIRECT;
			abCReferenceA* pattr = new abCReferenceA;
			conditional.pPayload = (FMx_Attribute*)pattr;

			return pattr->evaluateAttribute(pData, length, offset);
		}
		**/
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
        aCattrCondReference* attr = new aCattrCondReference;
		conditional.hydratePrototype(&attr->condRef);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCrefSPECA_


