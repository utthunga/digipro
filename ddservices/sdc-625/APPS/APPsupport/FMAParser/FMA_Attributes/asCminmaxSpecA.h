/*************************************************************************************************
 *
 * filename  asCminmaxSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Min Max Attribute class
 *
 */

#ifndef _ASCminmaxSPECA_
#define _ASCminmaxSPECA_

#include "icCminmaxA.h"

class asCminmaxSpecA : public FMx_Attribute
{
public:
	asCminmaxSpecA(int tag) : FMx_Attribute(tag) {};

public:
	vector<icMinMaxA> minmaxlist;	// HART doesn't do a conditional list here [3/17/2014 timj]

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		unsigned tagLength;
		int ret;

		if (ret = parseExpectedExplicitTag(pData, length, offset, OBJECT_TAG, tagLength))
		{
			LOGIT(CERR_LOG,"Error: MinMaxValue DIRECT tag expected.\n)");
			return ret;
		}
		int			exitLength		= length   - tagLength;
		unsigned	exitOffset		= offset   + tagLength;
		UCHAR	   *exitPdata		= (*pData) + tagLength;
		int			workingLength	= tagLength;

		while (tagLength > 0)
		{
			icMinMaxA minmax;

			if (ret = minmax.evaluateAttribute(pData, (int&)tagLength, offset))
				goto end;

			minmaxlist.push_back(minmax);
		}

end:
		*pData = exitPdata;
		length = exitLength;
		offset = exitOffset;

		return ret;
	};

	virtual void destroy(void) 
	{ 
//		conditional.destroy(); //  [3/17/2014 timj]

		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].destroy();
		}
	};

	virtual int out(void) 
	{
		FMx_Attribute::out();  

		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].out();
		}
		return 0; 
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrVarMinMaxList* attr = new aCattrVarMinMaxList;
		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].generateElement(&(attr->ListOminMaxElements));
		}
		
		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};


#endif //_ASCminmaxSPECA_


