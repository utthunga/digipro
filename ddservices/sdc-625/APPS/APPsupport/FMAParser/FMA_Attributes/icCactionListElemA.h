/*************************************************************************************************
 *
 * filename  icCactionListElemA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Action List Element Attribute class
 *
 */

#ifndef _icCactionListElemA_
#define _icCactionListElemA_

#include "..\FMA_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\icCactionListElem.h"
#include "..\FMA_Attributes\faCByteStringA.h"

class icCactionListElemA : public icCactionListElem
{
public: // construct / destruct
	icCactionListElemA() 
	{
		ref = new abCReferenceA();
		//method = new faCByteStringA();...Error: Inline method definitions are illegal in HART.
	};
	icCactionListElemA(const icCactionListElemA &src) { operator=(src);};
	virtual ~icCactionListElemA() {};

    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCactionListElemA& operator=(const icCactionListElemA& src)
	{
		choice = src.choice;
		if (src.ref)
		{
			abCReferenceA *r = (abCReferenceA*) src.ref;
			ref = new abCReferenceA(*r);
		}
		return *this;
	};
};


class icCactionListA : public vector<icCactionListElemA *>
{
public: // construct / destruct
	icCactionListA() {};
	icCactionListA(const icCactionListA& src) { operator=(src); };
	virtual ~icCactionListA() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCactionListA&	operator=(const icCactionListA& s);
};

#endif //_icCactionListElemA_



