/*************************************************************************************************
 *
 * filename  icCgridMemberA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Grid Member Attribute class
 *
 */

#ifndef _icCgridMemberA_
#define _icCgridMemberA_

#include "..\FMx_Attributes\icCgridMember.h"

class icCgridMemberA : public icCgridMember
{
public: // construct / destruct
	icCgridMemberA() : icCgridMember() {};
	icCgridMemberA(const icCgridMemberA& src) { operator=(src); };
	virtual ~icCgridMemberA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};


class icCgridMemberListA : public vector<icCgridMemberA *>
{
public: // construct / destruct
	icCgridMemberListA() {};
	icCgridMemberListA(const icCgridMemberListA& src) { operator=(src); };
	virtual ~icCgridMemberListA() { destroy(); };
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCgridMemberListA&	operator=(const icCgridMemberListA& s);
};

#endif //_icCgridMemberA_



