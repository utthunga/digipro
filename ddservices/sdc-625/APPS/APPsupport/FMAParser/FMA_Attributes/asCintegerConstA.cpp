/*************************************************************************************************
 *
 * filename  asCintegerConstA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Integer Constant Attribute
 *
 */

#include "logging.h"
#include "asCintegerConstA.h"
#include "..\FMx_Support_primatives.h"

int asCintegerConstA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned tagLength;
	unsigned tagValue;
	RETURNCODE  ret = 0;

	if (expectedTag > 0)
	{
		if (expectedTag & 0x80)
		{
			tagValue = expectedTag & 0x7f;
			if (ret = parseExpectedExplicitTag(pData, length, offset, tagValue, tagLength))
				return ret;
		}
		else
		{
			PARSE_EXPECTED_IMPLICIT_TAG(expectedTag, ret);
		}
	}

	value = 0;
	unsigned x = 0;
	ret = parseUnsigned(pData, length, offset, x);
	value = (int) x;
    
    return ret;
}

