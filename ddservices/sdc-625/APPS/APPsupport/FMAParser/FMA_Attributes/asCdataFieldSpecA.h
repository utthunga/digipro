/*************************************************************************************************
 *
 * filename  asCdataFieldSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Data Field Specifier Attribute class
 *
 */

#ifndef _ASCDATAFIELDSPECA_
#define _ASCDATAFIELDSPECA_


#include "..\FMA_conditional.h"
#include "..\FMx_Attributes\asCdataFieldSpec.h"
#include "icCdataElemA.h"

class asCdataFieldSpecA : public asCdataFieldSpec
{
public:
	asCdataFieldSpecA(int tag = 0) : asCdataFieldSpec(tag) {};

public:
	
	fC_A_condList<icCdataElemListA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		if (reconcile == false)
		{
			LOGIT(COUT_LOG,"%23s:\n","Data Field Spec");   // Attribute name
		}
		conditional.out();  
		return 0;  
	};

	virtual void hydrateConditional(aCcondList* cond)
	{ 
		conditional.hydratePrototype(cond);
	}
};

#endif //_ASCDATAFIELDSPECA_