/*************************************************************************************************
 *
 * filename  asCchartTypeSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Chart Type Specifier Attribute class
 *
 */

#ifndef _ASCchartTypeSPECA_
#define _ASCchartTypeSPECA_

#include "icCchartTypeA.h"
#include "..\FMA_conditional.h"

class asCchartTypeSpecA : public FMx_Attribute
{
public:
	asCchartTypeSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icCchartTypeA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };

	static asCchartTypeSpecA *generateDefault(unsigned atype, int unsigned val = 4)
	{
		asCchartTypeSpecA *spec = new asCchartTypeSpecA(0);
		icCchartTypeA *ic = new icCchartTypeA();
		if (spec && ic)
		{
			ic->value = val;	// payload
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);	//  [8/14/2015 timj]
		}
		return spec;
	};
};
#endif //_ASCchartTypeSPECA_


