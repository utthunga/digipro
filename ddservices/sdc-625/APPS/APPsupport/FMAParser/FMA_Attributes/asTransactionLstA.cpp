/*************************************************************************************************
 *
 * filename  asTransactionLstA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Transaction List Attribute
 *
 */


#include "logging.h"
#include "asTransactionLstA.h"
#include "icCtransactionA.h"
#include "..\FMx_Attributes\asTransactionLst.h"
#include "..\FMx_Support_primatives.h"

#include "tags_sa.h"


int asTransactionLstA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned   oneTransLen = 0;
	int          iTransLen = 0;
	RETURNCODE ret = 0;

	int			exitLength		= 0;
	unsigned	exitOffset		= offset   + length;
	UCHAR	   *exitPdata		= (*pData) + length;

	while (length > 0) // for each transaction
	{
		ret = parseExpectedExplicitTag(pData, length, offset, TRANSACTION_TAG_A, oneTransLen);

		if ( ret || oneTransLen == 0)
		{
			LOGIT(CERR_LOG,"Error: Transaction attribute tag/length is wrong.\n)");
			ret = -1;
			goto end;
		}

		icCtransactionA* pTrans = new icCtransactionA;
		if (pTrans == NULL)
		{
			LOGIT(CERR_LOG,"Error: memory on icCtransactionA.\n)");
			ret = -2;
			goto end;
		}

		length -= oneTransLen;
		iTransLen=oneTransLen;
		
		ret = pTrans->evaluateAttribute(pData, iTransLen, offset);

		if (ret || iTransLen != 0) 
		{
			LOGIT(CERR_LOG,"Error: Transaction parse failure.\n");
			delete pTrans;
			// continue to next transaxtion
		}
		else
		{
			transactionList.push_back(pTrans);
		}
	}//wend length of transaction attribute

end:
	if (exitPdata != (*pData))
	{
		LOGIT(CERR_LOG,"Transaction list pointer out of sync.\n");
	}
	if (exitLength != length)
	{
		LOGIT(CERR_LOG,"Transaction list ending length is incorrect.\n");
	}
	if(exitOffset != offset)
	{
		LOGIT(CERR_LOG,"Transaction list ending offset is incorrect.\n");
	}

	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}
