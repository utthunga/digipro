/*************************************************************************************************
 *
 * filename  asCexprA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Expression Attribute class
 *
 */

#ifndef _ASCEXPRA_
#define _ASCEXPRA_

#include "..\FMx_Attributes\asCexpr.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class asCexprA : public asCexpr<abCExpressionA>
{
public:
	asCexprA(int tag = 0) : asCexpr(tag) 
	{
		//pExpr = new abCExpressionA;
	};

	virtual ~asCexprA()
	{
		// done in parent...delete pExpr;
	};

	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset)
	{
		return Expr.evaluateAttribute(pData, length, offset);
	};

	virtual bool inhibitOut()
	{
		return (reconcile && attrType==ag_ValArr_length) ? true : false; 
	};

};

#endif //_ASCEXPRA_