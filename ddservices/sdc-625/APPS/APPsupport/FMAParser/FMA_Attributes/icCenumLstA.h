/*************************************************************************************************
 *
 * filename  icCenumLstA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A ENum List Attribute class
 * Definition for version A Status Classes Attribute class
 *
 */

#ifndef _icCenumLstA_
#define _icCenumLstA_

#include "..\FMx_Attributes\icCenumLst.h"

	
typedef struct
{
	unsigned baseClass;  // 0-24
	outputClass_t oc;
} classPair_t;			

class icCstatusClassesA : public icCstatusClasses
{
public: // construct 
	icCstatusClassesA() {};   // initialize underlying class data
    
	vector<classPair_t> classPairs;

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);

	virtual int out(void);		// special output for Enumerated

};

class icCenumElemA : public icCenumElem
{
public: // construct / destruct
	icCenumElemA() {}
	icCenumElemA(const icCenumElemA& x) { operator=(x); };
	// destroy() : use parent class implementation
	virtual ~icCenumElemA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCenumElemA& operator=(const icCenumElemA& src);
};

// refactoring [3/11/2014 timj]
// old				=>	new
// icCenumLst		=>	iccEnumElem
// icCenumLst?		=>	iccEnumElem?
//
// new class icCenumLst : public vector<icCenumElemA *>
//
// sooo an Enumerated has a list of Elements (values) 
// and an Element has fields.

class icCenumLstA : public vector<icCenumElemA *>
{
public: // construct / destruct
	icCenumLstA() {}
	icCenumLstA(const icCenumLstA& src) { operator=(src); };
	virtual ~icCenumLstA() { destroy(); }

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType* payld = 0);

public: // workers
	icCenumLstA&	operator=(const icCenumLstA& s);
};

#endif //_icCenumLstA_



