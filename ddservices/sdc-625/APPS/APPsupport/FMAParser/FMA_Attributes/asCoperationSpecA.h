/*************************************************************************************************
 *
 * filename  asCoperationSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Operation Specifier Attribute class
 *
 */

#ifndef _ASCoperationSPECA_
#define _ASCoperationSPECA_

#include "..\FMA_Attributes\asCoperationA.h"
#include "..\FMx_Attributes\asCoperationSpec.h"
#include "..\FMA_conditional.h"

class asCoperationSpecA : public asCoperationSpec
{
public:
	asCoperationSpecA(int tag) : asCoperationSpec(tag) {};
    
public:
	fC_A_Conditional<icCoperationA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(id);

		return attr;
    };
}
#endif //_ASCoperationSPECA_


