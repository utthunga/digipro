/*************************************************************************************************
 *
 * filename  icCmenuItmA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Menu Item Attribute class
 *
 */

#ifndef _icCmenuItmA_
#define _icCmenuItmA_

#include "..\FMx_Attributes\icCmenuItm.h"
#include "..\FMA_conditional.h"


class icCmenuItmA : public icCmenuItm
{
public: // construct / destruct
	icCmenuItmA() {}
	icCmenuItmA(const icCmenuItmA& mI) { operator=(mI); };
	virtual ~icCmenuItmA() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCmenuItmA& operator=(const icCmenuItmA& src);
};


class icCmenuItmListA : public vector<icCmenuItmA *>
{
public: // construct / destruct
	icCmenuItmListA() {}
	icCmenuItmListA(const icCmenuItmListA& src) { operator=(src); };
	virtual ~icCmenuItmListA() { destroy(); }
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCmenuItmListA&	operator=(const icCmenuItmListA& s);
};



#endif //_icCmenuItmA_



