/*************************************************************************************************
 *
 * filename  icChandlingA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Handling Attribute
 *
 */

#include "icChandlingA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

#include "faCBitStringA.h"

int icChandlingA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned) length)
	{
		LOGIT(CERR_LOG,"Error: asChandlingSpecA attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != HANDLING_TAG_A)
	{
		LOGIT(CERR_LOG,"Error: asChandlingSpecA attribute ASCII string explicit tag is not 61.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		faCBitStringA bitstring;
		if (bitstring.evaluateAttribute(pData, length, offset) != 0)
		{
			return -3;
		}

		value = bitstring.value;
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Handling attribute needs size but has no length.\n)");
		ret = -4;
	}

	return ret;
}


