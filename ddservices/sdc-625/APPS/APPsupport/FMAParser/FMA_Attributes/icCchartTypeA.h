/*************************************************************************************************
 *
 * filename  icCchartTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Chart Type Attribute class
 *
 */

#ifndef _icCchartTypeA_
#define _icCchartTypeA_

#include "..\FMx_Attributes\icCchartType.h"

class icCchartTypeA : public icCchartType
{
public: // construct / destruct
	icCchartTypeA() {}
	virtual ~icCchartTypeA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCchartTypeA_



