/*************************************************************************************************
 *
 * filename  icClineTypeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Line Type Attribute
 *
 */

#include "icClineTypeA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Line_Type ::= [STRUCTURE_TAG 59]
SEQUENCE 
{
	explicit-tag	Explicit_Tag, -- 59
	implicit-tag	Implicit_Tag,
	CHOICE    <as designated by implicit-tag.implicit-value>
	{
		data-n				[1]	INTEGER,
		low-low-limit		[2]	NULL,
		low-limit			[3]	NULL,
		high-limit			[4]	NULL,
		high-high-limit		[5]	NULL,
		transparent			[6]	NULL
	}
}
*/
																

int icClineTypeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	if (ret = parseExpectedExplicitTag(pData, length, offset, LINE_TYPE_TAG_A, tagLength))
		return ret;

	ret = parseUnsigned(pData, length, offset, value);		// int

	if (value == DATA_LINETYPE)
		ret = parseUnsigned(pData, length, offset, choice);	// which
	else
		choice = 0;

    return ret;
}


