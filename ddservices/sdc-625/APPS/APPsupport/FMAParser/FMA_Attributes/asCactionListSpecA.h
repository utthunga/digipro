/*************************************************************************************************
 *
 * filename  asCactionListSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Action List Specifier Attribute class
 *
 */

#ifndef _ASCactionListSPECA_
#define _ASCactionListSPECA_

#include "..\FMA_Attributes\icCactionListElemA.h"

#include "..\FMA_conditional.h"
#include "..\FMx_Attributes\asCactionListSpec.h"

class asCactionListSpecA : public asCactionListSpec
{
public:
	asCactionListSpecA(int tag) : asCactionListSpec(tag) {};
    
public:
	
    fC_A_condList<icCactionListA> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		conditional.coalescePayload = COALESCEPAYLOAD;
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		if (reconcile == false)
        {
	        LOGIT(COUT_LOG,"%23s:\n","Action List Spec");   // Attribute name
        }
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrActionList* list = new aCattrActionList;
		conditional.hydratePrototype(&list->RefList);
		isConditional = conditional.isConditional();

		list->attr_mask = maskFromInt(attrType);

		return list;
	};
};

#endif //_ASCactionListSPECA_


