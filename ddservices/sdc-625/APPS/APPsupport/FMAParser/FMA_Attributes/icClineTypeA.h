/*************************************************************************************************
 *
 * filename  icClineTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Line Type Attribute class
 *
 */

#ifndef _icClineTypeA_
#define _icClineTypeA_

#include "..\FMx_Attributes\icClineType.h"

class icClineTypeA : public icClineType
{
public: // construct / destruct
	icClineTypeA() {}
	virtual ~icClineTypeA() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icClineTypeA_



