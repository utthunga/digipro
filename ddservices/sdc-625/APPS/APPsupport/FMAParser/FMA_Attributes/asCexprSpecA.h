/*************************************************************************************************
 *
 * filename  asCexprSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Expression Specifier Attribute class
 *
 */

#ifndef _ASCEXPRSPECA_
#define _ASCEXPRSPECA_

#include "..\FMA_conditional.h"
#include "asCexprA.h"
#include "..\FMx_Attributes\asCexprSpec.h"

class asCexprSpecA : public asCexprSpec
{
public:
	asCexprSpecA(int tag = 0) : asCexprSpec(tag) {};
	asCexprSpecA(const asCexprSpecA& src) { operator=(src); };
	virtual ~asCexprSpecA() { clear();};
	asCexprSpecA& operator=(const asCexprSpecA& s) 
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&s))); 
		conditional = s.conditional;
		return *this;
	};

public:
	fC_A_Conditional<asCexprA> conditional;

public:
	void clear(){ conditional.destroy(); };
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondExpr* attr = new aCattrCondExpr;
		conditional.hydratePrototype(&attr->condExpr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};

	void hydratePrototype(aCondDestType* cond)
	{
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	}

	static asCexprSpecA *generateDefault(unsigned atype, int unsigned val)
	{
		asCexprSpecA *spec = new asCexprSpecA(0);
		asCexprA *ic = new asCexprA();
		if (spec && ic)
		{
			abCExpressionA expr(val);
			ic->Expr = expr;
			spec->attrType = atype;
			spec->conditional.MakeDirect(ic);	//  [8/14/2015 timj]

		}
		return spec;
	}

};

#endif //_ASCEXPRSPECA_