/*************************************************************************************************
 *
 * filename  asClineTypeSpecA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Line Type Specifier Attribute class
 *
 */

#ifndef _ASClineTypeSPECA_
#define _ASClineTypeSPECA_

#include "..\FMA_Attributes\icClineTypeA.h"
#include "..\FMA_conditional.h"

class asClineTypeSpecA : public FMx_Attribute
{
public:
	asClineTypeSpecA(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_A_Conditional<icClineTypeA> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrCondIntWhich* attr = new aCattrCondIntWhich;
		conditional.hydratePrototype(&attr->condIntWhich);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};
#endif //_ASClineTypeSPECA_


