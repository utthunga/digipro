/*************************************************************************************************
 *
 * filename  asCwaveTypeA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Wave Type Attribute class
 *
 */

#ifndef ASCWAVETYPEA
#define ASCWAVETYPEA

#include "..\FMx_Attributes\asCwaveType.h"

class asCwaveTypeA : public asCwaveType
{
public: // construct
	asCwaveTypeA(int tag) : asCwaveType(tag) {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //ASCWAVETYPEA
