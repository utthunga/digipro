/*************************************************************************************************
 *
 * filename  faCBitStringA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Bit String Attribute
 *
 */

#include "logging.h"
#include "faCBitStringA.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"

int faCBitStringA::evaluateAttribute(UCHAR** pData, int& size, unsigned& offset)
{
	RETURNCODE ret = parseBitString(pData, size, rawValue, offset);
	value = ConvertTo8::classA28(rawValue);
	return ret;
}

