/*************************************************************************************************
 *
 * filename  faCBitStringA.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Bit String Attribute class
 *
 */

#ifndef _FACBITSTRINGA_
#define _FACBITSTRINGA_

#include "..\FMx_Attributes\faCBitString.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class faCBitStringA : public faCBitString
{
public: // construct / destruct
	faCBitStringA() {}
	virtual ~faCBitStringA() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACBITSTRINGA_
