/*************************************************************************************************
 *
 * filename  icCrespCodeA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Response Code Attribute
 *
 */

#include "icCrespCodeA.h"
#include "faCStringA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Response_Code ::= [STRUCTURE_TAG 92]
SEQUENCE  {
	explicit-tag	Explicit_Tag,	-- tag = 92
	SEQUENCE  {
		value				INTEGER,
		type				Response_Code_Type,
		description			String,
		help				String  (OPTIONAL)
	}
}
*/

int icCrespCodeA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, RESPONSE_CODE_TAG_A, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	// value

	if (ret = parseUnsigned(pData, workingLength, offset, value))
		goto end;

	// type

	if (ret = parseUnsigned(pData, workingLength, offset, type))
		goto end;

	type = ConvertTo8::responseCodeType(type);

	// description

	description = new faCStringA();
	if (description == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = description->evaluateAttribute(pData, workingLength, offset))
		goto end;

	// optional help

	if (workingLength == 0)
		goto end;	

	help = new faCStringA();
	if (help == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = help->evaluateAttribute(pData, workingLength, offset))
		goto end;

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}





//=============================================================================================


int icCrespCodeListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCrespCodeA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCrespCodeA *pElem;

	while(length > 4)
	{
		pElem = new icCrespCodeA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCrespCodeListA::destroy()
{
	icCrespCodeA *pElem = NULL;
	vector<icCrespCodeA *>::iterator iT;//ptr2ptr2icCrespCodeA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCrespCodeListA::out(void)
{
	icCrespCodeA *pElem = NULL;
	vector<icCrespCodeA *>::iterator iT;//ptr2ptr2icCrespCodeA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


aPayldType* icCrespCodeListA::generatePayload(aPayldType*payld)
{	
	aCrespCodeList  *pPlst;
	if (payld)
	{
		pPlst = (aCrespCodeList  *) payld;
	}
	else
	{
		pPlst = new aCrespCodeList;
	}

	icCrespCodeA *pElem = NULL;
	vector<icCrespCodeA *>::iterator iT;//ptr2ptr2icCrespCodeA
	for( iT = begin(); iT != end(); ++iT)
	{
		aCrespCode  locRespCode;
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locRespCode);
			pPlst->push_back(locRespCode);
		}
	}//next

	return pPlst;
}


icCrespCodeListA& icCrespCodeListA::operator=(const icCrespCodeListA& s)
{
	icCrespCodeA *pElem = NULL, *pnewElem = NULL;
	vector<icCrespCodeA *>::const_iterator iT;//ptr2ptr2icCrespCodeA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCrespCodeA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}