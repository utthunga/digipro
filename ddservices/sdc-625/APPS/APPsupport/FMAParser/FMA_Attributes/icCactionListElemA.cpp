/*************************************************************************************************
 *
 * filename  icCactionListElemA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Action Element List Attribute
 *
 */

#include "icCactionListElemA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
*/

int icCactionListElemA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, LIST_ITEMS_ATT_NAME, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	ret = parseUnsigned(pData, length, offset, choice);

	if (choice == 0)
	{
		ret = ref->evaluateAttribute(pData, length, offset);
	}

	else if (choice == 1)
	{
		// ...Error: Inline method definitions are illegal in HART.
		LOGIT(CERR_LOG,"Error: Inline method definitions are illegal in HART.\n");
		goto end;
	}

	else
	{
		LOGIT(CERR_LOG,"Error: %s attribute error in choice value %d is should be on range [0, 1].\n)", attname, choice);
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;
  
    return ret;
}




//=============================================================================================


int icCactionListA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCactionListElemA*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCactionListElemA *pElem;

	while(length > 4)
	{
		pElem = new icCactionListElemA;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCactionListA::destroy()
{
	icCactionListElemA *pElem = NULL;
	vector<icCactionListElemA *>::iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCactionListA::out(void)
{
	icCactionListElemA *pElem = NULL;
	vector<icCactionListElemA *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCactionListA::generatePayload(aPayldType*payld)
{	
	aCreferenceList  *pPlst;
	if (payld)
	{
		pPlst = (aCreferenceList  *) payld;
	}
	else
	{
		pPlst = new aCreferenceList;
	}

	aCreference  locElem;
	icCactionListElemA *pElem = NULL;
	vector<icCactionListElemA *>::iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCactionListA& icCactionListA::operator=(const icCactionListA& s)
{
	icCactionListElemA *pElem = NULL, *pnewElem = NULL;
	vector<icCactionListElemA *>::const_iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCactionListElemA(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}