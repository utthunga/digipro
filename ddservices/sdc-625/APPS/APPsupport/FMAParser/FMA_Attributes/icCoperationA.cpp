/*************************************************************************************************
 *
 * filename  icCoperationA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Operation Attribute
 *
 */

#include "icCoperationA.h"
#include "logging.h"
#include "..\Convert.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Operation ::=  
ENUMERATED (size 1)
{
	command		(0), 
	read 		(1), 
	write 		(2), 
	exchange	(3) 
}
*/

int icCoperationA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE ret = 0;

	value = 0;
	ret = parseUnsigned(pData, length, offset, rawValue);
	value = ConvertTo8::operation(rawValue);

	return ret;
}


