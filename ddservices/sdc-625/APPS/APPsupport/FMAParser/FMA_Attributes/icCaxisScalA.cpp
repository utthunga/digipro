/*************************************************************************************************
 *
 * filename  icCaxisScalA.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version A Axis Scale Attribute
 *
 */

#include "icCaxisScalA.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"
#include "..\convert.h"

/*
Axis_Scaling ::= [STRUCTURE_TAG 64]
SEQUENCE
{
	implicit-tag		Implicit_Tag,	-- tag = 64
	ENUMERATED (size 1)
	{
		linear			(0),
		logarithmic		(1)
	}
}
*/

int icCaxisScalA::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned) length)
	{
		LOGIT(CERR_LOG,"Error: asCaxixScalSpecA attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != AXIS_SCALING_TAG_A)
	{
		LOGIT(CERR_LOG,"Error: asCaxisScalSpecA attribute explicit tag is not 64.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Axis Scaling attribute error in size extraction.\n)");
		}
		else
		{
			rawValue = (int)tempLongLong;
			value = ConvertTo8::axisScaling(rawValue);
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Axis Scaling attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}


