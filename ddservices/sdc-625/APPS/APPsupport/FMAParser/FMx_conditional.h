/**********************************************************************************************
 *
 * FMx_conditional.h 
 * 24oct13 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_conditional.h : This is the common code for all SDC style conditionals.
            The expression class is now passed in so the rest of the code can be generic.
 *
 * #include "FMx_conditional.h"
 *
 */

#ifndef _FMA_FMx_CONDITIONAL_H 
#define _FMA_FMx_CONDITIONAL_H

#ifdef INC_DEBUG

 #pragma message("In FMx_conditional.h") 

#endif

#include "FMx_defs.h"

#ifdef INC_DEBUG

 #pragma message("    Finished includes from FMx_conditional.h") 

#endif

extern char axiomStrings  [5][AXIOMSTRMAXLEN]  ;
extern char clauseStrings[8][CLAUSESTRMAXLEN];

/* these appear to be included 
typedef enum axiomType_e
{
		aT_Unknown = 0,
		aT_IF_expr,     // 1
		aT_SEL_expr,	// 2
		aT_Direct  = 4
}
//typedef
axiomType_t;

typedef enum clauseType_e
{
		cT_Unknown   = 0,
		cT_CASE_expr = 3,	// implies there is a clause expression
		// the rest imply no clause expression
		cT_IF_isTRUE = 5,  // 5 THEN
		cT_IF_isFALSE,     // 6 ELSE
		ct_SEL_isDEFAULT   // 7 always TRUE
}
//typedef
clauseType_t;
* ****/



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	fCbaseConditional is a generic template for all conditionals
	    It holds the payload class when a clause resolves to the direct type.
	    It holds the axiom (if or select) and its expression (if expression).
	    This is followed by a list of clauses which are then / else or a
	    list of cases with a possible default.
	    We pass in the expression class so all of the internal classes can be
	    adjusted.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
CLAUSECLASS is always the same as the wrapper class to this:
            ie FM?conditional<PAYLOADCLASS> or FM?ConditionalList<PAYLOADCLASS>
note that CLAUSECLASS, EXPRESSIONCLASS, PAYLOADCLASS must all have destroy functions
     CLAUSECLASS and PAYLOADCLASS must have copy constructors
     EXPRESSIONCLASS must have an operator= functionality.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            
template<class CLAUSECLASS, class EXPRESSIONCLASS, class PAYLOADCLASS>
class fCbaseConditional 
{
public: /** Contained Class **/
    /* this is a contained class in order to use the template values */
	class fCclauseItem
	{
	public: /* data */
	    clauseType_t    thisClause;	// subset of expressionType_t
	    EXPRESSIONCLASS clauseExpr; // ONLY used for a cT_CASE_expr clause
	    CLAUSECLASS   * clauseCond; // used for  then, else, case & default    
	    PAYLOADCLASS  * directClass;// ONLY used for a cT_Direct

	public: /* housekeeping */
	    fCclauseItem():thisClause(cT_Unknown),clauseCond(NULL), directClass(NULL) {};
		fCclauseItem(const fCclauseItem& ed) { operator=(ed);    };
		virtual ~fCclauseItem() { destroy(); };
	    fCclauseItem&	operator=(const fCclauseItem& ed)
	    {   thisClause = ed.thisClause;
	        clauseExpr = ed.clauseExpr;// operator=
            if (ed.clauseCond)
	            clauseCond  = new CLAUSECLASS(*ed.clauseCond);
			else
				clauseCond = NULL;
            if (ed.directClass)
	            directClass = new PAYLOADCLASS(*ed.directClass);
			else
				directClass = NULL;
	        return *this;
	    };
	    virtual int destroy() 
	    {   
			thisClause = cT_Unknown;
	        if (clauseCond){
	            clauseCond->destroy();
	            delete clauseCond;
	            clauseCond = NULL;
	        }
	        if (directClass){
	            directClass->destroy();
	            delete directClass;
	            directClass = NULL;
	        }
	        clauseExpr.destroy();
			return 0;
	    };// end fCclauseItem.destroy
		virtual void clear()
		{
			destroy();
			clauseExpr.clear();
			thisClause = cT_Unknown;
		};
	        
	    
	public: /* methods */
		virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
		{
			if (thisClause == cT_IF_isTRUE || thisClause == cT_IF_isFALSE   || 
			    thisClause == cT_CASE_expr || thisClause == ct_SEL_isDEFAULT )
			{
				clauseCond = new CLAUSECLASS;
				if (clauseCond == NULL)
				{
					return -2;
				}
				else
				{
					return clauseCond->evaluateAttribute(pData, length, offset);
				}
			}
			else
			{
				return -1;
			}
		};
		virtual void hydratePrototype(aCgenericConditional::aCexpressDest& rdestExp, bool coalesce = false)
		{
			rdestExp.destType = (expressionType_t)thisClause; // may require a cast
			if (thisClause == cT_Direct)
			{
				if (coalesce)
				{
					directClass->generatePayload(rdestExp.pPayload);
				}
				else
				{
					rdestExp.pPayload = directClass->generatePayload();
				}
			}
			else
			{
				if (thisClause == cT_CASE_expr)// case only
				{
					clauseExpr.hydratePrototype(&(rdestExp.destExpression));
				}
				// case and everything else
				if (rdestExp.pCondDest == NULL)
				{
					rdestExp.pCondDest = clauseCond->makeaCclass();	//  [3/13/2014 timj]
				}

				clauseCond->hydratePrototype(rdestExp.pCondDest);	//  [8/17/2015 timj]
			}
			return;
		};
	
		virtual int  out(void)
		{			
			//LOGIT(COUT_LOG,"        List %s. \n",condType_Str[thisClause]);
			LOGIT(COUT_LOG,"        List %s. \n",clauseStrings[thisClause]);	//  [3/12/2014 timj]
			//if (thisClause == cdt_CASE)
			if (thisClause == cT_CASE_expr)	//  [3/12/2014 timj]
			{
				clauseExpr.out();
			}
			else
			if (directClass == NULL && clauseCond == NULL)
			{
				LOGIT(COUT_LOG,"        ERROR:  CLAUSE HAS NO CONTENT.\n");
			}
			else
			//if (thisClause == cdt_DIRECT)
			if (thisClause == cT_Direct)	//  [3/12/2014 timj]
			{
				directClass->out();
			}
			else
			if (clauseCond)
			{
				clauseCond->out();
			}
			else
			{
				LOGIT(COUT_LOG,"        ERROR:  Non-Direct CLAUSE WITH NO CLAUSE POINTER.\n");
			}

			return SUCCESS;
		};
	};// end fCclauseItem class
	

public:     /** Data **/
    axiomType_t             axiomType;  // if/select/direct
	                                    // direct will have a single, direct clauseitem
    EXPRESSIONCLASS         axiomExpr;  // empty on direct
    vector<fCclauseItem>    clauseList; // single direct clause on direct

	bool                    requiredDirect;
	bool					forceDirect;			//  [8/14/2015 timj]
    
    typedef typename vector<fCclauseItem>::iterator  iTclause;

	bool isConditional() { return axiomType != aT_Direct; };

public: /* housekeeping */
    fCbaseConditional(): axiomType(aT_Unknown),requiredDirect(false), forceDirect(false) {};
    fCbaseConditional(const fCbaseConditional& src) { operator=(src); };
    virtual ~fCbaseConditional()   { destroy(); };
    fCbaseConditional& operator=(const fCbaseConditional& s)
    {   
		axiomType = s.axiomType;
        axiomExpr = s.axiomExpr;
		requiredDirect = s. requiredDirect;
		forceDirect = s.forceDirect;			//  [8/14/2015 timj]
        clauseList= s.clauseList;// vectors are supposed to do this automagically
        return *this;
    };// end fCbaseConditional operator=

    virtual void destroy()
    {   axiomType = aT_Unknown;
        axiomExpr.destroy();
        for ( iTclause iT = clauseList.begin(); iT != clauseList.end(); ++iT)
        {   ((fCclauseItem*)&(*iT))->destroy(); }; clauseList.clear();
    };

	virtual void clear()
	{
		requiredDirect = forceDirect = false;  //  [8/14/2015 timj]
		destroy();
		axiomType = aT_Unknown;
		axiomExpr.clear();
	};

public: /* methods */
	// virtual evaluate attribute
	// virtual hydrate prototype
	virtual int out(void);

	void MakeDirect(PAYLOADCLASS *payload)	//  [8/17/2015 timj]
	{
		fCclauseItem clause;
		clause.directClass = payload;
		clause.thisClause = cT_Direct;
		clauseList.push_back(clause);
		axiomType = aT_Direct;
	};
    
};// end class fCbaseConditional


/*===========================================================================================*
  fCbaseConditional  implementation
  ===========================================================================================*/
template<class CLAUSECLASS, class EXPRESSIONCLASS, class PAYLOADCLASS>
int fCbaseConditional<CLAUSECLASS, EXPRESSIONCLASS, PAYLOADCLASS>::		out(void)
{
	//if (axiomType == cdt_DIRECT || requiredDirect)
	if (axiomType == aT_Direct || requiredDirect)	//  [3/12/2014 timj]
	{
		// in the case of FMA print the underlying data without the conditional wrapper
		if ( requiredDirect && ( (axiomType != aT_Direct) || clauseList.size() < 1 ) )
		{
			LOGIT(CERR_LOG, "A conditional attribute requiring a payload has none.\n");
			return FAILURE;
		}
		fCclauseItem* pCI;
		for ( iTclause iT = clauseList.begin(); iT != clauseList.end(); ++ iT)
		{
			pCI = &(*iT);
			if (pCI->thisClause != aT_Direct || pCI->directClass == NULL)	//  [3/12/2014 timj]
			{
				LOGIT(CERR_LOG, "A conditional clause requiring a payload has none.\n");
				return FAILURE;
			}
			if ( ! requiredDirect )
				if (reconcile == false )
					LOGIT(COUT_LOG,"                  Non-Conditional: (Direct) clause.\n");
			pCI->directClass->out();
		}
	}
	else
	{
		LOGIT(COUT_LOG,"Conditional is a %s (%d)\n", axiomStrings[axiomType],axiomType);
		axiomExpr.out();
		
		if ( clauseList.size() > 0 )
		{
			int y = 0;
			vector<fCclauseItem>::iterator iT;
			fCclauseItem* pClauseOut = NULL;
			for (iT = clauseList.begin();iT != clauseList.end(); ++iT)
			{
				LOGIT(COUT_LOG,"\n Conditional Clause %d is",++y);
				pClauseOut =  &(*iT);
				pClauseOut->out();
			}
		}
	}
	return SUCCESS;
};

class fCbase
{
public:
	virtual aCondDestType* makeaCclass(void) = 0;
};

#endif  // _FMA_FMx_CONDITIONAL_H

