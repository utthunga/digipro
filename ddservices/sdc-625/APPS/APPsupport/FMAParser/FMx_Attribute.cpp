#include <memory.h>
#include "logging.h"
#include "endian.h"
#include "AttributeString.h"

#include "FMx_general.h"
#include "FMx_attribute.h"

#include "FM8_defs.h"	/* these are included in the FMx class because all versions 
						   are converting to the FM8 values and strings.          */
#include "FMx_Support_primatives.h"
#include "v_N_v+Debug.h"

extern void outHex(UCHAR* pS, unsigned offset, unsigned len);

unsigned FMx_Attribute::classGuid = 1;		// debugging only, should never see 0

int FMx_Attribute::parseExpectedTag(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned tagLength;
	unsigned tagValue;
	RETURNCODE  ret = SUCCESS;

	if (expectedTag > 0)
	{
		if (expectedTag & 0x80)
		{
			tagValue = expectedTag & 0x7f;
			if (ret = parseExpectedExplicitTag(pData, length, offset, tagValue, tagLength))
				return ret;
		}
		else
		{
			PARSE_EXPECTED_IMPLICIT_TAG(expectedTag, ret);
		}
	}

	return ret;
}

int FMx_Attribute::parseExpectedExplicitTag(UCHAR** pData, int& length, unsigned& offset, unsigned expected, unsigned &tagLength)
{
  	unsigned _tagValue;																
	RETURNCODE ret;
																					
	ret = parseTag(pData, length, _tagValue, tagLength, offset);				
																					
	if (tagLength > (unsigned) length)												
	{																				
		LOGIT(CERR_LOG,"Error: %s attribute with tag's length bigger than available.\n)", attname);	
		return -1;																	
	}																				
																					
	if (_tagValue != expected)														
	{																				
		LOGIT(CERR_LOG,"Error: %s attribute tag is not %d.\n)", attname, expected);	
		return -2;																	
	}				

	return SUCCESS;
}


int FMx_Attribute::parseExpectedImplicitTag(UCHAR** pData, int& length, unsigned& offset, unsigned expected)
{
  	unsigned _tagValue  = 0;																
  	unsigned _tagLength = 0;																
	RETURNCODE ret = SUCCESS;
																					
	ret = parseTag(pData, length, _tagValue, _tagLength, offset);				
																					
	if (_tagLength != 0)												
	{																				
		LOGIT(CERR_LOG,"Error: %s attribute expected 0 length for implicit tag.\n)", attname);	
		ret = -1;																	
	}																				
	else																				
	if (_tagValue != expected)														
	{																				
		LOGIT(CERR_LOG,"Error: %s attribute tag is not the expected %d.\n)", attname,expected);	
		ret = -2;																	
	}				

	return ret;
}


int FMx_Attribute:: parseUnsigned(UCHAR** pData, int& length, unsigned& offset, unsigned &x)
{																						
	RETURNCODE  ret = 0;
	UINT64	_tempLongLong;																

	if (length > 0)																		
	{																					
		ret = parseInteger(8, pData, length, _tempLongLong, offset);					
		if (ret != SUCCESS)															
		{																				
			LOGIT(CERR_LOG,"Error: %s attribute error in size extraction.\n)", attname);
		}																				
		else																			
		{																				
			x = (unsigned)_tempLongLong;												
		}																				
	}																					
	else																				
	{																					
		LOGIT(CERR_LOG,"Error: %s attribute needs size but has no length.\n)", attname);
		ret = -3;																	
	}		

	return ret;
}


int FMx_Attribute:: parseUINT64(UCHAR** pData, int& length, unsigned& offset, UINT64 &x)														
{																						
	RETURNCODE  ret = 0;
	UINT64	_tempLongLong;																

	if (length > 0)																		
	{																					
		ret = parseInteger(8, pData, length, _tempLongLong, offset);					
		if (ret != SUCCESS)															
		{																				
			LOGIT(CERR_LOG,"Error: %s attribute error in size extraction.\n)", attname);
		}																				
		else																			
		{																				
			x = _tempLongLong;												
		}																				
	}																					
	else																				
	{																					
		LOGIT(CERR_LOG,"Error: %s attribute needs size but has no length.\n)", attname);
		ret = -3;																	
	}		

	return ret;
}
void FMx_Attribute::destroy(void)
{	
	if (reconcile == false)
	{
		LOGIF(LOGP_MISC_CMTS)(COUT_LOG,"Attribute type %d did not implement the destroy function\n",attrType);
		LOGIF(LOGP_MISC_CMTS)(COUT_LOG,"********************************************************\n");
	}
}

void FMx_Attribute::clear()
{
	startOffset = 0;	length = 0;		lenOfLength = 0;		pBytes =  NULL;	
	attrType    = -1;	origType = -1;	status      = 0;		id.attrType= id.RefType = 0xff;
	heapOffset  = fileOffset   = 0;		pHeapBytes  = NULL;		externalIndex = 0;
	// leave		expectedTag;
	if (attname) delete attname; attname = NULL;    requiredDirect = false;
};

// note that attribute types and other values are converted to version 8 values
// the out functions only need to output FM8 type strings
int FMx_Attribute::out(void)
{
	if (startOffset != 0 && length != 0 && lenOfLength != 0 && pBytes  != NULL &&
		id.RefType != 0xff && id.attrType != 0xff )// not FMA attribute
	{// put out the hex if we can
		if ( id.RefType == 0 )
		{
			outHex(pBytes, startOffset, length+lenOfLength+1);// hex of this attribute
		}
		else
		if (id.RefType == 1)
		{
			outHex(pBytes, startOffset, lenOfLength+1);// hex of the type,length & offset
		}
		else
		{
			DEBUGLOG(CLOG_LOG,"No Posted RI value.\n");
		}
	}
	if (attrType >= ag_Last_Generic_Attr_Type)
	{
		return -1;
	}

	if ( ! reconcile )
	{
		LOGIT(COUT_LOG,"            Length: 0x%04x (%u).\n",length,length);
	}

	// This little section is for 8 only, should never be output for anything else.
	// zero is a valid attribute type
	if (id.attrType < 128 || id.RefType < 128)
	{
		if ( length == 0 )
		{
			return 0;//  we have an unused FMx_Attribute
		}
		if ( ! reconcile )
		{
			
			LOGIT(COUT_LOG,"           Ref Ind: 0x%02x (%u) :",id.RefType,id.RefType);
			if (id.RefType == 0)
			{
				LOGIT(COUT_LOG," Immediate <next bytes>\n");
			}
			else
			if (id.RefType == 1)
			{
				LOGIT(COUT_LOG," Local <in the heap>\n");
				LOGIT(COUT_LOG,"      Local Offset: 0x%04x (%u)\n",heapOffset, heapOffset);
			}
			else
			if (id.RefType == 2)
			{
				LOGIT(COUT_LOG," External <refs another attribute>\n");
			}
			else
			if (id.RefType == 3)
			{
				LOGIT(COUT_LOG," External All <Undefined - Never Used>\n");
			}
			else
			{
				LOGIT(COUT_LOG," UNDEFINED REFERENCE INDICATOR!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			}
			LOGIT(COUT_LOG,"          Attr Tag: 0x%02x (%u)\n\n",id.attrType,id.attrType);
		}
	}
	

	if (pHeapBytes != NULL || heapOffset != 0)
	{
		if ( ! reconcile  &&  ! maintenance )
		{
		LOGIT(COUT_LOG," . . . . annex . . . . \n");
		}

		if (!maintenance)
		{
			outHex(pHeapBytes, fileOffset,  length);  // hex of this attribute in the heap
		}
	}
	
	if ( reconcile )
	{
		LOGIT(COUT_LOG," Attribute: %s\n\n", AttributeString::get8(attrType>>8, attrType&0xff));
	}
	else // not reconcile
	if (origType < 0)
	{
		LOGIT(COUT_LOG," Attribute: 0x%02x (%u) %s\n\n",attrType,attrType,
			AttributeString::get8(attrType>>8, attrType&0xff));
	}
	else
	{
		LOGIT(COUT_LOG," Attribute: 0x%02x (%u) %s\n\n",origType,origType,
			AttributeString::get8(attrType>>8, attrType&0xff));
	}

	return 0;
}

aCattrBase* FMx_Attribute::createStringAttribute(ulong attrID)
{
	aCattrString* prPtr =  new aCattrString;

	prPtr->condString.priExprType = eT_Direct;

	aCgenericConditional::aCexpressDest tempaCDest;
	tempaCDest.destType = eT_Direct;
	tempaCDest.pPayload = generatePayload();

	prPtr->condString.destElements.clear();
	prPtr->condString.destElements.push_back(tempaCDest);

	prPtr->attr_mask = maskFromInt(attrID);

	return ((aCattrBase*)prPtr);
}

aCattrBase* FMx_Attribute::createLongAttribute(ulong attrID)
{
	aCattrCondLong* prPtr =  new aCattrCondLong;

	prPtr->condLong.priExprType = eT_Direct;

	aCgenericConditional::aCexpressDest tempaCDest;
	tempaCDest.destType = eT_Direct;
	tempaCDest.pPayload = generatePayload();

	prPtr->condLong.destElements.clear();
	prPtr->condLong.destElements.push_back(tempaCDest);

	prPtr->attr_mask = maskFromInt(attrID);

	return ((aCattrBase*)prPtr);
}

aCattrBase* FMx_Attribute::createBitstringAttribute(ulong attrID)
{
	aCattrBitstring* prPtr =  new aCattrBitstring;

	prPtr->condBitStr.priExprType = eT_Direct;

	aCgenericConditional::aCexpressDest tempaCDest;
	tempaCDest.destType = eT_Direct;
	tempaCDest.pPayload = generatePayload();

	prPtr->condBitStr.destElements.clear();
	prPtr->condBitStr.destElements.push_back(tempaCDest);

	prPtr->attr_mask = maskFromInt(attrID);

	return ((aCattrBase*)prPtr);
}

aCattrBase* FMx_Attribute::createIntWitchAttribute(ulong attrID)
{
	aCattrCondIntWhich* prPtr =  new aCattrCondIntWhich;

	prPtr->condIntWhich.priExprType = eT_Direct;

	aCgenericConditional::aCexpressDest tempaCDest;
	tempaCDest.destType = eT_Direct;
	tempaCDest.pPayload = generatePayload();

	prPtr->condIntWhich.destElements.clear();
	prPtr->condIntWhich.destElements.push_back(tempaCDest);

	prPtr->attr_mask = maskFromInt(attrID);

	return ((aCattrBase*)prPtr);
}


aCattrBase* FMx_Attribute::createReferenceAttribute(ulong attrID)
{
	aCattrCondReference* prPtr =  new aCattrCondReference;

	prPtr->condRef.priExprType = eT_Direct;

	aCgenericConditional::aCexpressDest tempaCDest;
	tempaCDest.destType = eT_Direct;
	tempaCDest.pPayload = generatePayload();

	prPtr->condRef.destElements.clear();
	prPtr->condRef.destElements.push_back(tempaCDest);

	prPtr->attr_mask = maskFromInt(attrID);

	return ((aCattrBase*)prPtr);
}

FMx_Attribute & FMx_Attribute::operator=(FMx_Attribute &src)
{
	classGuid = src.classGuid;		
	attrGuid = src.attrGuid;		
	startOffset = src.startOffset;	
	length = src.length;			
	lenOfLength = src.lenOfLength;	
	pBytes = src.pBytes;			
	attrType = src.attrType;		
	origType = src.origType;
	status = src.status;			
	id = src.id;				
	heapOffset = src.heapOffset;		
	fileOffset = src.fileOffset;     
	pHeapBytes = src.pHeapBytes; 
	externalIndex = src.externalIndex;	
	expectedTag = src.expectedTag;
	attname = src.attname;
	requiredDirect = src.requiredDirect;
	isConditional = src.isConditional;	
	return *this;
}
