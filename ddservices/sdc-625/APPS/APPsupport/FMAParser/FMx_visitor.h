/**********************************************************************************************
 *
 * $Workfile: FMx_visitor.h $
 * 02nov11 - stevev - created
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMx_visitor.h : holds definition of the visitor base class, interface to all visitors
 *
 * #include "FMx_visitor.h"
 *
 */

#ifndef _FMX_VISITOR_H
#define _FMX_VISITOR_H

class FMx_Base;

class FMx_visitor
{
public: // construct/destruct
	FMx_visitor() {};// no data, no construction(at this time)	
	virtual ~FMx_visitor() {};

public: // implementation
	virtual int Operate(FMx_Base* pVisited) = 0;// required function
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Notes on our visitor...
A visitor class is used to allow easy implementation and maintenence of various operations
on a given, reasonably stable, set of classes.  To implement a new function on all the classes
just derive a class from this one, implement Operate() for all the different class-types and
you are done.  This allows the encapsulation of functionality into a class (or class-set) even
though it may function on widely differnt data sets.(eg printObject(), each object's data set
is different but the visitor is the same).

This pattern is used to execute operations on multiple classes all derived from FMx_Base.
( we could have different call backs for different classes by having, say, 
  Obj8Operate(FM8_Object* p) in addition to others.. we don't have to, we won't)

Each unique operation will have a FMx_visitor-derived class that does that operation on all
known class types.  That derived class will have to have internal knowledge of the class(es)
it is operating on and will perform appropriately.

Somebody will call the object's ExecuteVisitor(ptr to a FMx_visitor-derived class )
That object will normally immediatly call FMx_visitor->Operate(this) to execute the operation.

FMx_Base has 6 types of classes derived from him.
Operate will have to cast as he sees fit.  Any classes that are not of interest (operation not
implemented for that class) will just return SUCCESS.  The ExecuteVisitor() should pass back
any errors to the calling class.

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#endif //_FMX_VISITOR_H