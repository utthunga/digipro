

#include "FMx_Dict.h"
#include "logging.h"
#include "globals.h"
#include "FM8_Dict.h"
#ifdef TOKENIZER
#include "ERR_GEN.H"
#endif

FMx_Dict::FMx_Dict() : saveBinary(false), pTable(NULL), fileKey(0LL)
{
}

FMx_Dict::~FMx_Dict()
{
	clearDict();
	pTable = NULL;	// the table is not ours
}


int FMx_Dict::retrieveStr(int str, wstring& retVal)
{	
	int rtn = -1;
	StrByNumber_t::iterator dictStrIT;
#if _MSC_VER >= 1900
		strListmutex.lock();
#endif

	dictStrIT = strByKey.find(str);

	if (dictStrIT == strByKey.end() )
	{
		retVal = DICT_STRING_NOT_FOUND;
	}
	else
	{
		dictStrList[dictStrIT->second].used = true;
		retVal = dictStrList[dictStrIT->second].StringItself;
		rtn = SUCCESS;
	}
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif
	return rtn;
}


int FMx_Dict::retrieveStr(wstring nm, wstring& retVal)
{
	int rtn = -1;
	StrByName_t::iterator dictStrIT;

#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	dictStrIT = strByName.find(nm);

	if (dictStrIT == strByName.end() )
	{
		retVal = DICT_STRING_NOT_FOUND;
	}
	else
	{
		unsigned index =  dictStrIT->second;
		dictStrList[index].used = true;
		retVal = dictStrList[index].StringItself;
		rtn = SUCCESS;
	}
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif

	return rtn;
}

int FMx_Dict::retrieveIndex(int index, wstring& retVal)
{
	int ret = SUCCESS;
	if (0 <= index && index < (int)(dictStrList.size()) )
	{
		retVal = dictStrList[index].StringItself; 
	}
	else
	{
		ret = FAILURE;
	}
	return SUCCESS;
}

unsigned FMx_Dict::retrieveKey(wstring nm)
{
	unsigned retKey = -1;	// not found
	StrByName_t::iterator dictStrIT;

#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	dictStrIT = strByName.find(nm);

	if (dictStrIT != strByName.end() )
	{
		unsigned index =  dictStrIT->second;
		dictStrList[index].used = true;
		retKey = dictStrList[index].keyNumber;
	}
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif

	return retKey;
}

// find the index of a string in the FMx_Dict - useful for only Literal String lookups
int FMx_Dict::findStr(wstring& val)
{
	unsigned hash = hash_fn(val);

	// brute force search for now
	int n = dictStrList.size();
	for (int i = 0; i < n; i++)
	{
		// literal strings store a hash of StringItself in keyNumber
		if (dictStrList[i].keyNumber == hash && val == dictStrList[i].StringItself)
		{
			return i;
		}
	}
	return -1;	// not found
}

int FMx_Dict::addUniqStr(wstring& newVal)
{
	int r = findStr(newVal);		// -1 on failure
	if (r == -1)
	{
		r = addStr(newVal);
	}
	return r;
}



// one arg is for Lit Strings
int FMx_Dict::addStr(wstring& newVal)
{// what's this
#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	int r = dictStrList.size();// index of the one we are about to add
	dictStr str;
	str.keyNumber = hash_fn(newVal);	// lit strings store a hash here [6/13/2016 tjohnston]
	str.StringItself = newVal;
	str.used = false;
	dictStrList.push_back(str);
	
	// removed strByKey usage for Lit Strings, key number stores a hash [6/13/2016 tjohnston]
	//// added 11apr16 to enable the find by key to work for literal or dictionary	
	//strByKey.insert(pair<unsigned, unsigned>(r, r));
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif
	return r;
}

// three args is for Dict Strings
int FMx_Dict::addStr(wstring& newVal, wstring& newName, unsigned newKey, bool newUsed)
{
#if _MSC_VER >= 1900
		strListmutex.lock();
#endif

	int r = dictStrList.size();// index of the one we are about to add
	dictStr str;
	str.keyNumber		= newKey;
	str.keyName			= newName;
	str.StringItself	= newVal;

	str.used			= newUsed;
	dictStrList.push_back(str);
	strByKey.insert(pair<unsigned, unsigned> (newKey,  r));
	strByName.insert(pair<wstring, unsigned>(newName, r));
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif
	return r;
}


void FMx_Dict::clearDict(void) // empty all the lists
{
#if _MSC_VER >= 1900
		strListmutex.lock();
#endif
	dictStrList.clear();// supposed to destroy and clear all memory
	strByKey.clear();
	strByName.clear();
#if _MSC_VER >= 1900
	strListmutex.unlock();
#endif
}

// copy dictStr entry from this dictionary to otherDict
// will not create a duplicate entry
// return SUCCESS if new entry is created in otherDict
int FMx_Dict::copyStrTo(int str, FMx_Dict *otherDict, bool markUsed)
{
	int rtn = FAILURE;
	wstring name;	// returned but not used
int sz = name.size();
	if (otherDict->retrieveStr(str, name) == -1)
	{
		StrByNumber_t::iterator it = strByKey.find(str);
		if (it != strByKey.end())	// TODO ASSERT_DBG
		{
			unsigned index = it->second;
			otherDict->addStr(dictStrList[index].StringItself, dictStrList[index].keyName, 
				dictStrList[index].keyNumber, (markUsed)?markUsed:dictStrList[index].used);
			rtn = SUCCESS;
		}
	}

	return rtn;
}


#ifndef TOKENIZER
#define pStandardDICTSTR this
#endif
void FMx_Dict::copyDictTo(FMx_Dict *otherDict)
{
#ifdef TOKENIZER
	wstring text;
	StrList_t::iterator it;
	for (it = dictStrList.begin(); it != dictStrList.end(); it++)
	{
		dictStr &str = (*it);

		/* stevev 05oct16 - this is currently only used for copying an imported dictionary 
		 *     into the working dictionary.  As such we have to make it match the legacy 
		 *     by using the standard strings, not the import strings.
		 */
		if ( pStandardDICTSTR->retrieveStr(str.keyNumber, text) == SUCCESS)
		{
			// unused.......FMx_Dict *pd = pWrkDICTSTR; // can't use the 2 DICTSTR macros on the same line
			pStandardDICTSTR->copyStrTo(str.keyNumber, otherDict, true);// copy and mark
		}
		else
		{
#ifdef TOKENIZER
			error(INVALID_DICT_REF, str.keyNumber >> 16, str.keyNumber & 0x0000ffff, " Imported");
#else
			LOGIT(CERR_LOG|CLOG_LOG,"Invalid dictionary string [%d,%d]%s.",
									str.keyNumber >> 16, str.keyNumber & 0x0000ffff, " Imported");
#endif
		}
	}// next
#endif
}

// iteration function on string name  (ie keyName)
void    FMx_Dict::setFirstString(void)
{
	//internalIT = strByName.begin();
	internalIT = strByKey.begin();
}
dictStr FMx_Dict::getNextString(void)
{
	dictStr retval;
	if (internalIT == strByKey.end())
	{
		retval.keyName      = L"";
		retval.keyNumber    = -1;
		retval.StringItself = L"";
		retval.used			= false;
	}
	else
	{
		retval.keyName   = internalIT->first;
		unsigned index = internalIT->second;
		retval = dictStrList[index];
		internalIT++;
	}
	return retval;
}

// iteration function on lit strings  (ie index)
void    FMx_Dict::setFirstLitString(void)
{
	litIT = dictStrList.begin();
}
int FMx_Dict::getNextLitString(dictStr &ds)
{
	if (litIT == dictStrList.end())
	{
		ds.keyName      = L"";
		ds.keyNumber    = -1;
		ds.StringItself = L"";
		return FAILURE;
	}
	else
	{
		ds = (*litIT);
		litIT++;
	}
	return SUCCESS;
}