#ifndef _CONVERT_
#define _CONVERT_

#include "dllapi.h"
#include "FMx_defs.h"
#include "ddbdefs.h"
#include "FMx.h"

#define EXPR_TYPE_CONVERT  {\
		eet_Undefined,\
		eet_logical_not,\
		eet_negation,\
		eet_complement,\
		eet_addition,\
		eet_subtraction,\
		eet_multiplication,\
		eet_division,\
		eet_modulus,\
		eet_left_shift,\
		eet_right_shift,\
		eet_bitwise_and,\
		eet_bitwise_or,\
		eet_bitwise_xor,\
		eet_logical_and,\
		eet_logical_or,\
		eet_less_than,\
		eet_greater_than,\
		eet_less_or_equal,\
		eet_greater_or_equal,\
		eet_equal,\
/*21*/	eet_not_equal,\
	\
/*22*/	eet_integer_constant,\
/*23*/	eet_floating_constant,\
/*24*/  eet_variable_id,\
/*25*/  eet_maxVal,\
/*26*/  eet_minVal,\
/*27*/	eet_numeric_value,\
/*28*/	eet_maxValRef,\
/*29*/	eet_minValRef,\
/*30*/	eet_error,\
/*31*/	eet_error,\
/*32*/	eet_error,\
/*33*/	eet_string_constant,\
/*34*/	eet_error,\
/*35*/	eet_count_ref,\
/*36*/	eet_capacity_ref,\
/*37*/	eet_first_ref,\
/*38*/	eet_last_ref  }

wchar_t* GetWC(const char* c);

class ConvertToDDL
{
public:
	ConvertToDDL(void) {};
	~ConvertToDDL(void) {};

//	static expressionType_t ConditionType(condType_t type);
	static expressionType_t ClauseType(condType_t type);
	static expressElemType_t ExpressionElementType(exprElemType_t type);
};

class ConvertToA
{
private:
	static exprElemType_t v8_Expr_2_V10[];

public:
	static exprElemType_t ExpressionElementType(expressElemType_t type)
	{
		return v8_Expr_2_V10[type];
	};
};


class ConvertTo8
{
public:
	static int refType(int fmaType);
	static int enumerationTag(int tag);
	static int baseClass(int tag);
	static int parameterType(int tag);
	static int classBitA28(int bitposA);
	static UINT64 classA28(UINT64 bsA);
	static unsigned chartType(unsigned typeA);
	static unsigned axisScaling(unsigned scaleA);
	static unsigned waveType(unsigned typeA);
	static unsigned orient(unsigned typeA);
	static unsigned outputClass(unsigned classA);
	static unsigned responseCodeType(unsigned typeA);
	static unsigned operation(unsigned opA);
	static unsigned style(unsigned styleA);
	static unsigned timeScale(unsigned typeA);
	static unsigned displaySize(unsigned typeA);

};



class ConvertToString
{
#ifndef DEF__LANG__CTRY
	#define DEF__LANG__CTRY    L"|en|"	
	#define COUNTRY_CODE_MARK  L'|'
#endif
//	static FMx* pFile;	// replaced with thread-local pFILE
public:
	ConvertToString(void) {};
	~ConvertToString(void) {};

public: //implementation
	static void fetchDictionString(unsigned strNumber, wstring& returnedStr);
	static void fetchLiteralString(unsigned strNumber, wstring& returnedStr);
	static void fetchDictionString(wstring  keyString, wstring& returnedStr);
	static void fetchSymbolNameNtype(unsigned symNumber,string& returnedstr, int& Stype);
	static void fetchSymbolNameByIndex(unsigned index, string& returnedstr, int& Stype);
	static void filterString(wstring& inAndOutString);//lang filter
//	static void setFilePtr(FMx* pF){pFile = pF;};

	static int  get_string_translation(wstring &instr, wstring &outstr);
	static int  get_string_translation(wchar_t *string,wchar_t *outbuf,int outbuf_size);

};

#endif //_CONVERT_
