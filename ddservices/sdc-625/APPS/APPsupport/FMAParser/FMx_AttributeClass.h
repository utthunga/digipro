#ifndef _FMX_ATTRIBUTECLASS_
#define _FMX_ATTRIBUTECLASS_

#include "FMx_Attribute.h"
#include "FMx_Attributes_Ref&Expr.h"

class asCdefaultValList : public FMx_Attribute
{
public:
	asCdefaultValList(int y) : FMx_Attribute(y) {};
};

class asCitemInfo : public FMx_Attribute
{
public:
	asCitemInfo(int y) : FMx_Attribute(y) {};
};



/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

// moved this block to the bottom as asCexprSpec is required by icCminmax
// when all these class defs have moved to files, the block will be vack up to the top where
// it belongs. - timj

#include "FMx_Attributes\faCString.h"
#include "FMx_Attributes\faCByteString.h"
#include "FMx_Attributes\asCvarType.h"
#include "FMx_Attributes\asCintegerConst.h"
#include "FMx_Attributes\faCMember.h"
#include "FMx_Attributes\asCdisplaySize.h"
#include "FMx_Attributes\asCasciiStr.h"
#include "FMx_Attributes\asCwaveType.h"
#include "FMx_Attributes\icCoperation.h"
#include "FMx_Attributes\icClineType.h"
#include "FMx_Attributes\icCmethType.h"
#include "FMx_Attributes\icCstyle.h"
#include "FMx_Attributes\asCnotHart.h"
#include "FMx_Attributes\icCminmax.h"
#include "FMx_Attributes\asCunknownType.h"
#include "FMx_Attributes\asCreserved.h"
#include "FMx_Attributes\asCattrRefOnly.h"
#include "FMx_Attributes\asCdefaultValuesList.h"

#endif //_FMX_ATTRIBUTECLASS_