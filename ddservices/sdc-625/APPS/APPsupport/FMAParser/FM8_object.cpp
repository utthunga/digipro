/**********************************************************************************************
 *
 * $Workfile: FM8_Object.cpp $
 * 31aug11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Object class implementation...the model of One File Object
 *		
 *
 */

#include "FM8_defs.h"
#include "FM8_object.h"
#include "FM8_objectSet.h"
#include "logging.h"

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );

FM8_Object::FM8_Object(FMx* pTC, FILE* pf, bool saveBin) : FMx_Base(pTC, pf, saveBin, 0x42)
{
	obj_Fixed         = 0;
	obj_payload       = 0;
	obj_annex_payload = 0;

	object_offset     = 0;

	obj_Index         = 0;
	obj_type          = obj_Unknown; // 0
	obj_annex_size    = 0;// in UCHARs
	obj_annex_offset  = 0;
	obj_payload_size  = 0;
#ifdef _DEBUG
	objHeapStartOffset = 0;
#endif
	pHandler = NULL;
	extProcessed = false;
}


FM8_Object::~FM8_Object()
{
	if ( obj_Fixed )
	{
		delete[] obj_Fixed; obj_Fixed = 0;
	}
	if (obj_payload != 0)
	{
		delete[] obj_payload; obj_payload = 0;
	}
	if (pHandler)
	{
		delete pHandler;  pHandler = 0;
	}
}

/*********************************************************************************************
 *	read_Object at current file pointer
 *
 *	in: remaining bytes in object section
 *
 *	returns: err code or success with the file pointer just after last byte of payload
 *
 *	reads in the entire object fixed and then the object extension 
 *		
 ********************************************************************************************
RETURNCODE FM8_Object::read_Object(unsigned& size) 
{
	RETURNCODE r = -1;

	if (fp == 0 )
		return FMx_BADPARAM;

	r = read_Object_Info( size, false );// read the object, payload and all

	return r;
}
***/
/*********************************************************************************************
 *	read_Object_Info   at current file pointer
 *
 *	in: we normally seek over the extension.  When we are used as a piece of reading an
 *      entire object, we read our part and leave the file pointer on the next byte
 *
 *	returns: err code or success 
 *			file pointer  will be just after last byte of the payload
 *            if skipExtensionRead then obj_payload will be NULL
			  else obj_payload will hold the payload binary
 *
 *	read in the object fixed and then read in the extension if asked or skip the extension
 *	Skip is used when indexing a file for future read-in
 *		
 ********************************************************************************************/
RETURNCODE FM8_Object::read_Object_Info(unsigned& size, bool skipExtensionRead)
{
	if (fp == 0 )
		return FMx_BADPARAM;

	object_offset = ftell(fp);

	obj_Fixed = new UCHAR[V8_FIXED_SIZE];
	int returns = fread(obj_Fixed, 1, V8_FIXED_SIZE, fp);
	if (returns != V8_FIXED_SIZE)
	{
		delete[] obj_Fixed; obj_Fixed = 0;
		return FMx_READFAIL;
	}// else continue working
	size -= returns;
	
	WORD us;
	us  = REVERSE_S( *((WORD*) &obj_Fixed[V8_IDX_OFFSET]) );// short          -Object_Index
	obj_Index = us;
// this is always 2, we're going to skip it and allow the instantiator to set type
//	obj_type   = (obj_type_t)obj_Fixed[V8_OBJ_CD_OFFSET];		// char- always 2 in fm8       -object-code <always 2 - domain>
	us = REVERSE_S( *((WORD*) &obj_Fixed[V8_LENGTH_OFFSET]) );// short   -max-bytes
	obj_annex_size = us;
	unsigned test = REVERSE_S( *((WORD*) &obj_Fixed[V8_LENGTH_OFFSET]) );// short   -max-bytes
	obj_annex_offset = REVERSE_L( *((DWORD*) &obj_Fixed[V8_HEAP_OFFSET_OFFSET]));//long    -local-address

	obj_payload_size = obj_Fixed[V8_EXT_SIZE_OFFSET];//                                        -extension-size

	if (skipExtensionRead)// the default, normal condition
	{		
		if ( fseek(fp,obj_payload_size,SEEK_CUR) )// zero on success
		{
			return FMx_REPOSFAIL;
		}// we're now pointing to the first byte of next object	
	}
	else
	{
		obj_payload  = new UCHAR[obj_payload_size];
		returns = fread(obj_payload, 1, obj_payload_size, fp);
		if (returns != obj_payload_size)
		{
			delete[] obj_payload; 
			obj_payload = 0;
			return FMx_READFAIL;
		}// else continue to the exit
		// we're now pointing to the first byte of next object, just fall thru
	}
	size -= obj_payload_size;

	if ( ! saveBinary )
	{
		delete[] obj_Fixed; obj_Fixed = 0;
		if ( obj_payload )
		{
			delete[] obj_payload; 
			obj_payload = 0;
		}
	}

	return SUCCESS;
}

RETURNCODE FM8_Object::readHeapChunk(unsigned& size)
{
	RETURNCODE r = -1;

	if ( obj_annex_offset == 0xffffffff || obj_annex_size == 0 )
	{// nothing to do
		return SUCCESS;// we get called whether there is a chunk or not
	}

	objHeapStartOffset = ftell(fp);
#ifdef _DEBUG
	if (obj_annex_size == 0xffff )
	{
		LOGIT(CLOG_LOG,"idx 0x%04x: Heap Chunk size not recorded for type %u!\n",
																	obj_Index,(int)obj_type);
	}
#endif
	if ( objHeapStartOffset != calcHeapStartOffset)
	{
		LOGIT(CERR_LOG,"idx 0x%04x: Heap Chunk location mismatch: Actual:%u  Calc'd:%d\n",
											obj_Index, objHeapStartOffset,calcHeapStartOffset);
		if (fseek(fp, calcHeapStartOffset, SEEK_SET))// zero on success
		{
			LOGIT(CERR_LOG,"Heap Chunk Seek failed.\n"); 
			return FMx_REPOSFAIL;
		}
	}

//	if (obj_annex_size == 0xffff && pHandler != NULL)
	if (obj_annex_size > 0 && pHandler != NULL)
	{// we should be at calcHeapStartOffset which is heap start + annex_offset
		//unsigned heapOffset = objHeapStartOffset-(pTopClass->headerData.getOffset2Heap());
		//if (pHandler->findOffset(heapOffset-obj_annex_offset) >= 0)
		//{// we own this offset
		//	r = pHandler->readChunk();
		//}// else error
		r = pHandler->readChunk( size );
	}
	else
	if (obj_annex_size == 0xffff)// extended size but no handler
	{// we must have internal knowledge of the structure to deal with an extended size
		LOGIT(CERR_LOG,"No handler for extended (-1) heap size.(Index 0x%04x)\n",obj_Index);
		r = FMx_OPPFAIL;
	}
	else // not extended size and no handler - should never happen
	{// default reader
		LOGIT(CERR_LOG,"No handler for %d heap size.(Index 0x%04x)\n",
															   (int)obj_annex_size, obj_Index);
		obj_annex_payload  = new UCHAR[obj_annex_size];
		unsigned returns = fread(obj_annex_payload, 1, obj_annex_size, fp);
		if (returns != obj_annex_size)
		{
			delete[] obj_annex_payload; 
			obj_annex_payload = 0;
			r =  FMx_READFAIL;
		}
		else
		{// continue to the exit
			r = SUCCESS;
		}
	}

	return r;
}

void FM8_Object::out(unsigned idx, unsigned size)
{
	LOGIT(COUT_LOG,"Object %4u] \n",idx);
	LOGIT(COUT_LOG,"\tfile offset to this  = %#04x\n",object_offset);
	LOGIT(COUT_LOG,"\t  size (just fixed)  = %#04x\n",V8_FIXED_SIZE);
		outHex(obj_Fixed, object_offset, V8_FIXED_SIZE);
	LOGIT(COUT_LOG,"\t              index  = 0x%04x (type %d",obj_Index,(int)obj_type);
	if (obj_type==2)
		LOGIT(COUT_LOG," [domain])\n");
	else
		LOGIT(COUT_LOG,")\n");
																     
	LOGIT(COUT_LOG,"\t        annex offset = 0x%04x ",obj_annex_offset);
	if (obj_annex_offset != 0xffffffff)
		LOGIT(COUT_LOG,"  <0x%08x>\n",pTopClass->headerData.getOffset2Heap()+obj_annex_offset);
	else
		LOGIT(COUT_LOG,"\n");
	LOGIT(COUT_LOG,"\t   size (just annex) = %#04x\n",obj_annex_size);
	LOGIT(COUT_LOG,"\t- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- \n");
	LOGIT(COUT_LOG,"\t    extension offset = %#06x\n",obj_payload_offset());
	LOGIT(COUT_LOG,"\t    size (extension) = %#04x\n",obj_payload_size);
		outHex(obj_payload, obj_payload_offset(), obj_payload_size);
//	if (obj_annex_offset != 0xffffffff && obj_annex_payload != NULL && obj_annex_size > 0)
//	{
//		LOGIT(COUT_LOG,
//			       "\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n"); 
//		outHex(obj_annex_payload, calcHeapStartOffset, obj_annex_size);
//	}
	if ( pHandler )
		pHandler->out();
LOGIT(COUT_LOG,
"____________________________________________________________________________"
"\n");
}

void FM8_Object::out2(unsigned idx, unsigned size)
{
	if (obj_annex_offset != 0xffffffff && obj_annex_payload != NULL && obj_annex_size > 0)
	{
		LOGIT(COUT_LOG,
				   "\t####################################################################\n");	
		LOGIT(COUT_LOG,"\t index  = 0x%04x     size = 0x%04x\n",obj_Index, obj_annex_size);
		LOGIT(COUT_LOG,
			       "\t + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +\n"); 
		outHex(obj_annex_payload, calcHeapStartOffset, obj_annex_size);
	}
	if ( pHandler )
		pHandler->out2();
}

/*********************************************************************************************
 *	read_ObjectPayload   at recorded offset
 *
 *	in: nothing
 *
 *	returns: err code or success with the file pointer just after last byte of the payload
 *
 *	seek to recorded payload location, read in the extension as payload
 *		
 *  annex will be read from the heap in the interpret visitor
 ********************************************************************************************/
RETURNCODE FM8_Object::read_ObjectPayload(void)
{
	if (fp == 0 || obj_payload_offset() == V8_FIXED_SIZE)// no payload is at zero offset
		return FMx_BADPARAM;

	if (fseek(fp, obj_payload_offset(), SEEK_SET))// zero on success
	{
		return FMx_REPOSFAIL;
	}

	if (obj_payload_size)
	{
		obj_payload = new UCHAR[obj_payload_size];
		int returns = fread(obj_payload, 1, obj_payload_size, fp);
		if (returns != obj_payload_size)
		{
			delete[] obj_payload; obj_payload = 0;
			return FMx_READFAIL;
		}// else continue working
	}// else nothing to read

	// the saveBinary will only come to play when the data is extracted from the binary

	return SUCCESS;
}

FMx_handler* FM8_Object::getObjectHandler(void)
{
	if (pHandler == NULL)// don't have one yet
	{// get one
		switch ( obj_type )
		{
		case obj_8_Format:
			{
				pHandler = new FM8_Format_handler(this);
			}
			break;
		case obj_8_Dev_Dir:
			{// we don't do this yet
				pHandler = new FM8_DevDir_handler(this);
			}
			break;
		case obj_8_Blk_Dir:
			{// we don't do this yet
				pHandler = new FM8_BlkDir_handler(this);
			}
			break;
		case obj_8_DD_item:
			{// we don't do this yet
				pHandler = new FM8_Item_handler(this);
			}
			break;
		case obj_Unknown:
		default:
			break;// leave r fail
		}
	}// else use the one it's got
	return pHandler;// could be null
}


/* stevev 21may12 - In order to make the version 8 run like the version 10, I'm adding
 *		a unique FM8Object to deal with the DDOD_ODES
 *
 * NOTE: The initial read_Object_Info has to do the whole thing.  The information is
 *	needed to handle the rest of the file.
 **************************************************************************************/
RETURNCODE FM8_ODES::read_Object_Info(unsigned& size, bool skipExtensionRead)
{
	RETURNCODE r = FAILURE;
	obj_payload_size = DD_ODES_SIZE;
	object_offset    = ftell(fp);
	obj_Fixed        = NULL; // we don't have one
	if ( obj_payload )//the raw extension chunk			should never happen
	{
		delete[] obj_payload; obj_payload = NULL;
	}

	obj_Index     = 0;// always for the ODES
	// leave as owner set...obj_type;

	/** leave these as empty as they were instantiated
		// ANNEX (on the heap) FOR FM8 ONLY
		obj_annex_offset;// heap payload offset from beginning of heap
		obj_annex_size;// heap payload length in UCHARs (-1 means extended)
		obj_annex_payload;// the part that is in the DDOD Heap @ offset
		objHeapStartOffset;// disk location
	****/

	// we have to do read_ObjectPayload()...normally done in the visitor ReadPayloadVisitor
	int iRetVal = 0;
	// we're going to store this in the extension payload pointer
	obj_payload = new UCHAR[obj_payload_size];
	if (obj_payload == NULL)
	{
		return FMx_MEMFAIL;
	}
	iRetVal = fread(obj_payload, 1, obj_payload_size, fp);

	if (iRetVal != obj_payload_size)
	{
		delete[] obj_payload;
		obj_payload = NULL;
		return FMx_READFAIL;
	}
	size -= obj_payload_size;

	// we have to do InterpretPayloadVisitor next
	pHandler = new FM8_ODES_handler(this);
	r = ((FM8_ODES_handler*)pHandler)->evaluate__Extension();

	return r;
}

RETURNCODE  FM8_ODES::read_ObjectPayload(void)
{// this was done in read_Object_Info above	

	return SUCCESS;
}

FMx_handler* FM8_ODES::getObjectHandler(void)
{
	if (! pHandler)
		pHandler = new FM8_ODES_handler(this);
 
	return pHandler;
}