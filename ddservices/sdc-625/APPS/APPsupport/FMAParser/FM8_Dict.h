/**********************************************************************************************
 *
 * $Workfile: FM8_Dict.h $
 * 29oct12 - stevev from fma
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Dict : FMA class description file
 *
 * #include "FM8_Dict.h"
 *
 */

#ifndef _FM8_DICT_H
#define _FM8_DICT_H

#ifdef INC_DEBUG
#pragma message("In FM8_Dict.h") 
#endif

#include <vector>
#include <string>

#include "FMx_Dict.h"

using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FM8_Dict.h") 
#endif

#ifdef STRING_TBL
#undef STRING_TBL
#endif

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class models the *.fm8 dictionary in two modes
 *	first -	Single language mode - only the start language is loaded
 *	second- All languiages, for changing languages on the fly
 *
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FM8_Dict : public FMx_Dict
{
public:		
	FM8_Dict();  // constructor
	virtual ~FM8_Dict();

public:		// workers
	//virtual void retrieveStr(int  str,   wstring& retVal);
	//virtual void retrieveStr(wstring nm, wstring& retVal);
	virtual void setTheTable( FMx_Table* pTbl);

protected:	// helpers

};



#endif	// _FM8_DICT_H