/**********************************************************************************************
 *
 * $Workfile: FMA_meta.cpp $
 * 11aug11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		implementation for handling the FMA meta data
 *		
 *
 */

#include "Endian.h"
#include "logging.h"

#include "FMA_meta.h"

FMA_meta::~FMA_meta()
{
	if (pDefDesc)
	{
		delete pDefDesc;
	}

	if (pDevTables)
	{
		delete pDevTables;
	}

	if (pBlockTable)
	{
		delete pBlockTable;
	}
}

/*********************************************************************************************
 *	index_MetaData
 *
 *	in: nada
 *
 *	returns: err code or success with the file open located at byte past meta data
 *
 *  reads and records pertinent info for each object in the meta data fixed area,skips payload 
 *			reads the description_object into memory
 *
 ********************************************************************************************/
RETURNCODE FMA_meta::index_MetaData(unsigned& size)
{
	RETURNCODE r = -1;
	unsigned  metaSize = 0;

//	FMA_Object *pBlockTable; 
	// raw read 'em all in, we'll need the data in a bit
	pDefDesc	= (FMA_Object  *) new  FMA_Object(pTopClass, fp, saveBinary);
	pDevTables  = (FMA_Object  *) new  FMA_Object(pTopClass, fp, saveBinary);
	pBlockTable = (FMA_Object  *) new  FMA_Object(pTopClass, fp, saveBinary); 

	if ( pDefDesc == NULL || pDevTables == NULL || pBlockTable == NULL )
	{
		if (pDefDesc   != NULL) delete pDefDesc;    pDefDesc = NULL;
		if (pDevTables != NULL) delete pDevTables;  pDevTables = NULL;
		if (pBlockTable!= NULL) delete pBlockTable; pBlockTable = NULL;
		return FMx_MEMFAIL;
	}

	pDefDesc->read_Object_Info(true);// read the data  payload...we'll decode this inline
	metaSize    = pDefDesc->obj_size();

	pDevTables->read_Object_Info(true);//->read_Object();
	metaSize   += pDevTables->obj_size();

	while ((size - metaSize) > OBJ_FIXED_SIZE )// fixed is missing if table doesn't exist
	{
		pBlockTable->read_Object_Info(true);//->read_Object();
		metaSize   += pBlockTable->obj_size();
//		blockTableList.push_back(pBlockTable);
#ifdef _DEBUG
		if (size != metaSize)
		{
			LOGIT(CLOG_LOG,"MetaDataRead did not match header.\n\t\tSize=%d  Header=%d\n",
				metaSize,size);
		}
#endif
		if ( (size - metaSize) > OBJ_FIXED_SIZE)
		{// there is another block- should never happen in HART DD
			LOGIT(CERR_LOG,"ERROR: HART DD has too many BLOCK tables.\n");
//			pBlockTable = (FMA_Object  *) new  FMA_Object(pTopClass, fp, saveBinary);
		}
	}// wend
	if ( size == metaSize)
		r = SUCCESS;	

	return r;
}

RETURNCODE FMA_meta::ExecuteVisitor(FMx_visitor* pVisitor)
{ 
	RETURNCODE r = -1;
	if (pDefDesc)
	{
		r = pDefDesc->ExecuteVisitor(pVisitor); 
		if (r != SUCCESS) return r;
	}

	if (pDevTables)
	{
		r = pDevTables->ExecuteVisitor(pVisitor); 
		if (r != SUCCESS) return r;
	}

	if (pBlockTable)
	{
		r = pBlockTable->ExecuteVisitor(pVisitor);
	}
	
	return r;
}

unsigned FMA_meta::get_item_count(void)
{
	if (pDefDesc == NULL || pDefDesc->pHandler == NULL)
		return 0;
	else
	{
		FMA_Description_handler* pHndlr =(FMA_Description_handler*)(pDefDesc->pHandler);
		return pHndlr->item_count;
	}
}

unsigned FMA_meta::get_first_obj_index(void)
{
	if (pDefDesc == NULL || pDefDesc->pHandler == NULL)
		return 0;
	else
	{
		FMA_Description_handler* pHndlr =(FMA_Description_handler*)(pDefDesc->pHandler);
		return pHndlr->firstItemIndex;
	}
}

unsigned FMA_meta::get_block_count(void)
{
	if (pDefDesc == NULL || pDefDesc->pHandler == NULL)
		return 0;
	else
	{
		FMA_Description_handler* pHndlr =(FMA_Description_handler*)(pDefDesc->pHandler);
		return pHndlr->block_count;
	}
}