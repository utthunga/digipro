#ifndef _FMA_ATTRIBUTECLASS_
#define _FMA_ATTRIBUTECLASS_

#include "FMx_AttributeClass.h"
#include "FMA_Defs.h"
#include "FMA_Attributes_Ref&Expr.h"
#include "FMA_conditional.h"

#include "FMA_Attributes\faCStringA.h"
#include "FMA_Attributes\faCByteStringA.h"
#include "FMA_Attributes\asCvarTypeA.h"
#include "FMA_Attributes\asCintegerConstA.h"
#include "FMA_Attributes\faCMemberA.h"
#include "FMA_Attributes\asCdisplaySizeA.h"
#include "FMA_Attributes\asCasciiStrA.h"
#include "FMA_Attributes\asCwaveTypeA.h"
#include "FMA_Attributes\asCstrSpecA.h"
#include "FMA_Attributes\asCboolSpecA.h"
#include "FMA_Attributes\asCmemberLstA.h"
#include "FMA_Attributes\asChandlingSpecA.h"
#include "FMA_Attributes\asCclassSpecA.h"
#include "FMA_Attributes\asCchartTypeSpecA.h"
#include "FMA_Attributes\asCintegerSpecA.h"
#include "FMA_Attributes\asCorientSpecA.h"
#include "FMA_Attributes\asCaxisScalSpecA.h"
#include "FMA_Attributes\asCtimeScaleSpecA.h"
#include "FMA_Attributes\icCoperationA.h"
#include "FMA_Attributes\asClineTypeSpecA.h"
#include "FMA_Attributes\icCmethTypeA.h"
#include "FMA_Attributes\icCstyleA.h"
#include "FMA_Attributes\asCnotHartA.h"
#include "FMA_Attributes\asCrefSpecA.h"
#include "FMA_Attributes\asCrefListSpecA.h"
#include "FMA_Attributes\asCactionListSpecA.h"
#include "FMA_Attributes\asCminmaxSpecA.h"
#include "FMA_Attributes\asCmenuItmSpecA.h"
#include "FMA_Attributes\asCmethParamsA.h"
#include "FMA_Attributes\asCvectorSpecA.h"
#include "FMA_Attributes\asCexprSpecA.h"
#include "FMA_Attributes\asCenumLstSpecA.h"
#include "FMA_Attributes\asCelementsSpecA.h"
#include "FMA_Attributes\asCunknownTypeA.h"
#include "FMA_Attributes\asCreservedA.h"
#include "FMA_Attributes\asCattrRefOnlyA.h"
#include "FMA_Attributes\asCrespCodesSpecA.h"
#include "FMA_Attributes\asTransactionLstA.h"
#include "FMA_Attributes\asCuuidA.h"
#include "FMA_Attributes\asCdefaultValuesListA.h"


class asCitemInfoA : public asCitemInfo
{
public:
	asCitemInfoA(int y) : asCitemInfo(y) {};

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{
		RETURNCODE  ret = SUCCESS;
		
		exitValues_t holdExit; 
		pushExitValues(holdExit, pData, length, offset);

		ret = parseExpectedTag(pData, length, offset);// conditioned on expected tag exists


// should be length,string
(*pData)+= length;
offset += length;
length = 0;

		/****
		
		explicit_tag	Explicit_Tag,		-- tag = 50
		file_name		String,				-- Literal String in table
		line_number		INTEGER,
		SEQUENCE	OF Attr_Info	
					<count continues till explicit-tag.explicit_length exhausted>

		***/

		return 0;
	}

	virtual int out()
	{
		if (! reconcile)
		{
			// TODO: figure out how to get the symbol name from here.  once we do that, we can 
			// remove the if (!reconcile) test from here and asCitemInfo8::out()
			// LOGIT(COUT_LOG,"       Symbol Name: %s\n", WTF goes here?);
		}
		FMx_Attribute::out();

		return 0;
	}

};


#endif //_FMA_ATTRIBUTECLASS_