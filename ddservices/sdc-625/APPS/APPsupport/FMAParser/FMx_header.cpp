#include "FMx_header.h"
#include "logging.h"
#include "v_N_v+Debug.h"

extern void outHex(UCHAR* pS, unsigned offset, unsigned len);

RETURNCODE FMx_header::readHeader(FILE* filePtr, bool saveBinary)  
{
	RETURNCODE r = -1;
	int returns = 0, newLen = 0;

	if ( filePtr == NULL )
		return FMx_BADPARAM;

	pStart  = new unsigned char[HDRSTARTLEN];

	returns = fread(pStart, 1, HDRSTARTLEN, filePtr);
	if (returns != HDRSTARTLEN)
	{
		delete[] pStart; pStart= NULL;
		return FMx_READFAIL;
	}

	magic_number = REVERSE_L( *((unsigned int*) &pStart[0]) );// expect 0x8A45F533
	header_size  = REVERSE_L( *((unsigned int*) &pStart[4]) );

	newLen = header_size - HDRSTARTLEN;

	pBody = new unsigned char[newLen];

	returns = fread(pBody, 1, newLen, filePtr);
	if (returns != newLen)
	{
		delete[] pStart; pStart= NULL;
		delete[] pBody;  pBody = NULL;
		return FMx_READFAIL;
	}
	
	ddef_meta_size     = REVERSE_L( *((unsigned int*) &pBody[0]));// all objects
	ddef_obj_size      =	REVERSE_L( *((unsigned int*) &pBody[4]));//post-object data size
	byManufacturer[0]  = pBody[8];
	byManufacturer[1]  = pBody[9];
	byManufacturer[2]  = pBody[10];
	byManufacturer[3]  = 0;
	device_type        =	REVERSE_S(*((unsigned short*) &pBody[11]));
	device_revision	  = pBody[13];
	dd_revision	      = pBody[14];
	EFF_rev_major      = pBody[15];
	EFF_rev_minor      = pBody[16];
	deviceProfile      = pBody[17];	//-- X01 HART, X02 FF, X03 Profi
	reserved1          = pBody[18];
	fileSignature      = REVERSE_L( *((unsigned int*) &pBody[19]) );
	reserved3          = REVERSE_L( *((unsigned int*) &pBody[23]) );
	reserved4          = REVERSE_L( *((unsigned int*) &pBody[27]) );//@31+8=39

	if (! saveBinary)
	{
		delete[] pStart; pStart= NULL;
		delete[] pBody;  pBody = NULL;
	}
	
	return SUCCESS;
}

void FMx_header::out(void)
{
	if (SUPPRESS_HEADER)
		return;

	LOGIT(COUT_LOG, "Header structure\n");

	if ( pStart != NULL)
	{
		outHex(pStart, 0, HDRSTARTLEN );
	}

	if ( pBody != NULL )
	{
		outHex(pBody, HDRSTARTLEN, header_size - HDRSTARTLEN );
	}

	unsigned tm = (byManufacturer[0]<<16)+(byManufacturer[1]<<8)+(byManufacturer[2]);
	if (reconcile == false)
	{
		LOGIT(COUT_LOG,"  magic number: %u    (%x)\n", magic_number, magic_number);
		LOGIT(COUT_LOG,"   header size: %6u (0x%06x)\n",header_size,header_size);

		if (SUPPRESS_PTOCSIZE==false)
		{
			LOGIT(COUT_LOG,"  objects size: %6u (0x%06x) <file offset to DDOD heap 0x%08x>\n" ,
				ddef_meta_size,ddef_meta_size,  ddef_meta_size+header_size ); 
		}
		if (! maintenance && SUPPRESS_PTOCSIZE==false)
		{
			LOGIT(COUT_LOG,"     heap size: %6u (0x%06x) <file offset to Image(s)  0x%08x>\n",
				ddef_obj_size, ddef_obj_size, ddef_meta_size+header_size+ddef_obj_size);
		}
	}
	LOGIT(COUT_LOG,"  Manufacturer: 0x%06x\n",tm);//byManufacturer[0-2]
	LOGIT(COUT_LOG,"   Device Type: 0x%04x\n",device_type);      
	LOGIT(COUT_LOG,"   Device  Rev: 0x%02x\n",device_revision);	 
	LOGIT(COUT_LOG,"   DD Revision: 0x%02x\n",dd_revision);     
	if (reconcile == false)
	{
		if (!SUPPRESS_SIGNATURE)
		{
			LOGIT(COUT_LOG,"Format Version: %u.%u\n",EFF_rev_major,EFF_rev_minor);
		}
		LOGIT(COUT_LOG,"       Profile: %s",
			(deviceProfile==1)?"HART":
				(deviceProfile==2)?"FF":
					(deviceProfile==3)?"Profibus":"Unknown");
	
		if(deviceProfile < 1 || deviceProfile > 3)
		{
			LOGIT(COUT_LOG," (0x%02x)\n",deviceProfile);
		}
		else
		{
			LOGIT(COUT_LOG,"\n");
		}
	
		if (! maintenance && !SUPPRESS_SIGNATURE)
		{
			LOGIT(COUT_LOG,"      reserved: %u\n",reserved1);       
			LOGIT(COUT_LOG,"     Signature: %08x\n",fileSignature);  
		}
		LOGIT(COUT_LOG,"      reserved: %u\n",reserved3);      
		LOGIT(COUT_LOG,"      reserved: %u\n",reserved4);     
	}
	LOGIT(COUT_LOG,"======================================================================\n");
};