/**********************************************************************************************
 *
 * $Workfile: FM8_Support_tables.h $
 * 14sep13 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Support_tables.h : These are the content handlers
 *
 * #include "FM8_Support_tables.h"
 *
 */

#ifndef _FM8_SUPP_TBLS_H
#define _FM8_SUPP_TBLS_H


#ifdef INC_DEBUG
#pragma message("In FM8_Support_tables.h") 
#endif

#include <vector>

using namespace std;
#pragma warning (disable : 4786) 

#include "FMA_Support_primatives.h"
#include "FMx_Table.h"
#include "FMx_handler.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes: FM8_Support_tables.h") 
#endif




enum devDirTblIndex_e
{
	item_table_idx,	// 0
	string_tbl_idx,
	dictionary_idx,
	cmd_table_idx,
	image_tbl_idx   // 4
};

#define DEV_DIR_TBL_CNT 5

#define item_segment    (devDirTblInfo[item_table_idx])
#define string_segment  (devDirTblInfo[string_tbl_idx])
#define dict_segment    (devDirTblInfo[dictionary_idx])
#define command_segment (devDirTblInfo[ cmd_table_idx])
#define image_segment   (devDirTblInfo[ image_tbl_idx])

#define item_data		(devDirTables[item_table_idx])
#define string_data		(devDirTables[string_tbl_idx])
#define dict_data		(devDirTables[dictionary_idx])
#define command_data	(devDirTables[ cmd_table_idx])
#define image_data		(devDirTables[ image_tbl_idx])

struct dictString_entry_s
{
	unsigned	number;
	wstring		name;
	wstring		actualStr;
};
////////////////////////////////////////////////
/*typedef struct Segment_s
{
	unsigned    offset;
	unsigned    actualoffset;// the ftell from the read start
	unsigned    length;
} segment_t;*/
struct img_item_s
{
	unsigned char	langCode[6];
	segment_t		img_location;//offset,length&offset from start of file
};
class img_table_elem
{
public:
	unsigned num_langs;
	img_item_s *imageArray;

public:
	void generate(AframeList_t *framelist, unsigned char *pBufferStart);
};
////////////////////////////////////////////////
class RRindex
{
public:
	unsigned        indxID;
	unsigned		index;
public:
	RRindex() { clear(); };
	RRindex(const RRindex& s){ operator=(s); };
	RRindex& operator=(const RRindex& p)
	{ indxID=p.indxID; index=p.index; return *this;};
	void clear(){indxID=0; index=0;};
	void out(int sp = 0);
};

class RRcmdTrans
{
public:
	unsigned        subIdx;
	unsigned		cmdNum;
	unsigned        trnNum;
	unsigned		weight;

	vector<RRindex>	idxList;
public:
	RRcmdTrans() { clear();};
	RRcmdTrans(const RRcmdTrans& s){ operator=(s); };
	virtual ~RRcmdTrans(){idxList.clear();};
	RRcmdTrans& operator=(const RRcmdTrans& p)
	{ subIdx=p.subIdx; cmdNum=p.cmdNum; trnNum=p.trnNum; weight=p.weight;
	idxList = p.idxList; return *this;  };// supposed to be a deep copy
	void clear(){subIdx=cmdNum=trnNum=weight=0; if (idxList.size()) idxList.clear();};
	void out(int sp = 0);
};

class RR_ref
{
public:
	unsigned	expr;
	unsigned    symNum; // this or the next will be non-zero
	RR_ref		*src;	// but not both
public:
	RR_ref():src(NULL) { clear(); };
	RR_ref(const RR_ref& s){ operator=(s); };
	virtual ~RR_ref(){ RAZE(src);};
	RR_ref& operator=(const RR_ref& p)
	{ expr=p.expr; symNum=p.symNum; 
	  if (p.src) src = new RR_ref(*(p.src)); else src = NULL; 
	  return *this;};
	void clear(){expr=symNum=0; if (src) delete src; src = NULL; };
	void out(int sp = 0);
};

class RR_ptoc_row_s
{
public:
	RR_ref  param_name;
	vector<RRcmdTrans>	redList;
	vector<RRcmdTrans>	wrtList;
public:
	RR_ptoc_row_s() { /*they should all instantiate clear*/ };
	RR_ptoc_row_s(const RR_ptoc_row_s& s){ operator=(s); };
	virtual ~RR_ptoc_row_s(){clear();};
	RR_ptoc_row_s& operator=(const RR_ptoc_row_s& p)
	{ param_name=p.param_name; redList=p.redList; wrtList=p.wrtList;return *this;};
	void clear(){param_name.clear();redList.clear();wrtList.clear();};
	void out(int sp = 0);
};
////////////////////////////////////////////////
class FM8_Table : public FMx_Table
{
public:// location
	UCHAR*      pPayload;
	unsigned    payldLength;// entire table (type,len,payld)

public:// data
	unsigned		rowCount;

public:// construct/destruct
	FM8_Table(){};
	virtual 
	~FM8_Table(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset);
	virtual int reconcileOut(unsigned offset);
};

class FM8_DictTable : public FM8_Table, public vector<dictString_entry_s>
{

public:// construct/destruct
	FM8_DictTable(){};
	virtual
	~FM8_DictTable(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset, UCHAR* pPayload, unsigned payldLength);
	
};


class FM8_LitStringTable : public FM8_Table, public vector<wstring>
{

public:// construct/destruct
	FM8_LitStringTable(){};
	virtual
	~FM8_LitStringTable(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset, UCHAR* pPayload, unsigned payldLength);
	
};


class FM8_CriticalParameterTable : public FM8_Table, public vector<unsigned>
{

public:// construct/destruct
	FM8_CriticalParameterTable(){};
	virtual
	~FM8_CriticalParameterTable(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset, UCHAR* pPayload, unsigned payldLength);
};

class FM8_RRptocTable : public FM8_Table, public vector<RR_ptoc_row_s>
{
// helpers
	int getRR(RR_ref* refOut, unsigned char ** pRaW, int& sz);
	int getCmdDesc(RRcmdTrans* pCmdOut, unsigned char ** pRaW, int& sz);

public:// construct/destruct
	FM8_RRptocTable(){};
	virtual
	~FM8_RRptocTable(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset, UCHAR* pPayload, unsigned payldLength);
};


class FM8_ImageTable : public FM8_Table, public vector<img_table_elem>
{
// helpers
	//int getRR(RR_ref* refOut, unsigned char ** pRaW, int& sz);
	//int getCmdDesc(RRcmdTrans* pCmdOut, unsigned char ** pRaW, int& sz);

public:// construct/destruct
	FM8_ImageTable(){};
	virtual
	~FM8_ImageTable(){};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset, UCHAR* pPayload, unsigned payldLength);
};
#endif //_FM8_SUPP_TBLS_H
