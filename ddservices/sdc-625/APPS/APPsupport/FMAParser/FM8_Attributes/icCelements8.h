
/*************************************************************************************************
 *
 * filename  icCelements.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 * *
 */
//   ___elements___

#ifndef _icCelements8_
#define _icCelements8_

#include "..\FMx_Attributes\icCelements.h"

#include "..\FM8_conditional.h"
#include "faCString8.h"

class icCelement8 : public icCelement
{
public: // construct / destruct
	icCelement8() {};
	icCelement8(const icCelement8& elem) { operator=(elem); };
	virtual ~icCelement8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCelement8& operator=(const icCelement8& src);
};


class icCelementList8 : public vector<icCelement8 *>
{
public: // construct / destruct
	icCelementList8() {};
	icCelementList8(const icCelementList8& src) { operator=(src); };
	virtual ~icCelementList8() { destroy(); };
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCelementList8&	operator=(const icCelementList8& s);
};

#endif //_icCelements8_



