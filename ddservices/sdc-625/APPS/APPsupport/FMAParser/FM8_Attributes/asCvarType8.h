/*************************************************************************************************
 *
 * filename  asCvarType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Variable Type Attributeclass
 *
 */

#ifndef _ASCVARTYPE8_
#define _ASCVARTYPE8_

#include "../FMx_Attributes/asCvarType.h"

class asCvarType8 : public asCvarType
{
public: // construct
	asCvarType8(int tag) : asCvarType(tag) {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCVARTYPE8_