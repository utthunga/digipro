/*************************************************************************************************
 *
 * filename  asCRefreshLst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Reference List Attribute 8 class
 *
 * #include "asCRefreshLst8.h"
 *
 */

#ifndef _ASCREFRESHLST8_
#define _ASCREFRESHLST8_

#include "..\FMx_Attribute.h"
#include "asCrefSpec8.h"
#include "asCrefListSpec8.h"
#include "..\FMx_Attributes\asCRefreshLst.h"
#include "asCrefListSpec8.h"
#include "..\AttributeString.h"

class asCRefreshLst8 : public asCRefreshLst
{
public:
	asCRefreshLst8(int tag) : asCRefreshLst(tag) 
	{
		watch = new asCrefListSpec8(0);
		update = new asCrefListSpec8(0);
	};

	~asCRefreshLst8()
	{
		delete watch;
		delete update;
	};

public:
	virtual int reconcile_out(void);
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCREFRESHLST8_