/*************************************************************************************************
 *
 * filename  asCrefListSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Reference List Specifier Attribute class
 *
 */

#ifndef _ASCrefListSPEC8_
#define _ASCrefListSPEC8_

#include "..\FM8_Attributes\icCrefList8.h"

#include "..\FM8_conditional.h"
#include "..\FMx_Attributes\asCrefListSpec.h"

class asCrefListSpec8 : public  asCrefListSpec /*FMx_Attribute*/
{
public:
	asCrefListSpec8(int tag, bool direct=false) : asCrefListSpec(tag) /*FMx_Attribute(tag)*/
	{
		conditional.requiredDirect = direct;
	};
    
public:
	
    fC_8_condList<icCrefList8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		if (reconcile==false)
		{
			LOGIT(COUT_LOG,"%23s:\n","Reference List Spec");   // Attribute name
		}
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrReferenceList* attr = new aCattrReferenceList;
		conditional.hydratePrototype(&attr->RefList);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCrefListSPEC8_


