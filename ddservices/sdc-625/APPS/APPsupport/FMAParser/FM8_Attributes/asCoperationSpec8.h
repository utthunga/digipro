/*************************************************************************************************
 *
 * filename  asCoperationSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Operation Specifier Attribute class
 *
 */

#ifndef _ASCoperationSPEC8_
#define _ASCoperationSPEC8_

//#include "..\FMx_Attributes\asCoperationSpec.h"

#include "..\FM8_conditional.h"
class asCoperationSpec8 : public  FMx_Attribute // asCoperationSpec
{
public:
	asCoperationSpec8(int tag, bool direct = false) 
	{
		conditional.forceDirect = true;
		conditional.requiredDirect = direct;
	};
    
public:
	fC_8_Conditional<icCoperation8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(cmdAttrOperation);//id);

		return attr;
    };
};

#endif //_ASCoperationSPEC8_


