/*************************************************************************************************
 *
 * filename  asCrespCodesSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Response Code Specifier Attribute class
 *
 */

#ifndef _ASCrespCodesSPEC8_
#define _ASCrespCodesSPEC8_

#include "..\FM8_Attributes\icCrespCode8.h"

#include "..\FM8_conditional.h"
#include "..\FMx_Attributes\asCrespCodesSpec.h"

class asCrespCodesSpec8 : public  asCrespCodesSpec
{
public:
	asCrespCodesSpec8(int tag) : asCrespCodesSpec(tag) {};
    
public:
	
	fC_8_condList<icCrespCodeList8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        LOGIT(COUT_LOG,"%23s:\n","Response Codes Spec");   // Attribute name
        conditional.out(); 
        return 0; 
    };	
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrCmdRspCd* list = new aCattrCmdRspCd;
		conditional.hydratePrototype(&list->respCodes);
		isConditional = conditional.isConditional();

		list->attr_mask = maskFromInt(attrType);

		return list;
	};

	virtual void hydrateConditional(aCcondList* cond) 
	{
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	};

};

#endif //_ASCrespCodesSPEC8_


