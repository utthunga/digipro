/*************************************************************************************************
 *
 * filename  icCactionListElem8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Action List Element Attribute
 *
 */

#include "icCactionListElem8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

int icCactionListElem8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	if (choice == 0)
	{
		ref->evaluateAttribute(pData, length, offset);
	}
	else
	{
		LOGIT(CERR_LOG,"Error: %s attribute error in choice value %d is should be on range [0, 1].\n)", attname, choice);
	}
  
    return ret;    
}



//=============================================================================================


int icCactionList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCactionListElem8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCactionListElem8 *pElem;

	while(length > 4)
	{
		pElem = new icCactionListElem8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCactionList8::destroy()
{
	icCactionListElem8 *pElem = NULL;
	vector<icCactionListElem8 *>::iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCactionList8::out(void)
{
	icCactionListElem8 *pElem = NULL;
	vector<icCactionListElem8 *>::iterator iT;//ptr2ptr2icCgridMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCactionList8::generatePayload(aPayldType*payld)
{	
	aCreferenceList  *pPlst;
	if (payld)
	{
		pPlst = (aCreferenceList  *) payld;
	}
	else
	{
		pPlst = new aCreferenceList;
	}

	aCreference  locElem;
	icCactionListElem8 *pElem = NULL;
	vector<icCactionListElem8 *>::iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCactionList8& icCactionList8::operator=(const icCactionList8& s)
{
	icCactionListElem8 *pElem = NULL, *pnewElem = NULL;
	vector<icCactionListElem8 *>::const_iterator iT;//ptr2ptr2icCactionListElemA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCactionListElem8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
