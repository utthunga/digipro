/*************************************************************************************************
 *
 * filename  icCmethType8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Method Type Attribute
 *
 */

#include "faCBitString8.h"
#include "asCasciiStr8.h"
#include "icCmethType8.h"
#include "faCString8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

int icCmethType8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, PARAMETER_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, value))
		goto end;

	if (ret = modifiers.evaluateAttribute(pData, workingLength, offset))
		goto end;

	if (ret = methodName.evaluateAttribute(pData, workingLength, offset))
		goto end;

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}


