/*************************************************************************************************
 *
 * filename  faCLong8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Long Attribute class
 *
 */

#ifndef _FACLONG8_
#define _FACLONG8_

#include "..\FMx_Attributes\faCLong.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class faCLong8 : public faCLong
{
public: // construct / destruct
	faCLong8() {}
	virtual ~faCLong8() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACLONG8_
