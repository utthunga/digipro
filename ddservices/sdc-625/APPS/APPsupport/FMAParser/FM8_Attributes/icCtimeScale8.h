/*************************************************************************************************
 *
 * filename  icCtimeScale8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Time Scale Attribute class
 *
 */

#ifndef _icCtimeScale8_
#define _icCtimeScale8_

#include "..\FMx_Attributes\icCtimeScale.h"

#include "..\FM8_conditional.h"
class icCtimeScale8 : public icCtimeScale
{
public: // construct / destruct
	icCtimeScale8() {}
	virtual ~icCtimeScale8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //icSCtimeScale8_



