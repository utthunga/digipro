/*************************************************************************************************
 *
 * filename  icCclass8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Class Attribute class
 *
 */

#ifndef _icCclass8_
#define _icCclass8_

#include "..\FMx_Attributes\icCclass.h"

#include "..\FM8_conditional.h"
class icCclass8 : public icCclass
{
public: // construct / destruct
	icCclass8() {}
	virtual ~icCclass8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCclass8_



