/*************************************************************************************************
 *
 * filename  asCunknownType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Unknown Type Attribute class
 *
 */

#ifndef _ASCUNKNOWNTYPE8_
#define _ASCUNKNOWNTYPE8_

#include "..\FMx_Attributes\asCunknownType.h"

class asCunknownType8 : public asCunknownType
{
public:
	asCunknownType8(int tag) : asCunknownType(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCUNKNOWNTYPE8_
