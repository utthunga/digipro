/*************************************************************************************************
 *
 * filename  asCmethParams8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Method Params Attribute class
 *
 */

#ifndef _ASCmethParams8_
#define _ASCmethParams8_

#include "..\FM8_Attributes\icCmethParam8.h"
#include "..\FMx_Attributes\asCmethParams.h"

#include "..\FM8_conditional.h"
#include "tags_sa.h"

/*
Method_Parameters_Specifier ::= [APPLICATION 91]
SEQUENCE  {
	explicit-tag	Explicit_Tag,		-- PARAMETER_SEQLIST_TAG
	SEQUENCE OF		Method_Parameter
}
*/

class asCmethParams8 : public  asCmethParams
{
public:
	asCmethParams8(int tag) : asCmethParams(tag) {};
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		unsigned tagLength;
		RETURNCODE  ret = 0;

		if (ret = parseExpectedExplicitTag(pData, length, offset, PARAMETER_SEQLIST_TAG, tagLength))
			return ret;

		int			exitLength		= length   - tagLength;
		unsigned	exitOffset		= offset   + tagLength;
		UCHAR	   *exitPdata		= (*pData) + tagLength;
		int			workingLength	= tagLength;

		while (workingLength > 0)
		{
			icCmethParam8 *p = new icCmethParam8;
			if (p == 0)
			{
				LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
				ret = -3;
				goto end;
			}
			paramList.push_back(p);

			if (ret = p->evaluateAttribute(pData, workingLength, offset))
				goto end;
		}

end:
		*pData = exitPdata;
		length = exitLength;
		offset = exitOffset;

		return ret;
    };
        
    
	virtual aCattrBase* generatePrototype()
    {
		return asCmethParams::generatePrototype();
    };
};
#endif //_ASCmethParams8_


