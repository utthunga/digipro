/*************************************************************************************************
 *
 * filename  faCString8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 String Attribute class
 *
 */

#ifndef _FACSTRING8_
#define _FACSTRING8_

#include "..\FM8_Dict.h"
#include "..\FMx_Attributes\faCString.h"
#include "..\FM8_Attributes_Ref&Expr.h"

class faCString8 : public faCString
{
public: // construct / destruct
	faCString8()
	{
		strRef = new abCReference8;
	};

	virtual ~faCString8()
	{
		//delete strRef; 	// strRef is now deleted in base class ~faCString() [4/2/2014 timj]
	}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACSTRING8_