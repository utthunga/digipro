/*************************************************************************************************
 *
 * filename  asCactionListSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Action List Specifier Attribute class
 *
 * #include "asCactionListSpec8.h"
 *
 */

#ifndef _ASCactionListSPEC8_
#define _ASCactionListSPEC8_

#include "..\FM8_Attributes\icCactionListElem8.h"

#include "..\FM8_conditional.h"
#include "..\FMx_Attributes\asCactionListSpec.h"

class asCactionListSpec8 : public  asCactionListSpec
{
public:
	asCactionListSpec8(int tag) : asCactionListSpec(tag) {};
    
public:
	
	fC_8_condList<icCactionList8> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        LOGIT(COUT_LOG,"%23s:\n","Action List Spec");   // Attribute name
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrActionList* list = new aCattrActionList;
		conditional.hydratePrototype(&list->RefList);
		isConditional = conditional.isConditional();

		list->attr_mask = maskFromInt(attrType);

		return list;
	};

	virtual void hydrateConditional(aCcondList* cond) 
	{
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	}
};

#endif //_ASCactionListSPEC8_


