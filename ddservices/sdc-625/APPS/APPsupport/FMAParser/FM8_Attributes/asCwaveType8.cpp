/*************************************************************************************************
 *
 * filename  asCwaveType8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Wave Type Attribute
 *
 */

#include "logging.h"
#include "asCwaveType8.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Waveform_Type_Specifier ::=  [APPLICATION 54]
SEQUENCE {
	implicit-tag	Implicit_Tag,
	ENUMERATED {
		xy				(0),
		yt				(1),
		horizontal		(2),
		vertical		(3)
		}
}
*/

int asCwaveType8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: asCwaveType8 attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != WAVE_TYPE_TAG)
	{
		LOGIT(CERR_LOG,"Error: asCwaveType8 attribute explicit tag is not 54.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: WaveType attribute error in size extraction.\n)");
		}
		else
		{
			value = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: WaveType attribute needs size but has no length.\n)");
		ret = -2;
	}

	return ret;
}
