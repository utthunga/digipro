/*************************************************************************************************
 *
 * filename  icCaxisScal8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Axis Scale Attribute class
 *
 */

#ifndef _icCaxisScal8_
#define _icCaxisScal8_

#include "..\FMx_Attributes\icCaxisScal.h"

#include "..\FM8_conditional.h"
class icCaxisScal8 : public icCaxisScal
{
public: // construct / destruct
	icCaxisScal8() {}
	virtual ~icCaxisScal8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_icCaxisScal8_



