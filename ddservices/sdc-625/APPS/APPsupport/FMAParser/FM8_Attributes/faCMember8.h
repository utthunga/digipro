/*************************************************************************************************
 *
 * filename  faCMember8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Member Attribute class
 *
 */

#ifndef _FACMEMBER8_
#define _FACMEMBER8_

#include "../FMx_Attributes/faCMember.h"
#include "..\FM8_Attributes_Ref&Expr.h"
#include "faCString8.h"

// this class is roughly equivelent to icCmembers8
class faCMember8 : public faCMember
{
public: // construct 
	faCMember8(int y = 0) : faCMember(y)	{};
	faCMember8(const faCMember8& elem) { operator=(elem); };
	faCMember8& operator=(const faCMember8& src);
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual ~faCMember8() {};
	virtual void GetSymbolName(string &name) { name = symbolName; }

};

class faCMemberList8 : public vector<faCMember8 *>
{
public: // construct / destruct
	faCMemberList8() {}
	faCMemberList8(const faCMemberList8& src) { operator=(src); };
	virtual ~faCMemberList8() { destroy(); }
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 faCMemberList8&	operator=(const faCMemberList8& s);
};
#endif //_FACMEMBER8_