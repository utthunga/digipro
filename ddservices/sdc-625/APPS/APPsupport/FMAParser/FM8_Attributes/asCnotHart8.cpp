/*************************************************************************************************
 *
 * filename  asCnotHart8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Not Hart Attribute
 *
 */

#include "logging.h"
#include "asCnotHart8.h"
#include "..\FMx_Support_primatives.h"

int asCnotHart8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	// not interested in content here, just scan over it all
	*pData += length;
	offset += length;
	length = 0;

	return 0;
}

