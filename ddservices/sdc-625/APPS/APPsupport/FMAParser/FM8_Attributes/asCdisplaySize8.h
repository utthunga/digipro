/*************************************************************************************************
 *
 * filename  asCdisplaySize8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Display Size Attribute class
 *
 */

#ifndef _ASCDISPLAYSIZE8_
#define _ASCDISPLAYSIZE8_

#include "..\FMx_Attributes\asCdisplaySize.h"

class asCdisplaySize8 : public asCdisplaySize
{
public:
	asCdisplaySize8(int tag) : asCdisplaySize(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCINTEGERCONST8_
