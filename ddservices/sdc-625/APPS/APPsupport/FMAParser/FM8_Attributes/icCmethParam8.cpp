/*************************************************************************************************
 *
 * filename  icCmethParam8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Method Params Attribute
 *
 */

#include "icCmethParam8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Method_Parameter ::=
SEQUENCE  {
		explicit-tag	Explicit_Tag,					-- PARAMETER_TAG
		variable_type	Encoded_Integer				-- see ddldefs.h �TYPE_VOID�
		type modifiers	BIT_STRING (Size 3 bits)	-- see ddldefs.h �ARRAYFLAG�
		variable name	ASCII_String
}
*/

int icCmethParam8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, PARAMETER_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, methodType))
		goto end;

	typeModifiers = new faCBitString8();
	if (typeModifiers == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = typeModifiers->evaluateAttribute(pData, workingLength, offset))
		goto end;


	paramName = new asCasciiStr8();
	if (paramName == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = paramName->evaluateAttribute(pData, workingLength, offset))
		goto end;

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;}



