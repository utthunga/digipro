/*************************************************************************************************
 *
 * filename  icCmethParam8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Method Params Attribute class
 *
 */

#ifndef _icCmethParam8_
#define _icCmethParam8_

#include "..\FMx_Attributes\icCmethParam.h"

#include "..\FM8_conditional.h"
#include "faCBitString8.h"
#include "asCasciiStr8.h"

class icCmethParam8 : public icCmethParam
{
public: // construct / destruct
	icCmethParam8() 
	{	
		typeModifiers = new faCBitString8();
		paramName = new asCasciiStr8();
	}

	virtual ~icCmethParam8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCmethParam8_



