/*************************************************************************************************
 *
 * filename  asCclassSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Class Specifier Attribute class
 *
 */

#ifndef _ASCclassSPEC8_
#define _ASCclassSPEC8_

#include "..\FM8_Attributes\icCclass8.h"

#include "..\FM8_conditional.h"
class asCclassSpec8 : public FMx_Attribute
{
public:
	asCclassSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_8_Conditional<icCclass8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		//aCattrCondLong* attr = new aCattrCondLong;
		aCattrBitstring* attr = new aCattrBitstring;
		// stevev 04mar16 we just new'd it...dynamic cast not required
		conditional.hydratePrototype(&attr->condBitStr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCclassSPEC8_


