/*************************************************************************************************
 *
 * filename  asCstrSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 String Specifier Attribute class
 *
 */

#ifndef _ASCSTRSPEC8_
#define _ASCSTRSPEC8_


#include "..\FM8_conditional.h"
#include "faCString8.h"

class asCstrSpec8 : public FMx_Attribute
{
public:
	asCstrSpec8(int tag) : FMx_Attribute(tag) {};

public:
	fC_8_Conditional<faCString8> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		RETURNCODE  ret = 0;

		ret = parseExpectedTag(pData, length, offset);//conditioned on expected tag exists

		if (ret == SUCCESS)
			ret = conditional.evaluateAttribute(pData, length, offset);  

		return ret;
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrString* attr = new aCattrString;
		attr->attr_mask = maskFromInt(attrType); //  [2/10/2016 tjohnston]
		conditional.hydratePrototype(&attr->condString);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	}
};

#endif //_ASCSTRSPEC8_
