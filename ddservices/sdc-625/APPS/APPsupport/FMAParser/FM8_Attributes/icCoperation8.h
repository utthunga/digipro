/*************************************************************************************************
 *
 * filename  icCoperation8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Operation Attribute class
 *
 */

#ifndef _icCoperation8_
#define _icCoperation8_

#include "..\FMx_Attributes\icCoperation.h"

#include "..\FM8_conditional.h"
class icCoperation8 : public icCoperation
{
public: // construct / destruct
	icCoperation8() {};//(int tag) : icCoperation(tag)  {}
	virtual ~icCoperation8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCoperation8_



