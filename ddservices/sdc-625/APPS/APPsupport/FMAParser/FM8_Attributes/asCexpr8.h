/*************************************************************************************************
 *
 * filename  asCexpr8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Expression Attribute class
 *
 */

#ifndef _ASCEXPR8_
#define _ASCEXPR8_

#include "..\FMx_Attributes\asCexpr.h"
#include "..\FM8_Attributes_Ref&Expr.h"

class asCexpr8 : public asCexpr<abCExpression8>
{
public:
	asCexpr8(int tag = 0) : asCexpr(tag) 
	{
		// pExpr = new abCExpression8;
	};
	asCexpr8(const asCexpr8& src)
	{   asCexpr<abCExpression8>::operator=(src); 
	};

	~asCexpr8()
	{
		//delete pExpr;
	};

	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset)
	{
		//return pExpr->evaluateAttribute(pData, length, offset);
		return Expr.evaluateAttribute(pData, length, offset);
	};
};

#endif //_ASCEXPR8_