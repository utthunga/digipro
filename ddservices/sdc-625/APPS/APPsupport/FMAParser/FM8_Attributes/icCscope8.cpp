/*************************************************************************************************
 *
 * filename  icCscope8.cpp
 *
 * 27 Aug 2013 - stevev
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Method Scope Attribute
 *
 */

#include "icCscope8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"


int icCscope8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
	RETURNCODE  ret = 0;

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Method Scope attribute error in value extraction.\n)");
		}
		else
		{
			value = (unsigned long)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Method Scope attribute needs value but has no length.\n)");
		ret = -2;
	}

	return ret;
}



