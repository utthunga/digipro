/*************************************************************************************************
 *
 * filename  icCdataElem8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Data Field Attribute class
 *
 */

#ifndef _ICCDATAFIELD8_
#define _ICCDATAFIELD8_

#include "..\FMx_Attributes\icCdataElem.h"
#include "..\FM8_Attributes_Ref&Expr.h"

#include "faCBitString8.h"

class icCdataElem8: public icCdataElem
{
public: // construct 
	icCdataElem8() 
	{
		varRef = NULL; // let's not alloc these till we need 'em ...new abCReference8;
		flags  = NULL; // new faCBitString8;
	};

	virtual ~icCdataElem8() {};

	icCdataElem8(const icCdataElem8& elem) { operator=( elem );};
 
public: // methods
	int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};


class icCdataElemList8 : public vector<icCdataElem8 *>
{
public: // construct / destruct
	icCdataElemList8() {};
	icCdataElemList8(const icCdataElemList8& src) { operator=(src); };
	virtual ~icCdataElemList8() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCdataElemList8&	operator=(const icCdataElemList8& s);
};
#endif //_ICCDATAFIELD8_
