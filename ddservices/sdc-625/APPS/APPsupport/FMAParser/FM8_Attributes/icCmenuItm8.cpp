/*************************************************************************************************
 *
 * filename  icCmenuItm8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Menu Item Attribute
 *
 */

#include "icCmenuItm8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FM8_Attributes\faCBitString8.h"
#include "tags_sa.h"

int icCmenuItm8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;
    
	//return -1;

	item = new abCReference8();  
	if (item == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		return FMx_MEMFAIL;
	}
	ret = item->evaluateAttribute(pData, length, offset);
	if (ret != SUCCESS)
		return ret;

	qualifiers = new faCBitString8();     
	if (qualifiers == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		return FMx_MEMFAIL;
	}
	ret = qualifiers->evaluateAttribute(pData, length, offset);


	return ret;
}



icCmenuItm8& icCmenuItm8::operator=(const icCmenuItm8& src)
{
	icCmenuItm::operator=(src);
	return *this;
}

//=============================================================================================


int icCmenuItmList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCmenuItm8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCmenuItm8 *pItm;

	//while(length > 4)	//  [6/23/2015 timj]
	while(length > 0)
	{
		pItm = new icCmenuItm8;
		if (pItm == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pItm->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pItm;
			return ret;
		}
		push_back(pItm);
		pItm = NULL;
	}// wend

	return ret;
}

void icCmenuItmList8::destroy()
{
	icCmenuItm8 *pI = NULL;
	vector<icCmenuItm8 *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			delete pI;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCmenuItmList8::out(void)
{
	icCmenuItm8 *pI = NULL;
	vector<icCmenuItm8 *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			pI->out();
		}
	}//next
}


//class aCmenuList : public aPayldType, public AmenuItemList_t // itemList;
aPayldType* icCmenuItmList8::generatePayload(aPayldType*payld)
{	
	aCmenuList  *pPlst;
	if (payld)
	{
		pPlst = (aCmenuList  *) payld;
	}
	else
	{
		pPlst = new aCmenuList;
	}

	aCmenuItem  locMI;
	icCmenuItm8 *pI = NULL;
	vector<icCmenuItm8 *>::iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = begin(); iT != end(); ++iT)
	{
		pI = (*iT);
		if ( pI != NULL )
		{
			pI->generateElement(&locMI);
			pPlst->push_back(locMI);
			locMI.clear();
		}
	}//next

	return pPlst;
}


icCmenuItmList8& icCmenuItmList8::operator=(const icCmenuItmList8& s)
{
	icCmenuItm8 *pmI = NULL, *pnewI = NULL;
	vector<icCmenuItm8 *>::const_iterator iT;//ptr2ptr2icCmenuItmA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pmI = (*iT);
		if ( pmI != NULL )
		{
			pnewI = new icCmenuItm8(*pmI);
			push_back(pnewI);pnewI = NULL;
		}
	}// next
	return *this;
}
