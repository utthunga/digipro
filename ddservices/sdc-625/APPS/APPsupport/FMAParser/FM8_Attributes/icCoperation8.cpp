/*************************************************************************************************
 *
 * filename  icCoperation8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Operation Attribute
 *
 */

#include "icCoperation8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Operation_Specifier ::=
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	CHOICE {
		operation	[2] ENUMERATED 
		{
			read	(1), 
			write	(2), 
			command	(3) 
		}
	}
}
*/

int icCoperation8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned) length)
	{
		LOGIT(CERR_LOG,"Error: Operation attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != OBJECT_TAG)
	{
		LOGIT(CERR_LOG,"Error: Operation attribute tag is not 2.\n)");
		return -2;
	}
    
	value = 0;
	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Operation attribute error in size extraction.\n)");
		}
		else
		{
			value = rawValue = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Operation attribute needs size but has no length.\n)");
		ret = -3;
	}

    return 0;
}



