/*************************************************************************************************
 *
 * filename  asCchartTypeSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Chart Type Specifier Attribute class
 *
 */

#ifndef _ASCchartTypeSPEC8_
#define _ASCchartTypeSPEC8_

#include "icCchartType8.h"

#include "..\FM8_conditional.h"
class asCchartTypeSpec8 : public FMx_Attribute
{
public:
	asCchartTypeSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_8_Conditional<icCchartType8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		icCchartType8* pattr = new icCchartType8;
		int ret = pattr->evaluateAttribute(pData, length, offset);
		conditional.MakeDirect(pattr);
		return ret;
	};
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCchartTypeSPEC8_


