/*************************************************************************************************
 *
 * filename  faCByteString8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Byte String Attribute
 *
 */

#include "logging.h"
#include "faCByteString8.h"
#include "..\FMx_Support_primatives.h"

int faCByteString8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;

	len = length;
	ret = parseByteString(pData, length, len, offset, &pStore);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: 8 ByteString has an error.\n)");
		return status;
	}

	return 0;
}
