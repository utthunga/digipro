/*************************************************************************************************
 *
 * filename  asCintegerSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Integer Specifier Attribute class
 *
 */

#ifndef _ASCINTEGERSPEC8_
#define _ASCINTEGERSPEC8_


#include "..\FM8_conditional.h"
#include "faCLong8.h"

class asCintegerSpec8 : public FMx_Attribute
{
public:
	asCintegerSpec8(int tag) : FMx_Attribute(tag) {};

public:
	fC_8_Conditional<faCLong8> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCINTEGERSPEC8_