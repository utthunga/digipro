/*************************************************************************************************
 *
 * filename  asCdefaultValuesList8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Default Value List Attribute class
 * Definition for version 8 Expression List Element Attribute class
 * Definition for version 8 Expression List Attribute class
 *
 */

#ifndef _asCdefaultValuesList8_
#define _asCdefaultValuesList8_

#include "..\FMx_Attributes\asCdefaultValuesList.h"


class icCexpressionListElem8 : public icCexpressionListElem
{
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCexpressionList8 : public icCexpressionList
{
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class asCdefaultValuesList8 : public asCdefaultValuesList
{
public: // construct / destruct
	asCdefaultValuesList8() {};
	virtual ~asCdefaultValuesList8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_asCdefaultValuesList8_



