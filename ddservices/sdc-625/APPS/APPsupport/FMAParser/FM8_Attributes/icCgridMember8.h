/*************************************************************************************************
 *
 * filename  icCgridMember8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Grid Member Attribute class
 *
 */

#ifndef _icCgridMember8_
#define _icCgridMember8_

#include "..\FMx_Attributes\icCgridMember.h"

#include "..\FM8_conditional.h"
#include "..\FM8_Attributes\faCString8.h"

class icCgridMember8 : public icCgridMember
{
public: // construct / destruct
	icCgridMember8() {};
	icCgridMember8(const icCgridMember8& src) { operator=(src); };
	virtual ~icCgridMember8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCgridMemberList8 : public vector<icCgridMember8 *>
{
public: // construct / destruct
	icCgridMemberList8() {};
	icCgridMemberList8(const icCgridMemberList8& src) { operator=(src); };
	virtual ~icCgridMemberList8() { destroy(); };
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCgridMemberList8&	operator=(const icCgridMemberList8& s);
};
#endif //_icCgridMember8_



