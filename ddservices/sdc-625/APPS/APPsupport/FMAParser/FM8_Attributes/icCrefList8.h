/*************************************************************************************************
 *
 * filename  icCrefList8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Reference List Attribute class
 *
 */

#ifndef _icCrefList8_
#define _icCrefList8_

#include "..\FMx_Attributes\icCrefList.h"

#include "..\FM8_conditional.h"
class icCrefListElem8 : public icCrefListElem
{
public: // construct / destruct
	icCrefListElem8() {};
	virtual ~icCrefListElem8() {};
	icCrefListElem8(const icCrefListElem8& src) { operator=(src); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

class icCrefList8 : public FMx_Attribute, vector<icCrefListElem8*>
{
public: // construct / destruct
	icCrefList8() {};
	icCrefList8(int tag) : FMx_Attribute(tag) {};
	icCrefList8(const icCrefList8& src) { operator=(src); };
	virtual ~icCrefList8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual int out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCrefList8&	operator=(const icCrefList8& s);
};

class asCrefList8 : public icCrefList8
{
public: // construct / destruct
	asCrefList8(int tag=0) : icCrefList8(tag) {};
	asCrefList8(const asCrefList8& src) 
	{ 
		const icCrefList8 *base = &src;
		icCrefList8::operator=(*base); 
	};
	virtual ~asCrefList8() {};


	virtual aCattrBase* generatePrototype()
	{
		aCgenericConditional* ddlcond = new aCgenericConditional;
		ddlcond->priExprType = eT_Direct;

		// hydrate the conditional internal
		aCgenericConditional::aCexpressDest edest;
		edest.destType = eT_Direct;//this destination type   - actually a clauseType_t  
		edest.pPayload = generatePayload();
		edest.pCondDest = NULL;
		ddlcond->destElements.push_back(edest);
		edest.destroy(); // we're assuming the push did a deep copy of the dest

		ddlcond->priExprType = eT_Direct; // - direct implies there is a single payload item in the dest_list
		
		aCattrReferenceList *pCondList = new aCattrReferenceList;
		pCondList->RefList.genericlist.push_back(*((aCcondList::oneGeneric_t*)ddlcond));

		pCondList->attr_mask = maskFromInt(attrType);

		return pCondList;
	}

public: // workers
	icCrefList8&	operator=(const icCrefList8& s);
};
#endif //_icCrefList8_



