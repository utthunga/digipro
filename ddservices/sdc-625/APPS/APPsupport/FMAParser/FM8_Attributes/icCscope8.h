/*************************************************************************************************
 *
 * filename  icCscope8.h
 *
 * 27 Aug 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Method Scope Attribute class
 *
 */

#ifndef _icCscope8_
#define _icCscope8_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCscope8: public FMx_Attribute
{
public: // construct 
	icCscope8() : value(0L) {};   // initialize underlying class data
    
public: // data
	UINT64 value;        // whatever data is included in the underlying class
    
public: // methods
	virtual void destroy(void){};// we have no pointers
	int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);

	virtual int out(void) 
    {
		FMx_Attribute::out();

        LOGIT(COUT_LOG,"					     Scope:\n");
        LOGIT(COUT_LOG,"					Bit String: 0x%016llx (%lld)\n",value,value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCbitString* pb = new aCbitString;
		pb->bitstringVal= value;

		return pb;
	}
};

#endif //_icCscope8_



