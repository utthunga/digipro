/*************************************************************************************************
 *
 * filename  icCmethType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Method Type Attribute class
 *
 */

#ifndef _icCmethType8_
#define _icCmethType8_

#include "..\FMx_Attributes\icCmethType.h"

#include "..\FM8_conditional.h"
#ifndef _FACBITSTRING8_	/* paths are different from different including entities */
#include "FM8_Attributes\faCBitString8.h"
#endif
#ifndef _ASCASCIISTR8_
#include "FM8_Attributes\asCasciiStr8.h"
#endif


class icCmethType8 : public icCmethType
{
public: // data	
	faCBitString8 modifiers;	// type modifier bitstring
	asCasciiStr8  methodName;

public: // construct / destruct
	icCmethType8() {}
	virtual ~icCmethType8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	
	virtual aCattrBase* generatePrototype()
	{
		aCparameter* pp = (aCparameter*)icCmethType::generatePrototype();
		pp->modifiers   = (int)modifiers.value;
		pp->paramName   = methodName.str;
		
		pp->attr_mask = maskFromInt(attrType);

		return pp;
	}
};
#endif //_icCmethType8_



