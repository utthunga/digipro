/*************************************************************************************************
 *
 * filename  asCasciiStr8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 ASCII String Attribute
 *
 */

#include "logging.h"
#include "asCasciiStr8.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
ASCII_String_Specifier ::=  [APPLICATION 57]
SEQUENCE {
		implicit_tag	Implicit_Tag
		len				Encoded_Integer	-- number of bytes in the string 		   		
		text			Byte_String	 		-- len bytes long � holds trailing null
}
*/

// this is equivelent to evalASCIIstring...type,len, string
int asCasciiStr8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: asCasciiStr attribute with should have implicit tag.\n)");
		return -1;
	}

	if (tagValue != NAME_STR_TAG)
	{
		LOGIT(CERR_LOG,"Error: asCasciiStr attribute ASCII string explicit tag is not 57.\n)");
		return -2;
	}

	// string length
	if (parseInteger(4, pData, length, tempLongLong, offset) != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: Unsigned Long attribute error in ASCII size extraction.\n)");
		return -3;
	}
	else
	{
		len = (unsigned long)tempLongLong;
	}

	// the string
	ret = parseByteString(pData, length, len, offset, &pStore);
	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Error: ASCII String has an extraction error.\n)");
		return ret;
	}

	str = (char *) pStore;

	if (len != (str.size()+1))// size() does not count trailing null
	{
		DEBUGLOG(CERR_LOG,"ASCII string size mismatch: wants %d got %d\n",len,str.size());
	}

	return ret;
}
