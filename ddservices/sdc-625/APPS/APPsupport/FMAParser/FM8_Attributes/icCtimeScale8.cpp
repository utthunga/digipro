/*************************************************************************************************
 *
 * filename  icCtimeScale8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Time Scale Attribute
 *
 */

#include "icCtimeScale8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

int icCtimeScale8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong;
	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > (unsigned) length)
	{
		LOGIT(CERR_LOG,"Error: asCtimeScaleSpec8 attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != TIME_SCALE_TAG)
	{
		LOGIT(CERR_LOG,"Error: asCtimeScaleSpec8 attribute ASCII string explicit tag is not 61.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Time Scaling attribute error in size extraction.\n)");
		}
		else
		{
			value = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Time Scaling attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}



