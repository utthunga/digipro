/*************************************************************************************************
 *
 * filename  asTransactionLst8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Transaction List Attribute
 *
 */

#include "logging.h"
#include "asTransactionLst8.h"
#include "icCtransaction8.h"
#include "..\FMx_Attributes\asTransactionLst.h"
#include "..\FMx_Support_primatives.h"

#include "tags_sa.h"

int asTransactionLst8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned   oneTransLen = 0;
	int          iTransLen = 0;
	RETURNCODE ret = 0;
	exitValues_t holdExit; 
	pushExitValues(holdExit, pData, length, offset);

	while (length > 0) // for each transaction
	{
		ret = parseExpectedExplicitTag(pData, length, offset, TRANSACTION_TAG, oneTransLen);

		if ( ret || oneTransLen == 0)
		{
			LOGIT(CERR_LOG,"Error: Transaction attribute tag/length is wrong.\n)");
			popExitValues(holdExit, pData, length, offset);
			ret = -1;
			break;
		}

		icCtransaction8* pTrans = new icCtransaction8;
		if (pTrans == NULL)
		{
			LOGIT(CERR_LOG,"Error: memory on icCtransaction8.\n)");
			popExitValues(holdExit, pData, length, offset);
			return -2;
		}


		length -= oneTransLen;
		iTransLen=oneTransLen;
		
		exitValues_t holdTransExit; 
		pushExitValues(holdTransExit, pData, iTransLen, offset);

		ret = pTrans->evaluateAttribute(pData, iTransLen, offset);

		if (ret || iTransLen != 0) 
		{
			LOGIT(CERR_LOG,"Error: Transaction parse failure.\n");
			popExitValues(holdTransExit, pData, length, offset);// always pop to correct exit vals
			delete pTrans;
			// continue to next transaxtion
		}
		else
		{
			transactionList.push_back(pTrans);
		}
//just covering up issues
//		popExitValues(holdTransExit, pData, length, offset);// always pop to correct exit vals

	}//wend length of transaction attribute

	if (holdExit.pExitData != (*pData))
	{
		LOGIT(CERR_LOG,"Transaction list pointer out of sync.\n");
		(*pData) = holdExit.pExitData;
	}
	if (holdExit.ExitLen != length)
	{
		LOGIT(CERR_LOG,"Transaction list ending length is incorrect.\n");
		length = holdExit.ExitLen;
	}
	if(holdExit.ExitOff != offset)
	{
		LOGIT(CERR_LOG,"Transaction list ending offset is incorrect.\n");
		offset = holdExit.ExitOff;
	}

	return ret;
}
