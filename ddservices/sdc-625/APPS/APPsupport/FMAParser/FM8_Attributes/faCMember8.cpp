/*************************************************************************************************
 *
 * filename  faCMember8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Member Attribute
 *
 */

#include "logging.h"
#include "faCMember8.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Defs.h"
#include "asCasciiStr8.h"
#include "tags_sa.h"

/*
Member ::= [STRUCTURE_TAG 83]
SEQUENCE 
{
	explicit-tag	Explicit_Tag,	-- tag = 83
	SEQUENCE  
	{
	name				INTEGER,  -- member�s numeric key
	member-symbol-index	INTEGER,  -- index into Symbol-Table (for name)
	item				Reference,-- reference is restricted by item-type
	description			String (OPTIONAL),-- required when help string 
	help				String (OPTIONAL) 
	}
}
*/

int faCMember8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, MEMBER_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, memberID))
		goto end;

	memberSymbolIndex = -1;		// no symbol table in FM8

	itemRef = new abCReference8();
	if (itemRef == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = itemRef->evaluateAttribute(pData, workingLength, offset))
		goto end;

	
	if (workingLength > 0)
	{
		asCasciiStr8 s;
		if (ret = s.evaluateAttribute(pData, workingLength, offset))
			goto end;
		symbolName = s.str.c_str();
	}

	if (workingLength > 0)
	{
		strDesc = new faCString8();
		if (strDesc == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = strDesc->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

	if (workingLength > 0)
	{
		strHelp = new faCString8();
		if (strHelp == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = strHelp->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}



faCMember8& faCMember8::operator=( const faCMember8& src )
{
	faCMember::operator=(src);
	return *this;
}



//=============================================================================================


int faCMemberList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<faCMember8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	faCMember8 *pElem;

	while(length > 4)
	{
		pElem = new faCMember8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void faCMemberList8::destroy()
{
	faCMember8 *pElem = NULL;
	vector<faCMember8 *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void faCMemberList8::out(void)
{
	faCMember8 *pElem = NULL;
	vector<faCMember8 *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* faCMemberList8::generatePayload(aPayldType*payld)
{	
	aCmemberElementList  *pPlst;
	if (payld)
	{
		pPlst = (aCmemberElementList  *) payld;
	}
	else
	{
		pPlst = new aCmemberElementList;
	}

	aCmemberElement  locElem;
	faCMember8 *pElem = NULL;
	vector<faCMember8 *>::iterator iT;//ptr2ptr2faCMemberA
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


faCMemberList8& faCMemberList8::operator=(const faCMemberList8& s)
{
	faCMember8 *pElem = NULL, *pnewElem = NULL;
	vector<faCMember8 *>::const_iterator iT;//ptr2ptr2faCMemberA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new faCMember8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}