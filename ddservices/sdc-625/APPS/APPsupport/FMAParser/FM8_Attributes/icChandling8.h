/*************************************************************************************************
 *
 * filename  icChandling8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Handling Attribute class
 *
 */

#ifndef _icChandling8_
#define _icChandling8_

#include "..\FMx_Attributes\icChandling.h"

#include "..\FM8_conditional.h"
class icChandling8 : public icChandling
{
public: // construct / destruct
	icChandling8() {}
	virtual ~icChandling8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icChandling8_



