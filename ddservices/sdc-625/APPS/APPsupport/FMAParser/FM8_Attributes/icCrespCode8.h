/*************************************************************************************************
 *
 * filename  icCrespCode8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Response Code Attribute class
 *
 */

#ifndef _icCrespCode8_
#define _icCrespCode8_

#include "..\FMx_Attributes\icCrespCode.h"

#include "..\FM8_conditional.h"
class icCrespCode8 : public icCrespCode
{
public: // construct / destruct
	icCrespCode8() {}
	icCrespCode8(const icCrespCode8& elem) {operator=(elem);};	// must have copy constructor 													
	virtual ~icCrespCode8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};


class icCrespCodeList8 : public vector<icCrespCode8 *>
{
public: // construct / destruct
	icCrespCodeList8() {};
	icCrespCodeList8(const icCrespCodeList8& src) { operator=(src); };
	virtual ~icCrespCodeList8() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCrespCodeList8&	operator=(const icCrespCodeList8& s);
};
#endif //_icCrespCode8_



