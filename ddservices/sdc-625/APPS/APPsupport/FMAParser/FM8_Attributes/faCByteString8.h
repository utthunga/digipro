/*************************************************************************************************
 *
 * filename  faCByteString8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Byte String Attribute class
 *
 */

#ifndef _FACBYTESTRINGA_
#define _FACBYTESTRINGA_

#include "..\FMx_Attributes\faCByteString.h"

class faCByteString8 : public faCByteString
{
public: // construct / destruct
	faCByteString8(int tag = 0) : faCByteString(tag) {}
	virtual ~faCByteString8() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACBYTESTRINGA_
