/*************************************************************************************************
 *
 * filename  icCmenuItm8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Menu Item Attribute class
 *
 */

#ifndef _icCmenuItm8_
#define _icCmenuItm8_

#include "..\FMx_Attributes\icCmenuItm.h"

#include "..\FM8_conditional.h"
class icCmenuItm8 : public icCmenuItm
{
public: // construct / destruct
	icCmenuItm8() {};
	icCmenuItm8(const icCmenuItm8& mI) { operator=(mI); };
	virtual ~icCmenuItm8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCmenuItm8& operator=(const icCmenuItm8& src);
};


class icCmenuItmList8 : public vector<icCmenuItm8 *>
{
public: // construct / destruct
	icCmenuItmList8() {}
	icCmenuItmList8(const icCmenuItmList8& src) { operator=(src); };
	virtual ~icCmenuItmList8() { destroy(); }
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	 icCmenuItmList8&	operator=(const icCmenuItmList8& s);
};

#endif //_icCmenuItm8_



