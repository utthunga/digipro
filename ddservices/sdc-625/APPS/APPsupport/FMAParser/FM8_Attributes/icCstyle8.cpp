/*************************************************************************************************
 *
 * filename  icCstyle8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Style Attribute
 *
 */

#include "icCstyle8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
from encode_menu_style_spec() in DDT-A

	chain = encode_integer_const(style);
*/

int icCstyle8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	PARSE_EXPECTED_IMPLICIT_TAG(MENU_STYLE_TAG, ret);

	if (ret == 0)
	{
		value = 0;
		ret = parseUnsigned(pData, length, offset, value);
	}
    
    return ret;
}



