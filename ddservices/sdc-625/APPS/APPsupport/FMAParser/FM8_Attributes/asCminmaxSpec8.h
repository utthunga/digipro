/*************************************************************************************************
 *
 * filename  asCminmaxSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Min Max Specifier Attribute class
 *
 */

#ifndef _ASCminmaxSPEC8_
#define _ASCminmaxSPEC8_

#include "icCminmax8.h"

class asCminmaxSpec8 : public FMx_Attribute
{
public:
	asCminmaxSpec8(int tag) : FMx_Attribute(tag)
	{ 
		attname = (tag == 0xa5)?"Min Value Spec":"Max Value Spec"; 
		expectedTag = tag;
	};

public:
	// note that this is for convienence only (to match FMA)
	// this condition is generated in the code as direct 
	//FM8_Conditional<icMinMax8> conditional;
	vector<icMinMax8> minmaxlist;	
	
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		int ret;
		while (length > 0)
		{
			icMinMax8 minmax;
			if (ret = minmax.evaluateAttribute(pData, length, offset))
				break;
			minmaxlist.push_back(minmax);
		}
		return ret;
	};

	virtual void destroy(void) 
	{ 
		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].destroy();
		}
	};

	virtual int out(void) 
	{
		FMx_Attribute::out(); 
		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].out();
		}
		return 0; 
	};

	virtual aCattrBase* generatePrototype()
	{		
		aCattrVarMinMaxList* attr = new aCattrVarMinMaxList;
		for (unsigned i=0; i< minmaxlist.size(); i++)
		{
			minmaxlist[i].generateElement(&(attr->ListOminMaxElements));
		}
			
		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCminmaxSPEC8_


