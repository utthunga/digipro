/*************************************************************************************************
 *
 * filename  icCrefList8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Reference List Attribute
 *
 */

#include "icCrefList8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
direct	[2] SEQUENCE OF Reference
we are not parsing the OBJECT_TAG (2) here
*/

int icCrefListElem8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{//isa vector<abCReference*>
	//unsigned tagLength;
	RETURNCODE  ret = 0;

	//if (ret = parseExpectedExplicitTag(pData, length, offset, REFERENCE_LIST_TAG_A, tagLength))
	//	return ret;

	//int			exitLength		= length;
	//unsigned	exitOffset		= offset;
	//UCHAR	   *exitPdata		= (*pData);
	//int			workingLength	= length;
	//int			exitLength		= length   - tagLength;
	//unsigned	exitOffset		= offset   + tagLength;
	//UCHAR	   *exitPdata		= (*pData) + tagLength;
	//int			workingLength	= tagLength;

	abCReference8 *pElem;

	//while(length > 4)
	while(length > 0)
	{
		pElem = new abCReference8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			goto end;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend


end:
	//*pData = exitPdata;
	//length = exitLength;
	//offset = exitOffset;

	return ret;
}

//=============================================================================================


int icCrefList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCrefListElem8>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCrefListElem8 *pElem;

	while(length > 0)
	{
		pElem = new icCrefListElem8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}


void icCrefList8::destroy()
{
	icCrefListElem8 *pElem = NULL;
	vector<icCrefListElem8 *>::iterator iT;//ptr2ptr2icCrefListElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	vector<icCrefListElem8*>::clear();//self vector
}

int icCrefList8::out(void)
{
	icCrefListElem8 *pElem = NULL;
	vector<icCrefListElem8 *>::iterator iT;//ptr2ptr2icCrefListElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next

	return 0;
}


aPayldType* icCrefList8::generatePayload(aPayldType*payld)
{	
	aCreferenceList  *pPlst;
	if (payld)
	{
		pPlst = (aCreferenceList  *) payld;
	}
	else
	{
		pPlst = new aCreferenceList;
	}

	aCreference  locElem;
	icCrefListElem8 *pElem = NULL;
	vector<icCrefListElem8 *>::iterator iT;//ptr2ptr2icCrefListElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generatePayload(pPlst);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCrefList8& icCrefList8::operator=(const icCrefList8& s)
{
	icCrefListElem8 *pElem = NULL, *pnewElem = NULL;
	vector<icCrefListElem8 *>::const_iterator iT;//ptr2ptr2icCrefListElemA
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCrefListElem8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}

//int icCrefList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
//{// isa vector of vector<icCrefListElem8>
//	RETURNCODE  ret = 0;
//	abCReference8 *p = NULL;
//
//	while (length > 0)
//	{
//		p = new abCReference8;
//		if (p == NULL)
//		{
//			LOGIT(CERR_LOG,"Error: icCrefList8 memory.\n");
//			return FMx_MEMFAIL;
//		}
//
//		ret = p->evaluateAttribute(pData, length, offset);
//		if (ret != SUCCESS)
//			return ret;
//
//		push_back(p);
//	}
//    																		
//	if (length > 0)
//	{																				
//		LOGIT(CERR_LOG,"Error: %s attribute with length bigger than available.\n)", attname);	
//		return -12;																	
//	}																				
//
//    return ret;
//}



