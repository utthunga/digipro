/*************************************************************************************************
 *
 * filename  icCrespCode8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Response Code Attribute
 *
 */

#include "icCrespCode8.h"
#include "faCString8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Response_Code ::= [APPLICATION 80]
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	SEQUENCE  {
		value				INTEGER,
		type				Response_Code_Type,
		description		String,
		help				String  (OPTIONAL)
	}
}
*/

int icCrespCode8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, RESPONSE_CODE_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	// value

	if (ret = parseUnsigned(pData, workingLength, offset, value))
		goto end;

	// type

	if (ret = parseUnsigned(pData, workingLength, offset, type))
		goto end;

	// description

	description = new faCString8();
	if (description == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = description->evaluateAttribute(pData, workingLength, offset))
		goto end;

	// optional help

	if (workingLength == 0)
		goto end;	

	help = new faCString8();
	if (help == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = help->evaluateAttribute(pData, workingLength, offset))
		goto end;

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}






//=============================================================================================


int icCrespCodeList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCrespCode8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCrespCode8 *pElem;

	while(length > 4)
	{
		pElem = new icCrespCode8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCrespCodeList8::destroy()
{
	icCrespCode8 *pElem = NULL;
	vector<icCrespCode8 *>::iterator iT;//ptr2ptr2icCrespCode8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCrespCodeList8::out(void)
{
	icCrespCode8 *pElem = NULL;
	vector<icCrespCode8 *>::iterator iT;//ptr2ptr2icCrespCode8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


aPayldType* icCrespCodeList8::generatePayload(aPayldType*payld)
{	
	aCrespCodeList  *pPlst;
	if (payld)
	{
		pPlst = (aCrespCodeList  *) payld;
	}
	else
	{
		pPlst = new aCrespCodeList;
	}

	icCrespCode8 *pElem = NULL;
	vector<icCrespCode8 *>::iterator iT;//ptr2ptr2icCrespCode8
	for( iT = begin(); iT != end(); ++iT)
	{
		aCrespCode  locRespCode;
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locRespCode);
			pPlst->push_back(locRespCode);
		}
	}//next

	return pPlst;
}


icCrespCodeList8& icCrespCodeList8::operator=(const icCrespCodeList8& s)
{
	icCrespCode8 *pElem = NULL, *pnewElem = NULL;
	vector<icCrespCode8 *>::const_iterator iT;//ptr2ptr2icCrespCode8
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCrespCode8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
