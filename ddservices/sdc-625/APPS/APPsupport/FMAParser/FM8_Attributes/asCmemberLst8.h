/*************************************************************************************************
 *
 * filename  asCmemberLst8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Member List Specifier Attribute class
 *
 */

#ifndef _ASCMEMBERLST8_
#define _ASCMEMBERLST8_

#include "faCMember8.h"
#include "..\FM8_conditional.h"

class asCmemberLst8 : public FMx_Attribute
{
public:
	asCmemberLst8(int tag=0) : FMx_Attribute(tag) {};  

public:
	
	fC_8_condList<faCMemberList8> conditional;


public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  

		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrMemberList* attr =  new aCattrMemberList;
		conditional.hydratePrototype(&attr->condMemberElemListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCMEMBERLST8_
