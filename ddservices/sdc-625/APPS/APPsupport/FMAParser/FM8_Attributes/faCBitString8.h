/*************************************************************************************************
 *
 * filename  faCBitString8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Bit String Attribute class
 *
 */

#ifndef _FACBITSTRING8_
#define _FACBITSTRING8_

#include "..\FMx_Attributes\faCBitString.h"
#include "..\FMA_Attributes_Ref&Expr.h"

class faCBitString8 : public faCBitString
{
public: // data
	int size; // version 8 bitstrings are given a size in lit80, nowhere else...

public: // construct / destruct
	faCBitString8() {size = 0;}
	virtual ~faCBitString8() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_FACBITSTRINGA_
