/*************************************************************************************************
 *
 * filename  asCvectorSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Vector Specifier Attribute class
 *
 */

#ifndef _ASCvectorSPEC8_
#define _ASCvectorSPEC8_

#include "..\FM8_Attributes\icCgridMember8.h"

#include "..\FM8_conditional.h"
#include "tags_sa.h"

class asCvectorSpec8 : public  FMx_Attribute
{
public:
	asCvectorSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_8_condList<icCgridMemberList8> conditional;
   
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrGridMemberList* attr = new aCattrGridMemberList;
		conditional.hydratePrototype(&attr->condGridMemberListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCvectorSPEC8_


