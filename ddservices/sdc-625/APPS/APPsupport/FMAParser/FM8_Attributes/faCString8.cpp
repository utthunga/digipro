/*************************************************************************************************
 *
 * filename  faCString8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 String Attribute
 *
 */

#include "logging.h"
#include "faCString8.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMA_Defs.h"
#include "ddbdefs.h"

int faCString8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;
	UINT64   tempLongLong;
	unsigned tagValue;
	unsigned tagLength;

	ret = parseTag(pData, length,   tagValue, tagLength, offset );// implicit type

	if (ret != SUCCESS || tagLength != 0 || tagValue >= STR8TYPE_COUNT )// implicit tag error
	{
		LOGIT(CERR_LOG,"Error: 8 String implicit tag has an error.\n)");
		return -3;
	}
	strRefType = (ddlstringType_t)tagValue; // was::>convert8_2_10[tagValue];

	switch (strRefType)
	{
		case ds_DevSpec:		//0
		{// int			
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Literal String mfg extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;	// mfg - should be ours

			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Literal String dev-type extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;	// dev-type - should be ours	

			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Literal String dev-rev extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;	// dev-rev - should be ours

			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Literal String dd-rev extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;	// dd-rev - should be ours

			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Literal String number extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;// the actual payload
		}
		break;

		case ds_Dict:     //3 
		{// int			
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: Dictionary table key extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;
		}
		break;

		case ds_VarRef:		//4
		{// ref		
			ret = strRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String reference extraction error.\n)");
				return ret;
			}
		}
		break;

		case ds_EnumRef:    //5
		{// ref + int	;
			ret = strRef->evaluateAttribute(pData, length, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum reference extraction error.\n)");
				return ret;
			}

			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum number extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;// which enum to read it from	
		}
		break;

		case ds_Var:    //1
		{// int + ref	;
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Variable number extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;// which enum to read it from	

			if (QANDA_8v8)
			{
				// do not delete the strRef pointer, faCString8 expects it
			}
			else
			{
				// turn it into a reference
				strRef->intValue = strInt;
				strRef->refType  = rT_Variable_id;  // this is required to be an ascii dd item

				// note we have changed the string type here!
				strRefType = ds_VarRef;
			}
		}
		break;

		case ds_Enum:    //2
		{// int + ref	;
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum number extraction error.\n)");
				return ret;
			}

			// turn the symbol number into a reference
			strRef->intValue = (int)tempLongLong;
// stevev 10nov2015 - version 8 reference needs to use the version 8 reference types
//			strRef->refType  = rT_variable_id;// this is required to be an enum dd item
			strRef->refType  = 0; //  VARIABLE_ID_REF;
							// note that the old version 8 puts zero here, so we'll do the same for now			
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: String Enum value extraction error.\n)");
				return ret;
			}
			strInt = (int)tempLongLong;// which enum value to read it from	
// stevev 10nov2015 - it's not a via-enum ref, the expresion is empty			strRef->pExpr = new abCExpression8(tempLongLong);
		}
		break;

		default:
		{
			LOGIT(CERR_LOG,"Error: Totally unknown string type. (0x%04x)\n", tagValue);
			return -4;
		}
		break;
	}

	return 0;
}
