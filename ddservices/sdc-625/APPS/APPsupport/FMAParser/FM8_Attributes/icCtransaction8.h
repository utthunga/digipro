/*************************************************************************************************
 *
 * filename  icCtransaction8.h
 *
 * 27 June 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version A Transaction Attribute class
 *
 * #include "icCtransaction8.h"
 *
 */

#ifndef _icCtransaction8_
#define _icCtransaction8_

#include "..\FMx_Attributes\icCtransaction.h"
#include "asCdataFieldSpec8.h"
#include "asCrespCodesSpec8.h"
#include "asCactionListSpec8.h"

class icCtransaction8 : public icCtransaction
{
public: // construct / destruct
	icCtransaction8() 	{};

	virtual ~icCtransaction8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCtransaction8_



