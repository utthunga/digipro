/*************************************************************************************************
 *
 * filename  asClineTypeSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Line Type Specifier Attribute class
 *
 */

#ifndef _ASClineTypeSPEC8_
#define _ASClineTypeSPEC8_

#include "..\FM8_Attributes\icClineType8.h"

#include "..\FM8_conditional.h"
class asClineTypeSpec8 : public  FMx_Attribute
{
public:
	asClineTypeSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_8_Conditional<icClineType8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrCondIntWhich* attr = new aCattrCondIntWhich;
		conditional.hydratePrototype(&attr->condIntWhich);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASClineTypeSPEC8_


