/*************************************************************************************************
 *
 * filename  icChandling8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Handling Attribute
 *
 */

#include "icChandling8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

#include "faCBitString8.h"

int icChandling8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	RETURNCODE  ret = 0;
 
	value = 0;
	if (length > 0)
	{
		faCBitString8 bitstring;
		if (bitstring.evaluateAttribute(pData, length, offset) != 0)
		{
			return -1;
		}

		value = bitstring.value;
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Handling attribute needs size but has no length.\n)");
		ret = -2;
	}

	return ret;
}



