/*************************************************************************************************
 *
 * filename  asCmethDef8.h
 *
 * 22 August 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Method Definition Attribute class
 *
 */

#ifndef _ASCmethDef8_
#define _ASCmethDef8_


class asCmethDef8 : public  faCByteString8
{
public:
	asCmethDef8(int tag = 0) : faCByteString8(tag) {};
    
public:
	// use base class evaluateAttribute
        
 /* this is a copy of the base class...lets use it.   
	virtual aCattrBase* generatePrototype()
    {
		
 		aCattrMethodDef* attr = new aCattrMethodDef;
		attr->blobLen = str.size();
		attr->pBlob   = new char[attr->blobLen+1];   // this class owns this memory
		strncpy(attr->pBlob, str.c_str(), attr->blobLen+1);

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
**/
};
#endif //_ASCmethDef8_


