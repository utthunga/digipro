/*************************************************************************************************
 *
 * filename  asCdisplaySize8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 DisplaySize Attribute
 *
 */

#include "logging.h"
#include "asCdisplaySize8.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Size_Specifier ::=  [APPLICATION 52]
SEQUENCE {
	implicit-tag	Implicit_Tag,
	ENUMERATED {
		XX_SMALL			(0),
		X_SMALL				(1),
		SMALL				(2),
		MEDIUM				(3),
		LARGE				(4),
		X_LARGE				(5),
		XX_LARGE			(6)
	}
}
*/

int asCdisplaySize8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

	value = 0;
 
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: Display Size attribute tag should be implicit.\n)");
		return -1;
	}

	if (tagValue != SCOPE_SIZE_TAG)
	{
		LOGIT(CERR_LOG,"Error: Display Size attribute tag is not 64.\n)");
		return -2;
	}


	if (length > 0)
	{
		ret = parseInteger(1, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: DisplaySize attribute error in size extraction.\n)");
		}
		else
		{
			value = rawValue = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: DisplaySize attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}
