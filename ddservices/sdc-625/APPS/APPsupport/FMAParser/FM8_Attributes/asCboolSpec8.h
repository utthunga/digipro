/*************************************************************************************************
 *
 * filename  asCboolSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 BOOL Specifier Attribute class
 *
 */

#ifndef _ASCBOOLSPEC8_
#define _ASCBOOLSPEC8_


#include "..\FM8_conditional.h"

#include "icCbool8.h"

class asCboolSpec8 : public FMx_Attribute
{
public:
	asCboolSpec8(int tag) : FMx_Attribute(tag) {};

public:
	fC_8_Conditional<icCBool8> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);
	};

	virtual void destroy(void)
	{	
		return conditional.destroy();
	};

	virtual int out(void)
	{
		FMx_Attribute::out();
		conditional.out();
		return 0;
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif // _ASCBOOLSPEC8_
