/*************************************************************************************************
 *
 * filename  asCintegerConst8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Integer Constant Attribute class
 *
 */

#ifndef _ASCINTEGERCONST8_
#define _ASCINTEGERCONST8_

#include "..\FMx_Attributes\asCintegerConst.h"

class asCintegerConst8 : public asCintegerConst
{
public:
	asCintegerConst8(int tag) : asCintegerConst(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);

	virtual bool inhibitOut() { 
		return (reconcile && attrType==ag_ValArr_length) ? true : false; 
	};

};

#endif //_ASCINTEGERCONST8_
