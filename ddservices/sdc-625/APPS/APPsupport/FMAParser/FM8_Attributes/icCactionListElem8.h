/*************************************************************************************************
 *
 * filename  icCactionListElem8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Action List Element Attribute class
 *
 */

#ifndef _icCactionListElem8_
#define _icCactionListElem8_

#include "..\FMx_Attributes\icCactionListElem.h"

#include "..\FM8_conditional.h"
#include "..\FM8_Attributes\faCByteString8.h"

class icCactionListElem8 : public icCactionListElem
{
public: // construct / destruct
	icCactionListElem8()
	{
		ref = new abCReference8();
		// method = new faCByteString8();
	}
	icCactionListElem8(const icCactionListElem8 &src) { operator=(src);};
	virtual ~icCactionListElem8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCactionListElem8& operator=(const icCactionListElem8& src)
	{
		choice = src.choice;
		if (src.ref)
		{
			abCReference8 *r = (abCReference8*) src.ref;
			ref = new abCReference8(*r);
		}
		return *this;
	};
};

class icCactionList8 : public vector<icCactionListElem8 *>
{
public: // construct / destruct
	icCactionList8() {};
	icCactionList8(const icCactionList8& src) { operator=(src); };
	virtual ~icCactionList8() { destroy(); };

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType*payld = 0);

public: // workers
	icCactionList8&	operator=(const icCactionList8& s);
};
#endif //_icCactionListElem8_



