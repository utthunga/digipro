/*************************************************************************************************
 *
 * filename  icCorient8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Orientation Attribute
 *
 */

#include "icCorient8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
	implicit GRID_ORIENT_TAG followed by integer
*/

int icCorient8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute expected implicit tag.\n)");
		return -1;
	}

	if (tagValue != GRID_ORIENT_TAG)
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute tag is not 51.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Orientation attribute error in size extraction.\n)");
		}
		else
		{
			value = rawValue = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Orientation attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}



