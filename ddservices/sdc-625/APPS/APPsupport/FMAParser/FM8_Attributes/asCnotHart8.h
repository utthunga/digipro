/*************************************************************************************************
 *
 * filename  asCnotHart8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Not Hart Attribute class
 *
 */

#ifndef _ASCNOTHART8_
#define _ASCNOTHART8_

#include "..\FMx_Attributes\asCnotHart.h"

class asCnotHart8 : public asCnotHart
{
public:
	asCnotHart8(int tag) : asCnotHart(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCNOTHART8_
