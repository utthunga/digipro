/*************************************************************************************************
 *
 * filename  icCgridMember8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Grid Member Attribute
 *
 */

#include "icCgridMember8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FM8_Attributes\faCString8.h"
#include "tags_sa.h"

/*
Grid_Member ::= [APPLICATION 88]
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	description		String,
	values			Grid_Value_List
	}
}
Grid_Value_List ::= [APPLICATION 56]
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	SEQUENCE OF 	Reference
*/

int icCgridMember8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, GRID_ELEMENT_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	description = new faCString8();
	if (description == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = description->evaluateAttribute(pData, workingLength, offset))
		goto end;


	if (ret = parseExpectedExplicitTag(pData, length, offset, GRID_MEMBERS_TAG, tagLength))
		goto end;

	workingLength	= tagLength;
	while (workingLength > 0)
	{
		abCReference8 *ref = new abCReference8();
		if (ref == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = ref->evaluateAttribute(pData, workingLength, offset))
			goto end;

		values.push_back(ref);
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}




//=============================================================================================


int icCgridMemberList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCgridMember8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCgridMember8 *pElem;

	while(length > 4)
	{
		pElem = new icCgridMember8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCgridMemberList8::destroy()
{
	icCgridMember8 *pElem = NULL;
	vector<icCgridMember8 *>::iterator iT;//ptr2ptr2icCgridMember8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCgridMemberList8::out(void)
{
	icCgridMember8 *pElem = NULL;
	vector<icCgridMember8 *>::iterator iT;//ptr2ptr2icCgridMember8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCgridMemberList8::generatePayload(aPayldType*payld)
{	
	aCgridMemberList  *pPlst;
	if (payld)
	{
		pPlst = (aCgridMemberList  *) payld;
	}
	else
	{
		pPlst = new aCgridMemberList;
	}

	aCgridMemberElement  locElem;
	icCgridMember8 *pElem = NULL;
	vector<icCgridMember8 *>::iterator iT;//ptr2ptr2icCgridMember8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCgridMemberList8& icCgridMemberList8::operator=(const icCgridMemberList8& s)
{
	icCgridMember8 *pElem = NULL, *pnewElem = NULL;
	vector<icCgridMember8 *>::const_iterator iT;//ptr2ptr2icCgridMember8
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCgridMember8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}
