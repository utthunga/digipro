/*************************************************************************************************
 *
 * filename  faCBitString8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Bit String Attribute
 *
 */

#include "logging.h"
#include "faCBitString8.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"

int faCBitString8::evaluateAttribute(UCHAR** pData, int& size, unsigned& offset)
{
	RETURNCODE ret = parseBitString(pData, size, rawValue, offset);
	value = rawValue;
	return ret;
}

