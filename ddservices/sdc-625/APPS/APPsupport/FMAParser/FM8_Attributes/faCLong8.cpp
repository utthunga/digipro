/*************************************************************************************************
 *
 * filename  faCLong8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Long Attribute
 *
 */

#include "logging.h"
#include "faCLong8.h"
#include "..\FMx_Support_primatives.h"
#include "..\FMx_Base.h"

int faCLong8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64 tempLongLong ;
	RETURNCODE  ret = 0;

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Unsigned Long attribute error in size extraction.\n)");
		}
		else
		{
			value = (unsigned long)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Unsigned Long attribute needs size but has no length.\n)");
		ret = -2;
	}

	return ret;
}

