/*************************************************************************************************
 *
 * filename  icCaxisScal8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Axis Scale Attribute
 *
 */

#include "icCaxisScal8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Axis_Scaling ::=
ENUMERATED {
	linear			(0),
	logarithmic		(1)
}
*/

int icCaxisScal8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
	RETURNCODE  ret = 0;

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Axis Scaling attribute error in size extraction.\n)");
		}
		else
		{
			value = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Axis Scaling attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}



