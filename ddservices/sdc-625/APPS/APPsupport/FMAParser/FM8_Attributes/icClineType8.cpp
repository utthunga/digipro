/*************************************************************************************************
 *
 * filename  icClineType8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Line Type Attribute
 *
 */

#include "icClineType8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Line_Type ::=
SEQUENCE {
	explicit-tag		Explicit_Tag,  NOTE: there is no explicit tag i the encoding
	CHOICE {
		data-n				[1]	INTEGER,
		low-low-limit		[2]	NULL,
		low-limit			[3]	NULL,
		high-limit			[4]	NULL,
		high-high-limit		[5]	NULL,
		transparent			[6]	NULL
	}
}
*/

int icClineType8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;
 
	ret = parseUnsigned(pData, length, offset, value);		// int
	
	if (value == 1)
		ret = parseUnsigned(pData, length, offset, choice);	// which
	else
		choice = 0;
	
    
    return ret;
}



