/*************************************************************************************************
 *
 * filename  asCuuid8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 UUID Attribute
 *
 */

#include "asCuuid8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
UUIdentifier ::= [STRUCTURE_TAG 63]
SEQUENCE 
{
  implicit-tag 		Implicit_Tag, 	 -- tag = 56
  upper_half 		INTEGER,        -- leftmost 16 hex digits
  lower_half 		INTEGER         -- rightmost 16 hex digits
}
*/

int asCuuid8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	if (ret = parseExpectedImplicitTag(pData, length, offset, UUID_TAG))
		return ret;

	if (ret = parseUINT64(pData, length, offset, upper_half))
		goto end;

	if (ret = parseUINT64(pData, length, offset, lower_half))
		goto end;

end:

	return ret;
}



