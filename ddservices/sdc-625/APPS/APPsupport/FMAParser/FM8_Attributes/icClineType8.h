/*************************************************************************************************
 *
 * filename  icClineType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Line Type Attribute class
 *
 */

#ifndef _icClineType8_
#define _icClineType8_

#include "..\FMx_Attributes\icClineType.h"

#include "..\FM8_conditional.h"
class icClineType8 : public icClineType
{
public: // construct / destruct
	icClineType8() {}
	virtual ~icClineType8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icClineType8_



