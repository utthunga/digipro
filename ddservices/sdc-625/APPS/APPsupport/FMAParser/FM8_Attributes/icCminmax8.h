/*************************************************************************************************
 *
 * filename  icMinMax8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Min Max Attribute class
 *
 */

#ifndef _icCminmax8_
#define _icCminmax8_

#include "..\FMx_Attributes\icCminmax.h"
#include "asCexprSpec8.h"
#include "TAGS_SA.H"
#include "FMx_Support_primatives.h"

class icMinMax8 : public icMinMax<asCexprSpec8>
{
public:
	icMinMax8() {};
	virtual ~icMinMax8() {};// i have no memory
	icMinMax8(const icMinMax8 &src) { icMinMax<asCexprSpec8>::operator =(src); };
	void destroy() { icMinMax<asCexprSpec8>::destroy(); };
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
	{
		unsigned tagLength;
		unsigned tagTag;
		UINT64 tmpLongLong;
		int ret;

		if (ret = parseTag(pData, length, tagTag, tagLength, offset))
		{
			LOGIT(CERR_LOG,
					"Error: MinMaxValue attribute error in application tag extraction.\n)");
			return ret;
		}

		if (tagTag != MAX_VALUE_TAG && tagTag != MIN_VALUE_TAG)
		{
			LOGIT(CERR_LOG,"Error: MinMaxValue attribute wrong type.\n)");
			return ret;
		}

		length -= tagLength;

		while (tagLength > 0)
		{
			minmaxValue<asCexprSpec8> Value;

			parseInteger(1, pData, (int&)tagLength, tmpLongLong, offset);
			Value.which = (int)tmpLongLong;

			ret = Value.Expression.evaluateAttribute(pData, (int&)tagLength, offset);

			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error: MinMaxValue attribute error in size extraction.\n)");
			}

			values.push_back(Value);
			Value.clear();				
		}

		return 0;
	}
};

#endif //_icCminmax8_


