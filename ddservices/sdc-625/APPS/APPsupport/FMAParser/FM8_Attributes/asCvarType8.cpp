/*************************************************************************************************
 *
 * filename  asCvarType8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Variable Type Attribute
 *
 */

#include "logging.h"
#include "asCvarType8.h"
#include "..\FMx_Support_primatives.h"

int asCvarType8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	UINT64   tempLongLong;
	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

#ifdef _DEBUG
	UCHAR* pEntry   = *pData;
	int entrylen = length;
#endif

	// explicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset );

	bool implicit = (tagLength == 0) && (ret == 0);
	
	if (tagLength > (unsigned)length )
	{
		LOGIT(CERR_LOG,"Error: VarType attribute with tag's length bigger than available.\n)");
		return -1;
	}

	varType = (var_t)tagValue;

	switch(varType)
	{
		case vt_integer:		// 2 INTEGER  size
		case vt_unsigned:		// 3 INTEGER  size
		case vt_enum:			// 6 INTEGER  size
		case vt_bitEnum:		// 7 INTEGER  size
		case vt_index:			// 8 INTEGER  size
		// these have explicit type plus implicit 0, followed by integer size
		{			
			// explicit tag // ReturnLen is zero (with no error) on an implicit tag
			ret = parseTag(pData, length,   tagValue,   tagLength, offset );// expect 0

			ret = parseInteger(4, pData, length, tempLongLong, offset);// size value
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error:VarType's integer error in size extraction.\n)");
			}
			else
			{
				varSize = (int)tempLongLong;
			}
#ifdef _DEBUG
			if (implicit)
			{
				LOGIT(CERR_LOG,"Error:VarType's tag encoding is implicit.\n)");
			}
#endif
		}
		break;

		case vt_ascii:			// 9 INTEGER  size				
		case vt_pkdAscii:	    //10 INTEGER  size				
		case vt_password:		//11 INTEGER  size
		// these have an implicit type followed by an integer size
		{		
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			if (ret != SUCCESS)
			{
				LOGIT(CERR_LOG,"Error:VarType's integer error in string size extraction.\n)");
			}
			else
			{
				varSize = (int)tempLongLong;
			}
#ifdef _DEBUG
			if (! implicit)
			{
				LOGIT(CERR_LOG,"Error:VarType's tag encoding is not implicit 4 string.\n)");
			}
#endif
		}
		break;

		case vt_time_value:		//20 INTEGER  size		
		{// implicit type, no size
			if (varType == vt_time_value)
			{
				varSize = 4;// version 8 has a set size
			}
		}
		break;

		case vt_float:			// 4 
		{// v8=explicit tag with zero length ...version 10=implicit size
			varSize = sizeof(float);	// FLOATCHANGE no change required [1/20/2016 tjohnston]
		}
		break;

		case vt_double:			// 5	 
		{// v8=explicit tag with zero length ...version 10=implicit size
			varSize = sizeof(double);
		}
		break;			

		case vt_date:			//13///		 
		{// v8=implicit size ...version 10=implicit size
			varSize = SIZEOF_DATE;
		}
		break;	

		case vt_bit_string:		//12/// INTEGER  size
		case vt_octet_string:	//18/// INTEGER  size
		case vt_visible_string:	//19/// INTEGER  size	
		{// not supported, needs an integer
			notHART = true;
			ret = 5;
		}
		break;	

		case vt_time:			//14///
		case vt_date_and_time:  //15///
		case vt_duration:		//16///
		case vt_euc:			//17///
		case vt_object_reference://21////
		{// not supported
			notHART = true;
			ret = 6;
		}
		break;

		case vt_no_type:		// 0///
		case vt_undefined:		// 1///
		default:
		{
			if (tagValue > 1)// default, bad type
			{
				LOGIT(CERR_LOG,"Error: VarType is not allowed (%d).\n)",tagValue);
				ret = -3;
			}
			else // illigal, unknown type
			{
				LOGIT(CERR_LOG,"Error: VarType is illegal (%d).\n)",tagValue);
				ret = -4;
			}
		}
		break;
	}//endswitch

	return ret;
}
