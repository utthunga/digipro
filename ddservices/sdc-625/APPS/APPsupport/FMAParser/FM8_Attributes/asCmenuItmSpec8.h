/*************************************************************************************************
 *
 * filename  asCmenuItmSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Menu Item Specifier Attribute class
 *
 */

#ifndef _ASCmenuItmSPEC8_
#define _ASCmenuItmSPEC8_

#include "..\FM8_Attributes\icCmenuItm8.h"

#include "..\FM8_conditional.h"

class asCmenuItmSpec8 : public  FMx_Attribute
{
public:
	asCmenuItmSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_8_condList<icCmenuItmList8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrMenuItems* attr = new aCattrMenuItems;
		conditional.hydratePrototype(&attr->condMenuListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCmenuItmSPEC8_


