/*************************************************************************************************
 *
 * filename  icCchartType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Chart Type Attribute class
 *
 */

#ifndef _icCchartType8_
#define _icCchartType8_

#include "..\FMx_Attributes\icCchartType.h"
#include "..\FMA_conditional.h"

class icCchartType8 : public icCchartType
{
public: // construct / destruct
	icCchartType8() {}
	virtual ~icCchartType8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCchartType8_



