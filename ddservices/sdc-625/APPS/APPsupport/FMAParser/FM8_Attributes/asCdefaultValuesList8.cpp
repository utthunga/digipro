/*************************************************************************************************
 *
 * filename  asCdefaultValuesList8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Implementation for version 8 Default Value List Attribute
 *
 */

#include "..\FM8_Attributes_Ref&Expr.h"
#include "asCdefaultValuesList8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Default_Values_List  ::= [STRUCTURE_TAG 54]
SEQUENCE  {
	explicit-tag	Explicit_Tag,		-- tag = 54
	SEQUENCE OF SEQUENCE
	{	<count determined by explicit-tag.explicit_length>
		data-item		Reference;				-- to a data holding item
		data-value		Expression_List;		-- constant expressions Only
	}
}

Expression_List ::= [STRUCTURE_TAG 98]
SEQUENCE  
{
	explicit-tag	Explicit_Tag,		-- tag = 98
	SEQUENCE OF CHOICE  
	{	<count determined by Expression_List.explicit-tag.length>
		<which determined by Explicit structure Tag od list Elelment>
		single_expr		Expression;			-- structure-tag 52
		listOf_expr		Expression_List;	-- structure-tag 98
	}
}

*/

int asCdefaultValuesList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, DEFAULT_VALUES_LIST_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;

	int workingLength = tagLength;

	while (workingLength > 0)
	{
		abCReference8 *ref = new abCReference8();
		if (ref == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = ref->evaluateAttribute(pData, workingLength, offset))
		{
			ret = -4;
			goto end;
		}
		refs.push_back(ref);

		icCexpressionList8 *exlist = new icCexpressionList8();
		if (exlist == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -5;
			goto end;
		}
		if (ret = exlist->evaluateAttribute(pData, workingLength, offset))
		{
			ret = -6;
			goto end;
		}

		values.push_back(exlist);
	}
	assert(workingLength == 0);

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}



int icCexpressionList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, EXPRESSION_LIST_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;

	int workingLength = tagLength;

	while (workingLength > 0)
	{
		icCexpressionListElem8 *elem = new icCexpressionListElem8();
		if (elem == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}		
		if (ret = elem->evaluateAttribute(pData, workingLength, offset))
		{
				ret = -4;
				goto end;
		}

		exprElemList.push_back(elem);
	}
	assert(workingLength == 0);

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}



int icCexpressionListElem8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;
 
	// look ahead at next tag value, masking high-order EXPLICIT tag bit
	UCHAR nextTag = 0x7f & **pData;
	if (nextTag == EXPRESSION_TAG)
	{
	expr = new abCExpression8();
	if (expr == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = expr->evaluateAttribute(pData, length, offset))
	{
			ret = -4;
		goto end;
	}
}
	else if (nextTag == EXPRESSION_LIST_TAG)
{
	exprList = new icCexpressionList8();
	if (exprList == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -5;
		goto end;
	}
	if (ret = exprList->evaluateAttribute(pData, length, offset))
	{
			ret = -6;
			goto end;
		}
	}
	else
	{
		assert(false);	// can't be here
	}

end:

	return ret;
}