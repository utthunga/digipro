/*************************************************************************************************
 *
 * filename  asCasciiStr8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 ASCII String Attribute class
 *
 */

#ifndef _ASCASCIISTR8_
#define _ASCASCIISTR8_

#include "..\FMx_Attributes\asCasciiStr.h"

class asCasciiStr8 : public asCasciiStr
{
public: // construct / destruct
	asCasciiStr8(int y = 0) : asCasciiStr(y) {};

	virtual ~asCasciiStr8() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCASCIISTR8_
