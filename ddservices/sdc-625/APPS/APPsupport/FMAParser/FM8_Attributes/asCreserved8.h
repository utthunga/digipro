/*************************************************************************************************
 *
 * filename  asCreserved8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Reserved Attribute class
 *
 */

#ifndef _ASCRESERVEDTYPE8_
#define _ASCRESERVEDTYPE8_

#include "..\FMx_Attributes\asCreserved.h"

class asCreserved8 : public asCreserved
{
public:
	asCreserved8(int tag) : asCreserved(tag) {};

public:
	virtual int evaluateAttribute (UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCRESERVEDTYPE8_
