/*************************************************************************************************
 *
 * filename  asCorientSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Orientation Specifier Attribute class
 *
 */

#ifndef _ASCorientSPEC8_
#define _ASCorientSPEC8_

#include "icCorient8.h"

#include "..\FM8_conditional.h"
class asCorientSpec8 : public FMx_Attribute
{
public:
	asCorientSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_8_Conditional<icCorient8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		icCorient8* pattr = new icCorient8;

		int ret = pattr->evaluateAttribute(pData, length, offset);
		conditional.MakeDirect(pattr);
		return ret;
	};
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
 		aCattrCondLong* attr = new aCattrCondLong;
		conditional.hydratePrototype(&attr->condLong);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};
#endif //_ASCorientSPEC8_


