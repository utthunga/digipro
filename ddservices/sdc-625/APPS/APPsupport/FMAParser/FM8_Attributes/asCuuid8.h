/*************************************************************************************************
 *
 * filename  asCuuid8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 UUID Attribute class
 *
 */

#ifndef _asCuuid8_
#define _asCuuid8_

#include "..\FMx_Attributes\asCuuid.h"

#include "..\FM8_conditional.h"
class asCuuid8 : public asCuuid
{
public: // construct 
	asCuuid8() {  };   // initialize underlying class data
	virtual ~asCuuid8() {}
    
public: // data
    
public: // methods    
 
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_asCuuid8_



