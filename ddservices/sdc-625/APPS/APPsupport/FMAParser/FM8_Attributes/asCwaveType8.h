/*************************************************************************************************
 *
 * filename  asCwaveType8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Wave Type Attribute class
 *
 */

#ifndef ASCWAVETYPE8
#define ASCWAVETYPE8

#include "..\FMx_Attributes\asCwaveType.h"

class asCwaveType8 : public asCwaveType
{
public: // construct
	asCwaveType8(int tag) : asCwaveType(tag) {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //ASCWAVETYPE8
