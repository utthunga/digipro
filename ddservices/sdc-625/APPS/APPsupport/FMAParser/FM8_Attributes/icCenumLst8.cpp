/*************************************************************************************************
 *
 * filename  icCenumLst8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 ENum List Attribute
 * Implementation for version 8 Status Classes Helper
 *
 */

#include "faCLong8.h"
#include "faCString8.h"
#include "icCenumLst8.h"
#include "icCclass8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "..\FM8_Attributes_Ref&Expr.h"
#include "tags_sa.h"

#include "ddldefs.h"

/*
Enumeration ::= [APPLICATION 77]
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	SEQUENCE  {
		value		[0] INTEGER,						-- implicit tag
		status		[1] Status_Classes (OPTIONAL),		-- explicit tag
		actions		[2] Reference (OPTIONAL),			-- implicit tag
		description	[3] String,							-- implicit tag
		help		[4] String (OPTIONAL),				-- implicit tag
		class		[5] Class (OPTIONAL)				-- implicit tag
	}
}
*/

int icCstatusClasses8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	RETURNCODE  ret = 0;

	// base class

	faCLong8 bc;
	if (ret = bc.evaluateAttribute(pData, length, offset))
		goto end;

	baseClass = bc.value;

	// zero or more output classes

	bool first = true;
	while (length > 0)
	{
		faCLong8 kclass;
		if (ret = kclass.evaluateAttribute(pData, length, offset))
			goto end;

		// a single zero is encoded after the baseClass if there are no output classes
		if (first)
		{
			first = false;
			if (kclass.value == 0)
				goto end;
		}

		// output class

		outputClass_t oc;

		oc.kindAndClass = kclass.value;
		oc.kind         = oc.kindAndClass & 0x7;
		oc.output_class = (oc.kindAndClass >> 3) & 0x03;// combined mode & reliability

		// read additional 'which' for OC_DV, OC_TV, OC_AO
		// if (oc.kind != OC_ALL)		   
		if (oc.kind != 0 && length > 0)
		{							   
			faCLong8 w;
			if (ret = w.evaluateAttribute(pData, length, offset))
				goto end;

			oc.which = w.value;
		}
		else
		{
			oc.which = 0;
		}

		outputClasses.push_back(oc);
	}

end:

	return ret;
}

int icCenumElem8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	unsigned tagValue;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, ENUMERATOR_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	while (workingLength > 0)
	{
		FMx_Attribute *attr = 0;
		if (ret = parseTag(pData, workingLength,   tagValue, tagLength, offset ))
			goto end;

		switch (tagValue)
		{
		case 0:
			attr = new faCLong8();
			break;

		case 1:
			attr = new icCstatusClasses8();
			break;

		case 2:
			attr = new abCReference8();
			break;
		
		case 3:
		case 4:
			attr = new faCString8();
			break;

		case 5:
			attr = new icCclass8();
			break;

		default:
			assert(0);
			ret = -4;
			goto end;
		};

		if (attr == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}

		if (tagValue == 1)
		{
			// only status class has explicit length
			int len = tagLength;
			if (ret = attr->evaluateAttribute(pData, len, offset))
				goto end;
			workingLength -= tagLength;
		}
		else
		{
			if (ret = attr->evaluateAttribute(pData, workingLength, offset))
				goto end;
		}
			
		fields[tagValue] = attr;
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}


icCenumElem8& icCenumElem8::operator=(const icCenumElem8& src)
{
	icCenumElem::operator=(src);
	return *this;
}

//=============================================================================================


int icCenumLst8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCenumElem8*>
	RETURNCODE  ret = 0;
	int			entryLength	= length;

	while (length > 0)
	{
		icCenumElem8 *pElem = new icCenumElem8;
		if (pElem == NULL)
		{
			ret = FMx_MEMFAIL;
			goto end;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			goto end;
		}
		push_back(pElem);
		pElem = NULL;
	}  // wend

end:

	return ret;
}

void icCenumLst8::destroy()
{
	icCenumElem8 *pElem = NULL;
	vector<icCenumElem8 *>::iterator iT;//ptr2ptr2icCenumElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCenumLst8::out(void)
{
	icCenumElem8 *pElem = NULL;
	vector<icCenumElem8 *>::iterator iT;//ptr2ptr2icCenumElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmenuList : public aPayldType, public AmenuItemList_t // itemList;
aPayldType* icCenumLst8::generatePayload(aPayldType* payld)
{	
	aCenumList  *pPlst;
	if (payld)
	{
		pPlst = (aCenumList  *)payld;
	}
	else
	{
		pPlst = new aCenumList;
	}

	icCenumElem8 *pElem = NULL;
	vector<icCenumElem8 *>::iterator iT;//ptr2ptr2icCenumElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			aCenumDesc  locEnumDesc;
			pElem->generateElement(&locEnumDesc);
			pPlst->push_back(locEnumDesc);
		}
	}//next

	return pPlst;
}


icCenumLst8& icCenumLst8::operator=(const icCenumLst8& src)
{
	icCenumElem8 *pElem = NULL;
	vector<icCenumElem8 *>::const_iterator iT;//ptr2ptr2icCenumElem8
	for( iT = src.begin(); iT != src.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			push_back(new icCenumElem8(*pElem));
		}
	}// next
	return *this;
}

