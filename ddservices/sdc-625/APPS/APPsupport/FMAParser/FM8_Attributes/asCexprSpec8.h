/*************************************************************************************************
 *
 * filename  asCexprSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Expression Specifier Attribute class
 *
 */

#ifndef _ASCEXPRSPEC8_
#define _ASCEXPRSPEC8_

#include "..\FMA_conditional.h"
#include "asCexpr8.h"
#include "..\FMx_Attributes\asCexprSpec.h"

class asCexprSpec8 : public asCexprSpec
{
public:
	asCexprSpec8(int tag = 0) : asCexprSpec(tag) {attname = "Expression";};
	asCexprSpec8(const asCexprSpec8& src) { operator=(src); };
	asCexprSpec8& operator=(const asCexprSpec8& s) 
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&s)));
		conditional = s.conditional; return *this;
	};

public:
	fC_8_Conditional<asCexpr8> conditional;

public:
	void clear() { conditional.destroy(); };
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		RETURNCODE  ret = SUCCESS;

		ret = parseExpectedTag(pData, length, offset);// conditioned on expected tag exists

		if (ret == SUCCESS)
			ret = conditional.evaluateAttribute(pData, length, offset);  

		return ret;
	}; 

	virtual void destroy(void)
	{	
		conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual aCattrBase* generatePrototype()
	{
		aCattrCondExpr* attr = new aCattrCondExpr;
		conditional.hydratePrototype(&attr->condExpr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};

	/* stevev 04mar16 - try making this consistent...
	was::   void hydratePrototype(aCgenericConditional* cond) ***/
	void hydratePrototype(aCondDestType* cond)
	{
		/* stevev 04mar16 - try making this consistent
		removed all the casting here         	***/
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	}
};

#endif //_ASCEXPRSPEC8_