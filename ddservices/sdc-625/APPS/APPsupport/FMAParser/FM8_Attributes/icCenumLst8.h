/*************************************************************************************************
 *
 * filename  icCenumLst8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 ENum List Attribute class
 * Definition for version 8 Status Class List Attribute class
 *
 */

#ifndef _icCenumLst8_
#define _icCenumLst8_

#include "..\FMx_Attributes\icCenumLst.h"

#include "..\FM8_conditional.h"

class icCstatusClasses8 : public icCstatusClasses
{
public: // construct 
	icCstatusClasses8() {};   // initialize underlying class data
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);

};


class icCenumElem8 : public icCenumElem
{
public: // construct / destruct
	icCenumElem8() {};
	icCenumElem8(const icCenumElem8& x) { operator=(x); };
	virtual ~icCenumElem8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	icCenumElem8& operator=(const icCenumElem8& src);
};


class icCenumLst8 : public vector<icCenumElem8 *>
{
public: // construct / destruct
	icCenumLst8() {}
	icCenumLst8(const icCenumLst8& src) { operator=(src); };
	virtual ~icCenumLst8() { destroy(); }

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	virtual void destroy();
	virtual void out(void);

	virtual aPayldType* generatePayload(aPayldType* payld = 0);

public: // workers
	icCenumLst8&	operator=(const icCenumLst8& s);
};
#endif //_icCenumLst8_



