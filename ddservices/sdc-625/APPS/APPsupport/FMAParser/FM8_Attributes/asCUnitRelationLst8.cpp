/*************************************************************************************************
 *
 * filename  asCUnitRelationLst8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Unit Relation Attribute
 *
 */

#include "logging.h"

#include "asCUnitRelationLst8.h"
#include "..\FMx_Support_primatives.h"
#include "..\AttributeString.h"


int asCUnitRelationLst8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	exitValues_t holdExit; 
	pushExitValues(holdExit, pData, length, offset);

	int ret = unit->evaluateAttribute(pData, length, offset);

	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Relation unit evaluate failed");

		popExitValues(holdExit, pData, length, offset);
		return -1;
	}

	ret = vars->evaluateAttribute(pData, length, offset);

	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Relation vars list evaluate failed");

		popExitValues(holdExit, pData, length, offset);
		return -2;
	}

	if (length != 0)
	{
		LOGIT(CERR_LOG,"Relation length error");

		return -3;
	}

	return 0;
}

int asCUnitRelationLst8::reconcile_out(void)
{
	FMx_Attribute::out();
	if (unit)
		unit->out();

	// here, we are making the UnitRelationLst "look like" two attrs
	// to emulate FMA's two actual attributes.  This is for reconcile only.

	LOGIT(COUT_LOG,
	"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
	LOGIT(COUT_LOG," Attribute: %s\n\n", "Update List");	// TODO: get rid of literal string
	if (vars)
		vars->out();

	return 0;
}
