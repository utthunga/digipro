/*************************************************************************************************
 *
 * filename  asCUnitRelationLst8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Unit Relation List 8 Attribute class
 *
 * #include "asCUnitRelationLst8.h"
 *
 */

#ifndef _ASCUNITRELATIONLST8_
#define _ASCUNITRELATIONLST8_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes\asCUnitRelationLst.h"
#include "asCrefSpec8.h"
#include "asCrefListSpec8.h"

class asCUnitRelationLst8 : public asCUnitRelationLst
{
public:
	asCUnitRelationLst8(int tag) : asCUnitRelationLst(tag) 
	{
		unit = new asCrefSpec8(0, true);
		vars = new asCrefListSpec8(0);
	};

	~asCUnitRelationLst8()
	{
		delete unit;
		delete vars;
	};

public:
	virtual int reconcile_out(void);
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASCUNITRELATIONLST8_