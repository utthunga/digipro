/*************************************************************************************************
 *
 * filename  asTransactionLst8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Transaction List Attribute class
 *
 */

#ifndef _ASTRANSACTIONLST8_
#define _ASTRANSACTIONLST8_

#include "..\FMx_Attributes\asTransactionLst.h"

class asTransactionLst8 : public asTransactionLst
{
public:
	asTransactionLst8(int tag) : asTransactionLst(tag) { attname = "Transaction List";};

public:
	int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ASTRANSACTIONLST8_