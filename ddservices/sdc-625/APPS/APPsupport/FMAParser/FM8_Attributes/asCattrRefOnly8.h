/*************************************************************************************************
 *
 * filename  asCattrRefOnly8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Ref Only Attribute class
 *
 */

#ifndef _ASCATTRREFONLY8_
#define _ASCATTRREFONLY8_

class asCattrRefOnly8 : public asCattrRefOnly
{
public:
	asCattrRefOnly8(int tag) : asCattrRefOnly(tag) {};

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{
		// not interested in content here, just scan over it all
		*pData += length;
		offset += length;
		length = 0;

		return 0;
	}
};


#endif // _ASCATTRREFONLY8_
