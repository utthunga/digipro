/*************************************************************************************************
 *
 * filename  icCBool8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 BOOL Attribute class
 *
 */

#ifndef _ICCBOOL8_
#define _ICCBOOL8_

#include "..\FMx_Attributes\icCBool.h"

class icCBool8 : public icCBool
{
public: // construct / destruct
	icCBool8() {}
	virtual ~icCBool8() {}

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

#endif //_ICCBOOL8_
