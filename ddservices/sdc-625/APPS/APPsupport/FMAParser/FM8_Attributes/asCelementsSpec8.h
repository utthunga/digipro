/*************************************************************************************************
 *
 * filename  asCelementsSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Element Specifier Attribute class
 *
 */

#ifndef _ASCelementsSPEC8_
#define _ASCelementsSPEC8_

#include "..\FM8_Attributes\icCelements8.h"

#include "..\FM8_conditional.h"

class asCelementsSpec8 : public  FMx_Attribute
{
public:
	asCelementsSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	
	fC_8_condList<icCelementList8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrMemberList* list = new aCattrMemberList;
		conditional.hydratePrototype(&list->condMemberElemListOlists);
		isConditional = conditional.isConditional();

		list->attr_mask = maskFromInt(attrType);

		return list;
	};
};

#endif //_ASCelementsSPEC8_


