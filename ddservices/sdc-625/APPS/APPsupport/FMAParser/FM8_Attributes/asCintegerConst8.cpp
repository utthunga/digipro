/*************************************************************************************************
 *
 * filename  asCintegerConst8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Integer Constant
 *
 */

#include "logging.h"
#include "asCintegerConst8.h"
#include "..\FMx_Support_primatives.h"

int asCintegerConst8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	RETURNCODE  ret = 0;

	parseExpectedTag(pData, length, offset);

	value = 0;
	unsigned x = 0;
	ret = parseUnsigned(pData, length, offset, x);
    value = (int) x;

    return ret;
}

