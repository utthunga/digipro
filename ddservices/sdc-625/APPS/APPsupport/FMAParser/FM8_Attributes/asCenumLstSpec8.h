/*************************************************************************************************
 *
 * filename  asCenumLstSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 ENum List Specifier Attribute class
 *
 */

#ifndef _ASCenumLstSPEC8_
#define _ASCenumLstSPEC8_

#include "..\FM8_Attributes\icCenumLst8.h"

#include "..\FM8_conditional.h"
class asCenumLstSpec8 : public  FMx_Attribute
{
public:
	asCenumLstSpec8(int tag) : FMx_Attribute(tag) {};
    
public:
	fC_8_condList<icCenumLst8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrEnum* attr = new aCattrEnum;
		conditional.hydratePrototype(&attr->condEnumListOlists);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ASCenumLstSPEC8_


