/*************************************************************************************************
 *
 * filename  icCstyle8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Style Attribute class
 *
 */

#ifndef _icCstyle8_
#define _icCstyle8_

#include "..\FMx_Attributes\icCstyle.h"

#include "..\FM8_conditional.h"
class icCstyle8 : public icCstyle
{
public: // construct / destruct
	icCstyle8() {};
	virtual ~icCstyle8() {};
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
	
	
	virtual aCattrBase* generatePrototype() 
	{ 
		return createLongAttribute(attrType);
	};
};
#endif //_icCstyle8_



