/*************************************************************************************************
 *
 * filename  icCchartType8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Chart Type Attribute
 *
 */

#include "icCchartType8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"

/*
Chart_Type_Specifier ::=	[APPLICATION 51]
SEQUENCE {
	implicit-tag		Implicit_Tag,
	ENUMERATED {
		strip				(0),
		sweep				(1),
		scope				(2),
		horizontal-bar		(3),
		vertical-bar		(4),
		gauge				(5)
	}
}
*/

int icCchartType8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	UINT64 tempLongLong ;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;
 
	// implicit tag // ReturnLen is zero (with no error) on an implicit tag
	ret = parseTag(pData, length,   tagValue,   tagLength, offset);

	if (tagLength > 0)
	{
		LOGIT(CERR_LOG,"Error: asCchartTypeSpec8 attribute with tag's length bigger than available.\n)");
		return -1;
	}

	if (tagValue != CHART_TYPE_TAG)
	{
		LOGIT(CERR_LOG,"Error: asCchartTypeSpec8 attribute explicit tag is not 51.\n)");
		return -2;
	}

	value = 0;
	if (length > 0)
	{
		ret = parseInteger(4, pData, length, tempLongLong, offset);
		if (ret != SUCCESS)
		{
			LOGIT(CERR_LOG,"Error: Chart Type attribute error in size extraction.\n)");
		}
		else
		{
			value = (int)tempLongLong;
		}
	}
	else// error
	{
		LOGIT(CERR_LOG,"Error: Chart Type attribute needs size but has no length.\n)");
		ret = -3;
	}

	return ret;
}



