/*************************************************************************************************
 *
 * filename  asCtimeScaleSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Time Scale Specifier Attribute class
 *
 */

#ifndef _ASCtimeScaleSPEC8_
#define _ASCtimeScaleSPEC8_

#include "icCtimeScale8.h"

#include "..\FM8_conditional.h"
class asCtimeScaleSpec8 : public FMx_Attribute
{
public:
	asCtimeScaleSpec8(int tag) : FMx_Attribute(tag) {conditional.forceDirect = true;};
    
public:
	fC_8_Conditional<icCtimeScale8> conditional;	
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy();
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out();
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCattrBitstring* attr = new aCattrBitstring;
		conditional.hydratePrototype(&attr->condBitStr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};
#endif //_ASCtimeScaleSPEC8_


