/*************************************************************************************************
 *
 * filename  asCrefSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Reference Specifier Attribute class
 *
 */

#ifndef _ASCrefSPEC8_
#define _ASCrefSPEC8_

#include "..\FM8_Attributes_Ref&Expr.h"

#include "..\FM8_conditional.h"
#include "..\FMx_Attributes\asCrefSpec.h"

class asCrefSpec8 : public asCrefSpec
{
public:
	asCrefSpec8(int tag, bool direct = false, bool forceDir = false) : asCrefSpec(tag) 
	{
		attname = "Reference Specifier";

		// if direct == true, then the attr is not conditional, so
		// the conditional.out() skips right to the payload.out()
		conditional.requiredDirect = direct;
		// forceDirect is true when the attribute has to be conditional but the encoding is not
		// it hard code sets the  type as DIRECT and then evaluates the payload.
		conditional.forceDirect    = forceDir;
	};
    
public:
	fC_8_Conditional<abCReference8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{		
		RETURNCODE  ret = 0;

		ret = parseExpectedTag(pData, length, offset);//conditional on expected tag exists

		if (ret == SUCCESS)
			ret = conditional.evaluateAttribute(pData, length, offset);  

		return ret;
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
        conditional.out(); 
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
	{
		aCattrCondReference* attr = new aCattrCondReference;
		conditional.hydratePrototype(&attr->condRef);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};

};

#endif //_ASCrefSPEC8_


