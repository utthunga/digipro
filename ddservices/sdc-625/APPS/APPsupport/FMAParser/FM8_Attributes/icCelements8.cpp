/*************************************************************************************************
 *
 * filename  icCelements8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Elements Attribute
 *
 */

#include "icCelements8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"
#include "..\FM8_Attributes_Ref&Expr.h"
#include "faCString8.h"

/*
Element ::= [APPLICATION 83]
SEQUENCE  {
	explicit-tag	Explicit_Tag,
	SEQUENCE  {
		index				INTEGER,
		item				Reference,
		description			String  (OPTIONAL),
		help				String  (OPTIONAL)
		}
}	
*/

int icCelement8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	unsigned tagLength;
	RETURNCODE  ret = 0;

	if (ret = parseExpectedExplicitTag(pData, length, offset, ITEM_ARRAY_ELEMENT_TAG, tagLength))
		return ret;

	int			exitLength		= length   - tagLength;
	unsigned	exitOffset		= offset   + tagLength;
	UCHAR	   *exitPdata		= (*pData) + tagLength;
	int			workingLength	= tagLength;

	if (ret = parseUnsigned(pData, workingLength, offset, index))
		goto end;

	item = new abCReference8();
	if (item == 0)
	{
		LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
		ret = -3;
		goto end;
	}
	if (ret = item->evaluateAttribute(pData, workingLength, offset))
		goto end;


	if (workingLength > 0)
	{
		description = new faCString8();
		if (description == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = description->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

	if (workingLength > 0)
	{
		help = new faCString8();
		if (help == 0)
		{
			LOGIT(CERR_LOG,"Memory Error: Attribute %s.\n", attname);
			ret = -3;
			goto end;
		}
		if (ret = help->evaluateAttribute(pData, workingLength, offset))
			goto end;
	}

end:
	*pData = exitPdata;
	length = exitLength;
	offset = exitOffset;

	return ret;
}


icCelement8& icCelement8::operator=( const icCelement8& src )
{
	icCelement::operator=(src);
	return *this;
}


//=============================================================================================


int icCelementList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCelement8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCelement8 *pElem;

	while(length > 4)
	{
		pElem = new icCelement8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCelementList8::destroy()
{
	icCelement8 *pElem = NULL;
	vector<icCelement8 *>::iterator iT;//ptr2ptr2icCekement8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCelementList8::out(void)
{
	icCelement8 *pElem = NULL;
	vector<icCelement8 *>::iterator iT;//ptr2ptr2icCelement8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


//class aCmemberElementList : public aPayldType, public AmemberElemList_t 
aPayldType* icCelementList8::generatePayload(aPayldType*payld)
{	
	aCmemberElementList  *pPlst;
	if (payld)
	{
		pPlst = (aCmemberElementList  *) payld;
	}
	else
	{
		pPlst = new aCmemberElementList;
	}

	aCmemberElement  locElem;
	icCelement8 *pElem = NULL;
	vector<icCelement8 *>::iterator iT;//ptr2ptr2icCelement8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
			locElem.clear();
		}
	}//next

	return pPlst;
}


icCelementList8& icCelementList8::operator=(const icCelementList8& s)
{
	icCelement8 *pElem = NULL, *pnewElem = NULL;
	vector<icCelement8 *>::const_iterator iT;//ptr2ptr2icCelement8
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCelement8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}