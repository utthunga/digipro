/*************************************************************************************************
 *
 * filename  asCdataFieldSpec8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Data Field Specifier Attribute class
 *
 */

#ifndef _ASCDATAFIELDSPEC8_
#define _ASCDATAFIELDSPEC8_


#include "..\FM8_conditional.h"
#include "..\FMx_Attributes\asCdataFieldSpec.h"
#include "icCdataElem8.h"

class asCdataFieldSpec8 : public asCdataFieldSpec
{
public:
	asCdataFieldSpec8(int tag = 0) : asCdataFieldSpec(tag) {};

public:
	
	fC_8_condList<icCdataElemList8> conditional;

public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
		return conditional.evaluateAttribute(pData, length, offset);  
	};

	virtual void destroy(void)
	{	
		conditional.destroy();  
	};

	virtual int out(void)
	{	
		FMx_Attribute::out();  
		conditional.out();  
		return 0;  
	};

	virtual void hydrateConditional(aCcondList* cond) 
	{
		conditional.hydratePrototype(cond);
		isConditional = conditional.isConditional();
	}
};

#endif //_ASCDATAFIELDSPEC8_