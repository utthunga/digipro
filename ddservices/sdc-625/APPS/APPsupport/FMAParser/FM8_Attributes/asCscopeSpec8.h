/*************************************************************************************************
 *
 * filename  asCscopeSpec8.h
 *
 * 27 Aug 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Scope Specifier Attribute class
 *
 */

#ifndef _ASCscopeSPEC8_
#define _ASCscopeSPEC8_

#include "icCscope8.h"

#include "..\FM8_conditional.h"
class asCscopeSpec8 : public FMx_Attribute
{
public:
	asCscopeSpec8(int tag) : FMx_Attribute(tag) 
	{ attname = "MethScope";   conditional.forceDirect = true;};
    
public:
	fC_8_Conditional<icCscope8> conditional;
    
public:
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{	
        return conditional.evaluateAttribute(pData, length, offset);  
    };
    
	virtual void destroy(void) 
    { 
        return conditional.destroy(); 
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		conditional.out(); 
        return 0; 
    };
    
	virtual bool inhibitOut() { return (reconcile) ? true : false; }

	virtual aCattrBase* generatePrototype()
    {
		aCattrBitstring* attr = new aCattrBitstring;
		conditional.hydratePrototype(&attr->condBitStr);
		isConditional = conditional.isConditional();

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };
};

#endif //_ASCscopeSPEC8_


