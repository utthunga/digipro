/*************************************************************************************************
 *
 * filename  icCdataElem8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Data Field Attribute
 *
 */

#include "icCdataElem8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "faCBitString8.h"

int icCdataElem8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
	exitValues_t holdExit;
 	UINT64 tempLongLong;
  	unsigned tagValue;
	unsigned tagLength;
	RETURNCODE  ret = 0;

	pushExitValues(holdExit, pData, length, offset);

	ret = parseTag(pData, length, tagValue, tagLength, offset);//implicit data element type

	if ( tagValue > 5 || tagLength != 0 || ret != 0 )
	{
		LOGIT(CERR_LOG,"Error: Data Field attribute error in size extraction.\n)");
		popExitValues(holdExit, pData, length, offset);
		ret = -1;
	}
	else
	{
		choice = tagValue;
		switch (tagValue)//tempLongLong)
		{
		case cmdDataConst:
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			intRef = (unsigned int)tempLongLong;
			break;

		case cmdDataReference:
			varRef = new abCReference8;
			varRef->evaluateAttribute(pData, length, offset);
			break;

		case cmdDataFlags:
			varRef = new abCReference8;
			varRef->evaluateAttribute(pData, length, offset);
			flags = new faCBitString8;
			flags->evaluateAttribute(pData, length, offset);
			break;

		case cmdDataWidth:
			varRef = new abCReference8;
			varRef->evaluateAttribute(pData, length, offset);
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			width = (int)tempLongLong;
			flags = new faCBitString8;
			// stevev 31march2016  moved to hydration generateElement()...flags->value = cmdDataItemFlg_WidthPresent;
			break;

		case cmdDataFlagWidth:
			varRef = new abCReference8;
			varRef->evaluateAttribute(pData, length, offset);
			ret = parseInteger(4, pData, length, tempLongLong, offset);
			width = (int)tempLongLong;
			flags = new faCBitString8;
			flags->evaluateAttribute(pData, length, offset);
			// stevev 31march2016  moved to hydration generateElement()...flags->value |= cmdDataItemFlg_WidthPresent;
			break;

		case cmdDataFloat:
			double dval = 0.0;
			parseFLOAT(pData, length, dval, offset);
			fvalue = (float)dval;	// FLOATCHANGE no change required [1/20/2016 tjohnston]
			break;
		}
	}

	return ret;
}



//=============================================================================================


int icCdataElemList8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{// isa vector of vector<icCdataElem8*>
#ifdef _DEBUG
	unsigned entryLen = length;
#endif
	int ret = 0;
	icCdataElem8 *pElem;

	while(length > 0)
	{
		pElem = new icCdataElem8;
		if (pElem == NULL)
		{
			return FMx_MEMFAIL;
		}
		ret = pElem->evaluateAttribute(pData, length, offset);
		if ( ret )
		{
			delete pElem;
			return ret;
		}
		push_back(pElem);
		pElem = NULL;
	}// wend

	return ret;
}

void icCdataElemList8::destroy()
{
	icCdataElem8 *pElem = NULL;
	vector<icCdataElem8 *>::iterator iT;//ptr2ptr2icCdataElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			delete pElem;
			(*iT) = NULL;
		}
	}//next
	clear();//self vector
}

void icCdataElemList8::out(void)
{
	icCdataElem8 *pElem = NULL;
	vector<icCdataElem8 *>::iterator iT;//ptr2ptr2icCdataElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pElem->out();
		}
	}//next
}


aPayldType* icCdataElemList8::generatePayload(aPayldType*payld)
{	
	aCdataitemList  *pPlst;
	if (payld)
	{
		pPlst = (aCdataitemList  *) payld;
	}
	else
	{
		pPlst = new aCdataitemList;
	}

	icCdataElem8 *pElem = NULL;
	vector<icCdataElem8 *>::iterator iT;//ptr2ptr2icCdataElem8
	for( iT = begin(); iT != end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			aCdataItem  locElem;
			pElem->generateElement(&locElem);
			pPlst->push_back(locElem);
		}
	}//next

	return pPlst;
}


icCdataElemList8& icCdataElemList8::operator=(const icCdataElemList8& s)
{
	icCdataElem8 *pElem = NULL, *pnewElem = NULL;
	vector<icCdataElem8 *>::const_iterator iT;//ptr2ptr2icCdataElem8
	for( iT = s.begin(); iT != s.end(); ++iT)
	{
		pElem = (*iT);
		if ( pElem != NULL )
		{
			pnewElem = new icCdataElem8(*pElem);
			push_back(pnewElem);pnewElem = NULL;
		}
	}// next
	return *this;
}