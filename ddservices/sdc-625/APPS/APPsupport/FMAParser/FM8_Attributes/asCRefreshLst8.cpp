/*************************************************************************************************
 *
 * filename  asCRefreshLst8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Refresh Attribute
 *
 */

#include "logging.h"
#include "asCRefreshLst8.h"
#include "..\FMx_Support_primatives.h"

int asCRefreshLst8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
{
	exitValues_t holdExit; 
	pushExitValues(holdExit, pData, length, offset);

	int ret = watch->evaluateAttribute(pData, length, offset);

	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Refresh watch list evaluate failed");

		popExitValues(holdExit, pData, length, offset);
		return -1;
	}

	ret = update->evaluateAttribute(pData, length, offset);

	if (ret != SUCCESS)
	{
		LOGIT(CERR_LOG,"Refresh update list evaluate failed");

		popExitValues(holdExit, pData, length, offset);
		return -2;
	}

	if (length != 0)
	{
		LOGIT(CERR_LOG,"Refresh length error");

		return -3;
	}

	return 0;
}


int asCRefreshLst8::reconcile_out(void)
{
	FMx_Attribute::out();
	if (update)
		update->out();

	// here, we are making the RefreshRelationLst "look like" two attrs
	// to emulate FMA's two actual attributes.  This is for reconcile only.

	LOGIT(COUT_LOG,
	"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
	LOGIT(COUT_LOG," Attribute: %s\n\n", "Update List");	// TODO: get rid of literal string
	if (watch)
		watch->out();

	return 0;
}