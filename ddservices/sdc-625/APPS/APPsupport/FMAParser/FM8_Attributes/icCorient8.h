/*************************************************************************************************
 *
 * filename  icCorient8.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Orientation Attribute class
 *
 */

#ifndef _icCorient8_
#define _icCorient8_

#include "..\FMx_Attributes\icCorient.h"

#include "..\FM8_conditional.h"
class icCorient8 : public icCorient
{
public: // construct / destruct
	icCorient8() {}
	virtual ~icCorient8() {}
    
public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};
#endif //_icCorient8_



