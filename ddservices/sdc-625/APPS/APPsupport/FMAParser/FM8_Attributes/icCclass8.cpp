/*************************************************************************************************
 *
 * filename  icCclass8.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for version 8 Class Attribute
 *
 */

#include "icCclass8.h"
#include "logging.h"
#include "..\FMx_Support_primatives.h"
#include "tags_sa.h"
#include "faCBitString8.h"

int icCclass8::evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) 
{
 	RETURNCODE  ret = 0;

	PARSE_BITSTRING(value, ret);

	if (SUPPRESS_CLASSERROR || QANDA_8vA)
	{
		// in reconcile mode only, map fm8 bits 0, 1 and 7 of high-order byte 
		// to their fma equivalents
		// must do this here instead of out() because icCclass has no knowledge that
		// this is an v8 EFF

		value &= 0xffffffff7fffffff;	// high-order error bit off

		UINT64 oldclass = 0x0000000001000000; 
		if (value & oldclass)
		{
			value &= ~oldclass;
			value |= DIAGNOSTIC_CLASS;
		}

		oldclass = 0x0000000002000000;
		if (value & oldclass)
		{
			value &= ~oldclass;
			value |= SERVICE_CLASS;
		}
	}

	return ret;
}

