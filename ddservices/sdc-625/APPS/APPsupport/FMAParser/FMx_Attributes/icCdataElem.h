/*************************************************************************************************
 *
 * filename  icCdataElem.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Data Field Attribute class
 *
 */

#ifndef _ICCDATAELEM_
#define _ICCDATAELEM_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

#include "faCBitString.h"

class icCdataElem: public FMx_Attribute
{
public: // construct 
	icCdataElem() : choice(0), width(0), intRef(0), fvalue(0.0), varRef(NULL), flags(NULL)
	 { };   // initialize underlying class data
	void destroy()
	{ 
		if (varRef) 
			varRef->destroy(); 
		if (flags) 
			flags->destroy(); 
	};
	icCdataElem(const icCdataElem& elem) { operator=( elem );};
	icCdataElem& operator=(const icCdataElem& src) {
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		choice = src.choice;
		intRef = src.intRef;
		if (src.varRef)
			varRef = new abCReference(*(src.varRef));
		width = src.width;
		if (src.flags)
			flags = new faCBitString(*(src.flags));
		fvalue = src.fvalue;
		return *this;
	};
	~icCdataElem() { destroy(); RAZE(varRef); RAZE(flags); };
    
public: // data
	unsigned      choice;// 0=intRef, 1=varRef, 2=varRef w/ flags, 3 = varRef w/ width, 4 = varRef with both, 5 = float
	unsigned int  intRef;
	abCReference* varRef;
	unsigned int  width;
	faCBitString* flags;
    double        fvalue;

public: // methods
	virtual int out(void) 
    {
		//FMx_Attribute::out();		//  [3/2/2015 timj]
		if (reconcile == false)
		{
			if (attname)
			LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
			else
			LOGIT(COUT_LOG,"%23s:\n","Attribute Name Missing");   // Attribute name
		}


		switch (choice)
		{
		case 0:
			LOGIT(COUT_LOG, "%23s: %u (0x%08x)\n", "cmdDataConst", intRef, intRef);
			break;

		case 1:
			LOGIT(COUT_LOG, "%23s: \n", "cmdDataReference");
			varRef->out();
			break;

		case 2:
			LOGIT(COUT_LOG, "%23s: \n", "cmdDataFlags");
			varRef->out();
			flags->out();
			break;

		case 3:
			LOGIT(COUT_LOG, "%23s: \n", "cmdDataWidth");
			varRef->out();
			LOGIT(COUT_LOG, "%23s: %d (0x%08x)\n", "width", width, width);
			break;

		case 4:
			LOGIT(COUT_LOG, "%23s: \n", "cmdDataFlagWidth");
			varRef->out();
			flags->out();
			break;

		case 5:
			LOGIT(COUT_LOG, "%23s: %g\n", "cmdDataFloat", (double) fvalue);
			break;
		}

        return 0;
    };

	// stevev 17march2016 - make this fit properly...How did this get through V&V?????
	// stevev 31march2016 - recode to match legacy parserinfc here, no longer in the evaluate function
	virtual void* generateElement(aCdataItem* dataItem)
	{
		if (dataItem == NULL)
			dataItem = new aCdataItem;

		dataItem->type   = choice;// sdc type exactly matches FMA choice
		switch (choice)
		{
		case 0:	//cmdDataConst
			dataItem->iconst = intRef;
			break;
		case 5:	//cmdDataFloat
			dataItem->fconst = (float)fvalue;// must be 'float' not double
			break;

		case 4:	//cmdDataFlagWidth
			// fall thru to pick up Ref, Flg & Wid
		case 2:	//cmdDataFlags
			// set flags
			if (flags)
			{
				dataItem->flags  = (ulong)flags->value;
			}
			else
			{
				dataItem->flags  = 0;
				LOGIT(CERR_LOG,"ERROR: Data Item choice requires FLAGS but its pointer is NULL.\n");
			}
			// fall thru to pick up Ref

		case 3:	//cmdDataWidth
			if (choice != 2)// only set if 4 or 3, 2 is excluded
			{
				dataItem->width  = width;
				dataItem->flags |= cmdDataItemFlg_WidthPresent;
			}
			// fall thru to pick up ref


		case 1:		// cmdDataReference...ref only
			{				
				if (varRef)
				{
					varRef->hydratePrototype(&dataItem->ref);
				}
				else
				{
					LOGIT(CERR_LOG,"ERROR: Data Item choice requires Reference but its pointer is NULL.\n");
				}
				dataItem->type = 1; // in the acclass, 0,1 and 5 are the only usable choices
			}
			break;
		// there is no default, 0 - 5 are only legal values
		}
		return dataItem;
	}
};

#endif //_ICCDATAELEM_
