/*************************************************************************************************
 *
 * filename  asCintegerConst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Integer Constant Attribute class
 *
 */

#ifndef _ASCINTEGERCONST_
#define _ASCINTEGERCONST_

#include "..\FMx_Attribute.h"

class asCintegerConst : public FMx_Attribute
{
public:	// construct
	asCintegerConst(int tag) : FMx_Attribute(tag) {attname = "Integer Constant";};

public:	// data
	int value;

public:	// methods
	virtual int out(void)
	{
		if (!FMx_Attribute::out())
		{			
			return out2();
		}

		return 0;
	};
	
	int out2(void)
	{
		LOGIT(COUT_LOG,"%23s: 0x%02x (%d)\n\n",attname, value, value);
		return 0;
	};

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}

	virtual aCattrBase* generatePrototype()
	{
		return createLongAttribute(attrType);
	}
};

#endif //_ASCINTEGERCONST_
