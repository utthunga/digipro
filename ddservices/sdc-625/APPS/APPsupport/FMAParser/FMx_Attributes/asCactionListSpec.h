/*************************************************************************************************
 *
 * filename  asCactionListSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for base Action List Specifier Attribute class
 *
 * #include "asCactionListSpec.h"
 *
 */

#ifndef _ASCactionListSPEC_
#define _ASCactionListSPEC_

#include "..\FMx_Attribute.h"

class asCactionListSpec : public  FMx_Attribute
{
public:
	asCactionListSpec(int tag) : FMx_Attribute(tag) {};
    
public:
	virtual void hydrateConditional(aCcondList* cond) {}
};

#endif //_ASCactionListSPEC_


