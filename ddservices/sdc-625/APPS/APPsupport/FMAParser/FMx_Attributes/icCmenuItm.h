/*************************************************************************************************
 *
 * filename  icCmenuItm.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Menu Item Attribute class
 *
 */

#ifndef _icCmenuItm_
#define _icCmenuItm_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\faCBitString.h"

class icCmenuItm : public FMx_Attribute
{
public: // construct 
	icCmenuItm() : item(0), qualifiers(0) {attname = "Menu Item"; };   // initialize underlying class data
	icCmenuItm(const icCmenuItm& mi) { operator=( mi );};
	icCmenuItm& operator=(const icCmenuItm& src){
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		if (src.item != NULL) item = new abCReference(*(src.item));
		if (src.qualifiers != NULL) qualifiers = new faCBitString(*(src.qualifiers));
		return *this;  } ;
	virtual ~icCmenuItm() { destroy(); };
    
public: // data
	abCReference *item;
	faCBitString *qualifiers;

    // this is in the base class  char *attname;
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
		if (item)
		{
			//item->destroy();
			delete item;// delete will destroy
			item = NULL;
		}

		if (qualifiers)
		{
			qualifiers->destroy();
			delete qualifiers;
			qualifiers = NULL;
		}
    };
    
	virtual int out(void) 
    {
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
		if (item)
		{
			item->out();
		}

		if (qualifiers)
		{
			qualifiers->out();
		}

		return 0;
    };

	virtual void* generateElement(aCmenuItem* menu = NULL)
	{
		if ( menu == NULL )
		{
			aCmenuItem* menu = new aCmenuItem;
		}// else use the passed in item

		if (qualifiers)
		{
			aCbitString bitstring;
			bitstring.bitstringVal = qualifiers->value;

			menu->qualifier = bitstring;
		}
		if (item)
		{
			item->hydratePrototype(&menu->itemRef);
		}
		else
		{			
			LOGIT(COUT_LOG,"ERROR: Menu item with null contents.\n");   // Attribute name
		}

		return menu;
	}
};

#endif //_icCmenuItm_




