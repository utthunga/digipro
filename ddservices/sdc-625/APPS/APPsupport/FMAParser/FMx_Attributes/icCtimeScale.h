/*************************************************************************************************
 *
 * filename  icCtimeScale.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Time Scale Attribute class
 *
 */

#ifndef _icCtimeScale_
#define _icCtimeScale_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCtimeScale: public FMx_Attribute
{
public: // construct 
	icCtimeScale() : value(0L) {};   // initialize underlying class data
    
public: // data
	unsigned long value;        // whatever data is included in the underlying class
    
public: // methods
	virtual void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *scales[] = { "invalid", "Seconds", "Minutes", "invalid", "Hours" };

		FMx_Attribute::out();
		LOGIT(COUT_LOG,"					   Time Scale:\n");

		char *s = (value < 5) ? scales[value] : "invalid";

        LOGIT(COUT_LOG,"					Unsigned Long: 0x%08x (%d)  %-20s\n",value,value, s);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_icCtimeScale_



