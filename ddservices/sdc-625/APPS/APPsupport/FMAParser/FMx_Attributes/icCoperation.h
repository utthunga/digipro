/*************************************************************************************************
 *
 * filename  icCoperation.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Operations Attribute class
 *
 */

#ifndef _icCoperation_
#define _icCoperation_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCoperation: public FMx_Attribute
{
public: // construct 
	icCoperation(int tag = 0) : value(0L) {attname = "Operation";};   // initialize underlying class data
    
public: // data
	unsigned rawValue;
	unsigned value;        // whatever data is included in the underlying class
    
public: // methods
	virtual void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *ops[] = { "invalid", "Read", "Write", "Command" };

		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name

		char *s = value < 4 ? ops[value] : ops[0];

        LOGIT(COUT_LOG,"%23s: %-20s  0x%08x (%d)\n", "Value", s, value, value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = NULL;
		if ( pV == NULL )
		{
			pl = new aCddlLong;
		}
		else
		{
			pl = (aCddlLong*)pV;
		}
		pl->ddlLong = value;
		return pl;
	};
	virtual aCattrBase* generatePrototype()
	{
		return createLongAttribute(attrType);
	};
	
};

#endif //_icCoperation_



