/*************************************************************************************************
 *
 * filename  icCenumLst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base ENum List Attribute class
 * Definition for Base Status Classes Attribute class
 *
 */

#ifndef _icCenumLst_
#define _icCenumLst_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "icCclass.h"
#include "..\v_N_v+Debug.h"


#define MAX_BASE_CLASS	24

typedef struct
{
	unsigned kindAndClass;// raw byte
						// as defined by FM8:
						// The least-significant three bits (bit 0 - bit 2) define the Kind of output (DV,TV, AO, or ALL).  
						// Bit 3 defines the Mode (AUTO or MANUAL).  Bit 4 defines the Reliability (GOOD or BAD). 
	
	unsigned kind;
	unsigned output_class; // combined mode & reliability
	unsigned which;		   // the following integer (optional)
}	outputClass_t;


class icCstatusClasses : public FMx_Attribute
{
public: // construct 
	icCstatusClasses() 
	{ 
		baseClass = 0;
		attname = "Status Classes"; 

		char **p = baseClassLabels;

		*p++ = "hardware-status";			// (0),
		*p++ = "software-status";			// (1),
		*p++ = "process-status";				// (2),
		*p++ = "mode-status";				// (3),
		*p++ = "data-status";				// (4),
		*p++ = "misc-status";				// (5),
		*p++ = "event-status";				// (6),
		*p++ = "state-status";				// (7),
		*p++ = "self-correcting-status";		// (8),
		*p++ = "correctable-status";			// (9),
		*p++ = "uncorrectable-status";		// (10),
		*p++ = "summary-status";				// (11),
		*p++ = "detail-status";				// (12),
		*p++ = "more-status";				// (13),
		*p++ = "comm-error-status";			// (14),
		*p++ = "ignore-in-handheld";			// (15), 
		*p++ = "bad-output";					// (16),
		*p++ = "ignore-in-host";				// (17),
		*p++ = "error-status"; 				// (18),
		*p++ = "warning-status"; 			// (19),
		*p++ = "info-status";				// (20), 
		*p++ = "DV-status";					// (21),		-- implies a following output-class
		*p++ = "TV-status"; 					// (22), 		-- implies a following output-class
		*p++ = "AO-status";					// (23), 		-- implies a following output-class
		*p++ = "ALL-status";					// (24) 		-- implies a following output-class
	};   // initialize underlying class data
    
public: // data
	// note that the data structure here is the FM8 structure: baseClass is a bitmap with vector of output_class following
	unsigned baseClass;
	vector<outputClass_t> outputClasses;
    char *baseClassLabels[MAX_BASE_CLASS+1];

public: // methods

	icCstatusClasses(const icCstatusClasses& x) { operator=( x );};
	icCstatusClasses& operator=(const icCstatusClasses& src)
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		// baseClassLabels is initialized by the constructor and is constant;
		baseClass = src.baseClass;
		for (unsigned i=0; i < src.outputClasses.size(); i++)
		{
			outputClasses = src.outputClasses;
		}
		return *this;  
	} ;
	virtual ~icCstatusClasses() { destroy(); };


	virtual void destroy(void) 
    {
        // code to destroy complex objects
    };
    
	virtual int out(void) 
    {
		char *classStrings[] = {
			"AUTO-GOOD",
			"MANUAL-GOOD",
			"AUTO-BAD",
			"MANUAL-BAD",
			"AUTO-MARGINAL",
			"MANUAL-MARGINAL",
			NULL
		};

		LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "base class", baseClass, baseClass);
		//LOGIT(COUT_LOG,"%23s: %d %s\n", "cutput classes", outputClasses.size(), " to follow");
		for (unsigned i=0; i < outputClasses.size(); i++)
		{
			LOGIT(COUT_LOG,"%23s:\n","output class");   // Attribute name
			LOGIT(COUT_LOG,"%33s: 0x%08x (%d)\n", "kind", outputClasses[i].kind, outputClasses[i].kind);
			LOGIT(COUT_LOG,"%33s: %-20s 0x%08x (%d)\n", "class", classStrings[outputClasses[i].output_class], outputClasses[i].output_class, outputClasses[i].output_class);
			LOGIT(COUT_LOG,"%33s: 0x%08x (%d)\n", "which", outputClasses[i].which, outputClasses[i].which);
		}

        return 0;
    };
};


class icCenumElem: public FMx_Attribute
{
	static const int maxfields = 6;

public: 
	icCenumElem() 
	{
		attname = "Enumeration Element";
		clear();
	};

	void clear(void)
	{
		for (unsigned i=0; i < maxfields; i++)
		{
			fields[i] = 0;
		}
	};
	icCenumElem(const icCenumElem& en) { operator=( en );};
	virtual ~icCenumElem() { destroy(); };	
    
    
public: // data
	FMx_Attribute *fields[maxfields];	// fields are ordered per FM8 ==> field[4] holds a help string
										// the index is the FM8 implicit tag value
										// null pointer indicates empty field
public: // methods
	bool isEmpty(void)
	{
		for (unsigned i=0; i < maxfields; i++)
		{
			if (fields[i] != 0)
			{
				return false;
			}
		}
		return true;
	};

	virtual void destroy(void) 
    {
        // code to destroy complex objects
		for (unsigned i=0; i < maxfields; i++)
		{
			if (fields[i])
			{
				fields[i]->destroy();
				delete fields[i];
				fields[i] = NULL;
			}
		}
    };
    
	virtual int out(void) 
    {
		char *fnames[maxfields] = {"value","status classes","actions","description","help","class"};
        LOGIT(COUT_LOG,"\n%23s:\n",attname);   // Attribute name
		for (unsigned i=0; i < maxfields; i++)
		{
			if (fields[i])
			{
				LOGIT(COUT_LOG,"%23s:\n",fnames[i]);   // field name
				fields[i]->out();
			}
		}
        return 0;
    };
/*	unsigned long   val;
	unsigned long   status_class;
			outStatusList_t oclasslist; // oclasses...Empty when kind_oclasses is OC_NORMAL
	aCreference     actions;
	aCddlString     descS;
	aCddlString     helpS;	
	aCbitString     func_class;	// functional class - a ulong bit pattern
* * * * * 
	typedef struct oStatus_s	// see varTypeEnum.h for explaination
	{
		unsigned short  kind;
		unsigned short  which;
		unsigned short  oclass;
	}   outputStatus_t;
	
	typedef vector<outputStatus_t>     outStatusList_t;
*/
	virtual void* generateElement(aCenumDesc* elem)
	{
		if (elem == NULL)
		{
			elem = new aCenumDesc;
		}// else use the passed in item

		if (fields[0])
		{
			elem->val = ((faCLong*)fields[0])->value;
		}
		
		if (fields[1])
		{
			outputClass_t* pOC = NULL;
			icCstatusClasses* pSC = ((icCstatusClasses*)fields[1]);
			elem->status_class = pSC->baseClass;
			vector<outputClass_t>::iterator IT;
			for ( IT = pSC->outputClasses.begin(); IT != pSC->outputClasses.end(); ++IT)
			{
				pOC = &(*IT);
				outputStatus_t os;
				os.kind  = pOC->kind;
				os.which = pOC->which;
				os.oclass= pOC->output_class;
				elem->oclasslist.push_back(os);
				os.clear();
			}
		}

		if (fields[2])
		{
			((abCReference*)fields[2])->hydratePrototype(&elem->actions);
		}

		if (fields[3])
		{
			((faCString*)fields[3])->hydratePrototype(&elem->descS);
			if (QANDA_8v8 && elem->descS.pRef==NULL) {elem->descS.enumVal = -1;}
		}

		if (fields[4])
		{
			((faCString*)fields[4])->hydratePrototype(&elem->helpS);
			if (QANDA_8v8 && elem->helpS.pRef==NULL) {elem->helpS.enumVal = -1;}
		}

		if (fields[5])
		{
			aCbitString bs;
			bs.bitstringVal = ((icCclass*)fields[5])->value;
			elem->func_class = bs;
		}

		return elem;
	};

	icCenumElem& operator=(const icCenumElem& src)
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		if (src.fields[0])
		{
			fields[0] = new faCLong(*((faCLong*) src.fields[0]));
		}

		if (src.fields[1])
		{
			fields[1] = new icCstatusClasses(*((icCstatusClasses*) src.fields[1]));
		}

		if (src.fields[2])
		{
			fields[2] = new abCReference(*((abCReference*) src.fields[2]));
		}

		if (src.fields[3])
		{
			fields[3] = new faCString(*((faCString*) src.fields[3]));
		}

		if (src.fields[4])
		{
			fields[4] = new faCString(*((faCString*) src.fields[4]));
		}

		if (src.fields[5])
		{
			fields[5] = new icCclass(*((icCclass*) src.fields[5]));
		}

		return *this;  
	};

};

#endif //_icCenumLst_




