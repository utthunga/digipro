/*************************************************************************************************
 *
 * filename  icCchartType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Chart Type Attribute class
 *
 */

#ifndef _icCchartType_
#define _icCchartType_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCchartType: public FMx_Attribute
{
public: // construct 
	icCchartType() : rawValue(0L), value(0L) {attname = "Chart Type";};   // initialize underlying class data
    
public: // data
	unsigned rawValue;     // encoded in the file
	unsigned value;        // fm8 value
    
public: // methods
	void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *typeStrings[] = {
			"Invalid",		
			"Gauge",	
			"Horiz Bar",	
			"Scope",	
			"Strip",	
			"Sweep",	
			"Vert Bar",
			NULL
		};

		FMx_Attribute::out();

		char *t = "chart type out of range error";
		if (0 <= value  && value < 7)
			t = typeStrings[value];

        LOGIT(COUT_LOG,"%33s: %s  0x%08x (%d)\n", "Chart Type", t, value, value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_icCchartType_



