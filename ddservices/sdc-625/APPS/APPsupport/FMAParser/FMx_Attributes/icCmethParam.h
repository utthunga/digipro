/*************************************************************************************************
 *
 * filename  icCmethParam.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Method Param Attribute class
 *
 */

#ifndef _icCmethParam_
#define _icCmethParam_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\faCBitString.h"
#include "..\FMx_Attributes\asCasciiStr.h"

class icCmethParam: public FMx_Attribute
{
public: // construct 
	icCmethParam() : typeModifiers(0), paramName(0) {attname = "Method Parameter"; }; 
    
public: // data
	unsigned methodType;
	faCBitString *typeModifiers;
	asCasciiStr *paramName;

    // this is in FMx_Attribute           char *attname;
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
		typeModifiers->destroy();
		delete typeModifiers;

		paramName->destroy();
		delete paramName;
    };
    
	virtual int out(void) 
    {
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Method Type", methodType, methodType);
		typeModifiers->out();
		paramName->out();
        return 0;
    };

	virtual void* generateElement()
	{
		aCparameter* param = new aCparameter;

		param->paramName = paramName->to_String();

		param->type = methodType;

		param->modifiers = (int) typeModifiers->value;

		return param;
	}
};

#endif //_icCmethParam_




