/*************************************************************************************************
 *
 * filename  asCreserved.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Reserved Attribute class
 *
 */

#ifndef _ASCRESERVEDTYPE_
#define _ASCRESERVEDTYPE_

#include "..\FMx_Attribute.h"

class asCreserved : public FMx_Attribute
{
public:	// construct
	asCreserved(int tag) : FMx_Attribute(tag) {attname = "Reserved";};

public:	// data

public:	// methods
	virtual int out(void)
	{
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"%23s:\n\n",attname);

		return 0;
	};
};

#endif //_ASCRESERVEDTYPE_
