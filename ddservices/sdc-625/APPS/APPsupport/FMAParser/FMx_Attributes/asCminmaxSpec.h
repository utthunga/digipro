#ifndef _ASCMinMax_
#define _ASCMinMax_

#include "icCminmax.h"

class asCminmaxSpec : public FMx_Attribute
{
public:
	asCminmaxSpec(int tag) : FMx_Attribute(tag) { attname = "MinMax"; };
	vector<icMinMax*> valueList;

public:
	virtual void destroy(void) 
	{
		for (vector<icMinMax*>::iterator it = valueList.begin(); it != valueList.end(); ++it)
		{
			icMinMax* val = *it;
			val->destroy();
		}
	};

	virtual int out(void) 
	{
		for (vector<icMinMax*>::iterator it = valueList.begin(); it != valueList.end(); ++it)
		{
			icMinMax* val = *it;
			val->out();
		}

		return 0;
	};

	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) { return 0; }

	virtual aCattrBase* generatePrototype()
	{
		//aCattrVarMinMaxList* attr = new aCattrVarMinMaxList;

		//for (vector<icMinMax*>::iterator it = valueList.begin(); it != valueList.end(); ++it)
		//{
		//	icMinMax* val = *it;
		//	
		//	aCminmaxVal ddlVal;

		//	ddlVal.whichVal = val->which;

		//	aCgenericConditional::aCexpressDest* exprDest = new aCgenericConditional::aCexpressDest;

		//	exprDest->destType = eT_Direct;

		//	// create and assign the payload object
		//	exprDest->pPayload = val->expression->generatePayload();

		//	// add new aCconditional to the destElements list
		//	ddlVal.condExpr.destElements.push_back(*exprDest);

		//	attr->ListOminMaxElements.push_back(ddlVal);
		//}

		//return attr;

		return NULL;
	};
};

#endif //_ASCMinMax_


