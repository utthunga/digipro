/*************************************************************************************************
 *
 * filename  asTransactionLst.cpp
 *
 * 06jun13 - stevev
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for Base Transaction list
 *
 */

#include "logging.h"
#include "asTransactionLst.h"
#include "..\v_N_v+Debug.h"



aCattrBase* asTransactionLst::generatePrototype()
{ 
	aCattrCmdTrans* attr = new aCattrCmdTrans;
	vector<icCtransaction*>::iterator it;
	
	for (it =  transactionList.begin(); it != transactionList.end(); ++it)
	{
		icCtransaction* at = *it;	

		aCtransaction tr;		
		
		tr.transNum = at->transactionNumber;

		if (at->pRequestPkt != NULL)
		{
			at->pRequestPkt->hydrateConditional(&tr.request);
		}
		
		if (at->pReplyPkt != NULL)
		{
			at->pReplyPkt->hydrateConditional(&tr.response);
		}
		
		if (at->pRespCodes != NULL)
		{
			at->pRespCodes->hydrateConditional(&tr.respCodes);
		}
		
		if (at->pActions != NULL)
		{
			// HART does not support these actions, 2041 spec does ...
			// sorry guys, HART and SDC supports these 
			at->pActions->hydrateConditional(&tr.postRqstRcvAction);
		}
		
		attr->transactionList.push_back(tr);
		tr.clear();
	}

	attr->attr_mask = maskFromInt(attrType);

	return attr;
}