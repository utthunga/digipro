/*************************************************************************************************
 *
 * filename  asCattrRefOnly.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Reference Only Attribute class
 *
 */

#ifndef _ASCATTRREFONLY_
#define _ASCATTRREFONLY_

class asCattrRefOnly : public FMx_Attribute
{
public:
	asCattrRefOnly(int y) : FMx_Attribute(y) { attname = "Attr-Ref-Only"; };
};

#endif // _ASCATTRREFONLY_