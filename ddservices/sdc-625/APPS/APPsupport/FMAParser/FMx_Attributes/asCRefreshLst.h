/*************************************************************************************************
 *
 * filename  asCRefreshLst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Reference List Attribute class
 *
 * #include "asCRefreshLst.h"
 *
 */

#ifndef _ASCREFRESHLST_
#define _ASCREFRESHLST_

#include "..\FMx_Attribute.h"
#include "asCrefListSpec8.h"
#include "dllapi.h"

class asCRefreshLst : public FMx_Attribute
{
public:
	asCRefreshLst(int tag) : FMx_Attribute(tag) {};

public: // data
	asCrefListSpec* watch;
	asCrefListSpec* update;

public:
	virtual int out(void)
	{
		FMx_Attribute::out();
		if (watch)
		{
			watch->out();
		}
		if (update)
		{
			LOGIT(COUT_LOG,
				"-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -\n");
			LOGIT(COUT_LOG," Attribute: 0x%02x (%u) %s\n\n",REFRESH_ITYPE,REFRESH_ITYPE,
				"Update List");
			update->out();
		}

		return 0;
	}

	virtual int reconcile_out(void)
	{
		return 0;
	}

	virtual aCattrBase* generatePrototype()
	{
		aCattrRefreshitems* attr = new aCattrRefreshitems;

		((asCrefListSpec8 *)watch)->conditional.hydratePrototype(&attr->condWatchList);

		((asCrefListSpec8 *)update)->conditional.hydratePrototype(&attr->condUpdateList);

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	}
};

#endif //_ASCREFRESHLST_