/*************************************************************************************************
 *
 * filename  asCvarType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Variable Type Attribute class
 *
 */

#ifndef _ASCVARTYPE_
#define _ASCVARTYPE_

#include "..\FMx_Attribute.h"
#include "..\FMA_defs.h" //TODO: get this header figured out!!!

class asCvarType : public FMx_Attribute
{
public: // construct
	asCvarType(int tag) : FMx_Attribute(tag), varType(vt_no_type), varSize(0), notHART(false) {};

public: // data
	var_t	varType;
	int		varSize;
	bool	notHART;

public: // methods
	virtual int out(void);
	virtual aCattrBase* generatePrototype();
};

#endif //_ASCVARTYPE_
