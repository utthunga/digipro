/*************************************************************************************************
 *
 * filename  icCorient.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Orientation Attribute class
 *
 */

#ifndef _icCorient_
#define _icCorient_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCorient: public FMx_Attribute
{
public: // construct 
	icCorient() : value(0L) {};   // initialize underlying class data
    
public: // data
	unsigned long rawValue;    
	unsigned long value;    
    
public: // methods
	virtual void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *typeStrings[] = {
			"Invalid",
			"Vertical",		// gd_Vertical		ORIENT_VERT		1
			"Horizontal"	// gd_Horizontal	ORIENT_HORIZ		2
		};

		char *t = "grid orientation out of range error";
		if (0 <= value  &&  value <= 2)
			t = typeStrings[value];

		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%33s: %s   0x%08x (%d)\n", "Orientation", t, value,value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_icCorient_



