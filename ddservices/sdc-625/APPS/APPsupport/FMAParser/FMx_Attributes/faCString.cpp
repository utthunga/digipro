/*************************************************************************************************
 *
 * filename  faCString.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for Base String Attribute
 *
 */

#include "logging.h"
#include "faCString.h"
#include "..\Convert.h"
#include "tags_sa.h"
#include "..\v_N_v+Debug.h"
#include "..\FMA_Attributes.h"

extern char ddlStringStrings[DDLSTRCOUNT][DDLSTRMAXLEN];
extern char fmaTypeStrings[(int)at_invalid_value][FMAATTRTYPMAXLEN];

	
// TODO extern FMx* daFile;... go through the convert function to include FMx.h

int faCString::out(void)
{
	ddlstringType_t type = strRefType;
	if (reconcile && strRefType == ds_Enum)
	{
		type = ds_EnumRef;
	}

	if (reconcile && type == ds_Var)
	{
		LOGIT(COUT_LOG,"            String attribute type: %s.\n",ddlStringStrings[ds_VarRef]);
	} 
	else
	{
		LOGIT(COUT_LOG,"            String attribute type: %s.\n",ddlStringStrings[type]);
	}

	if (strRefType == ds_DevSpec)// aka st_Literal
	{
		if ( reconcile )
		{
			wstring k;
			ConvertToString::fetchLiteralString(strInt, k);
			LOGIT(COUT_LOG,L"       Literal String: '%s'\n",k.c_str() );
		}
		else
		{
			LOGIT(COUT_LOG,"       Literal String Table entry: #%d\n",strInt);
		}
	}
	else if (strRefType == ds_Dict)// aka st_Dictionary
	{		
		if ( reconcile )
		{
			wstring k;
		    ConvertToString::fetchDictionString(strInt, k);

			if (k.compare(L"No Label Available")==0)
			{
				k = L"";
			}

			LOGIT(COUT_LOG,L"    Dictionary String: '%s'\n",k.c_str() );
		}
		else
		{
			unsigned hi, lo;
			hi = (strInt >> 16) & 0xffff;
			lo =  strInt & 0xffff;
			LOGIT(COUT_LOG,"                   Dictionary Key: 0x%08x [%d, %d]\n",strInt,hi,lo);
		}
	}
	else if (strRefType == ds_VarRef || strRefType == ds_Var)// aka st_Reference  // complex or simple reference [6/24/2015 timj]
	{
		LOGIT(COUT_LOG,"                        Reference:\n");
		strRef->out();
	}
	else if (strRefType == ds_Enum)
	{
		LOGIT(COUT_LOG,"     Enum number %2u in Reference:\n",strInt);
		if ( reconcile )
		{
			abCReference ref(0);
			ref.refType = VARIABLE_ID_REF;
			ref.intValue = strRef->intValue;
			ref.pRef = strRef->pRef;
			ref.pExpr = strRef->pExpr;
			ref.out();
		}
		else
		{
			strRef->out();
		}
	}
	else if (strRefType == ds_AttrRef)
	{
		LOGIT(COUT_LOG,"     Attr %s (%2u) in Reference:\n",fmaTypeStrings[strInt],strInt);
		strRef->out();
	}
	else if (strRefType == ds_EnumRef)// aka st_EnumDescRef
	{
		LOGIT(COUT_LOG,"     Enum number %2u in Reference:\n",strInt);
		strRef->out();
	}
	else
	{
		LOGIT(COUT_LOG,"ERROR: Undefined string type %u\n",(unsigned)strRefType);
	}

	return 0;
}

