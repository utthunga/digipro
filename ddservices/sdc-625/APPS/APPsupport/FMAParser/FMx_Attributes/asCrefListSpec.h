/*************************************************************************************************
 *
 * filename  asCrefListSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version Reference List Specifier Attribute class
 *
 */

#ifndef _ASCrefListSPEC_
#define _ASCrefListSPEC_

class asCrefListSpec : public  FMx_Attribute
{
public:
	asCrefListSpec(int tag) : FMx_Attribute(tag) {};
};

#endif //_ASCrefListSPEC_


