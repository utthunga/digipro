/*************************************************************************************************
 *
 * filename  asCdisplaySize.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Display Size Attribute class
 *
 */

#ifndef _ASCDISPLAYSIZE_
#define _ASCDISPLAYSIZE_

#include "..\FMx_Attribute.h"
/*
Display_Size ::=  
ENUMERATED (size 1)
{
	XX_SMALL			(0),
	X_SMALL				(1),
	SMALL				(2),
	MEDIUM				(3),
	LARGE				(4),
	X_LARGE				(5),
	XX_LARGE			(6),
	XXX_SMALL			(7)
}
*/
class asCdisplaySize : public FMx_Attribute
{
public:	// construct
	asCdisplaySize(int tag) : FMx_Attribute(tag) {};

public:	// data
	int value;
	int rawValue;

public:	// methods
	virtual int out(void)
	{
		extern unsigned currentObjectIndex;
		char *sizes[] = {"invalid value", "XX_SMALL", "X_SMALL", "SMALL", "MEDIUM", "LARGE", "X_LARGE", "XX_LARGE", "XXX_SMALL", "invalid value"};

		int i = 8;
		if (value >= 0 || value <= 7)
			i = value;

		FMx_Attribute::out();
		LOGIT(COUT_LOG,"     Display Size: 0x%02x (%d)  %s\n\n",value,value, sizes[i]);
		return 0;
	}

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}

	virtual aCattrBase* generatePrototype()
	{
		return createLongAttribute(attrType);
	}


	static asCdisplaySize *generateDefault(unsigned atype, int unsigned val = MEDIUM_DISPSIZE)
	{
		asCdisplaySize *spec = new asCdisplaySize(0);
		if (spec)
		{
			spec->value = val;
			spec->attrType = atype;
		}
		return spec;
	}

};

#endif //_ASCDISPLAYSIZE_
