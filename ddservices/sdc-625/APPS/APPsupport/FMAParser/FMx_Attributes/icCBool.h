/*************************************************************************************************
 *
 * filename  icCBool.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base BOOL Attribute class
 *
 */

#ifndef _ICCBOOL_
#define _ICCBOOL_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCBool: public FMx_Attribute
{
public: // construct 
	icCBool() : value(FALSE) {};
	void destroy(void){};// we have no pointers

public: // data
	bool value;

public: // methods
	virtual int icCBool::out(void)
	{
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"					      Boolean: %d\n",value);
		return 0;
	};

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = NULL;
		if ( pV == NULL )
		{
			pl = new aCddlLong;
		}
		else
		{
			pl = (aCddlLong*)pV;
		}
		pl->ddlLong = (this->value) ? 1L : 0L;

		return pl;
	};
	virtual aCattrBase* generatePrototype()
	{
		aCattrLong* attr = new aCattrLong;
		generatePayload( &attr->theLong );

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	};
};

#endif //_ICCBOOL_