/*************************************************************************************************
 *
 * filename  asCvarType.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for Base Variable Type Attribute
 *
 */

#include "logging.h"
#include "asCvarType.h"
#include "..\v_N_v+Debug.h"


int asCvarType::out(void)
{
	if (!FMx_Attribute::out())// on success
	{
		if (reconcile)
		{
			LOGIT(COUT_LOG," Variable Type: %s\n\n",varType_Str[varType]);
			LOGIT(COUT_LOG," Variable Size: 0x%04x (%u)\n",varSize,varSize);
		}
		else
		{
			LOGIT(COUT_LOG," Variable Type: 0x%02x (%u) %s\n\n",varType,varType,varType_Str[varType]);
			LOGIT(COUT_LOG," Variable Size: 0x%04x (%u)\n",varSize,varSize);
		}

		if (notHART)
		{
			LOGIT(COUT_LOG,"=+*=+*=+*=+*=+*=+*=+*=+*=+* NOT A HART VARIABLE TYPE =+*=+*=+*=+*=+*=+*=+*=+*=+*=+*\n");
		}	
	}

	return 0;
}

aCattrBase* asCvarType::generatePrototype()
{
	aCattrTypeType* attr = new aCattrTypeType;
	
	attr->notCondTypeType.actualVarType = varType;
/*  stevev 05aug13 - i'm going to believe the tokenizer */	
	attr->notCondTypeType.actualVarSize = varSize;
/* instead of faking it
	switch (varType)
	{
		case vt_no_type:
		case vt_undefined:
		case vt_time:
		case vt_date_and_time:
		case vt_duration:
		case vt_euc:
		case vt_octet_string:
		case vt_visible_string:	
		case vt_object_reference:
			return NULL;

		case vt_integer:
		case vt_unsigned:
		case vt_enum:
		case vt_bitEnum:
		case vt_index:
		case vt_ascii:	
		case vt_pkdAscii:			
		case vt_password:		 
		case vt_bit_string:	
		case vt_time_value:
			attr->notCondTypeType.actualVarSize = sizeof(int);
			break;

		case vt_date: 
			attr->notCondTypeType.actualVarSize = sizeof(int); // TODO: What is the size
			break;

		case vt_float:
			attr->notCondTypeType.actualVarSize = sizeof(float);
			break;

		case vt_double:
			attr->notCondTypeType.actualVarSize = sizeof(double);
			break;
	}
	* * */
	if ( ! varSize )
	{
		LOGIT(CERR_LOG," Variable Size zero for Var Type: 0x%02x (%s)\n",
																varType,varType_Str[varType]);
	}
	attr->attr_mask = maskFromInt(attrType);

	return attr;
}
