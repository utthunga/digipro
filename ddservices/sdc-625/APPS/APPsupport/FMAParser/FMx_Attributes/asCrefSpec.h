/*************************************************************************************************
 *
 * filename  asCrefSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version Reference Specifier Attribute class
 *
 */

#ifndef _ASCrefSPEC_
#define _ASCrefSPEC_

class asCrefSpec : public FMx_Attribute
{
public:
	asCrefSpec(int tag) : FMx_Attribute(tag) {attname = "Reference Specifier";};
};

#endif //_ASCrefSPEC_


