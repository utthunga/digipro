/*************************************************************************************************
 *
 * filename  asCdefaultValuesList.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Default Value List Attribute class
 * Definition for Base Expression List Element Attribute class
 * Definition for Base Expression List Attribute class
 *
 */

#ifndef _asCdefaultValuesList_
#define _asCdefaultValuesList_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCexpressionList;

class icCexpressionListElem : public FMx_Attribute
{
public:
	icCexpressionListElem() {expr=0; exprList=0;attname = "Expression List Element";}
	~icCexpressionListElem(){ destroy(); };
	abCExpression *expr;
	icCexpressionList* exprList;

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) = 0;
	virtual void destroy(void); 
	virtual int out(void);

	void hydratePrototype(aExpressionListElement* elem);
};

class icCexpressionList : public FMx_Attribute
{
public:
	icCexpressionList() {attname = "Expression List";} 
	vector<icCexpressionListElem*> exprElemList;

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) {return -1;};
	virtual void destroy(void);
	virtual int out(void);

	void hydratePrototype(aExpressionList* list)
	{
		icCexpressionListElem* elem;
		vector<icCexpressionListElem*>::iterator it;
		for (it = exprElemList.begin(); it != exprElemList.end(); ++it)
		{
			elem = *it;

			aExpressionListElement* paelem = new aExpressionListElement;
			elem->hydratePrototype(paelem);

			list->push_back(paelem);
		}
	}
};

class asCdefaultValuesList: public FMx_Attribute
{
public: // construct 
	asCdefaultValuesList() {attname = "Default Values List";};  
    
public: // data
	vector<abCReference*> refs;				// l-value
	vector<icCexpressionList*> values;		// r-value

    // this is in the base class   char *attname;
    
public: // methods
	virtual void destroy(void); 
	virtual int out(void);

	virtual aCattrBase* generatePrototype()
	{
		aCattrDefaultValuesList* attr = new aCattrDefaultValuesList;

		for (vector<abCReference*>::iterator it = refs.begin(); it != refs.end(); ++it)
		{
			abCReference* r = *it;
			
			aCreference aref;
			r->hydratePrototype(&aref);
			attr->refs.push_back(aref);
		}

		for (vector<icCexpressionList*>::iterator it = values.begin(); it != values.end(); ++it)
		{
			icCexpressionList* list = *it;

			aExpressionList* alist = new aExpressionList;
			list->hydratePrototype(alist);
			attr->values.push_back(alist);
		}
#ifdef _DEBUG
		if (attr->refs.size() != attr->values.size())
		{
			LOGIT(CERR_LOG,"aCattrDefaultValuesList generated a non-symetrical list.\n");
		}
#endif

		attr->attr_mask = maskFromInt(attrType);
out();
		return attr;
	};  
};

#endif //_asCdefaultValuesList_




