/*************************************************************************************************
 *
 * filename  asCasciiStr.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base ASCII String Attribute class
 *
 */

#ifndef _ASCASCIISTR_
#define _ASCASCIISTR_

#include "..\FMx_Attribute.h"

class asCasciiStr : public FMx_Attribute
{
public:
	asCasciiStr(int y) : str(""), len(0), pStore(NULL)  { attname = "ASCII String"; };

protected:
	UCHAR* pStore;

public: // data
	string	str;
	int		len;

public: // methods
	virtual void destroy(void)
	{
		delete pStore;
	}

	virtual int out(void)
	{
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"%23s:\n",attname);
		LOGIT(COUT_LOG,"%23s: 0x%04x (%u)\n","Length", len,len);
		LOGIT(COUT_LOG,"%23s: %s\n","Text", str.c_str());

		return 0;
	}

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlString* pl = NULL; //new aCddlString;
		// TODO: fix me

		// stevev 18sep15 - translate to utf8 and load aCddlString

		return pl;
	}

	virtual aCattrBase* generatePrototype()
	{
		return createStringAttribute(attrType);
	}

	string to_String()
	{
		return str;
	}
};

#endif //_ASCASCIISTR_