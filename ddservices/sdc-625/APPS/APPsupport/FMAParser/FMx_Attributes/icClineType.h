/*************************************************************************************************
 *
 * filename  icClineType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Line Type Attribute class
 *
 */

#ifndef _icClineType_
#define _icClineType_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icClineType: public FMx_Attribute
{
public: // construct 
	icClineType() : value(0L) {attname = "Line Type";};   // initialize underlying class data
    
public: // data
	unsigned choice;       // implicit tag value
	unsigned value;		// populated when choice == 1
    
public: // methods
	virtual void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *labels[] = {"invalid line type", "data-n", "low-low-limit", "low-limit", 
			"high-limit", "high-high-limit", "transparent"};

		char *label = (0 < choice  &&  choice <= 6) ? labels[choice] : labels[0];
		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)  %s\n", "Choice", choice, choice, label);
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Value", value, value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCintWhich* pl = new aCintWhich;
		pl->which = choice;
		pl->value = value;

		return pl;
	}
};

#endif //_icClineType_




