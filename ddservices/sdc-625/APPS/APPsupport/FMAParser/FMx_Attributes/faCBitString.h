/*************************************************************************************************
 *
 * filename  faCBitString.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Bit String Attribute class
 *
 */

#ifndef _FACBITSTRING_
#define _FACBITSTRING_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class faCBitString: public FMx_Attribute
{
public: // construct 
	faCBitString() : value(0L) {};
	void destroy() {};

public: // data
	UINT64 rawValue;
	UINT64 value;

public: // methods
	virtual int out(void)
	{
		LOGIT(COUT_LOG,"					        UINT64: 0x%16llx (%lld)\n",value,value);
		return 0;
	};

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_FACBITSTRING_