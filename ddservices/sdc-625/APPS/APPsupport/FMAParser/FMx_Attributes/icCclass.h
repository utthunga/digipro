/*************************************************************************************************
 *
 * filename  icCclass.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Class Attribute class
 *
 */

#ifndef _icCclass_
#define _icCclass_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "ddbdefs.h"

class icCclass: public FMx_Attribute
{
public: // construct 
	icCclass() : value(0L) {attname = "Class";};   // initialize underlying class data
	icCclass(const icCclass& x) { operator=( x );};
	icCclass& operator=(const icCclass& src)
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		value = src.value; rawValue = src.rawValue; return *this;
	} ;
	virtual ~icCclass() {};
    
public: // data
	UINT64 value;        // bitstring encoded as FM8 class
	UINT64 rawValue;	// bistring as encoded in the file
    
public: // methods
	void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		static wchar_t *classes[] = {\
			CLASSSTRINGS 
		};
//usage: char varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN] = {CLASSSTRINGS};

		UINT64 x = value;
		FMx_Attribute::out();

        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%016llx (%lld)\n", "Bit String", x, x);

		UINT64 onebit = 0x01;
		for (int n=0; n < CLASSSTRCOUNT; n++)
		{
			UINT64 bsn = (onebit << n);	// bitstring with bit set at position n
			if ( bsn & x )
			{
				LOGIT(COUT_LOG,_T("%23s: 0x%016llx (%lld)\n"), classes[n], bsn, bsn);
			}
		}
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCbitString* pb = new aCbitString;
		pb->bitstringVal= value;

		if ( QANDA_8vA ) 
		{
			// suppress these two classes which are handled unevenly in 8 and A
			pb->bitstringVal &= ~(SERVICE_CLASS | DIAGNOSTIC_CLASS);
		}

		return pb;
	}
};

#endif //_icCclass_



