/*************************************************************************************************
 *
 * filename  asTransactionLst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Transaction List Attribute class
 *
 * #include "asTransactionLst.h"
 *
 */

#ifndef _ASTRANSACTIONLST_
#define _ASTRANSACTIONLST_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes\icCtransaction.h"
//#include "..\FMA_Attributes\asCdataFieldSpecA.h"
//#include "..\FMA_Attributes\asCrespCodesSpecA.h"
//#include "..\FMA_Attributes\asCactionListSpecA.h"

class asTransactionLst : public FMx_Attribute
{
public:
	asTransactionLst(int tag) : FMx_Attribute(tag) {};

public: // data
	vector<icCtransaction*> transactionList;

public:
	virtual int out(void) 
	{  
		FMx_Attribute::out();
		for (unsigned i = 0; i < transactionList.size(); i++)
		{
			transactionList[i]->out();
		}
		return 0; 
	};

	virtual aCattrBase* generatePrototype(void);
};

#endif //_ASTRANSACTIONLST_