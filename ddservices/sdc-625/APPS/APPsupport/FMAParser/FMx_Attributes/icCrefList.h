/*************************************************************************************************
 *
 * filename  icCrefList.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Reference List Attribute class
 *
 */

#ifndef _icCrefList_
#define _icCrefList_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCrefListElem: public FMx_Attribute, public vector<abCReference*>
{
public: // construct 
	char *attname;
	icCrefListElem()  { attname = "Reference List"; };   // initialize underlying class data
	icCrefListElem(const icCrefListElem& src) { operator=(src); };
	icCrefListElem &operator=(const icCrefListElem& src) 
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		vector<abCReference *>::iterator iT;
		vector<abCReference *> *pList = (vector<abCReference *> *) &src;
		for( iT = pList->begin(); iT != pList->end(); ++iT)
		{
			abCReference *pRef = (*iT);
			if ( pRef != NULL )
			{
				abCReference *p = new abCReference(*pRef);
				this->push_back(p);
			}
		}//next
		return *this;
	};
	virtual ~icCrefListElem() {};

public: // data
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
		if ((*this).size())
		{
			for (unsigned i=0; i<(*this).size(); i++)
			{
				if ((*this)[i])
				{
					(*this)[i]->destroy();
					delete (*this)[i];
				}
			}
		}
    };
    
	virtual int out(void) 
    {
		FMx_Attribute::out();
		if (reconcile == false)
        {
	        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        }
		if ((*this).size())
		{
			for (unsigned i=0; i<(*this).size(); i++)
			{
				if ((*this)[i])
				{
					(*this)[i]->out();
				}
			}
		}
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCreferenceList* lst = NULL;
		if (pV)
		{
			lst = (aCreferenceList*)pV;
		}
		else
		{
			lst = new aCreferenceList;
		}

		for (vector<abCReference*>::iterator it = (*this).begin(); it != (*this).end(); ++it)
		{
			abCReference* r = *it;

			aCreference ddl_ref;
			r->hydratePrototype(&ddl_ref);

			lst->push_back(ddl_ref);
		}

		return lst;
	};

	// when this is used standalone (with no condional wrapper), 
	// it has top generate a conditional attribute (in all conditions true today)
	aCattrBase* generatePrototype() 
	{ 
		aCattrReferenceList *pAttr = new aCattrReferenceList;//isa  AcondReferenceList_t
															 //isa aCcond(*this)..<aCreference(*this)>

		aCgenericConditional* ddlCond = new aCgenericConditional;
		ddlCond->priExprType = eT_Direct;// from cdt_DIRECT

		aCgenericConditional::aCexpressDest* exprDest= new aCgenericConditional::aCexpressDest;
		exprDest->destType = eT_Direct;
		// create and assign the payload object
		exprDest->pPayload = generatePayload();

		ddlCond->destElements.push_back(*exprDest);

		pAttr->RefList.genericlist.push_back(*ddlCond);

		pAttr->attr_mask = maskFromInt(attrType);

		return pAttr;

	};
};

#endif //_icCrefList_




