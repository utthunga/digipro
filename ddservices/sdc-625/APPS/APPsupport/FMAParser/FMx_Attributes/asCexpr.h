/*************************************************************************************************
 *
 * filename  asCexpr.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Expression Attribute class
 *
 */

#ifndef _ASCEXPR_
#define _ASCEXPR_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "asCintegerConst.h"// for special case

template<class EXPRESSIONSPEC>
class asCexpr : public FMx_Attribute
{
public:
	asCexpr(int tag = 0) : FMx_Attribute(tag) { attname = "Expression"; };
	asCexpr(const asCexpr& src) { operator=(src); };
	//virtual ~asCexpr() { if (pExpr) delete pExpr; pExpr=NULL; };
	virtual ~asCexpr() { Expr.clear(); };
	void destroy()  { Expr.clear(); };

public:
	//abCExpression* pExpr;
	EXPRESSIONSPEC Expr;

	asCexpr& operator=(const asCexpr& s) 
	{  
		FMx_Attribute::operator=(*((FMx_Attribute *)(&s)));
		Expr = s.Expr; return *this;
	};

	virtual int out(void) 
    {
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
		//if (pExpr)
		//	pExpr->out();
		Expr.out();
		
		return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCexpression exp;
		//pExpr->hydratePrototype(&exp);
		Expr.hydratePrototype(&exp);

		aCExpressionPayload* pl = new aCExpressionPayload;
		pl->theExpression = exp;

		return pl;
	}

	// create an integer_const attr from this expression, if it is
	// an one-element list of one integer constant attr
	// very special case converting ValueArray length attr
	// from A to 8
	asCintegerConst *asIntegerConst()
	{
		// caller OWNS the memory allocated here
		//if (pExpr->exprList.size() == 1)
		if (Expr.exprList.size() == 1)
		{
			expressionElement* p1Elem;
			//eepIt_t elstIT = pExpr->exprList.begin();
			eepIt_t elstIT = Expr.exprList.begin();
			p1Elem = *elstIT;
			assert(p1Elem);
			if (p1Elem->exprElemType == eet_INTCST)
			{
				asCintegerConst *pic = new asCintegerConst(0);
				pic->attrType = attrType;
				pic->origType = origType;

				pic->value = (int) p1Elem->eeInt;
				return pic;
			}
		}

		return 0;
	}

};

#endif //_ASCEXPR_