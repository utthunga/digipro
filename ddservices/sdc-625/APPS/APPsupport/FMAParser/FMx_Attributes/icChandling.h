/*************************************************************************************************
 *
 * filename  icChandling.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Handling Attribute class
 *
 */

#ifndef _icChandling_
#define _icChandling_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icChandling: public FMx_Attribute
{
public: // construct 
	icChandling() : value(0L) {};   // initialize underlying class data
	void destroy(void){};// we have no pointers
    
public: // data
	UINT64 value;        // whatever data is included in the underlying class
    
public: // methods
	virtual int out(void) 
    {
		FMx_Attribute::out();

        LOGIT(COUT_LOG,"					     Handling:\n");
        LOGIT(COUT_LOG,"					   Bit String: 0x%016llx (%lld)\n",value,value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		//aCddlLong* pl = new aCddlLong;
		//pl->ddlLong = value;

		aCbitString* pb = new aCbitString;
		pb->bitstringVal= value;

		return pb;
	}
};

#endif //_icChandling_



