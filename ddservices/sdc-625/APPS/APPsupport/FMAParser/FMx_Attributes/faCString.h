/*************************************************************************************************
 *
 * filename  faCString.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base String Attribute class
 *
 */

#ifndef _FACSTRING_
#define _FACSTRING_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Dict.h"
#include "..\FMx.h"

#include "QA+Debug.h"

class faCString : public FMx_Attribute
{
public: // construct 
	faCString() : strInt(0), strRefType(ds_Undefined), strRef(0) {};
	faCString(const faCString& src) { operator=(src); } ;
	faCString& operator=(const faCString& s){
		FMx_Attribute::operator=(*((FMx_Attribute *)(&s)));
		strInt = s.strInt; strRefType = s.strRefType;
		if (s.strRef) strRef = new abCReference(*(s.strRef));
		return *this;
	};
	virtual ~faCString() { destroy(); };

public: // data
	abCReference*   strRef;
	int             strInt;
	// we convert all to version 8 types.... stringRefType_t strRefType;
	ddlstringType_t strRefType;

public: // methods
	virtual void destroy(void)
	{
		if (strRef)
			strRef->destroy();
		strRef = NULL;
	}

	virtual int out(void);

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		return toDDLString();
	}

	aCddlString* toDDLString()
	{
		aCddlString* p = new aCddlString;
		hydratePrototype(p);

		return p;
	}
	aCddlString* toDDLString(aCddlString* p)
	{
		hydratePrototype(p);

		return p;
	}

	void hydratePrototype(aCddlString* pStr)
	{
		unsigned blankkey = 0x01260009;		// [blank]
		unsigned key = 0;

		pStr->clear();	//  [7/9/2015 timj]
		pStr->flags  = 1;// to match earlier tokenizers
		pStr->isSet = false;

		switch (strRefType)
		{
			case ds_DevSpec:
			{
				key = strInt;
				if (key == blankkey && QANDA)
				{
					key = (unsigned long)((400 << 16) + 1);  // DEFAULT_DEV_SPEC_STRING;
				}
				if (strInt != blankkey)
				{
#ifndef TOKENIZER	// tokenizer does not load the string unless required
					// no longer used.. FMx::StringDict->retrieveStr(key, pStr->theStr);
					pLIT_STR->retrieveStr(key, pStr->theStr);
					pStr->isSet  = true;
					pStr->len    = pStr->theStr.size();
					if (QANDA_8v8) strInt = 0;
#endif
					pStr->enumVal = strInt; // stevev 11nov15 - record the reference for importing
				}
				pStr->strTag = strRefType;
				break;
			}

			case ds_Var:
			{
				pStr->strTag = strRefType;
				pStr->ddItem = strInt;
				break;
			}

			case ds_Enum:
			{
				pStr->strTag = strRefType;
				pStr->enumVal = strInt; // tok work to try and get these to match
				pStr->ddItem = 0;// stevev 10nov2015 ..remove to match the old parserinfc      strInt;

				if (!QANDA_AvA)
				{
					pStr->enumVal = strInt;	//   [2/5/2016 tjohnston]
				}

				// for situation where ds_EnumRef is converted to ds_Enum
				// during evaluate, strRef will be populated
				//  [4/17/2014 timj]
				if (strRef)
				{
					pStr->pRef = new aCreference;
					strRef->hydratePrototype(pStr->pRef);
					if (pStr->pRef->type == VARIABLE_ITYPE/* 1 */ )
					{
						pStr->pRef->type = RESERVED_ITYPE1;/* 0 */
						pStr->pRef->rtype = 0; // 0___undocumented
						pStr->ddItem = 0;
					}

					if (QANDA  &&  pStr->theStr == L""  &&  pStr->pRef->type == ITEM_ARRAY_ITYPE) //  [7/16/2015 timj]
					{
						pStr->pRef->type = RESERVED_ITYPE1;
						pStr->pRef->isRef = 0;
						pStr->pRef->rtype = 0; // 0___undocumented
						pStr->ddItem = 0;
					}

				}
				break;
			}

			case ds_Dict:
			{
				// tokenizer creates labels for some item types that have none and
				// assigns [blank] as the string value
				key = strInt;
				if (strInt != blankkey)
				{
					// pass all [blank] dict strings as empty strings.
					// SDC will handle these appropriately
					// no longer used ... FMx::DictionaryDict->retrieveStr(key, pStr->theStr);
					pDICTSTR->retrieveStr(key, pStr->theStr);
					pStr->len = pStr->theStr.size();
					pStr->isSet = true;
				}
				if (QANDA_8v8) 
				{
					strInt = 0;
#ifdef _QANDA
					int atype = qaAttrType & 0xff;
					if ((atype == VAR_LABEL_ID || atype == VAR_HELP_ID) && pStr->theStr.empty())
					{
						pStr->theStr = L"ERROR: Dict String Not Found";
						pStr->len = 28;
					}
#endif
				}
				pStr->strTag = strRefType;
				pStr->enumVal = strInt;
				break;
			}

			case ds_VarRef:
			{
				pStr->strTag = strRefType;
				pStr->pRef = new aCreference;
				strRef->hydratePrototype(pStr->pRef);
				break;
			}

			case ds_EnumRef:
			{
				pStr->strTag = strRefType;
				pStr->enumVal = strInt;
				pStr->pRef = new aCreference;
				strRef->hydratePrototype(pStr->pRef);
				break;
			}

			// used for default case
			case ds_NoString:
			{
				pStr->theStr = L"";	// Removed No Help Available, DDHost Test requires a blank label [6/25/2014 timj]
				pStr->isSet = true;
				pStr->len = pStr->theStr.size();
				pStr->strTag = ds_Dict;// merge this change into fma_parser [5/31/2016 tjohnston]

				if (QANDA_8vA)
					pStr->strTag = ds_DevSpec;// to match the V8 tok

				break;
			}

			case ds_MaxValid:
			case ds_Undefined:
			{
				// TODO: Log message
				break;
			}
		}
		if (pStr->theStr == L"ERROR: Dict String Not Found") // DELETE ME [2/5/2016 tjohnston]
		{
			int x = 0;
		}
		if (pStr->theStr == L"++Missing Enum String++") // DELETE ME [2/5/2016 tjohnston]
		{
			int x = 0;
		}
	}


};

#endif //_FACSTRING_
