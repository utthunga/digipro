/*************************************************************************************************
 *
 * filename  faCLong.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Long Attribute class
 *
 */

#ifndef _FACLONG_
#define _FACLONG_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class faCLong: public FMx_Attribute
{
public: // construct 
	faCLong() : value(0L) {};
	faCLong(const faCLong& x) { operator=( x );};
	faCLong& operator=(const faCLong& src)
	{
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		value = src.value;return *this;
	};
	virtual ~faCLong() {};
	void destroy(){};

public: // data
	unsigned long value;

public: // methods
	virtual int out(void)
	{
		LOGIT(COUT_LOG,"					Unsigned Long: 0x%08x (%d)\n",value,value);
		return 0;
	};

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_FACLONG_