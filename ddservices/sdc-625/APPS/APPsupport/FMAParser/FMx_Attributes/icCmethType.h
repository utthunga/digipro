/*************************************************************************************************
 *
 * filename  icCmethType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Method Type Attribute class
 *
 */

#ifndef _icCmethType_
#define _icCmethType_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCmethType: public FMx_Attribute
{
public: // construct 
	icCmethType() : value(0L) {attname = "Method Type";};   // initialize underlying class data
    
public: // data
	unsigned      value;        // type
    
public: // methods
	virtual int out(void) 
    {
		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Value", value, value);
        return 0;
    };

	virtual aCattrBase* generatePrototype()
	{
		aCparameter* pp = new aCparameter;
		pp->type        = value;
		pp->modifiers   = 0;
		pp->paramName.clear();
		
		pp->attr_mask = maskFromInt(attrType);

		return pp;
	}
};
#endif //_icCmethType_




