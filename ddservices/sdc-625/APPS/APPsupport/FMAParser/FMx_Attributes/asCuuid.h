/*************************************************************************************************
 *
 * filename  asCuuid.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base UUID Attribute class
 *
 */

#ifndef _asCuuid_
#define _asCuuid_

#include "..\FMx_Attribute.h"

class asCuuid : public FMx_Attribute
{
public: // construct 
	asCuuid() { attname = "UUID"; };   // initialize underlying class data
	virtual ~asCuuid() {}
    
public: // data
	UINT64 upper_half;
	UINT64 lower_half;
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
    };
    
	virtual int out(void) 
    {
		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%016llx\n", "Upper Half", upper_half);
        LOGIT(COUT_LOG,"%23s: 0x%016llx\n", "Lower Half", lower_half);

		char buf[100];
        sprintf(buf, "%016llx", upper_half);
        sprintf(buf+16, "%016llx", lower_half);
		string u = buf;

		u.insert(8, "-");
		u.insert(13, "-");
		u.insert(18, "-");
		u.insert(23, "-");

		// todo format per iso spec

		LOGIT(COUT_LOG,"%23s: %s\n", "Formatted UUID", u.c_str());

        return 0;
   };
 
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset) { return 0; };
    
	virtual aCattrBase* generatePrototype()
    {
        aCattrUuid* attr = new aCattrUuid;

		attr->lower_half.ddlLong = lower_half;
		attr->upper_half.ddlLong = upper_half;

		attr->attr_mask = maskFromInt(attrType);

		return attr;
    };  
};

#endif //_asCuuid_



