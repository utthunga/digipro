/*************************************************************************************************
 *
 * filename  asCwaveType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Wave Type Attribute class
 *
 */

#ifndef ASCWAVETYPE
#define ASCWAVETYPE

#include "..\FMx_Attribute.h"

class asCwaveType : public FMx_Attribute
{
public:
	asCwaveType(int y) : FMx_Attribute(y) {};

public:	// data
	int rawValue;
	int value;

public:	// methods
	virtual int out(void)
	{
		char *typeStrings[] = {
			"Invalid",
			"YT Waveform Type",
			"XY Waveform Type",
			"Horz Waveform Type",
			"Vert Waveform Type"
		};

		char *t = "wave type out of range error";
		if (0 <= value  &&  value <= 4)
			t = typeStrings[value];

		if (!FMx_Attribute::out())
		{			
			LOGIT(COUT_LOG,"%23s: %s   0x%02x (%d)\n\n", "Wave Type", t, value, value);
		}

		return 0;
	};

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}

	virtual aCattrBase* generatePrototype()
	{
		return createLongAttribute(attrType);
	}
};

#endif //ASCWAVETYPE