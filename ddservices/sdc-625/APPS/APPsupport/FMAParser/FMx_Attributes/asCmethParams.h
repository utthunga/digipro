#ifndef _ASCmethParams_
#define _ASCmethParams_

#include "..\FMx_Attributes\icCmethParam.h"
#include "..\FMx_Conditional.h"

class asCmethParams : public FMx_Attribute
{
public:
	asCmethParams(int tag) : FMx_Attribute(tag) { attname = "Method Parameters"; };
	vector<icCmethParam *>	paramList;
    
public:
	virtual void destroy(void) 
    { 
		for (unsigned i=0; i<paramList.size(); i++)
		{
			icCmethParam *p = paramList[i];
			p->destroy();
			delete p;
		}
    };
    
	virtual int out(void) 
    {
        FMx_Attribute::out();  
		for (unsigned i=0; i<paramList.size(); i++)
		{
			icCmethParam *p = paramList[i];
			p->out();
		}
        return 0; 
    };
    
	virtual aCattrBase* generatePrototype()
    {
		aCparameterList* list = new aCparameterList;
		
		for (vector<icCmethParam*>::iterator it = paramList.begin(); it != paramList.end(); ++it)
		{
			icCmethParam* attr = *it;

			aCparameter* param = (aCparameter*)(attr->generateElement());
			list->push_back(*param);
		}

		list->attr_mask = maskFromInt(attrType);

		return list;
	};
};

#endif //_ASCmethParams_


