/*************************************************************************************************
 *
 * filename  icMinMax.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Min Max Attribute class
 *
 */

#ifndef _icCminmax_
#define _icCminmax_

#include "..\FMx_Attribute.h"
#include "asCexprSpec.h"

template<class EXPRESSIONSPEC>
class minmaxValue
{
public: // construct
	minmaxValue() { which = 0;  };
	minmaxValue(const minmaxValue<EXPRESSIONSPEC>& src) { operator=(src); };
	minmaxValue& operator=(const minmaxValue<EXPRESSIONSPEC>& s)
	{
		which      = s.which;
		Expression = s.Expression;
		return *this;
	};
	virtual ~minmaxValue() { Expression.destroy(); };

public:  // data
	int which; 
	EXPRESSIONSPEC Expression;// an A or 8 expression

public: //methods
	void clear() { which = 0; Expression.clear(); };
	void destroy(void) 
	{
		Expression.destroy();
		//if (pExpression)
		//{
		//	delete pExpression;
		//	pExpression = NULL;
		//}
	};

	int out(void) 
	{
		LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Which", which, which);

		Expression.out();

		return 0;
	};
};



template<class EXPRESSIONSPEC>
class icMinMax : public FMx_Attribute
{
public: // construct
	icMinMax() { };
	icMinMax( const icMinMax& src) { operator=(src); };
	virtual ~ icMinMax() { destroy();};
	icMinMax& operator=(const icMinMax& s){	
		FMx_Attribute::operator=(*((FMx_Attribute *)(&s)));
		vector<minmaxValue<EXPRESSIONSPEC>>::const_iterator iT;
		const minmaxValue<EXPRESSIONSPEC>* pmnmxVal;

		for (iT = s.values.begin(); iT != s.values.end(); ++iT)
		{// ptr2mimax
			pmnmxVal = &(*iT);
			values.push_back(*pmnmxVal);
		}

		return *this;
	};

public: // data
	vector<minmaxValue<EXPRESSIONSPEC>> values;

public: // virtuals
	virtual int out(void) 
	{
		vector<minmaxValue<EXPRESSIONSPEC>>::iterator iT;
		minmaxValue<EXPRESSIONSPEC>* pmnmxVal;

		for (iT = values.begin(); iT != values.end(); ++iT)
		{// ptr2mimax
			pmnmxVal = &(*iT);
			pmnmxVal->out();
		}

		return 0;
	};
	void destroy()	{
		
		vector<minmaxValue<EXPRESSIONSPEC>>::iterator iT;
		minmaxValue<EXPRESSIONSPEC>* pmnmxVal;

		for (iT = values.begin(); iT != values.end(); ++iT)
		{// ptr2mimax
			pmnmxVal = &(*iT);
			pmnmxVal->destroy();
		}
		values.clear();
	};




	// replaces generatePrototype  //  [3/17/2014 timj]
	virtual void* generateElement(aCminmaxList *localMMList) 
	{		
		vector<minmaxValue<EXPRESSIONSPEC>>::iterator iT;
		minmaxValue<EXPRESSIONSPEC>* pmnmxVal;

		for (iT = values.begin(); iT != values.end(); ++iT)
		{// ptr2minmax
			aCminmaxVal localMMVal;
			pmnmxVal = &(*iT);
			localMMVal.whichVal = pmnmxVal->which;
			pmnmxVal->Expression.hydratePrototype( &(localMMVal.condExpr) );
			localMMList->push_back(localMMVal);
		}

		return localMMList;
	};

};

#endif //_icCminmax_

