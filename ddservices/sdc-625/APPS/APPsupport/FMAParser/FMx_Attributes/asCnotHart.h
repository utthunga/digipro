/*************************************************************************************************
 *
 * filename  asCnotHart.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Not Hart Attribute class
 *
 */

#ifndef _ASCNOTHART_
#define _ASCNOTHART_

#include "..\FMx_Attribute.h"

class asCnotHart : public FMx_Attribute
{
public:	// construct
	asCnotHart(int tag) : FMx_Attribute(tag) {attname = "Not HART";};

public:	// data

public:	// methods
	virtual int out(void)
	{
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"%23s:\n\n",attname);
		return 0;
	};
	virtual bool inhibitOut() { return (reconcile) ? true : false; }
};

#endif //_ASCNOTHART_
