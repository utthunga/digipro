/*************************************************************************************************
 *
 * filename  faCMember.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Member Attribute class
 *
 */

#ifndef _FACMEMBER_
#define _FACMEMBER_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "faCString.h"
#include "asCasciiStr.h"


// this class is roughly equivelent to icCelements
class faCMember : public FMx_Attribute
{
public: // construct
	faCMember(int y=0) : memberID(0), memberSymbolIndex(0), itemRef(NULL), strDesc(NULL), strHelp(NULL), symbolName("") { attname = "Member"; };
	faCMember(const faCMember& elem) { operator=( elem );};
	faCMember& operator =(const faCMember& src) {
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		memberID = src.memberID;
		memberSymbolIndex = src.memberSymbolIndex;
		if (src.itemRef != NULL) itemRef = new abCReference(* (src.itemRef));
		if (src.strDesc != NULL) strDesc = new faCString(*(src.strDesc));
		if (src.strHelp != NULL) strHelp = new faCString(*(src.strHelp));
		symbolName = src.symbolName;
		return *this;
	};
	virtual ~faCMember() { destroy(); };

public: // data
	unsigned        memberID;			// symbolID of the member
	unsigned        memberSymbolIndex;	// index into the symbol table
	abCReference*   itemRef;
	faCString*      strDesc;
	faCString*		strHelp;
	string			symbolName;	

public: // methods

	virtual void GetSymbolName(string &name) { name = "undefined"; };

	virtual int out()
	{
		FMx_Attribute::out();

        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name

		string name;
		GetSymbolName(name);
		LOGIT(COUT_LOG,"%23s: %s\n","Symbol Name", name.c_str());  

		// 27jan14 timj print sym name before ID facilitates DD library maintenance
		LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Member ID", memberID, memberID);

		if (itemRef)
		{
			LOGIT(COUT_LOG,"%23s:\n","Item");  
			itemRef->out();
		}
		if (strDesc)
		{
			LOGIT(COUT_LOG,"%23s:\n","Description");  
			strDesc->out();
		}
		if (strHelp)
		{
			LOGIT(COUT_LOG,"%23s:\n","Help");  
			strHelp->out();
		}
		return 0;
	};

	virtual void destroy()
	{
		if (itemRef)
		{
			itemRef->destroy();
			delete itemRef;
			itemRef = 0;
		}
 
		if (strDesc)
		{
			strDesc->destroy();
			delete strDesc;
			strDesc = 0;
		}

		if (strHelp)
		{
			strHelp->destroy();
			delete strHelp;
			strHelp = 0;
		}
	};

	virtual void* generateElement(aCmemberElement *pElement)// the element isn't a payload, it is pushed on the payload
	{
		if (pElement == NULL)
		{
			pElement = new aCmemberElement;
		}// else use the passed in item

		pElement->locator = memberID;
		itemRef->hydratePrototype(&(pElement->ref));
		if ( strDesc )// description is optional
		{	
			pElement->descS.enumVal = -1;
			strDesc->toDDLString(&(pElement->descS));

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (strDesc->strRefType == ds_Dict && pElement->descS.enumVal == 0)
			{
				pElement->descS.enumVal = -1;
			}
		}
		if ( strHelp )// help is optional
		{
			pElement->helpS.enumVal = -1;
			strHelp->toDDLString(&(pElement->helpS));

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (strHelp->strRefType == ds_Dict && pElement->helpS.enumVal == 0)
			{
				pElement->helpS.enumVal = -1;
			}
		}


		GetSymbolName(symbolName);
		pElement->mem_name = symbolName;// lookupSymbol(memberSymbolIndex);

		return pElement;
	};
};

#endif //_FACMEMBER_
