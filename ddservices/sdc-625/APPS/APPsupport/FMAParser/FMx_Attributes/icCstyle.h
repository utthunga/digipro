/*************************************************************************************************
 *
 * filename  icCstyle.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Style Attribute class
 *
 */

#ifndef _icCstyle_
#define _icCstyle_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCstyle: public FMx_Attribute
{
public: // construct 
	icCstyle() : value(0L) {attname = "Style";};   // initialize underlying class data
    
public: // data
	unsigned value;        // whatever data is included in the underlying class
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
    };
    
	virtual int out(void) 
    {
		char *styles[] = {"invalid", "Window", "Dialog", "Page", "Group", "Menu", "Table" };

		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name

		char * s = value < 7 ? styles[value] : styles[0];

        LOGIT(COUT_LOG,"%23s: %-20s 0x%08x (%d)\n", "Value", s, value, value);
        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};
#endif //_icCstyle_




