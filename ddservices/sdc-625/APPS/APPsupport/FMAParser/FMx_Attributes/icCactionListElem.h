/*************************************************************************************************
 *
 * filename  icCactionListElem.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Action List Element Attribute class
 *
 */

#ifndef _icCactionListElem_
#define _icCactionListElem_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\faCByteString.h"

class icCactionListElem : public FMx_Attribute
{
public: // construct 
	icCactionListElem() : ref(0),/* method(0),*/ choice(0L) { attname = "Action List Element"; };   // initialize underlying class data

public: // data
	unsigned choice;
	abCReference *ref;
	//faCByteString *method;...Error: Inline method definitions are illegal in HART.
    
public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
		if (ref)
		{
			ref->destroy();
			delete ref;
			ref = 0;
		}
/*
		if (method)
		{
			method->destroy();
			delete method;
			method = 0;
		}
*/
    };
    
	virtual int out(void) 
    {
		if (reconcile == false)
        {
	        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        }
		if (choice == 0)
		{
			if (ref)
				ref->out();
			else
				LOGIT(COUT_LOG,"Error: icCactionListElemList: reference is null.\n");
		}
		if (choice == 1)
			//method->out();
			LOGIT(COUT_LOG,"Error: Inline method definitions are illegal in HART.\n");

        return 0;
    };

	virtual void generateElement(aCreference *elem)
	{
		if (choice == 0)
		{
			if (elem == NULL)
			{
				elem = new aCreference;
			}

			ref->hydratePrototype(elem);
		}

		else if (choice == 1)
		{
			// todo: don't know where to put a byte string
		}
	}
};

#endif //_icCactionListElem_




