/*************************************************************************************************
 *
 * filename  faCByteString.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Byte String Attribute class
 *
 */

#ifndef _faCByteString_
#define _faCByteString_

#include "..\FMx_Attribute.h"

class faCByteString : public FMx_Attribute
{
public: // construct 
	faCByteString(int tag = 0) : FMx_Attribute(tag), /*str(""),*/ len(0), pStore(NULL)  {};
	virtual ~faCByteString() { destroy(); };
protected:
	UCHAR* pStore;

public: // data
	//string	str;
	int		len;

public: // methods
	virtual void destroy(void)
	{
		if (pStore)
		{
			delete pStore;
			pStore = NULL;
		}
	};

	virtual int out(void);
	virtual aCattrBase* generatePrototype();
};

#endif //_faCByteString_
