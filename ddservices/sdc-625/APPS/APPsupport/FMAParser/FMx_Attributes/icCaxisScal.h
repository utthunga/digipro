/*************************************************************************************************
 *
 * filename  icCaxisScal.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Axis Scale Attribute class
 *
 */

#ifndef _icCaxisScal_
#define _icCaxisScal_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"

class icCaxisScal: public FMx_Attribute
{
public: // construct 
	icCaxisScal() : rawValue(0L), value(0L) {};   // initialize underlying class data
    
public: // data
	unsigned long rawValue;   
	unsigned long value;      
    
public: // methods
	void destroy(void){};// we have no pointers
	virtual int out(void) 
    {
		char *typeStrings[] = {"error", "Linear", "Logarithmic" };

		FMx_Attribute::out();

		char *t = "axis scaling value out of range";
		if (1 <= value  &&  value <= 2)
			t = typeStrings[value];

        LOGIT(COUT_LOG,"%33s: %s  0x%08x (%d)\n", "Axis Scaling", t, value, value);

        return 0;
    };

	virtual aPayldType* generatePayload(void* pV = NULL)
	{
		aCddlLong* pl = new aCddlLong;
		pl->ddlLong = value;

		return pl;
	}
};

#endif //_icCaxisScal_



