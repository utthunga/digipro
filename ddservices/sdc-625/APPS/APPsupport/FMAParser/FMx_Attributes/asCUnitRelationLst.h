/*************************************************************************************************
 *
 * filename  asCUnitRelationLst.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Unit Relation List Attribute class
 *
 * #include "asCUnitRelationLst.h"
 *
 */

#ifndef _ASCUNITRELATIONLST_
#define _ASCUNITRELATIONLST_

#include "..\FMx_Attribute.h"
#include "asCrefSpec8.h"
#include "asCrefListSpec8.h"

class asCUnitRelationLst : public FMx_Attribute
{
public:
	asCUnitRelationLst(int tag) : FMx_Attribute(tag) {};

public: // data
	asCrefSpec* unit;
	asCrefListSpec* vars;

public:
	virtual int out(void)
	{
		FMx_Attribute::out();
		if (unit)
		{
			unit->out();
		}
		if (vars)
		{
			vars->out();
		}

		return 0;
	}

	virtual int reconcile_out(void)
	{
		return 0;
	}

	virtual aCattrBase* generatePrototype()
	{
		aCattrUnititems* attr = new aCattrUnititems;

		((asCrefSpec8 *)unit)->conditional.hydratePrototype(&attr->condVar);

		((asCrefListSpec8 *)vars)->conditional.hydratePrototype(&attr->condListOUnitLists);

		attr->attr_mask = maskFromInt(attrType);

		return attr;
	}
};

#endif //_ASCUNITRELATIONLST_