/*************************************************************************************************
 *
 * filename  icCtransaction.cpp
 *
 * 26mar2014 - timj
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 *
 */

#include "..\FMx_Attributes\icCtransaction.h"
#include "..\FMx_Attributes_Ref&Expr.h"


void* icCtransaction::generateElement()
{
	// this method is unused
	return 0;
}
