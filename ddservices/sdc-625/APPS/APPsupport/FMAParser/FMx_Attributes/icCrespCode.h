/*************************************************************************************************
 *
 * filename  icCrespCode.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Response Code Attribute class
 *
 */

#ifndef _icCrespCode_
#define _icCrespCode_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "faCString.h"

class icCrespCode: public FMx_Attribute
{
public: // construct 
	icCrespCode() : value(0L), type(0L), description(0), help(0) { attname = "Response Code"; };   // initialize underlying class data
    
public: // data
	unsigned int value; 
	unsigned int type;  
   	faCString *description;
	faCString *help;

public: // methods
	virtual void destroy(void) 
    {
        // code to destroy complex objects
		
		if (description)
		{
			description->destroy();
			delete description;
		}
		description = 0;

		if (help)
		{
			help->destroy();
			delete help;
		}
		help = 0;
    };

	icCrespCode(const icCrespCode& elem) { operator=( elem );};
	icCrespCode& operator=(const icCrespCode& src) {
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		value = src.value;
		type = src.type;
		if (src.description)
			description = new faCString(*(src.description));
		if (src.help)
			help = new faCString(*(src.help));
		return *this;
	};

	~icCrespCode() { destroy(); };

	virtual int out(void) 
    {
		char *typeStrings[] = {
			"invalid",
			"Success", 
			"Misc-Warning", 
			"Data-Entry-Warning", 
			"Data-Entry-Error", 	
			"Mode-Error", 
			"Process-Error",		
			"Misc-Error"	
		};

        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name

        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Value", value, value);

		char *s = (type < 8) ? typeStrings[type] : typeStrings[0];
        LOGIT(COUT_LOG,"%23s: %-20s  0x%08x (%d)\n", "Type", s, type, type);
		
		if (description)
			description->out();

		if (help)
			help->out();

        return 0;
    };

	
	virtual void* generateElement(aCrespCode* rspCd)
	{
		if (rspCd == NULL)
			rspCd = new aCrespCode;

		rspCd->val = value;
		rspCd->type= type;

		if (description != NULL)
		{
			rspCd->descS.enumVal = -1;
			description->hydratePrototype(&(rspCd->descS));

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (description->strRefType == ds_Dict && rspCd->descS.enumVal == 0)
			{
				rspCd->descS.enumVal = -1;
			}
		}

		if (help != NULL)
		{
			rspCd->helpS.enumVal = -1;
			help->hydratePrototype(&(rspCd->helpS));

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (help->strRefType == ds_Dict && rspCd->helpS.enumVal == 0)
			{
				rspCd->helpS.enumVal = -1;
			}
		}

		return rspCd;
	};
};
#endif //_icCrespCode_




