/*************************************************************************************************
 *
 * filename  asCrespCodesSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for version 8 Response Code Specifier Attribute class
 *
 */

#ifndef _ASCrespCodesSPEC_
#define _ASCrespCodesSPEC_

#include "..\FMx_Attribute.h"

class asCrespCodesSpec : public  FMx_Attribute
{
public:
	asCrespCodesSpec(int tag) : FMx_Attribute(tag) {};
    
public:
	virtual void hydrateConditional(aCcondList* cond) {};
};

#endif //_ASCrespCodesSPEC_


