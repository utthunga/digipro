/*************************************************************************************************
 *
 * filename  asCdefaultValuesList.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for Base Default Value List Attribute
 * Implementation for Base Expression List Element Attribute
 * Implementation for Base Expression List Attribute
 *
 */

#include "asCdefaultValuesList.h"

int asCdefaultValuesList::out(void) 
{
	unsigned quant = max (refs.size(), values.size());

	FMx_Attribute::out();
	for (unsigned i=0; i < quant; i++)
	{
		LOGIT(COUT_LOG,"\n%s: %d\n\n","Default Value", i+1);
		if ( i < refs.size() && refs[i] != 0)
			refs[i]->out();
		else
			LOGIT(COUT_LOG,"No reference for this value.\n");
		if ( i < values.size() && values[i] != 0 )
			values[i]->out();
		else
			LOGIT(COUT_LOG,"No value for this reference.\n");
	}
	return 0;
}

int icCexpressionList::out(void) 
{
	for (unsigned i=0; i<exprElemList.size(); i++)
	{
		LOGIT(COUT_LOG,"\n%23s: Element %d of %d   ",attname, i+1, exprElemList.size());
		exprElemList[i]->out();
	}
	return 0;
}

int icCexpressionListElem::out(void) 
{
//	LOGIT(COUT_LOG,"%23s:\n\n",attname);
	if (expr)
	{
		expr->out();
	}
	if (exprList)
	{
		exprList->out();
	}

	return 0;
}


void asCdefaultValuesList::destroy(void) 
{
	for (unsigned i=0; i<refs.size(); i++)
	{
		refs[i]->destroy();
		values[i]->destroy();

		delete refs[i];
		delete values[i];

		refs[i]  = NULL;
		values[i]= NULL;
	}

	refs.clear();
	values.clear();
}

void icCexpressionList::destroy(void) 
{
	for (unsigned i=0; i<exprElemList.size(); i++)
	{
#ifdef _DEBUG
		assert( exprElemList[i] != NULL);
#endif
		exprElemList[i]->destroy();
		delete exprElemList[i];
		exprElemList[i] = NULL;
	}
	exprElemList.clear();
}

void icCexpressionListElem::destroy(void) 
{
	if (expr)
	{
		expr->destroy();
		delete expr;
		expr = NULL;
	}
	if (exprList)
	{
		exprList->destroy();
		delete exprList;
		exprList = NULL;
	}
}
 
void icCexpressionListElem::hydratePrototype(aExpressionListElement* elem)
{
	if (expr)
	{
		elem->pExpr = new aCexpression;
		expr->hydratePrototype(elem->pExpr);
	}
	
	if (exprList)
	{
		elem->pExprList = new aExpressionList; 
		exprList->hydratePrototype(elem->pExprList);
	}
	

	if (!expr && !exprList)
		LOGIT(CERR_LOG,"icCexpressionListElem with no content.\n");
}
