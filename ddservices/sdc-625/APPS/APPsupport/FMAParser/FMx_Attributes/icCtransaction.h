/*************************************************************************************************
 *
 * filename  icCtransaction.h
 *
 * 27 June 2013 - stevev
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Transaction Attribute class
 *
 */

#ifndef _icCtransaction_
#define _icCtransaction_

#include "..\FMx_Attribute.h"
#include "asCdataFieldSpec.h"
#include "asCrespCodesSpec.h"
#include "asCactionListSpec.h"

class icCtransaction: public FMx_Attribute
{

public: // construct 
	icCtransaction(int tag = 0) : FMx_Attribute(tag), transactionNumber(-1), pRequestPkt(NULL),
		pReplyPkt(NULL), pRespCodes(NULL), pActions(NULL)
	{	attname = "Transaction"; };
	virtual ~icCtransaction()
	{
		RAZE(pRequestPkt);
		RAZE(pReplyPkt);
		RAZE(pRespCodes);
		RAZE(pActions);  
	};
    
public: // data
	int transactionNumber;
	
	asCdataFieldSpec*  pRequestPkt;
	asCdataFieldSpec*  pReplyPkt;
	asCrespCodesSpec*  pRespCodes;// a asCrespCodesSpec8 || asCrespCodesSpecA
	asCactionListSpec* pActions;  // a asCactionListSpec8|| asCactionListSpecA

	
public: // methods
	virtual int  evaluateAttribute(UCHAR** pData, int& length, unsigned& offset)
	{
		return -1;
	};

	virtual void destroy(void) 
	{
		// TODO implement destroy
	};
    
	virtual int out(void) 
	{   // stevev 06jan16 -
		// Yes Virginia, we have to tell the entire story.	
		if (SUPPRESS_INTEGER)
		{
			LOGIT(COUT_LOG, "%23s: %u\n", "----Number",(unsigned short)transactionNumber);
		}
		else
		{
			LOGIT(COUT_LOG, "%23s: %u\n", "----Number",(unsigned)transactionNumber);
		}
		// 
		if (pRequestPkt)
		{
			LOGIT(COUT_LOG, "%23s: \n", "Request"); 
			pRequestPkt->out();
		}

		if (pReplyPkt)
		{
			LOGIT(COUT_LOG, "\n%23s: \n", "Reply");
			pReplyPkt->out();
		}
		// The rest of the story - stevev 06jan16
		// assume empty pointer if there are no items in the DD
		if (pRespCodes)
		{
			LOGIT(COUT_LOG, "\n");
			pRespCodes->out();
		}

		if (pActions)
		{
			LOGIT(COUT_LOG, "\n");
			pActions->out();
		}
		LOGIT(COUT_LOG, "\n");
		// end of story

		return SUCCESS;    
	};

	virtual void* generateElement();
};

#endif //_icCtransaction_




