/*************************************************************************************************
 *
 * filename  faCByteString.cpp
 *
 * 08 June 2013 - steveh
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *    
 * Implementation for Base Byte String Attribute
 *
 */

#include "logging.h"
#include "faCByteString.h"

int faCByteString::out(void)
{
	FMx_Attribute::out();
	LOGIT(COUT_LOG," ByteString Length: 0x%04x (%u)\n",len,len);
	LOGIT(COUT_LOG,"        ByteString: \n");

	if (len /*str.length()*/ < LOG_BUF_SIZE)
	{
		// avoid substr copying if possible
		LOGIT(COUT_LOG, "%s", pStore /*str.c_str()*/);
	}
	else
	{
		// carve byte string up into blocks sized to 90% of the 
		// buffer size and put them out 1 at a time
		string str((char*)pStore);
		int size = (int)(0.9 * LOG_BUF_SIZE);
		int n = str.length()/size + 1;

		for (int i = 0; i < n; i++)
		{
			int pos = i*size;
			int leng = size;
			if (i == n-1)
				leng = str.length() - pos;

			string s = str.substr(pos, leng);
			LOGIT(COUT_LOG, "%s", s.c_str());
		}
	}

	return 0;
}

aCattrBase* faCByteString::generatePrototype() 
{// as of today (21oct13) the faCByteString is only used for mrthod definitions
	aCattrMethodDef* prPtr =  new aCattrMethodDef;
	// for now, make a copy.  The parser memory may disappear.

	prPtr->pBlob   = new char[len+1];   
	prPtr->blobLen = len;
		
	if (pStore)//04jan16 - added null test for tokenizer issue
		strncpy(prPtr->pBlob, (char*)pStore,len+1);
	else
		DEBUGLOG(CLOG_LOG,"generating a prototype for a ByteString that has a null pointer.\n");

	prPtr->attr_mask = maskFromInt(attrType);

	return ((aCattrBase*)prPtr);
}