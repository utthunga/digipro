/*************************************************************************************************
 *
 * filename  asCexprSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Expression Specifier Attribute class
 *
 */

#ifndef _ASCEXPRSPEC_
#define _ASCEXPRSPEC_

#include "..\FMx_Attribute.h"


class asCexprSpec : public FMx_Attribute
{

public:
	asCexprSpec(int tag = 0) : FMx_Attribute(tag) 
	{
		attname = "Expression Specifier"; 
		expectedTag = tag;
	};
	
	virtual void hydratePrototype(aCondDestType* cond) = 0;
};

#endif //_ASCEXPRSPEC_
