/*************************************************************************************************
 *
 * filename  icCelements.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Element Attribute class
 *
 */

#ifndef _icCelements_
#define _icCelements_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\faCString.h"

class icCelement: public FMx_Attribute
{
public: // construct 
	icCelement() : index(-1), item(0), description(0), help(0) { attname = "Element"; };   // initialize underlying class data
	icCelement(const icCelement& elem) { operator=( elem );};
	icCelement& operator=(const icCelement& src){
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		index = src.index;
		if (src.item != NULL) item = new abCReference(*(src.item));
		if (src.description != NULL) description = new faCString(*(src.description));
		if (src.help != NULL) help = new faCString(*(src.help));
		member_name = src.member_name;
		return *this;  } ;
		virtual ~icCelement() { destroy(); };

public: // data
	unsigned index;        // whatever data is included in the underlying class
    abCReference *item;
	faCString *description;
	faCString *help;
	string     member_name;//empty for ref-array elements
    
public: // methods
	virtual void destroy(void) 
    {
 		if (item)
		{
			item->destroy();
			item = 0;
		}
		if (description)
		{
			description->destroy();
			description = 0;
		}
		if (help)
		{
			help->destroy();
			help = 0;
		}
   };
    
	virtual int out(void) 
    {
		FMx_Attribute::out();
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name
        LOGIT(COUT_LOG,"%23s: 0x%08x (%d)\n", "Index", index, index);

		if (item)
		{
			LOGIT(COUT_LOG,"%23s:\n","Item");  
			item->out();
		}
		if (description)
		{
			LOGIT(COUT_LOG,"%23s:\n","Description");  
			description->out();
		}
		if (help)
		{
			LOGIT(COUT_LOG,"%23s:\n","Help");  
			help->out();
		}

        return 0;
    };

	virtual void* generateElement(aCmemberElement* elem = NULL)
	{
		if ( elem == NULL )
		{
			elem = new aCmemberElement;
		}// else use the passed in item

		if (description != NULL)
		{
			elem->descS.enumVal = -1;
			description->hydratePrototype(&elem->descS);

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (description->strRefType == ds_Dict && elem->descS.enumVal == 0)
			{
				elem->descS.enumVal = -1;
			}
		}

		if (help != NULL)
		{
			elem->helpS.enumVal = -1;
			help->hydratePrototype(&elem->helpS);

			// api must match fm8 contents, -1 for invalid [4/21/2014 timj]
			if (help->strRefType == ds_Dict && elem->helpS.enumVal == 0)
			{
				elem->helpS.enumVal = -1;
			}
		}

		elem->locator = index;
		
		if (item != NULL)
		{
			item->hydratePrototype(&elem->ref);
		}

		return elem;
	};
};

#endif //_icCelements_




