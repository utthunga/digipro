/*************************************************************************************************
 *
 * filename  asCunknownType.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Unknown Type Attribute class
 *
 */

#ifndef _ASCUNKNOWNTYPE_
#define _ASCUNKNOWNTYPE_

#include "..\FMx_Attribute.h"

class asCunknownType : public FMx_Attribute
{
public:	// construct
	asCunknownType(int tag) : FMx_Attribute(tag) {attname = "Unknown Type";};

public:	// data

public:	// methods
	virtual int out(void)
	{
		FMx_Attribute::out();
		LOGIT(COUT_LOG,"%23s:\n\n",attname);
		return 0;
	};
	virtual bool inhibitOut() { return (reconcile) ? true : false; }
};

#endif //_ASCUNKNOWNTYPE_
