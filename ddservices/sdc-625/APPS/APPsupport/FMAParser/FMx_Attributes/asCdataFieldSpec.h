 /*************************************************************************************************
 *
 * filename  asCdataFieldSpec.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Data Field Attribute class
 *
 */

#ifndef _ASCDATAFIELDSPEC_
#define _ASCDATAFIELDSPEC_

#include "..\FMx_Attribute.h"

class asCdataFieldSpec : public FMx_Attribute
{
public:
	asCdataFieldSpec(int tag = 0) : FMx_Attribute(tag) {}

	virtual int out(void) 
	{   
		//unsigned i;

		LOGIT(COUT_LOG, "<asCdataFieldSpec>\n");
		//for (i=0; i<list.size())
		//{
		//	list[i]->out();
		//}

		return SUCCESS;    
	};


	virtual void hydrateConditional(aCcondList* cond) {};

	virtual aPayldType* generatePayload(void* pV = NULL) { return NULL;};
};

#endif //_ASCDATAFIELDSPEC_
