/*************************************************************************************************
 *
 * filename  icCgridMember.h
 *
 * 08 June 2013 - steveh
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2013, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Definition for Base Grid Member Attribute class
 *
 */

#ifndef _icCgridMember_
#define _icCgridMember_

#include "..\FMx_Attribute.h"
#include "..\FMx_Attributes_Ref&Expr.h"
#include "..\FMx_Attributes\faCString.h"


class icCgridMember: public FMx_Attribute
{
public: // construct 
	icCgridMember() : description(0) { attname = "Grid Member";};   // initialize underlying class data
    
public: // data
	faCString *description;
	vector<abCReference*> values;
	icCgridMember(const icCgridMember& src) { operator=(src); };
	virtual ~icCgridMember() { destroy(); }
	icCgridMember&	operator=(const icCgridMember& src) {
		FMx_Attribute::operator=(*((FMx_Attribute *)(&src)));
		if (src.description)
			description = new faCString(*src.description);	

		for (unsigned i=0; i < src.values.size(); i++)
		{
			abCReference *ref = new abCReference(*src.values[i]);
			values.push_back(ref);
		}
		return *this;
	};
    
public: // methods

	virtual void destroy(void) 
    {
        // code to destroy complex objects
		DESTROY(description);

		for (unsigned i = 0; i < values.size(); i++)
		{
			abCReference *ref = values[i];
			if (ref)
			{
				ref->destroy();
				delete ref;
			}
		}
     };
    
	virtual int out(void) 
    {
        LOGIT(COUT_LOG,"%23s:\n",attname);   // Attribute name

		if (description)
		{
			description->out();
		}

		for (unsigned i = 0; i < values.size(); i++)
		{
			abCReference *ref = values[i];

			if (ref)
			{
				ref->out();
			}
		}
        return 0;
    };

	virtual void generateElement(aCgridMemberElement  *elem)
	{
		if (elem == NULL)
		{
			elem = new aCgridMemberElement;
		}

		elem->header.enumVal = -1;
		description->hydratePrototype(&(elem->header));

		// api must match fm8 contents, -1 for invalid [06/05/2014 timj]
		if (description->strRefType == ds_DevSpec && elem->header.enumVal == 0)
		{
			elem->header.enumVal = -1;
		}

		for (vector<abCReference*>::iterator it = values.begin(); it != values.end(); ++it)
		{
			abCReference* r = *it;

			aCreference ddl_ref;
			r->hydratePrototype(&ddl_ref);
			elem->refLst.push_back(ddl_ref);
		}
	}
};

#endif //_icCgridMember_




