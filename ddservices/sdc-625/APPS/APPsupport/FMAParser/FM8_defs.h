/**********************************************************************************************
 *
 * $Workfile: FM8_defs.h $
 * 31aug11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_defs.h : holds definitions that define the FM8 encoded file format
 *
 * #include "FM8_defs.h"
 *
 */

#ifndef _FM8_DEFS_H
#define _FM8_DEFS_H

#ifdef INC_DEBUG
#pragma message("FM8_defs.h") 
#endif

#include "ddbdefs.h"		// we're using version 8 defines as the standard

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FM8_defs.h") 
#endif


/*********************************************************************************************
 * general definitions
 ********************************************************************************************/


/*********************************************************************************************
 * Object Types - AKA in the EFF as object-code
 *******************************************************************************************
typedef enum obj_type_e
{
	obj_Unknown,	// unknown object code
	obj_Description,// 1)holds the DDOD_ODES info
	obj_Device_Tbls,// 2)holds the device tables, 
	obj_Block_Tbls, // 3)holds the block tables, 
	obj_DD_Items    // 4)holds an actual DD item.  There will be many of these
}obj_type_t;
*/

/*********************************************************************************************
 * meta defs
 ********************************************************************************************/

#define DD_ODES_SIZE  41

#define DD_ODES_INDEX_OFFSET		0
#define	RAM_ROM_FLAG_OFFSET			2
#define NAME_LENGTH_OFFSET			3
#define ACCESS_PROT_FLAG_OFFSET		4
#define VERSION_OFFSET				5
#define LOC_ADDR_ODES_OFFSET		7
#define STOD_LENGTH_OFFSET			11
#define LOC_ADDR_STOD_OFFSET		13
#define SOD_FIRST_INDX_OFFSET		17
#define SOD_LENGTH_OFFSET			19
#define LOC_ADDR_SOD_OFFSET			21
#define DPOD_FIRST_INDX_OFFSET		25
#define DPOD_LENGTH_OFFSET			27
#define LOC_ADDR_DPOD_OFFSET		29
#define DVOD_FIRST_INDX_OFFSET		33
#define DVOD_LENGTH_OFFSET			35
#define LOC_ADDR_DVOD_OFFSET		37


#define MAX_SOD   0x7fff	/* max signed short...just in case */


#define V8_IDX_OFFSET			0	// size 2
#define V8_OBJ_CD_OFFSET		2	// size 1
#define V8_LENGTH_OFFSET		3	// size 2
#define V8_PW_OFFSET			5	// size 1
#define V8_AC_OFFSET			6	// size 1
#define V8_AR_OFFSET			7	// size 2
#define V8_HEAP_OFFSET_OFFSET	9	// size 4
#define V8_DS_OFFSET		   13	// size 1
#define V8_US_OFFSET		   14	// size 1
#define V8_CNTR_OFFSET		   15	// size 1
#define V8_EXT_SIZE_OFFSET	   16	// size 1

#define V8_FIXED_SIZE		   17

/*********************************************************************************************
 * 
 ********************************************************************************************/
#define INDEX_FORMAT		0x64 /*100 - format object */
#define INDEX_DEV_DIR       0x65 /*101 - device directory table set */
#define INDEX_BLK_DIR       0x66 /*102 - block directory table set  */


/*********************************************************************************************
 * STRUCTURE TAGS
 ********************************************************************************************/
#define STRUCT8_TAG_EXPRESSION	  86
#define STRUCT8_TAG_MEMBER        85
#define STRUCT8_TAG_MEMBER_SPEC   84

/********************************************************************************************/


/*********************************************************************************************
 * ITEM TYPES
 ********************************************************************************************/
// use char iType_8[ITEM_TYPE_STR_CNT][ITEM_TYPE_MAX_LEN] = ITEM_TYPE_STRINGS;
#define ITEM_TYPE_STR_CNT  38
#define ITEM_TYPE_MAX_LEN  14

#define ITEM_TYPE_STRINGS \
	{"Undefined   ",\
	"Variable    ",\
	"Command     ",\
	"Menu        ",\
	"Edit Disp   ",\
	"Method      ",\
	"Refresh Rel ",\
	"Unit Rel    ",\
	"Write As One",\
	"Ref Array   ",\
	"Collection  ",\
	"Non-HARTa   ",\
	"Block typeA ",\
	"Not-Used    ",\
	"Non-HARTb   ",\
	"Value Array ",\
	"Non-HARTc   ",\
	"Non-HARTd   ",\
	"Not-Used    ",\
	"Member      ",\
	"File        ",\
	"Chart       ",\
	"Graph       ",\
	"Axis        ",\
	"Waveform    ",\
	"Source      ",\
	"List        ",\
	"Grid        ",\
	"Image       ",\
	"Blob        ",\
	"Plugin      ",\
	"Template    ",\
	"Not-Used    ",\
	"Non-HARTe   ",\
	"Non-HARTf   ",\
	"Non-HARTg   ",\
	"Non-HARTh   ",\
	"Non-HARTi   "}
//                       
#define ITEM_TYPE_MAX (ITEM_TYPE_STR_CNT-1)

extern char iType_8[ITEM_TYPE_STR_CNT][ITEM_TYPE_MAX_LEN];

/*********************************************************************************************
 * Attribute Type
 ********************************************************************************************/
typedef enum attrGeneric_e
{
							////it_Undefined,	// 0
	ag_Var_class 			= ((int)VARIABLE_ITYPE) << 8,	//	[ 0] Class_Specifier,
	ag_Var_handling 		, // [ 1] Handling_Specifier,
	ag_Var_constant_units	, // [ 2] String_Specifier, 
	ag_Var_label			, // [ 3] String_Specifier,
	ag_Var_help 			, // [ 4] String_Specifier,
	ag_Var_width		 	, // [ 5] Expression_Specifier,
	ag_Var_height		 	, // [ 6] Expression_Specifier,
	ag_Var_validity			, // [ 7] Boolean_Specifier,
	ag_Var_pre_read_actions	, // [ 8] Reference_List_Specifier,
	ag_Var_post_read_actions, // [ 9] Reference_List_Specifier,
	ag_Var_pre_write_actions, // [10] Reference_List_Specifier,
	ag_Var_post_write_actions,// [11] Reference_List_Specifier,
	ag_Var_pre_edit_actions	, // [12] Reference_List_Specifier,
	ag_Var_post_edit_actions, // [13] Reference_List_Specifier,
	ag_Var_response_codes	, // [14] Unsupported,
	ag_Var_type 			, // [15] Type_Specifier,
	ag_Var_display_format	, // [16] Display_Format_Specifier,
	ag_Var_edit_format		, // [17] Edit_Format_Specifier,
	ag_Var_min_values		, // [18] Min_Value_Specifier,
	ag_Var_max_values		, // [19] Max_Value_Specifier,
	ag_Var_scaling_factor	, // [20] Scaling_Specifier,
	ag_Var_enumerations		, // [21] Enumerations_Specifier,
	ag_Var_array_name		, // [22] Array_Name_Specifier, 
	ag_Var_default_value	, // [23] Expression_Specifier,
    ag_Var_refresh_actions  , // [24] Reference_List_Specifier,
    ag_Var_item_information , // [25] Item_Information_Specifier,
    ag_Var_post_request_action,//[26] Reference_List_Specifier,
    ag_Var_post_user_action , // [27] Reference_List_Specifier,
    ag_Var_time_format      , // [28] Time_Format_Specifier,
    ag_Var_time_scale       , // [29] Time_Scale_Value
	ag_Var_visible			, // [30] 
	ag_Var_private			, // [31]

	ag_Cmd_number 			= ((int)COMMAND_ITYPE) << 8,	//[0] Command_Number_Specifier,
	ag_Cmd_operation, 		//	[1] Operation_Specifier,
	ag_Cmd_transaction, 	//	[2] Transaction_Specifier_List,
	ag_Cmd_response_codes,	//	[3] Response_Codes_Specifier,
	ag_Cmd_item_information ,// [4]

	ag_Menu_label			= ((int)MENU_ITYPE) << 8,//[0] String_Specifier,
	ag_Menu_items,			//	[1] Menu_Items_Specifier,
	ag_Menu_help,			//	[2] String_Specifier,
	ag_Menu_validity, 		//	[3] Boolean_Specifier,
	ag_Menu_style, 			//	[4] Style_Specifier
	ag_Menu_item_information,  //[5] DEBUG info
	ag_Menu_visible,           // [ 6]
	ag_Menu_post_read_actions, // [ 7] Reference_List_Specifier,
	ag_Menu_post_write_actions,// [ 8] Reference_List_Specifier,
	ag_Menu_pre_read_actions,  // [ 9] Reference_List_Specifier,
	ag_Menu_pre_write_actions, // [10] Reference_List_Specifier,


	ag_EdDisp_label			= ((int)EDIT_DISP_ITYPE) << 8,//[0] String_Specifier,
	ag_EdDisp_edit_items,			//[1] Reference_List_Specifier,
	ag_EdDisp_display_items,		//[2] Reference_List_Specifier,
	ag_EdDisp_pre_edit_actions,		//[3] Reference_List_Specifier,
	ag_EdDisp_post_edit_actions,	//[4] Reference_List_Specifier,
	ag_EdDisp_help,					//[5] String_Specifier,
	ag_EdDisp_validity, 			//[6] Boolean_Specifier,
	ag_EdDisp_item_information,
	ag_EdDisp_visible,

	ag_Meth_class			= ((int)METHOD_ITYPE) << 8,//[0] Class_Specifier,
	ag_Meth_label,					//[1] String_Specifier,
	ag_Meth_help,					//[2] String_Specifier,
	ag_Meth_definition,				//[3] BYTE STRING,
	ag_Meth_validity, 				//[4] Boolean_Specifier
	ag_Meth_scope,					//[5] scope
	ag_Meth_type,					//[6] Method_Type_Specifier
	ag_Meth_parameters,				//[7] Method_Parameters_Specifier
   ag_Meth_item_information,
   ag_Meth_visible,
   ag_Meth_private,
   

	ag_Refresh_list			= ((int)REFRESH_ITYPE) << 8,//[0] Refresh_Specifier 
	ag_Refresh_item_information,	// [1] REFRESH_DEBUG_ID
	ag_Refresh_max_id,				// [2] - remains for backward compatability
	ag_Refresh_updateList,			// [3] REFRESH_UPDATE_LIST_ID -new format, works with 4
	ag_Refresh_watchList,			// [4] REFRESH_WATCH_LIST_ID - new format, works with 3

	ag_UnitRel_list			= ((int)UNIT_ITYPE) << 8,// [0] Unit_Specifier 
	ag_Unit_item_information,		// [1] REFRESH_DEBUG_ID
	ag_Unit_max_id,					// [2] - remains for backward compatability
	ag_UnitRel_var,					// [3] UNIT_VAR_ID - new format - 4 is the update list
	ag_Unit_updateList,				// [4] UNIT_UPDATE_LIST_ID -new format, works with 3

	ag_WAO_list				= ((int)WAO_ITYPE) << 8,// [0] Reference_List_Specifier 
	ag_WAO_item_information,		// [1] WAO_DEBUG_ID

	ag_RefArr_elements		= ((int)ITEM_ARRAY_ITYPE) << 8,//	[0] Elements_Specifier,
	ag_RefArr_label,				//	[1] String_Specifier,
	ag_RefArr_help,					//	[2] String_Specifier,
	ag_RefArr_validity, 			//	[3] Boolean_Specifier
	ag_RefArr_item_information,
	ag_RefArr_private,

	ag_Coll_members			= ((int)COLLECTION_ITYPE) << 8,//[0] Members_Specifier,
	ag_Coll_label,					//[1] String_Specifier,
	ag_Coll_help,	            //[2] String_Specifier
	ag_Coll_validity,
	ag_Coll_item_information,
	ag_Coll_visible,
	ag_Coll_private,

	ag_Block_Char			= ((int)BLOCK_ITYPE) << 8,//characteristics	[ 0] Reference,
	ag_Block_label,					//[ 1] String_Specifier,
	ag_Block_help,					//[ 2] String_Specifier,
	ag_Block_parameters,			//[ 3] Members_Specifier,

	ag_ValArr_unused			= ((int)ARRAY_ITYPE) << 8,
	ag_ValArr_label,				//[1] String_Specifier, 
	ag_ValArr_help,					//[2] String_Specifier, 
	ag_ValArr_validity,				//[3] Boolean_Specifier
	ag_ValArr_type,					//[4] Reference,
	ag_ValArr_length,				//[5] INTEGER,
	ag_ValArr_item_information,	//[6] debug structure
	ag_ValArr_private,				//[7] boolean,

	ag_File_members			= ((int)FILE_ITYPE) << 8,//[0] Members_Specifier,
	ag_File_label,					//[1] String_Specifier,
	ag_File_help,					//[2] String_Specifier
	ag_File_unused,            // no validity
	ag_File_item_information,
	ag_File_handling,
	ag_File_update_actions,
	ag_File_identity,

	ag_Chart_label			= ((int)CHART_ITYPE) << 8,//[0] String_Specifier,
	ag_Chart_help,					//[1] String_Specifier,
	ag_Chart_validity, 				//[2] Boolean_Specifier,
	ag_Chart_height,				//[3] Size_Specifier,
	ag_Chart_width,					//[4] Size_Specifier,
	ag_Chart_type,					//[5] Chart_Type_Specifier,
	ag_Chart_length,				//[6] Expression_Specifier,
	ag_Chart_cycle_time,			//[7] Expression_Specifier,
	ag_Chart_members, 				//[8] Members_Specifier
	ag_Chart_item_information,
	ag_Chart_visible,

	ag_Graph_label			= ((int)GRAPH_ITYPE) << 8,//[0] String_Specifier,
	ag_Graph_help,					//[1] String_Specifier,
	ag_Graph_validity, 				//[2] Boolean_Specifier,
	ag_Graph_height,				//[3] Size_Specifier,
	ag_Graph_width,					//[4] Size_Specifier,
	ag_Graph_x_axis,				//[5] Reference_Specifier,
	ag_Graph_members, 				//[6] Members_Specifier
	ag_Graph_item_information,
	ag_Graph_cycle_time,
	ag_Graph_visible,

	ag_Axis_label			= ((int)AXIS_ITYPE) << 8,//[0] String_Specifier, 
	ag_Axis_help,					//[1] String_Specifier,
	ag_Axis_validity, 				//[2] Boolean_Specifier,
	ag_Axis_min_value,				//[3] Expression_Specifier,
	ag_Axis_max_value,				//[4] Expression_Specifier,
	ag_Axis_scaling,				//[5] Axis_Scaling_Specifier,
	ag_Axis_constant_units,			//[6] String_Specifier
	ag_Axis_item_information,
	ag_Axis_view_min_value,
	ag_Axis_view_max_value,

	ag_Wave_label			= ((int)WAVEFORM_ITYPE) << 8,//[ 0] String_Specifier,
	ag_Wave_help 					,//[ 1] String_Specifier, 
	ag_Wave_handling 				,//[ 2] Handling_Specifier,
	ag_Wave_emphasis				,//[ 3] Boolean_Specifier,
	ag_Wave_line_type				,//[ 4] Line_Type_Specifier,
	ag_Wave_line_color				,//[ 5] Expression_Specifier,
	ag_Wave_y_axis					,//[ 6] Reference_Specifier,
	ag_Wave_key_points_x_values		,//[ 7] Reference_List_Specifier,
	ag_Wave_key_points_y_values		,//[ 8] Reference_List_Specifier,
	ag_Wave_waveform_type			,//[ 9] Waveform_Type_Specifier,
	ag_Wave_x_values				,//[10] Reference_List_Specifier,
	ag_Wave_y_values				,//[11] Reference_List_Specifier,
	ag_Wave_x_initial				,//[12] Expression_Specifier,
	ag_Wave_x_increment				,//[13] Expression_Specifier,
	ag_Wave_number_of_points		,//[14] Expression_Specifier,
	ag_Wave_init_actions			,//[15] Reference_List_Specifier,
	ag_Wave_refresh_actions			,//[16] Reference_List_Specifier,
	ag_Wave_exit_actions			,//[17] Reference_List_Specifier,
	ag_Wave_item_information   ,
	ag_Wave_validity           ,

	ag_Source_label			= ((int)SOURCE_ITYPE) << 8,//[0] String_Specifier,
	ag_Source_help 					,//[1] String_Specifier,
	ag_Source_validity 				,//[2] Boolean_Specifier,
	ag_Source_emphasis				,//[3] Boolean_Specifier,
	ag_Source_line_type				,//[4] Line_Type_Specifier,	
	ag_Source_line_color			,//[5] Expression_Specifier,
	ag_Source_y_axis				,//[6] Reference_Specifier,
	ag_Source_members 				,//[7] Members_Specifier
	ag_Source_item_information,
	ag_Source_init_actions,
	ag_Source_refresh_actions,
	ag_Source_exit_actions,

	ag_List_label			= ((int)LIST_ITYPE) << 8,//[0] String_Specifier,
	ag_List_help 					,//[1] String_Specifier,
	ag_List_validity 				,//[2] Boolean_Specifier,
	ag_List_type					,//[3] Reference,
	ag_List_count					,//[4] Expression_Specifier,
	ag_List_capacity		 		,//[5] INTEGER
	ag_List_item_information   ,
	ag_List_first              ,
	ag_List_last               ,
	ag_List_private            ,

	ag_Grid_label			= ((int)GRID_ITYPE) << 8,//[0] String_Specifier,
	ag_Grid_help 					,//[1] String_Specifier,
	ag_Grid_validity 				,//[2] Boolean_Specifier,
	ag_Grid_height					,//[3] ,
	ag_Grid_width					,//[4] ,
	ag_Grid_orientation	 			,//[5] ,
	ag_Grid_handling				,//[6] ,
	ag_Grid_members					,//[7] ,
	ag_Grid_item_info				,//[8] Item_Information
	ag_Grid_visible            ,

	ag_Image_label			= ((int)IMAGE_ITYPE) << 8,//[0] String_Specifier,
	ag_Image_help 			 		,//[1] String_Specifier,
	ag_Image_validity  				,//[2] Boolean_Specifier,
	ag_Image_link             		,//[3] Reference_Specifier,     
	ag_Image_image_table_index		,//[4] Integer_Specifier,
	ag_Image_item_info				,//[5] Item_Information
	ag_Image_visible              ,

	ag_Blob_label			= ((int)BLOB_ITYPE) << 8,//[  0] String_Specifier,
	ag_Blob_help 					,//[  1] String_Specifier, 
	ag_Blob_handling				,//[  2] Handling_Specifier, 
	ag_Blob_identity				,//[  3] ASCII_String
	ag_Blob_item_info				,//[  4] Item_Information

	ag_Plugin_label			= ((int)PLUGIN_ITYPE) << 8,//[  0] String_Specifier,
	ag_Plugin_help 					,//[  1] String_Specifier, 
	ag_Plugin_validity  			,//[2] Boolean_Specifier,
	ag_Plugin_visible				,//[3] Boolean_Specifier,
	ag_Plugin_uuid					,//[4] universally_unique_identifier
	ag_Plugin_item_info				,

	ag_Temp_label			= ((int)TEMPLATE_ITYPE) << 8,//[  0] String_Specifier,
	ag_Temp_help 					,//[  1] String_Specifier,
	ag_Temp_validity           ,
	ag_Temp_default_values			,//[  2] Default_Values_List 
	ag_Temp_item_info				,//[  3] Item_Information, -- debug only


	ag_Last_Generic_Attr_Type

}/*typedef*/ attrGeneric_t;


#endif //_FMA_DEFS_H
