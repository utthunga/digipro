#ifndef _FMX_OBJECTSET_
#define _FMX_OBJECTSET_

#include "FMx_Base.h"

/*********************************************************************************************
 * FMx_objectSet
 ********************************************************************************************/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class is the base class for reading the various object data.
 *			It holds the common information and interface from both FM8 & FMA formats.
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMx_ObjectSet : public FMx_Base
{  
public:	// construct/destruct
	 FMx_ObjectSet(FMx* pTC, FILE* pF, bool sb, unsigned ct) : FMx_Base(pTC, pF,sb, ct) {};
	~FMx_ObjectSet(){};

public: // workers// workers - these need to be virtual
	virtual RETURNCODE index_ObjectData(unsigned& size) = 0;
};

#endif //_FMX_OBJECTSET_
