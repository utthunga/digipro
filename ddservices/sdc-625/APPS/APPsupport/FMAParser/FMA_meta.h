/**********************************************************************************************
 *
 * $Workfile: FMA_meta.h $
 * 21Jul11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_meta.h : FMA's basic OBJECT class description file
 *
 * #include "FMA_meta.h"
 *
 */

#ifndef _FMA_FMA_META_H
#define _FMA_FMA_META_H

#ifdef INC_DEBUG
#pragma message("In FMA_meta.h") 
#endif

#include "FMA_objectSet.h"
#include "FMA_object.h"
#include "FMx_meta.h"

#include <stdio.h>
#include <vector>

using namespace std;
#pragma warning (disable : 4786) 

#ifdef INC_DEBUG
#pragma message("    Finished Includes: FMA_meta.h") 
#endif

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class holds the meta data for an FMA file
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FMA_meta : public FMx_meta
{
public:	// construct/destruct
	FMA_meta(FMx* pTC, FILE* pF, bool sb) : FMx_meta(pTC, pF, sb, 0x81), pDefDesc(NULL), pDevTables(NULL), pBlockTable(NULL) {};
	virtual ~FMA_meta();

public:	// data
	FMA_Object*	pDefDesc;   
	FMA_Object*	pDevTables; 
	FMA_Object*	pBlockTable;// There is only one in HART

public:	// required
	virtual RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor);
	virtual RETURNCODE index_MetaData(unsigned& size);
	virtual unsigned get_item_count(void);
	virtual unsigned get_first_obj_index(void);
	virtual unsigned get_block_count(void);
};


#endif // _FMA_FMA_META_H
