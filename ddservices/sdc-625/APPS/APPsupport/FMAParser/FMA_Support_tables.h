/**********************************************************************************************
 *
 * $Workfile: FMA_Support_tables.h $
 * 07sep11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Support_tables.h : These are the content handlers
 *
 * #include "FMA_Support_tables.h"
 *
 */

#ifndef _FMA_SUPP_TBLS_H
#define _FMA_SUPP_TBLS_H


#include <vector>

using namespace std;

#include "FMA_Support_primatives.h"
#include "FMx_Table.h"
#include "FMx.h"

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  These classes hold the implementation 
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class FMA_Table;	// forward reference

// Row is the base class for all rows in all tables
// column type depends on the table type
// all rows are represented by columns containing varients
class FMA_Row : public FMx_Row<CValueVarient>
{
public:// construct/destruct
	FMA_Row(TableType_t  t);//:FMx_Row<CValueVarient>(t){};
	FMA_Row(const FMA_Row& s);//:FMx_Row<CValueVarient>(s.tt) {	FMA_Row::operator=(s); };
	virtual ~FMA_Row();

public:// workers
	virtual FMA_Row& operator=(const FMA_Row& s);
	virtual int parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize);
//	virtual int parse(UCHAR** pp_Location, INT& remainingSize) = 0;
	virtual int out(int rowNumber) = 0;

	static bool supported(TableType_t tt);// true if our row decoder is in place
};

class FMA_Table : public vector<FMA_Row*>, public FMx_Table
{
public:// location
	UCHAR*      pPayload;
	unsigned    payldLength;// entire table (type,len,payld)

public:// data
	TableType_t		tableType;
	unsigned		rowCount;
	unsigned	    rawByteLen;	// how long this table is (without type & length)
								// note that the offset is in deviceTblOffset[] of the handler
	unsigned        lenLen;		// how many bytes the table length took up

public:// construct/destruct
	FMA_Table();
	virtual
	~FMA_Table(){release();};

public:// workers
	virtual int parse(UCHAR** pp_Location, INT& remainingSize);
	virtual int out(unsigned offset);
	virtual int reconcileOut(unsigned offset);
	virtual void release();

	FMA_Row* newRow();// the row factory
	virtual int reconcileRowCount(void) { return rowCount; }
};

class FMA_ItemToCmdTable : public FMA_Table
{
public:
	FMA_ItemToCmdTable(){};

	virtual int reconcileOut(unsigned offset);
	virtual int reconcileRowCount(void);
};

extern FMA_Table* get_A_TablePtr(FMx* fmx, TableType_t t);// global declaration

#endif // _FMA_SUPP_TBLS_H