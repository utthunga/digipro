/**********************************************************************************************
 *
 * $Workfile: FMA_handlers.cpp $
 * 03nov11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_handler classes implementations
 *	
 */

#include <memory.h>
#include "logging.h"
#include "endian.h"

#include "FMA_Support_tables.h"
#include "FMA_handlers.h"
#include "FMA_Object.h"
#include "FMA_defs.h"
#include "FMA_Attributes_Ref&Expr.h"
#include "FMx_Support_primatives.h"
#include "FMA_Attributes\asCvarTypeA.h"
#include "FMA_Attributes\asCexprA.h"
#include "FMA_Attributes\asCboolSpecA.h"
#include "FMA_Attributes\asChandlingSpecA.h"
#include "FMA_Attributes\asCrefSpecA.h"
#include "FMA_Attributes\asCrefListSpecA.h"
#include "FMA_Attributes\asCchartTypeSpecA.h"
#include "FMx_Attributes\asCdisplaySize.h"
#include "FMA_Attributes\asCexprSpecA.h"
#include "FMA_Attributes\asCaxisScalSpecA.h"
#include "FMA_Attributes\asCorientSpecA.h"
#include "FMA_Attributes\asCstrSpecA.h"
#include "v_N_v+Debug.h"

extern void outHex( UCHAR* pS, unsigned offset, unsigned len );
extern unsigned currentItemSymbolID;

static FMA_Table* makeMeATable(TableType_t kind)
{// normally returns a new FMA_Table.  Exceptions are handled here

	FMA_Table * pRet = NULL;
	switch(kind)
	{
	case string_table_type:
	case dictionary_table_type:						
	case symbol_table_type: 						
	case item_info_table_type:					
	case command_number_table_type:						
		{
			pRet = (FMA_Table *)new FMA_Table;
		}
		break;

	case image_table_type:						
	case block_info_table_type:							
		{
			pRet = (FMA_Table *)new FMA_Table;
		}
		break;

	case item_to_cmd_table_type:
		{
			pRet = (FMA_Table *)new FMA_ItemToCmdTable;
		}
		break;

		/* * Block table types * */		
	case critical_item_table_type:				
	case block_item_table_type:					
	case block_item_name_table_type:				
	case parameter_table_type:		
	case relation_table_type:					
	case update_table_type:		
		{
			pRet = (FMA_Table *)new FMA_Table;
		}
		break;


	case item_RR_to_cmd_table_type:		// version 8 only		
	case reserved_future_device_2:
	case parameter_member_table_type: 			
	case parameter_member_name_table_type:		
	case parameter_element_table_type:			
	case parameter_list_table_type:				
	case parameter_list_member_table_type:		
	case parameter_list_member_name_table_type:	
	case characteristic_table_type:				
	case characteristic_member_name_table_type:	
	default:
		{// do nothing...return NULL
		}
		break;
	}// endswitch
	return pRet;
}


/* ==========================================================================================*/
FMA_Description_handler::FMA_Description_handler(FMA_Object* pOwner):
				FMx_handler((FMx_Base*)pOwner),firstItemIndex(0),item_count(0),block_count(0)
{
}

FMA_Description_handler::~FMA_Description_handler()
{// nothing to do here
}

int
FMA_Description_handler::evaluateExtension()// breaks up the extension part
{
	FMA_Object* pOwner = (FMA_Object*)pBObj;
	if (pOwner == NULL || pOwner->pTopClass == NULL)
			return FAILURE;
	
	pOwner->itemIndex      = 1;
	// parse the description information
	if (pOwner->obj_Payload != 0 && pOwner->obj_type == obj_Description)
	{
		item_count     
			= REVERSE_L(*((unsigned int*  )&(pOwner->obj_Payload[DESC_ITMCNT_OFFSET])));
		block_count    
			= REVERSE_S(*((unsigned short*)&(pOwner->obj_Payload[DESC_BLKCNT_OFFSET])));
		firstItemIndex 
			= REVERSE_L(*((unsigned int*  )&(pOwner->obj_Payload[DESC_FRSTITMIDX_OFFSET])));
		pOwner->pTopClass->idx = firstItemIndex;// the others know their number.

		return SUCCESS;
	}
	else
	{// empty meta data
		return FAILURE;
	}
}


int FMA_Description_handler::out (void) // extension data to stdout
{// hex dump
	FMA_Object* pOwner = (FMA_Object*)pBObj;
	if (pOwner == NULL)
	{		return -1;
	}
	UCHAR* pData = pOwner->obj_Payload;
	unsigned off = pOwner->object_offset + OBJ_FIXED_SIZE;

	if ( pData )
	{
		outHex(pData, off, pOwner->obj_Length );

		if (reconcile)
		{
		}
		else
		{
			LOGIT(COUT_LOG,"     First Item's Object Index: %4u (0x%04x)\n",
																	firstItemIndex,firstItemIndex);
			LOGIT(COUT_LOG,"    Number of Items in this DD: %4u (0x%04x)\n",item_count,item_count);
			LOGIT(COUT_LOG,"            Number of 'Blocks': %4u \n", block_count);
		}
	}
	return 0;// success
}



/* ==========================================================================================*/

FMA_DevTables_handler::FMA_DevTables_handler(FMA_Object* pOwner):FMx_handler((FMx_Base*)pOwner)
{
	deviceTableCount = 0;  dtcOffset = 0; dtcLength = 0; pTblCnt = NULL;
	memset(&(deviceTables[0]),   0, DEV_TBL_CNT * sizeof(FMA_Table*));
	memset(&(deviceTblOffset[0]),0, DEV_TBL_CNT * sizeof(unsigned)  );
}

FMA_DevTables_handler::~FMA_DevTables_handler()
{
	for (int i = 0; i < DEV_TBL_CNT; i++)
	{
		if (deviceTables[i])
		{
			delete deviceTables[i];
			deviceTables[i] = NULL;
		}
	}
}


// new 1-dec-11 -timj
int FMA_DevTables_handler::deviceTablesCount()
{
	int count = 0;
	for (int i=0; i<DEV_TBL_CNT; i++)
	{
		if (deviceTables[i])
		{
			count++;
		}
	}

	return count;
}

int
FMA_DevTables_handler::evaluateExtension()// breaks up the extension part
{
	UINT   tableCount;
	UINT   offset;
	UCHAR *p_PayloadLoc  = NULL;
	int    remainingSize = 0;
	int status = SUCCESS;
	TableType_t tableType = (TableType_t) (update_table_type+1);

	FMA_Object *obj = (FMA_Object *) pBObj;

	obj->itemIndex      = 2;

	if (deviceTablesCount() > 0)
		return SUCCESS; // no need to do this twice

	if (obj->obj_Payload == NULL)// somebody failed to read payload before we got here
	{
		LOGIT(CERR_LOG, "Error: Device tables don't have a payload.\n");
		return FMx_OPPFAIL;
	}
	remainingSize = obj->obj_Length;
	p_PayloadLoc  = obj->obj_Payload;
	offset        = obj->objA_payload_offset();// was object_offset
											 //from beginning of the file to first payload byte

	// table count first
	pTblCnt = p_PayloadLoc;
	deviceTableCount = tableCount = *p_PayloadLoc++;// straight up 8 bit int
	dtcOffset = offset++; 
	dtcLength = 1;
	remainingSize--;
	
	DEBUGLOG(CLOG_LOG,"    Device Table Count: %d    remaining:  %7d    offset:  %#06x\n",
					tableCount,remainingSize, offset);
	// then each table
	while (remainingSize > 0 && tableCount > 0) // while we have tables left to evaluate
	{	// read a table object
		
		tableType = (TableType_t)*p_PayloadLoc;// peek the table type
		FMA_Table *table = (FMA_Table *) makeMeATable(tableType);
		if ( ! table)
		{
			status = FMx_MEMFAIL;
			DEBUGLOG(CLOG_LOG,"    Device Table Count:%d  remaining:%d - mem failed\n",
						tableCount,remainingSize);
			break;
		}
		if (status = table->parse(&p_PayloadLoc, remainingSize)) // offset returned
		{
DEBUGLOG(CLOG_LOG,"    Parsed Device Table:%d  remaining:%d - parse failed\n",tableCount,remainingSize);
			break;	// status is set, just leave
		}

		if (table->tableType > update_table_type)
		{
DEBUGLOG(CLOG_LOG,"    Parsed Device Table:%d  remaining:%d - type read failed\n",tableCount,remainingSize);
			status = FMx_READFAIL;
			break;
		}
		deviceTables[table->tableType]    = table;
		deviceTblOffset[table->tableType] = offset;// starting offset
		
		DEBUGLOG(CLOG_LOG,"    Parsed Device Table:%d  remaining now:%7d offset now:%#06x\n",
	  tableCount,remainingSize, offset);
		// get ready for the next one
		offset  = offset + table->rawByteLen + table->lenLen +      1;
		//                 payload length   table length's length   table type's length
		
		tableCount--;
	}// wend more tables to do


	return status;
}


int 
FMA_DevTables_handler::findOffset(unsigned offset)
{
	int r = -1;
	
	for (int i = 0; i < DEV_TBL_CNT; i++)
	{
		if (deviceTblOffset[i] == offset)
		{
			r = i;
			break; //out of for loop
		}
	}
	return r;
}

// set showRandom=true to display a random # if table not found
void  FMA_DevTables_handler::out5 (TableType_e tableType, bool showRandom) // reconcile operation
{  
	bool found = false;
	for (int i = 0; i < DEV_TBL_CNT; i++)
	{
		if (deviceTables[i]  &&  deviceTables[i]->tableType == tableType)
		{
			found = true;
			if (reconcile)
			{
				if (tableType == item_to_cmd_table_type)
				{
					if (deviceTables[i] && deviceTables[i]->size()>0)
					{
						LOGIT(COUT_LOG,"%23s:\n", "PTOC table found");				// use this line for 8vA [7/1/2015 timj]
					}
				}
			}
			else
			{
				deviceTables[i]->reconcileOut(deviceTblOffset[i]);
			}
		}
	}

	if (!found)
	{
		if (!reconcile)
		{
			LOGIT(COUT_LOG,"%23s: not found %d\n", tableTypeStrings[tableType], rand());
		}
	}
}

int  FMA_DevTables_handler::out (void) // extension data to stdout
{                          
	int i;
	
	if (SUPPRESS_TABLES)
	{
		return 0;
	}	
	
	outHex(pTblCnt, dtcOffset, dtcLength );// hex of the beginning

	
	if (reconcile  ||  maintenance)
	{
		// print summary line for certain tables in reconcile mode

		//if (reconcile==false)
		//{
		out5(string_table_type,!reconcile);
		out5(dictionary_table_type);
		out5(image_table_type, false);
		out5(item_info_table_type);
		out5(item_to_cmd_table_type, !reconcile);
//		}

		// always print contents for string & dict tables in reconcile mode
		for ( i = 0; i < DEV_TBL_CNT; i++)
		{
			if (deviceTables[i])
			{
				if (deviceTables[i]->tableType == string_table_type || 
					deviceTables[i]->tableType == dictionary_table_type)
				{
					deviceTables[i]->out(deviceTblOffset[i]);
				}	
			}
		}
	
	} 
	else		// !reconcile
	{
		DEBUGLOG(COUT_LOG,"    Device Table Count: %4u (0x%02x)\n",deviceTableCount,deviceTableCount);

		for ( i = 0; i < DEV_TBL_CNT; i++)
		{
			if (deviceTables[i])
			{
					// print table contents
					deviceTables[i]->out(deviceTblOffset[i]);
			}
		}

	}	return 0;// true - handled the entire payload
}

void FMA_DevTables_handler::out3(void)  // class data to stdout
{
}

/* ==========================================================================================*/

FMA_BlkTables_handler::FMA_BlkTables_handler(FMA_Object* pOwner):FMx_handler((FMx_Base*)pOwner)
{
	blockTableCount = 0; btcOffset = 0; btcLength = 0;  pTblCnt = NULL;
	memset(&(block_Tables[0]),  0, BLK_DIR_TBL_CNT_A * sizeof(FMA_Table*));
	memset(&(blockTblOffset[0]),0, BLK_DIR_TBL_CNT_A * sizeof(unsigned)  );
}
FMA_BlkTables_handler::~FMA_BlkTables_handler()
{
	for (int i = 0; i < BLK_DIR_TBL_CNT_A; i++)
	{
		if (block_Tables[i])
		{
			delete(block_Tables[i]);
			block_Tables[i] = NULL;
		}
	}
}

// new 10-9-12 - stevev from timj
int FMA_BlkTables_handler::blockTablesCount()
{
	int count = 0;
	for (int i=0; i<BLK_DIR_TBL_CNT_A; i++)
	{
		if (block_Tables[i])
		{
			count++;
		}
	}

	return count;
}

int
FMA_BlkTables_handler::evaluateExtension()// breaks up the extension part
{
	UINT   tableCount;
	UINT   offset;
	UCHAR *p_PayloadLoc  = NULL;
	int    remainingSize = 0;
	int status = SUCCESS;
	TableType_t tableType = (TableType_t) (update_table_type+1);

	FMA_Object* pOwner = (FMA_Object*)pBObj;
	pOwner->itemIndex  = 3;

	if (pOwner == NULL || pOwner->pTopClass == NULL)
			return FAILURE;

	pOwner->itemIndex      = 3;


	if (blockTablesCount() > 0)
		return SUCCESS; // no need to do this twice

	if (pOwner->obj_Payload == NULL)// somebody failed to read payload before we got here
	{
		LOGIT(CERR_LOG, "Error: Block tables don't have a payload.\n");
		return FMx_OPPFAIL;
	}
	remainingSize = pOwner->obj_Length;
	p_PayloadLoc  = pOwner->obj_Payload;
	offset        = pOwner->objA_payload_offset();// from beginning of the file to first payload byte

	// table count first
	pTblCnt = p_PayloadLoc;
	blockTableCount = tableCount = *p_PayloadLoc++;// straight up 8 bit int
	remainingSize--;
	btcLength = 1;
	btcOffset = offset++;
	
	DEBUGLOG(CLOG_LOG,"    Block Table Count:%d -remaining:%#04x-offset:%#04x\n",
						tableCount,remainingSize, offset);
	// then each table
	while (remainingSize > 0 && tableCount > 0) // while we have tables left to evaluate
	{	// read a table object	
		tableType = (TableType_t)*p_PayloadLoc;// peek the table type
		FMA_Table *table = (FMA_Table *) makeMeATable(tableType);
		if ( ! table)
		{
			status = FMx_MEMFAIL;
			DEBUGLOG(CLOG_LOG,"    Block Table Count:%d  remaining:%#04x - mem failed\n",
				  tableCount,remainingSize);
			break;
		}
		if (status = table->parse(&p_PayloadLoc, remainingSize)) 
		{
			DEBUGLOG(CLOG_LOG,"    Block Table Count:%d  remaining:%#04x - parse failed\n",
				  tableCount,remainingSize);
			break;	// status is set, just leave
		}

		if (table->tableType >update_table_type || table->tableType< critical_item_table_type)
		{
			DEBUGLOG(CLOG_LOG,"    Block Table Count:%d  remaining:%#04x - type read failed\n",
						tableCount,remainingSize);
			status = FMx_READFAIL;
			// the parser has setup for the next table.....break;
		}
		block_Tables  [table->tableType] = table;
		blockTblOffset[table->tableType] = offset;
		
		DEBUGLOG(CLOG_LOG,"    Block Table Count:%d  remaining:%#04x offset = %#04x\n",
			  tableCount,remainingSize, offset);

		// get ready for the next one
		offset  = offset + table->rawByteLen + table->lenLen +      1;
		//                 payload length   table length's length   table type's length
		
		tableCount--;
	}// wend each table

	return 0;// success
}

int 
FMA_BlkTables_handler::findOffset(unsigned offset)
{
	int r = -1;
	
	for (int i = 0; i < BLK_DIR_TBL_CNT_A; i++)
	{
		if (blockTblOffset[i] == offset)
		{
			r = i;
			break; //out of for loop
		}
	}
	return r;
}

// set showRandom=true to display a random # if table not found
void FMA_BlkTables_handler::out5 (TableType_e tableType, bool showRandom) // reconcile operation
{  
	bool found = false;
	for (int i = 0; i < BLK_DIR_TBL_CNT_A; i++)
	{
		if (block_Tables[i]  &&  block_Tables[i]->tableType == tableType)
		{
			found = true;
			block_Tables[i]->reconcileOut(blockTblOffset[i]);
		}
	}

	if (!found)
	{
		if (!reconcile)
		{
			LOGIT(COUT_LOG,"%23s: not found %d\n", tableTypeStrings[tableType], rand());
		}
		//else
		//{
		//	// there is often no image table
		//	LOGIT(COUT_LOG,"%23s: (rows:0x%04x)\n", tableTypeStrings[tableType], 0);
		//}
	}
}

int FMA_BlkTables_handler::out (void) // extension data to stdout
{  
	int i;

	if (SUPPRESS_TABLES)
	{
		return 0;
	}

	if (reconcile || maintenance)
	{
		out5(critical_item_table_type, false);

		for ( i = DEV_TBL_CNT+2; i < BLK_DIR_TBL_CNT_A; i++)
		{
			if (block_Tables[i] != NULL)
			{
				if (block_Tables[i]->tableType == critical_item_table_type)
				{
					block_Tables[i]->out(blockTblOffset[i]);
				}
			}
		}
		return 0;// true - handled the entire payload
	}
	
	outHex(pTblCnt, btcOffset, btcLength );// hex of the beginning
	DEBUGLOG(COUT_LOG,"    Block Table Count: %4u (0x%02x)\n",blockTableCount,blockTableCount);
	
	for ( i = DEV_TBL_CNT+2; i < BLK_DIR_TBL_CNT_A; i++)
	{
		if (block_Tables[i] == NULL)
		{
			continue;//with next table
		}
		block_Tables[i]->out(blockTblOffset[i]);
	}
	return 0;// true - handled the entire payload
}

void FMA_BlkTables_handler::out3(void)  // class data to stdout
{
}

/* ==========================================================================================*/

FMA_Item_handler::FMA_Item_handler(FMA_Object* pOwner) : FMx_handler((FMx_Base*)pOwner)
{
	item_type   = item_subtype = 0;
	item_number = item_attr_cnt = symbolTableIndex = 0;
}

int FMA_Item_handler::evaluateExtension()// breaks up the extension part
{	
	int status = SUCCESS;

	int    remainingSize = 0;
	UCHAR *p_PayloadLoc  = NULL;
	UINT   offset;
	UINT64 tempLongLong;
	FMA_Object* pOwner = (FMA_Object*)pBObj;

	if (pOwner == NULL || pOwner->pTopClass == NULL)
			return FAILURE;

	pOwner->itemIndex = pOwner->pTopClass->idx++;

	if (pOwner->obj_Payload == NULL)// somebody failed to read payload before we got here
	{
		// log error
		return FMx_OPPFAIL;
	}

	remainingSize = pOwner->obj_Length;	  // extension byte count - extension w/o count
	p_PayloadLoc  = pOwner->obj_Payload;  // points to data after extension byte count
	offset        = pOwner->objA_payload_offset();//from file beginning to first payload byte

	// all data pointers have been adjusted past the fixed portion
	item_type     = *p_PayloadLoc; // straight up 1 byte integer	
	p_PayloadLoc++; remainingSize--; 	offset ++;
	
	item_subtype  = *p_PayloadLoc; // straight up 1 byte integer	
	p_PayloadLoc++; remainingSize--; 	offset ++;

	item_number   = REVERSE_L( *((unsigned int*  )(p_PayloadLoc)) );// unsigned 32
currentItemSymbolID = item_number;
	p_PayloadLoc += 4; remainingSize-=4;  offset += 4;

	status = parseInteger(8, &p_PayloadLoc, remainingSize, tempLongLong, offset);

	if (status != SUCCESS)
	{
		return status;
	}

	symbolTableIndex = (unsigned)tempLongLong;
	
	status = parseInteger(4, &p_PayloadLoc, remainingSize, tempLongLong, offset);

	if (status != SUCCESS)
	{
		return status;
	}

	assert(symbolTableIndex != 0xffffffff);		// symbol table index not assigned in the tokenizer
	item_attr_cnt = (unsigned)tempLongLong;

	length2firstAttr = offset - pOwner->objA_payload_offset();
	items_attributes.objectIndex = pOwner->obj_Index;// for identity

	status =
	items_attributes.evaluateAttributePayload(p_PayloadLoc, remainingSize,offset, item_type, item_attr_cnt);

	return status;
}

void FMA_Item_handler::generatePrototype()
{ 
	protoItem = new aCitemBase;

	protoItem->itemId = (ITEM_ID)item_number;
	protoItem->itemType = (CitemType)item_type;
	protoItem->itemSubType = (CitemType)item_subtype;
	protoItem->attrLst.clear();
	protoItem->attrMask = 0;
	int unused;
	ConvertToString::fetchSymbolNameNtype(item_number, protoItem->itemName, unused);

	switch (item_type)
	{
	case BLOCK_ITYPE:
		protoItem->itemName = "Block";
		if (QANDA)
		{
			protoItem->attrMask = 0x0;// stevev - coming out of v8 tok
		}
		return;

	case UNIT_ITYPE:
		generatePrototypeUnitRel();
		break;

	case REFRESH_ITYPE:
		generatePrototypeRefreshRel();
		break;

	default:
		generatePrototypeAllOther();
		break;
	}
#ifndef TOKENIZER
	generatePrototypeDefaultAttrs(items_attributes);
#endif
	patchPrototypeAttrs(protoItem);
}

// case : UNIT_ITYPE
void FMA_Item_handler::generatePrototypeUnitRel()
{
#ifndef TOKENIZER
	aCattrUnititems *pUnitAttr =  new aCattrUnititems;

	pUnitAttr->condVar.clear();
	pUnitAttr->condListOUnitLists.genericlist.clear();
	pUnitAttr->attr_mask = maskFromInt(ag_UnitRel_list);

	for (fmaAttrIT at = items_attributes.begin(); at != items_attributes.end(); at++)
	{
		FMx_Attribute* pObj = (FMx_Attribute*)(*at);

		if (pObj->attrType == ag_UnitRel_var)
		{
			asCrefSpecA *p = (asCrefSpecA *)pObj;
			p->conditional.hydratePrototype(&pUnitAttr->condVar);
			p->isConditional = p->conditional.isConditional();
		}
		else if (pObj->attrType == ag_Unit_updateList)
		{
			asCrefListSpecA *p = (asCrefListSpecA*) pObj;
			//p->conditional.coalescePayload = COALESCEPAYLOAD;
			p->conditional.hydratePrototype(&pUnitAttr->condListOUnitLists);
			p->isConditional = p->conditional.isConditional();
		}
		else
		{
			// any other attrs
			aCattrBase*   ptype = pObj->generatePrototype();
			protoItem->attrMask |= ptype->attr_mask;
			protoItem->attrLst.push_back(ptype);
		}
	}

	protoItem->attrMask |= maskFromInt(ag_UnitRel_list);
	protoItem->attrLst.push_back(pUnitAttr);
#else // TOKENIZER encodes it like fma

//	unitAttr_Var, a aCattrCondReference	unitAttr_Var
//	unitAttr_Update aCattrReferenceList	unitAttr_Update
	

	aCattrCondReference *pUnitVar = new aCattrCondReference;
	aCattrReferenceList *pUnitLst = new aCattrReferenceList;

	pUnitVar->attr_mask = maskFromInt(unitAttr_Var);
	pUnitLst->attr_mask = maskFromInt(unitAttr_Update);

	for (fmaAttrIT at = items_attributes.begin(); at != items_attributes.end(); at++)
	{
		FMx_Attribute* pObj = (FMx_Attribute*)(*at);

		if (pObj->attrType == ag_UnitRel_var)
		{
			asCrefSpecA *p = (asCrefSpecA *)pObj;
			p->conditional.hydratePrototype(&pUnitVar->condRef);
			p->isConditional = p->conditional.isConditional();
		}
		else if (pObj->attrType == ag_Unit_updateList)
		{
			asCrefListSpecA *p = (asCrefListSpecA*) pObj;
			//p->conditional.coalescePayload = COALESCEPAYLOAD;
			p->conditional.hydratePrototype(&pUnitLst->RefList);
			p->isConditional = p->conditional.isConditional();
		}
		else
		{
			// any other attrs
			aCattrBase*   ptype = pObj->generatePrototype();
			protoItem->attrMask |= ptype->attr_mask;
			protoItem->attrLst.push_back(ptype);
		}
	}

	protoItem->attrMask |= maskFromInt(ag_UnitRel_var);
	protoItem->attrMask |= maskFromInt(ag_Unit_updateList);
	protoItem->attrLst.push_back(pUnitVar);
	protoItem->attrLst.push_back(pUnitLst);
#endif
}

void FMA_Item_handler::generatePrototypeRefreshRel()
{
	aCattrRefreshitems *pRefreshAttr =  new aCattrRefreshitems;

	pRefreshAttr->condWatchList.clear();
	pRefreshAttr->condUpdateList.genericlist.clear();
	pRefreshAttr->attr_mask = maskFromInt(ag_Refresh_list);

	for (fmaAttrIT at = items_attributes.begin(); at != items_attributes.end(); at++)
	{
		FMx_Attribute* pObj = (FMx_Attribute*)(*at);

		if (pObj->attrType == ag_Refresh_watchList)
		{
			asCrefListSpecA *p = (asCrefListSpecA*) pObj;
			//p->conditional.coalescePayload = COALESCEPAYLOAD;
			p->conditional.hydratePrototype(&pRefreshAttr->condWatchList);
			p->isConditional = p->conditional.isConditional();
		}
		else if (pObj->attrType == ag_Refresh_updateList)
		{
			asCrefListSpecA *p = (asCrefListSpecA*) pObj;
			//p->conditional.coalescePayload = COALESCEPAYLOAD;
			p->conditional.hydratePrototype(&pRefreshAttr->condUpdateList);
			p->isConditional = p->conditional.isConditional();
		}
		else
		{
			// any other attrs
			aCattrBase*   ptype = pObj->generatePrototype();
			protoItem->attrMask |= ptype->attr_mask;
			protoItem->attrLst.push_back(ptype);
		}
	}

	protoItem->attrMask |= maskFromInt(ag_Refresh_list);
	protoItem->attrLst.push_back(pRefreshAttr);
}

void FMA_Item_handler::generatePrototypeAllOther()
{
	// We need to use the variable's size to set the overall size
	unsigned targetType = (iT_Variable << 8) | varAttrTypeSize;
	
	for (fmaAttrIT at = items_attributes.begin(); at != items_attributes.end(); at++)
	{
		FMx_Attribute* pObj = (FMx_Attribute*)(*at);
		aCattrBase*   ptype = NULL;

		if ( pObj->attrType != ag_ValArr_length )// the normal path
		{
currentItemSymbolID = item_number;
			ptype = pObj->generatePrototype();
			if (pObj->isConditional)
			{
				protoItem->isConditional = true;
			}
		}
		else 
		{// special case of value array's number-of-elements encoded as an expression
			// but ptototyped as a conditiona long
			asCintegerConst *pic = ((asCexprA *)pObj)->asIntegerConst();
			if (pic)
			{
				ptype = pic->generatePrototype();
				delete pic;
			}// else stays null
		}

		if (ptype != NULL)
		{
			if ( pObj->attrType == ag_Axis_min_value || pObj->attrType == ag_Axis_max_value)
			{
				// this case only, pick the aCattrCondExpr out of the minmax list structure and use it
				aCattrVarMinMaxList *mmlist = (aCattrVarMinMaxList*)ptype;
				aCattrCondExpr *x = new aCattrCondExpr;
				x->condExpr = mmlist->ListOminMaxElements[0].condExpr;
				x->attr_mask = ptype->attr_mask;
				delete ptype;
				ptype = x;
			}
			if ( pObj->attrType == targetType )
			{
				protoItem->itemSize = ((asCvarTypeA*)pObj)->varSize;
			}

			protoItem->attrMask |= ptype->attr_mask;
			protoItem->isConditional = pObj->isConditional ? true : protoItem->isConditional;
			protoItem->attrLst.push_back(ptype);
		}
	}
}

void FMA_Item_handler::generatePrototypeDefaultAttrs(vector <FMx_Attribute *> &attrlist)
{

	// fma doesn't have an item-info for everything but the sdc expects one
	aCdebugInfo* pInfo = new aCdebugInfo;
	int dummy_param;
	if (pInfo != NULL)
	{
		ConvertToString::fetchSymbolNameNtype(item_number, pInfo->symbolName, dummy_param);
		protoItem->itemName = pInfo->symbolName;
		unsigned attrType = conversionAto8(map_AttrType,item_type, 0);
		pInfo->attr_mask = maskFromInt( (attrType & 0xff) );// we don't want the item type
		protoItem->attrMask |= pInfo->attr_mask;
		protoItem->attrLst.push_back(pInfo);
	}

	FMx_handler::generatePrototypeDefaultAttrs(attrlist);
}


int FMA_Item_handler::findOffset(unsigned offset)
{
	return -1; // not used here
}

int FMA_Item_handler::out(void) // extension data to stdout
{
	// debugging usage only
	extern unsigned currentItemSymbolID;
	currentItemSymbolID = item_number;

	FMA_Object* pOwner = (FMA_Object*)pBObj;
	UCHAR* pData = pOwner->obj_Payload;
	unsigned off = pOwner->object_offset + OBJ_FIXED_SIZE;
	outHex(pData, off, length2firstAttr );// hex of the beginning
	
	LOGIT(COUT_LOG,"         Item Type: 0x%02x %s\n",(int)item_type,itemType_Str[item_type]);
	LOGIT(COUT_LOG,"      Item SubType: 0x%02x %s\n",
												(int)item_subtype,itemType_Str[item_subtype]);
	LOGIT(COUT_LOG,"    Item Symbol ID: 0x%04x (%u)\n",item_number,item_number);
	if (!reconcile)
	{
	LOGIT(COUT_LOG,"Symbol Table Index: 0x%04x (%u)\n\n",symbolTableIndex,symbolTableIndex);
	}
	//LOGIT(COUT_LOG,"   Item's Attr Cnt: 0x%04x (%u)\n\n",item_attr_cnt,item_attr_cnt);
	// attribute list
	items_attributes.out();

// for now, just dump hex for attributes
//	outHex( pData+length2firstAttr, off+length2firstAttr, pOwner->obj_Length-length2firstAttr);

	return 0;// handled
}

void FMA_Item_handler::out3(void)  // class data to stdout
{//not
}

// apply any "patches" to the API attr object
void FMA_Item_handler::patchPrototypeAttrs( aCitemBase* acitem )
{
	for ( std::vector<aCattrBase*>::iterator iT = acitem->attrLst.begin(); iT < acitem->attrLst.end(); iT++)
	{ 
		aCattrBase* pAttr = (*iT);
		if ( (&pAttr != NULL) && (pAttr != NULL) )
		{
			if (protoItem->itemType == METHOD_ITYPE)
			{
				if (pAttr->attr_mask & METHOD_TYPE)
				{
					aCparameter* pp = (aCparameter*) pAttr;
					int unused;
					ConvertToString::fetchSymbolNameNtype(item_number, pp->paramName, unused);
				}
			}
		}
	}  // FOR_iT
}


