/**********************************************************************************************
 *
 * $Workfile: FM8_Objects.h $
 * 09aug11 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Objects.h : FMA's basic OBJECT class description file and its list
 *
 * #include "FM8_Objects.h"
 *
 */

#ifndef _FMA_FM8_OBJECTSET_H
#define _FMA_FM8_OBJECTSET_H

#include "FMx_objectSet.h"
#include "FM8_object.h"

using namespace std;
#pragma warning (disable : 4786)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  This class holds all the objects in an fm8 file
 *		
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class FM8_ObjectSet : public FMx_ObjectSet
{
public:	// construct/destruct
	 FM8_ObjectSet(FMx* pTC, FILE* pF, bool sb):FMx_ObjectSet(pTC, pF, sb, 0x43) {};
	 virtual ~FM8_ObjectSet();

public:	// data
	FM8_pObjectList_t objList;

public: // workers
	virtual RETURNCODE index_ObjectData(unsigned& size);
	virtual	void out(unsigned idx = 0xffffffff, unsigned size = 0);

public:	// required
	RETURNCODE ExecuteVisitor(FMx_visitor* pVisitor);
};


#endif // _FMA_FM8_OBJECTSET_H
