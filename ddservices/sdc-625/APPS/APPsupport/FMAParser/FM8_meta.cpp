/**********************************************************************************************
 *
 * $Workfile: FM8_meta.cpp $
 * 11aug11 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		implementation for handling the FM8 meta data
 *		
 *
 */

#include "FM8_defs.h"

#include "FM8_meta.h"
#include "FM8_Object.h"
#include "FM8_handlers.h"

#include "logging.h"

FM8_meta::~FM8_meta()
{
	delete pDefDesc;
	delete pFormat;  
	delete pDevDir;  
	delete pBlockDir;
}

RETURNCODE FM8_meta::index_MetaData(unsigned& size)
{
	RETURNCODE r = -1;
#ifdef _DEBUG
	metaSize = 0;
#endif

	// reads odes, treat it like an object
	pDefDesc  = (FM8_ODES  *)   new  FM8_ODES(pTopClass, fp, saveBinary);
	pFormat   = (FM8_Object  *) new  FM8_Object(pTopClass, fp, saveBinary); 
	pDevDir   = (FM8_Object  *) new  FM8_Object(pTopClass, fp, saveBinary);
	pBlockDir = (FM8_Object  *) new  FM8_Object(pTopClass, fp, saveBinary); 

	if ( pDefDesc == NULL || pDevDir == NULL || pBlockDir == NULL )
	{
		if (pDefDesc   != NULL) delete pDefDesc;    pDefDesc = NULL;
		if (pFormat    != NULL) delete pFormat;     pFormat   = NULL;
		if (pDevDir    != NULL) delete pDevDir;  pDevDir = NULL;
		if (pBlockDir!= NULL) delete pBlockDir; pBlockDir = NULL;
		return FMx_MEMFAIL;
	}

	pDefDesc ->obj_type = obj_8_ODES;
	pFormat  ->obj_type = obj_8_Format;
	pDevDir  ->obj_type = obj_8_Dev_Dir;
	pBlockDir->obj_type = obj_8_Blk_Dir;

	unsigned dummySize = 2048;
	
	pDefDesc ->read_Object_Info(size);// handlers will do the rest
	// DefDesc isn't a real object
#ifdef _DEBUG
	metaSize    = pDefDesc->obj_size();
#endif

	if (isMetadataObject())
	{
		pFormat  ->read_Object_Info(size);//->read_Object();
		if ( pFormat->obj_size() )
			handledObjects++;
#ifdef _DEBUG
		metaSize   += pFormat->obj_size();
#endif
	}

	if (isMetadataObject())
	{
		pDevDir  ->read_Object_Info(size);//->read_Object();
		if ( pDevDir->obj_size() )
			handledObjects++;
#ifdef _DEBUG
		metaSize   += pDevDir->obj_size();
#endif
	}

	if (isMetadataObject())
	{
		pBlockDir->read_Object_Info(size);//->read_Object();
		if ( pBlockDir->obj_size() )
			handledObjects++;
#ifdef _DEBUG
		metaSize   += pBlockDir->obj_size();
#endif
	}

/*** to be moved elsewhere


	if (! saveBinary)
	{
		delete[] pDefDesc->obj_payload;
		pDefDesc->obj_payload = NULL;
		pDefDesc->obj_payload_size = 0;
	}
	return 0;
	***/
	return SUCCESS;
}

RETURNCODE FM8_meta::ExecuteVisitor( FMx_visitor* pVisitor )
{ 
	RETURNCODE r = -1;
	if (pDefDesc)
	{
		r = pDefDesc->ExecuteVisitor( pVisitor ); 
		if (r != SUCCESS) return r;
	}
	if (pFormat)
	{
		r = pFormat->ExecuteVisitor( pVisitor );
		if (r != SUCCESS) return r;
	}
	if (pDevDir)
	{
		r = pDevDir->ExecuteVisitor( pVisitor ); 
		if (r != SUCCESS) return r;
	}
	if (pBlockDir)
	{
		r = pBlockDir->ExecuteVisitor( pVisitor );
	}
	
	return r;
}

void FM8_meta::out(unsigned idx, unsigned size)
{
	LOGIT(COUT_LOG, "DDOD Object Description -- unsupported\n");
	LOGIT(COUT_LOG,
	"=================================================================================\n");
} 

unsigned  FM8_meta::get_item_count(void)
{
	if (pDefDesc == NULL || pDefDesc->pHandler == NULL)
		return 0;
	else
	{
		FM8_ODES_handler* pHndlr =(FM8_ODES_handler*)(pDefDesc->pHandler);
		return pHndlr->item_count - handledObjects;
	}
}

unsigned  FM8_meta::get_first_obj_index(void)
{
	if (pDefDesc == NULL || pDefDesc->pHandler == NULL)
		return 0;
	else
	{
		FM8_ODES_handler* pHndlr =(FM8_ODES_handler*)(pDefDesc->pHandler);
		return pHndlr->firstItemIndex;
	}
}

unsigned  FM8_meta::get_block_count(void)
{
	return 1;
}


// look ahead in the binary to get the object type, return
// true if the object matches any of the bits in the mask
// mask=0x80 matches obj_8_ODES, obj_8_Format, obj_8_Dev_Dir and obj_8_Blk_Dir
// mask=0x1f matches   DD item types
bool FM8_meta::checkObjectType(UCHAR mask)
{
	bool status = false;

	if (fp == 0 )
	{
		LOGIT(COUT_LOG, "Check to skip reading object type: invalid FILE pointer\n");
	}
	else
	{
		long offset = ftell(fp);
		int n = V8_FIXED_SIZE+1;
		UCHAR fixed[V8_FIXED_SIZE+1];	// allow to read the first byte of the extension

		if (n != fread(fixed, 1, n, fp))
		{
			LOGIT(COUT_LOG, "Check to skip reading object: read failed\n");
		}
	
		// rewind as read_Object_Info() will re-read these bytes
		if ( fseek(fp,-1*n,SEEK_CUR) )// zero on success
		{
			LOGIT(COUT_LOG, "Check to skip reading object: seek failed\n");
		}

		// we're now pointing to the first byte of object
		assert(offset == ftell(fp));

		uchar otype = fixed[n-1];
		status = (otype & mask) > 0;
	}

	return status;
}
