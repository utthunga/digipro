/**********************************************************************************************
 *
 * $Workfile: FMA_Support_rows.cpp $
 * 05oct12 - stevev
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA support classes for tables
 *		
 *
 */

#include "Convert.h"
#include "FMA_Support_Rows.h"
#include "FMA_ResolvedRef.h"
#include "FMA_defs.h"
#include "Endian.h"
#include "FMx_Support_primatives.h"
#include "v_N_v+Debug.h"


extern string filter2print(const wstring& src); //in FMx_Support_primatives.cpp

// ----------- template base class's pure virtual functions ---------------------------------


/* inserting the resolved reference code here instead of making a unique, separate file */
/****************************************************************************************/

int resolved_reference::parse(UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	UINT64 tempLongLong;

	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	// get the implicit tag
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	type = (int)tempLongLong;
	if ( type != RESOLVED_REF_ITEMID && type != RESOLVED_REF_TYPEID)
	{// illegal type or wrong tag
		LOGIT(COUT_LOG|CERR_LOG,"Error: tag for resolved reference is %d, not 0 or 48\n",type);
		return FAILURE;
	}
	//else we're good to go

	// then get the item ID or element identifier
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	whichItem = (unsigned)tempLongLong;

	if ( type == RESOLVED_REF_TYPEID)// ref
	{
		resRef = new resolved_reference;
		if (resRef)
		{
			status = resRef->parse(&pLoc, remaining);
		}
		else
		{
			status = FAILURE;
			LOGIT(COUT_LOG|CERR_LOG,"memory error, new resolved_reference failed.\n");
		}
	}
	// else we're done
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

// note that the top original caller has to put a newline on this puppy...
int resolved_reference::out(int rowNumber)
{
	string ls;
	int    lt;

	if ( type == RESOLVED_REF_TYPEID )
	{
		if (resRef == NULL)
		{
			LOGIT(CERR_LOG," Resolved Reference: Row: 0x%04x has a type of %d but a NULL ref ptr.\n",
				rowNumber, type);            
		}
		resRef->out(rowNumber);

		// it is possible that we have a collection of arrays ...
		ConvertToString::fetchSymbolNameNtype(whichItem, ls, lt);
		if (lt != MEMBER_ITYPE)
		{        
			LOGIT(COUT_LOG,"[0x%02x]", whichItem);
		}
		else
		{
			LOGIT(COUT_LOG,".%s(0x%x)", ls.c_str(), whichItem);
		}
	}
	else // type = symbol id
	{
		ConvertToString::fetchSymbolNameNtype(whichItem, ls, lt);
		LOGIT(COUT_LOG," Resolved Reference: SymbolID: 0x%04x", whichItem);
		if (!reconcile && !SUPPRESS_PTOCNAME )
		{
			LOGIT(COUT_LOG," (%s) ", ls.c_str());
		}
	}
	return SUCCESS; // for now
}

int resolved_reference::ptocReconcileOut(int rowNumber)
{
	string ls;
	int    lt;

	if ( type == RESOLVED_REF_TYPEID || resRef != NULL)
	{
		resRef->out(rowNumber);

		ConvertToString::fetchSymbolNameNtype(whichItem, ls, lt);
		if (lt != MEMBER_ITYPE)
		{		
			LOGIT(COUT_LOG," <0x%02x> ", whichItem);
		}
		else
		{
			LOGIT(COUT_LOG,".%s(x%x)", ls.c_str(), whichItem);
		}
	}
	else // type = symbol id
	{
		ConvertToString::fetchSymbolNameNtype(whichItem, ls, lt);
		LOGIT(COUT_LOG,"    SymbolID: 0x%04x\n", whichItem);
	}
	return SUCCESS; // for now
}

	// add the resolved_reference aCreference generator here...
/****************************************************************************************/

// only needed if data added	FMA_Row(const FMA_Row& s){ operator=(s);};
// only needed if data added	~FMA_Row(){};
// only needed if data added	FMA_Row& operator=(const FMA_Row& s);

/*** item_info_table_type  ************/
FMA_DevRow_ItemInfo::FMA_DevRow_ItemInfo(): FMA_Row(item_info_table_type)
{
}

int FMA_DevRow_ItemInfo::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}

	// get item-symbol-id
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get item-object-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get symbol-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get item-type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get item-offset -- unsigned 32

	tempLongLong = REVERSE_L( *((unsigned *)pLoc) ); 
	pLoc        += sizeof(unsigned);
	remaining   -= sizeof(unsigned);
	push_back(val = (unsigned)tempLongLong);

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_ItemInfo::out(int rowNumber)
{
	CValueVarient lV, mV, nV, oV, pV;
	int retval = SUCCESS;
	
	LOGIT(COUT_LOG,"%4d",rowNumber);
	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

// push order: item-symbol-id, item-object-index, symbol-index, item-type, item-offset
	int t = 0;
	assert( size() == 5 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++; // item symbol number
	mV = *vLIT++; // item object index
	nV = *vLIT++; // symbol table index
	oV = *vLIT++; // item type
		t = (unsigned)oV;
	pV = *vLIT++; // item offset
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of ItemInfo row is not at the end(). (%d)\n",size());
		retval = FAILURE;
	}
	else
	if ( t > FMAITMTYPECOUNT )
	{
		LOGIT(COUT_LOG|CERR_LOG," Item-Info table item type is invalid.(%d)\n",t);
		retval = FAILURE;
	}
	else
	{
		LOGIT(COUT_LOG," ItemID:0x%08x   ObjectIndex: 0x%04x ",(unsigned)lV,(unsigned)mV);
		LOGIT(COUT_LOG," SymbolIdx:0x%04x   Item Type:%s  Item Offset:0x%06x\n", (unsigned)nV,
													itemType_Str[(unsigned)oV], (unsigned)pV);
	}
	return retval;
}


/*** string_table_type  ************/
FMA_DevRow_String::FMA_DevRow_String(): FMA_Row(string_table_type)
{
}

int FMA_DevRow_String::parse(FMx_Table *table, unsigned char** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	UINT64   tempLongLong;	
	wstring  locStr;
	unsigned slength;
	unsigned offset=0;

	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}

	// get the string's length
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	slength = (unsigned)tempLongLong;// we need it on the next read
	// we don't need this...	push_back(val = slength);

	// get the string text
	status = parseWideString(&pLoc, remaining, slength, offset, locStr);
	if (status != SUCCESS)
	{
		return status;
	} 
	push_back(val = locStr);
	locStr = L"";

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_String::out(int rowNumber)
{
	CValueVarient lV;
	string   locAscii;
	wstring  locStr;
	int retval = SUCCESS;
	
	if (SUPPRESS_STRINGINDEX == false)
		LOGIT(COUT_LOG,"%4d",rowNumber);

	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	lV = * begin();
	locStr = (wstring)lV;
	//locAscii = TStr2AStr(locStr);stevev 30apr13 - we are already converted from utf8 to unicode
	locAscii = filter2print(locStr);

	LOGIT(COUT_LOG,"  '%s'.\n",locAscii.c_str());
	return SUCCESS;
}


/*** dictionary_table_type  ************/
FMA_DevRow_Dictionary::FMA_DevRow_Dictionary(): FMA_Row(dictionary_table_type)
{
}

int FMA_DevRow_Dictionary::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	UINT64   tempLongLong;	
	wstring  locStr;
	unsigned slength;
	unsigned offset=0;
		
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}
	
	// dictionary_id
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = ((unsigned)tempLongLong));

	// name_length
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	slength = (unsigned)tempLongLong;// we'll need it later
	// the name itself
	status = parseWideString(&pLoc, remaining, slength, offset, locStr);
	if (status != SUCCESS)
	{
		return status;
	} 
	push_back(val = locStr);
	
	locStr= L"";


	// entry_length
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	slength = (unsigned)tempLongLong;// we'll need it later
	// the dictionary entry
	status = parseWideString(&pLoc, remaining, slength, offset, locStr);
	if (status != SUCCESS)
	{
		return status;
	} 
	push_back(val = locStr);

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_Dictionary::out(int rowNumber)
{
	CValueVarient lV, mV, nV;
	wstring  locStr, locNm;
	int retval = SUCCESS;

	if (SUPPRESS_STRINGINDEX == false)
		LOGIT(COUT_LOG,"%4d",rowNumber);

	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}


	//push order: dictID, dictNmStr, dictStr
	int hi,lo;
	assert( size() == 3 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;  
		hi = (((int)lV) >> 16) & 0xffff;
		lo =  ((int)lV)        & 0xffff;
	mV = *vLIT++;
	locNm = (std::wstring)mV;
	nV = *vLIT++;
	locStr = (std::wstring)nV;
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Dictionary row is not at the end(). (%d)\n",size());
	}
	if (reconcile && locNm.compare(L"blank")==0)
	{
		int x = 0;
	}
	else
	{
		LOGIT(COUT_LOG," [%4d,%4d] |%s|    :>'%s'.\n",hi,lo,
										filter2print(locNm).c_str(),filter2print(locStr).c_str());
	}

	return SUCCESS;
}


/*** image_table_type  ************/
FMA_DevRow_Image::FMA_DevRow_Image(): FMA_Row(image_table_type)
{
}

int FMA_DevRow_Image::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{// needs BYTE_STRING (size 6),  unsigned 32, unsigned 32 length
	CValueVarient val;
	string   locAscii;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	int lCnt = 0;

	int status = FMA_Row::parse(table, &pLoc, remaining);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}

	UCHAR* pLang = NULL;
	unsigned lenORoff = 0;

	
	// get the lang count
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);
	lCnt = val;

	while ( lCnt && remaining)
	{
		// get the language
		status = parseByteString(&pLoc, remaining, 6, lenORoff, &pLang);
		if (status != SUCCESS)
		{
			return status;
		}
		locAscii = (char*)pLang;
		delete[] pLang;
		push_back(val = locAscii);

		// get the offset
		status = parse_unsigned32(&pLoc, remaining, lenORoff);
		if (status != SUCCESS)
		{
			return status;
		}
		push_back(val = lenORoff);

		// then get the length
		status = parse_unsigned32(&pLoc, remaining, lenORoff);
		if (status != SUCCESS)
		{
			return status;
		}
		push_back(val = lenORoff);
		lCnt--;
	}//wend

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_Image::out(int rowNumber)
{
	CValueVarient lV, mV, nV, oV;
	string        locAscii;
	int retval = SUCCESS;
	
	LOGIT(COUT_LOG,"%4d ",rowNumber);
	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order lang cnt {lang-string,  unsigned 32 offset , unsigned 32 length}
	int t = 0, st = 0;
	vector<CValueVarient>::iterator vLIT = begin();;
	oV = *vLIT++;// language count
		t = (unsigned)oV;
	if(t<=0)
	{
		LOGIT(COUT_LOG,"        }}} Error, bad language count.\n");
		return FAILURE;
	}
	assert( size() == ((3*t)+1) );

	LOGIT(COUT_LOG," IMAGE has %2d languages/images defined\n",t);
	for (int y = 0; y < t; y++)
	{
		lV = *vLIT++;// language
			locAscii = (string)lV;
		mV = *vLIT++;// offset
		nV = *vLIT++;// length
		LOGIT(COUT_LOG,"            Language:'%6s'   Offset: 0x%06x   Length: 0x%04x\n", 
			  locAscii.c_str(), (unsigned)mV, (unsigned)nV);
	}
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Image row is not at the end(). (%d)\n",size());
		return FAILURE;
	}

	return SUCCESS;
}


void FMA_DevRow_Image::generate(AframeList_t *framelist, const unsigned char *pBufferStart)
{
	CValueVarient lV, mV, nV, oV;
	string        locAscii;

	assert(framelist);
	assert(pBufferStart);

	// push order lang cnt {lang-string,  unsigned 32 offset , unsigned 32 length}
	int t = 0, st = 0;
	vector<CValueVarient>::iterator vLIT = begin();
	oV = *vLIT++;// language count
		t = (unsigned)oV;
	if(t<=0)
	{
		LOGIT(COUT_LOG," Error, bad language count.\n");
		return;
	}
	assert( size() == ((3*t)+1) );

	for (int y = 0; y < t; y++)
	{
		lV = *vLIT++;// language
			locAscii = (string)lV;
		mV = *vLIT++;// offset
		nV = *vLIT++;// length

		struct imgframe_s frame;
		frame.size = nV;
		frame.offset = mV;

		frame.pRawFrame = (unsigned char *)((unsigned)pBufferStart + frame.offset);

		strcpy(frame.language, locAscii.c_str());
		framelist->push_back(frame);
	}
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Image row is not at the end(). (%d)\n",size());
	}

	return;
}

/****************************  item_to_cmd_table_type  ********************************/
FMA_DevRow_Item2Cmd::FMA_DevRow_Item2Cmd(): FMA_Row(item_to_cmd_table_type)
{
	// the other items should be constructed empty
}
FMA_DevRow_Item2Cmd::~FMA_DevRow_Item2Cmd()
{
	cmdIt_t cIT;
	command_entry* pCE;

	for (cIT = rdCmdList.begin(); cIT != rdCmdList.end(); ++ cIT)
	{
		pCE = &(*cIT);
		pCE->clear();
	}

	for (cIT = wrCmdList.begin(); cIT != wrCmdList.end(); ++ cIT)
	{
		pCE = &(*cIT);
		pCE->clear();
	}
}

int FMA_DevRow_Item2Cmd::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	UINT64 tempLongLong;
	int tmp, i;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	command_entry localCmd;

	/*  resolved reference--he who we have command #s for...usually a symbol number */
	status = rr.parse(&pLoc, remaining);
	if (status != SUCCESS)
	{
		
		remainingSize = remainingSize - ((FMA_Table*)table)->rawByteLen;// calc expected exit values
		*pp_Location  = pLoc          + ((FMA_Table*)table)->rawByteLen;

		return SUCCESS;// so the other parses don't destatus;
	}
	// else continue with the rest of it

	// get the rd-cmd count
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	tmp = (int)tempLongLong;

	for ( i = 0; i < tmp && remaining > 0; i++)
	{
		status = localCmd.parse(&pLoc, remaining);
		if (status != SUCCESS)
		{
			clear();
			return status;
		}
		// else
		rdCmdList.push_back(localCmd);
		localCmd.clear();
	}// next read command

	// get the wr-cmd count
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	tmp = (int)tempLongLong;

	for ( i = 0; i < tmp && remaining > 0; i++)
	{
		status = localCmd.parse(&pLoc, remaining);
		if (status != SUCCESS)
		{
			clear();
			return status;
		}
		// else
		wrCmdList.push_back(localCmd);
		localCmd.clear();
	}// next write command
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_Item2Cmd::out(int rowNumber)
{
	cmdIt_t cIT;
	command_entry* pCE;
	int i;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

if (reconcile)
{
	if (rdCmdList.size() < 1 && wrCmdList.size() < 1)
		return 1;
}
	rr.out(rowNumber);
	
	LOGIT(COUT_LOG,"\n %3d  Read Commands:", rdCmdList.size());
	i = 1;
	for (cIT = rdCmdList.begin(); cIT != rdCmdList.end(); ++ cIT)
	{
		pCE = &(*cIT);
		pCE->out(i++);
	}
	if (rdCmdList.size() < 1)
		LOGIT(COUT_LOG," -- NONE --\n");

	LOGIT(COUT_LOG," %3d Write Commands:", wrCmdList.size());
	i = 1;
	for (cIT = wrCmdList.begin(); cIT != wrCmdList.end(); ++ cIT)
	{
		pCE = &(*cIT);
		pCE->out(i++);
	}
	if (wrCmdList.size() < 1)
		LOGIT(COUT_LOG," -- NONE --\n");

	return SUCCESS;// for now
}

int FMA_DevRow_Item2Cmd::reconcileOut(int rowNumber)
{
	cmdIt_t cIT;
	command_entry* pCE;
	int i;

	if ( rr.type != RESOLVED_REF_TYPEID )
	{
		LOGIT(COUT_LOG,"%4d] ",rowNumber);
		rr.ptocReconcileOut(rowNumber);
		//LOGIT(COUT_LOG,"\n");

		LOGIT(COUT_LOG,"        %d Read Commands:\n",rdCmdList.size());
		i = 1;
		for (cIT = rdCmdList.begin(); cIT != rdCmdList.end(); ++ cIT)
		{
			pCE = &(*cIT);
			pCE->ptocReconcileOut(i++);
		}

		LOGIT(COUT_LOG,"        %d Write Commands:\n",wrCmdList.size());
		i = 1;
		for (cIT = wrCmdList.begin(); cIT != wrCmdList.end(); ++ cIT)
		{
			pCE = &(*cIT);
			pCE->ptocReconcileOut(i++);
		}
	}
	return SUCCESS;// for now
}
// supporting classes ////////////////////////////////////////////////////////////////////////

int command_entry::parse(UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	UINT64 tempLongLong;
	int tmp, i;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	command_index   localIdx;

	// get the cmd number
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	cmdNumber = (unsigned)tempLongLong;

	// get the cmd-itm-info index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	cmdItemInfoIndex = (unsigned)tempLongLong;

	// get the transaction number
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	transactionNumber = (unsigned)tempLongLong;

	// get the weight
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	weight = (unsigned)tempLongLong;

	// get the index count
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	tmp = (unsigned)tempLongLong;


	for (i = 0; i < tmp && remaining > 0; i++)
	{
		status = localIdx.parse(&pLoc, remaining);
		if (status != SUCCESS)
		{
			clear();
			return status;
		}
		// else
		Indexes.push_back(localIdx);
		localIdx.clear();
	}// next index
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int command_entry::out(int rowNumber)
{
	cmdIdxIt_t ciIT;
	command_index* pIdx;
	
	char spaceone[] = " ";
	char spaceout[] = "                     ";

	if ( SUPPRESS_CIII   )
	{
		cmdItemInfoIndex = 0;
	}
	if ( SUPPRESS_WEIGHT )
	{
		weight = 0;
	}
	LOGIT(COUT_LOG,"%s%2d> Cmd %3u  Trans: %3u Weight: %4u   Cmd Item Info Index 0x%04x \n",
		(rowNumber == 1)?spaceone:spaceout, 
		rowNumber, cmdNumber, (unsigned)transactionNumber, weight, cmdItemInfoIndex);

	int k = 1;
	for (ciIT = Indexes.begin(); ciIT != Indexes.end(); ++ ciIT)
	{		
		pIdx = &(*ciIT);
		if (pIdx->idx_value == 4294967295)
		{
			if (reconcile==false)
				LOGIT(COUT_LOG,"%s         %2d} Index SymbolID: 0x%04x   Index Value: -1 \n",
									spaceout, k++, pIdx->idx_Sym_number);
		}
		else 
		{
			if (reconcile==false)
				LOGIT(COUT_LOG,"%s         %2d} Index SymbolID: 0x%04x   Index Value: %3d \n",
									spaceout, k++, pIdx->idx_Sym_number, pIdx->idx_value);
		}
		
	}
	return SUCCESS;//for now
}

int command_entry::ptocReconcileOut(int rowNumber)
{
	cmdIdxIt_t ciIT;
	command_index* pIdx;
	
	char spaceone[] = " ";
	char spaceout[] = "                     ";

	if (SUPPRESS_INTEGER)
	{
	LOGIT(COUT_LOG,"\t\t%4d> Cmd %3u:%u\tWeight: %4u\n",
		rowNumber, cmdNumber, (unsigned /* 14jan16 short*/) transactionNumber, (reconcile ? 0 : weight));
	}
	else
	{
	LOGIT(COUT_LOG,"\t\t%4d> Cmd %3u:%u\tWeight: %4u\n",
		rowNumber, cmdNumber, (unsigned)transactionNumber, (reconcile ? 0 : weight));
	}

	int k = 1;
	for (ciIT = Indexes.begin(); ciIT != Indexes.end(); ++ ciIT)
	{		
		pIdx = &(*ciIT);
		if (pIdx->idx_value == 4294967295)
		{
			if (reconcile==false)
				LOGIT(COUT_LOG,"\t\t\t%2d} Index SymbolID: 0x%04x   Index Value:  -1 \n",
								k++, pIdx->idx_Sym_number);
		}
		else 
		{
			if (reconcile==false)
				LOGIT(COUT_LOG,"\t\t\t%2d} Index SymbolID: 0x%04x   Index Value: %3u \n",
								k++, pIdx->idx_Sym_number, pIdx->idx_value);
		}
		
	}
	return SUCCESS;//for now
}
int command_index::parse(UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	// get the symbol number
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	idx_Sym_number = (unsigned)tempLongLong;

	// get the value
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	idx_value = (unsigned)tempLongLong;

	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int command_index::out(int rowNumber)
{// not used right now...
	return FAILURE;//for now
}


/********************* symbol_table_type  *************************/
FMA_DevRow_Symbol::FMA_DevRow_Symbol(): FMA_Row(symbol_table_type)
{
}

int FMA_DevRow_Symbol::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	string   locAscii;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}

	char *name;
			
	// get the symbol_id
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// then get the symbol_type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// then get the symbol_SUB_type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// then get the item_info_index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// then get the symbol name
	unsigned offset=0;	// not needed here
	status = parseAsciiString(&pLoc, remaining, offset, &name);
	if (status != SUCCESS)
	{
		return status;
	}
	locAscii = name;

	val = locAscii;
	int k = size();
	push_back(val);

	delete[] name;
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_Symbol::out(int rowNumber)
{
	CValueVarient lV, mV, nV, oV, pV;
	string        locAscii;
	int retval = SUCCESS;
	
	LOGIT(COUT_LOG,"%4d",rowNumber);
	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, symbolType, symSubType, idx2ItemInfoTbl, symName
	int t = 0, st = 0;
	assert( size() == 5 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// symbol number
	mV = *vLIT++;// item type
		t = (unsigned)mV;
	nV = *vLIT++;// item sub type
		st= (unsigned)nV;
	oV = *vLIT++;// Item Info Table Index
	pV = *vLIT++;// symbol name

	/* timj 22jul2013
	 * the line below doesn't function because the symbol name is not available
	 * at the time the table is built, so it is always the empty string.

	 * stevev 23sep16 - it's only empty when saveBinary in the symbol table is false 
	stevev 26may17 - I'm going to leave this out for now since our vnv was done with it gone*/
	//	locAscii = (string)pV;
	 
	 /* the two lines below replace the original line, looking up the name when
	 * this out method is called:
	 stevev 26may17 - I'm going to leave this in for now*/

	int xt;	// throw away
	ConvertToString::fetchSymbolNameByIndex(rowNumber, locAscii, xt);
	
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Symbol row is not at the end(). (%d)\n",size());
	}
	if ( t > FMAITMTYPECOUNT || st > FMAITMTYPECOUNT )
	{
		LOGIT(COUT_LOG|CERR_LOG," Symbol table item type is invalid.\n");
		return FAILURE;
	}
	if ( t == it_Variable ) // it's a variable
	{
		LOGIT(COUT_LOG," 0x%08x   Type:%s subtype:%-15s",(unsigned)lV,itemType_Str[t], 
																		varType_Str[st]);
	}
	else
	{
		LOGIT(COUT_LOG," 0x%08x   Type:%s subtype:%15s",(unsigned)lV,itemType_Str[t], 
																		itemType_Str[st]);
	}
	LOGIT(COUT_LOG," ItmInfoIdx:0x%04x   Name:'%s'\n", (unsigned)oV, locAscii.c_str());

	return SUCCESS;
}


/*** block_info_table_type  ************/
FMA_DevRow_BlockInfo::FMA_DevRow_BlockInfo(): FMA_Row(block_info_table_type)
{
}

int FMA_DevRow_BlockInfo::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{	
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}
	
	// get the block-symbol-id
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get the block-item-info-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get the charRec-item-info-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get the charRec-blk-itm-name-idx
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	// get the block-tables-object-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_BlockInfo::out(int rowNumber)
{
	CValueVarient lV, mV, nV, oV, pV;
	int retval = SUCCESS;
	
	LOGIT(COUT_LOG,"%4d",rowNumber);
	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, iiIdx, charReciiIdx, charRecBlkItmNmIdx, BlockDirObjIndex
	int t = 0, st = 0;
	assert( size() == 5 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// symbol number
	mV = *vLIT++;// block item info index
		t = (unsigned)mV;
	nV = *vLIT++;// charRec-item-info Index
		st= (unsigned)nV;
	oV = *vLIT++;// charRec - Block Item Name Index
	pV = *vLIT++;// Block Table Set Index
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Symbol row is not at the end(). (%d)\n",size());
		return FAILURE;
	}
	LOGIT(COUT_LOG," BlkSymNumber:0x%08x  BlkItmInfo Idx:%d  ", (unsigned)lV, (int)mV);
	LOGIT(COUT_LOG,"charRecItmInfo Index:%d  charRecBIN Idx:%d'\n",(int)nV,(int)oV);
	LOGIT(COUT_LOG,"  BlkTables Object Index:%d \n", (int)pV);

	return SUCCESS;
}


/*** command_number_table_type  ************/
FMA_DevRow_CommandNumber::FMA_DevRow_CommandNumber(): FMA_Row(command_number_table_type)
{
}

int FMA_DevRow_CommandNumber::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;

	int status = FMA_Row::parse(table, pp_Location, remainingSize);// call the parent
	if (status != SUCCESS)
	{
		return status;
	}
	
	// get the command number
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// command-number		INTEGER,

	// then get the item info index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);//item-info-index	INTEGER

	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_DevRow_CommandNumber::out(int rowNumber)
{
	CValueVarient lV, mV;
	int retval = SUCCESS;
	
	LOGIT(COUT_LOG,"%4d",rowNumber);
	if ( size() <= 0 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}
	// push order: cmd Number, item info index
	int c = 0, iiI = 0;
	assert( size() == 2 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// command number
		c = (unsigned)lV;
	mV = *vLIT++;// Item Info Index
		iiI= (unsigned)mV;
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Command Number row is not at the end(). (%d)\n",size());
		return FAILURE;
	}
	LOGIT(COUT_LOG," Command Number:0x%04x   Item Info Table Index:0x%04x  \n", c,iiI);
	return SUCCESS;
}



/*******************  BLOCK DIRECTORY *******************************************************/

/*** critical_item_table_type  ** critical-items  [10] Critical_Item_Table_Struct**********/
FMA_BlkRow_CriticalItem::FMA_BlkRow_CriticalItem(): FMA_Row(critical_item_table_type)
{
}

FMA_BlkRow_CriticalItem::~FMA_BlkRow_CriticalItem()
{
}

int FMA_BlkRow_CriticalItem::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	CValueVarient val;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	int    status       = SUCCESS;

	status = rr.parse(&pLoc, remaining);
	if (status != SUCCESS)
	{		
		remainingSize = remainingSize - ((FMA_Table*)table)->rawByteLen;// calc expected exit values
		*pp_Location  = pLoc          + ((FMA_Table*)table)->rawByteLen;

		return SUCCESS;// so the other parses don't distatus;
	}
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_BlkRow_CriticalItem::out(int rowNumber)
{	
	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	rr.out(rowNumber);
	LOGIT(COUT_LOG,"\n");
	return SUCCESS;
}

/*********************************************************************************************/
/*** block_item_table_type     ** block-item      [11] Block_Item_Table_Struct ***************/

FMA_BlkRow_BlockItem::FMA_BlkRow_BlockItem(): FMA_Row(block_item_table_type)
{
	// the other items should be constructed empty
}
FMA_BlkRow_BlockItem::~FMA_BlkRow_BlockItem()
{
}

int FMA_BlkRow_BlockItem::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	
	// get the item-symbol-id
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);
	
	// get the Block-Item-Name-Index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_BlkRow_BlockItem::out(int rowNumber)
{	
	CValueVarient lV, mV;
	int retval = SUCCESS;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	if ( size() < 2 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, Index
	assert( size() == 2 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// symbol number
	mV = *vLIT++;// item index
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Block-Item row is not at the end(). (%d)\n",size());
	}
		
	LOGIT(COUT_LOG," Symbol Number: 0x%04x   Block-Item-Name Index: 0x%04x \n", 
																(unsigned)lV, (unsigned)mV);

	return SUCCESS;// for now
}



/*** block_item_name_table_type * block-item-name [12] Block_Item_by_Name_Table_Struct*****/
FMA_BlkRow_BlockItemName::FMA_BlkRow_BlockItemName(): FMA_Row(block_item_name_table_type)
{
	// the other items should be constructed empty
}
FMA_BlkRow_BlockItemName::~FMA_BlkRow_BlockItemName()
{
}

int FMA_BlkRow_BlockItemName::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	CValueVarient val;
	UINT64 tempLongLong;
	unsigned type;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	
	// get the implicit tag... hi bit zero, low 4 bits tell type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	type = (int)tempLongLong;
	if ( type != 0 ) // member-id
	{		
		LOGIT(CLOG_LOG|CERR_LOG," Block Item Name Row does not start w/ ID. (%d)\n",type);
		return FAILURE;
	}// else ... 
	// get the symbol-id  (spec calls it a member-id)
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	val = (unsigned)tempLongLong;
	val.vIndex = 0;// 'member-id'
	push_back(val);
	//////////////
	// get the implicit tag... hi bit zero, low 4 bits tell type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	type = (int)tempLongLong;
	if ( type != 1 ) // item-info-index
	{		
		LOGIT(CLOG_LOG|CERR_LOG,"Block Item Name Row does not have w/ itmInfoIdx.(%d)\n",type);
		return FAILURE;
	}// else ... 
	// get the Item-Info-Index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	val = (unsigned)tempLongLong;
	val.vIndex = 1;// item-info-index
	push_back(val);
	// while the next item is not an implict tag for a 'member-id' and we didn't finish
	while ( *pLoc != 0 && remaining > 0)
	{
		// get the next entry (paramIndex, Param-ListIdx, relIndex, itm2cmdIndex)
		// get the implicit tag... hi bit zero, low 4 bits tell type
		status = parseInteger(4, &pLoc, remaining, tempLongLong);
		if (status != SUCCESS)
		{
			return status;
		}
		type = (int)tempLongLong;
		if ( type < 2 || type > 5 ) //out of range
		{		
			LOGIT(CLOG_LOG|CERR_LOG,"Block Item Name Row entry type wrong.(%d)\n",type);
			return FAILURE;
		}// else ... 
		// get the entry
		status = parseInteger(4, &pLoc, remaining, tempLongLong);
		if (status != SUCCESS)
		{
			return status;
		}
		val = (unsigned)tempLongLong;
		val.vIndex = type;// item-info-index
		push_back(val);
	}// wend not a new member
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_BlkRow_BlockItemName::out(int rowNumber)
{	
	CValueVarient lV, mV;
	int retval = SUCCESS;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	if ( size() < 2 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, Index, others
	assert( size() >= 2 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// member id
	mV = *vLIT++;// item-info index

	LOGIT(COUT_LOG," Member ID: 0x%04x   Item-Info Index: 0x%04x ", 
																(unsigned)lV, (unsigned)mV);
	string fieldName;
	unsigned k = 2;
	while ( size() > k )
	{		
		mV = *vLIT++;// item-info index
		switch (mV.vIndex)
		{
		case 2: //param index
			fieldName = "Param-Index   ";
			break;
		case 3: //param-list index
			fieldName = "Param-List Idx";
			break;
		case 4: //relation index
			fieldName = "Relation Index";
			break;
		case 5: //Itm2Cmd index
			fieldName = "Item2Cmd Index";
			break;
		default:
			fieldName = "Unknown-----Index";
			break;
		}//endswitch
		k++;
		LOGIT(COUT_LOG," %s: 0x%04x ", fieldName.c_str(), (unsigned)mV);
	}//wend
	
	LOGIT(COUT_LOG,"\n");

	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Block-Item-Name row is not at the end(). (%d)\n",size());
	}		

	return SUCCESS;// for now
}



/*** parameter_table_type      ** parameter       [13] Parameter_Table_Struct *************/
FMA_BlkRow_Parameter::FMA_BlkRow_Parameter(): FMA_Row(parameter_table_type)
{
	// the other items should be constructed empty
}
FMA_BlkRow_Parameter::~FMA_BlkRow_Parameter()
{
}

int FMA_BlkRow_Parameter::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	CValueVarient val;
	UINT64 tempLongLong;
	unsigned type;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	
	// get the implicit tag... hi bit zero, low 4 bits tell type
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	type = (int)tempLongLong;
	if ( type != 0 ) // block-item-name-index
	{		
		LOGIT(CLOG_LOG|CERR_LOG," Parameter Row does not start w/ ID. (%d)\n",type);
		return FAILURE;
	}// else ... 
	// get the BIN-index  
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	val = (unsigned)tempLongLong;
	val.vIndex = 0;// which key 'block-item-name-index'
	push_back(val);
	//////////////
	// while the next item is not an implict tag for a 'block-item-name-index' and we have size
	while ( *pLoc != 0 && remaining > 0)
	{	// get the next entry (one of eight)
		// get the implicit tag... hi bit zero, low 4 bits tell type
		status = parseInteger(4, &pLoc, remaining, tempLongLong);
		if (status != SUCCESS)
		{
			return status;
		}
		type = (int)tempLongLong;
		if ( type < 1 || type > 9 ) //out of range
		{		
			LOGIT(CLOG_LOG|CERR_LOG,"Parameter Row entry type wrong.(%d)\n",type);
			return FAILURE;
		}// else ... 
		// get the entry
		status = parseInteger(4, &pLoc, remaining, tempLongLong);
		if (status != SUCCESS)
		{
			return status;
		}
		val = (unsigned)tempLongLong;
		val.vIndex = type;// item-info-index
		push_back(val);
	}// wend not a new param
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_BlkRow_Parameter::out(int rowNumber)
{	
	CValueVarient lV, mV;
	int retval = SUCCESS;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	if ( size() < 1 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, Index, others
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// block-item-name index

	LOGIT(COUT_LOG," BIN index: 0x%04x ", (unsigned)lV);
	string fieldName;
	unsigned k = 1;
	while ( size() > k )
	{		
		mV = *vLIT++;// value is that, vIndex is type
		switch (mV.vIndex)
		{
		case 5: //Itm2Cmd index
			fieldName = "Number-Of-Elements";
			break;
		case 6: //item-info-index-of-type
			fieldName = "Type's ItemInfoIdx";
			break;
		case 7: //param type
			fieldName = "Parameter Type    ";
			break;
		case 8: //param size
			fieldName = "Parameter Size    ";
			break;
		case 9: //param class
			fieldName = "Parameter Class   ";
			break;

		case 1:
			fieldName = "Error  paramMemIdx";
			break;
		case 2:
			fieldName = "Error  paramMemCnt";
			break;
		case 3:
			fieldName = "Error paramElemIdx";
			break;
		case 4:
			fieldName = "Error paramElemCnt";
			break;
		default:
			fieldName = "Unknown Field Type";
			break;
		}//endswitch
		k++;
		LOGIT(COUT_LOG," %s: 0x%04x ", fieldName.c_str(), (unsigned)mV);
	}//wend
	
	LOGIT(COUT_LOG,"\n");

	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Parameter row is not at the end(). (%d)\n",size());
	}		

	return SUCCESS;// for now
}


/*** relation_table_type       ** relations       [22] Relations_Table_Struct    **********/
FMA_BlkRow_Relations::FMA_BlkRow_Relations(): FMA_Row(relation_table_type)
{
	// the other items should be constructed empty
}
FMA_BlkRow_Relations::~FMA_BlkRow_Relations()
{
}

int FMA_BlkRow_Relations::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	
	///////// get the wao-block-item-info-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// value or -1
	
	///////// get the unit-block-item-info-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// value or -1
	
	///////// get the update-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// value or 0
	
	///////// get the update-count
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// value or 0
	
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}

int FMA_BlkRow_Relations::out(int rowNumber)
{	
	CValueVarient lV, mV, nV, oV;
	int retval = SUCCESS;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	if ( size() < 2 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	// push order: symID, Index
	assert( size() == 4 );
	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// symbol number
	mV = *vLIT++;// item index
	nV = *vLIT++;// item index
	oV = *vLIT++;// item index
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Relations row is not at the end(). (%d)\n",size());
	}
		
	LOGIT(COUT_LOG,
	" WAO item-info-idx: 0x%08x   Unit item-info-idx: 0x%08x Update Idx: 0x%04x and Count: 0x%04x \n", 
	         (unsigned)lV,          (unsigned)mV,      (unsigned)nV,     (unsigned)oV);

	return SUCCESS;// for now
}



/*** update_table_type         ** update          [23] Update_Table_Struct ****************/
FMA_BlkRow_Update::FMA_BlkRow_Update(): FMA_Row(update_table_type)
{
	// the other items should be constructed empty
}
FMA_BlkRow_Update::~FMA_BlkRow_Update()
{
}

int FMA_BlkRow_Update::parse(FMx_Table *table, UCHAR** pp_Location, INT& remainingSize)
//int FMA_BlkRow_Update::parse(UCHAR** pp_Location, INT& remainingSize)
{
	int status = SUCCESS;
	CValueVarient val;
	UINT64 tempLongLong;
	UCHAR* pLoc         = *pp_Location;
	int    remaining    = remainingSize;
	
	///////// get the item-info-index
	status = parseInteger(4, &pLoc, remaining, tempLongLong);
	if (status != SUCCESS)
	{
		return status;
	}
	push_back(val = (unsigned)tempLongLong);// value or -1
	
	///////// get the to be updated resolved reference
	status = rr.parse(&pLoc, remaining);
	if (status != SUCCESS)
	{		
		remainingSize = remainingSize - ((FMA_Table*)table)->rawByteLen;// calc expected exit values
		*pp_Location  = pLoc          + ((FMA_Table*)table)->rawByteLen;

		return SUCCESS;// so the other parses don't destatus;
	}
		
	*pp_Location = pLoc;
	remainingSize= remaining;

	return status;
}


int FMA_BlkRow_Update::out(int rowNumber)
{	
	CValueVarient lV, mV, nV, oV;
	int retval = SUCCESS;

	LOGIT(COUT_LOG,"%4d] ",rowNumber);

	if ( size() != 1 )
	{
		LOGIT(COUT_LOG,"        }}} Error, row has no data.\n");
		return FAILURE;
	}

	vector<CValueVarient>::iterator vLIT = begin();
	lV = *vLIT++;// symbol number
	if ( vLIT != end() )
	{// something is wrong
		LOGIT(CERR_LOG," End of Update row is not at the end(). (%d)\n",size());
	}
		
	LOGIT(COUT_LOG," item-info-idx: 0x%08x  ", (unsigned)lV);
	rr.out(-1);
	LOGIT(COUT_LOG,"\n");

	return SUCCESS;// for now
}




/*******************************************************************************************/