/**********************************************************************************************
 *
 * $Workfile: FMA_Attributes_Ref&Expr.h $
 * 23mar12 - stevev
 *     Revision, Date and Author are never in these files
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2012 HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FMA_Attributes_Ref&Expr.h : These are the content handlers
 *
 * #include "FMA_Attributes_Ref&Expr.h"
 */

#ifndef _FMA_ATTR_REF_N_EXPR_H
#define _FMA_ATTR_REF_N_EXPR_H

#include "FMx_Attributes_Ref&Expr.h"
#include "FMA_defs.h"
#include "Convert.h"

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class expressionElementA : public expressionElement
{
public: // construct
	expressionElementA() : eeAttrType(at_scan_list) {};
	expressionElementA(const expressionElementA& src) { operator=(src); };
	expressionElementA& operator=(const expressionElementA& s) 
	{
		eeAttrType = s.eeAttrType;
		ee_type    = s.ee_type;
		return *this;
	};
	virtual ~expressionElementA() {};// no data to destroy here


public: // data
	attr_t	eeAttrType;
	exprElemType_t ee_type;// the fma type

public: // methods
	virtual void clear() { expressionElement::clear(); eeAttrType=at_invalid_value;
							ee_type = eet_Undefined; expressionElement::clear(); };
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  Primative Attribute classes
 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCExpressionA : public abCExpression
{
public: // construct
	abCExpressionA() {};
	abCExpressionA(UINT64 const_value)
	{
		expressionElementA* pExprElem = new expressionElementA;
		pExprElem->eeInt = const_value;
		pExprElem->ee_type = eet_integer_constant;
		pExprElem->exprElemType = ConvertToDDL::ExpressionElementType(eet_integer_constant);

		exprList.push_back(pExprElem);
	};
	virtual ~abCExpressionA() {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class abCReferenceA : public abCReference
{
public: // construct
	abCReferenceA() {};

public: // methods
	virtual int evaluateAttribute(UCHAR** pData, int& length, unsigned& offset);
};

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

#endif //_FMA_ATTR_REF_N_EXPR_H