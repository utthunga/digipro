/**********************************************************************************************
 *
 * $Workfile: FM8_Act_ReadPayload.cpp $
 * 02Nov11 - stevev - created
 *     Revision, Date and Author are not in these files.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		FM8_Act_ReadPayload Visitor Operation on all object classes
 *		
 *
 */

///  this is still all FMA code!!!!!!!!!!!!!!!!!!!!!!
//#error("Still has FMA code in the file.")
#include "FM8_Activities.h"
#include "FMx.h"
#include "FM8_handlers.h"
#include "FM8_object.h"
#include "Endian.h"
#include "logging.h"

// // required function
//	> x81 disregard, x41 won't be seen(fma-meta & fm8-meta)
//	> x83 disregard, x43 won't be seen(fma-objset * fm8-objset)
//	> x82 disregard for now, x42 -- lots of work to do (fma-obj, work on fm8-obj)
int FM8_InterpretPayloadVisitor::Operate( FMx_Base* pVisited )
{
	RETURNCODE r = FAILURE;
	FM8_Object*  pThisObject = (FM8_Object*)pVisited;
	if ( pVisited == NULL )
		return FAILURE;
	FMx_handler* pHandler    = pThisObject->getObjectHandler();
	if ( pHandler == NULL )
		return FAILURE;// has to be out of memory or an unimplemented class

	unsigned ct = pVisited->classType;
	if ( (ct & (FM8_type|FMA_type)) == FMA_type ) //(fm8-meta fm8-objset,fm8-obj)
	{// we are going to disregard all FMA for now
		return SUCCESS;
	}
	if ( (ct & 0x0f) == __Meta )//( fma - meta & fma-objset )
	{// fm8 meta is not an object	
		return SUCCESS;//
	}
	// we have to have class type FM8_Object || FM8_ODES <all that's left>
	pThisObject = (FM8_Object*)pVisited;

	// handler instantiation is now the responsibility of the Object

	// evaluateExtension has to be smart about its job
	// if chunks need to be read in (offset !=-1 & size > 0 & data pointer empty)
	// then evaluate the extension data and continue
	// I'm going to use a 'extension-processed' flag to simplify
	if ( pThisObject->pHandler != NULL )
	{
		r = pThisObject->pHandler->evaluateExtension();
	}

	// then read the data heap section as required
	// note that this uses the handler since on a -1 size, it must have knowledge of contents
	pThisObject->readHeapChunk(heapSize);

	// stevev 18sep13 tell the attributes where the annex really starts
	if (pThisObject->obj_type == obj_8_DD_item)
	{
		FM8_AttributeList* pAL =
		&(((FM8_Item_handler*)(pThisObject->pHandler))->items_attributes);
		pAL->objHeapStartOffset = pThisObject->objHeapStartOffset;
	}
	// evaluateExtension has to be smart about its job
	// if chunks have already been read in (offset !=-1 & size > 0 & data pointer has value)
	// then evaluate the chunk data as it stands
	// else continue
	// I'm going to use a 'extension-processed' flag to simplify
	if ( pThisObject->pHandler != NULL )
	{
		r = pThisObject->pHandler->evaluateExtension();
	}
	return r;
};


