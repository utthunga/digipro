#include "ddbDevice.h"
#include "MEE.h"

#define MAX_METHOD_LIST 20
// Component History: 
// 
// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version
//
// 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
//
// WHS EP June of 2008 - huge rework of function GetDDVarValNEvalAssType()
//
//////////////////////////////////////////////////////////////////////////////////////////////////////

bool OneMeth::ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId, long lVarItemId)
{
	if (pDevice == NULL)
	{
		return false;
	}

	previousRetryCount = pDevice->pCmdDispatch->setRetries(0);

	if(0 != lVarItemId)
	{
		m_lPrePostItemId = lVarItemId;
	}

	m_pMEE = pMEE; //Initialize the MEE pointer;

	m_pDevice = pDevice;

//	pDevice->cacheDispValues(); 

	INTERPRETER_STATUS intStatus = ExecMethod(lMethodItemId);

	pDevice->pCmdDispatch->setRetries(previousRetryCount);previousRetryCount=0;

	if (intStatus != INTERPRETER_STATUS_OK)
	{
		//m_bSaveValues = false;
		// replaced by below----HandleSaveValues();
		//pDevice->uncacheDispVals(m_bSaveValues);

		return false;
	}

	if (m_bMethodAborted == true || m_pMEE->latestMethodStatus == me_Aborted )
	{// stevev - 14jun13 - this code is never called in an abort sequence.
		for( ListIterator = abortMethodList.begin();ListIterator != abortMethodList.end();ListIterator++ )
		{
			long lQueueMethodId = *ListIterator;
			intStatus = ExecMethod(lQueueMethodId);
			if (intStatus != INTERPRETER_STATUS_OK)
			{
				//m_bSaveValues = false;
				// replaced by below----HandleSaveValues();
				//pDevice->uncacheDispVals(m_bSaveValues);
				return false;
			}
		}
		if ( m_pMEE != NULL )// Copied from the Method-calling-Method version below
		{//                     specifically to stop a simple error return on methods that
		 //                     end via ProcessAbort.
			m_pMEE->latestMethodStatus = me_Aborted;
		}
	}

	// replaced by below----HandleSaveValues();
	//pDevice->uncacheDispVals(m_bSaveValues);
	if(m_bMethodAborted == true)
		return false;

	return true;
}

//this is the function to be called for all pre/post actions
/*bool OneMeth::ExecuteMethod(hCddbDevice *pDevice, long lMethodItemId, long lVarItemId)
{
	//return false;
	//added by prashant
	m_lPrePostItemId = lVarItemId;
	return ExecuteMethod(pDevice, lMethodItemId);
}*/
int OneMeth::abort()
{
	m_bMethodAborted = true;
	return 0;
}

int OneMeth::process_abort()
{
	m_bMethodAborted = true;
	return 0;
}

int OneMeth::_add_abort_method(long lMethodId)
{
	int nRetVal = BI_ERROR;

	//AOEP35747
	if(abortMethodList.size() < MAX_METHOD_LIST )
	{
		abortMethodList.push_back(lMethodId);
		nRetVal = BI_SUCCESS;
	}
	return nRetVal;
}

//
//
//
int OneMeth::_remove_abort_method(long lMethodId)
{
	int nRetVal = BI_ERROR;

	if( !GetMethodAbortStatus() )
	{
	ListIterator = abortMethodList.begin();
	while (ListIterator != abortMethodList.end())
	{
		long lQueueMethodId = *ListIterator;
		if (lQueueMethodId == lMethodId)
		{
			abortMethodList.erase(ListIterator);
			ListIterator = abortMethodList.begin();
				nRetVal = BI_SUCCESS;
			break;/*Vibhor 030305: Bug Fix : As per the definition in the spec it should only 
					remove the first occurence of a method in the abort list
			        So,loop no further after the first one !!*/
		}
		else
		{
			ListIterator++;
		}
	}
	}
	return nRetVal;
}

int OneMeth::remove_all_abort()
{        
	int nRetVal = BI_ERROR;

	if( !GetMethodAbortStatus() )
	{
	if( !abortMethodList.empty() ) 
    { 
		abortMethodList.clear(); 
    } 
		nRetVal = BI_SUCCESS;
	}

	return nRetVal;
}


bool OneMeth::GetMethodAbortStatus()
{
	return m_bMethodAborted;	
}

int OneMeth::_push_abort_method(long lMethodId)
{
	int nRetVal = BI_ERROR;

	if( abortMethodList.size() < MAX_METHOD_LIST )
	{
	abortMethodList.push_front(lMethodId);
		nRetVal = BI_SUCCESS;
}

	return nRetVal;
}

//
//
//
int OneMeth::_pop_abort_method()
{
	int nRetVal = BI_ERROR;
	if( !GetMethodAbortStatus() )
	{
	if(abortMethodList.size() !=0)
	{
		abortMethodList.pop_front();
			nRetVal = BI_SUCCESS;
		}
	}
	return nRetVal;
}

void OneMeth::AllocLibrary()
{
	FreeLibrary();
	m_pInterpreter = new CInterpreter;
	m_pBuiltinLib = new CHart_Builtins;

	if ((m_pInterpreter == NULL) || (m_pInterpreter == NULL))
	{
		LOGIT(CERR_LOG,"OneMeth::AllocLibrary() Allocation Failed.");
		FreeLibrary();
		return;
	}
	return;
}

void OneMeth::FreeLibrary()
{
	if (m_pInterpreter != NULL)
	{
		delete m_pInterpreter;
		m_pInterpreter = NULL;
	}
	if (m_pBuiltinLib != NULL)
	{
		delete m_pBuiltinLib;
		m_pBuiltinLib = NULL;
	}
}

INTERPRETER_STATUS	OneMeth::ExecMethod(long lMethodItemId)
{
	/* First check if the Item Id is valid */
	RETURNCODE rc = SUCCESS;
	hCitemBase*  p_ib = NULL;
	hCmethod*    p_m  = NULL;
	rc = m_pDevice->getItemBySymNumber(lMethodItemId, &p_ib);
	if (!(rc == SUCCESS && p_ib != NULL && p_ib->getIType() == iT_Method))
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	m_lMethodItemId = lMethodItemId;

	p_m = (hCmethod*)p_ib;
	int   sLen = 0;
	char* pS = p_m->getDef(sLen);

	if (pS == NULL)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	if (strlen(pS) <= 0)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	char pbyData[1] = "";//[10000] = "";
	char pbySymbolTable[10000] = "";

	AllocLibrary();
	m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,this);
	m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,m_lPrePostItemId); /* VMKP added on 200204 */
//	char pchCode[] = {"{float tmp;long lValue[3];tmp=2.8;lValue[0] = 150;lValue[1] = 151;lValue[2] = 152;acknowledge(\"Test string%{tmp} and val is %{0}\", lValue);}"};

	if (m_bMethodAborted == true)
	{
		this->m_pBuiltinLib->m_AbortInProgress = true;
	}
	m_pMEE->methodNameString = p_m->getName();

	previousRetryCount = m_pDevice->pCmdDispatch->setRetries(0);
	DEBUGLOG(CLOG_LOG,">> ExecMethod setting retries to 0 from %d.\n",previousRetryCount);

	INTERPRETER_STATUS intStatus =
		m_pInterpreter->ExecuteCode (m_pBuiltinLib,pS,"Test",pbyData, pbySymbolTable,m_pMEE);

	m_pDevice->pCmdDispatch->setRetries(previousRetryCount);
	DEBUGLOG(CLOG_LOG,">> ExecMethod setting retries back to %d.\n",previousRetryCount);
	previousRetryCount=0;

	if (intStatus != INTERPRETER_STATUS_OK)
	{
		switch (intStatus)
		{
			case INTERPRETER_STATUS_INVALID:
				DisplayMessage("**** Internal Error: ****\nInterpreter State Unknown !");
				FreeLibrary();
				return INTERPRETER_STATUS_INVALID;
			case INTERPRETER_STATUS_PARSE_ERROR:
				DisplayMessage("**** Internal Error: ****\nMethod code has parsing errors !");
				FreeLibrary();
				return INTERPRETER_STATUS_PARSE_ERROR;
			case INTERPRETER_STATUS_EXECUTION_ERROR:
				DisplayMessage("**** Internal Error: ****\nError in method execution !");
				FreeLibrary();
				return INTERPRETER_STATUS_EXECUTION_ERROR;
			case INTERPRETER_STATUS_UNKNOWN_ERROR:
				DisplayMessage("**** Internal Error: ****\nInterpreter State Unknown !");
				FreeLibrary();
				return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}
	}
	else // it is OK
	if (m_pMEE && m_pMEE->latestMethodStatus == me_Aborted)
	{
		m_bMethodAborted = true;
	}

  //commented by prashant 230204
	//DisplayMessage("", false);
  //prashant 230204 end
	FreeLibrary();
	return intStatus;
}

void OneMeth::save_values(void)
{
/* 17nov14 - save_values now has to be a 'snapshot' of the cached values when called.
	m_pMEE->m_bSaveValues = true;//m_bSaveValues = true;
we will do this by re-caching everything from display value to method cache */
	m_pDevice->cacheDispValues(true);// stevev 10feb15 generate notifications from here
}

//Added By Anil July 01 2005 --starts here
void OneMeth::discard_on_exit(void)
{
	if(m_pMEE->m_bSaveValues == true)
	{
		m_pMEE->m_bSaveValues =false;
	}
}
//Added By Anil July 01 2005 --Ends here

void OneMeth::DisplayMessage(char *pchMessage, bool bUserAcknowledgeValue)
{
	ACTION_USER_INPUT_DATA structUserInput;
	ACTION_UI_DATA structUIData;

	structUIData.userInterfaceDataType = TEXT_MESSAGE;
	structUIData.bUserAcknowledge = bUserAcknowledgeValue;
	/*Vibhor 030304: Start of Code*/
	structUIData.bEnableAbortOnly = false; // just defensive
	/*Vibhor 030304: End of Code*/
	/*Vibhor 040304: Start of Code*/
	structUIData.uDelayTime = 0;// just defensive
	/*Vibhor 040304: End of Code*/

	if(structUIData.textMessage.pchTextMessage != NULL)
	{
		delete[] structUIData.textMessage.pchTextMessage; 
		structUIData.textMessage.pchTextMessage = NULL; 
	}
	structUIData.textMessage.iTextMessageLength = strlen(pchMessage);
	if(strlen(pchMessage))
	{

		structUIData.textMessage.pchTextMessage = new tchar[strlen(pchMessage) + 1];
//		strcpy(structUIData.textMessage.pchTextMessage, pchMessage);
//TEMPORARY conversion
string  ss(pchMessage);
wstring ws; ws = AStr2TStr(ss);
_tstrcpy(structUIData.textMessage.pchTextMessage, ws.c_str());
	}
	else
	{
		structUIData.textMessage.pchTextMessage = NULL;
	}

	structUIData.bDisplayDynamic = false;	//Added by Prashant 20FEB2004
	m_pDevice->m_pMethSupportInterface->MethodDisplay(structUIData,structUserInput);

	if(strlen(pchMessage))
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;// WS:EPM 24may07
	}
	return;
}

bool OneMeth::RefreshWaveform(long lRfrshMethID)
{

	return m_pMEE->ExecuteMethod(m_pDevice,lRfrshMethID);
	
}

int OneMeth::ExecuteActions(ddbItemList_t actionList)
{
	for(ddbItemList_t :: iterator iT = actionList.begin(); iT != actionList.end(); iT++)
	{
		long lMethId = (*iT)->getID();
		bool bRet= m_pMEE->ExecuteMethod(m_pDevice,lMethId);
		if(false == bRet)
			return FAILURE;
	}
	return SUCCESS;
}



bool MEE ::ExecuteMethod(hCddbDevice *pDevice,long lMethodItemId)
{
	m_pDevice = pDevice; //Vibhor 070705: Added, Sorry I missed this one earlier !!!

	OneMeth *pMeth = new OneMeth();
IncreaseOneMethRefNo();
	latestMethodStatus = me_NoError;

	bool bRet = pMeth->ExecuteMethod(this,pDevice,lMethodItemId);
	DecreaseOneMethRefNo();

	delete pMeth;

	return bRet;

}

bool MEE ::ExecuteMethod(hCddbDevice *pDevice,long lMethodItemId,long lVarItemId)
{
	m_pDevice = pDevice; //Anil 12oct05: Vibhor, you missed this one too !!!

	OneMeth *pMeth = new OneMeth();
	IncreaseOneMethRefNo();
	latestMethodStatus = me_NoError;

	bool bRet = pMeth->ExecuteMethod(this,pDevice,lMethodItemId,lVarItemId);
	DecreaseOneMethRefNo();

	delete pMeth;

	return bRet;
}/*End ExecuteMethod*/

long MEE::FindGlobalToken(const _CHAR* pszTokenName, long lLineNum,DD_ITEM_TYPE &DDitemType)//changed the Function Type Anil September 16 2005
{
	RUL_TOKEN_TYPE		tokenType = RUL_SIMPLE_VARIABLE; 
	RUL_TOKEN_SUBTYPE  tokenSubType = RUL_SUBTYPE_NONE;

//	bIsVariableItem = true;
	DDitemType = DD_ITEM_VAR;
	_INT32 nIndx = -1;
		
	nIndx = m_GlobalSymTable.GetIndex(pszTokenName);

	//if not already there in the GlobalSymbolTable, then search the device
	if(-1 == nIndx) 
	{
		hCitemBase*  pIB = NULL;
		string strTokenName = pszTokenName;
		if((SUCCESS == m_pDevice->getItemBySymName(strTokenName,&pIB))
		   && NULL != pIB
		)//stevev 08dec14...parser can't bail on invalid, execute can... && (pIB)->IsValidTest()) 
		{
			itemType_t itemtype = pIB->getIType();
			//Added to check for method also Anil September 16 2005
			if(itemtype == iT_Variable)
			{
			//	bIsVariableItem = true;
				DDitemType = DD_ITEM_VAR;
				tokenSubType = RUL_DD_SIMPLE;	
			}
			else if(itemtype == iT_Method)
			{
				tokenSubType = RUL_DD_METHOD;
				DDitemType = DD_ITEM_METHOD;
			}
			else
			{	//its a non-variable DD item
				//bIsVariableItem = false;
				DDitemType = DD_ITEM_NONVAR;
				tokenSubType = RUL_DD_COMPLEX;			
			}
			tokenType = RUL_DD_ITEM;			
			CToken localToken(pszTokenName, tokenType, tokenSubType, lLineNum);
			localToken.m_bIsGlobal = true; 
			localToken.m_bVarVALIDITY = (pIB)->IsValidTest();//IsValid() is just validity, test tests cached as well
			
			nIndx = m_GlobalSymTable.Insert(localToken);//SymbolTable::Insert makes a copy of the token

		}//end if SUCCESS

	}//endif -1
	return nIndx;
	
}

void MEE::SetVariableValue(const char* pszVariableName, int iValue)
{
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV;
	if((SUCCESS== m_pDevice->getItemBySymName((string)pszVariableName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Index:
			case vT_Enumerated:
			case vT_BitEnumerated:
			case vT_Integer:
			case vT_Unsigned:
				tmpCV = iValue;
				pVar->setDispValue(tmpCV);
				break;
			default:
				//error!!!!
				break;

		}//end switch
		
	}//endif
	 
	//error!!!
}


void MEE::SetVariableValue(const char* pszVariableName, float fValue)
{
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV;
	if((SUCCESS== m_pDevice->getItemBySymName((string)pszVariableName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_FloatgPt:
				tmpCV = fValue;
				pVar->setDispValue(tmpCV);
				break;
			default:
				//error!!!!
				break;

		}//end switch
		
	}//endif
	 
	//error!!!
}

void MEE::SetVariableValue(const char* pszVariableName, double dValue)
{
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV;
	if((SUCCESS== m_pDevice->getItemBySymName((string)pszVariableName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Double:
				tmpCV = dValue;
				pVar->setDispValue(tmpCV);
				break;
			default:
				//error!!!!
				break;

		}//end switch
		
	}//endif
	 
	//error!!!
}


void MEE::SetVariableValue(const char* pszVariableName, char* pszValue)
{
	hCitemBase* pIB = NULL;
	char *szWithoutLanguageCode = NULL;
	RemoveLanguageCode(pszValue,&szWithoutLanguageCode); 
	CValueVarient tmpCV;
	if((SUCCESS== m_pDevice->getItemBySymName((string)pszVariableName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password:
				tmpCV = (string)szWithoutLanguageCode;//Anil July 11 2005 Changed to szWithoutLanguageCode fron pszValue
				pVar->setDispValue(tmpCV);
				break;
			default:
				//error!!!!
				break;

		}//end switch
		
	}//endif

	if(szWithoutLanguageCode)
	{
		delete[] szWithoutLanguageCode;
		szWithoutLanguageCode = NULL;
	}	 
	//error!!!
}

void MEE::SetVariableValue(const char* pszVariableName, bool bValue)
{
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV;
	if((SUCCESS== m_pDevice->getItemBySymName((string)pszVariableName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Boolean:
				tmpCV.vType = CValueVarient::isBool;
				tmpCV.vValue.bIsTrue = (bool)bValue;
				pVar->setDispValue(tmpCV);
				break;
			default:
				//error!!!!
				break;

		}//end switch
		
	}//endif
	 
	//error!!!
}

/*Vibhor 060705: End of Code*/



//Added By Anil July 11 2005 --starts here
//This is the utility Function to take care of the Language Code while doing string operations
void MEE::RemoveLanguageCode(const char* szOriginalString, char** szWithoutLanguageCode)
{	
	int istrLen = strlen(szOriginalString);
	if( (szOriginalString[0] == '|') &&  (szOriginalString[3] == '|') )
	{
		*szWithoutLanguageCode = new char[istrLen - 4 + 1];//4- Lang Code   1- Null char		
		memset(*szWithoutLanguageCode,0,(istrLen - 4 + 1));
		int count = 0;
		for( count = 4; count <istrLen ;count++)
		{
			szWithoutLanguageCode[0][count-4] = szOriginalString[count];
		}
		szWithoutLanguageCode[0][count-4] = '\0';		
	}
	else
	{
		*szWithoutLanguageCode = new char[istrLen + 1];///1 - Null char
		memset(*szWithoutLanguageCode,0,istrLen+1);
		strcpy(*szWithoutLanguageCode,szOriginalString);


	}
	return;

}

//Added By Anil July 11 2005 --Ends here




/******************************************************************************************************/

/*	This Function is for  building the Map which of Primary attributes to its data type and Unique ID
	When I say Primary
		1)This Should not have any further Attribute
		2)its "C" data type has to be defines
	If any new Primary attribute has to be added, Please define Unique ID, before putting in the Map	
	Refer DOT_OP_ATTR Structure defination for more Clarity
	
/************************************************************************************************/


void MEE::BuildDotOpNameToAttrMap()
{
#if 1 //_MSC_VER >= 1300  // HOMZ - port to 2003, VS7 >> prevent error C2552: 'DotOpAttr' : 
	                 //                         non-aggregates cannot be initialized with initializer list
	DotOpAttr[DOT_ID_NONE]      = DOT_OP_ATTR("NONE"		,DOT_ID_NONE	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_LABEL]     = DOT_OP_ATTR("LABEL"		,DOT_ID_LABEL	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_HELP]      = DOT_OP_ATTR("HELP"		,DOT_ID_HELP	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_MIN_VALUE] = DOT_OP_ATTR("MIN_VALUE"	,DOT_ID_MIN_VALUE  ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_MAX_VALUE] = DOT_OP_ATTR("MAX_VALUE"	,DOT_ID_MAX_VALUE  ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_DFLT_VALUE]= DOT_OP_ATTR("DEFAULT_VALUE",DOT_ID_DFLT_VALUE,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_VIEW_MIN]  = DOT_OP_ATTR("VIEW_MIN"	,DOT_ID_VIEW_MIN   ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_VIEW_MAX]  = DOT_OP_ATTR("VIEW_MAX"	,DOT_ID_VIEW_MAX   ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_COUNT]     = DOT_OP_ATTR("COUNT"		,DOT_ID_COUNT	   ,DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_CAPACITY]  = DOT_OP_ATTR("CAPACITY"	,DOT_ID_CAPACITY   ,DOT_TYPE_INTEGER);	
	DotOpAttr[DOT_ID_FIRST]     = DOT_OP_ATTR("FIRST"		,DOT_ID_FIRST	   ,DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_LAST]      = DOT_OP_ATTR("LAST"		,DOT_ID_LAST	   ,DOT_TYPE_INTEGER);
#else
	DOT_OP_ATTR DotOpAttr[] = 
	{
		//  DOT_OP_ATTR{DotOpName				,uiUniqueID					,uiVarType			
		{	DOT_OP_ATTR("LABEL"					,DOT_ID_LABEL				,DOT_TYPE_STRING)	},
		{	DOT_OP_ATTR("HELP"					,DOT_ID_HELP				,DOT_TYPE_STRING)	},
/*		{	DOT_OP_ATTR("DISPLAY_FORMAT"		,DOT_ID_DISPLAY_FORMAT		,DOT_TYPE_STRING)	},
		{	DOT_OP_ATTR("EDIT_FORMAT"			,DOT_ID_EDIT_FORMAT			,DOT_TYPE_STRING)	},
*/
		{	DOT_OP_ATTR("MIN_VALUE"				,DOT_ID_MIN_VALUE			,DOT_TYPE_REAL	)	},
		{	DOT_OP_ATTR("MAX_VALUE"				,DOT_ID_MAX_VALUE			,DOT_TYPE_REAL	)	},
/*		{	DOT_OP_ATTR("SCALING_FACTOR"		,DOT_ID_SCALING_FACTOR		,DOT_TYPE_REAL	)	},
*/
		{	DOT_OP_ATTR("DEFAULT_VALUE"			,DOT_ID_DEFAULT_VALUE		,DOT_TYPE_REAL	)	},
		{	DOT_OP_ATTR("VIEW_MIN"				,DOT_ID_VIEW_MIN			,DOT_TYPE_REAL	)	},
		{	DOT_OP_ATTR("VIEW_MAX"				,DOT_ID_VIEW_MAX			,DOT_TYPE_REAL	)	},
		{	DOT_OP_ATTR("COUNT"					,DOT_ID_COUNT				,DOT_TYPE_INTEGER)	},		
		{	DOT_OP_ATTR("CAPACITY"				,DOT_ID_CAPACITY			,DOT_TYPE_INTEGER)	},
/*sjv added 15aug07*/
		{	DOT_OP_ATTR("FIRST"					,DOT_ID_FIRST				,DOT_TYPE_INTEGER)	},
/*sjv added 15aug07*/			
		{	DOT_OP_ATTR("LAST"					,DOT_ID_LAST				,DOT_TYPE_INTEGER)	} 
//stevev 23jul07 - needs FIRST,  LAST,  X_AXIS, Y_AXIS, member-name <src/wvfrm> 
//														for graph.member-name.X_AXIS.MIN_VALUE
//stevev 23jul07 - remove DISPLAY_FORMAT,EDIT_FORMAT,SCALING_FACTOR
	};
#endif
	int i; // declare out here to (legally) use in both loops
	for( i=0; i<sizeof(DotOpAttr)/sizeof(DotOpAttr[0]); i++ )
	{
		m_MapDotOpNameToAttr[ DotOpAttr[i].strDotOpName ] = DotOpAttr[i];
	}
#define PUSH_IT( a, b ) AvailAttrOP[ a ].push_back( b )
	
	PUSH_IT( iT_Variable,DOT_ID_LABEL );     PUSH_IT( iT_Variable,DOT_ID_HELP);
	PUSH_IT( iT_Variable,DOT_ID_MIN_VALUE ); PUSH_IT( iT_Variable,DOT_ID_MAX_VALUE);
	PUSH_IT( iT_Variable,DOT_ID_DFLT_VALUE );
	
	PUSH_IT( iT_Axis,DOT_ID_LABEL );		 PUSH_IT( iT_Axis,DOT_ID_HELP);
	PUSH_IT( iT_Axis,DOT_ID_MIN_VALUE );	 PUSH_IT( iT_Axis,DOT_ID_MAX_VALUE);
	PUSH_IT( iT_Axis,DOT_ID_VIEW_MIN );		 PUSH_IT( iT_Axis,DOT_ID_VIEW_MAX);

	PUSH_IT( iT_List,DOT_ID_LABEL );		 PUSH_IT( iT_List,DOT_ID_HELP);
	PUSH_IT( iT_List,DOT_ID_COUNT );		 PUSH_IT( iT_List,DOT_ID_CAPACITY);
	PUSH_IT( iT_List,DOT_ID_FIRST );		 PUSH_IT( iT_List,DOT_ID_LAST);

	PUSH_IT( iT_Menu,       DOT_ID_LABEL );	 PUSH_IT( iT_Menu,       DOT_ID_HELP);
	PUSH_IT( iT_Grid,       DOT_ID_LABEL );	 PUSH_IT( iT_Grid,       DOT_ID_HELP);
	PUSH_IT( iT_Image,      DOT_ID_LABEL );	 PUSH_IT( iT_Image,      DOT_ID_HELP);
	PUSH_IT( iT_EditDisplay,DOT_ID_LABEL );  PUSH_IT( iT_EditDisplay,DOT_ID_HELP);
	PUSH_IT( iT_Method,     DOT_ID_LABEL );	 PUSH_IT( iT_Method,     DOT_ID_HELP);

	PUSH_IT( iT_Collection, DOT_ID_LABEL );  PUSH_IT( iT_Collection, DOT_ID_HELP);//members
	PUSH_IT( iT_File,       DOT_ID_LABEL );	 PUSH_IT( iT_File,       DOT_ID_HELP);//members

	PUSH_IT( iT_Chart,      DOT_ID_LABEL );	 PUSH_IT( iT_Chart,   DOT_ID_HELP);//members-source
	PUSH_IT( iT_Graph,      DOT_ID_LABEL );	 PUSH_IT( iT_Graph,   DOT_ID_HELP);//members-waveform
	PUSH_IT( iT_Source,     DOT_ID_LABEL );	 PUSH_IT( iT_Source,  DOT_ID_HELP);//members-numeric
	PUSH_IT( iT_Waveform,   DOT_ID_LABEL );  PUSH_IT( iT_Waveform,DOT_ID_HELP);

	PUSH_IT( iT_ItemArray,  DOT_ID_LABEL );  PUSH_IT( iT_ItemArray,DOT_ID_HELP);//elements
	PUSH_IT( iT_Array,      DOT_ID_LABEL );	 PUSH_IT( iT_Array,    DOT_ID_HELP);//elements
}


/***************************************************************************/
/*	This Function is for  having Secondary attributes in string vector
	When I say Secondary
		1)This Should have further Attribute to be resolved
		2)its "C" data type could not be defined	
/***************************************************************************/

void MEE::BuildSecondaryAttrList()
{
	m_svSecondaryAtt.push_back("X_AXIS");
	m_svSecondaryAtt.push_back("Y_AXIS");
//stevev 23jul07 - only attribute groups that hold specified accessible attributes are allowed here
//                 the following are not allowed.
/* 15aug07 - removed
	m_svSecondaryAtt.push_back("KEY_POINTS");
	m_svSecondaryAtt.push_back("X_VALUES");
	m_svSecondaryAtt.push_back("Y_VALUES");	
*/
}

/**********************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x  Where 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap()) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szTokenName      :DDItem
	pvar			 :Which has to be resolved out of szComplexDDExpre

/**********************************************************************************************/

RETURNCODE MEE::ResolveDDExp(const char* szComplexDDExpre,const char* szTokenName,
																			INTER_VARIANT* pvar)
{
	if(szComplexDDExpre == NULL || szTokenName == NULL)
		return FAILURE;	
	
	char* szLastAttr  = NULL;
	bool bPrimaryAttr = false;

	if(FAILURE == ValidateExp(szComplexDDExpre, &szLastAttr,bPrimaryAttr) )
	{
		if(szLastAttr)
		{
			delete[] szLastAttr;
			szLastAttr = NULL;
		}
		return FAILURE;
	}

	hCitemBase*  pIB = NULL;
	unsigned long iEndofEpression = 0;
	string strLastAttr;
	//If Expression followed by Last dot is Primary, 
	//   I need not include this in Resolution in below for loop 
	if(bPrimaryAttr == true)
	{
		strLastAttr = szLastAttr ;
		iEndofEpression = strlen(szComplexDDExpre) - strlen(szLastAttr) - 1; 

	}
	else
	{
		iEndofEpression = strlen(szComplexDDExpre);;
	}

	if(szLastAttr)
	{
		delete[] szLastAttr;
		szLastAttr = NULL;
	}


	hCitemBase* pIBFinal = NULL;
	if( FAILURE == ResolveExp((const char*) szTokenName,(const char*) szComplexDDExpre,iEndofEpression,&pIBFinal) )
	{
		return FAILURE;
	}
	if(pIBFinal == NULL)
	{
		return FAILURE;

	}
	
	//Here We resolved the szComplexDDExpre excluding Primary attribute,pIBFinal contains the item base ptr to corresponding DDitem
	//if bPrimaryAttr is true then We need to get the DD item attribute value and put it in pvar ( out param)
	if(bPrimaryAttr == true)
	{
		char* szLastAttr = new char[strLastAttr.size()+1];
		strcpy(szLastAttr,strLastAttr.c_str());

		int iReturnValue = GetDDItemAttrValue(pIBFinal,(const char*)szLastAttr,pvar);
		if(szLastAttr)
		{
			delete[] szLastAttr;
			szLastAttr = NULL;
		}
		if(	iReturnValue == FAILURE)
		{
			return FAILURE;
		}	
	}
	else
	{
		//Now, This should be a DD item of type variable , It cannot be other than this
		if(!(pIBFinal->IsVariable()))
		{
			return FAILURE;
		}
		hCVar* phCVar = (hCVar*)pIBFinal;
		//Get the dip value and update pvar
		if( FAILURE == GetDDVarVal(phCVar,pvar) )
		{
			return FAILURE;
		}		
	}//End of else
	
	return SUCCESS;

}

/******************************************************************************************************/

/*	This Function is for resolving the Primary attribute
	
	Function Input Parameters: 
	pIB				: item base ptr to DD item
	szPrimaryAttr   : this Should be one in BuildDotOpNameToAttrMap()
	pvar			 :Which has to be filled after getting the Primary attribute value

/*************************************************************************************************/


RETURNCODE MEE::GetDDItemAttrValue(hCitemBase* pIB, const char* szPrimaryAttr,INTER_VARIANT* pvar)
{hCitemBase* pib = NULL; return GetAttrValue(pIB,(char*)szPrimaryAttr,NULL,pvar,pib);}

RETURNCODE MEE::GetDDItemAttrValue(hCitemBase*    pIB,  const char* pAttrName,
								   CValueVarient* pvar, hCitemBase*& rpIB)
{return GetAttrValue(pIB,(char*)pAttrName,pvar,NULL,rpIB);}



RETURNCODE MEE::GetAttrValue( hCitemBase*     pIB, char* pAttrName,
						      CValueVarient* pVal, INTER_VARIANT* pvar, hCitemBase*& rpIB )
{
	if ( pIB == NULL || pAttrName == NULL || (pVal == NULL && pvar == NULL))
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}
//#			verify valid attribute for item	
	if (AvailAttrOP[pIB->getIType()].size() == 0)
	{// item type has no valid attribute references
		return MEE_ITEM_ATTRIBUTE_MISMATCH;
	}

	string OrigAttrName = pAttrName;
	int  minmaxNumber   = 0;// this must have a value when we have M??_VALUEn 'cause n==0 illegal
	int  strlenmaxvalue = strlen("MAX_VALUE");//Same as strlen(MIN_VALUE)
	//If the char szMinOrMax[10]; is Either MAX_VALUEn or MIN_VALUEn  
	//then hold orig and make char szMinOrMax[10]; as only Either   MAX_VALUE or  MIN_VALUE
	if(	( ( strstr(pAttrName, "MAX_VALUE") ) != NULL || 
		  ( strstr(pAttrName, "MIN_VALUE") ) != NULL   ) &&
		strlen(pAttrName) != strlenmaxvalue   )// isa numbered attribute
	{
		minmaxNumber = atoi(  pAttrName + strlenmaxvalue );
		pAttrName[ strlenmaxvalue ] = '\0';//terminate at numeric
	}

	RETURNCODE     rc = SUCCESS;
	DOT_OP_ITER_t  ID_iter;
	DOT_OP_ATTR    DotOpAtt;
	for ( ID_iter  = AvailAttrOP[pIB->getIType()].begin();
		  ID_iter != AvailAttrOP[pIB->getIType()].end();   ++ID_iter)
	{
		int idNumber = *((int*)&(*ID_iter));	// PAW &(*) added
		if ( DotOpAttr[idNumber].strDotOpName == pAttrName )
		{
			DotOpAtt = DotOpAttr[idNumber];
			break;
		}
	}
	if ( ID_iter == AvailAttrOP[pIB->getIType()].end()) 
	{// we had a meltdown
		return MEE_ITEM_ATTRIBUTE_MISMATCH;
	}


	wstring        tmpStr;
	CValueVarient tmpCV;
	hCVar*        pTmpVar = NULL;
	//depending On the Unique Id Call the apropriate interface	
	switch(DotOpAtt.uiUniqueID)
	{
		case DOT_ID_LABEL:
			{
				rc  = pIB->Label(tmpStr);
				if(rc != SUCCESS)
				{
					return rc;
				}
				if ( pvar )
				{
					*pvar = (wchar_t*)tmpStr.c_str();
				}
				if ( pVal )
				{
					*pVal = (wchar_t*)tmpStr.c_str();
				}
			}
			break;
		case DOT_ID_HELP:
			{
				rc = pIB->Help(tmpStr);
				if(rc != SUCCESS)
				{
					return rc;
				}
				if ( pvar )
				{
					*pvar = (wchar_t*)tmpStr.c_str();
				}
				if ( pVal )
				{
					*pVal = (wchar_t*)tmpStr.c_str();
				}
			}
			break;
/* stevev 15aug07 - 
   not specified:removed:DOT_ID_DISPLAY_FORMAT, DOT_ID_EDIT_FORMAT ,DOT_ID_SCALING_FACTOR  */
		case DOT_ID_MIN_VALUE:
			{
				CValueVarient locVar;
				hCRangeList RangeList;
				RangeList_t::iterator RangeIterator;
				if ( pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric() )
				{
					rc = ((hCNumeric*)pIB)->getMinMaxList(RangeList);
					if(rc != SUCCESS)		return rc;
				}
				else
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a numeric to "
									"resolve MIN_VALUE\n",	pIB->getName().c_str() );
					return MEE_NON_NUMERIC_4_MINMAX;
				}

				if ( minmaxNumber == 0 )
				{
					if (RangeList.size() > 1)
					{
						SDCLOG(CERR_LOG,"Method Reference: WARNING '%s' has %d MIN-MAX entries "
						"but %s was accessed\n",pIB->getName().c_str(), RangeList.size(),
						     OrigAttrName.c_str()  );
					}
					//locVar = RangeList[0].minVal;
					hCRangeItem ri = RangeList.begin()->second;
					locVar = ri.minVal;
				}
				else
				{// we are doing a number MIN_VALUEn
					RangeIterator = RangeList.find(minmaxNumber);
					if ( RangeIterator == RangeList.end() )
					{
						SDCLOG(CERR_LOG,"Method Reference: Did not find MIN_VALUE%d in '%s'\n",
							pIB->getName().c_str(), minmaxNumber  );
						return MEE_MINMAX_FIND_FAILURE;
					}// else we found it
					locVar = (RangeIterator->second).minVal;
				}
				// pVal has our value
				if (pVal) *pVal = locVar;
				if (pvar) *pvar = (double)locVar;
			}
			break;
		case DOT_ID_MAX_VALUE:
			{				
				CValueVarient locVar;
				hCRangeList RangeList;
				RangeList_t::iterator RangeIterator;
				if ( pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric() )
				{
					rc = ((hCNumeric*)pIB)->getMinMaxList(RangeList);
					if(rc != SUCCESS)		return rc;
				}
				else
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a numeric to "
									"resolve MAX_VALUE\n",	pIB->getName().c_str() );
					return MEE_NON_NUMERIC_4_MINMAX;
				}

				if ( minmaxNumber == 0 )
				{
					if (RangeList.size() > 1)
					{
						SDCLOG(CERR_LOG,"Method Reference: WARNING '%s' has %d MIN-MAX entries "
						"but %s was accessed\n",pIB->getName().c_str(), RangeList.size(),
						     OrigAttrName.c_str()  );
					}
					// stevev 18oct07 changed from::> locVar = RangeList[0].maxVal;//assumed which == 0
					hCRangeItem ri = RangeList.begin()->second;
					locVar = ri.maxVal;
				}
				else
				{// we are doing a number MAX_VALUEn
					RangeIterator = RangeList.find(minmaxNumber);
					if ( RangeIterator == RangeList.end() )
					{
						SDCLOG(CERR_LOG,"Method Reference: Did not find MAX_VALUE%d in '%s'\n",
							pIB->getName().c_str(), minmaxNumber  );
						return MEE_MINMAX_FIND_FAILURE;
					}// else we found it
					locVar = (RangeIterator->second).maxVal;
				}
				// pVal has our value
				if (pVal) *pVal = locVar;
				if (pvar) *pvar = (double)locVar;
			}
		break;
		case DOT_ID_DFLT_VALUE://Needs Help TODO
			{	/* modified 16aug07 stevev */
				hCattrVarDefault*  
				phCattrVarDefault = (hCattrVarDefault*)pIB->getaAttr(varAttrDefaultValue);
				if(NULL == phCattrVarDefault)
				{
					return MEE_ATTR_NOT_AVAILABLE;
				}
				if(SUCCESS == phCattrVarDefault->getDfltVal(tmpCV))
				{
					if ( pVal ) *pVal = tmpCV;
					if ( pvar )
					{
						if( tmpCV.vType == CValueVarient::isIntConst)
						{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
							*pvar = (int)tmpCV;
#else
							*pvar = (long)(int)tmpCV;
#endif // __GNUC__
						}
						if( tmpCV.vType == CValueVarient::isFloatConst)
						{
							*pvar = (float)tmpCV;
						}
						if( tmpCV.vType == CValueVarient::isString)
						{
							//tmpStr = (string)tmpCV.sStringVal;	
							//*pvar = (char*)tmpStr.c_str();
							*pvar = (char*)(tmpCV.sStringVal.c_str());
						}
					}
				}
				else
				{
					return MEE_ATTR_NOT_AVAILABLE;
				}				
			}
		break;
		case DOT_ID_VIEW_MIN://Needs Help TODO
			{
/* stevev - 16aug07 - not implemented **********************************************/				
				tmpCV = ((hCaxis*)pIB)->getAttrValue(axisAttrViewMinVal);

				SDCLOG(CERR_LOG,"Method Reference: VIEW_MIN access has not been implemented\n");
				return MEE_UNIMPLEMENTED;
			}
		break;
		case DOT_ID_VIEW_MAX://Needs HelpTODO
			{
/* stevev - 16aug07 - not implemented **********************************************/				
				tmpCV = ((hCaxis*)pIB)->getAttrValue(axisAttrViewMaxVal);

				SDCLOG(CERR_LOG,"Method Reference: VIEW_MAX access has not been implemented\n");
				return MEE_UNIMPLEMENTED;
			}
		break;
		case DOT_ID_COUNT:
			{
				pTmpVar = ((hClist*)pIB)->getAttrPtr(listAttrCount);
				if (pTmpVar == NULL )
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a COUNT\n",
																	pIB->getName().c_str() );
					return MEE_ATTR_NOT_AVAILABLE;
				}
				// else, return the info
				rpIB = pTmpVar;
				tmpCV = pTmpVar->getDispValue();
				minmaxNumber = (int)tmpCV;
				if (pVal) *pVal = tmpCV;
				if (pvar)
				{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
					*pvar = (int)minmaxNumber;
#else
					*pvar = (long int)minmaxNumber;
#endif // __GNUC__
				}
			}
		break;
		case DOT_ID_CAPACITY://Needs Help TODO
			{
				pTmpVar = ((hClist*)pIB)->getAttrPtr(listAttrCapacity);
				if (pTmpVar == NULL )
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a CAPACITY\n",
																	pIB->getName().c_str() );
					return MEE_ATTR_NOT_AVAILABLE;
				}
				// else, return the info
				rpIB  = pTmpVar;
				tmpCV = pTmpVar->getDispValue();
				minmaxNumber = (int)tmpCV;
				if (pVal) *pVal = tmpCV;
				if (pvar)
				{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
					*pvar = (int)minmaxNumber;
#else
					*pvar = (long int)minmaxNumber;
#endif // __GNUC__
				}
			}
		break;
		
		case DOT_ID_FIRST://Needs Help TODO
			{
				rpIB = ((hClist*)pIB)->getAttrPtr(listAttrFirst);
			}
		break;
		case DOT_ID_LAST://Needs Help TODO
			{
				rpIB = ((hClist*)pIB)->getAttrPtr(listAttrLast);
			}
		break;

		default:
			break;
		
	}//end switch				
		
	return SUCCESS;

}

/******************************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x  and 
	also updating the disp value for the same with the proper operator 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szTokenName      :DDItem
	pvar			 :is an in parameter to whhic the dd Item has to be assigned

	if the Lavalue turns out to be a attribute of DDitem , then please through the error , even without resolving.
	
	  

/******************************************************************************************************/


RETURNCODE MEE::ResolveNUpdateDDExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* ivVariableValue,RUL_TOKEN_SUBTYPE	AssignType)
{
	if(szComplexDDExpre == NULL || szTokenName == NULL)
		return FAILURE;

	char* szLastAttr = NULL;
	bool bPrimaryAttr = false;

	if(FAILURE == ValidateExp(szComplexDDExpre, &szLastAttr,bPrimaryAttr) )
	{
		if(szLastAttr)
		{
			delete[] szLastAttr;
			szLastAttr = NULL;
		}
		return FAILURE;
	}
		
	unsigned long iEndofEpression = strlen(szComplexDDExpre);;
	if( true == bPrimaryAttr )
	{
		iEndofEpression = strlen(szComplexDDExpre) - strlen(szLastAttr) - 1; //WS:EPM 10aug07
	}
		
	if(szLastAttr)
	{
		delete[] szLastAttr;
		szLastAttr = NULL;
	}

	/*WS:EPM 10aug07 - moved it up
	if( true == bPrimaryAttr )
	{
		return FAILURE;
	}  end  WS:EPM 10aug07 */


	//No we need to resolve the Coplex Expression in to an Var ptr which is pIBFinal
	//WS:EPM 10aug07 - moved it up
	//unsigned long iEndofEpression = strlen(szComplexDDExpre);;
	hCitemBase* pIBFinal = NULL;
	if( FAILURE == ResolveExp((const char*) szTokenName,(const char*) szComplexDDExpre,iEndofEpression,&pIBFinal) )
	{
		return FAILURE;
	}

	//now we got the final ptr to the variable
	//Cross check whether it is really a variable
	if( (NULL == pIBFinal) || ( !(pIBFinal->IsVariable()) ) )
	{
		return FAILURE;
	}

	CValueVarient tmpCV ;
	hCVar* pVar = (hCVar*)pIBFinal;
	if( FAILURE ==  GetDDVarValNEvalAssType(pVar,ivVariableValue,&tmpCV,AssignType))
	{
		return FAILURE;
	}
	//stevev 18oct07 - scaling is ONLY for display....pVar->setDispValue(tmpCV);
	pVar->setRawDispValue(tmpCV);

	return SUCCESS;

}


/******************************************************************************************************/

/*	This Function is for getting the CgroupItemDescriptor and casting accordingly


/******************************************************************************************************/

RETURNCODE MEE::GetGroupItemPtr(hCitemBase* pIBFinal,int iIndexValue,hCgroupItemDescriptor** pGID)
{
	
	if(pIBFinal == NULL)
		return FAILURE;


	int  rc = SUCCESS;
	itemType_t typeCheck = pIBFinal->getIType();
	switch (typeCheck)
	{
		
		case iT_ItemArray:
			rc= ((hCitemArray*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Collection:
			rc = ((hCcollection*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Array:			
			rc = ((hCarray*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_File:			
			rc = ((hCfile*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Chart:			
			rc = ((hCchart*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Graph:			
			rc = ((hCgraph*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Axis:			
			rc = ((hCaxis*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Waveform:			
			rc = ((hCwaveform*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
		case iT_List:
			rc = ((hClist*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;

			
		default:			
			return FAILURE;
			break;
		
	}//end of switch AssignType
	if ( rc != SUCCESS && *pGID == NULL )// its a ppGID and the function set the contents
	{
		return FAILURE;
	}
	
	return SUCCESS;
}





RETURNCODE MEE::ResolveDDExpForBuiltin(const char* szComplexDDExpre,const char* szTokenName,
														INTER_VARIANT* pvar, hCitemBase** ppDevObjVar)
{

	CValueVarient lcv;
	hCitemBase   *pib = NULL;
	RETURNCODE rc =
	 ResolveComplexReference((char*)szComplexDDExpre, &lcv, pib);
	if (ppDevObjVar) *ppDevObjVar = pib;
	if (pvar) *pvar = lcv;
	return rc;

///////////////////////////////////////////////////////////////////////////////////////////////
	if(szComplexDDExpre == NULL || szTokenName == NULL)
		return FAILURE;

	/*** test ***/


	char* szLastAttr = NULL;
	bool bPrimaryAttr = false;

	if(FAILURE == ValidateExp(szComplexDDExpre, &szLastAttr,bPrimaryAttr) )
	{
		if(szLastAttr)
		{
			delete[] szLastAttr;
			szLastAttr = NULL;
		}
		return FAILURE;
	}
	
	if(szLastAttr)
	{
		delete[] szLastAttr;
		szLastAttr = NULL;
	}

	if( true == bPrimaryAttr )
	{
		return FAILURE;

	}


	//No we need to resolve the Coplex Expression in to an Var ptr which is pIBFinal
	unsigned long iEndofEpression = strlen(szComplexDDExpre);;
	hCitemBase* pIBFinal = NULL;
	if( FAILURE == ResolveExp((const char*) szTokenName,(const char*) szComplexDDExpre,iEndofEpression,&pIBFinal) )
	{
		return FAILURE;
	}

	// now we got the final ptr to the variable
	// Cross check whether  it  is really a variable
	if( (NULL == pIBFinal) || ( !(pIBFinal->IsVariable()) ) )
	{
		return FAILURE;
	}

	//Now, This should be a DD item of type variable , It cannot be other than this		
	hCVar* phCVar = (hCVar*)pIBFinal;
	//Get the dip value and update pvar
	if( FAILURE == GetDDVarVal(phCVar,pvar) )
	{
		return FAILURE;
	}	
	if ( ppDevObjVar != NULL )// stevev 14aug07
	{
		*ppDevObjVar = phCVar;
	}

	return SUCCESS;
}



/************************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x and 
		etiurs the item base  ptr 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() 
																		function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szDDitemName     :DDItem
	iEndofEpression	 :Till what lenght of szComplexDDExpre it needs resolution
	hCitemBase       :Gives back the Itembase ptr which will refer to DD item, Which is after 
																	Resolving szComplexDDExpre	  

/************************************************************************************************/
RETURNCODE MEE::ResolveExp(const char* szDDitemName,const char* szComplexDDExpre,
											unsigned long iEndofEpression,hCitemBase**  pIBFinal)
{
	
	if( (szDDitemName == NULL)||(szComplexDDExpre == NULL))
	{
		return FAILURE;
	}
	hCitemBase*  pIB = NULL;
	string strDDitemName = szDDitemName;
	//get pIB from the szTokenName which is DD item Name
	if((SUCCESS == m_pDevice->getItemBySymName(strDDitemName,&pIB))
	   && NULL != pIB
	   && (pIB)->IsValidTest()) // Invalids are not supported
	{
		*pIBFinal = pIB;		

		// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast to iEndOfEpression>
		for( long int lCount = (long)strlen(szDDitemName); 
					  lCount < (long)iEndofEpression ; lCount++)
		{			
			//Check the Expression has any index ,ie in DDItem[i],   value of i
			if(szComplexDDExpre[lCount] == '[')
			{
				// you got left open backet , which mens u need to get the hCgroupItemDescriptor 
				//																	for the same 
				long int lCurrentPos = lCount;
				while(szComplexDDExpre[lCount] != ']')
				{
					lCount++;
				}				
				char* szIndexValue = new char[lCount - lCurrentPos];
				memset(szIndexValue,0,(lCount - lCurrentPos));
				strncpy(szIndexValue,&szComplexDDExpre[lCurrentPos+1], lCount - lCurrentPos-1);
				szIndexValue[lCount - lCurrentPos -1] = '\0'; 

				//Got the integer value as string So cast it to int
				int iIndexValue = atoi(szIndexValue);// stevev 23jul07 - index is allowed 
													//					to be an expression***
				//Delete the string, No more usefull
				if(szIndexValue)
				{
					delete[] szIndexValue;
					szIndexValue = NULL;
				}

				itemType_t typeCheck = (*pIBFinal)->getIType();
				if(iT_Variable == typeCheck)
				{
					hCVar* pVar = (hCVar*)pIBFinal;
					int iVarTpe = pVar->VariableType();
					if(vT_BitEnumerated == iVarTpe)
					{
						// stevev 23jul07 - this entire check ends in empty?????????????????????
					}

				}
				//With this index, query for the hCgroupItemDescriptor, 
				int  rc = SUCCESS;
				hCgroupItemDescriptor*  pGID = NULL ;
				// get the GetGroupItemPtr, if the index is out if range it should return failure  
				if(GetGroupItemPtr((*pIBFinal),iIndexValue,&pGID) == FAILURE)	
				{		
					RAZE( pGID ); // stevev 21feb07 - it may have been alloc'd in getbyindex in 
								  //											getgroupitemptr
					//Some time it may happen that this index is out of range					
					return FAILURE;
				}
				
				//We got valid hCgroupItemDescriptor from this index , so get fresh pIBFinal
				*pIBFinal = NULL;
				hCreference ClocalRef(pGID->getRef());
				rc = ClocalRef.resolveID((*pIBFinal));
				RAZE( pGID ); // stevev 21feb07 - was alloc'd in getbyindex in getgroupitemptr
				//again resolution of ClocalRef may faol
				if ( rc != SUCCESS)
				{
					return FAILURE;
				}
			
			}
			//or Case may be member of DD item or Secondary attribute or one of LAST, FIRST
			//This list may increase as more attribute gets added
			//For Primary attribute, Update BuildDotOpNameToAttrMap() function
			//Secondary attribute Update in BuildSecondaryAttrList()
			//For thing like  LAST and FIRST , we need to really look in to Spec as it is little 
			//special case 
			else if(szComplexDDExpre[lCount] == '.')
			{
				//You got dot which mens this should be either Member name 
				//							( currently not implimented) or Secondary attrubute
				long int lCurrentPos = lCount;
				lCount++;
				while((szComplexDDExpre[lCount] != '[') &&
					  (szComplexDDExpre[lCount] != '.') &&
					  (szComplexDDExpre[lCount] != NULL)   )
				{
					lCount++;
				}

				char* szStringAfterDot = new char[lCount - lCurrentPos];
				memset(szStringAfterDot,0,(lCount - lCurrentPos));
				strncpy(szStringAfterDot,&szComplexDDExpre[lCurrentPos+1], lCount - lCurrentPos-1);
				szStringAfterDot[lCount - lCurrentPos -1] = '\0'; 
				string strAfterDot = szStringAfterDot;
				lCount--;
				if(szStringAfterDot)
				{
					delete[] szStringAfterDot;
					szStringAfterDot = NULL;
				}

				//Or this could be any one of this
				if(strAfterDot ==  "LAST" || strAfterDot ==  "FIRST")
				{
					if(iT_List != (*pIBFinal)->getIType())
					{
						//This is an eror as other than list has LAST dot operator
						//TODO  call the Correspondin API
							return FAILURE;
					}
					
					unsigned int iListCount = 0;
					if(strAfterDot ==  "LAST")
					{
						hClist* pList = (hClist*)(*pIBFinal);
						iListCount = pList->getCount();
						iListCount--;
					}
					//With this index, query for the hCgroupItemDescriptor, 
					int  rc = SUCCESS;
					hCgroupItemDescriptor*  pGID = NULL ;
					if(GetGroupItemPtr((*pIBFinal),iListCount,&pGID) == FAILURE)	
					{			
						RAZE( pGID ); // stevev 21feb07 - it may have been alloc'd in getbyindex 
									  // in getgroupitemptr
						//Some time it may happen that this index is out of range					
						return FAILURE;
					}					
					//We got valid hCgroupItemDescriptor from this index , so get fresh pIBFinal
					*pIBFinal = NULL;
					hCreference ClocalRef(pGID->getRef());
					rc = ClocalRef.resolveID((*pIBFinal));	
					RAZE( pGID ); // stevev 21feb07 - was alloc'd in getbyindex in getgroupitemptr
					//again resolution of ClocalRef may faol
					if ( rc != SUCCESS)
					{
						return FAILURE;
					}
							
				
				}
				else if(strAfterDot == "KEY_POINTS")
				{
					long int lCurrentPos = lCount+1;
					//if it is KEY_POINTS, it should be either  KEY_POINTS.X_VALUES  or 
					//											KEY_POINTS.Y_VALUES
					//if not then error
					if(szComplexDDExpre[lCount] != '.')
					{
						return FAILURE;
					}
					while(szComplexDDExpre[lCount] != '[' || szComplexDDExpre[lCount] != '.')
					{
						lCount++;
					}
					char* szAfterKEYPOINTS = new char[lCount - lCurrentPos];
					memset(szAfterKEYPOINTS,0,(lCount - lCurrentPos));
					strncpy(szAfterKEYPOINTS,  &szComplexDDExpre[lCurrentPos+1], 
															               lCount - lCurrentPos-1);
					szAfterKEYPOINTS[lCount - lCurrentPos -1] = '\0'; 
					string strAfterKEYPOINTS = szStringAfterDot;
					//if it is any  of KEY_POINTS.X_VALUES  or KEY_POINTS.Y_VALUES, then error
					if( (strAfterKEYPOINTS != "X_VALUES") || (strAfterKEYPOINTS != "Y_VALUES") )
					{
						//Deallocate all memory
						if(szAfterKEYPOINTS)
						{
							delete[] szAfterKEYPOINTS;
							szAfterKEYPOINTS = NULL;
						}
						return FAILURE;
					}
					//TODO  call the Correspondin API
					if(szAfterKEYPOINTS)
					{
						delete[] szAfterKEYPOINTS;
						szAfterKEYPOINTS = NULL;
					}

				}
				else
				{
					//This could be member of Collection
					//TODO  call the Correspondin API
					//With this index, query for the hCgroupItemDescriptor, 
					int  rc = SUCCESS;
					hCgroupItemDescriptor*  pGID = NULL ;
					hCgrpItmBasedClass *phCgrpItmBasedClass = (hCgrpItmBasedClass*)(*pIBFinal);
					if(phCgrpItmBasedClass->getByName(strAfterDot, &pGID) == FAILURE)	
					{				
						//Some time it may happen that this index is out of range					
						return FAILURE;
					}					
					//We got valid hCgroupItemDescriptor from this index , so get fresh pIBFinal
					*pIBFinal = NULL;
					
					if (pGID == NULL)
						return FAILURE;

					hCreference ClocalRef(pGID->getRef());
					rc = ClocalRef.resolveID((*pIBFinal));	
					//again resolution of ClocalRef may faol
					if ( rc != SUCCESS)
					{
						return FAILURE;
					}
							

				}
			}
			
		}
	}

	return SUCCESS;

}




/******************************************************************************************************/

/*	This Function is for Validating the expression involving DD items
	for resolving the DD complex Expression such as  DDItem[i].member[j].x 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szLastAttr	     :out param which is "x"
	bPrimaryAttr	 : out param , is true if it is primary attribute 
	Retun vale		 :returns false is szLastAttr is secondary attribute
	
/******************************************************************************************************/
RETURNCODE MEE::ValidateExp(const char* szComplexDDExpre, char** szLastAttr,bool &bPrimaryAttr)
{
	if(szComplexDDExpre == NULL)
	{
		return FAILURE;
	}

	int iDDExpreLen = strlen(szComplexDDExpre);
	int iDotCount = 0;
	int iLastDotPos;
	//Get the position of last dot operator , if it is present
	for(int i = 0; i < iDDExpreLen ; i++)
	{
		if( szComplexDDExpre[i] == '.')
		{
			iDotCount++;
			iLastDotPos = i;
		}
	}
	
	//Get the Expression followed by Last dot	
	if(iDotCount != 0)
	{
		*szLastAttr = new char[iDDExpreLen - iLastDotPos];
		strncpy(*szLastAttr,&szComplexDDExpre[iLastDotPos+1], iDDExpreLen - iLastDotPos - 1) ;
		(*szLastAttr)[iDDExpreLen - iLastDotPos - 1] = '\0';
		char *szTemp = NULL;
		
		//if Expression followed by Last dot is secondary attribute , then it is not valid !! 
		//So return FAILURE
		string strDotExpression = *szLastAttr;
		for(int i = 0; i < (int)m_svSecondaryAtt.size() ; i++)  	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		{
			if(strDotExpression == m_svSecondaryAtt[i])
			{
				return FAILURE;
			}
		}
		//Before Checking it against primary attribute, because it may happen that it is MAX_VALUEn or MIN_VALUEn
		if(	( szTemp = strstr(*szLastAttr, "MAX_VALUE") ) != NULL || ( szTemp = strstr(*szLastAttr, "MIN_VALUE") ) != NULL )
		{
			char szMinOrMax[10];
			//As strlen(MIN_VALUE) is same as strlen("MAX_VALUE") only ine is Considered
			strncpy( szMinOrMax, *szLastAttr , strlen("MAX_VALUE"));
			szMinOrMax[strlen("MAX_VALUE")] = '\0';
			strDotExpression = szMinOrMax;

		}		
		//Check whether it is primary attribute
		m_MapDotOpNameToAttrIter = m_MapDotOpNameToAttr.find(strDotExpression);
		if(m_MapDotOpNameToAttrIter == m_MapDotOpNameToAttr.end())
		{
			//it is not a primary attribute. Good!! Reduces my work of resolution in latter part
			bPrimaryAttr = false;						
		}
		else
		{
			//it is a primary attribute . Not bad!! I need to take care of this in latter part
			bPrimaryAttr = true;
		}		
	}
	return SUCCESS;
}


/******************************************************************************************************/

/*	This Function is for To Get the DDexpression nalue and also Evalute the Assignment statement

	pIBFinal		 :ptr to an DD item of type variable
	ivVariableValue	 :Current var value
	CValueVarient	 :Value after evaluating 
	AssignType		 :Assignment type	
/******************************************************************************************************/
RETURNCODE MEE::GetDDVarValNEvalAssType(hCVar*  pIBFinal,INTER_VARIANT* ivVariableValue,CValueVarient* tmpCV,RUL_TOKEN_SUBTYPE AssignType)
{

	if( NULL == pIBFinal )
	{
		return FAILURE;
	}

	hCVar* pVar = (hCVar*)pIBFinal;	
	string tmpStr;
	//Get the current value Which may be needed if it has += kind of operation
	CValueVarient cvCurrentValue= pVar->getDispValue();				
	//Assgn properly depending on DD var type
	switch(pVar->VariableType())
		{
			case vT_Index:
			case vT_Enumerated:
			case vT_BitEnumerated:
			case vT_Integer:
			case vT_Unsigned:
			case vT_TimeValue: // stevev added 16nov10
				{
					//WHS EP June17-2008 This is in the spirit of less-is-more... let the inter_variant figure out what the capacity of the variable is.
					__int64 iValue = (__int64)(*ivVariableValue);
					__int64 iCurrent = (__int64)cvCurrentValue;

					//check with the Valid Operators
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{
								switch( ivVariableValue->GetVarType() )
								{
									case RUL_ULONGLONG:
										*tmpCV = (UINT64)(*ivVariableValue);
										break;
									default:
										*tmpCV = iValue;
										break;
								}
								break;
							}
						case RUL_PLUS_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue + (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent + iValue ;
								}
								break;
							}
						case RUL_MINUS_ASSIGN:
							{	
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue - (UINT64)(*ivVariableValue);
								}
								else
								{							
									*tmpCV = iCurrent - iValue ;
								}
								break;
							}
						case RUL_DIV_ASSIGN:
							{	
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									UINT64 ullDenominator = (UINT64)(*ivVariableValue);
									if( ullDenominator != 0 )
								{
										*tmpCV = (UINT64)cvCurrentValue / ullDenominator;
								}
								}
								else
								{		
									if( iValue != 0 )
									{
										*tmpCV = iCurrent / iValue;	
									}
								}
								break;
							}
						case RUL_MUL_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue * (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent * iValue;
								}
								break;
							}
						case RUL_MOD_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									UINT64 ullDenominator = (UINT64)(*ivVariableValue);
									if( ullDenominator != 0 )
								{
										*tmpCV = (UINT64)cvCurrentValue % ullDenominator;
								}
								}
								else
								{
									if( iValue != 0 )
									{
										*tmpCV = iCurrent % iValue;
									}
								}
								break;
							}			
						case RUL_BIT_AND_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue & (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent & iValue;
								}
								break;
							}
						case RUL_BIT_OR_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue | (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent | iValue;
								}
								break;
							}
						case RUL_BIT_XOR_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue ^ (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent  ^  iValue;
								}
								break;
							}
						case RUL_BIT_RSHIFT_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue >> (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent  >> iValue;
								}
								break;
							}
						case RUL_BIT_LSHIFT_ASSIGN:
							{
								if( ivVariableValue->GetVarType() == RUL_ULONGLONG )
								{
									*tmpCV = (UINT64)cvCurrentValue << (UINT64)(*ivVariableValue);
								}
								else
								{
									*tmpCV = iCurrent << iValue;	
								}
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType								
				}
				break;
			case vT_FloatgPt:
				{
					float fValue;
/* stevev 24oct05 does not convert type!!		ivVariableValue->GetValue(&fValue,RUL_FLOAT);
** changed to:  */
					fValue = (float)(*ivVariableValue);
					float fCurrent = (float)cvCurrentValue ;
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{
								*tmpCV = fValue;
								break;
							}
						case RUL_PLUS_ASSIGN:
							{
								*tmpCV = fCurrent + fValue ;								
								break;
							}
						case RUL_MINUS_ASSIGN:
							{								
								*tmpCV = fCurrent - fValue ;								
								break;
							}
						case RUL_DIV_ASSIGN:
							{
								if( fValue != 0.0 )
								{
								*tmpCV = fCurrent /fValue;								
								}								
								break;
							}
						case RUL_MUL_ASSIGN:
							{
								*tmpCV = fCurrent *fValue;
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType
					
				}
				break;
			case vT_Double:
				{
					double dValue;
/* stevev 24oct05 does not convert type!!		ivVariableValue->GetValue(&dValue,RUL_FLOAT);
** changed to:  */
					dValue = (double) (*ivVariableValue);
					double dCurrent = (double)cvCurrentValue ;
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{
								*tmpCV = dValue;
								break;
							}
						case RUL_PLUS_ASSIGN:
							{
								*tmpCV = dCurrent + dValue ;								
								break;
							}
						case RUL_MINUS_ASSIGN:
							{								
								*tmpCV = dCurrent - dValue ;								
								break;
							}
						case RUL_DIV_ASSIGN:
							{
								if( dValue != 0.0 )
								{
								*tmpCV = dCurrent /dValue;								
								}
								break;
							}
						case RUL_MUL_ASSIGN:
							{
								*tmpCV = dCurrent *dValue;
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType
						
				}
				break;
			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password: //Added By Anil 11th December 2005
				{
					
					char *szValue = NULL;
					ivVariableValue->GetStringValue(&szValue,RUL_CHARPTR);
					char *szWithoutLanguageCode = NULL;					
					RemoveLanguageCode(szValue,&szWithoutLanguageCode);
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{
								*tmpCV = (string)szWithoutLanguageCode;
								break;
							}
						case RUL_PLUS_ASSIGN:
							{								
								//We may need to remove the Language code before adding
								string strCurrent = (string)cvCurrentValue;
								char* szCurrent = new char[ strCurrent.length() + 1 ];
								strcpy(szCurrent,strCurrent.c_str());

								char *szCurrentWithoutLangCode = NULL;
								RemoveLanguageCode(szCurrent,&szCurrentWithoutLangCode);

								*tmpCV = (string)szCurrentWithoutLangCode + (string)szWithoutLanguageCode;
								if(szCurrent)
								{
									delete[] szCurrent;
									szCurrent = NULL;
								}
								if(szCurrentWithoutLangCode)
								{
									delete[] szCurrentWithoutLangCode;
									szCurrentWithoutLangCode = NULL;
								}
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType					
					

					if(szValue)
					{
						delete[] szValue;
						szValue = NULL;
					}
					if(szWithoutLanguageCode)
					{
						delete[] szWithoutLanguageCode;
						szWithoutLanguageCode = NULL;
					}
					
				}
				break;
			case vT_Boolean:
				{
					bool bValue;
/* stevev 24oct05 does not convert type!!		ivVariableValue->GetValue(&bValue,RUL_BOOL);
** changed to:  */
					bValue = (bool) (*ivVariableValue);
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{
								*tmpCV = bValue;								
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType					
				}
				break;

			//Added  Anil 12 December 2005
			case vT_HartDate:
				{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.

	Note: Compiler errors out on assignment to a long under RUL_ASSIGN below.
*/
#if defined(__GNUC__)
					int lCurrDate = (int)(*ivVariableValue);
#else
					long  lCurrDate = (int)(*ivVariableValue);
#endif
				
					switch (AssignType)
					{
						case RUL_ASSIGN:
							{																
								*tmpCV = lCurrDate;								
								break;
							}
						default:
							{
								return FAILURE;
								break;
							}					
					}//end of switch AssignType	

				
				}
				break;
			default:
				break;
			
		}//end switch
	return SUCCESS;

}



//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Helper function to get the Value of the DD item of type variable 
RETURNCODE MEE::GetDDVarVal(hCVar*  phCVar,INTER_VARIANT* pvar)
{

	if( NULL == phCVar )
	{
		return FAILURE;
	}
	CValueVarient tmpCV = phCVar->getDispValue();				
	string tmpStr;	
	
	//check for the variable type and then get the value accordingly
	switch(phCVar->VariableType())
	{
		case vT_Index:
		case vT_Enumerated:
		case vT_BitEnumerated:
		case vT_Integer:
		case vT_Unsigned:
		case vT_TimeValue:		/* added stevev 14aug08 to allow time to come into integer */
			{
				if( tmpCV.vType == CValueVarient::isVeryLong )
				{
					*pvar = (__int64)tmpCV;
				}
				else
				{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
					*pvar = (int)tmpCV;
#else
					*pvar = (long)(int)tmpCV;	
#endif // __GNUC__
				}
			}
			break;
		case vT_FloatgPt:
			{
				*pvar = (float)tmpCV;															
				
			}
			break;
		case vT_Double:
			{
				*pvar = (double)tmpCV;//WS:EPM 10aug07																			
				
			}
			break;
		case vT_Ascii:
		case vT_PackedAscii:
		case vT_Password: //Anil Added on 12 December 2005
			{
				tmpStr = (string)tmpCV;
				*pvar = (char*)tmpStr.c_str();
			}
			break;
		case vT_Boolean:
			{
				bool bTemp = false; 
				if( tmpCV.valueIsZero() )
				{
					bTemp=true;
				}
				*pvar = bTemp;						
			}
			break;
		case vT_HartDate:
			{
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
				*pvar = (int)tmpCV;
#else
				*pvar = (long)(int)tmpCV;
#endif // __GNUC__
			}
			break;
		default:
			break;
		
	}//end switch

	return SUCCESS;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This method is called by the DDComplex Expression with the Type DD_METHOD
//METHOD_ARG_INFO_VECTOR is a vector of Method argument, which contails all the related info regarding the Method parameter
//vectInterVar-- Value of the each Argument passed if it is passed by value and 
//should be filled by by interpreted vistor if it is passed by reference
//This function uses all the Helper functions for easy flow--this will easy for debugging in case of any issues
//szTokenName -- is the Called Method Name
//szComplexDDExpre- is the Called meth name with argument name
RETURNCODE MEE::ResolveMethodExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar,vector<INTER_VARIANT>* vectInterVar,METHOD_ARG_INFO_VECTOR* vectMethArgInfo)
{	

	hCitemBase*  pIB = NULL;
	string strDDitemName = szTokenName;
	long lMethodItemId =0;
	ParamList_t TempParamList;
	hCmethodParam phCmethodReturnValue;
	//get pIB from the szTokenName -- which is the Method name
	if((SUCCESS == m_pDevice->getItemBySymName(strDDitemName,&pIB))
	   && NULL != pIB
	   && (pIB)->IsValidTest()) // Invalids are not supported
	{
		hCmethod* phCmethod = (hCmethod*)pIB ;
		lMethodItemId = phCmethod->getID();		
		//Get the Parameter list which is used latter to fill the METHOD_ARG_INFO_VECTOR
		if(FAILURE == phCmethod->getMethodParams(TempParamList))
		{
			return FAILURE;
		}
		//Get the return type Parameter : which is used latter to fill the METHOD_ARG_INFO_VECTOR for return type
		if(FAILURE == phCmethod->getMethodType(phCmethodReturnValue) )
		{
			return FAILURE;
		}

	}
	else
	{
		return FAILURE;
	}
	
	vector<string> strvCallerArgList;
	GetCallerArgList(vectMethArgInfo,&strvCallerArgList);

	//First level of check to to make sure that the Number of Parameters passes are same as the Number of Args in the methods!!
	if( vectMethArgInfo->size() != TempParamList.size() )
	{
		return FAILURE;
	}
	
	// Validate the Data type and other stuffs--Please go inside this method to understand
	if( FAILURE == ValidateMethodArgs( vectMethArgInfo,&TempParamList))
	{
		return FAILURE;
	}

	//Update the caller Arg info
	if( FAILURE == UpdateCalledMethArgInfo( vectMethArgInfo,&TempParamList , &strvCallerArgList ))
	{
		return FAILURE;
	}

	//Update the retun Arg info
	if( FAILURE == UpdateRetunMethArgInfo( vectMethArgInfo,&phCmethodReturnValue,vectInterVar) )
	{
		return FAILURE;
	}
	DEBUGLOG(CLOG_LOG,"Method-> Calling Method.\n");
	//Now call the one meth and then instantiate the Separate interpreter for this called method
	IncreaseOneMethRefNo();
	OneMeth *pMeth = new OneMeth();
	bool bRet = pMeth->ExecuteMethod(this,m_pDevice,lMethodItemId,vectMethArgInfo,vectInterVar);
	DecreaseOneMethRefNo();
	DEBUGLOG(CLOG_LOG,"Method<- Returning Method.\n");
	
	if(pMeth)
	{
		delete pMeth;
		pMeth  = NULL;
	}

	if(bRet == true)
		return SUCCESS;
	else
	{
		return FAILURE;
	}

	return SUCCESS;

}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This is an Overaloaded function and Copy cat of the earlier but with two additional Parameter
//METHOD_ARG_INFO_VECTOR - mContains the all the Argument info of the callded method
//vectInterVar -- Contains the value of the argument passed
//These two additional parameter is passed till Parser level
bool OneMeth::ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId,METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar,long lVarItemId)
{

	if (pDevice == NULL)
	{
		return false;
	}

	if(0 != lVarItemId)
	{
		m_lPrePostItemId = lVarItemId;
	}

	m_pMEE = pMEE; //Initialize the MEE pointer;

	m_pDevice = pDevice;

//	pDevice->cacheDispValues(); 

	INTERPRETER_STATUS intStatus = ExecMethod(lMethodItemId,vectMethArg,vectInterVar);

	if (intStatus != INTERPRETER_STATUS_OK)
	{
//		m_bSaveValues = false;
		// replaced by below----HandleSaveValues();
//		pDevice->uncacheDispVals(m_bSaveValues);

		return false;
	}

	if (m_bMethodAborted == true || m_pMEE->latestMethodStatus == me_Aborted)
	{
		for( ListIterator = abortMethodList.begin();ListIterator != abortMethodList.end();ListIterator++ )
		{
			long lQueueMethodId = *ListIterator;
			intStatus = ExecMethod(lQueueMethodId);
			if (intStatus != INTERPRETER_STATUS_OK)
			{
				//m_bSaveValues = false;
				// replaced by below----HandleSaveValues();
				//pDevice->uncacheDispVals(m_bSaveValues);
				return false;
			}
		}
		/*just moving it out of the loop to fix the issues when two methods cannot abort properly
		*/
		if ( m_pMEE != NULL )
		{
			m_pMEE->latestMethodStatus = me_Aborted;
		}
	}

	// replaced by below----HandleSaveValues();
	//pDevice->uncacheDispVals(m_bSaveValues);
	if(m_bMethodAborted == true)
		return false;


	return true;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This is an Overaloaded function and Copy cat of the earlier but with two additional Parameter
//METHOD_ARG_INFO_VECTOR - mContains the all the Argument info of the callded method
//vectInterVar -- Contains the value of the argument passed
//These two additional parameter is passed till Parser level

INTERPRETER_STATUS	OneMeth::ExecMethod(long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar)
{
	/* First check if the Item Id is valid */
	RETURNCODE rc = SUCCESS;
	hCitemBase*  p_ib = NULL;
	hCmethod*    p_m  = NULL;
	rc = m_pDevice->getItemBySymNumber(lMethodItemId, &p_ib);
	if (!(rc == SUCCESS && p_ib != NULL && p_ib->getIType() == iT_Method))
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	m_lMethodItemId = lMethodItemId;

	p_m = (hCmethod*)p_ib;
	int   sLen = 0;
	char* pS = p_m->getDef(sLen);

	if (pS == NULL)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	if (strlen(pS) <= 0)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	char pbyData[1] = "";//[10000] = "";
	char pbySymbolTable[10000] = "";

	AllocLibrary();
	m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,this);
	m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,m_lPrePostItemId); 


	if (m_bMethodAborted == true)
	{
		this->m_pBuiltinLib->m_AbortInProgress = true;
	}

	INTERPRETER_STATUS intStatus =
		m_pInterpreter->ExecuteCode (m_pBuiltinLib,pS,"Test",pbyData, pbySymbolTable,m_pMEE,vectMethArg,vectInterVar);
	if (intStatus != INTERPRETER_STATUS_OK)
	{
		switch (intStatus)
		{
			case INTERPRETER_STATUS_INVALID:
				DisplayMessage("**** Internal Error: ****\nInterpreter State Unknown !");
				FreeLibrary();
				return INTERPRETER_STATUS_INVALID;
			case INTERPRETER_STATUS_PARSE_ERROR:
				DisplayMessage("**** Internal Error: ****\nMethod code has parsing errors !");
				FreeLibrary();
				return INTERPRETER_STATUS_PARSE_ERROR;
			case INTERPRETER_STATUS_EXECUTION_ERROR:
				DisplayMessage("**** Internal Error: ****\nError in method execution !");
				FreeLibrary();
				return INTERPRETER_STATUS_EXECUTION_ERROR;
			case INTERPRETER_STATUS_UNKNOWN_ERROR:
				DisplayMessage("**** Internal Error: ****\nInterpreter State Unknown !");
				FreeLibrary();
				return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}
	}

  	FreeLibrary();
	return intStatus;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Helper function  ot get the Caller Argument name list which ar estored in the vector
RETURNCODE MEE::GetCallerArgList(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,vector<string>* strvCallerArgList)
{
	//Loop through each of the entries in the 
	int iSize = vectMethArgInfo->size();
	for(int iCount = 0; iCount < iSize ; iCount++)
	{
		METHOD_ARG_INFO* cMethodArgInfo = &(*vectMethArgInfo)[iCount] ;
		if( cMethodArgInfo )
		{
		strvCallerArgList->push_back(cMethodArgInfo->GetCallerArgName());
		}// else ??? error, logit?????

	}
	return SUCCESS;
}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This one I wrote in the begining and just retained. Gets the Caller Argument list, which would have filled in the METHOD_ARG_INFO_VECTOR earlier 
RETURNCODE MEE::GetCallerArgList(const char* szDDitemName,const char* pszComplexDDExpre,vector<string>* strvCallerArgList)
{

	
	int iLeftPeranthis = 0;
	bool bValidMethodCall = false;
	long int i = strlen(szDDitemName);
	//Checkk for the Valid Method call, 
	//ie Method call should Start and end with open and Close Parenthesis respectively
	for(; i< (long)strlen(pszComplexDDExpre); i++) 	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		if(pszComplexDDExpre[i] == '(')
		{
			bValidMethodCall = true;
			iLeftPeranthis =1;
			i++;
			break;
		}
	}
	if( bValidMethodCall ==  false)
	{
		return false;
	}		

	//Now strat extracting the each Argument name and push it in the strvCallerArgList vector
	int iNoOfchar = 0;
	long int lstlen = strlen(pszComplexDDExpre);
	for(; i< (lstlen) ; i++)
	{
		//Look for the space and do not count
		if( (' ' != pszComplexDDExpre[i]) )
			
		{
			iNoOfchar++ ;
		}			
		//If u find ')', reduce the iLeftPeranthis and 
		//when u go out of this loop iLeftPeranthis dhould be zero
		if(')' == pszComplexDDExpre[i])
		{
			iLeftPeranthis--;

		}
		//If u find '(', increase the iLeftPeranthis and 
		//when u go out of this loop iLeftPeranthis dhould be zero
		if('(' == pszComplexDDExpre[i])
		{
			iLeftPeranthis++;

		}
		//if it is , or last ")", in that case iLeftPeranthis is zero, then for the arg name
		if((pszComplexDDExpre[i] == ',') || (0 == iLeftPeranthis))
		{
			//do insert here, Get the start pos of the arg name
			int istartPosOfPassedItem = 0 ;
			iNoOfchar--;//Because ; or ) is included
			int iNoOfSpaces = 0;
			//It may so happen that for the arg there are space before , or ')'
			//EG: ( ArgnameOne                  ,   argName2        ), thats why this below loop
			for(int x = i-1; ;x--) 
			{
				if(' ' == pszComplexDDExpre[x])
				{
					iNoOfSpaces++;

				}
				else
				{
					break;
				}

			}
			//Get the strating position and Char count
			istartPosOfPassedItem = i - iNoOfchar - iNoOfSpaces ;
			int iCount = iNoOfchar + 1 ;
			char* pchDecSource = new char[ iCount ];
			memset(pchDecSource,0,iCount);				
			strncpy(pchDecSource,(const char*)&pszComplexDDExpre[istartPosOfPassedItem],iNoOfchar);
			pchDecSource[iCount - 1] = '\0';
			//Push this on the vector list
			strvCallerArgList->push_back(pchDecSource);
			if(pchDecSource)
			{
				delete[] pchDecSource;
				pchDecSource = NULL;
			}
			iNoOfchar = 0;

		}
		if(0 == iLeftPeranthis)
			break;
	}
	return SUCCESS;

}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This function is to validate the methos Agrument, basically the data type of the Arguement
//Please refer the below one to how the maping is done from the Param list got from the Method
//Class to the interpreter data type, so basically these should match when one method calls the other
//i.e from the the caller, data type which he is passed should match to the called method
RETURNCODE MEE::ValidateMethodArgs(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,ParamList_t* TempParamList)
{
	if(vectMethArgInfo->size() != TempParamList->size() )
	{
		//return failure as it is not a valid arg list
		return FAILURE;
		
	}
	unsigned int uiNoOfArgs = vectMethArgInfo->size();
	//Extract all the Method parameters from the Parameter list
	for(unsigned int i = 0; i< uiNoOfArgs ; i++)
	{
		RUL_TOKEN_TYPE eTokenType;
		RUL_TOKEN_SUBTYPE eTokenSubType;
		METHOD_ARG_INFO cMethodArgInfo;
		hCmethodParam chCmethodParam;
		//Get the hcMetod param for each param
		chCmethodParam = (*TempParamList)[i];
		cMethodArgInfo = (*vectMethArgInfo)[i];
		//Chech the data type paased and Data type Defined in the Method

		//Below is the mapping from DevService Variable type to interpreter Var type
		/*enum methodVarType_e
		{
			methodVarVoid--------------RUL_NULL	
			
			methodVarChar--------------RUL_CHAR
			methodVarShort-------------RUL_CHAR

			methodVarLongInt-----------RUL_INT
			methodVarFloat-------------RUL_FLOAT
			methodVarDouble------------RUL_DOUBLE
			methodVar_U_Char-----------?
			methodVar_U_Short----------?
			methodVar_U_LongInt--------?
			methodVarInt64-------------?
			methodVar_U_Int64----------?
			methodVarDDString----------RUL_DD_STRING
			methodVarDDItem------------RUL_DD_COMPLEX //I would set this!!
			methodVar_Unknow-----------RUL_NULL
		}*/
		//
		//Now Compare with the the passed type
		switch(chCmethodParam.getType())
		{
			case methodVarChar:
			// Walt EPM 08sep08 - added/modified
				{
					eTokenSubType = RUL_CHAR_DECL;
				}
				break;
			case methodVar_U_Char:
				{
					eTokenSubType = RUL_UNSIGNED_CHAR_DECL;
				}
				break;
			case methodVarShort:
				{
					eTokenSubType = RUL_SHORT_INTEGER_DECL;
				}
				break;

			case methodVar_U_Short:
				{
					eTokenSubType = RUL_UNSIGNED_SHORT_INTEGER_DECL;
				}
				break;
			case methodVar_U_LongInt:			
				{
					eTokenSubType = RUL_UNSIGNED_INTEGER_DECL;
				}
				break;
			case methodVar_U_Int64:
			case methodVarInt64:			
				{
					eTokenSubType = RUL_LONG_LONG_DECL;
				}
				break;
				// Walt EPM 08sep08 - finish added / modified

			case methodVarLongInt:			
				{
					eTokenSubType = RUL_INTEGER_DECL;
				}
				break;

			case methodVarDouble:
				{
					eTokenSubType = RUL_DOUBLE_DECL;//WS:EPM 10aug07
				}
				break;

			case methodVarFloat:			
				{
					eTokenSubType = RUL_REAL_DECL;
				}
				break;

			case methodVarDDString:			
				{
					eTokenSubType = RUL_DD_STRING_DECL;
				}
				break;
			case methodVarDDItem:
				{
					eTokenSubType = RUL_DD_COMPLEX;
				}
				break;
		
			default:
				{
					
				//error!!!!
				}
				break;

		}//end switch

		//If the token Sub type is not matching, then it is an error
		// Walt EPM 08sep08 - added a couple
		switch( cMethodArgInfo.GetSubType() )
		{
			case RUL_UNSIGNED_CHAR_DECL:
			case RUL_SHORT_INTEGER_DECL:
			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
			case RUL_INTEGER_DECL:
			case RUL_UNSIGNED_INTEGER_DECL:
			case RUL_LONG_LONG_DECL:
			case RUL_LONG_DECL:
			case RUL_REAL_DECL: 
			case RUL_DOUBLE_DECL:
				switch( eTokenSubType )
				{
					case RUL_CHAR_DECL:
					case RUL_UNSIGNED_CHAR_DECL:
					case RUL_SHORT_INTEGER_DECL:
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					case RUL_INTEGER_DECL:
					case RUL_UNSIGNED_INTEGER_DECL:
					case RUL_LONG_LONG_DECL:
					case RUL_LONG_DECL:
					case RUL_REAL_DECL: 
					case RUL_DOUBLE_DECL:
						break;
					default:
			return FAILURE;
		}		
				break;
			case RUL_CHAR_DECL:
				switch( eTokenSubType )
				{
					case RUL_CHAR_DECL:
					case RUL_UNSIGNED_CHAR_DECL:
					case RUL_SHORT_INTEGER_DECL:
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					case RUL_INTEGER_DECL:
					case RUL_UNSIGNED_INTEGER_DECL:
					case RUL_LONG_LONG_DECL:
					case RUL_LONG_DECL:
					case RUL_REAL_DECL: 
					case RUL_DOUBLE_DECL:
					case RUL_STRING_DECL:
					case RUL_DD_STRING_DECL:
						break;
					default:
						return FAILURE;
				}
				break;
			case RUL_STRING_DECL:
			case RUL_DD_STRING_DECL:
				switch( eTokenSubType )
				{
					case RUL_STRING_DECL:
					case RUL_DD_STRING_DECL:
						break;
					default:
						return FAILURE;
				}
				break;
			default: 
				if( cMethodArgInfo.GetSubType() != eTokenSubType )
		{
			return FAILURE;
				}
		}	
		// Walt EPM 08sep08 - finished adding a couple	

		//check for the Simple variable and Array type
		// Get the token Type from the param list
		eTokenType = (chCmethodParam.isArray())?RUL_ARRAY_VARIABLE:RUL_SIMPLE_VARIABLE;
		
		//Override if it is of DD item type
		//To do Take this case latter		an error
		if (chCmethodParam.getType() == methodVarDDItem) 
		{
			eTokenType = RUL_DD_ITEM;
		}
		
		//if the token type is not same then it
		if(eTokenType != cMethodArgInfo.GetType() )
		{
			return FAILURE;
		}


	}
		
	return SUCCESS;

}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This method id to get the Called method information and update the METHOD_ARG_INFO_VECTOR class 
//for each of the parameter
//Get the Arguement name , and fill it in the class
//get the Passed type, i.e whether it is passed by value or by reference
RETURNCODE MEE::UpdateCalledMethArgInfo(METHOD_ARG_INFO_VECTOR*  vectMethArgInfo, 
										ParamList_t*             TempParamList,
										vector<string>*          strvCallerArgList)
{
	if(vectMethArgInfo->size() != TempParamList->size() )
	{
		//return failure as it is not a valid arg list
		return FAILURE;		
	}

	unsigned int uiNoOfArgs = vectMethArgInfo->size();
	for(unsigned int uiArgIndex = 0; uiArgIndex< uiNoOfArgs ; uiArgIndex++)
	{
		METHOD_ARG_INFO* cMethodArgInfo = &(*vectMethArgInfo)[uiArgIndex] ;
		hCmethodParam chCmethodParam;
		chCmethodParam = (*TempParamList)[uiArgIndex];

		//Get each of the argument
		string strCalledName = chCmethodParam.getName();
		cMethodArgInfo->SetCalledArgName(strCalledName.c_str());
		
		//get the passed type
		cMethodArgInfo->ePassedType = (chCmethodParam.isRef()) ? DD_METH_AGR_PASSED_BYREFERENCE
															   : DD_METH_AGR_PASSED_BYVALUE;
		//Over ride if iy is method type
		if (chCmethodParam.getType() == methodVarDDItem) 
		{
			//If it is DD item type, then we may have to resolve this, and pass the DD Item name at the end
			cMethodArgInfo->ePassedType = DD_METH_AGR_PASSED_UNKNOWN;
			string strTemp = (*strvCallerArgList)[uiArgIndex];
			char* szLastAttr = NULL;
			bool bPrimaryAttr = false;
			char* pchPassedValue = new char[strTemp.size()+1];
			strcpy(pchPassedValue,strTemp.c_str());

			//Jsut Validate the Expression , if it is an DD expression
			if(FAILURE == ValidateExp(pchPassedValue, &szLastAttr,bPrimaryAttr) )
			{
				if(szLastAttr)
				{
					delete[] szLastAttr;
					szLastAttr = NULL;
				}
				return FAILURE;
			}

			//If it is primary attribute like LABEL, then it is an error as it is not DD item
			if(bPrimaryAttr == true)
			{
				return FAILURE;
			}
			if(szLastAttr)
			{
				delete[] szLastAttr;
				szLastAttr = NULL;
			}			
			unsigned long iEndofEpression = strlen(pchPassedValue);
			//See whether there is any . or [ present , because it may be a Collection or array , on resolving this it may be DD item
			int nIndex = 0;
			for(; nIndex < (int)strlen(pchPassedValue); nIndex++) 	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
			{
				if('.' == pchPassedValue[nIndex] || '[' == pchPassedValue[nIndex])
					break;
			}
			char* pcdDdItemName = new char[nIndex+1];
			strncpy(pcdDdItemName,pchPassedValue,nIndex);
			pcdDdItemName[nIndex] = '\0';

			
			//Resolve the Expression which ultimately gives the item base ptr
			hCitemBase* pIBFinal = NULL;
			if( FAILURE == ResolveExp((const char*) pcdDdItemName,(const char*) pchPassedValue,iEndofEpression,&pIBFinal) )
			{
				return FAILURE;
			}
			if(pIBFinal == NULL)
			{
				return FAILURE;

			}
			//Get the name of the DD item
			//With the item base ptr , get the DD item name
			string strDDVarName = pIBFinal->getName();
			//Fill this in the METHOD_ARG_INFO_VECTOR class
			(*vectMethArgInfo)[uiArgIndex].SetCallerArgName(strDDVarName.c_str());
			(*vectMethArgInfo)[uiArgIndex].ulDDItemId = pIBFinal->getID();

			if(pchPassedValue)
			{
				delete[] pchPassedValue;
				pchPassedValue = NULL;
			}
			if(pcdDdItemName)
			{
				delete[] pcdDdItemName;
				pcdDdItemName = NULL;
			}
		}		
			

	}
	return SUCCESS;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This function is to fill the return arg name and its type
//The assumptions made here is the ,  for the caller and called method, Return agrument variable 
//name is "__RetVar_" + Called Method Name + "_XXX"; //I am really sorry, if some body in Called 
//method has an variable by this name... hopefully not!!
//Data type is filled apropriatly , and when coming out from called method, this variable is 
//filled with the return value. also there one more flag m_IsReturnVar set for this return variable
//so that in the called method , when this variable is on symbol table, can be known as returnn 
//variable There can be only one return variable in the called method Symbol table and is of only 
//the following data type
RETURNCODE MEE::UpdateRetunMethArgInfo(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,
									   hCmethodParam* phCmethodReturnValue,
									   vector<INTER_VARIANT>* vectInterVar    ) 
{
// stevev 20feb09 - dd_item conversion to parameter
	int iVsize= vectInterVar->size();
	int iSize = vectMethArgInfo->size();
	assert(iVsize == iSize);
	for(int iCount = 0; iCount < iSize ; iCount++)
	{
		METHOD_ARG_INFO* cMethodArgInfo = &(*vectMethArgInfo)[iCount] ;
		if( cMethodArgInfo )
		{
			if (cMethodArgInfo->GetType()    == RUL_DD_ITEM  &&
				cMethodArgInfo->GetSubType() == RUL_DD_COMPLEX)
			{// a non-variable DD item was passed
				(*vectInterVar)[iCount] = (unsigned int)cMethodArgInfo->ulDDItemId;
			}		
		}// else null pointer in list...??? error, logit?????
	}//next
// end stevev 20feb09
	//get the method name to form the return variable name	
	string StrMethName = phCmethodReturnValue->getName();
	//Return variable name
	string strUnikRetVarName = "__RetVar_" + StrMethName + "_XXX";
	INTER_VARIANT varTemp;

	METHOD_ARG_INFO cMethReturnArgInfo;
	RUL_TOKEN_TYPE eTokenType;
	RUL_TOKEN_SUBTYPE eTokenSubType;
	//Get the data type of the return variable
	//Refer above in ValidateMethodArgs,  for  forming the token type and sub type
	eTokenType = RUL_SIMPLE_VARIABLE;
	eTokenSubType = RUL_SUBTYPE_NONE;
	switch(phCmethodReturnValue->getType())
	{
		case methodVarChar:
			{
				eTokenSubType = RUL_CHAR_DECL;
				varTemp = (char)0;//WS: 25jun07 - set value and type

			}
			// Walt EPM 08sep08 added modified
			break; // stevev
		case methodVar_U_Char:
			{
				eTokenSubType = RUL_UNSIGNED_CHAR_DECL;
				varTemp = (unsigned char)0;//WS: 25jun07 - set value and type
			}
			break; // stevev
		case methodVarShort:
			{
				eTokenSubType = RUL_SHORT_INTEGER_DECL;
				varTemp = (short)0;//WS: 25jun07 - set value and type
			}
			break;
		case methodVar_U_Short:
			{
				eTokenSubType = RUL_UNSIGNED_SHORT_INTEGER_DECL;
				varTemp = (unsigned short)0;//WS: 25jun07 - set value and type

			}
			break;
		case methodVarLongInt:
			{
				eTokenSubType = RUL_INTEGER_DECL;
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
				varTemp = (int)0;//WS: 25jun07 - set value and type
#else
				varTemp = (long)0;//WS: 25jun07 - set value and type
#endif // __GNUC__
			}
			break;
		case methodVar_U_LongInt:
			{
				eTokenSubType = RUL_UNSIGNED_INTEGER_DECL;
				varTemp = (unsigned int)0;
			}
			break;
		case methodVar_U_Int64:
		case methodVarInt64:
			{
				eTokenSubType = RUL_LONG_LONG_DECL;
				varTemp = (UINT64)0;
			}
			break;
			// Walt EPM 08sep08 - end added/modified
		case methodVarDDItem:
			{
				eTokenSubType = RUL_INTEGER_DECL;
/* 
	LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix compiler error 
	(no long assignment override available). Check if that is what was 
	intended. Also see _INT32 typedef in typedefs.h.
*/
#if defined(__GNUC__)
				varTemp = (int)0;//WS: 25jun07 - set value and type
#else
				varTemp = (long)0;//WS: 25jun07 - set value and type
#endif // __GNUC__
			}
			break;

		case methodVarDouble:
			{
				eTokenSubType = RUL_DOUBLE_DECL;//WS:EPM 10aug07
				varTemp = (double)0.0;//WS: 25jun07 - set value and type
			}
			break;
		case methodVarFloat:			
			{
				eTokenSubType = RUL_REAL_DECL;
				varTemp = (float)0.0;//WS: 25jun07 - set value and type
			}
			break;

		case methodVarDDString:			
			{
				eTokenSubType = RUL_DD_STRING_DECL;
				varTemp = "";//WS: 25jun07 - set value and type
			}
			break;
		case methodVarVoid:
			{
				eTokenSubType = RUL_SUBTYPE_NONE;
			}
			break;


		default:
			{
				//TO DO We may need to validate the   whether it id DD item and Check accordingle
			//error!!!!
			}
			break;

	}//end switch
	//So Create new  METHOD_ARG_INFO class for the return variable and push it on the vector
	
	cMethReturnArgInfo.SetCalledArgName(strUnikRetVarName.c_str());
	cMethReturnArgInfo.SetCallerArgName(strUnikRetVarName.c_str());
	cMethReturnArgInfo.SetType(eTokenType);
	cMethReturnArgInfo.SetSubType(eTokenSubType);
	cMethReturnArgInfo.ePassedType = DD_METH_AGR_PASSED_BYREFERENCE;
	//this is very imp!! Set this flag only for the return variable
	cMethReturnArgInfo.m_IsReturnVar = true;
	vectMethArgInfo->push_back(cMethReturnArgInfo);

	vectInterVar->push_back(varTemp);
	return SUCCESS;

}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Just an helper function to determine whether it is an DD item or not
bool MEE::IsDDItem(const char* pszDDItemName)
{

	hCitemBase*  pIB = NULL;
	string strDDItemName = pszDDItemName;
	//Get the Item base ptr, if it is success , then it is a DD item
	if( (SUCCESS == m_pDevice->getItemBySymName(strDDItemName,&pIB)) && (NULL != pIB) )
	{
		return true;
	}
	else
	{
		return false;
	}
		
}

RETURNCODE OneMeth::ExecuteActionsInMethod(vector<itemID_t> actionList)
{
	
	for(int iCount = 0 ; iCount< (int)actionList.size(); iCount++)  	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		long lMethodItemId = actionList[iCount];				
		bool bRet= m_pMEE->ExecuteMethod(m_pDevice,lMethodItemId);
		if(false == bRet)
			return FAILURE;
	}
	
	return SUCCESS;
}

/* this should not be used stevev 06mar07 ---
//Anil 140906 Added the overloaded function
bool MEE::ExecuteMethod(vector<unsigned long> actionIDList)
{
	
	for(int iCount = 0 ; iCount< actionIDList.size(); iCount++)
	{
		long lMethodItemId = actionIDList[iCount];				
		bool bRet = ExecuteMethod(m_pDevice,lMethodItemId);
		if(false == bRet)
			return false;
	}
	
	return true;
}
end should not be used */

/************************************************************************************************/
/* stevev 15aug07 --- resolution of complex references is revisited.
	Earlier code had restrictions like indexes had to be constant integers.
	Earlier code was not designed to be recursive either, requiring more code to deal with fewer 
	conditions.

  The Following functions:   ResolveComplexReference()  ResolveDot()  ResolveBrack()
  are designed to overcome these deficiencies.
  They are coded separately here so they may be tested separately and then wrapped as required to
  fill the MEE requirements.

  NOTE:  There are some TODO: comments embedded in the following code.  Most of these have
  to do with needing to call higher level code in order to make the functionality correct.
  For example:an array index may be a DD-variable or a constant OR A METHOD_LOCAL OR METHOD-CALL.
  This function will handle the DD-variable and constant, but there are TODO comments getting
  the resolution to a built-in, method-call, or method-local is required.
  
  PLEASE-Someone that knows or wants to learn needs to take ownership of these TODOs and
  complete the functionality of this section!

*/


#define A_DOT	'.'
#define A_BRACK '['
#define A_EMPTY '\0'
#define A_CLOSE ']'

// helpers
char* skipWhite(char* src)
{//TODO: make language insensitive....
	if ( src )
	{
		while ( (*src == '\t' || *src == ' ' || *src == '\n') && *src != '\0' ) 
		{	src++;}// skip whitespace  
	}
	return src;
}

bool MEE::isAttr(char* pAttrStr)
{		
	DOT_OP_ATTR stDotOpAtt;	
	//Check  whether it is valid primary attribute	
	m_MapDotOpNameToAttrIter = m_MapDotOpNameToAttr.find(pAttrStr);
	if(m_MapDotOpNameToAttrIter == m_MapDotOpNameToAttr.end())
	{	//it is not valide attribute, Sorry man hang up!!
		return false;
	}
	//Get the Attribute Structure Which contains " C" Data type of attribute and its unique ID
	stDotOpAtt = m_MapDotOpNameToAttrIter->second;// for debugging

	return true;
}



RETURNCODE MEE::getAttrInfo( hCitemBase* pLeftItem, char* attributeName, 
 /* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar)
{
	return GetDDItemAttrValue(pLeftItem,  attributeName, pValue,rpDevObjVar);
}

// note that pMemberName is just the membername, not the right string
RETURNCODE MEE::getMemberInfo(hCitemBase* pLeftItem, const char*   pMemberName, 
 /* return values: */    CValueVarient* pValue, hCitemBase*& rpDevObjVar)
{
	RETURNCODE rc = SUCCESS;
	if ( pLeftItem == NULL  || pMemberName == NULL || pValue == NULL)
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}
	pMemberName = skipWhite((char*)pMemberName );
//#			verify itempointer has members	
	itemType_t it = pLeftItem->getIType();
	if (it !=iT_Collection && it !=iT_File && it !=iT_Chart && it !=iT_Graph && it !=iT_Source)
	{
		SDCLOG(CERR_LOG,"Method Reference: '%s' Does not have members to access via '%s'\n",
													pLeftItem->getName().c_str(),pMemberName);
		return MEE_NO_MEMBERS_IN_ITEM;
	}

	string memNm                = pMemberName;
	hCgroupItemDescriptor* pGid = NULL;	// we will own this memory - delete before exitting
	hCitemBase*   pLft          = NULL;
	CValueVarient vVal;

//#			get by name in item	
//#			return error if not found	
//#			fill value&ptr and return
	switch (it)
	{
	case iT_Collection:
		{
			rc = ((hCcollection*)pLeftItem)->getByName (memNm, &pGid);
			if ( rc == SUCCESS && pGid != NULL )
			{
				rc = pGid->getItemPtr(pLft);// constant references NOT allowed in collection
				if ( rc == SUCCESS && pLft != NULL)
				{
					rpDevObjVar = pLft;
					if ( pLft->IsVariable() )
					{
						vVal = ((hCVar*)pLft)->getDispValue();
						*pValue = vVal;
					}
					else
					{
						pValue->clear();
					}
					// leave rc SUCCESS
				}
				else
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' failed to get item from member "
									"'%s'\n",		pLeftItem->getName().c_str(),pMemberName);
					rc = MEE_MEMBER_NOT_FOUND;
				}
			}
			else
			{// no member
				SDCLOG(CERR_LOG,"Method Reference: '%s' failed to resolve membername '%s'\n",
													pLeftItem->getName().c_str(),pMemberName);
				rc = MEE_MEMBER_NOT_FOUND;
			}
			RAZE(pGid);
		}
		break;
	case iT_File:
		{
			rc = ((hCfile*)pLeftItem)->getByName (memNm, &pGid);
			if ( rc == SUCCESS && pGid != NULL )
			{
				rc = pGid->getItemPtr(pLft);// constant references NOT allowed in files
				if ( rc == SUCCESS && pLft != NULL)
				{
					rpDevObjVar = pLft;
					if ( pLft->IsVariable() )
					{
						vVal = ((hCVar*)pLft)->getDispValue();
						*pValue = vVal;
					}
					else
					{
						pValue->clear();
					}
					// leave rc SUCCESS
				}
				else
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' failed to get item from member "
									"'%s'\n",		pLeftItem->getName().c_str(),pMemberName);
					rc = MEE_MEMBER_NOT_FOUND;
				}
			}
			else
			{// no member
				SDCLOG(CERR_LOG,"Method Reference: '%s' failed to resolve membername '%s'\n",
													pLeftItem->getName().c_str(),pMemberName);
				rc = MEE_MEMBER_NOT_FOUND;
			}
			RAZE(pGid);
		}
		break;
	case iT_Chart:
		{
			rc = MEE_UNIMPLEMENTED;
		}
		break;
	case iT_Graph:
		{
			rc = MEE_UNIMPLEMENTED;
		}
		break;
	case iT_Source:
		{
			rc = MEE_UNIMPLEMENTED;
		}
		break;
		// no default - these are the only values allowed into the routine
	}// end switch
	return rc;	
}

/************************************************************************************************/

/*	This is a Helper Function is for resolving complex DD references like DDItem[i].member[j].x  
	See ResolveComplexReference() for a discussion of these.
	This Function handles the bracketed portion of the string  e.g.
	LeftItem     is the pointer to the DDItem
	RightString  is '[i].member[j].x'

    This function will Resolve the DDItem[i] part and then call ResolveDot() to handle the rest.
	ie   return ResolveDot( ptr2dditem_i, ".member[j].x", pValue, ppDevObjVar)

	Function Input Parameters: 
	pLeftItem		is a pointer to the DD item resolved by the left part of the reference to 
					this point.  This MUST be a pointer to a list or value OR reference ARRAY. 
					All others will fail.
	RightString		The string to the right of the portion resolved to pLeftItem.  This MUST
					start with a '[' and have a ']' in the rest of it or it will fail.
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
*/
/************************************************************************************************/


RETURNCODE MEE::ResolveBracket( hCitemBase* pLeftItem, char* pRightString, 
		/* return values: */    CValueVarient* pValue, hCitemBase*& rpDevObjVar)		
{		
	RETURNCODE rc = SUCCESS;
	if ( pLeftItem == NULL  || pRightString == NULL || pValue == NULL)
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}
	pRightString = skipWhite(pRightString);

	char    *pNxt      = pRightString,   *pIdx = NULL;
	bool    isNumeric = true;// will soon be disproven
	char    foundChar = A_EMPTY;
	hCitemBase* pLft  = NULL;
	CValueVarient vVal;
	string  sIndex;
	

//#	verify LeftItem has elements	
// stevev 19oct09 - there might be just one more....	
//	if ( pLeftItem->getIType() != iT_ItemArray && pLeftItem->getIType() != iT_Array )
	if ( pLeftItem->getIType() != iT_ItemArray && 
		 pLeftItem->getIType() != iT_Array     && 
		 pLeftItem->getIType() != iT_List       )
	{// the ONLY types that have elements, it it ain't one of dem, its an error
		SDCLOG(CERR_LOG,"Method Reference: '%s' is expected to be an Array to resolve '%s'\n",
													pLeftItem->getName().c_str(),pRightString);
		return MEE_NOT_AN_ARRAY_TYPE;
	}
//#	get RightString till ']' || mt
	if ( *pNxt != '[' )
	{
		SDCLOG(CERR_LOG,"Method Reference: '%s' is expected to start with '['\n", pRightString);
		return MEE_INVALID_INDEX;
	}
	
	pIdx = pNxt = skipWhite(++pNxt);// twixt '[' and index
	while ( *pNxt != ']' && *pNxt != '\0')
	{
		if ( ! isdigit(*pNxt) )
			isNumeric = false;
		pNxt++;
	}
//#	if MT	
	if ( (*pNxt != ']') || (pNxt == pIdx) )
	{
		SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid index.(from '%s')\n", pIdx,
																				pRightString);
		return MEE_INVALID_INDEX;
	}	
//#	else - no error	
	//vVal, pLft
//#		lookahead one char to be a Dot or Brack or MT (others are error)
	*pNxt  = '\0';// terminate index string
	sIndex = pIdx;
	*pNxt  = ']';
	pNxt   = skipWhite(++pNxt);// from ']' to rest of right
	if ( *pNxt == A_DOT )
	{
		foundChar = A_DOT;
	}
	else
	if ( *pNxt == A_BRACK )
	{
		foundChar = A_BRACK;
	}
	else
	if ( *pNxt == A_EMPTY )
	{
		foundChar = A_EMPTY;
	}
	else
	{
		SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid reference.(from '%s')\n", pNxt,
																				pRightString);
		return MEE_INVALID_REFERENCE;
	}
	//          a little dangerous\|/   (casting a const to a ptr...)
	rc = ResolveComplexReference((char*)sIndex.c_str(),  &vVal,pLft);// handles simple ones too
//#		exit on error or no Value	
	if ( rc != SUCCESS ) return rc;
	if ( (! vVal.vIsValid) || ((int)vVal < 0) )
	{
		SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid index.(from '%s')\n", 
																	sIndex.c_str(),pRightString);
		return MEE_INVALID_INDEX;
	}	
//#		else - no error		
//#			lookup by localValue in LeftItem into Value & Ptr
	hCgroupItemDescriptor* pGid = NULL;
	if ( pLeftItem->getIType() == iT_ItemArray )
	{
		rc = ((hCitemArray*)pLeftItem)->getByIndex((unsigned)vVal,&pGid,false);
	}
	else
	if ( pLeftItem->getIType() == iT_Array )
	{
		rc = ((hCarray*)pLeftItem)->getByIndex((unsigned)vVal,&pGid,false);
	}
	else // must be iT_List <we checked already>
	{
		rc = ((hClist*)pLeftItem)->getByIndex((unsigned)vVal,&pGid,false);
	}
//#			return error if not found	
	if ( rc || pGid == NULL )
	{
		return rc;// it should have already told the erroneous story
	}

	if ( (rc = pGid->getItemPtr(pLft)) || pLft == NULL )
	{
		SDCLOG(CERR_LOG,"Method Reference: Could not resolve '%s [%s]'\n", 
															pLeftItem->getName().c_str(), pIdx);
		RAZE(pGid);
		return MEE_INVALID_REFERENCE;
	}
	RAZE(pGid);


	if ( pLft->IsVariable() )
	{
		vVal = ((hCVar*)pLft)->getDispValue();
	}
	else // has to be an item reference...
	{
		vVal.clear();
	}
//#			if lookahead is MT	
	if (foundChar == A_EMPTY)
	{
//#				return the found Value & Ptr
		rpDevObjVar = pLft;
		*pValue     = vVal;
		return SUCCESS;
	}
//#			else if lookahead is Dot	
	if (foundChar == A_DOT  )
	{
		return   ResolveDot( pLft, pNxt, pValue,rpDevObjVar);
	}
//#			else - must be bracket	
	if (foundChar == A_BRACK)
	{
		return   ResolveBracket( pLft, pNxt, pValue,rpDevObjVar);
	}
	return FAILURE;
}


/************************************************************************************************/

/*	This is a Helper Function is for resolving complex DD references like DDItem[i].member[j].x  
	See ResolveComplexReference() for a discussion of these.
	This Function handles the .def portion of the string  e.g.
	LeftItem     is the pointer to the DD-item defined as DDItem[i]
	RightString  is '.member[j].x'

    This function will Resolve the DDItem[i].member part and then call ResolveBracket() to handle
	the rest.
	ie   return ResolveBracket( ptr2dditem_i_mem, "[j].x", pValue, ppDevObjVar)

	Function Input Parameters: 
	pLeftItem		is a pointer to the DD item resolved by the left part of the reference to 
					this point.  This MUST be a pointer to a non-relational DD item (eg not a 
					unit-relation,refresh-relation, wao <or cmd>).  All others will fail.
	RightString		The string to the right of the portion resolved to pLeftItem.  This MUST
					start with a '.' and have a symbol to the right of it or it will fail.
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
*/
/************************************************************************************************/


RETURNCODE MEE::ResolveDot( hCitemBase* pLeftItem, char* pRightString, 
	/* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar)		
{		
	RETURNCODE rc = SUCCESS;

	if (pLeftItem == NULL || pRightString == NULL || pValue == NULL)
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}
	char    *pNxt     = pRightString + 1;// skip past the leading '.'
	bool    isNumeric = true;// will soon be disproven
	char    foundChar = A_EMPTY;
	hCitemBase* pLft  = NULL;
	CValueVarient vVal;
	string  ls;

//#	get RightString to  '.' || '[' || eos    { flag if Numeric }	
	while ( *pNxt != '\0' )
	{
		if ( *pNxt == A_DOT )
		{
			foundChar = A_DOT;
			break;
		}
		if ( *pNxt == A_BRACK )
		{
			foundChar = A_BRACK;
			break;
		}
		if ( ! isdigit(*pNxt) )
			isNumeric = false;
		pNxt++;
	}

	if (foundChar == A_EMPTY)
	{//#  at MT - 
		ls    = ++pRightString;// skip over leading dot
		if ( isAttr(pRightString) )//	if string is attribute		
		{
			return getAttrInfo(pLeftItem, pRightString, pValue, rpDevObjVar);// deals w/ errors
		}
		else
		if ( ls == "X_AXIS" ) // possible reference to a DD item
		{// a secondary attribute
//#			verify valid attribute for item
			if ( pLeftItem->getIType() == iT_Graph)
			{
//#				lookup attribute in item 
				rpDevObjVar = ((hCgraph*)pLeftItem)->getXAxis();
//#				return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: X_AXIS of %s did not resolve.\n",
																pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}// else - all is well
//#				fill ptr, clear varient and return
				pValue->clear();
				return SUCCESS;
			}
			else // - invalid
			{
				SDCLOG(CERR_LOG,"Method Reference: 'X_AXIS' is invalid reference for %s\n",
																	pLeftItem->getName().c_str());
				return MEE_INVALID_REFERENCE;
			}
		}
		else
		if ( ls == "Y_AXIS" ) // possible reference to a DD item
		{// a secondary attribute

//#			verify valid attribute for item
			if ( pLeftItem->getIType() == iT_Source)
			{
//#				lookup attribute in item 
				rpDevObjVar = ((hCsource*)pLeftItem)->getYAxis();
//#				return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Source Y_AXIS of %s did not resolve.\n",
																	pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}// else - all is well
//#				fill ptr, clear varient and return
				pValue->clear();
				return SUCCESS;
			}
			else
			if ( pLeftItem->getIType() == iT_Waveform)
			{
//#			lookup attribute in item 
				rpDevObjVar = ((hCwaveform*)pLeftItem)->getYAxis();
//#			return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Waveform Y_AXIS of %s did not resolve.\n",
																	pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}// else - all is well
//#			fill ptr, clear varient and return
				pValue->clear();
				return SUCCESS;
			}
			else
			{
				SDCLOG(CERR_LOG,"Method Reference: 'Y_AXIS' is invalid reference for %s\n",
																	pLeftItem->getName().c_str());
				return MEE_INVALID_REFERENCE;
			}
		}
		else // must be a membername		
		{
			return getMemberInfo(pLeftItem, pRightString, pValue, rpDevObjVar);
		}
	}
	else // { Dot or Brack }		
	{						
		*pNxt = A_EMPTY;     // temporary
		ls    = pRightString+1;// up to the brack
		*pNxt = foundChar;   // put it back before we forget.

//#		if string is secondary attribute
		if ( ls == "X_AXIS" ) // possible reference to a DD item
		{// a secondary attribute
//#			verify valid attribute for item
			if ( pLeftItem->getIType() == iT_Graph)
			{
//#				lookup attribute in item as pLft
				pLft = ((hCgraph*)pLeftItem)->getXAxis();
//#				return error if not found or not an axis
				if (pLft == NULL || pLft->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: X_AXIS of %s did not resolve.\n",
																	pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}
				// else - all is well - pLft is set
			}
			else // - invalid
			{
				SDCLOG(CERR_LOG,"Method Reference: 'X_AXIS' is invalid reference for %s\n",
																	pLeftItem->getName().c_str());
				return MEE_INVALID_REFERENCE;
			}
		}
		else
		if ( ls == "Y_AXIS" ) // possible reference to a DD item
		{// a secondary attribute
//#			verify valid attribute for item
			if ( pLeftItem->getIType() == iT_Source)
			{
//#				lookup attribute in item as pLft
				pLft = ((hCsource*)pLeftItem)->getYAxis();
//#				return error if not found or not an axis
				if (pLft == NULL || pLft->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Source Y_AXIS of %s did not resolve.\n",
																	pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}
				// else - all is well and pLft is filled
			}
			else
			if ( pLeftItem->getIType() == iT_Waveform)
			{
//#				lookup attribute in item as pLft
				pLft = ((hCwaveform*)pLeftItem)->getYAxis();
//#				return error if not found or not an axis
				if (pLft == NULL || pLft->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Waveform Y_AXIS of %s did not resolve.\n",
																	pLeftItem->getName().c_str());
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}
				// else - all is well and pLft is filled
			}
			else
			{
				SDCLOG(CERR_LOG,"Method Reference: 'Y_AXIS' is invalid reference for %s\n",
																	pLeftItem->getName().c_str());
				return MEE_INVALID_REFERENCE;
			}
		}
		else // must be a membername		
		{	
//#			fill value&ptr and return	
			rc = getMemberInfo(pLeftItem, ls.c_str(), &vVal, pLft);
//#			return error if not found
			if ( rc != SUCCESS || pLft == NULL )// has to resolve to an item to deal w/dot|brack
			{
				SDCLOG(CERR_LOG,"Method Reference: '%s' is invalid reference for %s\n",
													ls.c_str(),	pLeftItem->getName().c_str());
				return MEE_INVALID_REFERENCE;
			}
			// else pLft is filled, exit resolving the rest
		}


		if (foundChar == A_DOT)
		{
			return   ResolveDot( pLft, pNxt, pValue, rpDevObjVar);
		}
		else // must be a brack
		if (foundChar == A_BRACK)
		{
			return   ResolveBracket( pLft, pNxt, pValue, rpDevObjVar);	
		}
		else
		{// should never get here
			SDCLOG(CERR_LOG,"Method Reference: '%s' Unknown type.\n",pRightString);
			return FAILURE;
		}
	}
}


/************************************************************************************************/

/*	This is the Function for resolving complex DD references like DDItem[i].member[j].x  
	Where 
	DDItem  :is a DD item (an list or array of something with members), 
	i,j		:are Constant integers, numeric DD-items or complex numeric DD references 
	member  :is a member of "DDItem[i]" that resolves to an array or list  
	x		:can be	1)member of "DDItem[i].member[j]"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap()) 
					3)secondary attribute ( like Y_AXIS --refer BuildSecondaryAttrList for this) 


	This Function is the principle entry to resolve this type of reference.

	Function Input Parameters: 
	ReferenceString is a pointer to the entire reference string
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
*/
/************************************************************************************************/

RETURNCODE MEE::ResolveComplexReference(char* ReferenceString, 
				/* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar)
{	
	RETURNCODE rc = SUCCESS;

	if (ReferenceString == NULL || ReferenceString[0] == '\0' || pValue == NULL)
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}
	char*   pNxt      = ReferenceString;
	bool    isNumeric = true;// will soon be disproven
	char    foundChar = A_EMPTY;
	hCitemBase* pLft  = NULL;
	string  ls;

//#	get from beginning to  '.' || '[' || eos    { flag if Numeric }	
	while ( *pNxt != '\0' )
	{
		if ( *pNxt == A_DOT )
		{
			foundChar = A_DOT;
			break;
		}
		if ( *pNxt == A_BRACK )
		{
			foundChar = A_BRACK;
			break;
		}
		if ( ! isdigit(*pNxt) )
			isNumeric = false;
		pNxt++;
	}

	if (foundChar == A_BRACK)
	{
//#	if Brack  {Numeric Left is error}
//#		get left as item - error if not found				
		*pNxt = A_EMPTY;// temporary
		ls    = ReferenceString;// up to the brack
		*pNxt = A_BRACK;// put it back before we forget.
		if (isNumeric)
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' cannot handle '%s'.\n",
																	ReferenceString,ls.c_str());
			return MEE_NUMERIC_ARRAYNAME;
		}

		rc = m_pDevice->getItemBySymName(ls, &pLft);

		if (rc != SUCCESS || pLft == NULL )// let ResolveBracket kick out in not an array
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' could not resolve '%s'.\n",
																	ReferenceString,ls.c_str());
			return MEE_DDITEM_NAME_NOT_RESOLVED;
		}
		return ResolveBracket(pLft, pNxt, pValue, rpDevObjVar);
	}
	else	
	if (foundChar == A_DOT)
	{			
		*pNxt = A_EMPTY;        // temporary
		ls    = ReferenceString;// up to the brack
		*pNxt = A_DOT;        // put it back before we forget.
		if ( isNumeric)
		{
//#			lookahead right for all digits to end	
			char* pL = pNxt + 1;// past the dot
			while ( pL != '\0' )
			{	if ( !isdigit(*pL) ) isNumeric = false; 
			}
//#			if true - all digits to end	
			if ( isNumeric )
			{
//#				convert string to float		
				double lD = atof(ReferenceString);
//#				put float into Value, Clear Ptr	
				*pValue     = (double)lD;
				rpDevObjVar = NULL;
				return      SUCCESS;
			}
			else // non-numeric to right of DP ( could be exponent but I'm not supporting that )
			{
				SDCLOG(CERR_LOG,"Method Reference: could not resolve '%s'.\n", ReferenceString);
				return MEE_DDITEM_NAME_NOT_RESOLVED;
			}
		}
		else //- left not numeric, must be an item 
		{
//#			get left item to Ptr	
			rc = m_pDevice->getItemBySymName(ls, &pLft);

			if (rc != SUCCESS || pLft == NULL )// let ResolveDot kick out type mismatch
			{
				SDCLOG(CERR_LOG,"Method Reference: '%s' could not resolve '%s'.\n",
																	ReferenceString,ls.c_str());
				return MEE_DDITEM_NAME_NOT_RESOLVED;
			}
			return ResolveDot(pLft, pNxt, pValue, rpDevObjVar);
		}
	}
	else //- must be MT (we're at the end)			
	{
		if ( isNumeric )
		{
//#			convert string to integer	
			int loc = atoi(ReferenceString);
//#			put integer into Value, Clear Ptr	& leave
			*pValue     = (int)loc;
			rpDevObjVar = NULL;
			return      SUCCESS;
		}
		else //- left not numeric		
		{
			// TODO: Recognize a leading + or - and handle correctly
//#			check for 0x entry
			pNxt      = ReferenceString;
			if (*pNxt == '0' && (*pNxt == 'x' || *pNxt == 'X') )
			{
//#				convert Hex string to integer
				unsigned loc = 0;
				sscanf(ReferenceString,"%x",&loc);
//#				put integer into Value, Clear Ptr & leave
				*pValue     = (int)loc;
				rpDevObjVar = NULL;
				return      SUCCESS;
			}
			else // not numeric, not hex, must be variable
			{
//#				try to find as a DD item
				ls = ReferenceString;
				
				rc = m_pDevice->getItemBySymName(ls, &pLft);

				if (rc != SUCCESS || pLft == NULL )
				{
/*  At this point, the string we are looking at could be a non-DDitem reference
	- it could be a method-local variable name or it could be a call to
	a built-in  or another method that  returns a value to be used here.
	TODO:  detect and get a value for these types of references !!!!!!!!!!!!!!
*/					SDCLOG(CERR_LOG,"Method Reference: '%s' could not be resolved.\n",
																				ReferenceString);
					return MEE_DDITEM_NAME_NOT_RESOLVED;
				}// else - return the info
				
				rpDevObjVar = pLft;
				if ( pLft->IsVariable() )
				{
					*pValue = ((hCVar*)pLft)->getDispValue();
				}
				else
				{
					pValue->clear();
				}
				return      SUCCESS;
			}
		}
	}
}
