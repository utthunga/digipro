#ifndef MEE_H
#define MEE_H

//#ifndef XMTRDD
#pragma warning (disable : 4786)
// #include "ddbAttributes.h"

#include "Interpreter.h"
#include "Hart_Builtins.h"
#include <list>
#include <vector>
#include "SymbolTable.h"

#ifndef RETURNCODE
/* error codes */
   #define RETURNCODE  int

   #define SUCCESS			(0)
   #define FAILURE			(1)

	#define MEE_ERROR_BASE    8000
#endif

// MEE error return codes
#define MEE_BAD_PARAM_PASSED_IN			(MEE_ERROR_BASE +  1)
#define MEE_DDITEM_NAME_NOT_RESOLVED	(MEE_ERROR_BASE +  2)
#define MEE_NUMERIC_ARRAYNAME			(MEE_ERROR_BASE +  3)
#define MEE_INVALID_REFERENCE			(MEE_ERROR_BASE +  4)
#define MEE_NOT_AN_ARRAY_TYPE			(MEE_ERROR_BASE +  5)
#define MEE_INVALID_INDEX			    (MEE_ERROR_BASE +  6)
#define MEE_ITEM_ATTRIBUTE_MISMATCH		(MEE_ERROR_BASE +  7)
#define MEE_NON_NUMERIC_4_MINMAX        (MEE_ERROR_BASE +  8)
#define MEE_MINMAX_FIND_FAILURE         (MEE_ERROR_BASE +  9)
#define MEE_UNIMPLEMENTED               (MEE_ERROR_BASE + 10)
#define MEE_ATTR_NOT_AVAILABLE          (MEE_ERROR_BASE + 11)
#define MEE_NO_MEMBERS_IN_ITEM			(MEE_ERROR_BASE + 12)
#define MEE_MEMBER_NOT_FOUND			(MEE_ERROR_BASE + 13)
#define MEE_METHOD_ABORTED   			(MEE_ERROR_BASE + 14)


//#endif 

typedef std::list<long> ITEMID_LIST;

typedef vector<hCitemBase*> ddbItemList_t;
class hCmethodParam;
typedef vector<hCmethodParam> ParamList_t;

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version

//Added By Anil July 19 2005 --starts here
struct DOT_OP_ATTR
{
	string			strDotOpName;
	unsigned int	uiUniqueID;
	unsigned int	uiVarType;	
public:
	DOT_OP_ATTR()
	{
		uiUniqueID = 0;
		uiVarType = 0;
	}

	DOT_OP_ATTR(string strTemp,unsigned int uiTemp1,unsigned int uiTemp2)
	{
		strDotOpName = strTemp;
		uiUniqueID = uiTemp1;
		uiVarType = uiTemp2;
	}
};

typedef std::map<string, DOT_OP_ATTR> DOT_OP_ATTR_MAP;

//Below are the Types of the Dot operator That Can Exists
#define DOT_ID_NONE                 0
#define DOT_ID_LABEL				1
#define DOT_ID_HELP					2
#define DOT_ID_MIN_VALUE			3
#define DOT_ID_MAX_VALUE			4
#define DOT_ID_DFLT_VALUE			5
#define DOT_ID_VIEW_MIN				6
#define DOT_ID_VIEW_MAX				7
#define DOT_ID_COUNT				8
#define DOT_ID_CAPACITY				9
#define DOT_ID_FIRST				10
#define DOT_ID_LAST					11
/* not allowed - stevev 16aug07
#define DOT_ID_DISPLAY_FORMAT		3
#define DOT_ID_EDIT_FORMAT			4
#define DOT_ID_SCALING_FACTOR		7
**/
//Below are the data types ogf Dot Operator
#define DOT_TYPE_INTEGER			1
#define DOT_TYPE_REAL				2
#define DOT_TYPE_DOUBLE				3
#define DOT_TYPE_STRING				4
#define DOT_TYPE_BOOLEAN			5
#define DOT_TYPE_LIST				6

//Added By Anil July 19 2005 --Ends here

class MEE;

class CValueVarient;

class hCgroupItemDescriptor;

class OneMeth
{
private:
	CInterpreter	*m_pInterpreter;
	CHart_Builtins	*m_pBuiltinLib;

	hCddbDevice		*m_pDevice;

	

	/* The Item Id of the method being executed */
	long	m_lMethodItemId;

	char	*m_pchMethodSourceCode;

	/* Item id of the Item for which Pre/post action is being executed */
	long	m_lPrePostItemId;

	/* List that holds the abort methods */
	ITEMID_LIST			abortMethodList;
	ITEMID_LIST::iterator ListIterator;

	/* Flag is set to true if any of the ABORT builtins are called */
	bool	m_bMethodAborted;

	/* Flag that is set when save_values is called */
	//bool	m_bSaveValues;
	int previousRetryCount;

public:
	/* HART only method info */

    int m_iAutoRetryLimit;
    
	unsigned char m_byCommAbortMask;
    unsigned char m_byCommRetryMask;
    unsigned char m_byStatusAbortMask;
    unsigned char m_byStatusRetryMask;
    unsigned char m_byRespAbortMask[RESP_MASK_LEN];
    unsigned char m_byRespRetryMask[RESP_MASK_LEN];
    bool          m_byReturnNodevAbortMask;
    bool          m_byReturnNodevRetryMask;

	unsigned char m_byXmtrCommAbortMask;
    unsigned char m_byXmtrCommRetryMask;
    unsigned char m_byXmtrStatusAbortMask;
    unsigned char m_byXmtrStatusRetryMask;
	unsigned char m_byXmtrRespAbortMask[RESP_MASK_LEN];
    unsigned char m_byXmtrRespRetryMask[RESP_MASK_LEN];
    bool          m_byXmtrReturnNodevAbortMask;
    bool          m_byXmtrReturnNodevRetryMask;
    unsigned char m_byXmtrDataAbortMask[DATA_MASK_LEN];
    unsigned char m_byXmtrDataRetryMask[DATA_MASK_LEN];
	

public:
	OneMeth():m_pInterpreter(NULL)
		, m_pBuiltinLib(NULL)
		, m_pDevice(NULL)
		, m_lMethodItemId(0)
		, m_pchMethodSourceCode(NULL)
		, m_lPrePostItemId(0)
		, m_bMethodAborted(false)
		, previousRetryCount(0)
		, m_pMEE(NULL)
	{
		m_iAutoRetryLimit = 0;

		m_byCommAbortMask = 0;
		m_byCommRetryMask = 0x7F;

		m_byStatusAbortMask = DEVICE_MALFUNCTION;
		m_byStatusRetryMask = 0;

		m_byReturnNodevAbortMask = true;
		m_byReturnNodevRetryMask = false;
		
		m_byXmtrCommAbortMask = 0;;
		m_byXmtrCommRetryMask = 0x7F;

		m_byXmtrStatusAbortMask = DEVICE_MALFUNCTION;
		m_byXmtrStatusRetryMask = 0;

		m_byXmtrReturnNodevAbortMask = true;
		m_byXmtrReturnNodevRetryMask = false;
		
		int i = 0;
 		for(i = 0; i < RESP_MASK_LEN; i++)
		{
			m_byRespAbortMask[i] = 0;
			m_byRespRetryMask[i] = 0;
			m_byXmtrRespAbortMask[i] = 0;
			m_byXmtrRespRetryMask[i] = 0;
		}

		/*ACCESS_RESTRICTED*/
		m_byRespAbortMask[ACCESS_RESTRICTED_BYTE]     = 
		m_byXmtrRespAbortMask[ACCESS_RESTRICTED_BYTE] = 
											0x01 << ACCESS_RESTRICTED_BIT;
		/*CMD_NOT_IMPLIMENTED*/
		m_byRespAbortMask[CMD_NOT_IMPLIMENTED_BYTE]     = 
		m_byXmtrRespAbortMask[CMD_NOT_IMPLIMENTED_BYTE] = 
											0x01 << CMD_NOT_IMPLIMENTED_BIT;

		for(i = 0; i < DATA_MASK_LEN; i++)
		{
			m_byXmtrDataAbortMask[i] = 0;
			m_byXmtrDataRetryMask[i] = 0;
		}
	}

	~OneMeth()
	{
		if (m_pInterpreter != NULL)
		{
			delete m_pInterpreter;
		}
		if (m_pBuiltinLib != NULL)
		{
			delete m_pBuiltinLib;
		}
	}


	bool ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId, long lVarItemId = 0);
	//Anil Octobet 5 2005 for handling Method Calling Method
	//Added Overloaded function 
	bool ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar,long lVarItemId = 0);

	int abort();

	int process_abort();

	int _add_abort_method(long lMethodId);

	int _remove_abort_method(long lMethodId);

	int remove_all_abort();

	void save_values();
	
	//Added By Anil July 01 2005 --starts here
	void discard_on_exit();
	//Added By Anil July 01 2005 --Ends here

	bool GetMethodAbortStatus(); 

	bool RefreshWaveform(long lRfrshMethID);

	int ExecuteActions(ddbItemList_t actionList);

	/*Arun 190505 Start of code */

	int _push_abort_method(long lMethodId);

	int _pop_abort_method();

	/*End of code*/

	RETURNCODE ExecuteActionsInMethod(vector<itemID_t> actionList);

private:
	INTERPRETER_STATUS ExecMethod(long lMethodItemId);
	//Anil Octobet 5 2005 for handling Method Calling Method
	//Added an OverLoaded function
	INTERPRETER_STATUS ExecMethod(long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar);
	void	AllocLibrary();
	void	FreeLibrary();
	void	DisplayMessage(char *pchMessage, bool bUserAcknowledgeValue = true);

public:
	MEE				*m_pMEE; //Vibhor 010705: Made it public to expose global symbol table

};

/*Vibhor 061204: Start of Code*/
class hCVar;
class MEE
{
private:
	
	hCddbDevice		*m_pDevice;
	/* The Item Id of the method being executed */
	long	m_lMethodItemId;
	unsigned int m_NoOneMethInstance;

//	vector <OneMeth*> MethStack; //To support methods calling methods

	
	
public:
	/*Exposed interfaces*/
	unsigned int IncreaseOneMethRefNo()
	{
		return(++m_NoOneMethInstance);
	}

	unsigned int DecreaseOneMethRefNo()
	{
		return(--m_NoOneMethInstance);

	}

	unsigned int GetOneMethRefNo()
	{
		return m_NoOneMethInstance;
	}	
	bool m_bSaveValues;

	bool ExecuteMethod(hCddbDevice *pDevice, long lMethodItemId); //Method Execution call

	bool ExecuteMethod(hCddbDevice *pDevice, long lMethodItemId, long lVarItemId); //Var-Action Execution call

	/* this should not be used - stevev 06mar07
	bool ExecuteMethod(vector<unsigned long> actionIDList); 
	*/

	MEE ():m_pDevice(NULL)
		   ,m_lMethodItemId(0)
		   //,MethStack.clear()
		   ,latestMethodStatus(me_NoError)
	{
		BuildDotOpNameToAttrMap();
		BuildSecondaryAttrList();
		m_NoOneMethInstance = 0;
		m_bSaveValues =false;
	
	}

	~MEE()
	{

	}
	
/*Vibhor 060705: Start of Code*/

	CSymbolTable m_GlobalSymTable; //Vibhor 010705: Added for DD Variables

	//accessors for Global (DD) data in interpreter

	long FindGlobalToken(const char* pszTokenName,long lLineNum,DD_ITEM_TYPE &DDitemType); /*Finds the token in Global symbol table
														If found returns the index else
														Searches the Device for the same,
														If found makes an entry in the global symbol table 
														& returns the index
														Otherwise returns -1, i.e. Not found
	                                                     *///changet the Functio parameter Anil September 16 2005
	
	
	void SetVariableValue(const char* pszVariableName,int iValue);
	void SetVariableValue(const char* pszVariableName,float fValue);
	void SetVariableValue(const char* pszVariableName,double dValue);
	void SetVariableValue(const char* pszVariableName,char* pszValue);
	void SetVariableValue(const char* pszVariableName,bool bValue);
	
/*Vibhor 060705: End of Code*/
	string          methodNameString;// stevev 21sep07 - for help in debugging
	methErrors_t    latestMethodStatus;

private:
	void RemoveLanguageCode(const char* szOriginalString, char** szWithoutLanguageCode);//Added By Anil July 11 2005 Utility Function to get rid of Language Code
	void BuildDotOpNameToAttrMap();//Anil July 19 2005
	
	//Added By Anil August 22 2005 --starts here
	RETURNCODE GetDDItemAttrValue(hCitemBase* pIB, const char* szDotExpression,INTER_VARIANT* pvar);
	RETURNCODE GetDDItemAttrValue(hCitemBase* pIB, const char* pAttrName,CValueVarient* pvar, hCitemBase*& rpIB);
	RETURNCODE GetAttrValue(hCitemBase*  pIB, char* pAttrName, CValueVarient* pVal, INTER_VARIANT* pvar, hCitemBase*& rpIB);

	void BuildSecondaryAttrList();	
	RETURNCODE GetGroupItemPtr(hCitemBase* pIBFinal,int iIndexValue,hCgroupItemDescriptor** pGID);
	//Below attribute Doesn't have any Datatype. 
	//If any DD Expression Ends with this, then it is invalid
	vector<string> m_svSecondaryAtt;
	//Please see the MEE.cpp file for the function comments
	RETURNCODE ResolveExp(const char* szDDitemName,const char* szDDExp,unsigned long ulLenOfexpToResolve,hCitemBase**  pIBFinal);
	RETURNCODE ValidateExp(const char* szDDExp, char** szLastAttr,bool &bPrimaryAttr);
	RETURNCODE GetDDVarValNEvalAssType(hCVar*  pIBFinal,INTER_VARIANT* pvar,CValueVarient* tmpCV,RUL_TOKEN_SUBTYPE AssignType = RUL_ASSIGN);
	RETURNCODE GetDDVarVal(hCVar*  pIBFinal,INTER_VARIANT* pvar);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE GetCallerArgList(const char* szDDitemName,const char* szDDExp,vector<string>* strvCallerArgList);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE GetCallerArgList(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,vector<string>* strvCallerArgList);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE ValidateMethodArgs(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,ParamList_t* TempParamList);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE UpdateCalledMethArgInfo(METHOD_ARG_INFO_VECTOR*vectMethArgInfo, ParamList_t* TempParamList,vector<string>* strvCallerArgList);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE UpdateRetunMethArgInfo(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,hCmethodParam* phCmethodReturnValue,vector<INTER_VARIANT>* vectInterVar);
	
public:
	//Added By Anil August 22 2005 --starts here
	//Please see the MEE.cpp file for the function comments
	DOT_OP_ATTR_MAP m_MapDotOpNameToAttr;
	DOT_OP_ATTR_MAP::iterator m_MapDotOpNameToAttrIter;	
	DOT_OP_ATTR     DotOpAttr[12];
	/* added stevev 16aug07 to map legal attributes to DD item types */
typedef vector<int>      DOT_OP_LIST_t;// we just need the names   eg DOT_ID_LAST
typedef DOT_OP_LIST_t::iterator  DOT_OP_ITER_t;
	DOT_OP_LIST_t		 AvailAttrOP[ iT_MaxType ]; // an array of DOT_ID vectors

	RETURNCODE ResolveDDExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar);
	RETURNCODE ResolveNUpdateDDExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar,RUL_TOKEN_SUBTYPE	AssignType = RUL_ASSIGN);
	RETURNCODE ResolveDDExpForBuiltin(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar, hCitemBase** ppDevObjVar);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE ResolveMethodExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar,vector<INTER_VARIANT>* vectInterVar,METHOD_ARG_INFO_VECTOR* vectMethArgInfo);
	bool IsDDItem(const char* pszDDItemName);
	//Added By Anil August 22 2005 --Ends here

	/* * * * * added by stevev 15aug07 * * * * */
	RETURNCODE ResolveComplexReference(char* ReferenceString, 
			    /* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar);
	RETURNCODE ResolveDot( hCitemBase* pLeftItem, char* RightString, 
	/* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar);
	RETURNCODE ResolveBracket( hCitemBase* pLeftItem, char* RightString, 
		/* return values: */   CValueVarient* pValue, hCitemBase*& rpDevObjVar);
	// helpers of helpers.........................
	bool isAttr(char* pAttrStr);
	RETURNCODE getAttrInfo(		hCitemBase* pLeftItem, char* attributeName, 
		/* return values: */    CValueVarient* pValue, hCitemBase*& rpDevObjVar);
	RETURNCODE getMemberInfo(	hCitemBase* pLeftItem, const char*   memberName, 
		/* return values: */    CValueVarient* pValue, hCitemBase*& rpDevObjVar);
	/* * end stevev additions 15auf07 * */



};

#endif /* MEE_H */

/*Vibhor 061204: End of Code*/

/***************************************************************************************
Vibhor : 071204: Comment

The MEE is now enhanced to support more than one methods in a single Device->ExecMethod 
instance call. So for a device still only one method will execute at a time, but for the 
Method environment, there could be other methods being executed due to the execution sequence,
e.g Actions on Waveforms being displayed on a graph in a method, or methods calling methods.

But overall, the method execution will be sequential. In this whole design, the device , 
method_interface and the MEE are Global objects. Each instance of a method now executes, in
a OneMeth object and has its own Interpreter and BuiltinLib.But the UI interactions from all
child methods(those not explicitly called from the device) will be on the same method UI as 
for the Parent method. 

/***************************************************************************************/













