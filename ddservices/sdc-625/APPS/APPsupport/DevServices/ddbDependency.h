/*************************************************************************************************
 *
 * $Workfile: ddbDependency.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The container class for dependency information
 *		20jan05	sjv	created 
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 * #include "ddbDependency.h"
 */

#ifndef _DDBDEPENDENCY_H
#define _DDBDEPENDENCY_H

#include "ddbGeneral.h"
#include "ddbTracking.h"
#include "ddbGlblSrvInfc.h"

extern ddbItemList_t globalDependencyTrail;// the global trail

/* which dependency we are working with inside of itembase*/
typedef enum itemBaseDependencyType_e
{
	ib_ErrorDep    = 0
	, ib_ValidityDep
	, ib_CondDep 
	, ib_Common			// stevev 14sep06 - we must consolidate for critical
	, ib_Contain		// stevev 27sep06 - parent derivation
} 
ibWhichDependency_t;

class hCdependency : public hCobject
{
	hCitemBase* pOwner;

protected:
	bool didValidity;
	bool didConditional;

public:
	ddbItemList_t  iDependOn;	// aka iContain		iAmConditionalOn
	ddbItemList_t  dependsOnMe; // aka containsMe   *conditional is oneway - up

public:
	hCdependency(DevInfcHandle_t h);
	hCdependency(hCitemBase* owner);// sets owner at instantiation
	virtual ~hCdependency(){iDependOn.clear();dependsOnMe.clear();};

	void setOwner(hCitemBase* owner){pOwner=owner;};

	virtual int  fillValidityList(void);// returns size:fills iDependOn from owner's validity
	virtual int  fillConditionalList(void);	// call's owner's getDepends(), returns size
	virtual int  fillIContainList(void);//stevev 27sep06 - used to acquire parents

	virtual void makeDependTree(ddbItemList_t& behind, ibWhichDependency_t depType, int depth = 0);
	//		checks the behindTrail for circular dependency
	//		if any depend on me then insert me as critical item
	//		recurse down the list -- VALIDITY ONLY

	void setValidDone(){didValidity=true;};
	bool is_ValidDone(){return (didValidity);};
	hCdependency& operator=(const hCdependency& s);

	RETURNCODE  informDependents(ibWhichDependency_t depType);// adds self to other's dependsOnMe

	bool itemDependsOnMe(hCitemBase* pI);// check the dependsOnMe list for pI
	bool iDependOnItem(  hCitemBase* pI);// check the iDependOn   list for pI
	/* global insertUnique so we only have it once 3jun06 */
	static RETURNCODE insertUnique(ddbItemList_t& insert2List, hCitemBase* p2insert);
	/* global uniquely insert all items in list to destlist */
	static RETURNCODE insertUnique(ddbItemList_t& insert2List, ddbItemList_t& l2insert);
#ifdef _DEBUG
	void clogDependency(void);
#endif
};

#endif //_DDBDEPENDENCY_H
