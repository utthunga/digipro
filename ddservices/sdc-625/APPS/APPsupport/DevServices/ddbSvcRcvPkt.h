/*************************************************************************************************
 *
 * $Workfile: ddbSvcRcvPkt.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This a virtual base class that describes the service that the command dispatcher 
 *		supplies the comm interface.  A separate base class to avoid include file complexity.
 *
 *		
 * #include "ddbSvcRcvPkt.h".
 *		
 */

#ifndef _DDBSVCRCVPKT_H
#define _DDBSVCRCVPKT_H

#include "pvfc.h"
#include "HARTsupport.h" // pkt description

class hCcommand;

// pure virtual base class for dispacher supplied service to the commInfc
class hCsvcRcvPkt
{
public:
	virtual void serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo) PVFC( "hCsvcRcvPkt" );
	// pointer2Pkt ownership stays with caller & will be destroyed when the function returns
/*** no longer used - moved to comm class	
	virtual void serviceAsyncReceive(void) = 0;
	// asyncronous handling of asycronous response codes
***/
/** stevev 18may07::>  making the following ifdef XMTRDD - totally confuses the linker and
	very weird things happen - so it exists for all, only a simulator needs to implement it.
	-- used when reading in a comm log, processes stx and ack together ---
***/
	virtual void servicePacketPair(hPkt* pRqstPkt, hPkt* pRplyPkt) PVFC( "hCsvcPktPair" );

/** stevev 26feb09 force all command dispatchers to deal with block transfers.  This function
   must be included in all hCcmdDispatcher's.  serviceReceivePacket calls this when it finds
   a cmd 111 or 112.  This, in turn, handles the transport and session layers and links with 
   the proper port which handles the presentation layer.  
***/
	virtual void serviceTransferPacket(hPkt* pPkt, hCcommand* pCmd, int transNum) 
																		PVFC( "hCsvcTransPkt" );

/** stevev 07oct10 we need a way for the comm interface task to use/clear the message cycle
	queue.  This is the easiest way to handle it.
***/
	virtual void serviceMsgCycleQue(int serviceType)PVFC( "hCsvcMsgCycleQue" );// type 0 is clear it
};

#endif // _DDBSVCRCVPKT_H

/*************************************************************************************************
* NOTES:
*  The command dispatcher owns the comm interface class.  The comm interface class supplies
* the send command services to the dispatcher. But the dispatcher must supply a received packet
* service to the comm interface.  This receive service may run in another task's time slot!
* The dispatcher supplies this service as a pointer to method to the comm interface.
* The comm interface is responsible for running the receive service and the dispatcher runs
* the send services as it sees fit. (ie Receive tasking is comm interface responsibility & send
* or read/write service tasking is the dispatcher responsibility).
*    Notice that this is a void function-- the dispatcher has the responsibility to handle any
* errors that may occur, the comm interface knows nothing about those.
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbSvcRcvPkt.h $
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/11/03    Time: 7:45a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */
