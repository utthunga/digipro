/*************************************************************************************************
 *
 * $Workfile: ddbDevice.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the Device class  - holds the error device services too
 *		5/7/2	sjv	creation
 *
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */
//#define MEMSIZE_CHK 1

#include <stdarg.h>
#include "ddbGeneral.h"
#include "logging.h"
#include "ddbDevice.h"
#include "ddb.h"

// no longer needed (it's empty)#include "ddb_UIInfc.h"
#include "logging.h"
#include "GraphicInfc.h"

//#ifndef XMTRDD

#include "Interpreter.h"
#include "Hart_Builtins.h"
#include "MEE.h"
#include "ddbItems.h"

#include "LitStringTable.h"
//#endif 
#include "ddbMutex.h"

#ifdef _DEBUG
#include <time.h>
// #define STARTTIME_CHECK	/* also in ddbCommand.cpp */

int isInstantiated = 0;

#ifdef STARTTIME_CHECK
extern DWORD startOfInstantiation, lastTickCnt, thisCnt;
extern unsigned long ddbGetTickCount();
#endif

#ifdef MEMSIZE_CHK
  #undef TRUE
  #define TRUE 1
  #undef FALSE
  #define FALSE 0
  #undef BYTE

  #include "..\..\SDC625\stdafx.h"       /* for memory tests */

  #include <crtdbg.h>
  #include <winnt.h>
  #include <afx.h>
  #undef TRUE
  #define TRUE "use true"
  #undef FALSE
  #define FALSE "use false"
#endif

#endif

#ifdef COUNT_MEMORY
extern void DUMPCHUNK(void);
 #ifndef XMTRDD
// we don't need this except when debugging startup  #define CHECK_NOTIFY /*undef to stop report*/
// #define LOG_SUCCESS
 #endif
#else
#define DUMPCHUNK() 
#endif

const elementID_t MTelem = {-1,0};

unsigned int hCddbDevice::getMEEdepth()
{
	if(NULL != pMEE)
	{
		return (pMEE->GetOneMethRefNo());
	}
	return 0;

}

hCddbDevice::hCddbDevice(DevInfcHandle_t h) : hCobject(h),
	varInstanceLst(h),		 cmdInstanceLst(h),
	menuInstanceLst(h),		 edDispInstanceList(h),
	methodInstanceList(h),	 refreshInstanceList(h),
	unitRelInstanceList(h),	 waoInstanceList(h),
	itemArrayInstanceList(h),collectionInstanceList(h),
	valArrayInstanceList(h), fileInstanceList(h),	
	chartInstanceList(h),	 graphInstanceList(h),
	axisInstanceList(h),	 waveformInstanceList(h),
	sourceInstanceList(h),	 listInstanceList(h),
	gridInstanceList(h),	 imageItemInstanceList(h)
{
	//Anil 140906 Added the below code
	pMEE = NULL;
	pMEE = new MEE;
	devDDkey = 0;
	isDDKeyPresent = false;
	ddbDeviceID.clear();
	pCmdDispatch = NULL;
	pItemMutxCntrl  = itemMutex.getMutexControl("ItemMutex");
	compatabilityMode = DEFAULT_COMPATABILITY;

	enableDispatchDeletion = false;
	/*Vibhor 041203: Start of Code*/
	pDevMgr = NULL;
	/*Vibhor 041203: End of Code*/
	/* stevev - 2/16/04 - track the device's time of existence*/
	updateCnt = 0;
	weightPerQuery = 0;
	/* stevev - 2/16/04 - end */
	nLastMethodsReturnCode = 0; // init to a value 08may07
	nextPsuedoID = FIRSTPSUEDOID; // stevev 30mar05
	setDeviceState( ds_OffLine);
	dictionary = NULL;
	literalStringTbl = NULL;
	idBase = 0;
	devIsExecuting = false;
	OpMode = om_Nothing;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"> device created in constructor.\n");
}


#ifdef _DEBUG
	extern map <void*,int> exElemMap;// in ddbReference
#endif
hCddbDevice::~hCddbDevice( )
{

LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "< device deleteing.\n");

	destroy();
	if(pMEE)
	{
		delete pMEE;
		pMEE = NULL;
	}
	if ( pCmdDispatch != NULL && enableDispatchDeletion)
	{
		LOGIT(CERR_LOG,"Device destroy did not destroy the cmd dispatcher.\n");
		/* point of closing crash - stopped calling destructor due to crash. see ddbQueue ****/
		RAZE(pCmdDispatch); // just in case
		(pCmdDispatch) = NULL;

LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "< device deleted dispatcher in distructor.-NOT\n");
	}
	// else it's already gone or somebody else is responsible

	if ( pItemMutxCntrl)
	{
		pItemMutxCntrl->relinquishMutex();
		delete pItemMutxCntrl;
		pItemMutxCntrl = NULL;
	}
	if (literalStringTbl)
	{
		delete literalStringTbl;
		literalStringTbl = NULL;
	}
	
#ifdef INI_OK	
	if ( closeIni() )
	{// error
		LOGIT(CERR_LOG|COUT_LOG,
			"ERROR: +++++++++++ ini file would not close ++++++++++++++\n");
//		exit_gracefully("NO FILE");
		return;
	}
#endif // INI_OK
#ifdef X_DEBUG
	if ( exElemMap.size() > 0 )
	{
		map <void*,int>::iterator miT;
		for (miT = exElemMap.begin(); miT != exElemMap.end(); ++miT)
		{
			LOGIT(CLOG_LOG,"EXELEM: 0x%p still registered from ",miT->first);
			if ( miT->second == 1)
			{
				LOGIT(CLOG_LOG,"Default Constructor\n");
			}
			else
			if (miT->second == 2 )
			{
				LOGIT(CLOG_LOG," Copy  Constructor\n");
			}
			else
			{
				LOGIT(CLOG_LOG," Unknown  Constructor(0x%x)\n",miT->second);
			}
		}
	}
#endif
};


// this is just too much stuff to copy - removed to force an error if ever needed
// hCddbDevice::hCddbDevice(const hCddbDevice& src){cerr<<"----StubbedOut hCddbDevice copy constructor"<<endl;};
//
// for debugging only
int entryCnt = 0;
// a method to remove all the memory and restore the device to new 
// but with interface intact
void hCddbDevice::destroy(void)
{
	entryCnt++;	
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"EXELEM Destroy device %I64x (%d)\n",devDDkey, entryCnt);
	int r = 0;
/*Vibhor 050404: Start of Code*/
	try{
	if ( enableDispatchDeletion && pCmdDispatch != NULL)
		// we own this instance of the dispatcher [[NOT the CommInfc]]
	{
		if ( (r = pCmdDispatch->shutdownDispatch()) == SUCCESS )
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "Device shut down the dispatcher\n");
		}
		else 
		if ( r == -1 )
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "CommInfc still receiving - Possible error on the horizon.\n");
			// try again
			r = pCmdDispatch->shutdownDispatch();
		}
		logTime();// isa logif start_stop (has newline)

		if ( r == SUCCESS)
		{
			RAZE( pCmdDispatch );
		}
		else
		{
			LOGIT(CERR_LOG,"Command Dispatcher did not shutdown: it will be a memory leak.\n");
		}
	}
//	pHartComm->reset();
//	systemKey.clear();
	// must be done first!!!!!!
	fileInstanceList.destroy();
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device destroyed files in destroy.");
							logTime();// isa logif start_stop (has newline)
	
	ddbDeviceID.clear();
	itemArrayInstanceList.destroy();
	collectionInstanceList.destroy();
	valArrayInstanceList.destroy(); //Vibhor 210904: Adding HART 6 stuff
	listInstanceList.destroy();	
	waveformInstanceList.destroy();  
	sourceInstanceList.destroy();	 
	chartInstanceList.destroy();
	graphInstanceList.destroy();	 
	axisInstanceList.destroy();		 
	gridInstanceList.destroy();
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device destroyed instances in destroy.");
							logTime();// isa logif start_stop (has newline)

	varInstanceLst.destroy();
	cmdInstanceLst.destroy();
	menuInstanceLst.destroy();
	edDispInstanceList.destroy();
	methodInstanceList.destroy();
	refreshInstanceList.destroy();
	unitRelInstanceList.destroy();
	waoInstanceList.destroy();
	imageItemInstanceList.destroy();
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device destroyed all lists in destroy.");
							logTime();// isa logif start_stop (has newline)

	// AsyncAvail		- leave connection information
	// DBnotAvailable	- leave connection information
	DeviceDescStrings.clear();
	if ( pItemMutxCntrl)
	{
		pItemMutxCntrl->relinquishMutex();
		delete pItemMutxCntrl;
		pItemMutxCntrl = NULL;
	}

	
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device gave up item mutex.");
							logTime();// isa logif start_stop (has newline)


	// empty the images
	if (imageInstanceList.size() > 0)
	{
		for (ImagePtrList_it i = imageInstanceList.begin(); i != imageInstanceList.end();++i)
		{// isa ptr2aPtr2a hCimage
			delete (*i);
			*i = NULL;
		}
	}

	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device destroyed the images.");
							logTime();// isa logif start_stop (has newline)
	systemSleep(50);
/*** comment out for now...creator should delete this........................................
	RAZE(pCmdDispatch);
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< device deleted dispatcher in destroy.");
							logTime();// isa logif start_stop (has newline)
******/
	// empty the map - moved to last action 28feb12
	for (itemMap_t::iterator 
		 iT  = masterMap.begin(); 
		 iT != masterMap.end(); 
		 ++iT)
	{//iT is ptr2pair<ulong, hCitemBase*>...so ->first is the key, ->second is the hCitemBase*
		iT->second = NULL;
	}
	masterMap.clear();
			

	/* stevev - 2/16/04 - track the device's time of existence*/
	updateCnt = 0;
	/* stevev - 2/16/04 - end */
	}
	catch(...)
	{
		LOGIT(CERR_LOG,"void hCddbDevice::destroy(void) inside catch(...) !!\n");
	}
/*Vibhor 050404: End of Code*/

#ifdef _DEBUG
	LOGIT(CLOG_LOG,"EXELEM: device %I64x\n",devDDkey);
	DUMPCHUNK();
	isInstantiated = 0;
#endif
}



#ifdef _DEBUG
void dumpDev(aCdevice* pDev);
#endif

OperatingMode_t hCddbDevice::getOpMode(void)
{
	return OpMode;
}

RETURNCODE hCddbDevice::populateStandard(DD_Key_t DDkey, DevInfcHandle_t newHndl, aCdevice* pADev)
{
	RETURNCODE rc = SUCCESS;
	bool isServer = false;

	returnItemMutex();  // be sure its available
//TURN-OFF DUMP	dumpDev(pAbstractDev); // dump from the application side of the interface

	// fill in our variables
	devDDkey     = DDkey;
	isDDKeyPresent = true;	
LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "> Populating Standard Tables\n");
	hCdeviceSrvc* pD = (hCdeviceSrvc*)this;

/* VMKP disabled on 200204: Since dispatcher is not required for a Standard DD Loading,
	Error in else part is showing up in error log, which is not intended. */
	
/*	if (pCmdDispatch != NULL)
	{
		hSdispatchPolicy_t policy;
		policy = pCmdDispatch->getPolicy();
		if ( policy.isReplyOnly )
			isServer = true;	// we are a device
		else
			isServer = false;	// we are a host

	}
	else
	{
		LOGIT(UI_LOG,"Programmer error: null command dispatcher.\n");
	}*/

	// load the Lists from the dd prototype
	rc  = varInstanceLst.        populate(pD,newHndl,devDDkey, pADev, isServer);// the only one that matters
	rc |= cmdInstanceLst.        populate(pD,newHndl,devDDkey, pADev);
	rc |= menuInstanceLst.       populate(pD,newHndl,devDDkey, pADev);

	rc |= edDispInstanceList.    populate(pD,newHndl,devDDkey, pADev);
	rc |= methodInstanceList.    populate(pD,newHndl,devDDkey, pADev);
	rc |= refreshInstanceList.   populate(pD,newHndl,devDDkey, pADev);
	rc |= unitRelInstanceList.   populate(pD,newHndl,devDDkey, pADev);
	rc |= waoInstanceList.       populate(pD,newHndl,devDDkey, pADev);
	rc |= itemArrayInstanceList. populate(pD,newHndl,devDDkey, pADev);
	rc |= collectionInstanceList.populate(pD,newHndl,devDDkey, pADev);
	rc |= valArrayInstanceList.  populate(pD,newHndl,devDDkey, pADev); //Vibhor 210904: Adding HART 6 stuff
	rc |= fileInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= chartInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= graphInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= axisInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= waveformInstanceList.  populate(pD,newHndl,devDDkey, pADev);
	rc |= sourceInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= listInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= gridInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= imageItemInstanceList. populate(pD,newHndl,devDDkey, pADev);
	



	/* stevev 18jan05 - acquire the typedef info, now that we have a full set */
	valArrayInstanceList.fillTypedefs();
	listInstanceList.fillTypedefs();


//	enableDispatchDeletion = devOwnsDispatch;
//clog << "> device initializing dispatcher."<<endl;
//	pCmdDispatch->initDispatch(newHndl); 
//	if ( rc == SUCCESS )
//	{
//		rc = collectXrefInfo();
//	}

LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, ">Standard device initialized.\n");
	return rc;

}

// RETURNCODE hCddbDevice::instantiate(unsigned long incomingDDkey /*= 0*/)
// called to factor the prototype into a device (by the Device Manager)
RETURNCODE hCddbDevice::instantiate
		(DD_Key_t DDkey, DevInfcHandle_t newHndl, aCdevice* pADev, 
			hCcmdDispatcher* pCD,
			hCMethSupport*   pMethSupportInterface,
			hCFileSupport*   pFileSupport,/* stevev 09nov04 */
			bool  devOwnsDispatch )
{
	RETURNCODE rc = SUCCESS;
	bool isServer = false;
	
	// fill in our variables
	devDDkey     = DDkey;
	isDDKeyPresent = true;
	pCmdDispatch = pCD;

	hSdispatchPolicy_t policy = getPolicy();
	if ( policy.isReplyOnly )
		isServer = true;	// we are a device
	else
		isServer = false;	// we are a host

	if (! policy.isPartOfTok && ( pCD == NULL || pADev == NULL || newHndl < 1 || DDkey == 0 ))
	{
		return APP_PROGRAMMER_ERROR; // parameter error
	}

	//prashant .oct 2003
	m_pMethSupportInterface = pMethSupportInterface;
	m_pFileSupport          = pFileSupport;

	returnItemMutex();  // be sure its available
//TURN-OFF DUMP	dumpDev(pAbstractDev); // dump from the application side of the interface

	// the dispatcher, by neccesity, had to be instantiated before we had a handle
	//move to the end pCmdDispatch->initDispatch(newHndl); 

LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "> device instantiating.\n");
	hCdeviceSrvc* pD = (hCdeviceSrvc*)this;

#ifdef _DEBUG
	int mtime;
#ifndef _WIN32_WCE			// PAW 24/04/09
	struct _timeb timebuffer, endbuffer;
	_ftime( &timebuffer );
#else
	time_t_ce timebuffer;
	time_t_ce timeMSbuffer;
	time_t_ce endbuffer;
	time_t_ce endMSbuffer;
	time_t_ce start_ce;	

	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	timeMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&timebuffer);	// time
#endif
#endif

#ifdef STARTTIME_CHECK
thisCnt = ddbGetTickCount();
LOGIT(CLOG_LOG,">    PopulatingLists.ET:%1.3f (%1.3f)\n",
	  (((float)thisCnt-(float)lastTickCnt)/1000),(((float)thisCnt-(float)startOfInstantiation)/1000 ));
lastTickCnt = thisCnt;
#endif
	// stevev 29aug07 - changed load order
	// load the Lists from the dd prototype
	rc  = varInstanceLst.        populate(pD,newHndl,devDDkey, pADev, isServer);
	// stevev 04nov08- we can't defaultvalue here because of 'lowest value index in array' default
	rc |= valArrayInstanceList.  populate(pD,newHndl,devDDkey, pADev); //Vibhor 210904: Adding HART 6 stuff
	rc |= listInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= itemArrayInstanceList. populate(pD,newHndl,devDDkey, pADev);
	rc |= collectionInstanceList.populate(pD,newHndl,devDDkey, pADev);
	// stevev - 04nov08 -	further population could depend on default values 
	//						(gets rid of unitialized local variable warning too)
	if ( ddbDeviceID.wManufacturer != 0 )// we don't do this when tok is importing a dd
	{
		varInstanceLst.registerListIndexes();
		varInstanceLst.setDefaultValues();
	}

	rc |= edDispInstanceList.    populate(pD,newHndl,devDDkey, pADev);
	rc |= methodInstanceList.    populate(pD,newHndl,devDDkey, pADev);
	rc |= refreshInstanceList.   populate(pD,newHndl,devDDkey, pADev);
	rc |= unitRelInstanceList.   populate(pD,newHndl,devDDkey, pADev);
	rc |= waoInstanceList.       populate(pD,newHndl,devDDkey, pADev);
//  moved to the end
	rc |= chartInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= graphInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= axisInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= waveformInstanceList.  populate(pD,newHndl,devDDkey, pADev);
	rc |= sourceInstanceList.	 populate(pD,newHndl,devDDkey, pADev);
	rc |= gridInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= imageItemInstanceList. populate(pD,newHndl,devDDkey, pADev);

	rc |= cmdInstanceLst.        populate(pD,newHndl,devDDkey, pADev);
	/* stevev 03aug11 - value arrays, lists and files should not have menus in them.
		Since adding the intial ref resolution to the menu-item operator equal, we get a lot
		of false errors resolving items with value-arrays, lists and files (the stuff that
		follows this).  Due to those facts, I'm moving the menues to fill last.  
		This should have zero impact on anything but false error reports.
	rc |= menuInstanceLst.       populate(pD,newHndl,devDDkey, pADev);
	**/
/*  do this last... so we have items to fill from the list */
	// this used to be done at the end of collectXrefInfo() moved here 25jan06
	if ( ddbDeviceID.wManufacturer != 0 )// we don't do this when tok is importing a dd
	{
		// stevev-04nov08-moved up to just after population::> varInstanceLst.setDefaultValues();

		/* stevev18jun06 - must be done before the file is loaded
		   stevev 18jan05 - acquire the typedef info, now that we have a full set 
		   stevev 04nov08 - these could hold container classes and most anything */
		valArrayInstanceList.fillTypedefs();
		listInstanceList.fillTypedefs();
	}
	rc |= fileInstanceList.		 populate(pD,newHndl,devDDkey, pADev);
	rc |= menuInstanceLst.       populate(pD,newHndl,devDDkey, pADev);

	m_criticalItems.fillCritical(pADev);// store the DD binary list for later use

		// instantiate graphics
	if (pADev->AimageList.size() > 0 )
	{
		imageInstanceList.clear();
		AimageList_t::iterator  iImgLst;
		AframeList_t::iterator  iFrmLst;

		hCimage *plocalImg;
		hCframe *plocalFrm;
		imgframe_t* pIF;

		for ( iImgLst  = pADev->AimageList.begin(); 
			  iImgLst != pADev->AimageList.end();  ++iImgLst)
		{// ptr2a AframeList_t (vector of frames)
			if (iImgLst->size() > 0)
			{
				plocalImg = new hCimage(newHndl);
				for (iFrmLst = iImgLst->begin(); iFrmLst != iImgLst->end(); ++iFrmLst)
				{// isa ptr2a imgframe_t
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
					pIF = &(*iFrmLst);
#else
					pIF = iFrmLst;
#endif
					plocalFrm = new hCframe(pIF);
					plocalImg->push_back(plocalFrm);
					plocalFrm = NULL;
				}
			}
			imageInstanceList.push_back(plocalImg);
			plocalImg = NULL;
		}
	}
	/* stevev 18jan05 - acquire the typedef info, now that we have a full set */
	// moved earlier valArrayInstanceList.fillTypedefs();
	// moved earlier listInstanceList.fillTypedefs();

	enableDispatchDeletion = devOwnsDispatch;
LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "> device initializing dispatcher.\n");
	pCmdDispatch->initDispatch(newHndl); 
#ifdef _DEBUG	
#ifndef _WIN32_WCE			// PAW 24/04/09
	_ftime( &endbuffer );	

	mtime = endbuffer.millitm - timebuffer.millitm;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer.time -= 1;
	}
	{
		__int64 dTm = (endbuffer.time - timebuffer.time);
		LOGIT(CLOG_LOG," Population time = %I64d.%d\n",dTm ,mtime);
	}
#else
	GetSystemTime(&st);
	SetTz(&st);
	endMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&endbuffer);	// time

	mtime = endMSbuffer - timeMSbuffer;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer -= 1;
	}
//	LOGIT(CLOG_LOG," Population time = %d.%d\n",(endbuffer - timebuffer),mtime);	00.01.09 crash with 3051
#endif
#endif
	if ( rc == SUCCESS && ddbDeviceID.wManufacturer != 0 )// a non-standard dd [a tokenizer thing]
	{
		LOGIF(LOGP_MISC_CMTS)(STAT_LOG|CLOG_LOG,"Cross-Referencing the DD\n");

#ifdef STARTTIME_CHECK
thisCnt = ddbGetTickCount();
LOGIT(CLOG_LOG,">    Starting Xref.ET:%1.3f (%1.3f)\n",
	  (((float)thisCnt-(float)lastTickCnt)/1000),(((float)thisCnt-(float)startOfInstantiation)/1000 ));
lastTickCnt = thisCnt;
#endif

		rc = collectXrefInfo();
	}
#ifdef _DEBUG // 00.01.11	
#ifndef _WIN32_WCE			// PAW 24/04/09
	_ftime( &timebuffer );	

	mtime = timebuffer.millitm - endbuffer.millitm;
	if (mtime < 0) 
	{	mtime+=1000;timebuffer.time -= 1;
	}
	
	{
		__int64 dTm = (timebuffer.time - endbuffer.time);
		LOGIT(CLOG_LOG," Cross-Reference time = %I64d.%d\n",dTm,mtime);
	}
#else
	GetSystemTime(&st);
	SetTz(&st);
	timeMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&timebuffer);	// time

	mtime = timeMSbuffer - endMSbuffer;
	if (mtime < 0) 
	{	mtime+=1000;timebuffer -= 1;
	}
	LOGIT(CLOG_LOG," Cross-Reference time = %d.%d\n",(timebuffer - endbuffer),mtime);
#endif
	
#endif
/**** TESTCODE ****
	RETURNCODE rcv;
	hCitemBase* pIB = NULL;
	string tstr("ResetMaintenanceMethod");
	rcv = getItemBySymName(tstr, &pIB);
	if (pIB != NULL)
	{
clog << "Test fetch	: item '"<<tstr<<"' got 0x"<<hex<<pIB->getID()<<dec<<"  ("<<rcv<<")"<<endl;;
	}
	else
	{
clog << "Test fetch	: item '"<<tstr<<"' got NULL response ("<<rcv<<")"<<endl;
	}
******************/
LOGIF(LOGP_MISC_CMTS)(CLOG_LOG, "> device initialized.\n");
#ifdef XXXXX_DEBUG /*_______ test insert and remove _________*/
	if (listInstanceList.size() > 0 )
	{
		// get a float ( assume all lists being tested are float )
		hCFloat* pF = NULL;
		vector< hCVar* >::iterator iT;
		int i = 0;

		for (iT = varInstanceLst.begin(); iT != varInstanceLst.end(); ++iT)
		{
			if ( (*iT)->VariableType() == vT_FloatgPt)
			{
				pF = (hCFloat*)(*iT);
				break; // out of for loop
			}
		}
		if ( listInstanceList[0]->insert(pF, i) == SUCCESS )
		{
			listInstanceList[0]->remove(i);
		}
		else
		{
			LOGIT(CERR_LOG,"** List insert test failed.\n");
		}
	}
#endif

	rc |= cmdInstanceLst.BuildIndexMap();

	return rc;
}



RETURNCODE hCddbDevice::collectXrefInfo(void)
{
	RETURNCODE  rc  = SUCCESS;
	UINT32      rid = 0, topID = 0;
	hCitemBase* pIbase = NULL;
	hCVar*      pVar   = NULL;
	itemType_t  iTy;
	vector<hCreferenceList *> payldList;// list of ptrs to refLists
	vector<hCreference*> snglPayldList;// list of ptrs to references
	vector<hCitemBase*>  allRefdPtrs;
	vector<hCVar*>  IndexPtrs;
#ifdef STARTTIME_CHECK
//DWORD cmdCnt = lastTickCnt;
#endif
	// for each command - mark the var usage list
	for ( CCmdList::iterator iTC = cmdInstanceLst.begin(); iTC < cmdInstanceLst.end(); iTC++)
	{// iTC is ptr 2 ptr 2 a hCcommand
		rc |= (*iTC)->updateVarCommandUsage(); // fills the variable command usage lists

#ifdef STARTTIME_CHECK
thisCnt = ddbGetTickCount();
LOGIT(CLOG_LOG,">    VarUsage Cmd %d.  ET:%1.3f\n",(*iTC)->getCmdNumber(),
	  (((float)thisCnt-(float)lastTickCnt)/1000) );
lastTickCnt = thisCnt;
#endif

	}// next command
#ifdef STARTTIME_CHECK
thisCnt = ddbGetTickCount();
LOGIT(CLOG_LOG,">    Updated Var Usage.ET:%1.3f (%1.3f)\n",
	  (((float)thisCnt-(float)lastTickCnt)/1000),(((float)thisCnt-(float)startOfInstantiation)/1000 ));
lastTickCnt = thisCnt;
#endif
// for each command - mark the var usage list
	for ( ClistList::iterator iTL = listInstanceList.begin(); iTL != listInstanceList.end(); ++iTL)
	{// iTL is ptr 2 ptr 2 a hClist
		hClist* pL = (hClist*) (*iTL);
		rc |= pL->resetIndexValues();// if count was set, resets any items' command index/value
	}// next list



	// all the vars have their command lists, make 'em choose one
	int ir = 0;
	hCVar* pV = NULL;
	for (varPtrList_t::iterator ivl = varInstanceLst.begin();ivl < varInstanceLst.end();ivl++)
	{//ivl is a ptr 2 ptr 2 hCVar
		pV = (*ivl);// stop using iterator as a pointer
#ifdef _DEBUG
		itemID_t id = pV->getID();
#endif
		ir =  pV->setRdWrCmds();// hi word = Rd, lo word = Wr commands on return
		if (pV->VariableType() == vT_Index && ! pV->IsLocal())
		{
			IndexPtrs.push_back(pV);
		}
	}
#ifdef STARTTIME_CHECK
thisCnt = ddbGetTickCount();
LOGIT(CLOG_LOG,">     Set the Rd/Wr Commands.ET:%1.3f (%1.3f)\n",
	  (((float)thisCnt-(float)lastTickCnt)/1000),(((float)thisCnt-(float)startOfInstantiation)/1000 ));
lastTickCnt = thisCnt;
#endif
	payldList.clear();
	// for each refresh watch, mark the dominant vars in refresh relations
	for ( CRefreshList::iterator iTR = refreshInstanceList.begin(); 
														iTR < refreshInstanceList.end(); iTR++)
	{// iTR is ptr 2 ptr 2 a hCrefresh
		topID = (*iTR)->getID();
		// for each in watch list, add a dominant
		payldList.clear();
		rc = (*iTR)->getWatchPayLdPtrs(payldList);// all conditional paths
		for (vector<hCreferenceList *>::iterator pll = payldList.begin(); 
																 pll < payldList.end(); pll++ )
		{// pll is a ptr 2 ptr 2 reflist
			// each reference, get a itemid
			for (HreferenceList_t::iterator ir = (*pll)->begin();     ir < (*pll)->end(); ir++)
			{// ir is a ptr 2 a hCreference
				allRefdPtrs.clear();
				//if ( ir->resolveID( rid ) == SUCCESS )
				if (ir->resolveAllIDs(allRefdPtrs) == SUCCESS && allRefdPtrs.size() > 0)
				{
					for (vector<hCitemBase*>::iterator iiB = allRefdPtrs.begin(); 
																iiB < allRefdPtrs.end(); iiB++)
					{// iiB is a ptr 2 ptr 2 hCitemBase
											
						iTy = (*iiB)->getIType();
						if ( iTy == iT_Variable )
						{
							((hCVar*)(*iiB))->setDomRel(topID);							
							// leave rc SUCCESS
						}
						else
						{
							LOGIT(CERR_LOG|CLOG_LOG,"RefreshRel with non-variable watch item.\n");
						}
					}// next
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"RefreshRel could not resolve reference to item.\n");
				}
			}// next reference in the list
		}// next (conditional) watch list
	}// next relation

	// mark the dominant vars in unit relations
	// mark the dependant var in unit relations
	for ( CunitRelList::iterator iTU = unitRelInstanceList.begin(); 
														iTU < unitRelInstanceList.end(); iTU++)
	{// iTU is ptr 2 ptr 2 a hCunitRel
		topID = (*iTU)->getID();
		// for domiant, add to dominant list
		snglPayldList.clear();

		rc = (*iTU)->getIndepdPayLdPtrs(snglPayldList);
		if (rc == SUCCESS && snglPayldList.size() > 0)
		{	
			for (vector<hCreference *>::iterator rPl = snglPayldList.begin(); 
				/* should only be one*/						rPl < snglPayldList.end(); rPl++ )
			{// pll is a ptr 2 ptr 2 reference
				allRefdPtrs.clear();
				//if ( (*rPl)->XXXXresolveID( rid ) == SUCCESS )
				if ((*rPl)->resolveAllIDs(allRefdPtrs) == SUCCESS && allRefdPtrs.size() > 0)
				{
					for (vector<hCitemBase*>::iterator iiB = allRefdPtrs.begin(); 
																iiB < allRefdPtrs.end(); iiB++)
					{// iiB is a ptr 2 ptr 2 hCitemBase

						iTy = (*iiB)->getIType();
						if ( iTy == iT_Variable )
						{
							((hCVar*)(*iiB))->setDomRel(topID);							
							// leave rc SUCCESS
						}
						else
						{
							LOGIT(CERR_LOG|CLOG_LOG,"UnitRel with non-variable independent item. %s\n", itemStrings[iTy]);
						}
					}// next
				}
				else
				{
					LOGIT(CERR_LOG|CLOG_LOG,"UnitRel could not resolve to an item id.\n");
				}
			}// next resolved reference
		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,"UnitRel has no independent variables item.\n");
		}

		// for each in dependent list, set the dependent
		payldList.clear();
		rc = (*iTU)->getDependPayLdPtrs(payldList);
		for (vector<hCreferenceList *>::iterator pll = payldList.begin(); 
																pll < payldList.end(); pll++ )
		{// pll is a ptr 2 ptr 2 reflist
			// each reference, get a itemid
			for (HreferenceList_t::iterator ir = (*pll)->begin(); ir < (*pll)->end(); ir++)
			{// ir is a ptr 2 a hCreference
				allRefdPtrs.clear();
				//if ( ir->XXXXresolveID( rid ) == SUCCESS )					
				if (ir->resolveAllIDs(allRefdPtrs) == SUCCESS && allRefdPtrs.size() > 0)
				{
					for (vector<hCitemBase*>::iterator iiB = allRefdPtrs.begin(); 
																iiB < allRefdPtrs.end(); iiB++)
					{// iiB is a ptr 2 ptr 2 hCitemBase
						iTy = (*iiB)->getIType();
						if ( iTy == iT_Variable)
						{
							((hCVar*)(*iiB))->setUnitRel(topID);							
							// leave rc SUCCESS
						}
						else
						if ( iTy == iT_Axis)
						{
							((hCaxis*)(*iiB))->setUnitRel(topID);							
							// leave rc SUCCESS
						}
						else
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"UnitRel with non-variable,non-axis dependent item.\n");
						}
					}//next
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"UnitRel could not resolve reference to item.\n");
				}
			}// next reference in the list
		}// next (conditional) dependent list
	}// next relation

	// mark the dependant var in wao  relations
	hCwao *      pWAO;
	hCreference* pREF;
	hCitemBase*  pITM;
	int p=0,k=0,r=0;
	for ( CwaoList::iterator iTW = waoInstanceList.begin(); 
														iTW < waoInstanceList.end(); iTW++)
	{// iTW is ptr 2 ptr 2 a hCwao
		pWAO  = (*iTW);
		topID = pWAO->getID();
		// for each in list, set the wao item
		payldList.clear();
		rc = pWAO->getWaoPayLdPtrs(payldList);
		DEBUGLOG(CLOG_LOG,"WAO Payload list %2d long.\n",payldList.size());
		p = 0;
		for (vector<hCreferenceList *>::iterator pll = payldList.begin(); 
																pll < payldList.end(); pll++ )
		{// pll is a ptr 2 ptr 2 reflist
			DEBUGLOG(CLOG_LOG,"%3d Payload's Reference list %2d long.\n",p,(*pll)->size());
			k = 0;
			// each reference, get a itemid
			for (HreferenceList_t::iterator  ir = (*pll)->begin();     ir < (*pll)->end(); ir++)
			{// ir is a ptr 2 a hCreference
				pREF = &(*ir);
				allRefdPtrs.clear();
				
				//if ( ir->XXXXresolveID( rid ) == SUCCESS )					
				if (pREF->resolveAllIDs(allRefdPtrs) == SUCCESS && allRefdPtrs.size() > 0)
				{
		DEBUGLOG(CLOG_LOG,"   %3d  Reference list resolved %2d items.\n",k,allRefdPtrs.size());
					for (vector<hCitemBase*>::iterator iiB = allRefdPtrs.begin(); 
																iiB < allRefdPtrs.end(); iiB++)
					{// iiB is a ptr 2 ptr 2 hCitemBase
						pITM = (*iiB);
						iTy = pITM->getIType();
						if ( iTy == iT_Variable )
						{
							((hCVar*)pITM)->setWaoRel(topID);							
							// leave rc SUCCESS
						}
						else
						if (iTy == iT_Collection || iTy == iT_ItemArray || iTy == iT_File)
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
								"WaoRel with unresolved item. (%s)\n",itemStrings[iTy] );
						}
						else
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
								"WaoRel with non-variable item. (%s)\n",itemStrings[iTy] );
						}
					}//next
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"WaoRel could not resolve reference to item.\n");
				}
			k++;
			}// next reference in the list
		p++;
		}// next (conditional) watch list
	}// next relation in device

	/* must be done after all of the other lists are finished*/
/* steve 25jan06
Note setDefaultValues() has been moved up in the execution sequence 
	It is now done after all of the item lists are loaded except the file items.
	setDefaultValues() is called to set the variables values
	and then the file items are generated, loaded from the OS file(s) and the variables 
	values reset (over the default.
	.............................................
	varInstanceLst.setDefaultValues();
 ***/
	/* stevev - 4/22/04 - setting the default values has been moved to the varList itself */
	/*                  - The new DDL spec allows the writer to specify a default value.  */

	/* dependency notes - cmdInstanceLst, refreshInstanceList, unitRelInstanceList,waoInstanceList
	   , fileInstanceList, 
	   have NO validity
	 */
	varInstanceLst.        calcValidityDependency();
	/* stevev - 21mar05*/
	methodInstanceList.    calcValidityDependency();
	/* 16aug05 - do the rest of 'em * */
	menuInstanceLst.       calcValidityDependency();
	edDispInstanceList.    calcValidityDependency();
	itemArrayInstanceList. calcValidityDependency();
	valArrayInstanceList.  calcValidityDependency();
	chartInstanceList.     calcValidityDependency();
	graphInstanceList.     calcValidityDependency();
	axisInstanceList.      calcValidityDependency();
	sourceInstanceList.    calcValidityDependency();
	imageItemInstanceList .calcValidityDependency();
	gridInstanceList      .calcValidityDependency();
	listInstanceList      .calcValidityDependency();
	collectionInstanceList.calcValidityDependency();
	waveformInstanceList  .calcValidityDependency();

	varInstanceLst.        calcAttributeDependency();
	/* stevev - 21mar05*/
	methodInstanceList.    calcAttributeDependency();
	/* 16aug05 - do the rest of 'em * */
	menuInstanceLst.       calcAttributeDependency();
	edDispInstanceList.    calcAttributeDependency();
	itemArrayInstanceList. calcAttributeDependency();
	valArrayInstanceList.  calcAttributeDependency();
	chartInstanceList.     calcAttributeDependency();
	graphInstanceList.     calcAttributeDependency();
	axisInstanceList.      calcAttributeDependency();
	sourceInstanceList.    calcAttributeDependency();
	imageItemInstanceList .calcAttributeDependency();
	gridInstanceList      .calcAttributeDependency();
	listInstanceList      .calcAttributeDependency();
	collectionInstanceList.calcAttributeDependency();
	waveformInstanceList  .calcAttributeDependency();

	menuInstanceLst.       fillIcontain();
	collectionInstanceList.fillIcontain();// as menu
	sourceInstanceList.    fillIcontain();//variables
	chartInstanceList.     fillIcontain();//sources
	waveformInstanceList  .fillIcontain();//variables
	graphInstanceList.     fillIcontain();//waveforms
	gridInstanceList      .fillIcontain();



/*** stevev 02oct06 - for debugging ***  used only when we have a set of values-of-interest/
	if LOGTHIS( LOGP_DEPENDENCY )
	{
		hCitemBase*    pBase;
		hCdependency*  pDep;
		ddbItemLst_it ppItmBase;
		itemID_t itmArr[] = {0x411d, 0x411e,0x411f,0x4120, 0x4121, 0x4075,
			                 0xe00008e7,0xe00008e8,0xe00008e9,0xe00008ea };

		for (int itmId = 0; itmId < (sizeof(itmArr)/4); itmId++)
		{
			if ( getItemBySymNumber(itmArr[itmId], &(pBase)) == SUCCESS && pBase != NULL)
			{// value-validity
				pDep = pBase->getDepPtr(ib_ValidityDep);
				if ( pDep != NULL)
				{					
					LOGIT(CLOG_LOG,"DEP: IdependOn   Validity/Value for 0x%04x as finished (%s)\n",
						pBase->getID(),pBase->getName().c_str());
					for (ppItmBase = pDep->iDependOn.begin(); ppItmBase != pDep->iDependOn.end();
																					 ++ppItmBase)
					{
					LOGIT(CLOG_LOG,"DEP:               0x%04x (%s)\n",
						(*ppItmBase)->getID(),(*ppItmBase)->getName().c_str());
					}
					
					LOGIT(CLOG_LOG,"DEP: DependsOnMe Validity/Value for 0x%04x as finished (%s)\n",
						pBase->getID(),pBase->getName().c_str());
					for (ppItmBase = pDep->dependsOnMe.begin(); 
												ppItmBase != pDep->dependsOnMe.end(); ++ppItmBase)
					{
					LOGIT(CLOG_LOG,"DEP:               0x%04x (%s)\n",
						(*ppItmBase)->getID(),(*ppItmBase)->getName().c_str());
					}		
				}
				else
				{
					LOGIT(CLOG_LOG,"DEP: ITEM 0x%04x had no validity/value.\n",itmArr[itmId]);
				}
			 // attribute
				pDep = pBase->getDepPtr(ib_CondDep);
				if ( pDep != NULL)
				{					
					LOGIT(CLOG_LOG,"DEP: IdependOn   AttrDepends for 0x%04x as finished (%s)\n",
						pBase->getID(),pBase->getName().c_str());
					for (ppItmBase = pDep->iDependOn.begin(); ppItmBase != pDep->iDependOn.end();
																					 ++ppItmBase)
					{
					LOGIT(CLOG_LOG,"DEP:               0x%04x (%s)\n",
						(*ppItmBase)->getID(),(*ppItmBase)->getName().c_str());
					}
					
					LOGIT(CLOG_LOG,"DEP: DependsOnMe AttrDepends for 0x%04x as finished (%s)\n",
						pBase->getID(),pBase->getName().c_str());
					for (ppItmBase = pDep->dependsOnMe.begin(); 
											ppItmBase != pDep->dependsOnMe.end();  ++ppItmBase)
					{
					LOGIT(CLOG_LOG,"DEP:               0x%04x (%s)\n",
						(*ppItmBase)->getID(),(*ppItmBase)->getName().c_str());
					}		
				}
				else
				{
					LOGIT(CLOG_LOG,"DEP: ITEM 0x%04x had no attribute dependency.\n",itmArr[itmId]);
				}
				LOGIT(CLOG_LOG,"DEP:---------------------------------------------------------\n");	
			}
			else
			{
				LOGIT(CLOG_LOG,"DEP: ITEM 0x%04x did not find.\n",itmArr[itmId]);
			}
		}// next symbol-number-of-interest	
	}
/  *** stevev end 02oct06 ***/

	fillDepTree();


	// stevev 19dec05 - add indexes last - temporary till we do indexes as dependencies
	// stevev 16aug06 - note that IndexPtrs are ONLY Device variable indexes (no locals)
	// stevev 13sep06 - changed insertCritical to put at end
	int thedepth = -1;// insert at new bottom level, change this value to that level
/* remove per PAR 725 - stevev 19jun07 -
	for (vector<hCVar*>::iterator II = IndexPtrs.begin(); II != IndexPtrs.end();++II)
	{//ptr2aptr2a hCVar
		getCriticalPtr()->insertCriticalItem((*II), thedepth);//13sep06
	}
 ** end remove **/
	// end stevev 19dec05
	rc = getCriticalPtr()->consolidateLists((hCdeviceSrvc*)this);
	if LOGTHIS(LOGP_DD_INFO)
		getCriticalPtr()->dumpCritcalItems();
	return rc;
}


void hCddbDevice::fillDepTree(void)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase*   pItem = NULL;
	hCdependency* pDep  = NULL;

	//CVarList		::iterator iT;	/* isa ptr 2a ptr 2a hCVar    */
	//CmethodList		::iterator iTm;	/* isa ptr 2a ptr 2a hCmethod */
				
	//CMenuList		::iterator iTe;
	//CEditDispList	::iterator iTd;
	//CitemArrayList	::iterator iTi;
	//CvalueArrayList	::iterator iTa;
	//CchartList		::iterator iTc;
	//CgraphList		::iterator iTg;
	//CaxisList		::iterator iTx;
	//CsourceList		::iterator iTs;
	//CimageItemList	::iterator iTt;
	//ClistList		::iterator iTl;
	//CgridList		::iterator iTr;

	//CCmdList		cmdInstanceLst
	//CRefreshList	refreshInstanceList
	//CunitRelList	unitRelInstanceList
	//CwaoList		waoInstanceList
	//CcollectionList collectionInstanceList
	//CfileList		fileInstanceList
	//CwaveformList	waveformInstanceList


	/* all of these first - tells those iDependOn that I'm needy */
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"\n  this  - depends on these\n");
	varInstanceLst       .inform_Dependents(ib_ValidityDep);
	varInstanceLst       .inform_Dependents(ib_CondDep);

	methodInstanceList   .inform_Dependents(ib_ValidityDep);
	methodInstanceList   .inform_Dependents(ib_CondDep);

	menuInstanceLst      .inform_Dependents(ib_ValidityDep);
	menuInstanceLst      .inform_Dependents(ib_CondDep);
	menuInstanceLst      .inform_Dependents(ib_Contain);

	edDispInstanceList   .inform_Dependents(ib_ValidityDep);
	edDispInstanceList   .inform_Dependents(ib_CondDep);

	itemArrayInstanceList.inform_Dependents(ib_ValidityDep);
	itemArrayInstanceList.inform_Dependents(ib_CondDep);

	valArrayInstanceList .inform_Dependents(ib_ValidityDep);
	valArrayInstanceList .inform_Dependents(ib_CondDep);

	chartInstanceList    .inform_Dependents(ib_ValidityDep);
	chartInstanceList    .inform_Dependents(ib_CondDep);
	chartInstanceList    .inform_Dependents(ib_Contain);

	graphInstanceList    .inform_Dependents(ib_ValidityDep);
	graphInstanceList    .inform_Dependents(ib_CondDep);
	graphInstanceList    .inform_Dependents(ib_Contain);

	axisInstanceList     .inform_Dependents(ib_ValidityDep);
	axisInstanceList     .inform_Dependents(ib_CondDep);

	sourceInstanceList   .inform_Dependents(ib_ValidityDep);
	sourceInstanceList   .inform_Dependents(ib_CondDep);
	// stevev 28sep06 - these have been rolled into the chart

	imageItemInstanceList.inform_Dependents(ib_ValidityDep);
	imageItemInstanceList.inform_Dependents(ib_CondDep);

	listInstanceList     .inform_Dependents(ib_ValidityDep);
	listInstanceList     .inform_Dependents(ib_CondDep);

	gridInstanceList     .inform_Dependents(ib_ValidityDep);
	gridInstanceList     .inform_Dependents(ib_CondDep);
	gridInstanceList     .inform_Dependents(ib_Contain);

	// these are needed	
	collectionInstanceList.inform_Dependents(ib_Contain);

	/* stevev 14sep06 - changed to work with the consolidated depends into the common dependency. 
	 */
	/* then do these */	
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," this  - is dominant to this tree\n");
	varInstanceLst        .fill_depend_tree(ib_Common);
	methodInstanceList    .fill_depend_tree(ib_Common);
	menuInstanceLst       .fill_depend_tree(ib_Common);
	edDispInstanceList    .fill_depend_tree(ib_Common);
	itemArrayInstanceList .fill_depend_tree(ib_Common);
	valArrayInstanceList  .fill_depend_tree(ib_Common);
	chartInstanceList     .fill_depend_tree(ib_Common);
	graphInstanceList     .fill_depend_tree(ib_Common);
	axisInstanceList      .fill_depend_tree(ib_Common);
	sourceInstanceList    .fill_depend_tree(ib_Common);
	imageItemInstanceList .fill_depend_tree(ib_Common);
	listInstanceList      .fill_depend_tree(ib_Common);
	gridInstanceList      .fill_depend_tree(ib_Common);

	// also clean up these lists to save memory: we no longer need any common or iDependOn	
	varInstanceLst        .clean_Depends();
	methodInstanceList    .clean_Depends();
	menuInstanceLst       .clean_Depends();
	edDispInstanceList    .clean_Depends();
	itemArrayInstanceList .clean_Depends();
	valArrayInstanceList  .clean_Depends();
	chartInstanceList     .clean_Depends();
	graphInstanceList     .clean_Depends();
	axisInstanceList      .clean_Depends();
	sourceInstanceList    .clean_Depends();
	imageItemInstanceList .clean_Depends();
	listInstanceList      .clean_Depends();
	gridInstanceList      .clean_Depends();

	/* stevev 14sep06 - end  addition */ 

/* stevev 14sep06 - Removed:: Commented out :: large  piece of code
	LOGIT(CLOG_LOG," this  - is dominant to this tree\n");
	varInstanceLst        .fill_depend_tree(ib_ValidityDep);
	methodInstanceList    .fill_depend_tree(ib_ValidityDep);
	menuInstanceLst       .fill_depend_tree(ib_ValidityDep);
	edDispInstanceList    .fill_depend_tree(ib_ValidityDep);
	itemArrayInstanceList .fill_depend_tree(ib_ValidityDep);
	valArrayInstanceList  .fill_depend_tree(ib_ValidityDep);
	chartInstanceList     .fill_depend_tree(ib_ValidityDep);
	graphInstanceList     .fill_depend_tree(ib_ValidityDep);
	axisInstanceList      .fill_depend_tree(ib_ValidityDep);
	sourceInstanceList    .fill_depend_tree(ib_ValidityDep);
	imageItemInstanceList .fill_depend_tree(ib_ValidityDep);
	listInstanceList      .fill_depend_tree(ib_ValidityDep);
	gridInstanceList      .fill_depend_tree(ib_ValidityDep);

	LOGIT(CLOG_LOG,"\n============================== Validity complete; start Conditionals=========================\n");
	/x* these should never have any in their dependsonme list
	   this call will generate a one dimenional tree ( a list )
		and insert any iDependOn items into the critical parameter table
		(if it isn't already there)
	*x/
	varInstanceLst        .fill_depend_tree(ib_CondDep);
	methodInstanceList    .fill_depend_tree(ib_CondDep);
	menuInstanceLst       .fill_depend_tree(ib_CondDep);
	edDispInstanceList    .fill_depend_tree(ib_CondDep);
	itemArrayInstanceList .fill_depend_tree(ib_CondDep);
	valArrayInstanceList  .fill_depend_tree(ib_CondDep);
	chartInstanceList     .fill_depend_tree(ib_CondDep);
	graphInstanceList     .fill_depend_tree(ib_CondDep);
	axisInstanceList      .fill_depend_tree(ib_CondDep);
	sourceInstanceList    .fill_depend_tree(ib_CondDep);
	imageItemInstanceList .fill_depend_tree(ib_CondDep);
	listInstanceList      .fill_depend_tree(ib_CondDep);
	gridInstanceList      .fill_depend_tree(ib_CondDep);
/  stevev 14sep06 - End Removal  large  piece of code
***/
}


void hCddbDevice::displayVars(void)
{
	RETURNCODE rc = SUCCESS;
	hCEnum* pEnum = NULL;
	hCenumList vList(devHndl());

	for ( CVarList::iterator vlit = varInstanceLst.begin(); vlit < varInstanceLst.end(); vlit++)
	{//vlit isa ptr 2 ptr 2 hCVar
		if ((*vlit)->VariableType() == ENUMERATED)
		{
			pEnum = (hCEnum*)(*vlit);
			if ( pEnum->procureList( vList ) != SUCCESS )
			{
				LOGIT(CERR_LOG,"Enum would not resolve!\n");
			}
			else
			{
				for (hCenumList::iterator edescIT = vList.begin();// a hCenumDesc*
						edescIT < vList.end(); edescIT++)
				{
	//not implemented				(*edescIT)->procureContents(desc3);
					hCenumDesc* pED = (hCenumDesc*) &(*edescIT);// for new compiler // PAW &* added 03/03/09
					LOGIF(LOGP_MISC_CMTS)(COUT_LOG,"Enum %6d  Val: %3d  Desc:%s  Help:%s.\n",(*vlit)->getID(),
						pED->val, pED->descS.procureVal().c_str(),
						pED->helpS.procureVal().c_str() );
				}
			}
		}
	}

}
void hCddbDevice::dumpSelf(int indent)
{
	LOGIT(COUT_LOG,"%s        DEVICE       \n",Space(indent) );
	string wrkstr;
	for (StrVector_t::iterator iT=	DeviceDescStrings.begin(); 
			iT < DeviceDescStrings.end(); iT++)
	{
		wrkstr = *iT;
		// this should work for compilers...
		LOGIT(COUT_LOG,"%s%s\n",Space(indent), wrkstr.c_str() );
	}
	LOGIT(COUT_LOG,"\n");
	LOGIT(COUT_LOG,"\n%s*-------------VAR LIST-------------------------*\n",Space(indent));
	varInstanceLst.dumpSelf(); // list of variable instances
	LOGIT(COUT_LOG,"\n%s*-------------CMD LIST-------------------------*\n",Space(indent));
	cmdInstanceLst.dumpSelf(); // list of command instances
	LOGIT(COUT_LOG,"\n%s*-------------MEN LIST-------------------------*\n",Space(indent));
	menuInstanceLst.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------EDD LIST-------------------------*\n",Space(indent));
	edDispInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------MET LIST-------------------------*\n",Space(indent));
	methodInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------RFS LIST-------------------------*\n",Space(indent));
	refreshInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------UNT LIST-------------------------*\n",Space(indent));
	unitRelInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------WAO LIST-------------------------*\n",Space(indent));
	waoInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------IAR LIST-------------------------*\n",Space(indent));
	itemArrayInstanceList.dumpSelf();
	LOGIT(COUT_LOG,"\n%s*-------------COL LIST-------------------------*\n",Space(indent));
	collectionInstanceList.dumpSelf();

}

/*
void	hCddbDevice::systemLog(int channel, char* format, ...) 
{
	char strActual[1024];
	va_list vMarker;

	va_start(vMarker, format);
	logoutV(channel, format,  vMarker);
	va_end(vMarker);
	return ;
#if 0
	if (channel & CERR_LOG)
	{
		va_start(vMarker, format);
		vsprintf(strActual, format, vMarker);
		LOGIT(CERR_LOG,"%s\n", strActual);
		va_end(vMarker);
	}
	if (channel & CLOG_LOG)
	{
		va_start(vMarker, format);
		vsprintf(strActual, format, vMarker);
		LOGIT(CLOG_LOG,"%s\n", strActual);
		va_end(vMarker);
	}
	if (channel & COUT_LOG)
	{
		va_start(vMarker, format);
		vsprintf(strActual, format, vMarker);
		LOGIT(COUT_LOG,"%s\n", strActual);
		va_end(vMarker);
	}
	if (channel & UI_LOG)
	{
		va_start(vMarker, format);
		LogMessageV(format, vMarker);
		va_end(vMarker);
	}
#endif
}
void	hCddbDevice::systemLog(int channel, int errNumber,...) 
{
	// TODO: these really need to log somewhere 
	LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: systemLog of an error code is not yet supported.\n");
}
*/


#if 0 /************************** move to the comm interface **********************************/
/**   Moved code removed 27jun05 stevev - see earlier version if you need to what was here   **/
#endif /***************************************************************************************/


// accessors
//RETURNCODE     hCddbDevice::get(int itemID, hCitemBase** returnItem){return FAILURE;};

RETURNCODE hCddbDevice::procure(hCreference& itmRef, hCitemBase** returnItem)
{
	RETURNCODE    rc = FAILURE;
	hCitemBase* pItem = NULL;
	itemMap_t::iterator mapPos;
	itemID_t   theID = 0;
		
	rc = itmRef.resolveID(theID);
	if ( rc == SUCCESS && theID != 0 )
	{
		mapPos = masterMap.find(theID);//[theID];;
		if ( mapPos == masterMap.end() ) pItem = NULL;
		else pItem = (mapPos->second); //mapPos should be a ptr2ptr2itembase

		*returnItem = pItem;
		
		if (pItem != NULL)
		{// overwrite
//DEAL WITH THIS LATER			rc = pItem->resolveProcure();// self and subs
// don't do this for now
rc = SUCCESS;
		}
		else
		{
			rc = FAILURE;
		}
	}
	/* return the resolve error
	else
	{
		rc = APP_PROGRAMMER_ERROR;// also an empty list error
	}
	**/
	return rc;
}


// this fills the item with evaluated information - no sublists are filled
// this expects a pointer to pointer which will be filled with a pointer to the actual item
RETURNCODE hCddbDevice::procure(int itemID, hCitemBase** returnItem)// a fully resolved item
{ 
	RETURNCODE    rc = SUCCESS;
	hCitemBase* pItem = NULL;
	itemMap_t::iterator mapPos;

	mapPos = masterMap.find(itemID);//[theID];;
	if ( mapPos == masterMap.end() ) pItem = NULL;
	else pItem = (mapPos->second); //mapPos should be a ptr2ptr2itembase

	*returnItem = pItem;
	
	if (pItem != NULL)
	{// overwrite
//DEAL WITH THIS LATER		rc = pItem->resolveProcure();// self and subs
// don't do this for now
rc = SUCCESS;
	}
	else
	{
		rc = FAILURE;
	}
	return rc;
}

//RETURNCODE hCddbDevice::get(int itemID, vector<CitemBase*>& itemList){return FAILURE;};
//RETURNCODE hCddbDevice::get(vector<CVar*>& variableList){return FAILURE;};
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< device service support >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

RETURNCODE hCddbDevice::getItemBySymNumber(itemID_t symNum, hCitemBase** ppReturnedItempointer)
{
	RETURNCODE rc = SUCCESS;
	itemMap_t::iterator mapPos;

	mapPos = masterMap.find(symNum);//[theID];;
	if ( mapPos == masterMap.end() )
	{
		(*ppReturnedItempointer) = NULL;
		rc = FAILURE;
		if ( symNum != LOOP_WARNING_VARIABLES )// designed to detect existence
		{
			if ( symNum == 1022 ) // byte_count
			{
				DEBUGLOG(CERR_LOG|CLOG_LOG,
		"getItem failed to find 0x03fe (byte count) in the device.(debug-only msg)\n",symNum);
			}
			else
			if ( symNum == 1009 )//VIEW_MENU
			{
			DEBUGLOG(CERR_LOG|CLOG_LOG,
			"getItem failed to find VIEW_MENU in this device.(debug-only msg)\n");
			}
			else
			{
			DEBUGLOG(CERR_LOG|CLOG_LOG,
			"getItem failed to find 0x%04x in the device.(debug-only msg)\n",symNum);
			}
		}
	}
	else 
	{
		(*ppReturnedItempointer) = (mapPos->second); //mapPos should be a ptr2ptr2itembase
		rc = SUCCESS;
	}
	return rc;
}

RETURNCODE hCddbDevice::getItemBySymName(const string& symName, hCitemBase** ppReturnedItempointer)
{
	RETURNCODE rc = SUCCESS;
	nameMap_t::iterator mapPos;

	mapPos = masterNameMap.find(symName);//[theSymbolName];
	if ( mapPos == masterNameMap.end() )
	{
		(*ppReturnedItempointer) = NULL;
		rc = FAILURE;
	}
	else 
	{
		(*ppReturnedItempointer) = (mapPos->second); //mapPos should be a ptr2ptr2itembase
		rc = SUCCESS;
	}
	return rc;
}

/* stevev 20sep06
   this is needed to force a read of a variable with a default value if it is critical */
RETURNCODE hCddbDevice::ReadCritical(hCitemBase* itemPtr,  CValueVarient& ppReturnedDataVal)
{
	RETURNCODE rc = FAILURE;
	if ( itemPtr->IsVariable() )
	{
		if ( ((hCVar*)itemPtr)->IsLocal() )
		{
			if (((hCVar*)itemPtr)->getDataQuality() == DA_HAVE_DATA ||
				((hCVar*)itemPtr)->getDataQuality() == DA_STALE_OK)
			{
				ppReturnedDataVal = ((hCVar*)itemPtr)->getDispValue();
				rc = SUCCESS;
			}
		}
		else
		{
			hCVar* pVar = (hCVar*) itemPtr;
			/* stevev 01Oct08 --
			   from Vibhor 3/10/08 - we should do the simple optimization of
				detecting when a value has been read by another command and
				not send that command again.  CACHED is only allowed to be
				set (on a non-local var) in the command reply's extraction
				process.  So, if it shows cached, it means it has been read 
				from the device.  Since criticals are read at the very beginning
				of SDC's operation, it could only be cached by a recent command.
			**/
			INSTANCE_DATA_STATE_T ids = IDS_CACHED;
			if (pVar->getDataState() != IDS_CACHED)
			{//all other states will force a read
				ids = pVar->markItemState(IDS_UNINITIALIZED);
			}
			// else - its cached, just get the value
				
			rc = ReadImd(itemPtr,  ppReturnedDataVal,false);

			if ( pVar->getDataState() == IDS_UNINITIALIZED)
			{// something bad happened
				pVar->markItemState(ids);// put it back the way it was
			}
		}
	}
	return rc;
}

RETURNCODE hCddbDevice::ReadImd (itemID_t  itemID,   CValueVarient& ppReturnedDataItem,bool isAtest)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase* pIB = NULL;

	if (getItemBySymNumber(itemID, &pIB) == SUCCESS)
	{
		if ( pIB->IsVariable() == true )
		{
			rc = ReadImd(pIB, ppReturnedDataItem,isAtest);
			//pVar = (hCVar*) pIB;
			//rc = pVar->Read(ppReturnedDataItem);
		}
		else //-- not a variable 
		//		note that we can't resolve item arrays or collections without an index
		{
			LOGIT(CLOG_LOG,"   ERROR: Read Immediate on a non variable 0x%04x\n",pIB->getID());
			ppReturnedDataItem.clear();
			rc = APP_TYPE_UNKNOWN;
		}
	}
	else //continue with null pVar
	{
		LOGIT(CLOG_LOG,"    ERROR: Could not resolve Item from it's symbol number 0x%04x\n"
																						,itemID);
		ppReturnedDataItem.clear();
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}
/*
 *            IDS_UNINITIALIZED, IDS_CACHED, IDS_STALE, IDS_PENDING, IDS_INVALID, IDS_NEEDS_WRITE
 *DA_HAVE_DATA       st01			st02		st02		st02		st01		st02
 *DA_NOT_VALID       st01			XX			ok-noAction	ok-noAction	st01		ok-noAction
 *DA_STALE_OK        st01			st02		st02<dfltV> st02		st01		st02
 *DA_STALEUNK        st01           XX          st01		st03		st01        ok-noAction
 ***/
RETURNCODE hCddbDevice::ReadImd(hCitemBase* itemPtr,  CValueVarient& ppReturnedDataVal,bool isAtest)
{
	RETURNCODE rc = SUCCESS;
	hCcommand* pC = NULL;
	hCVar*   pVar = NULL;
	int      iTransNumb = -1; // TODO: get the correct/lowest transaction number

	pVar = (hCVar*) itemPtr;
	
	try{   //DEEPAK 112504

	if ( pVar != NULL )// it is a variable
	{
			//IDS_CACHED, IDS_PENDING, IDS_NEEDS_WRITE
		INSTANCE_DATA_STATE_T ids = pVar->getDataState();
		DATA_QUALITY_T         dq = pVar->getDataQuality();
		indexUseList_t         localIdxList;
		itemID_t               varID       = pVar->getID();// for debugging purposes
			

		if (ids == IDS_UNINITIALIZED || ids == IDS_INVALID 
			||(ids== IDS_STALE && dq== DA_STALEUNK)  ) // st01
		{
			if (isAtest)
			{
				ppReturnedDataVal = pVar->getRealValue();
				ppReturnedDataVal.vIsValid = false;
				return SUCCESS;
			}
#ifdef VALID_TEST
			if (1)
#else
			if (pVar->IsValid())
#endif
			{
#ifdef QDBG
		LOGIT(CLOG_LOG,"    ReadImd for 0x%04x  needs a command.\n",pVar->getID());
#endif
				// get the command
				hCcommandDescriptor rdc = pVar->getRdCmd();
				if ( rdc.cmdNumber < 0xFFFF && rdc.cmdNumber > -1 )
				{	// get cmd ptr
					pC = cmdInstanceLst.getCmdByNumber(rdc.cmdNumber);
					if ( pC != NULL )
					{
	/* now done in the send command construction
	if (rdc.srcIdxVal >= 0 && rdc.srcIdxVar != 0x0000)
	{// verify it is a local index and then set to the value - make valid
		hCitemBase* pItmIndx = NULL;
		hCVar* pVarIndx;
		if (  ( pVarIndx = getVarPtrBySymNumber(rdc.srcIdxVar) ) != NULL )
		{
			if (pVarIndx->VariableType() == vT_Index && pVarIndx->IsLocal() )
			{
				CValueVarient newValue;
				newValue = rdc.srcIdxVal;
				pVarIndx->setDispValue(newValue);	// set one
				pVarIndx->ApplyIt();				// copy it to the other
				pVarIndx->markItemState(IDS_CACHED);// force it good
			}
		}
	}//else no index involved
	*/
	#ifdef _DEBUG
						if ( ! isInstantiated )
						{
							LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"Error: sending command to an uninstatiated device.\n");
						}
	#endif
						rc = FAILURE;
						ppReturnedDataVal = pVar->getRealValue();	// stevev 20apr06 as per FDM merge
						if (! getPolicy().isReplyOnly && IS_ONLINE && 
							  pVar->getDataQuality() != DA_HAVE_DATA && 
								pVar->getDataQuality() != DA_STALE_OK)
						{// no use doing this in a XMTR situation    or while offline
							LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," ReadImd: 0x%04x  using command %d, Trans:%d\n",
								pVar->getID(),rdc.cmdNumber,rdc.transNumb);
							rc = pCmdDispatch->Send(pC,rdc.transNumb,true,
							bt_ISBLOCKING,NULL,rdc.idxList,false,co_READ_IMD);//stevev 25may07 - added origin
						}
	#ifdef LOG_SUCCESS
						else
						LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"ReadImd: 0x%04x NO READ MADE.\n",pVar->getID());
	#endif
							// else - a transmitter - just return the value
						// was just: rc = pCmdDispatch->Send(pC,rdc.transNumb,true,bt_ISBLOCKING,
						//															NULL,rdc.idxList);
						if (rc == SUCCESS)	// stevev 20apr06 as per FDM merge (goes with one above)
							ppReturnedDataVal = pVar->getRealValue();
						else
							ppReturnedDataVal.vIsValid = false;
					}
					else //no command found
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: no command pointer for cmd: %d was found.\n",
																					rdc.cmdNumber );
						rc = FAILURE;
					}
				}
				else
				if ( pVar->IsLocal() )
				{
					LOGIT(CERR_LOG|CLOG_LOG,"WARN: local variable 0x%04x has data State = "
						"%s and data quality = %s\n",
						pVar->getID(),instanceDataStStrings[ids],dataQualityStrings[dq]);
					ppReturnedDataVal = pVar->getRealValue();
				}
				else
				{// no read command
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,"ERROR: ReadImd: Non-Local item "
					 "0x%04x (%s) has no Read Command.\n",pVar->getID(), pVar->getName().c_str());
				}
			}
			else //is invalid
			{
				LOGIT(CLOG_LOG,"ReadImd: 0x%04x is an invalid variable.\n",pVar->getID());
				ppReturnedDataVal = pVar->getRealValue();
#ifdef _DEBUG
				if ( ppReturnedDataVal.vIsValid )
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: is invalid but returned valid varient.\n");
					ppReturnedDataVal.vIsValid = false;
				}
				//else - is correct
#endif
			}
		}
		else //IDS_CACHED, IDS_PENDING, IDS_NEEDS_WRITE - Use as is
		if (dq == DA_STALE_OK || dq == DA_HAVE_DATA)
		{
			ppReturnedDataVal = pVar->getRealValue();

#ifdef LOG_SUCCESS					
			LOGIT(CLOG_LOG,"ReadImd: 0x%04x is stale-OK or good.\n",pVar->getID());
#endif	

#if 0
			/*<START>Added by ANOOP.17FEB2004.This is because ReadImd() fails for methods with dynamic variables %0
				and to get the latest value for a dynamic variable this has been added.*/
			if(pVar->IsDynamic())
			{
				// get the command
				hCcommandDescriptor rdc = pVar->getRdCmd();
				if ( rdc.cmdNumber < 0xFF && rdc.cmdNumber > -1 )
				{	// get cmd ptr
					pC = cmdInstanceLst.getCmdByNumber(rdc.cmdNumber);
					if ( pC != NULL )
					{

						rc = pCmdDispatch->
							Send(pC,rdc.transNumb,true,bt_ISBLOCKING,NULL,rdc.idxList);
						ppReturnedDataVal = pVar->getRealValue();
					}
					else //no command found
					{
						LOGIT(CERR_LOG,
							"ERROR: no command pointer for cmd: %d was found.\n",rdc.cmdNumber);
						rc = FAILURE;
					}
				}
				else
				if ( pVar->IsLocal() )
				{
					LOGIT(CERR_LOG,"WARN: local variable 0x%04x has data State = %s and "
						"data quality = %s\n",pVar->getID(),
						instanceDataStStrings[ids],dataQualityStrings[dq]);
					ppReturnedDataVal = pVar->getRealValue();
				}
				else
				{// no read command
					LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: ReadImd:: item 0x%04x Does not have a Read Command.\n" 
						 ,pVar->getID() );
				}
			}
			/*<END>Added by ANOOP.17FEB2004.This is because ReadImd() fails for methods with dynamic variables %0
				and to get the latest value for a dynamic variable this has been added.*/
#endif // 0
		}
		/* -- stevev 14dec05 */
		else
		if (ids== IDS_PENDING && dq== DA_STALEUNK)
		{
			/* PENDING and STALEUNK is common - we need to return a value that tells the caller
			   that the value is not good */			
			ppReturnedDataVal = pVar->getRealValue();// could be a DEFAULT_VALUE...
			ppReturnedDataVal.vIsValid = false;
			// will return SUCCESS default
#ifdef LOG_SUCCESS					
			LOGIT(CLOG_LOG,"ReadImd: 0x%04x Pending, Never Read before (Returning invalid).\n",pVar->getID());
#endif	
		}
		/* end -- stevev 14dec05 */
		else
		{
/* stevev - experiemnt 
Note that this doesn't work.
The log shows that the command is received and being extracted but it must, somehow, 
be in this task bause even sleeping for a second fails.
			if ( ids == IDS_PENDING)
			{
				Sleep(1000);
				ids = pVar->getDataState();
				if (ids != IDS_PENDING)
					clog <<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< no longer pending."<<endl;
				else
					clog <<"Slept ";

			}
/ * stevev end test */
			// this appears to be normal in some circumstances - 
			LOGIT(CLOG_LOG,"WARN:  ReadImd:0x%04x has a quality:%s and a state:%s %s\n",			
					pVar->getID(),dataQualityStrings[dq],instanceDataStStrings[ids],
					" -- No read Taken--");
			  
		/*	cerrxx << "ERROR: ReadImd:0x"<<hex<<pVar->getID() << dec<<" has a quality:"
		//		 <<dataQualityStrings[dq] <<" and a state:"<< instanceDataStStrings[ids] 
				 <<" -- No read Taken--" << endl;
		*/
		}
	}
	else
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR--ReadImd passed a NULL variable pointer.\n");
		rc = FAILURE;
	}
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"ReadiMd inside Catch(...)\n");
	}  //DEEPAK 112504
	//cerrxx << "ERROR--Unimplemented device.read(itembase..."<<endl;
	return rc;
}
RETURNCODE hCddbDevice::Read(vector<hCitemBase*>&  itemList)
{// mark 'em all stale and return
	//cerrxx << "ERROR--Unimplemented device.read(ddbItemList..."<<endl;
	hCVar* pVar = NULL;

	for (vector<hCitemBase*>::iterator iT = itemList.begin();iT<itemList.end();iT++)
	{
		pVar = (hCVar*)(*iT);
		if (pVar)// NULL if it isn't a variable
		{
			pVar->markItemState(IDS_STALE);
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: read request on an item ptr that was not a variable\n");
		}
	}
	return SUCCESS;
}

RETURNCODE hCddbDevice::WriteImd(itemID_t    itemID,   CValueVarient* dataVal)
{// This function should not be needed
	RETURNCODE rc = SUCCESS;
	hCVar*      pVar= getVarPtrBySymNumber(itemID);

	if (pVar)
	{
		//rc = pVar->Write(*dataVal);
		rc = WriteImd((hCitemBase*)pVar,dataVal);
	}
	else //continue with null pVar
	{
		dataVal->clear();
		rc = FAILURE;
	}
	return rc;
}


RETURNCODE hCddbDevice::WriteImd(hCitemBase* itemPtr,  CValueVarient* dataVal)
{// This function should not be needed
	RETURNCODE rc = SUCCESS;
	hCcommand* pC = NULL;

	
/*Vibhor 250304: Start of Code*/
	hMsgCycle_t localMsgCycle;
/*Vibhor 250304: End of Code*/

/*Vibhor 090404: Start of Code*/
	hCevent WriteEvent; // Stack event, will die on exit
/*Vibhor 090404: End of Code*/

	if (itemPtr == NULL)
	{
		LOGIT(CERR_LOG,"WriteImd received a null variable.\n");
		dataVal->clear();
		return APP_PROGRAMMER_ERROR;
	}


	hCVar*   pVar = (hCVar*) itemPtr;

	INSTANCE_DATA_STATE_T ids = pVar->getDataState();
	DATA_QUALITY_T         dq = pVar->getDataQuality();
	hIndexUse_t   localIdxUse;
	
	if (!(pVar->IsReadOnly()))
	{
		pVar->setDispValue(*dataVal);
		if (pVar->isChanged())
		{// we really don't care what the state is - pending shouldn't matter
			
			// get the command
			hCcommandDescriptor rdc = pVar->getWrCmd();
//			localIdxUse =  rdc;
			if ( rdc.cmdNumber < 0xFF && rdc.cmdNumber > -1 )
			{	// get cmd ptr
				pC = cmdInstanceLst.getCmdByNumber(rdc.cmdNumber);
				if ( pC != NULL )
				{
/* this should be done in send command
								if (rdc.srcIdxVal >= 0 && rdc.srcIdxVar != 0x0000)
								{// verify it is a local index and then set to the value - make valid
									hCitemBase* pItmIndx = NULL;
									hCVar* pVarIndx;
									if (  ( pVarIndx = getVarPtrBySymNumber(rdc.srcIdxVar) ) != NULL )
									{
										if (pVarIndx->VariableType() == vT_Index && pVarIndx->IsLocal() )
										{
											CValueVarient newValue;
											newValue = rdc.srcIdxVal;
											pVarIndx->setDispValue(newValue);	// set one
											pVarIndx->ApplyIt();				// copy it to the other
											pVarIndx->markItemState(IDS_CACHED);// force it good
										}
									}
								}//else no index involved
end send command functionality */

/*Vibhor 250304: Start of Code*/
/*Vibhor 090404: Start of Code*/
//Changed bt_BLOCKING to bt_NOTBLOCKING
/* VMKP Added on 160404: Removed localMsgCycle from the passed parameter list and added a WriteImd flag to
  identify Send called from WriteImd, so we are passing true here */
					rc = pCmdDispatch->Send(pC,rdc.transNumb,true,bt_NOTBLOCKING,&WriteEvent,rdc.idxList,
											true,co_WRITEIMD);//&localMsgCycle);//stevev 25may07-added origin
					hCevent::hCeventCntrl* pEC = WriteEvent.getEventControl();
					if(NULL != pEC && rc ==SUCCESS)
					{
						rc = pEC->pendOnEvent();
						if(SUCCESS == rc)
						{
							/* VMKP added on 150404 to check the response code from command
								dispatcher member variable, instead of WrkMsgCycle */
							if(pCmdDispatch->m_brespCdVal)
						rc = FAILURE; //Nonzero Response Code
						}
						else
						{
							rc = FAILURE; // Pend failed
						}
					}
/*Vibhor 090404: End of Code*/
/*Vibhor 250304: End of Code*/

						//(pC,rdc.transNumb,true,bt_ISBLOCKING,NULL,&localIdxUse);
				}
				else //no command found
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: no command pointer for cmd: %d was found.\n",
																				rdc.cmdNumber);
					rc = FAILURE;
				}
			}
			else
			{// no write command
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: WriteImd:: item 0x%04x Does not have a Write Command.\n",
																				pVar->getID());
                rc = FAILURE;
			}
		}
		// else just leave it alone - no reason to write a written
	}
	else
	{
		LOGIT(CERR_LOG,"WriteImd did NOT write the read-only variable.\n");
		dataVal->clear();
		rc = API_FAIL_READ_ONLY;
	}
	
	return rc;
}
RETURNCODE hCddbDevice::Write(vector<hCitemBase*>& itemList)
{
	hCVar* pVar = NULL;

	for (vector<hCitemBase*>::iterator iT = itemList.begin();iT<itemList.end();iT++)
	{
		pVar = (hCVar*)(*iT);// could be NULL if it's not a variable item
		if (pVar)
		{
			pVar->markItemState(IDS_NEEDS_WRITE);
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: read request on an item ptr that was not a variable\n");
		}
	}
	return SUCCESS;
}

//prashant: oct 2003
hCMethSupport*     hCddbDevice::getMethodSupportPtr()
{
	return (m_pMethSupportInterface);
}

// method added 01nov06 stevev
bool	   hCddbDevice::sizeImage(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInLn)
{
	return (m_pMethSupportInterface->UIimageSize(imgIndex,xCols,yRows, isInLn));
}
// method added 27mar07 stevev
bool	   hCddbDevice::sizeString(string* c_str,unsigned& xCols,unsigned& yRows,int hgtLim)
{
	return (m_pMethSupportInterface->UIstringSize(c_str,xCols,yRows, hgtLim));
}


DD_Key_t     hCddbDevice::getDDkey(void)
{	return (devDDkey);
}

void       hCddbDevice::getStartDate(dmDate_t& dt)
{
	dmDate_t locDt = {0,0,0};
	if (pDevMgr != NULL )
	{
		pDevMgr->getStartDate(dt);
	}
	else
	{
		dt = locDt;
	}
	return;
}

/* this has been modified to to be non executeActionMethods - special handling for actions*/
/* stevev - 07mar07 common code moved to doExecute - only the shell remains here          */
RETURNCODE hCddbDevice::executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue)
{
	RETURNCODE rc = SUCCESS;

	if( pMEE == NULL ||  m_pMethSupportInterface == NULL )	
	{
		return FAILURE;
	}

	if (pCmdDispatch)
	{
// stevev 13aug13 - this duplicates the activity in PreExecute
//		pCmdDispatch->disableAppCommRequests();
//			DEBUGLOG(CLOG_LOG,"Device executeMethod disables appComm\n");
		LOGIF(LOGP_NOTIFY)(CLOG_LOG,"device executemethod (single)  is doing STRT_methPkt.\n");
		pCmdDispatch->notifyVarUpdate(0,  STRT_methPkt);// stevev 23jan06
	}
	devIsExecuting = true;// stevev 06apr10 - bug 3034

	rc = doExecute(oneMethod,returnValue,( pMEE->GetOneMethRefNo() > 0 ));
							
#ifdef __MOVED_TO_DOEXECUTE__  		/*/////////////////////////////////////////////////////////////*/
	long lVariableItemId = -1;
	bool bReturnValue; 

/*Vibhor 260204: Start of Code*/
	hCitemBase* pIB = NULL;
	hCVar* pVar = NULL;
	CValueVarient tempVariant;
	int iCurrDevStat;
/*Vibhor 260204: End of Code*/

	pMEE->m_bSaveValues = false;//Vibhor 091106: 
	if(msrc_ACTION == oneMethod.source) //if called froma pre/post action
	{
		LOGIT(CLOG_LOG,"Start pre/post method ===============================\n");
			if(0 == oneMethod.paramList.size())//if paramlist is empty
			{
				LOGIT(CERR_LOG|UI_LOG,"Call to execute pre/post action not called because "
					           "the item ID of the concerned variable is not provided.\n");
				rc = FAILURE;
			}
			else
			{
				lVariableItemId = oneMethod.paramList[0].vValue.varSymbolID; //first element in the list is the variable item ID
				if(0 == pMEE->GetOneMethRefNo())
				{
					//This is the case where the Edit Actions is called when 
					//we try to Edit the varible which sits on the menu. ( Except the Display menu)
					cacheDispValues();//Vibhor 091106: 
					//bReturnValue = pMEE->ExecuteMethod(this,oneMethod.methodID,lVariableItemId);// moved common code to common location
				}

				if( 0 < pMEE->GetOneMethRefNo())
				{
					/*This is the case where  Edit actions are called from the Menu which is referred by 
					Display menu built in. Take the action apropriately.This will distinguish us from Actions 
					which are being called*/					
					
					
					/*First detrmine where it is pending from the previous method and start the Edit actions from there on
					We still need discussion on this
					*/
					// this is not allowed as per wally/paul 05mar07 from chris/paul/anil meeting ....cacheDispValues();//Vibhor 091106: 
					//bReturnValue = pMEE->ExecuteMethod(this,oneMethod.methodID,lVariableItemId);// moved common code to common location
					
					//TODO: Fill the code to call the Edit action					
					
				} 
				// the common location
				bReturnValue = pMEE->ExecuteMethod(this,oneMethod.methodID,lVariableItemId);
				

/*Vibhor 260204: Start of Code*/
/*The config change bit during a methode execution is being treated as an expected config change, so we'll reset that */
				rc = getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
				if(SUCCESS == rc && pIB != NULL)
				{
					pVar = (hCVar*)pIB;
					
					//tempVariant = pVar->getDispValue();// modified to below ( we really don't care what the user put in
					tempVariant = pVar->getRealValue();// modified for KROHNE device error 10/18/04 stevev
					iCurrDevStat = (int) tempVariant;
					if(iCurrDevStat & DS_CONFIGCHANGED)
					{// Its assumed that Command 38 is being supported by the device
						rc = sendMethodCmd(38,-1);
					}
				}

/*Vibhor 260204: End of Code*/
			}
		LOGIT(CLOG_LOG,"End - pre/post method ===============================\n");
		}
	else if(msrc_EXTERN == oneMethod.source)
		{
//		LOGIT(CLOG_LOG,"Start User method ===============================\n");
			if( 0 != pMEE->GetOneMethRefNo())
			{
				//Error
				//Anil 160806
				//This should not happen at any pont of time as is called from the extern and Instance should be zero. 
			}
			else // probably unneccessary but can't hurt
			{
				cacheDispValues();//Vibhor 091106: 
			}
			bReturnValue = pMEE->ExecuteMethod(this,oneMethod.methodID);			
/*Vibhor 260204: Start of Code*/
/*The config change bit during a methode execution is being treated as an expected config change, so we'll reset that */
			rc = getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
			if(SUCCESS == rc && pIB != NULL)
			{
				pVar = (hCVar*)pIB;
					
				tempVariant = pVar->getDispValue();
				iCurrDevStat = (int) tempVariant;
				if(iCurrDevStat & DS_CONFIGCHANGED)
				{// Its assumed that Command 38 is being supported by the device
					rc = sendMethodCmd(38,-1);
				}
			}

/*Vibhor 260204: End of Code*/
//		LOGIT(CLOG_LOG,"End - User method ===============================\n");
		}
	else
	{
		//This is not the valid case, as Methos calling methos is currently handed within the MEE
		//MEE  instanciates the One meth class.
		//See MEE::ResolveMethodExp
	}

		if (bReturnValue)
		{
			rc = SUCCESS;
		}
		else
		{
			rc = FAILURE;
		}

		nLastMethodsReturnCode = rc;
	uncacheDispVals(pMEE->m_bSaveValues); //Vibhor 091106: Added 
	pMEE->m_bSaveValues = false;          //Vibhor 091106: 
#endif  ////// __MOVED_TO_DOEXECUTE__ ///////////////////////////////////////////////////////////


		devIsExecuting = false;// stevev 06apr10 - bug 3034
		if (pCmdDispatch)
		{
// stevev 13aug13 - this duplicates the activity in PreExecute
//			pCmdDispatch->enableAppCommRequests();
//			DEBUGLOG(CLOG_LOG,"Device executeMethod enables appComm\n");
			LOGIF(LOGP_NOTIFY)(CLOG_LOG,"executeMethod (single)  is doing END_methPkt.\n");
			pCmdDispatch->notifyVarUpdate(0,  END_methPkt);// stevev 23jan06
		}

	return rc;
}

RETURNCODE hCddbDevice::executeMethod(methodCallList_t& actionList,CValueVarient&returnValue )
{
	RETURNCODE rc = SUCCESS;
	bool bReturnValue = false;
	//MEE  mee;
	vector<hCmethodCall>::iterator it;

	if (m_pMethSupportInterface != NULL && pMEE != NULL)
	{
		hCmethod* pMethod = NULL;
		hCitemBase* pItemBase = NULL;

		if (pCmdDispatch)
		{
// stevev 13aug13 - this duplicates the activity in PreExecute
//			pCmdDispatch->disableAppCommRequests();
//			DEBUGLOG(CLOG_LOG,"Device executeMethod list disables appComm\n");
		LOGIF(LOGP_NOTIFY)(CLOG_LOG,"executeMethod (list)  is doing STRT_methPkt.\n");
			pCmdDispatch->notifyVarUpdate(0,  STRT_methPkt);// stevev 23jan06
		}
		devIsExecuting = true;// stevev 06apr10 - bug 3034

		it = actionList.begin ();
		while (it != actionList.end ())
		{
			hCmethodCall oneMethod = (hCmethodCall)(*it);
/*       __MOVED_TO_DOEXECUTE__
			m_pMethSupportInterface->PreExecute();

			getItemBySymNumber(oneMethod.methodID,&pItemBase);
			pMethod = (hCmethod*)pItemBase;
			oneMethod.m_pMeth = pMethod;

			rc = m_pMethSupportInterface->DoMethodExecute(oneMethod);
			if (rc == FAILURE)
			{
				break;// stop executing
			}

			m_pMethSupportInterface->PostExecute();
*/
			rc = doExecute(oneMethod, returnValue, (pMEE->GetOneMethRefNo() > 0) );
			if ( rc != SUCCESS )
				break; // out of while loop

			it++;
		}
		devIsExecuting = false;// stevev 06apr10 - bug 3034
		if (pCmdDispatch)
		{
// stevev 13aug13 - this duplicates the activity in PreExecute
//			pCmdDispatch->enableAppCommRequests();
//			DEBUGLOG(CLOG_LOG,"Device executeMethod list enables appComm\n");
			LOGIF(LOGP_NOTIFY)(CLOG_LOG,"executeMethod (list)  is doing END_methPkt.\n");
			pCmdDispatch->notifyVarUpdate(0,  END_methPkt);// stevev 23jan06
		}
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: method list execution called with missing resource pointer.\n");
		rc = APP_PARAMETER_ERR;
	}
	return rc;
}

// L&T Modifications : VariableSupport - start
RETURNCODE hCddbDevice::executeMethodForVS(methodCallList_t& actionList,CValueVarient&returnValue, hCddbDevice *pCurDev)
{
	RETURNCODE rc = SUCCESS;
	bool bReturnValue = false;
	//MEE  mee;
	vector<hCmethodCall>::iterator it;

	if (m_pMethSupportInterface != NULL && pMEE != NULL)
	{
		hCmethod* pMethod = NULL;
		hCitemBase* pItemBase = NULL;

		if (pCmdDispatch)
		{
			pCmdDispatch->disableAppCommRequests();
			DEBUGLOG(CLOG_LOG,"Device executeMethod list disables appComm\n");
#ifndef _CONSOLE // Console application does not send MFC window messages
			pCmdDispatch->notifyVarUpdate(0,  STRT_methPkt);// stevev 23jan06
#endif _CONSOLE
		}
		devIsExecuting = true;// stevev 06apr10 - bug 3034

		it = actionList.begin ();
		while (it != actionList.end ())
		{
			hCmethodCall oneMethod = (hCmethodCall)(*it);
/*       __MOVED_TO_DOEXECUTE__
			m_pMethSupportInterface->PreExecute();

			getItemBySymNumber(oneMethod.methodID,&pItemBase);
			pMethod = (hCmethod*)pItemBase;
			oneMethod.m_pMeth = pMethod;

			rc = m_pMethSupportInterface->DoMethodExecute(oneMethod);
			if (rc == FAILURE)
			{
				break;// stop executing
			}

			m_pMethSupportInterface->PostExecute();
*/
			rc = doExecuteForMS(oneMethod, returnValue, (pMEE->GetOneMethRefNo() > 0), pCurDev);
			if ( rc != SUCCESS )
				break; // out of while loop

			it++;
		}
		devIsExecuting = false;// stevev 06apr10 - bug 3034
		if (pCmdDispatch)
		{
			pCmdDispatch->enableAppCommRequests();
			DEBUGLOG(CLOG_LOG,"Device executeMethod list enables appComm\n");
			pCmdDispatch->notifyVarUpdate(0,  END_methPkt);// stevev 23jan06
		}
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: method list execution called with missing resource pointer.\n");
		rc = APP_PARAMETER_ERR;
	}
	return rc;
}
// L&T Modifications : VariableSupport - end

// L&T Modifications : ABB Tx. Support - start
RETURNCODE hCddbDevice::executeMethodForMS(vector<hCmethodCall>& actionList, hCddbDevice *pCurDev)
{
	CValueVarient rV;
	return (executeMethodForVS(actionList, rV, pCurDev));
}
// L&T Modifications : ABB Tx. Support - end

RETURNCODE hCddbDevice::executeMethod(vector<hCmethodCall>& actionList)
{

	
	CValueVarient rV;
	return (executeMethod(actionList,rV));

/*  functionality modified and incorporated into original list handler /////////////////////////
	RETURNCODE rc = FAILURE;
	
	if(NULL == pMEE)
	{
		return rc;
	}
	bool bSuccess = false;
	vector<hCmethodCall>::iterator it;

	for(it = actionList.begin ();it < actionList.end ();it++)
	{
		hCmethodCall oneMethod = (hCmethodCall)(*it);
		pMEE->m_bSaveValues = false;//Vibhor 091106: 
		cacheDispValues();          //Vibhor 091106: 
		bSuccess = pMEE->ExecuteMethod(this,oneMethod.methodID,oneMethod.paramList[0].vValue.varSymbolID);
		uncacheDispVals(pMEE->m_bSaveValues);//Vibhor 091106: 
		pMEE->m_bSaveValues = false;         //Vibhor 091106: 
		if(!bSuccess)
			break;
	}

	if(bSuccess)
		rc = SUCCESS;
	return rc;
/////////  fini moved to original ********************/
}

/* stevev 06mar07 - moved common code to here */
/************************************************
 * oneMethod          describes the method call
 * returnValue        is unused at this time
 * isMultipleEntry    is true if there was a method running when execute was attempted
 *
 * returns:   SUCCESS if all is well, FAILURE (future reason) for aborted execution.
 *
 * It is assumed that this is (protected) common code and is called by a public function.
 * It is assumed that the caller has disabled AppComm Requests and has notified method-start.
 * It is assumed that the caller has verified valid input parameters & needed pointers.
 * It is assumed that the caller will re-enable AppComm & notify method-end at return.
 ************************************************/
RETURNCODE hCddbDevice::
           doExecute(hCmethodCall oneMethod, CValueVarient& returnValue, bool isMultipleEntry)
{
//	bool          bReturnSuccess;
	RETURNCODE    rc   = SUCCESS;

//	hCitemBase*   pIB  = NULL;
//	hCVar* pVar        = NULL;
//	CValueVarient tempVariant;
//	int           iCurrDevStat;
	/* actually implemented code moved from executeMethod()   16may07 - sjv */
	if (oneMethod.methodID > 0 && oneMethod.m_pMeth == NULL)
	{
		hCitemBase* pItemBase;
		getItemBySymNumber(oneMethod.methodID,&pItemBase);
		oneMethod.m_pMeth = (hCmethod*)pItemBase;
	}
	/* end sjv 16may07 */

	if ( ! isMultipleEntry) // then we are the first entry of a call stack
	{
		devIsExecuting      = true;  // stevev 06apr10
		pMEE->m_bSaveValues = false; // clear save value flag
		cacheDispValues();           // cache display values
		// @ debug verify there are no displays to push
	}
	/***else - done in method support where the dialogs are controlled
		// push active displays & clear display ptrs
		// verify stack and method count match
	***/
#ifdef _DEBUG
#define PPARM	oneMethod
#else
#define PPARM
#endif
	m_pMethSupportInterface->PreExecute(PPARM); // doesn't do much of anything right now
	oneMethod.m_MethState = mc_Initing;
	rc = m_pMethSupportInterface->DoMethodSupportExecute(oneMethod);// handles pushing & poping the dialogs
	m_pMethSupportInterface->PostExecute(PPARM);// doesn't do much of anything right now

	nLastMethodsReturnCode = rc;

	if ( ! isMultipleEntry)
	{
		// @ debug verify there are no displays to pop
		// un-cache display values as required		
		uncacheDispVals(pMEE->m_bSaveValues); 
		pMEE->m_bSaveValues = false;   
		// clear save values flag
		devIsExecuting      = false;  // stevev 06apr10
	}
	/* else - done in method support where the dialogs are controlled
		// pop previously active displays
	**/
	return rc;
}
/* end 06mar07 common code move */


RETURNCODE hCddbDevice::sendMethodCmd(int commandNumber,int transactionNumber, 
											indexUseList_t* pI)// stevev added indexes 29nov11
{
	RETURNCODE rc, hrc;
	if ( pCmdDispatch == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: sendMethodCmd with no dispatcher present.");
		rc = DEVICE_NO_DISPATCH | HOST_INTERNALERR;
	}
	else
	{
		hCcommand* pC = cmdInstanceLst.getCmdByNumber(commandNumber);
		if ( pC == NULL || pC->getIType() != iT_Command)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No command class for cmd# %d.",commandNumber);
			rc = DEVICE_NO_COMMANDCLASS | HOST_INTERNALERR;
		}
		else
		{
			indexUseList_t tmp;
			if (pI==NULL)
				pI = &tmp;// an empty list

			rc = pCmdDispatch->Send   
			(	pC, transactionNumber, 
				true,				// actions are enabled
				bt_ISBLOCKING,		// wait for response complete
				NULL,				// no event required
				*pI,				// indexes are up to the caller...good luck
				false,				// we are not in write-imd	stevev 25may07
				co_METHOD);			// origin is method			stevev 25may07
			hrc = pCmdDispatch->getHostErrorStatus();
			if ( rc == SUCCESS ) // a good host status hid the command failure
			{	rc = hrc;	}	// found by honeywell 29aug11
		}
	}

	return rc;// returns host error - hi byte non zero means internal error
}


RETURNCODE hCddbDevice::sendCommand(int commandNumber,int transactionNumber )
{
	RETURNCODE rc;
	if ( pCmdDispatch == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: sendCommand with no dispatcher present.");
		rc = DEVICE_NO_DISPATCH | HOST_INTERNALERR;
	}
	else
	{
		hCcommand* pC = cmdInstanceLst.getCmdByNumber(commandNumber);
		if ( pC == NULL || pC->getIType() != iT_Command)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No command class for cmd# %d.",commandNumber);
			rc = DEVICE_NO_COMMANDCLASS | HOST_INTERNALERR;
		}
		else
		{
			indexUseList_t tmp;
			rc = pCmdDispatch->Send   
			(	pC, transactionNumber, 
				true,				// actions are enabled - non method
				bt_ISBLOCKING,		// wait for response complete
				NULL,				// no event required
				tmp,			// indexes are up to the caller...good luck
				false,				// not a write imeddiate
				co_DEVICE);		// stevev 27frb15 - added a new source for this call

			rc = pCmdDispatch->getHostErrorStatus();
		}
	}
	return rc;// returns host error - hi byte non zero means internal error
}

// stevev 10feb15 - added parameter so SaveValues can notify UI that method changed a local
void hCddbDevice::cacheDispValues(bool generateNotify)
{
	CVarList::iterator 	iTvar;
	
	for ( iTvar = varInstanceLst.begin(); iTvar < varInstanceLst.end(); iTvar++)
	{// iTvar isa ptr 2a ptr 2a hCVar
		hCVar* pV = (*iTvar);
		pV->cacheValue(generateNotify);// everybody gets cached
	}
}

void hCddbDevice::uncacheDispVals(bool saveValuesCalled)
{
	CVarList::iterator 	iTvar;
	
	bool onechanged, ischanged, methChanged, anychanged = false;
	
	hCmsgList msgList;
	
	/* in the simulator, there is no SendToDevice...All modifications are assumed to be
	   in the device so saveValues is assumed to be called 100% of the time ***/
	if (saveValuesCalled || getPolicy().isReplyOnly)
	{
		hCVar* pV = NULL, *pVar;
		hCmsgList        msgs;  
		for ( iTvar = varInstanceLst.begin(); iTvar < varInstanceLst.end(); iTvar++)
		{// iTvar isa ptr 2a ptr 2a hCVar
			pVar = (hCVar*)(*iTvar);
			//stevev 12apr07 with Vibhor:  get xmtr- sdc to work a lot the same
			methChanged = pVar->didMethodChange();
			onechanged  = pVar->isChanged() ; 
			ischanged   = methChanged && onechanged ; 
			anychanged |= ischanged;
			pVar->cacheClear();// just clear 'em
			if(ischanged)
			{
				//Anil as Per Steve added below code
				if (pV == NULL) 
					pV  = pVar;// save any var pointer so we can get its services later
			/*	msgs.insertUnique(pVar->getID(), mt_Val, 0); stevev 6jun07 - preclude copying */
				msgs.insertUnique(pVar->getID(), mt_Mth, 0);
				/* 23apr07 - stevev as per Vibhor: reduce notifications: 
					Display value has changed without device value No need to notify dependents.
				if ( ! getPolicy().isReplyOnly ) // we only need the dependents for sdc
					(*iTvar)->notify(msgs);//fills from dependent's tree
				*** end 23apr07 **/
				// stevev 10may07 - xmtr changed values MUST be copied to device value like a local
				if ( getPolicy().isReplyOnly ) 
					pVar->ApplyIt();
				else
					// stevev 25sep08 - have to treat it like a user written
					pVar->setWriteStatus(1);

				// end stevev 10may07 xmtr-dd repair
			}
		}// next var
		if ( pV && msgs.size() )
				(pV)->notifyUpdate(msgs);		
	}
	else
	{
		for ( iTvar = varInstanceLst.begin(); iTvar < varInstanceLst.end(); iTvar++)
		{// iTvar isa ptr 2a ptr 2a hCVar
			hCVar *pV = (*iTvar);
			pV->uncacheVal();// restores DispValue if flagged, clears flag
		}
	}
	if ( anychanged )
	{
		ACTION_USER_INPUT_DATA structUserInput;
		ACTION_UI_DATA structUIData;

		structUIData.userInterfaceDataType = VARIABLES_CHANGED_MSG;
		structUIData.bDisplayDynamic = false; 

		m_pMethSupportInterface->MethodDisplay(structUIData,structUserInput);
	}
}


void       hCddbDevice::registerItem(itemID_t newItemID, hCitemBase* pNewItem, string& symName)
{	
	pair<int, hCitemBase*> insertionData(newItemID,pNewItem);

	masterMap.insert( insertionData );

if (symName.length() == 0)
{
	LOGIT(CLOG_LOG,"registering an empty symbol name\n");
}
	pair<string, hCitemBase*> nameInsertionData(symName,pNewItem);

	masterNameMap.insert( nameInsertionData );

}

void       hCddbDevice::unRegister(hCitemBase* rmvItem)
{
	nameMap_t::iterator pos;
	itemMap_t::iterator itm;

	itm = masterMap.find(rmvItem->getID());
	if ( itm == masterMap.end() )
	{
		LOGIT(CLOG_LOG,"Unregistering an item that is not registered!(0x%04x)\n",rmvItem->getID());
		return;
	}
	// else process it
	masterMap.erase(itm++);

	// the symbol element - all psuedo items have the same name
	for ( pos = masterNameMap.begin(); pos != masterNameMap.end(); )
	{
		if ( pos->second == rmvItem )
		{
			masterNameMap.erase(pos++);// be - careful, do-not use ++pos or just pos
			break; // out of for loop
		}
		else
		{
			++pos;
		}
	}
	// make the caller do this::rmvItem->destroy();
	//              ditto       delete rmvItem;
	return;
}

void hCddbDevice::registerElement(unsigned long elemIdx,string& elemName, itemID_t grpID)
{
	if ( ! getPolicy().isPartOfTok )
		return; // this is a no-op unless we're in the tokenizer

	if ( elemName.empty() || elemName.length() < 1 || elemName == " " )
		return;  // the nameless elements don't need to be mapped

	elemDesc_t elem;
	elem.elem_Idx = elemIdx;
	elem.elem_name= elemName.c_str();// try to force a string copy(not a pointer copy)
	elem.elem_users.push_back(grpID);

	RETURNCODE rc = SUCCESS;
	// see if it exists
	elemMap_t::iterator mapPos;	
	mapPos = masterElementMap.find(elemIdx);//[theIndex]
	if ( mapPos == masterElementMap.end() ) // not there
	{
		pair<unsigned long,elemDesc_t> elemInsertionData(elemIdx,elem);

		masterElementMap.insert( elemInsertionData );
	}
	else // this index has been registered
	{// insert unique into list...should be a ptr2aelemDesc_t
		elemDesc_t* pElemDesc = (elemDesc_t*)(&(mapPos->second));
		itemIDlist_t::iterator iT;
		bool found = false;
		for (iT = pElemDesc->elem_users.begin(); iT != pElemDesc->elem_users.end();++iT)
		{
			itemID_t itmID = *iT;
			if ( itmID == grpID )
			{
				found = true;
				break;// out of for loop
			}// else keep looking
		}//next
		if ( ! found )
		{
			pElemDesc->elem_users.push_back(grpID);// insert unique
		}
	}
}

RETURNCODE hCddbDevice::getElemNameByIndex(unsigned long elemIdx, string& returnedName)
{
	RETURNCODE rc = SUCCESS;
	if ( ! getPolicy().isPartOfTok )
		return FAILURE; // this is a no-op unless we're in the tokenizer

	elemMap_t::iterator mapPos;

	mapPos = masterElementMap.find(elemIdx);//[theIndex];;
	if ( mapPos == masterElementMap.end() ) // not there
	{
		returnedName.erase();
		rc = FAILURE;
		// stevev 10jul09 - reinstated to full error catagory because members used only in the standard dds
		//                  but undefined in the standard dds
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: Element # %d (0x%04x) was not found in the device.\n",elemIdx,elemIdx);
	}
	else 
	{
		returnedName = (mapPos->second).elem_name.c_str(); 
		rc = SUCCESS;
	}
	return rc;
}

hCobject*  hCddbDevice::getListPtr(CitemType  it)
{
	switch ( (itemType_t)it )
	{
	case iT_Variable:			return &varInstanceLst ;		break;
	case iT_Command:			return &cmdInstanceLst ;		break;
	case iT_Menu:				return &menuInstanceLst ;		break;
	case iT_EditDisplay:		return &edDispInstanceList ;	break;
	case iT_Method:				return &methodInstanceList ;	break;
	case iT_Refresh:/*relation*/return &refreshInstanceList ;	break;
	case iT_Unit:/*relation*/	return &unitRelInstanceList ;	break;
	case iT_WaO:				return &waoInstanceList ;		break;
	case iT_ItemArray:			return &itemArrayInstanceList ;	break;
	case iT_Collection:			return &collectionInstanceList;	break;

	case iT_ReservedOne:/*special*/	return (hCobject*)&imageInstanceList; break;// raw images

	case iT_Array:				return &valArrayInstanceList;	break;
	case iT_File:				return &fileInstanceList;		break;
	case iT_Chart:				return &chartInstanceList;		break;
	case iT_Graph:				return &graphInstanceList;		break;
	case iT_Axis:				return &axisInstanceList;		break;
	case iT_Waveform:			return &waveformInstanceList;	break;
	case iT_Source:				return &sourceInstanceList;		break;
	case iT_List:				return &listInstanceList;		break;
	case iT_Grid:				return &gridInstanceList;		break;
	case iT_Image:				return &imageItemInstanceList;	break; // image items

	case iT_Critical_Item:
		{
			itemIDlist_t* r = NULL;
			m_criticalItems.getCriticalIDs(r);
			return (hCobject*)r;// be sure to cast this correctly!
		}
		break;
	default:
		return NULL;
	}// end switch
}

bool hCddbDevice::isItemCritical(itemID_t  itm)
{
	return m_criticalItems.isItemCritical(itm);
}

/*********************************************************************************************
 * newPsuedoMenu
 *
 * in: A pointer to the menu item that is going to populate the dialog
 *
 *out: A pointer to a menu.
 *
 *	This generates a dialog menu with a single item (pContent) in it.
 *	It will always register the menu.  
 *  Use destroyPsuedoItem() to get rid of it.
 ********************************************************************************************/
hCitemBase* hCddbDevice::newPsuedoMenu(hCmenuItem* pContent)
{
	hCitemBase*  pNewItem = NULL;
	itemIdentity_t menuId;
	string       menuName;

	if ( pContent )
	{	
		menuId.newID = getAPsuedoID();
		char tmpNm[256];
		sprintf(tmpNm, "%s%04x","Table_Item", (menuId.newID & 0x0000FFFF) );
		menuId.newName = tmpNm;
		pNewItem = new hCmenu(pContent, menuId);

		if ( pNewItem != NULL )
		{
			menuInstanceLst.push_back((hCmenu*)pNewItem);
			registerItem(pNewItem->getID(), pNewItem, pNewItem->getSymbolName());
			return pNewItem;
		}// else memory error
	}// else parameter error
	return NULL;// error occured
}

/* stevev 30mar05 - modified to entire item 25may05 */
/* stevev 17jun05 - modified to template/value pair - value may be NULL (use default value) */
hCitemBase* 
hCddbDevice::newPsuedoItem(hCitemBase* pTypeDef, hCitemBase* pValueDef, 
						   psuedoItemFlags_t si_Flags, elementID_t thisElement)
{
	if ( pTypeDef == NULL )
		return NULL;// nothing we can do for you

	if ( pValueDef && ! isTypeCompatible(pTypeDef, pValueDef)  )
	{// this should have been checked before entry
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,
			"ERROR: Type mismatch in insert! trying to put %s into a %s\n"
			,pValueDef->getTypeName(),pTypeDef->getTypeName());
		return NULL;
	}
	hCitemBase* pItem = NULL;
	itemIdentity_t newID;
	newID.newID = getAPsuedoID(); // stevev 10aug06 - make a single entry point

	char tmpNm[256];
	memset(tmpNm,0,256);// stevev 12-12-12 we were corrupting the stack with no \0 termination
	//string tmpSt; tmpSt = pTypeDef->getName();
	//sprintf(tmpNm,"%s%04x",tmpSt.c_str(),(newID.newID & 0x0000FFFF));
	sprintf(tmpNm,"%s%04x",pTypeDef->getName().c_str(),(newID.newID & 0x0000FFFF));
	newID.newName = tmpNm;

	
#ifdef MEMSIZE_CHK
	_CrtMemState s1, s2, s3;   	
	_CrtMemCheckpoint( &s1 );
	#define newTypedef(className) new \
		className(( className *)pTypeDef,( className *)pValueDef, newID);\
		LOGIT(CLOG_LOG,"NEW Psuedo: sizeOf:%d  " #className " \n",sizeof(className)) 
#else
/**************** helper MACRO ********************************/
#define newTypedef(className) new \
		className(( className *)pTypeDef,( className *)pValueDef, newID)
/**************************************************************/
#endif
	switch ( pTypeDef->getIType() )
	{
	case iT_Variable:
		{
			variableType_t vType = (variableType_t)((hCVar*)pTypeDef)->VariableType();
			switch ( vType )
			{
			case vT_Integer:		pItem =	newTypedef(hCinteger);		break;
			case vT_Unsigned:		pItem = newTypedef(hCUnsigned);		break;
			case vT_FloatgPt:		pItem = newTypedef(hCFloat);		break;
			case vT_Double:			pItem = newTypedef(hCDouble);		break;
			case vT_Enumerated:		pItem = newTypedef(hCEnum);			break;
			case vT_BitEnumerated:	pItem = newTypedef(hCBitEnum);		break;
			case vT_Index:			pItem = newTypedef(hCindex);		break;
			case vT_Ascii:			pItem = newTypedef(hCascii);		break;
			case vT_PackedAscii:	pItem = newTypedef(hCpackedAscii);	break;
			case vT_Password:		pItem =	newTypedef(hCpassword);		break;
			//case vT_BitString:
			case vT_HartDate:		pItem = newTypedef(hChartDate);		break;
			case vT_TimeValue:		pItem = newTypedef(hCTimeValue);	break;
			default:
				{
					if ( vType >= vT_unused && vType < vT_MaxType)
					{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Unsupported Variable type (%s) in List or Array.",
													((hCVar*)pTypeDef)->VariableTypeString());
					}
					else
					{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Unsupported Variable type (0x%02x) in List or Array.",
																					(int)vType);
					}
					pItem = NULL;
				}
			}// end subtype switch
			pItem->setPsuedo(thisElement);
			hCVar *pVr = (hCVar*)pItem;
			if ( registerIt && ( pItem != NULL ) )
			{
				varInstanceLst.push_back(pVr);
				registerItem(pVr->getID(), pVr, pVr->getSymbolName());
			}

			if ( (! preserveValue) || ( pVr == NULL ) )// null is use-default
			{
				pVr->setDefaultValue();// moved to after the registration due to 
				//									notification using the registered id
			}
			else // preserve the value incoming...and its state
			if ( pValueDef != NULL )// it's NULL to insert an empty psuedo-item
			{
				pVr->markItemState(  ((hCVar*)pValueDef)->getDataState()  );
				pVr->setDataQuality( ((hCVar*)pValueDef)->getDataQuality());
				pVr->setDataStatus(  ((hCVar*)pValueDef)->getDataStatus() );
                CValueVarient val = ((hCVar*)pValueDef)->getDispValue();
                pVr->setDisplayValue(val);
				pVr->ApplyIt();
			}// we can pretty much guarantee the user has not written to this item
		}
		break;
	case iT_Array:		
		pItem =	newTypedef(hCarray);
		pItem->setPsuedo(thisElement);
		if ( registerIt  && ( pItem != NULL ))
		{
			valArrayInstanceList.push_back((hCarray*)pItem);
			registerItem(pItem->getID(), pItem, pItem->getSymbolName());
		}
		break;
	case iT_List:		
		pItem = newTypedef(hClist);	
		pItem->setPsuedo(thisElement);
		if ( registerIt  && ( pItem != NULL ))
		{
			listInstanceList.push_back((hClist*)pItem);
			registerItem(pItem->getID(), pItem, pItem->getSymbolName());
		}
		break;
	case iT_ItemArray:	
		pItem = newTypedef(hCitemArray);
		pItem->setPsuedo(thisElement);
		if ( registerIt  && ( pItem != NULL ))
		{
			itemArrayInstanceList.push_back((hCitemArray*)pItem);
			registerItem(pItem->getID(), pItem, pItem->getSymbolName());
		}
		break;	
	case iT_Collection:	
		pItem = newTypedef(hCcollection);
		pItem->setPsuedo(thisElement);
		if ( registerIt  && ( pItem != NULL ))
		{
			collectionInstanceList.push_back((hCcollection*)pItem);
			registerItem(pItem->getID(), pItem, pItem->getSymbolName());
		}
		break;
	case iT_File:			 		
		pItem = newTypedef(hCfile);	
		pItem->setPsuedo(thisElement);	
		if ( registerIt  && ( pItem != NULL ))
		{
			fileInstanceList.push_back((hCfile*)pItem);
			registerItem(pItem->getID(), pItem, pItem->getSymbolName());
		}
		break;
/**************************************************************************************
 The following are not implemented at this time - they really don't make sense in lists
 but the code is left in in case we have a problem later ------------------------------
	case iT_Command:		  		
						pItem = newTypedef(hCcommand);	
						if ( registerIt )
						{
							cmdInstanceLst.push_back((hCcommand*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Menu:			 		
						pItem = newTypedef(hCmenu);		
						if ( registerIt )
						{
							menuInstanceLst.push_back((hCmenu*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Method:			 		
						pItem = newTypedef(hCmethod);	
						if ( registerIt )
						{
							methodInstanceList.push_back((hCmethod*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Chart:			 		
						pItem = newTypedef(hCchart);	
						if ( registerIt )
						{
							chartInstanceList.push_back((hCchart*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Graph:			 		
						pItem = newTypedef(hCgraph);	
						if ( registerIt )
						{
							graphInstanceList.push_back((hCgraph*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Axis:			 		
						pItem = newTypedef(hCaxis);		
						if ( registerIt )
						{
							axisInstanceList.push_back((hCaxis*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Waveform:		 		
						pItem = newTypedef(hCwaveform);	
						if ( registerIt )
						{
							waveformInstanceList.push_back((hCwaveform*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Source:			 		
						pItem = newTypedef(hCsource);	
						if ( registerIt )
						{
							sourceInstanceList.push_back((hCsource*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
	case iT_Grid:			 		
						pItem = newTypedef(hCgrid);		
						if ( registerIt )
						{
							gridInstanceList.push_back((hCgrid*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;

	/// relational items make No sense in a list
	///case iT_EditDisplay:
	///case iT_Refresh:
	///case iT_Unit:
	///case iT_WaO:
	case iT_Image:					
						pItem = newTypedef(hCimageItem);
						if ( registerIt )
						{
							imageItemInstanceList.push_back((hCimageItem*)pItem);
							registerItem(pItem->getID(), pItem, pItem->getSymbolName());
						}
		break;
*************************** end exclusion **********************************************/
	default:						pItem = NULL;						break;
	}// endswitch

#ifdef MEMSIZE_CHK
			_CrtMemCheckpoint( &s2 );
	if ( _CrtMemDifference( &s3, &s1, &s2 ) )
	{
		_CrtMemDumpStatistics( &s3 );
	}
#endif

	return pItem;
}
/* end stevev 30mar05 */
/* added stevev 24jun05 */
RETURNCODE hCddbDevice::destroyPsuedoItem(hCitemBase* pPsuedoItem)
{// unregister, unlist and destroy
	RETURNCODE rc = SUCCESS;

	if ( pPsuedoItem == NULL )
	{
		return APP_PARAMETER_ERR;
	}
	
	unRegister(pPsuedoItem);	// take it out of the symbol and symbol-name maps

	switch ( pPsuedoItem->getIType() )
	{
	case iT_Variable:			// 1
		{
			rc = varInstanceLst.removeItem((hCVar*)pPsuedoItem);

			switch ( ((hCVar*)pPsuedoItem)->VariableType() )
			{
			case vT_Integer:		((hCinteger*)pPsuedoItem)->destroy();		break;
			case vT_Unsigned:		((hCUnsigned*)pPsuedoItem)->destroy();		break;
			case vT_FloatgPt:		((hCFloat*)pPsuedoItem)->destroy();			break;
			case vT_Double:			((hCDouble*)pPsuedoItem)->destroy();		break;
			case vT_Enumerated:		((hCEnum*)pPsuedoItem)->destroy();			break;
			case vT_BitEnumerated:	((hCBitEnum*)pPsuedoItem)->destroy();		break;
			case vT_Index:			((hCindex*)pPsuedoItem)->destroy();		break;
			case vT_Ascii:			((hCascii*)pPsuedoItem)->destroy();		break;
			case vT_PackedAscii:	((hCpackedAscii*)pPsuedoItem)->destroy();	break;
			case vT_Password:		((hCpassword*)pPsuedoItem)->destroy();		break;
			//case vT_BitString:
			case vT_HartDate:		((hChartDate*)pPsuedoItem)->destroy();		break;
			default:
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Unsupported Variable type in List or Array.");
					pPsuedoItem = NULL;
				}
			}// end subtype switch;
		}
		break;
	case iT_Array:				//15
		{			
			rc = valArrayInstanceList.removeItem((hCarray*)pPsuedoItem);
			((hCarray*)pPsuedoItem)->destroy();
		}
		break;
	case iT_List:				//26
		{	
			rc = listInstanceList.removeItem((hClist*)pPsuedoItem);
			((hClist*)pPsuedoItem)->destroy();
		}
		break;
	case iT_ItemArray:			// 9
		{
			rc = itemArrayInstanceList.removeItem((hCitemArray*)pPsuedoItem);
			((hCitemArray*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Collection:			//10
		{
			rc = collectionInstanceList.removeItem((hCcollection*)pPsuedoItem);
			((hCcollection*)pPsuedoItem)->destroy();
		}
		break;
	case iT_File:				//20
		{
			rc = fileInstanceList.removeItem((hCfile*)pPsuedoItem);
			((hCfile*)pPsuedoItem)->destroy();
		}
		break;
/******************* not implemented *********************-------------------------------------
	case iT_Command:			// 2
		{
			rc = cmdInstanceLst.removeItem((hCcommand*)pPsuedoItem);
			((hCcommand*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Menu:				// 3
		{
			rc = menuInstanceLst.removeItem((hCmenu*)pPsuedoItem);
			((hCmenu*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Method:				// 5
		{
			rc = methodInstanceList.removeItem((hCmethod*)pPsuedoItem);
			((hCmethod*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Chart:				//21
		{
			rc = chartInstanceList.removeItem((hCchart*)pPsuedoItem);
			((hCchart*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Graph:				//22
		{
			rc = graphInstanceList.removeItem((hCgraph*)pPsuedoItem);
			((hCgraph*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Axis:				//23
		{
			rc = axisInstanceList.removeItem((hCaxis*)pPsuedoItem);
			((hCaxis*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Waveform:			//24
		{
			rc = waveformInstanceList.removeItem((hCwaveform*)pPsuedoItem);
			((hCwaveform*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Source:				//25
		{
			rc = sourceInstanceList.removeItem((hCsource*)pPsuedoItem);
			((hCsource*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Grid:				//27
		{
			rc = gridInstanceList.removeItem((hCgrid*)pPsuedoItem);
			((hCgrid*)pPsuedoItem)->destroy();
		}
		break;
	case iT_Image:				//28
		{
			rc = imageItemInstanceList.removeItem((hCimageItem*)pPsuedoItem);
			((hCimageItem*)pPsuedoItem)->destroy();
		}
		break;

	//case iT_ReservedOne:			// specially used to get the image list
	//case iT_Block:				//12
	//case iT_Program:				//13
	//case iT_Record:				//14
	//case iT_VariableList:			//16
	//case iT_ResponseCodes:		//17
	//case iT_Domain:				//18
	//case iT_Member:				//19

	case iT_EditDisplay:		// 4
	case iT_Refresh:			// 6
	case iT_Unit:				// 7
	case iT_WaO:				// 8
*************************** end exclusion **********************************************/

	default:
		rc = FAILURE;
		break;
	}// endswitch

	return rc;

}/* end 24jun05 */


/*********************************************************************************************
 * isTypeCompatible
 *
 * in: Dest: A pointer to the typedef's itembase
 *     Sorc: A pointer to an itembase to be inserted or set equal to the Dest
 *
 *out: True if it will work, False if they do not match
 *
 *	as per Wally 07:50 27oct11 - Dest and Sorc Types must exactly match in type and size.
 *		reasoning:  Insert is only done in a method.  The DD writer has all the opportunity
 *				in the world to make them match.  Numeric promotion and warnings on loss of
 *				resolution (needed on near matches)is not worth explaining to the average user.
 *  
 ********************************************************************************************/
bool hCddbDevice::isTypeCompatible(hCitemBase* pDest, hCitemBase* pSorc)
{
	bool bReturn = false;
	itemType_t DstItype, SrcItype;

	if (pDest == NULL || pSorc == NULL)
		return false;
	if (pDest == pSorc)
		return true;// I believe their types will match

	DstItype = pDest->getIType();
	SrcItype = pSorc->getIType();
	if (DstItype != SrcItype)
		return false;


	switch ( DstItype )// and SrcItype
	{
	case iT_Variable:
		{

			variableType_t vDType = (variableType_t) (((hCVar*)pDest)->VariableType());
			variableType_t vSType = (variableType_t) (((hCVar*)pSorc)->VariableType());
			int iDSize = ((hCVar*)pDest)->VariableSize();
			int iSSize = ((hCVar*)pSorc)->VariableSize();

			if ( (vDType == vSType) && (iDSize == iSSize) )
				bReturn = true;
			else
				bReturn = false;
		}
		break;

	case iT_Array:	
		{
			hCitemBase* pSrcTypeDef = ((hCarray*)pSorc)->getTypePtr();	
			hCitemBase* pDstTypeDef = ((hCarray*)pDest)->getTypePtr();
			int iSrcElmCnt =  ((hCarray*)pSorc)->getElemCnt();
			int iDstElmCnt =  ((hCarray*)pDest)->getElemCnt();

			if ( isTypeCompatible(pDstTypeDef, pSrcTypeDef) && (iSrcElmCnt == iDstElmCnt) )
				bReturn = true;
			else
				bReturn = false;
		}
		break;

	case iT_List:	
		{
			bool SrcCapDefined =  ((hClist*)pSorc)->isCapacityDDdefined();		
			bool DstCapDefined =  ((hClist*)pDest)->isCapacityDDdefined();
			int iSrcCap = (SrcCapDefined)?((hClist*)pSorc)->getCapacity():0; 
			int iDstCap = (DstCapDefined)?((hClist*)pDest)->getCapacity():0; 
			hCitemBase* pSrcTypeDef = ((hCarray*)pSorc)->getTypePtr();	
			hCitemBase* pDstTypeDef = ((hCarray*)pDest)->getTypePtr();

			if ( isTypeCompatible(pDstTypeDef, pSrcTypeDef) && (iSrcCap == iDstCap) )
				bReturn = true;
			else
				bReturn = false;
		}
		break;

	case iT_ItemArray:		
	case iT_Collection:	
	case iT_File:
		{/* hCgrpItmBasedClasses are handled the same: if their contents match, they match*/
			RETURNCODE r;
			groupItemPtrList_t giplSrc, giplDst;
			r  = ((hCgrpItmBasedClass*)pSorc)->getList(giplSrc);
			r |= ((hCgrpItmBasedClass*)pDest)->getList(giplDst);
			groupItemPtrIterator_t SIT, DIT;// ptr2ptr2hCgroupItemDescriptor
			hCgroupItemDescriptor *pSrcGID, *pDstGID;
			hCitemBase *pSrcElem, *pDstElem;
			bool bothMatch = true;
			if ( r || (giplSrc.size() != giplDst.size()))
				bothMatch = false;// we had an error resolving a list or a gross size mismatch

			for (SIT  = giplSrc.begin(),  DIT  = giplDst.begin();
				 SIT != giplSrc.end() &&  DIT != giplDst.end() && bothMatch;   ++SIT, ++DIT  )
			{
				pSrcGID = *SIT;// dereference, don't use iterator as ptr
				pDstGID = *DIT;
				r  = pSrcGID->getItemPtr(pSrcElem);
				r |= pDstGID->getItemPtr(pDstElem);

				if ( r || pSrcElem == NULL || pDstElem == NULL || 
					 ! isTypeCompatible(pDstElem, pSrcElem)         )
				{
					bothMatch = false;
					break; // out of for loop
				}// else exact member match, continue with next
			}// next member pair
			if (bothMatch)
				bReturn = true;
			else
				bReturn = false;
		}
		break;

	default:	
		bReturn = false; // unknown type
		break;
	}// endswitch
	return bReturn;
}



/*<START>VMKP As provided by Steve on 01APR2004	*/	
#ifdef _DEBUG
void     hCddbDevice::aquireItemMutex(char* fileNm, int LineNum)
#else
void     hCddbDevice::aquireItemMutex(void)
#endif
{
    int c = 0, l = 0;
	while ( pItemMutxCntrl->aquireMutex(5000) != SUCCESS )
	{
		c++;
			//clog << "fail ";
		if (c > 10)
		{
                    //exit(23); // attempt to abort
                    // abortion is unacceptable
                    // returning to allow the system to work with items that are locked is unacceptable
                    // we enter a deadlocked state!!!!!!!!!!!!!!!!! unless the holder gives it up.
                    c = 0;
                    l++;
                    LOGIT(CLOG_LOG,"AquireItemMutex failed %d times (system locked).\n",l);
		}
	}
}
/*<END>VMKP As provided by Steve on 01APR2004	*/	


void     hCddbDevice::returnItemMutex(void)
{
//clog<<" retrning Item Mutex for device "<<hex<< devDDkey << dec <<" "<<endl;
	pItemMutxCntrl->relinquishMutex();
}



/* stevev 11apr11 - abort from anywhere..mainly no list element */
void hCddbDevice::abortDD(int retCode, hCitemBase* pList)
{
	itemID_t ID = 0;
	if (pList)
		ID = pList->getID();
	hSdispatchPolicy_t pol =  getPolicy();
	if (pol.abortOnNoListElem )
	{
		LOGIT(UI_LOG|USRACK_LOG|CLOG_LOG,L"Invalid List access. DD aborted.\n");
		notifyVarUpdate(ID,  ABORT_dd, retCode);
//		systemSleep(100);
	}
	// else, just keep running
}

void     hCddbDevice::notifyVarUpdate(itemID_t i,  NUA_t isChanged, long aTyp)
{
#ifdef _DEBUG
 #ifdef CHECK_NOTIFY /* top of this file */
	hCitemBase* pItem = NULL;
	if(getItemBySymNumber(i,&pItem)==SUCCESS && pItem != NULL && 
	   pItem->IsVariable()  )
		((hCVar*)pItem)->isNotified = true;
 #endif   /* all but method'd start & stop*/
#endif
	if ( pCmdDispatch != NULL )
	{
		pCmdDispatch->notifyVarUpdate(i,isChanged,aTyp);
	}// else do nothing for now
}


RETURNCODE    hCddbDevice::aquireIndexMutex(int millis
#ifdef _DEBUG
											           , char* fileNm, int LineNum )
#else
											           )
#endif
{
    int c = 0, l = 0;
	if (pCmdDispatch)
	{
#ifdef _DEBUG
		return pCmdDispatch->getIndexMutex(millis, fileNm, LineNum );
#else
		return pCmdDispatch->getIndexMutex(millis);
#endif
	}
	else
		return FAILURE;
}
/*<END>VMKP As provided by Steve on 01APR2004	*/	


void     hCddbDevice::returnIndexMutex(void)
{
	if ( pCmdDispatch != NULL )
	{
        pCmdDispatch->returnIndexMutex();
	}
	// else - play like its returned
}


RETURNCODE hCddbDevice::GetLastMethodsReturnCode(void) // this returns the error code returnrd by the method
{
	return nLastMethodsReturnCode;
}

RETURNCODE hCddbDevice::fillme(hCfile* fileClass)
{
	RETURNCODE rc = FAILURE;
	// find file
	hCformatData*  pOneData = NULL;

	if (fileClass == NULL || m_pFileSupport == NULL)
	{
		return FILE_RV_FAILED;
	}
	// finds &/or generates file
	rc = m_pFileSupport->OpenFileFile(&ddbDeviceID, fileClass->getID());

	// verify header and format
	if ( rc != SUCCESS )
	{//	if not exist, return that code
		if ( rc == FILE_RV_FILE_CREATED ) // didn't find it, made a new one
		{
			m_pFileSupport->CloseFile();
			rc = FILE_RV_NOT_AVAILABLE;
		}
	}
	else // success
	{// parse the header for the identity and the format
		unsigned char hdrBuf[HDR_SIZE+STRHEADROOM];
		uchar*   pB = &(hdrBuf[0]);
		uchar*   pH; // holder for memory alloc
		int len = HDR_SIZE, f;
		FileFormat_t fmt;
		Indentity_t fileID;

		// read 400 bytes from the file
		rc = m_pFileSupport->ReadFile(hdrBuf, len);
		if ( rc == SUCCESS && len == HDR_SIZE)
		{
			f = 0;
			rc = hCformatBase::extractHeader(pB, len,fileID,f );// a static method		
			// fills indentity & format and reads thru <HFDbody>
			if ( rc != SUCCESS )
			{
				m_pFileSupport->CloseFile();
				return FILE_RV_FAILED;
			}// else continue
			fmt = (FileFormat_t)f;
			if ( fmt > FMT_Undefined && fmt < FMT_Unsupported )
			{// call setFormat - will instaniate a format class or return not supported
				rc = fileClass->setFormat(fmt);
				if ( rc != SUCCESS )
				{
					m_pFileSupport->CloseFile();
					return FILE_RV_NOT_SUPPORTED;// format not supported
				}
			}
			if ( len > 0 )// insert anything left over from the header
			{
				fileClass->fill(pB,len);
#ifdef _DEBUG
				if ( len != 0 )
				{
					LOGIT(CLOG_LOG,"File's fill() did not empty the buffer.(%d)\n",len);
				}
#endif
			}


			// allocate buffer
			pH  = new uchar[FILE_RD_BUFF_SIZE];
			rc  = SUCCESS;
			
			// loop
			while(rc == SUCCESS)
			{
				pB  = pH;
				len = FILE_RD_BUFF_SIZE;
				//	read buffer chunk from file
				rc = m_pFileSupport->ReadFile(pB, len);

				//	if none read (eof)
				if ( rc == FILE_RV_EOF && len == 0 )
				{	// close the file so we can open it during debugging					
					m_pFileSupport->CloseFile();
					//		call fill with empty buffer
					rc = fileClass->fill(pB,len);
					// we be done - exit
					rc = FILE_RV_EOF;// fall out to exit
				}
				else
				{//	call fill with the data chunk
					rc = fileClass->fill(pB,len);
				}
				//	loop
			}// wend
			delete[] pH;
		}// bad read... just exit
	}// endelse file opened
	return rc;
}

// save the file, do not return till complete/failed
RETURNCODE hCddbDevice::saveme(hCfile* fileClass)
{
	RETURNCODE rc = FAILURE;
	hCformatData*  pOneData = NULL;
	//dataIterator_t fmtDiT;
	dataDITerator_t rvsFmtIt;

	if (fileClass == NULL || m_pFileSupport == NULL)
	{
		return FILE_RV_FAILED;
	}
	// finds/generates file
	rc = m_pFileSupport->OpenFileFile(&ddbDeviceID, fileClass->getID(), true);//delete existing

	// write the device file header in the correct format
	if ( rc == SUCCESS || rc == FILE_RV_FILE_CREATED ) // we opened the file
	{// lookup the needed save format --- when more than one available....
	 // call setFormat - will instaniate a format class
		rc = fileClass->setFormat(FMT_HCFformat);// the only one we're supporting for now
		if ( rc != SUCCESS )
		{
			m_pFileSupport->CloseFile();
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: Device could not set the format while saving file.\n");
			return rc;
		}
#ifdef _DEBUG
		int sz=0;
		int sc = 0;
#endif
		
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"\n* file opened.");
							logTime();// isa logif start_stop (has newline)

		// The first entry (with the pointer null) will return with the
		//		disk file header and the File item header.  Further calls will
		//		serialize the contents of the file
		for (rc = fileClass->empty(pOneData); rc == SUCCESS && pOneData != NULL; )
		{
			// write the data
#ifdef _DEBUG
			sz = pOneData->size();
			sc = 0;
#endif
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"* file empty, now writing %d parts.",pOneData->size());
							logTime();// isa logif start_stop (has newline)
			//for ( fmtDiT = pOneData->begin(); fmtDiT != pOneData->end(); ++fmtDiT )
			for ( rvsFmtIt = pOneData->rbegin(); rvsFmtIt != pOneData->rend(); ++rvsFmtIt )
			{// ptr2aptr2a hCdataElement       uchar* buffer, int buffLen
				//rc = m_pFileSupport->WriteFile((*fmtDiT)->buffer, (*fmtDiT)->length );

				rc = m_pFileSupport->WriteFile((*rvsFmtIt)->buffer, (*rvsFmtIt)->length );

#ifdef _DEBUG
				sc++;
				if ( sc > sz )
				{assert(0);
				}
#endif
				if ( rc != SUCCESS )
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: writing file.\n");
					continue; // will exit loop w/ error
				}
			}
			
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"* file written, going to empty again.");
							logTime();// isa logif start_stop (has newline)
			// call the file's empty() function for more data
			rc = fileClass->empty(pOneData);
			if ( rc != SUCCESS )
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: aquiring data from the file class.\n");
			}
		}
		m_pFileSupport->CloseFile();
			
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"* file written, and closed.");
							logTime();// isa logif start_stop (has newline)
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"\n");
	}
	else
	{
		m_pFileSupport->CloseFile();
		rc = FILE_RV_NOT_AVAILABLE;
	}
	return rc;
}

/* stevev 09dec04 */
RETURNCODE hCddbDevice::subscribe(int& returnedHandle,   ddbItemList_t SubscribedList,
				hCpubsub* pPublishTo,  unsigned long cycleTime,	 int cyclesPerPublish  )
{
	RETURNCODE rc = FAILURE;
	subscription_t s;

	s.subscribedList   = SubscribedList;
	s.pPublishto       = pPublishTo;
	s.cycleTime        = cycleTime;
	s.cyclesPerPublish = cyclesPerPublish;

	returnedHandle = subscriptionHandler.addSubscription(s);
	if ( returnedHandle )
		rc = SUCCESS;
	return rc;
}

RETURNCODE hCddbDevice::unsubscribe(int handle)
{
	RETURNCODE rc = SUCCESS;

	subscriptionHandler.delSubscription(handle);

	return rc;
}
/* endstevev 09dec04 */

/* stevev 06apr10 - working on bug 3034, moved critical reads to device so it my be called 
					from somewhere other than in the document  */
/* stevev 16feb12 - for INVENSYS allow critical reads Plus staled data.  This is pre upload
					so we will set the mode to om_DownLoading, read crits and then wait for 
					completions.
	behaviour == 1 - read crits then all stales
*/
void hCddbDevice::readCriticalparams(int behaviour /*= 0*/)
{ 
	RETURNCODE rc = SUCCESS;
	CValueVarient   vval;// we don't do anything with it

	if ( behaviour == 1)// download these and whatever they set
	{
		setOpMode( om_DownLoading );
	}
	LOGIT(STAT_LOG|CLOG_LOG,"Reading Critical Parameters\n");


	// stevev 13apr10 - bug 3034
	LOGIF(LOGP_NOTIFY)(CLOG_LOG,"readCriticalParameters  is doing STRT_methPkt.\n");
	notifyVarUpdate(0,  STRT_methPkt);
	// end 13apr10


	/* stevev 05oct10 - read any device indexes that haven't been read in the criticals */
	// stevev 26feb15 - these need to be read first.  If there are dependencies, this will
	//	fail and the leveled version will succeed in the next section. There are dependencies
	//  on these that aren't calculated.  eg say the first critical parameter is a 
	//	independent variable in a relation and dependent variables use indexes to resolve.
	//	you will get errors if you don't have those dependencies.
	// 2015 can't handle this structure...CVarList::itemList_it listIttr;
	CitemListBase<iT_Variable, hCVar>::iterator listIttr;

	for ( listIttr = varInstanceLst.begin(); listIttr != varInstanceLst.end(); ++listIttr)
	{// ptr2ptr2var
		hCVar* pVr = *listIttr;
#ifdef _DEBUG
		itemID_t thisID = pVr->getID();
#endif
		if (pVr->VariableType() == vT_Index && !(pVr->IsLocal()) )// device index
		{
			DATA_QUALITY_T   dqT = pVr->getDataQuality();
			if ( (dqT == DA_NOT_VALID  || dqT == DA_STALEUNK) && ! pVr->Idepend() )// never been read
			{
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"$   Critical index read 0x%04x ('%s')\n",
												pVr->getID(),	pVr->getName().c_str() );
				rc = ReadCritical(pVr, vval);// force a read
			}
		}
	}
	/* end 05oct10    - read critical indexes */



	itemIDlist_t* pCrit = (itemIDlist_t*)getListPtr(CitemType(iT_Critical_Item));
	if (pCrit && pCrit->size() > 0)
	{// read 'em now
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"$Critical Item read Length 0x%x\n",pCrit->size());
		hCitemBase* pIBase = NULL;


		for (itemIDlist_t::iterator z = pCrit->begin(); z < pCrit->end(); z++)
		{
			// stevev 13sep06 - with the expanded dependency, there are a lot of items in the list
			if ( getItemBySymNumber(*z,& pIBase) == SUCCESS && pIBase != NULL )
			{
				if ( pIBase->IsVariable() )
				{
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"$   Critical read 0x%04x\n",*z);
					// stevev 20sep06 - if an item is critical AND has a DEFAULT_VALUE
					// we must force a read from device - use ReadCritical()
					//
					rc = ReadCritical(pIBase, vval);// force a read

					if (rc != SUCCESS )
					{
						LOGIT(CLOG_LOG|UI_LOG,"$   Critical read 0x%04x FAILED: "
																"abort generation.\n",*z);
						LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc.generateDevice had a "
							" Critical Read Failure, stopping critical reads.\n");	
						LOGIF(LOGP_NOTIFY)(CLOG_LOG,"readCriticalParameters is doing END_methPkt.\n");
						notifyVarUpdate(0,  END_methPkt);	
						clrOpMode(om_DownLoading);// clear dwnload only
						return ;
					}
					systemSleep(1);
				}
#ifdef _DEBUG
				else
				{
					LOGIT(CLOG_LOG,"Critical item %s (0x%04x) a %s skipped reading.\n",
						pIBase->getName().c_str(),pIBase->getID(),pIBase->getTypeName());
				}
#endif
			}
		}// next critical

#ifdef _DEBUG
		dmpVarNotification();
		dmpVarDependency();
#endif
	}
	else
	{
		LOGIT(CLOG_LOG,"$ Empty Critical item list.\n");
	}
		
		
	// stevev 13apr10 - bug 3034
	LOGIF(LOGP_NOTIFY)(CLOG_LOG,"readCriticalParameters  is doing END_methPkt(v2).\n");
	notifyVarUpdate(0,  END_methPkt);
	// end 13apr10
	
	if ( behaviour == 1)// download these and whatever they set
	{
		// wait for any stale's to be read in too
		int waitCnt = 0;
		pCmdDispatch->setCond(dc_Reads);
		while ( (pCmdDispatch->getCond() & dc_Reads) != 0 && waitCnt < 60 )// fifteen seconds
		{
			systemSleep(250);// let the idle task weigh and the sender send.
			waitCnt++;
		}
	}

	clrOpMode(om_DownLoading);// clear dwnload only
}
/* end additions 06apr10 */

// implementation moved from .h file 26jun15 by stevev to set key at the same time
void  hCddbDevice::setIdentity(Indentity_t& id) 
{
	ddbDeviceID=id;
	
	devDDkey =	((UINT64)(id.wManufacturer & 0xffff)) << 48;
	devDDkey |=	((UINT64)(id.wDeviceType  & 0xffff) ) << 32;
	devDDkey |=	((UINT64)(id.cDeviceRev   & 0x00ff) ) << 16;
	devDDkey |=	((UINT64)(id.cSoftwareRev & 0x00ff) );// << 0
}
#ifdef _DEBUG
	void hCddbDevice::dmpVarNotification()
 #ifdef CHECK_NOTIFY
	{
		varInstanceLst.varNotificationReport();
	}
 #else
	{}
 #endif
#endif
#ifdef _DEBUG
	void hCddbDevice::dmpVarDependency()
 #ifdef CHECK_NOTIFY
	{
		varInstanceLst.varDependencyReport();
	}
 #else
	{}
 #endif
#endif

#if 0  /* these are supposed to be somewhere else */
/*************************************************************************************************
 * Transfer channel - instead of a whole file for one routine
 ************************************************************************************************/
// returns 0 success, non-zero reason code, -1 is failure,bi-error
// Re-entrant, the itemNumber == itemBeingTransfered only on the first entry
	
int CxmtrTransferChannel::fillPortBuffer(unsigned long itemNumber)
{
	hCitemBase* pItem = NULL;
	
	if (pDevice == NULL) 
		return -1;// big error

	if ( pDevice->getItemBySymNumber(itemNumber,&pItem) != SUCCESS || pItem == NULL)
	{
		return -1; //failure
	}
	if (itemNumber == itemBeingTransfered && activeChunk == -1 && transferBuffer.size() == 0)
	{// very first entry
		bufChunk_t firstCnk;
		transferBuffer.push_back(firstCnk);
		activeChunk = 0;
	}

	return fillPortBuffer( pItem );
}
int CxmtrTransferChannel::fillPortBuffer(hCitemBase* pItem)
{
	RETURNCODE rc = FAILURE;

	if (pDevice == NULL) 
		return -1;// big error

	switch(pItem->getIType())
	{
	case iT_Variable:		//VARIABLE_ITYPE 	       1
		{// it could be a string that is a novel long.
			// we must be sure that there is enough memory to hold it
			rc = fillVar((hCVar*)pItem);
		}
		break;
	case iT_Menu:			//MENU_ITYPE               3
		{// data only items in a depth first transition
			rc = fillMenu((hCmenu*)pItem);
		}
		break;
	case iT_ItemArray:		//ITEM_ARRAY_ITYPE         9
		{// data only items in a depth first transition
			rc = fillIarr((hCitemArray*)pItem);
		}
		break;
	case iT_Collection:		//COLLECTION_ITYPE        10
		{// data only items in a depth first transition
			rc = fillColl((hCcollection*)pItem);
		}
		break;
	case iT_Array:			//ARRAY_ITYPE             15
		{// data only items in a depth first transition
			rc = fillVarr((hCarray*)pItem);
		}
		break;
	case iT_File:			//FILE_ITYPE			  20
		{// data only items in a depth first transition
			rc = fillFile((hCfile*)pItem);
		}
		break;
	case iT_List:			//LIST_ITYPE			  26
		{// data only items in a depth first transition
			rc = fillList((hClist*)pItem);
		}
		break;
	case iT_Blob:			//BLOB_ITYPE              29
		{// look it up, load it into memory, start transferring
			rc = fillBlob((hCblob*)pItem);
		}
		break;
	// no default, if the item is not data, quietly skip it
	}// endswitch
	return rc;
}

int CxmtrTransferChannel::fillVar( hCVar* pVar )
{
	RETURNCODE rc = FAILURE;
	bufChunk_t firstCnk;
	BYTE* pC = NULL;
	if (! pVar->IsValid()) return -1;
	//BYTE* pByteCnt, BYTE** ppBuf, int& mask, length
	//rc = pVar->Add(pByteCnt,ppBuf, 0,length);	
	int Len = pVar->getSize();
	int Rem = TRANSFER_DATA_SZ - TCHUNK.len;//entire chunk, transfer[active]
	if ( Len > Rem )
	{// won't fit in the current chunk
		if (Len > TRANSFER_DATA_SZ)
		{// won't fit in any chunk
			if ( pVar->VariableType() != (int)vT_Ascii )	// parameters added to VariableType PAW 03/03/09
			{
				LOGIT(CERR_LOG|UI_LOG,"Error: A non-ASCII variable is too long to transfer.\n");
				rc = FAILURE;
			}
			else
			{// it is ascii, we can break it up
				wstring lwStr = pVar->getDispValueString();
				string  lStr  = TStr2AStr(lwStr);// ascii is only allowed ascii chars
				int sL = lStr.size() + 1;
				BYTE* tempBuff = new unsigned char[sL];
				strncpy((char*)tempBuff,lStr.c_str(),sL);
				pC = &(tempBuff[0]);
				strncpy((char*)&(TCHUNK.data[TCHUNK.len]),(char*)pC,Rem);// fill that one
				delete[] tempBuff;
				TCHUNK.len += Rem;
				pC += Rem;// where the next one would start
				sL -= Rem;	
				while (sL > 0)
				{
					activeChunk = transferBuffer.size();// the index of the next one added
					transferBuffer.push_back(firstCnk);
					if ( sL >= TRANSFER_DATA_SZ )
						Rem =  TRANSFER_DATA_SZ;
					else
						Rem = sL;
					strncpy((char*)&(TCHUNK.data[0]),(char*)pC,Rem);// fill that one
					TCHUNK.len = Rem;
					pC += Rem;
					sL -= Rem;	
				}//wend 
				// its in, we can go
				rc = SUCCESS;
			}//endelse it is ascii
		}
		else
		{// it'll fit in a new chunk
			activeChunk = transferBuffer.size();// the index of the next one added
			transferBuffer.push_back(firstCnk);
			int V = 0; pC = &(TCHUNK.data[0]);// its a new chunk
			rc = pVar->Add(&(TCHUNK.len), &pC, 0, V);//bytecount and data are updated
		}
	}
	else
	{// it'll fit in what we have
		int V = 0; pC = &(TCHUNK.data[TCHUNK.len]);// its an existing chunk
		rc = pVar->Add(&(TCHUNK.len), &pC, 0, V);
	}
	return rc;
}
int CxmtrTransferChannel::fillMenu( hCmenu* pMenu )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pMenu == NULL || ! pMenu->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid menu.\n");
		return COND_RESOLVED_TO_INVALID;
	}
	menuItemList_t localList;
	if ( pMenu->aquire( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Menu transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list
	menuItmLstIT_t milIt;

	for (milIt = localList.begin(); milIt != localList.end(); ++ milIt)
	{
		hCmenuItem* pMI = (hCmenuItem*)&(*pMI);
		if( pMI->getRef().resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillIarr( hCitemArray* pIarr )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pIarr == NULL || ! pIarr->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid reference array.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pIarr->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Reference Array transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillColl( hCcollection* pColl )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pColl == NULL || ! pColl->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid collection.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pColl->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Collection transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillVarr( hCarray* pVarr )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pVarr == NULL || ! pVarr->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid array.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	ddbItemList_t localList;
	if ( pVarr->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: List transfer failed to get an array.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	ddbItemLst_it giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCitemBase
		pItem = *((hCitemBase**)&(*giPIt));
		if( pItem != NULL && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillFile( hCfile* pFile )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pFile == NULL || ! pFile->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid file.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pFile->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: File transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillList( hClist* pList )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pList == NULL || ! pList->IsValidTest())
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid list.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	ddbItemList_t localList;
	if ( pList->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIT(CERR_LOG|UI_LOG,"Error: List transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	ddbItemLst_it giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCitemBase
		pItem = *((hCitemBase**)&(*giPIt));
		if( pItem != NULL && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int CxmtrTransferChannel::fillBlob( hCblob* pBlob )
{// look it up, load it into memory, start transferring
	return FAILURE;
}

int CxmtrTransferChannel::emtyPortBuffer(unsigned long itemNumber)
{// do all the above but backwards
	return FAILURE;
}
int CxmtrTransferChannel::emty(hCitemBase* pItem)
{// do all the above but backwards
	return FAILURE;
}
//////////////// helper functions //////////////////////////////////////////////

int CxmtrTransferChannel::setupCmd111(transferFunc_e fc)
{	
	CValueVarient  locVal;

//  @ false (NULL) will execute fill
	if (pPN != NULL || fillPointers() == BI_SUCCESS )
	{//ptrs ready		
		locVal = portNumber;
		pPN->setRawDispValue(locVal);
	
		locVal = (unsigned char)fc;
		pFC->setRawDispValue(locVal);
	
		locVal = max_segment_len;
		pMMaxSeg->setRawDispValue(locVal);
		
		locVal = masterSync;
		pPN->setRawDispValue(locVal);

		return BI_SUCCESS;
	}
	// else exit with error
	return BI_ERROR;
}

int CxmtrTransferChannel::fillPointers(void)
{
	hCitemBase* pIB= NULL;

	if( pDevice->getItemBySymName(string(RESPONSECD_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pRC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICESTAT_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pDS = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(PORT_NUMBR_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pPN = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(FUNCTIONCD_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pFC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(MASTERMAXL_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pMMaxSeg = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICEMAXL_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pSMaxSeg = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(MASTERBYTC_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pMBC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICEBYTC_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pDBC = (hCVar*)pIB;

	return BI_SUCCESS;
}

int CxmtrTransferChannel::fillPortInfo(int cmdNo)
{
	CValueVarient  locVal;
	if ( cmdNo != 111 && cmdNo != 112 )// minor diffs between the two
		return BI_ERROR;
	if ( pPN != NULL || fillPointers() == BI_SUCCESS )
	{//ptrs ready
		locVal = pPN->getRawDispValue();// port_number,
		if ( ((int)locVal) != portNumber )
			return BI_ERROR;

		locVal = pRC->getRawDispValue();//response
		respCd = (unsigned char)locVal;
		locVal = pFC->getRawDispValue();// function
		funcCd = (unsigned char)locVal;
		locVal = pDS->getRawDispValue();// device status
		devStat= (unsigned char)locVal;
		if (cmdNo == 111)
		{
			locVal = pSMaxSeg->getRawDispValue();// slave max seg len
			if ( (unsigned char)locVal < max_segment_len)
			{
				max_segment_len = (unsigned char)locVal;
			}
		}
		// else, we don't care about the redundent byte count in 112
		locVal = pMBC->getRawDispValue();//response
		masterSync = (unsigned char)locVal;
		locVal = pDBC->getRawDispValue();// function
		trans_Sync = (unsigned char)locVal;

		return BI_SUCCESS;
	}
	return BI_ERROR;
}
#endif //CxmtrTransferChannel support
/*************************************************************************************************
 * ERROR DEVICE
 ************************************************************************************************/
// this class is returned in an error condition from the global service interface
// it does as little as possible - always sends a cerr
hCerrDeviceSrvc::hCerrDeviceSrvc()
{// will always happen
}


//prashant: oct 2003
hCMethSupport*     hCerrDeviceSrvc::getMethodSupportPtr()
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: Could not get Method Support pointer.\n");
	return NULL;
}
//stevev 20jan05
hCcritical*     hCerrDeviceSrvc::getCriticalPtr()
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Could not get Critical Class pointer.\n");
	return NULL;
}

RETURNCODE hCerrDeviceSrvc::getItemBySymNumber(itemID_t symNum, hCitemBase** ppReturnedItempointer)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: error services getItem was called for 0x%04x.\n",symNum);
	if ( ppReturnedItempointer != NULL && (*ppReturnedItempointer) != NULL)
		(*ppReturnedItempointer) = NULL;
	else
		LOGIT(CERR_LOG, "     : error services getItem was called with a null pointer.\n");
	return FAILURE;
}


RETURNCODE hCerrDeviceSrvc::getItemBySymName(const string& symName, hCitemBase** ppReturnedItempointer)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: error services getItem by Name was called for %s.\n",symName.c_str());
	if ( ppReturnedItempointer != NULL && (*ppReturnedItempointer) != NULL)
		(*ppReturnedItempointer) = NULL;
	else
		LOGIT(CERR_LOG, "     : error services getItem by Name was called with a null pointer.\n");
	return FAILURE;
}

RETURNCODE hCerrDeviceSrvc::ReadImd(itemID_t itemID, CValueVarient& ppReturnedDataItem,bool isAtest)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: error services Read by ID was called for 0x%04x.\n",itemID);
	ppReturnedDataItem.clear();
	return FAILURE;
}
RETURNCODE hCerrDeviceSrvc::ReadImd(hCitemBase* itemPtr,  CValueVarient& ppReturnedDataVal,bool isAtest)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services Read by ptr was called.\n");
	ppReturnedDataVal.clear();
	return FAILURE;
}
RETURNCODE hCerrDeviceSrvc::Read(vector<hCitemBase*>&itemList)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services Read List by was called.\n");
	return FAILURE;
}

RETURNCODE hCerrDeviceSrvc::WriteImd(itemID_t             itemID,   CValueVarient* dataVal)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services Write by ID was called for 0x%04x.\n",itemID);
	dataVal->clear();
	return FAILURE;
}
RETURNCODE hCerrDeviceSrvc::WriteImd(hCitemBase*          itemPtr,  CValueVarient* dataVal)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services Write by Ptr was called.\n");
	dataVal->clear();
	return FAILURE;
}
RETURNCODE hCerrDeviceSrvc::Write(vector<hCitemBase*>& itemList)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services Write List was called.\n");
	return FAILURE;
}

DD_Key_t     hCerrDeviceSrvc::getDDkey(void)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services getDDkey was called.\n");
	return 0;
}

Indentity_t& hCerrDeviceSrvc::getIdentity(void)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services getIdentity was called.\n");
	return (ddbDeviceID);
}

RETURNCODE hCerrDeviceSrvc::executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services executeMethod was called.\n");
	return FAILURE;
}

RETURNCODE hCerrDeviceSrvc::executeMethod(vector<hCmethodCall>& actionList)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services executeMethod was called.\n");
	return FAILURE;

}

unsigned int hCerrDeviceSrvc:: getMEEdepth()
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services getMEEdepth was called.\n");
	return FAILURE;

}	
RETURNCODE hCerrDeviceSrvc::executeMethod(vector<hCmethodCall>& actionList,
																  CValueVarient& returnValue)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services executeMethod List was called.\n");
	return FAILURE;
}
RETURNCODE hCerrDeviceSrvc::sendMethodCmd(int commandNumber,int transactionNumber, 
															indexUseList_t* pI)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services sendMethodCmd was called.\n");
	return NO_RESPONSE;// normally returns host error
}
RETURNCODE hCerrDeviceSrvc::sendCommand(int commandNumber,int transactionNumber)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services sendCommand was called.\n");
	return NO_RESPONSE;// normally returns host error
}
void       hCerrDeviceSrvc::registerItem(itemID_t newItemID, hCitemBase* pNewItem, string& symName)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services registerItem was called for 0x%04x, %s.\n",newItemID,
																				symName.c_str());
}
void       hCerrDeviceSrvc::unRegister(hCitemBase* rmvItem)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services unRegister Item was called for 0x%04x\n",
																				rmvItem->getID());
}

void	   hCerrDeviceSrvc::registerElement(unsigned long elemIdx,string& elemName, itemID_t grpID)
{
	return;// no-op;
}
RETURNCODE hCerrDeviceSrvc::getElemNameByIndex(unsigned long elemIdx, string& returnedName)
{
	return FAILURE;
}

hCobject*  hCerrDeviceSrvc::getListPtr(CitemType  it)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services getListPtr was called for %s.\n",it.getTypeStr());
	return NULL;
}
/*---22aug08---
void	hCerrDeviceSrvc::systemLog(int channel, char* format, ...) 
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: errorDevice's systemLog was called.\n");
}
void	hCerrDeviceSrvc::systemLog(int channel, int errNumber,...) 
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: errorDevice's systemLog(errNo) was called.\n");
}
--------------*/
RETURNCODE hCerrDeviceSrvc::GetLastMethodsReturnCode(void) // this returns the error code returnrd by the method
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services GetLastMethodsReturnCode was called\n");
	return NULL;
}

hSdispatchPolicy_t hCerrDeviceSrvc::getPolicy(void)
{
	hSdispatchPolicy_t pt;
	pt.allowSyncWrites = 0;pt.allowSyncReads =0;
	pt.incrementalWrite= 0;pt.incrementalRead=0;
	pt.isReplyOnly = 0;

	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services getPolicy was called.\n");
	return pt;
}
#ifdef _DEBUG
void     hCerrDeviceSrvc::aquireItemMutex(char* fileNm, int LineNum)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services aquireItemMutex was called from '%s' at %d.\n",fileNm,LineNum);
	// This could be damaging
}
#else
void     hCerrDeviceSrvc::aquireItemMutex(void)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services aquireItemMutex was called.\n");
	// This could be damaging
}
#endif
void     hCerrDeviceSrvc::returnItemMutex(void)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services returnItemMutex was called.\n");
}
void     hCerrDeviceSrvc::notifyVarUpdate(itemID_t i,   NUA_t isChanged, long aTyp)
{
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: error services notifyVarUpdate was called.\n");
}

// L&T Modifications : MethodSupport - begin
RETURNCODE hCddbDevice::executeMethodForMS(hCmethodCall oneMethod, CValueVarient& returnValue, hCddbDevice *pCurDev)
{
	RETURNCODE rc = SUCCESS;

	if(pMEE == NULL ||  m_pMethSupportInterface == NULL )	
	{
		return FAILURE;
	}
	
	if (pCmdDispatch)
	{
		pCmdDispatch->disableAppCommRequests();
			DEBUGLOG(CLOG_LOG,"Device executeMethod disables appComm\n");
		pCmdDispatch->notifyVarUpdate(0,  STRT_methPkt);// stevev 23jan06
	}
	devIsExecuting = true;// stevev 06apr10 - bug 3034

	rc = doExecuteForMS(oneMethod,returnValue,( pMEE->GetOneMethRefNo() > 0 ), pCurDev);

	devIsExecuting = false;// stevev 06apr10 - bug 3034
	if (pCmdDispatch)
	{
		pCmdDispatch->enableAppCommRequests();
		DEBUGLOG(CLOG_LOG,"Device executeMethod enables appComm\n");
		pCmdDispatch->notifyVarUpdate(0,  END_methPkt);// stevev 23jan06
	}
	return rc;
}

RETURNCODE hCddbDevice::doExecuteForMS(hCmethodCall oneMethod, CValueVarient& returnValue, bool isMultipleEntry, hCddbDevice *pCurDev)
{
	RETURNCODE    rc   = SUCCESS;
	
	if (oneMethod.methodID > 0 && oneMethod.m_pMeth == NULL)
	{
		hCitemBase* pItemBase;
		getItemBySymNumber(oneMethod.methodID,&pItemBase);
		oneMethod.m_pMeth = (hCmethod*)pItemBase;
	}

	if ( ! isMultipleEntry) // then we are the first entry of a call stack
	{
		devIsExecuting      = true;  // stevev 06apr10
		pMEE->m_bSaveValues = false; // clear save value flag
		cacheDispValues();           // cache display values
		// @ debug verify there are no displays to push
	}

#ifdef _DEBUG
#define PPARM	oneMethod
#else
#define PPARM
#endif
	//m_pMethSupportInterface->PreExecute(PPARM); // doesn't do much of anything right now
#ifndef __GNUC__	// L&T Modifications : MethodSupport
#ifndef METHODSUPPORT
	m_pMethSupportInterface->PreExecute(PPARM);
#else
	m_pMethSupportInterface->PreExecuteForMS(PPARM, pCurDev);
#endif
#else
	m_pMethSupportInterface->PreExecuteForMS(oneMethod, pCurDev);
#endif
	oneMethod.m_MethState = mc_Initing;/*MethodSupportforLCinMethodSupport*/
	rc = m_pMethSupportInterface->DoMethodSupportExecuteForMS(oneMethod, pCurDev);// handles pushing & poping the dialogs
	m_pMethSupportInterface->PostExecute(PPARM);// doesn't do much of anything right now

	nLastMethodsReturnCode = rc;

	if ( ! isMultipleEntry)
	{
		uncacheDispVals(pMEE->m_bSaveValues); 
		pMEE->m_bSaveValues = false;   
		// clear save values flag
		devIsExecuting      = false;  // stevev 06apr10
	}

	return rc;
}
// L&T Modifications : MethodSupport -end
/*************************************************************************************************
 *
 *   $History: ddbDevice.cpp $
 * 
 * *****************  Version 7  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 6  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 4/07/03    Time: 9:55a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 7/25/02    Time: 11:59a
 * Updated in $/DD Tools/DDB/dbRead/ddbLib
 * before 7/25 restart of work
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 5/24/02    Time: 11:27a
 * Created in $/DD Tools/DDB/dbRead/ddbLib
 * initial
 * 
 *************************************************************************************************
 */
