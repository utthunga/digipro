/*************************************************************************************************
 *
 * $Workfile: ddbPayload.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the top level payload class implementation (for conditional implementation)
 * 
 */


#include <stdio.h>
#include "ddbItems.h"
#include "ddbGlblSrvInfc.h"
#include "DDBpayload.h"


/* stevev 25jul06 - modified to only generate a new class - NEVER copies anything */
/*static*/
void* hCpayload::newPayload(DevInfcHandle_t h, payloadType_t ldType, hCpayload* pPayload)
{
	void* pRet = NULL;
	payloadType_t localLdType = pltUnknown;

	if ( pPayload == NULL )	// indicates this is an aC class we are working on
	{
		localLdType = ldType;
	}
	else
	{// ldType is invalid - indicates a hC class call
		if ( pPayload->validPayload(pltddlStr) )
		{	localLdType = pltddlStr;
			// sjv 25jul06:pRet = new hCddlString(*((hCddlString*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltddlLong) )
		{	localLdType = pltddlLong;
			// sjv 25jul06:pRet = new hCddlLong(*((hCddlLong*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltbitStr) )
		{	localLdType = pltbitStr;
			// sjv 25jul06:pRet = new hCbitString(*((hCbitString*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltEnumList) )
		{	localLdType = pltEnumList;
			// sjv 25jul06:pRet = new hCenumList(*((hCenumList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltDataItmLst) )
		{	localLdType = pltDataItmLst;
			// sjv 25jul06:pRet = new hCdataitemList(*((hCdataitemList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltMenuList) )
		{	localLdType = pltMenuList;
			// sjv 25jul06:pRet = new hCmenuList(*((hCmenuList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltRespCdList) )
		{	localLdType = pltRespCdList;
			// sjv 25jul06:pRet = new hCRespCodeList(*((hCRespCodeList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltGroupItemList) )
		{	localLdType = pltGroupItemList;
			// sjv 25jul06:pRet = new hCgroupItemList(*((hCgroupItemList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltReferenceList) )
		{	localLdType = pltReferenceList;
			// sjv 25jul06:pRet = new hCreferenceList(*((hCreferenceList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltReference) )
		{	localLdType = pltReference;
			// sjv 25jul06:pRet = new hCreference(*((hCreference*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltExpression) )
		{	localLdType = pltExpression;
			// sjv 25jul06:pRet = new hCexprPayload(*((hCexprPayload*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltMinMax) )
		{	localLdType = pltMinMax;
			// sjv 25jul06:pRet = new hCMinMaxList(*((hCMinMaxList*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltIntWhich) )
		{	localLdType = pltIntWhich;
			// sjv 25jul06:pRet = new hCddlIntWhich(*((hCddlIntWhich*)pPayload));
		}
		else
		if ( pPayload->validPayload(pltGridMemberList) )
		{	localLdType = pltGridMemberList;
			// sjv 25jul06:pRet= new hCGridGroupList(*((hCGridGroupList*)pPayload));
		}
		else
		{// pltUnknown; pltLastLdType:;	default   
			localLdType = pltUnknown;	
			//cerr << "ERROR: newPayload was handed a Type (w/Ptr) it did not recognize."<<endl;
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: newPayload was handed a Type (w/Ptr) it did not recognize.\n");
			pRet = (void*)0L; 
		}// end if
	}
	
	switch(localLdType)
	{
	case pltddlStr		: pRet = new hCddlString(h);	break;
	case pltddlLong		: pRet = new hCddlLong(h);		break;
	case pltbitStr		: pRet = new hCbitString(h);	break;

	case pltEnumList	: pRet = new hCenumList(h);	    break;
	case pltDataItmLst	: pRet = new hCdataitemList(h); break;
	case pltMenuList	: pRet = new hCmenuList(h);	    break;//list is not parent
	case pltRespCdList	: pRet = new hCRespCodeList(h); break;
//	case pltEdDispLst	: pRet = new hCedDispList(h);	break;

//	pltTransList,		// 9	COND2TRLIST
//	pltMenuList,		//10	COND2PAYMENU
//	pltRespCdList,		//11	COND2PAYRESPCD
//	pltTypeType,		//12	COND2PAYTYPETYPE
//	pltEnumList,		//13	COND2PAYENUM
//	case pltItmArrElemLst:pRet = new hCelementList(h);  break;	//14	COND2PAYELIST
//	case pltCollMemList : pRet = new hCmemberList(h);	break;
	case pltGroupItemList:pRet = new hCgroupItemList(h);break;// coll,itmArr,File
//	pltRecMemList,		//16	COND2PAYRECLIST

//	case pltWrtAoneList:										//17    COND2PAYWAOLIST
//    case pltUnitList:											//18    COND2PAYUNITLIST
	//pltRefreshList:  pRet = new hCreferenceList(h);break;		//19    COND2PAYREFRESHLIST
	case pltReferenceList:pRet = new hCreferenceList(h);break;	//19    COND2PAYREFLIST
	case pltReference:    pRet = new hCreference(h);    break;  //20	COND2PAYREFERENCE
	case pltExpression:   pRet = new hCexprPayload(h);  break;  //21	  
	case pltMinMax:       pRet = new hCMinMaxList(h);   break;  //22
	case pltIntWhich:     pRet = new hCddlIntWhich(h);  break;
	case pltGridMemberList:pRet= new hCGridGroupList(h);break;  //24

	case pltUnknown	  :
	case pltLastLdType:
	default           :	
		//cerr << "ERROR: newPayload was handed a Type it did not recognize."<<endl;
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: newPayload was handed a Type it did not recognize.\n");
		pRet = (void*)0L; break;
	}// end switch

	return pRet;
}

/*************************************************************************************************
 *
 *   $History: ddbPayload.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
