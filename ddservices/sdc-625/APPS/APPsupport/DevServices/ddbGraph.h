/*************************************************************************************************
 *
 * $Workfile: ddbGraph.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbGraph.h".
 */

#ifndef _DDB_GRAPH_H
#define _DDB_GRAPH_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"

#include "ddbGrpItmDesc.h"

/*
	graphAttrLabel			// in base class	use aCattrString 
	graphAttrHelp,			// in base class	use aCattrString
	graphAttrValidity,		// in base class	use aCattrCondLong
	graphAttrHeight,							use aCattrCondLong	- as enum
	graphAttrWidth,								use aCattrCondLong	- as enum
	graphAttrXAxis,								use	aCattrCondReference
	graphAttrMembers,		// required			use aCattrMemberList
*/

class hCgraph : public hCitemBase 
{
	hCattrGroupItems* pMemberAttr;
	hCattrGrafixSize* pGraphHgt;
	hCattrGrafixSize* pGraphWid;
	hCattrAxisRef*    pXaxis;	
	hCattrExpr*       pCycleTime;	//aCattrCondExpr stevev 20apr07 - new function

public:
	hCgraph(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCgraph(hCgraph*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCgraph()  {/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 

	CValueVarient getAttrValue(unsigned attrType, int which = 0);

	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual );

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(GRAPH_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(GRAPH_LABEL, l); };

	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);
	bool       isInGroup (UINT32 indexValue);

	RETURNCODE Read(wstring &value){/* graph items are NOT read */ return FAILURE;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Graph Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Graph ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};
	/* added stevev - 20apr07 */
	RETURNCODE getCycleTime(CValueVarient & cv); // Returns 1/frequency of data updates

	/*Vibhor 071004: Start of Code*/
	RETURNCODE getWaveformList(hCgroupItemList& memberList); //returns a resolved list of members
	
	RETURNCODE getHeight(unsigned & height); // gives Graph's height

	RETURNCODE getWidth(unsigned & width);	// gives Graph's width

	hCaxis * getXAxis(); //returns ptr 2 a hCaxis object to which graph is referring 
	
	/*Vibhor 071004: End of Code*/
	RETURNCODE getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs);//5jul06 sjv

	hCattrGroupItems* getMemberList(void){ 
	#ifdef TOKENIZER
		return pMemberAttr;
	#else
		return NULL;
	#endif
		};

	void  fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	void  fillContainList(ddbItemList_t&  iContain); // stevev 27sep06
	int   notification(hCmsgList& msgList,bool isValidity);//stevev 05oct06

};


#endif	// _DDB_GRAPH_H
