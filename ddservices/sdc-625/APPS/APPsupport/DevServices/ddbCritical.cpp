/*************************************************************************************************
 *
 * $Workfile: ddbCritical.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		describes the hCcritical class that is responsible for dependencies on the device level
 *		Used to encapsulate the changing complexity of this responsibility
 *
 *		20jan05	sjv	started creation
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */

#include "ddbCritical.h"
#include "logging.h"




hCcritical::hCcritical()
{
}

hCcritical::~hCcritical()
{
	itemListPtrList_it iLoL;

	// Clear out the critical lists...
	for (iLoL = leveledCritical.begin(); iLoL != leveledCritical.end(); ++ iLoL)
	{    // ptr2a ptr2a hHitemList
		if ( (*iLoL) != NULL ) { delete (*iLoL);(*iLoL)	= NULL; }
	}
	leveledCritical.clear();

}


void hCcritical::fillCritical(aCdevice* pADev)
{
	// instantiate critical item list from the dd
	for (AnIdList_t::iterator z = pADev->CritItemList.begin();
			z < pADev->CritItemList.end();		z++)
	{
		DDcriticalItemList.push_back(*z);
	}
}

bool hCcritical::isItemCritical(itemID_t  itm)
{
	ddbItemLst_it z;

	for(z = CriticalItemList.begin(); z < CriticalItemList.end(); z++)
	{// ptr2a ptr2a hCitemBase
		if ( (*z)->getID() == itm )
			return true;
	}
	return false;
}

/*TEMP*/ int dbg = 0;
/* stevev 13sep06 - serious changes to how the items are inserted.
 *		The old way of doing this insertion made the list very dependent on the item order in the dd.
 *		The new way will only have a single copy of an item in the leveled list.
 *		To do this I will use the following rules (in the order given):
 *		a) If there is duplicate in a higher or equally numbered depth, we will not insert.
 *		b) After Insertion, if there is a duplicate in a lower numbered depth, we will remove it.
 *		These two will replace the single rule:: 
 *				If there is duplicate in the requested(equally numbered) depth, we will not insert.
 */

// depth less than zero::> put at bottom level & return that depth (depth is both input & return value)
void  hCcritical::insertCriticalItem(hCitemBase* pB, int& depth)
{
	ulong target = pB->getID();// for debugging purposes only
								// optimized out in release mode
	if ( depth < 0 )
	{
		depth = leveledCritical.size();
	}
	else
	if ( depth > MAX_VALIDITY_DEPTH )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: insertCriticalItem: depth= %d is greater than maximum allowed\n",
			depth);
		return;
	}
	if ( pB == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: insertCriticalItem: NULL item encountered.\n");
		return;
	}// else process it
	if (pB->getIType() != iT_Variable)
	{
	/*stevev 12jul06 - this is a condition where a menu is on a menu so the first menu is
						dependent on the second.  With the expanded dependency system, this
						is a normal condition - we will stop logging it.
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: attempt to register %s (%s) as a non-Variable critical item.\n",
			pB->getName().c_str(),pB->getTypeName());
	 **********/
		return;
	}

	hHitemList* pIL;

	if ( depth > (int)(leveledCritical.size()) )
	{
		LOGIT(CERR_LOG|CLOG_LOG,"_WARN: insertCriticalItem: depth= %d -skipped some levels.\n",depth);
	}
	for ( int y = leveledCritical.size(); y <= depth; y++)
	{// insert skipped empties
		pIL = new hHitemList;
		leveledCritical.push_back(pIL);
	}
	if ( ! existsGreaterEqual(depth, pB) )	// 13sep06 rule (a)
	{
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"[ Adding 0x%04x at level %d] ",pB->getID(),depth);
#endif
		leveledCritical[depth]->push_back(pB);// pre 13sep06:was::> insertUnique(pB);
		murderLessor(depth, pB);			// 13sep06 rule (b)
	}
	return;
}
/* stevev 13sep06 - end serious changes **/

/* stevev 13sep06 - support for new insertion rules*/

// check items @ >= depth

bool hCcritical::existsGreaterEqual(int depth, hCitemBase* pIB)
{
	itemListPtrList_it ipItemLst = leveledCritical.begin();
	ddbItemList_t**    ppItemList;
	ddbItemLst_it      ipItmBase;
	hCitemBase**       ppItmBase;

	if ( pIB == NULL || depth >= ((int)leveledCritical.size()) || depth < 0 )	return false;
	
	ulong target = pIB->getID();

	for (advance(ipItemLst,depth);//ipItemLst  = leveledCritical.at(depth); 
		 ipItemLst != leveledCritical.end();  ++ipItemLst )
	{// isa ptr 2a ptr 2a hHitemList (which isa ddbItemList_t [of itembase pointers])
		ppItemList = (ddbItemList_t**) &(*ipItemLst);
		//if ( ipItemLst == NULL || (*ipItemLst) == NULL ) continue; // an error
		if ( ppItemList == NULL || (*ppItemList) == NULL ) continue; // an error
		//for ( ipItmBase = (*ipItemLst)->begin(); ipItmBase != (*ipItemLst)->end(); ++ ipItmBase)
		for ( ipItmBase = (*ppItemList)->begin(); ipItmBase != (*ppItemList)->end(); ++ ipItmBase)
		{// isa ptr 2a ptr 2a hCitemBase
			ppItmBase = (hCitemBase**)&(*ipItmBase);
			//if (ipItmBase == NULL || (*ipItmBase) == NULL ) continue; // error
			if (ppItmBase == NULL || (*ppItmBase) == NULL ) continue; // error
			if ( (*ppItmBase)->getID() == target )
			{
				return true;// doesn't need to be but one..
			}
		}
	}
	return false;
}
//remove those @ <  depth
void hCcritical::murderLessor(int depth, hCitemBase* pIB)      
{// there will only be one...

	itemListPtrList_it ipItemLst, ipItemEnd = leveledCritical.begin(); // stevev 23feb07 eas:: end();
	ddbItemList_t**    ppItemList;
	ddbItemLst_it      ipItmBase;
	hCitemBase**       ppItmBase;

	// stevev 20feb07 - made equal levels exit (why did this ever work??)
	//if ( pIB == NULL || depth >= leveledCritical.size() || depth < 0 )	return;
	// stevev 21apr08 - enabled insertion at lowest level to go on and delete the above
	//					eg size =2, depth = 1, did not work with this::\/ abort test.
	//if ( pIB == NULL || (depth+1) >= leveledCritical.size() || depth < 0 )	return;
	// 21apr08 - note that this returns it to the state it was before the 20feb07 change
	if ( pIB == NULL || (depth+1) > ((int)leveledCritical.size()) || depth < 0 )	return;
	
	ulong target = pIB->getID();
	advance(ipItemEnd,depth); 
	int c = 0;

	for (ipItemLst = leveledCritical.begin(); ipItemLst != ipItemEnd;  ++ipItemLst, c++ )
	{// isa ptr 2a ptr 2a hHitemList (which isa ddbItemList_t [of itembase pointers])
		ppItemList = (ddbItemList_t**) &(*ipItemLst);
		//if ( ipItemLst == NULL || (*ipItemLst) == NULL ) continue; // an error
		if ( ppItemList == NULL || (*ppItemList) == NULL ) continue; // an error
		//for ( ipItmBase = (*ipItemLst)->begin(); ipItmBase != (*ipItemLst)->end(); ++ ipItmBase)
		for ( ipItmBase = (*ppItemList)->begin(); ipItmBase != (*ppItemList)->end(); ++ ipItmBase)
		{// isa ptr 2a ptr 2a hCitemBase
			ppItmBase = (hCitemBase**)&(*ipItmBase);
			//if (ipItmBase == NULL || (*ipItmBase) == NULL ) continue; // error
			if (ppItmBase == NULL || (*ppItmBase) == NULL ) continue; // error
			if ( (*ipItmBase)->getID() == target )
			{
#ifdef _DEBUG
				LOGIT(CLOG_LOG,"[Erasing 0x%04x at level %d] ",target,c);
#endif
				(*ppItemList)->erase(ipItmBase);
				return ;// doesn't need to be but one..
			}
		}
	}
}

/*end stevev 13sep06 - support */

RETURNCODE hCcritical::consolidateLists(hCdeviceSrvc* pD)//   put temps into working and clear temps
{
	RETURNCODE rc = SUCCESS;
	itemListPtrList_it iLoL;
	ddbItemLst_it      iIt;
	ptr2hCitemBase_t   pIB;
	itemIDlist_t::iterator iIL;
	ulong u;
/*TEMP*/ dbg = 1;
	for (iLoL = leveledCritical.begin(); iLoL != leveledCritical.end(); ++ iLoL)
	{// ptr2a ptr2a hHitemList
		for (iIt = (*iLoL)->begin(); iIt != (*iLoL)->end(); ++ iIt )
		{// ptr2a ptr2a hCitemBase
			pIB = (ptr2hCitemBase_t)(*iIt);
			// stevev 23feb07 - filter all but the truly critical
			if ( pIB->IsReallyCritical() )
				CriticalItemList.insertUnique(pIB);
		}
		// original::(*iLoL)->clear();		// HOMZ - Resolve Memory Leaks;  Do not clear this - Delete this in destructor

		delete (*iLoL);  // << leveledCritical SOLUTION 1 >>
		(*iLoL) = NULL;  // so we don't do it again
	}
	leveledCritical.clear(); // will be deleted by the class

	// now add any DD items that are not already there
	if(dbg)  LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"From DD Binary-----------------------------------\n");
	for(iIL = DDcriticalItemList.begin(); iIL != DDcriticalItemList.end(); ++iIL)
	{// ptr2a ulong item id
		u = *iIL;
		pD->getItemBySymNumber(u,&pIB);

		//pIB = (ptr2hCitemBase_t)(iIL);
		CriticalItemList.insertUnique(pIB);
	}

	return rc;
}



RETURNCODE hCcritical::getCriticalIDs(itemIDlist_t*& r)
{
	ddbItemLst_it i;

	if ( CriticalIDList.size() <= 0 && CriticalItemList.size() > 0 )
	{
		CriticalIDList.reserve(CriticalItemList.size());	// was just checked for zero size
		for ( i = CriticalItemList.begin(); i != CriticalItemList.end(); ++i)
		{
			CriticalIDList.push_back((*i)->getID());
		}
	}

	r = &CriticalIDList;
	return SUCCESS;
}
RETURNCODE hCcritical::getCriticalPtrList(ddbItemList_t*& r)
{
	 r = (ddbItemList_t*)(&CriticalItemList);
	 return SUCCESS;
}

/***********************************************************************************************
 *  helpers
 ***********************************************************************************************
 */

RETURNCODE hHitemList::insertUnique(ptr2hCitemBase_t pIB) // unique by symbol number
{
	if ( pIB == NULL )		return APP_PROGRAMMER_ERROR;
	
	ulong target = pIB->getID();

	for (ddbItemLst_it i = begin(); i != end(); ++i )
	{
		if ( (*i)->getID() == target )
		{// we're done
/*TEMP*/ if(dbg) LOGIT(CLOG_LOG,"Exists 0x%04x\n",pIB->getID());
			return SUCCESS;
		}
	}
	// if we get here then it will be unique
/*TEMP*/ if(dbg) LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Insert 0x%04x\n",pIB->getID());
	push_back(pIB);
	return SUCCESS;
}

void hHitemList::dumpList(void)
{
	ddbItemLst_it i;
	ptr2hCitemBase_t   pIB;
	LOGIT(CLOG_LOG,"------ Leveled Critical Parameters ------\n");
	if ( size() <= 0 )
		LOGIT(CLOG_LOG,"------ THERE ARE NO CRITICAL PARAMETERS -\n");
	for ( i = begin(); i != end(); ++i)
	{
		pIB = (ptr2hCitemBase_t) (*i);
		// HOMZ - port to 2003, VS7
		// error C228: left of '.c_str' must have class/struct/union type
		//LOGIT(CLOG_LOG,"0x%04x  %s\n", pIB->getID(), pIB->getName().c_str());
		string theName = pIB->getName();
		LOGIT(CLOG_LOG,"0x%04x  %s\n", pIB->getID(), theName.c_str());
	}
}

