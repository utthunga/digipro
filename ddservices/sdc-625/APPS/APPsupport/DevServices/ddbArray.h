/**********************************************************************************************
 *
 * $Workfile: ddbArray.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbArray.h"
 */

#ifndef _DDB_ARRAY_H
#define _DDB_ARRAY_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbVar.h"

/*
arrayAttrLabel			// base class,	use aCattrString 
arrayAttrHelp,			// base class	use aCattrString
arrayAttrValidity,		// base class	use aCattrCondLong
arrayAttrType,			// required 	use aCattrCondReference
arrayAttrElementCnt,	// required 	use aCattrCondLong
*/


class hCarray : public hCitemBase 
{
	hCreference  typedefRef;
	hCitemBase*  pTypeDef;  // cannot be conditional so just resolve it once
	hCconditional<hCddlLong> condElemCnt;// holds the value(s) 

	ddbItemList_t activeIL;// the actual data - vector of itembase ptrs

public:
	hCarray(DevInfcHandle_t h, aCitemBase* paItemBase);
	/* from-typedef constructor(replicator)*/
	hCarray(hCarray*    pSrc,  hCarray*    pVal, itemIdentity_t newID);
	RETURNCODE destroy(void);
	virtual ~hCarray() {destroy();};
	            // activeIL are pointers to items who are destroyed from their list in device 
	
	/* stevev 22jan09 */
	RETURNCODE setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue);
	RETURNCODE addCmdDesc(hCcommandDescriptor& rdD, bool isRead);

	hCattrBase*    newHCattr(aCattrBase* pACattr)  // builder 
	{LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: array's newHCattr() was called.\n");return NULL;};
	CValueVarient  getAttrValue(unsigned attrType, int which = 0);

	void initTypedef(hCarray* pVal);// get the typedef into the Var
	
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )// for symmetry only
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(ARRAY_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(ARRAY_LABEL, l); };

	DATA_QUALITY_T Read(wstring &value){/* TODO: read the succker*/ return DA_NOT_VALID;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Array Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Array ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

	int		   getSize(){return activeIL.size();};
	int        getElemCnt() {return ((int) getAttrValue(arrayAttrElementCnt,0) );};
	
	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);
	RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes);
	RETURNCODE getByIndex(UINT32 indexValue,hCgroupItemDescriptor** ppGID,bool suppress=true);
	
	hCitemBase* operator[](int idx0);
//	bool       isInGroup (UINT32 iV) {  return (iV >= 0 && iV < getSize()); };

	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	bool       isInGroup (UINT32 iV) {  return (iV >= 0 && iV < (UINT32)getSize()); };

	itemType_t getTypedefType(void);

	RETURNCODE getList(ddbItemList_t& r2ItemLst );// returns 'em wether valid or not

	void MakeStale(void); // virtual in itembase that sets all contents stale

	/* let it fall through to itembase ::- nothing is conditional in this item
	* void  fillCondDepends(ddbItemList_t&  iDependOn)
	*/
	hCitemBase*  getTypePtr(void) { return pTypeDef; };// needed for complex type comparison
};

#endif	// _DDB_ARRAY_H
