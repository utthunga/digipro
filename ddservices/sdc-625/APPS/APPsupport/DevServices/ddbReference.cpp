/**********************************************************************************************
 *
 * $Workfile: ddbReference.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the reference class implementation
 * 8/07/2	sjv	created from reference.cpp
 */


#if defined(__GNUC__)
#include <cmath>
#endif // __GNUC__

//#include "dd_load.h"
//---#include "ddbConditional.h"
#include "ddbItemBase.h"
#include "ddbReferenceNexpression.h"
//#include "expression.h"
#include "ddbCollAndArr.h"
#include "ddbList.h"
#include "ddbArray.h"
//#include "parseBase.h"
//#include "tags_sa.h"
#include "ddbAttributes.h"
#include "ddbVar.h"
#include "ddbGrpItmDesc.h"
#include "logging.h"

#include "ddbChart.h"
#include "ddbGraph.h"
#include "ddbSource.h"
#include "ddbAxis.h"
#include "ddbBlob.h"

#include "Dictionary.h"


#ifdef _DEBUG
  #include <assert.h>

  #ifdef X_DEBUG
    #include <map>
    #define DFLTCON 1
    #define COPYCON 2
    unsigned maxSizeElem = 0;
    map <void*,int> exElemMap;
    void UNREGMEM(void* p)
    {
    	map <void*,int>::iterator miT;
    	miT = exElemMap.find( p );
    	if ( miT == exElemMap.end() )
    	{
    		LOGIT(CLOG_LOG,"EXELEM: Unreg With No Reg.\n 0x%p\n",p);
    	}
    	else // found
    	{
    		exElemMap.erase(miT);
    	}
    }
    void REGISMEM(void* p, int src)
    {
    	exElemMap[ p ]=src;
    	if (exElemMap.size() > maxSizeElem )
    		maxSizeElem = exElemMap.size();
    }
  #else
    #define UNREGMEM( a )
    #define REGISMEM( b, c )
  #endif 

  #undef DBG_EXECUTION /* define to show a bunch */
  //#define DBG_EXECUTION 
  #undef DBG_REFTRAIL  /* define to clog reference resolutions */
  //#define DBG_REFTRAIL

#else /* release */
  #define assert(_Expression)     ((void)0) /* match the release version in assert.h */
#endif

#define LBLTRACE	false /*turn OFF label messages - set true to turn 'em on*/

/* 09jan09 stevev - for better error messages */
#define IS_DOT   1  /* collection-dot-member */
#define IS_BRACK 2  /* array-bracket-index-bracket */
#define IS_NONE  0  /* variable */


extern char exprElemStrings[EXPELEMSTRCNT][EXPELEMSTRMAXLEN] ;
extern char refTypeStrings[REFSTRCOUNT]    [REFMAXLEN];

extern CDictionary *dictionary;		// in devmgr

#define USE_ORIGINAL_EXPRESSION 1

void hCreference::onnew(void)
{
	rtype = 0; // item_id
	isRef = 0;
	id    = 0; // an illegal id
	type  = 0; // item_id - not really
	subindex=0;
	iName = 0;
	exprType = eT_Unknown;
	pExpr = NULL;
	pRef  = NULL;
	grpTrail.clear(); 
	myCollectionPtr = NULL;
	myMemberIndex   = -1;
}

hCreference::hCreference(DevInfcHandle_t h) : hCobject(h)
{
	onnew();
};
	
hCreference::hCreference(const hCreference& cr) : hCobject(cr.devHndl()) 
{ 
	onnew();		// this is newly created, null 'em
	operator=(cr);
};

hCreference::~hCreference() 
{
	// designed for object destructing this one to call
	destroy();
	//if (pExpr != NULL ) { delete pExpr; pExpr = NULL; }
	//if ( pRef != NULL ) { delete pRef;  pRef  = NULL; }
};

#ifdef _DEBUG
#define NOTEMPTY(a) (a!=NULL && a!=(void*)0xcdcdcdcd && a!=(void*)0xdddddddd && \
					a!=(void*)0xbaadf00d)
#define ISEMPTY(b)  (b==NULL || b==(void*)0xcdcdcdcd || b==(void*)0xdddddddd || \
					b==(void*)0xbaadf00d)
#else
#define NOTEMPTY(a) (a!=NULL)
#define ISEMPTY(b)  (b==NULL)
#endif
/*--------------------- reference part using expression -----*/
bool hCreference:: isEmpty(void)
{  
	if (pExpr != NULL && pExpr->expressionL.size() > 0){return false;} 
	if (pRef  != NULL && !(pRef->isEmpty()))  {return false;}
	if (id != 0 || (rtype != rT_NotA_RefType && rtype != 0)) { return false;}
	return true;
}

/*** Reference functions that use expression information ****/
/* stevev 20sep10 - insert uses a swap function that goes like this:
	_Ty _Tmp = _Left;
	_Left  = _Right;
	_Right = _Tmp;
this, of course, will usualy leave remnants around since it doesn't
do a clear() between the _tmp = _left and the _Left = _Right
***/

hCreference& hCreference::operator=(const hCreference& cr)
{	
	//NO  onnew();		// J.U. to null fields when they are created with default constructor
	//this is operator=;  an expression & ref exist then they have to be destroyed, not NULLed
	rtype      = cr.rtype;
	isRef      = cr.isRef;
	id         = cr.id;
	type       = cr.type;
		subindex   = cr.subindex;
	iName      = cr.iName;
	exprType   = cr.exprType;
	grpTrail   = cr.grpTrail;
	myCollectionPtr = cr.myCollectionPtr;
	myMemberIndex   = cr.myMemberIndex;
#ifdef _DEBUG
	bool gotnew= false;
#endif
/*** stevev 30sep11 - I'm going to simplify this ****************
if (pExpr != NULL && pExpr->expressionL.size() != 0)
{
	pExpr->clear();
	if ( pExpr->expressionL.size() != 0 )
		LOGIT(CLOG_LOG,"op= destination ref is not empty\n");
}
	if NOTEMPTY(cr.pExpr )
	{
		//if (pExpr == NULL) // does not exist
		if ISEMPTY(pExpr)
		{
			pExpr  = new hCexpression(cr.pExpr->devHndl());
			assert(pExpr->expressionL.size() == 0);
#ifdef _DEBUG
			gotnew= true;
#endif
		}// else overwrite
		assert(pExpr->expressionL.size() == 0);
		*pExpr = *cr.pExpr;
		assert(pExpr->expressionL.size() == cr.pExpr->expressionL.size());
	}
	else 
	{
		//if (pExpr != NULL) 
		RAZE(pExpr);
	}

	//if (cr.pRef != NULL)
	if NOTEMPTY(cr.pRef)
	{
		//if (pRef == NULL) // does not exist
		if ISEMPTY(pRef)
		{
			pRef  = new hCreference(cr.pRef->devHndl());
		}// else overwrite
		*pRef = *cr.pRef;
	}
	else 
	{
		//if (pRef != NULL) delete pRef;
		RAZE(pRef) ;
	}
**********************************************************/
	RAZE(pExpr);
	RAZE(pRef );
	if (cr.pExpr) // make this one look equal to the other
		pExpr = new hCexpression( *(cr.pExpr));
	if (cr.pRef ) // make this one look equal to the other
		pRef  = new hCreference( *(cr.pRef));
	return *this;
};	


/*** make this Reference a duplicate of the passed ****/
/*   assume that current contents are useless         */
void hCreference::duplicate(hCpayload* pPayLd, bool replicate, elementID_t thisElement)
{	
	if (pPayLd == NULL || ! pPayLd->validPayload(pltReference) )// nothing to do 
	{
		LOGIF(LOGP_NOT_TOK)
					(CERR_LOG|CLOG_LOG,"ERROR: reference's duplicate with a bad parameter.\n");
		return;
	}
	hCreference* pR = (hCreference*)pPayLd;

	rtype      = pR->rtype;
	isRef      = pR->isRef;
	id         = pR->id;
	type       = pR->type;
		subindex   = pR->subindex;
	iName      = pR->iName;
	exprType   = pR->exprType;
	grpTrail   = pR->grpTrail;
	myCollectionPtr = pR->myCollectionPtr;
	myMemberIndex   = pR->myMemberIndex;

	if ( ! isRef || ( isRef && id != 0) )
	{// is a direct reference---------------
		if (replicate)
		{// don't copy, replicate
			hCitemBase * pItem = NULL;
			RETURNCODE
			rc = devPtr()->getItemBySymNumber(id, &pItem);
			if ( rc == SUCCESS && pItem != NULL )
			{
				itemType_t iType = pItem->getIType();
				if (iType == iT_Variable   ||
					iType == iT_ItemArray  ||
					iType == iT_Collection ||
					iType == iT_Array      ||
					iType == iT_List  )
					
				{// replicate and return:         typedef, valueCopy, replicate
					// stevev 19oct09 ; replicate item AND values for all types
					// stevev 25jan07 ; changed to below from
					//				   pItem = devPtr()->newPsuedoItem(pItem, NULL,      true);
					pItem = devPtr()->newPsuedoItem(pItem, pItem,
														sif_registerANDpreserve, thisElement);
					if (pItem)
						id = pItem->getID();
					return; // tha, tha, that's all folks
				}
				// else fall through to duplicate
			}
		}
	}
	// else not direct or direct without replication


	if NOTEMPTY(pR->pExpr )
	{
		pExpr = new hCexpression(devHndl());
		pExpr->setDuplicate(*(pR->pExpr));
	}
	else 
	{
		pExpr = NULL;
	}

	if NOTEMPTY(pR->pRef)
	{
		pRef = new hCreference(devHndl());
		pRef->duplicate(pR->pRef, replicate, thisElement);
	}
	else 
	{
		pRef = NULL;
	}
	return;
};	

void  hCreference::setEqual(void* pAclass)
{
	aCreference* paRef = (aCreference*) pAclass;
	operator=(*paRef);
}


void hCreference::clear(void) 
{	rtype      = 0;
	isRef      = 0;
	id         = 0;
	type       = 0L;
		subindex   = 0;
	iName      = 0;
	exprType   = eT_Unknown;
	myCollectionPtr = NULL;
	myMemberIndex   = -1;

	grpTrail.clear();
	RAZE(pExpr);//pExpr      = NULL; // assume others have handled destruction
	RAZE(pRef); //pRef       = NULL; // assume others have handled destruction
};

hCreference& hCreference::operator=(const aCreference& cr)
{	rtype      = cr.rtype;
	isRef      = cr.isRef;
	id         = cr.id;
	type       = cr.type;
		subindex   = cr.subindex;
	iName      = cr.iName;
	exprType   = cr.exprType;

	if ( isRef || (rtype == rT_Image_id)     || (rtype == rT_Separator) || 
		          (rtype == rT_Constant)  || (rtype == rT_Row_Break) )
	{
		if (cr.pExpr != NULL)
		{
			if (pExpr == NULL) // does not exist
			{
				pExpr  = new hCexpression(devHndl());
			}// else overwrite
			assert(pExpr->expressionL.size() == 0);
			*pExpr = *cr.pExpr;
			assert(pExpr->expressionL.size() == cr.pExpr->expressionL.size());
		}
		else 
		{
			RAZE(pExpr);
		}

		if (cr.pRef != NULL)
		{
			if (pRef == NULL) // does not exist
			{
				pRef  = new hCreference(devHndl());
			}// else overwrite
			*pRef = *cr.pRef;
		}
		else 
		{
			RAZE(pRef);
		}
	}
	else // nota reference
	{
#ifdef _DEBUG
		if ( (cr.pRef!=NULL) ^ (cr.pExpr!=NULL) )
		{
			clog << "Reference with un balanced construction.";
			if (cr.pRef==NULL)
			{
				clog<<"(no Reference Pointer) Expression ";// expression may be munged
			}
			else
			{
				clog<<"(no Expression Pointer) Reference ";
			}
			clog<<"DISCARDED"<<endl;
		}
	// its been re-munged was:	if ( id <= 0 )
		if ( id == 0 && rtype.getType() < MAX_REFTYPE_ENUM)
		{
			clog <<"Reference is NOT a ref but has an invalid id value.(0)"<<endl;
		}
		else
		if ( id < 0 )
		{
			clog <<"Reference is NOT a ref but has a negative id value.(warning) 0x"
				<<hex << id <<dec<<endl;
		}
#endif
		pExpr    = NULL;
		pRef     = NULL;
	}
	if ((rtype != rT_Image_id)    && (rtype != rT_Separator) && 
		(rtype != rT_Constant) && (rtype != rT_Row_Break)   )
	{
		if ( (pRef!=NULL) ^ (pExpr!=NULL) ) 
		{
			if ( (cr.pRef!=NULL) ^ (cr.pExpr!=NULL) )
			{
				clog << "aReference with un balanced construction.-Src Error "<<endl;
			}
			else
			{
				LOGIT(CERR_LOG,"Reference with un balanced construction.\n");
			}
		}
	}
#ifdef _DEBUG
if ( cr.pExpr != NULL && (rtype == rT_via_Collect || rtype == rT_via_File))
{
	if ( cr.pExpr->expressionL.size() != 1 || pExpr->expressionL.size() != 1 )
	{
		LOGIT(CLOG_LOG,"A %s reference with %d expression elements.\n",
			rtype.getTypeStr(),cr.pExpr->expressionL.size());
	}
}

#endif
	return *this;
};	

bool hCreference::operator==(const hCreference& ri)
{
	if ( rtype != ((hCreference)ri).rtype.getType() )	return false;// cast is from const 
	if ( isRef != ri.isRef ) return false;
	if ( id    != ri.id    ) return false;
	if ( type.getType()  != ((hCreference)ri).type.getType()  ) return false;
	if ( subindex != ri.subindex ) return false;
	if ( iName    != ri.iName )    return false;
	if ( exprType != ri.exprType ) return false;
	if ( myCollectionPtr != ri.myCollectionPtr ) return false;
	if ( myMemberIndex   != ri.myMemberIndex )   return false;

	if ( pExpr != NULL && ri.pExpr != NULL )// both not null
	{
		if ( ! ( (*pExpr) == (*(ri.pExpr))) ) // same as expr != expr
		{
			return false;
		}
	}
	else if (pExpr != ri.pExpr)// not both null
	{
		return false; // one null, one not
	}
	
	if ( pRef != NULL && ri.pRef != NULL )// both not null
	{
		if ( ! ( (*pRef) == (*ri.pRef)) ) // recurse, same as ref != ref
		{
			return false;
		}
	}
	else if (pRef != ri.pRef)// not both null
	{
		return false; // one null, one not
	}
	return true;
}

/* stevev 12aug09 - used to determine if the reference can ever point to other
   than the current item.  This would mean there is an array with a variable index
   in the reference somewhere.
 */
bool hCreference::isConst(void) // true for unindexed references
{
	bool retVal = false;
	referenceType_t rt;

	if ( ! isRef || ( isRef && id != 0) )
	{
		return true; // a straight id
	}// else, do other possibilities

	rt = getRefType();
	switch (rt) 
	{
	case rT_Item_id:
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Block_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_Record_id:
	case rT_Array_id:
	case rT_VarList_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
	/* stevev 25oct04 eddl types */	
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{
			retVal = true; // a straight id	
		}
		break;
	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
	case rT_via_Chart:		// 40 - pMemberAttr to Source
	case rT_via_Graph:		//    - pMemberAttr to Waveform		
	case rT_via_Source:		//    - pMemberAttr to Variable
	case rT_via_Collect:		/* collection reference */
	case rT_via_File:			/* stevev 25oct04 eddl new VIA type */
	case rT_viaItemArray:		/* item array reference */
	case rT_via_Array:		
	case rT_via_List:
		{
			retVal = false; // handle all the else cases
			if (pExpr != NULL && pExpr->isConst())
			{
				if ( pRef != NULL && pRef->isConst() )
				{
					retVal = true;// both ref and expression are const
				}
			}
		}
		break;
	case rT_Separator:
	case rT_Row_Break:
	case rT_Constant:
	case rT_via_BitEnum:	// assume it can't reference something else
		{
			retVal = true;
		}
		break;	
	case rT_via_Blob:			/* stevev 14nov08 new item type (attr only)*/
	case rT_Image_id:			//assume it's conditional
	case rT_via_Attr:		//assume it's conditional
	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
	default:
		{
			retVal = false;
		}
		break;
	}// endswitch	


	return retVal;
}


RETURNCODE hCreference::dumpSelf(int indent, char* typeName )
{
	RETURNCODE rc = SUCCESS;

	if ( ! isRef )
	{
		if ( id )
		{
			// may need an itemID.dumpSelf()
			//COUTSPACE 
			//cout << "Item   Id: " << setw(5) << id   << "   subidx[" << subindex << "]" ;
			LOGIT(COUT_LOG,"%sItem   Id: %5d (0x%04x)   subidx[ %d ]",
															  Space( indent ),id,id,subindex);
			// may need a itemType.dumpSelf()
			////cout << "     Item Type: " << type ;
			int flg = 0;
			if ( rtype.getType() >= 0 && rtype.getType() < MAX_REFTYPE_ENUM )
			{
				LOGIT(COUT_LOG,"   RefType: %s",rtype.getTypeStr()) ;
				flg = 1;
			}
			if ( type.getType() >= 0 && type.getType() < MAX_ITYPE )
			{
				LOGIT(COUT_LOG,"   Item Type: %s", type.getTypeStr());
				flg |= 2;
			}
			if (flg == 0 )
			{
				LOGIT(COUT_LOG,"   NO Valid Type  " );
			}
		}
		else
		{
			LOGIT(COUT_LOG,"%sNone",Space( indent ));	// was  COUTSPACE << "None" ;
		}
		LOGIT(COUT_LOG,"\n");


		if ( ( pExpr != NULL || pRef != NULL ) && // use the operator== for the next line
			!(rtype == rT_Constant || rtype == rT_Separator || rtype == rT_Row_Break)   )
		{	
			LOGIT(COUT_LOG,"ddbERROR:  Not a Ref but we have an ");
			
			if (pExpr != NULL )
			{
				LOGIT(COUT_LOG,"Expression ");
				//COUTSPACE << "   ---- NOT A REF but we have an Expression " << endl;;
				//COUTSPACE << "   ***>";
				LOGIT(COUT_LOG,"   ---- NOT A REF but we have an Expression.\n   ***>");
				pExpr->dumpSelf(indent+3);
			}
			if (pRef != NULL)
			{
				if (pExpr != NULL)
				{
					LOGIT(COUT_LOG,"and ");
				}
				LOGIT(COUT_LOG,"a Reference.");
				//COUTSPACE << "   ---- NOT A REF but we have a Reference." << endl;;
				//COUTSPACE << "   ***>";
				LOGIT(COUT_LOG,"   ---- NOT A REF but we have a Reference.\n   ***>");
				pRef->dumpSelf(indent+3);
			}
			LOGIT(COUT_LOG,"\n");
		}
	}
	else
	{// is a reference
		// cout <<
		//COUTSPACE << "Reference:" << endl;
		LOGIT(COUT_LOG,"%sReference:  %s\n",Space( indent ), (typeName)?typeName:"");
		// may need an itemID.dumpSelf()
		//COUTSPACE << "   RefItem   Id: " << id   << " RefSubidx[" << subindex << "]" ;
		LOGIT(COUT_LOG,"%s   RefItem   Id: %d RefSubidx[%d]" ,Space( indent ), id,subindex);
		// may need a itemType.dumpSelf()...built into itemType now...
		int flg = 0;
		if ( rtype.getType() >= 0 && rtype.getType() < MAX_REFTYPE_ENUM )
		{
			//cout << "   RefType: " << rtype ;
			LOGIT(COUT_LOG,"   RefType: %s",rtype.getTypeStr());
			flg = 1;
		}
		if ( type.getType() >= 0 && type.getType() < MAX_ITYPE )
		{
			char* pStr; pStr = type.getTypeStr();
			//LOGIT(COUT_LOG,"   RefItem Type: %s", type.getTypeStr());
			LOGIT(COUT_LOG,"   RefItem Type: %s", pStr);
			flg |= 2;
		}
		if (flg == 0 )
		{
			//cout << "   NO Valid Type  " ;
			LOGIT(COUT_LOG,"   NO Valid Type  ");
		}
		//cout << endl;
		LOGIT(COUT_LOG,"\n");

		if ( pExpr != NULL )
		{
			//COUTSPACE << "   Ref Expression:" << endl;
			LOGIT(COUT_LOG,"%s   Ref Expression:\n",Space( indent ));
			rc = pExpr->dumpSelf(indent + 6);
		}
		if ( pRef != NULL )
		{
			//COUTSPACE << "   Ref Reference:" << endl;
			LOGIT(COUT_LOG,"%s   Ref Reference:\n" ,Space( indent ));
			//COUTSPACE << "      ";
			rc |= pRef->dumpSelf(indent + 6);
		}
	}

	return rc;
}


// resolve this reference (self) into an item id, then look up the ptr by symbol number
RETURNCODE hCreference::resolveID( hCitemBase*& r2pReturnedPtr, bool withStrings, 
								   bool suppress, int refMsg, bool isAtest      )
{
	RETURNCODE rc = SUCCESS;	
	itemID_t        itemNum = 0;
	hCitemBase*     pItem = NULL;
	//                                         added params (pass thru)   15mar11
	if ( ( rc = resolveID(itemNum, withStrings,suppress, refMsg, isAtest) ) == SUCCESS )
	{
		if (itemNum == 0 ) // a constant
		{
			r2pReturnedPtr = 0;
			rc = SUCCESS;
		}
		else
		if ( devPtr()->getItemBySymNumber(itemNum, &pItem) == SUCCESS )
		{
			r2pReturnedPtr = pItem;
			rc = SUCCESS;
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference could not get item number 0x%04x\n",
																					  itemNum);
			rc = APP_RESOLUTION_ERROR;
			r2pReturnedPtr = NULL;
		}
	}
	else
	if ( rc == APP_LIST_IS_EMPTY )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG, " WARN: reference to empty list is not resolvable.\n");
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: reference could not resolve the item number.\n");
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}

// returns an error code & value < 0 upon error 
//       or SUCCESS & zero itemID if constant,           returns the item id otherwise
RETURNCODE hCreference::resolveID( itemID_t&   returnedID, bool withStrings, 
								   bool suppress, int refMsg, bool isAtest  )
{
	RETURNCODE rc = FAILURE;

	referenceType_t rt;
	itemID_t     retVal = 0;
	returnedID   = -1; // error return value

	try{ // Deepak 112504
#ifdef DBG_REFTRAIL
		clog<<"        :->->->->" << endl;
#endif
	if (rtype == rT_Separator || rtype == rT_Constant || rtype == rT_Row_Break)
	{// these can't be resolved
		returnedID = id;
		return SUCCESS;
	}
	if ( ! isRef || ( isRef && id != 0) )
	{
		returnedID = id;
		if (grpTrail.isEmpty())
		{
			grpTrail.itmID = id;

			rc = devPtr()->getItemBySymNumber(id, &(grpTrail.itmPtr));
			if ( rc == SUCCESS && grpTrail.itmPtr != NULL )
			{
				grpTrail.containerNumber = NOT_PART_OF_GROUP;
				if ( withStrings )
				{
					grpTrail.itmPtr->Label(grpTrail.lablStr);
					grpTrail.itmPtr->Help (grpTrail.helpStr);
				}
				// leave all the pointers empty
			}
			else
			{
				if ( ! suppress )
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: symbol 0x%04x failed to get "
																	  "an item pointer.\n",id);
				}
				grpTrail.itmID  = 0;
				grpTrail.itmPtr = NULL;
			}
		}// else - this won't change.. leave the original fetches	
#ifdef DBG_REFTRAIL
		clog<<"REFTRAIL: entry resolution "<< ((isRef)?"IS ddpREFERENCE":"IS DIRECT") 
			<<" with an ID = 0x"<< hex << ((int)id) << dec << endl
			<<"--------:" << endl;
#endif
		return SUCCESS;
	}
	// else do the function

	CValueVarient  indexValue;
	grpTrail.clear();

	rt = getRefType();
	switch (rt) 
	{
	case rT_Item_id:
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Block_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_Record_id:
	case rT_Array_id:
	case rT_VarList_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
	/* stevev 25oct04 eddl types */	
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{
			returnedID     = id;
			if (grpTrail.isEmpty())
			{
				grpTrail.itmID = id;
				rc = devPtr()->getItemBySymNumber(id, &(grpTrail.itmPtr));
				if ( rc == SUCCESS && grpTrail.itmPtr != NULL )
				{
					grpTrail.containerNumber = NOT_PART_OF_GROUP;
					if ( withStrings )
					{
						grpTrail.itmPtr->Label(grpTrail.lablStr);
						grpTrail.itmPtr->Help (grpTrail.helpStr);// leave the pointers empty
					}
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: symbol 0x%04x failed to get an ID'd"
																		   " item pointer.\n");
					grpTrail.itmID  = 0;
					grpTrail.itmPtr = NULL;
				}
			}// else - this won't change.. leave the original fetches	
			
#ifdef DBG_REFTRAIL
		clog<<"REFTRAIL: reference type "<< refTypeStrings[rt] 
			<<" with an ID = 0x"<< hex << ((int)id) << dec << endl;
#endif		
		}
		break;
	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
	case rT_via_Chart:		// 40 - pMemberAttr to Source
	case rT_via_Graph:		//    - pMemberAttr to Waveform		
	case rT_via_Source:		//    - pMemberAttr to Variable
	case rT_via_Collect:		/* collection reference */
	case rT_via_File:			/* stevev 25oct04 eddl new VIA type */	
	case rT_via_Blob:			/* stevev 14nov08 new item type (attr only)*/
		/* end stevev 14jul05 **** */
		{
			rc = resolveVia(returnedID,withStrings,false,IS_DOT,isAtest);
/*****
stevev 04aug06 - The commented-out code that was here was moved to resolveVia awhile back
- The code has now been deleted from this location - see earlier versions to see the code
******/
		}
		break;
	case rT_viaItemArray:	/* item array reference */
	case rT_via_Array:		
		{/* arrays will have different error strings */
			rc = resolveVia(returnedID,withStrings,suppress,IS_BRACK,isAtest);
		}
		break;

	case rT_via_List:
		/* stevev 14jul05 - moved all those with common code together */	
		{/* list must supress empty list 'error's' */
#ifdef _DEBUG
			rc = resolveVia(returnedID,withStrings,suppress,IS_BRACK,isAtest);
#else
			rc = resolveVia(returnedID,withStrings,true);
#endif
		}
		break;

	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
		{
			clog << "WARNING: unhandled reference resolution for BLOCK types." <<endl;
			returnedID = -1;
			rc = FAILURE;
		}
		break;
	/* stevev 25oct04 eddl constant references */
	case rT_Image_id:
	case rT_Separator:
	case rT_Row_Break:
	case rT_Constant:
		{// unique combination of bad id & good returncode
			returnedID = 0;
			rc = SUCCESS;
		}
		break;
	/* stevev 25oct04 eddl new VIA types */
		/* stevev 14jul - case rT_via_Array:  now returns a real psuedo item */
	case rT_via_BitEnum:
		{// this returns the SOURCE ID; 
		 // it's up to the caller to deal with the expression & lookup
			itemID_t      refVal = 0;

			if ( isRef && pExpr != NULL && pRef != NULL )
			{// we have a valid reference
#ifdef DBG_REFTRAIL/*------------------------------------------------------------------------*/
				if (pRef->isaRef())
				{
					clog<<  "------- reference in reference via_BitEnum isaRef w/ id = 0x"<<hex
						<< ((int)pRef->getID()) <<dec<<" resolving"<<endl;
				}
				clog<<"          with a pRef " << endl;	
#endif	/*-----------------------------------------------------------------------------------*/
				
#ifdef _DEBUG
				rc = resolveVia(returnedID,withStrings,suppress,IS_BRACK,isAtest);
#else
				rc = resolveVia(returnedID,withStrings,true);
#endif
# ifdef PRE03may11
				rc = pRef->resolveID(refVal,withStrings,suppress,refMsg,isAtest);//added params
				hCvarGroupTrail* pRefGrpTrail = pRef->getGroupTrailPtr();        //15mar11

				if ( rc == SUCCESS && 
					 pRefGrpTrail != NULL && 
					 pRefGrpTrail->itmPtr != NULL  &&
					 pRefGrpTrail->itmPtr->getIType() == iT_Variable &&
					 ((hCVar*)pRefGrpTrail->itmPtr)->VariableType() == vT_BitEnumerated )
				{// we have an item of the correct type
					
					returnedID = refVal; /* success */
				}
				else
				{// the reference could not be resolved
					LOGIT(CERR_LOG,"Ref Failure Arr 16. (reference is to wrong type) \n");
					rc = APP_PROGRAMMER_ERROR;
				}
#endif //PRE03may11
			}
			else if (isRef) //then there is no reference or expression 
			{// no index or no reference
				LOGIT(CERR_LOG,"Ref Failure Arr 17.( no index or reference )\n");
				rc = FAILURE;
			}
			else
			{// failure
				LOGIT(CERR_LOG,"Ref Failure Arr 18.\n");
				rc = FAILURE;
			}
			//else	 returnedID is set
		}
		break;

	case rT_via_Attr:		//	  - const or symbolID
		{	
			itemID_t      refVal = 0;
			bool  reference = ( isRef && (! ( isRef && id != 0)) && 
														    (pRef != NULL) );// stevev 07aug06
			
			if ( reference && pExpr != NULL )
			{
				indexValue  = pExpr->resolve(&(grpTrail.pExprTrail));// the attribute Number
				if ( indexValue.vType == CValueVarient::isIntConst )
				{	   
					rc = SUCCESS;	
					grpTrail.containerNumber = (UINT32)indexValue.vValue.iIntConst;
					#ifdef DBG_REFTRAIL
					clog<<"REFTRAIL: reference type "<< refTypeStrings[rt] << endl
						<<"          with an Expr Value = 0x"<< hex << (int)indexValue 
						<< dec << endl;
					#endif	
				}
				else 
				{
					LOGIT(CERR_LOG,"Ref Failure Attr 000.\n");
					grpTrail.containerNumber = NOT_PART_OF_GROUP;
					rc = FAILURE;	
				}				
			}
			else
			{// error
				LOGIT(CERR_LOG,"Ref Failure Attr 001.\n");
				rc = FAILURE;
			}

			if (rc == SUCCESS && reference)
			{// we have an index ('name') and a reference
				#ifdef DBG_REFTRAIL
				if (pRef->isaRef())
				{
					clog<<    "------- reference in reference via_attribute isaRef "
								"w/ id = 0x"<<hex << ((int)pRef->getID()) <<dec;
				}
				clog<<"          with a pRef (resolving)" << endl;	
				#endif	
				rc = pRef->resolveID(refVal,withStrings);
				hCvarGroupTrail* pRefGrpTrail = pRef->getGroupTrailPtr();

				if ( rc == SUCCESS  && pRefGrpTrail != NULL && pRefGrpTrail->itmPtr != NULL)
				{// we have an itemID of the Item we are targetting
					if (devPtr() == NULL)
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No device for dereferencing.\n");
						rc = FAILURE;
					}
					else
					{	
						/* changed to psuedo-Variable resolution 04aug06 */
						hCVar*  pVar = 
							pRefGrpTrail->itmPtr->getAttrPtr(grpTrail.containerNumber);
						if ( pVar == NULL ) // error return
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Invalid attribute reference "
							"to '%s', attribute number %d\n",
							pRefGrpTrail->itmPtr->getName().c_str(),grpTrail.containerNumber);
						}
						else
						{// success
							returnedID     = pVar->getID();
							if (grpTrail.isEmpty())
							{
								grpTrail.itmID = returnedID;
								grpTrail.itmPtr= pVar;
								if ( withStrings )
								{
									pVar->Label(grpTrail.lablStr);
									pVar->Help (grpTrail.helpStr);// leave the pointers empty
								}
							
							}
						}
						/* was - pre 04aug06 ::
						CValueVarient rV = 
						pRefGrpTrail->itmPtr->
									getAttrValue((varAttrType_t)grpTrail.containerNumber);
						if ( rV.vType == CValueVarient::isSymID )
						{// we have a return Value
							returnedID = (unsigned int) rV;// symbol-ID to int conversion
							rc = SUCCESS;
						}
						else
						{// everything else is a value/constant return
							returnedID = 0;
							rc = SUCCESS;	// a unique ZERO + SUCCESS indicates Conastant
						}
						*** end was  - pre 04aug06 **/
					}// endelse - have a device
				}
				else
				{// the reference could not be resolved with a trail
					LOGIT(CERR_LOG,"Ref Failure Attr 002.\n");
					rc = APP_PROGRAMMER_ERROR;
				}
			}
			else if (rc==SUCCESS) //then there is no reference
			{// no index or no reference
				LOGIT(CERR_LOG,"Ref Failure Attr 003.\n");
				rc = FAILURE;
			}
			else
			{// pointer or expression failure
				LOGIT(CERR_LOG,"Ref Failure Attr 004.\n");
				rc = FAILURE;
			}

			if ( rc != SUCCESS )
			{
				clog << "WARNING: unhandled reference resolution for ATTR types." <<endl;
				rc = -3;
			}
				
#ifdef DBG_REFTRAIL
			else
			{
				grpTrail.clogSelf();
			}
#endif
			//else	 retVal is set

		}
		break;
	default:
		{
			clog << "WARNING: unrecognized reference type." <<endl;
			rc = -5;
		}
		break;
	}// endswitch	
#ifdef DBG_REFTRAIL
		clog<<"--------: exit from resolution w/ id = 0x"<<hex<<returnedID<<dec << endl;
#endif
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"ResolveID Inside catch(...) \n");
	} // Deepak 112504

	return rc;
}

RETURNCODE hCreference::resolveVia(itemID_t& returnID, bool withStrings, bool suppress, 
																	  int refMsg, bool isAtest)
{
	RETURNCODE rc = SUCCESS;

	itemID_t       refVal = 0;
	hCreference    itemRef(devHndl());
	
	hCitemBase*    pItem = NULL;
	CValueVarient  indexValue;

	/* test for expression list corruption 5jul06 */
	if ( getRefType() == rT_via_Collect  && pExpr != NULL )
	{
		if ( pExpr->expressionL.size() != 1 )
		{
			LOGIT(CLOG_LOG,"Too many expressions for member-name.\n");
		}
	}
	/* end test 5jul06 */

	if ( isRef && pExpr != NULL && pRef != NULL ) /* pRef check added for collections */
	{
		indexValue = pExpr->resolve( &(grpTrail.pExprTrail), isAtest);// the 'member name'
		/* stevev 2jul08 - 10:09AM - Wally believes we need to restrict indexes to 16 bits.
		 *  I believe they should stay an 'unsigned integer' as in the spec.  On this machine
		 *  that is a 32 bit integer.  Force to 32 bits (or smaller).
		 * * * */
		if ( indexValue.vType == CValueVarient::isVeryLong  && 
			(indexValue.vIsValid || isAtest) )
		{			
			CValueVarient  intValue;
			intValue = (UINT32)indexValue;
			if ( ! (intValue == indexValue) )// truncation error
			{
				LOGIF(LOGP_NOT_TOK)
					(CERR_LOG,"ERROR: index value truncation error from long long to long.\n");
			}
			indexValue.clear();
			indexValue = (UINT32)intValue;// should convert to CValueVarient::isIntConst
		}
		/* * * end stevev 2jul08 * * */
		if ( indexValue.vType == CValueVarient::isIntConst  && 
			(indexValue.vIsValid || isAtest) )
		{	   
			rc = SUCCESS;
			grpTrail.containerNumber = (UINT32)indexValue.vValue.iIntConst;
			#ifdef DBG_REFTRAIL
			clog<<"REFTRAIL: reference type "<< refTypeStrings[getRefType()] << endl
				<<"          with an Index = 0x"<< hex << ((int)indexValue) << dec << endl;
			#endif	
		}
		else
		{
			grpTrail.containerNumber = NOT_PART_OF_GROUP;
			rc = FAILURE;
			if ( ! suppress )
			{
				if (! indexValue.vIsValid)
				{
// already have a warning	LOGIT(CERR_LOG,"Ref Failure Via 0x. (Index expression invalid)\n");
					if ( pExpr->lastError == APP_LIST_IS_EMPTY )
					{
						rc = APP_LIST_IS_EMPTY;
					}
				}
				else
				{
				LOGIT(CERR_LOG,"Ref Failure Via 00. (Index expression unresolved)\n");
				}
				#ifdef DBG_REFTRAIL
				clog<< "Ref Failure Via 00. (Index expression unresolved)"<<endl;
				#endif	
			}
		}
	}
	else
	{// error
		LOGIT(CERR_LOG,"Ref Failure Via 01.(NULL pointer ");
		if (pExpr == NULL) 
			LOGIT(CERR_LOG,"for Expression )\n");
		else 
			LOGIT(CERR_LOG,"for Reference )\n");
		rc = FAILURE;
	}
	
	if (rc == SUCCESS && pRef != NULL)
	{// we have an index ('element') and a reference
		#ifdef DBG_REFTRAIL
		if (pRef->isaRef())
		{
			clog<<    "------- reference in resolveVia isaRef w/ id = 0x"<<hex
			<< ((int)pRef->getID()) <<dec<<" resolving";
		}
		clog<<"          with a pRef " << endl;	
		#endif	

		rc = pRef->resolveID(refVal, withStrings, suppress,0,isAtest);

		hCvarGroupTrail* pRefGrpTrail = pRef->getGroupTrailPtr();				
		grpTrail.pSourceTrail         = new hCvarGroupTrail(*(pRefGrpTrail));
		hCgroupItemDescriptor*   pGID = NULL;

		if ( rc == SUCCESS && pRefGrpTrail != NULL && pRefGrpTrail->itmPtr != NULL)
		{// we have an item
			if (devPtr() == NULL)
			{
				LOGIF(LOGP_NOT_TOK)
						(CERR_LOG | CLOG_LOG,"ERROR: Via 02 No device to dereference with.\n");
				rc = FAILURE;
			}
			else
			{	// use the expression as index
				bool suppressInfo= true;
				if (devPtr()->getPolicy().isPartOfTok && ! suppress)
					suppressInfo= false;// else: leave it suppressed

				switch ( getRefType() )
				{
				case rT_viaItemArray: 		
				case rT_via_Collect:	//      collection reference  
				case rT_via_File:

				case rT_via_List:
				case rT_via_Array: 
				case rT_via_Chart:		// 40 - pMemberAttr to Source 
				case rT_via_Graph:		//    - pMemberAttr to Waveform	 	
				case rT_via_Source:		//    - pMemberAttr to Variable 			 
				case rT_via_Blob:		//    - pIdentityAttr  
					rc = (pRefGrpTrail->itmPtr)->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);
					break;
					/* use the itembase call 31dec08
					rc = ((hCgrpItmBasedClass*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
				case rT_via_List:			 
					rc = ((hClist*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
				case rT_via_Array: 
					rc = ((hCarray*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
				case rT_via_Chart:		// 40 - pMemberAttr to Source 
					rc = ((hCchart*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
				case rT_via_Graph:		//    - pMemberAttr to Waveform	 
					rc = ((hCgraph*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;	
				case rT_via_Source:		//    - pMemberAttr to Variable 
					rc = ((hCsource*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
				case rT_via_Blob:		//    - pIdentityAttr  
					rc = ((hCblob*)(pRefGrpTrail->itmPtr))->
								getByIndex(grpTrail.containerNumber,&pGID,suppressInfo);break;
***/
				case rT_via_BitEnum: /* stevev added 3may11*/
					{
						wstring value, bLabel, bHelp;

						/*
							TODO(LINUX_PORT, "The code in the else part only compiles with "
							"ENUM_ALLOC_DEBUG defined in ddbVar.h. But defining that causes "
							"a linker error due to 'gblDescList' variable being undefined."
							"That variable is defined in Windows-specific code.");
						*/
#if defined(__GNUC__)
						hCenumDesc bitDesc(devHndl());
#else // defined(__GNUC__)
						hCenumDesc bitDesc(devHndl() SRC_LOCALREF);
#endif // defined(__GNUC__)
						if (((hCVar*)(pRefGrpTrail->itmPtr))->VariableType() == 
																			  vT_BitEnumerated)
						{
							rc = ((hCBitEnum*)(pRefGrpTrail->itmPtr))->
												procureEnum(grpTrail.containerNumber,bitDesc);
							(pRefGrpTrail->itmPtr)->Label(bLabel);
							(pRefGrpTrail->itmPtr)->Help (bHelp);
						}
						else
						{
							rc = FAILURE;
						}
						if ( rc != SUCCESS )
						{
							if ( pGID ) RAZE(pGID);
						}
						else
						{							
							grpTrail.containerDesc = (wstring)bitDesc.descS;
							grpTrail.containerHelp = (wstring)bitDesc.helpS;
							grpTrail.lablStr       = bLabel;
							grpTrail.helpStr       = bHelp;
							returnID = pRefGrpTrail->itmID;
						}
					}
					break;
				default:
					LOGIF(LOGP_NOT_TOK)
						(CERR_LOG,"ERROR: Via with an unknown reference Type %s.\n",
																refTypeStrings[getRefType()]);
					rc = FAILURE;
					returnID = 0;
				}

				if (  rc == SUCCESS    &&   pGID != NULL )
				{// we have the indexed element of the array described in pGID							
					itemRef = pGID->getRef();	// looked-up member item
					/*      <ADD> stevev 020304 - PAR 5256 - we must have the description! */
					if (withStrings)
					{
						//grpTrail.containerDesc = pGID->getDesc().procureVal().s;
						grpTrail.containerDesc = pGID->getDesc();
						//grpTrail.containerDesc = pGID->getExDesc();
						grpTrail.containerHelp = pGID->getHelp().procureVal();
					}
					/*      end<ADD> stevev 020304 - PAR 5256                              */
					#ifdef DBG_REFTRAIL
					clog <<  "          get by index OK, GID ref is "<< 
									refTypeStrings[itemRef.getRefType()] << " resolving"<<endl;
					#endif	
					
					if ( (rc = itemRef.resolveID(refVal, withStrings, suppress,0,isAtest)) 
							== SUCCESS )// assumed DIRECT - ALWAYS
					{// we have the referenced itemID of this element
						grpTrail.itmID   = refVal;

						rc = devPtr()->getItemBySymNumber(refVal, &pItem);

						if ( rc == SUCCESS && pItem != NULL)
						{// successful acquasition of the item id
							grpTrail.itmPtr = pItem;
						}
						else
						{// could be a constant or failure
							grpTrail.itmPtr = NULL;
							LOGIT(CERR_LOG,"Ref Failure Via 03.\n");
							#ifdef DBG_REFTRAIL
							clog<< "Ref Failure Via 03."<<endl;
							#endif	
							rc = FAILURE;
						}
						grpTrail.pLookupTrail =
											new hCvarGroupTrail(*(itemRef.getGroupTrailPtr()));
						returnID = refVal;
						#ifdef DBG_REFTRAIL
						clog <<  "         set srcTrail ptr to GID.ref.groupTrailPtr." << endl;
						#endif							
					}
					else
					{	
						LOGIT(CERR_LOG,"Ref Failure Via 04.\n");
						#ifdef DBG_REFTRAIL
						clog << "Ref Failure Via 04."<<endl;
						#endif	
						rc = FAILURE;
					}
				}
				else
				if ( rc == DB_NO_LIST_ELEMENT )
				{
					if ( ! suppress )
					{
						LOGIT(CLOG_LOG,"Ref Failure at not in list. (%s(0x%04x) [0x%04x] )\n",
						pRefGrpTrail->itmPtr->getName().c_str(),pRefGrpTrail->itmPtr->getID(),
						grpTrail.containerNumber);					
					}
	// this needs to abort the DD
	// no abort 02oct14 - stevev - devPtr()->abortDD(rc,pRefGrpTrail->itmPtr);// if prefs, may return here
				}
				else
				if ( rc == SUCCESS && getRefType() == rT_via_BitEnum )
				{// this is probably just fine...at least as good as we can get it
					rc = SUCCESS;// placeholder
				}
				else
				{// indexed item aquasition failed
					LOGIT(CERR_LOG,"Ref Failure Via 05. (%s(0x%04x) [0x%04x] )\n",
							pRefGrpTrail->itmPtr->getName().c_str(),
							pRefGrpTrail->itmPtr->getID(),          grpTrail.containerNumber);
					
					
					rc = FAILURE;
				}
			}
		}
		else
		{// the reference could not be resolved
			if ( ! suppress )
			{				
				if (refMsg == IS_BRACK)
				{
					if ( indexValue.vType == CValueVarient::isIntConst )
					{
						LOGIT(CERR_LOG,"WARNING: Reference[%d (0x%04x)] could not resolve"
							         " the reference.\n",(int)indexValue,(int)indexValue);
					}
					else
					{
						LOGIT(CERR_LOG,"WARNING: Reference[expression] could not resolve"
							         " the reference.\n");
					}
					
				}
				else
				if (refMsg == IS_DOT)
				{
					if ( indexValue.vType == CValueVarient::isIntConst )
					{
						LOGIT(CERR_LOG,"WARNING: Reference.%d (member-id 0x%04x) could not"
							     " resolve the reference.\n",(int)indexValue,(int)indexValue);
					}
					else
					{
						LOGIT(CERR_LOG,"WARNING: Reference.Unknown_memberID could not"
							     " resolve the reference.\n");
					}
				}
				else
				{
					LOGIT(CERR_LOG,"WARNING: Reference could not resolve.\n");
				}
		//		LOGIT(CLOG_LOG,"WARNING: Reference Failure Via 06.\n");
				#ifdef DBG_REFTRAIL
				clog << "Ref Failure Via 06."<<endl;
				#endif	
			}
			rc = APP_PROGRAMMER_ERROR;
		}
		if (pGID != NULL)
		{
			delete pGID;
		}
	}
	else if (rc==SUCCESS) //then there is no reference
	{// no index or no reference
		LOGIT(CERR_LOG,"Ref Failure Via 07.\n");
		#ifdef DBG_REFTRAIL
		clog << "Ref Failure Via 07."<<endl;
		#endif	
		rc = FAILURE;
	}
	else
	{// expression failure (may be duplicate msg) - document error
		if ( ! suppress )
		{
			hCitemBase* pItm = NULL;
			rc = pRef->resolveID(pItm, withStrings, suppress,0,isAtest);
			if ( rc == SUCCESS && pItm != NULL )
			{// got the item for message names
				// stevev 14jan09 - suppress empty list errors.
				if (refMsg == IS_BRACK && pItm->getIType() != iT_List)
				{
				LOGIT(CERR_LOG|CLOG_LOG,"WARNING: Reference %s[expression] failed to get a "
								"valid value for the expression.\n",pItm->getName().c_str() );
				}
				else
				if (pItm->getIType() != iT_List)
				{
				LOGIT(CERR_LOG|CLOG_LOG,"WARNING: Reference %s.expression failed to get a "
								"valid value for the expression.\n",pItm->getName().c_str() );
				}
			}
			else
			{
				LOGIT(CERR_LOG,"Ref Failure Via 08.\n");
				#ifdef DBG_REFTRAIL
				clog << "Ref Failure Via 08."<<endl;
				#endif	
			}
		}
	// let the original error percolate out...	rc = FAILURE;
	}

//	if ( rc != SUCCESS )
//	{
//		DEBUGLOG(CLOG_LOG,"WARNING: unhandled reference resolution for VIA type."
//													"{debug only msg-possible duplicate}\n");
//		rc = -3;
//	}
	//else	 returnID is set
	itemRef.destroy();

	return rc;
}


void hCreference::addUniqueUInt(UIntList_t& intlist, UINT32 newInt)
{
	for (UIntList_t::iterator pI=intlist.begin();pI < intlist.end();pI++)
	{
		if (*pI == newInt)  return; // duplicate
	}
	intlist.push_back(newInt);// does not exist, add
}

/* referenced item ids for all expression values used with all reference'd source items */
/* bool retTypeDef is set for tok evaluatuion :: returns the typedef id instead of psuedo-variables    */
RETURNCODE hCreference::resolveAllIDs(UIntList_t& allPossibleIDsReturned, bool stopAtID, tokCompatability_t retTypes)
{
	RETURNCODE rc = FAILURE;

	referenceType_t rt;
	hCitemBase* pItem = NULL;
	UINT32      retVal = 0;
	int entrySize = allPossibleIDsReturned.size();

	rt = getRefType();

if (stopAtID)// defaults true
{
	if ( ! isRef || ( isRef && id != 0) )
	{
		//allPossibleIDsReturned.push_back( id );
		addUniqueUInt(allPossibleIDsReturned,id);
		return SUCCESS;
	}
	// else do the function
}
	HreferenceList_t refLst; /* stevev 4/8/4 */

	switch (rt) 
	{
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Record_id:
	case rT_Array_id:
	case rT_VarList_id:
/*  stevev 12/11/03
	this is the incorrect handling of this type::
	incorrect code removed 15jan09 -stevev
* * */
	case rT_Item_id:
	case rT_Block_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
		/* stevev 15dec05 - add new ones */		
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Image_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{
			// allPossibleIDsReturned.push_back( id );
			addUniqueUInt(allPossibleIDsReturned, id);
			rc = SUCCESS;
		}
		break;
	case rT_via_Attr:
		{	
			hCitemBase*      pGroupItemBase = NULL;

			UIntList_t		  refList;
			UIntList_t		::iterator refIT;

			if (pRef == NULL || pExpr == NULL)
			{				
				LOGIF(LOGP_NOT_TOK)
				(CERR_LOG,"ERROR: Ref Failure(All): compound ref type without ref or expr.\n");
				rc = -9;
			}
			// in a via_Attr - the Expression MUST BE A CONSTANT INT
			CValueVarient vVal = pExpr->resolve(NULL,true);// is a test
			if ( vVal.vIsValid )
			{
				int attr = (int)vVal, which = vVal.vIndex;

				// get the data sources
				// get all symbol ids of correct type(lists, axis & variables in via_attr)
				if(SUCCESS == pRef->resolveAllIDs(refList) && refList.size() > 0 )
				{	// for each datasource(ref X); 
					//	 we have only one expression
					// references should all be direct--and reflist id a list of symbol numbers
					for (refIT = refList.begin(); refIT < refList.end(); refIT++)
					{// ptr 2a UINT32 - should be symbol number of collection or array
						bool isValidTYPE = false;
						RETURNCODE gibsnRC = devPtr()->getItemBySymNumber(*refIT, &pGroupItemBase);
						if (  gibsnRC == SUCCESS )
						{// only legal attribute references that may be dependent are: 
						 //     list,axis & variable
							if ( pGroupItemBase->getIType() == iT_Variable  
							  || pGroupItemBase->getIType() == iT_List								  
							  || pGroupItemBase->getIType() == iT_Axis  )
							{
								isValidTYPE = true;
							}// else - leave it false (error logged later)
						}// else - leave it false
						if ( ! isValidTYPE )
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: via_Attr reference resolution"
								" with bad itemType.(%s)\n"
								,itemStrings[pGroupItemBase->getIType()]);
						}
						else 
						{// lookup expression valued attributes in all referenced values

							/* 18jan12 -- this isn't calling the derived class's function
							allPossibleIDsReturned.push_back(pGroupItemBase->getAttrID(attr, which));
							- I don't have time to degug it right now... */
							// begin replacement 18jan12
							itemID_t attrVar = 0;
							if (pGroupItemBase->getIType() == iT_Variable)
							{
								attrVar = ((hCVar*)pGroupItemBase)->getAttrID(attr, which);
							}
							else
							if (pGroupItemBase->getIType() == iT_List)
							{
								attrVar = ((hClist*)pGroupItemBase)->getAttrID(attr, which);
							}
							else // has to be iT_Axis
							{
								attrVar = ((hCaxis *)pGroupItemBase)->getAttrID(attr, which);
							}
							allPossibleIDsReturned.push_back(attrVar);
							// end replacement 18jan12
						}// endif - good reference
					}// next possible ref						
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: resolveAllIDs failed in via_Attr.\n");
				}
			}
			else // leave empty handed
			{
				LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: via_Attribute reference expression "
															     "resolved to a non-valid.\n");
			}
		}
		break;
	case rT_viaItemArray:	/* item array reference */
	case rT_via_Array:			/* value array reference */
	case rT_via_Collect:	/* collection reference */
	case rT_via_Record:		/* record reference */
	case rT_via_VarList:		/* variable list reference */
	case rT_via_File:
		/* stevev 15dec05 - add new ones */			
	case rT_via_List:
	case rT_via_BitEnum:
	case rT_via_Chart:
	case rT_via_Graph:
	case rT_via_Source:
	case rT_via_Blob:
		{
			ExprTrailList_t   exprValues;
			ExprTrailList_t ::iterator exprIT;

			UIntList_t		  refList;
			UIntList_t		::iterator refIT;

			hCitemBase*      pGroupItemBase = NULL;
			hCgroupItemDescriptor* pGID = NULL;

			bool  useAllValues = false; // this only goes true in the tokenizer when the index is an int

			if (pRef == NULL || pExpr == NULL)
			{				
				LOGIT(CERR_LOG,"Ref Failure(All): compound ref type without ref or expr.\n");
				rc = -9;
			}
			else
			{	// get expression - all ints (currently does not support true expressions)			
				rc = pExpr->resolveAllIndexes(exprValues);
#ifdef TOKENIZER
				extern enum tokState MainState;
				if (rc == APP_EXPR_INT_AS_INDEX  &&		// tokenizer informational return code, not error
					retTypes != tokNoOp					// duplicating SDC operation here with tokNoOp, but
														// this is clause really should not be here
					)
				{
					if (retAllInts(retTypes))
					{// wrong var type - the tokenizer apparently takes all values possible for ref
						useAllValues = true;
						if ( (rt == rT_viaItemArray || rt == rT_via_Array) && MainState == 0x0a /*Verifying */)
						{
							if ( pAref )
							{
								warning(VFY_NON_INDEX_IN_REF,pAref->filename.c_str(),pAref->lineNumber);
							}
							else
							{
								warning(VFY_NON_INDEX_IN_REF,"",0);
							}
						}
					}
					rc = SUCCESS;
				}

				// tokIndexesByIndexed compatibility modification [1/12/2017 tjohnston]
				// to emulate Tok10: if array is indexed a reference, use the entire container
				// not just the portion referenced by the index
				// TODO remove both sections of code in this source file that use useExpandedFlag
				bool useExpandedFlag = false;
				if (retAllIdx(retTypes))
				{
					expressElemType_t etype = pExpr->expressionL[0].exprElemType;
					if (etype == eet_VARID || etype == eet_VARREF)
					{
						useExpandedFlag = true;
					}
				} // end useExpandedFlag
#endif				

				if ( rc == SUCCESS )//must have value?
				{	// we have the indexes, get the data sources
					// get pref - all symbol ids of correct type 
					//							(itemarray,array,collect,record,varlists,files)
					if(SUCCESS == (rc =  pRef->resolveAllIDs(refList, stopAtID, retTypes))   &&   refList.size() > 0 )
					{	// for each datasource(ref X); 
						//	 for each expression value
						// references should all be direct
						for (refIT = refList.begin(); refIT < refList.end(); refIT++)
						{// ptr 2a UINT32 - should be symbol number of collection or array
							bool isValidTYPE = false;
							RETURNCODE gibsnRC = devPtr()->getItemBySymNumber(*refIT, &pGroupItemBase);
#ifdef TOKENIZER

							// TODO remove both sections of code in this source file that use useExpandedFlag
							if (rc == SUCCESS && useExpandedFlag)
							{
								hCgrpItmBasedClass *pGrp = dynamic_cast<hCgrpItmBasedClass *>(pGroupItemBase);
								if (NULL != pGrp)
								{
									UIntList_t values;
									rc = pGrp->getAllindexValues(values);
									if (rc == SUCCESS  && values.size() > 0)
									{
										// replace existing exprValues with ones from parent ID, 
										// emulating Tok10 misbehavior
										exprValues.clear();
										hCexpressionTrail et;
										for (UIntList_t::iterator it = values.begin(); it != values.end(); it++)
										{
											et.clear();
											et.srcIdxVal = (*it);
											exprValues.push_back(et);
										}
									}
								}
							} // end useExpandedFlag

							// this only occurs if the expression is an integer reference
							if (  gibsnRC == SUCCESS && useAllValues )
							{
								//get all possible referenced values and put the ids in the return list
								
								if ( pGroupItemBase->getIType() == iT_ItemArray  
								  || pGroupItemBase->getIType() == iT_Collection 
								  || pGroupItemBase->getIType() == iT_File 
								  || pGroupItemBase->getIType() == iT_Chart 
								  || pGroupItemBase->getIType() == iT_Graph 
								  || pGroupItemBase->getIType() == iT_Source   )
								{
									hCattrGroupItems* pGI = pGroupItemBase->getMemberList();
									if ( pGI == NULL )
									{
										DEBUGLOG(CERR_LOG|CLOG_LOG,"ERROR: reference resolution "
																		   "with -member attribute.\n");
									}
									else
									{
										gibsnRC = pGI->getAllindexes(allPossibleIDsReturned, retTypes);// returns a UIntList_t
									}
								}
								else
								if ( pGroupItemBase->getIType() == iT_Array      
								  || pGroupItemBase->getIType() == iT_List  )
								{
									if (retTypeDef(retTypes))
									{
										allPossibleIDsReturned.push_back(pGroupItemBase->getID());
									}
									else // tokenizer doesn't do psuedovariables
									{
										DEBUGLOG(CERR_LOG|CLOG_LOG,"ERROR: reference resolution "
																		   "with psuedo-member itemType.\n");
									}
								}
								else
								if ( pGroupItemBase->getIType() == iT_Variable )
								{
									allPossibleIDsReturned.push_back(pGroupItemBase->getID());
								}
								else
								{
									LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference resolution "
																		   "with bad itemType.\n");
								}
							}
								//rc = pGroupItemBase->getAllindexValues//(allPossibleIDsReturned);

								// the group item has   
								// getAllindexValues(UIntList_t& allValidindexes);//all possible index numbers

								// charts, graphs and sources may need to have this function added
								// deal with variables type bit-enum  and typedefs for arrays and lists...

								//continue; to next reference in the list
							
#endif			


							if (  gibsnRC == SUCCESS && ! useAllValues)
							{// deal with the empty list error...
								if ( pGroupItemBase->getIType() == iT_List &&
									((hClist*)pGroupItemBase)->getCount() < 1 )
								{// we be whistl'n in da wind
									//entrySize -= 1;//allow a successful return
									rc = DB_ELEMENT_NOT_FOUND;// used for empty-list syndrome
									continue; // for loop, next list
								}
								else
								if ( pGroupItemBase->getIType() == iT_ItemArray  
								  || pGroupItemBase->getIType() == iT_Array  
								  || pGroupItemBase->getIType() == iT_Collection 
								  || pGroupItemBase->getIType() == iT_File      
								  || pGroupItemBase->getIType() == iT_List 
								  || pGroupItemBase->getIType() == iT_Chart 
								  || pGroupItemBase->getIType() == iT_Graph 
								  || pGroupItemBase->getIType() == iT_Source   )
								{
									isValidTYPE = true;
								}
								else
								if ( pGroupItemBase->getIType() == iT_Variable )
								{
									if (((hCVar*)pGroupItemBase)->VariableType() == vT_BitEnumerated)
									{
										isValidTYPE = true;
									}// else - leave it false
								}
								// else - leave it false
							if ( ! isValidTYPE )
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference resolution "
																	   "with bad itemType.\n");
							}
							else 
							{// lookup all expression values in all referenced values
								for (exprIT = exprValues.begin(); 
									 exprIT < exprValues.end(); 
									 exprIT++)
								{// ptr 2a hCexpressionTrail
	/* new, replacenment code for below - stevev - 4/8/04 */									
										if (pGroupItemBase->getIType() == iT_ItemArray  ||
											pGroupItemBase->getIType() == iT_Collection ||
											pGroupItemBase->getIType() == iT_File       ||
											pGroupItemBase->getIType() ==  iT_Array     ||
											pGroupItemBase->getIType() ==  iT_List         )
									{
									// when working in the tokenizer, this function needs to return the typedef
									// when it finds a list or arry element.  AMS &FDI use the tables...they say
											if ( retTypeDef(retTypes)   && 
												( pGroupItemBase->getIType() ==  iT_Array || pGroupItemBase->getIType() == iT_List)  )
										{// we want to return the typedef
											itemID_t theID = 0;
												if ( pGroupItemBase->getIType() == iT_List )
											{
													theID = ((hClist*)pGroupItemBase)->getTypedefId();
											}
											else // has to be array
											{
											// you gotta run this first to resolve it, then get the pointer
													((hCarray*)pGroupItemBase)->getTypedefType();
													hCitemBase* pTD = ((hCarray*)pGroupItemBase)->getTypePtr();
												if (pTD)
												{
													theID = pTD->getID();
												}
											}
											if ( theID )
											{
												allPossibleIDsReturned.push_back(theID);
												continue;
											}

										}
										else // normal path
											if (SUCCESS != ((hCgrpItmBasedClass*)pGroupItemBase)->
														getAllByIndex(exprIT->srcIdxVal, refLst))
										{
											LOGIF(LOGP_NOT_TOK)(CERR_LOG,
												"ERROR: Group Item index(s) doesn't exist.\n");
											continue;// loop to next expression value
										}// else - fall thru to common code
									}
									/* added to support new types (member attribute references) 
									 *  stevev   5jul06*/
									else
										if (pGroupItemBase->IsVariable())// must be bit-Enum
									{
											allPossibleIDsReturned.push_back(pGroupItemBase->getID());
									}
									else
										if (pGroupItemBase->getIType() == iT_Chart)// attribute reference
									{
											if (SUCCESS != ((hCchart*)pGroupItemBase)->
														getAllByIndex(exprIT->srcIdxVal, refLst))
										{
											LOGIF(LOGP_NOT_TOK)(CERR_LOG,
												"ERROR: Group Item Chart index(s) doesn't exist.\n");
											continue;// loop to next expression value
										}// else - fall thru to common code
									}
									else
										if (pGroupItemBase->getIType() == iT_Graph)// attribute reference
									{
											if (SUCCESS != ((hCgraph*)pGroupItemBase)->
														getAllByIndex(exprIT->srcIdxVal, refLst))
										{
											LOGIF(LOGP_NOT_TOK)(CERR_LOG,
												"ERROR: Group Item Graph index(s) doesn't exist.\n");
											continue;// loop to next expression value
										}// else - fall thru to common code
									}
									else
										if (pGroupItemBase->getIType() == iT_Source)// attribute reference
									{
											if (SUCCESS != ((hCsource*)pGroupItemBase)->
														getAllByIndex(exprIT->srcIdxVal, refLst))
										{
											LOGIF(LOGP_NOT_TOK)(CERR_LOG,
												"ERROR: Group Item Source index(s) doesn't exist.\n");
											continue;// loop to next expression value
										}// else - fall thru to common code
									}
									/* end added stevev 5jul06 */
									else
									{
										LOGIT(CLOG_LOG,"WARNING: %s type does not support "
											"xref 'getAllByIndex' at this time.\n",
												pGroupItemBase->getTypeName() );
										continue;
									}
										for (HreferenceList_t::iterator it = refLst.begin(); it != refLst.end();   ++it)
									{
											hCreference *pR = &(*it);
											//rc = it->resolveAllIDs(allPossibleIDsReturned);
											rc = pR->resolveAllIDs(allPossibleIDsReturned, stopAtID, retTypes);
									}
									refLst.clear();
	#if 0 /* removal - stevev - 4/8/4 the getByIndex was doing a ReadImd during conditional resolution
							this function is called before we have a device and gives problems*/
		 /* code deleted 19dec2016 - see earlier version if you need to see the code */
	#endif /* end removal - stevev - 4/8/4 */
								}// next expression value
							}// endif - good reference
							}// else - leave it false
						}// next ref						
					}
					else
					if ( (refList.size() < 1 && pGroupItemBase != NULL && pGroupItemBase->getIType() == iT_List) 
						|| rc == DB_ELEMENT_NOT_FOUND )
					{// probably an empty list situation,
						;// do not speak of it...
					}
					else
					if ( ! devPtr()->getPolicy().isPartOfTok )
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: addAllRefs failed.\n");
#ifdef _DEBUG
						LOGIF(LOGP_NOT_TOK)(COUT_LOG,"ERROR: addAllRefs failed.\n");
						pRef->dumpSelf(3, "Failure!");
						LOGIT(COUT_LOG,"---------------------------\n");
#endif
					}
				}
				else // resolve all indexes failed
				if ( rc == SUCCESS )
				{
					LOGIF(LOGP_MISC_CMTS)
						 (CERR_LOG,"ERROR: resolveAll indexes failed on ref's expression\n");
				}
				// else assume an error message has already been displayed
			}// endelse - expr & ref are non-null
		}
		break;
		
	case rT_Separator:
	case rT_Constant:
	case rT_Row_Break:
		break; // do nothing with these

	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
		{
			clog << "WARNING: unhandled reference resolution for BLOCK types." <<endl;
			rc = FAILURE;
		}
		break;
	case rT_NotA_RefType:		// 44
	default:
		{
			clog << "WARNING: unrecognized reference type.(" << (int)rt <<")"<<endl;
			rc = -5;
		}
		break;
	}// endswitch
	if ( (allPossibleIDsReturned.size() - entrySize) > 0  && rc != DB_ELEMENT_NOT_FOUND )
	{
		rc = SUCCESS;
	}
	return rc;
}

RETURNCODE hCreference::resolveAllIDs(vector<hCitemBase*>& allPossiblePtrsReturned, tokCompatability_t retTypes)
{
	RETURNCODE rc = SUCCESS;	
	UINT32          itemNum = 0;
	hCitemBase*     pItem = NULL;

	vector<UINT32>  listOitems;
	referenceType_t thisType = getRefType();
	/* check rT_Image_id */
	if ( thisType == rT_Separator || thisType == rT_Constant || 
		 thisType == rT_Row_Break || thisType >= rT_NotA_RefType )
	{// these never have IDs...
		return SUCCESS;// leave the list as it is
	}

	if ((rc = resolveAllIDs(listOitems, true, retTypes)) == SUCCESS && listOitems.size() > 0)
	{
		for ( vector<UINT32>::iterator iTu = listOitems.begin(); iTu < listOitems.end(); iTu++)
		{// iTu is a ptr 2 a UINT32
			if ( devPtr()->getItemBySymNumber(*iTu, &pItem) == SUCCESS || retUndefs(retTypes) )
			{
				allPossiblePtrsReturned.push_back(pItem);
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference resolveAllIDs could not get item pointer for 0x%04x.\n",UINT32(*iTu) );
				rc = APP_RESOLUTION_ERROR;
			}
		}// next
	}
	else
	if ( listOitems.size() <= 0 )
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Reference's resolveAllIDs got a successful empty list.\n");
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference could not resolve the item number.\n");
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}
#ifdef ADD_DEBUG
extern int IsCmd;
#endif
#ifdef _DEBUG
int depth = 0;
#endif
/* * *
	addAllVars
	to insert variable values for all indexes
	used to get command usage
	index information inside trail
	no trail supplied if constant index OR direct access
 * * */
RETURNCODE hCreference::addAllVars(GroupItemList_t& varGroup)
{
	RETURNCODE rc = FAILURE;
	hCGroupItemInfo localInfo;// what GroupItemList holds

#ifdef _DEBUG
depth ++;
#endif
	referenceType_t rt = getRefType();

	if ( ! isRef || ( isRef && id != 0) )
	{
		localInfo.theVarID = id;
		devPtr()->getItemBySymNumber(id, &localInfo.srcPtr);

		// leave .everything else empty
		varGroup.push_back( localInfo );// no trail additions
#ifdef _DEBUG
		hCitemBase* pItem = NULL;
		rc = devPtr()->getItemBySymNumber(id, &pItem);
		if ( rc == SUCCESS && pItem != NULL )
		{
			if ( pItem->getIType() != iT_Variable
				&& 0 /* turned this off*/  )
				LOGIT(CERR_LOG,"PROBLEM: addAllVars is adding a non-variable (0x%04x"
				") of type %s\n",id ,pItem->getTypeName());
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: addAllVars is inserting a id 0x%04x"
				" that does not exist.\n", id);
		}
#ifdef DBG_REFTRAIL
		clog << "Return AllVars = "<<varGroup.size()<<endl;
#endif
#endif
#ifdef _DEBUG
depth --;
#endif
		return SUCCESS;
	}
	// else do the function

	switch (rt) 
	{
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Record_id:
	case rT_Array_id:
	case rT_VarList_id:
/* *stevev 12/12/03
	handle these just like the constant id below
		{// all of these should have a via_XXXXXX above them to break down the response
			if ( id != 0 )
			{			
				localInfo.theVarID = id;
				varGroup.push_back( localInfo );// no trail additions
				rc = SUCCESS;
			}
			else
			{
				clog << "WARNING: compound actual with no id." <<endl;
				rc   = SUCCESS;
			}
		}
		break;
* */
	case rT_Item_id:
	case rT_Block_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Image_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{		
			localInfo.theVarID = id;
			devPtr()->getItemBySymNumber(id, &localInfo.srcPtr);
		//  leave .everything else
		// note: stevev 11feb09, this is method is re-entered for the ref side in FillCombinations
		//       so we can't filter all but variables here.  The caller will need to do that.
			varGroup.push_back( localInfo );// no trail additions
		//	cerr <<"     : AddAllVars is adding a non Var Type.("<<refTypeStrings[rt]<<")"<<endl;
			rc = SUCCESS;
		}
		break;
	case rT_viaItemArray:	/* item array reference */
	case rT_via_Collect:	/* collection reference */
	case rT_via_Array:			/* profibus array reference */
	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
	case rT_via_File:
	case rT_via_List:
	case rT_via_BitEnum:
	case rT_via_Chart:
	case rT_via_Graph:
	case rT_via_Source:
	case rT_via_Blob:
		{
			if (pRef == NULL || pExpr == NULL)
			{				
				LOGIT(CERR_LOG,"Ref Failure(AllVars): compound ref type without ref or expr.\n");
				rc = -12;
			}
			else
			{// fill all combinations of expression vs references
				rc = fillCombinations(varGroup);// aka: lookup(allexpr,allrefs)
#ifdef ADD_DEBUG
if(IsCmd)
	clog <<"  ADD: group has " << varGroup.size() << "  entries."<<endl;
#endif
			}
#ifdef DBG_REFTRAIL
int p = 0;
clog<<"-------------------"<<depth<<endl;
if ( varGroup.size() <= 0 )
{	clog<<" fillCombinations returned " << rc << " with NO data in the list."<<endl;
}
for (GroupItemList_t::iterator yt=varGroup.begin(); yt < varGroup.end(); yt++, p++)
{
	clog<<"-- Group Set "<<p << endl;
	yt->clogSelf(depth);
}
clog<<"-- End VIA -------- "<< depth<<" rc=("<<rc<<")" << endl;
#endif
		}
		break;
	case rT_via_Attr:
		{
			LOGIT(CLOG_LOG,"Attribute Reference is not allowed in a Data Item in a command.\n");
		}// returns FAILURE
		break;
	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
		{
			clog << "WARNING: unhandled reference resolution for BLOCK types." <<endl;
			rc = -7;
		}
		break;
	case rT_Separator:
	case rT_Constant:
	case rT_Row_Break:
		{
			clog << "WARNING: addAllVars for a constant reference type." <<endl;
			rc = -9;
		}
		break;

	default:
		{
			clog << "WARNING: unrecognized reference type." <<endl;
			rc = -5;
		}
		break;
	}// endswitch
#ifdef _DEBUG
depth --;
#endif
	return rc;
}

// helper function to break out the real work from the large function above
// assumes reference and expression pointers are both NOT-NULL
// only called in via_xxx situations
RETURNCODE hCreference::fillCombinations(GroupItemList_t& varGroup)
{
	RETURNCODE rc = SUCCESS;

	hCGroupItemInfo localInfo;		// what the GroupItemList_t holds
	GroupItemList_t localInfoList;
	GroupItemList_t ::iterator lilIT;
	GroupItemList_t lVG;
	GroupItemList_t ::iterator lvgIT;
	hCvarGroupTrail localGroupTrail;

	ExprTrailList_t exprValues;
	ExprTrailList_t ::iterator exprIT;

	hCitemBase*     pGroup = NULL;
	hCgroupItemDescriptor* pGID = NULL;

HreferenceList_t refLst; /* stevev 4/8/4 */

	// get expression - all ints (currently does not support true expessions)			
	if ( SUCCESS == (rc = pExpr->resolveAllIndexes(exprValues) )
		&& exprValues.size() > 0) // must have a value?
	{	// get pref - all symbol ids of correct type 
#ifdef ADD_DEBUG
if(IsCmd)
	clog <<"       fill 2 addAllVars reentry."<<endl;
#endif
#ifdef DBG_REFTRAIL
	clog<<" FillComb got "<<exprValues.size()<<" expression values."<<endl;
#endif
		//							(itemarray,array,collect,record,varlists)
		if( ( SUCCESS == ( rc = pRef->addAllVars(localInfoList)) ) && localInfoList.size() > 0 )
		{	// references may or may not be direct
#ifdef ADD_DEBUG
if(IsCmd)
{	clog <<"       fill 2 addAllVars return."<<endl;
	clog <<"  ADD: Info List has " << localInfoList.size() << "  entries."<<endl;
	clog <<"  ADD: Expr List has " << exprValues.size() << "  entries."<<endl;
	clog <<"  ADD:        giving " << exprValues.size() * localInfoList.size()<< "  lookups."<<endl;
}
#endif
#ifdef DBG_REFTRAIL
	clog<<" FillComb got "<<localInfoList.size()<<" Var values."<<endl;
#endif
			//   for each source reference
			for (lilIT = localInfoList.begin(); lilIT < localInfoList.end(); lilIT++)
			{// ptr 2a hCvarSrcTrail - should be symbol number of collection or array
				if ( devPtr()->getItemBySymNumber(lilIT->theVarID, &pGroup) != SUCCESS 
					|| 
					( pGroup->getIType() != iT_ItemArray && pGroup->getIType() != iT_Collection  &&
					  pGroup->getIType() != iT_List      && pGroup->getIType() != iT_Array       &&
					  pGroup->getIType() != iT_File  )
				)
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: multi-reference resolution with bad itemType.\n");
				}
				else 
				{// lookup all expression values in all referenced values

					//  for each expression resolution
					for (exprIT = exprValues.begin(); 
						 exprIT < exprValues.end(); 
						 exprIT++)
					{// ptr 2a hCexpressionTrail
						if (pGroup->getIType() == iT_ItemArray)
						{
/* stevev 4/8/4 -try to replace
							if(SUCCESS != ((hCitemArray*)pGroup)->
											      getByIndex(exprIT->srcIdxVal,&pGID)
							  || pGID == NULL )
							{
								cerr<<"ERROR: Array index doesn't exist."<<endl;
								continue;// loop to next expression value
							}// else - fall thru to common code
end stevev 4/8/4 - removes*/
							if (SUCCESS != ((hCitemArray*)pGroup)->
													getAllByIndex(exprIT->srcIdxVal, refLst))
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Reference Array index(s) doesn't exist.\n");
								continue;// loop to next expression value
							}// else - fall thru to common code
//end stevev 4/8/4 adds 
						}
// stevev 09jun05
						else
						if (pGroup->getIType() == iT_List)
						{
							if (SUCCESS != ((hClist*)pGroup)->
													getAllByIndex(exprIT->srcIdxVal, refLst))
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List index doesn't exist.\n");
								continue;// loop to next expression value
							}// else - fall thru to common code
						}
						else
						if (pGroup->getIType() == iT_Array)
						{
							if (SUCCESS != ((hCarray*)pGroup)->
													getAllByIndex(exprIT->srcIdxVal, refLst))
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Value Array index doesn't exist.\n");
								continue;// loop to next expression value
							}// else - fall thru to common code
						}
// end stevev 09jun05
						else // MUST be a collection or file
						{
/* stevev 4/8/4 -try to replace
							if (! ((hCcollection*)pGroup)->isInGroup(exprIT->srcIdxVal)) 
							{
								cerr<<"ERROR: Collection name doesn't exist.  0x"<<hex
									<<((hCcollection*)pGroup)->getID()<<"[0x"<<exprIT->srcIdxVal
									<<dec<<"]"<<endl;
								continue; // do next, we'll discard for now
							}
							if(SUCCESS != ((hCcollection*)pGroup)->
											      getByIndex(exprIT->srcIdxVal,&pGID)
							  || pGID == NULL )
							{
								cerr<<"ERROR: Collection index doesn't exist."<<endl;
								continue;// loop to next expression value
							}// else - fall thru to common code
end stevev 4/8/4 - removes*/
							if (SUCCESS != ((hCcollection*)pGroup)->
													getAllByIndex(exprIT->srcIdxVal, refLst))
							{
//getAllByIndex has already put out a message								LOGIT(CERR_LOG,"ERROR: Collection member doesn't exist.\n");
								rc = APP_RESOLUTION_ERROR;
								continue;// loop to next expression value
							}// else - fall thru to common code
//end stevev 4/8/4 adds 
						}
#ifdef ADD_DEBUG
if(IsCmd)
	clog <<"  ADD: lookup has " << refLst.size() << "  entries."<<endl;
#endif
#ifdef DBG_REFTRAIL
	clog<<" FillComb got "<<refLst.size()<<" lookup values from "
		<< itemStrings[pGroup->getIType()] <<" getAllByIndex()."<<endl;
#endif
						// common code
						// the lookup is done, record the outcome
// stevev 4/8/4			if (SUCCESS != pGID->getRef().addAllVars(lVG) || lVG.size() <= 0)
// stevev 4/8/4			{
// stevev 4/8/4				cerr<< "ERROR: reference lookup via index failed for 0x"
// stevev 4/8/4					<<hex<<lilIT->theVarID<<dec<<" ["<<exprIT->srcIdxVal<<"]"<<endl;
// stevev 4/8/4			}
// stevev 4/8/4			else
// stevev 4/8/4			{
/* stevev 4/8/4   }*/	for (HreferenceList_t::iterator it = refLst.begin();it<refLst.end();it++)
						{
							if ( SUCCESS != it->addAllVars(lVG))
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: looked up reference failed to get vars "
									"for 0x%04x [%d]\n", lilIT->theVarID, exprIT->srcIdxVal);
							}
							else
							{
#ifdef _DEBUG 
	if (lVG.size() <= 0)
	{clog <<"addAllVars returned an empty list"<<rc<<endl;
	}
#endif
								for (lvgIT = lVG.begin(); lvgIT < lVG.end(); lvgIT++)
								{// ptr 2a hCGroupItemInfo	
									// the source and expression is in each entry
									localInfo.srcExpr = *exprIT;
									localInfo.srcID   = lilIT->theVarID;
									localInfo.srcPtr  = pGroup;

									//lilIT  src ptr for src  history
									if (lilIT->srcID != 0)
									{
										localInfo.srcHist_p = new hCGroupItemInfo(*lilIT);
										//localInfo.theVarHist_p->srcID   = lvgIT->srcID;
										//localInfo.theVarHist_p->srcExpr = lvgIT->srcExpr;
										//localInfo.theVarHist_p->srcPtr  = lvgIT->srcPtr;
									}
									
									localInfo.theVarID  = lvgIT->theVarID;
									localInfo.theVarPtr = lvgIT->theVarPtr;
									localInfo.theVarExpr= lvgIT->theVarExpr;
									if (lvgIT->srcID != 0)
									{
										localInfo.theVarHist_p = new hCGroupItemInfo(*lvgIT);
										//localInfo.theVarHist_p->srcID   = lvgIT->srcID;
										//localInfo.theVarHist_p->srcExpr = lvgIT->srcExpr;
										//localInfo.theVarHist_p->srcPtr  = lvgIT->srcPtr;
									}

									varGroup.push_back(localInfo);
									localInfo.clear();
								}// next group item
							}// endelse - got vars
							lVG.clear();
						}// next 'looked-up' reference
#ifdef ADD_DEBUG
if(IsCmd)
	clog <<"  ADD: returnList now has " << varGroup.size() << "  entries."<<endl;
#endif
						refLst.clear();
// stevev 4/8/4						lVG.clear();
// stevev 4/8/4						delete pGID; pGID = NULL;
					}// next expression value
				}// endif - good reference
			}// next ref						
		}
		else
		if ( rc == DB_ELEMENT_NOT_FOUND )
		{
			;// supress empty list errors
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference addAllVars failed w/%d items in the list.\n"
																	, localInfoList.size());
		}
	}
	else
	if ( rc == DB_ELEMENT_NOT_FOUND )
	{
		;// supress empty list errors
	}
	else // resolve all indexes failed
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: resolveAll indexes failed on ref's expression\n");
	}

	localInfoList.clear();
	exprValues.   clear();
	return rc;
}

// tell the lists in the reference what command they are in and what index is needed
// When the list elements are instantiated, this information can be used to determine
// the read and write command for the dispatcher.
RETURNCODE hCreference::commandAllLists(hCcommandDescriptor& cmdDesc)
{
	RETURNCODE rc = FAILURE;
	hCGroupItemInfo localInfo;// what GroupItemList holds
	itemID_t       refVal = 0;

	referenceType_t rt = getRefType();
/*
	if ( ! isRef || ( isRef && id != 0) )
	{
		devPtr()->getItemBySymNumber(id, &localInfo.srcPtr);

		hCitemBase* pItem = NULL;
		rc = devPtr()->getItemBySymNumber(id, &pItem);
		if ( rc == SUCCESS && pItem != NULL )
		{
			// just return it
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: commandAllLists found an id 0x%04x"
				" that does not exist.\n", id);
		}
/x* for now...
		return SUCCESS;
.... *x/
	}
	// else do the function
*****/
	switch (rt) 
	{
	case rT_VarList_id:
	case rT_Record_id:
	case rT_Block_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
		{
			LOGIT(CLOG_LOG, "WARNING: unsupported reference type in commandAllLists.\n" );
			rc = -13;
		}
		break;
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Array_id:
/* *stevev 12/12/03
	handle these just like the constant id below
* */
	case rT_Item_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Image_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{		
			localInfo.theVarID = id;
			//devPtr()->getItemBySymNumber(id, &localInfo.srcPtr);
			cmdDesc.srcIdxVar = id;// return the value
			rc = SUCCESS;
		}
		break;
	case rT_viaItemArray:	/* item array reference */
	case rT_via_Array:		/* profibus array reference */
	case rT_via_List:
		{
			// verify that the expression is a variable
			// if the index is a constant, the info should not be updated
			//
			// get the list reference resolution (it could have a list in it too)
			// send the list the command info and the index variable name

			
			if (pRef == NULL || pExpr == NULL)
			{				
				LOGIT(CERR_LOG,"CAL:Ref Failure(AllVars): compound ref type without ref or expr.\n");
				rc = -12;
			}
			else
			{
				CValueVarient  v;
				hCexpressionTrail* retTrail = NULL;
				hCitemBase* pItem = NULL;

				// we haven't assigned read commands yet, so make isAtest true
				v = pExpr->resolve(&retTrail, true);// true...stevev 07apr15
				// update index variables?
				cmdDesc.srcIdxVar = 0;
				rc = pRef->commandAllLists(cmdDesc);
				if (rc == SUCCESS && cmdDesc.srcIdxVar != 0)// we have a source item
				{
					rc = devPtr()->getItemBySymNumber(cmdDesc.srcIdxVar, &pItem);				
					
					if ( rc == SUCCESS && pItem != NULL ) 
					{
						if (retTrail != NULL && retTrail->isVar &&
							pItem->getIType() == iT_List)// we will only update LISTs for now
						{
							hIndexUse_t locIdx;
							locIdx.indexSymID = retTrail->srcID;
							locIdx.indexDispValue = retTrail->srcIdxVal;
							cmdDesc.idxList.push_back(locIdx);
							if ( cmdDesc.cmdTyp == cmdOpRead )
								((hClist*)pItem)->addRDdesc(cmdDesc);
							else // has to be write				
								((hClist*)pItem)->addWRdesc(cmdDesc);

							cmdDesc.idxList.clear();
						}// otherwisw we don't do an update of the lists
						//always lookup v in cmdDesc.srcIdxVar
						hCgroupItemDescriptor* pGid = NULL;
						rc = pItem->getByIndex((UINT32)v,&pGid);

						if (  rc == SUCCESS    &&   pGid != NULL )
						{// we have the indexed element of the array described in pGID		
							//itemRef = pGID->getRef();	// looked-up member item
							
							if ( (rc = pGid->getRef().resolveID(refVal, false, false, 0, true)) 
								  == SUCCESS )
							{// we have the referenced itemID of this element
								cmdDesc.srcIdxVar   = refVal;
							}// else, failure return
						}
						else
						if( pItem->getIType() == iT_List && rc == APP_LIST_IS_EMPTY )
						{// if it's an empty list, return the typedef (ONLY in commandAllLists)
							cmdDesc.srcIdxVar =((hClist*)pItem)->getTypedefId();//could be zero
							rc = SUCCESS;
						}// else, failure return  
						else
						{
							cmdDesc.srcIdxVar = 0;
						}
						if ( pGid )
						{
							RAZE(pGid);
						}
					}// bad source id, failure return
				}// else probable error

                //CPMHACK: Deallocate the memory after performing operation on local variable
                RAZE(retTrail);
			}// end-else we have both parts
		}
		break;
	case rT_via_Collect:	/* collection reference */
	case rT_via_File:
	case rT_via_BitEnum:	/* a bit reference     */
	case rT_via_Chart:		/* 40 - pMemberAttr to Source */
	case rT_via_Graph:		/*    - pMemberAttr to Waveform */
	case rT_via_Source:		/*    - pMemberAttr to Variable */
	case rT_via_Blob:		/* - i have no idea */
		{
			if (pRef == NULL || pExpr == NULL)
			{				
				LOGIT(CERR_LOG,"CAL:Ref Failure(AllVars): compound ref type without ref or expr.\n");
				rc = -12;
			}
			else
			{
				CValueVarient  v;
				hCexpressionTrail* retTrail = 0;
				hCitemBase* pItem = NULL;

				v = pExpr->resolve(&retTrail);
				// update index variables?
				cmdDesc.srcIdxVar = 0;
				rc = pRef->commandAllLists(cmdDesc);
				if (rc == SUCCESS && cmdDesc.srcIdxVar != 0)// we have a source item
				{	//lookup v in cmdDesc.srcIdxVar
					rc = devPtr()->getItemBySymNumber(cmdDesc.srcIdxVar, &pItem);				
					
					if ( rc == SUCCESS && pItem != NULL ) 
					{
						hCgroupItemDescriptor* pGid = NULL;
						rc = pItem->getByIndex((UINT32)v,&pGid, false);

						if (  rc == SUCCESS    &&   pGid != NULL )
						{// we have the indexed element of the array described in pGID							
							//itemRef = pGID->getRef();	// looked-up member item
							
							if ( (rc = pGid->getRef().resolveID(refVal, false, false, 0, true)) 
									== SUCCESS )
							{// we have the referenced itemID of this element
								cmdDesc.srcIdxVar   = refVal;
							}// else, failure return
						}// else, failure return
						if ( pGid )
						{
							RAZE(pGid);
						}
					}
				}

                //CPMHACK: Deallocate the memory after performing operation on local variable
                RAZE(retTrail);
			}
		}
		break;
	case rT_via_Attr:
		{
			LOGIT(CLOG_LOG,"CAL:Attribute Reference is not allowed in a Data Item in a command.\n");
		}// returns FAILURE
		break;
	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
		{
			LOGIT(CLOG_LOG,"CAL:WARNING: unhandled reference resolution for BLOCK types.\n");
			rc = -7;
		}
		break;
	case rT_Separator:
	case rT_Constant:
	case rT_Row_Break:
		{
			LOGIT(CLOG_LOG,"CAL:WARNING: addAllVars called for a constant reference type.\n");
			rc = -9;
		}
		break;

	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
	default:
		{// test for a straight ID (shouldn't be here)
			if ( ! isRef || ( isRef && id != 0) )
			{// verify it's a valid item id
				hCitemBase* pItem = NULL;
				rc = devPtr()->getItemBySymNumber(id, &pItem);
				if ( rc == SUCCESS && pItem != NULL )
				{// just return it
					cmdDesc.srcIdxVar = id;// return the value
				}
				else
				{
					LOGIT(CLOG_LOG,"CAL:WARNING: unrecognized reference type with unknown id.\n");
					rc = -5;
				}
			}
			else
			{
				LOGIT(CLOG_LOG,"CAL:WARNING: unrecognized reference type.\n");
				rc = -6;
			}
		}
		break;
	}// endswitch
	return rc;
}

/* take everything in pitem that matches index and put it into varGroup
RETURNCODE hCreference::ref2GIL(hCitemBase* pItem, UINT32 index, 
					GroupItemList_t& varGroup, 
					hCGroupItemInfo& activeInfo, 
					itemID_t indxVarID / *= 0x0000* /)
{
	if ( pItem == NULL ) return FAILURE;

	itemType_t		tItem  = pItem->getIType();;
	itemID_t		ItemID = pItem->getID();

	if ( ItemID != activeInfo.theVarID )
	{
	}
	
	UINT32           retVal = 0;
	hCGroupItemInfo  localInfo;
	hCvarSrcTrail    localVarTrail;
	HreferenceList_t localConstRefs;
	vector<UINT32>   allPossibleIDs;

	RETURNCODE rc = FAILURE;


	//get all references that match the index - (for all possible resolutions)
	rc = pItem->getAllByIndex(index, localConstRefs);
	// localConstRefs array's references for matching index (all conditions)
	if ( rc != SUCCESS )
	{
		cerr << "ERROR: item 0x"<<hex<<ItemID<<
				" could not dereference INDEX 0x"<<index<<dec<<"."<<endl;
	//  return rc
	}
	else
	if ( localConstRefs.size() <= 0 )
	{
//		clog << "NOTE: item 0x"<<hex<<ItemID<<
//				" could not dereference INDEX 0x"<<index<<dec<<"."<<endl;
		// leave rc as success
	}
	else
	{
		for (HreferenceList_t::iterator itItemID = localConstRefs.begin();
										itItemID < localConstRefs.end();   itItemID++)
		{// itItemID isa ptr 2 a UINT32 which is a 
		 //			 symbol id resolved from element reference- huh?
		 // now it's a ptr to a reference that is the array's ref with given index
			//ERROR::: rc = itItemID->resolveID(retVal);---we need 'em all with no sent commands
			rc = itItemID->resolveAllIDs(allPossibleIDs);
			if ( rc != SUCCESS || allPossibleIDs.size() < 1 ) //was: retVal == 0x0000 )
			{
				cerr << "ERROR: item 0x"<<hex<<ItemID<< dec  <<
												" failed to resolve its ID " << endl;
				continue; // next index
			}// else keep working
			for ( vector<UINT32>::iterator iTu = allPossibleIDs.begin(); 
															iTu < allPossibleIDs.end(); iTu++)
			{
				localInfo.theVarID = *iTu;//was::retVal;
				//localInfo.theVarID = *itItemID;
			//		trail type=tItem,id=itII->theVarID,idxV=index,idxVar=itEx->srcID
				localVarTrail.srcType  = tItem;
				localVarTrail.srcID    = ItemID;
				localVarTrail.srcIdxVal= index;
				localVarTrail.srcIdxVar= indxVarID;
			//		local push trail
				localInfo.theVarTrail.push_back(localVarTrail);/// WE MAY WANT TO SKIP THIS?????
				for (TLiterator iTL = activeInfo.theVarTrail.begin();
								iTL < activeInfo.theVarTrail.end();  iTL++)
				{//iTL isa ptr 2 a hCvarSrcTrail
					localInfo.theVarTrail.push_back(*iTL);
				}
				varGroup.push_back(localInfo);
				localInfo.clear();
			}
			allPossibleIDs.clear();
		}
	}// endelse SUCCESS
	return rc;
}
*/


/* these have to be after expression declaration */
inline
RETURNCODE hCreference::destroy(void) 
{ 
/*Vibhor 050404: Start of Code*/
	try{
	if ( pExpr != NULL )
	{ /*for now, just delete*/ 
		/* test */pExpr->destroy();
		RAZE(pExpr);
	} 
	}
	catch(...)
	{
		LOGIT(CERR_LOG,"TempLog: Caught hCreference::destroy(void) in RAZE(pExpr)inside catch(...)\n");
	}

	if ( pRef != NULL ) 
	{ 
		try
		{
			pRef->destroy();  
		}
		catch(...)
		{
			LOGIT(CERR_LOG,"TempLog: Caught hCreference::destroy(void) in pRef->destroy();inside catch(...)\n");

		}
		try
		{
			RAZE(pRef);
		}
		catch(...)
		{
			LOGIT(CERR_LOG,"TempLog: Caught hCreference::destroy(void) in RAZE(pRef)inside catch(...)\n");

		}
	
	
	}
	try
	{
	//VMKP 260304
	grpTrail.clear();// same as destroy in this instance
		
	}
	catch(...)
	{
		LOGIT(CERR_LOG,"inline RETURNCODE hCreference::destroy(void) grpTrail.clear(); inside catch(...) !!\n");
	}
	return SUCCESS;

/*Vibhor 050404: End of Code*/
};



/////////////////////////////////////////////////////////////////////////////////////////
// stevev 13jul10 - try the new rules, re-entrant string builder
// Note: the trail is in reverse order with the first being the final resolution item
// we traverse to the end and then build up the string on the way back out
// The trail is populated when the reference is first resolved so it must be resolved before
// calling this function.  We just re-follow rthe trail here.
// Note 16sept10 that this is built against the proposal of 15sep10 and may 
//      not match the final solution.
//04nov10 - The "" empty string rules were thrown out and "" is the same as never exists.
//
//  pgt			current group trail being worked
//  str			the string we are building (return payload)
//  strNOTmt	we have to handle the addition of a "" empty string<<<<<<<<<<< NOT <<<<<<<<<<<
//	entry		re-entrancy count
//
//  return true on success
//
// 01dec10 work to use empty and null as the same
// 29dec10 tagg the Help along with it to reduce redundancy
// 17dec13 rev 14 spec has bit enums with description only. 
//                It also does away with the "No Label/Help available" strings

//stringNOTempty : 1
//topWASreference: 1
bool hCreference::getFullLabel(hCvarGroupTrail* pgt,       wstring& str, 
							   LabelBitEnum_t& LabelFlags, wstring& hstr, int entry)
{
//enable 'em    return false; // disable newest techniques	
	itemType_t thisItmType = iT_NotAnItemType;
	/* entry is zero only in the very first call */
	bool isBotm = (entry == 0); // first in the list...entry is zero on first entry only
	bool is_Top = false;        // last  in the list
	bool is_Sec = false;	    // second to top(SourceTrail is Top)
	wchar_t buff[30];
	wstring itmLabel;			// a local work string
	wstring itemHelp;			// a local work string
	hCddlString tmpStr(devHndl()); // used to getStandardStr ONLY

	if ( pgt == NULL )  return false;// has to resolve first

	// 23nov11 - the dd working group made collection references on menues be handled totally
	//		differently from normal references. According to Paul - today - the fall back is
	//		treat them as regular references if the exception isn't valid
	bool haveLabel = false, haveHelp = false;
	if (myCollectionPtr!=NULL && isBotm)//exception handled first-only valid on the first level
	{
		hCgroupItemDescriptor* plGID = NULL;
		if ( myCollectionPtr->getByIndex(myMemberIndex, &plGID) == SUCCESS && plGID != NULL)
		{
			str = (wstring)(plGID->getDesc());
			if ( ! str.empty() )
			{
				haveLabel = true;
				LabelFlags.stringNOTempty = true;// added 27dec11 stevev
			}
			hstr = (wstring)(plGID->getHelp());
			if ( !hstr.empty() )
			{
				haveHelp = true;
			}
		}

		DBGLOGIF( LBLTRACE )(CLOG_LOG,"We are a collection member.\n");
		if ( haveLabel && haveHelp )
		{
			return true;
		}
	}

	// traverse to the end
	// if more in the string, re-enter
	if ( pgt->pSourceTrail != NULL )// there is a source
	{
		is_Sec = (pgt->pSourceTrail->pSourceTrail == NULL);// end of the line

		/// stevev 30jan11
	//	if (is_Sec && pgt->itmPtr != NULL && pgt->itmPtr->getIType() == iT_Variable 
		if ( pgt->itmPtr != NULL && pgt->itmPtr->getIType() == iT_Variable 
			&& ((hCVar*)pgt->itmPtr)->VariableType() == vT_BitEnumerated )
		{
			//LOGIT(CLOG_LOG,"BitEnum Label fetch\n");
		}
		DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s1 DoSource.\n", S_pace(entry));
		if (! getFullLabel(pgt->pSourceTrail, str, LabelFlags, hstr, entry+1) )//<<<<<<<<<<<<<<
		{
			DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s2 SourceFalse-returning.\n", S_pace(entry));
			return false;
		}
		// else success, fall thru to handle this level
		DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s3 postSource (%s).\n",
													S_pace(entry),((is_Sec)?"isSec":"NOTsec"));
	}
	else // pSourceTrail is NULL
	if ( ! isRef && isBotm )// we have a simple top level reference; use the simple answer
	{
		if (! haveLabel)
		{
			str = pgt->lablStr;// added 30nov10 stevev - to fill empty menu labels
			haveLabel = true;
		}
		if (! haveHelp)
		{
			hstr= pgt->helpStr;// added 29dec10 stevev
			haveHelp = true;
		}
		if (str.size()) 
			LabelFlags.stringNOTempty = true;// added 30nov10 stevev

		DBGLOGIF(LBLTRACE)(CLOG_LOG,L"%s4 No SourceTrail-returning.(lablStr=%s)(helpStr=%s)\n",
													wSpace(entry),str.c_str(),hstr.c_str());
		return true;
	}
	else // detect leftmost reference:
	if (pgt->pSourceTrail == NULL && pgt->pLookupTrail == NULL && pgt->pExprTrail == NULL)
	{// we're there, start back up
		return true;// nothing happens here
	}
	else  // no source trail and not top level - this should not occur
	{
		DBGLOGIF(LBLTRACE)(CLOG_LOG,"%s4 No SourceTrail-but is a reference.Error condition!\n", 
																				S_pace(entry));
		/* the great and mighty said they don't like this...missing strings must be blank 
							//3=[default_label]='No Label Available'
		str = tmpStr.getStandardStr("default_dict_entries",3);
		hstr= tmpStr.getStandardStr("default_dict_entries",2);
		LabelFlags.stringNOTempty = true;// added 30nov10 stevev
		***/
		str = L"";
		hstr= L"";
		LabelFlags.stringNOTempty = false;// added 30nov10 stevev
		return true;
	}


#ifdef USE_TRAIL_LABEL
	itmLabel = pgt->pSourceTrail->lablStr;
#else
	pgt->pSourceTrail->itmPtr->Label(itmLabel);
	pgt->pSourceTrail->itmPtr->Help (itemHelp);
#endif
	// if itmLabel.empty() "label not available" frm dictionary

	thisItmType = pgt->pSourceTrail->itmPtr->getIType();

	if ( is_Sec )// we won't be recursing
	{// check for reference group on top
		if ( thisItmType == iT_ItemArray || thisItmType == iT_Collection )
			// stevev 18dec13 - i would expect iT_Array & iT_List to be here too
		{
			DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s5 handling top Group IS reference.\n", 
																				S_pace(entry));
			LabelFlags.topWASreference = 1;//true
		}
	}

	switch ( thisItmType)
	{
	case iT_Variable:
		{
			if ( ((hCVar*)pgt->pSourceTrail->itmPtr)->VariableType() == vT_BitEnumerated )
			{
				DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sv this is a bit reference.\n", S_pace(entry));
				// note that thr resolution of a bit reference will fill the label, help as
				//		well as the description label and help when any of them exist. It is
				//		up to this routine to fill the label and help as correctly as it can.
#ifdef _DEBUG
				if ( ! isBotm )
				{
				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: enum reference that is not on the bottom!\n");
				}
#endif
				/* as of spec 500 r14 18dec13 preliminary, these are only from the bit */
				if (isBotm)
				{
					str  = pgt->containerDesc;
					hstr = pgt->containerHelp;
				}
/**** 18dec13 - remove the old---
				// if it's is_Sec, it'll have hstr empty
				if (hstr.empty())
				{// either no prefix or this is a simple bit-reference
					hstr = itemHelp;// the source help (ie the variable's help)
				}
				if (! hstr.empty() )
				{
					hstr = hstr + L" ";
				}
				if (! pgt->containerHelp.empty()) // desc help available
				{
					hstr = hstr + pgt->containerHelp;// the help of the selected bit
				}
				//else, if still empty, it'll fall thru and get a 'No Help Available' @ exit


				// bit refs MUST be at the bottom, treat an empty string as a ONLY bit ref
				if ( LabelFlags.stringNOTempty )// not the only reference
				{
					str += L" ";
				}
				//31jan12 bit enum refs are Label + Desc if either exist, NLA otherwise 
				else
				{
					str = pgt->lablStr;
					if ( ! str.empty() )
					{
						str += L" ";
					}
				}// end 31jan12 addition

				if (! pgt->containerDesc.empty())
				{
					str += pgt->containerDesc;
				}
				//31jan12 if neither exist, let the end-processing give it NO LABEL AVALIABLE
				//else
				//{// it may be empty but its as good as it gets
				//	str += pgt->lablStr;
				//}
				***/
				if (! str.empty() )					
					LabelFlags.stringNOTempty = true;
			}
			// else leave it alone
		}
		break;

	case iT_File:
	case iT_ItemArray:
	case iT_Collection:
		{
			DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s6 this Group is reference.\n", S_pace(entry));

			/**** handle help strings here ****/
			// acquire state abbreviations
			unsigned uHelpSt = 0;
			if (is_Sec)
			{ uHelpSt += 4; } // else leave alone
			if (isBotm) 
			{ uHelpSt += 2; } // else leave alone
			if (! hstr.empty()) 
			{ uHelpSt += 1; } // else leave alone
			// we have states = 0 to 7
			if ( uHelpSt == 7 || uHelpSt == 5 )// STATS 21 - 24 && 29 - 32 (see note eof)
			{// top level and we already have a prefix - should never happen
				DEBUGLOG(CLOG_LOG,L"ERROR:Help has content at the top of the reference!"
					L"(clearing '%s')\n",hstr.c_str() );
				hstr = L""; // clear the string
			}
			if ( uHelpSt == 0 || uHelpSt == 4 || uHelpSt == 5 )
			{// top or middle with no prefix yet
				if ( ! pgt->containerHelp.empty() )
				{				
					hstr = pgt->containerHelp;
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%s6h Prefix Reference '%s' (%d).\n", 
														wSpace(entry),hstr.c_str(),uHelpSt);
				}// no-op, leave the prefix empty
			}
			else
			if ( uHelpSt == 3 )
			{// bottom with a prefix
				if ( ! pgt->containerHelp.empty() )
				{
					hstr = hstr + L" ";
					hstr = hstr + pgt->containerHelp;
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%s6h Suffix added '%s' (%d).\n", 
											wSpace(entry),pgt->containerHelp.c_str(),uHelpSt);
				}// no-op - use prefix only
			}
			else
			if ( uHelpSt == 2 || uHelpSt == 6 || uHelpSt == 7 )
			{//bottom or only ref but have no prefix; use simple-reference rules				
				if ( ! itemHelp.empty() )// simple two or four
				{
					hstr = itemHelp;  // the last container's help 
				}
				if ( ! pgt->containerHelp.empty() )// simple three or four
				{// element's help
					if (!  hstr.empty() )
						hstr = hstr + L" ";
					hstr = hstr + pgt->containerHelp;
				}
				if ( pgt->containerHelp.empty() && itemHelp.empty() )// simple one
				{
					if (pgt->itmPtr != NULL)
					{
						pgt->itmPtr->Help(hstr);
					}
					if (hstr.empty())// the resolved has no help
					{
						/* great and might says: "make it empty" 
						hstr = tmpStr.getStandardStr("default_dict_entries",2);// no help
						*/
						hstr = L"";
					}
				}
			}
			else
			{// middle reference, we have a prefix, no-op, keep looking
			}
			/* * * finished help * * */



			/* handle the single reference exception first*/
			if ( isBotm && is_Sec ) // = isOnly
			{
				DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sx this is the only reference.\n", S_pace(entry));
				bool havesrclabel = (pgt->pSourceTrail->lablStr.size() >  0 );// not empty
				bool haveelemdesc = (pgt->containerDesc.size() >  0);// not empty
				if (havesrclabel) // haveSrcLabel
				{
					str = itmLabel;
					if (haveelemdesc) //haveDescription
					{
						str += L" ";
						str += pgt->containerDesc;
					}// else don't attach anything
					LabelFlags.stringNOTempty = 1;
				}
				else
				if (haveelemdesc) //haveDescription without label
				{
					str = pgt->containerDesc;
					LabelFlags.stringNOTempty = 1;
				}
				else // we have neither
				if ( pgt->pLookupTrail != NULL && pgt->pLookupTrail->lablStr.size() > 0 )
				{
					str = pgt->pLookupTrail->lablStr;					
					LabelFlags.stringNOTempty = 1;
				}
				else // there are no strings - get 'No Label Available'
				{
					DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sx this single reference has no strings.\n", 
																			   S_pace(entry));
					/* great and might says: "make it empty"
					str = tmpStr.getStandardStr("default_dict_entries",3);//No Label Available
					LabelFlags.stringNOTempty = 1;// added 01dec10 stevev
					**/
					str = L"";
					LabelFlags.stringNOTempty = 0;// added 01dec10 stevev
				}

/* all help calcs are done at top...leave this here awhile in case the new stuff doesn't work
				// over ride the earlier help fill with exception fill	
				hstr = L"";// we're usng the exception rules here
				bool havesrcHelp = (pgt->pSourceTrail->helpStr.size() >  0 );// not empty
				bool havelemhelp = (pgt->containerHelp.size() >  0);// not empty
				if (havesrcHelp)
				{
					hstr = pgt->pSourceTrail->helpStr;					
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%sxh Source help = '%s'\n",wSpace(entry),hstr.c_str());
				}
				if (havelemhelp)
				{
					if (! hstr.empty() )
						hstr = hstr + L" ";
					hstr = hstr + pgt->containerHelp;					
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%sxh Elem help = '%s'\n",wSpace(entry),hstr.c_str());
				}
				if (hstr.empty())
				{
					int rc;
					hCitemBase *pItemBase = NULL;
					rc = resolveID(pItemBase);
					if (rc==SUCCESS && pItemBase != NULL)
					{
						pItemBase->Help(hstr);											
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%sxh Item help = '%s'\n",wSpace(entry),hstr.c_str());
					}
				}
				// if (hstr.empty())...we'll fill it before exiting
stevev 09aug11  *********/
			}
			else// leave the help as set earlier
			if ( LabelFlags.stringNOTempty )// not the top level
			{
				if (isBotm)// this is the bottom level reference (7&8)
				{
					if ( pgt->containerDesc.size() > 0 )
					{	// desc string exists  
						DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s7 this bottom reference.group has a "
										" container Description (%s).\n",
										S_pace(entry),TStr2AStr(pgt->containerDesc).c_str());
						if (str.length())// prevent leading spaces
						{
							str += L" ";
						}
						str += pgt->containerDesc;// space cat desc string to string
					}
					// else - nothing is added
				}
				else// not bottom level
				if (thisItmType == iT_Collection)//  (3)
				{
					;// nothing is added to the string in this condition
				}
				else // not bottom level, must be iT_ItemArray (4-6)
				if (LabelFlags.topWASreference)// 4
				{
					DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s8 reference.array had a reference top(no-op).\n", 
																			S_pace(entry));
					;// nothing is added to the string in this condition
				}
				else // (5 )
				{
					DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%s9 this reference.group is top (Label %s).\n", 
															wSpace(entry),itmLabel.c_str());
			//		if (itmLabel.length() > 0 )	//group-label exists           
			//		{
			//			str += L" ";
			//			str += itmLabel;		// add array label to string
			//		}
					//always
					// add "[" index "]" to string
					str += L" [";
					str += _itow(pgt->containerNumber, buff, 10);
					str += L"]";						
				}
			}
			else // label string is empty (1&2)
			{						
				DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sA this reference.group has an empty group string.\n",
									S_pace(entry));
				if ( pgt->containerDesc.size() > 0) 			
				{	// desc string exists  
					str += L" ";
					str += pgt->containerDesc;// space cat desc string to string		
					LabelFlags.stringNOTempty = 1;
				}				
				// else - nothing is added (empty string)
			}
			if ( str.length() > 0 || LabelFlags.stringNOTempty )
			{
				LabelFlags.stringNOTempty = 1;
			}
/* first implementation::>
			if (! pgt->containerDesc.empty() )
			{// element description, 
				DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s7 this reference.group has a container Description (%s).\n", 
					S_pace(entry),TStr2AStr(pgt->containerDesc).c_str());
				if (str.length())
				{
					str += L" ";
				}
				str += pgt->containerDesc;
			}
			else
			{//if missing then ->>label[index]
				if ( is_Top )//first
				{
					DBGLOGIF( LBLTRACE )(CLOG_LOG,"%s8 this reference.group is top (Label %s).\n", 
						S_pace(entry),TStr2AStr(itmLabel).c_str());
					str += itmLabel;
				}
				str += L"[";
				str += _itow(pgt->containerNumber, buff, 10);
				str += L"]";
			}
****/
		}
		break;
/*	case iT_Collection:
		{
			if ( pgt->containerDesc.empty() )
				break; //desc is empty ->> add nothing

			if ( isBotm )
			{//if last:: member desc 
				if (str.length())
				{
					str += L" ";
				}
				str += pgt->containerDesc;
			}
			else
			if ( is_Top )
			{// if first:: member desc
				str += pgt->containerDesc;
			}
			//else neither -> add nothing 
		}
		break;
**/
	case iT_Array:	// these are both
	case iT_List:	// handled the same
		{
			DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sB this Group is typed.\n", S_pace(entry));	
			if ( isBotm  && ( is_Sec || hstr.empty()) )
			{// only reference or final reference with no prefix
			   DEBUGLOG(CLOG_LOG,L"%s",(!hstr.empty())?L"ERROR:typed Help has content!\n":L"");
				hstr = itemHelp; // source help
				DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%s6g Final Reference '%s'.\n", 
																  wSpace(entry),hstr.c_str());
			}
			else
			{// not final or final with a prefix
				if (! isBotm )// not final 
				{
					if (hstr.empty())// and haven't found a prefix yet
					{
						hstr = itemHelp;// the help of the source item
						DBGLOGIF( LBLTRACE )(CLOG_LOG,L"%s6h Prefix Reference '%s'.\n",  
																  wSpace(entry),hstr.c_str());
					}
					// else not final and have a prefix, just skip it
				}
				else //  is final, must have a prefix
				{
					//>>>>>>>>>>>>>>>>>>>>>>>>>>>>> tentative <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					// may change depending on interpretation of row 1 in table 30
					hstr = hstr + L" ";
					hstr = hstr + itemHelp;//<<<<< assumed source  help <<<<<< may change
					// it may be the final psuedo item help...
				}
			}


			if ( str.empty() )
			{
				if (itmLabel.empty())
				{
					/* great and might says: "make it empty"
					// get dictionary string...3=[default_label]='No Label Available'
					str = tmpStr.getStandardStr("default_dict_entries",3);
					**/
					str = L"";
				}
				else
				{
					str = itmLabel;
				}
			}// else just add the index

			// +[index value]
			str += L" [";// space concatenate
			str += _itow(pgt->containerNumber, buff, 10);
			str += L"]";		 
			//TODO: deal with empty (no-op) and non-existent label string on top	
			LabelFlags.stringNOTempty = 1;
		}
		break;
	default:
		{// empty 
			;//- add nothing
		}
		break;
	}// endswitch
	DBGLOGIF( LBLTRACE )(CLOG_LOG,"%sC this String Is:%s.\n", S_pace(entry),TStr2AStr(str).c_str());

	if ( isBotm && ! LabelFlags.stringNOTempty )//almost done and nothing found
	{
		if ( pgt->itmPtr != NULL )
		{
			pgt->itmPtr->Label(str);
			if (str.empty())
			{/* great and might says: "make it empty"
			 // get dictionary string...3=[default_label]='No Label Available'
				str = tmpStr.getStandardStr("default_dict_entries",3);
				**/
				str = L"";
			}
			LabelFlags.stringNOTempty = true;
		}
	}
	if ( isBotm && hstr.empty() )//almost done and nothing found
	{	/* great and might says: "make it empty"
		hstr = tmpStr.getStandardStr("default_dict_entries",2);// no help
		**/
		hstr = L"";
	}

	return true;
}


RETURNCODE hCreference::getItemHelp(wstring & strLabel)
{
	return FAILURE;
}


/*Vibhor 243004: Start of Code*/
/*Note: this code has simply been moved from Doc to this place*/
// called from CDDLMenu::GetMenuItemList()
// valid label for an empty label that is supposed to be empty
RETURNCODE hCreference::getItemLabel(wstring &strLabel, bool& validLabel, wstring &strHelp)
{

	RETURNCODE rc = SUCCESS;
	wstring strTemp, dummyHelp;
	hCitemBase *pItemBase = NULL;


	rc = resolveID(pItemBase);

	if(rc == SUCCESS && NULL != pItemBase)
	{
		strTemp   =
		dummyHelp = L"";

		/* stevev - try to get generic labeling working */
		LabelBitEnum_t strFlags;// actually not-empty

		if (getFullLabel(getGroupTrailPtr(), strTemp, strFlags, dummyHelp))
		{
			validLabel = strFlags.stringNOTempty;
		}
		else
		{// do it the previous way
	
		if (getRefType() == rT_via_Collect  ||	getRefType() == rT_viaItemArray )
		{	// rules
			// go down the pSource till the next is empty
			// for each if desc not empty, overwrite current
			// when done take what was found and add a space and this desc
			hCvarGroupTrail* pVGT = getGroupTrailPtr()->pSourceTrail;// our first ref
			while ( pVGT != NULL )
			{
				if (pVGT->containerDesc.size() > 0) //!...empty()) 
				{// overwrite
					strTemp = pVGT->containerDesc;
				}// if it's empty, NO ACTION
				pVGT = pVGT->pSourceTrail;
			}
			if ( !(getGroupTrailPtr()->containerDesc.empty()) && !(strTemp.empty()))
			{
				strTemp += L" ";// delimiter, 275 uses a space, wally wanted a dot
						//                            too bad Wally...
			}// else we have nothing to separate from
			if ( ! getGroupTrailPtr()->containerDesc.empty() )
			{
				strTemp += getGroupTrailPtr()->containerDesc;
			}
			rc = SUCCESS;
		}
		if ( strTemp.empty() ) // ended up with nothing (or it ain't groupItem material)
		{
			rc = pItemBase->Label(strTemp);
		}
			
		}// end else - do it the previous way

/* stevev 04aug06 - changed the below to the above **************************		
			strTemp = getGroupTrailPtr()->pSourceTrail->containerDesc;
			if ( (! (strTemp.empty())) && strTemp != " " )
****** end   stevev 04aug06 ***********************************************/
    	strLabel = strTemp;
		strHelp  = dummyHelp;
	}
	else
	{
		strLabel = L" "; // This should never be reached !!!!!
		clog<<"getItemLabel : Couldn't resolve the reference ... returning blank label"<<endl;

	}

	return rc;


}/*end getItemLabel()*/


// recursive name generator
// we traverse to the bottom of the source trail 
// and then build the name string on the way out
wstring hCreference::nameFromTrail(hCvarGroupTrail* pVGT, trailList_t& trailmap)
{
	wstring locStr, tmpStr;
#if 0 // this needs to be totally redesigned to give the correct results-----------------------
	hCvarSrcTrail thisTrail;
	wchar_t indexStr[32];// to use to sprintf into
	int y = trailmap.size();// index of last is (y-1)
	//                         index of thisTrail is (will be) y
	bool endOfTrail = false;

	
	if ( pVGT->itmPtr == NULL )
	{// we have issues
		return locStr;//////////// error return
	}
	thisTrail.srcType   = pVGT->itmPtr->getIType();
	thisTrail.srcID     = pVGT->itmID;
	thisTrail.srcPtr    = pVGT->itmPtr;
	thisTrail.srcIdxVal = pVGT->containerNumber;
	if ( y > 0 )
	{// copy last from the last
		thisTrail.lastDescStr = trailmap[(y-1)].lastDescStr;
	}
	// the description is one level down from the source (at the lookup)
	// so just put it in reguardless of the type
	if (! pVGT->containerDesc.empty() && thisTrail.lastDescStr.empty())
	{// we're going down, we have a desc and there are no others 'after' us
		thisTrail.DescStr = pVGT->containerDesc;
	}

	if (thisTrail.srcType == iT_Collection || thisTrail.srcType == iT_File || 
		thisTrail.srcType == iT_ItemArray )
	{	
		if ( y > 0 )
		{// set the one behind us with parent type 
			trailmap[(y-1)].parentIsDescGrp = true;
		}
		// else we are the first element and this is an illegal reference
	}
	else
	//if (thisTrail.srcType == rT_via_Array || thisTrail.srcType == rT_via_List )
	if (thisTrail.srcType == iT_Array || thisTrail.srcType == iT_List )
	{
		thisTrail.isIndxGrp = true;
		// if we don't have a descriptor above, we'll need a label
		thisTrail.srcPtr->Label(thisTrail.LabelStr);
	}
	else // it's neither so we may need the label
	{
		thisTrail.srcPtr->Label(thisTrail.LabelStr);
	}

	trailmap.push_back(thisTrail);
	if (pVGT->pSourceTrail != NULL)
	{
		locStr = nameFromTrail(pVGT->pSourceTrail,trailmap);// curse again
	}
	else //this is the end of the trail, time to start back
	{
		endOfTrail = true;
	}

	// when we get here we're on our way out of the stack (could be at the end of the trail)
	/* some notes:
	 *   if we are a desc group, the lookup info will be one down (y-1) eg index & desc string
	 *	 we never use a group's label
	 *   we only use a final item's (usually a variable) label if there is no desc higher
	 *   we own the trail so we can pretty much do whatever we want
	 * new algorithm - set string for where we are then deal with next one down, erasing
	 *		strings as required
	 */

	// Short - circuit the common condition
	if ( thisTrail.DescStr.empty() && thisTrail.LabelStr.empty() && thisTrail.srcIdxVal == -1)
	{// normal occurance at endOfTrail
		return locStr;
	}// else keep working
	/*** step one: add the strings as required ***/
	if (!thisTrail.DescStr.empty())
	{
		tmpStr = thisTrail.DescStr;
		if ( y > 0 )
		{
			trailmap[(y-1)].haveDesc = true;
		}
	}
	else
	//if(!thisTrail.LabelStr.empty() && ! thisTrail.haveDesc )	
	if(!thisTrail.LabelStr.empty() && locStr.empty() )
	{
		tmpStr = thisTrail.LabelStr;
	}
	else
	{// both strings are empty
		tmpStr = L"";
	}
	if ( thisTrail.haveDesc && y > 0)
	{// pass it on
		trailmap[(y-1)].haveDesc = true;
	}

	if (thisTrail.srcIdxVal != -1 && ( !endOfTrail && trailmap[(y+1)].isIndxGrp) )
	{// see if the index is needed
		// add index 
		swprintf( indexStr, L"[%d]", thisTrail.srcIdxVal );
		tmpStr += indexStr;
	}

	/*** step two: clear unwanted strings in the next level down ***/
	if ( y != 0 )
	{// skip intermediate descriptions & set the isDesc in the next one down

		if ( thisTrail.isDescGrp )
		{// we are a desc-group -- our desc is in the next one down (the one it describes)
			// if previous has a desc string (its parent had to be a descgrp) and
			//    next isa desc group& has a string, skip this desc (in next record)
			if ( !thisTrail.DescStr.empty()  &&  // and the next is a desc				
				( y >= 2 && trailmap[(y-1)].isDescGrp && !(trailmap[(y-2)].DescStr.empty()) ) )
			{//clear ours
				trailmap[(y-1)].DescStr.erase();
			}
			//else - leave it alone
		}
	}
	// else we're on the bottom, there is no next down so do nothing

	if ( ! tmpStr.empty() )
	{
		if (! locStr.empty() )
			locStr = locStr + L" ";
		locStr = locStr + tmpStr;
	}
#endif // the above needs a 100% redesign before it called from the getlabel function----------
	return locStr;
}
 /* should never be needed..... stevev - WIDE2NARROW char interface
 RETURNCODE hCreference::getItemLabel( string & strLabel)
 {
	 wstring locStr, helpStr;
	 bool retb;
	 RETURNCODE rc = getItemLabel(locStr, retb, helpStr);
	 strLabel = TStr2AStr(locStr);
	 return rc;

 }
....*/

/*Vibhor 243004: End of Code*/

/* stevev 25oct04 - new for eddl - bit-enum and array should be the same here */
RETURNCODE hCreference::resolveExpr(CValueVarient& retValue)
{	
	RETURNCODE rc = SUCCESS;
	hCexpressionTrail* pExprTrail = NULL;

	if ( pExpr == NULL )
	{ 
		retValue.clear();
		rc = APP_RESOLUTION_ERROR;
	} 
	else
	if ( getRefType() == rT_via_Attr )
	{
		itemID_t      refVal = 0;
		CValueVarient attrNum;
		
		if ( isRef && pExpr != NULL && pExpr->expressionL.size() > 0)
		{	
			// stevev 22aug06 - changed technique to get # and which 
			unsigned which = 0; 
			if (pExpr->expressionL.begin()->exprElemType == eet_INTCST )
			{
				attrNum = pExpr->expressionL.begin()->expressionData;
				which   = (unsigned)(pExpr->expressionL.begin()->dependency.which);
									// already shifted..30aug06 >> 16;// it's in the upper word
			}
			else
			{// incorrect encoding of attribute reference
				
				LOGIT(CERR_LOG|CLOG_LOG,"Incorrect Coding of Attribute-Reference.\n"); 
				rc = FAILURE;
			}
			// was pre 22aug06::  attrNum = pExpr->resolve(&pExprTrail,false);
			
			if ( rc == SUCCESS && attrNum.vIsValid  && pRef != NULL ) 
			{
				rc = SUCCESS; 
				#ifdef DBG_REFTRAIL
				if (pRef->isaRef())
				{
					clog<<    "------- reference Expression via_attribute isaRef "
								"w/ id = 0x"<<hex << ((int)pRef->getID()) <<dec;
				}
				clog<<"          with a pRef (resolving)" << endl;	
				#endif	
				rc = pRef->resolveID(refVal);
				hCvarGroupTrail* pRefGrpTrail = pRef->getGroupTrailPtr();

				if ( rc == SUCCESS  && pRefGrpTrail != NULL && pRefGrpTrail->itmPtr != NULL)
				{// we have an itemID of the Item we are targetting
					if (devPtr() == NULL)
					{
					   LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No device to dereference with.\n");
						rc = FAILURE;
					}
					else
					{	
						CValueVarient rV =
					    pRefGrpTrail->itmPtr->getAttrValue((unsigned long)attrNum, which);	
						if ( rV.vType == CValueVarient::isSymID )
						{// we have a returned member - not available here ( resolveID() )
							retValue.clear();
							rc = FAILURE;
						}
						else
						{// everything else is a value/constant return
							retValue = rV;
							rc = SUCCESS;	
						}
					}// endelse - have a device
				}
				else
				{// the reference could not be resolved with a trail
					LOGIT(CERR_LOG,"Ref Expression Failure Attr 1.\n");
					rc = APP_PROGRAMMER_ERROR;
				}
			}
			else 
			{
				LOGIT(CERR_LOG,"Ref Expression Failure Attr 2.\n");
				rc = FAILURE;
			}

		}
		else
		{// error
			LOGIT(CERR_LOG,"Ref Expression Failure Attr 3.\n");
			rc = FAILURE;
		}
	}
	else
	{
		retValue = pExpr->resolve(&pExprTrail,false);
		if ( retValue.vIsValid ) rc = SUCCESS; else rc = FAILURE;
	}
	return rc;
}

RETURNCODE hCreference::fileWrt(hCformatData*  pData)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient   vvV;
	hCitemBase* piB = NULL;
	
	if ( pData == NULL ) 
		return APP_PARAMETER_ERR;

	if ( pExpr != NULL )
	{
		rc = resolveExpr(vvV);
		if (rc == SUCCESS && vvV.vIsValid)
		{
			if (vvV.vType == CValueVarient::isIntConst)
			{
				pData->pFormat->insertInt(pData,(int)vvV,"referenceIndex");
			}
			else
			if(vvV.vType == CValueVarient::isString)
			{
				pData->pFormat->insertStr(pData,(string)vvV,"referenceIndex");
			}
			else
			{
				pData->pFormat->insertStr(pData,"Unknown Resolution","referenceIndex");
			}
		}
		else
		{
			pData->pFormat->insertStr(pData,"Resolution Failure","referenceIndex");
		}
	}
//	if ( pRef  != NULL && isRef )
//	{
//		rc = pRef->resolveID( piB );
//		if ( rc == SUCCESS )
//		{
//			return (pData->pFormat->insertObject(pData, piB));
//		}
//		else
//		{
//			return (pData->pFormat->insertStr(pData,"Resolution Failure","reference"));
//		}
//	}
 
	rc = resolveID( piB , false );// self  --  27aug07- reduce string leak,don't make str
	if ( rc == SUCCESS && piB != NULL )
	{
		return (pData->pFormat->insertObject(pData, piB));
	}
	else
	{
		return (pData->pFormat->insertStr(pData,"Resolution Failure","non-reference"));
	}
}


/* stevev 3jul06 - added to streamline acquisition of dependency info */	
/*    addVars is true when the value of the resolved reference counts (like in an expression)*/
RETURNCODE hCreference::aquireDependencyList(ddbItemList_t& retItemIDs, bool addVars)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase*     pItem = NULL;
	referenceType_t rt = getRefType();
	ddbItemList_t   localList;
	int             lstSz = 0;

	hCdependency*   pValidLst = NULL;

/*	4 via, adds expr deps to ref deps [ TODO: add all possible resolution vars ]*/

	if ( ! isRef || ( isRef && id != 0) )
	{// oddball direct reference
		if ( rt == rT_Separator || rt == rT_Constant || rt == rT_Row_Break )
		{
			DBGLOGIF( LBLTRACE )(CLOG_LOG,"Odd Direct_Reference to a non-existant item 0x%04x\n",id);
			return -9;
		}
		/*	4 direct, adds item's validity dependencies to (if var:item)        */
		rc = devPtr()->getItemBySymNumber(id, &pItem);
		if ( rc == SUCCESS && pItem != NULL )
		{//make it look right
			rt = id2refTbl[pItem->getIType()];
		}
		else
		if ( rt != rT_Separator && rt != rT_Constant && rt != rT_Row_Break )
		{
			LOGIT(CLOG_LOG,"Odd Direct Reference to an item that does not exist 0x%04x\n",id);
			return -16;
		}
	}
	// else do the function

	switch (rt) 
	{
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Array_id:
	case rT_Item_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Image_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{
			/*  outputList += itemValidityList + (isVar)?item:nothing  */
			rc = devPtr()->getItemBySymNumber(id, &pItem);
			if ( rc == SUCCESS && pItem != NULL )
			{
				pValidLst = pItem->getDepPtr(ib_ValidityDep) ;
				if (pValidLst != NULL )
				{
					lstSz = pValidLst->fillValidityList();// short if nothing to do
					if (lstSz > 0)
					{
						if LOGTHIS( LOGP_DEPENDENCY )
						{
							LOGIT(CLOG_LOG,"DEP: Reference: Direct item ID with Validity."
								" 0x%04x\n",id);
							for ( ddbItemLst_it p2p2IB = pValidLst->iDependOn.begin();
											p2p2IB != pValidLst->iDependOn.end(); ++p2p2IB)
							{
							LOGIT(CLOG_LOG,"                0x%04x ",(*p2p2IB)->getID());
							}
							LOGIT(CLOG_LOG,"\n");
						}
						hCdependency::insertUnique(retItemIDs,pValidLst->iDependOn);
						//retItemIDs.insert(retItemIDs.end(),
						//	pValidLst->iDependOn.begin(),pValidLst->iDependOn.end());
					}// else - nothing to do
				}
				// addVars is false when it is an attribute reference
				if ( pItem->IsVariable() && addVars)
				{// vars may change value, add var itself
					hCdependency::insertUnique(retItemIDs,pItem);
					//was retItemIDs.push_back(pItem); 
					pItem->setReallyCritical(addVars);//stevev 23feb07 filter truely critical
				}
				if LOGTHIS( LOGP_DEPENDENCY )
				{
					LOGIT(CLOG_LOG,"DEP: Reference: Direct item ID 0x%04x '%s' (a %s)\n",
						id,pItem->getName().c_str(), pItem->getTypeName());
				}

				rc = SUCCESS;
			}
			else
			{// error exit
				LOGIT(CLOG_LOG,"Direct Reference to an item that does not exist 0x%04x\n",id);
				rc = -14;
			}
		}
		break;
		

	case rT_Record_id:
	case rT_VarList_id:
	case rT_Block_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
		{
			LOGIT(CLOG_LOG,"Not getting dependency for non-HART item 0x%04x\n",id);
			rc = -18;
		}
		break;
	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
		{
			LOGIT(CLOG_LOG,"Not getting dependency for non-HART via item \n");
			rc = -20;
		}
		break;
	case rT_viaItemArray:	/* item array reference */
	case rT_via_Collect:	/* collection reference */
	case rT_via_Array:			/* profibus array reference */
	case rT_via_File:
	case rT_via_List:
	case rT_via_BitEnum:
	case rT_via_Chart:
	case rT_via_Graph:
	case rT_via_Source:
	case rT_via_Attr:
	case rT_via_Blob:
		{
	/*	4 via, adds expr deps to ref deps [ TODO: add all possible resolution vars ]*/
			if (pRef == NULL )
			{	
				LOGIT(CERR_LOG|CLOG_LOG,
					"Ref Failure(dependVars): compound ref type without reference pointer.\n");
				rc = -13;
			}
			else
			if (pExpr == NULL)
			{	
				LOGIT(CERR_LOG|CLOG_LOG,
				   "Ref Failure(dependVars): compound ref type without expression pointer.\n");
				rc |= -12;
			}
			else
			{/* outputList += (reflist + expressionlist) */
				if LOGTHIS( LOGP_DEPENDENCY )
				{
					LOGIT(CLOG_LOG,"DEP: Reference: via Item.\n");
				}

				rc = pExpr->aquireDependencyList(retItemIDs, true);
				rc |= pRef->aquireDependencyList(retItemIDs, false);
					// stevev 06sep06
					// changed the ref acquire to NEVER ...was::>, addVars);
					// This has to be a container of some sort 
					//     (even a variable containing an attribute)
					//  sot it's VALUE will NEVER impact the outcome.

				if ( addVars )
				{// add the resolved VARIABLES
					rc = resolveAllIDs(localList);
					if ( rc == SUCCESS && localList.size() > 0 )
					{
						ddbItemLst_it iLi;
						for ( iLi = localList.begin(); iLi != localList.end(); ++iLi)
						{
							pItem = (*iLi);
							if ( pItem != NULL && pItem->IsVariable() )
							{	// add its validity dependency								
								pValidLst = pItem->getDepPtr(ib_ValidityDep) ;
								if (pValidLst != NULL )
								{
									LOGIF( LOGP_DEPENDENCY )(CLOG_LOG,"DEP: Reference: "
												"via Item's resolved Variable's Validity.\n");
									lstSz = pValidLst->fillValidityList();// in case
									if (lstSz > 0)
									{
										hCdependency::insertUnique(retItemIDs,
																		pValidLst->iDependOn);
									}// else - nothing to do
								}// else - nothing to do
								// add its self (value)
								LOGIF(LOGP_DEPENDENCY)(CLOG_LOG,"DEP: Reference: "
										"via Item's resolved item 0x%04x.\n",pItem->getID());
								hCdependency::insertUnique(retItemIDs,pItem);
							}
						}// next resolved
					}// else - won't resolve
					else
					LOGIF(LOGP_DEPENDENCY)(CLOG_LOG,"DEP: Reference: "
										"via Item's No addVars item 0x%04x.\n",pItem->getID());
				}// else - don't do this part
			}
		}
		break;
	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
		{
			rc = -7;
		}
		break;
	case rT_Separator:
	case rT_Constant:
	case rT_Row_Break:
		{
			rc = -9;
		}
		break;

	default:
		{
			rc = -5;
		}
		break;
	}// endswitch

	return rc;
}


/*************** hCdepends **************************************************/
/*
 *
 *
 */
RETURNCODE hCdepends::dumpSelf(int indent, char* typeName)
{
	if      ( useOptionVal == aOp_dontCare )
	{
		//TEMPORARY
#ifdef _DBGCOND
		COUTSPACE << "(no dependency Option) " << endl;
#endif
	}
	else if ( useOptionVal == aOp_wantMax )	// MaxVal arithOption
	{
		LOGIT(CLOG_LOG," (MAX) \n");
	}
	else if ( useOptionVal == aOp_wantMin )	// MinVal arithOption
	{
		LOGIT(CLOG_LOG," (MIN) \n");
	}
	else
	{
		LOGIT(COUT_LOG, " (UNDEFINED dependency Option [%d]\n", (int)useOptionVal);
	}

	LOGIT(COUT_LOG,"%sWhich = 0x%04x\n",Space( indent+3 ),which);

#ifdef _DBGCOND
COUTSPACE << "+Depends Ref Start" << endl;
	depRef.dumpSelf(indent+3);// fprce a direct append
COUTSPACE << "-End Depends Ref" << endl;
#else
	depRef.dumpSelf(indent+3);// fprce a direct append
#endif
	return SUCCESS;
}

void hCdepends::setDuplicate(hCdepends& rSrc)
{
	useOptionVal = rSrc.useOptionVal;
	which        = rSrc.which;
	depRef.duplicate(&(rSrc.depRef), false, 0);// expressions are not dup'd
}

/*************** hCexpressElement *******************************************/
/*
 *
 *
 */

hCexpressElement& hCexpressElement::operator=(const hCexpressElement& srcElem)
{   exprElemType  = srcElem.exprElemType; 
	expressionData= srcElem.expressionData;
	hCdepends* pD = (hCdepends*)(&srcElem.dependency);// sjv 5jul06 - convert to non-const
	dependency    = *pD; return *this;
}

// returns negative (Index +1) when dependency reference
// returns a return code otherwise
RETURNCODE hCexpressElement::dumpSelf(int indent, char* typeName )
{
#ifdef _DBGCOND
	COUTSPACE << "+Start ExpressElement" << endl;
#endif
	if (expressionData.vType == CValueVarient::invalid )
	{	//COUTSPACE << "*Expression Data INVALID*";
		LOGIT(COUT_LOG,"   *Expression Data INVALID*\n" );
	}
	else
	if (expressionData.vType == CValueVarient::isDepIndex )
	{	
		if (dependency.useOptionVal == aOp_wantMax)
		{
			LOGIT(COUT_LOG," MAX_VALUE for");
		}
		else
		if (dependency.useOptionVal == aOp_wantMin)
		{
			LOGIT(COUT_LOG," MIN_VALUE for");
		}
		else
		if (dependency.useOptionVal == aOp_dontCare)
		{
			LOGIT(COUT_LOG," value of");
		}
		else
		{
			LOGIT(COUT_LOG," erroneous OptionVal");
		}
		
		if (dependency.which != 0)			
			LOGIT(COUT_LOG,"    which = %I64d\n",dependency.which);
		LOGIT(COUT_LOG,"\n");
		dependency.depRef.dumpSelf(indent+9);
	}
	else
	if(expressionData.vType == CValueVarient::isSymID )
	{
		LOGIT(COUT_LOG," Symbol Number 0x%04x\n",expressionData.vValue.varSymbolID);
	}
	else
	{ 	switch(expressionData.vType)
		{
		case CValueVarient::isBool:
			if (expressionData.vValue.bIsTrue)
				LOGIT(COUT_LOG," TRUE boolean");
			else
				LOGIT(COUT_LOG," FALSE boolean");
			break;
		case CValueVarient::isOpcode:
			LOGIT(COUT_LOG," Opcode %d is non-printable",(int)expressionData.vValue.iOpCode);
			break;
		case CValueVarient::isIntConst:
			LOGIT(COUT_LOG," Const Int %d (0x%04x)",
				   (int)expressionData.vValue.iIntConst,(int)expressionData.vValue.iIntConst);
			break;
		case CValueVarient::isFloatConst:
			if (expressionData.vIsDouble)
				LOGIT(COUT_LOG," Const Double %g",expressionData.vValue.fFloatConst);
			else
				LOGIT(COUT_LOG," Const Float %f",(float)expressionData.vValue.fFloatConst);
			break;
		case CValueVarient::isVeryLong:
				LOGIT(COUT_LOG," Const LongLong %I64d",expressionData.vValue.longlongVal);
				break;
		case CValueVarient::isString:
				LOGIT(COUT_LOG," Const ascii string '%s'",expressionData.sStringVal);
				break;
		case CValueVarient::isWideString:
				LOGIT(COUT_LOG," Const ascii string '%S'",expressionData.sWideStringVal);
				break;
		default:
				LOGIT(COUT_LOG," Unknown Data type in Varient");
				break;
		}// endswitch
		LOGIT(COUT_LOG,"\n");
	}
#ifdef _DBGCOND
	COUTSPACE << "-- end ExpressElement -- " << endl;
#endif

	return SUCCESS;
}

void hCexpressElement::setDuplicate(hCexpressElement& rSrc)
{
	exprElemType   = rSrc.exprElemType;
	expressionData = rSrc.expressionData;
	dependency.setDuplicate(rSrc.dependency);
}

bool hCexpressElement::operator==(const hCexpressElement& ri)
{
	if (exprElemType != ri.exprElemType) return false;
	if ( ! ( expressionData == ri.expressionData)) return false;
	if ( ! ( dependency == ri.dependency) ) return false;
	return true;
}

// stevev 23may08 - this has to select the proper language for
//	the incoming strings and translate them to wide chars
// stevev 08jul09 - we must assume that the parser has translated any EFF 5 or 6
//	strings from narrow to wide.  The Device Object must assume all strings are wide.
hCexpressElement& hCexpressElement::operator=(aCexpressElement& srcElem)
{   
	exprElemType  = srcElem.exprElemType; 
	dependency    = srcElem.dependency; 
	if (exprElemType == eet_STRCONST)
	{
#ifdef _DEBUG
if (srcElem.expressionData.vType != CValueVarient::isString)
{ 
	DEBUGLOG(CLOG_LOG, "nonString from the Parser(exprElem = )\n");
}
#endif
// we have to have a dictionary to do anything...
/* stevev 08jul09 - the varient will translate utf8 2 unicode if required
		string	locStr(srcElem.expressionData.sStringVal);
		wstring widStr, retStr;
		widStr	= AStr2TStr(locStr);
****/
		wstring widStr, retStr;
		widStr = (wstring)srcElem.expressionData;

		dictionary->get_string_translation(widStr, retStr);
		expressionData.clear();
		expressionData = retStr;
/* stevev 08jul09 - let the varient earn its money...
		expressionData.sWideStringVal = retStr;
		expressionData.vType          = CValueVarient::isWideString;
****/
	}
	else
	{
		expressionData= srcElem.expressionData;
	}
	return *this;
};

// fill in the type from the varient
void hCexpressElement::makeConst(void)
{
	dependency.clear();
	if ( ! expressionData.vIsValid ) // something failed
	{
		exprElemType = eeT_Unknown;
	}
	else
	if( expressionData.vType == CValueVarient::isIntConst || 
		expressionData.vType == CValueVarient::isVeryLong || 
		expressionData.vType == CValueVarient::isBool     )
	{
		exprElemType = eet_INTCST;
	}
	else
	if(expressionData.vType == CValueVarient::isFloatConst)
	{
		exprElemType = eet_FPCST;
	}
	else
	if( expressionData.vType == CValueVarient::isString || 
		expressionData.vType == CValueVarient::isWideString)
	{
		exprElemType = eet_STRCONST;
	}
	else
	{
		exprElemType = eeT_Unknown;
	}
}
/*************** hCexpression ***********************************************/
/*
 *
 *
 */

hCexpression::hCexpression(const hCexpression& src) : hCobject( src.devHndl() )
{
	lastError    = SUCCESS;
//XPX	executionStack.clear();

	entryCnt = 0;
#ifdef _DEBUG
	if (src.entryCnt > 0 )
	{
		LOGIT(CLOG_LOG,"Copy constructor for Expression with an entry count!\n");
	}
#endif
	operator=(src);
}


hCexpression::~hCexpression() 
{
//XPX	executionStack.clear(); 
	destroy();
#ifdef _DEBUG
	int Z = expressionL.size();
#endif // debug
	expressionL.clear();
}

RETURNCODE hCexpression::destroy(void)
{
	RETURNCODE rc = SUCCESS;
	hRPNexpress_t::iterator exprPos;
	hCexpressElement *pExprElem;
#ifdef _DEBUG 
	int which = 0; 
#endif
/*Vibhor 050404: Start of Code*/
	try
	{
		for (exprPos = expressionL.begin(); exprPos != expressionL.end(); ++exprPos)
		{// exprPos isa ptr 2a hCexpressElement
			pExprElem = &(*exprPos);// stevev 30sep11 - stop using iterator as pointer to item
			rc |= pExprElem->destroy();
#ifdef _DEBUG 
			which++; 
#endif
		}
		expressionL.clear();// stevev 30sep11
//XPX	executionStack.clear();
	}
	catch(...)
	{
#ifdef _DEBUG 
		hCexpressElement* pee = &(expressionL[which]);
#endif
		LOGIT(CERR_LOG,"RETURNCODE hCexpression :: destroy(void) inside catch(...) !!\n");
	}
	return rc;
}

hCexpression& hCexpression :: operator=(const hCexpression& srcAExpr) 
{
	// not used hCexpressElement wrkElem(devHndl());
	if ( expressionL.size() > 0 )
		expressionL.clear();
	hRPNexpress_t* pL  = (hRPNexpress_t*)(&(srcAExpr.expressionL));//sjv 5jul06 make nn-const
	for(hRPNexpress_t::iterator iT = pL->begin();    iT != pL->end();    ++iT  )
	{//it is a ptr 2 aCexpressElement
		//wrkElem = (*iT);
		//expressionL.push_back(wrkElem); 
		//wrkElem.clear();
		// optimization by CW 09feb12
		expressionL.push_back(*iT);
	}		
	return *this;						
};


bool hCexpression :: operator==(const hCexpression& ei)
{
	bool ret;
	hRPNexpress_t::iterator myExprElem;
	hRPNexpress_t::const_iterator eiExprElem;//ptr to exprElem// PAW 03/03/09 added const
			// stevev: a const_iterator is an iterator that points to a const

	if ( expressionL.size() != ei.expressionL.size() )
	{// not equal
		ret = false;// different
	}
	else
	{// same number of items
		if ( expressionL.size() == 0 ) return true;// they are empty so equal

		for(myExprElem = expressionL.begin(),
			eiExprElem= /*(hCexpressElement*)PAW 03/03/09*/ei.expressionL.begin();	
		    myExprElem!= expressionL.end() && eiExprElem!= ei.expressionL.end(); 
			++myExprElem,++eiExprElem)
		{
			if ( (*myExprElem) == (*eiExprElem) )
			{// we are still equal
				ret = true;
			}
			else
			{	ret = false;
			}
			if ( ! ret ) 
				break;// out of for loop to return false
		}
	}

	return ret;
}

RETURNCODE hCexpression :: dumpSelf(int indent, char* typeName)
{
	RETURNCODE rc = SUCCESS;

// the owner of the expression tells what type it is...no more exprType
	// dump the expression list
	hRPNexpress_t::iterator exprPos;
	int r;

	if (expressionL.size() > 0)
	{
#ifdef _DBGCOND
		COUTSPACE << "===== start of RPN sequence =====(" << expressionL.size() << ")" << endl;
		for (exprPos = expressionL.begin();exprPos < expressionL.end();exprPos++)
		{
COUTSPACE << "Expression List Element" << endl;	
COUTSPACE << "   Start Expression List Element Item" << endl;		
			r = exprPos->dumpSelf(indent + 6);
COUTSPACE << "   -End Expression List Element Item" << endl;
COUTSPACE << "   Start Expression List Dependency Item" << endl;
#else
		int z = 0; 
		for (exprPos = expressionL.begin();  exprPos < expressionL.end();  exprPos++)
		{// ptr 2 hCexpressElement
			//COUTSPACE << z++ << "]" ;
			LOGIT(COUT_LOG,"%s%d]",Space( indent ),z++);
			r = exprPos->dumpSelf(3);
#endif//_DBGCOND
			if ( r < 0 )
			{// we have a dependent reference
				
#ifdef _DBGCOND
				COUTSPACE << "     + Depends On:" << endl;
				rc |= exprPos->dependency.dumpSelf(indent + 9);
				COUTSPACE << "     - End Depends On:" << endl;
#else
				//COUTSPACE <<" exprElement.dependency: "<<endl;
				LOGIT(COUT_LOG,"%s exprElement.dependency: \n",Space( indent ));
				rc |= exprPos->dependency.dumpSelf(indent + 3);
#endif
			
			}
			else
			{
				rc |= r;
			}
#ifdef _DBGCOND
COUTSPACE << "   -End Expression List Dependency" << endl;
COUTSPACE << "-End Expression List Element" << endl;
		}
COUTSPACE << "============ end of RPN sequence ========" << endl;
#else
		}
#endif
	}
	// else there is no more to do here
	else
	{
#ifdef _DBGCOND
		COUTSPACE << "-No Expression List!" << endl;
#endif
	}
	return rc;
}

//typedef vector<hCexpressElement>	hRPNexpress_t;

// get a list of all of the item ids that the expression uses to select a payload
RETURNCODE hCexpression::aquireDependencyList(ddbItemList_t& retItemIDs, bool addVarValues)
{
	RETURNCODE rc = SUCCESS;
	bool       notAttr = true;
	//UINT32 uiD;
/*3jul06 - no longer used
	ddbItemLst_it foundI;
	hCitemBase*   pIB;
*/
		// for each in the hRPNexpress_t list of hCexpressElement
	for(     hRPNexpress_t::iterator 
		iT = expressionL.begin(); 
		iT < expressionL.end()   && rc == SUCCESS; 
		iT++)
	{// iT is a ptr 2a hCexpressElement
			
		if (iT->exprElemType >= eet_VARID && iT->exprElemType <= eet_MINREF)// 3jul06 eet_MINVAL)
		{//		type is	VarId[Min|Max]	24-26
		 //					dependency.depRef should hold a var id - get the value
/* stevev 3jul06 - a little more complicated		rc = iT->dependency.depRef.resolveID( pIB );
 use this:*/
//********** TODO: we need to handle the 4 new references shown below**********************//
			if (iT->exprElemType == eet_VARID || iT->exprElemType == eet_VARREF)
			{	notAttr = true; // not attr // addVars
			}
			else
			{	notAttr = false; // is attr // do not addVars
			}
			if LOGTHIS( LOGP_DEPENDENCY )
			{
				LOGIT(CLOG_LOG,"DEP:     Expression:  Variable or Variable reference\n");
			}

			rc = iT->dependency.depRef.aquireDependencyList(retItemIDs, notAttr);
/* end 3jul06 */
			//uiD = iT->dependency.depRef.getID(retItemIDs);
//aquire	if ( rc != SUCCESS || pIB == NULL )
//does		{
//this			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: dependency reference did not resolve.\n");
//now		}
//			else
//			{
//				for(foundI = retItemIDs.begin(); foundI!=retItemIDs.end(); ++foundI)
//				{
//					if ( pIB == *foundI ) // comparing pointers - it should work ok
//						break;// exit before found == end()
//				}
//				if (foundI == retItemIDs.end())
//				{
//					retItemIDs.push_back(pIB);
//				}// else it already exists, don't duplicate
//			}
		}
		/* stevev - dependency logging */
		else
		if LOGTHIS( LOGP_DEPENDENCY )
		{
			switch ( iT->exprElemType )
			{
			case	 eet_INTCST:
			case	 eet_FPCST:
				{
				LOGIT(CLOG_LOG,"DEP:     Expression:  Constant Numeric\n");
				}break;
			case	 eet_SYSENUM:
			case	 eet_COUNTREF:
			case	 eet_CAPACITYREF:
			case	 eet_FIRSTREF:
			case	 eet_LASTREF:
				{
				LOGIT(CLOG_LOG,"DEP:     Expression:  eDDL reference\n");
				rc = iT->dependency.depRef.aquireDependencyList(retItemIDs, notAttr);
				}break;
			case	 eet_STRCONST:
			default:
				{	;// no op
				}break;
			}// endswitch
		}// end LOGP_DEPENDENCY
		// else discard it, no values used
		
		//else ---TODO: implement the following case                         //- done 3jul06
		//if (iT->exprElemType >= eet_VARREF && iT->exprElemType <= eet_MINREF)
		//{//		type is	Ref[Min|Max]	27-29
		//					dependency.depRef should hold a reference - get the value
	}// next
	return rc;
}
/*
RETURNCODE hCexpression::getExprInfo(ExprTrailList_t& exprTrail)// get all expression resolutions
{
	RETURNCODE rc = SUCCESS;
	hCitemBase* pV = NULL;
	UINT32 uiD;
	hCexpressionTrail localExprTrail;

	exprTrail.clear();
	// this is the only one we will recognize right now
	if ( expressionL.size() == 1 && expressionL[0].exprElemType==eet_VARID)
	{
		uiD = expressionL[0].dependency.depRef.getID();
		rc = devPtr()->getItemBySymNumber(uiD, &pV);// returns pV
		if ( rc != SUCCESS || pV == NULL )
		{
			cerr << "ERROR: getItemBySymNumber in getExprInfo was unsuccessful getting id 0x"
																		<<hex<<uiD<<dec<<endl;
			rc = FAILURE;
		}
		else
		if ( pV->getIType() != iT_Variable)
		{
			cerr << "    ---- ItemID in getExprInfo.expression is NOT a Variable.("
													<< itemStrings[pV->getIType()]<<")"<<endl;
			rc = FAILURE;
		}
		else
		{
			localExprTrail.isVar = true;
			localExprTrail.srcID = uiD;
			exprTrail.push_back(localExprTrail);
			rc = SUCCESS;
		}
	}
	else
	{
		CValueVarient vv;
		vv = resolve();// self
		if (vv.vType == CValueVarient::isIntConst)
		{
			localExprTrail.isVar = false;
			localExprTrail.srcIdxVal = (int)vv;
			exprTrail.push_back(localExprTrail);
			rc = SUCCESS;
		}
		else
		if (vv.vType == CValueVarient::isBool || vv.vType == CValueVarient::isFloatConst)
		{
			localExprTrail.isVar = false;
			localExprTrail.srcIdxVal = (int)vv;
			exprTrail.push_back(localExprTrail);
			rc = FAILURE;
		}
		else
		{
			rc = FAILURE;
		}
	}

	return rc;
}
*/
CValueVarient hCexpression::resolve(hCexpressionTrail** retTrail, bool isAtest)
{
	CValueVarient  retVar, op1Var,op2Var;
	varientList_t  executionStack;
	RETURNCODE rc = SUCCESS;

	if(expressionL.size() <= 0)
	{// there is nothing to do here
		return retVar;
	}
//temp	executionStack.clear(); // clear the decks for action..
	entryCnt++;
	if (entryCnt > MAX_EXPR_RE_ENTRY)// maximum re-entrancy
	{
		LOGIF(LOGP_NOT_TOK)(UI_LOG|CERR_LOG|CLOG_LOG,"ERROR: expression re-entrancy limit reached."
			" Apparent infinite loop attempted.(%d for p%08x)\n",(entryCnt-1),this);
		retVar = 0.0;		
		retVar.vIsValid = false;
		entryCnt--;
		return retVar;
	}
	else	/* WAP 16dec05 - log ALL expression re-entrancy for DD QA in the future */
	if (entryCnt > 1)
	{
//		LOGIT(CLOG_LOG,"WARNING: expression re-entrance #%d At p%08x\n", (entryCnt-1), this);
	}

	//DEEPAK 112504
	try{
#ifdef DBG_EXECUTION
	LOGIT(CLOG_LOG,"------- p%08x start with %d on the stack.(%d)\n",
										&executionStack, executionStack.size(),entryCnt);
	int cnt = 0;
#endif
	// for each in the hRPNexpress_t list of hCexpressElement
	for(     hRPNexpress_t::iterator 
		iT = expressionL.begin(); 
		iT < expressionL.end()   /*stevev 10/13/04 do it all && rc == SUCCESS*/; 
		iT++)
	{// iT is a ptr 2a hCexpressElement
		hCexpressElement* pEE = &(*iT);// stevev 13oct11 - stop using iterator as pointer
		if (pEE->exprElemType >= eet_NOT && pEE->exprElemType <= eet_NEQ)
		{//		type is opcode,			1 thru 21 
	//					get opcode from the varient      - dependency empty
			expressElemType_t thisOpcode = pEE->expressionData.vValue.iOpCode;
#ifdef DBG_EXECUTION
			LOGIT(CLOG_LOG,"EXPR[%02d]: %s\n",cnt, exprElemStrings[thisOpcode]);
#endif
//XPX			if ( (rc = pull_back(op1Var)) != SUCCESS )
			if ( (rc = pull_back(op1Var, executionStack)) != SUCCESS )
			{
				LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: opCode initial pull failed. - continuing\n");
				/* stevev 10/13/04: is false ::>was: continue;*/// in loop - should abort..
			}


			switch(thisOpcode)
			{
			case	eet_NOT:			//_OPCODE 1			bool	unary
				// result type is Int
			case	eet_NEG:			//_OPCODE 2			Value	unary
			case	eet_BNEG:			//_OPCODE 3			Value	unary - bitwise negation(~v)
				// result type is OP1_rslt
				{// we know we have one?
					if (op1Var.vIsValid)
					{
						if ( op1Var.vType == CValueVarient::isString )
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: expression execution has a non-numeric unary operand.\n");
							rc = APP_EXPRESSION_ERR;
						}
						else
						{
							retVar = execUnary(thisOpcode, op1Var);
							executionStack.push_back(retVar);// for further operations.
						}
					}
					else // invalid
					{
						retVar = (int)op1Var; // push the invalid
						retVar = 0;// false
						executionStack.push_back(retVar);// for further operations.
					}
				}
				break;
			case	eet_ADD:			//_OPCODE 4			Value	binary-numeric
			case	eet_SUB:			//_OPCODE 5			Value	binary-numeric	
			case	eet_MUL:			//_OPCODE 6			Value	binary-numeric
			case	eet_DIV:			//_OPCODE 7			Value	binary-numeric
			case	eet_MOD:			//_OPCODE 8			Value	binary-numeric
			case	eet_LSHIFT:			//_OPCODE 9			Value	binary-numeric
			case	eet_RSHIFT:			//_OPCODE 10		Value	binary-numeric
			case	eet_AND:			//_OPCODE 11		Value	binary-numeric
			case	eet_OR:				//_OPCODE 12		Value	binary-numeric
			case	eet_XOR:			//_OPCODE 13		bool	binary-numeric
			//--need2 && convert to same type - result is OP1 type??
				{
//XPX					rc = pull_back(op2Var);//stevev 10/13/04 - now fails with invalid return value
					rc = pull_back(op2Var,executionStack);//stevev 10/13/04 - now fails with invalid return value
					if ( op1Var.vIsValid  && op2Var.vIsValid )
					{
						retVar = execBinaryNumeric(thisOpcode, op1Var, op2Var);
						executionStack.push_back(retVar);// for further operations.
					}
					else
					{
						retVar.clear() ;// isvalid false
						retVar.vType  = CValueVarient::isBool;
						retVar.vValue.bIsTrue = false;
						executionStack.push_back(retVar);// for further operations.
					}
				}
				break;
			case	eet_LAND:			//_OPCODE 14		bool	binary-boolean
			case	eet_LOR:			//_OPCODE 15		bool	binary-boolean
				// -- need2 && result is int
			case	eet_LT:				//_OPCODE 16		bool	binary-boolean
			case	eet_GT:				//_OPCODE 17		bool	binary-boolean
			case	eet_LE:				//_OPCODE 18		bool	binary-boolean
			case	eet_GE:				//_OPCODE 19		bool	binary-boolean
			case	eet_EQ:				//_OPCODE 20		bool	binary-boolean
			case	eet_NEQ:			//_OPCODE 21		bool	binary-boolean
				// need2 && convert -- result is int
				{
//XPX					rc = pull_back(op2Var);//stevev 10/13/04 - now fails with invalid return value
					rc = pull_back(op2Var,executionStack);//stevev 10/13/04 - now fails with invalid return value					/* stevev 12Jul05 - the Logical OR relation only needs one valid */
					if (thisOpcode == eet_LOR && (op1Var.vIsValid || op2Var.vIsValid) )
					{// only needs one valid 
						retVar = execBinaryBoolean(thisOpcode, op1Var, op2Var);
						executionStack.push_back(retVar);// for further operations.
					}
					else  // needs both of 'em valid
					if ( op1Var.vIsValid && op2Var.vIsValid )
					{
						retVar = execBinaryBoolean(thisOpcode, op1Var, op2Var);
						executionStack.push_back(retVar);// for further operations.
					}
					else // or it will return a valid false
					{
						retVar.clear() ;// invalid type and isvalid false, 0 size, not double or unsigned
						retVar.vType          = CValueVarient::isBool;
						retVar.vValue.bIsTrue = false;
						retVar.vIsValid       = true;
						retVar.vSize          = BOOLTSIZE;
						executionStack.push_back(retVar);// for further operations.
					}
					/* end stevev 12jul05 mods */
				}
				break;
			//default:  none ---the 'if' stmt wouldn't let us in
			}//endswitch
		}
		else 
		if ( pEE->exprElemType == eet_INTCST || pEE->exprElemType == eet_FPCST || 
			 pEE->exprElemType == eet_STRCONST )
		{//		type is constant		22,23
	//					get const value from the varient - dependenct empty
#ifdef DBG_EXECUTION
			LOGIT(CLOG_LOG,"EXPR[%02d]: %s\n",cnt, "EXPR:constant to stack.");
#endif
			retVar.clear() ;
			if ( pEE->exprElemType == eet_INTCST )
			{
				/* 06feb12 note:  the default constant type is int, NOT unsigned. The tokenizer
				encodes this as unsigned and, if negative, follows it with a Unary Negate
				opcode.  We must store this as a Signed Integer so the comparison will not
				promote all operands to unsigned. ****/
				retVar = (__int64)(pEE->expressionData);
			}
			else
			{
				retVar = pEE->expressionData;
			}
			retVar.vIsValid = true;
			retVar.vIndex   = (unsigned)(pEE->dependency.which); // in case this is a via_attr number
			executionStack.push_back(retVar);
		}
		else
		if (pEE->exprElemType >= eet_VARID && pEE->exprElemType <= eet_MINVAL)
		{//		type is	VarId[Min|Max]	24-26
	//					dependency.depRef should hold a var id - get the value
			hCitemBase* pV = NULL;
			UINT32 uiD = pEE->dependency.depRef.getID();
			if (retTrail != NULL && (*retTrail) != NULL)
			 {
				delete *retTrail;(*retTrail)=NULL;/*stevev 01mar05 - prevent crash */
			 }

#ifdef DBG_EXECUTION
			LOGIT(CLOG_LOG,"EXPR[%02d]: 0x%04x VARID reference\n",cnt, uiD);
#endif

		//->	rc = pCcommAPI->Read(uiD,   (void**)&pV);
		//	rc = pCcommAPI->Retrieve(uiD, pV);// returns pV
			rc = devPtr()->getItemBySymNumber(uiD, &pV);// returns pV
			if ( rc != SUCCESS || pV == NULL )
			{				
				hSdispatchPolicy_t policy = devPtr()->getPolicy();
				int m = CERR_LOG | CLOG_LOG;
				if ( policy.isReplyOnly )
				{// we are a device
					m = CLOG_LOG;
				}
				// else - both
				LOGIT(m,
				"ERROR: getItemBySymNumber was unsuccessful getting id 0x%04x\n",uiD);
			}
			else
			if ( pV->getIType() != iT_Variable)/* TODO: handle ARRAYs */
			{
				LOGIT(CERR_LOG,"    ---- ItemID in expression is NOT a Variable.(%s)\n",
															itemStrings[pV->getIType()]);
			}
			else
			{// is a var
/* try a read instead*///	retVar = ((hCVar*)pV)->getRealValue();// reusing retVar as a working variable..
				rc = devPtr()->ReadImd(pV,retVar,isAtest);
				if (rc != SUCCESS)
				{
					LOGIT(CLOG_LOG|CERR_LOG,"    ERROR: VARID expression ReadImd of %s failed "
						"with return code %d\n",pV->getName().c_str(),rc);
				}
				// we may need a getRealMin()  and a getRealMax() too.....
				if ( ! (retVar.vIsValid) )
				{
				/*<START> stevev 02/04/04 this often happens if the first part of an AND is false*/	
					/* 2/11/04 - stevev added detail */
					if (!isAtest)  // stevev 11jan06 - prevent warning when we expect it
						if (rc)
						{
							LOGIT(CLOG_LOG,
								"  WARNING: VARID expression execution received an invalid value "
								"for %s.\n",pV->getName().c_str());
						}
						else
						if ( pV->getIType() == iT_Variable && 
							((hCVar*)  pV)->VariableType() == vT_Index  &&
							((hCindex*)pV)->pIndexed != NULL && 
							((hCindex*)pV)->pIndexed->getArrayPointer() != NULL &&
							((hCindex*)pV)->pIndexed->getArrayPointer()->getIType() == iT_List )
						{// an empty list is normal 
							DEBUGLOG(CLOG_LOG,"");; // don't output a thing
							if ( ((hClist*)((hCindex*)pV)->pIndexed->getArrayPointer())->getCount() <= 0 )
							{
								rc = APP_LIST_IS_EMPTY;
							}
						}
						else
						{
//temporary							LOGIT(CLOG_LOG|CERR_LOG,
//								"  WARNING: VARID expression execution read an invalid value for %s\n",
//								pV->getName().c_str());
						}
				/*< END > stevev 02/04/04*/
				}// else push it anyway...eval will handle it
				if (   (  ((hCVar*)pV)->VariableType() == vT_Index ||		/* all int vars are */
						  ((hCVar*)pV)->VariableType() == vT_Integer ||     /* now valid - a PAR*/
						  ((hCVar*)pV)->VariableType() == vT_Unsigned ||	/* for the tokenizer*/
						  ((hCVar*)pV)->VariableType() == vT_Enumerated ||	/* is in to filter  */
						  ((hCVar*)pV)->VariableType() == vT_BitEnumerated  /* these anomolies  */
					   )					
					&& retTrail != NULL)
				{
					if ( (*retTrail) != NULL) delete *retTrail;
					(*retTrail) = new hCexpressionTrail;
					(*retTrail)->isVar     = true;
					(*retTrail)->srcID     = uiD;
					(*retTrail)->srcIdxVal = (int)retVar;
				}
				executionStack.push_back(retVar);// for further operations.
				retVar.clear();
			}
		}
		else
		if (pEE->exprElemType >= eet_VARREF && pEE->exprElemType <= eet_MINREF)
		{//		type is	Ref[Min|Max]	27-29
		 //		dependency.depRef should hold a reference - get the value
		 // 22aug06 - shortcut via_Attr - it must return a value
			if ( pEE->dependency.depRef.getRefType() == rT_via_Attr  )
			{
				rc = pEE->dependency.depRef.resolveExpr(retVar);
				if ( rc == SUCCESS )
				{					
					executionStack.push_back(retVar);// for further operations.
					retVar.clear();
				}
			}
			else
			if ( pEE->dependency.depRef.getRefType() == rT_via_BitEnum )
			{
				rc = pEE->dependency.depRef.resolveExpr(retVar);
				if ( rc == SUCCESS )
				{						
					hCitemBase* pV; // we don't own what this points to
					rc = pEE->dependency.depRef.resolveID( pV );

					if ( rc != SUCCESS || pV == NULL )
					{						
						LOGIT(CERR_LOG,
								"ERROR: resolveID failed for bit-enum in an expression\n");
					}
					else
					if ( pV->getIType() != iT_Variable)
					{				
						LOGIT(CERR_LOG,
								"ERROR: bit-enum in an expression was not a variable\n");
					}
					else// we have an index
					{
						rc = devPtr()->ReadImd(pV,op1Var,isAtest);
						if (rc != SUCCESS || retVar.vIsValid != true)
						{
							LOGIT(CERR_LOG,
							"ERROR: bit-enum in an expression's ReadImd failed with return code"
							" %d\n",rc);
						}
						else					
						if ( op1Var.vIsValid != true )
						{
							LOGIT(CERR_LOG,
							"ERROR: bit-enum (%s) in an expression failed to fetch bit 0x%02x\n"
							,((hCBitEnum*)pV)->getName().c_str(),(unsigned long)retVar );
						}
						else// we have an index and var
						{
							unsigned long idx = (unsigned long)retVar;
							unsigned long val = (unsigned long)op1Var;
							op1Var.clear();
							op1Var = (unsigned int)(val & idx);
							
							executionStack.push_back(op1Var);// for further operations.
						}
					}
				}
				op1Var.clear();
				retVar.clear();
			}
			else
			{
		// end 22aug06 - shortcut { closing brace below too }------------
// VMKP Added on 100404				
				hCitemBase* pV; // we don't own what this points to
				unsigned int uiD = 0;
				if ( pEE->dependency.depRef.getRefType() == rT_Constant )
				{// a const - proly a string
				}
				rc = pEE->dependency.depRef.resolveID( pV );
				
				if (retTrail != NULL && (*retTrail) != NULL)
				{// clears incoming sjv
				   delete *retTrail;(*retTrail)=NULL;/*stevev 01mar05 - prevent crash */
				}
				if(rc == SUCCESS && pV == NULL )// unique combination of SUCCESS & NULL 
				{// is a constant - proly string -
				}
				else
				if(rc == SUCCESS && retTrail != NULL)// 17apr07 - added ptr check per Vibhor
				{
					uiD = pV->getID();
					hCvarGroupTrail *retGroupTrail = pEE->dependency.depRef.getGroupTrailPtr();
					*retTrail = (retGroupTrail->pExprTrail);
				}


#ifdef DBG_EXECUTION
			LOGIT(CLOG_LOG,"EXPR[%02d]: 0x%04x VAREF reference\n",cnt, uiD);
#endif

			//->	rc = pCcommAPI->Read(uiD,   (void**)&pV);
			//	rc = pCcommAPI->Retrieve(uiD, pV);// returns pV
				if ( rc != SUCCESS || pV == NULL )
				{				
					hSdispatchPolicy_t policy = devPtr()->getPolicy();
					int m = CERR_LOG | CLOG_LOG;
					if ( policy.isReplyOnly )
					{// we are a device
						m = CLOG_LOG;
					}
					// else - both
					LOGIT(m,
					"ERROR: getItemBySymNumber was unsuccessful getting id 0x%04x\n",uiD);
				}
				else
				if ( pV->getIType() != iT_Variable)
				{
					LOGIT(CERR_LOG,"    ---- ItemID in expression is NOT a Variable.(%s)\n",
																itemStrings[pV->getIType()]);
				}
				else
				{// is a var
	/* try a read instead*///	retVar = ((hCVar*)pV)->getRealValue();// reusing retVar as a working variable..
					rc = devPtr()->ReadImd(pV,retVar,isAtest);
					if (rc != SUCCESS)
					{
						LOGIT(CERR_LOG,
						"    ERROR: VARREF expression ReadImd failed with return code %d\n",rc);
					}
					// we may need a getRealMin()  and a getRealMax() too.....
					if ( retVar.vIsValid != true)
					{
					/*<START> stevev 02/04/04 this often happens if the first part of an AND is false*/	
						/* 2/11/04 - stevev added detail */
						if (!isAtest && ( ! devPtr()->getPolicy().isPartOfTok) )
										// stevev 11jan06 - prevent warning when we expect it
										// stevev 01nov10 - don't output this while tokenizing
							if (rc)
							{
								LOGIT(CLOG_LOG,
									"  WARNING: VARREF expression execution received an invalid value.\n");
							}
							else
							{
								LOGIT(CLOG_LOG,
									"  WARNING: VARREF expression execution read an invalid value for 0x%04x\n",
									pV->getID());
							}
					/*< END > stevev 02/04/04*/
					}// else push it anyway...eval will handle it
					if (   (  ((hCVar*)pV)->VariableType() == vT_Index ||		/* all int vars are */
							  ((hCVar*)pV)->VariableType() == vT_Integer ||     /* now valid - a PAR*/
							  ((hCVar*)pV)->VariableType() == vT_Unsigned ||	/* for the tokenizer*/
							  ((hCVar*)pV)->VariableType() == vT_Enumerated ||	/* is in to filter  */
							  ((hCVar*)pV)->VariableType() == vT_BitEnumerated  /* these anomolies  */
						   )					
						&& retTrail != NULL)
					{
						if ( (*retTrail) != NULL) delete *retTrail;
						(*retTrail) = new hCexpressionTrail;
						(*retTrail)->isVar     = true;
						(*retTrail)->srcID     = uiD;
						(*retTrail)->srcIdxVal = (int)retVar;
					}
					executionStack.push_back(retVar);// for further operations.
					retVar.clear();
				}// end else-isVar
			}// endif via_Attr
//			cerr <<"++++++++++++ Unimplemented reference access +++++++++++++"<<endl;
// VMKP End of VMKP Addition on 100404

		}
		else
		if (pEE->exprElemType >= eet_SYSENUM && pEE->exprElemType <= eet_LASTREF)
		{
			LOGIT(CERR_LOG,"********** Expression element:%s"
				" is not supported yet!!!!**********",exprElemStrings[pEE->exprElemType]);
		}
		else
		{// error - return the invalid varient - default_valref,viewmin,viewmax
			LOGIT(CERR_LOG,"++++++++++++ Invalid varient type +++++++++++++\n");
		}		
#ifdef DBG_EXECUTION
		cnt++;
#endif
	}// next RPN expression item

	entryCnt--;
if ( entryCnt <0 || entryCnt > MAX_EXPR_RE_ENTRY)
{
#ifdef _DEBUG
	LOGIT(CLOG_LOG,"EntryCount went out of range.\n");
#endif
	entryCnt = 0;
	executionStack.clear();
}

	if (executionStack.size() > 0)
	{
		retVar = executionStack.back();executionStack.pop_back();
#ifdef DBG_EXECUTION
		LOGIT(CLOG_LOG,"EXPR:finished----.\n");
#endif
		if (entryCnt <= 0 )
		{// entryCnt <= 0
			if (executionStack.size() > 1)
			{// should be empty
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: expression execution has stuff left on the stack! (%d)\n",executionStack.size());
		#ifdef DBG_EXECUTION
					clog << "EXPR:"<< executionStack.size()<<" left on the stack at final exit."<<endl;
		#endif
				executionStack.clear();
			}// else its as empty as it should be
		}
#ifdef DBG_EXECUTION
		else
		{	// we have been re-entered..
			if (executionStack.size() > 1)
			{
				clog << "EXPR:"<< executionStack.size()<<" left on the stack at exit."<<endl;
			}
		}	
#endif
/* the following gives a radically different (non-functional) outcome...
		retVar.vIsValid = false;// stevev 20mar06 headmerge - from original
   delete it till we figure out why... */
	}
	else
	{// not even one - error
		// should have one result left on it
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: expression execution has nothing on the stack! (%d)\n",
		executionStack.size() );
#ifdef DBG_EXECUTION
		clog << "EXPR: nothing left on the stack." <<endl;
#endif
		retVar.vIsValid = false;
	}

//	if (executionStack.size() > 1)
//	{// should only have the result left on it
//		cerr<<"ERROR: expression execution has stuff left on the stack! ("<<executionStack.size()<<")"<<endl;
//		
//#ifdef DBG_EXECUTION
//			clog << "EXPR:"<< executionStack.size()<<" left on the stack at exit."<<endl;
//#endif
//
//		retVar.vIsValid = false;
//	}	
//	else
//	if (executionStack.size() == 0)
//	{// should have one result left on it
//		cerr<<"ERROR: expression execution has nothing on the stack! ("<<executionStack.size()<<")"<<endl;
//#ifdef DBG_EXECUTION
//			clog << "EXPR: nothing left on the stack." <<endl;
//#endif
//		retVar.vIsValid = false;
//	}
//	else
//	{
//		retVar = executionStack.back();executionStack.pop_back();
//#ifdef DBG_EXECUTION
//			clog << "EXPR:finished----." <<endl;
//#endif
//	}

// temp 	executionStack.clear();

#ifdef DBG_EXECUTION
	LOGIT(CLOG_LOG,"------- p%08x End with %d\n", &executionStack , executionStack.size() );
#endif
	lastError = rc;

	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"hCexpression::resolve Inside Catch(...)\n");
	}
//DEEPAK 251104
	return retVar;
}


RETURNCODE hCexpression :: pull_back(CValueVarient& returnVarient, varientList_t& actvStk)
{
	RETURNCODE rc = SUCCESS;

//XPX	if (executionStack.size() < 1)
	try//DEEPAK 251104	
	{
	if (actvStk.size() < 1)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:  expression execution has an opcode with no operands.\n");
		rc = APP_EXPRESSION_ERR;
	}
	else
	{// we will always use one
//XPX		returnVarient = executionStack.back();
		returnVarient = actvStk.back();
//XPX		executionStack.pop_back();
		actvStk.pop_back();
		if (returnVarient.vType != CValueVarient::isBool       && 
			returnVarient.vType != CValueVarient::isIntConst   && 
			returnVarient.vType != CValueVarient::isVeryLong   && 
			returnVarient.vType != CValueVarient::isFloatConst &&
			returnVarient.vType != CValueVarient::isString     )
		{
			LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: expression execution has an unusable operand of type %s and value of: %s\n",
				varientTypeStrings[returnVarient.vType],((string)returnVarient).c_str());
			/* stevev 10/13/04 */
			returnVarient.vType = CValueVarient::isBool;
			returnVarient = false;
			returnVarient.vIsValid = false;
			/* end stevev 10/13/04 */
// don't pop 'em twice		executionStack.pop_back();
			rc = APP_EXPRESSION_ERR;
		}// else continue, all ok
	}
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...) PULL_BACK\n");
	}
//DEEPAK 251104
	return rc;
}


CValueVarient hCexpression :: execUnary(expressElemType_t Opcode,         CValueVarient& op1)
{
	CValueVarient rVar;

	try{//DEEPAK 251104

	if(Opcode == eet_NOT && op1.isNumeric() )			//_OPCODE 1			bool	unary
	{		// result type is Int
		rVar.vValue.bIsTrue = op1.valueIsZero();// any numeric
		rVar.vType    = CValueVarient::isBool;
		rVar.vSize    = BOOLTSIZE;
		rVar.vIsValid = op1.vIsValid;
	}
	else
		// result type is OP1_rslt
	if(Opcode == eet_NEG && op1.isNumeric() )			//_OPCODE 2			Value	unary
	{
		rVar = op1;
		if ( op1.vType == CValueVarient::isIntConst)
		{
			rVar.vValue.iIntConst = -(op1.vValue.iIntConst);
			if ( rVar.vIsUnsigned == true )
			{
				rVar.vIsUnsigned = false;
			}
		}
		else 
		if ( op1.vType == CValueVarient::isVeryLong)
		{
			rVar.vValue.longlongVal = -(op1.vValue.longlongVal);
			if ( rVar.vIsUnsigned == true )
			{
				rVar.vIsUnsigned = false;
			}
		}
		else 
		if ( op1.vType == CValueVarient::isFloatConst )
		{
			rVar.vValue.fFloatConst = -(op1.vValue.fFloatConst);
		}
		else
		{
			lastError     = APP_EXPR_TYPE_ERR;
			rVar.vIsValid = false;
		}
	}
	else
	if (Opcode == eet_BNEG && op1.isNumeric() )			//_OPCODE 3	  Value	unary - bitwise negation(~v)
	{
		rVar = op1;
		if ( op1.vType == CValueVarient::isIntConst )
		{
			rVar.vValue.iIntConst = ~ (op1.vValue.iIntConst);
		}
		else
		if ( op1.vType == CValueVarient::isVeryLong )
		{
			rVar.vValue.longlongVal = ~ (op1.vValue.longlongVal);
		}
		else// float or bool
		{
			lastError     = APP_EXPR_TYPE_ERR;
			rVar.vIsValid = false;
		}
	}
	// else return the empty, invalid varient
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...) execUnary(expressElemType_t Opcode\n");
	}
//DEEPAK 251104
	return rVar;
}

CValueVarient hCexpression :: execBinaryNumeric(expressElemType_t Opcode, CValueVarient& op1, CValueVarient& op2)
{//--need2 && convert to same type - result is OP1 type??

	CValueVarient rVar;
	try{  //DEEPAK 251104

	if ( (lastError = promote(rVar, /*from*/ op1,op2)) == SUCCESS )
	{
		switch(Opcode)
		{
		case	eet_ADD:			//_OPCODE 4			Value	binary-numeric
			{
				if (rVar.vType == CValueVarient::isFloatConst)
				{
					rVar.vValue.fFloatConst = op1.vValue.fFloatConst + op2.vValue.fFloatConst;
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.longlongVal = ((unsigned __int64)op1.vValue.longlongVal) + 
												  ((unsigned __int64)op2.vValue.longlongVal);
					}
					else
					{	rVar.vValue.longlongVal = op1.vValue.longlongVal + op2.vValue.longlongVal;}
				}
				else
				if (rVar.vType == CValueVarient::isIntConst)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.iIntConst = ((unsigned long)op1.vValue.iIntConst) + 
												((unsigned long)op2.vValue.iIntConst);
					}
					else
					{	rVar.vValue.iIntConst = op1.vValue.iIntConst + op2.vValue.iIntConst;}
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_SUB:			//_OPCODE 5			Value	binary-numeric	
			{
				if (rVar.vType == CValueVarient::isFloatConst)
				{
					rVar.vValue.fFloatConst = op2.vValue.fFloatConst - op1.vValue.fFloatConst;
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.longlongVal = ((unsigned __int64)op2.vValue.longlongVal) - 
												  ((unsigned __int64)op1.vValue.longlongVal);
					}
					else
					{	rVar.vValue.longlongVal = op2.vValue.longlongVal - op1.vValue.longlongVal;}
				}
				else
				if (rVar.vType == CValueVarient::isIntConst)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) - 
												((unsigned long)op1.vValue.iIntConst);}
					else
					{	rVar.vValue.iIntConst = op2.vValue.iIntConst - op1.vValue.iIntConst;}
				}
				else // doesn't work on bools
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_MUL:			//_OPCODE 6			Value	binary-numeric
			{
				if (rVar.vType == CValueVarient::isFloatConst)
				{
					rVar.vValue.fFloatConst = op2.vValue.fFloatConst * op1.vValue.fFloatConst;
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.longlongVal = ((unsigned __int64)op1.vValue.longlongVal) * 
												  ((unsigned __int64)op2.vValue.longlongVal);
					}
					else
					{	rVar.vValue.longlongVal = op1.vValue.longlongVal * op2.vValue.longlongVal;}
				}
				else
				if (rVar.vType == CValueVarient::isIntConst)
				{
					if (rVar.vIsUnsigned == true)
					{   rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) * 
												((unsigned long)op1.vValue.iIntConst);}
					else
					{	rVar.vValue.iIntConst = op2.vValue.iIntConst * op1.vValue.iIntConst;}
				}
				else // doesn't work on bools
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_DIV:			//_OPCODE 7			Value	binary-numeric
			{
				if (op1.valueIsZero() == true)
				{
					lastError = APP_EXPR_DIV_BY_ZERO;
					rVar.vIsValid = false;
				}
				else
				{
					if (rVar.vType == CValueVarient::isFloatConst)
					{
						rVar.vValue.fFloatConst = op2.vValue.fFloatConst / op1.vValue.fFloatConst;
					}
					else
					if (rVar.vType == CValueVarient::isVeryLong)
					{
						if (rVar.vIsUnsigned == true)
						{   rVar.vValue.longlongVal = ((unsigned __int64)op2.vValue.longlongVal) / 
													  ((unsigned __int64)op1.vValue.longlongVal);
						}
						else
						{	rVar.vValue.longlongVal = op2.vValue.longlongVal / op1.vValue.longlongVal;}
					}
					else
					if (rVar.vType == CValueVarient::isIntConst)
					{
						if (rVar.vIsUnsigned == true)
						{   rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) / 
													((unsigned long)op1.vValue.iIntConst);}
						else
						{	rVar.vValue.iIntConst = op2.vValue.iIntConst / op1.vValue.iIntConst;}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
			}
			break;
		case	eet_MOD:			//_OPCODE 8			Value	binary-numeric
			{
				if (op1.valueIsZero() == true)
				{
					lastError = APP_EXPR_DIV_BY_ZERO;
					rVar.vIsValid = false;
				}
				else
				{ 
					if (rVar.vType == CValueVarient::isFloatConst)
					{
						rVar.vValue.fFloatConst = fmod(op2.vValue.fFloatConst, op1.vValue.fFloatConst);
					}
					else
					if (rVar.vType == CValueVarient::isVeryLong)
					{
						if (rVar.vIsUnsigned == true)
						{   rVar.vValue.longlongVal = ((unsigned __int64)op2.vValue.longlongVal) % 
													  ((unsigned __int64)op1.vValue.longlongVal);
						}
						else
						{	rVar.vValue.longlongVal = op2.vValue.longlongVal % op1.vValue.longlongVal;}
					}
					else
					if (rVar.vType == CValueVarient::isIntConst)
					{
						if (rVar.vIsUnsigned == true)
						{   rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) % 
													((unsigned long)op1.vValue.iIntConst);}
						else
						{	rVar.vValue.iIntConst = op2.vValue.iIntConst % op1.vValue.iIntConst;}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
			}
			break;
		case	eet_LSHIFT:			//_OPCODE 9			Value	binary-numeric
			{
				if (rVar.vType == CValueVarient::isIntConst)
				{
					rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) << 
													((unsigned long)op1.vValue.iIntConst);
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					rVar.vValue.longlongVal = ((unsigned __int64)op2.vValue.longlongVal) << 
												  ((unsigned __int64)op1.vValue.longlongVal);
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_RSHIFT:			//_OPCODE 10		Value	binary-numeric
			{
				if (rVar.vType == CValueVarient::isIntConst)
				{
					rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) >> 
													((unsigned long)op1.vValue.iIntConst);
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					rVar.vValue.longlongVal = ((unsigned __int64)op2.vValue.longlongVal) >> 
												  ((unsigned __int64)op1.vValue.longlongVal);
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_AND:			//_OPCODE 11		Value	binary-numeric
			{
				if (rVar.vType == CValueVarient::isIntConst)
				{
					/* stevev 1/29/04 - ultrafast PAR 
									  - If first operand false, second not evaluated*/
					/* remove 
					rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) & 
													((unsigned long)op1.vValue.iIntConst);
					end remove stevev 1/29/04*/
					/*add stevev 1/29/04*/
					if ( op1.vValue.iIntConst == 0 && op1.vIsValid != false )
						// valid false - do not evaluate the second
					{
						rVar.vValue.iIntConst = ((unsigned long)op1.vValue.iIntConst);						
						rVar.vIsValid = op1.vIsValid;
					}
					else
					{// first one true or invalid
						rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) & 
													((unsigned long)op1.vValue.iIntConst);
						// leave validity the worst of the two
					}
					/* end add stevev 1/29/04*/
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{					
					if ( op1.vValue.longlongVal == 0 && op1.vIsValid != false )
						// valid false - do not evaluate the second
					{
						rVar.vValue.longlongVal = ((unsigned long)op1.vValue.longlongVal);						
						rVar.vIsValid = op1.vIsValid;
					}
					else
					{// first one true or invalid
						rVar.vValue.longlongVal = ((unsigned long)op2.vValue.longlongVal) & 
													((unsigned long)op1.vValue.longlongVal);
						// leave validity the worst of the two
					}
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_OR:				//_OPCODE 12		Value	binary-numeric
			{// first one true, don't evaluate the second
				if (rVar.vType == CValueVarient::isIntConst)
				{
					rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) | 
													((unsigned long)op1.vValue.iIntConst);
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					rVar.vValue.longlongVal = ((unsigned long)op2.vValue.longlongVal) | 
													((unsigned long)op1.vValue.longlongVal);
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		case	eet_XOR:			//_OPCODE 13		bool	binary-numeric
			{
				if (rVar.vType == CValueVarient::isIntConst)
				{
					rVar.vValue.iIntConst = ((unsigned long)op2.vValue.iIntConst) ^ 
													((unsigned long)op1.vValue.iIntConst);
				}
				else
				if (rVar.vType == CValueVarient::isVeryLong)
				{
					rVar.vValue.longlongVal = ((unsigned long)op2.vValue.longlongVal) ^ 
													((unsigned long)op1.vValue.longlongVal);
				}
				else
				{
					lastError = APP_EXPR_TYPE_ERR;
					rVar.vIsValid = false;
				}
			}
			break;
		// no default - the caller should not allow it
		}
	}
	else // promotion error
	{
		; // promote should have set the rVar to invalid
	}

	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...) execBinary(expressElemType_t Opcode\n");
	}
//DEEPAK 251104
	return rVar;
}

CValueVarient hCexpression :: execBinaryBoolean(expressElemType_t Opcode, CValueVarient& op1,     
																		  CValueVarient& op2)
{
	CValueVarient rVar;
	
	try{ //DEEPAK 251104

	if ( (lastError = promote(rVar, /*from*/ op1,op2)) == SUCCESS )
	{
		rVar.vSize          = BOOLTSIZE;
		rVar.vType          = CValueVarient::isBool;
		rVar.vIsUnsigned    = false;
		// stevev 12jul05 - return a Valid false...
		rVar.vIsValid       = true;
		rVar.vValue.bIsTrue = false;
		
		/* stevev 10/08/04 - as per WAP - either is invalid - binary is false */
		/* stevev 12jul05  - only one needs to be valid for an OR statement */
		if ((Opcode != eet_LOR) &&       ((! op1.vIsValid) || ( ! op2.vIsValid )) )
		{
			lastError = APP_EXPR_BIN_WO_VALID;
			// stevev 12jul05 - return a Valid false...rVar.vIsValid = false;
			// leave it as is
		}
		else
		{// continue forward

			switch(Opcode)
			{
			case	eet_LAND:			//_OPCODE 14		bool	binary-boolean
				{	if ( (lastError = toBoolT(rVar, /*from*/ op1,op2)) == SUCCESS )
					{	
					/*start<REMOVE>stevev 2/04/04 - If first operand false, second not evaluated
						if (op2.vValue.bIsTrue == true && op1.vValue.bIsTrue == true)
							rVar.vValue.bIsTrue = true;
						else
							rVar.vValue.bIsTrue = false;
					  end<REMOVE>stevev 2/04/04 - If first operand false, second not evaluated*/
					/*start<ADD> stevev 2/04/04 - */
						if ( op2.vValue.bIsTrue == false && op2.vIsValid != false )
							// 1st false and 1st validly true - do not evaluate the second
						{
							rVar.vValue.bIsTrue = false;						
							rVar.vIsValid = op2.vIsValid;
						}
						else
						{// first one true or invalid
							if (op2.vValue.bIsTrue == true && op1.vValue.bIsTrue == true)
								rVar.vValue.bIsTrue = true;
							else
								rVar.vValue.bIsTrue = false;
							// leave validity the worst of the two in both these cases
						}
					/* end <ADD> stevev 2/04/04 - */
					}// else rVar is already set invalid, last error is setup too
				}
				break;
			case	eet_LOR:			//_OPCODE 15		bool	binary-boolean
				{/* stevev 12jul05 - either or both could be INVALID */
					if ( (lastError = toBoolT(rVar, /*from*/ op1,op2)) == SUCCESS )
					{	/* stevev - 12jul05 - valid and true */
						if ((op2.vIsValid && op2.vValue.bIsTrue == true) || 
							(op1.vIsValid && op1.vValue.bIsTrue == true)  )
							rVar.vValue.bIsTrue = true;
						else
							rVar.vValue.bIsTrue = false;
					}// else rVar is already set invalid, last error is setup too
				}
				break;


			case	eet_LT:				//_OPCODE 16		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (op2.vValue.fFloatConst < op1.vValue.fFloatConst)
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) < 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal < op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) < 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst < op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
				break;
			case	eet_GT:				//_OPCODE 17		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (op2.vValue.fFloatConst > op1.vValue.fFloatConst)
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) > 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal > op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) > 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst > op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
				break;
			case	eet_LE:				//_OPCODE 18		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (  (op2.vValue.fFloatConst < op1.vValue.fFloatConst) ||
							  AlmostEqual2sComplement(op2.vValue.fFloatConst,op1.vValue.fFloatConst, 4) )
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) <= 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal <= op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) <= 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst <= op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
				break;
			case	eet_GE:				//_OPCODE 19		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (  (op2.vValue.fFloatConst > op1.vValue.fFloatConst)  || 
							  AlmostEqual2sComplement(op2.vValue.fFloatConst,op1.vValue.fFloatConst, 4) )
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) >= 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal >= op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) >= 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst >= op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
				break;
			case	eet_EQ:				//_OPCODE 20		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (AlmostEqual2sComplement(op2.vValue.fFloatConst,op1.vValue.fFloatConst, 4))
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) == 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal == op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) == 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst == op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
					if ( rVar.vType != CValueVarient::isBool)
					{
						clog << "Ref.EQ not bool."<<endl;
					}
				}
				break;
			case	eet_NEQ:			//_OPCODE 21		bool	binary-boolean
				{
					if (op2.vType == CValueVarient::isFloatConst)
					{
						if (! AlmostEqual2sComplement(op2.vValue.fFloatConst,op1.vValue.fFloatConst, 4))
							rVar.vValue.bIsTrue =  true ;
						else
							rVar.vValue.bIsTrue = false;
					}
					else
					if (op2.vType == CValueVarient::isVeryLong)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned __int64)op2.vValue.longlongVal) != 
								((unsigned __int64)op1.vValue.longlongVal)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.longlongVal != op1.vValue.longlongVal )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else
					if (op2.vType == CValueVarient::isIntConst)
					{
						if (op2.vIsUnsigned == true)
						{   
							if (((unsigned long)op2.vValue.iIntConst) != 
								((unsigned long)op1.vValue.iIntConst)  )
								rVar.vValue.bIsTrue =  true ;
							else
								rVar.vValue.bIsTrue = false;
						}
						else
						{	 
							if ( op2.vValue.iIntConst != op1.vValue.iIntConst )
								rVar.vValue.bIsTrue =  true;
							else
								rVar.vValue.bIsTrue =  false;
						}
					}
					else // doesn't work on bools
					{
						lastError = APP_EXPR_TYPE_ERR;
						rVar.vIsValid = false;
					}
				}
				break;
			}// endswitch
		}// endif - both valid
	}// else a promotion error - everything is ready to return
	
	if ( rVar.vType != CValueVarient::isBool)
	{
		clog << "Ref.ANY not bool."<<endl;
	}
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...)  execBinaryBoolean (expressElemType_t Opcode\n");
	}
//DEEPAK 251104
	return rVar;
}


RETURNCODE hCexpression :: promote(CValueVarient& returnVarient, CValueVarient& op1, CValueVarient& op2)
{
// value unchanged
	RETURNCODE rc = SUCCESS;

	try{ //DEEPAK 251104

	if (op1.vType == CValueVarient::isFloatConst || op2.vType == CValueVarient::isFloatConst)
	{
		returnVarient.vType = CValueVarient::isFloatConst;
		
		if (op1.vType == CValueVarient::isVeryLong ) 
		{
			if (op1.vIsUnsigned == false)
				op1.vValue.fFloatConst = (double)(op1.vValue.longlongVal);
			else
			{// bill hasn't implemented unsigned to double conversion
				op1.vValue.fFloatConst = (double)(op1.vValue.longlongVal & 0x7fffffffffffffffULL); // L&T Modifications : PortToAM335
				if ( (op1.vValue.longlongVal & 0x8000000000000000ULL) != 0) // L&T Modifications : PortToAM335
				{
					op1.vValue.fFloatConst += 9.223372036854775808e+18;
				}
				//else - it's ok as-is
			}
			op1.vType = CValueVarient::isFloatConst;
			op1.vIsUnsigned = false;
		}
		else
		if (op1.vType == CValueVarient::isIntConst ) 
		{
			if (op1.vIsUnsigned == false)
				op1.vValue.fFloatConst = op1.vValue.iIntConst;
			else
				op1.vValue.fFloatConst = ((unsigned long)op1.vValue.iIntConst);
			op1.vType = CValueVarient::isFloatConst;
			op1.vIsUnsigned = false;
		}
		else
		if (op1.vType == CValueVarient::isBool ) 
		{
			if  (op1.vValue.bIsTrue){ op1.vValue.fFloatConst = 1.0f;}
			else{op1.vValue.fFloatConst = 0.0f;}
			op1.vType  = CValueVarient::isFloatConst;
		}
		else
		if (op1.vType != CValueVarient::isFloatConst )  // & ! bool & ! int - error
		{
			op1.vValue.fFloatConst = 0.0f;
			op1.vType = CValueVarient::isFloatConst;
			op1.vIsUnsigned = false;
			op1.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already float

		
		if (op2.vType == CValueVarient::isVeryLong ) 
		{
			if (op2.vIsUnsigned == false)
				op2.vValue.fFloatConst = (double)(op2.vValue.longlongVal);
			else
			{// bill hasn't implemented unsigned to double conversion
				op2.vValue.fFloatConst = (double)(op2.vValue.longlongVal & 0x7fffffffffffffffULL); // L&T Modifications : PortToAM335
				if ( (op2.vValue.longlongVal & 0x8000000000000000ULL) != 0) // L&T Modifications : PortToAM335
				{
					op2.vValue.fFloatConst += 9.223372036854775808e+18;
				}
				//else - it's ok as-is
			}
			op2.vType = CValueVarient::isFloatConst;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType == CValueVarient::isIntConst ) 
		{
			if (op2.vIsUnsigned == false)
				op2.vValue.fFloatConst = op2.vValue.iIntConst;/* C&P repair 10/1/04 sjv*/
			else
				op2.vValue.fFloatConst = ((unsigned long)op2.vValue.iIntConst);/* C&P repair 10/1/04 sjv*/
			op2.vType = CValueVarient::isFloatConst;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType == CValueVarient::isBool ) 
		{
			if  (op2.vValue.bIsTrue) { op2.vValue.fFloatConst = 1.0f;}
			else{op2.vValue.fFloatConst = 0.0f;} /* C&P repair 10/1/04 sjv*/
			op2.vType  = CValueVarient::isFloatConst;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType != CValueVarient::isFloatConst )  // & ! bool & ! int - error
		{
			op2.vValue.fFloatConst = 0.0f;
			op2.vType = CValueVarient::isFloatConst;
			op2.vIsUnsigned = false;
			op2.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already float
	}
	else
	if (op1.vType == CValueVarient::isVeryLong || op2.vType == CValueVarient::isVeryLong) 
	{
		returnVarient.vType = CValueVarient::isVeryLong;
		
		if (op1.vType == CValueVarient::isIntConst ) 
		{
			if (op1.vIsUnsigned == false)
				op1.vValue.longlongVal = (__int64)op1.vValue.iIntConst;
			else // is unsigned
				op1.vValue.longlongVal = ((unsigned __int64)((unsigned)op1.vValue.iIntConst));
			op1.vType = CValueVarient::isVeryLong;
			op1.vIsUnsigned = false;
		}
		else
		if (op1.vType == CValueVarient::isBool ) 
		{
			if (op1.vValue.bIsTrue){op1.vValue.longlongVal = 1;}else{op1.vValue.longlongVal = 0;}
			op1.vType = CValueVarient::isVeryLong;
			op1.vIsUnsigned = false;
		}
		else
		if (op1.vType != CValueVarient::isVeryLong )  // & ! bool - error
		{
			op1.vValue.longlongVal = 0;
			op1.vType = CValueVarient::isVeryLong;
			op1.vIsUnsigned = false;
			op1.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already long long

		if (op2.vType == CValueVarient::isIntConst ) 
		{
			if (op2.vIsUnsigned == false)
				op2.vValue.longlongVal = (__int64)op2.vValue.iIntConst;
			else // is unsigned
				// this sign extends::>op2.vValue.longlongVal = ((unsigned __int64)op2.vValue.iIntConst); bug 3242 20apr10
				op2.vValue.longlongVal = ((unsigned __int64)((unsigned)op2.vValue.iIntConst));
			op2.vType = CValueVarient::isVeryLong;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType == CValueVarient::isBool ) 
		{
			if(op2.vValue.bIsTrue){op2.vValue.longlongVal = 1;}else{op2.vValue.longlongVal = 0;}
			op2.vType = CValueVarient::isVeryLong;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType != CValueVarient::isVeryLong )  // & ! bool - error
		{
			op2.vValue.longlongVal = 0;
			op2.vType = CValueVarient::isVeryLong;
			op2.vIsUnsigned = false;
			op2.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already int
		
		if (op1.vIsUnsigned || op2.vIsUnsigned){returnVarient.vIsUnsigned = true;}
		else{returnVarient.vIsUnsigned = false;}
		op1.vIsUnsigned = op2.vIsUnsigned = returnVarient.vIsUnsigned;
	}
	else
	if (op1.vType == CValueVarient::isIntConst || op2.vType == CValueVarient::isIntConst) // other must be bool or bad
	{
		returnVarient.vType = CValueVarient::isIntConst;
		
		if (op1.vType == CValueVarient::isBool ) 
		{
			if (op1.vValue.bIsTrue){op1.vValue.iIntConst = 1;}else{op1.vValue.iIntConst = 0;}
			op1.vType = CValueVarient::isIntConst;
			op1.vIsUnsigned = false;
		}
		else
		if (op1.vType != CValueVarient::isIntConst )  // & ! bool - error
		{
			op1.vValue.iIntConst = 0;
			op1.vType = CValueVarient::isIntConst;
			op1.vIsUnsigned = false;
			op1.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already int

		if (op2.vType == CValueVarient::isBool ) 
		{
			if(op2.vValue.bIsTrue){op2.vValue.iIntConst = 1;}else{op2.vValue.iIntConst = 0;}
			op2.vType = CValueVarient::isIntConst;
			op2.vIsUnsigned = false;
		}
		else
		if (op2.vType != CValueVarient::isIntConst )  // & ! bool - error
		{
			op2.vValue.iIntConst = 0;
			op2.vType = CValueVarient::isIntConst;
			op2.vIsUnsigned = false;
			op2.vIsValid    = false;
			rc = APP_EXPR_TYPE_ERR;
		}
		//else it's already int
		
		if (op1.vIsUnsigned || op2.vIsUnsigned){returnVarient.vIsUnsigned = true;}
		else{returnVarient.vIsUnsigned = false;}
		op1.vIsUnsigned = op2.vIsUnsigned = returnVarient.vIsUnsigned;
	}
	else
	if (op1.vType != CValueVarient::isBool || op2.vType != CValueVarient::isBool) // error
	{
		returnVarient.vType = CValueVarient::isBool;
		
		if (op1.vType != CValueVarient::isBool ) 
		{
			op1.vValue.bIsTrue = false;
			op1.vType = CValueVarient::isBool;
			op1.vIsUnsigned = false;
			op1.vIsValid    = false;
		}
		//else it's already a bool

		if (op2.vType != CValueVarient::isBool ) 
		{
			op2.vValue.bIsTrue = false;
			op2.vType = CValueVarient::isBool;
			op2.vIsUnsigned = false;
			op2.vIsValid    = false;
		}
		//else it's already a bool
		
		rc = APP_EXPR_TYPE_ERR;
	}
	// else both bool - leave 'em be

	returnVarient.vSize =
#if defined(__GNUC__)
			std::max(op1.vSize,op2.vSize);
#else
			__max(op1.vSize,op2.vSize);
#endif // __GNUC__
			
	op1.vSize=op2.vSize = returnVarient.vSize;

	if (op1.vIsValid && op2.vIsValid){returnVarient.vIsValid = true;}
	else{returnVarient.vIsValid = false;}

	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...)  Promote \n");
	}
	//DEEPAK 251104
	return rc;
}

RETURNCODE hCexpression :: toBoolT(CValueVarient& returnVarient, CValueVarient& op1, 
																 CValueVarient& op2)
{
	//RETURNCODE rc = SUCCESS;

	returnVarient.vType = CValueVarient::isBool;
	returnVarient.vIsUnsigned = false;
	returnVarient.vSize = BOOLTSIZE;

	if (op1.valueIsZero())
	{	op1.vValue.bIsTrue = false;	}
	else
	{	op1.vValue.bIsTrue = true;	}

	op1.vType = CValueVarient::isBool;
	op1.vIsUnsigned = false;
	op1.vSize = BOOLTSIZE;

	if (op2.valueIsZero())
	{	op2.vValue.bIsTrue = false;	}
	else
	{	op2.vValue.bIsTrue = true;	}

	op2.vType = CValueVarient::isBool;
	op2.vIsUnsigned = false;
	op2.vSize = BOOLTSIZE;

	return SUCCESS;

}

RETURNCODE hCexpression::resolveAllVarIndexes(hCitemBase* pVar, ExprTrailList_t& exprValueList)
{
	RETURNCODE rc = SUCCESS;
	hCexpressionTrail localExprTrail;
	UIntList_t il;
	hCitemBase* pIB = NULL;
	assert(pVar->getIType() == iT_Variable); // this should have checked by caller
	int varType = ((hCVar*)pVar)->VariableType();

	if ( varType == vT_Index  ) 					 
	{// index OK
		if ( ((hCindex*)pVar)->getAllValidValues(il) <= 0)// returns count 
		{
			if (((hCindex*)pVar)->pIndexed != NULL && 
				((hCindex*)pVar)->pIndexed->getArrayPointer(pIB) == SUCCESS  &&
				pIB->getIType() == iT_List )
			{
				rc = DB_ELEMENT_NOT_FOUND; // we supress empty list errors
			}
			else
			{
				LOGIT(CERR_LOG,"  ERROR: index expression fetch with no valid values.\n");
			}
		}
		else
		{
			for (UIntList_t::iterator iT = il.begin();iT<il.end();iT++)
			{// ptr 2a int
				localExprTrail.isVar    = true;
				localExprTrail.srcID    = pVar->getID();// is a var
				localExprTrail.srcIdxVal= *iT;

				exprValueList.push_back(localExprTrail);
				localExprTrail.clear();
			}//next
		}
	}
	// added stevev try 3/19/04
	else
	if ( varType == vT_Enumerated )
	{
		if ( ((hCEnum*)pVar)->getAllValidValues(il) <= 0)// returns count 
		{
			LOGIT(CERR_LOG,"  ERROR: index expression fetch with no valid values.\n");
		}
		else
		{
			for (UIntList_t::iterator iT = il.begin();iT<il.end();iT++)
			{// ptr 2a int
				localExprTrail.isVar    = true;
				localExprTrail.srcID    = pVar->getID();// is a var
				localExprTrail.srcIdxVal= *iT;

				exprValueList.push_back(localExprTrail);
				localExprTrail.clear();
			}//next
		}
	}
	else
	if (varType == vT_Integer || varType == vT_Unsigned)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: expression index resolution for %s (0x%04x)\n"
			"           with a %s (non-index, int) so we cannot get valid values.\n",
				pVar->getName().c_str(),pVar->getID(),((hCVar*)pVar)->VariableTypeString());
		rc = APP_EXPR_INT_AS_INDEX;// added for legacy tokenizer compatability
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: expression index resolution for %s (0x%04x)\n"
			"           with a %s (non-index) so we cannot get valid values.\n",
				pVar->getName().c_str(),pVar->getID(),((hCVar*)pVar)->VariableTypeString());
		rc = APP_EXPR_TYPE_ERR;
	}
	return rc;
}


/* this is expected to only be called for indexes or collections
   if it's a constant, add it and use it
   if it's a variable, verify it's an index and get all valid values
   otherwise - error
**/
// hCexpressionTrail:
//	bool        isVar;		// else is constant
//	itemID_t	srcID;		// @  isVar::  symbol number of the INDEX variable
//	int			srcIdxVal; 

//RETURNCODE hCexpression :: resolveAllIndexes(vector<UINT32>& valueList)// for array expressions
RETURNCODE hCexpression::resolveAllIndexes(ExprTrailList_t& exprValueList)
{// essentially a regular resolve using a valid value list for variable references
	RETURNCODE rc = SUCCESS;
	vector<UINT32> localList;
	
	hCexpressionTrail localExprTrail;
	varientList_t  localExprList;

//XPX	executionStack.clear(); // clear the decks for action..
		//DEEPAK 251104
	try{
	localExprList.clear(); // clear the decks for action..

	// for each in the hRPNexpress_t list of hCexpressElement
	for(     hRPNexpress_t::iterator 
		iT = expressionL.begin(); 
		iT < expressionL.end()   && rc == SUCCESS; 
		iT++)
	{// iT is a ptr 2a hCexpressElement
		hCexpressElement* pEE = &(*iT);
		if (pEE->exprElemType >= eet_NOT && pEE->exprElemType <= eet_NEQ)
		{//		type is opcode,			1 thru 21 
			/**** WE DO NOT SUPPORT true EXPRESSIONS IN INDEXES AT THIS TIME ****/
			LOGIT(CERR_LOG,"WARNING: operation found inside an index expression."
																"(eg [a+b]unsupported)\n");
		}
		else 
		if (pEE->exprElemType >= eet_INTCST && pEE->exprElemType <= eet_FPCST)
		{//		type is constant		22,23
	//					get const value from the varient - dependenct empty
#ifdef DBG_EXECUTION
			clog << "-I EXPR:constant to list." <<endl;
#endif
			localExprTrail.isVar    = false;
			localExprTrail.srcID    = 0;// not a var
			localExprTrail.srcIdxVal= pEE->expressionData;
			//valueList.push_back( (UINT32)iT->expressionData );
			exprValueList.push_back(localExprTrail);
			localExprTrail.clear();
		}
		else
		if (pEE->exprElemType >= eet_VARID && pEE->exprElemType <= eet_MINVAL)
		{//		type is	VarId[Min|Max]	24-26
	//					dependency.depRef should hold a var id - get the value
			hCitemBase* pV = NULL;
			UINT32 uiD = pEE->dependency.depRef.getID();
#ifdef DBG_EXECUTION
			clog << "-I EXPR:0x"<<hex<< uiD <<dec<< " reference." <<endl;
#endif
			rc = devPtr()->getItemBySymNumber(uiD, &pV);// returns pV
			if ( rc != SUCCESS || pV == NULL )
			{
				LOGIT(CERR_LOG,"-I ERROR: getItemBySymNumber was unsuccessful getting id 0x%04x\n",
																							uiD);
			}
			else // we have an item
			if ( pV->getIType() != iT_Variable)
			{
				LOGIT(CERR_LOG,"   --I ItemID (0x%04x) in an index expression is NOT a Variable."
					"(%s)\n",uiD, itemStrings[pV->getIType()]);;
			}
			else
			{// is a var ------- this is where it changes ---------------------------------------
				rc = resolveAllVarIndexes(pV, exprValueList);
			}
		}
		else
		if (pEE->exprElemType >= eet_VARREF && pEE->exprElemType <= eet_MINREF)
		{//		type is	Ref[Min|Max]	27-29
		 //		dependency.depRef should hold a reference - get the value
			hCitemBase* pV = NULL;
			itemID_t uiD = 0;  
			RETURNCODE qr = pEE->dependency.depRef.resolveID( uiD, false );
			if ( qr == SUCCESS && uiD != 0)
			{
#ifdef DBG_EXECUTION
				clog << "-I EXPR:0x"<<hex<< uiD <<dec<< " reference." <<endl;
#endif
				rc = devPtr()->getItemBySymNumber(uiD, &pV);// returns pV
				if ( rc != SUCCESS || pV == NULL )
				{
					LOGIT(CERR_LOG,"-I ERROR: getItemBySymNumber was unsuccessful getting id 0x%04x\n",
																								uiD);
				}
				else // we have an item
				if ( pV->getIType() != iT_Variable)
				{
					LOGIT(CERR_LOG,"   --I ItemID (0x%04x) in an index expression is NOT a Variable."
						"(%s)\n",uiD, itemStrings[pV->getIType()]);;
				}
				else
				if (pEE->exprElemType == eet_VARREF)
				{
					rc = resolveAllVarIndexes(pV, exprValueList);
				}
			}
		}
		else
		{// error - return the invalid varient
			LOGIT(CERR_LOG,"++++++++++++ Invalid varient type +++++++++++++\n");
		}
	}// next RPN expression item
//XPX	executionStack.clear();
	localExprList.clear();
	//if (valueList.size() <= 0 )
	if (exprValueList.size() <= 0 && rc == SUCCESS)
	{	rc = FAILURE;             }

	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Inside catch(...)  hCexpression :: resolveAllIndexes \n");
	}
  //DEEPAK 112504
	return rc;
}


RETURNCODE hCexpression :: resolveIndex(hCreference* pArrayRef, hCindex*& r2pIndex)
{
	RETURNCODE rc = SUCCESS;
	LOGIT(CERR_LOG,"---- SDC needs to Implement resolveIndex Function!!!!\n");
	return rc;
}

bool hCexpression :: isConst(void)  // true if only one value is possible for expression
{
	bool retVal = false;

	// for each in the hRPNexpress_t list of hCexpressElement
	for(     hRPNexpress_t::iterator 
		iT = expressionL.begin(); 
		iT < expressionL.end()   /*stevev 10/13/04 do it all && rc == SUCCESS*/; 
		iT++)
	{// iT is a ptr 2a hCexpressElement
		hCexpressElement *pEE = (hCexpressElement*)&(*iT);
		if (pEE->exprElemType >= eet_VARID )// stevev23jan14 - assume all of the above are variable  && pEE->exprElemType <= eet_MINREF)
		{// just assume the min/maxs are variable....for now at least
			return false;// just assume these are variables
		}
	}
	return true;
}


/*    set this as a duplicate of the passed-in expression */
void  hCexpression :: setDuplicate(hCexpression& rSrc)
{
	hCexpressElement locExprEle(devHndl());
//XPX	executionStack.clear();
	expressionL.clear();
	for (hRPNexpress_t::iterator iT = rSrc.expressionL.begin(); iT < rSrc.expressionL.end();++iT)
	{// iT isa ptr2a hCexpressElement
		locExprEle.setDuplicate(*(iT));
		expressionL.push_back(locExprEle);
		locExprEle.clear();
	}
}

#ifdef USE_ORIGINAL_EXPRESSION
hCexpression& hCexpression :: operator=(aCexpression& srcAExpr) 
{	
	hCexpressElement wrkElem(devHndl());
	aCexpressElement *pExElem;
	for(RPNexpress_t::iterator iT = srcAExpr.expressionL.begin();
							   iT!= srcAExpr.expressionL.end();  ++iT)
	{//it is a ptr 2 aCexpressElement
		pExElem = &(*iT);
		wrkElem = (*pExElem);
		expressionL.push_back(wrkElem); 
		wrkElem.clear();// stevev 5jul06 - get rid of extra stuff
	}
	return *this;
}
#else // USE_OPTIMIZED_EXPRESSION

#define IS_CONSTANT( expElem )  ((expElem.exprElemType == eet_INTCST) || \
			                     (expElem.exprElemType == eet_FPCST ) || \
			                     (expElem.exprElemType == eet_STRCONST)   )


hCexpression& hCexpression :: operator=(aCexpression& srcAExpr) 
{	
	hCexpressElement wrkElem(devHndl()),  resultElem(devHndl()), 
					 OP1Elem(devHndl()),  OP2Elem(devHndl())   ;
	aCexpressElement *pExElem;

	hRPNexpress_t	localStack;
	hRPNexpress_t::iterator iTlocalStk;
	expressElemType_t activeElemType;


	for(RPNexpress_t::iterator iT = srcAExpr.expressionL.begin();
							   iT!= srcAExpr.expressionL.end();  ++iT)
	{//it is a ptr 2 aCexpressElement
		pExElem = &(*iT);
		wrkElem = (*pExElem);
		activeElemType = wrkElem.exprElemType;//save some space...

		if (activeElemType > eeT_Unknown && activeElemType <= eet_NEQ)
		{// deal with an opcode
			if ( localStack.size() < 1 )
			{// previous moved everything to the expression
				// trust it, push the opcode and continue
				expressionL.push_back(wrkElem); // push opcode  on the expression
				wrkElem.clear();
				continue;
			}
			
			// back returns a reference to the actual list entry(ie modifiable)
			OP1Elem = localStack.back();// get the back
			localStack.pop_back();    // doesn't return anything			
			
			if (activeElemType == eet_NOT || /* if we are unary */
				activeElemType == eet_NEG || 
				activeElemType == eet_BNEG )
			{	
				if ( ! IS_CONSTANT( OP1Elem ) )
				{
					expressionL.push_back(OP1Elem); // push param 1 on the expression
					expressionL.push_back(wrkElem); // push this opcode
				}// fall thru to continue
				else
				{// is constant
					resultElem.expressionData = execUnary(activeElemType, OP1Elem.expressionData);
					resultElem.makeConst(); // make the element constant from Varient
					localStack.push_back(resultElem);// for further operations.
					resultElem.clear();
				}// fall thru to continue
				OP1Elem.clear();
				wrkElem.clear();
				continue; // ie   next
			}
			// else opcode is binary;   boolean | numeric
			if ( localStack.size() < 1 )// check for a second operand
			{// previous moved one to the expression
				// trust it, push the other and the opcode and continue
				expressionL.push_back(OP1Elem);
				OP1Elem.clear();
				expressionL.push_back(wrkElem); // push opcode  on the expression
				wrkElem.clear();
				continue;
			}// else keep working
			
			OP2Elem = localStack.back();// get the back
			localStack.pop_back();      // doesn't return anything			
			
			if ( (! IS_CONSTANT( OP1Elem ) ) || (! IS_CONSTANT( OP2Elem )) )
			{// one or the other is not a constant
				expressionL.push_back(OP2Elem);
				expressionL.push_back(OP1Elem);
				expressionL.push_back(wrkElem);
			}
			else // both ARE constant
			{
				if ( activeElemType >= eet_ADD && activeElemType <= eet_XOR ) // numeric
				{
					resultElem.expressionData = execBinaryNumeric(activeElemType, 
											OP1Elem.expressionData, OP2Elem.expressionData);
				}
				else // must be boolean
				{
					resultElem.expressionData = execBinaryBoolean(activeElemType, 
											OP1Elem.expressionData, OP2Elem.expressionData);
				}
				resultElem.makeConst(); // make the element constant from Varient
				localStack.push_back(resultElem);// for further operations.
				resultElem.clear();
			}
			OP2Elem.clear();
			OP1Elem.clear();
		}
		else
		{// deal with a value
			//localStack.push_back(wrkElem);
			expressionL.push_back(wrkElem);
		}
		wrkElem.clear();
	}// next		
	return *this;						
}
#endif

void  hCexprPayload::setEqual(void* pAclass)
{// aCminmaxVal
	aCExpressionPayload* pAcExprPay = (aCExpressionPayload*)pAclass;
// self is a hCexpression
	hCexpression::operator=(pAcExprPay->theExpression) ;
}

void  hCexprPayload::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	hCexprPayload* pExprPay = (hCexprPayload*)pPayLd;

	hCexpression::setDuplicate(*((hCexpression*)pExprPay)) ;
}

RETURNCODE  hCexprPayload::setValue(CValueVarient newValue)
{
	RETURNCODE rc = SUCCESS;
	hCexpressElement wrkElem(devHndl());

	switch(newValue.vType)
	{	
	case CValueVarient::isIntConst:
		wrkElem.exprElemType = eet_INTCST;
		break;
	case CValueVarient::isFloatConst:
		wrkElem.exprElemType = eet_FPCST;
		break;
	/*isBool,
	  isOpcode,
	  isDepIndex,
	  isString,
	  isSymID  */
	default:
		wrkElem.exprElemType = eeT_Unknown;
		rc = APP_TYPE_UNKNOWN;
		break;
	}// endswitch

	wrkElem.expressionData = newValue;

//  self is a hCexpression
	expressionL.push_back(wrkElem);

	return rc;
}

hCexprPayload& hCexprPayload::operator=(const hCexprPayload& srcExpr)
{
	*((hCexpression*)this) = (hCexpression)srcExpr;
	return *this;
}


/*************** hCreferenceList ***********************************************/
/*
 *
 *
 */
hCreferenceList::hCreferenceList(DevInfcHandle_t h)
			:hCreferenceListBase(h)
{
};


hCreferenceList::hCreferenceList(const hCreferenceList& rl)
			:hCreferenceListBase(rl.devHndl())
{ 	
//	payLoadType = rl.plt();
	operator=(rl);
};
hCreferenceList& hCreferenceList::operator= (const hCreferenceList& crl)
{
	hCreferenceListBase::operator=(crl);
	return *this;
};	


RETURNCODE hCreferenceList::getItemPtrs(ddbItemList_t& riL)    // list -> references -> items
{	
	RETURNCODE rc  = SUCCESS;
	hCitemBase* pI = NULL;

	for (HreferenceList_t::iterator iL = begin(); iL < end(); iL++)
	{// iL is a ptr 2 a hCreference
		// resolve the reference		
		rc = iL->resolveID( pI );

		if ( rc == SUCCESS && pI != NULL)
		{
			riL.push_back( pI );// leave rc SUCCESS
		}
		else
		{
			clog << "PROBLEM: reference did not resolve the id to a ptr for RefList.getItemPtrs."<<endl;
			rc = APP_RESOLUTION_ERROR;
		}
	}
	return rc;
}
	
RETURNCODE hCreferenceList::getVarPtrs(varPtrList_t& rlvL)       // list -> references -> items
{	
	RETURNCODE rc  = SUCCESS;
	hCitemBase* pI = NULL;
	itemType_t  iTy;

// #if _MSC_VER >= 1300  /// stevev 20feb07 - this is really a bug - don't know how it worked this far...
// HOMZ - port to 2003, VS7  >> error C2451: conditional expression of type 'std::vector<_Ty>::iterator' is illegal
//	for (HreferenceList_t::iterator iL = begin(); iL < end(); iL++)
//#else
	// error:: for (HreferenceList_t::iterator iL = begin(); end(); iL++)
	for (HreferenceList_t::iterator iL = begin(); iL != end(); ++iL)
//#endif
	{// iL is a ptr 2 a hCreference
		// resolve the reference		
		rc = iL->resolveID( pI );

		if ( rc == SUCCESS && pI != NULL)
		{
			iTy = pI->getIType();
			if ( iTy == iT_Variable )
			{
				rlvL.push_back( (hCVar*)pI );
				// leave rc SUCCESS
			}
		}
		else
		{
			clog << "PROBLEM: reference did not resolve the id ptr for RefList.getVarPtrs."<<endl;
			rc = APP_RESOLUTION_ERROR;
		}
	}
	return rc;
}

RETURNCODE hCreferenceList::resolveAllIDs(UIntList_t&  allPossibleIDsReturned, tokCompatability_t retTypes)// append only
{
	RETURNCODE rc = SUCCESS;
	hCreference* pRef;
	for (HreferenceList_t::iterator iL = begin(); iL != end(); ++iL)
	{// iL is a ptr 2 a hCreference
		pRef = &(*iL);
		// resolve the reference		
		rc |= pRef->resolveAllIDs(allPossibleIDsReturned, true, retTypes);
	}
	return rc;
}


void hCreferenceList::append(hCreferenceList* pList2Append) // insert a list behind a list
{
	reserve(size() + pList2Append->size());		//PO reserving to prevent multiple allocations
	insert(end(),pList2Append->begin(),pList2Append->end());
}


void hCreferenceList::setEqual(void* pAclass) // a hCpayload required method	                   
{// assume: pAclass pts to a  class aCclass : public aPayldType, public AreferenceList_t
 //    and  typedef vector<aCreference>        AreferenceList_t;
	
	if (pAclass == NULL) return;//nothing to do

	aCreferenceList* paL = (aCreferenceList*)pAclass;
	
	
//	hCmenuItem  wrkMenuItem(devHndl());
	hCreference wrkReference(devHndl());
	reserve(paL->size());	//PO reserve setequal
	for(AreferenceList_t::iterator iT = paL->begin(); iT<paL->end(); iT++)
	{ // iT is ptr 2 aCreference
		if (&(*iT) != NULL)	// PAW 03/03/09 &(*) added
		{
			wrkReference = *iT;      /* overloaded operator= gives conversion*/
			push_back(wrkReference); /* referenceList (self).*/
		//	wrkReference.clear();

			wrkReference.destroy();	
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: received a NULL ReferenceList Reference ptr.\n");
		}
	} 
}


void hCreferenceList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parent)	                   
{
	if (pPayLd == NULL) return;//nothing to do

	hCreferenceList* paL = (hCreferenceList*)pPayLd;
	
	hCreference wrkReference(devHndl());
	
	for(HreferenceList_t::iterator iT = paL->begin(); iT<paL->end(); ++iT)
	{ // iT is ptr 2 hCreference
		if (&(*iT) != NULL)	// PAW 03/03/09 added &(*)
		{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
	|| defined(__GNUC__)
			wrkReference.duplicate( &(*iT), replicate, parent);
#else
			wrkReference.duplicate(iT, replicate);      
#endif
			push_back(wrkReference); /* referenceList (self).*/
//Vibhor 280306: MEM LEAK FIX
//			wrkReference.clear();
			wrkReference.destroy();
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: received a NULL ReferenceList Reference ptr.\n");
		}
	} 
}

/*Vibhor 121004: Start of Code*/
RETURNCODE hCreferenceList::getValueList(varientList_t & vl,unsigned uMaxSize)
{
	//return getEntryList(vl,uMaxSize,accept_allValues);
	// stevev 04may07 - this is only called from Waveforms which only use numerics
	return getEntryList(vl,uMaxSize,accept_NumericValues);
}
/* the following is the original code body of the above getValueList() function
	stevev - 4may07 - replaced with filtered generic get-list function that
			supports all valid data types.
	RETURNCODE rc = SUCCESS;
	itemID_t        itemNum = 0;
	hCitemBase* pI = NULL;
	itemType_t  iTy ;
	CValueVarient vTmp, iTmp;

	for (HreferenceList_t::iterator iL = begin(); iL != end() && vl.size() <= uMaxSize; iL++)
	{// ptr 2a hCreference
		pI = NULL;
		iTy = iT_ReservedZeta; //default to skip the bad ones
		
		if (iL->getRefType() == rT_Constant )
		{
			if ((rc = iL->resolveExpr(vTmp)) == SUCCESS && vTmp.vIsValid)
			{	
				if (vTmp.vType == CValueVarient::isIntConst || 
					vTmp.vType == CValueVarient::isFloatConst )
				{
					vl.push_back(vTmp);
					 // stevev removed 2nov04 m_List.push_back( *iL );
					// leave rc SUCCESS
				}
				else
				{
					LOGIT(CLOG_LOG,"Non Numeric in constant value.\n");
				}
			}
			else
			{
				LOGIT(CLOG_LOG,"Constant value did not resolve.\n");
			}
		}
		else
		{
			rc = iL->resolveID( itemNum );

			if(SUCCESS == rc && itemNum != 0)
			{
				rc = devPtr()->getItemBySymNumber(itemNum, &pI);
				if ( SUCCESS == rc &&  NULL != pI)
				{
					iTy = pI->getIType();
				}
			}
			else if(SUCCESS == rc && itemNum == 0)// possible constant
			{
				iTy = iT_NotAnItemType;
			}

			switch(iTy)
			{
				case iT_Variable:
					{
						if (iL->getRefType() == rT_via_BitEnum && ((hCVar*)pI)->VariableType() == vT_BitEnumerated)
						{//getDispValue(ulong indexMask);
							//RETURNCODE resolveExpr(CValueVarient& retValue);
							if (iL->resolveExpr(iTmp) == SUCCESS && iTmp.vIsValid)
							{
								vTmp = ((hCBitEnum*)pI)->getDispValue((ulong) iTmp);
								vl.push_back(vTmp);
								// stevev removed 2nov04 m_List.push_back( *iL );
							}// else didn't resolve ERROR:
							else
							{
							}
						}
						else
						{					
							vTmp = ((hCVar*)pI)->getDispValue();
							vl.push_back(vTmp);
							// stevev removed 2nov04 m_List.push_back( *iL );
							// leave rc SUCCESS
						}
					
					} 
					break;
				case iT_Array:
					{
						ddbItemList_t vOptrs2ItmBase;
						ddbItemLst_it ptr2ptr2ItmBase;
						hCarray *pArr = (hCarray*)pI;

						if ( (rc = pArr->getList(vOptrs2ItmBase)) == SUCCESS )
						{// success
							for(ptr2ptr2ItmBase  = vOptrs2ItmBase.begin();
							    ptr2ptr2ItmBase != vOptrs2ItmBase.end()  &&
									  vl.size() <= uMaxSize;                  ++ptr2ptr2ItmBase )
							{
								iTy = (*ptr2ptr2ItmBase)->getIType();// reuse iTy variable for subtype
								if ( iTy == iT_Variable )
								{
									hCVar* pV = (hCVar*)(*ptr2ptr2ItmBase);
									vTmp = pV->getDispValue();
									vl.push_back(vTmp);
								}
								// else - expand items other than array here - None at this time
							}// next array item
						}
						else
						{//failure
							vOptrs2ItmBase.clear();
						}
					}
					break;
				case iT_ItemArray: //???????????????????????????????????????
				case iT_List: //????????????????????????????????????????????
				//case iT_NotAnItemType:
				default: //everything else is invalid here, just skip !!
					{						
						LOGIT(CLOG_LOG,"ReferenceList member is not a constant,variable or array.\n");
					}
					break;						
			}//end switch
		}// endelse = not a constant
	}//endfor
	return rc;

}//End getValueList
********** end code replacement from getValueList() ***********************/

/* stevev 23Aug05: Start of Code *
* NOTE: getEntryList - gets constants and symbolIDs of variables <skips bad ones> 
*                      gets string and bool constants too  
*   stevev 1may07 -- added a filter to this extractor      
* accept_All			fills varient list with constant values and SYMBOL-IDS 
* accept_AllValues,		fills varient list with constant values and variable values 
* accept_NumericMixed,  fills with constant values and SYMBOL-IDS      for NUMERIC values only
* accept_NumericValues  fills with constant values and variable values for NUMERIC values only
*/
/* changed 06jun13-stevev- uMaxSize is no longer used.  These points are needed before we can
		correctly calculate the the point count.  This has to resolve the entire list, needed
		or not.
****************/
RETURNCODE hCreferenceList::getEntryList(varientList_t & vl,unsigned uMaxSize,deRefFilter_t filter)
{
	RETURNCODE       rc  = SUCCESS;
	itemID_t         itemNum = 0;
	hCitemBase*		 pI = NULL;
	referenceType_t  rT;
	itemType_t       iTy ;
	CValueVarient    vTmp, iTmp;
	hCreference*     pRef;

	for (HreferenceList_t::iterator iL = begin(); iL != end()/* && vl.size() <= uMaxSize*/; iL++)
	{// ptr 2a hCreference
		pRef = (hCreference*)&(*iL); // PAW 03/03/09 &(*) added
		pI = NULL;
		iTy = iT_ReservedZeta; //default to skip the bad ones
		rT  = pRef->getRefType();

		if (rT == rT_Constant )
		{
			if ((rc = pRef->resolveExpr(vTmp)) == SUCCESS && vTmp.vIsValid)
			{	
				if (vTmp.vType == CValueVarient::isIntConst   || 
                    vTmp.vType == CValueVarient::isVeryLong   ||   // stevev 14aug08 
					vTmp.vType == CValueVarient::isFloatConst  )
				{// always used
					vl.push_back(vTmp);
				}
				else
				if (vTmp.vType == CValueVarient::isString     || 
                    vTmp.vType == CValueVarient::isWideString ||   // stevev 14aug08 
					vTmp.vType == CValueVarient::isBool         )
				{// used only for non-numeric filter
					if ( filter == accept_All || filter == accept_AllValues)
					{
						vl.push_back(vTmp);
					}
				}
				else
				{
					LOGIT(CERR_LOG|CLOG_LOG,"Erroneous varient type in constant value.\n");
				}
			}
			else
			{// failed or invalid
				LOGIT(CLOG_LOG,"Constant value did not resolve.\n");
			}
		}
		else
		if ( rT == rT_via_BitEnum)
		{/* special processing */
			
			if (pRef->resolveID( pI ) == SUCCESS && pI != NULL && 
				((hCVar*)pI)->VariableType() == vT_BitEnumerated )
			{
				vTmp = pI->getID();

				if (!(vTmp.valueIsZero())             && 
					 pRef->resolveExpr(iTmp) == SUCCESS && 
					 iTmp.vIsValid         )
				{
					//----------------------- filter ------------------------
					if ( filter == accept_AllValues || filter == accept_NumericValues )
					{
						vTmp = ((hCBitEnum*)pI)->getDispValue((ulong)iTmp);// a bool
					}
					else //accept_All || accept_NumericMixed
					{
						vTmp.vIndex = (unsigned long) iTmp;
						vTmp.vIsBit = true;
					}
					vl.push_back(vTmp);
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Expression resolution for a bit in "
						"bit-enum 0x%04x Failed. \n",(itemID_t)vTmp);// discard
				}
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Bit-Enum resolution for a bit "
					"Failed to resolve the Bit-Enum part.\n");// discard
			}
		}
		else
		{// the other 40 reference types
			rc = pRef->resolveID( itemNum );
			pI = NULL;

			if(SUCCESS == rc && itemNum != 0)
			{
				vTmp = static_cast<unsigned int>(itemNum);

				rc = devPtr()->getItemBySymNumber(itemNum, &pI);
				if ( SUCCESS == rc &&  NULL != pI)
				{
					iTy = pI->getIType();
				}
			}
			else if(SUCCESS == rc && itemNum == 0)// possible constant
			{
				iTy = iT_NotAnItemType;
			}

			switch(iTy)
			{
			case iT_Variable:	
				{
					variableType_t vt = (variableType_t)((hCVar*)pI)->VariableType();
					switch (vt)
					{
					case vT_Integer:
					case vT_Unsigned:
					case vT_FloatgPt:
					case vT_Double:
					case vT_Enumerated:
					case vT_BitEnumerated:
					case vT_Index:
                    case vT_TimeValue:      // stevev 14aug08
					case vT_HartDate:
						{
							if (filter == accept_AllValues || filter == accept_NumericValues)
							{
								vTmp = ((hCVar*)pI)->getDispValue();
							}
							// else - leave it the symbol number
							vl.push_back(vTmp);
						}
						break;// added 13jul07 - fixed an oopsy
					case vT_Ascii:
					case vT_PackedAscii:
					case vT_Password:
						{
							if (filter == accept_All)
							{
								vl.push_back(vTmp);
							}
							else
							if (filter == accept_AllValues)
							{
								vTmp = ((hCVar*)pI)->getDispValue();
								vl.push_back(vTmp);
							}
							// else non-numeric:: quietly skip it
						}
						break;
			
					case vT_unused:
					case vT_undefined:
					case vT_BitString:
					case vT_Time:
					case vT_DateAndTime:
					case vT_Duration:
					case vT_EUC:
					case vT_OctetString:
					case vT_VisibleString:
//					case vT_TimeValue:
					case vT_Boolean:
					case vT_MaxType:
					default:
						/* discard without comment */
						break;
					}// endswitch				
				} 
				break;
			case iT_Image:	
				{
					vl.push_back(vTmp);// use symbol# as-is
				}
				break;

			case iT_ItemArray:
				{// put the entire array into the list
					hCitemArray* pArr = (hCitemArray*)pI;
					groupItemPtrList_t  pGIDlist;
					groupItemPtrIterator_t pgIT;
					hCitemBase*  pItm = NULL;

					if (pArr->getList(pGIDlist) != SUCCESS)
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: reference to list failed to resolve.\n");
						break;// out of switch
					}// else process it

					for (pgIT = pGIDlist.begin(); pgIT != pGIDlist.end(); ++pgIT )
					{// ptr2a ptr2a hCgroupItemDescriptor
						rc = (*pgIT)->getRef().resolveID(pItm);
						if ( pItm != NULL && rc == SUCCESS)
						{
							if (filter != accept_All)// then has to be a variable
							{								
								if (pItm->getIType() == iT_Variable)
								{
									if (filter == accept_NumericMixed || 
										filter == accept_NumericValues )
									{
										if (((hCVar*)pItm)->IsNumeric())
										{
											if (filter == accept_NumericValues)
											{
												vTmp = ((hCVar*)pItm)->getDispValue();
											}
											else // accept_NumericMixed
											{
												vTmp = pItm->getID();;
											}
											vl.push_back(vTmp);
										}
										// else - non-numeric..quietly skip it
									}
									else // accept_AllValues
									{
										vTmp = ((hCVar*)pItm)->getDispValue();
										vl.push_back(vTmp);
									}
								}
								else
								{// discard it
									LOGIT(CLOG_LOG,"Ref Array entry resolved to non-variable.\n");
								}
							}
							else	// accept_All
							{// only needs a symbol number
								vTmp = pItm->getID();;
								vl.push_back(vTmp);
							}
						}
						else
						{
							LOGIT(CERR_LOG,"Ref Array entry failed to resolve item.\n");
						}
					}// next array item
				}
				break;
			case iT_Array:
				{
					hCarray *pArr = (hCarray*)pI;
					if ( filter != accept_All &&   /* all but All must be a variable */
					     pArr->getTypedefType() != iT_Variable)
						 // before name change this was getting typedeftype - now getIType()
						 // stevev-4may07 - getIType will always be an Array!-back to typedeftype
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: DD trying to to get values "
							"from non-Variable array %s\n",pArr->getName().c_str());
						break; // out of switch
					}
					int i;
					bool getVal = false, getNum = false;
					/* reduce filter to simple bool decision */
					pI = (*pArr)[0];
					if ( pI == NULL )// error
					{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Internal, Array held a NULL pointer!\n");
						break; // out of switch
					}
					
					if ( filter != accept_All )//accept_AllValues|accept_NumericMixed|NumValues
					{// we already know it is a variable (from test above)			
						if (filter == accept_AllValues)
						{
							getVal = true;
						}
						else// accept_NumericMixed | accept_NumericValues
						if ( ((hCVar*)pI)->IsNumeric() )
						{
							if (filter == accept_NumericMixed)
								getNum = true;
							else //accept_NumericValues
								getVal = true;
						}
						else // not from this array
						{	
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: DD trying to to get Numeric values "
								"from non-Numeric Variable array %s\n",pArr->getName().c_str());
							break; // out of switch
						}
					}
					else // accept_All
					{
						getNum = true; // get symbol Num ber
					}
					
					/* we are adding the entire array...*/
					for ( i = 0; i < pArr->getSize(); i++)
					{
						pI = (*pArr)[i];
						if (pI == NULL)
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Internal, Array held a NULL pointer!\n");
							continue; //next
						}
						if (getVal)
						{
							vTmp = ((hCVar*)pI)->getDispValue();
							vl.push_back(vTmp);
						}
						if (getNum)
						{
							vTmp = pI->getID();
							vl.push_back(vTmp);
						}
					}//next
				}
				break;
			case iT_List:
				{
					hClist *pLst = (hClist*)pI;
					if ( filter != accept_All &&   /* all but All must be a variable */
					     pLst->getTypedefType() != iT_Variable)
						 // before name change this was getting typedeftype - now getIType()
						 // stevev-4may07 - getIType will always be an Array!-back to typedeftype
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: DD trying to to get values "
							"from non-Variable list %s\n",pLst->getName().c_str());
						break; // out of switch
					}
					unsigned i;
					bool getVal = false, getNum = false;
					/* reduce filter to simple bool decision */
					pI = (*pLst)[0];
					if ( pI == NULL )// error
					{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Internal, List held a NULL pointer!\n");
						break; // out of switch
					}
					
					if ( filter != accept_All )//accept_AllValues|accept_NumericMixed|NumValues
					{// we already know it is a variable (from test above)			
						if (filter == accept_AllValues)
						{
							getVal = true;
						}
						else// accept_NumericMixed | accept_NumericValues
						if ( ((hCVar*)pI)->IsNumeric() )
						{
							if (filter == accept_NumericMixed)
								getNum = true;
							else //accept_NumericValues
								getVal = true;
						}
						else // not from this array
						{	
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"ERROR: DD trying to to get Numeric values "
								"from non-Numeric Variable list %s\n",pLst->getName().c_str());
							break; // out of switch
						}
					}
					else // accept_All
					{
						getNum = true; // get symbol Num ber
					}
					
					/* we are adding the entire array...*/
					for ( i = 0; i < pLst->getCount(); i++)
					{
						pI = (*pLst)[i];
						if (pI == NULL)
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Internal, List held a NULL pointer!\n");
							continue; //next
						}
						if (getVal)
						{
							vTmp = ((hCVar*)pI)->getDispValue();
							vl.push_back(vTmp);
						}
						if (getNum)
						{
							vTmp = pI->getID();
							vl.push_back(vTmp);
						}
					}//next
				}
				break;
				//case iT_NotAnItemType:
			default: //everything else is invalid here, just skip !!
				{						
					LOGIT(CLOG_LOG,"ReferenceList member is not a constant,variable or array.\n");
				}
				break;						
			}//end switch
		}// endelse = not a constant
	}//endfor
	return rc;

}/*End getEntryList*/

RETURNCODE hCreferenceList::getMethodCallList(methodCallSource_t mSrc,methodCallList_t & methList)
{
	RETURNCODE rc = SUCCESS;

	ddbItemList_t wrkItemList;
	itemType_t			 iTy;
	hCmethodCall         wrkMthCall;
	CValueVarient        retVal;

	rc = getItemPtrs(wrkItemList);

	if(SUCCESS == rc && wrkItemList.size() > 0)
	{
		for(ddbItemList_t::iterator iT = wrkItemList.begin(); iT!= wrkItemList.end(); iT++)
		{
			//iT isa ptr 2a ptr 2a hCitemBase
			iTy = (*iT)->getIType();
			if ( iTy != iT_Method)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: non-Method type in a Pre/Post action list.\n");
			}
			else
			{// we be good
			//  fill a method call with varID, msrc_ACTION,methodID
				wrkMthCall.methodID  = (*iT)->getID();
				wrkMthCall.source    = mSrc;
				wrkMthCall.paramList.push_back(retVal);
				methList.push_back(wrkMthCall);
				wrkMthCall.clear();
			}
			
		}// next method
	}

	return rc;

}

/*Vibhor 121004: End of Code*/

// stevev 13sep06 // for extra dependency calculations
RETURNCODE hCreferenceList::getRefPointers(HreferencePtrList_t& retPtrs)
{//											vector<hCreference*>
	RETURNCODE rc = FAILURE;
	// we are a vector<hCreference>
	HreferenceList_t::iterator itRef;
	hCreference* pRef;
	int c = 0;

	for (itRef = begin(); itRef != end(); ++itRef )
	{
		pRef = (hCreference*)&(*itRef);// PAW 03/03/09 &(*) added
		if (pRef != NULL )
		{
			retPtrs.push_back(pRef);
			rc = SUCCESS;
		}
	}
	return rc;
}

bool hCreferenceList::operator==(const hCreferenceList& ri )
{
	bool ret = false;

	HreferenceList_t::iterator myRef;
	HreferenceList_t::const_iterator riRef;//ptr to ref PAW 03/03/09 added const
			// stevev: a const_iterator is an iterator that points to const
	if ( size() != ri.size() )
	{// not equal
		ret = false;// different
	}
	else
	{// same number of items
		if ( size() == 0 ) return true;// they are empty so equal

		for(myRef=begin() , riRef = ri.begin();  
			myRef != end() && riRef != ri.end(); 
			++myRef,++riRef)
		{
			if ( (*myRef) == (*riRef) )
			{// we are still equal
				ret = true;
			}
			else
			{	ret = false;
			}
			if ( ! ret ) 
				break;// out of for loop to return false
		}
	}

	return ret;
}


/************************* hCreferenceListBase ***********************************************/
/*
 *
 *
 */
RETURNCODE hCreferenceListBase::destroy(void)
{	
	RETURNCODE rc = 0;
	hCreference* pR;
	FOR_this_iT(HreferenceList_t,this)
	{ 
		pR = &(*iT);
		rc |= pR->destroy(); 
	}
	return rc;
}

hCreferenceListBase& hCreferenceListBase::operator= (const hCreferenceListBase& crl)
{
	HreferenceList_t::const_iterator ciT;
	const hCreference* pRef;

	for (ciT = crl.begin(); ciT != crl.end(); ++ ciT)
	{
		pRef = &(*ciT);
		HreferenceList_t::push_back(*pRef);//copy construct a reference to match
	}
	return *this;
}


/*********************************************************************************************
 *
 *   $History: ddbReference.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
#undef STEVE_SPEED_UP	// get weight opt code 01/09/09 PAW
#ifdef STEVE_SPEED_UP	// get weight opt code 01/09/09 PAW
/* stevev 12aug09 - used to determine if the reference can ever point to other
   than the current item.  This would mean there is an array with a variable index
   in the reference somewhere.
 */
bool hCreference::isConst(void) // true for unindexed references
{
	bool retVal = false;
	referenceType_t rt;

	if ( ! isRef || ( isRef && id != 0) )
	{
		return true; // a straight id
	}// else, do other possibilities

	rt = getRefType();
	switch (rt) 
	{
	case rT_Item_id:
	case rT_ItemArray_id:
	case rT_Collect_id:
	case rT_Block_id:
	case rT_Variable_id:
	case rT_Menu_id:
	case rT_Edit_Display_id:
	case rT_Method_id:
	case rT_Refresh_id:
	case rT_Unit_id:
	case rT_WAO_id:
	case rT_Record_id:
	case rT_Array_id:
	case rT_VarList_id:
	case rT_Program_id:
	case rT_Domain_id:
	case rT_ResponseCode_id:
	/* stevev 25oct04 eddl types */	
	case rT_File_id:
	case rT_Chart_id:
	case rT_Graph_id:
	case rT_Axis_id:
	case rT_Waveform_id:
	case rT_Source_id:
	case rT_List_id:
	case rT_Grid_id:
	case rT_Blob_id:
		{
			retVal = true; // a straight id	
		}
		break;
	case rT_via_Record:			/* record reference */
	case rT_via_VarList:		/* variable list reference */
	case rT_via_Chart:		// 40 - pMemberAttr to Source
	case rT_via_Graph:		//    - pMemberAttr to Waveform		
	case rT_via_Source:		//    - pMemberAttr to Variable
	case rT_via_Collect:		/* collection reference */
	case rT_via_File:			/* stevev 25oct04 eddl new VIA type */
	case rT_viaItemArray:		/* item array reference */
	case rT_via_Array:		
	case rT_via_List:
		{
			retVal = false; // handle all the else cases
			if (pExpr != NULL && pExpr->isConst())
			{
				if ( pRef != NULL && pRef->isConst() )
				{
					retVal = true;// both ref and expression are const
				}
			}
		}
		break;
	case rT_Separator:
	case rT_Row_Break:
	case rT_Constant:
	case rT_via_BitEnum:	// assume it can't reference something else
		{
			retVal = true;
		}
		break;	
	case rT_via_Blob:			/* stevev 14nov08 new item type (attr only)*/
	case rT_Image_id:			//assume it's conditional
	case rT_via_Attr:		//assume it's conditional
	case rT_via_Param:			/* param name ref */
	case rT_via_ParamList:	/* param list ref */
	case rT_via_Block:	/* block characteristics ref */
	default:
		{
			retVal = false;
		}
		break;
	}// endswitch	


	return retVal;
}

bool hCexpression :: isConst(void)  // true if only one value is possible for expression
{
	bool retVal = false;

	// for each in the hRPNexpress_t list of hCexpressElement
	for(     hRPNexpress_t::iterator 
		iT = expressionL.begin(); 
		iT < expressionL.end()   /*stevev 10/13/04 do it all && rc == SUCCESS*/; 
		iT++)
	{// iT is a ptr 2a hCexpressElement
		hCexpressElement *pEE = (hCexpressElement*)&(*iT);
		if (pEE->exprElemType >= eet_VARID && pEE->exprElemType <= eet_MINREF)
		{// just assume the min/maxs are variable....for now at least
			return false;// just assume these are variables
		}
	}
	return true;
}


#endif
/**********************************************************************************************
 *
 *   Notes:  Help concatenation
 * 
 *  ***************** ***************** ***************** ***************** *****************
 * Collection, Reference Array or File----extracted from spec 500, rev 13 from websit--09nov11-

 ElmHEx=>Element/Item's Help string exists
 ConHEx=>Container's Help string exists
 str!mt=>Help string is Not empty (ie Prefix has already been added)
 ES.   => ERROR: clear the current help string & tell about the error & continue

 Simple__One=>Use referenced item's help or 'No Help Available'
 Simple__Two=>Container's help is the used String
 Simple_Thre=>Element's Help is the used string
 Simple_Four=>Container's help concatonated with Element's help is used string

is-sec	is-bot	str!mt		ElmHEx	ConHEx
false	false	false		false	false    middle   no prefix & no string:  no-op
false	false	false		false	true     middle   no prefix & no string:  no-op
false	false	false		true	false    middle   no prefix:  add elemHelp as Prefix
false	false	false		true	true     middle   no prefix:  add elemHelp as Prefix

false	false	true		false	false    middle   have prefix: no-op
false	false	true		false	true     middle   have prefix: no-op
false	false	true		true	false    middle   have prefix: no-op
false	false	true		true	true     middle   have prefix: no-op

false	true	false		false	false    bottom   no prefix no strings: * Simple__One
false	true	false		false	true     bottom   no prefix: bottom     * Simple__Two
false	true	false		true	false    bottom   no prefix:            * Simple_Thre
false	true	false		true	true     bottom   no prefix: bottom     * Simple_Four

false	true	true		false	false    bottom   w/ prefix, no Elem: no-op (prefix only)
false	true	true		false	true     bottom   w/ prefix, no Elem: no-op (prefix only)
false	true	true		true	false    bottom   w/ prefix: add ElemHelp as Suffix
false	true	true		true	true     bottom   w/ prefix: add ElemHelp as Suffix

true	false	false		false	false    top      no prefix, no Elem Help: no-op
true	false	false		false	true     top      no prefix, no Elem Help: no-op
true	false	false		true	false    top      no prefix: add Elem Help as prefix
true	false	false		true	true     top      no prefix: add Elem Help as prefix

true	false	true		false	false    top      w/ prefix: ES.
true	false	true		false	true     top      w/ prefix: ES.
true	false	true		true	false    top      w/ prefix: ES., add ElemHelp as Prefix
true	false	true		true	true     top      w/ prefix: ES., add ElemHelp as Prefix

true	true	false		false	false    only     no prefix: * Simple__One
true	true	false		false	true     only     no prefix: * Simple__Two
true	true	false		true	false    only     no prefix: * Simple_Thre
true	true	false		true	true     only     no prefix: * Simple_Four

true	true	true		false	false    only     w/ prefix: ES. * Simple__One
true	true	true		false	true     only     w/ prefix: ES. * Simple__Two
true	true	true	   	true	false    only     w/ prefix: ES. * Simple_Thre
true	true	true		true	true     only     w/ prefix: ES. * Simple_Four
 *********************************************************************************************/

