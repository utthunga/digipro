/**********************************************************************************************
 *
 * $Workfile: ddbMenu.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the menu class implementation
 * 08/08/02	sjv	created from itemMenu.cpp
 *
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */

#include "ddbItems.h"
#include "ddbMenu.h"
#include "ddbDevice.h"    // Vibhor  201904: Added
#include "ddbMenuLayout.h"// stevev 01nov06 added

#define GROUP_IS_SUBCANVAS 1 /* assume this goes through */

/* memory allocation for the strings */
const char menuStyleStrings[MENUSTYLESTRCOUNT] [MENUSTYLEMAXLEN] = {MENUSTYLESTRINGS};
extern char  menuAttrStrings[MENUATRSTRCOUNT] [MENUATRMAXLEN] ;

/***************************** Layout ********************************************************/
/** stevev 26oct06 a helpful macro - pulled from sdc625_drawTypes.cpp with layout code      **/

/* * * * * * * * * * * * * * * * * *
#define SECTIONRESET(w) \
{ \
	long tS = 0; \
	int   zt= 0; \
	int   ed= min(COLS_SUPPORTED-1,w);\
	menuTrePtrLstIT_t mtp;\
		/x* get array max *x/		\
	for ( zt = 0; zt <= ed; zt++) \
	{	\
		if (getColMax(maxRow,zt) > tS) \
			tS = getColMax(maxRow,zt); \
	} \
		/x* set 'em all to that*x/\
	for ( zt = 0; zt <= ed; getColMax(maxRow,zt++)=tS) ;\
		/x* set row for stretch*x/\
	for( mtp = rowList.begin(); mtp != rowList.end(); ++mtp)\
		((hCmenuTree*)*mtp)->setRowSize(brState.currentRowMaxWidth);\
	rowList.clear();\
	DEBUGLOG(CLOG_LOG,"Set RowWidth to %d columns.\n",brState.currentRowMaxWidth);  \
	brState.cursor.yval = tS + 1;	\
	brState.currentRowMaxWidth = brState.cursor.xval = 1; \
}
 * * * * * * * *  * * *  * * * *  * *  * */
// newline with w as rightmost filled column  - as updated  03jul12
#define SECTIONRESET(w) \
{ \
/*change algs	int lowest = setRowWidths(rowList, maxCol); */\
	int lowest = setRowWidths(rowList, maxCol, brState.currentRowMaxWidth); \
    DEBUGLOG(CLOG_LOG,"Reset Individual RowWidths down thru row %d.\n", tS);\
	brState.cursor.yval = lowest + 1;                      \
/* new 10jul13 - get that empty column counted */\
	localTree.canvasSize = max(brState.currentRowMaxWidth, localTree.canvasSize);\
	brState.currentRowMaxWidth = brState.cursor.xval = 1;  \
}


const int horizSizes[] = HORIZSCOPESIZES;
const int vert_Sizes[] = VERT_SCOPESIZES;


int hCmenu::acquireEntryCnt = 0;
int hCmenuTree::canvas_EntryCnt = 0;

/********************************************************************************************/

hCmenu::hCmenu(DevInfcHandle_t h, aCitemBase* paItemBase)
	/* initialize base class */  : hCitemBase(h, paItemBase), menuItems(h),condStyle(h, (char*)&menuStyleStrings)
{// label is set in the base class
	 // ib.AattributeList_t attrLst;// a typedef vector<aCattrBase*>	AattributeList_t;
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(menuAttr_Items); 
	if (aB == NULL)
	{	LOGIT(CERR_LOG,"ERROR: hCmenu::hCmenu<no menu item attribute supplied.>\n");
		LOGIT(CERR_LOG,"       trying to find: %s but only have:",
															menuAttrStrings[menuAttr_Items]);
		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG,"%d, ",(*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 	menuItems.setEqualTo( &(((aCattrMenuItems*)aB)->condMenuListOlists)  ); }

	aB = paItemBase->getaAttr(menuAttr_Style); 
	if (aB == NULL)
	{	// set to default
		//menuStyleEnum_t localMSE(h,menuStyleTable);
		//condStyle.setPayload(localMSE);
		condStyle.setSysEnum(menuStyleTable);// default
	} 
	else // generate a hart class from the abstract class
	{	condStyle.setEqual( aB ); }//&(((aCattrCondLong*)aB)->condLong)  ); }
}


/* from-typedef constructor */
hCmenu::hCmenu(hCmenu*    pSrc,  itemIdentity_t newID) : hCitemBase(pSrc,newID),
	menuItems(pSrc->menuItems),condStyle(pSrc->condStyle)
{
	LOGIT(CERR_LOG,"**** Implement menu typedef constructor.***\n");
}


/* for a single menu-item constructor */
hCmenu::hCmenu(hCmenuItem*  pMenuItm,  itemIdentity_t newID) ://hCitemBase(NULL,newID),
				hCitemBase(pMenuItm->devHndl(), iT_Menu, newID.newID,  0),
				menuItems(pMenuItm->devHndl()), condStyle( pMenuItm->devHndl(), (char*)&menuStyleStrings)
{
//	menu items	menuItems(pSrc->menuItems)  typedef hCcondList<hCmenuList>	condMenuList_t;
	hCmenuList	localMenuList(pMenuItm->devHndl());//class: public hCpayload public hCmenuBase,
												   //                     public menuItemList_t
	localMenuList.push_back(*pMenuItm);
	menuItems.appendNonCond(&localMenuList);

//	style		condStyle(pSrc->condStyle// caller sets theis via setStyle()
}


hCmenu::~hCmenu()
{
	menuItems.destroy();
	hCitemBase::destroy();
}

hCattrBase*   hCmenu::newHCattr(aCattrBase* pACattr)  // builder 
{
	LOGIT(CERR_LOG,"ERROR: menu item accessed unimplemented 'newHCattr'.\n");
	return NULL; // an error
}
/*
hCattrBase* hCmenu::newAttr(struct itemAttrTbl& it) 
{
	// long attrType;
	// long attrId; actually attribute's owner's itemID

	switch(it.attrType) // 77 ENUMERATOR_TAG - only one found
	{
	case MENU_LABEL           :
		{ return (CattrBase*) new CattrMenuLabel(this);}break;
	case MENU_ITEMS        :
		{ return (CattrBase*) new CattrMenuItems(this);}break;
	default                  :
		{ LOGIT(CERR_LOG,"ERROR: Unknown attribute type (%d"
						 ")in Ccommand::newAttr for item Id 0x04h\n",(int)it.attrType, 
						it.attrId);
		} break;
	}
	return NULL;
}
*/
/* loads all available attributes into list 
	from:: eval_item_menu(... 
*/
#if 0
******** if 0'd code removed 30mar05 - use earlier version to see what was here **************
#endif // 0 don't use

// NULL means it exists but not loaded <prototype--not loaded yet>
//		caller's responsability to have an empty list on entry (or not if that's required)
RETURNCODE hCmenu::aquire(menuItemList_t& menuItemLst, const menuHist_t& history)
{
	RETURNCODE rc = FAILURE;
	//vector<hCmenuList>* pListOfMenuItems = NULL;
	hCmenuList ListOfMenuItems(devHndl());
	hCddbDevice *pCurDev = (hCddbDevice*)this->devPtr();
	
	hCitemBase* pItemBase = NULL;

	unsigned uMenuItemType = 0;

	rc = menuItems.resolveCond(&ListOfMenuItems);  // a hCcondList<hCenumList>
	
	if (rc != SUCCESS)//sjv 23oct06:condition is OK || ListOfMenuItems.size() <= 0 ) 
	{
		// leave what they brung us....sjv 23oct06  menuItemLst.clear();	
	    return rc; // Vibhor : to clarify with Steve
	}
	else if (ListOfMenuItems.size() <= 0)
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		return SUCCESS;//nothing to do
	}
	else
	{	
		//return (ListOfMenuItems.getItemPtrs(menuItemLst));
//sjv 17jan05		menuItemLst = ListOfMenuItems;
//		if(this->getStyle() == menuStyleUnknown) //for old style menus
//			return SUCCESS;//This is a workaround for ROOT_MENU

	//	rc = ListOfMenuItems.getItemPtrs(menuItemLst);

#if 1//0
	//	rc = SUCCESS;
/*	for (vector<hCmenuList>::iterator iT = pListOfMenuItems->begin(); 
		                               iT != pListOfMenuItems->end();     ++iT )
		{// iT isa ptr 2 a hCmenuList
			// concatonate the lists
			//RETURNCODE getItemPtrs(vector<hCmenuItem*>& miL);
			rc |= iT->getItemPtrs(menuItemLst);
		} */
/*Vibhor 200904: Start of code*/

//sjv 17jan05		if (rc == SUCCESS && menuItemLst.size() > 0)
		if (rc == SUCCESS && ListOfMenuItems.size() > 0)
		{
//sjv 17jan05			for (vector<hCmenuItem>::iterator miT = menuItemLst.begin(); 
//sjv 17jan05												miT != menuItemLst.end(); miT++)
			for (vector<hCmenuItem>::iterator miT = ListOfMenuItems.begin(); 
														miT != ListOfMenuItems.end(); miT++)
			{
				bool bIsNotAnItem = false;

				hCmenuItem* pMenuItem = &(*miT);

				switch(pMenuItem->getRef().getRefType())
				{
					case rT_Constant:
						pMenuItem->setDrawAs(mds_constString);
						bIsNotAnItem = true;
						break;
					case rT_Separator:
						pMenuItem->setDrawAs(mds_separator);
						bIsNotAnItem = true;
						break;
					case rT_Row_Break:
						pMenuItem->setDrawAs(mds_newline);
						bIsNotAnItem = true;
						break;
					default:
						break; 

				}
				if(bIsNotAnItem == true)
				{					
					menuItemLst.push_back(*pMenuItem);
					continue; // get the next one
				}
				rc = pCurDev->procure(pMenuItem->getRef(), &pItemBase);
				if( rc == SUCCESS && pItemBase != NULL)
				{					
					if ( ! (pItemBase->IsValid() ) )
					{	continue; }

					/*  stevev 30mar05 - code that was here was moved to menuItem */
					pMenuItem->fillDrawType(pItemBase, history);
					pMenuItem->pItemBase = pItemBase;
				
				}//endif // no else  rc == SUCCESS && pItemBase != NULL
				else
				if (rc == DB_NO_LIST_ELEMENT)
				{
					if ( devPtr()->getPolicy().abortOnNoListElem )
					{
						menuItemLst.clear();
						return rc;// stevev 11apr11 - we are supposed to stop AbortDD()
					}
					else
					{
						continue;
					}
				}
				else// stevev 07apr06 -from collection as a menu code...
				{	
					wstring wS = getWName();
					LOGIT(CERR_LOG,L"Menu '%s' did not procure a reference.\n", wS.c_str());
					continue; // get the next one
                }
				menuItemLst.push_back(*pMenuItem);
			
			}//endfor
		
		}//endif rc == SUCCESS && menuItemLst.size() > 0

/*Vibhor 200904: End of code*/

	//	return SUCCESS; 
	/*	else
			return FAILURE; */
	#endif
	}
/*
******** commented code removed for clarity 30mar05 --- see earlier vesions for contents *****
*/
	return rc;
}

/* stevev 31oct06 - added to deal with special case */
/*    this generates all the leaves for the bit enum bits */
bool hCmenu::aquireBitEnum(hCmenuTree& thisMenusDesc, hCitemBase* pB)
{
	hCVar* pVar;
	hCEnum* pEnum;
	RETURNCODE rt = SUCCESS;
	DevInfcHandle_t DevHndl;

	int i;

	// Get the number of bits in the Bit Enum
	pVar = (hCVar*) thisMenusDesc.pItemBase;
	if ( pVar == NULL )
	{
		LOGIT(CERR_LOG|CLOG_LOG,L"aquireBitEnum had no ptr\n");
		return false;
	}
	DevHndl= pVar->devHndl();
	hCenumList		eList(DevHndl);
	hCmenuTree*		pNewTre; (DevHndl);

	
	if (pVar->VariableType() == vT_BitEnumerated)
	{
		pEnum = (hCEnum*)pVar;

		rt = pEnum->procureList(eList);
	}
	else
	{
		LOGIT(CERR_LOG|CLOG_LOG,L"aquireBitEnum had a non-bitenum in the pointer\n");
		return false;
	}

	hCreference localRef(DevHndl);

	// We need to create a entry for each of the bits in the Enum
	hCmenuTree* pParent = &thisMenusDesc;
	i = 1;//position is 1 based
	for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
	{
		pNewTre = new  hCmenuTree(DevHndl);
		pNewTre->pItemBase = thisMenusDesc.pItemBase;
		pNewTre->setDrawAs( mds_bit );
		pNewTre->setQualifier(thisMenusDesc.getQualifier());//combination of menuitem & varble
		localRef.setRef(pEnum->getID(), 0, VARIABLE_ITYPE, false, rT_Item_id);
		pNewTre->setReference(localRef);
		localRef.clear();
	
	    pNewTre->topleft.xval = 1; pNewTre->topleft.yval = i++;
	    pNewTre->hv_size.xval = 1; pNewTre->hv_size.yval = 1;// a bit enum size
		pNewTre->da_size = pNewTre->hv_size;
	  //localTree,branchList - will always be empty
		pNewTre->idx = it->val; // the index of the bit
		pNewTre->setHistory(menuHist_t(mds_bit));
		pNewTre->pParentTree = pParent;

		thisMenusDesc.branchList.push_back(pNewTre);
	}// next menu item
	thisMenusDesc.hv_size.xval = 1;
	thisMenusDesc.hv_size.yval = i;
	thisMenusDesc.da_size = thisMenusDesc.hv_size;

	return false;
}


// define for xmtr-dd    #define _USE_ORIG_AQUIRETREE
#ifdef _USE_ORIG_AQUIRETREE
The original algorithm's code was removed 27Jun12.
Consult an earlier version of this file if you want to see that code.
#endif //end-else _USE_ORIG_AQUIRETREE


#define ANYBRANCH   (thisMenusDesc->branchList.size() )
#define NOBRANCH    (thisMenusDesc->branchList.end()  )
#define FIRSTBRANCH (thisMenusDesc->branchList.begin())

// when we meet an item that is not menu of style PAGE, we stop adding pages to the tab item
// so we exit the page list and clear the Last Page tracker
#define STOPTABIFYING 	brState.maxPage.clear();  brState.firstPg.clear();

/*  when we come to a menu item that needs to be skipped we have to do some cleanup:
 *  * we clear the new branch we have been working on so it's ready after we loop
 *  * if we have a branch we are comparing against, this skipped item won't be in that
 *    list either.  But we have already incremented that list so we have to decrement
 *    so we get the exact same item after we loop.
 *  * else (we aren't comparing) we just set the compare branch to the end-of-list
 *  * and we continue at the for statement
 */
#define SKIPBRANCH  pNewBranch->clear();										\
					if( ppThisBranch != NOBRANCH && ppThisBranch != FIRSTBRANCH)\
						ppThisBranch--;											\
					else														\
						ppThisBranch = NOBRANCH;								\
					continue

/* stevev 23oct06 - added to do menu layout;  returns has-changed                           */
/*                  exactly like aquire but does the entire tree with sizes and locations   *
 *
 *	Two ways to use this routine.
 * a) to generate an entire sized and located menu tree
 *		thisMenusDesc.branchList is empty and targetID == 0
 *		will return hasChanged as true and all the branchLists full from menuItemPtr pB down
 * b) as an update filter
 *	thisMenusDesc.branchList holds the existing tree branch;  targetID==SymbolID to be checked
 *	will return hasChanged: true/false and, if true, all the branchLists under it are modified
 *		and pB will hold a pointer of the highest hCmenuTree that changed
 *		
 *********************************************************************************************/
/*static*/
bool hCmenu::aquireTree(hCitemBase* pB, hCmenuTree* thisMenusDesc, const menuHist_t& history,
								itemID_t targetID)
{
	bool ret = aquireInternalTree(pB, thisMenusDesc, history, targetID);
	thisMenusDesc->rowSize = max(thisMenusDesc->rowSize, thisMenusDesc->da_size.xval);
	acquireEntryCnt = 0;// make sure it is reset (error conditions could leave it set)
	return ret;
}

/* 25jul12 - stevev - The spec forces a virtual row break before and after a non-inline image.
   We have to keep track of the previous menu item so we don't get an extra rowbreak before an
   image.  This static is used for that function.
   13feb15 - stevev - the new definition of ColumnBreak (from the test 'spec')makes it take up
   a horizontal space when followed by another separator. We use this static to dtermine that.
******/
static menuItemDrawingStyles_t lastMenuItem = mds_do_not_use;

/*static*///   this4static   description of this menu  history (parent's type + restrictions)
bool hCmenu::aquireInternalTree(hCitemBase* pB, hCmenuTree* thisMenusDesc, const menuHist_t& history,
								itemID_t targetID)
{
	RETURNCODE rc  = SUCCESS;
	bool didChange = false, 
		 anyChange = false,
		 rebuild   = (thisMenusDesc->branchList.size() <= 0);// case (a)
	hv_location_t  entryValues(*thisMenusDesc);//->topleft,thisMenusDesc->hv_size);//loc,sz
	hCddbDevice   *pCurDev  = (hCddbDevice*)pB->devPtr();
	hCmenuTree*    pNewBranch = NULL;// pointer2a new tree, will be pushed on localTree
	hCmenuTree*    pParent = thisMenusDesc;
	//				  ptr 2a ptr 2 the thisMenusDesc  branch corresponding to the menu item
	menuTrePtrLstIT_t ppThisBranch;
	hCmenuTree*    pThisBranch;
	ulong  menuID =   pB->getID();// for debugging
	menuItemDrawingStyles_t 
					pBs_Draw_As = thisMenusDesc->getDrawAs();
	layState_t     brState;// branch state; has to stay local...this is a static function
	hCmenuTree     localTree(*thisMenusDesc);// the copy we are filling
											 // it might get copied to thisMenusDesc
	menuHist_t     branchHist;
	maxColumn      maxCol;	// encapsulates functions done to the column-maxRow map

	hCmenuList ListOfMenuItems(pB->devHndl());
	menuTreePtrList_t rowList;// the concept of a row is now required:stevev 21oct10
	  // note that this list does NOT own the memory to which it points
	  // do not delete what is pointed to!
	  // this list is just the subset of the branchlist inserted since the last row break

	hCcollection* pC = NULL;
	hCmenu*       pM = NULL;

	unsigned      thisMenuID = pB->getID(); // for debugging only

	acquireEntryCnt ++;
#ifdef _DEBUG
	if (thisMenuID == 0x4775)
	{
		rc = 0;
	}
	history.dumpHist();
#endif

	if (pB->getIType() == iT_Menu)
	{
		pM = (hCmenu*)pB;
		localTree.fill4Menu(*pM,history);
		rc = pM->aquire(ListOfMenuItems, history);// fills to legal draw type
	}
	else
	if (pB->getIType() ==  iT_Collection )
	{
		pC = (hCcollection*)pB;	
		localTree.fill4Collection(*pC,history);
		rc = pC->aquire(ListOfMenuItems, history);// fills to legal draw type
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)
			        (CERR_LOG|CLOG_LOG,L"ERROR: Programmer sent a non-menu to be laid-out.\n");

		acquireEntryCnt --;

		return false;
	}


// stevev 19oct09 - show what we have, return code or not	
//	if (rc != SUCCESS)
//	{
//		LOGIT(CERR_LOG,L"Menu failed to resolve item list.\n");
//		thisMenusDesc->hv_size.xval = thisMenusDesc->hv_size.yval = 0;
//		thisMenusDesc->topleft.xval = thisMenusDesc->topleft.yval = 0;
//		thisMenusDesc->da_size = thisMenusDesc->hv_size;
//		anyChange = didChange = false;
//	    // will fall thru   return false; 
//	}
//	else 
	if ( ListOfMenuItems.size() <= 0 ) // an empty list of menu items
	{// note that a failed conditional without a default||else stmt will exit here 
	 //      an empty E&H menu will get here too
		thisMenusDesc->hv_size.xval = thisMenusDesc->hv_size.yval = 0;
		thisMenusDesc->topleft.xval = thisMenusDesc->topleft.yval = 0;
		thisMenusDesc->da_size = thisMenusDesc->hv_size;
		anyChange = didChange = false;
		// will fall thru   return false;// nothing to do
	}
	else
	{
#ifdef _DEBUG
		LOGIT(CLOG_LOG,">>>> Start Content List    of 0x%04x (%s) %s w/%u items.\n",
				pB->getID(),pB->getName().c_str(), menuStyleStrings[localTree.getDrawAs()],
				ListOfMenuItems.size());

		if (ANYBRANCH && (thisMenusDesc->pItemBase != pB) )
			LOGIT(CLOG_LOG,L">><<       tree pointer failed to match 'this' ptr.\n");
#endif		
		localTree.hv_size.clear();
		localTree.da_size.clear();
		// done in aquire...rc = ListOfMenuItems.calculateDrawAs(history);

		menuItmLstIT_t miL;
		menuItemDrawingStyles_t ds, dsBr;
		//hCmenuTree     localTree(pB->devHndl());

		if ANYBRANCH // if parameter (thisMenusDesc) has any menuItems then
		{	ppThisBranch = FIRSTBRANCH;//ptr2aptr2a hCmenuTree (if existing)
			if ( thisMenusDesc->branchList.size() != ListOfMenuItems.size() )
			{
				anyChange = true;
			}
		}
		else
			ppThisBranch = NOBRANCH;

		// for each menu item in the list
		for ( miL = ListOfMenuItems.begin(); miL != ListOfMenuItems.end(); 
//11dec13:was		      ++miL, ((ANYBRANCH) ? ++ppThisBranch : ppThisBranch=NOBRANCH)  )
// branchList may be shorter than current list, modify to prevent crash
		      ++miL, ((ANYBRANCH) ? 
			   ((ppThisBranch != NOBRANCH)?++ppThisBranch:ppThisBranch=NOBRANCH)
			  : ppThisBranch=NOBRANCH)  )
		{// is ptr 2a hCmenuItem			
#ifdef _DEBUG
			unsigned itmID = 0;
#endif
			// 27jun12-stop using an iterator as a pointer
			if( ppThisBranch != NOBRANCH  )
			{
				pThisBranch = *ppThisBranch;
			}
			else// it is the end of the vector
			{
				pThisBranch = NULL;
			}

			hCmenuItem *pMi = (hCmenuItem*) &(*miL);// modified for new rules 16mar07

			if (pMi->pItemBase == NULL)
			{
			LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,"%d]>>List Item:     0x0000 (mt)\n", acquireEntryCnt );
			}
			else
			{
			LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,"%d]>>List Item:     0x%04x (%s)\n", acquireEntryCnt,
								pMi->pItemBase->getID(), pMi->pItemBase->getName().c_str()  );	
#ifdef _DEBUG
				itmID = pMi->pItemBase->getID();
#endif
			}


			ds = pMi->getDrawAs();
			branchHist = history;// in case we recurse (copy all the bools from passed in hist)
			branchHist.parentStyle = pBs_Draw_As;// we are the parent, show our style
			if (pMi->IsReview())
			{
				branchHist.inReview = true;
			}

			// the pointer will still be full if we aborted a loop
			if (pNewBranch == NULL)
			{
				pNewBranch = new hCmenuTree(pMi);// generate a new newBranch for this menu item
			}
			else
			{
				*pNewBranch = *pMi;// re-use the previously unused branch
			}

			if ( ds >= mds_do_not_use )
			{// don't use it
				SKIPBRANCH;
			}
			/*********************************************************************************/
			rc   = sizeBranch(pNewBranch, brState, branchHist, pThisBranch, didChange);
			dsBr = pNewBranch->getDrawAs();

			if ( rc == LAYOUT_SKIP_BRANCH )  // returns a success code
			{
				SKIPBRANCH;
				rc = 0;
			}
			else
			if ( rc == LAYOUT_RESET_PAGES ) // returns a success code
			{					
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,L"Reset Other Page sizes to x:%d y:%d\n",
												brState.maxPage.xval,brState.maxPage.yval);
				localTree.setPreviousPages(brState.maxPage); //  also sets their da_sizes;
				rc = 0;
			}
			else
			if ( rc )// all other errors
			{
				LOGIT(CLOG_LOG,"Programmer error in sizeBranch()\n");
				SKIPBRANCH;
			}
			// else continue working
			/*********************************************************************************/
			anyChange |= didChange;

			LOGIF(LOGP_LAYOUT)
				 (CLOG_LOG,
				 "  sizedBranch  x: %2d  y: %2d    w:%2d  h:%2d    dw: %2d  dh: %2d    c:%d\n",
				  pNewBranch->topleft.xval, pNewBranch->topleft.yval,
				  pNewBranch->hv_size.xval, pNewBranch->hv_size.yval,
				  pNewBranch->da_size.xval, pNewBranch->da_size.yval,pNewBranch->canvasSize);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"  sized  Page  x: %2d  y: %2d            cursor at x: %2d   y: %2d\n", 
				 brState.maxPage.xval,brState.maxPage.yval,
				 brState.cursor.xval, brState.cursor.yval );
/* 09mqr15 - implementing 'ColumnBreak Takes Space' (CBTS) concept in the layout.
 * rules:	@CB - if CB generates an auto RB (last col)then the CB will take a space
 *				- if previous entry was a CB, then this one adds a column to the row.
 */
			int iTmp = 0;
			if ( dsBr == mds_separator )
			{// column break the cursor
				if (localTree.getDrawAs() == mds_window)
				{
					LOGIF(LOGP_LAYOUT)(CLOG_LOG,"sepwin ");
				}
				else
				{
					LOGIF(LOGP_LAYOUT)(CLOG_LOG,"seprat ");
				}

				brState.cursor.xval ++;
				if ( brState.cursor.xval > COLS_SUPPORTED ) // won't fit - wrap it
				{
					SECTIONRESET(COLS_SUPPORTED);// should have no impact brState.cursor.xval-1) ;/*****was '1' stevev 21oct10*******/
				}
				else
				if( brState.cursor.xval > brState.currentRowMaxWidth )
					 brState.currentRowMaxWidth = brState.cursor.xval;
				//brState.cursor.yval = maxRow[brState.cursor.xval-1] + 1; // reset or not
				brState.cursor.yval = maxCol.getCol(brState.cursor.xval) + 1;
			//	                      maxCol.setCol(brState.cursor.xval, brState.cursor.yval) ;
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"cursor: x  %d;   y  %d \n",brState.cursor.xval,
																	 brState.cursor.yval);
				lastMenuItem = dsBr;
				maxCol.ColBrk(); // stevev 01Apr15 - implement the immortal-cell paradigm
				SKIPBRANCH; // skip the sizing
			}
			else
			if (dsBr == mds_newline)// rowbreak
			{
			//  form invisible line by setting all to the largest
			/* stevev 21oct10.....that's what section reset does...
			    int max = -1;
				for ( zt = 0; zt < COLS_SUPPORTED; zt++) 
				{	if (maxRow[zt] > max) max = maxRow[zt];	}
				for ( zt = 0; zt < COLS_SUPPORTED; maxRow[zt++]=max) ;
			*/
				if( brState.cursor.xval > brState.currentRowMaxWidth )//stevev 29nov10
					 brState.currentRowMaxWidth = brState.cursor.xval;

				SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS was brState.cursor.xval-1);
				// done in sectionreset
				// brState.cursor.yval = maxRow[brState.cursor.xval-1] + 1; 

				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"rowBrk cursor: x  %d;   y  %d \n",brState.cursor.xval,
																		 brState.cursor.yval);
				
				lastMenuItem = dsBr;
				maxCol.RowBrk(); // stevev 01Apr15 - implement the immortal-cell paradigm
				SKIPBRANCH; // skip the sizing
			}
			else
			if ( dsBr == mds_image )
			{// we have to do an implied rowbreak before and after a non-inline image
				int lastCol = 0;
				// see if it's not inline
				if (! pNewBranch->IsInline() )
				{// see if the last item was a rowbreak
					if (lastMenuItem != mds_newline && brState.cursor.yval > 0)
					{// insert a rowbreak
						if( brState.cursor.xval > brState.currentRowMaxWidth )
							 brState.currentRowMaxWidth = brState.cursor.xval;

						SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS
						LOGIF(LOGP_LAYOUT)(CLOG_LOG,"PreImgBrk cursor: x  %d;   y  %d \n",
													brState.cursor.xval, brState.cursor.yval);
						maxCol.RowBrk(); // stevev 01Apr15 - implement the immortal-cell paradigm
					}
					lastCol = max(pNewBranch->da_size.xval,brState.currentRowMaxWidth);
				}
				else
				{
					lastCol = pNewBranch->da_size.xval + brState.cursor.xval -1;
				}
				// INSERT IMAGE				
				DDSizeLocType    tS = maxCol.setThese2Lowest(brState.cursor.xval, lastCol);
				brState.cursor.yval = tS + 1;
				lastMenuItem = dsBr;

				if ( lastCol > brState.currentRowMaxWidth )
					brState.currentRowMaxWidth = lastCol;

				LOGIF(LOGP_LAYOUT)
				(CLOG_LOG," Image: leveled cursor: x  %d;   y  %d;   w %d;   h %d    i %d.\n",
							brState.cursor.xval, brState.cursor.yval,
							pNewBranch->da_size.xval,pNewBranch->da_size.yval,lastCol);

				maxCol.Insert(pNewBranch->da_size, maxCol.CGGG(dsBr));// stevev 01Apr15 - implement the immortal-cell paradigm

/*
				// execute post image rowbreak
				if( brState.cursor.xval > brState.currentRowMaxWidth )
					 brState.currentRowMaxWidth = brState.cursor.xval;
				SECTIONRESET(brState.cursor.xval-1);
				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"PstImgBrk cursor: x  %d;   y  %d \n",
													brState.cursor.xval, brState.cursor.yval);
*/
			}
			else
			if ( dsBr == mds_page )
			{			
				if (brState.cursor.yval > 1 || brState.cursor.yval > 1)
				{
					SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS  was::>pNewBranch->da_size.xval);//rowbreak before insert
					pNewBranch->hv_size.xval = pNewBranch->da_size.xval;
				}
				if ( pNewBranch->da_size.xval > COLS_SUPPORTED ) 
				{// make sure it will fit
					pNewBranch->hv_size.xval =
					pNewBranch->da_size.xval = COLS_SUPPORTED;
				}// else it's OK
				if ( pNewBranch->da_size.xval >  brState.currentRowMaxWidth ) 
					brState.currentRowMaxWidth = pNewBranch->da_size.xval;
				lastMenuItem = dsBr;
			}
			else
/* * * * * * * * * 
			if ((iTmp = (pNewBranch->da_size.xval + brState.cursor.xval -1)) <= COLS_SUPPORTED)
			{// fits - insert it
				DDSizeLocType tS = 0;
				// get array max (lowest column cursor this will cover from current cursor)
//looks at this col & next????
				for ( zt = brState.cursor.xval-1; 
				      zt < iTmp; 
				      zt++) 
				{
					if (getColMax(maxRow,zt) > tS) 
						tS = getColMax(maxRow,zt);
				}

				// set 'em all to that (the ones this will cover) 
				for ( zt = brState.cursor.xval-1; 
				      zt < iTmp; 
					  getColMax(maxRow,zt++) = tS   ) ;
* * * * * * * * */
			if ((iTmp = (pNewBranch->da_size.xval + brState.cursor.xval -1)) <= COLS_SUPPORTED)
			{// fits - insert it..iTmp pts to last column to be filled
				//DDSizeLocType    tS = maxCol.setAll2Lowest(brState.cursor.xval, iTmp);
				DDSizeLocType    tS = maxCol.setThese2Lowest(brState.cursor.xval, iTmp);
				brState.cursor.yval = tS + 1;
				lastMenuItem = dsBr;

				if ( iTmp > brState.currentRowMaxWidth )
					brState.currentRowMaxWidth = iTmp;

				LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,"  Fits: leveled cursor: x  %d;   y  %d;   w %d;   h %d    i %d.\n",
							brState.cursor.xval, brState.cursor.yval,
							pNewBranch->da_size.xval,pNewBranch->da_size.yval,iTmp);

				maxCol.Insert(pNewBranch->da_size, maxCol.CGGG(dsBr));// stevev 01Apr15 - implement the immortal-cell paradigm

				// the insertion point debug code has been removed. Microsoft believes my
				//	  instruction to hide this code is merely a suggestion and unhides at whim.
				// removed 25jul12 - see an erlier version if you need that code
			}
			else // doesn't fit
			if ( brState.cursor.xval > 1 ) // if not in the 1st column, we can gain some size
			{// do a section reset
					#ifdef _DEBUG
					if (1)//was (thisMenusDesc->getDrawAs() == mds_window)
					{					
						LOGIT(CLOG_LOG,"NoFit: cursor: x  %d;   y  %d;   w %d;   h %d.\n",
							brState.cursor.xval, brState.cursor.yval,
							pNewBranch->da_size.xval,pNewBranch->da_size.yval);
					}
					#endif
				SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS was::> pNewBranch->da_size.xval-1);//rowbreak before insert
				
				maxCol.RowBrk();// stevev 01Apr15 - implement the immortal-cell paradigm

				pNewBranch->hv_size.xval = pNewBranch->da_size.xval;
				if ( pNewBranch->da_size.xval > COLS_SUPPORTED ) 
				{// make sure it will fit
					pNewBranch->hv_size.xval =
					pNewBranch->da_size.xval = COLS_SUPPORTED;
				}// else it's OK
				if ( pNewBranch->da_size.xval >  brState.currentRowMaxWidth ) 
					brState.currentRowMaxWidth = pNewBranch->da_size.xval;
				lastMenuItem = dsBr;

				maxCol.Insert(pNewBranch->da_size, maxCol.CGGG(dsBr));// stevev 01Apr15 - implement the immortal-cell paradigm

					#ifdef _DEBUG
					if (1)//was (thisMenusDesc->getDrawAs() == mds_window)
					{			
						LOGIT(CLOG_LOG,"Reset: cursor: x  %d;   y  %d;   w %d;   h %d.\n",
							brState.cursor.xval, brState.cursor.yval,
							pNewBranch->da_size.xval,pNewBranch->da_size.yval);
					}
					#endif
			}
			else // doesn't fit && we are in the first column...we have to truncate it, so
			{// insert it anyway 
				DEBUGLOG(CLOG_LOG,"Force: cursor: x  %d;   y  %d;   w %d;   h %d.\n",
						brState.cursor.xval, brState.cursor.yval,
						pNewBranch->da_size.xval,pNewBranch->da_size.yval);

				if ( pNewBranch->da_size.xval > COLS_SUPPORTED ) 
				{
					pNewBranch->hv_size.xval = 
					pNewBranch->da_size.xval = COLS_SUPPORTED;
				}
					#ifdef _DEBUG
					if (1)//was  (thisMenusDesc->getDrawAs() == mds_window)
					{			
						LOGIT(CLOG_LOG,"   to: cursor: x  %d;   y  %d;   w %d;   h %d.\n",
							brState.cursor.xval, brState.cursor.yval,
							pNewBranch->da_size.xval,pNewBranch->da_size.yval);
					}
					#endif
				brState.currentRowMaxWidth = COLS_SUPPORTED;
				lastMenuItem = dsBr;

				maxCol.Insert(pNewBranch->da_size, maxCol.CGGG(dsBr));// stevev 01Apr15 - implement the immortal-cell paradigm

			}

			if (dsBr == mds_page)
			{
				if (brState.firstPg.isEmpty())// this is the first page on the tab
				{
					brState.firstPg = brState.cursor;//  start tab
					//stevev 03jul12 - brState.firstMx = maxRow[brState.cursor.xval-1]; //it is already leveled
					brState.firstMx = maxCol.getLowest(1,COLS_SUPPORTED);
					// add as before 
				}
				else // not the first page
				{
					brState.cursor = brState.firstPg;//all pages in tab start at the same place
					/* stevev 03jul 12 - reset to lowest..
					// reset the level
					for ( zt = brState.cursor.xval-1; 
						  zt < brState.cursor.xval-1 + pNewBranch->da_size.xval;
						  getColMax(maxRow,zt++) = brState.firstMx  );
					* * * */
					maxCol.setAll2Lowest(brState.cursor.xval,
						                 brState.cursor.xval + pNewBranch->da_size.xval - 1);
				}
			}
			else// not a page
			{
				brState.firstPg.clear();// end tab
			}

			pNewBranch->topleft.xval = brState.cursor.xval;
			pNewBranch->topleft.yval = brState.cursor.yval;

			/*  * * * * *
			DDSizeLocType cursorColNum = brState.cursor.xval-1;

			DDSizeLocType newVert = maxRow[cursorColNum] + pNewBranch->da_size.yval;
			// the columns covered have been leveled - add the ysize to 'em 
			//for ( int y = 1, zt = cursorColNum; y <= localTree.hv_size.xval; y++, zt++)
			for (zt = cursorColNum; 
			     zt < cursorColNum + pNewBranch->da_size.xval;// already verified it will fit
				getColMax(maxRow,zt++) = newVert );
			* * * * * * * * * * * */
#ifdef ORIG_LEVEL
			// level from insertion point for width, to insertion point value??? 
			//  this shouldn't be needed 
			maxCol.setAll2(brState.cursor.xval,
						   brState.cursor.xval + pNewBranch->da_size.xval, 
						   maxCol.getCol(brState.cursor.xval));

			/* level to the y cursor, x cursor stays the same */
			brState.cursor.yval += pNewBranch->da_size.yval;
#else // 03jul12 level
			/* level to the y cursor, x cursor stays the same */
			brState.cursor.yval += pNewBranch->da_size.yval;

			maxCol.setAll2(brState.cursor.xval,
						   brState.cursor.xval + pNewBranch->da_size.xval - 1, 
						   brState.cursor.yval-1);//minus 1 ..we don't want to level next col

#endif
	/* * * *      set our x & y -----( for others to use ) ---------------------
			// get maximum y value
			localTree.hv_size.yval = 
			localTree.da_size.yval = maxRow[0];
			for ( zt = 1; zt < COLS_SUPPORTED; zt++ )
			{
				if ( maxRow[zt] > localTree.da_size.yval )
				{
					localTree.hv_size.yval = 
					localTree.da_size.yval = maxRow[zt];
				}
			}
	* * * * * */// get maximum y value
			localTree.hv_size.yval = 
			localTree.da_size.yval = maxCol.getLowest(1,COLS_SUPPORTED);

			// we need to handle: Var Sep Sep Var  situation ie strip the trailing MT cols
			// we cover zero through the last column with something in it
			DDSizeLocType   maxRight = 0;

			/* stevev - 12jun12 - this is the width of the last row...that will seldom work
			maxRight = brState.currentRowMaxWidth;
			we're about to leave so we need to set the size so our owners can layout around us.
			we have to be the size of our measured canvas (max row width).
			***/
			maxRight = max(brState.currentRowMaxWidth, localTree.canvasSize);
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"    Max: currentRowWidth = %3d  tree Canvas: %3d\n",
				brState.currentRowMaxWidth,localTree.canvasSize);

#define CANSIZ   maxRight  /* was (maxRight + 1) */
			if ( CANSIZ > localTree.canvasSize )
			{
				localTree.canvasSize = CANSIZ;
				LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,L"   local CanvasSize set to %d\n",localTree.canvasSize);
			}
//temp 06may14			localTree.hv_size.xval =
			localTree.da_size.xval = CANSIZ;// needs 1 - 3(5)
			if (dsBr == mds_parent)
				LOGIT(CLOG_LOG,L" Problem ");

// end of cursor update

		//   add tree item to thisMenusDesc->branchList
			localTree.branchList.push_back(pNewBranch);
			rowList             .push_back(pNewBranch);

			/* we have to have an implied rowbreak after the insertion of the image */
			if ( dsBr == mds_image &&  ! pNewBranch->IsInline())
			{
				pNewBranch = new hCmenuTree(pMi->devHndl());// a new newBranch for a rowbreak
				pNewBranch->setDrawAs( mds_newline );

				// execute post image rowbreak
				if( brState.cursor.xval > brState.currentRowMaxWidth )
					 brState.currentRowMaxWidth = brState.cursor.xval;
				SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS was::> brState.cursor.xval-1);
				
				maxCol.RowBrk();// stevev 01Apr15 - implement the immortal-cell paradigm

				LOGIF(LOGP_LAYOUT)(CLOG_LOG,"PstImgBrk cursor: x  %d;   y  %d \n",
													brState.cursor.xval, brState.cursor.yval);

				// and everything to get it just like a RB
				pNewBranch->topleft.xval = brState.cursor.xval;
				pNewBranch->topleft.yval = brState.cursor.yval;

				brState.cursor.yval += pNewBranch->da_size.yval;

				maxCol.setAll2(brState.cursor.xval,
							   brState.cursor.xval + pNewBranch->da_size.xval - 1, 
							   brState.cursor.yval-1);

				localTree.hv_size.yval = 
				localTree.da_size.yval = maxCol.getLowest(1,COLS_SUPPORTED);

				maxRight = max(brState.currentRowMaxWidth, localTree.canvasSize);

				if ( maxRight > localTree.canvasSize )
				{
					localTree.canvasSize = maxRight;
					LOGIF(LOGP_LAYOUT)
					(CLOG_LOG,L"...local CanvasSize set to %d\n",localTree.canvasSize);
				}
				localTree.hv_size.xval =
				localTree.da_size.xval = maxRight;// needs 1 - 3(5)
			//   add tree item to thisMenusDesc->branchList
				localTree.branchList.push_back(pNewBranch);
				rowList             .push_back(pNewBranch);
			}// end of implied rowbreak
#ifdef _DEBUG
			LOGIT(CLOG_LOG,
				L"%d]<<Inserted:   x: %2d  y: %2d     w:%2d  h:%2d   dw:%2d  dh:%2d  rs:%2d",
				acquireEntryCnt, pNewBranch->topleft.xval, pNewBranch->topleft.yval,
				pNewBranch->hv_size.xval, pNewBranch->hv_size.yval,
				pNewBranch->da_size.xval, pNewBranch->da_size.yval,pNewBranch->rowSize);
																//brState.currentRowMaxWidth);
			if ( pNewBranch->pItemBase == NULL )
			{
				if (pNewBranch->getDrawAs() != mds_constString )
					LOGIT(CLOG_LOG,L" No Item Pointer supplied.\n");
				else
					LOGIT(CLOG_LOG,L"  Constant String\n");
			}
			else
			{
			int tmpID = pNewBranch->pItemBase->getID();
				LOGIT(CLOG_LOG,"  0x%04x (%s)\n",
					pNewBranch->pItemBase->getID(),pNewBranch->pItemBase->getName().c_str());
			}
			LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,L"  Total Tree Size After Insertion      x:%2d  y:%2d   "
				L"Cursor   x:%2d  y:%2d \n",
				  localTree.da_size.xval, localTree.da_size.yval,
				  brState.cursor.xval,brState.cursor.yval);
#endif
			//newBranch.clear();
			pNewBranch =NULL; // force a new one
		}// next item
//=============================================================================================
		// stevev 12jun12 we need to do the things that finish the row when we finish the list
		#ifdef _DEBUG
		LOGIT(CLOG_LOG,"Fini Menu's Branch List finished ---------------------------\n");
		#endif

		if( brState.cursor.xval > brState.currentRowMaxWidth )//stevev 29nov10
			 brState.currentRowMaxWidth = brState.cursor.xval;

		SECTIONRESET(brState.currentRowMaxWidth);//SJV_CBTS was::> maxCol.getWidth());

		maxCol.RowBrk();// stevev 01Apr15 - implement the immortal-cell paradigm

		if (brState.currentRowMaxWidth != maxCol.getWidth())//currentRowMaxWidth is always 1 here
		{//brState.currentRowMaxWidth width is always 1
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"RowMaxWidth MISMATCH brState:%d, maxCol:%d\n",
										brState.currentRowMaxWidth, maxCol.getWidth());
		}

		#ifdef _DEBUG
		LOGIT(CLOG_LOG,"cursor: x  %2d;   y  %2d     maxWidth %2d\n",
			brState.cursor.xval, brState.cursor.yval,brState.currentRowMaxWidth);
		#endif

//=============================================================================================


		//		thisMenusDesc->hv_size.yval = cursor.yval;// xval handles as we push 'em
		if ( rebuild || localTree.topleft.isEmpty())
			localTree.topleft.xval = localTree.topleft.yval = 1;// top left corner

	}// endelse - we have menuitems

	/**** tweaks go here **********/
	menuItemDrawingStyles_e mdStyle = localTree.getDrawAs();
	if ( mdStyle == mds_page )
	{// I am a page
		localTree.da_size.yval += TAB_HGT_ROWS;
		/*** FORCE tab style menu to be in the first column and full width****/
#ifdef _DEBUG
		if (localTree.topleft.xval != 1) 
			LOGIF(LOGP_NOT_TOK)(CLOG_LOG,L"ERROR: layout set topleft left to %d.\n",
																	   localTree.topleft.xval);
#endif
		localTree.topleft.xval = 1; // first column
		// the following is no longer spec'd
		// localTree.da_size.xval = COLS_SUPPORTED;
		/*** end FORCE *******************************************************/
#ifdef _DEBUG
		LOGIT(CLOG_LOG,L"%d] new page size x:%d y:%d\n",
							acquireEntryCnt, localTree.da_size.xval,localTree.da_size.yval);
#endif
	}// endif page
	else
	if ( mdStyle == mds_group )
	{
		localTree.da_size.yval += GRP_HGT_ROWS;
#ifdef _DEBUG
		LOGIT(CLOG_LOG,L"%d]new group size x:%d y:%d\n",
							acquireEntryCnt, localTree.da_size.xval,localTree.da_size.yval);
#endif
	}// endif group
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"%d]new %s size x:%d y:%d\n", acquireEntryCnt,menuStyleStrings[mdStyle],
							 localTree.da_size.xval,localTree.da_size.yval);
	}
#endif
	/********* end tweaks *********/

	hv_location_t  exitValues(localTree);

#ifdef _DEBUG
	LOGIF(LOGP_LAYOUT)
				(CLOG_LOG, "%d]< Finished Content List  of 0x%04x (%s)\n",
										acquireEntryCnt,pB->getID(),pB->getName().c_str());
	LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,L"++++ Entry:   tl: %2d,    %2d    sz:%2d,   %2d     hv:%2d,  %2d \n"
                          L"      Exit:   tl: %2d,    %2d    sz:%2d,   %2d     hv:%2d,  %2d  "
				   L"   Row: %d  canvas: %d\n",
					entryValues. topleft.xval, entryValues.topleft.yval,
					entryValues. da_size.xval, entryValues.da_size.yval,
					entryValues. hv_size.xval, entryValues.hv_size.yval,

					exitValues.  topleft.xval, exitValues. topleft.yval,
					exitValues.  da_size.xval, exitValues. da_size.yval,
					exitValues.  hv_size.xval, exitValues. hv_size.yval,
						exitValues.rowSize,    exitValues. canvasSize
		 );
	LOGIF(LOGP_LAYOUT)
				(CLOG_LOG,L"      branch state::  row:  %2d      canvas:%2d\n",brState.firstMx,
															brState.currentRowMaxWidth);
#endif
#ifdef USE_ORIGINAL
	didChange = (exitValues != entryValues);
#else
//stevev 01may13-try-	
	didChange = ((exitValues.hv_size.xval != entryValues.hv_size.xval) ||
				(exitValues.hv_size.yval != entryValues.hv_size.yval) ||
				(exitValues.topleft.xval != entryValues.topleft.xval) ||
				(exitValues.topleft.xval != entryValues.topleft.xval) );
#endif
	if (didChange || anyChange) 
	{
		if ( LOGTHIS( LOGP_LAYOUT ) )
		{
			LOGIT(CLOG_LOG,L"Change detected: Tree set to Local Tree.\n");
			localTree.dumpSelf();
		}
		thisMenusDesc->clear();
		thisMenusDesc->operator=( localTree );
	}
#if 0 /* SECTIONRESET above should have handled this */
	menuTrePtrLstIT_t mtp;
	if (rowList.size())
	{
		/*** stevev 12jun12 - setting the row width to the latest row width really won't work.
		 * modified to set the width to the largest row in this menu.
		 * the row list was cleared in the last section reset so we have to scan the actual
		 * branch list to get the info
		 ***/
		zt = 0;// reuse a local int
		for( mtp = localTree.branchList.begin(); mtp != localTree.branchList.end(); ++mtp)
		{
			pNewBranch = (hCmenuTree*)(*mtp);// reuse the branch pointer so iterator isn't ptr
			if ( pNewBranch->rowSize > zt )
				zt = pNewBranch->rowSize;
		}
		if ( zt > brState.currentRowMaxWidth )
			brState.currentRowMaxWidth = zt;
		DEBUGLOG(CLOG_LOG,"exit - Set RowWidth to %d columns.\n\n",
																   brState.currentRowMaxWidth);
		for( mtp = rowList.begin(); mtp != rowList.end(); ++mtp)
		{// make the end-of-menu act like an end-of-row
			((hCmenuTree*)*mtp)->setRowSize(brState.currentRowMaxWidth);
		}
		rowList.clear();
	}
#endif // 0
	// sjv - added condition
	if ( brState.currentRowMaxWidth > thisMenusDesc->canvasSize )
	{
		thisMenusDesc->canvasSize = brState.currentRowMaxWidth;
		DEBUGLOG(CLOG_LOG,"exit - Set  Canvas  to %d columns.\n\n",thisMenusDesc->canvasSize);
	}
	thisMenusDesc->canvas_EntryCnt = 0;
	if (thisMenusDesc->getDrawAs() != mds_group)
	{
		thisMenusDesc->setInternalCanvas(max(static_cast<DDSizeLocType>(MINIMUM_CANVAS), thisMenusDesc->canvasSize));
	}
	else
	{
		thisMenusDesc->setInternalCanvas( thisMenusDesc->canvasSize );
	}
	// else, there is no recurse so just leave the canvas value
#ifdef _DEBUG	
	if (maxCol.getWidth() != thisMenusDesc->canvasSize)
	{
		LOGIT(CLOG_LOG,L"PROBLEM: maxCol Width= %2d, but our canvas = %2d\n",
												maxCol.getWidth(), thisMenusDesc->canvasSize);
	}
	acquireEntryCnt--;
LOGIF(LOGP_LAYOUT)(CLOG_LOG,L"\n   ---did NOT equal in aquiretree and the destination is::\n");
if LOGTHIS(LOGP_LAYOUT) 
{
	thisMenusDesc->dumpSelf();
}
LOGIT(CLOG_LOG,L"<<<<  End Content List ------------\n");
	LOGIF(LOGP_LAYOUT)
				(CLOG_LOG, "%d]< Finished Content List  of 0x%04x (%s)\n",
										acquireEntryCnt,pB->getID(),pB->getName().c_str());
#endif
	maxCol.logDisplayTrack(" -- aquire exit -- ");

	return didChange;
}
/* end oct 23 code */


#ifdef _DEBUG
  #define  SIZELOG  1
#endif
//================ size the branch ===========================================================
// moved out of aquireInternalTree()  27jun12 to reduce the size of that routine
//
// pNewBranch - the new branch we are sizing - by single item or recursion into menu
// brState    - the parent's branch state that holds NewBranch
// branchHist - the histroy to use to size the NewBranch
// pThisBranch- the branch we are comparing against (or NULL @ no compare)
// didChangeRetVal - is the result of the comparison... true if no compare
//
int hCmenu::sizeBranch(hCmenuTree* pNewBranch, layState_t& brState, menuHist_t& branchHist,
					   hCmenuTree* pThisBranch, bool& didChangeRetVal )
{
	int rc, returncode = LAYOUT_SUCCESS;
	menuItemDrawingStyles_t ds = pNewBranch->getDrawAs();
	hCmenuItem *pMi = dynamic_cast<hCmenuItem *>(pNewBranch);
	bool didChange  = false;

	didChangeRetVal = false;

	if (pMi == NULL)// wasn't the right type or passed in null
	{
		return LAYOUT_PARAM_ERROR;
	}

	if ( pNewBranch->pItemBase == NULL ) // item not found
	{// must be separator or const string
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
					"    Max-Page-Size is cleared (we aren't paging anymore).{NULL item}\n");
#endif
		STOPTABIFYING;
		if ( ds == mds_constString)
		{
			CValueVarient msk; unsigned int x,y;
			if ( pMi->getRef().resolveExpr(msk) == SUCCESS && 
				( msk.vType == CValueVarient::isString || 
				  msk.vType == CValueVarient::isWideString  )  )
			{
				// possible qualifier to limit vertical size
				int sizeQual = (int)HGT_LIMIT(pMi->getQualifier().getBitStr());
				((hCddbDevice*)(pMi->devPtr()))
									->sizeString(&(string(msk)), x, y, vert_Sizes[sizeQual]);
				pNewBranch->hv_size.xval = x,
				pNewBranch->hv_size.yval = y;
			}
			pNewBranch->da_size      = pNewBranch->hv_size;
		}
		else
		if (ds == mds_separator || ds == mds_newline)
		{
			pNewBranch->hv_size.xval = pNewBranch->hv_size.yval = SEP_SIZE;
			pNewBranch->da_size = pNewBranch->hv_size;
		}
		else
		if (ds == mds_blank)
		{
			pNewBranch->hv_size.xval = pNewBranch->hv_size.yval = STR_SIZE;
			pNewBranch->da_size = pNewBranch->hv_size;
		}
		else // not constant, not separator; must be error
		{
			LOGIT(CERR_LOG|CLOG_LOG,L"Layout found no item pointer for item of type "
														L"%s.\n",menuStyleStrings[ds]);
//			SKIPBRANCH; // skip this one
			returncode = LAYOUT_SKIP_BRANCH;
		}
	}
	else	// it has an itembase	
/* pseudo drawing type
*/
	if ( ds >= mds_var )// past all the menu's drawing styles
	{// if not menu
		/* it's a  mds_var,	mds_index,	mds_edDisp,	mds_enum, mds_bitenum, mds_bit,
		   mds_method,	mds_image,	mds_scope,	mds_grid, mds_template,	mds_parent
	   */
#ifdef _DEBUG
		unsigned uid = pNewBranch->pItemBase->getID();
#endif
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is cleared (we aren't paging anymore).{non-menu item}\n");
#endif
		STOPTABIFYING;
		if (pNewBranch->pItemBase->IsReadOnly()||// merge item's handling
			pNewBranch->IsReadOnly()           ||// with menu's qualifier
			branchHist.inReview )    //and merge being in a review menu tree
		{
			hCbitString locBS(pNewBranch->getQualifier());// get existing
			locBS.setBitStr(locBS.getBitStr() | menuItemReadOnly);// or in the RO
			pNewBranch->setQualifier(locBS); // set the new
        }
		if ( ds == mds_bitenum  ) // a unique situation
		{
			aquireBitEnum(*pNewBranch);// has itembase of bitenum & merged qualifier
			// localTree.hv_size.yval += GRP_HGT_ROWS;
		}
		else
		if ( ds == mds_bit )
		{
			pNewBranch->hv_size.xval = BITSIZE_X; pNewBranch->hv_size.yval = BITSIZE_Y;
			pNewBranch->da_size = pNewBranch->hv_size;
		}
		//else
		//if ( ds == mds_image && miL->IsInline )
		//{// this is a constant
		//	localTree.hv_size.xval = INLINEIMAGE_X; 
		//  localTree.hv_size.yval = INLINEIMAGE_Y;
		//}
		else // not bitanything
		{
			UINT64 ul = pMi->getQualifier().getBitStr();
			rc = pNewBranch->pItemBase->setSize(pNewBranch->hv_size,(ulong)ul);//sets hv_size
			pNewBranch->da_size = pNewBranch->hv_size;
			if ( rc != SUCCESS )
			{
				LOGIT(CERR_LOG|CLOG_LOG,L"Layout could not get a size for type %s.\n",
					pNewBranch->pItemBase->getTypeName());
//				SKIPBRANCH; // skip this one				
				returncode = LAYOUT_SKIP_BRANCH;
			}
		}
	}
	else // - is a menu (or collection)
	if (pNewBranch->pItemBase->getIType() == iT_Menu)
	{
		if (ds == mds_dialog || ds == mds_menu || ds == mds_window || ds == mds_table)
		{// not page or group...
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is cleared (we aren't paging anymore).{non-Page menu}\n");
#endif
			STOPTABIFYING;
			if ( branchHist.inPopMenu && ds == mds_menu )
			{// if we are a 'button' menu on a pop-up menu, we need the submenues
			 //    all but menu will stop the tree and not go any deeper
				//now common::> history.parentStyle = ds;// change to ours for children
				//stevev  if(&(*ppThisBranch))	//&(*) added PAW
				//stevev 27jun12 if( ppThisBranch != NOBRANCH  )
				if( pThisBranch )// we are in comparision mode
				{	// bring empty or other branch
					pNewBranch->branchList= pThisBranch->branchList;
					pNewBranch->topleft   = pThisBranch->topleft;
					pNewBranch->hv_size   = pThisBranch->hv_size;
					pNewBranch->da_size   = pThisBranch->da_size;
				}
				else
				{
					pNewBranch->branchList.clear();
					//button, size it appropriately  
					pNewBranch->hv_size.xval = pNewBranch->hv_size.yval = 1;
					pNewBranch->da_size      = pNewBranch->hv_size;
				}
				//                       branchHist has been converted to us-as-parent
	/*reentry*/	didChange = ((hCmenu*)pNewBranch->pItemBase)->
					aquireInternalTree(pNewBranch->pItemBase, pNewBranch, branchHist);
#ifdef SIZELOG
LOGIF(LOGP_LAYOUT)
	(CLOG_LOG,L"---returned from aquiretree with the following::\\\\\\\\\\\\\\\\\\\\\\\\\\n");
if LOGTHIS( LOGP_LAYOUT )
{	pNewBranch->dumpSelf(); }
LOGIF(LOGP_LAYOUT)
	(CLOG_LOG,L"------------\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n");
LOGIF(LOGP_LAYOUT)
	(CLOG_LOG,L"#Acquired:     x: %2d  y: %2d     w:%2d  h:%2d     dw:%2d  dh:%2d    c:%d\n",
	pNewBranch->topleft.xval, pNewBranch->topleft.yval,
	pNewBranch->hv_size.xval, pNewBranch->hv_size.yval,
	pNewBranch->da_size.xval, pNewBranch->da_size.yval,pNewBranch->canvasSize);
#endif
			}
			else
			{//we are a button-- don't go down that branch
				pNewBranch->branchList.clear();
				//button, size it appropriately  
				pNewBranch->hv_size.xval = BUTSIZE_X; 
				pNewBranch->hv_size.yval = BUTSIZE_Y;
				pNewBranch->da_size      = pNewBranch->hv_size;
			}
		}
		else // page, group or method dialog
		{// note that this is only for page and group...
			//if( ppThisBranch != NOBRANCH )
			if ( pThisBranch )// we are in compare mode
			{// bring empty or other branch
				pNewBranch->branchList= pThisBranch->branchList;
				pNewBranch->topleft   = pThisBranch->topleft;
				pNewBranch->hv_size   = pThisBranch->hv_size;
				pNewBranch->da_size   = pThisBranch->da_size;
			}
			else
			{	pNewBranch->branchList.clear();
				pNewBranch->hv_size.clear();  
				pNewBranch->topleft.clear();// restart in corner
				pNewBranch->da_size   = pNewBranch->hv_size;
			}
			//menuHist_t branchHist(history);
			//branchHist.parentStyle = ds;//       converted to us-as-parent
			didChange = ((hCmenu*)pNewBranch->pItemBase)->	
					aquireInternalTree(pNewBranch->pItemBase, pNewBranch, branchHist);
#ifdef SIZELOG
LOGIT(CLOG_LOG,L"xAcquired:     x: %2d  y: %2d     w:%2d  h:%2d     dw:%2d  dh:%2d    c:%d\n",
		  pNewBranch->topleft.xval, pNewBranch->topleft.yval,
		  pNewBranch->hv_size.xval, pNewBranch->hv_size.yval,
		  pNewBranch->da_size.xval, pNewBranch->da_size.yval,pNewBranch->canvasSize);
#endif
			if ( ds == mds_page )
			{//	TABIFY
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is NOT cleared (we are paging).------------------------\n");
#endif
				bool isFirst =((brState.maxPage.yval==0) && (brState.maxPage.xval==0));
/************** now done in the aquiretree for the page **********************************
				pNewBranch->hv_size.yval += TAB_HGT_ROWS;
			/x*** FORCE tab style menu to be in the first column and full width****x/
				pNewBranch->topleft.xval = 1; // first column
				pNewBranch->hv_size.xval = COLS_SUPPORTED;
			/x*** end FORCE *******************************************************x/
				pNewBranch->da_size   = pNewBranch->hv_size;
				LOGIT(CLOG_LOG,"raw page size x:%d y:%d\n",
									pNewBranch->hv_size.xval,pNewBranch->hv_size.yval);
********************************************************************************************/
				// da is the layout size from aquiretree
				if (pNewBranch->da_size.xval > brState.maxPage.xval || 
					pNewBranch->da_size.yval > brState.maxPage.yval)
				{
					if (pNewBranch->da_size.xval > brState.maxPage.xval)
						brState.maxPage.xval = pNewBranch->da_size.xval;
					if (pNewBranch->da_size.yval > brState.maxPage.yval)
						brState.maxPage.yval = pNewBranch->da_size.yval;
					if (!isFirst)
					{// must be done before treeitem is added to branch list
						returncode = LAYOUT_RESET_PAGES;// needs to be done by caller
					}
				}
				else
				{// we are smaller than our predecessors
					LOGIT(CLOG_LOG,L"    Reset This Page size to x:%d y:%d\n",
											brState.maxPage.xval,brState.maxPage.yval);
					pNewBranch->da_size.yval = brState.maxPage.yval;
					pNewBranch->da_size.xval = brState.maxPage.xval;
					//pNewBranch->da_size = pNewBranch->hv_size;
				}
			}
			else // not a page (must be a group)
			{	
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is cleared (we aren't paging anymore).{is-group}\n");
#endif	
			STOPTABIFYING;
			}
		}// endelse this menu type
/************** now done in the aquiretree for the page **********************************
		if ( ds == mds_group )
		{
			pNewBranch->hv_size.yval += GRP_HGT_ROWS;
			pNewBranch->da_size = pNewBranch->hv_size;
		}
********************************************************************************************/
	}// endif it's a menu of type MENU
	else// - is a menu but not a MENU
	if (pNewBranch->pItemBase->getIType() == iT_Collection) 
	{
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is cleared (we aren't paging anymore).{Collection}\n");
#endif
		STOPTABIFYING;
		hCcollection* pColl =( hCcollection* )pNewBranch->pItemBase;
		//stevev    if(&(*ppThisBranch))//&(*) added PAW 03/03/09
		//if( ppThisBranch != NOBRANCH )
		if( pThisBranch )// we are in compare mode
		{// bring empty or other branch
			pNewBranch->branchList= pThisBranch->branchList;
			pNewBranch->topleft   = pThisBranch->topleft;
			pNewBranch->hv_size   = pThisBranch->hv_size;
			pNewBranch->da_size   = pThisBranch->da_size;
		}						
		else
		{
			pNewBranch->branchList.clear();
			pNewBranch->topleft.clear();
			pNewBranch->hv_size.clear();
			pNewBranch->da_size = pNewBranch->hv_size;
		}				
// 28oct10-change to below	didChange = pColl->aquireTree(pColl, pNewBranch, branchHist);
		didChange = aquireInternalTree(pColl, pNewBranch, branchHist, pColl->getID());
		//pNewBranch->hv_size.yval += GRP_HGT_ROWS;
		//done in aquire  pNewBranch->da_size.yval += GRP_HGT_ROWS;

#ifdef SIZELOG
LOGIT(CLOG_LOG,L"@Acquired:     x: %2d  y: %2d     w:%2d  h:%2d     dw:%2d  dh:%2d\n",
		  pNewBranch->topleft.xval, pNewBranch->topleft.yval,
		  pNewBranch->hv_size.xval, pNewBranch->hv_size.yval,
		  pNewBranch->da_size.xval, pNewBranch->da_size.yval);
#endif
	}
	else
	{// not constant, non-menu::> unknown type!
#ifdef SIZELOG
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,
				"    Max-Page-Size is cleared (we aren't paging anymore).{Unknown}\n");
#endif
		STOPTABIFYING;
		LOGIT(CLOG_LOG|CERR_LOG,"Layout could not size type %s. 0x%04x (%s)\n",
				pNewBranch->pItemBase->getTypeName(),pNewBranch->pItemBase->getID(),
				pNewBranch->pItemBase->getName().c_str());
//		SKIPBRANCH; // skip this one
		returncode = LAYOUT_SKIP_BRANCH;
	}
	//anyChange |= didChange;
	didChangeRetVal = didChange;
	//  we have the size, now locate the new tree branch on the window
#ifdef xxxSIZELOG
	LOGIT(CLOG_LOG,L"    PreInsert: x: %2d  y: %2d     w:%2d  h:%2d     dw:%2d  dh:%2d\n",
		pNewBranch->topleft.xval, pNewBranch->topleft.yval,pNewBranch->hv_size.xval, 
		pNewBranch->hv_size.yval,pNewBranch->da_size.xval, pNewBranch->da_size.yval);
#endif
//=============================================================================================
	return returncode;
}

// setRowWidths traverses the passed-in list of items added since the last ROWBREAK
//		for each one, it determines how many columns are occupied and sets the rowWidth
// returns the lowest used row
/* NOTE: 19feb15 - with no record of what Wally's algorithm actually is and what 
					problems it was supposed to resolve, we have test case Test.511:MLT13110
					that needs various width rows between rowbreaks.  That capability will be 
					restored here and we will have to see what breaks in the layout tests.
	These changes will ber designated '// was commented out 4 WAP alg'
****/
int hCmenu::setRowWidths(menuTreePtrList_t& row_List, maxColumn& mxCol, DDSizeLocType mxRowWth) 
{
	int rowWid = 0, rowNumber = 0, maxCol = 0;
	menuTrePtrLstIT_t mtp;
	hCmenuTree*       pTre;
	
	for( mtp = row_List.begin(); mtp != row_List.end(); ++mtp)
	{
		pTre = *mtp;// mtp is a ptr2ptr2tree
		rowNumber = pTre->topleft.yval;	
		rowWid = max(rowWid, mxCol.rowWidth(rowNumber));
	}
	LOGIF(LOGP_LAYOUT)(CLOG_LOG, "setRowWidths maxCol:%d & maxRowWth:%d\n",rowWid,mxRowWth);
	rowWid = 0;
	for( mtp = row_List.begin(); mtp != row_List.end(); ++mtp)
	{
		pTre = *mtp;// mtp is a ptr2ptr2tree
/** change algorithm to Wally's suggested							/  was commented out 4 WAP alg */
		rowNumber = pTre->topleft.yval;								// was commented out 4 WAP alg
		rowWid = mxCol.rowWidth(rowNumber);							// was commented out 4 WAP alg
		//pTre->setRowSize(rowWid);									// was commented out 4 WAP alg
		// for debugging
		if (pTre->pItemBase)
		{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG, "Row %d is 0x%04x as a %s with RowSize changed from %2d to %2d (%d)\n", 
			rowNumber, pTre->pItemBase->getID(), menuStyleStrings[pTre->getDrawAs()], pTre->rowSize, mxRowWth, rowWid);
		}
		else
		{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG, "Row %d is a %s with RowSize changed from %2d to %2d (%d)\n", 
			rowNumber, menuStyleStrings[pTre->getDrawAs()], pTre->rowSize, mxRowWth, rowWid);
		}

		// SJV_CBTS - we'll do the passed in number to see how that works.  The column breaks aren't
		// getting a place in the mcCol class array
		pTre->rowSize = mxRowWth;
		// SJV_CBTS was::> pTre->rowSize = rowWid;// aka last column filled in this row// was commented out 4 WAP alg
		if ( rowWid > maxCol ) maxCol = rowWid; // last full column	// was commented out 4 WAP alg
/***/
		// was NOT commented out 4 WAP alg LOGIF(LOGP_LAYOUT)(CLOG_LOG, "Row %d is a %s with RowSize changed from %2d to %2d\n", 
		// was NOT commented out 4 WAP alg 	++rowNumber, menuStyleStrings[pTre->getDrawAs()], pTre->rowSize, mxRowWth);
		// was NOT commented out 4 WAP alg pTre->rowSize = mxRowWth;
	}// next in the row list
	row_List.clear();// there is a master list holding these pointers, this does not erase them+
/** change algorithm to Wally's suggested /  was commented out 4 WAP alg */
	return mxCol.setAll2Lowest(1,maxCol);// level the column map when we're done// was commented out 4 WAP alg
/****/
// was NOT commented out 4 WAP alg 	int x = mxCol.setAll2Lowest(1,mxRowWth);// level the column map when we're done
// was NOT commented out 4 WAP alg 	LOGIF(LOGP_LAYOUT)(CLOG_LOG, "Column occupancy has been reset to %d on columns 1 - %d\n",
// was NOT commented out 4 WAP alg 			x,mxRowWth);
// was NOT commented out 4 WAP alg 		return x;
}

/*itembase	virtual */
RETURNCODE hCmenu::resolveProcure(void/*self*/) /* resolve this and all the sub stuff*/
{ 
	cerr << "Call to hCmenu resolve Procure." << endl; 
	return VIRTUALCALLERR;
}


/* stevev 18nov05 - overload to get menu specific notifications */

// called when an item in iDependOn has changed
int hCmenu::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	
	hCitemBase::notification(msgList,isValidity);// do validity and label

	// re-evaluate all the others ( item list is only one left )
	if ( (! isValidity) )
		/*** temporary::::   && menuItems.isConditional() )  
		a nonconditional list's item whose validity changes fails this but needs notification.
		stevev 18jun07 ***/
	{// my list could have changed
		// since the contents could change and the references in those contents could change
		// just force a redraw - we may refine this later...
		msgList.insertUnique(getID(), mt_Str);// tell the world
	}// else - no change;

	return (msgList.size() - init);// number added
}
/** end stevev 18nov05  **/


/*Vibhor 200904: Start of code*/

menuStyleType_t hCmenu :: getStyle(void)
{
	//RETURNCODE rc = SUCCESS;

	//menuStyleType_t retStyle;
	//menuStyleEnum_t *pSysStyle = NULL; 

	return condStyle.getSysEnum();

//	if ( rc == SUCCESS && pSysStyle != NULL )
//	{
//		retStyle = pSysStyle->getSysEnum();
//	}
//	else
//	{
//		retStyle = menuStyleUnknown;
//	}
//	return retStyle;


}/*End menuStyleTyipe_t getStyle(void)*/

/*Vibhor 200904: End of code*/

/* stevev 16nov05 - dependency extentions */
void  hCmenu :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// the only other attribute is items
	menuItems.aquireDependencyList(iDependOn,false);//conditional lists: conditional depends on
	/* 02dec05 - menu also depends on the conditional validity of its items **/
	vector<hCmenuList*> localPtr2MLvector;
	vector<hCmenuItem*> localPtr2MIvector;
	vector<hCitemBase*> localPtr2Itemvector;
	hCdependency* pDep = NULL;

	if (menuItems.aquirePayloadPtrList(localPtr2MLvector) == SUCCESS && 
		localPtr2MLvector.size() > 0)
	{
		for (vector<hCmenuList*>::iterator ITMenLst = localPtr2MLvector.begin();
			 ITMenLst != localPtr2MLvector.end();     ++ITMenLst )
		{// ptr2a ptr 2a hCmenuList
			(*ITMenLst)->getItemPtrs(localPtr2MIvector);
		}
		if ( localPtr2MIvector.size() > 0 )
		{
			for ( vector<hCmenuItem*>::iterator iTMenItm = localPtr2MIvector.begin();
				  iTMenItm != localPtr2MIvector.end();		++iTMenItm  )
			{// ptr 2a ptr 2a hCmenuItem
				//(*iTMenItm)->getRef().resolveAllIDs(localPtr2Itemvector);
				referenceType_t rt = (*iTMenItm)->getRef().getRefType();
				if ( rt != rT_Separator && rt != rT_Constant && rt != rT_Row_Break )
				{///stevev 13apr09 - added if stmnt to remove some incorrect error logs
					if ((*iTMenItm)->getRef().resolveAllIDs(localPtr2Itemvector) == SUCCESS )
					{
						(*iTMenItm)->getRef().aquireDependencyList(iDependOn,false);
					}
					else
						if ((!(devPtr()->getPolicy().isPartOfTok)) || LOGTHIS(LOGP_DEPENDENCY))
					{
						LOGIT(CLOG_LOG,"       MenuItem dependency calculation "
														"failed in %s.\n",getName().c_str());
					}
				}// else skip it and go to the next menu item
			}//next menu item
		}
	}
	// we have all possible items now
	for (vector<hCitemBase*>::iterator iT = localPtr2Itemvector.begin();
			iT != localPtr2Itemvector.end(); ++ iT )// typo repair stevev 6jul06
	{// ptr2aptr2a hCitemBase
		pDep = (*iT)->getDepPtr(ib_ValidityDep);
		if ( pDep && pDep->iDependOn.size() > 0)
		{// pDep->iDependOn is list of what this item's validity is dependent on(MT @ constant)
			// stevev 7jul06 - changed from this::> iDependOn.push_back((*iT));
			hCdependency::insertUnique(iDependOn,(*iT));
		}
	}

}
/** end stevev 16nov05  **/

/* stevev 27sep06 - more dependency extentions */
// get all possible items on this menu (menues, edit-displays, all of 'em)
// mark the collections-as-menues & return the list
void  hCmenu::fillContainList(ddbItemList_t&  iDependOn)
{// fill with items-I-contain (use to mark collections as menu-collection */
	vector<hCmenuList*> localPtr2MLvector;

	vector<hCmenuItem*> localPtr2MIvector;
	ddbItemList_t       localPtr2Itemvector;
	ddbItemLst_it       ilocalID;
	hCdependency* pDep  = NULL;
	hCitemBase*   pItem = NULL;

	if (menuItems.aquirePayloadPtrList(localPtr2MLvector) == SUCCESS && 
		localPtr2MLvector.size() > 0)
	{// we have a list of all possible lists
		for (vector<hCmenuList*>::iterator ITMenLst = localPtr2MLvector.begin();
			 ITMenLst != localPtr2MLvector.end();     ++ITMenLst )// for each possible list
		{// ptr2a ptr 2a hCmenuList 
			(*ITMenLst)->getItemPtrs(localPtr2MIvector);	// get the list's items
		}
		if ( localPtr2MIvector.size() > 0 )
		{
			for ( vector<hCmenuItem*>::iterator iTMenItm = localPtr2MIvector.begin();
				  iTMenItm != localPtr2MIvector.end();		++iTMenItm  )// for each menu-item
			{// ptr 2a ptr 2a hCmenuItem
				(*iTMenItm)->getRef().resolveAllIDs(localPtr2Itemvector);
																   // get all possible dd-items
			}
		}
	}

	for(ilocalID=localPtr2Itemvector.begin(); ilocalID != localPtr2Itemvector.end();++ilocalID)
	{// for each item ilocalID isa ptr 2a ptr 2a hCitemBase
		
		itemType_t  daType = (*ilocalID)->getIType() ;
		if (daType == iT_Collection)
		{
			((hCcollection*)*ilocalID)->is_on_menu = true;
		}
		iDependOn.push_back(*ilocalID);
	}
}
/** end stevev 27sep06 **/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// assume the caller has the list in the proper condition
RETURNCODE hCmenuList::getItemPtrs(vector<hCmenuItem*>& miL)
{
	RETURNCODE rc = SUCCESS;
	//menuItemList_t iL;
	for (menuItemList_t::iterator iL = /*itemList.*/begin(); iL < /*itemList.*/end(); iL++)
	{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
		miL.push_back(&(*iL));
#else
		miL.push_back((hCmenuItem*)iL);
#endif
	}
	return rc;
}

void  hCmenuList::setEqual(void* pAclass)
{// assume: pAclass pts to a  class aCmenuList : public aPayldType, public AmenuItemList_t
 //   and   typedef vector<aCmenuItem>         AmenuItemList_t;
	
	if (pAclass == NULL) return;//nothing to do

	aCmenuList* paL = (aCmenuList*)pAclass;
	hCmenuItem wrkMenuItem(devHndl());
	reserve(paL->size());		//PO reserve setequal
	for(AmenuItemList_t::iterator iT = paL->begin(); iT<paL->end(); iT++)
	{ // iT is ptr 2 aCmenuItem
		if (&(*iT) != NULL)	// PAW 03/03/09 added &(*)
		{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
			wrkMenuItem.setEqual(&(*iT));
#else
			wrkMenuItem.setEqual(iT);
#endif
			/*itemList.*/push_back(wrkMenuItem);
			wrkMenuItem.clear();
		}
		else
		{
			cerr <<"ERROR: received a NULL wrkMenuItem ptr."<<endl;
		}
	} 
}

void  hCmenuList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	if (pPayLd == NULL) return;//nothing to do

	hCmenuList* paL = (hCmenuList*)pPayLd;
	hCmenuItem wrkMenuItem(devHndl());
	
	for(menuItemList_t::iterator iT = paL->begin(); iT < paL->end(); ++iT)
	{ // iT is ptr 2 hCmenuItem
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
		wrkMenuItem.setDuplicate(*((hCmenuItem*) (&(*iT)) ));
#else
		wrkMenuItem.setDuplicate(*((hCmenuItem*)iT));
#endif
		push_back(wrkMenuItem);
		wrkMenuItem.clear();
	} 
}

void  hCmenuList::append(hCmenuList* pList2Append)
{
	reserve(size() + pList2Append->size());		//PO reserving to prevent multiple allocations
//TROUBLE WITH THIS 
//	/*itemList.*/insert(/*itemList.*/end(),pList2Append->begin(),pList2Append->end());
	for (menuItemList_t::iterator iT = pList2Append->begin(); 
	     iT < pList2Append->end(); push_back(*iT), iT++);
}


RETURNCODE hCmenuList::calculateDrawAs(menuHist_t& history)// and update the menuitem list
{
	RETURNCODE rc = SUCCESS;	
	menuItmLstIT_t miT;
	hCitemBase* pItemBase = NULL;
	hCmenuItem* pMenuItem = NULL;
	hCddbDevice *pCurDev  = (hCddbDevice*)this->devPtr();


	for (miT = begin(); miT != end(); miT++)
	{//      isa ptr 2a hCmenuItem
		bool bIsNotAnItem = false;
		pMenuItem = &(*miT);// PAW 03/03/09 &(*) added
		switch(pMenuItem->getRef().getRefType())
		{
			case rT_Constant:
				pMenuItem->setDrawAs(mds_constString);
				bIsNotAnItem = true;
				break;
			case rT_Separator:
				pMenuItem->setDrawAs(mds_separator);
				bIsNotAnItem = true;
				break;
			case rT_Row_Break:
				pMenuItem->setDrawAs(mds_newline);
				bIsNotAnItem = true;
				break;
			default:
				break; 
		}

		if(bIsNotAnItem == true)
		{		
			continue; // get the next one
		}
		rc = pCurDev->procure(pMenuItem->getRef(), &pItemBase);
		if( rc == SUCCESS && pItemBase != NULL)
		{					
			if ( ! (pItemBase->IsValid() ) )
			{	pMenuItem->setDrawAs(mds_do_not_use);// deleting it will mess up the iterator
				continue; 
			}
			// do the duty
			/*  stevev 30mar05 - code that was here was moved to menuItem */
			pMenuItem->fillDrawType(pItemBase, history);
			pMenuItem->pItemBase = pItemBase; // so we don't have to look it up again.
		
		}//endif // no else  rc == SUCCESS && pItemBase != NULL
		else// stevev 07apr06 -from collection as a menu code...
		{	
			pMenuItem->setDrawAs(mds_do_not_use);
			LOGIT(CERR_LOG,L"Menu did not procure a reference.\n");
			continue; // get the next one
		}
	}//endfor

	return rc;
}
/*********************************************************************************************
 * hCmenuItem class
 *
 *********************************************************************************************/
hCmenuItem::hCmenuItem(DevInfcHandle_t h) : hCobject(h), itemRef(h), qualifier(h) 
{ 
	itemRef.clear(); 
	qualifier.clear(); 
								
	myHistory.clear(); 
	pItemBase=NULL; 
	draw_as = mds_do_not_use;
}

hCmenuItem::hCmenuItem(const hCmenuItem& meD)
	:hCobject(meD.devHndl()),itemRef(meD.devHndl()),qualifier(meD.devHndl()),pItemBase(NULL)
{// do not initialize ref resolution in a copy constructor >not< *this = meD;
	itemRef   = meD.itemRef;
	qualifier = meD.qualifier;
	draw_as   = meD.draw_as;
	myHistory = meD.myHistory;
	pItemBase = meD.pItemBase;
}

hCmenuItem::~hCmenuItem()
{ 
	destroy();
}

RETURNCODE hCmenuItem::destroy(void)
{
	return (itemRef.destroy() & qualifier.destroy());	
}

hCmenuItem& hCmenuItem::operator=(const hCmenuItem& meD)
{
	itemRef   = meD.itemRef;
	qualifier = meD.qualifier;
	draw_as   = meD.draw_as;
	myHistory = meD.myHistory;
	pItemBase = meD.pItemBase;
	// do initial resolution if required
	if (pItemBase == NULL && !itemRef.isEmpty())
	{// 28jul11 - suppress error messages--this generates several during population of the 
	 //		device's menu list when a menu holds a menu that hasn't been populated yet.
	 // 10aug11 - enable string fetch, this is usually the first fetch, if we don't get 'em
	 //     now, we won't re-resolve later (due to optimization)
		itemRef.resolveID(pItemBase, true, true);
	}
	return *this;
}

hCmenuItem& hCmenuItem::operator=(const hCmenu& meN)// load it from a menu - no code yet..
{
	assert(0);// force crash
	return *this;
}

RETURNCODE hCmenuItem::dumpSelf(int indent /*= 0*/, char* typeName )
{
	RETURNCODE rc = SUCCESS;

	//COUTSPACE << "Menu Item: " << endl;
	LOGIT(COUT_LOG,"Menu Item: ");
	rc  = itemRef.  dumpSelf(indent + 12);
	rc |= qualifier.dumpSelf(indent + 12);

	return rc;
}

void  hCmenuItem::setEqual(void* pAitem)
{	
	if (pAitem==NULL) return;
	aCmenuItem* paMi = (aCmenuItem*)pAitem;

	itemRef   = (paMi->itemRef);			//aCreference
	qualifier.setEqual(&(paMi->qualifier));	//aCbitString 
}; 	                   


void  hCmenuItem::setDuplicate(hCmenuItem& rMi)
{	
	itemRef.duplicate(&(rMi.itemRef),false, 0);// put zero in for now
	qualifier.duplicate(&(rMi.qualifier),false);// does not use 'replicate'
};

// stevev 23oct06
//hCmenuItem& hCmenuItem::operator=(const hCmenu& meN)
//{// load it from a menu
//	hCmenu* pM = (hCmenu *)&meN;
//	itemRef.setRef(pM->getID(), 0, MENU_ITYPE, false, rT_Item_id);
	//only parent cares//qualifier;
//	myHistory = pM->getLastHistory();//none at this time
//	myHistory.parentStyle = (menuItemDrawingStyles_t)(pM->getStyle()-1);
//	fillDrawType((hCitemBase*) &meN, myHistory);//was pM->getLastHistory());
//	devPtr()->getItemBySymNumber(pM->getID(),&pItemBase);// incase &reference != &hCmenu
//	return (*this);
//}

/* This is to set its own drawing style from its item-type, menu style and history

   stevev 30mar05 - code moved from menu.aquire() 
   this has a void return, it will fill it with something!
*/
/* hist
	bool dlgInPath;		// if we are in a dialog, all must be modal (no windows/tables/menus)
	bool inMethDlg;
	bool grpDepthHit;	// groups only allowed 2 deep
	bool inPopMenu;		// many things are different in a pop-up menu child
	menuItemDrawingStyles_t parentStyle;
*/
void hCmenuItem::fillDrawType(hCitemBase* pItemBase, const menuHist_t& history )
{
	int usableStyles;
	menuItemDrawingStyles_t defaultStyle;// conversion dest of incorrect MENUEs

	menuHist_t localHistory = history;		// set to parent as default starting point
	/*
		LINUX_PORT - REVIEW_WITH_HCF: Since 'history' is now const-ref, it cannot 
		be updated here. I am not entirely sure, but I don't think it was their 
		intention to update passed-in 'history' here either -- a glance indicated 
		that it wasn't being used that way (not a 100 % sure about that).
		As a workaround to the issue, first assign 'history' to 'localHistory'
		and then set localHistory.inMethDlg. Revisit during TODO review.
	*/

	// just in case - this type is only at start of menu tree
	if (localHistory.parentStyle == mds_methDialog) localHistory.inMethDlg =  true;

	unsigned uMenuItemType = pItemBase->getTypeVal();

	switch (history.parentStyle)
	{
	case mds_window:
		usableStyles = LEGAL4WINDOW;
		defaultStyle = mds_page;
		break;
	case mds_dialog:
		usableStyles = LEGAL4DIALOG;
		defaultStyle = mds_page;
		localHistory.dlgInPath = true;// tell the chill'in
		break;
	case mds_page:
	case mds_tab:
		usableStyles = LEGAL4PAGEnGROUP;
		defaultStyle = mds_group;
		break;
	case mds_group:
		usableStyles = LEGAL4PAGEnGROUP;
		defaultStyle = mds_group;
		break;
	case mds_menu:
		usableStyles = LEGAL4MENU;
		defaultStyle = mds_table;
		localHistory.inPopMenu = true;// tell the chill'in
		break;
	case mds_table:
		usableStyles = LEGAL4TABLE;
		defaultStyle = mds_table;
		break;
	case mds_methDialog:
		usableStyles = LEGAL4METHODDIALOG;
		defaultStyle = mds_group;
		localHistory.inMethDlg = true;// tell the chill'in
		break;
	case mds_do_not_use: /* this is don't care for top level window/dialog/table */
		usableStyles = LEGALALL;// everything legal
		defaultStyle = mds_table;
		break;
	default:
		usableStyles = 0x00;/* nothing is legal here */
		defaultStyle = mds_table;
		break;
	}

	if (history.inPopMenu) // we could be in a dialog with a popup menu (not elseif)
	{
		usableStyles = LEGAL4MENU;// overwrite <menues are not allowed in a methdlg>
		defaultStyle = mds_table;
		localHistory.inPopMenu = true;// just to be sure

        if (history.dlgInPath || history.wndInPath)//Spec 500 does now allow for a TABLE to be
        {                                          //invoked from a MENU if any parent above is
        	usableStyles &= ( ~(1<<mds_table) );
            defaultStyle = mds_menu;              //WINDOW or DIALOG.Therefore, Pop-Up menu has
        }                                         // defaults to MENU in this case, POB-6Aug08
	}
	
	if ( history.dlgInPath  )  /* any parent is dialog */
    {   usableStyles &= ( ~((1<<mds_window) | (1<<mds_table)) );// these must be converted
        // leave the default as whatever it is ....was::> defaultStyle = mds_dialog; 
        localHistory.dlgInPath = true;// just to be sure
    }
	
	if(history.inMethDlg)// will overwrite the previous
	{
		usableStyles = LEGAL4METHODDIALOG;
		defaultStyle = mds_group;
	}// else leave it alone


	switch(uMenuItemType)// the item type of my(menuitem) item reference
	{
	case MENU_ITYPE:  // I am a menu
		{
			menuStyleType_t myStyle = ((hCmenu*)pItemBase)->getStyle();

			setDrawAs(defaultStyle);	// handles all of the 'else' clauses 
			switch(myStyle)
			{
			case	menuStyleWindow:
				if ( usableStyles & (1<<mds_window) ) // is legal
                {
					setDrawAs(mds_window);			  // change from default
                }
                /* note that the following does NOT cover the issue.  See bug tracker 8/13/08*/
                else if (history.dlgInPath && !history.inMethDlg )  
                {					// Window is not allowed in a DIALOG because it is modeless
									// We would normally default to PAGE (see above),
					setDrawAs(mds_dialog);
					// but in this special case we will convert it to modal window, POB -7Aug08
                }
				break;
			case	menuStyleDialog:
				if ( usableStyles & (1<<mds_dialog) ) 
				{
					setDrawAs(mds_dialog);
					localHistory.dlgInPath = true;
				}
				break;
			case	menuStylePage:
				if ( usableStyles & (1<<mds_page) ) 
					setDrawAs(mds_page);
				break;
			case	menuStyleGroup:	
				if ( usableStyles & (1<<mds_group) ) // if legal
				{
					if (history.grpDepthHit) // passed in
					{
						if (usableStyles & (1<<mds_dialog))// stevev 02mar07 - set to legal
						{
							setDrawAs(mds_dialog);
							localHistory.grpDepthHit = false;// my children don't have to be
						}
						else if ( defaultStyle != mds_group &&// stevev 02mar07 - set to legal
							(usableStyles & (1<<defaultStyle)))
						{
							setDrawAs(defaultStyle);
							localHistory.grpDepthHit = false;// my children don't have to be
						}
						else// stevev 02mar07 - there is nothing legal to set it to
						{
							setDrawAs(mds_do_not_use);
							localHistory.grpDepthHit = false;// just clean up the state
						}
					}
					else // legal and normal
					{
						setDrawAs(mds_group);
						if ( history.parentStyle == mds_group )
						{// parent is a group AND i am group then my child will not be
							localHistory.grpDepthHit = true;// for my children
						}
					}
				}// else leave it default
				break;
			case	menuStyleMenu:
				if ( usableStyles & (1<<mds_menu) )
				{
					setDrawAs(mds_menu);
					localHistory.inPopMenu = true;// children must conform
				}
				break;
			case	menuStyleTable:
				if ( usableStyles & (1<<mds_table) ) 
                { /* note that the following does NOT cover the issue.See bug tracker 8/13/08*/
                    if (!history.dlgInPath && !history.wndInPath)
                    {
					    setDrawAs(mds_table);  // Spec 500 does now allow for a TABLE to be  
                    }                          // invoked from a MENU of style WINDOW or DIALOG
                }  // else use default
				break;

			case	menuStyleNone:
			case	menuStyleUnknown: //should not come here
				default: // leave it the default style
					break;
			}// endswitch
		}
		break;
	case VARIABLE_ITYPE:	
		{
			int varType = ((hCVar*)pItemBase)->VariableType();

			if( varType     == ENUMERATED) 
			{
				setDrawAs(mds_enum);
			}
			else 
			if( varType     == BIT_ENUMERATED)
			{
				if (getRef().getRefType() == rT_via_BitEnum)
				{
					CValueVarient msk;
					if ( getRef().resolveExpr(msk) == SUCCESS && 
						 (  msk.vType == CValueVarient::isIntConst ||
						    msk.vType == CValueVarient::isVeryLong  )  )
					{
						setDrawAs(mds_bit);
					}
					else
					{
						setDrawAs(mds_bitenum);
					}
				}
				else
				{
					setDrawAs(mds_bitenum);
				}
			}
			else 
			if(varType      == INDEX)
			{
				setDrawAs(mds_index);
			}
			/* BITSTRING? */
			else
			{
				setDrawAs(mds_var);
			}
			if ( usableStyles & (1<<((int)getDrawAs())) )// is legal
			{
				;// leave as is
			}
			else
			{
				setDrawAs(mds_do_not_use);// illegal just disappear
			}
		}
		break;
	case EDIT_DISP_ITYPE:	
		if ( usableStyles & (1<<mds_edDisp) ) 
			setDrawAs(mds_edDisp);
		else // it disappears
			setDrawAs(mds_do_not_use);
		break;
	case METHOD_ITYPE:
		if ( usableStyles & (1<<mds_method) ) 
			setDrawAs(mds_method);
		else // it disappears
			setDrawAs(mds_do_not_use);
		break;
	case COLLECTION_ITYPE:

	/* wally didn't like it...
	case ARRAY_ITYPE:		//these 3 are like this for now, we'll see where it goes 
	case FILE_ITYPE:
	case LIST_ITYPE:*/
		// handling exactly like a group
		if ( usableStyles & (1<<mds_group) ) // if legal
		{
			if (history.grpDepthHit) // passed in
			{
				setDrawAs(mds_dialog);// as per WAP 09aug05
				localHistory.grpDepthHit = false;// my children don't have to be
			}
			else // legal and normal
			{
				setDrawAs(mds_group);
				if ( history.parentStyle == mds_group )
				{// parent is a group AND i am group then my child will not be
					localHistory.grpDepthHit = true;// for my children
				}
			}
		}
		else // groups aren't allowed
		if ( history.parentStyle != mds_methDialog && ! history.inMethDlg)
		{// as per WAP 09aug05
			setDrawAs(defaultStyle);// legal everywhere but in a methdialog
		}
		else // in a methdlg w/ groups illegal
		{
			setDrawAs(mds_do_not_use);// disappears - should never happen
		}
		break;
	case GRAPH_ITYPE:
	case CHART_ITYPE:		
		if ( usableStyles & (1<<mds_scope) ) 
			setDrawAs(mds_scope);
		else // it disappears
			setDrawAs(mds_do_not_use);// allow in menues and tables when they will
									  //  will popup on a dialog
		break;
	case IMAGE_ITYPE:
		if ( usableStyles & (1<<mds_image) ) 
			setDrawAs(mds_image);
		else // it disappears
			setDrawAs(mds_do_not_use);// allow in menues and tables when they will
									  //  will popup on a dialog
		break;
	case GRID_ITYPE:// just like scope
		if ( usableStyles & (1<<mds_grid) ) 
			setDrawAs(mds_grid);
		else // it disappears
			setDrawAs(mds_do_not_use);// allow in menues and tables when they will
									  //  will popup on a dialog
		break;	
	case BLOB_ITYPE:
		if ( usableStyles & (1<<mds_var) ) 
			setDrawAs(mds_var);
		else // it disappears
			setDrawAs(mds_do_not_use);// allow in menues and tables when they will
									  //  will popup on a dialog
		break;	
	/* these do not have itembase pointers and should never be in this routine
		mds_separator,
		mds_newline,	// rowbreak
		mds_constString,
		mds_blank
	 */
	default:
		setDrawAs(mds_do_not_use);// everybody else disappears
		break;
	}//end switch
// stevev 21mar07 - store passed-in (as computed)	localHistory.parentStyle = getDrawAs();
	setHistory(localHistory);
}

hCmenuTree& hCmenuTree::operator=(const hCmenu& meN)
{ 
	hCmenu* pM = (hCmenu*)&meN;

	hCmenuItem::itemRef.setRef(pM->getID(), 0, pM->getTypeVal());
	hCmenuItem::qualifier = 0;
	hCmenuItem::draw_as   = (menuItemDrawingStyles_t)(pM->getStyle()-1);//straight thru for now
	hCmenuItem::myHistory.clear();
	hCmenuItem::pItemBase = (hCitemBase *)&meN;
	return *this;
};


// set last page and previous to size passed in 
// if the new one is bigger (all pages are the size of the largest)
void hCmenuTree::setPreviousPages(hv_point_t sz) 
{
	if (branchList.size() <= 0) // stevev 15sep14
	{
	   	return;// nothing to do
	}

	menuTrePtrLstIT_t treeItmPtr = branchList.end()-1;// end is one past last good

	/* table behind table has issues, use more identification 
	   stevev 19aug08 */
	hv_point_t   firstPt =   (*treeItmPtr)->topleft;

	while ((*treeItmPtr)->getDrawAs() == mds_page  &&
		   (*treeItmPtr)->topleft     == firstPt    )
	{
		//treeItmPtr->hv_size = sz;
		// just make it bigger....28jun12...(*treeItmPtr)->da_size = sz;
		if (sz.xval > (*treeItmPtr)->da_size.xval)
			(*treeItmPtr)->da_size.xval = sz.xval;
		if (sz.yval > (*treeItmPtr)->da_size.yval)
			(*treeItmPtr)->da_size.yval = sz.yval;
		//this crashes  (*treeItmPtr)--;
		if ( treeItmPtr != branchList.begin() )
		{
			treeItmPtr--;
		}
		else
		{
			break;// in case all of 'em are pages
		}
	}//wend
}

//                                             hist has the draw-as of this tree's owner/parent
hCmenuTree& hCmenuTree::fill4Collection(hCcollection& coll,const menuHist_t& hist)
{
	//branchList.clear();

	itemRef.setRef(coll.getID(), 0, COLLECTION_ITYPE, false, rT_Item_id);
	qualifier.setBitStr(0);
	fillDrawType((hCitemBase*) &coll, hist); // sets this menu-item's draw-as and myHistory
	//myHistory filled above
	devPtr()->getItemBySymNumber(coll.getID(),&pItemBase);

	return (*this);
}

//                                             hist has the draw-as of this tree's owner/parent
hCmenuTree& hCmenuTree::fill4Menu(hCmenu& menu,const menuHist_t& hist)
{
	//branchList.clear();

	itemRef.setRef(menu.getID(), 0, MENU_ITYPE, false, rT_Item_id);
	qualifier.setBitStr(0);
	fillDrawType((hCitemBase*) &menu, hist); // sets this menu-item's draw-as and myHistory
	//myHistory filled above
	devPtr()->getItemBySymNumber(menu.getID(),&pItemBase);

	return (*this);
}

void hCmenuTree::setRowSize(unsigned w)  // sub menues must be set as well
{
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"setRowSize called: from %d to %d (self and children).\n",rowSize,w);
	rowSize = w;
#ifdef GROUP_IS_SUBCANVAS
	if ( draw_as == mds_group )
		return;
#endif
	menuTrePtrLstIT_t blit;
	for ( blit = branchList.begin();blit != branchList.end(); ++blit)
	{// ptr2ptr2hCmenuTree
		hCmenuTree* pT = (hCmenuTree*)*blit;
		pT->setRowSize(w);
	}
}


void hCmenuTree::setInternalCanvas(DDSizeLocType cSize)
{	
	// stevev 27mar13 - for trailing ColBrk, change: DDSizeLocType newSize = cSize;
	DDSizeLocType newSize = max(rowSize,cSize);
	canvas_EntryCnt++;
	if ( pItemBase ) 
	{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Internal Canvas for 0x%04x  from %d to %d;  rowsize:%d\n",
							pItemBase->getID(),canvasSize,newSize, rowSize );
	}
	else // its a constant string/separator etc
	{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Internal Canvas for  Const  from %d to %d;  rowsize:%d\n",
												canvasSize,newSize, rowSize );
	}

	if ( da_size.xval > newSize )
		LOGIT(CLOG_LOG,"MenuTree's X size is larger than than the overall canvas.\n");

	//if (draw_as == mds_group)// inserted 25jun12 - the items are getting centered on the
							   //  overall canvas instead of the group canvas
	if ((  draw_as == mds_group || draw_as == mds_bitenum )//26mar13-bits are centered on canva
		&& canvas_EntryCnt > 1)// 05jul12 - we have to tell our children
	{												// sometime, just don't recurse
		canvasSize = newSize; // the size we will be drawn upon
		return; // do not recurse into a group, our children already have a good canvas size
	}
						   //  overall canvas instead of the group canvas
	if (draw_as == mds_page && cSize > da_size.xval && itemRef.getID())// 18jul12 - expand the page as required
	{	// Added validation to ensure symbol ID of this menu is not zero in order to prevent population and 
        // a crash in setPreviousPages() below when a TAB page changes its VALIDITY from TRUE 
        // to FALSE, POB - 8/16/2014
		da_size.xval = newSize; // the size we will be drawn upon
		hv_point_t hv(newSize, da_size.yval);
		setPreviousPages(hv);
	}
	// all others recurse into their lists ... should only be pages
	menuTrePtrLstIT_t blit;
	for ( blit = branchList.begin();blit != branchList.end(); ++blit)
	{// ptr2ptr2hCmenuTree
		hCmenuTree* pT = (hCmenuTree*)*blit;
		pT->setInternalCanvas(newSize);
	}
	canvasSize = newSize;
	canvas_EntryCnt--;
}


void hCmenuTree::replace(hCmenuTree* p2B_removed, hCmenuTree* p2Replacemant)
{
	menuTrePtrLstIT_t pp2hCmenuTree;

	for (pp2hCmenuTree = branchList.begin();pp2hCmenuTree != branchList.end();++pp2hCmenuTree)
	{
		if ( (*pp2hCmenuTree) == p2B_removed )// pointer compare - bad, but all we have
		{
			p2Replacemant->pParentTree = (*pp2hCmenuTree)->pParentTree;
			(*pp2hCmenuTree) = p2Replacemant;
			RAZE( p2B_removed ); 
			return;// we assume there is only one of those nodes (should be a good assumption)
		}
	}
	LOGIF(LOGP_NOT_TOK)(CLOG_LOG,
					   L"ERROR: hCmenuTree's replace did not find 2-B-removed in the list.\n");
	return;
}

void hCmenuTree::dumpSelf(int indent,char* typeName)
{
	LOGIT(CLOG_LOG,"%sMenu Tree: ",Space( indent ));
	if (pItemBase)
		LOGIT(CLOG_LOG,L"0x%04x  (%hs) \n",pItemBase->getID(),pItemBase->getName().c_str());
	else
		LOGIT(CLOG_LOG,L"\n");

	itemRef.  dumpSelf(indent + 4);
	qualifier.dumpSelf(indent + 4);

	LOGIT(CLOG_LOG,L"%hsDrawAs: %hs   w/ idx= %d\n",
										Space( indent+4 ),menuStyleStrings[draw_as],idx);
	LOGIT(CLOG_LOG,L"%hsTopLeft: %d, %d    RectSize: %d,%d      Row: %d  Canvas: %d    "
		L"with %d branches\n",Space( indent+4 ),
		topleft.xval, topleft.yval,
		da_size.xval,da_size.yval, rowSize,canvasSize,branchList.size());

	for ( menuTrePtrLstIT_t tIP = branchList.begin(); tIP != branchList.end(); ++tIP)
	{
		((hCmenuTree*)(*tIP))->dumpSelf(indent+4);
	}

	return;
}

//int maxRow[COLS_SUPPORTED+1];// array of columns [1- 5]...0 not used


maxColumn::maxColumn()
{
	for (int i =0; i <= COLS_SUPPORTED; i++)
	{
		maxRow[i] = 0;
	}

	// full layout tracking added from here down	stevev - 01Apr15
	if (dispGrid.size()) dispGrid.clear();
	addRow(rowStopCell);// insert a RB zero row...unused
	addRow(0);// current insertion point

	firstRowInRow = 1;
	nxtCellNumber = MinCellNum;// no use having everything #1
	mxColCanvas   = 3; // minumum canvas
	insertPt.xval = insertPt.yval = 1;// start at top left corner
}


//	get the highest numbered (lowest on the screen) row used within start-stop range
//	start-stop range is inclusive
int maxColumn::getLowest(int startCol, int endCol)
{
	if (endCol > COLS_SUPPORTED || endCol < 1)
	{
		return -1;
	}
	if (startCol > COLS_SUPPORTED || startCol < 1 || startCol > endCol)
	{
		return -2;
	}
	int m = 0;
	for ( int i = startCol; i <= endCol; i++)
	{
		m = max(m, abs(maxRow[i]));
	}
	return m;
}

// get the lowest in range, then set all columns to that value (negative if outside range)
//	ie.  level the range...including the unset columns to negative value (for rowbreak)
int maxColumn::setAll2Lowest(int startCol, int endCol)
{
	int y = getLowest(startCol, endCol);
	if (y < 0) return y;
	int i = 0;
	for ( i = startCol; i <= endCol; i++)
	{
		maxRow[i] = y;
	}
	
	for ( i = endCol+1; i <= COLS_SUPPORTED; i++)
	{
		// we set all the others to this max...we are a row break!!!     if (maxRow[i] <= 0)
			maxRow[i] = 0 - y;// a negative value
		// else, leave it alone
	}
//	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"      All2Lowest: col %d to col %d set to %d\n",
//																		startCol,endCol,y);
	return y;
}

// get the lowest in range, then set all columns in the range to that value
//	ie.  level the range...reseting neg to positive (just before insertion)
int maxColumn::setThese2Lowest(int startCol, int endCol)
{
	int y = getLowest(startCol, endCol);
	if (y < 0) return y;
	for ( int i = startCol; i <= endCol; i++)
	{
		maxRow[i] = y;
	}
	return y;
}

//  set all columns in the range to the new row value
//	ie.  level the range to a new value
void maxColumn::setAll2(int startCol, int endCol, int newRow)
{
	if (endCol > COLS_SUPPORTED || endCol < 1)
	{
		return;
	}
	if (startCol > COLS_SUPPORTED || startCol < 1 || startCol > endCol)
	{
		return;
	}
	
	for ( int i = startCol; i <= endCol; i++)
	{
		maxRow[i] = newRow;
	}
//	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"      All2Value : col %d to col %d set to %d\n",
//																	startCol,endCol,newRow);
	return;
}

// set a single column to a used row number
void maxColumn::setCol(int columnNumber, int rowNumber)
{
	if (columnNumber < 1 || columnNumber > COLS_SUPPORTED || rowNumber < 0 )
		return;
	maxRow[columnNumber] = rowNumber;
}


// get used row number from a single column 
int maxColumn::getCol(int columnNumber)
{
	if (columnNumber < 1 || columnNumber > COLS_SUPPORTED )
		return - 100;
	return abs(maxRow[columnNumber]);
}

// return the last column that has something in it
int maxColumn::getWidth(void)
{
	for ( int i = 1; i <= COLS_SUPPORTED; i++)
	{
		if (maxRow[i] <= 0)
			return (i-1);// the last full row
	}
	return COLS_SUPPORTED;
}

// return the width of this row number
int maxColumn::rowWidth(int rowNumber)
{
#ifdef FIRST_TRY
	for ( int i = 1; i <= COLS_SUPPORTED; i++)
	{
		if (rowNumber > maxRow[i])
			return (i-1);// the last full col in this row
	}
#else	
	for ( int i = COLS_SUPPORTED; i > 0; i--)
	{
		if (maxRow[i] >= rowNumber)
			return (i);
	}
#endif
	return COLS_SUPPORTED;
}

//==============================================================================
// full layout tracking added from here down	stevev - 01Apr15

int maxColumn::ColBrk(void)
{// increment cell# because we are done with the current cell
	nxtCellNumber++;

	//if left is zero or X == 1, skip this
	markCB(); //mark as -1 (CB)  from y  for all matching cell to my left down ().
	
	insertPt.xval++; // increment insertion x
	if (insertPt.xval > COLS_SUPPORTED)
	{
		return RowBrk();//  verify this...
	}
	//y to lowest number row w/ val < 1 cell# in the X col from firstRowInRow to Y
	// has to search up //
	//for ( int i = firstRowInRow; i < (int)dispGrid.size(); i++ )
	for ( int i = (int)(dispGrid.size()-1) ; i >= firstRowInRow; i-- )
	{// exit if we hit a cell, a rowbreak flag or a column break flag (we should never see a CB above us)
		if (dispGrid[i].col[insertPt.xval] < MinCellNum && dispGrid[i].col[insertPt.xval] >= 0)
		{
			insertPt.yval = i;
		}
		else
		{
			break;// out of loop using the last found location
		}
	}
	//canvas = max( canvas, X)

    mxColCanvas =
#if defined(__GNUC__)
    std::max(mxColCanvas,(int)insertPt.xval);
#else
    __max(mxColCanvas,(int)insertPt.xval);
#endif // __GNUC__

	logDisplayTrack("COLBREAK");
	return 0;// for now  ???;
}

int maxColumn::RowBrk(void)
{// increment cell# because we are done with the current cell
	nxtCellNumber++;

	//if left is zero or X == 1, skip this
	markCB(); //mark as -1 (CB)  from y  for all matching cell to my left down ().

	insertPt.xval = 1; // x goes to 1
	insertPt.yval = RBlowest(); // lowest non zero row in the layout has all zeros set to -10
								// returns..y  is first val 0 | -1 from firstRowInRow down in col 1
	// done in RBlowest..set row widths from firstRowInRow to Y-1 to first zero column -1  from left 
	firstRowInRow = insertPt.yval; //set firstRowInRow  to Y

	logDisplayTrack("ROWBREAK");

	return 0; // for now
}

// inserts at the insertion point
// integral cell is one like GROUP, CHART etc
int maxColumn::Insert(hv_point_t size, bool integralCell)
{
	int x,y;
//	for each from X to X+width-1, Y to Y+height-1   set grid value to nxtCell
	for ( y = insertPt.yval; y < (insertPt.yval + size.yval); y++)// for each y
	{
		while ( y >= (int)dispGrid.size() )
			addRow(0);
		for ( x = insertPt.xval; x < (insertPt.xval + size.xval); x++)
		{
			dispGrid[y].col[x] = nxtCellNumber;
		}
	}
	//y+= height
	insertPt.yval += size.yval; // x doesn't change in an insertion
	while ( insertPt.yval >= (int)dispGrid.size() )
		addRow(0);
	//@ cell complete
	if ( integralCell ) 
		nxtCellNumber ++;
	// no need to mess with canvas here, RB will deal with these canvas = max( canvas, X)

	logDisplayTrack(" -- Insert -- ");
	return 0; // for now
}

// aka isIntegralCell()
bool maxColumn::CGGG( menuItemDrawingStyles_t it )
{
	return ( (it == mds_scope) || // chart, graph
		     (it == mds_group) || // group
			 (it == mds_grid ) ); // or grid
}


void maxColumn::logDisplayTrack(char* prefix)
{
	if LOGTHIS( LOGP_LAYOUT )
	{
		LOGIT(CLOG_LOG,"        ======== %s ========\n",prefix);
		LOGIT(CLOG_LOG,"        rowStart:%2d, nxtCell:%2d, Insert:(%2d, %2d) Canvas:%2d\n",
			firstRowInRow, nxtCellNumber, insertPt.xval, insertPt.yval, mxColCanvas);
		for ( unsigned i = 1; i < dispGrid.size(); i++)
		{
		LOGIT(CLOG_LOG,"        %2d:  %3d   %3d   %3d   %3d   %3d     RS:%1d\n", i,
			dispGrid.at(i).col[1],dispGrid.at(i).col[2],dispGrid.at(i).col[3],
			dispGrid.at(i).col[4],dispGrid.at(i).col[5],dispGrid.at(i).col[0]);
		}
		LOGIT(CLOG_LOG,"        ------------------------------------\n");
	}
}

//========================== helpers =========================================================

// this function inserts the immortal cell with a height equal to the cell to our left

void maxColumn::markCB(void)
{//if left is zero or X == 1, skip this
	if ( insertPt.xval > 1 && 
		 dispGrid[insertPt.yval].col[insertPt.xval-1] != 0 )
	{//mark sef col as -1 (CB)  from y  for all cell matching to my left down ().
		int left = dispGrid[insertPt.yval].col[insertPt.xval-1];
		for ( int i = insertPt.yval; i < (int)dispGrid.size(); i++ )
		{
			if (dispGrid.at(i).col[insertPt.xval-1] == left)
			{// mark self col
				dispGrid.at(i).col[insertPt.xval] = CBcell;
			}
			else
			{
				break;// there are no more that count
			}
		}
	}
}

// set each row width
int maxColumn::setWidth(void)
{
	int maxWidth = 0;
	for ( int i = firstRowInRow; i < (int)dispGrid.size(); i++ )
	{
		for ( int y = COLS_SUPPORTED; y > 0; y--)// right to left
		{
			if (dispGrid[i].col[y] != 0)// a cell or a cb
			{
				dispGrid[i].dispRowWidth = y;
				maxWidth = max(maxWidth,y);
				break;// out of inner loop
			}
		}// next x		
	}// next y
	return maxWidth;
}

// returns the new y insertion point
//
// sets the rowWidth for each display row in this DD row
// sets any unoccupied columns on the lowest display row of this DD row to block future CBs
//
int maxColumn::RBlowest(void)
{// lowest non zero row in the layout has all zeros set to -10
	//find lowest
	int i, lowest = 0, yinsert = 0, widest = 0;
	// do before reset..set row widths from firstRowInRow to Y-1 to first zero column -1  from left 
	mxColCanvas = max( mxColCanvas, setWidth());//sets row widths & canvas

	for ( i = firstRowInRow; i < (int)dispGrid.size(); i++)
	{
		if ( dispGrid.at(i).occupied())
		{
			lowest = i;
		}
	}
	for ( i = 1; i <= COLS_SUPPORTED; i++)
	{
		if ( dispGrid.at(lowest).col[i] < lowest && dispGrid.at(lowest).col[i] != CBcell)
		{
			dispGrid.at(lowest).col[i] = rowStopCell;
		}
	}
	yinsert = lowest + 1;// returns..y  is first val 0 | -1 from firstRowInRow down in col 1
	while (yinsert >= (int)dispGrid.size() )
		addRow(0);

	return yinsert;
}

void maxColumn::addRow(int fill)
{
	displayRow mtRow;
	mtRow.setAll2(fill);
	dispGrid.push_back(mtRow);
}
//fini
