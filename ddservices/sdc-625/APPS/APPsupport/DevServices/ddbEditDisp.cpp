/*************************************************************************************************
 *
 * $Workfile: ddbEditDisp.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the Edit Display class
 *		09/26/02	sjv	created from itemMenu.cpp
 */

#include "ddbItems.h"
#include "ddbEditDisplay.h"

#include "ddbDevice.h"


extern char edDispTypeStrings[EDDISPSTRCOUNT][EDDISPMAXLEN] ;
/* stevev 2nov4 ... using reference lists
void  hCedDispList::setEqual(void* pAclass)
{// assume: pAclass pts to a  class aCedDispList : public aPayldType, public AreferenceList_t
// 8/20/04 now it pts 2a aCreferenceList : public aPayldType, public AreferenceList_t
 //   and   typedef vector<aCreference>        AreferenceList_t;
	
	if (pAclass == NULL) return;//nothing to do

	aCreferenceList* paL = (aCreferenceList*)pAclass;
	
	
//	hCmenuItem  wrkMenuItem(devHndl());
	hCreference wrkReference(devHndl());
	
	for(AreferenceList_t::iterator iT = paL->begin(); iT<paL->end(); iT++)
	{ // iT is ptr 2 aCreference
		if (iT != NULL)
		{
			wrkReference = *iT;      * overloaded operator= gives conversion*
			push_back(wrkReference); * referenceList (self).*
			wrkReference.clear();
		}
		else
		{
			cerrxx <<"ERROR: received a NULL editDisplay Reference ptr."<<endl;
		}
	} 
}

void  hCedDispList::append(hCedDispList* pList2Append)
{
	/x*HreferenceList_t.*x/insert(end(),pList2Append->begin(),pList2Append->end());
}
end stevev 2nov4 */


hCattrBase*  hCeditDisplay::getaAttr(eddispAttrType_t attrType)
{
	ulong gt= hCattrBase::makeGenericType(iT_EditDisplay,attrType);
	FOR_iT(hAttrPtrList_t, attrLst)
	{// iT isa ptr2 hCattrBase
		if ((*iT)->getGenericType() == gt) 
			return ((hCattrBase*)(*iT));
		// else keep scanning 
	}
	return NULL;// not found
};

/****************************************************************************/

hCeditDisplay::hCeditDisplay(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), editItems(h), dispItems(h)
{// label is set in the base class
	 
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(eddispAttrEditItems); 
	if (aB == NULL)
	{	clog << "WARN: hCeditDisplay (0x"<<hex<<getID()
			 <<") constructor <no edit items attribute supplied.>"<<endl;
		clog << "       trying to find:" <<  eddispAttrEditItems  << " but only have:";

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG, " an empty list.\n");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ clog<< (*iT)->attr_mask << ", ";}
		}
		cerr<<endl;
	} 
	else // generate a hart class from the abstract class
	{ editItems.setEqualTo( &(((aCattrReferenceList*)aB)->RefList)  ); }



	aB = paItemBase->getaAttr(eddispAttrDispItems); 
	if (aB == NULL)
	{	
//tok users need a higher class of error report		LOGIT(CLOG_LOG,"WARN: hCeditDisplay (0x%04x) constructor <no display-items attribute supplied.>\n",getID());
		//LOGIT(CLOG_LOG,"WARN: %s (0x%04x) Edit-Display has no display-items attribute\n",getName().c_str(),getID());
//TODO: fix this, it disconnects the logging		LOGIT(CERR_LOG|CLOG_LOG,ERR_NO_DISP_ATTR,getWName().c_str(),getID());
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"       trying to find: %d but only have:",maskFromInt(eddispAttrDispItems));

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"%d, ",(*iT)->attr_mask);}
		}
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"\n");
	} 
	else // generate a hart class from the abstract class < RefList isA AcondReferenceList_t >
	{ dispItems.setEqualTo( &(((aCattrReferenceList*)aB)->RefList)  ); }


	aB = paItemBase->getaAttr(eddispAttrPreEditAct); 
	if ( aB != NULL )
	{
		hCattrEdDispPreEditAct* pEDPEA = new hCattrEdDispPreEditAct(h, (aCattrReferenceList*)aB );
#ifdef _DEBUG
	LOGIT(CERR_LOG, "########This edit display has Pre-Edit actions.\n");
#endif
		if (pEDPEA != NULL)
		{
			pEDPEA->setItemPtr((hCitemBase*)this);
			attrLst.push_back((hCattrBase*)pEDPEA);
		}
	}

	aB = paItemBase->getaAttr(eddispAttrPostEditAct); 
	if ( aB != NULL )
	{
		hCattrEdDispPostEditAct* pEDPEA = new hCattrEdDispPostEditAct(h, (aCattrReferenceList*)aB );
#ifdef _DEBUG 
	LOGIT(CERR_LOG, "########This edit display has Post-Edit actions.\n");
#endif
		if (pEDPEA != NULL)
		{
			pEDPEA->setItemPtr((hCitemBase*)this);
			attrLst.push_back((hCattrBase*)pEDPEA);
		}
	}
}


RETURNCODE hCeditDisplay::getMethodList(eddispAttrType_t vat,vector<hCmethodCall>& methList)
{
	RETURNCODE    rc = SUCCESS;
	
	ddbItemList_t        localItmList;
	itemType_t			 iTy;
	hCmethodCall         wrkMthCall;
	CValueVarient        retVal;
	retVal = getID();    // we use it first for the self parameter
	
	hCattrBase*  pAB = getaAttr(vat);
	if ( pAB != NULL )
	{// we have an attribute	
clog <<"getMethodList: found a "<< edDispTypeStrings[vat] << " attribute."<<endl;
		switch(vat)
		{
		case eddispAttrPreEditAct:			
		{
			hCattrEdDispPreEditAct*   pActions = (hCattrEdDispPreEditAct*) pAB;
			rc = pActions->getItemPtrs(localItmList);
		}
		break;
		case eddispAttrPostEditAct:	
		{
			hCattrEdDispPostEditAct*   pActions = (hCattrEdDispPostEditAct*) pAB;
			rc = pActions->getItemPtrs(localItmList);
		}
		break;
		default:
		{
clog <<"getMethodList: has an UNKNOWN "<< edDispTypeStrings[vat] << " attribute."<<endl;
			rc = FAILURE;
		}
		break;
		}// end switch		 

		if (rc == SUCCESS && localItmList.size() > 0)
		{//for each in the list
			for (ddbItemList_t::iterator iT = localItmList.begin();iT<localItmList.end();iT++)
			{//iT isa ptr 2a ptr 2a hCitemBase
				iTy = (*iT)->getIType();
				if ( iTy != iT_Method)
				{
					cerr<<"ERROR: non-Method type in a Pre/Post action list."<<endl;
				}
				else
				{// we be good
				//  fill a method call with varID, msrc_ACTION,methodID
					wrkMthCall.methodID  = (*iT)->getID();
					wrkMthCall.source    = msrc_ACTION;
					wrkMthCall.paramList.push_back(retVal);
					methList.push_back(wrkMthCall);
					wrkMthCall.clear();
				}
			}// next method
		}// else return the error

	}// else - no actions, just return SUCCESS
	return rc;
}

RETURNCODE hCeditDisplay::doPreEditActs(void)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient    retVal;
	methodCallList_t methodList;

	rc = getMethodList(eddispAttrPreEditAct,methodList);

	if (rc == SUCCESS && methodList.size() > 0 )
	{
		rc = devPtr()->executeMethod(methodList,retVal);
	}
	else
	{
		// NORMAL::cerrxx <<"ERROR: method list resolved to empty in PreEdit Actions.("<<rc<<")"<<endl;
		//rc = FAILURE;
	}

	return rc;
}


RETURNCODE hCeditDisplay::doPstEditActs(void)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient    retVal;
	methodCallList_t methodList;

	rc = getMethodList(eddispAttrPostEditAct,methodList);

	if (rc == SUCCESS && methodList.size() > 0 )
	{
		rc = devPtr()->executeMethod(methodList,retVal);
	}
	else
	{
		// NORMAL::cerrxx <<"ERROR: method list resolved to empty in PostEdit Actions.("<<rc<<")"<<endl;
		//rc = FAILURE;
	}

	return rc;
}


RETURNCODE hCeditDisplay::aquireEditList(varPtrList_t& varPtrList)
{
	RETURNCODE rc = FAILURE;

	//vector<hCmenuList>* pListOfMenuLists = NULL;
	//stevev 2nov4 hCedDispList ListOfEdDispLists(devHndl());
	hCreferenceList  ListOfEdDispLists(devHndl());

	//pListOfEdDispLists = editItems.resolveCond();  // a hCcondList<hCedDispList>
	rc = editItems.resolveCond(&ListOfEdDispLists);  // fills the 

	//if (pListOfEdDispLists == NULL || pListOfEdDispLists->size() <= 0 ) 
	if (rc != SUCCESS || ListOfEdDispLists.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		varPtrList.clear();	
	    return FAILURE; // no default, bad conditional (no else stmt and else required)
	}
	else
	{	
		rc = getItemPtrs(&ListOfEdDispLists, varPtrList); // was ( in MERGE_DONE_honeywell_012)::>  varTempPtrList);

	}
	return rc;
}

RETURNCODE hCeditDisplay::aquireDispList(varPtrList_t& varPtrList)
{
	RETURNCODE rc = FAILURE;

	//vector<hCmenuList>* pListOfMenuLists = NULL;
	//stevev 2nov4 hCedDispList ListOfEdDispLists(devHndl());
	hCreferenceList  ListOfEdDispLists(devHndl());

	//pListOfEdDispLists = dispItems.resolveCond();  // a hCcondList<hCedDispList>
	rc = dispItems.resolveCond(&ListOfEdDispLists); 

	//if (pListOfEdDispLists == NULL || pListOfEdDispLists->size() <= 0 ) 
	if (rc != SUCCESS || ListOfEdDispLists.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		varPtrList.clear();	
	    return FAILURE; 
	}
	else
	{	
		rc = getItemPtrs(&ListOfEdDispLists, varPtrList);
	}
	return rc;
}

bool hCeditDisplay::isChanged(void)
{
	varPtrList_t vplist;
	if (aquireEditList(vplist) == SUCCESS && vplist.size() > 0)
	{
		for (varPtrList_t::iterator iT =  vplist.begin(); iT < vplist.end(); iT++)
		{// ptr2a hCVar*
			if ( (*iT)->isChanged() )
			{
				return true; // abort and give result
			}
		}
		return false;
	}
	// err on the side of caution
	return true;
}

hCattrBase*   hCeditDisplay::newHCattr(aCattrBase* pACattr)  // builder 
{
	LOGIT(CERR_LOG,"    - hCedDispList's newHCattr is not implemented.\n");
	return NULL; // an error
}

// this was moved up from the dispList so we didn't have to have device access from 
//		the other side of the conditional
// assume the caller has the list in the proper condition
// stevev 2nov4 RETURNCODE hCeditDisplay::getItemPtrs(hCedDispList* pSrcList, varPtrList_t& varPtrList).
RETURNCODE hCeditDisplay::getItemPtrs(hCreferenceList* pSrcList, varPtrList_t& varPtrList)
{
	RETURNCODE rc  = SUCCESS;
	hCitemBase* pI = NULL;
	itemType_t  iTy;

	for (HreferenceList_t::iterator iL = pSrcList->begin(); iL < pSrcList->end(); iL++)
	{// iL is a ptr 2 a hCreference
		// resolve the reference		
		rc = iL->resolveID( pI );

		if ( rc == SUCCESS && pI != NULL)
		{
			iTy = pI->getIType();
			if ( iTy == iT_Variable )
			{
				varPtrList.push_back( (hCVar*)pI );
				// leave rc SUCCESS
			}
			else
			if (iTy == iT_WaO)
			{
				varPtrList_t loVarPtrs;
				if (  ((hCwao*)pI)->getWAOVarPtrs (loVarPtrs) == SUCCESS && loVarPtrs.size()>0)
				{// append list
					for (varPtrList_t::iterator iT = loVarPtrs.begin(); iT<loVarPtrs.end(); iT++)
					{// iT is a ptr to a hCVar ptr				
						varPtrList.push_back( (hCVar*)(*iT) );
					}
				}
			}
			else
			{
				clog<<"---edit display item type:"<<iTy<<" not supported."<<endl;
			}
		}
		else
		{
			clog << "PROBLEM: reference did not resolve the id ptr for EdDisp.getItemPtrs."<<endl;
			rc = APP_RESOLUTION_ERROR;
		}
	}
	return rc;
}

/*************************************************************************************************
 *
 *   $History: ddbEditDisp.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */
