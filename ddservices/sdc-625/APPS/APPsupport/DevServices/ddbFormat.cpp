/*************************************************************************************************
 *
 * $Workfile: ddbFormat.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003 - 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the base format functionality
 *		10nov04	sjv	started creation
 */


#include "ddbFormat.h"
#include <time.h>
#include "ddbItemBase.h"
#ifdef _WIN32_WCE
#include "time_ce.h"	// PAW function missing 06/05/09
#endif

void hCformatData::Insert(hCdataElement* pE)
{
	insertionPoint = insert(insertionPoint,pE);
}

void hCformatData::Add(hCdataElement* pE)
{
	insert(insertionPoint,pE);
}

RETURNCODE hCdataElement::insert(const char* value, int len)
{	destroy();  
	if (value == NULL || len <= 0 || len >= MAX_ITEM_LEN)
	{	return APP_PROGRAMMER_ERROR;	}
	length = len; buffer = new unsigned char[length+STRHEADROOM];
	if ( buffer == NULL ) { return APP_MEMORY_ERROR; }
	memcpy(buffer,value,length);
	return SUCCESS;
};

hCdataElement& hCdataElement::operator=(const hCdataElement& de)
{	destroy();
	buffer = new unsigned char[de.length+STRHEADROOM];
	length = de.length;
	memcpy(buffer,de.buffer,length);
	return *this;
};

// null terminated string
// prepends indent - NO newline!!
RETURNCODE hCformatBase::insertStr(hCformatData* pData, char* pC)
{
	RETURNCODE rc = SUCCESS;
	if (pData == NULL ||   pC == NULL ) return FAILURE;
	string wrkS(pData->indent,' '); wrkS += pC;
	hCdataElement*    pWrkE = new hCdataElement;
	rc = pWrkE->insert(wrkS.c_str(),wrkS.length());
	if ( rc == SUCCESS )
	{	pData->Insert(pWrkE); }
	return rc;
}

// prepends indent - NO newline!!
RETURNCODE hCformatBase::insertStr(hCformatData* pData, const string& rS)
{
	RETURNCODE rc = SUCCESS;
	if (pData == NULL ||  rS.empty() ) return FAILURE;
	string wrkS(pData->indent,' '); wrkS += rS;
	hCdataElement*    pWrkE = new hCdataElement;
	rc = pWrkE->insert(wrkS.c_str(),wrkS.length());
	if ( rc == SUCCESS )
	{	pData->Insert(pWrkE); }
	return rc;
}

// prepends indent - postpends \n
RETURNCODE hCformatBase::insertStr(hCformatData* pData, const string& S, char* keyStr, char* typeStr)
{
	RETURNCODE rc = SUCCESS;
	string wrkS;

	if (pData == NULL || keyStr == NULL ) return FAILURE;

	wrkS  = "<";  wrkS += keyStr; 
	if ( typeStr != NULL )
	{
		wrkS += " ";
		wrkS += typeStr;
	}

	wrkS += ">"; wrkS += S;
	wrkS += "</"; wrkS += keyStr; wrkS += ">\n"; 
	rc = insertStr(pData,wrkS);
	return rc;
}



// prepends indent - postpends \n
RETURNCODE hCformatBase::insertInt(hCformatData* pData,int D,char* keyStr,int radix,char* typeStr)
{
	RETURNCODE rc = SUCCESS;
	string wrkS;
	char numeric[80];

	if (pData == NULL || keyStr == NULL ) return FAILURE;
	//sjv 26jul06 itoa(D,numeric,radix);// 10 (dec) 16 (hex) 8 (octal) 2 (binary)
	_ultoa((unsigned long)D,numeric,radix);

	wrkS  = "<";  wrkS += keyStr; 	
	if ( typeStr != NULL )
	{
		wrkS += " ";
		wrkS += typeStr;
	}	
	wrkS += ">";  wrkS += numeric;
	wrkS += "</"; wrkS += keyStr; wrkS += ">\n"; 
	rc = insertStr(pData,wrkS);
	return rc;
}


RETURNCODE hCformatBase::insertFooter(hCformatData* pData)
{
	RETURNCODE rc = SUCCESS;
	string         wrkS;

	if ( pData == NULL ) return APP_PARAMETER_ERR;

	pData->Undent();// no indent here ( the preceding can be binary )
	wrkS += "</HFDbody>\n</HARTDeviceDescriptionFile>\n"; 
		hCdataElement*    pWrkE = new hCdataElement;
		rc = pWrkE->insert(wrkS.c_str(),wrkS.length());
		if ( rc == SUCCESS )
		{	pData->Add(pWrkE); }// without modifying the insertion point
	return rc;
}


// file header NOT virtual - must always be the same -
//            <HFDbody>         </HFDbody>
// everything between |   and   |  is considered format specific
RETURNCODE hCformatBase::insertHeader(hCformatData* pData, const Indentity_t& ident)
{
	RETURNCODE rc = FAILURE;
	string         wrkS;

	if ( pData == NULL ) return APP_PARAMETER_ERR;

	// new utf encoding 27dec07
	wrkS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<HARTDeviceDescriptionFile>\n"; 
		rc = hCformatBase::insertStr(pData, wrkS); if (rc != SUCCESS) { return rc;}		
	rc = hCformatBase::insertInt(pData, myType, "FormatVersion"); 
		if (rc != SUCCESS) { return rc;}


	wrkS = "<HARTDeviceInfo>\n"; 
		rc = hCformatBase::insertStr(pData, wrkS); if (rc != SUCCESS) { return rc;}

	pData->Indent();
		/* note: wally wants hart version here <UniversalCommand>6</UniversalCommand> wtf4 */
	rc = hCformatBase::insertInt(pData, ident.wManufacturer, "Manufacturer"); 
		if (rc != SUCCESS) { return rc;}
	rc = hCformatBase::insertInt(pData, ident.wDeviceType,   "DeviceType"); 
		if (rc != SUCCESS) { return rc;}
	rc = hCformatBase::insertInt(pData, ident.cDeviceRev,    "DeviceRevision"); 
		if (rc != SUCCESS) { return rc;}
	rc = hCformatBase::insertInt(pData, ident.dwDeviceId,    "DeviceID"); 
		if (rc != SUCCESS) { return rc;}
	pData->Undent();

	wrkS = "</HARTDeviceInfo>\n"; 
		rc = hCformatBase::insertStr(pData, wrkS); if (rc != SUCCESS) { return rc;}

#ifndef _WIN32_WCE	// function missing PAW 06/05/09
	time_t ltime;    
	time( &ltime );
	wrkS = ctime( &ltime );// appends a newline!!!
#else
	struct tm * tmbuffer;
	char buffer[30];
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SystemTimeToTm(&st, tmbuffer);
	strftime_ce(buffer, 26/* no chars*/, "%c\n", tmbuffer);
	for (char loop = 0; loop < 26; loop++)
		wrkS[loop] = buffer[loop];
#endif

	if ( wrkS[wrkS.length()-1] == '\n' )
	{
		wrkS = wrkS.substr(0,wrkS.length()-1);
	}
	rc = hCformatBase::insertStr(pData, wrkS, "Date");
		
	wrkS = "<HFDbody>"; 
		rc = hCformatBase::insertStr(pData, wrkS); if (rc != SUCCESS) { return rc;}
	
	return rc;
}

/**************** NOTE ********************************
 * like the header, errors are ALWAYS plain text 
 * reguardless of being a binary format or otherwise
 ******************************************************/

// the only non-item insertion object
RETURNCODE hCformatBase::insertMember(hCformatData* pData, hCgroupItemDescriptor* pgiD)
{
	RETURNCODE rc = FAILURE;
	string        wrkS;
	hCdataElement* pWrkE = new hCdataElement;
	// info
	//  call reference
	// end

	wrkS = "<member>WARNING: members are unimplemented in this file.</member>\n";
	pWrkE->insert(wrkS.c_str(),wrkS.length());
	pData->Insert(pWrkE);

	return rc;
}

// everything else
RETURNCODE hCformatBase::
	insertObject(hCformatData* pData, hCitemBase* pInfo)
{
	string         wrkS;
	hCdataElement* pWrkE = new hCdataElement;

	switch (  pInfo->getIType()  )
	{
	case iT_Member:				//19
		{// non-item type
			wrkS = "<member>Programmer ERROR (use insertMember())</member>\n";
			pWrkE->insert(wrkS.c_str(),wrkS.length());
		}
		break;
	case iT_Variable:			// 1
	case iT_ItemArray:			// 9
	case iT_Collection:			//10
	case iT_Array:				//15
	case iT_File:				//20
	case iT_List:				//26
		{// do 'em
			// start
			// call item's insert
			// end
			wrkS = "<item>ERROR: unimplemented type(";
			wrkS += itemStrings[ pInfo->getIType() ];
			wrkS += ")</item>\n";
			pWrkE->insert(wrkS.c_str(),wrkS.length());
		}
		break;

	case iT_Record:				//14
	case iT_VariableList:		//16
	case iT_Domain:				//18
		{// unsuported
			wrkS = "<item>ERROR (unsupported type)</item>\n";
			pWrkE->insert(wrkS.c_str(),wrkS.length());
		}
		break;

	case iT_Command:			// 2
	case iT_Menu:				// 3
	case iT_EditDisplay:		// 4
	case iT_Method:				// 5
	case iT_Refresh:			/*relation*/	// 6
	case iT_Unit:				/*relation*/	// 7
	case iT_WaO:				// 8
	case iT_Block:				//12
	case iT_Program:			//13
	case iT_ResponseCodes:		//17
	case iT_Chart:				//21
	case iT_Graph:				//22
	case iT_Axis:				//23
	case iT_Waveform:			//24
	case iT_Source:				//25
	case iT_Grid:				//27
	case iT_Image:				//28
		{// illegal
			wrkS = "<item>ERROR: illegal storage type(";
			wrkS += itemStrings[ pInfo->getIType() ];
			wrkS += ")</item>\n";
			pWrkE->insert(wrkS.c_str(),wrkS.length());
		}
		break;

	case iT_ReservedZeta:		// 0
	case iT_ReservedOne:
	case iT_MaxType:			//  27
	default:
		{// unknown
			wrkS = "<item>Programmer ERROR (unknown type)</item>\n";
			pWrkE->insert(wrkS.c_str(),wrkS.length());
		}
		break;
	}
	pData->Insert(pWrkE);
	return SUCCESS;
}



RETURNCODE hCformatBase::insertObjectEnd(hCformatData* pData, hCitemBase* pInfo)
{ return SUCCESS; // does nothing
}



// fills indentity & format and reads thru <HFDbody>
RETURNCODE hCformatBase::extractHeader(unsigned char*& pNxtByte, int& bytesLeft,
														Indentity_t& retIdent, int& retFmt)
{// Attributes are NOT allowed in the header
	// assume a the entire header (and possibly more) is in this buffer
	RETURNCODE rc = SUCCESS;
	char   wrkC[MAX_ITEM_LEN];
	string wrkS;
	char*  s = (char*)pNxtByte;
	int lp, bigLp, y;

	bigLp = 1;

	while ( bigLp )
	{
		lp = 1;
		while ( lp )
		{
			while(bytesLeft-- && (*s++ != '<'));
			if ( *s == '?' )
			{
				while ( ! ( *s == '?' && *(s+1) == '>' ) )
				{	s++;  bytesLeft--;}  s+=2; bytesLeft-=2;
			}
			if ( *s == '!' && *(s+1) == '-' && *(s+2) == '-')
			{
				while ( ! ( *s == '-' && *(s+1) == '-' && *(s+2) == '>' ) )
				{	s++;  bytesLeft--;  }  s+=3; bytesLeft-=3;
			}
			if ( *(s-1) == '<' ) { lp = 0; }
		}// wend

		y = 0;
		while ( (wrkC[y++] = *s++)  && bytesLeft--)
		{
			//if (*s=='\r'||*s=='\t'||*s=='\n'||*s==' '||*s=='>'||*s=='='||*s =='/'||*s=='<')
			if ( *s == '>' )
			{
				wrkC[y++] = '\0';
				s++;
				bytesLeft--;
				break;
			}
		}// wend fill string

		if (wrkC[0] != '/')
		{
			wrkS = wrkC;
			y = 0;
			if ( wrkS == "FormatVersion")
			{	while ( (wrkC[y++] = *s++) && bytesLeft--  && (*s != '<'));	// fill
				wrkC[y] = '\0';												// terminate
				retFmt = atoi(wrkC);										// convert
			}
			else
			if (wrkS == "Manufacturer" )
			{	while ( (wrkC[y++] = *s++) && bytesLeft--  && (*s != '<'));
				wrkC[y] = '\0';
				retIdent.wManufacturer = (BYTE)(atoi(wrkC) & 0xFF);
			}
			else
			if (wrkS == "DeviceType" )
			{	while ( (wrkC[y++] = *s++) && bytesLeft-- && (*s != '<'));
				wrkC[y] = '\0';
				retIdent.wDeviceType = (BYTE)(atoi(wrkC) & 0xFF);
			}
			else
			if (wrkS == "DeviceRevision" )
			{	while ( (wrkC[y++] = *s++) && bytesLeft-- && (*s != '<'));
				wrkC[y] = '\0';
				retIdent.cDeviceRev = (BYTE)(atoi(wrkC) & 0xFF);
			}
			else
			if (wrkS == "DeviceID" )
			{	while ( (wrkC[y++] = *s++) && bytesLeft-- && (*s != '<'));
				wrkC[y] = '\0';
				retIdent.dwDeviceId = (DWORD)atoi(wrkC);
			}
			else
			if (wrkS == "HFDbody" )
			{
				bigLp = 0; // we're done
			}
			else
			{
				// discard and loop
			}
		}// else ( is a /xxxx ) - discard and loop
	}// wend bigLp

	pNxtByte = (uchar*) s;
	return rc;
}

// call with length Zero & NULL ptr to terminate - expected to consume 'em all
RETURNCODE hCformatBase::extractData(unsigned char*& pNxtByte, int& bytesLeft)
{
	RETURNCODE rc = FAILURE;

	return rc;
}



