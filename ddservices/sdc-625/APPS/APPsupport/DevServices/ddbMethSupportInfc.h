/*************************************************************************************************
 *
 * $Workfile: ddbMethSupportInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is file that defines the base class hCMethSupport.Any host that needs to implement methods
 *    needs to derive from this class to implement UI interactions
 *	
 */


#ifndef _DDBMETHSUPPORTINTERFACE_H
#define _DDBMETHSUPPORTINTERFACE_H

#include "pvfc.h"
#include "ddbGeneral.h"
#include "MethodInterfaceDefs.h"

class hCmethodCall;

// L&T Modifications : MethodSupport - start
class hCddbDevice;
// L&T Modifications : MethodSupport - end

// the interface (supplied by the application) to the i/o channel to the device
// an abstract base class - MUST be subclassed
class hCMethSupport
{
public:	

  

	bool bEnableDynamicDisplay;  //Added by Prashant 17FEB2004 for notification of dynamic variables in a method


	//function to be implemented in derived class that handles initializations to be 
	//done before Method execution begins
#ifdef _DEBUG
	virtual void PreExecute(hCmethodCall& mc) PVFC( "hCMethSupport_d1" );
	//function to be implemented in derived class that handles cleanup to be done
	virtual void PostExecute(hCmethodCall& mc) PVFC( "hCMethSupport_d3" );

#else
	virtual void PreExecute() PVFC( "hCMethSupport_1" );
	//function to be implemented in derived class that handles cleanup to be done
	virtual void PostExecute() PVFC( "hCMethSupport_3" );

#endif

	//function to be implemented in derived class that actually calls the method
	//execution functions
	virtual RETURNCODE DoMethodSupportExecute(hCmethodCall& MethCall) RPVFC( "hCMethSupport_2",0 );

	// true when method(s) running
	virtual bool hasClients(void) RPVFC( "hCMethSupport_4",false );

	//function to be implemented in derived class that handles the display of methods UI 
	virtual bool MethodDisplay(ACTION_UI_DATA structMethodsUIData,ACTION_USER_INPUT_DATA& structUserInputData) RPVFC( "hCMethSupport_5",false );

	/* added 07mar07 by stevev */
	// function to be implemented in derived class that returns the number of methods currently running
	virtual int  numberMethodsRunning() RPVFC( "hCMethSupport_6",0 ); // should check that the number is correct as well
	
	/* 01nov06 - tack-on some UI interfaces needed for layout calcs */
	//   image index, UI looks up image(it knows what lang), converts pixels X pixels to rows X cols & returns
	virtual bool UIimageSize (int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline) RPVFC( "hCMethSupport_7",0 );
	//   string ptr, UI extracts the string(it knows what lang), converts lines X chars then to rows X cols & returns
	virtual bool UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit, bool isDialog=false) RPVFC( "hCMethSupport_8",0 );

// L&T Modifications : MethodSupport - Start
#ifndef __GNUC__
	virtual RETURNCODE DoMethodSupportExecuteForMS(hCmethodCall& MethCall, hCddbDevice *pCurDev) {return SUCCESS;};
	virtual void PreExecuteForMS(hCmethodCall& mc, hCddbDevice *pCurDev) {};
#else
	virtual RETURNCODE DoMethodSupportExecuteForMS(hCmethodCall& MethCall, hCddbDevice *pCurDev){};
	virtual void PreExecuteForMS(hCmethodCall& mc, hCddbDevice *pCurDev){};
#endif
// L&T Modifications : MethodSupport - Start
};


#endif//_DDBMETHSUPPORTINTERFACE_H
