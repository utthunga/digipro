/*************************************************************************************************
 *
 * $Workfile: ddbRfshUnitWaoRel.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		grouping the relation classes
 */


#include "ddbRfshUnitWaoRel.h"
#include "ddbVar.h"


/*************************************************************************************************
 *	hCrelation class
 *   
 * currently just for dependent handling
 *************************************************************************************************
 */
hCrelation::hCrelation(DevInfcHandle_t h, aCitemBase* paItemBase) 
		  : hCitemBase(h,paItemBase) 
{}

RETURNCODE hCrelation::MarkListStale(varPtrList_t& listOvarPtrs)
{
	RETURNCODE rc = SUCCESS;

	if ( listOvarPtrs.size() > 0 )
	{
		for (varPtrList_t::iterator iT = listOvarPtrs.begin(); iT < listOvarPtrs.end(); iT++)
		{// iT is a ptr 2 a ptr 2 a hCVar 
			if (&(*iT) != NULL && *iT != NULL && (*iT)->getDataState() != IDS_PENDING)		// PAW 03/03/09 added &(*)
			{
				(*iT)->MakeStale();
			}
		}
	}// else just return the rc

	return rc;
}

/*************************************************************************************************
 *	hCrefresh class
 *   
 * 
 *************************************************************************************************
 */

hCrefresh::hCrefresh(DevInfcHandle_t h, aCitemBase* paItemBase) 
		 : hCrelation(h,paItemBase), watchList(h), updateList(h)
{		 
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(refreshAttrItems); 

	
	if (aB == NULL)
	{
		aB = paItemBase->getaAttr(refreshAttr_Watch);
		if ( aB != NULL )
		{
			watchList. setEqualTo( &((( aCattrReferenceList*)aB)->RefList) );
			aB = paItemBase->getaAttr(refreshAttr_Update);
			if ( aB != NULL )
			{
				updateList.setEqualTo( &((( aCattrReferenceList*)aB)->RefList)); 
				return; // skip all the legacy and error handling
			}
		}// else fall thru to error
	}// else fall thru to handle legacy attribute
	


	if (aB == NULL)
	{	LOGIT(CERR_LOG, "ERROR: hCrefresh <no refresh items attribute supplied.>\n");
		LOGIT(CERR_LOG, "       trying to find: %d but only have:", (int)refreshAttrItems);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG, " an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG, " 0x%04x, ", (*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG, "\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		watchList. setEqualTo( &(((aCattrRefreshitems*)aB)->condWatchList) );
		updateList.setEqualTo( &(((aCattrRefreshitems*)aB)->condUpdateList)); 
	}
};

hCrefresh::~hCrefresh()
{};


RETURNCODE hCrefresh::destroy(void)
{	RETURNCODE rc;
	rc  = watchList.destroy();
	rc |= updateList.destroy();
	rc |= hCitemBase::destroy();
	return rc;
}


hCattrBase*   hCrefresh::newHCattr(aCattrBase* pACattr)  // builder 
{	LOGIT(CERR_LOG|CLOG_LOG,"ERROR: refresh item accessed unimplemented 'newHCattr'.\n");
	return NULL; /* an error */  
};

RETURNCODE hCrefresh::dumpSelf(int indent, char* typeName) 
{ 
	hCitemBase::dumpSelf(); 
	LOGIT(COUT_LOG,"REFRESH\n");
	return SUCCESS;};

//RETURNCODE hCrefresh::Label(wstring& retStr) 
//{ return baseLabel(NO_LABEL, retStr); };



RETURNCODE hCrefresh::getWatchVarPtrs (varPtrList_t& listOvarPtrs)
{	
	RETURNCODE rc = FAILURE;

	hCreferenceList ListOfrefs(devHndl());// list of references

	rc = watchList.resolveCond(&ListOfrefs);  // a hCcondList<hCreferenceList>

	if (rc != SUCCESS || ListOfrefs.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		listOvarPtrs.clear();	
	    return FAILURE; // no default, bad conditional (no else stmt and else required)
	}
	else
	{			
		hCitemBase* pI = NULL;
		itemType_t  iTy;

		for (hCreferenceList::iterator iL = ListOfrefs.begin(); iL < ListOfrefs.end(); iL++)
		{// iL is a ptr 2 a hCreference
			// resolve the reference		
			rc = iL->resolveID( pI );

			if ( rc == SUCCESS && pI != NULL)
			{
				iTy = pI->getIType();
				if ( iTy == iT_Variable )
				{
					listOvarPtrs.push_back( (hCVar*)pI );
					// leave rc SUCCESS
				}
			}
			else
			{
				LOGIT(CLOG_LOG, "PROBLEM: reference did not resolve the id ptr for hCrefresh.getWatchPtrs.\n");
				rc = APP_RESOLUTION_ERROR;
			}
		}
	}
	return rc;
}

RETURNCODE hCrefresh::getUpdateVarPtrs(varPtrList_t& listOvarPtrs)
{	
	RETURNCODE rc = FAILURE;

	hCreferenceList ListOfrefs(devHndl());// list of references

	rc = updateList.resolveCond(&ListOfrefs);  // a hCcondList<hCreferenceList>

	if (rc != SUCCESS || ListOfrefs.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		listOvarPtrs.clear();	
	    return FAILURE; // no default, bad conditional (no else stmt and else required)
	}
	else
	{			
		hCitemBase* pI = NULL;
		itemType_t  iTy;

		for (hCreferenceList::iterator iL = ListOfrefs.begin(); iL < ListOfrefs.end(); iL++)
		{// iL is a ptr 2 a hCreference
			// resolve the reference		
			rc = iL->resolveID( pI );

			if ( rc == SUCCESS && pI != NULL)
			{
				iTy = pI->getIType();
				if ( iTy == iT_Variable )
				{
					listOvarPtrs.push_back( (hCVar*)pI );
					// leave rc SUCCESS
				}
			}
			else
			{
				LOGIT(CLOG_LOG,"PROBLEM: reference did not resolve the id ptr for hCrefresh.getUpdatePtrs.\n");
				rc = APP_RESOLUTION_ERROR;
			}
		}
	}
	return rc;
}

RETURNCODE hCrefresh::MarkDependentsStale(void)
{
	RETURNCODE rc = SUCCESS;

	varPtrList_t listOvarPtrs;//typedef vector<hCVar*> varPtrList_t;

	if ( getUpdateVarPtrs(listOvarPtrs) == SUCCESS && listOvarPtrs.size() > 0)
	{
		rc = MarkListStale(listOvarPtrs);
	}// else return the the rc
	return rc;
}

bool hCrefresh::isIndependent(itemID_t varID)
{
	varPtrList_t indList;
	varPtrList_t::iterator ipVar;// a ptr 2 ptr 2 hCVar

	if ( getWatchVarPtrs(indList) == SUCCESS && indList.size() > 0 )
	{
		bool any = false;
		for (ipVar = indList.begin(); ipVar != indList.end(); ++ipVar )
		{
			if ((*ipVar)->getID() == varID)
			{// we found a match, that's all it takes to be true
				return true;
			}// else, keep looking
		}// next independent var - at not found, falls thru to false exit
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Refresh Relation has no Independent Variables.\n");
	}
	return false;
}
/*************************************************************************************************
 *	class hCunitRel class
 *   
 * 
 *************************************************************************************************
 */

//hCunitRel::hCunitRel(DevInfcHandle_t h,ITEM_TYPE item_type,itemID_t item_id, unsigned long DDkey)
//		 : hCitemBase(h), indepVar(h), dependentVars(h)
//{
//	LOGIT(CLOG_LOG, "WARNING: itemized constructor in hCunitRel called.\n");
//}

hCunitRel::hCunitRel(DevInfcHandle_t h, aCitemBase* paItemBase) 
		 : hCrelation(h,paItemBase), indepVar(h), dependentVars(h)
{	// label is set in the base class
	 
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(unitAttrItems); 
	
	if (aB == NULL)
	{
		aB = paItemBase->getaAttr(unitAttr_Var);
		if ( aB != NULL )
		{
			indepVar. setEqualTo( &(((aCattrCondReference*)aB)->condRef) );

			aB = paItemBase->getaAttr(unitAttr_Update);
			if ( aB != NULL )
			{
				dependentVars.setEqualTo( &((( aCattrReferenceList*)aB)->RefList)); 
				return; // skip all the legacy and error handling
			}
		}// else fall thru to error
	}// else fall thru to handle legacy attribute



	if (aB == NULL)
	{	LOGIT(CERR_LOG,"ERROR: hCunitRel<no unit items attribute supplied.>\n");
		LOGIT(CERR_LOG,"       trying to find: 0x%04x but only have:", unitAttrItems);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG," 0x%04x, ", (*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG, "\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		indepVar.     setEqualTo( &(((aCattrUnititems*)aB)->condVar) );
		dependentVars.setEqualTo( &(((aCattrUnititems*)aB)->condListOUnitLists)  ); 
	}
};

hCunitRel::~hCunitRel()
{};

RETURNCODE hCunitRel::destroy(void)
{	RETURNCODE rc;
	rc  = indepVar.destroy();
	rc |= dependentVars.destroy();
	rc |= hCitemBase::destroy();
	return rc;
}

hCattrBase*   hCunitRel::newHCattr(aCattrBase* pACattr)  // builder 
{	LOGIT(CERR_LOG|CLOG_LOG,"ERROR: unit relation item accessed unimplemented 'newHCattr'.\n");
	return NULL; /* an error */  
}; 

RETURNCODE hCunitRel::dumpSelf(int indent, char* typeName) 
{ 
	hCitemBase::dumpSelf(); 
	LOGIT(COUT_LOG,"UNIT_RELATION\n");
	return SUCCESS;
};

//RETURNCODE hCunitRel::Label(wstring& retStr) 
//{ return baseLabel(NO_LABEL, retStr); };


RETURNCODE hCunitRel::getIndependVarPtr(hCVar*& pIndependVar)
{
	RETURNCODE rc  = SUCCESS;		
	hCitemBase* pI = NULL;
	itemType_t       iTy;

	hCreference Ref(devHndl());
	rc = indepVar.resolveCond(&Ref);

	if ( rc == SUCCESS )
	{
		rc = Ref.resolveID(pI);
		if ( rc == SUCCESS && pI != NULL)
		{
			iTy = pI->getIType();
			if ( iTy == iT_Variable )
			{
				pIndependVar = (hCVar*)pI ;
				// leave rc SUCCESS
			}
			else
			{
				LOGIT(CLOG_LOG, "Warning: trying to getIndependVarPtr that is a non-variable.\n"); 
				rc = APP_RESOLUTION_ERROR;
			}
		}
		else
		{
			LOGIT(CLOG_LOG,"PROBLEM: reference did not resolve the independent id ptr for "
				" Unit Rel %s (getItemPtrs).\n",getName().c_str());
			rc = APP_RESOLUTION_ERROR;
		}
	}
	else //- problem
	{
		LOGIT(CERR_LOG, "ERROR:  conditional did not resolve for hCunitRel.getItemPtrs.\n");
		rc = APP_RESOLUTION_ERROR;
	}

	return rc;
}

// this returns the resolved dependents
RETURNCODE hCunitRel::getDependVarPtrs (varPtrList_t& listOvarPtrs)
{	
	RETURNCODE rc = FAILURE;

	hCreferenceList ListOfrefs(devHndl());// list of references

	rc = dependentVars.resolveCond(&ListOfrefs);  // a hCcondList<hCreferenceList>

	if (rc != SUCCESS || ListOfrefs.size() <= 0 ) // fix from '=' to '!=' reported by beamex 24feb10
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		listOvarPtrs.clear();	
	    return FAILURE; // no default, bad conditional (no else stmt and else required)
	}
	else
	{			
		hCitemBase* pI = NULL;
		itemType_t  iTy;
		hCreference* pref;

		for (hCreferenceList::iterator iL = ListOfrefs.begin(); iL < ListOfrefs.end(); iL++)
		{// iL is a ptr 2 a hCreference
			pref = &(*iL);
			// resolve the reference		
			rc = pref->resolveID( pI );

			if ( rc == SUCCESS && pI != NULL)
			{
				iTy = pI->getIType();
				if ( iTy == iT_Variable || iTy == iT_Axis)
				{
					listOvarPtrs.push_back( (hCVar*)pI );
					// leave rc SUCCESS
				}
			}
			else
			{
				LOGIT(CLOG_LOG,
				"PROBLEM: reference did not resolve the id ptr for hCunitRel.getItemPtrs.\n");
				rc = APP_RESOLUTION_ERROR;
			}
			pref->destroy();
		}
	}
	ListOfrefs.clear();
	return rc;
}

RETURNCODE hCunitRel::MarkDependentsStale(void)
{
	RETURNCODE rc = SUCCESS;

	varPtrList_t listOvarPtrs;

	if ( getDependVarPtrs(listOvarPtrs) == SUCCESS && listOvarPtrs.size() > 0)
	{
		rc = MarkListStale(listOvarPtrs);
	}// else return the the rc

	return rc;
}

bool hCunitRel::isIndependent(itemID_t varID)
{
	hCVar* pInd =  NULL;
	if (getIndependVarPtr(pInd) == SUCCESS && pInd != NULL)
	{
		return (pInd->getID() == varID);
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Unit Relation has no Independent Variable.\n");
		return false;
	}
}
/*************************************************************************************************
 *	class hCunitRel class
 *   
 * 
 *************************************************************************************************
 */
//hCwao::hCwao(DevInfcHandle_t h, ITEM_TYPE item_type, itemID_t item_id, unsigned long DDkey);
hCwao::hCwao(DevInfcHandle_t h, aCitemBase* paItemBase) 
	 : hCitemBase(h,paItemBase),waoList(h)
{	// label is set in the base class
	 
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(waoAttrItems); 

	if (aB == NULL)
	{	LOGIT(CERR_LOG|CLOG_LOG,"ERROR: hCwao::<no wao items attribute supplied.>\n");
		LOGIT(CERR_LOG|CLOG_LOG,"       trying to find: %d but only have:", (int)waoAttrItems);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG|CLOG_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG|CLOG_LOG, "%u, ",(*iT)->attr_mask);  }
		}
		LOGIT(CERR_LOG|CLOG_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		waoList.setEqualTo( &(((aCattrReferenceList*)aB)->RefList)  ); 
	}
};
hCwao::~hCwao(){};

// required virtual
hCattrBase*   hCwao::newHCattr(aCattrBase* pACattr)  // builder 
{	LOGIT(CERR_LOG|CLOG_LOG, "ERROR: WAO item accessed unimplemented 'newHCattr'.\n");
	return NULL; /* an error */  
};

//    RETURNCODE insert(void** pChunk);
RETURNCODE hCwao::dumpSelf(int indent, char* typeName) 
{ 
	hCitemBase::dumpSelf(); 
	LOGIT(COUT_LOG,"WAO\n");
	return SUCCESS;
};

//RETURNCODE hCwao::Label(wstring& retStr) 
//{ return baseLabel(NO_LABEL, retStr); };

	
RETURNCODE hCwao::getWAOVarPtrs (varPtrList_t& listOvarPtrs)
{	
	RETURNCODE rc = FAILURE;

	hCreferenceList ListOfrefs(devHndl());// list of references

	rc = waoList.resolveCond(&ListOfrefs);  // a hCcondList<hCreferenceList>

	if (rc != SUCCESS || ListOfrefs.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		listOvarPtrs.clear();	
	    return FAILURE; // no default, bad conditional (no else stmt and else required)
	}
	else
	{			
		hCitemBase* pI = NULL;
		itemType_t  iTy;

		for (hCreferenceList::iterator iL = ListOfrefs.begin(); iL < ListOfrefs.end(); iL++)
		{// iL is a ptr 2 a hCreference
			// resolve the reference		
			rc = iL->resolveID( pI );

			if ( rc == SUCCESS && pI != NULL)
			{
				iTy = pI->getIType();
				if ( iTy == iT_Variable )
				{
					listOvarPtrs.push_back( (hCVar*)pI );
					// leave rc SUCCESS
				}
			}
			else
			{
				LOGIT(CLOG_LOG,"PROBLEM: reference did not resolve the id ptr for hCwao.getItemPtrs.\n");
				rc = APP_RESOLUTION_ERROR;
			}
		}
	}
	return rc;
}


/*************************************************************************************************
 *	class hCunitRelList class
 *   
 * 
 *************************************************************************************************
 */

//aka  insertUnique()
void       hCunitRelList::setUnit_Rel(itemID_t unitRelationID)
{// verify not there already
	if (unitRelationID == InvalidItemID) 
		return;
	for (itemIDlist_t::iterator iT = begin(); iT != end(); ++iT)
	{
		if ( *iT == unitRelationID )
		{// it already exists
			return;
		}
#ifdef _DEBUG
		if (*iT == 0)
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: found a zero in the Unit_rel list.\n");
#endif
	}
	push_back(unitRelationID);
	return;
}

// scan the list of possible URs we may be dependent on
// for each one, re-resolve the conditionals & references in the dependent list
// the first one found that has our owner's id in the dependent list is returned
// returns NULL if there is no valid unit relation
hCunitRel*   hCunitRelList::getUnit_Rel(void)
{// new routine - re-resolve 
	hCunitRel* ret = NULL;
	if (ownerID == InvalidItemID || size() <= 0) return ret;

	RETURNCODE   rc       = SUCCESS;
	hCitemBase*  pUnitRel = NULL;
	varPtrList_t listOptrs;
	hCunitRel*   insideret = NULL;
#ifdef _DEBUG
	hCunitRel*   holdret = NULL;
#endif

	for (itemIDlist_t::iterator iT = begin(); iT != end() && (ret == NULL); ++iT)
	{// isa ptr 2a itemID
		if (*iT == InvalidItemID) 
		{
			DEBUGLOG(CLOG_LOG|CERR_LOG,"ERROR: zero item id in UnitRelList.\n");
			continue;
		}
		if ( (rc = pdev->getItemBySymNumber((*iT), &pUnitRel)) == SUCCESS &&
			 pUnitRel != NULL  && pUnitRel->getIType() == iT_Unit)
		{
			if ( (rc = ((hCunitRel*)pUnitRel)->getDependVarPtrs (listOptrs)) == SUCCESS &&
				listOptrs.size() > 0 )
			{// see if owner is there
				for (varPtrList_t::iterator ipT = listOptrs.begin();ipT != listOptrs.end();++ipT)
				{// ipT is a ptr 2 ptr to hCVar
					if ( (*ipT)->getID() == ownerID )
					{// we are related
						insideret = (hCunitRel*)pUnitRel;
						break; // out of inner loop
					}// else keep going
				}// next dependent var
			}
			else //no dependents
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getUnitRel has a relation with no dependents.\n");
			}
		}
		else
		{			
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getUnitRel did not find the relation pointer.(0x%04x)\n",(*iT));
		}
	#ifndef _DEBUG
		ret = insideret;// stops outer loop and returns the value if we found one
	#else /* is DEBUG - see if we end up in more than one relation */
		if ( insideret != NULL )
		{// found one
			if (holdret != NULL)
			{// we found one earlier
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: variable in more than one Unit Relation.");
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG," < 0x%04x  in 0x%04x and 0x%04x >\n",ownerID,
														insideret->getID(),holdret->getID());
			}
			else
			{// we have never found one
				holdret   = insideret;
			}
			insideret = NULL;
		}
		//else  didn't find one this round - leave everything alone
	#endif
	}// next - possible Unit relation with our owner in it
#ifdef _DEBUG
	ret = holdret;
#endif
	return ret;
}

// will return empty ("") string in worst case	
wstring     hCunitRelList::getUnit_Str(void) 
{
	wstring s;
	if (getUnit_String(s) != SUCCESS)
	{
		s = L"";
		// we may want to log here...
	}
	// else - SUCCESS - we be done
	return s;
}

RETURNCODE hCunitRelList::getUnit_String(wstring & str)
{
	RETURNCODE   rc  = FAILURE;
	hCVar*  pUnitVar = NULL;
	hCunitRel* pURel = getUnit_Rel();
	str.erase();

	if (pURel != NULL)
	{	
		if ( (pURel->getIndependVarPtr(pUnitVar) == SUCCESS)
		   &&(pUnitVar != NULL)  
		   )
		{// we have a unit var ptr
			if ( pUnitVar->VariableType() == vT_Enumerated )
			{
				if (pUnitVar->getDataQuality() == DA_HAVE_DATA ||
					pUnitVar->getDataQuality() == DA_STALE_OK )
				{
					CValueVarient cV = pUnitVar->getRealValue();
					rc = ((hCEnum*)pUnitVar)->procureString((UINT32)cV, str);
				}/*endif getDataQuality()*/
				else
				{// add a little more detail
					if (pUnitVar->getDataQuality() == DA_NOT_VALID)
					{
						str = L" Data Not Valid ";

					}
					else //DA_STALEUNK
					{
						str = L" Data Not Known ";
					}
					rc = FAILURE;
				}
			}/*endif vT_Enumerated*/
			else
			{
				// this should NOT be used....str = pUnitVar->getRealValueString();// finally: success
				str = pUnitVar->getDispValueString();// finally: success
				rc = SUCCESS;
			}			
		}
		else
		{// return failure
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getUnit_String failed to get the independent var.\n");
		}
	}// else return failure
	return rc;
}

// returns InvalidItemID if there is none
itemID_t   hCunitRelList::getUnit_VarId(void)
{
	itemID_t   retID = InvalidItemID;
	hCVar*  pUnitVar = NULL;
	hCunitRel* pURel = getUnit_Rel();

	if (pURel != NULL)
	{	
		if ( (pURel->getIndependVarPtr(pUnitVar) == SUCCESS)
		   &&(pUnitVar != NULL)  
		   )
		{// we have a unit var
			retID = pUnitVar->getID();
		}// else return invalid
	}// else return invalid
	return retID;
}

/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
