/*************************************************************************************************
 *
 * $Workfile: ddbAttributes.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The attribute''s upport code.  From ddbVar.cpp, and others to put all attribute code
 *	together for easier maintainence.  The new_Copy() method was added here.
 *
 */

#if defined(__GNUC__)
#include <cmath>
#endif // __GNUC__

#include "ddbVar.h"
#include "ddbAttributes.h"
#include "ddbGrid.h"
#include "ddbAttrEnum.h"
#include "logging.h"



void  hCMinMaxList::setEqual(void* pAclass)
{
	aCminmaxList* pMMlist = (aCminmaxList*) pAclass;
	hCminmaxVal   wrkMinMax(devHndl());

	if (pMMlist == NULL) 
	{
		return;
	}
	clear(); // the vector - if not empty, it will be
	for(ITminMaxList_t iT = pMMlist->begin();iT<pMMlist->end();iT++)
	{// iT is ptr 2a  aCminmaxVal
		aCminmaxVal* pmmV = &(*iT);//stevev 14jun13 stop using iterator as pointer
		wrkMinMax.whichVal = pmmV->whichVal;
		//wrkMinMax.ExprPayload = iT->pExpr; 
		wrkMinMax.condMinMaxExpr.makeEqual( &(pmmV->condExpr) );
		push_back(wrkMinMax);
		wrkMinMax.clear(); // for the next round
	} 

}

void  hCMinMaxList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	if (pPayLd == NULL) 	return;

	hCMinMaxList* pMMlist = (hCMinMaxList*) pPayLd;
	hCminmaxVal   wrkMinMax(devHndl());

	clear(); // the vector - if not empty, it will be
	for(MinMaxIT_t iT = pMMlist->begin();iT<pMMlist->end();++iT)
	{// iT is ptr 2a  hCminmaxVal
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
		|| defined(__GNUC__)
		wrkMinMax.setDuplicate(*(&(*iT)));
#else
		wrkMinMax.setDuplicate(*((hCminmaxVal*)iT));
#endif
		push_back(wrkMinMax);
		wrkMinMax.clear(); // for the next round
	} 
}


RETURNCODE hCattrVarIndex::getAllArrayPtrs(vector<hCitemBase*>& ArrayPtrList)
{
	RETURNCODE rc = SUCCESS;
	HreferencePtrList_t  refPtrList2Arrays;// vector of reference ptrs
	HreferencePtrList_t  ::iterator rpl2aIT;// ptr 2a ptr 2a hCreference
	vector<hCitemBase*>  itemBaseList;
	vector<hCitemBase*>  ::iterator iblIT;

	rc = condIndex.aquirePayloadPtrList(refPtrList2Arrays);
	if ( rc == SUCCESS && refPtrList2Arrays.size() > 0)
	{
		for (rpl2aIT = refPtrList2Arrays.begin(); rpl2aIT < refPtrList2Arrays.end();  rpl2aIT++)
		{// ptr2ptr2ref
			hCreference* pRef = *rpl2aIT;
			if (pRef == NULL)
			{
				DEBUGLOG(CERR_LOG,"Error: Ref Ptr List has a NULL pointer.\n");
				continue;
			}
			if (SUCCESS == pRef->resolveAllIDs(itemBaseList) && itemBaseList.size()>0)
			{
				for (iblIT = itemBaseList.begin(); iblIT < itemBaseList.end();iblIT++)
				{// ptr2aptr2aitembase
					if ( ( (*iblIT)->getIType() == iT_ItemArray)  ||
						 ( (*iblIT)->getIType() == iT_Array    )  ||
						 ( (*iblIT)->getIType() == iT_List     )   )
					{
						ArrayPtrList.push_back(*iblIT);
					}
					else
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: aquired a %s instead of an indexable class "
							"while getting all array pointers.\n",(*iblIT)->getTypeName());
					}
				}
			}
			else //reference didn't resolve
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: array index reference to array did not resolve.\n");
			}
			itemBaseList.clear();
		}
	}
	if (ArrayPtrList.size() == 0 )
	{
		rc = FAILURE;
	}
	return rc;
}


RETURNCODE hCattrVarIndex::getArrayPointer(hCitemBase*& pItemArray)
{
	RETURNCODE rc = SUCCESS;
	hCreference arrayRef(devHndl());
	hCitemBase* pItm; // we don't own what this points to

	rc = condIndex.resolveCond(&arrayRef);
	if ( rc == SUCCESS)
	{
		rc = arrayRef.resolveID( pItm );
		if (rc == SUCCESS && pItm != NULL)
		{
			if (pItm->getIType() != iT_ItemArray && pItm->getIType() != iT_Array && pItm->getIType() != iT_List)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: index attribute points to a Non Array.\n");
				rc = FAILURE;
			}
			else
			{
				pItemArray = pItm;
			}
		}
#ifdef _DEBUG
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: index array reference did NOT resolve.\n");
		}
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: index getArrayPointer conditional did not resolve.\n");
	}
#else
	}
#endif
	arrayRef.destroy();
	return rc;
}

bool  hCRangeList::isInRange(CValueVarient val2Check)
{
/* VMKP Modified on 050104 */
//	bool rv = true;
//	for (RangeList_t::iterator rlIt = begin(); rlIt != end() && rv; ++rlIt)
	bool rv = false;
	RangeList_t::iterator rlIt; // PAW added - see below 03/03/09
	for (/*RangeList_t::iterator PAW */rlIt = begin(); rlIt != end() && !rv; ++rlIt)
/* VMKP Modified on 050104 */
	{
#ifdef _DEBUG
		hCRangeItem* pRI = &(rlIt->second);
#endif
		rv |= rlIt->second.isInRange(val2Check);
	}
#ifdef _DEBUG
	if ( !rv)
	{
		string d,e,n,x;
		for ( rlIt = begin(); rlIt != end() && !rv; ++rlIt)
		{
			if ( rlIt->second.isInRange(val2Check) )
			{	d = "IS"; }
			else
			{	d = "NOT";}

			e = (string)val2Check;
			n = (string)rlIt->second.minVal;
			x = (string)rlIt->second.maxVal;
			DEBUGLOG(CLOG_LOG,"%s  %s in Min:%s Max:%x\n", e.c_str(),d.c_str(),n.c_str(),x.c_str());
		}
	}
#endif
	return rv;
}

double dbldiff(double first, double second)
{
	return fabs(first-second);
}
UINT64 intdiff( UINT64 first, UINT64 second )
{
	if ( first > second )
	{
		return (first - second);
	}
	else
	{
		return (second - first);
	}
}
INT64 intdiff( INT64 first, INT64 second )
{
	if ( first > second )
	{
		return (first - second);
	}
	else
	{
		return (second - first);
	}
}

bool hCRangeList::coerceVal(CValueVarient& val2Fix)
{
	bool rv = false;
	double ddeltaLO, ddeltaHI;// both are zero
	UINT64 ideltaLO, ideltaHI;
	bool isFlt = false;

	/* it says to coerce the value to be within the MIN_VALUE and MAX_VALUE
	   we're going to coerce to the first pair of values.  Someone will have
	   scream pretty loud to make it go to 'closest' min or max out of the list.
	 */
	if ( size() > 0 )  // we have a record
	{
		if ( begin()->second.isInRange(val2Fix) )
		{
			return true; // we coerced it into itself
		}

		if (begin()->second.maxVal.isFloatConst)
		{
			ddeltaHI = dbldiff(val2Fix, begin()->second.maxVal);
			ddeltaLO = dbldiff(begin()->second.minVal, val2Fix);
		
			if (ddeltaLO > ddeltaHI)
			{				
				 val2Fix = begin()->second.maxVal;
			}
			else
			{		
				 val2Fix = begin()->second.minVal;
			}
			return true; //let the next two be handled together...
		}
		else // assume it is an int or a longlong
		if (begin()->second.maxVal.vIsUnsigned)
		{
			ideltaHI = intdiff((UINT64)val2Fix, (UINT64)(begin()->second.maxVal));
			ideltaLO = intdiff((UINT64)(begin()->second.minVal), (UINT64)val2Fix);
		}
		else // assume signed int/longlong
		{
			ideltaHI = intdiff((INT64)val2Fix, (INT64)(begin()->second.maxVal));
			ideltaLO = intdiff((INT64)(begin()->second.minVal), (INT64)val2Fix);
		}

		if (ideltaLO > ideltaHI)
		{				
			 val2Fix = begin()->second.maxVal;
		}
		else
		{		
			 val2Fix = begin()->second.minVal;
		}
		return true;
	}
	// else there is no range list - so we don't know where to coerce it
	return false;
}




double hCattrScale::getScaleFactor(void)
{
	double ret = 1.0;
	if ( this == NULL ) 
	{
		return 1.0;
	}
	if (isConstScale)
	{
		ret = scaleFactor;
	}
	else // resolve it
	{
		hCexprPayload tExpr(devHndl());
		CValueVarient tV;

		if ( condScale.resolveCond(&tExpr) == SUCCESS)
		{
			tV  = tExpr.resolve();
			ret = (double)tV;
			// shortcut if possible
			if (condScale.priExprType == aT_Direct && tExpr.expressionL.size() > 0)
			{
				if (tExpr.expressionL[0].exprElemType == eet_FPCST ||
					tExpr.expressionL[0].exprElemType == eet_INTCST   )
				{
					isConstScale = true;
					scaleFactor  = ret;
				}// else leave it dependent
			}// else leave it false
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: Failed to resolve Scale.\n");
		}
	}
#ifdef _DEBUG
	if ( ret == 0 )
	{
		LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: Zero scale factor being returned.\n");
	}
#endif
	return ret;
}




RETURNCODE hCattrVarDefault::getDfltVal(CValueVarient& returnedValue)
{	
	RETURNCODE rc = SUCCESS;
	hCexprPayload tExpr(devHndl());

	// resolve cond 2 expr
	if ( condDefault.resolveCond(&tExpr)==SUCCESS )
	{
		//resolve expr to varient
		returnedValue = tExpr.resolve();
		if ( returnedValue.vIsValid )
		{
			rc = SUCCESS;
		}
		else
		{
			rc = FAILURE;
		}
	}
	else
	{
		returnedValue.clear();
		rc = FAILURE;
	}
	return rc;
}

void* hCattrValid::procure(bool isAtest)
{ 
	if ( is_UNKNOWN() || validity != externValidity )// unknown or different
	{// recalculate
		Resolved = v_UNKNOWN; // will be unchanged on NO-ELSE condition
		//Resolved.clear();// re-insert the bug for testing
		if ( condValid.resolveCond(&Resolved, isAtest) == SUCCESS )
		{	
			if ( Resolved.getDdlLong() == v_UNKNOWN )
			{
				// validity should be unchanged
			}
			else
			if ( Resolved.getDdlLong() == 0 )
			{
				validity = v_INVALID;
			}
			else // both are valid or unknown
			{
				validity = v_VALID;
			}
		}
		else
		{	
			validity = v_UNKNOWN;
			return NULL;		
		}
	}
	
	if (validity == v_INVALID || externValidity == v_INVALID)
	{
		//validity = v_INVALID;
		Resolved = 0L;
		return &Resolved;
	}
	else // both have to be v_VALID (extern could be Unknown)
	{
		Resolved = 1L;
		return &Resolved;
	}
};


bool hCattrValid::is_UNKNOWN(void)
{	
	if (validity != v_INVALID && validity != v_VALID )
	{
		return true;
	}
	else
	{
		return false;
	}
}

/* * * * the attributes * * * * *
hCattrLabel
hCattrClass
hCattrHndlg
hCattrUnit
hCattrValid
hCattrHelp
hCattrVarRdTime
hCattrVarWrTime
hCattrVarPreRdAct
hCattrVarPostRdAct
hCattrVarPreWrAct
hCattrVarPostWrAct
hCattrVarPreEditAct
hCattrVarPostEditAct
hCattrVarRefreshAct
#ifdef XMTR
hCattrVarPostRequestAct
hCattrVarPostUserAct
#endif
hCattrVarDisplay
hCattrVarEdit
hCattrVarMinVal
hCattrVarMaxVal
hCattrExpr
hCattrScale
hCattrVarIndex
hCattrVarDefault
CattrEdDispLabel
hCattrEdDispPreEditAct
hCattrEdDispPostEditAct
hCattrMethodClass
hCattrMethodScope
hCattrMethodDef
hCattrMethodType
hCattrMethodParams
hCattrGrafixSize
hCattrAxisRef
hCattrLink
hCattrPath
hCattrDebug

#attribute support classes

hCRangeList
hCminmaxVal
hCMinMaxList

* * * * end of attribute list * * */

/**** Caller must setItemPtr() ****/

#define GEN_NEW_CLASS(clsTyp, condName, repflag) \
	clsTyp * pAB = new clsTyp (devHndl(),pItem);\
	pAB-> condName . setDuplicate( condName, repflag )	/*; return pAB;*/

hCattrBase* hCattrLabel::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrLabel,condLabel,false); return pAB;
}
hCattrBase* hCattrClass::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrClass,condClass,false); return pAB;
}
hCattrBase* hCattrHndlg::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrHndlg,condHndlng,false); return pAB;
}
hCattrBase* hCattrUnit::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrUnit,condUnit,false); return pAB;
}
hCattrBase* hCattrValid::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrValid,condValid,false); return pAB;
}
hCattrBase* hCattrHelp::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrHelp,condHelp,false); return pAB;
}
#if 0 //  these are gone
hCattrBase* hCattrVarRdTime::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarRdTime,condRdTimeout,false); return pAB;
}
hCattrBase* hCattrVarWrTime::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarWrTime,condWrTimeout,false); return pAB;
}
#endif
#if 0 /* these aren'tr used any more, went with graphix type */
hCattrBase* hCattrVarWidth::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarWidth,condSize,false); return pAB;
}
hCattrBase* hCattrVarHeight::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarHeight,condSize,false); return pAB;
}
#endif // 0

hCattrBase* hCattrVarPreRdAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPreRdAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPostRdAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPostRdAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPreWrAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPreWrAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPostWrAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPostWrAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPreEditAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPreEditAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPostEditAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPostEditAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarRefreshAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarRefreshAct,condPrePostActList,false); return pAB;
}

hCattrBase* hCattrVarPostRequestAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPostRequestAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarPostUserAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarPostUserAct,condPrePostActList,false); return pAB;
}

hCattrBase* hCattrVarDisplay::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarDisplay,condFmt,false); return pAB;
}

hCattrBase* hCattrUpdateAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrUpdateAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrVarEdit::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarEdit,condFmt,false); return pAB;
}

hCattrBase* hCattrIdentity::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrIdentity,condFileName,false); return pAB;
}

hCattrBase* hCattrShared::new_Copy(itemIdentity_t newID)
{
	hCattrShared *pAB = new hCattrShared(*this);
	return pAB;
}

hCattrBase* hCattrVarMinVal::new_Copy(itemIdentity_t newID)
{
	//GEN_NEW_CLASS(hCattrVarMinVal,condMinMax,false); return pAB;
	hCattrVarMinVal* pMinAttr = new hCattrVarMinVal(devHndl(),pItem);
	pMinAttr->MinMaxList.duplicate((hCpayload*) (&MinMaxList), false);
	return pMinAttr;
}
hCattrBase* hCattrVarMaxVal::new_Copy(itemIdentity_t newID)
{
	//GEN_NEW_CLASS(hCattrVarMaxVal,condMinMax,false); return pAB;
	hCattrVarMaxVal* pMaxAttr = new hCattrVarMaxVal(devHndl(),pItem);
	pMaxAttr->MinMaxList.duplicate((hCpayload*) (&MinMaxList), false);
	return pMaxAttr;
}
hCattrBase* hCattrExpr::new_Copy(itemIdentity_t newID)
{
	hCattrExpr * pAB = new hCattrExpr (devHndl(),attr_Type,pItem);
	pAB-> condExpr . setDuplicate( condExpr ,false); return pAB;
}
hCattrBase* hCattrScale::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrScale,condScale,false);
	pAB->isConstScale = isConstScale;
	pAB->scaleFactor  = scaleFactor;
	return pAB;
}
hCattrBase* hCattrVarIndex::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarIndex,condIndex,false); return pAB;
}
hCattrBase* hCattrVarDefault::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarDefault,condDefault,false); return pAB;
}
hCattrBase* hCattrEdDispPreEditAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrEdDispPreEditAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrEdDispPostEditAct::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrEdDispPostEditAct,condPrePostActList,false); return pAB;
}
hCattrBase* hCattrMethodClass::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrMethodClass,condClass,false); return pAB;
}
hCattrBase* hCattrMethodScope::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrMethodScope,condScope,false); return pAB;
}
hCattrBase* hCattrMethodDef::new_Copy(itemIdentity_t newID)
{
	hCattrMethodDef* pAB = new hCattrMethodDef(devHndl(),pItem);
	pAB->defLen = defLen;
	pAB->pDef   = new char[defLen+1];//via DD@F
	if (pAB->pDef != NULL)
		strncpy(pAB->pDef,pDef,defLen);
	return pAB;
}
hCattrBase* hCattrMethodType::new_Copy(itemIdentity_t newID)
{/* not conditional, no complex internals */
	hCattrMethodType* pAB = new hCattrMethodType(*this);
	return pAB;
}
hCattrBase* hCattrMethodParams::new_Copy(itemIdentity_t newID)
{/* not conditional, no complex internals */
	hCattrMethodParams* pAB = new hCattrMethodParams(*this);
	return pAB;
}
hCattrBase* hCattrGrafixSize::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrGrafixSize,condSize, false); return pAB;
}
hCattrBase* hCattrAxisRef::new_Copy(itemIdentity_t newID)
{
	hCattrAxisRef * pAB = new hCattrAxisRef (devHndl(),attr_Type,pItem);
	pAB-> condAxis . setDuplicate( condAxis ,false); return pAB;
}
hCattrBase* hCattrTimeScale::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrTimeScale,condTS,false); return pAB;
}
hCattrBase* hCattrVarTimeFormat::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrVarTimeFormat,condFmt,false); return pAB;
}




CValueVarient hCattrAxisRef::getValue(void)
{
	CValueVarient rVal;
	rVal.vType = CValueVarient::isSymID;
	rVal.vSize = 4;		
	rVal.vIsValid    = true;

	hCreference* plr = (hCreference*)procure();
	if ( plr != NULL )
	{
		if ( plr->resolveID(rVal.vValue.varSymbolID) != SUCCESS )
		{
			rVal.vValue.varSymbolID = 0;
			rVal.vIsValid = false;
		}// else it's done
	}
	else
	{
		rVal.vValue.varSymbolID = 0;
		rVal.vIsValid = false;
	}
	return rVal;
}

RETURNCODE hCattrAxisRef::getDepends(ddbItemList_t& retList)
{	
	RETURNCODE rc = SUCCESS;
	//sjv 12jul07::>hCreference*	pRef = NULL;
	vector<hCitemBase*> localPtr2Itemvector;
	vector<hCitemBase*>::iterator iT;

	if ( condAxis.isConditional() )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Axis is Illegally Conditional.\n");
		// we are dependent on the axis and the reference route to get there
	}
	if (condAxis.resolveCond(&m_axisRef) == SUCCESS)
	{// we have a reference to an axis
		m_axisRef.aquireDependencyList(retList,false);// get the reference dependenciess
		m_axisRef.resolveAllIDs(localPtr2Itemvector);
		for ( iT = localPtr2Itemvector.begin();iT != localPtr2Itemvector.end();	++iT  )
		{// ptr 2a ptr 2a hCitemBase
			// We are dependent on the axis, the axis is dependent on other stuff 
			//        and should let us know when something changes
			retList.push_back( (*iT) );// usually just one
		}
	}
	return rc;
}

bool  hCattrAxisRef::reCalc(void)
{
	hCreference r(m_axisRef); 
	procure();                  // refills m_axisRef
	return( ! ( r == m_axisRef) ); // true on changed
}

hCattrBase* hCattrLink::new_Copy(itemIdentity_t newID)
{
	hCattrLink * pAB = new hCattrLink (devHndl(),attr_Type,pItem);
	pAB-> condLink . setDuplicate( condLink ,false); return pAB;
}
hCattrBase* hCattrPath::new_Copy(itemIdentity_t newID)
{
	hCattrPath * pAB = new hCattrPath (devHndl(),attr_Type,pItem);
	pAB-> condPath . setDuplicate( condPath ,false); return pAB;
}
hCattrBase* hCattrDebug::new_Copy(itemIdentity_t newID)
{/* not conditional, assume no complex types for now */
	hCattrDebug* pAB = new hCattrDebug(*this);
	return pAB;
}

hCattrBase* hCattrPrivate::new_Copy(itemIdentity_t newID)
{
	hCattrPrivate * pAB = new hCattrPrivate (devHndl(),pItem);
	pAB-> is_Private = is_Private; return pAB;
}
/***********************************************************************************************/

hCattrBase* hCattrGridItems::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrGridItems,condListOGridGrpLists,false); return pAB;
}

hCattrBase* hCattrEnum::new_Copy(itemIdentity_t newID)
{
	GEN_NEW_CLASS(hCattrEnum,condEnumListOlists,false); return pAB;
}



/****** NOTES ************************************************************************************
 *	The default, and most (if not all) written copy constructors will copy a pointer from the
 *	source class into the destination class, giving two pointers to the same object. This works
 *	well for the STL with its continiuous copying.
 *	For the 'typedef' style copy constructor, we need to duplicate EVERYTHING.  This really
 *	gets tough when using the STL.  The 'new_Copy()' class has been added to ALL attributes
 *  to generate a 100% full copy of itself and return the pointer to the copy.
 *   
 * 
 *************************************************************************************************
 */

/*************************************************************************************************
 * ReCalc
 *	these are called from ServiceReplyPkt and so CANNOT re-enter send with a blocking call.
 *
 * Returns:	 true if has-changed or probably-has-changed
 *
 * initially:	stevev 18nov05 - returns didChange
 *				stevev 14dec05 - modified to be NON-re-entrant to send
 *
 ************************************************************************************************/

bool  hCattrValid::reCalc(void)
{
	validity_t v = validity; 
	validity = v_UNKNOWN; 
	procure(true);// fills validity OR leaves unchanged
	return(v != validity);
}


bool  hCattrHndlg::reCalc(void)
{
	UINT64 bs = Resolved.getBitStr(); 
	void* pR = procure(true);
	if ( pR == NULL )// failed
	{
		return true; // assume changed
	}
	else
	{
		return(bs != Resolved.getBitStr());
	}
}


bool  hCattrLabel::reCalc(void)
{// this may have to be revisited when we need to insert the standard "No Label Available"
	wstring s = (wstring)Resolved; 
	//procure();

	Resolved.clear();		//             isATest
	if ( condLabel.resolveCond(&Resolved, true) != SUCCESS )
	{	
		return true;// assume it has changed
	}
	else
	{
		return(s != (wstring)Resolved);
	}
}


bool  hCattrScale::reCalc(void)
{
	double s = scaleFactor; 
	
	if (isConstScale)
	{
		return false;// by definition - constant did not change
	}
	else // resolve it
	{
		hCexprPayload tExpr(devHndl());
		CValueVarient tV;
		//								   isAtest!
		if ( condScale.resolveCond(&tExpr, true) == SUCCESS)
		{	//						  isAtest
			tV  = tExpr.resolve(NULL, true);
			if ( tV.vIsValid )
			{
				return(s != ((double)tV) );
			}
			else
			{// assume it changed
				return true;// assume it changed
			}
		}
		else
		{
			return true;// assume it changed
		}
	}
}


bool  hCattrVarDisplay::reCalc(void)
{
	wstring k, s((wstring)Resolved); 
	
	if ( condFmt.resolveCond(&Resolved, true) != SUCCESS )
	{	
		return true;// assume it has changed
	}
	else
	{
		return(s != (wstring)Resolved);
	}
}


bool hCattrPath::reCalc(void)
{
	ulong bs = (UINT32)Resolved.getDdlLong(); // very limited value set
	if ( procure(true) == NULL )
	{
		return true; // assume it changed if invalid
	}
	else
	{
		return(bs != (UINT32)Resolved.getDdlLong());// very limited value set
	}
}

/* remove - unneeded
bool hCattrExpr::isExprAref(void)
{
	// tbd
	return false;
}
**/


bool  hCattrExpr::reCalc(void) //true at changed
{// 
	CValueVarient v_Current, v_New;
	bool isFirst = true;
	if ( m_expr.expressionL.size() > 0 )
	{// stevev 27jan11 - made this a test to stop Send() deadlock
		v_Current = m_expr.resolve(NULL,true);
		isFirst   = false;
	}
	//else first time through - skip one side
	v_New      = getExprVal();// will overwrite m_expr

	if ( v_Current == v_New || isFirst )
	{
		return false;// first time through will not show changed!
	}
	else
	{
		return true;
	}
}

// for polymorphic overload of expression in COUNT
hCNumeric* hCattrExpr::getExprRef(void)	// returns NULL on not there
{
	hCexprPayload* pExpr;
	hCitemBase* pIB;
	hCNumeric* pretval = NULL;
	RETURNCODE r = SUCCESS;

	typedef hCconditional<hCexprPayload>::hCexpressDest CxpressDef;

	if (condExpr.priExprType      == aT_Direct &&	// polymorphic overload of expression in COUNT
		condExpr.destElements.size() == 1      &&	// MAY NOT BE CONDITIONAL
		condExpr.destElements[0].pPayload != NULL   )
	{
		CxpressDef* pED = &(condExpr.destElements[0]);
		pExpr = (hCexprPayload*) (pED->pPayload);
		if ( pExpr->expressionL.size() == 1  &&		// AND MUST BE A DIRECT REFERENCE !
			 (pExpr->expressionL[0].exprElemType == eet_VARID ||
			  pExpr->expressionL[0].exprElemType == eet_VARREF  ) ) 
		{
			if (pExpr->expressionL[0].exprElemType == eet_VARID)
			{
				if (pExpr->expressionL[0].expressionData.vType == CValueVarient::isDepIndex)
				{
					r = devPtr()->getItemBySymNumber
								  (pExpr->expressionL[0].dependency.depRef.getID(),&pIB);
				}
				else
				{
					r = devPtr()->getItemBySymNumber
							  (pExpr->expressionL[0].expressionData.vValue.varSymbolID,&pIB);
				}
			}
			else // must be a VARREF
			{
				r = pExpr->expressionL[0].dependency.depRef.resolveID(pIB,false);
			}
			// required to be a Numeric Variable
			if ( r == SUCCESS && pIB != NULL ) 
			{
				if ( pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric())
					pretval = (hCNumeric*) ((hCVar*)pIB);
				// else leave it null
			}// else leave it null
		}
	}
	return pretval;
}


CValueVarient hCattrVarMinVal::getMin(void)	// return the smallest of the small
{
	// based on hCMinMaxList
	varientList_t allValues;
	varientList_t::iterator p2Varient;

	CValueVarient  rVal;
	CValueVarient* pVar;

	vector<hCexprPayload *> ppExprList;
	vector<hCexprPayload *>::iterator  ppExpr;
	hCminmaxVal* pMMV;

	double lft,rgt;

	returnValue = 0;
	returnValue.vIsValid = false;
	
	for(MinMaxList_t::iterator iT = MinMaxList.begin();iT<MinMaxList.end();iT++)// ptr 2a hCminmaxVal
	{ 
		pMMV = (hCminmaxVal*)&(*iT);	// PAW added &(*) 03/03/09
		if ( pMMV->condMinMaxExpr.aquirePayloadPtrList(ppExprList) != SUCCESS )// all possible outcomes
		{// all possible hCexprPayload*
			return returnValue;
		}

		for ( ppExpr = ppExprList.begin(); ppExpr != ppExprList.end(); ++ ppExpr)
		{
			hCexprPayload * pExpPld = (hCexprPayload *)(*ppExpr);
if (pExpPld == NULL)
{
	DEBUGLOG(CERR_LOG,"Error: NULL pointer in MinMaxList.\n");
	continue;
}
			rVal = pExpPld->resolve(NULL,true);
			if (rVal.vIsValid)
			{
				allValues.push_back(rVal);
			}
		}
	}
	for ( p2Varient = allValues.begin(); p2Varient != allValues.end(); ++p2Varient )
	{
		pVar = (CValueVarient*)&(*p2Varient);
		if (p2Varient == allValues.begin())
		{
			returnValue = *pVar;
		}
		else		
		{
			lft = (double) *pVar;
			rgt = (double) returnValue;
			if (lft < rgt)
			returnValue = *pVar;
		}
		// else do next
	}
	return returnValue;
}

CValueVarient hCattrVarMinVal::getValue(int which)
{  
	CValueVarient rVal;
	if (which > 0 && which < 64)// 64 from tokenizer
	{
		hCminmaxVal* pVal    = getValptr(which);
		if ( pVal != NULL )
		{
			hCexprPayload Ex(pItem->devHndl());
			if ( pVal->condMinMaxExpr.resolveCond(&Ex) == SUCCESS )
			{
				rVal = Ex.resolve();
			}// else err return
		}// else err return
	}//else err return
	return rVal;
}


CValueVarient hCattrVarMaxVal::getValue(int which)
{  
	CValueVarient rVal;
	if (which > 0 && which < 64)// 64 from tokenizer
	{
		hCminmaxVal* pVal    = getValptr(which);
		if ( pVal != NULL )
		{
			hCexprPayload Ex(pItem->devHndl());
			if ( pVal->condMinMaxExpr.resolveCond(&Ex) == SUCCESS )
			{
				rVal = Ex.resolve();
			}// else err return
			Ex.destroy();
		}// else err return
	}//else err return
	return rVal;
}

/*************************************************************************************************
 *
 *   $History: ddbAttributes.cpp $
 * 
 *************************************************************************************************
 */
