/*************************************************************************************************
 *
 * $Workfile: ddbCritical.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		describes the hCcritical class that is responsible for dependencies on the device level
 *		Used to encapsulate the changing complexity of this responsibility
 *
 *		20jan05	sjv	started creation
 *
 *		
 * #include "ddbCritical.h"
 */

#ifndef _DDBCRITICAL_H
#define _DDBCRITICAL_H

#include "ddbGeneral.h"
#include "dllapi.h"
#include "ddbItemBase.h"

#define MAX_VALIDITY_DEPTH  64 /* arbitrary dependency depth limit */

/* a special helper class for dependencies */
class hHitemList : public ddbItemList_t
{
public:
	RETURNCODE insertUnique(ptr2hCitemBase_t pIB);// unique by symbol number

	void dumpList(void);
};

typedef vector<hHitemList*>         itemListPtrList_t;
typedef itemListPtrList_t::iterator itemListPtrList_it;


class hCcritical
{
	/* temporary lists */
	itemIDlist_t       DDcriticalItemList;// from dd
	itemListPtrList_t  leveledCritical;// from xref

	/* the real working list */
	hHitemList  CriticalItemList; // both of the above in dependency order

	itemIDlist_t CriticalIDList;// the IDs of the CriticalItemList

protected:
	bool existsGreaterEqual(int depth, hCitemBase* pIB);// check items @ >= depth
	void murderLessor(int depth, hCitemBase* pIB);      //remove those @ <  depth

public:
	hCcritical();
	virtual ~hCcritical();


public:
	void fillCritical(aCdevice* pADev);
	bool isItemCritical(itemID_t  itm);

	void insertCriticalItem(hCitemBase* pB, int& depth);// add a leveled dependent
														//@<0,put at new last level
	RETURNCODE consolidateLists(hCdeviceSrvc* pD);//   put temps into working and clear temps

	RETURNCODE getCriticalIDs(itemIDlist_t*& r);
	RETURNCODE getCriticalPtrList(ddbItemList_t*& r);


	void dumpCritcalItems(void){CriticalItemList.dumpList();};
};

/*
void hCreference::addUniqueUInt(UIntList_t& intlist, UINT32 newInt)
{
	for (UIntList_t::iterator pI=intlist.begin();pI < intlist.end();pI++)
	{
		if (*pI == newInt)  return; // duplicate
	}
	intlist.push_back(newInt);// does not exist, add
}

*/
#endif //_DDBCRITICAL_H


/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
