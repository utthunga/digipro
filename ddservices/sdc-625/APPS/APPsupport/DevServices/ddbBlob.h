/*************************************************************************************************
 *
 * $Workfile: ddbBlob.h $
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		14nov08	sjv	created 
 *
 *		
 * #include "ddbBlob.h"
 */

#ifndef _DDB_BLOB_H
#define _DDB_BLOB_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

/*
	axisAttrLabel			use aCattrString 
	axisAttrHelp,			use aCattrString
	axisAttrValidity,		use aCattrCondLong

	axisAttrMinVal,			use aCattrCondExpr
	axisAttrMaxVal,			use aCattrCondExpr
	axisAttrScale,			use aCattrCondLong		- as an enum
	axisAttrUnit,			use aCattrString
*/

class hCblob : public hCitemBase 
{
	hCattrHndlg*		pHandling;	    //aCattrBitstring
	hCattrExpr*         pIdentifier;	//aCattrCondExpr

protected:

public:
	hCblob(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCblob(hCblob*    pSrc,   itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCblob()  {/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(AXIS_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(AXIS_LABEL, l); };

	RETURNCODE Read(wstring &value){/* item cannot be read */ return FAILURE;};
	
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Axis Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Axis ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	int  notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent);
	// I don't know what this is supposed to do...
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);
};


#endif	// _DDB_BLOB_H
