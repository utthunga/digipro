/*************************************************************************************************
 *
 * $Workfile: ddb_XmtrDispatch.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		This is the defined interface from the device to the communication port.
 *		It also incorporates some (possibly user unique) methods for items & transactions.
 *
 * #include "ddb_XmtrDispatch.h"
 */

#ifndef _DDBXMTRDISPATCH_H
#define _DDBXMTRDISPATCH_H

#include "ddbCmdDispatch.h"

class CxmtrDispatcher : public hCcmdDispatcher
{
//	hCdeviceSrvc*	pMyDevice; 	     // init at at construction from handle
//	hCmsgCycleQueue theMsgQueue;
//	hCcommInfc*     pCommInterface;  
//	hCmutex         commMutex;       // linked from application
//	hCmutex         sendMutex;       // linked from application(to make atomic active commands)
public:
	CxmtrDispatcher(hCcommInfc* outComm, DevInfcHandle_t thisHandle );// : hCobject(thisHandle)

	RETURNCODE   //  blocking  send <Host Only --xmtr uses send-and-forget-->
		Send   (hCcommand* pCmd, ulong transNum, bool actionsEn, bool isBlking, 
				hCevent* peDone, hIndexUse_t* pIdxUse);	
	/* replaced in base class
	RETURNCODE   //  blockable send
		SvcSend(hCcommand* pCmd, ulong transNum, hIndexUse_t& useIdx);//bool actionsEn, bool isBlking, hCevent* peDone);
	***/
	RETURNCODE   //  decide next queue service cmd-used below
		getNextQueueTransaction(hCcommand* pReturnedCmd, ulong& returnedTransNum);

	/* not over-ridden notifyVarUpdate()  **/

		bool ServiceWrites(void);    // uses SvcSend     
		bool ServiceReads(void);     // uses SvcSend 

									 // for VBC hCsvcRcvPkt (passed to commInfc)
	void serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo); 
	void servicePacketPair(hPkt* pRqstPkt, hPkt* pRplyPkt) ;// XMTRDD only
	void serviceTransferPacket(hPkt* pPkt, hCcommand* pCmd, int transNum);//block transfer

//	void ReceiveTaskFunction(void);  // the receive function that runs as a task

	RETURNCODE  		//  used by the items via funcPtr
		getBestItemTransaction(itemID_t id, hCcommand* pReturnedCmd, 
								ulong& returnedTransNum, hIndexUse_t& retIdx);

						//  used by the transactions via funcPtr  
	RETURNCODE  calcWeight(hCtransaction* pThisTrans,  hIndexUse_t& retIdx, bool isRead);   

};

#endif //_DDBXMTRDISPATCH_H

/*************************************************************************************************
 *
 *   $History: ddb_XmtrDispatch.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 12:01p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: Xmtr handles commands via pipes, IDE sends Cmd Zero
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:15p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/08/03    Time: 9:08a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */
