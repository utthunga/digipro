/*********************************************************************************************
 *
 * $Workfile: ddbVar.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *		home of the variable instance class
 *		7/26/2	sjv	 created from ddbVariable.h
 *		25jun07 - notes in history area at end of file
 *
 * #include "ddbVar.h"
 */

#ifndef _DDBVAR_H
#define _DDBVAR_H

#pragma warning (disable : 4786) 

#include "pvfc.h"
#include "ddbGeneral.h"
#include "ddbItemBase.h"
#include "ddbPrimatives.h"
#include "ddbConditional.h"
#include "ddbAttributes.h"
#include "ddbRfshUnitWaoRel.h"
#include "dllapi.h"

#include "varient.h"
#include "ddbTracking.h"
#include "Char.h"

#if !defined(__GNUC__)
#define ENUM_ALLOC_DEBUG	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#endif // !defined(__GNUC__)

extern const std::string MT_STRING;

#ifdef ENUM_ALLOC_DEBUG
extern int enumCnt;
extern int enumId;

#define SRC_LOCAL    , 1
#define SRC_EXTRACT  , 2
#define SRC_LOCALREF , 3
#else
#define SRC_LOCAL 
#define SRC_EXTRACT
#define SRC_LOCALREF
#endif
/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */
#define DFLT_PASSWORD "SDC625"

#define STATUS_SIZE				3		/* size of status array				  */
#define STATUS_RESPONSE_CODE	0	 							   
#define STATUS_COMM_STATUS		1
#define STATUS_DEVICE_STATUS	2

#ifdef XMTRDD
 #ifndef XMTR
  #define XMTR		/* for simulation extensions */
 #endif
#endif
// use the ddbGeneral's generic types...sjv 30nov06 typedef vector<ITEM_ID> ITEM_ID_LIST_T;
/* moved to ddbdefs to gain strings ***
typedef enum instanceDataState {
	IDS_UNINITIALIZED, IDS_CACHED, IDS_STALE, IDS_PENDING, IDS_INVALID, IDS_NEEDS_WRITE
} INSTANCE_DATA_STATE_T ;

typedef enum data_avail  {
	DA_HAVE_DATA, DA_NOT_VALID, DA_STALE_OK, DA_STALEUNK
} DATA_QUALITY_T;
*********/

typedef vector<INSTANCE_DATA_STATE_T> instanceStateList_t;

extern char varTypeStrings[VARTYPESTRCNT]  [VARTYPESTRMAXLEN];
extern char instanceDataStStrings[6][INSTANCEDATASTATEMAXLEN];
extern char dataQualityStrings[4][DATAQUALITYMAXLEN];

/* the global mask converter...you have to cast the return value*/
extern int mask2value(ulong inMask); 

typedef enum extract_types  {
	NoFlags,    // 0
	DoNotApply, // 1 Discard extracted value
	DoInfoValue,// 2 Use the value but don't notify or stale dependents
	InfoNotApply// 3 Both
} extract_t;


/**********************************************************************************************
 * hCVar
 *
 * this is base class for VARIABLEs, enahances the CItemBase to provide support
 * for size, class, handling, read/write command lists, pre/post actions, and
 * variable type.
 **********************************************************************************************
 */
	// stevev - WIDE2NARROW char interface \/ \/
/*** Generic character varFunction Calls *****/
/*****NOTE that these are for use OUTSIDE the device object ****/
#ifdef _UNICODE
#define getValidDispValueTString	getValidDispValueString
#define getDispValueTString			getDispValueString
#define getRealValueTString			getRealValueString
#define getEditValueTString			getEditValueString
#else /* not unicode */
#define getValidDispValueTString	getValidDisplayValueString
#define getDispValueTString			getDisplayValueString
#define getRealValueTString			getReal_ValueString
#define getEditValueTString			getEdit_ValueString
#endif // _UNICODE		
/*********************************************/


// zzzz	need command list typedef
class hCattrEnum;
class hCattrClass;
class hCattrHndlg;
class hCattrUnit;
class hCmsgList;

class hClist;		// so we don't have to use the .h file for a count ref
class hCtransaction;
class hCenumDesc;

class hCVar : public hCitemBase 
{
// itemBase holds Validity, Label and Help
// internal holding vars
	variableType_t	varType; // the var type!! { not the item type! }
	unsigned int    varSize; // should match the item size
	int             queryCount;// a weighting multiplier

	grafixSize_t    height;
	grafixSize_t	width;

	/// sjv 11.11.10 hCattrEnum*			pEnum;	// not used
	
	hCattrHndlg*		pHandling;
	hCattrUnit*			pUnit;
	hCattrVarDefault*   pDefaultVal; // stevev added 15jul09

	INSTANCE_DATA_STATE_T dataState;
	DATA_QUALITY_T        dataQuality;

	int                   dataStatus;//stevev 2/27/04-added instead of modifying the state
	int                   writeStatus;//stevev 3/2/04-added instead of modifying the state

	hCcommandDescriptor	readCommand;	// 0xff == NONE 
	hCcommandDescriptor writeCommand;	// 0xff == NONE.

	cmdDescList_t rdCmdList;
	cmdDescList_t wrCmdList;

	// note: label, validity, help inherited from base clase

protected:	/* the generic, base class functionality */		
	bool    isCached;
	hCVar*	pPsuedoDflt;// moved from Numeric 15jul09

	// XREF information
	itemIDlist_t dominantRelations; // refresh and unit - I may be a dominant in these
	// sjv 30nov06 ITEM_ID unitRelation;	// I am dependent in these
	hCunitRelList  unitRelations;	  // I may be dependent in one of these

	itemID_t waoRelation;	// I am inlist	
	
	INSTANCE_DATA_STATE_T previousDataState;

	virtual
		RETURNCODE MarkDependStale(void);
	
	virtual											//extracts the value from an array of bytes
													//used when parsing command responses
		RETURNCODE ExtractIt(BYTE *byteCount, BYTE **data, BYTE* dest, BYTE size);
	virtual											//adds the value to a byte array
													//used when constructing a command request
		RETURNCODE AddIt(BYTE *byteCount, BYTE **data, BYTE* source, BYTE size);	
	virtual											// internal common code for actions
		RETURNCODE getMethodList(varAttrType_t vat,vector<hCmethodCall>& methList);	
	virtual	
		hCVar*     makePsuedoVar(CValueVarient& initValue)
	{	LOGIT(CERR_LOG,"WARN: Base class reached incorrectly  hCVar::makePsuedoVar().\n");
		return NULL;
	};

// cannot find a use????17jun08	
//	hCVar* 	pDefaultValue; // for dependency notification of defaultValue attribute
	/* helper functions */
	RETURNCODE 
		resetCmdDesc(hCcommandDescriptor* pCD,ddbItemList_t& indexPtrList,int newIdxValue);
	RETURNCODE updateIndexMap( hCtransaction* pThisTrans, 
							ddbItemList_t& indexPtrList, int newIdxValue, bool adding = true);

public:
	wstring returnString; //
	// stevev - WIDE2NARROW char interface \/ \/
	string return_String;

        hCattrClass*		pClass;	// bitString is a maskClass_t
  //prashant_begin
  bool m_bLoopWarnVariable ;
  //prashant_end

#ifdef _DEBUG
  bool isNotified;
#endif

//>>>part of item type in item base....string   sTypeName;//TEMPORARY TO HELP WITH DEBUGGING

	bool IsDynamic(void);				//the only class the counts
	bool IsLocal(void);					//   except this one		
	bool IsReadOnly(void);
		
	bool IsConfig(void);


	// RETURNCODE(setSize() --- Use base class ( 1X1 ) for all but hCBitEnum

	/*Vibhor 130104: Start of Code*/
	/*When a var is NOT ReadOnly then use this to differentiate READ_WRITE & WRITE_ONLY
	 handling variables..... Vars with WRITE_ONLY handling don't have a read command!!*/
	/*the definition of isWriteOnly() is actually:: write handling without read handling -sjv*/
		
	bool IsWriteOnly(void);

	
	/*Vibhor 130104: End of Code*/

	bool IsVariable(void) {return true; };
	virtual
	bool IsNumeric(void)  {return false;};
	virtual
	bool IsPassword(void) { return false;};
	virtual
	bool IsUnsigned(void) {if ((varType==vT_Unsigned)     ||(varType==vT_Enumerated)||
							   (varType==vT_BitEnumerated)||(varType==vT_Index)      )
						   return true; else return false;};
	virtual
	bool IsSigned(void){if (varType==vT_Integer) return true; else return false;};

	/* stevev 1/30/04 - need this information for command weighting */
	bool HasReadCmd(void) {if(readCommand.cmdNumber >= 0) return true; else return false;};


	virtual 
		int           VariableType(void)       { return ((int)varType);};//variableType_t
		int           VariableType(variableType_t newType); // new stevev - used sim only
		char*         VariableTypeString(void) { return (varTypeStrings[VariableType()]);};
	virtual 
		unsigned int  VariableSize(void)       { return ( varSize ); };

	virtual hCVar*	  getAttrPtr(unsigned  attr, int which = 0)
	{	hCVar* pV = NULL;
		LOGIT(CERR_LOG,"WARN: Variable Attribute info is not accessable in base class.\n");
		return pV;
	};

	virtual
	CValueVarient   getAttrValue(unsigned attrType, int which = 0)
	{	CValueVarient rvar;
		LOGIT(CERR_LOG,"WARN: Base class reached incorrectly  hCVar::getAttrValue().\n");
		return rvar;
	};

	wstring&		getClassStr(void);// gives 'Unclassed' if doesn't exist
	virtual
	int				getFormatInfo(int& maxChars, int& rightOdec, bool isEdit) // stevev 30jul08 
	// deprecated - do not use in new code, use protected version(not needed this public)
	{	int retval = 0;int fmtWidth;
		wstring whichFormat;
		if (isEdit)
		{
		retval = getFormatInfo(fmtWidth, rightOdec, maxChars,&whichFormat);
		}else{
		retval = getFormatInfo(fmtWidth, rightOdec, maxChars,NULL,NULL,&whichFormat);
		}
		return retval;	
	};
	virtual
		int			getEditFormatInfo(int& maxChars, int& rightOdec, wstring* pScan = NULL);
	virtual
		wstring		getEditFormat(void);// required to keep method dialog infc the same

	// XREF METHODS
	virtual
		RETURNCODE getDominant(itemIDlist_t& returnItemIdList);
	virtual
		hCunitRel* getUnitRel(void){return(unitRelations.getUnit_Rel());};
									//sjv 30nov06:was::>   {return unitRelation;};
	virtual
		void setUnitRel(itemID_t unitRelationID){unitRelations.setUnit_Rel(unitRelationID);};
					//sjv 30nov06:was::>  {unitRelation = unitRelationID;};
	virtual
		itemID_t getWaoRel(void)   {return waoRelation; };

	virtual
		void setWaoRel(itemID_t waoRelationID);//moved 11dec13{waoRelation  = waoRelationID; };
	virtual
		void setDomRel(itemID_t domRelationID);

	// not used - left here for convenience - to be removed
	RETURNCODE SetReadCommand(hCcommandDescriptor c)  {readCommand  = c; return SUCCESS;};
	RETURNCODE SetWriteCommand(hCcommandDescriptor c) {writeCommand = c; return SUCCESS;};

	void addRDdesc(hCcommandDescriptor& rdD);
	void addWRdesc(hCcommandDescriptor& wrD);
	//                        chooses the best rd/wr commands...returns those chosen
	int  setRdWrCmds(void);// hi word = Rd, lo word = Wr commands on return (0xff on none)
	void filterIndexes(hCcommandDescriptor* pCmdDesc);// try to reduce the index combinations
	hCcommandDescriptor  getRdCmd(void) {
		return (readCommand);			};
	hCcommandDescriptor  getWrCmd(void) {
		return (writeCommand);			};
	hCcommandDescriptor  getRdCmd(int cNum,int transactionNum,itemID_t idxSymID) ;// match
	hCcommandDescriptor  getWrCmd(int cNum,int transactionNum,itemID_t idxSymID) ;//match
	hCcommandDescriptor  getRdCmd(int cNum) ;
	hCcommandDescriptor  getWrCmd(int cNum) ;
	//22jan09 - stevev
	RETURNCODE setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue);
	RETURNCODE addCmdDesc(hCcommandDescriptor& rdD, bool isRead);

	INSTANCE_DATA_STATE_T getDataState(void)   {return (dataState);};
	DATA_QUALITY_T        getDataQuality(void) {return (dataQuality);};
	int                   getQueryCnt(void)    {return (queryCount);};
	void                  bumpQueryCnt(void)   { queryCount+=devPtr()->queryWeight();};

	virtual 
		bool 
		didMethodChange(void)
		{ LOGIT(CERR_LOG,"ERROR: base class didMethodChange called!!\n"); return false;};
	virtual 
		bool 
		isChanged(void)
		{ LOGIT(CERR_LOG,"ERROR: base class isChanged called!!\n"); return false;};
	virtual 
		bool SetIt(void* pValue)	// returns isChanged
		{ LOGIT(CERR_LOG,"ERROR: base class SetIt called!!\n");     return false;};
	virtual 
		bool ApplyIt(void)			// returns wasChanged
		{ LOGIT(CERR_LOG,"ERROR: base class ApplyIt called!!\n");   return false;};
	virtual 
		bool CancelIt(void)			// returns wasChanged
		{ LOGIT(CERR_LOG,"ERROR: base class CancelIt called!!\n");   return false;};

		//performs instance data mgt, dispatches command if needed
	/* these are async, they will return immediatly with data quality    */
	
	virtual void  fillCondDepends(ddbItemList_t&  iDependOn);/* stevev 16nov05  */

protected:
	virtual
		DATA_QUALITY_T Read(CValueVarient& returnedDataValue);
	virtual 
		RETURNCODE Write(void);  // mark for writing - another task actually does it
	
	virtual
	int	getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars,
				wstring* pEditFmt = NULL, wstring* pScanFmt = NULL, wstring* pDispFmt = NULL);
//	virtual
//only valid in numerics	wstring	getDispFormat(void);

public:
	virtual 
		DATA_QUALITY_T Read(wstring &value)  // value returned as string
		{ LOGIT(CERR_LOG,"ERROR: base class Read(string) called!!\n");   return DA_NOT_VALID;};

	/* note that all write functions work with the display value		 */
	/*   it is expected that the cmd dispatch will update the real value */
	/* these are async, they will return immediatly with data quality    */
	virtual							// stevev 23feb10 add const to match parent declaration	
									// writes the current string value 2 dispVal,calls write()
		RETURNCODE Write(const wstring value);	
	virtual
		RETURNCODE Write(CValueVarient& newDataValue);//writes to dispVal, calls write()

		
	INSTANCE_DATA_STATE_T markItemState(INSTANCE_DATA_STATE_T newSt, bool needsNotify = true );// returns old state
	void MakeStale(void) { markItemState(IDS_STALE); 
#ifdef _DEBUG
		if (getID() == 0x4083 )
			LOGIT(CLOG_LOG,"Mark 0x%04x Staler\n",getID());
		else
		LOGIT(CLOG_LOG,"Mark 0x%04x Stale\n",getID());
#endif
	};
// redundant::now inside		if (dataQuality==DA_STALE_OK   ||   dataQuality==DA_HAVE_DATA)
//		markItemState			{ dataQuality =DA_STALE_OK;}else{ dataQuality =DA_STALEUNK;} };
			// called in RELATION processing (or dynamic cycle) to tag all dependent as being
			// stale and in need of a re-read from the field device
	void setDataQuality(DATA_QUALITY_T newQuality){dataQuality=newQuality;};

	void setDataStatus(int newStatus){ dataStatus = newStatus;};// 1 normal, 0 stop weighing
	int  getDataStatus(void){return dataStatus;};
	void setWriteStatus(int newStatus){ writeStatus = newStatus;};// 0 normal, 1 user written
	int  getWriteStatus(void){return writeStatus;};


		/*******************************************/
		// usually called via hCVar::newVar(hCVar*) as a global function					
		//static hCVar* newVar(hCVar* pVariable);
		static hCVar* newVar      (DevInfcHandle_t h, aCitemBase * paItem);
		static hCVar* newPsuedoVar(DevInfcHandle_t h, variableType_t varType, int varSize);

		hCattrBase*   newHCattr(DevInfcHandle_t h, aCattrBase* pACattr);  // builder 

		/******* * required methods - ALL children must have these * *******                */
		/* can't make them pure virtual methods because of the template usage of this class */
		/*																					*/
		virtual RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len);
		virtual RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
						methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
						varPtrList_t &hasChangedList, bool doNotApply=false)
		{ extract_t Et = (doNotApply)?DoNotApply:NoFlags; 
			return Extract(byteCount, data, mask, len, eaLst, noteMsgs, hasChangedList, Et); };

		virtual RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
						methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
						varPtrList_t &hasChangedList, extract_t extFlags);

	    hCVar(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCVar(DevInfcHandle_t h, variableType_t vt, int vSize); 
		hCVar(hCVar* pSrc,  itemIdentity_t newID);/* from-typedef constructor */
		virtual ~hCVar();
		RETURNCODE destroy(void);

		// all variables must be able to set their own default value
		//     this one cannot be a pure virtual method due to tempalates
		virtual RETURNCODE setDefaultValue(void) 
		{ LOGIT(CERR_LOG,"ERROR: hCVar's setDefaultValue called.\n"); return FAILURE;};

		/* stevev 8jul05 - add common code to the top level class */
		virtual RETURNCODE setDefaultValue(CValueVarient dfltVarient);
		/* end stevev 8jul05 */
// type may only be changed at contruction time		void setType(int t){varType = t;};
		/// to make itembase happy
//		RETURNCODE load(void) 
//			{cerrxx<<"Call to hCVar load function."<<endl; return VIRTUALCALLERR;};
//		CattrBase* newAttr(struct itemAttrTbl&)
//			{cerrxx<<"Call to hCVar newAttr function."<<endl; return NULL;};
		const wstring& getValidDispValueString(void); // returns empty if invalid 
		// stevev - WIDE2NARROW char interface \/ \/
		const string& getValidDisplayValueString(void);

		virtual wstring& getDispValueString(void) 
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getDispValueString' has been called."
			"\n");  returnString=L"DDB ERROR";  return returnString;   };
		virtual wstring& getRealValueString(void)  //// used for FILE outputs ONLY
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getRealValueString' has been called."
			"\n");  returnString=L"DDB ERROR";  return returnString;   };
	
		// J.U. 17.02.11 DisplayValue property -CAUTION-see 'Invensys checkin' in ddbVar.h 
		virtual wstring& getRealValueFormatedString(void)  
		{	
			return getValueFormatedString(false);

		};
		//this gives a formated and scaled string for the real value and display value 
		// J.U. 17.02.11 DisplayValue property
		virtual wstring& getValueFormatedString(bool bIsDispValue) 
		{//-CAUTION-see 'Invensys checkin' in ddbVar.h 
			LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getValueFormatedString' has been "
			"called.\n");   returnString=L"DDB ERROR";    return returnString;
		}

		virtual wstring& getEditValueString(void)
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getEditValueString' has been called."
			"\n");  returnString=L"DDB ERROR";  return returnString;   };

		// stevev - WIDE2NARROW char interface \/ \/
		virtual string& getDisplayValueString(void) 
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getDisplayValueString' has been "
			"called.\n");  return_String="DDB ERROR";  return return_String;   };
		virtual string& getReal_ValueString(void)  //this gives a string for the real numeric!!
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getReal_ValueString' has been "
			"called.\n");  return_String="DDB ERROR";  return return_String;   };
		virtual string& getEdit_ValueString(int* maxChars = 0,int* maxDecDigits = 0)
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getEdit_ValueString' has been "
			"called.\n");  return_String="DDB ERROR";  return return_String;   };

		virtual CValueVarient getRealValue(void)
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getRealValue' has been called.\n");
			CValueVarient r;  return r;   };// created as invalid
		virtual CValueVarient getDispValue(bool iHaveMutex = false)
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'getDispValue' has been called.\n");
			CValueVarient r;  return r;   };// created as invalid
		
/*Vibhor 270204: Start of Code*/
/* This has to be used by "get" type builtins to get the "raw" data... 
   Not be used in any of the Display builtins !!!!*/
		virtual CValueVarient getRawDispValue(void)
		{	LOGIT(CERR_LOG,"ERROR:hCVar base version of 'getRawDispValue' has been called.\n");
			CValueVarient r; return r;   };// created as invalid
/*Vibhor 270204: End of Code*/

		virtual void setRealValueString(string& s) 
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'setRealValueString' has been "
			"called.\n");  returnString=L"DDB ERROR";  return;   };
		// the next two are assumed to be used from the UI and may return variious errors
		// like out-of-range or is-invalid and NOT set the value (Only SUCCESS indicates set)
		virtual 
		RETURNCODE setDisplayValueString(wstring& s) // see notes at end of file
		{	LOGIT(CERR_LOG,"ERROR: hCVar base wide version of 'setDisplayValueString' has been"
			" called.\n");  returnString=L"DDB ERROR";  return FAILURE;   };
		virtual 
		RETURNCODE setDisplayValue(CValueVarient& newValue)  // see notes at end of file
		{	LOGIT(CERR_LOG,"ERROR: hCVar base 'setDisplayValue' has been called.\n");
			returnString = L"DDB ERROR";  return FAILURE;   };


		virtual void setDispValue(CValueVarient& newValue, bool iHaveMutex = false) //original
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'setDispValue' has been called.\n");
			returnString = L"DDB ERROR";  return;   };
		virtual void setRealValue(CValueVarient& newValue, bool iHaveMutex = false)
		{	LOGIT(CERR_LOG,"ERROR: hCVar base version of 'setRealValue' has been called.\n");
			returnString = L"DDB ERROR";  return;   };
		// stevev 24may11 - to deal with INFO item's replacement after cmd finished

	/* This has to be used by "set" type builtins to set the "raw" data... 
	   Not be used in any of the Display builtins !!!!*/
		virtual void setRawDispValue(CValueVarient& newValue)
		{	LOGIT(CERR_LOG,"ERROR:hCVar base version of 'setRawDispValue' has been called.\n");
			returnString = L"DDB ERROR"; return;   };

		RETURNCODE resolveProcure(void)  { return SUCCESS;/*<<<<<< TODO: like this for now */};
		RETURNCODE Label(wstring& retStr) { return baseLabel(VAR_LABEL, retStr); }; 
		
		 // stevev - WIDE2NARROW char interface
		RETURNCODE Label( string &l) { return baseLabel(VAR_LABEL, l); };

		void testDmp(void) { wstring tmpStr; Label(tmpStr); 
		LOGIT(CLOG_LOG,"    %s:  Type:%s  Size:0x%x State:%d",	 // timj repaired during tokenizer V&V 16jul2014
			tmpStr,varTypeStrings[VariableType()],getSize(),dataState);
		};
		//TODO: RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) ;

		// stevev 30nov06 - getUnitStr gets Unit Relation String ONLY 
		//					(use getUnitString to get either UnitRel or Const)
		// will return empty ("") string in worst case
		wstring getUnitStr(void){return(unitRelations.getUnit_Str());};

		/*Vibhor 120104: Start of Code*/
		/*Adding this function to return the return code as SUCCESS or FAILURE when there's no
		valid string returned by getUnitStr function */	
		virtual /* stevev 05jun08 - for time value overload */
		RETURNCODE getUnitString(wstring & str);// stevev 30nov06 - now gets const | rel unit
		// stevev - WIDE2NARROW char interface
		RETURNCODE getUnitString(string & str){ wstring locStr; 
			RETURNCODE rc = getUnitString(locStr);str = TStr2AStr(locStr);return rc;};

		/*Vibhor 120104: End of Code*/
		/* stevev 1nov04 */
				// returns InvalidItemID if there is none else Unit Relation's dominant var
		itemID_t    getUnitVarId(void){return(unitRelations.getUnit_VarId());}; 
		/* stevev end 1nov04*/

		hCattrClass* getClassAttr(void); // 15sep06 - this function is NOT doing what it is 
		//	supposed to.It was moved to ddbVar.cpp to see if that would help.{ return pClass;};

		/* size test - implemented for numeric only //  true @ val2Check inside min-max set  */
		virtual bool isInRange(CValueVarient val2Check){return true;};
		virtual bool VerifyIt(wstring& s){return true;}	// J.U. 17.03.11<<<See note @ bottom ddbVar.h
		virtual bool isAvalidValue(UINT32 val){return true;};// overloaded for indexes
		virtual UINT64 limitAndExtend(UINT64 orig){return orig;};// base does nothing 

		virtual RETURNCODE getMinMaxList(hCRangeList& retList){return SUCCESS;};
		// made into a list reference: found by codewrights 8oct07

		// Actions //
		RETURNCODE doPreRdActs(void);
		RETURNCODE doPreWrActs(void);
		RETURNCODE getPreRdActs(methodCallList_t& pActList);// stevev  for the reply packet
		RETURNCODE getPstRdActs(methodCallList_t& pActList);
		RETURNCODE getPreWrActs(methodCallList_t& pActList);// stevev for postponed execution
		RETURNCODE getPstWrActs(methodCallList_t& pActList);
		RETURNCODE doPreEditActs(void);
		RETURNCODE doPstEditActs(void);
		RETURNCODE doRefreshActs(void);

		RETURNCODE getPostRequestActs(methodCallList_t& pActList);
		RETURNCODE doPstRequestActs(void);
		RETURNCODE doPstUserActs(void);

		bool hasRefreshActions(void); 

		/*Vibhor 040104: Start of Code*/
		
		 /*Adding this API to get action list as a vector of (method)
		 item Ids*/

		RETURNCODE getActionList(varAttrType_t actionType,vector<itemID_t>& actionList);

		/*Vibhor 040104: End of Code*/

		
	/* new caching methods for DD methods */
	virtual 
	void cacheValue(bool generateNotify = false)
	{LOGIT(CERR_LOG,"ERROR: call to base class cacheValue.\n");};// all data is different
	virtual
	void uncacheVal(void)	//                    uncache if flag set; always clear flag
	{	LOGIT(CERR_LOG,"ERROR: call to base class uncacheVal.\n");		};
	void cacheClear(void) {isCached = false;}; // clear when we don't want to uncache

	virtual int notification(hCmsgList& msgList,bool isValidity); // overload itembase

	virtual RETURNCODE notify(hCmsgList& msgList){return(hCitemBase::notify(msgList));};

	virtual void notifyIchanged(void);// normally called from children
	
	// for tokenizer support
	cmdDescList_t getRdCmdList(void) {return rdCmdList;};
	cmdDescList_t getWrCmdList(void) {return wrCmdList;};

// L&T Modifications : VariableSupport - start
public:
	virtual RETURNCODE doPstEditActsForVS(int temp);
// L&T Modifications : VariableSupport - end
};

// NOW IN ITEMBASE
//typedef vector<hCVar*> variablePtrList_t;
//typedef vector<hCVar>  variableList_t;

/******************************************************************************
 * Cnumeric
 *
 * base class for numeric data types.  enhnaces the DDVariable class to support
 * min/max values, scaling, units, display and edit formats.
 ******************************************************************************
 */
class hCNumeric : public hCVar 
{		// stevev 7aug08 - make private, force use of accessor GetScalingFactor()
	hCattrScale*		pScale;				// always filled (default if not in DD)
protected:			// the following will be null at memory/instantiation error
	hCattrVarDisplay*	pDispFmt;			// always filled (default if not in DD)
	hCattrVarEdit*		pEditFmt;			// always filled (default if not in DD)
	hCattrVarMinVal*    pMinVal;		// stevev 20sep06
	hCattrVarMaxVal*    pMaxVal;		// stevev 20sep06

	hClist*             pList4Count;	// see note at the bottom of this file.stevev 11dec08

	wstring  constant_units;				// if no units then string is empty
	virtual
	int	            getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
	    wstring* pEditFmtStr = NULL, wstring* pScanFmtStr = NULL, wstring* pDispFmtStr = NULL);
	//wstring			processFormat(wstring fmtStr);// helper for getFormatInfo
	wstring      parseFormat(wstring fmtStr,
	/*outputs::*/			wstring& flgs, int& wid, int& prec, wstring& lm, wchar_t& type);

    RETURNCODE validateTrim(wstring & inputStr); // will returnFAILURE on a parsing error, POB - 4/28/2014
		
typedef map<int,CValueVarient> localVarientList_t;

	// for dependency notification of Attribute Value changes
	vector<hCNumeric*> psuedoMinList;
	vector<hCNumeric*> psuedoMaxList;
	// moved to hCVar 15jul09 stevev    hCNumeric*         pPsuedoDflt;
	// support
	hCVar*     makePsuedoVar(CValueVarient& initValue);

	virtual     CValueVarient scaleValue(CValueVarient toBscaled, wstring fmt=L"")
				{	return toBscaled;	};
	virtual    wstring&      formatValue(CValueVarient incoming, wstring format); // to any
	virtual    wstring&      scaleNformatValue(CValueVarient incoming);//always display format

	CValueVarient condition(CValueVarient& src, IntType_t intType);

public:
	hCNumeric(DevInfcHandle_t h, aCitemBase* paItemBase);//body in ddbvar.cpp
	hCNumeric(DevInfcHandle_t h, variableType_t vt, int vSize);// allow temporary variables
	hCNumeric(hCNumeric* pSrc,  hCNumeric* pVal, itemIdentity_t newID);/* from-typedef */
//		hCNumeric(){clear();};
	virtual ~hCNumeric()/* code moved to .cpp 10apr07*/;
	virtual RETURNCODE setDefaultValue(void) RPVFC( "hCNumeric1",0 );

    //changed this method access level from protected to public by sadanand utthunga
    wstring		    getDispFormat(void);// will return default if nothing else available

	void clear(void){
	/*min_value= -numeric_limits<float>::infinity();  //numeric_limits<float>::quiet_NaN();
      max_value=  numeric_limits<float>::infinity();  */
		constant_units.erase();DESTROY(pDispFmt);DESTROY(pEditFmt);DESTROY(pScale);
							   DESTROY(pMinVal); DESTROY(pMaxVal);  };
	/* the following accessors are only good for min/max/dflt-val - zeros on all else*/
	CValueVarient getAttrValue(unsigned attrType, int which = 0);
	virtual
	itemID_t      getAttrID   (unsigned  attr, unsigned which = 0);// for psuedoVariables
	hCVar*        getAttrPtr  (unsigned  attr, unsigned which = 0);// 05sep06 

	wstring		  getEditFormat(void);// required for method dialog
	int			  getEditFormatInfo(int& maxChars, int& rightOdec, wstring* pScan = NULL);

	// calls will fall thru to hCVar's generic version of this call
	//int		getFormatInfo(int& maxChars, int& rightOdec, bool isEdit);// stevev 15jul08 
	// deprecated - do not use in new code, use protected version(not needed this public)
	CValueVarient getValueFromStr(wstring inputStr);//result invalid on error

	bool IsNumeric(void)  { return true; };

	/*** * pure virtual methods - ALL children must have these * ******/
	virtual RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len);
	virtual RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
						methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
			   varPtrList_t &hasChangedList, extract_t extFlags)RPVFC( "hCNumeric3",0 );

//		virtual RETURNCODE Read(string &value);	
//		virtual RETURNCODE Write(string value);	

	/* caller has to add the '%' to the format string */
	/* Do not use - edit formats are WAY more complex than that
	RETURNCODE ReadForEdit(wstring &editStr) {	//used when editing the variable
		if (pEditFmt)
		{ return pEditFmt->getFmtStr(editStr);}
		else 
		{editStr=L"";}						
		return SUCCESS;
	};
	***/
	// stevev - WIDE2NARROW char interface
	/* stevev - wide only
	RETURNCODE ReadForEdit(string &editStr) {	//used when editing the variable
		if (pEditFmt){ wstring locStr; RETURNCODE rc = pEditFmt->getFmtStr(locStr);
						editStr = TStr2AStr(locStr); return rc;}
		else {editStr="";						     return SUCCESS;}
	};
	***/

/* sizing functions */
	virtual void lowest (CValueVarient& retVal) PVFC( "hCNumeric4" );
	/* default { retVal = -numeric_limits<double>::infinity();};// smallest default*/
	virtual void highest(CValueVarient& retVal) PVFC( "hCNumeric5" );
	/* default { retVal =  numeric_limits<double>::infinity();};// largest default*/
	virtual 
	RETURNCODE resolveRangeList(hCRangeList& retList, IntType_t intType = No_Type, 
																	bool isAtest = false);
	// defined in hCVar::> 
	virtual bool isInRange(CValueVarient val2Check);//true @ val2Check ok in min-max set
	virtual RETURNCODE getMinMaxList(hCRangeList& retList); 
	virtual bool VerifyIt(wstring& s);	// J.U. 17.03.11<<<See note @ bottom ddbVar.h
	/*
	 * note: ReadUnits should be deleted and the engineering units returned as part 
	 *		the engineering value returned in Read(string&) for the 
	 */		
	//      returns the current value as a string
	virtual RETURNCODE ReadUnits(wstring &units) {units=constant_units; return SUCCESS;};	
	
	/*Adding following APIs for getting Disp formats & scaling factor*/

	/* Do not use - formats are WAY more complex than that-use getDispValueString
	RETURNCODE ReadForDisp(wstring &dispStr) {	//used when displaying the variable
		if  (pDispFmt){ return pDispFmt->getFmtStr(dispStr);}
		else {dispStr=L"";}						return SUCCESS;
	};
	**/
	virtual		/* will be replaced in time_value */
	double GetScalingFactor() {
		if (pScale){ return pScale->getScaleFactor();}
		else{return 1.0;}
	};
	
	hClist* getCountList(void){  return (pList4Count);};
	void    setCountList(hClist* pL){    
		pList4Count = pL;  };
	virtual void  fillCondDepends(ddbItemList_t&  iDependOn);/* stevev 16nov05 */

	virtual int notification(hCmsgList& msgList,bool isValidity); // overload hCVar
};

/******************************************************************************
 * CFloat
 *
 ******************************************************************************
 */
class hCFloat : public hCNumeric 
{
	float fValue;

	float f_WrtVal;//raw disp value

	float f_cacheVal;


public:
//*%		hCFloat(aCvTfloat* pActual);//,hCVar* pV);
	hCFloat(DevInfcHandle_t h,aCitemBase* paItemBase);//,hCVar* pV);
	hCFloat(hCFloat*   pSrc,  hCFloat*   pVal,itemIdentity_t newID);
	hCFloat(DevInfcHandle_t h); // for temporary variables

	RETURNCODE setDefaultValue(void);
//void lowest (CValueVarient& retVal){retVal = (float)(-numeric_limits<float>::infinity() );};
//void highest(CValueVarient& retVal){retVal = (float)( numeric_limits<float>::infinity() );};
	// 16jul08 - changed to actual largest value to verify max characters
	void lowest (CValueVarient& retVal){retVal = 0-(float)FLT_MAX;};
	void highest(CValueVarient& retVal){retVal =   (float)FLT_MAX;};

	DATA_QUALITY_T Read(float &value);		//returns the current value
	DATA_QUALITY_T Read(wstring &value);	
	RETURNCODE Write(float value);		//writes the current value
	RETURNCODE Write(const wstring value);// stevev 23feb10 add const match parent declaration	
	RETURNCODE Write(CValueVarient& newDataValue);

	//used when parsing command responses
	RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
					methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
					varPtrList_t &hasChangedList, extract_t extFlags);  
	//used when constructing a command request
	RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len);	

	CValueVarient scaleValue(CValueVarient toBscaled, wstring fmtStr=L"")
		{	CValueVarient rv; rv = (float)(((float)toBscaled) * GetScalingFactor() );
			return rv;};
	wstring& getDispValueString(void);	
	wstring& getEditValueString(void);
	wstring& getRealValueString(void)  // used for FILE outputs ONLY
	{	wchar_t fchar[64];
#if defined(__GNUC__)
		swprintf(fchar,(sizeof(fchar)/sizeof(fchar[0])),L"%f",fValue);
#else
		swprintf(fchar,L"%f",fValue);
#endif // __GNUC__
		returnString = fchar;; return returnString; 
	};
	string& getDisplayValueString(void){return_String = TStr2AStr(getDispValueString()); 
			return return_String;  };
	/// returns formated display value or formated device value 
	// J.U. 17.02.11
	//CAUTION-see'Invensys checkin'in ddbVar.h 
	wstring& getValueFormatedString(bool bIsDispValue);

	CValueVarient getRealValue(void)
	{	CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = fValue;
		retVal.vSize = getSize();
		if (IsValidTest()){retVal.vIsValid = true;}else{retVal.vIsValid = false;}
		return retVal;
	};
	CValueVarient getDispValue(bool iHaveMutex = false)
	{
		int w,p,m;
		wstring fmtStr;
		int t = getFormatInfo(w,p,m, NULL,NULL, &fmtStr); 
		
		CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = f_WrtVal; 
		retVal.vSize = getSize();
		retVal = scaleValue(retVal,fmtStr);
		if (IsValid())retVal.vIsValid = true;else retVal.vIsValid = false;
		return retVal;
	};
/*Vibhor 270204: Start of Code*/	
	CValueVarient getRawDispValue(void)
	{	
		/*No Scaling !!!*/
		CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = f_WrtVal;
		retVal.vSize = getSize();
		if (IsValid())retVal.vIsValid = true;else retVal.vIsValid = false;
		return retVal;
	};	
/*Vibhor 270204: End of Code*/	
	//virtual 
	RETURNCODE setDisplayValue(CValueVarient& newValue); // from UI
		
	RETURNCODE setDisplayValueString(wstring& s); // from UI

	void setRealValueString(string& s) 
	{
		float fL = 0.0;
		if ( s.size() > 0 )
		{
			sscanf(s.c_str(),"%f",&fL);
		}
		/*fValue = */fValue = fL;
	};
	void setDispValue(CValueVarient& newValue, bool iHaveMutex = false);
	void setRealValue(CValueVarient& newValue, bool iHaveMutex = false);// NOT SCALED

/*Vibhor 270204: Start of Code*/
/*Just set the raw value... no unscaling*/
	void setRawDispValue(CValueVarient& newValue);

/*Vibhor 270204: End of Code*/

	bool didMethodChange(void);// stevev 15feb07 4xmtr
	bool isChanged(void);//{return(f_WrtVal != fValue);};
	bool SetIt(void* pValue)	// returns isChanged - I don't think this should be scaled
	{	f_WrtVal = *((float*)pValue); 
#ifdef _DEBUG
	if (_isnan(f_WrtVal))
	{ 
		LOGIT(CLOG_LOG, "0x%04x set Display to Not a Number.\n",getID());// get a breakpoint
	}
#endif
		return isChanged();
	};   
	bool ApplyIt(void)			// returns wasChanged
	{ bool r = isChanged(); fValue = f_WrtVal; return r;};   
	bool CancelIt(void)	
	{ bool r = isChanged(); f_WrtVal = fValue;  
#ifdef _DEBUG
	if (_isnan(f_WrtVal))
	{ 
		LOGIT(CLOG_LOG, "0x%04x CancelIt set Display to Not a Number.\n",getID());// get a breakpoint
	}
#endif
	setWriteStatus(0);/*sjv 25jun07*/return r;};  
		
	/* sizing functions */
	/*void lowest (CValueVarient& retVal)   Use default parent */ // smallest default
	/*void highest(CValueVarient& retVal)   Use default parent */ // largest default

	bool isInRange(CValueVarient val2Check);// true @ val2Check inside min-max set
/* VMKP added on 020104 */
	RETURNCODE getMinMaxList(hCRangeList& retList);
/* VMKP added on 020104 */
	void cacheValue(bool generateNotify = false){
		if (generateNotify && f_cacheVal != f_WrtVal) {notifyIchanged();}
		f_cacheVal=f_WrtVal;isCached=true;	};
	void uncacheVal(void){
		if(isCached)
		{	f_WrtVal=f_cacheVal; if (IsLocal()){ ApplyIt(); } }
		isCached=false; };
};

/******************************************************************************
 * CDouble
 *
 ******************************************************************************
 */
class hCDouble : public hCNumeric {
	double dValue;

	double d_WrtVal;

	double d_cacheVal;

public:
//*%		hCDouble(aCvTdouble* pActual);//,hCVar* pV);
	hCDouble(DevInfcHandle_t h,aCitemBase* paItemBase);//,hCVar* pV);
	hCDouble(hCDouble*  pSrc,  hCDouble*  pVal, itemIdentity_t newID);

	RETURNCODE setDefaultValue(void);
//void lowest (CValueVarient& retVal){retVal=(double)(-numeric_limits<double>::infinity());};
//void highest(CValueVarient& retVal){retVal=(double)( numeric_limits<double>::infinity());};
	// 16jul08 - changed to actual largest value to verify max characters
	void lowest (CValueVarient& retVal){retVal=0-(double)DBL_MAX;};
	void highest(CValueVarient& retVal){retVal=  (double)DBL_MAX;};


	DATA_QUALITY_T Read(double &value) ;		//returns the current value
	DATA_QUALITY_T Read(wstring &value) ;	
	RETURNCODE Write(double value);		//writes the current value
	RETURNCODE Write(const wstring value);// stevev 23feb10 add const match parent declaration
	RETURNCODE Write(CValueVarient& newDataValue);
	
	//used when parsing command responses
	RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
					methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
					varPtrList_t &hasChangedList, extract_t extFlags); 	
	//used when constructing a command request
	RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len); 
	
	CValueVarient scaleValue(CValueVarient toBscaled, wstring fmtStr = L"")
		{	CValueVarient rv; rv = (double)(((double)toBscaled) * GetScalingFactor() );
			return rv; };
	wstring& getDispValueString(void);	
	wstring& getEditValueString(void);
	/// this gives a formated and scaled string for the real value or display value
	// J.U. 17.02.11
	//CAUTION-see 'Invensys checkin' in ddbVar.h  
	wstring& getValueFormatedString(bool bIsDispValue);
	wstring& getRealValueString(void)  // used for FILE outputs ONLY
	{	wchar_t fchar[64];
#if defined(__GNUC__)
		swprintf(fchar,(sizeof(fchar)/sizeof(fchar[0])),L"%f",dValue);
#else
		swprintf(fchar,L"%f",dValue);
#endif // __GNUC__
		returnString = fchar;; return returnString; 
	};
	//virtual 
	RETURNCODE setDisplayValue(CValueVarient& newValue); // from UI w/ range test
	RETURNCODE setDisplayValueString(wstring& s);  // from UI w/ range test

	void setRealValueString(wstring& s) 
	{
		double fL = 0.0;
		if ( s.size() > 0 )
		{
			swscanf(s.c_str(),L"%lf",&fL);
		}
		/*dValue = */dValue = fL;
	};
	void setDispValue(CValueVarient& newValue, bool iHaveMutex = false);
	void setRealValue(CValueVarient& newValue, bool iHaveMutex = false);

/*Just set the raw value... no unscaling*/
	void setRawDispValue(CValueVarient& newValue);

	CValueVarient getRealValue(void)
	{	CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = dValue;
		retVal.vSize = getSize();
		if (IsValidTest())retVal.vIsValid = true;else retVal.vIsValid = false; 
		return retVal;
	};
	CValueVarient getDispValue(bool iHaveMutex = false)
	{	
		int w,p,m;
		wstring fmtStr;
		int t = getFormatInfo(w,p,m, NULL,NULL, &fmtStr); 
	
		CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = d_WrtVal;
		retVal.vSize = getSize();
		retVal = scaleValue(retVal,fmtStr);
		if (IsValid())retVal.vIsValid = true;else retVal.vIsValid = false;
		return retVal;
	};

/*Vibhor 270204: Start of Code*/	
	CValueVarient getRawDispValue(void)
	{	
		/*No Scaling !!!*/
		CValueVarient retVal;
		retVal.vType = CValueVarient::isFloatConst;
		retVal.vValue.fFloatConst = d_WrtVal;
		retVal.vSize = getSize();
		if (IsValid())retVal.vIsValid = true; else retVal.vIsValid = false;
		return retVal;
	};	
/*Vibhor 270204: End of Code*/	

	bool didMethodChange(void);
	bool isChanged(void);//{return(d_WrtVal != dValue);};
	bool SetIt(void* pValue)	// returns isChanged
	{   d_WrtVal = *((double*)pValue);// I don't think this needs to be scaled
		return isChanged();
	}; 


	bool ApplyIt(void)			// returns wasChanged
	{ bool r = isChanged(); dValue = d_WrtVal; return r;};
	bool CancelIt(void)	
	{ bool r = isChanged(); d_WrtVal = dValue; setWriteStatus(0);/*sjv 25jun07*/return r;};		
		
	bool isInRange(CValueVarient val2Check);
/* VMKP added on 020104 */
	RETURNCODE getMinMaxList(hCRangeList& retList);
/* VMKP added on 020104 */
	void cacheValue(bool generateNotify = false){
		if (generateNotify && d_cacheVal != d_WrtVal) {notifyIchanged();}
		d_cacheVal=d_WrtVal;isCached=true;};
	void uncacheVal(void){
		if(isCached){d_WrtVal=d_cacheVal;if(IsLocal()){ApplyIt();} }
		                 isCached=false;};
};

/******************************************************************************
 * hCinteger
 *
 * base class for integer data types.  There are too many simularities not to
 * have a single integer class that is common
 ******************************************************************************
 */
class hCinteger : public hCNumeric 
{
protected:			/* since we are designed as a base class - don't make these private */
	UINT64 Value;	/* device value */

	UINT64 Wrt_Val; /* display value */

	UINT64 cacheVal;

	bool   isSigned;
	//     gives number to shift for masks (number of zeros right of first 1 bit)
	UINT32 right0bits(UINT64 value); 

	UINT64 limitAndExtend(UINT64 orig);// limit size and Sign extend signed negative values
	
	virtual
	bool   sizeAndScale(CValueVarient& retVal);//false = out-of-range, true=new value in retVal

	void updateCount(void);// if this is a local list count, update values

public:
	hCinteger(DevInfcHandle_t h, aCitemBase* paItemBase, bool isS = false); 
	hCinteger(hCinteger*  pSrc,  hCinteger*  pVal, itemIdentity_t newID);
	virtual ~hCinteger() { /* does nothing right now */ };

	virtual RETURNCODE setDefaultValue(void);
	virtual void lowest (CValueVarient& retVal);
	virtual void highest(CValueVarient& retVal);


	virtual
		void clear(void);


	virtual
		DATA_QUALITY_T Read(UINT64 &value);	 // uses ReadIt to get the value and return it
	virtual
		RETURNCODE Write(const UINT64 value);// set display value, write the device
	virtual
		DATA_QUALITY_T Read(wstring &value);	
	virtual
		RETURNCODE Write(const wstring value);	

	//CW fix begin - B27901 added missing reference operator // 29aug11
	virtual
		RETURNCODE Write(CValueVarient& newDataValue);
	//CW fix end

	virtual
		wstring& getDispValueString(void);	// this gives a string for the display numeric!!
	virtual
		wstring& getEditValueString(void);  // this gives a string for the editing of numeric!!
		// J.U. this gives a formated and scaled string for the real value // J.U. 17.02.11 
	virtual  //CAUTION-see 'Invensys checkin' in ddbVar.h  
	wstring& getValueFormatedString(bool bIsDispValue);
	virtual
		wstring& getRealValueString(void);	// used for FILE outputs ONLY
	virtual		
		RETURNCODE setDisplayValueString(wstring& s);// from UI with range test
	virtual 
		RETURNCODE setDisplayValue(CValueVarient& newValue);// from UI w/ rangetest 
	/*virtual		
		void setDispValueString(string& s);*/
	virtual		
		void setDispValue(CValueVarient& newValue, bool iHaveMutex = false);
	virtual
		void setRealValue(CValueVarient& newValue, bool iHaveMutex = false);

/*Vibhor 270204: Start of Code*/
/*Just set the raw value... no unscaling*/
	void setRawDispValue(CValueVarient& newValue);

/*Vibhor 270204: End of Code*/

	virtual		
		void setRealValueString(string& s);

	virtual		
		CValueVarient getRealValue(void);
	virtual				// may be unused, included for symmetry
		CValueVarient getDispValue(bool iHaveMutex = false);	

	virtual
		CValueVarient getRawDispValue(void); // may be unused, included for symmetry

		// use numeric's::>   
		//				wstring&   scaleNformatValue(CValueVarient incoming);
		CValueVarient scaleValue(CValueVarient toBscaled, wstring targetFmt=L"");
	virtual		/* be sure it gets on the vft */
		double GetScalingFactor(){ return hCNumeric::GetScalingFactor();};

	virtual
		bool didMethodChange(void);// stevev 15feb07 4xmtr
	virtual	
		bool isChanged(void);/*
		{
			return(Wrt_Val != Value);
		};*/
	virtual
		bool SetIt(void* pValue);// Sets the Write Value (not the Real Val) returns isChanged
	virtual
		bool ApplyIt(void);					// returns wasChanged
											// code moved to .cpp 11dec08
	virtual
		bool CancelIt(void);				// returns wasChanged
											// code moved to .cpp 11dec08
										
	virtual									//used when parsing command responses
		RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
							methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
							varPtrList_t &hasChangedList, extract_t extFlags);
	virtual									//used when constructing a command request  	
		RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len); 	
	/* sizing functions */
//	void lowest (CValueVarient& retVal)  // smallest default
//	{ if (! isSigned) { retVal = (unsigned int) 0;}
//	  else            { retVal = (long)(0 - (long)( ((unsigned long)1) << ((getSize()*8)-1)));}
//	};
//	void highest(CValueVarient& retVal);  // largest default
	bool isInRange(CValueVarient val2Check);// true @ val2Check inside min-max set
/* VMKP added on 020104 */
	RETURNCODE getMinMaxList(hCRangeList& retList);
/* VMKP added on 020104 */
	void cacheValue(bool generateNotify = false){
		if (generateNotify && cacheVal != Wrt_Val) {notifyIchanged();}
		cacheVal=Wrt_Val;isCached=true;};
	void uncacheVal(void){
		if(isCached){Wrt_Val=cacheVal;if(IsLocal()){ ApplyIt();}}isCached=false;};
#ifdef XX_DEBUG
	UINT64 getCacheVal(void){return cacheVal;};
#endif
};


/******************************************************************************
 * CUnsigned
 *
 ******************************************************************************
 */
class hCUnsigned : public hCinteger 
{
public:
//*%		hCUnsigned(aCvTunsigned* pActual);//,hCVar* pV);
	hCUnsigned(DevInfcHandle_t h, aCitemBase* paItemBase);//,hCVar* pV);
	hCUnsigned(hCUnsigned* pSrc,  hCUnsigned*  pVal, itemIdentity_t newID):
	           hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID){};

	virtual		/* be sure it gets on the vft */
	double GetScalingFactor(){return hCNumeric::GetScalingFactor(); };
//use base				RETURNCODE setDefaultValue(void);
//use base				virtual void lowest (CValueVarient& retVal);
//use base				virtual void highest(CValueVarient& retVal);

//use base				RETURNCODE Read(UINT32 &value);		//returns the current value
		// weird compile problem - couldn't resolve the overloaded read from the UI
// try to use cinteger's		RETURNCODE Read(UINT32 value) {return (hCVar::Read(value));};
// try to use cinteger's		RETURNCODE Read(string &value);	
//use base		RETURNCODE Write(UINT32 value);		//writes the current value
		// weird compile problem - couldn't resolve the overloaded write from the UI
// try to use cinteger's		RETURNCODE Write(UINT32 value) {return (hCVar::Write(value));};
// try to use cinteger's		RETURNCODE Write(string value);		
// try to use cinteger's		RETURNCODE Write(CValueVarient& newDataValue);
		
		//used when parsing command responses
// try to use cinteger's RETURNCODE Extract(INT16 *byteCount, BYTE **data);
		//used when constructing a command request  	
// try to use cinteger's RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len); 
	
	//void lowest (CValueVarient& retVal)     ... use integer // smallest default
	//void highest(CValueVarient& retVal)     ... use integer// largest default
	//bool isInRange(CValueVarient val2Check);... use integer //true @ val2Check inside min-max set
};

/******************************************************************************
 * CSigned
 *
 ******************************************************************************
 */
class hCSigned : public hCinteger 
{

public:
	//*%		hCSigned(aCvTinteger* pActual );//,hCVar* pV);
	hCSigned(DevInfcHandle_t h, aCitemBase* paItemBase);//,hCVar* pV);
	hCSigned(hCSigned* pSrc,  hCSigned* pVal, itemIdentity_t newID):
	         hCinteger((hCinteger*)pSrc,pVal,newID){};

// try to use cinteger's	RETURNCODE setDefaultValue(void);
// try to use cinteger's	void lowest (CValueVarient& retVal);
// try to use cinteger's	void highest(CValueVarient& retVal);
// try to use cinteger's	RETURNCODE Read(INT32 &value);		//returns the current value
// try to use cinteger's	RETURNCODE Read(string &value);	
// try to use cinteger's	RETURNCODE Write(INT32 value);		//writes the current value
// try to use cinteger's	RETURNCODE Write(string value);		
// try to use cinteger's	RETURNCODE Write(CValueVarient& newDataValue);
	
	DATA_QUALITY_T Read(INT64 &value)
						{UINT64 i;DATA_QUALITY_T rc = hCinteger::Read(i);value=i;return rc;};

	RETURNCODE Write(const INT64 value){UINT64 u = (UINT64)value; return hCinteger::Write(u);};

	RETURNCODE Write(CValueVarient& newDataValue);

	//used when parsing command responses
// try to use cinteger's RETURNCODE Extract(INT16 *byteCount, BYTE **data); 	
	//used when constructing a command request
// try to use cinteger's RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len); 

	//void lowest (CValueVarient& retVal)     ... use integer // smallest default
	//void highest(CValueVarient& retVal)     ... use integer// largest default
	//bool isInRange(CValueVarient val2Check);... use integer // true @  inside min-max set
};

/******************************************************************************
 * CTimeValue
 *			When Time_Scale is present, edit and display formats are usable.
 *			When Time_Scale is absent, Time_Format will be used.
 ******************************************************************************
 */
class hCTimeValue : public hCUnsigned 
{
	hCattrTimeScale		*pTimeScale;// NULL when none spec'd
	hCattrVarTimeFormat	*pTimeFmt;	// NULL when none spec'd
protected:
	virtual
	int getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
				wstring* pEditFmt = NULL, wstring* pScanFmt = NULL, wstring* pDispFmt = NULL);
public:
	hCTimeValue(DevInfcHandle_t h, aCitemBase* paItemBase );

	hCTimeValue(hCTimeValue* pSrc,  hCTimeValue*  pVal, itemIdentity_t newID);

	virtual ~hCTimeValue();

	virtual RETURNCODE setDefaultValue(void);
/*			   TODO
	virtual void lowest (CValueVarient& retVal);
	virtual void highest(CValueVarient& retVal);
*/
	virtual
		wstring& getDispValueString(void);	// this gives a string for the display numeric!!
	RETURNCODE   getUnitString(wstring & str);// overloads the parent.
	inline
		wstring& getEditValueString(void) ;


	
	virtual		
		RETURNCODE setDisplayValueString(wstring& s);// from UI with range test
	virtual 
		RETURNCODE setDisplayValue(CValueVarient& newValue);// from UI w/ rangetest 

	// calls will fall thru to hCVar's generic version of this call
	//int		getFormatInfo(int& maxChars, int& rightOdec, bool isEdit);// stevev 15jul08 
	// deprecated - do not use in new code, use protected version(not needed this public)
		// J.U. this gives a formated and scaled string for the real value // J.U. 17.02.11 
	virtual                                    //CAUTION-see 'Invensys checkin' in ddbVar.h   
		wstring& getValueFormatedString(bool bIsDispValue);

/*
	virtual
		wstring& getEditValueString(void);// this gives a string for the edditing of numeric!!
	virtual
		wstring& getRealValueString(void);	// used for FILE outputs ONLY
	virtual		
		void setDispValueString(wstring& s);
	virtual		
		void setDispValueString(string& s);
	virtual		
		void setRealValueString(string& s);

// don't 	bool isInRange(CValueVarient val2Check);// true @ val2Check inside min-max set

	RETURNCODE getMinMaxList(hCRangeList& retList);
*/
	bool isInRange(CValueVarient val2Check);
	virtual bool VerifyIt(wstring& s);	// J.U. 17.03.11<<<See note @ bottom ddbVar.h
	double GetScalingFactor();

	bool isTimeScale(){ return (pTimeScale != NULL); };

    wstring& getTimeValue();
};

/******************************************************************************
 * Enumeration class set
 *
 * hCEnum and supporting classes
 ******************************************************************************
 */
//in dllapi.h::>
//typedef struct oStatus_s
//{
//	unsigned short  kind;
//	unsigned short  which;
//	unsigned short  oclass;
//}   outputStatus_t;
//
//typedef vector<outputStatus_t>     outStatusList_t;
//

/*  the actual holder of the enumeration information */
class hCenumDesc : public hCobject
{
	// CcommAPI* m_pComm;
public:

	unsigned long   val;   // numeric value for this enumeration string
	hCddlString		descS; // this enumeration string
	hCddlString     helpS; // help for this enumeration string


	hCbitString		func_class;
	hCreference     actions; // reference to a method spec 500 & IEC spec this as a single method

#ifdef ENUM_ALLOC_DEBUG
	int src;
	int myId;
	int eqFromID;
	int ccFromID;
	itemID_t    ownerID;
	hCenumDesc(DevInfcHandle_t h, int from = -1);

	hCenumDesc( const hCenumDesc& hCed, int from = -5 ) ;

	hCenumDesc& operator=(const hCenumDesc& ib);

	virtual ~hCenumDesc(); 
#else

	hCenumDesc(DevInfcHandle_t h)
		:hCobject(h), descS(h),helpS(h),func_class(h),actions(h)  { clear();};

	hCenumDesc( const hCenumDesc& hCed ) ;
	
	hCenumDesc& operator=(const hCenumDesc& ib);

	virtual ~hCenumDesc()	
	{// remove the virtual from this did not affect the 'pur virtual function call' exception 
		descS.destroy();
		helpS.destroy();
		actions.destroy();
		oclasslist.clear(); 
	}; //VMKP  260304
#endif
	RETURNCODE destroy(void){ 
		descS.destroy();helpS.destroy();actions.destroy(); oclasslist.clear();return SUCCESS;};

	//BIT_ENUM_STATUS?
	unsigned long   status_class;
	outStatusList_t oclasslist;  // oclasses...Empty when kind_oclasses is OC_NORMAL

	void  setEqual(void* pAdesc); 
	void  setDuplicate(hCenumDesc* pED);
	void  clear(void){/*m_pComm=NULL;*/
					  val=0;descS.clear();helpS.clear();func_class.clear();
					  actions.clear();status_class=0;oclasslist.clear();};

//	CcommAPI* theComm(void){return m_pComm;};
//	void theComm(CcommAPI* pC){ m_pComm = pC;};
};

extern vector<hCenumDesc*> gblDescList;	// extern added to remove unresolved external symbol error - L&T

typedef vector<hCenumDesc> enumDescList_t;
typedef hCenumDesc*        ptr2EnumDesc_t;

class hCenumList : public hCpayload, public hCobject, public enumDescList_t
{
public:
	hCenumList(DevInfcHandle_t h) : hCobject(h) {};
	hCenumList(const hCenumList& src):hCobject(src.devHndl()) {
		operator=(src);//stevev 13jun05
	};
	virtual ~hCenumList();
	hCenumList& operator= (const hCenumList& src);
	
	RETURNCODE destroy(void);

	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);
	// some functions to perform on this list
	hCenumDesc* matchString(const wstring& keyStr/*, CcommAPI* pComm = NULL*/);//NULL @ failure
	hCenumDesc* matchValue( const UINT32 value);   // NULL on failure

	void  setEqual(void* pAclass); // set this from the aEnum	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType){ if (ldType != pltEnumList) return false; 
											  else return true;};	
	void  append(hCenumList* pList2Append);
	void  appendUnique(hCenumList* pList2Append);
	// use parent   void  clear(void){ vector<hCenumDesc>.clear();};
	// use parent   int   size(void) { return size();};
	int   maxDescLen(void); // give the longest Description length
	
	UINT32 coerceValue( const UINT32 val);   // NULL on failure


	bool operator== (/*const*/ hCenumList& src);
	RETURNCODE dumpSelf(int indent = 0){ 
		LOGIT(COUT_LOG,"%sEnumList does not dump at this time.\n",Space(indent));return 0;};
};

class hCgroupItemDescriptor;
typedef struct EnumTriad_s
{
	unsigned long   val;
	wstring         descS;
	wstring         helpS;

	struct EnumTriad_s& operator=(const struct EnumTriad_s& s)
		{  val = s.val;descS=s.descS;helpS=s.helpS;return *this;};
	struct EnumTriad_s& operator=( class hCenumDesc& s)
		{  val = s.val;
	       descS=s.descS.procureVal();
		   helpS=s.helpS.procureVal();return *this;};
	struct EnumTriad_s& operator=( class hCgroupItemDescriptor& s);
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	EnumTriad_s(const struct EnumTriad_s& s){ operator=(s); };
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	EnumTriad_s(){val = 0;};// default
	void   clear(){val = 0;descS.clear();helpS.clear();};
}
/*typedef*/ EnumTriad_t;
typedef vector<EnumTriad_t> triadList_t;

/******************************************************************************
 * CEnum
 *
 * this is base class for Enumerated VARIABLEs.  enhances the hCVar type
 * by providing access to the text for the list of enumerations
 ******************************************************************************
 */

//from:
//base:: CvarType
//	unsigned short mysize;>>in item base
//	unsigned short mytype;>>in item base
//	string   sTypeName;   >>from CitemType in item base
//
//class CvarEnumType : public CvarType
//	CconditionalList<CenumList> condEnumDescriptor;//
//
//			class CenumList 
//				vector<CenumDesc>  descList;
//
//				class CenumDesc 
//					payload data (the real values)

class hCEnum: public hCinteger // was: hCVar /* was hCVar:hCitemBase*/
{
protected:
//	UINT32 eValue;  // instance value (from device)

//	UINT32 eWrtVal;

	char fmtStr[32];

	hCcondList<hCenumList> condEnumDescrList;

	hCenumList Resolved;
	wstring    returnString;
public:
//*%	hCEnum(aCvTenumerated* pActual);//,hCVar* pV);
	//CEnum(hCVarType* pVarType,hCVar* pV);
	hCEnum(DevInfcHandle_t h,aCitemBase* paItemBase);
	hCEnum(hCEnum*    pSrc,  hCEnum*    pVal, itemIdentity_t newID);
	virtual ~hCEnum(){ destroy();
		condEnumDescrList.clear();
		Resolved.clear(); 
	};
	RETURNCODE destroy(void)
	{ // clog << "Destroying hCEnum." << endl;		/* infiniteloop:: @  hCVar::destroy()*/
	  Resolved.destroy();return (condEnumDescrList.destroy() ); };

	RETURNCODE setDefaultValue(void);
	void lowest (CValueVarient& retVal);
	void highest(CValueVarient& retVal);

//*%	void copyDesc(aCvarEnumType* pEn);// copy the main data from dllapi to ddbVar
	void copyDesc(aCitemBase* paItemBase);
	// out for now::> void copyAttrDesc(hCVar* pV);


//these are conditional!!! 
//	we gotta do something else!	vector<aCenumDesc*> enums;			// zzzz define EnumItem


//use base	RETURNCODE Read(UINT32 &value);		//returns the current value
	// weird compile problem - couldn't resolve the overloaded read from the UI
	DATA_QUALITY_T Read(UINT64 &value) {return (hCinteger::Read(value));};
	DATA_QUALITY_T Read(wstring &value);	
//use base	RETURNCODE Write(UINT32 value);		//writes the current value
	// weird compile problem - couldn't resolve the overloaded write from the UI
	RETURNCODE Write(UINT32 value) {return (hCinteger::Write(value));};
	RETURNCODE Write(const wstring value);//stevev 23feb10 add const 2 match parent declaration
	
	//used when parsing command responses
// try to use cinteger's	RETURNCODE Extract(INT16 *byteCount, BYTE **data); 
	//used when constructing a command request 	
// try to use cinteger's	RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len);

	RETURNCODE dumpDesc(void);
	
	wstring& getEditValueString(void) 
	{	if ( devPtr()->getPolicy().isReplyOnly )
		{	return (hCinteger::getDispValueString());		}
		else
		{	return (getDispValueString());			}//stevev20sep07-same as display
	};

	// input string, get a value
	virtual RETURNCODE procureValue (const wstring& keyStr, UINT32& value);
	// stevev - WIDE2NARROW char interface
	virtual RETURNCODE procureValue (const string& keyStr, UINT32& value);

	// input value, get a string
	virtual RETURNCODE procureString(const UINT32 value, wstring& mtchStr);
	// stevev - WIDE2NARROW char interface \/ \/ \/
	virtual RETURNCODE procureString(const UINT32 value, string& mtchStr) ;

	// get the resolved list..09mar11-add is test for access from notification(stops deadlock)
	virtual RETURNCODE procureList( hCenumList& returnList, bool isTest = false);  
	virtual RETURNCODE procureList( triadList_t& returnList); // get the resolved list
	virtual RETURNCODE procureEnum(const UINT32 value, hCenumDesc& returnEnum); //  one element


	// // J.U. 17.02.11 
	//CAUTION-see 'Invensys checkin' in ddbVar.h
	wstring& getValueFormatedString(bool bIsDispValue);

	// 27jul10 - An enum is limited to 32 bits by spec (as of today...)
	wstring& getDispValueString(void) 
	{		
		return getValueFormatedString(true); // J.U. 17.02.11
	};

	//UINT32 getDispValue(void){return WrtVal;};
	//UINT32 getRealValue(void){return eValue;   };
	bool isInRange(CValueVarient val2Check);

	/* stevev 3/19/04 - added to support enums as indexes in E&H DDs */
	int getAllValidValues(UIntList_t& returnList);// returns count in list
	
	virtual void fillCondDepends(ddbItemList_t&  iDependOn);
	int notification(hCmsgList& msgList,bool isValidity); // overload numeric
};


/*****************************************************************************
 * CBitEnum
 *
 *****************************************************************************
 */
 /* 17nov11 - the DD working group has decided (and codified in the dd host test) that bitenums
    do NOT have a value to change or read.  They must be trated as a collection of Bits. That
	implies only those bits that are defined in the DD may be seen or modified by the user. 
	functions and data marked '17nov11' below have been added or changed to accomodate this
	interpretation.	stevev
 */
class hCBitEnum : public hCEnum 
{	
protected:
	virtual
	bool   sizeAndScale(CValueVarient& retVal);//false = out-of-range, true=new value in retVal
public:
//*%		hCBitEnum(aCvTbitenumerated* pActual);//,hCVar* pV);
	hCBitEnum(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCBitEnum(hCBitEnum*  pSrc,  hCBitEnum*  pVal, itemIdentity_t newID):
			  hCEnum((hCEnum*)pSrc,pVal,newID)     { };
	void copyAttrDesc(hCVar* pV);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ){returnedSize.clear();
						LOGIT(CERR_LOG,"Bit-Enum setSize is required!\n"); return 1;};


	RETURNCODE setDefaultValue(void);
	// use parent's  void lowest (CValueVarient& retVal);
	// use parent's  void highest(CValueVarient& retVal);
	// use parent's  RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len);
	// use parent's  RETURNCODE Extract(INT16 *byteCount, BYTE **data);
// try to use parents
	RETURNCODE procureString(const UINT32 value, wstring& mtchStr) // input value, get a string
	{	if(value == 0)
		{
			mtchStr = L"";// stevev 3may11 - error::> L"0x00";
			DEBUGLOG(CERR_LOG,"ERROR: bit enum look up with a zero bit mask.\n");
			return FAILURE;
		}
		else // has a value
		{
			return( hCEnum::procureString(value, mtchStr) );
		}
	};
	
	// stevev - WIDE2NARROW char interface \/ \/ \/
	RETURNCODE procureString(const UINT32 value, string& mtchStr) 
	{	if(value == 0)
		{	mtchStr = "0x00";			return SUCCESS;
		}
		else // has a value
		{
			return( hCEnum::procureString(value, mtchStr) );
		}
	};
	// J.U. 17.02.11 
	wstring& getValueFormatedString(bool bIsDispValue);//CAUTION-see'Invensys checkin' ddbVar.h
	wstring& getDispValueString(void) ;
	wstring& getEditValueString(void) 
	{	if ( devPtr()->getPolicy().isReplyOnly )
		{	return (hCinteger::getDispValueString());		}
		else
		{	return (getDispValueString());			}//stevev20sep07-same as display
	};
	bool isInRange(CValueVarient val2Check);

	CValueVarient getRealValue(ulong indexMask);
	//            returns true if bit(s) in index are set
	CValueVarient getDispValue(ulong indexMaskbool, bool iHaveMutex = false);
	//                                                mask tells which bits to change
	void setDispValue(CValueVarient& newValue,  ulong indexMask, bool iHaveMutex = false);
	void setRealValue(CValueVarient& newValue,  ulong indexMask, bool iHaveMutex = false);

	CValueVarient getDispValue(bool iHaveMutex = false)// only valid in simulators
	{	return (hCinteger::getDispValue(iHaveMutex));   }; 
	
	RETURNCODE setDisplayValueString(wstring& s){return FAILURE; };//not supported
	RETURNCODE setDisplayValue(CValueVarient& newValue);//from UI w/ rangetest-17nov11-disabled
	// use parent's  int notification(hCmsgList& msgList,bool isValidity); // overload numeric

	UINT32  getDefinedMask(void);// 17nov11 -returns a value with bits set that are defined in
		// the DD note that this should always be gotten before use because it can change with 
		// conditional enum-item lists
};

class hCindex : public hCinteger
{
	UIntList_t  validValueList;	// list of integers

	wstring     resolved;
public:
	hCindex(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCindex(hCindex*    pSrc,  hCindex*    pVal, itemIdentity_t newID);
	//VMKP 260304
	virtual ~hCindex()/* code moved to .cpp 10apr07 */;
	//VMKP 260304
	RETURNCODE setDefaultValue(void);
	void lowest (CValueVarient& retVal);
	void highest(CValueVarient& retVal);

	void clear(void){hCinteger::clear(); validValueList.clear();resolved=L"";};

	//itemID_t   referencedArray;
	hCattrVarIndex*  pIndexed;
	void addValidValue(UINT32 u){validValueList.push_back(u);};
	// returns count in list (all conditional resolutions
	int getAllValidValues(UIntList_t& returnList);
	/* stevev - added for convienience */
	bool isAvalidValue(UINT64 val);
	
	wstring& getDispValueString(void);	// this gives a string for the display numeric!!
	wstring& getEditValueString(void);
	
	// fall thru to integer...RETURNCODE setDisplayValueString(wstring& s){return FAILURE; };
	RETURNCODE setDisplayValue(CValueVarient& newValue);// from UI w/ rangetest 

	bool isInRange(CValueVarient val2Check);
	
	RETURNCODE notify(hCmsgList& msgList);// see notification() in public area
	void setValidityFromList(bool newV)
	{ 
		setValidity(newV); };
};

/**********************************************************************************************
 * Cascii
 *
 **********************************************************************************************
 */
class hCascii : public hCVar 
{
protected: // we have to share with packed ascii
	UINT32	maxSize;
	string	str;    // device
	string	wstr;	// display
	string	cstr;   // cache

	hCVar*	makePsuedoVar(CValueVarient& initValue);

	virtual
	int	getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
				wstring* pEditFmt = NULL, wstring* pScanFmt = NULL, wstring* pDispFmt = NULL)
	{
		LOGIT(CLOG_LOG,"NOTICE!!!! An ascii variable asked for formatting information.\n");
		maxChars = maxSize;
		rightOdec= -1;  // not there, not 'zero places'  was::> 0;
		return L's';
	};

	public:
//		hCascii(DevInfcHandle_t h) : hCVar(h) { maxSize=0;};
//*%		hCascii(aCvTascii* pActual);//,hCVar* pV);
		hCascii(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCascii(hCascii*    pSrc,  hCascii*    pVal, itemIdentity_t newID);

		RETURNCODE setDefaultValue(void);
		void lowest (CValueVarient& retVal)
		{	string rs("");  retVal = rs;	};	// empty string is lowest
		void highest(CValueVarient& retVal){ string rs(VariableSize(),'~'); retVal = rs;};
		bool isInRange(CValueVarient val2Check)
		{	string L,H,v((string)val2Check); CValueVarient tv;
			hSdispatchPolicy_t policy = devPtr()->getPolicy();
			if (policy.isReplyOnly)
			{
				return true;
			}
			lowest(tv);  L = (string)tv;
			highest(tv); H = (string)tv;
			if ( v >= L && v <= H )
				 return true;
			else return false;
		};
		bool VerifyIt(wstring& s)// J.U. 17.03.11<<<See note @ bottom ddbVar.h
		{
			wstring str = s;
			CValueVarient var;
			var = str;
			return isInRange(var);
		}
			


		hCVar*	  getAttrPtr(unsigned  attr, int which = 0); 

//		void copyAscii(aCvTascii* pA ) { /*empty for now*/};
		void copyAscii(aCitemBase* paItemBase) 
		{	LOGIT(CERR_LOG,"hCascii.copyAscii has been called with no body.\n");};
  		
		virtual BYTE MaxSize(void);
		DATA_QUALITY_T Read(wstring &value);	
		RETURNCODE Write(char* pVal);
		RETURNCODE Write(const wstring value);//stevev 23feb10 add const
		RETURNCODE Write(CValueVarient& newDataValue);

		//used when parsing command responses
		RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
							methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
							varPtrList_t &hasChangedList, extract_t extFlags); 
		//used when constructing a command request 	
		RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len); 
		wstring& getDispValueString(void) 
		{	
			return getValueFormatedString(true);	// J.U. 17.02.11
		};
		wstring& getEditValueString(void) // stevev - 20sep07 - No difference from above
		{	returnString = AStr2TStr(wstr);
			return returnString;    		};
		// J.U. 17.02.11
		// This gives a formated and scaled string for the real value or Display value 
		//CAUTION-see 'Invensys checkin' in ddbVar.h  
		wstring& getValueFormatedString(bool bIsDispValue)
		{
			if (bIsDispValue)
			{
				returnString = AStr2TStr(wstr); 
				
			}
			else
			{
				returnString = AStr2TStr(str);
			}
			return returnString; 
		}
		wstring& getRealValueString(void) // used for FILE outputs ONLY
		{	returnString = AStr2TStr(str); return returnString; 
		};
		//void setDispValueString(wstring& s);
/*		void setDispValueString( string& s);*/

		RETURNCODE setDisplayValue(CValueVarient& newValue);// from UI w/ rangetest 
		RETURNCODE setDisplayValueString(wstring& s);

		void setRealValueString(wstring& s) 
		{
			if ( s.size() >= maxSize )
			{
        /*VMKP Modified on 240204:  All the strings (including Tag, Message are displaying
         one character less */
				s = s.substr(0,maxSize);
			}
			else
			if(s.size() <= 0)
			{
				s = L"";
			}
			//else - nop - use s as is
			str = TStr2AStr(s);
		};	
		void setDispValue(CValueVarient& newValue, bool iHaveMutex = false);
		void setRealValue(CValueVarient& newValue, bool iHaveMutex = false);

		void setRawDispValue(CValueVarient& newValue)
		{
			setDispValue(newValue);
		};

		CValueVarient getRealValue(void)
		{	CValueVarient retVal;
			retVal.vType = CValueVarient::isString;
			retVal.sStringVal = str;
			retVal.vSize = getSize();
			if (IsValidTest())retVal.vIsValid = true;else retVal.vIsValid = false; 
			return retVal;
		};
		CValueVarient getDispValue(bool iHaveMutex = false)
		{	CValueVarient retVal;
			retVal.vType = CValueVarient::isString;
			retVal.sStringVal = wstr;
			retVal.vSize = getSize();
			if(IsValid())retVal.vIsValid = true;else retVal.vIsValid = false;
			return retVal;
		};

//Hope this will never be called... just for symmetry		
		CValueVarient getRawDispValue(void)
		{	
			return getDispValue(); 
		};	
	
		// calls will fall thru to hCVar's generic version of this call
		//int		getFormatInfo(int& maxChars, int& rightOdec, bool isEdit);// stevev 15jul08 
		// deprecated - do not use in new code, use protected version(not needed this public)

		int	getEditFormatInfo(int& maxChars, int& rightOdec, wstring* pScan);

		bool didMethodChange(void); // added 15feb07 stevev 4xmtr
		bool isChanged(void);//{return(wstr != str);};
		bool SetIt(void* pValue)	// returns isChanged
		{ //int y = strlen(pValue);
			wstr = (char*)pValue; // *((char*)pValue); 
			return isChanged();
		};


		bool ApplyIt(void)			// returns wasChanged
		{ bool r = isChanged(); str = wstr; return r;};   
		bool CancelIt(void)				// returns wasChanged
		{ bool r = isChanged(); wstr = str; setWriteStatus(0);/*sjv 25jun07*/return r;};
	void cacheValue(bool generateNotify = false){
		if (generateNotify && cstr != wstr) {notifyIchanged();}
		cstr=wstr;isCached=true;};
	void uncacheVal(void)
	{	if(isCached){ wstr=cstr;if(IsLocal()){ApplyIt();}}
	    isCached=false;};
};

/******************************************************************************
 * CpackedAscii
 *
 ******************************************************************************
 */
class hCpackedAscii : public hCascii 
{
	BYTE * Pack( BYTE *dest, BYTE *source, int nChar );
	BYTE * UnPack( BYTE *dest, BYTE *source, int nChar );

public:
//*%		hCpackedAscii(aCvTpackedascii* pActual);//,hCVar* pV);
	hCpackedAscii(DevInfcHandle_t h, aCitemBase*    paItemBase);
	hCpackedAscii(hCpackedAscii* pSrc,  hCpackedAscii* pVal, itemIdentity_t newID);

	// use parent's	RETURNCODE setDefaultValue(void);
	// packed uses a restricted character set so it has a real highest/lowest
	void lowest (CValueVarient& retVal){ string rs("@"); retVal = rs;};//one of the lowest char
	void highest(CValueVarient& retVal){ string rs(VariableSize(),'?'); retVal = rs;};

	//used when parsing command responses
	RETURNCODE Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
					methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
					varPtrList_t &hasChangedList, extract_t extFlags);  	
	
	//used when constructing a command request
	RETURNCODE Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len);			
	RETURNCODE setDisplayValueString(wstring& s);

};


/******************************************************************************
 * Cpassword
 *		ascii with flag set
 ******************************************************************************
 */
class hCpassword : public hCascii 
{
public:
	hCpassword(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCpassword(hCpassword* pSrc,  hCpassword* pVal, itemIdentity_t newID);

	RETURNCODE  setDefaultValue(void);
	
	bool IsPassword(void) { return true;};
	/* everything else is like ascii */		

	// use parent's	RETURNCODE setDefaultValue(void);
};

/******************************************************************************
 * ChartDate
 *   hybrid of ascii and numeric - stored as numeric
 ******************************************************************************
 */

#define DATE_BYTECNT  3

class hChartDate : public hCinteger //public hCVar //public hCNumeric
{
//	UINT32 dtValue;		// dd  mm  yy since 1900 <universal command protocal spec>

//	UINT32 dtWrtVal;

	typedef enum dtEditFmt_e
	{
		undefined,
		usFormat,   // mm/dd/yy
		invsFormat,	// dd/mm/yy
		dotFormat 	// dd.mm.yy
	} dtEditFmt_t;
	dtEditFmt_t editFormat;

protected:	// internal helper
		int	getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
				wstring* pEditFmt = NULL, wstring* pScanFmt=NULL, wstring* pDispFmt = NULL);
public:
		hChartDate(DevInfcHandle_t h, aCitemBase* paItemBase);//,hCVar* pV);
		hChartDate( hChartDate* pSrc,  hChartDate* pVal, itemIdentity_t newID);
//		hChartDate(DevInfcHandle_t h ) : hCVar(h) {clear();};

		RETURNCODE setDefaultValue(void);
		void lowest (CValueVarient& retVal){ retVal = (unsigned int) 0x010100;};
		void highest(CValueVarient& retVal){ retVal = (unsigned int) 0x1f0cff;};
		bool isInRange(CValueVarient val2Check);
		bool VerifyIt(wstring& s); // J.U. 17.03.11<<<See note @ bottom ddbVar.h	
		void clear(void){hCinteger::clear(); editFormat = undefined;};

//use base		RETURNCODE Read(UINT32 &value);		//returns the current value
		DATA_QUALITY_T Read(wstring &value);	
		// stevev - WIDE2NARROW char interface
		DATA_QUALITY_T Read( string &value);
		
/* VMKP	300104 Added*/
		wstring& getDispValueString(void);
		wstring& getEditValueString(void) 
		{return (getDispValueString());};//stevev20sep07-same as display
		// J.U. this gives a formated and scaled string for the real value or display value
		//CAUTION-see 'Invensys checkin' in ddbVar.h  
		wstring& getValueFormatedString(bool bIsDispValue);//CAUTION-see 'Invensys checkin' in ddbVar.h  

		RETURNCODE getDispDate(UINT32& year, UINT32& month, UINT32& day);
		RETURNCODE setDispDate(UINT32  year, UINT32  month, UINT32  day);

/* VMKP	300104 Added*/
//use base		RETURNCODE Write(UINT32 value);		//writes the current value
		RETURNCODE Write(const wstring value);	// stevev 23feb10 add const 
		void setDispValue(CValueVarient& newValue, bool iHaveMutex = false);
		void setRealValue(CValueVarient& newValue, bool iHaveMutex = false);

		RETURNCODE setDisplayValueString(wstring& s);
		/* Uses the hCinteger version of setDisplayvalue().  If you are using your own date
		conversion software(date to integer), just setting the int makes sense.
		RETURNCODE setDisplayValue(CValueVarient& newValue){return FAILURE;};// not supported
		*/

		void setRawDispValue(CValueVarient& newValue)
		{	
			setDispValue(newValue);
		};
		void setRealValueString(wstring& s); /* added stevev 25apr05 4 symmetry*/

		//used when parsing command responses
//  use cinteger's		RETURNCODE Extract(INT16 *byteCount, BYTE **data);
		//used when constructing a command request  	
//  use cinteger's		RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len); 
		void setDispFormat(dtEditFmt_t newFmt) {editFormat=newFmt;};	
		
		// calls will fall thru to hCVar's generic version of this call
		//int		getFormatInfo(int& maxChars, int& rightOdec, bool isEdit);// stevev 15jul08 
		// deprecated - do not use in new code, use protected version(not needed this public)
	
		virtual
		int	getEditFormatInfo( int& maxChars, int& rightOdec, wstring* pScan = NULL )
		{// assume mm/dd/yyyy  for now (this should never be called)
			maxChars = 10;
			rightOdec= -1; // not there, not 'zero places'  was::> 0;
			if (pScan)  pScan->erase();
			//return L't';
			return L'k';// special encoding added 25mar13
		};

/* VMKP Made it as public on 300104 for data validation check in VariableDlg.cpp */
		UINT32 str2int(wstring value);
/* VMKP Made it as public on 300104 for data validation check in VariableDlg.cpp */
/*stevev 2/12/04 - to get the conversion right...*/
//		void setDispValueString(string& s) ;
//		void setDispValueString(wstring& s) ;
/*stevev 2/12/04*/
/*ANOOP Need to validate the date */
		bool ValidateDate(const UINT32);
/*ANOOP Need to validate the date */
protected:		// helpers

};
#ifdef XMTRDD
#undef XMTR		/* this prevents the compiler from running out of heap space */
#endif

#endif//_DDBVARIABLE_H

/**********************************************************************************************
 **** <<caution<<<See note @ bottom ddbVar.h---here----<<<<< caution
 * VerifyIt() function is the formatting checking part of setDispValueString()
 *    this was added for a partner's UI requirements.
 *    Use this function with caution...you can confuse your users by having some UI verify
 *    but not set the value.
 * setDispValueString is, essentially:
 *    if (VerifyIt(string))
 *    {  CValueVarient var = getValueFromStr(string);
 *       setDispValue(var);
 *       return success;
 *    }
 *    else
 *       return error
 *
 *  I cannot see a valid use case for checking a user input string and NOT putting it into
 *  the display value when it is a valid input.So use this function with caution...
 *  you can confuse your users
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *  Notes:
 *  setDisplayValue() is used to take a value, check its range and ,if inRange, unscale it and
		populate the the display-value in the variable.  if not inRange, will return an error.
 *  setDisplayValueString() is assumed to have a user-entered string in it.  This will parse 
		the string into the proper type (returning an error as required) and call 
		setDisplayValue	to check ranges and populate the display-value if possible.
 *  setDispValue() unscales and sets the display-value as best it can.  It returns no error but
		may	put a value other than the passed in value into the variable (eg a max value). 
 *  setRawDispValue() is used from the mee and is assumed to have unscaled and checked range.
		It places the passed-in value directly into the display-value of the variable.
 *
 *  setDispValueString() is no longer used.  If found, convert the call to one of the above.
 *
 * some errors:	APP_STRING_NOT_ASCII, APP_OUT_OF_RANGE_ERR, APP_PARSE_FAILURE
 *
 ******* new - stevev 11dec08 - as per WAP today
 *	the addition to the Numeric type of 
 *		hClist*             pList4Count;	
 *  has some ramifications.  When the DD writer makes more than one List's COUNT point to the 
 *  same Integer variable, that variable will change when any of the lists change size.  It  
 *  will reflect the size of the last changed List. 
 *  BUT, when the count changes (as in a method), only the list created last in the DD will be 
 *  made stale and re-read to match the count.
 *
 *	A similar situation exsists when there is a list of lists or an array of lists and the 
 *  list's typedef List has its COUNT pointing to an Integer variable.  All lists will point to
 *  that variable but that variable will only point to the last created list.
 *
 *  The bottom line: 
 *	It's stupid to do this.Unless a valid use case can be made to make it work differently, 
 *  the DD writer gets what he gets.
 *
 *  The COUNT variable is allowed to be any numeric BUT, only Integer types will do the LIST 
 *  reread when it changes.  Floats and doubles can't tell when the integer part changes
 *  (changing the list) or when the fractional part changes from some unknown reason.
 *
 ******** end new 11dec08 ****
 *
 **********************************************************************************************
 */
