/*************************************************************************************************
 *
 * $Workfile: ddbCmdDispatch.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the defined interface from the device to the communication port.
 *		It also incorporates some (possibly user unique) methods for items & transactions.
 *
 *		
 * #include "ddbCmdDispatch.h".
 *		
 */

#ifndef _DDBCMDDISPATCH_H
#define _DDBCMDDISPATCH_H

#include "ddbGeneral.h" // includes vector
#include "ddbTracking.h"
//#include "varient.h"
//#include "foundation.h"

#include "ddbDeviceService.h"
#include "ddbMsgQ.h"
#include "ddbCommInfc.h"
/*********1***/

#include "ddbMutex.h"
#include "ddbEvent.h"
#include "ddbCntMutex.h"
#include "ddbGlblSrvInfc.h"
//#include "ddbCommand.h"
#include "ddbPolicy.h"
#include "ddbSvcRcvPkt.h"
#include "registerwaits.h"
#include "ddbTransfer.h"

#include "ddbQueue.h"

#include "logging.h"

/* - moved to HARTsupport.h
#define REASONABLE_TIME 5000 / * 5 seconds * /
#define FOREVER_TIME    (hCevent::FOREVER)
//#define REASONABLE_TIME   FOREVER_TIME
*/

typedef enum dispatchCondition_e // bit-enum so we can add different conditions later
{ 
	dc_Nothing,		/* 0 */
	dc_Reads,   	/* 1 */// Cleared at first weigh with nothing to do, set at something to do
} dispatchCondition_t;

// external references
class hCcommand;
class hCtransaction;
class CCmdList;
class CVarList;

class hCcmdDispatcher	:  public hCsvcRcvPkt, public hCobject	
{// will need access to DeviceListInterface of device (now in device service)
protected:
	// main dispacher - may be subclassed by user and then changed out in the device
	hCmsgCycleQueue		theMsgQueue;
	hCcommInfc*			pCommInterface;  
	hCmutex				hCommMutex;     // linked from application
hCmutex::hCmutexCntrl*	pCommMutex;     // used to prevent the allocation/deallocation of the controls

/* stevev - replace with counting mutex 
	hCmutex				hCriticalSect;
hCmutex::hCmutexCntrl*	pCriticalSect;  // protection for entry cnt lock

	hCevent				hAsyncEnEvent;  // task2task flagging to make atomic active commands
hCevent::hCeventCntrl*	pAsyncEnEvent;
 end stevev replace remove */
/* stevev - replace with counting mutex - add*/
	hCcountingMutex     hCntMutex;

	hCmutex				hIndexMutex;
hCmutex::hCmutexCntrl*	pIndexMutex;  // protection for index variables that are set by many

	int                 indexMutexCount;// stevev 08sep09-implement counting mutex for Send() ONLY

	// async response code handling task
	HANDLE					hRegisteredAsyncTask;
	hCevent					hAsyncRCEvent;  
	hCevent::hCeventCntrl*	pAsyncRespCdEvent;
	
	// background communication handling task
	HANDLE					hRegisteredIdleTask;
	hCevent					hIdleTriggerEvent;  
	hCevent::hCeventCntrl*	pIdleTriggerEvent;
	unsigned int			idleRunCounter;

	
	deque<hMsgCycle_t>		msgDeque;
/* stevev - replace with counting mutex 
	bool            asyncWaiting;	// flag to Sync that Asnc needs to run
	int             entryCnt;		// count lock on Send method
 end stevev replace remove */
	bool					dispatchEnabled;// used during shutdown

	unsigned char		hostCommStatus;//this is being tested for exact values and cannot expand
	dispatchCondition_t	dispatch_Status;
	hSdispatchPolicy_t	ourPolicy;	 // set in constructor or by func 
	CCmdList*			pCmdList; // we use this a lot, keep a copy of the pointer
	CVarList*			pVarList; // 15nov11-varOptimization

	vector<int>        vBlockingCmdStack;

	devMode_t			lastCmpMode;	/*stevev 28jan05 - added to log changes */

	hCTransferChannel*  openPorts[0x100];	/*stevev 27feb09 - unused=null,  */

	hCVar*				pByteCntVar;    // stevev 27aug09 - store this to preclude errors

	hCcriticalSection   receiveCS;      // stevev 08sep09 - we have to keep others from running

protected:	// helper functions (aka common code)
/* stevev - replace with counting mutex 
	RETURNCODE incEntry(void);
	RETURNCODE decEntry(void);
 end stevev replace remove */
	/* stevev 4/23/04 - these got too complex to do inline */
	RETURNCODE enterSend(int cmdNum, bool isBlking);
	RETURNCODE exit_Send(int cmdNum, bool isBlking);

	ulong valueFromIntList(vector<ulong>& intList, ulong target);

	RETURNCODE findBestIdxList(indexUseList_t& returnIndexes,
		          IdxVal2VarList_t& IdxNVarPtrList, bool isRead, unsigned cNum, unsigned tNum);
	bool notInList(itemID_t varID, IdxVal2VarList_t& IdxValVarList);
	// true if varID not in resolved list

public:
	hCcmdDispatcher(hCcommInfc* outComm, DevInfcHandle_t thisHandle );// : hCobject(thisHandle)
	virtual ~hCcmdDispatcher();

	hSdispatchPolicy_t getPolicy(void) { return (ourPolicy);};
	hSdispatchPolicy_t setPolicy(hSdispatchPolicy_t newP);
	/*hSdispatchPolicy_t setPolicy(hSdispatchPolicy_t newP) 
	{ hSdispatchPolicy_t oldP = ourPolicy;  ourPolicy = newP; return (oldP);};*/

	dispatchCondition_t getCond(void);
	void setCond(dispatchCondition_t x);
	void clrCond(dispatchCondition_t y);
	int  setRetries(int newRetries) { if (pCommInterface) 
										return pCommInterface->setNumberOfRetries( newRetries ); 
	else return 0;};

/* stevev - replace 	
	bool isReady(void) { return(pCmdList != NULL && pCommInterface != NULL && pAsyncEnEvent != NULL); };
*/
	/* VMKP added on 150404:  Store the Response code of last command response,
	This is currently being used in WriteImd */
	BYTE	m_brespCdVal;
	bool isReady(void) { return(pCmdList != NULL && pCommInterface != NULL); };
	unsigned char getHostErrorStatus(void) {return hostCommStatus;};

	bool isDownloadMode(void)// device mode includes downloading
	{ return ( devPtr()->getOpMode() & om_DownLoading ) ? true : false;  };

	// stevev 08aug05 - index mutex needed to get an accurate isChanged
#ifdef _DEBUG
	RETURNCODE getIndexMutex(int time, char* fileNm, int LineNum )  
	{   if (pIndexMutex) return(pIndexMutex->aquireMutex(time)); else return FAILURE; };
#else
	RETURNCODE getIndexMutex(int time)  
	{   if (pIndexMutex) return(pIndexMutex->aquireMutex(time)); else return FAILURE; };
#endif
	void       returnIndexMutex()		{   if (pIndexMutex) pIndexMutex->relinquishMutex(); };

	virtual
		void setHandle(DevInfcHandle_t h) {setPtr(h);} ;
	virtual //  blocking  send - read/write and Method entry
		/* Added on 160404 Removed localMsgCycle from the passed parameter list and added a WriteImd flag to
			identify Send called from WriteImd */
		RETURNCODE  
		Send   (hCcommand* pCmd, long transNum, bool actionsEn, blockingType_t isBlking, 
				hCevent* peDone, indexUseList_t& rIdxUse,bool bIsWriteImd = false,
				cmdOriginator_t cmdOriginator = co_UNDEFINED);//Vibhor 250304: Added the last parameter);	
/* stevev - replace with counting mutex 
	virtual //  non blocking send
		RETURNCODE  //++++ Async Cyclic task MUST enter here
		SvcSend(hCcommand* pCmd, long transNum, indexUseList_t& useIdx);//actionsEn,bool isBlking,hCevent* peDone);
 end stevev replace remove */

	virtual //  decide next queue service cmd-used below
		RETURNCODE  
		getNextQueueTransaction(hCcommand* pReturnedCmd, long& returnedTransNum);
	
	virtual
		void   notifyVarUpdate(itemID_t i,  NUA_t isChanged = NO_change, long aTyp = 0L);// just a pass through for now
			//	{	if (pCommInterface) pCommInterface->notifyAppVarChange(i,isChanged, aTyp);};

	virtual 
		bool ServiceWrites(void);    // uses SvcSend    - return true if write was processed 
	virtual 
		bool ServiceReads(void);     // uses SvcSend	- return true if read  was processed

	virtual							 // for VBC hCsvcRcvPkt (passed to commInfc)
		void serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo); 
	virtual
		void servicePacketPair(hPkt* pRqstPkt, hPkt* pRplyPkt) {/* only needs to be in SIMULATOR*/};
	virtual
		void serviceTransferPacket(hPkt* pPkt, hCcommand* pCmd, int transNum);//TRANSPORT LAYER
	virtual
		void serviceMsgCycleQue(int serviceType = 0);// type 0 is clear it

	virtual
		hCTransferChannel* getPort( unsigned char portNum );// gets current or generates one

	virtual 		
		void commAsyncTask(waitStatus_t isTimerExpired);
	virtual 		
		void commIdleTask(waitStatus_t isTimerExpired);
	virtual
		void triggerIdleTask(void); // strobes the idle task execution

	virtual
		RETURNCODE					// must (at least) set the object handle if not zero.
		initDispatch(DevInfcHandle_t thisHandle);
	virtual
		RETURNCODE
		shutdownDispatch(void);// deactivation
	virtual		
		void  disableAppCommRequests(void);	
	virtual		
		void  enableAppCommRequests (void);
	virtual
		bool  appCommEnabled (void);


	virtual		//  used by the items via funcPtr
		RETURNCODE  
		getBestItemTransaction(itemID_t id, hCcommand*& pReturnedCmd, long& returnedTransNum, indexUseList_t& retIdx);

	virtual		//  used by the transactions via funcPtr  - returns weight
		int     calcWeight(hCtransaction* pThisTrans,  indexUseList_t& retIdx, bool isRead);   
//  a new weight calculator to try out
	virtual		//  used by the transactions via funcPtr  - returns weight
		int     calcWeightOptimized(hCtransaction* pThisTrans,  indexUseList_t& retIdx, bool isRead);   
	
	typedef int (hCcmdDispatcher::* fpCalcWtTrans_t)
										(hCtransaction*,  indexUseList_t& useIdx, bool isRead);
#define getMutexTimeOut			(pCommInterface->getMutexTime())
#define getLongMutexTimeOut		(pCommInterface->getLongMutexTime())
#define getMessageTime			(pCommInterface->getMessageTime())
};

inline
void   hCcmdDispatcher::notifyVarUpdate(itemID_t i,  NUA_t isChanged, long aTyp)
{	if (pCommInterface) 
		pCommInterface->notifyAppVarChange(i,isChanged, aTyp);
};
// -- instantiated in ddbDevice.cpp
//global pointer to function initialized with virtual address
//hCcmdDispatcher::fpCalcWtTrans_t fpCalcWgt  = & hCcmdDispatcher::calcWeight;

// usage:
// class myDispatch : public hCcmdDispatcher
// {
//     RETURNCODE  calcWeight(hCtransaction* pThisTrans);
// ...}
//
// myDispatch  aDispatchClass;						instantiate an overridden class
// hCcmdDispatcher* pDispBase = &aDispatchClass;	take the pointer to base class
// rc = (pDispBase->* fpCalcWgt)(pTrans);     will execute the overridden function in myDispatch


#endif//_DDBCMDDISPATCH_H




/*************************************************************************************************
* NOTES:
*       Cyclic                                  Acyclic
*           cs  if Flag								cs flag++ endCS
*                  Set isWaiting
*                  pend on sendmutex
*                  Clr isWaiting					@exit cs flag--
*               else  - Run									 if 0 and isWaiting then set event
**************************************************************************************************
*/
