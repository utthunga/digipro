/*************************************************************************************************
 *
 * $Workfile: ddbFormatHCL.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the default format functionality
 *		10nov04	sjv	started creation
 */


#include "ddbFormatHCL.h"
#include "ddbItemBase.h"
#include "ddbFile.h"
#include "ddbArray.h"
#include "ddbVar.h"
#include "ddbList.h"
#include "ddbPrimatives.h"/*debug*/


const char EOB = 0x1a;// End Of Buffer = cntl-Z


// the only non-item insertion object
RETURNCODE hChclFormat::insertMember(hCformatData* pData, hCgroupItemDescriptor* pgiD)
{
	RETURNCODE rc = SUCCESS;
	string        wrkS;
	pData->Indent();
	// member start
	rc = insertStr(pData,"<member>\n");                        if (rc != SUCCESS) { return rc;}
	pData->Indent();
	// member name ( a number )
	/* stevev 26jul06 change to use member strings in collections
	//rc = insertInt(pData, pgiD->getIIdx(),"memberName");       if (rc != SUCCESS) { return rc;}
	// add the following to use member names when available **/
	wrkS = pgiD->getName();
	if ( wrkS.length() > 0 )
	{
		rc = insertStr(pData, wrkS,"memberName"); 
	}
	else
	{// an array, has no string name
		rc = insertInt(pData, pgiD->getIIdx(),"memberIndex");
	}
	if (rc != SUCCESS) { return rc;}
	// member description

	
//TEMPORARY - convert to ascii chars till we rewrite the formatting
//	wrkS = pgiD->getDesc();
	wstring wS; wS = pgiD->getDesc();
	wrkS = TStr2AStr(wS);

	if ( wrkS.length() > 0 )
	{
		rc = insertStr(pData, wrkS,"memberDescription");       
		if (rc != SUCCESS) { return rc;}
	}
			
	rc = pgiD->getRef().fileWrt(pData);

	pData->Undent();
	rc = insertStr(pData,"</member>\n");                       if (rc != SUCCESS) { return rc;}

//	hCdataElement* pWrkE = new hCdataElement;
//	wrkS = "<member>FORMAT WARNING: members are unimplemented.</member>\n";
//	pWrkE->insert(wrkS.c_str(),wrkS.length());
//	pData->Insert(pWrkE);

	pData->Undent();
	return rc;
}

// everything else
// this will be called by item base if the item type is unsupported (beware of infinite loop)
RETURNCODE hChclFormat::
	insertObject(hCformatData* pData, hCitemBase* pInfo)
{
	RETURNCODE rc = SUCCESS;
	string         wrkS;

	if ( pData == NULL || pInfo == NULL ) 
		return APP_PARAMETER_ERR;

	switch (  pInfo->getIType()  )
	{
	case iT_Member:				//19
		{// non-item type
			wrkS = "Programmer ERROR (use insertMember())";
			rc = insertStr(pData, wrkS, "member"); 
		}
		break;
	case iT_File:				//20
		{	
			hCfile* pF = (hCfile*)pInfo;
			// file start - previous (body) has no newline! -
			rc = insertStr(pData,"\n<DDFile>\n");                if (rc != SUCCESS) { return rc;}
			pData->Indent();
			// symbol name - unavailable at this time
			rc = insertStr(pData, pF->getName(), "SymbolName");  if (rc != SUCCESS) { return rc;}
			// number item id
			rc = insertInt(pData, pF->getID(),"SymbolNumber");   if (rc != SUCCESS) { return rc;}
			
				
			wrkS = "<Members>\n"; 
				rc = insertStr(pData, wrkS);                     if (rc != SUCCESS) { return rc;}
		}
		break;
	case iT_Variable:			// 1
		{
			CValueVarient vV;
			hCVar* pV;

			rc = setItemHdr(pData, pInfo);

			if ( rc == SUCCESS )
			{				
				if ( pInfo->IsValid() && pInfo->IsVariable() )
				{
					char type[80];

					pV = (hCVar*)pInfo;

					sprintf(type,"VarType=\"%s\"",pV->VariableTypeString());
							
					rc = insertInt(pData, pV->VariableType(), "VariableType",10,type); 
						if (rc != SUCCESS) { return rc;}

//TEMPORARY - convert to ascii chars till we rewrite the formatting
//					wrkS = pV->getRealValueString();
					wstring wS; wS = pV->getRealValueString();
					wrkS = TStr2AStr(wS);

					rc = insertStr(pData, wrkS, "Value"); 
						if (rc != SUCCESS) { return rc;}

					sprintf(type,"VarQuality=\"%s\"",dataQualityStrings[pV->getDataQuality()] );
					rc = insertInt(pData, pV->getDataQuality(), "Quality",10,type); 
						if (rc != SUCCESS) { return rc;}
			
					rc = insertStr(pData, "TRUE", "Validity"); 
						if (rc != SUCCESS) { return rc;}
				}
				else
				{					
					rc = insertStr(pData, "FALSE", "Validity");
						if (rc != SUCCESS) { return rc;}
				}
			}
			rc = setItemFtr(pData, pInfo);
		}
		break;
	case iT_Array:				//15
		{// do 'em
			// start
			rc = setItemHdr(pData, pInfo);
			if ( rc == SUCCESS )
			{
				if ( pInfo->IsValid() )//	VALIDITYopt boolean-specifier
				{
					rc = insertStr(pData, "TRUE", "Validity"); 
						if (rc != SUCCESS) { return rc;}
			// call item's insert
					hCarray* pArray = (hCarray*)pInfo;
					rc = insertInt(pData, pArray->getSize(), "ElementCount");

					ddbItemList_t WrkList;	// list of itembase pointers
					rc = pArray->getList(WrkList);
					if ( rc != SUCCESS || WrkList.size() <= 0)
					{
						wrkS = "Empty Array";
						rc = insertStr(pData, wrkS, "element"); 	
						LOGIT(CERR_LOG,"The value Array has no elements.\n");
						return FILE_RV_FAILED;
					}
					else
						rc = insertElements(pData, WrkList);
				}
				else
				{					
					rc = insertStr(pData, "FALSE", "Validity"); 
				}
			}
			// end
			rc = setItemFtr(pData, pInfo);
			//wrkS = "ERROR: unimplemented type(";
			//wrkS += itemStrings[pInfo->getIType()];
			//wrkS += ")";
			//rc = insertStr(pData, wrkS, "item"); 			
		}
		break;
	case iT_List:				//26
		{	
			rc = setItemHdr(pData, pInfo);
			if ( rc == SUCCESS )
			{
				if ( pInfo->IsValid() )//	VALIDITYopt boolean-specifier
				{
					rc = insertStr(pData, "TRUE", "Validity"); 
						if (rc != SUCCESS) { return rc;}
					hClist* pList = (hClist*)pInfo;
					if (! (pList->isCapacityDDdefined() ) )
					{
						rc = insertInt(pData, pList->getCapacity(), "Capacity"); 	
					}
					rc = insertInt(pData, pList->getCount(), "Count"); 

					ddbItemList_t WrkList;// list of itembase pointers
					rc = ((hClist*)pInfo)->getList(WrkList);
					if ( rc != SUCCESS || WrkList.size() <= 0)
					{
						wrkS = "Empty List";
						rc = insertStr(pData, wrkS, "element"); 	
						LOGIT(CERR_LOG,"List in FILE has no members.\n");
						return FILE_RV_FAILED;
					}
					else
						rc = insertElements(pData, WrkList);
				}
				else
				{					
					rc = insertStr(pData, "FALSE", "Validity"); 
				}
			}
			rc = setItemFtr(pData, pInfo);
		}
		break;
	case iT_Collection:			//10
	case iT_ItemArray:			// 9
		{	
			rc = setItemHdr(pData, pInfo);
			if ( rc == SUCCESS )
			{
				if ( pInfo->IsValid() )
				{
					rc = insertStr(pData, "TRUE", "Validity"); 
						if (rc != SUCCESS) { return rc;}
					groupItemPtrList_t WrkList;
					rc = ((hCitemArray*)pInfo)->getList(WrkList);
					if ( rc != SUCCESS || WrkList.size() <= 0)
					{
						wrkS = "Empty Reference Array";
						rc = insertStr(pData, wrkS, "item"); 	
						LOGIT(CERR_LOG,"Item Array has no members.\n");
						return FILE_RV_FAILED;
					}
					rc = insertMembers(pData, WrkList);
				}
				else
				{					
					rc = insertStr(pData, "FALSE", "Validity"); 
				}
			}
			rc = setItemFtr(pData, pInfo);
		}
		break;

	case iT_Record:				//14
	case iT_VariableList:		//16
	case iT_Domain:				//18
		{// unsuported
			wrkS = "ERROR (unsupported type)";
			rc = insertStr(pData, wrkS, "item"); 
		}
		break;

	case iT_Command:			// 2
	case iT_Menu:				// 3
	case iT_EditDisplay:		// 4
	case iT_Method:				// 5
	case iT_Refresh:			/*relation*/	// 6
	case iT_Unit:				/*relation*/	// 7
	case iT_WaO:				// 8
	case iT_Block:				//12
	case iT_Program:			//13
	case iT_ResponseCodes:		//17
	case iT_Chart:				//21
	case iT_Graph:				//22
	case iT_Axis:				//23
	case iT_Waveform:			//24
	case iT_Source:				//25
	case iT_Grid:				//27
	case iT_Image:				//28
		{// illegal
			wrkS = "ERROR: illegal storage type(";
			wrkS += itemStrings[pInfo->getIType()];
			wrkS += ")";
			rc = insertStr(pData, wrkS, "item"); 
		}
		break;

	case iT_ReservedZeta:		// 0
	case iT_ReservedOne:
	case iT_MaxType:			//  27
	default:
		{// unknown
			wrkS = "Programmer ERROR (unknown type)";
			rc = insertStr(pData, wrkS, "item"); 
		}
		break;
	}
	return rc;
}


// terminate an item entry ( may be non functional )
RETURNCODE hChclFormat::
	insertObjectEnd(hCformatData* pData, hCitemBase* pInfo)
{
	RETURNCODE rc = SUCCESS;
	string         wrkS;
	hCdataElement* pWrkE = new hCdataElement;

	if ( pData == NULL || pInfo == NULL || pWrkE == NULL ) 
		return APP_PARAMETER_ERR;

	switch ( pInfo->getIType() )
	{
	case iT_File:				//20
		{	
			hCfile* pF = (hCfile*)pInfo;
			
			rc = insertStr(pData,"</Members>\n");               if (rc != SUCCESS) { return rc;}
			pData->Undent();
			rc = insertStr(pData,"</DDFile>\n");                if (rc != SUCCESS) { return rc;}
			pData->Undent();
	
		}
		break;
	case iT_Variable:			// 1
	case iT_ItemArray:			// 9
	case iT_Collection:			//10
	case iT_Array:				//15
	case iT_List:				//26
		{// do 'nothing for now
		}
		break;

	case iT_Record:				//14
	case iT_VariableList:		//16
	case iT_Domain:				//18
		{// unsupported
			// do 'nothing for now
		}
		break;

	case iT_Member:				//19
	case iT_Command:			// 2
	case iT_Menu:				// 3
	case iT_EditDisplay:		// 4
	case iT_Method:				// 5
	case iT_Refresh:			/*relation*/	// 6
	case iT_Unit:				/*relation*/	// 7
	case iT_WaO:				// 8
	case iT_Block:				//12
	case iT_Program:			//13
	case iT_ResponseCodes:		//17
	case iT_Chart:				//21
	case iT_Graph:				//22
	case iT_Axis:				//23
	case iT_Waveform:			//24
	case iT_Source:				//25
	case iT_Grid:				//27
	case iT_Image:				//28
		{// illegal// do 'nothing for now
		}
		break;

	case iT_ReservedZeta:		// 0
	case iT_ReservedOne:
	case iT_MaxType:			//  27
	default:
		{// unknown// do 'nothing for now
		}
		break;
	}
	if ( pWrkE->length > 0 )
	{
		pData->Insert(pWrkE);
	}
	else
	{
		delete pWrkE;
	}
	return rc;
}


/************************************************************************************************
 * Helper functions   (common code )\
 *
 ***********************************************************************************************/

RETURNCODE hChclFormat::setItemHdr(hCformatData* pData, hCitemBase* pInfo)
{	
	RETURNCODE rc = SUCCESS;
	char wrkC[MAX_ITEM_LEN];

	// item start 
	rc = insertStr(pData,"<item>\n");						      if (rc != SUCCESS){ return rc;}
	pData->Indent();

	// symbol name - unavailable at this time
	rc = insertStr(pData, pInfo->getName(), "SymbolName");        if (rc != SUCCESS){ return rc;}
	// number item id
	rc = insertInt(pData, pInfo->getID(),"SymbolNumber");         if (rc != SUCCESS){ return rc;}
	// number type
	sprintf(wrkC,"itemType=\"%s\"",pInfo->getTypeName());
	rc = insertInt(pData,pInfo->getTypeVal(),"SymbolType",10,wrkC);if(rc != SUCCESS){ return rc;}
	return rc;		
}

RETURNCODE hChclFormat::setItemFtr(hCformatData* pData, hCitemBase* pInfo)
{
	pData->Undent();
	return ( insertStr(pData,"</item>\n") );
}

RETURNCODE hChclFormat::insertMembers(hCformatData* pData, groupItemPtrList_t& r2grpIL)
{
	RETURNCODE rc ;
	if (pData == NULL || r2grpIL.size() <= 0)
	{
		return FAILURE;
	} 
	rc = insertStr(pData, "<Members>\n");                         if (rc != SUCCESS){ return rc;}
	// each member indents/undents self  pData->Indent();

	groupItemPtrIterator_t giIt;
	for (giIt = r2grpIL.begin(); giIt < r2grpIL.end() && rc == SUCCESS; ++ giIt)
	{
		rc = insertMember(pData, (hCgroupItemDescriptor*)(*giIt));
	}

	// each member indents/undents self  pData->Undent();
	rc = insertStr(pData, "</Members>\n");                        if (rc != SUCCESS){ return rc;}
	return rc;
}


RETURNCODE hChclFormat::insertElements(hCformatData* pData, ddbItemList_t& r2diL)
{
	RETURNCODE rc ;
	if (pData == NULL || r2diL.size() <= 0)
	{
		return FAILURE;
	} 
	rc = insertStr(pData, "<Elements>\n");                        if (rc != SUCCESS){ return rc;}
	// each element indents/undents self  pData->Indent();

	ddbItemLst_it diIt;
	for (diIt = r2diL.begin(); diIt < r2diL.end() && rc == SUCCESS; ++ diIt)
	{
		rc = insertElement(pData, (hCitemBase*)(*diIt));
	}

	// each member indents/undents self  pData->Undent();
	rc = insertStr(pData, "</Elements>\n");                       if (rc != SUCCESS){ return rc;}
	return rc;
}

RETURNCODE hChclFormat::insertElement(hCformatData* pData, hCitemBase* pIb)
{
	RETURNCODE rc = SUCCESS;
	string        wrkS;
	pData->Indent();
	// member start
	rc = insertStr(pData,"<element>\n");                       if (rc != SUCCESS) { return rc;}
	pData->Indent();
			
	//rc = pIb->fileWrt(pData);
	rc = insertObject(pData,pIb);

	pData->Undent();
	rc = insertStr(pData,"</element>\n");                       if (rc != SUCCESS) { return rc;}

//	hCdataElement* pWrkE = new hCdataElement;
//	wrkS = "<member>FORMAT WARNING: members are unimplemented.</member>\n";
//	pWrkE->insert(wrkS.c_str(),wrkS.length());
//	pData->Insert(pWrkE);

	pData->Undent();
	return rc;
}
// call with length Zero & NULL ptr to terminate
/***
 *  We will place all of the raw data into a structure..
 *  We will parse into a memory structure till done or </HFDbody> met
 *  We will then fill the data objects
 ***/
RETURNCODE hChclFormat::extractData(unsigned char*& pNxtByte, int& bytesLeft)
{
	RETURNCODE rc = FAILURE;

	if (pNxtByte != NULL && bytesLeft > 0)
	{
		bufferbuff* pBB = new bufferbuff(bytesLeft,pNxtByte);
		if ( pBB == NULL )
		{
			return FAILURE;
		}
		else
		{
			bufferList.push_back(pBB);
			rc = SUCCESS;
			pNxtByte = &(pNxtByte[bytesLeft]);
			bytesLeft = 0;
		}
	}
	else
	if (bytesLeft == 0)
	{// buffer filling over - time to work
		rc = extractValues();// from file text into a tree structure
		if ( rc == SUCCESS && pBaseFile != NULL)
		{
#ifdef XX_DEBUG
			LOGIT(CLOG_LOG,"\n---------------File-----------------\n");
			theFileItem.dump(0);
#endif
//			rc = setValues(&theFileItem);
			rc = setTypedValues(pBaseFile, &theFileItem);
		}
	}

	return rc;
}

/* helper function to deal with a single item, no loops here, may re-enter setTypedValues*/
RETURNCODE hChclFormat::setItemValues(hCitemBase* pTypedItem, int idx, fmtItem* pItm)
{
	RETURNCODE rc = SUCCESS;
	hClist*    pLst = NULL;
	hCarray*   pArr = NULL;
	hCgrpItmBasedClass* pGgiBased = NULL;

	hCitemBase* pIB = NULL;
	CValueVarient vv;
	
	itemList_t  dataMembers;


	int      it = 0;
	itemID_t id = 0;
	string   in;
	string   vS;

	if ( pItm->name != "item" )
	{
		LOGIT(CERR_LOG,"setItem: that is not an item ( a %s)\n",pItm->name);
		return FILE_RV_FAILED;
	}
	itemType_t  passedIT = iT_ReservedZeta;

	if ( pTypedItem != NULL )
	{
		passedIT = pTypedItem->getIType();
		if ( passedIT == iT_Array )
		{	pArr = (hCarray*)pTypedItem;		}
		else
		if ( passedIT == iT_List  )
		{	pLst = (hClist*) pTypedItem;		}
		else
		if ( passedIT == iT_ItemArray || passedIT == iT_Collection ||  passedIT == iT_File )
		{	pGgiBased = (hCgrpItmBasedClass*) pTypedItem;		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,"FILE setItemValues does not support the %s type.\n",
				pTypedItem->getTypeName());
			return APP_PARAMETER_ERR;
		}
	}


	/*	'item' always has:  "SymbolName""SymbolNumber""SymbolType"*/
	it = (int)(pItm->getIntValue("SymbolType"));
	id = (itemID_t) pItm->getIntValue("SymbolNumber");
	in = pItm->getStrValue("SymbolName");
	vS = pItm->getStrValue("Validity");// may not be there


	if ( vS  == "TRUE" && pDEV != NULL )
	{

		if ( pTypedItem != NULL ) // we have a target item
		{
			itemType_t baseType;
			if ( pLst )
			{
				pIB = NULL;
				baseType = pLst->getTypedefType(); //changed stevev 4may07 - was: getIType();
				if ( idx < (int)pLst->getCount()) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
					pIB = (*pLst)[idx];
				else
				if ( pLst->insert(NULL,idx) == SUCCESS )
				{
					pIB = (*pLst)[idx];
				}// else leave it NULL
				if (pIB == NULL)
				{
					LOGIT(CERR_LOG|CLOG_LOG,"FILE's List could not get element %d for fill\n"
						,idx);
				}
			}
			else
			if ( pArr ) 
			{
				baseType = pArr->getTypedefType(); 
				pIB = (*pArr)[idx];
			}
			if ( pGgiBased ) // item-array & collection
			{
				hCgroupItemDescriptor* pGiD = NULL;
				if ( pGgiBased->isInGroup (idx)                   &&
					 pGgiBased->getByIndex(idx, &pGiD) == SUCCESS && 
					 pGiD != NULL                                 &&
					 pGiD->getItemPtr(pIB) == SUCCESS             &&
					 pIB != NULL)
				{
					baseType = pIB->getIType();
					RAZE(pGiD);
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: GroupItem Type did not resolve.\n");
					baseType = iT_ReservedZeta;
					pIB = NULL;	
					RAZE(pGiD);
					return FILE_RV_FAILED;
				}
			}
			//else has already been checked - won't happen here

			if ( it != baseType )
			{	
				LOGIT(CERR_LOG,"FILE's fill type (%s) does not match DD item's typedef(%s)\n"
					,itemStrings[it],itemStrings[baseType]);				
				return FILE_RV_FAILED;
			}
#ifdef _FILE_CLOG
			else
			{
				LOGIT(CLOG_LOG,"    setItem: '%s' #%d is being set\n"
					,pTypedItem->getName().c_str(),idx);
			}
#endif
		}
		else // no typed item here
		{
			if ( ( rc = pDEV->getItemBySymName(in,&pIB)) == SUCCESS && pIB != NULL )
			{
				if ( pTypedItem == NULL  && pIB->getID() != id )
				{// if we have a typed item, these could mismatch
					LOGIT(CLOG_LOG|CERR_LOG,"FILE: '%s' in file has ID 0x%04x but DD binary "
						"has it as 0x%04x\n",in.c_str(),id,pIB->getID());				
					return FILE_RV_FAILED;
				}
				// else drop through to set it
			}
		}

		
	

		if ( it == iT_Variable ) // Variable
		{
			int      vt = (int) pItm->getIntValue("VariableType");
			int      ql = (int) pItm->getIntValue("Quality"); 

			switch( vt )
			{
			case vT_Integer:		// 2
			case vT_Unsigned:		// 3
			case vT_Enumerated:		// 6
			case vT_BitEnumerated:	// 7
			case vT_Index:			// 8
			case vT_BitString:		//12
			case vT_HartDate:		//13 - HART only
			case vT_Time:			//14
			case vT_DateAndTime:		//15
			case vT_TimeValue:		//20
			case vT_Boolean:			//21
				{
					vv = pItm->getIntValue("Value");
				}
				break;
			case vT_FloatgPt:		// 4
			case vT_Double:			// 5
				{
					vv = (double)(pItm->getFltValue("Value"));
				}
				break;
			case vT_Ascii:			// 9
			case vT_PackedAscii:	//10 - HART only
			case vT_Password:		//11
				{
					vv = pItm->getStrValue("Value");
				}
				break;
			case vT_Duration:		//16
			case vT_EUC:				//17
			case vT_OctetString:		//18
			case vT_VisibleString:	//19
			default:
				{
					/* no-op */
				}
				break;
			}

			hCVar* pVar = NULL;
			// replaced if ( pDEV->getItemBySymNumber(id,&pIB) == SUCCESS && pIB != NULL )
			if ( rc == SUCCESS && pIB != NULL )
			{
#ifdef _DEBUG
				if (pIB->getID() != id )
				{
					LOGIT(CERR_LOG|CLOG_LOG,"FILE: '%s' in file has ID 0x%04x but DD binary "
						"has it as 0x%04x\n",in.c_str(),id,pIB->getID());
				}
#ifdef _FILE_CLOG
				else
				{
					LOGIT(CLOG_LOG,"    setItem: Variable: 0x%04x '%s' is being set\n"
						,id,in.c_str());
				}
#endif
#endif
				pVar = (hCVar*) pIB;
				pVar->setRawDispValue(vv);
				pVar->ApplyIt();// display to device value

#ifdef _FILE_CLOG
		LOGIT(CLOG_LOG,"    setItem: *got Variable.\n");
#endif
				if (  ql == 0 || ql == 2 )// HAVE_DATA | STALE_OK
				{
					pVar->setDataQuality(DA_HAVE_DATA);
				}
				else // NOT_VALID | STALE_UNK
				{
					pVar->setDataQuality(DA_NOT_VALID);
				}

				if ( pVar->IsLocal() )
				{
					if (pVar->getDataQuality() == DA_HAVE_DATA)
					{
						pVar->markItemState(IDS_CACHED);// quality have-data
					}
					else //- local, bad quality - leave it at default state
					{
						LOGIT(CLOG_LOG,"    From File: Local & recorded bad - not Set.\n");
					}
				}
				else 
				{
					pVar->markItemState(IDS_STALE);// quality goes to stale-ok | stale-unk
				}
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Recorded item id (0x%04x) failed to "
					"resolve in DD binary.\n",id);
			}
		}
		else
		if ( it == iT_ItemArray ||  it == iT_Collection )   // files not allowed to be nested
		{// get consts symbol name, number              done:: type validity
		//  get item by name, verify id
			// set consts - none to set
			// members  ...  insertMembers via looping this for-loop
			if ( pItm->membList.size() <= 0 )
			{
				LOGIT(CLOG_LOG|CERR_LOG,"Coll/Rarr: '%s' has NO elements.\n",in.c_str());
			}
			else
			{
				fmtItem*   pMem = pItm->getFmtItem ("Members");
				if ( pMem != NULL )
				{
#ifdef _FILE_CLOG
		LOGIT(CLOG_LOG,"    setItem: *got C/A with %d members.\n",pMem->membList.size());
#endif
				// removed stevev 27jul06	rc = setValues( pMem );
					setTypedValues(pIB,pMem); // replaced with 27jul06
				}
				else
					LOGIT(CERR_LOG,"Valid Coll/Rarr has No 'Members' in file.\n");
			}
		}
		else
		if ( it == iT_Array)
		{
			vv = pItm->getIntValue("ElementCount");	
			int c; c = (int)vv;
			hCarray* pA = (hCarray*)pIB;
			if ( c != pA->getSize() )
			{
				LOGIT(CLOG_LOG|CERR_LOG,"Array: '%s' has DD COUNT %d but FILE "
					"has it as %d\n",in.c_str(),pA->getSize(),c);
			}
			//else  elements .. insertElements  via looping this for-loop
			if ( pItm->membList.size() <= 0 )
			{
				LOGIT(CLOG_LOG|CERR_LOG,"Array: '%s' has NO elements.\n",in.c_str());
			}
			else
			{
				fmtItem*   pElem  = pItm->getFmtItem ("Elements");
				if ( pElem != NULL )
				{
#ifdef _FILE_CLOG
		LOGIT(CLOG_LOG,"    setItem: *got array with %d elements.\n",pElem->membList.size());
#endif
					rc = setTypedValues(pA, pElem );
				}
				else
					LOGIT(CERR_LOG,"Valid Array has No 'Elements' in file.\n");
			}
		}
		else
		if ( it == iT_List)
		{	//      elements .. insertElements
			vv = pItm->getIntValue("Capacity");	
			int cap; cap = (int)vv;
			vv = pItm->getIntValue("Count");	
			int c; c = (int)vv;
			hClist* pA = (hClist*)pIB;
			// we gotta figure out how to do this...
			if ( pItm->membList.size() <= 0 )
			{
				LOGIT(CLOG_LOG|CERR_LOG,"List: '%s' has NO members.\n",in.c_str());
			}
			else
			{
				fmtItem*   pElem = pItm->getFmtItem ("Elements");
				if ( pElem != NULL )
				{
#ifdef _FILE_CLOG
		LOGIT(CLOG_LOG,"    setItem: *got list with %d elements.\n",pElem->membList.size());
#endif
					rc = setTypedValues(pA, pElem );
				}
				else
					LOGIT(CERR_LOG|CLOG_LOG,"Valid List has No 'Elements' in file.\n");
			}
		}
		else
		{
			LOGIT(CLOG_LOG,"    Item Type %d is NOT supported at this time.\n",it);
		}
	}
	// else - not valid - it does not exist in the device...

	return rc;
}


RETURNCODE hChclFormat::setValues(fmtItem* pItm)
{
	return setTypedValues(NULL,pItm);
}

// extraction to Device object 
// @ pTypedItem == NULL, value to specified variable
// @ pTypedItem != NULL, value to pTypedItem[number], inserted as required<in sequence>
// recursively re-entered method
RETURNCODE hChclFormat::setTypedValues(hCitemBase* pTypedItem, fmtItem* pItm)
{
	RETURNCODE rc = SUCCESS;
	hClist*    pLst = NULL;
	hCarray*   pArr = NULL;
	hCgrpItmBasedClass* pGgiBased = NULL;

	int      index = 0;

	hCitemBase* pIB = NULL;
	CValueVarient vv;
	
	itemList_t  dataMembers;

	itemType_t  passedIT = iT_ReservedZeta;

	if ( pTypedItem != NULL )
	{
		passedIT = pTypedItem->getIType();
		if ( passedIT == iT_Array )
		{	pArr = (hCarray*)pTypedItem;		}
		else
		if ( passedIT == iT_List  )
		{	pLst = (hClist*) pTypedItem;		}
		else
		if ( passedIT == iT_ItemArray || passedIT == iT_Collection ||  passedIT == iT_File )
		{	pGgiBased = (hCgrpItmBasedClass*) pTypedItem;		}
		else
		{
			LOGIT(CERR_LOG,"FILE setTypedValues does not support the %s type.\n",
				pTypedItem->getTypeName());
			return APP_PARAMETER_ERR;
		}
	}

#ifdef _FILE_CLOG
	LOGIT(CLOG_LOG,"setTyped: Entry Passed In %s with %d in its list.\n",
										pItm->name.c_str(), pItm->membList.size());
#endif
	string ItmName = pItm->name;
	// this hCfile is NOT registered with the device yet, we won't find it by name or number...
	if ( ItmName == "DDFile" )// we're at the highest level...
	{
#ifdef _FILE_CLOG
		if ( pGgiBased == NULL || pGgiBased->getIType() != iT_File )
		{
			LOGIT(CLOG_LOG,"setTyped: got the DDFile with no FILE pointer.\n");
		}
#endif
		fmtItem*   pFmem = pItm->getFmtItem ("Members");
		if ( pFmem != NULL )
		{
#ifdef _FILE_CLOG
			LOGIT(CLOG_LOG,"setTyped: got the DDFile with %d members.\n",pFmem->membList.size());
#endif
			return setTypedValues(pTypedItem, pFmem );
		}
		else
		{
			LOGIT(CERR_LOG,"DD-File type found with no 'Members' in file.\n");
			return FILE_RV_FAILED;
		}
	}
	else
	if( ItmName == "Members" )
	{// list of 'member' s
		pItm->getDataList(dataMembers);// out of the members list
		// fall through to handle list loop
	}
	else
	if ( ItmName == "Elements")
	{// list of 'element's
		pItm->getDataList(dataMembers);// out of the elements list
		// fall through to handle list loop
	}
	else
	if ( ItmName == "item" )
	{
		rc = setItemValues(pTypedItem, 0, pItm);
		return rc;
	}
	else//???
	{
		LOGIT(CERR_LOG|CLOG_LOG,"setTyped: Unhandled entry '%s'.\n",ItmName);
		return FILE_RV_FAILED;
	}


	pIB = NULL;
	for ( itmLstIT_t i = dataMembers.begin(); i < dataMembers.end(); ++i, index++ )
	{// ptr2ptr2 fmtItem

		if ( (*i)->name == "member" )
		{
			// if pTypedItem is null, we have no place to put these 
			// ( they should be going into the file )
#ifdef _DEBUG
			string memDesc; memDesc = (*i)->getStrValue("memberDescription");
#endif
			unsigned long  memIndex = (int) (*i)->getIntValue("memberIndex");
			string memName; memName =       (*i)->getStrValue("memberName");
			hCgroupItemDescriptor* pGID = NULL;

			if (pGgiBased != NULL )
			{
				if ( pGgiBased ->getIType() == iT_ItemArray )
				{
					if (pGgiBased ->getByIndex (memIndex, &pGID, true) != SUCCESS || 
						pGID == NULL )
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Reference-Array %s (0x%04x) did not hold %d index.\n",
										pGgiBased->getName().c_str(),pGgiBased->getID(), memIndex);
						RAZE(pGID);// stevev via HOMZ 21feb07 - maybe alloc'd in getbyindex
						continue;
					}
				}
				else
				if ( pGgiBased ->getIType() == iT_Collection  ||
					 pGgiBased ->getIType() == iT_File        )
				{
					if (pGgiBased ->getByName (memName, &pGID, true) != SUCCESS ||
						pGID == NULL )
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Collection did not hold %%d entry.\n",
													memName.c_str());
						RAZE(pGID);// stevev via HOMZ 21feb07 - alloc'd in getbyindex
						continue;
					}
				}
				else	
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: a %s item was found as a file gid.\n",
												itemStrings[pGgiBased ->getIType()]);
					RAZE(pGID);// stevev via HOMZ 21feb07 - maybe alloc'd in getbyindex
					continue;
				}

				hCitemBase* pLocIB = NULL;
				hCreference* pRef  = NULL;
				pRef= pGID->getpRef();

				if ( pRef != NULL && pRef->resolveID(pLocIB, false) == SUCCESS && pLocIB != NULL )
				{
					pTypedItem = pGgiBased; // were to put it by index;
					index = pGID -> getIIdx();
				}					
			}

			fmtItem*   pItem = (*i)->getFmtItem ("item");
			if (pItem != NULL)
			{
#ifdef _FILE_CLOG
			LOGIT(CLOG_LOG,"setTyped: got item with %d entries.\n",pItem->membList.size());
			//	always has:  "SymbolName""SymbolNumber""SymbolType"
			if (memIndex != index)
			{
			LOGIT(CLOG_LOG,"setTyped: got file Index %d '%s' for %dth entry.\n",
				memIndex, memName.c_str(), index);
			}
#endif
				rc = setItemValues(pTypedItem, index, pItem );
			}
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,"member in file without an 'item' body.\n");
				rc = FILE_RV_FAILED;
			}
		}
		else
		if ( (*i)->name == "element")
		{// should only have items
			
			hCgroupItemDescriptor* pGID = NULL;
			if (pGgiBased != NULL &&
				pGgiBased ->getByIndex (index, &pGID, true) == SUCCESS && 
				pGID != NULL )
			{
				hCitemBase* pLocIB = NULL;
				hCreference*  pRef = NULL;
				pRef= pGID->getpRef();

				if ( pRef->resolveID(pLocIB, false) == SUCCESS && pLocIB != NULL )
				{
					pTypedItem = pLocIB;
				}					
			}

			fmtItem*   pItem = (*i)->getFmtItem ("item");
			if (pItem != NULL)
			{
#ifdef _FILE_CLOG
			LOGIT(CLOG_LOG,"setTyped: got item with %d entries.\n",pItem->membList.size());
			//	always has:  "SymbolName""SymbolNumber""SymbolType"
#endif

				rc = setItemValues(pTypedItem, index, pItem );
			}
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,"element in file without an 'item' body.\n");
				rc = FILE_RV_FAILED;
			}
			RAZE(pGID);// stevev via HOMZ 21feb07 - alloc'd in getbyindex
		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,"setTyped:item in memberlist with a '%s' item (%s).\n"
				,(*i)->name,(*i)->getStrValue("SymbolName").c_str());
		}

	}// next settable item

	return rc;
}

#define GETC bufferList.getChar
#define MAXNAMELEN  0x100
#define MAXELEMLEN  0x400

// buffer to extraction
RETURNCODE hChclFormat::extractValues(void)
{
	RETURNCODE rc = SUCCESS;
	char c;

	// run up to start of element
	while ( (c = GETC()) != '<' );
	rc = extractAnItem(&theFileItem);
	return rc;
}

//assumes buffer is on the first char of the name ( just after '<')
RETURNCODE hChclFormat::extractAnItem(fmtItem* pItm)
{
	RETURNCODE rc = SUCCESS;
	char txt[MAXELEMLEN];
	char nam[MAXNAMELEN];
	char c;

	if ( pItm == NULL )
	{
		return APP_PARAMETER_ERR;
	}

	// Note that comments are NOT allowed

	// get the name
	if ( (rc = bufferList.fillStr(txt, MAXELEMLEN, "\r\n\t >=</\0")) == SUCCESS )
	{
		pItm->name = txt;
	}
	else // eob | maxbuf
	{
		return rc;
	}
	c = GETC();
//LOGIT(CLOG_LOG,"extractAnItem:Entry char = '%c'----\n",c);
	while (  c != EOB && c != '>' )
	{// get attribute(s)
		while (c != EOB && strchr("\r\n\t >", c) != NULL ){c = GETC();} // skip leadup to name
		bufferList.putBack(c); // the non matching char
		if ( (rc = bufferList.fillStr(nam, MAXNAMELEN, "\r\n\t >=</\0")) == SUCCESS )
		{
			baseItem_t* pAttr = new baseItem_t;
			pAttr->name = nam;
			c = GETC();
			while ( c != '\"' ){ c = GETC();} // skip leadup to value
			if ( (rc = bufferList.fillStr(nam, MAXNAMELEN, "\"")) == SUCCESS )
			{
				pAttr->value = nam;
				pItm->attrList.push_back(pAttr);
//LOGIT(CLOG_LOG,"extractAnItem: Insert Attribute Value '%s'\n",nam);
				c = GETC();
				if ( c == '\"' ) c = GETC();// it should be..get one after value quote
				continue; // while not '>' end of element descr
			}
			else
			{ // eob | maxbuf
				while ( c != EOB && c != '>' ){ c = GETC();} // error scan to end of name
			}
		}
		else
		{// eob | maxbuf
		
			while (  c != EOB && c != '>' ){ c = GETC();} // error scan to end of name
		}	
	}// wend EOB | '>'
	if ( c == '>' )
	{//get values or members
//LOGIT(CLOG_LOG,"extractAnItem:Post Attribute Entry\n",c);
		bool getMore = true;
		while(getMore)
		{
			if ( (rc = bufferList.fillStr(txt, MAXELEMLEN, "<\0")) == SUCCESS )
			{// txt is a) run-up to member, or b) Value
				c = GETC();
				if ( c == '<' )
				{
					if (bufferList.peekChar() == '/')
					{// b) value
						pItm->value = txt;
						GETC(); // discard '/'
						rc = bufferList.fillStr(txt, MAXELEMLEN, ">\0");
						if ( txt != pItm->name )
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: item '%s' doesn't match terminator '%s'\n",
								pItm->name.c_str(), txt);
						}
						getMore = false;
					}
					else
					{// a) run-up 2 member
						// new member
						fmtItem* pNewItm = new fmtItem;
						// fill member
						if ( rc = extractAnItem(pNewItm) == SUCCESS )
						{
							pItm->membList.push_back(pNewItm);
							// loop for more members
//LOGIT(CLOG_LOG,"extractAnItem:Insert a New Member to memberList\n",c);
						}
						// else??
						// loop for another member
					}
				}
				else
				if ( c == EOB )
				{
					getMore = false;
					rc = FILE_RV_EOF;
				}
				else
				{
				}
			}
			else
			{// eob | maxbuf
				c = GETC();
				while (  c != EOB && c != '>' ){ c = GETC();} // error scan to end of name
				if ( c == EOB ) 
				{
					getMore = false;
					rc = FILE_RV_EOF;
				}
			}
		}// wend getMore
	}
	else //must be eob - i hope we're done
	{
		rc = FILE_RV_EOF;
	}
//LOGIT(CLOG_LOG,"extractAnItem:Return--------------\n",c);
	
	return rc;
}

/************************************************************************************************
 *
 */

// char pbC;
// bool isPB;

char bufLst::getChar(void)
{
	char r = EOB;
	bufferbuff* pK = NULL;

	if (isPB)
	{
		isPB=false;
		return pbC;
	}
	if ( activeIndex < 0 && size() > 0)
	{
		activeIndex     = 0;
		pK = at(0);
		pK->pNxtChar = pK->pBuffer;
		pK->left     = pK->len;
	}
	if (activeIndex < (int)size())  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{	
		pK = at(activeIndex);
		while( 1 )
		{
			if ( pK->left > 0 )
			{
				r = *(pK->pNxtChar);
				(pK->pNxtChar)++;
				pK->left--;
				break;// out of while
			}
			else
			{
				activeIndex++;
				if (activeIndex >= (int)size() ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
				{
					r = EOB;
					break;
				}
				else
				{
					pK = at(activeIndex);
					pK->pNxtChar = pK->pBuffer;
					pK->left     = pK->len;
				}// loop
			}
		}// wend
	}// else - keep returning EOB
	return r;
}

char bufLst::peekChar(uchar offset)
{
	char r = EOB;
	if (offset == 0 && isPB)
	{
		return pbC;
	}
	if ( activeIndex < 0 && size() > 0)
	{
		activeIndex     = 0;
		at(0)->pNxtChar = at(0)->pBuffer;
		at(0)->left     = at(0)->len;
	}
	int idxOffset = 0;
	uchar* pC;
	int   lft;

	while ( (activeIndex+idxOffset) < (int)size() && r == EOB ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{	
		pK = at(activeIndex+idxOffset);

		if ( idxOffset == 0 ) // use the current counter
		{
			pC  = pK->pNxtChar;
			lft = pK->left;
		}
		else // use the base counter
		{
			pC  = pK->pBuffer;
			lft = pK->len;
		}
					
		if ( lft > offset )
		{
			r = pC[offset];
		}
		else //- rolls into next buffer
		{
			if ( (activeIndex+idxOffset+1) < (int)size() )  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
			{
				idxOffset++;
				offset -= lft;
				// loop
			}
			else
			{
				r = EOB;
				break;
			}
		}

	}// wend - nxt buffer
	return r;
}

void bufLst::putBack(char c)
{
#ifdef _DEBUG
	if ( isPB )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: More than one char putback to buffer.\n");
	}
#endif
	
	isPB=true;
	pbC = c;
}


RETURNCODE bufLst::fillStr(char* ret, int bufMaxSz, char* toOneOfThese)
{
	if ( ret == NULL || bufMaxSz <= 0 || toOneOfThese == NULL )
	{
		return FAILURE;
	}

	int  cnt = 0;
	char* pR = NULL;
	char   p = peekChar();
	char   c = getChar();

	while( c != EOB && cnt < bufMaxSz && pR == NULL )
	{
		/*** Comments are NOT allowed in this file ***/
		pR = strchr( toOneOfThese, c );
		if (pR==NULL)// no match
		{
			ret[cnt++] = c;
			c = getChar();
		}
		else // match
		{
			putBack(c);
		}
	}
	ret[cnt] = '\0';
	if ( c == EOB || cnt >= bufMaxSz)
	{
		return FAILURE;
	}
	else
	{
		return SUCCESS;
	}
}

void fmtItem::clear(void)
{ 
	for ( baseItmLstIT_t i = attrList.begin(); i < attrList.end(); ++i)
	{
		delete (*i);
	}
	attrList.clear();
	for ( itmLstIT_t x = membList.begin(); x < membList.end(); ++x)
	{
		delete (*x);
	}	
	membList.clear();
}

void fmtItem::dump(int indent)
{ 
	string sp(indent,' ');
	LOGIT(CLOG_LOG,"%s%s    ",sp.c_str(),name.c_str());
	if ( attrList.size() > 0)
	{
		for ( baseItmLstIT_t i = attrList.begin(); i < attrList.end(); ++i)
		{			
			LOGIT(CLOG_LOG,"Attr:%s =|%s|   ",(*i)->name.c_str(),(*i)->value.c_str());
		}
	}
	LOGIT(CLOG_LOG,"\n");
	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{			
			(*y)->dump(indent+2);
		}
	}
	else
	{
		LOGIT(CLOG_LOG,"%sValue = |%s|\n",sp.c_str(),value.c_str());
	}
}

#if 1
int fmtItem::getDataList(itemList_t& returnList)
{
	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			returnList.push_back(*y); 			
		}
	}
	// else - we ain't one of the chosen...
	return returnList.size();
}
#else
// how it was before 19jul06
int fmtItem::getDataList(itemList_t& returnList)
{
	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			if ( (*y)->name == "SymbolType" )
			{// our symbol type
				if ((*y)->value == "1"  || /* Variable */
					(*y)->value == "15" || /* Array */
					(*y)->value == "26"  ) /* List */
				{
					returnList.push_back(this); 
				}
			}

			(*y)->getDataList(returnList);
		}
	}
	// else - we ain't one of the chosen...
	return returnList.size();
}
#endif

// converted to int 64 03jun11
UINT64  fmtItem::getIntValue(string s)
{
	UINT64 uLR = 0L;

	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			if ( (*y)->name == s )
			{// our symbol type
				// sjv 26jul06 uLR = atol((*y)->value.c_str());
				uLR = _strtoui64((*y)->value.c_str(),NULL,10);
				break; // out of for loop
			}
		}
	}

	return uLR;
}

double         fmtItem::getFltValue(string s)
{
	double R = 0.0;
	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			if ( (*y)->name == s )
			{// our symbol type
				R = atof((*y)->value.c_str());
				break; // out of for loop
			}
		}
	}

	return R;
}

string         fmtItem::getStrValue(string s)
{
	string sR;

	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			if ( (*y)->name == s )
			{// our symbol type
				sR = (*y)->value.c_str();
				break; // out of for loop
			}
		}
	}

	return sR;
}


fmtItem*       fmtItem::getFmtItem (string s)
{
	fmtItem* pR = NULL;// returns this if not found

	if ( membList.size() > 0)
	{
		for ( itmLstIT_t y = membList.begin(); y < membList.end(); ++y)
		{	
			if ( (*y)->name == s )
			{// our symbol type
				pR = (*y);
				break; // out of for loop
			}
		}
	}

	return pR;
}