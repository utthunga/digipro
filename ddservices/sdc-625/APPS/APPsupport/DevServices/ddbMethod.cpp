/*************************************************************************************************
 *
 * $Workfile: ddbMethod.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		09/09/03	sjv	started creation
 */
#include "ddbMethod.h"
#ifndef __GNUC__	// L&T Modifications : MethodSupport
#include "ddbMethSupportInfc.h"
#else
#include "ddbMethSupportInfc.h"
#endif
#include "logging.h"

//hCmethod::hCmethod(DevInfcHandle_t h, ITEM_TYPE item_type, itemID_t item_id, unsigned long DDkey)
//: hCitemBase(h,item_type,item_id,DDkey)
//{
//	pClass = NULL;
//	pDef   = NULL;
//	pScope = NULL;
//}


hCmethod::hCmethod(DevInfcHandle_t h, aCitemBase* paItemBase) : hCitemBase(h,paItemBase)
{
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(methodAttrClass); 
	if (aB == NULL)
	{	pClass = NULL; } 
	else // generate a hart class from the abstract class
	{ 
		pClass = new hCattrMethodClass  (h, (aCattrBitstring*)aB) ; 
		if (pClass)
			pClass->setItemPtr(this); 
	}
	aB = paItemBase->getaAttr(methodAttrDefinition); 
	if (aB == NULL)
	{	pDef = NULL; } 
	else  // generate a hart class from the abstract class
	{ 
		pDef = new hCattrMethodDef  (h,  (aCattrMethodDef*)aB); 
		if (pDef)
			pDef-> setItemPtr(this); 
	}	
	aB = paItemBase->getaAttr(methodAttrScope) ; 
	if (aB == NULL)
	{	pScope  = NULL; }
	else  // generate a hart class from the abstract class
	{ 
		pScope  = new hCattrMethodScope   (h,  (aCattrBitstring*)aB);
		if (pScope)
			pScope-> setItemPtr(this); 
	}
	
	aB = paItemBase->getaAttr(methodAttrType) ; 
	if (aB == NULL)
	{	pType  = NULL; }
	else  // generate a hart class from the abstract class
	{ 
		pType  = new hCattrMethodType   (h,  (aCparameter*)aB);
		if (pType)
			pType-> setItemPtr(this); 
	}
		
	aB = paItemBase->getaAttr(methodAttrParams) ; 
	if (aB == NULL)
	{	pParams  = NULL; }
	else  // generate a hart class from the abstract class
	{ 
		pParams  = new hCattrMethodParams   (h,  (aCparameterList*)aB);
		if (pParams)
			pParams-> setItemPtr(this); 
	}
}

/* from-typedef constructor */
hCmethod::hCmethod(hCmethod*    pSrc,  itemIdentity_t newID) : hCitemBase(pSrc,newID)
{
	cerr<<"****Implement method typedef constructor!****"<<endl;
}


hCmethod::~hCmethod()
{
	if (pDef)
		delete pDef;
	pDef = NULL;
	RAZE(pClass);
	RAZE(pScope);
	// HOMZ & stevev -21feb07-forgot to destruct these two new guys
	RAZE(pType);
	RAZE(pParams);
}


// required virtual
hCattrBase*   hCmethod::newHCattr(aCattrBase* pACattr)  // builder 
{	
	cerr << "ERROR: method item accessed unimplemented 'newHCattr'."<<endl;
	return NULL; /* an error */  
}

RETURNCODE hCmethod::selfExecute(hCmethodCall& MethodCall,CValueVarient& rV)
{
	//hCmethodCall MethodCall;
	//MethodCall.source   = msrc_EXTERN;// ALL non-action invocations should come through here.
//	MethodCall.methodID = getID();
	//MethodCall.paramList.clear();

	RETURNCODE nRet = devPtr()->executeMethod(MethodCall,rV);

	return nRet;
}

char* hCmethod::getDef(int& l)
{
	if ( pDef )
	{
		l = pDef->getDefLen();
		return (pDef->getMethodDefinition());
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: call to a method with no definition.");
		l = 0;
		return NULL;
	}
}


RETURNCODE hCmethod::dumpSelf(int indent, char* typeName) 
{ 
	hCitemBase::dumpSelf(); 
	//cout << "METHOD" << endl; return SUCCESS;
	LOGIT(CLOG_LOG,"METHOD: \n");
	return 0;
}


//RETURNCODE hCmethod::Label(wstring& retStr) 
//{ 
//	return baseLabel(METHOD_LABEL, retStr); 
//}

RETURNCODE hCmethod::GetLastMethodsReturnCode(void) // this returns the error code returnrd by the method
{
	RETURNCODE rc = devPtr()->GetLastMethodsReturnCode();
	return rc;
}


RETURNCODE hCmethod::getMethodType(hCmethodParam& mP)
{
	hCmethodParam lP, *pLp;

	if ( pType == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method has no type Attribute.\n");
		mP = lP;
		return APP_PROGRAMMER_ERROR;
	}
	pLp = (hCmethodParam*) pType->procure();
	if ( pLp == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method did not resolve Type attribute.\n");
		mP = lP;
		return APP_PROGRAMMER_ERROR;
	}
	mP = *pLp;
	return SUCCESS;
}

RETURNCODE hCmethod::getMethodParams(ParamList_t& pL)
{
	pL.clear();
	if ( pParams == NULL )
	{
		return SUCCESS; // a normal conclusion
	}

	ParamList_t * pPL = (ParamList_t*) pParams->procure();
	
	FOR_p_iT(ParamList_t, pPL)
	{	
		pL.push_back(*iT);
	}
	if ( pL.size() == pPL->size() )
		return SUCCESS;
	else
		return APP_ERROR_BASE;// general error
}


CValueVarient hCmethod::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;

	methodAttrType_t mat = (methodAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( mat >= methodAttr_Unknown )
	{
		LOGIT(CERR_LOG,
					"ERROR: Methods's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	switch (mat)
	{
	case methodAttrLabel:
	case methodAttrHelp:
	case methodAttrValidity:
		rV = hCitemBase::getAttrValue(mat);		
		break;
	case methodAttrClass:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method class information is not accessable.\n");
		}
		break;
	case methodAttrDefinition:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method definition information is not accessable.\n");
		}
		break;
	case methodAttrScope:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method scope information is not accessable.\n");
		}
		break;
	case methodAttrType:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method type information is not accessable.\n");
		}
		break;
	case methodAttrParams:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: method parameter information is not accessable.\n");
		}
		break;
	case methodAttrDebugInfo:
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}//endswitch
	return rV;
}

bool hCmethod::isBackGround(void)
{
	bool isB = false;

	if (pClass != NULL)
	{
		hCbitString* pbs = (hCbitString*)pClass->procure();
		if ( pbs != NULL )
		{
			if ( ( pbs->getBitStr() & (1<<(classBackground-1))) )
				isB = true;
		}
		// else return false
	}// else return false

	return isB;
};
