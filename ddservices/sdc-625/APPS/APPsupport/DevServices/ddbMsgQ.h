/*************************************************************************************************
 *
 * $Workfile: ddbMsgQ.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *	
 * #include "ddbMsgQ.h"
 */

#ifndef _DDBMSGQUEUE_H
#define _DDBMSGQUEUE_H

#ifdef _WIN32_WCE			// PAW 24/04/09
#include "time_CE.h"	// PAW 24/04/09
#else
#include <sys/timeb.h>
#endif

#include "ddbGeneral.h" // includes vector
#include "ddbQueue.h"
#include "ddbResponseCode.h"
#include "ddbTracking.h"
#include "ddbDeviceService.h"

#include "HARTsupport.h"

#ifdef _DEBUG /* allocation debugging */
	// to get GetCurrentThreadId();
#include "xplatform_thread.h"
extern bool enableAssert;
#endif

#ifdef DO_NOT_DEBUG
#define _DEBUG_MEM 1
#define CTRLSIZE   0x20
#endif
// external references
class hCcommand;
class hPkt;

/* moved to tracking */
/*typedef*/// hIndexUse_t;
//typedef vector<hIndexUse_t> indexUseList_t;

/* notes on blocking...
 * The receive packet handler triggers the Send routine to continue. The Send routine sees that
 * it is blocking and returns the msg cycle which clears the isBlocking to Zero.
 * When the receive packet handler runs again, it tests the isBlocking parameter which is now false
 * and returns it...again which fouls up the queue.
 */
typedef enum blockingType_e
{
	bt_UNDEFINED,
	bt_ISBLOCKING,
	bt_NOTBLOCKING
}
/*typedef*/ blockingType_t;

typedef vector<hCVar*> varPtrList_t;
typedef vector<unsigned char> ucharList_t;

// definitions
typedef struct hMsgCycle_s
{
#ifdef _DEBUG_MEM /* debugging delete crash */
	THREAD_ID_TYPE threadID;// make this the top item so it equals 'this'
	unsigned char cntrlArea[CTRLSIZE];
#endif
	hCcommand*			pCmd;
	long				transNum;
	cmdOperationType_t	cmdType;
	unsigned char       chkSum;
	/* 10/2/03 - add RC && DS here so we can handle it all at once */
	unsigned char       respCdVal;
	unsigned char		devStatVal;
	/* 10/17/03- add the host status for future expansion of detail */
	unsigned char		hostCommStatus;
	bool				actionsEn;
	blockingType_t		isBlking;
	hCevent*			peDone;
#ifndef _WIN32_WCE			// PAW 24/04/09
	struct _timeb       sendTimeBuffer;
	struct _timeb       recvTimeBuffer;
#else
	time_t_ce			sendTimeBuffer;
	time_t_ce			sendMSBuffer;
	time_t_ce			recvTimeBuffer;
	time_t_ce			recvMSBuffer;
#endif
#ifndef _WIN32_WCE	// think these are causing a memory corruption PAW 14/08/09
	time_t				sendTime;//use SendTime = time(NULL);// see localtime() for conversions
	time_t				recvTime;
#endif
	indexUseList_t		indexList;	
	hCRespCodeList		respCds;// treat as a vector of <hCrespCode>
	methodCallList_t	pre_Actions;
	methodCallList_t    postActions;
	varPtrList_t        transVarList;// stevev 2/26/04 - list of vars actually used
	ucharList_t			transSignature;//stevev 10/13/04 - constants at the beginning of the packet
	__int64             transactionID; // stevev 082809 - a applicationID|time tick for unique ID

	cmdOriginator_t       m_cmdOrginator;
#if !defined(__GNUC__)
	struct 
#endif // __GNUC__
	hMsgCycle_s() : respCds(0) {clear();
#ifdef _DEBUG_MEM /* debugging delete crash */
	threadID = Thread_GetCurrentThreadId();
	unsigned char* p = (unsigned char*)this;// (&threadID);
	p -= CTRLSIZE;
	memcpy(cntrlArea,p,CTRLSIZE);
#endif
	};
#if !defined(__GNUC__)
	struct 
#endif // __GNUC__
	hMsgCycle_s(const struct hMsgCycle_s& src) : respCds(0),m_cmdOrginator(co_UNDEFINED) {operator=(src);
#ifdef _DEBUG_MEM /* debugging delete crash */
	threadID = Thread_GetCurrentThreadId();
	unsigned char* p = (unsigned char*)this;// (&threadID);
	p -= CTRLSIZE;
	memcpy(cntrlArea,p,CTRLSIZE);
#endif
	};
	// paw - changed to below   virtual  ~hMsgCycle_s() {respCds.destroy();/*postActions.destroy();*/};
	
	virtual  ~hMsgCycle_s() // split out for debugging PAW 12/08/09
	{
		respCds.destroy();
		/*postActions.destroy();*/

#ifdef _DEBUG_MEM /* debugging delete crash */
	 THREAD_ID_TYPE end_threadID = Thread_GetCurrentThreadId();
	 if ( enableAssert)
	 assert(end_threadID == threadID);
#endif
	};

	void clear(void)
		{pCmd=NULL;peDone=NULL;transNum=-1;actionsEn=false;isBlking=bt_UNDEFINED;
		 recvTime=sendTime=0;chkSum=0;respCds.clear();postActions.clear();cmdType=cmdOpNone;
		 indexList.clear();respCdVal=devStatVal=0;transVarList.clear();pre_Actions.clear();
		 transSignature.clear();m_cmdOrginator=co_UNDEFINED;};

	struct hMsgCycle_s& operator=(const struct hMsgCycle_s& src)
		{pCmd=src.pCmd; peDone=src.peDone;transNum=src.transNum;actionsEn=src.actionsEn;
		 chkSum=src.chkSum;isBlking=src.isBlking;sendTime=src.sendTime;recvTime=src.recvTime;
		 indexList=src.indexList;respCds=src.respCds;postActions=src.postActions;
		 pre_Actions=src.pre_Actions;cmdType=src.cmdType;respCdVal=src.respCdVal;
		 devStatVal=src.devStatVal;transVarList=src.transVarList; 
		 transSignature=src.transSignature; m_cmdOrginator=src.m_cmdOrginator;return (*this);};
		 
	bool signatureMatch(hPkt* pPkt);/*
	{	if (pPkt == NULL || pCmd == NULL)		
		{	return false;	}
		if ( transSignature.size() <= 0 || transSignature.size() > (pPkt->dataCnt-2))	
		{	return (pCmd->getCmdNumber() == pPkt->cmdNum); }
		if (pCmd->getCmdNumber() == cmdNum) 
		{	int i = 2;
			for (ucharList_t::iterator  iT = transSignature.begin(); 
										iT < transSignature.end(); iT++, i++)
			{	if ( pPkt->theData[i] != *iT )
				{
					return false;
				}
			}
			return true;// they all match
		}
		else { return false;}
	};*/
}
/*typedef*/ hMsgCycle_t;

class hCmsgCycleQueue : public hCqueue<hMsgCycle_t>	
{  
public://constructor
	//09jan12 - hCmsgCycleQueue(int gb = 1):hCqueue<hMsgCycle_t>(gb) {};
	hCmsgCycleQueue():hCqueue<hMsgCycle_t>() {};
	RETURNCODE getMsgByPkt(hPkt* pPkt, hMsgCycle_t** ppMsgCyc, int id);
	void InitItem (hMsgCycle_t* pItem);
	void EmptyItem(hMsgCycle_t* pItem);
	int  PutObject2Que(hMsgCycle_t * pNewObject, bool toTail = true);
};

#endif//_DDBMSGQUEUE_H

/*************************************************************************************************
 *
 *   $History: ddbMsgQ.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
/*************************************************************************************************
* NOTES:
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbMsgQ.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:23a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
