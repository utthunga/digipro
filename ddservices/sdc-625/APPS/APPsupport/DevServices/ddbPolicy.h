/*************************************************************************************************
 *
 * $Workfile: ddbPolicy.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "ddbPolicy.h"		
 */

#ifndef _POLICY_H
#define _POLICY_H

class hSdispatchPolicy_t
{
public:	
	unsigned int allowSyncWrites;	// true if Syncronous Write available
	unsigned int allowSyncReads;	// true if Asyncronous Write available
	unsigned int incrementalWrite ;	// true if Single command Write service desired
	unsigned int incrementalRead;	// true if Single command Read  service desried
	unsigned int isReplyOnly	;   // true if Device simulator and only replies to messages
	unsigned int isPartOfTok;		// true if Device Object is a part of the tokenizer
	unsigned int abortOnNoListElem;	// true if We follow the spec exactly (& abort @ no list)

	hSdispatchPolicy_t():allowSyncWrites(0),allowSyncReads(0),incrementalWrite(0),
		incrementalRead(0),isReplyOnly(0),isPartOfTok(0),abortOnNoListElem(1){};
	hSdispatchPolicy_t& operator=(const hSdispatchPolicy_t& s)
	{allowSyncWrites=s.allowSyncWrites;allowSyncReads=s.allowSyncReads; 
	 incrementalWrite=s.incrementalWrite;incrementalRead=s.incrementalRead;
	 isReplyOnly=s.isReplyOnly; isPartOfTok = s.isPartOfTok; 
	 abortOnNoListElem = s.abortOnNoListElem; return *this;};
/*	struct dispatchPolicy_s():allowSyncWrites(0),allowSyncReads(0),incrementalWrite(0),
		incrementalRead(0),isReplyOnly(0){};
	struct dispatchPolicy_s& operator=(struct dispatchPolicy_s& s)
	{allowSyncWrites=s.allowSyncWrites;allowSyncReads=s.allowSyncReads; 
	 incrementalWrite=s.incrementalWrite;incrementalRead=s.incrementalRead;
	 isReplyOnly=s.isReplyOnly; return *this;};
	 */
};
/*typedef* / hSdispatchPolicy_t;*/

#endif //_POLICY_H


// NOTES:
//	Single command services:  The ServiceWrites/ServiceReads functions will scan for the best
//		command to send. If no command to send, they return FALSE. Otherwise - they send the 
//		command and then 
//		IF incrementalXXXX is TRUE, they return TRUE - assuming that they will be called again
//			shortly to see if there are any more commands to process.
//		ELSE ( is FALSE ) they loop to scan and process till there are no more to do.
//		-- Normally FALSE if separate task, TRUE when background task (not required architechure)
/*************************************************************************************************
 *
 *   $History: ddbPolicy.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
