/*************************************************************************************************
 *
 * $Workfile: ddbItemLists.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the All the item lists 
 *      lists become classes so they can fill themselves
 *		7/25/2	sjv	creation
 *
 * #include "ddbItemLists.h"
 */

#ifndef _DDBITEMLISTS_H
#define _DDBITEMLISTS_H

#include "ddbItems.h"	/* loads all the includes for items almost all the uncludes)*/
#include "ddb.h"

class CddbDevice;

#include "ddbItemListBase.h" /* breaking up file into base + all others */
#include "ddbCmdList.h"
#include "ddbVarList.h"

// a list of menus (in a device)
class CMenuList : public CitemListBase<iT_Menu, hCmenu>
{
public:
	CMenuList(DevInfcHandle_t h) : CitemListBase<iT_Menu, hCmenu>(h){};
};


class CEditDispList : public CitemListBase<iT_EditDisplay, hCeditDisplay>
{
public:
	CEditDispList(DevInfcHandle_t h) : CitemListBase<iT_EditDisplay, hCeditDisplay>(h){};
};


class CmethodList : public CitemListBase<iT_Method, hCmethod>
{
public:
	CmethodList(DevInfcHandle_t h) : CitemListBase<iT_Method, hCmethod>(h){};
	int getBackgroundMethods(methodCallList_t& methList);
};


class CRefreshList : public CitemListBase<iT_Refresh, hCrefresh>
{
public:
	CRefreshList(DevInfcHandle_t h) : CitemListBase<iT_Refresh, hCrefresh>(h){};
};


class CunitRelList : public CitemListBase<iT_Unit, hCunitRel>
{
public:
	CunitRelList(DevInfcHandle_t h) : CitemListBase<iT_Unit, hCunitRel>(h){};
};


class CwaoList : public CitemListBase<iT_WaO, hCwao>
{
public:
	CwaoList(DevInfcHandle_t h) : CitemListBase<iT_WaO, hCwao>(h){};
};


class CitemArrayList : public CitemListBase<iT_ItemArray, hCitemArray>
{
public:
	CitemArrayList(DevInfcHandle_t h) : CitemListBase<iT_ItemArray, hCitemArray>(h){};
};


class CcollectionList : public CitemListBase<iT_Collection, hCcollection>
{
public:
	CcollectionList(DevInfcHandle_t h) : CitemListBase<iT_Collection, hCcollection>(h){};
};


class CvalueArrayList : public CitemListBase<iT_Array, hCarray>
{
public:
	CvalueArrayList(DevInfcHandle_t h) : CitemListBase<iT_Array, hCarray>(h){};
};


class CfileList : public CitemListBase<iT_File, hCfile>
{
public:
	CfileList(DevInfcHandle_t h) : CitemListBase<iT_File, hCfile>(h){};
};


class CchartList : public CitemListBase<iT_Chart, hCchart>
{
public:
	CchartList(DevInfcHandle_t h) : CitemListBase<iT_Chart, hCchart>(h){};
};


class CgraphList : public CitemListBase<iT_Graph, hCgraph>
{
public:
	CgraphList(DevInfcHandle_t h) : CitemListBase<iT_Graph, hCgraph>(h){};
};


class CaxisList : public CitemListBase<iT_Axis, hCaxis>
{
public:
	CaxisList(DevInfcHandle_t h) : CitemListBase<iT_Axis, hCaxis>(h){};
};


class CwaveformList : public CitemListBase<iT_Waveform, hCwaveform>
{
public:
	CwaveformList(DevInfcHandle_t h) : CitemListBase<iT_Waveform, hCwaveform>(h){};
};


class CsourceList : public CitemListBase<iT_Source, hCsource>
{
public:
	CsourceList(DevInfcHandle_t h) : CitemListBase<iT_Source, hCsource>(h){};
};


class ClistList : public CitemListBase<iT_List, hClist>
{
public:
	ClistList(DevInfcHandle_t h) : CitemListBase<iT_List, hClist>(h){};
	
	void setDefaultValues(void);/* stevev - added 25may05 */
};


class CgridList : public CitemListBase<iT_Grid, hCgrid>
{
public:
	CgridList(DevInfcHandle_t h) : CitemListBase<iT_Grid, hCgrid>(h){};
};


class CimageItemList : public CitemListBase<iT_Image, hCimageItem>
{
public:
	CimageItemList(DevInfcHandle_t h) : CitemListBase<iT_Image, hCimageItem>(h){};
};

/* didn't work first time

class CblobList : public CitemListBase<iT_Blob, hCblob>
{
public:
	CblobList(DevInfcHandle_t h) : CitemListBase<iT_List, hCblob>(h){};
};


class CpluginList : public CitemListBase<iT_PlugIn, hCplugin>
{
public:
	CpluginList(DevInfcHandle_t h) : CitemListBase<iT_PlugIn, hCplugin>(h){};
};


class CtemplateList : public CitemListBase<iT_Template, hCtemplate>
{
public:
	CtemplateList(DevInfcHandle_t h) : CitemListBase<iT_Template, hCtemplate>(h){};
};
*/
/*  unused
Cblock
Crecord

  Cprog
  CvarList
  CitemRespCode
  Cdomain
  Cmember

FORMAT_OBJECT_TYPE
DEVICE_DIR_TYPE
BLOCK_DIR_TYPE
PARAM_ITYPE
PARAM_LIST_ITYPE
BLOCK_CHAR_ITYPE
*/
#endif

/*************************************************************************************************
 *
 *   $History: ddbItemLists.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
