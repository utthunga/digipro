/*************************************************************************************************
 *
 * $Workfile: ddbDeviceMgr.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the singleton device manager for the application.  It holds the 
 * identity 2 handle 2 CommInfc 2 DevicePtr registration map and manages it.
 *	
 *
 *		
 * #include "ddbDeviceMgr.h"
 */

/* * * more notes on this class are at the end of this file * * */


#ifndef _DDBDEVICEMANAGER_H
#define _DDBDEVICEMANAGER_H

#include "ddbGeneral.h"
#include "ddbGlblSrvInfc.h"

#include "ddbCommInfc.h"
#include "ddbDeviceService.h"
#include "ddbCmdDispatch.h"

//prashant for methods support. oct 2003
#include "ddbMethSupportInfc.h"

#include <map>

/* VMKP added (for DDParser Integration) */
//#include "DDlDevDescription.h"
#include "Retn_Code.h"
#include "Dictionary.h"
/* End of VMKP addition */

#include "ddbFileSupportInfc.h"

#define LOOP_WARNING_VARIABLES 1004
/*Vibhor 041203: Start of Code*/
#define STANDARD_DEVICE_HANDLE 1 /*This is the default (reserved) device handle*/
#define SDC_DEVICE_HANDLE      2 /*This has to be separate from the tables     */
/*Vibhor 041203: End of Code*/

/*Anoop 091203: Start of Code*/
#define COMPANY_IDENTIFICATION_CODE 2031 /*This is the default (reserved) device handle*/
/*Anoop 091203: End of Code*/

/* stevev 01may08 - we get our own dd: f9/83/01/00*/
#define SDC_DD_ID   0x00F9008300010000
#define SDC_DD_MFG  0x00F9
#define SDC_DD_DTY  0x0083
#define SDC_DD_DRV  0x0001		/* we are designed for rev one */

// this tells the highest eff version we can handle
#define SUPPORTED_VERSION_MAX  8

#define LANG_LEN  24  /* make it long enough to handle wide string language string */

extern unsigned long getDataItem2Transfer(unsigned char uc);// from port2item map

// external class
class hCddbDevice;
class CVarList;

// stevev 22apr09 - moved from ddbParserInfc.h so we can use 'm from the tokenizer
// stevev 13may08 - moved from ddbParserInfc.cpp so device object can use 'em
/************************************************************************************************
 * manage Indentity_t cInternalFlags
 * BYTE so there are 8 bits
 ***********************************************************************************************/
#define intFlgs_foundDflt        0x01
#define intFlgs_run_silent        0x02  /* don't output progress info                      */
#define intFlgs_look4HARTdflt     0x04  /* re-entry looking for DEFAULT  (first re-entry)  */
#define intFlgs_look4RoseMntDflt  0x08  /* re-entry looking for RM_DEFAULT(second re-entry)*/
#define intFlgs_lookExpandedDT    0x10  /* look in the expanded device type directory      */
#define intFlgs_Do_NOT_load_Dflt  0x20  /* we don't want to shed to generic when trying to load standards or sdc dd */
#define intFlgs_Fmt8orAbove		  0x40	/* set at eff >= 8 is found(no externals required)*/
#define intFlgs_exactDDrevDesired 0x80	/* set to search for exact ddrev  (xmtr-dd)      */

#define intFlgs_look4Dflt         0x0c
#define intFlgs_looking4defaults  0x1c  /* all three bits */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
extern  hCddbDevice * pSDCdevice;

class hCDeviceManger 
{
public:
	static
	int /*bool */    IsdbNOTavailable(void);

	// the application interface is this set of methods
	//    leave these narrow char till we have an international PC to test against
	static hCDeviceManger*  instantiate(const char* pDbdir, 
										const char* pLang = NULL, const char* pAppDir = NULL);
	static
		void         destantiate(); // external deletion

	//  the actual application interface is NOT static - 'this' must be instantiated first -
	//  newDevice, incomingDDkey must be zero for newIdent to be used
	//             if pCD is NULL, the default dispatcher will be used with commInfc
	//			   else (notNULL)  the dispatcher is assumed instantiated and commInfc 
	//					will not be used
	//
	// the below   hCdeviceSrvc*    newDevice( Indentity_t newIdent, 
	hCddbDevice*    newDevice( Indentity_t newIdent,
								hCFileSupport*  pFileSupport,
								hCMethSupport* pMethSupportInterface,
								hCcommInfc* pThisCommInterface, 								
								hCcmdDispatcher* pCD = NULL 
/* 8feb07  no longer supported, left for compatability* , DWORD incomingDDkey = 0 );*/
/* 26sep07 reinstated to pass the info around*/, DD_Key_t incomingDDkey = 0 
/* 11nov08 add a way to detect the tokenizer */, bool isInTokenizer = false  );
	RETURNCODE       deleteDevice(hCdeviceSrvc** pDeviceSrvc); // for symmetry

	/* stevev 06jul05 - set start date */
	void setStartDate(dmDate_t  dateS ){startDate = dateS;};
	void getStartDate(dmDate_t& dateS ){dateS = startDate;};

	static
	hCdeviceSrvc*    getDevicePtr(DevInfcHandle_t myHandle); // Earlier this was a protected one

	unsigned long getDataItem4Port(unsigned char ucc) {return getDataItem2Transfer(ucc);};

	string  getLangStr(void){ return string(language);};

protected:
	// protected contructor - a static method must instantiate me
	hCDeviceManger();
	virtual ~hCDeviceManger();

	// support for the device service interface 
	//   --- USE 'hCglobalServiceInterface' to access this functionality!
	// ----  just returns the correct device's interface pointer (or err interface at error)
//	static
//	hCdeviceSrvc*    getDevicePtr(DevInfcHandle_t myHandle);
	dmDate_t startDate;
	static 
	char language[LANG_LEN];// make it plenty long to handle wide strings

private:
	static hCDeviceManger* pSelf;
	int    DBnotAvailable; // holds the return code of the initDB (FALSE is success)
	DevInfcHandle_t  nextHandle;


	//map for registry - maps handle to device
	map<DevInfcHandle_t, hCdeviceSrvc*> deviceMap;  // handle 2 device ptr map


	friend class hCglobalServiceInterface;   // in lieu of being an abstract parent
											 //       (it's internals can access our internals)


	/*Vibhor 041203: Start of Code*/
	static	hCddbDevice*    stdDevice( Indentity_t newIdent,
								hCFileSupport*  pFileSupport,
								hCMethSupport* pMethSupportInterface,
								hCcommInfc* pThisCommInterface, 								
								hCcmdDispatcher* pCD = NULL,
/* 8feb07  no longer supported, left for compatability DWORD incomingDDkey = 0 );*/
/* 26sep07 reinstated bigger to pass info around */ DD_Key_t incomingDDkey = 0 );
	/*Vibhor 041203: End of Code*/
	void dealWithXXXmethodVariables(aCdevice* pAbstractDev);// helper to break up big functions
	void setLoopWarningVariables( hCitemArray *pItemArray, CVarList *pVARlist );// helper

};



#endif // _DDBDEVICEMANAGER_H

/*************************************************************************************************
* NOTES:
*	The device manager is a singleton object with a protected constructor.  It is only constructed
* if it has never been constructed before when instantiate is called. instantiate may be called
* again if another copy of the device manager is needed.
* 
* Function getMyDevice is used by device components to obtain a device interface 
*    (see ddbDeviceService.h...application doesn't need it since it gets an interface when it 
*         instaniates a device.)
*	 It returns a null pointer if the manager has not been initialized
*
* Function instantiate generates a manager and connects to the database (manager owns the DB!)
*	The application gets a pointer to the device manager so it can generate new devices.
*
* Function newDevice kicks off a class factory that goes to the DB and gets a prototype and then
*	generates an instance of that prototype and assigns it a handle. It then connects the
*	communications interface and returns the device interface pointer.
*  TAKE NOTE: the device manager owns the device memory and is responsible for deleting it.  The
*		device interface pointer justs gives a public interface and must not be destroyed.
*
*
* The hCobject is the ubiquitous parent class for all classes in a device.  It allows the items
*	or attributes or primatives to access the device services.  The device class factory insures
*	that all of the device's objects will have the correct handle.  Note that the handle is
*	instance specific and not dd specific.
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbDeviceMgr.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:15a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
