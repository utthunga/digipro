/*************************************************************************************************
 *
 * $Workfile: ddbTracking.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		Trail generation classes for index resolution in commands
 * #include "ddbTracking.h"
 */

#ifndef _DDBTRACKING_H
#define _DDBTRACKING_H

#include "ddbGeneral.h"
#include "ddbdefs.h"
#include <map>
// normally in  #include "ddbItemBase.h" 
#include "logging.h"
#include "varient.h"

#include "assert.h"


class hCgroupItemDescriptor;
#define GID_T hCgroupItemDescriptor


class   hCitemBase;
typedef hCitemBase*             ptr2hCitemBase_t;
typedef vector<ptr2hCitemBase_t>  ddbItemList_t; // && itemPtrList_t
typedef ddbItemList_t::iterator   ddbItemLst_it; // && iItmList_t

class   hCVar;
typedef vector<hCVar>  varList_t;
typedef varList_t::iterator varLstIT_t;
typedef vector<hCVar*> varPtrList_t;
typedef varPtrList_t::iterator varPtrLstIT_t;




class hCexpressionTrail
{
public:
	bool        isVar;		// else is constant
	itemID_t	srcID;		// @  isVar::  symbol number of the INDEX variable
	int			srcIdxVal;  // @ !isVar::  constant index/name value
	
	hCexpressionTrail()  {clear();};
	hCexpressionTrail(const hCexpressionTrail& s){operator=(s);};

	bool isEmpty(void){ return (isVar==false && srcID==0  && srcIdxVal==0 ); };
	void clear(void)			{isVar=false;   srcID=0x0000;srcIdxVal=0;};
	hCexpressionTrail& operator=(const hCexpressionTrail& s)
				{ isVar=s.isVar;srcID = s.srcID; srcIdxVal=s.srcIdxVal;return *this;};
	void clogSelf(int indent = 0);

};

typedef vector<hCexpressionTrail>   ExprTrailList_t;
typedef ExprTrailList_t::iterator	ETLiterator;

/*************************************************************************************************
 * hCvarGroupTrail
 *
 * this is a data class for dealing with nested collections/arrays
 * used for aquiring information through a resolution
 *************************************************************************************************
 */
class hCitemBase;
#define NOT_PART_OF_GROUP  ((unsigned long)(-1))

class hCvarGroupTrail
{
public:
	itemID_t		 itmID;		// symbol number of item(normally a collection or arrayID or Var)
	hCitemBase*		 itmPtr;	// the value from getItemBySymbolNumber(srcID)<we DO NOT own this>

	wstring         lablStr;
	wstring         helpStr;
	wstring         containerDesc;// member | element info
	wstring         containerHelp;
	unsigned long   containerNumber;// aka: index
	
	hCexpressionTrail* pExprTrail;  // copy of expression   - if it exists <we own these pointers
	hCvarGroupTrail* pSourceTrail;	// copy of source trail - if it exists>
	hCvarGroupTrail* pLookupTrail;  // copy of lookup trail - if it exists

	hCvarGroupTrail():pSourceTrail(NULL),pLookupTrail(NULL),pExprTrail(NULL)  { clear(); };
	hCvarGroupTrail(const hCvarGroupTrail& src)
					 :pSourceTrail(NULL),pLookupTrail(NULL),pExprTrail(NULL)  {*this=src;};
	virtual ~hCvarGroupTrail(){clear();};

	/* the following are in ddbCollAndArr.cpp */
	void clear(void);
	bool isEmpty(void);

	hCvarGroupTrail& operator=(const hCvarGroupTrail& src);
	hCvarGroupTrail& operator=( hCgroupItemDescriptor& src);

	void clogSelf(int indent = 0);
};



/*************************************************************************************************
 * hCvarSrcTrail
 *
 * this is a data class for tracking nested collections/arrays
 * used for detecting index usage/values for commands
 *************************************************************************************************
 */
class hCvarSrcTrail
{
public:
	itemType_t	srcType;	// type of srcID (collect or array)
	itemID_t	srcID;		// symbol number of source var(normally a collection or arrayID)
	hCitemBase*	srcPtr;	    // the value from getItemBySymbolNumber(srcID)<we DO NOT own this>

	int			srcIdxVal;	// index value for this resolution
	bool        parentIsDescGrp;  
	bool        parentIsIndxGrp;

	wstring     lastDescStr;
	wstring     frstDescStr;
	wstring     LabelStr;

	hCvarSrcTrail(){clear();};
	hCvarSrcTrail(const hCvarSrcTrail& src){*this=src;};

	void clear(void)
			{srcType=iT_ReservedZeta;srcPtr=NULL;srcID=0x0000;srcIdxVal=-1;
	         parentIsDescGrp=parentIsIndxGrp= false;  lastDescStr.erase();frstDescStr.erase();
			 LabelStr.erase();};
	hCvarSrcTrail& operator=(const hCvarSrcTrail& src)
			{srcType=src.srcType;srcID=src.srcID;  srcPtr=src.srcPtr;srcIdxVal=src.srcIdxVal; 
			 parentIsDescGrp = src.parentIsDescGrp;  parentIsIndxGrp = src.parentIsIndxGrp; 
			 lastDescStr = src.lastDescStr; frstDescStr = src.frstDescStr; LabelStr=src.LabelStr;
			 return *this;};
};
typedef vector<hCvarSrcTrail>   trailList_t;
typedef trailList_t::iterator	TLiterator;


class hCGroupItemInfo
{
public://								LU=lookup
	itemID_t			theVarID;//		aka	LUvalue
	hCitemBase*			theVarPtr; //	aka LUptr
	hCexpressionTrail   theVarExpr;//	aka LUExpr
	hCGroupItemInfo*    theVarHist_p;//	aka LUhistory*
	itemID_t			srcID;
	hCitemBase*			srcPtr;
	hCexpressionTrail   srcExpr;
	hCGroupItemInfo*    srcHist_p;

	hCGroupItemInfo();
	hCGroupItemInfo(const hCGroupItemInfo& s);
	virtual ~hCGroupItemInfo();

	void clear(void);
	hCGroupItemInfo& operator=(const hCGroupItemInfo& s);
	
	int  fillExpressions(ExprTrailList_t& etList);// extract all expression Vars
		
	void clogSelf(int indent = 0);
};
typedef vector<hCGroupItemInfo>     GroupItemList_t;
typedef GroupItemList_t::iterator	GILiterator;


// index usage 
// Assumes a single index will resolve a single dataitem in the command.
// When structures like arrays of arrays are supported, this may have to add
// a list of hIndexUse_t that resolve the next layer of reference.
/* 30jan14 - for command selection this used to be populated like this:
		                                		for a localvar			for a device var
				indexSymID						index's itemID			index's itemID

				indexDispValue					value required			-1
				indexRealValue					same value as			value required
												indexDispValue
	and all the indexes were lumped together assuming one per resolution.
	-- the problem was that while the local index could me manipulated, the device-var index
	could not change and had to be a specific value to resolve a variable.
	::> changed the system so that a single instance of this class will resolve one variable.
	If there are more than two indexes required to resolve a variable, an error will be output.
	New population::>
	                        		for a localvar			for a device var	for both
				indexSymID			index's itemID			 0		            index's itemID
				indexDispValue		value required			-1					value required
				indexRealValue		value required			-1					value required
												
				devIdxSymID              0                  devVar itemID       devVar itemID
				devVarRequired			-1					value required2		value required2
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


class hIndexUse_t
{
public:
	itemID_t	indexSymID;
	int			indexWrtStatus; // stevev added 5aug08 0 normal/1 user writen/2 was info
	INSTANCE_DATA_STATE_T indexDataState;
	CValueVarient		  indexDispValue;
	CValueVarient		  indexRealValue;

	itemID_t			  devIdxSymID;
	unsigned			  devVarRequired;


	hIndexUse_t() {clear();};
	~ hIndexUse_t(){clear();};
	hIndexUse_t(const hIndexUse_t& s) { operator=(s); };

	void   clear(void){ indexSymID = 0; indexWrtStatus=0; indexDataState = IDS_UNINITIALIZED;
						devIdxSymID = 0;indexRealValue.clear(); indexDispValue.clear();
						devVarRequired=-1;};
	hIndexUse_t& operator=(const hIndexUse_t& s)
					{ indexSymID =s.indexSymID;           indexWrtStatus = s.indexWrtStatus;
					  indexDataState = s.indexDataState;  indexRealValue=s.indexRealValue;
					  indexDispValue = s.indexDispValue;  devVarRequired=s.devVarRequired; 
					  devIdxSymID    = s. devIdxSymID;       return (*this);  };	
	bool   isNOTempty(void) {return((indexSymID>0)||(devIdxSymID>0));};
	void   clogSelf(void){
		LOGIF(LOGP_WEIGHT)(CLOG_LOG," LocalIdxVar: 0x%04x w/Val:0x%x, "
									"DeviceIdxVar: 0x%04x w/Val:0x%x\n",
									   indexSymID,(unsigned)indexDispValue,
									   devIdxSymID,  devVarRequired );};
};

typedef vector<hIndexUse_t> indexUseList_t;
typedef indexUseList_t::iterator  idxUseIT;


class hCcommandDescriptor
{
public:
	int					cmdNumber;
	int					transNumb;
	int					rd_wrWgt;
	//	the following may need to become a list of hCvarSrcTrail as tokenizer improves
	//int				srcIdxVal;	// index value for this resolution - -1 @ none needed
	itemID_t			srcIdxVar;	//enabled stevev-30dec08, return value from commandAllLists
	indexUseList_t		idxList;
	cmdOperationType_t	cmdTyp;

	hCcommandDescriptor(){clear();};
	hCcommandDescriptor(const hCcommandDescriptor& cd) {operator=(cd);};
	void clear(void)
	{   cmdNumber=transNumb=-1;rd_wrWgt=0;srcIdxVar=0;cmdTyp=cmdOpNone;idxList.clear();  };
	hCcommandDescriptor& operator=(const hCcommandDescriptor& cd);
	void dumpSelf(void);

	hIndexUse_t indexValue(itemID_t idxItm)
		{	hIndexUse_t retType;
			for (idxUseIT iuIT = idxList.begin(); iuIT != idxList.end(); ++iuIT)
			{	hIndexUse_t *pIu = (hIndexUse_t *) &(*iuIT);
				if (pIu->indexSymID == idxItm) retType = *pIu;
			}
			return retType;
		};
};

typedef vector<hCcommandDescriptor> cmdDescList_t;
typedef cmdDescList_t::iterator     cmdDescListIT;


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
 * The following are used for index precalculation to preclude calculating this each weigh cycle.
 * Originally done by Honeywell but not keyed correctly (see CVS branch Honeywell_2), this
 * implementation has a IndexValue map in each transaction.
 * Note that this implementation still does not deal with more than one index variable per 
 * dataitem.  That expansion will be complicated but these classes will help to hide some of
 * that complexity.
 *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

class hCIndexValueAndVariables	// an index value and the variables it resolves
{
public:
	unsigned     indexValue;
	unsigned	 devicValue;
	unsigned     deviceIdxID;
//  stevev changed to preclude looking up pointer at each use
//	itemIDlist_t resolvedVarList;// iterator is itemIDlistIT_t
	varPtrList_t resolvedVarList;// iterator is varPtrLstIT_t..a ptr2ptr2hCVar

public:
	hCIndexValueAndVariables(): indexValue(-1), devicValue(-1), deviceIdxID(0)
		{	resolvedVarList.clear();};
	hCIndexValueAndVariables(const hCIndexValueAndVariables& IV2V): indexValue(-1), 
																devicValue(-1), deviceIdxID(0)
		{	operator=(IV2V); };
//	hCIndexValueAndVariables(unsigned idxValue, unsigned deval, hCVar* pV): 
//		indexValue(idxValue), devicValue(deval) 
	hCIndexValueAndVariables(hIndexUse_t& idxIn, hCVar* pV): deviceIdxID(idxIn.devIdxSymID),
		indexValue((int)idxIn.indexDispValue),	 devicValue(idxIn.devVarRequired)
		{	resolvedVarList.clear(); resolvedVarList.push_back( pV ); };
	virtual
	~hCIndexValueAndVariables()
		{	clear(); };
public:	
	void clear(void)
		{	indexValue = 0xffffffff; resolvedVarList.clear();};
	hCIndexValueAndVariables& operator=(const hCIndexValueAndVariables& IVVM)
		{	indexValue = IVVM.indexValue; 
			devicValue = IVVM.devicValue;  deviceIdxID=IVVM.deviceIdxID;
			resolvedVarList = IVVM.resolvedVarList;return *this; };

	bool isEmpty() 
		{	return(resolvedVarList.size() == 0 && 
				   (indexValue == 0xffffffff || indexValue == 0)); 
		};
	RETURNCODE appendUnique(hCVar * pVTarget);
	void   clogSelf(void);
};

typedef vector<hCIndexValueAndVariables> valueNvars_t;
typedef valueNvars_t::iterator           valueNVarsIT_t;

/* this is the list of the index's valid values and the variables they each resolve */
class hCIndexValue2Variable : public valueNvars_t
{
	itemID_t idxID;// used in weighing, not always filled when using map

public:	
	hCIndexValue2Variable() 
		{	clear();	};
	hCIndexValue2Variable( const hCIndexValue2Variable& IVnV ) 
		{	operator=( IVnV );	idxID=IVnV.idxID; };
//	hCIndexValue2Variable(unsigned idxValue, unsigned devValue, hCVar* pVar)
//		{	clear(); 
//assert(0);// getting the right combinationsof vars and values
//			hCIndexValueAndVariables  localIVnV(idxValue,  devValue,  pVar);
//			push_back(localIVnV);
//		};
	hCIndexValue2Variable(indexUseList_t& idxNvalueList, hCVar* pVar)
		{	clear(); 
			idxID =  idxNvalueList[0].indexSymID;

			idxUseIT       iT;
			hIndexUse_t *pIUt;

			for ( iT = idxNvalueList.begin(); iT != idxNvalueList.end(); ++iT )
			{// for each incoming value/devValue
				pIUt = (hIndexUse_t *)(&(*iT));
				hCIndexValueAndVariables local(*pIUt, pVar);
				push_back(local);
			}// next incoming index value
		};
	virtual
	~hCIndexValue2Variable()
		{	clear();	};
public:
	void     setID(itemID_t newID){
		idxID = newID;        };
	itemID_t getID(void){
		return (idxID); };

	void clear(void)
	{	idxID = 0;  valueNvars_t::clear();	};
	hCIndexValue2Variable& operator=(const hCIndexValue2Variable& IVVM)
		{	valueNvars_t::operator=( IVVM ); idxID=IVVM.idxID; return *this;	};
	bool isEmpty() 
		{	return(size() == 0);	};
/*	RETURNCODE appendUnique(unsigned idxValue, unsigned devValue, hCVar* pVar)
		{	bool notThere = true;
	assert(0);// find where these are being used
			for ( valueNVarsIT_t I = begin(); I != end(); ++I)
			{	hCIndexValueAndVariables *pIVVs = (hCIndexValueAndVariables *)(&(*I));
				if ( pIVVs->indexValue == idxValue && pIVVs->devicValue == devValue) 
				{	notThere = false;    
					pIVVs->appendUnique(pVar);
					break;   
				}// else - keep looking
			}// next existing index value
			if ( notThere )
			{
				hCIndexValueAndVariables local(idxValue, devValue, pVar);
				push_back(local);
			}
			return SUCCESS;
		};
*/

	// called for this index ID, new var
	RETURNCODE appendUnique(indexUseList_t& idxNvalueList, hCVar* pVar)
		{	bool notThere = true;
			idxUseIT       iT;
			hIndexUse_t *pIUt;

			for ( iT = idxNvalueList.begin(); iT != idxNvalueList.end(); ++iT )
			{// for each incoming value/devValue
				pIUt = (hIndexUse_t *)(&(*iT));
				for ( valueNVarsIT_t I = begin(); I != end(); ++I)
				{	// for each existing value
					hCIndexValueAndVariables *pIVVs = (hCIndexValueAndVariables *)(&(*I));
					if ( pIVVs->indexValue == (unsigned)(pIUt->indexDispValue)     && 
						 pIVVs->deviceIdxID== pIUt->devIdxSymID  &&
						 pIVVs->devicValue == pIUt->devVarRequired   ) 
					{	notThere = false;    
						pIVVs->appendUnique(pVar);
						break;   
					}// else - keep looking
				}// next existing index value

				if ( notThere )
				{
					hCIndexValueAndVariables local(*pIUt, pVar);
					push_back(local);
				}
			}// next incoming index value

			return SUCCESS;
		};

	void   clogSelf(void)
		{	if (isEmpty() )
			{	LOGIT(CLOG_LOG,"       Has NO values.\n"); return;}//else clog
			LOGIT(CLOG_LOG,"    IndexID: 0x%04x\n",idxID);
			for ( valueNVarsIT_t I = begin(); I != end(); ++I)
			{	hCIndexValueAndVariables *pIVVs = (hCIndexValueAndVariables *)(&(*I));
				pIVVs->clogSelf();
			}// next value
		};
};

typedef vector<hCIndexValue2Variable>       IdxVal2VarList_t;
typedef IdxVal2VarList_t::iterator          IdxVal2VarList_IT_t;//ptr2class

typedef map<itemID_t,hCIndexValue2Variable> IndexID2ValueVariable_t;
typedef IndexID2ValueVariable_t::iterator   IndexID2ValueVarIT_t;

/* hCIndexIDValueVariableMap
 *	maps the index variable to a list of values & the variableIDs resolved by the values
 * This is expected to exist in a transaction so that the key would be:
 *      command/transaction/indexVarID/Value to give all the variables it resolves
 */
class hCIndexIDValueVariableMap : public IndexID2ValueVariable_t
{
public:
	hCIndexIDValueVariableMap()
		{	clear();	};
	hCIndexIDValueVariableMap(const hCIndexIDValueVariableMap& IVVM) 
		{	*this = IVVM;	};
	virtual
	~hCIndexIDValueVariableMap() 
		{	clear();	};
	// use the map's version of clear and equal...
	//void clear(void);
	//hCIndexIDValueVariableMap& operator=(const hCIndexIDValueVariableMap& IVVM);

/*	// needs to uniquely put in a two stage map for this data
	RETURNCODE appendUnique(hIndexUse_t idxNvalue, hCVar* pVar)
		{	IndexID2ValueVarIT_t idxID2VVit = find(idxNvalue.indexSymID);
			if ( idxID2VVit == end() )
			{// not there
				hCIndexValue2Variable localIV2V((int)idxNvalue.indexDispValue, 
					                            (int)idxNvalue.devVarRequired ,pVar);
				insert(IndexID2ValueVariable_t::value_type(idxNvalue.indexSymID,localIV2V));
			}else // found one
			{	//idxID2VVit->second is an actual item - not a pointer to an item
				
				hCIndexValue2Variable *pIV2V = (hCIndexValue2Variable*)&(idxID2VVit->second);
			pIV2V->appendUnique((int)idxNvalue.indexDispValue, -1, pVar);// -1 is stubout till fixed
			}
			return SUCCESS;
		};
**/
	// needs to uniquely put in a two stage map for this data - called for a single indexID
	RETURNCODE appendUnique(indexUseList_t& idxNvalueList, hCVar* pVar)
		{	// caller mus verify list has something in it
			unsigned idxID = idxNvalueList[0].indexSymID;
			IndexID2ValueVarIT_t idxID2VVit = find(idxID);
			if ( idxID2VVit == end() )
			{// this index is not there yet
				hCIndexValue2Variable localIV2V(idxNvalueList ,pVar);
				insert(IndexID2ValueVariable_t::value_type(idxID,localIV2V));
			}
			else // found one..this index has been handled before
			{	//idxID2VVit->second is an actual item - not a pointer to an item				
				hCIndexValue2Variable *pIV2V = (hCIndexValue2Variable*)&(idxID2VVit->second);

				pIV2V->appendUnique(idxNvalueList, pVar);
			}
			return SUCCESS;
		};



	void   clogSelf(void)// order is kinda random
		{	for(IndexID2ValueVarIT_t I = begin(); I != end(); ++I)
			{	// (I->first) reference to ID,  (I->second) ref to hCIndexValue2Variable
				LOGIT(CLOG_LOG,"IdxVar: 0x%04x Has:\n",(I->first) );
				I->second.clogSelf();
			}// next index variable
		};
};


#endif //_DDBTRACKING_H

/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
