/*************************************************************************************************
 *
 * $Workfile: ddbPrimatives.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the lowest level class (string, long, etc) implementation
 * 4/8/2	sjv	created
 */

//#include "tags_sa.h"

//#include "env_info.h"
//#include "Dictionary.h"

///////// FUDGE
//* #include "ddbDevice.h"
//* #if defined(ISDLL) || defined(SKEL)
//* CddbDevice* getDevPtr(void){return NULL;};
//* #else
//* extern CddbDevice* getDevPtr(void);
//* #endif

#include "ddbItemBase.h"
#include "ddbVar.h"
#include "ddb.h"

/*Vibhor 041203: Start of Code*/
#include "ddbDevice.h"
/*Vibhor 041203: End of Code*/
#include "Dictionary.h"

#include "ddbPrimatives.h"

extern	CDictionary *dictionary;


#ifndef DEV_STRING_INFO_FLAG
typedef struct {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;
#endif

#if 0
int pcnt=0, icnt=0,izcnt=0;
#endif // 0

#ifdef __cplusplus
    extern "C" {
#endif
extern int keyFromDevInfo(DEV_STRING_INFO& dsi);
//extern int app_func_get_dict_string(ENV_INFO *env_info, ulong index, ddpSTRING *str);
//	#ifndef ISDLL
//	extern int app_func_get_dict_string(ENV_INFO *env_info, ulong index, ddpSTRING *str);
//	#endif
#ifdef __cplusplus
    }
#endif

//#ifdef ISDLL
//	extern Cdictionary* pMainDict;
//#endif


hCddlString::hCddlString(DevInfcHandle_t h) : hCobject(h)
{
	//theStr     assume the string will instantiate empty()
	len     = 0; 
    flags = FREE_STRING;
	ddKey   = 0;  
	ddItem  = 0;
	enumVal = 0;
	strTag  = 0;
	pRef  = NULL;
#ifdef _DEBUG_ALLOC
		clog<<"   String new     0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
}

hCddlString::~hCddlString()
{
	//RAZE(pRef); // always ours now
	if(pRef!=NULL)
	{ 
		pRef->destroy();
	//	delete pRef; 
	//	pRef = NULL;
		RAZE(pRef); 
	
	}
	//theStr.~string();
	theStr.erase();
#ifdef _DEBUG_ALLOC
		clog<<"   String destrct 0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
		if ( ((unsigned int)this) == 0x146cfe8 )
		{
			clog<<"   Critical string deleted."<<endl;
		}
#endif
}

/* stevev 18jan05 - removed old code enclosed in #if 0... */
/* protected function */
/* stevev 04sep13 - unprotected so we can update the referenced strings */
RETURNCODE hCddlString::fillStr(/*CcommAPI* pComm,*/ unsigned int iID)
{
	RETURNCODE rc = SUCCESS;
	//ddpSTRING tString;
//	rc = fillStr(strTag);

	if ( ddKey==0 && ddItem == 0 && enumVal == 0 && pRef == NULL)
	{
		if (flags & USE_DFLT_VALUE)
		{
			theStr = THE_DEFAULT_VALUE; // assume empty is the default
			return rc;
		}
		else
		if ( !(strTag == DICTIONARY_STRING_TAG && flags == FREE_STRING)
			&& !(flags & ISEMPTYSTRING))
		{// we get this error when the label is::  LABEL [blank]
			LOGIT(CERR_LOG, "Warning: String is empty with no default flag.\n");
		}
		//else - dict str & free str - normal on dict string [blank]
	}

	switch(strTag)
	{
		case DEV_SPEC_STRING_TAG:
		{
			//use ddKey && ddItem as key/index to get the string
			// DICTDDSTR::  DDkey (from idDB) itemNumber of string

			ddKey = devPtr()->getDDkey();

			wstring* pDLLstr = NULL;
/*try- didn't work rc = procureStringByDoubleRemote(ddKey,iID,DICTDDSTR,DFLT_LANG,&pDLLstr);*/
			theStr = devPtr()->getLiteralString(iID);
			//if (rc == SUCCESS && pDLLstr != NULL)
			//{
			//	theStr = (pDLLstr->c_str());
			//}
			//else
			if (rc != SUCCESS)
			{
				theStr = getStandardStr("default_string_entries",2);//++Key-SymID lookup failed
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s 0x%08x 0x%04x\n",
																	theStr.c_str(),ddKey,iID);
			}
			releaseString(&pDLLstr);
//no main		theStr = (*ppMainDict)->getDictString(ddKey, iID, DICTDDSTR, USEDEFAULT);
//			//theStr = (*ppMainDict)->getDictString(ddKey, ddItem, DICTDDSTR, USEDEFAULT);
			CLRPLACEHOLDER;
		}
		break;
		case VARIABLE_STRING_TAG:
		{
		//pRef is Not a a ref and holds an ID
		// rc = app_func_get_param_value(env_info, &oper_ref, &var_value);
		//      returned in var_value.val.s			
		//theStr = L"++Variable String++";SETPLACEHOLDER;		//01.02.01
		theStr = getStandardStr("default_string_entries",3);SETPLACEHOLDER;//++Variable String++ 01.02.01
/* stevev modified 21jan05 */
if ( ddItem != 0 )
{
	hCitemBase* pIB = NULL;
	if ( SUCCESS == devPtr()->getItemBySymNumber(ddItem,&pIB) )
	{
		if ( pIB != NULL && pIB->IsVariable() )
		{
#ifdef _DEBUG
	int vt = ((hCVar*)pIB)->VariableType();
	if ( vt != 	vT_Ascii && vt != vT_PackedAscii && vt != vT_Password )
	{
		LOGIT(CERR_LOG,"WARNING: variable-string string type is NOT a string.(%s)\n",
			varTypeStrings[((hCVar*)pIB)->VariableType()]);
	}
#endif
			theStr = ((hCVar*)pIB)->getDispValueString();
		}
		else
		{
		LOGIT(CERR_LOG,"WARNING: variable-string string type is NOT a variable.(%s)\n",
			itemStrings[pIB->getIType()]);
		}
	}
}
#ifdef _DEBUG
else
{
	LOGIT(CERR_LOG,"WARNING: variable-string string has no variable ID.\n");
}
#endif
		}
		break;
		case VAR_REF_STRING_TAG:
		{
		//pRef is a ref and holds the required info
		// rc = app_func_get_param_value(env_info, &oper_ref, &var_value);
		//      returned in var_value.val.s	
//theStr = L"++Variable Reference String++";SETPLACEHOLDER;
			LOGIT(CLOG_LOG,"+++ERROR!!!! we got a var-ref string we couldn't handle.\n");
												//++Variable Reference String++ 01.02.01
			theStr = getStandardStr("default_string_entries",4);SETPLACEHOLDER;
		}
		break;
		case ENUMERATION_STRING_TAG:
		{
		//pRef is Not a a ref and holds an ID (or ddItem)--- enumVal is valid and must be used
		//rc = string_enum_lookup(enum_id, enum_val, string, depinfo, env_info, var_needed);
		// scan the enumeration descriptions to fill the string
		//	get the item ptr from the device
		//	aquire the string by value from the enumeration		
		//theStr = L"++Enum String++";SETPLACEHOLDER;01.02.01
		theStr = getStandardStr("default_string_entries",1);//++Enum String++,stevev 14oct11
				 SETPLACEHOLDER;					

			hCitemBase* pItem = NULL;

// use pComm retreive to get the correct value...
			if ( /*pComm != NULL  &&*/ pRef != NULL || ddItem != 0 )
			{
				itemID_t itemNumber = 0;
				if (ddItem != 0)
				{
					itemNumber = ddItem;
				}
				else // has to be a pref...
				{
					itemNumber = pRef->getID();
				}
				if ( itemNumber != 0 )
				{
					//if ( pComm->Retrieve(pRef->getID(),   pItem) == SUCCESS )
/*Vibhor 041203: Start of Code*/
					hCddbDevice *pCurrentDevice;
					pCurrentDevice = (hCddbDevice*)devPtr();
/*Vibhor 041203: End of Code*/
					if (pRef == NULL)
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"   
									" id = 0x%04x but Ref ptr is NULL\n ", itemNumber);
						return FAILURE;
					}
					if (pCurrentDevice->getItemBySymNumber(itemNumber, &pItem) != SUCCESS ||
						pItem == NULL )
					{// not in the device, check the standard tables
                        hCDeviceManger *Ptr2DevMgr = pCurrentDevice->pDevMgr;
                        
                        hCddbDevice *stdTbl = 
							(hCddbDevice*)Ptr2DevMgr->getDevicePtr(STANDARD_DEVICE_HANDLE);

						if( stdTbl==NULL || stdTbl->getDDkey() == 0 ||
							stdTbl->getItemBySymNumber(pRef->getID(),&pItem) != SUCCESS ||
							pItem == NULL)
						{// don't have standard tables or the tables don't have it either
							// try the SDC DD							
							stdTbl =(hCddbDevice*)Ptr2DevMgr->getDevicePtr(SDC_DEVICE_HANDLE);

							if( stdTbl==NULL ||  stdTbl->getDDkey() == 0 ||
								stdTbl->getItemBySymNumber(pRef->getID(),&pItem) != SUCCESS )
							{// string doesn't exist
								pItem = NULL;
								LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"                            
									" ID:0x%04x could not be retrieved from the device,"
									" the standard tables or from the sdc-device\n",
									pRef->getID() );
								rc = FAILURE;
							}
						}
					}
					if ( pItem != NULL )// we found one
					{
						if ( pItem->getIType() == iT_Variable)
						{
							int t = ((hCVar*)pItem)->VariableType() ;
							if ( t == vT_Enumerated || t == vT_BitEnumerated )
							{
								wstring tStr;
								if ( ((hCEnum*)pItem)->procureString(enumVal, tStr) == SUCCESS)
								{
									theStr = tStr;
									rc = SUCCESS;
								}
								else
								{
									LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"   
									" Could not procure string for enum ID:0x%04x (%s) "
									"value:%d \n",
									pRef->getID(), pItem->getName().c_str(),enumVal);
                                    rc = FAILURE;
								}
							}
							else
							{
								LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"     
								" ID:0x%04x (%s) is not an enumerated type (%s)\n",
								pRef->getID(), pItem->getName().c_str(),pItem->getTypeName()); 
                                rc = FAILURE;
                            }
                        }/*Endif iT_Variable*/
                        else
                        {
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"    
							" ID:0x%04x (%s) is not a variable (%s) \n",
							pRef->getID(), pItem->getName().c_str(),pItem->getTypeName() );
                            rc = FAILURE;
                        }
                    }/*Endif getItembySymNumber - else we've already put out an error */
				}
/*Vibhor 041203: End of Code*/
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr -"
												" the reference resolved to item id ZERO.\n");
					rc = FAILURE;
				}
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: ddlString.fillStr - pRef was NULL.\n");
				rc = FAILURE;
			}

//* CddbDevice* pDev = getDevPtr();
//* #ifndef SKEL  /* the loader cannot resolve this string!*/
#if 0
        code that was here was removed 02jul09 - see earlier versions if content required
#endif
		}
		break;
		case ENUM_REF_STRING_TAG:
		{
		//pRef is a ref and holds the required info--- enumVal is valid and must be used
		//rc = string_enum_lookup(enum_id, enum_val, string, depinfo, env_info, var_needed);
		// scan the enumeration descriptions to fill the string
		//theStr = L"++Enum Reference String++";SETPLACEHOLDER;01.02.01
														//5,++Enum Reference String++ 01.02.01
		theStr = getStandardStr("default_string_entries",5);SETPLACEHOLDER;
		}
		break;
		case DICTIONARY_STRING_TAG:
		{ //ddItem holds the dictionary string index			
//			theStr = (*ppMainDict)->getDictString(ddItem, DICTALLST, USEDEFAULT);
				//rc = app_func_get_dict_string(NULL, temp, &tString);
				//if not found
				//	get index: DEFAULT_STD_DICT_STRING
			if ( theStr.empty())
			{
//				theStr = (*ppMainDict)->getDictString(DEFAULT_STD_DICT_STRING, DICTALLST, USEDEFAULT);
			}
			CLRPLACEHOLDER;
		}
		break;
////		case UNDEFINED_TAG:
		default:
		{		
//			theStr = (*ppMainDict)->getDictString(DEFAULT_STD_DICT_STRING, DICTALLST, USEDEFAULT);
		}
		break;
	}
	if (theStr == L"ERROR: Dict String Not Found")
	{
		int x = 0;
	}

	return rc;
}


/* public:  the other one is protected */
RETURNCODE hCddlString::fillStr(ulong dictStringNumber)
{
	RETURNCODE rc = SUCCESS;

#ifdef ISDLL
	theStr = pMainDict->getDictString(dictStringNumber);
	if (theStr.empty())
	{
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"DDLstring: Dict String Not Found: Key=0x%04x\n",dictStringNumber);
#endif
		theStr = pMainDict->getDictString(DEFAULT_STD_DICT_STRING);
		LOGIT(CERR_LOG,"Dictionary String Not Found (fillStr). [ %d, %d ]\n",
						(dictStringNumber / 0x10000), (dictStringNumber % 0x10000) );
	}
	CddlString::len = (int)theStr.length();
#else
//	ddpSTRING tString;


//	rc = app_func_get_dict_string(NULL, dictStringNumber, &tString);
//	rc = dictionary->get_dictionary_string(dictStringNumber,theStr);

	wstring newstr;
	dictionary->get_dictionary_string(dictStringNumber,newstr);
	dictionary->get_string_translation(newstr, theStr);


	/* If a string was not found, get the default error string.*/
	if (rc != SUCCESS || theStr.empty()) // rc is always SUCCESS [2/5/2016 tjohnston]
	{
#ifdef _DEBUG
		if ( rc != SUCCESS )
			LOGIT(CLOG_LOG,"DDLstring.fillStr: Dict String Not Found: Key=0x%04x\n",dictStringNumber);
		else
			LOGIT(CLOG_LOG,"DDLstring.fillStr: Dict String Returned Empty: Key=0x%04x\n",dictStringNumber);
#endif
//		rc = app_func_get_dict_string(NULL,DEFAULT_STD_DICT_STRING, &tString);
		rc = dictionary->get_dictionary_string(DEFAULT_STD_DICT_STRING,theStr);
		if (rc != SUCCESS || theStr.empty()) 
		{
			//theStr = L"Dictionary string could not be found.";
			theStr = getStandardStr("default_string_entries",6);
			LOGIT(CERR_LOG,"Dictionary String Not Found (fillStr). [ %d, %d ]\n",
							(dictStringNumber / 0x10000), (dictStringNumber % 0x10000) );
			SETPLACEHOLDER;
			return rc;
		}
	}
//	*this = &tString;
#endif
	CLRPLACEHOLDER;
	return rc;
}

// eg newStr = getStandardStr("default_string_entries", 1 ); added from HCF repository 01.02.01
wstring hCddlString::getStandardStr(char* enum_Name, ulong enumValue)
{
	string      itemName;
	hCitemBase* pItmBase  = NULL;
	hCEnum*     pItemEnum = NULL;

	wstring     retVal;
	RETURNCODE rc = FAILURE;

	if ( enum_Name != NULL && enum_Name[0] != '\0' )
	{
		itemName = enum_Name;
		rc = stdPtr()->getItemBySymName(itemName, &pItmBase);

		if ( rc == SUCCESS  && pItmBase != NULL)
		{
			pItemEnum = (hCEnum*)pItmBase;
			rc = pItemEnum->procureString(enumValue,retVal);
		}
	}
	if ( rc != SUCCESS || retVal.empty() )
	{
		retVal = STR_OF_LAST_RESORT;
	}

	return retVal;
}


/*************************************************************************************************
 *
 *   $History: ddbPrimatives.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */



