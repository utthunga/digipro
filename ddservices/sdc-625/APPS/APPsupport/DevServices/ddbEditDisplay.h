/*************************************************************************************************
 *
 * $Workfile: ddbEditDisplay.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The Edit Display item classes
 *		8/29/02	sjv	created from ddbItems.h
 *
 *		
 * #include "ddbEditDisplay.h".
 */

#ifndef _DDB_EDITDISPLAY_H
#define _DDB_EDITDISPLAY_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"
#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "logging.h"

// extern reference
class hCVar;
// forward reference
//class hCeditDisplay;

// an edit display will have a list of 


class hCeditDispBase : public HreferenceList_t, public hCobject
{// common and list manipulation code goes here
//protected:
//	hCeditDisplay* pEdDisp;

public:
	hCeditDispBase(DevInfcHandle_t h): hCobject(h) {};
//	hCeditDispBase(const hCeditDispBase& edb){pEdDisp=edb.edDisp();};
	virtual ~hCeditDispBase(){
		destroy();
		clear();
/*
    LINUX_PORT - REVIEW_WITH_HCF: Having to call STL container base class 
    destructor explicitly because they are not designed to be base classes 
    (no virtual destructor).
*/
		if ( size() ) // stevev 26aug10 try to avoid deletion crash & memory leaks
#if defined(__GNUC__)
			this->~HreferenceList_t();
#else // !__GNUC__
			HreferenceList_t::~vector();
#endif // __GNUC__
	};// should self destruct
	RETURNCODE destroy(void)
	{	RETURNCODE rc = 0;
		for(HreferenceList_t::iterator iT = begin(); iT < end(); iT++)
		{	rc |= iT->destroy();
		} return rc;
	};
//	virtual hCeditDisplay* edDisp(void){ return pEdDisp;};
};



/*****************************************************************************
 * the Generic Edit Display List 
 *		- there are 4 of 'em in the Edit Display
 *	
 *	eddispAttrEditItems,  - conditional list of references to items 
 *	eddispAttrDispItems,  - conditional list of references to items
 *	eddispAttrPreEditAct, - conditional list of references to methods
 *	eddispAttrPostEditAct - conditional list of references to methods
 *
 * a hCedDispList is the destination of the conditionals
 *             and is a list of references
 * EditDisp attributes are 
 *		lists of conditionals 
 *			each conditional resolves to a list of references <an hCedDispList>
 *
 *****************************************************************************/
/* stevev 2nov4      use hCreferenceList
class hCedDispList : public hCpayload, public hCeditDispBase // parent is the reference list
{

public:
	hCedDispList(DevInfcHandle_t h):hCeditDispBase(h) {};
//	hCedDispList(hCeditDisplay* pT) {pEdDisp=pT;};
	virtual ~hCedDispList(){hCeditDispBase::~hCeditDispBase();};
	RETURNCODE destroy(void){return (hCeditDispBase::destroy());};


	hCedDispList(const hCedDispList& ed):hCeditDispBase(ed.devHndl())
	{ 		
		//itemList = ed.itemList;
		*this = ed;
//		// probably needs to copy the base classe'seddisp pointr too
	};


	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)
	{
		COUTSPACE << "      " << size() << " Edit Display " << typeName << " Items   " << endl;
		for(iterator p = begin(); p < end(); p++)
		{
			p->dumpSelf(indent);
		}
		return SUCCESS;
	};
	RETURNCODE getItemPtrs(varPtrList_t& edL);   // list -> references -> items
	void       append(hCedDispList* pList2Append); // insert a list behind a list

	void  setEqual(void* pAclass); // a hCpayload required method	                   
	// each payload must tell if the abstract payload type is correct for itself
	boolT validPayload(payloadType_t ldType){ if (ldType != pltReferenceList) return isFALSE; 
											  else return isTRUE;};
};


typedef hCcondList<hCedDispList>	condEdDispList_t;
end stevev 2nov4 */

class hCeditDisplay : public hCitemBase  /* itemType 4 EDIT_DISP_ITYPE */
{
	condReferenceList_t editItems;
	condReferenceList_t dispItems;

protected:  // use aquire...
	RETURNCODE    getItemPtrs(hCreferenceList* pSrcList, varPtrList_t& varPtrList);

public:
//	hCeditDisplay(DevInfcHandle_t h, itemID_t item_id, unsigned long DDkey);
	hCeditDisplay(DevInfcHandle_t h, aCitemBase* paItemBase);

//	CeditDisplay(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~hCeditDisplay(){};
	RETURNCODE destroy(void){return ( editItems.destroy() | dispItems.destroy() | hCitemBase::destroy());};
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*TODO:*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: EditDisplay Attribute information is not accessable.\n");
		return rV;
	};

	// RETURNCODE(setSize() --- Use base class ( 1X1 )

	RETURNCODE getAttrs(void) { LOGIT(CERR_LOG,"CeditDisplay.getAttrs is NOT implemented yet.\n");return FAILURE;};
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		{ hCitemBase::dumpSelf(); LOGIT(COUT_LOG, "EDIT_DISPLAY\n"); return SUCCESS;};
RETURNCODE Label(wstring& retStr) { return baseLabel(EDIT_DISPLAY_LABEL, retStr); }; 
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(EDIT_DISPLAY_LABEL, l); };

RETURNCODE aquireEditList(varPtrList_t& varPtrList);
RETURNCODE aquireDispList(varPtrList_t& varPtrList);

	// // Actions // //
	hCattrBase*  getaAttr(eddispAttrType_t attrType);

	RETURNCODE getMethodList(eddispAttrType_t vat,vector<hCmethodCall>& methList);

	RETURNCODE doPreEditActs(void);
	RETURNCODE doPstEditActs(void);

	bool isChanged(void); // if any ofthe edit vars are changed
#ifdef TOKENIZER
	#include "tokEdDisp.h"
#endif
};


#endif //_DDB_EDITDISPLAY_H

/*************************************************************************************************
 *
 *   $History: ddbEditDisplay.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
