/*************************************************************************************************
 *
 * $Workfile: ddbArray.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
// #define MEMSIZE_CHK 1

#include "ddbArray.h"
#include "ddbGrpItmDesc.h"
#include "logging.h"


#include <algorithm> /* 2015 to get min/max*/


#ifdef MEMSIZE_CHK
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

#include "..\..\SDC625\stdafx.h"

#include <crtdbg.h>
#include <winnt.h>
#include <afx.h>
#undef TRUE
#define TRUE "use true"
#undef FALSE
#define FALSE "use false"
#endif

/* initial fill of array ... pVal set when copy constructor or duplicating */
void hCarray::initTypedef(hCarray* pVal)// get the typedef into the Var..NULL=>use default vals
{
	RETURNCODE  rc = SUCCESS;
	hCitemBase* pDef = NULL, *pNew = NULL;
	int       iLoc;
	int       iTar;
	
	// stevev 25oct11 - we will make this exactly equal to the value array, delete old first
	if (activeIL.size())
	{
		ddbItemList_t::iterator ailIT;
		for (ailIT = activeIL.begin(); ailIT != activeIL.end(); ++ailIT)
		{
			pDef = (*ailIT);
			pDef->destroy();
			delete pDef;
		}
		activeIL.clear();
	}
	if (pVal)
	{
		iTar = pVal->getElemCnt();
		if (iTar != pVal->getSize())
		{
			DEBUGLOG(CERR_LOG,"ERROR: Array inititialize with a value array size mismatch.\n"
							  "       Value array should have %d elements but has %d.\n",
							  iTar,pVal->getSize());
			iTar = pVal->getSize();// we'll use actual size
		}
		if ( getTypedefType() != pVal->getTypedefType() )
		{
			LOGIT(CERR_LOG,	
			"ERROR: Values array typedef type (%s) does not match typedef type (%s) for %s.\n",
				itemStrings[pVal->getTypedefType()],itemStrings[getTypedefType()],
				getName().c_str() );
			iTar = 0;  // do not use pVals
		}
	}
	else
	{
		iTar = 0;// we'll use default values of typedef
	}
	
#ifdef MEMSIZE_CHK
	_CrtMemState s1, s2, s3;   
#endif

	rc = typedefRef.resolveID( pDef );
	if ( rc == SUCCESS && pDef != NULL )// we have a typedef for each element
	{
		iLoc = getElemCnt();
		if ( iLoc > 0 ) // we know how many we are supposed to have
		{
#ifdef MEMSIZE_CHK
			_CrtMemCheckpoint( &s1 );
#endif
			if (iTar != 0 && (iTar != iLoc))// use default
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error- initializing with an Array"
					" that has %d elements instead of %d\n",iTar,iLoc);
				iLoc = min(iLoc,iTar);
			}

			psuedoItemFlags_t flags = sif_RegisterNew;
			elementID_t       thisElem;
			thisElem.contID  = getID();

			for ( int y = 0; y < iLoc; y++)
			{	/* stevev 19oct09 - arrays do not preserve values, does default on each*/
				// stevev 25oct11: I can't find why they don't, we have to for the host test
				if (iTar)
				{
					pNew = (*pVal)[y];//
					flags =(flags & sif_RegisterNew)?sif_registerANDpreserve:sif_PreserveValue;
				}
				else
				{
					pNew = NULL;// use default value
					flags = sif_RegisterNew;
				}
				thisElem.which = y;
				pNew =  devPtr()->newPsuedoItem(pDef, pNew, flags, thisElem);
				if ( pNew != NULL )
				{
					activeIL.push_back(pNew);
				}
#ifdef _DEBUG
				else
				{
					LOGIT(CERR_LOG,"ERROR: no new psuedo item for the type\n");
					break; // out of for loop
				}
#endif
			}// next element
#ifdef MEMSIZE_CHK
			_CrtMemCheckpoint( &s2 );
#endif
		}
		else
		{
			LOGIT(CERR_LOG,
				"ERROR: could not determine Number-Of-Elements for %s\n",getName().c_str() );
		}
	}
	else
	{
		LOGIT(CERR_LOG,
				"ERROR: Typedef reference did not resolve for %s.\n",getName().c_str() );
	}
#ifdef MEMSIZE_CHK
	if ( _CrtMemDifference( &s3, &s1, &s2 ) )
	{
		_CrtMemDumpStatistics( &s3 );
	}
#endif
}


hCarray::hCarray(DevInfcHandle_t h, aCitemBase* paItemBase) : 
							hCitemBase(h,paItemBase),typedefRef(h), condElemCnt(h),pTypeDef(NULL)
{// label, help & validity is set in the base class
	pTypeDef = NULL;

	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(arrayAttrType); // required
	if (aB == NULL)
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCarray::hCarray<no type attribute supplied.>\n"
		               "       trying to find: %d but only have:",(int)arrayAttrType);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG,"%u, ", (*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ // the typedef ref is not allowed to be conditional
		aCattrCondReference* paCr = (aCattrCondReference*)aB;
		if ( paCr->condRef.priExprType != eT_Direct                ||
			 paCr->condRef.destElements.size() == 0                ||
			 paCr->condRef.destElements[0].destType != eT_Direct   ||
			 paCr->condRef.destElements[0].pPayload->whatPayload() != pltReference)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: typedef reference is invalid.");
		}
		else
		{
			typedefRef = *((aCreference*)paCr->condRef.destElements[0].pPayload);
		}
	}

	
	aB = paItemBase->getaAttr(arrayAttrElementCnt); // required
	if (aB == NULL)
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCarray::hCarray<no element-count attribute supplied.>\n"
		               "       trying to find: %d but only have:",(int)arrayAttrElementCnt);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG,"%u, ", (*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 	condElemCnt.setEqualTo( &(((aCattrCondLong*)aB)->condLong) );
	}
}

/* from-typedef constructor */
hCarray::hCarray(hCarray* pSrc, hCarray* pVal, itemIdentity_t newID):
		 hCitemBase((hCitemBase*)pSrc,newID),typedefRef(pSrc->devHndl()),
											 condElemCnt(pSrc->devHndl())
{
	pTypeDef = NULL;
	typedefRef  = pSrc->typedefRef;
	condElemCnt = pSrc->condElemCnt;

	initTypedef(pVal);// we gotta hope that everything we need is already instantiated.
}


itemType_t    hCarray::getTypedefType(void)
{	
	if ( pTypeDef == NULL )
	{
		RETURNCODE rc;
		rc = typedefRef.resolveID( pTypeDef );
		if ( rc != SUCCESS || pTypeDef == NULL )
		{// we failed
			pTypeDef = NULL;
		}
	}
	if ( pTypeDef != NULL )
	{
		return (pTypeDef->getIType());
	}
	else
	{// error
		return iT_NotAnItemType;
	}
}
		 
RETURNCODE hCarray::getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = SUCCESS;
	
	if ( indexValue < activeIL.size() )
	{
		hCreference localRef(devHndl());
		// make a reference from activeIL[indexValue]
		localRef.setRef(activeIL[indexValue]->getID(),0,
					    activeIL[indexValue]->getTypeVal(),false,rT_Item_id);
		returnedItemRefs.push_back(localRef);
		rc = SUCCESS;
	}
	// else no match to index - do nothing, return success

	return rc;
}

RETURNCODE hCarray::getAllindexValues(vector<UINT32>& allValidindexes)
{
	int y = (getSize()>getElemCnt()) ?getSize() :getElemCnt();// max()
	for ( int i = 0; i < y; i++ ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		allValidindexes.push_back(i);
	}
	return SUCCESS;
}


RETURNCODE hCarray::getByIndex(UINT32 indexValue,hCgroupItemDescriptor** ppGID,bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( activeIL.size() > 0 && indexValue >= 0 && indexValue < activeIL.size() )
	{
		ptr2hCitemBase_t pIb = activeIL[indexValue];
		if ( pIb != NULL && pIb->getIType() != 0 && pGid != NULL )
		{
			hCreference locRef(devHndl() );
			locRef.setRef(pIb->getID(), 0, pIb->getTypeVal(), false,rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(indexValue);
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Array Item getByIndex failed to find index '0x%02x" 
									"' in %d elements.\n",indexValue,activeIL.size());
		}
		rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}


CValueVarient hCarray::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;

	arrayAttrType_t aat = (arrayAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( aat >= arrayAttr_Unknown )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Array's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	switch (aat)
	{
	case arrayAttrLabel:
	case arrayAttrHelp:
	case arrayAttrValidity:
		rV = hCitemBase::getAttrValue(aat);		
		break;
	case arrayAttrType:
		{
			if ( pTypeDef == NULL )
			{
				RETURNCODE rc;
				rc = typedefRef.resolveID( pTypeDef );
				if ( rc != SUCCESS || pTypeDef == NULL )
				{// we failed
					pTypeDef = NULL;
				}
			}
			if ( pTypeDef != NULL )
			{
				rV.vValue.varSymbolID = pTypeDef->getID();
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Array's getAttrValue did not resolve type.\n");
				rV.vValue.varSymbolID = 0;
			}
			rV.vType = CValueVarient::isSymID;
			rV.vSize = 4;// default
			rV.vIsValid = true;
			rV.vIsUnsigned = true;
		}
		break;
	case arrayAttrElementCnt:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = false;

			hCddlLong L(devHndl());

			if ( condElemCnt.resolveCond(&L) != SUCCESS )
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid = false;
			}
			else
			{
				rV.vValue.iIntConst = (INT32)L.getDdlLong();// array size is limited value set
			}
		}
		break;
	case arrayAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			//LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
			LOGIT(CERR_LOG,ERR_IN_PROGRAM,__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}


RETURNCODE hCarray::getList(ddbItemList_t& r2ItemLst )
{
//	r2ItemLst = activeIL; 
	for ( ddbItemLst_it anIT = activeIL.begin(); anIT != activeIL.end(); ++anIT )
		{ r2ItemLst.push_back( *anIT );	}
	return (r2ItemLst.size() != activeIL.size());
}


hCitemBase* hCarray::operator[](int idx0)
{
	if ( idx0 < 0 || idx0 > (int)(activeIL.size()-1) ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		return NULL;// exception
	}

	return activeIL[idx0];// a list of pointers, this returns the pointer
}

RETURNCODE hCarray::destroy(void)
{ 
	if(!sharedAttr)
	{
		typedefRef.destroy(); 
	
		return condElemCnt.destroy(); 
		/*return hCVar::destroy();*/
	}
	else
	{
		return SUCCESS;
	}
	
	hCitemBase * pDef;
	ddbItemList_t::iterator ailIT;
	for (ailIT = activeIL.begin(); ailIT != activeIL.end(); ++ailIT)
	{
		pDef = (*ailIT);
		pDef->destroy();
		delete pDef;
	}
	activeIL.clear();
}
	

void hCarray::MakeStale(void)
{	
	RETURNCODE rc = SUCCESS;
	ddbItemList_t locList;
	ptr2hCitemBase_t pItm;
	ddbItemLst_it   ItItm;

	rc = 	getList(locList );

	if (rc == SUCCESS && locList.size() > 0 )
	{// ptr2ptr2itembase
		for (ItItm = locList.begin(); ItItm != locList.end(); ++ ItItm)
		{
			pItm = (ptr2hCitemBase_t)(*ItItm);
			pItm->MakeStale();
		}
	}
	return;
}

/* stevev 22jan09 */
RETURNCODE hCarray::setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase* pIB = NULL;

	for ( ddbItemLst_it anIT = activeIL.begin(); anIT != activeIL.end(); ++anIT )
	{ 
		pIB = (hCitemBase*) *(&(*anIT));// ptr2ptr2hCitemBase
		rc |= pIB->setCommandIndexes(indexPtrList,newIdxValue);	
	}
	return rc;
}

/* stevev 22jan09 */
RETURNCODE hCarray::addCmdDesc(hCcommandDescriptor& rdD, bool isRead)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase* pIB = NULL;

	for ( ddbItemLst_it anIT = activeIL.begin(); anIT != activeIL.end(); ++anIT )
	{ 
		pIB = (hCitemBase*) *(&(*anIT));// ptr2ptr2hCitemBase
		rc |= pIB->addCmdDesc(rdD,isRead);	
	}
	return rc;
}
