/*************************************************************************************************
 *
 * $Workfile: ddbCommInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the interface to the send command / receive command external functionality.
 *
 *		
 * #include "ddbCommInfc.h".
 */

#ifndef _DDBCOMMINTERFACE_H
#define _DDBCOMMINTERFACE_H

#include "pvfc.h"
#include "ddbGeneral.h"
#include "ddbEvent.h"

#include "HARTsupport.h"
#include "ddbSvcRcvPkt.h"
#include "ddbMsgQ.h"

/* stevev 2/26/04
	we have to delay screen updates until post actions are complete.
	the following enum give notifyVarUpdate mor lattitude in handling
	the message data, specifically holding information till after actions.
	This requires a pre-extract call to warn of post actions, followed by the
	normal extract calls for each variable, followed by a post action call
	to enable the updates.
*/
// MOVED TO GENERAL.H
//typedef enum notifyUpdateAction_e
//{
	//NO_change,	/* 0 */
	//IS_changed,
	//STRT_actPkt,
	//END_actPkt
//}
//typedef NUA_t;
/* end stevev 2/26/04 */

// define the commInfc parent class

// the interface (supplied by the application) to the i/o channel to the device
// an abstract base class - MUST be subclassed
class hCcommInfc
{
	int  MutexWaitTime;
	int  LongMutexWaitTime;
	int  MessagePendTime;

protected:
	hCsvcRcvPkt* cpRcvService; // class pointer into the dispatcher for a single function
	int  moreStatusRepression; // the user-set number of MoreStatus to skip before sending cmd48

	/* stevev 18nov05 - handle structure changes *xx/
	itemIDlist_t pstActPktList;	// hold value pkts
	itemIDlist_t pstActStrList; // hold structure  pkts
	moved to DDLBaseComm.cpp as static ..31may11 xx***/

	/* stevev 2/26/04 - replaced with above 18nov05 *  /	
	vector<hCVar*> pstActPktList;  end replacement */

	bool inActionPkt; // storing
	/* stevev 23jan06 - try to stack the action packets */
	int  ActionPktEntryCnt;

	/* stevev 24jan06 - added to restrict method changes to structure */
	bool inMethodPkt; 
	int  MethodPktEntryCnt;

	/* end stevev 02.26/04 */
public:	

	int maxEFFversionSupported; // temp for tok to pass in a value

	hCcommInfc();/*{cpRcvService=NULL;moreStatusRepression=50; pstActPktList.clear();
	             inActionPkt=false;  MutexWaitTime=REASONABLE_TIME;
				 LongMutexWaitTime = 2*REASONABLE_TIME;  MessagePendTime = 3 * REASONABLE_TIME;
				 /x* stevev 23jan06 - try to stack the action packets *x/
				 ActionPktEntryCnt = 0;  MethodPktEntryCnt=0;inMethodPkt=false; };
	moved to DDLBaseComm.cpp	*** *** *** *** *** *** ***/
	virtual ~hCcommInfc(){ 
		RAZE(cpRcvService);		};
	virtual									      // sets the dispatcher's service routine
		void  setRcvService(hCsvcRcvPkt* cpRcvSvc) {cpRcvService = cpRcvSvc; };
	virtual									      // sets the dispatcher's service routine
		hCsvcRcvPkt*  getRcvService( void) {return (cpRcvService); };
	virtual
		void  setStatusRepressionCount(int skipNumber){moreStatusRepression=skipNumber;};
	virtual
		int   getStatusRepressionCount(void){return moreStatusRepression;};
														
	virtual											// 'PUSH INTERFACE'
		void  notifyAppVarChange(itemID_t changedItemNumber, NUA_t isChanged = NO_change, long aTyp = 0)PVFC( "hCcommInfc_A" );
	virtual 
        void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle) PVFC( "hCcommInfc_B" );// call back for windows actions

	virtual
		short setNumberOfRetries( short newVal ) RPVFC( "hCcommInfc_O",0 );// returns previous val (for methods)

	virtual
		hPkt* getMTPacket(bool pendOnMT) RPVFC( "hCcommInfc_C",NULL );// gets an empty packet for generating a command 
									        //		(gives caller ownership of the packet*)
	virtual
		void  putMTPacket(hPkt*)PVFC( "hCcommInfc_D" );	// returns the packet memory back to the interface 
									//		(this takes ownership of the packet*)
	virtual
		RETURNCODE
			SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "hCcommInfc_D",0 );
						 // puts a packet into the send queue  (takes ownership of the pkt*)
	virtual
		RETURNCODE 
			SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "hCcommInfc_E",0 );
			// puts a packet into the front of the send queue(takes ownership of the pkt*)
	virtual
		RETURNCODE		
			SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)RPVFC( "hCcommInfc_F",0 );
	virtual
		int sendQsize(void)RPVFC( "hCcommInfc_G",0 ); 	        // how many are in the send queue ( 0 == empty )
	virtual
		int	sendQmax(void) RPVFC( "hCcommInfc_H",0 );		    // how many will fit
	virtual
		RETURNCODE  initComm(void) RPVFC( "hCcommInfc_I",0 );  // setup connection (called after instantiation of device)
	virtual
		RETURNCODE  shutdownComm(void)RPVFC( "hCcommInfc_J",0 ); // de init in a non destructive environment
	virtual		
		void  disableAppCommRequests(void) PVFC( "hCcommInfc_K" );	// handle the Application's updates during methods
	virtual		
		void  enableAppCommRequests (void) PVFC( "hCcommInfc_L" );
	virtual
		bool  appCommEnabled (void) RPVFC( "hCcommInfc_M",0 );  // returns current value of above
	virtual															// All will have to do this
		RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr) RPVFC( "hCcommInfc_N",0 );

	/* Pend Timing additions 3/26/04 stevev */
	// put into accessors because we may need to have protection on them in the future
	int   getMutexTime(void)         { return(MutexWaitTime);     };
	void  setMutexTime(int newT)     { MutexWaitTime = newT;      };
	int   getLongMutexTime(void)     { return(LongMutexWaitTime); };
	void  setLongMutexTime(int newT) { LongMutexWaitTime = newT;  };
	int   getMessageTime(void)       { return(MessagePendTime);   };
	void  setMessageTime(int newT)   { MessagePendTime = newT;    };
};


#endif//_DDBCOMMINTERFACE_H

/*************************************************************************************************
 * NOTES:
 *   There is a single communication interface class (hCcommInfc) instantiation for each device.  
 * IT IS THE RESPONSIBILITY OF THIS CLASS TO KNOW THE ADDRESS OF THE DEVICE.
 * Code that use these public functions believe that it is the only client to the device server.
 * This is a near virtual base class and is REQUIRED to be subclassed by the application.
 * That subclass may keep the address and path to the device in any form it sees fit.
 * 5/13/03 - Push interface
 *		notifyAppVarChange   added to implement the 'push interface'
 * It is located here to force the app to implement something...even an empty method.
 * This will be called when an item has changed.  It may or may not require the item to repaint.
 * THis is mainly called by the vars when a value changes from the device, most consistently with 
 * dynamic variables.  Note that the implementer must determine how to notify the correct device
 * with the item id.  It may be extended to include other item types if a variable they depend on
 * changes (ie a variable in a conditional).
 * The act of calling this means the real value has been updated.
 * The item id is the item (usually a var) that was updated.
 * The ischanged bool tells whether the value written to the realValue matched the previous value.
 *	(This is included as a hint to the Application so it can intelligently decide what to paint.)
 *************************************************************************************************
 */


/*************************************************************************************************
 *
 *   $History: ddbCommInfc.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:23a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
