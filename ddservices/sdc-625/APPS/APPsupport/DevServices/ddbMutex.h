/*************************************************************************************************
 *
 * $Workfile: ddbMutex.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the api to an OS specific semaphore mechanism.
 *		The application must generate the code (usually by subclassing this class)
 *
 * #include "ddbMutex.h"
 */

#ifndef _DDBMUTEX_H
#define _DDBMUTEX_H

#include "ddbGeneral.h" // includes vector
#include "ddbEvent.h"
#include <map>
#include "xplatform_thread.h"
#include "logging.h"

extern THREAD_ID_TYPE getTHID(void);

// designed to encapsulate the OS specific items - linked externally
class hCmutex		
{   // a non-counting semaphore (only one task can own it)
	
	int mutexHandle;// private:  this is where the internal data is hidden

public:
	class hCmutexCntrl	// one per task needing control of the resource
	{	
		int controlHandle; // private - hidden
#ifdef _DEBUG
		string userName;
	public:
		hCmutex* pParent;
		hCmutexCntrl(int h,string useNm);
#else
	public:
		hCmutex* pParent;
		hCmutexCntrl(int h);
#endif
		~hCmutexCntrl()// stevev - try to find where Bill lost it.
		{ 
			controlHandle = 0;
			pParent =0;
		};

		// returns APP_WAIT_TIMEOUT at timeout || APP_WAIT_ERROR at error || SUCCESS 
#ifndef _DEBUG
		RETURNCODE aquireMutex(unsigned long ulTimeOut = hCevent::FOREVER );	// wait for mutex (mS time)
#else /* is DEBUG */
		RETURNCODE aquireMutex(unsigned long ulTimeOut = hCevent::FOREVER, char* fileStr = __FILE__, int lineNum = __LINE__ );
#endif
								
		void relinquishMutex(void);	// allows others to use it                        
	};

public:	// each task using that event will need to getMutexControl before accessing the protected resource

	hCmutex();

	THREAD_ID_TYPE HolderThdID;
#ifdef _DEBUG	
	char    HolderFile[LOG_MAX_PATH];
	int     HolderLine;
	THREAD_ID_TYPE fetchCurrentThreadId ();//to help in debugging
#endif
	// string is unused in release mode
	hCmutexCntrl* getMutexControl(char* pNm);		// executes control constructor delete when done

	void returnMutexControl(hCmutex::hCmutexCntrl* pmc);// stevev 23aug10 - try to stop 2008 from trashing stack
	virtual ~hCmutex();
};





#endif//_DDBMUTEX_H

/*************************************************************************************************
* NOTES:
**************************************************************************************************
*/


/*************************************************************************************************
 *
 *   $History: ddbMutex.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:29a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
