/*************************************************************************************************
 *
 * $Workfile: ddbDeviceMgr.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the singleton device manager and it's static instantiation
 *		03/14/03	sjv	started creation
 */

#ifndef __GNUC__ // L&T Modifications
// added by L&T: To remove error C3861: 'GetCurrentThreadId': identifier not found
#include <Windows.h>
// ended
#endif // L&T Modifications

#ifdef _WIN32_WCE
//#define LOAD_DEVICE_FROM_FILE	// attempted to save device to disk after created - failed PAW 31/07/09
 #ifdef LOAD_DEVICE_FROM_FILE	// PAW 31/07/09
  #include <atlconv.h>	// for USES_CONVERSION
  #include <atlcomcli.h>
  #include <iostream>
  #include <fstream>
 #endif
#endif

#ifdef _DEBUG
//#define DUMP_DEBUG
#endif
#include "ddbDeviceMgr.h"
#include "ddbDevice.h"
// no longer needed (it's empty)#include "ddb_UIInfc.h"

#include "Dictionary.h"
#include "ddbParserInfc.h"
#include "ddbFormat.h"
#include "ddbFormatHCL.h"
#include "logging.h"

extern char chInputFileName[];// in ddbparserinfc.cpp

#ifdef DUMP_DEBUG
extern void dumpDev(aCdevice* pDev);
#endif

#ifdef _DEBUG
extern int isInstantiated;
//#define STARTTIME_CHECK /* also in ddbDevice.cpp, ddbCommand.cpp, sdc625treeview.cpp */
unsigned objCnt = 0;
int thrdHndl[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int thrdCntr[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int   getHndl(void)
{
	DWORD wrkingThreadID = GetCurrentThreadId();
	int r = 0;
	while( r < 16 )
	{
		if (thrdHndl[r]==0)
		{
			thrdHndl[r] = wrkingThreadID;
			return r;
		}
		else
		if ( thrdHndl[r] == wrkingThreadID )
		{
			return r;
		}
		else
		{
			r++;
		}
	}//wend
	return -1;//force an error
}
void incThrdCnt(void)
{
	objCnt++;
	int h = getHndl();
	thrdCntr[h]++;
}
void decThrdCnt(void)
{
	objCnt--;
	int h = getHndl();
	thrdCntr[h]--;
}

#endif

#ifdef STARTTIME_CHECK
extern DWORD startOfInstantiation, lastTickCnt, thisCnt;
#endif

extern INT8  entryCount; // in ddbParserInfc.cpp

// port number to item number map - valid for all devices
static std::map<unsigned char, unsigned long> port2item;
//global - gives the official mapped data item if it exists
unsigned long getDataItem2Transfer(unsigned char uc)
{
	std::map<unsigned char, unsigned long>::iterator pItemPair;
	pItemPair = port2item.find(uc);
	if (pItemPair == port2item.end() )
	{
		return 0;
	}
	else
		return pItemPair->second;
};


/* global function to instantiate the correct format */
hCformatBase* getNewFormat(FileFormat_t ft)
{
	hCformatBase* pR = NULL;

	switch ( ft )
	{
	default:
	case FMT_Undefined:
		{
			ft = FMT_Undefined; // in case of default
			pR = (hCformatBase*) new hCundefinedFormat;
		}
		break;
	case FMT_HCFformat:
		{
			pR = (hCformatBase*) new hChclFormat;
		}
		break;
	case FMT_Unsupported:
		{
			pR = (hCformatBase*) new hCunSupportedFormat;
		}
		break; 
	}
	pR->myType = ft;
	return pR;
};

/* Dictionary */
CDictionary *dictionary;
extern char chDbdir[];// a global in the ddbParserInfc.cpp

//  STATIC         The device manager's existance is static
//                 part of the singleton infrastructure
hCDeviceManger*  hCDeviceManger::pSelf      = NULL;
char             hCDeviceManger::language[LANG_LEN] = {0};


static hCerrDeviceSrvc errorDeviceSrvc;             // static - meaning module access only
static hCddbDevice * pSDCdevice = NULL;				// sdc device is available to all items

hCddlString* pSysStrings = NULL;			// used for getting strings out of the sdc device

// insert always - not always used...#ifdef QDBG
int Qhandle = 0;

/* protected constructor */
/* instantiate() constructs this and then does the rest of the startup */
hCDeviceManger::hCDeviceManger()
{
	DBnotAvailable = 1; // true - not available ...yet
//	nextHandle     = 1; // 0 and neg are illegal
	/*Vibhor 041203: Start of Code*/
	nextHandle     = SDC_DEVICE_HANDLE+1;// stevev 31jul08 - new reserved handle STANDARD_DEVICE_HANDLE + 1 ; // 0 and neg are illegal, 1 is reserved for the Std Tables
	/*Vibhor 041203: Start of Code*/
	/* stevev 06jul05 */
	dmDate_t ldt = {0,0,0};
	setStartDate(ldt);
	dictionary = NULL; // J.U. this is checked in the destructor
}

hCDeviceManger::~hCDeviceManger()
{
	if ( ! DBnotAvailable )// is available
	{// last one out turns off the lights
		releaseDB();
		closeDB();
		DBnotAvailable = true;
/*Vibhor 041203: Start of Code*/		


		if (dictionary != NULL)		//J.U. check if the dictionary is created. 
			delete dictionary; // Dictionary Cleanup
		dictionary = NULL;
	
/*Vibhor 041203: End of Code*/				
	}
}

/* static */
hCdeviceSrvc* hCDeviceManger::getDevicePtr(DevInfcHandle_t myHandle)
{
	hCdeviceSrvc* pDS     = &errorDeviceSrvc;
	if (pSelf != NULL)
	{
		map<DevInfcHandle_t, hCdeviceSrvc*>::iterator mapPos;
		// lookup the device in the registry and return the correct ptr
		mapPos = pSelf->deviceMap.find(myHandle);
		if ( mapPos != pSelf->deviceMap.end() )
		{
			pDS = (mapPos->second); //mapPos should be a ptr2ptr2hCdeviceSrvc
		}
		else // points to deviceMap.end() iterator
		{
			pDS= NULL;
		}
		if ( pDS == NULL ) // a map content error or not found
		{
			pDS     = &errorDeviceSrvc;
		}
	}
	// just return the error
	return pDS;
}


/* static */
int 	hCDeviceManger::IsdbNOTavailable(void)
{
	if ( pSelf == NULL )
	{
		return (1); // true - it is NOT available
	}
	else
	{
		return (pSelf->DBnotAvailable);
	}
}


/* static */
void   hCDeviceManger::destantiate() // external deletion
{
/*Vibhor 120504: Start of Code */
	if (pSelf)
	{
		if(pSelf->deviceMap.size() > 1)
		{
			LOGIT(CLOG_LOG,
					"Device Manager getting destantiated with more than one device left\n");
		}
		//else
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Deleting Standard Devices ......\n");
			hCdeviceSrvc *pStdDev = getDevicePtr(STANDARD_DEVICE_HANDLE);
			if(NULL != pStdDev )
			{
				RETURNCODE rc = SUCCESS;
				if ( pStdDev->getDDkey() != 0 )// error device gives zero
					rc = pSelf->deleteDevice(&pStdDev);
				if(SUCCESS != rc)
				{
					LOGIT(CERR_LOG,"Deletion of Std Device returned rc =  0x%04x\n", rc);
				}
			}
			else
			{
				LOGIT(CERR_LOG,"hCDeviceManger::destantiate(): Got a NULL Std Device Ptr !!!\n");
			}
			pStdDev = getDevicePtr(SDC_DEVICE_HANDLE);
			if(NULL != pStdDev )
			{
				RETURNCODE rc = SUCCESS;
				if ( pStdDev->getDDkey() != 0 )// error device gives zero
					rc = pSelf->deleteDevice(&pStdDev);
				if(SUCCESS != rc)
				{
					LOGIT(CERR_LOG,"Deletion of SDC Device returned rc =  0x%04x\n", rc);
				}
			}
			else
			{
				LOGIT(CERR_LOG,"hCDeviceManger::destantiate(): Got a NULL Sdc Device Ptr !!!\n");
			}
		}
		
		delete pSelf;
		pSelf = NULL; // stevev 17sep07 for petr.
	}
/*Vibhor 120504: End of Code */
}

/* static */
hCDeviceManger* hCDeviceManger::instantiate(const char* pDbdir, 
											const char* pLang, const char* pAppDir)
{	
/*Vibhor 041203: Start of Code*/
//	Indentity_t stdTblIdentity;
	DD_Key_t tempKey = 0;			
/*Vibhor 041203: End of Code*/	
	RETURNCODE rc = SUCCESS;

	if ( pSelf == NULL )
	{
		pSelf = new hCDeviceManger();
	}
	// else - check the database
	if ( pSelf == NULL )
	{// new manager failed
		// LOGIT error message
		LOGIT(CERR_LOG | UI_LOG,
		"ERROR: Device manager failed to construct. (memory error)");
		//cerrxx << "ERROR: +++ Device Manager did not construct!" << endl;
		// null will be returned
	}
	else if (pSelf->DBnotAvailable)
	{// finish instantiation using pSelf

		pSelf->DBnotAvailable = initDB(pDbdir);	//verifies and sets release dir
		if ( pSelf->DBnotAvailable != SUCCESS )
		{
			LOGIT(CERR_LOG | UI_LOG,
			"ERROR: Device manager could not verify the device library at \n%s.(%d)\n",
			pDbdir, pSelf->DBnotAvailable);
			//cerrxx << "ERROR: +++ database did not start up +++"<< pDbdir << endl;
			// we can do nothing in this situation
			delete pSelf;
			pSelf = NULL;
			// null will be returned
		}
		if (pLang)
			strncpy((char*)(&(language[0])),pLang,LANG_LEN);
		/* commented code removed 15oct08 - see previous versions for content */
	}
	// else - mgr is OK, DB OK...just return a pointer
	// endif - successful new manager
	return pSelf;
}

	
// Note that it is the caller's responsibility to identify devices on the network,
//		read their cmd 0s, select the one to instantiate and call this method w/ identity
//		(as per WAP)
// A value in the incomingKey will overide anything in the identity!
// A value in pThisCommInterface will only be used when pCD is NULL (default dispatcher)
//
// try the below...hCdeviceSrvc* hCDeviceManger::newDevice(Indentity_t newIdent, 
/* stevev 15oct08 - cleanup: remove commented out code - see earlier versions for content
 *	added subroutines for xxx handling and loop-warning variable handling
 *	modified hCDeviceManager to instantiate a dictionary only once and load the SDC standard
 *	at that time (once).  This is now re-entrant to handle the standard device!
 * 31jul09 - the only way to load the standard table device is with a null identity & all else)
 */
hCddbDevice* hCDeviceManger::newDevice(Indentity_t newIdent,
										hCFileSupport*   pFileSupport,
										hCMethSupport*   pMethSupportInterface,									 
										hCcommInfc*      pThisCommInterface,
										hCcmdDispatcher* pCD /* = NULL */
/* 26sep07 reinstated as the best way to pass the info*/ ,DD_Key_t incomingDDkey /*=0*/
/* 11nov08 add a way to detect the tokenizer */          ,bool isInTokenizer /*=false*/  )
{
	RETURNCODE      rc = SUCCESS;
	DD_Key_t        wrkingKey = 0;
	Indentity_t     stdTblIdentity;
	DD_Key_t        tempKey       = 0;	
	LitStringTable* locPtr2LitStrTbl = NULL;

	hCddbDevice*    pDev    = NULL;
	hCdeviceSrvc*   pDS     = NULL;	
	DevInfcHandle_t newHndl = 0;
	bool standardDevice = (pFileSupport == NULL) && (pMethSupportInterface == NULL) &&
						  (pThisCommInterface == NULL) && (pCD == NULL) && (incomingDDkey == 0);
	bool sdcDevice      = standardDevice && !newIdent.isEmpty();
	if (standardDevice && !sdcDevice)
		newHndl = STANDARD_DEVICE_HANDLE;
	else if (standardDevice && sdcDevice)
		newHndl = SDC_DEVICE_HANDLE;
	else
		newHndl = nextHandle++;


	// instaniate an empty device here
	pDev = new hCddbDevice(newHndl);
#ifdef _WIN32_WCE
#ifdef LOAD_DEVICE_FROM_FILE	// PAW 31/07/09
		ofstream out_dictf;
		HANDLE tempHandle;
		WIN32_FIND_DATA FindFileData;
		int pCurDevSize = 0;
		char InputFileName[50] = {"SDCard\\3144.dev"};

		USES_CONVERSION;
		CComBSTR combstr(InputFileName);
		LPTSTR   szTemp    = OLE2T(combstr.m_str);
		HANDLE   DirSearch =::FindFirstFile(szTemp, &FindFileData );


		if(DirSearch!= INVALID_HANDLE_VALUE)
		{
			pCurDevSize = FindFileData.nFileSizeLow;

			ifstream in_dictf( "SDCard\\3144.dev" );
			if( in_dictf ) 
			{
				in_dictf.read( (char *)pDev, pCurDevSize );
		    }
			FindClose(tempHandle);
		}

#endif
#endif

	if ( pDev != NULL )
	{
		pDS = (hCdeviceSrvc*)pDev;
		if ( IsdbNOTavailable() )
		{	// this should already be an error	
			LOGIT(CERR_LOG | UI_LOG,
				"ERROR: Device manager could not generate a new device.(no binary available)");
			delete pDev;
			LOGIF(LOGP_MISC_CMTS) (CLOG_LOG,"> deviceMgr does not have a DB -  leaving.\n");	
			return NULL; // error exit	
		}
		// else - ready to go
		
		StrVector_t* pRemoteStr = NULL;
		
		// stevev 20aug07 - xmtr-dd needs to get info from file list...convert key to identity
		if ( newIdent.isEmpty() )
		{
			if (incomingDDkey != 0)
			{// leave newIdent.cInternalFlags as they are found
				newIdent.wManufacturer = (WORD) ((incomingDDkey >> 48) & 0x000000000000ffff);
				newIdent.wDeviceType   = (WORD) ((incomingDDkey >> 32) & 0x000000000000ffff);
				newIdent.cDeviceRev    = (UINT8)((incomingDDkey >> 16) & 0x00000000000000ff);
				newIdent.cSoftwareRev  = (UINT8)( incomingDDkey        & 0x00000000000000ff);
			}// else it is zero
			else
			if (newHndl == STANDARD_DEVICE_HANDLE)
			{
				newIdent.wManufacturer = 0x0000;
				newIdent.wDeviceType   = 0x0001;
				newIdent.cDeviceRev    = 0xff;// ff is search for latest
				newIdent.cSoftwareRev  = 0xff;// ff is search for latest 
				newIdent.cUniversalRev = 0x05;// use 1 byte mfg search technique(12aug10 6 to 5)
			}
			// else - we haven't a clue what we are supposed to be loading as a device
			//		- so we'll trap in the identifyDDfile
		}
		// else use the identity as passed in

		// verify that the file exists
		entryCount = 0; // just in case
		if ( identifyDDfile(newIdent, wrkingKey, &pRemoteStr) != SUCCESS )
		{			
			LOGIT(CERR_LOG | UI_LOG,
				"ERROR: Device manager could not identify a file for the new device.\n");
			RAZE( pDev );
			return NULL; // error exit
		}

		// move the file strings to the device
		if (pRemoteStr != NULL )// identifyDDfile filled the strings
		{// file the device strings
			string locStr; 

			for(StrVector_t::iterator iT = pRemoteStr->begin(); iT <  pRemoteStr->end();iT++ )
			{// iT is a ptr to a string in the dll memory area
				locStr = iT->c_str(); // use c_str to force a content copy
				pDev->DeviceDescStrings.push_back(locStr);
			}
			releaseStringVect(&pRemoteStr);
		}
		/*  stevev 24mar09 - To make the 'Available DDs' dialog work, we have to have the standard
		 *                   tables loaded.  Instead of loading it as needed, its much easier to
		 *                   load it here, while we are loading up.
		 */
		// do the following only once for the device manager
		if (dictionary == NULL)// make a dict ONCE	
		{
			dictionary = new CDictionary(&(language[0])); // stevev 14mar08

			/* stevev 14mar08 - add Load supporting files  for pre fm8 files */
			if ( eFFVersionMajor < 8 )
			{// then we have to load extra stuff
				LOGIF(LOGP_MISC_CMTS) (STAT_LOG|CLOG_LOG,"Getting Dictionary");
				rc = buildDicitonary(dictionary);  // uses chDbdir to open and fill
				if(rc != SUCCESS)
				{
					LOGIT(CERR_LOG | UI_LOG,
						"ERROR: Device manager could not load the dictionary '%s'",chDbdir);
					// we can do nothing in this situation
					RAZE( pDev );
					RAZE( dictionary );
					return NULL;			
				}
			}
		}
		if ( ! standardDevice )// load common files ONCE	
		{			
			if ( ! isInTokenizer && eFFVersionMajor < 8)//this is loaded on import within the tok
			{											// not needed with 8+ version
				// Load the Standard tables after the dictionary and BEFORE the SDC dd
				// we now do this for all DDs w/ version < 8
				//check to see if somebody else loaded it
				hCddbDevice *stdDev = (hCddbDevice*)getDevicePtr(STANDARD_DEVICE_HANDLE);
				// getDevicePointer never returns NULL stevev 19apr13 if ( stdDev == NULL )
				if (stdDev->isDDKeyPresent == false || stdDev->getDDkey() == 0 )// error device gives zero
				{
					//stdTblIdentity.wManufacturer = 0x0000;
					//stdTblIdentity.wDeviceType   = 0x0001;
					//stdTblIdentity.cDeviceRev    = 0xff;// ff is search for latest
					//stdTblIdentity.cSoftwareRev  = 0xff;// ff is search for latest 
					//stdTblIdentity.cUniversalRev = 0x06;// use 1 byte mfg search technique
					stdTblIdentity.clear();

					LOGIF(LOGP_MISC_CMTS)(STAT_LOG|CLOG_LOG,"Loading Standard Tables");

					//rc = identifyDDfile(stdTblIdentity,tempKey);// find standards
				//if(rc == SUCCESS)
				//{// push the globals
					UINT8 holdFileVersionMaj = eFFVersionMajor;
					UINT8 holdFileVersionMin = eFFVersionMinor;
					char  holdFileName[MAX_FILE_NAME_PATH];
					memcpy(holdFileName, chInputFileName,MAX_FILE_NAME_PATH);
					// re-enter to load the standards
					hCddbDevice *stdDev = newDevice(stdTblIdentity,NULL,NULL,NULL,NULL,0,isInTokenizer);
				//	hCddbDevice *stdDev = stdDevice(stdTblIdentity,NULL,NULL,NULL,NULL);
					if(NULL == stdDev)
					{
						LOGIT(CERR_LOG | UI_LOG,
							"ERROR: Device manager could not load the Standard Tables");
						RAZE( pSelf );
						return NULL;// don't crash when we cease to exist
					}				
					LOGIT(CLOG_LOG,"Started Standard Tables Device\n");
					// pop the globals
					eFFVersionMajor = holdFileVersionMaj;
					eFFVersionMinor = holdFileVersionMin;
					memcpy(chInputFileName, holdFileName, MAX_FILE_NAME_PATH);
				//}
				//else
				//{
				//	LOGIT(CERR_LOG | UI_LOG,
				//		"ERROR: Device manager could not find the Standard Table DD");
				//	RAZE( pSelf );
				//	return NULL; // don't crash when we cease to exist
				//}
				}// else somebody has already loaded the standard tables
			}

			// stevev 02jul09 - always load the sdc device !!!
			if ( eFFVersionMajor >= 0 )// I want to load the standard table before the SDC
			{	// literal string table is now alloc'd for every device (outside of if(dict) )
				// dictionary will be filled when the DD is loaded (for fm8s)

				/* now we need the sdc dd added 01may08 by stevev */
				stdTblIdentity.wManufacturer = SDC_DD_MFG;
				stdTblIdentity.wDeviceType   = SDC_DD_DTY;
				stdTblIdentity.cDeviceRev    = SDC_DD_DRV;// load the one we are designed for
				stdTblIdentity.cSoftwareRev  = 0xff;// ff is search for latest 
				stdTblIdentity.cUniversalRev = 0x06;// use 1 byte mfg search technique
				stdTblIdentity.cInternalFlags= intFlgs_Do_NOT_load_Dflt;
				LOGIF(LOGP_MISC_CMTS)(STAT_LOG|CLOG_LOG,"Loading SDC base DD\n");

				entryCount = 0; // just in case
				rc = identifyDDfile(stdTblIdentity,tempKey);
				if(rc == SUCCESS)// exists
				{// push the globals
					UINT8 holdFileVersionMaj = eFFVersionMajor;
					UINT8 holdFileVersionMin = eFFVersionMinor;
					char  holdFileName[MAX_FILE_NAME_PATH];
					memcpy(holdFileName, chInputFileName,MAX_FILE_NAME_PATH);
					// re-enter to load the sdc-device <null params except identity>
					pSDCdevice = newDevice(stdTblIdentity,NULL,NULL,NULL,NULL,0,isInTokenizer);
				//	hCddbDevice *stdDev = stdDevice(stdTblIdentity,NULL,NULL,NULL,NULL);
					if(NULL == pSDCdevice)
					{
						LOGIT(CERR_LOG | UI_LOG, 
							"ERROR: Device manager could not load the SDC DD");
						RAZE( pSelf );
						return NULL; // can't do much without the standard
					}// else all is well
					if ( ! isInTokenizer )
						LOGIF(LOGP_START_STOP)(CLOG_LOG,"Started the Device Object\n");
					// pop the globals
					eFFVersionMajor = holdFileVersionMaj;
					eFFVersionMinor = holdFileVersionMin;
					memcpy(chInputFileName, holdFileName, MAX_FILE_NAME_PATH);

					pSysStrings = (hCddlString*) new hCddlString(pSDCdevice->devHndl());
					aCddlString localStr;
					localStr.ddKey  = tempKey;
					localStr.ddItem = 16385; //x4001, default_string_entries
					pSysStrings->setEqual(&localStr);
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)(CERR_LOG | UI_LOG,"ERROR: Device manager could not get the SDC DD");
					// this is suicidal (and causes crashes)::>    RAZE( pSelf );
				}
			}//end if file version number >= 8
		}
		// else  dictionary already allocated, assume std files loaded as well			
		// we always gotta have a literal string table for a device
		locPtr2LitStrTbl = new LitStringTable;// transfered to device a little later

		LOGIF(LOGP_MISC_CMTS)(STAT_LOG|CLOG_LOG,"Getting Device's Description\n");
		// get prototype from the db
		aCdevice* pAbstractDev = NULL;
		rc = allocDevice(wrkingKey, &pAbstractDev);// alloc abstract device


		#ifdef STARTTIME_CHECK
			thisCnt = GetTickCount();
			LOGIT(CLOG_LOG,"> Aquired abstract Device.ET:%1.3f (%1.3f)\n",
				  (((float)thisCnt-(float)lastTickCnt)/1000),
				  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
			lastTickCnt = thisCnt;
		#endif

		if ( rc == SUCCESS ) // abstract device allocated
		{
			newIdent.cInternalFlags |= intFlgs_run_silent;// we've done this before, so be quiet 
			entryCount = 0; // be sure we are entering on top
			identifyDDfile(newIdent, wrkingKey, &pRemoteStr); // this has already worked once
			releaseStringVect(&pRemoteStr);// we aren't using them. 10jan14 DD@fluke

			// verify we haven't done this before
			map<DevInfcHandle_t, hCdeviceSrvc*>::iterator mapP;
			int i = deviceMap.size();
			mapP = deviceMap.find(newHndl);
			if ( mapP != deviceMap.end() )
			{			
				delete pAbstractDev;
				return (hCddbDevice*)mapP->second;
			}

			//   fill all of the items & attributes of the abstract device, also dict & strings
			rc = getDevice( wrkingKey, pAbstractDev,dictionary,locPtr2LitStrTbl,isInTokenizer );

			if ( rc != SUCCESS )
			{			
				LOGIT(CERR_LOG | UI_LOG,
					"ERROR: Device manager could not get the device template for key "
																"0x%016I64x (%d)",wrkingKey, rc);
				releaseDev(&pAbstractDev);
				RAZE( pDev );
				return NULL; // error exit			
			}
			else // we have the device prototype
			{
				LOGIF(LOGP_MISC_CMTS) (CLOG_LOG,"> deviceMgr got an abstract device with %d "
									"items in its list.\n", pAbstractDev->AitemPtrList.size());

				char* pCH = &(chInputFileName[0]);
				if ( isInTokenizer )
				{
#ifdef _DEBUG
					LOGIT(CLOG_LOG|UI_LOG,
							"Opened Handle %d, device "
#if defined(__GNUC__)
							"0x%016llx"
#else // !__GNUC__
							"0x%0I64x"
#endif // __GNUC__
							" from '%s'\n",
							newHndl, wrkingKey, pCH
							);
#else
					LOGIT(CLOG_LOG|UI_LOG,"Opened %d]'%s' \n",newHndl,pCH);
#endif
				}
				else // in sdc
				{
					LOGIT(CLOG_LOG|UI_LOG,
							"Device manager has loaded the device for "
#if defined(__GNUC__)
							"0x%016llx"
#else //!__GNUC__
							"0x%0I64x"
#endif
							" from '%s'\n",
					  wrkingKey,pCH);
				}

				#ifdef STARTTIME_CHECK
					thisCnt = GetTickCount();
					LOGIT(CLOG_LOG,"> Aquired an initialized Device.ET:%1.3f (%1.3f)\n",
						  (((float)thisCnt-(float)lastTickCnt)/1000),
						  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
					lastTickCnt = thisCnt;
				#endif
				
				if ( ! standardDevice )
					dealWithXXXmethodVariables(pAbstractDev);

				#ifdef DUMP_DEBUG
				dumpDev(pAbstractDev);
				#endif

				// get a command dispatcher for this device
				bool dfltDispatch = true; // assume we use the default unless told otherwise
				if ( pCD == NULL && ! standardDevice) // passed in is empty, use the default
				{	
					LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr will use the dflt dispatcher.\n");		
					if ( pThisCommInterface == NULL ) // no comm passed in available
					{// a fatal error
						LOGIT(CERR_LOG | UI_LOG,
							"ERROR: Device manager was not given a communication interface.");
						releaseDev(&pAbstractDev);
						LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
											"> deviceMgr has no comm infc to use in new device.");
						RAZE( pDev );
						return NULL; // error exit			
					}
					else // we have a comm interface to use
					{
						pCD = new hCcmdDispatcher(pThisCommInterface, newHndl);// the default
						if (pCD == NULL)
						{		
							LOGIT(CERR_LOG | UI_LOG,
								"ERROR: Device manager could not get a new default Dispatcher."
																			"(memory error)\n");
							releaseDev(&pAbstractDev);
							RAZE( pDev );
							return NULL; // error exit		
						}
						//else we have all the parts - do the deed	
						hSdispatchPolicy_t localPolicy;
						localPolicy = pCD->getPolicy();
						if (isInTokenizer)
							localPolicy.isPartOfTok = 1;
						else
							localPolicy.isPartOfTok = 0;
						pCD->setPolicy(localPolicy);

						LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr new'd a dflt dispatcher.\n");
					}
				}
				else  //..  assume that the passed in dispatcher is instantiated - 
					  //			 pThisCommInterface is unused (dispatcher will have the comm
				if ( ! standardDevice )// not so on standard
				{
					dfltDispatch = false; // caller is responsible for deletion	
					LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
										"> deviceMgr using passed-in dispatcher new device.\n");
					pCD->setHandle(newHndl);
					
					hSdispatchPolicy_t localPolicy = pCD->getPolicy();
					if (isInTokenizer)
						localPolicy.isPartOfTok = 1;
					else
						localPolicy.isPartOfTok = 0;
					pCD->setPolicy(localPolicy);

				}
				// manager registers the device::>
				deviceMap.insert( pair<DevInfcHandle_t, hCdeviceSrvc*>(newHndl,pDS) );
			
				// transfer pointers to the device internals
				pDev->dictionary       = dictionary;	
				pDev->literalStringTbl = locPtr2LitStrTbl;

				// transfer the identity information to the device internals
				Indentity_t locIDent;
				locIDent.wManufacturer = (WORD) ((wrkingKey >> 48) & 0xffff);
				locIDent.wDeviceType   = (WORD) ((wrkingKey >> 32) & 0xffff);
				locIDent.cDeviceRev    = (BYTE) ((wrkingKey >> 16) & 0x00ff);
				locIDent.cSoftwareRev  = (BYTE) ((wrkingKey >>  0) & 0x00ff);
				locIDent.cInternalFlags= newIdent.cInternalFlags;
				pDev->setIdentity( locIDent );
			
				LOGIF(LOGP_MISC_CMTS)(STAT_LOG|CLOG_LOG,"Generating device %08I64x", wrkingKey);	
				// build the device based on the prototype
				if ( standardDevice )// we'll deal with the duplication later
					rc = pDev->populateStandard(wrkingKey,  newHndl, pAbstractDev);
				else
					rc = pDev->instantiate(wrkingKey,  newHndl, pAbstractDev, pCD,
										pMethSupportInterface,pFileSupport,dfltDispatch );
				if (rc != SUCCESS)
				{
					LOGIT(CERR_LOG | UI_LOG,
						"ERROR: Device manager could not populate the device");
					RAZE( pDev );
					LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr failed populating the device.\n");
					return NULL; // error exit		- we can't let it go on
				}

				#ifdef STARTTIME_CHECK
					thisCnt = GetTickCount();
					LOGIT(CLOG_LOG,"> Instantiated Device.ET:%1.3f (%1.3f)\n",
						  (((float)thisCnt-(float)lastTickCnt)/1000),
						  (((float)thisCnt-(float)startOfInstantiation)/1000 ));
					lastTickCnt = thisCnt;
				#endif

				/* VMKP added on 311203
					set the write-only poll address value */
				hCitemBase *pItem = NULL;
				if ( ! standardDevice && locIDent.wManufacturer != 0 &&
					 pDev->getItemBySymNumber(DEVICE_POLL_ADDRESS, &pItem) == SUCCESS)
				{
					CValueVarient tmpVar;
					tmpVar = newIdent.PollAddr;
					((hCVar *)pItem)->setDispValue(tmpVar);
					((hCVar *)pItem)->ApplyIt();
					((hCVar *)pItem)->markItemState( IDS_CACHED );
				}
				
				pDev->pDevMgr = this;	// who's yo daddy...

				// the device will connect the comm interface

				// set the loopwarning variable flag in those variables
				CitemType varType(iT_Variable);
				CVarList       *pVARlist    = (CVarList*)(pDev->getListPtr(varType));

				hCitemBase  *pItemBase = NULL;
				if ( ! standardDevice  &&
					 pDev->getItemBySymNumber(LOOP_WARNING_VARIABLES,&pItemBase) == SUCCESS &&
					 pItemBase != NULL && pVARlist != NULL )
				{
					setLoopWarningVariables( (hCitemArray *)pItemBase, pVARlist );
				}
 
				releaseDev(&pAbstractDev);  // deallocates the abstract memory from the DLL
				// stevev 5/11/04 - add for file sniffing support
                /* Sethu (Utt) 23/10/17 - Commented below as it is not used anywhere.
                   Uncommenting the below line, leads to crash during device object destruction*/
                //rc = getDDfileInfo(pDev->getFileInfoPtr());
				if ( ! isInTokenizer )
				LOGIT(CLOG_LOG,"Started Device Handle %d\n",newHndl);
			}
		}
		else
		{		
			LOGIT(CERR_LOG | UI_LOG,
			"ERROR: Device manager could not allocate a device for 0x%08x (%d)",wrkingKey, rc);
			//cerr << "ERROR: Device Manager's newDevice could not allocate device."<<endl;
			RAZE( pDev );	
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr failed to allocate a new device.\n");		
			return NULL; // error exit			
		}

		#ifdef _DEBUG
			isInstantiated = 1;
		#endif
	}
	else // failure of new() - probable memory issue
	{		
		LOGIT(CERR_LOG | UI_LOG,
		"ERROR: Device manager could not allocate memory for a device.");
		pDS = NULL; // error exit	
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr could NOT allocate a new device.\n");
	}
	if (! (pSelf->DBnotAvailable))// is available
	{
		releaseDB();  /* stevev 9/22/03 - but leave it open till we're done */
	}
	
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> deviceMgr leaving new device.\n");
	return pDev;
}

#if 0  /* the above is re-entrant to make the following obsolete duplication */

stevev 06jul09 - removed the commented out code, see earlier versions for contents

#endif /* if 0 - now unused */



// stevev 10nov04 - files need the device during destroy - fixed this
// stevev 14apr11 - fixed to use the handle, not the key (multiple instances...)
RETURNCODE hCDeviceManger::deleteDevice(hCdeviceSrvc** ppDeviceSrvc)
{
	RETURNCODE rc = FAILURE;
	DevInfcHandle_t devHndl = 0;
	map<DevInfcHandle_t, hCdeviceSrvc*>::iterator mapPos;

	// verify the inputs
	if ( ppDeviceSrvc == NULL || *ppDeviceSrvc == NULL )
	{
		DEBUGLOG(CERR_LOG,"DBGERROR: deleting a null device pointer.\n");
		_CrtDbgBreak();
		return rc;
	}
	if (*ppDeviceSrvc == &errorDeviceSrvc)
	{
		DEBUGLOG(CERR_LOG,"DBGERROR: attempt to delete the error device.\n");
		_CrtDbgBreak();
		return rc;
	}

	hCddbDevice* pDev = dynamic_cast<hCddbDevice*>(*ppDeviceSrvc);// down-cast it
	if (pDev == NULL)
	{
		DEBUGLOG(CERR_LOG,"DBGERROR: attempt to deleteDevice a non-device.\n");
		_CrtDbgBreak();
		return rc;
	}

	devHndl = pDev->devHndl();

	// lookup the device in the registry and return the correct ptr
	mapPos = deviceMap.find(devHndl);
	if ( mapPos == deviceMap.end() 
#ifdef _DEBUG
		                           || (mapPos->second) != *ppDeviceSrvc )
#else
		                           )// do not compare pointers in release code
#endif
	{//mapPos should be a ptr2ptr2hCdeviceSrvc
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
						"ERROR: attempt to delete  unregistered device - forcing deletion.\n");
	}
	else
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"< device found by manager: destroying>\n");
		pDev->destroy();
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"< device found by manager: deleting  >\n");
		// 10apr07, now done via erase above......miT->second = NULL;
		// 13jul07 - fri - stevev - erasing the device before destroy causes havoc
		//			during the FILE storage that is done in destroy().
		//			I don't know why erase was moved to the top so I'm moving it back here
		deviceMap.erase(mapPos);  // HOMZ - Nov2006; Erase this element of the map
	}
	
	delete (pDev);// either unregistered or found, it's gotta go
	*ppDeviceSrvc = NULL;

	return SUCCESS;
}

/* stevev 15oct08 - extract from NewDevice to try and shorten it up
 *	originally by VMKP  on 040203
 *  does detection and substitution of undocumented functions
 */
void hCDeviceManger::dealWithXXXmethodVariables(aCdevice* pAbstractDev)
{ 
	//now iterate through the variables list
	AitemList_t::iterator itr1;// ptr2ptr2aCitemBase
	aCitemBase *pAcIB = NULL;

	for(itr1 = (pAbstractDev->AitemPtrList).begin();
	    itr1 !=(pAbstractDev->AitemPtrList).end();
		++itr1) 
	{
		pAcIB = (aCitemBase *)(*itr1);// ptr2ptr - found by kari @ beamex
		if( pAcIB->itemType.getType() == METHOD_ITYPE)
		{
			aCattrBase* pAcAttr = NULL;
			FOR_iT(AattributeList_t,pAcIB->attrLst) // ptr2ptr2aCattrBase
			{
				pAcAttr = (aCattrBase*)(*iT);// ptr2ptr - found by kari @ beamex
				genericTypeEnum_t git = getAttrType4Mask(pAcIB->itemType, pAcAttr->attr_mask);

				if(git.gAttrMethod == methodAttrDefinition)
				{
					BYTE byPreOrPostItem[100];
					UINT nItemId[100];
					int nNoofPrePostCommitItems = 
						handle_XXX_Pre_Post_Commit_Items( ((aCattrMethodDef*)pAcAttr)->pBlob,
														   byPreOrPostItem,    nItemId);

					for(int loop = 0; loop < nNoofPrePostCommitItems; loop++)
					{
						if( byPreOrPostItem[loop] == XXX_PRE_COMMIT )
						{
							UpdateDevicePreWriteAction(pAbstractDev,nItemId[loop],pAcIB->itemId);
						}
						else 
						if ( byPreOrPostItem[loop] == XXX_POST_COMMIT )
						{
							UpdateDevicePostWriteAction(pAbstractDev,nItemId[loop],pAcIB->itemId);
						}
					}// next prepostItem
				}// else it's not the definition, so skip it
			}// next attribute in this method item
		}// else it's not a method item so skip it
	}// next item
	return;
}



/* stevev 15oct08 - extract from NewDwvice to try and shorten it up
 *	originally by prashant_begin
 *  flags the loop-warning-variables
 */
void hCDeviceManger::setLoopWarningVariables( hCitemArray *pItemArray, CVarList*pVARlist )
{  
	UIntList_t ArrayItemIDs;
	UIntList_t::iterator iID ; // ptr2unsignedInt

	pItemArray->getAllindexes(ArrayItemIDs);// actually all item IDs

	//now iterate through the variables list setting the flag in all variables
	for (CVarList::iterator ivT = pVARlist->begin(); ivT < pVARlist->end(); ivT++ )
	{
		hCVar *pCVar = (*ivT);
		pCVar->m_bLoopWarnVariable = false;// handles the else for all ifs below

		for(iID = ArrayItemIDs.begin(); iID != ArrayItemIDs.end(); ++iID )
		{
			if( pCVar->getID() == (itemID_t)*iID )
			{
				pCVar->m_bLoopWarnVariable = true;
				break;// do the next item from array
			}
		}// next array item ID
	}// next variable in the device
	return;
}

/*************************************************************************************************
 * hCglobalServiceInterface class
 *************************************************************************************************
*/
/* static */
hCdeviceSrvc*    hCglobalServiceInterface::myDevicePtr(DevInfcHandle_t myHandle)
{
	if ( hCDeviceManger::pSelf != NULL )// there can only be one.
	{
		return (hCDeviceManger::pSelf->getDevicePtr(myHandle));
	}
	else
	{
		LOGIT(CERR_LOG,"ERROR: Global services is returning the error device.\n");
		return ( &errorDeviceSrvc );
	}
}

/* static */
int /*bool */    hCglobalServiceInterface::dbNOTavailable(void)
{
	if ( hCDeviceManger::pSelf != NULL )
	{
		return (hCDeviceManger::pSelf->IsdbNOTavailable());
	}
	else
	{
		return ( 1 ); // it is not available if the manager doesn't exist
	}
}

#if 0
/* static */
int   hCglobalServiceInterface::output(int channel, char* format, ...)
{	
	int r = 0;
	va_list vMarker;
//temporary
//	char strActual[1024];
//	va_start(vMarker, format);
//		vsprintf(strActual, format, vMarker);
//		cerrxx <<"++++++++"<< strActual << endl;
//	va_end(vMarker);
// end temporary

	va_start(vMarker, format);
	r = hCglobalServiceInterface::voutput(channel, format, vMarker);
	va_end(vMarker);

	return r;
}
/* static */
int    hCglobalServiceInterface::voutput(int channel, char* format, va_list vL)
{	
	if (channel > (CERR_LOG|CLOG_LOG|COUT_LOG|UI_LOG)) return 1;
	char strActual[1024];
	va_list tL = vL;

	if (channel & CERR_LOG)
	{
		tL = vL;
		vsprintf(strActual, format, tL);
		cerrxx << strActual << endl;
	}
	if (channel & CLOG_LOG)
	{
		tL = vL;
		vsprintf(strActual, format, tL);
		clog << strActual << endl;
	}
	if (channel & COUT_LOG)
	{
		tL = vL;
		vsprintf(strActual, format, tL);
		cout << strActual << endl;
	}
	if (channel & UI_LOG)
	{
		tL = vL;
		LogMessageV(format, tL);
	}
	va_end(vL);// this appears to clean the stack...be careful

	return 0;
}

/* static */
int  hCglobalServiceInterface::output(int channel, int errNumber, ...)
{
	cerrxx << "ERROR:  LOGIT via error number is not implemented yet."<<endl;
	return 1;
}
/* static */
int  hCglobalServiceInterface::voutput(int channel, int errNumber, va_list vL)
{
	cerrxx << "ERROR:  LOGIT via error number is not implemented yet."<<endl;
	return 1;
}
#endif
/*************************************************************************************************
 *  hCobject class
 *************************************************************************************************
*/
hCobject::hCobject(DevInfcHandle_t newHandle) 
	: MyDeviceHandle(newHandle)
{
	pTheDevice = NULL;
#ifdef _DEBUG
	incThrdCnt();
#endif
} 
hCobject::~hCobject()
{	
#ifdef _DEBUG
	decThrdCnt(); 
#else
	/* we own no memory, nothing to do */ 
#endif
}


// the sdc device is available to everybody
hCdeviceSrvc* hCobject::stdPtr(void)
{
    /* CPMHACK: Since sizeof Pointer is 8 bytes in 64-bit compiler,
       changed from int to unsigned long type for type casting which works
       for both 32-bit and 64-bit compiler */
    if (((unsigned long)pSDCdevice) > 0x1000)
	{
		return pSDCdevice;
	}
	else
	{
		return (&errorDeviceSrvc);
	}
	return NULL; // unreachable
}


hCdeviceSrvc* hCobject::devPtr(void)
{ 
#ifdef _DEBUG
    if ( ((unsigned long)pTheDevice) < 0x1000 && ((unsigned long)pTheDevice) > 0x0000)
	{// get a breakpoint
		LOGIT(CLOG_LOG," ");
	}
#endif
    /* CPMHACK: Since sizeof Pointer is 8 bytes in 64-bit compiler,
       changed from int to unsigned long type for type casting which works
       for both 32-bit and 64-bit compiler */
	//if (pTheDevice == NULL || pTheDevice == &errorDeviceSrvc )
    if (((unsigned long)pTheDevice) < 0x1000 || pTheDevice == &errorDeviceSrvc )// make an assumption about PC memory
	{
		pTheDevice = hCglobalServiceInterface::myDevicePtr(MyDeviceHandle) ; 
	}
#ifdef _DEBUG
    if ( ((unsigned long)pTheDevice) < 0x1000 && ((unsigned long)pTheDevice) > 0x0000)
	{// get a breakpoint
		LOGIT(CLOG_LOG," ");
	}
#endif
	return pTheDevice;
}

// notice that this does not allow you to set the handle to an invalid Zero
void  hCobject::setPtr(DevInfcHandle_t h)
{
	if ( h != 0 )
	{
		if (MyDeviceHandle == 0)
		{// normal
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," - Setting Handle from 0 to %d\n",h);
		}
		else
		if (MyDeviceHandle != h)
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," - Resetting Handle from %d to %d\n",MyDeviceHandle, h);
		}
		else
		{			
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," - Not Setting Handle: it is already that value.\n");
		}
		MyDeviceHandle = h;
	}
	else
	{
		LOGIT(CLOG_LOG, " - Not Setting Handle: Zeroing object handles is illegal.\n");
	}
}

// a useful function

hCVar* hCobject::getVarPtrBySymNumber(itemID_t symNum)
{	
	hCitemBase*     pItem = NULL;
	hCVar*          pVar  = NULL;

	if (symNum != 0)
	{
		if ( devPtr()->getItemBySymNumber(symNum, &pItem) == SUCCESS && pItem != NULL)
		{
			if (pItem->IsVariable())
			{
				pVar = (hCVar*)pItem;
			}
			else
			{
				DEBUGLOG(CLOG_LOG|CERR_LOG,"%s (0x%04x) Expected to be a variable.(dbg-only msg)\n",pItem->getName().c_str(),pItem->getID());
			}
		}
	}
	return pVar;// NULL on error
}

devMode_t hCobject::compatability(void)
{
	return devPtr()->whatCompatability();
}

/*************************************************************************************************
 *
 *   $History: ddbDeviceMgr.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
