/*************************************************************************************************
 *
 * $Workfile: ddbGrid.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		1Apr05	sjv	started creation
 */
#include "ddbGrid.h"
#include "logging.h"
#include "ddbMenuLayout.h"// stevev 01nov06 added
#include "ddbItemBase.h"

/* the global mask converter...you have to cast the return value*/
extern int  mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];

const char gridOrientStrings[GRIDORIENTSTRCOUNT] [GRIDORIENTMAXLEN] = {GRIDORIENTSTRINGS};



void  hCGridGroupList::append(hCGridGroupList* pList2Append)
{
	reserve(size() + pList2Append->size());		//PO reserving to prevent multiple allocations
	//TROUBLE WITH THIS /*itemList.*/insert(/*itemList.*/end(),pList2Append->begin(),pList2Append->end());
	for (gridList_t::iterator iT = pList2Append->begin(); 
	     iT < pList2Append->end(); push_back(*iT), iT++);
}


/*************************************************************************************************
 * grid class
 ************************************************************************************************/

extern void dumpItem(aCitemBase* paItm);

hCgrid::hCgrid(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pMemberAttr(NULL),
								 pGridHgt(NULL),pGridWid(NULL), GridOrient(h),pGridHandling(NULL) 
{// label , help & validity is set in the base class
#ifdef STOP_DEBUG
dumpItem(paItemBase);
#endif
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(gridAttrMembers); 
	if (aB == NULL)
	{	LOGIT(CERR_LOG, "ERROR: hCgrid::hCgrid<no member attribute supplied.>\n");
		LOGIT(CERR_LOG,"       trying to find: %d but only have:", gridAttrMembers);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG, " an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG," %d, ", (*iT)->attr_mask);}
		}
		cerr<<endl;
		pMemberAttr = NULL;
	} 
	else // generate a hart class from the abstract class
	{ 
		pMemberAttr = (hCattrGridItems*)newHCattr(aB);
		if (pMemberAttr != NULL)
		{
			pMemberAttr->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMemberAttr);// keep it on the list too
		}
		else // is NULL
		{
			grpItmAttrType_t en = (grpItmAttrType_t)mask2value(aB->attr_mask); 
			LOGIT(CERR_LOG,"++ No New attribute ++ (%s)\n", grpItmAttrStrings[en]);
		}
	}
	
	// NOT required			
		
	aB = paItemBase->getaAttr(gridAttrHeight); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pGridHgt = (hCattrGrafixSize*)newHCattr(aB);
		if (pGridHgt != NULL)
		{
			pGridHgt->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pGridHgt);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Height attribute for Grid ++ (memory error?)\n");
		}
	}

	aB = paItemBase->getaAttr(gridAttrWidth); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pGridWid = (hCattrGrafixSize*)newHCattr(aB);
		if (pGridWid != NULL)
		{
			pGridWid->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pGridWid);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Width attribute for Grid ++ (memory error?)\n");
		}
	}


	aB = paItemBase->getaAttr(gridAttrOrient); // uses aCddlLong
	if (aB != NULL)
	{
		//GridOrient.setEqual(aB);
		hCconditional<hCddlLong> tempOrient( h );

		tempOrient.setEqualTo((void*) &(((aCattrCondLong*)aB)->condLong));
#ifdef _DEBUG
if (tempOrient.destElements[0].pPayload && 
	((hCddlLong*)(tempOrient.destElements[0].pPayload))->is1 )
{
	assert(0);
}
#endif
		if ( tempOrient.resolveCond(&GridOrient) != SUCCESS )
		{
#ifdef _DEBUG
if (tempOrient.destElements[0].pPayload && 
	((hCddlLong*)(tempOrient.destElements[0].pPayload))->is1 )
{
	assert(0);
}
#endif
			cerr<<"ERROR: Orientation has temporary conditional (programer) error."<<endl;
		}
		else
		if (GridOrient.getDdlLong() > 2)
		{
#ifdef _DEBUG
if (tempOrient.destElements[0].pPayload && 
	((hCddlLong*)(tempOrient.destElements[0].pPayload))->is1 )
{
	assert(0);
}
#endif
			cerr<<"ERROR: Orientation value is invalid."<<endl;
		}
#ifdef _DEBUG
if (tempOrient.destElements[0].pPayload && 
	((hCddlLong*)(tempOrient.destElements[0].pPayload))->is1 )
{
	assert(0);
}
#endif
	}	

	aB = paItemBase->getaAttr(gridAttrHandling); // uses aCattrBitstring
	if (aB != NULL)
	{
		pGridHandling = (hCattrHndlg*)newHCattr(aB);
		if (pGridHandling != NULL)
		{
			pGridHandling->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pGridHandling);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Handling attribute for Grid ++ (memory error?)\n");
		}
	}	

}


/* from-typedef constructor */
hCgrid::hCgrid(hCgrid*    pSrc,  itemIdentity_t newID) : hCitemBase((hCitemBase*)pSrc,newID),
			 pMemberAttr(NULL), pGridHgt(NULL),pGridWid(NULL), GridOrient(NULL),pGridHandling(NULL) 
{
	LOGIT(CERR_LOG,"***** hCgrid typedef constructor needs a body.*****\n");
}



hCattrBase*   hCgrid::newHCattr(aCattrBase* pACattr)
{
	if (pACattr->attr_mask == maskFromInt(gridAttrMembers)) 
	{
		return new hCattrGridItems(devHndl(), (aCattrGridMemberList*)pACattr, this );		
	}
	else
	if (pACattr->attr_mask == maskFromInt(gridAttrHeight))	// same value as graphAttrHeight
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(gridAttrWidth))
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(gridAttrHandling)) 
	{
		return 
			new hCattrHndlg(devHndl(), (aCattrBitstring*)pACattr, gridAttrHandling, this );		
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(gridAttrLabel) | 
			 maskFromInt(gridAttrHelp)  | 
			 maskFromInt(gridAttrValidity) | 
			 maskFromInt(gridAttrDebugInfo) ))
	{//known attribute but shouldn't be here		
		cerr << "ERROR: grid attribute type is not handled by base class."<<endl;
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIT(CERR_LOG,"ERROR: grid attribute type is unknown.\n");
		return NULL; /* an error */  
	}
}


RETURNCODE hCgrid::getHeight(unsigned & height)
{

	RETURNCODE rc = SUCCESS;
	if(NULL != pGridHgt)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pGridHgt->procure();
		if(ptr2TempLong)
		{
			height = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
		{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;
	 
}/*End getHeight*/

RETURNCODE hCgrid::getWidth(unsigned &  width)
{

	RETURNCODE rc = SUCCESS;
	if(NULL != pGridWid)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pGridWid->procure();
		if(ptr2TempLong)
		{
			width = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
	{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;

}/*End getWidth*/


RETURNCODE hCgrid ::getOrient(gridOrient_t & co)
{
	RETURNCODE rc = SUCCESS;

	co = (gridOrient_t)GridOrient.getDdlLong();
	if(0 == (int)co)
	{	
		co = gd_Vertical;
	}
	return SUCCESS;

}/*End getOrient */


RETURNCODE hCgrid::getHandling(UINT64 & uHdlg)
{
	RETURNCODE rc = SUCCESS;
	if (NULL == pGridHandling) 
		return APP_ATTR_NOT_SUPPLIED; 
	else
	{
		hCbitString *pTmpHdlg = (hCbitString *)pGridHandling->procure();
		if(pTmpHdlg)
		{
			uHdlg = pTmpHdlg->getBitStr();
		}
		else
		{
			uHdlg = READ_HANDLING;// default to read-only
		}
	}//end else
	return rc;

}/*End getHandling*/


CValueVarient hCgrid::getAttrValue(unsigned attrType, int which)
{	
	CValueVarient rV;

	gridAttrType_t gat = (gridAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( gat >= gridAttr_Unknown )
	{
		LOGIT(CERR_LOG,"ERROR: Grid's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	switch (gat)
	{
	case gridAttrLabel:
	case gridAttrHelp:
	case gridAttrValidity:
		rV = hCitemBase::getAttrValue(gat);		
		break;
	case gridAttrHeight:
		{// TODO
			LOGIT(CERR_LOG,"WARN: gridHeight information is not accessable.\n");
		}
		break;
	case gridAttrWidth:
		{// TODO
			LOGIT(CERR_LOG,"WARN: gridWidth information is not accessable.\n");
		}
		break;
	case gridAttrOrient:
		{// TODO
			LOGIT(CERR_LOG,"WARN: gridOrientation information is not accessable.\n");
		}
		break;
	case gridAttrHandling:
		{// TODO
			LOGIT(CERR_LOG,"WARN: gridHandling information is not accessable.\n");
		}
		break;
	case gridAttrMembers:
		{
			LOGIT(CERR_LOG,"ERROR: member information is not accessable from here.\n");
		}
		break;
	case gridAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIT(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

/******
	getVectorList

	The passed in list will be appended (clear it before calling if that's what you need)
	The Vector list holds an entry for each row | column (depending on orientation)
	Each entry is a gridGroupDesc riptor consisting of a header string and a list of values.
	The values are contained in a varient list and may be constants OR symbolIDs.
******/
RETURNCODE hCgrid::getVectorList(gridDesc_t& vectorList)
{
	if ( pMemberAttr == NULL )
	{
		return APP_ATTR_NOT_SUPPLIED;
	}// else continue
	
	return ( pMemberAttr->getList(vectorList) );
}

/* stevev 12sep06 - dependency extentions */
void  hCgrid :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes
	if ( pMemberAttr != NULL )
	{
		pMemberAttr->getDepends(iDependOn);
	}	

	if ( pGridHandling != NULL )
	{
		pGridHandling->getDepends(iDependOn);
	}		

/* Size and orientation may not change while running */
}
/** end stevev 12sep06  **/

/** added stevev 10oct06 **/
// called when an item in iDependOn has changed 
int hCgrid::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();

	hCitemBase::notification(msgList,isValidity);// do validity and label
/* note to self - 05oct06 - cannot filter on isConditional if any part of the payload is a 
							reference or expression */
	// re-evaluate all the others 
	if ( (! isValidity) && (pMemberAttr != NULL) )
	{
		if (pMemberAttr != NULL && pMemberAttr->reCalc())// list
		{// my member list changed
			msgList.insertUnique(getID(), mt_Str, pMemberAttr->getGenericType());
		}// else - no change;
	}
	
	if ( (! isValidity) && pGridHandling != NULL && pGridHandling->isConditional() && 
																pGridHandling->reCalc())
	{// my validity changed
		msgList.insertUnique(getID(), mt_Str, pGridHandling->getGenericType());
	}// else - no change;

	return ( msgList.size() - init);
}

RETURNCODE hCgrid::setSize( hv_point_t& returnedSize, ulong qual )
{
	unsigned X= DFLTWID_INDX,Y = DFLTHGT_INDX;
	
	if ( getHeight(Y) != SUCCESS)
	{	Y = DFLTHGT_INDX;
	}
	if ( getWidth(X)  != SUCCESS)
	{	X = DFLTWID_INDX;
	}

	// translate size codes into number of 1x1 frames
	returnedSize.yval = vert_Sizes[Y];
	returnedSize.xval = horizSizes[X];

	return SUCCESS;
}

/************************************************************************************************/

/************************************************************************************************/

RETURNCODE hCattrGridItems::getList(gridDesc_t& r2gD)
{	
	RETURNCODE rc = SUCCESS;

	resolvedGGL.clear();
	
	rc = condListOGridGrpLists.resolveCond(&resolvedGGL);

	if (rc == SUCCESS && resolvedGGL.size() > 0 )
	{
		return (resolvedGGL.getGridDesc(r2gD) );	
	}
	else
	{	
		return SUCCESS;	 // note that a conditional without a default
	}
	
}
	
// 12sep06 stevev
RETURNCODE hCattrGridItems::getDepends(ddbItemList_t& retList)
{
	RETURNCODE rc = SUCCESS;
	
	// get what the conditional depends on
	condListOGridGrpLists.aquireDependencyList(retList, false);

	/* also depends on the conditional and/or validity of its items */
	vector<hCGridGroupList*>       localpGGLL;
	vector<hCGridGroupList*>::iterator ipGGLL;

	// get what the resolution depends on: 
	// -- make the grouplist do all the work
	if (condListOGridGrpLists.aquirePayloadPtrList(localpGGLL) == SUCCESS && localpGGLL.size()>0)
	{// we have a list of all possible ptrs to hCGridGroupList's
		for (ipGGLL = localpGGLL.begin(); ipGGLL != localpGGLL.end();  ++ipGGLL )
		{// ptr 2a ptr 2a hCGridGroupList
			hCGridGroupList *pggl = (*ipGGLL);
			if (pggl == NULL)
				DEBUGLOG(CLOG_LOG,"Grid Items has a NULL in its list.\n");
			else
				rc |= pggl->getDepends(retList);// appends onto end
		}
	}

	return rc;
}
	
bool hCattrGridItems::reCalc(void) // returns didChange
{
	bool ret = false;

	return ret;
}

/* fill list of vectors */
RETURNCODE hCGridGroupList::getGridDesc(gridDesc_t& r2gD)
{
	if ( size() <= 0 )
	{
		LOGIT(CERR_LOG,"WARNING: Grid vector has empty data list.\n");
		return SUCCESS; // not an error - caller must test list size
	}// else get the list
	gridGroupDesc wrkDesc;

	for (itGridGroup iT = begin(); iT != end(); ++iT)
	{// isa ptr2a hCgridGroup ::for each hCgridGroup in the list (one vector)
		wrkDesc.headerValue = (wstring)(*iT);
		if ( iT->getEntryList(wrkDesc.vectorValues) == SUCCESS )
		{
			r2gD.push_back(wrkDesc);
		}
		wrkDesc.clear();
	}
	return SUCCESS;
}


void  hCGridGroupList::setEqual(void* pAclass)// assume: pAclass pts to a  class aCgridMemberList
{
	if (pAclass == NULL) return;//nothing to do
	if ( ((aCgridMemberList*)pAclass)->whatPayload() != pltGridMemberList )
	{
		LOGIT(CERR_LOG,"ERROR: grid group list's setEqual to a NON grid member list.\n");
		return;
	}

	AGridMemberElemList_t* pcL = (aCgridMemberList*)pAclass;
	hCgridGroup wrkMember(devHndl());

	for(AGridMemberElemList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	{ // iT is ptr 2 aCgridMemberElement..a header, reflist pair
		if (&(*iT) != NULL)	// PAW &(*) added
		{
			wrkMember.setEqual(&(*iT));
			push_back(wrkMember);
			wrkMember.clear();
		}
		else
		{
			LOGIT(CERR_LOG,"ERROR: received a NULL wrkMember ptr.\n");
		}
	} 
}

void  hCGridGroupList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	hCGridGroupList* pcL = (hCGridGroupList*)pPayLd;
	hCgridGroup wrkMember(devHndl());

	for(gridList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	{ // iT is ptr 2 hCgridGroup..a header, reflist pair

#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
	|| defined(__GNUC__)
		wrkMember.setDuplicate(*((hCgridGroup*)(&(*iT)) ));
#else
		wrkMember.setDuplicate(*((hCgridGroup*)iT));
#endif
		push_back(wrkMember);
		wrkMember.clear();
	} 
}

hCGridGroupList::hCGridGroupList(hCGridGroupList& el) : hCobject(el.devHndl())
{ 	
	itGridGroup iT;
	for (iT = el.begin(); iT != el.end(); ++iT)
	{
		push_back(*iT);
	}
#ifdef _DEBUG
	if (el.size() != size())
	{
		cerr<<"*ERROR: copy constructor failed for grid group list."<<endl;
	}
#endif
};

// 12sep06 stevev
RETURNCODE hCGridGroupList::getDepends(ddbItemList_t& retList)
{
	RETURNCODE rc = SUCCESS;

	HreferencePtrList_t       localRefpL;
	HreferencePtrList_t::iterator iRefp;
	
	ddbItemList_t           localIpL;
	ddbItemLst_it           iIpLst;

	hCdependency* pDep = NULL;

	for (itGridGroup iGGp = begin(); iGGp != end(); ++iGGp)//for each gridgroup
	{//is a ptr 2a hCgridGroup
		iGGp->getRefPointers(localRefpL);// fill the ref ptrs
	}
	// we have a list of all references: get dependencies
	for ( iRefp = localRefpL.begin(); iRefp != localRefpL.end(); ++iRefp )
	{// ptr 2a ptr 2a hCreference
		(*iRefp)->aquireDependencyList(retList,false);// what the reference depends on
		(*iRefp)->resolveAllIDs(localIpL);// all possible ids
	}
	// we have all possible resolved items now
	for (iIpLst = localIpL.begin(); iIpLst != localIpL.end(); ++ iIpLst )
	{// ptr 2a ptr 2a hCitemBase
		pDep = (*iIpLst)->getDepPtr(ib_ValidityDep);
		if ( pDep && pDep->iDependOn.size() > 0)
		{// list of what this item's validity is dependent on (MT @ constant)
			hCdependency::insertUnique(retList,(*iIpLst));//append list uniquely <a static helper>
		}
	}

	return rc;
}

/************************************************************************************************/

/************************************************************************************************/

void  hCgridGroup::setEqual(aCgridMemberElement* pAe)
{
	hdr = pAe->header;
	dataList.setEqual((void*) &(pAe->refLst));
}

void  hCgridGroup::setDuplicate(hCgridGroup& rgg)
{
	hdr.duplicate(&(rgg.hdr),false);// does not use 'replicate'
	//elementID_t eid = { getID(), -1 };
	dataList.duplicate(&(rgg.dataList),false);// , eid);//refList
}


RETURNCODE hCgridGroup::getEntryList(varientList_t& valList)
{
	RETURNCODE rc = SUCCESS;
	rc = dataList.getEntryList(valList);
	return rc;
}

