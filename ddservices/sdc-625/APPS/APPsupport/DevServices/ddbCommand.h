/**********************************************************************************************
 *
 * $Workfile: ddbCommand.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		8/29/02	sjv	created from ddbItems.h
 *		9/05/02   sjv added attrTrans.h to the file
 *	
 * #include "ddbCommand.h"
 */

#ifndef _DDB_COMMAND_H
#define _DDB_COMMAND_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbItemBase.h"

#include "ddbConditional.h"
#include "ddbResponseCode.h"
#include "ddbdefs.h"
#include "ddbCmdDispatch.h"
#include "ddbAttributes.h"

extern hCcmdDispatcher::fpCalcWtTrans_t fpCalcWgt;

/* stevev 16nov05 - get all the messages put together
	the following is here due to this being the common include file for all concerned
	it is used to percolate messages out of the extraction
*/
typedef enum msgType_e
{
	mt_Unk,	// error
	mt_Val,	// value
	mt_Str,	// structure
	mt_All  // the nuclear option
   ,mt_Mth  // stevev 6jun07 - method-changed Value
}
/*typedef*/ msgType_t;

/* stevev 27oct08 - the following is to add more information without adding another parameter 
 *	to the ExtractIt function call. Part of the conversion to a 2 pass extraction system.
 *  (pass 1 to get possible indexes (of class LOCAL or subtype INDEX) then pass 2 to extract).
 */
typedef enum transType_e
{
	tt_NoType,		//0
	tt_isWrite,		//1 write command reply packet
	tt_isExtract,	//2 as opposed to pass 1 to get indexes from command
	tt_isExtNwrt,	//3 is extraction pass on a write command reply
}transactionType_t;



class hCmessage
{
public:
	itemID_t  msgItem;
	msgType_t msgType;
	ulong     msgAttr;	// informational

public:
	hCmessage():msgItem(0),msgType(mt_Unk),msgAttr(0){};
	hCmessage(const hCmessage& src):
							msgItem(src.msgItem),msgType(src.msgType),msgAttr(src.msgAttr){};	
	hCmessage(itemID_t itemID, msgType_t thisType, ulong thisAttr = 0):
		msgItem(itemID),msgType(thisType),msgAttr(thisAttr){};
	virtual ~hCmessage(){};
	
	bool	operator==(const hCmessage& src)
	{ return ((msgItem==src.msgItem) && (msgType==src.msgType) ); };
};

typedef vector<hCmessage>   msgList_t;
typedef msgList_t::iterator msgLstItr_t;
typedef vector<hCmessage*>  msgPtrList_t;
typedef msgList_t::iterator msgPtrLstItr_t;

class hCmsgList : public msgList_t
{
public:
	bool isRqstPkt; // stevev 18may07 - to identify pkt type to Extraction (needed when adding
					//												read-comm log capability )
	hCmsgList():isRqstPkt(false) {};
	virtual ~hCmsgList(){};

	RETURNCODE insertUnique(hCmessage& newMsg);
	RETURNCODE insertUnique(itemID_t itemID, msgType_t thisType, ulong thisAttr = 0)
	{  hCmessage lm(itemID,thisType,thisAttr); return (insertUnique(lm)); };
};
/** end stevev 16nov05 **/

// a command will have a list of transactions
// a transaction will have a number, request, reply, & (possibly) response codes
// a request & reply will each have a list of data items

class hCdataItem
{
public: /* was private:  (kinda useless) */
	// from DATA_ITEM
	ulong  type;				// see cmdDataItemType_t(const,ref,flags,width,flgwidth,float)
	cmdDataItemFlags_t  flags;	// info, index, index&info,width present
	bool  widthValid;			//true/false
	//ulong  width;
	UINT64  width;	// made longer to match the partner 19aug10,stevev
	// value: only one used
	ulong   iconst;
	float   fconst;
	hCreference		ref;// was call to ddl_parse_ref 
						// w/ flags = RESOLVE_OP_REF | RESOLVE_DESC_REF | RESOLVE_TRAIL


public:
	hCdataItem(DevInfcHandle_t h) :ref(h)       {clear();};
					//stevev 5jul06 - stop duplicate reference construction in copy constructor
	hCdataItem(const hCdataItem& src):ref(src.ref.devHndl()) { operator=(src);};
	hCdataItem& operator=(const hCdataItem& src)
			{	if ( this == NULL ) return (*this);
			//DEEPAk 310105
				clear();
			//END
			type = src.type; flags = src.flags; widthValid = src.widthValid; width = src.width;
			iconst = src.iconst; fconst = src.fconst; ref = src.ref; return (*this);};
	virtual ~hCdataItem();
	RETURNCODE destroy(void){ ref.destroy(); return SUCCESS; };

	// see notes at the end of the file
	RETURNCODE AddIt  (BYTE* pByteCnt, BYTE** ppBuf, int& length, bool& retIsValid ); 
	RETURNCODE AddMask(hCVar* pVAR, BYTE* pByteCnt, BYTE** ppBuf, int& length);

	RETURNCODE ExtractIt  (BYTE* pByteCnt, BYTE** ppBuf, int& length, 
			methodCallList_t& eaLst, hCmsgList&  noteMsgs, varPtrList_t &hasChangedList, 
			hIndexUse_t& info4Idx, transactionType_t transType, 
			indexUseList_t* pSentIdex = NULL);       // added 16may11 to detect index mismatch
	RETURNCODE ExtractMask(hCVar* pVAR, BYTE* pByteCnt, BYTE** ppBuf, int& length,
					methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
					varPtrList_t &hasChangedList, transactionType_t transType);

	RETURNCODE markPending(void);

	// handle actions, executes pre, puts posts into list
	RETURNCODE handleActions(cmdOperationType_t rdWr, methodCallList_t& pPostAction);
													//HreferenceList_t* pPostAction);
	hCVar*     getVarPtr(void);
	RETURNCODE addAllVars(GroupItemList_t& varGroup);// add all possible vars to list
	RETURNCODE commandAllLists(hCcommandDescriptor& cmdDesc);// tell the lists

	void setEqual(aCdataItem* pAdataItem);
	void setDuplicate(hCdataItem& pDI);

	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet);

	// stevev 19sep07 added to support POST_RQSTUPDATE_ACTIONS
	RETURNCODE getItemPtr( hCitemBase*& r2pReturnedPtr)
	{ return (ref. resolveID( r2pReturnedPtr, false )); };

	void clear(void);

protected:  /* helper functions */
	int  maskLen(ulong mask);

};

typedef vector<hCdataItem>  dataItemList_t;
typedef vector<hCdataItem*> dataItemPtrList_t;
typedef dataItemList_t::iterator    iTdataItmLst;


/***************************
 * dataitemList holds the list of references that make up a request/reply
 * part of the transaction 
 **************************/

class hCdataitemList : public hCpayload, public dataItemList_t, public hCobject
{	// CconditionalList<hCdataItem> items;
	// - is parent now::>was<  dataItemList_t items;
public:
	hCdataitemList(DevInfcHandle_t h) : hCobject(h) {};
	hCdataitemList(const hCdataitemList& src) : hCobject(src.devHndl()) {operator=(src);};
	hCdataitemList& operator=(const hCdataitemList& src);
	virtual ~hCdataitemList();

	// from:  ddl_dataitems_choice(
	// see below RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet = 0) ;
		//{cerrxx<< "____stubout___hCdataItemList" << endl;return FAILURE;};
	RETURNCODE destroy(void);

	RETURNCODE clear(void){ dataItemList_t::clear(); return SUCCESS; };

	void  setEqual(void* pAclass); // a hCpayload required method
	void  duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType)
		{	if (ldType != pltDataItmLst) return false; else return true;	};
	RETURNCODE dumpSelf(int indent = 0){ 
		LOGIT(COUT_LOG,"%sDataItemList has no dump at this time.\n",Space(indent));return 0;};

	int append(hCdataitemList* pSrcItemsList);// needed for conditional.h during resolve
	int appendUnique(hCdataitemList* pSrcItemsList); // both return total size after appending
};
/*** - always used as a Conditional List target payload ***/
typedef hCcondList<hCdataitemList> condDataItemL_t;
typedef vector<hCdataitemList*> listOptrs2dataItemLists_t;

/***************************
 * transaction base class
 *      note that the resolved list now has dataitem, not pointers to transitive existance
 **************************/
class hCtransBase : public  hCobject
{// common code goes here
public:	
	typedef struct resolvedTrans_s
	{
		int   Number;
		short size;
		/* stevev 3/23/04 - changed because we must deal with all constants not just first
		short firstReqByte; // -1 means NOT constant, else value {DD spec says this is UINT8}
		*/
		short constCnt;  // 0 means none, else value is number of constants
		/*end stevev 3/23/04*/
	//	bool  isRequest;    // tells the type of pResolvedDataItemList 
		hCdataitemList  RslvdDataItemList;// points to the condList's retList - do not delete
		ddbItemList_t   dataPtrList;// vector of ptrs 2 hCitemBase
		itemIDlist_t    dataNumList;// vector of symbolids
		RETURNCODE destroy(void){};

		void clear(void){Number = -1; size = 0; constCnt=-1;
						 RslvdDataItemList.clear(); dataPtrList.clear(); dataNumList.clear();};
#if !defined(__GNUC__)
		struct
#endif // __GNUC__
		resolvedTrans_s(DevInfcHandle_t h):RslvdDataItemList(h) {clear();};
	}/*typedef*/ resolvedTrans_t; 

public:
	hCtransBase(DevInfcHandle_t h) :hCobject(h){};
	hCtransBase(const hCtransBase& s) :hCobject( (hCobject)s ){/*nothing to copy construct*/;};
	virtual ~hCtransBase()      /* we have no data*/{};
	RETURNCODE getDescription(int oSet = 0) 
	{ LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Virtual function entry2 into CtransBase.\n");
	  return FAILURE;   };
};


/***************************
 * transactionRespCodes holds the list of response codes that make up 
 * part of the transaction - base class holds all the info
 **************************/
class hCtransRespCodes : public hCRespCodeBase
{
public:
	hCtransRespCodes(DevInfcHandle_t h) : hCRespCodeBase(h){};
	hCtransRespCodes(const hCtransRespCodes& src) : hCRespCodeBase(src){}; 
																// parent is all that's needed
	//from:  ddl_rspcodes_choice(..
	//-> use CrespCodeBase's base class's CconditionalList.parseSelf()
//TEMP till conditional done
//	int size(void) const { return hCRespCodeBase::size();} ;
	RETURNCODE clear(void){ hCRespCodeBase::clear();return SUCCESS;/*was:return FAILURE;*/ };
	void setEqualTo( AcondRespCodeL_t* pAcndList ) {hCRespCodeBase::setEqualTo(pAcndList);};
};

#ifdef XMTRDD
 #ifndef XMTR
  #define XMTR
  #define DEFINEDHERE
 #endif
#endif
/***************************
 * The transaction itself 
 *			this class now has a lot of work to do
 **************************/
class hCtransaction : public hCtransBase
{
//	int               ResHndl;       // used for both of the following
//	resolvedTrans_t   resReqstStruct;// holds the resolved information (inc resHndl)
//	resolvedTrans_t   resReplyStruct;// holds the resolved information (inc resHndl)

	ulong             transNum;
	condDataItemL_t   request;
	condDataItemL_t   response;
	hCtransRespCodes  respCodes; //list of transaction response codes
	// global hCcmdDispatcher::
	//		 fpCalcWtTrans_t  fpWgtCalcs;
	hCcmdDispatcher*  pDispatch; // which instance to use the pointer into
	hCcommand*        pCmd;

	//condReferenceList_t condPostRcvActList;// size > 0 means there are some
	// encoded as conditional...not:   hCreferenceList RcvActList;
	condReferenceList_t	RcvActList;

	
	hCIndexIDValueVariableMap indexMap; // a index variable mapped to their valid values
										// index optimization for weighing
public:
	
	typedef struct transInfo_s
	{/* this is an abreviated form of resolvedTrans_t ( no list ) */
		int   Number;// transaction number
		short size;  // data packet size in bytes
		/*
		short firstReqByte; // -1 means NOT constant, else value
		*/
		short constCnt;  // 0 means none, else value is number of constants
		/*end stevev 3/23/04*/

		void clear(void){ Number = -1; size = 0; constCnt=-1;};
		struct transInfo_s& operator=(const hCtransBase::resolvedTrans_t& rtt)
									{Number   = rtt.Number;         size = rtt.size;
									 constCnt = rtt.constCnt;   return (*this);};
		struct transInfo_s& operator=(const struct transInfo_s& rtt)
									{Number   = rtt.Number;         size = rtt.size;
									 constCnt = rtt.constCnt;   return (*this);};
	}/*typedef*/ transInfo_t; 

protected:
//?	virtual     RETURNCODE resolveRequests(int& resHndl);
//?	virtual     RETURNCODE resolveReplys  (int& resHndl);
	transInfo_t getTransInfo(bool isRequest, resolvedTrans_t* pResolvedRet=NULL);
	//transInfo_t getTransInfo( int& resHndl, bool isRequest);
	//RETURNCODE 	setResolvedStruct(int& resHndl, hCdataitemList*  pRDIList, 
	//														resolvedTrans_t* pResolvedStruct);
	// changed setResolvedStruct to fill in the rest from the internal RslvdDataList
	RETURNCODE 	setResolvedStruct(resolvedTrans_t* pResolvedStruct);
	// stevev - helper functions to make generateReply packet smaller (and more understandable)
	RETURNCODE  setLocalIndexes(resolvedTrans_t* pReplyLst, indexUseList_t& indexLst);
	RETURNCODE  getREPLYactions(resolvedTrans_t* pReplyLst, 
								methodCallList_t& preActions, 
								methodCallList_t& postActions, bool isWrite = false);
	RETURNCODE extractReplyPkt  // stevev added 7mar05 - added msgs 17nov05
				(hPkt* pReplyPkt, methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
				 varPtrList_t &hasChangedList, bool isWrite, indexUseList_t* pSentIdex = NULL);

public:
	hCtransaction(DevInfcHandle_t h, hCcommand* pCmd);
	hCtransaction(const hCtransaction& src);
	RETURNCODE destroy(void);
	hCtransaction& operator=(const hCtransaction& src);
	virtual ~hCtransaction();

	void setWgtTrans(hCcmdDispatcher* pDpch){pDispatch=pDpch;};
	int  weigh(bool isRead, indexUseList_t& useIdx);
	// weighing optimization step - we do this once and use a look-up table in the weigh cycle
	RETURNCODE BuildIndexMap(void);
	int        sizeOfIndexMap(void) { return indexMap.size(); };
	hCIndexValue2Variable* getMap4Index(itemID_t indexID);// NULL on error
	RETURNCODE insert2Map(indexUseList_t & idxNvalue, hCVar* pVar) {
												return indexMap.appendUnique(idxNvalue,pVar);};
	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet);
	RETURNCODE setEqualTo( aCtransaction* pAtransaction );
	RETURNCODE getRespCodes(hCRespCodeList& rcList);// resolve and fill return list

	void setCmdPtr(hCcommand* pCMD){ pCmd = pCMD;};

	RETURNCODE clear(void) 
	{ request.clear(); 
	 response.clear(); 
	 respCodes.clear(); 
	 indexMap.clear();    transNum = -1; 
	 pDispatch=NULL;      pCmd=NULL; 
	 RcvActList.clear();  return SUCCESS;};

	// accessors
	ulong getTransNum(void) { return transNum; };
	transInfo_t getTransREQInfo(resolvedTrans_t* pRetResolved = NULL);
	transInfo_t getTransREPInfo(resolvedTrans_t* pRetResolved = NULL);

	RETURNCODE  getReqstList(varPtrList_t& itemList);
	RETURNCODE  getReqstList(itemIDlist_t&      itemList);

	RETURNCODE  getReplyList(varPtrList_t& itemList);
	RETURNCODE  getReplyList(itemIDlist_t&      itemList);

	// gets ALL possible payload lists..................uses aquirePayloadPtrList
	RETURNCODE  getREQlistOdiLists(vector<hCdataitemList *>& retList);
	RETURNCODE  getREPlistOdiLists(vector<hCdataitemList *>& retList);

	// yet another list resolution...I'll get it right eventually
	RETURNCODE  getReqstDIlist(hCdataitemList* pDIlist)
				{	return(request.resolveCond(pDIlist));	};
	RETURNCODE  getReplyDIlist(hCdataitemList* pDIlist)
				{	return(response.resolveCond(pDIlist));	};

	RETURNCODE  invalidateReply(void); //clear state on erroneous request
	hCcommand*  getCmdPtr(void){ return (pCmd);};
	RETURNCODE  invalidateIndexedReply(indexUseList_t& erList);

	// these two return false for ALL invalid,          -1: ALL VALID,   else cnt of valid
	int anyValidInReq(resolvedTrans_t* pReqLst=NULL); // 0: one or more invalid
	int anyValidInRep(resolvedTrans_t* pRepLst=NULL); // 0: All INValid, 1+: Cnt of Valid

	int responseSize(void); // number of variables in the response packet

	// generates the packet(s) executes correct actions, lists the opposite actions
	RETURNCODE 	generateRequestPkt(hPkt* ppReqPkt,  hMsgCycle_t* pMC=NULL, 
								   resolvedTrans_t* pTransLst=NULL);
	RETURNCODE generateReplyPkt(hPkt* ppReplyPkt,bool actOK = false,HreferenceList_t* pA=NULL);

	// opposite, extracts the information and updates vars etc
	RETURNCODE extractRequestPkt(hPkt* pReqstPkt, varPtrList_t &hasChangedList, 
		/* for simulator */						   indexUseList_t& infoList, bool isWrite);

	RETURNCODE extractReplyPkt (hPkt* pReplyPkt, methodCallList_t& eaLst, 
		hCmsgList&  noteMsgs, varPtrList_t &hasChangedList, indexUseList_t* pSentIdex = NULL);
	// stevev 7mar05 - different when write
	// stevev 17nov05- extract messages up to the rcvService function
	RETURNCODE extractReplyWritePkt(hPkt* pReplyPkt,      methodCallList_t&  eaLst,
		hCmsgList&  noteMsgs, varPtrList_t& hasChangedList, indexUseList_t* pSentIdex = NULL);

	RETURNCODE restoreINFOitems(indexUseList_t& infoList);

	RETURNCODE markReplyPending  (resolvedTrans_t* pTransLst=NULL);
#ifdef XMTR
	RETURNCODE getActionPtrs(ddbItemList_t& riL);		// get those *&^%$# items
	RETURNCODE doRcvActs(void);
#endif
};

typedef vector<hCtransaction>  TransList_t;
typedef vector<hCtransaction:: transInfo_t>  transInfoList_t;


/***************************
 * The transaction list container class 
 *					expectedListTag; // a SEQLIST ie ENUMERATOR_SEQLIST_TAG
 **************************/
class hCtransactionList : public TransList_t , public  hCobject
{//private:
	int resolutionHandle;
	hCcommand*  pCmd;
	// a working variable
//	CgenericConditional< CconditionalList,PAYLOADTYPE > wrkCond;// working variable
	//hCcmdDispatcher::fpCalcWtTrans_t fpWgt;
	hCcmdDispatcher*   pDispatcher;
protected:	//common code
	int getTransNumbByDataList(BYTE datLen, BYTE* pData, int skipNum, bool useRequest);

public:
	hCtransactionList(DevInfcHandle_t h);
	
	hCtransactionList(const hCtransactionList& src) : hCobject(src.devHndl())
		{ operator=( src ); };
	virtual ~hCtransactionList()	
	{	destroy();
		clear();
	};
	RETURNCODE destroy(void) 
		{  RETURNCODE rc = 0;
			FOR_this_iT(vector< hCtransaction >,this) 
			{	rc |= iT->destroy();  }
			clear();
			return rc;
		};

	hCtransactionList& operator=(const hCtransactionList& src);

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet = 0);
	RETURNCODE setEqualTo( AtransactionL_t* pAtransList );

	void setCmdPtr(hCcommand* pCMD){ pCmd = pCMD;};

	void setWgtVisitor(hCcmdDispatcher* pDispatch)
		{		
			pDispatcher = pDispatch;
			FOR_this_iT(vector< hCtransaction >,this) {	iT->setWgtTrans(pDispatch); }
		};

	// weighing optimization step - we do this once and use a look-up table in the weigh cycle
	RETURNCODE BuildIndexMap(void)
		{	FOR_this_iT(vector< hCtransaction >,this) 
			{	hCtransaction* pT = (hCtransaction*) &(*iT);
				pT->BuildIndexMap(); 
			}
			return SUCCESS;
		};	

	hCtransaction* getTransactionByNumber(int tNumber);// straight look up (no resHndle needed)

	// returns best guess transaction number or negative response code on error
	//		the byte cnt and data pointer are from the REQUEST packet of incoming command
	int getTransNumbByRequest(BYTE datLen, BYTE* pData, int skipNum = -1);
	int getTransNumbByReply  (BYTE datLen, BYTE* pData, int skipNum = -1);

};



class hCcmdRespCodes : public hCRespCodeBase
{
public:
	hCcmdRespCodes(DevInfcHandle_t h) 
		: hCRespCodeBase(h)			{ };
	hCcmdRespCodes(const hCcmdRespCodes& src) 
		: hCRespCodeBase(src)		{ };
};

/***************************
 * The command class 
 * 
 **************************/
class hCcommand  : public hCitemBase /* itemType 2 COMMAND_ITYPE  */
{
//	CattrBase* newAttr(struct itemAttrTbl&);//{NEWATTRNOTIMPLEMENTED("Ccommand");return NULL;};

//	CattrCmdNum(this);
	hCconditional<hCddlLong/*,aCattrCmdNum*/>       condCmdNum;// holds the value(s) 

//	CattrCmdOperation(this);
	hCconditional<hCddlLong/*,aCattrCmdOperation*/> condCmdOp; // holds the value(s) 

//	CattrCmdTrans(this);
	hCtransactionList transList; // a straight list of transactions (request/response are cond)
//	CattrCmdRspCd(this);
	hCcmdRespCodes  cmdRespCodes;

protected:
	int getTheWgt(int& transN, indexUseList_t& useIdx, bool isRd);
	// helper function for updateVarCommandUsage()
	RETURNCODE updateVarCmdWeight(int baseWgt, int transNum,
								  hCdataitemList& dataItmList,  bool isWrite);
public:
//	hCcommand(DevInfcHandle_t h, ITEM_TYPE item_type, itemID_t item_id, unsigned long DDkey);
	hCcommand(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCcommand(hCcommand*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
//	Ccommand(struct itemTbl&  itmR) : hCitemBase(itmR) {};
//	Ccommand(const Ccommand& v) { pFlatCmd = v.pFlatCmd;
//									CitemBase::operator=(v);};
//	hCcommand(DevInfcHandle_t h ) : hCitemBase(h) {};
	virtual ~hCcommand();
	RETURNCODE destroy(void);
	// visit the weight calculation algorithm upon the transactions
	void insertWgtVisitor(hCcmdDispatcher* pDispatch);// all transactions
	int  getWrWgt(int& transN, indexUseList_t& useIdx);
	int  getRdWgt(int& transN, indexUseList_t& useIdx);
	// weighing optimization step - we do this once and use a look-up table in the weigh cycle
	RETURNCODE BuildIndexMap(void)
	{	return (transList.BuildIndexMap());	}

	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*TODO:*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: command Attribute information is not accessable.\n");
		return rV;
	};
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
				{ hCitemBase::dumpSelf(); LOGIT(COUT_LOG,"\n"); return SUCCESS;};
RETURNCODE Label(wstring& retStr) { return baseLabel(NO_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(NO_LABEL, l); };

	RETURNCODE Read(wstring &value){value = L"ERROR:Not a Var"; return FAILURE;};

	// accessors //
	int                getCmdNumber(void);// -1 on error
	cmdOperationType_t getOperation(void);
	int                getTransCnt(void)   { return (transList.size());};
	RETURNCODE         getRespCodes(hCRespCodeList& rcList);// resolve and fill return list
	RETURNCODE         BuildRequest(hPkt* pPkt, hMsgCycle_t* pMsgCy );

	hCtransaction* getTransactionByNumber(int tNumber);// look up so we can do the cmd
	int            getTransNumbByRequestPkt(hPkt* pReqPkt, int skipTrans = -1);// -1 on error
	int            getTransNumbByReplyPkt  (hPkt* pReqPkt, int skipTrans = -1);// -1 on error

	// the following only return data items!! <constants in commands are skipped>
	RETURNCODE  getReqstData4Trans(int transNumber,hIndexUse_t& useIdx,varPtrList_t& itemList);
	RETURNCODE  getReqstData4Trans(int transNumber,hIndexUse_t& useIdx,itemIDlist_t& itemList);
	RETURNCODE  getReplyData4Trans(int transNumber,hIndexUse_t& useIdx,varPtrList_t& itemList);
	RETURNCODE  getReplyData4Trans(int transNumber,hIndexUse_t& useIdx,itemIDlist_t& itemList);

	RETURNCODE  markReplyPending(int transNumber);
	RETURNCODE  markReplyPending(varPtrList_t& varPtrList);//stevev 2/26/04

	RETURNCODE  invalidateReply( int transNumber);//clear state to INVALID on erroneous request

	// for host side
	int getRdTransNumbByItemID(itemID_t itemID);// -1 if not in Response Pkts
	int getWrTransNumbByItemID(itemID_t itemID);// -1 if not in Request  Pkts

	// xref methods
	RETURNCODE getAllOperations(vector<hCddlLong*>& retLst);
	RETURNCODE updateVarCommandUsage(void); // inserts cmd/trans# into rd/wr lists in vars used
};


#ifdef XMTRDD
 #ifdef DEFINEDHERE
  #undef XMTR
 #endif
#endif

#endif //_DDB_COMMAND_H

/**********************************************************************************************
 * NOTES:
 *	Transaction's packet generation is done by looping through the list of data items and 
 * asking each one to add itself to the buffer.  The interesting stuff happens when there is a 
 * mask present(hCdataItem.width).  
 * When the dataitem encounters a mask ( if length != 0 then this is a continuation of ) it
 * will calculate the byte width in the buffer (ie 0x8000 has width of 2) and pass the mask &
 * the byte width (length) to the dataitem's variable (via Add())  The variable will place the 
 * masked data into the buffer but will NOT increment the buffer ptr or byte count. The length 
 * (in bytes)will percolate back up to the transaction who will pass it to the next data item 
 * until done.
 * The data item determines it is done when the variable's Add() returns from processing a mask
 * with the least significant bit set. The Dataitem then increments the buffer and byte count 
 * and clears the length variable before returning to the transaction.
 *
 *	transType and INFO
 *	Transaction type has two significant bits. Bit 1 is set when processing a write command
 *  reply packet.  When this is set, ALL variables get immediatly transfered from the device
 *  value to the display value after being extracted into the device value.
 *  Bit 2 is clear on the first pass through the dataitem list.  On this pass all items marked
 *  INFO will have their entry values and state recorded so they may be restored later then all
 *  items qualified INDEX and all variables of type Index and all variables of class LOCAL will
 *  be extracted and set.
 *  When Bit 2 is set (isExtract) we're in pass 2 and everything is extracted.
 *  Depending on the application (SDC,XMTR,ANALYS) the values saved from the variables marked
 *  INFO will be replaced. In SDC, as soon as the packet has completed extraction, in XMTR-DD
 *  as soon as the reply packet has been constructed and in ANALYS...never(all data is stored).
 *
 * ResHndl
 * A resolution handle. Set the passed in handle to zero to force a new conditional resolution.
 * a new handle will be set.  Pass that returned handle to methods that you do not want 
 * re-resolved. If the passed in handle does not match the current resolution, the conditionals
 * will be resolved again.
 **********************************************************************************************
 */
/**********************************************************************************************
 *
 *   $History: ddbCommand.h $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 **********************************************************************************************
 */
