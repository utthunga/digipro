/*************************************************************************************************
 *
 * $Workfile: ddbCondResolution.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		this is where the work occurs
 *			This is broken out of ddbConditional.h to reduce the size of the file
 *			Note that this file is included at the end of ddbConditional
 *				and may be moved there at some time in the future.
 *
 *|*|*|*|*|*| this should ONLY be included at the end of ddbConditional.h!!!! |*|*|*&|*|*|*
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */

#ifndef _DDBCONDITIONAL_H

doNot = compile<<; // force an error

#else

// debug strings
extern char clauseStrings[8][CLAUSESTRMAXLEN];

template<class DESTTYPE, class PAYLDTYPE>
  			 					/* DESTTYPE is a conditional OR conditionalList */
#if _MSC_VER >= 1300  /* <<-- HOMZ - port to 2003, VS7 */ || defined(__GNUC__)
typename 
#endif
hCgenericConditional <DESTTYPE, PAYLDTYPE>::hCexpressDest* hCgenericConditional <DESTTYPE, PAYLDTYPE>
:: findDest(clauseType_t targetClaws) 
{
	hCexpressDest* pRetDest = NULL;
	typename dest_List::iterator yT; // PAW see below 03/03/09
	for (/*dest_List::iterator PAW see above */ yT = destElements.begin(); 
							 yT < destElements.end() ;   yT++)
	{
		if ( yT->destType == targetClaws)
		{// this is the one
			return (hCexpressDest*)&(*yT);  // HOMZ - port to 2003, VS7 was::> return (hCexpressDest*)yT;
		}
		// else continue searching
	}
// detect an error
//  two clause types are optional, we don't count them as errors
#if 0 /* as per wally - remove warning, they get what they get */
	if(targetClaws == cT_IF_isFALSE)
	{
		LOGIT(CLOG_LOG,"WARNING: Expression conditional needs an ELSE statement.\n");
		return NULL;
	}
	if(targetClaws == ct_SEL_isDEFAULT)
	{
		LOGIT(CLOG_LOG,"WARNING: Expression conditional needs an DEFAULT statement.\n");
		return NULL;
	}
#endif /* 0 */
		 //
// debug the other errors
//	cout << "____________ Target:" << clauseStrings[targetClaws] << " from " << destElements.size() << " dest elements."<< endl;
/* no else gives this dump....
	for (yT = destElements.begin(); yT < destElements.end() ; yT++)
	{
		yT->dumpSelf();
	}
**/
	return NULL;// failure
};

template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
:: resolveCond(PAYLDTYPE* pRetVal, bool isAtest)
{
	//PAYLDTYPE* pRet  = NULL;
	// stevev 07sep05 - force a isTest when OFFLINE
	/* if ( devPtr()->getDeviceState() == (int)ds_OffLine )
	*/

	unsigned char at_end = true;	// PAW 03/03/09
#ifndef _WIN32_WCE	// debugging PAW 18/06/09 replaced 27/07/09
	if ( devPtr()->getDeviceState() == (int)ds_OffLine ||  // never came online
		 devPtr()->getDeviceState() &  (int)ds_OffIniting )// or (off|on line)initializing
		isAtest = true;
#endif
	RETURNCODE rc = SUCCESS;
	// pRetVal = NULL;
	if ( pRetVal == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: genericConditional's resolveCond was passed a null pointer.\n");
		return COND_RETURN_VALUE_BASE;
	}
	CValueVarient exprVal; 
	
	switch ( priExprType )
	{
	case aT_IF_expr:    // 1
		{// result is T/F ::> THEN/ELSE

			hCexpressDest* pLocalDest = NULL;  // HOMZ

			clauseType_t exprTargetClause = cT_Unknown;
			exprVal = priExpression.resolve(NULL,isAtest);

			if (exprVal.vType == CValueVarient::invalid || exprVal.vIsValid == false)
			{// error abort - return NULL
				//useless stmnt...exprTargetClause = cT_Unknown;
				/* VMKP added on 300104 (If expression resolving return InValid,
				make the condition as false,  this fix results in SDC menu structure
				equivalent with 275, tested for E&H PROMAG) */
				exprVal.vValue.bIsTrue = false;
				exprTargetClause = cT_IF_isFALSE;
				pLocalDest = findDest(exprTargetClause);
				/* VMKP added on 300104 */
			}
			else
			if (exprVal.vType == CValueVarient::isBool)
			{
				if ( exprVal.vValue.bIsTrue == true ) 
					{ exprTargetClause = cT_IF_isTRUE;}
				else{ exprTargetClause = cT_IF_isFALSE;}
				pLocalDest = findDest(exprTargetClause);
			}
			else
			if (exprVal.vType == CValueVarient::isVeryLong)
			{
				if ( exprVal.vValue.longlongVal == 0 ) 
					{ exprTargetClause = cT_IF_isFALSE;}
				else{ exprTargetClause = cT_IF_isTRUE; }
				pLocalDest = findDest(exprTargetClause);
			}
			else
			if (exprVal.vType == CValueVarient::isIntConst)
			{
				if ( exprVal.vValue.iIntConst == 0 ) 
					{ exprTargetClause = cT_IF_isFALSE;}
				else{ exprTargetClause = cT_IF_isTRUE; }
				pLocalDest = findDest(exprTargetClause);
			}
			else
			if (exprVal.vType == CValueVarient::isFloatConst)
			{
				if ( exprVal.vValue.fFloatConst == 0.0 ) 
					{ exprTargetClause = cT_IF_isFALSE;}
				else{ exprTargetClause = cT_IF_isTRUE; }
				pLocalDest = findDest(exprTargetClause);
			}
			else
			if (exprVal.vType == CValueVarient::isDepIndex)
			{
				int dex = exprVal.vValue.depIndex;
				if ( dex >= 0 && dex < (int)destElements.size() ) 	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
				{	pLocalDest = &(destElements[dex]);	}
				else
				{//leave the dest unknown
					LOGIT(CLOG_LOG,"    resolveCond's expression resolved to an "
						"out-of-range destination.\n");
					pLocalDest = NULL;
				}
			}
			else // string or unknown
			{// error
				exprTargetClause = cT_Unknown;
				LOGIT(CLOG_LOG,"          \\/ string or unknown varient type.\n");
				pLocalDest = NULL;
			}

			if (  exprTargetClause == cT_Unknown || 
			     (pLocalDest != NULL && pLocalDest->destType == cT_Unknown))
			{
				LOGIT(CLOG_LOG,"    resolveCond's expression resolved to INVALID\n");
				rc = COND_RESOLVED_TO_INVALID;
			}	// do nothing else, return null
			else
			if (pLocalDest == NULL)
			{
				if ( exprTargetClause == cT_IF_isFALSE ) // no else statement found
				{
					// valid but bad null return
//stevev 9nov10 - disengage					LOGIT(CLOG_LOG,"    local NULL, clause FALSE.\n");
					DEBUGLOG(CLOG_LOG,"local NULL, clause FALSE.(debug only.. 4 brkpt)\n");
				}
				else
				{
					LOGIT(CLOG_LOG,"    ERROR: resolveCond's 'if' expression has a NULL "
						    "local destination\n");
					rc = COND_IF_WITH_NULL;
				}
			}
			else
			{// localDest is valid
				if (pLocalDest->destType == cT_Direct)
				{
					if (pLocalDest->pPayload != NULL )
					{
						*pRetVal = *(pLocalDest->pPayload);// copy information
					}
					else
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:resolveCond's primary IF expression IS Direct but has "
							 "a NULL payload ptr.\n");
					}
				}
				else
				{	//cT_CASE_expr = 3,	// implies there is an expressionL					
					//cT_IF_isTRUE,    // 5 THEN
					//cT_IF_isFALSE,   // 6 ELSE
					//ct_SEL_isDEFAULT // 7 always true
					if (pLocalDest->pCondDest != NULL )
					{
						pLocalDest->pCondDest->resolveCond(pRetVal, isAtest);
					}
					else
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:resolveCond's primary IF expression is NOT Direct but has "
							 "a NULL condDest ptr.\n");
						pRetVal = NULL;
					}
				}
			}
		}
		break;
	case aT_SEL_expr:	// 2
		{// result is a value that must match the expression value in the case list
		 /* stevev 12aug10 - cleaned up this case to do without the versions and be
		 	comprehensible at the same time. */
			hCexpressDest* pLocalDest    = NULL;
			hCexpressDest* pIteratorDest = NULL;
			CValueVarient caseVal; 

			exprVal = priExpression.resolve(NULL,isAtest);
			// for each dest
			for (typename dest_List::iterator yT = destElements.begin(); yT != destElements.end(); ++yT)
			{// yT is a ptr2a hCexpressDest
			//	if a case
				pIteratorDest  = (hCexpressDest*)&(*yT);//12aug10 - 2005, don't use iterator
				if ( pIteratorDest->destType == cT_CASE_expr)
				{
			//		do expression
					caseVal = pIteratorDest->destExpression.resolve(NULL,isAtest);
			//		check for equality - use the pri method since it isn't a static function
					CValueVarient rVV = priExpression.execBinaryBoolean(eet_EQ, caseVal, exprVal);
					if ( rVV.vType == CValueVarient::isBool && rVV.vValue.bIsTrue == true )
					{	//		if equal
						pLocalDest = pIteratorDest;
						//			we be done
						break;//  PAW added see below 03/03/09
					}	
				// debug stmt
					else if(rVV.vType != CValueVarient::isBool)
					{  LOGIT(CLOG_LOG,"ISEQUAL returned a non boolean varient.\n");
					}
			//		else - continue with next dest
				}
				else 
				if ( yT->destType == ct_SEL_isDEFAULT)//	else if default
				{
			//		we be done (verify there are no more in the list)
					pLocalDest = pIteratorDest;
					break; // stevev 12aug10 - for symmetry
				}
				else // error - illegal or unknown clause type
				{
					LOGIT(CLOG_LOG|CERR_LOG,
						"ERROR: select resolution has an illegal or unknown destination type.\n");
					rc = COND_SLCT_WITH_BAD_DEST;
				}
			}// next		 
			
			if (pLocalDest != NULL)
				//stevev 12aug10..error and condition can never happen::
				//if (pLocalDest != NULL && pLocalDest != destElements.end())
			{// if found a match
			    // localDest is valid
				//	  return payload || resolved conditional
				if (pLocalDest->destType == cT_Direct)
				{
					*pRetVal = *(pLocalDest->pPayload);
				}
				else
				{	//cT_CASE_expr = 3,	// implies there is an expressionL					
					//ct_SEL_isDEFAULT // 7 always true
					pLocalDest->pCondDest->resolveCond(pRetVal, isAtest);
				}
			}
			else
			{// else return error
				LOGIT(CLOG_LOG,"    resolveCond's select expression has a NULL local destination\n");
				LOGIT(CLOG_LOG,"               -- may be due to no default selection being available.\n");
				rc = COND_SLCT_WITH_BAD_DEST;
			}
		}
		break;
	case aT_Direct:		//  = 4
		{
			if ( destElements.size() > 0 && destElements[0].pPayload != NULL)
				*pRetVal = *(destElements[0].pPayload); 
			else
			{
				LOGIF(LOGP_NOT_TOK)(CLOG_LOG|CERR_LOG,"ERROR: resolveCond has Direct with a NULL payload\n");
				pRetVal = NULL;
				rc = COND_DIRECT_W_NULL_PAYLOAD;
			}
		}
		break;
	case aT_Unknown:	//  = 0,
	default:
		{// nop - return NULL				extern char axiomStrings
			LOGIF(LOGP_NOT_TOK)(CLOG_LOG|CERR_LOG,"ERROR: resolveCond has a bad primary axiom type (%s)\n",
						axiomStrings[priExprType]);
			rc = COND_BAD_PRI_AXIOM_TYPE;;
		}
		break;
	}

	return rc;
};


#endif // in conditional.h

/*************************************************************************************************
 *
 *   $History: ddbCondResolution.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
