/*************************************************************************************************
 *
 * $Workfile: DDBpayload.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the abstract base class required of all payload classes
 *		Provides a common interface for conditional handling
 *		9/10/2	sjv	created
 *
 * #include "ddbPayload.h"
 */

#ifndef _DDBPAYLOAD_H
#define _DDBPAYLOAD_H


#include "pvfc.h"

#pragma warning (disable : 4786) 
#include "ddbdefs.h"
#include <iostream>
#include "ddbGlblSrvInfc.h"


class hCpayload  // an (almost)abstract base class
{
public:
/*	
void*  hCpayload::getNewPayload(int paySet) // { return getNewPayloadClass(paySet);};

//void*      getNewPayloadClass(int paySet)
{
	void * pr = (void*)0 ;

	switch(paySet)
	{
	case COND2DILIST:		pr = (void*) new	CdataitemList		; break;
	case COND2TRLIST:		pr = (void*) new	CtransactionList	; break;		
//	case DEST2COND:			pr = (void*) new	Cconditional		; break;
//	case DEST2CONDLIST:		pr = (void*) new	ConditionalList		; break;
	case COND2PAYDDLSTR:	pr = (void*) new	CddlString			; break;
	case COND2PAYDDLBSTR:	pr = (void*) new	CbitString			; break;
	case COND2PAYDDLONG:	pr = (void*) new	CddlLong			; break;

	case COND2PAYMENU	   :pr = (void*) new	CmenuItem			; break;
	case COND2PAYMLIST	   :pr = (void*) new	CmenuList			; break;
	case COND2PAYRESPCD	   :pr = (void*) new	CrespCode			; break;
	case COND2RCLIST	   :pr = (void*) new	CrespCodeList		; break;
	case COND2PAYTYPETYPE  :pr = (void*) new	CvarTypeDescriptor	; break;
	case COND2PAYENUM	   :pr = (void*) new	CenumDesc			; break;
	case COND2PAYENUMLIST  :pr = (void*) new	CenumList			; break;
//	case COND	   :pr = (void*) new	C			; break;
	default:
		cerrxx<< "ERROR: New Payload for the set number ("<<paySet<<") was NOT found."<<endl;
		break;
	}//endswitch

	return pr;
};

*/
	// this sets the payload class from an abstract payload class in ddlapi
	// each payload must override it since they are the only ones to know 
	//      what their matching dllapi abstract class is
	//
	// We cannot make these pure virtual methods or we wouldn't be able
	//		to refer to their children as payloads
	//*?* virtual void  setEqual(void* pAclass) = 0;                      // a required method
	virtual void  setEqual(void* pAclass) PVFC( L"hCpayload_a" );                 // a required method
//	{
//		std::cerrxx<<"+ERROR+++++ Payload Virtual function 'setEqual' called."<<std::endl;
//		return;
//	};
	// each payload must tell if the abstract payload type is correct for itself
	//*?* virtual boolT validPayload(payloadType_t ldType) = 0;
	virtual bool validPayload(payloadType_t ldType) RPVFC( "hCpayload_b",false );
//	{
//		std::cerrxx<<"+ERROR+++++ Payload Virtual function 'validPayload' called."
//		<<std::endl; return isFALSE;
//	};
	
	virtual void duplicate(hCpayload* pPayLd, bool replicate,itemID_t parentID=0) 
																		PVFC( L"hCpayload_c" );
	
	/* make pPayload NULL to generate an empty item */
	static void*  newPayload(DevInfcHandle_t h, payloadType_t ldType, hCpayload* pPayload);

	virtual ~hCpayload(){};
	virtual RETURNCODE destroy(void) RPVFC( L"hCpayload_d",0 );
};
//	boolT validPayload(payloadType_t ldType){ if (ldType != ) return isFALSE; else return isTRUE;};

#endif//_DDBPAYLOAD_H

/*************************************************************************************************
 *
 *   $History: DDBpayload.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
