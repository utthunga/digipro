/**********************************************************************************************
 *
 * $Workfile: foundation.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		the home of those tiny little classes that everything is built on
 * 4/25/02	sjv	created
 */


#include "foundation.h"
#include "Char.h"
#include "DDLDEFS.H"


static const int halfShift  = 10; /* used for shifting by 10 bits */
static const UTF32 halfBase = 0x0010000UL;
static const UTF32 halfMask = 0x3FFUL;

#define UNI_SUR_HIGH_START  (UTF32)0xD800
#define UNI_SUR_HIGH_END    (UTF32)0xDBFF
#define UNI_SUR_LOW_START   (UTF32)0xDC00
#define UNI_SUR_LOW_END     (UTF32)0xDFFF

/*
 * Once the bits are split out into bytes of UTF-8, this is a mask OR-ed
 * into the first byte, depending on how many bytes follow.  There are
 * as many entries in this table as there are UTF-8 sequence types.
 * (I.e., one byte sequence, two byte... etc.). Remember that sequencs
 * for *legal* UTF-8 will be 4 or fewer bytes total.
 */
static const UTF8 firstByteMark[7] = { 0x00, 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };
/*
 * Index into the table below with the first byte of a UTF-8 sequence to
 * get the number of trailing bytes that are supposed to follow it.
 * Note that *legal* UTF-8 values can't have 4 or 5-bytes. The table is
 * left as-is for anyone who may want to do such conversion, which was
 * allowed in earlier algorithms.
 */
static const char trailingBytesForUTF8[256] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
};

/*
 * Magic values subtracted from a buffer value during UTF8 conversion.
 * This table contains as many values as there might be trailing bytes
 * in a UTF-8 sequence.
 */
static const UTF32 offsetsFromUTF8[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 
		     0x03C82080UL, 0xFA082080UL, 0x82082080UL };

#ifdef _DEBUG
static int erCnt = 0;
#endif

/* --------------------------------------------------------------------- */

/* The interface converts a whole buffer to avoid function-call overhead.
 * Constants have been gathered. Loops & conditionals have been removed as
 * much as possible for efficiency, in favor of drop-through switches.
 * (See "Note A" at the bottom of the file for equivalent code.)
 * If your compiler supports it, the "isLegalUTF8" call can be turned
 * into an inline function.
 */

/* --------------------------------------------------------------------- */

ConversionResult ConvertUTF16toUTF8 ( const UTF16** sourceStart, const UTF16* sourceEnd, 
	UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags) 
{
    ConversionResult result = conversionOK;
    const UTF16* source = *sourceStart;
    UTF8* target = *targetStart;
    while (source < sourceEnd) 
	{
	UTF32 ch;
	unsigned short bytesToWrite = 0;
	const UTF32 byteMask = 0xBF;
	const UTF32 byteMark = 0x80; 
	const UTF16* oldSource = source; /*In case we have to back up because of target overflow.*/
	ch = *source++;
	/* If we have a surrogate pair, convert to UTF32 first. */
	if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_HIGH_END) 
	{
	    /* If the 16 bits following the high surrogate are in the source buffer... */
	    if (source < sourceEnd) 
		{
		UTF32 ch2 = *source;
		/* If it's a low surrogate, convert to UTF32. */
		if (ch2 >= UNI_SUR_LOW_START && ch2 <= UNI_SUR_LOW_END) {
		    ch = ((ch - UNI_SUR_HIGH_START) << halfShift)
			+ (ch2 - UNI_SUR_LOW_START) + halfBase;
		    ++source;
		} 
		else if (flags == strictConversion) 
		{ /* it's an unpaired high surrogate */
		    --source; /* return to the illegal value itself */
		    result = sourceIllegal;
		    break;
		}
	    } 
		else 
		{ /* We don't have the 16 bits following the high surrogate. */
		--source; /* return to the high surrogate */
		result = sourceExhausted;
		break;
	    }
	} 
	else if (flags == strictConversion) 
	{
	    /* UTF-16 surrogate values are illegal in UTF-32 */
	    if (ch >= UNI_SUR_LOW_START && ch <= UNI_SUR_LOW_END) 
		{
		--source; /* return to the illegal value itself */
		result = sourceIllegal;
		break;
	    }
	}
	/* Figure out how many bytes the result will require */
	if (ch < (UTF32)0x80) {	     bytesToWrite = 1;
	} else if (ch < (UTF32)0x800) {     bytesToWrite = 2;
	} else if (ch < (UTF32)0x10000) {   bytesToWrite = 3;
	} else if (ch < (UTF32)0x110000) {  bytesToWrite = 4;
	} else {			    bytesToWrite = 3;
					    ch = UNI_REPLACEMENT_CHAR;
	}

	target += bytesToWrite;
	if (target > targetEnd) {
	    source = oldSource; /* Back up source pointer! */
	    target -= bytesToWrite; result = targetExhausted; break;
	}
	switch (bytesToWrite) { /* note: everything falls through. */
	    case 4: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 3: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 2: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 1: *--target =  (UTF8)(ch | firstByteMark[bytesToWrite]);
	}
	target += bytesToWrite;
    }
    *sourceStart = source;
    *targetStart = target;
    return result;
}

//
//
//
static Boolean isLegalUTF8(const UTF8 *source, int length) 
{
    UTF8 a;
    const UTF8 *srcptr = source+length;
    switch (length) 
	{
    default: 
		return false;
	/* Everything else falls through when "true"... */
    case 4: 
		if ((a = (*--srcptr)) < 0x80 || a > 0xBF) 
		{
			return false;
		}
    case 3: 
		if ((a = (*--srcptr)) < 0x80 || a > 0xBF) 
		{
			return false;
		}
    case 2: 
		if ((a = (*--srcptr)) > 0xBF) 
		{
			return false;
		}

		switch (*source) 
		{
			/* no fall-through in this inner switch */
			case 0xE0: if (a < 0xA0) return false; break;
			case 0xED: if (a > 0x9F) return false; break;
			case 0xF0: if (a < 0x90) return false; break;
			case 0xF4: if (a > 0x8F) return false; break;
			default:   if (a < 0x80) return false;
		}

    case 1: 
		if (*source >= 0x80 && *source < 0xC2) 
		{
			return false;
		}
    }
    if (*source > 0xF4) 
	{
		return false;
	}
    return true;
}

/*
 * Exported function to return whether a UTF-8 sequence is legal or not.
 * This is not used here; it's just exported.
 *modified 09jul09 by stevev to check all the way to sourceEnd    /
Boolean isLegalUTF8Sequence(const UTF8 *source, const UTF8 *sourceEnd) 
{
    int length = trailingBytesForUTF8[*source]+1;
    if (source+length > sourceEnd) 
	{
		return false;
    }
    return isLegalUTF8(source, length);
}
*** end obsoleted code ****/

// actually it's sourcePastEnd...eg the trailing null or beginning of next utf8 char
bool isLegalUTF8Sequence(const UTF8 *source, const UTF8 *sourceEnd) 
{
    int length = trailingBytesForUTF8[*source]+1;
	if (source+length >= sourceEnd)
	{
		return 0;// false, not utf8
	}

	Boolean isLegal = 1;
	while ( (source+length < sourceEnd) && isLegal )
	{
		isLegal = isLegalUTF8(source, length);
		source += length;
		length  = trailingBytesForUTF8[*source]+1;
	}
#ifdef _INVENSYS	
	/* stevev 12aug11 - this looks incorrect. I believe it will always force a false return */
	if (source+length >= sourceEnd)	// J.U. 19.07.2011 _INVENSYS
	{
		isLegal = 0;
	}
#endif	
    return isLegal;
}

ConversionResult 
ConvertUTF8toUTF16 (const UTF8** sourceStart, const UTF8* sourceEnd, UTF16** targetStart, 
												 UTF16* targetEnd, ConversionFlags flags) 
{
    ConversionResult result = conversionOK;
    const UTF8* source = *sourceStart;
    UTF16* target = *targetStart;
    
	while (source < sourceEnd) 
	{
		UTF32 ch = 0;
		unsigned short extraBytesToRead = trailingBytesForUTF8[*source];
		if (source + extraBytesToRead >= sourceEnd) 
		{
			result = sourceExhausted; break;
		}
		/* Do this check whether lenient or strict */
		if (! isLegalUTF8(source, extraBytesToRead+1)) 
		{
			result = sourceIllegal;
			break;
		}
		/*
		 * The cases all fall through. See "Note A" below.
		 */
		switch (extraBytesToRead) 
		{
			case 5: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
			case 4: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
			case 3: ch += *source++; ch <<= 6;
			case 2: ch += *source++; ch <<= 6;
			case 1: ch += *source++; ch <<= 6;
			case 0: ch += *source++;
		}
		ch -= offsetsFromUTF8[extraBytesToRead];

		if (target >= targetEnd) 
		{
			source -= (extraBytesToRead+1); /* Back up source pointer! */
			result = targetExhausted; break;
		}
		if (ch <= UNI_MAX_BMP) 
		{ /* Target is a character <= 0xFFFF */
			/* UTF-16 surrogate values are illegal in UTF-32 */
			if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END) 
			{
				if (flags == strictConversion) 
				{
					source -= (extraBytesToRead+1); /* return to the illegal value itself */
					result = sourceIllegal;
					break;
				} 
				else 
				{
					*target++ = UNI_REPLACEMENT_CHAR;
				}
			} 
			else 
			{
				*target++ = (UTF16)ch; /* normal case */
			}
		} 
		else 
		if (ch > UNI_MAX_UTF16) 
		{
			if (flags == strictConversion) 
			{
				result = sourceIllegal;
				source -= (extraBytesToRead+1); /* return to the start */
				break; /* Bail out; shouldn't continue */
			} 
			else 
			{
				*target++ = UNI_REPLACEMENT_CHAR;
			}
		} 
		else 
		{/* target is a character in range 0xFFFF - 0x10FFFF. */
			if (target + 1 >= targetEnd) 
			{
				source -= (extraBytesToRead+1); /* Back up source pointer! */
				result = targetExhausted; break;
			}
			ch -= halfBase;
			*target++ = (UTF16)((ch >> halfShift) + UNI_SUR_HIGH_START);
			*target++ = (UTF16)((ch & halfMask) + UNI_SUR_LOW_START);
		}
    }//wend
    *sourceStart = source;
    *targetStart = target;
    return result;
}


/* --------------------------------------------------------------------- */

ConversionResult ConvertUTF32toUTF8(
    const UTF32** sourceStart, const UTF32* sourceEnd, 
    UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags
) 
{
    ConversionResult result = conversionOK;
    const wchar_t* source =  reinterpret_cast<const wchar_t*>(*sourceStart);
    UTF8* target = *targetStart;
    while (source < sourceEnd) {
    wchar_t ch;
    unsigned short bytesToWrite = 0;
    const UTF32 byteMask = 0xBF;
    const UTF32 byteMark = 0x80; 
    ch = *source++;
    if (flags == strictConversion ) {
        /* UTF-16 surrogate values are illegal in UTF-32 */
        if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END) {
        --source; /* return to the illegal value itself */
        result = sourceIllegal;
        break;
        }
    }
    /*
     * Figure out how many bytes the result will require. Turn any
     * illegally large UTF32 things (> Plane 17) into replacement chars.
     */
    if (ch < (UTF32)0x80) {      bytesToWrite = 1;
    } else if (ch < (UTF32)0x800) {     bytesToWrite = 2;
    } else if (ch < (UTF32)0x10000) {   bytesToWrite = 3;
    } else if (ch <= UNI_MAX_LEGAL_UTF32) {  bytesToWrite = 4;
    } else {                bytesToWrite = 3;
                        ch = UNI_REPLACEMENT_CHAR;
                        result = sourceIllegal;
    }

    target += bytesToWrite;
    if (target > targetEnd) {
        --source; /* Back up source pointer! */
        target -= bytesToWrite; result = targetExhausted; break;
    }
    switch (bytesToWrite) { /* note: everything falls through. */
        case 4: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
        case 3: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
        case 2: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
        case 1: *--target = (UTF8) (ch | firstByteMark[bytesToWrite]);
    }
    target += bytesToWrite;
    }
    *sourceStart = reinterpret_cast<const UTF32*>(source);
    *targetStart = target;
    return result;
}

/* --------------------------------------------------------------------- */

ConversionResult ConvertUTF8toUTF32(
    const UTF8** sourceStart, const UTF8* sourceEnd, 
    UTF32** targetStart, UTF32* targetEnd, ConversionFlags flags
) 
{
    ConversionResult result = conversionOK;
    const UTF8* source = *sourceStart;
    wchar_t* target = reinterpret_cast<wchar_t*>(*targetStart);
    while (source < sourceEnd) {
    wchar_t ch = 0;
    unsigned short extraBytesToRead = trailingBytesForUTF8[*source];
    if (source + extraBytesToRead >= sourceEnd) {
        result = sourceExhausted; break;
    }
    /* Do this check whether lenient or strict */
    if (! isLegalUTF8(source, extraBytesToRead+1)) {
        result = sourceIllegal;
        break;
    }
    /*
     * The cases all fall through. See "Note A" below.
     */
    switch (extraBytesToRead) {
        case 5: ch += *source++; ch <<= 6;
        case 4: ch += *source++; ch <<= 6;
        case 3: ch += *source++; ch <<= 6;
        case 2: ch += *source++; ch <<= 6;
        case 1: ch += *source++; ch <<= 6;
        case 0: ch += *source++;
    }
    ch -= offsetsFromUTF8[extraBytesToRead];

    if (target >= targetEnd) {
        source -= (extraBytesToRead+1); /* Back up the source pointer! */
        result = targetExhausted; break;
    }
    if (ch <= UNI_MAX_LEGAL_UTF32) {
        /*
         * UTF-16 surrogate values are illegal in UTF-32, and anything
         * over Plane 17 (> 0x10FFFF) is illegal.
         */
        if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END) {
        if (flags == strictConversion) {
            source -= (extraBytesToRead+1); /* return to the illegal value itself */
            result = sourceIllegal;
            break;
        } else {
            *target++ = UNI_REPLACEMENT_CHAR;
        }
        } else {
        *target++ = ch;
        }
    } else { /* i.e., ch > UNI_MAX_LEGAL_UTF32 */
        result = sourceIllegal;
        *target++ = UNI_REPLACEMENT_CHAR;
    }
    }
    *sourceStart = source;
    *targetStart = reinterpret_cast<UTF32*>(target);
    return result;
}



/********************** A global foundation function ************************/
string TStr2AStr(const wstring& src)
{
	string dest;
	int nSize = src.size();// 12aug10   
	int nSuperSize = 4*nSize;
	if( nSize )
	{
		wchar_t *szsrc = new wchar_t[nSize+2];
		char   *szdest = new  char[nSuperSize+2];//The UTF-8 is larger string than the source

		 memset(szsrc, 0, (nSize+2)     * sizeof(wchar_t));// fixed 12aug10
		 memset(szdest,0,(nSuperSize+2) * sizeof(char)   );// fixed 12aug10

		wcsncpy( szsrc, src.c_str(), nSize+1 );//size +1 to include null termination

		UTF8 * pUTF8     = (UTF8 *)szdest;
		UTF8 * pUTF8_end = (UTF8 *)(&szdest[nSuperSize+1]);

		/*
			Determine the conversion functions to use at compile-time using 
			the WCHAR_MAX C-standard specified macro.
			Ref: http://stackoverflow.com/a/9885037
		*/
#if WCHAR_MAX > 0xFFFFu // 4-byte wide characters.
		// changed to address of 30/07/09 PAW
		UTF32* pUTF32     = reinterpret_cast<UTF32*>(szsrc);
		UTF32* pUTF32_end = reinterpret_cast<UTF32*>(&szsrc[nSize+1]);

		ConversionResult cr = ConvertUTF32toUTF8 ( (const UTF32 **)&pUTF32, (const UTF32 *)pUTF32_end, 
								   (UTF8  **)&pUTF8,         pUTF8_end,    lenientConversion);
#else // WCHAR_MAX - 2-byte wide characters.
		// changed to address of 30/07/09 PAW
		UTF16* pUTF16     = reinterpret_cast<unsigned short*>(szsrc);
		UTF16* pUTF16_end = reinterpret_cast<unsigned short*>(&szsrc[nSize+1]);

		ConversionResult cr = ConvertUTF16toUTF8 ( (const UTF16 **)&pUTF16, (const UTF16 *)pUTF16_end, 
								   (UTF8  **)&pUTF8,         pUTF8_end,    lenientConversion);
#endif // WCHAR_MAX


	
		if ( conversionOK == cr )
		{
			dest = szdest;
		}
#ifdef _DEBUG
		else
		{
			// leave dest empty
			erCnt++;// mainly for a breakpoint
		}
#endif
		delete [] szdest;
		delete [] szsrc;
	}
	return dest;
}



// UTF8 to unicode conversion
wstring AStr2TStr(const string& src )
{
	wstring dest;
	int nSize = strlen(src.c_str());
	int nSuperSize = 4*nSize;
	if( nSize )
	{
		char *szsrc = new char[nSuperSize+2];
		//unicode will be the same size or smaller, unless it is not valid utf8
		wchar_t *szdest = new wchar_t[nSuperSize+2];						

		//memset(szsrc,0,sizeof(szsrc));
		//memset(szdest,0,sizeof(szdest));
		// as reported by Kari at beamex 9/3/10
		memset(szsrc, 0,sizeof(char)*(nSuperSize+2));
		memset(szdest,0,sizeof(wchar_t)*(nSuperSize+2));

		strncpy(szsrc,src.c_str(),nSize+1);//make sure to copy the null termination

		if( !isLegalUTF8Sequence( (const UTF8 *)szsrc, (const UTF8 *)&szsrc[nSize+1] ) )
		{
			char *szlatindest = new char[nSuperSize+2];

			memset(szlatindest,0,sizeof(char)*(nSuperSize+2));
													// was: 13aug10-fri: sizeof(szlatindest));

			strncpy( szsrc, src.c_str(), nSize+1 );
			//This may convert into something bigger!
			latin2utf8( szsrc, szlatindest, nSuperSize+1 );
														
			strncpy( szsrc, szlatindest, nSuperSize+1 );
			nSize = strlen(szsrc);//re-calculate the new string size.

			delete [] szlatindest;
		}
		else
		{// This should be a straight UTF8 to Unicode conversion.
			strncpy( szsrc, src.c_str(), nSize+1 );
		}
		UTF8* pUTF8       = reinterpret_cast<unsigned char*>(szsrc);
		UTF8* pUTF8_end   = reinterpret_cast<unsigned char*>(&szsrc[nSize+1]);
														// changed to address of 21/07/09 PAW

		/*
			Determine the conversion functions to use at compile-time using 
			the WCHAR_MAX C-standard specified macro.
			Ref: http://stackoverflow.com/a/9885037
		*/
#if WCHAR_MAX > 0xFFFFu // 4-byte wide characters.
		UTF32* pUTF32     = reinterpret_cast<UTF32*>(szdest);
		UTF32* pUTF32_end = reinterpret_cast<UTF32*>(&szdest[nSize+1]);	
														// changed to address of 21/07/09 PAW
		ConversionResult cr = 
		   ConvertUTF8toUTF32 ( (const UTF8 **)&pUTF8, (UTF8 *)pUTF8_end, 
		                                      &pUTF32,        pUTF32_end, lenientConversion);

#else // WCHAR_MAX - 2-byte wide characters.
		UTF16* pUTF16     = reinterpret_cast<unsigned short*>(szdest);
		UTF16* pUTF16_end = reinterpret_cast<unsigned short*>(&szdest[nSize+1]);	
														// changed to address of 21/07/09 PAW
		ConversionResult cr = 
		   ConvertUTF8toUTF16 ( (const UTF8 **)&pUTF8, (UTF8 *)pUTF8_end, 
		                                      &pUTF16,        pUTF16_end, lenientConversion);
#endif // WCHAR_MAX

		int cl = wcslen(szdest);
		if ( conversionOK == cr  &&  cl < (nSuperSize + 1) )
		{
			dest = szdest;
		}
  #ifdef _DEBUG
		else
		{
			erCnt++;// mainly for a breakpoint
		}
  #endif

		delete [] szdest;
		delete [] szsrc;
	}
	return dest;
}
#if _MSC_VER < 1400	/* VS8 (2005) */
double _wtof(const wchar_t* a )
{
	//cerr<<"ERROR: *** Wide char string to float has not been implemented!!!!"<<endl;
	//return 0.0;
	return ( wcstod(a, NULL) );
}
#endif

//#ifndef FMAPARSER
#if 1
/****** end global functions *************************************************/
char CitemType::itmTyName[MAX_ITYPE][16] = {
	{"Undefined"},		// 0	RESERVED_ITYPE1	
	{"Variable"},		// 1	VARIABLE_ITYPE 	
	{"Command"},		// 2	COMMAND_ITYPE   
	{"Menu"},			// 3	MENU_ITYPE      
	{"Edit-Display"},	// 4	EDIT_DISP_ITYPE 
	{"Method"},			// 5	METHOD_ITYPE    
	{"Refresh"},		// 6	REFRESH_ITYPE   
	{"Units"},			// 7	UNIT_ITYPE      
	{"Write-As-One"},	// 8	WAO_ITYPE 	    
	{"Reference-Array"},// 9	ITEM_ARRAY_ITYPE
	{"Collection"},		//10	COLLECTION_ITYPE
	{"Undefined11"},	//11	  RESERVED_ITYPE2	
	{"Block"},			//12	BLOCK_ITYPE     
	{"Program"},		//13	PROGRAM_ITYPE   
	{"Record"},			//14	RECORD_ITYPE    
	{"Value-Array"},	//15	ARRAY_ITYPE     
	{"Variable-List"},	//16	VAR_LIST_ITYPE  
	{"Response-Code"},	//17	RESP_CODES_ITYPE
	{"Domain"},			//18	DOMAIN_ITYPE
	{"Member"},			//19	MEMBER_ITYPE
	{"File"},			//FILE_ITYPE	
	{"Chart"},			//CHART_ITYPE
	{"Graph"},			//GRAPH_ITYPE
	{"Axis"},			//AXIS_ITYPE	
	{"Waveform"},		//WAVEFORM_ITYPE
	{"Source"},			//SOURCE_ITYPE
	{"List"},			//LIST_ITYPE	
	{"Grid"},			//GRID_ITYPE	
	{"Image"},			//IMAGE_ITYPE   
	{"Blob"}			//BLOB_ITYPE
	,{"Plugin"}			//PLUGIN_ITYPE
	,{"Template"}		//PLUGIN_ITYPE
	,{""}				//RESERVED_ITYPE3
	,{"Amorphous-Array"}//AMORPHOUS_ARRAY_ITYPE=63
};    

// tied to ddbdefs.h - referenceType_t    
char CrefType::refTyName[MAX_REFTYPE_ENUM+1][18] = {
	{"Item_ID"},		// 0___undocumented					// to ZERO
	{"ItemArray_ID"},	// 1								// to ITEM_ARRAY_ITYPE
	{"Collection_ID"},	// 2								// to COLLECTION_ITYPE
	{"via_ItemArray"},	// 3  exp-index, ref-array			// to  ITEM_ARRAY_ITYPE
	{"via_Collection"},	// 4  member-itemId, ref-collection // to  COLLECTION_ITYPE
	{"via_Record"},		// 5  member-itemId, ref-record 	// to  RECORD_ITYPE,
	{"via_Array"},		// 6  exp-index, ref-array			// to  ARRAY_ITYPE,
	{"via_VarList"},	// 7  member-itemId, ref-var list	// to  VAR_LIST_ITYPE,
	{"via_Param"},		// 8  int: parameter member id		// to  PARAM_ITYPE,----special  200
	{"via_ParamList"},  // 9  int: param-list-name			// to  PARAM_LIST_ITYPEspecial  201
	{"via_Block"},		//10  int: member name				// to  BLOCK_ITYPE,----special  202
	{"Block_ID"},		//11								// to BLOCK_ITYPE,
	{"Variable_ID"},	//12								// to VARIABLE_ITYPE,
	{"Menu_ID"},		//13								// to MENU_ITYPE,
	{"Edit-Display_ID"},//14								// to EDIT_DISP_ITYPE,
	{"Method_ID"},		//15								// to METHOD_ITYPE,
	{"Refresh_ID"},		//16								// to REFRESH_ITYPE,
	{"Unit-Relation_ID"},//17								// to UNIT_ITYPE,
	{"Write-As-One_ID"},//18								// to WAO_ITYPE,
	{"Record_ID"},		//19								// to RECORD_ITYPE,
	{"Array_ID"},		//20___undocumented					// to ARRAY_ITYPE,
	{"Var-List_ID"},	//21___undocumented					// to VAR_LIST_ITYPE,
	{"Program_ID"},		//22___undocumented					// to PROGRAM_ITYPE,
	{"Domain_ID"},		//23___undocumented					// to DOMAIN_ITYPE,
	{"ResponseCode_ID"},//24___undocumented					// to RESP_CODES_ITYPE
	{"File_ID"},
	{"Chart_ID"},
	{"Graph_ID"},
	{"Axis_ID"},
	{"Waveform_ID"},
	{"Source_ID"},	//30
	{"List_ID"},

	{"Image"},
	{"Separator"},
	{"Constant"},
	{"via_File"},
	{"via_List"},
	{"via_BitEnum"},
	{"Grid_ID"},	//rT_Grid_id,
	{"Rowbreak"},	//rT_Row_Break,
	{"via_Chart"},	//rT_via_Chart,		// 40
	{"via_Graph"},	//rT_via_Graph,
	{"via_Source"},	//rT_via_Source,
	{"via_Attribute"},	//rT_via_Attr,
	{"Blob_ID"},
	{"via_Blob"},
	{"Template_ID"},
	{"Plugin_ID"},
	{"Not_a_RefType"}
};
		
const
int CitemType::ref2idTbl[] = 
{	0, /*ITEM_ID_REF */	// 0->  0 reserved??
	ITEM_ARRAY_ITYPE,	// 1->  9
	COLLECTION_ITYPE,	// 2-> 10
	ITEM_ARRAY_ITYPE, /*   3->  9 via ItemArray*/
	COLLECTION_ITYPE, /*   4-> 10 via Collection*/
	RECORD_ITYPE,		// 5-> 14 via_Record
	ARRAY_ITYPE,		// 6-> 15 via_Array
	VAR_LIST_ITYPE,		// 7-> 16 via_VarList
	PARAM_ITYPE,		// 8->200 via_Param
	PARAM_LIST_ITYPE,	// 9->201 via_ParamList
	BLOCK_ITYPE,		//10-> 12 via_Block
	BLOCK_ITYPE,		//11-> 12
	VARIABLE_ITYPE,		//12->  1
	MENU_ITYPE,			//13->  3
	EDIT_DISP_ITYPE,	//14->  4
	METHOD_ITYPE,		//15->  5
	REFRESH_ITYPE,		//16->
	UNIT_ITYPE,			//17->
	WAO_ITYPE,			//18->
	RECORD_ITYPE,		//19->
	ARRAY_ITYPE,		//20->
	VAR_LIST_ITYPE,		//21->
	PROGRAM_ITYPE,		//22->
	DOMAIN_ITYPE,		//23->
	RESP_CODES_ITYPE,	//24-> 17
	/* new reference types */
	FILE_ITYPE,
	CHART_ITYPE,
	GRAPH_ITYPE,
	AXIS_ITYPE,
	WAVEFORM_ITYPE,
	SOURCE_ITYPE,		//30
	LIST_ITYPE,

	IMAGE_ITYPE,
	MAX_ITYPE,			// Constant Separator,		/* == colbreak */
	MAX_ITYPE,			// Constant
	FILE_ITYPE,		// 35
	LIST_ITYPE,			//	  via_List,
	VARIABLE_ITYPE,		//	  via_BitEnum,
	GRID_ITYPE,
	MAX_ITYPE,			//Constant  Row_Break,
	CHART_ITYPE,		// 40 via_Chart
	GRAPH_ITYPE,		//	  via_Graph
	SOURCE_ITYPE,		//	  via_Source,
	MAX_ITYPE,			//	  via_Attr,
	BLOB_ITYPE,			//		Blob_id
	BLOB_ITYPE,			//    via_Blob
	TEMPLATE_ITYPE,		// new 06mar17
	PLUGIN_ITYPE,		// added 06mar17
	MAX_REFTYPE_ENUM			//48->ERROR
};

/* global */
const  /* referenceTpe = id2refTbl[itemType]; */
referenceType_t id2refTbl[] = 
{
	rT_Item_id,				//	  RESERVED_ITYPE1			0
	rT_Variable_id,			//  VARIABLE_ITYPE 	       	1
		rT_NotA_RefType,	//  COMMAND_ITYPE           2
	rT_Menu_id,				//  MENU_ITYPE              3
	rT_Edit_Display_id,		//  EDIT_DISP_ITYPE         4
	rT_Method_id,			//  METHOD_ITYPE            5
	rT_Refresh_id,			//  REFRESH_ITYPE           6
	rT_Unit_id,				//  UNIT_ITYPE              7
	rT_WAO_id,				//  WAO_ITYPE 	       		8
	rT_ItemArray_id,		//  ITEM_ARRAY_ITYPE        9
	rT_Collect_id,			//  COLLECTION_ITYPE        10
		rT_NotA_RefType,	//	  RESERVED_ITYPE2			11
	rT_Block_id,			//  BLOCK_ITYPE             12
	rT_Program_id,			//   PROGRAM_ITYPE           13
	rT_Record_id,			//  RECORD_ITYPE            14
	rT_Array_id,			//  ARRAY_ITYPE             15
	rT_VarList_id,			//   VAR_LIST_ITYPE          16
	rT_ResponseCode_id,		//   RESP_CODES_ITYPE        17
	rT_Domain_id,			//   DOMAIN_ITYPE            18
		rT_NotA_RefType,	//   MEMBER_ITYPE            19
	rT_File_id,				//  FILE_ITYPE				20
	rT_Chart_id,			//  CHART_ITYPE				21
	rT_Graph_id,			//  GRAPH_ITYPE				22
	rT_Axis_id,				//  AXIS_ITYPE				23
	rT_Waveform_id,			//  WAVEFORM_ITYPE			24
	rT_Source_id,			//  SOURCE_ITYPE			25
	rT_List_id,				//  LIST_ITYPE				26
	rT_Grid_id,				//  GRID_ITYPE				27
	rT_Image_id,			//  IMAGE_ITYPE             28
	rT_Blob_id,				//  BLOB_ITYPE              29	
	rT_Plugin_id,			// PLUGIN_ITYPE				30
	rT_Template_id,			// TEMPLATE_ITYPE			31
	// RESERVED_ITYPE3			32
	// COMPONENT_ITYPE			33	// not in HART
	// COMP_FOLDER_ITYPE		34	// not in HART
	//	COMP_DESCRIPTOR_ITYPE	35 // not in HART "component_reference" in spec
	// COMP_RELATION_ITYPE		36	// not in HART
	// RESERVED_ITYPE4         37
		rT_NotA_RefType		// MAX_ITYPE					/* must be last i
};
#endif // FMAPARSER		

string ws2as(const wstring& src)
{
	string dest;
    dest.resize(src.size());
    for (unsigned int i=0; i<src.size(); i++)
        dest[i] = src[i] < 256 ? src[i] : '?';
	return dest;
}

//wstring as2ws(const string& str )
//{
//	wstring dest;
//    dest.resize(str.size());
//    for (unsigned int i=0; i<str.size(); i++)
//        dest[i] = (short)str[i];
//	return dest;
//}

#ifdef _UNICODE
 #ifndef UNICODE
  #define UNICODE
 #endif
#endif
#undef TCHAR
//stevev 23feb10 - vs8 turn off windows
//#include <windows.h>  /* TEMPORARY - to use Bills Code */
#include <string>

using namespace std;

// convert UTF-8 multi-byte string to Unicode little-endian wstring
// use bill's library function
wstring c2w(char *c)
{
	return ( AStr2TStr( string( c ) ) );
	/* we don't use windows
	const int size = MultiByteToWideChar(CP_ACP, 0, c, -1, NULL, 0);
	wchar_t *w = (wchar_t *) malloc(sizeof(wchar_t)*size);
	MultiByteToWideChar(CP_UTF8, 0, c, -1, w, size);
	return wstring(w);
	*/
}


// convert wstring to UTF-8 for printing
// use bill's library function
char *w2c(wstring &w)
{
	string ret( TStr2AStr(w) );
	char* c = (char*)malloc( ret.size()+1 );
#if defined(__GNUC__)
    strncpy(c,ret.c_str(),ret.size()+1);
#else // !__GNUC__
	strncpy_s(c,ret.size()+1,ret.c_str(),ret.size()+1);
#endif // __GNUC__
	return c;
	/* we don't use windows
	const int size = WideCharToMultiByte(CP_UTF8, 0, w.c_str(), -1, NULL, 0, NULL, NULL);
	char *c = (char *) malloc(sizeof(char)*size);

	if (c)
		WideCharToMultiByte(CP_UTF8, 0, w.c_str(), -1, c, size, NULL, NULL);

	return c;
	*/
}


// latin-1 string translated to UFT-8 narrow encoding and placed into buffer.
// pointer to buffer is returned
// characters > bufsiz are truncated
char *latin2utf8(char *s, char *buf, int bufsize)
{
	unsigned char* p;
	int i, c;

	p = (unsigned char*)s;
	i = 0;
	for (c = *(p++); c; c = *(p++))
	{
		if (i == bufsize - 1)
			break;

		if ( c >= 128 )
		{
			buf[i++] = (char)(((c & 0x7c0)>>6) | 0xc0);
			buf[i++] = (char)((c & 0x3f) | 0x80);
		}
		else
			buf[i++] = c;

	}

	buf[i] = '\0';

	return buf;
} 

// calculate storage in bytes required to hold latin1 string
// converted to utf8 WITHOUT trailing null
int latin2utf8size(char *s)
{
	int n = 0;
	while (unsigned int c = *(s++))
	{
		if ( c >= 128 )
			n += 2;

		else
			n++;
	}

	return n;
} 


// convert narrow utf8 string to wstring
// this is a functional equivalent to c2w() above

wstring UTF82Unicode(const char* src)
{
	wstring dest;
	/* stevev 19mar09 - this is just wrong!!!  
	int sz = strlen(src);
	dest.resize(sz);//src.size());
	unsigned int utf_idx=0;
	unsigned int ws_idx=0;
	int c, c1, c2, c3;

	while ( utf_idx < (unsigned int)sz )
	{
		c = static_cast<unsigned char>(src[utf_idx++]);

		if ((c & 0x80) == 0)
			 dest[ws_idx++] = c & 0x7F;

		else if ((c & 0xE0) == 0xC0){
			 c1 = c;
			 c2 = src[utf_idx++];
			 dest[ws_idx++] = ((c1 & 0x1F) << 6) | (c2 & 0x3F);
		}
		else if ((c & 0xF0) == 0xE0) {
			 c1 = c;
			 c2 = src[utf_idx++];
			 c3 = src[utf_idx++];
			 dest[ws_idx++] = ((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6) | (c3 & 0x3F);
		}
	}
	dest.resize(ws_idx);
	****************************/
	string sc(src);
	dest = AStr2TStr(sc);
	return dest;
}

  
// break text up into no more than 80 chars on a line
string addlinebreaks(char *s)
{
	string p(s);
	int n = p.length();
	if (n < 80)
		return p;

	string q("\n");
	int pos = 0;
	while (pos < n)
	{
		q = q + p.substr(pos, 80) + "\n";
		pos += 80;
	}

	return q;
}

double uint64TOdbl(UINT64 uint64value)
{
	double retValue  = 0.0;
	__int64 posValue = uint64value & 0x7fffffffffffffffULL; // L&T Modifications : PortToAM335

	bool hiSet  = ( (uint64value & 0x8000000000000000ULL) != 0 ); // L&T Modifications : PortToAM335

	// stevev 27aug10 - we don't want to change types with a cast
	// we just want to set the double to the value of the integer.
	// possible accuracy reduction is possible but that's why we use
	// this subroutine...we'll have to do the best we can.
	#pragma warning( push )
	#pragma warning( disable : 4244 )
	retValue    = posValue;// max = 18,446,744,073,709,551,615
	#pragma warning( pop ) 

	if (hiSet)
	{
		retValue += 0x7fffffffffffffffULL; // L&T Modifications : PortToAM335
		retValue += 1;
	}

	return retValue;
}


bool isUsableItemType(itemType_t it)
{
	if (  it == iT_Program       || it == iT_VariableList ||
	      it == iT_ResponseCodes || it == iT_Domain       || ( it > iT_Template && it <= iT_ReservedThre )
#ifndef TOKENIZER
	||   it == iT_ReservedZeta  /* allow it in the tokenizer, not elsewhere */
	||   it == iT_Record 		/* required for munging..... (ie DD 0.63 ) */
	||   it == iT_MaxType		/* it's used in hydrate  for a non value*/
#endif		  		  
		   )// not hart
		return false;

	if ( it == iT_NotAnItemType   /* allow this one */
#ifdef TOKENIZER
	  || it == iT_AmorphousArray  /* allow this in the tokenizer */
#endif
		|| (it >= iT_ReservedZeta && it < iT_ReservedTwo)// all the normals not listed above
		)
		return true;
// all the psuedo numbers, incl iT_MaxType & iT_AmorphousArray
	return false;
};
std::string trim(const std::string& str,  const std::string& whitespace)
{
	const unsigned strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content
    const unsigned strEnd = str.find_last_not_of(whitespace);
    const unsigned strRange = strEnd - strBegin + 1;
    return str.substr(strBegin, strRange);
}
/**********************************************************************************************
 *
 *   $History: foundation.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/Common/RdWrCommon
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 3/31/03    Time: 12:31p
 * Updated in $/DD Tools/DDB/Common/RdWrCommon
 * 
 **********************************************************************************************
 */
