/*************************************************************************************************
 *
 * $Workfile: ddbItemLists.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the All the item lists 
 *	
 *		7/25/2	sjv	creation
*/

#include "ddbVar.h"

#include "ddbItemLists.h"
#include "ddb.h"             // the database
#include "ddbDevice.h"
#include "ddbTracking.h"

#ifdef INI_OK
bool openIni();
bool closeIni();
char *getSymbolValue(unsigned long id);
char *getSymbolValue(char *id);
#endif

#ifdef _DEBUG
#define BestReadTime
#endif




int CVarList::bumpAllVars(void)
{
	int c = 0;
	vector<hCVar*>::iterator iT;

//Deepak : 112504
	bool bAquiredMutex=false;
	data_avail tds;
	bool bIsValid=false;

	hCVar* pV = NULL; // got an assert 'vector iterator not dereferencable' 23Jan12

	try{
	for (iT = begin(); iT < end(); iT++)
	{// ptr2aPtr2a hCVar
		pV = (hCVar*) (*iT);// 23Jan12
		if ( pV == NULL )
		{
			DEBUGLOG(CERR_LOG,
				"ERROR: BumpAllVars found a NULL pointer in the VAR list!(debug only)\n");
			continue;
		}
		// add a filter (or 2 or 3...)
		tds=pV->getDataQuality() ;

		try{
		bIsValid = pV->IsValid() ;
		}
		catch(...)
		{
			bIsValid=true;
		}

		if ((! pV->IsWriteOnly()) && // handling test
			(! pV->IsLocal()    ) && // locality test
			(bIsValid/*(*iT)->IsValidTest()*/      ) && // validity test /*DEEPAK 101804 changed to isvalid to isvalidtest()*/
			(pV->HasReadCmd()   ) && // readablity test
			//((*iT)->getDataQuality() == DA_NOT_VALID || (*iT)->getDataQuality() == DA_STALEUNK)
			(tds!= DA_HAVE_DATA)/* sjv04dec06-must bump all stales or they may never get read */
		   )
		{
			devPtr()->aquireItemMutex(
#ifdef _DEBUG
							__FILE__,__LINE__);
#else
							  );
#endif
			bAquiredMutex=true;
			pV->bumpQueryCnt();
			if ( tds != DA_STALE_OK ) pV->bumpQueryCnt();/*sjv04dec06-doublebump all but OK*/
			devPtr()->returnItemMutex();
			bAquiredMutex=false;
		}
		c++;
	}// next
	}//try
	catch(...)
	{
		if(bAquiredMutex)
			devPtr()->returnItemMutex();
		LOGIT(CERR_LOG,"Inside bumpAllVars catch(...)\n");
	}
//END changes 112504

	return c;
}

/* stevev - 02/17/04 - added */
void CVarList::markAllItemState(INSTANCE_DATA_STATE_T newSt )
{
	int c = 0;
	vector<hCVar*>::iterator iT;

	for (iT = begin(); iT < end(); iT++)
	{// ptr2aPtr2a hCVar
		// add a filter (or 2 or 3...)
		if (! (*iT)->IsLocal() ) 
		{
			(*iT)->markItemState(newSt);
			c++;
		}
	}

	return;
}
/* stevev - 02/17/04 - end add */

bool  CVarList::isChanged(void)
{
	vector<hCVar*>::iterator iT;

	for (iT = begin(); iT != end(); iT++)
	{// ptr2aPtr2a hCVar
		hCVar* pVr = (*iT);
		if ( pVr->isChanged() )
		{
			return true; // abort function and exit with info
		}
	}
	return false;
}

/* varible instantiation */
//RETURNCODE CVarList::populate(DevInfcHandle_t h, UINT32 theDDkey, aCdevice* pAbstractDevice)
RETURNCODE CVarList
::populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl, DD_Key_t theDDkey, 
														aCdevice* pAbstractDevice,bool isServer)
{			
	RETURNCODE rc = SUCCESS;
	hCVar* pVar;
	itemID_t		tmpID=0;
	CValueVarient	tmpValue;
/*no longer needed
	UINT32          ddKey;
	UINT8		mfg;
	UINT8		devt;
	UINT8		trev;
	UINT8		ddrv;
*/
	aCitemBase* paItem;
			//aCvarTypeDescriptor acVarDesc;
	variableType_t vt;

	if (pAbstractDevice != NULL)
	{
		for (AitemList_t::iterator iT = pAbstractDevice->AitemPtrList.begin(); 
		                           iT < pAbstractDevice->AitemPtrList.end  ();    iT++)
		{// iT is ptr 2 a ptr to a aCitemBase
			paItem = (*iT);
			if (paItem == NULL)
				continue;
			if (paItem->itemType.getType() == VARIABLE_ITYPE)  // filter just this type
			{// build it - constructor builds from the prototype and it's sub parts do too
				aCattrBase* pB = paItem->getaAttr(varAttrTypeSize);
				if ( pB != NULL)
				{
					vt = (variableType_t)(((aCattrTypeType*)pB)->notCondTypeType.actualVarType);
					
					if ( vt < vT_Integer || vt >= vT_MaxType )
					{
						LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: unknown type in type descriptor (CVarList:populate)"
																				" %d\n", ((int)vt) );
						rc = APP_TYPE_UNKNOWN;
					}
					else // it appears valid
					{
						pVar = hCVar::newVar(devHndl(),paItem); 

						if (pVar)
						{
#if 0
							// string y;
							// clear the var values
//Set Default some other way							pVar->setDispValueString((y="")) ;// sets the write value
							ddKey = pDev->getDDkey();
							
							ddrv =  ddKey     & 0xFF;
							trev = (ddKey>> 8)& 0xFF;
							devt = (ddKey>>16)& 0xFF;				
							mfg  = (ddKey>>24)& 0xFF ;
							tmpID = pVar->getID();
							{// special initial values
								switch(tmpID)
								{
								case DEVICE_MFG_ID:			// enumerated      153 (std,imp) 
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = mfg; 
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_DEVICE_TYPE:	// enumerated      154 (std,mand,imp) 
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = devt;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_XMTR_REVISION:	// unsigned        157 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = trev;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_SFTWR_REVISION: // unsigned        158 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = ddrv;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_REQST_PREAMBLES:// unsigned        155 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = 5;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_HDWR_REVISION:  // unsigned        159 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = 2;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_POLL_ADDRESS:	// unsigned        162 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = 0;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								case DEVICE_HART_REV_LEVEL:	// unsigned        156 (std,mand,imp)  
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = 5;
									else
										tmpValue = -1;    //VMKP
									pVar->setDispValue(tmpValue);
									break;
								default:   
									/* sjv - init */
									if (isServer) // device simulator
										tmpValue = 0;
									else
										tmpValue = -1;    //VMKP
									break;/* do nothing */
								} 
							}
							pVar->ApplyIt(); 
#endif
							/*self*/push_back(pVar);
							pDev->registerItem(pVar->getID(), pVar, pVar->getSymbolName());
//clog<<"Setup a " << pVar->getTypeName() << " with id# " << hex << pVar->getID() << dec << endl;
						}
						else
						{
							LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: New variable failed to instantiate."
																	"(%d)\n", (long)paItem->itemType );
							rc = APP_CONSTRUCT_ERR;
						}
					}// endelse - valid descriptor						
				}
				else
				{// no type descriptor
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,
						"ERROR: Variable item (0x%04x) with no Type/Size Descriptor.\n",
						paItem->itemId);
				// leave it skipped for now....	rc = APP_DD_ERROR;
				}
			}// else skip the wrong types
			else
			{
//WAY too many of these				LOGIT(CLOG_LOG,"ERROR: a non-Variable (%s) found in the variable list.\n",
//    debug later       					(*iT)->itemType.getTypeStr());
			}
		}// next item
LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"> device.varlist has %d vars. of %d items.\n",
		size(),pAbstractDevice->AitemPtrList.size());
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: populate list was not handed a prototype abstract class.VARIABLE\n");
		rc = APP_PARAMETER_ERR;
	}
	return rc;
}

/* stevev - added 4/22/04 
	THIS MUST BE DONE AFTER ALL OF THE OTHER LISTS ARE CONSTRUCTED AND BEFORE ANY CMDS ARE SENT*/
int cnty = 0;
void CVarList::setDefaultValues(void)
{
	hCVar* pVar = NULL;
	/* moved from the Device's collectXrefInfo() 4/22/04 */
	/* this will probably be converted later to just a call to the variable's SetDefaultValue*/
	/* stevev 12/15/03  set valid default values to indexes and the like */
	for (CVarList::iterator iT = begin(); iT < end(); iT++)
	{// isa ptr 2a ptr 2a hCVar
		pVar = (hCVar*)(*iT);
		pVar->setDefaultValue();
cnty++;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * converted to a variable-based setDefaultValue() to isolate the uniqueness
 *
 old, commented out code that was here has been removed 07jan09 by stevev - see earlier for code
  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		if (pVar->IsLocal())
		{
			pVar->markItemState(IDS_CACHED);	//dataState   = IDS_CACHED;
			pVar->setDataQuality(DA_HAVE_DATA);//dataQuality = DA_HAVE_DATA;
		}
	}// next var
}
/* end add - 4/22/04 */

/* stevev 8jan09 - Lists have to know who their indexes are */
void CVarList::registerListIndexes(void)
{	
	hCVar* pVr = NULL;

	for (CVarList::iterator iT = begin(); iT < end(); iT++)
	{// isa ptr 2a ptr 2a hCVar
		pVr = (hCVar*) (*iT);
		if (pVr->VariableType() == vT_Index)
		{
			hCindex* pIdx = (hCindex*)pVr;
			hCitemBase* pDexed = NULL;
			if ( pIdx->pIndexed != NULL && 
				 pIdx->pIndexed->getArrayPointer(pDexed) == SUCCESS && 
				 pDexed != NULL)
			{
				if (pDexed->getIType() == iT_List)
				{
					hClist* pLst = (hClist*)pDexed;
					pLst->addIndex(pIdx);
				}// else skip array indexes
			}// else error an index variable that indexes nothing
		}// else skip, we're looking for list indexes
	}// next var
	return;
}
/* end new code 8jan09 */

// 15nov11-varOptimization
// we get just the command/transactions that support commands that need read or write
RETURNCODE CVarList::getNeededList(cmdDescList_t& cmdDescLst, bool isWrt)
{
	cmdDescLst.clear();
		
	hCVar* pVr = NULL;

	cmdDescList_t varCmdList;
	
	cmdDescList_t varList;

	for (CVarList::iterator iT = begin(); iT < end(); iT++)
	{// isa ptr 2a ptr 2a hCVar
		varList.clear();
		pVr = (hCVar*) (*iT);
		if ( pVr == NULL || (! pVr->IsValid()) )
		{
			continue;// skip this entry
		}
		itemID_t thisItmID = pVr->getID();// also good for debugging
		if (thisItmID == RESPONSECODE_SYMID || thisItmID == DEVICESTATUS_SYMID )
		{
			continue;// we don't weigh these
		}

		if (pVr->getDataStatus() == 0)
		{// do not weigh this item -  it has been marked as a problem variable
			continue;
		}

		INSTANCE_DATA_STATE_T     tDS = pVr->getDataState();
		if ( tDS == IDS_NEEDS_WRITE &&  isWrt )
		{// deal with write command list
			varList = pVr->getWrCmdList();
		}
		else
        /* Excluded IDS_UNINITIALIZED condition as it prevents reading all the parameters initially..*/
        if ( (! isWrt) && (tDS == IDS_STALE || /*tDS == IDS_UNINITIALIZED || */ tDS == IDS_INVALID))
		{// deal with read command list
			varList = pVr->getRdCmdList();
		}
		//else // (tDS == IDS_CACHED || tDS == IDS_PENDING ) leave the list clear

		// this is append unique for cmdDescList_t
		// for each var command
		cmdDescListIT varIT, outIT;
		hCcommandDescriptor *pVardesc, *pOutdesc;
		bool foundMatch = false;
		for (varIT = varList.begin(); varIT != varList.end(); ++ varIT )
		{// for each read or write command that could service this var...
			pVardesc = &(*varIT);
			for (outIT = cmdDescLst.begin(); outIT != cmdDescLst.end(); ++ outIT  )
			{//check if its already there
				pOutdesc = &(*outIT);
				if ( pVardesc->cmdNumber == pOutdesc->cmdNumber     &&
					 pVardesc->transNumb == pOutdesc->transNumb    )
				{// we aren't going to compare indexes in this first cut
					foundMatch = true;// already in the list, we can skip adding it
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"-<%d.%d>", pVardesc->cmdNumber, pVardesc->transNumb);
					break; // out of inner loop
				}
				// else keep looking
			}// next unique output value
			if ( ! foundMatch )// we need to add it since it isn't there
			{
				cmdDescLst.push_back(*pVardesc);
			}
			// else skip it and go on to the next var list entry
		}// next var command

	}// next var
	return SUCCESS;
}

#ifdef _DEBUG
void CVarList::varNotificationReport()
{
	int total      = 0;
	int isNotifd   = 0;
	int notNotfied = 0;
	
	string       classStr;

	vector<hCVar*>::iterator ihCVar;
	LOGIT(COUT_LOG,"\n Variable notification report.\n");
	for ( ihCVar = begin();ihCVar != end(); ++ihCVar )
	{// ptr 2a ptr 2a hCVar
		total++;
		if ((*ihCVar)->isNotified )
		{
			isNotifd++;
		}
		else
		{
			notNotfied++;
			classStr = TStr2AStr(  (*ihCVar)->getClassStr()  );

			LOGIT(COUT_LOG,"Variable %24s (0x%08x) has not been notified: is of class %s.\n",
				(*ihCVar)->getName().substr(0,min(24,((*ihCVar)->getName().length()))).c_str(), 
				(*ihCVar)->getID(), classStr.c_str());
		}
	}
	LOGIT(COUT_LOG,"Of the %d variables, %d were notified and %d were NOT notified.\n",
		total,isNotifd,notNotfied);
}

void CVarList::varDependencyReport()
{
	hCdependency* pComDep = NULL;
	ddbItemLst_it ipItemID;
	hCitemBase*   pIB = NULL;
	string        classStr;
	int  lp;

	vector<hCVar*>::iterator ihCVar;
	LOGIT(COUT_LOG,"\n\n***** Variable Dependency report ******\n");
	for (lp = ib_ValidityDep; lp < ib_Common; lp++)
	{
		if (lp == ib_ValidityDep)
		{
			LOGIT(COUT_LOG,"\n\n*****          Validity          ******\n");
		}
		else
		{
			LOGIT(COUT_LOG,"\n\n*****         Attributes         ******\n");
		}
	for ( ihCVar = begin();ihCVar != end(); ++ihCVar )//for each Var
	{// ptr 2a ptr 2a hCVar
		pComDep = (*ihCVar)->getDepPtr((ibWhichDependency_t)lp);//var or Cond Dep
		if (pComDep == NULL ) 
		{
			LOGIT(COUT_LOG,"0x%08x Does not have a dependency.\n",(*ihCVar)->getID());
			continue;// skip the rest
		}
		hCVar* pV = (*ihCVar);

		ddbItemList_t* pdepOnMe = &(pComDep->dependsOnMe);
		
		//classStr = pV->getClassStr();
		classStr = TStr2AStr(   pV->getClassStr()   );
		
		int  k = pdepOnMe->size();
		if ( k > 0 ) // iscritical
		{//					number name  class      is dominant to %d items
//unsigned long tL = pV->getID();
//char *        tC = pV->getName();
//char *        tCC=classStr.c_str();
//			LOGIT(COUT_LOG,"0x%08x '%s' [class: %s] is dominant to %d items.\n",tL,tC,tCC,k);

			LOGIT(COUT_LOG,"0x%08x '%24.24s' [class: %s] is dominant to %d items.\n"
					,pV->getID(),pV->getName().c_str(),classStr.c_str(),k);

			for (ipItemID = pdepOnMe->begin(); ipItemID != pdepOnMe->end(); ++ ipItemID)
			{//ptr 2a ptr 2a hCitemBase
				pIB = (*ipItemID);

				itemType_t type = pIB->getIType();
				string  typeStr = pIB->getTypeName();
				string  prtName = pIB->getName().substr(0,min(24,(pIB->getName().length())));
				if ( type == iT_Variable)
				{
					//classStr = ((hCVar*)pIB)->getClassStr();
					classStr = TStr2AStr(   ((hCVar*)pIB)->getClassStr()   );
					LOGIT(COUT_LOG,"        0x%08x '%24.24s' %s [class: %s] is dependent.\n"
					,pIB->getID()   ,prtName.c_str()
					,typeStr.c_str(),classStr.c_str());
				}
				else
				{
					LOGIT(COUT_LOG,"        0x%08x '%24.24s' %s is dependent.\n"
							,pIB->getID()	,prtName.c_str()
							,typeStr.c_str() );
				}
			}				 
		}
		else // not critical
		{//					number name class is not dominant.
			LOGIT(COUT_LOG,"0x%08x '%24.24s' [class: %s] is NOT dominant.\n"
					,pV->getID()
					,pV->getName().c_str()
					,classStr.c_str()  );
		}		
	}
	}
}
#endif

/**************************************************************************
 * end varlist
 * start commandlist
 *************************************************************************/

void CCmdList::insertWgtVisitor(hCcmdDispatcher* pDispatch)// all commands
{
	for ( vector<hCcommand*>::iterator iT = begin(); iT < end(); iT++)
	{//iT is a ptr 2 a ptr 2 a hCcommand
		(*iT)->insertWgtVisitor(pDispatch);
	}
}


hCcommand* CCmdList::getCmdByNumber(int cmdNumber)// all commands
{
	hCcommand* pCRet = NULL;
	for ( vector<hCcommand*>::iterator iT = begin(); iT < end(); iT++)
	{//iT is a ptr 2 a ptr 2 a hCcommand
		if ( (*iT)->getCmdNumber() == cmdNumber )
		{
			pCRet = (*iT);
			break; // out of for loop
		}
	}
	return pCRet;
}
//  0 == look at only transaction 0
// -1 == scan all transactions, returning heaviest
#define TRTYPE -1 // scan 'em all 0

RETURNCODE CCmdList::getBestWrite// pList is not null:: weigh using var optimization
			(hCcommand*& pCmd, int& transNum, indexUseList_t& useIdx, cmdDescList_t* pList)
{
	RETURNCODE rc = FAILURE; // indicates nothing to write
	int w, tN=TRTYPE, curWgt = 0;
	// filter the read commands
	cmdOperationType_t cot; 
	indexUseList_t    localUseIdx;

	if (pList)
	{
		for ( vector<hCcommand*>::iterator iT = begin(); iT < end(); iT++)
		{//iT is a ptr 2 a ptr 2 a hCcommand
#ifdef _DEBUG
			int yy = (*iT)->getCmdNumber();
#endif
			cot = (*iT)->getOperation();
			if ( cot == cmdOpWrite )// cmdcmd never to be sent outside method || cot == cmdOpCmdCmd )
			{
				w = (*iT)->getWrWgt(tN, localUseIdx);
				if ( w > curWgt )
				{	
					curWgt   = w;
					pCmd     = *iT;
					transNum = tN;
					useIdx   = localUseIdx;
					localUseIdx.clear();
				}
				// else continue
				tN = TRTYPE;
			}// else skip
		}// next
	}// nothing needs writing
	if ( curWgt > 0 )
	{
		rc = SUCCESS;

		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Best Wr Cmd %d Trans %d  with weight %d\n", pCmd->getCmdNumber(),transNum,curWgt);
	}
	else
	{
		pCmd     = NULL;
		transNum = -1;
		rc       = FAILURE; // nothing to do
		localUseIdx.clear();
	}
	return rc;
}

#ifdef BestReadTime  // top of this file
struct _timeb       BestReadStartBuffer,BestReadEndBuffer, lastTimeBuffer;
double tmpET;

#define GETELAPSED(ltb, dbl)  {struct _timeb tt;double s,e;_ftime( &(tt) );\
	s = ltb.time + ((double)ltb.millitm/1000); e = tt.time + ((double)tt.millitm/1000);\
	dbl = e - s; ltb = tt; }
#define GET_SPLIT(ltb, dbl)  {struct _timeb tt;double s,e;_ftime( &(tt) );\
	s = ltb.time + ((double)ltb.millitm/1000); e = tt.time + ((double)tt.millitm/1000);\
	dbl = e - s;  }
#endif

RETURNCODE CCmdList::getBestRead // pList is not null:: weigh using var optimization
			(hCcommand*& pCmd, int& transNum, indexUseList_t& useIdx, cmdDescList_t* pList)
{
	RETURNCODE rc = FAILURE; // indicates nothing to read
	int w, tN = TRTYPE, curWgt = 0;
	// filter the read commands
	cmdOperationType_t cot; 
	indexUseList_t localUseIdx;
	int stillNotValidCnt;
	useIdx.clear();
	transNum = -1;
	// second highest weight
	int        sndWgt = 0;
	hCcommand* p2Cmd, *pCmdit;
	int        trans2Num;
	indexUseList_t use2Idx;
	//
#ifdef _DEBUG
	int cmd, maxCmd = 0;
#endif
#ifdef BestReadTime  // top of this file
	_ftime( &(BestReadStartBuffer) );
	lastTimeBuffer = BestReadStartBuffer;
#endif

	if ( pList == NULL || pList->size() <= 0 )// signal to do it the original technique
	{

		for ( vector<hCcommand*>::iterator iT = begin(); iT < end(); iT++)
		{//iT is a ptr 2 a ptr 2 a hCcommand
			pCmdit = (*iT);// 15nov11, stop using iterator as a pointer
			tN = TRTYPE;// send in - search all transactions
#ifdef _DEBUG
			cmd = pCmdit->getCmdNumber();
#endif
			cot = pCmdit->getOperation();
			if ( cot == cmdOpRead )
			{
				w = pCmdit->getRdWgt(tN,localUseIdx);
				if ( w > curWgt )
				{	
					curWgt   = w;
					pCmd     = pCmdit;
					transNum = tN;
#ifdef _DEBUG
					maxCmd = cmd;
#endif
#ifdef _DEBUG	///////////////////////////////
					if (localUseIdx.size() > 0)
					{
						int pz = 0;
						for (indexUseList_t::iterator itTEST = localUseIdx.begin();
							 itTEST < localUseIdx.end();    itTEST++,pz++)
						{// itIUL isa ptr2a hIndexUse_t
							if ( 0 == itTEST->indexSymID)
							{
								LOGIT(CERR_LOG,
								"-----: Zero Symbol ID in index %d from getRdWgt()\n",pz);
							}
						}
					}
#endif			////////////////////////////////
					useIdx   = localUseIdx;
				}
				else
				if ( w > sndWgt)
				{
					sndWgt   = w;
					p2Cmd    = pCmdit;
					trans2Num= tN;
					use2Idx   = localUseIdx;
				}
				localUseIdx.clear();
			}// else skip it
		}// next command

	}// the original way
	else // we have a list pointer
	{

		hCcommandDescriptor *pDesc;
		for ( cmdDescListIT dliT = pList->begin(); dliT != pList->end(); ++dliT )
		{//iT is a ptr 2 a hCcommandDescriptor
			pDesc = &(*dliT);
			pCmdit = getCmdByNumber(pDesc->cmdNumber);
	#ifdef _DEBUG
			cmd = pCmdit->getCmdNumber();
	#endif
			cot = pCmdit->getOperation();
			if ( cot == cmdOpRead )
			{
				tN = pDesc->transNumb;// send in specific transaction
				w = pCmdit->getRdWgt(tN,localUseIdx);
				if ( w > curWgt )
				{	
					curWgt   = w;
					pCmd     = pCmdit;
					transNum = tN;
	#ifdef _DEBUG
					maxCmd = cmd;
	#endif
	#ifdef XXX_DEBUG	///////////////////////////////
					if (localUseIdx.size() > 0)
					{
						int pz = 0;
						for (indexUseList_t::iterator itTEST = localUseIdx.begin();
							 itTEST < localUseIdx.end();    itTEST++,pz++)
						{// itIUL isa ptr2a hIndexUse_t
							if ( 0 == itTEST->indexSymID)
							{
								LOGIT(CERR_LOG,
									"-----: Zero Symbol ID in index %d from getRdWgt()\n",pz);
							}
						}
					}
	#endif			////////////////////////////////
					useIdx   = localUseIdx;
				}
				else
				if ( w > sndWgt)
				{
					sndWgt   = w;
					p2Cmd    = pCmdit;
					trans2Num= tN;
					use2Idx   = localUseIdx;
				}
				localUseIdx.clear();
			}// else skip it
		}// next command
#ifdef BestReadTime  // top of this file
	GET_SPLIT( BestReadStartBuffer, tmpET );
	LOGIT(CLOG_LOG,"T> %d transactions scanned in %f sec.\n",pList->size(),tmpET);
#endif

	}

#ifdef _DEBUG
	int z = useIdx.size();
#endif
	bool duplicate = false;
	stillNotValidCnt = ((CVarList*)(devPtr()->getListPtr(iT_Variable)))->bumpAllVars();
	// we will probably need to do something when stillNotValidCnt when it
	//                                a)stops changing or b) goes to zero

	if (curWgt > 0)
	{
		if (lastBestCmdNum == pCmd->getCmdNumber() && lastBestTransN == transNum)
		{
			if (useIdx.size())// we have indexes
			{
				bool found = false;
				hIndexUse_t *pThis, *pLast;
				for (idxUseIT IT = useIdx.begin(); IT != useIdx.end(); ++IT )
				{
					pThis = &(*IT);
					for (idxUseIT IU = lastIndexSet.begin(); IU != lastIndexSet.end(); ++IU )
					{
						pLast = &(*IU);
						if (pThis->indexSymID     == pLast->indexSymID      &&
							pThis->indexDispValue == pLast->indexDispValue  &&
							pThis->devIdxSymID    == pLast->devIdxSymID	    &&
							pThis->devVarRequired == pLast->devVarRequired	  )
						{
							found = true;// we have a duplicate index
							break;// out of inner loop
						}
					}
					if ( ! found )
					{
						duplicate = false;// if one doesn't match then the whole thing doesn't
						break; // out of outer loop
					}
				}
				if (found)// they all match
				{
					duplicate = true;
				}// else leave duplicate false
			}
			else
			{
				duplicate = true;
			}
		}
			
		if(duplicate)
		{
			// matches last cycle, reduce windup-skip it
			if (sndWgt>0)// we have a second-highest weight
			{
				curWgt = sndWgt;
				pCmd = p2Cmd;
				transNum = trans2Num;
				useIdx = use2Idx;
			}
#ifndef _INVENSYS
		/* stevev 12aug11 - skip count is an anti-windup mechanism. discard with caution*/
			else
			if (skipCount<0 || skipCount > MAX_CMD_SKIP)
			{
				skipCount = 0; 
				// leave the primary weight as - is
			}
			else // we have no option, skip 'em all
			{
				curWgt = 0;
				skipCount++;
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,
					  "Best Rd Cmd %d Trans %d  was sent last time and"
					  " there is no second best.  >> discarding weight.\n",
					  lastBestCmdNum,lastBestTransN);

			}
#endif
		}
		// else not a match, keep working
	}
	if ( curWgt > 0 )
	{
		rc = SUCCESS;
		lastBestCmdNum = pCmd->getCmdNumber();
		lastBestTransN = transNum;
		lastIndexSet   = useIdx;

		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,
					  "Best Rd Cmd %d Trans %d  with weight %d and %d indexes.\n", 
					  pCmd->getCmdNumber(),transNum,curWgt, useIdx.size());

	}
	else
	{
		pCmd     = NULL;
		transNum = -1;
		rc       = FAILURE;//nothing to do
		useIdx.clear();
#ifdef _DEBUG
		CVarList* pvl = ((CVarList*)(devPtr()->getListPtr(iT_Variable)));
		if ( pvl )
		{
			INSTANCE_DATA_STATE_T ids;
			int cnt = 0;
			for (CVarList::iterator iT = pvl->begin(); iT<pvl->end();iT++)
			{
				ids = (*iT)->getDataState();
				if (ids == IDS_STALE )
				{
					cnt++;
				}
			}
		}
#endif
	}
		
#ifdef BestReadTime  // top of this file
	// void GETELAPSED(lastTimeBuffer, tmpET); // param timeb to now, returned in a double
	GETELAPSED( BestReadStartBuffer, tmpET );
	LOGIT(CLOG_LOG,"TTTTTTTTTTTTTTTTTTTT Best read took %f sec to find.\n",tmpET);
#endif

	return rc;
}

/* this is the command weighing optimization step 
 * the index relation is only calculated once - here after all the xref but before
 * ANY commands are sent
 */
RETURNCODE CCmdList::BuildIndexMap(void)
{
	RETURNCODE rc = SUCCESS;
	for ( vector<hCcommand*>::iterator iT = begin(); iT != end(); iT++)
	{//iT is a ptr 2 a ptr 2 a hCcommand
		hCcommand *pC = (hCcommand*) (*iT);

		rc |= pC->BuildIndexMap();

	}// next command in the device's command list
	return rc;
}

int CmethodList::getBackgroundMethods(methodCallList_t& methList)
{	
	int fndCnt = 0;
	hCmethodCall locMethCall;

	for ( vector<hCmethod*>::iterator iT = begin(); iT < end(); iT++)
	{//iT is a ptr 2 a ptr 2 a hCmethod
		if ( (*iT)->isBackGround() )
		{
			locMethCall.m_pMeth  = (hCmethod*)(*iT);
			locMethCall.methodID = (*iT)->getID();
			locMethCall.source   = msrc_EXTERN;
			methList.push_back(locMethCall);
			locMethCall.clear();
			fndCnt++;
		}
	}
	return fndCnt;

}


/*************************************************************************************************
 *
 *   $History: ddbItemLists.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
