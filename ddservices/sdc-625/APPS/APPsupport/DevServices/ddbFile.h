/*************************************************************************************************
 *
 * $Workfile: ddbFile.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbFile.h".
 */

#ifndef _DDB_FILE_H
#define _DDB_FILE_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbCollAndArr.h"
#include "ddbFormat.h"

#define NO_MORE_MEMBERS    (-1)
#define FILE_COMPLETE      (-43691)
#define FILE_START_INDENT  ( 0)

#define FILE_RD_BUFF_SIZE	4096

class hCfile : public hCgrpItmBasedClass 
{
protected:
	/* in the parent ::: hCattrGroupItems* pMemberAttr; */
	//hCformatData*      pWrkData;
	hCformatBase*      pFileFormat;
	groupItemPtrList_t WrkList;
	int                nxtMemberIdx;
	
	hCattrHndlg*		pHandling;	// FILE_HANDLING_ID			use aCattrBitstring		// 5  added 29aug16
	
	hCattrUpdateAct*    pUpdateActs;// FILE_UPDATE_ACTIONS_ID	use aCattrActionList	// 6  added 29aug16
// FILE_IDENTITY_ID			use aCattrString		// 7  added 29aug16
	hCattrIdentity*		pIdentity;
	
// FILE_PRIVATE_ID          use aCattrBool          // 8  added 29aug16
	hCattrPrivate*		pPrivate; // (hCattrBase*)new hCattrPrivate(h, (aCattrCondLong*)pACattr );
// FILE_SHARED_ID           use aCattrBool          // 9  added 29aug16
	hCattrShared*		pShared;


public:
	hCfile(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCfile(hCfile*    pSrc,  hCfile*    pVal, itemIdentity_t newID);/* from-typedef constructor */
	RETURNCODE destroy(void);
	virtual ~hCfile()  {
/*
    LINUX_PORT - REVIEW_WITH_HCF: hCgrpItmBasedClass has a virtual destructor 
    that is automatically called on hCfile destruction. Calling it again 
    leads to double deletes, resulting in object destruction issues and 
    aborts.
*/
#if !defined(__GNUC__)
		hCgrpItmBasedClass::~hCgrpItmBasedClass();
#endif // __GNUC__
	};
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{	CValueVarient rV;
		if (  ((fileAttrType_t)attrType) == fileLabel || ((fileAttrType_t)attrType) == fileHelp)
		{
			rV = hCitemBase::getAttrValue(attrType);
		}
		else
		{
			LOGIT(CERR_LOG,"WARN: File's attribute information is not accessable.(%d)\n",attrType);
		}
		return rV;
	};
	
//	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder  //Vibhor 061004: Commented

//unused	RETURNCODE fileWrt(hCformatData*  pData);// self write4file (pData normally==pWrkData)

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(FILE_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(FILE_LABEL, l); };
	RETURNCODE Read(wstring &value){/* TODO: read the succker*/ return FAILURE;};
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ) 
						{/* for now */returnedSize.xval=returnedSize.yval = 0;  return 0;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------File Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "File ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};
	/* stevev 08nov04 - file */
				// called by device with length data in the buffer to fill the file class
				// called with length == zero when complete - memory owned by the device
				// return values: buffer nxt byte to process, length: bytes left to process
	RETURNCODE fill(unsigned char*& buffer, int& length);
				// may return an error if the format is unsupported
	RETURNCODE setFormat(FileFormat_t fmt);
				// called by device until length returned is zero - memory belongs to file
	RETURNCODE empty(hCformatData*& pWriteData);
	/* end    stevev 08nov04 */
};


#endif	// _DDB_FILE_H
