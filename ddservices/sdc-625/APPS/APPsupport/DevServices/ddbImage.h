/*************************************************************************************************
 *
 * $Workfile: ddbImage.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbImage.h".
 */

#ifndef _DDB_IMAGE_H
#define _DDB_IMAGE_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"

#include "ddbGrpItmDesc.h"

/*
	imageAttrLabel			// in base class	use aCattrString 
	imageAttrHelp,			// in base class	use aCattrString
	imageAttrValidity,		// in base class	use aCattrCondLong
	imageAttrLink,								use	aCattrCondReference // could be constString
	imageAttrPath								use aCattrCondLong // index into the image tbl
*/

class hCimage; // the language based image in the image table

class hCimageItem : public hCitemBase 
{// note: run the default browser/app accociated with the type:
	//LONG r = ShellExecute(NULL, "open", "http://www.microsoft.com", NULL, NULL, SW_SHOWNORMAL);
	hCattrLink* pLink;
	hCattrPath* pPath;

public:
	hCimageItem(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCimageItem( hCimageItem* pSrc, itemIdentity_t newID);/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); DESTROY(pLink);DESTROY(pLink);};
	virtual ~hCimageItem()  
	{if (pLink){ DESTROY(pLink); } 
	 if (pPath){ DESTROY(pPath); } };
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual );

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(IMAGE_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(IMAGE_LABEL, l); };

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Image Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Image ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};
	
	//CxImage * getImage(); 
	int  getImageIndex(void);

	bool isLinkString();// returns if the Link is a constant string

	RETURNCODE getLink(string& retString);
	RETURNCODE getLink(hCitemBase*& retString);
	
	void  fillCondDepends(ddbItemList_t&  iDependOn);/* stevev 16nov05 - dependency extentions */
	int notification(hCmsgList& msgList,bool isValidity); // stevev 18nov05 - overload 4 path
};


#endif	// _DDB_IMAGE_H
