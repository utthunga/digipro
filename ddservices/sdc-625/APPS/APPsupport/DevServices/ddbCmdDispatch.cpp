/*************************************************************************************************
 *
 * $Workfile: ddbCmdDispatch.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the code for a defaultdispatcher 
 */


#ifdef _DEBUG /* debugging delete crash */
bool enableAssert = false;
#endif

#include "ddbCmdDispatch.h"
#include "ddbTransfer.h"
#include "ddbCommand.h"
#include "ddbItemLists.h"
#include "ddbDeviceService.h"
#include <time.h>
#include "registerwaits.h"

#include "MEE.h"

#include "ddbDevice.h"	//support transport


// we hold the mutex thru the build process but don't re-acquire if we re-enter to send a readImd

#define WGT_CMD			33
#define WGT_CMD_ALT		174	/* for contrast */
#define WGT_SYMBOL_OF_INTEREST   (0x4090)	/* (0xd2) || ((*VLIT)->getID())*/

//#define LOG_INDEX_MUTEX
#ifdef _DEBUG
#define LOG_WEIGH_FILTER 1
#define WGT_DEBUG	/* undefine to turn off the weighing logs */

bool isOfInterest = false;  /* used by WGT_DEBUG to trigger subroutine logging */

#endif

#define _USE_VAR_OPT 1 /* use the variable optimization techniques */

/**** helper for index setting so we don't have to change the interface to the vars */
static char tmpNumber[64];
//char* int2str(int v) { itoa(v,tmpNumber,10); return tmpNumber;}; changed itoa below to _itoa PAW 24/04/09
char* int2str(int v) { _itoa(v,tmpNumber,10); return tmpNumber;};


extern unsigned long ddbGetTickCount(); // located in ddb_EventMutex.cpp
extern __int64 idBase; // there is only one allowed per application, a global is appropriate.

// this is a global virtual function pointers::
// global pointer to function initialized with virtual address:
//      this is a pointer to the function in the CLASS !! you must tell the 
//		compiler what instance of the class to use: (pMyDispatcher->*fpCalcWgt)()
// these should be valid even in a derived class (you may want to check it though)
//
hCcmdDispatcher::fpCalcWtTrans_t fpCalcWgt  = & hCcmdDispatcher::calcWeightOptimized;
// re-enabled 12feb10
// this is no longer used..hCcmdDispatcher::fpCalcWtTrans_t fpCalcWgt  = & hCcmdDispatcher::calcWeight;

/*************************************************************************************************
*  Call backs for the registered tasks
*  These are wrappers for task's global C callback to get to the class's task methods
*
*/

//////////////////////////////////////////////////////////////////////////////////////////////////
// This is the background response code/status/cmd-48 task. It is triggered every time we 
//	receive a cmd48 or a non-zero response code or device status
//
VOID CALLBACK gfWaitAndTimerAsyncCallback(
	  PVOID   lpParameter,       // thread data(Context)
	  waitStatus_t TimerOrWaitFired)  // reason -- true == Timeout/death, false == event signaled
{	
	hCcmdDispatcher* pDispatch = (hCcmdDispatcher*)lpParameter;
	// execute this class's rcv event callback
	if (pDispatch != NULL)
	{
		pDispatch->commAsyncTask(TimerOrWaitFired);
	}
	// exit to loop and wait again forever
	return;
}


/*************************************************************************************************
 *  Call back for the background task
 *  This is a 'C' wrapper for task's global C callback to get to the class's task methods
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////
// This is the background communications task. It triggers when the UI has idle time, does a  
//	little work and waits some more.  
VOID CALLBACK gfWaitAndTimerIdleCallback(
      PVOID   lpParameter,      // thread data(Context)
      waitStatus_t TimerOrWaitFired)  // reason -- true == Timeout/death, false == event signaled
{	
    hCcmdDispatcher* pDispatch = (hCcmdDispatcher*)lpParameter;
    // execute this class's send event callback
    if (pDispatch != NULL && pDispatch->appCommEnabled() )// don't mess with idle task disabled
    {
    	pDispatch->commIdleTask(TimerOrWaitFired);// will do some work and return 
    }
    // exit to loop and wait again forever
    return;
}


/********************************************************************************************* 
 * -- Download Mode --
 *  When opening a device to set a few parameters, you don't want to wait for an entire upload.
 *	The Critical Parameters sometimes set other values STALE through relations of one sort or
 *  other.  So, when you are trying to just get the minimum load, you want the Criticals and
 *  those others that were set.  Having isDownloadMode() return true will turn off the auto 
 *  reading of the Uninitialized and allow only the STALE (from the relations) be read.
 *   bool isDownloadMode();// checks device's mode for this condition
 *********************************************************************************************/

//////////////////////////////////////////////////////////////////////////////////////////////////

extern volatile unsigned long randomKey;

//////////////////////////////////////////////////////////////////////////////////////////////////
//	A global function to generate support for the transfer ports
//  Notice that these classes support both the simulation and the host/master sides
//
//  If the port requested is not supported, this will return NULL
//
hCPort* newPortSupportClass( unsigned char  portNumber, hCdeviceSrvc * pD, hCTransferChannel* p )
{
	hCPort* newport = NULL;
	if ( portNumber < 2 || portNumber >239 || pD == NULL )
		return NULL;

	switch (portNumber)
	{
	case  2: newport = (hCPort*)new hCRdDevConfig(pD,p); break;//read device configuration class 
												   // would have to be a decendent of hCPort class
	case  3: 
	case  4:
	case  5:
	case  6:
	case  7:
	case  8:
	case  9:
	case 10:
	case 11:
	case 12:
	case 13:
	case 14:
	case 15:
	case 16:
	case 17:
	case 18:
	case 19:
	case 20:

	default:
		newport = NULL; // unsupported
		break;
	}
	return newport;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//	debug helper function
//
wstring getOriginString(cmdOriginator_t from)
{
	wstring ret;

	switch (from)
	{
	case  co_UNDEFINED:		// = 0x00,      // I haven't a clue where I came from
		{	ret = L"Undefined";
		}
		break;
	case  co_READ_IMD:		//  = 0x01,	    // From ReadImd (was co_DEV_LOADING)
		{	ret = L"Read Immed";
		}
		break;
	case  co_METHOD:		//  = 0x02,		// From sendMethCmd
		{	ret = L"Method";
		}
		break;
	case  co_WRITEIMD:		//  = 0x04,		// From WriteImd (was co_WRITE)
		{	ret = L"Write Immed";
		}
		break;
	case  co_ASYNC_READ:	// = 0x08,	// From Async Read (ServiceReads)
		{	ret = L"Asynchronous Rd";
		}
		break;
	case  co_ASYNC_WRITE:	// =0x10,	// From Async Write (ServiceWrites)
		{	ret = L"Asynchronous Wr";
		}
		break;
	case  co_TRANSPORT:		// = 0x20   // command 112 cycles
		{	ret = L"Block Transport";
		}
		break;
	default:
		{	ret = L"Unknown Source";
		}
		break;
	}
	return ret;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
//	The command dispatcher class 
//
hCcmdDispatcher::hCcmdDispatcher(hCcommInfc* outComm, DevInfcHandle_t thisHandle ) 
	: hCobject(thisHandle),theMsgQueue() 
#ifdef _DEBUG
	, hCntMutex("CmdDispatch")
#endif
{
	pCommInterface = outComm; 

	if ( outComm != NULL )
	{
		outComm->setRcvService((hCsvcRcvPkt*)this);
	}
	ourPolicy.allowSyncWrites = 0;
	ourPolicy.allowSyncReads  = 0;
	ourPolicy.incrementalWrite= 1;
	ourPolicy.incrementalRead = 1;// temp 6jan14..0;// tmp stevev 24jan06 - 4 samson valve....was:: 1;
	ourPolicy.isReplyOnly     = 0; // default to client
	ourPolicy.isPartOfTok     = 0;
#ifndef PASS_LISTS		/* command line define for non-compliant list handling */
	ourPolicy.abortOnNoListElem = 0;//// AGAINST SPEC - DO TO allow List Test
#else
	ourPolicy.abortOnNoListElem = 1;//// WITHIN SPEC - DO NOT allow List Test to pass
#endif
	hostCommStatus            = 0x00;
	hRegisteredAsyncTask      = 0;
	pCommMutex       = NULL;
//stevev - remove	pAsyncEnEvent    = NULL;
//stevev - remove	pCriticalSect    = NULL;
	pIndexMutex      = NULL;
	pCmdList         = NULL;
	pVarList         = NULL;// 15nov11-varOptimization
	pAsyncRespCdEvent= NULL;
	/* VMKP added on 030404*/
	dispatchEnabled = true;// shake the hand
	/* VMKP added on 030404*/
	msgDeque.clear();
	lastCmpMode = compatability();
	/* stevev 28jul05 */
	hRegisteredIdleTask  = 0;
	pIdleTriggerEvent   = NULL;
	idleRunCounter      = 0;
	pByteCntVar         = NULL; // stevev 27aug09
	dispatch_Status     = dc_Nothing;    // stevev 16feb12

	memset((void*)&(openPorts[0]),0,sizeof(openPorts));
#ifndef _WIN32_WCE
	srand( (unsigned)time( NULL ) );
#else
	srand( (unsigned)time_ce( NULL ) );
#endif
	randomKey = rand();

	indexMutexCount = 0;
	theMsgQueue.initialize(16);// original grow by was 3
}

hCcmdDispatcher::~hCcmdDispatcher() 
{
	
	if (hRegisteredAsyncTask != 0)
	{
		Thread_UnregisterWait( hRegisteredAsyncTask ) ;
		hRegisteredAsyncTask = 0;
	}
	msgDeque.clear();

	RAZE(pCommMutex);
//stevev - remove	RAZE(pAsyncEnEvent);
//stevev - remove	RAZE(pCriticalSect);
	RAZE(pIndexMutex);// releases as required
	RAZE(pAsyncRespCdEvent);
/* Enabling this will cause assertion in ddbQueue.h (hCqueue:: destroy) function 
	Since Empty queue packets allocated for queue are done in one thread
	and deleting them in another thread causing the assertion.
	Generate a memory leak in order not to avoid an assertion (VMKP 260304)
*/
//	theMsgQueue.destroy();
	/* stevev 28jul05 */
	if (hRegisteredIdleTask != 0)
	{
		Thread_UnregisterWait( hRegisteredIdleTask );
		hRegisteredIdleTask = 0;
	}
	RAZE(pIdleTriggerEvent);

	// stevev 27feb09 - we own the ports...
	for ( int y=0; y < 0x100; y++)
	{
		RAZE( (openPorts[y]) );
	}
	if ( pCommInterface != NULL && pCommInterface->getRcvService() == (hCsvcRcvPkt*)this)
	{
		pCommInterface->setRcvService((hCsvcRcvPkt*)NULL);
	}
// the following generates an assertion on shutdown
/*	theMsgQueue.destroy();  let it leak*/
}

hSdispatchPolicy_t  hCcmdDispatcher::
setPolicy(hSdispatchPolicy_t newP) 
{ 
	hSdispatchPolicy_t oldP; oldP = ourPolicy;  
	//ourPolicy = newP; 
	ourPolicy.allowSyncWrites=newP.allowSyncWrites;
	ourPolicy.allowSyncReads=newP.allowSyncReads; 
	ourPolicy.incrementalWrite=newP.incrementalWrite;
	ourPolicy.incrementalRead=newP.incrementalRead;
	ourPolicy.isReplyOnly=newP.isReplyOnly; 
	ourPolicy.isPartOfTok=newP.isPartOfTok;
	ourPolicy.abortOnNoListElem = newP.abortOnNoListElem;
	return (oldP);
};

// since the dispatcher must be passed into the device creation, we have no handle at
// at construction time.  The device constructor/initializer must set the dispatcher
// handle to match it's own.  Any other post-device-creation / pre-operation initialization
// would go here.
// 
RETURNCODE hCcmdDispatcher::
initDispatch(DevInfcHandle_t thisHandle)
{
	RETURNCODE rc = SUCCESS, sc = SUCCESS;
	setPtr(thisHandle);
	pCommMutex        = hCommMutex.getMutexControl("CommMutex");
//stevev - remove	pAsyncEnEvent     = hAsyncEnEvent.getEventControl();
//stevev - remove	pCriticalSect     = hCriticalSect.getMutexControl();
	pIndexMutex       = hIndexMutex.getMutexControl("IndexMutex");
	indexMutexCount = 0;
	RAZE(pAsyncRespCdEvent);	
	pAsyncRespCdEvent = hAsyncRCEvent.getEventControl();
	// stevev 28jul05
	RAZE(pIdleTriggerEvent);	
	pIdleTriggerEvent = hIdleTriggerEvent.getEventControl();
//stevev - remove	asyncWaiting   = false;
//stevev - remove	entryCnt       = 0;
	
	hostCommStatus = 0x00;

	theMsgQueue.FlushQueue();

//cerrxx<<">>>>>>>>>>>> init Dispatch >>>>>>>>>>>>>>"<<endl;
	if ( devPtr() != NULL )
	{
		pCmdList = (CCmdList*) devPtr()->getListPtr(iT_Command);//<hCcommand*>
		if ( pCmdList != NULL )
		{
			pCmdList->insertWgtVisitor((hCcmdDispatcher*)this);// into all transactions
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: no command list for the dispatcher to use.\n");
			rc = FAILURE;
		}
		
		pVarList = (CVarList*) devPtr()->getListPtr(iT_Variable);
		if ( pVarList == NULL )
		{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: no variable list for the dispatcher to use.\n");
			rc = FAILURE;
		}
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Device not found in dispatcher's initDispatch.\n");
		pCmdList = NULL;
		rc = FAILURE;
	}

	if ( rc == SUCCESS )
	{
		// generate background task to handle the async response codes asyncronously
		if ( pAsyncRespCdEvent )
		{
			Thread_RegisterWaitForSingleObject(& hRegisteredAsyncTask, 
							pAsyncRespCdEvent->getEventHandle(), gfWaitAndTimerAsyncCallback,
							(void*)this,     INFINITE,     WT_EXECUTEDEFAULT
#ifdef _DEBUG
							,"Async Response Handler"
#endif
							);
		}
		
		// generate background task to handle the command weighing while nothing else happening
		if ( pIdleTriggerEvent )
		{
			Thread_RegisterWaitForSingleObject(& hRegisteredIdleTask, 
							pIdleTriggerEvent->getEventHandle(), gfWaitAndTimerIdleCallback,
							(void*)this,     INFINITE,     WT_EXECUTEDEFAULT | WT_NORM_LESS_TWO
			//dbg			(void*)this,     INFINITE,     WT_EXECUTEDEFAULT | WT_NORMAL_PRI
#ifdef _DEBUG
							,"Idle task background operations"
#endif
							);
		}

	}

	hCitemBase* pItm = NULL;    // fill special symbol for testing (if it exists in the DD)
	sc = devPtr()->getItemBySymNumber(1022,&pItm);
	if ( sc == SUCCESS && pItm != NULL && pItm->IsVariable())
	{
		pByteCntVar = (hCVar*) pItm;
	}// else leave it null
	return rc;
}

void hCcmdDispatcher::triggerIdleTask(void)
{
	if (pIdleTriggerEvent && hRegisteredIdleTask)//init'd
	{
		pIdleTriggerEvent->triggerEvent();
	}
}

RETURNCODE hCcmdDispatcher::shutdownDispatch(void)// deactivation
{
	RETURNCODE rc = SUCCESS;
		
	if (hRegisteredIdleTask != 0)
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> Stopping Idle Thread.\n");
		Thread_UnregisterWait( hRegisteredIdleTask, 20000 ) ;
		hRegisteredIdleTask = 0;
	}
	RAZE(pIdleTriggerEvent);

	LOGIF(LOGP_START_STOP)(CLOG_LOG,"> Idle Thread stopped.\n");
	if ( pCommInterface != NULL )
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> command dispatcher shutting down (3)\n");
		rc = pCommInterface->shutdownComm();// this will ripple thru the methods in this class
		if ( rc == SUCCESS && theMsgQueue.GetQueueState() > 0 )
		{
			LOGIF(LOGP_START_STOP)(CLOG_LOG,
						"> command dispatcher shut down with stuff in the msgQueue (16)\n");
			rc = -1;
		}//else let the error code percolate out
		else
			LOGIF(LOGP_START_STOP)(CLOG_LOG,
						"> command dispatcher shut down without incident (16s)\n");
	}
	else
	{// no comm, nothing we need to do
		rc = SUCCESS;
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
					"> command dispatcher shut down without a comm Interface (16wo)\n");
	}	
	if (hRegisteredAsyncTask != 0)
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> Stopping Async Thread.\n");
		Thread_UnregisterWait( hRegisteredAsyncTask ) ;
		hRegisteredAsyncTask = 0;
	}
	RAZE(pAsyncRespCdEvent);
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"> Async Thread stopped\n"
									"> command dispatcher shut down (17)  ");
							logTime();// isa logif start_stop (has newline)

	return rc;
}

void  hCcmdDispatcher::disableAppCommRequests(void)
{
	if (pCommInterface)
		pCommInterface->disableAppCommRequests();
}
void  hCcmdDispatcher::enableAppCommRequests (void)
{
	if (pCommInterface)
		pCommInterface->enableAppCommRequests();
}
// stevev 31aug05
bool  hCcmdDispatcher::appCommEnabled (void)
{
#ifndef __GNUC__	//L&T MODIFICATIONS : KEEP TRACK!!!
    if (pCommInterface == NULL)
		return false;
    else
    	return pCommInterface->appCommEnabled();
#else
	return pCommInterface->appCommEnabled();
#endif
}
// end 31aug05
/* stevev - replace with counting mutex 
RETURNCODE hCcmdDispatcher::incEntry(void)
{
	if (pCriticalSect == NULL) 
	{
		cerrxx<< "ERROR:  CmdDispatch.incEntry didn't find a critical resource."<<endl;
		return APP_CONSTRUCT_ERR;
	}

	RETURNCODE rc = pCriticalSect->aquireMutex(getMutexTimeOut);
	if ( rc != SUCCESS )
	{ 
		cerrxx<< "ERROR:  CmdDispatch.incEntry aquire critical mutex failed."<<endl;
	}
	else
	{
		entryCnt++;
		pCriticalSect->relinquishMutex();
	}
	return rc;
}

RETURNCODE hCcmdDispatcher::decEntry(void)
{
	if (pCriticalSect == NULL || pAsyncEnEvent == NULL) 
	{
		cerrxx << "ERROR:  CmdDispatch.decEntry didn't find a critical resource."<<endl;
		return APP_CONSTRUCT_ERR;
	}
	RETURNCODE rc =  pCriticalSect->aquireMutex(getMutexTimeOut);
	if ( rc != SUCCESS )
	{ 
		cerrxx << "ERROR:  CmdDispatch.decEntry aquire critical mutex failed."<<endl;
		entryCnt--;
		return rc;
	}
	// else continue normal exit
	entryCnt--;
	if (entryCnt == 0 && asyncWaiting)
	{
		pCriticalSect->relinquishMutex();
		pAsyncEnEvent->triggerEvent();// allow async to run now
	}
	else
	{
		pCriticalSect->relinquishMutex();
	}
	return SUCCESS;
}
 end stevev replace remove */

 //  blockable  send -- Syncro Rd/Wr and methods
//	from designed: SendMethod(cmd*, trans#, actionsEnabled, isBlocking, completeEventId)
/* Added on 160404 Removed localMsgCycle from the passed parameter list and added a WriteImd flag to
			identify Send called from WriteImd */
RETURNCODE hCcmdDispatcher::
Send   (hCcommand* pCmd, long transNum, bool actionsEn, blockingType_t isBlking, 
		hCevent* peDone, indexUseList_t& rIdxUse,bool bIsWriteImd,cmdOriginator_t cmdOriginator) //hMsgCycle_t *pMsgCycle/*Vibhor 250304*/)
{
	RETURNCODE rc = FAILURE;
	hPkt* pXmitPkt = NULL;
	CValueVarient  methRetValue;
	cmdInfo_t sendInfo;
	sendInfo.clear();
	indexUseList_t localIndexList;	// used to save index values
	bool haveIdxMutex = false; // stevev 21sep09 - to deal with try/catch
  #ifdef _WIN32_WCE
	time_t_ce start_ce;	// PAW win CE time 24/04/09
  #endif


  try{ /* fdm merge try/catch */

/* stevev - replace with counting mutex 
	if ( pCmd == NULL || pCommMutex == NULL || pAsyncEnEvent == NULL )
*/
	if ( pCmd == NULL || pCommMutex == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: CmdDispatch.Send called without a critical resource.");
		if (pCmd == NULL)
		{ LOGIT(CERR_LOG,"(Command ptr NULL)"); }
		if (pCommMutex == NULL)
		{ LOGIT(CERR_LOG,"(Mutex ptr NULL)"); }
// stevev - replace		if (pAsyncEnEvent == NULL)
// stevev - replace		{ cerrxx << "(Event ptr NULL)"; }
		LOGIT(CERR_LOG,"\n");
		return APP_CONSTRUCT_ERR;
	}
#ifdef _DEBUG
	if ( peDone != NULL && isBlking == bt_NOTBLOCKING)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Programmer error-a completion event was passed into Send "
			"without blocking.\n");
	}
	int dbgCmdNo = pCmd->getCmdNumber();
#endif
/* stevev - replace with counting mutex 
	if ( (rc = incEntry()) != SUCCESS )
	{	return rc;
	}
   end stevev replace remove */
// changed to enterSend()	hCntMutex.getCountingMutex(); // wait forever - we can't go on without it
/* end stevev - add*/
	// stevev 08sep09 - this probably does no good since the routine is re-entrant anyway
	if ( (rc = enterSend(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING)) != SUCCESS )
		return rc;

	/* stevev 4/23/04
	int ecnt = hCntMutex.getEntryCount();
	if ( ecnt > 1 
	end stevev4/23/04*/
//	hold the completeEventID (we may need to overwrite it)
	hCevent* holdEvent = peDone;
	hCevent  localEvent;// on the stack, will be deleted on exit
	hCevent::hCeventCntrl* pLocalEvtCntl =  NULL;

	//get an empty MsgCycle
	hMsgCycle_t* pWrkMsgCycle = NULL;

	pWrkMsgCycle = theMsgQueue.GetEmptyObject( (isBlking==bt_ISBLOCKING) );// pend on an empty when blocking
	
	//hMsgCycle_t* pWrkMsgCycle = theMsgQueue.GetEmptyObject();
#ifdef QDBG
clog <<"got Msg Cycle "<<hex << pWrkMsgCycle << dec << endl;
#endif

	if (pWrkMsgCycle == NULL)
	{		
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:  CmdDispatch.Send could not get an empty Msg packet."
			" (non-blocking call)\n");
		// replace stevev - decEntry();
		// replaced stevev hCntMutex.putCountingMutex();
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
		return FAILURE;
	}// else work on

	// set cmd#, transaction#, isBlocking, actionsEnabled, completeEventID 
	pWrkMsgCycle->actionsEn = actionsEn;
	pWrkMsgCycle->isBlking  = isBlking;
	pWrkMsgCycle->pCmd      = pCmd;
	pWrkMsgCycle->transNum  = transNum;
	pWrkMsgCycle->peDone    = peDone;

	//Anil : 151206 We need information down So fill it in the message cycle
	pWrkMsgCycle->m_cmdOrginator =  cmdOriginator;


	if (rIdxUse.size() > 0)
	{
		pWrkMsgCycle->indexList = rIdxUse;
#ifdef _DEBUG
		LOGIT(CLOG_LOG, " Send Cmd %d with %d indexes in list.\n", pCmd->getCmdNumber(),rIdxUse.size());
		for ( idxUseIT TImp = rIdxUse.begin(); TImp != rIdxUse.end(); ++TImp)
		{LOGIT(CLOG_LOG,"    0x%04x (%d)  ",TImp->indexSymID,(int)(TImp->indexDispValue)); }LOGIT(CLOG_LOG,"\n");
#endif
		if ( 0 == indexMutexCount )// Dispacher's Send() owns the mutex
		{// first to come in
			pIndexMutex->aquireMutex(hCevent::FOREVER);
			// stevev 17sep09 - move outside...indexMutexCount++;
		}
		indexMutexCount++;
		haveIdxMutex = true;
#ifdef LOG_INDEX_MUTEX
		LOGIT(CLOG_LOG,"Got Index Mutex: #%d Dispatch Line %d\n",indexMutexCount,__LINE__);
#endif

		hIndexUse_t localIdxUse;
		hCitemBase* pItemWrk = NULL;
		hCinteger* pIntWrk;
		CValueVarient localWrkVar;
		indexUseList_t::iterator itIUL;
LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Send presetting local indexes:");
		for (itIUL = rIdxUse.begin();itIUL < rIdxUse.end();itIUL++)
		{// itIUL isa ptr2a hIndexUse_t
			if ( SUCCESS == (rc = devPtr()->getItemBySymNumber(itIUL->indexSymID,&pItemWrk))
				&& pItemWrk != NULL )
			{
				localIdxUse.indexSymID = itIUL->indexSymID;
				pIntWrk     = (hCinteger*)pItemWrk;
				localWrkVar = pIntWrk->getDispValue(true);
				localIdxUse.indexDispValue = localWrkVar;
				localIndexList.push_back(localIdxUse);
				localIdxUse.clear();

				if ( pIntWrk->IsLocal() )
				{		
					localWrkVar= (unsigned)(itIUL->indexDispValue);
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," Idx 0x%04x to %d [",
										  itIUL->indexSymID,(int)(itIUL->indexDispValue));
					pIntWrk->setDispValue(localWrkVar, true);	// set one
					pIntWrk->ApplyIt();				// copy it to the other
					pIntWrk->markItemState(IDS_CACHED);// force it good
				}
				else
				{
LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," Not Set/Non-Local 0x%04x ",localIdxUse.indexSymID);
				}
			}// not much we can do
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Send could not get item pointer for an index symbol "
					"0x%04x", itIUL->indexSymID);
				if (rc != SUCCESS)
				{
					LOGIT(CERR_LOG," (rc= 0x02x)\n",rc);
				}
				else
				{
					LOGIT(CERR_LOG," (item PointerNull)\n");
				}
			}
		}// next
LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"\n");
////////////////////temp
	for (itIUL = pWrkMsgCycle->indexList.begin();itIUL < pWrkMsgCycle->indexList.end();itIUL++)
	{// itIUL isa ptr2a hIndexUse_t
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," Idx 0x%04x to %d {",
			itIUL->indexSymID,(int)(itIUL->indexDispValue));
	}// next
	LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"\n");
///////////////////////		
//
//		/* BuildRequest can re-enter here, we cannot hold this mutex */
//		pIndexMutex->relinquishMutex();//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
//////////////////////
	}
	else
	{	// stevev 27aug10 - due to MS STL error, you cannot clear an empty list(without crashing)
		if (pWrkMsgCycle->indexList.size())
			pWrkMsgCycle->indexList.clear();
	}

	//cmd->getResponse Codes (cmd RCs into msgCycle)
	pCmd->getRespCodes(pWrkMsgCycle->respCds);
	// transaction response codes handled in build request

	//cmd->BuildRequest(the MsgCycle)
	pXmitPkt = pCommInterface->getMTPacket((isBlking==bt_ISBLOCKING));// pend on MT when blocking
	#ifdef QDBG
	clog <<"got    Pkt "<<hex << pXmitPkt << dec << endl;
	#endif
	if (pXmitPkt == NULL)
	{
		if (isBlking==bt_ISBLOCKING)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:  CmdDispatch.Send could not get an empty packet.\n");
		}
		#ifdef QDBG
		clog <<"        CmdDispatch.Send could not get an empty packet." << endl;
		#endif
		/* VMKP Added on 270104 
		  (Allow other task to get blocked empty packet in GetEmptyObject 
		  in case the getMTPacket returns NULL),  This will allow
		  to trigger an event in ReturnMTObject in case of return packet 
		  by getMTPacket is NULL */
#ifdef NO_GOVERNOR
		#ifdef QDBG
		clog <<"returning MT Pkt "<<hex << pXmitPkt << dec << endl;
		#endif
		pCommInterface->putMTPacket(pXmitPkt);
#endif
		/* End of VMKP Modification 270104 */
//we cannot hold the mutex due to re-entrancy deadlock
//	if (rIdxUse.size() > 0)
//	{		
//		pIndexMutex->relinquishMutex();
//	}
		#ifdef QDBG
		clog <<"returning MT Msg-Cycle "<<hex << pWrkMsgCycle << dec << endl;
		#endif
		if (rIdxUse.size() > 0 )
		{
			if ( indexMutexCount == 1 )
			{
				pIndexMutex->relinquishMutex();//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
			}
			indexMutexCount--;
			haveIdxMutex = false;
		}
		theMsgQueue.ReturnMTObject(pWrkMsgCycle);
		// stevev replace:: decEntry();		
		// stevev replace:: hCntMutex.putCountingMutex();		
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
		return FAILURE;
	}// else work on
//if (pCmd->getCmdNumber() == 136 )
//{
//	clog<<"break"<<endl;
//}
	//rc = pCurTrans->generateRequestPkt( pXmitPkt, resolutionHandle);
	rc = pCmd->BuildRequest(pXmitPkt, pWrkMsgCycle );// note this may re-enter this method
	if (rc == SUCCESS && pWrkMsgCycle->indexList.size() > 0 )// stevev 17mar11-aded rc test --
									// no need to set indexes if we're leaving in the next step
	{// restore
		hCitemBase* pItemWrk = NULL;
		hCinteger* pIntWrk;
		CValueVarient localWrkVar;

//#ifdef _DEBUG
//		pIndexMutex->aquireMutex(hCevent::FOREVER, __FILE__, __LINE__ );
//#else
//		pIndexMutex->aquireMutex();// waits forever.....*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
//#endif // _DEBUG

#ifdef LOG_INDEX_MUTEX
		LOGIT(CLOG_LOG,"Got Index Mutex: Dispatch Line %d\n",__LINE__);
#endif
		for (indexUseList_t::iterator itIUL = localIndexList.begin();
			 itIUL < localIndexList.end();    itIUL++)
		{// itIUL isa ptr2a hIndexUse_t
			if ( SUCCESS == devPtr()->getItemBySymNumber(itIUL->indexSymID,&pItemWrk)
				&& pItemWrk != NULL )
			{
				pIntWrk     = (hCinteger*)pItemWrk;
				int y = (int)(itIUL->indexDispValue);
				pIntWrk->SetIt(&y);	//Sets the Write Value (not the Real Val) returns isChanged
				pIntWrk->ApplyIt();
			}// not much we can do
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: could not get item pointer to restore an index symbol.\n");
			}
		}
		localIndexList.clear();	 	
		if ( indexMutexCount == 1 )
		{// last one releases it
			// stevev 17sep09 - move outside...indexMutexCount = 0;
			pIndexMutex->relinquishMutex();//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
		}
		indexMutexCount--;
		haveIdxMutex = false;
	}
	/**********************************************************/
	/*	This section is a work around for methods not running in this thread
		If any methods send a command then there will be a deadlock pending counting mutex.
		Note that this could be dangerous if others are waiting for this task.
	*/
	if (rc == SUCCESS && pWrkMsgCycle->actionsEn &&  pWrkMsgCycle->pre_Actions.size() > 0)
	{	//device->ExecuteMethod(itemID) for each in the list (as determined by previous success)
		//													 <possible spec interpretation>
		hCmethodCall wrkMthCall;
		vector<hCmethodCall> postActionCallList;
		UINT32       methID = 0;
	
		// replace stevev - hCntMutex.putCountingMutex();
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
// L&T Modifications : ABB Tx. Support - start
#ifndef __GNUC__
#ifdef _CONSOLE
#define METHODSUPPORT
#else
#undef METHODSUPPORT
#endif

#ifndef METHODSUPPORT
		rc = devPtr()->executeMethod(pWrkMsgCycle->pre_Actions);
#else
		rc = ((hCddbDevice*)devPtr())->executeMethodForMS(pWrkMsgCycle->pre_Actions,(hCddbDevice*)devPtr());
#endif
#else
		rc = ((hCddbDevice*)devPtr())->executeMethodForMS(pWrkMsgCycle->pre_Actions,(hCddbDevice*)devPtr());
#endif
// L&T Modifications : ABB Tx. Support - end
		//TODO : handle the return code and teke the action apropriately
		//Anil 140906 Commented the below code
		//rc = devPtr()->executeMethod(pWrkMsgCycle->pre_Actions, methRetValue);
//changed to enterSend()		hCntMutex.getCountingMutex();
		if ( rc == SUCCESS )
			 rc = enterSend(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);

		// TODO: handle return values here	
	}//endif
	/**********************************************************/
	if ( rc != SUCCESS)
	{// do NOT send this command
		#ifdef QDBG
		clog <<"returning MT Pkt "<<hex << pXmitPkt << dec << endl;
		#endif
		pCommInterface->putMTPacket(pXmitPkt);
		#ifdef QDBG
		clog <<"returning MT Msg Cycle "<<hex << pWrkMsgCycle << dec << endl;
		#endif
		theMsgQueue.ReturnMTObject(pWrkMsgCycle);
		// replace - stevev:: decEntry();
		// replace - stevev:: hCntMutex.putCountingMutex();
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
		return rc;
	}
	//if isBlocking || MsgCycle.Post Action List is not empty
	//DEEPAK: 170105 post actions are handled at servicerceive packet
	//Anil 140906 Added the below code and made it as blocking call if uiNoOfMethRunning is greater thatn zero
	//uiNoOfMethRunning is greater than zero means Already a method is running and it is executing action  again
	unsigned int uiNoOfMethRunning = devPtr()->getMEEdepth();
	if (pWrkMsgCycle->isBlking == bt_ISBLOCKING || uiNoOfMethRunning != 0|| pWrkMsgCycle->postActions.size() > 0)
	{//get an event and put it into MsgCycle.CompleteEvent (overwites passed in version)
		pWrkMsgCycle->peDone   = &localEvent;
		//set MsgCycle.isBlocking to true
		pWrkMsgCycle->isBlking = bt_ISBLOCKING;
	}//endif
	// *************************************************************************************
	//get Comm-Mutex ( block other access to actual send)  
	//				[in case the service and the display request a send at the same time ]
	//rc = pCommMutex->aquireMutex();
	rc = pCommMutex->aquireMutex(getLongMutexTimeOut);
	if ( rc != SUCCESS )
	{ 
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:  CmdDispatch.Send aquire comm mutex failed.\n");
		//pSendMutex->relinquishMutex();
#ifdef QDBG
	clog <<"returning MT2 Pkt "<<hex << pXmitPkt << dec << endl;
#endif
		pCommInterface->putMTPacket(pXmitPkt);
#ifdef QDBG
	clog <<"returning MT2 Msg Cycle "<<hex << pWrkMsgCycle << dec << endl;
#endif
		theMsgQueue.ReturnMTObject(pWrkMsgCycle);
		// replace - stevev:: decEntry();
		// replace - stevev:: hCntMutex.putCountingMutex();
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
		return rc;
	
	}
	//Set MsgCycle sendtime and  Add MsgCycle to queActivity List 
#ifndef _WIN32_WCE			// PAW 24/04/09
	pWrkMsgCycle->sendTime = time(NULL);// or time(&(pWrkMsgCycle->sendTime));	
	_ftime( &(pWrkMsgCycle->sendTimeBuffer) );
#else
 #ifdef USED	// think these are causing a memory corruption PAW 14/08/09
	pWrkMsgCycle->sendTime = time_ce(NULL);// or time(&(pWrkMsgCycle->sendTime));	
 #endif
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	pWrkMsgCycle->sendMSBuffer = st.wMilliseconds;
//	start_ce = time_ce(&(pWrkMsgCycle->sendTimeBuffer));	// timedebugging replace this
#endif
	if ( cmdOriginator == co_METHOD)
	{
		pXmitPkt->m_thrdCntl.fromMethod = true;
	}
	else
	{// should already be this value
		pXmitPkt->m_thrdCntl.fromMethod = false;
	}
	pWrkMsgCycle->chkSum = pXmitPkt->checkSum();
	pWrkMsgCycle->transactionID =       // see notes on transaction id below
		pXmitPkt->transactionID = devPtr()->getIDbase() + ddbGetTickCount();
	int msgRet = 0;
	if (pWrkMsgCycle->isBlking == bt_ISBLOCKING)
	{
		msgRet = theMsgQueue.PutObject2Que(pWrkMsgCycle, false);// at the front
	}
	else
	{
		msgRet = theMsgQueue.PutObject2Que(pWrkMsgCycle, true);// at the end
	}
	if ( msgRet < 0 )// now does so uniquely
	{// a queue problem or exists in the mesg queue already
		DEBUGLOG(CLOG_LOG,"   message cycle queue failed  (cmd probably already sent)\n");
		pCommMutex->relinquishMutex();
#ifdef QDBG
	clog <<"returning MT 2 Pkt "<<hex << pXmitPkt << dec << endl;
#endif
		pCommInterface->putMTPacket(pXmitPkt);
#ifdef QDBG
	clog <<"returning MT 2 Msg Cycle "<<hex << pWrkMsgCycle << dec << endl;
#endif
		theMsgQueue.ReturnMTObject(pWrkMsgCycle);
		// replace - stevev:: decEntry();
		// replace - stevev:: hCntMutex.putCountingMutex();
		exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
		return rc;
	}
	//else // queue took ownership of msgcycle
	msgRet = 0; // in case this is needed later

	//CommIfc->SendCommand()
	/* * * * * * the actual send * * * * * * * */
	if (pWrkMsgCycle->pCmd != NULL)
	{
#ifndef _WIN32_WCE	// PAW 24/04/09
#ifndef  _USE_32BIT_TIME_T /* timeb.h...#define _timeb      __timeb64 */
//		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,L"%I64d.%hd Send   Cmd %d  Trans %d from %ws\n",
//										 pWrkMsgCycle->sendTimeBuffer.time,/* int64 */
// this crashes the logging          	 pWrkMsgCycle->sendTimeBuffer.millitm,/*unsigned short*/
// function for some unknown reason		 pCmd->getCmdNumber(), transNum, // int, long
//										 getOriginString(cmdOriginator).c_str());// wide string
		wstring tmplocal = getOriginString(cmdOriginator);
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,L"%I64d.%hd Send   Cmd %d  Trans %d %s\n",
										 pWrkMsgCycle->sendTimeBuffer.time,/* int64 */
										 pWrkMsgCycle->sendTimeBuffer.millitm,/*unsigned short*/
										 pCmd->getCmdNumber(), transNum, tmplocal.c_str());
#else
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Send   Cmd %d  Trans %d\n",
										 pWrkMsgCycle->sendTimeBuffer.time,
										 pWrkMsgCycle->sendTimeBuffer.millitm,
										 pCmd->getCmdNumber(), transNum);
#endif
#else
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Send   Cmd %d  Trans %d\n",
										 pWrkMsgCycle->sendTimeBuffer,
										 pWrkMsgCycle->sendMSBuffer,
										 pCmd->getCmdNumber(), transNum);
#endif
	}
	/* stevev 2/19/04 - wait to mark pending till we get here */
	//stevev 2/26/04::: rc = pCmd->markReplyPending(pWrkMsgCycle->transNum);
	rc = pCmd->markReplyPending(pWrkMsgCycle->transVarList);//stevev 2/26/04-do not re-resolve
	/* end stevev 2/19/04*/

	if (pWrkMsgCycle->isBlking == bt_ISBLOCKING)
	{
		rc = pCommInterface->SendPriorityPkt(pXmitPkt, 0,sendInfo);// takes ownership of pkt
	}
	/* Added on 160404: Though the packet is a Non Blcking one, but still send
		Priority packet to do the WriteIMd fast */
	else if(bIsWriteImd)
	{
		rc = pCommInterface->SendPriorityPkt(pXmitPkt, 0,sendInfo);// takes ownership of pkt
	}
	else
	{
		rc = pCommInterface->SendPkt(pXmitPkt, 0,sendInfo);// takes ownership of packet
	}
	//If MsgCycle.isBlocking  < note that this is done while holding the message mutex >
	if (rc == SUCCESS && pWrkMsgCycle->isBlking == bt_ISBLOCKING && 
																pWrkMsgCycle->peDone != NULL )
	{//	Pend on MsgCycle.CompleteEvent < !!!!!!!! handle timeout ????????????>
		pLocalEvtCntl = (pWrkMsgCycle->peDone)->getEventControl();	// will need to be deleted
		/* VMKP Modified on 270104 
		(In wait for more time to get the response packet) */
		rc = pLocalEvtCntl->waitOnEvent(getMessageTime);// blocking call MUST block
		/* End of VMKP Modification 270104 */
#ifdef _DEBUG
LOGIT(CLOG_LOG,"    Blocking Send got the done event.\n");
#endif
		//Handle errors as required
		if ( rc != SUCCESS )
		{// APP_WAIT_ERROR || WAIT_TIMEOUT
#ifndef _WIN32_WCE			// PAW 24/04/09
			_ftime( &(pWrkMsgCycle->recvTimeBuffer) );
#else
			SYSTEMTIME	st;
			GetSystemTime(&st);
			SetTz(&st);
			pWrkMsgCycle->recvMSBuffer = st.wMilliseconds;
//			start_ce = time_ce(&(pWrkMsgCycle->recvTimeBuffer));	// timedebugging replace this
#endif


#ifndef _WIN32_WCE			// PAW 24/04/09
  #ifndef  _USE_32BIT_TIME_T /* timeb.h...#define _timeb      __timeb64 */
			LOGIT(CLOG_LOG,"(%d) %I64d.%hd Pend Failure Cmd %d\n",hCntMutex.getEntryCount(),
							pWrkMsgCycle->recvTimeBuffer.time,/* int64 */
							pWrkMsgCycle->recvTimeBuffer.millitm,/*unsigned short*/
							pWrkMsgCycle->pCmd->getCmdNumber());
  #else
			LOGIT(CLOG_LOG,"(%d) %d.%d Pend Failure Cmd %d\n",hCntMutex.getEntryCount(),
							pWrkMsgCycle->recvTimeBuffer.time,
							pWrkMsgCycle->recvTimeBuffer.millitm,
							pWrkMsgCycle->pCmd->getCmdNumber());
  #endif
#else
			LOGIT(CLOG_LOG,"(%d) %d.%d Pend Failure Cmd %d\n",hCntMutex.getEntryCount(),
							pWrkMsgCycle->recvTimeBuffer,
							pWrkMsgCycle->recvMSBuffer,
							pWrkMsgCycle->pCmd->getCmdNumber());
#endif
			// Once sent, the msg cycle must stay in the queue
			
			// deleteing the event will make the receive task return the msgCycle
			pWrkMsgCycle->peDone = NULL;// blocking with no event
			msgRet = -1; // flag future activity - we don't want to return the msgcycle here
			pWrkMsgCycle->pCmd->invalidateReply(pWrkMsgCycle->transNum);
		}
		// else all is well, continue

		delete pLocalEvtCntl; pLocalEvtCntl=NULL;
		hostCommStatus = pWrkMsgCycle->hostCommStatus;
	}//endif 
#ifdef _DEBUG
	else
LOGIT(CLOG_LOG,"    Non-Blocking Send just keeps going.\n");
#endif
	//put Comm-Mutex   (very short hold time in the non-Blocking scenario)	
    if(nullptr != pCommMutex)
	pCommMutex->relinquishMutex();// **********************************************************

// stevev 8feb05 - moved from end of function to here on a trail basis
	exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);

	//If  MsgCycle.Post Action List is not empty	<empty at actions disabled> 
	//												< must be done AFTER mutex relinquished >
	if (rc == SUCCESS && msgRet == 0 && pWrkMsgCycle->isBlking == bt_ISBLOCKING  &&
		pWrkMsgCycle->actionsEn &&  pWrkMsgCycle->postActions.size() > 0)
	{	//device->ExecuteMethod(itemID) for each in the list (as determined by previous success)
		//													 <possible spec interpretation>
		hCmethodCall wrkMthCall;
		vector<hCmethodCall> postActionCallList;
		UINT32       methID = 0;

		
		/*
		FOR_iT(HreferenceList_t,pWrkMsgCycle->postActions) 
		{// iT isa ptr 2a hCreference...should be a method here
			rc = iT->resolveID( methID );
			
			if (rc==SUCCESS && methID > 0)
			{
				wrkMthCall.methodID = methID;// command actions don't use the symbol ptr first param
				wrkMthCall.source   = msrc_ACTION;
				postActionCallList.push_back(wrkMthCall);
				wrkMthCall.clear();
			}
			else
			{
				devPtr()->systemLog(CERR_LOG,"ERROR:command post action resolution returned %d",rc);
			}
		}
		rc = devPtr()->executeMethod(postActionCallList, methRetValue);
		*/
		if ( msgRet >=0 )// not a pend failure
			rc = devPtr()->executeMethod(pWrkMsgCycle->postActions);
		//TODO : handle the return code and take the action apropriately
		//Anil 140906 Commented the below code
		//rc = devPtr()->executeMethod(pWrkMsgCycle->postActions, methRetValue);
		// TODO: handle return values here	


		//1 stevev 24feb2015 - I can find no place in serviceReceivePacket that starts an action packet
		//1						without also ending it.  I am removing this till proven it is needed.
		//1 It gives an extra END_actPkt on normal post read actions.
		//1
		//1LOGIF(LOGP_NOTIFY)(CLOG_LOG,"Send function is doing END_actPkt.\n");
		//1devPtr()->notifyVarUpdate(0,  END_actPkt);// assume that the servicepacket started it
	}//endif
	//If held completeEventID is not empty (one was passed in) && 
	//	 held completeEventID != MsgCycle.CompleteEvent (not already sent)
	if (holdEvent != NULL && holdEvent != pWrkMsgCycle->peDone)
	{//	Send held (entry) completeEventID 
		pLocalEvtCntl = holdEvent->getEventControl(); // will need to be deleted
		pLocalEvtCntl->triggerEvent();
		delete pLocalEvtCntl; pLocalEvtCntl=NULL;
	}// else - no action
	// the only way holdEvent could be NOTNULL and already sent would be for
	//   the caller to have passed in an event without the blocking flag set(debug error logged)
	
	/* Removed on 160404: Not required */
#if 0	
	/*Vibhor 250304: Start of Code*/
	if(NULL != pMsgCycle  && msgRet >= 0)
	{ //  the calling routine wants the msgCycle, pass it
		// stevev - modified to a standard return	pMsgCycle->respCdVal = pWrkMsgCycle->respCdVal;
		*pMsgCycle = *pWrkMsgCycle;
	}
/*Vibhor 250304: End of Code*/
#endif

	// our responsibility AND no error removing msgcycle on abort
	if (pWrkMsgCycle->isBlking == bt_ISBLOCKING  && msgRet >= 0)
	{// our responsibility to dispose of the msg cycle
#ifdef QDBG
		clog <<"("<<hCntMutex.getEntryCount() 
			 <<")  (blking) returning MT Msg Cycle "<<hex << pWrkMsgCycle << dec << endl;
#endif
		theMsgQueue.ReturnMTObject(pWrkMsgCycle);
	}// else - in a non blocking scenario or we dont have msgcycle, the recvPkt handler will dispose of it
	if ( rc != SUCCESS )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: SendPkt returned failure code: 0x%02x\n", rc);
	}
	// replace - stevev:: decEntry();
	// replace - stevev:: hCntMutex.putCountingMutex(); // normal exit
//stevev 8feb05 - try moving the following to occur before the post actions
// all of variables used after the relinquish ar local/on-the-stack and should not 
// have a threading issue
/////moved to occur earlier 	exit_Send(pCmd->getCmdNumber(), isBlking == bt_ISBLOCKING);
	// Anil Merger Activity From FDM to HCF November 2005 -Srart
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Send inside catch.\n");
		if ( haveIdxMutex )
		{
			if ( indexMutexCount == 1 )
			{	// stevev 17sep09 - move outside...indexMutexCount = 0;
				pIndexMutex->relinquishMutex();//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
			}
			indexMutexCount--;
		}
	}
	// Anil Merger Activity From FDM to HCF November 2005-End

	return rc; // normally SUCCESS when the SendPkt was successful
}// end of send()

/* stevev 4/23/04 - now not inline */
/* returns SUCCESS or, on rentry error - APP_DD_ERROR	*/
RETURNCODE hCcmdDispatcher::
enterSend(int cmdNum, bool isBlking)
{
	RETURNCODE rc = SUCCESS;
	if (pCommMutex == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: CmdDispatch missing critical resource.\n");
		return APP_CONSTRUCT_ERR;
	}// \/ allows same-thread re-entry
	if ( (rc = hCntMutex.getCountingMutex()) != SUCCESS ) // wait forever - we can't go on without it
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: Send Entry for cmd# %d Failed wo timeout\n",cmdNum);
		return rc;// added stevev 13apr10 to percolate error upwards
	}
	/* pre-1.8 patch:: 
	hCntMutex.getCountingMutex(); // wait forever - we can't go on without it
	*/
	int ecnt = hCntMutex.getEntryCount();

	if ( isBlking )
	{//    re-entered  & there is a blocking call in our past
		if ( ecnt > 1 && vBlockingCmdStack.size() > 0 )// this is a blocking reentry
		{
			for (vector<int> :: iterator iP = vBlockingCmdStack.begin(); 
				 iP < vBlockingCmdStack.end();   iP ++)
			{
				if ( *iP == cmdNum )
				{
				 //duplicate entry
					LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Reentry to the same command (# %d)\n",cmdNum);					
					hCntMutex.putCountingMutex();
					return APP_DD_ERROR;
				}// else keep lookinh
			}//next
			// to get here then this blocking command is a different number then entered before
		}
		// else, this is first entry for this cmd# OR has aprevious entry by a non blocking call
		vBlockingCmdStack.push_back(cmdNum);
#ifdef QDBG		
		LOGIT(CLOG_LOG,"Send Entry(Blocking) with counting mutex pushed cmd %d."
			" (cnt:%d, stk:%d)\n",	cmdNum,ecnt,vBlockingCmdStack.size());
#endif
	}
	// else // not blocking - return success
#ifdef QDBG
	else
	{
		LOGIT(CLOG_LOG,"Send Entry with counting mutex.\n");
	}
#endif
	return rc;
}

RETURNCODE hCcmdDispatcher::
exit_Send(int cmdNum, bool isBlking)
{
	RETURNCODE rc = SUCCESS;

	int ecnt = hCntMutex.getEntryCount();

	if ( isBlking )
	{
#ifdef QDBG
		LOGIT(CLOG_LOG,"Send Exit  (Blocking) with counting mutex.(cnt:%d, stk:%d",
			ecnt,vBlockingCmdStack.size());
#endif
		for (vector<int> :: iterator iP = vBlockingCmdStack.begin(); 
			 iP < vBlockingCmdStack.end();   iP++)
		{
			if ( *iP == cmdNum )
			{
				vBlockingCmdStack.erase(iP);// erase self
				break;
			}// else keep looking
		}
#ifdef QDBG
		LOGIT(CLOG_LOG," -> stk:%d)\n",vBlockingCmdStack.size());
#endif
	}
	// else // not blocking - return success
#ifdef QDBG
	else
	{
		LOGIT(CLOG_LOG,"Send Exit  with counting mutex.\n");
	}
#endif

	hCntMutex.putCountingMutex();

	return rc;
}
/* end stevev 4/23/04 */

/* stevev - replace with counting mutex 
   -- the blocking mutex makes this entire function obsolete
//  non-blocking send -- Async Rd/Wr MUST enter here
RETURNCODE hCcmdDispatcher::
SvcSend(hCcommand* pCmd, long transNum, indexUseList_t& useIdx)//true in svc
{
 	if ( pCriticalSect == NULL || pAsyncEnEvent == NULL || pIndexMutex == NULL)
	if ( pAsyncEnEvent == NULL || pIndexMutex == NULL)
	if (pIndexMutex == NULL)
	{// resource error
		cerrxx << "ERROR: CmdDispatch.SvcSend: did not construct a mutex."<<endl;
		return APP_CONSTRUCT_ERR;
	}

	bool haveSend = false;// tell us when to release it
	RETURNCODE rc =  pCriticalSect->aquireMutex(getLongMutexTimeOut);

	if ( rc != SUCCESS )
	{ 
		cerrxx << "ERROR:  CmdDispatch.SvcSend aquire critical mutex failed."<<endl;
		return rc;
	}

	if (entryCnt) // somebody is in the send routine
	{
		asyncWaiting = true;
		pCriticalSect->relinquishMutex();
		// syncronize with the send method...we are allowed to run now
		// VMKP commented out the below line on 190104 as per steve mail on 510104 
		// stevev jan 24, 04 - The mail 150104 said nothing about commenting out
		//   that I saw - This PAR (5181) will be taken up immediatly to make permanent fixes
		//   this has been uncommented - works fine on straight SDC,
		//      we may need to make a conditional compilation of this till 5181 is complete 
		rc = pAsyncEnEvent->pendOnEvent(getMessageTime + getLongMutexTimeOut);
		// VMKP commented on 190104 as per steve mail on 510104 
		
		if ( rc != SUCCESS )
		{ 
			cerrxx << "ERROR:  CmdDispatch.SvcSend aquire async Event failed."<<endl;
			return rc;
		}
		//else
		asyncWaiting = false;
		haveSend     = true;
	}
	else
	{		
		pCriticalSect->relinquishMutex();
	}

	struct _timeb timebuffer;
	_ftime( &timebuffer );
	LOGIT(CLOG_LOG,"%d.%03d SvcSendCmd %d\n",timebuffer.time,timebuffer.millitm,
										   pCmd->getCmdNumber());
#ifdef _DEBUG
if (pCmd->getCmdNumber()==187)
{
	clog<<".";
}
#endif
	rc = Send(pCmd, transNum, true, bt_NOTBLOCKING, NULL, useIdx);// never needs a completion!!

	// may return a shutdown in progress error code
	return rc;
}
  end stevev replace remove */

//  decide next queue service cmd-used below
RETURNCODE hCcmdDispatcher::
getNextQueueTransaction(hCcommand* pReturnedCmd, long& returnedTransNum)
{
	RETURNCODE rc = FAILURE;
	return rc;
}


bool hCcmdDispatcher::ServiceWrites(void) // uses SvcSend - return true if a write was processed    
{
	RETURNCODE rc = SUCCESS;
	bool retVal = true;
	hCcommand* pCmd = NULL;
	int        transNum = -1;// default to seach 'em all
	indexUseList_t localUseIdx;
#ifdef _USE_VAR_OPT
	cmdDescList_t  needyList;
	cmdDescList_t *pNeedy = NULL;
#endif
#ifdef _DEBUG
	int mtime;
#ifndef _WIN32_WCE			// PAW 24/04/09
	struct _timeb alltime, allend, timebuffer, endbuffer;
	_ftime( &alltime );
#else
	time_t_ce timebuffer;
	time_t_ce timeMSbuffer;
	time_t_ce endbuffer;
	time_t_ce endMSbuffer;
	time_t_ce start_ce;	
#endif
#endif

	if (pCmdList == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No command list to service.\n");
		retVal = false;
	}
	else
	if (! appCommEnabled())
	{
		LOGIT(CLOG_LOG,"WARNING: servicing writes with no appcomm.\n");
		retVal = false;
	}
	else
	{
		do
		{	// scan for the best command to send to service writes
#ifdef _DEBUG
#ifndef _WIN32_WCE		// PAW 21/04/09
			_ftime( &timebuffer );
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	timeMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&timebuffer);	// time
#endif

#endif
#ifdef _USE_VAR_OPT
	if ( pVarList )
	{
		pVarList->getNeededWrit(needyList);
		if (needyList.size())
		{
			pNeedy = &needyList;
		}
		else
		{
			pNeedy = NULL; // do it the original way
		}
	}
	else
	{
		pNeedy = NULL; // do it the original way
	}
#else
	pNeedy = NULL; // do it the original way
#endif
			// if index protection is found to be needed here - put it in calcWeight()
			rc = pCmdList->getBestWrite(pCmd, transNum, localUseIdx, pNeedy);

			if ( rc != SUCCESS || pCmd == NULL ||
				! appCommEnabled())// added a4aug06 try and stop shutdown crash
			{//		if No command needs sending 
				retVal = false;// nothing needs work
				CVarList* pVarList = (CVarList*) devPtr()->getListPtr(iT_Variable);//<hCcommand*>
				// ======================== J.U. =======================================
#ifdef _INVENSYS		/* stevev 12aug11 - appears to be UI specific */
				if (pVarList != NULL)
				{
					vector<hCVar*>::iterator iT;

					for (iT = pVarList->begin(); iT != pVarList->end(); iT++)
					{// ptr2aPtr2a hCVar
						if ( (*iT)->getDataState() == IDS_NEEDS_WRITE)
						{
							LOGIT(CERR_LOG|UI_LOG,"Write command for 0x%x variable was not found.\n", (*iT)->getID());
							notifyVarUpdate((*iT)->getID(), STAT_changed, IDS_INVALID );
							//(*iT)->markItemState(IDS_UNINITIALIZED);
						}
					}
				}
#endif
				if ( pVarList != NULL && pVarList->isChanged())
				{
// this happens too much
// skip for now  LOGIT(CERR_LOG|UI_LOG,"Write command for a changed variable was not found.\n");
				}
				// ===========================================================
			}
			else
			{//	call SvcSend//cmd*/trans# the  non-blocking send -- Async Rd/Wr MUST enter here
			 // stevev - replace with send's counting mutex::
			 // rc = SvcSend(pCmd, transNum, localUseIdx);
				rc = Send(pCmd, transNum, true, bt_NOTBLOCKING, NULL, localUseIdx
						,false,co_ASYNC_WRITE);// stevev 25may07 - added origination
				if ( rc != SUCCESS )
				{
					retVal = false;// nothing needs work//break out of this loop
				}
				else
				if (ourPolicy.incrementalWrite != 0 )// incremental desired
				{// return after one
#ifdef _DEBUG	
#ifndef _WIN32_WCE		// PAW 24/04/09
	_ftime( &endbuffer );	

	mtime = endbuffer.millitm - timebuffer.millitm;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer.time -= 1;
	}
	LOGIT(CLOG_LOG," Service One Write Send time = %d.%03d\n",(endbuffer.time - timebuffer.time),mtime);
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	endMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&endbuffer);	// time

	mtime = endMSbuffer - timeMSbuffer;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer -= 1;
	}
	LOGIT(CLOG_LOG," Service One Write Send time = %d.%03d\n",(endbuffer - timebuffer),mtime);
#endif
#endif
					return true;
				}
				else // non- incremental...do 'em all till we drop
				{//	loop to scan for best write command
				}
			}
			localUseIdx.clear();
		}while(retVal);// loop till no more need work
	}
#ifdef _DEBUG	
	_ftime( &allend);	

	mtime = allend.millitm - alltime.millitm;
	if (mtime < 0) 
	{	mtime+=1000;allend.time -= 1;
	}
	LOGIT(CLOG_LOG," Service All Write Send time = %I64d.%03d\n",
														(allend.time - alltime.time),mtime);
#endif
	return retVal;
}

bool hCcmdDispatcher::ServiceReads(void)     // uses SvcSend - return true if a read was processed 
{
// Anil Merger Activity From FDM to HCF November 2005 -start
try
{
// Anil Merger Activity From FDM to HCF November 2005 -End
	RETURNCODE rc = SUCCESS;
	hCcommand* pCmd;
	bool retVal = true;
	int        transNum = -1;// default to seach 'em all
	indexUseList_t localUseIdx;
#ifdef _USE_VAR_OPT
	cmdDescList_t  needyList;
	cmdDescList_t *pNeedy = NULL;
#endif


#ifdef _DEBUG
	int mtime;
#ifndef _WIN32_WCE	// PAW 24/04/09
	struct _timeb alltime, allend, timebuffer, endbuffer;
	_ftime( &alltime );
#else
	time_t_ce timebuffer;
	time_t_ce timeMSbuffer;
	time_t_ce endbuffer;
	time_t_ce endMSbuffer;
	time_t_ce start_ce;	
#endif
#endif
	if (pCmdList == NULL )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No command list to service.\n");
		retVal = false;
	}
	else
	if (! appCommEnabled())
	{
		LOGIT(CLOG_LOG,"WARNING: servicing reads with no appcomm.\n");
		retVal = false;
	}
	else
	{
		do
		{	// scan for the best command to send to service reads
#ifdef _DEBUG
#ifndef _WIN32_WCE		// PAW 24/04/09
			_ftime( &timebuffer );
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	timeMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&timebuffer);	// time
#endif
#endif
#ifdef _USE_VAR_OPT
	if ( pVarList )
	{
		pVarList->getNeededRead(needyList);
		if (needyList.size())
		{
			pNeedy = &needyList;
		}
		else
		{
			pNeedy = NULL; // do it the original way
		}
	}
	else
	{
		pNeedy = NULL; // do it the original way
	}
#ifdef _DEBUG	
  #ifndef _WIN32_WCE		// PAW 24/04/09
	_ftime( &endbuffer );	

	mtime = endbuffer.millitm - timebuffer.millitm;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer.time -= 1;
	}
	LOGIT(CLOG_LOG," Service %d needy; elapsed time = %I64d.%03d\n",
								needyList.size(), (endbuffer.time - timebuffer.time),mtime);

  #endif
#endif
#else
	pNeedy = NULL; // do it the original way
#endif
			// if index protection is found to be needed here - put it in calcWeight()
			rc = pCmdList->getBestRead(pCmd, transNum, localUseIdx, pNeedy);

			if ( rc != SUCCESS  || pCmd == NULL ||
				! appCommEnabled())// added a4aug06 try and stop shutdown crash
			{//		if No command needs sending 
				retVal = false;// nothing needs work
				// set our status
				clrCond(dc_Reads);
			}
			else
			{//	call SvcSend//cmd*/trans# the  non-blocking send -- Async Rd/Wr MUST enter here
				setCond(dc_Reads);
			 // stevev - replace with send's counting mutex::
			 // rc = SvcSend(pCmd, transNum,localUseIdx);
				rc = Send(pCmd, transNum, true, bt_NOTBLOCKING, NULL, localUseIdx
					,false,co_ASYNC_READ);//stevev 25may07 - added origination
#ifdef _DEBUG	
#ifndef _WIN32_WCE		// PAW 24/04/09
	_ftime( &endbuffer );	

	mtime = endbuffer.millitm - timebuffer.millitm;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer.time -= 1;
	}
	LOGIT(CLOG_LOG," Service One Read Send time = %I64d.%03d\n",
													(endbuffer.time - timebuffer.time),mtime);
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	endMSbuffer = st.wMilliseconds;
	start_ce = time_ce(&endbuffer);	// time

	mtime = endMSbuffer - timeMSbuffer;
	if (mtime < 0) 
	{	mtime+=1000;endbuffer -= 1;
	}
	LOGIT(CLOG_LOG," Service One Read Send time = %d.%03d\n",(endbuffer - timebuffer),mtime);
#endif
#endif
				if ( rc != SUCCESS )
				{
					retVal = false; // break out of the loop
				}
				else
				if (ourPolicy.incrementalRead != 0 )// incremental desired
				{// return after one
					return true;
				}
				else // non- incremental...do 'em all till we drop
				{//	loop to scan for best write command
				//	CVarList* pVarList = (CVarList*) devPtr()->getListPtr(iT_Variable);
				//	if ( pVarList != NULL && pVarList->isChanged())
					if ( pVarList != NULL && pVarList->isChanged())
					{
						ServiceWrites();
					}
				}// then check for reads again
			}
			localUseIdx.clear();
		}while(retVal);// loop till no more need work
	}
#ifdef _DEBUG	
	_ftime( &allend );	

	mtime = allend.millitm - alltime.millitm;
	if (mtime < 0) 
	{	mtime+=1000;allend.time -= 1;
	}
	LOGIT(CLOG_LOG," Service All Read Send time = %I64d.%03d\n",
														(allend.time - alltime.time),mtime);
#endif
	return retVal;
}
// Anil Merger Activity From FDM to HCF November 2005 -start
catch(...)
{
	LOGIT(CERR_LOG,"CommandDispatcher: ServiceReads: inside catch(...)\n");
}
return 0;// post catch return
// Anil Merger Activity From FDM to HCF November 2005 -End
}


/*      03/02/04 - stevev - cleaned up this method! - rearranged execution to prevent task error*/
// **** this may run under a separate task ***
// the receive function that comm interface calls when it gets a packet
// expect this to run under another task - caller's responsibility to handle data protection
/*virtual */		
void hCcmdDispatcher::serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo)
{// handle the packet as required (pkt ptr belongs to caller & will be destroyed
		//----- in the comm interface
		//	Pends on packet, timeout 
		//	If timeout - send timeout message/event
		//	Else (packet)
		//		get packet from hdwr-Ifc
		//----- this method ( parse command# & status )

	RETURNCODE rc   = SUCCESS;
	hMsgCycle_t*    pMsgCyc = NULL;
	hCtransaction*  pTrans  = NULL;
	indexUseList_t localIndexList;	// used to save index values
	methodCallList_t localMethList; // used for handling enum actions (only executed at bit change)
	varPtrList_t    varChangedList; // from the variable extract, we have to do a notify on each one
	hCmsgList        msgs;
	int              cmd = -1;

	HANDLE myThread = 0;
	int entrypriority = 0;
	
	varPtrLstIT_t lvpIT; 

#ifdef _WIN32_WCE
	time_t_ce start_ce;	// PAW win CE time 24/04/09		
#endif
	
	string tS;/* stevev - new 275 - see PAR 5492 - we need a working string */

	if ( pPkt == NULL || pCmdInfo == NULL ) 
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No access to the device from serviceReceivePacket.\n");
		rc = APP_PARAMETER_ERR;
	}
	else
	{	// check for shutdown scenario (clrNexit flag set)
		if (pCmdInfo->clrNexit)
		{// flush the message queue, triggering all done events - return from method!
			while (theMsgQueue.getMsgByPkt(pPkt, &pMsgCyc,4) == SUCCESS)
			{
				if ( pMsgCyc != NULL )
				{
					if ( pMsgCyc->peDone != NULL )
					{
						hCevent::hCeventCntrl* pEC = pMsgCyc->peDone->getEventControl();
						if ( pEC != NULL )
						{
							pEC->triggerEvent();// trigger 'em to prevent pend forever
							delete pEC;
						}// else skip it
					}
					// else invalid msg cycle
					if ( (pMsgCyc->isBlking == bt_NOTBLOCKING) ||// not blocking,we must return it
						 (pMsgCyc->peDone == NULL)   ) // or the blocking aborted
					{// return pMsgCyc to queue
						theMsgQueue.ReturnMTObject( pMsgCyc );
					}
					// else, in a blocking scenario, the owner (Send()) will dispose of it
				}// else the msgcycle is null - an error we'll skip
			}// wend next message packet

#ifdef _DEBUG
			if (theMsgQueue.GetQueueState() > 0 )
			{
				LOGIT(CLOG_LOG,
				    "> serviceReceivePacket w/clrNexit with messages still in the Q.(13++)\n");
				if ( theMsgQueue.FlushQueue() )
				{
					LOGIT(CLOG_LOG,
					"> serviceReceivePacket w/clrNexit Flush failed.               (13+++)\n");
				}
			}
#endif

			dispatchEnabled = false;// shake the hand

#ifdef _DEBUG
			LOGIT(CLOG_LOG,
				"> serviceReceivePacket w/clrNexit and dispatchEnabled false;returning.(14)\n");
#endif

			return; // that's all we want to do
		}
		// else - normal entry - just continue


		if ( lastCmpMode != compatability() )
		{
			lastCmpMode = compatability() ;
			if      (lastCmpMode == dm_Standard ) tS = "Standard";
			else if (lastCmpMode == dm_275compatible) tS = "Compatible";
			else tS = "Unknown";
			LOGIT(CLOG_LOG|CERR_LOG,
				"++++++++++++++ Compatability Mode Changed to %s ++++++++++++++++\n",tS.c_str());
		}

//		hCitemBase* pItm = NULL;
//		// fill special symbol for testing (if it exists in the DD)
//		rc = devPtr()->getItemBySymNumber(1022,&pItm);
//		if ( rc == SUCCESS && pItm != NULL )
//		{// store previous
//			if ( pItm->IsVariable() )
//			{
		if ( pByteCntVar != NULL )
		{
			CValueVarient lV; lV = pPkt->dataCnt;
			pByteCntVar->setDispValue(lV);
		}
//			}
//		}

//		dequeue the MsgCycle     <if it ain't there - discard packet, log an error & loop to pend>
		// deal with burst mode here - no transaction number so no msgcycle //
		if ( (pPkt->transactionID == 0) || /*   its a burst mode message                 OR */
			(theMsgQueue.getMsgByPkt(pPkt, &pMsgCyc, 5) == SUCCESS && pMsgCyc != NULL) )// a cycled msg
		{
			int rep =  0;	
#ifndef _WIN32_WCE		// PAW 24/04/09
			hCcommand* pCommand = NULL; // to preserve the pointer till needed stevev - 24sep09
			if (pPkt->transactionID == 0 && pMsgCyc == NULL)// burst mode packet
			{
				pMsgCyc = theMsgQueue.GetEmptyObject();
				if (pMsgCyc == NULL)
				{// no memory...we're done
					return;
				}
				else
				{
					CCmdList* pCmdLst = (CCmdList*)devPtr()->getListPtr(iT_Command);
					if ( pCmdLst == NULL )
					{
						return;
					}
					pMsgCyc->pCmd = pCommand = pCmdLst->getCmdByNumber(pPkt->cmdNum);
					pMsgCyc->isBlking = bt_ISBLOCKING;
				}
			}
			pMsgCyc->recvTime = time(NULL);	

			_ftime( &(pMsgCyc->recvTimeBuffer) );
			if (pMsgCyc->pCmd)
			{			
				cmd = pMsgCyc->pCmd->getCmdNumber();// for debugging only
				pCommand = pMsgCyc->pCmd;
				if (pPkt->transactionID == 0)
				{
					pTrans   = pCommand->getTransactionByNumber(pCommand->getTransNumbByReplyPkt(pPkt));
				}
				else
				{
					pTrans   = pCommand->getTransactionByNumber(pMsgCyc->transNum);
				}
				// stevev 06aug10 moved down to record transaction
				
  #ifndef  _USE_32BIT_TIME_T /* timeb.h...#define _timeb      __timeb64 */
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%I64d.%hd Recv   Cmd %d  Trans %d  (msgcycleTrans %d)\n",
							pMsgCyc->recvTimeBuffer.time,pMsgCyc->recvTimeBuffer.millitm,
							pMsgCyc->pCmd->getCmdNumber(),pTrans->getTransNum(),pMsgCyc->transNum );
  #else
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Recv   Cmd %d  Trans %d  (msgcycleTrans %d)\n",
							pMsgCyc->recvTimeBuffer.time,pMsgCyc->recvTimeBuffer.millitm,
							pMsgCyc->pCmd->getCmdNumber(),pTrans->getTransNum(),pMsgCyc->transNum );
  #endif
			}
			else
			{			
  #ifndef  _USE_32BIT_TIME_T /* timeb.h...#define _timeb      __timeb64 */
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%I64d.%hd Recv   Cmd **** NULL POINTER ****\n",
						pMsgCyc->recvTimeBuffer.time,pMsgCyc->recvTimeBuffer.millitm);
  #else
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Recv   Cmd **** NULL POINTER ****\n",
						pMsgCyc->recvTimeBuffer.time,pMsgCyc->recvTimeBuffer.millitm);
  #endif
				pTrans = NULL;
			}
#else	// _WIN32_WCE is defined
 #ifdef USED	// think these are causing a memory corruption PAW 14/08/09
	pMsgCyc->recvTime = time_ce(NULL);			
 #endif
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	pMsgCyc->recvMSBuffer = st.wMilliseconds;
//	start_ce = time_ce(&(pMsgCyc->recvTimeBuffer));	// time debugging replace this
	hCcommand* pCommand = NULL; // to preserve the pointer till needed stevev - 24sep09
			
	if (pMsgCyc->pCmd)
	{
		pCommand = pMsgCyc->pCmd;
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Recv   Cmd %d\n",pMsgCyc->recvTimeBuffer,
					pMsgCyc->recvTimeBuffer,pMsgCyc->pCmd->getCmdNumber());
		if (pPkt->transactionID == 0)
		{
			pTrans   = pCommand->getTransactionByNumber(pCommand->getTransNumbByReplyPkt(pPkt));
		}
		else
		{
			pTrans   = pCommand->getTransactionByNumber(pMsgCyc->transNum);
		}
	}
	else
	{
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Recv   Cmd **** NULL POINTER ****\n",
				pMsgCyc->recvTimeBuffer,pMsgCyc->recvMSBuffer);
		pTrans = NULL;
	}	
#endif				
			if ( pTrans == NULL)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:The receive packet handler could not get the transaction.");
			}
			else
			{
				pMsgCyc->hostCommStatus = 0;	// default to all-is-well
				// with newer comm drivers you would place your getCommStatus call here
				if (pPkt->dataCnt == 0xFF )// when response ends in the middle of a packet, we get datacnt FF but 00 success...&& pPkt->timeout != 0 )
				{// the no-response scenario
					rep = -1; // skip the async later
					pMsgCyc->hostCommStatus = HOST_NORESPONSE;
//don't do this now	if (pCmdInfo->pOtherData) delete pCmdInfo->pOtherData;
					pCmdInfo->pOtherData = (void*) new hMsgCycle_t(*pMsgCyc);
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Data Count 0xFF no response flag------\n");
				}
/* this code has to be inside the index setting mechanism so we invalidate the correct items
				else  //we rc & ds but we expected more 
				if (pPkt->theData[0] != 0 && pPkt->dataCnt == 2 && pTrans->responseSize() > 2 )
				{// command error scenario (NonZero RC, RC & DS only, expected data)
					pTrans->invalidateReply();
					rep =  pTrans->extractReplyPkt(pPkt, localMethList, msgs);// rc & ds hCmsgList  msgs
				}
 *  moved inside the index mutex 17jun09 *****************************************************/
				else
				if (! (pCmdInfo->discardMsg ) )
				{// don't allow anybody else to run 
/*+++ critical section Entry ++++++*/
					receiveCS.EnterCS();

					if ( pMsgCyc->indexList.size() > 0 )
					{/* cache the index vars ( we may have interuppted someone using 'em ) */
						hIndexUse_t localIdxUse;
						hCitemBase* pItemWrk = NULL;
						hCinteger* pIntWrk;
						CValueVarient localWrkVar;

/******INDEX MUTEX*** - not used 8sep09 -  pIndexMutex->aquireMutex();*/// waits forever
//#ifdef LOG_INDEX_MUTEX
//		LOGIT(CLOG_LOG,"Got Index Mutex: Dispatch Line %d\n",__LINE__);
//#endif
						for (indexUseList_t::iterator itIUL = pMsgCyc->indexList.begin();
							                   itIUL < pMsgCyc->indexList.end();         itIUL++)
						{// itIUL isa ptr2a hIndexUse_t
							rc = devPtr()->getItemBySymNumber(itIUL->indexSymID,&pItemWrk);
							if ( rc == SUCCESS && pItemWrk != NULL )
							{// store previous
								localIdxUse.indexSymID = itIUL->indexSymID;
								pIntWrk     = (hCinteger*)pItemWrk;
								//localWrkVar = pIntWrk->getDispValue();
								localWrkVar = pIntWrk->getRealValue();
								localIdxUse.indexDispValue = localWrkVar;
								/* stevev - new 275 - see PAR 5492 */	
								/*NOTE: must set the device value so resolutions works */
								if (compatability() == dm_275compatible)// non-returned index value
								{
									LOGIT(CLOG_LOG,"    SvcRcv Setting index 0x%04x to %d\n",
										  localIdxUse.indexSymID,((int)localWrkVar));
									// wrong:: localWrkVar = itIUL->indexDispValue;
									//pIntWrk->setRawDispValue(localWrkVar);
									tS = int2str((int)(itIUL->indexDispValue));
									pIntWrk->setRealValueString(tS);

								}// end non-returned compatability
								localIndexList.push_back(localIdxUse);
								localIdxUse.clear();
							}// not much we can do without an item
							else
							{
								LOGIT(CERR_LOG,
							  "ERROR: Recv could not get item pointer for an index symbol 0x%04x",
										itIUL->indexSymID);
								if (rc != SUCCESS) 
								{
									LOGIT(CERR_LOG," (rc= 0x%x)\n",rc);
								}
								else // success with a null pointer
								{
									LOGIT(CERR_LOG," (item PointerNull)\n");
								}
							}
						}// next index in list
					}// end index handling

					/* the error packet handling was moved in here 17jun09 */
					if (pPkt->theData[0] != 0 && pPkt->dataCnt == 2 && pTrans->responseSize() > 2 )
					{// command error scenario (NonZero RC, RC & DS only, expected data)
						pTrans->invalidateReply();
						rep =  pTrans->extractReplyPkt(pPkt, localMethList, msgs, varChangedList);
					}
					else
					{
						/* stevev 2/26/04 - delay display update */
						if (pMsgCyc->actionsEn && pMsgCyc->postActions.size() > 0)
						{
							devPtr()->notifyVarUpdate(0,  STRT_actPkt);
						}
#if 0  // causes havoc in HART 6
/* VMKP added on 310304 */
/* Update Burst Mode and Burst Option data if present in the device, Do this Before extracting
	the packet */
/* stevev 28jan05 - NOTE: the following code will toggle the burst mode variable (state) in
                    the device object every other message (every burst mode msg).  this needs to
					be reviewed when we implement burst mode in the modem server */
/* this code was removed from the source 17jun09 to improve readability - see earlier for content*/
/* VMKP added on 310304 */
#endif  //#if 0  // causes havoc in HART 6

						/* branch elsewhere if a transfer */
						if (pPkt->cmdNum == 111 || pPkt->cmdNum == 112)
						{
							serviceTransferPacket(pPkt, pMsgCyc->pCmd, pMsgCyc->transNum);
							rep = 0;// out of do-while
						}

/* extract */	/*	rep = pTrans->extractReplyPkt(pPkt); - replaced as below stevev 7mar05 */
				/*  Wally - 2/22/05 - A write Command that does NOT return an error RC
						is assumed to be 'gospel'. The value returned should immediatly
						be transfered to the Display value. notify is executed on change   */
/* bad std dd 	if ( pMsgCyc->cmdType == cmdOpWrite )*/
						else
						if ( pMsgCyc->cmdType == cmdOpWrite || pMsgCyc->cmdType == cmdOpCmdCmd)
						{
							rep = pTrans->extractReplyWritePkt
							(pPkt, localMethList, msgs, varChangedList,&(pMsgCyc->indexList));
						}
						else
						{
							rep = pTrans->extractReplyPkt
							(pPkt, localMethList, msgs, varChangedList,&(pMsgCyc->indexList));
						}
						/* end replace 7mar05 - stevev */

						if ( rep < 0 )// error
						{
							LOGIT(CERR_LOG,
							"ERROR: (c:%d t:%d) transaction failed to parse reply packet.\n",
								pMsgCyc->pCmd->getCmdNumber(),pMsgCyc->transNum);
						}// else all is well

						if ( pMsgCyc->indexList.size() > 0 )
						{/* un-cache the index vars caches earlier */
					// unused 24nov14		hCitemBase* pItemWrk = NULL;
					// unused 24nov14		hCinteger* pIntWrk;
					// unused 24nov14		CValueVarient localWrkVar;
							//unused 27jan05   bool retChngd;
					// unused 24nov14		indexUseList_t tmpErrorList;
					// unused 24nov14		hIndexUse_t    tmpIdx;

/* moved to data item so we can detect the INFO index mismatches-------------------------------
-- 24nov14 - moved to include the whole loop since once the activity in the loop was removed, 
   the loop did nothing
							// detect change before restoration
							for (indexUseList_t::iterator itIxUL = pMsgCyc->indexList.begin();
												 itIxUL < pMsgCyc->indexList.end();   itIxUL++)
							{// itIxUL isa ptr2a hIndexUse_t
								rc =devPtr()->getItemBySymNumber(itIxUL->indexSymID,&pItemWrk);
								if ( rc == SUCCESS && pItemWrk != NULL )
								{
									pIntWrk     = (hCinteger*)pItemWrk;

									if (pIntWrk->IsLocal() == true)// filter log message
									{//          target value compared2          current value
										if ( (int)(itIxUL->indexDispValue) !=
											 (int) pIntWrk->getRealValue())
										{ 
														LOGIT(CLOG_LOG,
					"-x-x-x-x-x- Index Value changed in the extraction of Cmd %d. Trans %d, \n"
					"            Sym#:0x%04x  Was Sent:%d Device Now:%d, Display Now:%d\n",
												  pMsgCyc->pCmd->getCmdNumber(),
												  pMsgCyc->transNum,
												  itIxUL->indexSymID,
												  (int)(itIxUL->indexDispValue),
												  (int) pIntWrk->getRealValue(),
												  (int) pIntWrk->getDispValue(true)   );
	#ifdef _DEBUG
	if ( ! pIntWrk->IsLocal() )
	LOGIT(CLOG_LOG,"      Uncaching a NON-local index.'%s'\n",pIntWrk->getName().c_str());
	#endif
											// store it for changed processing
											tmpIdx.indexSymID = pIntWrk->getID();
										// Anil Merger Activity From FDM to HCF November 2005
											// added by stevev 14dec05
											tmpIdx.indexDispValue = itIxUL->indexDispValue;
											tmpErrorList.push_back(tmpIdx);
										}
										// else - didn't change, skip it
									}
									// else not local, it is allowed to change
								}
								// else we can't work without an item
							}// next msg cycle

							if (  (tmpErrorList.size() > 0       ) && 
								  (compatability() == dm_Standard)     )
							{// invalidate before replacing indexes
								pTrans->invalidateIndexedReply(tmpErrorList);
							}
	// 24nov14 - since the loop was emptied, the error list was always empty
--------------------- end of move ************************************************************/
						}// endif misc index processing

						// I don't know where this went but I'm putting it back here 24may11
						if (pMsgCyc->actionsEn && pMsgCyc->postActions.size() > 0)
						{
							devPtr()->notifyVarUpdate(0,  END_actPkt);
						}


					}// end else it's not an error packet

					if ( pMsgCyc->indexList.size() > 0 )
					{// now we can uncache them
						hCitemBase* pItemWrk = NULL;
						hCinteger* pIntWrk;

						for (indexUseList_t::iterator itIUL = localIndexList.begin();
							 itIUL < localIndexList.end();    itIUL++)
						{// itIUL isa ptr2a hIndexUse_t
							rc = devPtr()->getItemBySymNumber(itIUL->indexSymID,&pItemWrk);
							if ( rc == SUCCESS && pItemWrk != NULL )
							{
								pIntWrk     = (hCinteger*)pItemWrk;
								/* stevev  - new 275 27jan05
								retChngd = pIntWrk->SetIt(&(itIUL->indexDispValue));
						// 28jan05-NOT:Sets the Write Value (not the Real Val) returns isChanged
								pIntWrk->ApplyIt();// set the real value
								end remove 27jan05 - begin insert */
								tS = int2str((int)(itIUL->indexDispValue));
								pIntWrk->setRealValueString(tS);
								/* end 27jan05*/

							}
							else
							{
								LOGIT(CERR_LOG,
							   "ERROR: could not get item pointer to restore an index symbol.\n");
							}
						}// next
						localIndexList.clear();
/******INDEX MUTEX*** -- not used  8sep09 -pIndexMutex->relinquishMutex();*/
					}// else no indexes in list, don't process
/*+++ criical section exit ++++++*/
					receiveCS.Exit_CS();
				}
				else // leave rep 0 ( is 'discard message' )
				{
					LOGIT(CLOG_LOG,"    Discarding the message as per cmdInfo\n");
				}				
			}// endelse - we have a transaction
			
			// we got a message cycle, we may or may not have processed a transaction
#ifndef _WIN32_WCE			// PAW 24/04/09
			int mtime = pMsgCyc->recvTimeBuffer.millitm - pMsgCyc->sendTimeBuffer.millitm;
			if (mtime < 0) 
			{	mtime+=1000;pMsgCyc->recvTimeBuffer.time -= 1;;
			}
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," Transaction time = %d.%d\n",
					(pMsgCyc->recvTimeBuffer.time - pMsgCyc->sendTimeBuffer.time),mtime);

#else
			int mtime = pMsgCyc->recvMSBuffer - pMsgCyc->sendMSBuffer;
			if (mtime < 0) 
			{	mtime+=1000;pMsgCyc->recvTimeBuffer -= 1;;
			}
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG," Transaction time = %d.%d\n",
					(pMsgCyc->recvTimeBuffer - pMsgCyc->sendTimeBuffer),mtime);
#endif

			/* stevev 2/26/04 - moved the notify to occur post actions*/				
			/* stevev 3/02/04 - moved signal trigger to post actions  */

			blockingType_t tmpISblocking = pMsgCyc->isBlking;// hold so other task doesn't trash

			if (tmpISblocking == bt_NOTBLOCKING)// not blocking, check actions
			{// act as required
				// stevev 11oct05 - execute enum actions BEFORE post actions
				if (pMsgCyc->actionsEn && localMethList.size() > 0)
				{	
					CValueVarient  methRetValue;

					//Anil 110906 Commented and inserted below code
					//rc = devPtr()->executeMethod(localMethList, methRetValue);
					rc = devPtr()->executeMethod(localMethList);
					// NOTE: these actions are detected @extract so the notifications are still 
					//       active.  This may produce a problem in the future when these start 
					//       being used

				}//else - no enum actions, don't act
				localMethList.clear();

/* stevev 14aug06 - NOTE: the following should never occur.  Send should always make post-action 
						  commands synchronous. If a post action would send a command, a deadlock 
						  would occur waiting for the response that this method could not service 
						  while running a method    ie THIS ROUTINE MAY NEVER BLOCK */
				/* stevev - 2/19/04 - execute async post actions */
				if (pMsgCyc->actionsEn && pMsgCyc->postActions.size() > 0)
				{	//device->ExecuteMethod(itemID) for each in the list 
					//						(as determined by previous success)
					//						<possible spec interpretation>
					hCmethodCall wrkMthCall;
					vector<hCmethodCall> postActionCallList;
					CValueVarient  methRetValue;

					//Anil 110906 Commented and inserted below code
					//rc = devPtr()->executeMethod(pMsgCyc->postActions, methRetValue);					
					//rc = devPtr()->executeMethod(localMethList);
					rc = devPtr()->executeMethod(pMsgCyc->postActions);//stevev 13oct06 Fri
					devPtr()->notifyVarUpdate(0,  END_actPkt);// allow it to continue
					// TODO: handle return values here
				}//else - no actions, don't act
				/* end stevev 2/19/04 */
			}// else blocking, blocked task responsible for actions
/* stevev 14aug06 is blocking & have enum action list, pass the list via msgCycle to sender * /
			else
			if (localMethList.size() > 0)
			{
			// add to the front of the list  
			// ie for ( lmliT=localMethList.begin();lmliT !=  localMethList.end(); ++lmliT)
			//		pMsgCyc->postActions.insert( pMsgCyc->postActions.begin(),1,lmliT);
			}
			// else is-blocking but no enum actions - do nothing
/ * end stevev 14aug06 */
			/* stevev 2/26/04 - split out actions and msg que return */

			/*-stevev moved notify here */
			/* stevev 9/25/03-added response code handling for all commands PAR5020,5133,5024*/

			// successful data extraction, good packet with data in it, tools to signal
			//		AND the response code + device status is non-zero or isa Cmd48

			/* stevev 1/29/04 - send response codes even if extraction failure */
			/* stevev 2/20/04 - add another filter to skip call when there is no content */
			if ( pPkt != NULL  &&  pPkt->dataCnt > 1  &&  pAsyncRespCdEvent != NULL
				 /*below added stevev 2/20/04 */
				 /*removed 3/3/04 stevev-the new notify function expects to be called often
				 &&   ( (pPkt->theData[0]|pPkt->theData[1]) || (pPkt->cmdNum == 48) )
				 end removal */
				 && pMsgCyc->hostCommStatus != HOST_NORESPONSE )
			         /* end addition stevev 2/20/04 */
			{// do this part before the event trigger
				/* VMKP: 150404, Stored the received command Response code in a
				   command dispatcher global variable */
				m_brespCdVal	=	pPkt->theData[0];
				pMsgCyc->respCdVal  = pPkt->theData[0];
				pMsgCyc->devStatVal = pPkt->theData[1];
				msgDeque.push_back(*pMsgCyc);// makes a copy
				//       let somebody else handle it(method calls filtered in async)
				/* VMKP removed on 150404,  Added this after Write Imd event fire */
				//	pAsyncRespCdEvent->triggerEvent();
			}
			/* <end> addition */
			/*-end move notify */
			/* 28nov05 - notify everybody here */
			/* 9-9-9 move to AFTER we trigger any events::>pMsgCyc->pCmd->notifyUpdate(msgs);*/

			/* stevev 3/2/4 moved trigger here 
			   trigger post-action but pre-release, block status stored so triggered event 
			   cannot change it (message cycle is cleared when returned to the MT queue   */
			
			/* stevev 14apr11 -  ww must decouple the UI notification from the Cancel operation
				required to get the just received device value into the display value. That
				cancel operation must happen before the sender is released.  ie The function
				has to do a readImd to get a value to complete what it is doing.  When they
				were coupled, the function would be told the command was complete by triggering
				its event but, since the notification can cause another send, it is done later
				leaving the display value the same as before the command.  The unblocked
				function uses the display value as if the command has completed even though it
				has just one more thing to do. We will do the cancel here and the notify there.
				-stevev 12may11 - note that this is redundant for write commands who have their
				Cancel done at extraction time.
			*/
			for (lvpIT = varChangedList.begin(); lvpIT != varChangedList.end(); ++lvpIT)
			{                                                                          
				hCVar* pVK = (hCVar*)(*lvpIT);
				pVK->CancelIt();  // copies device value to display value 
			}  
			/* end of 11apr11 */


			//	Signal MsgCycle.CompleteEvent 
			//                      - Send it when we have a msg cycle - someone is waiting
			if ( pMsgCyc->peDone != NULL ) // done-event && not-blocking is a legal combination
			{
				hCevent::hCeventCntrl* pEC = pMsgCyc->peDone->getEventControl();
				if ( pEC != NULL )
				{
					DEBUGLOG(CLOG_LOG,">> RcvPkt Triggering Msg Cycle done event p%p\n",
																			pMsgCyc->peDone);
					pEC->triggerEvent();
					delete pEC;
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)
							(CERR_LOG,"ERROR: could not get msgCycle's done event control.\n");
				}
			}
			else  // is NULL
			if (tmpISblocking == bt_ISBLOCKING)// - an aborted blocking call
			{	
				DEBUGLOG(CLOG_LOG,"(block)returning MT Rcv Msg Cycle p%p\n",pMsgCyc);
				theMsgQueue.ReturnMTObject( pMsgCyc );
				pMsgCyc = NULL;
			}
			else
			{// the following is normal in an async environment
				LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,
					                  "    Finished receiving asycronous command response.\n");
			}

			/* VMKP added on 150404:  Allow trigger of Aync task event to happen after
				Write Imd event,  Above async firing event is commented */
			if ( pPkt != NULL  &&  pPkt->dataCnt > 1  &&  pAsyncRespCdEvent != NULL
				 /*below added stevev 2/20/04 */
				 /* removed 3/3/04 stevev-the new notify function expects to be called always
				 &&   ( (pPkt->theData[0]|pPkt->theData[1]) || (pPkt->cmdNum == 48) )
				 end removal */
				 && pMsgCyc != NULL && pMsgCyc->hostCommStatus != HOST_NORESPONSE )
			{
			         /* end addition stevev 2/20/04 */
				//       let somebody else handle it(method calls filtered in async)														
				DEBUGLOG(CLOG_LOG,">> RcvPkt Triggering Async Resp Code event \n");
				pAsyncRespCdEvent->triggerEvent();
			}
			/* End of VMKP 150404 */
			/* 9-9-9 - do notify that was removed from extract and notifyUpdate moved from above */
			systemSleep(1); // allow the sender to release before continuing                     //
                                                                                                 //
			varPtrLstIT_t lvpIT;                                                                 //
			for (lvpIT = varChangedList.begin(); lvpIT != varChangedList.end(); ++lvpIT)         //
			{                                                                                    //
				hCVar* pVK = (hCVar*)(*lvpIT);                                                   //
				pVK->notify(msgs);  // just fills the msgs list                                   //
			}                                                                                    //
			if ( pCommand != NULL )																 //
			{                                                                                    //
				pCommand->notifyUpdate(msgs);                                                    //
			}                                                                                    //
			/* end 9-9-9 modifications ----------------------------------------------------------*/

			/* stevev - finally,  we return the msgcycle */
			if ( (tmpISblocking == bt_NOTBLOCKING) && (pMsgCyc != NULL)) // we must get rid of it
			{
#ifdef QDBG
				LOGIT(CLOG_LOG,"(nonBlk)returning MT3 Msg Cycle p%p\n",pMsgCyc);
#endif
				theMsgQueue.ReturnMTObject( pMsgCyc );
			}
			// else in a blocking scenario, the owner (Send()) will dispose of it
		}
		else
		{// failed to find the matching message cycle
			// could be a burst packet +++++++++++++++++++++++++++++++++++
			LOGIT(CERR_LOG,"FAILED to get Msg Cycle by pkt p%p\n",pMsgCyc);
		}

	}// end else - we have good parameters
	//	Loop to pend on the next packet or timeout
	if      (lastCmpMode != dm_Standard ) tS = "Lenient"; else tS = "";
	DEBUGLOG(CLOG_LOG,"-x-------------------%s------ cmd %d\n", tS.c_str(),cmd);
}

/* <end cleanup> 03/02/04 - stevev - */


//  used by the items via funcPtr
RETURNCODE hCcmdDispatcher::
getBestItemTransaction(itemID_t id, hCcommand*& pReturnedCmd, 
					   long& returnedTransNum, indexUseList_t& retIdx)
{
	RETURNCODE rc = FAILURE;
	return rc;
}

typedef vector<hCVar*> varPtrList_t;

#ifdef MEM_DEBUG
#include "..\..\sdc625\stdafx.h"       /* for memory tests */
// was built with ,_UNICODE defined
#endif


/* stevev 3/25/04 - the following is a rewrite of the weighing method */

#define WGT_CONSTANT_DATAITEM	  5		/* we prefer constants  */
#define WGT_PER_DATAITEM		(-1)	/* the longer the worse */
#define WGT_FOR_STALE			50		/* higher than most     */
#define WGT_FOR_UNINIT			 5		/* pretty low           */
#define WGT_FOR_INVALID			20		/* has tried and failed */
#define WGT_FOR_WRITE			5000	/* puts it at pretty high priority */
#define WGT_FOR_CMD_MATCH		10
#ifdef WGT_DEBUG
  //	weighTime = time(NULL);_ftime( &(WeighTimeBuffer) );
  static 	struct _timeb       WeighTimeBuffer, lastTimeBuffer, loopTime;
  static	time_t				weighTime;
  static	double				tmpET;
  //	LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d \n",WeighTimeBuffer.time,WeighTimeBuffer.millitm);

  // void GETELAPSED(lastTimeBuffer, tmpET); // param timeb to now, returned in a double
  #define GETELAPSED(ltb, dbl)  {struct _timeb tt; double s,e;_ftime( &(tt) );\
	s = ltb.time + ((double)ltb.millitm/1000); e = tt.time + ((double)tt.millitm/1000);\
	dbl = e - s; ltb = tt; }
  #define GET_SPLIT(ltb, dbl)  {struct _timeb tt; double s,e;_ftime( &(tt) );\
	s = ltb.time + ((double)ltb.millitm/1000); e = tt.time + ((double)tt.millitm/1000);\
	dbl = e - s; }
#endif
//  used by the transactions via funcPtr 
//  returns weight and appended index list
int hCcmdDispatcher::
calcWeight(hCtransaction* pThisTrans, indexUseList_t& retIdx, bool isRead)  
{
	RETURNCODE rc = FAILURE;
	hCdataitemList  RslvdDataItemList(devHndl());
	hCdataitemList  RslvdRqstItemList(devHndl());
	int weight    = 0;
	int idxWgt    = 0;
	INSTANCE_DATA_STATE_T tDS;
	// Anil Merger Activity From FDM to HCF November 2005
	try
	{
#ifdef MEM_DEBUG
    CMemoryState entryMemState, chkPt01MemState, chkPt02MemState, chkPt03MemState, chkPt04MemState,
		                        chkPt05MemState, diffMemState;
    entryMemState.Checkpoint();
#endif
#ifdef WGT_DEBUG
	weighTime = time(NULL);_ftime( &(WeighTimeBuffer) );
	loopTime = lastTimeBuffer = WeighTimeBuffer;
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"%d.%d Enter Command Scale.\n",
					WeighTimeBuffer.time,WeighTimeBuffer.millitm);
#endif
#ifdef _DEBUG
	int  y = retIdx.size();
#endif
	if (pThisTrans == NULL)
	{
		return -1; // very little weight
	}

	if ( isRead )
	{
		idxWgt = 0;
		//rc = pThisTrans->getReplyList(vpl);
		rc = pThisTrans->getReplyDIlist(&RslvdDataItemList);
		rc|= pThisTrans->getReqstDIlist(&RslvdRqstItemList);
		// evaluate request for reads ... idxWgt
		for (hCdataitemList::iterator  iT = RslvdRqstItemList.begin(); 
														iT < RslvdRqstItemList.end() ;  iT++)
		{
			if (iT->type == cmdDataConst || iT->type == cmdDataFloat)
			{
				idxWgt += WGT_CONSTANT_DATAITEM;
			}
			idxWgt += WGT_PER_DATAITEM; // lower by one per request dataitem
		}
	}
	else
	{
		rc = pThisTrans->getReqstDIlist(&RslvdDataItemList);
		RslvdRqstItemList.clear();
	}
	int cmdNum = (pThisTrans->getCmdPtr())->getCmdNumber();
	int trnNum = pThisTrans->getTransNum();

#ifdef WGT_DEBUG	/* defined at the top of this file */
	if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,":::::::::: Entry w/ C %d  T %d calc Weight::::::::::\n",
			  cmdNum,trnNum);
#endif

	if ( rc != SUCCESS )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getReqstList Failed in calcWeight.\n");

		RslvdDataItemList.clear();// in case some got filled
		RslvdRqstItemList.clear();
		return -1;	// abort execution
	}
	// else we have some lists of this transaction's data items

	// isWrite : RslvdDataItemList is list of interest, RslvdRqstItemList is empty
	// isRead  : RslvdDataItemList is list of interest, RslvdRqstItemList has request info

	GroupItemList_t localGroup;
	hIndexUse_t     tmpIdxUse;
	hCVar*          pDataItemVar;
	hCVar*          pVar = NULL;
	hCcommandDescriptor cmdInfo;
	indexUseList_t  requestIndexList;
	int				tmpWgt;
	// 2feb09 stevev - add request side of APP_CMD_VALIDITY_RULE_ERR to weighing
	//					if the command will not be sent, it automatically has zero weight
	int  reqstListSize = 0, requestlistValid = 0;

	/* get the index and info subset of items */
	if ( isRead )
	{	
		if (RslvdRqstItemList.size() > 0)
		{
			requestlistValid =		// we'll reduce by invalids
			reqstListSize = RslvdRqstItemList.size();// stevev 2feb09
			hIndexUse_t locIdx;
			for (hCdataitemList::iterator  iT = RslvdRqstItemList.begin(); 
										   iT < RslvdRqstItemList.end()  ;  iT++)
			{
				// stevev 2feb09 - reduce by each invalid value
				pDataItemVar = iT->getVarPtr();
				if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
				{
					requestlistValid --;
				}
				else// no use messing with invalids...
				if ((iT->flags & cmdDataItemFlg_indexNinfo) == cmdDataItemFlg_indexNinfo)
				{
					// moved outside 'if' stmt 2feb09     pDataItemVar = iT->getVarPtr();
					if (pDataItemVar != NULL)
					{	
						locIdx.indexSymID = pDataItemVar->getID();
#ifdef WGT_DEBUG	/* defined at the top of this file */
						if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
						  LOGIF(LOGP_WEIGHT)(CLOG_LOG,"=insert requested RD index 0x%04x\n",
												locIdx.indexSymID);
#endif
						requestIndexList.push_back(locIdx);
					}
				}
			}
		}// else - empty request
	}
	/* <START> stevev 020504 - add write weighting */
	else /* isWrite */
	{
		if (RslvdDataItemList.size() > 0)
		{
			requestlistValid =		// we'll reduce by invalids
			reqstListSize = RslvdDataItemList.size();// stevev 2feb09
			hIndexUse_t locIdx;
			for (hCdataitemList::iterator  iT = RslvdDataItemList.begin(); 
										   iT < RslvdDataItemList.end() ;  iT++)
			{
				// stevev 2feb09 - reduce by each invalid value
				pDataItemVar = iT->getVarPtr();
				if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
				{
					requestlistValid --;
				}
				if ((iT->flags & cmdDataItemFlg_indexNinfo) == cmdDataItemFlg_indexNinfo)
				{
					// moved outside 'if' stmt 2feb09     pDataItemVar = iT->getVarPtr();
					if (pDataItemVar != NULL)
					{	
						locIdx.indexSymID = pDataItemVar->getID();
#ifdef WGT_DEBUG	/* defined at the top of this file */
						if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
						  LOGIF(LOGP_WEIGHT)(CLOG_LOG,"=insert WR requested index 0x%04x\n",
						  locIdx.indexSymID);
#endif
						requestIndexList.push_back(locIdx);
					}
				}
			}
		}// empty request - bad form for a write command
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Write command [# %d] with empty Request transaction.\n",cmdNum);
		}
	}
	/* stevev 02feb09 - add request side of validty command rules to the weighing algorithm */
	if (reqstListSize > 0)
	{
		if ( (! isRead ) &&  (compatability() == dm_275compatible)  )
		{// write and lenient is a special case
			if ( requestlistValid <= 0 )
			{// ALL request items are invalid - we be done	
#ifdef WGT_DEBUG
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,
						"ERROR: calcWeight exit on Request Validity error (lenient).\n");
#endif
				RslvdDataItemList.clear();// in case some got filled
				RslvdRqstItemList.clear();
				return -1;	// abort execution with very low weight
			}
			// else continue weighing, no exception
		}
		else
		if( requestlistValid < reqstListSize )
		{// one or more request packet items are invalid
#ifdef WGT_DEBUG
			LOGIF(LOGP_WEIGHT)(CLOG_LOG,"ERROR: calcWeight exit on Request Validity error.\n");
#endif
			RslvdDataItemList.clear();// in case some got filled
			RslvdRqstItemList.clear();
			return -1;	// abort execution with very low weight
		}
		// else continue weighing, no exception
	}// else its an empty request (no index, no write data)

	/* < END > stevev 020504 - adding write weighting */
#ifdef MEM_DEBUG
    chkPt01MemState.Checkpoint();
    if( diffMemState.Difference( entryMemState, chkPt01MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** entry-2-01 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif

	/* we have our basic lists, process for weight */
	if (requestIndexList.size() > 0)// a request with one or more info/index
	{// do it the new way < must be a read >...not necessarily - deal with it later		
#ifdef WGT_DEBUG	/* defined at the top of this file */
		if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"IndexSubset holds %d indexes.\n",requestIndexList.size());
#endif
#ifdef LOG_WEIGH_FILTER
		bool doWayLog = false;
		if (requestIndexList.size() > 0)// was 2, then 1, now 0
			doWayLog = true;
#endif
#ifdef _DEBUG
		if (requestIndexList.size() > 2)
		{
			LOGIT(CLOG_LOG,"Weighing multiples.\n");
		}
#endif
		// get the list of variables
		CVarList* pVarList = (CVarList*)devPtr()->getListPtr(iT_Variable);
		hCcommandDescriptor  curRdCmd;
		if ( pVarList != NULL)
		{// ptr to a list of pointers to hCVars - will exit otherwise
// these proly aren't needed			
//itemIDlist_t foundIDlist;
//itemIDlist_t::iterator fidlIT;

			bool fndOne;
			bool haveAltValue;	// flag in the loop to see if we keepgoing
			int  lastWgt;
			int  varValWgt;		// the weight count for the current index value
			bool keepgoing;
			itemIDlist_t foundIdxValues; // list of longs, values already tested
			itemIDlist_t foundVarsIndexed;// stevev 10jan06 - filter prevents reweigh-for entire cmd
			itemID_t     lastVarIndexed = 0, tmpVar = 0;  // stevev 10jan06 - for filter
			//indexUseList_t
			hIndexUse_t  lastVarIndexUse;// best weight
			hIndexUse_t  currentIndexUse;//indexSymID  [0], indexDispValue [-1]
			hIndexUse_t  indexVarUse;
#ifdef WGT_DEBUG /* temp*/
//	vector<ulong>::iterator i;
#endif			
			for (idxUseIT ITX = requestIndexList.begin();ITX < requestIndexList.end();ITX++)
			{// for each index variable in the request transaction	

#ifdef WGT_DEBUG	/* defined at the top of this file */
				if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )	
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"== test Requested index 0x%04x if %d indexes\n",
					ITX->indexSymID, requestIndexList.size());
				int goingCnt = 0;
#endif
				fndOne       = false;
				haveAltValue = false;
				lastWgt      = -50000; // everything will be bigger
				varValWgt    = 0;
				keepgoing    = true;
				currentIndexUse.clear();// negative indexes are illegal
				lastVarIndexUse.clear();// should have been stored before we get here

				try
				{		/* nested try from fdm merge */
				while (keepgoing) // loop for all index values
				{
#ifdef WGT_DEBUG
	GETELAPSED(lastTimeBuffer,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"===starting idx 0x%04x ET: %5.3f ====\n",
															indexVarUse.indexDispValue,tmpET);
	int varCnt = 0;
	goingCnt++;
	// profile results 06aug09: the rest of this routine takes between 0 and 15 mS
#endif
					for(CVarList::iterator VLIT = pVarList->begin(); 
					                       VLIT < pVarList->end();   VLIT++)
					{// ptr 2a ptr 2a hCVar ---- for each device variable
						hCVar* pVar = (hCVar*)(*VLIT);
#ifdef WGT_DEBUG
varCnt++;
#endif
						// issues with 2005 compiler 
						//   if ( VLIT != NULL && (*VLIT) != NULL && (!((*VLIT)->IsValid())) )
						if ( pVar != NULL  && (!(pVar->IsValid())) )
						{// we do not weigh invalid variables
							continue; // next variable
						}
						/** start stevev - add filter previous resolved **/
						int cc = pVar->getID();
						if ( valueFromIntList(foundVarsIndexed,cc) )
						{// we have an index that resolves this variable 
							continue;// do not re-weigh it
						}
						/**  end  stevev - add filter previous resolved **/
						if ( isRead )
						{
							curRdCmd = pVar->getRdCmd(cmdNum,trnNum,ITX->indexSymID);
						}
						else // isWrite
						{
							curRdCmd = pVar->getWrCmd(cmdNum,trnNum,ITX->indexSymID);
						}

						if ( curRdCmd.cmdNumber < 0xffff )// 0xffff on none found
						{// VLIT uses this command/transaction with this index (otherwise skip)
#ifdef WGT_DEBUG
							if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
							{	if ((*VLIT)->getID() == WGT_SYMBOL_OF_INTEREST)
								{
									LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Critical val command = %d\n",
																		curRdCmd.cmdNumber);
								}
							}
#endif
							// get the actual index use case
							for (idxUseIT itCD = curRdCmd.idxList.begin(); 
										  itCD < curRdCmd.idxList.end();   itCD++)
							{// itCD isa ptr2a hIndexUse_t
								if ( itCD->indexSymID == ITX->indexSymID)
								{// found one
									indexVarUse = (*itCD);
									break; // out of for
								}
							}
							if (indexVarUse.isNOTempty() && 
								( (int)(indexVarUse.indexDispValue) >=0))
							{// we have something to work with 

								// now see if we already have it
								if (! fndOne )// we are looking for a usable index value
								{
									if (! valueFromIntList(foundIdxValues,
										  (int)(indexVarUse.indexDispValue)))
									{// this value is not in the list
										fndOne = true;
										currentIndexUse = indexVarUse;
										foundIdxValues.push_back
													   ((int)(currentIndexUse.indexDispValue));
#ifdef WGT_DEBUG
										if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
										{	if (pVar->getID() == WGT_SYMBOL_OF_INTEREST)
											{
										LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Critical first found\n");
											}
										}
#endif
									}
									else
									{// we have already processed this index value
										continue;// do the next variable
									}
								}
								// else just use the current currentIndexUse

								if (indexVarUse.indexDispValue == 
									currentIndexUse.indexDispValue)
								{// we got one ( will always match the first one)
#ifdef WGT_DEBUG
									if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
									{	if (pVar->getID() == WGT_SYMBOL_OF_INTEREST)
										{
							LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Critical var usage found value = %d"
												"\n",(int)(currentIndexUse.indexDispValue) );
										}
									}
#endif
									tmpWgt = 0;
/*******************				generate weight */
									tDS = pVar->getDataState();

									if (pVar->getDataStatus() == 0)// weigh if not zero
									{// do not weigh this item - 
									 // it has been marked as a problem variable
#ifdef WGT_DEBUG	/* defined at the top of this file */
										if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
							LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   Var 0x%04x Has a BAD data status "
											"(mis-behaved)"	"\n",pVar->getID() );
#endif
									}
									else
									if (tDS == IDS_STALE )
									{
										tmpWgt = WGT_FOR_STALE;							
									}
									else
									if (tDS == IDS_UNINITIALIZED)
									{
										tmpWgt = WGT_FOR_UNINIT;							
									}
									else
									if (tDS == IDS_INVALID)
									{
										tmpWgt = WGT_FOR_INVALID;							
									}
									else
									if ( ( ! isRead) && tDS == IDS_NEEDS_WRITE)
									{
										tmpWgt = WGT_FOR_WRITE;							
									}
									else // IDS_CACHED,IDS_PENDING,
									{
										tmpWgt = 0;
#ifdef WGT_DEBUG	/* defined at the top of this file */
										if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
							LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x is in a default state "
											" %s\n",pVar->getID(),instanceDataStStrings[tDS]);
									}
									if ((cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT ) &&
										(pVar->getID() == WGT_SYMBOL_OF_INTEREST))
									{
							LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   Critical Value has state: %s  with"
										  " a weight: %d\n",instanceDataStStrings[tDS],tmpWgt);
#endif
									}
									int qcnt = (pVar->getQueryCnt() + 1);//05feb14 stevev
									tmpWgt *= (qcnt*qcnt);// make the queue more important
#ifdef _DEBUG
									/** In an array of collections scenario, a single index may
										resolve several collection elements in a single transaction
										This log is no longer needed
									if (varValWgt) // stevev added detection 10jan06
										LOGIT(CLOG_LOG,"Found another Var using C/T/I/V.\n");
									**/
#endif
/*******************				end generate weight */
									tmpVar = pVar->getID();// stevev 10jan06 - 4 filter (capture last only - should only be one)
									varValWgt += tmpWgt;
#ifdef LOG_WEIGH_FILTER
		if( doWayLog ) LOGIT(CLOG_LOG,"WF: found Idx:0x%04x Val:%d Wgt:%d Resolves:0x%04x\n",
		   currentIndexUse.indexSymID ,(int)(currentIndexUse.indexDispValue),varValWgt,tmpVar);
#endif
								}
								else
								{// a different, unprocessed index value for this index var
									// skip weigh-in and try to flag
									if (! valueFromIntList
										(foundIdxValues,(int)(indexVarUse.indexDispValue)))
									{// this value is not in the list
										haveAltValue = true;// flag to continue processing
									}
									// else - it's been processed, take no action
								}
							}
							else
							{// no index value - ??????? do we need to do something else????????
								LOGIT(CLOG_LOG,"Index Has no value\n");
							}
						}
						// else - this var does NOT use this command/transaction with this index
					}// next variable
					// finished this pass with this index variable and this unique value
					if (varValWgt > lastWgt )
					{
						lastWgt = varValWgt;
						lastVarIndexUse = currentIndexUse;
						lastVarIndexed = tmpVar ;// stevev 10jan06 - 4 filter - the var this resolves
#ifdef WGT_DEBUG
						if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
						{
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Critical var weighed %d for a total %d\n",varValWgt,lastWgt);
						}
#endif
#ifdef LOG_WEIGH_FILTER
		if( doWayLog )
		{	LOGIT(CLOG_LOG,"WF: maxed 0x%04x Val:%d Wgt:%d Resolves:0x%04x\n",
		   currentIndexUse.indexSymID ,(int)(currentIndexUse.indexDispValue),varValWgt,tmpVar);
			LOGIT(CLOG_LOG,"  : going cnt = %d, var cnt = %d\n",goingCnt,varCnt);
		}
#endif
					}
					// else, just skip it since we already have a better weight

					if (haveAltValue)
					{// reset for another unique value of this index variable
						keepgoing       = true;
						fndOne          = false; 
						haveAltValue    = false;
						varValWgt       = 0;
						currentIndexUse.clear(); 
#ifdef X_WGT_DEBUG /* temp - remove to cut down printing time */
	LOGIT(CLOG_LOG,"Still going.  Cmd:%d  Trans:%d Index 0x%04x\n",cmdNum,trnNum,ITX->indexSymID);
	LOGIT(CLOG_LOG,"              Used Index Values:\n");
	for ( i = foundIdxValues.begin(); i < foundIdxValues.end(); i++)
	{
	LOGIT(CLOG_LOG,"                      %d\n", (*i));
	}
#endif
					}
					else // no more unique values
					{
						keepgoing = false; // we made the last pass, do the next index now
					}
				}// wend, keepgoing
#ifdef WGT_DEBUG
	GETELAPSED(lastTimeBuffer,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====xET: %5.3f ====\n",tmpET);
	// profile results 06aug09: the rest of this routine takes between 0 and 15 mS
#endif
				// Anil Merger Activity From FDM to HCF November 2005 -start
				}///* nested try from fdm merge */
				catch(...)
				{
					LOGIT(CERR_LOG,"Calc weight: Inside Index Variable Weight calc: "
																	"inside catch(...)\n");
				}
				// Anil Merger Activity From FDM to HCF November 2005 -End
				// add lastWgt and lastVarIndexUse to the command return info
				if (lastVarIndexUse.isNOTempty())
				{
#ifdef _DEBUG
					if (lastVarIndexUse.indexSymID == 0)
					{
						LOGIT(CLOG_LOG,"Index symbol not allowed: 0x%04x\n",
							lastVarIndexUse.indexSymID);
					}
#endif
					retIdx.push_back(lastVarIndexUse);  // a hIndexUse_t list
					foundVarsIndexed.push_back(lastVarIndexed);// stevev 10jan06 - add variable resolved to filter
#ifdef LOG_WEIGH_FILTER
		if( doWayLog ) LOGIT(CLOG_LOG,"WF: returned 0x%04x Val:%d Resolves:0x%04x\n",
			lastVarIndexUse.indexSymID ,(int)(lastVarIndexUse.indexDispValue),lastVarIndexed);
#endif
				}
				// weight generated in second part
				//    weight += (lastWgt + 3);			// add idxWgt before return
				foundIdxValues.clear();  // do it here so it's clear on exit of loop
			}// next index variable
		}
		else
		{// ptr to a list of pointers to hCVars - could not be aquired
			retIdx.clear();
			weight = -1;
		}			
#ifdef WGT_DEBUG
		if ( (cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT) && weight > 0 )
		{
LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Cmd:%d Trans:%d acquired %d weight with %d indexes returned:\n",
														cmdNum,trnNum,weight,retIdx.size());
			for ( idxUseIT itdmp = retIdx.begin(); itdmp < retIdx.end(); itdmp++)
			{// for each index to be returned
				LOGIT(CLOG_LOG,"              ");
				itdmp->clogSelf();
			}
		}
#endif
#ifdef LOG_WEIGH_FILTER
	if (doWayLog)
LOGIF(LOGP_WEIGHT)(CLOG_LOG,"WF:===========================================\n");
#endif
	}// endif requestIndexList has values
	// else there are no index and info in the request, drop through to weigh everything else
	requestIndexList.clear();

#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====aET: %5.3f ====\n",tmpET);
	// profile results 06aug09: the rest of this routine takes between 0 and 15 mS
#endif
#ifdef MEM_DEBUG
    chkPt02MemState.Checkpoint();
    if( diffMemState.Difference(chkPt01MemState , chkPt02MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** 01-2-02 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif
	// now set these indexes , re-resolve the set

	if (retIdx.size() > 0)
	{
		hCitemBase* pIB ;
		hCinteger*  pII;
		CValueVarient vv;

/******INDEX MUTEX****/ pIndexMutex->aquireMutex(hCevent::FOREVER);// waits forever

		
#ifdef LOG_INDEX_MUTEX
		LOGIT(CLOG_LOG,"Got Index Mutex: Dispatch Line %d\n",__LINE__);
#endif
		// store  the current value, set a new value
		for (idxUseIT itRet = retIdx.begin(); itRet < retIdx.end(); itRet++)
		{
			if ( SUCCESS == devPtr()->getItemBySymNumber(itRet->indexSymID,&pIB) && pIB != NULL )				
			{
				tmpIdxUse.indexSymID = itRet->indexSymID;
				pII                  = (hCinteger*)pIB;
				tmpIdxUse.indexDispValue = pII->getDispValue(true);
				tmpIdxUse.indexWrtStatus = pII->getWriteStatus();
				pII->setWriteStatus(0);// set to NOT user written to avoid screen updations
				requestIndexList.push_back(tmpIdxUse);

				if ( pII->IsLocal() )
				{	
				//LOGIT(CLOG_LOG," Idx 0x%04x to %d ",itRet->indexSymID,itRet->indexDispValue);
					vv = (unsigned)(itRet->indexDispValue);
					pII->setDispValue(vv, true);	// set one
					pII->ApplyIt();				    // copy it to the other
					pII->markItemState(IDS_CACHED); // force it good
				}
				else
				{
					LOGIT(CLOG_LOG," Info & Index Var NOT Set (is NOT Local) 0x%04x ",
																		itRet->indexSymID);
				}
				tmpIdxUse.clear();
			}// not much we can do
		}
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====bET: %5.3f ====\n",tmpET);
#endif
		// re-resolve list
		RslvdDataItemList.clear();
		if(isRead)
		{
			rc = pThisTrans->getReplyDIlist(&RslvdDataItemList);
		}
		else // isWrite
		{
			rc = pThisTrans->getReqstDIlist(&RslvdDataItemList);
		}
		if ( rc != SUCCESS )
		{
			LOGIT(CLOG_LOG,"Indexed List did not re-resolve.\n");
		}
	}
#ifdef MEM_DEBUG
    chkPt04MemState.Checkpoint();
    if( diffMemState.Difference(chkPt02MemState , chkPt04MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** 06 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif
	requestlistValid =		// we'll reduce by invalids...just in case it isRead
	reqstListSize = RslvdDataItemList.size();// stevev 2feb09
	
	// weigh the reply 4 read or request 4 write
	for ( hCdataitemList::iterator  iT = RslvdDataItemList.begin(); 
									iT < RslvdDataItemList.end() ;  iT++)
	{// iT is a ptr 2 a hCdataitem	   
		pDataItemVar = iT->getVarPtr();	
		if(pDataItemVar && isRead && (! pDataItemVar->IsValid()) )// isRead  AND Not Valid
		{// this is the reply packet with an invalid item
			requestlistValid --;
		}

		/* stevev 12mar07 - we need to deal with the unsent write due to validity*/
		if(pDataItemVar && (! isRead) && (! pDataItemVar->IsValid()) )// isWrite AND Not Valid
		{// this won't be sent
			weight -= 50000;
			continue;
		}
		/** end 12mar07 **/
		if ( (iT->flags & cmdDataItemFlg_Info) != 0 ||   // it is an info data Item
			  pDataItemVar == NULL                  ||
			! pDataItemVar->IsValid()               ||// stevev 23mar09 bug2596..added a couple )
			pDataItemVar->getID() == RESPONSECODE_SYMID ||
			pDataItemVar->getID() == DEVICESTATUS_SYMID )// stevev - skip weighing these
		{											// includes index and info weighed above
			continue;// skip it (add zero weight)
		}

		int tmpWgt = 0;

		tDS = pDataItemVar->getDataState();	

		if (pDataItemVar->getDataStatus() == 0)// weigh if not zero < when reading only
		{// do not weigh this item -  it has been marked as a problem variable
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   Read Var 0x%04x Has a BAD data status (mis-behaved)\n",
																		pDataItemVar->getID());
#endif
		}
		else
		if ( isRead && tDS == IDS_STALE )
		{
			tmpWgt = WGT_FOR_STALE;			
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for stale.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_STALE,tmpWgt);
#endif				
		}
		else
		if ( isRead && tDS == IDS_UNINITIALIZED)
		{
			tmpWgt = WGT_FOR_UNINIT;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for uninit.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_UNINIT,tmpWgt);
#endif				
		}
		else
		if ( isRead && tDS == IDS_INVALID)
		{
			tmpWgt = WGT_FOR_INVALID;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for invalid.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_INVALID,tmpWgt);
#endif					
		}
		else
		if ((!isRead) && tDS == IDS_NEEDS_WRITE)
		{
			tmpWgt = WGT_FOR_WRITE;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for needs write.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_WRITE,tmpWgt);
#endif											
		}
		else
		{	tmpWgt = 0;
		}	
		int qcnt = (pDataItemVar->getQueryCnt() + 1);
#ifdef WGT_DEBUG	/* defined at the top of this file */
		if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x multiplies %d by query %d to get: %d\n",
		//			pDataItemVar->getID(),tmpWgt,(pDataItemVar->getQueryCnt() + 1),
		//			(tmpWgt*(pDataItemVar->getQueryCnt() + 1))  );
					pDataItemVar->getID(),tmpWgt,qcnt*qcnt,(tmpWgt*qcnt*qcnt)  );
#endif									
		tmpWgt *= (qcnt * qcnt);

		if (tmpWgt)
		{
			hCcommandDescriptor thisCmdDesc;
			if (isRead)
			{
				thisCmdDesc = pDataItemVar->getRdCmd();
				cmdInfo     = pDataItemVar->getRdCmd(cmdNum);
			}
			else
			{
				thisCmdDesc = pDataItemVar->getWrCmd();
				cmdInfo     = pDataItemVar->getWrCmd(cmdNum);
			} 
			if (cmdInfo.cmdNumber < 0xffff)
			{
				weight+=tmpWgt;// will resolve a variable
			}// else invalid cmd number

			if (cmdInfo.cmdNumber == thisCmdDesc.cmdNumber)
			{
#ifdef WGT_DEBUG	/* defined at the top of this file */
				if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for command match.\n",
														pDataItemVar->getID(),WGT_FOR_CMD_MATCH);
#endif
				weight+=WGT_FOR_CMD_MATCH;// skew toward selected commands
			}
		}//no weight so far, we shouldn't add more
	}// next packet var
	if ( weight > 0 ) // no reason to give weight to a weightless command
	{
		weight += idxWgt; // mess with the request weight ( zero on write cmd )
	}

#ifdef MEM_DEBUG
    chkPt03MemState.Checkpoint();
    if( diffMemState.Difference(chkPt02MemState , chkPt03MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** 02-2-03 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif

	if (retIdx.size() > 0)
	{		
		hCitemBase* pIB ;
		hCinteger*  pII;
		CValueVarient vv;
		// restore  the current value
		for (idxUseIT itRet = requestIndexList.begin(); itRet < requestIndexList.end(); itRet++)
		{
			if ( SUCCESS == devPtr()->getItemBySymNumber(itRet->indexSymID,&pIB) && pIB != NULL )				
			{
				pII                  = (hCinteger*)pIB;
				if ( pII->IsLocal() )
				{	
				//LOGIT(CLOG_LOG," Idx 0x%04x to %d ",itRet->indexSymID,itRet->indexDispValue);
					vv = (unsigned)(itRet->indexDispValue);
					pII->setDispValue(vv, true);	// set one
					pII->ApplyIt();				    // copy it to the other
					pII->markItemState(IDS_CACHED); // force it good
					pII->setWriteStatus(itRet->indexWrtStatus);// put it back like we found it
				}
				//else - NOT-Local...was logged when we stored it, don't do it again
			}// not much we can do
		}
/******INDEX MUTEX****/ pIndexMutex->relinquishMutex();
	}
	requestIndexList.clear();
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====cET: %5.3f ====\n",tmpET);
#endif
	/* stevev 2feb09 - handle the invalid rules for the reply packet (request done above)*/
	if ( ! isRead ) // we have to deal with the isWrite reply
	{
		rc = pThisTrans->getReplyDIlist(&RslvdRqstItemList);// put reply into an empty list
		requestlistValid =		// we'll reduce by invalids...just in case it isRead
		reqstListSize = RslvdRqstItemList.size();// stevev 2feb09

		for (hCdataitemList::iterator  iT = RslvdRqstItemList.begin(); 
														iT < RslvdRqstItemList.end() ;  iT++)
		{//  reduce by each invalid value
			pDataItemVar = iT->getVarPtr();
			if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
			{
				requestlistValid --;
			}
		}
	}
	//else the isRead reply is done
	// the reply rule is: if all in the reply packet are invalid, the command is not sent
	// but response code and device status don't count for this.  I assume that 100% of
	// tokenized reply packets have response-code & device-status variables.
	if ( requestlistValid <= 2 && reqstListSize > 2 )// none valid & not a command-command
	{// invalid packet
#ifdef WGT_DEBUG
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"ERROR: calcWeight exit on Reply Validity error.\n");
#endif
		RslvdDataItemList.clear();// in case some got filled
		RslvdRqstItemList.clear();
		return -1;	// abort execution with very low weight
	}
	/* stevev end 2feb09 **/

	RslvdDataItemList.clear();// before destruction
	RslvdRqstItemList.clear();

#ifdef WGT_DEBUG	/* defined at the top of this file */
	if (weight > 0 || (cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT) )
	{
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"     ** Cmd %03d Trans %d (%03d)\n",cmdNum,trnNum,weight);
	}
	else
	{
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"     ** Cmd %03d Trans %d has Zero Weight\n",cmdNum,trnNum);
	}
#endif
#ifdef MEM_DEBUG
    chkPt05MemState.Checkpoint();
    if( diffMemState.Difference( entryMemState, chkPt05MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
	//	if (diffMemState.m_lSizes[1] > 100000 )
		if (diffMemState.m_lTotalCount > 10000000 )
		{
			TRACE(_T("** entry -2-05 total diff **\n"));
			diffMemState.DumpStatistics();
		
			if( diffMemState.Difference( chkPt03MemState, chkPt05MemState ) )
			{
		//		oldMemState.DumpAllObjectsSince();
//				if (diffMemState.m_lSizes[1] > 100000 )
				if (diffMemState.m_lTotalCount > 10000000 )
				{
					TRACE(_T("** 05-2-03 last section diff **\n"));
					diffMemState.DumpStatistics();
				}
			}


		}
    }
#endif
#ifdef WGT_DEBUG	/* defined at the top of this file */
	if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,":::::::::: Exit w/ C %d  T %d Weight %d ::::::::::\n",
																		cmdNum,trnNum,weight);
	for ( idxUseIT itxdmp = retIdx.begin(); itxdmp < retIdx.end(); itxdmp++)
	{// for each index to be returned
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Return Index:  ");
		itxdmp->clogSelf();
	}

	weighTime = time(NULL);_ftime( &(WeighTimeBuffer) );
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"%d.%d Exit Command Scale.\n",
					WeighTimeBuffer.time,WeighTimeBuffer.millitm);
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ==== Leaving ET: %5.3f ====\n",tmpET);
	// profile results 06aug09: the rest of this routine takes between 0 and 15 mS
#endif
#ifdef _DEBUG
	int gh = retIdx.size();
#endif
	return weight;
// Anil Merger Activity From FDM to HCF November 2005 -start
	}//try
	catch(...)
	{
		LOGIT(CERR_LOG,"Calc weight inside catch(...)\n");
		//Deepak 101804
		weight = WGT_FOR_STALE;
		pIndexMutex->relinquishMutex();
		//END
	}
// Anil Merger Activity From FDM to HCF November 2005 -End
	return weight;
}

#ifdef _DEBUG // hunting the change during run
int retIdxSize = 0;
bool acEnabled = false;
#endif
//  used by the transactions via funcPtr // visited instead of one above, 
//  uses hCIndexIDValueVariableMap in the transaction to optimize the index calculations
//  returns weight and appended index list
int hCcmdDispatcher::
calcWeightOptimized(hCtransaction* pThisTrans, indexUseList_t& retIdx, bool isRead)  
{
	RETURNCODE rc = FAILURE;
	hCdataitemList  RslvdRplyItemList(devHndl());
	hCdataitemList  RslvdRqstItemList(devHndl());
	hCdataitemList::iterator  iT;
	hCVar*          pDataItemVar;
	int weight    = 0;
	int idxWgt    = 0;
	int  reqstListSize  = 0, 
		 reqstlistValid = 0;
	int  replyListSize  = 0,
		 replyListValid = 0;

	// 7dec09 - moved from below to be usable throughout
	int cmdNum = (pThisTrans->getCmdPtr())->getCmdNumber();
	int trnNum = pThisTrans->getTransNum();

#ifdef WGT_DEBUG
	weighTime = time(NULL);_ftime( &(WeighTimeBuffer) );
	loopTime = lastTimeBuffer = WeighTimeBuffer;
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"================= %I64d.%03hd Enter Transaction Scale.\n",
					WeighTimeBuffer.time,WeighTimeBuffer.millitm);
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{	
		isOfInterest = true;
	}
#endif	
	try
	{		
		if (pThisTrans == NULL || ! appCommEnabled())// stevev 08apr10 bug 3034 subsequent
		{
			return -1; // very little weight
		}
#if 0
		if ( isRead )
		{
			idxWgt = 0;
			//rc = pThisTrans->getReplyList(vpl);
			rc = pThisTrans->getReplyDIlist(&RslvdRplyItemList);
			rc|= pThisTrans->getReqstDIlist(&RslvdRqstItemList);
			
			// we'll deal with the invalid count in this loop
			reqstlistValid =		// we'll reduce by invalids
			reqstListSize  = RslvdRqstItemList.size();// stevev 2feb09

			// evaluate request for reads ... idxWgt
			for (iT = RslvdRqstItemList.begin(); iT != RslvdRqstItemList.end();  ++iT)
			{
				hCdataItem *pDI = (hCdataItem*)&(*iT);
				pDataItemVar = pDI->getVarPtr();
#ifdef _DEBUG
				unsigned id = pDataItemVar->getID();
#endif
				
				if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
				{
					reqstlistValid --;
				}

				if (pDI->type == cmdDataConst || pDI->type == cmdDataFloat)
				{
					idxWgt += WGT_CONSTANT_DATAITEM;
				}
				idxWgt += WGT_PER_DATAITEM; // lower by one per request dataitem
			}// next request item
		}
		else // its a write
		{
			rc = pThisTrans->getReqstDIlist(&RslvdRqstItemList);
			RslvdRplyItemList.clear();

			// we need to count the invalids, now is good
			reqstlistValid =		         // we'll reduce by invalids
			reqstListSize    = RslvdRqstItemList.size();

			// evaluate request for writes
			for (iT = RslvdRqstItemList.begin(); iT != RslvdRqstItemList.end();  ++iT)
			{
				hCdataItem *pDI = (hCdataItem*)&(*iT);
				pDataItemVar = pDI->getVarPtr();
#ifdef _DEBUG
				unsigned id = pDataItemVar->getID();
#endif
				
				if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
				{
					reqstlistValid --;
				}
			}// next request item
			idxWgt = 0;
		}
		
		if ( rc != SUCCESS )
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getReqstList Failed in calcWeightOptimized.\n");

			RslvdRplyItemList.clear();// in case some got filled
			RslvdRqstItemList.clear();
			return -1;	// abort execution
		}
		// else we have some lists of this transaction's data items

		/* add request side of validty command rules to the weighing algorithm */
		// test the request list ( should be filled whether read or write )	
		if (reqstListSize > 0)
		{
			if ( (! isRead ) &&  (compatability() == dm_275compatible)  )
			{// write and lenient is a special case
				if ( reqstlistValid <= 0 )
				{// ALL request items are invalid - we be done	
					RslvdRplyItemList.clear();   // in case some got filled
					RslvdRqstItemList.clear();
					return -1;	                 // abort execution with very low weight
				}
				// else continue weighing, no vaidity exception was found
			}
			else
			if( reqstlistValid < reqstListSize )
			{// one or more request packet items are invalid
				RslvdRplyItemList.clear();// in case some got filled
				RslvdRqstItemList.clear();
				return -1;	// abort execution with very low weight
			}
			// else continue weighing, no vaidity exception was found
		}// else its a command command
#else
		rc = pThisTrans->getReqstDIlist(&RslvdRqstItemList);// this merely resolves the cond list
		
#endif /* 0   -skip */
		
		// extract the index&info variables from the request packet
		itemID_t                  idxID          = 0;
		hCIndexValue2Variable    *pIV2V          = NULL;
		IdxVal2VarList_t          transIdxNVarList;

		retIdx.clear(); // we do it all, we are not additive

		for ( iT = RslvdRqstItemList.begin(); iT < RslvdRqstItemList.end(); iT++)
		{// for each index variable in the request transaction				
			hCdataItem *pDI = (hCdataItem*)&(*iT);
			pDataItemVar    = pDI->getVarPtr();
			if (pDataItemVar != NULL && pDataItemVar->IsValid() )
			{
#ifdef _INVENSYS	/* stevev 12aug11 - this looks like a bad idea- use with caution */			
				if ((pDI->flags & cmdDataItemFlg_Index) == cmdDataItemFlg_Index)	// J.U.
#else
				if ((pDI->flags & cmdDataItemFlg_indexNinfo) == cmdDataItemFlg_indexNinfo)
#endif
				{// we only deal with info & index
					idxID = pDataItemVar->getID();
					pIV2V = pThisTrans->getMap4Index(idxID);
					// we have a list of values & the variables each resolves
					if (pIV2V == NULL )
					{// error condition - not found in the map
						continue;// next index
					}// else - process	

					transIdxNVarList.push_back(*pIV2V);

				}// else it is not an index, we don't consider it here
			}// else - item is null (possible constant) or is invalid, do next dataitem
		}// next dataitem 

#ifdef WGT_DEBUG
	int cnt = 0;
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{
		GET_SPLIT(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ***cmd %d enter idxlist ST: %5.3f ***\n", cmdNum, tmpET);
	
	isOfInterest = true;
	}
#endif	
		// from that list of index&infos, get the best combination of indexes.
		rc = findBestIdxList(retIdx, transIdxNVarList, isRead, cmdNum, trnNum);
#ifdef _DEBUG
	#ifdef WGT_DEBUG
		if ( ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT ) && retIdx.size() > 0 )
		{
			LOGIT(CLOG_LOG,"=+=+=+= best index list =+=+=+=\n");
			for ( IdxVal2VarList_IT_t iIT = transIdxNVarList.begin();
															iIT != transIdxNVarList.end();++iIT)
			{
				hCIndexValue2Variable* piv2v = (hCIndexValue2Variable*)&(*(iIT));
				piv2v->clogSelf();
			}
			LOGIT(CLOG_LOG,"=+=+= end best index list =+=+=\n");
		}
    #endif
#endif
		hIndexUse_t               localIdxUse,
			                     *pIdxUse        = NULL;
		INSTANCE_DATA_STATE_T     tDS;

//get index mutex,
//see line 2459 for re-resolution sequence
//-- its like we're redoing the whole thing under the index mutex...???
// with the best index(es) selected
		
	indexUseList_t  requestIndexList;
//	hCdataitemList  RslvdRqstItemList(devHndl());
//	hCdataitemList  RslvdRplyItemList(devHndl());
	
	hCcommandDescriptor cmdInfo;

	// we have to use a local flag to tell if we have acquired the indexMutex or not
	// The MEE can startup after we get the mutex, disable appComm and then pend on the mutex
	// we continue to the end but the appComm is disabled down there so we don't release the mutex
	// Setting the flag when we get it and testing the flag at the end stops this mutex loss
	bool haveIdxMutex = false;

	if (retIdx.size() > 0 && appCommEnabled())// stevev 08apr10 bug 3034 subsequent error
	{
		hCitemBase* pIB ;
		hCinteger*  pII;
		CValueVarient vv;
		localIdxUse.clear();// we're about to reuse it
#ifdef _DEBUG
		retIdxSize = retIdx.size();
		acEnabled  = appCommEnabled();
#endif
/******INDEX MUTEX****/ pIndexMutex->aquireMutex(hCevent::FOREVER);// waits forever
		haveIdxMutex = true;
		
#ifdef LOG_INDEX_MUTEX
		LOGIT(CLOG_LOG,"Got Index Mutex: WeightOpt Line %d\n",__LINE__);
#endif
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====have Indexes & mutex ET: %5.3f ====\n",tmpET);
#endif
		// store  the current value, set a new value
		for (idxUseIT itRet = retIdx.begin(); itRet < retIdx.end(); itRet++)
		{
			pIdxUse = (hIndexUse_t*)&(*itRet);

			if (SUCCESS == devPtr()->getItemBySymNumber(pIdxUse->indexSymID,&pIB) && pIB != NULL)
			{
				localIdxUse.indexSymID     = pIdxUse->indexSymID;
				pII                        = (hCinteger*)pIB;
				localIdxUse.indexDispValue = pII->getDispValue(true);
				localIdxUse.indexWrtStatus = pII->getWriteStatus();
				pII->setWriteStatus(0);// set to NOT user written to avoid screen updations
				requestIndexList.push_back(localIdxUse);//entry values

				if ( pII->IsLocal() )
				{	
					vv = (unsigned)(pIdxUse->indexDispValue);
					pII->setDispValue(vv, true);	// set one to the selected value
					pII->ApplyIt();				    // copy it to the other half
					pII->markItemState(IDS_CACHED); // force it good
				}
				else
				{
					LOGIT(CLOG_LOG," Info & Index Var NOT Set (is NOT Local) 0x%04x ",
																		pIdxUse->indexSymID);
				}
				localIdxUse.clear();
			}// not much we can do without an item pointer
		}// next return index
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====Indexes set ET: %5.3f ====\n",tmpET);
#endif
	}
	else
	if (retIdx.size() > 0 )
	{// we have indexes we need to set but appComm is disabled and we can't..no use continuing here
		return -1;
	}
#ifdef _DEBUG  // hunting the change during run
	else
	{
		retIdxSize = retIdx.size();
		acEnabled  = appCommEnabled();
#ifdef WGT_DEBUG
		if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
		{
			GET_SPLIT(loopTime,tmpET);
		   LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   **** Cmd %d No Indexes ST: %5.3f ***\n", cmdNum, tmpET);
		}
		else
			LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ==== No Indexes ====\n",tmpET);
#endif
	}
#endif
	// always re-resolve list
	RslvdRplyItemList.clear();
	if(isRead)
	{
		rc = pThisTrans->getReplyDIlist(&RslvdRplyItemList);
	}
	else // isWrite
	{
		rc = pThisTrans->getReqstDIlist(&RslvdRplyItemList);
	}
	if ( rc != SUCCESS )
	{
		LOGIT(CLOG_LOG,"Indexed List did not re-resolve.\n");
	}
#ifdef MEM_DEBUG
    chkPt04MemState.Checkpoint();
    if( diffMemState.Difference(chkPt02MemState , chkPt04MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** 06 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif
	reqstlistValid =		// we'll reduce by invalids...just in case it isRead
	reqstListSize  = RslvdRplyItemList.size();// stevev 2feb09

#ifdef WGT_DEBUG
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{
		GET_SPLIT(loopTime,tmpET);
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ***cmd %d enter loop ST: %5.3f ***\n", cmdNum, tmpET);
	}
#endif	
	// weigh the reply 4 read or request 4 write
	for ( hCdataitemList::iterator  iT = RslvdRplyItemList.begin(); 
					iT < RslvdRplyItemList.end() && appCommEnabled();  iT++)
					// stevev 08apr10 bug 3034 subsequent error, allow exit
	{// iT is a ptr 2 a hCdataitem	   
		pDataItemVar = iT->getVarPtr();	
#ifdef _DEBUG
		itemID_t thisItmID = 0;
		if (pDataItemVar) // null on constant in packet
			thisItmID = pDataItemVar->getID();
#endif
		if(pDataItemVar && isRead && (! pDataItemVar->IsValid()) )// isRead  AND Not Valid
		{// this is the reply packet with an invalid item
			reqstlistValid --;
		}

		/* stevev 12mar07 - we need to deal with the unsent write due to validity*/
		if(pDataItemVar && (! isRead) && (! pDataItemVar->IsValid()) //isWrite AND Not Valid
			       &&  (compatability() != dm_275compatible) )// and we ain't being lenient
		{// this won't be sent
			weight -= 50000;
			continue;
		}
		/** end 12mar07 **/
		if ( (iT->flags & cmdDataItemFlg_Info) != 0 ||   // it is an info data Item
			  pDataItemVar == NULL                  ||
			! pDataItemVar->IsValid()               ||// stevev 23mar09 bug2596..added a couple )
			pDataItemVar->getID() == RESPONSECODE_SYMID ||
			pDataItemVar->getID() == DEVICESTATUS_SYMID )// stevev - skip weighing these
		{											// includes index and info weighed above
			continue;// skip it (add zero weight)
		}

		int tmpWgt = 0;

		tDS = pDataItemVar->getDataState();	

#ifdef WGT_DEBUG
		cnt++;
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{
		GET_SPLIT(loopTime,tmpET);
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ***cmd %d, %d adding weight. ST: %5.3f ***\n", 
																		cmdNum, cnt, tmpET);
	}
#endif	
		if (pDataItemVar->getDataStatus() == 0)// weigh if not zero < when reading only
		{// do not weigh this item -  it has been marked as a problem variable
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   Read Var 0x%04x Has a BAD data status (mis-behaved)\n",
																		pDataItemVar->getID());
#endif
		}
		else
		if ( isRead && tDS == IDS_STALE )
		{
			tmpWgt = WGT_FOR_STALE;			
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for stale.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_STALE,tmpWgt);
#endif				
		}
		else
		if ( isRead && tDS == IDS_UNINITIALIZED && ! isDownloadMode() )//downloadMode described
		//                                                              at the top of this file
		{
			tmpWgt = WGT_FOR_UNINIT;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for uninit.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_UNINIT,tmpWgt);
#endif				
		}
		else
		if ( isRead && tDS == IDS_INVALID&& ! isDownloadMode() )
		{
			tmpWgt = WGT_FOR_INVALID;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for invalid.(%d)\n",
												pDataItemVar->getID(),WGT_FOR_INVALID,tmpWgt);
#endif					
		}
		else
		if ((!isRead) && tDS == IDS_NEEDS_WRITE)
		{
			tmpWgt = WGT_FOR_WRITE;		
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for needs write.(%d)\n",
													pDataItemVar->getID(),WGT_FOR_WRITE,tmpWgt);
#endif											
		}
		else
		{	tmpWgt = 0;
#ifdef WGT_DEBUG	/* defined at the top of this file */
			if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds zero.(%d)\n",
																pDataItemVar->getID(),tmpWgt);
#endif											
		}	
		int qcnt = (pDataItemVar->getQueryCnt() + 1);
#ifdef WGT_DEBUG	/* defined at the top of this file */
		if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
			LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x multiplies %d by query %d to get: %d\n",
			//		pDataItemVar->getID(),tmpWgt,(pDataItemVar->getQueryCnt() + 1),
			//		(tmpWgt*(pDataItemVar->getQueryCnt() + 1))  );
			pDataItemVar->getID(),tmpWgt,qcnt*qcnt,(tmpWgt*(qcnt*qcnt))  );
#endif									
		tmpWgt *= (qcnt * qcnt);

#ifdef WGT_DEBUG
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{
		GET_SPLIT(loopTime,tmpET);
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ***cmd %d, %d checking native. ST: %5.3f ***\n", 
																			cmdNum, cnt,tmpET);
	}
#endif	

		if (tmpWgt)
		{
			hCcommandDescriptor thisCmdDesc;
			if (isRead)
			{
				thisCmdDesc = pDataItemVar->getRdCmd();
				cmdInfo     = pDataItemVar->getRdCmd(cmdNum);
			}
			else
			{
				thisCmdDesc = pDataItemVar->getWrCmd();
				cmdInfo     = pDataItemVar->getWrCmd(cmdNum);
			} 
			if (cmdInfo.cmdNumber < 0xffff)
			{
				weight+=tmpWgt;// will resolve a variable
			}// else invalid cmd number

			if (cmdInfo.cmdNumber == thisCmdDesc.cmdNumber)
			{
#ifdef WGT_DEBUG	/* defined at the top of this file */
				if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
					LOGIF(LOGP_WEIGHT)(CLOG_LOG,"    Var 0x%04x adds %d for command match.\n",
													pDataItemVar->getID(),WGT_FOR_CMD_MATCH);
#endif
				weight+=WGT_FOR_CMD_MATCH;// skew toward selected commands
			}
		}//no weight so far, we shouldn't add more
#ifdef WGT_DEBUG
	if ( cmdNum == WGT_CMD  || cmdNum == WGT_CMD_ALT )
	{
		GET_SPLIT(loopTime,tmpET);
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ***cmd %d, %d loop for next. ST: %5.3f ***\n", 
																		cmdNum, cnt,tmpET);
	}
#endif	
	}// next packet var
	if ( weight > 0 ) // no reason to give weight to a weightless command
	{
		weight += idxWgt; // mess with the request weight ( zero on write cmd )
	}

#ifdef MEM_DEBUG
    chkPt03MemState.Checkpoint();
    if( diffMemState.Difference(chkPt02MemState , chkPt03MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			TRACE(_T("** 02-2-03 **\n"));
			diffMemState.DumpStatistics();
		}
    }
#endif
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ===have weight ET: %5.3f "
								"(searched %d dataitems %5.4f ea, cmd %d )==\n",
											tmpET,reqstListSize, (tmpET/reqstListSize),cmdNum);
#endif

	/* stevev 21oct09 - this needs to be done while the indexes are still in place        */
	/* stevev  2feb09 - handle the invalid rules for the reply packet (request done above)*/
	if ( ! isRead && appCommEnabled()) // we have to deal with the isWrite reply
	{                // stevev 08apr10 bug 3034 subsequent error, allow exit
		RslvdRqstItemList.clear(); // J.U. 28.04.11
		rc = pThisTrans->getReplyDIlist(&RslvdRqstItemList);// put reply into an empty list
		replyListValid =		// we'll reduce by invalids...just in case it isRead
		replyListSize  = RslvdRqstItemList.size();// stevev 2feb09

		for (hCdataitemList::iterator  iT = RslvdRqstItemList.begin(); 
														iT < RslvdRqstItemList.end() ;  iT++)
		{//  reduce by each invalid value
			pDataItemVar = iT->getVarPtr();
			if (pDataItemVar != NULL && ! pDataItemVar->IsValid() )
			{
				replyListValid --;
			}
		}
	}
	//else the isRead reply is done

	/////  if (retIdx.size() > 0 && appCommEnabled())// stevev 08apr10 bug 3034 subsequent error
	if (retIdx.size() > 0 && haveIdxMutex)// stevev 23sep10 stop mee multitasking from losing index
	{		
		hCitemBase* pIB ;
		hCinteger*  pII;
		CValueVarient vv;
		// restore  the current value
		for (idxUseIT itRet = requestIndexList.begin(); itRet < requestIndexList.end(); itRet++)
		{
			if ( SUCCESS == devPtr()->getItemBySymNumber(itRet->indexSymID,&pIB) && pIB != NULL )				
			{
				pII                  = (hCinteger*)pIB;
				if ( pII->IsLocal() )
				{	
				//LOGIT(CLOG_LOG," Idx 0x%04x to %d ",itRet->indexSymID,itRet->indexDispValue);
					vv = (unsigned)(itRet->indexDispValue);
					pII->setDispValue(vv, true);	// set one
					pII->ApplyIt();				    // copy it to the other
					pII->markItemState(IDS_CACHED); // force it good
					pII->setWriteStatus(itRet->indexWrtStatus);// put it back like we found it
				}
				//else - NOT-Local...was logged when we stored it, don't do it again
			}// not much we can do
		}
/******INDEX MUTEX****/ pIndexMutex->relinquishMutex();
	}
	else
	{
#ifdef _DEBUG
		int anIdxSize = retIdx.size();
		bool acEnbld  = appCommEnabled();

		if ( pIndexMutex->pParent->HolderThdID == pIndexMutex->pParent->fetchCurrentThreadId() )
		{// this should never happen
			LOGIT(CERR_LOG|CLOG_LOG,"exiting optimized calcweight while this thread has the index mutex (Debug only msg)\n");
		}
#endif
	}
	requestIndexList.clear();
#ifdef WGT_DEBUG
	GETELAPSED(loopTime,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ====Reset Indexes & gave up Mutex ET: %5.3f ====\n",tmpET);
#endif


	// the reply rule is: if all in the reply packet are invalid, the command is not sent
	// but response code and device status don't count for this.  I assume that 100% of
	// tokenized reply packets have response-code & device-status variables.
	if ((!isRead && replyListValid <= 2 && replyListSize > 2 ) || //write reply pkt check 
		( isRead && reqstlistValid <= 2 && reqstListSize > 2 ) )  // read reply pkt check
	{// none valid & not a command-command
		// invalid packet
#ifdef WGT_DEBUG
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"ERROR: calcWeight exit on Reply Validity error.\n");
#endif
		RslvdRplyItemList.clear();// in case some got filled
		RslvdRqstItemList.clear();
		return -1;	// abort execution with very low weight
	}
	/* stevev end 2feb09 **/

	RslvdRplyItemList.clear();// before destruction
	RslvdRqstItemList.clear();

#ifdef WGT_DEBUG	/* defined at the top of this file */
	if (weight > 0 || (cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT) )
	{
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"      ** Cmd %03d Trans %d (%03d)\n",cmdNum,trnNum,weight);
	}
	else
	{
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"      ** Cmd %03d Trans %d has Zero Weight\n",cmdNum,trnNum);
	}
#endif
#ifdef MEM_DEBUG
    chkPt05MemState.Checkpoint();
    if( diffMemState.Difference( entryMemState, chkPt05MemState ) )
    {
//		oldMemState.DumpAllObjectsSince();
	//	if (diffMemState.m_lSizes[1] > 100000 )
		if (diffMemState.m_lTotalCount > 10000000 )
		{
			TRACE(_T("** entry -2-05 total diff **\n"));
			diffMemState.DumpStatistics();
		
			if( diffMemState.Difference( chkPt03MemState, chkPt05MemState ) )
			{
		//		oldMemState.DumpAllObjectsSince();
//				if (diffMemState.m_lSizes[1] > 100000 )
				if (diffMemState.m_lTotalCount > 10000000 )
				{
					TRACE(_T("** 05-2-03 last section diff **\n"));
					diffMemState.DumpStatistics();
				}
			}


		}
    }
#endif
#ifdef WGT_DEBUG	/* defined at the top of this file */
	if ( cmdNum == WGT_CMD || cmdNum == WGT_CMD_ALT )
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,":::::::::: Exit w/ C %d  T %d Weight %d ::::::::::\n",
																		cmdNum,trnNum,weight);
	for ( idxUseIT itxdmp = retIdx.begin(); itxdmp < retIdx.end(); itxdmp++)
	{// for each index to be returned
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Return Index:  ");
		itxdmp->clogSelf();
	}

	GETELAPSED(lastTimeBuffer,tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"   ==== Leaving, total ET: %5.3f ====\n",tmpET);
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"----------------- %I64d.%03hd  Exit Transaction Scale.\n",
					lastTimeBuffer.time,lastTimeBuffer.millitm);
	isOfInterest = false;
#endif
#ifdef _DEBUG
	int gh = retIdx.size();
#endif
	return weight;
	

/************************************************************/


	}// end try
	catch(...)
	{
		LOGIT(CERR_LOG|CLOG_LOG,"Calc weight optimized inside catch(...)\n");
		weight = WGT_FOR_STALE;
		pIndexMutex->relinquishMutex();
	}// end catch
	return weight;
}


/*   command dispatch weighing helper function  
 *
 *   weighs the variables each combination of indexes would resolve
 *   returns the best combination
 *   recursively called
 */
RETURNCODE hCcmdDispatcher::
findBestIdxList(indexUseList_t& returnIndexes, IdxVal2VarList_t& IdxNVarList, bool isRead,
																unsigned cNum, unsigned tNum)
{
	RETURNCODE rc = SUCCESS;

	IdxVal2VarList_t       cpyIdxNVarList(IdxNVarList);
	hCIndexValue2Variable  OurIndex; 
#ifdef WGT_DEBUG
		if (isOfInterest)
		{
			LOGIF(LOGP_WEIGHT)(CLOG_LOG,"        Find Best Index: entry w/ %d in Idx List\n",
														IdxNVarList.size());
		}
#endif

	if (cpyIdxNVarList.size() <= 0)
	{
		returnIndexes.clear();
		return SUCCESS;// we have the best combination of indexes (none)
	}
	else
	{	// get last element for use later
		OurIndex = (hCIndexValue2Variable)(IdxNVarList.back());
		cpyIdxNVarList.pop_back();// removes last element we just got
		IdxNVarList.clear();// we don't need those anymore, we'll use the copy

		if ( cpyIdxNVarList.size() > 0 )// we have yet to reach bottom
		{// curse again  - we only handle our index, the recursive versions handle the others
			rc = findBestIdxList(returnIndexes, cpyIdxNVarList, isRead, cNum, tNum);
			if ( rc != SUCCESS )
				return rc;
			// else
			// we have a list of indexes in return indexes 
			// and a list of values and variables resolved in the returned varptrlist
			IdxNVarList = cpyIdxNVarList; //put in the returned-so-far
		}
		//else there is only one index to do (pOurIndex)
	}// end else there's var ptrs

	// we are doing OurIndex without re-resolving anything in IdxNVarList

	int                       tmpWgt,  varValWgt      = 0 ;
	hCVar*                    *pV                     = NULL;
	int                       maxIdxWeight            = -50000; // everything will be bigger
	hIndexUse_t               localIdxUse;
	hCitemBase*               pDevIdxItmBase          = NULL;
	unsigned                  localIdxValue           = 0;
	unsigned                  devicIdxValue           = 0;
	unsigned                  maxIdxValue             = 0;// index valid value for max weight
	valueNVarsIT_t            VnViT;
	hCIndexValueAndVariables *pIVnV = NULL, *maxIVnV  = NULL;
	varPtrLstIT_t             vPtrLstIt;
	INSTANCE_DATA_STATE_T     tDS;
	varPtrList_t              tempVarPtrs, maxVarPtrs;

	hCIndexValue2Variable     retV2V; // 2 b pushed on IdxNVarList 4 return
	hCIndexValueAndVariables  retVnV; // 2 b pushed on retV2V

	localIdxUse.indexSymID = OurIndex.getID();
	for( VnViT = OurIndex.begin(); VnViT != OurIndex.end(); ++VnViT)
	{// for each valid value of our index
		pIVnV = (hCIndexValueAndVariables*)&(*VnViT);
		localIdxValue = pIVnV->indexValue;	// this valid value
		devicIdxValue = pIVnV->devicValue;
		varValWgt  = 0;// total for each valid value (could resolve multiple temps)
		tempVarPtrs.clear(); // clear the resolved variable list
		CValueVarient idxValue;
		if ( pIVnV->deviceIdxID > 0 && pIVnV->deviceIdxID != -1 )
		{
			if ( (rc = devPtr()->getItemBySymNumber(pIVnV->deviceIdxID, &pDevIdxItmBase)) 
				     == SUCCESS 	&& pDevIdxItmBase != NULL  )
			{
				if (pDevIdxItmBase->IsVariable())
				{
					idxValue = ((hCVar*)pDevIdxItmBase)->getRealValue();
					if (devicIdxValue != (unsigned)idxValue)
					{// this is unusable, discard and keep looking
#ifdef WGT_DEBUG
	if (isOfInterest)
	{
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,
		"                                     Indx 0x%04x [%2d] not considered due to Device Val = %d.\n", 
											pDevIdxItmBase->getID(),devicIdxValue,(int)idxValue);
	}
#endif
						continue;
					}// else it matches, use this one
				}
				else
				{
					LOGIT(CERR_LOG,"ERROR: Index 0x%04x is not type VARIABLE.\n",
																pIVnV->deviceIdxID);
					continue;// discard this one
				}
			}
			else // we have an error
			{
				DEBUGLOG(CLOG_LOG," ERROR: device index 0x%04x not found in device.\n",
																pIVnV->deviceIdxID);
					continue;// discard this one
			}

		}
#ifdef WGT_DEBUG
		if (isOfInterest)
		{
		LOGIF(LOGP_WEIGHT)(CLOG_LOG,"        Find Best Index: 0x%04x [%2d] resolves %d vars",
						localIdxUse.indexSymID,localIdxValue,pIVnV->resolvedVarList.size());
			if (pIVnV->deviceIdxID > 0 && pIVnV->deviceIdxID != -1)
			{
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Dev: 0x%04x [%2d] ",
				//						localIdxUse.devIdxSymID,localIdxUse.devVarRequired);
										pIVnV->deviceIdxID, pIVnV->devicValue);
			}
			else
			{
				LOGIF(LOGP_WEIGHT)(CLOG_LOG,"                  ");
			}
		}
#endif
		hCcommandDescriptor matchedCD;
		for ( vPtrLstIt  = pIVnV->resolvedVarList.begin(); 
			  vPtrLstIt != pIVnV->resolvedVarList.end(); 
			  ++vPtrLstIt)
		{// for each variable resolved by this index&value
			hCVar* pV = (hCVar *)(*vPtrLstIt);// I is ptr2ptr2hCVar
			tmpWgt    = 0;

			if (pV != NULL && notInList(pV->getID(), IdxNVarList) )
			{// it exists and hasn't been resolved already
				matchedCD.clear();
				if ( isRead )
				{
					matchedCD = pV->getRdCmd(cNum,tNum,localIdxUse.indexSymID);
				}
				else
				{
					matchedCD = pV->getWrCmd(cNum,tNum,localIdxUse.indexSymID);
				}

				if ( matchedCD.cmdTyp == cmdOpNone || matchedCD.cmdNumber < 0 )
				{
					DEBUGLOG(CLOG_LOG,"BestIndex: did not find a command descriptor.\n");
				}
				else
				{
#ifdef WGT_DEBUG
		if (isOfInterest)
		{
			LOGIF(LOGP_WEIGHT)(CLOG_LOG," %#04x; ", pV->getID());
			if (localIdxUse.devIdxSymID != 0)
			{
				//LOGIF(LOGP_WEIGHT)(CLOG_LOG,"Dev: 0x%04x [%2d] ",
				//									 localIdxUse.devIdxSymID,devVarRequired);
			}
		}
#endif

				if ( matchedCD.idxList.size() > 1 )
				{// multiple indexed entry
					bool hasLocal = false;
					bool isUseful = true;
					for (idxUseIT iuIT  = matchedCD.idxList.begin(); 
								  iuIT != matchedCD.idxList.end();   ++iuIT)
					{	hIndexUse_t *pIu = (hIndexUse_t *) &(*iuIT);
						hCitemBase* pIdxVar;
						if ( devPtr()->getItemBySymNumber(pIu->indexSymID,&pIdxVar) != SUCCESS 
						 ||	 pIdxVar == NULL)
						{
							DEBUGLOG(CLOG_LOG,"BestIndex: Multiple Index: 0x%04x not found\n",
								pIu->indexSymID);
						}// just skip it
						else
						{
							if ( ((hCVar*)pIdxVar)->IsLocal() )
							{
				/* no longer useful
								if (hasLocal)
								{
									DEBUGLOG(CLOG_LOG|CERR_LOG,"ERROR: Index: 0x%04x is also "
										"Local\n",((hCVar*)pIdxVar)->getID());
									isUseful = false;// don't weigh it
									break; // out of for loop
								}
								else
								{
									hasLocal = true;
									
									DEBUGLOG(CLOG_LOG|CERR_LOG,"Index: 0x%04x is Local.\n",((hCVar*)pIdxVar)->getID());

								}
				***/
							}
							else // non local
							if (!  (((hCVar*)pIdxVar)->getDispValue() == pIu->indexRealValue) )
							{//if actual value != current value then not useful
#ifdef WGT_DEBUG
				LOGIT(CLOG_LOG,"BestIndex: Multiple Index: 0x%04x is: %u  needs: %u\n",
				((hCVar*)pIdxVar)->getID(), 
				(unsigned)((hCVar*)pIdxVar)->getDispValue(), (unsigned)pIu->indexRealValue);
#endif
								isUseful = false;
								break; // abort index loop
							}
							else // non local and device index matches
							{//if actual value== current value then  useful
#ifdef WGT_DEBUG
				LOGIT(CLOG_LOG,"BestIndex: Multiple Index: 0x%04x is: %u  matches: %u\n",
				((hCVar*)pIdxVar)->getID(), 
				(unsigned)((hCVar*)pIdxVar)->getDispValue(), (unsigned)pIu->indexRealValue);
#endif
							}
						}
					}// next index
					if ( ! isUseful )
					{
#ifdef WGT_DEBUG
				if (isOfInterest)
				{
					LOGIF(LOGP_WEIGHT)(CLOG_LOG," 0x%04x shown UNuseful; ", pV->getID());
				}
#endif
						continue; // no use in weighing it, it can't be reached
					}
				}
				// else it a normal non-indexed or single indexed entry - no-op
				}

/*******************	*****	generate a weight */
				tDS = pV->getDataState();
				//=============== J.U. 28.04.11
				if (!pV->IsValid())
				{// do not bump the query count count on this one
					continue;
				}
				else
				//=============== J.U. 28.04.11
				if (pV->getDataStatus() == 0)// weigh if not zero
				{// do not weigh this item - it has been marked as a problem var
					continue;// do nothing
				}
				else
				if (tDS == IDS_STALE )
				{
					tmpWgt = WGT_FOR_STALE;							
				}
				else
				if (tDS == IDS_UNINITIALIZED)
				{
					tmpWgt = WGT_FOR_UNINIT;							
				}
				else
				if (tDS == IDS_INVALID)
				{
					tmpWgt = WGT_FOR_INVALID;							
				}
				else
				if ( ( ! isRead) && tDS == IDS_NEEDS_WRITE)
				{
					tmpWgt = WGT_FOR_WRITE;							
				}
				else // IDS_CACHED,IDS_PENDING,
				{
					tmpWgt = 0;
				}
				int qcnt = (pV->getQueryCnt() + 1);
				tmpWgt *= (qcnt*qcnt);
/*******************			end generate weight */
				varValWgt += tmpWgt;// add this value-variable to total							
			}// else a bad item id, skip it and keep going
		}// next variable resolved by this value
		// varValWgt - total weight for localIdxValue resolving tempVarPtrs
		if (maxIdxWeight < varValWgt)
		{
			maxIVnV      = pIVnV;
			maxIdxWeight = varValWgt;
			maxIdxValue  = localIdxValue;
		}

#ifdef WGT_DEBUG
		if (isOfInterest)
		{
			LOGIF(LOGP_WEIGHT)(CLOG_LOG," Wgt: %4d\n",varValWgt);
		}
#endif
	}// next valid value for our index
	// we have a set of variables for this index

	// stevev 13mar14 ... or not - add null test
	if(maxIVnV != NULL)
	{
	  //localIdxUse.indexSymID was set before the loop
		localIdxUse.indexDispValue = maxIdxValue;
		returnIndexes.push_back(localIdxUse);
		retVnV = *maxIVnV;// operator = to fill the return copy
		retV2V.push_back(retVnV);
		retV2V.setID(localIdxUse.indexSymID);
		IdxNVarList.push_back(retV2V);  //hCIndexValue2Variable*
	}
	else
	{
		DEBUGLOG(CLOG_LOG,"Cmd %d, Trans %d (%s): null index ptr.\n",
													cNum,tNum,(isRead)?"Reading":"Writing");
	}

	retV2V.clear();
	retVnV.clear();
#ifdef WGT_DEBUG
		if (isOfInterest)
		{
			LOGIF(LOGP_WEIGHT)(CLOG_LOG,"        Find Best Index: exit w/ %d indexes & %d. \n",
													returnIndexes.size(),IdxNVarList.size());
		}
#endif

	return rc;
}


bool hCcmdDispatcher:: // true if varID not in resolved list(s)
notInList(itemID_t varID, IdxVal2VarList_t& IdxValVarList)
{
	IdxVal2VarList_IT_t plIT;

	for ( plIT = IdxValVarList.begin(); plIT != IdxValVarList.end(); ++plIT)
	{// for each index 
		hCIndexValue2Variable* pIV2V = (hCIndexValue2Variable*) &(*plIT);
		// there should only be one value for this resolved index
		if (pIV2V->size() > 1)
		{
			LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: %d values found in resolved list.\n",pIV2V->size());
		}
		hCIndexValueAndVariables *pIVVs = (hCIndexValueAndVariables *)(&(*(pIV2V->begin())));
		varPtrList_t resolvedVarList;// iterator is varPtrLstIT_t
		for ( varPtrLstIT_t pVit = pIVVs->resolvedVarList.begin();
		                    pVit != pIVVs->resolvedVarList.end();    ++pVit )
		{// for each resolved variable..usually only one
			hCVar* pV = (hCVar *)(*pVit);// pVit is ptr2ptr2hCVar
			if (pV->getID() == varID)
			{
				return false;// it only takes one In the list
			}
		}// next resolved variable
	}// next index

	return true;// NOT in the list(s)
}


#if 0	/* method totally rewritten 3/25/04 */
/* stevev 16feb05 - remove the replaced code (look in previous versions if you need it)*/
#endif /* stevev 3/25/04 rewrite */

void  hCcmdDispatcher::commAsyncTask(waitStatus_t isTimerExpired)
{
	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Dispatch RC task entry with %d in the deque.\n ",msgDeque.size());
	// we don't need to clear anything in this scenario, let it fall thru the while loop
	if (msgDeque.size() == 0)
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"         Big error.");
		if(isTimerExpired == evt_timeout)
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"  Timer Expired. With an empty queue.\n");
		}
		else
		if(isTimerExpired == evt_fired)
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"  Signaled Event. With an empty queue.\n");
		}
		else // death
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"  Last Call. With an empty queue.\n");
		}
	}
	//hMsgCycle_t wrkCycle;

	// for each msgcycle in the queue
	while (msgDeque.size() > 0 && dispatchEnabled) // normally only one
	{
		/*DEEPAK : 230404*/
		hMsgCycle_t wrkCycle;
		/*DEEPAK : 230404*/

		wrkCycle = msgDeque.front();	// gets it
		msgDeque.pop_front();			// removes it from the queue
/*Vibhor 120204: Start of Code*/

//was 	if (pCommInterface != NULL)/*so the callback was being called even for methods!!*/ 
		if ( pCommInterface != NULL && pCommInterface->appCommEnabled() &&
			(wrkCycle.actionsEn || ! wrkCycle.isBlking) &&
			dispatchEnabled)// not in shutdown mode
		{
			if (wrkCycle.pCmd)
			{
				LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," -- Notify -- CmdID: 0x%02x  RC: 0x%02x   DS: 0x%02x\n",
					  wrkCycle.pCmd->getID(), (int)wrkCycle.respCdVal,(int)wrkCycle.devStatVal);
			}
			else
			{
				LOGIF(LOGP_MISC_CMTS)(CLOG_LOG," -- empty MsgCycle retrieved from the queue -- \n");
			}
			pCommInterface->notifyRCDScmd48(wrkCycle);
		}
		// else - eat it
		wrkCycle.clear();

/*Vibhor 120204: End of Code*/

	}// check for any more on the queue
}

	

			
void hCcmdDispatcher::commIdleTask(waitStatus_t isTimerExpired)	
{
//	if ( (! isTimerExpired)      &&  dispatchEnabled && pCommInterface != NULL  )
	//   appCommEnabled does comm status & autoupdateDisabled
	if ( appCommEnabled()        && dispatchEnabled && 
		 pCommInterface != NULL  && isTimerExpired != evt_lastcall)
	{// not in shutdown mode
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"       Idle Entry.\n");
#endif
		if ( (idleRunCounter   % 2) == 0 )
		{
			ServiceWrites();
		}
		else
		if ( (idleRunCounter % 1)==0 )
		{
			ServiceReads();
		}
		idleRunCounter++;
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"       Idle Exit.\n");
#endif
	}
}

// returns NUL if only on out-of-memory!!!!
hCTransferChannel* hCcmdDispatcher::getPort( unsigned char portNum )//gets current or makes one
{
	if (openPorts[portNum] == NULL)
	{
		openPorts[portNum] = new hCTransferChannel(devPtr(),portNum);
	}
	return openPorts[portNum];
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *	helper function
 *	returns zero if not found, returns true 0xffffffff when found
 */
ulong hCcmdDispatcher::valueFromIntList(vector<ulong>& intList, ulong target)
{
	ulong ret = 0;// not found
	vector<ulong>::iterator i;
    if ( target < 0 )
	{
		LOGIT(CLOG_LOG,"Trying to look up an invalid index: 0x%04x\n",target);
		return 0xffffffff;
	}
	for ( i = intList.begin(); i < intList.end(); i++)
	{
		if ( *i == target )
		{
			ret = 0xffffffff;// found
			break;//out of for loop
		}
	}
	return ret;
}

/***********************************************************************************************
 *
 * start hCmsgCycleQueue class
 *
 ***********************************************************************************************/
void hCmsgCycleQueue::InitItem(hMsgCycle_t* pItem)
{
	pItem->clear();
	pItem->m_cmdOrginator = co_INITED;
}

void hCmsgCycleQueue::EmptyItem(hMsgCycle_t* pItem)
{
	pItem->clear();
}

int  hCmsgCycleQueue::PutObject2Que(hMsgCycle_t * pNewObject, bool toTail )
{	
	RETURNCODE rc   = SUCCESS;
	hMsgCycle_t *pM = NULL, *pT;

	if (pMtxCtrl==NULL || pNewObject == NULL || pNewObject->pCmd == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No mutex in message cycle queue.\n");
		rc = -1;// couldn't do it
	}
	else
	{
		int newCmd = pNewObject->pCmd->getCmdNumber();
		pMtxCtrl->aquireMutex(hCevent::FOREVER);
		if (loadDeque.size() < 1 )
		{
			pM = NULL;// the best kind of success
		}
		else
		{	
			pM = NULL;
			rc = APP_PROGRAMMER_ERROR;
			for (deque<hMsgCycle_t*>::iterator iT= loadDeque.begin();iT != loadDeque.end();++iT)
			{// iT is a ptr 2 a ptr 2 a hMsgCycle_t				
				pT = (hMsgCycle_t*)(*iT);
				if ( (pT) != NULL  && 
					 (pT)->pCmd != NULL &&
					 (pT)->pCmd->getCmdNumber() == newCmd &&
					 (pT)->chkSum == pNewObject->chkSum      )
				{				
					pM = pT;
					// there is a unique situation that when there is an error, the object
					//	may be in both the main queue and the MT queue - this makes it match
					//	100% of the time.
					if ( pM == pNewObject ) // when the pointers match
					{
						clog <<"Msg Queue error, main queue already has msg to be added.-deleting msgque and resubmitting."<<endl;
						iT = loadDeque.erase(iT);// remove from the middle
						//iT is now  the first element remaining beyond any elements removed
						iT--;// the loop will ++
						pM = NULL;
					}
					else
					{
						rc = SUCCESS;
						break;
					}
				}
#ifdef XXX_DEBUG
				else
				if ( (*iT) != NULL  && (*iT)->pCmd != NULL &&
					 (*iT)->pCmd->getCmdNumber() == newCmd &&
					 (*iT)->chkSum != pNewObject->chkSum      )
				{
					clog <<"Msg Queue info: main queue already has msg-cmd to be added.-Checksums do not match."<<endl;
				}
#endif
			}//next
		}
		pMtxCtrl->relinquishMutex();
		if ( pM != NULL ) // this command packet already exists in the queue
		{
			DEBUGLOG(CLOG_LOG,"   MsgCycle for cmd %d exists in the queue.\n",newCmd);
			rc = -3;// exists already
		}
		else
		{
			if (pNewObject->pCmd == NULL)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Null command pointer in message cycle to queue.\n");
			}
			rc = hCqueue<hMsgCycle_t>::PutObject2Que(pNewObject,toTail);
		}
	}
	return rc;
}

RETURNCODE hCmsgCycleQueue::getMsgByPkt(hPkt* pPkt, hMsgCycle_t** ppMsgCyc, int id)
{
	RETURNCODE rc = SUCCESS;
	hMsgCycle_t* pM = NULL;

	// Anil Merger Activity From FDM to HCF November 2005 -start
	//DEEPAK :170105
	if(pPkt ==NULL)
		return FAILURE;

	if(ppMsgCyc==NULL)
		return FAILURE;
	//END
	// Anil Merger Activity From FDM to HCF November 2005 -End

	if (pMtxCtrl==NULL)
	{
		*ppMsgCyc = NULL;
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No mutex in message cycle queue.\n");
		rc = APP_PROGRAMMER_ERROR;
	}
	else
	{
		pMtxCtrl->aquireMutex(hCevent::FOREVER);
		if (loadDeque.size() < 1 )
		{
			*ppMsgCyc = NULL;
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: No items in message cycle queue.\n");
			rc = APP_PROGRAMMER_ERROR;
		}
		else
		{	// try the easy one first
			pM = loadDeque.front();
		}	
		if(pM != NULL)
		{
			if (pM->pCmd == NULL)
			{
				clog <<" Null command pointer in front of message cycle queue."
					<<"(in getMsgByPkt)"<<endl;
				loadDeque.pop_front();
			}
			else
			//if ( pM->pCmd->getCmdNumber() == pPkt->cmdNum )
			if ( pM->signatureMatch(pPkt) )
			{// first one is the charm
				*ppMsgCyc = pM;
				loadDeque.pop_front();
#ifdef _DEBUG
#ifdef QDBG
		clog << "Q:" << thisQhndl << " dequed, added to RdMIA: "<< hex<<pM<<dec<<endl;
#endif
		struct identity myid = { pM, 3 };
		RdMIAList.push_back( myid );
#endif
				rc = SUCCESS;
			}
			else
			{// first one ain't it, scan for match
				*ppMsgCyc = NULL;
				rc = APP_PROGRAMMER_ERROR;
				for (deque<hMsgCycle_t*>::iterator iT= loadDeque.begin();iT<loadDeque.end();iT++)
				{// iT is a ptr 2 a ptr 2 a hMsgCycle_t
					//if ( (*iT)->pCmd && (*iT)->pCmd->getCmdNumber() == pPkt->cmdNum )
					if ( (*iT)->signatureMatch(pPkt) )
					{
						*ppMsgCyc = pM = (*iT);
#ifdef _DEBUG
 #ifdef QDBG
		clog << "Q:" << thisQhndl << " dequed, added to RdMIA: "<< hex<<pM<<dec<<endl;
 #endif
		struct identity mrid = { pM, 6 };
		RdMIAList.push_back( mrid );
#endif
						loadDeque.erase(iT);// pop from position
#ifdef QDBG
		clog << "Q:" << thisQhndl << " post erase capacity: "<< loadDeque.max_size()<<endl;
#endif
						rc = SUCCESS;
						break;
					}
				}//next
#ifdef _DEBUG
	if ( rc != SUCCESS )
	{
		LOGIT(CLOG_LOG,"  Cmd:0x%02x signature not found in 0x%02x waiting packets.\n",
					(int)pPkt->cmdNum, loadDeque.size());
	}
	else
	{
		LOGIT(CLOG_LOG,"  Cmd:0x%02x signature successfully found in 0x%02x waiting packets.\n",
					(int)pPkt->cmdNum, loadDeque.size());
	}
#endif
			}
		}// else the queque shows to be empty anyway...
		pMtxCtrl->relinquishMutex();
	}
	return rc;
}


/*bool hCmsgCycleQueue::signatureMatch(hPkt* pPkt, ucharList_t* pSig)
{
	if (pPkt == NULL || pSig == NULL 
		|| pSig->size() <= 0 
		|| pSig->size() > (pPkt->dataCnt-2))
	{
		return false;
	}
	int i = 2;
	for (ucharList_t::iterator iT = pSig->begin(); iT < pSig->end(); iT++, i++)
	{
		if ( pPkt->theData[i] != *iT )
		{
			return false;
		}
	}
	return true;// they all match
}
*/

bool hMsgCycle_t :: signatureMatch(hPkt* pPkt)
{	
	if (pPkt == NULL || pCmd == NULL)		
	{	return false;	}

	if ( pPkt->transactionID == transactionID )
	{
		return true;
	}
	else//                                                 needs to be signed
		if ( transSignature.size() <= 0 || (int)transSignature.size() > (int)(pPkt->dataCnt-2))	
	{	
		bool RT = (pCmd->getCmdNumber() == pPkt->cmdNum);
		if (RT)
			LOGIT(CLOG_LOG,"Signature: ID mismatch with command match\n");
		// return RT;
		return false; 
	}
	else
	if (pCmd->getCmdNumber() == pPkt->cmdNum) 
	{	
		int i = 2;
		// bug 1953 - in lenient mode with a singleton transaction, all we need is a command match
		if ( pCmd->compatability() == dm_275compatible && pCmd->getTransCnt() == 1 )
		{
			LOGIT(CLOG_LOG,"Signature: ID mismatch with compatable match\n");
			//return true;
			return false;
		}
		for (ucharList_t::iterator  iT = transSignature.begin(); 
									iT < transSignature.end(); iT++, i++)
		{	
			if ( pPkt->theData[i] != *iT )
			{
#ifdef _DEBUG
				LOGIT(CLOG_LOG,"  Cmd:0x%02x signature failed at byte %d. This is 0x%02x, "
					"Looking for 0x%02x\n",(int)pPkt->cmdNum, i, (int)(*iT), 
																 (int)(pPkt->theData[i]) );
#endif
				return false;
			}
		}
		LOGIT(CLOG_LOG,"Signature: ID mismatch with signature match\n");
		//return true;// they all match
		return false;
	}
	else 
	{ 
		return false;
	}
};

/*************************************************************************************************
 *  serviceTransferPacket - lowest level session and transport layer handling
 * 
 *************************************************************************************************
 */
// HOST_SIDE  message handler for Block Transfer capability...handling the reply packet
void hCcmdDispatcher::serviceTransferPacket(hPkt* pPkt, hCcommand* pCmd, int transNum)
{
	hCtransaction* pTran = pCmd->getTransactionByNumber(transNum);
	if (pTran == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Transaction %d NOT found in Cmd #%d\n",transNum, pPkt->cmdNum);
		return;
	}
	else // we have a transaction
	if (pCmd->getCmdNumber() == 111)
	{// SESSION HANDLING
		if ( pPkt->dataCnt < 7 )
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Cmd 111 Reply too Short\n");
		}
		else
		{//this is host so cmd 111,reply pkt:
			hCTransferChannel* pChannel = getPort( pPkt->theData[2] );// we do not own this memory

			if ( pChannel == NULL ) // we are out of memory
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Cmd 111 Could not allocate a channel.\n");
				return;
			}
			else
			if( ! pChannel->isSupported())
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Cmd 111 port %d is not supported.\n");
				return;
			}
			// else not null and supported
			int master_byte, slave_byte;

			pChannel->respCd          = pPkt->theData[0];
			pChannel->devStat         = pPkt->theData[1];
			
			pChannel->portNumber      = pPkt->theData[2];
			pChannel->funcCd          = pPkt->theData[3];
			pChannel->max_segment_len = pPkt->theData[4];// devices max length..the one we'll use
			master_byte               = (pPkt->theData[5]<<8) + (pPkt->theData[6]);
			slave_byte                = (pPkt->theData[7]<<8) + (pPkt->theData[8]);
		}
	}
	else// we have a transaction AND it is NOT cmd 111
	if (pCmd->getCmdNumber() == 112)//  be sure its 112
	{//TRANSPORT HANDLING
	// if we are starting, snapshot the data into a list of buffers of segment size
	//  deal with retry requests (lost packet in the middle of the transfer)
		hCTransferChannel* pChannel = getPort( pPkt->theData[2] );// we do not own this memory

		if ( pChannel == NULL ) // we are out of memory
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Cmd 112 Could not find the channel.\n");
			return;
		}
		else
		if( ! pChannel->isSupported())
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Cmd 112 port %d is not supported.\n");
			return;
		}
		// else not null and supported
		bufChunk_t    locChunk;

		pChannel->respCd          = pPkt->theData[0];
		pChannel->devStat         = pPkt->theData[1];
		
		// should already match  pChannel->portNumber      = pPkt->theData[2];
		pChannel->funcCd          = pPkt->theData[3];

		locChunk.len              = pPkt->theData[4];
		locChunk.masterStart      = (pPkt->theData[5]<<8) + (pPkt->theData[6]);
		locChunk.slaveStart       = (pPkt->theData[7]<<8) + (pPkt->theData[8]);

		unsigned x,y;
		for ( x = 0, y = 9; x < locChunk.len; x++, y++)
		{
			locChunk.data[x] = pPkt->theData[y];
		}

		if ( locChunk.masterStart != pChannel->masterSync )
		{
			LOGIF(LOGP_TRANSFER)(CLOG_LOG,"Transfer:masterStart mismatch %d reply,%d expected.\n"
				,locChunk.masterStart, pChannel->masterSync);
			// leave the count to be retransmitted
		}
		else
		{
			pChannel->recv_Buffer.push_back(locChunk);
			pChannel->masterSync += locChunk.len;
			if (pChannel->masterSync <= locChunk.len)
				pChannel->maserSyncRolloverCnt++;
		}
	}
	else// we have a transaction AND it is NOT cmd 111 and NOT cmd 112
	{// we have a transaction and the command isn't special (we shouldn't be here)
	//try to extract it.
	}//endelse we have a transaction
}// end serviceTransferPacket


void hCcmdDispatcher::serviceMsgCycleQue(int serviceType)// type 0 is clear it
{
	hPkt*           pPkt    = NULL;	
	hMsgCycle_t*    pMsgCyc = NULL;

	if ( serviceType != 0 )// all we support right now
		return;

	if (theMsgQueue.getMsgByPkt(pPkt, &pMsgCyc, 7) == SUCCESS)
	{
		if ( pMsgCyc != NULL )
		{
			if ( pMsgCyc->peDone != NULL )
			{
				hCevent::hCeventCntrl* pEC = pMsgCyc->peDone->getEventControl();
				if ( pEC != NULL )
				{
					pEC->triggerEvent();// trigger 'em to prevent pend forever
					delete pEC;
					RAZE(pMsgCyc->peDone);
				}// else skip it
			}// else mia
			theMsgQueue.ReturnMTObject( pMsgCyc );// always 
		}// else the msgcycle is null - an error we'll skip
	}// else - an error

}// end serviceMsgCycleQue


dispatchCondition_t hCcmdDispatcher::getCond(void)
{
	return dispatch_Status;
}
void hCcmdDispatcher::setCond(dispatchCondition_t x)
{
	dispatch_Status = (dispatchCondition_t)(dispatch_Status | (int)x);
}
void hCcmdDispatcher::clrCond(dispatchCondition_t y)
{
	dispatch_Status = (dispatchCondition_t) (dispatch_Status & (int)(~y));
}


  
/*************************************************************************************************
 * The Transfer Channel 
 ************************************************************************************************/
hCTransferChannel::hCTransferChannel(hCdeviceSrvc *p, unsigned char portNum):
		portNumber(portNum), max_segment_len(0), startMasterSync(0), masterSync(0),
		maserSyncRolloverCnt(0), startSlave_Sync(0), slave_Sync(0), slaveSyncRolloverCnt(0),
		activeRecvChunk(-1), recvState(ts_Undefined), activeXmitChunk(-1),xmitState(ts_Undefined),
		pDevice(p),  pPortSupport(0), Session_State(ss_Undefined)
{	
	RETURNCODE rc = SUCCESS;
	recv_Buffer.clear();
	xmit_Buffer.clear();

	string locStr;

	pPortSupport = newPortSupportClass( portNum, p , this);
	fillPointers();
};     
void hCTransferChannel::sendSession(void)// sending 111 for this channel
{//must handle max seg, .. just send ours
	max_segment_len =  pPortSupport->getMaxSegLen();
	// fill 'em and send the command
}

// this only makes a packet::>
void hCTransferChannel::replySession(hPkt* pSndPkt, hCtransaction* pTran)
{	// only on slave side
	if (max_segment_len > pPortSupport->getMaxSegLen())
		respCd = rc_8_UpdateErr;
	max_segment_len =
#if defined(__GNUC__)
			std::min(max_segment_len, pPortSupport->getMaxSegLen());
#else
			__min(max_segment_len, pPortSupport->getMaxSegLen());
#endif
			

	pPortSupport->receivedSession();// must set slave_Sync & rollover
	// fill 'em and send the command
	CValueVarient lV;
	
	lV = respCd;			((hCVar*)pRC)->setDisplayValue(lV); 
	lV = devStat;			((hCVar*)pDS)->setDisplayValue(lV); 
	lV = portNumber;		((hCVar*)pPN)->setDisplayValue(lV); 
	lV = funcCd;			((hCVar*)pFC)->setDisplayValue(lV); 
	lV = max_segment_len;	((hCVar*)pSMaxSeg)->setDisplayValue(lV); 
	lV = masterSync;		((hCVar*)pMBC)    ->setDisplayValue(lV); 
	lV = slave_Sync;		((hCVar*)pDBC)    ->setDisplayValue(lV); 

	//reply
	if ( pTran == NULL || pTran->generateReplyPkt( pSndPkt) != SUCCESS )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Transaction %d NO REPLY in Cmd %d\n",pTran->getTransNum(),
																				pSndPkt->cmdNum);
		pSndPkt->clear();
	}
	else
	{
		pSndPkt->cmdNum = 111;
	}
	// caller will do the sending
}

void hCTransferChannel::reset(void)
{	// call support reset, close
}

void hCTransferChannel::open(void)
{// call support open, open	
	startMasterSync		 = masterSync;
	maserSyncRolloverCnt = 0;
	Session_State        = ss_Opening;

	pPortSupport->open();// should handle the slave sync issues if required
}


void hCTransferChannel::close(void)
{// call support close, then close
	
	funcCd = tf_PortClosed;
	Session_State = ss_Closed;
	// clear up the value and memory issues
	respCd = devStat = max_segment_len = 0;
	startMasterSync  = masterSync = maserSyncRolloverCnt = 0;
	startSlave_Sync  = slave_Sync = slaveSyncRolloverCnt = 0;
	recv_Buffer.clear(); activeRecvChunk = -1;
	xmit_Buffer.clear(); activeXmitChunk = -1;
	recvState = xmitState = ts_Undefined;
}

bool hCTransferChannel::transferComplete(void) 
{// returns true if no more remains to transfer
	return true;
}

/************************************************************************************************/
// returns 0 success, non-zero reason code, -1 is failure,bi-error
// Re-entrant, the itemNumber == itemBeingTransfered only on the first entry
// Note that these are ALL for handling the Xmit buffer fill
	
int hCTransferChannel::fillPortBuffer(itemID_t itemNumber)
{
	hCitemBase* pItem = NULL;
	
	if (pDevice == NULL) 
		return -1;// big error

	if ( pDevice->getItemBySymNumber(itemNumber,&pItem) != SUCCESS || pItem == NULL)
	{
		return -1; //failure
	}

	return fillPortBuffer( pItem );
}

int hCTransferChannel::fillPortBuffer(hCitemBase* pItem)
{
	RETURNCODE rc = FAILURE;

	if (pDevice == NULL) 
		return -1;// big error

	if (activeXmitChunk == -1 && xmit_Buffer.size() == 0)
	{// very first entry - make sure there is one to work with
		bufChunk_t firstCnk;
		firstCnk.masterStart = startMasterSync;
		xmit_Buffer.push_back(firstCnk);
		activeXmitChunk = 0;
	}

	switch(pItem->getIType())
	{
	case iT_Variable:		//VARIABLE_ITYPE 	       1
		{// it could be a string that is a novel long.
			// we must be sure that there is enough memory to hold it
			rc = fillVar((hCVar*)pItem);
		}
		break;
	case iT_Menu:			//MENU_ITYPE               3
		{// data only items in a depth first transition
			rc = fillMenu((hCmenu*)pItem);
		}
		break;
	case iT_ItemArray:		//ITEM_ARRAY_ITYPE         9
		{// data only items in a depth first transition
			rc = fillIarr((hCitemArray*)pItem);
		}
		break;
	case iT_Collection:		//COLLECTION_ITYPE        10
		{// data only items in a depth first transition
			rc = fillColl((hCcollection*)pItem);
		}
		break;
	case iT_Array:			//ARRAY_ITYPE             15
		{// data only items in a depth first transition
			rc = fillVarr((hCarray*)pItem);
		}
		break;
	case iT_File:			//FILE_ITYPE			  20
		{// data only items in a depth first transition
			rc = fillFile((hCfile*)pItem);
		}
		break;
	case iT_List:			//LIST_ITYPE			  26
		{// data only items in a depth first transition
			rc = fillList((hClist*)pItem);
		}
		break;
	case iT_Blob:			//BLOB_ITYPE              29
		{// look it up, load it into memory, start transferring
			rc = fillBlob((hCblob*)pItem);
		}
		break;
	// no default, if the item is not data, quietly skip it
	}// endswitch
	return rc;
}

int hCTransferChannel::fillVar( hCVar* pVar )
{
	RETURNCODE rc = FAILURE;
	bufChunk_t firstCnk;
	BYTE* pC = NULL;
	if (! pVar->IsValid()) return -1;
	//BYTE* pByteCnt, BYTE** ppBuf, int& mask, length
	//rc = pVar->Add(pByteCnt,ppBuf, 0,length);	
	int Len = pVar->getSize();
	int Rem = max_segment_len - XCHUNK.len;//entire Xmit chunk, transfer[active]
	if ( Len > Rem )
	{// won't fit in the current chunk
		if (Len > max_segment_len)
		{// won't fit in any chunk
			if ( pVar->VariableType() != (int)vT_Ascii )
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: A non-ASCII variable is too long to transfer.\n");
				rc = FAILURE;
			}
			else
			{// it is ascii, we can break it up
				wstring lwStr = pVar->getDispValueString();
				string  lStr  = TStr2AStr(lwStr);// ascii is only allowed ascii chars
				int sL = lStr.size() + 1;
				BYTE* tempBuff = new unsigned char[sL];
				strncpy((char*)tempBuff,lStr.c_str(),sL);
				pC = &(tempBuff[0]);
				strncpy((char*)&(XCHUNK.data[XCHUNK.len]),(char*)pC,Rem);// fill that one
				delete[] tempBuff;
				XCHUNK.len += Rem;
				pC += Rem;// where the next one would start
				sL -= Rem;	
				while (sL > 0)
				{
					activeXmitChunk = xmit_Buffer.size();// the index of the next one added
					xmit_Buffer.push_back(firstCnk);
					if ( sL >= max_segment_len )
						Rem =  max_segment_len;
					else
						Rem = sL;
					strncpy((char*)&(XCHUNK.data[0]),(char*)pC,Rem);// fill that one
					XCHUNK.len = Rem;
					pC += Rem;
					sL -= Rem;	
				}//wend 
				// its in, we can go
				rc = SUCCESS;
			}//endelse it is ascii
		}
		else
		{// it'll fit in a new chunk
			activeXmitChunk = xmit_Buffer.size();// the index of the next one added
			xmit_Buffer.push_back(firstCnk);
			int V = 0; pC = &(XCHUNK.data[0]);// its a new chunk
			rc = pVar->Add(&(XCHUNK.len), &pC, 0, V);//bytecount and data are updated
		}
	}
	else
	{// it'll fit in what we have
		int V = 0; pC = &(XCHUNK.data[XCHUNK.len]);// its an existing chunk
		rc = pVar->Add(&(XCHUNK.len), &pC, 0, V);
	}
	return rc;
}
int hCTransferChannel::fillMenu( hCmenu* pMenu )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pMenu == NULL || ! pMenu->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid menu.\n");
		return COND_RESOLVED_TO_INVALID;
	}
	menuItemList_t localList;
	if ( pMenu->aquire( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Menu transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list
	menuItmLstIT_t milIt;

	for (milIt = localList.begin(); milIt != localList.end(); ++ milIt)
	{
		hCmenuItem* pMI = (hCmenuItem*)&(*milIt);
		if( pMI->getRef().resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillIarr( hCitemArray* pIarr )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pIarr == NULL || ! pIarr->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid reference array.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pIarr->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Reference Array transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillColl( hCcollection* pColl )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pColl == NULL || ! pColl->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid collection.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pColl->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Collection transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillVarr( hCarray* pVarr )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pVarr == NULL || ! pVarr->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid array.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	ddbItemList_t localList;
	if ( pVarr->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: List transfer failed to get an array.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	ddbItemLst_it giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCitemBase
		pItem = *((hCitemBase**)&(*giPIt));
		if( pItem != NULL && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillFile( hCfile* pFile )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pFile == NULL || ! pFile->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid file.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	groupItemPtrList_t localList;
	if ( pFile->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: File transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	groupItemPtrIterator_t giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCgroupItemDescriptor
		hCgroupItemDescriptor* pGI = *((hCgroupItemDescriptor**)&(*giPIt));
		if( pGI->getpRef()->resolveID( pItem, false ) == SUCCESS && 
			pItem != NULL  && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillList( hClist* pList )
{
	hCitemBase* pItem = NULL;
	RETURNCODE rc = SUCCESS;

	if (pList == NULL || ! pList->IsValidTest())
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: Trying to transfer an invalid list.\n");
		return COND_RESOLVED_TO_INVALID;
	}

	ddbItemList_t localList;
	if ( pList->getList( localList ) != SUCCESS || localList.size() < 1 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,"Error: List transfer failed to get a list.\n");
		return APP_RESOLUTION_ERROR;
	}// else keep processing the list

	ddbItemLst_it giPIt;

	for (giPIt = localList.begin(); giPIt != localList.end(); ++ giPIt)
	{//ptr2ptr2 hCitemBase
		pItem = *((hCitemBase**)&(*giPIt));
		if( pItem != NULL && pItem->IsValid() )
			rc = fillPortBuffer(pItem);
		//else - skip it and go to the next one
	}
	return SUCCESS;
}
int hCTransferChannel::fillBlob( hCblob* pBlob )
{// look it up, load it into memory, start transferring
	return FAILURE;
}

int hCTransferChannel::emtyPortBuffer(itemID_t itemNumber)
{// do all the above but backwards
	return FAILURE;
}
int hCTransferChannel::emtyPortBuffer(hCitemBase* pItem)
{// do all the above but backwards
	return FAILURE;
}
//////////////// helper functions //////////////////////////////////////////////

int hCTransferChannel::setupCmd111(transferFunc_e fc)
{	
	CValueVarient  locVal;

//  @ false (NULL) will execute fill
	if ( pPN != NULL )
	{//ptrs ready		
		locVal = portNumber;
		((hCVar*)pPN)->setRawDispValue(locVal);
	
		locVal = (unsigned char)fc;
		((hCVar*)pFC)->setRawDispValue(locVal);
	
		locVal = max_segment_len;
		((hCVar*)pMMaxSeg)->setRawDispValue(locVal);
		
		locVal = masterSync;
		((hCVar*)pMBC)->setRawDispValue(locVal);

		return BI_SUCCESS;
	}
	// else exit with error
	return BI_ERROR;
}


int hCTransferChannel::fillPointers(void)
{
	hCitemBase* pIB= NULL;

	if( pDevice->getItemBySymName(string(RESPONSECD_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pRC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICESTAT_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pDS = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(PORT_NUMBR_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pPN = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(FUNCTIONCD_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pFC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(MASTERMAXL_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pMMaxSeg = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICEMAXL_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pSMaxSeg = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(MASTERBYTC_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pMBC = (hCVar*)pIB;
	if( pDevice->getItemBySymName(string(DEVICEBYTC_STR),&pIB) != SUCCESS || pIB == NULL)
		return BI_ERROR;
	pDBC = (hCVar*)pIB;

	return BI_SUCCESS;
}

int hCTransferChannel::fillPortInfo(int cmdNo)
{
	CValueVarient  locVal;
	if ( cmdNo != 111 && cmdNo != 112 )// minor diffs between the two
		return BI_ERROR;
	if ( pPN != NULL )
	{//ptrs ready
		locVal = ((hCVar*)pPN)->getRawDispValue();// port_number,
		if ( ((int)locVal) != portNumber )
			return BI_ERROR;

		locVal = ((hCVar*)pRC)->getRawDispValue();//response
		respCd = (unsigned char)locVal;
		locVal = ((hCVar*)pFC)->getRawDispValue();// function
		funcCd = (unsigned char)locVal;
		locVal = ((hCVar*)pDS)->getRawDispValue();// device status
		devStat= (unsigned char)locVal;
		if (cmdNo == 111)
		{
			locVal = ((hCVar*)pSMaxSeg)->getRawDispValue();// slave max seg len
			if ( (unsigned char)locVal < max_segment_len)
			{
				max_segment_len = (unsigned char)locVal;
			}
		}
		// else, we don't care about the redundent byte count in 112
		locVal = ((hCVar*)pMBC)->getRawDispValue();//response
		masterSync = (unsigned char)locVal;
		locVal = ((hCVar*)pDBC)->getRawDispValue();// function
		slave_Sync = (unsigned char)locVal;

		return BI_SUCCESS;
	}
	return BI_ERROR;
}

void hCTransferChannel::uploadData(hPkt* pSndPkt, unsigned short massa)
{
	CValueVarient  locVal;

	if (massa != XCHUNK.masterStart)
	{	unsigned short a = 0, b = 0xffff, w = 0, x = 0;
		for (bufChunkIter_t iT = xmit_Buffer.begin(); iT != xmit_Buffer.end(); ++iT, x++ )
		{
			w = abs( ((int)massa) - ((int)(xmit_Buffer[x].masterStart)) );
			if ( w < b )
			{
				b = w;
				a = x;
			}
			if (w == 0)
			{
				a = x;
				break; // out of for loop
			}
		}
		activeXmitChunk = a;
	}
	int c = 4;

	pSndPkt->cmdNum = 112;
	pSndPkt->theData[0] = respCd;
	pSndPkt->theData[1] = 0;// status code
	pSndPkt->theData[2] = portNumber;
	pSndPkt->theData[3] = funcCd;
	pSndPkt->theData[c++] = XCHUNK.len;
	pSndPkt->theData[c++] = (XCHUNK.masterStart >> 8) & 0xff;
	pSndPkt->theData[c++] = (XCHUNK.masterStart >> 0) & 0xff;
	pSndPkt->theData[c++] = 0;
	pSndPkt->theData[c++] = 0;// slave address
	for ( unsigned y = 0; y < XCHUNK.len; y++)
	{
		pSndPkt->theData[c++] = XCHUNK.data[y];
	}
	pSndPkt->dataCnt = c;
	activeXmitChunk++;
	if (activeXmitChunk >= xmit_Buffer.size())
	{
		pSndPkt->theData[3] = tf_Ready2Close;
	}
}

void hCTransferChannel::downloadData(void)//doesn't return till its done
{ 
	RETURNCODE rc = FAILURE;
	bool  stillDownLding = true;
	hCcmdDispatcher* pCD = NULL;	
	pCD = ((hCddbDevice*)pDevice)->pCmdDispatch;
	if (pCD == NULL) return;

	if (pPortSupport->isSIM)	// slave side: data from host
	{// not currently implemented
	}
	else //host side: data from slave
	{
		CValueVarient  locVal;
		indexUseList_t mtLst;
		hCcommand*     pCmd = NULL;
		CCmdList* pCL = (CCmdList*)pDevice->getListPtr(iT_Command);
		if (pCL)
			pCmd = pCL->getCmdByNumber(112);
		if (pCmd == NULL) 
			stillDownLding = false;// unsupported - fall out
		// we may have to look in the SDC dd for this command...

		while ( stillDownLding )
		{
			if (pPN != NULL)
			{//ptrs ready		
				locVal = portNumber;
				((hCVar*)pPN)->setRawDispValue(locVal);
			
				locVal = (unsigned char)tf_Transfer;
				((hCVar*)pFC)->setRawDispValue(locVal);
				
				// byte count is zero in this transaction

				locVal = masterSync;
				((hCVar*)pMBC)->setRawDispValue(locVal);
			
				locVal = slave_Sync;
				((hCVar*)pDBC)->setRawDispValue(locVal);

				rc = SUCCESS;
			}
			else // exit with error
			{
				return;
			}

			rc = pCD->Send(pCmd, 2, true, bt_ISBLOCKING, NULL, mtLst,false,co_TRANSPORT );
			// doesn't go into the var...locVal = ((hCVar*)pFC)->getRawDispValue();//funcCd

			if (rc != SUCCESS || funcCd == tf_Ready2Close )
			{
				stillDownLding = false;
			}
		}//wend
	}// endelse data from slave
	return;
}

/*


RETURNCODE            
hCddbDevice::closePort(CxmtrTransferChannel* pCh )// remove from list & delete port
{
	if (pCh == NULL)  return FAILURE;

	transferItr_t ppCTC = portTransferList.find(pCh->portNumber);
	if ( ppCTC != portTransferList.end() )
	{//
		CxmtrTransferChannel* pLstVer = ppCTC->second;
		if ( pCh != pLstVer && pCh->portNumber == pLstVer->portNumber )
		{
			LOGIT(CERR_LOG,"ProgrammerError: Deleting a copy of port %d\n",pCh->portNumber);
		}
		else
		if (pCh->portNumber != pLstVer->portNumber)
		{
			LOGIT(CERR_LOG,"ProgrammerError: port number changed\n",pCh->portNumber);
		}
		else // numbers match & pointers match
		{
			if (pLstVer->myState != ts_Closed)
				DEBUGLOG(CLOG_LOG,"Deleting port %d in state %d\n",
															pLstVer->portNumber,pLstVer->myState);
			pLstVer->myState = ts_Undefined;
			pLstVer->transferBuffer.clear();
			delete pLstVer;
			portTransferList.erase(ppCTC);
			return SUCCESS;
		}
		
	}
	// else it doesn't exist to delete
	return FAILURE;
}
*******************/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Ports
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
//
// port 2 //
//
//device_configuration

hCRdDevConfig::hCRdDevConfig( hCdeviceSrvc * pD, hCTransferChannel* p): hCPort(pD, p)
{//device_configuration when isSIM
	isSIM = (pD->getPolicy().isReplyOnly)?true:false; 
};


void hCRdDevConfig::open(){};
void hCRdDevConfig::close(){};
unsigned char hCRdDevConfig::getMaxSegLen(void) { return MY_MAX_SEGMENT-200; };// small for testing
bool hCRdDevConfig::isSupported() { return true; };// must be overridden by a real port support


// command 112,    called  before sending (sim-reply, sdc-request)
int hCRdDevConfig::receivedTransport( )
{
	return SUCCESS;
}

// command 111, called right before sending (sim-reply, sdc-request)
int hCRdDevConfig::receivedSession()
{
	return SUCCESS;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * NOTES:
 *
 *  Transaction IDs: A transaction id is made up of two parts, the first (hi 4 bytes) is a semi
 *		unique id number for the application. It is generated from the serial number.  The second
 *		part (lo 4 bytes)is the tick-count from the OS.  This is the number of milliseconds since 
 *		the OS was last started.  It rolls over every 49.7 days. If your message turn-around time 
 *		is longer than that, you need to get another occupation ;-).
 *		It is made from two parts because, in the future, there could be many transactions going thru
 *		a comm server from many different copies/types of SDC hosts.  The chances of a collision are
 *		pretty remote.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
