# Microsoft Developer Studio Project File - Name="DevServices" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=DevServices - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DevServices.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DevServices.mak" CFG="DevServices - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DevServices - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "DevServices - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "DevServices - Win32 ReleaseWithSymbols" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DD Tools/DDB/ddbRead/ddbLib", NPLBAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DevServices - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\.." /I "..\..\..\common" /I "..\..\..\Apps\Appsupport\DDParser" /I "..\..\..\Apps\Appsupport\Interpreter" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Apps\Appsupport\HART_DE" /I "..\..\Device.Lib" /I "..\..\..\Apps\Appsupport\BuiltinLib" /I "..\..\..\Apps\Appsupport\MEE" /I "..\..\..\Device.Lib" /I "..\parserinfc" /D "DBG_PVFC" /D "NDEBUG" /D "IS_SDC" /D "XMTR" /D "WIN32" /D "_UNICODE" /D "_FULL_RULE_ENGINE" /U "_DEBUG" /FD /Zm900 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DevServices - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GR /GX /Zi /Od /I "..\.." /I "..\..\..\common" /I "..\Interpreter" /I "..\..\..\COMMON" /I "..\DEVSERVICES" /I "..\..\..\COMMON\RDWRCOMMON\inc" /I "..\Builtinlib" /I "..\MEE" /I "..\DDParser" /I "..\..\..\Device.Lib" /I "..\..\SDC625" /I "..\ParserInfc" /D "_DEBUG" /D "_LIB" /D "IS_SDC" /D "XMTR" /D "WIN32" /D "_UNICODE" /D "UNICODE" /D "_FULL_RULE_ENGINE" /Fr /FD /Zm200 /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "DevServices - Win32 ReleaseWithSymbols"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "DevServices___Win32_ReleaseWithSymbols"
# PROP BASE Intermediate_Dir "DevServices___Win32_ReleaseWithSymbols"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "DevServices___Win32_ReleaseWithSymbols"
# PROP Intermediate_Dir "DevServices___Win32_ReleaseWithSymbols"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\.." /I "..\..\..\common" /I "..\..\..\Apps\Appsupport\DDParser" /I "..\..\..\Apps\Appsupport\Interpreter" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Apps\Appsupport\HART_DE" /I "..\..\Device.Lib" /I "..\..\..\Apps\Appsupport\BuiltinLib" /I "..\..\..\Apps\Appsupport\MEE" /I "..\..\..\Device.Lib" /I "..\parserinfc" /D "XMTR" /D "INI_OK" /D "ISLIB" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_UNICODE" /D "_FULL_RULE_ENGINE" /U "_DEBUG" /FR /FD /Zm200 /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Zi /Od /I "..\.." /I "..\..\..\common" /I "..\..\..\Apps\Appsupport\DDParser" /I "..\..\..\Apps\Appsupport\Interpreter" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Apps\Appsupport\HART_DE" /I "..\..\Device.Lib" /I "..\..\..\Apps\Appsupport\BuiltinLib" /I "..\..\.." /I "..\..\..\Apps\Appsupport\MEE" /I "..\..\..\Device.Lib" /I "..\parserinfc" /D "DBG_PVFC" /D "NDEBUG" /D "IS_SDC" /D "XMTR" /D "WIN32" /D "_UNICODE" /D "_FULL_RULE_ENGINE" /U "_DEBUG" /FR /FD /Zm200 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "DevServices - Win32 Release"
# Name "DevServices - Win32 Debug"
# Name "DevServices - Win32 ReleaseWithSymbols"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\COMMON\dbg_dllApi.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbArray.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbAttributes.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbAxis.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbChart.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCmdDispatch.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCollAndArr.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCommand.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCritical.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\ddbdefs.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbDependency.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbDevice.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceMgr.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbEditDisp.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbFile.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbFormat.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbFormatHCL.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbGraph.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbImage.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbItemBase.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbItemLists.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbList.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbMethod.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbPayload.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbPrimatives.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbPublish.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbReference.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbRfshUnitWaoRel.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbSource.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbVar.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbVarNumeric.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbWaveform.cpp

!IF  "$(CFG)" == "DevServices - Win32 Release"

!ELSEIF  "$(CFG)" == "DevServices - Win32 Debug"

!ELSEIF  "$(CFG)" == "DevServices - Win32 ReleaseWithSymbols"

# ADD CPP /O2

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\foundation.cpp
# End Source File
# Begin Source File

SOURCE=.\grafix.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\varient.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ddbArray.h
# End Source File
# Begin Source File

SOURCE=.\ddbAttrEnum.h
# End Source File
# Begin Source File

SOURCE=.\ddbAttributes.h
# End Source File
# Begin Source File

SOURCE=.\ddbAxis.h
# End Source File
# Begin Source File

SOURCE=.\ddbChart.h
# End Source File
# Begin Source File

SOURCE=.\ddbCmdDispatch.h
# End Source File
# Begin Source File

SOURCE=.\ddbCmdList.h
# End Source File
# Begin Source File

SOURCE=.\ddbCollAndArr.h
# End Source File
# Begin Source File

SOURCE=.\ddbCommand.h
# End Source File
# Begin Source File

SOURCE=.\ddbCommInfc.h
# End Source File
# Begin Source File

SOURCE=.\ddbConditional.h
# End Source File
# Begin Source File

SOURCE=.\ddbCondResolution.h
# End Source File
# Begin Source File

SOURCE=.\ddbDependency.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\ddbDevice.h
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceMgr.h
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceService.h
# End Source File
# Begin Source File

SOURCE=.\ddbEditDisplay.h
# End Source File
# Begin Source File

SOURCE=.\ddbEvent.h
# End Source File
# Begin Source File

SOURCE=.\ddbFile.h
# End Source File
# Begin Source File

SOURCE=.\ddbGlblSrvInfc.h
# End Source File
# Begin Source File

SOURCE=.\ddbGraph.h
# End Source File
# Begin Source File

SOURCE=.\ddbGrid.h
# End Source File
# Begin Source File

SOURCE=.\ddbGrpItmDesc.h
# End Source File
# Begin Source File

SOURCE=.\ddbImage.h
# End Source File
# Begin Source File

SOURCE=.\ddbItemBase.h
# End Source File
# Begin Source File

SOURCE=.\ddbItemListBase.h
# End Source File
# Begin Source File

SOURCE=.\ddbItemLists.h
# End Source File
# Begin Source File

SOURCE=.\ddbItems.h
# End Source File
# Begin Source File

SOURCE=.\ddbList.h
# End Source File
# Begin Source File

SOURCE=.\ddbMenu.h
# End Source File
# Begin Source File

SOURCE=.\ddbMethod.h
# End Source File
# Begin Source File

SOURCE=.\ddbMethSupportInfc.h
# End Source File
# Begin Source File

SOURCE=.\ddbMsgQ.h
# End Source File
# Begin Source File

SOURCE=.\ddbMutex.h
# End Source File
# Begin Source File

SOURCE=.\DDBpayload.h
# End Source File
# Begin Source File

SOURCE=.\ddbPolicy.h
# End Source File
# Begin Source File

SOURCE=.\ddbPrimatives.h
# End Source File
# Begin Source File

SOURCE=.\ddbReferenceNexpression.h
# End Source File
# Begin Source File

SOURCE=.\ddbResponseCode.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\ddbRfshUnitWaoRel.h
# End Source File
# Begin Source File

SOURCE=.\ddbSource.h
# End Source File
# Begin Source File

SOURCE=.\ddbSvcRcvPkt.h
# End Source File
# Begin Source File

SOURCE=.\ddbTracking.h
# End Source File
# Begin Source File

SOURCE=.\ddbVar.h
# End Source File
# Begin Source File

SOURCE=.\ddbVarList.h
# End Source File
# Begin Source File

SOURCE=.\ddbWaveform.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\DDLDEFS.H
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\Endian.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\grafix.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\HARTsupport.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\varient.h
# End Source File
# End Group
# Begin Group "Interpreter"

# PROP Default_Filter ""
# Begin Group "Interpreter Source Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Interpreter\ArrayExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\AssignmentStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\BreakStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CaseStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ComplexDDExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CompoundExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CompoundStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ContinueStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Declaration.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Declarations.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ELSEStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\EmptyStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ExpParser.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Expression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\FunctionExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\GrammarNode.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\GrammarNodeVisitor.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IFExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\INTER_SAFEARRAY.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\INTER_VARIANT.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\InterpretedVisitor.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Interpreter.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationDoWhile.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationFor.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\LexicalAnalyzer.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\OMServiceExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\OptionalStatements.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ParseEngine.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Parser.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ParserBuilder.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\PrimaryExpression.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Program.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ReturnStatement.CPP
# End Source File
# Begin Source File

SOURCE=..\Interpreter\RuleServiceStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SelectionStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ServiceStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Statement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\StatementList.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SwitchStatement.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SymbolTable.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SynchronisationSet.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Token.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\TypeCheckVisitor.cpp
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Variable.cpp
# End Source File
# End Group
# Begin Group "Interpreter Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Interpreter\ArrayExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\AssignmentStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\BreakStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\BuiltinFunctions.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CaseStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ComplexDDExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CompoundExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\CompoundStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ContinueStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Declaration.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Declarations.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ELSEStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\EmptyStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ErrorDefinitions.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ExpParser.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Expression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\FunctionExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\GrammarNode.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\GrammarNodeVisitor.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\INTER_SAFEARRAY.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\INTER_VARIANT.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\InterpretedVisitor.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Interpreter.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationDoWhile.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationFor.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\IterationStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\LexicalAnalyzer.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\OMServiceExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\OptionalStatements.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Parser.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ParserBuilder.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ParserDeclarations.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\PrimaryExpression.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Program.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ReturnStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\RuleServiceStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SafeVar.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SelectionStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\ServiceStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Statement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\StatementList.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SwitchStatement.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SymbolTable.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\SynchronisationSet.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Token.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\TypeCheckVisitor.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\typedefs.h
# End Source File
# Begin Source File

SOURCE=..\Interpreter\Variable.h
# End Source File
# End Group
# End Group
# Begin Group "BuiltinLib"

# PROP Default_Filter ""
# Begin Group "Builtin Lib Source File"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\BuiltinLib\AbortBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\BuiltinInvoke.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\BuiltinUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\DateTimeBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\Delay_Builtin.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\Hart_Builtins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\MathBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\PrePostActionsBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\ResolveBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\SendCmdBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\SetBuiltIns.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\StringBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\TransferBuiltins.cpp
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\UIBuiltins.cpp
# End Source File
# End Group
# Begin Group "Builtin Lib Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\BuiltinLib\BI_CODES.H
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\Delay_Builtin.h
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\Hart_Builtins.h
# End Source File
# Begin Source File

SOURCE=..\BuiltinLib\RTN_CODE.H
# End Source File
# End Group
# End Group
# Begin Group "MEE"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\MEE\MEE.cpp
# End Source File
# Begin Source File

SOURCE=..\MEE\MEE.h
# End Source File
# End Group
# Begin Group "DDParserInfc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\ParserInfc\ddbParserInfc.cpp
# End Source File
# Begin Source File

SOURCE=..\ParserInfc\ddbParserInfc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\COMMON\sign.cpp
# End Source File
# End Group
# End Target
# End Project
