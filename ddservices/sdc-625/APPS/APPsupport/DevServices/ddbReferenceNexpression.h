/*************************************************************************************************
 *
 * $Workfile: ddbReferenceNexpression.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		support objects for the conditional base class
 *		2/6/3	sjv	created - from ddbConditional.h
 *
 * #include "DDBreferenceNexpression.h"
 */


#ifndef _DDBREFNEXPR_H
#define _DDBREFNEXPR_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbdefs.h"
#include "varient.h"
#include "DDBpayload.h"
#include "dllapi.h"
#include "ddbTracking.h"

#include "ddbDeviceService.h"
#include "ddbGlblSrvInfc.h"
#include "ddbFormat.h"
#include "ddbMutex.h"

extern const elementID_t MTelem;// in ddbDevice.cpp

class hCexpression;// forward references
class hCitemBase;  // should be in the include file but some sequences don't work
class hCcollection;

class hCVar;       
class hCindex;

#ifdef X_DEBUG
extern void UNREGMEM(void* p);
extern void REGISMEM(void* p, int src);
#else
#define UNREGMEM( a )
#define REGISMEM( b, c )
#endif 

//typedef vector<hCitemBase*> ddbItemList_t;
/* these are duplicates from dependency and itembase *xx/
class   hCitemBase;
typedef hCitemBase*           ptr2hCitemBase_t;
typedef vector<ptr2hCitemBase_t> ddbItemList_t;
typedef ddbItemList_t::iterator  ddbItemLst_it;// ptr2ptr2itembase
#define _ITEM_BASE_DEFINED
*/

typedef struct s_bitEnumLabel
{									// help example said to "compile with: /LD"
	unsigned	stringNOTempty : 1; // a bool
	unsigned	topWASreference: 1;	// a bool, true if top was a reference-group

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	s_bitEnumLabel() {stringNOTempty=topWASreference=0;};
}
/*typedef*/ LabelBitEnum_t;


/*--------------------- reference ------------------------*/
typedef enum deRefFilter_e
{
	accept_All,		// 00-get const values & item# for all
	accept_AllValues,	// 01-get values only of all types
	accept_NumericMixed,// 10-get values and item# for numerics
	accept_NumericValues// 11-get values for numerics only

} deRefFilter_t;

typedef enum  tokCompatability_e
{
	tokNoOp				=0,	// normal SDC functionality
	tokSupplyTypedef,		//flag to return typedef instead of psuedovariables
	tokDerefInts,			//flag to return all possible indexed items when index is int
	tokTypeDefAndDerefInts,	// flag to do both typedef return and deref Integers
	tokReturnUndefined,		// 0x004 return a null if the item id is not in the item list (ie -u option)
	tokIndexesByIndexed = 8, // 0x008 resolveIDs uses the indexed item to generate possible indicies
	tokTypeDefAndDefreIntsAndIndexed = 11	// tokTypeDefAndDerefInts + tokIndexesByIndexed

}tokCompatability_t;
#define retTypeDef( a )  (a == tokSupplyTypedef || a == tokTypeDefAndDerefInts || a == tokTypeDefAndDefreIntsAndIndexed )
#define retAllInts( a )  (a == tokDerefInts     || a == tokTypeDefAndDerefInts || a == tokTypeDefAndDefreIntsAndIndexed )
#define retUndefs( a )   (  ( a & tokReturnUndefined) == tokReturnUndefined )
#define retAllIdx( a)	   (a == tokTypeDefAndDefreIntsAndIndexed  || a == tokIndexesByIndexed )

class hCreference : public hCpayload, public hCobject
{
	CrefType        rtype; // in foundation .h -> 0 - 24...0 == Item_id,  3 = via item array, 
						   //					  4 = via_Collect, 24 = response code id

	int             isRef; // used as bool:isRef else it's an ID...inited to ID (0)
	ITEM_ID         id;	   // ! isRef && refType == 0,1,2,11 to 24 

	// item type may or may not be valid for the given ITEM_ID... we need to check after a load
	CitemType		type;  // in foundation .h -> 0 - 16...1 == variable, 9 = item_array, 
							//														19 = member
					SUB_INDEX       subindex;	// unknown usage????? may or may not be valid

	// ----- used for via_xxx types (via_item_array, via_collection,etc. (types 3-10))
	int              iName;			// member-name,parameter-name,parameter-list-name,
									//										characteristic-name
	// stevev 23nov11 - The reference has to know it's a collection member so it can generate
	//					the correct (totally different) label and help string.
	hCcollection*	myCollectionPtr;// only valid for member reference on a menu
	unsigned		myMemberIndex;  // which part am I

	expressionType_t exprType;      // type <direct,if,select,case::> unknown==empty expression>
	hCreference*     pRef;			// ItemArray_ref,   (array-ref, collection-ref, record-ref)

	hCvarGroupTrail  grpTrail;

	void onnew(void); // private helper function (common code)
	void addUniqueUInt(UIntList_t& intlist, UINT32 newInt);// private helper function 
	RETURNCODE fillCombinations(GroupItemList_t& varGroup);// helper to breakup big function
//	RETURNCODE ref2GIL(hCitemBase* pItem, UINT32 index, GroupItemList_t& varGroup, 
//						hCGroupItemInfo& activeInfo,  itemID_t indxVarID = 0x0000);
	// stevev 14jul05 - common code extraction, 09jan09 - added message suppression param */
	RETURNCODE resolveVia(itemID_t&   returnID, bool withStrings, bool suppress = false, 
														int refMsg = 0, bool isAtest = false );
	wstring     nameFromTrail(hCvarGroupTrail* pVGT, trailList_t& trailmap);
	/* stevev - try to get generic labeling working */
	bool getFullLabel(hCvarGroupTrail* pgt, wstring& str, LabelBitEnum_t& LabelFlags, 
					  wstring& hstr, int entry=0);// returns true at success

public: 
	// these are pointers since they are used (instantiated) only in particular circumstances
	hCexpression*    pExpr;			// array's (item-array & array) index only
	hCreference(DevInfcHandle_t h);
	virtual ~hCreference();
	RETURNCODE destroy(void);
	// copy constructor
	hCreference(const hCreference& cr);// : hCobject(cr.devHndl()) { clear(); operator=(cr);  };

	hCreference& operator= (const hCreference& cr);
	bool         operator==(const hCreference& ri);

	hCreference& operator=(const aCreference& cr);

	RETURNCODE getRef   (int* pOption=NULL,int* pWhich=NULL,hCreference* pParent=NULL) ;
	RETURNCODE resolveID( itemID_t& returnedID, bool withStrings=true, bool suppress = false, 
														int refMsg = 0, bool isAtest = false );
	RETURNCODE resolveID(hCitemBase*& r2pReturnedPtr,bool withStrings=true,bool suppress = false, 
														int refMsg = 0, bool isAtest = false );
	RETURNCODE resolveExpr(CValueVarient& retValue);

	RETURNCODE resolveAllIDs(UIntList_t& allPossibleIDsReturned, bool stopAtID = true, tokCompatability_t retTypes = tokNoOp);
#define        resolveAllVars(a)  resolveAllIDs((a), true, tokIndexesByIndexed)
	RETURNCODE resolveAllIDs(vector<hCitemBase*>& allPossiblePtrsReturned, tokCompatability_t retTypes = tokNoOp);
	RETURNCODE addAllVars(GroupItemList_t&  varGroup);
//	RETURNCODE addAllRefs(vector<hCreference>/*HreferenceList_t*/& refGroup);
	RETURNCODE commandAllLists(hCcommandDescriptor& cmdDesc);

	//accessors                                      is ref to 0(from 1)  30aug16 stevev
	void setRef(ITEM_ID i, SUB_INDEX s, ITEM_TYPE t, int ir = 0,referenceType_t rt=rT_Item_id)   
				{id=i;subindex=s;type=(long)t;isRef = ir;rtype=rt;};
	ITEM_ID    getID    (void)                          {return id;};
	SUB_INDEX  getSUBIDX(void)                          {return subindex;};
	ITEM_TYPE  getITYPE (void)                          {return type.getType();};
	ITEM_TYPE  getRTYPE (void)                          {return rtype.getType();};
	referenceType_t getRefType(void)                    {return (referenceType_t)rtype.getType();};
	bool       isaRef(void)                             {bool r = 0; if(isRef){ r = 1;} return r;};
	CitemType& getCtype(void)                           {return type;};
	CrefType&  getRtype(void)                           {return rtype;};
	hCvarGroupTrail* getGroupTrailPtr(void)				{return &grpTrail;};

	void    makeAref(void)                              { isRef = 1; };
	void    makeNotAref(void)                           { isRef = 0; rtype = 0/*item_id*/;};

	// payload required
	void  setEqual(void* pAclass);                  // a required method
			
	// make this one a dup of src
	// replicate == false ->> final itemID point to the original
	// replicate ==  true ->> 
	//				if item type is one of 5 data-reference types
	//					final itemID is gotton after a newPsuedoItem
	//				else - not oneOfFive
	//					treat it exactly as if replicate is false
	void duplicate(hCpayload* pPayLd, bool replicate, elementID_t thisElement= MTelem);
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parent=0)
	{	elementID_t elem = { -1, parent };   // stevev 23mar16 - appears backwards was   { parent,-1}
	duplicate(pPayLd, replicate, elem);};
	bool validPayload(payloadType_t ldType)
	{ bool r = false; if(ldType==pltReference) r = true; return r;	};


	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	void clear(void) ;// defined after expression
	bool isEmpty(void);

	/*Vibhor 240304: Start of Code*/
	/*This function is being added to get the label of an item referred thru an array or collection,
	  via collection ref, or via array ref*/ 
	 RETURNCODE getItemLabel(wstring &strLabel, bool& validLabel, wstring &strHelp);
	 // stevev - WIDE2NARROW char interface
	 // we should never need this................RETURNCODE getItemLabel( string & strLabel);	

	 /*This function is being added to get the help of an item referred thru a reference 15sep10*/    	 
	 RETURNCODE getItemHelp(wstring & strLabel); 
	 // we should never need this................RETURNCODE getItemLabel( string & strLabel);	
	 
	 /*This function is being added to get the value of an item referred thru a reference 16sep10*/    	 
	 RETURNCODE getItemValue(wstring & strLabel); // value as a string

	/*Vibhor 240304: End of Code*/
	 RETURNCODE fileWrt(hCformatData*  pData);

	 /* stevev 12aug09 - support for index optimization for weighing */
	 bool isConst(void);// true for unindexed references

	 void setMemberRef(hCcollection* ptr, unsigned memberIdx) 
	 { myCollectionPtr = ptr; myMemberIndex = memberIdx; };
	 
	/* stevev 3jul06 - added to streamline acquisition of dependency info
		4 via, adds expr deps to ref deps [ TODO: add all possible resolution vars ]
		4 direct, adds item's validity dependencies to (if var:item)
	-- addVars is false when it is an expression's min/max value (or any attribute reference */	 
	RETURNCODE aquireDependencyList(ddbItemList_t& retItemIDs, bool addVars = true);

};

typedef vector<hCreference>        HreferenceList_t;
typedef vector<hCreference*>       HreferencePtrList_t;


/*--------------------- expression ------------------------*/

class hCdepends : public hCobject
{
public:// data
	arithOp_t      useOptionVal;	// Max/Min/don't Care
	DDL_UINT       which;           // offset+1 int MIN | MAX range->list[]
	hCreference    depRef;			// a reference - resolves to a variable item id

public: // construct/destruct
	hCdepends(DevInfcHandle_t h): hCobject(h), depRef(h) 
	{	useOptionVal = aOp_dontCare;which = 0;}; /*added on 090304 */
	hCdepends(const hCdepends& src) : hCobject(src.devHndl()), depRef(src.devHndl()) 
	{	operator=(src); };
	virtual
		~hCdepends()
	{ depRef.destroy(); };

	RETURNCODE destroy(void) 
	{	depRef.destroy(); return SUCCESS;
	};
	void clear(void) { useOptionVal=aOp_dontCare;which=0;depRef.clear();};

public:// functions
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);	
	hCdepends& operator=(const hCdepends& srcDep){
		useOptionVal = srcDep.useOptionVal;
		which = srcDep.which;
		depRef = srcDep.depRef; return *this;
	};
	hCdepends& operator=(aCdepends& srcDep){
		useOptionVal = srcDep.useOptionVal;
		which = srcDep.which;
		depRef = srcDep.depRef; return *this;
	};
	bool operator==(const hCdepends& di){ if ( ! (depRef == di.depRef) ) return false;
		if (which != di.which) return false;  /* else*/ return true; };
	//RETURNCODE resolveIndex(hCreference* pArrayRef, hCitemBase/* hCindex */ & r2pIndex);
	void setDuplicate(hCdepends& rSrc);
};

class hCexpressElement : public hCobject
{
public: 
	expressElemType_t   exprElemType;    // types 1 thru 21 are opcodes; 22-29 values
	CValueVarient       expressionData;  // holds opcode(1-21),const(22,23)(index type not used
	hCdepends			dependency;      // holds !isRef @ 24, holds dependency info (25-29)	

public: // construct/destruct
	hCexpressElement(DevInfcHandle_t h) 
		: hCobject(h), dependency(h) 
	{exprElemType = eeT_Unknown;
	REGISMEM(this,1);// 1 = default constructor
	}; /* VMKP added on 090304*/
	hCexpressElement(const hCexpressElement& src) 
		: hCobject(src.devHndl()), dependency(src.devHndl())// copy construct is duplicate of =
	{ operator=(src); 
	REGISMEM(this,2);// 2 = copy constructor
	};
	virtual ~hCexpressElement()
	{
		dependency.destroy();
		expressionData.clear();
		UNREGMEM(this);
	};
	RETURNCODE destroy(void)
	{
		expressionData.clear();
		return dependency.destroy();
	};
	void clear(void) {exprElemType = eeT_Unknown;expressionData.clear();dependency.clear();};

public://functions
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
//	hCexpressElement& operator=(hCexpressElement& srcElem);	
	bool              operator==(const hCexpressElement& ri);// stevev 091006
	hCexpressElement& operator=(const hCexpressElement& srcElem);
	hCexpressElement& operator=(aCexpressElement& srcElem);
	// stevev 23may08 - moved to dbReference.cpp
	//{   exprElemType  = srcElem.exprElemType; 
	//    expressionData= srcElem.expressionData;
	//	dependency    = srcElem.dependency; return *this;
	//};
	void setDuplicate(hCexpressElement& rSrc);
	void makeConst(void);// make a constant element from the values in the varient
};

typedef vector<hCexpressElement>	hRPNexpress_t;
// use varientList_t typedef vector<CValueVarient>       varientList_t;


#define MAX_EXPR_RE_ENTRY	8

class hCexpression : public hCobject
{	// the expression type always goes with the containing class.
//	varientList_t  executionStack;

	int                  entryCnt;

	// private (helper) methods
	RETURNCODE pull_back(CValueVarient& returnVarient, varientList_t& actvStk);
	RETURNCODE    promote(CValueVarient& returnVarient, CValueVarient& op1, CValueVarient& op2);
	RETURNCODE    toBoolT(CValueVarient& returnVarient, CValueVarient& op1, CValueVarient& op2);
	RETURNCODE    resolveAllVarIndexes(hCitemBase* pVar, ExprTrailList_t& exprValueList);

public: // made these public so others could use 'em (like the binary isEqual)
	CValueVarient 
		execUnary(expressElemType_t Opcode,         CValueVarient& op1);
	CValueVarient 
		execBinaryNumeric(expressElemType_t Opcode,CValueVarient& op1,CValueVarient& op2);
	CValueVarient 
		execBinaryBoolean(expressElemType_t Opcode,CValueVarient& op1,CValueVarient& op2);

public: // data
	RETURNCODE       lastError;
	hRPNexpress_t	 expressionL;	// list of CexpressElement in RPN order  

public:// construct/destruct
	hCexpression(DevInfcHandle_t h) : hCobject(h),lastError(0),entryCnt(0) 
		{	expressionL.clear();};  
	hCexpression(const hCexpression& src);
	virtual ~hCexpression();
	RETURNCODE destroy(void);
	void clear(void){ expressionL.clear(); lastError = SUCCESS; entryCnt=0; };
 
public:// functions
	CValueVarient    resolve(hCexpressionTrail** retTrail = NULL, bool isAtest=false);
	//RETURNCODE       resolveAllIndexes(vector<UINT32>& valueList);// for array expressions
	RETURNCODE       resolveAllIndexes(ExprTrailList_t& exprValueList);

	RETURNCODE       resolveIndex(hCreference* pArrayRef, hCindex*& r2pIndex);
	RETURNCODE aquireDependencyList(ddbItemList_t& retItemIDs, bool addVarValues = true);
	RETURNCODE getExprInfo(ExprTrailList_t& exprTrail);// get all expression resolutions
	void       setDuplicate(hCexpression& rSrc);// make this a duplicate of passed-in
	bool       isConst(void);// true if only one value is possible for expression
	
	bool          operator==(const hCexpression& ri);
	hCexpression& operator=(const hCexpression& srcAExpr);
	hCexpression& operator=(aCexpression& srcAExpr);// moved to ddbReference.cpp 2auf16
	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	RETURNCODE checkExpressionVars(void) {return SUCCESS;};
};

class hCexprPayload : public hCpayload, public hCexpression// has a hCobject
{
public:
	hCexprPayload(DevInfcHandle_t h) : hCexpression(h) {};
	hCexprPayload(hCexprPayload& src) : hCexpression(src.devHndl()) {operator=(src);};
	virtual ~hCexprPayload() {hCexpression::destroy();};
	RETURNCODE destroy(void){ return hCexpression::destroy();};

	hCexprPayload& operator=(const hCexprPayload& srcExpr);

	// payload required
	void  setEqual(void* pAclass) ; // aCexpression
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);
	bool validPayload(payloadType_t ldType)
	{bool r = false; if (ldType==pltExpression) r = true; return r; };	

	// let it go to virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	void clear(void) {hCexpression::clear();};

	RETURNCODE setValue(CValueVarient newValue); // constant
};


/*--------------------- reference list ------------------------*/

class hCreferenceListBase : public HreferenceList_t, public hCobject
{
public:
	hCreferenceListBase(DevInfcHandle_t h): hCobject(h) {};
	
	hCreferenceListBase(const hCreferenceListBase& rl): hCobject(rl.devHndl())	
	{  
		operator= (rl);  
	};
	hCreferenceListBase& operator= (const hCreferenceListBase& crl);

	virtual ~hCreferenceListBase()
	{	destroy();
		HreferenceList_t::clear();
	};
	virtual 
	RETURNCODE destroy(void);
};


class hCreferenceList : public hCpayload, public hCreferenceListBase /*parent is reference list*/
{
	// stevev removed 2nov04 HreferenceList_t m_List; //Vibhor 121004: Added

public:
	hCreferenceList(DevInfcHandle_t h);

//	payloadType_t plt(void)const {return(payLoadType);};// for copy construction

	hCreferenceList(const hCreferenceList& rl);
	hCreferenceList& operator= (const hCreferenceList& crl);

	virtual ~hCreferenceList()
	{	hCreferenceListBase::destroy();	
		hCreferenceListBase::clear();        };
	virtual RETURNCODE destroy(void) {
		return hCreferenceListBase::destroy();
	};	

	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parent=0);

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)
	{
	//	COUTSPACE << "      " << size() << " Reference List " << typeName << " Items  " 
	//		      << endl;
		LOGIT(COUT_LOG,"    %d   Reference List %s Items  \n",size(),typeName);

		for(iterator p = begin(); p < end(); p++)
		{
			p->dumpSelf(indent);
		}
		return SUCCESS;
	};
	RETURNCODE getVarPtrs(vector<hCVar*>& rlvL);      // list -> references -> items/* do not include itembase.h*/
	RETURNCODE getItemPtrs(ddbItemList_t& riL);		// for those non var items
	RETURNCODE resolveAllIDs(UIntList_t&allPossibleIDsReturned, tokCompatability_t retTypes=tokNoOp);// append only
	void       append(hCreferenceList* pList2Append); // insert a list behind a list
	void       appendUnique(hCreferenceList* pList2Append) 
	{// temporary bypass
		append(pList2Append);
	};

	void  setEqual(void* pAclass); // a hCpayload required method	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType)
	{ if (ldType != pltReferenceList ) return false; else return true;};

	/*Vibhor 121004: Start of Code*/
/*This guy processes itself and 
	1.  stevev removed 2nov04 ::> generates m_List (reference list) of valid reference values with atmost uMax length
	2. fills the vList with equal number of Varient Values.
*/	/* NOTE: getValueList - gets constants and CURRENT display value of variables 
	 *                      skips string and bool constants                       */
	RETURNCODE getValueList(varientList_t &vList, unsigned uMaxSize);
	/* NOTE: getEntryList - gets constants and symbolIDs of variables <skips bad ones> 
	 *                      gets string and bool constants too                    */
	RETURNCODE getEntryList(varientList_t &vList, unsigned uMaxSize = 0xffffffff,
															deRefFilter_t filter = accept_All);

/*Sets the value of variable/item referenced at specified index*/
	RETURNCODE setValueAtIndex(CValueVarient cv, unsigned uIdx); 

	RETURNCODE getMethodCallList(methodCallSource_t mSrc, methodCallList_t & methList);
	/*Vibhor 121004: End of Code*/

	RETURNCODE getRefPointers(HreferencePtrList_t& retPtrs);// append only

	bool operator== (const hCreferenceList& ri );
};


#endif // _DDBREFNEXPR_H

/*************************************************************************************************
 *
 *   $History: ddbReferenceNexpression.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
