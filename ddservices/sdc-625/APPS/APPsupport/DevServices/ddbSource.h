/*************************************************************************************************
 *
 * $Workfile: ddbSource.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbSource.h".
 */

#ifndef _DDB_SOURCE_H
#define _DDB_SOURCE_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbGrpItmDesc.h"
#include "grafix.h"

class hCchart;// in lieu of including the whole file for one pointer

/*
sourceAttrLabel		SOURCE_LABEL_ID		aCattrString
sourceAttrHelp,		SOURCE_HELP_ID		aCattrString
sourceAttrValidity,	SOURCE_VALID_ID		aCattrCondLong

sourceAttrEmphasis,	SOURCE_EMPHASIS_ID	aCattrCondLong
sourceAttrLineType,	SOURCE_LINETYPE_ID	aCattrCondLong
sourceAttrLineColor	SOURCE_LINECOLOR_ID	aCattrCondLong
sourceAttrYAxis,	SOURCE_YAXIS_ID		aCattrCondReference
sourceAttrMembers,	SOURCE_MEMBERS_ID	aCattrMemberList
*/

class hCsource : public hCitemBase 
{
	hCattrGroupItems*	pMemberAttr; // required - sourceAttrMembers

	hCattrLineType*		pLineType;	//sourceAttrLineType aCattrCondIntWhich 

	hCattrValid*        pEmphasis;	//sourceAttrEmphasis,	aCattrCondLong
	hCattrExpr*			pLineColor;	//sourceAttrLineColor	aCattrCondExpr 
	hCattrAxisRef*		pYaxis;		// sourceAttrYAxis,		aCattrCondReference

	hCattrRefList*      pInitActs;	// sourceAttrInitActions,	aCattrActionList...isa aCattrReferenceList
	hCattrRefList*      pRfshActs;	// sourceAttrRfshActions,	aCattrActionList	
	hCattrRefList*      pExitActs;	// sourceAttrExitActions,	aCattrActionList
public:
	hCsource(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCsource(hCsource*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void);// stevev 13jul07 - made bigger...{ return hCitemBase::destroy(); };
	virtual ~hCsource() ;
	
	hCattrBase*    newHCattr(aCattrBase* pACattr)  // builder 
	{LOGIT(CERR_LOG,"ERROR: hCsource's newHCattr() was called.\n"); return NULL;};
	CValueVarient  getAttrValue(unsigned attrType, int which = 0);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(SOURCE_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(SOURCE_LABEL, l); };

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Source Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Source ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

	RETURNCODE doInitActions();
	RETURNCODE doRefreshActions();
	RETURNCODE doExitActions();

	/*Vibhor 110204: Start of Code*/

	lineType_t getLineType() ;
	
	bool needsEmphasis();

	RETURNCODE getLineColor(unsigned & color);

	hCaxis* getYAxis(); //!!!! Generate Axis from Data if it returns NULL !!!!

	RETURNCODE getMemberList(hCgroupItemList& memberList); //returns a resolved list of members (variable refs)
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);

	/*Vibhor 110204: End of Code*/
	RETURNCODE getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs);//5jul06 sjv

	hCattrGroupItems* getMemberList(void){ 
	#ifdef TOKENIZER
		return pMemberAttr;
	#else
		return NULL;
	#endif
		};

	/* stevev 11sep06 - add dependency calcs */
	void   fillCondDepends(ddbItemList_t&  iDependOn);
	/* stevev 11sep06 - End of add */
	void   fillContainList(ddbItemList_t&  iContain);//stevev 27sep06
	int notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent);

};

#endif	// _DDB_SOURCE_H
