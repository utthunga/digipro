/*************************************************************************************************
 *
 * $Workfile: ddbSource.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbSource.h"
#include "ddbChart.h"
#include "ddbAxis.h"

#include "logging.h"

/* the global mask converter...you have to cast the return value*/
extern int  mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];


hCsource::hCsource(DevInfcHandle_t h, aCitemBase* paItemBase)
	/* initialize base class */  : hCitemBase(h, paItemBase),
	pMemberAttr(NULL),pLineType(NULL),pEmphasis(NULL),pLineColor(NULL),pYaxis(NULL)
	,pInitActs(NULL),pRfshActs(NULL),pExitActs(NULL)
{// label, help and validity are set in the base class
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(sourceAttrMembers); 
	if (aB == NULL)
    {
        //CPMHACK: When Source artifact implemented the below line shall be uncommented
//        cerr << "ERROR: hCsource::hCsource<no member attribute supplied.>"<<endl;
//		cerr << "       trying to find:" <<  sourceAttrMembers  << " but only have:";

		if (paItemBase->attrLst.size() <= 0)
		{
            //CPMHACK: When Source artifact implemented the below line shall be uncommented
//			cerr << " an empty list.";
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ cerr<< (*iT)->attr_mask << ", ";}
		}
        //CPMHACK: When Source artifact implemented the below line shall be uncommented
//		cerr<<endl;
	} 
	else // generate a hart class from the abstract class
	{ 
		pMemberAttr = new hCattrGroupItems(devHndl(), (aCattrMemberList*)aB, this );	

		if (pMemberAttr != NULL)
		{
			pMemberAttr->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMemberAttr);// keep it on the list too
		}
		else // is NULL
		{
			grpItmAttrType_t en = (grpItmAttrType_t)mask2value(aB->attr_mask); 
			cerr << "++ No New attribute ++ (" << grpItmAttrStrings[en] <<")"<<endl;
		}
	}


	// NOT required - but interesting
	aB = paItemBase->getaAttr(sourceAttrLineType); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pLineType = new hCattrLineType(devHndl(), (aCattrCondIntWhich*)aB, this);
		if (pLineType != NULL)
		{
			pLineType->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pLineType);// keep it on the list too
		}
		else // is NULL
		{
			cerr << "++ No New attribute ++ (source line type)"<<endl;
		}

	}
	// else - leave it empty
	
	aB = paItemBase->getaAttr(sourceAttrEmphasis); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pEmphasis = new hCattrValid(devHndl(), (aCattrCondLong*) aB, this );
	}
	// else - leave it empty
	aB = paItemBase->getaAttr(sourceAttrLineColor); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pLineColor = new hCattrExpr(devHndl(),sourceAttrLineColor,(aCattrCondExpr*) aB,this);
	}
	// else - leave it empty
	aB = paItemBase->getaAttr(sourceAttrYAxis); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pYaxis = new hCattrAxisRef(devHndl(),sourceAttrYAxis,(aCattrCondReference*)aB,this);
	}
	// else - leave it empty 
	aB = paItemBase->getaAttr(sourceAttrInitActions); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pInitActs = 
			new hCattrRefList(devHndl(),sourceAttrInitActions,(aCattrReferenceList*)aB,this);
	}
	// else - leave it empty
	aB = paItemBase->getaAttr(sourceAttrRfshActions); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pRfshActs = 
			new hCattrRefList(devHndl(),sourceAttrRfshActions,(aCattrReferenceList*)aB,this);
	}
	// else - leave it empty
	aB = paItemBase->getaAttr(sourceAttrExitActions); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		pExitActs = 
			new hCattrRefList(devHndl(),sourceAttrExitActions,(aCattrReferenceList*)aB,this);
	}
	// else - leave it empty

}

/* from-typedef constructor */
hCsource::hCsource(hCsource*    pSrc,  itemIdentity_t newID) : hCitemBase(pSrc,newID)
{
	cerr<<"**** Implement source typedef constructor.***"<<endl;
}

hCsource::~hCsource()  
{
	RAZE(pMemberAttr);
	RAZE(pLineType);
	RAZE(pEmphasis);
	RAZE(pLineColor);
	RAZE(pYaxis);
	RAZE(pInitActs);
	RAZE(pRfshActs);
	RAZE(pExitActs);
};

#define ANNIHILATE( a ) if(( a )!=NULL){delistAttr( a );( a )->destroy();delete ( a );( a )=NULL;}
		
// 13jul07 - fri - stevev - got a more complex and complete destruction.
RETURNCODE hCsource::destroy(void)
{
	ANNIHILATE(pMemberAttr);
	ANNIHILATE(pLineType);
	ANNIHILATE(pEmphasis);
	ANNIHILATE(pLineColor);
	ANNIHILATE(pYaxis);
	ANNIHILATE(pInitActs);
	ANNIHILATE(pRfshActs);
	ANNIHILATE(pExitActs);
	hCitemBase::destroy();
	return SUCCESS;
}

RETURNCODE hCsource::doInitActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pInitActs) /*No methods*/
		return rc;

	rc = pInitActs->getResolvedList(&wrkList);

	if(SUCCESS == rc && wrkList.size() > 0)
	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}


RETURNCODE hCsource::doRefreshActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pRfshActs) /*No methods*/
		return rc;

	rc = pRfshActs->getResolvedList(&wrkList);

	if(SUCCESS == rc && wrkList.size() > 0)
	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}

RETURNCODE hCsource::doExitActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pExitActs) /*No methods*/
		return rc;

	rc = pExitActs->getResolvedList(&wrkList);

	if(SUCCESS == rc && wrkList.size() > 0)
	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}


/*Vibhor 110204: Start of Code*/

lineType_t hCsource	::getLineType()
{
	if(NULL == pLineType)
	{
		return lT_Data;//default, to be implemented in JIT Parser
	}
	hCddlIntWhich tmpIntWhich(devHndl());

	tmpIntWhich = pLineType->procureBase();

	unsigned uLType = (UINT32)tmpIntWhich.getDdlLong();// very limited value set

	int which = tmpIntWhich.getWhich();

	switch((lineType_t)uLType)
	{
		case lT_Data:
			{
			if(which > 0 && which < 10)
				return ((lineType_t)( lineTypeOffset + which));
			else
				return lT_Data;

			}
			break;
		case lT_LowLow:
		case lT_Low:	
		case lT_High:
		case lT_HighHigh:
		case lT_Trans:
			return (lineType_t)uLType;
			break;
		case lT_Unknown:
		case lT_Undefined:
		default:
			cerr <<"hCwaveform::getLineType() received an Invalid Line type !! "<<endl;
			break;

	}
	return lT_Data;

}/*End getLineType*/


bool hCsource::needsEmphasis()
{
	unsigned uTemp = 0;
	if(NULL == pEmphasis)
		return false;
	else
	{
		hCddlLong *pTmpLong = (hCddlLong*) pEmphasis->procure();
		if(pTmpLong)
		{
			uTemp = (UINT32)pTmpLong->getDdlLong();// very limited value set
			if(1 == uTemp)
				return true;
			else
				return false;
		}
		else
		{
			cerr<<"Emphasis expression resolution failed for waveform :" << this->getID() <<endl;
			return false; //This is an error


		}
	}

}/*End needsEmphasis*/


RETURNCODE hCsource ::getLineColor(unsigned & uColor)
{
	RETURNCODE rc = SUCCESS;
	if(NULL == pLineColor)// leave uColor as entered
		return APP_ATTR_NOT_SUPPLIED; //Since JIT Parser will always provide a defualt;
	else
	{
		uColor = (unsigned) pLineColor->getExprVal();
	}

	return rc;

}/*End getLineColor*/



hCaxis* hCsource ::getYAxis()
{
	RETURNCODE rc = SUCCESS;
	if (NULL == pYaxis)
	{
		return NULL; //The Caller needs to generate the axis in the library from the values
	}
	
	hCreference *ptrTempRef = (hCreference *)pYaxis->procure();

	if(NULL == ptrTempRef)
		return NULL; 

	hCitemBase *ptr2BaseItm = NULL;
	rc = ptrTempRef->resolveID(ptr2BaseItm);
	if(SUCCESS != rc)
		return NULL; 
	else
		return (hCaxis *)ptr2BaseItm;

}/*End getYAxis*/


RETURNCODE hCsource ::getMemberList(hCgroupItemList& memberList)
{
	RETURNCODE rc = SUCCESS;

	groupItemPtrList_t tmpList;

	rc = pMemberAttr->getList(tmpList);

	memberList.clear();
	if(SUCCESS == rc && tmpList.size() > 0)
	{
		groupItemPtrList_t :: iterator it;
		
		for(it = tmpList.begin(); it != tmpList.end();it++)
		{
			memberList.push_back(**it);	
		}

	}

	tmpList.clear();

	return rc;
}

/*Vibhor 110204: End of Code*/
RETURNCODE 
hCsource::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( pMemberAttr != NULL  && pGid != NULL )
	{
		rc = pMemberAttr->getItemByIndex(indexValue, *pGid);
		if ( rc != SUCCESS)
		{
			delete pGid;
			*ppGID = NULL;
			if ( ! suppress)
			{
				cerr << "ERROR: Source getByIndex failed to getItemByIndex.'" << hex <<
					indexValue <<"'  Item 0x" << getID() <<endl;
				clog << "ERROR: Source getByIndex failed to getItemByIndex."<<endl;
			}
			return rc;
		}
	}
	if (rc != SUCCESS)
	{
		RAZE(pGid);		
		*ppGID = NULL;
	}
	return rc;
}


CValueVarient hCsource::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;

	sourceAttrType_t sat = (sourceAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( sat >= sourceAttr_Unknown )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Source's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	switch (sat)
	{
	case sourceAttrLabel:
	case sourceAttrHelp:
	case sourceAttrValidity:
		rV = hCitemBase::getAttrValue(sat);		
		break;
	case sourceAttrEmphasis:
		{
			rV.vType = CValueVarient::isBool;
			rV.vSize = 1;// default
			rV.vIsValid = true;
			rV.vIsUnsigned = false;

			bool x = needsEmphasis();

			rV.vValue.bIsTrue = (x)?true:false;
		}
		break;
	case sourceAttrLineType:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			lineType_t lt = getLineType() ;
			rV.vValue.iIntConst = (int) lt;
		}
		break;
	case sourceAttrLineColor:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned lc;
			if ( getLineColor(lc) == SUCCESS )
			{
				rV.vValue.iIntConst = lc;
			}
			else
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid    = false;
			}
		}
		break;
	case sourceAttrYAxis:
		{
			rV.vType = CValueVarient::isSymID;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			if ( pYaxis == NULL )
			{// - don't know the correct action here since the graph will have an axis anyway
				// TODO: figure out how to deal with this
				rV.vValue.varSymbolID = 0;
				rV.vIsValid = false;
			}
			else
			{
				rV = pYaxis->getValue();
			}
		}
		break;
	case sourceAttrMembers:
		{// not available - must use resolve
			LOGIT(CERR_LOG,"ERROR: member information is not accessable.\n");
		}
		break;
	case sourceAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIT(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

// gets all possible references for a given member number  
// get the member value from all valid lists (used in the unitialized mode - no reads allowed)
//				ref& expr pointers must be destroyed by caller...What is pointed to must NOT
RETURNCODE hCsource::getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllItemsByIndex(indexValue, returnedItemRefs);
		if (rc != SUCCESS || returnedItemRefs.size() == 0 )
		{/* NOTE: this often occurs if PRIMARY is defined as 1 in imports but 0 in the DD */
		 /*       comes from the difference in macros.ddl and macros.h                    */
			LOGIT(CERR_LOG,"ERROR: %s member list failed to find index %d.\n",
																	getName().c_str(), indexValue);
#ifdef _DEBUG
			LOGIT(CERR_LOG,"       possible mismatch of PRIMARY definition (DD to imports).\n");
#endif
		}
	}
	else
	{
		LOGIT(CERR_LOG,
				"ERROR: No Member attribute for a member based item (%s).\n",getName().c_str());
	}
	return rc;
}


/* stevev 12sep06 - dependency extentions */
void  hCsource :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes

	if ( pYaxis != NULL )// Y_AXIS 
	{
		pYaxis->getDepends(iDependOn);
	}
	if ( pMemberAttr != NULL )// X_VALUES 
	{
		pMemberAttr->getDepends(iDependOn);
	}
	if ( pEmphasis != NULL )// EMPHASIS 
	{
		pEmphasis->getDepends(iDependOn);
	}		

/* LineType, Line Color :: Cannot change while running */
}
/* end stevev stevev 12sep06  */

/* stevev 27sep06 - more dependency extentions */
// get all possible items in this source
void  hCsource::fillContainList(ddbItemList_t&  iContain)
{// fill with items-I-contain 
	if ( pMemberAttr == NULL ) return;		

	//pMemberAttr-> getAllindexes(UIntList_t& allindexedIDsReturned);
	//	pMemberAttr - get all possible lists out of conditional to a groupItemListPtrList_t
	//				- for each hCgroupItemList - call getItemIDs(returnList)
	//		getItemIDs	- for each hCgroupItemDescriptor in thisList
	//					-    get the reference 
	//					-	 reference:resolveAllIDs(returnList)
	/////////////////////////////////////////////////////////////////////////////////////////////

	vector<unsigned int>  localitemIDList;
	vector<unsigned int>::iterator iItemID;
	hCitemBase* pItem   = NULL ;

	if (getAllindexes(localitemIDList) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{
			if ( devPtr()->getItemBySymNumber(*iItemID, &pItem) == SUCCESS && pItem != NULL)
			{
				iContain.push_back(pItem);
			}
		}
	}// else move on
}
/** end stevev 27sep06 **/

/** added stevev 10oct06 **/
// called when an item in iDependOn has changed in the chart (called from Chart's notification)
//		Since a source is merely an attribute grouping for charts, we only notify the chart
int hCsource::notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	hCitemBase* pItem = NULL;

	hCitemBase::notification(msgList,isValidity);// do validity and label
/* note to self - 05oct06 - cannot filter on isConditional if any part of the payload is a 
							reference or expression */
	// re-evaluate all the others 
	if (! isValidity) 
	{// members, emphisis, axis
		if (pYaxis != NULL )
		{
			if ( pYaxis->reCalc())//axis item
			{// my axis changed
				msgList.insertUnique(pParent->getID(), mt_Str, pYaxis->getGenericType());
			}// else - no change;
			CValueVarient v = pYaxis->getValue();
			if ( v.vType == CValueVarient::isSymID && v.vIsValid )
			{	if ( devPtr()->getItemBySymNumber((unsigned int)v, &pItem) == SUCCESS && 
																					pItem != NULL)
				((hCaxis*) pItem)->notification(msgList, isValidity, pParent); 
			}
		}

		if (pEmphasis != NULL && pEmphasis->isConditional() && pEmphasis->reCalc())// long
		{// my emphasis changed
			msgList.insertUnique(pParent->getID(), mt_Str, pEmphasis->getGenericType());
		}// else - no change;

		if (pMemberAttr != NULL && pMemberAttr->reCalc())// list
		{// my member list changed
			msgList.insertUnique(pParent->getID(), mt_Str, pMemberAttr->getGenericType());
		}// else - no change;

	}
	return (msgList.size() - init);// number added
}
/** end stevev 10oct06  **/