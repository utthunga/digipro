/**********************************************************************************************
 *
 * $Workfile: ddbVar.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *      definition of the variable instance class
 * 8/30/2   sjv  creation from ddbVariable
 * Component History: 
 * 16Nov06 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 * 16jul08 - stevev moved the pure numerics(signed,unsgnd,time,int,flt,dbl) 
 *                  to ddbVarNumerics.cpp
 */

#include <limits>
const double maxFloat = std::numeric_limits<double>::max();
const double minFloat = std::numeric_limits<double>::min();

#if defined(__GNUC__)
#include <ctype.h>
#include <cmath> // For HUGE_VAL
#include <cctype>
#endif // __GNUC__

// L&T Modifications : VariableSupport - start
#ifdef __GNUC__
#include <string>
#endif
// L&T Modifications : VariableSupport - end


#include "ddbItems.h"
#include "ddbVar.h"
#include "ddbDevice.h"  /* stevev 12sep07 - to get access to the index mutex */
//#include "ddbBaseDevice.h"
#include "ddbDeviceService.h"
#include "ddbAttrEnum.h"


#include "Endian.h"
#include <algorithm>

#include "ddbVarList.h"
#include "Char.h"



#ifdef INI_OK
extern bool openIni();
extern bool closeIni();
#ifdef ISDLL
char *getSymbolValue(unsigned long id){return "getSymbol NOT implemented";};
char *getSymbolValue(char *id){return "getSymbol NOT implemented";};
#else
extern char *getSymbolValue(unsigned long id);
extern char *getSymbolValue(char *id);
#endif
#endif //INI_OK


//#define  LOG_ATTRIBUTES 1  /* define to log reception of min-max/range/pre-post etc */

extern char varAttrStrings[VARATTRSTRCOUNT][VARATTRSTRMAXLEN];
const std::string MT_STRING("");
const std::wstring MT_WSTRING(L"");

#define MAX_INPUTSTRING     64      // totally arbitrary

//                     0  1  2  3   4   5   6   7   8
char digits4bytes[] = {0, 4, 6, 9, 11, 14, 16, 18, 20};

/* global function to convert a mask to a value */
/*        takes the lowest numbered bit if if there is more than one*/
int mask2value(ulong inMask)
{// note that inMask is really a local copy
    for ( int y = 0 ; y < (sizeof(ulong) * 8); inMask>>=1, y++)
    {
        if (inMask & 0x01)
        {// bit is set, end it
            return y;
        }
        //else shift, inc, loop
    }
    return -1;// no bits set
}

// a remove_if algorithm predicate
bool hasDevIdx(hIndexUse_t elem) {return ( elem.devIdxSymID != 0 ); }


#define DFLT_ISDYNAMIC  false
#define DFLT_ISLOCAL    false
#define DFLT_ISREADONLY true

#define LOG_CMD_SELECTION 1


#define  USE_INDEX_MUTEX  /* get index mutex before reading indexes */

/* stevev 08aug06 - added
 * global function to generate a psuedo-Variable of any type w/ CLASS LOCAL
 * NOTE: caller is responsible for setting an intial value
 *       handle is passed in to keep this a static function
 *       -- the zero in the aC's ID number will tell itembase to gen a psuedo id & register
 *///  static
hCVar* hCVar::newPsuedoVar(DevInfcHandle_t h, variableType_t varType, int varSize)
{
    if (varType<=vT_unused || varType>=vT_MaxType || varSize<=0 )
    {// error return
        return NULL; 
    }
    aCitemBase  baseItem;                           // aC template (easiest to build)

    aCattrTypeType att; 
    att.attr_mask     = (1<<varAttrTypeSize);       // typesize attribute
    att.notCondTypeType.actualVarType = varType;
    att.notCondTypeType.actualVarSize = 
    baseItem.itemSize                 = varSize;

    aCattrBitstring aClas;                          // class attribute
    aClas.attr_mask = (1<< varAttrClass);
    aClas.condBitStr.priExprType = eT_Direct;       //      iscondional direct

     aCgenericConditional::aCexpressDest tempaCDest;// deleted on exit
     tempaCDest.destType = eT_Direct;
     tempaCDest.pPayload = new aCbitString();
     ((aCbitString *)tempaCDest.pPayload)->bitstringVal = maskLocal;
                
    aClas.condBitStr.destElements.clear();
    aClas.condBitStr.destElements.push_back(tempaCDest);

    baseItem.itemId      = 0;
    baseItem.itemType    = iT_Variable;
    baseItem.itemSubType = 0;
    baseItem.attrMask    = att.attr_mask || aClas.attr_mask; 
    baseItem.itemName    = "Var_Template";
        
    baseItem.isConditional = false;

    baseItem.attrLst.push_back(&att);
    baseItem.attrLst.push_back(&aClas);
/*original                                                      // we now have our aC template
    hCVar* pV = hCVar::newVar(h, &baseItem);
    pV->setDefaultValue();
                                                        // we now have our hC template
    hCitemBase* retPtr = pV->devPtr()->newPsuedoItem(pV, NULL, false);
*/

//**************** helper MACRO ******************************** /
#define newItem(className)      new     className(h,&baseItem)
//************************************************************** /

    hCVar* pV = NULL;
    switch ( varType )
    {
    case vT_Integer:        pV = newItem(hCSigned);     break;
    case vT_Unsigned:       pV = newItem(hCUnsigned);   break;
    case vT_FloatgPt:       pV = newItem(hCFloat);      break;
    case vT_Double:         pV = newItem(hCDouble);     break;
    case vT_Enumerated:     pV = newItem(hCEnum);       break;
    case vT_BitEnumerated:  pV = newItem(hCBitEnum);    break;
    case vT_Index:          pV = newItem(hCindex);      break;
    case vT_Ascii:          pV = newItem(hCascii);      break;
    case vT_PackedAscii:    pV = newItem(hCpackedAscii);break;
    case vT_Password:       pV = newItem(hCpassword);   break;
    //case vT_BitString:
    case vT_HartDate:       pV = newItem(hChartDate);   break;
    case vT_TimeValue:      pV = newItem(hCTimeValue);  break;
    default:
        {
            LOGIT(CERR_LOG,"ERROR(internal): Unsupported Variable type.");
        }
    }// end subtype switch
    /* 
    // delete should handle the rest on the way out   baseItem.destroy;
    pV->destroy();
    delete pV;
    end of new code */
    // 10aug06 - anil issue - on exit of this routine, the aC classes try to delete attributes
    //                      on the stack. - empty the list and delete 'em first 

    FOR_iT(AattributeList_t, baseItem.attrLst)
    { 
        if(*iT) 
            (*iT)->clear(); 
    }
    baseItem.attrLst.clear();
    delete tempaCDest.pPayload;

    return ( pV ); // was  (hCVar*)retPtr );
}


// construct from a dllapi base construct
hCVar::hCVar(DevInfcHandle_t h, aCitemBase* paItem)
    /* initialize base class */  :hCitemBase(h, paItem), pPsuedoDflt(NULL)
#ifdef _DEBUG
  ,isNotified(false)
#endif
{
    dataState   = IDS_UNINITIALIZED;
    dataQuality = DA_NOT_VALID;
    /// sjv 11.11.10 pEnum       = NULL;
    pClass      = NULL;
    pHandling   = NULL;
    pUnit       = NULL;
    pDefaultVal = NULL;
    isCached    = false;
    queryCount  = 0;
    dataStatus  = 1;// stevev 02/26/04 - 1 is normal, keep going(0-do not weigh)
    writeStatus = 0;// 0 is normal condition - 1 means user written

    height      = gS_xxxSmall;
    width       = gS_xxxSmall;

    dominantRelations.clear(); 
    // sjv 30nov06  unitRelation = INVALID_ITEM_ID; 
    unitRelations.clear();
    unitRelations.setOwner(getID(),devPtr());
    waoRelation  = InvalidItemID;   
    
        //prashant
    m_bLoopWarnVariable = false;
    previousDataState = dataState;// stevev 5jul05
// stevev 20jul06 - share attributes //
    sharedAttr = false;

#ifdef _DEBUG
if (getID() == 0x41ad)
{
    LOGIT(CLOG_LOG,"got bp.{dbg only msg}\n");
}
#endif
    if ( paItem == NULL)
        return;
    ///////////////////////////////////////////////////////////////////////////////////
// itemBase does:
//  hCattrValid,  
//  hCattrLabel, 
//  hCattrHelp
//  hCattrDebug

    // ib.AattributeList_t attrLst;// a typedef vector<aCattrBase*> AattributeList_t;
    aCattrBase* aB = NULL;
    // try to get an abstract attribute pointer (if abstract attribute exists)
    aB = paItem->getaAttr(varAttrEnums); 
    /// sjv 11.11.10 if (aB == NULL){ pEnum = NULL; } 
    /// sjv 11.11.10 else // generate a hart class from the abstract class
    /// sjv 11.11.10 { 
    /// sjv 11.11.10    // now handled in enum class....
    /// sjv 11.11.10 }

    aB = paItem->getaAttr(varAttrClass); // all labels must use the same structure
    if (aB == NULL) { pClass = NULL; } 
    else  // generate a hart class from the abstract class
    { pClass = new hCattrClass  (h, (aCattrBitstring*)aB);
      pClass->setItemPtr((hCitemBase*)this);
        attrLst.push_back(pClass);
    }
    
    aB = paItem->getaAttr(varAttrHandling) ; 
    if (aB == NULL)
    {// *********** DEFAULT -READ_HANDLING | WRITE_HANDLING 
        RETURNCODE rc = SUCCESS;
        pHandling  = new hCattrHndlg(h);
        if ( pHandling != NULL)
        {
            pHandling->setDefaultVals(rc);
        }
        else
        {
            LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: Handling attribute could not be 'newed';\n");
        }
    }
    else  // generate a hart class from the abstract class
    { 
        pHandling  = new hCattrHndlg(h, (aCattrBitstring*)aB );
    }
    pHandling->setItemPtr((hCitemBase*)this);// moved sjv 10apr07
    attrLst.push_back(pHandling);
    
    aB = paItem->getaAttr(varAttrConstUnit) ; 
    if (aB == NULL){ pUnit  = NULL; }
    else  // generate a hart class from the abstract class
    { pUnit  = new hCattrUnit(h, (aCattrString*)aB );
      pUnit->setItemPtr((hCitemBase*)this);
      attrLst.push_back(pUnit);
    }

    aB = paItem->getaAttr(varAttrTypeSize); 
    if (aB == NULL){ varSize = 0; varType = vT_undefined;
        LOGIT(CLOG_LOG, "NOTE: No Type-Size in attribute list.\n"); } 
    else // generate a hart class from the abstract class
    { 
        aCvarTypeDescriptor* pvtd = &(((aCattrTypeType*)aB)->notCondTypeType);
        varSize = pvtd->actualVarSize;
        varType = (variableType_t)pvtd->actualVarType;
        
#ifdef _DEBUG
        if (varSize != itemSize)
        {
            LOGIT(CLOG_LOG|CERR_LOG, "NOTE: Sizes do not match: item= %d. abstract = 0x%x\n",
                itemSize,varSize); 
        }
#endif
    }

    aB = paItem->getaAttr(varAttrDefaultValue); // default value now for all vars
    if (aB == NULL) { pDefaultVal = NULL; } 
    else  // generate a hart class from the abstract class
    { pDefaultVal = new hCattrVarDefault  (h, (aCattrCondExpr*)aB);
      pDefaultVal->setItemPtr((hCitemBase*)this);
        attrLst.push_back(pDefaultVal);
    }

    // NOT required
    aB = paItem->getaAttr(varAttrHeight); // uses aCattrCondLong    - as enum CgrafixSize
    if (aB != NULL)
    {
	   // hCattrVarHeight* pHA = (hCattrVarHeight*)newHCattr(h, aB);
		hCattrGrafixSize* pHA = (hCattrGrafixSize*)newHCattr(h, aB);
        if (pHA != NULL)
        {
            height = pHA->get_GsSize();
            delete pHA;
        }
        //else // is NULL- leave it default;
    }
    else
    if( devPtr()->getPolicy().isPartOfTok )
    {
        height = gS_Undefined;// tok needs to know it was never included
    }// else leave it default

    
    aB = paItem->getaAttr(varAttrWidth); 
    if (aB != NULL)
    {
		//hCattrVarWidth* pHA = (hCattrVarWidth*)newHCattr(h, aB);
		hCattrGrafixSize* pHA = (hCattrGrafixSize*)newHCattr(h, aB);
        if (pHA != NULL)
        {
            width = pHA->get_GsSize();
            delete pHA;
        }
        //else // is NULL- leave it default;
    }
    else
    if( devPtr()->getPolicy().isPartOfTok )
    {
        width = gS_Undefined;// tok needs to know it was never included
    }// else leave it default


    // add the rest of 'em to the list
    for (AattributeList_t::iterator iT = paItem->attrLst.begin(); 
            iT < paItem->attrLst.end(); iT++)
    {// iT isa ptr2ptr2 aCattrBase
        if ((*iT) &&
            ((*iT)->attr_mask != maskFromInt(varAttrValidity) &&  /* itembase */
            (*iT)->attr_mask != maskFromInt(varAttrLabel)    &&  /* itembase */ 
            (*iT)->attr_mask != maskFromInt(varAttrHelp)     &&  /* itembase */ 
            (*iT)->attr_mask != maskFromInt(varAttrDebugInfo)&&  /* itembase */  
            (*iT)->attr_mask != maskFromInt(varAttrEnums)    &&  /* hCEnum   */ 
            (*iT)->attr_mask != maskFromInt(varAttrIndexItemArray)&&  /* hCindex */
            (*iT)->attr_mask != maskFromInt(varAttrClass)    &&  /* hCVar */
            (*iT)->attr_mask != maskFromInt(varAttrHandling) &&  /* hCVar */  
            (*iT)->attr_mask != maskFromInt(varAttrTypeSize) &&  /* hCVar */  
            (*iT)->attr_mask != maskFromInt(varAttrConstUnit)&&  /* hCVar */  
/* stevev 03nov09 */   
            (*iT)->attr_mask != maskFromInt(varAttrDefaultValue)&&  /* hCVar */ 
/* end == 03nov09 */  
            (*iT)->attr_mask != maskFromInt(varAttrDisplay)  &&  /* handled in numeric */
            (*iT)->attr_mask != maskFromInt(varAttrEdit)     &&  /* handled in numeric */ 
            (*iT)->attr_mask != maskFromInt(varAttrScaling)  &&  /* handled in numeric */ 
/* sjv 20sep06*/
            (*iT)->attr_mask != maskFromInt(varAttrMinValue) &&  /* handled in numeric */
            (*iT)->attr_mask != maskFromInt(varAttrMaxValue)     /* handled in numeric */
/* end 20sep06*/
/* stevev 03nov09 */                                         &&
            (*iT)->attr_mask != maskFromInt(varAttrTimeScale)&&  /* handled in timeValue */
            (*iT)->attr_mask != maskFromInt(varAttrTimeFormat)   /* handled in timeValue */
/* end == 03nov09 */ 
            )
            )
        {// we don't already have it
            hCattrBase* pAttr = NULL; //VMKP : 050304
            pAttr = newHCattr(h, *iT);
            if (pAttr != NULL)
            {
/*
varAttrRdTimeout,     varAttrWrTimeout,    varAttrPreReadAct,     varAttrPostReadAct,  
varAttrPreWriteAct,   varAttrPostWriteAct, varAttrPreEditAct,     varAttrPostEditAct,    ,  
varAttrResponseCodes, varAttrRefreshAct,   varAttrPostRequestAct, varAttrPostUserAct
*/
#ifdef LOG_ATTRIBUTES /* defined (or not) at the top of this file */                
                LOGIT(CLOG_LOG, "got a listed attribute.%s for item 0x%x\n",
                    varAttrStrings[(varAttrType_t)mask2value((*iT)->attr_mask)],getID()); 
#endif
                pAttr->setItemPtr((hCitemBase*)this);
                attrLst.push_back(pAttr);
            }
            else
            {
                varAttrType_t en = (varAttrType_t)mask2value((*iT)->attr_mask); 
                if (  !(devPtr()->getPolicy().isReplyOnly)                && 
                    ( en == varAttrPostUserAct || en == varAttrPostRequestAct )  )
                {// not a simulator and is a simulation attribute then
                    ; /* do nothing */
                }
                else
                {
                    LOGIT(CERR_LOG, "++ No New attribute ++ (%s)\n",varAttrStrings[en]);
                }
            }
        }
    }

    readCommand.clear();// = 0xff;
    writeCommand.clear();//= 0xff;
}

hCVar::hCVar(DevInfcHandle_t h, variableType_t vt, int vSize)
    :hCitemBase(h, vt, vSize), pPsuedoDflt(NULL)
#ifdef _DEBUG
  ,isNotified(false)
#endif
{
    dataState   = IDS_UNINITIALIZED;
    dataQuality = DA_NOT_VALID;
    /// sjv 11.11.10 pEnum       = NULL;
    pClass      = NULL;
    pHandling   = NULL;
    pUnit       = NULL;
    pDefaultVal = NULL;
    isCached    = false;
    queryCount  = 0;
    dataStatus  = 1;// stevev 02/26/04 - 1 is normal, keep going(0-do not weigh)
    writeStatus = 0;// 0 is normal condition - 1 means user written

    height      = gS_xxxSmall;
    width       = gS_xxxSmall;

    varType     = vt;
    varSize     = vSize;
    readCommand .clear();//= 0xff;
    writeCommand.clear();//= 0xff;

    dominantRelations.clear();   
    // sjv 30nov06  unitRelation = INVALID_ITEM_ID; 
    unitRelations.clear();
    unitRelations.setOwner(getID(),devPtr());

    waoRelation  = InvalidItemID;   

    previousDataState = dataState;// stevev 5jul05
}

                        
hCVar::hCVar(hCVar* pSrc,  itemIdentity_t newID)
      :  hCitemBase((hCitemBase*)pSrc,newID), pPsuedoDflt(NULL)
#ifdef _DEBUG
  ,isNotified(false)
#endif
{   
    dataState   = IDS_UNINITIALIZED;
    dataQuality = DA_NOT_VALID;
    dataStatus  = 1;
    writeStatus = 0;
    queryCount = 0;

    varType = pSrc->varType; 
    varSize = pSrc->varSize;

    readCommand .clear();
    writeCommand.clear();
    returnString.erase(); //

    rdCmdList.clear();
    wrCmdList.clear();
    
    isCached            = pSrc->isCached;
    m_bLoopWarnVariable = pSrc->m_bLoopWarnVariable;
    
    dominantRelations.clear();   
    // sjv 30nov06  unitRelation = INVALID_ITEM_ID; 
    unitRelations.clear();      
    unitRelations.setOwner(getID(),devPtr());

    waoRelation  = InvalidItemID;   

    /// sjv 11.11.10 pEnum      = NULL;
    pClass      = NULL;
    pHandling   = NULL;
    pUnit       = NULL;
    pDefaultVal = NULL;
    previousDataState = dataState;// stevev 5jul05

/*  Convert to using original's attributes --- trying to save memory
    if (pSrc->pEnum     != NULL )
    {   pEnum = (hCattrEnum*)pSrc->pEnum->new_Copy();
    }
    if (pSrc->pClass    != NULL )
    {   pClass = (hCattrClass*)pSrc->pClass->new_Copy();
    }
    if (pSrc->pHandling != NULL )
    {   pHandling = (hCattrHndlg*)pSrc->pHandling->new_Copy();
    }
    if (pSrc->pUnit     != NULL )
    {   pUnit = (hCattrUnit*)pSrc->pUnit->new_Copy();
    }
**/ // stevev 20jul06 - share attributes //
    /// sjv 11.11.10 pEnum       = pSrc->pEnum;
    pClass      = pSrc->pClass;
    pHandling   = pSrc->pHandling;
    pUnit       = pSrc->pUnit;
    pDefaultVal = pSrc->pDefaultVal;
    
    pPsuedoDflt = pSrc->pPsuedoDflt;
    
    height      = pSrc->height;
    width       = pSrc->width;
    
    if ( ! sharedAttr ) // is still false <itembase should have set it to true>
    {
        LOGIF(LOGP_NOT_TOK)
                (CERR_LOG,"ERROR: programming: Var base class did not replicate correctly.\n");
    }
}


hCVar::~hCVar()
{//hCattrBase*  getaAttr(varAttrType_t attrType);
    // stevev 20jul06 - share attributes //
    if ( !  sharedAttr )
    {
        // stevev 15jul09 - psuedo attr variables
        DESTROY(pPsuedoDflt);// this one is shared too
        /*  some attributes are kept on the list too.  
            For those, let the base class delete 'em (called after this method)*/
        hCattrBase* pAB ;
#if 0 // turn this off
// debugging code
if (pHandling)
{
    LOGIT(CLOG_LOG,"HNDLG: 0x%04x var deleting %p\n",getID(),pHandling);
}
// end debugging code
#endif
        /// sjv 11.11.10 if ( (pAB = getaAttr(varAttrEnums))    == pEnum )       pEnum = NULL;
        // setting to NULL allows delete from list
        if ( (pAB = getaAttr(varAttrClass))    == pClass)          pClass= NULL;
        if ( (pAB = getaAttr(varAttrHandling)) == pHandling)       pHandling = NULL;
        if ( (pAB = getaAttr(varAttrConstUnit))== pUnit )          pUnit = NULL; 
        if ( (pAB = getaAttr(varAttrDefaultValue))== pDefaultVal ) pDefaultVal = NULL;
        /// sjv 11.11.10 RAZE(pEnum);//
        RAZE(pClass);//
        RAZE(pHandling);//
        RAZE(pUnit);//
        RAZE(pDefaultVal);
    }
}

RETURNCODE hCVar::destroy(void)
{
    RETURNCODE rc = 0;

    /*Vibhor 050404: Start of Code*/
    try{
    switch (varType) 
    {
    //case vT_Integer:      pR = new hCSigned(  h,   paItem);break;
    //case vT_Unsigned:     pR = new hCUnsigned(h,   paItem);break;
    //case vT_FloatgPt:     pR = new hCFloat(   h,   paItem);break;
    //case vT_Double:           pR = new hCDouble(  h,   paItem);break;
    case vT_Enumerated:     
        rc = ((hCEnum*)this)->destroy(); break;
    case vT_BitEnumerated:  
        rc = ((hCBitEnum*)this)->destroy();break;/* I don't believe this is ever used */
    //case vT_Index:            pR = new hCindex(   h,   paItem);break;
    //case vT_Ascii:            pR = new hCascii(   h,   paItem);break;
    //case vT_PackedAscii:    pR = new hCpackedAscii(h,paItem);break;
    //case vT_HartDate:     pR = new hChartDate(   h,paItem);break;
    //case vT_Password:     //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_BitString:        //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_Time:         //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_DateAndTime:  //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_Duration:     //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_EUC:          
    //  pR = NULL; break;   //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_OctetString:  //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_VisibleString:    //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_TimeValue:        //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
    //case vT_Boolean:      //>>>>>>>>NOT USED IN HART  pR = new CvTboolean;break;
    default:
        break;
    }// end switch



    // stevev 20jul06 - share attributes //
    if ( !  sharedAttr )
    {
        hCattrBase* pAB ;
        /// sjv 11.11.10 if ( (pAB = getaAttr(varAttrEnums))    == pEnum )    pEnum = NULL; 
        // setting attribute to NULL allows destroy from list
        if ( (pAB = getaAttr(varAttrClass))    == pClass)           pClass= NULL;
        if ( (pAB = getaAttr(varAttrHandling)) == pHandling)        pHandling = NULL;
        if ( (pAB = getaAttr(varAttrConstUnit))== pUnit )           pUnit = NULL;
        if ( (pAB = getaAttr(varAttrDefaultValue))== pDefaultVal )  pDefaultVal = NULL; 
    /// sjv 11.11.10 DESTROY(pEnum);
    DESTROY(pClass);
    DESTROY(pHandling);
    DESTROY(pUnit);
    DESTROY(pDefaultVal);
    /* do it for hCitemBase without deleteing*/ 
        // setting attribute to NULL allows destroy from list
        if ( (pAB = getaAttr(varAttrValidity)) == pValid) pValid = NULL; 
        if ( (pAB = getaAttr(varAttrLabel))    == pLabel) pLabel = NULL; 
        if ( (pAB = getaAttr(varAttrHelp))     == pHelp)  pHelp  = NULL; 
        if ( (pAB = getaAttr(varAttrDebugInfo))== pDebug) pDebug = NULL;
    DESTROY(pValid);
    DESTROY(pLabel);
    DESTROY(pHelp);
    DESTROY(pDebug);
    }
    //hCitemBase::destroy();
        
    }
    catch(...)
    {
        LOGIT(CLOG_LOG|CERR_LOG,"hCVar::destroy()  0x%04x exception in catch\n",getID());
    }

    return SUCCESS;
/*Vibhor 050404: End of Code*/
}

hCattrBase*   hCVar::newHCattr(DevInfcHandle_t h, aCattrBase* pACattr)  // builder 
{
    hCattrBase*    pReturnPtr = NULL;
    if (pACattr==NULL)
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: variable's newHCattr() passed a null aCattr.\n");
    }
    else //- generate one
    {
        varAttrType_t  attrType = (varAttrType_t)mask2value(pACattr->attr_mask);
        switch (attrType)
        {// generate a hart class from the abstract class
		case  varAttrWidth:	// 5	was read timeout									
			pReturnPtr  = (hCattrBase*)new hCattrGrafixSize(h, (aCattrCondLong*)pACattr );
			DEBUGLOG(CLOG_LOG,"Width attribute may have been Read Timeout.\n");
            break;
		case  varAttrHeight:	// 6 was write timeout										
			pReturnPtr  = (hCattrBase*)new hCattrGrafixSize(h, (aCattrCondLong*)pACattr );		
			DEBUGLOG(CLOG_LOG,"Height attribute may have been Write Timeout.\n");		
            break;
        case  varAttrPreReadAct://,                                     
            pReturnPtr  = (hCattrBase*)new hCattrVarPreRdAct(h, (aCattrActionList*)pACattr );
            break;
        case  varAttrPostReadAct://,                                    
            pReturnPtr  = (hCattrBase*)new hCattrVarPostRdAct(h,(aCattrActionList*)pACattr );
            break;
        case  varAttrPreWriteAct://,    //10                            
            pReturnPtr  = (hCattrBase*)new hCattrVarPreWrAct(h, (aCattrActionList*)pACattr );
            break;
        case  varAttrPostWriteAct://,                                   
            pReturnPtr  = (hCattrBase*)new hCattrVarPostWrAct(h, (aCattrActionList*)pACattr );
            break;
        case  varAttrPreEditAct://,                                     
            pReturnPtr  = (hCattrBase*)new hCattrVarPreEditAct(h, (aCattrActionList*)pACattr );
            break;
        case  varAttrPostEditAct://,                                    
            pReturnPtr  = (hCattrBase*)new hCattrVarPostEditAct(h,(aCattrActionList*)pACattr );
            break;

        case  varAttrDisplay://,                                        
            pReturnPtr  = (hCattrBase*)new hCattrVarDisplay(h, (aCattrString*)pACattr );
            break;
        case  varAttrEdit://,                                           
            pReturnPtr  = (hCattrBase*)new hCattrVarEdit(h, (aCattrString*)pACattr );
            break;

        case  varAttrMinValue://,                                       
            pReturnPtr  = (hCattrBase*)new hCattrVarMinVal(h, (aCattrVarMinMaxList*)pACattr );
            break;
        case  varAttrMaxValue://,                                       
            pReturnPtr  = (hCattrBase*)new hCattrVarMaxVal(h, (aCattrVarMinMaxList*)pACattr );
            break;
        case  varAttrScaling://,        //20                            
            pReturnPtr  = (hCattrBase*)new hCattrScale(h, (aCattrCondExpr*)pACattr );
            break;

        case  varAttrIndexItemArray://,                                 
            pReturnPtr  = (hCattrBase*)new hCattrVarIndex(h, (aCattrCondReference*)pACattr );
            break;

        case  varAttrDefaultValue://,                                   
            pReturnPtr  = (hCattrBase*)new hCattrVarDefault(h, (aCattrCondExpr*)pACattr );
            break;

        case  varAttrRefreshAct:                                
            pReturnPtr  = (hCattrBase*)new hCattrVarRefreshAct(h, (aCattrActionList*)pACattr );
            break;  
        case  varAttrPostRequestAct:
            if ( devPtr()->getPolicy().isReplyOnly )// only filled in simulator
                pReturnPtr  = 
                    (hCattrBase*)new hCattrVarPostRequestAct(h, (aCattrActionList*)pACattr );
            break;
        case  varAttrPostUserAct:
            if ( devPtr()->getPolicy().isReplyOnly )// only filled in simulator
                pReturnPtr  = 
                    (hCattrBase*)new hCattrVarPostUserAct(h, (aCattrActionList*)pACattr );
            break;
/* done in time value...these should never be called due to the filters in newHCattr but we'll
    comment them them out anyway...stevev 27jan11 - inventsys checkin
        case  varAttrTimeFormat:            // added 02jun08                                
            pReturnPtr  = (hCattrBase*)new hCattrVarTimeFormat(h, (aCattrString*)pACattr );
            break;  
        case  varAttrTimeScale:             // added 02jun08                                
            pReturnPtr  = (hCattrBase*)new hCattrTimeScale(h, (aCattrBitstring*)pACattr );
            break;  
***/
/* Now done in ItemBase:::
        case  varAttrDebugInfo:
            LOGIT(CLOG_LOG, "    - hCVar's newHCattr is not implemented for %s\n",
                varAttrStrings[attrType]);
            break;
** end now donw ***/
		case  varAttrPrivate:
			if ( devPtr()->getPolicy().isReplyOnly )// only filled in simulator
				pReturnPtr  = 
					(hCattrBase*)new hCattrPrivate(h, (aCattrLong*)pACattr );
			break;
		case  varAttrVisibility: // not supported by HART
        default:
            LOGIT(CERR_LOG, "    - hCVar's newHCattr is not implemented for %s\n",
                varAttrStrings[attrType]);
            break;
        }// endSwitch on attribute type
        if (pReturnPtr)
        {
            pReturnPtr->setItemPtr((hCitemBase*)this);
        }
#ifdef XX_DEBUG
        if(attrType == varAttrMinValue)
        {
            ((hCattrVarMinVal*)pReturnPtr)->dumpSelf(3);
        }
#endif
    }
    return pReturnPtr;
}

/****************************** ASSUMPTION *****************************/
/* The type doesn't really change...even though it's a conditional.....*/

/*static*/  //Returns null ptr on error
//            Construct a new hCVar type class from an equivellent aCvarType class
hCVar* hCVar::newVar(DevInfcHandle_t h, aCitemBase * paItem) // from api variable base class
{
    void* pR = NULL;
    //aCvarTypeDescriptor t;
    variableType_t vt;
    int vs; // size
    

    aCattrBase* pB = paItem->getaAttr(varAttrTypeSize);
    if ( pB == NULL)
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:The hCVar didn't find the TypeType descriptor.\n");
        return NULL;
    }
        


    vt = (variableType_t)(((aCattrTypeType*)pB)->notCondTypeType.actualVarType);
    //t = pVariable->getType();
    //vt = (variableType_t)paItem->itemType.getType(); 
    vs = paItem->itemSize; 

    if ( vt < vT_Integer || vt >= vT_MaxType )
    {
      LOGIF(LOGP_NOT_TOK)
      (CERR_LOG,"ERROR: unknown type in type descriptor in newVar's TypeType: %d\n",((int)vt));
    }
    else
    {
        switch (vt) 
        {
        case vT_Integer:        pR = new hCSigned(  h,   paItem);break;
        case vT_Unsigned:       pR = new hCUnsigned(h,   paItem);break;
        case vT_FloatgPt:       pR = new hCFloat(   h,   paItem);break;
        case vT_Double:         pR = new hCDouble(  h,   paItem);break;
        case vT_Enumerated:     pR = new hCEnum(    h,   paItem);break;/* may be unused */
        case vT_BitEnumerated:  pR = new hCBitEnum( h,   paItem);break;/* may be unused */
        case vT_Index:          pR = new hCindex(   h,   paItem);break;
        case vT_Ascii:          pR = new hCascii(   h,   paItem);break;
        case vT_PackedAscii:    pR = new hCpackedAscii(h,paItem);break;
        case vT_HartDate:       pR = new hChartDate(   h,paItem);break;
        case vT_Password:       pR = new hCpassword(   h,paItem);break;// ascii with flag
        case vT_TimeValue:      pR = new hCTimeValue(  h,paItem);break;// stevev 19feb08
        case vT_BitString:      //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_Time:           //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_DateAndTime:    //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_Duration:       //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_EUC:
            LOGIT(CERR_LOG,
                    "WARNING: call to an Unimplemented hCVar constructor! : %d\n",((int)vt));
            pR = NULL; break;   //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_OctetString:    //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_VisibleString:  //>>>>>>>>>>>>>>>>NOT IMPLEMENTED YET
        case vT_Boolean:        //>>>>>>>>NOT USED IN HART  pR = new CvTboolean;break;
        default:
            pR = NULL;
            LOGIT(CERR_LOG,"WARNING: hCVar Unknown Type's Type: %d\n",((int)vt));
            break;
        }// end switch
    }// end quality else

    //  vT_Enumerated || vT_BitEnumerated  handled elsewhere
    return ((hCVar*) pR);
}

/* <START> stevev 2/11/04 - new function, only works for device simulation */
//   sets the variable type,  returns the old variable type 
int  hCVar::VariableType(variableType_t newType)
{
    variableType_t rt = varType;
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly && newType > vT_unused &&  newType < vT_MaxType)
    {// device simulation
        varType = newType;
    }
    // else do nothing
    return rt;
}


RETURNCODE hCVar::getDominant(itemIDlist_t& returnItemIdList)
{
    returnItemIdList = dominantRelations;
    if (returnItemIdList.size() == dominantRelations.size())
    {
        return SUCCESS;
    }
    else
    {
        return FAILURE;
    }
}


void hCVar::setDomRel(ITEM_ID domRelationID) 
{// verify not there already
    for (itemIDlist_t::iterator iT = dominantRelations.begin();iT<dominantRelations.end();iT++)
    {
        if ( *iT == domRelationID )
        {// it's already there
            return;
        }
    }
    dominantRelations.push_back(domRelationID);
    return;
}

/* stevev 30nov06 - moved to pass thru to hCunitRelList-------------------
ITEM_ID hCVar::getUnitRel(void)
{
    return 0;// failure for now!!!!!!!!!!!!!!!!
}
---------------------------end stevev 30nov06----------------------------*/

RETURNCODE hCVar::MarkDependStale(void)
{
    RETURNCODE rc = SUCCESS;
    hCitemBase* pItemBase;
    hCrelation* pRel;

    if (dominantRelations.size() > 0)
    {
        for(itemIDlist_t::iterator iT = dominantRelations.begin();
                                    iT < dominantRelations.end();       iT++)
        {// iT is a ptr 2 an ITEM_ID
            rc = devPtr()->getItemBySymNumber(*iT, &pItemBase);// a refresh or unit item
            if ( rc == SUCCESS && pItemBase != NULL )
            {
                pRel = (hCrelation*)pItemBase;
                if ( ! pRel->isIndependent(getID()) )
                {//              we are not in the dominant portion of this relation
                    continue; // skip the action, go to next dominant
                }
                rc = pRel->MarkDependentsStale();
                if ( rc != SUCCESS )
                {// just log it                 
                    LOGIT(CLOG_LOG,"Relation:(%s) failed to mark dependents stale.\n",
                                                                pItemBase->getName().c_str() );
                }
            }
            else
            {
                LOGIF(LOGP_NOT_TOK)(CERR_LOG,
                    "ERROR: dominantRelation list had a bad symbol number.(0x%04x)\n",  (*iT));
            }
        }// next
    }// else just return success

    return rc;
}

void hCVar::addRDdesc(hCcommandDescriptor& rdD)
{
    cmdDescListIT itRdCmds;
    hCcommandDescriptor* pCD;
    bool rdCmdUsed = false;
int y = rdD.idxList.size();

    for ( itRdCmds = rdCmdList.begin(); itRdCmds != rdCmdList.end(); ++itRdCmds)
    {// itRdCmds isa ptr 2a hCcommandDescriptor
        pCD = &(*itRdCmds);
        if (pCD->cmdNumber == rdD.cmdNumber &&  pCD->transNumb == rdD.transNumb)
        {// there can only be one per command
            indexUseList_t::iterator itIUL, itInIUL;
            hIndexUse_t *pExIUt, *pInIUt;
            bool foundDup = false;

            // insert unique     
            // for each in the incoming index list
            for (itInIUL = rdD.idxList.begin();  itInIUL != rdD.idxList.end(); ++itInIUL)
            {// itInIUL ia ptr2a hIndexUse_t  ... for each incoming index (should never be > 1)
                pInIUt = &(*itInIUL);
                // for each of the existing index list
                for (itIUL = pCD->idxList.begin();  itIUL != pCD->idxList.end(); ++itIUL)
                {// itIUL ia ptr2a hIndexUse_t... for each existing index
                    pExIUt = &(*itIUL);
                    if (pInIUt->indexSymID     == pExIUt->indexSymID &&
                        pInIUt->indexDispValue == pExIUt->indexDispValue &&
                        pInIUt->devIdxSymID    == pExIUt->devIdxSymID &&
                        pInIUt->devVarRequired == pExIUt->devVarRequired    )
                    {
                        /* stevev 10feb09 - if this incoming index value (equal to old or not) 
                                            weighs more, use it ***/
                        if (rdD.rd_wrWgt > pCD->rd_wrWgt)
                        {
                            pCD->rd_wrWgt = rdD.rd_wrWgt;
                            assert(pInIUt->indexSymID != (int)(pInIUt->indexDispValue));
                            pExIUt->indexDispValue  = pInIUt->indexDispValue;// error
                        }// else leave it as is
                        foundDup = true; // cmd/trans/index
#ifdef DBG_RD_VAR
						if (itemId == DBG_RD_VAR )
						{
							LOGIT(CLOG_LOG,"AddReadDesc: DUPLICATE: c:%d t:%d I:0x%04x V:%d\n",
					  rdD.cmdNumber, rdD.transNumb, pInIUt->indexSymID, pInIUt->indexDispValue);
						}
#endif
                    }// else keep looking
                }// next existing
                if (! foundDup) // this is a new index for the same cmd/transaction
                {// insert incoming into existing
                    pCD->idxList.push_back(*pInIUt);
#ifdef DBG_RD_VAR
					if (itemId == DBG_RD_VAR )
					{
						LOGIT(CLOG_LOG,"AddReadDesc: NEW INDEX: c:%d t:%d I:0x%04x V:0x%04x\n",
					  rdD.cmdNumber, rdD.transNumb, pInIUt->indexSymID,pInIUt->indexDispValue);
					}
#endif
                }
                foundDup = false;// stevev 06jan09 - clear for next round
            }// next incoming (seldom seen)
            // note that an index list larger than the needed indexes may be generated

            rdCmdUsed = true;// found the command/transaction entry
            break;           // out of for loop <there is only one>
        }// else, continue to next read command
    }// next rd cmd
    if (! rdCmdUsed)
    {// we haven't seen this cmd/transaction yet
        rdCmdList.push_back(rdD);// insert the entire command
#ifdef DBG_RD_VAR
		if (itemId == DBG_RD_VAR )
		{
			LOGIT(CLOG_LOG,"AddReadDesc: NEW: c:%d t:%d Icnt:%d (now %d)\n",
			rdD.cmdNumber, rdD.transNumb, rdD.idxList.size(),rdCmdList.size());
		}
#endif
    }
}

void hCVar::addWRdesc(hCcommandDescriptor& wrD)
{
/* stevev 19jan16 - this has been a straight insertion for a very long time.
	Detecting the duplicates in the PTOC table has led to modifying the situation.
	was::
    wrCmdList.push_back(wrD);
***/	
    cmdDescListIT itWrCmds;
    hCcommandDescriptor* pCD;
    bool wrCmdUsed = false;
int y = wrD.idxList.size();

    for ( itWrCmds = wrCmdList.begin(); itWrCmds != wrCmdList.end(); ++itWrCmds)
    {// itWrCmds isa ptr 2a hCcommandDescriptor
        pCD = &(*itWrCmds);
        if (pCD->cmdNumber == wrD.cmdNumber &&  pCD->transNumb == wrD.transNumb)
        {// there can only be one per command
            indexUseList_t::iterator itIUL, itInIUL;
            hIndexUse_t *pExIUt, *pInIUt;
            bool foundDup = false;

            // insert unique     
            // for each in the incoming index list
            for (itInIUL = wrD.idxList.begin();  itInIUL != wrD.idxList.end(); ++itInIUL)
            {// itInIUL ia ptr2a hIndexUse_t  ... for each incoming index (should never be > 1)
                pInIUt = &(*itInIUL);
                // for each of the existing index list
                for (itIUL = pCD->idxList.begin();  itIUL != pCD->idxList.end(); ++itIUL)
                {// itIUL ia ptr2a hIndexUse_t... for each existing index
                    pExIUt = &(*itIUL);
                    if (pInIUt->indexSymID     == pExIUt->indexSymID &&
                        pInIUt->indexDispValue == pExIUt->indexDispValue &&
                        pInIUt->devIdxSymID    == pExIUt->devIdxSymID &&
                        pInIUt->devVarRequired == pExIUt->devVarRequired    )
                    {
                        /* stevev 10feb09 - if this incoming index value (equal to old or not) 
                                            weighs more, use it ***/
                        if (wrD.rd_wrWgt > pCD->rd_wrWgt)
                        {
                            pCD->rd_wrWgt = wrD.rd_wrWgt;
                            assert(pInIUt->indexSymID != (int)(pInIUt->indexDispValue));
                            pExIUt->indexDispValue  = pInIUt->indexDispValue;// error
                        }// else leave it as is
                        foundDup = true; // cmd/trans/index
#ifdef DBG_WR_VAR
						if (itemId == DBG_WR_VAR )
						{
							LOGIT(CLOG_LOG,"addWRdesc: DUPLICATE: c:%d t:%d I:0x%04x V:%d\n",
					  wrD.cmdNumber, wrD.transNumb, pInIUt->indexSymID, pInIUt->indexDispValue);
						}
#endif
                    }// else keep looking
                }// next existing
                if (! foundDup) // this is a new index for the same cmd/transaction
                {// insert incoming into existing
                    pCD->idxList.push_back(*pInIUt);
#ifdef DBG_WR_VAR
					if (itemId == DBG_WR_VAR )
					{
						LOGIT(CLOG_LOG,"AddReadDesc: NEW INDEX: c:%d t:%d I:0x%04x V:0x%04x\n",
					  wrD.cmdNumber, wrD.transNumb, pInIUt->indexSymID,pInIUt->indexDispValue);
					}
#endif
                }
                foundDup = false;// stevev 06jan09 - clear for next round
            }// next incoming (seldom seen)
            // note that an index list larger than the needed indexes may be generated

            wrCmdUsed = true;// found the command/transaction entry
            break;           // out of for loop <there is only one>
        }// else, continue to next read command
    }// next rd cmd
    if (! wrCmdUsed)
    {// we haven't seen this cmd/transaction yet
        wrCmdList.push_back(wrD);// insert the entire command
#ifdef DBG_WR_VAR
		if (itemId == DBG_WR_VAR )
		{
			LOGIT(CLOG_LOG,"addWRdesc: NEW: c:%d t:%d Icnt:%d (now %d)\n",
			wrD.cmdNumber, wrD.transNumb, wrD.idxList.size(),rdCmdList.size());
		}
#endif
    }
}


/* stevev 012904 make command selection independent of variable handling.
   Variable handling is a UI function - the command RD/WR determines th
   capability of sending to a device
*/
int  hCVar::setRdWrCmds(void) // hi word = Rd, lo word = Wr commands on return
{
	int r = 0;
	cmdDescListIT icd;
	hCcommandDescriptor* pFirstBest = NULL;
	/* stevev 012904   :: attrHandling_t handling;  :: */
	maskClass_t    dataclass;
	hCbitString*	pBS = NULL;

	if (IsLocal() )
	{
		if LOGTHIS(LOGP_DD_INFO) 
		{
			LOGIT(COUT_LOG,"Item 0x04x LOCAL: No command selected\n",getID());
		}
		readCommand.clear();
		writeCommand.clear();
		return (readCommand.cmdNumber << 16) | (writeCommand.cmdNumber & 0x0000ffff);
	}
	if ( pClass != NULL  &&  (pBS = (hCbitString*) (pClass->procure()) ) != NULL )
	{
		//pClass->getBstring(dataclass);
		dataclass = (maskClass_t)pBS->getBitStr();
	}
	else
	{
		dataclass = maskNoClass;
	}
	/* stevev 012904 	
	if ( pHandling != NULL  &&  (pBS = (hCbitString*) (pHandling->procure()) ) != NULL  )
	{
		//pHandling->getBstring(handling);
		handling = (attrHandling_t)pBS->getBitStr();
	}
	else
	{
		//handling = handlingUnknown;
		handling = (attrHandling_t)(READ_HANDLING | WRITE_HANDLING);
	}
	stevev 012904 end */


	for(icd = rdCmdList.begin();icd < rdCmdList.end(); icd++)
	{// icd is a ptr 2 a hCcommandDescriptor
/*TEMP-try to eliminate cmd 1 for now */
//if (icd->cmdNumber == 1 && rdCmdList.size() > 1 )
//{
//	continue;// skip it
//}
		if ( pFirstBest == NULL)
		{
			pFirstBest = (hCcommandDescriptor*)(&(*icd));
		}
		else
		{
			if ( icd->rd_wrWgt > pFirstBest->rd_wrWgt )
			{
				pFirstBest = (hCcommandDescriptor*)(&(*icd));
			}
		}
	}
	if (pFirstBest == NULL )
	{
		readCommand.clear();// = 0xff; // not local and has read handling
		/* stevev 012904::comment out
		// if ( ((dataclass & maskLocal) == 0) && ((handling & handlingRead) != 0) )*/
		if ( (dataclass & maskLocal) == 0 )
		{
			if LOGTHIS(LOGP_DD_INFO) 
			{
				LOGIT(CLOG_LOG|COUT_LOG," Variable 0x%04x  %s has no Read  command.\n",
															getID(),getName().c_str() );
			}
		}
		else
		if ((dataclass & maskLocal) == 0)
		{
			if LOGTHIS(LOGP_DD_INFO) 
			{
				LOGIT(COUT_LOG,"Item 0x%04x Has a LOCAL Mask:\n",getID());
			}
		}
	}
	else
	{
		if LOGTHIS(LOGP_DD_INFO) 
		{
			LOGIT(COUT_LOG,"Item 0x%04x READ  Command:",getID());
			pFirstBest->dumpSelf();
		}
		readCommand = *pFirstBest;//->cmdNumber;
	}

	pFirstBest = NULL;
	for(icd = wrCmdList.begin();icd < wrCmdList.end(); icd++)
	{// icd is a ptr 2 a hCcommandDescriptor
		if ( pFirstBest == NULL)
		{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
			pFirstBest = &(*icd);
#else
			pFirstBest = (hCcommandDescriptor*)icd;
#endif
		}
		else
		{
			if ( icd->rd_wrWgt > pFirstBest->rd_wrWgt )
			{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
				pFirstBest = &(*icd);
#else
				pFirstBest = (hCcommandDescriptor*)icd;
#endif
			}
		}
	}
	if (pFirstBest == NULL )
	{
		writeCommand.clear();// = 0xff;// not local and has write handling
		/* stevev 012904::comment out
		if ( ((dataclass & maskLocal) == 0) && ((handling & handlingWrite) != 0) )*/
		if ( (dataclass & maskLocal) == 0)
		{
			if LOGTHIS(LOGP_DD_INFO) 
			{
				LOGIT(CLOG_LOG|COUT_LOG,
					" Variable 0x%04x  %s has no Write command.\n",getID(),getName().c_str());
			}
		}
		else
		if ((dataclass & maskLocal) == 0)
		{
			if LOGTHIS(LOGP_DD_INFO) 
			{
				LOGIT(COUT_LOG,"Item 0x%4x Has a LOCAL Mask:\n",getID());
			}
		}
	}
	else
	{
		if LOGTHIS(LOGP_DD_INFO) 
		{
			LOGIT(COUT_LOG,"Item 0x%4x WRITE Command:",getID());
			pFirstBest->dumpSelf();
		}
		writeCommand = *pFirstBest;//->cmdNumber;
	}

	r = (readCommand.cmdNumber << 16) | (writeCommand.cmdNumber & 0x0000ffff);

	return r;
}
/* added 25feb2014 - there are 40 entries for a var in command 9 in the test DD
 *                   we need to try and reduce that number by eliminating
 *					 those with a) duplicate local var AND b) have a device var
 * This is an extenstion of setRdWrCmds() above (used for read and write)
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void hCVar::filterIndexes(hCcommandDescriptor* pCmdDesc)
{
	idxUseIT             itIU;
    hIndexUse_t*         pUI;
	indexUseList_t       localList;

	if (pCmdDesc == NULL || pCmdDesc->idxList.size() < 2)
	{
		return; //there is nothing to do
	}

    for (itIU = pCmdDesc->idxList.begin(); itIU != pCmdDesc->idxList.end();++itIU)
    {
        pUI = (hIndexUse_t*) &(*itIU);
        if (pUI == NULL ) continue;

		// remove the index entries that are not needed
		// for now, if there are single value indexes, delete all the multivalue indexes
		if ( pUI->indexSymID && pUI->devIdxSymID == NULL )
		{
			localList.push_back(*pUI);// has no device index requirement
		}
	}// next index

	if ( localList.size() > 0 && localList.size() < pCmdDesc->idxList.size() )
	{// remove the ones not in localList
		pCmdDesc->idxList.erase(
			remove_if(pCmdDesc->idxList.begin(),pCmdDesc->idxList.end(),hasDevIdx),
														pCmdDesc->idxList.end() );
	}
	//else local list is empty or hass all of 'em
	//	do nothing
}


/**************
 * helper function
 *  insert or freshen values for indexes
 **************/
RETURNCODE hCVar::updateIndexMap( hCtransaction* pThisTrans, 
                                   ddbItemList_t& indexPtrList, int newIdxValue, bool adding)
{

    RETURNCODE rc = SUCCESS;
    if ( pThisTrans == NULL || indexPtrList.size() < 1 || newIdxValue < 0 )
    {
        return APP_PARAMETER_ERR;
    }

    hCIndexValue2Variable*    pIndxV2V = NULL;
    ddbItemLst_it             ppItmBase;
    hCindex *                 pIdx;
    valueNVarsIT_t            pIdxVnV;
    hCIndexValueAndVariables* pIndexVnV = NULL;
    varPtrLstIT_t             ppVar;
    hCVar*                    pVariable = NULL;
    bool                      bExactMatch = false;

    // for each index used 
    for ( ppItmBase = indexPtrList.begin(); ppItmBase != indexPtrList.end(); ++ppItmBase)
    {
        pIdx     = ( hCindex*) (*ppItmBase);
        pIndxV2V = pThisTrans->getMap4Index(pIdx->getID());// NULL on error
        if (pIndxV2V != NULL) // then update it
        {
            assert(pIndxV2V->getID() == pIdx->getID());
            if (pIndxV2V->size() > 0)
            {
                for (pIdxVnV = pIndxV2V->begin(); pIdxVnV != pIndxV2V->end(); ++pIdxVnV)
                {// for each value of that index.. ptr2.hCIndexValueAndVariables
                    pIndexVnV = (hCIndexValueAndVariables*)&(*pIdxVnV);// for 'new' Bill
                    if ( pIndexVnV->indexValue == newIdxValue )
                    {// value already exists, see if we're there
                        for (ppVar = pIndexVnV->resolvedVarList.begin();
                             ppVar != pIndexVnV->resolvedVarList.end();  ++ ppVar)
                        {// for each resolved variable
                            pVariable = (hCVar*) (*ppVar);// for 'new' Bill
                            if ( pVariable->getID() == getID() )
                            {
                                bExactMatch = true; 
                                break; // out of for loop
                            }
                            // else keep going
                        }// next variable for this value
                        // bExactMatch is set | not
                        break; // out of value loop w/ ptr in pIndexVnV & bExactMatch set | clr
                    }
                    else
                    {// this ain't it
                        pIndexVnV = NULL;//  & bExactMatch clr
                    }
                }// next value

                if (pIndexVnV == NULL)// no values match
                {// add value, var
                    hCIndexValueAndVariables localVnV;
                    localVnV.indexValue = newIdxValue;
                    localVnV.resolvedVarList.push_back(this);
                    pIndxV2V->push_back(localVnV);
                    localVnV.clear();
                }
                else // a value matched
                if ( ! bExactMatch )// but this variable isn't there
                {// add var
                    pIndexVnV->appendUnique(this);
                }
                else // variable and value already exist
                {
                    DEBUGLOG(CLOG_LOG,"A Variable AND Value already exists for this Var.\n");
                }
            }
            else
            {
                DEBUGLOG(CLOG_LOG,"Index Map has no indexes.\n");
            }
        }
        else // add if its not used
        {// add index, value, var
            hIndexUse_t iut;
            iut.indexSymID = pIdx->getID();
            iut.indexDispValue = newIdxValue;
            indexUseList_t lLst; lLst.push_back(iut);//31jan14 - convert to list - may be wrong solution
            pThisTrans->insert2Map(lLst, this);
        }
    }// next index used  

     return rc;
 }

/**************
 * helper function...
 *      common code for the function below
 * stevev 22oct09 - this only resets it in the command descriptor
 *                - add function to reset/insert for into indexNVar indexMap
 **************/
RETURNCODE hCVar::resetCmdDesc(hCcommandDescriptor* pCD, 
                                            ddbItemList_t& indexPtrList, int newIdxValue)
 {
    idxUseIT             itIU;
    hIndexUse_t*         pUI;
    ddbItemLst_it        ppItBs;
    ptr2hCitemBase_t     pItmBs;

    if (pCD == NULL) return FAILURE;
    for (itIU = pCD->idxList.begin(); itIU != pCD->idxList.end();++itIU)
    {
        pUI = (hIndexUse_t*) &(*itIU);
        if (pUI == NULL ) continue;

        for ( ppItBs = indexPtrList.begin(); ppItBs != indexPtrList.end(); ++ppItBs)
        {
            pItmBs = (ptr2hCitemBase_t)(*ppItBs);
            if ( pItmBs == NULL ) continue;
            if ( pItmBs->getID() == pUI->indexSymID )
            {
                pUI->indexDispValue = newIdxValue;
            }// else we leave it lay
        }//next list-index (passed in)
    }// next used index

    CCmdList*      pCmdList = NULL;
    hCcommand*     pThisCmd = NULL;
    hCtransaction* pThisTrans =  NULL;
    
    pCmdList= (CCmdList*) devPtr()->getListPtr(iT_Command);//<hCcommand*>
    if ( pCmdList != NULL )
    {
        pThisCmd = pCmdList->getCmdByNumber(pCD->cmdNumber);
        if ( pThisCmd != NULL )
        {
            pThisTrans = pThisCmd->getTransactionByNumber(pCD->transNumb);
            if ( pThisTrans != NULL )
            {
                updateIndexMap( pThisTrans, indexPtrList, newIdxValue, true);
            }
            else
            {
                DEBUGLOG(CLOG_LOG,"resetCmdDesc() failed to find Transaction %d in Cmd %d\n",
                    pCD->transNumb, pCD->cmdNumber);
            }
        }
        else
        {
            DEBUGLOG(CLOG_LOG,"resetCmdDesc() failed to find Command # %d\n", pCD->cmdNumber);
        }
    }       
    else
    {
        DEBUGLOG(CLOG_LOG,"resetCmdDesc() failed finding Cmd list in Device(debug only)\n");
    }

    return SUCCESS;
 }

/* stevev 22jan09 - this is where the work is done for changing the indexes of list
 *                  content after an insertion
 *
 * only implemented for variables and containers at this time 
 * the containers pass thru the info to final psuedovar
 *
 *  This routine goes through all of the rd and wr command descriptions and, for all
 *  indexes listed in   indexPtrList   ,changes the value to    newIdxValue
 *   stevev 22oct09 -> those that don't exist yet, get added -
 */
RETURNCODE hCVar::setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue)
{
    RETURNCODE rc = SUCCESS;
    if ( indexPtrList.size() < 1 ) return FAILURE;
    cmdDescListIT itCD;
    hCcommandDescriptor* pCD;
#ifdef _DEBUG
	int symNum = itemId;
#endif

    for ( itCD = rdCmdList.begin(); itCD != rdCmdList.end(); ++ itCD )
    {
        pCD = (hCcommandDescriptor*) &(*itCD);
        if (pCD == NULL) continue;

        rc |= hCVar::resetCmdDesc(pCD, indexPtrList, newIdxValue);

    }// next command descriptor
    rc |= hCVar::resetCmdDesc(&readCommand, indexPtrList, newIdxValue);
    if ( rc != SUCCESS )
        return rc;
    
    for ( itCD = wrCmdList.begin(); itCD != wrCmdList.end(); ++ itCD )
    {
        pCD = (hCcommandDescriptor*) &(*itCD);
        if (pCD == NULL) continue;

        rc |= hCVar::resetCmdDesc(pCD, indexPtrList, newIdxValue);
    }// next command descriptor
    rc |= hCVar::resetCmdDesc(&writeCommand, indexPtrList, newIdxValue);
    return rc;
};

/* stevev 22jan09 - this is where the work is done for adding the indexes of list
 *                  content for insertion
 *
 * only implemented for variables and containers at this time 
 * the containers pass thru the info to final psuedovar
 *
 *  This routine just routes it to the proper location
 */
RETURNCODE hCVar::addCmdDesc(hCcommandDescriptor& rdD, bool isRead)
{
    if ( isRead )
        addRDdesc(rdD);
    else
        addWRdesc(rdD);
    return SUCCESS; 
}


bool hCVar::IsDynamic(void)//the only class the counts          
{// this is a PROCURE call - any unresolved conditional will return default false
    ulong bst = 0;

    if ( pClass == NULL ) 
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Variable has no Class (attribute).\n");
        return DFLT_ISDYNAMIC; // the default class
    }

    // bitstring.bitstringVal
    hCbitString* pbs = (hCbitString*) (pClass->procure());
    if (pbs != NULL)
    {
	ulong classBS = pbs->getBitStr();

        if ( classBS & maskDynamic )
        {
            return true;
        }
        else
        {
            return false;//01dec06-was 'return true;' found by Vibhor
        }
    }
    //else - class has output an error, just return the default;
    return DFLT_ISDYNAMIC; // default class
}

bool hCVar::IsLocal(void)//the only class the counts            
{// this is a PROCURE call - any unresolved conditional will return default false
    ulong bst = 0;

    if ( pClass == NULL ) 
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Variable has no Class (attribute).\n");
        return DFLT_ISLOCAL; // the default class
    }

    // bitstring.bitstringVal
    hCbitString* pbs = (hCbitString*) (pClass->procure());
    if (pbs != NULL)
    {
	ulong classBS = pbs->getBitStr();

        if ( classBS & maskLocal )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //else - class has output an error, just return the default;
    return DFLT_ISLOCAL; // default class
}


bool hCVar::IsReadOnly(void)
{
    ulong bst = 0;

    if ( pHandling == NULL ) 
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Variable has no Handling attribute.\n");
        return DFLT_ISREADONLY; // default R/W
    }

    hCbitString* pbs = (hCbitString*) (pHandling->procure());
    if (pbs != NULL)
    {
	ulong handlingBS = pbs->getBitStr();

        if ( ! (handlingBS & WRITE_HANDLING) ) // if not writable then read only
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //else - handling has output an error, just return the default;
    return DFLT_ISREADONLY; // default class
}

bool hCVar::IsConfig(void)
{
    ulong bst = 0;

    if ( pHandling == NULL ) 
    {
        return false;
    }

    hCbitString* pbs = (hCbitString*) (pHandling->procure());
    if (pbs != NULL)
    {
	ulong handlingBS = pbs->getBitStr();

        if (handlingBS & CONFIG_HANDLING)  
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //else - handling has output an error, just return the default;
    return false; 
}

/*Vibhor 130104: Start of Code*/

bool hCVar::IsWriteOnly(void)
{
    ulong bst = 0;

    if ( pHandling == NULL ) 
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Variable has no Handling attribute.\n");
        return false; // default R/W
    }

    hCbitString* pbs = (hCbitString*) (pHandling->procure());
    if (pbs != NULL)
    {
	ulong handlingBS = pbs->getBitStr();

        if ( (handlingBS & WRITE_HANDLING) && !(handlingBS & READ_HANDLING)) 
        {//       if writable              &          not readable 
            return true;
        }
        else
        {
            return false;
        }
    }
    //else - handling has output an error, just return false
    return false; 


}/*End IsWriteOnly*/

/*Vibhor 130104: End of Code*/

/***** commented out code removed 25may05 stevev - see earlier versions **/


/* virtual put the new value into the dispValue & write through */
RETURNCODE hCVar::Write(CValueVarient& newDataValue)
{   LOGIT(CERR_LOG,L"WARN:hCVar virtual call to Write varient.\n"); return FAILURE;}


/*virtual*///writes the current value as a string
 RETURNCODE hCVar::Write(wstring value)
 {  LOGIT(CERR_LOG,L"WARN:hCVar virtual call to Write string.\n"); return SUCCESS;}

/*virtual*///extracts the value from an array of bytes
RETURNCODE hCVar::ExtractIt(BYTE *byteCount, BYTE **data, BYTE* dest, BYTE size)
{   
    memcpy(dest, *data, size);
    *byteCount -= size;
    *data += size;
    return SUCCESS;
}

/*virtual*///adds the value to a byte array - the generic parent class version
RETURNCODE hCVar::AddIt(BYTE *byteCount, BYTE **data, BYTE* source, BYTE size)
{   memcpy(*data, source, size);
    *byteCount += size;
    *data += size;
    return SUCCESS;
}

RETURNCODE  hCVar::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{   LOGIT(CERR_LOG,L"WARN:hCVar virtual call to Add.\n"); return FAILURE;
}
RETURNCODE  hCVar::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
                    methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                    varPtrList_t &hasChangedList, extract_t extFlags)
{   LOGIT(CERR_LOG,L"WARN:hCVar virtual call to Extract.\n"); return FAILURE;
}


/* Reads the Var from the RT database - marks it stale if it needs updating-*/
/* ** ALWAYS returns a value !!!!!!!! check the quality !!!!!!!!!!!!!!!!!!!  */
DATA_QUALITY_T hCVar::Read(CValueVarient& returnValue)
{
    DATA_QUALITY_T daRet;

    /* VMKP delted getRealValue() on 190104 */
    returnValue = getDispValue(); //getRealValue();
    /* VMKP delted getRealValue() on 190104 */

    daRet       = getDataQuality();

    switch (getDataState()) 
    {
    case IDS_NEEDS_WRITE:   // write stale - service task will enqueue the command
                            // DispValue is set in this app and not verified from the device
                
    case IDS_PENDING:       // a command is enqueued and waiting for an update/complete
                            // the data is OK but may be stale

    case IDS_CACHED:        // have the data. if it is dynamic we need to send a command.

    case IDS_STALE:         // the service task will enqueue a command  
                            // the data is OK but stale
        break;
        

    case IDS_INVALID:          // the variable had a bad read.  try again

    case IDS_UNINITIALIZED: // this means this command has yet to be initialized 
                            // the data is not available        (need to read the first time)
        MakeStale();        // marks it for the read service to do it
        break;

    }// endswitch

// was  if     (daRet == DA_HAVE_DATA || DA_STALE_OK)   
    if   (  (daRet == DA_HAVE_DATA ) || (daRet == DA_STALE_OK)  )
    {   
        returnValue.vIsValid = true;    
    }
    else// (daRet == DA_NOT_INITIALIZED || DA_NOT_VALID || DA_STALEUNK
    {
        returnValue.vIsValid = false;
    }

    return daRet;
};


/* Writes this Var Async */
RETURNCODE hCVar::Write(void) 
{
    DATA_QUALITY_T  daRet;
    RETURNCODE rc = SUCCESS;

    daRet = getDataQuality();       // may be anything

    switch (getDataState()) 
    {
    case IDS_NEEDS_WRITE:       // already write stale - service task will enqueue the command
                                // the data has been set here and not verified from the device
        break;

    case IDS_PENDING:           // this means a command is enqueued and we are 
                                //      waiting for an update / complete
                                // mark it write, we should get an answer & then send the write
    case IDS_INVALID:          // the variable had a bad read/write.  try again
    case IDS_CACHED:            // have the data. - mark it to write
    case IDS_UNINITIALIZED:     // this means this variable has never been read from the device
                                // the data is not available        

    case IDS_STALE:             // been marked stale - write takes priority - mark it write 

        markItemState(IDS_NEEDS_WRITE ); // marks the item for service routine to service
        break;
        

    }// endswitch

    if      (daRet == DA_HAVE_DATA || daRet == DA_STALE_OK )    
    {
        rc = SUCCESS;   
    }
    else  /* daRet == DA_NOT_VALID, DA_STALEUNK */
    {
        rc = DATA_QUALITY_NOT_GOOD;
    }

    return rc;
};

/*  stevev 17apr07 - notes on transition restriction:
 *  a variable can be changed while read command is pending, that is handled by writeStatus
 *  But, if the commit button is pressed while the read command is pending, the state goes
 *  to needs-write.  When the read command returns, the state goes to cached and the new
 *  value is never written.  The restrictions added below to deal with this situation
 */
// stevev 26aug14 - from and - needsNotify is defaults true - sometimes we don't need to notify
INSTANCE_DATA_STATE_T hCVar::markItemState(INSTANCE_DATA_STATE_T newSt, bool needsNotify )
{
    INSTANCE_DATA_STATE_T r = dataState;
	DATA_QUALITY_T        q = dataQuality;
    // stevev 17apr07 :: now on a case-by-case basis::> dataState = newSt;
        
    switch (newSt) 
    {
    case IDS_STALE: 
        if (r == IDS_NEEDS_WRITE)//stevev 17apr07-transition restriction
            break;  // - skip any state change
    case IDS_NEEDS_WRITE:
    case IDS_PENDING:
        if (dataQuality == DA_STALE_OK || dataQuality == DA_HAVE_DATA )
        {   dataQuality  = DA_STALE_OK; }
        else // notvalid or staleunk
        {   dataQuality  = DA_STALEUNK; }
        /* for the removal of encoded state in the values (ie -1) 05jul05*/
        if ( r == IDS_UNINITIALIZED || r == IDS_INVALID )
        {
            previousDataState = r;// this is cleared in extract
        }// else leave it alone
        dataState = newSt;// stevev 17apr07
        break;      
    case IDS_CACHED:
        if (r != newSt && r != IDS_NEEDS_WRITE)
        {
			if ( needsNotify )
            devPtr()->notifyVarUpdate(getID(), STAT_changed, newSt);    // J.U. 17.02.11
        }
        if (r == IDS_NEEDS_WRITE)//stevev 17apr07-transition restriction
            break; // - skip any state change
        dataQuality = DA_HAVE_DATA;
        queryCount  = 0;
        dataState = newSt;// stevev 17apr07
        break;      
    case IDS_INVALID:
        if (dataState != newSt)
        {
			if ( needsNotify )
            devPtr()->notifyVarUpdate(getID(), STAT_changed, newSt);    // J.U. 22.03.11
		}// no break, fall thru
    case IDS_UNINITIALIZED: 
    default:        
        dataQuality = DA_NOT_VALID;
        dataState = newSt;// stevev 17apr07
        break;
    }// endswitch
#ifdef x_DEBUG
    if (getID() == 0x0fd0)
    LOGIT(CLOG_LOG,
        "Marked ItemState of %s (0x%04x) from %s to new %s & %s to %s\n",getName().c_str(),getID(),
										instanceDataStStrings[r],instanceDataStStrings[newSt], 
										dataQualityStrings[q], dataQualityStrings[dataQuality]);
#endif
    return r;
}


/* stevev 30nov06 - now a pass thru to unit hCunitRelList ----------
string hCVar::getUnitStr(void)
{
    string      retStr;
    RETURNCODE  rc = getUnitString(retStr);
    return retStr;
}
------------------------------ end stevev 30nov06-------------------*/
/* stevev 4/1/04 - modified to get rid of duplicate code (maintainence issue) 
  *** removed commented out code 26may05 - see earlier versions for contents
end comment out duplicate code stevev 4/1/04 */
/* stevev 1nov04 */
/* stevev 30nov06 - now a pass thru to unit hCunitRelList ----------------------------
ITEM_ID hCVar::getUnitVarId(void) // returns InvalidItemID if there is none
{
    ITEM_ID r = InvalidItemID;
    RETURNCODE rc = SUCCESS;
    hCitemBase* pUnitRel = NULL;
    hCVar*      pUnitVar = NULL;

    if ( unitRelation != InvalidItemID )// we could have a relation AND a constant unit
    {       
        if ( (rc = devPtr()->getItemBySymNumber(unitRelation, &pUnitRel)) == SUCCESS &&
             pUnitRel != NULL  && pUnitRel->getIType() == iT_Unit)
        {// we have a relation
            if ( (rc = ((hCunitRel*)pUnitRel)->getIndependVarPtr(pUnitVar)) == SUCCESS &&
                 ( pUnitVar != NULL )  )
            {
                r = pUnitVar->getID();
            }
        }// else return invalid
    }// else return invalid
    return r;
}
------------------------------ end stevev 30nov06-------------------*/
/* stevev end 1nov04*/
/*Vibhor 120104: Start of Code*/
/*    added function to get the return code and the string..getUnitStr() only gets the string*/
/* stevev 30Nov06 - modified to get either UnitRel or Const unit string (error string @ both)*/
RETURNCODE hCVar::getUnitString(wstring & str)
{
    RETURNCODE  rc = SUCCESS;
    wstring      retStr;
#ifdef _DEBUG
unsigned entryCnt = objCnt;
#endif

    if (pUnit != NULL && unitRelations.size() > 0 )
    {// const unit & unit rel - an error
        str = L"DD ERROR";
        LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,
                    "ERROR: Const Unit AND Unit Relation Assigned.(%s)\n",getName().c_str());
        rc = FAILURE;
    }
    else
    if (pUnit != NULL)
    {
        str = (wstring)pUnit->getConstUnitStr();
    }
    else
    if ( unitRelations.size() > 0 )
    {
        str = unitRelations.getUnit_Str();
    }
    else // neither
    {
        str.erase();
    }
    return rc;
}/*End getUnitString()*/

/*Vibhor 120104: End of Code*/





//////////// Actions //////////////////
/* common code to fetch the info */
RETURNCODE hCVar::getMethodList(varAttrType_t vat,vector<hCmethodCall>& methList)
{
    RETURNCODE    rc = SUCCESS;
    
    ddbItemList_t        localItmList;
    itemType_t           iTy;
    hCmethodCall         wrkMthCall;
    CValueVarient        retVal;
    retVal = getID();    // we use it first for the self parameter
    methodCallSource_t   srcTyp = msrc_ACTION; // sjv 06mar07
    
    hCattrBase*  pAB = getaAttr(vat);
    if ( pAB != NULL )
    {// we have an attribute
        switch(vat)
        {
        case varAttrPreReadAct:         
        {
            hCattrVarPreRdAct*   pActions = (hCattrVarPreRdAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
            srcTyp = msrc_CMD_ACT;
        }
        break;
        case varAttrPostReadAct:        // count = 0
        {
            hCattrVarPostRdAct*   pActions = (hCattrVarPostRdAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
            srcTyp = msrc_CMD_ACT;
        }
        break;
        case varAttrPreWriteAct:    //10// count = 0
        {
            hCattrVarPreWrAct*   pActions = (hCattrVarPreWrAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
            srcTyp = msrc_CMD_ACT;
        }
        break;
        case varAttrPostWriteAct:       // count = 0
        {
            hCattrVarPostWrAct*   pActions = (hCattrVarPostWrAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
            srcTyp = msrc_CMD_ACT;
        }
        break;
        case varAttrPreEditAct:         // count = 0
        {
            hCattrVarPreEditAct*   pActions = (hCattrVarPreEditAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
        }
        break;
        case varAttrPostEditAct:        // count = 0
        {
            hCattrVarPostEditAct*   pActions = (hCattrVarPostEditAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
        }
        break;
        case varAttrRefreshAct:     // count = 0
        {
            hCattrVarRefreshAct*   pActions = (hCattrVarRefreshAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
        }
        break;
        case varAttrPostRequestAct: 
        {
            hCattrVarPostRequestAct*   pActions = (hCattrVarPostRequestAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
        }
        break;
        case varAttrPostUserAct:
        {
            hCattrVarPostUserAct*   pActions = (hCattrVarPostUserAct*) pAB;
            rc = pActions->getItemPtrs(localItmList);
        }
        break;

        default:
        {
            LOGIT(CERR_LOG,"getMethodList: has UNKNOWN %s attribute.\n",varAttrStrings[vat]);
            rc = FAILURE;
        }
        break;
        }// end switch       

		vector<hCmethodCall>::iterator mcIT;
        if (rc == SUCCESS && localItmList.size() > 0)
        {//for each in the list
            for (ddbItemList_t::iterator iT = localItmList.begin();iT<localItmList.end();iT++)
            {//iT isa ptr 2a ptr 2a hCitemBase
                iTy = (*iT)->getIType();
                if ( iTy != iT_Method)
                {
                    LOGIF(LOGP_NOT_TOK)
                            (CERR_LOG,"ERROR: non-Method type in a Pre/Post action list.\n");
                }
                else
                {// we be good
                //  fill a method call with varID, msrc_ACTION,methodID  

					/* stevev 02dec14 - post action calls should never be executred twice.
						if the methodID + varID is already in the list, then do not put
						it in again.  ie a post write method on the request variable
						that matches a reply variable should not execute that method twice.**/
					bool found = false;
					for (mcIT = methList.begin(); mcIT != methList.end(); ++mcIT)
					{
						hCmethodCall* pmc = &(*mcIT);
						if (pmc->methodID == (*iT)->getID() && pmc->paramList[0] == retVal)
						{
							found = true;
							break; //out of for loop
						}
					}

					if ( ! found )
					{
						wrkMthCall.methodID  = (*iT)->getID();
						wrkMthCall.source    = srcTyp;  // sjv 06mar07 - usually:  msrc_ACTION;
						wrkMthCall.paramList.push_back(retVal);
						methList.push_back(wrkMthCall);
					}
					else	// else, just skip it
					{
						DEBUGLOG(CLOG_LOG,"Duplicate in method list for 0x%04x\n",(unsigned)retVal);
					}
                    wrkMthCall.clear();
                }
            }// next method
        }// else return the error

    }// else - no actions, just return SUCCESS
    return rc;
}

/* doPreReadActs - expected to be executed a variable at a time */
RETURNCODE hCVar::doPreRdActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrPreReadAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        // NORMAL:: not error
        //rc = FAILURE;
    }

    return rc;
}

RETURNCODE hCVar::getPreWrActs(methodCallList_t& pActList)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;

    rc = getMethodList(varAttrPreWriteAct,pActList);

    return rc;
}

RETURNCODE hCVar::doPreWrActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrPreWriteAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 
        && IsValid()  )// added 02nov05 -  Could result in re-entry of send()
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        //NORMAL: not error
        //rc = FAILURE;
    }

    return rc;
}

RETURNCODE hCVar::doPreEditActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient    retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrPreEditAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        //NORMAL::not error
        //rc = FAILURE;
    }

    return rc;
}
RETURNCODE hCVar::doPstEditActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient    retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrPostEditAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        /* normal occurance - do not log
        cerr < <"ERROR: method list resolved to empty in PostEdit Actions.("< <rc< <")"< <endl;
        rc = FAILURE;
        **/
    }

    return rc;
}

// L&T Modifications : VariableSupport - start
RETURNCODE hCVar::doPstEditActsForVS(int ValueChanged)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient    retVal;
	methodCallList_t methodList;

	hCddbDevice *pCurDev = (hCddbDevice*)devPtr();
	rc = getMethodList(varAttrPostEditAct,methodList);

	if (rc == SUCCESS && methodList.size() > 0 )
	{
		rc = ((hCddbDevice*)devPtr())->executeMethodForVS(methodList,retVal, pCurDev);
	}
	else
	{
		/* normal occurance - do not log
		cerr < <"ERROR: method list resolved to empty in PostEdit Actions.("< <rc< <")"< <endl;
		rc = FAILURE;
		**/
	}

	return rc;
}
// L&T Modifications : VariableSupport - end

RETURNCODE hCVar::doRefreshActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient    retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrRefreshAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        /* normal occurance - do not log
        rc = FAILURE;
        **/
    }
    return rc;
}

bool hCVar::hasRefreshActions(void)
{ 
    RETURNCODE rc = FAILURE;
	methodCallList_t methodList;

    rc = getMethodList(varAttrRefreshAct,methodList);

    if (rc != SUCCESS || methodList.size() <= 0 )
    {
        return false;
    }
    else
    {
		return true;
    }
}

RETURNCODE hCVar::getPostRequestActs(methodCallList_t& pActList)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;

    rc = getMethodList(varAttrPostRequestAct,pActList);

    return rc;
}

RETURNCODE hCVar::doPstRequestActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient    retVal;
    methodCallList_t methodList;

    rc = getMethodList(varAttrPostRequestAct,methodList);

    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        /* normal occurance - do not log **/
    }

    return rc;
}
RETURNCODE hCVar::doPstUserActs(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient    retVal;
    methodCallList_t methodList;

    
    if ( devPtr()->getPolicy().isReplyOnly )
    {
        rc = getMethodList(varAttrPostUserAct,methodList);

        if (rc == SUCCESS && methodList.size() > 0 )
        {
            rc = devPtr()->executeMethod(methodList,retVal);
        }
        else
        {
            /* normal occurance - do not log    **/
        }
    }// else return success;

    return rc;
}



RETURNCODE hCVar::getPreRdActs(methodCallList_t& pActList)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;

    rc = getMethodList(varAttrPreReadAct,pActList);

    return rc;
}

RETURNCODE hCVar::getPstRdActs(methodCallList_t& methodList)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;

    rc = getMethodList(varAttrPostReadAct,methodList);
/* stevev 2/18/04 - the get methods should not be executing (don't know where this came from)
    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        //NORMAL::not error
        //rc = FAILURE;
    }
end stevev 2/18/04*/
    return rc;
}
RETURNCODE hCVar::getPstWrActs(methodCallList_t& methodList)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient        retVal;

    rc = getMethodList(varAttrPostWriteAct,methodList);
/* stevev 2/18/04 - the get methods should not be executing (don't know where this came from)
    if (rc == SUCCESS && methodList.size() > 0 )
    {
        rc = devPtr()->executeMethod(methodList,retVal);
    }
    else
    {
        //NORMAL::not error
        //rc = FAILURE;
    }
end stevev 2/18/04*/
    return rc;
}

/*Vibhor 040104: Start of Code*/
        
         /*Adding this API to get action list as a vector of (method)
         item Ids*/

RETURNCODE hCVar:: getActionList(varAttrType_t actionType,vector<itemID_t>& actionList)
{

    RETURNCODE rc = SUCCESS;
    vector<hCmethodCall> methList;

    vector<hCmethodCall> :: iterator it;

    rc = getMethodList(actionType,methList);

    if(rc == SUCCESS && methList.size() > 0)
    {
        for(it = methList.begin(); it != methList.end(); it++)
        {
            itemID_t actionID = 0;

            actionID = (*it).methodID;

            actionList.push_back(actionID);
        }

    }

    return rc;

}

/*Vibhor 040104: End of Code*/


hCcommandDescriptor  hCVar::getRdCmd(int cNum) 
{
    hCcommandDescriptor retDesc;
    retDesc.cmdNumber = 0xffff; // the error return
    FOR_iT(cmdDescList_t, rdCmdList)
    {
        if (iT->cmdNumber == cNum)
        {
            retDesc = *iT;
            break; // out of for
        }
    }
    return retDesc;
}

hCcommandDescriptor  hCVar::getWrCmd(int cNum)
{
    hCcommandDescriptor retDesc;
    retDesc.cmdNumber = 0xffff; // the error return
    FOR_iT(cmdDescList_t, wrCmdList)
    {
        if (iT->cmdNumber == cNum)
        {
            retDesc = *iT;
            break; // out of for
        }
    }
    return retDesc;
}

// returns the first command descriptor that uses the cmd,transNum & index Var specified
hCcommandDescriptor  hCVar::getRdCmd(int cNum,int transactionNum,itemID_t idxSymID) 
{
    hCcommandDescriptor retDesc;
    retDesc.cmdNumber = 0xffff; // the error return - reserved HART command #
    retDesc.transNumb = 0xffff; // added 03jan13 to make a unique not-found signature
    FOR_iT(cmdDescList_t, rdCmdList)
    {// iT isaPtr2a hCcommandDescriptor
        if (iT->cmdNumber == cNum && iT->transNumb == transactionNum)
        {
            for (idxUseIT itCD = iT->idxList.begin(); itCD < iT->idxList.end(); itCD++)
            {// itCD isptr2a hIndexUse_t
                if ( itCD->indexSymID == idxSymID)
                {// found one
                    retDesc = *iT;
                    //break; // out of for
                    return retDesc;
                }
            }
        }// else continue to next descriptor
    }
    return retDesc;
}
hCcommandDescriptor  hCVar::getWrCmd(int cNum,int transactionNum,itemID_t idxSymID)
{
    hCcommandDescriptor retDesc;
    retDesc.cmdNumber = 0xffff; // the error return
    FOR_iT(cmdDescList_t, wrCmdList)
    {
        if (iT->cmdNumber == cNum && iT->transNumb == transactionNum)
        {
            for (idxUseIT itCD = iT->idxList.begin(); itCD < iT->idxList.end(); itCD++)
            {// itCD isptr2a hIndexUse_t
                if ( itCD->indexSymID == idxSymID)
                {// found one
                    retDesc = *iT;
                    //break; // out of for
                    return retDesc;
                }
            }
        }
    }
    return retDesc;
}

/* stevev 8jul05 - common code that all can use                          */
/*               - old code removed 9oct09                               */
/*               - NOTE: default Value from DD (dfltVarient) is UNSCALED */
RETURNCODE hCVar::setDefaultValue(CValueVarient dfltVarient)
{// dfltVarient is default default-value
    RETURNCODE rc = FAILURE;
    CValueVarient  lo,hi;
    hSdispatchPolicy_t policy  = devPtr()->getPolicy();
    hCattrVarMinVal*   pMVattr = (hCattrVarMinVal*) getaAttr(varAttrMinValue);
    int flg = 0; // zero - no-value; 1 default-value; 2 min-value;
    bool gotLimits = false;
    /////////   minvalue ain't a default value attribute (but its often used that way)
    CValueVarient  vv;
    // *********** assume that the Xmtr .ini value will overwrite DD's Default value  *********
    if ( NULL != pDefaultVal)
    {
        rc = pDefaultVal->getDfltVal(vv);
        if ( rc == SUCCESS && vv.vIsValid )
        {
            flg = 1;
        }// else leave it zero
    }
    if ( 1 != flg )// no default, see if there is a min
    {
        if ( (NULL != pMVattr) && ((vv = pMVattr->getMin()).vIsValid ) )
        {
            rc = SUCCESS;
            flg = 2;
        }
    }

/* new 09oct09 */
    switch (varType)
    {
    case vT_Integer:        // 2
    case vT_Unsigned:       // 3
        {
            vv = limitAndExtend((UINT64)vv);
            // 'cast' vv to this type
            if ( varType == vT_Integer && vv.vIsUnsigned )
            {
                vv.vIsUnsigned = false;
            }
            if ( varType == vT_Unsigned && !vv.vIsUnsigned )
            {
                vv.vIsUnsigned = true;
            }

            ((hCinteger*)this)->lowest(lo);     // these should be the low/hi natural values
            ((hCinteger*)this)->highest(hi);
            gotLimits = true;

        }
        // do not break, fall through to finish processing
    case vT_FloatgPt:       // 4
        {
            if ( ! gotLimits )
            {
                ((hCFloat*)this)->lowest(lo);   // these should be the low/hi natural values
                ((hCFloat*)this)->highest(hi);
                gotLimits = true;

                if ( vv.vType != CValueVarient::isFloatConst || vv.vIsDouble )
                {// 'cast' the const
                    float x = (float)vv;
                    vv = x;
                    vv.vIsDouble = false;
                }
            }
        }
        // do not break, fall through to finish processing
    case vT_Double:         // 5
        {// be sure dfltVarient is inside min/max, if not, coerce to nearest 

            if ( ! gotLimits )
            {
                ((hCDouble*)this)->lowest(lo);  // these should be the low/hi natural values
                ((hCDouble*)this)->highest(hi);
                gotLimits = true;

                if ( vv.vType != CValueVarient::isFloatConst || ! vv.vIsDouble )
                {// 'cast' the const
                    double x = (double)vv;
                    vv = x;
                    vv.vIsDouble = true;
                }
            }
            // if there is a DEFAULT_VALUE attribute
            if ( 0 != flg )// default or min value treated the same
            {// if vv is outside the natural range, it will be coerced to the nearest natural
                // these MUST be the same type &|size as this variable
                if ( (vv >= hi && !(vv == hi)) )
                {
                    vv = hi; // coerced to highest
                }
                else
                if (vv <= lo && !(vv == lo)) 
                {
                    vv = lo; // coerced to lowest
                }
            }
            else
            {// we're using dflt-dflt-value, it has to be in-range 
             //                                     (it's unscaled so we can't use isInRange())
                vv = dfltVarient;
                hCRangeList localList;
                ((hCNumeric*)this)->resolveRangeList(localList,No_Type,true);  
                            // ...get min max, resolve to list, populate
                            // stevev 23sep10 - added isAtest so it wouldn't try to read a 
                            //                  value this early in instantiation
                if ( localList.size() > 0 )   
                {// should always be >0    ..it is populated with default values
                    if ( ! localList.isInRange(vv))
                    {// we have to coerce it to closest
                        localList.coerceVal(vv);
                    }// else, in range...just use it
                }
                else
                if ( (vv >= hi && !(vv == hi))  )
                {
                    vv = hi;
                }
                else
                if ( (vv <= lo) && !(vv == lo)  )
                {
                    vv = lo;
                }
                //else
                //  use it as-is
            }
        }
        break;
    case vT_Enumerated:     // 6
        {
            if ( (flg == 1) && (! isInRange(vv)) )// we have an out-of-range dfltVal Attribute
            {//coerce to nearest legal value
                vv = limitAndExtend((UINT64)vv); // make it fill natural size
                hCenumList returnList(devHndl());
                if ( ((hCEnum*)this)->procureList(returnList) == SUCCESS && 
                     returnList.size() > 0 )
                {
                    vv = returnList.coerceValue( (UINT32)vv );   
                }
                // else  leave it, this can't really be an enum
                returnList.destroy();
            }
            else 
            if (flg != 1)//  no attribute  - use dflt dflt
            {
                vv = dfltVarient;
            }
            // else - has attribute and it is in range --- vv already set
        }
        break;
    case vT_BitEnumerated:  // 7
        {
            if (flg != 1) // no default attribute
            {
                vv = dfltVarient;
            }
            else // - use the default attribute as-is.
            {
                vv = limitAndExtend((UINT64)vv); // make it fill natural size
            }
        }
        break;
    case vT_Index:          // 8
        {
            if ( flg != 1 ) // no default attribute
            {
                vv = dfltVarient;
            }
            // else use default vv directly here
            // indexes must be coerced to nearest legal value (NOT natural value)
            vv = limitAndExtend((UINT64)vv); // this forces a fit for indexes
        }
        break;

    case vT_Ascii:          // 9
    case vT_PackedAscii:    //10 - HART only
    case vT_Password:       //11
        {// if there is a DEFAULT_VALUE attribute

            if ( (flg == 1) && (! isInRange(vv)) )
            {
                string z; z = getName();
                LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s Var 0x%04x (%s) has a Default Value"
                    " Attribute that is not a valid value.\n",VariableTypeString(),getID(),
                    z.c_str());
                vv = dfltVarient;
            }
            else // no default attr or it is in range
            if ( flg != 1 ) // no default attr
            {
                vv = dfltVarient;
            }
            //else - dflt is in range,  use it

            string tmp;
            tmp = (string)vv;
            if ( varType == vT_PackedAscii)
            {// promote lower to upper
                for ( unsigned i = 0; i < tmp.length(); i++ )
                {
                    if ( tmp[i] > 0x60 && tmp[i] < 0x7B )// it's a lower case
                    {
                        tmp[i] = tmp[i] - 0x20;
                    }
                }
            }
            // must fit into length - truncate to fit
            if (tmp.length() > VariableSize())
            {
                vv = tmp.substr(0,VariableSize());
            }
        }
        break;
    case vT_HartDate:       //13 - HART only
        {// if hartdate is outside the accepted numerics, it is discarded and dfltVarient used
    
            if ( flg != 1 || (! ((hChartDate*)this)->ValidateDate((UINT32) vv ))  )
            {   // no default-value attribute OR (there is one but it does not validate
                vv = dfltVarient;// verify it fits
            }
            //else it's a defalut attribute and it valid...so just use it
            // we assume the default-default-value is valid
        }
        break;
    case vT_TimeValue:      //20
        {// if vv is outside the natural range, it will be coerced to the nearest legal
            
            if (flg != 1) // no default attribute
            {
                vv = dfltVarient;
            }
            else // - use the default attribute as-is.
            {
                vv = limitAndExtend((UINT64)vv); // make it fill natural size
            }
        }
        break;

    case vT_unused:
    case vT_undefined:      // 1
    case vT_BitString:      //12
    case vT_Time:           //14 - not HART
    case vT_DateAndTime:    //15 - not HART
    case vT_Duration:       //16 - not HART
    case vT_EUC:            //17 - not HART
    case vT_OctetString:    //18 - not HART
    case vT_VisibleString:  //19 - not HART
    case vT_Boolean:        //21 - not HART
    case vT_MaxType:        //  22
    default:
        {
        }
        break;
    }//endswitch
    // we have a vv default value
    
    setRawDispValue(vv);         // will notify & apply some locals
    if ( flg == 1 ) // we have a default attribute
    {
    //    setDataQuality(DA_HAVE_DATA);// will allow the MakeStale to be STALEOK not STALEUNK
        MakeStale();                 // valid to use but needs to be updated from the device
    }
    ApplyIt();// disp 2 device
    if ( policy.isReplyOnly || IsLocal() )
    {// we are a device
        markItemState(IDS_CACHED);// overwrites stale possibly set earlier
    }
    return SUCCESS;


/***************/
    

//  if ( ( (pDVattr != NULL && pDVattr->getDfltVal(vv) == SUCCESS) || 
//         (pMVattr != NULL && pMVattr->getDfltVal(vv) == SUCCESS)   ) && vv.vIsValid) 

            
    if ( (  (pDefaultVal != NULL && pDefaultVal->getDfltVal(vv) == SUCCESS) || 
            (   (pMVattr) != NULL && (vv = pMVattr->getMin()).vIsValid   )        
          ) && vv.vIsValid) 
    {// we have a default from the attribute
        if ( IsNumeric() && VariableType() != vT_FloatgPt && VariableType() != vT_Double )
        {// for all integers (inc enums)
            vv = limitAndExtend((UINT64)vv);
        }
        else // for all floating point, string, date
        if ( ! isInRange(vv) )
        {
            string z; z = getName();
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s Var 0x%04x (%s) has a Default Value"
                " Attribute that is not a valid value.\n",VariableTypeString(),getID(),
                z.c_str());// use it anyway...
        }
        // else - all is well - use it
        setRawDispValue(vv);// will notify & apply some locals
        setDataQuality(DA_HAVE_DATA);// will allow the MakeStale to be STALEOK not STALEUNK
        MakeStale();// valid to use but needs to be updated from the device
    }
    else
    {
        setRawDispValue(dfltVarient);// will notify & apply some locals
    }
    ApplyIt();// disp 2 device
    if ( policy.isReplyOnly || IsLocal() )
    {// we are a device
        markItemState(IDS_CACHED);// overwrites stale possibly set earlier
    }
    return SUCCESS;
}

/* stevev 16nov05 - dependency extentions */
void  hCVar::fillCondDepends(ddbItemList_t&  iDependOn)
{
    // call the base to get the label
    hCitemBase::fillCondDepends(iDependOn);
    // the only other attribute:
    if ( pHandling != NULL )//VAR_HANDLING_ID  
    {
        pHandling->getDepends(iDependOn);
    }    
// in numeric
    //VAR_DISPLAY_ID 
    //VAR_SCALE_ID    
// in bitenum   
    //BIT_ENUM_LIST

}
/** end stevev 16nov05  **/

/* stevev 18nov05 - overload to get var specific notifications */

// called when an item in iDependOn has changed
int hCVar::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
    int init = msgList.size();
    
    hCitemBase::notification(msgList,isValidity);// do validity and label

    // re-evaluate all the others ( handling is only one in this base class)
    if ( (! isValidity) && pHandling != NULL && 
         pHandling->isConditional()          &&   pHandling->reCalc())
    {// my validity changed
        msgList.insertUnique(getID(), mt_Str, pHandling->getGenericType());// tell the world
    }// else - no change;

    return (msgList.size() - init);// number added
}
/** end stevev 18nov05  **/

// stevev 10feb15 - common code from all over..call when you want to generate a 
//				    notification that your value has changed.
void hCVar::notifyIchanged(void)
{ 
	hCmsgList  noteMsgs;
    noteMsgs.insertUnique(getID(), mt_Val, 0);     
    notify(noteMsgs);
    notifyUpdate(noteMsgs); // send it now
}



// stevev 15sep06 - moved from .h file to try and make it work
//inline
hCattrClass* hCVar::getClassAttr(void)
{ 
    hCattrClass* pC = NULL;
    pC = pClass;
    return pC;

    //return pClass;
};

wstring& hCVar::getClassStr(void)
{
    hCattrClass* cls = NULL;
//CPMHACK : Modified for 32bit ARM compilation
#ifdef CPM_64_BIT_OS
    ulong bS;
#else
    UINT64 bS;
#endif
    
    cls = getClassAttr();
    if (cls != NULL )
    {
        cls->getBstring(bS); 
        returnString =  cls->translateBstring(bS);
    }
    else
    {
        returnString = L" UnClassed ";
    }
    return returnString;
}


// stevev 15sep06 - moved from .h file to make it easier to debug an intermittent mt_string
const wstring& hCVar::getValidDispValueString(void) // returns empty if invalid
{// stevev 20jan15 - add validity to validDispValue test
    if((dataQuality== DA_HAVE_DATA || dataQuality== DA_STALE_OK
        /* if the user just set it, don't blank it out*/
        || writeStatus == 1) && IsValid()  ) 
        return(getDispValueString() ); 
    else 
        return (MT_WSTRING);
};

// stevev - WIDE2NARROW char interface \/ \/
const string& hCVar::getValidDisplayValueString(void)
{
    if(dataQuality== DA_HAVE_DATA || dataQuality== DA_STALE_OK) 
        return(getDisplayValueString() ); 
    else 
        return (MT_STRING);
}


int hCVar::getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
                                       wstring* pEditFmt, wstring* pScanFmt, wstring* pDispFmt)
{// this needs to be over ridden for everybody that can be put on the screen.
 // at this time, numerics are done
    LOGIT(CLOG_LOG,"ERROR: programmer..baseclass getFormatInfo has been called.\n");
    fmtWidth = maxChars = 0;
    rightOdec= -1;// 'not there' not 'zero places' was::> 0;
    _CrtDbgBreak();// will break in debug builds
    return -1;// error return
}
wstring hCVar::getEditFormat(void)
{
    LOGIT(CLOG_LOG,"ERROR: program..baseclass getEditFormat has been called.\n");
    return wstring(L"");
}
int hCVar::getEditFormatInfo(int& maxChars, int& rightOdec, wstring* pScan)
{
    LOGIT(CLOG_LOG,"ERROR: programmer..baseclass getEditFormatInfo has been called.\n");
    maxChars =  0;
    rightOdec= -1;
    if (pScan) pScan->erase();
    return (int)(L"");
}

void hCVar::setWaoRel(itemID_t waoRelationID) 
{   // 16 dec 13 - change from last-one-wins to first-one-wins
    //     was::>  waoRelation  = waoRelationID; 
    if (waoRelation == InvalidItemID)
    {
        DEBUGLOG(CLOG_LOG,"WAO set\t (0x%04x) %s's WAO to\t0x%04x.\n",
                                                      getID(),getName().c_str(),waoRelationID);
        waoRelation  = waoRelationID;
    }
}

/********************************************************














***********************************************************/
/// hCVar
///     BYTE size;
///     INSTANCE_DATA_STATE_T dataState;
///     +++ command lists
/// CNumeric
//      string constant_units,      // if no units then string is empty
//      
//      float min_value,            // if no min value then set to -inf
//          max_value,              // if no min value then set to +inf
//          scaling_factor;         // if no scaling factor then set to 1.00

//*%hCNumeric::hCNumeric(aCvarType* pVarType) 
hCNumeric::hCNumeric(DevInfcHandle_t h, aCitemBase* paItemBase)
          :hCVar(h,paItemBase),pDispFmt(NULL),pEditFmt(NULL),pScale(NULL),
                               pMinVal(NULL), pMaxVal(NULL), pList4Count(NULL)
{
    bool hasTimeScale = false;
    clear();//hCNumeric();// initialize values
    if (paItemBase == NULL)
        return;
    aCattrBase* aB = NULL;

    variableType_t vtt = (variableType_t)VariableType();
    if ( vtt == vT_TimeValue )
    {
        if (paItemBase->getaAttr(varAttrTimeScale) != NULL)
        {
            hasTimeScale = true;
        }
        // else leave it false
    }
    
    
    aB = paItemBase->getaAttr(varAttrDisplay) ; 
    if (aB == NULL)
    {// *********** DEFAULT - type dependent
        RETURNCODE rc = SUCCESS;
        if ( vtt == vT_TimeValue )
        {// time value with no display format, see if it has scale
            if ( hasTimeScale )
            {// when present, defaults to float
                vtt = vT_FloatgPt;
				pDispFmt  = new hCattrVarDisplay(h);
            }
            else //no time-scale so Display Format is not legal- do not generate default
            {               
                pDispFmt = NULL;
            }
        }
        else // it must be a numeric type, that's OK
		{
			pDispFmt  = new hCattrVarDisplay(h);
		}

        if ( pDispFmt != NULL)
        {
            pDispFmt->setItemPtr((hCitemBase*)this);// needed to set default format
            if ( pDispFmt->setDefaultVals(vtt) != SUCCESS )
            {
                pDispFmt->destroy(); 
                delete pDispFmt; 
                pDispFmt = NULL;
            }
        }
        // else // is null - either a memory error or display format is not allowed
    }
	else // Has a display format
	if ( (vtt == vT_TimeValue) && (! hasTimeScale) )
	{
        LOGIT(CERR_LOG,"ERROR:Display attribute requires TIME_SCALE. Discarding.\n");
		pDispFmt = NULL;
	}
    else  // generate a hart class from the abstract class
    { pDispFmt  = new hCattrVarDisplay(h, (aCattrString*)aB );
      pDispFmt->setItemPtr((hCitemBase*)this);
    }
    if (pDispFmt != NULL ) attrLst.push_back(pDispFmt);
        
    aB  = paItemBase->getaAttr(varAttrEdit) ; 
    vtt = (variableType_t)VariableType();// in case it was converted to float
    if (aB == NULL)
    {// *********** DEFAULT - type dependent
        RETURNCODE rc = SUCCESS;
        if (  vtt == vT_TimeValue )
        {// time value with no edit format, see if it has scale
            if ( hasTimeScale )
            {//... when present, defaults to float
                vtt = vT_FloatgPt;
				pEditFmt  = new hCattrVarEdit(h);
            }
            else //no time-scale so Edit Format is not legal- do not generate a default
            {               
                pEditFmt = NULL;
            }
        }
        else // it must be a numeric type, that's OK
		{
			pEditFmt  = new hCattrVarEdit(h);
		}

        if ( pEditFmt != NULL)
        {
            pEditFmt->setItemPtr((hCitemBase*)this);// needed to set default format
            if ( pEditFmt->setDefaultVals(vtt) != SUCCESS )
            {
                pEditFmt->destroy();
                delete pEditFmt; 
                pEditFmt = NULL;
            }
            // else we be done
        }
        // else  // is null  - either a memory error or edit format is not allowed
    }
	else // Has a display format
	if ( (vtt == vT_TimeValue) && (! hasTimeScale) )
	{
        LOGIT(CERR_LOG,"ERROR:Edit attribute requires TIME_SCALE. Discarding.\n");
		pEditFmt = NULL;
	}
    else  // generate a hart class from the abstract class
    { pEditFmt  = new hCattrVarEdit(h, (aCattrString*)aB );
      pEditFmt->setItemPtr((hCitemBase*)this);
    }
    if (pEditFmt != NULL ) attrLst.push_back(pEditFmt);
        
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    aB = paItemBase->getaAttr(varAttrScaling) ; // all helps must use the same structure
    if (aB == NULL || policy.isReplyOnly) // device simulator uses default 1
    {// *********** DEFAULT - one
        RETURNCODE rc = SUCCESS;
        pScale  = new hCattrScale(h);
        if ( pScale != NULL)
        {
            if ( pScale->setDefaultVals((variableType_t)VariableType()) != SUCCESS )
            {
                pScale->destroy(); delete pScale; pScale = NULL;
            }// else we be done
            else
            {
                pScale->setItemPtr((hCitemBase*)this);
            }
            if (policy.isReplyOnly)
            {
                markItemState(IDS_CACHED);
            }
        }
        else
        {
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Scaling attribute could not be 'newed';\n");
        }
    }
    else  // generate a hart class from the abstract class
    { 
        pScale  = new hCattrScale(h, (aCattrCondExpr*)aB );
        pScale->setItemPtr((hCitemBase*)this); 
    }
    if (pScale != NULL ) attrLst.push_back(pScale);

    /* stevev 20sep06 - we need to do this in Numeric now */    
    aB = paItemBase->getaAttr(varAttrMinValue) ; 
    if (aB == NULL)
    {// *********** DEFAULT - type dependent
        RETURNCODE rc = SUCCESS;
        pMinVal  = new hCattrVarMinVal(h);
        if ( pMinVal != NULL)
        {
            if ( pMinVal->setDefaultVals((variableType_t)VariableType()) != SUCCESS )
            {
                pMinVal->destroy(); delete pMinVal; pMinVal = NULL;
            }// else we be done
            else
            {
                pMinVal->setItemPtr((hCitemBase*)this);
            }
        }
        else
        {
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Min_Value attribute could not be 'newed';\n");
        }
    }
    else  // generate a hart class from the abstract class
    { pMinVal  = new hCattrVarMinVal(h, (aCattrVarMinMaxList*)aB, this );
      pMinVal->setItemPtr((hCitemBase*)this);
    }
    if (pMinVal != NULL ) attrLst.push_back(pMinVal);
        
    aB = paItemBase->getaAttr(varAttrMaxValue) ; 
    if (aB == NULL)
    {// *********** DEFAULT - type dependent
        RETURNCODE rc = SUCCESS;
        pMaxVal  = new hCattrVarMaxVal(h);
        if ( pMaxVal != NULL)
        {
            if ( pMaxVal->setDefaultVals((variableType_t)VariableType()) != SUCCESS )
            {
                pMaxVal->destroy(); delete pMaxVal; pMaxVal = NULL;
            }// else we be done
            else
            {
                pMaxVal->setItemPtr((hCitemBase*)this);
            }
        }
        else
        {
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Max Value attribute could not be 'newed';\n");
        }
    }
    else  // generate a hart class from the abstract class
    { pMaxVal  = new hCattrVarMaxVal(h, (aCattrVarMinMaxList*)aB, this );
      pMaxVal->setItemPtr((hCitemBase*)this);
    }
    if (pMaxVal != NULL ) attrLst.push_back(pMaxVal);
    /* end 20sep06 */

};
hCNumeric::hCNumeric(DevInfcHandle_t h, variableType_t vt, int vSize)
     :hCVar(h,vt,vSize),pDispFmt(NULL),pEditFmt(NULL),pScale(NULL),pMinVal(NULL),pMaxVal(NULL),
                        pList4Count(NULL)
{
 clear();
}

/* from-typedef constructor */
hCNumeric::hCNumeric(hCNumeric* pSrc, hCNumeric* pVal,  itemIdentity_t newID) : 
           hCVar((hCVar*)pSrc,newID)
{
    
/**-- stevev 20jul06 - share attributes 
    if ( pSrc->pDispFmt != NULL )
    {   pDispFmt = (hCattrVarDisplay*)pSrc->pDispFmt->new_Copy();
    }
    if ( pSrc->pEditFmt != NULL )
    {   pEditFmt = (hCattrVarEdit*)pSrc->pEditFmt->new_Copy();
    }
    if ( pSrc->pScale != NULL )
    {   pScale = (hCattrScale*)pSrc->pScale->new_Copy();
    }
**/
    pDispFmt = pSrc->pDispFmt;
    pEditFmt = pSrc->pEditFmt;
    pScale   = pSrc->pScale;
    pMinVal  = pSrc->pMinVal;
    pMaxVal  = pSrc->pMaxVal;
    // base class has set sharedAttr

    constant_units = pSrc->constant_units;  
    /* stevev 15sep06 - try this to stop shutdown crash*/
    
    psuedoMinList = pSrc->psuedoMinList;
    psuedoMaxList = pSrc->psuedoMaxList;

    pList4Count   = pSrc->pList4Count;
    
}
// moved from .h file & modified to handle attribute pointers 10apr07
/* virtual*/
hCNumeric::~hCNumeric()
{
    constant_units.erase();
    // stevev 08sep06 - psuedo attr variables
    // moved to Var 15jul09  DESTROY(pPsuedoDflt);
    vector<hCNumeric*>::iterator iT; // PAW 03/03/09
    for (iT = psuedoMinList.begin();  iT != psuedoMinList.end();++iT)  
    {   
        devPtr()->unRegister(*iT);   
        DESTROY(*iT); /* sets to NULL*/ 
    }
    psuedoMinList.clear();
    for ( iT = psuedoMaxList.begin(); iT != psuedoMaxList.end();++iT)  
    {   
        devPtr()->unRegister(*iT);   
        DESTROY(*iT); /* sets to NULL*/ 
    }
    psuedoMaxList.clear();
    // stevev 20jul06 - share attributes //
    if ( !  sharedAttr )
    {       
        hCattrBase*  pAB;
        // null attribute pointer allows delete from list
        if ( (pAB = getaAttr(varAttrDisplay)) == pDispFmt)pDispFmt = NULL;
        if ( (pAB = getaAttr(varAttrEdit))    == pEditFmt)pEditFmt = NULL;
        if ( (pAB = getaAttr(varAttrScaling)) == pScale)  pScale   = NULL;
        if ( (pAB = getaAttr(varAttrMinValue))== pMinVal) pMinVal  = NULL;
        if ( (pAB = getaAttr(varAttrMaxValue))== pMaxVal) pMaxVal  = NULL;
        
        DESTROY(pDispFmt);
        DESTROY(pEditFmt);
        DESTROY(pScale); 
        DESTROY(pMinVal); 
        DESTROY(pMaxVal);
    }            
}

/*virtual*/
RETURNCODE hCNumeric::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{ LOGIT(CERR_LOG,L"WARN: hCNumeric virtual call to Add.\n"); return FAILURE;};

/*virtual*/
//RETURNCODE hCNumeric::Extract(BYTE *byteCount, BYTE **data, ulong mask, int& len)
//{ return FAILURE; };

/* lowest and highest use the default numeric values */
bool hCNumeric::isInRange(CValueVarient val2Check)// pretty generic
{ 
    hCRangeList localList;
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    IntType_t thisType = No_Type;
    if (IsSigned() ) thisType  = Is_Signed;
    if (IsUnsigned()) thisType = Un_Signed;
    resolveRangeList(localList,thisType);  // ...get min max, resolve to list, populate
    if ( localList.size() > 0 )   // should always be..populated with default values
    {
        return (localList.isInRange(val2Check));
    }
    else
    {
        return true;// in lieu of a min max value, it must be in range
    }
};// true @ val2Check inside min-max set 


bool hCNumeric::VerifyIt(wstring& s) // J.U. 17.03.11< <caution< < <See note @ bottom ddbVar.h
{
    bool bRes = true;
    CValueVarient var = getValueFromStr(s);
    if (!var.vIsValid)
    {
        bRes = false;
        
    }
    if (bRes == true && ! isInRange(var))
    {
        bRes = false;
    }
    return bRes;

}
/* VMKP added on 020104 */
RETURNCODE hCNumeric::getMinMaxList(hCRangeList& retList)
{
    RETURNCODE rc = SUCCESS;

    resolveRangeList(retList);  // ...get min max, resolve to list, populate
    return rc;
}
/* VMKP added on 020104 */
/* stevev 05sep06 - added */

itemID_t hCNumeric::getAttrID   (unsigned  attr, unsigned which)
{
    itemID_t r = 0; 

    if ( attr >= ((unsigned)varAttrLastvarAttr) )
    {
        LOGIF(LOGP_NOT_TOK)
            (CERR_LOG,"ERROR: Variable's getAttrID with invalid attribute type (%d)\n",attr);
        return r;
    }// else process request

    hCVar* pAttr = getAttrPtr(attr, which);
    if ( pAttr != 0 )
    {
        r = pAttr->getID();
    }
    else
    {
        LOGIT(CERR_LOG,"ERROR(internal): Variable's getAttrID failed.\n");
        r = 0;
    }

    return r;
}

hCVar* hCNumeric::makePsuedoVar(CValueVarient& initValue)
{
    hCVar* pRet = NULL;
    if ( initValue.vIsValid )
    {
        if (initValue.vType == CValueVarient::isIntConst)
        {
            if ( initValue.vIsUnsigned )
            {
                pRet = newPsuedoVar(devHndl(), vT_Unsigned, 4);
            }
            else
            {
                pRet = newPsuedoVar(devHndl(), vT_Integer,  4);
            }
        }
        else
        if (initValue.vType == CValueVarient::isFloatConst)
        {
            pRet = newPsuedoVar(devHndl(), vT_Double,  8);
        }
        else 
        {
            LOGIT(CERR_LOG|CLOG_LOG,"Default Value not Numeric.\n");// return null
        }
        if ( pRet )
        {
            pRet->setDispValue(initValue);
            pRet->ApplyIt();
            pRet->markItemState(IDS_CACHED);// cached & have-data
            // register these for deletion
            string s("NumericAttrPsuedoVar");
            devPtr()->registerItem(pRet->getID(), pRet, s);
            CVarList* pVL = (CVarList*)devPtr()->getListPtr(iT_Variable);
            if ( pVL )  
                pVL->push_back(pRet);
        }

    }// else invalid, NULL
    return pRet;
}

hCVar*   hCNumeric::getAttrPtr  (unsigned  attr, unsigned which)
{   
    hCVar*        pVar = NULL;
    hCNumeric*    pNum = NULL;
    CValueVarient vVar;
    RETURNCODE rc = SUCCESS;
    
    if ( attr >= ((unsigned)varAttrLastvarAttr) )
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,
                    "ERROR: Numeric Var's getAttrID with invalid attribute type (%d)\n",attr);
        return NULL;
    }// else process request
    if ( ! IsValid() )
    {
        return NULL; // when the variable goes away, so does its attributes
    }

    varAttrType_t vat = (varAttrType_t)attr;
    hCattrBase*   pAb = getaAttr(vat);
    if ( pAb == NULL ) // this attribute was not supplied
    {
        return NULL;
    }

    switch(vat)
    {
    case varAttrDefaultValue:
        {
            if (pPsuedoDflt !=NULL)
            {
                pVar = pPsuedoDflt;
            }
            else
            {// make one and return it
                hCattrVarDefault* pvD = (hCattrVarDefault*)pAb;
                pvD->getDfltVal(vVar);
                if (vVar.vIsValid && IsValid() ) vVar.vIsValid=true; else vVar.vIsValid=false;
                pVar = pPsuedoDflt = makePsuedoVar(vVar);
                string nM; nM = getName();
                nM += "_DfltVal";
                // default must be constant: no dependency
                pVar->setName(nM);
            }
        }
        break;
    case varAttrMinValue:
        {
            if (which <= 0 )
            {
                return NULL;// starts with one
            }
            if ( psuedoMinList.size() >= which )
            {
                pNum = psuedoMinList[which-1];
            }
            else
            {
                pNum = NULL;
            }
            // get pNum...
            if (pNum != NULL)// !empty/valid?
            {
                pVar = pNum;
            }
            else  // empty or invalid
            {// make one and return it
                hCattrVarMinVal* pVm = (hCattrVarMinVal*)pAb;
                vVar = pVm->getValue(which);
                if (vVar.vIsValid && IsValid() ) vVar.vIsValid=true; else vVar.vIsValid=false;
                pVar = pNum = (hCNumeric*)makePsuedoVar(vVar);
                string nM; nM = getName();
                nM += "_MinVal";
                if (which != 1) nM += which;
                pVar->setName(nM);
                hCdependency* pD = pVar->getDepPtr(ib_ValidityDep);// value OR validity
                hCminmaxVal* pMM = NULL;
                if ( pD )
                {//acquire the Value dependency (all this psuedo has)
                    pMM = pVm->getValptr( which );
                    if ( pMM )
                    {   
                        vector<hCexprPayload*> payldlist;
                        rc = pMM->condMinMaxExpr.aquireDependencyList(pD->iDependOn, true);
                        rc = pMM->condMinMaxExpr.aquirePayloadPtrList(payldlist);
                        if ( rc == SUCCESS && payldlist.size() > 0 )
                        {
                            for (vector<hCexprPayload*>::iterator iT = payldlist.begin();
                                 iT != payldlist.end();  ++iT )
                            {// ptr2ptr2expression
                                rc = (*iT)->aquireDependencyList(pD->iDependOn);
                            }
                        }
                        //pD->didValidity = true;
                        pD->setValidDone();
                    }
                }
                if ( psuedoMinList.size() < which )
                {
                    psuedoMinList.resize(which);
                }
                psuedoMinList[which-1] = pNum;
            }
        }
        break;
    case varAttrMaxValue:
        {
            if (which <= 0 )
            {
                return NULL;// starts with one
            }
            if ( psuedoMaxList.size() >= which )
            {
                pNum = psuedoMaxList[which-1];
            }
            else
            {
                pNum = NULL;
            }
            // get pNum...
            if (pNum != NULL)// !empty/valid?
            {
                pVar = pNum;
            }
            else  // empty or invalid
            {// make one and return it
                hCattrVarMaxVal* pVm = (hCattrVarMaxVal*)pAb;
                vVar = pVm->getValue(which);
                if (vVar.vIsValid && IsValid() ) vVar.vIsValid=true; else vVar.vIsValid=false;
                pVar = pNum = (hCNumeric*)makePsuedoVar(vVar);
                string nM; nM = getName();
                nM += "_MaxVal";
                if (which != 1) nM += which;
                pVar->setName(nM);
                hCdependency* pD = pVar->getDepPtr(ib_CondDep);
                hCminmaxVal* pMM = NULL;
                if ( pD )
                {
                    pMM = pVm->getValptr( which );
                    if ( pMM )
                    {   
                        vector<hCexprPayload*> payldlist;
                        rc = pMM->condMinMaxExpr.aquireDependencyList(pD->iDependOn, true);
                        rc = pMM->condMinMaxExpr.aquirePayloadPtrList(payldlist);
                        if ( rc == SUCCESS && payldlist.size() > 0 )
                        {
                            for (vector<hCexprPayload*>::iterator iT = payldlist.begin();
                                 iT != payldlist.end();  ++iT )
                            {// ptr2ptr2expression
                                rc = (*iT)->aquireDependencyList(pD->iDependOn);
                            }
                        }
                        //pD->didConditional = true;
                    }
                }
                if ( psuedoMaxList.size() < which )
                {
                    psuedoMaxList.resize(which);
                }
                psuedoMaxList[which] = pNum;
            }
        }
        break;
    case varAttrLabel:
    case varAttrHelp:
        {// the spec says you can???
            return (hCitemBase::getAttrPtr(vat));
        }
        break;
    /** the rest are not allowed to have attribute access **/
    default:
        {// leave pVar NULL //
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getAttrPtr with invalid attribute type %s(%d)\n",
            varAttrStrings[vat],attr);
        }
        break;
    }//endswitch

    return pVar;
}
/* stevev 05sep06 - end add */

/* stevev 22aug06 - to resolve attribute references */
CValueVarient hCNumeric::getAttrValue(unsigned attrType, int which)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient retVal;
    hCattrBase*  pAb = getaAttr((varAttrType_t)attrType);


    if ( pAb == NULL )
    {   return (retVal);  }

    switch((varAttrType_t)attrType)
    {
    case varAttrLabel:          
    case varAttrHelp:               
    case varAttrValidity:
        {
            return ( hCitemBase::getAttrValue(attrType, which));
        }
        break;
    case varAttrMinValue:
        {   
            hCattrVarMinVal* pMinAttr = (hCattrVarMinVal*)pAb;
            retVal = pMinAttr->getValue(which);
            if (retVal.vIsValid && IsValid()) retVal.vIsValid=true; else retVal.vIsValid=false;
/* removed 06sep06 - see earlier versions */
/*******************************************
            //hCMinMaxList  MML(devHndl());
            //rc = pMinAttr->getList(&MML);
            hCMinMaxList MML(devHndl());
            rc = pMinAttr->getList(&MML);
            if ( rc == SUCCESS && MML.size() > 0 )
            {
                for ( MinMaxIT_t iT = MML.begin(); iT != MML.end(); ++iT)
                {// a ptr2a hCminmaxVal
                    if (iT->whichVal == which )
                    {
                        // pre 23aug06::>retVal = iT->ExprPayload.resolve(/x*NULL,false*x/);
//temp - make function-global                       hCexprPayload expPyLd(devHndl());
                        rc = iT->condMinMaxExpr.resolveCond(&expPyLd, false);

                        if ( rc == SUCCESS )
                        {
                            retVal = expPyLd.resolve(NULL,false);
                            retVal.vIsValid = IsValid();
                        }
                        else
                        {// else return an empty retVal
                            LOGIT(CERR_LOG|CLOG_LOG,"Min_Val Attribute did not resolve.\n");
                        }
                        break; // only ONE of each which is allowed!
                    }
                }
            }
            else
            {
                LOGIT(CERR_LOG|CLOG_LOG,"Min_Val Attribute has no List.\n");
                return (retVal);  
            }
            MML.clear();
**************************************/
        }
        break;      
    case varAttrMaxValue:
        {
            hCattrVarMaxVal* pMaxAttr = (hCattrVarMaxVal*)pAb;
            retVal = pMaxAttr->getValue(which);
            if (retVal.vIsValid && IsValid()) retVal.vIsValid=true; else retVal.vIsValid=false;
/* removed 06sep06 - see earlier versions */
/******************************************
            hCMinMaxList MML(devHndl());
            hCattrVarMaxVal* pMaxAttr = (hCattrVarMaxVal*)getaAttr(varAttrMaxValue);
            if ( pMaxAttr == NULL )
            {
                LOGIT(CERR_LOG|CLOG_LOG,"Max_Val Attribute does not exist.\n");
                return (retVal);  
            }// else continue working
            rc = pMaxAttr->getList(&MML);
            if ( rc == SUCCESS && MML.size() > 0 )
            {
                for ( MinMaxIT_t iT = MML.begin(); iT != MML.end(); ++iT)
                {// a ptr2a hCminmaxVal
                    if (iT->whichVal == which )
                    {
                        // pre 23aug06::>retVal = iT->ExprPayload.resolve(/x*NULL,false*x/);
                        hCexprPayload expPyLd(devHndl());
                        rc = iT->condMinMaxExpr.resolveCond(&expPyLd, false);
                        if ( rc == SUCCESS )
                        {
                            retVal = expPyLd.resolve(NULL,false);
                            retVal.vIsValid = IsValid();
                        }
                        else
                        {// else return an empty retVal
                            LOGIT(CERR_LOG|CLOG_LOG,"Max_Val Attribute did not resolve.\n");
                        }
                    }
                }
            }
            else
            {
                LOGIT(CERR_LOG|CLOG_LOG,"Max_Val Attribute has no List.\n");
                return (retVal);  
            }           
            MML.clear();
*********************************************/
        }
        break;
    case varAttrDefaultValue:
        {
            hCattrVarDefault* pDfltVal=  (hCattrVarDefault*)getaAttr(varAttrDefaultValue);
            if (pDfltVal == NULL )
            {
                LOGIT(CERR_LOG|CLOG_LOG,"Default_Val Attribute does not exist.\n");
                return (retVal);  
            }
            else
            {
                rc = pDfltVal->getDfltVal(retVal);
                if (retVal.vIsValid && IsValid() ) 
                    retVal.vIsValid=true; 
                else 
                    retVal.vIsValid=false;
            }
        }
        break;
    default:
        {// this isn't one the spec says you can use
            LOGIT(CERR_LOG|CLOG_LOG,"Illegal attribute type in an expression.\n");
            return (retVal);  
        }
        break;
    }
         /* Spec says these are not available ----------------------
         varAttrClass       varAttrHandling,   varAttrConstUnit,
         varAttrRdTimeout,  varAttrWrTimeout,  varAttrPreReadAct,       
         varAttrPostReadAct,varAttrPreWriteAct,varAttrPostWriteAct, 
         varAttrPreEditAct, varAttrPostEditAct,varAttrResponseCodes,    
         varAttrTypeSize,   varAttrDisplay,    varAttrEdit,
         varAttrScaling,    varAttrEnums,      varAttrIndexItemArray,
         varAttrRefreshAct, varAttrDebugInfo,   ?varAttrTimeFormat
        varAttrLastvarAttr                      ?varAttrTimeScale
        *///---------------------------------------------------------

    return retVal;
}

CValueVarient hCNumeric::condition(CValueVarient& src, IntType_t intType)
{
    CValueVarient ret = src;
    if (intType == No_Type)  return src;
    if ( src.vType != CValueVarient::isIntConst && 
         src.vType != CValueVarient::isVeryLong   ) return src;
    // we know we have an int type
    if ( src.vIsUnsigned )
    {
        if (intType == Un_Signed) // matches, nothing to do
            return src;
        else // must be signed - make it match
            ret.vIsUnsigned = false;// will be returned later
    }
    else // its signed
    {
        if (intType == Is_Signed) // matches, nothing to do
            return src;
        else // must be unsigned - make it match
            ret.vIsUnsigned = true;// will be returned later
    }

    // to get here we have made a change
    return ret;
}
/* end stevev 22aug06 */
RETURNCODE hCNumeric::resolveRangeList(hCRangeList& retList, IntType_t intType, bool isAtest)
{
    RETURNCODE rc = SUCCESS;

    //hCattrVarMinVal* pMinAttr;
    //hCattrVarMaxVal* pMaxAttr;

    hCMinMaxList minAttrList(devHndl());// essentially a vector<hCminmaxVal>
    hCMinMaxList maxAttrList(devHndl());

    /* get any DD specified attributes */
    //pMinAttr = (hCattrVarMinVal*)getaAttr(varAttrMinValue);
    //pMaxAttr = (hCattrVarMaxVal*)getaAttr(varAttrMaxValue);
#ifdef xxx_DEBUG
    if(getID() == 0x00a2)
    {
        LOGIT(CERR_LOG,L"Bp\n");
    }
#endif
    if (pMinVal != NULL)
    {
        pMinVal->getList(&minAttrList);
    }
    if (pMaxVal != NULL)
    {
        pMaxVal->getList(&maxAttrList);
    }

    CValueVarient l;
    CValueVarient h;
    lowest(l);
    highest(h);
    retList.clear();

    // CValueVarient t = expression.resolve();
    if ( (minAttrList.size() > 0 || maxAttrList.size() > 0) && 
         (abs((int)minAttrList.size() - (int)maxAttrList.size()) <=1)   )
    {// the DD supplied something
        localVarientList_t  tmpMinVals;
        localVarientList_t::iterator mmFnd;
        CValueVarient t;
        MinMaxIT_t mmIt;
        hCminmaxVal *pVal;
        hCexprPayload localExpr(devHndl());

        // generate a tmp, resolved min list
        for (mmIt = minAttrList.begin(); mmIt < minAttrList.end(); mmIt++)
        {// mmIt isa ptr 2 a hCminmaxVal
            //t = mmIt->ExprPayload.resolve();
            pVal = (hCminmaxVal *)&(*mmIt);// PAW 03/03/09 added &(*)
            rc = pVal->condMinMaxExpr.resolveCond(&localExpr,isAtest);
            if ( rc == SUCCESS )
            {
                t = localExpr.resolve(NULL,isAtest);
                if ( t.vIsValid )
                {
                    tmpMinVals[pVal->whichVal] = condition(t,intType);
                }
            }
            // else error log???
            localExpr.clear();
        }
        // generate the return list
        for ( mmIt = maxAttrList.begin(); mmIt < maxAttrList.end(); mmIt++)
        {// mmIt isa ptr 2 a hCminmaxVal
            //t = mmIt->ExprPayload.resolve();  
            pVal = (hCminmaxVal *)&(*mmIt);     // &(*) added PAW 03/03/09
            rc = pVal->condMinMaxExpr.resolveCond(&localExpr,isAtest);
            if ( rc == SUCCESS )
            {
                t = localExpr.resolve(NULL,isAtest);
                if ( t.vIsValid )
                {
                    retList[pVal->whichVal].maxVal = condition(t,intType);
                    mmFnd = tmpMinVals.find(pVal->whichVal);
                    if ( mmFnd != tmpMinVals.end() )
                    {               
                        //retList[pVal->whichVal].minVal = mmFnd->second;
                        retList[pVal->whichVal].minVal = tmpMinVals[pVal->whichVal];
                        //mmFnd->second.clear();// mark it as processed
                        tmpMinVals[pVal->whichVal].clear();// sets invalid
                    }
                    else
                    {
                //just compensate for a single mismatch
                //      LOGIT(CERR_LOG,ERR_MISNG_MIN, getName().c_str(), pVal->whichVal);
                        retList[pVal->whichVal].minVal = condition(l,intType);
                                                                      // L not one... lowest()
                    }
                }
            }
            // else error log???
            localExpr.clear();          
        }
        // see if there is a non-symetrical list to the min side
        for ( mmFnd = tmpMinVals.begin(); mmFnd != tmpMinVals.end(); ++mmFnd)
        {// mmFnd isa ptr 2 a localVarientList_t pair
            if (mmFnd->second.vIsValid)// not yet processed (processed have been cleared)
            {           
                //LOGIT(CERR_LOG,"WARN: %s has a numbered [%d] "
                //       "MIN_VALUE without a matching MAX.\n",getName().c_str(),mmFnd->first);
        //just compensate for a single mismatch
        //      LOGIT(CERR_LOG,ERR_MISNG_MAX, getName().c_str(), mmFnd->first);
                retList[mmFnd->first].minVal = mmFnd->second;
                retList[mmFnd->first].maxVal = condition(h,intType);
                mmFnd->second.clear();// mark it as processed
            }// else it's already processed...skip it
        }
    }
    else
    {// set defaults
        hCRangeItem tItm;

        tItm.minVal = condition(l,intType);
        tItm.maxVal = condition(h,intType);

        retList[0] = tItm;
        if (minAttrList.size() > 0 || maxAttrList.size() > 0) 
        {// more than 1 item difference
            LOGIT(CERR_LOG,ERR_MINMAX_MISMATCH, getName().c_str(),
                                                       minAttrList.size(),maxAttrList.size() );
        }
    }

    // stevev 01sep06-the destruct on exit does a destroy on the list, killing our payload ptr
    minAttrList.clear();
    maxAttrList.clear();

    return rc;
}

/* stevev 16nov05 - dependency extentions */
void  hCNumeric::fillCondDepends(ddbItemList_t&  iDependOn)
{
    // call the base to get the label,enumlist & handling
    hCVar::fillCondDepends(iDependOn);
    // the only other attributes:
    
    if ( pDispFmt != NULL )//VAR_DISPLAY_ID 
    {
        pDispFmt->getDepends(iDependOn);
    }    
    if ( pScale != NULL )//VAR_SCALE_ID 
    {
        pScale->getDepends(iDependOn);
    }

    /* 03oct06 - we no longer do this. These attributes do not make any difference to
        the structure, value or validity of this dd item.  
    get any DD specified attributes 
    hCattrVarMinVal* pMinAttr = (hCattrVarMinVal*)getaAttr(varAttrMinValue);
    hCattrVarMaxVal* pMaxAttr = (hCattrVarMaxVal*)getaAttr(varAttrMaxValue);
     
    if ( pMinAttr != NULL )//VAR_MIN_VAL_ID
    {
        pMinAttr->getDepends(iDependOn);
    }    
    if ( pMaxAttr != NULL )//VAR_MAX_VAL_ID 
    {
        pMaxAttr->getDepends(iDependOn);
    }
    ** end 03oct06 **/
}
/** end stevev 16nov05  **/

/* stevev 18nov05 - overload to get var specific notifications */
// called when an item in iDependOn has changed
int hCNumeric::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
    int init = msgList.size();
    
    hCVar::notification(msgList,isValidity);// do validity and label

    // re-evaluate all the others 
    // these will send a value update without changing the internal value
    // so there is no need to call dependent's notification
    if ( ! isValidity )
    {
        if( pScale != NULL && pScale->isConditional() && pScale->reCalc())
        {
            msgList.insertUnique(getID(), mt_Val, pScale->getGenericType());
        }
        if( pDispFmt != NULL && pDispFmt->isConditional() && pDispFmt->reCalc())
        {
            msgList.insertUnique(getID(), mt_Val, pDispFmt->getGenericType());
        }
/*TODO      if( pMinVal != NULL && pDispFmt->isConditional() && pDispFmt->reCalc())
        {
            msgList.insertUnique(getID(), mt_Val, pDispFmt->getGenericType());
        }
        if( pMaxVal != NULL && pDispFmt->isConditional() && pDispFmt->reCalc())
        {
            msgList.insertUnique(getID(), mt_Val, pDispFmt->getGenericType());
        }
*/
    }

    return (msgList.size() - init);// number added
}
/** end stevev 18nov05  **/
/****************************************************************************************
removed 14feb11
wstring    hCNumeric::processFormat(wstring fmtStr)
{
    wstring fIStr(L"%");
    variableType_t  vt = (variableType_t)VariableType();
    
    
    if (fmtStr.empty())
    {
        switch (vt)
        {
        case vT_Integer:        // 2
            {   fmtStr=L"d";
            }
            break;
        case vT_Enumerated: // this should never come here, bu if it does..
        case vT_BitEnumerated:
        case vT_Index:
        case vT_Unsigned:       // 3
            {   fmtStr=L"u";
            }
            break;
        // no formatting....case vT_Enumerated:     // 6
        // no formatting....case vT_Index:          // 8
        case vT_TimeValue:      //20
            {
                if ( ((hCTimeValue*)this)->isTimeScale() )
                    fmtStr=L"f";
                else
                    fmtStr=L"T";
            }
            break;
        case vT_FloatgPt:       // 4
        case vT_Double:         // 5
            {   fmtStr=L"f";
            }
            break;
// latest spec says it uses 'f'
//      case vT_Double:         // 5
//          {   fmtStr=L"e";
//          }
//          break;
//
        default:
            {   LOGIT(CLOG_LOG,
                "WARNING: format free variable type is being asked for its format.\n");
                fmtStr.erase();
            }
            break;
        }
    }
    
    // determine int or double output required
    // if the user embedds one of these, he's SOL
    if ( fmtStr.find_first_of(L"eEfgG") != std::string::npos )
    {// it's a float output
        fIStr += fmtStr;        // use original w/ prepended percent
    }
    else
    if ( vt == vT_Integer    || vt == vT_Unsigned || vt == vT_Index ||
         vt == vT_Enumerated || vt == vT_BitEnumerated )//dloux
    {// it's an int format
    //we are going to assume that there is nothing but the format in the string -stevev 19jun08
        fIStr += fmtStr.substr(0,fmtStr.size()-1);// put everything but type into main string
        fIStr += L"I64";                            // say we're passing in a long long
        fIStr += fmtStr[fmtStr.size()-1];           // tack the type back on the end     
    }
    else
    { // time-format?
        fIStr += fmtStr;        // use original w/ prepended percent
    }

    return fIStr;
}
***************************************************************************************/
/* stevev 15jul08 - move from UI - do formatting in the device object */
/* stevev 08feb11 - modify to handle all format information & update to latest spec         */
/*********************************************************************************************
 *
 *  >>> removed 08feb11isEdit is set to true when we want the edit format info, else will 
    >>> give display format info
 *
 *  maxChars is a return value of maximum width required for the format  [dflt: 0 (no limit)]
 *  rightOdec   a return value of the precision format specifier  [dflt: 1 for int, 6 for flt]
 *  10sep10 - 12.0f is different from 12f,[dflt:-1(not there) for int, 6 for float, 8 for dbl]
 * 08feb11 - default formats are now specified in spec 500 and incorporated here.
 *
 *  returns the type character (diouxaefg)  as a wchar_t in an int
 *          
 *  returns a negative number on error
 *
 *  08feb11 now fills edit format, display format, scan format as well as limit numbers.
 *  NOTE to those who want to do their own formatting: Unscaling and range checking is required
 *      the best way to proceed is use getDispValueString() to get the current value that
 *      is scaled and formatted for you.  You can use the getEditValueString() when the user
 *      goes into edit mode
 *
 */
int hCNumeric::getEditFormatInfo(int& fmtWidth, int& rightOdec, wstring* pScan)
{
    int mx = 0, rr = 0;
    wstring dummy;
    rr = getFormatInfo(fmtWidth,rightOdec,mx,&dummy);
    if (pScan) *pScan = dummy;
    return rr;
}

// public:                This should ONLY be used for the method dialog interface.
// Everybody else should be using getEditValueString() and setDisplayValueString(s)
wstring hCNumeric::getEditFormat(void)
{
    wstring local;
    int maxChars,rightOdec, fmtWid;
    int t = getFormatInfo(fmtWid, rightOdec, maxChars, &local);
    if ( t <= 0 )
    {
        LOGIT(CLOG_LOG|CERR_LOG,"ERROR: getFormatInfo failed.\n");// should never happen
        local = L"";
    }
    return local;
}

// protected
wstring    hCNumeric::getDispFormat(void)
{
    int maxChars, rightOdec, fmtWid;
    wstring e,s,d;
    int t = getFormatInfo(fmtWid, rightOdec, maxChars, &e,&s,&d);

    if ( t <= 0 )
    {
        LOGIT(CLOG_LOG|CERR_LOG,"ERROR: getFormatInfo failed in getDispFormat.\n");
        d = L"";
    }
    return d;
}


/* stevev 15jul08 - move from UI - do formatting in the device object */
/* stevev 08feb11 - modify to handle all format information & update to latest spec         */
/*********************************************************************************************
 *
 *  >>> removed 08feb11::> isEdit is set to true when we want the edit format info, else will 
    >>>                    give display format info
 *
 *  fmtWidth is a return value of maximum width required for the format  [dflt: 0 (no limit)]
 *  rightOdec   a return value of the precision format specifier  [dflt: 1 for int, 6 for flt]
 *  10sep10 - 12.0f is different from 12f,[dflt:-1(not there) for int]
 *  08feb11 - default formats are now specified in spec 500 and have been incorporated here.
 *
 *  returns the type character (diouxaefg)  as a wchar_t in an int < cspn are not legal >
 *  11feb11 - returns edit format information: width,precision,max,& type char unless 
 *            pEditFmtStr is null, then it returns display format information
 *          
 *  returns a negative number on error
 *
 *  08feb11 now fills edit format, display format, scan format as well as limit numbers.
 *  11feb11 All numeric types should have an edit and display format at instatiation.  If
 *          one is not provided in the DD, the new default will be inserted at instantiation.
 *  NOTE to those who want to do their own formatting: Unscaling and range checking is required
 *      the best way to proceed is use getDispValueString() to get the current value that
 *      is scaled and formatted for you.  You can use the getEditValueString() when the user
 *      goes into edit mode
 *
 */
// protected:  
int hCNumeric::getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars,
                            wstring* pEditFmtStr, wstring* pScanFmtStr, wstring* pDispFmtStr)
{
    int retVal = -1;

    int dwidth, dprecision;
    wstring dflags, dLmod;
    wchar_t dconvert;

    int ewidth, eprecision;
    wstring eflags, eLmod;
    wchar_t econvert;

    wstring dfmt, efmt, d1fmt;// has to be different so any use of 'efmt = dfmt;' won't crash
                             // parseFormat(efmt...) <the I^$ in the format gives crash>
    if (pDispFmt != NULL) 
        pDispFmt->getFmtStr(dfmt);// always returns success
    else 
    {
        if (VariableType()!= vT_Enumerated && VariableType() != vT_BitEnumerated)
        {       
            DEBUGLOG(CERR_LOG,"ERROR: getFormatInfo (hCNumeric type: %s) without a disp format "
                "string.(debug only)\n",VariableTypeString());
        }
        int vs = VariableSize();
        switch (VariableType())
        {
        case vT_Integer:
            {
                if ( vs == 1 )          dfmt = L"4d";
                else
                if ( vs == 2 )          dfmt = L"6d";
                else
                if ( vs >2 && vs <=4)   dfmt = L"11d";

                else                    dfmt = L"20d";
            }
            break;
        case vT_Unsigned:
        case vT_Enumerated:
        case vT_BitEnumerated:
        case vT_Index:
            {
                if ( vs == 1 )          dfmt = L"4u";
                else
                if ( vs == 2 )          dfmt = L"6u";
                else
                if ( vs >2 && vs <=4)   dfmt = L"11u";

                else                    dfmt = L"20u";
            }
            break;
        case vT_FloatgPt:
            {
                dfmt = L"12.5g";
            }
            break;
        case vT_Double:
            {
                dfmt = L"18.8g";
            }
            break;
        default:
            {
                dfmt = L"";
            }break;
        }//endswitch
    }

// L&T Modifications : LanguageChange - start
/* The format dfmt and efmt will have the language code along with the value
	if the multile language support is used. Hence it needs to be removed.*/
/* In case of Xmtr-DD, as its device object does not have the languageCode
	the variable langCode will be null and hence finding this in the format
	specifier(dfmt and efmt) results in run-time error.
	The following code made modifications are made:
	1. The variable wlanguage is checked to see if it is empty or not.
	If it is empty then the format specifier is used without modification.
	i.e the language code is not searched in the wlanguage.
	2. Preprocessor directives are inserted to bypass the following code in
	case of Xmtr-DD as language support is not required for this.
	Similar changes are made during the verification of the format specifier efmt.
	*/
#ifndef __GNUC__
#ifdef _CONSOLE
//Define if Multiple languange support is required in Windows console
#undef LANGUAGECHANGE
#endif//_CONSOLE
#else //__GNUC__
//Define if Multiple languange support is required in Linux console
#undef LANGUAGECHANGE
#endif//__GNUC__
#ifdef LANGUAGECHANGE
	wchar_t langCode[10];
	wcsncpy (langCode, ((hCddbDevice*)(this->devPtr()))->dictionary->languageCode, 10);
	std::string tempFormatString(dfmt.begin(), dfmt.end());
	std::wstring wlanguage = langCode;
	std::string slanguage(wlanguage.begin(), wlanguage.end());
	std::size_t findLanguageSymbol = tempFormatString.find(slanguage);
	if(wlanguage != L"")
	{
		if (findLanguageSymbol!=std::string::npos)
		{
			std::string removeLangSymbol = tempFormatString.substr(findLanguageSymbol+4);
			std::wstring tempWstring(removeLangSymbol.begin(), removeLangSymbol.end());
			dfmt = tempWstring;
		}
		else
		{
			findLanguageSymbol = tempFormatString.find("|en|");
			if (findLanguageSymbol!=std::string::npos)
			{
				std::string removeLangSymbol = tempFormatString.substr(findLanguageSymbol+4);
				std::wstring tempWstring(removeLangSymbol.begin(), removeLangSymbol.end());
				dfmt = tempWstring;
			}
		}
	}
#endif //LANGUAGECHANGE
// L&T Modifications : LanguageChange - end
	d1fmt = parseFormat(dfmt,    dflags,dwidth,dprecision,dLmod,dconvert);
	if (pDispFmtStr != NULL) 
        *pDispFmtStr = L"" + d1fmt;
	// else the caller doesn't want the info
	
	efmt = dfmt;// else value
	if (pEditFmt != NULL)
		pEditFmt->getFmtStr(efmt);// always returns success
	else 
	if (VariableType()!= vT_Enumerated && VariableType() != vT_BitEnumerated)
	{		
		DEBUGLOG(CERR_LOG,"ERROR: getFormatInfo (hCNumeric type: %s) without an edit format "
			"string.(debug only)\n",VariableTypeString());
	}

// L&T Modifications : LanguageChange - start
#ifdef LANGUAGECHANGE
	if(wlanguage != L"")
	{
		std::string tempFormatString2(efmt.begin(), efmt.end());
		findLanguageSymbol = tempFormatString2.find(slanguage);
		if (findLanguageSymbol!=std::string::npos)
		{
			std::string removeLangSymbol = tempFormatString2.substr(findLanguageSymbol+4);
			std::wstring tempWstring(removeLangSymbol.begin(), removeLangSymbol.end());
			efmt = tempWstring;
		}
		else
		{
			findLanguageSymbol = tempFormatString2.find("|en|");
			if (findLanguageSymbol!=std::string::npos)
			{
				std::string removeLangSymbol = tempFormatString2.substr(findLanguageSymbol+4);
				std::wstring tempWstring(removeLangSymbol.begin(), removeLangSymbol.end());
				efmt = tempWstring;
			}
		}
	}
#endif //LANGUAGECHANGE
	efmt = parseFormat(efmt,    eflags, ewidth, eprecision, eLmod, econvert);
	if (pEditFmtStr != NULL)
        *pEditFmtStr = L"" + efmt;
	// else the caller doesn't want the info
	
	CValueVarient hi, lo;
	highest(hi);//natural default range
	lowest(lo);

	wstring  hiStr, loStr;


    //isDisplay - calculate the max characters
    if(pEditFmtStr != NULL)
    {
        hiStr = formatValue(hi, *pEditFmtStr); 
        loStr = formatValue(lo, *pEditFmtStr); 
        maxChars = max(hiStr.length(), loStr.length());
        fmtWidth  = ewidth;
        rightOdec = eprecision;
        retVal    = econvert;
        if (pScanFmtStr != NULL)
        {// % optionalwidth optional length-modifier conversion-spec
            *pScanFmtStr = L"%";// start
            if (ewidth>0)
            {
                wchar_t number[64];memset(number,0,sizeof(wchar_t)*64);
                _itow(ewidth,number,10);// optional max width
                *pScanFmtStr += number;
            }// otherwise empty
            *pScanFmtStr += eLmod;   // length-modifier...usually empty
            *pScanFmtStr += econvert;// the conversion type specifier
        }
    }
    else // edit format is null
    if ( pDispFmtStr != NULL )
    {
        hiStr = formatValue(hi, *pDispFmtStr); 
        loStr = formatValue(lo, *pDispFmtStr); 
        maxChars = max(hiStr.length(), loStr.length());//longest natural value disp formt'd
        fmtWidth  = dwidth;
        rightOdec = dprecision;
        retVal    = dconvert;
    }
    else// we have neither format
    {
        maxChars  = 0;  
        fmtWidth  = 0;
        rightOdec = -1;
        retVal    = -1;
    }   
    return retVal;
}

/*
 * this returns an invalid value on error
 *            this is for NUMERIC strings - not time H:m:s nor date m/d/y work here
 *              uses this->editFormat to deal woith the incoming string
 *
 * notes
 * scanf  %[max-fieldWidthNumeric][lengthModifier(size of receiving object)]conversionTypeChar
 * lengthModifier: 'hh' signed/unsigned char;  'h' signed/unsigned short;
 *      'l'(ell) signed/unsigned long for intTypeChar OR double for float TypeChar
 *      'll' signed/unsigned long long int TypeChar 'L' long double for float TypeChar
 *
 */
CValueVarient hCNumeric::getValueFromStr(wstring inputStr)
{
    CValueVarient retVal;
	wstring wrkStr, editFormat, lenMod;
    int vt, dp, prec, h, ft;
    int maxChars, rightOdec, fmtWidth;
    bool  isInt = true;
    wchar_t  fmt = 0xffff,  precStr[MAX_INPUTSTRING];

    // Ensure that the input string is a valid numerical
	// string, POB - 4/28/2014
    if (validateTrim(inputStr))
    {
        /*
         * This is a parsing error!
         */
        retVal.vIsValid = false;
        return retVal;
    }

    //editFormat = getEditFormat();
    wstring dsp,scn;
    ft         = getFormatInfo(fmtWidth, rightOdec, maxChars, &editFormat,&scn,&dsp);
#if 0 
    this functionality is done in getFormatInfo().. code removed 13sep10 (see cvs for contents)
#endif //ft, maxChars, rightOdec
    vt = VariableType();
    if ( editFormat.size() <= 0 || ft < 0) // a getFormatInfo error
    {// bad or missing edit format      
        editFormat = L"";
        rightOdec  = -1;// non existent
        if ( vt == vT_Integer)
        {
            fmt = L'd';
            lenMod     = L"";
            maxChars   = digits4bytes[VariableSize()];
        }
        else if ( vt == vT_Unsigned || vt == vT_Index )
        {
            fmt = L'u';
            lenMod     = L"";
            maxChars   = digits4bytes[VariableSize()]-1;// neg sign uneeded
        }
        else if ( vt == vT_FloatgPt )
        {
            fmt = L'f';// edit format desired
            lenMod     = L"";
            rightOdec  = 6;
            maxChars   = 12;
        }
        else if ( vt == vT_Double )
        {
            fmt = L'f';// edit format desired
            lenMod     = L"";
            rightOdec  = 8;
            maxChars   = 18; 
        }
        else // unsupported
        {
            retVal.vIsValid = false;
            return retVal;
        }
        wchar_t buffer[65];

        editFormat  = L"%";
        editFormat += _itow(maxChars,buffer,10);// max-fieldWidth
        editFormat += lenMod;// length modifier (of receiving var)
        editFormat += fmt;// conversion type
        ft          = fmt;
    }
    else
        fmt = ft;

	wrkStr = scn;
	transform(wrkStr.begin(),wrkStr.end(),wrkStr.begin(),
#if defined(__GNUC__)
			::towlower
#else
			 tolower
#endif // __GNUC__
			 );
/*  // can't happen...filtered above
    if ( ft < 0 )
    {
        retLoc = wrkStr.find_last_of(L"diouxefg");// last instance of any of the letters
        if (retLoc == wstring::npos)
        {
            retVal.vIsValid = false;// error return <no legal formatting characters>
            return retVal;
        }
        else
            fmt = wrkStr[retLoc];// lower case version
    }
    // fmt is OK
**/
    if (fmt == L'e' || fmt == L'f' || fmt == L'g' || 
        fmt == L'E' || fmt == L'F' || fmt == L'G')
    {// float format--- measure input-string's precision
        isInt = false;
        dp = inputStr.find(L'.');
        prec = 0;
        if ( dp != wstring::npos ) 
        {
            for ( prec=0, h = dp+1; 
                  h < (int)(inputStr.length()) && prec < MAX_INPUTSTRING;      h++)
            {
                if (iswdigit(inputStr[h]) )
                {
                    precStr[prec++] = inputStr[h];
                    precStr[prec] = '\0';
                }
                else
                    break; // out of for-loop on any non-digit
            }
            // prec is string-length of digits to right of DP
        }
        //else -- no dp means no precision - leave prec zero

        if (prec > rightOdec && dp != wstring::npos && rightOdec >= 0)
        {// so limit the precision to format amount
            wrkStr   = inputStr.substr(0,dp+1);
            wrkStr  += inputStr.substr(dp+1,rightOdec);
            inputStr = wrkStr;
        }

        // stevev 28aug08 - apparent typo....if ( editFormat.length() > maxChars )
        if( (unsigned)maxChars > 0 && inputStr.size() >  (unsigned)maxChars )
        {
            retVal.vIsValid = false;// error return <bigger than format>
            return retVal;
        }
        // else - process the string
        double ffL = 0.0;
        wchar_t *endPtr = NULL;
        const wchar_t *strtPtr = inputStr.c_str();

        // scanf only goes into floats.  widestring to double
		ffL = wcstod( strtPtr, &endPtr );// handles 'dDeE' as exponent delimiter

        // stevev 26apr11 round to the rightOdec location
        if (rightOdec >= 0)
        {
#if 0
            //ffL += (5/pow(10.0,(rightOdec+1)));
            double p = pow(10.0,(rightOdec+1));
            double d = 5.0/p;
            ffL += d;
#else
            // note - we must round to edit format and then again if we convert to int
            ffL = roundDbl(ffL, rightOdec);
#endif
        }

		//if (inputStr.c_str() == endPtr || ffL == HUGE_VAL || ffL == -HUGE_VAL || *endPtr != 0)
		if ( strtPtr == endPtr || !( ffL >= -maxFloat  && ffL <= maxFloat )
			|| *endPtr != 0)
        {                                                    // J.U. to more strict invalidate
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getValueFromStr did not decode.|%s|\n",
                                                                             inputStr.c_str());
            retVal.vIsValid = false;
        }
        else
        {
            retVal = ffL;
            retVal.vIsValid = true;
        }
    }
    else // not a float edit format
    {
        isInt = true;
        INT64   sfL = 0;

        //verify the I64 is in the non=float versions
        int loc = scn.find(L"I64");
        if ( loc == wstring::npos)
        {
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getValueFromStr (i) needs to add I64\n");
            retVal.vIsValid = false;
        }

		/*  stevev 07feb11 - scanf rolls over when a number larger than I64_MAX is typed in.
			eg '9223372036854775809' comes out as 1 (_I64_MAX = 9223372036854775807)
			This is adequate for the host test because "the value can't be changed to that" 
			too large number.
		*/
// L&T Modifications : VariableSupport - start
#ifdef __GNUC__
             std::string tempStr1(inputStr.begin(), inputStr.end());
             std::string tempStr2;
             sfL = strtoll(tempStr1.c_str(), 0, 0);
#else
// L&T Modifications : VariableSupport - end
		if ( 0 == swscanf(inputStr.c_str(),scn.c_str(),&sfL) )
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,L"ERROR: getValueFromStr (i) did not decode.|%s|\n",
																			inputStr.c_str());
			retVal.vIsValid = false;
		}
		else // success
		{// conversion from signed to unsigned storage
// L&T Modifications : VariableSupport - start
#endif
// L&T Modifications : VariableSupport - end
			if ( vt == vT_Unsigned || vt == vT_Index )
			{
				retVal = (UINT64)sfL;
				retVal.vIsUnsigned = true;
			}
			else			
			{
				retVal.vIsUnsigned = false;
				retVal = sfL;
			}
			retVal.vIsValid = true;
// L&T Modifications : VariableSupport - start
#ifndef __GNUC__
// L&T Modifications : VariableSupport - end
		}
// L&T Modifications : VariableSupport - start
#endif
// L&T Modifications : VariableSupport - end
	}
	return retVal;
}


//scales and formats to display format
wstring&  hCNumeric::scaleNformatValue(CValueVarient incoming)
{
    int w,p,m;
    wstring fmtStr;
    int t = getFormatInfo(w,p,m, NULL,NULL, &fmtStr); 
    CValueVarient scaled;
    scaled = scaleValue(incoming,fmtStr);
    returnString = formatValue(scaled, fmtStr);
    return  returnString; 
}
//to any format
wstring&  hCNumeric::formatValue(CValueVarient incoming, wstring format)
{
    const size_t FCHAR_SIZE = 351;
    wchar_t fchar[FCHAR_SIZE]; // 1.79e305 makes for a long string
    if (format.empty())
    {
        returnString = L"";
        return returnString;
    }

    // CPMHACK : Prefixing '%0' with format value to provide proper display string
    format = L"%0" + format;

    //int w,p,m;
    //wstring fmtStr, fIStr;

	//int t = getFormatInfo(w,p,m, NULL,NULL, &fIStr); //we only need display here
	
	// determine int or double output required
	if ( format.find_first_of(L"eEfFgG") != std::string::npos )
	{// it's a float output
		// bug 2527 - stack blown (a LOT) with swprintf()
		int r =
#if defined(__GNUC__)
				swprintf
#else
				_snwprintf
#endif // __GNUC__
						(fchar,FCHAR_SIZE - 1,format.c_str(),(double)incoming);
		if ( r > (FCHAR_SIZE - 1) || r < 0 )
		{
#if defined(__GNUC__)
			swprintf
#else
			_snwprintf
#endif // __GNUC__
				(fchar,64,L"%g",(double)incoming);

			DEBUGLOG(CERR_LOG,"ERROR: printf failed in formatValue.(debug only)\n");
		}
	}
	else
	{// it's a (long long) int format
		if (incoming.vIsUnsigned)
		{
// L&T Modifications : VariableSupport - start
#ifndef __GNUC__
			swprintf(fchar,
#if defined(__GNUC__)
					FCHAR_SIZE,
#endif // __GNUC__
					format.c_str(), (UINT64)incoming);
#else
/* The above function swprintf was not filling the fchar value properly.
So the below logic is applied for linux. */
                    char buf[100];
                    sprintf(buf, "%llu", (UINT64)incoming.vValue.longlongVal);
                    mbstowcs(fchar, buf, FCHAR_SIZE);
#endif
// L&T Modifications : VariableSupport - end
		}
		else
		{
			swprintf(fchar,
#if defined(__GNUC__)
					FCHAR_SIZE,
#endif // __GNUC__
					format.c_str(), (INT64)incoming);
		}
	}

	returnString = fchar;
	return  returnString; 
}
/*********************************************************************************************
 *  hCenumDesc
 *********************************************************************************************/

#ifdef ENUM_ALLOC_DEBUG

itemID_t enumID; // global instead of passing it all the way down to debug 

int enumCnt = 0;
int enumId  = 1;

// L&T Modifications - start
#ifndef XMTR
extern vector<hCenumDesc*> gblDescList;
#else
vector<hCenumDesc*> gblDescList;
#endif
// L&T Modifications - end

hCenumDesc::hCenumDesc(DevInfcHandle_t h, int from )
    :hCobject(h), descS(h),helpS(h),func_class(h),actions(h)  
{   enumCnt++;
    src = from;
    myId = enumId++;
    eqFromID = 0;
    ccFromID = 0;  clear();
    gblDescList.push_back(this);
    myId = gblDescList.size() - 1;
    ownerID = enumID; 
};

hCenumDesc::hCenumDesc( const hCenumDesc& hCed, int from  )
        :hCobject(hCed.devHndl()), descS(hCed.devHndl()),helpS(hCed.devHndl()),
         func_class(hCed.devHndl()),actions(hCed.devHndl())
{   enumCnt++;
    src  = from;
    myId = enumId++;  
    
    eqFromID = 0;
    ccFromID =  hCed.myId;
    gblDescList.push_back(this);
    myId = gblDescList.size() - 1;
    ownerID = enumID; 
    clear();

    val   = hCed.val;       
    descS = hCed.descS; 
    helpS = hCed.helpS;
    func_class   = hCed.func_class;         actions    = hCed.actions;
    status_class = hCed.status_class;       oclasslist = hCed.oclasslist;
//  m_pComm      = hCed.m_pComm;
}

hCenumDesc& hCenumDesc::operator=(const hCenumDesc& ib)
{   
    eqFromID = ib.myId;
    val   = ib.val;     
    descS = ib.descS;   
    helpS = ib.helpS;
    func_class   = ib.func_class;       actions    = ib.actions;
    status_class = ib.status_class;     oclasslist = ib.oclasslist;
    return *this;
}

hCenumDesc::~hCenumDesc()   
{ 
    descS.destroy();
    helpS.destroy();
    actions.destroy();
    oclasslist.clear(); 

    gblDescList.at(myId) = NULL;
    enumCnt--;
}; 
#else

hCenumDesc::hCenumDesc( const hCenumDesc& hCed )
        :hCobject(hCed.devHndl()), descS(hCed.devHndl()),helpS(hCed.devHndl()),
         func_class(hCed.devHndl()),actions(hCed.devHndl())
{
    val   = hCed.val;       
    descS = hCed.descS; 
    helpS = hCed.helpS;
    func_class   = hCed.func_class;         actions    = hCed.actions;
    status_class = hCed.status_class;       oclasslist = hCed.oclasslist;
//  m_pComm      = hCed.m_pComm;
}

hCenumDesc& hCenumDesc::operator=(const hCenumDesc& ib)
{   
    val   = ib.val;     
    descS = ib.descS;   
    helpS = ib.helpS;
    func_class   = ib.func_class;       actions    = ib.actions;
    status_class = ib.status_class;     oclasslist = ib.oclasslist;
    return *this;
}
#endif
// set this from abstract
void  hCenumDesc::setEqual(void* pAdesc)
{
    if (pAdesc==NULL) 
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: enum descriptor set equal to NULL.\n");
        return;
    }
    aCenumDesc* paEd = (aCenumDesc*)pAdesc;

    val = paEd->val;
    status_class = paEd->status_class;
    if (paEd->oclasslist.size() > 0 )
    {
        oclasslist.clear();
        oclasslist.reserve(paEd->oclasslist.size());
        oclasslist.assign(paEd->oclasslist.begin(),paEd->oclasslist.end());
    }
    descS.setEqual(&(paEd->descS));
    helpS.setEqual(&(paEd->helpS),0);// 31jan12 - won't pass ddht:: );
    func_class.setEqual(&(paEd->func_class));
    actions = paEd->actions;
};                     

void  hCenumDesc::setDuplicate(hCenumDesc* pED)
{
    val = pED->val;
    descS.duplicate(&(pED->descS),false);// does not use 'replicate'
    helpS.duplicate(&(pED->helpS),false);
    func_class.duplicate(&(pED->func_class),false);// does not use 'replicate'
    actions.duplicate(&(pED->actions), false, 0);// put zero in for now);//ref
    
    status_class = pED->status_class;

    if (pED->oclasslist.size() > 0 )
    {/* no complex types, just copy it */
        oclasslist = pED->oclasslist;
    }
    else
    {
        oclasslist.clear();
    }
};       

/*********************************************************************************************
 *
 *  hCenumList
 *
 ********************************************************************************************/
// some functions to perform on this list
hCenumDesc* hCenumList::matchString(const wstring& keyStr)  // NULL on failure
{
        // find a match
    vector<hCenumDesc>::iterator edIT;


    for (edIT = begin(); edIT < end(); edIT ++)  // simple linear search
    {       
        if ( edIT->descS.procureVal(/*pComm*/) == keyStr )
        {
            return (hCenumDesc*)&(*edIT);
        }       
    }
    return NULL; // FAILURE
}

UINT32 hCenumList::coerceValue( const UINT32 val)   // NULL on failure
{
    vector<hCenumDesc>::iterator edIT;
    UINT32 lastVal = begin()->val;
    /* stevev 12-oct-09 assume the list is in order */
    for (edIT = begin(); edIT < end(); edIT ++)  // simple linear search
    {       
        if ( edIT->val <= val ) // target is above us or is us
        {
            lastVal = edIT->val;
        }
        else                    // target is below us
        {
            break;
        }
    }
    return lastVal;
}

hCenumDesc* hCenumList::matchValue( const UINT32 val)   // NULL on failure
{
        // find a match
    vector<hCenumDesc>::iterator edIT;
    hCenumDesc *pEnDesc;

    for (edIT = begin(); edIT != end(); ++edIT)  // simple linear search
    {   
        pEnDesc = &(*edIT);
        if ( pEnDesc->val == val )
        {
            return pEnDesc;
        }       
    }
    return NULL;// FAILURE
}

int   hCenumList::maxDescLen(void) // give the longest Description length
{
    int r = 0;
    wstring local;
    vector<hCenumDesc>::iterator edIT;
    hCenumDesc* thisDesc = NULL;

    for (edIT = begin(); edIT != end(); ++edIT)  // simple linear search
    {       
        thisDesc = &(*edIT);
        local = thisDesc->descS.procureVal();
        if ( ((int)local.length()) > r )
        {
            r = (int)local.length();
        }       
    }
    return r;
}

void  hCenumList::append(hCenumList* pList2Append)
{
    reserve(size() + pList2Append->size()); //PO reserving to prevent multiple allocations
    insert(end(),pList2Append->begin(),pList2Append->end());
}

void  hCenumList::appendUnique(hCenumList* pList2Append)
{
    enumDescList_t::const_iterator elIT;//normal iterator now 'incompatible'(according to bill)
    hCenumDesc *pelIT, *pmyIT;
    hCenumList locDescList(devHndl());// try to get by the iterator issue...
    for (elIT = pList2Append->begin(); elIT != pList2Append->end(); ++elIT)
    {
        enumDescList_t::iterator myIT;
        //enumDescList_t::const_iterator myIT;//try to get rid of iterator mismatch triggered
                                            //(incorrectly) in the following'myIT != end()'
        bool  isDUP = false;
        //pelIT = &((hCenumDesc)(*elIT));
        pelIT = (hCenumDesc*)&(*elIT);

        for (myIT = begin(); 
             myIT != end(); 
             ++myIT)// gave "this->_Has_container()" ++myIT)
        {                                   //myIT != end() ::> "vector iterators incompatible"
            pmyIT = (hCenumDesc *)&(*myIT);
            if ( pmyIT->val == pelIT->val )
            {
                isDUP = true;
                break;// out of internal for loop
            }
        }
        if (! isDUP)
        {
            //push_back(*pelIT);
            locDescList.push_back(*pelIT);
        }
        // else, skip insertion and keep looking
    }
    append(&locDescList);
}



void  hCenumList::setEqual(void* pAclass)
{
    // assume pAclass points to a aCenumList
    if (pAclass == NULL) return;//nothing to do

    aCenumList* paL = (aCenumList*)pAclass;
    aCenumDesc* peL;
    hCenumDesc wrkEnumDesc(devHndl() SRC_LOCAL);
    // stevev 09may13 - showing a leak::> reserve(paL->size()); //PO reserve setequal
    for( AenumDescList_t::iterator iT = paL->begin(); iT != paL->end(); ++iT )
    { 
        peL = &(*iT);
        if (peL != NULL)    
        {
            wrkEnumDesc.setEqual( peL );
            push_back(wrkEnumDesc);
            wrkEnumDesc.destroy();
            wrkEnumDesc.clear();
        }
        else
        {
            LOGIT(CERR_LOG,L"ERROR: received a NULL aCenumDesc ptr.\n");
        }
    } 
}

void  hCenumList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
    // assume pPayLd points to a hCenumList
    if (pPayLd == NULL) return;//nothing to do

    hCenumList* paL = (hCenumList*)pPayLd;
    hCenumDesc wrkEnumDesc(devHndl() SRC_LOCAL);

    FOR_p_iT(enumDescList_t, paL)
    {// iT is ptr 2 a hCenumDesc 
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7 */ \
	|| defined(__GNUC__)
        wrkEnumDesc.setDuplicate( &(*iT) );  // no return status
#else
        wrkEnumDesc.setDuplicate(iT); // no return status
#endif      
        push_back(wrkEnumDesc);

        wrkEnumDesc.clear();        
    }// next
}

// moved from .h file 08may13 ////////////////////////////////////////////////////////////////
hCenumList::~hCenumList()
{   
    hCenumDesc* pED;
    FOR_this_iT(enumDescList_t,this)
    {// iT is ptr 2a hCenumDesc
        pED = &(*iT);// stop using iterator as a ptr 08may13
        pED->destroy();
        pED->clear();   // stevev 12aug11 -added after seeing invensys check in.can't hurt
    }
    clear(); // the enumDescList_t

/*
    LINUX_PORT - REVIEW_WITH_HCF: Having to call STL container base class 
    destructor explicitly because they are not designed to be base classes 
    (no virtual destructor).
*/
	if ( size() ) // stevev 26aug10 try to avoid deletion crash & memory leaks
#if defined(__GNUC__)
		this->~enumDescList_t();
#else // !__GNUC__
		enumDescList_t::~vector(); 
#endif // __GNUC__
};
//VMKP 260304
RETURNCODE hCenumList::destroy(void)
{ 
    hCenumDesc* pED;
    RETURNCODE rc = 0;
    FOR_this_iT(enumDescList_t,this)
    {// iT is ptr 2a hCenumDesc
        pED = &(*iT);// stop using iterator as a ptr 08may13
        rc |= pED->destroy();
    }
    clear();
    return rc;
}

hCenumList& hCenumList::operator= (const hCenumList& src)
{  
    const hCenumDesc* pED;
    
    for(enumDescList_t::const_iterator iT = src.begin();iT!=src.end();++iT)
    {
        pED = &(*iT);
        push_back( *pED); // makes a copy
    }  
    return *this;
};

////////// end 08may13 move from .h file /////////////////////////////////////////////////////

/*********************************************************************************************
 *
 *  hCenum
 *
 ********************************************************************************************/

bool hCEnum::isInRange(CValueVarient val2Check)
{ 
    hCenumList returnList(devHndl());
    hCenumDesc* ped;
    bool r = false; // all in list invalid, cannot be in range

    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }

    if ( procureList(returnList) == SUCCESS && returnList.size() > 0 )
    {
         ped = returnList.matchValue( (UINT32) val2Check);   // NULL on failure
         r = ( ped != NULL ); // true on found
    }
    // else  r is still false
    returnList.destroy();
    return r;
};

// 09mar11 - receive calls notification which can readimd and wait for response-deadlock
//         - added isTest for notification - defaults false
RETURNCODE hCEnum::procureList( hCenumList& returnList, bool isTest) // get the resolved list
{   
    RETURNCODE rc = SUCCESS;
// unused   vector<hCenumList>* pListOfEnumLists = NULL;
// unused   hCenumDesc localEnumDesc(devHndl());
    // stevev 03mar06 - get rid of huge memory leak by always resolving into an empty list
    try     // J.U. 
    {       
        while (returnList.size() > 0 )
        {
            returnList.back().destroy();
            returnList.back().clear();
            returnList.pop_back();
            // (resolve cond MUST append to the list!!)
        }
        returnList.clear();
    }
    catch(...)
    {
        LOGIT(CERR_LOG|CLOG_LOG, L"UNMANAGED ERROR: returnList.clear() failed. ");
        return SUCCESS;
    }


    rc= condEnumDescrList.resolveCond(&returnList, isTest);

    if (rc != SUCCESS || returnList.size() == 0)
    {// note that a conditional without a default||else stmt will exit here
        // the user gets an empty list...
        try // J.U. 
        {
            returnList.clear();
        }
        catch(...)
        {
            LOGIT(CERR_LOG|CLOG_LOG, L"UNMANAGED ERROR: empty returnList.clear() failed. ");
        }
        return SUCCESS;
    }
    else
    {
        // wtf....returnList = returnList;//EL;  // a hCenumList
        // stevev 04sep13 - we have to re-resolve the conditional strings 
        enumDescList_t::iterator IT;
        hCenumDesc* pEdesc;
        for(IT = returnList.begin(); IT != returnList.end(); ++IT)
        {
            pEdesc = &(*IT);
            if (pEdesc && 
                (pEdesc->descS.getStrType() == ds_Enum ||
                 pEdesc->descS.getStrType() == ds_Var    )
                 )
            {
                pEdesc->descS.fillStr();
            }
        }

        return SUCCESS;
    }
};

RETURNCODE hCEnum::procureList( triadList_t& returnList) // get the resolved list
{
    RETURNCODE rt = SUCCESS;
    hCenumList eList(0);
    EnumTriad_t wrkTriad;
    enumDescList_t::iterator edIT;

    rt = procureList(eList);
    if ( rt == SUCCESS && eList.size() > 0 )
    {
        for(edIT = eList.begin(); edIT != eList.end(); ++edIT)
        {
            wrkTriad = *((hCenumDesc*)&(*edIT));// PAW 03/03/09 added &(*)
            returnList.push_back(wrkTriad);
            wrkTriad.clear();
        }
    }
    return rt;
}

// get one element
RETURNCODE hCEnum::procureEnum(const UINT32 value, hCenumDesc& returnEnum)
{
    RETURNCODE rc = FAILURE;

    returnEnum.clear();
    //hCenumList  realList(devHndl());
    ptr2EnumDesc_t pED = NULL;
    if ( (rc = procureList( Resolved )) == SUCCESS )
    {
        pED = Resolved.matchValue( value );   // NULL on failure
        if ( pED != NULL )
        {
            returnEnum = *pED;
            rc = SUCCESS;
        }// else leave it failure, null
    }
    // else - do nothing, return failure
    return rc;
}

 //aCvTenumerated   : public aCvarEnumType
 //class aCvarEnumType   : public aCvarType
        //aCcondList<aCenumList> condEnumDescriptor;// 
        //           aCenumList  which is a list of :aCenumDesc:
 //  class aCvarType : public aCitemBase
        //aCconditional<aCvarTypeDescriptor> condTypeDescriptor; // not in attr list
        //string   sTypeName;

 // construct from a dllapi item
//*%hCEnum::hCEnum(aCvTenumerated* pActual)
//*%        /* init base class */
//*%        :hCVar((aCvarType*)pActual)//,pV)
 hCEnum::hCEnum(DevInfcHandle_t h, aCitemBase* paItemBase)
        /* init base class */
        :hCinteger(h, paItemBase,false), condEnumDescrList(h), Resolved(h) //,pV)
 {   // stevev 02/10/04 - we gotta have 'em both...see bottom of method
    hSdispatchPolicy_t policy = devPtr()->getPolicy();

     // init conditional list of lists
    copyDesc(paItemBase); // pass the base class
//temporary       
    sprintf(fmtStr,"0x%c0%dX\0",'%',(VariableSize() * 2));// 2 nibbles per byte
    // initialize
    if ( policy.isReplyOnly )
    {// we are a device
/* during instantiation, the critical parameters have not been loaded
// ya gotta wait till default values are set
        hCenumList tmpList(h);
        if ( SUCCESS == condEnumDescrList.resolveCond(&tmpList) && tmpList.size() > 0)
        {
            Wrt_Val = Value = tmpList[0].val; // try to use the first valid value
        }
        else
        {
            Wrt_Val = Value = 0x0;// otherwise force zero
        }
***/
		Wrt_Val = Value = 0x0;// otherwise force zero
        markItemState(IDS_CACHED);
    }
    else
        // we are a host
        Wrt_Val = Value = 0xffffffff;   

 }


hCEnum::hCEnum(hCEnum*    pSrc,  hCEnum*    pVal, itemIdentity_t newID):
        hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID),condEnumDescrList(pSrc->devHndl()),
            Resolved(pSrc->devHndl())
{
    strncpy(fmtStr, pSrc->fmtStr, sizeof(fmtStr));
    condEnumDescrList.setDuplicate(pSrc->condEnumDescrList, false);// not dup'd
}

        
void hCEnum::lowest (CValueVarient& retVal)
{
    //hCenumList returnList(devHndl());
    unsigned int L = 0xffffffff;
    bool r = false;

    if ( procureList(Resolved) == SUCCESS && Resolved.size() > 0 )
    {
        if ( VariableType() != vT_BitEnumerated)
        {
            for (hCenumList::iterator iT = Resolved.begin(); iT < Resolved.end(); ++iT)
            {// ptr2a hCenumDesc
                if ( iT->val < L )
                {
                    L = iT->val;
                r = true;
                }
            }//next
        }
        else
        {// is bit-enumerated
            r = false; // force lowest to be zero
        }
        if ( ! r ) 
        {
            retVal = (unsigned int) 0;
            retVal.vIsValid = true;
        }
        else
        {
            retVal = L;
        }
    }
    else
    {
        LOGIT(CLOG_LOG,"--procure enum list failed for 0x%04x.\n",getID());
        retVal = (unsigned int) 0;
        retVal.vIsValid = false;
    }
//  returnList.destroy();
    return;
}

void hCEnum::highest(CValueVarient& retVal)
{
    //hCenumList returnList(devHndl());
    unsigned int h = 0;
    bool r = false;

    if ( procureList(Resolved) == SUCCESS && Resolved.size() > 0 )
    {
        for (hCenumList::iterator iT = Resolved.begin(); iT < Resolved.end(); ++iT)
        {// ptr2a hCenumDesc
            if ( iT->val > h )
            {
                h = iT->val;
                r = true;
            }
        }//next
        if ( ! r ) 
        {
            retVal = (unsigned)limitAndExtend(0xffffffff);
            retVal.vIsValid = false;
        }
        else
        {
            if ( VariableType() == vT_BitEnumerated )
            {
                h = 0xffffffff >> ( (sizeof(long)*8) - mask2value(h) - 1 );
            }

            retVal = h;
        }
    }
    else
    {
        LOGIT(CLOG_LOG,"--procure enum list failed for 0x%04x.\n",getID());
        retVal = limitAndExtend(0xffffffff);
        retVal.vIsValid = false;
    }
    //returnList.destroy();
    return;
}

RETURNCODE  hCEnum::setDefaultValue(void)
{
    RETURNCODE rc = SUCCESS;
    CValueVarient  vv;
    UIntList_t   vvList;

    if ( getAllValidValues(vvList) <= 0 )
    {
        LOGIT(CERR_LOG,"ERROR: enum 0x%04x (%s) has no valid values.\n",
                                                            (int)getID(),getName().c_str());
        vv = (UINT32) 0;
        rc = FAILURE;
    }
    else // has a vv list
    {// set to first - not lowest
        vv = (UINT32) *(vvList.begin());
        rc = SUCCESS;
    }
    rc = hCVar::setDefaultValue(vv);// from hCVar
    vvList.clear();
    return rc;

    /* moved to common code in hCVar...8jul05... see previous version for details
       removed completely 07jan09
    *** end moved 8jul05 ***/

    vvList.clear();
    return rc;
}

/* stevev 12sep06 - dependency extentions */
void  hCEnum::fillCondDepends(ddbItemList_t&  iDependOn)
{
    // call the base to get the label,handling,display,scale
    hCNumeric::fillCondDepends(iDependOn);         
    
    condEnumDescrList.aquireDependencyList(iDependOn,false);
}
/** end stevev 12sep06  **/


/*********************************************************************************************
 *
 * Bit enumerated
 *
 *********************************************************************************************/
// get by mask - NOTE: mask is NOT verified for inRange!
CValueVarient hCBitEnum::getRealValue(ulong indexMask)
{
    CValueVarient retVal;
    retVal = hCinteger::getRealValue();// must be an int

    if ( retVal.vIsValid )
    {
        int y = ((int)retVal) & indexMask;
        retVal.vType = CValueVarient::isBool;
        if ( y )
        {
            retVal.vValue.bIsTrue = true;
        }
        else
        {
            retVal.vValue.bIsTrue = false;
        }
    }
    else
    {
        retVal.clear();
    }
    return retVal;  
}

// get by mask - returns bool, are bit(s) in mask set?
CValueVarient hCBitEnum::getDispValue(ulong indexMask,bool iHaveMutex)
{
    CValueVarient retVal;
    retVal = getRawDispValue();// must be an int(hCinteger's getRaw..)
    if ( retVal.vIsValid )
    {
        int y = ((int)retVal) & indexMask;
        retVal.vType = CValueVarient::isBool;
        if ( y )
        {
            retVal.vValue.bIsTrue = true;
        }
        else
        {
            retVal.vValue.bIsTrue = false;
        }
    }
    else
    {
        retVal.clear();
    }
    return retVal;  
}

// indexMask tells what bits in the newValue are active (set/clear only bits in indexMask)
void hCBitEnum::setDispValue(CValueVarient& newValue, ulong indexMask, bool iHaveMutex)
{
    unsigned int y = (unsigned int)newValue;
    unsigned int z, w, v = getDefinedMask();
    CValueVarient varVal;
    
    z = (unsigned int)Wrt_Val;

    indexMask &= v;// only allowed to modify defined bits - clear others if attempt made

    w = z & ~indexMask;         // clear all the affected bit(s)
    w = w | (y & indexMask);    // (re)set the passed-in bit(s) filtered by affected bit mask

    // consolodate activity into setDispValue stevev 22apr05
    varVal = w;
    hCinteger::setDispValue(varVal);
};

void hCBitEnum::setRealValue(CValueVarient& newValue, ulong indexMask, bool iHaveMutex)
{
    unsigned int y = (unsigned int)newValue;
    unsigned int z, w;  
    
    z = (unsigned int)Value;

    w = z & ~indexMask;         // clear the intended bit(s)
    w = w | (y & indexMask);    // set the passed-in intended bit(s)

    Value = w;
};

RETURNCODE hCBitEnum::setDisplayValue(CValueVarient& newValue) // from UI w/ rangetest 
{
/* 17nov11 - this is no longer valid to use on a bit enum (see .h file)
   -- we have to leave the overide or the integer set display value will be used --
    if ( isInRange(newValue) )
    {
        hCinteger::setDispValue(newValue);
        return SUCCESS;
    }
    else
    {
        return APP_OUT_OF_RANGE_ERR;
    }
**** end 17nov11 ***/
    // too much LOGIT(CLOG_LOG|CERR_LOG,"Program error: Bit enum set display value called.\n");
    //return FAILURE;
     setDispValue(newValue, getDefinedMask());// 17nov11
     return SUCCESS;
}

bool hCBitEnum::isInRange(CValueVarient val2Check)
{
    return sizeAndScale(val2Check); // much better way to test a bit enum
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    /**** stevev 2may08 **** use a better technique
    hCenumList returnList(devHndl());
    hCenumDesc* ped;
    UINT32 valCheck = (UINT32)val2Check;
    UINT32 msk = 0;
    bool r = true;

    if ( procureList(returnList) == SUCCESS && returnList.size() > 0 )
    {
        if (valCheck == 0)
        {
            // stevev 02may08 - enums don't have a value for zero...
            //ped = returnList.matchValue(valCheck);   // NULL on failure
            //r   = ( ped != NULL ); // true on found
        }
        else
        {
            while (valCheck != 0 && r )// any bit mismatch is a failure
            {
                msk = 1 << right0bits(valCheck);

                ped = returnList.matchValue( msk );   // NULL on failure
                r = ( ped != NULL ); // true on found
                valCheck &= ~msk;
            }
        }
    }
    else
    {   r = false;// no valid values to test against...all values are invalid
    }
    returnList.destroy();
    return r;
    *** end better technique replacement ***/
}

// false = out-of-range, true=new value in retVal
bool   hCBitEnum::sizeAndScale(CValueVarient& retVal)
{
/* 17nov11 - the DD working group has decided (and codified in the dd host test) that bitenums
    do NOT have a value to change or read.  They must be treated as a collection of Bits. That
    implies only those bits that are defined in the DD may be seen or modified by the user. 
    this function has been changed to accomodate this interpretation.   stevev
 */
    //  determine if any bits that are differnt are defined in the DD
    unsigned m = getDefinedMask();
    unsigned n = (unsigned)retVal;
    unsigned c = (unsigned) Value;// just the low half

    unsigned changed = c ^ n;
    unsigned unallowed = changed & ~m;// non=zero if any changed bits are NOT in the mask

    return ( unallowed == 0 );// in range if we did not change any undefined bits

    /* previous to 17nov11 was:   
    unsigned long msk,v = (unsigned long)retVal;
    hCenumList returnList(devHndl());
    hCenumList::iterator  penum;
    ptr2EnumDesc_t        pEnumDesc;
    //hCenumDesc* ped;
    //UINT32 valCheck = (UINT32)val2Check;
    //UINT32 msk = 0;
    bool r = true;

    if ( procureList(returnList) == SUCCESS && returnList.size() > 0 )
    {
        msk = 0L;
        for (penum = returnList.begin(); penum != returnList.end(); ++penum)
        {
            pEnumDesc = (ptr2EnumDesc_t)&(*penum);// PAW 03/03/09 added &(*)
            msk |= pEnumDesc->val;
        }
        v = ~msk & v; 
        if ( v != 0 ) // retVal has extra bits set
        {
            r = false;
            retVal = v; // a valid value to return
        }
        else
        {
            r = true;
        }
    }
    else
    {   r = false;// no valid values to test against...all values are invalid
    }
    returnList.destroy();
    return r;
    */
}

RETURNCODE  hCBitEnum::setDefaultValue(void)
{
	CValueVarient vv;
	/* 
		LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix ambiguous assignment 
		compiler error (no long assignment override available). Check if that 
		is what was intended.
	*/
	vv = (int) 0L;
	hCVar::setDefaultValue(vv);// from hCVar
	return SUCCESS;

    /* moved to common code in hCVar...8jul05... see previous version for details
    RETURNCODE rc = SUCCESS;
    CValueVarient l;

    if ( hCinteger::setDefaultValue() == SUCCESS )  // we have a DD dfltVal
    {
        l = Wrt_Val;
    }
    else // no DD default value
    {// use the lowest
        l = 0x00; // bitenums go to ZERO (all bits clear), not a value
    }
    setDispValue(l, 0xffffffff);
    ApplyIt();// display to device
    return rc;
    *** end move 8jul05 ****/
}

// 17nov11 - added for new definition of bit enums
UINT32  hCBitEnum::getDefinedMask(void)
{
    RETURNCODE r = FAILURE;
    UINT32  retVal = 0;

    hCenumList localList(devHndl());
    r = procureList( localList );
    if ( r != SUCCESS || localList.size() <= 0 )
    {
        LOGIT(CERR_LOG, "ERROR: No eList for bit enum mask.\n");
        return 0;// there are no defined bits
    }

    for (hCenumList::iterator iT = localList.begin(); iT != localList.end(); ++iT)
    {// tokenizer is supposed to check for a single bit per value
        ptr2EnumDesc_t pED = (ptr2EnumDesc_t)&(*iT);
        retVal |= pED->val;
        pED->destroy(); // we won't need it again
    }
    localList.clear();// try to avoid mem leaks

    return retVal;
}

/* stevev 16nov05 - dependency extentions * use enum's version /
void  hCBitEnum::fillCondDepends(ddbItemList_t&  iDependOn)
{
    // call the base to get the label,handling,display,scale
    hCNumeric::fillCondDepends(iDependOn);
    // the only other attributes:
    //BIT_ENUM_LIST       
    
    condEnumDescrList.aquireDependencyList(iDependOn,false);
}
/  ** end stevev 16nov05  **/

/* stevev 18nov05 - overload to get var specific notifications */
/* stevev 05oct05 - made it the same for bit enum and enum     */
// called when an item in iDependOn has changed
int hCEnum::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals 
    int init = msgList.size();
    
    hCNumeric::notification(msgList,isValidity);// do validity and others

    // re-evaluate list (treated as a menu)
    if ( ! isValidity && condEnumDescrList.isConditional())
    {// now we got some work to do...
        bool isChanged = false;

        hCenumList initialList(Resolved);
        // 09mar11 make procure not send commands from notification (avoid deadlock)
        if ( SUCCESS == procureList(Resolved, true) )
        {
            if ( initialList ==  Resolved)
            {
                ; /* do nothing ( we didn't overload the != */
            }
            else    /*initialList !=  Resolved */
            {
                msgList.insertUnique(getID(), mt_Str);
            }
        }
        // else - error, just leave
    }

    return (msgList.size() - init);// number added
}
/** end stevev 18nov05  **/

//=================================

hCindex::hCindex(DevInfcHandle_t h, aCitemBase* paItemBase)
        :hCinteger(h,paItemBase,false)
{
    validValueList.clear();

    if (paItemBase == NULL)
        return;
    aCattrBase* aB = NULL;

    // stevev - removed merge error...
    aB = paItemBase->getaAttr(varAttrIndexItemArray) ; 
    if (aB == NULL)
    { 
        pIndexed = NULL;
        LOGIT(CERR_LOG,L"ERROR: No Indexed Item attribute in a Index Variable.\n"); 
    }
    else  // generate a hart class from the abstract class
    { 
        pIndexed  = new hCattrVarIndex(h, (aCattrCondReference*)aB );
        pIndexed->setItemPtr((hCitemBase*)this);
        attrLst.push_back(pIndexed);
    }
    //VMKP 290304
    /* For index Variable always set default value as zero ( In Rosemount 3051,  the Snsr
    trim Calc typ never get read from the device, and command 139 keeps on sending,
    because the default value for a index variable is set to 0xff (Item id: 0xfd6) and 
    this value is going in a command 139 request, device is responding with Non-zero response
    code and we are invalidating the reply packet,  By setting all the index variable
    default value to zero, this issue gets resolved,  
    The default value was zero in the old code (2 months back) and it was working fine. */
    Wrt_Val = Value = 0; // bit enums will start all clear
    //VMKP 290304
}

/*  NOTE: this has to point to the same array, value must be the closest valid value */
hCindex::hCindex(hCindex* pSrc, hCindex* pVal, itemIdentity_t newID) : 
         hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID),pIndexed(pSrc->pIndexed)
{
    validValueList = pSrc->validValueList;

    if (pSrc->pIndexed != NULL ) 
    {
        // stevev 20jul06 - share attributes //
        //pIndexed  = (hCattrVarIndex*)(pSrc->pIndexed->new_Copy());
        pIndexed  = pSrc->pIndexed;
    }
    else 
    {
        pIndexed = NULL;// should be illegal
    }
    if ( ! isAvalidValue(Value) )
    {
        UIntList_t tmpLst;
        if ( getAllValidValues(tmpLst) <= 0 )
        {
            setDefaultValue();
        }
        else // has valid values, this ain't one of 'em
        {   
            int lastVal = -1, found = 0;;

            for (UIntList_t::iterator iT = tmpLst.begin();iT<tmpLst.end();iT++)
            {// ptr 2a int
                if ( *iT > Value )
                {// we went past it's place
                    if ( lastVal < 0 )
                    {
                        Value = *iT; // at the front
                    }
                    else if ( (Value - lastVal) < (*iT - Value) )
                    {
                        Value = lastVal;// closest
                    }
                    else
                    {
                        Value = *iT;// closest or default
                    }
                    found = 1;// flag it
                    break; // we don't need to keep going
                }
                else
                {// we have gotten there yet
                    lastVal = *iT;
                }
            }//next
            if ( ! found )
            {
                Value = lastVal;// at the end
            }
        }
    }// else, leave it
}

// 10apr07 - stevev moved from .h file
/*virtual*/
hCindex::~hCindex()
{   
    validValueList.clear();  
    resolved.erase(); 
    if(pIndexed && (! sharedAttr))
    {   
        hCattrBase* pAB ;
        if ( (pAB = getaAttr(varAttrIndexItemArray)) == pIndexed )    
            pIndexed = NULL; // allow delete from list in hCitemBase
        else
        {// not in the list, delete it here
            pIndexed->destroy();
            delete pIndexed; 
            pIndexed = NULL;
        }
    } 
}

int hCindex::getAllValidValues(UIntList_t& returnList)// returns count in list
{
    int cnt = 0;
    vector<hCitemBase*> ArrayPtrList;
    vector<hCitemBase*> ::iterator aplIT;// ptr 2a ptr 2a hCitemArray, hCarray,hClist
    
    if ( pIndexed != NULL )
    {
        if ( pIndexed->getAllArrayPtrs(ArrayPtrList) == SUCCESS && ArrayPtrList.size() > 0)
        {// for all conditional outcomes
            for (aplIT = ArrayPtrList.begin();aplIT < ArrayPtrList.end(); aplIT++)
            {
                (*aplIT)->getAllindexValues(returnList);
            }
            cnt = returnList.size();
        }
        // else return zero
        else
        {
            DEBUGLOG(CLOG_LOG,"Index Variable 0x%04x (%s) indexes no items!\n",
                getID(),getName().c_str());
        }
    }// else just return zero
    else
    {
        DEBUGLOG(CLOG_LOG,"Index Variable 0x%04x (%s) has no 'indexed' attribute!\n",
            getID(),getName().c_str());
    }
    if ( cnt == 0 )
    {
        DEBUGLOG(CLOG_LOG,"Indexed item(s) have no contents!\n");
    }
    return cnt;
}

bool hCindex::isAvalidValue(UINT64 val)
{
    UIntList_t  localList;
    UIntList_t* pList;
    UIntList_t::iterator lIT;
    bool r = false;
    ;
    if (validValueList.size() > 0 )
    {
        pList = &validValueList;
    }
    else
    if (getAllValidValues(localList) > 0)
    {
        pList = &localList;
    }
    else
    {
        pList = NULL;
    }
    if ( pList != NULL )
    {
        for (lIT = pList->begin(); lIT < pList->end(); lIT++)
        {
            if ( *lIT == val )
            { 
                r = true;
                break; // out of loop
            }
        }
    }
    // else - leave it false - no valid values, it can't be in the list

    // stevev 15may09 - Single byte indexes are restricted in range
    if ( !r && VariableSize() == 1 && val >= 250)
    {
        r = true; // a single-byte enumeration encoded index value
    }
    // stevev 15may09 - end this part of the restricted range issue
    return r;
}


void hCindex::lowest (CValueVarient& retVal)
{
    UIntList_t   vvList;
    UIntList_t ::iterator vvIT;
    //unsigned int L =numeric_limits<unsigned int>::max();//from 0xffffffff; 29jun06(4 symetry)
    unsigned int L = UINT_MAX; // 12sep07 - conflict twixt <limits> & 'ddbDevice.h'
    bool r = false;
    int cnt;

    if ( (cnt = getAllValidValues(vvList)) <= 0 )
    {
		LOGIT(CERR_LOG,"ERROR: index 0x%04x (%s) has no valid values.\n",
             (int)getID(),getName().c_str());
        hCinteger::lowest(retVal);
        retVal.vIsValid = false;
        return;
    }
    else
    if ( cnt == 1 )
    {
        retVal = *(vvList.begin());
            return;
    }
    else //  a vv list
    {// get the lowest
        for (vvIT = vvList.begin(); vvIT<vvList.end();vvIT++)
        {
            if ( (*vvIT) < L ){ L = *vvIT; r = true; } // else go to next
        }
        if ( ! r )
        {
            LOGIT(CERR_LOG,"ERROR: index 0x%04x (%s) has incorrect values.\n",
                                                            (int)getID(),getName().c_str());
            hCinteger::lowest(retVal);
            retVal.vIsValid = false;
            return;
        }
    }
    retVal = L;
    return;
}

void hCindex::highest(CValueVarient& retVal)
{
    UIntList_t   vvList;
    UIntList_t ::iterator vvIT;
    unsigned int L = 0;// 12sep07 conflict w/'ddbDevice.h' numeric_limits<unsigned int>::min()
                       // changed from 0 29jun06
    bool r = false;
    int cnt = 0;

    if ( (cnt = getAllValidValues(vvList)) <= 0 )
    {
        LOGIT(CERR_LOG,"ERROR: index 0x%04x (%s) has no valid values.\n",
                                                            (int)getID(),getName().c_str());
        hCinteger::highest(retVal);
        retVal.vIsValid = false;
        return;
    }
    else
    if ( cnt == 1 )
    {
        retVal = *(vvList.begin());
        return;// stevev 10jun09 - don't let it fall thru and be reset to 0
    }
    else //  a vv list
    {// get the highest
        for (vvIT = vvList.begin(); vvIT<vvList.end();vvIT++)
        {
            if ( (*vvIT) > L ){ L = *vvIT; r = true;} // else go to next
        }
        if ( ! r )
        {
            LOGIT(CERR_LOG,"ERROR: index 0x%04x (%s) has incorrect values.\n",
                                                            (int)getID(),getName().c_str());
            hCinteger::highest(retVal);
            retVal.vIsValid = false;
            return;
        }
    }
    retVal = L;
    return;
}

RETURNCODE  hCindex::setDefaultValue(void)          // integer parent
{
    RETURNCODE rc = FAILURE;
    CValueVarient vv; vv = 0;
    UIntList_t   vvList;
    hCitemBase*  pIndexedItm = NULL;
    
    if ( getAllValidValues(vvList) > 0 )
    {
        vv = (UINT32) *(vvList.begin());
        rc = SUCCESS;
    }
    else // no valid values
    {
        if (pIndexed != NULL)
        {
            pIndexed->getArrayPointer(pIndexedItm);
            if ( pIndexedItm  && pIndexedItm->getIType() == iT_List )
            {
                hClist* pLst = (hClist*)pIndexedItm;
                unsigned listCnt = pLst->getCount();
                if ( listCnt < 1 )
                {// assume default-default value stevev 14jan09 
                    vv = 0; // 14jan09 changed from -1 to zero
                    rc = SUCCESS;
                }
                else
                {// leave vv & rc as is
                    LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: List %s has %d as a count "
                        "but has no valid values.\n",pIndexedItm->getName().c_str(),listCnt);
                }
            }
            else
            if (pIndexedItm)
            {// leave vv & rc as is
                LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
                  "ERROR: Item %s is not a list but has no valid values.\n ",
                   pIndexedItm->getName().c_str());
            }
            else
            {// leave vv & rc as is - tokenizer should never allow this to happen
                LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
                                            "ERROR: Indexed Item does not exist in the DD.\n");
            }
        }
        else 
        {// leave vv & rc as is - tokenizer should never allow this to happen
            LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: Indexed Item is NULL.\n ");
			vv = 0;
        }
    }
    hCVar::setDefaultValue(vv);// from hCVar
    vvList.clear();
    return rc;

/* commented code removed 7jan09 - stevev (see earlier versions for contents */
};

RETURNCODE hCindex::setDisplayValue(CValueVarient& newValue)// from UI w/ rangetest 
{   // if not xmtr
    if ( devPtr()->getPolicy().isReplyOnly || isInRange(newValue))
    {
        setDispValue(newValue);
        return SUCCESS;
    }
    else
    {
        return APP_OUT_OF_RANGE_ERR;
    }
}

RETURNCODE hCindex::notify(hCmsgList& msgList)
{
    if(! IsLocal())
    {       
        msgList.insertUnique(getID(), mt_All);
/* instead::        
msgList.insertUnique(getID(), mt_Str, 0);// mt_Val didn't update the root_menu right
   - updates some of the window view  but leaves out the tree view unchANGED */
    }
    return hCitemBase::notify(msgList);
}

//=================================================

//*% hCascii::hCascii(aCvTascii* pActual)/*,Cvariable* pV)*/:hCVar((aCvarType*)pActual)//,pV)
 hCascii::hCascii(DevInfcHandle_t h, aCitemBase* paItemBase):hCVar(h,paItemBase)
 {
     /// there are no abstract var types... copyAscii(paItemBase) ;
    maxSize = itemSize;  // ASSUMPTION (10/31/02) the ascii max size is the var size!!!!!
 }

hCascii::hCascii(hCascii* pSrc,  hCascii* pVal, itemIdentity_t newID):
      hCVar((hCVar*)pSrc,newID)
{
    maxSize = pSrc->maxSize;
    if ( pVal )
    {
        str = pVal->str;
    }
    else
    {
        str = setDefaultValue();
    }
    wstr = str;
}

hCVar*   hCascii::getAttrPtr  (unsigned  attr, int which)
{   
    hCVar*     pVar = NULL;
    hCNumeric* pNum = NULL;
    CValueVarient vVar;
    RETURNCODE rc = SUCCESS;
    
    if ( attr >= ((unsigned)varAttrLastvarAttr) )
    {
        LOGIF(LOGP_NOT_TOK)(CERR_LOG,
                    "ERROR: ASCII Var's getAttrID with invalid attribute type (%d)\n",attr);
        return NULL;
    }// else process request
    if ( ! IsValid() )
    {
        return NULL; // when the variable goes away, so does its attributes
    }

    varAttrType_t vat = (varAttrType_t)attr;
    hCattrBase*   pAb = getaAttr(vat);
    if ( pAb == NULL ) // this attribute was not supplied
    {
        return NULL;// doesn't exist
    }

    switch(vat)
    {
    case varAttrDefaultValue:  // only one valid for ascii today
        {
            if (pPsuedoDflt !=NULL)
            {// already created
                pVar = pPsuedoDflt;
            }
            else
            {// make one and return it
                hCattrVarDefault* pvD = (hCattrVarDefault*)pAb;
                pvD->getDfltVal(vVar);
                if (vVar.vIsValid && IsValid() ) vVar.vIsValid=true; else vVar.vIsValid=false;
                pVar = pPsuedoDflt = makePsuedoVar(vVar);
                string nM; nM = getName();
                nM += "_DfltVal";
                // default must be constant: no dependency
                pVar->setName(nM);
            }
        }
        break;
        // default, return null
    }//endswitch
    return pVar;
}//endfunction

hCVar* hCascii::makePsuedoVar(CValueVarient& initValue)
{
    hCascii* pRet = NULL;
    string s;
    CValueVarient locVarient;


    if ( initValue.vIsValid )
    {
        s = (string)initValue;// does wide to narrow conversion if required
        locVarient = s;

        if (initValue.vType == CValueVarient::isString)
        {
            pRet = (hCascii*)newPsuedoVar(devHndl(), vT_Ascii, VariableSize());
        }
        else
        if (initValue.vType == CValueVarient::isWideString)
        {
            pRet = (hCascii*)newPsuedoVar(devHndl(), vT_Ascii, VariableSize());
        }
        else 
        {
            LOGIT(CERR_LOG|CLOG_LOG,"Default Value not a string.\n");// return null
        }
        if ( pRet )
        {
            pRet->setDispValue(locVarient);
            pRet->ApplyIt();
            pRet->markItemState(IDS_CACHED);// cached & have-data
            // register these for deletion
            s = "AsciiDfltValAttrPsuedoVar";
            devPtr()->registerItem(pRet->getID(), pRet, s);
            CVarList* pVL = (CVarList*)devPtr()->getListPtr(iT_Variable);
            if ( pVL )  
                pVL->push_back(pRet);
        }// else leave it null to return
    }// else invalid, NULL
    return pRet;
}

RETURNCODE  hCascii::setDefaultValue(void)
{   
    CValueVarient vv;
    string dfltStr(VariableSize(),'?');
    vv = dfltStr;
    hCVar::setDefaultValue(vv);// from hCVar
    return SUCCESS;

    /* moved to common code in hCVar...8jul05... see previous version for details
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    hCattrVarDefault*  pDVattr = (hCattrVarDefault*) getaAttr(varAttrDefaultValue);
    CValueVarient  vv;
    // *********** assume that the Xmtr .ini value will overwrite DD's Default value  *********
    
    if ( pDVattr != NULL && pDVattr->getDfltVal(vv) == SUCCESS && vv.vIsValid) 
    {// we have a default
        wstr = str =  (string)vv;
        MakeStale();//OK to use but needs an update from the device
        return SUCCESS;
    }
    // else - no DD supplied default value, use min

    // now spec'd...no longer:: wstr = str = ""; // empty string is the default 
    string dfltStr(VariableSize(),'?');
    wstr = str = dfltStr; 

    if ( policy.isReplyOnly )
    {// we are a device
        markItemState(IDS_CACHED);
    }
    return SUCCESS;
    ** end move 8jul05 ***/
}

bool hCascii::didMethodChange(void)
{
    return(wstr != cstr);
}

bool hCascii::isChanged(void)
{
#ifdef _DEBUG
    if (wstr != str)
    {
        LOGIF(LOGP_NOTIFY)(CLOG_LOG," 0x%04x Changed dis %s  dev %s  cah %s  %s\n",getID(),
            wstr.c_str(), str.c_str(), cstr.c_str(),instanceDataStStrings[getDataState()]);
        return true;
    }
    else
    {
        return false;
    }
#else
    return(wstr != str);
#endif
};

// no error return! it just does it as well as possible
void hCascii::setDispValue(CValueVarient& newValue, bool iHaveMutex)
{
    wstring s; s = (wstring)newValue;

    if ( s.size() >= maxSize )
    {
        s = s.substr(0,maxSize);
    }
    else
    if(s.size() <= 0)
    {
        s = L"";
    }
    //else - nop - use s as is

    wstr = TStr2AStr(s); 
#ifdef _DEBUG
    char fl = 0;
    if (wstr.empty())
        fl = 0;
    else
        fl = wstr[0];
#endif
    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( (str != wstr) )// stevev 02jan07 - locals must be updated reguardless
        {/* needs notification */
            if ( ApplyIt() && ! devPtr()->isInMethod() )
            {  
				notifyIchanged();
            }
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }// else leave it
}

void hCascii::setRealValue(CValueVarient& newValue, bool iHaveMutex)
{
    wstring s; s = (wstring)newValue;

    if ( s.size() >= maxSize )
    {
        s = s.substr(0,maxSize);
    }
    else
    if(s.size() <= 0)
    {
        s = L"";
    }
    //else - nop - use s as is

    str = TStr2AStr(s); 
}



RETURNCODE hCascii::setDisplayValueString(wstring& s)
{   // verify ascii
    for ( unsigned j = 0; j < s.size(); j++)
    {
        wchar_t u = s[j];
        if ( ((UINT16)u ) > 255 )
        {
            return APP_STRING_NOT_ASCII;
        }
    }
    // else - it is ok
    CValueVarient t; t = s;
    setDispValue(t);// just sets it
    return SUCCESS;
}

RETURNCODE hCascii::setDisplayValue(CValueVarient& newValue) // from UI w/ rangetest 
{
    wstring ws; ws = (wstring)newValue;
    return (setDisplayValueString(ws));
}

/**
void hCascii::setDispValueString(string& s) 
{
    if ( s.size() >= maxSize )
    {
        s = s.substr(0,maxSize);
    }
    else
    if(s.size() <= 0)
    {
        s = "";
    }
    //else - nop - use s as is
    str  = s;
    if ( IsLocal() ) 
    {// go ahead and apply it
        if (str != wstr)// stevev 02jan07 - locals must be updated reguardless  of criticality
        {// needs notification 
            hCmsgList  noteMsgs;// moved 04dec06
            //setDependentsUNK();
            //NUA_t p = IS_changed;
            if (
            ApplyIt()//;//Value = Wrt_Val;
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);// copies devVar->dispVale
        
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}
**/

int hCascii::getEditFormatInfo(int& maxChars, int& rightOdec, wstring* pScan)
{
    //LOGIT(CLOG_LOG,"ERROR: programmer..baseclass getEditFormatInfo has been called.\n");
    maxChars =  maxSize; // stevev 14feb12..was 0;
    rightOdec= -1;
    if (pScan) pScan->erase();
    int V = (int)(L's');
    return V;
}
hCpassword::hCpassword(DevInfcHandle_t h, aCitemBase* paItemBase):hCascii(h,paItemBase)
{
}
hCpassword::hCpassword(hCpassword* pSrc, hCpassword* pVal, itemIdentity_t newID):
            hCascii((hCascii*)pSrc,(hCascii*)pVal,newID)
{
}

RETURNCODE  hCpassword::setDefaultValue(void)
{   
    CValueVarient vv;
    string dfltStr(DFLT_PASSWORD);
    vv = dfltStr;
    hCVar::setDefaultValue(vv);// from hCVar
    return SUCCESS;

    /* moved to common code in hCVar...8jul05... see previous version for details
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    hCattrVarDefault*  pDVattr = (hCattrVarDefault*) getaAttr(varAttrDefaultValue);
    CValueVarient  vv;
    // ********** assume that the Xmtr .ini value will overwrite DD's Default value  *********
    
    if ( pDVattr != NULL && pDVattr->getDfltVal(vv) == SUCCESS && vv.vIsValid) 
    {// we have a default
        wstr = str =  (string)vv;
        return SUCCESS;
    }
    // else - no DD supplied default value, use min
    // Wally:  "pick one"  
    string dfltStr(DFLT_PASSWORD);
    wstr = str = dfltStr; 

    if ( policy.isReplyOnly )
    {// we are a device
        markItemState(IDS_CACHED);
    }
    return SUCCESS;
    *** end move 8jul05  ***/
}


hCpackedAscii::hCpackedAscii(DevInfcHandle_t h, aCitemBase* paItemBase):hCascii(h,paItemBase)
{
}

hCpackedAscii::hCpackedAscii(hCpackedAscii* pSrc, hCpackedAscii* pVal, itemIdentity_t newID):
               hCascii((hCascii*)pSrc,(hCascii*)pVal,newID)
{
}
        
hCBitEnum::hCBitEnum(DevInfcHandle_t h, aCitemBase* paItemBase):hCEnum(h,paItemBase)
{
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
//   copyAttrDesc(pV);    
    sprintf(fmtStr,"0x%c0%dX\0",'%',(VariableSize() * 2));// 2 nibbles per byte
     // initialize
    if ( policy.isReplyOnly )
    {// we are a device
        hCenumList tmpList(h);
        if ( SUCCESS == condEnumDescrList.resolveCond(&tmpList) && tmpList.size() > 0)
        {
            Wrt_Val = Value = tmpList[0].val; // try to use the first valid value
        }
        else
        {
            Wrt_Val = Value = 0x0;// otherwise force zero
        }
        markItemState(IDS_CACHED);
    }
    else
        // we are a host
        Wrt_Val = Value = 0xffffffff;   
}
// J.U. 17.02.11 
wstring& hCBitEnum::getValueFormatedString(bool bIsDispValue)//CAUTION
                                                        // ->see'Invensys checkin' in ddbVar.h 
{
    const size_t FCHAR_SIZE = 64;
    wchar_t fchar[FCHAR_SIZE];
     wstring fmt;
    //long localLong = Wrt_Val;// unsigned to signed conversion <no scaling>

    unsigned localLong = 0;
    if (bIsDispValue)
    {
        localLong = (UINT32)Wrt_Val;// stevev 27aug10 <bit enum limited to 32 bits>
    }
    else
    {
        localLong = (UINT32)Value;// stevev 27aug10 <bit enum limited to 32 bits>

    }
    int  s = 1;
    if (VariableSize() > 0 && VariableSize() < 5 )
    {
        s = VariableSize();
    }
    switch (s)
    {
    case 2:
        fmt = L"0x%04x";  break;
    case 3:
        fmt = L"0x%06x";  break;
    case 4:
        fmt = L"0x%08x";  break;
    case 1:
    default:
        fmt = L"0x%02x";  break;
    }
	swprintf(fchar,
#if defined(__GNUC__)
			FCHAR_SIZE,
#endif // __GNUC__
	fmt.c_str(),localLong);
    returnString = fchar;
    return  returnString; 
};

wstring& hCBitEnum::getDispValueString(void) 
{
    return getValueFormatedString(true);    // J.U. 17.02.11
};

/*
// note that the device map is required to process ItemRef to ITEMID
RETURNCODE 
hCBitEnum::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
    RETURNCODE rc = FAILURE;
    hCenumList returnList(devHndl());
    hCgroupItemDescriptor* pGid = NULL;

    hCenumDesc* ped;
    UINT32 msk = 0;
    bool r = true;

  
    if ( ppGID == NULL || indexValue == 0 )
    {   
        return APP_PARAMETER_ERR;      
    }
    else
    {   pGid   = new hCgroupItemDescriptor(devHndl());
        if ( *ppGID != NULL )   delete *ppGID; // get rid of passed in memory
        *ppGID = pGid;                         // use our memory
        rc = FAILURE; // set it to delete pGid if no attribute found
    }


    if ( procureList(returnList) == SUCCESS && returnList.size() > 0 )
    {
        if ()
        {
            ped = returnList.matchValue(indexValue);   // NULL on failure
            r   = ( ped != NULL ); // true on found
        }
        else
        {// we'll match the first non-zero bit from the right
            while (indexValue != 0 && r )// any bit mismatch is a failure
            {
                msk = 1 << right0bits(indexValue);

                ped = returnList.matchValue( msk );   // NULL on failure
                r = ( ped != NULL ); // true on found
                indexValue &= ~msk;
            }
        }
    }
    else
    {   r = false;// no valid values to test against...all values are invalid
    }
    returnList.destroy();// will be deleted on exit
    if (rc != SUCCESS || ! r )
    {
        if(pGid)    
        {
            RAZE(pGid);     
        }
        *ppGID = NULL;
    }
    return rc;
}
*/

 hChartDate::hChartDate(DevInfcHandle_t h, aCitemBase* paItemBase)
        /* init base class */
        :hCinteger(h,paItemBase,false)
 {
     editFormat = usFormat; // default to english format
/* VMKP:  060404 (Always initialize Day and Month as 1st.  Zero is invalid for day and month
        Year zero in the sense 1900,  An initialization error occurs  in MFC date and time 
        control if day or month or year is zero (A validatio issue) */

     Value = 0x010100; 
     Wrt_Val = 0x010100;
/* VMKP:  060404 */
 }

hChartDate::hChartDate(hChartDate* pSrc, hChartDate* pVal, itemIdentity_t newID):
         hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID)
{
    editFormat = usFormat; // default to english format
}


RETURNCODE  hChartDate::setDefaultValue(void)
{
    CValueVarient vv;
    vv = (UINT32) 0x010100; // new default value from post ballot spec
                            // was::> (UINT32) 0x010150;  // HCF spec'd default value
    hCVar::setDefaultValue(vv);// from hCVar
    return SUCCESS;

    /* moved to common code in hCVar...8jul05... see previous version for details
    RETURNCODE rc = SUCCESS;

    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    CValueVarient l;

    if ( hCinteger::setDefaultValue() == SUCCESS )  // we have a DD dfltVal
    {// verify that its a valid index
        if (! ValidateDate(Wrt_Val) )
        {// we use what we have but tell 'em about the problem
            LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Date Var 0x%04x (%s) has a Default Value"
                " Attribute (0x%x) that is not a valid value.\n",getID(),
                getName().c_str,(UINT32)Wrt_Val);
        }
        rc = SUCCESS;
    }
    else // no DD default value
    if ( policy.isReplyOnly )
    {// we are a device
        Value = Wrt_Val = 0x010150;  // HCF spec'd default values...no longer:: 0x010100; 
        markItemState(IDS_CACHED);
        rc = SUCCESS;
    }
    else
    {
        Value = Wrt_Val = 0x010150;
        rc = SUCCESS;
    }
    ApplyIt();
    return rc;
    *** end move 8jul05 ***/
}

bool  hChartDate::isInRange(CValueVarient val2Check)
{
	UINT32 LocVal;
	CValueVarient vv;
	hSdispatchPolicy_t policy = devPtr()->getPolicy();
	if (policy.isReplyOnly)
	{
		return true;
	}
	if ( val2Check.vType == CValueVarient::isString)
	{
		LocVal = str2int((wstring)val2Check);
	}
	else
	if ( val2Check.vType == CValueVarient::isWideString)
	{
		LocVal = str2int(val2Check);
	}
	else // few others are legal but it just the same
	{
                // To validate the parameter whether its in range or not
                return (val2Check.vType == 0) ? true : false;
	}
	return (ValidateDate(LocVal));
}

int hChartDate::getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
                                    wstring* pEditFmt, wstring* pScanFmt, wstring* pDispFmt)
{
    return  -1;//failure
}

bool  hChartDate::VerifyIt(wstring & s)//J.U. 17.03.11< <caution< < <See note @ bottom ddbVar.h
{
    bool bRes = true;
    CValueVarient var;
    try
    {
        unsigned int t = str2int(s);
        var = t; 
    }
    catch(...)
    {
        LOGIT(CERR_LOG,"Inproper edit format.\n");
        bRes = false;
    }
    if (bRes == true && !isInRange(var))
    {
        bRes = false;
    }
    return bRes;
}


bool hCenumList::operator== (/*const*/ hCenumList& src)
{
    if (size() != src.size())
    {
        return false;
    }
     // compare 'em
    FOR_this_iT(enumDescList_t,this)
    {// iT is ptr 2a hCenumDesc
        if ( src.matchValue(iT->val) == NULL ) // not found
            return false;
        // else - check the next one
    }
    return true;
}

 // base class function: copyDesc
 //      copy the main data (descriptors) from dllapi to ddbVar
 //      memory allocated here...hCEnum controls the memory
//*% void hCEnum::copyDesc(aCvarEnumType* pEn)
 void hCEnum::copyDesc(aCitemBase* paItemBase)
 {
     RETURNCODE rc = SUCCESS;
    //aCcondList<aCenumList>       condEnumDescriptor;
    //           aCenumList  which is a list of :aCenumDesc:
    //for each list in the conditional list
    //      for each item in the list
    //          copy
/*
    condEnumDescrList.erase(); // get rid of any existing
    if ( (rc = pEn->condEnumDescriptor.fetchList(listOlists)) == SUCCESS)
    {
        for(vector<hCenumList*>::iterator it=listOlists.begin();
        /-*each list*-/                             it < listOlists.end(); it++ )
        {
            rc = (*it)->fetchList(enumPtrList);
            if ( rc != SUCCESS )
            {
                cerr xx "WARN: fetch the enum list constants failed." xxendl;
            }
        }
    }


     pET->aquireDescList(enums); // no need to check return values, we're in the constructor
*/
    aCattrBase* paB = paItemBase->getaAttr((unsigned int)varAttrEnums);
    if (paB == NULL)
    {
        LOGIT(CERR_LOG,L"ERROR:  Enum attribute not found for an enum type.\n");
        return;
    }// else - continue
#ifdef _DEBUG
    if ( ((aCattrEnum*)paB)->condEnumListOlists.genericlist.size() == 0 )
    {
        LOGIT(CERR_LOG,L"Enum Item 0x%04x has no values.\n",itemId);
    }
#endif
//#ifdef ENUM_ALLOC_DEBUG
#ifdef _DEBUG
    unsigned enumID = getID();
#endif
#ifndef TOKENIZER
	if ( getID() == RESPONSECODE_SYMID )
	{// strip the supeflous garbage
		// stevev - 12may16 - put it back for now...for xmtr-dd
    condEnumDescrList.setEqualTo(&( ((aCattrEnum*)paB)->condEnumListOlists)); // list = Alist
	}
	else
#endif
	{
    condEnumDescrList.setEqualTo(&( ((aCattrEnum*)paB)->condEnumListOlists)); // list = Alist
//DEBUGLOG(CLOG_LOG,"### Enum id 0x%04x used %8d of memory.\n",enumID,
//		 ms[15].lTotalCount-ms[14].lTotalCount);
	}
     return;    
 };

 /*
 void hCEnum::copyAttrDesc(Cvariable* pV)
 {
     //CvarEnumType* pET = (CvarEnumType*)pEn;

     //pET->aquireDescList(enums); // no need to check return values, we're in the constructor
     //return;  

     hCattrEnum*     pEnum = NULL;
    if( pV->getAttr(&pEnum) != SUCCESS || pEnum == NULL)
    {
        cerr xx "ERROR: Enumeration without enum attribute."xxendl;
    }
    else //if ( iType == ENUMERATED )
    {
        pEnum->aquireDescList(enums);
    }
    //else if ( iType == BIT_ENUMERATED )
    //{
    //  *((CBitEnum*)pR) = *pEnum;
    //}
    //else
    //{
    //  cerr xx "ERROR: programmer error in enum new."xxendl;
    //}
 };

void hCBitEnum::copyAttrDesc(hCVar* pV)
 {
    hCattrEnum*     pEnum = NULL;
    if( pV->getAttr(&pEnum) != SUCCESS || pEnum == NULL)
    {
        cerr < < "ERROR: Bit Enumeration without enum attribute."< <endl;
    }
    else 
    {
        pEnum->aquireDescList(enums);
    }
 };
 */

RETURNCODE hCEnum::dumpDesc(void)
{
    return condEnumDescrList.dumpSelf();// since it's conditional, must dump as such
}

// evaluates and then a value for a given enum string
RETURNCODE hCEnum::procureValue(const wstring& keyStr, UINT32& val)
{
    RETURNCODE rc = FAILURE;
    val = 0xffffffff;

    // procure a list
    if ( SUCCESS == procureList(Resolved) && Resolved.size() > 0 )
    {//  get the match
        hCenumDesc* pEDesc = Resolved.matchString(keyStr);
        if (pEDesc != NULL)
        {// a match
            val = pEDesc->val;
            rc = SUCCESS;
        }
        else
        {// (no match) continue
            //rc = FAILURE;
        }   
    }
    else
    {// conditional could NOT be resolved
        //rc = FAILURE;
    }
    return rc;
}
// stevev - WIDE2NARROW char interface
RETURNCODE hCEnum::procureValue (const string& keyStr, UINT32& value)
{
    wstring locStr; locStr = AStr2TStr(keyStr);
    return ( procureValue(locStr, value) );
}

// evaluates and then retrieves string for value
RETURNCODE hCEnum::procureString( const UINT32 val, wstring& mtchStr)
{
    RETURNCODE rc = FAILURE;
    mtchStr.erase();// returns empty on failure

	// procure a list
	if ( SUCCESS == procureList(Resolved) && Resolved.size() > 0 )
	{//  get the match		
		const size_t FMT_SIZE = 64;
		wchar_t fmt[FMT_SIZE];
		hCenumDesc* pEDesc = Resolved.matchValue(val);
		if (pEDesc != NULL)
		{// a match
			mtchStr = pEDesc->descS.procureVal(/*getDeviceAPI()*/);
			rc = SUCCESS;
		}
		else if (val == 0)
		{
			swprintf(fmt,
#if defined(__GNUC__)
					FMT_SIZE,
#endif // __GNUC__
					L"0x%02 ZERO INVALID",Wrt_Val);// unscaled
			mtchStr = fmt; // leave failure
		}
		else
		{// (no match) continue	
			//swprintf(fmt,L"0x%02X Undefined in 0x%02X",val, Wrt_Val);// unscaled
			swprintf(fmt,
#if defined(__GNUC__)
					FMT_SIZE,
#endif // __GNUC__
					L"0x%02X Undefined",val);// unscaled
			mtchStr = fmt; // leave failure
		}
	}
	//else leave it FAILURE
	return rc;
}
// stevev - WIDE2NARROW char interface \/ \/ \/
RETURNCODE hCEnum::procureString(const UINT32 value, string& mtchStr) 
{
    wstring locStr;
    RETURNCODE rc = procureString(value,locStr);
    mtchStr = TStr2AStr(locStr);
    return rc;
}
/* stevev 3/19/04 - added to support enums as indexes in E&H DDs */
int hCEnum::getAllValidValues(UIntList_t& returnList)// returns count in list
{
    RETURNCODE rc = SUCCESS;
    int cnt = 0;
    vector<hCenumList*>  ListOPtr2enumList;// vector of reference ptrs
    vector<hCenumList*>  ::iterator      iTenumListOfPtr2List;// vector of reference ptrs
/*===================*/ 
FOR_iT(hCcondList<hCenumList>::condListOlists_t, condEnumDescrList.genericlist)
{// iT is a ptr to a oneGeneric_t  hCgenericConditional< hCcondList<hCenumList>,hCenumList >
    iT->aquirePayloadPtrList(ListOPtr2enumList);
}
/*===================*/

//  rc = condEnumDescrList.aquirePayloadPtrList(ListOPtr2enumList);// all possible lists
    if (rc == SUCCESS && ListOPtr2enumList.size() > 0)
    {// we have at least one list of enum [vector<hCenumDesc>]
        for (iTenumListOfPtr2List = ListOPtr2enumList.begin();
             iTenumListOfPtr2List < ListOPtr2enumList.end();    iTenumListOfPtr2List++)
        {// it is a Ptr 2a Ptr 2a hCenumList
            for (vector<hCenumDesc>::iterator iTED = (*iTenumListOfPtr2List)->begin();
            iTED < (*iTenumListOfPtr2List)->end(); iTED++)
            {// isa ptr 2a hCenumDesc
                returnList.push_back(iTED->val);
                cnt++;
            }
        }
    }
    // else just return zero
    return cnt;
}

/* This invensys function is used for offline data handling.
   The user should normally never see a NON-display value.
   CAUTION-see 'Invensys checkin' in ddbVar.h                */
wstring& hCEnum::getValueFormatedString(bool bIsDispValue)  // J.U. 29.06.2011
{
	try
	{
		const size_t FCHAR_SIZE = 351;
		wchar_t fchar[FCHAR_SIZE];
		
		if (bIsDispValue)
		{
#ifdef _INVENSYS
			swprintf(fchar, L"%d", (UINT64)Wrt_Val);
			returnString = fchar;	
#else
			procureString(((UINT32)Wrt_Val),returnString); 
#endif			
		}
		else
		{	
			swprintf(fchar,
#if defined(__GNUC__)
					FCHAR_SIZE,
#endif // __GNUC__
					L"%d", (UINT64)Value);
			returnString = fchar;			
		}
	}
	catch(...)
	{		
		LOGIT(CLOG_LOG, "hCEnum::getValueFormatedString, id = %04x\n", this->itemId);
		returnString.clear();
	}
	
	return  returnString; 
}
/* end 3/19/04 method */






DATA_QUALITY_T hCEnum::Read(wstring &value) // returns the current value desc string
{
    /* VMKP changed on 190104 */
    UINT64 val;
    DATA_QUALITY_T rq = hCinteger::Read(val);
    /* VMKP changed on 190104 */

    //procureString(Value, value);
    procureString((UINT32)Wrt_Val, value);// enum limited to 32 bits

    return rq;
};

RETURNCODE hCEnum::Write(wstring value)
{
    RETURNCODE rc = FAILURE;
    UINT32 tmpVal;
    rc = procureValue (value, tmpVal);
    if ( rc == SUCCESS )
    {
        rc = hCinteger::Write(tmpVal);
    }
    return rc;
}

//RETURNCODE hCEnum ::Write(UINT32 value) // use default hCVar version

//RETURNCODE hCindex::Read(UINT32 &value)// use hCVar's default routine

//RETURNCODE hCindex::Write(UINT32 value) // use default hCVar version

wstring& hCindex::getDispValueString(void)
{
    RETURNCODE rc = SUCCESS;
    bool  r = false;
    hCitemArray* pIA = NULL;
    hCitemBase*  pIB = NULL;
    hCgroupItemDescriptor* pGID = NULL;

    if (pIndexed!=NULL) // else  nothing to compare to, all values are invalid
    {
#ifdef USE_INDEX_MUTEX
  #ifdef _DEBUG
        ((hCddbDevice*)devPtr())->aquireIndexMutex(10000,__FILE__,__LINE__);
  #else
        ((hCddbDevice*)devPtr())->aquireIndexMutex(10000);
  #endif
        UINT32 valCheck = (UINT32)Wrt_Val;// array sizes are limited to unsigned int
        ((hCddbDevice*)devPtr())->returnIndexMutex();
#else
        UINT32 valCheck = (UINT32)Wrt_Val;// array sizes are limited to unsigned int
#endif
        
        if ( pIndexed->getArrayPointer(pIB) == SUCCESS && pIB != NULL )
        {
            if (pIB->getIType() == iT_ItemArray)// stevev 29jul08 - only indexee w/ description
            {
                pIA = (hCitemArray*)pIB;
                rc = pIA->getByIndex(valCheck, &pGID);
                if ( rc == SUCCESS && pGID != NULL)
                {
                    //resolved = (pGID->getDesc().procureVal());
                    resolved = (pGID->getDesc());
                    RAZE(pGID);
                    if ( !resolved.empty() )
                    {
                        return resolved;
                    }// else fall thru to get int
                }// else fall thru
            }// else fall thru
        }// else fall thru
    }
    RAZE(pGID);
    return (hCinteger::getDispValueString());
}

wstring&  hCindex::getEditValueString(void)
{		
	if (devPtr()->getPolicy().isReplyOnly)
	{
		const size_t WCHAR_SIZE = 68;
		wchar_t wchar[WCHAR_SIZE];
//#ifndef_WIN32_WCE
#if !(defined(_WIN32_WCE) || defined(__GNUC__))
		_i64tow( Wrt_Val, wchar, 10 );
#else
		swprintf( wchar,
#if defined(__GNUC__)
				WCHAR_SIZE,
#endif // __GNUC__
				L"%i[I64]", Wrt_Val, wchar ); // see above PAW 27/04/09
#endif
		returnString = wchar;
	}
	else
	{
		returnString = getDispValueString();
	}
	return returnString;
}

bool hCindex::isInRange(CValueVarient val2Check)
{
    RETURNCODE rc = SUCCESS;
    bool  r = false;
    hCitemArray* pIA = NULL;
    hCitemBase*  pIB = NULL;
    hCgroupItemDescriptor* pGID = NULL;

    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }

    if (pIndexed!=NULL) // else  nothing to compare to, all values are invalid
    {
        UINT32 valCheck = val2Check;
        
        if ( pIndexed->getArrayPointer(pIB) == SUCCESS && pIB != NULL )
        {
            pIA= (hCitemArray*) pIB;
            rc = pIA->getByIndex(valCheck, &pGID);
            if ( rc == SUCCESS && pGID != NULL)
            {
                r = true;
            }// else leave it false

        }// else leave it false
    }
    RAZE(pGID);
    return r;
}



BYTE hCascii::MaxSize(void)   {return maxSize;};


DATA_QUALITY_T hCascii::Read(wstring &value)  
{
    CValueVarient myVar;
    DATA_QUALITY_T da = hCVar::Read(myVar);

    value = (wstring)myVar;

    return da;
}


// from the raw device write
RETURNCODE hCascii::Write(char* pVal) 
{       
    RETURNCODE rc = FAILURE;    

    if (! IsReadOnly() && pVal != NULL )  
    {   
        //str = pVal;
        //wstr= str;
        wstr = pVal;
        hCVar::Write();// get it passed through
        rc = SUCCESS;
    }
    else 
    { 
        rc = (-1 /*READ_ONLY*/); 
    }
    return rc;
}
RETURNCODE hCascii::Write(CValueVarient& newDataValue)
{
    //CW fix 22aug11
    // added "isWideString" case
    if ( newDataValue.vType == CValueVarient::isString ||
         newDataValue.vType == CValueVarient::isWideString )
    {
        return (Write((wstring)newDataValue) );// will convert to wide
    }
    else
    {
        return FAILURE;
    }
}

RETURNCODE hCascii::Write(wstring value) 
{ 
    RETURNCODE rc = FAILURE;
    //CValueVarient localVar;
    if (! IsReadOnly() )  
    {
        if ( value.size() < maxSize)
        {
            wstr = TStr2AStr(value);
        }
        else
        {
            wstr = TStr2AStr( value.substr(0,maxSize) );
        }
        rc = hCVar::Write();    // base class 'Write fastest way'
    }
    else 
    {
        rc = DATA_QUALITY_NOT_WRITABLE; 
    }

    return rc;
}

RETURNCODE hCascii::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
                            methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                            varPtrList_t &hasChangedList, extract_t extFlags)
{
    char buffer[256],
            *pBuf = buffer;
    string tS;
    bool isDifferent;
    /* stevev 3/22/04 - added to handle Error Packets */
    if ( (*byteCount) < 1 )
    {
        LOGIT(CLOG_LOG,"Message too short for Ascii variable.\n");
    }/* end add */

    memset (buffer, 0x00, 256);     //ascii types are filled with nulls
    memcpy(buffer, *data, VariableSize());
    *byteCount -= VariableSize();
    *data += VariableSize();
    tS = buffer;

    // stevev 2/25/04       
    if ((extFlags & DoNotApply) == DoNotApply)
    {// that's all there is to do
        return SUCCESS;
    }

    bool isInfo = ((extFlags & DoInfoValue) == DoInfoValue);

    devPtr()->aquireItemMutex(
#ifdef _DEBUG
                            __FILE__,__LINE__);
#else
                              );
#endif
    isDifferent = (tS != str);
    if (previousDataState == IDS_INVALID || previousDataState == IDS_UNINITIALIZED)
    {
        isDifferent = true;
    }
    // stevev 18jan05 - moved outside of wrtstaus (always set it)
/* VMKP Modified on 310104 */
//  str = tS;
    wstring st = AStr2TStr(tS);
    setRealValueString(st);

    // stevev 15dec05 - moved from below to be symetrical with other changes
    markItemState( IDS_CACHED );

/* VMKP Modified on 310104 */
    /* stevev 3/2/4 - preclude applying user written variables */
    // pre 28nov05::
    //if (getWriteStatus() == 0 ||(isDifferent && devPtr()->isItemCritical(getID())))
    //post 28nov05 - never execute if no change, 
    if (!isInfo && isDifferent && (getWriteStatus() == 0 || devPtr()->isItemCritical(getID())))
    {// go ahead and apply it
        /* pre 28nov05 --------------------------------------------
        {// go ahead and apply it
            //  setDependentsUNK();
            //NUA_t p = NO_change; if (isDifferent) p = IS_changed;
            //devPtr()->notifyVarUpdate(getID(),p);
        // filter unchanged, get all that this affects...

        notify(noteMsgs);

        ---post 28nov05 --------------------------------------------***/
        if ( getWriteStatus() == 0 )
        {// notify self if not user written
            noteMsgs.insertUnique(getID(), mt_Val, 0);
        }
        // get all that this affects...
//9-9-9 lets do this later....notify(noteMsgs);// fills list from dependents (if there are any)
        hasChangedList.push_back(this);// stevev 9-9-9
        /*-- end 28nov05 ---------------------------------------------*/
    }/* end stevev 3/2/4 */
    if (!isInfo && isDifferent && dominantRelations.size() > 0
        /* stevev 22feb06 */&& devPtr()->getDeviceState() == ds_OnLine)
    {
        MarkDependStale();// Mark the dependent stale
    }
    if (devPtr()->getPolicy().isReplyOnly)
    {
#ifdef XMTR
        if ( isDifferent && IsConfig() )
        {       
            RETURNCODE      rc;
            hCitemBase*     pItem = NULL;
            hCVar*          pVar  = NULL;
            CValueVarient   vv;
            unsigned char   iv;
            if (( rc = devPtr()->getItemBySymNumber(DEVICESTATUS_SYMID,&pItem))== SUCCESS
             && (pItem != NULL) )
            {
                pVar = (hCVar*) pItem;
                vv = pVar->getDispValue();
                iv = vv;
                iv |= CONFIG_CHANGED_BIT ;          
                vv = iv;
                pVar->setDispValue(vv);
            }
        }
#endif
        // note that the value must be copied when it's xmtr
        CancelIt();
    }
    // moved earlier - stevev 15dec05  markItemState( IDS_CACHED );
    /* * this should be the ONLY place a non-local's previous should be cleared */
    previousDataState = IDS_CACHED;// stevev 05jul05 - if its a local we'll never be here

    LOGIF(LOGP_COMM_PKTS)
                    (CLOG_LOG,"    Ext: 0x%04x marked as cached. (%s)\n",getID(),str.c_str());

    devPtr()->returnItemMutex();

    return SUCCESS;
};  
RETURNCODE hCascii::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{ 
    char buffer[256],
            *pBuf = buffer;

    memset (buffer, 0x00, 256);     //ascii types are filled with nulls
    
    for (string::iterator pStr = wstr.begin(); pStr < wstr.end()  ;  pStr++, pBuf++) {
        *pBuf = *pStr;
    }

    memcpy(*data, buffer, VariableSize());
    *byteCount += VariableSize();
    *data += VariableSize();

    return SUCCESS;
}; 


/*************************************************************************
 * pack/unpack
 *
 * these two utilities support the HART data type "Packed ASCII"  this
 * data type stuffs 4 characters into three bytes
 *
 * from the HART Command Summary Specifications
 *
 * Some of the alpha-numeric data passed by the protocol is transmitted
 * to and from the devices in the Packed-ASCII format. Packed-ASCII is a
 * subset of ASCII produced by removing the two most significant bits of
 * each ASCII character. This allows four Packed-ASCII characters to be
 * placed in the space of three ASCII characters. Typically, Packed-ASCII
 * strings are defined in even multiples of three bytes, four ASCII characters.
 *
 * Packed-ASCII Data Byte  | #0        | #1        | #2        | ...
 * ASCII Data Byte         | #0     #1 |      #2   | #2 #3     | ...
 * ASCII Data Bit          | 543210 54 | 3210 5432 | 10 543210 | ...
 *
 * Construction of Packed-ASCII characters:
 *      a) Truncate Bit #6 and #7 of each ASCII character.
 *      b) Pack four, 6 bit-ASCII characters into three bytes.
 *
 * Reconstruction of ASCII characters:
 *      a) Unpack the four, 6-bit ASCII characters.
 *      b) Place the complement of Bit #5 of each unpacked, 6-bit ASCII
 *         character into Bit #6.
 *      c) Set Bit #7 of each of the unpacked ASCII characters to zero.

*/

/*************************************************************************
 * pack
 *
 * stuffs 4 ASCII (ISO-Latin 1) characters into three bytes
 *
 *************************************************************************
 */
BYTE * hCpackedAscii::Pack( BYTE *dest, BYTE *source, int nChar ) {

    BYTE *s = source,
        *d = dest,
        i;

    unsigned long p;
    for ( ; nChar > 0 ; nChar -= 4) {
        // fill p with the 4 packed ascii char (24bits into p)
        for (p = i = 0; i < 4 ; i++) {
            p <<= 6;
            p |= toupper(*s) & 0x3f;
            s++;
        }
        // put the three bytes into the packed ascii destination
        d += 2;
        for (i = 0; i < 3 ; i++) {
            *d-- = (BYTE)(p & 0xff);
            p >>= 8;
        }
        d += 4;
    }
    return dest;
}

/*************************************************************************
 * unPack
 *
 * unstuffs HART packed ASCII
 *
 *************************************************************************
 */
BYTE * hCpackedAscii::UnPack( BYTE *dest, BYTE *source, int nChar ) {

    BYTE *s = source,
        *d = dest, // - 5,
        oneChar,        // where the raw byte is placed to flip the bits
        i, z;
    BYTE byFirstTime = 0;

    unsigned long p;

    for ( ; nChar > 0 ; nChar -= 4) {
        // fill p with the 4 packed ascii char 
        // (24bits into p from s)
        for (p = i = 0; i < 3 ; i++) {
            p <<= 8;
            p |= *s++;
        }

        // unpack p
        /* VMKP Modified on 090304 */
        if(!byFirstTime)
        {d += 4; byFirstTime = 1;}
        else
        {d += 8;}

        for (i = 0; i < 4 ; i++) {
            oneChar = p & 0x3F;     // get the LS 6 bits
            p >>= 6;
            if (oneChar & 0x20) z = oneChar; else z = (oneChar | 0x40);
            *(--d) = z;
        }
    }
    return dest;
}


RETURNCODE hCpackedAscii::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, 
                int& len,methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                varPtrList_t &hasChangedList, extract_t extFlags)
{
    char buffer[256],
        packedBuffer[256],
        *pBuf = buffer,
        packedBufSize = (VariableSize()/4)*3;//(4 * VariableSize())/3;
    string tS;
    bool isDifferent;
    /* stevev 3/22/04 - added to handle Error Packets */
    if ( (*byteCount) < 3 )
    {
        LOGIT(CLOG_LOG,"Message too short for Packed variable.\n");
    }/* end add */

    memcpy(packedBuffer, *data, packedBufSize);
    *byteCount -= packedBufSize;
    *data += packedBufSize;
    
    memset (buffer, 0x00, 256);     //packed ascii strings a filled with space characters
    UnPack((BYTE*)buffer, (BYTE*)packedBuffer, VariableSize());
    tS = buffer;
    // stevev 2/25/04       
    if ((extFlags & DoNotApply) == DoNotApply)
    //if (doNotApply || getDataState() != IDS_PENDING)//  stevev 2/25/04
    {
        return SUCCESS;
    }

    bool isInfo = ((extFlags & DoInfoValue) == DoInfoValue);

    devPtr()->aquireItemMutex(
#ifdef _DEBUG
                            __FILE__,__LINE__);
#else
                              );
#endif/**************/
    isDifferent = (tS != str);
    if (previousDataState == IDS_INVALID || previousDataState == IDS_UNINITIALIZED)
    {
        isDifferent = true;
    }
    // stevev 18jan05 - moved outside of wrtstatus (always set it)
/* VMKP Modified on 310104 */
//  str = tS;
    wstring st = AStr2TStr(tS);
    setRealValueString(st);

    // stevev 15dec05 - to be symeterical with others 
    markItemState( IDS_CACHED );

/* VMKP Modified on 310104 */
    /* stevev 3/2/4 - preclude applying user written variables */
    // pre 28nov05:
    // if (getWriteStatus() == 0 ||(isDifferent && devPtr()->isItemCritical(getID())))
    //post 28nov05 - never execute if no change, 
    if (!isInfo && isDifferent && (getWriteStatus() == 0 || devPtr()->isItemCritical(getID())))
    {// go ahead and apply it
        /* pre 28nov05 --------------------------------------------
            // stevev -  18jan05 - notify if critical & different -
        {// go ahead and apply it
                //  setDependentsUNK();
                //  NUA_t p = NO_change; if (isDifferent) p = IS_changed;
                //  devPtr()->notifyVarUpdate(getID(),p);
            // filter unchanged, get all that this affects...

            notify(noteMsgs);
        ---post 28nov05 --------------------------------------------***/
        if ( getWriteStatus() == 0 )
        {// notify self if not user written
            noteMsgs.insertUnique(getID(), mt_Val, 0);
        }
        // get all that this affects...
        hasChangedList.push_back(this);// stevev 9-9-9
        /*-- end 28nov05 ---------------------------------------------*/
    }/* end stevev 3/2/4 */
    if (!isInfo && isDifferent && dominantRelations.size() > 0
        /* stevev 22feb06 */&& devPtr()->getDeviceState() == ds_OnLine)
    {
        MarkDependStale();// Mark the dependent stale
    }
    if (devPtr()->getPolicy().isReplyOnly)
    {
#ifdef XMTR
        if ( isDifferent && IsConfig() )
        {       
            RETURNCODE      rc;
            hCitemBase*     pItem = NULL;
            hCVar*          pVar  = NULL;
            CValueVarient   vv;
            unsigned char   iv;
            if (( rc = devPtr()->getItemBySymNumber(DEVICESTATUS_SYMID,&pItem))== SUCCESS
             && (pItem != NULL) )
            {
                pVar = (hCVar*) pItem;
                vv = pVar->getDispValue();
                iv = vv;
                iv |= CONFIG_CHANGED_BIT ;          
                vv = iv;
                pVar->setDispValue(vv);
            }
        }
#endif
        // note that the value must be copied when it's xmtr
        CancelIt();
    }
    // moved earlier - stevev 15dec05  markItemState( IDS_CACHED );
    /* * this should be the ONLY place a non-local's previous should be cleared */
    previousDataState = IDS_CACHED;// stevev 05jul05 - if its a local we'll never be here

    LOGIF( LOGP_COMM_PKTS )
                    (CLOG_LOG,"    Ext: 0x%04x marked as cached. (%s)ext\n",getID(),str.c_str());

    devPtr()->returnItemMutex();/**************/
    return SUCCESS;
};      
RETURNCODE hCpackedAscii::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{ 
    char buffer[256],
        packedBuffer[256],
        *pBuf = buffer,
        packedBufSize = (VariableSize()/4)*3;//(4 * VariableSize())/3;

    memset (buffer, ' ', 256);      //packed ascii strings a filled with space characters
    

    unsigned short wstrsize = 0;
    for (string::iterator pStr = wstr.begin(); pStr < wstr.end()  ;  pStr++, pBuf++) {
        *pBuf = *pStr;

        /* Some reason the wstr.size() is gettting greater than 256, must be checked  (VMKP)*/
        wstrsize++;
        if(wstrsize > 255)
            break;
    }

    Pack((BYTE*)packedBuffer, (BYTE*)buffer, VariableSize());
    memcpy(*data, packedBuffer, packedBufSize);
    *byteCount += packedBufSize;
    *data += packedBufSize;

    return SUCCESS;
}; 
RETURNCODE hCpackedAscii::setDisplayValueString(wstring& s)
{	
	transform(s.begin(),s.end(),s.begin(),
#if defined(__GNUC__)
			::towupper
#else
			 toupper
#endif // __GNUC__
			);
	return hCascii::setDisplayValueString(s);
}


/*   ALWAYS returns a value --- check the quality!!!!!!!!!!!!!!!!!!!!!! */
DATA_QUALITY_T hChartDate::Read(wstring &value)  
{	
	const size_t VAL_SIZE = 256;
	wchar_t val[VAL_SIZE];
	UINT64 LocVal;
	DATA_QUALITY_T rq = hCinteger::Read(LocVal);
/*<START>06APR2004 Modified by ANOOP to support 4 digit year format	*/	
	UINT8 d,m;
	UINT16 y;
	y = LocVal         & 0xFF;
	m = (LocVal >>  8) & 0xFF;
	d = (LocVal >> 16) & 0xFF;

	y = y + 1900;
//	y = LocVal % 100;	Commented by ANOOP 05APR2004

	switch (editFormat)
	{
	case invsFormat:
		{
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d/%02d/%04d", d,m, y);// dd/mm/yy
		}
		break;
	case dotFormat:
		{
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d.%02d.%04d", d,m, y);// dd.mm.yy
		}
		break;
	default:
	case usFormat:
		{
//			sprintf (val,"%02d/%02d/%02d", m,d, y);// mm/dd/yy Commented by ANOOP 05APR2004
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d/%02d/%04d", m,d, y);// mm/dd/yyyy
		}
		break;
	//	{
	//		sprintf (val,"0x%06H",Value); // no format
	//	}
	}// endswitch

	value = val;
	
	return rq;
};
// stevev - WIDE2NARROW char interface
DATA_QUALITY_T hChartDate::Read(string &value)  
{   
    char val[256];
    UINT64 LocVal;
    DATA_QUALITY_T rq = hCinteger::Read(LocVal);
/*<START>06APR2004 Modified by ANOOP to support 4 digit year format */  
    UINT8 d,m;
    UINT16 y;
    y = LocVal         & 0xFF;
    m = (LocVal >>  8) & 0xFF;
    d = (LocVal >> 16) & 0xFF;

    y = y + 1900;
//  y = LocVal % 100;   Commented by ANOOP 05APR2004

    switch (editFormat)
    {
    case invsFormat:
        {
            sprintf (val,"%02d/%02d/%04d", d,m, y);// dd/mm/yy
        }
        break;
    case dotFormat:
        {
            sprintf (val,"%02d.%02d.%04d", d,m, y);// dd.mm.yy
        }
        break;
    default:
    case usFormat:
        {
//          sprintf (val,"%02d/%02d/%02d", m,d, y);// mm/dd/yy Commented by ANOOP 05APR2004
            sprintf (val,"%02d/%02d/%04d", m,d, y);// mm/dd/yyyy
        }
        break;
    //  {
    //      sprintf (val,"0x%06H",Value); // no format
    //  }
    }// endswitch

    value = val;
    
    return rq;
};

bool hChartDate::ValidateDate(const UINT32 LocVal ) // true at valid
{
    bool            bRetVal;
    UINT32          nLocalVal;

    bRetVal=false;
//  DATA_QUALITY_T  rq = hCinteger::Read(LocVal);
    
    UINT16 d,m,y;
    y = LocVal         & 0xFF;
    m = (LocVal >>  8) & 0xFF;
    d = (LocVal >> 16) & 0xFF;

    nLocalVal = y + 1900;
    y = nLocalVal % 100;

    //  valid number has nothing to do with the formatting
    if ( (m <= 12 && m >= 1 ))
    {
        if( (d <=31 && d >=1 ))
        {
            bRetVal=true;   
        }
    }

    return bRetVal;

}

//RETURNCODE hChartDate::Write(UINT32 value) // use default hCVar version
void hChartDate::setDispValue(CValueVarient& newValue, bool iHaveMutex)///ddpSTRING
{
    UINT32 LocVal;
    CValueVarient vv;
    if ( newValue.vType == CValueVarient::isWideString)
    {
        LocVal = str2int((wstring)newValue);
    }
    else // few others are legal but it just the same
    {
        LocVal = (unsigned int)newValue;
    }
    vv = LocVal;
    hCinteger::setDispValue(vv); // concentrate activity into setdispval
    //Wrt_Val = LocVal;
    //if (IsLocal()) Value = Wrt_Val;
}

void hChartDate::setRealValue(CValueVarient& newValue, bool iHaveMutex)
{
    Value = (unsigned int)newValue;
}


/* stevev 25apr05*/
void hChartDate::setRealValueString(wstring& s) 
{
    Value = str2int(s);// just set it

}/* end stevev 25apr05*/

RETURNCODE hChartDate::setDisplayValueString(wstring& s) 
{   
    UINT32 loc = str2int(s) ;
    CValueVarient vv; vv = loc;

    if ( s.size() <= 0 )
    {
        /*
         * Blank values
         * We do not nofity the user of an issue, we simply restore the previous value
         * This is now consistent with how the floats and integers are being handled, POB - 4/18/2014
         */
        return APP_PARSE_FAILURE;
    }

    if ( isInRange(vv) )
    {
        hCinteger::setDispValue(vv);// will deal with locals and their notification
        return SUCCESS;
    }
    else
    {
        return APP_OUT_OF_RANGE_ERR;
    }
}
/**
void hChartDate::setDispValueString(string& s) 
{   
    // consolodate activity
    CValueVarient vv;vv = s;
    setDispValue(vv);
    //Wrt_Val = str2int(s);
    //if (IsLocal()) Value = Wrt_Val;
}
**/
UINT32 hChartDate::str2int(wstring value)  
{
    UINT32 LocVal;
    //UINT8 d=0,m=0,y=0;
    int d=0,m=0,y=0;
    int iy=0, thisdec;//swscanf has to have %d var an int!(PaulW@GE)

    /*** stevev 6jul05 - modified all to support all dates intelligently ***/
    dmDate_t  stDate = {0,0,0};
    devPtr()->getStartDate(stDate);
    thisdec = ((int)(stDate.year / 100) * 100); // this decade


    switch (editFormat)
    {
    case invsFormat:
        {
            if ( swscanf(value.c_str(),L"%02d/%02d/%04d",&d,&m,&iy) != 3 )// dd/mm/yyyy
            {   // logit not enough digits
                LocVal = 0;// leave d m y zero
            }
            // else do the processing
        }
        break;
    case dotFormat:
        {
            if ( swscanf(value.c_str(),L"%02d.%02d.%04d",&d,&m,&iy) != 3 )// dd.mm.yyyy
            {   // logit not enough digits
                LocVal = 0;
            }
            // else do the processing
        }
        break;
        default:
    case usFormat:
        {   // start stevev 6jul05
            if ( swscanf(value.c_str(),L"%2d/%2d/%4d",&m,&d,&iy) != 3 )// mm/dd/yyyy
            {   // logit not enough digits
                LocVal = 0;
            }
            // else process it
        }
        break;
    }// endswitch

    /* algorithm: force 2 digit year to be within +/- 50 years of SDC start date, 
                  convert to proper decade / millenia  or use 4 digits supplied  */
    if( iy < 100 && (m > 0 && d > 0) )      
    {   iy += thisdec;// stevev 6jul05
        if ( (iy - stDate.year) <= 50 && (iy - stDate.year) > -50 )
        {
            iy = iy; // use as-is
        }
        else
        if ( (iy - 100 ) < (stDate.year - 50) )
        {
            iy += 100;
        }
        else
        {
            iy -= 100;
        }
        iy += 1900; // stevev 25jul07 - oops, missed a step.
    }   
    else    
    if ( iy < 1900 || iy > 2155 )
    {
        iy = 1900;
    }
    //else use it as - is
    // end  stevev 6jul05
    y = iy - 1900;

    LocVal = (d << 16)+(m << 8)+((y&0xff));// all zeros on error

    DEBUGLOG(CLOG_LOG,"Date conversion:|%s|  to 0x%04x\n",value.c_str(),LocVal);

    return LocVal;
}

RETURNCODE hChartDate::Write(wstring value)  
{
    RETURNCODE rc = SUCCESS;    
    CValueVarient LocVal;

    if (! IsReadOnly() )  
    {
        LocVal = str2int(value);
        rc     = hCinteger::Write(LocVal);
    }
    else { rc = FAILURE; /*READ_ONLY*/ }

    return rc;
}

wstring& hChartDate::getDispValueString(void)
{   
    return getValueFormatedString(true);    // J.U. 17.02.11
}

//CAUTION-see 'Invensys checkin' in ddbVar.h
wstring& hChartDate::getValueFormatedString(bool bIsDispValue) // J.U. 17.02.11
{
	const size_t VAL_SIZE = 256;
	wchar_t val[VAL_SIZE];
	UINT32 LocVal = 0;
	if (bIsDispValue)
	{
		LocVal = (UINT32)Wrt_Val;// date uses only 4 bytes
	}
	else
	{
		LocVal = (UINT32)Value;// date uses only 4 bytes
	}
// not required...DATA_QUALITY_T rq = hCinteger::Read(LocVal);
	
	UINT8 d,m;
	UINT16 y;
	y = LocVal         & 0xFF;
	m = (LocVal >>  8) & 0xFF;
	d = (LocVal >> 16) & 0xFF;

	y = y + 1900;
//	y = LocVal % 100;

	switch (editFormat)
	{
	case invsFormat:
		{
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d/%02d/%04d", d,m,y);// dd/mm/yy
		}
		break;
	case dotFormat:
		{
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d.%02d.%04d", d,m,y);// dd.mm.yy
		}
		break;
	default:
	case usFormat:
		{
			swprintf (val,
#if defined(__GNUC__)
					VAL_SIZE,
#endif // __GNUC__
					L"%02d/%02d/%04d", m,d,y);// mm/dd/yy
		}
		break;
	//	{
	//		sprintf (val,"0x%06H",Value); // no format
	//	}
	}// endswitch

    returnString = val;
    
    return returnString;
};

RETURNCODE hChartDate::getDispDate(UINT32& year, UINT32& month, UINT32& day)
{   
    year  =  Wrt_Val        & 0xFF;
    month = (Wrt_Val >>  8) & 0xFF;
    day   = (Wrt_Val >> 16) & 0xFF;

    year += 1900;
    if ( month > 12 )
    {
        year = month = day = 0; 
        return FAILURE;
    }
    if ( day   > 31 )
    {
        year = month = day = 0; 
        return FAILURE;
    }
    return SUCCESS;
}       

RETURNCODE hChartDate::setDispDate(UINT32  year, UINT32  month, UINT32  day)
{
    CValueVarient v;

    v = (UINT32)(
        ((day   << 16)&0xFF) | 
        ((month << 8 )&0xFF) | 
        ((year  << 0 )&0xFF) 
                );

    if (isInRange(v))
    {
        Wrt_Val = (UINT32)v;
        return SUCCESS;
    }
    else
    {
        return APP_OUT_OF_RANGE_ERR;
    }
}


struct EnumTriad_s& EnumTriad_t::operator=( class hCgroupItemDescriptor& s)
{  val = s.getIIdx();
   //descS=s.getDesc().procureVal();
   descS=s.getDesc();
   helpS=s.getHelp().procureVal();
   return *this;
}
/**********************************************************************************************
 *
 *   $History: ddbVar.cpp $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 **********************************************************************************************
 */
 
