/*************************************************************************************************
 *
 * $Workfile: ddbFile.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbFile.h"
#include "ddbFileSupportInfc.h"
#include "logging.h"

/* the global mask converter...you have to cast the return value*/
extern int mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];

hCfile::hCfile(DevInfcHandle_t h, aCitemBase* paItemBase)
	/* initialize base class */  : hCgrpItmBasedClass(h, paItemBase),pFileFormat(NULL)
{// label & help are set in the item base class
	// members are set in the groupitem base class	
	aCattrBase* aB = NULL;

	if (pMemberAttr == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: File has No Members\n");
	}

	//******************** optional Handling
	aB = paItemBase->getaAttr(grpItmAttrHandling) ; 
	if (aB == NULL){ pHandling  = NULL; }
	else  // generate a hart class from the abstract class
	{ pHandling  = new hCattrHndlg(h, (aCattrBitstring*)aB );
	  pHandling->setItemPtr((hCitemBase*)this);
	  attrLst.push_back(pHandling);
	}
	  
	//******************** optional Update Actions
	aB = paItemBase->getaAttr(grpItmAttrUpdateSActions) ; 
	if (aB == NULL){ pUpdateActs  = NULL; }
	else  // generate a hart class from the abstract class
	{ pUpdateActs  = new hCattrUpdateAct(h, (aCattrActionList*)aB );
	  pUpdateActs->setItemPtr((hCitemBase*)this);
	  attrLst.push_back(pUpdateActs);
	}
	
	//******************** optional Identity
	aB = paItemBase->getaAttr(grpItmAttrIdentity) ; 
	if (aB == NULL){ pIdentity  = NULL; }
	else  // generate a hart class from the abstract class
	{ pIdentity  = new hCattrIdentity(h, (aCattrString*)aB );
	  pIdentity->setItemPtr((hCitemBase*)this);
	  attrLst.push_back(pIdentity);
	}
	
	//******************** optional Private
	aB = paItemBase->getaAttr(grpItmAttrPrivate) ; 
	if (aB == NULL){ pPrivate  = NULL; }
	else  // generate a hart class from the abstract class
	{ pPrivate  = new hCattrPrivate(h, (aCattrLong*)aB );
	  pPrivate->setItemPtr((hCitemBase*)this);
	  attrLst.push_back(pPrivate);
	}
	
	//******************** optional Shared
	aB = paItemBase->getaAttr(grpItmAttrShared) ; 
	if (aB == NULL){ pShared  = NULL; }
	else  // generate a hart class from the abstract class
	{ pShared  = new hCattrShared(h, (aCattrBool*)aB );
	  pShared->setItemPtr((hCitemBase*)this);
	  attrLst.push_back(pShared);
	}

	RETURNCODE rc = devPtr()->fillme(this);
}

/* from-typedef constructor */
hCfile::hCfile(hCfile*    pSrc,  hCfile*    pVal, itemIdentity_t newID)
	: hCgrpItmBasedClass(pSrc, pVal, newID),pFileFormat(NULL) 
{
	cerr<<"**** Implement file typedef constructor.***"<<endl;
}


#if 0  //Vibhor 061004: Commented...
/* Label, Help  and Validity should be handled by the item base class */
hCattrBase*   hCfile::newHCattr(aCattrBase* pACattr)  // builder 
{
	hCattrGroupItems* pMemAttr = NULL;


	if (pACattr->attr_mask == maskFromInt(fileMembers)) 
	{
		pMemAttr  = new hCattrGroupItems(devHndl(), (aCattrMemberList*)pACattr, this );
		return pMemAttr;
		
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(fileLabel) | 
			 maskFromInt(fileHelp) ))
	{//known attribute but shouldn't be here		
		LOGIT(CERR_LOG,"ERROR: file attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIT(CERR_LOG, "ERROR: file attribute type is unknown.\n");
		return NULL; /* an error */  
	}
};

#endif


// OS disk file to device object
// expects to have the format already set!
// called by device with length data in the buffer to fill the file class
// called with length == zero when complete 
// note that the device owns the memory.
RETURNCODE hCfile::fill(unsigned char*& buffer, int& length)
{
	// if first call, verify the format is set up for filling
	if ( pFileFormat == NULL )
	{
		return FILE_RV_FAILED;
	}
	else 
	{// let format do all the work
		//return pWrkData->pFormat->extractData(buffer,length);
		pFileFormat->pBaseFile = this; // give us a starting point
		return pFileFormat->extractData(buffer,length);
	}
}

// may return an error if the format is unsupported
RETURNCODE hCfile::setFormat(FileFormat_t fmt)
{
	RETURNCODE rc = SUCCESS;
	// instantiate the internal format class to support the given type
	if ( fmt <= FMT_Undefined || fmt >= FMT_Unsupported)
	{
		return FILE_RV_NOT_SUPPORTED;
	}
	if ( pFileFormat != NULL )
	{
		LOGIT(CLOG_LOG,"File setting format while there is already one set.\n");
		DESTROY(pFileFormat);
	}
	
	pFileFormat = getNewFormat(fmt);//new hCformatData(fmt, devPtr());
	if (pFileFormat != NULL)
	{
		pFileFormat->pDEV = devPtr();
		rc = SUCCESS;
	}
	else
	{
		rc = APP_MEMORY_ERROR;
	}

	return rc;
}

// dev object to OS disk file
// assumes format is set before the first entry
// first entry has pointer set to NULL, file will fill it w/ pWrkData
// called by device until pointer returned is NULL - memory belongs to file
// note: this is a device Snapshot...dependencies are NOT recorded
RETURNCODE hCfile::empty(hCformatData*& pWriteData)
{
	RETURNCODE rc = SUCCESS;
	if ( pFileFormat == NULL )
	{
		return APP_PROGRAMMER_ERROR;// must have a format to do anything
	}
	// on first call, initialize nxtMember to first member
	if (pWriteData == NULL) // initial entry
	{// setup to start
		WrkList.clear();
		rc = getList(WrkList);
		if ( rc != SUCCESS || WrkList.size() <= 0)
		{
			nxtMemberIdx = NO_MORE_MEMBERS;
			return FILE_RV_NOT_AVAILABLE;
		}
		nxtMemberIdx = 0;
		if (pFileFormat->pWrkData == NULL)
		{
			pFileFormat->pWrkData = new hCformatData(pFileFormat,devPtr());
		}
		else
		{
			pFileFormat->pWrkData->clear();// clear the write data (it's ours)
		}
		pWriteData = pFileFormat->pWrkData; 
		// insert the disk file header
		//rc = pWriteData->pFormat->insertHeader(pWriteData,devPtr()->getIdentity());
		rc = pFileFormat->insertHeader(pWriteData,devPtr()->getIdentity());
		pWriteData->indent = FILE_START_INDENT;

		// insert file's information
		//rc = pWriteData->pFormat->insertObject(pWriteData,(hCitemBase*)this);
		rc = pFileFormat->insertObject(pWriteData,(hCitemBase*)this);
		// rc = fileWrt();// uses pWrkData
// check rc
	}
	else// if    flagged no-more-members - clean-up and empty the pointer, return
	if (nxtMemberIdx == NO_MORE_MEMBERS)
	{
		pWriteData->clear();// clear the write data (it's ours)
		//rc = pWriteData->pFormat->insertObjectEnd(pWriteData, (hCitemBase*)this);
		//rc = pWriteData->pFormat->insertFooter(pWriteData);
		rc = pFileFormat->insertObjectEnd(pWriteData, (hCitemBase*)this);
		rc = pFileFormat->insertFooter(pWriteData);
		nxtMemberIdx = FILE_COMPLETE;
	}
	else
	if (nxtMemberIdx == FILE_COMPLETE)
	{
		DESTROY(pFileFormat);// sets the pointer null
		pWriteData = NULL;   // flag for eof
		WrkList.clear();
	}
	else  // call nxtMember's fileWrt( format class(contains the dataptr) )
	{
		pWriteData->clear();// clear the write data (it's ours)
		// fill member info, ( uses pWrkData)
		//rc = pWriteData->pFormat->insertMember(pWriteData,WrkList[nxtMemberIdx]);
		rc = pFileFormat->insertMember(pWriteData,WrkList[nxtMemberIdx]);

		nxtMemberIdx++ ;// inc nxtMember 
		if ( nxtMemberIdx >= (int)WrkList.size() ) // flag if no more to do // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		{
			nxtMemberIdx = NO_MORE_MEMBERS; // forces the next call to clean and finish
		}
	}

	return rc;
}


/* writes this file's header info into the data structure
RETURNCODE hCfile::fileWrt  (hCformatData*  pData)
{
	RETURNCODE rc = FAILURE;
//pFileFormat
	if ( pWrkData == NULL ) return rc;
	// file start
	// pWrkData->indent(); 
	// symbol name - unavailable at this time
	// number item id
	// pWrkData->undent(); 
	// file end, record index
	// pWrkData->indent(); // for the next guy

	return rc;
}
unused */


RETURNCODE hCfile::destroy(void)
{ 
	RETURNCODE rc;

	LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"File 0x%04x (%s) saving from destroy.\n",getID(),getName().c_str());

	rc = devPtr()->saveme(this);// won't return till complete

	DESTROY(pFileFormat);
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< file saved in file's destroy.");
							logTime();// isa logif start_stop (has newline)

	rc |= hCgrpItmBasedClass::destroy(); 
	if (pMemberAttr)
	{
		rc |= pMemberAttr->destroy();
		RAZE(pMemberAttr);
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< file members destroyed from file's destroy.");
							logTime();// isa logif start_stop (has newline)
	}
	return rc;
}


