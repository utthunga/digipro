/*************************************************************************************************
 *
 * $Workfile: ddbList.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbList.h"
#include "ddbVar.h"
#include "ddbCommand.h" // for msglist (notification)
#include "logging.h"


#include <algorithm> /* 2015 to get min/max*/

/* initial fill - use default value */
void hClist::initTypedef(hClist* pVals)	// get the typedef into the Var. NULL=>use default vals
{
	RETURNCODE  rc = SUCCESS;
	hCitemBase* pTmp;
	int wrkCap, wrkCnt, i, t;
	CValueVarient wrk;

	pTypeDef = NULL;
	
	rc = typedefRef.resolveID( pTypeDef );
	if ( rc == SUCCESS && pTypeDef != NULL )
	{// we have a typedef variable
		
		if (pCount)
		{
			// the attribute polymorphic overload to a proxy variable MAY NOT BE CONDITIONAL
			hCNumeric* pR = pCount->getExprRef();
			if ( pR != NULL && pR->IsNumeric()) // is a proxy var
			{
				pExternalCount = pR;// it's value will float.
				countIsRef = true;  // record that it is a proxy
				wrkCnt = 0;
				wrk = wrkCnt;
				//8jan09- bypass the range restraints <above> (this is never scaled anyway)
				pExternalCount->setRawDispValue(wrk);// change it from its default value (1)
				pExternalCount->ApplyIt(); // its new default value (till read)
				pExternalCount->setCountList(this);// tell him he has a new friend
			}
			else
			{
				wrk    = pCount->getExprVal();
				if ( wrk.vIsValid)
				{
					wrkCnt = wrk;
					countIsRef = false; // size is FROZEN at this size
				}
				else
				{
					LOGIT(CERR_LOG,"List's COUNT attribute did not resolve to a proxy nor a value.(%s)\n"
						, getName().c_str() );
					
					countIsRef = false;
					DESTROY(pCount);//default to no attribute state
				}
			}
		}
		else
		{
			wrkCnt = 0;
			countIsRef = false;
		}
		if ( capacityDefined )
		{
			wrkCap = (INT32)capacityValue.getDdlLong();// very limited value set
			if ( wrkCap < wrkCnt ) { t = wrkCap; }
			else/* cap >= cnt */   { t = wrkCnt; }
		}
		else
		{
			wrkCap = -1;// undefined
			t = wrkCnt;
		}
		if ( activeIL.size() == t )
		{// this could have been done as a part of an earlier list or an array
			setIdxValidity(true); // in case it wasn't done, extern validity is good
			return; // already done for us
		}
		else
		if (activeIL.size() > 0 )
		{
			setIdxValidity(true); // in case it wasn't done, extern validity is good
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error - initializing a List that already "
				" has %d elements ( needs %d )\n",activeIL.size(),t);
		}// else fall thru to init the typedef
		elementID_t thisElem;
		thisElem.contID = getID();
		for ( i = 0; i < t; i++)
		{	// init for a list is like an array, all new values */
			thisElem.which = i;
			pTmp = devPtr()->newPsuedoItem(pTypeDef, NULL, sif_RegisterNew,thisElem);
			if ( pTmp )
			{
				pTmp->setPsuedo(thisElem);// y isn't defined
				activeIL.push_back(pTmp);
// done in newpsuedoitem:: devPtr()->registerItem(pTmp->getID(), pTmp, string("psuedoItem")); 
				setIdxValidity(true); // if we have something in it, extern validity is good
			}// else error 
		}
		setCount(i);// 03aug06-was:iCount = i;
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Typedef reference did not resolve.\n");
		pTypeDef = NULL;
	}
}


hClist::hClist(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase),typedefRef(h),capacityValue(h),
	  pCount(NULL),iCount(0),pTypeDef(NULL),countIsRef(true)
{// label, help and validity are set in the base class
	
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(listAttrType); // required
	if (aB == NULL)
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hClist::hClist<no type attribute supplied.>\n");
		LOGIF(LOGP_NOT_TOK)(CERR_LOG, "       trying to find: %d but only have:", listAttrType);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIF(LOGP_NOT_TOK)(CERR_LOG,"0x%02x, ",(*iT)->attr_mask);}
		}
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ // the typedef ref is not allowed to be conditional
		aCattrCondReference* paCr = (aCattrCondReference*)aB;
		if ( paCr->condRef.priExprType != eT_Direct                ||
			 paCr->condRef.destElements.size() == 0                ||
			 paCr->condRef.destElements[0].destType != eT_Direct   ||
			 paCr->condRef.destElements[0].pPayload->whatPayload() != pltReference)
		{
			LOGIT(CERR_LOG,"ERROR: typedef reference is invalid.\n");;
		}
		else
		{
			typedefRef = *((aCreference*)paCr->condRef.destElements[0].pPayload);
		}
	}

	
	aB = paItemBase->getaAttr(listAttrCapacity); 
	if (aB != NULL)
	{	// the capacity is not allowed to be conditional (now)
		aCattrCondLong* paCl = (aCattrCondLong*)aB;
		if ( paCl->condLong.priExprType != eT_Direct                ||
			 paCl->condLong.destElements.size() == 0                ||
			 paCl->condLong.destElements[0].destType != eT_Direct   ||
			 paCl->condLong.destElements[0].pPayload->whatPayload() != pltddlLong)
		{
			cerr<<"ERROR: capacity value is invalid."<<endl;	
			capacityDefined = false;
		}
		else
		{
			capacityValue.setEqual((aCddlLong*)(paCl->condLong.destElements[0].pPayload));
			capacityDefined = true;
		}
	} 
	else 
	{		
		capacityDefined = false;
	}


	aB = paItemBase->getaAttr(listAttrCount);
	if (aB != NULL)
	{	
		pCount = new hCattrExpr(devHndl(),listAttrCount,(aCattrCondExpr*) aB,this);
		countIsRef = false; // default to frozen till initTypedef()
	}
	else //leave it empty
	{
		countIsRef = false; // count fluctuates normally
	}
	// these are only instantiated if they are used in the DD....
	pPsuedoCnt = pPsuedoCap = NULL;
	pExternalCount = NULL;
}

hClist::hClist(hClist* pSrc, hClist* pVal, itemIdentity_t newID)
	: hCitemBase((hCitemBase*)pSrc,newID),typedefRef(pSrc->devHndl()),countIsRef(true),
		capacityValue(pSrc->devHndl()),pCount(NULL),iCount(0),pTypeDef(NULL)
{	
	typedefRef	    = pSrc->typedefRef;
	capacityValue   = pSrc->capacityValue;
	capacityDefined = pSrc->capacityDefined;
	if (pSrc->pCount != NULL)
		pCount = new hCattrExpr(*(pSrc->pCount));
	// reconcile the count and capacity between the pSrc & pVal
	// these are only instatiated if they are used in the DD
	pPsuedoCnt = pPsuedoCap = NULL;
	pExternalCount = NULL;

	rdCmd_IndexList = pSrc->rdCmd_IndexList;
	wrCmd_IndexList = pSrc->wrCmd_IndexList;

	// to be here, we have to be a typedef instance in another container's contents
	// that means we do NOT have any indexes that will index us
	listOindex.clear();

	initTypedef(pVal); // we gotta hope that everything we need is already instantiated.
		// in deeply nested typedefs, we gotta do it then, there could be an array of em
		// the only way would be to continually init until there is no more allocation
}

hClist::~hClist()  
{
	if (pPsuedoCnt)
	{
		devPtr()->unRegister(pPsuedoCnt);
		DESTROY(pPsuedoCnt);
	}
	if (pPsuedoCap)
	{
		devPtr()->unRegister(pPsuedoCap);
		DESTROY(pPsuedoCap);
	}
}

/* this is ONLY called from a method built-in!!! 
   If you try to use it elsewhere the side-effects could be interesting */
/* 19jul06 - stevev - modified to insert an empty typedef item when pBase = NULL*/
RETURNCODE hClist::insert(hCitemBase* pBase, int& idx)
{
	hCitemBase* pTmp = NULL;
	// 19jul06 - was:: if (pBase == NULL || idx < 0 || pTypeDef == NULL)
	// 19jul06 -	return APP_PARAMETER_ERR;
	// verify idx
	if ( idx < 0 || idx > (int)activeIL.size() ) // size is last+1 // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		return APP_OUT_OF_RANGE_ERR;
	}
	if ( capacityDefined && ((activeIL.size()+1) > capacityValue.getDdlLong()) )
	{
		return APP_OUT_OF_RANGE_ERR;
	}
	// verify the type - 19jul06 - only if not empty
	if ( pTypeDef == NULL )
	{
		if ( typedefRef.resolveID( pTypeDef) != SUCCESS )
		{
			pTypeDef = NULL;
			LOGIT(CERR_LOG,"List could not reference its typedef.");
			return APP_RESOLUTION_ERROR;
		}
	}
	if ( pBase != NULL && (pBase->getIType() != pTypeDef->getIType()))
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: Type mismatch in insert! trying %s into a %s\n"
			,pBase->getTypeName(),pTypeDef->getTypeName());
		return APP_TYPE_MISMATCH;
	}
	// old code removed here 22jan09 - stevev
	elementID_t thisElem;
	thisElem.contID =  getID();
	thisElem.which  = idx;
	// instantiate a TYPE//// Verify that this will copy all VALUES to the new
	pTmp = devPtr()->newPsuedoItem(pTypeDef,pBase,sif_registerANDpreserve, thisElem);
	// copy the values
	if ( pTmp && ! COUNT_ISFROZE)
	{// insert the commands and indexes used to read and/or write this new element
		cmdDescListIT  iTidxLst;
		hCcommandDescriptor* pCmdDesc;
		for (iTidxLst = rdCmd_IndexList.begin(); iTidxLst != rdCmd_IndexList.end(); ++iTidxLst)
		{	pCmdDesc = (hCcommandDescriptor*)&(*iTidxLst);
			pTmp->addCmdDesc(*pCmdDesc, true);
		}
		for (iTidxLst = wrCmd_IndexList.begin(); iTidxLst != wrCmd_IndexList.end(); ++iTidxLst)
		{	 pCmdDesc = (hCcommandDescriptor*)&(*iTidxLst);
			pTmp->addCmdDesc(*pCmdDesc, false);
		}

	// insert it into the list
		/************************************************** assumption *******/
		/* assume that FIRST == ZERO ALL the time     */
		if( activeIL.size() == idx )// stevev 11dec08 - Walt's repair
		{							// indications are that the newer STL that Bill has won't
		   activeIL.push_back(pTmp);// add a record when asked to insert at a new location
									// so this push is needed when adding an item @ non existent
		   // modify the index value for the last item
		   // this cme back zero????pTmp = (hCitemBase*)(*(activeIL.back()));// back is ptr2ptr2itembase
		   pTmp = (hCitemBase*)activeIL[idx];
		   pTmp->setCommandIndexes(listOindex, idx);// reset the index values
		}
		else
		{
			activeIL.insert(activeIL.begin()+idx, pTmp);
			// modify the index values for the moved items
			elementID_t elem = {(int)(getID()),0};
			for (unsigned i = idx; i < activeIL.size(); i++)
			{
				elem.which = i;
				activeIL[i]->setPsuedo(elem);// update owner index
				activeIL[i]->setCommandIndexes(listOindex, i);// reset the index values
			}
		}

		// modify capacity and count
		setCount(activeIL.size());// 03aug06-was:iCount = activeIL.size();
		//03aug06-was:if ( ! capacityDefined  && iCount > capacityValue.getDdlLong())
		if ( ! capacityDefined  && getCount() > capacityValue.getDdlLong())
		{
			capacityValue = getCount();//03aug06-was::iCount;// takes on the largest size
		}
		// else - we wouldn't get here if it was defined and too big
		setIdxValidity(true);// extern validity is true
		return SUCCESS;
	}// else error 

	return FAILURE;
}

/* this is ONLY called from a method built-in!!! 
   If you try to use it elsewhere the side - effects could be interesting */
RETURNCODE hClist::remove(int& idx)
{
	hCitemBase* pTmp = NULL;
	if ( (idx < 0) || (idx > (int)(activeIL.size()-1)) ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		return APP_OUT_OF_RANGE_ERR;
	if ( COUNT_ISFROZE )
		return FAILURE;
	// move the pointer out of the active 
	pTmp = *(activeIL.begin()+idx);// assumes that FIRST is always ZERO

	devPtr()->notifyVarUpdate(pTmp->getID(),DEL_element);// we have to tell the UI that pointrs
													    // to this element are no longer usable
	// get rid of it
	devPtr()->destroyPsuedoItem(pTmp);// to get rid of its internal memories & unregisters it
	activeIL.erase(activeIL.begin()+idx);// calls the destructor
	setCount(activeIL.size());// 03aug06-was:iCount = activeIL.size();

	return SUCCESS;
}

// 03aug06 - moved getCount to the bottom

itemType_t    hClist::getTypedefType(void) // stevev 4may07 - was:: getIType(void) [ we need to show we are a list if asked... ]
{	
	if ( pTypeDef == NULL )
	{
		RETURNCODE rc;
		rc = typedefRef.resolveID( pTypeDef );
		if ( rc != SUCCESS || pTypeDef == NULL )
		{// we failed
			pTypeDef = NULL;
		}
	}
	if ( pTypeDef != NULL )
	{
		return (pTypeDef->getIType());
	}
	else
	{// error
		return iT_NotAnItemType;
	}
}
itemID_t    hClist::getTypedefId(void)
{
	getTypedefType(); //force a resolution
	if (pTypeDef != NULL)
		return (pTypeDef->getID());
	else
		return 0;
}

RETURNCODE hClist::getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient rV;

	if (pCount != NULL)
	{	
		rV = pCount->getExprVal();
	}
	else
	{
		rV = getCount();// 03aug06-was:iCount;
	}

	if ( rV.vIsValid  && rV.isNumeric() )
	{
		if ( ((int)rV) > 0 )
		{
			if ( indexValue < (UINT32)rV && indexValue < activeIL.size() )
			{
				hCreference localRef(devHndl());
				// make a reference from activeIL[indexValue]
				localRef.setRef(activeIL[indexValue]->getID(),0,
					            activeIL[indexValue]->getTypeVal(),false,rT_Item_id);
				returnedItemRefs.push_back(localRef);
				rc = SUCCESS;
			}
			// else no match to index - do nothing, return success
		}
		// else no content - do nothing, return success
	}
	else
	{
		cerr <<"ERROR: List has no members in getAllByIndex."<<endl;
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}


RETURNCODE hClist::getAllindexValues(vector<UINT32>& allValidindexes)
{
	/****** NOTE: this assumes a contiguous list - not yet specified 
	              stevev 22jan09 - it is specified now....   ********/
	//for ( int i = 0; i < (int)activeIL.size(); i++ ) //for each psuedoVar installed
	//{
	//	allValidindexes.push_back(i);
	//}
	// stevev 09apr15 - added other optional index definitions
	int maxIdx = -1;
	CValueVarient k;
	allValidindexes.clear();// in case caller hasn't done it

	if (activeIL.size())//for each psuedoVar installed
	{
		maxIdx = (int)activeIL.size();
	}
	else   // none installed - normal route
	{
		if ( pCount != NULL ) // value of COUNT when constant or variable
		{ // cond 3 & 4
			if (countIsRef && pExternalCount != NULL)
			{// cond 3

				/************** removed, un needed complexity WAP 19may15 *******************
				if ( pExternalCount->getDataQuality() == DA_HAVE_DATA )
				{// var data quality is good  (should never happen)				
					k = pExternalCount->getRealValue();//	use that value
					if ( k.vIsValid && k.isNumeric() )
					{
						maxIdx = (int)k;
					}
				}
				else
				if (pExternalCount->hasDefaultValue())
				{// else - has a default value  
					if ( pExternalCount->getDfltVal(k) == SUCCESS &&  k.vIsValid && k.isNumeric() )	
					{//	use that
						maxIdx = (int)k;
					}
				}				

				if (pExternalCount->VariableType() == vT_Enumerated)
				{// or it's an enum	 								
					pExternalCount->highest(k);//	use max index
					if ( k.vIsValid && k.isNumeric() )
					{
						maxIdx = (int)k;
					}
				}
				// else - fall thru to check capacity		
				************** end , un needed complexity WAP 19may15 *******************/
				// so we do nothing here and let it fall through to get the default 1 element
			}
			else
			{// cond 4 - possible constant - use that
				k = pCount->getExprVal();
				if ( k.vIsValid && k.isNumeric() )
				{
					maxIdx = (int)k;
				}
			}
		}		
		/************** removed, un needed complexity WAP 19may15 *******************
		if (allValidindexes.size() == 0 && maxIdx < 0 && capacityDefined)// not done & have a capacity
		{// all of capacity
			maxIdx = (int)(capacityValue.getDdlLong());
		}
		************* end , un needed complexity WAP 19may15 *******************/
		if ( devPtr()->getPolicy().isPartOfTok )
		{
			maxIdx = max(maxIdx,1);// force a minium of 1 element
		}

		if (maxIdx <= 0)
		{
			LOGIT(CLOG_LOG|CERR_LOG,"List static analysis found no elements.\n");
		}
	}

	for ( int i = 0; i < maxIdx; i++ ) 
	{
		allValidindexes.push_back(i);
	}
	int y = allValidindexes.size();
	LOGIF(LOGP_WEIGHT)(CLOG_LOG|COUT_LOG,"List.getAllindexes() returned %d.\n",y);
	return SUCCESS;
}


hCitemBase* hClist::operator[](int idx0)
{
	if ( idx0 < 0 || idx0 > (int)(activeIL.size()-1) ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		return NULL;// exception
	}

	return activeIL[idx0];// a list of pointers, this returns the pointer
}


RETURNCODE hClist::getByIndex(UINT32 indexValue,hCgroupItemDescriptor** ppGID,bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	if (activeIL.size() <= 0)
	{
		rc = DB_NO_LIST_ELEMENT;/* we are trying to access an item that does not exist */
		if ( !suppress)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: List's getByIndex failed to find index "
				"'0x%04x' in empty list.\n", indexValue);
		}
		return rc;	 
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( isInGroup(indexValue) )
	{
		ptr2hCitemBase_t pIb = activeIL[indexValue];
		if ( pIb != NULL && pIb->getIType() != 0 && pGid != NULL )
		{
			hCreference locRef(devHndl() );
			locRef.setRef(pIb->getID(), 0, pIb->getTypeVal(), false,rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(indexValue);
			rc = SUCCESS;
		}
	}
	else
	{
		rc = DB_NO_LIST_ELEMENT;/* we are trying to access an item that does not exist */
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: List's getByIndex failed to find index "
				"'0x%04x' in 0x%04x elements.\n", indexValue, activeIL.size());
		}
		// let NO_LIST through...rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}


CValueVarient hClist::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;

	listAttrType_t lat = (listAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( attrType >= ((unsigned)listAttr_Unknown) )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	if ( ! IsValid() )
		return rV;

	switch (lat)
	{
	case listAttrLabel:
	case listAttrHelp:
	case listAttrValidity:
		rV = hCitemBase::getAttrValue(lat);		
		break;
	case listAttrType:
		{
			if ( pTypeDef == NULL )
			{
				RETURNCODE rc;
				rc = typedefRef.resolveID( pTypeDef );
				if ( rc != SUCCESS || pTypeDef == NULL )
				{// we failed
					pTypeDef = NULL;
				}
			}
			if ( pTypeDef != NULL )
			{
				rV.vValue.varSymbolID = pTypeDef->getID();
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List's getAttrValue did not resolve type.\n");
				rV.vValue.varSymbolID = 0;
			}
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid = IsValid();
			rV.vIsUnsigned = true;
		}
		break;
	case listAttrCount:
		{
			rV.vValue.iIntConst = getCount();// 03aug06-was:iCount;
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid = IsValid();
			rV.vIsUnsigned = false;
		}
		break;
	case listAttrCapacity:
		{
			rV.vValue.iIntConst = (INT32)capacityValue.getDdlLong();// very limited value set
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid = IsValid();
			rV.vIsUnsigned = false;
		}
		break;
	case listAttrFirst:
		{
			rV.vType = CValueVarient::isSymID;// this returns a reference to the item at
			rV.vSize = 4;// default
			rV.vIsValid = IsValid();
			rV.vIsUnsigned = true;
			if ( activeIL.size() <= 0 )
			{
				rV.vValue.varSymbolID = 0;
				rV.vIsValid = false;
			}
			else
			{
				rV.vValue.varSymbolID = activeIL[0]->getID();
			}
		}
		break;
	case listAttrLast:
		{
			rV.vType = CValueVarient::isSymID;// this returns a reference to the item at
			rV.vSize = 4;// default
			rV.vIsValid = IsValid();
			rV.vIsUnsigned = true;
			if ( activeIL.size() <= 0 )
			{
				rV.vValue.varSymbolID = 0;
				rV.vIsValid = false;
			}
			else
			{
				rV.vValue.varSymbolID = activeIL.back()->getID();
			}
		}
		break;
	case listAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

// 03aug06 -stevev begin code --- the psuedoVariable handling for references
itemID_t      hClist::getAttrID (unsigned  attr, int which)
{
	itemID_t r = 0;	

	if ( attr >= ((unsigned)listAttr_Unknown) )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List's getAttrID with invalid attribute type (%d)\n",attr);
		return r;
	}// else process request

	if ( ! IsValid() )
		return r;

	hCVar* pAttr = getAttrPtr(attr);
	if ( pAttr != 0 )
	{
		r = pAttr->getID();
	}
	/* these two will return the id of item at that location in the list
	else
	if ( attr != listAttrFirst && attr != listAttrLast)
	{
		LOGIT(CERR_LOG,"ERROR(internal): List's getAttrPtr failed.\n");
		r = 0;
	}
	// else those two don't have an attribute variable
	***/
	return r;
}


hCinteger* hClist::getPsuedoInt(int initialValue)
{
	hCinteger* pInt = (hCinteger*)hCVar::newPsuedoVar(devHndl(),vT_Integer,4);
	if (pInt != NULL)
	{
		CValueVarient lv; 
		lv = initialValue;
		pInt->setDispValue(lv);// this does ApplyIt() and Notification for LOCALs
		pInt->markItemState(IDS_CACHED);// make it usable
	}
	return pInt;
}


hCVar*        hClist::getAttrPtr(unsigned  attr, int which)// 03aug06 
{
	hCVar* pVar = NULL;

	
	if ( attr >= ((unsigned)listAttr_Unknown) )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List's getAttrID with invalid attribute type (%d)\n",attr);
		return NULL;
	}// else process request

	if ( ! IsValid() )
		return pVar;


	listAttrType_t lat = (listAttrType_t)attr;

	switch(lat)
	{
	case listAttrCount:
		{
			if (pPsuedoCnt!=NULL)
			{
				pVar = pPsuedoCnt;
			}
			else 
			{// make one and return it
				pVar = pPsuedoCnt = (hCinteger*)getPsuedoInt(getCount());
				string nM; nM = getName();
				nM += "_Count";
				// default must be constant: no dependency
				pVar->setName(nM);
			}
		}
		break;
	case listAttrCapacity:
		{
			if (pPsuedoCap!=NULL)
			{
				pVar = pPsuedoCap;
			}
			else 
			{// make one and return it
				pVar = pPsuedoCap = (hCinteger*)getPsuedoInt((int)capacityValue.getDdlLong());
				string nM; nM = getName();
				nM += "_Capacity";
				// default must be constant: no dependency
				pVar->setName(nM);
			}
		}
		break;
	case listAttrFirst:
		{
			if ( activeIL.size() <= 0 )
			{
				pVar = NULL;
			}
			else
			{
				pVar = (hCVar*)activeIL[0];// if it isn't a var, caller has to cast back
			}
/* these reference the item at front and back:  NOT THE COUNT!
			if (pPsuedoFirst!=NULL)
			{
				pVar = pPsuedoFirst;
			}
			else 
			{// make one and return it
				int i = getCount();
				if ( i != 0 ) 
					i = 0;// always first except when empty
				else          
					i = -1;// equal to last
				pVar = pPsuedoFirst = (hCinteger*)getPsuedoInt(i);
			}
 -- end NOT THE COUNT***/
		}
		break;
	case listAttrLast:
		{
			if ( activeIL.size() <= 0 )
			{
				pVar = NULL;
			}
			else
			{
				pVar = (hCVar*)activeIL.back();// if it isn't a var, caller has to cast back
			}
/* these reference the item at front and back:  NOT THE COUNT!
			if (pPsuedoLast!=NULL)
			{
				pVar = pPsuedoLast;
			}
			else 
			{// make one and return it
				pVar = pPsuedoLast = (hCinteger*)getPsuedoInt(getCount()-1);
			}
 -- end NOT THE COUNT***/
		}
		break;
	case listAttrLabel:
	case listAttrHelp:
		{// the spec says you can???
			return (hCitemBase::getAttrPtr(lat));// note - probably will NOT have the same attribute number
		}
		break;
	/** these are not allowed to have psuedos **/
	case listAttrValidity:
	case listAttrType:
	case listAttrDebugInfo:
	case listAttr_Unknown:
	default:
		{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getAttrPtr with invalid attribute type %s(%d)\n",
			listAttrStrings[lat],attr);
		}
		break;
	}//endswitch

	return pVar;
}


// 03aug06 COUNT access - do not access any other way
unsigned int  hClist::setCount(unsigned  newCnt)
{// this MUST keep the psuedo Variable and internal count ALIKE
	// and give notifications on ischanged
	if (newCnt != iCount)
	{
		iCount = newCnt;
		CValueVarient newVarient;newVarient = iCount;
		if (pPsuedoCnt != NULL)
		{
			pPsuedoCnt->setDispValue(newVarient);
		}
		if ( pExternalCount != NULL )
		{
			devPtr()->aquireItemMutex(
#ifdef _DEBUG
							__FILE__,__LINE__);
#else
							  );
#endif
			pExternalCount->setDispValue(newVarient);
			pExternalCount->setWriteStatus(1);// user written
			pExternalCount->markItemState(IDS_CACHED);
			pExternalCount->cacheClear();//do a save values
			devPtr()->returnItemMutex();

			/* only apply if local, setDispValue handles all this
			if ( pExternalCount->ApplyIt() )
			if ( pExternalCount->isChanged() )// is different and not local
			{		
				hCmsgList  noteMsgs;
				noteMsgs.insertUnique(pExternalCount->getID(), mt_Val, 0);
				pExternalCount->notify(noteMsgs);
				pExternalCount->notifyUpdate(noteMsgs); // send 'em now
			}
			*/
		}
	}// else - always set the index validity
	// update last and last's psuedoitem too (if required)
	if (iCount)  setIdxValidity(true); else setIdxValidity(false);
	return iCount;
}

// moved from above and modified
unsigned int  hClist::getCount(void)
{
	//activeIL.size();
	// 03aug06-was:return (iCount = activeIL.size());
	int r = 0;
	setCount( r = activeIL.size() );// updates to actual while retrieving
	// setCount deals with index validity
	return r;
}


void hClist::staleListOnCount(unsigned  latestCount)// stevev 11dec08
{
	int w = 0;

	if ( capacityDefined  && latestCount > capacityValue.getDdlLong() )
	{
		return; // somebody messed up
	}
	if ( latestCount > LIST_MAX_ALLOWED )
	{
		return; // somebody messed up
	}
	// else we are within capacity
//	adjust size to the external var
	// we don't want to go through the setCount cycle when we are trying to 
	// adjust the size to a number from a command (for instance)
	//while( latestCount > getCount() )
	while( latestCount > activeIL.size() )
	{
		//w = getCount();
		w = activeIL.size();
		insert(NULL, w);
	}
	
	//while( latestCount < getCount() )
	while ( latestCount < activeIL.size() )
	{
		//w = getCount() -1;// from the back
		w = activeIL.size() -1;
		remove(w);
	}
	//else // they are equal... no adjustment needed

//	make all (remaining) elements stale
	MakeStale();
}

void hClist::MakeStale(void)
{ 
	ptr2hCitemBase_t pItem = NULL;
	for (ddbItemLst_it ItItem = activeIL.begin(); ItItem != activeIL.end(); ++ ItItem)
	{// ptr2ptr2itembase
		pItem = (ptr2hCitemBase_t)(*ItItem);// for newer compilers that can't just cast an iterator
		DEBUGLOG(CLOG_LOG,".",getID());
		pItem->MakeStale();
	}
	return; 
}

void hClist::addWRdesc(hCcommandDescriptor& rdD)
{
	addDesc(wrCmd_IndexList, rdD);

	// from stablefortok10
	/* see addRDdesc for notes -  stevev 04nov15 */
	if (activeIL.size())
	{
		for (ddbItemLst_it iT = activeIL.begin(); iT != activeIL.end(); ++iT)
		{
			hCitemBase* pIB = (hCitemBase*)*iT;
			if (pIB->getIType() == iT_Variable)
			{
				((hCVar*)pIB)->addWRdesc(rdD);
			}
			//else - we will probably have to carry this down thru collections
			//		and arrays (ie complex references) one day
		}
	}
}

void hClist::addRDdesc(hCcommandDescriptor& rdD)
{
	addDesc(rdCmd_IndexList, rdD);

	// from stablefortok10
	/* initTypedef will instantiate one element when it's in the tokenizer.
	   This allows the tokenizer to have an example to generate a ptoc table entry.
		This is only called at device instantiation so mosts lists will not have elements
		unless we are included in the tokenizer.
    stevev 04nov15 */
	if (activeIL.size())
	{
		for (ddbItemLst_it iT = activeIL.begin(); iT != activeIL.end(); ++iT)
		{
			hCitemBase* pIB = (hCitemBase*)*iT;
			if (pIB->getIType() == iT_Variable)
			{
				((hCVar*)pIB)->addRDdesc(rdD);
			}
			//else - we will probably have to carry this down thru collections
			//		and arrays (ie complex references) one day
		}
	}
}

void hClist::addDesc(cmdDescList_t& targetList, hCcommandDescriptor& cD)
{
	cmdDescListIT itRdCmds;
	bool rdCmdUsed = false;

	for ( itRdCmds = targetList.begin(); itRdCmds < targetList.end(); itRdCmds++)
	{// itRdCmds isa ptr 2a hCcommandDescriptor
		if (itRdCmds->cmdNumber == cD.cmdNumber &&
			itRdCmds->transNumb == cD.transNumb)
		{// there can only be one per command      -- already exists so just update indexes
			if (cD.rd_wrWgt > itRdCmds->rd_wrWgt)
			{
				itRdCmds->rd_wrWgt = cD.rd_wrWgt;//  update weight as required
			}// else leave it as is

			indexUseList_t::iterator itIUL, itInIUL;
			bool foundDup = false;

			// insert unique	                   -- insert indexes that don't exist yet
			for (itInIUL = cD.idxList.begin(); 
				 itInIUL < cD.idxList.  end(); itInIUL++)
			{// itInIUL ia ptr2a hIndexUse_t        ... for each incoming index
				for (itIUL = itRdCmds->idxList.begin(); 
					 itIUL < itRdCmds->idxList.  end(); itIUL++)
				{// itIUL ia ptr2a hIndexUse_t      ... for each existing index
					if (itInIUL->indexSymID == itIUL->indexSymID  &&
						itInIUL->indexDispValue == itIUL->indexDispValue )
					{
						foundDup = true;
						break; // out of inner for loop (only one can exist)
					}// else keep looking
				}// next existing
				if (! foundDup)
				{// insert incoming into existing					
					itRdCmds->idxList.push_back(*itInIUL);
				}
				foundDup = false; // clear for next round (in case it was set)
			}// next incoming (seldom seen)
			// note that an index list larger than the needed indexes may be generated

			rdCmdUsed = true;// found the command/transaction entry
			break;           // out of for loop <there is only one>
		}// else, continue to next read command
	}
	if (! rdCmdUsed)
	{
		targetList.push_back(cD);// insert the entire command
	}
}


//listOindex // list of pointers to Items

//private!!!
void hClist::appendUnique(ptr2hCitemBase_t pIdx)
{
	if (pIdx == NULL) return;

	ptr2hCitemBase_t pLstedIdx;
	ddbItemLst_it itIdx;
	for (itIdx = listOindex.begin(); itIdx != listOindex.end();++itIdx)
	{
		pLstedIdx = (ptr2hCitemBase_t)*itIdx;
		if ( pIdx->getID() == pLstedIdx->getID() )
		{// we have it already, bye bye
			return;
		}// else keep looking
	}//next
	// its not in the list, add it
	listOindex.push_back(pIdx);
}

void hClist::addIndex(hCindex* pIndex)
{
	appendUnique((ptr2hCitemBase_t)pIndex);
	getCount();// handles all the count intricacies, including index validity
}

void hClist::setIdxValidity(bool newV)
{
	ddbItemLst_it itIdx;
	for (itIdx = listOindex.begin(); itIdx != listOindex.end();++itIdx)
	{
		hCindex* pIndxVar = (hCindex*)*itIdx;
		if (  (pIndxVar->IsValid() ^ newV) )// xor is true on different
		{
			;// notify item and dependents
		}
		pIndxVar->setValidityFromList(newV);
	}//next
}

/* new capabilities added 22jan09 */
RETURNCODE hClist::setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue)
{
	ptr2hCitemBase_t pItem = NULL;
	for (ddbItemLst_it ItItem = activeIL.begin(); ItItem != activeIL.end(); ++ ItItem)
	{// ptr2ptr2itembase
		pItem = (ptr2hCitemBase_t)(*ItItem);//for newer compilers that can't just cast iterator
		pItem->setCommandIndexes(indexPtrList, newIdxValue);//down to the variable 
	}
	return SUCCESS; 
}

RETURNCODE hClist::addCmdDesc(hCcommandDescriptor& rdD, bool isRead)
{
	ptr2hCitemBase_t pItem = NULL;
	for (ddbItemLst_it ItItem = activeIL.begin(); ItItem != activeIL.end(); ++ ItItem)
	{// ptr2ptr2itembase
		pItem = (ptr2hCitemBase_t)(*ItItem);//for newer compilers that can't just cast iterator
		pItem->addCmdDesc(rdD,isRead);// will trickle-down to the variable where the work is
	}
	return SUCCESS; 
}


RETURNCODE hClist::resetIndexValues(void)//setCommandIndexes on each existing element
{
	RETURNCODE rc = SUCCESS;

	for ( unsigned y = 0; y < activeIL.size(); y++ )
	{
		rc  |= activeIL[y]->setCommandIndexes(listOindex, (int)y);
	}

	return rc;
}
