/*************************************************************************************************
 *
 * $Workfile: ddbPublish.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		09Dec04	sjv	started creation
 */
#pragma warning (disable : 4786) 

#include "ddbPublish.h"
#include "logging.h"
#include "ddbVar.h"
#ifdef _WIN32_WCE
#include "time_ce.h"	// PAW function missing 06/05/09
#endif

/*	int chngCntr;		// inc'd at each pub/un-pub call
	int timeEventCntr;	// timer's call to me counter
	int currentTimerLen;// value of last calcTimerLen() return
	map<int,subscription_t> subList;
*/

hCsubscriptions::hCsubscriptions():chngCntr(0),timeEventCntr(0),currentTimerLen(0),pubsChngCntr(0)
{
	pPubMtx = subscribeMutex.getMutexControl("PublishMutex");
	pSubMtx = subscribeMutex.getMutexControl("SubscribMutex");
	pDelMtx = subscribeMutex.getMutexControl("DeleteMutex");
};

hCsubscriptions::~hCsubscriptions()
{
	RAZE(pPubMtx);
	RAZE(pSubMtx);
	RAZE(pDelMtx);
}

/* private */
void hCsubscriptions::fillDataSet(subscription_t* pSS)
{
	ddbItemList_t::iterator ppIB;
	dataPoint_t     localDP;
	dataStruct_t    localDStruct;
	// get time ( time_t is nearest second )
#ifndef _WIN32_WCE
	time_t curTime;
	time( &curTime );
#else
	time_t_ce curTime;
	time_ce( &curTime );
#endif
	for ( ppIB=pSS->subscribedList.begin(); ppIB != pSS->subscribedList.end(); ++ppIB)
	{// ptr2ptr2itembase   it better not be null


		itemType_t  t = (*ppIB)->getIType();
		localDP.iID   = (*ppIB)->getID();

		if ( t == iT_Variable )
		{
			hCVar* pV = (hCVar*)(*ppIB);
			localDP.vd= pV->getDispValue();
			localDStruct.dpL.push_back(localDP);
			pV->MakeStale();
			/* stevev 27oct05 */
			pV->bumpQueryCnt();

		}
		else
		if ( t == iT_Array)
		{
			LOGIT(CERR_LOG,"*Charting arrays is not yet supported.\n");
		}
		else
		if ( t == iT_List)
		{
			LOGIT(CERR_LOG,"*Charting lists is not yet supported.\n");
		}
		else
		{
			CitemType  itype(t);
			LOGIT(CERR_LOG,"*cannot chart item type %s.\n",itype.getTypeStr());
		}
	}
	localDStruct.timestamp = curTime;
	pSS->cycleData.push_back(localDStruct);
	localDStruct.dpL.clear();localDStruct.clear();
}

// use subscriptions & passed dynamic update requirement to calc a new time
	// granularity == 0, use GCD algorithm, else use ticks
// assume caller will change to this time!!!!!!

/* private */
int hCsubscriptions::calcTimerLen(int currentUpdate, int granularity)
{
	int newtime = 0;

	if (granularity)
	{
		float f;
		subscriptionItr iT;
		subscription_t* pS;

		// granularity should not change so existing subscriptions should 
		// not have a value change here and therefore not bump
		for (iT=subList.begin();iT != subList.end();++iT)
		{
			pS = &(iT->second);
			f = ((float)pS->cycleTime) / granularity;
			pS->subCyclesPerCycle = (int)(f + 0.5); // round result
		}
		newtime = granularity;
	}
	else  // calculate GCD and calculate from there
	{
		/*
		put currentUpdate in list-of-times
		for each in sub list
			put cycleTime into list-of-times
		newtime = GCD(list-of-times);
		for each in sub list
			convert current subcycleCnt to newtime (prevent bump)
			subCyclesPerCycle = cycleTime/newtime;
		*/
	}

	currentTimerLen = newtime;
	return newtime;
}

/* private */
int hCsubscriptions::maxKey(void)
{
	subscriptionItr sIt;
	int m = -1;
	for (sIt = subList.begin(); sIt != subList.end(); ++sIt)
	{
		if ( sIt->first > m )
		{
			m = sIt->first;
		}
	}
	return m;
}
/* * * PUBLIC * * * PUBLIC * * * PUBLIC * * * PUBLIC * * * PUBLIC * * * PUBLIC * * */

/* send data when correct time, reset time event counter as required */
RETURNCODE hCsubscriptions::publish(unsigned int updatePeriod)
{
	RETURNCODE rc = SUCCESS;
	if ( pPubMtx == NULL || pPubMtx->aquireMutex(hCevent::FOREVER) != SUCCESS )
	{
		return FAILURE;
	}
	timeEventCntr++;
		
	subscriptionItr iT;
	int maxTime = -1;

	//scan the list for subcyclecnt
	for (iT=subList.begin();iT != subList.end();++iT)
	{
#ifdef _DEBUG
		subscription_t *pSec = &(iT->second);
#define SUB_SECOND pSec->
#else
#define SUB_SECOND iT->second.
#endif
		if (SUB_SECOND subCyclesPerCycle > maxTime)
		{
			maxTime = SUB_SECOND subCyclesPerCycle;
		}
		SUB_SECOND subcycleCnt++;
		if (SUB_SECOND subcycleCnt >= SUB_SECOND subCyclesPerCycle)
		{
			SUB_SECOND cycleCnt++;
			
			fillDataSet(&(iT->second));
#ifdef _DEBUG
		int thisHndl = iT->first;
		subscription_t* pS = &(iT->second);
			if (pS->cycleCnt >= pS->cyclesPerPublish)
			{
//				LOGIT(CLOG_LOG,"p0x%02x ",thisHndl);
//ok now,stop logging				LOGIT(CLOG_LOG,"pub time: %u ",pS->cycleData[0].timestamp);
	if (pS->pPublishto->pubsParent == (CdrawBase *)0xfeeefeee)
	{
				LOGIT(CLOG_LOG,"lostIt ");
	}
				rc = pS->pPublishto->publish(pS->cycleData);
				pS->cycleCnt = 0;
				pS->cycleData.clear();
			}// else not time yet
			pS->subcycleCnt = 0;
#else
			if (iT->second.cycleCnt >= iT->second.cyclesPerPublish)
			{
				rc = SUB_SECOND pPublishto->publish(SUB_SECOND cycleData);
				SUB_SECOND cycleCnt = 0;
				SUB_SECOND cycleData.clear();
			}// else not time yet
			iT->second.subcycleCnt = 0;
#endif
		}
	}
	
	if (isChanged(pubsChngCntr))
	{
		calcTimerLen(updatePeriod,GRANULARITY);
	}
#ifdef _DEBUG
//ok now,stop logging	LOGIT(CLOG_LOG,"----\n");
#endif
	pPubMtx->relinquishMutex();	
	return rc;
}

int  hCsubscriptions::addSubscription(subscription_t sub)
{
	if ( pSubMtx == NULL || pSubMtx->aquireMutex(hCevent::FOREVER) != SUCCESS )
	{
		return 0;
	}
	int n = maxKey();
	if ( n < 0 ) n = subList.size();	// always starts at zero (plus one)
	subList.insert(subscriptionMap::value_type(++n,sub));// and goes up
	chngCntr++;
#ifdef _DEBUG
LOGIT(CLOG_LOG,"Subscribed:  ");
for (ddbItemList_t::iterator II = sub.subscribedList.begin(); II != sub.subscribedList.end();++II)
{//p2p2ib
LOGIT(CLOG_LOG," 0x%04x",(*II)->getID());
}
LOGIT(CLOG_LOG,"   Address %p Handle 0x%x  in 0x%x\n",sub. pPublishto,n,getTHID());
#endif // _DEBUG

	pSubMtx->relinquishMutex();	
	return n;
}

void hCsubscriptions::delSubscription(int handle)
{
	if ( pDelMtx == NULL || pDelMtx->aquireMutex(hCevent::FOREVER) != SUCCESS )
	{
		return;
	}
	/* verify it exists */
	subscriptionItr sIt;
	sIt = subList.find(handle);
	/* get rid of it    */
	if ( sIt != subList.end() )
	{
LOGIT(CLOG_LOG,"Un-Subscribed Handle 0x%x  in thread 0x%x\n",handle,getTHID());
		subList.erase(sIt);
		chngCntr++;
	}
	pDelMtx->relinquishMutex();	
}

