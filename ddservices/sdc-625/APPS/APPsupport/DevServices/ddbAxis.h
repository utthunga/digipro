/*************************************************************************************************
 *
 * $Workfile: ddbAxis.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbAxis.h"
 */

#ifndef _DDB_AXIS_H
#define _DDB_AXIS_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "ddbRfshUnitWaoRel.h"

/*
	axisAttrLabel			use aCattrString 
	axisAttrHelp,			use aCattrString
	axisAttrValidity,		use aCattrCondLong

	axisAttrMinVal,			use aCattrCondExpr
	axisAttrMaxVal,			use aCattrCondExpr
	axisAttrScale,			use aCattrCondLong		- as an enum
	axisAttrUnit,			use aCattrString
*/

class hCaxis : public hCitemBase 
{
	hCattrExpr*              pMinVal;	//aCattrCondExpr
	hCattrExpr*              pMaxVal;	//aCattrCondExpr
	hCconditional<hCddlLong> condScale; //aCattrCondLong		- as an enum 
	hCattrUnit*			     pUnit;		//aCattrString
protected:
	// sjv 30nov06 ITEM_ID unitRelation;	// I am dependent in this
	hCunitRelList unitRelations;	        // I may be dependent in one of these
	scale_t scalingType;	// used for recalc

public:
	hCaxis(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCaxis(hCaxis*    pSrc,   itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCaxis()  {/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(AXIS_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(AXIS_LABEL, l); };

	RETURNCODE Read(wstring &value){/* item cannot be read */ return FAILURE;};
	
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Axis Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Axis ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

/*Vibhor 151004: Start of Code*/

   RETURNCODE getMinValue(CValueVarient & minVal);

   RETURNCODE getMaxValue(CValueVarient & maxVal);

// use  getUnitString      RETURNCODE getUnit(string & unitStr);

   bool isLogarithmic(void);// true if scale Logarithmic

/*Vibhor 151004: End of Code*/
	
   /* stevev 30nov06 - the following five become pass through to the list class */
    hCunitRel* getUnitRel(void){return(unitRelations.getUnit_Rel());}; 
								//sjv 30nov06:was::>  {return unitRelation;};
	void       setUnitRel(itemID_t unitRelationID){unitRelations.setUnit_Rel(unitRelationID);}; 
								//sjv 30nov06:was::>  {unitRelation = unitRelationID;};
		
	wstring     getUnitStr(void); // will return empty ("") string in worst case
	RETURNCODE getUnitString(wstring & str);//gets rel or const unit!!!!
	// stevev - WIDE2NARROW char interface
	RETURNCODE getUnitString( string & str);//gets rel or const unit!!!!

	itemID_t   getUnitVarId(void); // returns INVALID_ITEM_ID if there is none

	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	int  notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent);

};


#endif	// _DDB_AXIS_H
