/*************************************************************************************************
 *
 * $Workfile: ddbFormatHCL.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:  Holds all of base format structures
 *		
 *		09nov04	sjv	created 
 *
 *		
 * #include "ddbFormatHCL.h"
 */


#ifndef _DDB_FORMAT_HCL_H
#define _DDB_FORMAT_HCL_H

#include "ddbFileSupportInfc.h"
#include "ddbFormat.h"
#include "ddbGrpItmDesc.h"
#include "logging.h"

extern const char EOB;

typedef struct baseItem_s
{
	string name;
	string value;

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	baseItem_s(const struct baseItem_s& src){name = src.name;value = src.value; }

#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	baseItem_s(){};
} baseItem_t;

typedef vector<baseItem_t*> baseItemList_t;
typedef baseItemList_t::iterator baseItmLstIT_t;
class fmtItem;
typedef vector<fmtItem*> itemList_t;
typedef itemList_t::iterator itmLstIT_t;

class fmtItem : public baseItem_t
{
public:
	baseItemList_t attrList;
	itemList_t     membList;

	void clear(void);
	fmtItem(){};
	virtual ~fmtItem() { clear(); };

	void dump(int indent);
	int  getDataList(itemList_t& returnList);// extracts the dataItems
	UINT64 getIntValue(string s);
	double          getFltValue(string s);
	string          getStrValue(string s);
	fmtItem*        getFmtItem (string s);
};


class bufferbuff
{public:
	int len;
	int left;
	unsigned char* pBuffer;// for deallocation
	unsigned char* pNxtChar;// for traversel

	bufferbuff():len(0),pBuffer(NULL){};
	bufferbuff(int l, unsigned char* pB):len(l),pBuffer(NULL)
	{	pNxtChar = pBuffer = new uchar[l+1]; memcpy(pBuffer,pB,l); pBuffer[l]='\0';};
	virtual ~bufferbuff(){ delete[] pBuffer; len = 0; };
};
typedef vector<bufferbuff*>::iterator buffIT_t;


class bufLst : public vector<bufferbuff*> 
{
	char pbC;
	bool isPB;
	int  activeIndex;
	bufferbuff* pK;
public:
	char getChar(void);
	char peekChar(uchar offset=0);// forward only
	void putBack(char c) ;
	RETURNCODE fillStr(char* ret, int bufMaxSz, char* toOneOfThese);

	void resetBuf(void) {activeIndex = -1;};

	bufLst(): isPB(false),activeIndex(-1) {};
	virtual ~bufLst(){	if (size() > 0) 	
				{	for ( buffIT_t i = begin(); i < end(); ++i) 
					{	delete (*i);  }
					clear(); 
				}
	} ;
};

class hChclFormat : public hCformatBase
{
	bufLst  bufferList;
	fmtItem theFileItem;

// helper functions
	RETURNCODE setItemHdr(hCformatData* pData, hCitemBase* pInfo);
	RETURNCODE setItemFtr(hCformatData* pData, hCitemBase* pInfo);
	RETURNCODE insertMembers(hCformatData* pData, groupItemPtrList_t& r2grpIL);
	RETURNCODE insertElements(hCformatData* pData, ddbItemList_t& r2diL);

	RETURNCODE extractValues(void); //buffers into values
	RETURNCODE extractAnItem(fmtItem* pItm);
	RETURNCODE setValues(fmtItem* pItm);// values into variables
	RETURNCODE setTypedValues(hCitemBase* pTypedItem, fmtItem* pItm);

	RETURNCODE setItemValues(hCitemBase* pTypedItem, int idx, fmtItem* pItm);//helper

public:
	hChclFormat(){};
	virtual	~hChclFormat()	{};

	/* always use the parent's
	RETURNCODE insertHeader(hCformatData* pData, Indentity_t& ident);
	*/
	RETURNCODE insertMember   (hCformatData* pData, hCgroupItemDescriptor* pgiD);
	RETURNCODE insertElement  (hCformatData* pData, hCitemBase* pIb);

	RETURNCODE insertObject   (hCformatData* pData, hCitemBase* pInfo);
	RETURNCODE insertObjectEnd(hCformatData* pData, hCitemBase* pInfo);

	RETURNCODE extractData(unsigned char*& pNxtByte, int& bytesLeft);
};

#endif/*_DDB_FORMAT_HCL_H*/
