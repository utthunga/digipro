/*************************************************************************************************
 *
 * $Workfile: ddbGrpItmDesc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		More item classes
 *		10/24/02	sjv	created from ddbCollAndArr.h to use in other areas
 *		8/19/04		sjv made itmArr,Coll & File use these (expanded) classes
 *
 *		
 * #include "ddbGrpItmDesc.h"
 */

#ifndef _DDB_GRPITMDESC_H
#define _DDB_GRPITMDESC_H

#include "ddbPrimatives.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "ddbMenu.h"		/* stevev 30mar05 */
#include "ddbCommand.h" // for hCmsgList def

class hCattrBase;

/*************************************************************************************************
 *  An individual element
 ************************************************************************************************/

// a new base type for array elements and collection members
class hCgroupItemDescriptor : public hCobject
{
	unsigned long    itmIdx;
	hCreference		 ref;
	string           memName;

	hCddlString      descS;
	hCddlString      helpS;

	wstring			 retString;// for returning values only
      
public:

	hCgroupItemDescriptor(DevInfcHandle_t h)
		:hCobject(h), ref(h), descS(h), helpS(h) {  clear(); };
	hCgroupItemDescriptor(const hCgroupItemDescriptor& giD)
		:hCobject(giD.devHndl()),ref(giD.devHndl()),
		 descS(giD.devHndl()),helpS(giD.devHndl()),memName(giD.memName){operator=(giD);};
	virtual ~hCgroupItemDescriptor() 
	{ destroy(); };
	RETURNCODE destroy(void)
	{	RETURNCODE rc = SUCCESS;
		rc  = ref.destroy();
		      memName.erase();
		rc |= descS.destroy();
		rc |= helpS.destroy();
		return rc;
	};

	/* 15dec04-stevev for list/array instance generation */
	hCgroupItemDescriptor(hCreference& inRef)
		:hCobject(inRef.devHndl()), ref(inRef), descS(inRef.devHndl()), helpS(inRef.devHndl()) 
	{ };

	hCgroupItemDescriptor& operator=(const hCreference& inRef)
	{	itmIdx = -1;       /* you must set the correct index separately */
		ref    = inRef;
		descS.clear();		helpS.clear(); memName.erase();
		return *this;
	};
	void setIndex(unsigned long newIndex){itmIdx = newIndex;};
	/* end new */

	hCgroupItemDescriptor& operator=(const hCgroupItemDescriptor& giD)
	{	itmIdx = giD.itmIdx;
		ref    = giD.ref;
		descS  = giD.descS;
		helpS  = giD.helpS;
		memName= (giD.memName.c_str());// force a copy
		return *this;
	};
	void  setEqual(void* pAitem);
	void setDuplicate(hCgroupItemDescriptor& rGid, itemID_t parentID);

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	void clear(void)
	{	itmIdx = 0; ref.clear(); descS.clear(); helpS.clear();memName.erase();
	};

	// accessors
	unsigned long getIIdx(void) { return itmIdx;};
	hCreference&  getRef (void) { return ref;   };
	hCreference*  getpRef(void) { return &ref;  };
	hCddlString&  getDesc(void) { return descS; };
	hCddlString&  getHelp(void) { return helpS; };
	string&       getName(void) { return memName;};
	RETURNCODE    getItemPtr(hCitemBase*& pItem) 
					{ return (ref.resolveID( pItem,false )); };// stevev 27jul06
	RETURNCODE    getViaRef(hCitemBase* pBase, hCreference& rRef);// stevev 30mar05
};


typedef vector<hCgroupItemDescriptor>  groupItemList_t;
typedef vector<hCgroupItemDescriptor*> groupItemPtrList_t;
typedef vector<hCgroupItemDescriptor*>::iterator          groupItemPtrIterator_t;//ptr2ptr2gid
/*************************************************************************************************
 *  The list of elements that makeup the payload of the attribute
 ************************************************************************************************/

class hCgroupItemList : public groupItemList_t, public hCpayload, public hCobject
{
public:
	hCgroupItemList(DevInfcHandle_t h) : hCobject(h)	{	};
	hCgroupItemList(const hCgroupItemList& el) : hCobject(el.devHndl())
	{ 		
//should be OK???		cerrxx<< ">>>>>>>>>>hCgroupItemList reference copy constructor .\n");
		groupItemList_t::operator=(el);
	};
	hCgroupItemList(const hCgroupItemList* pel) : hCobject(pel->devHndl())
	{ 		
		LOGIT(CERR_LOG,">>>>>>>>>>hCgroupItemList pointer copy constructor .\n");		
		groupItemList_t::operator=(*pel);
	};
	virtual ~hCgroupItemList()
	{
		destroy();
	};
	RETURNCODE destroy(void)
	{	RETURNCODE rc = 0;
		for(groupItemList_t::iterator p = begin();p!=end(); ++p )
		{	hCgroupItemDescriptor* pgid = (hCgroupItemDescriptor*)&(*p);// PAW 03/03/09 added &(*)
			rc |= pgid->destroy();  }
		clear();  return rc;
	};

	void append(hCgroupItemList* addlist) 
	{
		reserve(size() + addlist->size());	//PO reserving to prevent multiple allocations
		for(groupItemList_t::iterator p = addlist->begin();p<addlist->end(); p++ )
		{// ptr 2a hCgroupItemDescriptor
			hCgroupItemDescriptor* pgid = (hCgroupItemDescriptor*)&(*p);// PAW 03/03/09 added &(*)
#ifdef _DEBUG
			if ( pgid == NULL || pgid == (hCgroupItemDescriptor*)0xcdcdcdcd )
			{LOGIT(CERR_LOG," - Bad group item ptr in groupitemList Append (0x"
				  "%x)\n",pgid);			}
			else
#endif /* _DEBUG*/
				push_back( *pgid );
		}// next
	};
	void appendUnique(hCgroupItemList* addlist) 
	{// temporary bypass
		append(addlist);
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)
	{
		/*COUTSPACE*/LOGIT(COUT_LOG,"      &d Group Items   \n",size() );
		if ( typeName == NULL )
			typeName = "GroupItem";
		for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{
			p->dumpSelf(indent, typeName);
		}
		return SUCCESS;
	};
	RETURNCODE getGroupItemPtrs(groupItemPtrList_t& giL );// adds 'em to the end of the list
	RETURNCODE getItemIDs(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemIndexes(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemByIdx(unsigned long idx, 
							hCgroupItemDescriptor& returnedElement, bool suppress = false)
	{   for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{	if (p->getIIdx() == idx)
			{
				returnedElement = *p;
				return SUCCESS;		
			}
		}
		if ( !suppress)
		{
			string locName;
			devPtr()->getElemNameByIndex(idx, locName);
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Group-Item List failed to match index/member '" 
					 "0x%x' (%s) in %d elements.\n",idx, locName.c_str(), size() );
			LOGIT(CERR_LOG,"       ");
			for(groupItemList_t::iterator p = begin(); p < end(); p++)
			{	
				LOGIT(CERR_LOG,"%d, ",p->getIIdx() );
			}
			LOGIT(CERR_LOG,"\n");
		}
		return DB_ELEMENT_NOT_FOUND;
	};	
	RETURNCODE getItemByIdx(unsigned long idx, hCitemBase*& returnedPtr, bool suppress = false)
	{
		hCgroupItemDescriptor itmDesc(devHndl());
		RETURNCODE rc = getItemByIdx(idx,itmDesc,suppress);
		if ( rc == SUCCESS )
		{
			rc = itmDesc.getItemPtr(returnedPtr);
		}
		itmDesc.destroy();
		return rc;
	};
	RETURNCODE getItemByName(string& idx, 
							hCgroupItemDescriptor& returnedElement, bool suppress = false)
	{   for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{	if (p->getName() == idx)
			{
				returnedElement = *p;
				return SUCCESS;		
			}
		}
		if ( !suppress)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,
						"ERROR: Group-Item List failed to match name '%s' in %d elements.\n"
				,idx.c_str(),size() );
			SDCLOG(CERR_LOG,"       ");
			for(groupItemList_t::iterator p = begin(); p < end(); p++)
			{	
				SDCLOG(CERR_LOG,"%s, ",p->getName().c_str());
			}
			SDCLOG(CERR_LOG,"\n");
		}
		return DB_ATTRIBUTE_NOT_FOUND;
	};
	bool        isItemInList(unsigned long idx)
	{   for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{	if (p->getIIdx() == idx) return true;		}
		return false;
	};
		
	/* payload required methods */
	void  setEqual(void* pAclass);	
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID);
	hCgroupItemList& operator=(const hCgroupItemList& src)
		{	groupItemList_t::operator=(src); return *this;   };

	bool validPayload(payloadType_t ldType) // actually isPayloadType()
					{ if (ldType != pltGroupItemList) return false; else return true;};
	/* end payload methods */

	RETURNCODE getItemPtrs(ddbItemList_t & ptrList); //Vibhor 131004: Added ..see note below
	// NOTE: by stevev - getItemPtrs() just gives a list! The array is allowed to be sparse
	//                   so don't get caught thinking the index into the ddbItemList_t is the
	//					 same as the index into the item-array!!
};


typedef hCcondList<hCgroupItemList>	condGroupItemList_t;
typedef vector<hCgroupItemList*>    groupItemListPtrList_t;

/*************************************************************************************************
 *  The attribute for holding the group-items of the class
 ************************************************************************************************/

class hCattrGroupItems : public hCattrBase
{	// conditional list of member lists
	condGroupItemList_t condListOGrpItmLists;

protected :
	hCgroupItemList resolvedGIL;

public:
	/* from-typedef constructor */
	hCattrGroupItems( hCattrGroupItems* pSrc, itemIdentity_t newID ) :
				hCattrBase(*((hCattrBase*)pSrc)), 
	/*sjv 24jul06 condListOGrpItmLists(pSrc->condListOGrpItmLists),*/
				condListOGrpItmLists(pSrc->devHndl()),
				resolvedGIL(pSrc->devHndl())
	{
		condListOGrpItmLists.setDuplicate((pSrc->condListOGrpItmLists), true, newID.newID);
	//sjv 24jul06	setItemPtr(pSrc->pItem);
	};
	hCattrGroupItems(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrElements),condListOGrpItmLists(h),resolvedGIL(h) {};

	hCattrGroupItems(const hCattrGroupItems& sae)
					:hCattrBase(sae),condListOGrpItmLists(sae.devHndl()),resolvedGIL(sae.devHndl())
	{ 
		attr_Type            = sae.attr_Type;
		condListOGrpItmLists = sae.condListOGrpItmLists;
	};
	// abstract constructor (from abstract class)
	hCattrGroupItems(DevInfcHandle_t h, const aCattrMemberList*  aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrElements),condListOGrpItmLists(h),resolvedGIL(h)
		{ condListOGrpItmLists.setEqualTo( (void*) &(aCa->condMemberElemListOlists));};
	hCattrBase* new_Copy(itemIdentity_t newID);// self is typedef so return a copy of self for user
	void setDuplicate(hCattrGroupItems& sae, bool replicate)        // make self a duplicate
	{condListOGrpItmLists.setDuplicate(sae.condListOGrpItmLists, replicate, pItem->getID());};

	virtual ~hCattrGroupItems () { destroy(); resolvedGIL.destroy(); };
	RETURNCODE destroy(void) 
	{	resolvedGIL.destroy(); //stevev 28aug07-quest to resolve memory leak in big files
		resolvedGIL.clear();
		return  condListOGrpItmLists.destroy();};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	return condListOGrpItmLists.dumpSelf(indent);};
//{return FAILURE;};

	RETURNCODE getList(groupItemPtrList_t& r2epL)
	{	
		RETURNCODE rc = SUCCESS;
		//hCgroupItemList mL(devHndl());

		resolvedGIL.destroy();// stevev 27aug07 ..added to recover memory
		resolvedGIL.clear();
		// stevev 25mar09  if ( pItem != NULL )// if itemid ok\/  else return failure
		// stevev 25mar09  {	//pL = condArrListOlists.resolveCond(/*getDevAPI()*/);
			rc = condListOGrpItmLists.resolveCond(&resolvedGIL);
			if (rc == SUCCESS && resolvedGIL.size() > 0 )
			{	return (resolvedGIL.getGroupItemPtrs(r2epL) );	}
			else
			{	return SUCCESS;	} // note that a conditional without a default
		// stevev 25mar09  }						  //      or an else stmt will exit here
		// stevev 25mar09  else
		// stevev 25mar09  {	return FAILURE;		}
	};								

	RETURNCODE getAllindexes(UIntList_t& allindexedIDsReturned); // in ddbCollAndArr.cpp
	// get indexed value from all branches of all conditionals
	RETURNCODE getAllItemsByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);

	RETURNCODE getAllindexValues(UIntList_t& allindexedIDsReturned);

	RETURNCODE getItemByIndex(unsigned long name, hCgroupItemDescriptor& returnedElement, bool suppress = false)
	{	
		hCgroupItemList mL(devHndl());

		RETURNCODE rc = SUCCESS;
		if ( pItem != NULL )
		{	rc = condListOGrpItmLists.resolveCond(&mL);
			if ( mL.size() > 0 )
			{	return (mL.getItemByIdx(name, returnedElement, suppress) );	}
			else
			{	returnedElement.clear();
			    return SUCCESS;	}  // note that a conditional without a default||else stmt will exit here
		}
		else
		{	return FAILURE;		}
	};
	RETURNCODE getItemByIndex(unsigned long name, hCitemBase*& pItem, bool suppress = false)
	{	
		RETURNCODE rc = APP_RESOLUTION_ERROR;
		resolvedGIL.destroy();
		resolvedGIL.clear();
		rc = condListOGrpItmLists.resolveCond(&resolvedGIL);
		if (rc == SUCCESS && resolvedGIL.size() > 0 )
			return ( rc = resolvedGIL.getItemByIdx(name, pItem, suppress) );
		else
			return rc;
	};
	
	RETURNCODE getItemByName(string& name, hCgroupItemDescriptor& returnedElement)
	{	
		hCgroupItemList mL(devHndl());

		RETURNCODE rc = SUCCESS;
		if ( pItem != NULL )
		{	rc = condListOGrpItmLists.resolveCond(&mL);
			if ( mL.size() > 0 )
			{	return (mL.getItemByName(name, returnedElement) );	}
			else
			{	returnedElement.clear();
			    return SUCCESS;	}  // note that a conditional without a default||else stmt will exit here
		}
		else
		{	return FAILURE;		}
	};
	
	bool       isItemInList(unsigned long name)
	{	hCgroupItemList mL(devHndl());
		condListOGrpItmLists.resolveCond(&mL);
		if ( mL.size() > 0 )
		{	return (mL.isItemInList(name));	}
		else
		{	return false;           		}		
	};
	// stevev 12sep06
	RETURNCODE getDepends(ddbItemList_t& retList);
	bool isConditional(void) { return (condListOGrpItmLists.isConditional()); };
	bool reCalc(void);
	void MakeStale(void);
};


/*************************************************************************************************
 *  The generic class - for item-arrays, collections, files
 ************************************************************************************************/

class hCgrpItmBasedClass: public hCitemBase /* class for collections,itemarrays, & files */
{
protected:
	hCattrGroupItems* pMemberAttr;

public:
	hCgrpItmBasedClass(DevInfcHandle_t h, aCitemBase* paItemBase);
	/* from-typedef constructor */
	hCgrpItmBasedClass( hCgrpItmBasedClass* pSrc, hCgrpItmBasedClass* pVal, itemIdentity_t newID);
	virtual ~hCgrpItmBasedClass(){ destroy();};
	RETURNCODE destroy(void);
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);
	void          MakeStale(void);

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{ hCitemBase::dumpSelf(); LOGIT(COUT_LOG,"GROUP-ITEM\n"); 
	/* TODO: -more- */  return SUCCESS;};
	RETURNCODE Label(wstring& retStr) { return baseLabel(0, retStr); };
	// stevev - WIDE2NARROW char interface
	RETURNCODE Label( string& retStr) { return baseLabel(0, retStr); };

	RETURNCODE getList(groupItemPtrList_t& r2grpIL);

	bool       isInGroup (UINT32 indexValue);
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	RETURNCODE getByIndex(UINT32 indexValue, hCitemBase*& pItem, bool suppress = true);
	RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	RETURNCODE Read(wstring &value){value = L"ERROR:Not a Var"; return FAILURE;};
	RETURNCODE getAllindexValues(UIntList_t& allValidindexes);//all possible index numbers
	RETURNCODE getAllindexes(UIntList_t& allindexedIDsReturned);// all the items possibly indexed
	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);//match index

	hCattrGroupItems* getMemberList(void){ 
	#ifdef TOKENIZER
		return pMemberAttr;
	#else
		return NULL;
	#endif
		};
	
	RETURNCODE getDescriptionList( triadList_t& listOdesc );//descriptions of indexed items

	/* stevev 22jan09 */
	RETURNCODE setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue);
	RETURNCODE addCmdDesc(hCcommandDescriptor& rdD, bool isRead);

	/* stevev 30mar05 - add to support collections as menues */
	RETURNCODE aquire(menuItemList_t& menuItemLst, const menuHist_t& history = menuHist_t());
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
};




#endif //_DDB_GRPITMDESC_H

/*************************************************************************************************
 *
 *   $History: ddbGrpItmDesc.h $
 * 
 *************************************************************************************************
 */
