/*************************************************************************************************
 *
 * $Workfile: ddbWaveform.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbWaveform.h"
#include "logging.h"
#include "ddbVar.h"
#include "ddbGraph.h"
#include "ddbCommand.h"
#include "ddbAxis.h"

#include <algorithm> /* 2015 to get min/max*/

/* memory allocation */
const char waveTypeStrings[WAVETYPESTRCOUNT] [WAVETYPEMAXLEN] = {WAVETYPESTRINGS};
const char waveformAttrStrings[WAVEFORMTATRSTRCOUNT][WAVEFORMTATRMAXLEN]={WAVEFORMATRTYPESTRINGS};

extern int mask2value(ulong inMask);

hCwaveform::hCwaveform(DevInfcHandle_t h, aCitemBase* paItemBase)
	/* initialize base class(s) */  : hCitemBase(h, paItemBase), waveType(h, (char*)&waveTypeStrings),
	pLineType(NULL),pHandling(NULL),pEmphasis(NULL),pLineColor(NULL),pYaxis(NULL),pXkeyPts(NULL),pYkeyPts(NULL),
	pXVals(NULL),pYVals(NULL),pPntCnt(NULL),pX_initial(NULL),pX_inc(NULL),
	pInitActs(NULL),pRfshActs(NULL),pExitActs(NULL),m_KeyX(h),m_KeyY(h),m_XRefs(h),m_YRefs(h)
{// label & help is set in the base class
	aCattrBase* aB = NULL;

	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(waveformAttrType); // this is the only required attribute
	if (aB == NULL)
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCwaveform::hCwaveform<no type attribute supplied.>\n");;
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"       trying to find: %d but only have:", waveformAttrType);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIF(LOGP_NOT_TOK)(CERR_LOG, "0x%02x, ",(*iT)->attr_mask);}
		}
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ // the type is not allowed to be conditional
		aCattrCondLong* paCl = (aCattrCondLong*)aB;
		if ( paCl->condLong.priExprType != eT_Direct                ||
			 paCl->condLong.destElements.size() <= 0                ||
			 paCl->condLong.destElements[0].destType != eT_Direct   ||
			 paCl->condLong.destElements[0].pPayload->whatPayload() != pltddlLong)
		{
			LOGIT(CERR_LOG,"ERROR: waveform type aC class is invalid.\n");
		}
		else
		{
			//waveType.setEqual((aCddlLong*)(paCl->condLong.destElements[0].pPayload)); 
			waveType.setEqual(paCl); 
		}
	}
			
	// NOT required - but interesting
/*	aB = paItemBase->getaAttr(waveformAttrLineType); // uses aCattrCondLong	- as enum 
	if (aB != NULL)
	{
		lineType.setEqual((aCattrCondIntWhich*)aB);//aCattrCondIntWhich
	}
	// else - leave it empty
*/
	
	for (AattributeList_t::iterator iT = paItemBase->attrLst.begin(); 
			iT < paItemBase->attrLst.end(); iT++)
	{// iT isa ptr2ptr2 aCattrBase
		if ((*iT)->attr_mask != maskFromInt(waveformAttrLabel) &&  /* itembase */
			(*iT)->attr_mask != maskFromInt(waveformAttrHelp)  &&  /* itembase */ 
			(*iT)->attr_mask != maskFromInt(waveformAttrType) &&  /* itembase */
			(*iT)->attr_mask != maskFromInt(waveformAttrValidity)  &&  /* itembase */ 
			(*iT)->attr_mask != maskFromInt(waveformAttrDebugInfo) // &&	/* above   */
			/*(*iT)->attr_mask != maskFromInt(waveformAttrLineType)*/  )/* above   */
		{// we don't already have it
			hCattrBase* pAttr = NULL; //VMKP : 050304
			pAttr = newHCattr(*iT);
			if (pAttr != NULL)
			{
/*
waveformAttrHandling,	waveformAttrEmphasis,	waveformAttrLineType,	
waveformAttrLineColor,	waveformAttrYAxis,		waveformAttrKeyPtX,		
waveformAttrKeyPtY,		waveformAttrType,		waveformAttrXVals,		
waveformAttrYVals,		waveformAttrXinitial,	waveformAttrXIncr,		
waveformAttrPtCount,	waveformAttrInitActions,waveformAttrRfshActions,
waveformAttrExitActions
*/
#ifdef LOG_ATTRIBUTES /* defined (or not) at the top of this file */				
				LOGIT(CLOG_LOG, "got a listed attribute.%s for item 0x%x\n",
				waveformAttrStrings[(waveformAttrType_t)mask2value((*iT)->attr_mask)],getID()); 
#endif
				pAttr->setItemPtr((hCitemBase*)this);
				attrLst.push_back(pAttr);
			}
			else
			{
				waveformAttrType_t en = (waveformAttrType_t)mask2value((*iT)->attr_mask); 
				LOGIT(CERR_LOG, "++ No New attribute ++ (%s)\n",waveformAttrStrings[en]);
			}
		}// else we have handled it 
	}// next available attribute
}

/* from-typedef constructor */
hCwaveform::hCwaveform(hCwaveform*    pSrc,  itemIdentity_t newID): hCitemBase(pSrc,newID), waveType(pSrc->devHndl(), (char*)&waveTypeStrings),
	pLineType(NULL),pHandling(NULL),pEmphasis(NULL),pLineColor(NULL),pYaxis(NULL),pXkeyPts(NULL),pYkeyPts(NULL),
	pXVals(NULL),pYVals(NULL),pPntCnt(NULL),pX_initial(NULL),pX_inc(NULL),
	pInitActs(NULL),pRfshActs(NULL),pExitActs(NULL),m_KeyX(pSrc->devHndl()),m_KeyY(pSrc->devHndl()),
	m_XRefs(pSrc->devHndl()),m_YRefs(pSrc->devHndl())
{
	cerr<<"**** Implement waveform typedef constructor.***"<<endl;
}



hCattrBase*   hCwaveform::newHCattr(aCattrBase* pACattr)
{
	switch (pACattr->attr_mask)
	{
	case maskFromInt(waveformAttrHandling):
		{	return (pHandling = new hCattrHndlg(devHndl(), 
										(aCattrBitstring*)pACattr, waveformAttrHandling, this ));
		}	break;
	case maskFromInt(waveformAttrEmphasis):
		{	return (pEmphasis = new hCattrValid(devHndl(), (aCattrCondLong*) pACattr, this ));
		}	break;
	case maskFromInt(waveformAttrLineColor):
		{	return (pLineColor = 
				new hCattrExpr(devHndl(),waveformAttrLineColor,(aCattrCondExpr*) pACattr,this ));
		}	break;
	case maskFromInt(waveformAttrYAxis):
		{	return (pYaxis = 
			 new hCattrAxisRef(devHndl(),waveformAttrYAxis,(aCattrCondReference*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrKeyPtX):
		{	return (pXkeyPts = 
			new hCattrRefList(devHndl(),waveformAttrKeyPtX,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrKeyPtY):
		{	return (pYkeyPts = 
			new hCattrRefList(devHndl(),waveformAttrKeyPtY,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrXVals):
		{	return (pXVals = 
			 new hCattrRefList(devHndl(),waveformAttrXVals,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrYVals):
		{	return (pYVals = 
			new hCattrRefList(devHndl(), waveformAttrYVals,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrXinitial):
		{	return (pX_initial = 
				new hCattrExpr(devHndl(), waveformAttrXinitial,(aCattrCondExpr*)pACattr, this ));
		}	break;
	case maskFromInt(waveformAttrXIncr):
		{	return (pX_inc = 
				new hCattrExpr(devHndl(), waveformAttrXIncr,(aCattrCondExpr*)pACattr,    this ));
		}	break;
	case maskFromInt(waveformAttrPtCount):
		{	return (pPntCnt = 
				new hCattrExpr(devHndl(), waveformAttrPtCount, (aCattrCondExpr*)pACattr, this ));
		}	break;
	case maskFromInt(waveformAttrInitActions):
		{	return (pInitActs = 
		new hCattrRefList(devHndl(),waveformAttrInitActions,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrRfshActions):
		{	return (pRfshActs = 
		new hCattrRefList(devHndl(),waveformAttrRfshActions,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrExitActions):
		{	return (pExitActs = 
		new hCattrRefList(devHndl(),waveformAttrExitActions,(aCattrReferenceList*)pACattr,this));
		}	break;
	case maskFromInt(waveformAttrLineType):
		{	return (pLineType = new hCattrLineType(devHndl(),(aCattrCondIntWhich*) pACattr, this ));
		}	break;

	case maskFromInt(waveformAttrType):
	{	cerr << "ERROR: waveform attribute type not a newHCattr."<<endl;return NULL;}break;
	
#ifdef _DEBUG
	case maskFromInt(waveformAttrLabel):
	case maskFromInt(waveformAttrHelp):		//known attribute but shouldn't be here	
	case maskFromInt(waveformAttrDebugInfo):	
	case maskFromInt(waveformAttrValidity):
	{	cerr << "ERROR: waveform attribute type was not handled by base class."<<endl;return NULL;
	}	break;
#endif
	default:
	{	cerr << "ERROR: waveform attribute type is unknown."<<endl;  return NULL; /* an error */  
	}	break;
	}// endswitch
	return NULL;  // never reached
}

/*Vibhor 081004: Start of Code*/
RETURNCODE hCwaveform::getHandling(unsigned &hdlg)
{ 
	waveformType_t waveType;

	hCreferenceList::iterator iT;
	hCreference tmpRef(devHndl());
	bool bAllConstants = true;

	RETURNCODE rc = SUCCESS;
	// STEVEV - 7may07-class lists may not have been filled yet...
	if ( m_YRefs.size()<= 0 )
	{
		m_YRefs.clear();
		m_XRefs.clear();
		if(NULL != pYVals)
			rc = getResolvedRefList(pYVals,m_YRefs);
		if(NULL != pXVals)
			rc = getResolvedRefList(pXVals,m_XRefs);
	}
	if (NULL == pHandling) 
		return APP_ATTR_NOT_SUPPLIED; 
	else
	{
	  hCbitString *pTmpHdlg = (hCbitString *)pHandling->procure();
	  if(pTmpHdlg)
	  {
	 	 hdlg = pTmpHdlg->getBitStr();

		 if(hdlg & WRITE_HANDLING)
		 {
			 
			waveType = getWaveFrmType();

			switch(waveType)
			{
				case wT_Y_Time:			// 1	YT_WAVEFORM_TYPE
				case wT_Horiz:			// 3	HORZ_WAVEFORM_TYPE
					{//In both these only Y values are specified		
						for(iT = m_YRefs.begin();iT != m_YRefs.end();iT++)
						{
							
							tmpRef = (*iT);
							if(tmpRef.getITYPE() == iT_Variable)
							{
								hCVar *pVar = tmpRef.getVarPtrBySymNumber(tmpRef.getID());
								if(pVar && pVar->IsReadOnly())
								{ //we found a single var as read-only then waveform is read-only
									hdlg = READ_HANDLING;
									bAllConstants = false;
									break;
								}
								bAllConstants = false;
							}
						}
					}
					break;
				case wT_Y_X:			// 2	XY_WAVEFORM_TYPE
					{
						for(iT = m_XRefs.begin();iT != m_XRefs.end();iT++)
						{							
							tmpRef = (*iT);
							if(tmpRef.getITYPE() == iT_Variable)
							{
								hCVar *pVar = tmpRef.getVarPtrBySymNumber(tmpRef.getID());
								if(pVar && pVar->IsReadOnly())
								{ //we found a single var as read-only then waveform is read-only
									hdlg = READ_HANDLING;
									bAllConstants = false;
									break;
								}
								bAllConstants = false;
							}
						}
						
						if(! bAllConstants)// stevev 09feb11 by CW: if is-all-constants, 
										// we haven't found one yet and need to continue
							break;
						for(iT = m_YRefs.begin();iT != m_YRefs.end();iT++)
						{
							
							tmpRef = (*iT);
							if(tmpRef.getITYPE() == iT_Variable)
							{
								hCVar *pVar = tmpRef.getVarPtrBySymNumber(tmpRef.getID());
								if(pVar && pVar->IsReadOnly())
								{ //we found a single var as read-only then waveform is read-only
									hdlg = READ_HANDLING;
									bAllConstants = false;
									break;
								}
								bAllConstants = false;
							}
						}
						
						

					}
					break;
				case wT_Vert:			// 4	VERT_WAVEFORM_TYPE
					{
						for(iT = m_XRefs.begin();iT != m_XRefs.end();iT++)
						{
							
							tmpRef = (*iT);
							if(tmpRef.getITYPE() == iT_Variable)
							{
								hCVar *pVar = tmpRef.getVarPtrBySymNumber(tmpRef.getID());
								if(pVar && pVar->IsReadOnly())
								{ //we found a single var as read-only then waveform is read-only
									hdlg = READ_HANDLING;
									bAllConstants = false;
									break;
								}
								bAllConstants = false;
							}
						}
						
					}
					break;
				case wT_Unknown:	
				case wT_Undefined:  
				default:
					return 0; 	//Error ... Log it
			}
		 }
			 
	  }
	  else
	  {
		 return FAILURE; //This is an error
	  }
	}//end else
	if(bAllConstants)
		hdlg = READ_HANDLING;
	return rc;

}/*End getHandling*/

bool hCwaveform :: needsEmphasis()
{
	unsigned uTemp = 0;
	if(NULL == pEmphasis)
		return false;
	else
	{
		hCddlLong *pTmpLong = (hCddlLong*) pEmphasis->procure();
		if(pTmpLong)
		{
			uTemp = (UINT32)pTmpLong->getDdlLong();// very limited value set
			if(uTemp)
				return true;
			else
				return false;
		}
		else
		{
			cerr<<"Emphasis expression resolution failed for waveform :" << this->getID() <<endl;
			return false; //This is an error
		}
	}
}/*End getEmphasis*/


RETURNCODE hCwaveform ::getLineColor(unsigned &uColor)
{
	RETURNCODE rc = SUCCESS;
	if(NULL == pLineColor)
		return APP_ATTR_NOT_SUPPLIED; //Since JIT Parser will always provide a defualt;
	else
	{
		uColor = (unsigned) pLineColor->getExprVal();
	}

	return rc;
}/*End getLineColor*/


hCaxis* hCwaveform::getYAxis()
{
	RETURNCODE rc = SUCCESS;
	if (NULL == pYaxis)
	{
		return NULL; //The Caller needs to generate the axis in the library from the values
	}
	
	hCreference *ptrTempRef = (hCreference *)pYaxis->procure();

	if(NULL == ptrTempRef)
		return NULL; 

	hCitemBase *ptr2BaseItm = NULL;
	rc = ptrTempRef->resolveID(ptr2BaseItm);
	if(SUCCESS != rc)
		return NULL; 
	else
		return (hCaxis *)ptr2BaseItm;

}/*End getYAxis*/
/*
void hCwaveform::getKeyPtsX()
{
	RETURNCODE rc = SUCCESS;
	if(NULL == pXkeyPts)
	{
		m_pKeyX = NULL;
		return rc;
	}
	else
	{
		m_pKetX = (hCreferenceList *)pXkeyPts->procure();
	}

}/*End getKeyPtsX()*/

RETURNCODE hCwaveform::getResolvedRefList(hCattrRefList* pListAttr,hCreferenceList & RslvdList)
{
	RETURNCODE rc = SUCCESS;

	if(NULL == pListAttr)
	{
		RslvdList.clear();
		return APP_ATTR_NOT_SUPPLIED;
	}
	else
	{
		//pRslvdList = (hCreferenceList *)pListAttr->procure();
		RslvdList.clear();
		rc = pListAttr->getResolvedList(&RslvdList);
		
	}

	return rc;

}/*End getKeyPtsX()*/

unsigned hCwaveform::getNumPoints()
{
	/* 06jun13 - stevev - changed this to only use the resolved points:m_XVars & m_YVars.
	   These need to be resolved BEFORE calling this function or you will get the wrong count.
	   This is because the point count of Arrays and Lists are different from the variable
	   reference count in pXVals&pYVals.
    **/
	RETURNCODE rc = SUCCESS;
	unsigned pntCnt; // no longer needed after 06jun13, xCnt, yCnt;

//Step 1: Get the Number of points, if Specified in DD
	if(NULL == pPntCnt)
		pntCnt = 0xffffffff;
	else
	{
		pntCnt = (unsigned) pPntCnt->getExprVal();
	}

#if 0 /* reactivated 07jul08 - there is no known reason for this being if'd out
	     Needed because the pPntCnt expression is always OPTIONAL.
	   */
	/* deactivated (again) it 06jun13 - this didn't work for arrays and lists */
//Step 2: Get Size of X pts list
	if(NULL == pXVals)
	{
		xCnt = 0;
	}
	else
	{
		m_XRefs.clear();
		rc = getResolvedRefList(pXVals,m_XRefs);
		if(rc)
		{
			xCnt = 0;
		}
		else
		{
			xCnt = m_XRefs.size();
		}
	}

//Step 3: Get Size of Y pts list

	if(NULL == pYVals)
	{
		yCnt = 0;
	}
	else
	{
		m_YRefs.clear();
		rc = getResolvedRefList(pYVals,m_YRefs);
		if(rc)
		{
			yCnt = 0;
		}
		else
		{
			yCnt = m_YRefs.size();
		}
	}

	waveformType_t wvType = getWaveFrmType();

	switch(wvType)			
	{
		case wT_Y_Time:			// 	YT_WAVEFORM_TYPE
		case wT_Horiz:			// 	HORZ_WAVEFORM_TYPE
			{
				return(yCnt < pntCnt ? yCnt : pntCnt);
			}
			break;
		case wT_Y_X:			// 	XY_WAVEFORM_TYPE
			{
				return (xCnt < yCnt ? (xCnt < pntCnt ? xCnt:pntCnt):
									  (yCnt < pntCnt ? yCnt:pntCnt));
			}
			break;
		case wT_Vert:			// 	VERT_WAVEFORM_TYPE
			{
				return(xCnt < pntCnt ? xCnt : pntCnt);
			}
			break;
		case wT_Unknown:	
		case wT_Undefined:  
		default:
			return 0; 	//Error ... Log it
	}//end switch
#else
	/* 06jun13 code changed */

	waveformType_t wvType = getWaveFrmType();

	switch(wvType)			
	{
		case wT_Y_Time:			// 	YT_WAVEFORM_TYPE
		case wT_Horiz:			// 	HORZ_WAVEFORM_TYPE
			{
                pntCnt = min((int)pntCnt, (int)m_YVars.size());
			}
			break;
		case wT_Y_X:			// 	XY_WAVEFORM_TYPE
			{
                pntCnt = min((int)pntCnt, (int)min((int)m_XVars.size(),(int)m_YVars.size()));
			}
			break;
		case wT_Vert:			// 	VERT_WAVEFORM_TYPE
			{
                pntCnt = min( (int)pntCnt, (int)m_XVars.size() );
			}
			break;
		case wT_Unknown:	
		case wT_Undefined:  
		default:
			pntCnt = 0; 	
	}//end switch
#endif

	return (pntCnt == 0xffffffff) ? 0 : pntCnt; // single exit point
}/*getNumPoints*/

// all this does is fill the X/Y var lists with valid hCVar IDs
// NOTE that this makes the assumption that waveforms require numbers
RETURNCODE hCwaveform::resolvePoints(void)
{
	RETURNCODE rc = SUCCESS;

	waveformType_t wvType = getWaveFrmType();

// 06jun13 - this has to be done after vars are filled	unsigned uPts2Plot = getNumPoints();
	unsigned uPts2Plot = 0;

	int i = 0;
//refilled in getNumPoints	m_YRefs.clear();
//refilled in getNumPoints	m_XRefs.clear();
	m_YVars.clear();
	m_XVars.clear();
// 06jun13 -- we'll let em try...	if (uPts2Plot < 1)// 0
//										return SUCCESS;
//refilled in getNumPoints... 06jun13...used to be filled there, done here now 
	//(only place that getNumPoints was called)
	if(NULL != pYVals)
	{
		m_YRefs.clear();
		rc = getResolvedRefList(pYVals,m_YRefs);
	}
	if(NULL != pXVals)
	{
		m_XRefs.clear();
		rc = getResolvedRefList(pXVals,m_XRefs);
	}
	

	switch(wvType)
	{
		case wT_Y_Time:		// 1	YT_WAVEFORM_TYPE
			{
				if(!m_YRefs.size())
				{
					return  FAILURE; 
				}
				else
				{
					// - use filter... rc = m_YRefs.getValueList(m_YVars,uPts2Plot);
					rc = m_YRefs.getEntryList(m_YVars,uPts2Plot,accept_NumericMixed);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						return FAILURE;
					}
					//with YTime, the point count is determined by the Y variable size when 
					// Number of points not included. added 12aug08
					uPts2Plot = getNumPoints(); // inserted 06jun13

					/* removed 06jun13 - this is in getNumPoints()
					if ( pPntCnt == NULL) 
						uPts2Plot = m_YVars.size();
					else
					if(m_YVars.size() < uPts2Plot)
						uPts2Plot = m_YVars.size();
					***/

					CValueVarient xInit,xIncr,xValue;
					rc = getXInitial(xInit);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						return FAILURE;
					}
					rc = getXIncrement(xIncr);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						return FAILURE;
					}

					for(i = 0; i < (int)uPts2Plot ;  i++) 
					{
						if(m_YVars[i].vIsValid)  //Filter out the invalid points
						{
							xValue  = (float)xInit + (float)(xIncr )* i; 
						}
						else
						{// keep a 1:1 relationship
							xValue.vIsValid = false;
							xValue.vType    = CValueVarient::invalid;
						}
						m_XVars.push_back(xValue);
					}
				}
			}
			break;
		case wT_Y_X:			// 2	XY_WAVEFORM_TYPE
			{
				if(!m_XRefs.size() && !m_YRefs.size()) //Need to have both
				{
					m_XRefs.clear(); m_YRefs.clear();
					return FAILURE; 
				}
				else
				{
					// - use filter... rc = m_YRefs.getValueList(m_YVars,uPts2Plot);
					rc = m_YRefs.getEntryList(m_YVars,uPts2Plot,accept_NumericMixed);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						return FAILURE;
					}
					// 06jun13 - this is in getNumPoints  if(m_YVars.size() < uPts2Plot)
					//                      	              uPts2Plot = m_YVars.size();

					// - use filter... rc = rc = m_XRefs.getValueList(m_XVars,uPts2Plot);
					rc = m_XRefs.getEntryList(m_XVars,uPts2Plot,accept_NumericMixed);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						m_XVars.clear();
						return FAILURE;
					}
					uPts2Plot = getNumPoints(); // inserted 06jun13

					// 06jun13 - this is in getNumPoints  if(m_XVars.size() < uPts2Plot)
					//                      	          	uPts2Plot = m_XVars.size();
				}
			}
			break;
		case wT_Horiz:		// 3	HORZ_WAVEFORM_TYPE
			{
				if(!m_YRefs.size())
				{
					return FAILURE; 
				}
				else
				{
					m_XVars.clear();
					// - use filter... rc = m_YRefs.getValueList(m_YVars,uPts2Plot);
					rc = m_YRefs.getEntryList(m_YVars,uPts2Plot,accept_NumericMixed);
					if(SUCCESS != rc)
					{
						m_YVars.clear();
						return FAILURE;
					}
					uPts2Plot = getNumPoints(); // inserted 06jun13
					// 06jun13 - this is in getNumPoints  if(m_YVars.size() < uPts2Plot)
					//                      	          	uPts2Plot = m_YVars.size();				
				}
			}
			break;
		case wT_Vert:			// 4	VERT_WAVEFORM_TYPE
			{
				if(!m_XRefs.size())
				{
					return FAILURE; 
				}
				else
				{
					m_YVars.clear();
					// - use filter... rc = m_XRefs.getValueList(m_XVars,uPts2Plot);
					rc = m_XRefs.getEntryList(m_XVars,uPts2Plot,accept_NumericMixed);
					if(SUCCESS != rc)
					{
						m_XVars.clear();
						return FAILURE;
					}
					uPts2Plot = getNumPoints(); // inserted 06jun13
					// 06jun13 - this is in getNumPoints  if(m_XVars.size() < uPts2Plot)
					//                      	          	uPts2Plot = m_XVars.size();
				}
			}
			break;
		case wT_Unknown:	
		case wT_Undefined:     // 8	MAX_SIZE
		default:
			rc = FAILURE;
			break;
	}

	return rc;
} // end of resolvePoints()


// this will recalculate conditionals and references
// designed for use in an update_structure situation
RETURNCODE hCwaveform::getNewPointList(pointValueList_t& pointList, ddbItemList_t& subLst)
{
	RETURNCODE rc = SUCCESS;
	pointList.clear(); //cleanup the list

	rc = resolvePoints();// re-resolve the conditionals and references

	if ( rc == SUCCESS )
	{
		return ( getPointList(pointList,subLst) );
	}
	else
	{
		return rc;
	}
}

#define X_MT  1
#define Y_MT  2
#define BOTH_MT 3

// this just gets the latest values for the current variable list(s)
// designed for use in an update_value situation

RETURNCODE hCwaveform::getPointList(pointValueList_t& pointList, ddbItemList_t& subLst)
{
	RETURNCODE rc = SUCCESS;	
	pointList.clear(); //cleanup the list; defensive

	// we use lstType on the m_X/YRefs to determine their fill status
	int lstType = 0;
	if (m_YRefs.size() <= 0) lstType |= Y_MT;
	if (m_XRefs.size() <= 0) lstType |= X_MT;
	// just in case somebody isn't thinking...
	if ( lstType == BOTH_MT )
	{
		rc = resolvePoints();
		if ( rc != SUCCESS )
		{
			return rc;
		}
		lstType = 0;
		if (m_YRefs.size() <= 0) lstType |= Y_MT;
		if (m_XRefs.size() <= 0) lstType |= X_MT;
		if (lstType == BOTH_MT) 
			return FAILURE;
	}

	// we use lstType on the m_X/YVars to determine the data to fill
	// the Refs won't match the Vars on stuff that internally generates the x's
	lstType = 0;
	if (m_YVars.size() <= 0) lstType |= Y_MT;
	if (m_XVars.size() <= 0) lstType |= X_MT;
	hCitemBase* pItem = NULL;
	CValueVarient* pVy, *pVx;
	pointValue_t point;
	varientList_t::iterator xIt,yIt;
#ifdef _DEBUG
	unsigned itrCnt = 0;
	for ( xIt  = m_XVars.begin(), yIt  = m_YVars.begin();
		!(xIt == m_XVars.end() && yIt == m_YVars.end()) ; itrCnt++/*internal ++xIt, ++yIt*/)
	{
		if (itrCnt >= m_YVars.size() || itrCnt >= m_XVars.size())
		{
			LOGIT(CLOG_LOG,"iterator end not matching size.(points list)\n");
			break;
		}
#else
	for ( xIt  = m_XVars.begin(), yIt  = m_YVars.begin();
	/* the following works for well behaved pont sets, it crashes when one is shorter than other
		!(xIt == m_XVars.end() && yIt == m_YVars.end()); //internal ++xIt, ++yIt
	changed to the following 12aug08 by stevev */
		 (xIt != m_XVars.end() && yIt != m_YVars.end());
		)
	{
#endif
		pVy = (CValueVarient*)&(*yIt);		// PAW 03/03/09 added &(*)
		pVx = (CValueVarient*)&(*xIt);		// PAW added &(*)
		if ( (lstType & Y_MT)  == Y_MT )// bitwise AND not 0 means Y empty
		{
			point.vy.clear();// sets invalid
		}
		else //  y has values
		{
			point.vy  =	*pVy;
			++yIt;
		}
		if ( (lstType & X_MT) == X_MT )// bitwise AND not 0 means X empty
		{
			point.vx.clear();// sets invalid
		}
		else //  x has values
		{
			point.vx  = *pVx;
			++xIt;
		}
		 
		//if ((! point.vy.vIsValid) || (! point.vx.vIsValid))
		//{// one was not valid when constructed - just skip it
		//	continue;
		//}
		if (point.vy.vType == CValueVarient::isSymID && point.vy.vIsValid)
		{// get the value
			if (devPtr()->getItemBySymNumber((unsigned int)point.vy, &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that.
#ifndef _INVENSYS 		// Invensys needs SymbolId not Display value J.U. 
				point.vy = ((hCVar*)pItem)->getDispValue();
#endif
				ItmLstappendUnique(subLst, pItem);
			}
			else
			{
				point.vy.vIsValid = false;
			}
		}
		if (point.vx.vType == CValueVarient::isSymID && point.vx.vIsValid)
		{// get the value
			if (devPtr()->getItemBySymNumber((unsigned int)point.vx, &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that.
#ifndef _INVENSYS 		// Invensys needs SymbolId not Display value J.U. 
				point.vx = ((hCVar*)pItem)->getDispValue();
#endif
				ItmLstappendUnique(subLst, pItem);
			}
			else
			{
				point.vx.vIsValid = false;
			}
		}
		pointList.push_back(point);// push valid and invalid alike
	}// next point pair
if (pointList.size() <= 0)
{
	return FAILURE;
}
	return SUCCESS; // stuff in the list
}
	
bool  hCwaveform::isThisApoint(itemID_t iid)
{
	bool r = false;
	
	// just in case somebody isn't thinking...
	if ( m_YRefs.size() <= 0 && m_XRefs.size() <= 0 )
	{
		if ( resolvePoints() != SUCCESS )
		{
			return false;
		}
	}
	
	int lstType = 0;
	if (m_YVars.size() <= 0) lstType |= Y_MT;
	if (m_XVars.size() <= 0) lstType |= X_MT;
	CValueVarient* pVy, *pVx;
	//pointValue_t point;
	unsigned itrCnt = 0;
	varientList_t::iterator xIt,yIt;
	for ( xIt  = m_XVars.begin(), yIt  = m_YVars.begin();
//		  xIt != m_XVars.end() && yIt != m_YVars.end();  ++xIt, ++yIt)
		!(xIt == m_XVars.end() && yIt == m_YVars.end());  itrCnt ++ )
	{
		if (itrCnt >= m_YVars.size() || itrCnt >= m_XVars.size())
		{
			LOGIT(CLOG_LOG,"Programmer error::> iterator end not matching size (CheckPoint).\n");
			#ifdef _DEBUG
			LOGIT(CLOG_LOG,"                    iterator @ %d, Xsize:%d  Ysize:%d\n",itrCnt,
						m_XVars.size(),m_YVars.size());
			#endif
			break;
		}

		//point.vy  =	*yIt;
		//point.vx  = *xIt; 
		pVy = (CValueVarient*)&(*yIt);// PAW 03/03/09 added &(*)
		pVx = (CValueVarient*)&(*xIt);// PAW added &(*)
		//if ((! point.vy.vIsValid) || (! point.vx.vIsValid))
		//{// one was not valid when constructed - just skip it
		//	continue;
		//}
		if ( (lstType & Y_MT)  == 0 )// bitwise AND is 0 means Y NOT empty
		{
			if (pVy->vType == CValueVarient::isSymID && pVy->vValue.varSymbolID == iid)
			{
				r = true;// one is a match... we be done
				break;   // out of for loop
			}			
			++yIt;
		}

		if ( (lstType & X_MT) == 0 )// bitwise AND is 0 means X NOT empty
		{
			if (pVx->vType == CValueVarient::isSymID && pVx->vValue.varSymbolID == iid)
			{
				r = true;// one is a match... we be done
				break;   // out of for loop
			}
			++xIt;
		}
	
	}// next point pair
	return r; // stuff in the list
}

int hCwaveform::ItmLstappendUnique(ddbItemList_t& subLst, hCitemBase* pItem)
{
	ddbItemList_t::iterator p2ptr2hCitemBase_t;
	ptr2hCitemBase_t pLstItm;
	for ( p2ptr2hCitemBase_t = subLst.begin(); p2ptr2hCitemBase_t != subLst.end(); ++p2ptr2hCitemBase_t)
	{
		pLstItm = (hCitemBase*)(*p2ptr2hCitemBase_t);
		if (pLstItm->getID() == pItem->getID())
		{
			return subLst.size();
		}
		// else - keep looking
	}
	// if we get here it doesn't exist, add it
	subLst.push_back(pItem);
	return subLst.size();
}

void hCwaveform::markDynStale()
{
	// just in case somebody isn't thinking...
	if ( m_YRefs.size() <= 0 && m_XRefs.size() <= 0 )
		return;
	
	int lstType = 0;
	if (m_YVars.size() <= 0) lstType |= Y_MT;
	if (m_XVars.size() <= 0) lstType |= X_MT;
	CValueVarient* pVy, *pVx;
	//pointValue_t point;
	hCitemBase*  pItem;

	unsigned itrCnt = 0;
	varientList_t::iterator xIt,yIt;
	for ( xIt  = m_XVars.begin(), yIt  = m_YVars.begin();
//		  xIt != m_XVars.end() && yIt != m_YVars.end();  ++xIt, ++yIt)
		!(xIt == m_XVars.end() && yIt == m_YVars.end());  itrCnt++ )
	{
		if (itrCnt >= m_YVars.size() || itrCnt >= m_XVars.size())
		{
			LOGIT(CLOG_LOG,"Programmer error::> iterator end not matching size (Mark Stale).\n");
			#ifdef _DEBUG
			LOGIT(CLOG_LOG,"                    iterator @ %d, Xsize:%d  Ysize:%d\n",itrCnt,
						m_XVars.size(),m_YVars.size());
			#endif
			break;
		}


		//point.vy  =	*yIt;
		//point.vx  = *xIt;  
		pVy = (CValueVarient*)&(*yIt);	// PAW 03/03/09
		pVx = (CValueVarient*)&(*xIt);	// PAW
		//if ((! pVy->vIsValid) || (! pVx->vIsValid))
		//{// one was not valid when constructed - just skip it
		//	continue;
		//}
		
		if ( (lstType & Y_MT)  == 0 &&  pVy != NULL )       // bitwise AND is 0 means Y NOT empty
		{
			if (pVy->vType == CValueVarient::isSymID &&	// stevev 23may07 - must handle all consts too
				devPtr()->getItemBySymNumber((unsigned int)*pVy, &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that.
				if ( ((hCVar*)pItem)->IsDynamic() )
				{
					((hCVar*)pItem)->MakeStale();
				}
			}
			//else - skip it
			++yIt;
		}

		if ( (lstType & X_MT)  == 0 &&  pVx != NULL )      // bitwise AND is 0 means X NOT empty			   )
		{
			if (pVx->vType == CValueVarient::isSymID  &&// stevev 23may07 - must handle all consts too
				devPtr()->getItemBySymNumber((unsigned int)*pVx, &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that.
				if ( ((hCVar*)pItem)->IsDynamic() )
				{
					((hCVar*)pItem)->MakeStale();
				}
			}
			//else - skip it
			++xIt;
		}
	}// next point pair
	return;
}

RETURNCODE hCwaveform::getXInitial(CValueVarient & cv)
{
	RETURNCODE rc = SUCCESS;

	if(NULL == pX_initial)
		return APP_ATTR_NOT_SUPPLIED; //should never happen
	else
	{
		cv = pX_initial->getExprVal();
	}

	return rc;

}


RETURNCODE hCwaveform::getXIncrement(CValueVarient & cv)
{
	RETURNCODE rc = SUCCESS;
	if(NULL == pX_inc)
		return APP_ATTR_NOT_SUPPLIED; //should never happen
	else
	{
		cv = pX_inc->getExprVal();
	}

	return rc;
}

RETURNCODE hCwaveform::getKeyPoints(pointValueList_t & keyPtList, ddbItemList_t& subLst)
{
	RETURNCODE rc = SUCCESS;

	varientList_t tempXList,tempYList;
	varientList_t tmpMixedX,tmpMixedY;
	pointValue_t point;
	unsigned i = 0;
	if(NULL != pYkeyPts)
		rc = getResolvedRefList(pYkeyPts,m_KeyY);
	else
		rc = FAILURE; // Since both should be defined for KeyPts irrespective of waveType
	if(NULL != pXkeyPts)
		rc = getResolvedRefList(pXkeyPts,m_KeyX);
	else
		rc = FAILURE; // Since both should be defined for KeyPts irrespective of waveType


	if(!m_KeyX.size() && !m_KeyY.size()) //Need to have both //Vibhor fixed: 171204
	{
		keyPtList.clear();
		return FAILURE; 
	}
	else
	{
		rc  = m_KeyY.getValueList(tempYList,0xffffffff);
		if(SUCCESS != rc)
		{
			return FAILURE;
		}
		rc  = m_KeyX.getValueList(tempXList,0xffffffff);
		if(SUCCESS != rc)
		{
			return FAILURE;
		}
		rc  = m_KeyY.getEntryList( tmpMixedY );
		rc |= m_KeyY.getEntryList( tmpMixedX );
		hCitemBase* pItem;
		
		for(i = 0; i < tempXList.size() && i < tempYList.size() ;  i++)  
		{
			if(tempYList[i].vIsValid && tempXList[i].vIsValid) //Filter out the invalid points
			{
				point.vy =	tempYList[i];
				point.vx =  tempXList[i];
				keyPtList.push_back(point);
			}
			
			if (i < tmpMixedY.size()  &&  tmpMixedY[i].vType == CValueVarient::isSymID &&
				devPtr()->getItemBySymNumber((unsigned int)tmpMixedY[i], &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that
				ItmLstappendUnique(subLst, pItem);
			}
			
			if (i < tmpMixedX.size()  && tmpMixedX[i].vType == CValueVarient::isSymID &&
				devPtr()->getItemBySymNumber((unsigned int)tmpMixedX[i], &pItem)==SUCCESS && pItem != NULL)
			{// list has been filtered to numeric hCVars only
				// do not test validity. - let update structure do that
				ItmLstappendUnique(subLst, pItem);
			}
		}
	}

	return SUCCESS; //just for now.

}/*End getKeyPoints*/


RETURNCODE hCwaveform::doInitActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pInitActs) /*No methods*/
		return rc;

	rc = pInitActs->getResolvedList(&wrkList);

	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}


RETURNCODE hCwaveform::doRefreshActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pRfshActs) /*No methods*/
		return rc;

	rc = pRfshActs->getResolvedList(&wrkList);

	if(SUCCESS == rc && wrkList.size() > 0)
	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}

RETURNCODE hCwaveform::doExitActions()
{
	RETURNCODE rc = SUCCESS;
	CValueVarient        retVal;
	methodCallList_t methodList;

	hCreferenceList wrkList(devHndl());

	if(NULL == pExitActs) /*No methods*/
		return rc;

	rc = pExitActs->getResolvedList(&wrkList);

	if(SUCCESS == rc && wrkList.size() > 0)
	{

		rc = wrkList.getMethodCallList(msrc_ACTION,methodList);

		if (rc == SUCCESS && methodList.size() > 0 )
		{
			rc = devPtr()->executeMethod(methodList,retVal);
		}
		else
		{
			// NORMAL:: cerr <<"ERROR: method list resolved to empty in doInitActions ("<<rc<<")"<<endl;
			//rc = FAILURE;
		}
	}

	return rc;
}

/*Vibhor 221004: Start of Code*/

lineType_t hCwaveform::getLineType()
{
	if(NULL == pLineType)
	{
		return lT_Data;//default, to be implemented in JIT Parser
	}
	hCddlIntWhich tmpIntWhich(devHndl());

	tmpIntWhich = pLineType->procureBase();

	unsigned uLType = (UINT32)tmpIntWhich.getDdlLong();// very limited value set

	int which = tmpIntWhich.getWhich();

	switch((lineType_t)uLType)
	{
		case lT_Data:
			{
			if(which > 0 && which < 10)
				return ((lineType_t)( lineTypeOffset + which));
			else
				return lT_Data;

			}
			break;
		case lT_LowLow:
		case lT_Low:	
		case lT_High:
		case lT_HighHigh:
		case lT_Trans:
			return (lineType_t)uLType;
			break;
		case lT_Unknown:
		case lT_Undefined:
		default:
			cerr <<"hCwaveform::getLineType() received an Invalid Line type !! "<<endl;
			break;

	}
	return lT_Data;
}


/*Vibhor 071204: Start of code*/

RETURNCODE hCwaveform::getActionList(actionType_t actionType,ddbItemList_t &actionList)
{
	RETURNCODE rc = SUCCESS;

	hCreferenceList wrkList(devHndl());

	switch(actionType)
	{
		case eT_actionInit :
			{
				if(NULL == pInitActs) /*No methods*/
				{
					rc = FAILURE;
					break;
				}

				rc = pInitActs->getResolvedList(&wrkList);

			}
			break;
		case eT_actionRefresh:
			{
				if(NULL == pRfshActs) /*No methods*/
				{
					rc = FAILURE;
					break;
				}
				rc = pRfshActs->getResolvedList(&wrkList);
			}
			break;
		case eT_actionExit:
			{
				if(NULL == pExitActs) /*No methods*/
				{
					rc = FAILURE;
					break;
				}
				rc = pExitActs->getResolvedList(&wrkList);
				
			}
			break;
		default:
			break;
	}

	if(SUCCESS == rc && wrkList.size() > 0)
	{
		rc = wrkList.getItemPtrs(actionList);
		return rc;
	}

	return rc;

}/*End getActionList*/

/*Vibhor 071204: End of code*/


CValueVarient hCwaveform::getAttrValue(unsigned attrType, int which)
{	
	CValueVarient rV;

	waveformAttrType_t wat = (waveformAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( wat >= waveformAttr_Unknown )
	{
		LOGIT(CERR_LOG,"ERROR: Waveform's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request


	switch (wat)
	{
	case waveformAttrLabel:
	case waveformAttrHelp:
		rV = hCitemBase::getAttrValue(wat);		
		break;
	case waveformAttrHandling:
		{			
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned u = 0;
			if ( getHandling(u) != SUCCESS )
			{
				rV.vValue.iIntConst = READ_HANDLING | WRITE_HANDLING ;
			}
			else
			{
				rV.vValue.iIntConst = u;
			}
		}
		break;
	case waveformAttrEmphasis:
		{			
			rV.vType = CValueVarient::isBool;
			rV.vSize = 1;
			rV.vIsValid = true;
			rV.vIsUnsigned = false;

			bool x = needsEmphasis();

			rV.vValue.bIsTrue = (x)?true:false;
		}
		break;
	case waveformAttrLineType:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			lineType_t lt = getLineType() ;
			rV.vValue.iIntConst = (int) lt;
		}
		break;
	case waveformAttrLineColor:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned lc;
			if ( getLineColor(lc) == SUCCESS )
			{
				rV.vValue.iIntConst = lc;
			}
			else
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid    = false;
			}
		}
		break;
	case waveformAttrYAxis:
		{
			rV.vType = CValueVarient::isSymID;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			if ( pYaxis == NULL )
			{// - don't know the correct action here since the graph will have an axis anyway
				// TODO: figure out how to deal with this
				rV.vValue.varSymbolID = 0;
				rV.vIsValid = false;
			}
			else
			{
				rV = pYaxis->getValue();
			}
		}
		break;
	case waveformAttrType:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			rV.vValue.iIntConst = (int)getWaveFrmType();
		}
		break;
	case waveformAttrXinitial:
		{	
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = false;

			if (pX_initial == NULL)
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid    = false;
			}
			else
			{
				rV = pX_initial->getExprVal();
			}
		}
		break;
	case waveformAttrXIncr:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = false;

			if (pX_inc == NULL)
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid    = false;
			}
			else
			{
				rV = pX_inc->getExprVal();
			}
		}
		break;
	case waveformAttrPtCount:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid    = true;
			rV.vIsUnsigned = false;

			if (pPntCnt == NULL)
			{
				rV.vValue.iIntConst = 0;
				rV.vIsValid    = false;
			}
			else
			{
				rV = pPntCnt->getExprVal();
			}
		}
		break;
		//=======================================================================================
	case waveformAttrKeyPtX:
		{// Cannot return a list::  hCattrRefList*      pXkeyPts
			LOGIT(CERR_LOG,"ERROR: waveformXkeyPts information is not accessible from here.\n");
		}
		break;
	case waveformAttrKeyPtY:
		{// Cannot return a list::  hCattrRefList*      pYVals
			LOGIT(CERR_LOG,"ERROR: waveformYkeyPts information is not accessible from here.\n");
		}
		break;
	case waveformAttrXVals:
		{// Cannot return a list::  hCattrRefList*      pXVals
			LOGIT(CERR_LOG,"ERROR: waveformXvalues information is not accessible from here.\n");
		}
		break;
	case waveformAttrYVals:
		{// Cannot return a list::  hCattrRefList*      pYVals
			LOGIT(CERR_LOG,"ERROR: waveformYvalues information is not accessible from here.\n");
		}
		break;
	case waveformAttrInitActions:
	case waveformAttrRfshActions:
	case waveformAttrExitActions:
		{
			LOGIT(CERR_LOG,"ERROR: waveform Action information is not accessible from here.\n");
		}
		break;
	case waveformAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIT(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

/* stevev 11sep06 - dependency extentions */
void  hCwaveform :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes
	if ( pHandling != NULL )//VAR_HANDLING_ID  
	{
		pHandling->getDepends(iDependOn);
	}
	if ( pYaxis != NULL )// Y_AXIS 
	{
		pYaxis->getDepends(iDependOn);
	}

	if ( pXVals != NULL )// X_VALUES 
	{
		pXVals->getDepends(iDependOn);
	}
	if ( pYVals != NULL )// Y_VALUES 
	{
		pYVals->getDepends(iDependOn);
	}
	if ( pPntCnt != NULL )// [NUMBER_OF_POINTS] 
	{
		pPntCnt->getDepends(iDependOn);
	}
	if ( pX_initial != NULL )// X_INITIAL 
	{
		pX_initial->getDepends(iDependOn);
	}
	if ( pX_inc != NULL )// X_INCREMENT 
	{
		pX_inc->getDepends(iDependOn);
	}
	if ( pEmphasis != NULL )// EMPHASIS 
	{
		pEmphasis->getDepends(iDependOn);
	}		

/* LineType, Line Color, X & Y Key Points :: Cannot change while running */
}
/** end stevev 16nov05  **/

/* stevev 27sep06 - more dependency extentions */
// get all possible items in this source
void  hCwaveform::fillContainList(ddbItemList_t&  iContain)
{// fill with items-I-contain 
	if ( pXVals != NULL )
	{
		pXVals->fillContainList(iContain);
	}
	if ( pYVals != NULL )
	{
		pYVals->fillContainList(iContain);
	}
}
/** end stevev 27sep06 **/

/** added stevev 05oct06 **/
// called when an item in iDependOn has changed in the graph (called from Graph's notification)
//		Since a waveform is merely an attribute grouping for graphs, we only notify the graph
int hCwaveform::notification(hCmsgList& msgList,bool isValidity, hCgraph* pParent)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	hCitemBase* pItem;
	
	hCitemBase::notification(msgList,isValidity);// do validity and label
/* note to self - 05oct06 - cannot filter on isConditional if any part of the payload is a 
							reference or expression */
	// re-evaluate all the others 
	if (! isValidity) 
	{
		if (pHandling != NULL && pHandling->isConditional() && pHandling->reCalc())//bitstring
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pHandling->getGenericType());// tell the world
		}// else - no change;

		if (pEmphasis != NULL && pEmphasis->isConditional() && pEmphasis->reCalc())// long
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pEmphasis->getGenericType());// tell the world
		}// else - no change;

		if (pXVals != NULL /*&& pXVals->isConditional()*/ && pXVals->reCalc())//reflist
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pXVals->getGenericType());// tell the world
		}// else - no change;

		if (pYVals != NULL /*&& pYVals->isConditional()*/ && pYVals->reCalc())//reflist
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pYVals->getGenericType());// tell the world
		}// else - no change;

		if (pPntCnt != NULL /*&& pPntCnt->isConditional()*/ && pPntCnt->reCalc())// expression
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pPntCnt->getGenericType());// tell the world
		}// else - no change;

		if (pX_initial != NULL /*&& pX_initial->isConditional()*/ && pX_initial->reCalc())//expression
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pX_initial->getGenericType());// tell the world
		}// else - no change;

		if (pX_inc != NULL /*&& pX_inc->isConditional()*/ && pX_inc->reCalc())// expression
		{// my handling changed
			msgList.insertUnique(pParent->getID(), mt_Str, pX_inc->getGenericType());// tell the world
		}// else - no change;
		
		if (pYaxis != NULL )
		{
			if ( pYaxis->reCalc())//axis item
			{// my axis changed
				msgList.insertUnique(pParent->getID(), mt_Str, pYaxis->getGenericType());
			}// else - no change;


			CValueVarient v = pYaxis->getValue();
			if ( v.vType == CValueVarient::isSymID && v.vIsValid )
			{	if ( devPtr()->getItemBySymNumber((unsigned int)v, &pItem) == SUCCESS && 
																					pItem != NULL)
				((hCaxis*) pItem)->notification(msgList, isValidity, pParent); 
			}
		}
	}
	return (msgList.size() - init);// number added
}
/** end stevev 05oct06  **/
