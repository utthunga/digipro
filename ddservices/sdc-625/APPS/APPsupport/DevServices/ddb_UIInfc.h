/*************************************************************************************************
 *
 * $Workfile: ddb_UIInfc.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the interface to the outside world - must be supplied by others -
 *		
 * #include "ddb_UIInfc.h"		
 */

#ifndef _DDB_UI_INTERFACE_H
#define _DDB_UI_INTERFACE_H

#include <stdarg.h>
#include <stdio.h>

// external references
//extern void LogMessageV(char *strMessage,va_list varArgList);
/*  future  */
/*
extern void CriticalError (char *strMessage,va_list varArgList);// system abort notice
extern void UserAckMessage(char *strMessage,va_list varArgList);// dialog with 'OK' button
extern  int UserCancelMsg (char *strMessage,va_list varArgList);// dialog with 'OK'|'Cancel'
*/



#endif //_DDB_UI_INTERFACE_H


/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
