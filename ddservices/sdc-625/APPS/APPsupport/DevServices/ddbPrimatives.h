/*************************************************************************************************
 *
 * $Workfile: ddbPrimatives.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the various primative classes
 *		8/12/2	sjv	created from ddlstring.h
 *
 * #include "ddbPrimatives.h"
 */


#ifndef DDBPRIMATIVES_H
#define DDBPRIMATIVES_H

#include <string>

#include "ddbConditional.h"
#include "DDBpayload.h"
#include "ddbGlblSrvInfc.h"

#include "dllapi.h"

#include <stdlib.h>		// for '_max'

#include "ddbdefs.h"    // for MAX_LEGAL_STRING_LEN

//#define LOG_DFLT_STR_FILL 1 /* define to log the dictionary string fill */

/*  
 *  DEFAULT STRINGS
 */
#define DFLT_DICT                 400
#define DFLT_DICT_STR				0
#define DEFAULT_STD_DICT_STRING         (unsigned long)((400 << 16) + 0) /*[      400,   0]	0x 1900000	dict_str_not_found
                                                                         							"|en|ERROR: Dict String Not Found"	*/
#define DFLT_DEVSPEC_STR			1
#define DEFAULT_DEV_SPEC_STRING          (unsigned long)((400 << 16) + 1)/*[      400,   1]	0x 1900001	lit_str_not_found
                                                                         							"|en|ERROR: Lit String Not Found"*/
#define DFLT_HELP_STR				2
#define DEFAULT_STD_DICT_HELP           (unsigned long)((400 << 16) + 2) /*[      400,   2]	0x 1900002	default_help
                                                                         							"|en|No Help Available"*/
#define DFLT_LABEL_STR				3
#define DEFAULT_STD_DICT_LABEL          (unsigned long)((400 << 16) + 3) /*[      400,   3]	0x 1900003	default_label
                                                                         							"|en|No Label Available"*/
#define DFLT_DESC_STR				4
#define DEFAULT_STD_DICT_DESC           (unsigned long)((400 << 16) + 4) /*[      400,   4]	0x 1900004	default_desc
                                                                         							"|en|No Description Available"*/
#define DFLT_DISP_INT_STR			5
#define DEFAULT_STD_DICT_DISP_INT       (unsigned long)((400 << 16) + 5) /*[      400,   5]	0x 1900005	default_display_format_int
                                                                         							"|en|d"*/
#define DFLT_DISPUINT_STR			6
#define DEFAULT_STD_DICT_DISP_UINT      (unsigned long)((400 << 16) + 6) /*[      400,   6]	0x 1900006	default_display_format_uint
                                                                         							"|en|u"*/
#define DFLT_DISPFLT_STR			7
#define DEFAULT_STD_DICT_DISP_FLOAT     (unsigned long)((400 << 16) + 7) /*[      400,   7]	0x 1900007	default_display_format_float
                                                                         							"|en|f"*/
#define DFLT_DISPDBL_STR			8
#define DEFAULT_STD_DICT_DISP_DOUBLE    (unsigned long)((400 << 16) + 8) /*[      400,   8]	0x 1900008	default_display_format_double
                                                                         							"|en|f"*/
#define DFLT_EDIT_INT_STR			9
#define DEFAULT_STD_DICT_EDIT_INT       (unsigned long)((400 << 16) + 9) /*[      400,   9]	0x 1900009	default_edit_format_int
                                                                         							"|en|d"*/
#define DFLT_EDITUINT_STR			10
#define DEFAULT_STD_DICT_EDIT_UINT      (unsigned long)((400 << 16) + 10)/*[      400,  10]	0x 190000a	default_edit_format_uint
                                                                         							"|en|u"*/
#define DFLT_EDITFLT_STR			11
#define DEFAULT_STD_DICT_EDIT_FLOAT     (unsigned long)((400 << 16) + 11)/*[      400,  11]	0x 190000b	default_edit_format_float
                                                                         							"|en|f"*/
#define DFLT_EDITDBL_STR			12
#define DEFAULT_STD_DICT_EDIT_DOUBLE    (unsigned long)((400 << 16) + 12)/*[      400,  12]	0x 190000c	default_edit_format_double
                    
// 13 is reserved...??? unlucky i guess
                                             							"|en|f"*/
#define DFLT_TIMEFMT_STR			14
#define DEFAULT_STD_DICT_TIME_FORMAT    (unsigned long)((400 << 16) + 14)/*[      400,  13]	0x 190000e	default_time_format
                                                                 							"|en|T"*/

#define DEFAULT_STD_DICT_MAXDFLT		14



 /* originally defined in dictDB.h (still there)  */
#define DICTALL     0  /* unknown or all dictionary types        */
#define DICTSTND    1  /* the standard dictionary          KEY   */
#define DICTSUPP    2  /* the supplemental standards       KEY   */
#define DICTDDSTR   3  /* multilanguage dd strings         KEY   */
#define DICTALLST   4  /* all standards..standard+supplemental   */

#define DFLT_LANG           L"en"
#define USEDEFAULT          L"USEDFLT"
// moved to ddbdefs.h 10nov08 
// #define MAX_LEGAL_STRING_LEN  1024  /*was 512 */  /* was: 256*/


/*
 * 	STRING tags
 */

#define DEV_SPEC_STRING_TAG     0	/* string device specific id - LITERAL_STRING in the tokenizer */
#define VARIABLE_STRING_TAG     1	/* string variable id */
#define ENUMERATION_STRING_TAG  2	/* enumeration string information */
#define DICTIONARY_STRING_TAG   3	/* dictionary string id */
#define VAR_REF_STRING_TAG      4	/* variable_reference_string */
#define ENUM_REF_STRING_TAG     5	/* enumerated reference string */


class hCddlString : public hCobject, public hCpayload
{
#define USE_DFLT_VALUE    0x8000	/* a flags flag */
#define THE_DEFAULT_VALUE	L""		/*"d-DEFAULT-d"*/
#define	DONT_FREE_STRING	0X00    /* flags value */
#define	FREE_STRING			0X01    /* flags value */
#define ISEMPTYSTRING       0X10

#define PLACEHOLDER       0x4000  /* a flags flag */
#define ISPLACEHOLDER      (flags & PLACEHOLDER)
#define SETPLACEHOLDER     flags|=PLACEHOLDER
#define CLRPLACEHOLDER     flags&=~PLACEHOLDER

	//const char     *str;	/* the pointer to the string */
	wstring          theStr;

	unsigned short  len;	/* the length of the string */
	unsigned short  flags;	/* memory allocation flags *///5/29/02 -added a use-default flag
	unsigned long   strTag;

    // STRING;
	DD_Key_t        ddKey;  /* in case you wanna get another language*/
	itemID_t        ddItem;

	unsigned long   enumVal; // which

	hCreference*     pRef;	/* rarely needed, allocate as required */
	//modified 6/5/03 so that this class always owns the reference(will be a copy);
	string          retString;//stevev 02jan08 - added to return a narrow version
	wstring		    retStringW;
public:
	hCddlString(DevInfcHandle_t h);
	hCddlString (DevInfcHandle_t h, unsigned long Tag, unsigned int ddi, 
		unsigned long ev=0,unsigned int ddk=0, unsigned short flg=0)
	  :hCobject(h),	strTag(Tag),ddItem(ddi),enumVal(ev),ddKey(ddk),flags(flg)
	{len=0;pRef=NULL;	
#ifdef _DEBUG_ALLOC
		clog<<"   String new     0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	} ;

	ddlstringType_t    getStrType() { return (ddlstringType_t)strTag;};
	hCddlString( const hCddlString& src ) : hCobject(src.devHndl())
	{	theStr  = src.theStr;
		len     = src.len;
		flags   = src.flags;
		strTag  = src.strTag;
		ddKey   = src.ddKey; 
		ddItem  = src.ddItem;
		enumVal = src.enumVal;
		if (src.pRef != NULL)
		{
			// stevev 26apr07 - try to be more consistant in our construction/destruction
			//pRef    = new hCreference(src.devHndl());
			//*pRef   = *src.pRef;
			pRef = new hCreference(*src.pRef);
		}
		else
		{
			pRef = NULL;
		}	 
#ifdef _DEBUG_ALLOC
		clog<<"   String newCopy 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&src) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	};
	// make this one a dup of src
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0) 
	{//									@ true, make final item a new psuedo-instance		
#ifdef _DEBUG
		if (! pPayLd->validPayload(pltddlStr))
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: ddlString  asked to duplicate a NON ddlString class.\n");
			return;
		}
#endif

		hCddlString* pS = (hCddlString*)pPayLd;	
		theStr  = pS->theStr;
		len     = pS->len;
		flags   = pS->flags;
		strTag  = pS->strTag;
		ddKey   = pS->ddKey; 
		ddItem  = pS->ddItem;
		enumVal = pS->enumVal;
		if (pS->pRef != NULL)
		{
			pRef    = new hCreference(pS->devHndl());
			pRef->duplicate(pS->pRef,false,parentID);// this is a reference to an existing var
		}
		else
		{
			pRef = NULL;
		}	 
#ifdef _DEBUG_ALLOC
		clog<<"   String duplicate 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&pS) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	};

	hCddlString& operator=(const aCddlString& src ) 
	{	if (src.theStr.c_str() != NULL)
			theStr  = (src.theStr.c_str());// force a copy to this memory space
		else
			//theStr.erase();
			theStr = L"";
		len     = src.len;
		/* TEMPORARY */
		if ( len == 1 && theStr == L" "/*.size() == 0*/ )
		{
			DEBUGLOG(CLOG_LOG,"MT string{debug only msg}\n");
		}
		/* end TEMP */
		flags   = src.flags;
		strTag  = src.strTag;
		ddKey   = src.ddKey; 
		ddItem  = src.ddItem;
		enumVal = src.enumVal;
		if (src.pRef != NULL)
		{
			if (pRef) delete pRef;     // get rid of existing 
			pRef    = new hCreference(devHndl());
			*pRef   = *(src.pRef);
		}
		else
		{
			pRef = NULL;
		}
		if ( theStr == L"d-DEFAULT-d" )
		{
			theStr = L"";
			flags |= ISEMPTYSTRING;
		}
		else if ( theStr == L"" || theStr.length() == 0 )
		{
			flags |= ISEMPTYSTRING;
		}
#ifdef _DEBUG
if ( len > 0x400 || theStr.length() > 0x400 )
{
	LOGIT(CERR_LOG,"DDLSTRING LENGTH(%d) EXCEEDS LONG(%d){debug only msg}\n",
#if defined(__GNUC__)
											std::max(size_t(len),theStr.length()),
#else
											max(size_t(len),theStr.length()),
#endif // __GNUC__
											0x400);
}
#endif
		if (len > MAX_LEGAL_STRING_LEN             || 
			theStr.length() > MAX_LEGAL_STRING_LEN )
		{
			LOGIT(CERR_LOG,"DDLSTRING LENGTH (%d) EXCEEDS MAXIMUM(%u)\n",
#if defined(__GNUC__)
				std::max(size_t(len),theStr.length()), 
#else
				max(size_t(len),theStr.length()), 
#endif // __GNUC__
				MAX_LEGAL_STRING_LEN );
		}
		return *this;
	};
	hCddlString& operator=(const hCddlString& src ) 
	{	 
#ifdef _DEBUG_ALLOC
		if ( src.len == 0xfeee )
		{
		clog<<"   String opEqual 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&src) <<dec<<" *** Error ***********************"<<" ( "
			<<__LINE__<<" )"<<endl;
		return *this;
		}
		if ( ((unsigned int)src.pRef) == 0xfdfdfdfd )
		{
		clog<<"   String opEqual 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&src) <<dec<<" *** Error mem OverRun ***********"<<" ( "
			<<__LINE__<<" )"<<endl;
		return *this;
		}
#endif	
		
		if (src.theStr.c_str() != NULL)
		{
			//theStr  = (src.theStr.c_str());// force a copy to this memory space
			theStr = src.theStr;
		}
		else
			//theStr.erase();
			theStr = L"";
		len     = src.len;
		flags   = src.flags;
		strTag  = src.strTag;
		ddKey   = src.ddKey; 
		ddItem  = src.ddItem;
		enumVal = src.enumVal;
		if (src.pRef != NULL)
		{
			if (pRef) delete pRef;     // get rid of existing 
			pRef    = new hCreference(devHndl());
			*pRef   = *src.pRef;
		}
		else
		{
			pRef = NULL;
		}
		return *this;
	};
	virtual ~hCddlString();
	RETURNCODE destroy(void)
	{ 
/*Vibhor 050440:Start of Code*/
	try{
			if (pRef != NULL) // split out for debugging PAW 12/08/09
			{ 
				pRef->destroy();
			}
			RAZE(pRef); 
		  //theStr.~string();// 26apr07 - don't ~string for now...
			theStr.erase();
	}
	catch(...)
	{
		LOGIT(CERR_LOG,L"RETURNCODE  hCddlString::destroy(void) inside catch(...)!!\n");
	}
	 return SUCCESS;
/*Vibhor 050440:End of Code*/
	};

	// overload the virtual base method(s)
	void setEqual(void* pAclass){setEqual(pAclass,0);};
	void setEqual(void* pAclass, unsigned long dfltDictStr )
	{
		aCddlString* pAstr = (aCddlString*)pAclass;
		operator=(*pAstr);
		if ( len != theStr.size() && strTag == 0 && dfltDictStr != 0)
		{
			fillStr(dfltDictStr);
			len = theStr.size();
		}
		else
		if ( theStr.size() == 0 && (flags & ISEMPTYSTRING) == 0)// not designated empty
		{
#ifdef LOG_DFLT_STR_FILL /* defined at the top of this file */
			clog<< "    filling empty string with dictionary string.  len ="<<len<<" tag="
				<<strTag<<" String# 0x"<<hex<<dfltDictStr<<dec<<endl;
#endif
			fillStr();
		}
	};
	bool validPayload(payloadType_t ldType){ if (ldType != pltddlStr) return false; else return true;};


	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//this must convrt to narrow before dump
	
		
		if (theStr.size() > 0 && ! ISPLACEHOLDER ) 
		{
			string tmp = TStr2AStr(theStr);
			LOGIT(COUT_LOG,"\"%s\"\n",tmp.c_str());// needs a \n
		}
		else 
		{	fillStr();
			string tmp = TStr2AStr(theStr);
			LOGIT(COUT_LOG,"-\"%s\"\n",theStr.c_str());
		}
	    return SUCCESS;
	};
	RETURNCODE fillStr(ulong dictStringNumber);
	static
	wstring    getStandardStr(char* enum_Name, ulong enumValue);

	RETURNCODE setStr(wstring& newStr, unsigned long Tag=0, unsigned int ddi=0, 
								unsigned long ev=0,  unsigned int ddk=0, unsigned short flg=0)
	{	strTag = Tag; ddItem = ddi; enumVal=ev;ddKey=ddk;flags=flg;
		theStr = newStr;len=theStr.length();return SUCCESS;
	} ;
	bool isEmpty(void)
	{	return(ddKey==0 && ddItem == 0 && enumVal == 0 && pRef == NULL);  };//is0len||isMissing
	bool is0len(void);
	bool isMissing(void);
	bool noString(void){return (len==0 && theStr.size()==0);};
	int getTag(void) { return ((int)strTag);};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	wstring& procureVal(/*CcommAPI* pComm,*/ unsigned int iID = 0) 
	{ 
		if (theStr.size() <= 0 || ISPLACEHOLDER)
		{	
/*Vibhor 051203: Start of Code*/
			if(!(DEV_SPEC_STRING_TAG == strTag))
			{
				fillStr(/*pComm, */ iID);
			}
			else
			{
				theStr = L"d-DEFAULT-d";
			}
/*Vibhor 051203: End of Code*/
		}
		if ( theStr == L"d-DEFAULT-d" )
		{	theStr = L"";	}// was theStr.erase();		}
		retStringW        = theStr;
		return retStringW; 
	};

	//operator wstring()			{ fillStr(); return theStr;};
	operator wstring()			{  return theStr;};
	
	// stevev - WIDE2NARROW char interface
	// stevev - all wide now...operator string& ()	{ return ( retString=TStr2AStr( procureVal() )); };

	void clear(void) 
	{
		//theStr.erase();
		theStr = L"";
		len    = 0;
		flags  = 0;
		strTag = 0;
		ddKey  = 0;
		ddItem = 0;
		enumVal= 0;
		//RAZE( pRef );
		
		if(pRef!=NULL)
		{ 
			delete pRef; 
			pRef = NULL;
		}
	};	

//protected:
	RETURNCODE fillStr(/*CcommAPI* pComm = NULL,*/ unsigned int iID = 0);
};

class hCbitString  : public hCpayload, public hCobject
{
// L&T Modifications : ReadNWriteSupport - start
/* The below data member has been made public to access this
qualifier from SDCW class. This was private earlier. */
public:
	ulong bitstringVal;
// L&T Modifications : ReadNWriteSupport - end

public:
	hCbitString(DevInfcHandle_t h) : hCobject(h) 
	{ 
		bitstringVal = 0; 
#ifdef _DEBUG_ALLOC
		clog<<"Bitstring new     0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	};
	hCbitString(const hCbitString& src) : hCobject( src.devHndl() ) 
	{ 
		bitstringVal = src.bitstringVal; 
#ifdef _DEBUG_ALLOC
		clog<<"Bitstring newCopy 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&src) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	};
	void setDuplicate(hCbitString* pSrc)
	{	bitstringVal = pSrc->bitstringVal;  };


	// make this one a dup of src
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0) 
	{
		hCbitString* pS = (hCbitString*)pPayLd;			
#ifdef _DEBUG
		if (! pPayLd->validPayload(pltbitStr))
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: ddlBitString asked to duplicate a NON ddlBitString class.\n");
			return;
		}
#endif
		bitstringVal    = pS->bitstringVal; 
#ifdef _DEBUG_ALLOC
		clog<<"Bitstring duplicate 0x"<<hex<<((unsigned int)this) <<"  from  0x"
			<<((unsigned int)&pS) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	};
	RETURNCODE destroy(void) 
	{ 
		bitstringVal=0; 
		return SUCCESS;
	};
	virtual ~hCbitString()
	{ 
		destroy(); 
#ifdef _DEBUG_ALLOC
		clog<<"Bitstring destrct 0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
	}

	// overload the virtual base method(s)
	void setEqual(void* pAclass)
	{
		aCbitString* pAstr = (aCbitString*)pAclass;
		bitstringVal = pAstr->bitstringVal;
	};
	bool validPayload(payloadType_t ldType){ if (ldType != pltbitStr) return false; else return true;};


	hCbitString operator=(const hCbitString& src)
	{
		bitstringVal = src.bitstringVal;
#ifdef _DEBUG_ALLOC
		if ( src.bitstringVal == 0xfeefee )
		{
		clog<<"Bitstring freed!! 0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
		}
#endif
		return *this;
	};
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		{ LOGIT(COUT_LOG,"Bitstring is 0x%x\n", bitstringVal);return SUCCESS;};
	void clear(void) { bitstringVal= 0; };
	// accessors
	void  setBitStr(ulong bs) {bitstringVal = bs;};
	ulong getBitStr(void)     {return bitstringVal;};
	RETURNCODE getDescription(int oSet = 0);// {cerr << "____stubout___CbitString" << endl;return FAILURE;};
};


class hCddlLong  : public hCpayload, public hCobject
{
	// stevev 27aug10..convert to long long values   ulong ddlLong;
	UINT64  ddlLong;
public:
#ifdef _DEBUG
	bool is1;
#endif
	hCddlLong(DevInfcHandle_t h):hCobject(h) 
	{ ddlLong = 0;
#ifdef _DEBUG
	is1 = false;
#endif
	};
	hCddlLong(const hCddlLong& src) : hCobject( src.devHndl() ) 
	{ ddlLong = src.ddlLong;
#ifdef _DEBUG
	   is1 = false;
#endif
	};
	virtual ~hCddlLong(){
#ifdef _DEBUG
		if (is1)
			_CrtDbgBreak();
#endif
	};
	RETURNCODE destroy(void) {return SUCCESS;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		{ /*COUTSPACE << "ddlLong is 0x" << hex << ddlLong << dec << endl;*/
		LOGIT(COUT_LOG,"    ddlLong is 0x%x\n",ddlLong); return SUCCESS;};

	void clear(void) { ddlLong= 0; };
	// accessors
	// stevev 27aug10..convert to long long values   ulong ddlLong;
	//void  setDdlLong(ulong bs) {ddlLong = bs;};
	//ulong getDdlLong(void)     {return ddlLong;};
	void  setDdlLong(UINT64 bs) {ddlLong = bs;};
	UINT64 getDdlLong(void) const    {return ddlLong;};
	hCddlLong& operator=( hCddlLong& s){setDdlLong(s.getDdlLong());return *this;};
	hCddlLong& operator=( ulong bs ){setDdlLong(bs);return *this;};
	operator bool() const { return (ddlLong==0ll)?false:true;};

	void setEqual(void* pAclass) // a required method
	{	if (pAclass) 
		//	((aCddlLong*)pAclass)->ddlLong = ddlLong;/*else-nop*/
		ddlLong = ((aCddlLong*)pAclass)->ddlLong;/*else-nop*/
	};

	void setDuplicate( hCddlLong *pLng ) { ddlLong = pLng->ddlLong; };
	// make this one a dup of src...parentID should not be needed here
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0) 
	{//									@true - make final element a new instance
#ifdef _DEBUG
		if (! pPayLd->validPayload(pltddlLong))
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: ddlLong  asked to duplicate a NON ddlLong class.\n");
			return;
		}
#endif
		hCddlLong* pS = (hCddlLong*)pPayLd;	
		ddlLong       = pS->ddlLong;
	};
	bool validPayload(payloadType_t ldType){ if (ldType != pltddlLong) return false; else return true;};
};


class hCddlIntWhich : public hCddlLong
{
	int ddlwhich;

public:
	hCddlIntWhich(DevInfcHandle_t h):hCddlLong(h) { ddlwhich = 0; };
	hCddlIntWhich(const hCddlIntWhich& src) : hCddlLong( src ) { ddlwhich = src.ddlwhich;};
	RETURNCODE destroy(void) {return SUCCESS;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		{ /* COUTSPACE; */ LOGIT(COUT_LOG,"\t");
			if (typeName)	LOGIT(COUT_LOG,"%s",typeName);
			LOGIT(COUT_LOG," ddlInt is 0x%x ddlWhich is 0x%x\n",hCddlLong::getDdlLong(),ddlwhich);
			return SUCCESS;};

			void clear(void) { hCddlLong::setDdlLong(0); };
	// accessors
	void  setDdlLong(ulong bs, int wh) {hCddlLong::setDdlLong(bs);ddlwhich=wh;};
	void  setDdlLong(ulong bs) {hCddlLong::setDdlLong(bs);};
	// stevev 27aug10..convert to long long values   ulong ddlLong;
	//ulong getDdlLong(int& retWhich)     {retWhich=ddlwhich; return hCddlLong::getDdlLong();};
	//ulong getDdlLong(void)              {return hCddlLong::getDdlLong();}
	UINT64 getDdlLong(int& retWhich) const    {retWhich=ddlwhich; return hCddlLong::getDdlLong();};
	UINT64 getDdlLong(void)  const            {return hCddlLong::getDdlLong();}

	int  getWhich(void){return ddlwhich;};
	hCddlIntWhich& operator=( const hCddlIntWhich& s)
	// stevev 27aug10..convert to long long values   ulong ddlLong;
	//{ulong t; t = s.getDdlLong(ddlwhich); hCddlLong::setDdlLong(t);return *this;};
	{UINT64 t; t = s.getDdlLong(ddlwhich); hCddlLong::setDdlLong(t);return *this;};

	void setEqual(void* pAclass) // a required method
	{	if (pAclass) 
		{	hCddlLong::setDdlLong(((aCintWhich*)pAclass)->value);
			ddlwhich = ((aCintWhich*)pAclass)->which;/*else-nop*/ }
	};
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parent = 0)
	{
#ifdef _DEBUG
		if (! pPayLd->validPayload(pltIntWhich))
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: ddlIntWhich  asked to duplicate a NON intWhich class.\n");
			return;
		}
#endif
		hCddlLong::duplicate(pPayLd, replicate);
		ddlwhich = ((hCddlIntWhich*)pPayLd)->ddlwhich;
	};
	bool validPayload(payloadType_t ldType)
	{if(ldType != pltIntWhich) return false; else return true;};
};

#endif//DDBPRIMATIVES_H

/*************************************************************************************************
 *
 *   $History: ddbPrimatives.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
