/*

critSect.cpp 

for HON'd need of a OS critical section semaphore

*/



#include "stdafx.h"

#define _CRITSECT_CPP

#include "critSect.h"

#undef _CRITSECT_CPP

CRITICAL_SECTION* pcsLstCS = 0;


#include < basetsd.h >
#include < vector >
using namespace std;

static vector<CRITICAL_SECTION*> csList;// static - as in available here ONLY

long csGetCurrentThreadId(void)
{
	return GetCurrentThreadId ();
}

unsigned long ddbGetTickCount()
{
	return GetTickCount();
}


csCriticalSection::csCriticalSection() : csIdx(0)
{
	// first one in sets the list critical section
	if (pcsLstCS == NULL)
	{
		pcsLstCS = new CRITICAL_SECTION;
		InitializeCriticalSection(pcsLstCS);
	}
	// else - no-op
}

csCriticalSection::~csCriticalSection()
{
	//DeleteCriticalSection(csList[csIdx]);
	DeleteCS();
	EnterCriticalSection(pcsLstCS);
	csList[csIdx] = NULL;
	csIdx = 0;

	for ( int k = 0; k < csList.size(); k++ )
	{
		if ( csList[k] != NULL )
		{
			LeaveCriticalSection(pcsLstCS);
			return;
		}
	}
	// last one out turns off the lights
	LeaveCriticalSection(pcsLstCS);
	DeleteCriticalSection(pcsLstCS);
	pcsLstCS = NULL;
}

void csCriticalSection::Leave()
{
	if(csList[csIdx] != NULL)
	{
		LeaveCriticalSection(csList[csIdx]);
	}
}
void csCriticalSection::Enter()
{
	if(csList[csIdx] != NULL)
	{
	EnterCriticalSection(csList[csIdx]);
	}
}
void csCriticalSection::Init ()
{
	if(csIdx == 0 || csList[csIdx] == NULL)
	{
		EnterCriticalSection(pcsLstCS);
		CRITICAL_SECTION* pcs = new CRITICAL_SECTION;
	
		if ( ! csIdx )
		{	
			csIdx = csList.size();
			csList.push_back(pcs);
		}
		else 
		{
			csList[csIdx] = pcs;
		}
		
		//csIdx = csList.size();
		InitializeCriticalSection(pcs);
		LeaveCriticalSection(pcsLstCS);
	}
}

void csCriticalSection::DeleteCS()
{
	DeleteCriticalSection(csList[csIdx]);
	
	EnterCriticalSection(pcsLstCS);
	
	csList[csIdx] = NULL;
//	csIdx--;	
	// last one out turns off the lights
	LeaveCriticalSection(pcsLstCS);

}




