/*************************************************************************************************
 *
 * $Workfile: ddbAttrEnum.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		Enum specific classes
 *		
 *
 *		
 * #include "ddbAttrEnum.h"
 */

#ifndef _DDBATTRENUM_H
#define _DDBATTRENUM_H

#include "ddbVar.h"

// make this one global so others may use it
//class  hCenumDesc;
//class  hCenumList;
typedef hCcondList<hCenumList>	condEnumList_t;

/******************** ENUMERATED **************************/
/*  notice that this is not at a heirarchy described in the language spec */
class hCattrEnum : public hCattrBase
{
	condEnumList_t condEnumListOlists; // of CenumDesc

protected:
	RETURNCODE fetch(void);

public:
	// there is no 'default constructor...we MUST have the handle
	hCattrEnum(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		: hCattrBase(h,pIB,varAttrEnums),  condEnumListOlists(h) {};
	// copy constructor
	hCattrEnum(const hCattrEnum& sae)
		: hCattrBase(sae), condEnumListOlists(sae.devHndl())
		{ condEnumListOlists = sae.condEnumListOlists;};
	// abstract constructor (from abstract class)
	hCattrEnum(DevInfcHandle_t h, const aCattrEnum* aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrEnums), condEnumListOlists(h)
		{ condEnumListOlists.setEqualTo( (void*) &(aCa->condEnumListOlists));};
		//{ condEnumListOlists = aCa->condEnumListOlists;};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){return (condEnumListOlists.destroy());};

	virtual ~hCattrEnum()
	{
		condEnumListOlists.destroy();
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
				{/*return condEnumListOlists.dumpSelf(indent,typeName);*/
		         clog<<"    hCattrEnum.dumpSelf has no body!" << endl;  return FAILURE; };


	RETURNCODE setDefaultVals(RETURNCODE rrc)
				{ //	condEnumListOlists.clear(); // make sure the size is zero
					return SUCCESS;
				};

//	RETURNCODE insertSelf(int iSet = 0 ) INSERTATTRGENERIC(condEnumListOlists,ATTR2CONDLIST);
//	RETURNCODE insertFromCond(void) { return insertSelf(COND2PAYENUM);};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrEnum");};
//		{return condEnumListOlists.getDescription(ATTR2CONDLIST);};// set is expected to be ATTR2COND but we make it right
	RETURNCODE aquireDescList(vector<hCenumDesc*>& enumPtrList);

//	void* procure(void) { cerr << "ERROR: hCattrEnum.procure() needs a body."<<endl;};
#if 0
	void* procure(void) { return ( (void*)procureList( (hCcondList<hCpayload>*) &condEnumListOlists) ); };
#endif
};

#endif // _DDBATTRENUM_H

/*************************************************************************************************
 *
 *   $History: ddbAttrEnum.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
