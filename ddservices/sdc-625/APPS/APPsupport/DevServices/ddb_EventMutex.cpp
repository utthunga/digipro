/*************************************************************************************************
 *
 * $Workfile: ddb_EventMutex.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the code for a OS resources needed by the library 
 *		 
 * IMPORTANT NOTE: For some reason Bill doesn't give unique Event handles on CreateEvent.
 *		I modified my debug code to detect this condition and just create another till
 *		he gets it right.
 */

#pragma warning (disable : 4786) 


#if defined(__GNUC__)

#include "linux/event_objects_posix.h"
#define USE_OS

#else // !__GNUC__

//#include <afxmt.h>
#include "stdafx.h"			/*changed from <stdafx.h> J.U. 27jan10 */ 

#endif // __GNUC__


#include <time.h>
#ifndef _WIN32_WCE
// removed PAW 30/04/09 does not exist in Win CE
#include <sys/timeb.h> 
#else
#include "time_CE.h"	// see above PAW 30/04/09
#endif
#include <map>

#include "ddbEvent.h"
#include "ddbMutex.h"
#include "ddbCntMutex.h"
#include "SDCthreadSync.h"


#include "logging.h"

#ifdef TOK
#undef TRACE
#define TRACE	(void)0 
#endif
// define to get mucho logs....
//#define DBG_CNTMUTEX
// or                          #define DBG_MAINMUTEX
// or                          #define LOG_EVENT_HANDLING
// or                          #define DBG_WAIT_SLEEP
//#define DBG_CNTMUTEX
//#define DBG_MAINMUTEX

//#define TST_GLBL
//#define TST_GLBL_OUT

#ifdef TST_GLBL
#include "ddbGeneral.h"
#include <map>
bool inUIthread = false;
typedef map <HANDLE, DWORD> hndlMAP_t;
typedef hndlMAP_t::iterator hmIT_t;
hndlMAP_t hndlMap;

int firstInvalid = 0;
#endif

// also in critSect.cpp if that is included, pick one
unsigned long ddbGetTickCount()
{
	return GetTickCount();
}

#ifdef _DEBUG
THREAD_ID_TYPE hCmutex::fetchCurrentThreadId () //to help in debugging
{
	return Thread_GetCurrentThreadId();
} ;
#endif


//#include <vector>
//using namespace std;

//    designed to encapsulate the OS specific items- linked externally


// static (very local) definition
typedef struct lockItem_s
{
	HANDLE eventHandle;
}
/*typedef*/ lockItem_t;


static vector<lockItem_t> liList;// static - as in available only in this module -
typedef vector<lockItem_t>::iterator	liIT_t;

static DWORD UIthreadID = 0;	 // must be set by the UI when it instaniates 
								//       (via hCevent::setUIthreadID())

//class hCevent	  ********************************************************************************

const unsigned long hCevent::FOREVER = INFINITE;// 0xffffffff;	


hCevent::hCevent()
{
	lockItem_t li, *p_itLI;
	int flag = 1;
	while (flag )
	{
		li.eventHandle = CreateEvent(
			NULL,	// no security
			FALSE,	// auto reset event <resets to unsignaled when a task unblocks
			FALSE,	// initial state is unsignaled
			NULL	// not named, we'll access via handle
			);
	// note that larger systems / or one that runs a long time getting and deleting these
	//		will need to put a scan in here to reuse empty event slots in the list.
		eventHandle = liList.size(); // index we are about to fill
	////clog << "create Event handle "<<eventHandle<<"  "<<hex << li.eventHandle <<dec<<endl;
		flag = 0;//static vector<lockItem_t> liList; at the top of this page
		liIT_t iT;
		for( iT = liList.begin(); iT != liList.end(); ++iT)
		{
			p_itLI = &(*iT);
			//if (iT->eventHandle == li.eventHandle)
			if(p_itLI->eventHandle == li.eventHandle)
			{
				////clog<<"Duplicate Event Handles: "<<hex << li.eventHandle <<dec<<endl;
				flag++;
			}
		}
	}
#ifdef LOG_EVENT_HANDLING
	clog<<"created event "<<eventHandle<<"  for "<<hex<<li.eventHandle<<dec<<endl;
#endif
	liList.push_back(li);
}

hCevent::hCeventCntrl*  
		 hCevent::getEventControl()	// executes constructor 
{
////clog << " get a Event control for "<<eventHandle<<"  "<<hex << liList[eventHandle].eventHandle <<dec<<endl;
#ifdef _DEBUG
	hCevent::hCeventCntrl* pEC = NULL;
	pEC = (hCevent::hCeventCntrl*)new hCeventCntrl(eventHandle);
	return pEC;
#else
	return ( new hCeventCntrl(eventHandle) );
#endif
}

hCevent::~hCevent()
{	// if we remove it then all other handles will be incorrect
#ifdef LOG_EVENT_HANDLING /* top of this file */
clog << "killing Event handle "<<eventHandle<<"  "<<hex << liList[eventHandle].eventHandle <<dec<<endl;
#endif
	CloseHandle(liList[eventHandle].eventHandle);
	liList[eventHandle].eventHandle = NULL;  
}

void hCevent::setUIthreadID(DWORD uiThreadID )
{
#ifdef _DEBUG
	if (UIthreadID)
	{
		TRACE(_T("ERROR:****** UI tread being reset! ******\n"));
	}
#endif
	UIthreadID = uiThreadID;
}

// class hCevent::hCeventCntrl********************************************************************

hCevent::hCeventCntrl::hCeventCntrl(int h):controlHandle(h)
{
	clrResult();
};

RETURNCODE hCevent::hCeventCntrl::pendOnEvent(unsigned long ulTimeOut)// wait for signal
{
////clog <<"pend On Event " << controlHandle << "  "<<hex<< liList[controlHandle].eventHandle <<dec<<endl;
//	DWORD dwWaitResult = WaitForSingleObject( liList[controlHandle].eventHandle, ulTimeOut );
	DWORD dwWaitResult = systemWaitSingleObject( liList[controlHandle].eventHandle, ulTimeOut );

	RETURNCODE rc = SUCCESS;

	if (dwWaitResult == WAIT_OBJECT_0) 
						// has been set to signaled - this is automatic so it is now NONsignaled
	{
		rc = SUCCESS; 
	}
	else 
	if (dwWaitResult == WAIT_TIMEOUT)
						//The time-out interval elapsed, and the object's state is nonsignaled.
	{
////clog<<" -Timeout- "<<controlHandle<<endl;
#ifdef QDBG
		clog << "Timed Out after "<<ulTimeOut <<" milliseconds"<< endl;
#endif

		rc = APP_WAIT_TIMEOUT;
	}
//	else 
//	if (WAIT_ABANDONED)
//The specified object is a mutex object that was not released by the thread that owned the mutex 
//	object before the owning thread terminated. Ownership of the mutex object is granted to the 
//	calling thread, and the mutex is set to nonsignaled. 
//	{
//		rc = APP_WAIT_ABANDONED;
//	}
	else // error
	{
////clog<<" -Error- "<<controlHandle<<endl;
		rc = APP_WAIT_ERROR;
	}

	return rc;
}

/* sjv 25aug05 - just like above but a blocking wait */
RETURNCODE hCevent::hCeventCntrl::waitOnEvent(unsigned long ulTimeOut)// wait for signal
{
	DWORD dwWaitResult = WaitForSingleObject( liList[controlHandle].eventHandle, ulTimeOut );
//	DWORD dwWaitResult = systemWaitSingleObject( liList[controlHandle].eventHandle, ulTimeOut );

	RETURNCODE rc = SUCCESS;

	if (dwWaitResult == WAIT_OBJECT_0) 
						// has been set to signaled - this is automatic so it is now NONsignaled
	{
		rc = SUCCESS; 
	}
	else 
	if (dwWaitResult == WAIT_TIMEOUT)
						//The time-out interval elapsed, and the object's state is nonsignaled.
	{
////clog<<" -Timeout- "<<controlHandle<<endl;
#ifdef QDBG
		clog << "Timed Out after "<<ulTimeOut <<" milliseconds"<< endl;
#endif

		rc = APP_WAIT_TIMEOUT;
	}
//	else 
//	if (WAIT_ABANDONED)
//The specified object is a mutex object that was not released by the thread that owned the mutex 
//	object before the owning thread terminated. Ownership of the mutex object is granted to the 
//	calling thread, and the mutex is set to nonsignaled. 
//	{
//		rc = APP_WAIT_ABANDONED;
//	}
	else // error
	{
////clog<<" -Error- "<<controlHandle<<endl;
		rc = APP_WAIT_ERROR;
	}

	return rc;
}

void hCevent::hCeventCntrl::triggerEvent(void)	// raise signal    
{
////clog <<"trigger Event " << controlHandle<<"  "<<hex<< liList[controlHandle].eventHandle <<dec<<endl;
	if (! SetEvent(liList[controlHandle].eventHandle))
	{
		LOGIT(CERR_LOG,"ERROR: set event failed.\n");
	}
}
void hCevent::hCeventCntrl::clearEvent(void)
{
	ResetEvent(liList[controlHandle].eventHandle);
}
/* note that the system dependent HANDLE is used here while OS_HANDLE is used in the .h file
   The compiler should complain if these two are ever different.  As long as they are the
   same basic type, all will compile and run just fine
 */
HANDLE hCevent::hCeventCntrl::getEventHandle(void)	// allow a wait-multiple 
{
////clog <<"get an Event Control ONLY " << controlHandle << "  "<<hex<< liList[controlHandle].eventHandle <<dec<<endl;
	return (liList[controlHandle].eventHandle);
}


void hCevent::hCeventCntrl::clrResult(void) { resultVal = ere_NoResult;};
void hCevent::hCeventCntrl::setResult(EventResultEnum_t newVal)
{	if (newVal >= ere_NoResult && newVal < ere_PassedLast ) resultVal = newVal;
	else
	{
		LOGIT(CERR_LOG,"ERROR: Event Control setting an illegal result value.\n");
	}
};
hCevent::EventResultEnum_t  hCevent::hCeventCntrl::getResult(void){return resultVal;};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ============================= MUTEX ===========================================================
// designed to encapsulate the OS specific items - linked externally

hCmutex::hCmutex()	: HolderThdID(THREAD_ID_NONE)
{
	lockItem_t li;
	int flag = 1;
	while(flag)
	{
		li.eventHandle = CreateEvent(
			NULL,	// no security
			FALSE,	// auto reset event (so it immediatly becomes unavailable when aquired)
			TRUE,	// initial state is signaled (available)
			NULL	// not named, we'll access via handle
			);

		mutexHandle = liList.size(); // index we are about to fill
		////clog << "create mutex handle "<<mutexHandle<<"  "<<hex << li.eventHandle <<dec<<endl;
		flag = 0;
		for(vector<lockItem_t>::iterator iT = liList.begin(); iT < liList.end();iT++)
		{
			if (iT->eventHandle == li.eventHandle)
			{
				////clog<<"Duplicate Mutex Handles: "<< hex << li.eventHandle <<dec<<endl;
				flag++;
			}
		}
	}
	liList.push_back(li);
}





hCmutex::hCmutexCntrl*  
hCmutex::getMutexControl(char* pNm)	// executes constructor 
{
#ifdef _DEBUG
	string j(pNm);
	hCmutex::hCmutexCntrl* pR = new hCmutexCntrl(mutexHandle, j) ;
#else
	hCmutex::hCmutexCntrl* pR = new hCmutexCntrl(mutexHandle); 
#endif
	pR->pParent =  this;
	return ( pR);
}

// stevev 23aug10 - try to stop 2008 from trashing stack
  
void hCmutex::returnMutexControl(hCmutex::hCmutexCntrl* pmc)	// executes destructor 
{
	pmc->pParent =  0;
	delete pmc;
	return;
}



hCmutex::~hCmutex()
{	// if we remove it then all other handles will be incorrect
////clog << "killing mutex handle "<<mutexHandle<<"  "<<hex << liList[mutexHandle].eventHandle <<dec<<endl;
	CloseHandle(liList[mutexHandle].eventHandle);
	liList[mutexHandle].eventHandle = NULL;  
}



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ============================= MUTEX CONTROL ===================================================
// class hCmutex::hCeventCntrl********************************************************************

#ifdef _DEBUG
hCmutex::hCmutexCntrl::hCmutexCntrl(int h, string nm):controlHandle(h),pParent(NULL){userName=nm;};
#else
hCmutex::hCmutexCntrl::hCmutexCntrl(int h):controlHandle(h),pParent(NULL){};
#endif


RETURNCODE hCmutex::hCmutexCntrl::aquireMutex(unsigned long ulTimeOut  // wait for signal this long
#ifndef _DEBUG
	)
#else
	, char* fileStr, int lineNum)
#endif
{	// waits till available
////clog <<"aquire mutex " << controlHandle << "  "<<hex<< liList[controlHandle].eventHandle <<dec<<endl;
#ifdef DBG_MAINMUTEX
#ifdef _DEBUG
LOGIT(CLOG_LOG,"&Acquiring for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x for %s\n",
				controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId(),userName.c_str());
#else
LOGIT(CLOG_LOG,"&Acquiring for 0x%04x cntl with Hndl == 0x%04x\n",
												controlHandle,liList[controlHandle].eventHandle);
#endif//debug
#endif
	if ( liList.size() == 0 || liList.size() < (unsigned int)controlHandle) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		return APP_WAIT_ABANDONED;
	DWORD dwWaitResult = 0;
	RETURNCODE rc = SUCCESS;
#ifdef _DEBUG
	// stevev - safety debugging added - 19dec05
	if ( ulTimeOut == hCevent::FOREVER && Thread_IsThreadSame(Thread_GetCurrentThreadId(), pParent->HolderThdID) )
	{
		LOGIT(CERR_LOG,"ERROR: Programmer error - Mutex Deadlock Detected.\n");
		// return APP_WAIT_ABANDONED;
	}
	else // stevev 23feb07
	// end stevev 19dec05
#endif
	if (!Thread_IsThreadSame(Thread_GetCurrentThreadId(), pParent->HolderThdID))// stevev 08apr10- bug 3034 subsequent error
	{
		dwWaitResult = WaitForSingleObject( liList[controlHandle].eventHandle, ulTimeOut );
//		a mutex is the ultimate protection, we MUST allow the UI to pend on one and block!!!!!!!!
//		DWORD dwWaitResult = systemWaitSingleObject( liList[controlHandle].eventHandle, ulTimeOut );
		rc = SUCCESS;
	}
	else// stevev 08apr10- bug 3034 subsequent error - added to avert deadlock @ reread criticals & notify
		dwWaitResult = WAIT_TIMEOUT;

	if (dwWaitResult == WAIT_OBJECT_0) 
						// has been set to available - this is automatic so it is now NOTavailable
	{
#ifdef DBG_MAINMUTEX
LOGIT(CLOG_LOG,"@Acquired  for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x\n",
	  controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId());
#endif

		if ( pParent != NULL )
		{
			pParent->HolderThdID = Thread_GetCurrentThreadId();
#ifdef _DEBUG
			strcpy(pParent->HolderFile, fileStr);
			pParent->HolderLine = lineNum;
#endif
		}
	// #else do nothing

		rc = SUCCESS; 
	}
	else 
	if (dwWaitResult == WAIT_TIMEOUT)
						//The time-out interval elapsed, and the object's state is not available.
	{
#ifdef DBG_MAINMUTEX
LOGIT(CLOG_LOG,"@Timed out for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x\n",
	  controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId());
#endif
////clog<<" -Timeout- "<<controlHandle<<endl;
		rc = APP_WAIT_TIMEOUT;
	}
	else 
	/*
		LINUX_PORT - REVIEW_WITH_HCF - Check with Steve - this was just 
		if(WAIT_ABANDONED) instead of (dwWaitResult == WAIT_ABANDONED)
	*/
	if (dwWaitResult == WAIT_ABANDONED)
//The specified object is a mutex object that was not released by the thread that owned the mutex 
//	object before the owning thread terminated. Ownership of the mutex object is granted to the 
//	this thread, and the mutex is set to NOTavailable. 
	{
////clog<<" -Abandoned- "<<controlHandle<<endl;
		rc = APP_WAIT_ABANDONED;

		pParent->HolderThdID = 0;

	}
	else // error
	{
#ifdef DBG_MAINMUTEX
LOGIT(CLOG_LOG,"@@@@ ERROR for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x\n",
	  controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId());
#endif
////clog<<" -Error- "<<controlHandle<<endl;
		rc = APP_WAIT_ERROR;
	}

	return rc;
}

void hCmutex::hCmutexCntrl::relinquishMutex(void)	// make it available   
{
////clog <<"giveup mutex " << controlHandle<<"  "<<hex<< liList[controlHandle].eventHandle <<dec<<endl;
#ifdef DBG_MAINMUTEX
#ifdef _DEBUG
LOGIT(CLOG_LOG,"RELINQUISH for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x for %s\n",
	  controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId(),userName.c_str());
#else
LOGIT(CLOG_LOG,"RELINQUISH for 0x%04x cntl with Hndl == 0x%04x\n",
	  controlHandle,liList[controlHandle].eventHandle);
#endif // debug
#endif
	if ( liList[controlHandle].eventHandle == NULL )
	{
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"RELINQUISH for 0x%04x cntl with Hndl == 0x%04x ThdID=0x%04x for %s "
			" was deleted before it was relinquished.\n",
		controlHandle,liList[controlHandle].eventHandle,Thread_GetCurrentThreadId(),userName.c_str());
#endif
	}
	if (! SetEvent(liList[controlHandle].eventHandle))
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: set event failed relinquishing mutex.\n");
	}
	else
	{
		pParent->HolderThdID   = THREAD_ID_NONE;
#ifdef _DEBUG
		pParent->HolderFile[0] = 0;
		pParent->HolderLine    = 0;
#endif
	}
}

/*************************************************************************************************
 * The counting Mutex class

  CWinThread* AfxGetThread( );
	DWORD CWinThread::m_nThreadID is public
 */

//	THREAD_ID_TYPE activeThreadID;		// current thread id;
//	int   activeEntryCnt;		// count of entries/re-entries
//	vector<hCevent*>  mv_UnusedEvents;	// all events must be in one of these two
//	deque< hCevent*>  mdq_PendingEvts;	// pending events (threads)

#ifdef _DEBUG
hCcountingMutex::hCcountingMutex(char* nm):cntMname(nm)
#else
hCcountingMutex::hCcountingMutex()
#endif
{
	activeThreadID = THREAD_ID_NONE;// none
	activeEntryCnt = 0;
#ifdef _DEBUG
	string snm(cntMname); snm  += "CNTRL";
	char* pC = (char*)snm.c_str();
	pCntMutex = hCntMutex.getMutexControl(pC);
#else
	pCntMutex = hCntMutex.getMutexControl("CntingMutex");
#endif
}

hCcountingMutex::~hCcountingMutex()
{// delete events
	if (mv_UnusedEvents.size())
	{
	}

// sjv - merge - 19feb07 should be good all the time #if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7 - Memory debugging
	if (pCntMutex != NULL)
	{
		delete pCntMutex;
	}
//#endif
}

// gets an usused or new event pointer
hCevent* hCcountingMutex::getNewEvent(void)
{
	hCevent* wev = NULL;

	if ( mv_UnusedEvents.size() == 0 )
	{// allocate a new one
		wev = new hCevent;
	}
	else
	{// get a used one
		wev = mv_UnusedEvents.back();
		mv_UnusedEvents.pop_back();// supposed to remove the last element
		wev->clear();
	}
	return wev;
}


RETURNCODE hCcountingMutex::getCountingMutex(unsigned long ultimeout)
{
	RETURNCODE rc = FAILURE, trc;
	THREAD_ID_TYPE wrkingThreadID;

#ifdef DBG_CNTMUTEX
	THREAD_ID_TYPE wrkingThreadID_deux;
#endif
//+ get Critical Section
	if (pCntMutex == NULL)
	{
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"ERROR: CM*no mutex in getCountingMutex.\n");
#endif
		return APP_CONSTRUCT_ERR;
	}
	rc = pCntMutex->aquireMutex(ultimeout);
	if ( rc != SUCCESS)
	{//<if timeout - exit timeout>
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"ERROR: CM*could not aquire in %d mS within getCountingMutex.\n",ultimeout);
#endif
		return rc;
	}
	
//	get current thread id
/*Vibhor 050404: Start of Code*/
	wrkingThreadID = Thread_GetCurrentThreadId();//AfxGetThread( )->m_nThreadID;
/*Vibhor 050404: End of Code*/	

//	if current thread id == active thread id || active thread id == 0
	if((activeThreadID == THREAD_ID_NONE) || Thread_IsThreadSame(activeThreadID, wrkingThreadID))
	{// I am active or nobody is active
//		inc active count
		activeEntryCnt++;
//		set active thread to current thread id // may set to current value
		activeThreadID = wrkingThreadID;
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM*aquired for thread ID:0x%03x within getCountingMutex.\n",activeThreadID);
#endif
	}
	else // another thread is active
	{

		/*stevev 13apr10 - disallow duplicate pends _ yeah, systemWait will re-enter*/
		for (deque< hCevent*>::iterator iET = mdq_PendingEvts.begin();
			iET < mdq_PendingEvts.end(); iET++ )
		{// iET isa Ptr 2a Ptr 2a hCevent
			if ( (*iET)->threadPendingOn == wrkingThreadID )
			{// we'll be going
				pCntMutex->relinquishMutex();
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"    CM*Duplicate pend was rejected for thread 0x%03x.\n",wrkingThreadID);
#endif
				return APP_WAIT_ERROR;
			}
		}// end stevev 13apr10



//		get new event
		hCevent* pLocEventPtr = getNewEvent();
		if (pLocEventPtr == NULL)
		{
			pCntMutex->relinquishMutex();
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"ERROR: CM*could not aquire a new event within getCountingMutex.\n");
#endif
			return APP_MEMORY_ERROR;
		}
#ifdef _DEBUG
		pLocEventPtr->threadPendingOn = wrkingThreadID;
#endif
		hCevent::hCeventCntrl* pEvtCntrl = pLocEventPtr->getEventControl();
		if (pEvtCntrl == NULL)
		{
			pCntMutex->relinquishMutex();
#ifdef DBG_CNTMUTEX
LOGIF(LOGP_MUTEX)(CLOG_LOG,"ERROR: CM*could not aquire event's control within getCountingMutex.\n");
#endif 
			return APP_MEMORY_ERROR;
		}
#ifdef DBG_CNTMUTEX
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"    CM*My Pend thread:0x:%03x\n",wrkingThreadID);
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"    >>* Active thread:0x:%03x within getCountingMutex.\n",activeThreadID);
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"    >>* My event is:0x%03x\n",pEvtCntrl->getEventHandle());
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"    >>* Others waiting on the counting mutex:");
		if (mdq_PendingEvts.size() == 0 )
			LOGIF(LOGP_MUTEX)(CLOG_LOG," None.\n");
		else
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"\n");
		for (deque< hCevent*>::iterator iWT = mdq_PendingEvts.begin();
			iWT < mdq_PendingEvts.end(); iWT++ )
		{// iWT isa Ptr 2a Ptr 2a hCevent
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"        >>>*Event Handle:0x%02x thrd(0x%03x)\n",
				(*iWT)->getEventControl()->getEventHandle(),(*iWT)->threadPendingOn);
		}
#endif
//		push on back of dequeue
		mdq_PendingEvts.push_back(pLocEventPtr);
//-		release Critical Section
		pCntMutex->relinquishMutex(); // do not pend while holding mutex
//		pend on the new event - hold return value
		rc = pEvtCntrl->pendOnEvent(ultimeout);
//+		get Critical Section
		trc = pCntMutex->aquireMutex(ultimeout);
#ifdef DBG_CNTMUTEX
		wrkingThreadID_deux = Thread_GetCurrentThreadId();
		if ( wrkingThreadID != wrkingThreadID_deux )
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM* Rcv'd Event but in an unexpected thread.(expected 0x%03x"
									   " but got 0x%03x).\n",activeThreadID,wrkingThreadID_deux);
		else
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM* Rcv'd Event 0x%02x in thread 0x%03x\n",
										pEvtCntrl->getEventHandle(),wrkingThreadID_deux);
#endif
//		<if timeout - exit timeout>
		if ( trc != SUCCESS )
		{//---do not leave event on the queue
			for (deque< hCevent*>::iterator iET = mdq_PendingEvts.begin();
				iET < mdq_PendingEvts.end(); iET++ )
			{// iET isa Ptr 2a Ptr 2a hCevent
				if ( (*iET) == pLocEventPtr )
				{
					mdq_PendingEvts.erase(iET);// calls destructor
					break; // out of for loop
				}
			}
#ifdef DBG_CNTMUTEX
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM*Rcv'd Event but cannot get Mutex for thread ID:0x%03x\n",
				 activeThreadID);
#endif
			delete pEvtCntrl;
			return trc;
		}
//		if held return value from pend is timeout
		if (rc != SUCCESS)
		{
//			remove my event from the dequeue
			for (deque< hCevent*>::iterator iET = mdq_PendingEvts.begin();
				iET < mdq_PendingEvts.end(); iET++ )
			{// iET isa Ptr 2a Ptr 2a hCevent
				if ( (*iET) == pLocEventPtr )
				{
					mdq_PendingEvts.erase(iET);// calls destructor
					break; // out of for loop
				}
			}
#ifdef DBG_CNTMUTEX
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM*Rcv'd Error 0x%x while pending for Event for Thread ID:0x%03x\n",
				 rc,activeThreadID);
#endif
//			exit timeout
			delete pEvtCntrl;	
			pCntMutex->relinquishMutex();
			return rc;
		}
		else // all is well, we've been signaled it's our turn
		{
			activeEntryCnt = 1;
//			set active thread to current thread id
			activeThreadID = wrkingThreadID;
			// signaler is responsible to cycle the event

			/* stevev 13apr10 - we generated it, we need to dispose */
			mv_UnusedEvents.push_back(pLocEventPtr);// allow reuse

#ifdef DBG_CNTMUTEX
			LOGIF(LOGP_MUTEX)(CLOG_LOG,"CM*Rcv'd Event for thread ID:0x%03x"
										" within getCountingMutex.\n",activeThreadID);
#endif
		}//endif
		delete pEvtCntrl;
	}//endif
//-	release Critical Section	
	pCntMutex->relinquishMutex();
	return SUCCESS;	
}

RETURNCODE hCcountingMutex::putCountingMutex(void)
{// assumes that we actually have the mutex
	RETURNCODE rc = SUCCESS;
	THREAD_ID_TYPE wrkingThreadID;

//	get current thread id
/*Vibhor 050404: Start of Code*/
	wrkingThreadID = Thread_GetCurrentThreadId ();//AfxGetThread( )->m_nThreadID;
/*Vibhor 050404: End of Code*/

//+ get Critical Section
	if (pCntMutex == NULL)
	{
#ifdef DBG_CNTMUTEX
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*NoMutex in return for thread:0x:%03x\n",wrkingThreadID);
#endif
		return APP_CONSTRUCT_ERR;
	}
	rc = pCntMutex->aquireMutex(hCevent::FOREVER);// forever
	if ( rc != SUCCESS)
	{//<if timeout - exit timeout>
#ifdef DBG_CNTMUTEX
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*Could not acquire Return Mutex for thread:0x:%03x\n",
									wrkingThreadID);
#endif
		return rc;
	}
	
//	VERIFY active thread is current thread id AND active count is > 0
	if ( !Thread_IsThreadSame(wrkingThreadID, activeThreadID) || activeEntryCnt < 1)
	{// < release Critical Section, exit failure>	
		pCntMutex->relinquishMutex();
#ifdef DBG_CNTMUTEX
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*My     thread:0x:%03x  Active thread:0x:%03x"
					" active count= %d\n  >>*ERROR: Invalid return of counting Mutex.\n",
					wrkingThreadID,activeThreadID,activeEntryCnt);
#endif
		return APP_PROGRAMMER_ERROR;// releasing one you don't own
	}// else continue
		
//	dec active count
	activeEntryCnt--;
#ifdef DBG_CNTMUTEX
	LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*Returning for thread:0x:%x new active count = %d\n"
								,wrkingThreadID,activeEntryCnt);
#endif
	if (activeEntryCnt == 0 )
	{
		if (mdq_PendingEvts.size() > 0 )// somebody is waiting
		{//	get front event
			hCevent* pLocEventPtr = mdq_PendingEvts.front();
			mdq_PendingEvts.pop_front();
			if (pLocEventPtr == NULL)
			{	
#ifdef DBG_CNTMUTEX
				LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*NULL pending event in Return for thread:0x:%03x\n"
					,wrkingThreadID);
#endif
				pCntMutex->relinquishMutex();
				return APP_PROGRAMMER_ERROR;// releasing one you don't own
			}// else we have an event
			hCevent::hCeventCntrl* 
			pEvtCntrl = pLocEventPtr->getEventControl();
			if (pEvtCntrl == NULL)
			{
				mv_UnusedEvents.push_back(pLocEventPtr);	
#ifdef DBG_CNTMUTEX
				LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*No pending event control in Return for thread:0x:%03x\n",
					wrkingThreadID);
#endif
				pCntMutex->relinquishMutex(); 
				return APP_MEMORY_ERROR;
			}// else we have control
			//	signal event
#ifdef DBG_CNTMUTEX
		LOGIF(LOGP_MUTEX)(CLOG_LOG,"  CM*Signal another's event:0x%02x (0x%03x)\n",
						pEvtCntrl->getEventHandle(),  pLocEventPtr->threadPendingOn );
#endif	
			pEvtCntrl->triggerEvent();// while we hold the mutex, rcvr should pend on mutex
			delete pEvtCntrl;
			/* stevev 13apr10 - make the pender, who new'd this guy, put him back when he's done pending
			mv_UnusedEvents.push_back(pLocEventPtr);// allow reuse
			instead, set the active thread so this thread can't aquire between signal and reception***/
			activeThreadID = THREAD_ID_OOR;
		}
		else // nobody is waiting
		{// set active thread to zero 
			activeThreadID = THREAD_ID_NONE;// count already is
		}//endif
	}
	else // active count > 0
	{
		// no-op, it is not yet time to release it
	}//endif
//  -	release Critical Section
	pCntMutex->relinquishMutex();
	Sleep(1); // pause to let others (receiving the trigger) to run
	return SUCCESS;
}


/* 25jul05 - add global replacements for wait functions
 *
 */
/************************ UI only funcs from SDCthreadSync.h ***********************************/

// TEMP::::
extern bool m_doingIdle;

// replace WaitForMultipleObjects
// this returns Windows codes just like Wait4MultipleObjects
DWORD systemWaitMultipleObjects(DWORD nCount, HANDLE *lpHandles,
								bool bWaitAll,DWORD dwMilliseconds)	
{
	DWORD retVal = 0;

#ifndef USE_OS /* define if you want to operating system calls */

	if ( 1 )
		// try above (1) was:: UIthreadID == Thread_GetCurrentThreadId() /*TEMP::*/&& ! m_doingIdle)
	{// wait with message pump
		

#ifdef TST_GLBL	
		hmIT_t itHndlMp;
		if (inUIthread && hndlMap.find(lpHandles[0]) != hndlMap.end())
		{			
			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: system Wait re-entrancy! ");
#ifdef TST_GLBL_OUT
			for ( int q = 0; q < nCount; q++)
			{
				itHndlMp = hndlMap.find(lpHandles[q]);
				if (itHndlMp == hndlMap.end() )// not found
				{			
					LOGIT(CERR_LOG|CLOG_LOG," [0x%04x] ",lpHandles[q]);
				}
				else
				{			
					LOGIT(CERR_LOG|CLOG_LOG," 0x%04x ",lpHandles[q]);
				}
			}
#endif
			LOGIT(CERR_LOG|CLOG_LOG," {%d}\n",hndlMap.size());
			//return 0xffffffff;// error with no geterror...
		}
#endif

#ifdef TST_GLBL		//else
		{
			inUIthread = TRUE;
#ifdef TST_GLBL_OUT
			LOGIT(CLOG_LOG,"Inserting ");
#endif
			for ( int y = 0; y < nCount; y++)
			{
				itHndlMp = hndlMap.find(lpHandles[y]);
				if (itHndlMp == hndlMap.end() )// not found
				{
					hndlMap[lpHandles[y]] = UIthreadID;
#ifdef TST_GLBL_OUT
					LOGIT(CLOG_LOG," 0x%04x ",lpHandles[y]);
					
				}
				else
				{
					LOGIT(CERR_LOG|CLOG_LOG," ERROR: system Wait with duplicate Event Handles! 0x%04x "
						,lpHandles[y]);
				}
			}
			LOGIT(CLOG_LOG," {%d}\n",hndlMap.size());
#else
				}
			}
#endif
		}
#endif

		/* clipped from KB Q136885 - added stuff to it for genericity */
	   MSG msg;
#ifndef _WIN32_WCE			// PAW 24/04/09
	   struct _timeb startTm, tmpTm;
#else
		time_t_ce startTm;
		time_t_ce startTmMS;
		time_t_ce tmpTm;
		time_t_ce tmpTmMS;
		time_t_ce start_ce;	

#endif
	   DWORD  elapsedMs;
	   int    diffMs;
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Wait for UI thread with %d items, Hndl0== 0x%04x\n",nCount,lpHandles[0]);
#endif
#ifdef _DEBUG
for (int y = 0; y < (int)nCount;y++) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
{
	if (lpHandles[y] == NULL || lpHandles[y] > (void*)0x0000ffff)
	{
		LOGIT(CLOG_LOG,"Wait Multiple item %d was a weird (0x%04x)\n",y,lpHandles[y]);
	}
}
#endif
	   while(1)
	   {
#ifndef _WIN32_WCE			// PAW 24/04/09
		   _ftime( &startTm );
#else
	SYSTEMTIME	st;
	GetSystemTime(&st);
	SetTz(&st);
	startTmMS = st.wMilliseconds;
	start_ce = time_ce(&startTm);	// time
#endif

		   retVal = MsgWaitForMultipleObjects( nCount,// array size
				   lpHandles,						// The array of events
				   FALSE,							// Wait for any event ( not ALL )
				   dwMilliseconds,					// Timeout value
				   QS_ALLINPUT);					// Any message wakes up
		   if(retVal >= WAIT_OBJECT_0 && retVal < (WAIT_OBJECT_0 + nCount))
		   {  // The event was signaled, return
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Got- for UI thread with %d items, Hndl0== 0x%04x\n",nCount,lpHandles[0]);
#endif
#ifdef TST_GLBL
				inUIthread = FALSE;
				for ( int w = 0; w < nCount; w++)
				{
					hndlMap.erase(lpHandles[w]);
#ifdef TST_GLBL_OUT
					LOGIT(CLOG_LOG," Removed 0x%04x ",lpHandles[w]);
				}
				LOGIT(CLOG_LOG," {%d}\n",hndlMap.size());
#else
				}
#endif
#endif
				break; // out of while; which will return retVal;
		   } 
		   else 
		   if(retVal == WAIT_OBJECT_0 + nCount)
		   {
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Got- for UI thread with %d items, Msg== 0x%04x\n",nCount,retVal);
#endif
				// There is a window message available. Dispatch it.
#ifndef _WIN32_WCE
				while(PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE))  //00.01.12 see below
#else
				while(PeekMessage(&msg,NULL,NULL,0xbfff,PM_REMOVE)) 
#endif
				{

#ifdef DBG_WAIT_SLEEP
if (msg.message != WM_TIMER )
LOGIT(CLOG_LOG,"+++ Got- for UI thread MsgVal== 0x%04x (redispatch)\n",msg.message);
#endif
#ifdef FNDMSG
if (msg.message==FNDMSG){LOGIT(CLOG_LOG,"~X");} 
#endif
					if ( msg.message == WM_QUIT /*stevev 01mar06 try:*/ 
						/* --- issues in 2008 (and yokogawa german machines) -----
						                                 || msg.message == WM_NULL  end try */
						)
					{
						//LOGIT(CLOG_LOG,"PeekMessage returned a message number of WM_QUIT.\n");
						PostQuitMessage( msg.lParam );// param is exit code
#ifdef TST_GLBL
						inUIthread = FALSE;
						for ( int k = 0; k < nCount; k++)
						{
							hndlMap.erase(lpHandles[k]);
#ifdef TST_GLBL_OUT
							LOGIT(CLOG_LOG," Removed 0x%04x ",lpHandles[k]);
						}
						LOGIT(CLOG_LOG," {%d}\n",hndlMap.size());
#else
						}
#endif
#endif

						return WAIT_FAILED;
					}
#ifdef TOK
				    if ( msg.message != WM_NULL )
					{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
					}
#else
					if (msg.message != WM_NULL && !AfxGetApp()->PreTranslateMessage(&msg))
					{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
					}
#endif
					Sleep(/*1 00.01.12 */0); // stevev 20jun06 - see if any other threads need to run between msgs
											 // stevev 11feb10 - try zero, it might work....
				}
#ifndef _WIN32_WCE			// PAW 24/04/09
				_ftime( &tmpTm );
				diffMs    = tmpTm.millitm - startTm.millitm;
				elapsedMs = (DWORD)((tmpTm.time - startTm.time)*1000)+ diffMs ;// if we wait longer than a DWORD we're in a heap 'a hurt
#else
				SYSTEMTIME	st;
				GetSystemTime(&st);
				SetTz(&st);
				tmpTmMS = st.wMilliseconds;
				start_ce = time_ce(&tmpTm);	// time

				diffMs    = tmpTmMS - startTmMS;
				elapsedMs = ((tmpTm - startTm)*1000)+ diffMs ;
#endif
				if ( elapsedMs < dwMilliseconds )
					dwMilliseconds -= elapsedMs;// prepare to reenter timed wait
		   } 
		   else
			if (retVal == WAIT_TIMEOUT )
			{
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Got- for UI thread WAIT_TIMEOUT - returning\n");
#endif
#ifdef TST_GLBL
				inUIthread = FALSE;
				for ( int z = 0; z < nCount; z++)
				{
					hndlMap.erase(lpHandles[z]);
#ifdef TST_GLBL_OUT
					LOGIT(CLOG_LOG," Removed 0x%04x ",lpHandles[z]);
				}
				LOGIT(CLOG_LOG," {%d}\n",hndlMap.size());
#else
				}
#endif
#endif
				return WAIT_TIMEOUT;//APP_WAIT_TIMEOUT;
			}
		   else
		   {
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Error for UI thread with %d items, Hndl0== 0x%04x\n",nCount,lpHandles[0]);
#endif
			  // Something else happened. Return.
#ifdef _DEBUG
LOGIT(CLOG_LOG,"Something else happened in a UI wait - returning.(0x%08x)\n",retVal);
if (retVal == 0xffffffff)
{LPVOID lpMsgBuf;
 FormatMessage( 
    FORMAT_MESSAGE_ALLOCATE_BUFFER | 
    FORMAT_MESSAGE_FROM_SYSTEM | 
    FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    GetLastError(),
    0, // Default language
    (LPTSTR) &lpMsgBuf,
    0,
    NULL 
 );
 LOGIT(CLOG_LOG,L"         error String '%s'\n",lpMsgBuf);
 TRACE(_T("         error String '%s'\n"),lpMsgBuf);

}
#endif
#ifdef TST_GLBL
				inUIthread = FALSE;
				for ( int w = 0; w < nCount; w++)
				{
					hndlMap.erase(lpHandles[w]);
#ifdef TST_GLBL_OUT
					LOGIT(CLOG_LOG," Removed 0x%04x ",lpHandles[w]);
				}
				LOGIT(CLOG_LOG," {%d}\n",hndlMap.size());
#else
				}
#endif
#endif

			  return retVal;// wait abandondoned etc
		   }
	   }//wend
	}
	else
	{// just wait
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"+++ Wait for 0x%04x thread with %d items, Hndl0== 0x%04x\n",Thread_GetCurrentThreadId(),nCount,lpHandles[0]);
#endif
		retVal = WaitForMultipleObjects(nCount,lpHandles,bWaitAll,dwMilliseconds);
#ifdef DBG_WAIT_SLEEP
LOGIT(CLOG_LOG,"--- Got- for 0x%04x thread with %d items, Hndl0== 0x%04x\n",Thread_GetCurrentThreadId(),nCount,lpHandles[0]);
#endif
	}
#else  //USE_OS
	retVal = WaitForMultipleObjects(nCount, lpHandles, bWaitAll, dwMilliseconds); //Stay traditional
#endif //USE_OS

	return retVal;
}

// replace WaitForSingleObject
DWORD systemWaitSingleObject(HANDLE hHandle, DWORD mSecs)
{
	DWORD retVal = 0;

	/* keep the more complex operation in a single place... multiple...*/
	retVal = systemWaitMultipleObjects(1, &hHandle,false,mSecs);
	return (retVal);
}

// replace Sleep
//		Note that the UI sleep will be inaccurate by the message handling time.
//		If you need more accuracy - time a NON-UI thread!!!!!!!!!
void systemSleep(DWORD mSecs)									 
{	
#ifndef USE_OS
	MSG  msg;
	BOOL bRet; // OS uses big BOOL
	DWORD cnt =	mSecs / slotTime;
	DWORD rem = mSecs % slotTime;
	DWORD rCnt= 0;

	if ( UIthreadID == Thread_GetCurrentThreadId() )
	{// wait with message pump
#ifdef TST_GLBL
		bool clearOnExit = TRUE; 
		if (inUIthread)
		{
			LOGIT(CERR_LOG,"ERROR: system Sleep re-entrancy!\n");
			//return ;// error with no geterror...
			clearOnExit = FALSE;
		}
		else
		{
			inUIthread = TRUE;
		}
#endif

		if ( mSecs == INFINITE  || mSecs == 0 )
		{	return; /* don't allow lock-up */  }
		// 
		// simulate message pump 
		// 
		//CWinApp *pApp = AfxGetApp();

		while ( cnt )
		{
			cnt = cnt;	// WM_QUIT message

			if( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
			{ 
				if (bRet == 0 )
				{
					LOGIT(CLOG_LOG,"SleepPump: zero message ret,\n");
				}
#ifdef TOK
				if (bRet != -1 )
				{
					::TranslateMessage(&msg); 
					::DispatchMessage(&msg); 
				} 
#else
				if (bRet != -1 && !AfxGetApp()->PreTranslateMessage(&msg))
				{
					::TranslateMessage(&msg); 
					::DispatchMessage(&msg); 
				} 
				else
				{
					AfxGetApp()->OnIdle(++rCnt);
				}
#endif
			} 

			Sleep(slotTime);
			cnt--;
		}
		if ( rem )
		{
			Sleep(rem);
		}
#ifdef TST_GLBL
		if (clearOnExit)
		{
			inUIthread = FALSE;
		}
#endif

	}
	else
#endif // USE_OS
	{// just wait
		Sleep(mSecs);
	}
}

void systemFlush(void)
{
#if defined(USE_OS) && defined(_WIN32)

//	CWinApp *pApp = AfxGetApp();
	int  cnt  = 150;
	BOOL bRet = 0; // OS uses big BOOL
	MSG  msg;

#ifndef _WIN32_WCE
	while ( cnt && PeekMessage(&msg,NULL,NULL,NULL, PM_NOREMOVE) )
#else // LINUX_PORT - REVIEW_WITH_HCF - This was also '#endif' (instead of '#else'?.  Check with Steve.
	while ( cnt && PeekMessage(&msg,NULL,NULL,0xbfff,PM_NOREMOVE)) 
#endif
	{

			cnt = cnt;	// WM_QUIT message


		if( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
		{ 	
#ifdef TOK
			if (bRet != -1)
			{
				::TranslateMessage(&msg); 
				::DispatchMessage(&msg); 
			} 
#else
			if (bRet != -1 && !AfxGetApp()->PreTranslateMessage(&msg))
			{
				::TranslateMessage(&msg); 
				::DispatchMessage(&msg); 
			} 
#endif
		} 

		Sleep(/*10 00.01.12 */0);// we'll try zero, it might work
		cnt--;
	}
//TRACE("   Flushed %d Messages in 0x%x\n",(150-cnt),Thread_GetCurrentThreadId());
#endif // defined(USE_OS) && defined(_WIN32)
}


/*************************** ddb interface from ddbEvent.h *************************************/
// replace WaitForSingleObject
DWORD systemWaitSingle(hCevent::hCeventCntrl* pEvt, DWORD mSecs)
{
	DWORD retVal = SUCCESS;

	if ( pEvt == NULL )
	{
		return APP_WAIT_ERROR;
	}
	if (mSecs == 0)
	{
		mSecs = 1;// allow a task switch - possibly
	}
	DWORD dwWaitResult = systemWaitSingleObject( pEvt->getEventHandle(), mSecs );

	if (dwWaitResult == WAIT_OBJECT_0) // zero
					// has been set to available - this is automatic so it is now NOTavailable
	{
		retVal = SUCCESS; 
	}
	else 
	if (dwWaitResult == WAIT_TIMEOUT)//x102
						//The time-out interval elapsed, and the object's state is not available.
	{
		retVal = APP_WAIT_TIMEOUT;
	}
	else 
	if (dwWaitResult == WAIT_ABANDONED)// 0x80
//The specified object is a mutex object that was not released by the thread that owned the mutex 
//	object before the owning thread terminated. Ownership of the mutex object is granted to the 
//	this thread, and the mutex is set to NOTavailable. 
	{
		retVal = APP_WAIT_ABANDONED;
	}
	else // error
	{
		retVal = APP_WAIT_ERROR;
	}

	return retVal;
}
// replace WaitForMultipleObjects
DWORD systemWaitMultiple(hCevent::eventPtrList_t& evtList,   DWORD mSecs)	 
{
	DWORD retVal = 0;
	DWORD evtCnt = evtList.size();
	if (evtCnt < 1)
	{
		retVal = APP_WAIT_ERROR;
	}
	HANDLE *pEA = new HANDLE[ evtCnt ];
	int idx = 0;
	for (hCevent::eventPtrList_t::iterator iT = evtList.begin(); iT != evtList.end();++iT)
	{
		pEA[idx++] = (*iT)->getEventHandle();
		(*iT)->clrResult();
	}
	
	retVal = systemWaitMultipleObjects(evtCnt, pEA, false, mSecs);

	if (retVal >= WAIT_OBJECT_0 && retVal < (WAIT_OBJECT_0 + evtCnt)) 
	{
		evtList[retVal - WAIT_OBJECT_0]->setResult( hCevent::ere_Success );
		retVal = SUCCESS; 
	}
	else 
	if (retVal == WAIT_TIMEOUT)
						//The time-out interval elapsed, and the object's state is not available.
	{
		retVal = APP_WAIT_TIMEOUT;
	}
	else 
	if (retVal >= WAIT_ABANDONED_0 && retVal < (WAIT_ABANDONED_0 + evtCnt) )
//The specified object is a mutex object that was not released by the thread that owned the mutex 
//	object before the owning thread terminated. Ownership of the mutex object is granted to the 
//	this thread, and the mutex is set to NOTavailable. 
	{
		evtList[retVal - WAIT_ABANDONED_0]->setResult( hCevent::ere_Abandoned );
		retVal = APP_WAIT_ABANDONED;
	}
	else // error
	{
		retVal = APP_WAIT_ERROR;
	}

	return retVal;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Implement a critical section class
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/


#if defined(__GNUC__)
hCcriticalSection::hCcriticalSection():
		threadID(),
		threadHandle(THREAD_HANDLE_INVALID),
		entryPriority(0),
		cs()
{
    // Empty
}

hCcriticalSection::~hCcriticalSection()
{
    // Empty
}


bool hCcriticalSection::EnterCS(void)
{
	cs.lock();
	threadHandle      = Thread_GetCurrentThread();
	threadID          = Thread_GetCurrentThreadId();
	entryPriority     = Thread_GetThreadPriority(threadHandle);
	Thread_SetThreadPriority(threadHandle, THREAD_PRIORITY_TIME_CRITICAL);
	return false;// success

}

bool hCcriticalSection::Exit_CS(void)
{
	THREAD_ID_TYPE localThreadID = Thread_GetCurrentThreadId();
	if ( localThreadID != threadID )
	{
		return true; // we encountered an error
	}// else continue processing
	Thread_SetThreadPriority(threadHandle, entryPriority);

	cs.unlock();
	return false;

}


#else // not __GNUC__


struct CS {
	int oneDummyElement;
};

hCcriticalSection::hCcriticalSection()
{
	threadHandle = THREAD_HANDLE_INVALID;
	threadID = 0;
	pBillsCS = (CS *)new CRITICAL_SECTION;
	InitializeCriticalSection( (CRITICAL_SECTION*)pBillsCS );
	entryPriority = 0;
}

hCcriticalSection::~hCcriticalSection()
{
	DeleteCriticalSection( (CRITICAL_SECTION*)pBillsCS );
	delete pBillsCS;
}


bool hCcriticalSection::	EnterCS(void)
{
	EnterCriticalSection( (CRITICAL_SECTION*)pBillsCS );
	threadHandle      = Thread_GetCurrentThread();
	threadID          = Thread_GetCurrentThreadId();
	entryPriority     = Thread_GetThreadPriority(threadHandle);
	Thread_SetThreadPriority(threadHandle, THREAD_PRIORITY_TIME_CRITICAL);
	return false;// success
}

bool hCcriticalSection::	Exit_CS(void)
{
	THREAD_ID_TYPE localThreadID = Thread_GetCurrentThreadId();
	if ( localThreadID != threadID )
	{
		return true; // we encountered an error
	}// else continue processing
	Thread_SetThreadPriority(threadHandle, entryPriority);

	LeaveCriticalSection( (CRITICAL_SECTION*)pBillsCS );
	return false;
}
#endif // __GNUC__

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * test a thread specific stack
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
#if 0
template <class B> 
int tsPtr<B>::index(hCmutex::hCmutexCntrl* pM)
{
	int L;
	BOOL ownMutex = FALSE;
	ulong t = Thread_GetCurrentThreadId();
	if (pM == NULL)
	{
#ifdef _DEBUG
		pM = mapMutex.getMutexControl("tsPtr");
#else
		pM = mapMutex.getMutexControl();
#endif
		ownMutex = TRUE;
	}
pM->aquireMutex();

	map<ulong,int>::iterator loc = thrd2classIdxMap.find(t);
	if ( loc == thrd2classIdxMap.end() )
	{
		L = classItems.size(); // where the next push 'll go
		thrd2classIdxMap.insert(make_pair(t,L));

		B* pU = new B;
		classItems.push_back(pU);
		classEntryCnt.push_back(0);
	}
	else
	if ( classItems[loc->second] == NULL )
	{
		L = loc->second;

		B* pU = new B;
		classItems[L] = pU;
		classEntryCnt[L] = 0;
	}
	else
	{// found it 
		L = loc->second;
	}
pM->relinquishMutex();
if ( ownMutex )
{
	ownMutex = FALSE;
	delete pM;
}

	return L;
}
template <class B> 
void tsPtr<B>::removeIdx(int idx, hCmutex::hCmutexCntrl* pM )
{
	BOOL ownMutex = FALSE;
	ulong t = Thread_GetCurrentThreadId();
	if (pM == NULL)
	{
#ifdef _DEBUG
		pM = mapMutex.getMutexControl("tsPtr");
#else
		pM = mapMutex.getMutexControl();
#endif
		ownMutex = TRUE;
	}
pM->aquireMutex();

	map<ulong,int>::iterator loc = thrd2classIdxMap.find(t);
	if ( loc == thrd2classIdxMap.end() )
	{
		LOGIT(CLOG_LOG,"Removing a item that doesn't exist.\n");
	}
	else
	if ( classItems[loc->second] == NULL )
	{
		LOGIT(CLOG_LOG,"Removing a item that has already been removed.\n");
	}
	else
	{// found it 
		int L = loc->second;
		delete classItems[L]; 
		classItems[L] = NULL;
		classEntryCnt[L] = 0;
	}
	pM->relinquishMutex();
	if ( ownMutex )
	{
		ownMutex = FALSE;
		delete pM;
	}
}
#endif

THREAD_ID_TYPE getTHID(void)
{
	return Thread_GetCurrentThreadId();
};

/*************************************************************************************************
 *
 *   $History: ddb_EventMutex.cpp $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:23a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:14p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 9:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART standards
 * 
 *************************************************************************************************
 */
                        
