/*************************************************************************************************
 *
 * $Workfile: ddbItemListBase.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		generated from the top half of previous version of ddbItemLists.h
 *
 * #include "ddbItemListBase.h"
 */

#ifndef _DDBITEMLISTBASE_H
#define _DDBITEMLISTBASE_H

#include "ddbItems.h"	/* loads all the includes for items almost all the uncludes)*/
#include "ddb.h"
#include "ddbDependency.h"

class CddbDevice;

/* * * * * * * * * * * list base class * * * * * * * * * * * * */
template<itemType_t MY_ITYPE, class LISTOFCLASS>  
class CitemListBase : public vector< LISTOFCLASS* >, public hCobject
{
	itemType_t myType;
public:

	typedef typename vector< LISTOFCLASS* >::iterator itemList_it;

public:
	CitemListBase(DevInfcHandle_t h) : hCobject(h) { myType = MY_ITYPE;};
	virtual ~CitemListBase(){ /*destroy();*/ };
	//virtual 
	//RETURNCODE populate(CddbBaseDevice* pDev, UINT32 theDDkey, aCdevice* pAbstractDevice=NULL);
	virtual RETURNCODE populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl, 
											        DD_Key_t theDDkey, aCdevice* pAbstractDevice);
	virtual RETURNCODE removeItem(LISTOFCLASS* pTarget);
	virtual void fillTypedefs(void);// empty except for LISTs and ARRAYs
	virtual void dumpSelf(int indent=0);
	virtual void destroy(void); // clear contents before deletion
	virtual void calcValidityDependency(void);
	virtual void calcAttributeDependency(void);// readded 3jul06
	virtual void fillIcontain(void); // stevev 27sep06
	virtual void fill_depend_tree(ibWhichDependency_t depType);
	virtual void clean_Depends(void);
	virtual RETURNCODE inform_Dependents(ibWhichDependency_t depType);
	virtual LISTOFCLASS* getBySymbolName(string& symbolName);// stevev 26jul06 (mainly 4 debugging)

};

template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: destroy(void)
{
	//LISTOFCLASS* ploc;
int cnt = 0;
	for ( itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// iT is a ptr2ptr2 LISTOFCLASS
		LISTOFCLASS *pItm = ( LISTOFCLASS * )(*iT);
//		DESTROY( pItm );// stevev 25jul06 - this was an undefined var causing a crash
		if((pItm)!=NULL)
		{ 
			(pItm) ->destroy(); 
			delete (pItm); 
			(pItm) = NULL;
		}
//////////////////////

		(*iT) = NULL;
cnt++;
	}// next

	this->clear();
	// leave our type alone
//clog <<"Destroy List " << MY_ITYPE << "  " << cnt << " items."<<endl;
	return;
}

/* * * * * * * base class  method * * * * * * */
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: dumpSelf(int indent)
{		
	int idx;
	if (myType <= iT_MaxType) idx = (int)myType;
	else 
	if (myType >= iT_FormatObject && myType <= iT_BlockDirectory)   idx = (int)iT_MaxType+1+(myType%iT_FormatObject);
	else 
	if (myType >= iT_Parameter && myType <= iT_BlockCharacteristic) idx = (int)iT_MaxType+4+(myType%iT_Parameter);
	else
		idx = -1;

	if ( idx >=0)
		LOGIT(COUT_LOG,"%s* %s List\n",Space(indent),itemStrings[idx] );
	else
		LOGIT(COUT_LOG,"%sUnknown Item Type %s(0x%02x)\n",Space(indent),itemStrings[myType],myType);

	for ( itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// iT is a ptr2ptr2 LISTOFCLASS
		(*iT)->dumpSelf(indent + 4,"");
		LOGIT(COUT_LOG,"+++\n");
	}
	return;
}

/* * * * * * * base class  method * * * * * * */
/* instantiate all of this item type
 * if we have an abstract, populate from that, else get the data 
 */
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
RETURNCODE CitemListBase <MY_ITYPE,LISTOFCLASS>
//:: populate(CddbBaseDevice* pDev, UINT32 theDDkey, aCdevice* pAbstractDevice)
:: populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl, DD_Key_t theDDkey, aCdevice* pAbstractDevice)
{		
	RETURNCODE rc = SUCCESS;
	CitemType errType(MY_ITYPE);// for debugging logs

	if (pAbstractDevice != NULL)
	{
		for (AitemList_t::iterator iT = pAbstractDevice->AitemPtrList.begin(); 
		                           iT < pAbstractDevice->AitemPtrList.end  ();    iT++)
		{// iT is ptr 2 a ptr to a aCitemBase
			if ((*iT)->itemType.getType() == MY_ITYPE)  // filter just this type
			{// build it - constructor builds from the prototype and it's sub parts do too
				LISTOFCLASS* pItem = NULL; // VMKP 050304
				pItem = new LISTOFCLASS(hndl, *iT ); // passes in a ptr to a aCitemBase
				if (pItem)
				{
					/*self*/this->push_back(pItem);
					pDev->registerItem(pItem->getID(), pItem, pItem->getSymbolName());
				}
			}
		}
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"> device.list has %3d entries. of %3d items of type: %hs\n",
			size(),pAbstractDevice->itemCount(),errType.getTypeStr() );
#endif
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,L"ERROR: populate list was not handed a prototype abstract class. %s\n",
																		  errType.getTypeStr() );

#if 0	/* we really shouldn't need to do all this again */
		AitemList_t* pLocalItemList = NULL;  // local abstract list ptr

		rc = allocListOfItems(theDDkey, MY_ITYPE, &pLocalItemList ); // alloc and fetch

		// load item list from the dd
		rc = getListOfItems(theDDkey, MY_ITYPE, pLocalItemList );// items / with attributes

		if ( rc == SUCCESS)
		{	// for each item

			for (AitemList_t::iterator 
				pos = pLocalItemList->begin();
				pos < pLocalItemList->end();
				++pos )//pos = localItemList.erase(pos)   )// removes 'em as we go
			{	// don't fill its attributes- 
				// pos is a ptr to a ptr to a aCitemBase			
				if ( rc == SUCCESS && (*pos)!= NULL)
				{// instantiate class w/ list of attributes
					pItem = new LISTOFCLASS( *pos );// passes in a ptr to a aCitemBase
					if (pItem)
					{
						pItem->setDevice(pDev);
						/*self*/push_back(pItem);
						//pDev->masterMap.insert( pair<int, hCitemBase*>(pItem->getID(),pItem) );
						pDev->insert2map( pair<int, hCitemBase*>(pItem->getID(),pItem) );
					}
				}
			}
			releaseItemList(&pLocalItemList);// return the local abstract's memory to the dll
		}
#endif 
	}
	return rc;
};

 /*ie               iT_Variable,    hCVar*/
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
RETURNCODE CitemListBase <MY_ITYPE,LISTOFCLASS>
:: removeItem(LISTOFCLASS* pTarget)
{
	bool erasedIt = false;
	if ( pTarget == NULL || pTarget->getIType() != MY_ITYPE )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: ItemList's removeItem was passed a bad pointer.\n");
		return APP_PARAMETER_ERR;
	}

	itemID_t targetID = pTarget->getID(); // we'll look it up by ID

	itemList_it iT;	// PAW 03/03/09 see below
	for ( /*iterator PAW*/ iT = this->begin(); iT < this->end(); iT++)
	{// iT is a ptr2ptr2 LISTOFCLASS
		if ( (*iT)->getID() == targetID )
		{
			this->erase(iT);// deletes the pointer (not what it points to)
			erasedIt = true;
			break;
		}
	}// next

	return (! erasedIt );// succesful on false 
};


template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: fillTypedefs(void)
{
	if ( myType == iT_Array)
	{	
		int i,   aCnt = this->size();
		hCarray* pArr = NULL;
		for ( i = 0; i < aCnt; i++)
		{// we know that any new ones will go on the end...
			// we have to cast because this is compiled for all LISTOFCLASS
			pArr = (hCarray*)this->at(i);// this is supposed to recalculate location each time
			pArr->initTypedef(NULL);
		}

		/* this doesn't work because down in initTypedef, it can instantiate
			another array, totally screwing up the iterator
		for ( iterator iT = begin(); iT < end(); iT++)
		{// iT is a ptr2ptr2 LISTOFCLASS <hCarray>
			((hCarray*)(*iT))->initTypedef();
		}
		**/
	}
	else
	if ( myType == iT_List)
	{
		int i,  lCnt = this->size();
		hClist* pLst = NULL;
		for ( i = 0; i < lCnt; i++)
		{// we know that any new ones will go on the end...
			// we have to cast because this is compiled for all LISTOFCLASS
			pLst = (hClist*)this->at(i);// this is supposed to recalculate location each time
			pLst->initTypedef(NULL);
		}
		/* this doesn't work because down in initTypedef, it can instantiate
			another list, totally screwing up the iterator		
		for ( iterator iT = begin(); iT < end(); iT++)
		{// iT is a ptr2ptr2 LISTOFCLASS <hClist>
			((hClist*)(*iT))->initTypedef();
		}
		**/
	}
	else
	{
		;/* no op - just return */
	}
	return;
};

template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: calcValidityDependency(void)
{// the class MUST have a parent of hCitemBase
	hCdependency* pDep = NULL;
	hCitemBase*   pIB  = NULL;// stevev 21apr08
	globalDependencyTrail.clear();// stevev 21apr08

	for (itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// isa ptr 2a ptr 2a hCitemBase derived class
		pIB  = (hCitemBase*) (*iT);
		pDep = (*iT)->getDepPtr(ib_ValidityDep);
		pDep->fillValidityList();
		globalDependencyTrail.clear();// stevev 21apr08
//stevev 3jul06 - moved to separate cover so we can do validity first
//		/* stevev 16nov05 */
//		pDep = (*iT)->getDepPtr(ib_CondDep);
//		pDep->fillConditionalList();	
//		/** end 16nov05 **/
	}// next var
	return;
};


// readded 3jul06
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: calcAttributeDependency(void)
{// the class MUST have a parent of hCitemBase
	hCdependency* pDep = NULL;

	for (itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// isa ptr 2a ptr 2a hCitemBase derived class
		pDep = (*iT)->getDepPtr(ib_CondDep);
		pDep->fillConditionalList();
	}// next var
	return;
};


// readded 3jul06
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: fillIcontain(void)
{// the class MUST have a parent of hCitemBase
	hCdependency* pDep = NULL;

	for (itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// isa ptr 2a ptr 2a hCitemBase derived class
		pDep = (*iT)->getDepPtr(ib_Contain);
		pDep->fillIContainList();
	}// next var
	return;
};


template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
RETURNCODE CitemListBase <MY_ITYPE,LISTOFCLASS>
:: inform_Dependents(ibWhichDependency_t depType)
{
	RETURNCODE rc = SUCCESS;
	hCdependency* pDep = NULL;

	for ( itemList_it iT = this->begin(); iT < this->end(); ++iT)
	{	
		pDep  = (*iT)->getDepPtr(depType);		
		rc   |= pDep->informDependents(depType);	
	}//next
	return rc;
};


template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: fill_depend_tree(ibWhichDependency_t depType)
{
	hCdependency* pDep = NULL;
	ddbItemList_t trail;

	for ( itemList_it iT = this->begin(); iT < this->end(); ++iT)
	{	
		pDep  = (*iT)->getDepPtr(depType);
		
		if ( pDep->dependsOnMe.size() > 0)							
		{	
			pDep->makeDependTree(trail,depType);// depth = zero at the top here
		}	
		trail.clear();/* get ready for the next one */
	}//next
	return;
};

/* stevev 14sep06 - added to recycle a chunk of memory */
//                  all the commons and the iDependOn in bit Valid & Cond
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
void CitemListBase <MY_ITYPE,LISTOFCLASS>
:: clean_Depends(void)
{
	RETURNCODE rc = SUCCESS;
	hCdependency* pDep = NULL;

	for ( itemList_it iT = this->begin(); iT < this->end(); ++iT)
	{	
		pDep  = (*iT)->getDepPtr(ib_ValidityDep);
		if ( pDep != NULL )
		{
			pDep->iDependOn.clear();
		}
		pDep  = (*iT)->getDepPtr(ib_CondDep);
		if ( pDep != NULL )
		{
			pDep->iDependOn.clear();
		}

		if ( pDep != NULL )
		{			
			(*iT)->deleteDependency(ib_Common);
		}	
	}//next
	return;
};
 
template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
LISTOFCLASS* CitemListBase <MY_ITYPE,LISTOFCLASS>
:: getBySymbolName(string& symbolName)
{		
	for ( itemList_it iT = this->begin(); iT < this->end(); iT++)
	{// iT is a ptr2ptr2 LISTOFCLASS 
		if (  ((hCitemBase*)(*iT))->getName() == symbolName ) 
		{
			return ((LISTOFCLASS*)(*iT));
		}
	}
	return NULL;
}

#endif

/*************************************************************************************************
 *
 *   $History: ddbItemListBase.h $
 * 
 *************************************************************************************************
 */
