/*************************************************************************************************
 *
 * $Workfile: ddbItemBase.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The base class and support classes for item's bases class
 *		7/26/02	sjv	created fom itembase.h
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *
 * #include "ddbItemBase.h"
 */

#ifndef _DDBITEMBASE_H
#define _DDBITEMBASE_H

#ifdef _IS_STATIC
#define _AFXDLL
#endif

#include "ddbGeneral.h"
#include "ddbPrimatives.h"
#include "ddbTracking.h"
#include "ddbDependency.h"
#include "ddbdefs.h"
#include "foundation.h"
#include "ddbGlblSrvInfc.h"
#include "ddbDeviceService.h"
#include "ddbConditional.h"
#include "dllapi.h"
#include "ddbFormat.h"

#ifndef VIRTUALCALLERR
#define VIRTUALCALLERR  (2)
#endif

#define NO_LABEL -1 /* never found key */

extern char itemStrings[][ITMAXLEN];

class hCattrValid;	// references so we don't have a  circular with ddbattributes
class hCattrLabel;
class hCattrHelp;
class hCVar;

#ifndef _DDBTRACKING_H   // HOMZ 2003 port
class hCgroupItemDescriptor;
#endif

class hCattrDebug;
//class CddbBaseDevice;
//class hCdependency;

//typedef vector<CattrBase*>  attributeList_t;
//typedef vector<hCitemBase*> ddbItemList_t;---now at the end of the file
//typedef struct itemTbl     itmTbl_t;
class hCattrBase;
class hCmsgList;

typedef vector<hCattrBase*>  hAttrPtrList_t;

#define getSymbolName  getName

class hCitemBase : public hCobject
{
protected:

//<<<<<<<< DATA >>>>>>>>>>>>>
	DD_Key_t        DDkey;     //27jul10 - changed from::> unsigned long   DDkey; 
	//CddbBaseDevice* pDevice;

	itemID_t        itemId;
	CitemType       itemType;	// see itemType_t

	ITEM_SIZE       itemSize;
	CitemType		itemSubType;

	string          itemName;	
	wstring        witemName;

//	attributeList_t attrLst;
	hAttrPtrList_t  attrLst;
//	CbinarySvcs*    pBinSvc;

/* VMKP added on 291203 */
	bool			isConditional;
/* VMKP added on 291203 */
	
	bool            isReallyCritical;   /* stevev 23feb07 - add filter for the truely critical */

	/* common atributes */
	hCattrValid*     pValid;//void* procure(void){return (procureVal((hCconditional<hCpayload>*) &condValid) ); };
	hCattrLabel*     pLabel;
	hCattrHelp*      pHelp;
	hCattrDebug*     pDebug;
	// stevev 25jul06 - share attributes //
	bool   sharedAttr;

	// 03aug06 - support for attribute references for label & help	
	hCVar* pPsuedoLabel;
	hCVar* pPsuedoHelp ;
/* made temorarily public
	itemID_t        ownerID;// psuedo-item's array or list id that contains him - 20sep12
	int             ownerIndex;// owner's index of this psduedo item;
*/
	// 06nov06 - each item has to record it's place in the last layout (via aquireTree)
	//		so it can tell if its size or location has changed
	hv_location_t myLoc;

protected:
	RETURNCODE baseLabel(int key, wstring &l);          /*returns the label if the item (or null)*/
	// stevev - WIDE2NARROW char interface \/ \/ 
	RETURNCODE baseLabel(int key,  string &l);          /*returns the label if the item (or null)*/
	
	/* stevev 19jan05 - added  */
	hCdependency*  m_pValidityDepends;
	hCdependency*  m_pConditionalDepends;
	hCdependency*  m_pCommonDepends;// stevev 14sep06 - must consolidate 4 critical.
	hCdependency*  m_pContainer;    // stevev 27sep06 - for parent derivation

	/* stevev 17nov05 - added - this is only called from an item */
	virtual
	RETURNCODE notify(hCmsgList& msgList);// see notification() in public area
	virtual 
	void setValidity(bool newV); // sets the external validity (used by Lists ONLY!)

public:

	
	itemID_t        ownerID;// psuedo-item's array or list id that contains him - 20sep12
	int             ownerIndex;// owner's index of this psduedo item;




	/* stevev 22jan09 */
	virtual // only implemented for variables and containers at this time (resets index values)
	RETURNCODE setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue)
	{/* default implementation */ return FAILURE; };
	virtual // only implemented for variables and containers(pass thru to vars) at this time
	RETURNCODE addCmdDesc(hCcommandDescriptor& rdD, bool isRead)
	{/* default implementation */ return FAILURE; };

	/* stevev 20jan05 */
	hCdependency*  getDepPtr(ibWhichDependency_t depType);	
	void           deleteDependency(ibWhichDependency_t depType);
	
	bool			Idepend(void);

	virtual void   fillCondDepends(ddbItemList_t&  iDependOn); // it will do the best it can
	virtual void   fillContainList(ddbItemList_t&  iContain);  // over ride by container classes

	// stevev 17nov05 - called by items i depend on
	virtual int    notification(hCmsgList& msgList,bool isValidity);

	// stevev 12dec08 - containers have to mark contents
	virtual void   MakeStale(void) { return; }; // this is the default for non-container classes 
	
	RETURNCODE notifyUpdate(hCmsgList& msgList);// sends the message list

	virtual RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ) // expected to be overidden
							{/* most are 1 */returnedSize.xval=returnedSize.yval = 1; return 0;};

	// accessors
#ifdef CPM_64_BIT_OS	
    itemID_t      getID(void)      { return itemId; };
#else
    unsigned int  getID(void)      { return static_cast<unsigned int>(itemId);}
#endif
//	CitemType&    getType(void)    { return itemType; };
	itemType_t    getIType(void)   { return ((itemType_t)itemType.getType()); };
	unsigned int  getTypeVal(void) { return itemType.getType();};
	char*         getTypeName(void){ return itemType.getTypeStr();};
	//unsigned long getKey(void)     { return DDkey;};
	unsigned int  getSize(void)    { return itemSize; };
/* VMKP added on 291203 */
	bool  IsConditional(void)    { return isConditional; };
/* VMKP added on 291203 */
	// stevev 02nov05 - added for menu's items validity dependency
	// stevev 07jan09 - made virtual so a list can return true all the time
	virtual bool isValidityConditional(void);
	// end add 02dec05
	string&       getName(void) ;
	wstring&      getWName(void);

	void          setName(string& s);// only works on psuedo variables
	hCattrLabel*  getLabelAttr(void)   { return pLabel;};
	void          setPsuedo(elementID_t elem);// sets the back values for 'resolved references'

	virtual                    /* stevev 11nov04 - add self write to support file capability */
		RETURNCODE fileWrt(hCformatData*  pData);
    virtual 
		RETURNCODE Label(wstring &l) RPVFC( "hCitemBase",0 );          /*returns the label of the item (or empty)*/
	virtual // stevev - WIDE2NARROW char interface
		RETURNCODE Label( string &l) RPVFC( "hCitemBase",0 );

    virtual bool HasHelp(){
#ifdef _DEBUG
		if (pHelp == NULL)
		{
//			clog <<"help attr:0x"<<hex<<itemId<<" of "<< itemType.getTypeStr() <<dec << " Does NOT exist."<<endl;
			return false;
		}
		else
		{
//			clog <<"help attr:0x"<<hex<<itemId<<" of "<< itemType.getTypeStr() <<dec << " Does exist."<<endl;
			return true;
		}
	};
#else
		return (pHelp!=NULL);};		/*returns true if help is available*/
#endif
    virtual RETURNCODE Help(wstring &h);// { h="VirtualBase Help";return FAILURE;};	/*returns the help string (or null)*/
    virtual RETURNCODE Help( string &h);	/*returns the help string (or null)*/

    virtual bool        IsValid();     /*returns true if the item is currently valid*///return validity
	virtual bool        IsValidTest(); /*returns true if valid and CACHED (does not read to resolve) */
	virtual bool        is_UNKNOWN(void);/* stevev 21jan05---tests for 3rd state */
	virtual void        setDependentsUNK(void);/* for dependentents to be recalculated */

	virtual bool IsDynamic(void)  { return false; }; //the only class the counts
	virtual bool IsReadOnly(void) { return false; }; // these need to be overridden in the hCvar class
	virtual bool IsVariable(void) { return false; };
	virtual bool IsReallyCritical(void)   { return isReallyCritical;};//stevev 23feb07 - filter the truely critical		
	virtual void setReallyCritical(bool nxt){isReallyCritical = nxt;};//stevev 23feb07 - filter the truely critical		

	virtual hCattrBase*  getaAttr(varAttrType_t attrType);/*Variables ONLY - override for others*/
	virtual RETURNCODE   delistAttr( hCattrBase* pAttr ); // stevev 13jul07 - remove from list -
	/* stevev 14Jul05 - used to resolve attribute references 
	                  - return val may be isSymID             */
	virtual CValueVarient getAttrValue(unsigned attrType, int which = 0);

	// 03aug06 - support for attribute references for label & help	
	// 05sep06 - moved from below and added which to these  for entry into virtual for min-max
	virtual itemID_t    getAttrID (unsigned  attr, unsigned which = 0);
	virtual hCVar*		getAttrPtr(unsigned  attr, unsigned which = 0);

	virtual class hCattrGroupItems* getMemberList(void){ return NULL;		};// only certain classes can do this


	//// construct/destruct ////
	hCitemBase(DevInfcHandle_t h); // : hCObject(h)

	hCitemBase(const hCitemBase& ib) :hCobject((hCobject)ib){*this=ib;};
	hCitemBase( hCitemBase* pSrc, itemIdentity_t newID);/* from-typedef constructor */
	hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, itemID_t  iId,  unsigned long Dkey);
	hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, ITEM_SIZE  iSz);
	hCitemBase(DevInfcHandle_t h, aCitemBase* apIb );// from dllapi class
	virtual ~hCitemBase();

	/*
		LINUX_PORT - REVIEW_WITH_HCF: hCerrDeviceSrvc::getItemBySymNumber 
		'allocation' (NULL assignment) triggers compiler error because 
		marking 'destroy' is pure virtual here. There is actually an 
		implementation in ddbItemBase.cpp, so it shouldn't be pure 
		virtual.
	*/
	virtual RETURNCODE destroy(void);

	// - force all items to build their own attributes
	virtual hCattrBase*  newHCattr(aCattrBase* pACattr)  // builder 
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: item:%s accessed the base class 'newHCattr'.\n",getTypeName());
		return NULL; /* an error */  
	};

	///// overloads /////
	hCitemBase& operator=(const hCitemBase& ib);

	
	hCitemBase& operator=(const aCitemBase& ib) {
		if ( &ib == NULL ) return *this;
	 DDkey =       devPtr()->getDDkey();//was 0xffffffff; //ib.DDkey;
//	 pDevice =	   NULL;       //ib.pDevice;
	 itemId =      ib.itemId;
	 itemType =    ib.itemType;
	 itemSize =    ib.itemSize;
	 itemSubType = (long&) (ib.itemSubType);	
/* VMKP added on 291203 */
 	 isConditional = ib.isConditional;
/* VMKP added on 291203 */
	 itemName =    (ib.itemName.c_str()); // note that straight equal copies the ptr (to dll mem)	
	  pValid  =     NULL;		//ib.pValid;
	  pLabel  =     NULL;		//ib.pLabel;
	  pHelp   =     NULL;		//ib.pHelp;
	  pDebug  =     NULL;
// ib.AattributeList_t attrLst;

	return *this;
	};

	/*** commented out code removed 25may05 - see earlier versions */

	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);  // to cout
//	RETURNCODE setDevice( CddbBaseDevice*   pd) {  pDevice = pd; return SUCCESS; };

	virtual RETURNCODE resolveProcure(void) /* resolve this and all the sub stuff*/
			{ LOGIT(CERR_LOG,"Call to itembase virtual resolve Procure. item:"
															"0x%x\n",itemId); return VIRTUALCALLERR;};
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true) // only coll&itmArr
			{LOGIT(CERR_LOG,"Call to itembase virtual getByIndex.\n"); return VIRTUALCALLERR;};
//**	virtual RETURNCODE getByIndex(UINT32 indexValue, hCreference& returnedItemRef) // only coll&itmArr
//**			{ cerrxx<<"Call to itembase virtual getByIndex."<< endl; return VIRTUALCALLERR;};
	virtual RETURNCODE getAllindexes(vector<UINT32>& allindexedIDsReturned)// only coll&itmArr
			{ LOGIT(CERR_LOG,"Call to itembase virtual getAllindexes.\n"); return VIRTUALCALLERR;};
	virtual RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes)// only coll&itmArr
			{ LOGIT(CERR_LOG,"Call to itembase virtual getAllindexValues.\n"); return VIRTUALCALLERR;};
	virtual RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs)
			{ LOGIT(CERR_LOG,"Call to itembase virtual getAllByIndex.\n"); return VIRTUALCALLERR;};

	operator hCVar*() { if(IsVariable() && this != NULL){ return (hCVar*)this;}else{ return (hCVar*)NULL;} };

	// // Actions // //
	virtual
		RETURNCODE doPreEditActs(void){ return SUCCESS;};// override if your item has an action
	virtual
		RETURNCODE doPstEditActs(void){ return SUCCESS;};// edit displays and variables for now

//protected:
	// utility functions

//	RETURNCODE fetchBinaryItem(void** ppRetItem);

//	RETURNCODE getAttr( UINT32 varAttrMsk /*attrBase.attr_mask*/, CattrBase** pAttr );
//	RETURNCODE restoreDBstate(void);
//	RETURNCODE fillDBstruct(struct itemTbl& dS);
#ifndef NEWATTRNOTIMPLEMENTED
#define NEWATTRNOTIMPLEMENTED(a) (LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:item newattr NOT IMPLEMENTED in %s\n", a ))
#endif
	// pure virtual::children MUST overload

	friend class hCdependency;

// L&T Modifications : VariableSupport - start
public:
	virtual RETURNCODE doPstEditActsForVS(int temp){ return SUCCESS;};
// L&T Modifications : VariableSupport - end

};


#endif



/*************************************************************************************************
 *
 *   $History: ddbItemBase.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
