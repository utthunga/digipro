/*

critSect.h

  #include "critSect.h"

*/




#ifndef __CRITSECT_H
#define __CRITSECT_H


#ifndef _CRITSECT_CPP
// not in the actual cpp file
//#include "windows.h"

#ifdef RTL_CRITICAL_SECTION /* CRITICAL_SECTION */
#error "Device Object does not allow inclusion od OS dependent files."
// this is only includable in dev object and the critSect.cpp file
#endif

//#define DDBCRITICAL_SECTION  csCriticalSection
//#define LeaveCriticalSection( a )		( a )->Leave()
//#define EnterCriticalSection( a )		( a )->Enter()
//#define InitializeCriticalSection( a )	( a )->Init()
//#define DeleteCriticalSection( a )     delete ( a );

#endif

extern unsigned long ddbGetTickCount(void);
extern long csGetCurrentThreadId(void);

class csCriticalSection
{
	int csIdx;

public:
	csCriticalSection();
	~csCriticalSection();

	void Leave(void);
	void Enter(void);
	void Init (void);
	void DeleteCS();
};

#endif