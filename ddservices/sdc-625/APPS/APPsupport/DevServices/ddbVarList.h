/*************************************************************************************************
 *
 * $Workfile: ddbVarList.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		generated from from a portion of previous version of ddbItemLists.h
 *
 * #include "ddbVarList.h"
 */

#ifndef _DDBVARLIST_H
#define _DDBVARLIST_H

// 15nov11 #include "ddbItems.h"/* loads all the includes for items almost all the uncludes)*/
#include "ddbVar.h"// 15nov11

#include "ddb.h"

#include "ddbItemListBase.h"

class CVarList : public CitemListBase<iT_Variable, hCVar> // list of ptrs 2 hCVars
{
//	RETURNCODE instantiateList(CddbBaseDevice* pDev, AitemList_t* pAItemList);
	RETURNCODE instantiateList(DevInfcHandle_t h, AitemList_t* pAItemList);

	RETURNCODE getNeededList(cmdDescList_t& cmdDescLst, bool isWrt);// 15nov11-varOptimization
public:
	CVarList(DevInfcHandle_t h) : CitemListBase<iT_Variable, hCVar>(h){};

//	RETURNCODE populate(CddbBaseDevice* pDev, UINT32 theDDkey, aCdevice* pAbstractDevice );
	/* stevev 021004 - added isServer for XMTR-DD initialization :: server == a Device */
	RETURNCODE populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl,
						DD_Key_t theDDkey, aCdevice* pAbstractDevice = NULL,bool isServer = false);
	int  bumpAllVars(void);
	bool isChanged(void); // if any var returns true, this is true
	void markAllItemState(INSTANCE_DATA_STATE_T newSt );// for all of 'em
	void setDefaultValues(void);/* stevev - added 4/22/04 */
	void registerListIndexes(void);/* stevev 08jan09 */

	RETURNCODE getNeededRead(cmdDescList_t& cmdDescLst)// 15nov11-varOptimization
	{	return getNeededList(cmdDescLst,false);   };
	RETURNCODE getNeededWrit(cmdDescList_t& cmdDescLst)// 15nov11-varOptimization
	{	return getNeededList(cmdDescLst,true);   };

#ifdef _DEBUG
	void varNotificationReport();
	void varDependencyReport();
#endif
};


#endif	//_DDBVARLIST_H
/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
