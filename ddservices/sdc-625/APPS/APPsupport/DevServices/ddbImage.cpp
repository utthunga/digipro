/*************************************************************************************************
 *
 * $Workfile: ddbImage.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This holds the DD item whose path attribute is an index into the list of images which
 *		hold the multiple languages and the raw image constructs.
 *
 *		08/30/04	sjv	started creation
 */
#include "ddbImage.h"
#include "logging.h"
#include "ddbCommand.h" // to get msgList

/* the global mask converter...you have to cast the return value*/
extern int  mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];


hCimageItem::hCimageItem(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pLink(NULL), pPath(NULL)
{// label, help validity are set in the base class
	// Required::>
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(imageAttrPath); 
	if (aB == NULL)
	{	
		if (pPath){pPath->destroy(); delete pPath; pPath = NULL;}
		LOGIT(CERR_LOG,"ERROR: hCimageItem::hCimageItem<no image path attribute supplied.>\n");
		LOGIT(CERR_LOG, "       trying to find: #%d but only have:", imageAttrPath);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG, " %d, ", (*iT)->attr_mask);}
		}
		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		pPath = (hCattrPath*)newHCattr(aB);
		if (pPath != NULL)
		{
			pPath->setItemPtr((hCitemBase*)this);
// we will handle this outside of the base class
//			attrLst.push_back(pPath);// keep it on the list too
		}
		else // is NULL
		{
			imageAttrType_t en = (imageAttrType_t)mask2value(aB->attr_mask); 
			LOGIT(CERR_LOG,"++ No New attribute ++ (&s)\n", imageAttrStrings[en]);
		}
	}

	// NOT required			
		
	aB = paItemBase->getaAttr(imageAttrLink); // uses aCattrCondRef	- could be a string
	if (aB != NULL)
	{
		pLink = (hCattrLink*)newHCattr(aB);
		if (pLink != NULL)
		{
			pLink->setItemPtr((hCitemBase*)this);
// we will handle this outside of the base class
//			attrLst.push_back(pLink);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Link attribute for Image ++ (memory error?)\n");
		}
	}
}

/* from-typedef constructor */
hCimageItem::hCimageItem(hCimageItem*    pSrc,  itemIdentity_t newID):
		 hCitemBase((hCitemBase*)pSrc,newID)
{
LOGIT(CERR_LOG,"***** hCimageItem typedef constructor needs a body.*****\n");
}

/* Label, Help  and Validity should be handled by the item base class */
hCattrBase*   hCimageItem::newHCattr(aCattrBase* pACattr)  // builder 
{

	if (pACattr->attr_mask == maskFromInt(imageAttrPath)) 
	{
		return 
			new hCattrPath(devHndl(), imageAttrPath, (aCattrCondLong*)pACattr, this );		
	}
	else
	if (pACattr->attr_mask == maskFromInt(imageAttrLink))
	{
		return 
			new hCattrLink(devHndl(), imageAttrLink, (aCattrCondReference*)pACattr, this );
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(imageAttrLabel) | 
			 maskFromInt(imageAttrHelp)  | 
			 maskFromInt(imageAttrValidity) | 
			 maskFromInt(imageAttrDebugInfo) ))
	{//known attribute but shouldn't be here		
		LOGIT(CERR_LOG, "ERROR: image attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIT(CERR_LOG, "ERROR: image attribute type is unknown.\n");
		return NULL; /* an error */  
	}
};


int  hCimageItem::getImageIndex(void)
{
	if ( pPath == NULL )
	{
		LOGIT(CERR_LOG,"Image  in hCimageItem with NO descriptor.\n");
		return -1;
	}
	// else...
	ulong idx = -1;
	hCddlLong* pLng = (hCddlLong*)	pPath->procure();
	if ( pLng != NULL ) // can be NULL on a un-handleable conditional
		idx = (UINT32)pLng->getDdlLong();// very limited value set
	else
		idx = -1;
	return ((int)idx);
}


bool hCimageItem::isLinkString() // returns if the Link is a constant string
{
	if ( pLink == NULL )
	{
		return false;
	}
	else
	{
		hCreference* pR = (hCreference*)pLink->procure();
		if ( pR == NULL )
		{
			return false;
		}
		else
		if ( pR->getRtype() == rT_Constant ) // assume it's a const string
		{
			return true;
		}
		else // must be an item ref
		{
			return false;
		}
	}
}


RETURNCODE hCimageItem::getLink(string& retString)
{
	RETURNCODE rc = FAILURE;
	if ( pLink != NULL )
	{
		hCreference* pR = (hCreference*)pLink->procure();
		if ( pR != NULL )
		{
			if ( pR->getRtype() == rT_Constant ) // assume it's a const string
			{
				CValueVarient rV;
				if ( (rc = pR->resolveExpr(rV)) == SUCCESS )
				{
					if ( rV.vType == CValueVarient::isString )
					{
						retString = (string)rV;
						// leave rc success
					}
					// else - don't return other types
				}
				// else return the return code
			}
			// else // must be an item ref - fail
		}
		// else procure failure
	}
	return rc;
}


RETURNCODE hCimageItem::getLink(hCitemBase*& retString)// reference to a pointer to set
{
	RETURNCODE rc = FAILURE;
	if ( pLink != NULL )
	{
		hCreference* pR = (hCreference*)pLink->procure();
		if ( pR != NULL && ! pR->isEmpty())
		{
			if ( pR->getRtype() != rT_Constant ) // assume it's a const string
			{
				rc = pR->resolveID( retString);
			}
			// else // must be a const Ref - fail
		}
		// else procure failure or empty else statement

	}
	return rc;
}


CValueVarient hCimageItem::getAttrValue(unsigned attrType, int which)
{	
	CValueVarient rV;

	imageAttrType_t iat = (imageAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( iat >= imageAttr_Unknown )
	{
		LOGIT(CERR_LOG,"ERROR: Image's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request
	
	switch (iat)
	{
	case imageAttrLabel:
	case imageAttrHelp:
	case imageAttrValidity:
		rV = hCitemBase::getAttrValue(iat);		
		break;
	case imageAttrLink:
		{// TODO
			LOGIT(CERR_LOG,"WARN: imageLink information is not accessable.\n");
		}
		break;
	case imageAttrPath:
		{// TODO
			LOGIT(CERR_LOG,"WARN: imagePath information is not accessable.\n");
		}
		break;
	case imageAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIT(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

/* stevev 16nov05 - dependency extentions */
void  hCimageItem::fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// the only other attribute:
	if ( pPath != NULL ) 
	{
		pPath->getDepends(iDependOn);
	}	 


}


/* stevev 18nov05 - overload to get var specific notifications */

// called when an item in iDependOn has changed
int hCimageItem::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals 
	int init = msgList.size();
	
	hCitemBase::notification(msgList,isValidity);// do validity and label

	// re-evaluate all the others ( handling is only one in this base class)
	if ( (! isValidity) && pPath != NULL && pPath->isConditional() && pPath->reCalc())
	{// my path changed
		msgList.insertUnique(getID(), mt_Str, pPath->getGenericType());// tell the world
	}// else - no change;

	return (msgList.size() - init);// number added
}
/** end stevev 18nov05  **/

/** end stevev 27sep06 **/
/* stevev 01nov06 - layout */
RETURNCODE  hCimageItem::setSize( hv_point_t& returnedSize, ulong qual )
{ 	
	bool ret = false;
	int	itemIndex = getImageIndex();

	if (itemIndex >= 0)
	{	unsigned x,y;			//						isInLine
		ret = devPtr()->sizeImage(itemIndex,x,y,((qual & menuItemInLine)!=0));
		returnedSize.xval= x;     returnedSize.yval = y; 
	}
	if ( ret )
		return FAILURE;
	else
		return SUCCESS;
}
/** end stevev 01nov06 **/