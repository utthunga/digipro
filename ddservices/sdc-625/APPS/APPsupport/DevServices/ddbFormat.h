/*************************************************************************************************
 *
 * $Workfile: ddbFormat.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:  Holds all of base format structures
 *		
 *		09nov04	sjv	created 
 *
 *		
 * #include "ddbFormat.h"
 */


#ifndef _DDB_FORMAT_H
#define _DDB_FORMAT_H

#include "ddbGlblSrvInfc.h"
#include "ddbdefs.h"
#include <list>

#define INDENT_SIZE  (4)
#define MAX_ITEM_LEN (1024)
#define HDR_SIZE     (400)
// headroom is needed to be sure the trailing \0 is accounted for and we can add a \n if needed
#define STRHEADROOM   2

typedef enum FileFormat_e
{
	FMT_Undefined,
	FMT_HCFformat,	// * default* //that's the only one for now
	FMT_Unsupported // must be last
}FileFormat_t;

// forward reference
class hCformatBase;

// external (global) reference
extern hCformatBase* getNewFormat(FileFormat_t ft);


class hCdataElement
{
public:
	unsigned char* buffer;
	int            length;

public:
	void clear() { destroy(); };
	void destroy() // anil 23aug07 - memory leak
	{ if ( buffer != NULL) { delete[] buffer;} buffer = NULL;length = 0;};

	RETURNCODE insert(const char* value, int len);
	hCdataElement& operator=(const hCdataElement& de);

	hCdataElement() : buffer(NULL), length(0) {};	
	hCdataElement(const hCdataElement& de) { operator=( de ); };	
	virtual ~hCdataElement() { destroy(); };
};

//typedef vector<hCdataElement*>    dataPtrList_t;
typedef list<hCdataElement*>     dataPtrList_t;
typedef dataPtrList_t::iterator  dataIterator_t;
typedef dataPtrList_t::reverse_iterator  dataDITerator_t;


/*  this class is used to pass down the heirarchy to collect data */
/*  a list of pointers to data elements                           */

class hCformatData : public dataPtrList_t
{
public:
	int            indent;
	hCformatBase*  pFormat; // so we don't constantly look it up
	dataIterator_t insertionPoint;

public:
	hCformatData(hCformatBase* pF, hCdeviceSrvc* p): 
				pFormat(pF),insertionPoint(begin()),indent(0) 
	{	};
	virtual
		~hCformatData(){ destroy();};

	void Insert(hCdataElement* pE); // put item at insertion point, update insertion point
	void Add   (hCdataElement* pE); // put item at insertion point, DO NOT update    point
	void destroy()
	{  
		for ( dataIterator_t i=begin(); i != end(); ++i) 
		{
			(*i)->destroy();
			// anil 23aug07 - memory leak
			hCdataElement* phCdataElement = (*i);
			if(phCdataElement)
				delete phCdataElement ;// destroy's then deletes
			// end anil 23aug07
		} 
		dataPtrList_t::clear();
		insertionPoint = begin();
	};
	void clear(void)  { destroy(); };
	void Indent(void) {indent += INDENT_SIZE;};
	void Undent(void) {indent -= INDENT_SIZE;if(indent<0)indent=0;};
};


extern char itemStrings[ITMSTRCOUNT][ITMAXLEN];
class hCitemBase;
class hCgroupItemDescriptor;
class hCfile;

/* derive formats from this */
class hCformatBase
{

public: /* **>HCF/header/error format<**  too useful to protect */
	virtual /* prepends indent, no trailing newline */
	RETURNCODE insertStr(hCformatData* pData, char* pC  ); // helper function
	virtual /* prepends indent, no trailing newline */
	RETURNCODE insertStr(hCformatData* pData, const string& rS); // helper
	virtual /* prepends indent, adds trailing newline   -- single line object */
	RETURNCODE insertStr(hCformatData* pData, const string& S, char* keyStr, char* typeStr=NULL);
	virtual /* prepends indent, adds trailing newline   -- single line object */
	RETURNCODE insertStr(hCformatData* pData, char*  pS, char* keyStr, char* typeStr=NULL)
	{ string S(pS); return insertStr(pData,S,keyStr,typeStr);};
	virtual /* prepends indent, adds trailing newline   -- single line object */
	RETURNCODE insertInt(hCformatData* pData, int D,char* keyStr,int radix=10,char* typeStr=NULL);

public:
	FileFormat_t   myType;
	hCdeviceSrvc*  pDEV;    // we need it to do any real work
	hCformatData*  pWrkData;// working data
	hCfile*        pBaseFile;

	hCformatBase():pDEV(NULL),pWrkData(NULL),myType(FMT_Undefined),pBaseFile(NULL) {};
	virtual
	~hCformatBase(){};
	void destroy(void){ DESTROY(pWrkData); };

	// disk-file header/footer NOT virtual - must always be the same -
	RETURNCODE insertHeader(hCformatData* pData, const Indentity_t& ident);
	RETURNCODE insertFooter(hCformatData* pData);

	virtual// the only non-item insertion object
	RETURNCODE insertMember(hCformatData* pData, hCgroupItemDescriptor* pgiD);
	virtual// all else
	RETURNCODE insertObject(hCformatData* pData, hCitemBase* pInfo);
	virtual	
	RETURNCODE insertObjectEnd(hCformatData* pData, hCitemBase* pInfo);

	// fills indentity & format and reads thru <HFDbody>
	// pointer and bytes left are modified at exit
	static
	RETURNCODE extractHeader(unsigned char*& pNxtByte, int& bytesLeft,
															Indentity_t& retIdent, int& retFmt);
	virtual // call with length Zero & NULL ptr to terminate
	RETURNCODE extractData(unsigned char*& pNxtByte, int& bytesLeft);

};


class hCundefinedFormat : public hCformatBase
{
public:
};


class hCunSupportedFormat : public hCformatBase
{
public:
};


#endif /*_DDB_FORMAT_H*/
