/*************************************************************************************************
 *
 * $Workfile: ddbCntMutex.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the api to an OS specific counting Mutex.
 *		The application must generate the code (usually by subclassing this class)
 *
 *		
 * #include "ddbCntMutex.h".
 */

#ifndef _DDBCNTMUTEX_H
#define _DDBCNTMUTEX_H

#include "ddbMutex.h"	// includes general & event
#include "xplatform_thread.h"
#include <deque>

// designed to encapsulate thread to thread protection mechanism
//		if a thread asks for an re-entry to itself, the count is inc'd
//		other threads are queued to be signaled when the count goes to zero
//		One of these classes for each protected resource, may entered by all threads
class hCcountingMutex		
{   
	hCmutex				hCntMutex;     // critical section mutex
hCmutex::hCmutexCntrl*	pCntMutex;     // used to prevent the allocation/deallocation of the controls

	THREAD_ID_TYPE activeThreadID;		// current thread id;0 = nobody has the mutex, -1 @ somebody gave it up & signaled another
	int   activeEntryCnt;		// count of entries/re-entries
	vector<hCevent*>  mv_UnusedEvents;	// all events must be in one of these two
	deque< hCevent*>  mdq_PendingEvts;	// pending events (threads)

	hCevent* getNewEvent(void);// gets an usused or new event pointer

public:
	RETURNCODE getCountingMutex(unsigned long ultimeout = hCevent::FOREVER );
	RETURNCODE putCountingMutex(void);

	int getEntryCount(void){return activeEntryCnt;};

public:	
#ifdef _DEBUG
	string cntMname;
	hCcountingMutex(char* nm);
#else
	hCcountingMutex();
#endif

	virtual ~hCcountingMutex();// delete events
};


#endif // _DDBCNTMUTEX_H
