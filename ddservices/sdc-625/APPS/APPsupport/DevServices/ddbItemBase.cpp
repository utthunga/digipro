/*************************************************************************************************
 *
 * $Workfile: ddbItemBase.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		method code for the base-base class for all items 
 *		9/4/2	sjv	 creation
 */

#include "ddbItemBase.h"
#include "ddbAttributes.h"
/*Vibhor 220104: Start of Code*/
#include "logging.h"
/*Vibhor 220104: End of Code*/
#include "ddbDependency.h"
#include "ddbCommand.h" // for mshlist

hCitemBase::hCitemBase(DevInfcHandle_t h) : hCobject(h)
{
	DDkey       = devPtr()->getDDkey();

	itemId      = 0;
	itemType    = 0;

	itemSize    = 0;
	itemSubType = 0;
/* VMKP added on 291203 */
	isConditional = false;
/* VMKP added on 291203 */
	sharedAttr = false;// stevev 25jul06

	itemName.erase();

	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
	m_pValidityDepends    = new hCdependency(this);
	m_pConditionalDepends = new hCdependency(this);
	m_pCommonDepends      = new hCdependency(this);// stevev 14sep06 - must consolidate  
	m_pContainer          = new hCdependency(this);// stevev 27sep06 - parents  
	clog << "Itembase construction without any initializers...what's with this?"<<endl;

	pPsuedoLabel = pPsuedoHelp = NULL;// instantiated on demand
	ownerID      = 0;
	ownerIndex   = 0;
	isReallyCritical = false; // stevev 23feb07
}


hCitemBase::hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, ITEM_SIZE  iSz)
	: hCobject(h), itemType(iType)
{	
	DDkey       = devPtr()->getDDkey();

	itemId      = 0;

	itemSize    = iSz;
	itemSubType = 0;
/* VMKP added on 291203 */
	isConditional = false;
/* VMKP added on 291203 */
	sharedAttr = false;// stevev 25jul06

	itemName.erase();

	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
	
	m_pValidityDepends    = new hCdependency(this);
	m_pConditionalDepends = new hCdependency(this);
	m_pCommonDepends      = new hCdependency(this);// stevev 14sep06 - must consolidate
	m_pContainer          = new hCdependency(this);// stevev 27sep06 - parents  

	pPsuedoLabel = pPsuedoHelp = NULL;// instantiated on demand
	ownerID      = 0;
	ownerIndex   = 0;
	isReallyCritical = false; // stevev 23feb07
}

// construction from dllapi class
hCitemBase::hCitemBase(DevInfcHandle_t h, aCitemBase* apIb) : hCobject(h)
{
	sharedAttr = false;// stevev 25jul06
	/* stevev 7aug06 - just build one */
	if (apIb == NULL)
	{	pValid = NULL;
		pLabel = NULL;
		pHelp  = NULL;
		pDebug = NULL;
		isConditional = false;
		return;
	}
	// get all we can - easily
	operator=(*apIb);// just clears DDkey, pDevice,     pValid,pLabel,pHelp, copies what it can
	/* stevev - 10aug06 - need to handle an empty id for static var constructor */
	if (itemId == 0)// aC class did not supply an ID - probably from static var generator*/
	{
		itemId = devPtr()->getAPsuedoID();
		
		// we have to allow lookups
		devPtr()->registerItem(itemId, this, getSymbolName());//
	}
	// get what we need from the attribute collection
	// There is no standard key for any given attribute set.
	// We have to aquire the correct key by item type
	genericTypeEnum_t  ValidTarget, LabelTarget, HelpTarget, DebugTarget;
	itemType_t    thisType = (itemType_t)(itemType.getType());
	ValidTarget = LabelTarget = HelpTarget = DebugTarget =
							(genericTypeEnum_t)0xffffffff;// zero is a valid attribute type
	pPsuedoLabel = pPsuedoHelp = NULL;// instantiated on demand
	ownerID      = 0;
	ownerIndex   = 0;
	isReallyCritical = false; // stevev 23feb07
	
	//set the target types
	switch (thisType)
	{
	case iT_Variable:				// 1
		ValidTarget = (genericTypeEnum_t)varAttrValidity;// set the target(s) enum varAttrType_e
		LabelTarget = (genericTypeEnum_t)varAttrLabel;
		HelpTarget  = (genericTypeEnum_t)varAttrHelp;
		DebugTarget = (genericTypeEnum_t)varAttrDebugInfo;
		break;
	case iT_Command:				// 2
		DebugTarget = (genericTypeEnum_t)cmdAttrDebugInfo;
		break;
	case iT_Menu:					// 3
		ValidTarget = (genericTypeEnum_t)menuAttr_Validity;
		LabelTarget = (genericTypeEnum_t)menuAttr_Label;
		HelpTarget  = (genericTypeEnum_t)menuAttr_Help;
		DebugTarget = (genericTypeEnum_t)menuAttrDebugInfo;
		break;
	case iT_EditDisplay:			// 4
		ValidTarget = (genericTypeEnum_t)eddispAttrValidity;
		LabelTarget = (genericTypeEnum_t)eddispAttrLabel;
		HelpTarget  = (genericTypeEnum_t)eddispAttrHelp;
		DebugTarget = (genericTypeEnum_t)eddispAttrDebugInfo;
		break;
	case iT_Method:					// 5
		ValidTarget = (genericTypeEnum_t)methodAttrValidity;
		LabelTarget = (genericTypeEnum_t)methodAttrLabel;
		HelpTarget  = (genericTypeEnum_t)methodAttrHelp;
		DebugTarget = (genericTypeEnum_t)methodAttrDebugInfo;
		break;
	case iT_Refresh:/*relation*/	// 6
		DebugTarget = (genericTypeEnum_t)refreshAttrDebugInfo; break;
	case iT_Unit:/*relation*/		// 7
		DebugTarget = (genericTypeEnum_t)unitAttrDebugInfo;    break;
	case iT_WaO:					// 8
		DebugTarget = (genericTypeEnum_t)waoAttrDebugInfo;     break;
		break;
	case iT_ItemArray:				// 9
		ValidTarget = (genericTypeEnum_t)grpItmAttrValidity;
		LabelTarget = (genericTypeEnum_t)grpItmAttrLabel;
		HelpTarget  = (genericTypeEnum_t)grpItmAttrHelp;
		DebugTarget = (genericTypeEnum_t)grpItmAttrDebugInfo;
		break;
	case iT_Collection:				//10
		// never has validity : ValidTarget = (genericTypeEnum_t)grpItmAttrValidity;
		LabelTarget = (genericTypeEnum_t)grpItmAttrLabel;
		HelpTarget  = (genericTypeEnum_t)grpItmAttrHelp;
		DebugTarget = (genericTypeEnum_t)grpItmAttrDebugInfo;
		break;
	case iT_Block:					//12
		LabelTarget = (genericTypeEnum_t)blockAttrLabel;
		HelpTarget  = (genericTypeEnum_t)blockAttrHelp;
		break;
	case iT_Program:				//13
	case iT_Record:					//14
		LabelTarget = (genericTypeEnum_t)recordAttrLabel;
		HelpTarget  = (genericTypeEnum_t)recordAttrHelp;
		break;
	case iT_Array:					//15
		ValidTarget = (genericTypeEnum_t)arrayAttrValidity;
		LabelTarget = (genericTypeEnum_t)arrayAttrLabel;
		HelpTarget  = (genericTypeEnum_t)arrayAttrHelp;
		DebugTarget = (genericTypeEnum_t)arrayAttrDebugInfo;
		break;
	case iT_VariableList:			//16
		LabelTarget = (genericTypeEnum_t)varlistAttrLabel;
		HelpTarget  = (genericTypeEnum_t)varlistAttrHelp;
		break;
	case iT_ResponseCodes:			//17
	case iT_Domain:					//18
	case iT_Member:					//19
		// leave 'em unusable
		break;

	case iT_File:				//20
		// never has validity : ValidTarget = (genericTypeEnum_t)fileAttrValidity;
		LabelTarget = (genericTypeEnum_t)fileLabel;
		HelpTarget  = (genericTypeEnum_t)fileHelp;
		DebugTarget = (genericTypeEnum_t)fileDebugInfo;
		break;
	case iT_Chart:				//21				
		ValidTarget = (genericTypeEnum_t)chartAttrValidity;
		LabelTarget = (genericTypeEnum_t)chartAttrLabel;
		HelpTarget  = (genericTypeEnum_t)chartAttrHelp;
		DebugTarget = (genericTypeEnum_t)chartAttrDebugInfo;
		break;
	case iT_Graph:				//22				
		ValidTarget = (genericTypeEnum_t)graphAttrValidity;
		LabelTarget = (genericTypeEnum_t)graphAttrLabel;
		HelpTarget  = (genericTypeEnum_t)graphAttrHelp;
		DebugTarget = (genericTypeEnum_t)graphAttrDebugInfo;
		break;
	case iT_Axis:				//23				
		ValidTarget = (genericTypeEnum_t)axisAttrValidity;
		LabelTarget = (genericTypeEnum_t)axisAttrLabel;
		HelpTarget  = (genericTypeEnum_t)axisAttrHelp;
		DebugTarget = (genericTypeEnum_t)axisAttrDebugInfo;
		break;
	case iT_Waveform:			//24							
		ValidTarget = (genericTypeEnum_t)waveformAttrValidity;// stevev 12jul07 - added to get rid od error message
		LabelTarget = (genericTypeEnum_t)waveformAttrLabel;
		HelpTarget  = (genericTypeEnum_t)waveformAttrHelp;
		DebugTarget = (genericTypeEnum_t)waveformAttrDebugInfo;
		break;
	case iT_Source:				//25				
		ValidTarget = (genericTypeEnum_t)sourceAttrValidity;
		LabelTarget = (genericTypeEnum_t)sourceAttrLabel;
		HelpTarget  = (genericTypeEnum_t)sourceAttrHelp;
		DebugTarget = (genericTypeEnum_t)sourceAttrDebugInfo;
		break;
	case iT_List:				//26				
		ValidTarget = (genericTypeEnum_t)listAttrValidity;
		LabelTarget = (genericTypeEnum_t)listAttrLabel;
		HelpTarget  = (genericTypeEnum_t)listAttrHelp;
		DebugTarget = (genericTypeEnum_t)listAttrDebugInfo;
		break;
	case iT_Grid:				//27							
		ValidTarget = (genericTypeEnum_t)gridAttrValidity;
		LabelTarget = (genericTypeEnum_t)gridAttrLabel;
		HelpTarget  = (genericTypeEnum_t)gridAttrHelp;
		DebugTarget = (genericTypeEnum_t)gridAttrDebugInfo;
		break;
	case iT_Image:				//28				
		ValidTarget = (genericTypeEnum_t)imageAttrValidity;
		LabelTarget = (genericTypeEnum_t)imageAttrLabel;
		HelpTarget  = (genericTypeEnum_t)imageAttrHelp;
		DebugTarget = (genericTypeEnum_t)imageAttrDebugInfo;
		break;

	case iT_FormatObject:
	case iT_DeviceDirectory:		//129
	case iT_BlockDirectory:			//130

	case iT_Parameter:
	case iT_ParameterList:			//201
	case iT_BlockCharacteristic:	//202
	default:
		// leave 'em unusable
		break;
	}// end switch on item type

    // ib.AattributeList_t attrLst;// a typedef vector<aCattrBase*>	AattributeList_t;
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = apIb->getaAttr(ValidTarget); // method & var MUST be the same attribute struct
	if (aB == NULL){ pValid = NULL; } 
	else // generate a hart class from the abstract class
	{ 
		pValid = new hCattrValid  (h, (aCattrCondLong*)aB) ; 
		pValid->setItemPtr(this); 
	}

	aB = apIb->getaAttr(LabelTarget); // all labels must use the same structure
	if (aB == NULL){ pLabel = NULL; } 
	else  // generate a hart class from the abstract class
	{ pLabel = new hCattrLabel  (h,  (aCattrString*)aB); pLabel->setItemPtr(this); }
	
	aB = apIb->getaAttr(HelpTarget) ; // all helps must use the same structure
	if (aB == NULL){ pHelp  = NULL; }
	else  // generate a hart class from the abstract class
	//was  { pHelp  = new hCattrHelp(h, *((aCattrHelp*)aB) ); pHelp->setItemPtr(this); }	
	{ 
		pHelp  = new hCattrHelp   (h,  (aCattrString*)aB);  
		pHelp-> setItemPtr(this); 
//		clog <<"help attr:0x"<<hex<<itemId<<" of "<< itemType.getTypeStr() <<dec <<endl;
	}

	aB = apIb->getaAttr(DebugTarget); // all debugs must use the same structure
	if (aB == NULL)
	{ 
		pDebug = NULL; 
#ifdef _DEBUG
		//  0x00 @ psuedo     true on load munged
		if (apIb->itemId && ! ( 0x80000000 & apIb->itemId) )// only output for non-munged aC items
		LOGIT(CERR_LOG,"No Symbol aName attribute for Variable 0x%04x\n",apIb->itemId);
		else if ( ! ( 0x80000000 & itemId) )// only output for non-munged this items
		LOGIT(CERR_LOG,"No Symbol Name attribute for Variable 0x%04x\n",itemId);
#endif
	} 
	else  // generate a hart class from the abstract class
	{ 
		pDebug = new hCattrDebug  (h, mask2value(aB->attr_mask),(aCdebugInfo*)aB);
		pDebug->setItemPtr(this); 
#ifdef _DEBUG
		if ( pDebug != NULL && pDebug->getSymbolName() != itemName)
		clog<<"DbgSymbol=|"<<pDebug->getSymbolName()<<"| symSymbol=|"<<itemName<<"|"<<endl;
#endif
	}

	m_pValidityDepends    = new hCdependency(this);
	m_pConditionalDepends = new hCdependency(this);
	m_pCommonDepends      = new hCdependency(this);// stevev 14sep06 - must consolidate
	m_pContainer          = new hCdependency(this);// stevev 27sep06 - parents  

};

hCitemBase::hCitemBase( DevInfcHandle_t h, ITEM_TYPE iType, itemID_t   iId,  unsigned long Dkey )
: hCobject(h)
{
	DDkey       = Dkey;
	//pDevice     = NULL;

	itemId      = iId;
	itemType    = (long)iType;

	itemSize    = 0;
	itemSubType = 0;
/* VMKP added on 291203 */
	isConditional = false;
/* VMKP added on 291203 */	
	sharedAttr = false;// stevev 25jul06
	pPsuedoLabel = pPsuedoHelp = NULL;// instantiating on demand
	ownerID      = 0;
	ownerIndex   = 0;

	itemName.erase();

	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
	
	m_pValidityDepends    = new hCdependency(this);
	m_pConditionalDepends = new hCdependency(this);
	m_pCommonDepends      = new hCdependency(this);// stevev 14sep06 - must consolidate
	m_pContainer          = new hCdependency(this);// stevev 27sep06 - parents  
	isReallyCritical = false; // stevev 23feb07
}


/* from-typedef constructor !! */
hCitemBase::hCitemBase( hCitemBase* pSrc, itemIdentity_t newID) : hCobject(pSrc->devHndl())
{
	
	DDkey			= pSrc->DDkey; 
	itemId			= newID.newID;
	itemType		= pSrc->itemType;

	itemSize		= pSrc->itemSize;
	itemSubType		= pSrc->itemSubType;

	itemName		= newID.newName;	//pSrc->itemName;
	isConditional	= pSrc->isConditional;	

	//hAttrPtrList_t  attrLst;
	pPsuedoLabel = pPsuedoHelp = NULL;// instantiated on demand
	ownerID      = pSrc->ownerID;
	ownerIndex   = pSrc->ownerIndex;


	/* common atributes */
	pValid    = pSrc->pValid;
	pLabel    = pSrc->pLabel;
	pHelp     = pSrc->pHelp;
	pDebug    = pSrc->pDebug;

	sharedAttr= true;
	isReallyCritical = false; // stevev 23feb07

#if 0 // 25jul06----- share attributes to save memory_________________________
/**/	if (pSrc->pValid != NULL)
/**/	{
/**/	    //pValid = new hCattrValid(*(pSrc->pValid));
/**/		//pValid->setItemPtr(this);
/**/		pValid = (hCattrValid*)pSrc->pValid->new_Copy();
/**/	}
/**/	else
/**/	{
/**/		pValid = NULL;
/**/	}
/**/	if (pSrc->pLabel != NULL)
/**/	{
/**/	    //pLabel = new hCattrLabel(*(pSrc->pLabel));
/**/		//pLabel->setItemPtr(this);
/**/		pLabel = (hCattrLabel*)pSrc->pLabel->new_Copy();
/**/	}
/**/	else
/**/	{
/**/		pLabel = NULL;
/**/	}
/**/	if (pSrc->pHelp != NULL)
/**/	{
/**/	    //pHelp = new hCattrHelp (*(pSrc->pHelp));
/**/		//pHelp->setItemPtr(this);
/**/		pHelp = (hCattrHelp*)pSrc->pHelp->new_Copy();
/**/	}
/**/	else
/**/	{
/**/		pHelp = NULL;
/**/	}
/**/	if (pSrc->pDebug != NULL)
/**/	{
/**/	    //pDebug = new hCattrDebug(*(pSrc->pDebug));
/**/		//pDebug->setItemPtr(this);
/**/		pDebug = (hCattrDebug*)pSrc->pDebug->new_Copy();
/**/		pDebug->setSymbolName(newID.newName);
/**/	}
/**/	else
/**/	{
/**/		pDebug = NULL;
/**/	}
---------------- end 25jul06 conversion to shared attributes ----------
#endif
	
	m_pValidityDepends = new hCdependency(pSrc->devHndl());
	m_pValidityDepends->setOwner(this);
	m_pConditionalDepends = new hCdependency(pSrc->devHndl());
	m_pConditionalDepends->setOwner(this);
	m_pCommonDepends      = new hCdependency(pSrc->devHndl());// stevev 14sep06 
	m_pCommonDepends->setOwner(this);// stevev 14sep06
	m_pContainer          = new hCdependency(pSrc->devHndl());// stevev 27sep06 - parents 
	m_pContainer->setOwner(this); // stevev 27sep06
}


hCitemBase::~hCitemBase() 
{
	// moved below
	if (pPsuedoLabel != NULL)
	{
		devPtr()->destroyPsuedoItem(pPsuedoLabel);
		delete pPsuedoLabel;
		pPsuedoLabel = NULL;
	}
	if (pPsuedoHelp != NULL)
	{
		devPtr()->destroyPsuedoItem(pPsuedoHelp);
		delete pPsuedoHelp;
		pPsuedoHelp = NULL;
	}


	if ( attrLst.size() > 0)
	{
		for ( hAttrPtrList_t::iterator iT = attrLst.begin();
		      iT < attrLst.end();   /* done internally 10apr07 sjv  iT++*/)
		{// iT isa ptr 2a ptr 2a hCattrBase
			// stevev 09apr07 - some pointers appear in both places
			if ( ((hCattrValid*)(*iT)) == pValid ) pValid = NULL;
			if ( ((hCattrLabel*)(*iT)) == pLabel ) pLabel = NULL;
			if ( ((hCattrHelp* )(*iT)) == pHelp  ) pHelp  = NULL;
			if ( ((hCattrDebug*)(*iT)) == pDebug ) pDebug = NULL;
			if ( (*iT) != NULL )
			{
				//delete (*iT);
				if ( !  sharedAttr )
				{

#if 0 // turn this off // debugging code
if( (*iT)->getGenericType() == 0x010001 )// var handling
{
hCattrHndlg *pHndl = (hCattrHndlg*)(*iT);
	LOGIT(CLOG_LOG,"HNDLG: 0x%04x itmBase deleting %p\n",getID(),pHndl);
}
// end debugging code
#endif // 0
					(*iT)->destroy();// attrBase requires this function
					delete (*iT);
				}
				(*iT) = NULL;
				iT = attrLst.erase(iT);// returns the next iterator pos
			}
			else
			{
				++iT;
			}
		}
	}
	// stevev 09apr07 - moved from above
	// stevev 20jul06 - share attributes //
	if ( !  sharedAttr )
	{
	RAZE(pValid); 
	RAZE(pLabel); 
	RAZE(pHelp);
	RAZE(pDebug);
	}
	RAZE(m_pValidityDepends);
	RAZE(m_pConditionalDepends);
	RAZE(m_pCommonDepends);
	RAZE(m_pContainer); 
}

RETURNCODE hCitemBase::destroy(void)
{
	hCattrBase* psb;
	RETURNCODE rc = SUCCESS;
	if ( attrLst.size() > 0)
	{
		for ( hAttrPtrList_t::iterator iT = attrLst.begin();
		      iT < attrLst.end();  /* done internally 10apr07 sjv  iT++*/ )
		{// iT isa ptr 2a ptr 2a hCattrBase
			// stevev 09apr07 - some pointers appear in both places
			if ( ((hCattrValid*)(*iT)) == pValid ) pValid = NULL;
			if ( ((hCattrLabel*)(*iT)) == pLabel ) pLabel = NULL;
			if ( ((hCattrHelp* )(*iT)) == pHelp  ) pHelp  = NULL;
			if ( ((hCattrDebug*)(*iT)) == pDebug ) pDebug = NULL;
			if ( (*iT) != NULL )
			{
				psb = (hCattrBase*) *iT;
				rc |= psb->destroy();// will need to decode the type
				if ( !  sharedAttr )
						delete psb;
				(*iT) = NULL;
				//27aug07 - do it the more conventional way iT = attrLst.erase(iT);// returns the next iterator pos
			}
			//27aug07 - do it the more conventional way was::>  else
				++iT;
		}
		attrLst.clear();// 27aug07 - do it the more conventional way
	}

	if ( !  sharedAttr )
	{
	DESTROY(pValid); 
	DESTROY(pLabel); 
	DESTROY(pHelp);
	DESTROY(pDebug);
	}
	if (pPsuedoLabel != NULL)
	{
		devPtr()->destroyPsuedoItem(pPsuedoLabel);
		delete pPsuedoLabel;
		pPsuedoLabel = NULL;
	}
	if (pPsuedoHelp != NULL)
	{
		devPtr()->destroyPsuedoItem(pPsuedoHelp);
		delete pPsuedoHelp;
		pPsuedoHelp = NULL;
	}

	RAZE(m_pValidityDepends);
	RAZE(m_pConditionalDepends);
	RAZE(m_pCommonDepends);
	RAZE(m_pContainer); 
	return rc;
}
 

hCitemBase& hCitemBase::operator=(const hCitemBase& ib) {
		if ( &ib == NULL ) return *this;
	 DDkey =       ib.DDkey;
//	 pDevice =	   ib.pDevice;
	 itemId =      ib.itemId;
	 itemType =    ib.itemType;
	 itemSize =    ib.itemSize;
	 itemSubType = ib.itemSubType;	
	 itemName =    ib.itemName;	
/* VMKP added on 291203 */
 	 isConditional = ib.isConditional;
/* VMKP added on 291203 */

	pValid  =     ib.pValid;// new?
	pLabel  =     ib.pLabel;// new?
	pHelp   =     ib.pHelp;   // new?
	pDebug  =     ib.pDebug;

	m_pValidityDepends = ib.m_pValidityDepends;
	m_pValidityDepends-> setOwner(this);
	m_pConditionalDepends = ib.m_pConditionalDepends;
	m_pConditionalDepends-> setOwner(this);
	m_pCommonDepends = ib.m_pCommonDepends;
	m_pCommonDepends-> setOwner(this);
	m_pContainer = ib.m_pCommonDepends;
	m_pContainer-> setOwner(this);
	isReallyCritical = ib.isReallyCritical; // stevev 23feb07 - stevev fixed typo

	return *this;
	};



RETURNCODE hCitemBase::baseLabel(int key, wstring &k)
{
	RETURNCODE rc = FAILURE;

	if (pLabel!=NULL)
	{
		rc = pLabel->getString(k,key);
	}
	else
	{
		k.erase();
	}
	return rc;
}

// stevev - WIDE2NARROW char interface
RETURNCODE hCitemBase::baseLabel(int key, string &l)
{
	wstring locStr; 
	RETURNCODE rc = baseLabel(key,locStr);
	l = TStr2AStr(locStr);
	return rc;
}


RETURNCODE hCitemBase::dumpSelf(int indent, char* typeName)  // to cout
{

	LOGIT(COUT_LOG,"Item ID: 0x%x,\t",itemId );
	//COUTSPACE << hex << "    Item ID: 0x" << itemId << dec ;
	// type is variable,command...
	if (! itemName.empty() )
	{
		LOGIT(COUT_LOG,"Name:  %s,\t",itemName.c_str());
		//	COUTSPACE << "  Name:  " << itemName << "   " ;
	}
	else
	{
//		COUTSPACE << "  Name:  No Item Name! " ;
		LOGIT(COUT_LOG,"No Item Name,\t");
	}
//	COUTSPACE << endl;
//	COUTSPACE <<        "  Item Type: " << itemType.getTypeStr() << endl;
	LOGIT(COUT_LOG,"Item Type: %s\t",itemType.getTypeStr());
//	COUTSPACE <<        "  Item Size: " << itemSize << endl;
//	LOGIT(LOG_DEBUG,"varType: %d,\t",varType);
	DEBUGLOG(COUT_LOG,"\n");

	if (pValid) pValid->dumpSelf(indent + 4);
//	else COUTSPACE <<   "   Validity:  No attribute!" << endl;
	if (pLabel) pLabel->dumpSelf(indent + 4);
//	else COUTSPACE <<   "     Label:   No attribute!" << endl;
	if (pHelp)  pHelp->dumpSelf (indent + 4);
//	else COUTSPACE <<   "      Help:   No attribute!" << endl;

//	unsigned long   DDkey;      // active DDkey value <0xffffffff-none>
//	CitemType		itemSubType;

	return SUCCESS;
};

/*virtual */
bool hCitemBase::IsValidTest()  /*returns true if valid and CACHED (does not read to reolve */
{/* * * see notes in IsValid() below * * */
	unsigned long answer;
	if (pValid != NULL)
	{
		hCddlLong* pLng = (hCddlLong*)pValid->procure(true);
		if (pLng != NULL)
		{
			answer = (UINT32)pLng->getDdlLong();// very limited value set..zero & one
			if(answer == 0)
			{
				return false; 
			}
			else
			{
				return true; // donno why ????
			}
		}
		else
		{// this is a test - remember...this is only a test
			return false; //Vibhor: As before
		}
	}
	else
	{
#ifdef LOG_MISSING_VALIDITY
		clog << "VALIDITY DOESN'T EXIST - ASSUME VALID."<<itemName<<endl;
#endif
		return true; // default is valid
	}
}

/* virtual */
bool hCitemBase::is_UNKNOWN(void)
{
	if (pValid != NULL)
	{
		return pValid->is_UNKNOWN();
	}
	else
	{
#ifdef LOG_MISSING_VALIDITY
		clog << "VALIDITY DOESN'T EXIST - ASSUME VALID."<<endl;
#endif
		return false; // default is valid
	}
}

/* virtual */
bool hCitemBase::IsValid(void) /*returns true if the item is currently valid*///return validity	
{
	unsigned long answer;
	if (pValid != NULL)
	{
		hCddlLong* pLng = (hCddlLong*)pValid->procure();
		if (pLng != NULL)
		{
			answer = (UINT32)(pLng->getDdlLong());// very limited value set
// TEMPORARY SUCCESS
/*Vibhor 220104: Start of Code*/
			if(answer == 0)
			{
				return false; 
			}
			else
			{
				return true; // donno why ????
			}

/*Vibhor 220104: End of Code*/
		}
		else
		{
			cerr<<"ERROR: (0x"<<hex<<itemId<<dec<<") validity did not return a payload."<<endl;
			/*Vibhor 220104: Start of Code*/
			LOGIT(UI_LOG,"%u : Validity did not return a payload",itemId);//Added By Vibhor 220104

// TEMPORARY SUCCESS



/*As per discussion with Steve, returning true is a better solution here.
Ideally this shld never be reached. and if it does we need to verify that 
scenario  
We may need to revisit this  again  */

// return true; //Vibhor : As before <START>VMKP 300104 Commented to remove variables coming under menu not available.<END>

		return false; //Vibhor: As before

/*Vibhor 220104: End of Code*/
		}
	}
	else
	{
#ifdef LOG_MISSING_VALIDITY
		clog << "VALIDITY DOESN'T EXIST - ASSUME VALID."<<endl;
#endif
		return true; // default is valid
	}
};


bool hCitemBase::isValidityConditional(void)
{ 
	if (pValid && pValid->isConditional() ) 
		return true; 
	else 
		return false;
}

void hCitemBase::setDependentsUNK(void)
{
	ddbItemLst_it i;
//  currently the only one
	if ( m_pValidityDepends == NULL )
	{	return; /* nothing we can do */  }

	for ( i  = m_pValidityDepends->dependsOnMe.begin(); 
		  i != m_pValidityDepends->dependsOnMe.end(); ++i)
	{//ptr2a ptr2a hCitemBase
		if ( (*i)->pValid != NULL )
		{
		  (*i)->pValid->setUNKNOWN();// will force a re calculation the next time its accessed
		}
		(*i)->setDependentsUNK();// goes for dependents of dependents too
	}		
}


//VMKP Modified for Help conditional handling
RETURNCODE hCitemBase::Help(wstring &h)
{ 
	RETURNCODE rc = FAILURE;
	wstring g;
	hCddlString tmpStr(devHndl()); // used to getStandardStr ONLY
	if (pHelp!=NULL)
	{
//		hCddlString* pDDLstr = (hCddlString*)(pHelp->procure());
//		h = pDDLstr->procureVal();
		rc = pHelp->getString(g,0);
	}
	else
	{
		//g = tmpStr.getStandardStr("default_dict_entries",2);
		g = L"";// prevent 'No Help Avaiable s00_E7-Help' scenario
	}
	h = g;
	return rc;
};	
// stevev - WIDE2NARROW char interface
RETURNCODE hCitemBase::Help( string &h)
{
	wstring locStr;
	RETURNCODE rc = Help(locStr);
	h = TStr2AStr(locStr);
	return rc;
}


// attr_Type in a hCattrBase is an enum value! (in a aCattrBase it is a mask)
// this is for variables ONLY - over ride the virtual for other types
hCattrBase*  hCitemBase::getaAttr(varAttrType_t attrType)
{
	ulong gt= hCattrBase::makeGenericType(getIType(),attrType);
	FOR_iT(hAttrPtrList_t, attrLst)
	{// iT isa ptr2 hCattrBase
		//if ((*iT)->getGenericType() == gt) 
		if ((*iT)->getGenericType(getIType()) == gt)
			return ((hCattrBase*)(*iT));
		// else keep scanning 
	}
	return NULL;// not found
};

RETURNCODE   hCitemBase::delistAttr( hCattrBase* pAttr ) // stevev 13jul07 - remove from list -
{
	if (pAttr == NULL) return FAILURE;
	ulong gt = pAttr->getGenericType();
	FOR_iT(hAttrPtrList_t, attrLst)
	{// iT isa ptr2aptr2a hCattrBase
		hCattrBase* pAttrLst = (hCattrBase*) *iT;
		if (pAttrLst->getGenericType() == gt)
		{
			attrLst.erase(iT);
			return SUCCESS;// only one attribute per type is allowed
		}
#ifdef _DEBUG
		else if ( (pAttrLst->getGenericType() & 0xffff0000) == 0)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"ItemBase: Attribute in list has no Item pointer set.-Programmer error-\n");
		}
#endif
		// else - keep looking
	}// next attr
	return FAILURE;
}

RETURNCODE hCitemBase::fileWrt(hCformatData*  pData)
{// errors are always in plain text
	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s item tried to be written to a file.\n",itemStrings[getID()]);
	pData->pFormat->hCformatBase::insertObject(pData, this); 
	return SUCCESS;
}

// ANY of these could return NULL ! ! ! ! !
hCdependency*  hCitemBase::getDepPtr(ibWhichDependency_t depType)
{ 
	switch(depType)
	{
	case ib_ValidityDep:
		return m_pValidityDepends;
		break;
	case ib_CondDep:
		return m_pConditionalDepends;
		break;
	case ib_Common:
		return m_pCommonDepends;
		break;
	case ib_Contain:
		return m_pContainer;
		break;
	case ib_ErrorDep:
	default:
		return NULL;
		break;
	}
}

// determine if this item depends on another item
bool hCitemBase::Idepend(void)
{
	if (m_pValidityDepends && m_pValidityDepends->iDependOn.size())
		return true;
	if (m_pConditionalDepends && m_pConditionalDepends->iDependOn.size())
		return true;
	if (m_pCommonDepends && m_pCommonDepends->iDependOn.size())
		return true;
	return false;
}
	
// must delete from inside the class !
void  hCitemBase::deleteDependency(ibWhichDependency_t depType)
{ 
	switch(depType)
	{
	case ib_ValidityDep:
		delete m_pValidityDepends;
		m_pValidityDepends = NULL;
		break;
	case ib_CondDep:
		delete m_pConditionalDepends;
		m_pConditionalDepends = NULL;
		break;
	case ib_Common:
		delete m_pCommonDepends;
		m_pCommonDepends = NULL;
		break;
	case ib_Contain:
		delete m_pContainer;
		m_pContainer = NULL;
		break;
	case ib_ErrorDep:
	default:
		return;
		break;
	}
}

wstring& hCitemBase::getWName(void)
{
	string l;
	l = getName();

	witemName = AStr2TStr(l);// to unicode

	return (witemName);
}

string& hCitemBase::getName(void)
{ 
	if ( pDebug != NULL )
	{
#ifdef _DEBUG
		// stevev 02nov09 - put the shared first, the shared attributes may have been deleted
		//        and the pointer is always left in the elements...
		if (!sharedAttr && pDebug->getSymbolName() != itemName)// stevev 26jul06 filter dup'd typedef
		{
			cerr<<"Debug Info's symbol Name ("<<pDebug->getSymbolName()
				<< ") does NOT match the symbol file name("<<itemName<<")."<<endl;
		}
#endif
		if (itemName.empty() )
		{
			return pDebug->getSymbolName();
		}
	}
	return itemName; 
};
void hCitemBase::setName(string& s) 
{ 
	if(itemId & 0x80000000)// only valid on psuedo variables
	{
		itemName = s;
	} 
}

/*  removed commented out section 20jan05 (Read()) */


CValueVarient hCitemBase::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	int target = 0;// 1=l; 2=h; 4=v

	itemType_t  myIT =  getIType();
	switch (myIT)
	{	
	case iT_Variable:			// 3,4,7
		if (attrType == 3)target=1;else if (attrType == 4)target=2;else if (attrType == 7)target=4;
		break;
	case iT_Menu:				// 0,2,3
		if (attrType == 0)target=1;else if (attrType == 2)target=2;else if (attrType == 3)target=4;
		break;
	case iT_EditDisplay:		// 0,5,6
		if (attrType == 0)target=1;else if (attrType == 5)target=2;else if (attrType == 6)target=4;
		break;
	case iT_Method:				// 1,2,4
		target=attrType;if (target != 1 && target !=2 && target != 4) target = 0;
		break;
	case iT_Collection:			// 1,2, -
	case iT_File:				// 1,2, -
		if (attrType == 1)target=1;else if (attrType == 2)target=2;
		break;
	case iT_Waveform:			// 0,1, -
		if (attrType == 0)target=1;else if (attrType == 1)target=2;
		break;

	case iT_ItemArray:			// 1,2,3
	case iT_Array:				// 1,2,3
		if (attrType == 1)target=1;else if (attrType == 2)target=2;else if (attrType == 3)target=4;
		break;

	case iT_Chart:				// 0,1,2
	case iT_Graph:				// 0,1,2
	case iT_Axis:				// 0,1,2
	case iT_Source:				// 0,1,2
	case iT_List:				// 0,1,2
	case iT_Grid:				// 0,1,2
	case iT_Image:				// 0,1,2
		if (attrType == 0)target=1;else if (attrType == 1)target=2;else if (attrType == 2)target=4;
		break;

	case iT_Command:			// none
	case iT_Refresh:/*relation*/// none
	case iT_Unit:/*relation*/	// none
	case iT_WaO:				// none
	case iT_ReservedZeta:		//00
	case iT_ReservedOne:		// specially used to get the image list
	case iT_Block:				//12
	case iT_Program:			//13
	case iT_Record:				//14
	case iT_VariableList:		//16
	case iT_ResponseCodes:		//17
	case iT_Domain:				//18
	case iT_Member:				//19
	default:
		return rV;
	}

	if ( target == 0 )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Item Base's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	if (target == 1)
	{// Label
		if ( Label(rV.sWideStringVal) == SUCCESS )
		{
			rV.vType = CValueVarient::isString;
			rV.vSize = rV.sStringVal.size();
			rV.vIsValid = true;
		}
		else
		{
			rV.vType = CValueVarient::isString;
			rV.vSize = rV.sStringVal.size();
			rV.vIsValid = false;
		}
	}
	else
	if (target == 2 )
	{// help 
		if ( Help(rV.sWideStringVal) == SUCCESS )
		{
			rV.vType = CValueVarient::isString;
			rV.vSize = rV.sStringVal.size();
			rV.vIsValid = true;
		}
		else
		{
			rV.vType = CValueVarient::isString;
			rV.vSize = rV.sStringVal.size();
			rV.vIsValid = false;
		}
	}
	else
	if (target == 4 )
	{//validity
			rV.vType = CValueVarient::isBool;
			rV.vSize = 1;
			rV.vIsValid = true;
			rV.vValue.bIsTrue = false;
			if (IsValid()) rV.vValue.bIsTrue = true;
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
	}
	return rV;
}

// overide this virtual if your item is a container class
// eg menu, collection-as-menu,grid,graph (waveform), chart (source)
void  hCitemBase::fillContainList(ddbItemList_t&  iDependOn) 
{// this is NOT common to all classes.  If not over-ridden, doesn't contain
	return;
}

// no inputs or outputs...it will do the best it can
// overide this virtual if your item has more than label (call this to get label depends)
void  hCitemBase::fillCondDepends(ddbItemList_t&  iDependOn) 
{//hAttrPtrList_t  attrLst;
	//hAttrPtrList_t::iterator aiT;
	//ulong genericTyp = 0;

	if (pLabel != NULL )
	{
		pLabel->getDepends(iDependOn);
	}
	// pHelp never appears on a menu so no dependency is required
	//for ( aiT = attrLst.begin(); aiT != attrLst.end(); ++aiT)//ptr2 ptr2 hCattrBase
	//{// assumes that pointers to ALL attributes are in this central list
	//	//aiT isa ptr2ptr2 hCattrBase
	//	genericTyp = (*aiT)->getGenericType();
	//}
}

// stevev 17nov05 
// called when this item has changed ( ie value or validity changed )
RETURNCODE hCitemBase::notify(hCmsgList& msgList) // protected
{// call notification for each dependent
	// assume we have already been put into the msgList
	ddbItemList_t::iterator it;
	hCdependency* pDep;

	/* stevev 20feb07 added notify lockout while NOT ONLINE - due to hang-up with 3144 */
	if ( devPtr()->getDeviceState() != ds_OnLine )
		return SUCCESS;// like we knew what we were doing

	pDep = getDepPtr(ib_ValidityDep);
	for ( it = pDep->dependsOnMe.begin(); it != pDep->dependsOnMe.end();++it)
	{// isa ptr2ptr2 hCitemBase
		(*it)->notification(msgList, true);// tell 'em they may have changed a validity
		// stevev 7jul06 - we went down the tree, now put this one in
		// TEST TEST
		// hold test    msgList.insertUnique((*it)->getID(), mt_Str);
	}
	pDep = getDepPtr(ib_CondDep);
	hCitemBase *pItem;
	for ( it = pDep->dependsOnMe.begin(); it != pDep->dependsOnMe.end();++it)
	{
		pItem = (*it);// stop using iterator as a pointer
		pItem->notification(msgList, false);// tell 'em they may have changed a cond
		// stevev 7jul06 - we went down the tree, now put this one in
		// TEST TEST
		// hold test    msgList.insertUnique((*it)->getID(), mt_Str);
	}
	return FAILURE;
}

// called when an item in iDependOn has changed
int hCitemBase::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
	int r = 0;

	if (isValidity )
	{
		if ( pValid != NULL && pValid->reCalc() )
		{// my validity changed
			/* stevev 28sep06 - add contain to handle parent notification */
			ddbItemList_t::iterator it;
			hCdependency* pDep;

			pDep = getDepPtr(ib_Contain);
			// this is really 'containsMe' list
			for ( it = pDep->dependsOnMe.begin(); it != pDep->dependsOnMe.end();++it)
			{// isa ptr2ptr2 hCitemBase 
			 //              of the item that contains me and needs to know my validity changed*/
				msgList.insertUnique((*it)->getID(), mt_Str);// tell container ('parent') 
			}
			/* stevev 28sep06 - end */

			msgList.insertUnique(getID(), mt_Val, pValid->getGenericType());// tell the world
			notify(msgList); // tell my dependents
			r = 1;
		}// else - no change - return 0;
	}
	else
	{// re-evaluate all the others ( label is only one in the base class )
		if ( pLabel != NULL && pLabel->reCalc() )
		{// my validity changed
			msgList.insertUnique(getID(), mt_Str, pLabel->getGenericType());// tell the world
			r = 1;
		}// else - no change - return 0;
		// no value change here - no need to call notify
	}
	return r;// number added
}

// sends the messages in the proper order
RETURNCODE hCitemBase::notifyUpdate(hCmsgList& msgList)
{
	msgLstItr_t iT;
	if (msgList.size() > 0 )
	{	// nuclear first
		for (  iT = msgList.begin(); iT != msgList.end(); ++iT)
		{// ptr2a hCmessage
			if ( iT->msgType == mt_All )
			{
				devPtr()->notifyVarUpdate(ROOT_MENU,STR_changed,0L);
				devPtr()->notifyVarUpdate(VIEW_MENU,STR_changed,0L);
			//	we must continue to get all the cancels done
			}
		}	
		// then structure 
		for (  iT = msgList.begin(); iT != msgList.end(); ++iT)
		{// ptr2a hCmessage
			if ( iT->msgType == mt_Str )
			{
				devPtr()->notifyVarUpdate(iT->msgItem,STR_changed,iT->msgAttr);
			}
		}
		// then values
		for (  iT = msgList.begin(); iT != msgList.end(); ++iT)
		{// ptr2a hCmessage
			if ( iT->msgType == mt_Val )
			{
				devPtr()->notifyVarUpdate(iT->msgItem,IS_changed,iT->msgAttr);
			}
			/* 6jun07 - stevev had to preclude copying device2Display further down */
			else
			if( iT->msgType == mt_Mth )
			{
				devPtr()->notifyVarUpdate(iT->msgItem,METH_changed,iT->msgAttr);
			}
		}
	}// else leave well
	return SUCCESS;
}


// 03aug06 - support for attribute references for label & help	
//			returns NULL on error (ie not an accesible attribute number)
itemID_t hCitemBase::getAttrID (unsigned  attr, unsigned which)
{
LOGIT(CLOG_LOG,"++++ Itembases's getAttrID needs to be implemented.\n");
	itemID_t ID = 0;
	return ID;
}

hCVar* hCitemBase::getAttrPtr(unsigned  attr, unsigned which)
{
LOGIT(CLOG_LOG,"++++ Itembases's getAttrPtr needs to be implemented.\n");
	hCVar* pV = NULL;
	return pV;
}


/* virtual  */
void hCitemBase::setValidity(bool newV) // sets the external validity (used by Lists ONLY!)
{ 
	if (pValid == NULL) 
		return; 

	if (newV) 
		pValid->setValue( hCattrValid::v_VALID ); 
	else 
		pValid->setValue( hCattrValid::v_INVALID ); 
}

void  hCitemBase::setPsuedo(elementID_t elem)
{
	ownerID     = elem.contID;
	ownerIndex  = elem.which;
}
/*************************************************************************************************
 *
 *   $History: ddbItemBase.cpp $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
