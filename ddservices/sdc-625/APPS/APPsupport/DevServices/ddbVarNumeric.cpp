/**********************************************************************************************
 *
 * $Workfile: ddbVarNumeric.cpp $
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *      definition of the classes derived from hCNumeric in ddbVar
 * 16jul08  sjv  creation from ddbVar - ddbVar got too big for visual studio to handle
 *
 */


#if defined(__GNUC__)
#include "ddbGeneral.h"
#endif // __GNUC__

#include "ddbVar.h"

#include <limits>
#include <algorithm>		/* see 'transform' */
#include "ddbCommand.h"		/* for hCmsgList */
#include "ddbList.h"			/* for count support */
#include "Endian.h"

#include "softfloat.h"
#include <time.h>			/* for wcsftime */

void addPercent(wstring& fmtStr);

// compiled in the UI in order to get locale information
extern wchar_t *strptime(const wchar_t *buf, const wchar_t *fmt, struct tm *tm);

void fillStruct(struct tm *pTime, unsigned bgtm);// forward declaration

extern bool isEqual(float A, float B);  //AlmostEqual2sComplement with NaN == NaN
extern bool isEqual(double A, double B);

/**********************************************************************************************
 *  the hCinteger Base Class
 *                                  ie hCunsigned:hCinteger:hCnumeric:hCvar:hCitembase:hCobject
 **********************************************************************************************
 */

hCinteger::hCinteger(DevInfcHandle_t h, aCitemBase* paItemBase, bool isS)
         :hCNumeric(h,paItemBase), isSigned(isS)
{// done at instantiation, setDefaultValue() will be called when conditionals may be resolved
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
#ifdef XX_DEBUG
    hCattrVarMinVal* pT = (hCattrVarMinVal*)getaAttr(varAttrMinValue);  
#endif
    if ( policy.isReplyOnly )
    {// we are a device
        Value = Wrt_Val = cacheVal =0L;
        markItemState(IDS_CACHED);

        // **** special response code in device simulation ****
        itemID_t symID = getID();

        if ( symID == RESPONSECODE_SYMID )
        {
            VariableType(vT_Unsigned); // in device simulation, force to an int
        }
        // else leave it be
    }
    else
        Value = Wrt_Val = cacheVal = -1L;// default for undefined
}


hCinteger::hCinteger(hCinteger*  pSrc,  hCinteger*  pVal, itemIdentity_t newID):
           hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID),
                      cacheVal(0),isSigned(pSrc->isSigned)
{
    /* stevev 13oct08 - we have to initialize the pIndexed pointer in 'this'
       before we try to setDefaultValue() of an index variable. I have not
       found a way to force that to be initialized before this constructor 
       is called.
    */
    Value = Wrt_Val = cacheVal = -1L;// default for undefined
    if ( pSrc->VariableType() == vT_Index )
    {
        hCindex* pIdx = (hCindex*)pSrc;
        hCindex* pIme = (hCindex*)this;
        pIme->pIndexed= pIdx->pIndexed;
    }

    if ( pVal == NULL )
    {
        setDefaultValue();
    }
    else
    {
        Value = (UINT64) pVal->getDispValue();
        Wrt_Val = Value;
    }
    cacheVal = Wrt_Val;
}

// limit size and Sign extend signed negative values
UINT64 hCinteger::limitAndExtend(UINT64 orig)
{
    UIntList_t   vvList;
    UIntList_t ::iterator vvIT;

	int s = VariableSize();// assume 1 - 8 for integers
	// limit size
	UINT64 m = 0xffffffffffffffffULL >> ((8-s)*8); // L&T Modifications : PortToAM335
	orig &= m;	// size limit
	UINT64 sn= 0x8000000000000000ULL >> ((8-s)*8); // L&T Modifications : PortToAM335
	// sign extend
	if ( ((orig & sn) != 0) && isSigned )  // extend
	{
		/* NOTE: Mr gate's math is different from mine!
		 he believes that 0xffffffff << 32 is equal to 0xffffffff
		 oh *&^%$#@  */
		UINT64 yy = 0xffffffffffffffffULL; // L&T Modifications : PortToAM335
		if ( s == 8 )
		{
			yy = 0;
		}
		else
		{
			yy = (0xffffffffffffffffULL << (s*8)); // L&T Modifications : PortToAM335
		}
		orig |= yy; // used to be::> (0xffffffff << (s*8));     // sign extend it
	}

    // Only Index and Date have hard-spec'd valid values
    // Other types must be dealt with elsewhere
    
    if (VariableType() == vT_Index )
    {// closest value
        /* NOTE stevev 03jul08 - indexes are arbitrarily restricted to positive 
         *      values of 32 bit integers.  (concurred by wally yesterday)
         */
        INT32 target = 0;// convert long long to long
        if ( orig > (UINT64)_I32_MAX ) 
            target = _I32_MAX;
        else                           
            target = (INT32)orig;

        INT32 al = 0, ah = _I32_MAX, aA = 0; // limits of valid index values

		if ( ((hCindex*)this)->pIndexed == NULL)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
			  "ERROR: index 0x%04x (%s) has no array.\n",getID(),getName().c_str());
			orig = 0;
		}
		else
        if ( ((hCindex*)this)->getAllValidValues(vvList) <= 0 )// error | no items
        {// we are allowed to set it to -1 if its a list....
            hCitemBase*  pIndexedItm = NULL;
            ((hCindex*)this)->pIndexed->getArrayPointer(pIndexedItm);
            if ( pIndexedItm  && pIndexedItm->getIType() == iT_List )  // is a list     
            {// lists are usually empty....
                orig = 0;// we'll return zero
                setValidity(false);//and set to invalid as a list index
            }
            else
            {// not a list, this is an anomoly
                LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
                  "ERROR: index 0x%04x (%s) has no valid values.\n",getID(),getName().c_str());
                orig = 0;
            }
        }
        else
        {           
            for (vvIT = vvList.begin(); vvIT<vvList.end();++vvIT)
            {//vvIT is a pointer to a UINT32
                if ((UINT32)(*vvIT) > (UINT32)_I32_MAX)
                    aA = _I32_MAX; // should never have got thru the tokenizer
                else
                    aA = (INT32)(*vvIT);

                if ( aA == target )
                { 
                    al = ah = target;
                    break; 
                } 
                else
                if ( aA < target )
                {
                    if (aA > al) 
                    {
                        al = aA;
                    }
                }
                else// ( (*vvIT) > orig aka aA > target)
                {
                    if (aA < ah)
                    {
                        ah = aA;
                    }
                }
            }// next
            if ( (al < target) && (ah > target) )//if we didn't find an exact match
            {// set to nearest valid value
                if ( (target-al) < (ah - target) )
                {
                    orig = (UINT64)al;
                }
                else
                {
                    orig = (UINT64)ah;
                }
            }
            else
            {
                orig = target;
            }
        }//endelse
    }
    else
    if ( VariableType() == vT_HartDate )
    {// set to a valid value - dmy
        orig &= 0x1f0fff;
        if (! (orig & 0x1f0000 ) )// zero day
        {
            orig =  orig  | 0x010000;
        }
        if (! (orig & 0x000f00 ) )// zero month
        {
            orig =  (orig & 0xff00ff ) | 0x000100 ;
        }
        if ( (orig & 0x000f00 ) > 0x000c00 )// post December
        {
            orig =  (orig & 0xff00ff ) | 0x000c00;
        }
    }
    // else - how other out-of-range variables are handled is location/source dependent

    return orig;
}


void hCinteger::highest(CValueVarient& retVal)
{
	int s = VariableSize();// assume 1 - 8 for integers
	if (isSigned)
	{
		retVal =  (INT64) (0xffffffffffffffffULL>>(((8-s)*8)+1)); // L&T Modifications : PortToAM335
	}
	else
	{
		retVal = (UINT64) (0xffffffffffffffffULL>>(((8-s)*8))  ); // L&T Modifications : PortToAM335
	}
	return;
}

void hCinteger::lowest (CValueVarient& retVal)
{
	int s = VariableSize();// assume 1 - 8 for integers
	if (isSigned)
	{
		retVal = (INT64) (0xffffffffffffffffULL<<((s*8)-1));//  most negative number available // L&T Modifications : PortToAM335
	}
	else
	{
		retVal = (UINT64) (0);// smallest possible number
	}
	return;
}

RETURNCODE  hCinteger::setDefaultValue(void)
{
	CValueVarient  vv;
	/* 
		LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix ambiguous assignment 
        	compiler error (no long assignment override available). Check if that 
        	is what was intended.
	*/
	vv = (int) 1L; // new HCF spec'd default value
	hCVar::setDefaultValue(vv);
	/* Special Device modification!!! */
	if ( devPtr()->getPolicy().isReplyOnly )
	{// we are a device
		itemID_t symID = getID();

		if ( symID == RESPONSECODE_SYMID )
		{
			VariableType(vT_Unsigned); // in device simulation, force to an int
		}
		// else leave it be
	}
	return SUCCESS;
}

// returns number of zero bits to the right of a one bit (first bit set from right)
UINT32 hCinteger::right0bits(UINT64 value)
{
    UINT32 cnt = 0;
    if ( value != 0 ) // we have a bit set
    {
        for (; ((value & 0x0001) != 0x0001) && (cnt < (sizeof(UINT64)*8)); cnt++,value>>=1) ;
    }// else return zero
#ifdef _DEBUG
    else 
    {
        LOGIT(CERR_LOG,L"ERROR: right0bits passed a zero value (no bits set).\n");
    }
#endif
    
    return cnt;
}

/* VMKP changed on 300104 from unsigned long to unsigned int 
   (for some reason retVal.vType is returning isSymID, but should return isIntConst), 
   changed similar to the lowest */
//void hCinteger :: highest(CValueVarient& retVal)  // largest default
//{ if (! isSigned) { retVal = (unsigned int) (0xffffffff >> ( 32 - ((getSize()*8))));  } 
//else              { retVal = (long)          (0xffffffff >> ((32 -  (getSize()*8))+1));}
//};

// 10feb06 stevev - moved common code
bool hCinteger::isInRange(CValueVarient val2Check)// true @ val2Check inside min-max set
{
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    return ( sizeAndScale(val2Check)) ; // does it all
}

//  hCRangeList localList;
//  resolveRangeList(localList);  // ...get min max, resolve to list, populate
//  if ( localList.size() > 0 )   // should always be..it is populated with default values
//  {
//      double R = (((double)val2Check) / (pScale->getScaleFactor()));
//      if ( isSigned )
//      {           
//          if ( R > numeric_limits<int>::max() || R < numeric_limits<int>::min() )
//          {
//              /* we cannot represent this number as an int - must be out of range */
//              return false;
//          }
//          val2Check = (INT32) R;//(((double)val2Check) / (pScale->getScaleFactor()));
//      }
//      else
//      {
//          if ( R > numeric_limits<unsigned int>::max() || 
//               R < numeric_limits<unsigned int>::min() )
//          {
//              /* we cannot represent this number as an int - must be out of range */
//              return false;
//          }
//          val2Check = (UINT32) R;//(((double)val2Check) / (pScale->getScaleFactor()));
//      }
//      return (localList.isInRange(val2Check));
//  }
//  else
//  {
//      return true;// in lieu of a min max value, it must be in range
//  }
//}
// end 10feb06 stevev
/* VMKP added on 020104 */

RETURNCODE hCinteger::getMinMaxList(hCRangeList& retList)
{
    RETURNCODE rc = SUCCESS;
    IntType_t thisType = No_Type;
    if (IsSigned() ) thisType  = Is_Signed;
    if (IsUnsigned()) thisType = Un_Signed;
    resolveRangeList(retList,thisType);  // ...get min max, resolve to list, populate
    return rc;
}
/* VMKP added on 020104 */



RETURNCODE hCinteger::Write(const UINT64 val) 
{
    RETURNCODE rc = FAILURE;
    CValueVarient localVar;
    if (! IsReadOnly() )  
    {
        Wrt_Val = (UINT64)( ((INT64)val) / GetScalingFactor());
        //was::>(pScale->getScaleFactor()));
        // stevev 19feb07 - merge - casting sequence to aviod roundoff error
        //localVar = val;
        //setDispValue(localVar);   // writes only deal with display values 
                                // (read back fills real value)
        rc = hCVar::Write();    // base class 'Write fastest way'
    }
    else 
    {
        rc = DATA_QUALITY_NOT_WRITABLE; 
    }

    return rc;
}


DATA_QUALITY_T hCinteger::Read(UINT64 &val) 
{       //returns the current device (real) value
     
    CValueVarient myVar;
    DATA_QUALITY_T   rc = hCVar::Read(myVar);// will get from device or cache(this class)

//  val = (UINT32)  (((int)myVar) * (pScale->getScaleFactor())); //VMKP
    val = (UINT64)  ((INT64)myVar); //VMKP

    return rc;
}

wstring& hCinteger::getValueFormatedString(bool bIsDispValue) //CAUTION
                                                    //-->>>>see 'Invensys checkin' in ddbVar.h
{
    CValueVarient var; 

    updateCount();
    
    if (bIsDispValue)
    {
        var = (INT64)Wrt_Val;
    }
    else
    {
        var = (INT64)Value;
    }   

    returnString = scaleNformatValue(var);
    return  returnString; 
}
wstring& hCinteger::getDispValueString(void) 
{
    return getValueFormatedString(true);
    
    
    //CValueVarient var; var = (INT64)Wrt_Val;
    //returnString = scaleNformatValue(var);
    //return  returnString; 
/****
    wchar_t fchar[64];  
    INT64  signedLongLong = (INT64)Wrt_Val;
    UINT64 llng;
    double localLong;
#ifndef _SOFTFLOAT_
    if(isSigned)
    {
        localLong  = signedLongLong      * (pScale->getScaleFactor());
        llng = (INT64)localLong; 
                        // stevev 19feb07 - merge - casting sequence to aviod roundoff error
    }
    else
    {
        localLong = uint64TOdbl(Wrt_Val) * (pScale->getScaleFactor());
        llng = (UINT64)localLong; 
                        // stevev 19feb07 - merge - casting sequence to aviod roundoff error
    }
#else // softfloat is included 
    float128 localLng, sclFact, fltVal;
    double x = pScale->getScaleFactor();
    float64 lf64x = *( (float64*) &x );
    sclFact = float64_to_float128( lf64x );

    if(isSigned)
    {
        fltVal   = int64_to_float128( ((INT64)Wrt_Val) );
        localLng = float128_mul(fltVal, sclFact);
        INT64 ll64y = float128_to_int64_round_to_zero( localLng );
        llng = (UINT64)ll64y;
    }
    else
    {
        double z = uint64TOdbl(Wrt_Val);
        lf64x    = *( (float64*) &z );
        fltVal   = float64_to_float128( lf64x );
        localLng = float128_mul(fltVal, sclFact);
        INT64 ll64y = float128_to_int64_round_to_zero( localLng );
        llng = (UINT64) ll64y;
    }
    lf64x     = float128_to_float64(localLng);
    localLong = *( (double*) &lf64x );
    //or ll64y
#endif
    // NOTE: 27jun08 - an email has been sent for ideas for how to handle this:
    //  The way we have done it with 32 bit integers is to convert to double, do the scaling 
    //  calculations and then convert the result back to integer.That works fine since a double
    //  has 52 bits of resolution. (32 -> 52 -> 32 is lossless).
    //  Doing this with 64 bit integers loses 12 bits of resolution, almost a full word.
    //  At this time, I'm leaving this alone. We'll see if anybody comes up with a good idea.
    //
    wstring fmtStr, fIStr;
    fIStr = getDispFormat();
    
    // determine int or double output required
    // if the user embedds one of these, he's SOL
    if ( fIStr.find_first_of(L"eEfgG") != std::string::npos )
    {// it's a float output
        swprintf(fchar,fIStr.c_str(),localLong);
        // note that %10.3g gives 2 decimal places and %16.4g gives 
        //      '     9.223e+018' (3)decimal places
        //      and %5.4f gives 9223372036854775800.000
    }
    else
    {// it's an int format
        swprintf(fchar,fIStr.c_str(),llng);   // will cast llng depending on 'd'||'u' in format
    }
    returnString = fchar;
#ifdef _DEBUG
    if (returnString.empty())
    {
        clog< <".";//get a breakpoint
    }
#endif
    return returnString; 
***/
}

inline
wstring& hCinteger::getEditValueString(void) 
{
    CValueVarient var; ////var = (INT64)Wrt_Val;
    
    updateCount();

    if (isSigned)
    {
        var = (INT64)Wrt_Val;
        if ( var.vIsUnsigned )
        {
            var.vIsUnsigned = false;
            LOGIT(CLOG_LOG,"Varient Error: did not get the integer signage correct.\n");
        }
    }
    else
    {
        var = Wrt_Val;//an UINT64
        if ( ! var.vIsUnsigned )
        {
            var.vIsUnsigned = true;
            LOGIT(CLOG_LOG,"Varient Error: did not get the integer signage correct..\n");
        }
    }

    int w,p,m;
    wstring fmtStr;
    /*int    getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
      wstring* pEditFmtStr = NULL, wstring* pScanFmtStr = NULL, wstring* pDispFmtStr = NULL);
    **/
    int t = getFormatInfo(w,p,m, &fmtStr, NULL,NULL); // edit format string 

    CValueVarient scaled;
    scaled = scaleValue(var, fmtStr);

    returnString = formatValue(scaled, fmtStr);

    return  returnString; 
}

wstring& hCinteger::getRealValueString(void)  // used for FILE outputs ONLY
{
	const size_t FCHAR_SIZE = 64;
	wchar_t fchar[FCHAR_SIZE];
	INT64  signedLongLong = (INT64)Value;// unsigned to signed conversion
	if (isSigned)
	{
		//_ltow(localLong,fchar,10);// 10::decimal, 16::hex(with leading '0x'),  2::binary	
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				L"%I64d",signedLongLong);
	}
	else
	{
		//_ultow(Value,fchar,10);// 10::decimal, 16::hex(with leading '0x'),  2::binary		
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				L"%I64u",Value);
	}
	returnString = fchar;
	return returnString; 
}
        
/*      
void hCinteger::setDispValueString(string& s) 
{
    UINT32 fL = 0;
    INT32 sfL = 0;
    CValueVarient aValue;

    if ( s.size() > 0 )
    {
        if (isSigned)
        {
            if ( 0 == sscanf(s.c_str(),"%i",&sfL) )
            {
                cerr < < "ERROR: setDisplayValueString (s) did not decode.|"< <s< <"|"< <endl;
            }
            else
            {// conversion from signed to unsigned storage
                aValue = sfL;
                setDispValue(aValue);
                //Wrt_Val = ((double)sfL) / (pScale->getScaleFactor());
                //if (IsLocal()) Value = Wrt_Val;
            }
        }
        else
        {
            if ( 0 == sscanf(s.c_str(),"%u",&fL) )
            {
                cerr < < "ERROR: setDisplayValueString (u) did not decode.|"< <s< <"|"< <endl;
            }
            else
            {// conversion from signed to unsigned storage
                aValue = fL;
                setDispValue(aValue);
                //Wrt_Val = ((double)fL) / (pScale->getScaleFactor());
                //if (IsLocal()) Value = Wrt_Val;
            }
        }
    }
    else
    {
cerr < < "ERROR: setDisplayValueString (Value from string) was passed an empty string."< <endl;
    }
}
*/


void hCinteger::setRealValueString(string& s) 
{
    UINT32 fL = 0;    
    INT32 sfL = 0;    
    if ( s.size() > 0 )
    {
        if (isSigned)
        {
            sscanf(s.c_str(),"%i",&sfL);
            // list is only staled if the incoming value from the device changes it
            // all other changes do not affect list reads
            //if (pList4Count != NULL && ((int)Value) != sfL)
            //{
            //  pList4Count->staleListOnCount(sfL);
            //}
            Value = sfL;// conversion from signed to unsigned storage 
        }
        else
        {
            sscanf(s.c_str(),"%u",&fL);
            // list is only staled if the incoming value from the device changes it
            // all other changes do not affect list reads
            //if (pList4Count != NULL && ((unsigned)Value) != fL )
            //{
            //  pList4Count->staleListOnCount(fL);
            //}
            Value = fL; 
        }
    }
    else
    {
        LOGIT(CERR_LOG,
            L"ERROR: setRealValueString (Value from string) was passed an empty string.\n");
    }
}
    
bool hCinteger::didMethodChange(void)
{
    return(Wrt_Val != cacheVal);//disp_value different from cached value
}

bool hCinteger::isChanged(void)
{
#ifdef _DEBUG
    if (Wrt_Val != Value)
    {
        unsigned ipd = getID();// debugging use
        LOGIF(LOGP_NOTIFY)(CLOG_LOG,
            " 0x%04x isChanged dis 0x%04I64x  dev 0x%04I64x  cah 0x%04I64x %s\n",getID(),
            Wrt_Val, Value, cacheVal,instanceDataStStrings[getDataState()]);
        return true;
    }
    else
    {
        return false;
    }
#else
    return(Wrt_Val != Value);
#endif
}

void hCinteger::updateCount(void) //protected
{   
    if (pList4Count == NULL || ! IsLocal() )
    {
        return;// device counts are handled in device
    }
    Value = Wrt_Val = pList4Count->getCount();
}

CValueVarient hCinteger::getRealValue(void)
{   CValueVarient retVal;
    retVal.vType = CValueVarient::isVeryLong;
    updateCount();// if we are a local count for a list, we need to update our values
    /*the verylong with a size of one was generating error messages and confusing other areas*/
    retVal.vSize = getSize();
    if ( retVal.vSize < 5 )
    {
        retVal.vType = CValueVarient::isIntConst;
        if(isSigned)
        {
            retVal.vValue.iIntConst = (INT32)Value;      
            retVal.vIsUnsigned      = false;
        }
        else
        {   
            retVal.vValue.iIntConst = (UINT32)Value;        // unsigned to signed conversion 
            retVal.vIsUnsigned      = true;
        }
    }
    else
    {
        retVal.vType = CValueVarient::isVeryLong;
        if(isSigned)
        {
            INT64 tmp_Value;
            tmp_Value = Value;
            retVal.vValue.longlongVal = (INT64)tmp_Value;      
            retVal.vIsUnsigned      = false;
        }
        else
        {   
            retVal.vValue.longlongVal = (UINT64)Value;        // unsigned to signed conversion 
            retVal.vIsUnsigned      = true;
        }
    }
    
    if   (IsValidTest()) { retVal.vIsValid = true;  }
    else { retVal.vIsValid = false; } //VMKP changed on 010404

    //if   (isSigned){ retVal.vIsUnsigned = false; }
    //else {retVal.vIsUnsigned = true;}
    return retVal;
}

//      this is from device object to user (multiply by scaling factor)
//      use sizeAndScale to get the opposite function
CValueVarient hCinteger::scaleValue(CValueVarient toBscaled, wstring targetFmt)
{
    CValueVarient retVal;
    UINT64 llng;
    INT64  ilng;
    double localLong;
    wstring locEx;
    bool isSigned;

#ifndef _SOFTFLOAT_
    INT64  signedLongLong = (INT64)toBscaled;
    if(isSigned)
    {
        localLong  = signedLongLong * (pScale->getScaleFactor());
        llng       = (INT64)localLong; 
    }
    else
    {
        localLong = uint64TOdbl(Wrt_Val) * (pScale->getScaleFactor());
        llng      = (UINT64)localLong;
    }
#else /*softfloat is included */
    float128 localLng, sclFact, fltVal;
    double x = GetScalingFactor();
    float64 lf64x = *( (float64*) &x );
    sclFact = float64_to_float128( lf64x );

	//if(isSigned)
	if ( ! toBscaled.vIsUnsigned )
	{//signed
		fltVal   = int64_to_float128( ((INT64)toBscaled) );
		localLng = float128_mul(fltVal, sclFact);
		ilng = float128_to_int64_round_to_zero( localLng );
		//// llng = (UINT64)ll64y;
		isSigned = true;
	}
	else
	{// unsigned
		isSigned = false;
		float128  first;
		float128  second = int64_to_float128( (INT64)0x7fffffffffffffffULL ); // L&T Modifications : PortToAM335
		float128  add    = int64_to_float128( (INT64)1 );
		float128  tmp    = float128_add( second, add );//8000000000000000

		if (VariableSize() > 7)
		{// special unsigned to flt128
			UINT64 vin = (UINT64)toBscaled;
			INT64  win = vin & 0x7fffffffffffffffULL; // L&T Modifications : PortToAM335
			bool hiSet  = ( (vin & 0x8000000000000000ULL) != 0 ); // L&T Modifications : PortToAM335

            fltVal = int64_to_float128( win );
            first  = fltVal;
            if (hiSet)
            {
                fltVal = float128_add( tmp, first ); // overwrite win
                first  = float128_add( tmp, second); //ffffffffffffffff
            }
        }
        else
        {// we can just assign it, it'll fit in a double
            double z = uint64TOdbl((UINT64)toBscaled);
            lf64x    = *( (float64*) &z );
            fltVal   = float64_to_float128( lf64x );
        }

		localLng = float128_mul(fltVal, sclFact);
		
		if( float128_lt( localLng, tmp ) )// signed == unsigned
		{
			INT64 ll64y = float128_to_int64_round_to_zero( localLng );
			llng = (UINT64) ll64y;
		}
		else // convert
		if (float128_lt( localLng, first ))// it'll fit but hi bit is set
		{
			second = float128_sub( localLng, tmp );
			ilng   = float128_to_int64_round_to_zero( second );
			llng = ((UINT64) ilng) + 0x8000000000000000ULL; // L&T Modifications : PortToAM335
		}
		else// scaled value won't fit into an UINT64
		{
			LOGIT(CLOG_LOG|CERR_LOG,"ERROR: scaled value will not fit an unsigned (8)!\n");
			llng = 0xffffffffffffffffULL;// as close as we go // L&T Modifications : PortToAM335
		}
	}// end unsigned scale
	lf64x     = float128_to_float64(localLng);// extract a double
	localLong = *( (double*) &lf64x );        // extract a double
#endif
    //scaled values are in llng(unsigned) and localLong
    retVal.clear();

    // determine int or double output required
    //if (pDispFmt)
    //{ 
    //  pDispFmt->getFmtStr(locEx);  
    //  pDispfmtStr = (wstring)locEx;
    //}
    //if ( (pDispfmtStr.find_first_of(L"eEfgG") != std::wstring::npos ) )
    if ( (targetFmt.find_first_of(L"eEfgG") != std::wstring::npos ) )
    {// it's a float
        retVal.vType = CValueVarient::isFloatConst;
        retVal = localLong;
    }
    else
    {//format doesn't exist or is not a float
        // note: we don't care how big the variable size is, we are using a scaled value here
        // we'll make it long long to match vType( 0 length is considered invalid in compare)
        retVal.vSize = 8;
        retVal.vType = CValueVarient::isVeryLong; 
        if (isSigned)
        {
            retVal.vValue.longlongVal = (INT64)ilng;
            retVal.vIsUnsigned = false; 
        }
        else
        {
            retVal.vValue.longlongVal = (UINT64)llng;
            retVal.vIsUnsigned = true; 
        }
    }
/* End of VMKP Modified on 180204 */
    // stevev 09feb06 - common code moved to common location (here)
    if   (IsValid()) { retVal.vIsValid = true;}
    else { retVal.vIsValid = false; }
    return retVal;
}

CValueVarient hCinteger::getDispValue(bool iHaveMutex) // may be unused, included for symmetry
{   
    CValueVarient var, ret;;

    updateCount();

    var = (INT64)Wrt_Val;

    int w,p,m;
    wstring fmtStr;
    // what was I thinking....int t = getFormatInfo(w,p,m, &fmtStr, NULL,NULL); // edit format string
    int t = getFormatInfo(w,p,m, NULL, NULL, &fmtStr); // display format string
    ret   = scaleValue(var, fmtStr);
    return ( ret );
}

/*Vibhor 272004: Start of Code*/
CValueVarient hCinteger::getRawDispValue(void) // may be unused, included for symmetry
{   
/*As the name suggests this function returns the "raw" value of a var
 as returned from the device... No Scaling and No conversion from int 
 to float if the edit/disp formats are floats */
    
    CValueVarient retVal;

    retVal.vType = CValueVarient::isIntConst;   
    retVal.vSize = getSize();

    updateCount();

    if(isSigned)
    {
        if (retVal.vSize <= sizeof(int))
        {
            retVal.vValue.iIntConst = (INT32)Wrt_Val;
            retVal.vType = CValueVarient::isIntConst;   
        }
        else // its a long long
        {
            retVal.vType = CValueVarient::isVeryLong;   
            retVal.vValue.longlongVal = (INT64)Wrt_Val;
        }
        retVal.vIsUnsigned = false;
    }
    else  // unsigned
    {
        if (retVal.vSize <= sizeof(int))
        {
            retVal.vValue.iIntConst = (((UINT32)Wrt_Val) & 0xffffffff);
            retVal.vType = CValueVarient::isIntConst;
        }
        else // its a long long
        {
            retVal.vType = CValueVarient::isVeryLong;   
            retVal.vValue.longlongVal = (INT64)Wrt_Val;
        }
        retVal.vIsUnsigned      = true;
    }

    // moved up      retVal.vSize = getSize();
    if  (IsValid())retVal.vIsValid = true; 
    else           retVal.vIsValid = false;
    //if  (isSigned) retVal.vIsUnsigned = false;
    //else retVal.vIsUnsigned = true;


    return retVal;
}


/*Vibhor 272004: End of Code*/
//used from Integer Extract, and from Send to set Indexes
bool hCinteger::SetIt(void* pValue) // returns isChanged
{   Wrt_Val = *((UINT32*)pValue); // returns isChanged - I don't think this should be scaled
    return isChanged();
}
    
bool hCinteger::ApplyIt(void)                   // returns wasChanged
{ 
    bool r = isChanged(); 
    // list is only staled if the incoming value from the device changes it
    // all other changes do not affect list reads
    //if (pList4Count != NULL && Value != Wrt_Val && ! IsLocal() )
    //{// max size of a list is limited to an unsigned int
    //  pList4Count->staleListOnCount((UINT32)Wrt_Val);
    //}
    Value = Wrt_Val; 
    return r;
}

bool hCinteger::CancelIt(void)                      // returns wasChanged
{ 
    bool r = isChanged(); 
    Wrt_Val = Value; 
    setWriteStatus(0);/*sjv 25jun07*/
    return r;
};

//returns the current value as a formatted string
DATA_QUALITY_T hCinteger::Read(wstring &value)
{   //                  what to do with unsigned that are scaled and use a float format string?
    //char val[128];
    CValueVarient  vVal;
    DATA_QUALITY_T rq      = hCVar::Read(vVal);

    if ( rq != DA_HAVE_DATA && rq != DA_STALE_OK )
    {
        value = L"";        // we are going to have to deal with status at some point
    }
    else //use Value in memory
    {
        value = getDispValueString() ;
    }
    return rq;
}

RETURNCODE hCinteger::Write(const wstring value)  
{
    RETURNCODE rc = FAILURE;    
    
    UINT32 b = 0;
    wchar_t fmtS[6] = L"%u";

    if (isSigned)
    {
        fmtS[1] = L'd'; // gives %d
    }

    if (! IsReadOnly() )  
    {   // buf should hold the data
        if ( swscanf(value.c_str(),fmtS, &b) > 0)
        {
            rc = Write(b);// will be scaled in this write
        }
    }
    else 
    { 
        rc = API_FAIL_READ_ONLY; 
    }
    return rc;
}

RETURNCODE hCinteger::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
                                methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                                varPtrList_t &hasChangedList, extract_t extFlags )
{
    int    s = VariableSize(); // short cut to reduce the fetches
    /* stevev 3/22/04 - added to handle Error Packets */
    if ( ( (*byteCount) < s && len <= 0 ) || ( len > 0 && (*byteCount) < len ) )
    {
        LOGIT(CERR_LOG,
                "Message too short for Int variable.(have %d bytes to fill a %d byte var)\n",
                (*byteCount),s);
    }/* end add */
#ifdef _DEBUG 
    if (s < 1 || s > 8 || len > 8)
    {
        LOGIT(CERR_LOG, "ERROR: hCinteger.  Variable size is illegal (%d)\n",s);
        return FAILURE;
    }
#endif
    UINT64 t = 0,q = 0, V;  /* added q 29sep05 */
    bool isDifferent = false;
    BYTE *pValue;
    RETURNCODE rc = SUCCESS;

    /**** special response code / comm status multiplexed integer *****/
    itemID_t symID = getID();// comm status = 152;  device status = 150
    hCitemBase* pItm;
    hCBitEnum*  pInt;
    int         emptyBE = 0;

    if ( symID == RESPONSECODE_SYMID )
    {
        rc = devPtr()->getItemBySymNumber(COMMSTATUS_SYMID,&pItm);
        if (rc == SUCCESS && pItm != NULL )
        {// do comm status
            pInt = ((hCBitEnum*)((hCVar*)pItm));
            if ( (*data)[0] & 0x80 ) //hi bit of next datum set
            {//
                Value = 0; // clear this one - set the other one
                return pInt->
                    Extract(byteCount,data,mask,len, eaLst, noteMsgs, hasChangedList,extFlags);
            }
            else // we're in the right place
            {// clear the other one
                rc = pInt->SetIt(&emptyBE);
                rc = pInt->ApplyIt();
            }
        }
    }
    else 
    if ( symID == COMMSTATUS_SYMID   )
    {
        rc = devPtr()->getItemBySymNumber(RESPONSECODE_SYMID,&pItm);
        if ( rc == SUCCESS && pItm != NULL )
        {// do response code
            pInt = ((hCBitEnum*)((hCVar*)pItm));
            if ( ! ((*data)[0] & 0x80) ) //hi bit clr
            {//
                Value = 0; // clear this one - set the other one
                return  pInt->
                    Extract(byteCount,data,mask,len,eaLst, noteMsgs, hasChangedList,extFlags);
            }
            else // we're in the right place
            {// clear the other guy
                rc = pInt->SetIt(&emptyBE);
                rc = pInt->ApplyIt();
            }
        }
    }
    // else - just process it
    /******* end special multiplexed parameter code ********/
    
    if ( len > 0 ) // we are involved in a masking operation
    {   
        // we assume the size is 1 to 4 !!!!!!!!!!!
        // get the raw byte set from the message
        pValue = ((BYTE*)(&t)) + sizeof (t) - len; // offset into the UINT32
        memcpy(pValue, *data, len);     // get the raw value from the message
        t   = REVERSE_H(t);             // make it intel friendly
        
        t &= mask;// extract the answer
        t >>= right0bits(mask); // right justify it(right0bits() returns number clr bits
        // put it into the buffer
        /* changed from     stevev 29sep 05 
        V = t;
        **** change to:    */
        V = Value;
        q = ~(mask >> right0bits(mask));
        V = ( V & q) | t;
        /* end change 20sep05 sjv */
        rc = SUCCESS;
    }
    else // normal path
    {   t = 0;
        pValue  = ((BYTE*)(&t)) + sizeof (t) - s;   //calc offset into Value
        rc = ExtractIt(byteCount, data, pValue, s); // pValue points in to t 0,1,2 or 3 bytes
                                                    //        ..always returns success..
        V   = REVERSE_H(t);

		// deal with sign extension
		//  is a signed int AND not already signed(8,input already) AND sign bit set (negative)
		// for debugging
		UINT64  intermediateValueONE = (0x80LL << ((s-1)*8));//<< note that's 80LL			
		UINT64  intermediateValueTWO = V & intermediateValueONE;
		if ( isSigned && (s < 8)  && intermediateValueTWO != 0 )
		//if ( isSigned && (s < 8)  && ((V & (0x80 << ((s-1)*8))) != 0) )  
		{
			/* NOTE: Mr gate's math is different from mine!
			 he believes that 0xffffffff << 32 is equal to 0xffffffff
			 oh *&^%$#@  */
			//UINT32 yy = 0xffffffff;
			// s == 8 can't happen   UINT64 yy = 0xffffffffffffffff;
			//if ( s == 8 )
			//{
			//	yy = 0;
			//}
			//else
			//{
			//	yy = (0xffffffffffffffff << (s*8));
			//}
			UINT64 yy = 0xffffffffffffffffULL << (s*8) ;// shift in zeros for original value // L&T Modifications : PortToAM335
			V = (V) | yy;
		// was::>	
		//	V = (V) | (0xFFFFFFFF << (s*8));     // sign extend it
		}
		if (VariableType() == vT_Index && rc == SUCCESS)
		{
			CValueVarient val; val = V;
			//if (! isInRange(val) )- cannot send a command from extract
			if (! isAvalidValue(val) )
			{
				if (compatability() == dm_275compatible)
				{
					; /* don't reject */
				}
				else
				if (VariableSize() == 1 && V >= 250)// indexes of size 1 that have an invalid 
													  // value >= 250 are encoded flags in the 
													  // index type
				{
					/*rc = APP_INDEX_SPECIAL_VALUE;  don't reject */
				}
				else
				{
					// 13may11...doNotApply = true;
					extFlags = (extFlags & DoInfoValue)?InfoNotApply:DoNotApply;
					rc = APP_OUT_OF_RANGE_ERR;
				}
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|UI_LOG,
					"ERROR: command received an invalid index.[%s got %d] and \n  stopped "
					"extracting data from the reply packet.\n",	getName().c_str(),(int)val);
			}
		}
	}

    // stevev 2/25/04   
    if ((extFlags & DoNotApply) == DoNotApply) // stevev 13may11
    //if (doNotApply)
    // if (doNotApply || getDataState() != IDS_PENDING)//  stevev 2/25/04
    {
        return rc;
    }

    bool isInfo = ((extFlags & DoInfoValue) == DoInfoValue);

/*** bit enum actions :: added by stevev 29sep05 */
    if ( !isInfo && Value != V && VariableType() == vT_BitEnumerated )
    {// something changed
        hCenumDesc anEnum(devHndl()  SRC_EXTRACT);
        hCitemBase* pIB = NULL;
        CValueVarient        retVal, methRetValue;
        hCEnum* pE = (hCEnum*)this;
        hCmethodCall wrkMthCall;

		q = V & (Value ^ V);
		while ( q ) // a bit went set
		{

#if defined(__GNUC__)
			t = __UINT64_C(1) << right0bits(q);
#else
			t = 1ui64 << right0bits(q);
#endif // __GNUC__
			// add actions if there are any
			// enum size is limited to a unsigned int
			if (SUCCESS == pE->procureEnum((UINT32)t,anEnum) && 
				// this was not detecting a missing reference
				//anEnum.actions.getRefType()!=rT_NotA_RefType  )
				! anEnum.actions.isEmpty() )
			{
				if ( SUCCESS == anEnum.actions.resolveID( pIB ) && pIB != NULL )
				{
					if ( pIB->getIType() == iT_Method )
					{// fill a method call with varID, msrc_ACTION,methodID
						wrkMthCall.methodID  = pIB->getID();
						wrkMthCall.source    = msrc_ACTION;
						wrkMthCall.paramList.push_back(retVal);
						eaLst.push_back(wrkMthCall);
						wrkMthCall.clear();
					}
					else
					{
					// error - non-method spec'd for action
					}
				}// else - proly has no action
			}
			q ^= t; // turn off the bit and loop for more
		}
// 11oct05 - pass in a list to fill, execute before post rd/wr actions
//      if (methList.size() > 0)
//      {
//          rc = devPtr()->executeMethod(methList, methRetValue);
//      }
    }
/** end enum actions */

    devPtr()->aquireItemMutex(
#ifdef _DEBUG
                            __FILE__,__LINE__);
#else
                              );
#endif
    isDifferent = (Value != V);
    if (previousDataState == IDS_INVALID || previousDataState == IDS_UNINITIALIZED)
    {
        isDifferent = true;
    }

    Value = V; 

    // stevev 15dec05 - as per DEEPAK 122404
    markItemState( IDS_CACHED );

    /* stevev 3/2/4 - preclude applying user written variables */
    // pre 28nov05:
    // if (getWriteStatus() == 0 ||(isDifferent && devPtr()->isItemCritical(getID())))
    //post 28nov05 - never execute if no change, 
    if (!isInfo && isDifferent && (getWriteStatus() == 0 || devPtr()->isItemCritical(getID())))
    {// go ahead and apply it

        /* pre 28nov05 --------------------------------------------
        if (isDifferent && devPtr()->isItemCritical(getID()))
        {
        //  setDependentsUNK();
        //NUA_t p = NO_change; if (isDifferent) p = IS_changed;
        //devPtr()->notifyVarUpdate(symID,p);
        // filter unchanged, get all that this affects...

            notify(noteMsgs);
        }
        ---post 28nov05 --------------------------------------------***/
        if ( getWriteStatus() == 0 )
        {// notify self if not user written
            noteMsgs.insertUnique(getID(), mt_Val, 0);
        }
        // get all that this affects...
//9-9-9 lets do this later....notify(noteMsgs);// fills list from dependents (if there are any)
        hasChangedList.push_back(this);// stevev 9-9-9
        /*-- end 28nov05 ---------------------------------------------*/

    }/* end stevev 3/2/4 */
    if (!isInfo && isDifferent && dominantRelations.size() > 0 
        /* stevev 22feb06 */&& devPtr()->getDeviceState() == ds_OnLine)
    {
        rc = MarkDependStale();// Mark the dependent stale
    }
    if ( devPtr()->getPolicy().isReplyOnly )// simulator
    {
        if ( isDifferent && IsConfig() )
        {       
            RETURNCODE      rc;
            hCitemBase*     pItem = NULL;
            hCVar*          pVar  = NULL;
            CValueVarient   vv;
            unsigned char   iv;
            if (( rc = devPtr()->getItemBySymNumber(DEVICESTATUS_SYMID,&pItem))== SUCCESS
             && (pItem != NULL) )
            {
                pVar = (hCVar*) pItem;
                vv = pVar->getDispValue();
                iv = vv;
                iv |= CONFIG_CHANGED_BIT ;          
                vv = iv;
                pVar->setDispValue(vv);
            }
        }
        // note that the value must be copied when it's xmtr
        CancelIt();
    }// endif reply only

    // moved earlier - stevev 15dec05  markItemState( IDS_CACHED );
    /* * this should be the ONLY place a non-local's previous should be cleared */
    previousDataState = IDS_CACHED;// stevev 05jul05 - if its a local we'll never be here

    LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"    Ext: 0x%04x. marked as cached. (%I64d)ext64\n",symID,Value);

    devPtr()->returnItemMutex();
    // stale list can require the item mutex, execute it AFTER we release the mutex
    // this should be the only place this occurs
    if (!isInfo && pList4Count != NULL && isDifferent/*was  Value != V */)
    {// max size of a list is limited to an unsigned int
        pList4Count->staleListOnCount((UINT32)V);
    }
    return rc;  
}

RETURNCODE hCinteger::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{
    int     s = VariableSize();
    UINT64 wv = Wrt_Val;
    UINT64 xv = 0;
    UINT64 t  = 0;
    BYTE *pValue;
#ifdef _DEBUG 
    if (s < 1 || s > 8)
    {
        LOGIT(CERR_LOG,L"ERROR: hCinteger.  Variable size is illegal (%d)\n",s);
        return FAILURE;
    }
#endif
    /* stevev 2/12/04 - allow comm status handling in reply packet */
    if ( getID() == RESPONSECODE_SYMID )// only in reply packet
    {
        RETURNCODE      rc;
        hCitemBase*     pItem = NULL;
        hCVar*          pVar  = NULL;
        CValueVarient   vv;
        int             iv;
        if (( rc = devPtr()->getItemBySymNumber(COMMSTATUS_SYMID,&pItem))== SUCCESS
         && (pItem != NULL) )
        {
            pVar = (hCVar*) pItem;
            vv = pVar->getDispValue();
            iv = vv;
            if ( iv != 0 )
            {
                return pVar->Add(byteCount,data,mask,len);
            }
        }
    }
    if ( getID() == COMMSTATUS_SYMID )// only in reply packet
    {
        wv |= 0x80; // be sure the high bit is set
    }
    /* stevev 2/12/04 */
    // we assume the size is 1 to 4 !!!!!!!!!!!
    if ( len > 0 ) // we are involved in a masking operation
    {   // prepare the answer
        int
        y   = right0bits(mask);
        xv  = wv << y;
        wv  = xv & mask;
        // get the results of previous efforts
        pValue = ((BYTE*)(&t)) + sizeof (t) - len;
        memcpy(pValue, *data, len);     // get the latest value 
        t   = REVERSE_H(t);             // make it intel friendly
        // insert the answer
        t &= ~mask;
        t |= wv;
        // put it into the buffer
        t = REVERSE_H(t);// note that pValue hasn't changed 
        memcpy(*data, pValue, len);
        return SUCCESS;// note that the data item will reset the data pointer & byteCnt 
    }
    else // normal path
    {
        t = REVERSE_H(wv);
        pValue = ((BYTE*)(&t)) + sizeof (t) - s;    //calc offset into uValue
        return AddIt(byteCount, data, pValue, s);
    }
}

    
//CW fix begin - B27901 added missing reference operator  // 29aug11
RETURNCODE hCinteger::Write(CValueVarient& newDataValue)
//CW fix end
{
    UINT32 b  = newDataValue;
    return (hCinteger::Write(b));// will be scaled
}


void hCinteger::clear(void)
{        
    // list is only staled if the incoming value from the device changes it
    // all other changes do not affect list reads
    //if (pList4Count != NULL && Value != -1)
    //{// max size of a list is limited to an unsigned int
    //  pList4Count->staleListOnCount((UINT32)Value);
    //}
    Value = Wrt_Val = -1;// was 0; Vibhor 241203 

}

void hCinteger::setDispValue(CValueVarient& newValue, bool iHaveMutex)
{   
    int    s = VariableSize(); 
// unused 10feb06   unsigned int tmp;
    CValueVarient T;
    // stevev 10feb06 - use common code to size and test 
    T = newValue;
    if ( sizeAndScale(T) /*&& T == newValue allow scaled value update */)
    {// it is in range
#ifdef USE_INDEX_MUTEX
        if ( VariableType() == vT_Index &&  ! iHaveMutex )
        {
            ((hCddbDevice*)devPtr())->aquireIndexMutex(10000);
        }
#endif
        Wrt_Val = T;
#ifdef USE_INDEX_MUTEX
        if (  VariableType() == vT_Index &&  ! iHaveMutex )
        {
            ((hCddbDevice*)devPtr())->returnIndexMutex();
        }
#endif
    }
    else
    {// out of range
        string z;
        z = getName();
        LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: %s Var 0x%04x (%s) has a Set Display "
                "Value (0x%x) that is not a valid value.\n",VariableTypeString(),getID(),

        z.c_str(),(UINT32)newValue);// sjv 29jun06 was: Wrt_Val);
        Wrt_Val = T; // added by sadanand utthunga (to handle invalid value)
        // return; // we won't do anything with it
	}
	
// stevev 10feb06 - 	tmp = (UINT32) (((double)newValue) / (pScale->getScaleFactor()));
// stevev 10feb06 - 	T   = tmp;

// stevev 10feb06 -     Wrt_Val = T;
// stevev 10feb06 - Wrt_Val = limitAndExtend(Wrt_Val);// will set index & date to a valid value
// stevev 10feb06 - if ( getID() == 0xa2 )
// stevev 10feb06 - {
// stevev 10feb06 -     clog< <"Pause for poll address."< <endl;
// stevev 10feb06 - }
// stevev 10feb06 -     if (! isInRange(T) )
// stevev 10feb06 -     {
// stevev 10feb06 -         string z;
// stevev 10feb06 -         z = getName();
// stevev 10feb06 -LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s Var 0x%04x (%s) has a Set Display "
// stevev 10feb06 -"Value (0x%x) that is not a valid value.\n",VariableTypeString(),getID(),
// stevev 10feb06 -                 z.c_str(),(UINT32)Wrt_Val);
// stevev 10feb06 -     }
    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( ( (Value != Wrt_Val) ) // stevev 02jan07 
                                    //     - locals must be updated reguardless of criticality
            /* preclude - NON-user written, local indices */
            && (! (getWriteStatus() == 0 && VariableType() == vT_Index))
            )
        {/* needs notification */
            hCmsgList  noteMsgs;// moved 04dec06
                //setDependentsUNK();
                //NUA_t p = NO_change; if (Value != Wrt_Val) p = IS_changed;
            if (
            ApplyIt()//;//Value = Wrt_Val;
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);// copies devVar->dispVal

            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}

void hCinteger::setRealValue(CValueVarient& newValue, bool iHaveMutex)
{   
    Value = (UINT64)newValue;
}

// stevev 18nov05 - moved from .h file
void hCinteger::setRawDispValue(CValueVarient& newValue)
{
    Wrt_Val = limitAndExtend((UINT64) newValue);
    /* stevev 25apr05 - we still need notification
    if (IsLocal())
    {
        ApplyIt();
    }
    */

    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( ( (Value != Wrt_Val) ) // stevev 02jan07 
                                    //     - locals must be updated reguardless of criticality
            /* preclude - NON-user written, local indices */
            && (! (getWriteStatus() == 0 && VariableType() == vT_Index))
            )
        {/* needs notification */
            hCmsgList  noteMsgs;
            //setDependentsUNK();
            //NUA_t p = NO_change; if (Value != Wrt_Val) p = IS_changed;
            if (
            ApplyIt()//;//Value = Wrt_Val;  
            && ! devPtr()->isInMethod()
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);// copies devVar->dispVale
            
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}
    
RETURNCODE hCinteger::setDisplayValueString(wstring& s)// from UI with range test
{   
    CValueVarient aValue;
    RETURNCODE rc = SUCCESS;

    aValue = getValueFromStr(s);
    if (aValue.vIsValid)
    {
        return setDisplayValue(aValue);
    }
    else
    {
        if (aValue.vType == CValueVarient::isVeryLong)
        {
            /*
             * Values were assigned so there must have been
             * an overflow issue. Inform the user of an out
             * of range issue.
             * Fixed defect #4502 and #4503, POB - 4/17/2014
             */
            return APP_OUT_OF_RANGE_ERR;
        }
        else
        {
            return APP_PARSE_FAILURE;
        }
    }
}

RETURNCODE hCinteger::setDisplayValue(CValueVarient& newValue)// from UI w/ rangetest 
{
    if ( isInRange(newValue) )
    {
        setDispValue(newValue);
        return SUCCESS;
    }
    else
        return APP_OUT_OF_RANGE_ERR; 
}

// stevev 10feb06 - common code for range & scale and size 
// false = out-of-range, true=new value in retVal
//      this is from user to device object (divide by scaling factor)
//      use scaleNformatValue or scaleValue to get the opposite function
/*  stevev 17feb11 - the incoming value must NOT be sign extended.  If the user put in a
        negative number, the routine that converts must sign extend...we can't tell if a
        0x00800000 (we're a 3 byte signed int) was put in by the user as 8388608 (too big 
        a number and needs to be rejected) or, possibly, -8388608.  Only the routine that sees
        the string can tell if there was a negative sign inserted.
        RULE: sign-extend right before inserting into the VARIABLE's value 
        AND:  when the user string is converted to a real value (scanf should handle it)
***/
bool   hCinteger::sizeAndScale(CValueVarient& retVal)
{
    bool brv = true;
    // UINT64 iRet;
    UINT64 iLoc = 0;
        
#ifndef _SOFTFLOAT_
//29jun06 changed from: double R = (((double)retVal) / (pScale->getScaleFactor()));
    double R = (double)retVal;//handles signed or unsigned, int or long long
    if ( pScale != NULL )
    {
        R = (R) / GetScalingFactor();//was::>pScale->getScaleFactor();
    }
    // else leave it retVal
// end 29jun06 change
    /* NOTE: if you are tracing into this routine looking for 64 bit math issues, be sure to 
       check your round off errors.  As an example: 0x7fffffffffffffff displayed as %6.3f gives
       627963145224193 round-off error. That's a six hundred and twenty eight TRILLION error.
    */
    double B = 0.0;
    if ( isSigned )
    {// if ( R > numeric_limits<int>::max() || R < numeric_limits<int>::min() )
                                                        ////<limits> conflict w/ 'ddbDevice.h'
        if ( R > _I64_MAX || R < _I64_MIN)
        {   /* we cannot represent this number as an int - must be out of range */
            return false;
        }
        //retVal = (INT32) (R + 0.50 );// try and overcome the truncation error
        modf(R, &B);
        retVal = (INT64) B;
    }
    else // has to be unsigned
    {
        //if ( R > numeric_limits<unsigned int>::max() || 
                                                        ////<limits> conflict w/ 'ddbDevice.h'
        //   R < numeric_limits<unsigned int>::min() )
        if ( R > uint64TOdbl(_UI64_MAX) || R < 0 )
        {   /* we cannot represent this number as an int - must be out of range */
            return false;
        }
        //retVal = (UINT32) (R + 0.50 );// try and overcome the truncation error
        modf(R, &B);
        retVal = (UINT64) B;
    }
#else // soft float is here - stevev 01jul08
    INT64  sllng;
    UINT64 ullng;

    double   locDbl;
    float64  lf64x;
    float128 sclFact,rawVal,tmpVal;

    /* stevev 02jun09 - modified to sign-extend smaller signed integers */
    // stevev 17feb11 - modified to NOT sign extend incoming value (host test)

	if ( retVal.vType == CValueVarient::isIntConst  ||
		 retVal.vType == CValueVarient::isVeryLong    )
	{// the integer types
		if ( retVal.vIsUnsigned )
		{
			if ( retVal.vType == CValueVarient::isIntConst )
			{
				unsigned y = (UINT32)retVal;
				if ( (y & 0x80000000) == 0 )// hi bit clear
				{// same as int
					sllng = y;
				}
				else					   // hi bit set
				{// could be mistaken as negative- be sure it is as-arrived
					sllng  = y & 0x7fffffff;// get the lower part
					sllng += 0x7fffffff;    // get most of the hi bit
					sllng += 1;             // get the rest of the hi bit
				}
				rawVal = int64_to_float128( sllng );
			}
			else // isVeryLong
			{
				ullng = (UINT64)retVal;
				if ( (ullng & 0x8000000000000000ULL) == 0 ) // L&T Modifications : PortToAM335
				{	// hi bit clear
					rawVal = int64_to_float128( (INT64)ullng );
				}
				else // hi bit set
				{// bill doesn't support a straight cast...
												// get all but hi bit
					rawVal = int64_to_float128( (INT64)(ullng & 0x7fffffffffffffffULL) ); // L&T Modifications : PortToAM335
					tmpVal = int64_to_float128(0x7fffffffffffffffULL); // L&T Modifications : PortToAM335
												// add most of the hi bit
					rawVal = float128_add(rawVal,tmpVal);
#if defined(__GNUC__)
					tmpVal = int64_to_float128(__INT64_C(0x0000000000000001)); 
#else
					tmpVal = int64_to_float128(0x0000000000000001i64); 
#endif // __GNUC__
												// add the rest of the hi bit
					rawVal = float128_add(rawVal,tmpVal);				
				}
			}
		}
		else // signed
		{
			if ( retVal.vType == CValueVarient::isIntConst )
			{
				int y = (INT32)retVal;
				sllng = y;
			}
			else // isVeryLong
			{
				sllng  = (INT64)retVal;
			}
			/* stevev 17feb11..DO NOT sign extend 
			int nSize = VariableSize();
			assert( nSize > 0 && nSize < 9 );
			if ( (sllng & ( 1ui64 << ((nSize*8)-1) )) != 0 )
			{// we have to extend it
				sllng = sllng | (_UI64_MAX << (nSize*8));
			}
			*/
			// else hi bit clear, leave it be
			rawVal = int64_to_float128( sllng );
		}
	}
	else
	if (retVal.vType == CValueVarient::isFloatConst )
	{// this'll handle float and double
		locDbl = (double)retVal;
		lf64x  = *( (float64*) &locDbl );
		rawVal = float64_to_float128( lf64x );
	}
	else
	{// error, wrong function called
		return false;// out of range
	}

//  if ( pScale != NULL )// stevev 7aug08 this is now private
//  {// there is probably a better way to do this but oh well...
        locDbl  = GetScalingFactor();//was::>pScale->getScaleFactor();
        lf64x   = *( (float64*) &locDbl );
        sclFact = float64_to_float128( lf64x );

        rawVal = float128_div(rawVal,sclFact);// value / scale-factor
//  }
    // else leave it rawVal

	/* NOTE: if you are tracing into this routine looking for 64 bit math issues, be sure to 
	   check your round off errors.  As an example: 0x7fffffffffffffff displayed as %6.3f gives
	   627963145224193 round-off error. That's a six hundred and twenty eight TRILLION error.
	*/
	float128 i64Max,i64Min,u64Max,i64zero;
	i64Max = int64_to_float128( _I64_MAX );
	i64Min = int64_to_float128( _I64_MIN );
#if defined(__GNUC__)
	i64zero= int64_to_float128(__INT64_C(0x0000000000000000));
#else
	i64zero= int64_to_float128(0x0000000000000000i64);
#endif // __GNUC__
	
	// generate an unsigned 64 bit max using signed math only
	u64Max = int64_to_float128( (INT64)(_UI64_MAX & 0x7fffffffffffffffULL) ); // L&T Modifications : PortToAM335
	if ( (_UI64_MAX & 0x7fffffffffffffffULL) != 0 )// always true // L&T Modifications : PortToAM335
	{
		tmpVal = int64_to_float128(0x7fffffffffffffffULL); // L&T Modifications : PortToAM335
		u64Max = float128_add(u64Max,tmpVal);
#if defined(__GNUC__)
		tmpVal = int64_to_float128(__INT64_C(0x0000000000000001));
#else
		tmpVal = int64_to_float128(0x0000000000000001i64);
#endif // __GNUC__
		u64Max = float128_add(u64Max,tmpVal);
	}

    // check the natural range first
    if ( isSigned )
    {//                         <                               <
        if ( float128_lt(i64Max , rawVal) || float128_lt(rawVal , i64Min)  )
        {   /* we cannot represent this number as an int - must be out of range */
            return false;
        }
        // standard C spec states that float to int conversion will TRUNCATE the float
        // stevev 27apr11 - stopped following the C spec to conform to the host test.
        // instead of truncating, we now round.
        //sllng  = float128_to_int64_round_to_zero(rawVal);//aka: truncation
        sllng  = float128_to_int64(rawVal);
        retVal = sllng;
    }
    else // has to be unsigned
    {//   
#ifdef _DEBUG       
        double dRaw = (double)((__int64)float128_to_float64( rawVal ));
        double dUMx = (double)((__int64)float128_to_float64( u64Max ));
        double dZro = (double)((__int64)float128_to_float64( i64zero));
#endif
        bool  first, secnd;
        if ( float128_le(rawVal, u64Max) == 0 ) first = false; else first = true;
        if ( float128_lt(rawVal, i64zero)== 0 ) secnd = false; else secnd = true;
        //        >          <
        if ( (! first) ||  secnd )
        {   /* we cannot represent this number as an int - must be out of range */
            return false;
        }
        // else convert
        //                      <                 
        if ( float128_lt(i64Max , rawVal)  )
        {// too big for a signedrawVal > i64max
            // reuse a float
            //i64zero = float128_sub(i64Max,rawVal);
            i64zero = float128_sub(rawVal,i64Max);
            // standard C spec states that float to int conversion will TRUNCATE the float
            // stevev 27apr11 - stopped following the C spec to conform to the host test.
            // instead of truncating, we now round.
            //sllng  = float128_to_int64_round_to_zero(i64zero);//aka: truncation
            sllng  = float128_to_int64(rawVal);
            ullng   = (UINT64)sllng;
            ullng   = ullng + (_I64_MAX);
            sllng   = (INT64)ullng;
        }
        else
        {// should fit fine
            // standard C spec states that float to int conversion will TRUNCATE the float
            // stevev 27apr11 - stopped following the C spec to conform to the host test.
            // instead of truncating, we now round.
            //sllng  = float128_to_int64_round_to_zero(rawVal);//aka: truncation
            sllng  = float128_to_int64(rawVal);
            retVal = (UINT64) sllng;
        }
    }
#endif

    
    /* stevev 21may09 - we do not limit values in xmtr-dd
        a user often has to input invalid numbers to test error handling in the host */
    if ( devPtr()->getPolicy().isReplyOnly )
        return true;// success
    
    
    
    CValueVarient  hi,lo;
    highest(hi);
    lowest (lo);

    iLoc = (UINT64)retVal;
    hCRangeList localList;
    IntType_t thisType = No_Type;
    if (IsSigned() ) thisType  = Is_Signed;
    if (IsUnsigned()) thisType = Un_Signed;
    resolveRangeList(localList,thisType);  // ...get min max, resolve to list, populate
    if ( localList.size() > 0 )   // should always be..it is populated with default values
    {
        brv = localList.isInRange(retVal);
    }
    else
    if ( (retVal >= hi && !(retVal == hi)) || 
         (retVal <= lo && !(retVal == lo))  )
    {
        brv = false;// out of range
    }
    else
    {
        brv = true;
    }
    if ( brv )  // in range
    {
        /* we do not extend here..we are already in range.
        iRet = limitAndExtend(iLoc);// must occur after range test(sjv4oct07i have no idea why)
        if ( isSigned ) {   retVal = (INT64) iRet; }
        else            {   retVal = iRet;         }
        */
    }// else leave it all alone
    else
        LOGIT(CLOG_LOG,"   Value not in Range List of %s (0x%04x).\n",
                                                                    getName().c_str(),getID());
    return brv;
}


/*********************************************************************************************/

hCUnsigned::hCUnsigned(DevInfcHandle_t h, aCitemBase* paItemBase)
          :hCinteger(h,paItemBase,false)
{//empty - use interger's
}


hCSigned::hCSigned(DevInfcHandle_t h, aCitemBase* paItemBase)
        :hCinteger(h,paItemBase,true)
{//empty - use integer's
}


//RETURNCODE CUnsigned::Read(UINT32 &value):: use hCVar default


//RETURNCODE hCUnsigned::Write(UINT32 value) // use default hCVar version
    
//RETURNCODE hCUnsigned::Write(CValueVarient& newDataValue)
//{
//  UINT32 b  = newDataValue;
//  return (hCinteger::Write(b));
//}
    
RETURNCODE hCSigned::Write(CValueVarient& newDataValue)
{
    UINT32 b = newDataValue;
    return (hCinteger::Write(b));
}

/*********************************************************************************************/
//TimeValue

hCTimeValue::hCTimeValue(DevInfcHandle_t h, aCitemBase* paItemBase):hCUnsigned(h,paItemBase)
{
    aCattrBase* 
    aB = paItemBase->getaAttr(varAttrTimeScale) ; 
    if (aB == NULL) // none in the aClass
    {
        pTimeScale  = NULL; // this is a flag for how formatting works
    }
    else  // generate a hart class from the abstract class
    { 
        pTimeScale  = new hCattrTimeScale(h, (aCattrBitstring*)aB, varAttrTimeScale, this );
        // set const unit?
        // set a scaling factor?
    }
    if (pTimeScale != NULL ) attrLst.push_back(pTimeScale);

    
    aB = paItemBase->getaAttr(varAttrTimeFormat) ; 
    if (aB == NULL) // none in the aClass
    {// default format not generated, assume the default formatting is hard coded.
        pTimeFmt  = NULL; // this is a flag for how formatting works
    }
    else  // generate a hart class from the abstract class
	if (pTimeScale)
	{
		LOGIT(CERR_LOG,"ERROR: TIME_FORMAT is not allowed with TIME_SCALE.\n");
	}
	else
    { 
        pTimeFmt  = new hCattrVarTimeFormat(h, (aCattrString*)aB, this );
        // set const unit?
        // set a scaling factor?
    }
    if (pTimeFmt != NULL ) attrLst.push_back(pTimeFmt);

}

hCTimeValue::hCTimeValue(hCTimeValue* pSrc,  hCTimeValue*  pVal, itemIdentity_t newID):
           hCUnsigned((hCUnsigned*)pSrc,(hCUnsigned*)pVal,newID)
{// shared attributes   
    pTimeScale = pSrc->pTimeScale; 
    pTimeFmt   = pSrc->pTimeFmt;  
};

hCTimeValue::~hCTimeValue()
{// stevev 24oct08 - share attributes 
    if ( !  sharedAttr )
    {
        /*  some attributes are kept on the list too  */
        hCattrBase* pAB ;
        if ( (pAB = getaAttr(varAttrTimeScale)) == pTimeScale ) pTimeScale = NULL; 
        if ( (pAB = getaAttr(varAttrTimeFormat))== pTimeFmt)    pTimeFmt= NULL; 
        RAZE(pTimeScale);//
        RAZE(pTimeFmt);//
    }// else, is shared so let the original owner delete the single copy
}

wstring& hCTimeValue::getDispValueString(void) 
{
    return getValueFormatedString(true);
}
wstring& hCTimeValue::getValueFormatedString(bool bIsDispValue) // J.U. 17.02.11 
//CAUTION-see 'Invensys checkin' in ddbVar.h
{
	const size_t FCHAR_SIZE = 64;
	wchar_t fchar[FCHAR_SIZE];
	double scaledValue;	
	wstring fmtStr, fStr;
	wstring locEx;

    if (pTimeScale) // major section 1
    {
        double sff = GetScalingFactor();// also sets constant_units
        
        if (bIsDispValue)
        {
            scaledValue = ((double)(INT64)Wrt_Val) * sff;
        }
        else
        {
            scaledValue = ((double)(INT64)Value) * sff;

        }

    //      get display format and use that
        if (pDispFmt)
        {   pDispFmt->getFmtStr(locEx);
            fmtStr = (wstring)locEx;   }
        if (fmtStr.empty())
        {
            fmtStr=L"f";
        }

        fStr  = L"%";
        fStr += fmtStr;
        // determine int or double output required
        // if the user embedds one of these, he's SOL
        if ( fStr.find_first_of(L"eEfgG") != std::string::npos )
        {// it's a float
			swprintf(fchar,
#if defined(__GNUC__)
					FCHAR_SIZE,
#endif // __GNUC__
					fStr.c_str(),scaledValue);
		}
		else
		{// it's an int
			swprintf(fchar,
#if defined(__GNUC__)
					FCHAR_SIZE,
#endif // __GNUC__
					fStr.c_str(),(int)scaledValue);
		}
	}
    else // no timescale spec'd - major section 2
    {   
        tm tmTime = {0};

        if (pTimeFmt)//     if time-format
        {   wstring ex;
            pTimeFmt->getFmtStr(ex);
            fmtStr=ex;  }//         get timeformat
        if (fmtStr.empty())
        {
            //    fmtStr=L"T";//        else    use default time format
            // Bill's strftime does not support %T, use the equivelent
            fmtStr=L"H:M:S";
        }
        //if ( fmtStr.find_first_of(L"%") == std::string::npos )
        //{// does not have percents in it
         // recursively fill '%' signs
        addPercent(fmtStr);
        //}
        //      call formatting...
        // time value is limited to an unsigned int
        fillStruct(&tmTime, (UINT32)Wrt_Val);
    /*
        unsigned long  remainSecs,
        totalSecs    = Wrt_Val   / TS_SEC_SCALE_FACTOR;
        int nHours   = totalSecs / TS_SEC_PER_HR;
        remainSecs   = totalSecs % TS_SEC_PER_HR;
        int nMinutes = remainSecs/ TS_SEC_PER_MIN;
        int nSeconds = remainSecs% TS_SEC_PER_MIN;

        tmTime.tm_hour  = nHours;
        tmTime.tm_min   = nMinutes;
        tmTime.tm_sec   = nSeconds;
    */

        try
        {
        
        wcsftime(fchar, 64,fmtStr.c_str(),&tmTime);
    }
        catch(...)
        {
            fchar[0] = 0;   // J.U. if the format is invalid 
        }
    }
    returnString = fchar;
    return returnString; 
}

wstring& hCTimeValue::getTimeValue()
{
    const size_t FCHAR_SIZE = 64;
    wchar_t fchar[FCHAR_SIZE];
    double scaledValue;
    wstring fmtStr;

    if (pTimeScale)
    {
        double sff = GetScalingFactor();
        scaledValue = ((double)(INT64)Wrt_Val) * sff;
        returnString = std::to_wstring(scaledValue);
    }
    else
    {
        tm tmTime = {0};
        fmtStr=L"H:M:S";
        addPercent(fmtStr);
        fillStruct(&tmTime, (UINT32)Wrt_Val);

        try
        {
            wcsftime(fchar, 64,fmtStr.c_str(),&tmTime);
        }
        catch(...)
        {
            fchar[0] = 0;   // J.U. if the format is invalid
        }
        returnString = fchar;
    }
    return returnString;
}

void addPercent(wstring& fmtStr)
{   
    wstring locStr;
    std::string::size_type loc;

    if ( (loc = fmtStr.find_first_of(L"HIMpRST")) != std::string::npos )
    {
        locStr = fmtStr.substr(loc+1); // post found letter
        addPercent(locStr);// add percent to the rest of the string
        if ( loc == 0 )
        {
            fmtStr  = L"%" + fmtStr.substr(loc,1) + locStr;
        }
        else
        if (loc > 0 && fmtStr[loc-1] != L'%')
        {
            fmtStr  = fmtStr.substr(0,loc) + L"%" + fmtStr.substr(loc,1) + locStr;
        }
        else
        {
            fmtStr  = fmtStr.substr(0,loc) + fmtStr.substr(loc,1) + locStr;
        }
    }
    //else we are the end of the string - no more to do
    return;
}

// 20jan12 - stevev, useful routine to support partial time updates
void getActiveValues(wstring& fmtStr,  timeScale_t& returnMask)
{   
    unsigned x = 0, y = (unsigned)returnMask;
    wstring locStr;
    std::string::size_type loc;

    if ( (loc = fmtStr.find_first_of(L"HIMpRST")) != std::string::npos )
    {
        locStr = fmtStr.substr(loc+1); // post found letter
        getActiveValues(locStr, returnMask);// look in the rest of the string
        y = (unsigned)returnMask;
        if ( fmtStr.at(loc) == L'H' /*|| fmtStr.at(loc) == L'I'*/ )
        {           
            x = (unsigned)tsHr_Scale;
        }
        else // Fixed defect #4511, 'I' now has its its own category, POB - 4/21/2014
        if ( fmtStr.at(loc) == L'I' )
        {
            x = (unsigned)ts_I_Scale;
        }
        else
        if ( fmtStr.at(loc) == L'M' )
        {       
            x = (unsigned)tsMinScale;
        }
        else
        if ( fmtStr.at(loc) == L'S' )
        {       
            x = (unsigned)tsSecScale;
        }
        else
        if ( fmtStr.at(loc) == L'p' )
        {       
            x = ts_p_Scale; // Fixed defect #4511, to determine if 'p' is present, POB - 4/21/2014
        }
        else
        {
            LOGIT(CLOG_LOG,
                      L"Warning: Format string %s has an R or T illegally.\n",fmtStr.c_str());
        }
    }
    //else we are the end of the string - no more to do
    y |= x;
    returnMask = (timeScale_t)y;
    return;
}

//integer time-value to structure
void fillStruct(struct tm *pTime, unsigned bgtm)
{
    pTime->tm_isdst = -1;      // unknown if it is or not DST
    pTime->tm_mday  =
    pTime->tm_mon   =
    pTime->tm_year  =
    pTime->tm_wday  =
    pTime->tm_yday  = 0;        // we are not doing date

        unsigned s = bgtm / TS_SEC_SCALE_FACTOR;// total seconds
        unsigned m    = s / TS_SEC_PER_MIN;     // total minutes w/ tm_sec the fractional part
    pTime->tm_sec = s % TS_SEC_PER_MIN;     // seconds remaining
        unsigned h    = m / TS_MIN_PER_HR;      // hours
    pTime->tm_min = m % TS_MIN_PER_HR;      // minutes remaining
    pTime->tm_hour= h % TS_HRS_PER_DAY;     // hours remaining (it can roll over)
    return;
}
//structure to integer time-value
unsigned extractStruct(struct tm *pTime)
{
    unsigned retVal = 0;
    unsigned Min = (pTime->tm_hour * TS_MIN_PER_HR) + pTime->tm_min;
    unsigned Sec = (Min * TS_SEC_PER_MIN) + pTime->tm_sec;
    retVal       = Sec * TS_SEC_SCALE_FACTOR;
    return retVal;
}

RETURNCODE   hCTimeValue::getUnitString(wstring & str)
{   
    getDispValueString();// be sure we calc'd/recalc'd what is needed already
    if (pTimeScale)
    {
        str = constant_units;
    }
    else
    {
        str = TS__NO_UNIT;
    }
    return SUCCESS;
}


int hCTimeValue::getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
                                    wstring* pEditFmt, wstring* pScanFmt, wstring* pDispFmt)
{
    wstring fmt,wrkStr;
    wchar_t R = 0;

    if (pTimeScale) // major section 1
    {// use edit/display formats from integer - except it defaults to float
        return 
            hCNumeric::getFormatInfo(fmtWidth, rightOdec,maxChars,pEditFmt,pScanFmt,pDispFmt);
    }
    else // major section 2
    {// use time formats - note that pTimeFmt is for edit AND display
        wstring ex;
        if (pTimeFmt != NULL && pTimeFmt->getFmtStr(ex) == SUCCESS && ex.length() > 0)
        {
            fmt=ex;  // we already did it all
        }
        else
        {
            //fmt = L"T"; microsoft decided not to be compliant here
            fmt = L"H:M:S";;
        }
        // the following should never happen due to the filters in getFmtStr above.. 27jan11
        if (fmt == wstring(L"%T"))
        {
            fmt=L"H:M:S";           // J.U. 13.09.10 wcsftime does not support %T   
        }
        else if (fmt == wstring(L"%R") )
        {
            fmt = L"H:M";           // J.U. 13.09.10 wcsftime does not support %R format
        }
        //%H, hour as a 24 hr clock (00 to 23)  // 24 hr clock
        //%I, hour as a 12 hr clock (01 to 12)  ****
        //%M, minute as a decimal   (00 to 59)
        //%p, locale senitive AM/PM designation ****
        //%R, macro for '%H:%M'             // 24 hr clock  ****** Not supported by Microsoft  
        //%S, second as a decimal   (00 to 59)
        //%T  macro for '%H:%M:%S'          // 24 hr clock  ****** Not supported by Microsoft
        wrkStr = fmt;

        // stevev 28apr11 - make edit format == display format
        if (pEditFmt)
            (*pEditFmt) = fmt;
        if (pDispFmt)
            (*pDispFmt) =  fmt;

        wstring::size_type idx;
        idx = wrkStr.find_first_of( L"pI" );
        if ( idx == wstring::npos )
        {// failed to find
            R = L'r'; // 24 hour time
        }
        else
        {
            R = L't'; // 12 hr time (w/ am.pm)
        }

        addPercent(wrkStr);
        // stevev 28apr11 - make edit format == display format
        if (pEditFmt)
            (*pEditFmt) = wrkStr;
        if (pDispFmt)
            (*pDispFmt) = wrkStr;

        wchar_t  scratch[80];
        struct tm timeStruct;
        CValueVarient bigtime;
        highest(bigtime);
         
        fillStruct(&timeStruct, bigtime);

        maxChars = wcsftime(scratch, 80, wrkStr.c_str(),&timeStruct);

        return R; // L't';// time format
    }
}

/********
 * 20jan12 - stevev - This function is the ONLY place that a partial time value will be
 *      honored.  ie Partial value updates are required for the DD host test. (%H format
 *      leaves the minutes and hours untouched).  
 *******/
RETURNCODE hCTimeValue::setDisplayValueString(wstring& s)// from UI with range test
{
    CValueVarient aValue;
    RETURNCODE rc = SUCCESS;

    if ( s.size() <= 0 )
    {
        /*
         * There is a blank value...
         * We do not nofity the user of an issue, we simply restore the previous
         * value.  This is now consistent with how the floats and integers are 
         * being handled, POB - 4/18/2014
         */
        aValue.vIsValid = false;
        return rc = APP_PARSE_FAILURE;
    }

    // there are no min/max here to test
    if (pTimeScale) // major section 1
    {// use edit/display formats from integer
        return hCinteger::setDisplayValueString(s);
    }
    else // major section 2
    {// use time formats - note that pTimeFmt is for edit AND display
        wstring fmt;
        wstring ex;
        wstring wsRemaining;
        wchar_t * pRet;

        if (pTimeFmt != NULL && pTimeFmt->getFmtStr(ex) == SUCCESS && ex.length() > 0)
        {
            fmt=ex;  // we already did it all
        }
        else
        {
            fmt = L"H:M:S";// MS doesn't support this ...L"%T";//stevev 27jan10
        }

        unsigned curVal = (UINT32)Value;  // was Wrt_Val;before but roll-overs were cumulative
        unsigned remainingValue;
        struct tm tS, tR;
        memset(&tS,0,sizeof(struct tm));
        memset(&tR,0,sizeof(struct tm));
        addPercent(fmt);

        /*
         * Return wchar_t *
         *  NULL: Did not complete the parsing format, out of range value or bad user 
         *        input string
         *  string: Completed the parsing of the applied format.
         * Capture the results, POB - 4/18/2014
         */
        pRet = strptime(s.c_str(), fmt.c_str(), &tS);// char to time...compiled in UI

        if (pRet)
        {
            /* 
             * We have a success!
             * Parsing has completed on the applied format, POB - 4/18/2014
             */
            wsRemaining = pRet;

            if (!wsRemaining.empty())
            {
                /* 
                 * The string is not empty...
                 * If it was truly a succcess, the string will be empty. 
                 * This likely indicates that there is a problem with the
                 * last time value as since there are left over characters.
                 * We need to indicate a failure, POB - 4/18/2014
                 */
                pRet = NULL;
            }
        }

        if (pRet)
        {
            timeScale_t fmtMsk = tsNo_Scale;
            //ex can be MT:: getActiveValues(ex, fmtMsk);
            getActiveValues(fmt, fmtMsk);//test the same format as we used to extract

            fillStruct( &tR, curVal);
            remainingValue = curVal - extractStruct(&tR);
            
            // was::> locVar = (unsigned)( ((((tS.tm_hour*60)+tS.tm_min)*60)+tS.tm_sec)*32000 );
            if ( fmtMsk & tsHr_Scale )
            {
                tR.tm_hour = tS.tm_hour;
            }
            if( fmtMsk & ts_I_Scale )
            {
                if ( !(fmtMsk & ts_p_Scale) && tR.tm_hour >= 12 ) // Check current value when %p is absent
                {
                    /* 
                     * strptime() will return values 0 - 11 when user enters 12
                     * for %I.  Ensure that PM does not get changed to AM in the
                     * process.  Fixed defect #4511, POB - 4/21/2014
                     */
                    tR.tm_hour = tS.tm_hour + 12;
                }
                else
                {
                    tR.tm_hour = tS.tm_hour;
                }
            }
            if ( fmtMsk & tsMinScale )
            {
                tR.tm_min = tS.tm_min;
            }
            if ( fmtMsk & tsSecScale )
            {
                tR.tm_sec = tS.tm_sec;
            }

            aValue = (unsigned) (remainingValue + extractStruct(&tR));

            // Parsing Success
            setDispValue(aValue);
        }
        else
        {
            // Parsing Failure
            aValue.vIsValid = false;
            rc = APP_OUT_OF_RANGE_ERR;
        }

        return rc;
    }
}
bool hCTimeValue::isInRange(CValueVarient val2Check)
{
    CValueVarient localVarient;
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    if (val2Check.vType == CValueVarient::isWideString) // from a UI
    {
        if (pTimeScale) // major section 1
        {// use edit/display formats from integer
            localVarient = getValueFromStr(val2Check.sWideStringVal);
        }
        else // use time formats
        {// convert and then test for in range
            wstring fmt;
            wstring ex;
            if (pTimeFmt != NULL && pTimeFmt->getFmtStr(ex) == SUCCESS && ex.length() > 0)
            {
                fmt=ex;  // we already did it all
            }
            else
            {
                fmt = L"H:M:S";// MS doesn't support this ...L"%T";//stevev 27jan10
            }
            struct tm tS; memset(&tS,0,sizeof(struct tm));
            addPercent(fmt);
            strptime(val2Check.sWideStringVal.c_str(), fmt.c_str(), &tS);// char to time...
                                                                    //strptime compiled in UI
            localVarient = (unsigned)( ((((tS.tm_hour*60)+tS.tm_min)*60)+tS.tm_sec)*32000 );
        }
        return hCinteger::isInRange(localVarient);
    }
    else
        if (val2Check.vType == CValueVarient::isIntConst ||
            val2Check.vType == CValueVarient::isVeryLong ||
            val2Check.vType == CValueVarient::isFloatConst )
    {
        return hCinteger::isInRange(val2Check);
    }
    else
        return false;// can't convert...must be out of range
}

bool hCTimeValue::VerifyIt(wstring& s)// J.U. 17.03.11< <caution< < <See note @ bottom ddbVar.h
{
    bool bRes = true;
    CValueVarient var; 
    if (pTimeScale) // major section 1
    {// use edit/display formats from integer
        var = hCinteger::getValueFromStr(s);
        if (var.vIsValid == false)
        {
            bRes = false;
        }
    }
    else // major section 2
    {// use time formats - note that pTimeFmt is for edit AND display
        wstring fmt;
        wstring ex;
        if (pTimeFmt != NULL && pTimeFmt->getFmtStr(ex) == SUCCESS && ex.length() > 0)
        {
            fmt=ex;  // we already did it all
        }
        else
        {
            fmt = L"H:M:S";// MS doesn't support this ...L"%T";//stevev 27jan10
        }
        struct tm tS;memset(&tS,0,sizeof(struct tm));
        addPercent(fmt);
        try
        {
            strptime(s.c_str(), fmt.c_str(), &tS);// char to time...compiled in UI
            var = (unsigned)( ((((tS.tm_hour*60)+tS.tm_min)*60)+tS.tm_sec)*32000 );
        }
        catch( ... )
        {
            bRes = false;
        }

    }
    if (bRes == true && !isInRange(var))
    {
        bRes = false;
    }
    return bRes;
}
RETURNCODE hCTimeValue::setDisplayValue(CValueVarient& newValue)// from UI w/ rangetest 
{// stevev 01oct08 - check range of input against size min/max
    if ( hCinteger::isInRange(newValue) )
    {
        setDispValue(newValue);
        return SUCCESS;
    }
    else
        return APP_OUT_OF_RANGE_ERR; 
}

double hCTimeValue::GetScalingFactor()
{
    double retFval = 0.0;       
    hCbitString* pTSv;
    ulong bsVal;

    if (pTimeScale) // major section 1
    {
        pTSv = (hCbitString*) (pTimeScale->procure());
        if (pTSv)
        {
        //  get scale & scale value
            bsVal = pTSv->getBitStr(); 
            if (bsVal & tsHr_Scale)
            {
                retFval = 1.0 / ((double)TS_HOURSCALE_FACTOR);// eg 115200000 or 8.6805555e-9
                constant_units = TS_HOURUNIT;
            }
            else
            if (bsVal & tsMinScale)
            {
                retFval = 1.0 / ((double)TS_MIN_SCALE_FACTOR);
                constant_units = TS_MIN_UNIT;
            }
            else    //all but seconds were coming out 1.0 - fixed,stevev 21jul09
            if (bsVal & tsSecScale)
            {
                retFval = 1.0 / ((double)TS_SEC_SCALE_FACTOR);
                constant_units = TS_SEC_UNIT;
            }
            else
            {
                retFval = 1.0 / ((double)TS__NO_SCALE_FACTOR);
                constant_units = TS__NO_UNIT;
            }
        }
        else
        {// no value - an error occured...
            retFval = ((double)TS__NO_SCALE_FACTOR);
            constant_units = TS__NO_UNIT;
        }
    }
    else // no timescale spec'd - major section 2
    {   
        retFval = 1.0;
    }
    
    return retFval; 
}

RETURNCODE  hCTimeValue::setDefaultValue(void)
{
	CValueVarient vv;
	/* 
		LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix ambiguous assignment 
		compiler error (no long assignment override available). Check if that 
		is what was intended.
	*/
	vv = (int) 0L;	// set to zero 08oct09 - by Wally spec
	return ( hCVar::setDefaultValue(vv) );// from hCVar
}

inline
wstring& hCTimeValue::getEditValueString(void) 
{// stevev 28apr11 - make edit string be the display string
    return getDispValueString();
}

/*********************************************************************************************/

hCFloat::hCFloat(DevInfcHandle_t h) : hCNumeric(h,vT_FloatgPt,FLT_CONST_SIZE)
{
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    // initialize
    if ( policy.isReplyOnly )
    {// we are a device
        f_WrtVal = fValue = f_cacheVal = 0.0f;
        markItemState(IDS_CACHED);
    }
    else
        f_WrtVal = fValue = f_cacheVal = FLT_MIN;
}


hCFloat::hCFloat(DevInfcHandle_t h, aCitemBase* paItemBase) :hCNumeric(h,paItemBase)
{// done at instantiation, setDefaultValue() will be called when conditionals may be resolved
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    // initialize
    if ( policy.isReplyOnly )
    {// we are a device
        f_WrtVal = fValue = f_cacheVal = 0.0f;
        markItemState(IDS_CACHED);
    }
    else
        f_WrtVal = fValue = f_cacheVal = FLT_MIN;
}
hCFloat::hCFloat(hCFloat*  pSrc, hCFloat* pVal, itemIdentity_t newID):
         hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID)
{
    if ( pVal == NULL )
    {
//stevev 20feb08 - this has to be done AFTER the item is registered:       setDefaultValue();
       f_cacheVal=f_WrtVal;
    }
    else
    {
        fValue = (float) pVal->getDispValue();
        f_WrtVal = f_cacheVal = fValue;
    }
}

RETURNCODE  hCFloat::setDefaultValue(void)
{
    CValueVarient  vv;
    vv = 0.0f;
    hCVar::setDefaultValue(vv);
    return SUCCESS;
    
    /* moved to common code in hCVar...8jul05... see previous version for details
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    hCattrVarDefault*  pDVattr = (hCattrVarDefault*) getaAttr(varAttrDefaultValue);
    CValueVarient  vv;
    // ********** assume that the Xmtr .ini value will overwrite DD's Default value  *********
    if ( pDVattr != NULL && pDVattr->getDfltVal(vv) == SUCCESS && vv.vIsValid) 
    {// we have a default
        f_WrtVal = fValue = (float) vv;
        return SUCCESS;
    }
    else // - no DD supplied default value, use min 
    if ( policy.isReplyOnly )
    {// we are a device
        f_WrtVal = fValue = 0.0f;
        markItemState(IDS_CACHED);
    }
    else
        // * 05jul05 - set to HCF spec'd value...f_WrtVal = fValue = FLT_MIN;*x/
        f_WrtVal = fValue = 0.0; // as spec'd 
    return SUCCESS;
    * end moved 8jul05 */
}

bool hCFloat::didMethodChange(void)
{
    return (! AlmostEqual2sComplement(f_WrtVal, f_cacheVal, 4));
}
bool hCFloat::isChanged(void)
{
#ifdef _DEBUG
//  if (f_WrtVal != fValue)
    if (! AlmostEqual2sComplement(f_WrtVal, fValue, 4))
    {
        unsigned ipd = getID();// 4 debugging use
        LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
            " 0x%04x Changed dis %2.2f  dev %2.2f  cah %2.2f %s\n",getID(),
            f_WrtVal, fValue, f_cacheVal,instanceDataStStrings[getDataState()]);
        return true;
    }
    else
    {
        return false;
    }
#else
//  return(f_WrtVal != fValue);
    return ! AlmostEqual2sComplement(f_WrtVal, fValue, 4);
#endif
}

// stevev 18nov05 - moved from h file
void hCFloat::setDispValue(CValueVarient& newValue, bool iHaveMutex)
{
    f_WrtVal = (float)( (double)newValue / GetScalingFactor());
    //if (IsLocal()) fValue = f_WrtVal;

    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( (fValue != f_WrtVal) ) // stevev 02jan07 
                                    // - locals must be updated reguardless of criticality
        {/* needs notification */
            hCmsgList  noteMsgs;// moved 04dec06
            //setDependentsUNK();
            //NUA_t p = IS_changed;
            if (
            ApplyIt()//;// this must be done before notify...
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);//     copies devVar->dispVar
            
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}

void hCFloat::setRealValue(CValueVarient& newValue, bool iHaveMutex)
{
    fValue = (float)newValue;// know what you're doing before you use this
}

void hCFloat::setRawDispValue(CValueVarient& newValue)
{
    f_WrtVal = (float)newValue;
    //if (IsLocal())
    //{
    //  ApplyIt();
    //}
    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( (fValue != f_WrtVal) ) // stevev 02jan07 
                                    // - locals must be updated reguardless of criticality
        {/* needs notification */
            hCmsgList  noteMsgs;
            //setDependentsUNK();
            //NUA_t p = IS_changed;
            if (
            ApplyIt() /* this must be done before notify...*/  
            && ! devPtr()->isInMethod()
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);//     copies devVar->dispVar
            
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}

RETURNCODE hCFloat::setDisplayValue(CValueVarient& newValue) // from UI
{
    if ( isInRange(newValue) )
    {
        setDispValue(newValue);
        return SUCCESS;
    }
    else
        return APP_OUT_OF_RANGE_ERR; 
}


RETURNCODE hCFloat::setDisplayValueString(wstring& s) // from UI
{
    CValueVarient aValue;
    RETURNCODE rc = SUCCESS;

    aValue = getValueFromStr(s);
    if (aValue.vIsValid)
    {
        return setDisplayValue(aValue);
    }
    else
    {
        return APP_PARSE_FAILURE;
    }
/*** old version
//this needs to be fixed to do range check and stuff
//todo
    float fL = 0.0;
    CValueVarient vv;
    if ( s.size() > 0 )
    {
        swscanf(s.c_str(),L"%f",&fL);
    }
    //f_WrtVal = fL / (pScale->getScaleFactor());
    //if (IsLocal()) fValue = f_WrtVal;
    vv = fL;
    setDispValue(vv);
return FAILURE;//todo
*/
}


wstring& hCFloat::getEditValueString(void) 
{   
/* as of spec 500 rev 13 (ballotted), the string to be displayed 
   is exactly the same for display and edit
*xxxx/
  Rev 13 was changed rev m ...on the website... now has a footnote giving the UI types and the
  edit format required....  SDC now gets to change back to the edit format is the edit box
  display format. A bug was added to resolve the spec conflict.
    return getDispValueString() ;
}
***/
    CValueVarient var; 

    var = f_WrtVal;// a double

    int w,p,m;
    wstring fmtStr;
    /*int    getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
      wstring* pEditFmtStr = NULL, wstring* pScanFmtStr = NULL, wstring* pDispFmtStr = NULL);
    **/
    int t = getFormatInfo(w,p,m, &fmtStr, NULL,NULL); // edit format string

    CValueVarient scaled;
    scaled = scaleValue(var, fmtStr);

    returnString = formatValue(scaled, fmtStr);

    return  returnString; 
}

wstring& hCFloat::getDispValueString(void) 
{
    return getValueFormatedString(true);// J.U. 17.02.11
}

//CAUTION-see 'Invensys checkin' in ddbVar.h
wstring& hCFloat::getValueFormatedString(bool isDispValue) // J.U. 17.02.11
{
	const size_t FCHAR_SIZE = 64;
	wchar_t fchar[FCHAR_SIZE];
/* VMKP changed on 190104 */
    wstring fmtStr, fStr;  fStr=L"%";
    if (pDispFmt)
    {   wstring ex;
        pDispFmt->getFmtStr(ex);
        fmtStr  = ex;
        // stevev 28apr08 - I have no idea what is trying to happen in the next 2 lines
        //      but it is messing up normal formatting!! I'm removing it
        //if(fmtStr.find(L"%d")) 
        //  fStr += L"%";
    }
    if ( fmtStr.empty())
    {   fmtStr=L"f";}
/* VMKP changed on 190104 */
	fStr += fmtStr;
	double fv;
	if (isDispValue)
	{
		fv = f_WrtVal * GetScalingFactor();//was::>(pScale->getScaleFactor());
	}
	else
	{
		fv = fValue * GetScalingFactor();
	}
	if ( fStr.find_first_of(L"eEfgG") != std::string::npos )
	{// it's a float
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				fStr.c_str(),fv);
	}
	else
	{// it's an int
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				fStr.c_str(),(int)fv);
	}
	//sprintf(fchar,fStr.c_str(),fv);
	returnString = fchar;; 
#ifdef _DEBUG
    if (returnString.empty() || returnString == L"%f")
    {
        LOGIT(CLOG_LOG,L".");//get a breakpoint
    }
#endif
    return returnString; 
}

DATA_QUALITY_T hCFloat::Read(float &value)  
{    
    CValueVarient myVar;
    DATA_QUALITY_T   rc = hCVar::Read(myVar);// will get from device or cache(this class)

//  value = ((double)myVar) * pScale->getScaleFactor(); //VMKP
    value = (float)((double)myVar); 
    return rc;       // ALWAYS returns a value ---CHECK QUALITY!!!!!!!!!!!
}


DATA_QUALITY_T hCFloat::Read(wstring &value)  {
// *            float v = atof(getSymbolValue(getID()/*item_id*/)); 
//  char val[256];
//  char fmt[256];
    CValueVarient  vVal;
    DATA_QUALITY_T rq      = hCVar::Read(vVal);

    if ( rq != DA_HAVE_DATA && rq != DA_STALE_OK )
    {
        value = L"";        // we are going to have to deal with status at some point
    }
    else
    {
        value = getDispValueString();// will scale it
    }
    return rq;
}

RETURNCODE hCFloat::Write(float value) 
{ 
    RETURNCODE rc = FAILURE;
    if (! IsReadOnly() )  
    {// writes only deal with display values (read back fills real value)
        f_WrtVal = (float)(value / GetScalingFactor());
                        // stevev 19feb07 - merge - casting sequence to aviod roundoff error
        rc = hCVar::Write();// base class 'Write fastest way'
    }
    else 
    {
        rc = DATA_QUALITY_NOT_WRITABLE; 
    }

    return rc;
}

RETURNCODE hCFloat::Write(CValueVarient& newDataValue)
{
    float value = newDataValue;
    return (Write(value));
}

// used by incoming code to set the value from the incoming write string
RETURNCODE hCFloat::Write(wstring value)  
{
    RETURNCODE rc = FAILURE;    
    
    float b = 0.0;

    if (! IsReadOnly() )  
    {   // buf should hold the data
        if ( swscanf(value.c_str(),L"%f", &b) > 0)
        {
            f_WrtVal = (float)(b / GetScalingFactor());
                        // stevev 19feb07 - merge - casting sequence to aviod roundoff error
            // this line was :: fValue = fWrtVal;           
            rc = hCVar::Write();// base class 'Write fastest way'
            rc = SUCCESS;
        }
    }
    else 
    { 
        rc = (-1 /*READ_ONLY*/); 
    }
    return rc;
}


RETURNCODE hCFloat::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
                            methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                            varPtrList_t &hasChangedList, extract_t extFlags)
{
    unsigned   long t; 
    float      V;
    bool       isDifferent;
    /* stevev 3/22/04 - added to handle Error Packets */
    if ( (*byteCount) < VariableSize() )
    {
        LOGIT(CLOG_LOG,"Message too short for Float variable.\n");
    }/* end add */

    RETURNCODE rc = ExtractIt(byteCount, data, (BYTE*) &t, 4);

    t = REVERSE_L( t );
    // stevev 2/25/04       
    if ((extFlags & DoNotApply) == DoNotApply)
    //if (doNotApply || getDataState() != IDS_PENDING)//  stevev 2/25/04
    {
        return SUCCESS;
    }

    bool isInfo = ((extFlags & DoInfoValue) == DoInfoValue);

    devPtr()->aquireItemMutex(
#ifdef _DEBUG
                            __FILE__,__LINE__);
#else
                              );
#endif  
    V = *((float*) &t);
    
    /* Modified By Deepak: due to strange reason this comparison was failing
    for value 1.#Q0, it always results in false. So the next line was added
    which is working correctly*/
//  isDifferent = (fValue != V);
	/* stevev 19may16 - this didn't work very well either...
    isDifferent = ((fValue > V) || (fValue < V));
	---
	added a function that would make any NaN equal any other NaN
	Uses AmostEqual if they are both numbers..  */
	isDifferent = ! isEqual(fValue, V);

	t = 0;// may reuse it as a flag
    if (previousDataState == IDS_INVALID || previousDataState == IDS_UNINITIALIZED)
    {
        isDifferent = true;
    }

    fValue = V;

    // stevev 15dec05 - as per DEEPAK 122404
    markItemState( IDS_CACHED );

    /* stevev 3/2/4 - preclude applying user written variables */
    // pre 28nov05:
    //if (getWriteStatus() == 0 ||(isDifferent && devPtr()->isItemCritical(getID())))
    //post 28nov05 - never execute if no change, 
    if (!isInfo && isDifferent && (getWriteStatus() == 0 || devPtr()->isItemCritical(getID())))
    {// go ahead and apply it

        /* pre 28nov05 ---------------------------------------------
    {// go ahead and apply it
        //  setDependentsUNK();
        //NUA_t p = NO_change; if (isDifferent) p = IS_changed;
        //devPtr()->notifyVarUpdate(symID,p);
        // filter unchanged, get all that this affects...

        notify(noteMsgs);

        ---post 28nov05 --------------------------------------------***/
        if ( getWriteStatus() == 0 )
        {// notify self if not user written
            noteMsgs.insertUnique(getID(), mt_Val, 0);
        }
        // get all that this affects...
//9-9-9 lets do this later....notify(noteMsgs);// fills list from dependents (if there are any)
        hasChangedList.push_back(this);// stevev 9-9-9  
#ifdef _DEBUG
		t = 0xffff;
		if (0x40cb == getID() )
		{
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
            " 0x%04x Pushed on HasChanged dis %2.2f  dev %2.2f  cah %2.2f (%s)\n",getID(),
			f_WrtVal, fValue, f_cacheVal, (isCached)?"MethodCached":"Not MethodCached");
		}
#endif
        /*-- end 28nov05 ---------------------------------------------*/
    }/* end stevev 3/2/4 */	  
#ifdef _DEBUG
	else
	if (0x40cb == getID() )
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
        " 0x%04x NOT Pushed on HasChanged dis %2.2f  dev %2.2f  cah %2.2f (%s)\n",getID(),
		f_WrtVal, fValue, f_cacheVal, (isCached)?"MethodCached":"Not MethodCached");
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
			" 0x%04x  %s,  %s  wrtStat %d %s\n",getID(), (isInfo)?"marked INFO":"Not Info",
		    (isDifferent)?"is Different":"NOT different",  getWriteStatus(), 
			(devPtr()->isItemCritical(getID()))?"is Critical":"NOT critical");
	}
#endif


    if (!isInfo && isDifferent && dominantRelations.size() > 0
        /* stevev 22feb06 */&& devPtr()->getDeviceState() == ds_OnLine)
    {
        rc = MarkDependStale();// Mark the dependent stale
    }
    if ( devPtr()->getPolicy().isReplyOnly)
    {       
#ifdef XMTR
        if ( isDifferent && IsConfig() )
        {       
            RETURNCODE      rc;
            hCitemBase*     pItem = NULL;
            hCVar*          pVar  = NULL;
            CValueVarient   vv;
            unsigned char   iv;
            if (( rc = devPtr()->getItemBySymNumber(DEVICESTATUS_SYMID,&pItem))== SUCCESS
             && (pItem != NULL) )
            {
                pVar = (hCVar*) pItem;
                vv = pVar->getDispValue();
                iv = vv;
                iv |= CONFIG_CHANGED_BIT ;          
                vv = iv;
                pVar->setDispValue(vv);
            }
        }
#endif
        // note that the value must be copied when it's xmtr
        CancelIt();
    }
    // moved earlier - stevev 15dec05  markItemState( IDS_CACHED );
    /* * this should be the ONLY place a non-local's previous should be cleared */
    previousDataState = IDS_CACHED;// stevev 05jul05 - if its a local we'll never be here

    LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"    Ext: 0x%04x marked as cached. (%f)f\n",getID(),fValue);

    devPtr()->returnItemMutex();
	  
#ifdef _DEBUG
	if (0x40cb == getID() && ! t )// t used as a pushed-it flag
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,
        " 0x%04x NOT Pushed on HasChanged dis %2.2f  dev %2.2f  cah %2.2f (%s)\n",getID(),
		f_WrtVal, fValue, f_cacheVal, (isCached)?"MethodCached":"Not MethodCached");
	}
#endif

    return rc;
}

RETURNCODE hCFloat::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{// float to reversed uint32
    unsigned long t = REVERSE_L( *((unsigned long*)(&f_WrtVal)) );
    //return AddIt(byteCount, data, (BYTE*) &fWrtVal, 4);
    if ( ((t & 0x0000007f) == 0x0000007f) && (t & 0xffffff00) != 0 )//a NaN - signalling or Not
    {// force a HART NaN
         t = 0x0000a07f; // a reversed HART NaN
    }
    return AddIt(byteCount, data, (BYTE*) &t, 4);
}

// expects a scaled value 'from the user'
bool hCFloat::isInRange(CValueVarient val2Check)// pretty generic
{ 
    hCRangeList localList;
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    resolveRangeList(localList);  // ...get min max, resolve to list, populate
    if ( localList.size() > 0 )   // should always be..populated with default values
    {
        val2Check = (((double)val2Check) / GetScalingFactor() );
        return (localList.isInRange(val2Check));
    }
    else
    {
        return true;// in lieu of a min max value, it must be in range
    }
}// true @ val2Check inside min-max set 

/* VMKP added on 020104 */
RETURNCODE hCFloat::getMinMaxList(hCRangeList& retList)
{
    RETURNCODE rc = SUCCESS;

    resolveRangeList(retList);  // ...get min max, resolve to list, populate
    return rc;
}
/* VMKP added on 020104 */





/*********************************************************************************************
 *
 * Doubles
 *
 *********************************************************************************************/

hCDouble::hCDouble(DevInfcHandle_t h, aCitemBase* paItemBase):hCNumeric(h,paItemBase)
{
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    // initialize
    if ( policy.isReplyOnly )
    {// we are a device
      d_WrtVal = dValue = 0.00;
      markItemState(IDS_CACHED);
    }
    else
        /*Deepak changed to DBL_MAX as a device was found having DBL_MIN 
        value for PV, Is changed will always be false in such cases*/
      d_WrtVal = dValue = DBL_MAX;
}
hCDouble::hCDouble(hCDouble*   pSrc,  hCDouble*   pVal, itemIdentity_t newID)
         :hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID)
{
    if ( pVal == NULL )
    {
       setDefaultValue();
    }
    else
    {
        dValue = (double) pVal->getDispValue();
        d_WrtVal = dValue;
    }
}

RETURNCODE  hCDouble::setDefaultValue(void)
{
    CValueVarient  vv;
    vv = 0.00f;
    hCVar::setDefaultValue(vv);
    return SUCCESS; 
}

bool hCDouble::didMethodChange(void)
{
    return (! AlmostEqual2sComplement(d_WrtVal, d_cacheVal, 4));
}

bool hCDouble::isChanged(void)
{
#ifdef _DEBUG
//  if (d_WrtVal != dValue)
    if (! AlmostEqual2sComplement(d_WrtVal, dValue, 4))
    {
        return true;
    }
    else
    {
        return false;
    }
#else
//  return(d_WrtVal != dValue);
    return (! AlmostEqual2sComplement(d_WrtVal, dValue, 4));
#endif
}

// stevev 18nov05 - moved from .h file
void hCDouble::setRawDispValue(CValueVarient& newValue)
{
    d_WrtVal = ((double)newValue);
    //if (IsLocal())
    //{
    //  ApplyIt();
    //}
    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( (dValue != d_WrtVal) ) // stevev 02jan07 
                                    // - locals must be updated reguardless of criticality
        {/* needs notification */
            hCmsgList  noteMsgs;// moved 04dec06
            //setDependentsUNK();
            //NUA_t p = IS_changed;
            // stevev 12apr07 - modified 2b like setDispVal() as per Vibhor
            if (
            ApplyIt()//;//Value = Wrt_Val;  
            && ! devPtr()->isInMethod()
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
            //devPtr()->notifyVarUpdate(getID(),p);
            
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now
            }
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
}

RETURNCODE hCDouble::setDisplayValue(CValueVarient& newValue) // from UI w/ range test
{
    if ( isInRange(newValue) )
    {
        setDispValue(newValue);
        return SUCCESS;
    }
    else
        return APP_OUT_OF_RANGE_ERR; 
}

RETURNCODE hCDouble::setDisplayValueString(wstring& s)  // from UI w/ range test
{
    CValueVarient aValue;
    RETURNCODE rc = SUCCESS;

    aValue = getValueFromStr(s);
    if (aValue.vIsValid)
    {
        return setDisplayValue(aValue);
    }
    else
    {
        return APP_PARSE_FAILURE;
    }
/*** old version
//  fix this to do range checks etc & return errors as required
//todo
    double dL = 0.0;
    CValueVarient vv;
    if ( s.size() > 0 )
    {
        swscanf(s.c_str(),L"%f",&dL);
    }
    //d_WrtVal = dL / (pScale->getScaleFactor());
    //if (IsLocal()) dValue = d_WrtVal;
    vv = dL;
    setDispValue(vv);
return FAILURE;//todo
****/
};

DATA_QUALITY_T hCDouble::Read(double &value)
{ 
    CValueVarient    myVar;
    DATA_QUALITY_T   rc = hCVar::Read(myVar);// will get from device or cache(this class)

    //value = ((double)myVar) * pScale->getScaleFactor(); //VMKP
    value = ((double)myVar); //VMKP

    return rc;       // ALWAYS returns a value ---CHECK QUALITY!!!!!!!!!!!
}   //returns the current value

DATA_QUALITY_T hCDouble::Read(wstring &value)
{
    CValueVarient  vVal;
    DATA_QUALITY_T rq      = hCVar::Read(vVal);

    if ( rq != DA_HAVE_DATA && rq != DA_STALE_OK )
    {
        value = L"";        // we are going to have to deal with status at some point
    }
    else
    {
        value = getDispValueString();// will scale it
    }
    return rq;
}

RETURNCODE hCDouble::Write(double value)
{
    RETURNCODE rc = FAILURE;
    if (! IsReadOnly() )  
    {// writes only deal with display values (read back fills real value)
        d_WrtVal = value / GetScalingFactor();//was::>(pScale->getScaleFactor());           
        rc = hCVar::Write();// base class 'Write fastest way'
    }
    else 
    {
        rc = DATA_QUALITY_NOT_WRITABLE; 
    }

    return rc;
} //writes the current value

RETURNCODE hCDouble::Write(CValueVarient& newDataValue)
{
    double value = newDataValue;
    return (Write(value));
}

// used by incoming code to set the value from the incoming write string
RETURNCODE hCDouble::Write(wstring value)  
{
    RETURNCODE rc = FAILURE;    
    
    double b = 0.0;

    if (! IsReadOnly() )  
    {   // buf should hold the data
        if ( swscanf(value.c_str(),L"%lf", &b) > 0)
        {
            d_WrtVal = b / GetScalingFactor();//was::>(pScale->getScaleFactor());
            // this line was :: fValue = fWrtVal;           
            rc = hCVar::Write();// base class 'Write fastest way'
            rc = SUCCESS;
        }
    }
    else 
    { 
        rc = (-1 /*READ_ONLY*/); 
    }
    return rc;
}

void hCDouble::setDispValue(CValueVarient& newValue, bool iHaveMutex)
{
    d_WrtVal = ((double)newValue) / GetScalingFactor();//was::>(pScale->getScaleFactor());
    if ( IsLocal() ) 
    {// go ahead and apply it
        if ( (dValue != d_WrtVal) ) // stevev 02jan07 
                                    // - locals must be updated reguardless of criticality
        {/* needs notification */
            hCmsgList  noteMsgs;// moved 04dec06
                // setDependentsUNK();
                //NUA_t p = IS_changed;
            if (
            ApplyIt()//;//Value = Wrt_Val;
            ){  noteMsgs.insertUnique(getID(), mt_Val, 0);// we forgot to add ourselves
                //devPtr()->notifyVarUpdate(getID(),p);
                // filter unchanged, get all that this affects...
            
            notify(noteMsgs);
            notifyUpdate(noteMsgs); // send 'em now         
            }//12apr07 sjv moved ending to not do notification if not changed 
        }
        else
        {
            ApplyIt();//Value = Wrt_Val;
        }
    }
    //if (IsLocal()) dValue = d_WrtVal;
}
void hCDouble::setRealValue(CValueVarient& newValue, bool iHaveMutex)
{
    dValue = ((double)newValue);// know what you're doing before you use this
}

//CAUTION-see 'Invensys checkin' in ddbVar.h
wstring& hCDouble::getValueFormatedString(bool bIsDispValue)  // J.U. 17.02.11
{	
	const size_t FCHAR_SIZE = 64;
	wchar_t fchar[FCHAR_SIZE];
	wstring fmtStr, fStr;fStr=L"%";
	wstring ex; ex = fStr;
	if (pDispFmt)
	{	pDispFmt->getFmtStr(ex);fmtStr=ex;	}
	else 
	{	fmtStr=L"f";}
	fStr += fmtStr;
	double dv;
	if (bIsDispValue)
	{
		dv = d_WrtVal * GetScalingFactor();//was::>(pScale->getScaleFactor());
	}
	else
	{
		dv = dValue * GetScalingFactor();//was::>(pScale->getScaleFactor());
	}

	if ( fStr.find_first_of(L"eEfgG") != std::string::npos )
	{// it's a float format
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				fStr.c_str(),dv);
	}
	else
	{// it's an int format
		swprintf(fchar,
#if defined(__GNUC__)
				FCHAR_SIZE,
#endif // __GNUC__
				fStr.c_str(),(int)dv);
	}
	// redundant stevev 20sep07....sprintf(fchar,fStr.c_str(),dv);
	returnString = fchar;; 
#ifdef _DEBUG
    if (returnString.empty())
    {
        LOGIT(CLOG_LOG,L".");//get a breakpoint
    }
#endif
    return returnString; 
}
    
wstring& hCDouble::getEditValueString(void) 
{
/* as of spec 500 rev 13 (ballotted), the string to be displayed 
   is exactly the same for display and edit
*xxxx/
  Rev 13 was changed rev m ...on the website... now has a footnote giving the UI types and the
  edit format required....  SDC now gets to change back to the edit format is the edit box
  display format. A bug was added to resolve the spec conflict.
    return getDispValueString() ;
}
***/
    CValueVarient var; 

    var = d_WrtVal;// a double

    int w,p,m;
    wstring fmtStr;
    /*int    getFormatInfo(int& fmtWidth, int& rightOdec, int& maxChars, 
      wstring* pEditFmtStr = NULL, wstring* pScanFmtStr = NULL, wstring* pDispFmtStr = NULL);
    **/
    int t = getFormatInfo(w,p,m, &fmtStr, NULL,NULL); // edit format string

    CValueVarient scaled;
    scaled = scaleValue(var, fmtStr);

    returnString = formatValue(scaled, fmtStr);

    return  returnString; 
}

wstring& hCDouble::getDispValueString(void) 
{   
    return getValueFormatedString(true);
}
/*
    wchar_t fchar[64];
    wstring fmtStr(L"f"), fStr(L"%");
    
    if (pEditFmt)
    {   pEditFmt->getFmtStr(fmtStr);    }
    // 14jul08 - no longer default to display format
    //else if (pDispFmt)
    //{ pDispFmt->getFmtStr(fmtStr);    }
    // else leave it f
    fStr += fmtStr;
    double dv = d_WrtVal * GetScalingFactor();//was::>(pScale->getScaleFactor());
    if ( fStr.find_first_of(L"eEfgG") != std::string::npos )
    {// it's a float format
        swprintf(fchar,fStr.c_str(),dv);
    }
    else
    {// it's an int format
        swprintf(fchar,fStr.c_str(),(int)dv);
    }
    returnString = fchar;; 
    return returnString; 
}**/

RETURNCODE hCDouble::Extract(BYTE *byteCount, BYTE **data, UINT64 mask, int& len,
                            methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
                            varPtrList_t &hasChangedList, extract_t extFlags)
{   // we have to do this the hard way
    double ld,dl;
    bool   isDifferent;
    /* stevev 3/22/04 - added to handle Error Packets */
    if ( (*byteCount) < VariableSize() )
    {
        LOGIT(CLOG_LOG,"Message too short for Double variable.\n");
    }/* end add */
    RETURNCODE rc = ExtractIt(byteCount, data, (BYTE*) &ld, 8);// backwards
    //
    BYTE* src = (BYTE*) &ld;
    BYTE* dst = (BYTE*) &dl; dst+=7;
    for (int i = 0; i < 8; i++)
    {   *(dst--) = *(src++);
    }

    // stevev 2/25/04       
    if ((extFlags & DoNotApply) == DoNotApply)
    //if (doNotApply || getDataState() != IDS_PENDING)//  stevev 2/25/04
    {
        return SUCCESS;
    }

    bool isInfo = ((extFlags & DoInfoValue) == DoInfoValue);

    devPtr()->aquireItemMutex(
#ifdef _DEBUG
                            __FILE__,__LINE__);
#else
                              );
#endif
    isDifferent = ! isEqual(dValue, dl);   
    if (previousDataState == IDS_INVALID || previousDataState == IDS_UNINITIALIZED)
    {
        isDifferent = true;
    }
    dValue = dl;

    // stevev 15dec05 - as per DEEPAK 122404
    markItemState( IDS_CACHED );

    /* stevev 3/2/4 - preclude applying user written variables */   
    // pre 28nov05:
    //if (getWriteStatus() == 0 ||(isDifferent && devPtr()->isItemCritical(getID())))
    //post 28nov05 - never execute if no change, 
    if (!isInfo && isDifferent && (getWriteStatus() == 0 || devPtr()->isItemCritical(getID())))
    {// go ahead and apply it
        /* pre 28nov05 --------------------------------------------
        {// go ahead and apply it
            //setDependentsUNK();
            //NUA_t p = NO_change; if (isDifferent) p = IS_changed;
        //devPtr()->notifyVarUpdate(getID(),p);

        notify(noteMsgs);

        }
        ---post 28nov05 --------------------------------------------***/
        if ( getWriteStatus() == 0 )
        {// notify self if not user written
            noteMsgs.insertUnique(getID(), mt_Val, 0);
        }
        // get all that this affects...
//9-9-9 lets do this later....notify(noteMsgs);// fills list from dependents (if there are any)
        hasChangedList.push_back(this);// stevev 9-9-9
        /*-- end 28nov05 ---------------------------------------------*/

    }/* end stevev 3/2/4 */
    if (!isInfo && isDifferent && dominantRelations.size() > 0
        /* stevev 22feb06 */&& devPtr()->getDeviceState() == ds_OnLine)
    {
        rc = MarkDependStale();// Mark the dependent stale
    }
    if (devPtr()->getPolicy().isReplyOnly)
    {
#ifdef XMTR
        if ( isDifferent && IsConfig() )
        {       
            RETURNCODE      rc;
            hCitemBase*     pItem = NULL;
            hCVar*          pVar  = NULL;
            CValueVarient   vv;
            unsigned char   iv;
            if (( rc = devPtr()->getItemBySymNumber(DEVICESTATUS_SYMID,&pItem))== SUCCESS
             && (pItem != NULL) )
            {
                pVar = (hCVar*) pItem;
                vv = pVar->getDispValue();
                iv = vv;
                iv |= CONFIG_CHANGED_BIT ;          
                vv = iv;
                pVar->setDispValue(vv);
            }
        }
#endif
        // note that the value must be copied when it's xmtr
        CancelIt();
    }
    // moved earlier - stevev 15dec05  markItemState( IDS_CACHED );
    /* * this should be the ONLY place a non-local's previous should be cleared */
    previousDataState = IDS_CACHED;// stevev 05jul05 - if its a local we'll never be here

    LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"    Ext: 0x%04x marked as cached. (%f)d\n",getID(),dValue);

    devPtr()->returnItemMutex();
    return rc;
}


RETURNCODE hCDouble::Add(BYTE *byteCount, BYTE **data, UINT64 mask, int& len)
{       // we have to do this the hard way
    double ld;
    //
    BYTE* src = (BYTE*) &d_WrtVal;  src+=7;
    BYTE* dst = (BYTE*) &ld; 
    for (int i = 0; i < 8; i++)
    {   *(dst++) = *(src--);
    }   
    return AddIt(byteCount, data, (BYTE*) &ld, 8); 
}


bool hCDouble::isInRange(CValueVarient val2Check)// pretty generic
{ 
    hCRangeList localList;
    hSdispatchPolicy_t policy = devPtr()->getPolicy();
    if (policy.isReplyOnly)
    {
        return true;
    }
    resolveRangeList(localList);  // ...get min max, resolve to list, populate
    if ( localList.size() > 0 )   // should always be..populated with default values
    {
        val2Check = (((double)val2Check) / GetScalingFactor() );
        return (localList.isInRange(val2Check));
    }
    else
    {
        return true;// in lieu of a min max value, it must be in range
    }
} // true @ val2Check inside min-max set 


RETURNCODE hCDouble::getMinMaxList(hCRangeList& retList)
{
    RETURNCODE rc = SUCCESS;

    resolveRangeList(retList);  // ...get min max, resolve to list, populate
    return rc;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  parseFormat
 *  takes a format string and parses out the essential pieces according to the C spec
 *
 *  returns a string with appropriate changes like a '%' prepended and/or an I64 inserted
 *          passed-in references are modified to reflect the contents of the format string
 *
 *  NOTE: this algorithm makes the assumption that the item instantiation generated the proper
 *      default format string when the DD did not supply one.  Therefore no default values are
 *      filled in here (from tables 3 & 4 in spec 500).
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
wstring    hCNumeric::parseFormat(wstring fmtStr,
    /*outputs::*/           wstring& flgs, int& wid, int& prec, wstring& lm, wchar_t& type)
{
    wchar_t numeric[34]={0};
    int y, z, strt = 0;

    wstring fIStr(fmtStr);
    if ( fmtStr.length() <= 0 )
    {
        DEBUGLOG(CERR_LOG,"ERROR: process format called with an empty format string.\n");
        fIStr = L"";
        return fIStr;
    }
    variableType_t  vt = (variableType_t)VariableType();

    // deal with percent sign..if it was included
    while (1) // use break and continue
    {
        strt = fmtStr.find('%');
        if (strt == wstring::npos)// no %
        {
            if (fmtStr.length() > 0)
            {
                fmtStr = L'%' + fmtStr;
                strt = 1;
                break;// we're done here
            }
            else // the entire string was illegal(never a % in an empty string)
            {
                fIStr.clear();
                return fIStr;// not much to do
            }
        }
        else // there is a %
        {
            if (strt > 0)// past leading stuff before '%'
            {
                fmtStr = fmtStr.substr(strt);// % to end
                strt = 1;// point to location after %
            //  retLoc -= (fmt.length() - wrkStr.length());// compensate 
            }
            else // strt == 0...starts with a '%'
            {
                strt = 1;
            }
            if (fmtStr[strt] == L'%')// embedded %%...strings aren't allowed here
            {
                strt++;// get past second percent
                fmtStr = fmtStr.substr(strt);// past second % to end
                // loop to see if we have a real percent
                strt = 0;
            }
            else
            {
                break; // we have what we need
            }
        }
    }//wend
    // we should have a format string with a prepended % and strt pointing behind it

    wstring wrkStr(fmtStr);// get a working copy
    
    bool isInt = false;
    int dLoc, fLoc, useLoc;
    int XPos = -1;
    
    // Check string for upper case 'X'
    // Fixed defect #4505 and #4506, POB - 4/24/2014
    dLoc = wrkStr.find(L"X");
    if (dLoc != wstring::npos)
    {
        // We found an upper case 'X' in the format
        XPos = dLoc;
    }
	transform(wrkStr.begin(),wrkStr.end(),wrkStr.begin(),
#if defined(__GNUC__)
			::towlower
#else
			tolower
#endif // __GNUC__
			);// standard format


    dLoc = wrkStr.find_last_of(L"dioux");// last instance of any integer type
    fLoc = wrkStr.find_last_of(L"efg"); // last instance of any floating type<a not allowed>
    if (dLoc == wstring::npos && fLoc == wstring::npos)
    {
        DEBUGLOG(CLOG_LOG,"ERROR:  trying to parse a format with no legal type conversion.\n");
        fIStr = L"";
        return fIStr;// error return <no legal formatting characters>
    }
    else
    if (fLoc == wstring::npos)// it's a integer type character only
    {
        isInt  = true;
        useLoc = dLoc;
        
        // Fixed defect #4505 and #4506, POB - 4/24/2014
        if (XPos > 0)
        {
            // Restore the upper case 'X' in the format
            wrkStr.replace(XPos, 1, L"X");
        }

        type= (wchar_t)wrkStr[dLoc];// tolower should never change the length
    }
    else// has to be float or both float and integer (defaults to float)
    {
        isInt  = false;
        useLoc = fLoc;
        type= (wchar_t)wrkStr[fLoc];// tolower should never change the length
    }

    wstring flagList(L" +-0#");// all the legal flag characters
    wstring lenModLst(L"hljzt");
    prec = -1;// default value
    wid  =  0;// ditto
    flgs.erase();// no flags
    lm.erase();  // no length modifier
    // scan from left to right, filling in the piece-parts
    int state = 1;// flags (skip percent sign)
    z = y = 0;
    while ( strt <= useLoc )
    {
        if ( state == 1 && strt == useLoc ) 
        {
            break; // possible flags & type only
        }

        if (state == 1)// we are looking for flags
        {
            int loc = flagList.find(wrkStr[strt]);
            if (loc != wstring::npos)// we found a flag
            {
                flgs = flgs + wrkStr[strt++];// keep looking
            }
            else // not flags
            {
                state = 2;// then fall thru to deal with it
            }
        }

        if ( state == 2 && strt == useLoc ) // looking for width
        {
            if (z > 0)
            {
                wid  = _wtoi(numeric);
                numeric[0] = '\0';
                z = 0;
            }// else no width
            break;// no more to do
        }
        
        if ( state == 2 && strt < useLoc ) // looking for width
        {
            if (iswdigit(wrkStr[strt]) )
            {
                numeric[z++] = wrkStr[strt++];
                numeric[z] = '\0';
            }
            else// can't be width anymore
            if (wrkStr[strt] == L'.')
            {
                state = 3;// precision
                strt++;
            }
            else
            {// leave prec default -1
                state = 4;// length modifier, fall thru
            }
            if (state > 2 && z > 0)
            {
                wid  = _wtoi(numeric);
                numeric[0] = '\0';
                z = 0;
            }// else it's still getting numbers or there is no width
        }

        if ( state == 3 && strt == useLoc ) // looking for precision
        {
            if (z > 0)
            {
                prec  = _wtoi(numeric);
                numeric[0] = '\0';
                z = 0;
            }// else there is no precision
            break;// nothing else to do
        }
        
        if ( state == 3 && strt < useLoc ) // looking for precision
        {
            if (iswdigit(wrkStr[strt]) )
            {
                numeric[z++] = wrkStr[strt++];
                numeric[z] = '\0';
            }
            else
            {
                state = 4;// length modifier or type
            }
            if (state > 3 && z > 0)
            {
                prec  = _wtoi(numeric);
                numeric[0] = '\0';
                z = 0;
            }// else it's still getting numbers or there is no precision
        }
        if ( state == 4 && strt == useLoc) // looking for length modifiers - not legal
        {
            break;// we're done
        }

        if ( state == 4 && strt < useLoc) // looking for length modifiers - not legal
        {// from here to useLoc (the type value) all illegal
            // illegal, discard
            wchar_t loc = wrkStr[strt];
            // note that %c in a wide version printf is supposed to represent a wchar_t
            LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,L"ILLEGAL FORMAT STRING: removing '%c' from %s.\n",
                                                                        loc, fmtStr);
            wstring tmps = wrkStr.substr(strt+1);
            wrkStr = wrkStr.substr(0,strt);
            wrkStr = wrkStr + tmps;// character removed
            // do the actual return value too
            tmps  = fIStr.substr(strt+1);
            fIStr = fIStr.substr(0,strt);
            fIStr = fIStr + tmps;// character removed
            useLoc--;// it just moved left one...strt stays the same but its on the next char
        }

    }//wend


    if ( ( vt == vT_Integer    || vt == vT_Unsigned || vt == vT_Index ||
           vt == vT_Enumerated || vt == vT_BitEnumerated )//dloux
           // it's an int variable and NOT a float format
       && !(type == L'f' || type == L'e' || type == L'g')  )
    {
    // we are going to assume there is nothing but the format in the string -stevev 19jun08
        fIStr = fIStr.substr(0,fIStr.size()-1);// put everything but type into main string
        fIStr += L"";                            // say we're passing in a long long
        fIStr += type;          // tack the type back on the end   
        lm     =  L"";
    }
    else
    {
        lm.erase();
    }

    return fIStr;
}


/* modified to allow exponents stevev 9may14 check in*/
RETURNCODE  hCNumeric::validateTrim(wstring & inputStr)
{
    wstring locStr;
    std::string::size_type loc;
    bool isNegative = false;
    int vt = VariableType();

    // Capture a copy of the user input string
    locStr = inputStr;

    // Check to determine if empty string

    if ( locStr.size() <= 0 )
    {
        /*
         * Nothing has been entered.
         * This is a parsing error!
         */
        return FAILURE;
    }

    // Currently, we are only validating on Integers here
    // Floats and Doubles will be handled differently
    // in hCNumeric::getValueFromStr().
    if (  vt == vT_Unsigned || vt == vT_Integer || vt == vT_Index )
    {
	    /*
	     * Trim any leading spaces
	     *
	     * NOTE:  This code may never be used if the user interface
	     * has already performed a trim left on the input string, but
	     * better be certain it the spaces are removed.
	     */
	    if ( (loc = locStr.find_first_not_of(L" ")) != std::string::npos )
	    {
	        // We found the position of the first non-space value
	        if (loc)  // loc = 0 means there are no "leading" spaces
	        {
	            locStr = locStr.substr(loc); // update the input string without the leading zeros            
	        }
	    }
	
	    // Check to see if there is a hexadecimal sign in the value
	    if ( (loc = locStr.find_first_of(L"xX")) != std::string::npos )
	    {
	        /* 
	         * We found a hexadecimal sign!
	         */
	
	        //update the locStr without the hexadecimal sign
	        locStr = locStr.substr(loc+1);
	    }
	
	    // Check to see if there is a negative character in the value
	    if ( (loc = locStr.find_first_of(L"-")) != std::string::npos )
	    {
	        /* 
	         * We found a negative character!
	         */
	
	              
	        // Check to determine if empty string
	        if ( locStr.size() <= 0 )
	        {
	            /*
	             * There is no value after the negative character.
	             * This is a parsing error!
	             */
	            return FAILURE;
	        }
	
	       
	        isNegative = true;
	    }
	
	    // Trim any leading zeros ( and after any minus sign)
	    if ( (loc = locStr.find_first_not_of(L"0")) != std::string::npos )
	    {
	        // We found the position of the first non-zero value
	        if (loc)  // loc = 0 means there are no "leading" zeros
	        {
	            locStr = locStr.substr(loc); // update the input string without the leading zeros
	        }
	    }
	
	    // Any change to the locStr must be assigned back to the inputStr
	    inputStr = locStr;
    }

    return SUCCESS;
}
