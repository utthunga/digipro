/*************************************************************************************************
 *
 * $Workfile: ddbCollAndArr.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		More item classes
 *		8/29/02	sjv	created from ddbEditDisplay.h
 *
 *		
 * #include "ddbCollAndArr.h".
 */

#ifndef _DDB_COLLANDARR_H
#define _DDB_COLLANDARR_H

#include "ddbGeneral.h"
//#include "foundation.h"
#include "ddbPrimatives.h"
//#include "ddbConditional.h"


//#include "ddbdefs.h"
#include "ddbItemBase.h"

#include "ddbAttributes.h"
#include "ddbGrpItmDesc.h"

class hCattrBase;

#if 0 /* Use group item stuff */
/* old reference array element handling code...its genericized to group item stuff...
   old code deleted 22jan09 stevev (see earlier versions to look at the old code **/
#endif /* use group item stuff */
/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ITEM ARRAY CLASS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class hCitemArray: public hCgrpItmBasedClass /* itemType 9 ITEM_ARRAY_ITYPE */
{
public:
	hCitemArray(DevInfcHandle_t h, aCitemBase* paItemBase)
		: hCgrpItmBasedClass(h, paItemBase)                     {};
	hCitemArray(hCitemArray*    pSrc,  hCitemArray*    pVal, itemIdentity_t newID)   /* from-typedef */
		: hCgrpItmBasedClass(pSrc, pVal, newID)                        {};
	virtual ~hCitemArray(){

/*
    LINUX_PORT - REVIEW_WITH_HCF: hCgrpItmBasedClass has a virtual 
    destructor that is automatically called on hCitemArray destruction.
    Calling it again leads to double deletes, resulting in object 
    destruction issues and aborts.
*/
#if !defined(__GNUC__)
		hCgrpItmBasedClass::~hCgrpItmBasedClass();
#endif // __GNUC__
	};

	/* all functionallity is in the base class */
};
#if 0
/* old reference array element handling code...its genericized to group item stuff...
   old code deleted 22jan09 stevev (see earlier versions to look at the old code **/
#endif /* don't compile the old item array class */

/*====================================== Collections ===========================================*/
#if 0 /* don't compile the old collection stuff */
/* old collection member handling code...its genericized to group item stuff...
   old code deleted 22jan09 stevev (see earlier versions to look at the old code **/
#endif /* don't compile the old collection stuff */

/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx COLLECTION CLASS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class hCcollection: public hCgrpItmBasedClass /* itemType 10 COLLECTION_ITYPE */
{
public:	
	bool is_on_menu;// stevev 27sep06 - true if used as a menu-item (makes it a container)

public:
	hCcollection(DevInfcHandle_t h, aCitemBase* paItemBase)
		: hCgrpItmBasedClass(h, paItemBase),is_on_menu(false)                           {};
	hCcollection(hCcollection*    pSrc,  hCcollection*    pVal, itemIdentity_t newID)	/* from-typedef */
		: hCgrpItmBasedClass((hCgrpItmBasedClass*)pSrc, pVal, newID),is_on_menu(false)  {};
	virtual ~hCcollection(){
/*
    LINUX_PORT - REVIEW_WITH_HCF: hCgrpItmBasedClass has a virtual 
    destructor that is automatically called on hCcollection destruction.
    Calling it again leads to double deletes, resulting in object 
    destruction issues and aborts.
*/
#if !defined(__GNUC__)
		hCgrpItmBasedClass::~hCgrpItmBasedClass();
#endif // __GNUC__
	};

	/* all functionallity is in the base class */
	/* except for a collection-as-menu specific function */
	void  fillContainList(ddbItemList_t&  iContain);
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ){returnedSize.clear();
						LOGIT(CERR_LOG,"Collection setSize is required!\n"); return 1;};
						
	/* aquireTree returns bool hasChangedSize  since the last aquire tree sjv 230ct06 **/
	bool       aquireTree(hCitemBase* pB, hCmenuTree* thisMenusDesc,  
						  const menuHist_t& history = menuHist_t(), bool thisLevelOnly=false  );
};



#if 0 /* don't compile the old collection stuff */
/* old collection member handling code...its genericized to group item stuff...
   old code deleted 22jan09 stevev (see earlier versions to look at the old code **/
#endif /* don't compile the old collection class */

//class hCfile: public hCgrpItmBasedClass /* itemType 20 FILE_ITYPE */
//{
//public:
//	hCfile(DevInfcHandle_t h, aCitemBase* paItemBase)
//		: hCgrpItmBasedClass(h, paItemBase)                     {};
//	virtual ~hCfile(){ hCgrpItmBasedClass::~hCgrpItmBasedClass(); };
//
//	/* all functionallity is in the base class */
//};

#endif //_DDB_COLLANDARR_H

/*************************************************************************************************
 *
 *   $History: ddbCollAndArr.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
