/**********************************************************************************************
 *
 * $Workfile: ddbCommand.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the h command class
 *		08/07/02	sjv	created from itemCommand.cpp
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */

#include "ddbItems.h"
//#include "itemCommand.h"
#include "ddbVarList.h"

#ifdef _DEBUG
#define STARTTIME_CHECK /* also in ddbDevice.cpp */
//#include <afx.h> /* for memory tests */
// temp stevev 05jun06:  #include "..\..\sdc625\stdafx.h"
#endif

#ifdef STARTTIME_CHECK
extern DWORD startOfInstantiation, lastTickCnt, thisCnt;
#endif

#define  VALIDY_RULE_LENIENT  lenient
/**********************************************************************************************
 * The transaction itself 
 * 
 **********************************************************************************************
 */
hCtransaction::hCtransaction(DevInfcHandle_t h, hCcommand* pCMD)
			 : hCtransBase(h),request(h),response(h),respCodes(h),pDispatch(NULL),pCmd(pCMD)
			 , RcvActList(h)
			   /*,resReqstStruct(h),  resReplyStruct(h)*/
{	    };

hCtransaction::hCtransaction(const     hCtransaction& src)
			 : hCtransBase(src),       request(src.request), 
			   response(src.response), respCodes(src.respCodes)
			 , RcvActList(src.RcvActList)
{ // request and response are already done in copy constructors above
  // so we can't use operator=()... just copy the rest of the data
	//operator=(src); 
	
	transNum   = src.transNum;
	pDispatch  = src.pDispatch;
	pCmd       = src.pCmd;
	indexMap   = src.indexMap;
}


hCtransaction::~hCtransaction()
{
	destroy();
}

RETURNCODE hCtransaction::destroy(void)
{	
	RETURNCODE rc = 0;
	if (request.genericlist.size() > 0 )
	{
		rc  |= request.destroy();
	}
	if (response.genericlist.size() > 0)
	{
		rc  |= response.destroy();
	}
	if (respCodes.genericlist.size() > 0)
	{
		rc  |= respCodes.destroy();
	}
//	if (RcvActList.size() > 0 )
//	{
		rc  |= RcvActList.destroy();
//	}
	indexMap.clear();

	transNum = -1;
	pDispatch= NULL;
	return rc;
}


hCtransaction& hCtransaction::operator=(const hCtransaction& src)
{
	//DEEPAk 310105
	request.clear ();
	response.clear ();
	respCodes.clear ();
	//END
	transNum   = src.transNum;
	pDispatch  = src.pDispatch;
	request    = src.request;
	response   = src.response;
	respCodes  = src.respCodes;
	pCmd       = src.pCmd;
	RcvActList = src.RcvActList;
	indexMap   = src.indexMap;
	return (*this);
}


RETURNCODE hCtransaction::setEqualTo( aCtransaction* pAtransaction )
{
	RETURNCODE rc = SUCCESS;
	
	transNum   = pAtransaction->transNum;
	//int cnt = pSrc->genericlist.size();
	if ( pAtransaction->request.genericlist.size() > 0 )
	{
		request.  setEqualTo( &(pAtransaction->request) );
	}// else it's a command-command

	response. setEqualTo( &(pAtransaction->response));

	if ( pAtransaction->respCodes.genericlist.size() > 0 )
	{		
		respCodes.setEqualTo( &(pAtransaction->respCodes)); 
	}// else - no-op ... these are not required
	//fpWgtCalcs = src.fpWgtCalcs;

	if ( devPtr()->getPolicy().isReplyOnly && 
		 pAtransaction->postRqstRcvAction.size() > 0 )
	{// policy test done here so we don't have to evry time - list'll be empty
	 // not cond	condPostRcvActList.
	 //		setEqualTo((AcondReferenceList_t*) &(aCa->postRqstRcvAction))
		RcvActList.setEqualTo(&(pAtransaction->postRqstRcvAction)) ;
		DEBUGLOG(CLOG_LOG,"Command %d has a Post-Receive Action(s)\n",pCmd->getCmdNumber());
	}

	return rc;
}


// uses the resolved data item list to fill the resolved transaction structure
// common for request/reply
RETURNCODE hCtransaction::setResolvedStruct(resolvedTrans_t* pResolvedStruct)
{
	RETURNCODE rc = SUCCESS;
	cmdDataItemType_t dit;
	hCitemBase*       pItem = NULL;
	itemID_t  iItem = 0;
	int iSize = 0;
	if ( pResolvedStruct == NULL ) {	return FAILURE;   }


	pResolvedStruct->Number            = transNum;
	// now the source....pResolvedStruct->RslvdDataItemList = *pRDIList;
	/* sjv 3/23/04...pResolvedStruct->firstReqByte = -1;// default to resolve the elses below
	if (( iSize = pResolvedStruct->RslvdDataItemList.size()) > 0)
	{
		if (pResolvedStruct->RslvdDataItemList.begin()->type == cmdDataConst )
		{
			pResolvedStruct->firstReqByte = pResolvedStruct->RslvdDataItemList.begin()->iconst;
		}
		//else - leave default
	}
	//else - leave default
	end sjv 3/23/04 */
	pResolvedStruct->constCnt = 0;	
	pResolvedStruct->size     = 0;// start with no message size
	UINT64  maxMask = 0;		// stevev 26jan09 - reduce size when masks are used
	// scan the resolved list for size 
	iTdataItmLst iT;
	for ( iT = pResolvedStruct->RslvdDataItemList.begin();
		  iT < pResolvedStruct->RslvdDataItemList.end();   iT++)
	{// iT is a ptr 2 a hCdataItem
		dit = (cmdDataItemType_t)iT->type;
		if (dit == cmdDataConst)
		{
			pResolvedStruct->size += INT_CONST_SIZE;
			pResolvedStruct->constCnt++;
		}
		else
		if (dit == cmdDataFloat)
		{
			pResolvedStruct->size += FLT_CONST_SIZE;
			// we are not going to compare float const for transaction match 3/23/04
			// pResolvedStruct->constCnt++;
		}
		else
		if (dit == cmdDataReference)
		{
			if ( (rc = iT->ref.resolveID(iItem, false))  == SUCCESS )
			{
				pResolvedStruct->dataNumList.push_back(iItem);
				if ( ( rc = devPtr()->getItemBySymNumber(iItem, &pItem))  == SUCCESS 
					&& pItem != NULL )
				{// stevev 26jan09 - deal with byte size when masked variables
					if (iT->width != 0)
					{
						maxMask |= iT->width;
					}
					else // no mask
					if ( maxMask != 0 )// used to be a mask then count it
					{// not our job to verify the mask is correct, just how big it is
						while ( maxMask != 0 )// at least 1 size
						{
							pResolvedStruct->size ++;
							maxMask >>= 8;
						}// comes out zero, ready for another mask if used
					}
					else// no mask now, didn't use to have a mask
					{
						pResolvedStruct->size += pItem->getSize();
					}
					pResolvedStruct->dataPtrList.push_back(pItem);
					rc = SUCCESS;
				}
				else
				{
					LOGIT(CERR_LOG,
								L"ERROR: reference could not get item number 0x%04x\n", iItem);
					rc = APP_RESOLUTION_ERROR;
				}
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)
							(CERR_LOG,L"ERROR: could not resolve reference in dataitem.(1)\n");
				// let it finish the for loop
			}
		}
		else
		{
			LOGIT(CERR_LOG, L"ERROR: transaction %d has an unknown dataitem type. (%d)\n",
							pResolvedStruct->Number,(int)dit);
			// let it finish the for loop
		}
	}// next
	// stevev 26jan09 if there is mask left, then the last item was masked, deal with it here
	if ( maxMask != 0 )// used to be a mask then count it
	{// not our job to verify the mask is correct, just how big it is
		while ( maxMask != 0 )// at least 1 size
		{
			pResolvedStruct->size ++;
			maxMask >>= 8;
		}// comes out zero, ready for another mask if used
	}

	// done - return
	return rc;
}

// generic code for the others
// resolves either request or reply - send in a ptr if you want that info back
//									- otherwise you only get the return struct
hCtransaction::transInfo_t hCtransaction::
									getTransInfo(bool isRequest, resolvedTrans_t* pResolvedRet)
{	
	RETURNCODE        rc = SUCCESS;
	transInfo_t       returnInfoStruct;
//	cmdDataItemType_t dit;
	hCitemBase*       pItem = NULL;
	itemID_t          iItem = 0;
	//hCdataitemList*   pResolvedDataItemList;//RslvdDataItemList
	bool IownResolved = false;// do not delete on exit

	if ( pResolvedRet == NULL )
	{
		pResolvedRet = new resolvedTrans_t(devHndl());
		IownResolved = true;
	}
	//else	// owned elsewhere...just fill it
	pResolvedRet->clear();

	if (isRequest)
	{
		rc =  request.resolveCond(&(pResolvedRet->RslvdDataItemList));
	}
	else
	{
		rc = response.resolveCond(&(pResolvedRet->RslvdDataItemList));
	}
	
	if ( rc != SUCCESS )
	{
		LOGIT(CERR_LOG,
					  L"ERROR: getTransInfo could not resolve the conditional list.(%d)\n",rc);
		returnInfoStruct.clear();
	}
	else
	{// resolved
		// new API
		rc = setResolvedStruct(pResolvedRet);// fills in the rest of the structure
		if ( rc != SUCCESS )
		{
			LOGIT(CERR_LOG,"ERROR: getTransInfo could not resolve the list.\n");
			returnInfoStruct.clear();
		}
		else
		{// use res-struct as it stands or has been modified
			returnInfoStruct = *pResolvedRet;// pulls out the relevant data (operator=)
		}
	}//endelse it is resolved

	if (IownResolved)
	{
		delete pResolvedRet;
		pResolvedRet = NULL;
	}// else owner deletes
	return returnInfoStruct;
}

hCtransaction::transInfo_t hCtransaction::getTransREQInfo(resolvedTrans_t* pRetResolved)
{
	return (getTransInfo(true,pRetResolved));// resolves &request));
}
hCtransaction::transInfo_t hCtransaction::getTransREPInfo(resolvedTrans_t* pRetResolved)
{
	return (getTransInfo(false,pRetResolved));// resolves &response));
}


RETURNCODE  hCtransaction::getReqstList(varPtrList_t& itemList)
{
	RETURNCODE rc = SUCCESS;
	hCVar*     pVar = NULL;

	resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());

	hCtransaction::transInfo_t ti = getTransInfo(true, pTransLst);
	
	itemList.clear();
	if ( ti.Number < 0 ) // failure
	{
		rc =  FAILURE;
	}
	else
	{	// copy with cast
		for (ddbItemList_t::iterator iT = pTransLst->dataPtrList.begin(); 
									 iT < pTransLst->dataPtrList.end();   iT++)
		{// iT is ptr 2 ptr 2 hCitemBase
			pVar = (hCVar*) (*iT);
			itemList.push_back(pVar);
		}
	}
	delete pTransLst;
	return rc;
}
RETURNCODE  hCtransaction::getReqstList(itemIDlist_t&      itemList)
{
	RETURNCODE rc = SUCCESS;
	hCVar*     pVar = NULL;
	resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	hCtransaction::transInfo_t ti = getTransInfo(true, pTransLst);
	
	itemList.clear();
	if ( ti.Number < 0 ) // failure
	{
		rc =  FAILURE;
	}
	else
	{// else get the info
	// copy 
		itemList = pTransLst->dataNumList;
	}

	delete pTransLst;
	return rc;
}

RETURNCODE  hCtransaction::getReplyList(varPtrList_t& itemList)
{
	RETURNCODE rc   = SUCCESS;
	hCVar*     pVar = NULL;
	resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	hCtransaction::transInfo_t ti = getTransInfo(false, pTransLst);
	
	itemList.clear();
	if ( ti.Number < 0 ) // failure
	{
		rc =  FAILURE;
	}
	else
	{// else get the info
		// copy and cast
		for (ddbItemList_t::iterator iT = pTransLst->dataPtrList.begin(); 
									 iT < pTransLst->dataPtrList.end();   iT++)
		{// iT is ptr 2 ptr 2 hCitemBase
			pVar = (hCVar*) (*iT);
			itemList.push_back(pVar);
		}
	}
	delete pTransLst;
	return rc;
}
RETURNCODE  hCtransaction::getReplyList(itemIDlist_t&      itemList)
{
	RETURNCODE rc = SUCCESS;
	hCVar*     pVar = NULL;
	resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	hCtransaction::transInfo_t ti = getTransInfo(false, pTransLst);
	
	itemList.clear();
	if ( ti.Number < 0 ) // failure
	{
		rc =  FAILURE;
	}
	else
	{// else get the info
		// copy 
		itemList = pTransLst->dataNumList;
	}
	delete pTransLst;

	return rc;
}

/**********************************************************************************************
 * The transaction command Response generation loop 
 *	   - used by device simulation software
 **********************************************************************************************
 */
RETURNCODE hCtransaction::generateReplyPkt( hPkt* ppReplyPkt, bool actOK, HreferenceList_t* pA)
{
	transInfo_t ti;
	bool isValid;
	hCVar * pVar = NULL;
	RETURNCODE  rc = SUCCESS;
	resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	int         len = 0; // this is persisent data from the data item. We do nothing with it
						 // but pass it to the next data item.Filled and emptied from item.
#ifdef _DEBUG
	int cmd = -1;
	if (pCmd)
		cmd = pCmd->getCmdNumber();
#endif
	ti = getTransREPInfo(pTransLst);// resolve all the conditionals
	if ( ti.Number < 0 ) // failure
	{
		return  FAILURE;
	}
	else
	{
		BYTE* pBuf = ppReplyPkt->theData; // a working pointer
		bool trunc = false; // stevev 18jan06
		itemID_t ID= 0;     // stevev 18jan06

		hCdataItem* pDataItem; // 12may16 - stop using iterator as pointer...
		// scan the resolved list constructing each data item 
		//for ( hCdataitemList::iterator iT = resReplyStruct.pRslvdDataItemList->begin();
		//	              iT < resReplyStruct.pRslvdDataItemList->end() && rc == SUCCESS; iT++)
		for ( hCdataitemList::iterator iT = pTransLst->RslvdDataItemList.begin();
							 iT < pTransLst->RslvdDataItemList.end() && rc == SUCCESS; iT++)
		{// iT is a ptr 2 a hCdataItem
/** stevev 18jan05 - **/
			pDataItem = &(*iT);
			pVar = pDataItem->getVarPtr();
			if ( pVar != NULL && (ID = pVar->getID()) == RESPONSECODE_SYMID)
			{
				if ( ! (pVar->getDispValue()).valueIsZero() )
				{// if response code is non-zero, only two bytes allowed
					trunc = true;
				}
			}
/** stevev 18jan05 - end **/ 
			//let the data item handle the adding
            BYTE z = ppReplyPkt->dataCnt;
			rc |= pDataItem->AddIt(&(z), &pBuf, len, isValid);
			ppReplyPkt->dataCnt = z;
#ifdef _DEBUG
			if (rc != SUCCESS)
			{
				LOGIT(CLOG_LOG,"Command's transaction failed to add item to packet.\n");
			}
#endif
/** stevev 18jan05 - **/
			if ( trunc )
			{
				if (ID == DEVICESTATUS_SYMID)
				{// we just added the last legal byte
					break; // out of for loop
				}
				else
				if (ID == RESPONSECODE_SYMID)
				{// clear the RC for next command
					/* 
						LINUX_PORT - REVIEW_WITH_HCF: Casting to int to fix ambiguous assignment 
						compiler error (no long assignment override available). Check if that 
						is what was intended.
					*/
					CValueVarient tv; tv = (int) 0L;
					pVar->setDispValue(tv);
					pVar->ApplyIt();
				}
			}// else nop
/** stevev 18jan05 - end **/ 
		}// next data item
	}
	delete pTransLst;

	return rc;
}

RETURNCODE hCtransaction::markReplyPending  (resolvedTrans_t* pTransLst)// ptr means: filled
{
	transInfo_t ti;
	hCVar * pVar = NULL;
	RETURNCODE  rc = SUCCESS;
	bool IownTrans = false;
	int         len = 0; // this is persisent data from the data item. We do nothing with it
						 // but pass it to the next data item.Filled and emptied from item.

	if (pTransLst == NULL)
	{
		pTransLst = new resolvedTrans_t(devHndl());
		/* added stevev 13apr-06 from vibhor - merge queries spread */
		if (pTransLst != NULL )	
		{
			IownTrans = true;
		}// end add

		ti = getTransREPInfo(pTransLst);// resolve all the conditionals
		if ( ti.Number < 0 || pTransLst == NULL) // failure
		{
			rc =  FAILURE;
		}
		// else leave rc = SUCCESS;
	}
	if ( rc == SUCCESS )
	{
		// scan the resolved list constructing each data item 
		//for ( hCdataitemList::iterator iT = resReplyStruct.pRslvdDataItemList->begin();
		//	              iT < resReplyStruct.pRslvdDataItemList->end() && rc == SUCCESS; iT++)
		for ( hCdataitemList::iterator iT = pTransLst->RslvdDataItemList.begin();
							 iT != pTransLst->RslvdDataItemList.end() && rc == SUCCESS; iT++)
		{// iT is a ptr 2 a hCdataItem
			//let the data item handles the adding
			//TODO: we should iterate ptr list so we don't resolve again
			rc |= iT->markPending();
		}// next data item
	}
	if (IownTrans)
	{
		delete pTransLst;
	}// else owner deletes

	return rc;
}


RETURNCODE  hCtransaction::invalidateReply(void) //clear state on erroneous request
{
	RETURNCODE rc = FAILURE;
	hCVar*     pVar = NULL;
	resolvedTrans_t* pTransLst =  new resolvedTrans_t(devHndl());
	transInfo_t      ti;
	ti = getTransREPInfo(pTransLst);// resolve all the conditionals
	if ( ti.Number < 0 ) // failure
	{
		rc =  FAILURE;
	}
	else
	{
		rc = SUCCESS;
		for ( hCdataitemList::iterator iT = pTransLst->RslvdDataItemList.begin();
/* stevev for Vibhor 13apr06 - do not abort when encountering a constant
					iT < pTransLst->RslvdDataItemList.end()  &&   rc == SUCCESS; iT++)
*/								iT != pTransLst->RslvdDataItemList.end(); iT++)
		{// iT is a ptr 2 a hCdataItem	
			pVar = iT->getVarPtr();
			if (pVar != NULL)
			{
//redundant		pVar->setDataQuality(DA_NOT_VALID);	// command failed - quality is bad		
				pVar->markItemState(IDS_INVALID);// force it bad
			}
			else
			{
				rc = FAILURE;
			}
		}// next
	}
	//VMKP 260304	
	pTransLst->clear();
	RAZE(pTransLst);
	return rc;
}

/**********************************************************************************************
 * The transaction command generation loop 
 *	
 **********************************************************************************************
 */	
// if pTransList not NULL then it will be filled with reply info
// 2nov05 - this will now execute pre-WRITE actions and put pre-read actions in the list
RETURNCODE hCtransaction::
			  generateRequestPkt(hPkt* ppReqPkt, hMsgCycle_t* pMC, resolvedTrans_t* pReplyLst)
{
	RETURNCODE rc = SUCCESS;
	transInfo_t     tiQ,      tiP;
	bool isValid;
	bool IownList = false;
	methodCallList_t          preActions;

	int cmdNum = -1;
	if (pCmd)
		cmdNum = pCmd->getCmdNumber();


	if ( pMC == NULL )
	{// we cannot function in this scenario
		LOGIF(LOGP_NOT_TOK)
				(CERR_LOG,"ERROR: (programmer error) No message cycle generating packet.\n");
		return APP_PARAMETER_ERR;
	}

	cmdOperationType_t        cmdOp;
	int       len = 0; // this is persisent data from the data item. We do nothing with it
						 // but pass it to the next data item.Filled and emptied from item.
	resolvedTrans_t* pReqstLst = new hCtransBase::resolvedTrans_t(devHndl());

	if (pReplyLst == NULL)
	{
		pReplyLst = new resolvedTrans_t(devHndl());
		IownList  = true;
	}
	else
	{
		pReplyLst->clear();
		IownList  = false;
	}
	cmdOp = pMC->cmdType;

	tiQ = getTransREQInfo(pReqstLst);// resolve all the conditionals
	tiP = getTransREPInfo(pReplyLst);// and then get all the pointers
	/* note that the dataitem list is really the only valid part of this
	   until the indexes are filled   */	
	if (pMC->indexList.size()>0)
	{	// set local indexes
		rc = setLocalIndexes(pReqstLst, pMC->indexList);
		if ( rc == SUCCESS )
		{
			rc = setResolvedStruct(pReqstLst);// fills in the rest of the structure from items
			if ( rc != SUCCESS )
			{
				LOGIF(LOGP_NOT_TOK)
					   (CERR_LOG,"ERROR: setResolvedStruct Req could not resolve the list.\n");
				tiQ.clear();
			}
			else
			{
				tiQ = *pReqstLst;//  (operator=)
			}
/*** appears to be a double click of cntl-V -- 
	7apr11 comment out...if no issue later, remove this code
			rc = setResolvedStruct(pReplyLst);// fills in the rest of the structure from items
			if ( rc != SUCCESS )
			{
				LOGIF(LOGP_NOT_TOK)
					(CERR_LOG,"ERROR: setResolvedStruct Rep could not resolve the list.\n");
				tiP.clear();
			}
			else
			{
				tiP = *pReplyLst;//  (operator=)
			}
****/
		}
		/* stevev 13apr06 - since the indexes failed, we need to fail the command. 
							- from Vibhor's comment */
		else
		{
			return rc;
		}
	}// else - all three lists should be valid 


	if ( tiQ.Number >= 0 && tiP.Number >= 0 ) // success
	{
		BYTE*  pBuf    = ppReqPkt->theData; // a working pointer
		hCVar* pVar    = NULL;

		rc = SUCCESS;

		/* see section PACKET CONSTRUCTION in the NOTES: at the end of this file */
		// set local indexes
		// determine if we are going to send this command		
		int reqV = anyValidInReq(pReqstLst) ;// returns false for ALL invalid, 
		int repV = anyValidInRep(pReplyLst);// -1 for ALL valid,else cnt

		// Read:
		if ( cmdOp == cmdOpRead 
			&& pMC->m_cmdOrginator != co_METHOD ) // stevev 25may07 - all rules are discarded
		{//	if  All of Request Pkt AND ANY of Reply Pkt isValid() then OK 
			if (reqV >= 0 || repV == 0 ) // all or any invalid in req||all invalid in rep FAILS
			{// we aren't sending this command
				rc = APP_CMD_VALIDITY_RULE_ERR;
			}
			else // we are sending it, continue			
			if ( pMC->actionsEn ) // do the actions if needed
			{//	extract the pre-read actions and post read actions from the reply pkt
				// removed commented code 2nov05 - cleanup
				rc = getREPLYactions(pReplyLst, pMC->pre_Actions, pMC->postActions, false);
			}// else - we're sending it - fall through to do that (rc should be SUCCESS)
		}
		else
		// Write & Cmd-Cmd
		if ( ( cmdOp == cmdOpWrite || cmdOp == cmdOpCmdCmd)  
			 && pMC->m_cmdOrginator != co_METHOD ) // stevev 25may07 - all rules are discarded
		{//	if Any of Request Pkt AND ANY of Reply Pkt isValid() then OK 
#ifdef VALIDY_RULE_LENIENT		/* top of this file */
			if (compatability() == dm_275compatible)
			{
				if (reqV == 0 || repV == 0 ) // either pkt is ALL Invalid - do not send
					rc = APP_CMD_VALIDITY_RULE_ERR;
			}
			else
#endif
			// march 06 as per WAP- rd & wrt pkt rules are now the same
			//	if All of Request Pkt AND ANY of Reply Pkt isValid() then OK 
			if (reqV >= 0 || repV == 0 ) // all or any invalid in req||all invalid in rep FAILS
			{// we aren't sending this command
				rc = APP_CMD_VALIDITY_RULE_ERR;
			}
			// else // we are sending it, continue	w/ SUCCESS
			// we get the post actions after the packet construction so they will be 
			//     executed in the correct order
		}
		//else	// cmdNone 

		// common code - some differences between rd/wr
		if ( rc == SUCCESS)// validity rules passed (or we're in a method)
		{
//stevev 3/4/04 - needs reply pkt not request		pMC->transVarList.clear();// stevev 2/26/04
			//	scan the resolved list;   constructing each data item 
			//		for each request data item
			hCdataitemList::iterator iIT;		
			int paramCnt = 0;
			// Note that we have to go through the item list in order to get the constants
			for ( iIT = pReqstLst->RslvdDataItemList.begin();
						iIT < pReqstLst->RslvdDataItemList.end()  &&   rc == SUCCESS;    iIT++)
			{// iIT is a ptr 2a hCdataItem	... for each request packet data item
				pVar = iIT->getVarPtr();// reference is re-evaluated here (could be different)
				paramCnt++;
				if (pVar != NULL)
				{
					if ( pMC->actionsEn  && cmdOp != cmdOpCmdCmd )
					{//	execute pre action (rd | wr)
						if ( cmdOp == cmdOpWrite )
						{
							//if write
							//	put post write action to list
							//  - 2nov05 - moved before execution
							rc = pVar->getPstWrActs(pMC->postActions);
/* stevev 2nov05 - we are executing in the UI thread now. We must do pre-write actions BEFORE
				   we fill the packet.  In theory, any sends here will re-enter OK */
/* stevev 4/20/04 - just get the list - 2nov05 - re-enable as per note above */
							rc = pVar->doPreWrActs();
/* start remove - we gotta execute now .. end remove - stevev 4/20/04 - just get the list -*^/
							rc = pVar->getPreWrActs(pMC->pre_Actions);
/^*  end remove end add - stevev 4/20/04 - just get the list -*/
						}
						else
						if ( cmdOp == cmdOpRead )
						{
/* stevev 4/20/04 - just get the list -
							rc = pVar->doPreRdActs();// rc is the return value
end remove - stevev 4/20/04 - just get the list -*/
							rc = pVar->getPreRdActs(pMC->pre_Actions);
/*end add - stevev 4/20/04 - just get the list -*/
						}
						// else it's a cmd-cmd and we won't do any actions
					}// else actions are disabled so we don't deal with 'em

					//insert value into packet

					//Anil 151206 If the data that we are interested within the 
					//  method scope( i.e when method is running) then we should not read from
					//  the device hence the new check pMC->m_cmdOrginator != co_METHOD
					//We got this issue when running D/A trim method in Honeywell ST3000 device
					// stevev 25may07 - also for beck device -				
					if (  pMC->m_cmdOrginator    != co_METHOD        && 
						( pVar->getDataQuality() == DA_NOT_VALID ||  
						  pVar->getDataQuality() == DA_STALEUNK	   )    )
					{
						CValueVarient RetDataVal;// discarded
						if ( devPtr()->ReadImd((hCitemBase*)pVar,RetDataVal) != SUCCESS ||
							 pVar->getDataQuality() == DA_NOT_VALID ||  
							 pVar->getDataQuality() == DA_STALEUNK     )
						{
							// L&T Modifications : PortToAM335 - Start
							LOGIT(CERR_LOG|CLOG_LOG,
                            "ERROR: Cmd %s %d 0x%04x \n",
								"GenerateRequestPacket could not get good data for",
                                pCmd->getCmdNumber(),pVar->getID());// send the trash anyway
							// L&T Modifications : PortToAM335 - End
						}// else it's good to go
					}//else it's good to go

//stevev 3/4/04 - needs reply pkt not request			pMC->transVarList.push_back(pVar);

					BYTE z = ppReqPkt->dataCnt;
					rc = iIT->AddIt(&(z), &pBuf, len, isValid);
					ppReqPkt->dataCnt = z;
					/* stevev 3/2/4 - write status is for writing dynamic vars.
						It is set when the user writes it and cleared here (where
						the write command is sent).  The extraction does not update 
						the display variable if this is set - allowing dynamic reads 
						to continue until the user commits or aborts.
					*/
					if ( cmdOp == cmdOpWrite )
					{
						pVar->setWriteStatus(0);// clear to normal
					}

				}// pVar is null
				else // it's probably a constant - let addit handle the work
				{
					BYTE z = ppReqPkt->dataCnt;
					rc = iIT->AddIt(&(z), &pBuf, len, isValid);
					ppReqPkt->dataCnt = z;					
				}
			}// next data item


			// we get these after the packet for correct execution order
			if ( cmdOp == cmdOpWrite || cmdOp == cmdOpCmdCmd)
			{		
				if ( pMC->actionsEn && cmdOp != cmdOpCmdCmd) 
				{//	append reply packet's post write actions
				 //	append reply packet's post  read actions
/* stevev 4/20/04 - just get the list -
					rc = getREPLYactions(pReplyLst, preActions, pMC->postActions, true);
end remove - stevev 4/20/04 - just get the list -*/
					rc = getREPLYactions(pReplyLst, pMC->pre_Actions, pMC->postActions, true);
/*end add - stevev 4/20/04 - just get the list -*/
				}// else discard
			}// else we don't care
			ppReqPkt->timeout = tiP.size;// overloading timeout to carry expected size
		}
		else // validity rules failed or action fetch failed		
		{
			if ( rc == APP_CMD_VALIDITY_RULE_ERR )
			{
#ifdef _DEBUG
				int cmd;
				if (pCmd)
					cmd = pCmd->getCmdNumber();
				else
					cmd = -1;
				/*
				// these two return false for ALL invalid, -1 for ALL valid,else cnt
				*/
				char locQ[30], locP[30];
				if (reqV < 0 )			// ALL valid
				{ strcpy(locQ,"No");   }
				else
				if(reqV == 0)			// ALL INVALID
				{ strcpy(locQ,"All");   }
				else					// some valid
				{ sprintf(locQ,"+ %d +",reqV);}

				if (repV < 0 )			// ALL valid
				{ strcpy(locP,"No");   }
				else
				if(repV == 0)			// ALL INVALID
				{ strcpy(locP,"All");   }
				else					// some valid
				{ sprintf(locP,"%d",(repV-2));}
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: Command validity rule failure. "
				"Cmd:%d,Trans:%d had %s invalid in request and %s invalid in reply (of %d).\n",
				cmd,transNum,locQ,locP,(pReplyLst->RslvdDataItemList.size() - 2));
#else
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Command validity rule failure.%d.%d\n",
					cmdNum, transNum);
#endif
			}
			else // action fetch failed
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: action fetch failure %d.\n",rc);
			}
		}
	}
	else // tiQ.Number < 0 || tiP.Number < 0
	{
		LOGIF(LOGP_NOT_TOK)
			(CERR_LOG,"ERROR: REQ/REP acquasition failed. Q=%d P=%d.\n",tiQ.Number,tiP.Number);
	}

	if (IownList)
	{
		delete pReplyLst;
	}
	else // fill with response data owner deletes
	if ( rc == SUCCESS )
	{
		if ( tiP.Number < 0 ) // failure
		{
			rc =  FAILURE;
		}// else leave it success
	}
	//VMKP 260304	
	pReqstLst->clear();
	RAZE(pReqstLst);
	return rc;
}

// helper function for generate reply packet
//      read command  response - get pre-read and post-read actions from the reply vars
//		write command response - get all post-write followed by all post-read actions
// assumptions on entry -	that all three lists in the resolvedTrans_t are valid
//							the two action lists are in condition to be appended
RETURNCODE  hCtransaction::
	getREPLYactions(resolvedTrans_t* pReplyLst, 
				   methodCallList_t& preActions, methodCallList_t& postActions, bool isWrite)
{
	RETURNCODE rc = SUCCESS;
	if (pReplyLst == NULL)
	{
		return APP_PARAMETER_ERR;
	}

	hCVar* pVar = NULL;
	hCdataitemList::iterator iIT;		

	for ( iIT = pReplyLst->RslvdDataItemList.begin();
				iIT < pReplyLst->RslvdDataItemList.end()  &&   rc == SUCCESS;    iIT++)
	{// iIT is a ptr 2a hCdataItem	... for each request packet data item
		pVar = iIT->getVarPtr();// reference is re-evaluated here (could be different)
	
		if (pVar != NULL)
		{
			if ( isWrite )
			{ // get all post-write followed by all post-read actions
				rc = pVar->getPstWrActs(postActions);	
			}
			else// is read
			{//get pre-read and post-read actions from the reply vars
				rc = pVar->getPreRdActs(preActions);
				if ( pVar->getWriteStatus() == 0 ) // normal
				{
					rc = pVar->getPstRdActs(postActions);
				}
			}
		}
		//else skip null var ptrs (normal on constants)
	}// next data item

	if ( isWrite)// we can skip this on reads
	{	
		for ( iIT = pReplyLst->RslvdDataItemList.begin();
					iIT < pReplyLst->RslvdDataItemList.end()  &&   rc == SUCCESS;    iIT++)
		{// iIT is a ptr 2a hCdataItem	... for each request packet data item
			pVar = iIT->getVarPtr();// reference is re-evaluated here (could be different)
		
			if (pVar != NULL)
			{
				rc = pVar->getPstRdActs(postActions);
			}
			//else skip null var ptrs (normal on constants)
		}// next data item
	}// else we be done
	return rc;
}

// helper function for generate reply packet
// does the logic to fill local indexes in the command
//		only the data item list in the first parameter is valid at the time this is called
RETURNCODE  hCtransaction::setLocalIndexes(resolvedTrans_t* pDIlist, indexUseList_t& indexLst)
{
	RETURNCODE      rc = SUCCESS;
	itemID_t     iItem = 0;
	hCitemBase*  pItem = NULL;
	hCVar*       pVar  = NULL;
	int nxtIndex2use   = 0;				// position of const in index list
	int paramCnt       = 0;
	hIndexUse_t              idxUse;
	hCdataitemList::iterator iT;

	if ( indexLst.size() <= 0 )
	{
		return SUCCESS; // nothing to do
	}

	// scan the resolved list to fill local indexes from passed in list
	for ( iT = pDIlist->RslvdDataItemList.begin();
								iT < pDIlist->RslvdDataItemList.end()  &&  rc == SUCCESS; iT++)
	{// iT is a ptr 2 a hCdataItem	... for each request packet data item
		pVar = NULL;	// setup default condition
		paramCnt++;

		if (iT->type == (ulong)cmdDataReference)
		{			
			if ( (rc = iT->ref.resolveID(iItem,false))  == SUCCESS )
			{
				if ( ( rc = devPtr()->getItemBySymNumber(iItem, &pItem))  == SUCCESS 
					&& pItem != NULL )
				{	
					if ( pItem->getIType() == iT_Variable )
					{
						pVar = (hCVar*)pItem;
					}
					//else- data item is an array/collection - do not try to use it (pVar NULL)
					/* WAP: 2/18/04 - we do not support arrays or collections of local indexes 
							to any depth (ie arrays of collections of local indexes) */
					// if group resolves to a var, we don't need it now, 
					// if group resolves to a local index, we don't support it now
					// skip all but variables
				}
				else
				{
					LOGIF(LOGP_NOT_TOK)
					  (CERR_LOG, "ERROR: reference could not get item number 0x%04x.\n",iItem);
					rc = APP_RESOLUTION_ERROR;
				}
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)
							(CERR_LOG, "ERROR: could not resolve reference in dataitem.(2)\n");
				// leave rc success to let it finish the for-loop (pVar will be NULL)
			}
				
			if (pVar != NULL &&
				pVar->VariableType()==vT_Index && 
				pVar->IsLocal() )
			{// we have a local index and some passed in values/symbols
				CValueVarient newValue;
				int fnnd = 0, allRvalues = 1;
#ifdef _DEBUG
/**/			int c = indexLst.size();
/**/			if ( c > 0 )
/**/			{
/**/				LOGIT(CLOG_LOG,"Cmd %d with index list %d long.\n",pCmd->getCmdNumber(),c);
/**/			}
#endif
				indexUseList_t::iterator iK;	// PAW added 03/03/09
				for (iK= indexLst.begin(); iK < indexLst.end(); iK++ )
				{// ptr 2a hIndexUse_t ... for each passed in index
					if (iK->indexSymID > 0)// it is a symbol (not a const value)
					{
						allRvalues = 0;// we have a symbol
						if ( iK->indexSymID == pVar->getID() )
						{
							//newValue= idxUse.indexValue;
							newValue= (unsigned)(iK->indexDispValue);
							pVar->setDispValue(newValue);	// set one
							pVar->ApplyIt();				// copy it to the other
							pVar->markItemState(IDS_CACHED);// force it good
							fnnd = 1;
							idxUse = *iK;
							break;// out of for loop
						}// else - look some more
					}//else it won't match - look at the next
				}// next passed in index
				if (fnnd && (! allRvalues))// we had some index symbols and found this var
				{// stevev - filter success to eliminate error message

/**/			
/**/				DEBUGLOG(CLOG_LOG,"Local Index found by symbolID 0x%04x with Value %d\n",
/**/										idxUse.indexSymID,(int)idxUse.indexDispValue);
/**/				// it worked, use it as is (already set

				}
				else 

				if (!fnnd)  // - not a directed index (no symbolID for value)
				{// two combinations handled similarly	
					
#ifdef _DEBUG
/**/				if (!allRvalues)
/**/				{// this symbol not there (possibly mixed symbols/constants)
/**/					LOGIT(CLOG_LOG,L"The index list did not hold symbol 0x%04x for Local " 
/**/																L"Index\n",pVar->getID());
/**/				}
/**/				else // (allRvalues)
/**/				{//we have no symbols in the list so we didn't find this one
/**/					LOGIT(CLOG_LOG,L"The index list did not hold 0x%04x nor any symbols.\n"
/**/																		   ,pVar->getID());
/**/				}
#endif

					// scan for the nxtIndex2use constant
					while (nxtIndex2use < (int)indexLst.size() && !fnnd) 
					{// current next or scan for one

						if (indexLst[nxtIndex2use].indexSymID == 0 )// is const
						{
							fnnd = 1;
							newValue = indexLst[nxtIndex2use].indexDispValue;
#ifdef _DEBUG
/**/						LOGIT(CLOG_LOG,L" Setting 0x%04x  to value %s\n",pVar->getID(), 
/**/															 ((wstring)newValue).c_str());
#endif
							/** stevev 17jan06 - do not set an invalid value!! **/
							if ( ! pVar->isInRange(newValue) )
							{
#ifdef _DEBUG
	/**/							LOGIT(CLOG_LOG,L" Changing 0x%04x's invalid %d to ",
	/**/									pVar->getID(), (int)newValue );
#endif
								((hCindex*)pVar)->lowest(newValue);
#ifdef _DEBUG
/**/								LOGIT(CLOG_LOG,L" %d\n",(int)newValue );
#endif
							}
							/** stevev 17jan06 -- end **/
							// all values so I know to use this value
							pVar->setDispValue(newValue);	// set one
							pVar->ApplyIt();				// copy it to the other
							pVar->markItemState(IDS_CACHED);// force it good
							if (++nxtIndex2use  >= (int)indexLst.size() )
							{
								nxtIndex2use = 0;// reuse the first one(s)
							}
						}
						else
						{// this is a symbol in a (possibly) mixed list - try the next one
							nxtIndex2use++;
						}
					}// wend next in list

					if (nxtIndex2use >= (int)indexLst.size())
					{// was never found
						newValue = indexLst[0].indexDispValue;						
						/* VMKP changed on 200204:  SDC was crashing at this line when i 
								run with XMTR for the device rosemount 3144 (0x26190302) */
						LOGIT(CLOG_LOG,
							" Const value for 0x%04x was not found. %s %d for param# %d.\n",
							pVar->getID(),"We will use the first index value",
							(int)indexLst[0].indexDispValue, paramCnt);
						/* End of VMKP Modifications */
						// all values so I know to use this value
						pVar->setDispValue(newValue);	// set one
						pVar->ApplyIt();				// copy it to the other
						pVar->markItemState(IDS_CACHED);// force it good
						nxtIndex2use = 0; // restart from the top
					}
				}
				else // all values and found - an impossible combination
				{
#ifdef _DEBUG
/**/				LOGIT(CLOG_LOG,L"The index list did not hold 0x%04x\n",pVar->getID());
#endif
					LOGIF(LOGP_NOT_TOK)
								   (CERR_LOG,L"ERROR: found index when all Values in list.\n");
				}
					
#ifdef _DEBUG				
/**/			newValue = pVar->getDispValue();
/**/			if (iK < indexLst.end() &&   ((int)newValue) != (int)iK->indexDispValue )
/**/			{
/**/				LOGIT(CLOG_LOG, L"Index mismatch.  0x%04x has %d and not %d for 0x%04x"
/**/								L"  Found %d of %d\n",pVar->getID(),(int)newValue,
/**/										(int)(iK->indexDispValue),iK->indexSymID, fnnd, c);
/**/			}
/**/												
/**/			if ( pVar->getID() != idxUse.indexSymID )
/**/			{//not an error - we don't do abbreviated commands, paramCnt must always be > 1
/**/				LOGIT(CLOG_LOG, L"WARNING: Param# %d symbols don't match. Var=0x%04x"
/**/				L" while Index=0x%04x (%d in index List)        [if Param numbers are > 1 "
/**/				L"this should be OK.-SDC does not abbreviate commands.]\n",
/**/				paramCnt, pVar->getID(), idxUse.indexSymID, indexLst.size());
/**/			}
#endif				
			}
			// else it not an index or is not local - skip it			
		}// else item is not a ref, (is a constant) - skip it
	}// next data item in list
	
	return rc;
}

int hCtransaction::responseSize(void) // number of variables in the response packet
{	
	int cnt = 0;
	RETURNCODE rc = SUCCESS;	
	hCdataitemList  RslvdDataItemList(devHndl());

	rc = response.resolveCond(&RslvdDataItemList);
	if ( rc != SUCCESS ) // failure
	{
		LOGIF(LOGP_NOT_TOK)
			 (CERR_LOG,"ERROR: responseSize failed to resolve the response dataitem list.\n");
	}
	else
	{
		cnt = RslvdDataItemList.size();
	}
	return cnt;// should be at least two
}

// an index has come back incorrect, invalidate the data it is used in
RETURNCODE  hCtransaction::invalidateIndexedReply(indexUseList_t& erList)
{
	RETURNCODE rc = SUCCESS;
	if (erList.size() > 0)
	{
		hCdataitemList  RslvdDataItemList(devHndl());

		rc = response.resolveCond(&RslvdDataItemList);
		if ( rc != SUCCESS ) // failure
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,
				"ERROR: extractReplyPacket failed to resolve the response dataitem list.\n");
		}
		else
		{	itemID_t tL = 0;
			
			LOGIT(UI_LOG|CERR_LOG|CLOG_LOG,"Device Error: (c:%d t:%d) Index in Reply does not "
				"match Index in Request.\n",pCmd->getCmdNumber(),transNum);
			hCvarGroupTrail* pGT = NULL;
			hCitemBase* pItemWrk = NULL;
			hCinteger* pIntWrk;
			int  holdIdxVal;

			for ( idxUseIT idxIT = erList.begin(); idxIT < erList.end(); idxIT++ )
			{// idxIT isa ptr 2a hIndexUse_t
				// set the index to its 'supposed to be" value
				if ( SUCCESS == devPtr()->getItemBySymNumber(idxIT->indexSymID,&pItemWrk)
					&& pItemWrk != NULL )
				{
					pIntWrk     = (hCinteger*)pItemWrk;
					holdIdxVal  = (int) pIntWrk->getDispValue();
					int y = (int)(idxIT->indexDispValue);
					pIntWrk->SetIt(&y);
					// force it to the requested value for ref resolution					
					pIntWrk->ApplyIt();
				}// else we are out of luck...



				for ( hCdataitemList::iterator iT = RslvdDataItemList.begin();
									 iT < RslvdDataItemList.end() && rc == SUCCESS; iT++)
				{// iT is a ptr 2 a hCdataItem
					if (iT->ref.getRefType() == rT_viaItemArray )
					{
						rc = iT->ref.resolveID(tL,false);// fill the trail
						if ( rc == SUCCESS )
						{
							pGT = iT->ref.getGroupTrailPtr();
							if ( pGT != NULL)
							{// WE ONLY TEST THE FIRST LEVEL OF INDIRECTION
								if ( idxIT->indexSymID >0  &&  
									pGT->pExprTrail->isVar && 
									idxIT->indexSymID == pGT->pExprTrail->srcID )
								{// mark the resolved data INVALID - bad					
									// we have already resolved the index pointer
									pIntWrk->markItemState(IDS_INVALID);// unusable
									pIntWrk->setDataStatus(0);// stop weighing
									LOGIT(CLOG_LOG,
										  "Marked 0x%04x Status to zero.\n",pIntWrk->getID());
									if (pGT->itmPtr != NULL && 
										pGT->itmPtr->getIType() == iT_Variable)
									{//                                        unusable
										((hCVar*)pGT->itmPtr)->markItemState(IDS_INVALID);
										((hCVar*)pGT->itmPtr)->setDataStatus(0);//stop weighing
										LOGIT(CLOG_LOG,
												 "Marked 0x%04x Status to zero.\n",pGT->itmID);
									}
								}
								// else no match - continue
								// note that constants cannot be compared
							}
						}
					}
					// else we don't have to worry about it
				}// next data item
				// return it to its passed in value
				holdIdxVal  = (int) pIntWrk->getDispValue();
				pIntWrk->SetIt(&(holdIdxVal));
				pIntWrk->ApplyIt();
			}
		}
		RslvdDataItemList.clear();// before destruction
	}
	//else	// nothing to do - return success

	return rc;
}

// real parse reply work happens here
//  returns # updated, neg on error
// stevev 16may11 - added the sent index list to allow the detection of index mismatch down
//							in the extraction section while the INFO items are still valid.
int hCtransaction::extractReplyPkt
(hPkt* pReplyPkt, methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
									varPtrList_t &hasChangedList, indexUseList_t* pSentIdex  )
{
	return (extractReplyPkt(pReplyPkt, eaLst, noteMsgs, hasChangedList, false, pSentIdex));
}

int hCtransaction::extractReplyWritePkt
(hPkt* pReplyPkt, methodCallList_t& eaLst, hCmsgList& noteMsgs, 
									varPtrList_t &hasChangedList, indexUseList_t* pSentIdex )
{
	return (extractReplyPkt(pReplyPkt, eaLst, noteMsgs, hasChangedList, true, pSentIdex));
}

/* common - extraction code */
// stevev 16may11 - pSentIdex is for detecting index mismatch ONLY
RETURNCODE hCtransaction::extractReplyPkt  
				(hPkt* pReplyPkt, methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
				varPtrList_t &hasChangedList, bool isWrite, indexUseList_t* pSentIndex)
{
	int cnt = -1;
#ifdef _DEBUG
	int cmdN = pCmd->getCmdNumber();
#endif
	RETURNCODE rc = SUCCESS;	
	hIndexUse_t  indexInfo;
	indexUseList_t infoList;
	//transInfo_t ti;
	//resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	hCdataitemList  RslvdDataItemList(devHndl());

	indexUseList_t* pSentIdx = pSentIndex; // we will treat an empty list the same as no list
	if (pSentIdx != NULL && pSentIdx->size() == 0)
	{
		pSentIdx = NULL;
	}

	int        len = 0;  // this is persisent data from the data item. We do nothing with it
						 // but pass it to the next data item.Filled and emptied from item.

	//ti = getTransREPInfo(pTransLst);// resolve all the conditionals
	rc = response.resolveCond(&RslvdDataItemList);
	if ( rc != SUCCESS ) // failure
	{
		LOGIF(LOGP_NOT_TOK)
		(CERR_LOG,"ERROR: extractReplyPacket failed to resolve the response dataitem list.\n");
	}
	else
	{		
		BYTE* pBuf = &(pReplyPkt->theData[0]); // a working pointer
		BYTE  dCnt =   pReplyPkt->dataCnt;

		// scan the resolved list extracting each possible index item (extraction pass 1)
		hCdataitemList::iterator iT;
		transactionType_t transType;
		if (isWrite)transType = tt_isWrite; else transType = tt_NoType;// do not extract data

		
		//		isExtract is clear meaning this is PASS ONE ----------------------------------
		for ( iT = RslvdDataItemList.begin(); 
			  iT < RslvdDataItemList.end() && rc == SUCCESS; iT++)
		{// iT is a ptr 2 a hCdataItem
		#ifdef _DEBUG
			if ((dCnt & 0xF0) == 0xF0)// went negative
			{
				LOGIF(LOGP_NOT_TOK)
						(CLOG_LOG,"ERROR: data item extraction went into negative length.\n");
			}
			else
			{
		#endif
				//let the data item handle the extraction of index wannabes
				rc = iT->ExtractIt(&dCnt,&pBuf,len,eaLst,noteMsgs,
											   hasChangedList,indexInfo,transType,pSentIndex);
				if (rc == APP_INDEX_MISMATCH)
				{
					LOGIT(UI_LOG|CERR_LOG|CLOG_LOG,L"Device Error: (c:%d t:%d) Index in Reply "
					L"does not match Index in Request.\n",pCmd->getCmdNumber(),transNum);
					rc = SUCCESS;// let it keep going
				}


				if (dCnt == 0 || rc != SUCCESS)
				{
					rc = FAILURE;// abort loop
				}
				else
				if (dCnt > pReplyPkt->dataCnt)
				{
					rc = FAILURE;// abort loop
				}
				
				if ( indexInfo.indexWrtStatus == 2 )// its an info
				{
					infoList.push_back(indexInfo);
					indexInfo.clear();
				}

		#ifdef _DEBUG
			}
		#endif
		}// next data item
		
		// stevev 04nov08 - oops - didn't reset the pointers
		pBuf = &(pReplyPkt->theData[0]); // a working pointer
		dCnt =   pReplyPkt->dataCnt;
		rc   =   SUCCESS;

		transType = (transactionType_t)(transType | (int)tt_isExtract);// pass2 - extract data
		// scan the resolved list extracting each item in the list	(extraction pass 2)
#ifdef _DEBUG
	unsigned c = 0;
#endif
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"    Ext - - - - - pass 2 - - - - - - -\n" );//-------

		for ( iT = RslvdDataItemList.begin(); 
			  iT != RslvdDataItemList.end() && rc == SUCCESS; iT++)
		{// iT is a ptr 2 a hCdataItem
#ifdef _DEBUG
	assert(c < RslvdDataItemList.size());
#endif
			//let the data item handle the extraction 
			rc |= iT->ExtractIt(&dCnt, &pBuf, len, eaLst, noteMsgs, 
														hasChangedList, indexInfo, transType);
#ifdef _DEBUG
	c++;
#endif

			if ( rc == SUCCESS)
			{
				cnt++;// first is zero at this point
			}

			if (dCnt == 0)
			{
				rc = FAILURE;// abort loop
			}
			else
			if (dCnt > pReplyPkt->dataCnt)
			{
				LOGIF( LOGP_COMM_PKTS )
					(CLOG_LOG," Reply extraction went past packet size on un-even boundry.\n");
				rc = FAILURE;// abort loop
			}

		}// next data item

		if ( cnt >= 0 ) cnt++; // convert to counting number

		restoreINFOitems(infoList);// return the INFO values before we return
	}//endelse cond list resolved
	//delete pTransLst;
	RslvdDataItemList.clear();// before destruction
	return cnt;

//--------------------------------------------	
//			a) vars (reference)->extract & update
//			b) masked (demask to referenced)
//			c) info or index - extract & do nothing
//			Var->update()
//				Get Item Mutex
//				If changed
//				realVar updated & DispVar updated
//				If displayed             - post update message to subscriber
//				if Actions Enabled: 
//					- If dominant in relation - mark dependents as stale.
//				/endif
//				realVar-marked CACHED
//				Release Item Mutex
//			-return
//		-return
//	Next item
// 	-return
}


// if we are extracting a request packet, we must be a reply-only simulator
// infoList is to get pre-extraction values of changed INFO items - is is needed outside here
// because we have to generate a reply packet..after that the values are reinstated
int hCtransaction::extractRequestPkt(hPkt* pReqstPkt, varPtrList_t &hasChangedList, 
														indexUseList_t& infoList, bool isWrite)
{	
	int cnt = -1;
	RETURNCODE rc = SUCCESS;	
	//transInfo_t ti;
	//resolvedTrans_t* pTransLst = new resolvedTrans_t(devHndl());
	hCdataitemList  RslvdDataItemList(devHndl());
	methodCallList_t eaLst;// stevev 11oct05 - we aren't dealing with these in xmtr right now
	hCmsgList   noteMsgs;  noteMsgs.isRqstPkt = true;
	hIndexUse_t indexInfo;

	int        len = 0;  // this is persisent data from the data item. We do nothing with it
						 // but pass it to the next data item.Filled and emptied from item.

	//ti = getTransREQInfo(pTransLst);// resolve all the conditionals
	rc = request.resolveCond(&RslvdDataItemList);
	if ( rc != SUCCESS ) // failure
	{
		LOGIF(LOGP_NOT_TOK)
		  (CERR_LOG,"ERROR: extractRequestPkt failed to resolve the request dataitem list.\n");
	}
	else
	if (RslvdDataItemList.size() > 0)// success and NOT a command command
	{				
		BYTE* pBuf = &(pReqstPkt->theData[0]); // a working pointer
		BYTE  dCnt =   pReqstPkt->dataCnt;

		// scan the resolved list extracting each possible index item (extraction pass 1)
		hCdataitemList::iterator iT;
		transactionType_t transType;
		if (isWrite)transType = tt_isWrite; else transType = tt_NoType;//do not extract @pass 1

		// isExtract is clear so this is PASS ONE ---------------------------------------------
		for ( iT = RslvdDataItemList.begin(); 
			  iT < RslvdDataItemList.end() && rc == SUCCESS;     ++iT)
		{// iT is a ptr 2 a hCdataItem
			//let the data item handle the extraction
			rc |= iT->ExtractIt(&dCnt, &pBuf, len, eaLst, noteMsgs, hasChangedList, 
									indexInfo, transType);// isWrite don't care on request pkt
			if ( rc == SUCCESS)
			{
				cnt++;// first is zero at this point
				if ( indexInfo.indexWrtStatus == 2 )
				{
					infoList.push_back(indexInfo);
					indexInfo.clear();
				}
			}
			else
			{
				cnt = -1;// error return
				break; // out of for loop
			}
		}// next data item
		
		// reset the pointers
		pBuf = &(pReqstPkt->theData[0]); // a working pointer
		dCnt =   pReqstPkt->dataCnt;

		transType = (transactionType_t)(transType | (int)tt_isExtract);//pass2 - extract data
		cnt = -1;// error return

		// scan the resolved list extracting each item in the list	 (extraction pass 2)
		for ( iT = RslvdDataItemList.begin(); 
			  iT != RslvdDataItemList.end() && rc == SUCCESS;	++iT)
		{// iT is a ptr 2 a hCdataItem
			//let the data item handle the extraction
			rc |= iT->ExtractIt(&dCnt, &pBuf, len, eaLst, noteMsgs,
														hasChangedList, indexInfo, transType);
														   // isWrite don't care on request pkt
			if ( rc == SUCCESS)
			{
				cnt++;// first is zero at this point
#ifdef XMTR
			// execute POST_RQSTUPDATE_ACTIONS ****************<<<<<<<<<<<<<<<<<<<
				if ( iT->type != cmdDataConst )// stevev 26jan09 stop lookup errors 
				{// all the other types get converted to ref...
					hCitemBase* pIB = NULL;
					hCVar*      pVar= NULL;

					rc = iT->getItemPtr(pIB);
					if ( rc == SUCCESS && pIB != NULL )
					{
						if ( pIB->getIType() == iT_Variable )
						{
							pVar = (hCVar*)pIB;
							pVar->doPstRequestActs();
						}// else skip it
					}// else skip it
				}// else don't look it up
#endif
			}
			else
			{
				LOGIT(CLOG_LOG,"   Extraction of request item %d failed.\n",cnt);
				cnt = -1;// error return
				break; // out of for loop
			}
		}// next data item
		if ( cnt >= 0 ) cnt++; // convert to counting number
	}
	else
	{// command command - no request data
		cnt = 0;
	}
	//delete pTransLst;
	RslvdDataItemList.clear();// before destruction
#ifdef XMTR
	if ( RcvActList.genericlist.size() > 0 )	// this will always be zero in a NON simulator
	{
		RETURNCODE rc = SUCCESS;
		ddbItemList_t  riL;
		hCmethodCall   wrkMthCall;
		CValueVarient  retVal;
		vector<hCmethodCall> methList;

		// rc = RcvActList.getItemPtrs(riL);
		hCreferenceList  tmpList(devHndl());
		if ( RcvActList.resolveCond(&tmpList) == SUCCESS )
		{
			rc = tmpList.getItemPtrs(riL);
		}

		for ( ddbItemLst_it IT = riL.begin(); IT != riL.end(); ++ IT)
		{// ptr2ptr2 hCitemBase
			if ( (*IT)->getIType() == iT_Method )
			{
				wrkMthCall.methodID  = (*IT)->getID();
				wrkMthCall.source    = msrc_CMD_ACT;//sjv 06mar07  msrc_ACTION;
				wrkMthCall.paramList.push_back(retVal);
				methList.push_back(wrkMthCall);
				wrkMthCall.clear();
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CLOG_LOG|CERR_LOG,"Error: Non method in action list.\n");
			}
		}// next	

		if ( methList.size() > 0 && ! devPtr()->getPolicy().isReplyOnly )
		{
			rc = devPtr()->executeMethod(methList,retVal);
		}
		else
		{
			// NORMAL::
			//cerr < <"ERROR:method list resolved to empty in PreEdit Actions.("< <rc< <")"
			//< <endl;
			//rc = FAILURE;
		}
	}
#endif

	return cnt;
}


// returns a list all dataitemLists
RETURNCODE  hCtransaction::getREQlistOdiLists(vector<hCdataitemList *>& retList)
{	// all possible payloads
	return (request.aquirePayloadPtrList(retList));
}

// returns a list all dataitemLists
RETURNCODE  hCtransaction::getREPlistOdiLists(vector<hCdataitemList *>& retList)
{
	return (response.aquirePayloadPtrList(retList));
}
// returns false for ALL invalid, -1 for ALL valid,else cnt
int hCtransaction::anyValidInReq(resolvedTrans_t* pReqLst) 
{
	RETURNCODE rc = SUCCESS;
	transInfo_t     ti;
	bool allValid, anyValid;
	bool IownTrans = false;
	int  itm = 0, cnt = 0; 

	if (pReqLst == NULL)
	{
		pReqLst = new resolvedTrans_t(devHndl());
		if (pReqLst != NULL)// memory leak repair - as per vibhor 13apr06
		{
			IownTrans = true;
		}
		ti = getTransREQInfo(pReqLst);// resolve all the conditionals
		if ( ti.Number < 0 || pReqLst == NULL) // failure
		{
			rc =  FAILURE;
		}
		else
		{
			rc = SUCCESS;
		}
	}
	
	if ( rc == SUCCESS )
	{
		hCVar* pVar = NULL;
		allValid = true;
		anyValid = false;

		// scan the resolved list constructing each data item 
		//		for each response data item
		for ( ddbItemList_t::iterator iT = pReqLst->dataPtrList.begin();
/* stevev 14ap06 - as per vibhor...no impact
								  iT < pReqLst->dataPtrList.end() && rc == SUCCESS; iT++)
*/								  iT != pReqLst->dataPtrList.end(); iT++)
		{// iT is a ptr 2 a ptr 2 a hCitemBase	
			pVar = (hCVar*)(*iT);
			if ( pVar != NULL )
			{	
				itm = pVar->getID();
				if ( itm < 150 || itm > 152)  // stevev 3/29/04 changed as per prasad S.
				{
					if (pVar->IsValidTest())
					{
						anyValid = true;
						cnt++;
					}
					else
					{
						allValid = false;
						break; //stevev - see notes below for Vibhor 13apr06
							   //        : Added, We should break as soon
							   //as we get the first invalid in request
								// sjv <reduces re-entry to resolve items/validity>
					}
				}// else do not evaluate response code and dev status
			}
			//else we can do nothing
		}// next data item

/* march of '06 - Wally (at vibhor's prompting) changed his mind on the rules
		Now all must be valid in the request packet for read AND write.
		This change makes the different packet validity states superflous..................
		// returns false for ALL invalid, -1 for ALL valid,else cnt
		if (pReqLst->RslvdDataItemList.size() <= 0 )// no data 
		{
			cnt = -1;
		}
		else
		if (allValid)
		{
			cnt = -1;
		}
		else
		if (! anyValid)
		{
			cnt = 0;// not all && not any then none valid
		}
		//else leave it alone
************** end of removal 13apr06 *************/
		// stevev 06jun07 - Made Lenient mode allow writes with invalids so:
		if ( (pReqLst->RslvdDataItemList.size() <= 0) || allValid )// no data or all OK
		{
			cnt = -1;
		}
		else // allValid is F
#ifdef VALIDY_RULE_LENIENT
		if (! anyValid)// stevev 06jun07 - see above
#endif
		{
			cnt =  0;
			// return like None ALL invalid (some invalid is a don't-care to the outside world)
		}
		// else - return cnt of valids // stevev 06jun07
/***** end add 13apr06 - see notes above **********/
	}
	else
	{// failed to get a list
		cnt = 0; // all invalid
	}
	if (IownTrans)
	{
		delete pReqLst;
	}
	return cnt;
}

// returns false for ALL invalid, -1 for ALL valid,else cnt
int hCtransaction::anyValidInRep(resolvedTrans_t* pRepLst) 
{
	RETURNCODE rc = SUCCESS;
	transInfo_t     ti;
	bool allValid, anyValid;
	bool IownTrans = false;
	int  itm = 0, cnt = 0; 

	if (pRepLst == NULL)
	{
		pRepLst = new resolvedTrans_t(devHndl());
		if (pRepLst != NULL)// memory leak repair - as per vibhor 13apr06
		{
			IownTrans = true;
		}
		ti = getTransREPInfo(pRepLst);// resolve all the conditionals
		if ( ti.Number < 0 || pRepLst == NULL) // failure
		{
			rc =  FAILURE;
		}
		else
		{
			rc = SUCCESS;
		}
	}
	
	if ( rc == SUCCESS )
	{
		hCVar* pVar = NULL;
		allValid = true;
		anyValid = false;

		// scan the resolved list constructing each data item 
		//		for each response data item
		for ( ddbItemList_t::iterator iT = pRepLst->dataPtrList.begin();
/* stevev 14ap06 - as per vibhor...no impact
								  iT < pRepLst->dataPtrList.end() && rc == SUCCESS; iT++)
*/								  iT != pRepLst->dataPtrList.end() ; iT++)
		{// iT is a ptr 2 a ptr 2 a hCitemBase	
			pVar = (hCVar*)(*iT);
			if ( pVar != NULL )
			{	
				itm = pVar->getID();
				if ( itm < 150 || itm > 152) // stevev 3/29/04 changed as per prasad S.
				{
					if (pVar->IsValidTest())
					{
						anyValid = true;
						cnt++;
						// stevev 22feb07:: it matters in the reply
						// break; //Vibhor 180105: Added, We should break as soon
							   //as we get the first valid in reply
								// sjv <reduces re-entry to resolve items/validity>
					}
					else
					{
						allValid = false;
					}
				}// else do not evaluate response code and dev status
			}
			//else we can do nothing
		}// next data item

		// returns false for ALL invalid, -1 for ALL valid,else cnt
		if (pRepLst->RslvdDataItemList.size() <= 2 )// no data (respCd & staus don't count)
		{
			cnt = -1;// ALL valid
		}
		else// something in the list
		if (allValid)
		{
			cnt = -1;// ALL valid
		}
		else// something in the list && one or more invalid
		if (! anyValid)
		{
			cnt = 0;// all invalid.....not all && not any then none valid
		}
		//else leave it alone - some valid and some not count of valids
	}
	if (IownTrans)
	{
		delete pRepLst;
	}
	return cnt;

}

int  hCtransaction::weigh(bool isRead, indexUseList_t& useIdx) 
{ 
	int retInt = 0;

#ifdef MEM_DEBUG
	CMemoryState oldMemState, newMemState, diffMemState;
	oldMemState.Checkpoint();
#endif


	if ( pDispatch == NULL )
	{
		LOGIT(CERR_LOG,L"ERROR: dispatch/function null in hCTransaction:weigh.\n");
		retInt = -1;
	}
	else
	{
		retInt = ( (pDispatch->*fpCalcWgt)(this, useIdx, isRead) );
	}

#ifdef MEM_DEBUG
	newMemState.Checkpoint();
	if( diffMemState.Difference( oldMemState, newMemState ) )
	{
		LOGIT(CLOG_LOG,"Memory leaked! while weighing.\n" );
//		oldMemState.DumpAllObjectsSince();
		if (diffMemState.m_lSizes[1] > 100000 )
		{
			diffMemState.DumpStatistics();
		}
	}
#endif

	return retInt;
};

// index optimization
RETURNCODE hCtransaction::BuildIndexMap(void)
{
	RETURNCODE rc = SUCCESS;
	
	hCdataitemList  RslvdReplyItemList(devHndl());
	hCdataitemList  RslvdReqstItemList(devHndl());
	hCcommandDescriptor cmdInfo;
	cmdOperationType_t ourCmdType = cmdOpNone;

	itemIDlist_t    ReqstIdxIDList;
	int  indexCnt = 0; // mainly for debugging...

	if ( pCmd != NULL )
	{
		ourCmdType =	pCmd->getOperation();
	}
	else
	{
		LOGIT(CLOG_LOG|CERR_LOG,"Programming issue: no command pointer in transaction.\n");
		return FAILURE;
	}
	if ( ourCmdType != cmdOpRead && ourCmdType != cmdOpWrite )
	{	return SUCCESS; // we never weigh cmd-cmds nor unknown types
	}// else continue

	int cmdNo = pCmd->getCmdNumber();// for debug messages and breakpoints
	//transNum;

	LOGIF(LOGP_WEIGHT)(CLOG_LOG,
			"+=+=+=+=+=+ Index Map for Command %3d Transaction %2d +=+=+=+=+\n",cmdNo,transNum);

	indexMap.clear(); // in case something has happened, start over
	
	rc = getReqstDIlist(&RslvdReqstItemList);// both use request
	RslvdReplyItemList.clear();// this is the else condition in next if stmt

	if ( ourCmdType == cmdOpRead )
	{
		rc|= getReplyDIlist(&RslvdReplyItemList);
	}//	else ( it has to be cmdOpWrite ), leave reply empty
	if ( rc != SUCCESS )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,"ERROR: getReqstList Failed in BuildIndexMap.\n");
		return FAILURE;	// abort execution
	}
	
	/* extract the index and info subset of items into ReqstIdxIDList */	
	itemID_t thisIndexID;

	for (hCdataitemList::iterator  iT = RslvdReqstItemList.begin(); 
								   iT < RslvdReqstItemList.end()  ;  iT++)
	{
		hCVar* pV = NULL;
		hCdataItem *pDI = (hCdataItem*)&(*iT);
		if ((pDI->flags & cmdDataItemFlg_indexNinfo) == cmdDataItemFlg_indexNinfo)
		{// we only do the index&info indexes
			if ( pDI->ref.isConst() )
			{
				pV = pDI->getVarPtr();// index has to be a var
			}
			else// - an index'd ref to an index :-( won't even try to handle this one
			{	
				LOGIT(CERR_LOG|CLOG_LOG,"+=+=+ Index reference in Command %d is NOT constantly"
					" variable and will not be used!\n", cmdNo); 
			}

			// only locals would be valid at this early stage
			if (pV != NULL && pV->IsLocal() && pV->IsValid())
			{	
				thisIndexID = pV->getID();
				ReqstIdxIDList.push_back(thisIndexID);
			}
		}
		// else, we don't care about 'em here...go to next data item
	}// next raw request data item
	indexCnt = ReqstIdxIDList.size();
	LOGIF(LOGP_WEIGHT)(CLOG_LOG,"+=+=+ has %d valid local index|info items\n", indexCnt);

	// get the list of all variables in the device
	CVarList* pVarList = (CVarList*)devPtr()->getListPtr(iT_Variable);
	int indexValue = -1;

	if ( pVarList == NULL ) return FAILURE;// can't do much with no variables in the dd

	for (itemIDlistIT_t IIT = ReqstIdxIDList.begin(); IIT != ReqstIdxIDList.end(); ++IIT)
	{// for each request-packet-index
		thisIndexID = *((itemID_t*)&(*IIT));

		for(CVarList::iterator VLIT = pVarList->begin(); VLIT < pVarList->end();   VLIT++)
		{// ptr 2a ptr 2a hCVar ---- for each device variable
			hCVar* pVar = (hCVar*) (*VLIT);
#ifdef _DEBUG
			itemID_t thisItmID = 0;
			if (pVar) // null on constant in packet
				thisItmID = pVar->getID();
#endif

// we still have to deal with device-index only commands...

			//if (pVar == NULL || ! pVar->IsValid())
			//{// skip non-existent and invalid variables
			// it could become valid and we have to weigh it, filter goes there, not here
			if (pVar == NULL)
			{
				continue;// next variable
			}
			if ( ourCmdType == cmdOpRead )
			{
				cmdInfo = pVar->getRdCmd(cmdNo, transNum, thisIndexID);
			}
			else // isWrite
			{
				cmdInfo = pVar->getWrCmd(cmdNo, transNum, thisIndexID);
			}
			if ((cmdInfo.cmdNumber & 0xffff)!= 0xffff && (cmdInfo.transNumb & 0xffff)!= 0xffff)
			{// we found one
				// modified 31jan14 to deal with entire list
				idxUseIT    itIUL;
				hIndexUse_t *pIUL;
				indexUseList_t locLst;
				for (itIUL = cmdInfo.idxList.begin(); itIUL != cmdInfo.idxList.end(); ++itIUL)
				{
					pIUL = &(*itIUL);
					if (pIUL->indexSymID == thisIndexID )
					{// we have a local index (possibly a device index too)
						locLst.push_back(*pIUL);//localindex+deviceIndex that resolves this var
					}
				}
		//mk list		hIndexUse_t activeIndexANDvalue = cmdInfo.indexValue(thisIndexID);
		//	one  	indexValue = (int)activeIndexANDvalue.indexDispValue;

		//		if ( indexValue >= 0 )
		//		{// we have a command/transaction/indexID/Value resolving a variable
		//			indexMap.appendUnique(activeIndexANDvalue, pVar);

				if ( locLst.size() > 0 )
				{// we have a cmd,trans, indexID, value + devIndx.ID & value resolving a Var
					indexMap.appendUnique(locLst, pVar);
					if LOGTHIS(LOGP_WEIGHT) 
					{
						hIndexUse_t *py;
						for (idxUseIT iy = locLst.begin();iy != locLst.end();++iy)
						{
							py = (hIndexUse_t *)&(*iy);
							LOGIT(CLOG_LOG,"+=+=+ 0x%04x [%d] resolves  %s",py->indexSymID,
										(int)py->indexDispValue,	pVar->getName().c_str() );
						
							if (py->devIdxSymID != 0)
							{
								LOGIT(CLOG_LOG," with 0x%04x being %d\n",
														py->devIdxSymID,py->devVarRequired);
							}
							else
							{
								LOGIT(CLOG_LOG,"\n");
							}
						}
					}// end logif...
				}
				// else there was no index found or it has a bad value, continue with next var
				else
				{
					LOGIF(LOGP_WEIGHT)(CLOG_LOG,"+=+=+ 0x%04x failed to match for %s\n",
														thisIndexID,pVar->getName().c_str() );
				}
			}
		}// next variable in the device
	}// next request-packet-index
	if ( LOGTHIS( LOGP_WEIGHT ) )
	{
		if ( indexMap.size() == 0 )
		{
			LOGIT(CLOG_LOG,"+=+=+ has an empty Index Map\n");
		}
		indexMap.clogSelf();
		LOGIT(CLOG_LOG,"+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n");
	}
	return rc;
}// end of 	BuildIndexMap(void);


int hCtransaction::getRespCodes(hCRespCodeList& rcList) // resolve and fill return list
{// append transaction level response codes into list (uniquely)
	 
	RETURNCODE rc = SUCCESS;//hCtransRespCodes::hCRespCodeBase
							//					hCRespCodeBase::hCcondList<hCRespCodeList>
	hCRespCodeList tmpList(devHndl());
	rc = respCodes.resolveCond(&tmpList);
	if ( rc != SUCCESS )
	{// may return error on empty list or conditional?????
		tmpList.destroy();
		LOGIT(CERR_LOG,L"ERROR: transaction response code conditional failure.\n");
		return rc;
	}
	rcList.append(&tmpList);
	tmpList.destroy();

	return SUCCESS;
};

hCIndexValue2Variable* hCtransaction::getMap4Index(itemID_t indexID)
{
	hCIndexValue2Variable* pRet = NULL;
	IndexID2ValueVarIT_t   mapIT;
	if (indexID > 0)
	{
		mapIT = indexMap.find(indexID);
		if (mapIT != indexMap.end() )
		{
			pRet = (hCIndexValue2Variable*) &(mapIT->second);
			pRet->setID(indexID);
		}// else - not found: return null
	}// else - zero parameter: return null
	return pRet; // this memory is not yours, do not delete
}

#ifdef XMTR
RETURNCODE hCtransaction::getActionPtrs(ddbItemList_t& riL)		// get those *&^%$# items
{	//return RcvActList.getItemPtrs(riL);   }	
	RETURNCODE rc = 0;
	hCreferenceList  tmpList(devHndl());
	if ( (rc = RcvActList.resolveCond(&tmpList)) == SUCCESS )
	{
		rc = tmpList.getItemPtrs(riL);
	}
	return rc;
}

RETURNCODE hCtransaction::doRcvActs(void)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient    retVal;
	methodCallList_t methodList;

	//rc = getMethodList(varAttrPostRequestAct,methodList);
	ddbItemList_t        localItmList;
	hCmethodCall         wrkMthCall;
	itemType_t			 iTy;
	methodCallSource_t   srcTyp = msrc_CMD_ACT; 

	rc = getActionPtrs(localItmList);
	if (rc == SUCCESS && localItmList.size() > 0)
	{//for each in the list
		for (ddbItemList_t::iterator iT = localItmList.begin();iT<localItmList.end();iT++)
		{//iT isa ptr 2a ptr 2a hCitemBase
			iTy    = (*iT)->getIType();
			if ( iTy != iT_Method)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: non-Method type in a Rcv action list.\n");
			}
			else
			{// we be good
			//  fill a method call with varID, msrc_ACTION,methodID  
				wrkMthCall.methodID  = (*iT)->getID();
				wrkMthCall.source    = srcTyp;	// sjv 06mar07 - usually:  msrc_ACTION;
				wrkMthCall.paramList.push_back(retVal);
				methodList.push_back(wrkMthCall);
				wrkMthCall.clear();
			}
		}// next method
	}// else return the error


	if (rc == SUCCESS && methodList.size() > 0 )
	{
		rc = devPtr()->executeMethod(methodList,retVal);
	}
	else
	{
		/* normal occurance - do not log **/
	}

	return rc;
}
#endif

/*
	CValueVarient		  indexDispValue;
	CValueVarient		  indexRealValue;
	*/
RETURNCODE hCtransaction::restoreINFOitems(indexUseList_t& infoList)
{
	RETURNCODE rc = SUCCESS;
	hIndexUse_t *pIUType = NULL;
	hCitemBase  *pItm;

	for ( idxUseIT iutIT = infoList.begin(); iutIT != infoList.end(); ++ iutIT)
	{
		pIUType = &(*iutIT);
		if (pIUType->indexWrtStatus == 2)
		{
			rc = devPtr()->getItemBySymNumber(pIUType->indexSymID,&pItm);
			if ( rc == SUCCESS && pItm != NULL && pItm->IsVariable() )
			{
				hCVar *pV = (hCVar *)pItm;
				pV->setDispValue( pIUType->indexDispValue );
				if (pIUType->indexDispValue == pIUType->indexRealValue)
				{
					pV->ApplyIt();
				}
				else
				{// in lieu of adding a setRealValue() to all variable types
					pV->setRealValue( pIUType->indexRealValue );
				}
				pV->markItemState(pIUType->indexDataState);
			}
			// else get id error
		}
		// else skip it, it wasn't INFO
	}// next indexUse

	return rc;
}

/**********************************************************************************************
 * The transaction list container class 
 * see resolution information at the end of this file
 **********************************************************************************************
 */
hCtransactionList::hCtransactionList(DevInfcHandle_t h):
													hCobject(h),resolutionHandle(0),pCmd(NULL)
{  // invalid resHndl as starting point will force a resolution the first time
};

hCtransactionList& hCtransactionList::operator=(const hCtransactionList& src)
{
	TransList_t::const_iterator iT;
	hCtransaction* pTrans;

	resolutionHandle = src.resolutionHandle;
	pCmd             = src.pCmd;
	pDispatcher      = src.pDispatcher;

	for(iT = src.begin(); iT != src.end(); ++iT)
	{	pTrans = const_cast<hCtransaction*>(&(*iT));
		push_back( *pTrans );// will make a copy
	}
	return *this;
}

RETURNCODE hCtransactionList::setEqualTo( AtransactionL_t* pAtransList )
{
	RETURNCODE rc = FAILURE;

	hCtransaction  wrkTrans(devHndl(), pCmd);

	FOR_p_iT(AtransactionL_t, pAtransList)
	{// iT is a ptr2 aCtransaction
//stevev 15oct09 - works in VS6 too...#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7  >>
//                 error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc
		if ( wrkTrans.setEqualTo( &(*iT) ) == SUCCESS)
//#else
//		if ( wrkTrans.setEqualTo(iT) == SUCCESS )
//#endif
		{
			push_back(wrkTrans);
		}
		else// just fall through to loop for next
		{
			LOGIT(CERR_LOG,L"WARNING: Transaction SetEqual failed on contruction.\n");
		}
		wrkTrans.clear();
		wrkTrans.setCmdPtr(pCmd);
	}// next
	if ( size() > 0 )
	{
		rc = SUCCESS;
	}
	// else leave it failure

	return rc;
}


/* modified 11-4-03 to use tNumber = -1 to get the first transaction number*/
/* there is no requirement for the transactions to start at zero           */
/* modified 03-16-04 - stevev - there is no relation between list size and number */
hCtransaction* hCtransactionList::getTransactionByNumber(int tNumber)
{
	int tlsz = size();
	hCtransaction* pRetPtr = NULL;

	if ( tlsz > 0 /*&& tNumber <= tlsz */) // transaction numbers start at 0?
	{
		if ( tNumber >= 0 )
		{
			for(TransList_t::iterator iT = begin(); iT < end(); iT++)
			{// iT = ptr 2 hCtransaction
				if (iT->getTransNum() == tNumber)
				{
//works for vs6 #if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
					pRetPtr = &(*iT);
//#else
//					pRetPtr = iT;
//#endif
					break; // out of for...
				}
			}
		}
		else // get lowest transaction number
		{
			tNumber = 255;
			for(TransList_t::iterator iT = begin(); iT < end(); iT++)
			{// iT = ptr 2 hCtransaction
				if (iT->getTransNum() < (ulong)tNumber) 
				{
					tNumber = iT->getTransNum();
//works for vs6 #if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
					pRetPtr = &(*iT);
//#else
//					pRetPtr = iT;
//#endif
				}
			}
		}
		if (pRetPtr == NULL)
		{
			LOGIT(CERR_LOG,L"ERROR: Transaction number %d was not found in %d transactions.\n",
																				tNumber,tlsz);
#ifdef _DEBUG
			LOGIT(CERR_LOG,L"       ");
			for(TransList_t::iterator iT = begin(); iT < end(); iT++)
			{// iT = ptr 2 hCtransaction
				if ( iT != begin() )
					LOGIT(CERR_LOG,L", ");
				LOGIT(CERR_LOG,L"%d",iT->getTransNum());
			}
			LOGIT(CERR_LOG,L"\n");
#endif
		}
	}
	else
	{
//		cerr< <"ERROR: Transaction number "< < tNumber
//								< < " out of range for " < < tlsz < < " transactions."< <endl;
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Empty transaction list.\n");
	}
	return pRetPtr;
}

// NOTICE THAT THIS IS ONLY USED FOR BURST-MODE PACKETS
// returns a negative response code on error
//
int hCtransactionList::getTransNumbByReply  (BYTE datLen, BYTE* pData, int skipNum)
{	
	return (getTransNumbByDataList(datLen,pData,skipNum,false));
}

// NOTICE THAT THIS IS ONLY USED IN DEVICE SIMULATORS (Xmtr-DD)
// returns a negative response code on error
// see notebook discussion of transaction matching rules (p33 - from 4/25/03)
//                        data length and data from a REQUEST packet for this command
//
int hCtransactionList::getTransNumbByRequest(BYTE datLen, BYTE* pData, int skipNum)
{
	return (getTransNumbByDataList(datLen,pData,skipNum,true));
} 

//  COMMON CODE for two above     getTransNumbByDataList()
//
// Summary of functionality
//		shorthand abbreviations: TRqPkt == Transaction's Request Packet
//								 InRqPkt== Incoming(to xmtr) Request Packet
//
/* stevev 3/23/04 - modified */
// When ALL transactions have a constant BYTE defines as the first byte of their request pkts
//		AND That constant byte value matches the first data byte in the InRqPkt
/* to */
// When more than one transaction
//		AND transaction request packet has one or more constants
//		AND all request constant byte values matche the data byte(s) in the InRqPkt
//		AND The packet size of the InRqPkt is >= the size of the TRqPkt
//		THEN use this transaction
//	ELSE
//		Find the first transaction whose size exactly equals the InRqPkt
//		If found 
//		THEN   Use that transaction
//	ELSE ( not found )
//		If ALL of transaction's request packet sizes are LARGER than the InRqPkt
//		THEN	return - rc_TooFewBytes error code
//	ELSE ( there are one or more TRqPkt's which is smaller than the  InRqPkt )
//		Use the First closest match to the size of the InRqPk
//

int hCtransactionList::getTransNumbByDataList
									   (BYTE datLen, BYTE* pData, int skipNum, bool useRequest)
{
	transInfoList_t tiList;
	hCtransaction::transInfo_t     wrkTinfo;

	if (size() == 1 )
	{
		return at(0).getTransNum();
	}
	int transNumber    = -1;
	int NotConst       =  0; // flag byte
	int transSizeMatch = -1; // flag byte
	hCtransBase::resolvedTrans_t* pDIList = new hCtransBase::resolvedTrans_t(devHndl());

	// for each transaction,        filter as we go
	for (TransList_t::iterator iT = begin(); iT < end(); iT++)
	{// iT is ptr 2 hCtransaction
		if (useRequest)
		{
			wrkTinfo = iT->getTransREQInfo(pDIList);
		}
		else //useReply
		{
			wrkTinfo = iT->getTransREPInfo(pDIList);
		}
		tiList.push_back(wrkTinfo);
		if ( wrkTinfo.Number == skipNum )
		{
			pDIList->clear();
			continue;
		}
		// filtering...-1 means NOT constant, else value
		// filtering...0 means NO constants, else value
		if ( pDIList->constCnt <= 0 )
		{
			NotConst += 1; //true if any are not constant
			if (wrkTinfo.size == datLen && transSizeMatch < 0 ) //matches & not matched yet
			{				
				transNumber = transSizeMatch = wrkTinfo.Number;
			}// else not exact - do nothing
		}
		else // we have consts, matches and not a match already
			// 19sep14 - added the NotConst test so we don't always go with the zero  trans
		if (/* wrkTinfo.firstReqByte == pData[0] &&3/24/04*/ transNumber < 0 || NotConst == 1)
		{
			/*  we must to handle several const in a pkt */
			/*  note that the Binary file format does not support masked consts  */
			// 3/23/04 - Serious rework to compare all consts in transaction
			transSizeMatch = -1;
			int k = 0;// location in pkt
			int y = 0;// location in ptr list
			int j = pDIList->constCnt; // track to see if we match all of 'em
			iTdataItmLst iDL;
			for ( iDL = pDIList->RslvdDataItemList.begin();
				  iDL < pDIList->RslvdDataItemList.end() && // not end of dataitem list
					  k < datLen         &&					// not end of packet
					  transSizeMatch < 0 &&					// not mis-match
					  j > 0;								// have not matched 'em all yet
				  iDL++)
			{// ptr 2a hCdataItem
				if ( iDL->type == cmdDataConst )
				{// appear to be required UINT8 size
					if ( iDL->iconst != pData[k] )
					{// it is a constant mis-match
						transSizeMatch = 0;// bail out- no use in continueing this transaction
					}
					else //continue to match constants
					{
						k++;
						j--;// see if we matched 'em all
					}
				}
				else 
				if ( iDL->type == cmdDataFloat )
				{//AlmostEqual2sComplement( f, f, 4)
					k += FLT_CONST_SIZE;// move the packet ptr - we don't match float consts
				}
				else
				{// it's a ref
					k += pDIList->dataPtrList[y++]->getSize();// inc pkt by var size
				}
			}// next
			if (transSizeMatch < 0 && j <= 0)//matched 'em all 
			{
				transNumber = transSizeMatch = wrkTinfo.Number;
			}
			/* stevev 20jul05 - we have to undo the setting to zero to bail out */
			else 
			if (transSizeMatch == 0)
			{
				transSizeMatch = -1;
			}
			// 3/23/04 - end of serious rework
		}// else no const matches, do nothing
		pDIList->clear();  // for next round
	}// next transaction

	//VMKP 260304
	pDIList->clear();
	delete pDIList; // don't need it any more
	pDIList = NULL;

	if ( (! NotConst) && transNumber >= 0 )
	{
		;// done - break out of selection
	}
	else
	if (transSizeMatch >= 0)
	{
		;// done - break out of selection
	}
	else // we try and make the closest match
	{
		transNumber    = -1;
		transSizeMatch =  10000; // used here as min differential size
		int s; // temporary wrk var

		// scan for first closest
		for (transInfoList_t::iterator pTI = tiList.begin(); pTI<tiList.end();pTI++)
		{// pTI is a ptr 2 a hCtransaction::transInfo_t
			s = datLen - pTI->size;
			if ( s >= 0 && s < transSizeMatch)// dd trans size smaller than actual, & smallest
			{
				transSizeMatch = s;
				transNumber    = pTI->Number;
			}// else skip it
		}
		if ( transNumber < 0 )
		{
			 transNumber = 0 - (int)rc_TooFewBytes;// response code error return
		}
	}

	return transNumber;
}




/**********************************************************************************************
 * The command class 
 * 
 **********************************************************************************************
 */
hCcommand::hCcommand(DevInfcHandle_t h, aCitemBase* paItemBase) 
		: hCitemBase(h, paItemBase), condCmdNum(h), condCmdOp(h), transList(h), cmdRespCodes(h)
{// base does Validiity, Label, Help - of which this item has none -
	transList.setCmdPtr(this);
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(cmdAttrNumber); 
	if (aB != NULL)
	{
		condCmdNum.setEqualTo( &(((aCattrCondLong*)aB)->condLong)  );
	}
	else // no command number - this is useless
	{
		LOGIT(CERR_LOG,L"ERROR: No command number attribute available from the template.\n");
		return; // abort
	}
#ifdef _DEBUG
	int c = getCmdNumber();
#endif
	
	aB = paItemBase->getaAttr(cmdAttrOperation); 
	if (aB != NULL)
	{
		condCmdOp.setEqualTo( &(((aCattrCondLong*)aB)->condLong)  ); 
	}
	else // no operation - we may need to set this to default RdWr
	{
		LOGIT(CERR_LOG,L"WARNING: No command operation attribute available from template.\n");
		// we may need to do a setPayload on the conditional (see conditional.h) to get dflt
	}
	
	
	aB = paItemBase->getaAttr(cmdAttrTransaction); 
	if (aB != NULL)
	{
		transList.setEqualTo( &(((aCattrCmdTrans*)aB)->transactionList)  ); 
	}
	else // no transactions - this is useless
	{
		LOGIT(CERR_LOG,L"ERROR: No command transaction attribute available from template.\n");
		return; // nothing we can do
	}
	
	
	aB = paItemBase->getaAttr(cmdAttrRespCodes); 
	if (aB != NULL)
	{
		cmdRespCodes.setEqualTo( &(((aCattrCmdRspCd*)aB)->respCodes)  ); 
	}
	//else / / no response codes...a normal occurance
	return;
};

/* from-typedef constructor */
hCcommand::hCcommand(hCcommand*    pSrc,  itemIdentity_t newID) : hCitemBase(pSrc,newID),
	condCmdNum(pSrc->condCmdNum),condCmdOp(pSrc->condCmdOp),
	transList(pSrc->transList),cmdRespCodes(pSrc->cmdRespCodes)
{
	LOGIT(CERR_LOG,L"**** Implement command's typedef constructor.!*******\n");
}


hCcommand::~hCcommand()
{
	destroy();
}


RETURNCODE hCcommand::destroy()
{
	RETURNCODE rc;
#ifdef _DEBUG
	if (transList.size())// if we haven't been destroyed before...
	int c = getCmdNumber();
#endif
	rc = condCmdNum.destroy();
	rc |= condCmdOp.destroy();
	rc |= transList.destroy();
	rc |= cmdRespCodes.destroy();
	rc |= hCitemBase::destroy();
	return rc;
}

void hCcommand::insertWgtVisitor(hCcmdDispatcher* pDispatch) // all transactions
{// for each in transaction list: setVisitor(fpWgtCalcs);
	transList.setWgtVisitor(pDispatch);
}


hCattrBase*   hCcommand::newHCattr(aCattrBase* pACattr)  // builder 
{
	LOGIT(CERR_LOG,L"    - hCcommand's newHCattr is not implemented.\n");
	return NULL; // an error
}

int hCcommand::getCmdNumber(void)// -1 on error
{ 
	//int r = 0;
	RETURNCODE rc = SUCCESS;
	//hCddlLong* pDLng = condCmdNum.resolveCond();
	hCddlLong dl(devHndl());
	rc = condCmdNum.resolveCond(&dl);
	//if ( pDLng == NULL )
	if ( rc != SUCCESS )
	{
		LOGIT(CERR_LOG,L"ERROR: Command number did not resolve.\n");
		return -1;
	}
	else
	{
		return (int)(dl.getDdlLong());// command numbers are limited in size
	}
}


int        hCcommand::getRespCodes(hCRespCodeList& rcList) // resolve and fill return list
{// copy command level response codes into list
	//hCRespCodeList* pRespCdLst = cmdRespCodes.resolveCond(); 
	RETURNCODE rc = SUCCESS;
	hCitemBase* pItm;
	hCEnum*  pInt;
	hCenumList rcLst(devHndl());
	vector<hCenumDesc>::iterator ied;
	hCrespCode wrkingRC(devHndl());
	hCRespCodeList localRCList(devHndl());

	// get the variable first then overlay the command's;  transaction will overlay that
	rc = devPtr()->getItemBySymNumber(RESPONSECODE_SYMID,&pItm);
	if ( rc == SUCCESS && pItm != NULL )
	{// do response code
		pInt = ((hCEnum*)((hCVar*)pItm));
		rc = pInt->procureList( rcLst );
		if ( rc == SUCCESS && rcLst.size() > 0 )
		{
			for (ied = rcLst.begin(); ied != rcLst.end();++ied)
			{// ptr2a hCenumDesc
//works for vs6 #if _MSC_VER >= 1300  
// HOMZ-port to 2003,VS7>> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc
				wrkingRC = *((hCenumDesc*)( &(*ied) ) );
//#else
//				wrkingRC = *((hCenumDesc*)ied);
//#endif

				rcList.push_back(wrkingRC);
				wrkingRC.clear();
			}
		}
	}

	//hCRespCodeList* pRespCdLst = cmdRespCodes.resolveCond();
	rc = cmdRespCodes.resolveCond(&localRCList);
	if ( rc == SUCCESS && localRCList.size() > 0 )
	{
		rcList.append(& localRCList); // appends uniquely with insertion overwritting existing
		localRCList.destroy();
	}

	return SUCCESS;
}

RETURNCODE  hCcommand::invalidateReply( int transNumber) //clear state on erroneous request
{
	RETURNCODE rc = FAILURE;
	hCtransaction*     pTrans = getTransactionByNumber(transNumber);
	if (pTrans != NULL)
	{
		rc = pTrans->invalidateReply();
	}
	return rc;
}

// stevev 2/26/04 - do not re-resolve the transaction; use list
RETURNCODE  hCcommand::markReplyPending(varPtrList_t& varPtrList)
{
	RETURNCODE rc = SUCCESS;
	FOR_iT(varPtrList_t, varPtrList)
	{// iT isa ptr 2a ptr 2a hCVar
		rc |= (*iT)->markItemState(IDS_PENDING);;
	}
	return rc;
}
// stevev 2/19/04 - we don't mark till ready to send
RETURNCODE  hCcommand::markReplyPending(int transNumber)
{
	RETURNCODE rc = SUCCESS;
	hCtransaction*  pTrans = getTransactionByNumber(transNumber);
	hCtransBase::resolvedTrans_t* pRepLst = NULL;

	if (pTrans == NULL)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: could not get transaction %d\n",transNumber);
		return APP_PROGRAMMER_ERROR;
	}

	rc = pTrans->markReplyPending (pRepLst);
	return rc;
}
// end stevev 2/19/04 */

// the return code is critical - if not success, DO NOT send
RETURNCODE hCcommand::BuildRequest(hPkt* pPkt, hMsgCycle_t* pMsgCy )
{
	RETURNCODE rc = FAILURE;
	bool anyValid      = false;
	int  result        = 0;
	hCtransBase::resolvedTrans_t* pRepLst = new hCtransBase::resolvedTrans_t(devHndl());

	cmdOperationType_t cmdOp  = getOperation();
	hCtransaction*     pTrans = getTransactionByNumber(pMsgCy->transNum);

	pMsgCy->cmdType  = cmdOp;

//	append command level into to data buffer << none - comm interface handles this>>
	pPkt->clear();
	pPkt->cmdNum = getCmdNumber();

	if (pTrans == NULL )
	{
		LOGIT(CERR_LOG,L"ERROR: BuildRequest did not find transaction %d\n",pMsgCy->transNum);
		return FAILURE;
	}
	// else - continue

	/* stevev 2/18/04 - all build logic now in generateRequest */
	/* stevev 02nov05 - brought common code to one place       ---- start */
	// 02nov05 -- generateRequestPkt now executes pre-write actions !!!!!!!!!
	rc = pTrans->generateRequestPkt( pPkt, pMsgCy, pRepLst);
	// handle errors
	
// stevev 15sep15 - incorrect action as per wap and myself(ie validity rules are not enforced inside a method)...	if ( rc == SUCCESS )// we can continue < all are valid in request >
	if ( rc == SUCCESS && pMsgCy->m_cmdOrginator != co_METHOD )// we can continue < all are valid in request >
	{	// returns false for ALL invalid, -1 for ALL valid,else cnt
		if ( ( result = pTrans->anyValidInRep(pRepLst) ) == 0 )// ALL invalid
		{
			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: ");
			if ( cmdOp == cmdOpRead )     LOGIT(CERR_LOG|CLOG_LOG,L"read");
			if ( cmdOp == cmdOpWrite)     LOGIT(CERR_LOG|CLOG_LOG,L"write");//cmdcmd should never happen
			LOGIT(CERR_LOG|CLOG_LOG,L" transaction has all invalid items in the response.\n");
			rc = APP_CMD_VALIDITY_RULE_ERR;
		}
		//else do not mark response pending till send time - return success
	}
	//else - let it return this code
	/*   end common code addition 02nov05 ----------------------------end */

/***** 02nov05 consolodated into common code section above ********** (will be removed later)
*	if ( cmdOp == cmdOpRead )//	if Read Cmd	
*	{
*		rc = pTrans->generateRequestPkt( pPkt, pMsgCy, pRepLst);
*		// handle errors
*		if ( rc == SUCCESS )// we can continue < all are valid in request >
*		{
*			// returns false for ALL invalid, -1 for ALL valid,else cnt
*			if (result = pTrans->anyValidInRep(pRepLst))// is true
*			{// mark response pending
*				/ * stevev 2/19/04 - this must be delayed till send time
*				rc = pTrans->markReplyPending (pRepLst);
*				end stevev 2/19/04 * /
*				rc = SUCCESS;
*			}
*			else
*			{
*			cerr < < "ERROR: read transaction has all invalid items in the response."< <endl;
*				rc = APP_CMD_VALIDITY_RULE_ERR;
*			}
*		}
*		//else - let it return this code
*	}
*	else//	/elif  Write Cmd
*	if (cmdOp == cmdOpWrite)
*	{//WAP interpretation: ANY of Request Pkt AND ANY of Reply Pkt isValid() then OK to send
*		rc = pTrans->generateRequestPkt( pPkt, pMsgCy, pRepLst);
*		// handle errors
*		if ( rc == SUCCESS )// we can continue < one or more valid in request >
*		{
*			// returns false for ALL invalid, -1 for ALL valid,else cnt
*			if (result = pTrans->anyValidInRep(pRepLst))// is true
*			{
*				/ * stevev 2/19/04 - this must be delayed till send time
*				rc = pTrans->markReplyPending (pRepLst);
*				end stevev 2/19/04 * /
*				rc = SUCCESS;
*			}
*			else
*			{
*			cerr < < "ERROR: write transaction has all invalid items in the response."< < endl;
*				rc = APP_CMD_VALIDITY_RULE_ERR;
*			}
*		}
*	}
*	else//	/elif  Cmd Cmd
*	if (cmdOp == cmdOpCmdCmd)
*	{
*		rc = pTrans->generateRequestPkt( pPkt, pMsgCy, pRepLst);
*		// handle errors
*		if ( rc == SUCCESS )// we can continue < one or more valid in request >
*		{
*			// returns false for ALL invalid, -1 for ALL valid,else cnt
*			if (result = pTrans->anyValidInRep(pRepLst))// is true
*			{
*				/ * stevev 2/19/04 - this must be delayed till send time
*				rc = pTrans->markReplyPending (pRepLst);
*				end stevev 2/19/04 * /
*				rc = SUCCESS;
*			}
*			else
*			{
*			cerr < < "ERROR: write transaction has all invalid items in the response."< < endl;
*				rc = APP_CMD_VALIDITY_RULE_ERR;
*			}
*		}
*	}
*	else 
*	{
*	}
//	/endif   (read/write command (command-command doesn't do anything))
***************** end of code consolodation 2nov05 ***********/
//
//	For each transaction response code
//		Add to or Overwrite RC in the MsgCycle List
//	Next
	if (pTrans)        
	{
		pTrans->getRespCodes(pMsgCy->respCds);
	}

	hCVar*  pDIVar;
	hCdataItem* pDataItem;

	pMsgCy->transVarList.clear();
	pMsgCy->transSignature.clear();

	DEBUGLOG(CLOG_LOG,"Sig: cmd %d trans: %d:(expect %d)  ",pPkt->cmdNum, pMsgCy->transNum, pRepLst->RslvdDataItemList.size());

	int dataCnt = 0, constCnt = 0;              // traverse the REPLY packet
	for (iTdataItmLst iTdil = pRepLst->RslvdDataItemList.begin();
										iTdil < pRepLst->RslvdDataItemList.end();   iTdil++  )
	{// iTdil isa ptr2a hCdataItem
		pDataItem = &(*iTdil);// stevev - 29jun11 stop using iterator as pointer
		pDIVar = NULL;
		if (pDataItem != NULL)
		{
			pDIVar = pDataItem->getVarPtr();
			if (pDIVar != NULL)
			{	
				pMsgCy->transVarList.push_back(pDIVar);
				dataCnt++;// skip RC & St for signature possibilities
				if ( dataCnt > 2 && constCnt > 0) 
				{// we're flagging that there cannot be any more sequential constants
					constCnt = -769;
					
					// tried to break;//stevev 13apr09 -->> cnt won't stop all embedded 
					//                                      constants from going into the list
					// stevev 07may09 - I don't fully understand the above comment.  cnt < 0
					//				stops both lists from pushing the item.  It appears that
					//				I misunderstood the code in the earlier (13apr) change.  
					//				The release note says:
					//    "Fix signature match(found via walkthru) all consts were being used."
				}
				// i really don't need the reply packet...DEBUGLOG(CLOG_LOG," <byte[%d]>0x%02x",dataCnt,pDIVar->getID());
			}
			else//NULL is a non-var...record the first-found sequential constants 
			if (pDataItem->type == cmdDataConst && dataCnt >= 2 && constCnt >= 0)
			{
// Note: stevev 12feb10 - Druck had this line commented out?	
// removed - not used PAW 26/06/09 causes crash			
// but it's used in signatureMatch() in ddbCmdDispatch.cpp
				pMsgCy->transSignature.push_back((unsigned char) (pDataItem->iconst&0xff));
				constCnt++;
 
				DEBUGLOG(CLOG_LOG," [%d]0x%02x",constCnt,(int)(pDataItem->iconst&0xff));
			}
			// else its a non-contiguous constant in the packet, we don't need to save it.
			else
			{
				DEBUGLOG(CLOG_LOG,"non-contiguous constant discarded while "
																"building Request Packet.\n");
			}
		}// else discard;
		else
		{
			DEBUGLOG(CLOG_LOG,"NULL data item encountered while building Request Packet.\n");
		}
	}
	DEBUGLOG(CLOG_LOG,"\n");

	delete pRepLst;
	return rc;
}




//	cmdOpNone		 = 0,
//	cmdOpRead,
//	cmdOpWrite,
//	cmdOpCmdCmd		// 3
cmdOperationType_t hCcommand::getOperation(void)
{
	cmdOperationType_t r = cmdOpNone;
	//hCddlLong* pL;
	hCddlLong DL(devHndl());
	RETURNCODE rc = SUCCESS;

	//pL = condCmdOp.resolveCond();
	rc = condCmdOp.resolveCond(&DL);
	//if (pL != NULL)
	if ( rc == SUCCESS )
	{
		r = (cmdOperationType_t) DL.getDdlLong();
	}
	return r;
}


RETURNCODE hCcommand::getAllOperations(vector<hCddlLong*>& retLst)
{
	RETURNCODE rc = SUCCESS;
	rc = condCmdOp.aquirePayloadPtrList(retLst);
	return rc;
}

hCtransaction* hCcommand::getTransactionByNumber(int tNumber)
{
	return (transList.getTransactionByNumber(tNumber));
}

int hCcommand::getTransNumbByRequestPkt(hPkt* pReqPkt, int skipTrans)// -1 on error
{
	if ( transList.size() == 1 )
	{
		//return 0; return the only transaction number...not always zero
		return transList[0].getTransNum();
	}
	else
	{
		return (transList.getTransNumbByRequest(pReqPkt->dataCnt,pReqPkt->theData, skipTrans));
	}
}

// this is expected to used by a burst packet
int hCcommand::getTransNumbByReplyPkt (hPkt* pReqPkt, int skipTrans)// -1 on error
{
	if ( transList.size() == 1 )
	{// return the only transaction number...not always zero
		return transList[0].getTransNum();
	}
	else
	{
		return (transList.getTransNumbByReply(pReqPkt->dataCnt, pReqPkt->theData, skipTrans));
	}
}


int hCcommand::getRdTransNumbByItemID(itemID_t itemID)// -1 if not in Response Pkts
{
	// implement for host
	return -1;
}

int hCcommand::getWrTransNumbByItemID(itemID_t itemID)// -1 if not in Request  Pkts
{
	// implement for host
	return -1;
}


#ifdef ADD_DEBUG
// global flag to reference
int IsCmd = 0;
#endif
// helper function for updateVarCommandUsage()
// common code for read or write, baseWgt = starting weight, 
//			dataItmList=the data item list to process, isWrite = which list to insert it in
RETURNCODE hCcommand::updateVarCmdWeight(int baseWgt,    int transNum, 
										 hCdataitemList& dataItmList, bool isWrite)
{
	RETURNCODE          rc = SUCCESS, rb = SUCCESS;
	GroupItemList_t		localVarGroup;// a list of hCGroupItemInf
	hCcommandDescriptor	localDesc;    //int cmdNumber;	int transNumb;	int rd_wrWgt other;

	hCdataitemList::iterator rdi; 
	hCdataItem *pDItm;

	localDesc.transNumb = transNum;
	localDesc.cmdNumber = getCmdNumber();
	localDesc.rd_wrWgt  = baseWgt;//stevev 3/25/04 noticed we were missing the base weight 
							//in the first iteration stevev 29jan09 - moved from below 4 lists
	localDesc.cmdTyp    = getOperation();//to keep from looking it up all the time..

	int c = 1;// counting numbers
	for(rdi = dataItmList.begin(); rdi < dataItmList.end(); rdi++,c++)
	{//rdi is a ptr 2 hCdataItem
		pDItm = &(*rdi);
#ifdef ADD_DEBUG
if (getCmdNumber() == 150)
	IsCmd = 1;
#endif
		// stevev 06may09 - Remove INFO items as writable variables
		if ((pDItm->flags & cmdDataItemFlg_Info) != 0 )
                {
			continue;
		}
		// end stevev 06may09 & 20may15
		rc = pDItm->addAllVars(localVarGroup);
		//  appends all possible outcomes <skips cmdDataConst && cmdDataFloat>
		//  ie all variables in an array
		if ( rc != SUCCESS )
		{
			LOGIF(LOGP_DEPENDENCY)
				(CERR_LOG|CLOG_LOG,"       Command Number %d, Transaction %d 's"
				" Data Item #%d failed to resolve one or more possible values.\n",
				getCmdNumber(), transNum, c );
		}
		rb = pDItm->commandAllLists(localDesc);// stevev 30dec08 - give lists the command info
		if ( rb != SUCCESS )
		{
			LOGIF(LOGP_NOT_TOK)
				(CERR_LOG|CLOG_LOG,"       Command Number %d, Transaction %d 's Data Item "
				"number %d failed to resolve.\n",getCmdNumber(),transNum,c);
		}
#ifdef ADD_DEBUG
	IsCmd = 0;
#endif
	}// next dataitem
	DEBUGLOG(CLOG_LOG,">    Cmd: %d has %d Vars added in addAllVars.(dbg only msg)\n",
														getCmdNumber(),localVarGroup.size() );
	localDesc.idxList.clear();
	int  non_local_Index  = 0;   // valid for the entire list

	
	hCVar*           pVar     = NULL;
	hCVar*			 pIdx     = NULL;
	ExprTrailList_t  retList;// ETLiterator
	hIndexUse_t      locIdx;
	GroupItemList_t::iterator   itGIL;
	hCGroupItemInfo *pGIL;
	int grpCnt = 0; // for debugging only

	for (itGIL = localVarGroup.begin();itGIL != localVarGroup.end(); ++itGIL)
	{//itGIL is a ptr 2 hCGroupItemInfo of ONE possible var in this response
		pGIL = &(*itGIL);
		grpCnt++;
		if ( pGIL != NULL   &&   ((pVar = getVarPtrBySymNumber(pGIL->theVarID))!=NULL)  )
		{
			if (   (pVar->VariableType() == vT_Index ||
					pVar->VariableType() == vT_Enumerated)
				&& (! pVar->IsLocal() )                     )
			{
				non_local_Index = 100; // force it to be last considered
			}	// used for all vars in the command
#ifdef _DEBUG
			unsigned cmdNo = getCmdNumber();
			unsigned varID = pVar->getID();
			LOGIT(CLOG_LOG,"UVCW: command %d vargroupCnt %d variable 0x%04x (%s)::\n",
												cmdNo,grpCnt, varID, pVar->getName().c_str());
#endif
			if ( pGIL->fillExpressions(retList) > 0 ) // has non constants
			{// retList of <isVar,srcID,srcIndxVal>ExprTrailList classes
				hCexpressionTrail *pExpTrl;
				for (ETLiterator ET = retList.begin();ET < retList.end();ET++)
				{//ET is ptr to hCexpressionTrail (isVar,srcID,srcIndxVal)
					pExpTrl = &(*ET);
					pIdx = (hCVar*)getVarPtrBySymNumber(pExpTrl->srcID);
					if (pIdx != NULL)
					{
						if (pIdx->VariableType() !=vT_Index &&  
							pIdx->VariableType() !=vT_Enumerated )
						{
							DEBUGLOG(CLOG_LOG,"UVCW: command %d dependent on a non-index"
								" variable (%s).\n",getCmdNumber(),pIdx->getName().c_str() );
						}
						else  // is vT_Index or enum
						{	
																		
							if ( pIdx->IsLocal() )// assume it must be an index
							{
#ifdef _DEBUG
								if ( locIdx.indexSymID != 0 )//has a local Index & value
								{
									LOGIT(CLOG_LOG,"UVCW: has Local 0x%04x, Val: %d | wants 0x%04x, Val: %d\n",
														locIdx.indexSymID, (unsigned)locIdx.indexDispValue,
														pExpTrl->srcID,pExpTrl->srcIdxVal );
								}
#endif							
								locIdx.indexSymID     = pExpTrl->srcID;
								locIdx.indexDispValue = 
								locIdx.indexRealValue = pExpTrl->srcIdxVal;
									
								DEBUGLOG(CLOG_LOG,"UVCW: insert Local 0x%04x, Val: %d \n",
														pExpTrl->srcID,pExpTrl->srcIdxVal );
							}
							else // not local
							{// hit the weight again
								non_local_Index += 50;// for all vars
//stevev 14Sep11 - The varient clears to zero! this is INCORRECT.  Since this is NON-LOCAL
//		we need to flag it with a -1 (zero is a valid index value) to use whatever the current
//		value for the index is.
//was::>						
								//locIdx.indexDispValue.clear();
// added::>
								//locIdx.indexDispValue = (unsigned) 0xffffffff;// -1
#ifdef _DEBUG
								if ( locIdx.devIdxSymID != 0 )//has a device Index & value
								{
									LOGIT(CLOG_LOG,"UVCW:has Device 0x%04x, Val: %d | wants %x04x, Val: %d\n",
														locIdx.devIdxSymID, locIdx.devVarRequired,
														pExpTrl->srcID,     pExpTrl->srcIdxVal );
								}
#endif							
								locIdx.devIdxSymID     = pExpTrl->srcID;
								locIdx.devVarRequired  = pExpTrl->srcIdxVal;
									
								DEBUGLOG(CLOG_LOG,"UVCW:insert Device 0x%04x, Val: %d \n",
														pExpTrl->srcID,pExpTrl->srcIdxVal );
							}
						}
					}// else pIdx is NULL - skip for now
					else
					{
						DEBUGLOG(CLOG_LOG,"UVCW: command %d has a null variable in exprTrail\n"
																			,getCmdNumber() );
					}
					// stevev 20may15 - change to make every index its own entry
					localDesc.idxList.push_back(locIdx);
					locIdx.clear();
					DEBUGLOG(CLOG_LOG,"     -- push --\n");
				}// next in the expression trail
				/* stevev 20may15 - change to make every index its own entry
				localDesc.idxList.push_back(locIdx);
				locIdx.clear();
				DEBUGLOG(CLOG_LOG,"     -- push --\n");
				****/
				retList.clear();
			}// else retlist is empty, continue.
			 //                   offset for all in the list(deductive weighting)
			localDesc.rd_wrWgt -= non_local_Index;
			if ( isWrite )
			{
				pVar->addWRdesc(localDesc);
				if LOGTHIS(LOGP_DD_INFO) 
				{
					LOGIT(CLOG_LOG|COUT_LOG,"UVCW: Item 0x%04x WRIT Desc:",pGIL->theVarID);
					localDesc.dumpSelf();  
				}
			}
			else
			{
				pVar->addRDdesc(localDesc);
				if LOGTHIS(LOGP_DD_INFO) 
				{
					LOGIT(CLOG_LOG|COUT_LOG,"UVCW: Item 0x%04x READ Desc:",pGIL->theVarID);
					localDesc.dumpSelf();  
				}
			//	if (localDesc.idxList.size() > 1)
			//	{
			//		LOGIT(CLOG_LOG,"WARNING: command %d dependent on %d local indexes.\n",
			//										getCmdNumber(),localDesc.idxList.size() );
			//	}
			}
		}
		else // null in list or pointer not found for symbol
		{
			if (&(*pGIL) == NULL)// PAW added &* 03/03/09
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: resp groupItem was NULL from addAllVars"
															" in updateVarCommandUsage\n");
			}
			else // get var by pointer failed
			{
				LOGIF(LOGP_NOT_TOK)
						(CERR_LOG,"ERROR: resp groupItem's getVarPtrBySymNumber failed "
						" for Item 0x%04x (%s) in updateVarCommandUsage.\n",pGIL->theVarID,
						(pGIL->theVarPtr)?pGIL->theVarPtr->getName().c_str():"");

				hCitemBase*     pItem = NULL;

				if ( devPtr()->getItemBySymNumber(pGIL->theVarID, &pItem)
					== SUCCESS && pItem != NULL)
				{
					LOGIT(CERR_LOG,"     : is a %s item.",pItem->getTypeName());
				}
				else
				{
					LOGIT(CERR_LOG,"     : apparently does not exist in this Device.");
				}
				LOGIT(CERR_LOG,
						"(Used in command number %d (%s))\n",getCmdNumber(),getName().c_str());
			}
		}// endif null or pointer not found
		localDesc.rd_wrWgt = baseWgt;// reset the weighting for each var
		localDesc.idxList.clear();
		non_local_Index = 0;
		DEBUGLOG(CLOG_LOG,"UVCW:     <<<< next <<<<\n");
	}// next group item
	//Memleak fix: DEEPAK 290105 
	 localVarGroup.clear();
	 //END
	return rc;
}

// inserts cmd/trans# into rd/wr lists in vars used
RETURNCODE hCcommand::updateVarCommandUsage(void) 
{
	RETURNCODE rc        = SUCCESS;
	vector<hCddlLong*>   opPtrLst;
	hCtransaction* pTr   = NULL;
	cmdOperationType_t o = cmdOpNone, 
					   O = cmdOpNone;
	listOptrs2dataItemLists_t::iterator iP2DIL;
	hCitemBase* pThisVarItem = NULL;
	int tNum;
#ifdef _DEBUG
	int myCmd = getCmdNumber();
#endif
//#ifdef VALID_TEST
//	if ( getCmdNumber() == 1 )
//	{
//		return 0;
//	}
//#endif

	rc = getAllOperations(opPtrLst);
	if ( rc != SUCCESS || opPtrLst.size() == 0)
	{
		rc = APP_DD_ERROR;
	}
	else
	{
		if (opPtrLst.size() > 1)
		{
			for (vector<hCddlLong*>::iterator yT = opPtrLst.begin();
				yT < opPtrLst.end(); yT++ )
			{//yT is a ptr 2 ptr 2 DllLong
				O = (cmdOperationType_t)(*yT)->getDdlLong();
				if ( o == cmdOpNone)
				{
					LOGIT(CLOG_LOG|CERR_LOG,
										 "Command %d has NO operation type.\n",getCmdNumber());
					o = O;
				}
				else
				if ( o != O )
				{
					LOGIT(CLOG_LOG,L"WARN: more than one operation type in command %d op1="
											L" %d op2=%d\n",getCmdNumber(),(int)o,(int)O);
					o = O; // use the last found
				}
				// else same, loop
			}//next
		}
		else
		{
			o = (cmdOperationType_t)(opPtrLst[0])->getDdlLong();
		}
		// stevev 12feb09 - added cheap optimization..
		if ( o == cmdOpCmdCmd )
			return rc;// we need go no further
		tNum = 0;
		listOptrs2dataItemLists_t	listOPtrs2datItmLsts;
		hCdataitemList				listORespDataItems(devHndl()), 
									listOReqDataItems(devHndl());		
		
		int	holdBaseWgt = 0;

		for(TransList_t::iterator iTr = transList.begin(); iTr < transList.end(); iTr++)
		{// iTr = ptr 2 hCtransaction
//works for vs6 #if _MSC_VER >= 1300  
//HOMZ- port to 2003,VS7 >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..
			pTr = &(*iTr);
//#else
//			pTr = (hCtransaction*) iTr;
//#endif
			if (pTr != NULL)// not the end of the list
			{
				tNum = pTr->getTransNum();

				listOPtrs2datItmLsts.clear();
				listORespDataItems.clear();
				listOReqDataItems.clear();
				if ( o == cmdOpNone )
				{
					LOGIT(CLOG_LOG,L"Command %d has NO operation type.\n",getCmdNumber());
				}
				else
				{// it's a read || write - get both lists to get weight, use just one	
					
					// get response list
					listOPtrs2datItmLsts.clear();
					rc = pTr->getREPlistOdiLists(listOPtrs2datItmLsts);//several lists
int y = listOPtrs2datItmLsts.size();

					for (iP2DIL = listOPtrs2datItmLsts.begin(); 
													iP2DIL<listOPtrs2datItmLsts.end();iP2DIL++)
					{// iP2DIL is Ptr 2 Ptr 2 hCdataitemList
						listORespDataItems.appendUnique(*iP2DIL);//append all parts to list
					}
y = listORespDataItems.size();

					// get request list					
					listOPtrs2datItmLsts.clear();
					rc = pTr->getREQlistOdiLists(listOPtrs2datItmLsts);
y = listOPtrs2datItmLsts.size();
					for (iP2DIL = listOPtrs2datItmLsts.begin(); 
													iP2DIL<listOPtrs2datItmLsts.end();iP2DIL++)
					{// iP2DIL is Ptr 2 Ptr 2 hCdataitemList
						listOReqDataItems.appendUnique(*iP2DIL);// append parts to main list
					}
y = listOReqDataItems.size();				


					if ( o == cmdOpRead )
					{	// highest number wins:: extra data means try not to use it
						if ( getCmdNumber() == 1 )
							holdBaseWgt  = 256;// only use this as a last resort
						else
							holdBaseWgt  = 1024;// deductive weighting
						holdBaseWgt -= (listOReqDataItems.size()); //less request var cnt
									// note that indexes should be exempt of this deduction
						holdBaseWgt -= (listORespDataItems.size() - 3);//rc,stat,thisitem
						
						rc = updateVarCmdWeight(holdBaseWgt,tNum,listORespDataItems,false);
						if ( rc != SUCCESS && ! devPtr()->getPolicy().isPartOfTok )
						{
							LOGIT(CERR_LOG,"updateVarCmdWeight failed for read command\n");
						}
						// else just continue
					}
					else
					if ( o == cmdOpWrite )
					{
						holdBaseWgt  = 1024;// deductive weighting
						holdBaseWgt -= (listOReqDataItems.size()); //less request var cnt
									// note that indexes should be exempt of this deduction
						holdBaseWgt -= (listORespDataItems.size() - 3);//rc,stat,thisitem
						
						rc = updateVarCmdWeight(holdBaseWgt,tNum,listOReqDataItems,true);
						if ( rc != SUCCESS)
						{
							LOGIT(CERR_LOG,"updateVarCmdWeight failed for write command\n");
						}
						// else just continue
					}// end elseif is write
					// else none OR command-command <cmd 11,40,45,46 - do-not-dispatch>
				}// endelse - has a command type
			}
			// else trans ptr is null, end of transaction list, loop
		}// next transaction
	}
	return rc;
}

//RETURNCODE hCreference::resolveID( hCitemBase*& r2pReturnedPtr)
//ddbItemList_t is hCItemBase
RETURNCODE  hCcommand::
				getReqstData4Trans(int transNumber, hIndexUse_t& useIdx,varPtrList_t& itemList)
{
	RETURNCODE rc = FAILURE;

	hCtransaction*  thisTrans = getTransactionByNumber(transNumber);
	if (thisTrans == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: getReqstData4Trans did not find transaction %d\n ",transNumber);
		rc = APP_PARAMETER_ERR;
	}
	else
	{// have a transaction
		rc = thisTrans->getReqstList(itemList);
	}
	return rc;
}
RETURNCODE  hCcommand::
				getReqstData4Trans(int transNumber, hIndexUse_t& useIdx,itemIDlist_t& itemList)
{
	RETURNCODE rc = FAILURE;

	hCtransaction*  thisTrans = getTransactionByNumber(transNumber);
	if (thisTrans == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: getReqstData4Trans did not find transaction %d\n",transNumber);
		rc = APP_PARAMETER_ERR;
	}
	else
	{// have a transaction
		rc = thisTrans->getReqstList(itemList);
	}
	return rc;
};
RETURNCODE  hCcommand::
				getReplyData4Trans(int transNumber, hIndexUse_t& useIdx,varPtrList_t& itemList)
{
	RETURNCODE rc = FAILURE;

	hCtransaction*  thisTrans = getTransactionByNumber(transNumber);
	if (thisTrans == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: getReqstData4Trans did not find transaction %d\n",transNumber);
		rc = APP_PARAMETER_ERR;
	}
	else
	{// have a transaction
		rc = thisTrans->getReplyList(itemList);
	}
	return rc;
};
RETURNCODE  hCcommand::
				getReplyData4Trans(int transNumber,hIndexUse_t& useIdx, itemIDlist_t& itemList)
{
	RETURNCODE rc = FAILURE;

	hCtransaction*  thisTrans = getTransactionByNumber(transNumber);
	if (thisTrans == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: getReqstData4Trans did not find transaction %d\n",transNumber);
		rc = APP_PARAMETER_ERR;
	}
	else
	{// have a transaction
		rc = thisTrans->getReplyList(itemList);
	}
	return rc;
};

// when transIsValid, & >=0 , just weigh it	15nov11
int hCcommand::getTheWgt(int& transN, indexUseList_t& useIdx, bool isRd)
{
	int i, wgt = 0, cn     = getCmdNumber();//cn is for debugging
	hCtransaction*  pTrans = NULL;
	indexUseList_t  idxUse;
	useIdx.clear();

	if ( transN >= 0 )// trans# have nothing to do w/ listSz.&& transN < (int)transList.size())
	{// weigh the spec'd transaction
		pTrans = getTransactionByNumber(transN);
		if ( pTrans != NULL )
		{
			wgt = pTrans->weigh(isRd,useIdx);// not a read
			// transN stays valid - return weight
		}
		else
		{
			LOGIT(CERR_LOG,"ERROR: transaction list #%d could not be weighed in cmd %d\n",
																		transN,getCmdNumber());
			transN = -1;
			wgt    = 0;
		}
	}
	else
	{// scan all of 'em
		i = 0;
#ifdef _DEBUG 
		int o = 0;
#endif
		for (TransList_t::iterator iT = transList.begin(); iT < transList.end(); iT++)
		{//iT is a ptr 2 a hCtransaction
			i = iT->weigh(isRd,idxUse);
			if ( i > wgt )
			{
				wgt    =  i;
				transN =  iT->getTransNum();
				useIdx =  idxUse;
#ifdef _DEBUG 
				o = idxUse.size();
#endif
			}
			idxUse.clear(); // stevev 7jul06 - index from earlier trans being passed w/ later
							// stevev 5may09 - moved outside of if so it is always clear going
							//                 into weigh
		}		
	}
	return wgt;
}

// transN is in and out, 
//	out - transaction weight was calculated for
//   in - transaction to calc the weight of
//      - if < 0 , return highest weight of transactions
int hCcommand::getWrWgt(int& transN, indexUseList_t& useIdx)
{
	return (getTheWgt(transN, useIdx, false) );
}

int hCcommand::getRdWgt(int& transN, indexUseList_t& useIdx)
{
	return (getTheWgt(transN, useIdx, true) );
}

#if 0
CattrBase* hCcommand::newAttr(struct itemAttrTbl& it) 
{
	// long attrType;
	// long attrId; actually attribute's owner's itemID

	switch(it.attrType) // 77 ENUMERATOR_TAG - only one found
	{
	case COMMAND_NUMBER           :
		{ return (CattrBase*) new CattrCmdNum(this);}break;
	case COMMAND_OPER        :
		{ return (CattrBase*) new CattrCmdOperation(this);}break;
	case COMMAND_TRANS            :
		{ return (CattrBase*) new CattrCmdTrans(this);}break;
	case COMMAND_RESP_CODES           :
		{ return (CattrBase*) new CattrCmdRspCd(this);}break;
	default                  :
		{ cerr< <"ERROR: Unknown attribute type ("< < it.attrType
						< <")in hCcommand::newAttr for item Id " < < hex 
						< < it.attrId < < dec < < endl;} break;
	}
	return NULL;
}
#endif

// we have no data of our own
// just let the call fall through to parent
//RETURNCODE hCcommand::insertSelf(void)
//{}

/**********************************************************************************************
 * The data item class 
 * 
 **********************************************************************************************
 */

hCdataItem::~hCdataItem()
{ 
	ref.destroy(); 
};

hCVar* hCdataItem::getVarPtr(void)
{
	RETURNCODE rc = FAILURE;
	hCitemBase*     pItem;
	hCVar*          pV;
	cmdDataItemType_t dit = (cmdDataItemType_t)type;

	if (dit == cmdDataReference  && // should not attempt resolve if not data reference
		(( rc = ref.resolveID(pItem,false) ) == SUCCESS)  && pItem != NULL )
	{
		pV = (hCVar*) pItem;
	}
	else
	{
		pV = NULL;
		if (dit == cmdDataReference)
		   LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: dataitem could not be resolved to an item.\n");
		//else - it's a constant...no message required
	}
	return pV;
}

void  hCdataItem::setEqual(aCdataItem* pAclass)
{	
	if (pAclass == NULL) return;//nothing to do

	type   = pAclass->type;
	flags  = (cmdDataItemFlags_t)pAclass->flags;// force it
		//bool  widthValid;			//isTRUE/false - don't know where this comes from???...stevev 04jan17..true when flags has bit cmdDataItemFlg_WidthPresent
	widthValid = (flags & cmdDataItemFlg_WidthPresent)? true : false;
	width  = pAclass->width;
	iconst = pAclass->iconst;
	fconst = pAclass->fconst;
	ref    = (pAclass->ref) ;

#ifdef XX_TEMP__DEBUG
	if ( type == cmdDataReference && // stevev29aug07-constants generate a false error here
		ref.devPtr()->getIdentity().wManufacturer != 0 )// not a tokenizer import
	{
		RETURNCODE rc = SUCCESS;
		itemID_t id = 0;
		rc = ref.resolveID( id, false);// force an error asap- 
									   // this can generate false errors for stuff like
					// list elements that don't exist yet...
		if (rc)
		{
			LOGIT(CERR_LOG,"----- pre instantiation:Warning only ------dbg\n");
		}
	}
#endif
}


void  hCdataItem::setDuplicate(hCdataItem& pDI)
{	
	type   = pDI.type;
	flags  = (cmdDataItemFlags_t)pDI.flags;// force it
	widthValid = pDI.widthValid;
	width  = pDI.width;
	iconst = pDI.iconst;
	fconst = pDI.fconst;
	ref.duplicate(&(pDI.ref),false, 0);// put zero in for now
}

void hCdataItem::clear(void)
{
	type = 0L;
	flags = cmdDataItemFlg_None;
	widthValid = false;			//isTRUE/false
	width = 0L;
	
	iconst = 0L;
	fconst = 0.0;
	ref.clear();   /*  stevev 14aug06 - We may have to destroy this */
}

RETURNCODE hCdataItem::handleActions(cmdOperationType_t rdWr, methodCallList_t& pPostAction)
													// was    HreferenceList_t* pPostAction)
{
	RETURNCODE rc = FAILURE;
	hCitemBase*     pItem = NULL;
	hCVar*          pVar  = NULL; 
		
	if ((cmdDataItemType_t)type == cmdDataReference)
	{
		if (( rc = ref.resolveID(pItem,false) )  == SUCCESS && pItem != NULL )
		{
			pVar = (hCVar*) pItem;
			if (rdWr == cmdOpRead)
			{
			 //	if Post Read Actions then insert item into MsgCycle post list.
				rc = pVar->getPstRdActs(pPostAction);	
			 //	do item's PreRead actions (will block for method) device->ExecuteMethod(itemID)
				rc = pVar->doPreRdActs();// rc is the return value
			}
			else
			if (rdWr == cmdOpWrite)
			{// do the same for writes
				rc = pVar->getPstWrActs(pPostAction);
				rc = pVar->doPreWrActs();// this is the return value
			}
			else
			{// we know nothing...we do nothing...
				rc = SUCCESS;
			}
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG, 
									L"ERROR: could not resolve reference in dataitem.(3)\n");
		}
	}
	return rc;
}

// see notes at the end of the ddbCommand.h file
RETURNCODE hCdataItem::AddIt(BYTE* pByteCnt, BYTE** ppBuf, int& length, bool& isValid)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient   wrkVarient;
	hCitemBase*     pItem = NULL;
	hCVar*          pVar  = NULL;

	isValid = false; // the error exit value

	cmdDataItemType_t dit = (cmdDataItemType_t)type;
	if (dit == cmdDataConst)
	{
		*(*ppBuf) = (BYTE) (iconst & 0xFF);// unsigned byte - by spec definition   
									 
		(*ppBuf)++;
		(*pByteCnt)++;
		rc = SUCCESS;
		isValid = true;
	}
	else
	if (dit == cmdDataFloat)
	{
		hCFloat floatVar(0);
		wrkVarient = fconst;
		floatVar.setDispValue(wrkVarient);
#ifdef _DEBUG
if (length)
{
	LOGIT(CERR_LOG,L"ERROR: Length has a value in a const float dataitem that has no mask.\n");
}
#endif
		length = 0;
		rc = floatVar.Add(pByteCnt,ppBuf,0,length);
		isValid = true;
		// floatVar destroyed
	}
	else
	if (dit == cmdDataReference)
	{
		if (( rc = ref.resolveID(pItem,false) )  == SUCCESS)
		{
			pVar = (hCVar*) pItem;
			if ((flags & cmdDataItemFlg_WidthPresent) != 0 )// 0x8000
			{//	slit the mask functionality into another method for maintainence purposes
				rc = AddMask(pVar, pByteCnt,ppBuf, length);
			}
			else
			{	// these don't count in a reply
				//cmdDataItemFlg_Info:			// 0x01
				//cmdDataItemFlg_Index:			// 0x02
				//cmdDataItemFlg_indexNinfo:	// 0x03
				//cmdDataItemFlg_None:			// 0x00,
#ifdef _DEBUG
if (length)
{
	LOGIT(CERR_LOG,L"ERROR: Length has a value in a dataitem that has no mask.\n");
}
#endif
				length = 0;
				rc = pVar->Add(pByteCnt,ppBuf, 0,length);				
			}// end else(normal case)
			if (pVar->IsValid()) { isValid = true;}else{isValid = false;}
		}
		else
		{
			LOGIT(CERR_LOG,L"ERROR: Transaction's DataItem could not resolve reference.\n");
		}
	}
	else // unknown
	{
		LOGIT(CERR_LOG,L"ERROR: hCdataItem::AddIt has an unknown type.\n");
		rc = APP_TYPE_UNKNOWN;
	}
	return rc;
}

// see notes at the end of the ddbCommand.h file
RETURNCODE hCdataItem::markPending(void)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase*     pItem = NULL;
	cmdDataItemType_t dit = (cmdDataItemType_t)type;
	if (dit == cmdDataReference)
	{
		if (( rc = ref.resolveID(pItem,false) )  == SUCCESS && pItem != NULL)
		{ 
			if (pItem->getIType() == iT_Variable)
			{
				((hCVar*) pItem)->markItemState(IDS_PENDING);		
			}
			else
			{
				LOGIT(CERR_LOG,L"ERROR: dataitem reference resolved to a non-Variable.\n");
			}
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: dataitem could not resolve reference.(4)\n");
		}
	}
	//else - const or unknown - don't try to mark
	return rc;
}

RETURNCODE hCdataItem::AddMask(hCVar* pVAR, BYTE* pByteCnt, BYTE** ppBuf, int& length)
{// to get here we have determined there is a mask.
#ifdef _DEBUG
if (width == 0)
{
	LOGIT(CERR_LOG,L"ERROR: width present with width zero (no mask)\n");
	return FAILURE;
}
#endif
	RETURNCODE rc = FAILURE;
	//int mskLen;
	if ( length == 0 )
	{// first mask - MUST include the highest bit (by DD spec)
		length = maskLen((UINT32)width);// stevev 27aug10 - as of right now, the tokenizer is
										//                  limited to a ulong
		if (length == 0)
		{	
			return FAILURE;
		}// else we are ready to start - call Add()
	}// else - length not zero, continuation - just call Add()

	rc = pVAR->Add(pByteCnt,ppBuf, width, length);// we could use another method if needed
	if ( length != 0 )
	{// normal condition
		if ( (width & 0x00000001) != 0 )// low bit set - we be done
		{
			*pByteCnt += length;
			* ppBuf   += length;
			length     = 0;
			// return the rc from the last add call
		}
		// else return to call the next dataitem with the next mask w/ length > 0
	}
	else // an error condition where the item took default action(&inc'd buff)
	{
		LOGIT(CERR_LOG,L"ERROR: AddMask had an item clear the length.\n");
	}// endelse
	return rc;
}

// see notes at the end of the ddbCommand.h file
// serious rework of internals to handle INFO stevev 13may11(friday the thirteenth)
RETURNCODE hCdataItem::ExtractIt(BYTE* pByteCnt, BYTE** ppBuf, int& length, 
								 methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
								 varPtrList_t &hasChangedList, hIndexUse_t& info4Idx,  
								 transactionType_t transType, indexUseList_t* pSentIndex)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient   wrkVarient;
	hCitemBase*     pItem = NULL;
	hCVar*          pVar  = NULL;
	itemID_t        localID;

	info4Idx.clear();// info to be restored later

	cmdDataItemType_t dit = (cmdDataItemType_t)type;
	if (dit == cmdDataConst)
	{
		BYTE iC = *(*ppBuf);// unsigned byte - by spec definition
		(*ppBuf)++;
		(*pByteCnt)--;	// stevev 12nov09 - changed ++ to --; 
						//                  Vega issue when using several constants
		if (iC != iconst)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,
			L"ERROR: Constant mismatch: Received:0x%02x Expected: 0x%02x\n", (int)iC, iconst);
		}
		rc = SUCCESS;
	}
	else
	if (dit == cmdDataFloat)
	{
		hCFloat floatVar(0);
		wrkVarient = fconst;
		floatVar.setDispValue(wrkVarient);
		#ifdef _DEBUG
		if (length)
		{
			LOGIF(LOGP_NOT_TOK)
			   (CERR_LOG,"ERROR: Length has a value in a const float dataitem but no mask.\n");
		}
		#endif
		length = 0;															 // to real value
		rc = floatVar.Extract(pByteCnt,ppBuf,0,length,eaLst, noteMsgs, hasChangedList,NoFlags);
		// floatVar destroyed on exit
		// stevev 06mar07 - after discussion w/paul, this should be handled like int
		wrkVarient = floatVar.getDispValue();
		if ( ! ( ((float)wrkVarient) == fconst) )
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,
			 "ERROR: Constant mismatch: received:%g Expected: %g\n",(float)wrkVarient, fconst);
		}
		// end stevev 06mar07 end //
	}
	else
	if (dit == cmdDataReference)
	{
		if (( rc = ref.resolveID(pItem,false) )  == SUCCESS && pItem != NULL)
		{
			pVar = (hCVar*) pItem;
			/*  stevev 27oct08 - only apply if pass 2, 
								 on Pass one, apply class local or subtype index or INDEX
			 */
			//bool isSimulator = (pVar->devPtr()->getPolicy().isReplyOnly)? true : false;
			bool passTwo     = (transType & tt_isExtract)               ? true : false;
			bool doNotApply  = false; //do apply...pass 2

			if (! passTwo)// pass one
			{// all INFO items will be stored for all apps
				localID = pVar->getID();
				if ( (flags & cmdDataItemFlg_Info) != 0 ) // any marked info
				{
					info4Idx.indexSymID     = localID;
					info4Idx.indexWrtStatus = 2;//info
					info4Idx.indexDispValue = pVar->getDispValue();
					info4Idx.indexRealValue = pVar->getRealValue();
					info4Idx.indexDataState = pVar->getDataState();
				}

		/* new algorithm 04jun13 from WAP:
		 * Pass one: extract all the items in the packet that have an itemID reference type
		 * Pass two: extract all of 'em regardless.
		 **** NOTE: in simulators extracting request packet, it must be INFO as well
		
				// three conditions for 'look-ahead' extraction
				if ( pVar->IsLocal()                    || 
					 pVar->VariableType() == vT_Index   || 
					 (flags & cmdDataItemFlg_Index) != 0    )
		 */		
				bool isFlagged;
				if ( pVar->devPtr()->getPolicy().isReplyOnly )
				{//simulator: assume a request packet 
					isFlagged = (flags & cmdDataItemFlg_indexNinfo) != 0; // either set take it
				}
				else // host reply packet
				{
					isFlagged = true; // always take it
				}

				if ( (ref.getRefType() == rT_Variable_id || ! ref.isaRef() ) && isFlagged )
				{// direct reference and is flagged then apply on the first pass
					doNotApply = false; // apply these types, we'll undo it later
				}
				else
				{
					doNotApply  = true;// do not store other types for pass one
				}
			}
			// else pass two is to extract everything...doNotApply is false


			if ((flags & cmdDataItemFlg_WidthPresent) != 0 )// 0x8000
			{//	split the mask functionality into another method for maintainence purposes
				rc = ExtractMask    // does it all<this is the only call to this function>
				(pVar, pByteCnt,ppBuf, length, eaLst, noteMsgs, hasChangedList, transType); 
			}
			else
			{	//cmdDataItemFlg_Index:			// 0x02 - nothing special
				//cmdDataItemFlg_indexNinfo:	// 0x03 0x01 - disreguard - do not apply
				//cmdDataItemFlg_None:			// 0x00,- normal
#ifdef _DEBUG
				// temporary to learn more
				if ((flags & cmdDataItemFlg_Index) != 0 && 
					(flags & cmdDataItemFlg_Info)  == 0)
				{
					LOGIT(CLOG_LOG,"An incoming index without an INFO.\n");
				}
				if (length)
				{
				 LOGIT(CERR_LOG,"ERROR: Length has a value in a dataitem that has no mask.\n");
				}
#endif
				// 13may11 - we extract everything..INFO is restored by a higher routine
				length = 0;
				rc = pVar->Extract(pByteCnt, ppBuf, 0, length, eaLst, noteMsgs,
														hasChangedList, doNotApply);// normal
				if ( rc == SUCCESS && ! doNotApply)
				{	
					pVar->cacheClear();// so it won't be restored
				}
				if ( (transType & tt_isWrite) != 0 && ! doNotApply)//was:> ( isWrite )
				{
#ifdef _DEBUG
					int wasChanged =
#endif
					pVar->CancelIt();// device value to display value in a write reply
				}
			}// end else(normal case)
			

			if (pSentIndex != NULL && ! passTwo )// then it exists and has stuff in it 
			{
				idxUseIT iuIT;hIndexUse_t *piut;
				for (iuIT = pSentIndex->begin(); iuIT != pSentIndex->end();++iuIT)
				{
					piut = &(*iuIT);
					if (localID == piut->indexSymID)
					{
						CValueVarient tmpVar = pVar->getRealValue();// what we extracted
						if ( !(tmpVar == piut->indexDispValue) )
						{							
							LOGIT(CLOG_LOG,L"-x-x-x-x-x- Index Value changed in the extraction"
							L"\n            Sym#:0x%04x  Was Sent:%d Device Now:%d, "
							L"Display Now:%d\n",
							localID, (int)(piut->indexDispValue),(int)tmpVar,
							(int) pVar->getDispValue(true)   );
							
							rc = APP_INDEX_MISMATCH;
							break;// out of for loop
						}
					}
				}// next sent index
			}

#ifdef XMTR
			if ( rc == SUCCESS && noteMsgs.isRqstPkt  && passTwo)
			{// no-op if not a simulator, won't be here in a simulator in a reply pkt, 
			 //                                                              skip on pass one
				pVar->doPstRequestActs();
			}
#endif
		}
		else
		{
			LOGIT(CERR_LOG,L"ERROR: could not resolve reference in dataitem extraction.\n");
		}
	}
	else // unknown
	{
		LOGIT(CERR_LOG,L"ERROR: hCdataItem::ExtractIt has an unknown type.\n");
		rc = APP_TYPE_UNKNOWN;
	}
	return rc;
}

RETURNCODE hCdataItem::ExtractMask(hCVar* pVAR, BYTE* pByteCnt, BYTE** ppBuf, int& length,
								   methodCallList_t& eaLst, hCmsgList&  noteMsgs, 
								   varPtrList_t &hasChangedList,
								   transactionType_t transType)
{// to get here we have determined there is a mask.
#ifdef _DEBUG
if (width == 0)
{
	LOGIT(CERR_LOG,L"ERROR: width present with width zero (no mask)\n");
	return FAILURE;
}
#endif
	RETURNCODE rc = FAILURE;
	//int mskLen;
	if ( length == 0 )
	{// first mask - MUST include the highest bit (by DD spec)
		length = maskLen((UINT32)width);// stevev 27aug10 - as of right now, 
										//					the tokenizer is limited to a ulong
		if (length == 0)
		{	
			return FAILURE;
		}// else we are ready to start - call Add()
	}// else - length not zero, continuation - just call Add()

	/*  stevev 27oct08 - a) match Extract by having ALL flags being applied 
	 *	(flags in reply don't count)  and b) only apply if pass 2
	 */
	bool DoNotWrite = false;
	if (transType & tt_isExtract) DoNotWrite = false; else DoNotWrite = true;// pass one
	// stevev 05jun09 - we aren't getting a masked index, add the following
	if (DoNotWrite)// pass one
	{
		if ( pVAR->IsLocal() || pVAR->VariableType() == vT_Index )
		{
			DoNotWrite = false; // apply these types
		}
	}

	if ( (flags & cmdDataItemFlg_indexNinfo) == cmdDataItemFlg_indexNinfo)
	{// an index/info variable
		rc = pVAR->Extract(pByteCnt,ppBuf, width, length, eaLst, noteMsgs, 
													hasChangedList, DoNotWrite);// do not apply
	}
	else
	{	//										stevev 31jan06 - changed from true (PAR 5632)
		rc = pVAR->Extract(pByteCnt,ppBuf, width, length, eaLst, noteMsgs, 
													hasChangedList, DoNotWrite);// apply
		if ( rc == SUCCESS )
		{	pVAR->cacheClear();// so it won't be restored
		}
	}
	if ( length != 0 )
	{// normal condition
		if ( (width & 0x00000001) != 0 )// low bit set - we be done
		{
            /* UTTHUNGA FIX : Using pVAR->getSize() instead of "length"
               as parameters are not getting proper value
               when bit mask parameter is present in the command */
            *pByteCnt -= pVAR->getSize();// 31oct11 - codewrights - changed from +=
            * ppBuf   += pVAR->getSize();
			length     = 0;
			// return the rc from the last add call
		}
		// else return to call the next dataitem with the next mask w/ length > 0
	}
	else // an error condition where the item took default action(&inc'd buff)
	{
		LOGIT(CERR_LOG,L"ERROR: ExtractMask had an item clear the length.\n");
	}// endelse
	return rc;
}

int hCdataItem::maskLen(ulong mask)
{// must return number of bytes to the right of the highest set bit 
 //																(only called on first of set)
	int i,k;
	ulong mskMsk;
	for (i = (sizeof(mask)-1)*8, k = sizeof(mask); i >= 0; i-=8, k--)
	{
		mskMsk = 0xFF << i; // we could use 0x80 if we want to be stringent
		if ( (mask & mskMsk) != 0 )// we can return on any bit(only called on first of set)
		{	return k;
		}// else loop for next
	}
	LOGIT(CERR_LOG,L"ERROR: No bits set in mask passed to maskLen.\n");
	return 0;
}

// add all possible vars to list
RETURNCODE hCdataItem::addAllVars(GroupItemList_t& varGroup)
{
	RETURNCODE rc = SUCCESS;
	hCGroupItemInfo gii;

	if (type == cmdDataReference)
	{
		rc = ref.addAllVars(varGroup);// let the reference do the work
	}// else - return success with nothing added (skip constants)
	return rc;
}

// update all lists in this reference
RETURNCODE hCdataItem::commandAllLists(hCcommandDescriptor& cmdDesc)
{
	RETURNCODE rc = SUCCESS;

	if (type == cmdDataReference)
	{
		rc = ref.commandAllLists(cmdDesc);// let the reference do the work
	}// else - return success with nothing added (skip constants)

	return rc;
}

/**********************************************************************************************
 * The data item LIST  class 
 * 
 **********************************************************************************************
 */
void  hCdataitemList::setEqual(void* pAclass)
{
	if (pAclass == NULL) return;//nothing to do

	aCdataitemList* pDIList = (aCdataitemList*) pAclass;
	hCdataItem wrkDitm(devHndl());

// special test for abb par
//if (pDIList->at(0).ref.id == 0x96 &&
//	pDIList->at(1).ref.id == 0x9a )
//{
//pDIList->at(1).ref.id = 0x97;
//}

	reserve(pDIList->size());	//PO reserve setequal
	FOR_p_iT(aCdataitemList, pDIList)
	{// iT is ptr 2 a aCdataItem
		wrkDitm.setEqual(&(*iT));
		push_back(wrkDitm);

		wrkDitm.clear();		
	}// next
	
}


void  hCdataitemList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	if (pPayLd == NULL) return;//nothing to do

	hCdataitemList* pDIList = (hCdataitemList*) pPayLd;

	hCdataItem wrkDitm(devHndl());

	FOR_p_iT(dataItemList_t, pDIList)
	{// iT is ptr 2 a hCdataItem 


		wrkDitm.setDuplicate(*((hCdataItem*)(&(*iT)))); // no return status
		push_back(wrkDitm);

		wrkDitm.clear();		
	}// next
	
}

RETURNCODE hCdataitemList::destroy(void)
{	
	RETURNCODE rc = 0;
	hCdataItem *pDi = NULL;

	for(dataItemList_t::iterator iT = begin();iT!=end();++iT)
	{// iT is ptr 2 a hCdataItem 	
		pDi = const_cast<hCdataItem *>( &(*iT) );		
		rc |= pDi->destroy();		
	}// next
	clear();
	return rc;
}

int hCdataitemList::append(hCdataitemList* pSrcItemsList)
{
	reserve(size() + pSrcItemsList->size());	//PO reserving to prevent multiple allocations
	FOR_p_iT(hCdataitemList, pSrcItemsList)
	{// iT is ptr 2 a hCdataItem 		
		push_back(*iT);		
	}// next
	return size();
}

int hCdataitemList::appendUnique(hCdataitemList* pSrcItemsList)
{
	RETURNCODE rc = SUCCESS;
//	dataItemList_t::iterator iL;
	UINT32 mark=0;
//	UINT32 SrcID, DstID;

	reserve(size() + pSrcItemsList->size());	//PO reserving to prevent multiple allocations
	if (pSrcItemsList == NULL)/// stevev 10jul07 - added
	{// an error
		LOGIT(CLOG_LOG|CERR_LOG,"Appending a NULL dataitem list!\n");
		return size();
	}
	FOR_p_iT(hCdataitemList, pSrcItemsList)
	{// iT is ptr 2 a hCdataItem 
		if (&(*iT) != NULL)// PAW added &* 03/03/09
		{	/* stevev 03aug07 - it is completely legal to dsend the same item more than
				once in a command request or reply.  This cannot be unique so the unused
				uniqueifying code has been removed.   ***/
			push_back(*iT);
		}// else - skip nulls
		else
		{
			DEBUGLOG(CLOG_LOG|CERR_LOG,"NOT Appending a NULL dataitem from src list!\n");
		}
	}// do 'em all
	return size();
}


// 15sep09 - added to prevent reusing freed memory errors after a push_back

hCdataitemList& hCdataitemList::operator=(const hCdataitemList& s)
{	
	dataItemList_t::const_iterator iT;
	const hCdataItem *pDi = NULL;

	for(iT = s.begin(); iT != s.end(); ++iT)
	{// iT is ptr 2 a hCdataItem 	
		pDi = &(*iT);
		push_back(*pDi);				
	}// next

	return (*this);
}

hCdataitemList::~hCdataitemList()// new 08may13
{
	destroy();
}

/**********************************************************************************************
 * The response code LIST  class 
 * 
 **********************************************************************************************
 */
// 10/20/03 - modified to append uniquely with new list supplanting old on duplicates
void hCRespCodeList::append(hCRespCodeList* addlist)
{	

	reserve(size() + addlist->size());		//PO reserving to prevent multiple allocations

	//was::> insert(end(),addlist->begin(),addlist->end()); 
	//hCRespCodeList::responseCodeList_t
	
	/* since self's list will be realloc'd on every push_back, we have to map indicies */
	typedef map<unsigned long, unsigned long >/*hCrespCode*>*/ rcMap_t;
	rcMap_t tmpMap;
	rcMap_t::iterator pos;

	// fill the map with our info
	{// block to eliminate iT when done
		unsigned long loc = 0;
		for (responseCodeList_t::iterator iT=begin();iT != end(); ++iT,loc++)
		////////FOR_this_iT(responseCodeList_t,addList) // addlist is unused
		{//iT isa ptr2a hCrespCode
			tmpMap[iT->getVal()] = loc;/*(hCrespCode*)iT;*/
		}
	}

	FOR_iT(responseCodeList_t,*addlist) // for each new value
	{// iT isa ptr2a hCrespCode
		if (&(*iT) != NULL)// PAW added &* 03/03/09
		{
			unsigned long v = iT->getVal();
			pos = tmpMap.find(v);

//works for vs6 #if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
			hCrespCode* hRCode = &(*iT);
//#endif
			if ( pos == tmpMap.end() )
			{// no duplicate found
				tmpMap[v] = size();// index it will be inserted to...not::>  (hCrespCode*)iT;
				/*this*/

				push_back(* (hRCode) );
			}
			else // duplicate, we should replace the the entry value
			{
				at(pos->second) = (* (hRCode) );
			}
		}// else should never happen
	}
};

void  hCRespCodeList::setEqual(void* pAclass)
{
	if (pAclass == NULL) return;//nothing to do

	aCrespCodeList* pRCL = (aCrespCodeList*) pAclass;
	ArespCodesList_t* pLst = (ArespCodesList_t*)pRCL;

	hCrespCode wrkRCLst(devHndl());
	reserve(pRCL->size());	//PO reserve setequal
	FOR_p_iT(ArespCodesList_t, pLst)
	{// iT is ptr 2 a aCrespCode
		wrkRCLst.setEqual( &(*iT));
		push_back(wrkRCLst);

		wrkRCLst.clear();		
	}// next
}

void  hCRespCodeList:: duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{
	hCRespCodeList* pRcS = (hCRespCodeList*)pPayLd;	

	if (pRcS == NULL) return;//nothing to do

	hCrespCode wrkRC(devHndl());

	FOR_p_iT(responseCodeList_t, pRcS)
	{// iT is ptr 2 a hCrespCode
		wrkRC.setDuplicate( &(*iT) );
		push_back(wrkRC);
		wrkRC.clear();		
	}// next
}
/********** helper functions *****************************************************************/
	

hCcommandDescriptor& hCcommandDescriptor::operator=(const hCcommandDescriptor& cd)
{
	cmdNumber=cd.cmdNumber;
	transNumb=cd.transNumb;
	rd_wrWgt =cd.rd_wrWgt;
	srcIdxVar=cd.srcIdxVar;
	idxList  =cd.idxList;
	cmdTyp   =cd.cmdTyp;

	if (idxList.size() != cd.idxList.size())
	{
		LOGIT(CERR_LOG,"Internal error: copy failed in commandDescriptor\n");
	}

	return *this;
};
void hCcommandDescriptor::dumpSelf(void)
{
	LOGIT(CLOG_LOG|COUT_LOG,"C %d ::T %d    wgt:%d",cmdNumber,transNumb,rd_wrWgt);
	if (idxList.size())
	{
		LOGIT(CLOG_LOG|COUT_LOG," with Index: ");
	}
	FOR_iT(indexUseList_t,idxList)
	{
		if (iT->indexSymID)
		{
		LOGIT(CLOG_LOG|COUT_LOG,"Local Var 0x%04x [val:%d]  ",iT->indexSymID,(int)(iT->indexDispValue));
		}
		if (iT->devIdxSymID)
		{
		LOGIT(CLOG_LOG|COUT_LOG,"Device Var 0x%04x [val:%d]  ",iT->devIdxSymID,(int)(iT->devVarRequired));
		}
	}
	LOGIT(CLOG_LOG|COUT_LOG,"\n");
};

/*********************************************************************************************/
RETURNCODE hCIndexValueAndVariables::appendUnique(hCVar * pVTarget)
{	bool notThere = true;
	for ( varPtrLstIT_t I = resolvedVarList.begin(); I != resolvedVarList.end(); ++I)
	{	hCVar* pV = (hCVar *)(*I);// I is ptr2ptr2hCVar
		if ( pV->getID() == pVTarget->getID() ) 
		{	notThere = false;   break;   }
	}// next existing variable id
	if ( notThere ) 
	{	resolvedVarList.push_back(pVTarget);	}
	return SUCCESS;
}

void   hCIndexValueAndVariables::clogSelf(void)
{	if (isEmpty() )
	{	LOGIT(CLOG_LOG,"       Value: EMPTY  Resolves: Nothing\n"); return;}//else clog
	LOGIT(CLOG_LOG,"       Value: 0x%04x Resolves: ",indexValue);
	for (varPtrLstIT_t I = resolvedVarList.begin(); I != resolvedVarList.end(); ++I)
	{	hCVar* pV = (hCVar *)(*I);// I is ptr2ptr2hCVar
		LOGIT(CLOG_LOG,"%04x ", pV->getID());
	}// next varID
	LOGIT(CLOG_LOG,"\n");
};
/**********************************************************************************************
 * The message LIST  class 
 * 
 **********************************************************************************************
 */


RETURNCODE hCmsgList::insertUnique(hCmessage& newMsg)
{
	// see if it exists
	for ( iterator it = begin(); it != end(); ++it )
	{
		if ( (*it) == newMsg )
		{
			return SUCCESS; // exists already
		}
	}
	// then
	push_back(newMsg);
	return SUCCESS;
}

/**********************************************************************************************
 * NOTES:
 *	The transaction will resolve it's list of items when the passed in resolution handle 
 *		(resHndl) does not match it's current resolution handle.  This allows a single 
 *		condition resolution for a given command operation since there may be a LOT of accesses
 *	    to the information.The transaction list will be the keeper of the handle and will tell
 *		the transaction what it is
 *	Calling a Transaction list method with a handle that does not match the current handle will
 *		result in all transactions re-resolving their information. Zero is an invalid res hndl.
 *
 * PACKET CONSTRUCTION:
 * WAP interpretation: 
 *			RdCmd  All of Request Pkt AND ANY of Reply Pkt isValid() then OK to send
 *			WrCmd  Any of Request Pkt AND ANY of Reply Pkt isValid() then OK to send
 * WAP change of interpretaion march - 06 < we will not write invalid data to a device >
 *			WrCmd  All of Request Pkt AND ANY of Reply Pkt isValid() then OK to send
 *
 * Action Handling:
 *     ie: RdCmd * RqstPkt is any Variable in the request packet that has the type of action
 *         indicated by the column heading will execute that that action at the time shown in
 *         square.
 *
 * Action execution timing:  
 *  Note: Assume that all pre-rd/wr actions will modify the data in the variable and must
 *        be executed before that value is added to the packet.
 *
 *         PreIns     Pre-Insertion to packet - these can be done one at a time, just before
 *                    Data insertion.
 *         PreRqst    Reply's pre actions must execute before the Pre-Insertion group.
 *         PstParseA  Post Extracting and updating the value of the Variable.  These are done 
 *                    first after all the reply values are extracted into the device variables.
 *         PstParseB  Post Extracting and updating the value of the Variable.  These are done 
 *                    after PstParseA and before PstParseC.
 *         PstParseC  Post Extracting and updating the value of the Variable.  Done last.
 *
 *                    *   PreRd    *   PostRd   *   PreWr    *  PostWr    *
 *                    *------------*------------*------------*------------*
 *  RdCmd  * ReqstPkt *  PreIns    *  Discard   *  Discard   *  Discard   *
 *         * ReplyPkt *  PreRqst   *  PstParse  *  Discard   *  Discard   *
 *--------------------*------------*------------*------------*------------*
 *  WrCmd  * ReqstPkt *  Discard   *  Discard   *  PreIns    *  PstParseA * 
 *         * ReplyPkt *  Discard   *  PstParseC *  Discard   *  PstParseB *
 *
 **********************************************************************************************
 */
/**********************************************************************************************
 *
 *   $History: ddbCommand.cpp $
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:20a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:14p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 **********************************************************************************************
 */
