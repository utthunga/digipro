/*************************************************************************************************
 *
 * $Workfile: ddbResponseCode.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the response code base class
 * Response Codes appear in items,transactions & commands
 *
 *	4/22/02	sjv	created ddbrespcd.h
 *	9/6/02  sjv generated ddb... from responsecdbase.h
 *
 *
 * #include "ddbResponseCode.h"
 */

#ifndef _RESPONSECODE_H
#define _RESPONSECODE_H

#include "ddbPrimatives.h"
#include "ddbConditional.h"

#include "ddbVar.h"

/*****************************
 * hCrespCode holds the actual data
 * from the structure RESPONSE_CODE
 ****************************/
class hCrespCode  : public hCobject
{
	unsigned long    val;
	unsigned long    type;
	hCddlString      descS;
	hCddlString      helpS;
	string retString; // stevev - WIDE2NARROW char interface
public:
	// no default constructor (we MUST have the handle)
	hCrespCode(DevInfcHandle_t h)
		:hCobject(h),descS(h),helpS(h) {clear();};
	// copy constructor
	hCrespCode(const hCrespCode& src )
		:hCobject(src.devHndl()),descS(src.devHndl()),helpS(src.devHndl()){ operator=(src);};
	RETURNCODE destroy(void)// split out for debug 12/08/09 PAW
	{  
		RETURNCODE return_value = descS.destroy();
		return_value |= helpS.destroy();
		return ( return_value );
	};
	virtual ~hCrespCode() {destroy(); clear();};// 11jan11-added destroy to try and stop a leak

	hCrespCode& operator=(const hCrespCode& src )
	{val = src.val; type = src.type; descS = src.descS; helpS = src.helpS; return (*this);};
	
	hCrespCode& operator=(const hCenumDesc& src )
	{val = src.val; type = 0; descS = src.descS; helpS = src.helpS; return (*this);};

	unsigned long getVal(void) {return val;};
	wstring       getDescStr(void) {
		return descS.procureVal();
	};
	hCddlString & getDesc(void) { return descS; };
	// stevev - WIDE2NARROW char interface \/ \/ \/
	// stevev - always wide... string       get_DescStr(void) {return ((string) descS);};
	unsigned long getType(void) { return type; };
	static
	unsigned long getType(unsigned char c ) /* stevev 7mar05 - make translation available to all */
		{	if ( (c>=0 && c <= 7) || (c >=16 && c <=23) || ( c>=32 && c <=64 )
			  || (c>=9 && c <=13) || (c >=65 && c <=95) || ( c== 15) 
			  || (c ==28 ) || ( c== 29) )
			{/* it is an error */	return MISC_ERROR_RSPCODE;			}
			else
			{/* it is a warning */	return MISC_WARNING_RSPCODE;		}
		};
#define	RESPCD_ISERROR(v)  ((v==MISC_ERROR_RSPCODE)||(v==DATA_ENTRY_ERROR_RSPCODE)||(v==MODE_ERROR_RSPCODE)||(v==PROCESS_ERROR_RSPCODE))

	void setEqual(aCrespCode* pArc)
	{	val = pArc->val; type = pArc->type; descS = pArc->descS; helpS = pArc->helpS; };
	void setDuplicate(hCrespCode* pSrc)
	{	val = pSrc->val; type = pSrc->type; 
		descS.duplicate(&(pSrc->descS), false); helpS.duplicate(&(pSrc->helpS), false); };
		// duplicate strings with any replicate, they don't use it

	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	void       clear(void){ val = type = 0; descS.clear(); helpS.clear();};
};

typedef vector<hCrespCode>   responseCodeList_t;

/*****************************
 * hCRespCodeList holds a single list of response code data classes
 * 
 ****************************/
class hCRespCodeList  : public hCpayload, public responseCodeList_t, public hCobject
{
public:

	hCRespCodeList(DevInfcHandle_t h) : hCobject(h) {};
	hCRespCodeList(const hCRespCodeList& src):hCobject(src.devHndl())
	{   operator=( src ); };
	hCRespCodeList& operator=(const hCRespCodeList& s)
	{	destroy();
		responseCodeList_t::const_iterator iT;
		for( iT = s.begin(); iT != s.end(); ++iT)
		{	
			push_back( *iT );
		}
		return *this;
	};
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);

	virtual ~hCRespCodeList(){ destroy(); };// 09may13 - clear now done in destroy
	RETURNCODE destroy(void)
	{  // opened out for debugging PAW 12/08/09
		RETURNCODE rc = 0;
		try	// added 12/08/09 for debugging PAW
		{
		FOR_this_iT(responseCodeList_t,this)
		{ 
			rc |= iT->destroy();
		} 
		}
		catch (...)
		{
			LOGIT(CERR_LOG,"hCrespCode::destroy() CRASH \n");
			rc = -1;
		}
		clear();
		return rc;
	};

	// see below  RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet = 0) ;
								//{ cerrxx< "____stubout___CrespCodeList" << endl;return FAILURE;};

	void append(hCRespCodeList* addlist);// modified to append with added list overwriting existing
										 // was::>{	insert(end(),addlist->begin(),addlist->end()); };
	void appendUnique(hCRespCodeList* addlist) 
	{// temporary bypass
		append(addlist);
	};

	RETURNCODE clear(void){ responseCodeList_t::clear(); return SUCCESS; };
	void  setEqual(void* pAclass); // a hCpayload required method	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType)
		{	if (ldType != pltRespCdList) return false;  else  return true;  };
	RETURNCODE dumpSelf(int indent = 0){ 
		LOGIT(COUT_LOG,"%sRespCodeList does not dump at this time.\n",Space(indent));return 0;};
};

/***************************
 * a response code list is a conditional list of response code lists
 ***************************/
typedef hCcondList<hCRespCodeList> condRespCodeL_t;


class hCRespCodeBase : public condRespCodeL_t
{
public:
	hCRespCodeBase(DevInfcHandle_t h) : condRespCodeL_t(h){};
	hCRespCodeBase(const hCRespCodeBase& src) : condRespCodeL_t(src)
		{ *this = src;/*respCodes = src.respCodes;*/};	
	RETURNCODE destroy(void){  return condRespCodeL_t::destroy(); };
	virtual ~hCRespCodeBase() { destroy(); };


	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);

};


#endif//_RESPONSECODE_H

/*************************************************************************************************
 *
 *   $History: ddbResponseCode.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
