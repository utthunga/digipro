/*************************************************************************************************
 *
 * $Workfile: ddbGlblSrvInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the interface to the devices/device parts so they can get their own pointer.
 * This is included in a large percentage of the modules
 *		
 * #include "ddbGlblSrvInfc.h"		
 */

#ifndef _DDBGLOBALSERVICEINTERFACE_H
#define _DDBGLOBALSERVICEINTERFACE_H

#include "ddbGeneral.h"

// external references
class hCdeviceSrvc;
class hCVar;

#ifdef _DEBUG
extern void incThrdCnt(void); // in ddbDeviceMgr.cpp
extern void decThrdCnt(void);
#endif

// define the global parent class everybody can get to
/* * * notice that this is a pure virtual base class! * * */

class hCglobalServiceInterface
{
public:
	// the device service interface just returns the correct device's interface pointer 
	//  on error it will return the error device interface
	static  
	hCdeviceSrvc*    myDevicePtr(DevInfcHandle_t myHandle);
	static
	int /*bool */    dbNOTavailable(void);
/*	static
	int              output(int channel, char* format, ...);
	static
	int              voutput(int channel, char* format, va_list vL);
	static
	int              output(int channel, int errNumber, ...);
	static
	int              voutput(int channel, int errNumber, va_list vL);
*/
	/* * other global DB services may need to be added in the future * */
};

// no longer valid - use LOGIT...#define OUTPUT  hCglobalServiceInterface::output

/* base class to almost everything  -- implemented in ddbDeviceMgr.cpp -- */
//		gives everyone access to this device's services
class hCobject
{
	DevInfcHandle_t MyDeviceHandle;
	hCdeviceSrvc*   pTheDevice;		/* shortcut to eliminate subsequent pointer fetches */

protected:
	/* so subclasses (everybody) can access the device services */
	// made public 8/aug06 hCdeviceSrvc*    devPtr(void);
	void             setPtr(DevInfcHandle_t h); // had to add this so we can instantiate
												// before we know what our handle is (chk 4 zero)
public:
	/* so subclasses can use this handle to construct lower level classes*/
	/* it is public so copy constructors can copy the handle             */
	DevInfcHandle_t  devHndl(void)  const { return MyDeviceHandle; };
	// constructor - no default - must use value
	hCobject(DevInfcHandle_t newHandle);     // : myHandle(newHandle) < should be changed >
	virtual	~hCobject();

	// a useful function that will return NULL on err or not-a-var
	hCVar* getVarPtrBySymNumber(itemID_t symNum);
	devMode_t        compatability(void);

	hCdeviceSrvc*    devPtr(void);// stevev 8aug06 - allow statics and others to get device services
	static
	hCdeviceSrvc*    stdPtr(void);// stevev 03jul09- allow use of the SDC DD in any item.
	// use the default copy constructor...for now
	// hCobject(const hCobject& srcObject);
};


#endif //_DDBGLOBALSERVICEINTERFACE_H


/*************************************************************************************************
 *
 *   $History: ddbGlblSrvInfc.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:17a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
