/*************************************************************************************************
 *
 * ddbCmdList.h
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		generated from from a portion of previous version of ddbItemLists.h
 *
 * #include "ddbCmdList.h"
 */

#ifndef _DDBCMDLIST_H
#define _DDBCMDLIST_H

#include "ddbItems.h"	/* loads all the includes for items almost all the uncludes)*/
#include "ddb.h"

#include "ddbItemListBase.h"

#ifdef _INVENSYS
#define MAX_CMD_SKIP	0 
#else
#define MAX_CMD_SKIP	2   /* send it every MAX_CMD_SKIP times it is tried sequentially */
#endif

class CCmdList : public CitemListBase<iT_Command, hCcommand>
{
	int skipCount;// dispatcher skip count

public:
	// to support anti windup, we record the last command/transaction here
	int				lastBestCmdNum;
	int				lastBestTransN;
	indexUseList_t	lastIndexSet;

public:
	CCmdList(DevInfcHandle_t h) : CitemListBase<iT_Command, hCcommand>(h)
	{lastBestCmdNum = -1; lastBestTransN=-1;};

	RETURNCODE getBestWrite
			   (hCcommand*& pCmd, int& transNum, indexUseList_t& useIdx, cmdDescList_t* pList);
	RETURNCODE getBestRead 
			   (hCcommand*& pCmd, int& transNum, indexUseList_t& useIdx, cmdDescList_t* pList);
	hCcommand* getCmdByNumber(int cmdNumber);

	void insertWgtVisitor(hCcmdDispatcher* pDispatch);
	RETURNCODE BuildIndexMap(void);// index optimization


};

#endif	//_DDBCMDLIST_H
/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
