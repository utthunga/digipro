/*************************************************************************************************
 *
 * $Workfile: ddbChart.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbChart.h".
 */

#ifndef _DDB_CHART_H
#define _DDB_CHART_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"

#include "ddbGrpItmDesc.h"
#include "sysEnum.h"
#include "logging.h"


/*************************************************************************************************
 * chart Type enum declaration
 *************************************************************************************************
 */
/* memory location of strings */
extern const char chartTypeStrings[CHARTTYPESTRCOUNT] [CHARTTYPEMAXLEN]; // in ddbChart.cpp
/* this enum type::	  enum type,  zeroth string, highest vale,    array string length*/
typedef  hCsystemEnum<chartType_t,hCddlLong,cT_Undefined,cT_UNKNOWN,CHARTTYPEMAXLEN> chart_Type_t;

/* stream io for the enum */
inline ostream& operator<<(ostream& ostr,	chart_Type_t& it)\
{  ostr << it.getSysEnumStr();	return ostr; }	;

/************************************************************************************************/



class hCchart : public hCitemBase 
{
	chartType_t       chartype;	    // required - not allowed to be conditional
	hCattrGroupItems* pMemberAttr;	// required

	hCattrGrafixSize* pChartHgt;	//aCattrCondLong
	hCattrGrafixSize* pChartWid;	//aCattrCondLong
	hCattrExpr*       pLength;		//aCattrCondExpr
	hCattrExpr*       pCycleTime;	//aCattrCondExpr

public:
	hCchart(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCchart(hCchart*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCchart()  {/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 

	CValueVarient getAttrValue(unsigned attrType, int which = 0);

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(CHART_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(CHART_LABEL, l); };

	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);

	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual );

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Chart Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Chart ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};
	/*Vibhor 011104: Start of Code*/


	RETURNCODE getHeight(unsigned & height); // gives Chart's height

	RETURNCODE getWidth(unsigned & width);	// gives Chart's width

	chartType_t getChartType(); // Returns the type of Chart

	RETURNCODE getLength(CValueVarient & cv); // Returns the duration of the chart display

	RETURNCODE getCycleTime(CValueVarient & cv); // Returns the frequency of sampling the data

	RETURNCODE getSourceList(hCgroupItemList& memberList); //returns a resolved list of members (sources)

	/*Vibhor 011104: End of Code*/
	RETURNCODE getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs);//5jul06 sjv
	hCattrGroupItems* getMemberList(void){ 
	#ifdef TOKENIZER
		return pMemberAttr;
	#else
		return NULL;
	#endif
		};

	
	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	void fillContainList(ddbItemList_t&  iContain); // stevev 27sep06
	int  notification(hCmsgList& msgList,bool isValidity);
};

#endif	// _DDB_CHART_H
