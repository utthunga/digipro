/*************************************************************************************************
 *
 * $Workfile: ddb_XmtrDisp.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		This is the code for a XMTR-DD dispatcher 
 */



#include "XmtrComm.h"
#include "ddb_XmtrDispatch.h"


//#include "ddbItemLists.h"
//typedef vector<hCVar*> varPtrList_t;
#include "ddbItemBase.h"
#include "ddbItemListBase.h"

#include "ddbCmdList.h"
#define XMTR
#include "ddbCommand.h"

// INSTEAD OF THE ENTIRE ddbDeviceMgr.h file
extern unsigned long getDataItem2Transfer(unsigned char uc);// from port2item map

/************************************************************************************
*/
//class CxmtrDispatcher : public hCcmdDispatcher

//	hCdeviceSrvc*	pMyDevice; 	     // init at at construction from handle
//	hCmsgCycleQueue theMsgQueue;
//	hCcommInfc*     pCommInterface;  
//	hCmutex         commMutex;       // linked from application
//	hCmutex         sendMutex;       // linked from application(to make atomic active commands)

CxmtrDispatcher::CxmtrDispatcher(hCcommInfc* outComm, DevInfcHandle_t thisHandle )
	: hCcmdDispatcher(outComm, thisHandle)
{
	pCommInterface = outComm; 
	if ( outComm != NULL)
	{
		STARTLOG << " - CxmtrDispatcher instantiate - setting comminfc 'setRcvService'."<<ENDLOG;
		outComm->setRcvService(this);
	}
	/*
	ourPolicy.allowSyncWrites = 1;
	ourPolicy.allowSyncReads  = 1;
	ourPolicy.incrementalWrite= 0;
	ourPolicy.incrementalRead = 0;
	ourPolicy.isReplyOnly     = 1; // we are a Server (device)
	*/
	hSdispatchPolicy_t policy;
	policy.allowSyncWrites = 1;
	policy.allowSyncReads  = 1;
	policy.incrementalWrite= 0;
	policy.incrementalRead = 0;
	policy.isReplyOnly     = 1;
	policy.isPartOfTok     = 0;
	setPolicy(policy);
}

RETURNCODE   //  blocking  send
CxmtrDispatcher::
	Send   (hCcommand* pCmd, ulong transNum, bool actionsEn, bool isBlking, 
				hCevent* peDone, hIndexUse_t* pIdxUse)
{
	RETURNCODE rc = SUCCESS;
	varPtrList_t ipL;
	
	//cerr << "ERROR: XMTR-DD called a blocking send."<<endl;
	// just make 'em all valid
	pCmd->getReplyData4Trans(transNum, *pIdxUse, ipL);
	if ( ipL.size() > 0 )
	{
		for ( varPtrList_t::iterator iT = ipL.begin(); iT < ipL.end();iT++)
		{//iT is ptr 2 ptr 2 hCVar
			(*iT)->markItemState(IDS_CACHED);
		}
	}
	return rc;
}
/**************** this was removed from base class, removed here 18may07 by stevev
RETURNCODE   //  blockable send
CxmtrDispatcher::
	SvcSend(hCcommand* pCmd, ulong transNum, hIndexUse_t& useIdx)
	//hCcommand* pCmd, ulong transNum, bool actionsEn, bool isBlking, 
	//										hCevent* peDone, hIndexUse_t* pIdxUse)
{
	RETURNCODE rc = FAILURE;
//	cmdInfo_t cmdStatus;

	if (pCommInterface == NULL)
	{
		return rc;
	}
	// else send it
	hPkt* pPkt = pCommInterface->getMTPacket(false);// do not pend

	if ( pPkt != NULL )
	{
	// call the pCmd->buildCmd(transNum, pPkt);
	// @ success
	//   call pCommInterface->SendPkt(pPkt,2000,cmdStatus);
		clog << ">>>>SvcSend needs some code."<<endl;
	}

	return rc;
}
******************* end removal 18may07 *******************/

RETURNCODE   //  decide next queue service cmd-used below
CxmtrDispatcher::
	getNextQueueTransaction(hCcommand* pReturnedCmd, ulong& returnedTransNum)
{
	RETURNCODE rc = FAILURE;
	cerr << "ERROR: XMTR-DD called getNextQueueTransaction."<<endl;
	return rc;
}

bool CxmtrDispatcher::ServiceWrites(void)    // uses SvcSend     
{
	cerr << "ERROR: XMTR-DD called ServiceWrites."<<endl;
	return false;
}

bool CxmtrDispatcher::ServiceReads(void)     // uses SvcSend 
{
	cerr << "ERROR: XMTR-DD called ServiceReads."<<endl;
	return false;
}

									 // for VBC hCsvcRcvPkt (passed to commInfc)
void CxmtrDispatcher::serviceReceivePacket(hPkt* pPkt, cmdInfo_t* pCmdInfo)
{// XMTR-DD Does EVERYTHING HERE!!!!!!!!---Pkt is a Request pkt!
	RETURNCODE rc   = SUCCESS;
	hCcommand* pCmd = NULL;
	hPkt*   pSndPkt = NULL;
	varPtrList_t    varChangedList;
	int transNum    = -1;
	int commdNum    = -1;
	cmdInfo_t  cStat;      cStat.clear();

	LOGIF(LOGP_START_STOP)(CLOG_LOG," - serviceReceivePacket len = %d\n",((int)pPkt->dataCnt));
	// get command list
	CCmdList* pCmds = (CCmdList*)devPtr()->getListPtr(iT_Command);// devPtr is never NULL
	if ( pCmds == NULL || pCommInterface == NULL ) // no access to the device or communications
	{
		cerr << "ERROR: No access to the device from serviceReceivePacket."<<endl;
		//return; // we can do nothing
	}
	else
	{	// get command
		CXmtrComm* pXcommIfc = (CXmtrComm*)pCommInterface;
		for(vector<hCcommand*>::iterator iT = pCmds->begin();iT<pCmds->end();iT++)
		{// iT is a Ptr2Ptr2 hCommand
			if ( (*iT)->getCmdNumber() == pPkt->cmdNum )
			{
				pCmd = *iT;
				commdNum = pPkt->cmdNum;
				break; // out of iterator loop;
			}
			// else keep looking
		}// next command

		if ( pCmd != NULL ) // we found one
		{
			LOGIF(LOGP_START_STOP)(CLOG_LOG," - received command %d\n",pCmd->getCmdNumber());

			bool tryAgain = false;
			transNum = -1;
			pSndPkt  = NULL;

			do
			{ // while tryAgain (set internally)
				// get transaction match
				transNum = pCmd->getTransNumbByRequestPkt(pPkt,transNum);
				// parse & update request packet
				if ( transNum >= 0 )
				{	
					STARTLOG <<" -          transaction " << transNum <<ENDLOG;
					
					if (commdNum == 111 || commdNum == 112)
					{
						serviceTransferPacket(pPkt, pCmd, transNum);
						tryAgain = false; // be sure we don't
						break;// out of do-while
					}

					if ( pSndPkt == NULL )
					{
						pSndPkt = pCommInterface->getMTPacket(true);
					}
					if (pSndPkt == NULL ) // packet error
					{
						cerr << "ERROR: No packet aquired for response." << endl;
						pXcommIfc->sendErrorPkt(pPkt,rc_XmitrCmdErr,DS_DEVMALFUNCTION);
						tryAgain = false;// be sure we don't
					}
					else
					{
						hCtransaction* pTran = pCmd->getTransactionByNumber(transNum);
						if (pTran == NULL)
						{
							cerr << "ERROR: Transaction "<< transNum << " NOT found in Cmd " <<
																		   pPkt->cmdNum<< endl;
							pXcommIfc->sendErrorPkt(pPkt,rc_XmitrCmdErr,DS_DEVMALFUNCTION);
							pCommInterface->putMTPacket(pSndPkt);
							tryAgain = false;// be sure we don't
						}
						else
						{	// extract & handle request Packet!!!!!!!!!!!!!!!!!!!!!		
							indexUseList_t infoList;
							int rep = pTran->extractRequestPkt(pPkt, varChangedList, infoList,
														   (pCmd->getOperation()==cmdOpWrite));
							if ( rep < 0 )// error
							{
								LOGIT(CERR_LOG,
									   L"ERROR: transaction failed parsing request packet.\n");
								if (! tryAgain )
								{
									tryAgain = true;
								}
								else
								{
									tryAgain = false;// try only once more
								 pXcommIfc->sendErrorPkt(pPkt,rc_InvalidSel,DS_DEVMALFUNCTION);
								}
							}
							else
							{
								// execute   POST_RQSTRECEIVE_ACTIONS   here ***************<<<
								pTran->doRcvActs();

								tryAgain = false;// never retry if we get here
								// continue to generate reply
								STARTLOG <<" - extracted " << rep 
														 <<" items from Request Pkt." <<ENDLOG;
								// generate a reply packet
								if ( pTran->generateReplyPkt( pSndPkt) != SUCCESS )
								{
									cerr << "ERROR: Transaction "<< transNum 
										 << " NO REPLY in Cmd "  << pPkt->cmdNum<< endl;
									pXcommIfc->sendErrorPkt
										               (pPkt,rc_XmitrCmdErr,DS_DEVMALFUNCTION);
									pCommInterface->putMTPacket(pSndPkt);
								}
								else
								{// call send for it -- will take ownership of the packet
									pTran->restoreINFOitems(infoList);
									
									STARTLOG <<" - sending reply packet... " <<ENDLOG;
									pSndPkt->cmdNum = pCmd->getCmdNumber();
									if ( (rc = pCommInterface->SendPkt(pSndPkt, -1, cStat)) 
										                                            != SUCCESS)
									{
										cerr << "ERROR: Reply to Cmd " << pPkt->cmdNum <<
																" has a SEND ERROR:"<< rc << endl;
										pCommInterface->putMTPacket(pSndPkt);
										// nothing else we can do
									}
									else // - OK exit
									{
										STARTLOG <<"*SentPkt to commIfc successfully."<<ENDLOG;
									}
								}
							}// end post-extraction
							// restore 
							if ( infoList.size() > 0 )
							{// restore the values
							}
						}// end have-transaction
					}
				}
				else
				{
					cerr << "ERROR: No transaction found for cmd " << pPkt->cmdNum << endl;
					pXcommIfc->sendErrorPkt(pPkt,(response5Code_t)-transNum,DS_NO_STATUS);
					tryAgain = false;// be sure we don't
				}
			}
			while(tryAgain);
		}
		else
		{
			cerr << "ERROR: No command item found for pkt cmd " << pPkt->cmdNum << endl;
			pXcommIfc->sendErrorPkt(pPkt,rc_CmdNotImpl,DS_NO_STATUS);// command not supported
		}
	}
}


//void CxmtrDispatcher::ReceiveTaskFunction(void)  // the receive function that runs as a task
//{// XMTR-DD Does EVERYTHING HERE!!!!!!!!
//	clog << ">>>>ReceiveTaskFunction needs some code."<<endl;
//}

RETURNCODE  		//  used by the items via funcPtr
CxmtrDispatcher::
	getBestItemTransaction(itemID_t id, hCcommand* pReturnedCmd, 
									ulong& returnedTransNum, hIndexUse_t& retIdx)
{
	RETURNCODE rc = FAILURE;
	cerr << "ERROR: XMTR-DD called getBestItemTransaction."<<endl;
	return rc;
}

					//  used by the transactions via funcPtr  
RETURNCODE  CxmtrDispatcher::
	calcWeight(hCtransaction* pThisTrans,  hIndexUse_t& retIdx, bool isRead)
{
	RETURNCODE rc = FAILURE;
	cerr << "ERROR: XMTR-DD called calcWeight."<<endl;
	return rc;
}

void CxmtrDispatcher::servicePacketPair(hPkt* pRqstPkt, hPkt* pRplyPkt) 
{
	RETURNCODE rc   = SUCCESS;
	varPtrList_t    varChangedList;
	hCcommand* pCmd = NULL;
	int transNum    = -1;

	methodCallList_t methList;
	hCmsgList        mesgList;mesgList.isRqstPkt = false;// only passed in for reply

	if (pRqstPkt == NULL || pRplyPkt == NULL || pRqstPkt->cmdNum != pRplyPkt->cmdNum)
	{
		LOGIT(CERR_LOG,"ERROR: SvcPktPr:bad parameters in servicePacketPair.\n");
		return;
	}
		// get command list
	CCmdList* pCmds = (CCmdList*)devPtr()->getListPtr(iT_Command);// devPtr is never NULL
	if ( pCmds == NULL || pCommInterface == NULL ) // no access to the device or communications
	{
		LOGIT(CLOG_LOG,"SvcPktPr:ERROR: No access to the device from serviceReceivePacket.\n");
	}
	else
	{	// get command
		CXmtrComm* pXcommIfc = (CXmtrComm*)pCommInterface;
		for(vector<hCcommand*>::iterator iT = pCmds->begin();iT<pCmds->end();iT++)
		{// iT is a Ptr2Ptr2 hCommand
			if ( (*iT)->getCmdNumber() == pRqstPkt->cmdNum )
			{
				pCmd = *iT;
				break; // out of iterator loop;
			}
			// else keep looking
		}
		if ( pCmd != NULL ) // we found one
		{
			transNum = -1;
			// get transaction match
			transNum = pCmd->getTransNumbByRequestPkt(pRqstPkt,transNum);
			// parse & update request packet
			if ( transNum >= 0 )
			{	
				indexUseList_t infoList;
				hCtransaction* pTran = pCmd->getTransactionByNumber(transNum);
				if (pTran == NULL)
				{
					LOGIT(CERR_LOG,"ERROR: SvcPktPr:Transaction %d NOT found in Cmd %d.\n", 
																 transNum, pRqstPkt->cmdNum);
				}
				else
				{	// extract & handle request Packet!!!!!!!!!!!!!!!!!!!!!	
					// we have to do both packets because some devices are misbehaved and
					// do not echo the values used in their replies meaning the request
					// can change a value that will not show in the reply.
					int rep = pTran->extractRequestPkt(pRqstPkt,varChangedList, infoList,
														(pCmd->getOperation()==cmdOpWrite)  );
					if ( rep < 0 )// error
					{
						LOGIT(CERR_LOG,"ERROR: SvcPktPr:transaction failed to parse "
																		"request packet.\n");
					}
					else
					{	// extract & handle reply Packet!!!!!!!!!!!!!!!!!!!!!
						int rep = pTran->extractReplyPkt(pRplyPkt, methList, mesgList,varChangedList );
						if ( rep < 0 )// error
						{
							LOGIT(CERR_LOG,"ERROR: SvcPktPr:transaction failed to parse "
																			"reply packet.\n");
						}
						else
						{	// it all worked !!!!!!!!!!!!!!!!!!!!!
						// discard the lists, xmtr don't do that
							methList.clear();
							mesgList.clear();						
						}	
					}//end else-do reply pkt too	
				}// end else - we got a transaction
			}
			else // no tranction number
			{
				LOGIT(CERR_LOG,"ERROR: SvcPktPr:No transaction found for cmd %d\n",
																		 pRqstPkt->cmdNum);
			}// transaction complete
		}
		else // no command
		{	// command in comm log not supported in the DD (mismatch)		
			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: SvcPktPr:No command item found for pkt cmd %d.\n",
																			  pRqstPkt->cmdNum);
		}
	}// end-else we have all the pointers...
}
/*************************************************************************************************
 *  serviceTransferPacket - lowest level session and transport layer handling
 *					we overide the base class to do simulation (acquire request & generate reply)
 *************************************************************************************************
 */
// DEVICE_SIDE  message handler for Block Transfer capability...we are getting the request packet
void CxmtrDispatcher::serviceTransferPacket(hPkt* pPkt, hCcommand* pCmd, int transNum)
{
	RETURNCODE rc = SUCCESS;		
	bufChunk_t locChunk;
	cmdInfo_t  cStat;      cStat.clear();
	hCtransaction* pTran = pCmd->getTransactionByNumber(transNum);
	varPtrList_t    varChangedList;
	if (pTran == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: Transaction %d NOT found in Cmd #%d\n",transNum, pPkt->cmdNum);
		((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_XmitrCmdErr,DS_DEVMALFUNCTION);
	}
	else // we have a transaction
	if (pCmd->getCmdNumber() == 111)
	{// SESSION HANDLING
		if ( pPkt->dataCnt < 5 )
		{
			LOGIT(CERR_LOG,"ERROR: Cmd 111 Request too Short\n");
			((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_TooFewBytes,DS_NO_STATUS);
		}
		else
		{//this is simulator so cmd 111,request pkt:
			
			indexUseList_t infoList;

			int rep = pTran->extractRequestPkt(pPkt, varChangedList, infoList, true);
							
			pTran->doRcvActs();

			hCTransferChannel* pChannel = getPort( pPkt->theData[0] );// we do not own this memory

			if ( pChannel == NULL ) // we are out of memory
			{
			   ((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_11InvalidMode,DS_NO_STATUS);
				return;
			}
			else
			if( ! pChannel->isSupported())
			{
				((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_InvalidSel,DS_NO_STATUS);
				return;
			}
			// else not null and supported
			
			pChannel->respCd          =
			pChannel->devStat         = 0;// clear these on incoming packet
			pChannel->portNumber      = pPkt->theData[0];
			pChannel->funcCd          = pPkt->theData[1];
			pChannel->max_segment_len = pPkt->theData[2];
			pChannel->masterSync      = (pPkt->theData[3]<<8) + (pPkt->theData[4]);

			//--if (pPos != portList.end() )
			if (  pChannel->Session_State == ss_Opened || 
				  pChannel->Session_State == ss_Opening  )
			{// channel found :: port is opened 
				response5Code_t rcV = rc_NoError;

				if ( pChannel->funcCd == tf_Transfer )
				{//	not part of session???
					rcV = rc_InvalidSel;
				}
				else
				if ( pChannel->funcCd == tf_PortClosed )
				{
					rcV = rc_InvalidSel;
				}
				else
				if ( pChannel->funcCd == tf_OpenPort )
				{
					rcV = rc_10ValTooLo;
				}
				if (rcV)
				{
					((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rcV,DS_NO_STATUS);
					return;
				}

				// to get here we have a valid function code, we will reply
				if ( pChannel->funcCd == tf_ResetPort )
				{
					pChannel->reset();// will reset the port support
					pChannel->funcCd = tf_PortClosed;
				}
				else
				if ( pChannel->funcCd == tf_Ready2Close )
				{
					if (pChannel->transferComplete())
					{
						pChannel->close();
					}
					else
						pChannel->funcCd = tf_Transfer;
				}
				
				hPkt*   pSndPkt = pCommInterface->getMTPacket(true);;
				pChannel->replySession(pSndPkt,pTran);// will call support group
				if ( (rc = pCommInterface->SendPkt(pSndPkt, -1, cStat))!= SUCCESS)
				{
					LOGIT(CERR_LOG,"ERROR: Reply to Cmd %d  has a SEND ERROR:: %d\n",
																		pPkt->cmdNum, rc);
					pCommInterface->putMTPacket(pSndPkt);
					// nothing else we can do
				}
				else // - OK exit
				{
					STARTLOG <<"*SentPkt to commIfc successfully."<<ENDLOG;
				}
			}
			else
			{// session is::> undefined, closing, closed
				if ( pChannel->funcCd == tf_OpenPort )
				{
					if (pChannel->max_segment_len == 0)
					{				
					((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_12InvalidUnit,DS_NO_STATUS);
					}
					else
					{
						hPkt*   pSndPkt;
						pChannel->open();
						pChannel->funcCd = tf_OpenPort;
						pSndPkt = pCommInterface->getMTPacket(true);
						//hCtransaction* pTran = pCmd->getTransactionByNumber(0);
						pChannel->replySession(pSndPkt,pTran);// will call support group
						if ( (rc = pCommInterface->SendPkt(pSndPkt, -1, cStat))!= SUCCESS)
						{
							LOGIT(CERR_LOG,"ERROR: Reply to Cmd %d  has a SEND ERROR: %d\n",
																				pPkt->cmdNum, rc);
							pCommInterface->putMTPacket(pSndPkt);
							// nothing else we can do
						}
						else // - OK exit
						{
							STARTLOG <<"*SentPkt to commIfc successfully."<<ENDLOG;
						}
					}
				}
				else // nobody else is useful to a closed port
				{					
					((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_InvalidSel,DS_NO_STATUS);
				}
			}// endelse
		}// endelse
	}
	else// we have a transaction AND it is NOT cmd 111
	if (pCmd->getCmdNumber() == 112)//  be sure its 112
	{//TRANSPORT HANDLING
	// if we are starting, snapshot the data into a list of buffers of segment size
	//  deal with retry requests (lost packet in the middle of the transfer)
		hCTransferChannel* pChannel = getPort( pPkt->theData[0] );// we do not own this memory

		if ( pChannel == NULL ) // we are out of memory
		{
		   ((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_11InvalidMode,DS_NO_STATUS);
			return;
		}
		else
		if( ! pChannel->isSupported())
		{
			((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_InvalidSel,DS_NO_STATUS);
			return;
		}
		// else not null and supported

		pChannel->respCd          =
		pChannel->devStat         = 0;// clear these on each incoming packet
		pChannel->portNumber      = pPkt->theData[0];
		pChannel->funcCd          = pPkt->theData[1];
		locChunk.len              = pPkt->theData[2];
		locChunk.masterStart      = (pPkt->theData[3]<<8) + (pPkt->theData[4]);
		locChunk.slaveStart       = (pPkt->theData[5]<<8) + (pPkt->theData[6]);

		int x,y;
		for ( x = 0, y = 7; x < locChunk.len; x++, y++)
		{
			locChunk.data[x] = pPkt->theData[y];
		}

		//--if (pPos != portList.end() )
		if (  pChannel->Session_State == ss_Opened )
		{// ongoing packets
		}
		else
		if (  pChannel->Session_State == ss_Opening )// this is first packet
		{
			pChannel->Session_State = ss_Opened;
			// we probably should read the first packet, but not on this prototype
			// fill the buffers (snapshot the data)
			// assume we are channel 2
			string lS("device_configuration");
			hCitemBase* pItem = NULL;
			if ( devPtr()->getItemBySymName( lS , &pItem) == SUCCESS && pItem != NULL )
			{			
				pChannel->fillPortBuffer(pItem);
				// finish the process by updating the master counts
				unsigned short mS = pChannel->startMasterSync;// the next start
				for ( bufChunkIter_t iT = pChannel->xmit_Buffer.begin(); 
														iT != pChannel->xmit_Buffer.end(); ++ iT )
				{// ptr2abufChunk_t
					((bufChunk_t*)&(*iT))->masterStart = mS;
					mS += ((bufChunk_t*)&(*iT))->len;
				}
			}
			else
			{// generate an error
			((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_XmitrCmdErr,DS_NO_STATUS);
			return;
			}
		}
		else // port not ready
		{// not open error
			((CXmtrComm*)pCommInterface)->sendErrorPkt(pPkt,rc_9_ValTooHi,DS_NO_STATUS);
			return;
		}


		hPkt*   pSndPkt;
		if ( pChannel->funcCd == tf_Transfer )
		{
			pSndPkt = pCommInterface->getMTPacket(true);
			//hCtransaction* pTran = pCmd->getTransactionByNumber(0);
			pChannel->uploadData(pSndPkt,locChunk.masterStart);// will call support group
			if ( (rc = pCommInterface->SendPkt(pSndPkt, -1, cStat))!= SUCCESS)
			{
				LOGIT(CERR_LOG,"ERROR: Reply to Cmd %d  has a SEND ERROR: %d\n",
																	pPkt->cmdNum, rc);
				pCommInterface->putMTPacket(pSndPkt);
				// nothing else we can do
			}
			else // - OK exit
			{
				if (lE) LOGIT(CLOG_LOG,"*SentPkt to commIfc successfully.\n");
			}
		}
		else
		{// wrong mode
		}



	}
	else// we have a transaction AND it is NOT cmd 111 and NOT cmd 112
	{// we have a transaction and the command isn't special (we shouldn't be here)
	//try to extract it.
	}//endelse we have a transaction

}// end serviceTransferPacket

