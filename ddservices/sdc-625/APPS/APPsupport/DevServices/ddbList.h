/*************************************************************************************************
 *
 * $Workfile: ddbList.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003 
 *		
 * #include "ddbList.h".
 */

#ifndef _DDB_LIST_H
#define _DDB_LIST_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "ddbGrpItmDesc.h"
#include "ddbVar.h"

#define LIST_MAX_ALLOWED		5000
/*
listAttrLabel		LIST_LABEL_ID		aCattrString
listAttrHelp,		LIST_HELP_ID		aCattrString
listAttrValidity,	LIST_VALID_ID		aCattrCondLong

listAttrType,		LIST_TYPE_ID		aCattrCondReference	-- required
listAttrCount,		LIST_COUNT_ID		aCattrCondExpr
listAttrCapacity,	LIST_CAPACITY_ID	aCattrCondLong	- non-cond in a integer conditional 
*/
class hClist : public hCitemBase 
{
	hCreference  typedefRef;// for reference
	hCitemBase*  pTypeDef;  // cannot be conditional so just resolve it once
	hCddlLong    capacityValue;
	bool		 capacityDefined;	// is defined in the DD (else dynamicaly allocated)
	hCattrExpr*	 pCount;			// initial count - NULL if not defined in the DD
	int          iCount;// dynamiclly maintained actual count DO NOT USE DIRECTLY!!!
	bool		 countIsRef; // count is NOT a constant from the DD

	/* pCount	countIsRef	Description
		 NULL		true	erroneous condition  ( has not been thru initTypedef() yet )
		 NULL		false	count fluctuates 0 to Capacity
		 !NULL		true	count fluctuates 0 to Capacity AND echoed to pExternalCount
		 !NULL		false	count is frozen (list is deprecated to a value-array)
	 **/
#define COUNT_ISFREE  (pCount==NULL && !countIsRef)
#define COUNT_ISECHO  (pCount!=NULL &&  countIsRef)
#define COUNT_ISFROZE (pCount!=NULL && !countIsRef)

	ddbItemList_t activeIL;// the actual data - vector of itembase ptrs
	// the active count is activeIL.size();
	
	// 03aug06 - added to allow attribute resolution via Reference ( we own the memory )
	hCinteger*	pPsuedoCnt;		// the count's		psuedo Variable w/o shared attributes
	hCinteger*	pPsuedoCap;		// the capacity's	psuedo Variable w/o shared attributes
	// first and last are references, not values!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	hCNumeric*  pExternalCount;	// from spec's polymorhism of an expression

	/* stevev 31dec08 - add generic command references */
	// note, elements will have command intersection + values
	cmdDescList_t rdCmd_IndexList;
	cmdDescList_t wrCmd_IndexList;

	// the list has to know about its indexes so when its empty, they all go invalid
	ddbItemList_t  listOindex;// list of pointers to Items that are hCindex type

	void appendUnique(ptr2hCitemBase_t pIdx);
	void setIdxValidity(bool newV);


protected:
	hCinteger* getPsuedoInt(int initialValue);// helper function

//	RETURNCODE    insert(hCgroupItemDescriptor& element, int& idx);// helper
	void addDesc(cmdDescList_t& targetList, hCcommandDescriptor& cD);

public:
	hClist(DevInfcHandle_t h, aCitemBase* paItemBase);
	hClist(hClist* pSrc, hClist* pVal, itemIdentity_t newID);
	RETURNCODE destroy(void)// 10sep14 memory leak fix
	{ 
		activeIL.clear();
		if ((pCount)!= NULL)
		{
			pCount->destroy();
			delete pCount;
			pCount = NULL;
		}
		return hCitemBase::destroy(); 
	};
	virtual ~hClist();
	
	hCattrBase*   newHCattr(aCattrBase* pACattr)  // builder 
	{LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hClist.newHCattr() called.\n");return NULL;};

	void   initTypedef(hClist* pVals);// initiate: get the psuedo items into the list
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ) 
					{/* for now */returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(LIST_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(LIST_LABEL, l); };

	RETURNCODE Read(wstring &value){/* TODO: read the succker*/ return FAILURE;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------List Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "List ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};


	unsigned long getCapacity(void){ return (UINT32)(capacityValue.getDdlLong()); };
	bool          isCapacityDDdefined() { return capacityDefined;};
	itemID_t      getAttrID   (unsigned attr, int which=0);// 03aug06 the psuedoVariable's ID for refs
	hCVar*        getAttrPtr  (unsigned attr, int which=0);// 03aug06 
	CValueVarient getAttrValue(unsigned attr, int which=0);// 06sep06

	unsigned int  setCount(unsigned  newCnt);// 03aug06 COUNT access - do not access any other
	unsigned int  getCount(void);            // 03aug06       way!!!
	void staleListOnCount(unsigned  latestCount);// stevev 11dec08

	RETURNCODE    insert(hCitemBase* pBase, int& idx);
	RETURNCODE    remove(int& idx);
	void          MakeStale(void);

	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);//match index
	RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes);
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);

	hCitemBase* operator[](int idx0);// returns NULL on bounds error, cast to correct type.
	bool       isInGroup (UINT32 indexValue) {  return (indexValue >= 0 && indexValue < getCount()); };

//	itemType_t    getIType(void);// of typedef- stevev 4may07 changed from this to typedeftype 
								 // - still needs to tell this is a list from getIType in itembase
	itemType_t    getTypedefType(void);	// type of typedef
	itemID_t      getTypedefId(void);// symbol number of typedef
	RETURNCODE getList(ddbItemList_t& r2diL) 
	// 	{r2diL = activeIL; return (r2diL.size() != activeIL.size());};
	{ 
#if defined(__GNUC__) || _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
		std::vector<hCitemBase*>::iterator anIT;
#else		// note these are exactly the same...???
		ddbItemLst_it anIT = NULL;
#endif
		for (anIT  = activeIL.begin(); anIT != activeIL.end(); ++anIT )
		{ 
			r2diL.push_back( *anIT );	
		}	
		return (r2diL.size() != activeIL.size());
	};
	
	/* stevev 31dec08 - add generic command references */
	void addRDdesc(hCcommandDescriptor& rdD);
	void addWRdesc(hCcommandDescriptor& wrD);

	/* stevev 22jan09 */
	RETURNCODE setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue);
	RETURNCODE addCmdDesc(hCcommandDescriptor& rdD, bool isRead);
	RETURNCODE resetIndexValues(void);//setCommandIndexes on each existing element

	/* stevev 07jan09 - forIndex validity maintainance */
	void addIndex(hCindex* pIndex);

	/*
	RETURNCODE Read(string &value){value = "ERROR:Not a Var"; return FAILURE;};
	RETURNCODE getAllindexes(UIntList_t& allindexedIDsReturned);// all the items possibly indexed
add instantiation at construction if count exists & > 0

	RETURNCODE getGroupItemPtrs(groupItemPtrList_t& giL );// adds 'em to the end of the list
	RETURNCODE getItemIDs(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemIndexes(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemByIdx(unsigned long idx, 
							hCgroupItemDescriptor& returnedElement, bool suppress = false);
	bool       isItemInList(unsigned long idx);
		
	/ * payload required methods * /
	void  setEqual(void* pAclass);
	boolT validPayload(payloadType_t ldType) // actually isPayloadType()
					{ if (ldType != pltGroupItemList) return isFALSE; else return isTRUE;};
	/ * end payload methods  * /

	RETURNCODE getItemPtrs(ddbItemList_t & ptrList); //Vibhor 131004: Added

  */
	hCitemBase*  getTypePtr(void) { return pTypeDef; };// needed for complex type comparison
};


#endif	// _DDB_LIST_H
