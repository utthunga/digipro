/*************************************************************************************************
 *
 * $Workfile: grafix.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of those tiny little classes that help the structure
 *
 *		09/01/04	sjv	created
 */
#include "grafix.h"


/* memory allocation for the strings */
const char grafSizeStrings[GRAFSIZESTRCOUNT] [GRAFSIZEMAXLEN] = {GRAFSIZESTRINGS};
const char lineTypeStrings[LINETYPESTRCOUNT] [LINETYPEMAXLEN] = {LINETYPESTRINGS};
const char scaleStrings   [SCALESTRCOUNT]    [SCALEMAXLEN]    = {SCALESTRINGS};



string hCattrLineType::getLineTypeString(void)
{
	Resolved.clear();
	if ( condLineType.resolveCond(&Resolved) == SUCCESS )
	{ 
		ulong Int; int Which;
		Int = (INT32)Resolved.getDdlLong(Which);// very limited value set
		if ( Int == lT_Data )
		{
			if (Which > 0 && Which < (lT_END-lineTypeOffset))
			{
				return (lineTypeStrings[Which+lineTypeOffset]);
			}
			else
			{
				return (lineTypeStrings[Int]);// therestare data0
			}
		}
		else
		{
			if ( Int <= lT_Unknown )
			{
				return (lineTypeStrings[Int]);
			}
			else
			{
				return (lineTypeStrings[lT_Undefined]);
			}
		}
	}
	else
	{
		cerr<<"ERROR: hCddlIntWhich: would not resolve conditional."<< endl;
		return ("");
	}
};


int   hCattrLineType::getEnumStrList(vector<string>& retList) // returns 0 at no error
{
	string tmpStr;
	for ( int i = 0; i < LINETYPESTRCOUNT; i++)
	{
		tmpStr = lineTypeStrings[i];
		retList.push_back(tmpStr);
	}
	return 0;
}

hCattrBase*   hCattrLineType::new_Copy(itemIdentity_t newID)
{
	hCattrLineType * pAB = new hCattrLineType (devHndl(),pItem);
	pAB->condLineType.setDuplicate( condLineType, false ); // attribute is not dup'd
	return pAB;
}

/************************************************************************************************/
hCattrBase* hCattrRefList::new_Copy(itemIdentity_t newID)
{
	hCattrRefList * pAB = new hCattrRefList (devHndl(),attr_Type,pItem);
	pAB->condRefList.setDuplicate( condRefList, false); // only used in waveforms - not dup'd

	return pAB;
}

RETURNCODE hCattrRefList::getDepends(ddbItemList_t& retList)
{
	RETURNCODE rc = SUCCESS;
	
	// get what the conditional depends on
	condRefList.aquireDependencyList(retList, false);

	/* also depends on the conditional and/or validity of its items */
	vector<hCreferenceList*>       localpRefL;
	vector<hCreferenceList*>::iterator ipRefL;

	HreferencePtrList_t        localRefpL;// complete list of pointers to references
	HreferencePtrList_t::iterator  iRefpL;

	ddbItemList_t           localIpL;
	ddbItemLst_it           iIpLst;

	hCdependency* pDep = NULL;
/*
condRefList condList of hCreferenceList
    isa List of Conditionals { condList | hCreferenceList }
	    isa Expression with destList for THEN|ELSE|CASE|DEFAULT 
		    each Dest points to { condList | hCreferenceList }
a hCreferenceList isa HreferenceList_t
   HreferenceList_t isa vector of hCreference
	
*/
	// get what the resolution depends on: the reference and the item's validity
	if (condRefList.aquirePayloadPtrList(localpRefL) == SUCCESS && localpRefL.size() > 0)
	{// we have a list of all possible ptrs to hCreferenceList's
		for (ipRefL = localpRefL.begin(); ipRefL != localpRefL.end();  ++ipRefL )
		{// ptr 2a ptr 2a hCreferenceList
			(*ipRefL)->getRefPointers(localRefpL);// appends onto end
		}
		// have a list of pointers to all possible references
		for (iRefpL = localRefpL.begin(); iRefpL != localRefpL.end(); ++iRefpL  )
		{// ptr 2a ptr 2a hCgroupItemDescriptor
			(*iRefpL)->aquireDependencyList(retList,false);// what the reference depends on
			(*iRefpL)->resolveAllIDs(localIpL);// all possible ids
		}
	}
	// we have all possible resolved items now
	for (iIpLst = localIpL.begin(); iIpLst != localIpL.end(); ++ iIpLst )
	{// ptr 2a ptr 2a hCitemBase
		pDep = (*iIpLst)->getDepPtr(ib_ValidityDep);
		if ( pDep && pDep->iDependOn.size() > 0)
		{// pDep->iDependOn is a list of what this item's validity is dependent on (MT @ constant)
			hCdependency::insertUnique(retList,(*iIpLst));// append list uniquely <a static helper>
		}
	}
	return rc;
}

void hCattrRefList::fillContainList(ddbItemList_t& retList)
{
	RETURNCODE rc = SUCCESS;
	/* also depends on the conditional and/or validity of its items */
	vector<hCreferenceList*>       localpRefL;
	vector<hCreferenceList*>::iterator ipRefL;

	HreferencePtrList_t        localRefpL;// complete list of pointers to references
	HreferencePtrList_t::iterator  iRefpL;

	ddbItemList_t           localIpL;// itmbase ptr list
	ddbItemLst_it           iIpLst;

//	hCdependency* pDep = NULL;

	if (condRefList.aquirePayloadPtrList(localpRefL) == SUCCESS && localpRefL.size() > 0)
	{// we have a list of all possible ptrs to hCreferenceList's
		for (ipRefL = localpRefL.begin(); ipRefL != localpRefL.end();  ++ipRefL )//for each list
		{// ptr 2a ptr 2a hCreferenceList										 // get refs
			(*ipRefL)->getRefPointers(localRefpL);// appends onto end
		}
		// have a list of pointers to all possible references
		for (iRefpL = localRefpL.begin(); iRefpL != localRefpL.end(); ++iRefpL  )
		{// ptr 2a ptr 2a hCreference
			(*iRefpL)->resolveAllIDs(localIpL);// all possible ids
		}
	}
	// we have all possible resolved items now
	for (iIpLst = localIpL.begin(); iIpLst != localIpL.end(); ++ iIpLst )
	{// ptr 2a ptr 2a hCitemBase
		retList.push_back(*iIpLst);
	}
	return;
}

bool hCattrRefList::reCalc(void)
{// error till done

	bool isChanged = false;
	hCreferenceList tempList(m_refList);
	procure(); // refills m_refList
	if (tempList == m_refList)// old == new
	{	isChanged = false;
	}
	else
	{	isChanged = true;
	}
	return isChanged;
}

/*************************************************************************************************
 * image support
 *************************************************************************************************
 */

hCframe& hCframe::operator=( imgframe_t* pAframe )
{  
	cLang  = pAframe->language;     uSize      = pAframe->size;
	uOffset= pAframe->offset;	   
	if ( uSize > 0 )
	{
		pbRawImage = new BYTE[uSize];
		memcpy(pbRawImage,pAframe->pRawFrame, uSize);
	}
	return *this;
};	

void hCimage::clear(void)
{
	for (FramePtrList_it iF = begin(); iF != end(); ++iF)
	{// ptr2aptr2a hCframe
		(*iF)->clear();
	}
}

hCimage::~hCimage()
{
	for (FramePtrList_it iF = begin(); iF != end(); ++iF)
	{// ptr2aptr2a hCframe
		delete (*iF);
		*iF = NULL;
	}
}

void hCframe::clear(void)
{
	cLang.erase();
	uSize      = 0;
	pbRawImage = NULL;
	uOffset    = 0;
}

void hCframe::destroy(void)
{
	RAZE_ARRAY(pbRawImage);//RAZE array DD@F 19aug13
	clear();
}

	
hCimage& hCimage::operator=(const hCimage& s )
{  
	hCframe* plocFrm;

	hCimage* pI = (hCimage *) &s;// iterator requires a non-const class
	for (FramePtrList_it i = pI->begin(); i != pI->end(); ++i )
	{
		plocFrm = *i;
		push_back(plocFrm);
		plocFrm = NULL;
		(*i)->clear();// there can be only one pointer!
	}

	return *this;
};	

hCimage::hCimage(DevInfcHandle_t h, const hCimage& src)
{	
	operator=(src);    
}



/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
