/*************************************************************************************************
 *
 * $Workfile: ddbGrid.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbGrid.h"
 */

#ifndef _DDB_GRID_H
#define _DDB_GRID_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"

#include "ddbGrpItmDesc.h"
#include "sysEnum.h"

extern const char gridOrientStrings[GRIDORIENTSTRCOUNT] [GRIDORIENTMAXLEN];

/* 
	gridGroupDesc 
		completely describes the data inside of a grid row or column
		all of the description is available to the user.
*/
class gridGroupDesc
{
public:
	wstring        headerValue;// row-left || col-top
	varientList_t vectorValues; // data: constants and symbolIDs

	gridGroupDesc(){};
	gridGroupDesc(const gridGroupDesc& src){ operator=(src);};

	gridGroupDesc& operator=(const gridGroupDesc& s){ headerValue  = s.headerValue;
												vectorValues = s.vectorValues;return *this;};

	void clear(void){headerValue.erase(); vectorValues.clear();};
	virtual ~gridGroupDesc(){clear();};
};
/* 
	gridDesc_t 
		completely describes the data inside of a grid
		all of the description is available to the user.
*/
typedef vector<gridGroupDesc>  gridDesc_t;


/*----------------- device object class ---------------------------------------------------------*/

class hCgridGroup : public hCobject
{
	hCddlString      hdr;// row/col header
	hCreferenceList  dataList; // NON conditional reference list

public:
	hCgridGroup(DevInfcHandle_t h):hCobject(h),hdr(h),dataList(h){};
	
	hCgridGroup(const hCgridGroup& gg):
		hCobject(gg.devHndl()),hdr(gg.hdr),dataList(gg.dataList){};

	void destroy(void)
	{
		hdr.destroy();
		dataList.destroy();
	};
	
	void  setEqual(aCgridMemberElement* pAe);
	void  setDuplicate(hCgridGroup& rgg);
	void  clear(void){hdr.clear(); dataList.clear();};

	operator wstring() { return hdr; };

	RETURNCODE getEntryList(varientList_t& valList);// appends resolved reference entries

	RETURNCODE getRefPointers(HreferencePtrList_t& retPtrs)
		{	return(dataList.getRefPointers(retPtrs));	};
};
typedef vector<hCgridGroup>  gridList_t;
typedef gridList_t::iterator itGridGroup;

typedef vector<hCgridGroup*>     grdGrpPtrLst_t;
typedef grdGrpPtrLst_t::iterator  itGrdGrpPtr_t;


class hCGridGroupList : public gridList_t, public hCpayload, public hCobject
{
public:
	hCGridGroupList(DevInfcHandle_t h):hCobject(h){};
	hCGridGroupList(/*const*/ hCGridGroupList& el);

	hCGridGroupList(const hCGridGroupList* pel) : hCobject(pel->devHndl())
	{ 		
		LOGIT(CERR_LOG,">>>>>>>>>>hCGridGroupList pointer copy constructor .\n");
	};
	virtual ~hCGridGroupList()
	{
		destroy();
	};
	RETURNCODE destroy(void)
	{	RETURNCODE rc = 0;
		for(gridList_t::iterator p = begin();p<end(); p++ )
		{	p->destroy();  }
		gridList_t::clear(); return rc;
	};

	
	/* payload required methods */
	void  setEqual(void* pAclass);
	void  duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);
	bool validPayload(payloadType_t ldType) // actually isPayloadType()
					{ if (ldType != pltGridMemberList) return false; else return true;};
	/* end payload methods */
	RETURNCODE dumpSelf(int indent = 0){ 
		LOGIT(COUT_LOG,"%sGridGroupList does not dump at this time.\n",Space(indent));return 0;};

	RETURNCODE getGridDesc(gridDesc_t& r2gD);

	void  append(hCGridGroupList* pList2Append);
	void  appendUnique(hCGridGroupList* pList2Append)
	{// temporary bypass
		append(pList2Append);
	};

	RETURNCODE getDepends(ddbItemList_t& retList);// 12sep06 stevev

/****************************************

public:

	void append(hCgroupItemList* addlist) 
	{
		for(groupItemList_t::iterator p = addlist->begin();p<addlist->end(); p++ )
		{
#ifdef _DEBUG
			if ( p == NULL || p == (hCgroupItemDescriptor*)0xcdcdcdcd )
			{cerrxx<< " - Bad group item ptr in groupitemList Append (0x"
				  << hex<<p<<dec<<")"<<endl;			}
			else
#endif /x* _DEBUG*x/
				push_back( *p );
		}// next
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)
	{
		COUTSPACE << "      " << size() << " Group Items   " << endl;
		for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{
			p->dumpSelf(indent, typeName);
		}
		return SUCCESS;
	};
	RETURNCODE getGroupItemPtrs(groupItemPtrList_t& giL );// adds 'em to the end of the list
	RETURNCODE getItemIDs(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemIndexes(UIntList_t& allindexedIDsReturned);
	RETURNCODE getItemByIdx(unsigned long idx, 
							hCgroupItemDescriptor& returnedElement, bool suppress = false)
	{   for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{	if (p->getIIdx() == idx)
			{
				returnedElement = *p;
				return SUCCESS;		
			}
		}
		if ( !suppress)
		{
			cerrxx<< "ERROR: Group-Item List failed to match index '" 
					 << hex << idx << dec <<"' in "<< size() <<" elements."<<endl;
		}
		return DB_ATTRIBUTE_NOT_FOUND;
	};
	bool       isItemInList(unsigned long idx)
	{   for(groupItemList_t::iterator p = begin(); p < end(); p++)
		{	if (p->getIIdx() == idx) return true;		}
		return false;
	};
	

	RETURNCODE getItemPtrs(ddbItemList_t & ptrList); //Vibhor 131004: Added
	*************************************/
};

typedef vector<hCGridGroupList>  gridGrpListList_t;
typedef vector<hCGridGroupList*> gridGrpListptrList_t;
typedef gridGrpListptrList_t::iterator iGrdGrpLpL_t;

typedef hCcondList<hCGridGroupList>	condGridGroupList_t;

class hCattrGridItems : public hCattrBase
{	// conditional list of member lists
	condGridGroupList_t condListOGridGrpLists;
protected:
	hCGridGroupList resolvedGGL;

public:
	
	hCattrGridItems(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,gridAttrMembers),condListOGridGrpLists(h),resolvedGGL(h) {};
	hCattrGridItems(const hCattrGridItems& sae)
		:hCattrBase(sae),condListOGridGrpLists(sae.devHndl()),resolvedGGL(sae.devHndl())
	{ 
		attr_Type             = sae.attr_Type;
		condListOGridGrpLists = sae.condListOGridGrpLists;
	};
	// abstract constructor (from abstract class)
	hCattrGridItems(DevInfcHandle_t h, const aCattrGridMemberList*  aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,gridAttrMembers),condListOGridGrpLists(h),resolvedGGL(h)
		{ condListOGridGrpLists.setEqualTo( (void*) &(aCa->condGridMemberListOlists));};
	hCattrBase* new_Copy(itemIdentity_t newID);

	virtual ~hCattrGridItems () { resolvedGGL.clear(); };
	RETURNCODE destroy(void) { return condListOGridGrpLists.destroy();};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
//	{	return condMemListOlists.dumpSelf(indent);};
{return FAILURE;};

	RETURNCODE getList(gridDesc_t& r2gD);	
	
	RETURNCODE getDepends(ddbItemList_t& retList);// 12sep06 stevev
	bool reCalc(void); // returns didChange

/*
TODO	RETURNCODE getAllindexes(UIntList_t& allindexedIDsReturned); // in ddbCollAndArr.cpp
	// get indexed value from all branches of all conditionals
TODO	RETURNCODE getAllItemsByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);

TODO	RETURNCODE getAllindexValues(UIntList_t& allindexedIDsReturned);

TODO	RETURNCODE getItemByIndex(unsigned long name, hCgroupItemDescriptor& returnedElement)
	{	
		hCgroupItemList mL(devHndl());

		RETURNCODE rc = SUCCESS;
		if ( pItem != NULL )
		{	rc = condListOGrpItmLists.resolveCond(&mL);
			if ( mL.size() > 0 )
			{	return (mL.getItemByIdx(name, returnedElement) );	}
			else
			{	returnedElement.clear();
			    return SUCCESS;	}  // note that a conditional without a default||else stmt will exit here
		}
		else
		{	return FAILURE;		}
	};
	
TODO	bool       isItemInList(unsigned long name)
	{	hCgroupItemList mL(devHndl());
		condListOGrpItmLists.resolveCond(&mL);
		if ( mL.size() > 0 )
		{	return (mL.isItemInList(name));	}
		else
		{	return false;           		}		
	};
*/

};

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

class hCgrid : public hCitemBase 
{
	hCattrGridItems*  pMemberAttr;	// required

	hCattrGrafixSize* pGridHgt;	//aCattrCondLong
	hCattrGrafixSize* pGridWid;	//aCattrCondLong
	hCddlLong         GridOrient;//aCddlLong
	hCattrHndlg*	  pGridHandling;//aCattrBitstring

public:
	hCgrid(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCgrid(hCgrid*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCgrid()  {/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);

	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual );

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(GRID_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(GRID_LABEL, l); };



	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Grid Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Grid ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};
	/*Vibhor 011104: Start of Code*/

	RETURNCODE getHeight(unsigned & height); // gives Grid's height

	RETURNCODE getWidth(unsigned & width);	// gives Grid's width

	RETURNCODE getOrient(gridOrient_t & co); 

	RETURNCODE getHandling(UINT64 & uHdlg);

	RETURNCODE getVectorList(gridDesc_t& vectorList); //returns a resolved list of vectors

	/*Vibhor 011104: End of Code*/
	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	int  notification(hCmsgList& msgList,bool isValidity);
};

#endif	// _DDB_GRID_H
