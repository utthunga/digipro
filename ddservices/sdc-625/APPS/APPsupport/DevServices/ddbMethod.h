/*************************************************************************************************
 *
 * $Workfile: ddbMethod.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The method / method support classes
 *		8/29/02	sjv	created from ddbItems.h
 *
 * #include "ddbMethod.h"
 */

#ifndef _DDB_METHOD_H
#define _DDB_METHOD_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbItemBase.h"

#include "ddbAttributes.h"

// parameter
//	type = methodVarType_t
//  flags .. isArray(), isConst(), isRef()
//  string paramName

// params  - list of parameters
class hCmethod   : public hCitemBase /* itemType 5 METHOD_ITYPE   */
{
	hCattrMethodClass* pClass;
	hCattrMethodDef*   pDef;
	hCattrMethodScope* pScope;
	hCattrMethodType*  pType;
	hCattrMethodParams*pParams;
public:
	hCmethod(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCmethod(hCmethod*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	virtual ~hCmethod();
	RETURNCODE destroy(void){ RETURNCODE rc = 0;
		if (pClass) { rc |=  pClass->destroy(); }
		if (pDef)   { rc |=    pDef->destroy(); }
		if (pScope) { rc |=  pScope->destroy(); }
		if (pType)  { rc |=   pType->destroy(); }
		if (pParams){ rc |= pParams->destroy(); }
		rc |= hCitemBase::destroy();	return rc;
	};
#ifdef XMTR
	bool isBackGround(void);
#endif
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 1; return 0;};
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);
	CValueVarient getAttrValue(unsigned attrType, int which = 0);

	RETURNCODE selfExecute(hCmethodCall& MethCall,CValueVarient& rV);

//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);
	RETURNCODE Label(wstring& retStr){return baseLabel(METHOD_LABEL, retStr);}; 
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(METHOD_LABEL, l); };

	char* getDef(int& l);

	RETURNCODE GetLastMethodsReturnCode(void); // this returns the error code returnrd by the method

	RETURNCODE getMethodType(hCmethodParam& mP);
	RETURNCODE getMethodParams(ParamList_t& pL);
	RETURNCODE getMethodScope(UINT64& rS){ if (pScope) {rS = ((hCbitString*)(pScope->procure()))->getBitStr(); return SUCCESS;} 
											else { rS = 0;  return FAILURE; }   
     };
};


#endif //_DDB_METHOD_H

/*************************************************************************************************
 *
 *   $History: ddbMethod.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
