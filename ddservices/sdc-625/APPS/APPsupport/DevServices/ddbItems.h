/*************************************************************************************************
 *
 * $Workfile: ddbItems.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		(ALL)   The item classes
 *		8/07/02	sjv	created from DDitem.h
 *
 * #include "ddbItems.h"
 */

#ifndef _DDB_ITEMS_H
#define _DDB_ITEMS_H

#include <algorithm>

#include "ddbGeneral.h"
#include "foundation.h"

#include "ddbConditional.h"
#include "ddbItemBase.h"
#include "ddbRfshUnitWaoRel.h"
#include "ddbEditDisplay.h"
#include "ddbCollAndArr.h"
#include "ddbCommand.h"
#include "ddbMethod.h"
#include "ddbMenu.h"
#include "ddbVar.h"

#include "ddbArray.h"
#include "ddbAxis.h"
#include "ddbChart.h"
#include "ddbFile.h"
#include "ddbGraph.h"
#include "ddbList.h"
#include "ddbSource.h"
#include "ddbWaveform.h"
#include "ddbGrid.h"
#include "ddbImage.h"
#include "ddbBlob.h"

#ifdef __cplusplus
    extern "C" {
#endif

 int item_id_to_name(ITEM_ID item_id, char **item_name);

#ifdef __cplusplus
    }
#endif


#if 0  /* enable the following as they are implemented */
class Cblock   : public CitemBase /* itemType 12 BLOCK_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Cblock");return NULL;};
public:
	Cblock(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	Cblock(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~Cblock();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "BLOCK" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(VAR_LABEL, retStr); };
};
class Cprog    : public CitemBase /* itemType 13 PROGRAM_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Cprog");return NULL;};
public:
	Cprog(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	Cprog(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~Cprog();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "PROGRAM" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(NO_LABEL, retStr); };
};
class Crecord  : public CitemBase /* itemType 14 RECORD_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Crecord");return NULL;};
public:
	Crecord(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	Crecord(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~Crecord();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "RECORD" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(RECORD_LABEL, retStr); };
};
class CvarList : public CitemBase /* itemType 16 VAR_LIST_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("CvarList");return NULL;};
public:
	CvarList(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	CvarList(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~CvarList();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "VAR_LIST" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(VAR_LIST_LABEL, retStr); };
};
class CitemRespCode: public CitemBase, CrespCodeBase /* itemType 17 RESP_CODES_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("CitemRespCode");return NULL;};
public:
	CitemRespCode(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	CitemRespCode(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~CitemRespCode();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "RESP_CODES" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(NO_LABEL, retStr); };
};
class Cdomain  : public CitemBase /* itemType 18 ----DOMAIN_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Cdomain");return NULL;};
public:
	Cdomain(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	Cdomain(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~Cdomain();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "DOMAIN" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(NO_LABEL, retStr); };
};
class Cmember  : public CitemBase /* itemType 19 ----MEMBER_ITYPE */
{
	CattrBase* newAttr(struct itemAttrTbl&) {NEWATTRNOTIMPLEMENTED("Cmember");return NULL;};
public:
	Cmember(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
	Cmember(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~Cmember();
	RETURNCODE load(void);
//    RETURNCODE insert(void** pChunk);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) { CitemBase::dumpSelf(); cout << "MEMBER" << endl; return SUCCESS;};
RETURNCODE Label(string& retStr) { return baseLabel(NO_LABEL, retStr); };
};
/*
RESERVED_ITYPE2			11
MAX_ITYPE				20	/ * must be last in list * /
*/
#endif /* 0   unimplemented types */

#endif //_DDB_ITEMS_H

/*************************************************************************************************
 *
 *   $History: ddbItems.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
