/*************************************************************************************************
 *
 * $Workfile: ddbFileSupportInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003,2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is file that defines the virtual base class hCFileSupport.
 *    All hosts that supports the eDDL File item must derive from this class.
 *    The device services interface to this file for all file services.
 *	
 * #include "ddbFileSupportInfc.h"
 */


#ifndef _DDBFILESUPPORTINFC_H
#define _DDBFILESUPPORTINFC_H

#include "pvfc.h"
#include "ddbGeneral.h"

#define FILE_RV_OK_NOT_FINISHED	( FILE_RETURN_VALUE_BASE +  1) /* function success but needs more*/
#define FILE_RV_NOT_FOUND		( FILE_RETURN_VALUE_BASE +  2)
#define FILE_RV_NOT_SUPPORTED	( FILE_RETURN_VALUE_BASE +  3) /* operation/format not available*/
#define FILE_RV_FILE_CREATED	( FILE_RETURN_VALUE_BASE +  4) /* file is open but empty */
#define FILE_RV_FAILED			( FILE_RETURN_VALUE_BASE +  5) /* operation failed */
#define FILE_RV_NOT_AVAILABLE	( FILE_RETURN_VALUE_BASE +  6) /* proly not opened */
#define FILE_RV_SEEK_FAILED		( FILE_RETURN_VALUE_BASE +  7) 
#define FILE_RV_EOF				( FILE_RETURN_VALUE_BASE +  8) 
#define FILE_RV_BUF_FAIL		( FILE_RETURN_VALUE_BASE +  9) 

#define FILE_EXTENSION  _T("hfd") /* HART File Description (changed from 'hcl')*/

// the interface (supplied by the application) to the device in order to give file i/o service 
//     to the device
// a virtual base class - MUST be subclassed (subclass must track file handles and stuff)
class hCFileSupport
{
public:	
	// not currently used.... to be used later for DD directory reading
	//function to be implemented in derived class that handles directory handling 
	virtual RETURNCODE FindFile(string& pattern, StrVector_t& returnedFiles) RPVFC( "hCFileSupport_a",0 );
	//function to be implemented in derived class that handles initializations 
	virtual RETURNCODE OpenDDFile(Indentity_t* deviceID) RPVFC( "hCFileSupport_b",0 );

	/** Note - only one file may opened at a time (DD or File) through this interface **/

	//function to be implemented in derived class that handles initializations 
	virtual 
		RETURNCODE OpenFileFile(Indentity_t* deviceID, itemID_t fileID, bool clearOnOpen=false)RPVFC( "hCFileSupport_c",0 );

	//function to be implemented in derived class that handles cleanup 
	virtual 
		RETURNCODE CloseFile(void) RPVFC( "hCFileSupport_e",0 );/* whichever type is opened */

	//function to be implemented in derived class that actually reads the file info
	virtual 
		RETURNCODE ReadFile(uchar* buffer, int& buffLen, ulong location = 0) RPVFC( "hCFileSupport_f",0 );

	//function to be implemented in derived class that actually writes the file info
	virtual 
		RETURNCODE WriteFile(uchar* buffer, int buffLen) RPVFC( "hCFileSupport_g",0 );

};


#endif//_DDBFILESUPPORTINFC_H
