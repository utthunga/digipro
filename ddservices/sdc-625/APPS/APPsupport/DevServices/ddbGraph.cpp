/*************************************************************************************************
 *
 * $Workfile: ddbGraph.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbGraph.h"
#include "ddbAxis.h"
#include "logging.h"
#include "ddbCommand.h" // for hCmsgList def
#include "ddbWaveform.h"
#include "ddbMenuLayout.h"// stevev 01nov06 added

/* the global mask converter...you have to cast the return value*/
extern int  mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];


hCgraph::hCgraph(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pMemberAttr(NULL), pGraphHgt(NULL), pGraphWid(NULL),pXaxis(NULL)
	,pCycleTime(NULL)
{// label, help validity are set in the base class
	// required::>
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(graphAttrMembers); 
	if (aB == NULL)
	{	LOGIT(CERR_LOG,"ERROR: hCgraph::hCgraph<no menu item attribute supplied.>\n");
		LOGIT(CERR_LOG,"       trying to find: %d but only have:",graphAttrMembers);

		if (paItemBase->attrLst.size() <= 0)
		{
			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG, "%d, ",(*iT)->attr_mask );}
		}
		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		pMemberAttr = (hCattrGroupItems*)newHCattr(aB);
		if (pMemberAttr != NULL)
		{
			pMemberAttr->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMemberAttr);// keep it on the list too
		}
		else // is NULL
		{
			grpItmAttrType_t en = (grpItmAttrType_t)mask2value(aB->attr_mask); 
			LOGIT(CERR_LOG,"++ No New attribute ++ (%s)\n", grpItmAttrStrings[en]);
		}
	}

	// NOT required			
		
	aB = paItemBase->getaAttr(graphAttrHeight); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pGraphHgt = (hCattrGrafixSize*)newHCattr(aB);
		if (pGraphHgt != NULL)
		{
			pGraphHgt->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pGraphHgt);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Height attribute for Graph ++ (memory error?)\n");
		}
	}

	aB = paItemBase->getaAttr(graphAttrWidth); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pGraphWid = (hCattrGrafixSize*)newHCattr(aB);
		if (pGraphWid != NULL)
		{
			pGraphWid->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pGraphWid);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Width attribute for Graph ++ (memory error?)\n");
		}
	}


	aB = paItemBase->getaAttr(graphAttrXAxis); // uses aCattrCondReference
	if (aB != NULL)
	{
		pXaxis = (hCattrAxisRef*)newHCattr(aB);
		if (pXaxis != NULL)
		{
			pXaxis->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pXaxis);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Axis attribute for Graph ++ (memory error?)\n");
		}
	}
	
	aB = paItemBase->getaAttr(graphAttrCycleTime); // uses aCattrCondExpr
	if (aB != NULL)
	{
		pCycleTime = (hCattrExpr*)newHCattr(aB);
		if (pCycleTime != NULL)
		{
			pCycleTime->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pCycleTime);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New CycleTime attribute for Graph ++ (memory error?)\n");
		}
	}	
}

/* from-typedef constructor */
hCgraph::hCgraph(hCgraph*    pSrc,  itemIdentity_t newID)
	: hCitemBase(pSrc, newID), pMemberAttr(NULL), pGraphHgt(NULL), pGraphWid(NULL),pXaxis(NULL)
	  ,pCycleTime(NULL)
{
	cerr<<"**** Implement graph typedef constructor.***"<<endl;
}


// check the existance
bool hCgraph::isInGroup(UINT32 indexValue)
{
	bool retVal = false;

	if (pMemberAttr != NULL)
	{
		retVal = pMemberAttr->isItemInList(indexValue);
		/*TODO verify isItemInList will go into a ref2var to see if it exists?? */
	}
	return retVal;
}

// note that the device map is required to process ItemRef to ITEMID
RETURNCODE 
hCgraph::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( pMemberAttr != NULL  && pGid != NULL )
	{
		rc = pMemberAttr->getItemByIndex(indexValue, *pGid);
		if ( rc != SUCCESS)
		{
			delete pGid;
			*ppGID = NULL;
			if ( ! suppress)
			{
				LOGIT(CERR_LOG|CLOG_LOG, "ERROR: Graph getByIndex failed to getItemByIndex.'%d'  Item 0x%04x\n", indexValue, getID());
			}
			return rc;
		}
	}
	if (rc != SUCCESS)
	{
		RAZE(pGid);		
		*ppGID = NULL;
	}
	return rc;
}


/* Label, Help  and Validity should be handled by the item base class */
hCattrBase*   hCgraph::newHCattr(aCattrBase* pACattr)  // builder 
{
	hCattrGroupItems* pMemAttr = NULL;


	if (pACattr->attr_mask == maskFromInt(graphAttrMembers)) 
	{
		pMemAttr  = new hCattrGroupItems(devHndl(), (aCattrMemberList*)pACattr, this );
		return pMemAttr;
		
	}
	else
	if (pACattr->attr_mask == maskFromInt(graphAttrHeight))	// same value as chartAttrHeight
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(graphAttrWidth))
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(graphAttrXAxis))
	{
		return 
			new hCattrAxisRef(devHndl(), graphAttrXAxis, (aCattrCondReference*)pACattr, this );
	}
	if (pACattr->attr_mask == maskFromInt(graphAttrCycleTime)) 
	{
		return 
			new hCattrExpr(devHndl(), graphAttrCycleTime, (aCattrCondExpr*)pACattr, this );		
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(graphAttrLabel) | 
			 maskFromInt(graphAttrHelp)  | 
			 maskFromInt(graphAttrValidity) | 
			 maskFromInt(graphAttrDebugInfo)   ))
	{//known attribute but shouldn't be here		
		LOGIT(CERR_LOG, "ERROR: graph attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIT(CERR_LOG, "ERROR: graph attribute type is unknown.\n");
		return NULL; /* an error */  
	}
};

/* stevev added 20apr07 */
RETURNCODE hCgraph::getCycleTime(CValueVarient & cv)
{
	
	RETURNCODE rc = SUCCESS;

	if(NULL == pCycleTime)
	{
		cv = (int)0;
		return APP_ATTR_NOT_SUPPLIED; //happens all the time
	}
	else
	{
		cv = pCycleTime->getExprVal();
	}

	return rc;

}/*End getCycleTime -  20apr07 */

/*Vibhor 071004: Start of Code */

RETURNCODE hCgraph::getWaveformList(hCgroupItemList& memberList)
{
	RETURNCODE rc = SUCCESS;

	groupItemPtrList_t tmpList;

	rc = pMemberAttr->getList(tmpList);

	memberList.clear();
	if(SUCCESS == rc && tmpList.size() > 0)
	{
		groupItemPtrList_t :: iterator it;
		
		for(it = tmpList.begin(); it != tmpList.end();it++)
		{
			memberList.push_back(**it);	
		}

	}

	tmpList.clear();
	return rc;
}/*End hCgraph::getWaveformList()*/

RETURNCODE hCgraph ::getHeight(unsigned & height)
{
	RETURNCODE rc = SUCCESS;
	if(NULL != pGraphHgt)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pGraphHgt->procure();
		if(ptr2TempLong)
		{
			height = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
		{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;
}

RETURNCODE hCgraph ::getWidth(unsigned & width)
{
	RETURNCODE rc = SUCCESS;
	if(NULL != pGraphWid)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pGraphWid->procure();
		if(ptr2TempLong)
		{
			width = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
		{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;

}

hCaxis * hCgraph::getXAxis()
{
	RETURNCODE rc = SUCCESS;
	if (NULL == pXaxis)
	{
		return NULL;
	}
	
	hCreference *ptrTempRef = (hCreference *)pXaxis->procure();

	if(NULL == ptrTempRef)
		return NULL;

	hCitemBase *ptr2BaseItm = NULL;
	rc = ptrTempRef->resolveID(ptr2BaseItm);
	if(SUCCESS != rc)
		return NULL;
	else
		return (hCaxis *)ptr2BaseItm;
}
/*Vibhor 071004: End of Code */



CValueVarient hCgraph::getAttrValue(unsigned attrType, int which)
{	
	CValueVarient rV;

	graphAttrType_t gat = (graphAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( gat >= graphAttr_Unknown )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Graph's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request


	switch (gat)
	{
	case graphAttrLabel:
	case graphAttrHelp:
	case graphAttrValidity:
		rV = hCitemBase::getAttrValue(gat);	
		break;
	case graphAttrHeight:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned v = 0;
			if (getHeight(v) == SUCCESS)
			{
				rV.vValue.iIntConst = v;
			}
			else
			{
				rV.vValue.iIntConst = MEDIUM_DISPSIZE;
			}
		}
		break;
	case graphAttrWidth:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned v = 0;
			if (getWidth(v) == SUCCESS)
			{
				rV.vValue.iIntConst = v;
			}
			else
			{
				rV.vValue.iIntConst = MEDIUM_DISPSIZE;
			}
		}
		break;
	case graphAttrXAxis:
		{	
			rV.vType = CValueVarient::isSymID;
			rV.vSize = 4;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			hCaxis *pA = NULL;
			if ( (pA = getXAxis()) != NULL )
			{
				rV.vValue.varSymbolID = pA->getID();
			}
			else
			{
				rV.vValue.varSymbolID = 0;
				rV.vIsValid    = false;
			}
		}
		break;
	/* added stevev 20apr07 */	
	case graphAttrCycleTime:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			if (getCycleTime(rV) != SUCCESS )
			{
				rV.vIsValid    = false;
			}
		}
		break;
	/*end add 20apr07*/
	case graphAttrMembers:
		{	
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: graph members must be accessed differently.\n");
		}
		break;
	case graphAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG,ERR_NO_DEBUG_INFO);
		}
		break;
	default:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

// gets all possible references for a given member number  
// get the member value from all valid lists (used in the unitialized mode - no reads allowed)
//				ref& expr pointers must be destroyed by caller...What is pointed to must NOT
RETURNCODE hCgraph::getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllItemsByIndex(indexValue, returnedItemRefs);
		if (rc != SUCCESS || returnedItemRefs.size() == 0 )
		{/* NOTE: this often occurs if PRIMARY is defined as 1 in imports but 0 in the DD */
		 /*       comes from the difference in macros.ddl and macros.h                    */
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s member list failed to find index %d.\n",
																	getName().c_str(), indexValue);
#ifdef _DEBUG
			LOGIT(CERR_LOG,"       possible mismatch of PRIMARY definition (DD to imports).\n");
#endif
		}
	}
	else
	{
		LOGIT(CERR_LOG,
				"ERROR: No Member attribute for a member based item (%s).\n",getName().c_str());
	}
	return rc;
}

/* stevev 05sep06 - overload to get graph specific notifications */

// called when an item in iDependOn has changed
int hCgraph::notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	
	hCitemBase::notification(msgList,isValidity);// do validity and label

	// re-evaluate all the others ( item list is only one left )
	if ( (! isValidity) && (pMemberAttr != NULL) )
	{// my list could have changed	

		if (pMemberAttr != NULL && pMemberAttr->reCalc())// list
		{// my member list changed
			msgList.insertUnique(getID(), mt_Str, pMemberAttr->getGenericType());
		}// else - no change;

		/* drill down into the contained waveforms */
		hCgroupItemList localList(devHndl());
		groupItemList_t::iterator gidIT;
		hCitemBase*      pWvFrm = NULL;
//
		if ( getWaveformList(localList) == SUCCESS && localList.size() > 0 )
		{
			for ( gidIT = localList.begin(); gidIT != localList.end(); ++gidIT )
			{
				if ( gidIT->getItemPtr(pWvFrm) == SUCCESS && pWvFrm != NULL )
				{
#ifdef _DEBUG
					if ( pWvFrm->getIType() != iT_Waveform )
					{
						LOGIT(CERR_LOG, "Trying to Notification a Non-Waveform in Graph.\n");
					}
					else
#endif
					((hCwaveform*)pWvFrm)->notification(msgList,isValidity,this);
				}
			}
		}
	 // cycletime
		if (pCycleTime != NULL && pCycleTime->reCalc())// expression
		{// my handling changed
			msgList.insertUnique(getID(), mt_Str, pCycleTime->getGenericType());// tell the world
		}// else - no change;

	}// else - no change;
	

	/** TODO: Axis change... ***/

	return (msgList.size() - init);// number added
}
/** end stevev 05sep06  **/


// stevev 12sep06
void  hCgraph::fillCondDepends(ddbItemList_t&  iDependOn)// stevev 12sep06
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes
	if ( pMemberAttr != NULL )//VAR_HANDLING_ID  
	{//hCattrGroupItems*
		pMemberAttr->getDepends(iDependOn);
	}
	if ( pXaxis != NULL )// Y_AXIS 
	{
		pXaxis->getDepends(iDependOn);
	}

	/* height and width are not conditional and cannot change while running */
}



/* stevev 27sep06 - more dependency extentions */
// get all possible items in this source
void  hCgraph::fillContainList(ddbItemList_t&  iContain)
{// fill with items-I-contain 
	if ( pMemberAttr == NULL ) return;		

	//pMemberAttr-> getAllindexes(UIntList_t& allindexedIDsReturned);
	//	pMemberAttr - get all possible lists out of conditional to a groupItemListPtrList_t
	//				- for each hCgroupItemList - call getItemIDs(returnList)
	//		getItemIDs	- for each hCgroupItemDescriptor in thisList
	//					-    get the reference 
	//					-	 reference:resolveAllIDs(returnList)
	/////////////////////////////////////////////////////////////////////////////////////////////

	vector<unsigned int>  localitemIDList;
	vector<unsigned int>::iterator iItemID;
	hCitemBase* pItem   = NULL ;
	hCdependency* pDep  = NULL;

	if (getAllindexes(localitemIDList) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{// for each waveform
			if ( devPtr()->getItemBySymNumber(*iItemID, &pItem) == SUCCESS && pItem != NULL)
			{
#ifdef _DEBUG
				if (pItem->getIType() != iT_Waveform)
				{
					LOGIT(CERR_LOG,"Graph %s (0x%04x) has a member that is a %s\n",
						pItem->getName().c_str(), pItem->getID(),itemStrings[pItem->getIType()]);
					continue;
				}
#endif
				pDep = pItem->getDepPtr(ib_Contain);
				if (pDep->iDependOn.size() > 0)
				{
					hCdependency::insertUnique(iContain, pDep->iDependOn);//appends list uniquely
					// others may need it, don't::>  pDep->iDependOn.clear();
				}
				// else nothing to add
			}
		}
	}// else move on
}
/** end stevev 27sep06 **/
/* stevev 01nov06 - layout */
RETURNCODE  hCgraph::setSize( hv_point_t& returnedSize, ulong qual )
{ 
	unsigned X= DFLTWID_INDX,Y = DFLTHGT_INDX;
	
	if ( getHeight(Y) != SUCCESS)
	{	Y = DFLTHGT_INDX;
	}
	if ( getWidth(X)  != SUCCESS)
	{	X = DFLTWID_INDX;
	}

	// translate size codes into number of 1x1 frames
	returnedSize.yval = vert_Sizes[Y];
	returnedSize.xval = horizSizes[X];

	return SUCCESS;
}
/** end stevev 01nov06 **/