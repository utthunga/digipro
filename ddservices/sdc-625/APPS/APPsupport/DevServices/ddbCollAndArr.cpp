/*************************************************************************************************
 *
 * $Workfile: ddbCollAndArr.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the hCcollection and hCitemArray classes
 *		01/28/03	sjv	created 
 */

//#include "ddbAttributes.h"
#include "ddbItemBase.h"
#include "ddbCollAndArr.h"
#include "ddbItems.h"
#include "ddbDevice.h"
#include "dllapi.h"

#ifdef _DEBUG
//	#define GETALL_DEBUG
#endif

//extern char collAttrStrings[COLLATRSTRCOUNT] [COLLATRMAXLEN];
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];

/*=============================== support code ============================*/
void hCvarGroupTrail::clear(void)
{
	if (pSourceTrail)    
	{	pSourceTrail->clear();    delete pSourceTrail;    pSourceTrail = NULL;}
	if (pLookupTrail)    
	{	pLookupTrail->clear();    delete pLookupTrail;    pLookupTrail = NULL;}
	if (pExprTrail)    
	{	pExprTrail->clear();      delete pExprTrail;      pExprTrail = NULL;}
	//if ( ! lablStr.empty() ) lablStr.erase(); 
	// this crashes on an empty string.....lablStr.clear();
	if ( ! lablStr.empty() ) lablStr.clear();
	if ( ! helpStr.empty() ) helpStr.erase();
	itmID=0x0000;   containerNumber=0; 
	//containerDesc.erase(); 
	containerDesc.clear(); 
	containerHelp.erase();
	itmPtr=NULL;
}

bool hCvarGroupTrail::isEmpty(void)
{
	return(itmID==0 && itmPtr==NULL);
}

hCvarGroupTrail& hCvarGroupTrail::operator=(const hCvarGroupTrail& src)
{
	itmID =src.itmID;
	itmPtr=src.itmPtr;
	//if (! src.lablStr.empty())	lablStr = src.lablStr;
	lablStr = src.lablStr;
	if (! src.helpStr.empty())	helpStr = src.helpStr;
	//if (! src.containerDesc.empty())	containerDesc = src.containerDesc;
	containerDesc = src.containerDesc;
	if (! src.containerHelp.empty())	containerHelp = src.containerHelp;
	containerNumber=src.containerNumber;

	if (pSourceTrail)    
	{	pSourceTrail->clear();    delete pSourceTrail;    pSourceTrail = NULL;}
	if (src.pSourceTrail)
	{
		pSourceTrail = new hCvarGroupTrail(*src.pSourceTrail);      // copy constructor
	}// else leave it NULL
	
	if (pLookupTrail)    
	{	pLookupTrail->clear();    delete pLookupTrail;    pLookupTrail = NULL;}
	if (src.pLookupTrail)
	{
		pLookupTrail = new hCvarGroupTrail(*src.pLookupTrail);      // copy constructor
	}// else leave it NULL
	
	if (pExprTrail)    
	{	pExprTrail->clear();    delete pExprTrail;    pExprTrail = NULL;}
	if (src.pExprTrail)
	{
		pExprTrail = new hCexpressionTrail(*src.pExprTrail);      // copy constructor
	}// else leave it NULL

	return *this;
}

// the trail for an element or member
hCvarGroupTrail& hCvarGroupTrail::operator=( hCgroupItemDescriptor& src)
{
	RETURNCODE rc = SUCCESS;
	hCvarGroupTrail localGT;
#ifdef _DEBUG
	if (containerNumber != src.getIIdx())
	{
		LOGIT(CLOG_LOG,"VarGroup's index does not match GID. this:0x%04x  GID:0x%04x\n",
			containerNumber, src.getIIdx() );
	}
#endif;
	hCreference localRef(src.getRef());
 
	if ( localRef.resolveID( itmPtr ) == SUCCESS && itmPtr != NULL )
	{
		itmID = itmPtr->getID();
		if (itmPtr->Label(lablStr) != SUCCESS )
		{
			lablStr.clear();
		}
		if (itmPtr->Help(helpStr) != SUCCESS )
		{
			helpStr.erase();
		}
	}

	//containerDesc = (wstring)src.getDesc().procureVal();
	containerDesc = src.getDesc();
	containerHelp = (wstring)src.getHelp().procureVal();

	containerNumber = src.getIIdx();
	
	//hCvarGroupTrail* pSourceTrail;	
	// leave this since it may already be set correctly
	localRef.destroy();
	return *this;
}

void hCvarGroupTrail::clogSelf(int indent)
{
	CLOGSPACE<< "       item:0x"<<hex<<itmID<< "   from INDEX:0x"<<containerNumber<<dec<<endl;
	CLOGSPACE<< "      label:";
	wcout<<lablStr<<L":    help:"<<helpStr<<L":"<<endl;
	CLOGSPACE<< "       desc:";
	wcout<<containerDesc<<L":    help:"<<containerHelp<<L":"<<endl;
	if (pSourceTrail != NULL) 
	{CLOGSPACE<<"     Source:"<<endl;pSourceTrail->clogSelf(indent+11);}
	if (pExprTrail != NULL) 
	{CLOGSPACE<<" Expression:"<<endl;pExprTrail->clogSelf(indent+11);}
	if (pLookupTrail != NULL) 
	{CLOGSPACE<<"     Lookup:"<<endl;pLookupTrail->clogSelf(indent+11);}
}

void hCexpressionTrail::clogSelf(int indent)
{
	if (isVar)
	{
	CLOGSPACE<< "      Value:0x"<<hex<< srcIdxVal <<"  from Item 0x"<<srcID<<dec<<endl;
	}
	else
	if ( ! isEmpty() )
	{
	CLOGSPACE<< "Const Value:0x"<<hex<< srcIdxVal <<dec<<endl;
	}// else don't print anything
	else
	{
		clog<<endl;
	}
}

/* ------- hCGroupItemInfo --------*/// like hCvarGroupTrail but for lists
hCGroupItemInfo::hCGroupItemInfo()
	:theVarPtr(NULL),theVarHist_p(NULL),srcPtr(NULL),srcHist_p(NULL)
{
	clear();
};
	
hCGroupItemInfo::hCGroupItemInfo(const hCGroupItemInfo& s)
	:theVarPtr(NULL),theVarHist_p(NULL),srcPtr(NULL),srcHist_p(NULL)
{	operator=(s);		
};
	
hCGroupItemInfo::~hCGroupItemInfo()
{	clear();
};

void hCGroupItemInfo::clear(void) 
{	theVarID=srcID=0x0000; theVarPtr = srcPtr = NULL; theVarExpr.clear();
	RAZE(theVarHist_p); RAZE(srcHist_p); srcExpr.clear();
};
	
hCGroupItemInfo& hCGroupItemInfo::operator=(const hCGroupItemInfo& s)
{	theVarID   = s.theVarID;
	theVarPtr  = s.theVarPtr;
    theVarExpr = s.theVarExpr;

	if (s.theVarHist_p != NULL){theVarHist_p = new hCGroupItemInfo(*s.theVarHist_p); }

	srcID = s.srcID;
	srcPtr= s.srcPtr;

	if (s.srcHist_p != NULL){srcHist_p = new hCGroupItemInfo(*s.srcHist_p); }

	srcExpr = s.srcExpr;

	return *this;
};
		
void hCGroupItemInfo::clogSelf(int indent) 
{	
	CLOGSPACE<<"VarID:0x"<<hex<<theVarID<<dec<<"    ";     
	theVarExpr.clogSelf();
	
	if (theVarHist_p != NULL)  theVarHist_p->clogSelf(indent +8);

	CLOGSPACE<<"SrcID:0x"<<hex<<srcID<<dec<<"    ";     
	srcExpr.clogSelf();
	
	if (srcHist_p != NULL)  srcHist_p->clogSelf(indent +8);
};

//   returns count of variable in expressions
int  hCGroupItemInfo::fillExpressions(ExprTrailList_t& etList)
{
	int cnt = 0;

	// one day it may be usefull to look at the constants too---not today
	if (srcExpr.isVar)
	{
		etList.push_back(srcExpr);
		cnt++;
	}
	if (theVarExpr.isVar)
	{
		etList.push_back(theVarExpr);
		cnt++;
	}
	if (srcHist_p != NULL)
	{
		cnt += srcHist_p->fillExpressions(etList);
	}
	if (theVarHist_p != NULL)
	{
		cnt += theVarHist_p->fillExpressions(etList);
	}
	return cnt;
}

/*=========================== end support code ============================*/

/*==============================================================================================*/


#if 0 // use group item stuff

void  hCmemberList::setEqual(void* pAclass) // a hCpayload required method	 
{// assume: pAclass pts to a  class aCCollectionMemberList : public aPayldType, public AcollectionMemList_t
 //   and   typedef vector<aCCollectionMember>         AcollectionMemList_t;
	
	if (pAclass == NULL) return;//nothing to do

	//aCCollectionMemberList* pcL = (aCCollectionMemberList*)pAclass;
	AcondMemberElemList_t* pcL = &(((aCattrMemberList*)pAclass)->condMemberElemListOlists);
	hCmember wrkMember(devHndl());
	
	//for(AcollectionMemList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	for(AcondMemberElemList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	{ // iT is ptr 2 aCmemberElementList..an aCcondList..which holds a list of conditionals
		if (iT != NULL)
		{
			wrkMember.setEqual(iT);
			push_back(wrkMember);
			wrkMember.clear();
		}
		else
		{
			LOGIT(CERR_LOG,"ERROR: received a NULL wrkMember ptr.\n");
		}
	} 
}

void  hCmember::setEqual(void* pAitem)
{	
	if (pAitem==NULL) return;
	aCCollectionMember* paCm = (aCCollectionMember*)pAitem;

	name  = paCm->name;		// a long
	ref	  = paCm->ref	;	// aCreference

	descS = paCm->descS	;	// aCddlString
	helpS = paCm->helpS	;	// aCddlString
}

hCmember::~hCmember()
{ 
	ref.destroy(); 
	//descS.destroy(); 
	//helpS.destroy();
}

/*****************************************************************************
 *
 *   Item Arrays
 * 
 *****************************************************************************
 */

/* off for now
// note that the device map is required to process ItemRef to ITEMID
RETURNCODE hCitemArray::getByIndex(UINT32 indexValue, hCreference& returnedItemRef)
{
	hCgroupItemDescriptor* pGID = NULL;
	RETURNCODE rc = getByIndex(indexValue, &pGID);
	if ( rc == SUCCESS )
	{
		returnedItemRef = pGID->getRef();
	}
	else
	{
		returnedItemRef.clear();
	}
	return rc;

}
*/




void  hCelementList::setEqual(void* pAclass) // a hCpayload required method	 
{	
	if (pAclass == NULL) return;//nothing to do

	aCItmArrElementList* pcL = (aCItmArrElementList*)pAclass;
	hCelement wrkElement(devHndl());
	
int cnt = pcL->size();
	for(AitmArrElemList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	{ // iT is ptr 2 aCCollectionMember
		if (iT != NULL)
		{
			wrkElement.setEqual(iT);
//			elementList.push_back(wrkElement);
			push_back(wrkElement);
			wrkElement.clear();
		}
		else
		{
			LOGIT(CERR_LOG,"ERROR: received a NULL wrkElement ptr.\n");
		}
	} 
}

#endif //???????????????????




/*************************************************************************************************
 *  The common class for group items
 ************************************************************************************************/

extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];

/************************************************************************************************/
// aquires a current (resolved) list of items

RETURNCODE hCgrpItmBasedClass::getList(groupItemPtrList_t& r2grpIL)
{
	RETURNCODE rc = SUCCESS;

	if (pMemberAttr == NULL)
	{
		return APP_ATTR_NOT_SUPPLIED;
	}
	else
	{
		return (pMemberAttr->getList(r2grpIL));
	}
}
	

void hCgrpItmBasedClass::MakeStale(void)
{
	RETURNCODE rc = SUCCESS;
	if (pMemberAttr == NULL)
		return;// nothing to do

	pMemberAttr->MakeStale();
}


// gets all possible references for a given index into this list of group-items 
// get the indexed value from all valid lists (used in the unitialized mode - no reads allowed)
//				ref& expr pointers must be destroyed by caller...What is pointed to must NOT
RETURNCODE hCgrpItmBasedClass::getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllItemsByIndex(indexValue, returnedItemRefs);
		if (rc != SUCCESS || returnedItemRefs.size() == 0 )
		{/* NOTE: this often occurs if PRIMARY is defined as 1 in imports but 0 in the DD */
		 /*       comes from the difference in macros.ddl and macros.h    */
			if (getIType() == iT_Collection)
			{
				string locName;
				devPtr()->getElemNameByIndex(indexValue, locName);             
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s member list failed to find member '0x%04x' (%s).\n",
												getName().c_str(), indexValue,locName.c_str());
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s member list failed to find index '0x%04x' (%d).\n",
													getName().c_str(), indexValue,indexValue);
			}
		}
	}
	else
	{
		LOGIT(CERR_LOG,
				"ERROR: No Member attribute for a member based item (%s).\n",getName().c_str());
	}
	return rc;
}

/* aquires a unique list of index values for all possible group-item access */
RETURNCODE hCgrpItmBasedClass::getAllindexValues(UIntList_t& allValidindexes)
{
	RETURNCODE rc = FAILURE;
	
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllindexValues(allValidindexes);
	}
	return rc;
}


/* gets the unique list of all item IDs possibly indexed by this class */
RETURNCODE hCgrpItmBasedClass::getAllindexes(UIntList_t& allindexedIDsReturned)
{
	RETURNCODE rc = FAILURE;

	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllindexes(allindexedIDsReturned);
	}
	return rc;
}


RETURNCODE hCgrpItmBasedClass::getDescriptionList( triadList_t& listOdesc )
{
	groupItemPtrList_t     localList;
	groupItemPtrIterator_t llIT;
	hCgroupItemDescriptor  *pGID = NULL;
	EnumTriad_t            wrkTri;

	if ( getList(localList) == SUCCESS && localList.size() > 0 )
	{
		for (llIT = localList.begin(); llIT != localList.end(); ++ llIT )
		{// llIT isa ptr2ptr2gid
			pGID   = *((hCgroupItemDescriptor  **)&(*llIT));	// PAW &(*) added 03/03/09
			wrkTri = *pGID;// moves value,desc,help
			listOdesc.push_back(wrkTri);
		}
		if ( localList.size() > 0 )
			return SUCCESS;
		//else fall thru to exit failure
	}
	return FAILURE;
}


RETURNCODE 
hCgrpItmBasedClass::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress)
{
	
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getItemByName(indexName, *pGid);
		if ( rc != SUCCESS)
		{
			delete pGid;
			pGid = NULL;	//Vibhor: 200904
			*ppGID = NULL;
			if ( ! suppress)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG,
				 "ERROR: Group-Item getByName failed to getItemByName.'%s'  Item 0x%04x\n"
					,indexName.c_str(), getID());
			}
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}


// note that the device map is required to process ItemRef to ITEMID
RETURNCODE 
hCgrpItmBasedClass::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE,tRC=SUCCESS;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getItemByIndex(indexValue, *pGid, true);// temp suppress);
		if ( rc != SUCCESS)
		{
			delete pGid;
			pGid = NULL;	//Vibhor: 200904
			*ppGID = NULL;
//always			if ( ! suppress)
			{// all this is for a decent error message
				string type; type = itemType.getTypeStr();
				string name; tRC   = devPtr()->getElemNameByIndex(indexValue,name);
				if ( tRC == SUCCESS && ! name.empty() )
				{
					string h("(");
					h += name;   h += ")";
					name = h;
				}
				else
				{
					name = "";
				}

				LOGIF(LOGP_NOT_TOK)
					(CERR_LOG|CLOG_LOG,"ERROR: %s (0x%04x) %s's getByIndex() failed to "
					"getItemByIndex for index value '%d'%s\n",type.c_str(),getID(),
					itemName.c_str(),indexValue,name.c_str());
				//clog << "ERROR: Collection getByIndex failed to getItemByIndex."<<endl;
			}
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

RETURNCODE 
hCgrpItmBasedClass::getByIndex(UINT32 indexValue, hCitemBase*& pItem, bool suppress)
{
	RETURNCODE rc = DB_ATTRIBUTE_NOT_FOUND;
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getItemByIndex(indexValue,pItem,suppress);
	}
	return rc;
}

// check the existance
bool hCgrpItmBasedClass::isInGroup(UINT32 indexValue)
{
	bool retVal = false;

	if (pMemberAttr != NULL)
	{
		retVal = pMemberAttr->isItemInList(indexValue);
	}
	return retVal;
}


/* Label, Help  and Validity should be handled by the item base class */
hCattrBase*   hCgrpItmBasedClass::newHCattr(aCattrBase* pACattr)  // builder 
{
	hCattrGroupItems* pMemAttr = NULL;

	if (pACattr->attr_mask == maskFromInt(grpItmAttrElements)) 
	{
		//pMemAttr  = new hCattrCollMembers(devHndl(), (aCattrCollMembers*)pACattr, this );	
		pMemAttr  = new hCattrGroupItems(devHndl(), (aCattrMemberList*)pACattr, this );
		return pMemAttr;
		
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(grpItmAttrLabel) | 
			 maskFromInt(grpItmAttrHelp)  | 
			 maskFromInt(grpItmAttrValidity) | 
			 maskFromInt(grpItmAttrDebugInfo)  ))
	{//known attribute but shouldn't be here		
		LOGIT(CERR_LOG,"ERROR: group-item attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: group-item attribute type is unknown.\n");
		return NULL; /* an error */  
	}
};


hCgrpItmBasedClass::hCgrpItmBasedClass(DevInfcHandle_t h, aCitemBase* paItemBase) 
				  : hCitemBase(h, paItemBase)
{// itembase does Label, Help & Validiity
	pMemberAttr = NULL;

	// the only attribute left is elements
	for (AattributeList_t::iterator iT = paItemBase->attrLst.begin(); 
			iT < paItemBase->attrLst.end();                  iT++)
	{// iT isa ptr2ptr2 aCattrBase
		if ((*iT)->attr_mask == maskFromInt(grpItmAttrElements) )
		{
//clog<<itemType<<"+++++"<<endl;
			pMemberAttr = (hCattrGroupItems*)newHCattr(*iT);
//clog<<itemType<<"-----"<<endl;
			if (pMemberAttr != NULL)
			{
				pMemberAttr->setItemPtr((hCitemBase*)this);
				attrLst.push_back(pMemberAttr);// keep it on the list too
			}
			else // is NULL
			{
				grpItmAttrType_t en = (grpItmAttrType_t)mask2value((*iT)->attr_mask); 
				LOGIT(CERR_LOG,"++ No New attribute ++ (%s)\n",grpItmAttrStrings[en]);
			}
		}
	}
#ifdef _DEBUG
	if (pMemberAttr == NULL)
	{
		LOGIT(CERR_LOG,"++ No Elements attribute in group-item constructor ++ \n");
	}
#endif
};

/* from-typedef constructor */
hCgrpItmBasedClass::
hCgrpItmBasedClass(hCgrpItmBasedClass* pSrc, hCgrpItmBasedClass* pVal, itemIdentity_t newID)
	  : hCitemBase((hCgrpItmBasedClass*)pSrc, newID)
{// TODO-- verify this is correct for def + val instance
	// if pVal:
	// verify all possible members of pSrc exist in pVal
	// I think:: getAllindexValues of both and verify all of each are in the other
	// loop thru all possible members of both and instantiate via newPsuedoItem

	if ( pSrc->pMemberAttr != NULL )
	{// 20jul06 - Change in spec: everything this points to gets to be duplicated
		// conditional list MUST be duplicated!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		pMemberAttr =  (hCattrGroupItems *) (pSrc->pMemberAttr->new_Copy(newID));
		pMemberAttr->setItemPtr((hCitemBase*)this);

		// stevev 25jan07 - added the replication for collections etc (par 676)
		if (pVal != NULL && pVal->pMemberAttr != NULL)
		{
			pMemberAttr->setDuplicate(*(pVal->pMemberAttr),true);// replicate
		}
		// stevev end add 25jan07 
		attrLst.push_back(pMemberAttr);// keep it on the list too
		/* makes everything much worrse:::
		// sharedAttr = false;// stevev 28aug07-the only attribute is replicated 
						   //        so attributes are NOT shared (allows deletion)
			so we won't use it (for now) */
	}
	else
	{
		pMemberAttr = NULL;
	}
}


// we new'd and inserted the member list, we need to get rid of it
RETURNCODE hCgrpItmBasedClass::destroy(void)
{
	RETURNCODE rc = hCitemBase::destroy();// call our parent, he goes thru the attr list

	//hCgrpItmBasedClass does NOT have sharedAttr turned on...don't do this:: RAZE( pMemberAttr );

	int attrLeft = 0;
	// this irterator was causing a 'Expression: vector iterators incompatible' crash for fluke
	//for ( hAttrPtrList_t::iterator i=attrLst.begin();i<attrLst.end();++i)
	//{	if ( (*i) != NULL ) { attrLeft++; }   }
	// 13oct12 I changed to
	if (pMemberAttr)// we don't want to do this twice
	{		
		if (sharedAttr)			// 11sep14 fluke
		{						// 11sep14 fluke
			RAZE(pMemberAttr);	// 11sep14 fluke
		}						// 11sep14 fluke

		for ( hAttrPtrList_t::iterator i = attrLst.begin();i != attrLst.end(); ++i)
		{	hCattrBase* pab = *i;
			if ( pab ) 
			{ 
				attrLeft++; 
			}   
		}
	}

	if ( attrLeft == 0 )
	{
		pMemberAttr = NULL;
	}
#ifdef _DEBUG
	else
	LOGIT(CLOG_LOG,"Group Item Based class failed to destroy all attributes in Itembase.\n");
#endif
	return rc;
}

/* the aquire function is to treat groupitem classes like menues       */
/* action similar to menu's aquire but a psuedo menu list is generated */
/* item-arrays, collections, files                                     */
// stevev 23nov11 - The reference has to know it's a collection member so it can generate
//					the correct label and help string.
RETURNCODE 
hCgrpItmBasedClass::aquire(vector<hCmenuItem>& menuItemLst, const menuHist_t& history)
{
	RETURNCODE rc = SUCCESS;

	
	hCddbDevice *pCurDev = (hCddbDevice*)this->devPtr();
	if (pCurDev == NULL)
	{
		return FAILURE; // we can't operate in this condition
	}

	hCmenuItem localMenuItem(devHndl());
	
	hCitemBase* pItemBase = NULL;
	hCreference newRef(devHndl());

	unsigned uMenuItemType = 0;

	groupItemPtrList_t listOpGIDs;

	rc = getList(listOpGIDs);	
	
	if (rc != SUCCESS || listOpGIDs.size() <= 0 ) 
	{// note that a conditional without a default||else stmt will exit here (an empty list)
		menuItemLst.clear();	
	    return SUCCESS; // Vibhor : to clarify with Steve
	}
	else
	{	
		for (groupItemPtrList_t::iterator giT=listOpGIDs.begin(); giT!=listOpGIDs.end(); giT++)
		{// ptr2ptr2 a gid
			bool bIsNotAnItem = false;
			if ( (*giT)->getViaRef((hCitemBase*)this, newRef) != SUCCESS )
			{
				newRef.clear();
				LOGIT(CLOG_LOG,"GroupItem as Menu failed to get a via reference.\n");
				continue;
			}
			else
			{
				localMenuItem.setReference(newRef);
				newRef.clear();
				/* qualifier is left empty - WAP 30mar05 */
			}

			// 23nov11 - be sure the reference knows it was part of a collection
			localMenuItem.getRef().setMemberRef((hCcollection*)this, (*giT)->getIIdx());

			switch(localMenuItem.getRef().getRefType())
			{// these shouldn't exist in a collection
				case rT_Constant:
					localMenuItem.setDrawAs(mds_constString);
					bIsNotAnItem = true;
					break;
                //case rT_Image_id:
				//	localMenuItem.setDrawAs(mds_image);
				//	bIsNotAnItem = true;
				//	break;
				case rT_Separator:
					localMenuItem.setDrawAs(mds_separator);
					bIsNotAnItem = true;
					break;
				case rT_Row_Break:
					localMenuItem.setDrawAs(mds_newline);
					bIsNotAnItem = true;
					break;
				default:
					break; //TODO : Handle mds_bit style case

			}
			if(bIsNotAnItem == true)
			{					
				menuItemLst.push_back(localMenuItem);
				localMenuItem.clear();
				continue; // get the next one
			}
			rc = pCurDev->procure(localMenuItem.getRef(), &pItemBase);
			if( rc == SUCCESS && pItemBase != NULL)
			{					
				if ( ! (pItemBase->IsValid() ) )
				{ 
					localMenuItem.clear();
					continue;
				}

				/*  stevev 30mar05 - code that was here was moved to menuItem */
				localMenuItem.fillDrawType(pItemBase,history);					
				localMenuItem.pItemBase = pItemBase;
			
			}//endif // no else  rc == SUCCESS && pItemBase != NULL
			else
			{	
				LOGIT(CERR_LOG,"Coll as Menu did not procure a reference.\n");
				localMenuItem.clear();
				continue; // get the next one
            }
			menuItemLst.push_back(localMenuItem);
			localMenuItem.clear();// for the next round
		
		}//endfor
	
	}//endif rc == SUCCESS && menuItemLst.size() > 0

	return rc;
}


/* stevev 18jul05 */
CValueVarient hCgrpItmBasedClass::getAttrValue(unsigned attrType, int which)
{	CValueVarient rV;
	if (  ((grpItmAttrType_t)attrType) == grpItmAttrLabel || 
		  ((grpItmAttrType_t)attrType) == grpItmAttrHelp  ||
		  ((grpItmAttrType_t)attrType) == grpItmAttrValidity 
	   )
	{
		rV = hCitemBase::getAttrValue(attrType);
	}
	else
	if ( getIType() == iT_ItemArray )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: Ref Array attribute information is not accessable.(%d)\n",attrType);
	}
	else
	if ( getIType() == iT_Collection )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: Collection attribute information is not accessable.(%d)\n",attrType);
	}
	else
	if ( getIType() == iT_File )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: File attribute information is not accessable.(%d)\n",attrType);
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Unknown Type for attribute information.(%d)\n",attrType);
	}
	return rV;
}
/* end stevev 18jul05 */

// steve 12sep06 - calculate dependencies (attribute)
void hCgrpItmBasedClass::fillCondDepends(ddbItemList_t&  iDependOn)
{// the ONLY non-itembase attribute is the memberlist
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);

	if ( pMemberAttr == NULL )
	{
		LOGIT(CERR_LOG,"ERROR %s's Member List does not exist\n",getName());
		return;
	}
	pMemberAttr->getDepends(iDependOn);
}
// end  steve 12sep06


// stevev 22jan09 - add pass down capability to support list command insert remove

RETURNCODE hCgrpItmBasedClass::setCommandIndexes(ddbItemList_t& indexPtrList, int newIdxValue)
{
	RETURNCODE  rc = FAILURE;
	UIntList_t  localitemIDList;
	UIntList_t::iterator iItemID;
	hCitemBase* pItem   = NULL ;

	if ( ( rc = getAllindexes(localitemIDList) ) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{
			unsigned f = (unsigned) *(&(*iItemID));
			if ( devPtr()->getItemBySymNumber(f, &pItem) == SUCCESS && pItem != NULL )
			{
				pItem->setCommandIndexes(indexPtrList,newIdxValue);
			}
		}
		rc = SUCCESS;
	}// else move on
	return rc;
}

RETURNCODE hCgrpItmBasedClass::addCmdDesc(hCcommandDescriptor& rdD, bool isRead)
{
	RETURNCODE  rc = FAILURE;
	UIntList_t  localitemIDList;
	UIntList_t::iterator iItemID;
	hCitemBase* pItem   = NULL ;

	if ( ( rc = getAllindexes(localitemIDList) ) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{
			unsigned f = (unsigned) *(&(*iItemID));
			if ( devPtr()->getItemBySymNumber(f, &pItem) == SUCCESS && pItem != NULL )
			{
				pItem->addCmdDesc(rdD,isRead);
			}
		}
		rc = SUCCESS;
	}// else move on
	return rc;
}

// end stevev 22jan09

/*************************************************************************************************
 *  This is a Collection Only Function  ie collection-as-menu
 ************************************************************************************************/

/* stevev 27sep06 - more dependency extentions */
// get all possible items on this collection-as-menu
void  hCcollection::fillContainList(ddbItemList_t&  iContain)
{// fill with items-I-contain 
	if ( (! is_on_menu) ||  pMemberAttr == NULL ) return;		// not-a-container

	//pMemberAttr-> getAllindexes(UIntList_t& allindexedIDsReturned);
	//	pMemberAttr - get all possible lists out of conditional to a groupItemListPtrList_t
	//				- for each hCgroupItemList - call getItemIDs(returnList)
	//		getItemIDs	- for each hCgroupItemDescriptor in thisList
	//					-    get the reference 
	//					-	 reference:resolveAllIDs(returnList)
	/////////////////////////////////////////////////////////////////////////////////////////////

	vector<unsigned int>  localitemIDList;
	vector<unsigned int>::iterator iItemID;
	hCitemBase* pItem   = NULL ;

	if (getAllindexes(localitemIDList) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{
			if ( devPtr()->getItemBySymNumber(*iItemID, &pItem) == SUCCESS && pItem != NULL)
			{
				iContain.push_back(pItem);
			}
		}
	}// else move on
}
/** end stevev 27sep06 **/

/*************************************************************************************************
 *  The actual list of elements
 ************************************************************************************************/
void  hCgroupItemList::setEqual(void* pAclass)
{// assume: pAclass pts to a  class aCmemberElementList
	
	if (pAclass == NULL) return;//nothing to do
	if ( ((aCmemberElementList*)pAclass)->whatPayload() != pltGroupItemList )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: group item list's setEqual to a NON group item list.\n");
		return;
	}

	AmemberElemList_t* pcL = (aCmemberElementList*)pAclass;
	hCgroupItemDescriptor wrkMember(devHndl());

	reserve(pcL->size());	//PO reserve setequal
	for(AmemberElemList_t::iterator iT = pcL->begin(); iT<pcL->end(); iT++)
	{ // iT is ptr 2 aCmemberElementList..an aCcondList..which holds a list of conditionals
		if (&(*iT) != NULL)	// PAW added &(*) 03/03/09
		{
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
	|| defined(__GNUC__)
			wrkMember.setEqual(&(*iT));
#else
			wrkMember.setEqual(iT);
#endif
			push_back(wrkMember);
			wrkMember.clear();
		}
		else
		{
			LOGIT(CERR_LOG,"ERROR: received a NULL wrkMember ptr.\n");
		}
	} 
}

void  hCgroupItemList::duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID)
{// verify: pPayLd pts to a  class hCgroupItemList
	
	if (pPayLd == NULL)
	{ 
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Programming error - trying to duplicate a null payload.\n");
		return;//nothing to do
	}
	if ( ! validPayload(pltGroupItemList) )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Trying to duplicate a Group-Item List with a different payload.\n");
		return;//nothing to do
	}

	hCgroupItemList* pcL  = (hCgroupItemList*)pPayLd;
	hCgroupItemDescriptor wrkMember(devHndl());

	for(groupItemList_t::iterator iT = pcL->begin(); iT < pcL->end(); ++iT)
	{ // iT is ptr 2 hCgroupItemDescriptor
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
	|| defined(__GNUC__)
		wrkMember.setDuplicate(*((hCgroupItemDescriptor*)(&(*iT)) ), parentID);
#else
		wrkMember.setDuplicate(*((hCgroupItemDescriptor*)iT));
#endif
		push_back(wrkMember);
		wrkMember.clear();
	} 
}


RETURNCODE hCgroupItemList::getGroupItemPtrs(groupItemPtrList_t& giL )
{
	hCgroupItemDescriptor* pG;
	int entrySz = giL.size();
	for ( groupItemList_t::iterator iT = begin();  iT < end(); iT++  )
	{// iT is ptr 2a hCgroupItemDescriptor
#if _MSC_VER >= 1300  /* HOMZ - port to 2003, VS7  >> error C2440: 'type case' cannot convert from 'std::vector<_Ty> ..etc */ \
	|| defined(__GNUC__)
		pG = &(*iT);
#else
		pG = (hCgroupItemDescriptor*)iT;
#endif
		giL.push_back(pG);
	}
	if (giL.size() > (UINT32)entrySz) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		return SUCCESS;
	else
		return FAILURE;

}

/* adds (unique) index values to the return list for each group-item in this list */
RETURNCODE hCgroupItemList::getItemIndexes(UIntList_t& allindexedIDsReturned)
{
	RETURNCODE  rc = SUCCESS;
	UIntList_t:: iterator iUI;
	bool fnd = false;
	
	for ( groupItemList_t::iterator iT = begin();  iT < end(); iT++  )
	{// iT is ptr 2a hCgroupItemDescriptor
		fnd = false;
		for (iUI=allindexedIDsReturned.begin(); iUI < allindexedIDsReturned.end();iUI++)
		{
			if (*iUI == iT->getIIdx())
			{
				fnd = true;
				break; // out of for loop
			}
		}
		if ( ! fnd )
		{
			allindexedIDsReturned.push_back(iT->getIIdx());
		}
	}
	return rc;
}

/* all item ids referenced through each reference in this list's elements */
RETURNCODE hCgroupItemList::getItemIDs(UIntList_t& allindexedIDsReturned)
{
	RETURNCODE  rc = SUCCESS;
	
	for ( groupItemList_t::iterator iT = begin();  iT < end(); iT++  )
	{// iT is ptr 2a hCgroupItemDescriptor			  
		rc |= (iT->getRef ()).resolveAllIDs(allindexedIDsReturned);
	}
	return rc;
}

/*Vibhor 131004: Start of Code*/

RETURNCODE hCgroupItemList::getItemPtrs(ddbItemList_t &ptrList)
{
	RETURNCODE rc = SUCCESS;

	hCreferenceList localRefLst(devHndl());

	for(groupItemList_t :: iterator iT = begin(); iT != end();iT++)
	{
		localRefLst.push_back(iT->getRef());
	}

	ptrList.clear();

	rc = localRefLst.getItemPtrs(ptrList);

	localRefLst.clear();

	return rc;


}/*End getmemberList*/

/*Vibhor 131004: End of Code*/

/*************************************************************************************************
 *  The attribute of element lists
 ************************************************************************************************/

hCattrBase* hCattrGroupItems::new_Copy(itemIdentity_t newID)
{
// start of the tree:  we are the typeDef, we are returning a fully instantiated duplicate
// Variables, Lists, Ref-Arrays, Val-Arrays, and Collections in our list become typedefs too
// this must be done in the conditional so ALL branches are instantiated
	//itemIdentity_t dummyIdent; dummyIdent.newID=newID; dummyIdent.newName = "";
	hCattrGroupItems* pgi  =   new hCattrGroupItems(this, newID);
	return (hCattrBase*) pgi;
}

RETURNCODE 
hCattrGroupItems::getAllItemsByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{	
	RETURNCODE rc = SUCCESS;
	groupItemListPtrList_t  memLists;
	hCgroupItemDescriptor   localMember(devHndl());
	
	rc = condListOGrpItmLists.aquirePayloadPtrList(memLists);
	if ( rc == SUCCESS && memLists.size() >0)
	{//	for each list
		for (groupItemListPtrList_t::iterator iTe =memLists.begin(); iTe <memLists.end(); iTe++)
		{//iTm is ptr 2 ptr 2 hCgroupItemList
			hCgroupItemList *pgil = (*iTe);
			if (pgil == NULL)
			{
				LOGIT(CERR_LOG,"groupItemList has a NULL in the payload ptr list.\n");
				continue;
			}
#ifndef GETALL_DEBUG
			if ( (rc = pgil->getItemByIdx(indexValue, localMember, true) ) == SUCCESS )
			{// assumes indexes MUST be unique in a given list - we only use the first one
				returnedItemRefs.push_back(localMember.getRef());
			}
#else		/* is GETALL_DEBUG */
			if ( (rc = (*iTe)->getItemByIdx(indexValue, localMember, false) ) == SUCCESS )
			{// assumes indexes MUST be unique in a given list - we only use the first one
				returnedItemRefs.push_back(localMember.getRef());
			}
			else
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: getAllItemsByIndex getItemByIdx failed.(%d)\n",indexValue);
			}
#endif
			localMember.clear();// added back in 21feb07 merge (no notes as to why removed)
		}// next list out of resolved lists
		if ( returnedItemRefs.size() )
		{
			rc = SUCCESS;
		}
	}
	else
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Group Item List Attribute member list has no payloads.\n");
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}

/* aquires a unique list of index numbers for all possible accesses */
RETURNCODE hCattrGroupItems::getAllindexValues(UIntList_t& allindexedIDsReturned)
{	
	RETURNCODE rc = SUCCESS;
	groupItemListPtrList_t  elmLists;
	
	rc = condListOGrpItmLists.aquirePayloadPtrList(elmLists);
	if ( rc == SUCCESS && elmLists.size() >0)
	{
		for (groupItemListPtrList_t::iterator iTe = elmLists.begin(); 
																iTe < elmLists.end(); iTe++)
		{//iTm is ptr 2 ptr 2 hCgroupItemList
			rc |= (*iTe)->getItemIndexes(allindexedIDsReturned);
		}//
	}
	return rc;
}

/* All item ids possibly index through all paths in the conditional */
RETURNCODE hCattrGroupItems::getAllindexes(UIntList_t& allindexedIDsReturned)
{	
	RETURNCODE rc = SUCCESS;
	groupItemListPtrList_t  elmLists;
	
	rc = condListOGrpItmLists.aquirePayloadPtrList(elmLists);// all possible lists
	if ( rc == SUCCESS && elmLists.size() >0)
	{
		for (groupItemListPtrList_t::iterator iTe = elmLists.begin(); 
																iTe < elmLists.end(); iTe++)
		{//iTm is ptr 2 ptr 2 hCgroupItemList
			rc |= (*iTe)->getItemIDs(allindexedIDsReturned);
		}//
	}
	return rc;
};

void hCattrGroupItems::MakeStale(void)
{
	RETURNCODE rc = SUCCESS;
	ddbItemList_t locList;
	ptr2hCitemBase_t pItm;
	ddbItemLst_it   ItItm;

	rc = 	condListOGrpItmLists.resolveCond(&resolvedGIL);
	if (rc == SUCCESS && resolvedGIL.size() > 0 )
	{	
		rc = resolvedGIL.getItemPtrs(locList);	
	}
	else
	{	return;	
	} // failed

	if (rc == SUCCESS && locList.size() > 0 )
	{
		for (ItItm = locList.begin(); ItItm != locList.end(); ++ ItItm)
		{// ptr2ptr2itembase
			pItm = (ptr2hCitemBase_t)(*ItItm);
			pItm->MakeStale();
		}
	}
	return;
}

RETURNCODE hCattrGroupItems::getDepends(ddbItemList_t& retList)
{
	RETURNCODE rc = SUCCESS;
	
	// get what the conditional depends on
	condListOGrpItmLists.aquireDependencyList(retList, false);

	/* also depends on the conditional and/or validity of its items */
	groupItemListPtrList_t  localGIpL;
	groupItemListPtrList_t::iterator iGIpL;

	groupItemPtrList_t      localGIDpL;// complete list of descriptors
	groupItemPtrIterator_t  iGIDpL;

	ddbItemList_t           localIpL;
	ddbItemLst_it           iIpLst;

	hCdependency* pDep = NULL;

	// get what the resolution depends on: the reference and the item's validity
	if (condListOGrpItmLists.aquirePayloadPtrList(localGIpL) == SUCCESS && localGIpL.size() > 0)
	{// we have a list of all possible ptrs to hCgroupItemList's
		for (iGIpL = localGIpL.begin(); iGIpL != localGIpL.end();  ++iGIpL )
		{// ptr 2a ptr 2a hCgroupItemList
			if (*iGIpL)
			{
			(*iGIpL)->getGroupItemPtrs(localGIDpL);// appends onto end
		}
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Payload (in getDepends)has a null pointer.\n");
			}
		}
		// have a list of all possible groupitemdescriptors
		for (iGIDpL = localGIDpL.begin(); iGIDpL != localGIDpL.end(); ++iGIDpL  )
		{// ptr 2a ptr 2a hCgroupItemDescriptor
			if (*iGIDpL)
			{
			(*iGIDpL)->getpRef()->aquireDependencyList(retList,false);
			(*iGIDpL)->getpRef()->resolveAllIDs(localIpL);// all possible
		}
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,"ERROR: GIDpL (in getDepends)has a null pointer.\n");
	}
		}
	}
	// we have all possible resolved items now
	for (iIpLst = localIpL.begin(); iIpLst != localIpL.end(); ++ iIpLst )
	{// ptr 2a ptr 2a hCitemBase
		if (*iIpLst)
		{
		pDep = (*iIpLst)->getDepPtr(ib_ValidityDep);
		if ( pDep && pDep->iDependOn.size() > 0)
		{// pDep->iDependOn is a list of what this item's validity is dependent on (MT @ constant)
			hCdependency::insertUnique(retList,(*iIpLst));// append list uniquely
		}
	}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: localIpL (in getDepends)has a null pointer.\n");
		}
	}
	return rc;
}

// returns didChange
bool hCattrGroupItems::reCalc(void)
{// error till done

	bool isChanged = false;
	bool noErr     = true; // goes false on error
	int  newSz = 0;

	groupItemPtrList_t oldList, newList;
	groupItemPtrList_t::iterator o,n; //ptr 2 ptr 2 hCgroupItemDescriptor

	if (resolvedGIL.size() > 0 )
	{
		if ( resolvedGIL.getGroupItemPtrs(oldList) == SUCCESS && oldList.size() > 0 )
		{	// we have old list			
			resolvedGIL.destroy(); // stevev 27aug07 - get rid of the memory of the old list
			resolvedGIL.clear();	// getList will refill this 

			if ( getList(newList) == SUCCESS && ( newSz = newList.size() ) > 0 )
			{// we have both lists
				if (newSz != oldList.size())
				{	return true;// they must be different
				}
				// they are the same size
				for (o =  oldList.begin() , n=newList.begin();
				     o != oldList.end()  && n != newList.end();
					 ++o,++n )
				{
					if ( ((*o)->getIIdx() != (*n)->getIIdx()) ||
						 !((*o)->getRef() == (*n)->getRef() )   )
					{// something doesn't match
						return true; // didChange
					}
				}
			}
			else
			{// we have only one
			}
		}
		else
		{	// we have no lists
		}
	}
	else 
	{
		return true;// was nothing, now something, we're done
	}
	return false;
}

/*************************************************************************************************
 *  The individual elements
 ************************************************************************************************/
void hCgroupItemDescriptor::setEqual(void* pAitem)
{	
	if (pAitem==NULL) return;
	aCmemberElement* paCm = (aCmemberElement*)pAitem;

	itmIdx = paCm->locator;	// a long
	ref	   = paCm->ref	;	// aCreference
	memName= paCm->mem_name;    // the string version of the member name
	//descS  = paCm->descS	;	// aCddlString
	descS  = paCm->descS;	// wstring
	helpS  = paCm->helpS	;	// aCddlString

	// register member / idx with the device
	devPtr()->registerElement(itmIdx,memName,0);// zero - we don't know our container!!??!!
//clog<<"MEMBER:"<<memName <<"|"<<endl;
}

void hCgroupItemDescriptor::setDuplicate(hCgroupItemDescriptor& rGid, itemID_t parentID)
{	
	itmIdx = rGid.itmIdx;	
	elementID_t elemID; elemID.contID = parentID; elemID.which = itmIdx;
	ref.duplicate(&(rGid.ref), true, elemID);

	//descS.duplicate(&(rGid.descS), false);
	descS  = rGid.descS;	// wstring
	helpS.duplicate(&(rGid.helpS), false);
	
	memName= rGid.memName;
}

RETURNCODE hCgroupItemDescriptor::dumpSelf(int indent, char* typeName)
{
	RETURNCODE rc = SUCCESS;
	if (typeName == NULL)
		typeName = "GroupItemDescriptor";

	LOGIT(COUT_LOG,"%sGroup Item: %s # 0x%04x\n",Space(indent),typeName,itmIdx);
	rc  = ref.  dumpSelf(indent + 12);
	LOGIT(COUT_LOG,"%s            MemberName: %s\n",Space(indent),memName.c_str());
	rc |= descS.dumpSelf(indent + 12);
	// new type::>   LOGIT(COUT_LOG,L"\"%s\"",((wstring)descS).c_str());
	rc |= helpS.dumpSelf(indent + 12);

	return rc;
}

/* stevev 30mar05
 *  get the via_Array,collect reference from this member reference (for items as menues)*
 *
 * parameters: pBase = our base item (collection,array etc), 
 *             rRef  = returned reference           
 ****************************************************************************************
 * stevev 01oct10
 * The latest spec says that collections on a menu must be treated as menues.
 * To do that we have to strip off the collection connection.  That means that this list
 * has to be a list of references in the collection, not a list of collection references.
 * The collection insertion has been removed in the function but left as a parameter
 * to minimize the impact.
 */
RETURNCODE hCgroupItemDescriptor::getViaRef(hCitemBase* pBase, hCreference& rRef)
{
	rRef = ref; // stevev 01oct10 nothing else is required
	return SUCCESS;
#if 0
	/*	
	unsigned long    itmIdx;
	hCreference		 ref;

	hCddlString      descS;
	hCddlString      helpS;
	*/
	/* stevev 8aug07 - we can't use pointers to local expression/reference or the 
		auto deletion at exit barfs all over it - use new() so delete() will work*/
	//stevev 8aug07 aCreference      thisRef;
	aCreference*      pRef = new aCreference;   //stevev 8aug07 - sub'd below
  //thisRef.rtype is below
	pRef->id    = pBase->getID();// this
	pRef->isRef = 0; // an id, notaref
	pRef->type  = pBase->getIType(); // this type
	// all else is as clear()ed

	aCreference      locAref;
  //locAref.rtype is below
	locAref.isRef    = 1;// we're making a via_xxx
	locAref.id       = 0;
	locAref.type     = ref.getCtype();  // itemtype... 
	locAref.subindex = 0;
	locAref.iName    = 0;// wtf - over

	locAref.exprType = eT_Direct;
	  //stevev 8aug07	aCexpression     locAexpr;			// the index
	  aCexpression*    pAexpr = new aCexpression;//stevev 8aug07 - sub'd below----was     locAexpr;
	  aCexpressElement locExEle;
	  locExEle.expressionData = (unsigned int)itmIdx;
	  locExEle.exprElemType   = eet_INTCST;
	  pAexpr->expressionL.push_back(locExEle);
	locAref.pExpr = pAexpr;

	locAref.pRef  = pRef;	// &thisRef;

	if (pBase->getIType()==iT_ItemArray)
	{	locAref.rtype = rT_viaItemArray;
		pRef->rtype   = rT_ItemArray_id;}
	else
	if (pBase->getIType()==iT_Collection)
	{	locAref.rtype = rT_via_Collect;	
		pRef->rtype   = rT_Collect_id;	}
	else
	if (pBase->getIType()==iT_File)
	{	locAref.rtype = rT_via_File;	
		pRef->rtype   = rT_File_id;		}
	else
	{	return FAILURE;					}		

	rRef = locAref;
	//stevev 8aug07 -  the plan is for locAref to destruct on exit. it deletes ref & expr
	return SUCCESS;
#endif
}
/* end stevev 30mar05 */

/* ***** stevev 02nov06 ***** */
/* add aquire tree to do as a menu does and fill/locate self */
bool hCcollection::aquireTree(hCitemBase* pB, hCmenuTree* thisMenusDesc, const menuHist_t& history,
							  bool thisLevelOnly)
{
	if (thisLevelOnly) 
	return
		hCmenu::aquireTree(this, thisMenusDesc, history, 0L);
	else
	return
		hCmenu::aquireTree(this, thisMenusDesc, history, getID());
}

/*
*********************************************************************
 *
 *   $History: ddbCollAndArr.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */
