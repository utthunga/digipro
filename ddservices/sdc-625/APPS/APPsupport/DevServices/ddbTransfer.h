/*************************************************************************************************
 *
 * $Workfile: ddbTransfer.h $
 * 24nov08 - stevev
 *
 *************************************************************************************************
 * Copyright (c) 2008, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		This defines the transfer piece-parts that several files need to know about.
 *
 * #include "ddbTransfer.h"
 *
 */


#ifndef _DDBTRANSFER_H
#define _DDBTRANSFER_H

#include <map>

class hCVar;
class hCmenu;
class hCitemArray;
class hCcollection;
class hCarray;
class hCfile;
class hClist;
class hCblob;

class hCdeviceSrvc; // instead of "ddbDeviceService.h"

class hPkt;
class hCtransaction;

#define MY_MAX_SEGMENT	 235
#define TRANSFER_DATA_SZ 0x100


/*	preamble 2, start char 1,address 5,command 1, bytecnt 1,responscode 1,status 1,
	port_number 1, function_code 1, byte_cnt_n 1, master_byte_cntr 2, device_byte_cntr 2,
	checksum 1 >= 20; 255-20 = 235
*/
 /*	  "Transfer Data" usually a 112 function
	  "Open Port"},  --> to slave::'time to start'
	  "Reset Port"}, --> to slave:: basically an unconditioanl close

	  "Ready to Close"},  --> 'time to close'
	  "Port Closed" answer<-- 'OK'
			or
	  "Transfer Data" answer <-- 'I have more to do'
*/
typedef enum sessionState_e
{
	ss_Undefined,	/* closed, has never been opened */
	ss_Opening,		/* transitory while command is In-Air */
	ss_Opened,		/* ready to transfer */
	ss_Closing,		/* transitory while command is In-Air */
	ss_Closed		/* no more transmission or stream filling will take place */
}sessionState_t;


typedef enum transferState_e
{
	ts_Undefined // others to be determined

}transferState_t;


typedef enum transferFunc_e
{
	tf_Transfer,	// 0
	tf_OpenPort,
	tf_ResetPort,
	tf_Ready2Close,
	tf_PortClosed
}transferFunc_t;

typedef struct bufChunk_s
{
	unsigned char  len;
	unsigned short masterStart;	 // master's count of first byte
	unsigned short slaveStart;   // my count of first byte
	unsigned char  data[TRANSFER_DATA_SZ];   

	void clear(){len = 0; masterStart = slaveStart=0; memset(data,0,TRANSFER_DATA_SZ);};
#if !defined(__GNUC__)
	struct
#endif // __GNUC__
	bufChunk_s(){clear();};
}bufChunk_t;

typedef std::vector<bufChunk_t>		bufChunkList_t;
typedef bufChunkList_t::iterator	bufChunkIter_t;

// names of needed variables
#define RESPONSECD_STR "response_code"
#define DEVICESTAT_STR "device_status"
#define PORT_NUMBR_STR "port_number"
#define FUNCTIONCD_STR "function_code"
#define MASTERMAXL_STR "master_max_seg_len"
#define DEVICEMAXL_STR "device_max_seg_len"
#define MASTERBYTC_STR "master_byte_cntr"
#define DEVICEBYTC_STR "device_byte_cntr"

// forward definition
class hCTransferChannel;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *  hCPort				- the port support package base class
 *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
class hCPort
{
	hCdeviceSrvc *		pDev;
	hCTransferChannel*  pChan;

public://con/des tructors
	hCPort(hCdeviceSrvc *p, hCTransferChannel*  pCh):pDev(p),pChan(pCh)  {};
	virtual
	~hCPort(){};

	virtual
		void open(){};
	virtual
		void close(){};
	virtual 
		unsigned char getMaxSegLen(void) { return MY_MAX_SEGMENT; };
	virtual		
		bool isSupported() { return false; };// must be overridden by a real port support

public:
	virtual// command 112, called  before sending (sim-reply, sdc-request)
		int receivedTransport(){return -1;};
	virtual// command 111,   called  before sending (sim-reply, sdc-request)
		int receivedSession(){return -1;};

public:	
	 bool isSIM;
};

// where we get support for the ports
extern 
hCPort* newPortSupportClass(unsigned char portNumber, hCdeviceSrvc * pD, hCTransferChannel* P );


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *   hCTransferChannel 
 *	Desc:  the basic port information & functions needed for session and tranport functions
 *
 *	currently implemented in ddbCmdDispatch.cpp
 *
 *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

class hCTransferChannel
{
	hCdeviceSrvc *  pDevice;
	hCPort*        pPortSupport;

public://data
	sessionState_t  Session_State;

	// for getTransferStatus
	unsigned char   respCd;
	unsigned char   devStat;// unused at this time
	unsigned char   funcCd;
	// for port function
	unsigned char	portNumber;
	unsigned char	max_segment_len;  // as negotiated
	unsigned char   padding[3];

	unsigned short  startMasterSync;  // where massa started
	unsigned short	masterSync;		  // where the massa is
	unsigned short  maserSyncRolloverCnt;	// location extension (will be 1 too hi)

	unsigned short  startSlave_Sync;  // where massa started
	unsigned short	slave_Sync;		  // where the slave is
	unsigned short  slaveSyncRolloverCnt;	// location extension (will be 1 too hi)

	bufChunkList_t  recv_Buffer; // vector of packet specifics
	unsigned long   activeRecvChunk;		// the index of the active buffChunk
	transferState_t recvState;
#define RCHUNK  (recv_Buffer[activeRecvChunk])	

	bufChunkList_t  xmit_Buffer; // vector of packet specifics
	unsigned long   activeXmitChunk;		// the index of the active buffChunk
	transferState_t xmitState;
#define XCHUNK  (xmit_Buffer[activeXmitChunk])

	// I'm duplicating these for every port so I don't have to go to the device for these
	hCitemBase *pRC,*pDS,*pPN,*pFC,*pMMaxSeg,*pSMaxSeg,*pMBC,*pDBC;// actually hCVar s

public://con/des tructors
	hCTransferChannel(hCdeviceSrvc *p, unsigned char portNum);    
	virtual
		~hCTransferChannel(){ RAZE( pPortSupport ); };

public://implementation
	virtual	void sendSession(void);						
	virtual	void replySession(hPkt* pSndPkt, hCtransaction* pTran);//reply is Simulation only...
											        //only fills the packet, must handle max seg
	virtual	void reset(void);			// call support reset, close
	virtual	void open(void);			// call support open, open
	virtual	void close(void);

	virtual void downloadData(void);//doesn't return till its done
	virtual void uploadData(hPkt* pSndPkt, unsigned short massa);

	virtual	bool transferComplete(void); // returns true if no more remains to transfer

	unsigned char getMaxSegLen() {return pPortSupport->getMaxSegLen();};

	// Xmit   device object to port buffers
	int fillPortBuffer(itemID_t    itemNumber);
	int fillPortBuffer(hCitemBase* pItem);

	// Recv   port buffers to device object
	int emtyPortBuffer(itemID_t    itemNumber);
	int emtyPortBuffer(hCitemBase* pItem);

	// support functions
	virtual	int setupCmd111(transferFunc_e fc);// uses port info for the rest
	virtual	int fillPortInfo(int cmdNo = 112);// minor diffs between 111 & 112

	bool isSupported() 
	{ if (pPortSupport != NULL) return(pPortSupport->isSupported()); else return false; };

protected: // Xmit helper functions
	//int fillPortBuffer( unsigned long itemNumber);
	//int fillPortBuffer( hCitemBase* pItem);

	int fillVar ( hCVar* pVar );
	int fillMenu( hCmenu* pMenu );
	int fillIarr( hCitemArray* pIarr );
	int fillColl( hCcollection* pColl );
	int fillVarr( hCarray* pVarr );
	int fillFile( hCfile*  pFile );
	int fillList( hClist*  pList );
	int fillBlob( hCblob*  pBlob );

	int fillPointers(void);
};


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Ports
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* these should not be needed in SDC...we're leaving them for now */
 class hCRdDevConfig : public hCPort
{
 public:	 
	hCRdDevConfig( hCdeviceSrvc * pD, hCTransferChannel* p);

	void open();
	void close();
	unsigned char getMaxSegLen(void);
	bool isSupported();
	
	int receivedTransport();// command 112,    called  before sending (sim-reply, sdc-request)
	int receivedSession();// command 111, called right before sending (sim-reply, sdc-request)

};


#endif //_DDBTRANSFER_H
