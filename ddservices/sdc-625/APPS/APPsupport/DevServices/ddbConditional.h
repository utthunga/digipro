/**********************************************************************************************
 *
 * $Workfile: ddbConditional.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the conditional base class
 *		8/9/2	sjv	created - from dllapi.h and conditional.h
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *
 * #include "ddbConditional.h"
 */

#ifndef _DDBCONDITIONAL_H
#define _DDBCONDITIONAL_H

#include "pvfc.h"
#pragma warning (disable : 4786) 
#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbdefs.h"
#include "varient.h"
#include "DDBpayload.h"
#include "dllapi.h"
#include "ddbGlblSrvInfc.h"

#ifdef _DEBUG
 #include <assert.h>
#else /* release */
 #ifndef assert
  #define assert(f)
 #endif
#endif

extern char axiomStrings[5][AXIOMSTRMAXLEN];
extern char clauseStrings[8][CLAUSESTRMAXLEN];

#ifndef  MOVED_TO_DDBREFERENCENEXPRESSION_H
#include "ddbReferenceNexpression.h"


#else
	22Jun05 - stevev - removed the moved code for clarity 
		    - use earlier version to see what was here
#endif // MOVED_TO_DDBREFERENCENEXPRESSION_H

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	hCgenericConditional is a generic template for all conditionals
		see tutorial file for a detailed explaination
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
template<class DESTTYPE, class PAYLDTYPE>  /* desttype is a conditional OR conditionalList */
class hCgenericConditional : public hCobject
{
public:
			class hCexpressDest : public hCobject
			{
			public:
				hCexpressDest(const hCexpressDest& ed) 
					: hCobject(ed.devHndl()),destExpression(ed.devHndl()),
										destType(cT_Unknown), pCondDest(NULL), pPayload(NULL)
				{	//21oct11 modified to simply use operator=
					operator=(ed);// make a copy
				};
				hCexpressDest(DevInfcHandle_t h) : hCobject(h), destExpression(h),
										destType(cT_Unknown), pCondDest(NULL), pPayload(NULL)
				{
					destExpression.clear();
				};

				virtual ~hCexpressDest()
				{   //21oct11 now we have operator= & copy constructor, always delete payload
					destroy();
				};

				/* stevev 21oct11 - add operator = for a copy operator.  Then add deletes to
				    destructor and destroy.
				*/
				hCexpressDest& operator=(const hCexpressDest& s)
				{// moved from copy constructor 21oct11
					destType       = s.destType;
					destExpression = s.destExpression;// operator=
				 // stevev added 13oct11  we have to take ownership of the memory so destroy
				 //						won't cause a crash in the future when cond is deleted
				 /* stevev 20oct11 - this doesn't work for stuff like using a typedef in an 
					array.  The	the operator= is expecting the array element to be a copy of 
					the typedef,  not take ownership of the memory...
					A copy constructor should make a copy!
				 pEDest->pCondDest = NULL; pEDest->pPayload = NULL;
				 */
					hCexpressDest* pS = (hCexpressDest*)&s;// un-constify it
					if (s.pCondDest)
					{
						pCondDest = new DESTTYPE (*(pS->pCondDest));
					}
					if (s.pPayload)
					{
						
						pPayload  = new PAYLDTYPE(*(pS->pPayload));
					}
					return *this;
				};


				RETURNCODE destroy(void)
				{
						if (pCondDest){ /*sjv 25jul06 delete pCondDest; pCondDest = NULL;*/
							pCondDest->destroy();//pre 31aug06 (crashed) DESTROY(pCondDest);
							delete pCondDest;  // add delete 21oct11
							pCondDest  = NULL; // add null   21oct11
						} 
						if (pPayload != NULL) 
						{// a destruction crash here usually means the destructor has an issue
							pPayload->destroy();
							delete pPayload;  
							pPayload  = NULL;
						}   
						destType = cT_Unknown;
					return destExpression.destroy();             
				};
                //CPMHACK: Delete the pCondDest and pPayload objects memory in clear
                void clear(void)
                {
                    destType = cT_Unknown;
                    if (pCondDest)
                    {
                        pCondDest->destroy();//pre 31aug06 (crashed) DESTROY(pCondDest);
                        delete pCondDest;  // add delete 21oct11
                        pCondDest = NULL;
                    }
                    if (pPayload)
                    {
                        pPayload->destroy();
                        delete pPayload;
                        pPayload  = NULL;
                    }
                    destExpression.clear();
                };
				//expressionType_t   destType;	//this is the destination type     
				//			    //     - direct,then,else,case,default are the ONLY valid types
				clauseType_t destType;
				DESTTYPE*    pCondDest;	// destination type that is not direct 
										//                  (all others:then,else,case,default)
				PAYLDTYPE*   pPayload;	// destination type at is a direct

				hCexpression destExpression; // CASE type EXPRESSION only - otherwise Empty	
				
				void dumpSelf(int indent=0)
				{
					LOGIT(COUT_LOG,"%sDestination Type = %s\n",Space(indent),
																 clauseStrings[(int)destType]);
					if ( pCondDest != NULL )
					{	pCondDest->dumpSelf(indent);
					}
					else
					if ( pPayload != NULL)
					{	pPayload->dumpSelf(indent);
					}
					else
					{	LOGIT(COUT_LOG,
					"%sNo COnditional Nor Payload in Expression Destination.\n",Space(indent));
					}
					if (destType == cT_CASE_expr)
					{	destExpression.dumpSelf(indent+3);
					}
				};

			};

			typedef vector<hCexpressDest> dest_List;

    //expressionType_t priExprType;   // type <if, select, direct are the ONLY valid types>
	//				 //		- direct implies there is a single payload item in the dest_list
	axiomType_t      priExprType;
	hCexpression     priExpression; // primary expression - empty on direct

	dest_List        destElements;  // a vector of destinations
					//   - direct,then,else,case,default are the ONLY valid values

	
//temp regress to previous version:: 	hCgenericConditional(const hCgenericConditional& src);
	hCgenericConditional(DevInfcHandle_t h) : hCobject(h), priExpression(h),
																	   priExprType(aT_Unknown) 
	{};
	virtual ~hCgenericConditional();
	RETURNCODE destroy(void);

/* * * Methods * * */
	RETURNCODE resolveCond(PAYLDTYPE* pRetVal, bool isAtest=false);

	RETURNCODE aquirePayloadPtrList(vector<PAYLDTYPE *>& retList, bool checkExpression = false);// all possible outcomes
	RETURNCODE aquireDependencyList(ddbItemList_t& retItemIDs, bool getPayloads);//from express
	/* note on aquireDependencyList: When getPayloads is true, the expression dependencies AND 
			all the actual payloads and their dependendencies are put in the list.  this is 
			mostly used in expressions chains where a complex reference is depended upen, so
			its validy AND value are needed.
			When getPayloads is false then only the conditional dependencies are acquired.
			This is usually used in conditionals where the payload does not impact the 
			existence of payload.
	*******/

	void clear(void)
	{	
		priExpression.clear();
		priExprType = aT_Unknown;
		hCexpressDest* pEd;
		for(typename dest_List::iterator iT = destElements.begin(); iT != destElements.end();++iT)
		{	pEd = &(*iT);
			pEd->clear();	
		}
		if (destElements.size())
			destElements.clear(); 
	};
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);

	//make a hCcond from an abstract
	hCgenericConditional& makeEqual(aCgenericConditional* pAbsSrc);
	hCgenericConditional& makeEqual(hCgenericConditional<DESTTYPE,PAYLDTYPE>& rSrc,
														bool replicate, itemID_t parentID = 0);
	RETURNCODE setPayload(PAYLDTYPE& tmpPayld);
	/* stevev 19jan05 - dependency */
	//RETURNCODE dependency(itemIDlist_t& depList);
 
private:	// helpers
	hCexpressDest* findDest(clauseType_t targetClaws);
};

template<class DESTTYPE, class PAYLDTYPE>
hCgenericConditional <DESTTYPE, PAYLDTYPE>
::~hCgenericConditional()
{ 	
	/*** regress to previous version :: put back:: */
	/* stevev 27aug10 - continue to prevent destructor crashes
      if( destElements.size() != 0 )// from vivek@invensys 12aug10
            destElements.~vector();
	***/
	destroy(); // stevev 09may13
	// in destroy()    priExpression.destroy();// stevev 05oct11 - looking to stop memory leak
	clear(); // end stevev 27aug10
	/*** regress--- don't delete pointers! ****
	FOR_iT(typename dest_List, destElements)
	{// iT is a ptr to a hCexpressDest
		if (iT->pPayload != NULL)
		{
			iT->pPayload->destroy();
			delete (iT->pPayload);
			iT->pPayload =  NULL;
		}
		if (iT->pCondDest != NULL)
		{
			iT->pCondDest->destroy();
			delete iT->pCondDest;
			iT->pCondDest = NULL;
		}
		iT->destExpression.destroy();
	}
	destElements.clear();

	priExpression.destroy();
	************* end regress *******************/
};

template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
::setPayload(PAYLDTYPE& tmpPayld)// payload values must be set before entry
{
	PAYLDTYPE*      pLoad;
#ifdef _DBGCOND
	LOGIT(CLOG_LOG,"DIRECT:SetPAYLOAD ");
#endif	
	hCexpressDest wrkDest(devHndl());
	wrkDest.clear();
	///*my*/expression.setDestExprType(eT_Direct);
	priExprType = aT_Direct;
	priExpression.clear();
	
	pLoad = new PAYLDTYPE(tmpPayld);// copy constructor
	if ( pLoad == NULL )
	{
		return APP_MEMORY_ERROR;
	}
	wrkDest.destType = cT_Direct;
	wrkDest.pPayload = pLoad;
	wrkDest.pCondDest= NULL;
	wrkDest.destExpression.clear();
	
	destElements.push_back(wrkDest);// copies
	//dest_elements now makes a deep copy...don't clear,let the default destructor handle it.
	// DD@fluke   wrkDest.clear();
	return SUCCESS;

};//setPayload

template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
::destroy(void)
{	RETURNCODE rc = SUCCESS;

	FOR_iT(typename dest_List, destElements)
	{// iT is a ptr to a hCexpressDest
		hCexpressDest* pEd = &(*iT);
	//VMKP 260304
		/* stevev 25jul06 - made this a part of hCexpressDest.destroy()
		if (iT->destType == cT_Direct)
		{
		rc |= iT->destroy();
		}
		else
		{
			iT->pCondDest->destroy();
			delete iT->pCondDest;
			iT->pCondDest = NULL;
		}
		end 25jul06 **********/
		if (pEd)
			rc |= pEd->destroy();
	}

	destElements.clear();
	rc = priExpression.destroy();

	//VMKP 260304
	return rc;
};

template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
::aquirePayloadPtrList(vector<PAYLDTYPE *>& retList, bool checkExpression )// all possible outcomes
{	
	RETURNCODE rc = SUCCESS;
	if ((priExprType == aT_IF_expr || priExprType == aT_SEL_expr) && checkExpression )
	{
		rc = priExpression.checkExpressionVars();
		if ( rc )// else all is well, continue processing
			return rc;
	}
	FOR_iT(typename dest_List, destElements)
	{// iT is a ptr to a hCexpressDest
		hCexpressDest* pEd = (hCexpressDest*)&(*iT);
		if ( pEd->destType == cT_Direct )
		{
			retList.push_back(pEd->pPayload); // push the pointer
		}
		else
		if (pEd->pCondDest != NULL)
		{
			if (checkExpression)// all others:then,else,case,default
			{
				rc = pEd->destExpression.checkExpressionVars();
				if ( rc )// else all is well, continue processing
					return rc;
			}
			rc = pEd->pCondDest->aquirePayloadPtrList(retList,checkExpression);
			if (checkExpression && rc == COND_VAR_USE_INVALID)
			{
				break; // out of for loop to exit with the error
			}
		}
		else 
		{
			LOGIT(CERR_LOG,
				"ERROR: Conditional is NOT Direct type but Conditional Ptr is NULL.\n");
			if (pEd->pPayload != NULL )
			{
			LOGIT(CERR_LOG,"     : Conditional's Payload Ptr is HAS a Value.\n");
			}
		}
	}//next
	return rc;
};


template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
::aquireDependencyList(ddbItemList_t& retItemIDs, bool getPayloads)//from expression
{	
	RETURNCODE rc = FAILURE;
	int rcnt = retItemIDs.size();// for logginh if required..
	if (priExprType > aT_Unknown && priExprType < aT_Direct )
	{
		LOGIF( LOGP_DEPENDENCY )
			(CLOG_LOG,"DEP: Conditional's Primary Expression\n");
		rc = priExpression.aquireDependencyList(retItemIDs, true);// always get payload vars
	}
	else
	{// direct or bad
		// don't clear, others may have inserted some....retItemIDs.clear();
		rc = SUCCESS;
	}
	if (destElements.size() > 0)
	{
		for (typename dest_List::iterator id = destElements.begin();id!=destElements.end();++id)
		{// ptr2a hCexpressDest
			hCexpressDest* pE = &(*id);
			switch (pE->destType)
			{
			case cT_CASE_expr:// = 3,	// implies there is an expressionL
				{
					LOGIF( LOGP_DEPENDENCY )
						(CLOG_LOG,"DEP: Conditional's Case Expression\n");
					rc = pE->destExpression.aquireDependencyList(retItemIDs, true);
				}
				break;
			case cT_Direct:  // 4
				{// resolves to pPayload
					if ( getPayloads )
					{	
						LOGIF( LOGP_DEPENDENCY )
							(CLOG_LOG,"DEP: cT_Direct without a body!\n");
					}
				}
				break;
		// the rest imply that: expressionL & exprDependsL are MT
			case cT_IF_isTRUE://    // 5 THEN
			case cT_IF_isFALSE://   // 6 ELSE
			case ct_SEL_isDEFAULT:  // 7 always true
				{
					if (pE->pCondDest == NULL)
					{
						LOGIT(CERR_LOG,
						"ERROR: dest element with THEN,ELSE | DEFAULT but no condDest ptr.\n");
					}
					else
					{
						LOGIF( LOGP_DEPENDENCY )
							(CLOG_LOG,"DEP: Conditional's If/Then/Default Destination\n");
						rc = pE->pCondDest->aquireDependencyList(retItemIDs,getPayloads);
					}
				}
				break;
			case cT_Unknown://   = 0,
			default:
			   LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: dest element UNKNOWN destination type.\n");
			}// endswitch
		}// next dest item
	}
	if ( rcnt <= (int)retItemIDs.size() )
	{
		LOGIF( LOGP_DEPENDENCY )
			(CLOG_LOG,"DEP: Exit Conditional's AcquireDeps------added %d -------\n",
				retItemIDs.size() - rcnt);
	}
	
	return rc;
};



#ifdef NOW_LOCATED_IN_THE_COND_RESOLUTION_FILE
/*
template<class DESTTYPE, class PAYLDTYPE>
PAYLDTYPE* hCgenericConditional <DESTTYPE, PAYLDTYPE>
::resolveCond(void) // use to pass in interface::X..CcommAPI* pCcommAPI)
{
	PAYLDTYPE* pRet  = NULL;
#if 1
	//aPayldType* pPay = NULL;
	
	//hCconditional* paC = &(((aCattrLabel*)paB)->condLabel);
	if (priExprType == eT_Direct)
	{
		pRet = (PAYLDTYPE*)destElements[0].pPayload; 
	}
	else
	{
		//sf = "+Conditional+";
		cerrxx<< " resolveCond is a true conditional." << endl;
		// much work to do
	}

#endif

	return pRet;
};
*/
#endif // moved



// set this conditional a 100% duplicate of the source conditional
template<class DESTTYPE, class PAYLDTYPE>
hCgenericConditional<DESTTYPE, PAYLDTYPE>& hCgenericConditional <DESTTYPE, PAYLDTYPE>
::makeEqual(hCgenericConditional<DESTTYPE,PAYLDTYPE>& rSrc, bool replicate, itemID_t parentID )
{
	int cnt = rSrc.destElements.size();

	priExprType   = rSrc.priExprType;
#ifdef _DEBUG
	if ( priExprType < aT_Unknown || priExprType > aT_Direct)
	{
		LOGIT(CERR_LOG,
		"ERROR: generic conditional (makeEqual-Dup) was passed an invalid priExprType.\n");
	}
#endif
	priExpression.setDuplicate(rSrc.priExpression);// expressions aren'r duplicated

	if ( cnt == 0)
	{// may be normal with constant references
		LOGIT(CERR_LOG,
		"genericCond.makeEqual(dup) was passed a source with no destination elements.\n");
		return *this;
	}// else continue
	destElements.clear();
	destElements.reserve(cnt);
	hCexpressDest wrkingDest(devHndl());
	wrkingDest.clear();

	for (typename hCgenericConditional::dest_List::iterator pIT = rSrc.destElements.begin();
			pIT < rSrc.destElements.end();		pIT++)
	{// pIT is a ptr 2 hCexpressDest
		wrkingDest.destType       = pIT->destType;
		wrkingDest.destExpression.setDuplicate(pIT->destExpression);

		if (pIT->pCondDest != NULL)
		{// pCondDest is either a conditional or a conditional list, they have to cast
			wrkingDest.pCondDest = new DESTTYPE(devHndl());
			wrkingDest.pCondDest->setDuplicate( *(pIT->pCondDest), replicate, parentID);
		}
		else
		{
			wrkingDest.pCondDest = NULL;
		}

		if (pIT->pPayload != NULL )
		{
			wrkingDest.pPayload = (PAYLDTYPE*) 
				                  hCpayload::newPayload(devHndl(),pltUnknown,pIT->pPayload);
			if ( wrkingDest.pPayload == NULL )
			{
				LOGIT(CERR_LOG,
					"ERROR: hCgenericConditional.makeEqual(Dup) cannot make a payload.\n");
			}
			else
			{	// sets one to the other
 				((PAYLDTYPE*)wrkingDest.pPayload)->duplicate(pIT->pPayload,replicate,parentID);
			}
		}
		else
		{
			wrkingDest.pPayload =  NULL;
		}
		destElements.push_back(wrkingDest); // pointer ownership transfers to the list!!
		wrkingDest.clear();
	}
	return *this;
};

// set the conditional to the abstract values
template<class DESTTYPE, class PAYLDTYPE>
hCgenericConditional<DESTTYPE, PAYLDTYPE>& hCgenericConditional <DESTTYPE, PAYLDTYPE>
//::makeEqual(aCconditional/*aCgenericConditional<void,void>*/ * pC)
::makeEqual(aCgenericConditional * pC)
{
	if ( pC == NULL)
	{
		LOGIT(CERR_LOG,"genericCond.makeEqual was passed a null source pointer.\n");
		return *this;
	}// else continue

	aCgenericConditional* pAbsSrc = (aCgenericConditional*)pC;
	int cnt = pAbsSrc->destElements.size();
	if ( cnt == 0)
	{
		LOGIT(CERR_LOG,
			"genericCond.makeEqual was passed a source with no destination elements.\n");
		return *this;
	}// else continue

	// for some reason this can't be done inside the for() expression
//	aCgenericConditional<void,void>::dest_List::iterator 
//		pIT = pAbsSrc->destElements.begin();

	priExprType   = axiomFrom(pAbsSrc->priExprType);
#ifdef _DEBUG
	if ( priExprType < aT_Unknown || priExprType > aT_Direct)
	{
		LOGIT(CERR_LOG,
			"ERROR: generic conditional (makeEqual) was passed an invalid priExprType.\n");
	}
#endif
	priExpression = pAbsSrc->priExpression;

	destElements.clear();
	destElements.reserve(cnt);
	hCexpressDest wrkingDest(devHndl());
	wrkingDest.clear();

	for (aCgenericConditional::dest_List::iterator pIT = pAbsSrc->destElements.begin();		   
			pIT < pAbsSrc->destElements.end();		pIT++)
	{// pIT is a ptr 2 aCexpressDest
		aCgenericConditional::aCexpressDest* paE = &(*pIT);
		wrkingDest.destType       = clauseFrom(paE->destType);
		wrkingDest.destExpression = paE->destExpression;

		if (paE->pCondDest != NULL)
		{// pCondDest is either a conditional or a conditional list, they have to cast
			wrkingDest.pCondDest = new DESTTYPE(devHndl());
			wrkingDest.pCondDest->setEqualTo(paE->pCondDest); 
		}
		else
		{
			wrkingDest.pCondDest = NULL;
		}

		if (paE->pPayload != NULL )
		{
			// wrkingDest.pPayload = new PAYLDTYPE;
			payloadType_t ldType = paE->pPayload->whatPayload() ;
			if ( ldType <= pltUnknown || ldType >= pltLastLdType )
			{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: aCexpressDest has a illegal payload type.\n");
			}
			else 
			{
				wrkingDest.pPayload = (PAYLDTYPE*) hCpayload::newPayload(devHndl(),ldType,NULL);
				if ( wrkingDest.pPayload == NULL )
				{
					LOGIT(CERR_LOG,
						"ERROR: hCgenericConditional.makeEqual cannot make a payload. for %d\n"
						 , (int)ldType);
				}
				else
				{
 					//((hCpayload*)wrkingDest.pPayload)->setEqual(paE->pPayload);
 					//((PAYLDTYPE*)wrkingDest.pPayload)->setEqual(paE->pPayload);
					wrkingDest.pPayload->setEqual(paE->pPayload);
					
				}
			}
		}
		else
		{
			wrkingDest.pPayload =  NULL;
		}
		destElements.push_back(wrkingDest); // pointer ownership transfers to the list!!
		// stevev 13may13 - hCEnumList has a bad leak here. clear() nulls the pointers
		//		I'm making it destroy which deletes the pointers...we'll see if the wheels pop off
		// Risky change - needs extensive testing.
		//wrkingDest.clear();
		wrkingDest.destroy();
	}
	return *this;
};

/*********** Temporary - regress to previous version
template<class DESTTYPE, class PAYLDTYPE>
hCgenericConditional <DESTTYPE, PAYLDTYPE>
::hCgenericConditional(const hCgenericConditional<DESTTYPE, PAYLDTYPE>& src) : 
			hCobject(src.devHndl()), priExpression(src.priExpression),priExprType(aT_Unknown)
{
	int cnt = src.destElements.size();
	if ( cnt == 0)
	{
		cerrxx<< "genericCond.copy constructor was passed a source with no destination elements." << endl;
		return;
	}// else continue

	priExprType   = src.priExprType;
#ifdef _DEBUG
	if ( priExprType < aT_Unknown || priExprType > aT_Direct)
	{
		cerrxx<<"ERROR: generic conditional (copy constructor) was passed an invalid priExprType."<<endl;;
	}
#endif

	destElements.clear();
	destElements.reserve(cnt);
	hCexpressDest wrkingDest(devHndl());
	wrkingDest.clear();

	dest_List::iterator pIT;


	for ( pIT = (hCgenericConditional<DESTTYPE, PAYLDTYPE>::hCexpressDest *)src.destElements.begin();   
	pIT < src.destElements.end(); 
	++pIT)
	{// pIT is a ptr 2 hCexpressDest
		wrkingDest.destType       = pIT->destType;
		wrkingDest.destExpression = pIT->destExpression;// operator=

		if (pIT->pCondDest != NULL)
		{// pCondDest is either a conditional or a conditional list, they have to cast
			wrkingDest.pCondDest = new DESTTYPE((DESTTYPE*)(pIT->pCondDest)); //new DESTTYPE(devHndl());
			// copy constructor above...wrkingDest.pCondDest->setEqualTo(pIT->pCondDest); 
		}
		else
		{
			wrkingDest.pCondDest = NULL;
			if ( pIT->pPayload == NULL )
			{
				clog<<"generic conditional copy constructor without conditional dest nor payload."<<endl;
			}
		}

		if (pIT->pPayload != NULL )
		{			
			//wrkingDest.pPayload = (PAYLDTYPE*) hCpayload::newPayload((PAYLDTYPE*)pIT);		
			//wrkingDest.pPayload = new PAYLDTYPE(*((PAYLDTYPE*)pIT->pPayload));
			wrkingDest.pPayload = (PAYLDTYPE*)hCpayload::newPayload(devHndl(),
																	 pltUnknown, pIT->pPayload);
			if ( wrkingDest.pPayload == NULL )
			{
				cerrxx<"ERROR: hCgenericConditional copy constructor cannot make a payload"<<endl;
			}
			// else all is OK
		}
		else
		{
			wrkingDest.pPayload =  NULL;
		}
		destElements.push_back(wrkingDest); // pointer ownership transfers to the list!!
		wrkingDest.clear();
	}
}
********** end temporary regression ************/


template<class DESTTYPE, class PAYLDTYPE>
RETURNCODE hCgenericConditional <DESTTYPE, PAYLDTYPE>
::dumpSelf(int indent, char* typeName )
{
	RETURNCODE rc = SUCCESS;
#if 1 // Not Implemented yet
		#ifdef _DBGCOND
		LOGIT(COUT_LOG,"%s+Start Conditional\n",Space(indent));   indent += 3;
		#endif

		if (priExprType > 4  || priExprType < 0)
		{
			LOGIT(COUT_LOG,"%sPrimary expression type ILLEGAL: %s ("
				"0x%02x)\n",Space( indent ), axiomStrings[priExprType],priExprType);
		}
		else
		{
			LOGIT(COUT_LOG,"%sPrimary expression type: %s with %d Destinations\n",
							  Space( indent ), axiomStrings[priExprType],destElements.size());
		}
	if ( priExprType == aT_Unknown )
	{
		LOGIT(COUT_LOG,"UNDEFINED (and illegal)\n");
	}
	else if ( priExprType == aT_Direct )
	{// a single destination payload
		if ( destElements.size() >= 1 )
		{
			destElements[0].dumpSelf(indent+3);
			if ( destElements.size() > 1 )
			{
				LOGIT(COUT_LOG,"%s*E* Destination List size is illegal (%d)\n",Space(indent),
					destElements.size());
			}
		}
		else
		{
			LOGIT(COUT_LOG,"%sDirect conditional primary expression type "
													"with no destination!\n",Space(indent));
		}
	}
	else
	{//IF || SEL
		LOGIT(COUT_LOG,"\n");
			#ifdef _DBGCOND
			LOGIT(COUT_LOG,"%s+Conditional Expression Start\n",Space(indent));
		priExpression.dumpSelf(indent+3, typeName);
			LOGIT(COUT_LOG,"%s-End Conditional Expression\n",Space(indent));
			#else
		priExpression.dumpSelf(indent+3, typeName);
			#endif
	}
	// do the dests
		#ifdef _DBGCOND
		LOGIT(COUT_LOG,"%sConditional Destinations: %d\n",Space(indent),destElements.size());
		#endif
		int k = 0;

		if ( priExprType != aT_Direct )
		{
			typename dest_List::iterator destPos;
			for (destPos = destElements.begin(); destPos != destElements.end(); ++destPos)
			{
				hCexpressDest* pE = &(*destPos);
				LOGIT(COUT_LOG,"%s-Conditional Destination Element::>%d\n",Space(indent),k++);
				pE->dumpSelf(indent +3);
			}
		}// endif already did the destinations
		#ifdef _DBGCOND
		indent -= 3; LOGIT(COUT_LOG,"%s-End Conditional\n",Space(indent));
		#endif
#endif // not implemented
	return rc;
}


class baseCondDest	: public hCobject
{
public:
	baseCondDest(DevInfcHandle_t h) : hCobject(h){};

	// both conditionals & conditional Lists have to have the same entry for this method
	virtual void setEqualTo(void* pSrcCnd) PVFC( "baseCondDest" );   // a virtual base class
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	template class hCconditional	      a normal (non-list type) conditional
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
template<class PAYLOODTYPE>
class hCconditional : 
	public baseCondDest , public hCgenericConditional<hCconditional<PAYLOODTYPE>,PAYLOODTYPE>
{// no contents...parent has it all
public:
	hCconditional(DevInfcHandle_t h) : 
							baseCondDest(h), hCgenericConditional<hCconditional,PAYLOODTYPE>(h)
	{};
	virtual 
	~hCconditional(){ destroy(); };
/* temporary regress to previous version
	hCconditional(const hCconditional<PAYLOODTYPE>* pSrc)
		:hCgenericConditional<hCconditional,PAYLOODTYPE>
				(*((hCgenericConditional<hCconditional,PAYLOODTYPE>*) pSrc)), 
		 baseCondDest(((baseCondDest*)pSrc)->devHndl()) {};
* end temp regression */
	RETURNCODE destroy(void){ 
		return hCgenericConditional<hCconditional,PAYLOODTYPE>::destroy();};

	// xxthe dllapi has a special structure so we can have a unique copy method
	// xxassumes that the basepayload defined has an operator=(dllapiConditional.basePayload)
	// hCconditional& operator=(class aCconditional& paCond){};
	void setEqualTo(void* pSrcCnd){this->makeEqual((aCconditional*)pSrcCnd);};
	void setDuplicate(hCconditional<PAYLOODTYPE>& origCond, bool replicate,itemID_t parent= 0)
	{
		this->makeEqual(origCond, replicate, parent);};
	// add test-stevev 13jul07:
	//bool isConditional(void) { return (priExprType != aT_Direct); };
	bool isConditional(void) { if (this->priExprType != aT_Direct) return true;
                               else {  assert(this->destElements.size() == 1); return false; }  };
	RETURNCODE fetchPayLoad(PAYLOODTYPE *pPayLd)
    {
#ifndef _DDBCONDITIONAL_H
        return resolveCond(pPayLd);
#else
        return SUCCESS;
#endif
    };
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	template class hCconditionalList  it is a list of conditionally included lists
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
template<class PAYLOADTYPE>
class hCcondList : public baseCondDest	//  public hCobject (in baseCondDest)
{
public:
	typedef vector<PAYLOADTYPE> payloadList_t;
	//==> vector< hCgenericConditional< hCcondList<PAYLOADTYPE>,PAYLOADTYPE > > genericlist;
	// divided into two typedefs to try and make it understandable::)
	typedef hCgenericConditional< hCcondList<PAYLOADTYPE>,PAYLOADTYPE >  oneGeneric_t;
	typedef	vector<oneGeneric_t> condListOlists_t;

	condListOlists_t genericlist;

	//payloadList_t    retList;
	//PAYLOADTYPE        retList;

// temp regress to previous::	hCcondList(const hCcondList<PAYLOADTYPE>* src);
	hCcondList(DevInfcHandle_t h) : baseCondDest(h)/*, retList(h), genericlist(h)*/ {};

	virtual ~hCcondList()
	{
		/* stevev 27aug10 try to avoid deletion crash 
		if ( genericlist.size() )// stevev 25aug10 try to avoid deletion crash & memory leaks
			genericlist.~vector();
		****/
		genericlist.clear();
	};
	RETURNCODE destroy(void)
	{	RETURNCODE rc = 0;
		FOR_iT(typename condListOlists_t, genericlist)
		{// iT is a ptr to a oneGeneric_t
			rc |= iT->destroy();
		}
		genericlist.clear();
		return rc;
	};

	// the dllapi has a special structure so we can have a unique copy method (list of vector base classes)
	// assumes that the basepayload defined here has an operator=(dllapiCondList.basePayload)
	//hCcondList& operator=(class aCcondList&);
	//void setEqualTo(aCcondList<void> * pSrcCndLst); // set self from dllapi
	void setEqualTo(void* pSrcCnd); // must cast to        aCcondList<void> *

	// make self a duplicate
	void setDuplicate(hCcondList<PAYLOADTYPE>& origCondLst, bool replicate,itemID_t parent= 0);

	void clear(void);
	RETURNCODE dumpSelf(int indent = 0) 
	{ 
#ifndef XMTRDD // crashing compiler with heap-memory Zm error C1076
		int Y = 0;
		//FOR_iT(condListOlists_t, genericlist)
		for(typename condListOlists_t::iterator iT = (genericlist).begin();iT!=(genericlist).end();++iT)
		{// iT is a ptr to a oneGeneric_t
			LOGIT(COUT_LOG,"%sConditionalList element %d\n",Space(indent),Y++);
			iT->dumpSelf(indent+3);
		}
		//cerxx<< "Conditional Lists need a dump routine." << endl;return FAILURE;};
		return SUCCESS;
#else
		return FAILURE;
#endif
	};
	RETURNCODE appendNonCond(PAYLOADTYPE* pNewList);// append a non conditional list
	
	//PAYLOADTYPE* resolveCond(void);// use to pass in interface::X..CcommAPI* pCcommAPI);
	RETURNCODE resolveCond(PAYLOADTYPE* pRetList, bool isAtest = false);

	RETURNCODE aquirePayloadPtrList(vector<PAYLOADTYPE *>& retList, bool checkExpression = false);// all possible outcomes
	RETURNCODE aquireDependencyList(ddbItemList_t&  retItemIDs, bool getPayloads);// from expression
	
	bool isConditional(void);
};

/** temporary regress to previous version (w/o copy constructor)
template<class PAYLOADTYPE>	
hCcondList<PAYLOADTYPE>
::hCcondList(const hCcondList<PAYLOADTYPE>* src) : baseCondDest(src->devHndl())
{
	genericlist.clear();  // we'll build all new ones
	if (src == NULL) 
	{
		cerrxx<< "hCondList typedef constructor was called with no source listOlists." << endl;
		return;
	}
	int cnt = src->genericlist.size();
	if ( cnt == 0)
	{
		cerrxx<< "hCondList typedef constructor was called with an empty source listOlists." << endl;
		return;
	}
	//else continue
//		typedef	vector<oneGeneric_t> condListOlists_t;
//tmp	oneGeneric_t wrking1generic(devHndl());
	condListOlists_t::iterator pIT;

	for ( pIT =  (oneGeneric_t*)src->genericlist.begin();
		  pIT != src->genericlist.end(); pIT++)
	{// pIT is a pointer to a oneGeneric_t			
//tmp		wrking1generic = *pIT;
//tmp		genericlist.push_back(wrking1generic);
		genericlist.push_back(*pIT);
//		wrking1generic.clear();
	}
//	wrking1generic.destroy();
	return;
}
*** end temp regression ***/


template<class PAYLOADTYPE>	
RETURNCODE hCcondList<PAYLOADTYPE>
::aquirePayloadPtrList(vector<PAYLOADTYPE *>& retList, bool checkExpression)// all possible outcomes
{
	RETURNCODE rc = SUCCESS;
	FOR_iT(typename condListOlists_t, genericlist)
	{// iT is a ptr to a oneGeneric_t
		rc = iT->aquirePayloadPtrList(retList, checkExpression);
		if ( checkExpression && rc == COND_VAR_USE_INVALID)
		{
			break; // out of loop to return error
		}
	}
	return rc;
};

template<class PAYLOADTYPE>	
RETURNCODE hCcondList<PAYLOADTYPE>
::aquireDependencyList(ddbItemList_t& retItemIDs, bool getPayloads)// from expression
{
	RETURNCODE rc = SUCCESS;
	FOR_iT(typename condListOlists_t, genericlist)
	{// iT is a ptr to a oneGeneric_t
		rc |= iT->aquireDependencyList(retItemIDs, getPayloads);
	}
	LOGIF( LOGP_DEPENDENCY )
		(CLOG_LOG,"DEP:  Exit Conditional List's acquireDep++++++++++++++\n");
	return rc;
};


template<class PAYLOADTYPE>
//vector<PAYLOADTYPE>* hCcondList<PAYLOADTYPE>
RETURNCODE hCcondList<PAYLOADTYPE>
::resolveCond(PAYLOADTYPE* pRetList, bool isAtest)
{
	RETURNCODE rc = SUCCESS;

	//vector<PAYLOADTYPE>* pRetList = &retList;// = new:: a vector<PAYLOADTYPE>;
	//PAYLOADTYPE rTmpList(rRetList.devHndl());
	//PAYLOADTYPE* pTmpList = NULL;
	//PAYLOADTYPE TmpList(devHndl());
	int incomingsize = pRetList->size();

	//// TESTING /////
	//PAYLOADTYPE aPayloadClass;

// removed due to causing too many errors::	retList.clear();
	//24oct11 - stevev - moved out of loop and made it exact size:
	pRetList->reserve(pRetList->size() + genericlist.size());

	FOR_iT(typename condListOlists_t, genericlist)
	{// iT is a ptr to a oneGeneric_t that resolves to a list
		PAYLOADTYPE TmpList(devHndl());// stevev 27aug07 - attempt to plug a leak where 
									   //                   TmpList is filled in append().
// 00.01.09	speed improvements
// moved outside loop... see above		pRetList->reserve(10);	
// 00.01.09	end
		if (iT->priExprType == eT_Direct)
		{	
			// no reson to add to list since there is only one
			//pTmpList = /*(PAYLOADTYPE*)*/(iT->resolveCond(/*pCcommAPI*/));
			rc = iT->resolveCond(&TmpList, isAtest);
			//pRetList = (void*)iT->destElements[0].pPayload; 
		}
		else
		{
			// we have to resolve it and ADD it to the list
//			cerrxx<< "Device's resolveCondList is truely conditional." << endl;
			
			//pRetList.append( (vector<PAYLOADTYPE>*)iT->resolveCond(pCcommAPI) );
			//pTmpList = /*(PAYLOADTYPE*)*/(iT->resolveCond(/*pCcommAPI*/));
			rc = (iT->resolveCond(&TmpList, isAtest));
		}
		// payload is hCondList<PAYLOADTYPE>
		if ( rc == SUCCESS && TmpList.size() > 0 )
		{
			//pRetList->push_back(*pTmpList);
			// stevev 08nov10    pRetList->append(&TmpList);
			pRetList->appendUnique(&TmpList);
		}
		//else
		//{// didn't resolve
			// don't clear for now, just don't add...retList.clear();
		//}
		TmpList.destroy(); // 27aug07 - try to reduce memory leak
		TmpList.clear(); // get ready for the next round
//stevev - try to prevent deleion crash 27aug10...was::>		TmpList.~PAYLOADTYPE();
	}
	if ( incomingsize > (int)pRetList->size() )  	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		if ( genericlist.size() > 0 )
		{
			LOGIT(CLOG_LOG,"hCcondList:: resolveCond did NOT resolve the conditional."
																	"(probably elseless)\n");
			LOGIT(CLOG_LOG,"             from %d generic lists.\n",genericlist.size());
		// this is normal when there is no else clause or default clause
		}
		else
		{// this happens on an empty request transaction list
			// we must leave the entry data :: pRetList->clear();
		}
	}

	//return pRetList;// may only have one
	return rc;
};


// make self a duplicate
template<class PAYLOADTYPE>
void hCcondList <PAYLOADTYPE>
::setDuplicate(hCcondList<PAYLOADTYPE>& origCondLst, bool replicate, itemID_t parentID)
{
	genericlist.clear();  // we'll build all new ones
	
	int cnt = origCondLst.genericlist.size();
	if ( cnt == 0)
	{
		LOGIT(CERR_LOG,"hCondList.setDuplicate(Dup) was called with an empty source listOlists.\n");
		return;
	}
	//else continue

	for (typename hCcondList::condListOlists_t::iterator pIT = origCondLst.genericlist.begin();
		pIT < origCondLst.genericlist.end();  pIT++)
	{// pIT is a pointer to a hCcondList::oneGeneric_t

		oneGeneric_t wrking1generic(devHndl());// stevev 27aug07-put inside to try to fix mem leak
		wrking1generic.makeEqual(*pIT, replicate, parentID);
		genericlist.push_back(wrking1generic);
		wrking1generic.destroy();// added to alleviate memory leak by DD@Fluke, indications
		wrking1generic.clear();  // are that the pushback always makes a deep copy of all elements
	}
// stevev 27aug07-	wrking1generic.destroy();
	return;
}


// make self a duplicate
template<class PAYLOADTYPE>
RETURNCODE hCcondList <PAYLOADTYPE>
::appendNonCond(PAYLOADTYPE* pNewList)
{
	if ( isConditional() )
	{
		return COND_ISCOND_ERROR;
	}// else do it

	oneGeneric_t wrkingGeneric(devHndl());
	wrkingGeneric.setPayload(*pNewList);
	genericlist.push_back(wrkingGeneric);
	wrkingGeneric.clear();
	return SUCCESS;
}


template<class PAYLOADTYPE>
void hCcondList <PAYLOADTYPE>
::setEqualTo(void* pSrcCndLst)
{
	aCcondList/*<void>*/ * pSrc = (aCcondList/*<void>*/ * ) pSrcCndLst;

	genericlist.clear();  // we'll build all new ones
	if (pSrc == NULL) 
	{
		LOGIT(CERR_LOG,"hCondList.setEqualTo was called with no source listOlists.\n");
		return;
	}
	int cnt = pSrc->genericlist.size();
	if ( cnt == 0)
	{
		LOGIT(CERR_LOG,"hCondList.setEqualTo was called with an empty source listOlists.\n");
		return;
	}
	//else continue
//	genericlist.reserve(cnt);
	oneGeneric_t wrking1generic(devHndl());

	// for some reason this can't be done inside the for() expression
	//aCcondList<void>::listOconds_t::iterator 
	//	pIT = pSrcCndLst->genericlist.begin();


	//for (condListOlists_t::iterator myIT = genericlist.begin();		   
	//		myIT < genericlist.end() && pIT < pSrcCndLst->genericlist.end();
	//	 myIT++, pIT++)
	for (aCcondList/*<void>*/::listOconds_t::iterator pIT = pSrc->genericlist.begin();
		pIT < pSrc->genericlist.end(); pIT++)
	{// pIT is a pointer to a aCcondList::oneGeneric_t
		//was::> wrking1generic.makeEqual((aCconditional/*aCgenericConditional<void,void>*/ *)pIT);		
		// oneGeneric_t in ddbConditional.h looks like:  
		//                  typedef hCgenericConditional< hCcondList<PAYLOADTYPE>,PAYLOADTYPE >
		// oneGeneric_t in dllapi.h looks like        
		//              :  typedef aCgenericConditional (where aCgenericConditional is a class)
		// pIT looks like       :  std::vector<hCcondList<PAYLOADTYPE>::oneGeneric_t>::iterator
		wrking1generic.makeEqual((aCgenericConditional*)&(*pIT));
		genericlist.push_back(wrking1generic);
		wrking1generic.destroy();
		wrking1generic.clear();
	}
	return;
};


template<class PAYLOADTYPE>
void hCcondList <PAYLOADTYPE>
::clear(void) 
{ 	// clear is not supposed to delete pointers(destroy does that)
// 16sep09 stevev revert to leaky code (after a week of working on this)
	 genericlist.clear(); 
	 if ( genericlist.size() ) // stevev 26aug10 try to avoid deletion crash & memory leaks
		genericlist.~vector<oneGeneric_t>(); 
// 16sep09 stevev revert to leaky code	
//	oneGeneric_t* pGen;
//	FOR_iT(typename condListOlists_t, genericlist)
//	{// iT is a ptr to a oneGeneric_t
//		pGen = (oneGeneric_t*)&(*iT);
//		pGen->clear();// each generic conditional
//	}
//	genericlist.clear(); // now empty it
};

template<class PAYLOADTYPE>
bool hCcondList <PAYLOADTYPE>
::isConditional(void)
{
	bool iC = false;
	oneGeneric_t * pCond;

	//straight lists of conditional lists
	FOR_iT(typename condListOlists_t, genericlist)
	{// iT is a ptr to a oneGeneric_t that resolves to a list
		pCond = &(*iT);
		if (pCond->priExprType != eT_Direct || 
		    pCond->destElements.size() > 1  || 
			pCond->destElements[0].pPayload == NULL )
		{	
			iC = true;
			break; // out of for loop
		}
		// else keep looking- it should only have one
	}
	return iC;// false if all are false
};

// break up this monster file - at least for now
#include "ddbCondResolution.h"


#endif //_DDBCONDITIONAL_H

/*************************************************************************************************
 *
 *   $History: ddbConditional.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
