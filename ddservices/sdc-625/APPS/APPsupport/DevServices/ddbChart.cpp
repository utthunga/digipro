/*************************************************************************************************
 *
 * $Workfile: ddbChart.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbChart.h"
#include "ddbSource.h"
#include "logging.h"
#include "ddbMenuLayout.h"// stevev 01nov06 added

/* the global mask converter...you have to cast the return value*/
extern int  mask2value(ulong inMask); 
extern char grpItmAttrStrings[GRPITMATRSTRCOUNT] [GRPITMATRMAXLEN];

const char chartTypeStrings[CHARTTYPESTRCOUNT] [CHARTTYPEMAXLEN] = {CHARTTYPESTRINGS};

hCchart::hCchart(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), chartype(cT_Undefined),  pMemberAttr(NULL),
								 pChartHgt(NULL),pChartWid(NULL), pLength(NULL),pCycleTime(NULL) 
{// label , help & validity is set in the base class
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	aB = paItemBase->getaAttr(chartAttrMembers); 
	if (aB == NULL)
    {
        //CPMHACK: When Chart artifact implemented the below line shall be uncommented
//        LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCchart::hCchart<no member attribute supplied.>\n"
//		               "       trying to find: %d but only have:",(int)chartAttrMembers);

		if (paItemBase->attrLst.size() <= 0)
		{
            //CPMHACK: When Chart artifact implemented the below line shall be uncommented
//			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG,"%u, ", (*iT)->attr_mask);}
		}
        //CPMHACK: When Chart artifact implemented the below line shall be uncommented
//		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ 
		pMemberAttr = (hCattrGroupItems*)newHCattr(aB);
		if (pMemberAttr != NULL)
		{
			pMemberAttr->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMemberAttr);// keep it on the list too
		}
		else // is NULL
		{
			grpItmAttrType_t en = (grpItmAttrType_t)mask2value(aB->attr_mask); 
			LOGIT(CERR_LOG,"++ No New attribute ++ (%s)\n", grpItmAttrStrings[en]);
		}
	}
	
	/* stevev 8feb05 - fix the chart type */
	aB = paItemBase->getaAttr(chartAttrType); // required - stored as aCattrCondLong
	if (aB == NULL)
	{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCchart::hCchart<no type attribute supplied.>\n"
		               "  looking for Chart 'Type' attribute(%d) but only have:",(int)chartAttrType);

		if (paItemBase->attrLst.size() <= 0)
		{
            //CPMHACK: When Chart artifact implemented the below line shall be uncommented
//			LOGIT(CERR_LOG," an empty list.");
		}
		else
		{
			FOR_iT(AattributeList_t, paItemBase->attrLst)
			{ LOGIT(CERR_LOG,"%u, ", (*iT)->attr_mask);}
		}
        //CPMHACK: When Chart artifact implemented the below line shall be uncommented
//		LOGIT(CERR_LOG,"\n");
	} 
	else // generate a hart class from the abstract class
	{ // the type is not allowed to be conditional
		aCattrCondLong* paCr = (aCattrCondLong*)aB;
		if ( paCr->condLong.priExprType != eT_Direct                ||
			 paCr->condLong.destElements.size() == 0                ||
			 paCr->condLong.destElements[0].destType != eT_Direct   ||
			 paCr->condLong.destElements[0].pPayload->whatPayload() != pltddlLong)
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: chart's type attribute is invalid.\n");
		}
		else
		{
			aCddlLong* pAClng = (aCddlLong*)paCr->condLong.destElements[0].pPayload;
			unsigned long L   = (UINT32)pAClng->ddlLong;// very limited value set

			if ( L <= cT_Undefined  || L >= cT_UNKNOWN )
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: chart's type (%u) is out of range.\n",L);
				chartype = cT_Strip;// force it to be something
			}
			else
			{
				chartype = (chartType_t) L;
			}
		}
	}
	/* end fix chart type */

	// NOT required			
		
	aB = paItemBase->getaAttr(chartAttrHeight); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pChartHgt = (hCattrGrafixSize*)newHCattr(aB);
		if (pChartHgt != NULL)
		{
			pChartHgt->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pChartHgt);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Height attribute for Chart ++ (memory error?)\n");
		}
	}

	aB = paItemBase->getaAttr(chartAttrWidth); // uses aCattrCondLong	- as enum CgrafixSize
	if (aB != NULL)
	{
		pChartWid = (hCattrGrafixSize*)newHCattr(aB);
		if (pChartWid != NULL)
		{
			pChartWid->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pChartWid);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Width attribute for Chart ++ (memory error?)\n");
		}
	}


	aB = paItemBase->getaAttr(chartAttrLength); // uses aCattrCondExpr
	if (aB != NULL)
	{
		pLength = (hCattrExpr*)newHCattr(aB);
		if (pLength != NULL)
		{
			pLength->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pLength);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Length attribute for Chart ++ (memory error?)\n");
		}
	}	

	aB = paItemBase->getaAttr(chartAttrCycleTime); // uses aCattrCondExpr
	if (aB != NULL)
	{
		pCycleTime = (hCattrExpr*)newHCattr(aB);
		if (pCycleTime != NULL)
		{
			pCycleTime->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pCycleTime);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New CycleTime attribute for Chart ++ (memory error?)\n");
		}
	}	

}


/* from-typedef constructor */
hCchart::hCchart(hCchart*    pSrc,  itemIdentity_t newID)
	: hCitemBase(pSrc, newID), chartype(cT_Undefined),  pMemberAttr(NULL),
					pChartHgt(NULL),pChartWid(NULL), pLength(NULL),pCycleTime(NULL) 
{
	LOGIT(CERR_LOG,"**** Implement chart typedef constructor.***\n");
}


hCattrBase*   hCchart::newHCattr(aCattrBase* pACattr)
{
	if (pACattr->attr_mask == maskFromInt(chartAttrMembers)) 
	{
		return new hCattrGroupItems(devHndl(), (aCattrMemberList*)pACattr, this );		
	}
	else
	if (pACattr->attr_mask == maskFromInt(chartAttrHeight))	// same value as graphAttrHeight
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(chartAttrWidth))
	{
		return 
			new hCattrGrafixSize(devHndl(), (aCattrCondLong*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(chartAttrLength)) 
	{
		return 
			new hCattrExpr(devHndl(), chartAttrLength, (aCattrCondExpr*)pACattr, this );		
	}
	else
	if (pACattr->attr_mask == maskFromInt(chartAttrCycleTime)) 
	{
		return 
			new hCattrExpr(devHndl(), chartAttrCycleTime, (aCattrCondExpr*)pACattr, this );		
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(chartAttrLabel) | 
			 maskFromInt(chartAttrHelp)  | 
			 maskFromInt(chartAttrValidity)   | 
			 maskFromInt(chartAttrDebugInfo)))
	{//known attribute but shouldn't be here		
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: chart attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: chart attribute type is unknown.\n");
		return NULL; /* an error */  
	}
}

/*Vibhor 110204: Start of Code*/

RETURNCODE hCchart::getHeight(unsigned & height)
{

	RETURNCODE rc = SUCCESS;
	if(NULL != pChartHgt)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pChartHgt->procure();
		if(ptr2TempLong)
		{
			height = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
		{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;
	 
}/*End getHeight*/

RETURNCODE hCchart::getWidth(unsigned &  width)
{

	RETURNCODE rc = SUCCESS;
	if(NULL != pChartWid)
	{
		hCddlLong * ptr2TempLong = (hCddlLong *)pChartWid->procure();
		if(ptr2TempLong)
		{
			width = (UINT32)ptr2TempLong->getDdlLong();// very limited value set
		}
		else
	{
			return FAILURE; //error
		}
	}
	else 
	{
		return APP_ATTR_NOT_SUPPLIED; //error
	}

	return rc;

}/*End getWidth*/


chartType_t hCchart::getChartType()
{
	return chartype;

}/*End getChartType*/

RETURNCODE hCchart ::getLength(CValueVarient & cv)
{
	RETURNCODE rc = SUCCESS;

	if(NULL == pLength)
		return APP_ATTR_NOT_SUPPLIED; //should never happen
	else
	{
		cv = pLength->getExprVal();
	}

	return rc;

}/*End getLength */


RETURNCODE hCchart::getCycleTime(CValueVarient & cv)
{
	
	RETURNCODE rc = SUCCESS;

	if(NULL == pCycleTime)
		return APP_ATTR_NOT_SUPPLIED; //should never happen
	else
	{
		cv = pCycleTime->getExprVal();
	}

	return rc;

}/*End getCycleTime*/


RETURNCODE hCchart ::getSourceList(hCgroupItemList & memberList)
{
	RETURNCODE rc = SUCCESS;

	groupItemPtrList_t tmpList;

	rc = pMemberAttr->getList(tmpList);

	memberList.clear();
	if(SUCCESS == rc && tmpList.size() > 0)
	{
		groupItemPtrList_t :: iterator it;
		
		for(it = tmpList.begin(); it != tmpList.end();it++)
		{
			memberList.push_back(**it);	
		}

	}

	tmpList.clear();
	return rc;
	
}/*End getSourceList*/

/*Vibhor 110204: End of Code*/


// note that the device map is required to process ItemRef to ITEMID
RETURNCODE 
hCchart::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	delete *ppGID;
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	if ( pMemberAttr != NULL  && pGid != NULL )
	{
		rc = pMemberAttr->getItemByIndex(indexValue, *pGid);
		if ( rc != SUCCESS)
		{
			delete pGid;
			*ppGID = NULL;
			if ( ! suppress)
			{
				LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Chart getByIndex failed to getItemByIndex.'0x%04x'"
					"Item 0x%04x\n",indexValue,getID());
				clog << "ERROR: Chart getByIndex failed to getItemByIndex."<<endl;
			}
			return rc;
		}
	}
	if (rc != SUCCESS)
	{
		RAZE(pGid);		
		*ppGID = NULL;
	}
	return rc;
}


CValueVarient hCchart::getAttrValue(unsigned attrType, int which)
{	
	CValueVarient rV;

	chartAttrType_t cat = (chartAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( cat >= chartAttrUnknown )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Chart's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request


	switch (cat)
	{
	case chartAttrLabel:
	case chartAttrHelp:
	case chartAttrValidity:
		rV = hCitemBase::getAttrValue(cat);	
		break;
	case chartAttrHeight:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned v = 0;
			if (getHeight(v) == SUCCESS)
			{
				rV.vValue.iIntConst = v;
			}
			else
			{
				rV.vValue.iIntConst = MEDIUM_DISPSIZE;
			}
		}
		break;
	case chartAttrWidth:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;
			unsigned v = 0;
			if (getWidth(v) == SUCCESS)
			{
				rV.vValue.iIntConst = v;
			}
			else
			{
				rV.vValue.iIntConst = MEDIUM_DISPSIZE;
			}
		}
		break;
	case chartAttrType:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 1;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			rV.vValue.iIntConst = (int)getChartType();
		}
		break;
	case chartAttrLength:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			if (getLength(rV) != SUCCESS )
			{
				rV.vIsValid    = false;
			}
		}
		break;
	case chartAttrCycleTime:
		{
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;
			rV.vIsValid    = true;
			rV.vIsUnsigned = true;

			if (getCycleTime(rV) != SUCCESS )
			{
				rV.vIsValid    = false;
			}
		}
		break;
	case chartAttrMembers:
		{	
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: chart members must be accessed differently.\n");
		}
		break;
	case chartAttrDebugInfo:		// not accessable
		{
			//LOGIT(CERR_LOG,"WARN: debug information is not accessable.\n");
			LOGIT(CERR_LOG, ERR_NO_DEBUG_INFO );
		}
		break;
	default:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

// gets all possible references for a given member number  
// get the member value from all valid lists (used in the unitialized mode - no reads allowed)
//				ref& expr pointers must be destroyed by caller...What is pointed to must NOT
RETURNCODE hCchart::getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	
	if ( pMemberAttr != NULL )
	{
		rc = pMemberAttr->getAllItemsByIndex(indexValue, returnedItemRefs);
		if (rc != SUCCESS || returnedItemRefs.size() == 0 )
		{/* NOTE: this often occurs if PRIMARY is defined as 1 in imports but 0 in the DD */
		 /*       comes from the difference in macros.ddl and macros.h                    */
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: %s member list failed to find index %d.\n",
																	getName().c_str(), indexValue);
#ifdef _DEBUG
			LOGIT(CERR_LOG,"       possible mismatch of PRIMARY definition (DD to imports).\n");
#endif
		}
	}
	else
	{
		LOGIT(CERR_LOG,
				"ERROR: No Member attribute for a member based item (%s).\n",getName().c_str());
	}
	return rc;
}

/* stevev 09oct06 - overload to get chart specific notifications */

// called when an item in iDependOn has changed
int hCchart :: notification(hCmsgList& msgList,bool isValidity)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	
	hCitemBase::notification(msgList,isValidity);// do validity and label

	// re-evaluate all the others ( item list is only one left )
	if (! isValidity) 
	{// members
		if (pMemberAttr != NULL && pMemberAttr->reCalc())//reflist
		{// my list changed
			msgList.insertUnique(getID(), mt_Str, pMemberAttr->getGenericType());// tell the world
		}// else - no change;

	 // length 
		if (pLength != NULL && pLength->reCalc())// expression
		{// my length changed
			msgList.insertUnique(getID(), mt_Str, pLength->getGenericType());// tell the world
		}// else - no change;
		
	 // cycletime
		if (pCycleTime != NULL && pCycleTime->reCalc())// expression
		{// my handling changed
			msgList.insertUnique(getID(), mt_Str, pCycleTime->getGenericType());// tell the world
		}// else - no change;


		/* drill down into the contained sources */
		hCgroupItemList localList(devHndl());
		groupItemList_t::iterator gidIT;
		hCitemBase*      pSrc = NULL;
//
		if ( getSourceList(localList) == SUCCESS && localList.size() > 0 )
		{
			for ( gidIT = localList.begin(); gidIT != localList.end(); ++gidIT )
			{
				if ( gidIT->getItemPtr(pSrc) == SUCCESS && pSrc != NULL )
				{
#ifdef _DEBUG
					if ( pSrc->getIType() != iT_Source )
					{
						LOGIT(CERR_LOG, "Trying to Notification a Non-Source in Chart.\n");
					}
					else
#endif
					((hCsource*)pSrc)->notification(msgList,isValidity,this);
				}
			}
		}

	}// else - let validity lye

	return (msgList.size() - init);// number added
}
/** end stevev 09oct06  **/


/* stevev 12sep06 - dependency extentions */
void  hCchart :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes
	if ( pMemberAttr != NULL )
	{
		pMemberAttr->getDepends(iDependOn);
	}		

/* everything else is supposed to be non-conditional (constant) */
}
/** end stevev 12sep06  **/

/* stevev 27sep06 - more dependency extentions */
// get all possible items in this source
void  hCchart::fillContainList(ddbItemList_t&  iContain)
{// fill with items-I-contain 
	if ( pMemberAttr == NULL ) return;		

	//pMemberAttr-> getAllindexes(UIntList_t& allindexedIDsReturned);
	//	pMemberAttr - get all possible lists out of conditional to a groupItemListPtrList_t
	//				- for each hCgroupItemList - call getItemIDs(returnList)
	//		getItemIDs	- for each hCgroupItemDescriptor in thisList
	//					-    get the reference 
	//					-	 reference:resolveAllIDs(returnList)
	/////////////////////////////////////////////////////////////////////////////////////////////

	vector<unsigned int>  localitemIDList;
	vector<unsigned int>::iterator iItemID;
	hCitemBase* pItem   = NULL ;
	hCdependency* pDep  = NULL;

	if (getAllindexes(localitemIDList) == SUCCESS && localitemIDList.size() > 0)
	{
		for (iItemID = localitemIDList.begin(); iItemID != localitemIDList.end(); ++iItemID)
		{// for each source
			if ( devPtr()->getItemBySymNumber(*iItemID, &pItem) == SUCCESS && pItem != NULL)
			{
#ifdef _DEBUG
				if (pItem->getIType() != iT_Source)
				{
					LOGIT(CERR_LOG,"Chart %s (0x%04x) has a member that is a %s\n",
						pItem->getName().c_str(), pItem->getID(),itemStrings[pItem->getIType()]);
					continue;
				}
#endif
				pDep = pItem->getDepPtr(ib_Contain);
				if (pDep->iDependOn.size() > 0)
				{
					hCdependency::insertUnique(iContain, pDep->iDependOn);//appends list uniquely
					// others may need it, don't::>  pDep->iDependOn.clear();
				}
				// else nothing to add
			}
		}
	}// else move on
}
/** end stevev 27sep06 **/
/* stevev 01nov06 - layout */
RETURNCODE  hCchart::setSize( hv_point_t& returnedSize, ulong qual )
{ 
	unsigned X= DFLTWID_INDX,Y = DFLTHGT_INDX;
	
	if ( getHeight(Y) != SUCCESS)
	{	Y = DFLTHGT_INDX;
	}
	if ( getWidth(X)  != SUCCESS)
	{	X = DFLTWID_INDX;
	}

	// translate size codes into number of 1x1 frames
	returnedSize.yval = vert_Sizes[Y];
	returnedSize.xval = horizSizes[X];

	return SUCCESS;
}
/** end stevev 01nov06 **/