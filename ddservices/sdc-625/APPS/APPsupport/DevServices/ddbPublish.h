/*************************************************************************************************
 *
 * $Workfile: ddbPublish.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		describes the publish-subscribe interface classes
 *		12/09/4	sjv	created
 *
 * #include "ddbPublish.h"
 *
 */

#ifndef _PUBLISH_H
#define _PUBLISH_H

#include "pvfc.h"
#include "ddbMutex.h"
#include "GraphicInfc.h"

#include <map>

/* instead of ddbReferenceNexpression.h */
class   hCitemBase;
typedef vector<hCitemBase*> ddbItemList_t;


#define GRANULARITY	100 /* mS*/

#ifdef _DEBUG
class CdrawBase;
#endif

/* anyone wanting to recieve published data MUST inherit from here */
class hCpubsub
{
public:
	virtual
	RETURNCODE publish(dataStructList_t dsL) RPVFC( "hCpubsub",0 );
	/* note: one dataStruct_t per cycletime, and 
	         one dataStruct_t.dataPoint_t  per subscribed variable */

#ifdef _DEBUG
CdrawBase* pubsParent;
#endif
};

/*************************************************************************************************
	Device's pub/sub services
 ************************************************************************************************/
/*
		int& returnedHandle,			// in order to unsubscribe 
		ddbItemList_t subscribedList,	// list of variables we are subscribing to
		hCpubsub* pPublishTo,			// pointer to the callback function
		unsigned long cycleTime = 1000,	// requested update interval
		int cyclesPerPublish    = 1     // number of cycleTimes before call publish()
*/

typedef struct subscription_s
{		
	ddbItemList_t subscribedList;	// list of variables we are subscribing to
	hCpubsub*     pPublishto;		// pointer to the callback function
	unsigned long cycleTime;		// requested update interval
	int           cyclesPerPublish;

	dataStructList_t  cycleData;    // collection of data to be published
	
	int cycleCnt;	// inc'd per full cycletime, (each zero of subcycleCnt)
	int subcycleCnt;// inc'd per timer call, zero'd at subCyclesPerCycle

	int subCyclesPerCycle;// filled when calcTime()

	/*********************************************/
	subscription_s():pPublishto(NULL),cycleTime(0),cyclesPerPublish(0)
		{	cycleCnt=subcycleCnt=subCyclesPerCycle=0;	};

}
/*typedef*/ subscription_t;

typedef map<int,subscription_t>   subscriptionMap;
typedef subscriptionMap::iterator subscriptionItr;


class hCsubscriptions
{
	unsigned chngCntr;		// inc'd at each pub/un-pub call
	unsigned timeEventCntr;	// timer's call to me counter

	int currentTimerLen;// value of last calcTimerLen() return

	unsigned pubsChngCntr;		// stored at each publish

	map<int,subscription_t> subList;
	
	int  maxKey(void);
	void fillDataSet(subscription_t* pSS);
/*** the following are now done in publish!! ****/
	// use subscriptions & passed dynamic update requirment to calc a new time
	// granularity == 0, use GCD algorithm, else use ticks
	// assume caller will change to this time!!!!!!
	int calcTimerLen(int currentUpdate, int granularity = 0);// private - MUST use mutex control
	
	bool isChanged(unsigned int& myChngCnt)			// private - MUST be used under mutex control
		{if(chngCntr != myChngCnt){myChngCnt=chngCntr; return true;}else{return false;} } ;

	hCmutex                subscribeMutex;
	hCmutex::hCmutexCntrl* pPubMtx;
	hCmutex::hCmutexCntrl* pSubMtx;
	hCmutex::hCmutexCntrl* pDelMtx;

public:
	 hCsubscriptions();
	virtual ~hCsubscriptions();

	/* publish: send data when correct time, reset time event counter as required */
	/*          also calculates a new time if change cntr is different            */
	RETURNCODE publish(unsigned int updatePeriod);

	int  addSubscription(subscription_t sub);// returns handle
	void delSubscription(int handle);
};


#endif//_PUBLISH_H
