/*************************************************************************************************
 *
 * $Workfile: ddbTemplate.h $
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2016, FieldComm Group Inc.
 *************************************************************************************************
 *
 * Description:
 *		
 *		14oct16	sjv	created 
 *
 *		
 * #include "ddbTemplate.h"
 */
 
#pragma once

#ifndef _DDB_TEMPLATE_H
#define _DDB_TEMPLATE_H


#ifdef INC_DEBUG
#pragma message("In ddbTemplate.h") 
#endif

#include "DDBgeneral.h"
#include "foundation.h"

//#include "DDBdefs.h"
//#include "DDBitemBase.h"
//#include "ddbAttributes.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbTemplate.h") 
#endif

//	iT_Template,			//31
// TEMPLATE_LABEL_ID			use aCattrString	// 0 added 14oct16
// TEMPLATE_HELP_ID				use aCattrString	// 1 added 14oct16
// TEMPLATE_VALIDITY_ID			use aCattrCondLong	// 2 added 14oct16
// TEMPLATE_DEFAULT_VALUES_ID	use aCattrDefaultValuesList	// 3 added 14oct16
#if 0 // don't need this right now....
class hCdefaultValues;

class hCTemplate : public hCitemBase 
{
	hCdefaultValues		DfltVals;	    //required aCattrDefaultValuesList

protected:

public:
	hCTemplate(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCTemplate(hCTemplate*    pSrc,   itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ DfltVals.destroy(); return hCitemBase::destroy(); };
	virtual ~hCTemplate()  {DfltVals.destroy(); };
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType);

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(TEMPLATE_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(TEMPLATE_LABEL, l); };

	RETURNCODE Read(wstring &value){/* item cannot be read */ return FAILURE;};
	
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Template ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

	void fillCondDepends(ddbItemList_t&  iDependOn);// stevev 12sep06
	int  notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent);
	// I don't know what this is supposed to do...
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);
};
#endif

#endif	// _DDB_TEMPLATE_H
