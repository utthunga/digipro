/*************************************************************************************************
 *
 * $Workfile: ddbAttributes.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		8/9/2	sjv	created from attributes.h
 *		
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *	
 * #include "ddbAttributes.h"
 */


#ifndef _DDBATTRIBUTES_H
#define _DDBATTRIBUTES_H

#include "pvfc.h"
#include "ddbGeneral.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbPrimatives.h"

#include "ddbGlblSrvInfc.h"

extern wchar_t varClassStrings[CLASSSTRCOUNT][CLASSSTRMAXLEN];
typedef hCcondList<hCreferenceList>	 condReferenceList_t;

extern int mask2value(ulong inMask);

#ifdef USED // removed PAW 03/03/09
/********************* VARIABLE ATTRIBUTES BASE *******************/
#if _MSC_VER < 1300  // HOMZ - port to 2003, VS7
class hCitemBase;
#else
template <class hCitemBase>
#endif
#endif
//unsigned int  hCitemBase::getTypeVal(void); removed PAW 03/03/09
//class Cvariable;
// No longer used  class CVar;
class hCNumeric; // needed for COUNT attribute polymorphic overload

/******************************************************************
 * added by stevev 23jul10 
 * An encoded Integer has no way to tell if it is signed or unsigned.
 * When checking range(among other things) the sign can be important.
 * We take the position of casting the min max to the type of the
 * numeric being compared.  C rules will handle this except for
 * integer signedness.  Se we add a parameter to the value fetch.
 ***/
typedef enum IntType_e
{
	No_Type,		// 0
	Is_Signed,	// 1
	Un_Signed	// 2
} 
IntType_t;

/*******************************************************************
 *  the attribute needs to know the itembase so the itembase's basedevice
 *  is usable by the attribute and its children
 */
class hCattrBase : public hCobject
{
protected:
	hCitemBase*	pItem;
	// due to no unique typing: upper 2 bytes of GenericType is item type, lower 2 are attr type 
	// This is NOT the mask! as earlier comments suggested.
	// This is a varAttrType_t,or a eddispAttrType_t or a etc 
	//			(must be long to handle the different enum types)
	//         filled hardcoded from the instantiation call
	ulong		attr_Type;
	//ulong		attr_Type;   // the mask for this type of attribute - required for proper casting
							 // should be filled when child instantiates
public:
	//  copy construction
	hCattrBase(const hCattrBase& aB) : hCobject(aB.devHndl()) 
		{  attr_Type= aB.attr_Type; pItem = aB.pItem; };

	// raw construction
	hCattrBase(DevInfcHandle_t h, hCitemBase* pIB, ulong attrType = 0) :hCobject(h)
		{  attr_Type = attrType; pItem = pIB;};
	virtual ~hCattrBase()
	{
		/// HOMZ - pItem does not have to be deleted, this is handled elsewhere.
	};
	virtual RETURNCODE  destroy(void)  RPVFC( "hCattrBase_1",0 );//force all to implement

	virtual hCattrBase* new_Copy(itemIdentity_t newID) RPVFC( "hCattrBase_2",NULL );

	void    setItemPtr(hCitemBase* p) { pItem = p; 
							if (p==NULL){LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:attributes' item set to NULL\n");}};

	// force all children to return a resolved conditional
	// cannot be virtual because the compiler cannot resolve it when a base class is accessed
	virtual void* procure(void)		
	{	LOGIF(LOGP_NOT_TOK)(CLOG_LOG,"ERROR: hCattrBase virtual 'procure' function has been called.\n");
		return NULL; /* an error*/  };

	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) // to cout
			{	LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCattrBase access to base class virtual dumpSelf.\n"); 
				//COUTSPACE 
					LOGIT(COUT_LOG,"\t hCattrBase's dumpSelf() has not been overridden for mask("
					"0x%x)\n", attr_Type );
	            return VIRTUALCALLERR; };

	ulong static makeGenericType(itemType_t it, unsigned short at)
															{ return ((it<<16) | (at&0xffff));};
	// accessors
	/* aquires the item type AND the attribute type 
	                                         <attribute type enuim is dependent on item type> */
	ulong      getGenericType(void) { 
		if(pItem)
		{       return(makeGenericType(pItem->getIType(), (unsigned short)attr_Type)); 	}   // warning C4244: '=' : conversion from 'ulong' to 'unsigned short' <HOMZ: added cast>
		else{	return attr_Type;   		}
	};
	ulong      getGenericType(itemType_t it){return(makeGenericType(it,(unsigned short)attr_Type));};
};

#define GETDESCNOTHERE( a ) LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR:getDescription NOT IMPLEMENTED in %s\n", a );\
							return FAILURE

typedef vector<hCattrBase>  hAttrList_t;
#define ATTR_PARTOFGENERIC(a)  (a & 0xFFFF)
#define ITEM_PARTOFGENERIC(a)  ((a >> 16) & 0xFFFF)




/******************** GRAPHICS SUPPORT  **************************/

// this is a system defined enumeration
// TODO: get list of valid values to allow changing
class hCattrGrafixSize : public hCattrBase
{   // this is an enum of CgrafixSize type containing a grafixSize_t
	hCconditional<hCddlLong> condSize;// holds the value(s) 

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCddlLong  Resolved;

public:
	// NOTE: both graphs and charts have the same attribute enum value (3)
	// default and parent ID constructor
	hCattrGrafixSize(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,chartAttrHeight), condSize(h),Resolved(h) {};
	// copy constructor
	hCattrGrafixSize(const hCattrGrafixSize& sae)
		:hCattrBase(sae), condSize(sae.devHndl()),Resolved(sae.devHndl())
		{   condSize = sae.condSize; }
	// abstract constructor (from abstract class)
	hCattrGrafixSize(DevInfcHandle_t h, const aCattrCondLong*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,chartAttrHeight), condSize(h),Resolved(h) 
		{ condSize.setEqualTo((void*) &(aCa->condLong));};
	virtual hCattrBase* new_Copy(itemIdentity_t newID);
	virtual RETURNCODE destroy(void){ return ( Resolved.destroy() | condSize.destroy());};

	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Height:                 ");
		return condSize.dumpSelf(indent);
	};

	virtual void* procure(void){ 
	//  modified to not need API since the lower objects know how to read and write
		//CcommAPI* pCA = NULL;	
		//if ( (pCA = getDevAPI()) != NULL)
		Resolved.clear();
		if ( condSize.resolveCond(&Resolved) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};

	virtual grafixSize_t get_GsSize(void)
	{	if ( procure() ){return ((grafixSize_t)Resolved.getDdlLong() );}
										else      {return (gS_Undefined);} };
	virtual int          getintSize(void){if (procure() ){return ((int)Resolved.getDdlLong());}
										else      {return ( 0 );} };

	virtual RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCddlLong tLong(devHndl());
		tLong.setDdlLong((unsigned long)gS_Medium);
		condSize.clear();
		return condSize.setPayload(tLong);
	};

	virtual RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrSize");};
};


/******************** LABEL **************************/
class hCattrLabel : public hCattrBase
{
	hCconditional<hCddlString> condLabel;// holds the value(s) 
protected:
	hCddlString Resolved;// temporary holder of resolved value

public:
	// default and up-Pointer constructor
	hCattrLabel(DevInfcHandle_t h, hCitemBase* pIB = NULL) : hCattrBase(h,pIB,varAttrLabel), 
				Resolved(h), condLabel(h)  
		{	if (pIB == NULL) 
			{  LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Label is instantiating without an item.\n");  }
		};
	// copy constructor
	hCattrLabel(const hCattrLabel& sae) : hCattrBase(sae), condLabel(sae.devHndl()),
				Resolved(sae.devHndl())
	{ 
		attr_Type = sae.attr_Type;
		condLabel = sae.condLabel;
	};
	// abstract constructor (from abstract class)
	hCattrLabel(DevInfcHandle_t h, const aCattrString* aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrLabel), Resolved(h), condLabel(h)
		//{ condLabel = aCa.condLabel;};
	{
		condLabel.makeEqual((aCconditional*) &(aCa->condString));
	};
	RETURNCODE destroy(void){ Resolved.destroy();return condLabel.destroy();};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrLabel()
	{
		Resolved.destroy();
		condLabel.destroy();
	}

	// returns the payload as a void* or NULL
	void* procure(void){ 
	// modified to not need API since the lower objects know how to read and write
		//CcommAPI* pCA = NULL;	
		//if ( (pCA = getDevAPI()) != NULL)
		Resolved.clear();
		if ( condLabel.resolveCond(&Resolved) != SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	LOGIT(COUT_LOG,"%sLabel: ", Space(indent));
		return condLabel.dumpSelf(indent,typeName);
	};

	RETURNCODE getString(wstring& retStr,unsigned int iID = 0)
	{ 
		RETURNCODE rc = FAILURE;
		if ( pItem != NULL )
		{
// debugging 
//if ( iID >= 0x4000 || pItem->getID() >= 0x4000 )
//{
//	clog << " DEBUGGINH: " << hex << iID << dec << endl;
//}
			Resolved.clear();
			rc = condLabel.resolveCond(&Resolved);
			if (rc == SUCCESS)
			{
				retStr = Resolved.procureVal();//getDevAPI());
				if (retStr.length() > 0 )
				{
					rc = SUCCESS;
				}
				// else leave it failure
			}
			else
			{	// note that a conditional without a default||else stmt will exit here
				retStr = L"No String Defined";
				rc = SUCCESS;
			}
		}
		return rc;
	};	
	bool  reCalc(void);// stevev 18nov05 - returns didChange
	
	RETURNCODE getDepends(ddbItemList_t& retList)
				{ return (condLabel.aquireDependencyList(retList,false));};

};

/******************** CLASS **************************/
// from DDLDEFS.H-------------------------------------
//#define DIAGNOSTIC_CLASS			0x000001
//#define DYNAMIC_CLASS				0x000002
//#define SERVICE_CLASS				0x000004
//#define CORRECTION_CLASS			0x000008
//#define COMPUTATION_CLASS			0x000010
//#define INPUT_BLOCK_CLASS			0x000020
//#define ANALOG_OUTPUT_CLASS		0x000040
//
//#define HART_CLASS				0x000080
//
//#define LOCAL_DISPLAY_CLASS		0x000100
//#define FREQUENCY_CLASS			0x000200
//#define DISCRETE_CLASS			0x000400
//#define DEVICE_CLASS				0x000800
//#define LOCAL_CLASS				0x001000
//#define INPUT_CLASS				0x002000
//
/*
 * The UNASSIGNED_CLASS is not a valid class, but is used as a default
 * for a HART variable or method in which the class was omitted.  This
 * is done to prevent an error condition from being set in the tokenizer
 * or DDS.  This will probably be removed in the long term.
 */
//#define UNASSIGNED_CLASS			0x000000
//
//#define FACTORY_CLASS				0x100000
/*      +++++++++++++++++++++++++++++++++++++++++++++++++++++++     */
class hCattrClass : public hCattrBase
{	      
	hCconditional<hCbitString> condClass;// holds the value(s) 

protected:
	wstring returnString;
	hCbitString Resolved; // temporary pointer to resolved conditional

	// unused/unimplemented RETURNCODE fetch(void);

public:
	// default and parent ID constructor
	hCattrClass(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrClass), condClass(h),Resolved(h) {};
	// copy constructor
	hCattrClass(const hCattrClass& sae)
		:hCattrBase(sae), condClass(sae.devHndl()), Resolved(sae.devHndl()) 	
	{ condClass = sae.condClass;}
	// abstract constructor (from abstract class)
	hCattrClass(DevInfcHandle_t h, const aCattrBitstring*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrClass), condClass(h),Resolved(h)  
		{ condClass.setEqualTo((void*) &(aCa->condBitStr));};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrClass() 
	{ 
		destroy(); 
	};
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condClass.destroy());};



	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
				{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Class:                    ");
					return condClass.dumpSelf(indent, typeName);};

	void* procure(void)
	{ 
		Resolved.clear();
		if ( SUCCESS == condClass.resolveCond(&Resolved) )
		{	return &Resolved;	}
		else
		{	return NULL;}
	};


	RETURNCODE getDescription(int oSet)/*{return condClass.getDescription(oSet);};*/{GETDESCNOTHERE("hCattrClass");};
	RETURNCODE getBstring(UINT64& classBitString)
	{
		hCbitString BSClass = NULL;

		if ( condClass.resolveCond(&BSClass) == SUCCESS )
		{
			classBitString = BSClass.getBitStr();
			return SUCCESS;
		}
		else
		{
			classBitString = 0LL;
			
			// 22aug08 - OUTPUT(CERR_LOG, "ERROR: Class couldn't aquire the conditional bitstring.\n");
			LOGIF(LOGP_NOT_TOK)(CERR_LOG, "ERROR: Class couldn't aquire the conditional bitstring.\n");
			return FAILURE;
		}
	};
	// returns a comma-delimited string of bit meanings
	wstring& translateBstring(UINT64 bitStrValue)// TODO: add language option
	{
		returnString.erase();
		for(int i = 1; i < CLASSSTRCOUNT; i++) /* i == 0 is No Class */
		{
			if ( (bitStrValue>>(i-1)) & 0x01 )
			{
				if ( ! returnString.empty() )
				{
					returnString += L", ";
				}
				returnString += varClassStrings[i];
			}
		}
		if ( returnString.length() == 0 )
		{
			returnString = varClassStrings[0];
		}
		return returnString;
	};
};

/******************** HANDLING ***********************/

//#define READ_HANDLING 				0x01
//#define WRITE_HANDLING 				0x02

class hCattrHndlg : public hCattrBase
{      
	hCconditional<hCbitString> condHndlng;// holds the value(s) 

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCbitString Resolved;

public:
	// default and parent ID constructor
	hCattrHndlg(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrHandling), condHndlng(h),Resolved(h) {};
	// copy constructor
	hCattrHndlg(const hCattrHndlg& sae)
		:hCattrBase(sae), condHndlng(sae.devHndl()),Resolved(sae.devHndl())
		{   condHndlng = sae.condHndlng; }
	// abstract constructor (from abstract class)
	hCattrHndlg(DevInfcHandle_t h,const aCattrBitstring* aCa, ulong attrTyp = varAttrHandling,
				hCitemBase* pIB = NULL)	:hCattrBase(h,pIB,attrTyp), condHndlng(h),Resolved(h) 
	{	condHndlng.setEqualTo((void*) &(aCa->condBitStr));	};
	RETURNCODE destroy(void){ 
		RETURNCODE r = Resolved.destroy();
		r |= condHndlng.destroy();
		return r;                     };
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrHndlg()
	{
		Resolved.destroy() ;
		condHndlng.destroy() ;
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Handling:                 ");
		return condHndlng.dumpSelf(indent);
	};

	bool isConditional(void){ return (condHndlng.isConditional());};

	void* procure(bool isAtest = false){ 
	//  modified to not need API since the lower objects know how to read and write
		//CcommAPI* pCA = NULL;	
		//if ( (pCA = getDevAPI()) != NULL)
		Resolved.clear();
		if ( condHndlng.resolveCond(&Resolved,isAtest) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};


	RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCbitString tBstr(devHndl());
		tBstr.setBitStr(READ_HANDLING | WRITE_HANDLING);
		condHndlng.clear();
		return condHndlng.setPayload(tBstr);
	};

//	RETURNCODE fetch(void);
//	RETURNCODE insertSelf(int iSet = 0)INSERTATTRCOND(condHndlng);
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrHndlg");};
//		{return condHndlng.getDescription(oSet);};// set is expected to be ATTR2COND but we make sure
	RETURNCODE getBstring(unsigned long long & handlingBitString)
	{
		hCbitString Hlg(devHndl());

		if (condHndlng.fetchPayLoad(&Hlg) == SUCCESS)
		{
			handlingBitString = Hlg.getBitStr();
			return SUCCESS;
		}
		else
		{
			handlingBitString = 0L;
			LOGIT(CERR_LOG,"ERROR: Handling couldn't aquire the conditional bitstring.\n");
			return FAILURE;
		}

	};
	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condHndlng.aquireDependencyList(retList,false));};

	bool  reCalc(void);// stevev 18nov05 - returns didChange


};





//const CddlString (unsigned long Tag, unsigned int ddItem, unsigned long enumVal,unsigned int ddKey=0)  dfltUnit;
extern hCddlString dfltUnit;//see attrVars.cpp


//extern int cntUnitParse  ;
//extern int cntUnitInsert ;
//extern int cntUnitDefault;
//extern int cntUnitUnknown;
//extern int cntUnitPushed;


/******************** (CONSTANT) UNIT **************************/
class hCattrUnit : public hCattrBase
{ 
	hCconditional<hCddlString/*,aCattrUnit*/> condUnit;// holds the value(s) 

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCddlString Resolved;

public:
	// no default (we MUST have a handle)
	hCattrUnit(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrConstUnit), condUnit(h),Resolved(h) {};
	// copy constructor
	hCattrUnit(const hCattrUnit& sae)
		:hCattrBase(sae), condUnit(sae.devHndl()),Resolved(sae.devHndl())
		{  condUnit   = sae.condUnit;	};
	// abstract constructor (from abstract class)
	hCattrUnit(DevInfcHandle_t h, const aCattrString*  aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrConstUnit), condUnit(h),Resolved(h)
		{ condUnit.setEqualTo( (void*) &(aCa->condString));};
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condUnit.destroy());};
	hCattrBase* new_Copy(itemIdentity_t newID);







	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"          Unit:                 ");
		return condUnit.dumpSelf(indent);};

	wstring getConstUnitStr(void)
	{
		Resolved.clear();
		if ( condUnit.resolveCond(&Resolved) == SUCCESS )
		{ 
			return (Resolved.procureVal());
		}
		else
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: hCddlString: would not resolve conditional in "
				"getConstUnitStr.\n");
			wstring retString;
			return retString;// the empty string
		}
	};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrUnit");};
//		{return condUnit.getDescription(ATTR2COND);};// set is expected to be ATTR2COND but we make sure
	RETURNCODE setDefaultVals(RETURNCODE rrc){
//cntUnitDefault++;
//		return condUnit.setPayload(dfltUnit);};
		return FAILURE;};
};


/******************** VALIDITY **************************/
class hCattrValid : public hCattrBase
{
	hCconditional<hCddlLong> condValid;// holds the value(s) 

public:	
	typedef enum validity_e
	{
		v_INVALID = 0,
		v_VALID   = 0xffffffff
		/*EVERYTHING ELSE IS UNDEFINED*/
	}validity_t;
#define v_UNKNOWN ((validity_t)0x0f0f0f0f)

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCddlLong  Resolved;

	validity_t validity;
	validity_t externValidity;// Used for empty lists, and (18may09) for single-byte indexes 
							  //      with the value 250 (Do-Not-Use)

public:
	// default and up-Pointer constructor
	hCattrValid(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrValidity),  condValid(h),Resolved(h),validity(v_UNKNOWN),
		 externValidity(v_UNKNOWN)		{};
	// copy constructor
	hCattrValid(const hCattrValid& sae)// base handles hCobject
		:hCattrBase(sae),  condValid(sae.devHndl()) ,Resolved(sae.devHndl())
		{condValid = sae.condValid;validity = sae.validity;externValidity = sae.externValidity;}
	// abstract constructor (from abstract class)
	hCattrValid(DevInfcHandle_t h, const aCattrCondLong* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrValidity),  condValid(h),Resolved(h),validity(v_UNKNOWN),
		 externValidity(v_UNKNOWN)		
		{condValid.makeEqual((aCconditional*) &(aCa->condLong));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condValid.destroy();};

	virtual ~hCattrValid()
	{
		condValid.destroy();
	}

	void* procure(bool isAtest = false);/*implementation moved to ddbVar.cpp*/
	bool  reCalc(void);// stevev 18nov05 - returns didChange
	bool  is_UNKNOWN();
	void  setValue( validity_t vt ) {externValidity = vt;};// stevev 07jan09 for empty lists
	void  setUNKNOWN() {validity = v_UNKNOWN;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"%sValidity: ",Space(indent));
		return condValid.dumpSelf(indent,typeName);
	};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrValid");};
//		{return condValid.getDescription(ATTR2COND);};// set is expected to be ATTR2COND but we make sure
	/* stevev 19jan05 - dependency */
	RETURNCODE getDepends(ddbItemList_t& retList)
		{ return (condValid.aquireDependencyList(retList,false));};
	bool isConditional(void) { return (condValid.isConditional()); };
};



/************************ HELP **************************/
//VMKP Modified for Help conditional handling
class hCattrHelp : public hCattrBase	// was::CattrVarHelp, made it generic here
{
	hCconditional<hCddlString> condHelp;// holds the value(s) 
protected:
	hCddlString Resolved;

public:	
	// default and up-Pointer constructor
	hCattrHelp(DevInfcHandle_t h, hCitemBase* pIB = NULL):
				hCattrBase(h,pIB,varAttrHelp),condHelp(h),Resolved(h)  
		{
			if (pIB == NULL) 
			{  LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Help is instantiating without an item.\n");  }		
		};
	// copy constructor
	hCattrHelp(const hCattrHelp& sae):
				hCattrBase(sae), condHelp(sae.devHndl()),Resolved(sae.devHndl())
		{ attr_Type = sae.attr_Type;  condHelp = sae.condHelp;};
		
	// abstract constructor (from abstract class)
	hCattrHelp(DevInfcHandle_t h, const aCattrString * aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrHelp), condHelp(h),Resolved(h)
		{condHelp.makeEqual((aCconditional*) &(aCa->condString));};
	RETURNCODE destroy(void){ Resolved.destroy();return condHelp.destroy();};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrHelp()
	{
		Resolved.destroy();
		condHelp.destroy();
	}

	// returns ptr2ddlstring
	void* procure(void)
	{	Resolved.clear();
		if ( condHelp.resolveCond(&Resolved) == SUCCESS )
		{	return  &Resolved;	}
		else
		{	return NULL;		}  
	};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarHelp");};
	

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	LOGIT(COUT_LOG,"%sHelp: ",Space(indent));
		return condHelp.dumpSelf(indent,typeName);
	};
	RETURNCODE getString(wstring& retStr,unsigned int iID = 0){ 
		RETURNCODE rc = FAILURE;
		if ( pItem != NULL )
		{
// debugging 
//if ( iID >= 0x4000 || pItem->getID() >= 0x4000 )
//{
//	clog << " DEBUGGINH: " << hex << iID << dec << endl;
//}
			Resolved.clear();
			rc = condHelp.resolveCond(&Resolved);
			if (rc == SUCCESS)
			{
				retStr = Resolved.procureVal();//getDevAPI());
				if (retStr.length() > 0 )
				{
					rc = SUCCESS;
				}
				// else leave it failure
				else
				{
					retStr = L"";
					rc = APP_RESOLUTION_ERROR;
				}
			}
			else
			{	// note that a conditional without a default||else stmt will exit here
				retStr = L"No String Defined";
				rc = SUCCESS;
			}
		}
		return rc;
	};
};

/***********&&&&&&&&&&&&&&&&&&&&&&&&&**********************/
//class hCminMaxList : public hCattrBase
//{	
//	hCconditional<hCddlLong> condmmlist;
//public:
//	hCminMaxList(DevInfcHandle_t h, hCitemBase* pIB = NULL)
//		:hCattrBase(h,pIB,varAttrMinValue),condmmlist(h){};
//	RETURNCODE destroy(void){ return condmmlist.destroy();};
//	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrminMaxList");};
//
//};
/***********&&&&&&&&&&&&&&&&&&&&&&&&&**********************/
#if 0 // these are gone //////////////////////////////////////////////////////////////////////
class hCattrVarRdTime : public hCattrBase
{	
	hCconditional<hCexprPayload> condRdTimeout;
public:
	hCattrVarRdTime(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrRdTimeout),condRdTimeout(h)  {};
	hCattrVarRdTime(const hCattrVarRdTime& sae)
		:hCattrBase(sae),condRdTimeout(sae.devHndl())  
		{condRdTimeout = sae.condRdTimeout;};
	hCattrVarRdTime(DevInfcHandle_t h, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrRdTimeout),  condRdTimeout(h)
		{condRdTimeout.makeEqual((aCconditional*) &(aCa->condExpr));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condRdTimeout.destroy();};

	virtual ~hCattrVarRdTime()
	{
		condRdTimeout.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarRdTime");};
};
class hCattrVarWrTime : public hCattrBase
{	
	hCconditional<hCexprPayload> condWrTimeout;
public:
	hCattrVarWrTime(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrWrTimeout),  condWrTimeout(h)  {};
	hCattrVarWrTime(const hCattrVarWrTime& sae)
		:hCattrBase(sae),condWrTimeout(sae.devHndl())  
		{condWrTimeout = sae.condWrTimeout;};
	hCattrVarWrTime(DevInfcHandle_t h, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrWrTimeout),  condWrTimeout(h)
		{condWrTimeout.makeEqual((aCconditional*) &(aCa->condExpr));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condWrTimeout.destroy();};

	virtual ~hCattrVarWrTime()
	{
		condWrTimeout.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarWrTime");};
};
#endif ///////////////////////////////////////////////////////////////////////////////////////

class   hCattrVarWidth :  public hCattrBase
{   // this is an enum of CgrafixSize type containing a grafixSize_t
	hCconditional<hCddlLong> condSize;// holds the value(s) 

protected:
	// unused/unimplemented  RETURNCODE fetch(void);
	hCddlLong  Resolved;

public:
	hCattrVarWidth(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrWidth), condSize(h),Resolved(h) {};
	
	hCattrVarWidth(const hCattrVarWidth& sae)
		:hCattrBase(sae), condSize(sae.devHndl()),Resolved(sae.devHndl())
		{   condSize = sae.condSize; attr_Type = varAttrWidth;   };
	
	hCattrVarWidth(DevInfcHandle_t h, const aCattrCondLong*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h, pIB, varAttrWidth), condSize(h),Resolved(h) 
		{ condSize.setEqualTo((void*) &(aCa->condLong));};

	 hCattrBase* new_Copy(itemIdentity_t newID);
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condSize.destroy());};

	 RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Width:                 ");
		return condSize.dumpSelf(indent);
	};

	 void* procure(void){ 
		Resolved.clear();
		if ( condSize.resolveCond(&Resolved) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};

	 grafixSize_t get_GsSize(void)
	{	if ( procure() ){return ((grafixSize_t)Resolved.getDdlLong() );}
										else      {return (gS_Undefined);} };
	 int          getintSize(void){if (procure() ){return ((int)Resolved.getDdlLong());}
										else      {return ( 0 );} };

	 RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCddlLong tLong(devHndl());
		tLong.setDdlLong((unsigned long)gS_Medium);
		condSize.clear();
		return condSize.setPayload(tLong);
	};

	 RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarWidth");};
};


class 	hCattrVarHeight :  public hCattrBase
{   // this is an enum of CgrafixSize type containing a grafixSize_t
	hCconditional<hCddlLong> condSize;// holds the value(s) 

protected:
	// unused/unimplemented  RETURNCODE fetch(void);
	hCddlLong  Resolved;

public:
	hCattrVarHeight(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrHeight), condSize(h),Resolved(h) {};
	
	hCattrVarHeight(const hCattrVarHeight& sae)
		:hCattrBase(sae), condSize(sae.devHndl()),Resolved(sae.devHndl())
		{   condSize = sae.condSize; attr_Type = varAttrHeight;   };
	
	hCattrVarHeight(DevInfcHandle_t h, const aCattrCondLong*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrHeight), condSize(h),Resolved(h) 
		{ condSize.setEqualTo((void*) &(aCa->condLong));};

	hCattrBase* new_Copy(itemIdentity_t newID);
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condSize.destroy());};

	 RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Height:                 ");
		return condSize.dumpSelf(indent);
	};

	 void* procure(void){ 
		Resolved.clear();
		if ( condSize.resolveCond(&Resolved) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};

	 grafixSize_t get_GsSize(void)
	{	if ( procure() ){return ((grafixSize_t)Resolved.getDdlLong() );}
										else      {return (gS_Undefined);} };
	 int          getintSize(void){if (procure() ){return ((int)Resolved.getDdlLong());}
										else      {return ( 0 );} };

	 RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCddlLong tLong(devHndl());
		tLong.setDdlLong((unsigned long)gS_Medium);
		condSize.clear();
		return condSize.setPayload(tLong);
	};

	 RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarHeight");};
};

class actionList
{
public:	// stevev 26jul16 - made it all public to allow access to the list
	condReferenceList_t condPrePostActList;
	actionList(DevInfcHandle_t h):condPrePostActList(h){};
};


class hCattrVarPreRdAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPreRdAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreReadAct), actionList(h) {};
	hCattrVarPreRdAct(const hCattrVarPreRdAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPreRdAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreReadAct), actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrVarPreRdAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPreRdAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarPostRdAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPostRdAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostReadAct),actionList(h) {};
	hCattrVarPostRdAct(const hCattrVarPostRdAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPostRdAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostReadAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrVarPostRdAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPostRdAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarPreWrAct :public actionList,  public hCattrBase
{	
public:
	hCattrVarPreWrAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreWriteAct),actionList(h) {};
	hCattrVarPreWrAct(const hCattrVarPreWrAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPreWrAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreWriteAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrVarPreWrAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPreWrAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarPostWrAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPostWrAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostWriteAct),actionList(h) {};
	hCattrVarPostWrAct(const hCattrVarPostWrAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPostWrAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostWriteAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrVarPostWrAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPostWrAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarPreEditAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPreEditAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreEditAct),actionList(h) {};
	hCattrVarPreEditAct(const hCattrVarPreEditAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPreEditAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPreEditAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrVarPreEditAct()
	{
		condPrePostActList.destroy();	
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPreEditAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarPostEditAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPostEditAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostEditAct),actionList(h) {};
	hCattrVarPostEditAct(const hCattrVarPostEditAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPostEditAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostEditAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarPostEditAct");};

	virtual ~hCattrVarPostEditAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};
class hCattrVarRefreshAct : public actionList, public hCattrBase
{	
public:
	hCattrVarRefreshAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrRefreshAct),actionList(h) {};
	hCattrVarRefreshAct(const hCattrVarRefreshAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarRefreshAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrRefreshAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrVarRefreshAct`");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};


class hCattrVarPostRequestAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPostRequestAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostRequestAct),actionList(h) {};
	hCattrVarPostRequestAct(const hCattrVarPostRequestAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPostRequestAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostRequestAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrVarPostRequestAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};

class hCattrVarPostUserAct : public actionList, public hCattrBase
{	
public:
	hCattrVarPostUserAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostUserAct),actionList(h) {};
	hCattrVarPostUserAct(const hCattrVarPostUserAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrVarPostUserAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrPostUserAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrVarPostUserAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};


class hCattrUpdateAct : public actionList, public hCattrBase
{	
public:
	hCattrUpdateAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrUpdateSActions),actionList(h) {};
	hCattrUpdateAct(const hCattrUpdateAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrUpdateAct(DevInfcHandle_t h, const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrUpdateSActions),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrVarUpdateAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	};
};

class hCattrVarDisplay : public hCattrBase
{
	hCconditional<hCddlString> condFmt;// holds the value(s) 
protected:
	hCddlString Resolved;
public:
	hCattrVarDisplay(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDisplay),condFmt(h),Resolved(h) {};
	hCattrVarDisplay(const hCattrVarDisplay& sae)
		:hCattrBase(sae),condFmt(sae.devHndl()),Resolved(sae.devHndl())  
		{condFmt = sae.condFmt;};
	hCattrVarDisplay(DevInfcHandle_t h, const aCattrString* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDisplay),condFmt(h),Resolved(h)
		{condFmt.makeEqual((aCconditional*) &(aCa->condString));}
	virtual ~hCattrVarDisplay(){
		condFmt.destroy(); 
		Resolved.destroy();};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ RETURNCODE rc = 0;
		condFmt.destroy();
		rc = Resolved.destroy();
		return rc; };
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarDisplay");};

	/* stevev 10feb11 - Table 3 & 4 have complicated this to the point where I have rewritten
	   this function to reflect all the different combinations in spec 500 tables 3 & 4
    */
	RETURNCODE setDefaultVals(variableType_t vt)// Display Format
	{ 	RETURNCODE rc = SUCCESS;
		hCddlString tStr(devHndl());
		wstring format;
		unsigned size;
		if ( pItem != NULL )
		{
			size = pItem->getSize();
		}
		else
		{
			size = 0;
			DEBUGLOG(CLOG_LOG,"ERROR: setting default display format without a pointer!\n");
		}

		switch (vt)
		{
		case vT_Integer:
		case vT_Unsigned:
		case vT_Index:		// added 10nov11
			//table 4
			if ( size == 0)
				format = L"";
			else
			if ( size == 1)
				format = L"4";
			else
			if ( size == 2 )
				format = L"6";
			else
			if ( size <= 4 )// 3 & 4
				format = L"11";
			else
			if ( size <= 8 )// 5,6,7 & 8
				format = L"20";

			if ( vt == vT_Integer )
				format += L"d";// table 3
			else
				format += L"u";// table 3
			break;
		case vT_Double:
			format = L"18.8g";// table 3 & 4
			break;
		case vT_FloatgPt:
			format = L"12.5g";// table 3 & 4
			break;
		case vT_TimeValue:
			DEBUGLOG(CLOG_LOG,"ERROR: display format default with TimeValue set!\n");
			// should have been reset to float if timescale was present..fall thru 2 error
		default:
			rc = APP_PARAMETER_ERR;
			break;
		}
		tStr.setStr(format);
		condFmt.clear();
		if ( rc == SUCCESS)
		{
			return condFmt.setPayload(tStr);
		}
		else
		{
			return rc;
		}
	};
	RETURNCODE getFmtStr(wstring& retStr)
	{
		RETURNCODE rc = condFmt.resolveCond(&Resolved);
		if ( rc == SUCCESS )
			retStr=Resolved.procureVal(); 
		else
			retStr=L"d";
		return SUCCESS;
	};
	
	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condFmt.aquireDependencyList(retList,false));};

	bool isConditional(void) {	return ( condFmt.isConditional() );	};   // stevev 18nov05

	bool  reCalc(void);// stevev 18nov05 - returns didChange
};

class hCattrVarEdit : public hCattrBase
{
	hCconditional<hCddlString> condFmt;// holds the value(s) 
protected:
	hCddlString Resolved;
public:
	hCattrVarEdit(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrEdit),condFmt(h),Resolved(h) {};
	hCattrVarEdit(const hCattrVarEdit& sae)
		:hCattrBase(sae),condFmt(sae.devHndl()),Resolved(sae.devHndl())  
		{condFmt = sae.condFmt;};
	hCattrVarEdit(DevInfcHandle_t h, const aCattrString* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrEdit),condFmt(h),Resolved(h)
		{condFmt.makeEqual((aCconditional*) &(aCa->condString));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condFmt.destroy();};

	virtual ~hCattrVarEdit()
	{
		condFmt.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarEdit");};
	
	/* stevev 10feb11 - Table 3 & 4 have complicated this to the point where I have rewritten
	   this function to reflect all the different combinations in spec 500 tables 3 & 4
    */
	RETURNCODE setDefaultVals(variableType_t vt)
	{ 	RETURNCODE rc = SUCCESS;
		hCddlString tStr(devHndl());
		wstring format;
		unsigned size;
		if ( pItem != NULL )
		{
			size = pItem->getSize();
		}
		else
		{
			size = 0;
			DEBUGLOG(CLOG_LOG,"ERROR: setting default edit format without a pointer!\n");
		}

		switch (vt)
		{
		case vT_Integer:
		case vT_Unsigned:
		case vT_Index:		// added 10nov11
			//table 4
			if ( size == 0)
				format = L"";
			else
			if ( size == 1)
				format = L"4";
			else
			if ( size == 2 )
				format = L"6";
			else
			if ( size <= 4 )// 3 & 4
				format = L"11";
			else
			if ( size <= 8 )// 5,6,7 & 8
				format = L"20";

			if ( vt == vT_Integer )
				format += L"d";// table 3
			else
				format += L"u";// table 3
			break;
		case vT_Double:
			format = L"18.8f";// table 3 & 4
			break;
		case vT_FloatgPt:
			format = L"12.5f";// table 3 & 4
			break;
		case vT_TimeValue:
			DEBUGLOG(CLOG_LOG,"ERROR: display format default with TimeValue set!\n");
			// should have been reset to float if timescale was present..fall thru 2 error
		default:
			rc = APP_PARAMETER_ERR;
			break;
		}
		tStr.setStr(format);
		condFmt.clear();
		if ( rc == SUCCESS )
		{
			return condFmt.setPayload(tStr);
		}
		else
		{
			return rc;
		}
	};
	RETURNCODE getFmtStr(wstring& retStr)
	{
		hCddlString	localDDstr(devHndl());
		RETURNCODE rc = condFmt.resolveCond(&localDDstr);
		if ( rc == SUCCESS )
			retStr=localDDstr.procureVal(); 
		else
		{
			retStr=L"d";
			DEBUGLOG(CERR_LOG,"ERROR: edit format string did not resolve its conditional.\n");
		}
		localDDstr.destroy();
		return SUCCESS;
	};
};


/************************ IDENTITY **************************/
class hCattrIdentity : public hCattrBase	
{
	hCconditional<hCddlString> condFileName;// never conditional
protected:
	hCddlString Resolved;

public:	
	// default and up-Pointer constructor
	hCattrIdentity(DevInfcHandle_t h, hCitemBase* pIB = NULL):
				hCattrBase(h,pIB,grpItmAttrIdentity),condFileName(h),Resolved(h)  
		{
			if (pIB == NULL) 
			{  LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Identity is instantiating without an item.\n");  }		
		};
	// copy constructor
	hCattrIdentity(const hCattrIdentity& sae):
				hCattrBase(sae), condFileName(sae.devHndl()),Resolved(sae.devHndl())
		{ attr_Type = sae.attr_Type;  condFileName = sae.condFileName;};
		
	// abstract constructor (from abstract class)
	hCattrIdentity(DevInfcHandle_t h, const aCattrString * aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrIdentity), condFileName(h),Resolved(h)
		{condFileName.makeEqual((aCconditional*) &(aCa->condString));};
	RETURNCODE destroy(void){ Resolved.destroy();return condFileName.destroy();};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrIdentity()
	{
		Resolved.destroy();
		condFileName.destroy();
	}

	// returns ptr2ddlstring
	void* procure(void)
	{	Resolved.clear();
		if ( condFileName.resolveCond(&Resolved) == SUCCESS )
		{	return  &Resolved;	}
		else
		{	return NULL;		}  
	};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrIdentity");};
	

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	LOGIT(COUT_LOG,"%sIdentity: ",Space(indent));
		return condFileName.dumpSelf(indent,typeName);
	};
	RETURNCODE getString(wstring& retStr,unsigned int iID = 0){ 
		RETURNCODE rc = FAILURE;
		if ( pItem != NULL )
		{
			Resolved.clear();
			rc = condFileName.resolveCond(&Resolved);
			if (rc == SUCCESS)
			{
				retStr = Resolved.procureVal();//getDevAPI());
				if (retStr.length() > 0 )
				{
					rc = SUCCESS;
				}
				// else leave it failure
				else
				{
					retStr = L"";
					rc = APP_RESOLUTION_ERROR;
				}
			}
			else
			{	// note that a conditional without a default||else stmt will exit here
				retStr = L"No String Defined";
				rc = SUCCESS;
			}
		}
		return rc;
	};
};


class hCattrPrivate : public hCattrBase
{// never conditional - not passed in as conditional
	hCddlLong is_Private;// holds the raw value(s) 

protected:
	RETURNCODE fetch(void);
	hCddlLong  Resolved;

	bool isPrivate;

public:
	// default and up-Pointer constructor
	hCattrPrivate(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrPrivate),  is_Private(h),Resolved(h),isPrivate(false)    {};
	// copy constructor
    hCattrPrivate(hCattrPrivate& sae)// base handles hCobject
		:hCattrBase(sae),  is_Private(sae.devHndl()) ,Resolved(sae.devHndl())
		{is_Private = sae.is_Private;isPrivate = sae.isPrivate; };
	// abstract constructor (from abstract class)
	hCattrPrivate(DevInfcHandle_t h, const aCattrLong* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrPrivate),  is_Private(h),Resolved(h),isPrivate(false)
		{ is_Private.setEqual( (void*)(&(aCa->theLong)) ); }
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return is_Private.destroy();};

	virtual ~hCattrPrivate()
	{
		is_Private.destroy();
	}

	void* procure(bool isAtest = false);/*implementation moved to ddbVar.cpp*/
	bool  reCalc(void);// stevev 18nov05 - returns didChange

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"       Private:                 ");
//		return condValid.dumpSelf(indent);};
	return FAILURE;};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrPrivate");};

	bool isConditional(void) { return false; };
};

class hCattrShared : public hCattrBase // the other bool
{
	hCddlLong isShared;// holds the value(s) 

//protected:
//	RETURNCODE fetch(void);
//

public:
	// default and up-Pointer constructor
	hCattrShared(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrShared),  isShared(h)   {};
	// copy constructor
	hCattrShared(const hCattrShared& sae)// base handles hCobject
		:hCattrBase(sae),  isShared( sae.isShared) 	{};
	// abstract constructor (from abstract class)
	hCattrShared(DevInfcHandle_t h, const aCattrBool* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,grpItmAttrShared),  isShared(h)
		{ isShared.setEqual( (void*)&( aCa->theLong ) ); }
	hCattrBase* new_Copy(itemIdentity_t newID);
	
	RETURNCODE destroy(void){ return isShared.destroy();};

	virtual ~hCattrShared()
	{
		isShared.destroy();
	}

	void* procure(bool isAtest = false);/*implementation moved to ddbVar.cpp*/
	bool  reCalc(void);// stevev 18nov05 - returns didChange

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
    {	/*COUTSPACE*/LOGIT(COUT_LOG,"       Private:                 ");
//		return condValid.dumpSelf(indent);};
    return FAILURE;};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrPrivate");};

	bool isConditional(void) { return (false); };
};

/*======================= MIN-MAX VALUES ========================*/

class hCRangeItem
{
public:
	CValueVarient minVal;
	CValueVarient maxVal;

	hCRangeItem(){minVal.clear();maxVal.clear();};// sets 'em to invalid
	hCRangeItem(const hCRangeItem& s){operator=(s);};

	hCRangeItem& operator=(const hCRangeItem& s)
	{minVal=s.minVal;maxVal=s.maxVal;return *this;};

	bool isInRange(CValueVarient val2Check)
	{	
		return (val2Check >= minVal && val2Check <= maxVal);};
};

// map is used in case there is a sparse array of values
typedef map<int,hCRangeItem> RangeList_t;

class hCRangeList : public RangeList_t
{
public:
	hCRangeList(){RangeList_t::clear();};
	hCRangeList(const hCRangeList& s){ *this = s; };
		
	bool isInRange(CValueVarient  val2Check);
	bool coerceVal(CValueVarient& val2Fix);// returns succeeded (t|f)

};


class hCminmaxVal
{
public:
	int  whichVal;
	//hCexpression ExprPayload;//The Binary file spec indicates this is actually a conditional<expression>
	hCconditional<hCexprPayload> condMinMaxExpr;

	//hCminmaxVal(DevInfcHandle_t h) : ExprPayload(h)  { clear();};	
	hCminmaxVal(DevInfcHandle_t h) : condMinMaxExpr(h)  { clear();};

	hCminmaxVal& operator=(const hCminmaxVal& src)
	//{whichVal=src.whichVal;ExprPayload=src.ExprPayload;return *this;};
	{whichVal=src.whichVal;condMinMaxExpr=src.condMinMaxExpr;return *this;};

	//void destroy(void)	{	ExprPayload.destroy();};
	void destroy(void)	{	condMinMaxExpr.destroy();};
	virtual ~hCminmaxVal()
	{
		//ExprPayload.destroy();
		// stevev 25aug06 - this is deleting the poinyters when we do a Push_back
		// (doing a realloc), the second time in, it dies
		// a copy constructor may make a difference
		//condMinMaxExpr.destroy();
	}
	//void clear(void)    {   whichVal = -1; ExprPayload.clear();  };
	void clear(void)    {   whichVal = -1; condMinMaxExpr.clear();  };

	void setDuplicate(hCminmaxVal& rMmv){whichVal = rMmv.whichVal; 
	//							    ExprPayload.setDuplicate(rMmv.ExprPayload);};
							condMinMaxExpr.setDuplicate(rMmv.condMinMaxExpr,false);};
#ifdef _DEBUG
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	LOGIT(COUT_LOG,"%s%s%d description:\n",
			Space( indent ),typeName,whichVal);
		condMinMaxExpr.dumpSelf(indent+3, typeName);return 0;
	};
#endif
};

typedef vector<hCminmaxVal>         MinMaxList_t;
typedef MinMaxList_t::iterator      MinMaxIT_t;

// Min-Max List - just a list of conditional expressions -
class hCMinMaxList : public hCobject, public hCpayload, public MinMaxList_t
{	 
public: 
	hCMinMaxList(DevInfcHandle_t h):hCobject(h) {MinMaxList_t::clear();};
	virtual ~hCMinMaxList(){		
		for(MinMaxList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} 
		MinMaxList_t::clear();
	};
	void clear(void) {
		/* destroying in a clear() is TERRIBLE - we kill off the original owner 
		 FOR_this_iT(MinMaxList_t,this) {iT->destroy();}
		 ******/		 
		 FOR_this_iT(MinMaxList_t,this) {iT->clear();}
		MinMaxList_t::clear();         };
	RETURNCODE  destroy(void){		
		for(MinMaxList_t::iterator iT = begin();iT<end();iT++){ iT->destroy();} return SUCCESS;
	};
	RETURNCODE getDepends(ddbItemList_t& retList){	RETURNCODE rc = SUCCESS;	
		for(MinMaxList_t::iterator iT = begin();iT<end();iT++)// ptr 2a hCminmaxVal
		{ rc |= iT->condMinMaxExpr.aquireDependencyList(retList,false);  } return rc;
	};

	void  setEqual(void* pAclass);//aCminmaxList
	void duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);
	bool validPayload(payloadType_t ldType)
	{ bool r = false; if (ldType==pltMinMax) { r = true;} return r;};
#ifdef _DEBUG
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      List Of %s descriptions:\n",typeName);
		int c = 0;
		for ( MinMaxIT_t yt = begin(); yt != end(); ++yt)
		{   LOGIT(COUT_LOG,"%selement #%2d:\n",Space( indent ),++c);
			yt->dumpSelf(indent+3, typeName);
		}return 0;
	};
#endif
};
   
/********************** MIN MAX ATTRIBUTE  ************************/

class hCattrVarMinVal : public hCattrBase
{
	//       hCconditional<hCMinMaxList> condMinMax;// holds the value(s) 
	/* NOTE: stevev 23aug06 -the list itself cannot be conditional.
	   ie if ( x ) { MIN_VALUE1  47; } ELSE { MIN_VALUE2  55; }
	   the values in the list can be conditional, but NOT the list
	   ie MIN_VALUE1 IF( x ) { 47; } ELSE { 55; }
	*/
	hCMinMaxList  MinMaxList;
	CValueVarient returnValue;
public:
	hCattrVarMinVal(DevInfcHandle_t h, hCitemBase* pIB = NULL)
	//	:hCattrBase(h,pIB,varAttrMinValue),condMinMax(h) {};
		:hCattrBase(h,pIB,varAttrMinValue),MinMaxList(h) {};
	hCattrVarMinVal(DevInfcHandle_t h, const aCattrVarMinMaxList* aCa, hCitemBase* pIB = NULL)
	//	:hCattrBase(h,pIB,varAttrMinValue),condMinMax(h)
	//	{condMinMax.makeEqual((aCconditional*) &(aCa->condMinMaxVal));}//aCminmaxList
		:hCattrBase(h,pIB,varAttrMinValue),MinMaxList(h)
		{
			MinMaxList.setEqual((aCconditional*) &(aCa->ListOminMaxElements));}//aCminmaxList
	
	hCattrBase* new_Copy(itemIdentity_t newID);

	//RETURNCODE destroy(void){ return ( condMinMax.destroy());};
	RETURNCODE destroy(void){ return ( MinMaxList.destroy());};

	virtual ~hCattrVarMinVal()
	{
		//condMinMax.destroy();
		MinMaxList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarMinVal");};

	RETURNCODE setDefaultVals(variableType_t vt)
	{ 	RETURNCODE rc = SUCCESS;
		hCexprPayload tExpr(devHndl());
	//		hCMinMaxList  MinMaxList;
	//		isa public hCpayload, public MinMaxList_t
	//			isa vector<hCminmaxVal>         MinMaxList_t;
	//		isa class hCminmaxVal has:public:	int  whichVal; hCconditional<hCexprPayload> condMinMaxExpr;
// leave it empty for now
// stevev 27jul10 -- not enough info in "leave it empty for now".
//                -- commented out the ::>>>     return FAILURE;
// stevev 02aug10 - it doesn't work...
		return FAILURE;


		CValueVarient tV;

		switch(vt)
		{			
		case	vT_Integer:			// 2 
			tV = ((int)1);
			break;
		case	vT_Unsigned:		// 3
			tV = ((unsigned int)1);
			break;
		case	vT_FloatgPt:		// 4
			tV = ((float)1.0);
			break;
		case	vT_Double:			// 5
			tV = ((double)1.0);
			break;
		default:
			return FAILURE;// must be a numeric type to scale it
			break;
		}
		//scaleFactor = tV;
		//isConstScale= true;

		rc = tExpr.setValue(tV);

		MinMaxList.clear();
		if ( rc == SUCCESS)
		{
			return MinMaxList[0].condMinMaxExpr.setPayload(tExpr);
		}
		else
		{
			return rc;
		}
	};

	RETURNCODE getList(hCMinMaxList* pMML)// resolves the list
	{	//return ( condMinMax.resolveCond(pMML) );     
		if ( ! pMML)return FAILURE;  // you must suppluy an empty list
		(*pMML) = MinMaxList;        // make you a copy
		return (pMML->size() != MinMaxList.size());// SUCCESS is FALSE 
	};
	/* stevev 21aug06 - dependency */
	RETURNCODE getDepends(ddbItemList_t& retList)
		{ //return (condMinMax.aquireDependencyList(retList,true));
		    return (MinMaxList.getDepends(retList));  };
	hCminmaxVal* getValptr( int which )
	{	for (MinMaxIT_t it = MinMaxList.begin(); it != MinMaxList.end();++it)
		{ if (it->whichVal == which) return ((hCminmaxVal*)&(*it)); } return NULL;  // was just it PAW 03/03/09
	};
	CValueVarient getValue(int which = 0);
	CValueVarient getMin(void);	// return the smallest of the small

#ifdef _DEBUG
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	LOGIT(COUT_LOG,"\n%sMIN_VALUE attribute for %s (0x%04x)\n",
				Space( indent ),(pItem)?pItem->getName().c_str():NULL,
								(pItem)?pItem->getID():0);
		MinMaxList.dumpSelf(indent+3,"MIN");return 0;
	};
#endif

};
class hCattrVarMaxVal : public hCattrBase
{
	//       hCconditional<hCMinMaxList> condMinMax;// holds the value(s) 
	/* NOTE: stevev 23aug06 -the list itself cannot be conditional.
	   ie if ( x ) { MIN_VALUE1  47; } ELSE { MIN_VALUE2  55; }
	   the values in the list can be conditional, but NOT the list
	   ie MIN_VALUE1 IF( x ) { 47; } ELSE { 55; }
	*/
	hCMinMaxList  MinMaxList;	 
public:
	hCattrVarMaxVal(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrMaxValue),MinMaxList(h) {}; // condMinMax(h) {};
	hCattrVarMaxVal(DevInfcHandle_t h, const aCattrVarMinMaxList* aCa, hCitemBase* pIB = NULL)
		//:hCattrBase(h,pIB,varAttrMaxValue),MinMaxList(h) {}; // condMinMax(h)
		//{condMinMax.makeEqual((aCconditional*) &(aCa->condMinMaxVal));}
		:hCattrBase(h,pIB,varAttrMaxValue),MinMaxList(h)
		{MinMaxList.setEqual((aCconditional*) &(aCa->ListOminMaxElements));}//aCminmaxList

	hCattrBase* new_Copy(itemIdentity_t newID);

	//RETURNCODE destroy(void){ return condMinMax.destroy();};
	RETURNCODE destroy(void){ return ( MinMaxList.destroy());};

	virtual ~hCattrVarMaxVal()
	{
		//condMinMax.destroy();
		MinMaxList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarMaxVal");};

	RETURNCODE setDefaultVals(variableType_t vt)
	{ 	RETURNCODE rc = SUCCESS;
		hCexprPayload tExpr(devHndl());
	//		hCMinMaxList  MinMaxList;
	//		isa public hCpayload, public MinMaxList_t
	//			isa vector<hCminmaxVal>         MinMaxList_t;
	//		isa class hCminmaxVal has:public:	int  whichVal; hCconditional<hCexprPayload> condMinMaxExpr;
// leave it empty for now
return FAILURE;


		CValueVarient tV;

		switch(vt)
		{			
		case	vT_Integer:			// 2 
			tV = ((int)1);
			break;
		case	vT_Unsigned:		// 3
			tV = ((unsigned int)1);
			break;
		case	vT_FloatgPt:		// 4
			tV = ((float)1.0);
			break;
		case	vT_Double:			// 5
			tV = ((double)1.0);
			break;
		default:
			return FAILURE;// must be a numeric type to scale it
			break;
		}
		//scaleFactor = tV;
		//isConstScale= true;

		rc = tExpr.setValue(tV);

		MinMaxList.clear();
		if ( rc == SUCCESS)
		{
			return MinMaxList[0].condMinMaxExpr.setPayload(tExpr);
		}
		else
		{
			return rc;
		}
	};

	RETURNCODE getList(hCMinMaxList* pMML)// resolves the list
	{	//return ( condMinMax.resolveCond(pMML) );   
		if ( ! pMML)return FAILURE;  
		(*pMML) = MinMaxList;
		return (pMML->size() != MinMaxList.size());// SUCCESS is FALSE 
	};
	/* stevev 21aug06 - dependency */
	RETURNCODE getDepends(ddbItemList_t& retList)
	{ //return (condMinMax.aquireDependencyList(retList, true));};
		    return (MinMaxList.getDepends(retList));  };
	hCminmaxVal* getValptr( int which )
	{	for (MinMaxIT_t it = MinMaxList.begin(); it != MinMaxList.end();++it)
		{ if (it->whichVal == which) return ((hCminmaxVal*)&(*it)); } return NULL;  // was just it PAW 03/03/09
	};
	CValueVarient getValue(int which = 0);
};


class hCattrExpr : public hCattrBase
{	
	hCconditional<hCexprPayload> condExpr;
							// polymorphic overload NOT ALLOWED to be Conditional
protected:

	hCexprPayload m_expr;
	bool isProxy;        // AKA: is a polymorphic overload of expression in COUNT

public:
	hCattrExpr(DevInfcHandle_t h, int attrType, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,attrType),condExpr(h),m_expr(h)		{};// empty constructor
	hCattrExpr(DevInfcHandle_t h, int attrType, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,attrType),condExpr(h),m_expr(h),isProxy(false) 
		{condExpr.makeEqual((aCconditional*) &(aCa->condExpr));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE  destroy(void){ return condExpr.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrExpr");};
		
	CValueVarient getExprVal(void)
	{
		CValueVarient tV;
		m_expr.clear();
		//     made isaTest to stop deadlock in send
		if ( condExpr.resolveCond(&m_expr,true) == SUCCESS)
		{// still locks up, make resolve a test too

			tV  = m_expr.resolve(NULL,true);// made this a test to stop a deadlock 27jan11
		}
		return tV;
	};
	// 08aug06 - stevev - the ability to update
	// remove - un needed---bool isExprAref(void);
	hCNumeric* getExprRef(void);// returns NULL on not there

	RETURNCODE getDepends(ddbItemList_t& retList)
	{		return (condExpr.aquireDependencyList(retList,false));  	};
	
	bool  reCalc(void);
};

class hCattrScale : public hCattrBase
{	
	hCconditional<hCexprPayload> condScale;

	// shortcuts for constant scale
	bool                isConstScale;
	double				scaleFactor;


public:
	hCattrScale(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrScaling),condScale(h),scaleFactor(0.0),isConstScale(false){};
	hCattrScale(DevInfcHandle_t h, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrScaling),condScale(h),scaleFactor(0.0),isConstScale(false)
		{condScale.makeEqual((aCconditional*) &(aCa->condExpr));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condScale.destroy();};

	virtual ~hCattrScale()
	{
		condScale.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrScale");};
		
	bool isConditional(void)							// stevev 18nov05
	{	if ( (! isConstScale) && condScale.isConditional() )	return true;
		else /* is constant or NotConditional */				return false;
	};
	bool  reCalc(void);// stevev 18nov05 - returns didChange

	RETURNCODE setDefaultVals(variableType_t vt)
	{ 	RETURNCODE rc = SUCCESS;
		hCexprPayload tExpr(devHndl());
		CValueVarient tV;

		switch(vt)
		{			
		case	vT_Integer:			// 2 
			tV = ((int)1);
			break;
		case	vT_Unsigned:		// 3
			tV = ((unsigned int)1);
			break;
		case	vT_FloatgPt:		// 4
			tV = ((float)1.0);
			break;
		case	vT_Double:			// 5
			tV = ((double)1.0);
			break;
		default:
			return FAILURE;// must be a numeric type to scale it
			break;
		}
		scaleFactor = tV;
		isConstScale= true;

		rc = tExpr.setValue(tV);

		condScale.clear();
		if ( rc == SUCCESS)
		{
			return condScale.setPayload(tExpr);
		}
		else
		{
			return rc;
		}
	};
	double getScaleFactor(void);
	
	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condScale.aquireDependencyList(retList,false));};
};

class hCitemArray; // external def

class hCattrVarIndex : public hCattrBase
{		
	hCconditional<hCreference> condIndex;
public:
	hCattrVarIndex(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrIndexItemArray),condIndex(h)  {};
	hCattrVarIndex(DevInfcHandle_t h, const aCattrCondReference* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrIndexItemArray),condIndex(h)
		{condIndex.makeEqual((aCconditional*) &(aCa->condRef));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condIndex.destroy();};

	virtual ~hCattrVarIndex()
	{
		condIndex.destroy();	
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarIndex");};

	RETURNCODE  getArrayPointer(hCitemBase*& pItemArray);	
	hCitemBase* getArrayPointer(void)
	{  hCitemBase* pIB = NULL; if (getArrayPointer(pIB)==SUCCESS) return pIB; else return NULL; };
	RETURNCODE getAllArrayPtrs(vector<hCitemBase*>& ArrayPtrList);// for all conditional outcomes
};


class hCattrVarDefault : public hCattrBase
{		
	hCconditional<hCexprPayload> condDefault;
public:
	hCattrVarDefault(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDefaultValue),condDefault(h)  {};
	hCattrVarDefault(DevInfcHandle_t h, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDefaultValue),condDefault(h)
		{condDefault.makeEqual((aCconditional*) &(aCa->condExpr));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condDefault.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarDefault");};

	RETURNCODE getDfltVal(CValueVarient& returnedValue);
};


/* hart vars have no response codes
class hCattrVarRespCds : public hCattrBase
{		
	hCconditional<hCreference> condRespCd;
public:
	hCattrVarRespCds(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrResponseCodes),condRespCd(h)  {};
	hCattrVarRespCds(DevInfcHandle_t h, const aCattrVarRespCds* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrResponseCodes),condRespCd(h)
		{condRespCd.makeEqual((aCconditional*) &(aCa->condVarRespCd));}

	RETURNCODE destroy(void){ return condRespCd.destroy();};

	virtual ~hCattrVarRespCds()
	{
		condRespCd.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarRespCds");};
};
*/


/*======================= EDIT DISPLAYS ========================*/
//#include "itemEditDisplay.h"

#include "ddbEditDisplay.h"
//class hCedDispList;
// stevev 2nov4....typedef hCcondList<hCedDispList>	condEdDispList_t;
   
   
/********************** EDIT DISP LABEL  ************************
class CattrEdDispLabel : public hCattrBase
{
//	CeditDisp* pParentEdDisp;//EDIT_DISPLAY
	      ;
	hCconditional<hCddlString/x*,aCeditDisp*x/> condEdDispLabel;// holds the value(s) 

protected:
	RETURNCODE fetch(void);

public:
	CattrEdDispLabel(DevInfcHandle_t h, hCitemBase* pIB = NULL)     
		:hCattrBase(h,pIB,eddispAttrLabel),condEdDispLabel(h) {};
	CattrEdDispLabel(const CattrEdDispLabel& sae)
		:hCattrBase(sae),condEdDispLabel(sae.devHndl())	{  condEdDispLabel = sae.condEdDispLabel;}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	condEdDispLabel.dumpSelf(indent);
		cout << endl;
		return SUCCESS;
	};

//	RETURNCODE fetch(void);
};

// stevev 2nov4 - we use rference lists now.......
******************** EDIT DISP EDIT ITEMS  *********************
class CattrEdDispEdits : public hCattrBase
{
//	CeditDisp* pParentEdDisp;
	      ;
	// conditional list of reference lists
	condEdDispList_t condEditListOlists;

protected:
	RETURNCODE fetch(void);

public:
	CattrEdDispEdits(DevInfcHandle_t h, hCitemBase* pIB = NULL)     
		:hCattrBase(h,pIB,eddispAttrEditItems),condEditListOlists(h) {};
	CattrEdDispEdits(const CattrEdDispEdits& sae)
		:hCattrBase(sae),condEditListOlists(sae.devHndl()) { condEditListOlists = sae.condEditListOlists;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	return condEditListOlists.dumpSelf(indent);};

//	RETURNCODE fetch(void);
	RETURNCODE getList(vector<hCitemBase*>& vML);//{ return condEditListOlists.fetchList(vML);};
};

********************** EDIT DISP DISPLAY ITEMS  *********************
class CattrEdDispDisplays : public hCattrBase
{
//	CeditDisp* pParentEdDisp;

	condEdDispList_t condDispListOlists;

protected:
	RETURNCODE fetch(void);

public:
	CattrEdDispDisplays(DevInfcHandle_t h, hCitemBase* pIB = NULL)  
		:hCattrBase(h,pIB,eddispAttrDispItems),condDispListOlists(h) {};
	CattrEdDispDisplays(const CattrEdDispDisplays& sae)
		:hCattrBase(sae),condDispListOlists(sae.devHndl()) { condDispListOlists = sae.condDispListOlists;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
			{	return condDispListOlists.dumpSelf(indent);};

//	RETURNCODE fetch(void);
	RETURNCODE getList(vector<hCreference*>& vML);//{ return condDispListOlists.fetchList(vML);};
};
end stevev 2nov4  */


/********************* EDIT DISP PRE EDIT ACTIONS  *******************/
class hCattrEdDispPreEditAct : public actionList, public hCattrBase
{	
public:
	hCattrEdDispPreEditAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,eddispAttrPreEditAct),actionList(h) {};
	hCattrEdDispPreEditAct(const hCattrEdDispPreEditAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrEdDispPreEditAct(DevInfcHandle_t h, 
									const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,eddispAttrPreEditAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrEdDispPreEditAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrEdDispPreEditAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	}; 
};

/******************** EDIT DISP POST EDIT ACTIONS  *******************/
class hCattrEdDispPostEditAct : public actionList, public hCattrBase
{
public:
	hCattrEdDispPostEditAct(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,eddispAttrPostEditAct),actionList(h) {};
	hCattrEdDispPostEditAct(const hCattrEdDispPostEditAct& sae)
		:hCattrBase(sae),actionList(sae.devHndl())  
		{condPrePostActList = sae.condPrePostActList;};
	hCattrEdDispPostEditAct(DevInfcHandle_t h, 
									const aCattrActionList* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,eddispAttrPostEditAct),actionList(h)
		{condPrePostActList.setEqualTo((AcondReferenceList_t*) &(aCa->ActionList));};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPrePostActList.destroy();};

	virtual ~hCattrEdDispPostEditAct()
	{
		condPrePostActList.destroy();
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("hCattrEdDispPostEditAct");};

	RETURNCODE getItemPtrs(ddbItemList_t& riL)		// get those *&^%$# items
	{	RETURNCODE rc = SUCCESS;
		hCreferenceList  localRefList(devHndl());
		if ( (rc=condPrePostActList.resolveCond(&localRefList)) == SUCCESS)
		{	rc = localRefList.getItemPtrs(riL);
		}// else return the error
		return rc;
	}; 
};



/******************** METHOD SUPPORT  **************************/

class hCattrMethodClass : public hCattrBase
{
	hCconditional<hCbitString> condClass;// holds the value(s) 

protected:
	wstring returnString;
	hCbitString Resolved; // temporary pointer to resolved conditional

	// unused/unimplemented RETURNCODE fetch(void);

public:
	// default and parent ID constructor
	hCattrMethodClass(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrClass), condClass(h),Resolved(h) {};
	// copy constructor
	hCattrMethodClass(const hCattrMethodClass& sae)
		:hCattrBase(sae), condClass(sae.devHndl()), Resolved(sae.devHndl()) 	
	{ condClass = sae.condClass;}
	// abstract constructor (from abstract class)
	hCattrMethodClass(DevInfcHandle_t h, const aCattrBitstring*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrClass), condClass(h),Resolved(h)  
		{ condClass.setEqualTo((void*) &(aCa->condBitStr));};
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condClass.destroy());};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrMethodClass()
	{
		Resolved.destroy() ;
		condClass.destroy();
	}
	
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
				{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Class:                    ");
					return condClass.dumpSelf(indent, typeName);};

	void* procure(void)
	{ 
		Resolved.clear();
		if ( SUCCESS == condClass.resolveCond(&Resolved) )
		{	return &Resolved;	}
		else
		{	return NULL;}
	};


	RETURNCODE getDescription(int oSet)
			{GETDESCNOTHERE("hCattrMethodClass");};
	RETURNCODE getBstring(ulong& classBitString)
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: Class couldn't aquire the conditional bitstring.\n");
		return FAILURE;
	};
	// returns a comma-delimited string of bit meanings
	// Methods use the same classes as variables!
	wstring& translateBstring(ulong bitStrValue)// TODO: add language option
	{
		returnString.erase();
		for(int i = 1; i < CLASSSTRCOUNT; i++) /* i == 0 is No Class */
		{
			if ( (bitStrValue>>(i-1)) & 0x01 )
			{
				if ( ! returnString.empty() )
				{
					returnString +=L", ";
				}
				returnString += varClassStrings[i];
			}
		}
		if ( returnString.length() == 0 )
		{
			returnString = varClassStrings[0];
		}
		return returnString;
	};
};
class hCattrMethodScope : public hCattrBase
{
	hCconditional<hCbitString> condScope;// holds the value(s) 

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCbitString Resolved;

public:
	// default and parent ID constructor
	hCattrMethodScope(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrScope), condScope(h),Resolved(h) {};
	// copy constructor
	hCattrMethodScope(const hCattrMethodScope& sae)
		:hCattrBase(sae), condScope(sae.devHndl()),Resolved(sae.devHndl())
		{   condScope = sae.condScope; }
	// abstract constructor (from abstract class)
	hCattrMethodScope(DevInfcHandle_t h, const aCattrBitstring*  aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrScope), condScope(h),Resolved(h) 
		{ condScope.setEqualTo((void*) &(aCa->condBitStr));};
	hCattrBase* new_Copy(itemIdentity_t newID);
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condScope.destroy());};
	
	virtual ~hCattrMethodScope()
	{
		Resolved.destroy() ;
		condScope.destroy();
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Scope:                 ");
		return condScope.dumpSelf(indent);
	};

	void* procure(void){ 
	//  modified to not need API since the lower objects know how to read and write
		Resolved.clear();
		if ( condScope.resolveCond(&Resolved) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};


	RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCbitString tBstr(devHndl());
		tBstr.setBitStr(USER_TYPE_METHOD);//SCALING_TYPE_METHOD
//		return condHndlng.setPayload(tBstr);
return FAILURE;
	};

	RETURNCODE getDescription(int oSet = 0)
		{GETDESCNOTHERE("hCattrMethodScope");};
	RETURNCODE getBstring(ulong& scopeBitString)
	{

	};
};
class hCattrMethodDef : public hCattrBase
{
	char* pDef;   // this class owns this memory
	int   defLen;

protected:
	// unused/unimplemented RETURNCODE fetch(void);

public:
	hCattrMethodDef(DevInfcHandle_t h, hCitemBase* pIB = NULL)     
		:hCattrBase(h,pIB,methodAttrDefinition) 
		{	pDef = NULL; defLen = 0;  };
	hCattrMethodDef(const hCattrMethodDef& sae)	:hCattrBase(sae)
		{	defLen = sae.defLen;
			pDef = new char[defLen+1];//via DD@F
			if (pDef)
				strncpy(pDef,sae.pDef,defLen);
		};
	// abstract constructor (from abstract class)
	hCattrMethodDef(DevInfcHandle_t h, const aCattrMethodDef*  aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrDefinition)
		{	defLen = aCa->blobLen;
			pDef = new char[defLen+1];
			if (pDef)
				strncpy(pDef, aCa->pBlob, defLen);
		};
	hCattrBase* new_Copy(itemIdentity_t newID);
	RETURNCODE destroy(void){ delete[] pDef; pDef = NULL; defLen = 0; return SUCCESS;};//via DD@F

	virtual ~hCattrMethodDef()
	{
		delete[] pDef; pDef = NULL; defLen = 0; //via DD@F
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	
		LOGIT(COUT_LOG,"%p\n", pDef);
		return SUCCESS;
	};

	char* getMethodDefinition(void) { return pDef; };
	int   getDefLen(void) { return defLen;};
};


class hCmethodParam
{
	methodVarType_t		type;
	methodTypeFlag_t	flags;
	string				name;
public:
	hCmethodParam() :type(methodVarVoid),flags(methodType_NonFlaged),name(""){};
	hCmethodParam(const hCmethodParam& mp):type(mp.type),flags(mp.flags),name(mp.name){};
	hCmethodParam(const aCparameter*  aCa)
		:type((methodVarType_t)(aCa->type)),flags((methodTypeFlag_t)(aCa->modifiers)),
		 name(aCa->paramName){/* 3oct05 - arrays are ONLY by ref (as per wap)*///Anil  has changed on behalf of Steve
		if (flags & methodType_Array) 
			flags = (methodTypeFlag_t)(flags |(int)methodType_Reference);
	};

	RETURNCODE destroy(void) {name.erase(); return SUCCESS;};
	methodVarType_t		getType() { return type;};
	bool				isArray() { return ((flags & methodType_Array) != 0); };
	bool				isRef()   { return ((flags & methodType_Reference) != 0); };
	bool				isConst() { return ((flags & methodType_Const) != 0); };
	string&             getName() { return name; };
	/* DD is the only Setter */	
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	if (typeName != NULL ){
		/*COUTSPACE*/LOGIT(COUT_LOG,"%s: ",typeName);}else{
		/*COUTSPACE*/LOGIT(COUT_LOG,"      Param:  ");}
		if (isConst()){LOGIT(COUT_LOG," const ");} 
		LOGIT(COUT_LOG,"%s  ",methodVarTypeStrings[type]); 
		if (isRef()){LOGIT(COUT_LOG," & ");}		 
		LOGIT(COUT_LOG,"%s   ",name); return SUCCESS;
	};
	hCmethodParam& operator=(const hCmethodParam& mP)
	{	type = mP.type;flags=mP.flags;name=mP.name; return *this;};
};

typedef vector<hCmethodParam> ParamList_t;
typedef ParamList_t::iterator paramIt_t;

class hCattrMethodType : public hCattrBase
{
	hCmethodParam methType;

public:
	hCattrMethodType(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,methodAttrType), methType() {  };

	hCattrMethodType(const hCattrMethodType& mt)
		:hCattrBase(mt), methType(mt.methType){  };

	hCattrMethodType(DevInfcHandle_t h, const aCparameter*  aCa, hCitemBase* pIB = NULL)
		: hCattrBase(h,pIB,methodAttrType), methType(aCa){};
	hCattrBase* new_Copy(itemIdentity_t newID);

	virtual ~hCattrMethodType() {};
	RETURNCODE destroy(void) { return (methType.destroy()); };

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrMethodType");};

	virtual void* procure(void) { return &methType; };
					
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Type:                    ");
		return methType.dumpSelf(indent, typeName);};
};

class hCattrMethodParams : public hCattrBase
{
	ParamList_t paramLst;
public:
	hCattrMethodParams(DevInfcHandle_t h, hCitemBase* pIB = NULL) 
		:hCattrBase(h,pIB,methodAttrParams){ paramLst.clear();  };	

	hCattrMethodParams(const hCattrMethodParams& mp)
		:hCattrBase(mp), paramLst(mp.paramLst){  };

	hCattrMethodParams(DevInfcHandle_t h, const aCparameterList*  aCa, hCitemBase* pIB = NULL)
		: hCattrBase(h,pIB,methodAttrParams)
	{	FOR_p_iT(AparamList_t, (aCparameterList*)aCa)
		{	
#if _MSC_VER >= 1300  /* <--- HOMZ - port to 2003, VS7 */ || defined(__GNUC__)
			// error C2664: '=' cannot convert parameter 1 from 'std::vector<_Ty>::iterator' to 'const hCmethodParam&'
			/*aCparameter aCa;
			aCa.attr_mask = iT->attr_mask;
			aCa.modifiers = iT->modifiers;
			aCa.type      = iT->type;
			aCa.paramName = iT->paramName;
			hCmethodParam tmpPar(&aCa);*/
			hCmethodParam tmpPar( &(*iT) );
#else
			hCmethodParam tmpPar(iT);
#endif
			paramLst.push_back(tmpPar);
		}
	};
	hCattrBase* new_Copy(itemIdentity_t newID);

	virtual ~hCattrMethodParams() { paramLst.clear(); };

	RETURNCODE destroy(void)
	{	FOR_iT(ParamList_t, paramLst)
		{	iT->destroy();			}
		paramLst.clear();
		return SUCCESS;
	};

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrMethodParams");};

	virtual void* procure(void) { return &paramLst; };
					
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      Parameter List:  \n");;
		FOR_iT(ParamList_t, paramLst)
		{	iT->dumpSelf(indent+3,"Param");
		}
		return SUCCESS;
	};


};


class hCaxis; // external def

class hCattrAxisRef : public hCattrBase
{		
	hCconditional<hCreference> condAxis;
protected:
	hCreference m_axisRef;
public:
	hCattrAxisRef(DevInfcHandle_t h, int attrNum, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,   attrNum    ),condAxis(h),m_axisRef(h)  {};

	hCattrAxisRef(DevInfcHandle_t h, int attrNum, const aCattrCondReference* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,    attrNum     ),condAxis(h),m_axisRef(h)
		{condAxis.makeEqual((aCconditional*) &(aCa->condRef));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condAxis.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrAxis");};
	/*Vibhor 071004: Start of Code*/
	void * procure()
	{
		m_axisRef.clear();
		if ( condAxis.resolveCond(&m_axisRef) == SUCCESS )
		{	return &m_axisRef;	}
		else
		{	return NULL;		}
	};

	/*Vibhor 071004: End of Code*/

	/* stevev 18jul05 */
	CValueVarient getValue(void);
	RETURNCODE getDepends(ddbItemList_t& retList);
	bool  reCalc(void); // stevev 10oct06 - returns didChange
	int   notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent);


	
};

/******************** IMAGE SUPPORT  **************************/

class hCattrLink : public hCattrBase
{		
	hCconditional<hCreference> condLink;
protected:
	hCreference m_linkRef;
public:
	hCattrLink(DevInfcHandle_t h, int attrNum, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,   attrNum    ),condLink(h),m_linkRef(h)  {};

	hCattrLink(DevInfcHandle_t h, int attrNum, const aCattrCondReference* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,    attrNum     ),condLink(h),m_linkRef(h)
		{condLink.makeEqual((aCconditional*) &(aCa->condRef));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void)
	{ return condLink.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrLink");};
	/*Vibhor 071004: Start of Code*/
	void * procure()
	{
		m_linkRef.clear();
		if ( condLink.resolveCond(&m_linkRef) == SUCCESS )
		{	return &m_linkRef;	}
		else
		{	return NULL;		}
	};
};


class hCattrPath : public hCattrBase
{		
	hCconditional<hCddlLong> condPath; 
protected:
	hCddlLong Resolved;

public:
	hCattrPath(DevInfcHandle_t h, int attrNum, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,   attrNum    ),condPath(h),Resolved(h)  {};

	hCattrPath(DevInfcHandle_t h, int attrNum, const aCattrCondLong* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,    attrNum     ),condPath(h),Resolved(h)
		{condPath.makeEqual((aCconditional*) &(aCa->condLong));}
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ return condPath.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrPath");};
	/*Vibhor 071004: Start of Code*/
	void * procure(bool isAtest = false)
	{
		Resolved.clear();
		if ( condPath.resolveCond(&Resolved, isAtest) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};

	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condPath.aquireDependencyList(retList,false));};

	bool isConditional(void) { return condPath.isConditional();};// stevev 18nov05 
	bool reCalc(void);// stevev 18nov05 - returns didChange
};

class hCattrVarTimeFormat : public hCattrBase
{
	hCconditional<hCddlString> condFmt;// holds the value(s) 
protected:
	hCddlString Resolved;
public:
	hCattrVarTimeFormat(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrTimeFormat),condFmt(h),Resolved(h) {};
	hCattrVarTimeFormat(const hCattrVarTimeFormat& sae)
		:hCattrBase(sae),condFmt(sae.devHndl()),Resolved(sae.devHndl())  
		{condFmt = sae.condFmt;};
	hCattrVarTimeFormat(DevInfcHandle_t h, const aCattrString* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrTimeFormat),condFmt(h),Resolved(h)
		{condFmt.makeEqual((aCconditional*) &(aCa->condString));}
	virtual ~hCattrVarTimeFormat(){condFmt.destroy(); Resolved.destroy();};
	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void){ condFmt.destroy();return Resolved.destroy();};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarTimeFormat");};

	RETURNCODE setDefaultVals(variableType_t vt)
	{ 	RETURNCODE rc = SUCCESS;
		hCddlString tStr(devHndl());

		assert( vt == vT_TimeValue);
		tStr.fillStr(DEFAULT_STD_DICT_TIME_FORMAT);// should %T
		condFmt.clear();
		condFmt.setPayload(tStr);
	};
	/* stevev 14sep10 - 
	//%R, macro for '%H:%M'					// 24 hr clock	****** Not supported by Microsoft	   
	//%T  macro for '%H:%M:%S'				// 24 hr clock	****** Not supported by Microsoft
	compensate in the getFmtStr
	**/
	RETURNCODE getFmtStr(wstring& retStr)
	{
        RETURNCODE rc = condFmt.resolveCond(&Resolved);	// J.U. to be the same as Stevev (14sep10)
		if ( rc == SUCCESS )
			retStr=Resolved.procureVal(); 
		else
			retStr=L"H:M:S";//%T
		if ( retStr == L"%T" || retStr == L"T" )
			retStr = L"H:M:S"; 
		if ( retStr == L"%R" || retStr == L"R" )
			retStr = L"H:M";

		return SUCCESS;
	};
	
	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condFmt.aquireDependencyList(retList,false));};

	bool isConditional(void) {	return ( condFmt.isConditional() );	};   // stevev 18nov05

	bool  reCalc(void);// stevev 18nov05 - returns didChange
};

class hCattrTimeScale : public hCattrBase
{
	hCconditional<hCbitString> condTS;// holds the value(s) 

protected:
	// unused/unimplemented RETURNCODE fetch(void);
	hCbitString Resolved;

public:
	// default and parent ID constructor
	hCattrTimeScale(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrTimeScale), condTS(h),Resolved(h) {};
	// copy constructor
	hCattrTimeScale(const hCattrTimeScale& sae)
		:hCattrBase(sae), condTS(sae.devHndl()),Resolved(sae.devHndl())
		{   condTS = sae.condTS; }
	// abstract constructor (from abstract class)
	hCattrTimeScale(DevInfcHandle_t h, const aCattrBitstring*  aCa, ulong attrTyp = varAttrTimeScale, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,attrTyp), condTS(h),Resolved(h) 
		{ condTS.setEqualTo((void*) &(aCa->condBitStr));};
	RETURNCODE destroy(void){ return ( Resolved.destroy() | condTS.destroy());};
	hCattrBase* new_Copy(itemIdentity_t newID);
	virtual ~hCattrTimeScale()
	{
		Resolved.destroy() ;
		condTS.destroy() ;
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
	{	/*COUTSPACE*/LOGIT(COUT_LOG,"      TimeScale:                 ");
		return condTS.dumpSelf(indent);
	};

	bool isConditional(void){ return (condTS.isConditional());};

	void* procure(bool isAtest = false){ 
	//  modified to not need API since the lower objects know how to read and write
		//CcommAPI* pCA = NULL;	
		//if ( (pCA = getDevAPI()) != NULL)
		Resolved.clear();
		if ( condTS.resolveCond(&Resolved,isAtest) == SUCCESS )
		{	return &Resolved;	}
		else
		{	return NULL;		}
	};


	RETURNCODE setDefaultVals(RETURNCODE rrc)
	{ 	hCbitString tBstr(devHndl());
		tBstr.setBitStr(READ_HANDLING | WRITE_HANDLING);
		condTS.clear();
		return condTS.setPayload(tBstr);
	};

//	RETURNCODE fetch(void);
//	RETURNCODE insertSelf(int iSet = 0)INSERTATTRCOND(condTS);
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrHndlg");};
//		{return condTS.getDescription(oSet);};// set is expected to be ATTR2COND but we make sure

	RETURNCODE getDepends(ddbItemList_t& retList)
		{	return (condTS.aquireDependencyList(retList,false));};

	bool  reCalc(void);// stevev 18nov05 - returns didChange


};

/******************** DEBUG SUPPORT  **************************/
class hCdebugAttrInfo_t  : public hCobject
{
public:
	/* TODO define these */
};

typedef vector < hCdebugAttrInfo_t > hCdebugAttrList_t;

class hCattrDebug : public hCattrBase
{	
	string		  symbolName;
	hCddlString		fileName;
	int			  lineNumber;
	ulong		   use_flags;// not needed, carried 4 tokenizer use
	
	hCdebugAttrList_t  attrs;

public:
	hCattrDebug(DevInfcHandle_t h, int attrNum, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,   attrNum    ),fileName(h)  {};

	hCattrDebug(DevInfcHandle_t h, int attrNum, const aCdebugInfo* aCdb, hCitemBase* pIB = NULL)
		: hCattrBase(h,pIB, attrNum ),fileName(h)
		{symbolName = aCdb->symbolName; fileName = aCdb->fileName; lineNumber = aCdb->lineNumber;
		use_flags = aCdb->use_flags; /* TODO: init attributeList */}
	/* copy constructor */
	hCattrDebug(const hCattrDebug& sae) : hCattrBase(sae), symbolName(sae.symbolName),
		fileName(sae.fileName),lineNumber(sae.lineNumber), attrs(sae.attrs)
		{ use_flags = sae.use_flags;};

	hCattrBase* new_Copy(itemIdentity_t newID);

	RETURNCODE destroy(void)
	{ return fileName.destroy();symbolName.erase();/*TODO: destroy attr list*/};
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrDebug");};
	
	void * procure()
	{
	//	m_linkRef.clear();
	//	if ( condLink.resolveCond(&m_linkRef) == SUCCESS )
	//	{	return &m_linkRef;	}
	//	else
		{	return NULL;		}
	};

	string& getSymbolName(void) { return symbolName;};
	void    setSymbolName(string& s) { symbolName = s; };// for psuedo items
};

#endif//_ATTRIBUTES_H

/*************************************************************************************************
 *
 *   $History: ddbAttributes.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
