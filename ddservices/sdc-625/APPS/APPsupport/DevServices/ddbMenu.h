/**********************************************************************************************
 *
 * $Workfile: ddbMenu.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the menus and their descriptiors
 *		8/09/2	sjv	created from itemMenu.h
 *
 * #include "ddbMenu.h"
 */

#ifndef _ITEMMENU_H
#define _ITEMMENU_H

#include "ddbConditional.h"
#include "ddbPrimatives.h"
#include "sysEnum.h"
#include "ddbMenuLayout.h"
#include "ddbItemBase.h"

#ifdef ALLOC_DEBUGGING
#include "CustomAlloc.h" // not in cvs...
#endif

/* * NOTE: hCmenu is defined in ddbItems.h 
 *			supporting classes are here
 */

// a menu will have a list of hCmenuItem s

#ifdef _DBGMENU
extern int nxtMenuID;
#endif

// layout and allowed are different if limited display host
#define NOT_LIMITED_DISPLAY 1

class hCcollection;

/**********************************************************************************************
 *  set the legal styles
 **********************************************************************************************
 */
// Fixed the styles in accordance with Spec 500.  Window does not allow for all styles (ie, TABLE 
// not allowed), POB - 6 Aug 2008
#define LEGALALL 	    	(0xffffffff) /* all are legal here */ 
#define LEGAL4WINDOW		(LEGALALL & ~(1<<mds_table) )
#define LEGAL4DIALOG		(LEGALALL & ~((1<<mds_window) /*| (1<<mds_menu)*/ | (1<<mds_table)))

#define LEGAL4PAGEnGROUP	(LEGALALL &  ~((1<<mds_page) | (1<<mds_table)))

#define LEGAL4MENU         ((1<<mds_window)   | (1<<mds_dialog)  | (1<<mds_menu)  |\
	                        (1<<mds_table)    | (1<<mds_edDisp)  | (1<<mds_method)|\
                            (1<<mds_separator)| (1<<mds_newline) | (1<<mds_constString) )

#define LEGAL4TABLE			(LEGALALL & ~( (1<<mds_page)|(1<<mds_group) ))

#define LEGAL4METHODDIALOG  (LEGALALL & ~( (1<<mds_window)| (1<<mds_dialog) | (1<<mds_page)  |\
	                                           (1<<mds_menu)  | (1<<mds_table)  | (1<<mds_edDisp)|\
											   (1<<mds_method)  /* this one may go */)  )

#define MENU_TYPES          ((1<<mds_window) | (1<<mds_dialog) | (1<<mds_page) | (1<<mds_group)|\
	                         (1<<mds_menu)   | (1<<mds_table)  | (1<<mds_methDialog) )

//::menuHist_t  moved to ddbMenuLayout.h  

/**********************************************************************************************
 * menu's style enum declaration
 **********************************************************************************************
 */
/* memory location of strings */
extern const char menuStyleStrings[MENUSTYLESTRCOUNT] [MENUSTYLEMAXLEN];// in ddbmenu.cpp
//					  enum type,      zeroth string,  highest vale,    array string length
/* this enum type */
#pragma warning (disable : 4786) 
#include "ddbConditional.h"

typedef  hCsystemEnum<menuStyleType_t,hCddlLong,menuStyleNone,  menuStyleUnknown,
									MENUSTYLEMAXLEN> menuStyleEnum_t;

#pragma warning (disable : 4786) 
#include "ddbConditional.h"
/* stream io for the enum */
inline ostream& operator<<(ostream& ostr,	menuStyleEnum_t& it)\
{  ostr << it.getSysEnumStr();	return ostr; }	

/*********************************************************************************************/

class hCmenu;
class hCmenuBase : public hCobject
{// common code goes here

public:
	// no default constructor (we MUST have the handle)
	hCmenuBase(DevInfcHandle_t h) : hCobject(h){};
};

/* hCmenuItem  && hCmenuTree moved to ddbMenuLayout.h ****************************************/


// a menuList is the destination of the conditionals
//             is a list of lists/items
// menu has 
//		list of conditionals 
//			each conditional resolves to a list of menu lists <CmenuList>
//				which contain a list of items <CmenuItem>
//

class maxColumn; // forward reference

#ifdef ALLOC_DEBUGGING
typedef vector<hCmenuItem, CustomAlloc<hCmenuItem> >       menuItemList_t;
#else
typedef vector<hCmenuItem>       menuItemList_t;
#endif
typedef menuItemList_t::iterator menuItmLstIT_t;

class hCmenuList : public hCpayload, public hCmenuBase, public menuItemList_t
{
	//// data ////
	//menuItemList_t itemList;

public:
	hCmenuList(DevInfcHandle_t h) : hCmenuBase(h)	
	{	
		//cerrxx<< "hCmenuList default constructor ."<< endl;
		// this is the normal instantiation sequence: instantiate w/ 'h' then use setEqual
		// to aquire the values
	};
	hCmenuList(const hCmenuList& el) : hCmenuBase(el.devHndl())
	{ 		
// should be OK??		cerrxx<< "hCmenuList reference copy constructor ."<<endl;
		//itemList = el.itemList;
		menuItemList_t::operator=(el);
	};
	hCmenuList(const hCmenuList* pel) : hCmenuBase ( pel->devHndl() )
	{ 		
		LOGIT(CERR_LOG,"hCmenuList pointer copy constructor .\n");
		//itemList = pel->itemList;
		menuItemList_t::operator=(*pel);
	};
	virtual ~hCmenuList(){destroy();clear();};
	RETURNCODE destroy(void)
	{	RETURNCODE rc = 0;
		for(menuItemList_t::iterator p = /*itemList.*/begin(); p < /*itemList.*/end(); p++)
		{	rc |= p->destroy();	} return rc;
	};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)
	{	LOGIT(COUT_LOG,"      %d Menu Items   \n",size());	 // timj repaired during tokenizer V&V 16jul2014
		for(menuItemList_t::iterator p = /*itemList.*/begin(); p < /*itemList.*/end(); p++)
		{
			p->dumpSelf(indent);
		}
		return SUCCESS;
	};
	RETURNCODE getItemPtrs(vector<hCmenuItem*>& miL);// adds 'em to the end of the list

	RETURNCODE calculateDrawAs(menuHist_t& history);
		
	void  setEqual(void* pAclass); // a hCpayload required method

	void  duplicate(hCpayload* pPayLd, bool replicate, itemID_t parentID=0);	

	void  append(hCmenuList* pList2Append);
	void  appendUnique(hCmenuList* pList2Append) 
	{// temporary bypass
		append(pList2Append);
	};
	// use the parent's void  clear(void){itemList.clear();};
	// use the parent's int   size(void) { return itemList.size();};

	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType)
								{ if (ldType != pltMenuList) return false; else return true;};
};

typedef hCcondList<hCmenuList>	condMenuList_t;


class hCmenu     : public hCitemBase /* itemType 3 MENU_ITYPE     */
{
//	CattrBase* newAttr(struct itemAttrTbl&);// {NEWATTRNOTIMPLEMENTED("Cmenu");return NULL;};
//protected:	
	/* * * * * * * * * * * * *  base clase overrides* * * * * * * * * * * * * * * */
	RETURNCODE resolveProcure(void/*self*/); /* resolve this and all the sub stuff*/

	// in base class CattrValid*     pValid;
	// in base class hCattrLabel*    pLabel;
	// in base class CattrVarHelp*   pHelp;
	condMenuList_t	 menuItems;	
	//hCconditional<menuStyleEnum_t> condStyle;
	menuStyleEnum_t  condStyle;
	
	/* the last-resolved menu size */
	hv_point_t		 hv_size;
	//menuHist_t       lastHistory;
protected:  // helper methods
	//bool fillNsizeBranch(hCmenuTree* pThisBranch, layState_t& curState, 
	//									hCmenuTree* pParallelBranch, menuHist_t& history);
	static
	bool	aquireInternalTree(hCitemBase* pB, hCmenuTree* thisMenusDesc, 
						  const menuHist_t& history = menuHist_t(), itemID_t targetID=0L );

	static     // has to be static to be called from the static above
	int		sizeBranch(hCmenuTree* pNewBranch, layState_t& brState, menuHist_t& branchHist,
						  hCmenuTree* pThisBranch, bool& didChangeRetVal );
	static    // layout helper....has to be static to be called from the static above
	int	setRowWidths(menuTreePtrList_t& row_List, maxColumn& mxCol, DDSizeLocType mxRowWth);
public:

	static int  acquireEntryCnt;

//*	hCmenu(DevInfcHandle_t h, unsigned long DDkey);
	hCmenu(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCmenu(hCmenu*     pSrc,    itemIdentity_t newID);	// from-typedef constructor 
	hCmenu(hCmenuItem* pMitem,  itemIdentity_t newID);  // standard menu for a single item

	RETURNCODE destroy(void){menuItems.destroy(); return hCitemBase::destroy(); };
//	Cmenu(ITEM_TBL_ELEM  itemElem, unsigned long DDkey, CbinarySvcs* pBSvc);
//	Cmenu(struct itemTbl&  itmR) : CitemBase(itmR) {};
	virtual ~hCmenu();
		// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0)
	{/*TODO:*/	CValueVarient rV;
		LOGIT(CERR_LOG,"WARN: menu Attribute information is not accessable.\n");
		return rV;
	};
	void  fillCondDepends(ddbItemList_t&  iDependOn); // overload to get members
	void  fillContainList(ddbItemList_t&  iDependOn);//stevev 27sep06


	/* * * * * * * * * * * * *  CDDMenu  * * * * * * * * * * * * * * * * * * * * */
    //vector<CDDMenuItem> menuItems;
	// use CmenuList class from itemMenu.h <attribute conditional resolves to that>
	RETURNCODE aquire(menuItemList_t& menuItemLst, const menuHist_t& history = menuHist_t() );
	/* aquireTree returns bool hasChangedSize  since the last aquire tree sjv 230ct06 **/
	static
	bool       aquireTree(hCitemBase* pB, hCmenuTree* thisMenusDesc, 
						  const menuHist_t& history = menuHist_t(), itemID_t targetID=0L );
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ){returnedSize.clear();
						LOGIT(CERR_LOG,"Menu setSize is NOT required!\n"); return 1;};
	// we now use thisMenusDesc->pItemBase to get a device handle so we don't need pB
	// pB was left in as null so we don't have to change allf the references to it
	RETURNCODE	setCanvasSize(int newSize);// recursivly sets tha canvas size of these items

	static
	bool       aquireBitEnum(hCmenuTree& thisMenusDesc, hCitemBase* pB=NULL);

// fall through to itembase who will look it up
//  bool IsValid() {return true; };
//  bool HasHelp() { return false; };
//  RETURNCODE Help(string &helpString) { return API_FUNCTION_NOT_SUPPORTED; }; 
	/*              returns the label of the MENU (or null)*/
	RETURNCODE Label(wstring& retStr) { return baseLabel(MENU_LABEL, retStr); }; 
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(MENU_LABEL, l); };


	RETURNCODE Read(wstring &value){value = L"ERROR:Not a Var"; return FAILURE;};

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


//	RETURNCODE load(void);
    // use parent   RETURNCODE insertSelf(void);
	// use parent   RETURNCODE insertItemInfo(void);
//	RETURNCODE getAttrs(void);
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Menu Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Menu ID/Label:                  "   << itemId << "   ";
		menuItems.dumpSelf(indent+4);
		condStyle.dumpSelf(indent+4);
		return SUCCESS;
	};

	menuStyleType_t getStyle(void); /*Get the specified style of the menu*/	
	
	void setStyle(menuStyleType_t mt){condStyle.setCondValue( mt );}; 
	//menuHist_t getLastHistory(void)         { return lastHistory; };

	int notification(hCmsgList& msgList,bool isValidity); // overload itembase
};

// a helper class -
// we want to deal with the old maxRow array using actual col/row numbers instead of (r-1)
// encasulating this class helps make it clearer and easier to use
// 
// 01apr15 - add a full layout tracking array.  DDHT forces tracking Cells, we
//			 do it in this addition.

#define CBcell      (-1)
#define rowStopCell (-10)
#define MinCellNum  (10)


class displayRow
{
public:
	int col[COLS_SUPPORTED+1];// array of columns [1- 5]...0 used for row width
#define dispRowWidth  col[0]

public:
	displayRow() { clear();};
	virtual ~displayRow(){}; // no-op
	
	displayRow(const displayRow& src) { operator=(src); };
	displayRow& operator=(const displayRow& src)
	{	for (int i =0; i <= COLS_SUPPORTED; col[i] = src.col[i], i++);  
		return *this;
	};

	void clear(void){for (int i =0; i <= COLS_SUPPORTED;col[i++] = 0);  };

	// others to be added as required
	void setAll2(int rowval){col[1]=col[2]=col[3]=col[4]=col[5]=rowval;};
	// occupied if any row value is a cell
	bool occupied(void){return ( (col[1]>=MinCellNum) || (col[2]>=MinCellNum) ||
		 (col[3]>=MinCellNum) || (col[4]>=MinCellNum) || (col[5]>=MinCellNum) );};
};
typedef vector<displayRow>          displayRowList_t;
typedef displayRowList_t::iterator  displayRowList_tIT_t;

// end 01apr15 - display rows---- used in the following class -----


class maxColumn
{
	int maxRow[COLS_SUPPORTED+1];// array of columns [1- 5]...0 not used

public://ctor dtor
	maxColumn();
	~maxColumn(){}; // nothing here to destroy

public:	// workers
	//	get the highest numbered (lowest on the screen) row used within start-stop range
	//	start-stop range is inclusive, lowest, populated or not
	int getLowest(int startCol, int endCol); 

	// get the lowest in range, then set all columns in the range to that value
	//	ie.  level the range  ( and return what you set it to)
	int setAll2Lowest(int startCol, int endCol);

	// get the lowest in range, then set all columns in the range to that value
	//	ie.  level the range...reseting neg to positive (just before insertion)
	int setThese2Lowest(int startCol, int endCol);

	//  set all columns in the range to the new row value
	//	ie.  level the range to a new value
	void setAll2(int startCol, int endCol, int newRow);

	// set a single column to a used row number
	void setCol(int columnNumber, int rowNumber);

	// get used row number from a single column 
	int getCol(int columnNumber);

	// return the last column that has something in it
	int getWidth(void);

	// return the width of this row number
	int rowWidth(int rowNumber);
//==============================================================================
// full layout tracking added from here down	stevev - 01Apr15
	displayRowList_t dispGrid; // vector of display rows

	int firstRowInRow;	// first display row in the DD row currently active
	int nxtCellNumber;	// just to have a unique number for each cell
	int mxColCanvas;
	hv_point_t insertPt;//insertion point

	int Insert(hv_point_t size, bool integralCell);//activity undertaken at item insert
	int ColBrk(void); //activity undertaken at a COLUMNBREAK
	int RowBrk(void); //activity undertaken at a ROWBREAK

	void logDisplayTrack(char* prefix);
	bool CGGG( menuItemDrawingStyles_t it);

protected: // helper functions that hold common code
	void markCB(void);
	int RBlowest(void);// sets rowWidths, RB@ lowest,returns the new y insertion point
	int setWidth(void);//sets row's width & returns largest
	void addRow(int fill);
};

#endif //_ITEMMENU_H

/**********************************************************************************************
 *
 *   $History: ddbMenu.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 **********************************************************************************************
 */
