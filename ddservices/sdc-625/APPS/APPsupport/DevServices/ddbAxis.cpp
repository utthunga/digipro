/*************************************************************************************************
 *
 * $Workfile: ddbAxis.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "ddbAxis.h"
#include "logging.h"
#include "ddbRfshUnitWaoRel.h"
#include "ddbVar.h"
#include "ddbCommand.h" /* for msglist */


hCaxis::hCaxis(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase),pMinVal(NULL),pMaxVal(NULL),condScale(h),pUnit(NULL),
	  scalingType(sT_Linear)
{// label is set in the base class
	unitRelations.clear();
	unitRelations.setOwner(getID(),devPtr());
	 // ib.AattributeList_t attrLst;// a typedef vector<aCattrBase*>	AattributeList_t;
	aCattrBase* aB = NULL;
	// try to get an abstract attribute pointer (if abstract attribute exists)
	// Note that NOTHING is required in an Axis
	aB = paItemBase->getaAttr(axisAttrMinVal); 
	if (aB != NULL)
	{	
		pMinVal = (hCattrExpr*)newHCattr(aB);
		if (pMinVal != NULL)
		{
			pMinVal->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMinVal);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Min-Val attribute for Axis ++ (memory error?)\n");
		}		
	} 

	aB = paItemBase->getaAttr(axisAttrMaxVal); 
	if (aB != NULL)
	{	
		pMaxVal = (hCattrExpr*)newHCattr(aB);
		if (pMaxVal != NULL)
		{
			pMaxVal->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pMaxVal);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Max-Val attribute for Axis ++ (memory error?)\n");
		}		
	} 

	aB = paItemBase->getaAttr(axisAttrUnit); 
	if (aB != NULL)
	{	
		pUnit = (hCattrUnit*)newHCattr(aB);
		if (pUnit != NULL)
		{
			pUnit->setItemPtr((hCitemBase*)this);
			attrLst.push_back(pUnit);// keep it on the list too
		}
		else // is NULL
		{
			LOGIT(CERR_LOG,"++ No New Unit attribute for Axis ++ (memory error?)\n");
		}		
	}

	aB = paItemBase->getaAttr(axisAttrScale); 
	if (aB != NULL)
	{	
		condScale.setEqualTo(&(((aCattrCondLong*)aB)->condLong));// an enum (binary) representation
	} 
}

/* from-typedef constructor */
hCaxis::hCaxis(hCaxis*    pSrc,  itemIdentity_t newID) : 
				hCitemBase(pSrc,newID),condScale(pSrc->devHndl())
{
	// TODO  condScale.newCopy(&(pSrc->condScale)); // in lieu of copy constructor
	LOGIT(CERR_LOG,"****Implement axis typedef constructor!*********\n");
}



hCattrBase*   hCaxis::newHCattr(aCattrBase* pACattr)
{
/*	axisAttrMinVal,			use aCattrCondExpr
	axisAttrMaxVal,			use aCattrCondExpr
	axisAttrScale,			use aCattrCondLong		- as an enum
	axisAttrUnit,			use aCattrString*/


	if (pACattr->attr_mask == maskFromInt(axisAttrMinVal)) 
	{
		return 
			new hCattrExpr(devHndl(), axisAttrMinVal, (aCattrCondExpr*)pACattr, this );		
	}
	else
	if (pACattr->attr_mask == maskFromInt(axisAttrMaxVal))	
	{
		return 
			new hCattrExpr(devHndl(), axisAttrMaxVal, (aCattrCondExpr*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(axisAttrUnit))
	{
		return 
			new hCattrUnit(devHndl(), (aCattrString*)pACattr, this );
	}
	else
	if (pACattr->attr_mask == maskFromInt(axisAttrScale))
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: axis attribute SCALE is not newed.\n");
		return NULL; /* an error */  
	}
#ifdef _DEBUG
	else 
	if (pACattr->attr_mask &
			(maskFromInt(axisAttrLabel) | 
			 maskFromInt(axisAttrHelp)  | 
			 maskFromInt(axisAttrValidity)  | 
			 maskFromInt(axisAttrDebugInfo) ))
	{//known attribute but shouldn't be here		
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: axis attribute type is not handled by base class.\n");
		return NULL; /* an error */  
	}
#endif
	else	//Attr_Unknown
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: axis attribute type is unknown.\n");
		return NULL; /* an error */  
	}
}

/*Vibhor 181004: Start of Code*/

RETURNCODE hCaxis ::getMinValue(CValueVarient & minVal)
{
	RETURNCODE rc = SUCCESS;

	if(NULL == pMinVal)
		return APP_ATTR_NOT_SUPPLIED; 
	else
	{
		minVal = pMinVal->getExprVal();
	}

	return rc;

}/*End getMinValue*/


RETURNCODE hCaxis ::getMaxValue(CValueVarient & maxVal)
{
	RETURNCODE rc = SUCCESS;

	if(NULL == pMaxVal)
		return APP_ATTR_NOT_SUPPLIED; 
	else
	{
		maxVal = pMaxVal->getExprVal();
	}

	return rc;

}/*End getMinValue*/


bool hCaxis::isLogarithmic()
{
	RETURNCODE rc = SUCCESS;

	// moved to class 101006  scale_t scalingType;

	bool bLog = false; //default is linear, in any case
		
	hCddlLong *pTmpLong = NULL;
	pTmpLong = new hCddlLong(devHndl());

	if ( condScale.priExprType == aT_Unknown)// doesn't exist
	{
		return false;
	}
	else
	{
		rc = condScale.resolveCond(pTmpLong);

		if(SUCCESS == rc)
		{
			scalingType =(scale_t)pTmpLong->getDdlLong();
			if(sT_Log == scalingType)
				bLog = true;
			else
				bLog = false;
		}
	}

	delete pTmpLong;
	pTmpLong = NULL;
	return bLog;

}
/*Vibhor 181004: End of Code*/

 
/* stevev 30nov06 - now a pass thru to unit hCunitRelList ---------- 
string  hCaxis::getUnitStr(void)
{ // will return empty ("") string in worst case
	
	string      retStr;
	RETURNCODE  rc = getUnitString(retStr);
	return retStr;
}
------------------------------ end stevev 30nov06-------------------*/

/*    added function to get the return code and the string..getUnitStr() only gets the string*/
/* stevev 30Nov06 - modified to get either UnitRel or ConstUnit string (error string @ both)*/
RETURNCODE hCaxis::getUnitString(wstring & str)
{
	RETURNCODE  rc = SUCCESS;

	if (pUnit != NULL && unitRelations.size() > 0 )
	{// const unit & unit rel - an error
		str = L"DD ERROR";
		LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,"ERROR: Const Unit AND Unit Relation Assigned.(%s)\n",getName().c_str());
		rc = FAILURE;
	}
	else
	if (pUnit != NULL)
	{
		str = (wstring) pUnit->getConstUnitStr();
	}
	else
	if ( unitRelations.size() > 0 )
	{
		str = (wstring)unitRelations.getUnit_Str();
	}
	else // neither
	{
		str.erase();
	}
	return rc;

}/*End getUnitString()*/
// stevev - WIDE2NARROW char interface
RETURNCODE hCaxis::getUnitString( string & str)
{
	wstring locStr;
	RETURNCODE rc = getUnitString(locStr);
	str = TStr2AStr(locStr);
	return rc;
}


/* stevev 30nov06 - now a pass thru to unit hCunitRelList ----------------------------
ITEM_ID hCaxis::getUnitVarId(void) // returns INVALID_ITEM_ID if there is none
{
	ITEM_ID r = INVALID_ITEM_ID;
	RETURNCODE rc = SUCCESS;
	hCitemBase* pUnitRel = NULL;
	hCVar*      pUnitVar = NULL;

	if ( unitRelation != INVALID_ITEM_ID )// we could have a relation AND a constant unit
	{		
		if ( (rc = devPtr()->getItemBySymNumber(unitRelation, &pUnitRel)) == SUCCESS &&
			 pUnitRel != NULL  && pUnitRel->getIType() == iT_Unit)
		{// we have a relation
			if ( (rc = ((hCunitRel*)pUnitRel)->getIndependVarPtr(pUnitVar))	== SUCCESS &&
				 ( pUnitVar != NULL )  )
			{
				r = pUnitVar->getID();
			}
		}// else return invalid
	}// else return invalid
	return r;
}
------------------------------ end stevev 30nov06-------------------*/


CValueVarient hCaxis::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV;

	axisAttrType_t xat = (axisAttrType_t) attrType;
	// set for error return
	rV.clear();
	rV.vType = CValueVarient::isSymID;
	rV.vValue.varSymbolID = 0; // leave inValid

	if ( xat >= axisAttr_Unknown )
	{
		LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: List's getAttrValue with invalid attribute type (%d)\n",attrType);
		return rV;
	}// else process request

	switch (xat)
	{
	case axisAttrLabel:
	case axisAttrHelp:
	case axisAttrValidity:
		rV = hCitemBase::getAttrValue(xat);		
		break;
	case axisAttrMinVal:
		{
			rV.vType = CValueVarient::isFloatConst;
			rV.vSize = 4;// default
			rV.vIsValid = true;
			rV.vIsUnsigned = false;
			if ( pMinVal == NULL )
			{// default = NO
				rV.vValue.bIsTrue = false;// default - none
			}
			else
			{
				rV = pMinVal->getExprVal();
			}
		}
		break;
	case axisAttrMaxVal:
		{
			rV.vType = CValueVarient::isFloatConst;
			rV.vSize = 4;// default
			rV.vIsValid = true;
			rV.vIsUnsigned = false;
			if ( pMaxVal == NULL )
			{// default = NO
				rV.vValue.bIsTrue = false;// default - none
			}
			else
			{
				rV = pMaxVal->getExprVal();
			}
		}
		break;
	case axisAttrScale:
		{// TODO  hCconditional<hCddlLong> condScale
			rV.vType = CValueVarient::isIntConst;
			rV.vSize = 4;// default
			rV.vIsValid = true;
			rV.vIsUnsigned = true;

			hCddlLong Val(devHndl());
			if ( condScale.resolveCond(&Val) == SUCCESS )
			{
				rV.vValue.iIntConst = (INT32)Val.getDdlLong();// very limited value set [0&1]
			}
			else
			{
				rV.vIsValid = false;
			}
		}
		break;
	case axisAttrUnit:
		{
			rV.vType = CValueVarient::isString;
			rV.vSize = 0;// default
			rV.vIsValid    = true;

			if ( pUnit == NULL )
			{
				rV.vIsValid = false;
			}
			else
			{
				rV.sWideStringVal = (wstring)pUnit->getConstUnitStr();
			}
		}
		break;
	case axisAttrViewMinVal:
		{// TODO -- How to get the actual value??????????// TODO
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: Axis ViewMin information is not accessable.\n");
			// return the id of the same psuedo variable you gave
		}
		break;
	case axisAttrViewMaxVal:
		{// TODO -- How to get the actual value??????????// TODO
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: Axis ViewMax information is not accessable.\n");
		}
		break;
	case axisAttrDebugInfo:		// not accessable
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"WARN: debug information is not accessable.\n");
		}
		break;
	default:
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}


/* stevev 12sep06 - dependency extentions */
void  hCaxis :: fillCondDepends(ddbItemList_t&  iDependOn)
{
	// call the base to get the label
	hCitemBase::fillCondDepends(iDependOn);
	// do the other attributes

	if ( pMinVal != NULL )  
	{
		pMinVal->getDepends(iDependOn);
	}
	if ( pMaxVal != NULL ) 
	{
		pMaxVal->getDepends(iDependOn);
	}

	condScale.aquireDependencyList(iDependOn,false);

/* Constant units :: Cannot change while running (dah..)*/
}
/** end stevev 12sep06  **/

/** added stevev 10oct06 **/
// called when an item in iDependOn has changed in the chart.source 
//		( Chart's notification calls Source's notification calls us)
//		Since a axis is merely an attribute grouping for sources & waveforms, 
//			we only notify the chart
int hCaxis::notification(hCmsgList& msgList,bool isValidity, hCitemBase* pParent)
{// must re-evaluate conditionals , call notify if our validity changes
	int init = msgList.size();
	hCitemBase* pItem = NULL;

	hCitemBase::notification(msgList,isValidity);// do validity and label
/* note to self - 05oct06 - cannot filter on isConditional if any part of the payload is a 
							reference or expression */
	// re-evaluate all the others   
	if (! isValidity) 
	{// min, max, scaling
		if( condScale.isConditional() )
		{// notify parent big-time
			scale_t st = scalingType, nst = sT_Undefined;
			if (isLogarithmic()) nst = sT_Log; else nst = sT_Linear;
			if ( st != nst )// it changed		;  
				msgList.insertUnique(pParent->getID(), mt_Str, 
											((getIType()<<16) | (axisAttrScale&0xffff)));
		}
		if( pMinVal != NULL &&  pMinVal->reCalc())
		{
#ifdef _INVENSYS
			msgList.insertUnique(pParent->getID(), mt_Str, (getIType()<<16) | 2);	
		// pMinVal->attrType is 3 and is the same as pLabel->attrType J.U. 
#else
			msgList.insertUnique(pParent->getID(), mt_Str, pMinVal->getGenericType());
#endif
		}
		if( pMaxVal != NULL && pMaxVal->reCalc())
		{
			msgList.insertUnique(pParent->getID(), mt_Str, pMaxVal->getGenericType());
		}
	}
	return (msgList.size() - init);// number added
}
/** end stevev 10oct06  **/
