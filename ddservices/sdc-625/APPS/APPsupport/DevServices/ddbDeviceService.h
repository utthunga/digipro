/*************************************************************************************************
 *
 * $Workfile: ddbDeviceService.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the defined interface to the device.  It is used by many classes throughout the
 * device description (from the items to the primatives).  It is also the principle interface from
 * the application to the device.
 *		
 * See Notes at the end of the file
 *		
 * #include "ddbDeviceService.h"
 */


#ifndef _DDBDEVICESERVICE_H
#define _DDBDEVICESERVICE_H

#include "pvfc.h"
#include "ddbGeneral.h" // includes vector
#include "varient.h"
#include "foundation.h"
#include "ddbPolicy.h"
//prashant for methods support.oct 2003
#include "ddbMethSupportInfc.h"

extern const elementID_t MTelem;// in ddbDevice.cpp

class MEE;

#define DEFAULT_COMPATABILITY dm_Standard		/* dm_275compatible */

/*######### systemLog destination channels ##############*   /
use logging.h
#define CERR_LOG	0x01
#define CLOG_LOG	0x02
#define COUT_LOG	0x04
#define UI_LOG		0x08
#define STAT_LOG    0x10   / * status Bar Update * /
#define TRAC_LOG    0x20   / * TRACE * /

/     * future *
#define CRITLOG		0x040 32
#define USRACK_LOG	0x080 64
#define USRCCNLLOG	0x100 128
***********/


typedef struct dmdate_s
{
	unsigned char  day;
	unsigned char  mnth; 
	unsigned short year;
}dmDate_t;


typedef enum dState_e 
{ 
	ds_OffLine,		/* 0 */
	ds_OnLine,		/* 1 */
	ds_OffIniting,	/* 2 - offline & Initializing */
	ds_OnIniting	/* 3 -  online & Initializing */
,	ds_Closing      /* 4 - Set in CloseDevice()   */
} dState_t;


// This is an operating mode
typedef enum OperatingMode_e // bit-enum so we can add modes later
{ 
	om_Nothing,		/* 0 */
	om_DownLoading 	/* 1 */// we want critical params AND all the criticals set STALE
} OperatingMode_t;


class hCmethod;
class CMenuDlg; 
class CMethodDlg;
typedef enum methodCallSource_e
{
	msrc_UNKNOWN,	// 0
	msrc_ACTION,	// called from a pre/post action
	msrc_EXTERN,	// called from an external location(usually the UI)
	msrc_METHOD		// future method calling a method
   ,msrc_CMD_ACT	// action from inside send()/srvcRcvPkt() ie pre/post read/writes
}/*typedef*/ methodCallSource_t;

// defines for method calls
typedef enum mState_e 
{ 
	mc_NoTExist,	/* 0 */
	mc_Initing,		/* 1 */
	mc_Running,		/* 2 */
	mc_Closing      /* 3 */
} mState_t;

class hCmethodCall
{
public:
	unsigned methodThreadID;
	itemID_t methodID;
	hCmethod* m_pMeth;
	methodCallSource_t    source;  // what kind of call this is
	vector<CValueVarient> paramList;
	// stevev 27jan06 - track the used dialogs on the stack
	CMethodDlg* m_pMethodDlg;
	CMenuDlg*   m_pMenuDlg;	

	mState_t    m_MethState;

	hCmethodCall():methodID(0),source(msrc_UNKNOWN),m_pMeth(NULL),m_pMethodDlg(NULL),
									m_pMenuDlg(NULL),m_MethState(mc_NoTExist),methodThreadID(0)
		{paramList.clear();};
	virtual ~hCmethodCall(){clear();};
	hCmethodCall(const hCmethodCall& src){operator=(src);};
	hCmethodCall& operator = (const hCmethodCall& src)
		{methodID = src.methodID; source = src.source; paramList = src.paramList;
	     m_pMeth  = src.m_pMeth;  m_pMethodDlg = src.m_pMethodDlg; m_pMenuDlg = src.m_pMenuDlg;
		 m_MethState = src.m_MethState; methodThreadID = src.methodThreadID; return *this;};
	   //VMKP
	void clear(void){methodID=0;source=msrc_UNKNOWN;
			if(paramList.size())
				paramList.clear();m_MethState = mc_NoTExist;
			m_pMeth=NULL;m_pMethodDlg=NULL;m_pMenuDlg=NULL;};
};

typedef vector<hCmethodCall>  methodCallList_t;

//The interface to sendMethodCmd will be defined such that host errors will be returned in a 
//bit-mapped return code from the method.  
#define NO_RESPONSE  0x80
#define HOST_PARITY  0x40
#define HOST_OVERRUN 0x20
#define HOST_FRAMING 0x10
#define HOST_CHKSUM  0x08
#define HOST_GAP     0x04
#define HOST_BUFFER  0x02
#define HOST_PKT_FRAMING 0x01
#define HOST_SUCCESS 0x00  
//  It is expected that this interface will only return NO_RESPONSE or HOST_SUCCESS in the beta 
//  version.
/////////////////////////////////////////////////////////////////////////////////////////////////

// external references
class   hCitemBase;

class hCreference;
class hCpubsub;
class hCfile;

class hCcritical;

// forward references
class hCobject;

/* * * notice that this is a pure virtual base class! * * */
#ifdef NDEBUG
#pragma warning( push )
#pragma warning ( disable : 4172 )
#endif

class hCdeviceSrvc
{
public:     /* this is the ONLY public interface to the device itself */
//assume uneeded until proven otherwise
//	virtual 
//		RETURNCODE InitComm(void)                 = 0;  

	virtual 
		RETURNCODE getItemBySymNumber(symbolNumb_t synNum, hCitemBase** ppReturnedItempointer)RPVFC( "hCdeviceSrvc_a",0 );
	virtual 
		RETURNCODE getItemBySymName  (    const string& symName, hCitemBase** ppReturnedItempointer)RPVFC( "hCdeviceSrvc_b",0 );

	/* * * * * Synchronous * * * * * */
	virtual 
		RETURNCODE ReadImd(itemID_t           itemID,   CValueVarient& ppReturnedDataItem,
							bool isAtest=false) RPVFC( "hCdeviceSrvc_c",0 );
	virtual 
		RETURNCODE ReadImd(hCitemBase*        itemPtr,  CValueVarient& ppReturnedDataVal, 
							bool isAtest=false) RPVFC( "hCdeviceSrvc_d",0 );

	virtual 
		RETURNCODE WriteImd(itemID_t          itemID,   CValueVarient* dataVal) RPVFC( "hCdeviceSrvc_e",0 );// -> to RTDB
	virtual 
		RETURNCODE WriteImd(hCitemBase*       itemPtr,  CValueVarient* dataVal) RPVFC( "hCdeviceSrvc_f",0 );// ->

	/* * * * * Asynchronous List * * * * * */
	virtual  // These Items will be marked stale and read from the device ASAP
		RETURNCODE Read(vector<hCitemBase*>&  itemList) RPVFC( "hCdeviceSrvc_g",0 ); // <- dev
	virtual  // the values are expected to be in the Var's display Val
		RETURNCODE Write(vector<hCitemBase*>& itemList) RPVFC( "hCdeviceSrvc_h",0 ); // -> dev

	virtual
		DD_Key_t    getDDkey(void) RPVFC( "hCdeviceSrvc_i",0 );
	virtual
		void      getStartDate(dmDate_t& dt) PVFC( "hCdeviceSrvc_j" );
	virtual
		Indentity_t& getIdentity(void) RPVFC( "hCdeviceSrvc_k",Indentity_t() );

	virtual // required by item constructors
		void      registerItem(itemID_t newItemID, hCitemBase* pNewItem, string& symName) PVFC( "hCdeviceSrvc_l" ); 
	virtual // required for handling psuedo items
		void      unRegister(hCitemBase* rmvItem) PVFC( "hCdeviceSrvc_m" );
	virtual // at not tokenizer, will do nothing
		void	  registerElement(unsigned long elemIdx, string& elemName, itemID_t grpID)PVFC( "hCdeviceSrvc_b1" );
	virtual 
		RETURNCODE getElemNameByIndex(unsigned long elemIdx, string& returnedName)  RPVFC( "hCdeviceSrvc_b2",-1 );
	// we could also getElemUsageByIndex(unsigned long elemIdx, itemIDlist_t& elemUserList); but not now...

	virtual // must be cast to proper list type ( like new() is done )
		hCobject* getListPtr(CitemType  it) RPVFC( "hCdeviceSrvc_n",NULL ); 
	virtual
		wstring  getLiteralString(unsigned stringIndex) RPVFC( "hCdeviceSrvc_ar",NULL ); 
	virtual
		hSdispatchPolicy_t getPolicy(void) RPVFC( "hCdeviceSrvc_o",hSdispatchPolicy_t() );
#ifdef _DEBUG
	virtual
		void     aquireItemMutex(char* fileNm, int LineNum) PVFC( "hCdeviceSrvc_p" );		
#else
	virtual
		void     aquireItemMutex(void) PVFC( "hCdeviceSrvc_p" );
#endif
	virtual
		void     returnItemMutex(void) PVFC( "hCdeviceSrvc_q" );	
	virtual
		void     notifyVarUpdate(itemID_t i,  NUA_t isChanged = NO_change, long aTyp = 0L) PVFC( "hCdeviceSrvc_r" );
	/* stevev 17jan05 - to support repaints - temporarily */
	virtual
		bool     isItemCritical(itemID_t  itm) RPVFC( "hCdeviceSrvc_s",false );// true if i in critical param list
	virtual
		hCcritical* getCriticalPtr(void) RPVFC( "hCdeviceSrvc_t",NULL );//stevev 20jan05

	virtual
		OperatingMode_t getOpMode(void) RPVFC( "hCdeviceSrvc_as", om_Nothing );//stevev 16feb12
	virtual
		void  setOpMode(OperatingMode_t g) PVFC( "hCdeviceSrvc_as" );//stevev 16feb12

//???	virtual RETURNCODE Retrieve(UINT32 itemID,ptr2hCitemBase_t& ptrReturned) = 0;
	  //read value and return the variable ptr
/*---22aug08---
	virtual
		void	systemLog(int channel, char* format, ...) PVFC( "hCdeviceSrvc_u" );
	virtual
		void	systemLog(int channel, int errNumber,...) PVFC( "hCdeviceSrvc_v" );
---------------*/
			//prashant oct 2003
	virtual hCMethSupport*   getMethodSupportPtr() RPVFC( "hCdeviceSrvc_w",NULL );

	virtual unsigned int getMEEdepth() RPVFC( "hCdeviceSrvc_x",0 ); 

	// METHOD HANDLING
	virtual
		RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue) RPVFC( "hCdeviceSrvc_y",0 );
	virtual
		RETURNCODE executeMethod(vector<hCmethodCall>& actionList,CValueVarient& returnValue) RPVFC( "hCdeviceSrvc_z",0 );
	virtual
		RETURNCODE executeMethod(vector<hCmethodCall>& actionList) RPVFC( "hCdeviceSrvc_aa",0 );

	//was:RETURNCODE executeMethod(vector<hCreference>& actionList, CValueVarient& returnValue) = 0;
	virtual RETURNCODE GetLastMethodsReturnCode(void) RPVFC( "hCdeviceSrvc_ab",0 ); // this returns the error code returnrd by the method

	// METHOD SUPPORT
	virtual			// note that the return value is only the first byte: bit encoded
		RETURNCODE sendMethodCmd(int commandNumber,int transactionNumber = 0,
			indexUseList_t* pI=NULL) RPVFC( "hCdeviceSrvc_ac",0 );//stevev added index 29nov11
	/* stevev - 2/17/04 - added to give external control of part the weighting */
	virtual			// note that the return value is only the first byte: bit encoded
		RETURNCODE sendCommand(int commandNumber,int transactionNumber = 0) RPVFC( "hCdeviceSrvc_ad",0 );// for cmd48
	virtual
		int queryWeight(void) RPVFC( "hCdeviceSrvc_ae",0 );
	/* end add stevev */
	virtual
		devMode_t whatCompatability(void) RPVFC( "hCdeviceSrvc_af", devMode_t());
	/* stevev 08nov04 - added file interface */
	virtual
		RETURNCODE fillme(hCfile* fileClass) RPVFC( "hCdeviceSrvc_ag",0 );
	virtual
		RETURNCODE saveme(hCfile* fileClass) RPVFC( "hCdeviceSrvc_ah",0 );
	/* end stevev 08nov04*/
	/* stevev 09-dec-04 add pub-sub interface */
	virtual 
		RETURNCODE subscribe(
					int& returnedHandle,			// in order to unsubscribe 
					ddbItemList_t subscribedList,	// list of variables we are subscribing to
					hCpubsub* pPublishTo,			// pointer to the callback function
					unsigned long cycleTime = 1000,	// requested update interval
					int cyclesPerPublish    = 1     // number of cycleTimes before call publish()
					) RPVFC( "hCdeviceSrvc_ai",0 );

	virtual RETURNCODE unsubscribe(int handle) RPVFC( "hCdeviceSrvc_aj",0 );

	/* end stevev 09dec04 */
	/* stevev 30mar05 */
	virtual  // psuedo Item generation - see notes in ddbDevice.h
		hCitemBase* newPsuedoItem(hCitemBase* pTypeDef, hCitemBase* pValueDef, 
				psuedoItemFlags_t si_Flags = sif_RegisterNew, elementID_t thisElement= MTelem)
				RPVFC( "hCdeviceSrvc_ak",NULL );
	virtual	// destroys-does not delete; unregisters it; removes it from the instance list
		RETURNCODE  destroyPsuedoItem(hCitemBase* pPsuedoItem) RPVFC( "hCdeviceSrvc_al",0 );
	/* end stevev 30mar05 */
	virtual
		itemID_t    getAPsuedoID(void) RPVFC( "hCdeviceSrvc_am", itemID_t()); // stevev 10aug06 - need for static var constructor
	virtual
		int  getDeviceState(void) RPVFC( "hCdeviceSrvc_an",0 ); // may return an enum in the future - Offline = 0; Online = 1 (4now)
	virtual/* stevev - 01nov06 we gotta calc images outside the device object */
		bool sizeImage(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInLn) RPVFC( "hCdeviceSrvc_ao",false );
	virtual/* stevev - 27mar07 we gotta calc strings outside the device object */
		bool sizeString(string* c_str, unsigned& xCols, unsigned& yRows, int hgtLim=(int)gS_Unknown) 
															RPVFC( "hCdeviceSrvc_ap",false );
	virtual/* stevev 28aug09 - to get the semi-unique application id from anywhere */
		__int64 getIDbase(void)  RPVFC( "hCdeviceSrvc_aq",false );

	virtual/* stevev 11apr11 - abort from anywhere..mainly no list element */
		void abortDD(int retCode, hCitemBase* pList)  PVFC( "hCdeviceSrvc_ar" );

	virtual/* stevev 10apr13 - returns true if the device is currently executing a method */
		bool isInMethod(void)RPVFC( "hCdeviceSrvc_as",false );
};

#ifdef NDEBUG
#pragma warning( pop )
#endif


// The global service interface is GUARANTEED not to return a NULL pointer!
// this class is returned in an error condition from the global service interface
class hCerrDeviceSrvc : public hCdeviceSrvc
{
	Indentity_t   ddbDeviceID;// leave this empty
public:
	hCerrDeviceSrvc();

	RETURNCODE getItemBySymNumber(itemID_t symNum, hCitemBase** ppReturnedItempointer);
	RETURNCODE getItemBySymName  (const string& symName, hCitemBase** ppReturnedItempointer);

	RETURNCODE ReadImd(itemID_t           itemID,   CValueVarient& ppReturnedDataItem,
							bool isAtest=false);
	RETURNCODE ReadImd(hCitemBase*        itemPtr,  CValueVarient& ppReturnedDataVal,
							bool isAtest=false);

	RETURNCODE WriteImd(itemID_t          itemID,   CValueVarient* dataVal);// -> to RTDB
	RETURNCODE WriteImd(hCitemBase*       itemPtr,  CValueVarient* dataVal);// ->

	RETURNCODE Read(vector<hCitemBase*>&  itemList);
	RETURNCODE Write(vector<hCitemBase*>& itemList); // ->
	
	DD_Key_t     getDDkey(void);
	void         getStartDate(dmDate_t& dt){dmDate_t r={0,0,0}; dt = r; return;};
	Indentity_t& getIdentity(void);

	// required by item constructors::
	void       registerItem(itemID_t newItemID, hCitemBase* pNewItem, string& symName); 
	void       unRegister(hCitemBase* rmvItem);
	void	   registerElement(unsigned long elemIdx,string& elemName, itemID_t grpID);
	RETURNCODE getElemNameByIndex(unsigned long elemIdx, string& returnedName);

	wstring    getLiteralString(unsigned stringIndex){return wstring(L"");};

	hCobject*  getListPtr(CitemType  it);//must be cast to proper list type(like new() is done)
	
	OperatingMode_t getOpMode(void) {return om_Nothing;};//stevev 16feb12
	void  setOpMode(OperatingMode_t g) {};//noop stevev 16feb12

	hSdispatchPolicy_t getPolicy(void);
#ifdef _DEBUG
	void     aquireItemMutex(char* fileNm, int LineNum);	
#else
	void     aquireItemMutex(void);	
#endif
	void     returnItemMutex(void);	
	void     notifyVarUpdate(itemID_t i,  NUA_t isChanged = NO_change, long aTyp = 0L);
	bool     isItemCritical(itemID_t  itm){ return false;};
	hCcritical* getCriticalPtr(void);//stevev 20jan05
/*
	void	 systemLog(int channel, char* format, ...) ;
	void	 systemLog(int channel, int errNumber,...) ;
*/
		//prashant oct 2003
	hCMethSupport*   getMethodSupportPtr();

	unsigned int getMEEdepth();

	RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue);
	RETURNCODE executeMethod(vector<hCmethodCall>& actionList, CValueVarient& returnValue);
	RETURNCODE executeMethod(vector<hCmethodCall>& actionList) ;
	RETURNCODE GetLastMethodsReturnCode(void);

	RETURNCODE sendMethodCmd(int commandNumber,int transactionNumber = 0, 
											indexUseList_t* pI=NULL);	// stevev added 30nov11
	/* stevev - 2/17/04 - added to give external control of part the weighting */
		RETURNCODE sendCommand(int commandNumber,int transactionNumber = 0);// for cmd48
	int queryWeight(void) {return 0;};
	/* end add stevev */
	/* stevev 08nov04 - added file interface */
	RETURNCODE fillme(hCfile* fileClass) { return FAILURE;};
	RETURNCODE saveme(hCfile* fileClass) { return FAILURE;};
	/* end stevev 08nov04*/
	devMode_t whatCompatability(void){return DEFAULT_COMPATABILITY;};
	/* stevev 09dec04 */
	RETURNCODE subscribe(int& returnedHandle,ddbItemList_t subscribedList,	hCpubsub* pPublishTo,			// pointer to the callback function
					unsigned long cycleTime = 1000,	int cyclesPerPublish = 1 )
	{returnedHandle = -1; return FAILURE;};
	virtual RETURNCODE unsubscribe(int handle){return FAILURE;};
	/* end stevev 09dec04 */
	hCitemBase* newPsuedoItem(hCitemBase* pTypeDef, hCitemBase* pValueDef, 
		psuedoItemFlags_t si_Flags = sif_RegisterNew, elementID_t thisElement= MTelem) 
	{ return NULL; };	//stevev 30mar05
	RETURNCODE  destroyPsuedoItem(hCitemBase* pPsuedoItem) { return FAILURE;}; // stevev 24jun05
	itemID_t    getAPsuedoID(void) { return 0;}; // stevev 10aug06

	int  getDeviceState(void) 
	{ 
		return 0;// split out for debugging PAW 27/07/09
	};// offline
	bool sizeImage(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInLn)
													{xCols=yRows=0; return true;};
	bool sizeString(string* c_str, unsigned& xCols, unsigned& yRows, int hgtLim=(int)gS_Unknown)
		{xCols=yRows=0; return true;};
	__int64 getIDbase(void)  {  return ((__int64)0L); };
	
	void abortDD(int retCode, hCitemBase* pList)  {return;};
	
	bool isInMethod(void) { return false;};

};


#endif//_DDBDEVICESERVICE_H

/*************************************************************************************************
* NOTES:
* 5/13/03 - Change of interface design
* The device  Read/Write Interface has been modified.  The methods are ReadImd/WriteImd for
* Read/Write immediate.  This is the Synchronous interface.  Calling these will not return until
* they have a value or error from the device.
* The HCVar Read/Write Interface is now the Asynchronous interface.  Calling these read/write 
* functions will return immediately, with or without a valid value. You can allow the 
* 'push interface' to notify you when they have been updated or you can try again later.
*
* The list handling Read/Write are Asynchronous ONLY.
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbDeviceService.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:17a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
