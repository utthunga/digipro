# Microsoft Developer Studio Project File - Name="ddbLib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ddbLib - Win32 StaticDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ddbLib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ddbLib.mak" CFG="ddbLib - Win32 StaticDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ddbLib - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ddbLib - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "ddbLib - Win32 StaticDebug" (based on "Win32 (x86) Static Library")
!MESSAGE "ddbLib - Win32 Release Static" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/DD Tools/DDB/ddbRead/ddbLib", NPLBAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ddbLib - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I ".\\" /I ".\inc" /I "..\\" /I "..\..\\" /I "C:\Work\CommonClasses\Arguments\include" /I "c:\tools\rdm50\include" /I "C:\Work\HART\DDDB" /I "C:\Work\HART\DDDB\common" /D "DBGHEAP" /D "INI_OK" /D "ISLIB  WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FD /Zm200 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ddbLib - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I "..\.." /I "..\..\common" /D "INI_OK" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "ISLIB" /D "DBGHEAP" /FR /FD /Zm200 /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ddbLib - Win32 StaticDebug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "ddbLib___Win32_StaticDebug"
# PROP BASE Intermediate_Dir "ddbLib___Win32_StaticDebug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "ddbLib___Win32_StaticDebug"
# PROP Intermediate_Dir "ddbLib___Win32_StaticDebug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I ".\\" /I ".\inc" /I "..\\" /I "..\..\\" /I "C:\Work\CommonClasses\Arguments\include" /I "c:\tools\rdm50\include" /I "C:\Work\HART\DDDB" /I "C:\Work\HART\DDDB\common" /D "INI_OK" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "ISLIB" /D "DBGHEAP" /FR /FD /Zm200 /GZ /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "c:\tools\rdm50\include" /I "C:\Work\HART\DDDB2" /I "C:\Work\HART\DDDB2\common" /D "INI_OK" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "ISLIB" /D "DBGHEAP" /FR /FD /Zm200 /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ddbLib - Win32 Release Static"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ddbLib___Win32_Release_Static"
# PROP BASE Intermediate_Dir "ddbLib___Win32_Release_Static"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "ddbLib___Win32_Release_Static"
# PROP Intermediate_Dir "ddbLib___Win32_Release_Static"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /I ".\\" /I ".\inc" /I "..\\" /I "..\..\\" /I "C:\Work\CommonClasses\Arguments\include" /I "c:\tools\rdm50\include" /I "C:\Work\HART\DDDB" /I "C:\Work\HART\DDDB\common" /D "DBGHEAP" /D "INI_OK" /D "ISLIB  WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FD /Zm200 /c
# ADD CPP /nologo /W3 /GX /O2 /I ".\\" /I ".\inc" /I "..\\" /I "..\..\\" /I "C:\Work\CommonClasses\Arguments\include" /I "c:\tools\rdm50\include" /I "C:\Work\HART\DDDB" /I "C:\Work\HART\DDDB\common" /D "DBGHEAP" /D "INI_OK" /D "ISLIB  WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FD /Zm200 /c
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "ddbLib - Win32 Release"
# Name "ddbLib - Win32 Debug"
# Name "ddbLib - Win32 StaticDebug"
# Name "ddbLib - Win32 Release Static"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\Common\dbg_dllApi.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCmdDispatch.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCollAndArr.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbCommand.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbdefs.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbDevice.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceMgr.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbEditDisp.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbItemBase.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbItemLists.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbPayload.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbPrimatives.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbReference.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbRfshUnitWaoRel.cpp
# End Source File
# Begin Source File

SOURCE=.\ddbVar.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\RdWrCommon\foundation.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\Common\ddb.h
# End Source File
# Begin Source File

SOURCE=.\ddbAttrEnum.h
# End Source File
# Begin Source File

SOURCE=.\ddbAttributes.h
# End Source File
# Begin Source File

SOURCE=.\ddbCmdDispatch.h
# End Source File
# Begin Source File

SOURCE=.\ddbCollAndArr.h
# End Source File
# Begin Source File

SOURCE=.\ddbCommand.h
# End Source File
# Begin Source File

SOURCE=.\ddbCommInfc.h
# End Source File
# Begin Source File

SOURCE=.\ddbConditional.h
# End Source File
# Begin Source File

SOURCE=.\ddbCondResolution.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbdefs.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbDevice.h
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceMgr.h
# End Source File
# Begin Source File

SOURCE=.\ddbDeviceService.h
# End Source File
# Begin Source File

SOURCE=.\ddbEditDisplay.h
# End Source File
# Begin Source File

SOURCE=.\ddbEvent.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbGeneral.h
# End Source File
# Begin Source File

SOURCE=.\ddbGlblSrvInfc.h
# End Source File
# Begin Source File

SOURCE=.\ddbItemBase.h
# End Source File
# Begin Source File

SOURCE=.\ddbItemLists.h
# End Source File
# Begin Source File

SOURCE=.\ddbItems.h
# End Source File
# Begin Source File

SOURCE=.\ddbMenu.h
# End Source File
# Begin Source File

SOURCE=.\ddbMethod.h
# End Source File
# Begin Source File

SOURCE=.\ddbMsgQ.h
# End Source File
# Begin Source File

SOURCE=.\ddbMutex.h
# End Source File
# Begin Source File

SOURCE=.\DDBpayload.h
# End Source File
# Begin Source File

SOURCE=.\ddbPolicy.h
# End Source File
# Begin Source File

SOURCE=.\ddbPrimatives.h
# End Source File
# Begin Source File

SOURCE=.\ddbReferenceNexpression.h
# End Source File
# Begin Source File

SOURCE=.\ddbResponseCode.h
# End Source File
# Begin Source File

SOURCE=..\..\common\ddbRfshUnitWaoRel.h
# End Source File
# Begin Source File

SOURCE=.\ddbSvcRcvPkt.h
# End Source File
# Begin Source File

SOURCE=.\ddbVar.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\DDLDEFS.H
# End Source File
# Begin Source File

SOURCE=..\..\Common\dllapi.h
# End Source File
# Begin Source File

SOURCE=..\..\common\Endian.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\foundation.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\HARTsupport.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\RdWrCommon\rtnCode.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\varient.h
# End Source File
# End Group
# End Target
# End Project
