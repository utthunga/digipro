/*************************************************************************************************
 *
 * $Workfile: ddbEvent.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the api to an OS specific event mechanism.
 *		The application must generate the code (usually by subclassing this class)
 *
 *		
 * #include "ddbEvent.h".
 */

#ifndef _DDBEVENT_H
#define _DDBEVENT_H

#include "ddbGeneral.h" // includes vector
#include "xplatform_thread.h"


//    designed to encapsulate the OS specific items- linked externally
class hCevent	  
{	
	int eventHandle;// private:  this is where the internal data is hidden

public:
	THREAD_ID_TYPE threadPendingOn;

	typedef enum EventResultEnum_e
	{
		ere_NoResult,
		ere_Success,
		ere_Abandoned,
		ere_PassedLast
	} EventResultEnum_t;
									// in lieu of a global #define <public so mutex can use it>
	static const unsigned long FOREVER; 
	void  clear(void){eventHandle=0;
#ifdef _DEBUG
	threadPendingOn = 0;
#endif
	};
	class hCeventCntrl	// one per task needing control of the event
	{	
		int controlHandle; // private - hidden
		EventResultEnum_t resultVal;

	public:
		hCeventCntrl(int h);

		// returns APP_WAIT_TIMEOUT at timeout || APP_WAIT_ERROR at error || SUCCESS 
		RETURNCODE pendOnEvent(unsigned long ulTimeOut = FOREVER );	// sytemWait for signal (mS time)
		RETURNCODE waitOnEvent(unsigned long ulTimeOut = FOREVER );	// WAIT for signal sjv 25aug05
		
		void triggerEvent(void);					// raise signal    
		void clearEvent(void);                      // clears signal without pending
		
		OS_HANDLE getEventHandle(void); // allows a WaitForMultipleEvents || systemWaitMultiple

		/* stevev 25jul05 - add return value info and support */
		void clrResult(void);
		void setResult(EventResultEnum_t newVal);
		EventResultEnum_t getResult(void);
	};

typedef  vector<hCeventCntrl*>	eventPtrList_t;// to allow wait for multiple events

public:	// ONE event per channel - each task using that event will need to getEventCntrl first

	hCevent();

	hCeventCntrl* getEventControl(void);		// executes control constructor 	
		
	virtual ~hCevent();

	/* 25jul05 - static to set the UI thread when it instantiates */
	static void setUIthreadID(DWORD uiThreadID);
};

/* 08sep09 - add thread-priority based critical section class  
 *
 *  enter critical section, bump priority way up - so no other task can modify data
 *  exit  critical section, restore priority
 */
struct CS;

class hCcriticalSection
{// private state
	THREAD_ID_TYPE threadID;
	THREAD_HANDLE_TYPE threadHandle;
	int entryPriority;

#if defined(__GNUC__)
	RecursiveMutex cs;
#else // not __GNUC__
	//CRITICAL_SECTION billsCS;
	CS        *pBillsCS;
#endif // __GNUC__

public:
	hCcriticalSection();
	virtual
		~hCcriticalSection();

	bool EnterCS(void);// true on yes, there was an error, false on NO error
	bool Exit_CS(void);// true on yes, there was an error, false on NO error
};


/* 25jul05 - add global, intelligent functions to pend/sleep with UI task implications 
 *			- the UI task may only pend while allowing the message pump to continue
 *			- these globally available functions will do the right thing under all
 *				circumstances
 */

DWORD systemWaitSingle(hCevent::hCeventCntrl* pEvt, DWORD mSecs);// replace WaitForSingleObject
DWORD systemWaitMultiple(hCevent::eventPtrList_t& evtList, DWORD mSecs);
 // replace Sleep
extern void  systemSleep(DWORD mSecs);	// built in ddb_EventMutex.cpp

#endif//_DDBEVENT_H

/*************************************************************************************************
* NOTES:
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbEvent.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:24a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
