/*************************************************************************************************
 *
 * $Workfile: ddbWaveform.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbWaveform.h".
 */

#ifndef _DDB_WAVEFORM_H
#define _DDB_WAVEFORM_H

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbConditional.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"

#include "ddbGrpItmDesc.h"
#include "sysEnum.h"
#include "grafix.h"
#include "GraphicInfc.h"

class hCgraph;

/*
	waveformAttrLabel			aCattrString
	waveformAttrHelp,			aCattrString 

	waveformAttrHandling,		aCattrBitstring
	waveformAttrEmphasis,		aCattrCondLong		
	waveformAttrLineType,		aCattrCondLong		
	waveformAttrLineColor,		aCattrCondExpr		
	waveformAttrYAxis,			aCattrCondReference
	waveformAttrKeyPtX,			aCattrReferenceList 
	waveformAttrKeyPtY,			aCattrReferenceList 
	waveformAttrType,			aCattrCondLong		- required		
	waveformAttrXVals,			aCattrReferenceList 
	waveformAttrYVals,			aCattrReferenceList 
	waveformAttrXinitial,		aCattrCondExpr
	waveformAttrXIncr,			aCattrCondExpr
	waveformAttrPtCount,		aCattrCondExpr
	waveformAttrInitActions,	aCattrActionList
	waveformAttrRfshActions,	aCattrActionList	
	waveformAttrExitActions,	aCattrActionList
*/


/***************************************************************************************
 * waveform Type enum declaration
 ***************************************************************************************
 */
/* memory location of strings */
extern const char waveTypeStrings[WAVETYPESTRCOUNT] [WAVETYPEMAXLEN]; // in ddbWaveform.cpp
/* this enum type::	  enum type,     zeroth string, highest vale,    array string length*/
typedef  hCsystemEnum<waveformType_t,hCddlLong,wT_Undefined,  wT_Unknown,  WAVETYPEMAXLEN>
		 waveType_t;

/* stream io for the enum */
inline ostream& operator<<(ostream& ostr,	waveType_t& it)\
{  ostr << it.getSysEnumStr();	return ostr; }	

/****************************************************************************************/

class hCattrRefList;

class hCwaveform : public hCitemBase 
{
	waveType_t			waveType;	// required & not allowed to be conditional
	hCattrLineType*		pLineType;	//waveformAttrLineType aCattrCondIntWhich 

	hCattrHndlg*		pHandling;	//waveformAttrHandling,	aCattrBitstring
	hCattrValid*        pEmphasis;	//waveformAttrEmphasis,	aCattrCondLong

	hCattrExpr*			pLineColor;	//waveformAttrLineColor aCattrCondExpr - NOT conditional just a numeric
	hCattrAxisRef*		pYaxis;		// waveformAttrYAxis,	aCattrCondReference

	hCattrRefList*      pXkeyPts;	// waveformAttrKeyPtX,	aCattrReferenceList (refs & consts)
	hCattrRefList*      pYkeyPts;	// waveformAttrKeyPtY,	aCattrReferenceList (refs & consts)
	
	hCattrRefList*      pXVals;		// waveformAttrXVals,	aCattrReferenceList 
	hCattrRefList*      pYVals;		// waveformAttrYVals,	aCattrReferenceList 
	hCattrExpr*			pPntCnt;	//waveformAttrPtCount,	aCattrCondExpr

	hCattrExpr*			pX_initial;	//waveformAttrXinitial,	aCattrCondExpr
	hCattrExpr*			pX_inc;		//waveformAttrXIncr,	aCattrCondExpr

	hCattrRefList*      pInitActs;	// waveformAttrInitActions,	aCattrActionList...isa aCattrReferenceList
	hCattrRefList*      pRfshActs;	// waveformAttrRfshActions,	aCattrActionList	
	hCattrRefList*      pExitActs;	// waveformAttrExitActions,	aCattrActionList

	/*Vibhor 111004: Start of Code*/

	hCreferenceList		m_KeyX; //Filled by getKeyPtsX()
	hCreferenceList		m_KeyY; //Filled by getKeyPtsY()

	hCreferenceList		m_XRefs; //Filled by getXValues()
	hCreferenceList		m_YRefs; //Filled by getYValues()
	
	varientList_t       m_XVars; // The points being displayed. 
	varientList_t       m_YVars; // These will be mixed constant values & item IDs

	RETURNCODE getResolvedRefList(hCattrRefList* pListAttr,hCreferenceList & RslvdList);

	unsigned getNumPoints();
	
	RETURNCODE getXInitial(CValueVarient &cv);

	RETURNCODE getXIncrement(CValueVarient &cv);

	/*Vibhor 111004: End of Code*/
	// stevev 01may07 - common code
	RETURNCODE resolvePoints(void); // one step in above

public:
	hCwaveform(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCwaveform(hCwaveform*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	RETURNCODE destroy(void){ return hCitemBase::destroy(); };
	virtual ~hCwaveform()  {m_KeyX.clear();m_KeyY.clear();/*TODO: see if we need a destructor*/};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);

	waveType_t getWaveSysEnum() { return waveType;}; // returns the SysEnum object
	
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(WAVEFORM_LABEL, retStr); };
	 // stevev - WIDE2NARROW char interface
	RETURNCODE Label( string &l) { return baseLabel(WAVEFORM_LABEL, l); };

	RETURNCODE Read(wstring &value){/* Waveforms are Not readable */ return FAILURE;};

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) 
	{	//COUTSPACE << "--------Waveform Item--------" << endl;
		hCitemBase::dumpSelf(indent,typeName);
		//COUTSPACE << "Waveform ID/Label:                  "   << itemId << "   ";
		// dump attributes here
		return SUCCESS;
	};

	/*Vibhor 081004: Start of Code*/

	
	
	waveformType_t getWaveFrmType(){return waveType.getSysEnum();};

	lineType_t getLineType() ;
	
	RETURNCODE getHandling(unsigned & uHdlg); 

	bool needsEmphasis();

	RETURNCODE getLineColor(unsigned & color);

	hCaxis* getYAxis(); //!!!! Generate Axis from Data if it returns NULL !!!!


	RETURNCODE 
		getPointList(pointValueList_t& pointList, ddbItemList_t& subLst);//getPointValueList()
																		 //   <update value>
	RETURNCODE 
		getNewPointList(pointValueList_t& pointList,ddbItemList_t& subLst);//aka re-resolve 
													//	& getPointValueList() <update structure>
	RETURNCODE getKeyPoints(pointValueList_t& keyPtList, ddbItemList_t& subLst);
	
	RETURNCODE doInitActions();

	RETURNCODE doRefreshActions();

	RETURNCODE doExitActions();
		
	/*Vibhor 081004: End of Code*/
	bool  isThisApoint(itemID_t iid);// if item is listed in any x or y of an EXISTING point
	int   ItmLstappendUnique(ddbItemList_t& subLst, hCitemBase* pItem);

	/*Vibhor 071204: Start of Code*/	
	//This function returns the actions as itemBase pointer list for execution from methods !!
	RETURNCODE getActionList(actionType_t actionType,ddbItemList_t& actionList);
	/*Vibhor 071204: End of Code*/	

	/*Vibhor 201204: Start of Code */
/*These interfaces return the current - resolved references whose values are on the plot 
  at the given moment Note that these references get "refreshed" on every waveform refresh*/
	hCreferenceList & getCurrentXRefs(){return m_XRefs;};
	hCreferenceList & getCurrentYRefs(){return m_YRefs;};

	void markDynStale();// stevev 10may07 - to get dynamic updates

	/*Vibhor 201204: End of Code */
	/* stevev 11sep06 - add dependency calcs */
	void   fillCondDepends(ddbItemList_t&  iDependOn);
	/* stevev 11sep06 - End of add */
	void   fillContainList(ddbItemList_t&  iContain);//stevev 27sep06

	int    notification(hCmsgList& msgList,bool isValidity, hCgraph* pParent);

};

#endif	// _DDB_WAVEFORM_H
