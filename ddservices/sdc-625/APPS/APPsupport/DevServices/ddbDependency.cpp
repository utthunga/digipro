/*************************************************************************************************
 *
 * $Workfile: ddbDependency.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		method code for the dependency class 
 *		20jan05	sjv	 creation
 */

#include "ddbDependency.h"
#include "logging.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "ddbCritical.h"


// stevev 21apr08 - detect circular dependencies::
ddbItemList_t globalDependencyTrail;  // list of pointers to items

//public:  static
RETURNCODE 
hCdependency::insertUnique(ddbItemList_t& insert2List, hCitemBase* p2insert)
{
	if (p2insert == NULL) return FAILURE;

	ddbItemLst_it foundI;
	for(foundI = insert2List.begin(); foundI!=insert2List.end(); ++foundI)
	{
		if ( p2insert->getID() == (*foundI)->getID() )//IDs MUST be unique in the devobj
			break;// exit before found == end()
	}
	if (foundI == insert2List.end())
	{
		insert2List.push_back(p2insert);
	}// else it already exists, don't duplicate
	return SUCCESS;
}
RETURNCODE	/* uniquely insert all items in list to destlist */
hCdependency::insertUnique(ddbItemList_t& insert2List, ddbItemList_t& l2insert)
{
	if (l2insert.size() <= 0 ) return SUCCESS;

	itemID_t id2insert = 0;
	ddbItemLst_it foundI, fromI;
	for(fromI = l2insert.begin(); fromI != l2insert.end(); ++fromI)
	{// for each 2 b inserted
		if ((*fromI) == NULL )// defensive
			continue;
		id2insert = (*fromI)->getID();
		for(foundI = insert2List.begin(); foundI!=insert2List.end(); ++foundI)
		{// see if already there
			if ( id2insert == (*foundI)->getID() )//IDs MUST be unique in the devobj
				break;// exit before found == end()
		}// next to item to check
		if ( foundI == insert2List.end() )
		{
			insert2List.push_back(*fromI);
		}// else it already exists, don't duplicate
	}// next from item
	return SUCCESS;
}


hCdependency::hCdependency(DevInfcHandle_t h) : hCobject(h),pOwner(NULL)
{	didValidity = didConditional = false;
}

hCdependency::hCdependency(hCitemBase* owner) : hCobject(owner->devHndl()),pOwner(owner)
{	didValidity = didConditional = false;
}

int hCdependency::fillIContainList(void)	// returns size
{
	if ( pOwner == NULL )
	{
		iDependOn.clear();
		return 0;
	}

	if (iDependOn.size() > 0 )// we are using iDependOn to hold the iContain info
	{
		pOwner->fillContainList(iDependOn);
	}

	return (iDependOn.size());
}


int hCdependency::fillConditionalList(void)	// returns size
{
	if ( pOwner == NULL )
	{
		iDependOn.clear();
		return 0;
	}
#ifdef _DEBUG
if (pOwner->getID() == 0x4075)
{
	LOGIT(CLOG_LOG,"got bp.{dbg only msg}\n");
}
#endif
	if (! didConditional)
	{
		pOwner->fillCondDepends(iDependOn);// item's non-validity dependency list
		didConditional = true;
	}
	return (iDependOn.size());
}

int hCdependency::fillValidityList(void)	// returns size
{
	if ( pOwner == NULL || ( pOwner->pValid == NULL && ! didValidity))
	{
		iDependOn.clear();
		return 0;
	}
	// check trail	- stevev 21apr08
	ddbItemList_t::iterator ppILit, ppIt;
	hCitemBase* pIB = NULL;
	int c = 0;
	for (ppILit = globalDependencyTrail.begin();ppILit!=globalDependencyTrail.end();++ppILit)
	{	pIB = (hCitemBase*)(*ppILit);
		if (pOwner->getID() == pIB->getID() )
		{
			LOGIF(LOGP_NOT_TOK)(CERR_LOG|CLOG_LOG|UI_LOG,"ERROR: Circular dependency detected in "
				"%s.\n",pOwner->getName().c_str());
			for (ppIt = globalDependencyTrail.begin();
			     ppIt!=globalDependencyTrail.end();   ++ppIt)
			{	pIB = (hCitemBase*)(*ppIt);
				if ( c == 0 )
				{
					LOGIT(CERR_LOG|CLOG_LOG,"       %s",pIB->getName().c_str());
				}
				else
				{
					LOGIT(CERR_LOG|CLOG_LOG," to %s",pIB->getName().c_str());
				}
				c++;
			}
			LOGIT(CERR_LOG|CLOG_LOG,"\n");
			return (iDependOn.size());
		}
	}
	globalDependencyTrail.push_back(pOwner);
	// end 21apr08 trail test
	if (! didValidity && pOwner->pValid != NULL)
	{
		pOwner->pValid->getDepends(iDependOn);// validity attr's dependency list
		didValidity = true;
	}
	// 21apr08 trail test
#ifdef _DEBUG
	if (globalDependencyTrail.back()->getID() != pOwner->getID() )
	{
		assert( 1 == 0 );
	}
#endif
	globalDependencyTrail.pop_back();
	// 21apr08 trail test
	return (iDependOn.size());
}

void hCdependency::makeDependTree(ddbItemList_t& behind, ibWhichDependency_t depType, int depth)
{
	RETURNCODE rc = SUCCESS;
	hCitemBase*   pItem = NULL;
	if ( pOwner == NULL )   return ;
	itemID_t thisItemID = pOwner->getID();


	hCcritical* pCrit = pOwner->devPtr()->getCriticalPtr();

LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"-0x%04x ",thisItemID);//pOwner->getName(); // symbol name as string
//LOGIT(CLOG_LOG,"%d.0x%04x",depth,thisItemID);//pOwner->getName(); // symbol name as string
	ddbItemLst_it i;
	hCdependency* pDep = NULL;

	for ( i = behind.begin(); i != behind.end(); ++ i)
	{// ptr2aPtr2a hCitemBase
		if ( thisItemID == (*i)->getID() )
		{
			LOGIT(CLOG_LOG,  "***************************************************\n"
				             "*********************** CIRCULAR ******************\n"
							 "***************************************************\n");
			LOGIT(CLOG_LOG,"%s (0x%04x) Depends on:\n",	pOwner->getName().c_str(), thisItemID);
			hCitemBase*   pI;
			int L = behind.size();
			int y;
			if ( L > 0 ) L -= 1; 
			for (y = L; y >= 0; y--)
			{
				pI = (hCitemBase*) behind[y];
				if ( y == 0 )// last one
				{
				LOGIT(CLOG_LOG,"%s (0x%04x)\n",	pI->getName().c_str(), pI->getID());
				}
				else
				{
				LOGIT(CLOG_LOG,"%s (0x%04x) Depends on:\n",	pI->getName().c_str(), pI->getID());
				}
			}
			LOGIT(TOKERR," Circular dependency found.");
			return;
		}
	}
	if (dependsOnMe.size() > 0)
	{
		if (!( pOwner->IsVariable() )) // I am not a variable
		{// somebody is using a NON-VARIABLE in a condition?
#ifdef _DEBUG
			LOGIT(CLOG_LOG,"\n-------The Non-Variable %s (0x%04x) is dominant to:",
				pOwner->getName().c_str(),pOwner->getID() );
			// for each
			for(ddbItemLst_it ipIB = dependsOnMe.begin(); ipIB != dependsOnMe.end(); ++ipIB)
			{// isa ptr 2a ptr 2a hCitemBase
				LOGIT(CLOG_LOG,"\n           %s  %s (0x%04x) ",(*ipIB)->getTypeName(),
				(*ipIB)->getName().c_str(),(*ipIB)->getID() );
			}
			LOGIT(CLOG_LOG,"\n------- end non-variable dependents --------\n");  
#else
			LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"\nThe Non-Variable %s (0x%04x) is dominant.?\n",
				pOwner->getName().c_str(),pOwner->getID() );
#endif
			return;
		}
		
		// curse again
		string o, s("       |");
		for (int y = 0; y <= (depth); y++)
		{
			o += s;
		}
		//o += "|      |";
		if ( pCrit != NULL )// only critical if others depend it
		{
			pCrit->insertCriticalItem(pOwner, depth);
		}

		behind.push_back(pOwner);
		for (i = dependsOnMe.begin(); i != dependsOnMe.end(); ++i)
		{// ptr2aPtr2a hCitemBase			
			if ( i != dependsOnMe.begin())
			{
				LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"%s",o.c_str());
			}
			pDep = (*i)->getDepPtr(depType);
			pDep->makeDependTree(behind, depType, depth+1 );
//			LOGIT(CLOG_LOG,"\n");
		}
		behind.pop_back();
	}
	else
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"\n");
	}
}


hCdependency& hCdependency::operator=(const hCdependency& s)
{
	pOwner = NULL;

	iDependOn   = s.iDependOn; 
	dependsOnMe = s.dependsOnMe ;

	return *this;
}

/* stevev 14sep06 - changed to consolidate depends into the common dependency.*/
RETURNCODE  hCdependency::informDependents(ibWhichDependency_t depType)
{
	RETURNCODE rc = SUCCESS;
	if ( pOwner == NULL ) return APP_PROGRAMMER_ERROR;
	// tell each of those that I depend on 
	ddbItemLst_it ppIdepend;
	hCdependency* pDep = NULL;
	hCdependency* pCom = NULL;// stevev 14sep06

	if ( iDependOn.size() > 0 )
	{
		LOGIF(LOGP_DEPENDENCY)(CLOG_LOG,"D:0x%04x:  ",pOwner->getID());

//      for each one that this var depends on    			
		for (ppIdepend  = iDependOn.begin(); ppIdepend != iDependOn.end(); ++ppIdepend)
		{// ptr 2a ptr 2a hCitemBase
			pDep = (*ppIdepend)->getDepPtr(depType);
			pCom = (*ppIdepend)->getDepPtr(ib_Common);// stevev 14sep06
//			insert this Var (uniquely) into that dependsonme
			if (!(pDep->itemDependsOnMe(pOwner)))// does not already depend on me
			{
LOGIF(LOGP_DEPENDENCY)(CLOG_LOG," 0x%04x ",(*ppIdepend)->getID());
	
				//(*ppIdepend)->validityDepends.dependsOnMe.push_back(pOwner);
				pDep->dependsOnMe.push_back(pOwner);
			}
			// stevev 28aug06 - only merge validity & attributes, not containers (only for notification)
			if (depType == ib_ValidityDep || depType == ib_CondDep )
			{
			// stevev 28sep06 -- end
				// stevev 14sep06 -----
				if (!(pCom->itemDependsOnMe(pOwner)))// does not already depend on me
				{
					pCom->dependsOnMe.push_back(pOwner);
				}
				// stevev 14sep06 ----- end
			// stevev 28aug06 - 
			}
			// stevev 28sep06 -- end
		}// next one i depend on
LOGIF(LOGP_DEPENDENCY)(CLOG_LOG,"\n");
	}

	return rc;
}



bool hCdependency::itemDependsOnMe(hCitemBase* pI)
{
	if (pI == NULL || pOwner == NULL )
	{
		return false;
	}

	itemID_t iTarget = pI->getID();
	ddbItemLst_it ppIDOm;

	for (ppIDOm = dependsOnMe.begin(); ppIDOm != dependsOnMe.end(); ++ppIDOm)
	{
		if (iTarget == (*ppIDOm)->getID())
		{
			return true;
		}// else keep looking
	}
	return false;
}

bool hCdependency::iDependOnItem(  hCitemBase* pI)
{
	if (pI == NULL || pOwner == NULL )
	{
		return false;
	}

	itemID_t iTarget = pI->getID();
	ddbItemLst_it ppIDOm;

	for (ppIDOm = iDependOn.begin(); ppIDOm != iDependOn.end(); ++ppIDOm)
	{
		if (iTarget == (*ppIDOm)->getID())
		{
			return true;
		}// else keep looking
	}
	return false;
}


#ifdef _DEBUG
void hCdependency::clogDependency(void)
{
	ptr2hCitemBase_t pItm = NULL;
	ddbItemLst_it itmIT;

	string n("");

	if ( didValidity )
	{		
		n = "Validity";
	}
	if ( didConditional )
	{
		n += "Conditional";
	}
	LOGIT(CLOG_LOG,"Dependency for 0x%04x %s----------\n",
		  pOwner->getID(), pOwner->getName().c_str());

	if (iDependOn.size() <= 0 )
	{
		LOGIT(CLOG_LOG,"    %s Depends on no Item.\n",n.c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"    %s Depends on the following:\n",n.c_str());
		for ( itmIT = iDependOn.begin(); itmIT != iDependOn.end(); ++itmIT)
		{//ptr2ptr2itmbase
			pItm = (*itmIT);
			LOGIT(CLOG_LOG,"        0x%04x %s\n",pItm->getID(), pItm->getName().c_str());
		}
	}
	if (dependsOnMe.size() <= 0 )
	{
		LOGIT(CLOG_LOG,"    %s Has NO dependents.\n",n.c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"    %s The following depend on me:\n",n.c_str());
		for ( itmIT = dependsOnMe.begin(); itmIT != dependsOnMe.end(); ++itmIT)
		{//ptr2ptr2itmbase
			pItm = (*itmIT);
			LOGIT(CLOG_LOG,"        0x%04x %s\n",pItm->getID(), pItm->getName().c_str());
		}
	}
	LOGIT(CLOG_LOG,"-------- End Dependency for 0x%04x----------\n",  pOwner->getID());
}
#endif
/* end stevev 19jan05 */