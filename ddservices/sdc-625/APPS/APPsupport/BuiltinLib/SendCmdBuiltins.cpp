//#include "stdafx.h"
//#include <windows.h>
#include "ddbGeneral.h"
//#include <ALPHAOPS.H>
#include <limits.h>
#include <float.h>
#include "ddbDevice.h"
#include "Interpreter.h"
#include "Hart_Builtins.h"
#include "Delay_Builtin.h"
#include "MethodInterfaceDefs.h"
#include "MEE.h"
#include "messageUI.h"

#ifndef _USE_ORIG_SEND_CMD_
int	CHart_Builtins:: SEND_COMMAND(
			
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int  iCmdType
			, bool bEnableCmd48IfRequired	/* enable cmd48 sending */
			, int& iMoreInfoSize
			)
{
	int iRetVal = BI_SUCCESS; /*Return value of this function*/
	int iSendMethCmdRetCode = SUCCESS; /*Return value from the SendMethCmd for NORMAL_COMMAND*/
	int iSendMethCmdRetCode48 = FAILURE;/*Return value from the SendMethCmd for COMMAND_48*/
	bool bRetry=false; /*Main loop controlling var*/
	bool bRetryAsked=false;/*Flag set from the retry/abort masks for the method execution*/
	bool bMoreStatusAvailable=false;/*Flag to indicate there's more status available*/
	int  iXmtrDataCount=0;/*Byte count for Cmd 48 reply*/
#define isCmd48  (iCommandNumber==48)
	int iRetryCount = 0; /*Command Retry Counter*/

	int i=0;

	varPtrList_t itemList ;
	
	hCitemBase* pIB = NULL;
	CValueVarient ValueNeeded;
	hCVar *pLocalVar = NULL;

	//int iRc;
	/*Initialize the ResponseStatus array*/

	pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
	pchResponseStatus[STATUS_COMM_STATUS]   = 0;
	pchResponseStatus[STATUS_DEVICE_STATUS] = 0;

	/*Initialize the MoreDataStatus (Cmd 48) array*/
/*Vibhor 120204: Start of Code*/

/*There was copy paste problem!! */

	pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
	pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
	pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;

/*And this initialization was missing */

	for(i = 0 ; i < MAX_XMTR_STATUS_LEN ; i++)
	{
		pchMoreDataInfo[i] = 0;
	}

/*Vibhor 120204: End of Code*/
	do
	{
		bRetry = false;
		bRetryAsked = false;
		bMoreStatusAvailable = false;
		iRetVal = BI_SUCCESS;

		if(iCmdType == NORMAL_CMD)// all but get_more_status
		{
			iSendMethCmdRetCode = m_pDevice->sendMethodCmd(iCommandNumber,iTransNumber);

			if(iSendMethCmdRetCode & HOST_INTERNALERR)	//As per Steve anil on 3 November 2005
				/* stevev 03dec14 - This function can only return:
					HOST_INTERNALERR - command not found or no dispatcher instantiated
					HOST_NORESPONSE  - retries all done, no device
					SUCCESS
					other bits may be set but they are just giving finer grain detail from these
				***/
			{	/*Command_Not_Implemented*/
				pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
				pchResponseStatus[STATUS_COMM_STATUS]   = 0;
				pchResponseStatus[STATUS_DEVICE_STATUS] = 0;
				//Anil added to Fix Par 5573 , command not implemented  is send
				tchar pchString[1024]={0};
				_tstrcpy(pchString,M_METHOD_SEND_ERROR);
				acknowledge(pchString, NULL, 0);	
				// no use sticking around - command not implemented in DD or no dispatcher
				return BI_ERROR;// command not found or dispatcher is missing
			}			
			else
			/*  to get here we have HOST_NORESPONSE or SUCCESS */
			{
				if(m_pDevice->getItemBySymNumber(DEVICE_RESPONSECODE, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204
					
					pchResponseStatus[STATUS_RESPONSE_CODE] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_RESPONSECODE*/

				if(m_pDevice->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204

					int itemp = (int)ValueNeeded;

					pchResponseStatus[STATUS_COMM_STATUS] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_COMM_STATUS*/

				if(m_pDevice->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204

					int itemp = (int)ValueNeeded;

					pchResponseStatus[STATUS_DEVICE_STATUS] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_COMM48_STATUS*/

					
				if (isCmd48 )				// DDHT expects the data test to be on ANY command 48
				{
					bMoreStatusAvailable  = true;
					iSendMethCmdRetCode48 = iSendMethCmdRetCode;
				}

			}/* End else*/
		}/*Endif NORMAL_CMD*/
		else
		/* only not NORMAL_CMD in get_more_status() builtin
		 *
		 * pretend we sent the command so the more status command
		 * can be issued
		 */
		{
			iSendMethCmdRetCode = SUCCESS;

			pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
			pchResponseStatus[STATUS_COMM_STATUS]   = 0;
			pchResponseStatus[STATUS_DEVICE_STATUS] = 0;

			bEnableCmd48IfRequired = true;// should already be set
		}/*End else*/

		/* iSendMethCmdRetCode is unchanged...
					HOST_INTERNALERR - this is gone---
					HOST_NORESPONSE  - retries all done, no device response
					SUCCESS
		 */
		if(iSendMethCmdRetCode == SUCCESS) // ie iSendMethCmdRetCode != HOST_NORESPONSE
		{// send command 48 as required		
			if(iCmdType == NORMAL_CMD && ! isCmd48)// stevev 08dec15 - added isCmd48 for DDHT
			{
				bMoreStatusAvailable  = false;// redundant
				iSendMethCmdRetCode48 = SUCCESS;
			}/*Endif iCmdType*/

			if ( (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_DEVMALFUNCTION) != 0 &&
				 (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_MORESTATUSAVAIL) == 0 )
			{
				LOGIT(CERR_LOG,"DEVICE ERROR: More Status Available is required whenever there is a Device Malfunction.\n");
				pchResponseStatus[STATUS_DEVICE_STATUS] |= DS_MORESTATUSAVAIL;
				// set it here to make the code work
			}
			if(  (iCmdType == MORE_STATUS_CMD) // always send cmd 48
				 || 
				 ( (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_MORESTATUSAVAIL) != 0 // bit set
				   &&																   // and
				   (bEnableCmd48IfRequired == true)									   // 48 allowed
				  )
			  )
			{/*Send Command 48*/

				iSendMethCmdRetCode48 = 
							   m_pDevice->sendMethodCmd(48,DEFAULT_TRANSACTION_NUMBER);
				bMoreStatusAvailable = true;

				/* stevev 03dec14 - This function can only return:
					HOST_INTERNALERR - command not found or no dispatcher instantiated
					HOST_NORESPONSE  - retries all done, no device
					SUCCESS
					other bits may be set but they are just giving finer grain detail from these
				***/

				if(iSendMethCmdRetCode48 & HOST_INTERNALERR)
				{	/*Command_Not_Implemented - or internal screwup...assume the former */
					pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
					pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
					pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;
				}/*Endif DEVICE_NO_COMMANDCLASS*/
				else 
				/*  must be
				HOST_NORESPONSE  - SUCCESS
				**/
				{
					if(m_pDevice->getItemBySymNumber(DEVICE_RESPONSECODE, &pIB) == SUCCESS)
					{
						pLocalVar = (hCVar*)pIB;
						ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204
						
						pchMoreDataStatus[STATUS_RESPONSE_CODE] = (char)((int)ValueNeeded);
						
					}/*Endif DEVICE_RESPONSECODE*/

					if(m_pDevice->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB) == SUCCESS)
					{
						pLocalVar = (hCVar*)pIB;
						ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204

						int itemp = (int)ValueNeeded;

						pchMoreDataStatus[STATUS_COMM_STATUS] = (char)((int)ValueNeeded);
						
					}/*Endif DEVICE_COMM_STATUS*/

					if(m_pDevice->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB) == SUCCESS)
					{
						pLocalVar = (hCVar*)pIB;
						ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();Vibhor 270204

						int itemp = (int)ValueNeeded;

						pchMoreDataStatus[STATUS_DEVICE_STATUS] = (char)((int)ValueNeeded);
						
					}/*Endif DEVICE_COMM48_STATUS*/
				}/* End else*/						
			}/*Endif send command 48 */

			if(bMoreStatusAvailable == true )// we sent command 48
			{
				if(iSendMethCmdRetCode48 == SUCCESS)// not  HOST_INTERNALERR nor HOST_NORESPONSE
				{
					hCcommand *pCommand = NULL; // WS:EPM 24may07
					CCmdList *ptrCmndList = (CCmdList *)m_pDevice->getListPtr(iT_Command);
					// WS:EPM 24may07  hCcommand *pCommand = ptrCmndList->getCmdByNumber(48); 
					if (ptrCmndList ) 
                    { 
						pCommand = ptrCmndList->getCmdByNumber(48); 
                    } // end  WS:EPM 24may07

					if(pCommand)
					{							
						hCtransaction *pTrans =	pCommand->getTransactionByNumber(DEFAULT_TRANSACTION_NUMBER);
						
						if(pTrans)
						{
/*Vibhor 120204: Start of Code*/
/*Fix for PAR 5326 : Earlier pTrans->GetReplyList() was being used, which only returned
the lits of valid var ptrs in the reply packet, but here we want the whole byte string
of the 	reply */
							hPkt tempPkt;
							tempPkt.clear();
							if(pTrans->generateReplyPkt(&tempPkt) == SUCCESS)
							{
								if(tempPkt.dataCnt > 2)
								{
									for( i= 2;((i < tempPkt.dataCnt) && (i < MAX_XMTR_STATUS_LEN)) ;i++)
									{
										pchMoreDataInfo[i-2] =	tempPkt.theData[i];

									}/*Endfor*/
									iMoreInfoSize  =
									iXmtrDataCount = tempPkt.dataCnt - 2;
								}

								else 
								{
									iMoreInfoSize  =		// stevev 20nov14 - make it symetrical with if
									iXmtrDataCount = 0;
								}

							}/* Endif got reply packet */
						}/*Endif got transaction pTrans*/
					}/*Endif got command  pCommand*/
				}/*Endif iSendMethCmdRetCode48 == SUCCESS*/
				else
				// it's  HOST_INTERNALERR or HOST_NORESPONSE
				if(iSendMethCmdRetCode48 & HOST_NORESPONSE)
				{
					if(m_pMeth->m_byXmtrReturnNodevAbortMask){
						
						iRetVal = BI_ABORT;
					}
					else if(m_pMeth->m_byXmtrReturnNodevRetryMask){

						bRetryAsked = true;
					}
					else
					{ 
						iRetVal = BI_NO_DEVICE;
					}
				}/*Endif HOST_NORESPONSE*/
				else // it's HOST_INTERNALERR; command not supported or no dispatcher
				{
					iRetVal = BI_ERROR;
				}
			}/*Endif we sent command 48
			else we do nothing with the command 48 variables  */
		}/*Endif we sent the command OK.....iSendMethCmdRetCode == SUCCESS*/			
		else 
		/*  command not sent ok
		    iSendMethCmdRetCode is 
				HOST_NORESPONSE  - retries all done, no device response
		**/
		if(iSendMethCmdRetCode & HOST_NORESPONSE)// only one possible
		{
			bool Amask = (isCmd48)? m_pMeth->m_byXmtrReturnNodevAbortMask  :m_pMeth->m_byReturnNodevAbortMask;
			bool Rmask = (isCmd48)? m_pMeth->m_byXmtrReturnNodevRetryMask  :m_pMeth->m_byReturnNodevRetryMask;
			if(Amask)
			{				
				iRetVal = BI_ABORT;
			}
			else 
			if(Rmask)
			{
				bRetryAsked = true;
			}
			else
			{ 
				iRetVal = BI_NO_DEVICE;
			}
		}/*Endif HOST_NORESPONSE*/
		else 	
		{// any unknown or newly added return codes
			DEBUGLOG(CLOG_LOG|CERR_LOG,"ERROR: Method SEND_COMMAND found an undocumented return code.\n");
			//bRetryAsked = true;
			iRetVal = BI_ERROR;
		}




		/* to get here...both the commands went fine or only one was requested */

		if( iSendMethCmdRetCode == SUCCESS &&		// command sent OK   and
			((bEnableCmd48IfRequired == false) ||	// cmd48 not allowed OR
			 (iSendMethCmdRetCode48 == SUCCESS)  )  //   cmd48 sent OK
		  )
		{
			if((iRetVal == BI_SUCCESS) // we haven't already found an abort or a no device or an internal error AND
				&&(  ( m_pMeth->m_byRespAbortMask BIT(pchResponseStatus[STATUS_RESPONSE_CODE]) )//this RC is set OR
				   ||(  pchResponseStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byCommAbortMask   )// any match     OR
				   ||(  pchResponseStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byStatusAbortMask )// any match
				  )
			)
			{/*<START>Added by ANOOP to acknowledge the user that the current execution of method is aborted due to response code or device status */
				m_pMeth->abort();// stevev 08dec14 - to turn off abort button for host test
				tchar pchString[1024]={0};
				_tstrcpy(pchString,M_METHOD_RC_ABORT);
				acknowledge(pchString, NULL, 0);	
				/*<END>Added by ANOOP to acknowledge the user that the current execution of method is aborted due to response code or device status */
				iRetVal = BI_ABORT;
			}

			if(bMoreStatusAvailable == true)// cmd48 was sent
			{
				if((iRetVal == BI_SUCCESS) // we haven't already found an abort or a no device or an internal error AND
					&&(  (m_pMeth->m_byXmtrRespAbortMask BIT( pchMoreDataStatus[STATUS_RESPONSE_CODE] ))//cmd48 RC is set OR
					   ||(pchMoreDataStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byXmtrCommAbortMask)     // any match     OR
					   ||(pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusAbortMask)   // any match  
					  )
				  )
				{
					iRetVal = BI_ABORT;// cmd 48 abort
				}

				if(iRetVal == BI_SUCCESS)// all worked well, transfer data
				{
					for(i = 0;i < iXmtrDataCount; i++)
					{
						if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataAbortMask[i])
						{
							iRetVal = BI_ABORT;
							break;
						}
					}
				}
			}/*Endif bMoreStatusAvailable*/

			if( (iRetVal == BI_SUCCESS) // we haven't already found an abort or a no device or an internal error AND
				&&(   (m_pMeth->m_byRespRetryMask BIT( pchResponseStatus[STATUS_RESPONSE_CODE] ))
				    ||(pchResponseStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byCommRetryMask     )
				    ||(pchResponseStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byStatusRetryMask   )
				  )
			  )
			{						
				bRetryAsked = true;
			}

			if(bMoreStatusAvailable == true)
			{
				if( (iRetVal == BI_SUCCESS)
					&&(   (m_pMeth->m_byXmtrRespRetryMask BIT( pchMoreDataStatus[STATUS_RESPONSE_CODE] ))
						||(pchMoreDataStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byXmtrCommRetryMask     )
						||(pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusRetryMask   )
					  )
				  )
				{						
					bRetryAsked = true;
				}

				if(iRetVal == BI_SUCCESS)
				{
					for(i = 0; i < iXmtrDataCount; i++)
					{
						if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataRetryMask[i])
						{
							bRetryAsked = true;
							break;
						}
					}
				}
			}/*Endif bMoreStatusAvailable*/
		
		}/*Endif iSendMethCmdRetCode == SUCCESS && ....*/


		if(bRetryAsked == true)
		{
			if(iRetryCount < m_pMeth->m_iAutoRetryLimit)
			{
				iRetryCount++;
				bRetry = true;
			}
			else
			{
				iRetVal = BI_ABORT;// beyond retries
			}
		}

	}while (bRetry == true);

	/* stevev 04dec14 - Perhaps we should actually abort the method when we say we will
		instead of putting this snippet in each send-type builtin, I'm putting it here **/
	if (iRetVal == BI_ABORT)
	{
		m_pMeth->process_abort ();
		iRetVal = METHOD_ABORTED;
	}
#ifdef _DEBUG
	string dbgStr;
	switch (iRetVal)
	{
	case BI_SUCCESS:
		dbgStr = " SUCCESS";
		break;
	case BI_ERROR:
		dbgStr = " ERROR";
		break;
	case BI_ABORT:
	case METHOD_ABORTED:
		dbgStr = " ABORT";
		break;
	case BI_NO_DEVICE:
		dbgStr = " NO_DEVICE";
		break;
	case BI_COMM_ERR:
		dbgStr = " COMM_ERROR";
		break;
	case BI_CONTINUE:
		dbgStr = " CONTINUE";
		break;
	case BI_RETRY:
		dbgStr = " RETRY";
		break;
	default:
		dbgStr = "UnknownValue";
		break;
	}//endswitch
	if (dbgStr == "UnknownValue")
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Method Builtin's base send "
						"function returning %s.\n",dbgStr.c_str());
	}
	else
	{
		LOGIF(LOGP_MISC_CMTS)(CLOG_LOG,"Method Builtin's base send "
						"function returning UnknownValue %d.\n",iRetVal);
	}
#endif

	return iRetVal;

}/*End SEND_COMMAND*/
#else
int	CHart_Builtins:: SEND_COMMAND(
			
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int  iCmdType
			, bool bMoreDataFlag
			, int& iMoreInfoSize
			)
{
	int iRetVal = BI_SUCCESS; /*Return value of this function*/
	int iSendMethCmdRetCode = SUCCESS; /*Return value from the SendMethCmd for NORMAL_COMMAND*/
	int iSendMethCmdRetCode48 = FAILURE;/*Return value from the SendMethCmd for COMMAND_48*/
	bool bRetry=false; /*Main loop controlling var*/
	bool bRetryAsked=false;/*Flag set from the retry/abort masks for the method execution*/
	bool bMoreStatusAvailable=false;/*Flag to indicate there's more status available*/
	int  iXmtrDataCount=0;/*Byte count for Cmd 48 reply*/

	int iRetryCount = 0; /*Command Retry Counter*/

	int i=0;

	varPtrList_t itemList ;
	
	hCitemBase* pIB = NULL;
	CValueVarient ValueNeeded;
	hCVar *pLocalVar = NULL;

	//int iRc;
	/*Initialize the ResponseStatus array*/

	pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
	pchResponseStatus[STATUS_COMM_STATUS]   = 0;
	pchResponseStatus[STATUS_DEVICE_STATUS] = 0;

	/*Initialize the MoreDataStatus (Cmd 48) array*/
/*Vibhor 120204: Start of Code*/

/*There was copy paste problem!! */

	pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
	pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
	pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;

/*And this initialization was missing */

	for(i = 0 ; i < MAX_XMTR_STATUS_LEN ; i++)
	{
		pchMoreDataInfo[i] = 0;
	}

/*Vibhor 120204: End of Code*/
	do
	{
		bRetry = false;
		bRetryAsked = false;
		bMoreStatusAvailable = false;
		iRetVal = BI_SUCCESS;

		if(iCmdType == NORMAL_CMD)
		{
			iSendMethCmdRetCode = m_pDevice->sendMethodCmd(iCommandNumber,iTransNumber);

			if(iSendMethCmdRetCode & (/*DEVICE_NO_DISPATCH |*/ HOST_INTERNALERR))//As per Steve anil on 3 November 2005
			{	/*Command_Not_Implemented*/
				pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
				pchResponseStatus[STATUS_COMM_STATUS]   = 0;
				pchResponseStatus[STATUS_DEVICE_STATUS] = 0;
				//Anil added to Fix Par 5573 , command not implemented  is send
				tchar pchString[1024]={0};
				_tstrcpy(pchString,M_METHOD_SEND_ERROR);
				acknowledge(pchString, NULL, 0);	

			}/*Endif DEVICE_NO_COMMANDCLASS*/
			else
			{
				if(m_pDevice->getItemBySymNumber(DEVICE_RESPONSECODE, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;

/*Vibhor 270204: Start of Code*/
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/
					
					pchResponseStatus[STATUS_RESPONSE_CODE] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_RESPONSECODE*/

				if(m_pDevice->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;
/*Vibhor 270204: Start of Code*/
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/
					int itemp = (int)ValueNeeded;

					pchResponseStatus[STATUS_COMM_STATUS] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_COMM_STATUS*/

				if(m_pDevice->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB) == SUCCESS)
				{
					pLocalVar = (hCVar*)pIB;

/*Vibhor 270204: Start of Code*/
					ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/

					int itemp = (int)ValueNeeded;

					pchResponseStatus[STATUS_DEVICE_STATUS] = (char)((int)ValueNeeded);
					
				}/*Endif DEVICE_COMM48_STATUS*/

			}/* End else*/

		}/*Endif NORMAL_CMD*/
		/*
		 * else pretend we sent the command so the more status command
		 * can be issued
		 * only set in get_more_status() builtin
		 */
		else
		{
			iSendMethCmdRetCode = SUCCESS;

			pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
			pchResponseStatus[STATUS_COMM_STATUS]   = 0;
			pchResponseStatus[STATUS_DEVICE_STATUS] = 0;

			bMoreDataFlag = true;
		}/*End else*/

		if((iSendMethCmdRetCode == SUCCESS))
		{			
			if(bMoreDataFlag == true)
			{
				if(iCmdType == NORMAL_CMD)
				{
					bMoreStatusAvailable = false;
					iSendMethCmdRetCode48 = SUCCESS;
				}/*Endif iCmdType*/

				if((iCmdType == MORE_STATUS_CMD)
					 || (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_MORESTATUSAVAIL))
				{/*Send Command 48*/

					iSendMethCmdRetCode48 = 
								   m_pDevice->sendMethodCmd(48,DEFAULT_TRANSACTION_NUMBER);
					bMoreStatusAvailable = true;

					if(iSendMethCmdRetCode48 & (DEVICE_NO_DISPATCH | HOST_INTERNALERR))
					{	/*Command_Not_Implemented*/
						pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
						pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
						pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;
					}/*Endif DEVICE_NO_COMMANDCLASS*/
					else
					{
						if(m_pDevice->getItemBySymNumber(DEVICE_RESPONSECODE, &pIB) == SUCCESS)
						{
							pLocalVar = (hCVar*)pIB;

/*Vibhor 270204: Start of Code*/
							ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/
							
							pchMoreDataStatus[STATUS_RESPONSE_CODE] = (char)((int)ValueNeeded);
							
						}/*Endif DEVICE_RESPONSECODE*/

						if(m_pDevice->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB) == SUCCESS)
						{
							pLocalVar = (hCVar*)pIB;

/*Vibhor 270204: Start of Code*/
							ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/

							int itemp = (int)ValueNeeded;

							pchMoreDataStatus[STATUS_COMM_STATUS] = (char)((int)ValueNeeded);
							
						}/*Endif DEVICE_COMM_STATUS*/

						if(m_pDevice->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB) == SUCCESS)
						{
							pLocalVar = (hCVar*)pIB;

/*Vibhor 270204: Start of Code*/
							ValueNeeded = pLocalVar->getRawDispValue();//was getDispValue();
/*Vibhor 270204: Start of Code*/

							int itemp = (int)ValueNeeded;

							pchMoreDataStatus[STATUS_DEVICE_STATUS] = (char)((int)ValueNeeded);
							
						}/*Endif DEVICE_COMM48_STATUS*/

					}/* End else*/
							
				}/*Endif iCmdType == MORE_STATUS_CMD*/

				if(bMoreStatusAvailable == true)
				{
					if(iSendMethCmdRetCode48 == SUCCESS)
					{
						hCcommand *pCommand = NULL; // WS:EPM 24may07
						CCmdList *ptrCmndList = (CCmdList *)m_pDevice->getListPtr(iT_Command);
						// WS:EPM 24may07  hCcommand *pCommand = ptrCmndList->getCmdByNumber(48); 
						if (ptrCmndList ) 
                        { 
							pCommand = ptrCmndList->getCmdByNumber(48); 
                        } // end  WS:EPM 24may07


						if(pCommand)
						{
							
							hCtransaction *pTrans =	pCommand->getTransactionByNumber(DEFAULT_TRANSACTION_NUMBER);
							
							if(pTrans)
							{
/*Vibhor 120204: Start of Code*/
/*Fix for PAR 5326 : Earlier pTrans->GetReplyList() was being used, which only returned
the lits of valid var ptrs in the reply packet, but here we want the whole byte string
of the 	reply */
								hPkt tempPkt;
								tempPkt.clear();
								if(pTrans->generateReplyPkt(&tempPkt) == SUCCESS)
								{
									if(tempPkt.dataCnt > 2)
									{
										for( i= 2;((i < tempPkt.dataCnt) && (i < MAX_XMTR_STATUS_LEN)) ;i++)
										{
											pchMoreDataInfo[i-2] =	tempPkt.theData[i];

										}/*Endfor*/
										iMoreInfoSize  =
										iXmtrDataCount = tempPkt.dataCnt - 2;
									}

									else 
									{
										iMoreInfoSize  =		// stevev 20nov14 - make it symetrical with if
										iXmtrDataCount = 0;
									}

								}/*Endif SUCCESS*/
/*Vibhor 120204: Start of Code*/
							}/*Endif pTrans*/
						}/*Endif pCommand*/
					}/*Endif iSendMethCmdRetCode48 == SUCCESS*/
					else
					if(iSendMethCmdRetCode48 & HOST_NORESPONSE)
					{
						if(m_pMeth->m_byXmtrReturnNodevAbortMask){
							
							iRetVal = BI_ABORT;
						}
						else if(m_pMeth->m_byXmtrReturnNodevRetryMask){

							bRetryAsked = true;
						}
						else
						{ 
							iRetVal = BI_NO_DEVICE;
						}

					}/*Endif HOST_NORESPONSE*/


					else if(iSendMethCmdRetCode48 & HOST_PARITYERR 
											 & HOST_OVERUNERR 
											 & HOST_FRAMINGERR
											 & HOST_RXBUFFOVER 
											 & HOST_CHKSUMERR 
											 & HOST_HARTFRAMEERR)
					{
						if(m_pMeth->m_byXmtrCommAbortMask & pchMoreDataStatus[STATUS_COMM_STATUS])
						{
							iRetVal = BI_ABORT;
						}
						else 
						if(m_pMeth->m_byXmtrCommRetryMask & pchMoreDataStatus[STATUS_COMM_STATUS])
						{							
							bRetryAsked = true;
						}
						else
						{
							iRetVal = BI_COMM_ERR;
						}


					}/*Endif HOST_COMM_ERROR*/

					else
					{/*For all other conditions  Request a retry 
					  * and set iSendMethCmdRetCode48 = SUCCESS
					  */
						if (iSendMethCmdRetCode48 != SUCCESS)
						{
							bRetryAsked = true;
							iSendMethCmdRetCode48 = SUCCESS;
						}
						else
						{
							bRetryAsked = false;
						}							
					}
				}/*Endif bMoreStatus == true*/
			}/*Endif bMoreDataFlag*/				
		}/*Endif iSendMethCmdRetCode == SUCCESS*/
		else
		if(iSendMethCmdRetCode & HOST_NORESPONSE)
		{
			if(m_pMeth->m_byReturnNodevAbortMask)
			{				
				iRetVal = BI_ABORT;
			}
			else if(m_pMeth->m_byReturnNodevRetryMask)
			{
				bRetryAsked = true;
			}
			else
			{ 
				iRetVal = BI_NO_DEVICE;
			}
		}/*Endif HOST_NORESPONSE*/
		else 
		if(iSendMethCmdRetCode & HOST_PARITYERR 
							   & HOST_OVERUNERR 
							   & HOST_FRAMINGERR
							   & HOST_RXBUFFOVER 
							   & HOST_CHKSUMERR 
							   & HOST_HARTFRAMEERR)
		{
			if(m_pMeth->m_byCommAbortMask & pchResponseStatus[STATUS_COMM_STATUS])
			{
					iRetVal = BI_ABORT;
			}
			else if(m_pMeth->m_byCommRetryMask & pchResponseStatus[STATUS_COMM_STATUS])
			{				
				bRetryAsked = true;
			}
			else
			{
				iRetVal = BI_COMM_ERR;
			}
		}/*Endif HOST_COMM_ERROR*/
		else
		if (iSendMethCmdRetCode != SUCCESS)
		{/*For all other conditions  Request a retry */
			bRetryAsked = true;
		}
		else
		{					
			bRetryAsked = false;
		}
		/*If both the commands went fine or only one was requested*/

		if(iSendMethCmdRetCode == SUCCESS && 
			((bMoreDataFlag == false) || (iSendMethCmdRetCode48 == SUCCESS))  )
		{
			if((iRetVal == BI_SUCCESS)
				&&(  ( m_pMeth->m_byRespAbortMask BIT(pchResponseStatus[STATUS_RESPONSE_CODE]) )//this RC is set
				   ||(  pchResponseStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byCommAbortMask   )// any match
				   ||(  pchResponseStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byStatusAbortMask )// any match
				  )
			)
			{/*<START>Added by ANOOP to acknowledge the user that the current execution of method is aborted due to response code or device status */
				tchar pchString[1024]={0};
				_tstrcpy(pchString,M_METHOD_RC_ABORT);
				acknowledge(pchString, NULL, 0);	
				/*<END>Added by ANOOP to acknowledge the user that the current execution of method is aborted due to response code or device status */
				iRetVal = BI_ABORT;
			}

			if(bMoreStatusAvailable == true)
			{
				if((iRetVal == BI_SUCCESS)
					&&(  (m_pMeth->m_byXmtrRespAbortMask BIT( pchMoreDataStatus[STATUS_RESPONSE_CODE] ))
					   ||(pchMoreDataStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byXmtrCommAbortMask)
					   ||(pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusAbortMask)
					  )
				  )
				{
					iRetVal = BI_ABORT;
				}

				if(iRetVal == BI_SUCCESS)
				{
					for(i = 0;i < iXmtrDataCount; i++)
					{
						if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataAbortMask[i])
						{
							iRetVal = BI_ABORT;
							break;
						}
					}
				}
			}/*Endif bMoreStatusAvailable*/

			if( (iRetVal == BI_SUCCESS)
				&&(   (m_pMeth->m_byRespRetryMask BIT( pchResponseStatus[STATUS_RESPONSE_CODE] ))
				    ||(pchResponseStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byCommRetryMask     )
				    ||(pchResponseStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byStatusRetryMask   )
				  )
			  )
			{						
				bRetryAsked = true;
			}

			if(bMoreStatusAvailable == true)
			{
				if( (iRetVal == BI_SUCCESS)
					&&(   (m_pMeth->m_byXmtrRespRetryMask BIT( pchMoreDataStatus[STATUS_RESPONSE_CODE] ))
						||(pchMoreDataStatus[STATUS_COMM_STATUS]   & m_pMeth->m_byXmtrCommRetryMask     )
						||(pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusRetryMask   )
					  )
				  )
				{						
					bRetryAsked = true;
				}

				if(iRetVal == BI_SUCCESS)
				{
					for(i = 0; i < iXmtrDataCount; i++)
					{
						if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataRetryMask[i])
						{
							bRetryAsked = true;
							break;
						}
					}
				}
			}/*Endif bMoreStatusAvailable*/
		
		}/*Endif iSendMethCmdRetCode == SUCCESS && ....*/

		if(bRetryAsked == true)
		{
			if(iRetryCount < m_pMeth->m_iAutoRetryLimit)
			{
				iRetryCount++;
				bRetry = true;
			}
			else
			{
				iRetVal = BI_ABORT;
			}
		}

	}while (bRetry == true);

	return iRetVal;

}/*End SEND_COMMAND*/

#endif  /* _USE_ORIG_SEND_CMD_  */


int CHart_Builtins::send(int cmd_number,uchar *cmd_status)
{
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size = 0;
	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,cmd_status,
						more_status, more_status_data,
						NORMAL_CMD, true, info_size);	
	// made true 03dec14 - only send_command and send_command_trans have no cmd 48
}




int CHart_Builtins::send_command(int cmd_number)
{
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size = 0;
	
	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,cmd_status,
						more_status,more_status_data,
						NORMAL_CMD, false, info_size);
	// made false 03dec14 - only send_command and send_command_trans have no cmd 48


}


int CHart_Builtins::send_command_trans(int cmd_number,int iTransNumber)
{
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size = 0;
	
	//return SEND_COMMAND(cmd_number,iTransNumber,cmd_status,more_status,more_status_data,NORMAL_CMD,true);
	return SEND_COMMAND(cmd_number,iTransNumber,cmd_status,
						more_status,more_status_data,
						NORMAL_CMD,false,info_size);
}


int CHart_Builtins::send_trans(int cmd_number,int iTransNumber,uchar *pchResponseStatus)
{
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size = 0;
	
	//return SEND_COMMAND(cmd_number,iTransNumber,pchResponseStatus,more_status,more_status_data,NORMAL_CMD,false);
	return SEND_COMMAND(cmd_number,iTransNumber,pchResponseStatus,more_status,more_status_data,
		                                                             NORMAL_CMD,true,info_size);

}

int CHart_Builtins::ext_send_command(int cmd_number	, uchar *pchResponseStatus, 
								uchar *pchMoreDataStatus, uchar *pchMoreDataInfo, 
								int& moreInfoSize)
{

	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,pchResponseStatus,
		pchMoreDataStatus,pchMoreDataInfo,NORMAL_CMD,true,moreInfoSize);
	
}

int CHart_Builtins::ext_send_command_trans
		(
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int&  moreInfoSize
		)
{

	// stevev 21nov14 - always default- I can't believe it has never been found!!!!!!!!!!!!
	//return SEND_COMMAND(iCommandNumber,DEFAULT_TRANSACTION_NUMBER,pchResponseStatus,
	//						pchMoreDataStatus,pchMoreDataInfo,NORMAL_CMD,true,moreInfoSize);
	return SEND_COMMAND(iCommandNumber,iTransNumber,pchResponseStatus,
							pchMoreDataStatus,pchMoreDataInfo,NORMAL_CMD,true,moreInfoSize);
}

int CHart_Builtins::tsend_command
		(
			int iCommandNumber
		)
{
	LOGIT(CERR_LOG," Attempt to use tsend_command() builtin.\n");
	/*
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	return SEND_COMMAND(iCommandNumber,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_status,
													more_status_data,NORMAL_CMD,false,info_size);
	**/
	return FAILURE;
}

int CHart_Builtins::tsend_command_trans
		(
			int iCommandNumber
			, int iTransNumber
		)
{
	LOGIT(CERR_LOG," Attempt to use tsend_command_trans() builtin.\n");
	/*
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	return SEND_COMMAND(iCommandNumber,iTransNumber,cmd_status,more_status,more_status_data,
																	NORMAL_CMD,false,info_size);
	**/
	return FAILURE;
}


int CHart_Builtins::get_more_status(uchar *more_data_status,uchar *more_data_info, int& moreInfoSize)
{
	
	uchar cmd_status[STATUS_SIZE];

	return SEND_COMMAND(0,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_data_status,more_data_info,
																MORE_STATUS_CMD,true,moreInfoSize);
	
}
