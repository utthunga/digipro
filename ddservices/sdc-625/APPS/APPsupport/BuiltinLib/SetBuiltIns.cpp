//#include "stdafx.h"
//#include <windows.h>
#include "ddbGeneral.h"
//#include <ALPHAOPS.H>
#include <limits.h>
#include <float.h>
#include "ddbDevice.h"
#include "Interpreter.h"
#include "Hart_Builtins.h"
#include "Delay_Builtin.h"
#include "MethodInterfaceDefs.h"
#include "MEE.h"



/***** Start Scaling ABORT, IGNORE, RETRY builtins *****/
/* 02Dec14 WAP&I have interpreted the ABORT_ON_COMM error to mean any comm error 
           in the device OR in the host.  That indicates the high order bit of
		   the 8 bit mask has been overloaded to indicate any comm error on the 
		   HOST. (ABORT_ON_COMM_ERROR sets 0xff, while ABORT_ON_ALL_COMM_STATUS 
		   sets 0x7f.)
	   -----Since the Modem Server and the HART server automatically retry on
			HOST comm errors and only return 'NO_RESPONSE' to the application,
			we cannot implement	this in SDC.  It is probably un-testable as well.
			That means, in the SDC, a comm mask of 0xFF functions exactly the 
			same as one of 0x7F. 
			This makes ABORT_ON_ERROR      exactly equal to ABORT_ON_ALL_COMM_STATUS
			and        IGNORE_COMM_ERROR   exactly equal to IGNORE_ALL_COMM_STATUS
			and        RETRY_ON_COMM_ERROR exactly equal to RETRY ON_ALL_COMM_STATUS
***/
int CHart_Builtins:: _set_comm_status(int iCommStatus, int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byCommAbortMask  =  (unsigned char) iCommStatus; // set
			m_pMeth->m_byCommRetryMask &= ~(unsigned char) iCommStatus; // can't retry
			break;

		case __RETRY__:
			m_pMeth->m_byCommAbortMask &= ~(unsigned char) iCommStatus; // can't abort
			m_pMeth->m_byCommRetryMask  =  (unsigned char) iCommStatus; // set
			break;

		default:		/* __IGNORE__ 0 */
			m_pMeth->m_byCommAbortMask &= ~(unsigned char) iCommStatus;// can't abort
			m_pMeth->m_byCommRetryMask &= ~(unsigned char) iCommStatus;// can't retry
			break;
	}

 return BI_SUCCESS;
}/*End _set_comm_status*/

int CHart_Builtins :: _set_device_status (int iDeviceStatus, int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byStatusAbortMask  =  (unsigned char) iDeviceStatus;// set
			m_pMeth->m_byStatusRetryMask &= ~(unsigned char) iDeviceStatus;// can't retry
			break;

		case __RETRY__:
			m_pMeth->m_byStatusAbortMask &= ~(unsigned char) iDeviceStatus;// can't abort
			m_pMeth->m_byStatusRetryMask  =  (unsigned char) iDeviceStatus;// set
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byStatusAbortMask &= ~(unsigned char) iDeviceStatus;// can't abort
			m_pMeth->m_byStatusRetryMask &= ~(unsigned char) iDeviceStatus;// can't retry
			break;
	}

 return BI_SUCCESS;

}/*End _set_device_status*/

int CHart_Builtins :: _set_resp_code(int iResponseCode, int iAbortIgnoreRetry)
{
	if (iResponseCode == 0 ) return BI_ERROR;// don't allow action on SUCCESS response code
	unsigned char X = (1 << BITNUM(iResponseCode));
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byRespAbortMask[BYTENUM(iResponseCode)] |=  X;// set 1
			m_pMeth->m_byRespRetryMask[BYTENUM(iResponseCode)] &= ~X;// can't retry
			break;

		case __RETRY__:
			m_pMeth->m_byRespAbortMask[BYTENUM(iResponseCode)] &= ~X;// can't abort
			m_pMeth->m_byRespRetryMask[BYTENUM(iResponseCode)] |=  X;// set 1
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byRespAbortMask[BYTENUM(iResponseCode)] &= ~X;// can't abort
			m_pMeth->m_byRespRetryMask[BYTENUM(iResponseCode)] &= ~X;// can't retry
			break;
	}

 return BI_SUCCESS;

}/*End _set_resp_code*/


int CHart_Builtins :: _set_all_resp_code(int iAbortIgnoreRetry)
{
	int i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			
			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byRespAbortMask[i] = 0xFF;
				m_pMeth->m_byRespRetryMask[i] = 0;
			}
			// zero is success, don't allow abort on success
			m_pMeth->m_byRespAbortMask[0] = 0xFE;
			break;

		case __RETRY__:
			
			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byRespAbortMask[i] = 0;
				m_pMeth->m_byRespRetryMask[i] = 0xFF;
			}
			// zero is success, don't allow abort on success
			m_pMeth->m_byRespRetryMask[0] = 0xFE;
			break;

		default:		/* __IGNORE__ */
			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byRespAbortMask[i] = 0;
				m_pMeth->m_byRespRetryMask[i] = 0;
			}
			break;
	}
	
	return BI_SUCCESS;

}/*End _set_all_resp_code*/

int CHart_Builtins	:: _set_no_device(int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byReturnNodevAbortMask = true;
			m_pMeth->m_byReturnNodevRetryMask = false;
			break;

		case __RETRY__:
			m_pMeth->m_byReturnNodevAbortMask = false;
			m_pMeth->m_byReturnNodevRetryMask = true;
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byReturnNodevAbortMask = false;
			m_pMeth->m_byReturnNodevRetryMask = false;
			break;
	}

 return BI_SUCCESS;


}/*End _set_no_device*/


int CHart_Builtins :: SET_NUMBER_OF_RETRIES(int iNumberOfRetries )
{
	
	m_pMeth->m_iAutoRetryLimit = iNumberOfRetries;

	return BI_SUCCESS;

}/*End SET_NUMBER_OF_RETRIES*/



/* End Scaling ABORT, IGNORE, RETRY builtins */




/***** Start XMTR ABORT, IGNORE, RETRY builtins *****/
int CHart_Builtins :: _set_xmtr_comm_status(int iCommStatus, int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byXmtrCommAbortMask  =  (unsigned char) iCommStatus;// set it
			m_pMeth->m_byXmtrCommRetryMask &= ~(unsigned char) iCommStatus;
			break;

		case __RETRY__:
			m_pMeth->m_byXmtrCommAbortMask &= ~(unsigned char) iCommStatus;
			m_pMeth->m_byXmtrCommRetryMask  =  (unsigned char) iCommStatus;// set it
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byXmtrCommAbortMask &= ~(unsigned char) iCommStatus;
			m_pMeth->m_byXmtrCommRetryMask &= ~(unsigned char) iCommStatus;
			break;
	}

 return BI_SUCCESS;
}

int CHart_Builtins :: _set_xmtr_device_status(int iDeviceStatus, int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byXmtrStatusAbortMask  =  (unsigned char) iDeviceStatus;// set
			m_pMeth->m_byXmtrStatusRetryMask &= ~(unsigned char) iDeviceStatus;
			break;

		case __RETRY__:
			m_pMeth->m_byXmtrStatusAbortMask &= ~(unsigned char) iDeviceStatus;
			m_pMeth->m_byXmtrStatusRetryMask  =  (unsigned char) iDeviceStatus;// set
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byXmtrStatusAbortMask &= ~(unsigned char) iDeviceStatus;
			m_pMeth->m_byXmtrStatusRetryMask &= ~(unsigned char) iDeviceStatus;
			break;
	}

 return BI_SUCCESS;

}/*End _set_xmtr_device_status*/

int CHart_Builtins :: _set_xmtr_resp_code(int iResponseCode, int iAbortIgnoreRetry)
{
	if (iResponseCode == 0) return BI_ERROR;
	unsigned char X = (1<<BITNUM(iResponseCode));
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byXmtrRespAbortMask[BYTENUM(iResponseCode)] |=  X;
			m_pMeth->m_byXmtrRespRetryMask[BYTENUM(iResponseCode)] &= ~X;
			break;

		case __RETRY__:
			m_pMeth->m_byXmtrRespAbortMask[BYTENUM(iResponseCode)] &= ~X;
			m_pMeth->m_byXmtrRespRetryMask[BYTENUM(iResponseCode)] |=  X;
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byXmtrRespAbortMask[BYTENUM(iResponseCode)] &= ~X;
			m_pMeth->m_byXmtrRespRetryMask[BYTENUM(iResponseCode)] &= ~X;
			break;
	}

 return BI_SUCCESS;
}/*End _set_xmtr_resp_code*/


int CHart_Builtins :: _set_xmtr_all_resp_code(int iAbortIgnoreRetry)
{
	int i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			
			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrRespAbortMask[i] = 0xFF;
				m_pMeth->m_byXmtrRespRetryMask[i] = 0;
			}
			m_pMeth->m_byXmtrRespAbortMask[0] = 0xFE;
			break;

		case __RETRY__:

			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrRespAbortMask[i] = 0;
				m_pMeth->m_byXmtrRespRetryMask[i] = 0xFF;
			}
			m_pMeth->m_byXmtrRespRetryMask[0] = 0xFE;
			break;

		default:		/* __IGNORE__ */
			for(i = 0; i< RESP_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrRespAbortMask[i] = 0;
				m_pMeth->m_byXmtrRespRetryMask[i] = 0;
			}
			break;
	}
	
	return BI_SUCCESS;
}/*End _set_xmtr_all_resp_code*/

int CHart_Builtins :: _set_xmtr_no_device(int iAbortIgnoreRetry)
{
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_byXmtrReturnNodevAbortMask = true;
			m_pMeth->m_byXmtrReturnNodevRetryMask = false;
			break;

		case __RETRY__:
			m_pMeth->m_byXmtrReturnNodevAbortMask = false;
			m_pMeth->m_byXmtrReturnNodevRetryMask = true;
			break;

		default:		/* __IGNORE__ */
			m_pMeth->m_byXmtrReturnNodevAbortMask = false;
			m_pMeth->m_byXmtrReturnNodevRetryMask = false;
			break;
	}

 return BI_SUCCESS;

}/*End _set_xmtr_no_device*/

int CHart_Builtins :: _set_xmtr_all_data(int iAbortIgnoreRetry)
{
	int i;
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			
			for(i = 0; i< DATA_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrDataAbortMask[i] = 0xFF;
				m_pMeth->m_byXmtrDataRetryMask[i] = 0;
			}
			break;

		case __RETRY__:
			
			for(i = 0; i< DATA_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrDataAbortMask[i] = 0;
				m_pMeth->m_byXmtrDataRetryMask[i] = 0xFF;
			}
			break;

		default:		/* __IGNORE__ */
			
			for(i = 0; i< DATA_MASK_LEN; i++)
			{
				m_pMeth->m_byXmtrDataAbortMask[i] = 0;
				m_pMeth->m_byXmtrDataRetryMask[i] = 0;
			}
			break;
	}
	
	return BI_SUCCESS;

}/*End _set_xmtr_all_data*/

/* stevev 18jun15 - this is screwed up: 
	It is supposed to be BitNumber = ((iByteCode *8)+ iBitMask);
	m_pMeth->m_byXmtrDataAbortMask[BYTENUM(BitNumber)] |= (unsigned char)(1<<BITNUM(BitNumber));
	etc/etc
	from:
	#define XMTR_ABORT_ON_DATA(x,y)    	do{SET_ARRAY_BIT(xmtr_data_abort_mask,	\
											((x)*8)+(y)); 							\
											CLR_ARRAY_BIT(xmtr_data_retry_mask, 	\
											((x)*8)+(y));}while(0)
	#define SET_ARRAY_BIT(n,x)	(n[BYTENUM(x)] = (n[BYTENUM(x)] | (1<<BITNUM(x))))
	#define	BYTENUM(x)		((x)/8)
	#define	BITNUM(x)		((x)-(BYTENUM(x)*8))
*/
/* stevev 09dec15 - the data portion is construed to be Byte number, bit number in the byte */
	
int CHart_Builtins :: _set_xmtr_data(int iByteCode, int iBitMask, int iAbortIgnoreRetry)
{
	if (iByteCode > DATA_MASK_LEN ) return BI_ERROR;
	if (iBitMask > 7 )              return BI_ERROR;

	DEBUGLOG(CLOG_LOG,"set_xmtr_data from: Abort[%d] = 0x%02x  Retry[%d] = 0x%02x (byte:%d bit:%d)\n",
	iByteCode,m_pMeth->m_byXmtrDataAbortMask[iByteCode],
	iByteCode,m_pMeth->m_byXmtrDataRetryMask[iByteCode], iByteCode,iBitMask);
	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
		/*	m_pMeth->m_byXmtrDataAbortMask[BYTENUM(iByteCode)] |= (unsigned char)(1<<BITNUM(iBitMask));
			m_pMeth->m_byXmtrDataRetryMask[BYTENUM(iByteCode)] &= ~(unsigned char)(1<<BITNUM(iBitMask));
		*/
			m_pMeth->m_byXmtrDataAbortMask[iByteCode] |= (unsigned char)(1<<iBitMask);
			m_pMeth->m_byXmtrDataRetryMask[iByteCode] &= ~(unsigned char)(1<<iBitMask);
			break;

		case __RETRY__:
		/*	m_pMeth->m_byXmtrDataAbortMask[BYTENUM(iByteCode)] &= ~(unsigned char)(1<<BITNUM(iBitMask));
			m_pMeth->m_byXmtrDataRetryMask[BYTENUM(iByteCode)] |= (unsigned char)(1<<BITNUM(iBitMask));
		*/
			m_pMeth->m_byXmtrDataAbortMask[iByteCode] &= ~(unsigned char)(1<<iBitMask);
			m_pMeth->m_byXmtrDataRetryMask[iByteCode] |=  (unsigned char)(1<<iBitMask);
			break;

		default:		/* __IGNORE__ */
		/*	m_pMeth->m_byXmtrDataAbortMask[BYTENUM(iByteCode)] &= ~(unsigned char)(1<<BITNUM(iBitMask));
			m_pMeth->m_byXmtrDataRetryMask[BYTENUM(iByteCode)] &= ~(unsigned char)(1<<BITNUM(iBitMask));
		*/
			m_pMeth->m_byXmtrDataAbortMask[iByteCode] &= ~(unsigned char)(1<<iBitMask);
			m_pMeth->m_byXmtrDataRetryMask[iByteCode] &= ~(unsigned char)(1<<iBitMask);
			break;
	}
	
	DEBUGLOG(CLOG_LOG,"               to : Abort[%d] = 0x%02x  Retry[%d] = 0x%02x\n",
	iByteCode,m_pMeth->m_byXmtrDataAbortMask[iByteCode],iByteCode,m_pMeth->m_byXmtrDataRetryMask[iByteCode]);
 return BI_SUCCESS;
	
}/*End _set_xmtr_data*/


