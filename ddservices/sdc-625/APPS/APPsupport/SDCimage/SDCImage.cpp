/*************************************************************************************************
 *
 * $Workfile: SDCImage.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		SDCImage.cpp : implementation of the SDCimage class
 */

#include "stdafximg.h"
#include "SDCImage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/************************************************************************************************
 * ImageList
 ************************************************************************************************
 */
SDC_ImageList::~SDC_ImageList()
{
	destroy();
}


void SDC_ImageList::destroy(void)
{
	for (DDLimagePtrList_it y = begin(); y != end(); ++y)
	{//ptr2ptr2a DDL_Image
		(*y)->destroy();
		delete (*y); (*y) = NULL;	//stevev 23oct09
	}
}


SDClangImage* SDC_ImageList::GetImage(unsigned idx, char* lang)
{
	SDClangImage* pRet = NULL;
	DDL_Image*    pImg;
	string dfltLang("en");

	// if no lang, use default
	if ( idx < 0 || idx >= size() )
	{
		return NULL;
	}// else process it

	// get indexed DDL_Image
	pImg = at(idx);
	if ( pImg != NULL )
	{// ask it for the language
		if ( lang == NULL )
		{
			lang = (char*)dfltLang.c_str();
		}
		pRet = pImg->GetImage(lang);
	}
	// return it

	return pRet;
}


/************************************************************************************************
 * DDL_Image
 ************************************************************************************************
 */
void DDL_Image::destroy(void)
{
	for (SDCimagePtrList_it y = begin(); y != end(); ++y)
	{//ptr2ptr2a SDClangImage
		(*y)->destroy();
		delete (*y); (*y) = NULL;	//stevev 23oct09
	}
}

SDClangImage* DDL_Image::GetImage(char* lang)
{
	SDClangImage* pRet = NULL, *pDflt = NULL;
	string targetLang, dfltLang("en");
	// if no lang, get default
	if ( lang != NULL )
	{
		targetLang = lang;
	}
	else
	{
		targetLang = dfltLang;
	}
	SDClangImage* pLI = NULL;
	// look up the language key
	for ( SDCimagePtrList_it ppI = begin();ppI != end(); ++ppI)
	{
		pLI = *ppI;	// stop using iterator as a pointer
		if ( pLI->language == targetLang )
		{
			pRet = pLI;
		}
		else
		if( pLI->language == dfltLang )
		{
			pDflt = pLI;
		}
	}
	if ( pRet == NULL )// not found
	{
		pRet = pDflt;// found or not, it's our best chance
	}
	// return it

	return pRet;
}

/************************************************************************************************
 * SDClangImage
 ************************************************************************************************
 */
SDClangImage::SDClangImage(BYTE * buffer, DWORD size, DWORD imagetype)
			: CxImage(buffer,size,imagetype), language("Uninitialized")
{
	DWORD ft =	GetType();
	if (ft == CXIMAGE_FORMAT_GIF && GetNumFrames()>1)
	{
		SetRetreiveAllFrames(true);
		SetFrame(GetNumFrames()-1);
		//Load(filename, type);
		
	//	Startup(imagetype);
		CxMemFile stream(buffer,size);
		Decode(&stream,imagetype);

	}
};


void SDClangImage::destroy(void)
{
	/* do any CxImage destruction here */
	Destroy();
}

SDClangImage::~SDClangImage()
{
	destroy();
}