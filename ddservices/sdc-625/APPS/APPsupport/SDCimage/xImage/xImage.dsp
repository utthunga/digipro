# Microsoft Developer Studio Project File - Name="xImage" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=xImage - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xImage.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xImage.mak" CFG="xImage - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xImage - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "xImage - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "xImage - Win32 ReleaseWithSymbols" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xImage - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\..\..\CommonClasses\ImgSupport\xImage" /I "..\..\..\..\Common" /I ".\\" /I "..\\" /I "..\..\..\..\CommonClasses\ImgSupport\jpeg" /I "..\..\..\..\CommonClasses\ImgSupport\png" /I "..\..\..\..\CommonClasses\ImgSupport\zlib" /D "DBG_PVFC" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "_AFXDLL" /D "JAS_WIN_MSVC_BUILD" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x410 /d "NDEBUG"
# ADD RSC /l 0x410 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\xImage.lib"

!ELSEIF  "$(CFG)" == "xImage - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /Zi /Od /I "..\..\..\..\CommonClasses\ImgSupport\CxImage" /I "..\..\..\..\Common" /I ".\\" /I "..\\" /I "..\..\..\..\CommonClasses\ImgSupport\jpeg" /I "..\..\..\..\CommonClasses\ImgSupport\png" /I "..\..\..\..\CommonClasses\ImgSupport\zlib" /I "..\..\..\..\CommonClasses\ImgSupport\xImage" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /D "_AFXDLL" /D "JAS_WIN_MSVC_BUILD" /Fr /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x410 /d "_DEBUG"
# ADD RSC /l 0x410 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\xImageD.lib"

!ELSEIF  "$(CFG)" == "xImage - Win32 ReleaseWithSymbols"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "xImage___Win32_ReleaseWithSymbols"
# PROP BASE Intermediate_Dir "xImage___Win32_ReleaseWithSymbols"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "xImage___Win32_ReleaseWithSymbols"
# PROP Intermediate_Dir "xImage___Win32_ReleaseWithSymbols"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I ".\\" /I "..\\" /I "..\..\..\..\CommonClasses\ImgSupport\jpeg" /I "..\..\..\..\CommonClasses\ImgSupport\png" /I "..\..\..\..\CommonClasses\ImgSupport\zlib" /I "..\..\..\..\CommonClasses\ImgSupport\xImage" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "_AFXDLL" /D "JAS_WIN_MSVC_BUILD" /FD /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\..\..\CommonClasses\ImgSupport\xImage" /I "..\..\..\..\Common" /I ".\\" /I "..\\" /I "..\..\..\..\CommonClasses\ImgSupport\jpeg" /I "..\..\..\..\CommonClasses\ImgSupport\png" /I "..\..\..\..\CommonClasses\ImgSupport\zlib" /D "DBG_PVFC" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "_AFXDLL" /D "JAS" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x410 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x410 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\lib\xImage.lib"
# ADD LIB32 /nologo /out:"..\lib\xImage.lib"

!ENDIF 

# Begin Target

# Name "xImage - Win32 Release"
# Name "xImage - Win32 Debug"
# Name "xImage - Win32 ReleaseWithSymbols"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\tif_xfile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximabmp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximadsp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximaenc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximaexif.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximagif.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximahist.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximaico.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximainfo.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximaint.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximaj2k.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximajas.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximajbg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximajpg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximalpha.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximalyr.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximamng.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximapal.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximapcx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximapng.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximasel.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximatga.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximath.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximatif.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximatran.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximawbmp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximawmf.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\ximawnd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\CxImage\xmemfile.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "jpeg"

# PROP Default_Filter "*.cpp"
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcapimin.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcapistd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jccoefct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jccolor.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcdctmgr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jchuff.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jchuff.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcinit.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcmainct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcmarker.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcmaster.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcomapi.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jconfig.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcparam.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcphuff.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcprepct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jcsample.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jctrans.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdapimin.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdapistd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdatadst.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdatasrc.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdcoefct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdcolor.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdct.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jddctmgr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdhuff.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdhuff.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdinput.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdmainct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdmarker.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdmaster.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdmerge.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdphuff.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdpostct.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdsample.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jdtrans.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jerror.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jerror.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jfdctflt.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jfdctfst.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jfdctint.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jidctflt.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jidctfst.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jidctint.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jidctred.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jinclude.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jmemmgr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jmemnobs.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jmemsys.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jmorecfg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jpegint.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jpeglib.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jquant1.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jquant2.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jutils.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\jpeg\jversion.h
# End Source File
# End Group
# Begin Group "png"

# PROP Default_Filter "*.cpp"
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\png.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngerror.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngget.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngmem.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngpread.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngread.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngrio.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngrtran.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngrutil.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngset.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngtrans.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngvcrd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngwio.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngwrite.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngwtran.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\png\pngwutil.c
# End Source File
# End Group
# Begin Group "zlib"

# PROP Default_Filter "*.cpp"
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\adler32.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\compress.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\crc32.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\deflate.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\gzio.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\infback.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\inffast.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\inflate.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\inftrees.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\trees.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\uncompr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\CommonClasses\ImgSupport\zlib\zutil.c
# End Source File
# End Group
# End Target
# End Project
