
// xima_sdc.h
// 21dec15 
// from the additions to the original ximage.h file


/* this file is designed to be included INSIDE the CxImage class in ximage.h to add capabilites */
public:
#if CXIMAGE_SUPPORT_WINDOWS
	long    DrawB(HDC hdc, const RECT& SrcRect, RECT* pDestRect, bool bSmooth);/* 13jan05 sjv*/
#endif


	// 12jan05 - sjv add drawmodes - public
	void AppendDrawMode(DRAWMODES newMode);/* or the setting */
	void SetDrawMode(DRAWMODES newMode);/* set absolutely */
	long GetDrawMode(void);

protected:	
	void Reset2Aspect(RECT& pntBox);/* 12jan05 - sjv */

#if CXIMAGE_SUPPORT_ENCODE
protected:	
	long    outFileSize;

public:
	long GetOutSize();
	bool Save(FILE* hFile, DWORD imagetype);

#endif