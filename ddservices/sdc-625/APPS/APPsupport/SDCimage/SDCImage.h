/*************************************************************************************************
 *
 * $Workfile: SDCImage.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface of the SDCimage class
 * #include "SDCImage.h".		
 */



#ifndef _SDCIMAGE_H
#define _SDCIMAGE_H
#ifdef INC_DEBUG
#pragma message("In SDCImage.h") 
#endif

#include "ximage.h"
#pragma warning (disable : 4786) 

#include <vector>
#include <string>
#include <algorithm>
#include < ostream >
#include < iostream >

using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes::SDCImage.h") 
#endif


/* the low level, language mapped, actual image */
class SDClangImage : public CxImage
{
public:
	string language;

	SDClangImage(BYTE * buffer, DWORD size, DWORD imagetype = CXIMAGE_FORMAT_UNKNOWN);

	void destroy(void);
	virtual
		~SDClangImage();
};

typedef vector<SDClangImage*>       SDCimagePtrList_t;
typedef SDCimagePtrList_t::iterator SDCimagePtrList_it;//ptr2ptr2a SDClangImage


/* the DD_Image - with one per language */
class DDL_Image : public SDCimagePtrList_t
{
public:
	SDClangImage* GetImage(char* lang = NULL);//char* ie "en"
	void destroy(void);
};

/* the accumulation of all of the images,frames, etc */
typedef vector<DDL_Image*>			DDLimagePtrList_t;
typedef DDLimagePtrList_t::iterator DDLimagePtrList_it;//ptr2ptr2a DDL_Image


/* SDC's list of images with necessary accessors */
class SDC_ImageList : public DDLimagePtrList_t
{
public:
	SDClangImage* GetImage(unsigned idx, char* lang = NULL);//char* ie "en"

	void destroy(void);
	virtual
		~SDC_ImageList();
};

#endif /*_SDCIMAGE_H*/