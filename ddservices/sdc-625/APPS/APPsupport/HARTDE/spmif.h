/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2002 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
** HEADER NAME: SPMIF.h
**
** PURPOSE:
**		Header file that contains the definition of the Request and response 
**	structures (structures passed between SPM & PMM).
**
**		
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 08-May-02  TSR Prasad  initial release
**
** Date...: 08-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/
#ifndef SPMIF_H
#define SPMIF_H

/*
The structure, sent from PMM to SPM and vice versa
*/
typedef struct tagSPMStruct
{
	WORD	wMessageSize; /* Size of the byte stream pointed to be pbyMessage */
	BYTE	*pbyMessage; /* BYTE stream */
} SPMStruct;

#endif /* SPMIF_H */

