
/*************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003 - 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
*/
/**************************************************************************
**
** APPLICATION: HART/DE Modem Interface
** HEADER NAME: HARTDEModem.cpp
**
** PURPOSE: Implementation of HARTDE Modem Interface
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0	12/01/04	Prasad S	initial release after the code review
**
** Date...:12/01/04
** Author.:Prasad S
** Comment:made from file
**
***************************************************************************/
#pragma warning (disable : 4244)
#ifndef _WIN32_WCE
#include "stdafx.h"
#include <iostream.h>
#include <time.h>
#include <io.h>
#include <process.h>
#endif
#include <stdio.h>
#include <windows.h>
#include <string.h>

/*********************************************************************************************
Header files
**********************************************************************************************/
#include "defines.h"
#include "HARTDEDefs.h"
#include "spm.h"
#include "HARTDEModem.h"

FILE *f1;

/*********************************************************************************************
** Local Defines
**********************************************************************************************/

#define MODEM_CHECKSUM_FAIL		0
#define NO_DEVICE_RESPONSE		1
#define DEVICE_CHECKSUM_FAIL	2
#define HARTDE_MODEM_BUSY		3
#define MESSAGE_LENGTH_SHORTER	4
#define MESSAGE_LENGTH_LONGER	5
#define BUFFER_OVERFLOW			6
#define BAD_CHECKSUM			7
#define	FRAMING_ERROR			8
/*#define	UNKNOWN_COMMAND			-3
#define ILLEGAL_DATA			-2
#define	NAK						-1*/



/*********************************************************************************************
Global Functions
**********************************************************************************************/
DWORD __stdcall DeviceScanningReadHandler(void *pParam);
DWORD __stdcall ModemScanningReadHandler(void *pParam);
DWORD __stdcall BurstMessageHandlingHandler(void *pParam);
void ErrorFunction(void);
/***************************************************************************
** FUNCTION NAME: CHartDEModem::InitializeModemDEInterface
**
** PURPOSE		: Initializes the COM port to which the Modem and devices
**					connected
**
** PARAMETER	: Serialport number, baud rate, Retry count, Start and
**					and end address of the devices connected to modem
**
** RETURN VALUE	: none
****************************************************************************/

HCMRESULT CHartDEModem::InitializeModemDEInterface(  BYTE nPortNumber
												, UINT nBaudRate
												, BYTE byRetryCount
												, BYTE byDevStartAddr
												, BYTE byDevEndAddr
												)
{
	BYTE retval,loop,i;
//	static BYTE byModemInitialized = FALSE;

//	if(!byModemInitialized)
	{
	pInputQueue.InitializeQueue();
	pOutputQueue.InitializeQueue();

	/* Initialize the number of devices present on the network to zero */
	BYTE No_dev_present = 0;

	/* Error function to be called */
	pfErrorFunction = ((ErrorCallbackFunction *)&ErrorFunction);


	/* Initialize a serial port, its parameters and Input and output queues
	    ( Parameters are:
		  Input queue pointer, Output queue pointer, Error function to be called on Exceptions
	      COM1, Baud rate (9600), No parity, 1 stop bit  and No hardware flow */
 	/* Set the port settings
		Port number - x , Baudrate - xxxx, NOPARITY, Number of bits - x
		Number of stop bits - (0 - 1 stopbit, 1 - 1.5 stop bits
		2 - 2 stopbits), NO_FLOW_CONTROL */
	cspm.Initialize(&pInputQueue, &pOutputQueue, pfErrorFunction, nPortNumber, nBaudRate, NOPARITY, 8, 0, NO_FLOW_CONTROL);

	/* Wait till port is initialized */
	Sleep(5000);

	/* Initialize the Critical Section Objects */
	InitializeCriticalSection(&HARTModemCriticalSection);
	InitializeCriticalSection(&HARTDevCriticalSection);


	if (cspm.m_bPortOpen ==  TRUE)
	{
		#ifdef _HARTDE_DEBUG_
			printf("COM Port%d Initialized\n",nPortNumber);
		#endif

		/* Added on 030205 to wait for 800msec after DTR Pin Enable to go high */
		Sleep(1000); 
	}
	else
	{
		#ifdef _HARTDE_DEBUG_
			printf("COM Port%d NotInitialized\n",nPortNumber);
		#endif

		// Send Failure message to client (COM_PORT_INIT_FAILED)
		return HART_DE_MODEM_COM_PORT_INIT_FAILED;
	}

	RetryCount = byRetryCount;
	DevStartAddr = byDevStartAddr;
	DevEndAddr	 = byDevEndAddr;

	if (((DevStartAddr < MIN_DEV_ADDRESS) || (DevStartAddr > MAX_DEV_ADDRESS))  ||
		 ((DevEndAddr < MIN_DEV_ADDRESS) || (DevEndAddr > MAX_DEV_ADDRESS)) ||
		 (DevStartAddr > byDevEndAddr))
	{
		#ifdef _HARTDE_DEBUG_
			printf("Device address is out of range\n",nPortNumber);
		#endif

		//Send device address out of range error (DEV_ADDR_WRONG)
		return HART_DE_MODEM_DEV_ADDR_WRONG;
	}
//		byModemInitialized = TRUE;
	}

	m_byExitThreads = FALSE;
	
	m_hBurstHandleThread = 0;


	f1 = fopen("SDC_Log.txt","w");

	if(NULL == f1)
	{
		return HART_DE_MODEM_BURST_MODE_THREAD_CREATE_FAIL;
	}

	BYTE byStsRetryCount = 0;

	/* Added three retries on 030205 to find the availability of the Modem */
	do
	{
		retval = GetHARTDEModemStatus();
		if(retval)
		{
			#ifdef _HARTDE_DEBUG_
				printf("Modem present\n");
			#endif

			break; /* Come out of the while loop, Modem status is success */
		}
		else
		{
			#ifdef _HARTDE_DEBUG_
				printf("No Modem present\n");
			#endif
		}

		byStsRetryCount++;
	}
	while(byStsRetryCount < 3);

	if(!retval)
	{
		// Send No modem present error (NOT_PRESENT)
		return HART_DE_MODEM_NOT_PRESENT;
	}


	retval = SetHARTDEModemProtocol();

	if(retval)
	{
		#ifdef _HARTDE_DEBUG_
			printf("Modem protocol set to HART success\n");
		#endif
	}
	else
	{
		#ifdef _HARTDE_DEBUG_
			printf("Modem protocol set to HART Failed\n");
		#endif
		// Send HART protocol set error (SET_PROTOCOL_FAIL)
		return HART_DE_MODEM_SET_PROTOCOL_FAIL;

	}

	/* SET Burst Mode handling OFF (0x02) */
	retval = SetHARTDEModemBurstModeHandling(0x02);

	if(retval)
	{
		#ifdef _HARTDE_DEBUG_
			printf("Modem Burst Mode OFF set success\n");
		#endif
	}
	else
	{
		#ifdef _HARTDE_DEBUG_
			printf("Modem Burst Mode OFF set Failed\n");
		#endif
		// Send HART protocol set error (SET_PROTOCOL_FAIL)
//		return HART_DE_MODEM_SET_PROTOCOL_FAIL;
		fprintf(f1,"HARTDEMODEM: Modem Burst Mode OFF set Failed\n");
	}
	/* Create the Scan thread */
	m_hBurstHandleThread = (HANDLE)CreateThread
							(
							NULL
							, 0
							, (DWORD  (__stdcall *)(void*)) (LPTHREAD_START_ROUTINE)(BurstMessageHandlingHandler)
							, this
							, 0
							, &m_dwBustHandleThreadId
							);
	if (m_hBurstHandleThread == NULL)
	{
			#ifdef _HARTDE_DEBUG_
				printf("\nFailed to create Burst Message Handler thread \n");
			#endif
			//Send Burst Mode Message Handler thread create failed error to client (HART_DE_MODEM_BURST_MODE_THREAD_CREATE_FAIL)
			return HART_DE_MODEM_BURST_MODE_THREAD_CREATE_FAIL;
	}


#if 0
	/* Create the Scan thread */
	m_hDevScanThread = (HANDLE)CreateThread
							(
							NULL
							, 0
							, (DWORD  (__stdcall *)(void*)) (LPTHREAD_START_ROUTINE)(DeviceScanningReadHandler)
							, this
							, 0
							, &m_dwDevScanThreadId
							);
	if (m_hDevScanThread == NULL)
	{
			#ifdef _HARTDE_DEBUG_
				printf("\nFailed to create Device Scan thread \n");
			#endif
			//Send Device scan thread create failed error to client (HART_DE_MODEM_DEV_SCAN_THREAD_CREATE_FAIL)
			return HART_DE_MODEM_DEV_SCAN_THREAD_CREATE_FAIL;
	}

		/* Create the Scan thread */
	m_hModemScanThread = (HANDLE)CreateThread
							(
							NULL
							, 0
							, (DWORD  (__stdcall *)(void*)) (LPTHREAD_START_ROUTINE)(ModemScanningReadHandler)
							, this
							, 0
							, &m_dwModemScanThreadId
							);
	if (m_hModemScanThread == NULL)
	{
			#ifdef _HARTDE_DEBUG_
				printf("\nFailed to create Modem Scan thread \n");
			#endif
			//Send modem scan thread create failed error to client (HART_DE_MODEM_SCAN_THREAD_CREATE_FAIL)
			return HART_DE_MODEM_SCAN_THREAD_CREATE_FAIL;
	}

#endif

 	/* Enter Critical section of accessing Hart Modem data base */
  	 EnterCriticalSection(&HARTModemCriticalSection);

	/* Clear Modem subscription parameter data base */
	 for(i=0;i<HART_DE_MODEM_MAX_READ_COMMANDS;i++)
		modemparam[i].Subscribe_Flag = CLEAR;

 	/* Leave Critical section of accessing Hart Modem data base */
  	 LeaveCriticalSection(&HARTModemCriticalSection);

 	/* Enter Critical section of accessing Device data base */
  	 EnterCriticalSection(&HARTDevCriticalSection);

	/* Remove all the devices from the network */
	for (loop = DevStartAddr; loop <= DevEndAddr; loop++)
	{
		slaveid[loop].byIsBurstMode = FALSE;
		slaveid[loop].poll_address = DEV_NOT_PRESENT;
	}

	/* Client Request to FALSE */
	ClientRequest = FALSE;

 	/* Leave Critical section of accessing Device data base */
  	LeaveCriticalSection(&HARTDevCriticalSection);

	//Send Initialization success message to client (INIT_SUCCESS)
	return HART_DE_MODEM_INIT_SUCCESS;
}

/*F*/
/***************************************************************************
** FUNCTION NAME: CHartDEModem::UnInitializeModemDEInterface
**
** PURPOSE		: UnInitializing all the resources initialized by the Initialize
**					Member function
**
** PARAMETER	: None
**
** RETURN VALUE	: success
****************************************************************************/

HCMRESULT CHartDEModem::UnInitializeModemDEInterface()
{
	cspm.UnInitialize();

	m_byExitThreads = TRUE;

    WaitForSingleObject(m_hBurstHandleThread, INFINITE);

	if (m_hBurstHandleThread)
		CloseHandle(m_hBurstHandleThread);

	DeleteCriticalSection(&HARTModemCriticalSection);
	DeleteCriticalSection(&HARTDevCriticalSection);

	if(NULL != f1)
		fclose(f1);

	return HART_DE_MODEM_SUCCESS;
}

BYTE CHartDEModem::IsBurstMessage()
{
	BYTE byRetStatus = FALSE;
	UINT byArrayIndex = 0;

	while(TRUE) //(in_buf_len >= 34)
	{
		if(in_buf[byArrayIndex] == 0x00 && in_buf[byArrayIndex+1] == 0x00 
				&& (in_buf_len >= (in_buf[byArrayIndex + 2] + 4)))
		{
			byRetStatus = TRUE;
//			Save the Burst Command Number in the device structure
			BYTE byManufacturerID = in_buf[byArrayIndex + 4] & 0x3f;
			BYTE byDeviceType = in_buf[byArrayIndex + 5];
			UINT nDevID =  (in_buf[byArrayIndex + 6] << 16) + 
							(in_buf[byArrayIndex + 7] << 8)	+ 
							in_buf[byArrayIndex + 8];

			for(int i = 0; i <= 15; i++)
			{
				if((slaveid[i].ManufacturerID == byManufacturerID) &&
				   (slaveid[i].DeviceType == byDeviceType) &&
					slaveid[i].DeviceID == nDevID)
				{
					slaveid[i].byCmdNum = in_buf[byArrayIndex + 9];
					break;
				}
			}
			in_buf_len -= in_buf[byArrayIndex + 2] + 4;
			byArrayIndex += in_buf[byArrayIndex + 2] + 4;
//			in_buf += 34;
			break;
		}
		else if(in_buf[byArrayIndex] == 0x01 && in_buf[byArrayIndex+1] == 0x06)
		{
			in_buf_len -= in_buf[2] + 4;
			byArrayIndex += in_buf[2] + 4;
			byRetStatus = TRUE;
//			byArrayIndex+=4;
//			in_buf += 4;
		}
		else
		{
			return FALSE;
		}
	}

	/* This is a busy status return TRUE */
/*	if(in_buf_len == 4 && in_buf[byArrayIndex] == 0x01 && in_buf[byArrayIndex+1] == 0x06)
	{
			in_buf_len -= in_buf[2] + 4;
			byRetStatus = TRUE;
//			byArrayIndex+=4;
//			in_buf += 4;
	}*/

	return byRetStatus;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::ModemScanningReadHandler
**
** PURPOSE		: Handler which scans invokes the function, which scan
**					all the devices connected on the network
**
** PARAMETER	: None
**
** RETURN VALUE	: -1
****************************************************************************/
DWORD __stdcall ModemScanningReadHandler(void *pParam)
{
	CHartDEModem *HARTDEObj = (CHartDEModem *)pParam;

	DWORD dwRetVal = HARTDEObj->ScanHARTDEModemParameters();

	return dwRetVal;

}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::DeviceScanningReadHandler
**
** PURPOSE		: Handler which scans invokes the function, which scan
**					all the devices connected on the network
**
** PARAMETER	: None
**
** RETURN VALUE	: -1
****************************************************************************/
DWORD __stdcall DeviceScanningReadHandler(void *pParam)
{
	CHartDEModem *HARTDEObj = (CHartDEModem *)pParam;

	DWORD dwRetVal = HARTDEObj->DeviceScanning();

	return dwRetVal;

}


/***************************************************************************
** FUNCTION NAME: CHartDEModem::BurstMessageHandlingHandler
**
** PURPOSE		: Handler which invokes the Burst Mode Message Handler thread
**
** PARAMETER	: None
**
** RETURN VALUE	: -1
****************************************************************************/
DWORD __stdcall BurstMessageHandlingHandler(void *pParam)
{
	CHartDEModem *HARTDEObj = (CHartDEModem *)pParam;

	DWORD dwRetVal = HARTDEObj->HandleBurstMessage();

	return dwRetVal;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::HandleBurstMessage
**
** PURPOSE		: Handles the Burst Mode Messages
**
**
** PARAMETER	: None
**
** RETURN VALUE	: -1
****************************************************************************/

DWORD CHartDEModem::HandleBurstMessage()
{
	UINT i,mes_len;
	DWORD retstatus = WAIT_TIMEOUT;
	UINT tmp_buf_len = 15;

	in_buf_len = 0;

	SPMStruct *pSPMBurstStruct;

	while(TRUE)
	{
		/* Enter Critical section of accessing Device data base */
		EnterCriticalSection(&HARTDevCriticalSection);

		in_buf_len = 0;

//		do
		{
		/* Clear the Old Message if exists */
		while(!pInputQueue.IsQueueEmpty() || (in_buf_len < tmp_buf_len) && in_buf_len)
		{
			pSPMBurstStruct = (SPMStruct *)pInputQueue.GetMessageFromQueue();

			if(m_byExitThreads)
				break;

			if(pSPMBurstStruct == NULL)
			{
				continue;
			}

			mes_len = pSPMBurstStruct->wMessageSize;

			for(i=0;i<mes_len;i++)
				in_buf[i+in_buf_len] = pSPMBurstStruct->pbyMessage[i];

			in_buf_len += mes_len;

			delete[] pSPMBurstStruct->pbyMessage;
			delete[] pSPMBurstStruct;

				if(in_buf_len > 2)
					tmp_buf_len = in_buf[2] + 4;

//			Sleep(10);
		}

		if(in_buf_len)
		{
			IsBurstMessage();
		}

//			tmp_buf_len = 15;
		}
//		while(in_buf_len);

//		in_buf_len = 0;

		/* Enter Critical section of accessing Device data base */
		LeaveCriticalSection(&HARTDevCriticalSection);

		Sleep(1000);

		if(m_byExitThreads)
			break;
	}
	
return -1;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::DeviceScanning
**
** PURPOSE		: Thread for scanning all the devices connected on the network
**
**
** PARAMETER	: None
**
** RETURN VALUE	: -1
****************************************************************************/

DWORD CHartDEModem::DeviceScanning()
{
	BYTE loop,*HART_response,HART_resp_len;
	DWORD retval;
	#ifdef _HARTDE_DEBUG_
		BYTE i;
	#endif

	while(TRUE)
	{
		for (loop = DevStartAddr; loop <= DevEndAddr; loop++)
		{
			/* Enter Critical section of accessing Device data base */
			EnterCriticalSection(&HARTDevCriticalSection);

			retval = ScanNetworkDevice(loop);

			#ifdef _HARTDE_DEBUG_
				printf("\n\nDevice poll address = %d \n", loop);
			#endif

			if(retval == WAIT_OBJECT_0)
			{

				#ifdef _HARTDE_DEBUG_
					/* Just print it  (For debugging purpose) */
					printf(" Reply Message length is  %d \n Reply Message is ", in_buf_len);

					for(i=0;i<in_buf_len;i++)
						printf(" %d ", in_buf[i]);
				#endif

				if (in_buf[1] == 0)
				{
					HART_response = new BYTE[in_buf_len];
					HART_resp_len = GetHARTResponse(HART_response);

					if(HART_resp_len < 19)
						loop--;
					else if (!(slaveid[loop].poll_address))
					{
						/* Add device ID to the local structure */
						slaveid[loop].poll_address = DEV_PRESENT;
						slaveid[loop].ManufacturerID = HART_response[7] & 0x3f;
						slaveid[loop].DeviceType = HART_response[8];

						slaveid[loop].DeviceID = (HART_response[15] << 16)
															+ (HART_response[16] << 8)
															+ (HART_response[17]);

						#ifdef _HARTDE_DEBUG_
							printf("\n%d device added to the network \n",loop);
						#endif

						// Send Added Device to the network message (DEV_ADDED)

					}
				}
				else if ((in_buf[1] == 0x05) && (in_buf[3] == 0x02) && (slaveid[loop].poll_address))
				{
					/* Add device ID to the local structure */
					slaveid[loop].poll_address = DEV_NOT_PRESENT;

					#ifdef _HARTDE_DEBUG_
						printf("\n%d device Removed from the network \n",loop);
					#endif

					//Send Removed device from the network error (DEV_REMOVED)

				}
				else if (in_buf[1] != 5)
					loop--;

			}
			else if (retval == WAIT_TIMEOUT)
			{
				#ifdef _HARTDE_DEBUG_
					printf("[FILE: %s LINE: %d] Timed out\n",__FILE__,__LINE__);
				#endif
			}

			/* Leave Critical section of accessing Device data base */
			LeaveCriticalSection(&HARTDevCriticalSection);

			// Check any client is waiting to talk to device or modem (allow them)
//			while(ClientRequest)
				Sleep(500); //sleep for 500msec

		} //end of for loop
	}//end of while

return -1;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::GetNetworkHierarchy
**
** PURPOSE		: Gets the complete network hierarchy
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::GetNetworkHierarchy()
{
	BYTE loop;

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	#ifdef _HARTDE_DEBUG_
			printf("\n\n\nIn GetNetworkHierarchy\n");
	#endif

	for (loop = DevStartAddr; loop <= DevEndAddr; loop++)
	{
		if (slaveid[loop].poll_address)
		{
			#ifdef _HARTDE_DEBUG_
				printf("%d device present in the network\n",loop);
			#endif
			// 	Use slaveid[loop].ManufacturerID, slaveid[loop].DeviceType &
			//	slaveid[loop].DeviceID to send it to client
		}

	}
	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

	return HART_DE_MODEM_SUCCESS;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::RebuildNetworkHierarchy
**
** PURPOSE		: Gets the complete network hierarchy,
**					Same as GetNetworkHierarchy()
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::RebuildNetworkHierarchy()
{
	BYTE loop;

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	#ifdef _HARTDE_DEBUG_
			printf("\n\n\nIn RebuildNetworkHierarchy\n");
	#endif

	for (loop = DevStartAddr; loop <= DevEndAddr; loop++)
	{
		if (slaveid[loop].poll_address)
		{
			#ifdef _HARTDE_DEBUG_
				printf("%d device present in the network\n",loop);
			#endif
			// 	Use slaveid[loop].ManufacturerID, slaveid[loop].DeviceType &
			//	slaveid[loop].DeviceID to send it to client
		}

	}
	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

	return HART_DE_MODEM_SUCCESS;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::WriteParameters
**
** PURPOSE		: Method by which command to be sent to the HART/DE Modem
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::WriteParameters(DWORD* CommandID,BYTE *Data,
										UINT NoofCommands,DWORD dwTransId)
{
	DWORD *ParamResult,dwWaitResult;
	UINT i;
	BYTE pCmdBuff,Command;

	if(NoofCommands > HART_DE_MODEM_MAX_WRITE_COMMANDS)
		return HART_DE_MODEM_MAX_COMMANDS;

	ParamResult=new DWORD[NoofCommands];

	/* Enter Critical section of accessing Device data base */
  	 EnterCriticalSection(&HARTDevCriticalSection);

	for(i=0;i<NoofCommands;i++)
	{
		pCmdBuff = *(Data + i);

		if(*(CommandID + i) == HART_DE_MODEM_PROTOCOL)
			Command = HART_DE_SET_PROTOCOL;
		else if(*(CommandID + i) == HART_DE_MODEM_PREAMBLES)
			Command = HART_DE_SET_PREAMBLES;
		else if(*(CommandID + i) == HART_DE_MODEM_MODE)
			Command = HART_DE_SET_MODE;

		/* From HART/DE Modem Status command */
		out_buf_len = GetModemCommand(Command,&pCmdBuff,1);

		/* Send it to port and get the response */
		dwWaitResult = SendAndGetDataFromModem();

		if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == Command) && (*(in_buf+1) == 0x00))
			*(ParamResult + i) = HART_DE_MODEM_SUCCESS;
		else
			*(ParamResult + i) = HART_DE_MODEM_FAIL;
	}

	// Send the list to client (CommandID, ParamResult, NooCommands, DwTransId);
	for(i=0;i<NoofCommands;i++)
	{
		#ifdef _HARTDE_DEBUG_
		printf("\nCommand ID = %d, Result = %d  \n",*(CommandID + i),*(ParamResult + i));
		#endif
	}

	/* Leave Critical section of accessing Device data base */
  	 LeaveCriticalSection(&HARTDevCriticalSection);

	return HART_DE_MODEM_SUCCESS;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::SubscribeModemParameters
**
** PURPOSE		: Subscribe to the HART/DE Modem parameters
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::SubscribeModemParameters( DWORD *nParameters
												, UINT nParamCount
												, DWORD dwTransId)
{
	BYTE i,subscribe_array_index;

	if(nParamCount > HART_DE_MODEM_MAX_READ_COMMANDS)
		return HART_DE_MODEM_MAX_COMMANDS;

	/* Enter Critical section of accessing HART Modem parameter data base */
  	 EnterCriticalSection(&HARTModemCriticalSection);

	 for(i=0; i<nParamCount; i++)
	 {
		 if (*(nParameters + i) == HART_DE_MODEM_PROTOCOL) //HART or DE
			 subscribe_array_index = 0;
		 else if (*(nParameters + i) == HART_DE_MODEM_MODE) //Burst or No burst or Open or Undetermined
			 subscribe_array_index = 1;
		 else if (*(nParameters + i) == HART_DE_MODEM_FIRMWARE_VERSION)  //Firmware Version
			 subscribe_array_index = 2;
		 else if (*(nParameters + i) == HART_DE_MODEM_STATUS)  //ROM or RAM fail, Low Batter, Dev type
			 subscribe_array_index = 3;
		 else
			 return HART_DE_MODEM_PARAMETER_ERROR;

		if (modemparam[subscribe_array_index].Subscribe_Flag == CLEAR)
			modemparam[subscribe_array_index].Subscribe_Flag = SET;
	 }

	 /* Leave Critical section of accessing HART Modem parameter data base */
  	 LeaveCriticalSection(&HARTModemCriticalSection);


	return HART_DE_MODEM_SUCCESS;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::UnSubscribeModemParameters
**
** PURPOSE		: Subscribe to the HART/DE Modem parameters
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::UnSubscribeModemParameters( DWORD *nParameters
												 , UINT nParamCount
												 , DWORD dwTransId)
{

	BYTE i,subscribe_array_index;

	if(nParamCount > HART_DE_MODEM_MAX_READ_COMMANDS)
		return HART_DE_MODEM_MAX_COMMANDS;

	/* Enter Critical section of accessing HART Modem parameter data base */
  	 EnterCriticalSection(&HARTModemCriticalSection);

	 for(i=0; i<nParamCount; i++)
	 {
		 if (*(nParameters + i) == HART_DE_MODEM_PROTOCOL) //HART or DE
			 subscribe_array_index = 0;
		 else if (*(nParameters + i) == HART_DE_MODEM_MODE) //Burst or No burst or Open or Undetermined
			 subscribe_array_index = 1;
		 else if (*(nParameters + i) == HART_DE_MODEM_FIRMWARE_VERSION)  //Firmware Version
			 subscribe_array_index = 2;
		 else if (*(nParameters + i) == HART_DE_MODEM_STATUS)  //ROM or RAM fail, Low Batter, Dev type
			 subscribe_array_index = 3;
		 else
			 return HART_DE_MODEM_PARAMETER_ERROR;

		if (modemparam[subscribe_array_index].Subscribe_Flag == SET)
			modemparam[subscribe_array_index].Subscribe_Flag = CLEAR;
	 }

	 /* Leave Critical section of accessing HART Modem parameter data base */
  	 LeaveCriticalSection(&HARTModemCriticalSection);


	return HART_DE_MODEM_SUCCESS;
}

/***************************************************************************
** FUNCTION NAME: CHartDEModem::ScanHARTDEModemParameters
**
** PURPOSE		: Scan the HART/DE Modem parameters
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

DWORD CHartDEModem::ScanHARTDEModemParameters()
{
	BYTE i,Command,SendCommand;
	DWORD dwWaitResult;

	while(TRUE)
	{
		for (i=0;i<HART_DE_MODEM_MAX_READ_COMMANDS;i++)
		{
			/* Enter Critical section of accessing Modem data base */
			EnterCriticalSection(&HARTModemCriticalSection);

			/* Enter Critical section of accessing Device data base */
			EnterCriticalSection(&HARTDevCriticalSection);

			if ((i==0) && (modemparam[i].Subscribe_Flag == SET))
			{
				Command = HART_DE_GET_PROTOCOL;
				SendCommand = SET;
			}
			else if ((i==1) && (modemparam[i].Subscribe_Flag == SET))
			{
				Command = HART_DE_GET_NETWORKSTATE;
				SendCommand = SET;
			}
			else if ((i==2) && (modemparam[i].Subscribe_Flag == SET))
			{
				Command = HART_DE_GET_FIRMWARE_VER;
				SendCommand = SET;
			}
			else if ((i==3) && (modemparam[i].Subscribe_Flag == SET))
			{
				Command = HART_DE_GET_STATUS;
				SendCommand = SET;
			}

			if( SendCommand == SET)
			{
				/* From HART/DE Modem Status command */
				out_buf_len = GetModemCommand(Command,NULL,NULL);

				/* Send it to port and get the response */
				dwWaitResult = SendAndGetDataFromModem();

				if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == Command)
						&& (*(in_buf+1) == 0x00))
				{
					if ((i==0) && (modemparam[i].Parameter_val[0] != *(in_buf+3)))
					{
						modemparam[i].Parameter_val[0] = *(in_buf+3);

						//Send Protocol Change info to client
						#ifdef _HARTDE_DEBUG_
							printf("\n\n Modem Protocol Changed \n");
						#endif
					}
					else if ((i==1) && (modemparam[i].Parameter_val[0] != *(in_buf+3)))
					{
						modemparam[i].Parameter_val[0] = *(in_buf+3);

						//Send Network state Change info to client
						#ifdef _HARTDE_DEBUG_
							printf("\n\n Modem Network state Changed \n");
						#endif
					}
					else if ((i==2) && ((modemparam[i].Parameter_val[0] != *(in_buf+3))
									|| (modemparam[i].Parameter_val[1] != *(in_buf+4))))
					{
						modemparam[i].Parameter_val[0] = *(in_buf+3);
						modemparam[i].Parameter_val[1] = *(in_buf+4);

						//Send Firmware Version Change info to client
						#ifdef _HARTDE_DEBUG_
							printf("\n\n Modem Firmware Version Changed \n");
						#endif
					}
					else if ((i==3) && ((modemparam[i].Parameter_val[0] != *(in_buf+3))
									|| (modemparam[i].Parameter_val[1] != *(in_buf+4))))
					{
						modemparam[i].Parameter_val[0] = *(in_buf+3);
						modemparam[i].Parameter_val[1] = *(in_buf+4);

						//Send Modem Status Change info to client
						#ifdef _HARTDE_DEBUG_
							printf("\n\n Modem Status Changed \n");
						#endif
					}
				}

			}

			SendCommand = CLEAR;

			/* Leave Critical section of accessing Device data base */
			LeaveCriticalSection(&HARTDevCriticalSection);

			/* Enter Critical section of accessing Modem data base */
			LeaveCriticalSection(&HARTModemCriticalSection);

			// Check any client is waiting to talk to device or modem (allow them)
//			while(ClientRequest)
//				Sleep(500); //sleep for 500msec

		} //end of for loop

		Sleep(5000); //sleep for 5 secs

	}//end of while

return -1;

}


/***************************************************************************
** FUNCTION NAME: CHartDEModem::SendCommand
**
** PURPOSE		: Method by which command to be sent to the HART device
**
** PARAMETER	: None
**
** RETURN VALUE	: None
****************************************************************************/

HCMRESULT CHartDEModem::SendCommand(BYTE *pCmdBuff
								   ,  UINT nCmdBuffSize
								   ,  DWORD dwTransId)
{
	DWORD dwWaitResult1;
	HCMRESULT retval;

	/* Set client Request flag */
	ClientRequest = TRUE;

 	/* Enter Critical section of accessing Device data base */
  	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x01,pCmdBuff,nCmdBuffSize);
	BYTE bzRetry = 0,bzRetryFlag = 0,ModemNoResponse = 0,bzMinRecvRespLen;
	UINT HART_resp_len = 0;
	BYTE bzDevChkSumWrng =0 ,bzModemChkSumWrng = 0,bzDevNoresp = 0,ModemBusy = 0;

	if(f1)
	{
		unsigned long position1, position2, size;
		fseek( f1, 0, SEEK_SET);
		position1 = ftell( f1 );
		fseek( f1, 0, SEEK_END);
		position2 = ftell( f1 );

		size = position2 - position1;
		// If file size > 1 MB
		if( size > 0x100000)
		{
			fclose(f1);
			f1 = fopen("SDC_Log.txt","w");
		}
	}

		


	do
	{
			/* Send it to port and get the response */
			dwWaitResult1 = SendAndGetDataFromModem();

			if (dwWaitResult1 == WAIT_OBJECT_0)
			{
				/* This function normally gets only Long Frame address */
				if(pCmdBuff[0] & 0x80) //Long Frame Address
					bzMinRecvRespLen =  14;
				else //Short Frame Address
					bzMinRecvRespLen =  11;

				if(in_buf_len > bzMinRecvRespLen)
				{
					HART_resp_len = GetHARTResponse(HART_response);
					BYTE byBurstRetStatus = IsBurstMessage();
				
					if (byBurstRetStatus) 
						bzRetryFlag = SET;
				}
				else
				{
					fprintf(f1,"HARTDEMODEM: NO Device Response length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					HART_resp_len = NO_DEVICE_RESPONSE;
				}

				if (HART_resp_len == MODEM_CHECKSUM_FAIL)
				{
					fprintf(f1,"HARTDEMODEM: MODEM_CHECKSUM_FAIL, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == NO_DEVICE_RESPONSE)
				{
					fprintf(f1,"HARTDEMODEM: NO_DEVICE_RESPONSE, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == DEVICE_CHECKSUM_FAIL)
				{
					fprintf(f1,"HARTDEMODEM: DEVICE_CHECKSUM_FAIL, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == HARTDE_MODEM_BUSY)
				{
					fprintf(f1,"HARTDEMODEM: HARTDE_MODEM_BUSY, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == MESSAGE_LENGTH_SHORTER)
				{
					fprintf(f1,"HARTDEMODEM: MESSAGE_LENGTH_SHORTER, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == MESSAGE_LENGTH_LONGER)
				{
					fprintf(f1,"HARTDEMODEM: MESSAGE_LENGTH_LONGER, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == BUFFER_OVERFLOW)
				{
					fprintf(f1,"HARTDEMODEM: BUFFER_OVERFLOW, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == BAD_CHECKSUM)
				{
					fprintf(f1,"HARTDEMODEM: BAD_CHECKSUM, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
				else if (HART_resp_len == FRAMING_ERROR)
				{
					fprintf(f1,"HARTDEMODEM: FRAMING_ERROR, Msg length = %d\n",in_buf_len);
					fprintf(f1,"Response Bytes Are:\n");

					for(int m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					bzRetryFlag = SET;
				}
/*					else if (HART_resp_len == ILLEGAL_DATA)
				{
					//TRACE("HARTDEMODEM: ILLEGAL_DATA\n");
					bzRetryFlag = SET;
				}
				else if (HART_resp_len == UNKNOWN_COMMAND)
				{
					//TRACE("HARTDEMODEM: UNKNOWN_COMMAND\n");
					bzRetryFlag = SET;
				}
				else if (HART_resp_len == NAK)
				{
					//TRACE("HARTDEMODEM: NAK\n");
					bzRetryFlag = SET;
				}*/


				if(!bzRetryFlag)
				{
					retval = HART_DE_MODEM_SUCCESS;

					BYTE byManufacturerID = HART_response[1] & 0x3f;
					BYTE byDeviceType = HART_response[2];
					UINT nDevID =  (HART_response[3] << 16) + 
									(HART_response[4] << 8)	+ 
									HART_response[5];

					for(int i = 0; i <= 15; i++)
					{
						if((slaveid[i].ManufacturerID == byManufacturerID) &&
						   (slaveid[i].DeviceType == byDeviceType) &&
							slaveid[i].DeviceID == nDevID)
						{
							slaveid[i].byIsBurstMode = (HART_response[1] & 0x40)?TRUE:FALSE;
							break;
						}
					}
					break;
				}
				else
					Sleep(50); //Wait till modem get ready to receive next command
			}
			else
			{
				//TRACE("HARTDEMODEM: Timed out\n");
				bzRetryFlag = SET;
				ModemNoResponse++;
			}

			if (bzRetryFlag)
			{
				bzRetryFlag =  CLEAR;
				bzRetry++;

				/* Retry till the number of Retry counts */
				if (bzRetry < RetryCount)
				{
					/* Wait for some time to get modem ready */
					//Sleep(50);
					fprintf(f1,"Retrying the command RetryCount = %d\n",bzRetry);
					//continue;
				}
				else
				{
					bzRetry = 0;
					retval = HART_DE_MODEM_FAIL;
				}
			}
	}while(bzRetry);

	/* Unset client Request flag */
	ClientRequest = CLEAR;

	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

	return retval;
}



/***************************************************************************
** FUNCTION NAME: CHartDEModem::CHartDEModem
**
** PURPOSE		: The class constructor
**
** PARAMETER	: Initializes the member variables to default values.
**
** RETURN VALUE	: none
****************************************************************************/
CHartDEModem::CHartDEModem()
{

	return;
}/* End of Constructor */

/***************************************************************************
** FUNCTION NAME: CHartDEModem::~CHartDEModem
**
** PURPOSE		: The destructor ... does nothing !
**
** PARAMETER	: none
**
** RETURN VALUE	: none
****************************************************************************/
CHartDEModem::~CHartDEModem()
{
	return;
}/* End of Destructor */

/***************************************************************************
** FUNCTION NAME: CHartDEModem::GetHARTDevUniqueID
**
** PURPOSE		:
**
** PARAMETER	:
**
** RETURN VALUE	:
****************************************************************************/

DWORD CHartDEModem::GetHARTDevUniqueID(BYTE polladdress)
{
BYTE *HART_response=NULL;
UINT HART_resp_len;
DWORD retval,devpresent;
BYTE bzRetry = 0,bzRetryFlag = 0;

	if( (polladdress > DevEndAddr)  || (polladdress < DevStartAddr))
	{
		#ifdef _HARTDE_DEBUG_
			printf("Invalid polladdress = %d \n",polladdress);
		#endif
		return 0;
	}

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	do
	{
	// Scan device for presence
		retval = ScanNetworkDevice(polladdress);

		
		if(retval == WAIT_OBJECT_0)
		{
	//		if (in_buf[1] == 0)
	//		{
	//			if(in_buf_len < 11) //Minimum Guaranteed data from device for Cmd 0
	//			{
	//				 devpresent = 0;
	//			}
	//			else
				{
					HART_response = new BYTE[in_buf_len];
					HART_resp_len = GetHARTResponse(HART_response);

					BYTE byBurstRetStatus = IsBurstMessage();
					
					if (byBurstRetStatus) 
					{
						bzRetryFlag = SET;
					}

					if (HART_resp_len == MODEM_CHECKSUM_FAIL)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == NO_DEVICE_RESPONSE)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == DEVICE_CHECKSUM_FAIL)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == HARTDE_MODEM_BUSY)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == MESSAGE_LENGTH_SHORTER)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == MESSAGE_LENGTH_LONGER)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == BUFFER_OVERFLOW)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == BAD_CHECKSUM)
					{
						bzRetryFlag = SET;
					}
					else if (HART_resp_len == FRAMING_ERROR)
					{
						bzRetryFlag = SET;
					}

					if(!bzRetryFlag)
					{
						if(HART_resp_len == 19)
						{
							slaveid[polladdress].poll_address = DEV_PRESENT;
							slaveid[polladdress].ManufacturerID = HART_response[7] & 0x3f;
							slaveid[polladdress].DeviceType = HART_response[8] ;
							slaveid[polladdress].DeviceID = (HART_response[15] << 16)
															+ (HART_response[16] << 8)
															+ HART_response[17];

							slaveid[polladdress].byIsBurstMode = (HART_response[1] & 0x40)?TRUE:FALSE;

							#ifdef _HARTDE_DEBUG_
									printf("\n HART device present at poll address = %d \n",polladdress);
									printf(" ManufacturerID = %x\n DeviceType = %x \n DeviceID = %8x \n",
									slaveid[polladdress].ManufacturerID,slaveid[polladdress].DeviceType,
									slaveid[polladdress].DeviceID);
							#endif
							devpresent = 1;
						}
						else
							devpresent = 0;

						bzRetry = 0;
					}
					else
					{
						bzRetryFlag =  CLEAR;
						bzRetry++;

						/* Retry till the number of Retry counts */
						if (bzRetry < (RetryCount - 3))
						{
							/* Wait for some time to get modem ready */
						//	Sleep(50);
							fprintf(f1,"Retrying the command RetryCount = %d\n",bzRetry);
						//	continue;
						}
						else
						{
							bzRetry = 0;
							//retval = HART_DE_MODEM_FAIL;
						}
					}
					delete	HART_response;
					HART_response = NULL;
				}
	/*		}
			else
			{
				#ifdef _HARTDE_DEBUG_
					printf("HART device not present at poll address = %d \n",polladdress);
				#endif
				devpresent = 0;
			}*/

		}
		else if (retval == WAIT_TIMEOUT)
		{
			#ifdef _HARTDE_DEBUG_
				printf("HART device not present at poll address = %d \n",polladdress);
			#endif
				devpresent = 0;
		}
		
	}while(bzRetry);

	in_buf_len = 0;

	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

return devpresent;
}

/*********************************************************************************************
Function: GetModemCommand(BYTE, BYTE *, BYTE)

**********************************************************************************************/

UINT CHartDEModem::GetModemCommand(BYTE command, BYTE *pbuf, UINT p_buf_len)
{
	BYTE checksum = 0;
	UINT i;

	out_buf[0] = command;
	checksum ^= command;
	out_buf[1] = p_buf_len;
	checksum ^= p_buf_len;

	for(i = 0 ; i < p_buf_len; i++)
	{
		out_buf[i + 2] = pbuf[i];
		checksum ^= out_buf[i + 2];
	}

	out_buf[i + 2]  = checksum;

return p_buf_len+3;
}

/*********************************************************************************************
Function: GetHARTResponse(BYTE *)

**********************************************************************************************/

UINT CHartDEModem::GetHARTResponse(BYTE *pbuf)
{
	UINT i,HART_mes_len = 0;

	/* Calculate the checksum of the Modem */
	BYTE checksum = 0;
	
	/* If modem is busy */
	if (in_buf[1] == 0x06)
	{

		if(in_buf_len > 4)
		{
			//Remove first 5 elements from in_buf;
			for(i = 0; i < in_buf_len - 4; i++)
				in_buf[i] = in_buf[i + 4];

			/* in_buf[2] contains the data present in the HART/DE Modem Frame 
			 4 gives, HARTDE Modem command, status, check sum and data byte length */
			in_buf_len = 4 + in_buf[2];
		}
		else
		{	
			return HARTDE_MODEM_BUSY;
		}
	}
	else if (in_buf[1] == 0x0A)
		return MESSAGE_LENGTH_SHORTER;
	else if (in_buf[1] == 0x0B)
		return MESSAGE_LENGTH_LONGER;
	else if (in_buf[1] == 0x09)
		return BUFFER_OVERFLOW;
	else if (in_buf[1] == 0x04)
		return BAD_CHECKSUM;
	else if (in_buf[1] == 0x03)
		return FRAMING_ERROR;
/*	else if (in_buf[1] == 0x02)
		return ILLEGAL_DATA;
	else if (in_buf[1] == 0x01)
		return UNKNOWN_COMMAND;
	else if (in_buf[1] == 0x07)
		return NAK;*/

	/* Check the the Modem data length (= Hart Message), if the data
	  length is one,  device not reponded */
	if ( in_buf[1] == 5 && in_buf_len == 5)
		return NO_DEVICE_RESPONSE;

	/* There are times when device is responding after Modem given divice not responding
	   message,  So remove device not responded message of modem from overall
	   message (if it comes first)*/
	if (in_buf[1] == 5)
	{
		//Remove first 5 elements from in_buf;
		for(i = 0; i < in_buf_len - 5; i++)
			in_buf[i] = in_buf[i + 5];
	}

	/* in_buf[2] contains the data present in the HART/DE Modem Frame 
	 4 gives, HARTDE Modem command, status, check sum and data byte length */
	in_buf_len = 4 + in_buf[2];

	for (i=0;i<in_buf_len - 1;i++)
		checksum ^= in_buf[i];

	if (checksum != in_buf[i])
		return MODEM_CHECKSUM_FAIL;


	/* Calculate the HART message checksum */
	checksum = 0;

	for (i=3;i<in_buf_len - 2;i++)
		checksum ^= in_buf[i];

	if (checksum != in_buf[i])
		return DEVICE_CHECKSUM_FAIL;

	/* If every thing passes send the hart device data and data length */
	for(i = 3 ; i < in_buf_len - 1; i++)
	{
		*(pbuf + i - 3) = in_buf[i];
		HART_mes_len++;
	}

return HART_mes_len;
}

/*********************************************************************************************
Function: ValidateHARTResponse(BYTE *, BYTE)

**********************************************************************************************/

BYTE CHartDEModem::ValidateHARTResponse(BYTE *HART_response,BYTE HART_Resp_len)
{
	if (HART_response[8] & 0x80)
	{
		#ifdef _HARTDE_DEBUG_
			printf("Communication error occured \n");
		#endif
		return FALSE;
	}
return TRUE;
}




/*********************************************************************************************
Function: SendAndGetDataFromModem()

**********************************************************************************************/

DWORD CHartDEModem::SendAndGetDataFromModem()
{
	UINT i,mes_len;
	DWORD retstatus = WAIT_TIMEOUT;
	BYTE byBurstRetStatus;
	UINT nTmpBufLen = 0,nOldTmpBufLen = 0;
	

	in_buf_len = 0;
	/*pSPMInputStruct = for Reading data from m_OutputQueue
	pSPMOutputStruct = Queue for sending data to m_InputQueue	*/
	SPMStruct *pSPMInputStruct = NULL,*pSPMOutputStruct = NULL;

	SPMStruct *pSPMBurstStruct = NULL;

	/* Wait for some time before clearing the old messages,  at times
	   observed that, the burst message is not getting read completely in the
	   Handle Burst Message Thread wait for some time till you receive
	   rest of the burst message */
// 	Sleep(50);

	/* Clear the Old Message if exists */
	while(!pInputQueue.IsQueueEmpty())
	{
		pSPMBurstStruct = (SPMStruct *)pInputQueue.GetMessageFromQueue();

		if(pSPMBurstStruct == NULL)
		{
			continue;
		}

		mes_len = pSPMBurstStruct->wMessageSize;

		for(i=0;i<mes_len;i++)
			in_buf[i+in_buf_len] = pSPMBurstStruct->pbyMessage[i];

		in_buf_len += mes_len;

		delete[] pSPMBurstStruct->pbyMessage;
		delete[] pSPMBurstStruct;
	}

	if(in_buf_len)
	{
		IsBurstMessage();
	}
	nOldTmpBufLen = in_buf_len;
	in_buf_len = 0;

	/* Initialize a structure to be filled with the input message and write it to the
	   Output Queue */
	pSPMInputStruct = new SPMStruct;
	pSPMInputStruct->pbyMessage = new BYTE[out_buf_len];
	pSPMInputStruct->wMessageSize = out_buf_len;

	/* Copy the the modem command to the Structure to be written to Output queue */
	memcpy(pSPMInputStruct->pbyMessage, out_buf, out_buf_len);

	fprintf(f1,"\n\nSend Command:\n");

	for(unsigned int m=0;m<out_buf_len;m++)
		fprintf(f1,"\t%d",out_buf[m]);

	fprintf(f1,"\n");

	/* Write message to output queue */
	pOutputQueue.AddMessageToQueue(pSPMInputStruct);

	DWORD dwWaitResult = 0;
	/* Time out on serial port to get data from modem (Device) */
	DWORD PortTimeOut = 2000;
	/* Wait till complete data to be received from Queue */
	while(TRUE)
	{
		/* Wait for the device ( HART/DE modem to respond)
		   i.e, Check if the Input queue event is received */
		dwWaitResult = WaitForSingleObject(pInputQueue.GetQueueEventHandle(), PortTimeOut);

		if(dwWaitResult == WAIT_OBJECT_0)
		{

			pSPMOutputStruct = (SPMStruct *)pInputQueue.GetMessageFromQueue();

			if(pSPMOutputStruct == NULL)
			{
				continue;
			}	

			do
			{
				mes_len = pSPMOutputStruct->wMessageSize;
				
				for(i=0;i<mes_len;i++)
					in_buf[i+in_buf_len] = pSPMOutputStruct->pbyMessage[i];

				in_buf_len += mes_len;

				delete[] pSPMOutputStruct->pbyMessage;
				delete[] pSPMOutputStruct;

				pSPMOutputStruct = (SPMStruct *)pInputQueue.GetMessageFromQueue();

			}
//			while(!pInputQueue.IsQueueEmpty());
			while(NULL != pSPMOutputStruct);

			retstatus = dwWaitResult;

			/* Reduce time to receive rest of the message */
			PortTimeOut = 200;
		}
		else
		{
			if(in_buf_len)
			{
				nTmpBufLen = in_buf_len;
				
				byBurstRetStatus = IsBurstMessage();

//				TRACE("nOldTmpBufLen = %d  nTmpBufLen = %d\n",nOldTmpBufLen,nTmpBufLen);

				fprintf(f1,"\n\nReceived Response Bytes:\n");

				for(unsigned int m=0;m<in_buf_len;m++)
					fprintf(f1,"\t%d",in_buf[m]);

				fprintf(f1,"\n");

				if(byBurstRetStatus && in_buf_len)
				{
					for(unsigned int  m = 0; m < in_buf_len; m++)
						in_buf[m] = in_buf[m + nTmpBufLen - in_buf_len];

					fprintf(f1,"\nSelected Response Bytes:\n");

					for(m=0;m<in_buf_len;m++)
						fprintf(f1,"\t%d",in_buf[m]);

					fprintf(f1,"\n");

					break;
				}
				else if(byBurstRetStatus)
				{
					/* Reduce time to receive rest of the message */
					PortTimeOut = 2000;
					in_buf_len = 0;
					continue;
				}
				else
				{
					break;
				}
			}

			break;
		}
	}


return retstatus;
}


/*********************************************************************************************
Function: GetHARTDEModemStatus()

**********************************************************************************************/

BYTE CHartDEModem::GetHARTDEModemStatus()
{
	DWORD dwWaitResult;
	BYTE byRetStatus = FALSE;

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x08,NULL,0);

	/* Send it to port and get the response */
    dwWaitResult = SendAndGetDataFromModem();

	if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == 0x08) && (*(in_buf+3) & 0x03) == 0x00)
	{
		byRetStatus = TRUE;
	}

	 /* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

return byRetStatus;
}

/*********************************************************************************************
Function: SetHARTDEModemProtocol()

**********************************************************************************************/

BYTE CHartDEModem::SetHARTDEModemProtocol()
{
	DWORD dwWaitResult;
	BYTE data = 0x02;
	BYTE byRetStatus = FALSE;

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x03,&data,1);

	/* Send it to port and get the response */
    dwWaitResult = SendAndGetDataFromModem();

    if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == 0x03) && (*(in_buf+1) == 0x00))
	{
		byRetStatus = TRUE;
	}

	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

return byRetStatus;
}

/*********************************************************************************************
Function: SetHARTDEModemBurstModeHandling()

**********************************************************************************************/

BYTE CHartDEModem::SetHARTDEModemBurstModeHandling(BYTE byBurstMode)
{
	DWORD dwWaitResult;
	BYTE data = byBurstMode;
	BYTE byRetStatus = FALSE;

	/* Enter Critical section of accessing Device data base */
	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x0A,&data,1);

	/* Send it to port and get the response */
    dwWaitResult = SendAndGetDataFromModem();

    if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == 0x0A) && (*(in_buf+1) == 0x00))
	{
		byRetStatus = TRUE;
	}

	/* Leave Critical section of accessing Device data base */
	LeaveCriticalSection(&HARTDevCriticalSection);

return byRetStatus;
}

/*********************************************************************************************
Function: GetHARTDEModemProtocol()

**********************************************************************************************/

BYTE CHartDEModem::GetHARTDEModemProtocol()
{
	DWORD dwWaitResult;
	BYTE data = 0x02;

	/* Enter Critical section of accessing Device data base */
//	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x04,NULL,0);

	/* Send it to port and get the response */
    dwWaitResult = SendAndGetDataFromModem();

    if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == 0x04) && (*(in_buf+1) == 0x00) && (*(in_buf+2) == 0x01))
		return *(in_buf+3);

	/* Leave Critical section of accessing Device data base */
//	LeaveCriticalSection(&HARTDevCriticalSection);

return 0;
}

/*********************************************************************************************
Function: GetHARTDEModemProtocol()

**********************************************************************************************/

BYTE CHartDEModem::GetHARTDENetworkState()
{
	DWORD dwWaitResult;
	BYTE data = 0x02;

	/* Enter Critical section of accessing Device data base */
//	EnterCriticalSection(&HARTDevCriticalSection);

	/* From HART/DE Modem Status command */
	out_buf_len = GetModemCommand(0x05,NULL,0);

	/* Send it to port and get the response */
    dwWaitResult = SendAndGetDataFromModem();

    if ((dwWaitResult == WAIT_OBJECT_0) && (*in_buf == 0x05) && (*(in_buf+1) == 0x00) && (*(in_buf+2) == 0x01))
		return *(in_buf+3);

	/* Leave Critical section of accessing Device data base */
//	LeaveCriticalSection(&HARTDevCriticalSection);

return 0;
}

/*********************************************************************************************
Function: ScanNetworkDevices()

**********************************************************************************************/
DWORD CHartDEModem::ScanNetworkDevice(BYTE devaddress)
{
	BYTE loop = 0,checksum=0;
	DWORD i, dwWaitResult;

	/* HART command 0 to be written to Modem */
	BYTE pbyMessage[] = {0x02,0x00,0x00,0x00,0x00};

	BYTE pbuf_len = sizeof(pbyMessage);

	/* Assign the device address */
	pbyMessage[1] = devaddress;

	/* Calculate the checksum for the HART Command only and place it in pbyMessage[6] byte
	  modem request */
	checksum = 0;
	for (i=0;i<sizeof(pbyMessage) - 1;i++)
		checksum ^= pbyMessage[i];

	pbyMessage[sizeof(pbyMessage) - 1] = checksum;

	/* Get Device status (By sending 01 modem command) */
	out_buf_len = GetModemCommand(0x01,pbyMessage,pbuf_len);

#ifdef DEBUG
	/* VMKP added on 150704 */
	Sleep(1000);
	/* VMKP added on 150704 */
#endif

	/* Send it to port and get the response */
	dwWaitResult = SendAndGetDataFromModem();

return dwWaitResult;
} //End of ScanNetworkDevice

/*********************************************************************************************
Function: ErrorFunction()

 Error function has to be implemented
**********************************************************************************************/

void ErrorFunction(void)
{};

/***************************************EOF*************************************************/
