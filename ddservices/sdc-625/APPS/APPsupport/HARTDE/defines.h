/*C*/

/***************************************************************************/

/*                                                                         */

/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */

/*                                                                         */

/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */

/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */

/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */

/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */

/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */

/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */

/*    "COPYRIGHT 2003 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */

/*                                                                         */

/***************************************************************************/

/*I*/

/**************************************************************************

**

** APPLICATION: HART/DE Modem Interface 

** HEADER NAME: defines.h

**

** PURPOSE: Contains global defines and macros

**

** REVISION HISTORY:

**  Rev  Date      Programmer    Comment

**  ---- --------- ------------- ------------------------------------------

**  0.0								initial release

**

** Date...:  

** Author.: 

** Comment: 

**

***************************************************************************/



#ifndef _DEFINES_H

#define _DEFINES_H





/*****************************************************************************

* Basic datatypes

*****************************************************************************/





typedef unsigned char		uint8;  /*  8 bits */
typedef unsigned short int	uint16; /* 16 bits */
typedef unsigned int		uint32; /* 32 bits */
typedef signed char			int8;   /*  8 bits */
typedef signed short int	int16;  /* 16 bits */
typedef signed int			int32;  /* 32 bits */

/*****************************************************************************
* Defines
*****************************************************************************/



#ifndef TRUE
	#define TRUE	1
#endif

#ifndef FALSE
	#define FALSE   0
#endif

#ifndef SET
	#define SET   1
#endif

#ifndef CLEAR
	#define CLEAR   0
#endif

#ifndef YES
	#define YES   1
#endif

#ifndef NO
	#define NO   0
#endif


#ifndef ZERO
	#define ZERO   0
#endif

#ifndef FAILURE
	#define FAILURE 				-1	/* used for failure checks */
#endif


#endif /*_DEFINES_H*/

