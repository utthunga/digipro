/*C*/

/***************************************************************************/

/*                                                                         */

/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */

/*                                                                         */

/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */

/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */

/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */

/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */

/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */

/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */

/*    "COPYRIGHT 2003 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */

/*                                                                         */

/***************************************************************************/

/*I*/

/**************************************************************************

**

** APPLICATION: HART/DE Modem Interface 

** HEADER NAME: HARTDEDefs.h

**

** PURPOSE: Contains Defines required for HART/DE Modem Interface

**

** REVISION HISTORY:

**  Rev  Date      Programmer    Comment

**  ---- --------- ------------- ------------------------------------------

**  0.0								initial release

**

** Date...:  

** Author.: 

** Comment: 

**

***************************************************************************/



#ifndef _HARTDEDEFS_H

#define _HARTDEDEFS_H



/* Defines for Port success/failure notification */

#define INITIALIZE_PORT_FAILED	0

#define INITIALIZE_PORT_SUCCESS	1



/* Maximum number of HART devices  accessed by HART/DE modem */

#define MAX_MODEM_HART_DEVICES	16



/* MIN and MAX address of HART devices accessed by HART/DE modem */

#define MIN_DEV_ADDRESS			0

#define MAX_DEV_ADDRESS			15



/* Device addition and removal from the network */

#define HART_DEV_ADDED		0

#define HART_DEV_REMOVED	1



/* HART device present or not */

#define		DEV_PRESENT			1

#define		DEV_NOT_PRESENT		0



/* Commands */

#define HART_DE_SET_PROTOCOL		3

#define HART_DE_GET_PROTOCOL		4

#define HART_DE_GET_NETWORKSTATE	5

#define HART_DE_SET_PREAMBLES		6

#define HART_DE_GET_FIRMWARE_VER	7

#define HART_DE_GET_STATUS			8

#define HART_DE_SET_MODE			10



/* Maximum HART/DE commands */

#define	HART_DE_MODEM_MAX_READ_COMMANDS    4

#define	HART_DE_MODEM_MAX_WRITE_COMMANDS   3

#endif