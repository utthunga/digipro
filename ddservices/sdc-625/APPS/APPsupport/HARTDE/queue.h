/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2002 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
** HEADER NAME: Queue.h
**
** PURPOSE: Contains definitions for the functions that manipulate the 
**	message queues.
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 03-May-02  TSR Prasad  initial release
**
** Date...: 03-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/
#ifndef QUEUE_H
#define QUEUE_H

/* --- Module Includes ------------------------------------------------- */
#include "_Queue.h"
#include <windows.h>
/* ---- Definitions: ------------------------------------------------------ */

class CMessageQueue
{
private:
	/* The Queue Object */
	Queue MessageQueue;

	/* The Queue Object Iterator */
	Queue::iterator QueueIterator;

	/* The Critical Section Objects for the queues */
	CRITICAL_SECTION QueueCriticalSection;

	/* The event that must be set whenever the queue is written into */
	HANDLE hEventNewMessageInQueue;

public:
	/* Initializes the queues, must be called before calling any other
	functions below */
	void InitializeQueue();

	/* UnInitializes the queues */
	void UnInitializeQueue();

	/* 
		The functions manage the usage of the queue. 
	*/
	/* Adds a void pointer to a Byte stream to the queue */
	void AddMessageToQueue(void *pQueueElememt);
	/* Returns a pointer to a Byte Strean from the queue */
	void *GetMessageFromQueue();

	/* Return the event Handle for the client to Wait on !!! */
	HANDLE GetQueueEventHandle();

	/* Returns TRUE if queue is empty */
	BOOL IsQueueEmpty();

	/* Returns a pointer to a Byte Strean from the queue */
	void *PeekMessageFromQueue( int nPos );
	// Delete Message from Queue
	void DeleteMessageFromQueue( int nPos );

};

#endif /* QUEUE_H */

