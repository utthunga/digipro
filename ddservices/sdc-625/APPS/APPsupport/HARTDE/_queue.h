/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL IS PROHIBITED  EXCEPT AS AUTHORIZED	       */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2002 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
** HEADER NAME: _Queue.h
**
** PURPOSE: Contains definition for the queue object.
**		This file should not be included in any other file expect 
**		Queue.h !!!! 
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 03-May-02  TSR Prasad  initial release
**
** Date...: 03-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/

#ifndef _QUEUE_H
#define _QUEUE_H

/* --- Standard Includes ------------------------------------------------- */
#include <list>

/* ---- Definitions: ------------------------------------------------------ */
typedef std::list<void *> POINTER_LIST;

typedef POINTER_LIST Queue;


#endif /* _QUEUE_H */

