/*************************************************************************************************
 *
 * $Workfile: HARTDECommInfc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		docCommInfc.cpp : implementation file
 */
#pragma warning (disable : 4786)
#include "stdafx.h"			/* compiled in UI */
#include "SDC625.h"

// For ATL's variant support
#include <atlbase.h>
#include <atlimpl.cpp>
#include <objbase.h>

//#include "HModemSvr.h"
//#include "HModemSvr_i.c"

//#include "IHartComm_i.c"

//#include "HUtil_i.c"

#include "HARTDECommInfc.h"
#include "DDLCommErr.h"

#include "registerWaits.h"
#include "logging.h"

#include "HARTsupport.h"

#include "HARTDEModem.h"

#define  APP_HARTDE_MODEM_NOT_PRESENT			(APP_ERROR_BASE + 26)
#define  APP_HART_DE_MODEM_SET_PROTOCOL_FAIL	(APP_ERROR_BASE + 27)
/*Vibhor 040204: Start of Code*/

#include "Mainfrm.h"

/*Vibhor 040204: End of Code*/

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//WAITORTIMERCALLBACK Callback,      // triggers on an object ready in the queue
VOID CALLBACK gfHARTDEWaitAndTimerCallback(
	  PVOID   lpParameter,      // thread data(Context)
	  waitStatus_t TimerOrWaitFired)  // reason -- true == Timeout/death, false == event signaled
{	
	HARTDECommInfc* pCommInfc = (HARTDECommInfc *)lpParameter;
	// execute this class's send event callback
	if (pCommInfc != NULL)
	{
		pCommInfc->commInfcTask(TimerOrWaitFired);// will send the packet in proper form 
	}
	// loops to wait again forever
	return;
}



/////////////////////////////////////////////////////////////////////////////
// HARTDECommInfc

CString  cl_Url;

HARTDECommInfc::HARTDECommInfc(CDDIdeDoc* pD) : /*m_pDoc(pD),*/ thePktQueue(DOCCOMMQSIZE)// # initial pkts
{
	m_pDoc = pD;
	m_thisIdentity.clear();
	m_commHndl   = 0;
	m_pollAddr   = 0xff;
	pComm        = NULL;
//	pUtil        = NULL;
	m_connected  = false;
	/*
	m_enableComm = true;  // set to false to start the shutdown
	m_CommDisabled=false; // set true when shutdown complete
	*/
	m_CommStatus = commNC;// all of comm state rolled into one
	m_hadConfigChanged  = false;
	m_skipCount         = 0;

	hRegisteredTask = 0;
	/* Initialize the Critical Section Objects */
	InitializeCriticalSection(&HARTDESendCriticalSection);

	hartdemodem = new CHartDEModem();
/*Vibhor 120204: Start of Code*/

	m_bMoreStatusSent = false;

/*Vibhor 120204: Start of Code*/

/* stevev 2/12/04 - try to get light working */
	m_pMainFrm = NULL;
}

HARTDECommInfc::~HARTDECommInfc()
{   // Cleanup
//VMKP 260304
	thePktQueue.destroy();
//VMKP 260304
clog << "< doc-comm infc deleting."<<endl;
	if (hRegisteredTask != 0)
	{
		UnregisterWait( hRegisteredTask ) ;
		hRegisteredTask = 0;
	}

	hartdemodem->UnInitializeModemDEInterface();
	delete hartdemodem;
}

/************ stevev 25jan06 - use the base class...***********************************************/
/////////////////////////////////////////////////////////////////////////////
// HARTDECommInfc - virtual base class required functions  hCcommInfc
// stevev 2/26/04 - severly modified */
// The PUSH INTERFACE - see notes at end of "ddbCommInfc.h"
//void  HARTDECommInfc::notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged)
//{	
//	hCitemBase* pIB = NULL;
//	hCVar*      pIV = NULL;
/* stevev 18nov05 - replaced with below (post comment-out
//	if (inActionPkt)
//	{
//		switch (isChanged)
//		{
//		case NO_change:  / * assume that the action can change it. * /
//		case IS_changed: / * same as original * /
//			{
//				if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
//				{
//					if (   (SUCCESS == (m_pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB)) )
//						&& (pIB != NULL) )
//					{
//						if ( pIB->getIType() == iT_Variable)
//						{
//							pIV = (hCVar*)pIB;
//							pIV->CancelIt();
//							pstActPktList.push_back(pIV);
//						}
//					}
//					// else no use trying to continue
//				}
//				// else - we don't have enough pointers
//			} break;
//		case STRT_actPkt:	/ * start holding information * /
//			{
//				LOGIT(CERR_LOG,"ERROR: post action packet start w/o termination.\n");
//				inActionPkt = true; // redundant
//				pstActPktList.clear();
//			} break;
//		case END_actPkt:    / * act on held information * /
//			{
//				HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);
//
//				for (vector<hCVar*>::iterator iT = pstActPktList.begin();
//					 iT < pstActPktList.end();			iT++             )
//				{// iT isa Ptr 2a Ptr 2a hCVar
//					if (hwndMainWindow != NULL)
//					{// don't know if action has messed with it or not, all are considered changed
//						::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, (*iT)->getID(), NULL);
//					}// else we send it
//				}
//				inActionPkt = false; // redundant
//				pstActPktList.clear();
//			}break;
//		}// endswitch
//	}
//	else / * not in packet * /
//	{
//		switch (isChanged)
//		{
//		case IS_changed: / * same as original * /
//			{
//				if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
//				{
//					if (   (SUCCESS == (m_pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB)) )
//						&& (pIB != NULL) )
//					{
//						if ( pIB->getIType() == iT_Variable)
//						{
//							((hCVar*)pIB)->CancelIt(); // moves realValue to DispValue
//						}
//					}
//					// else no use trying to continue
//				}
//				// else we don't have the pointers to do this yet
//				HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);
//				if (hwndMainWindow != NULL)
//				{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
//
//			/ * VMKP added on 030204 * /
//  				if( / *!(pIB->IsReadOnly()) && stevev 10/11/04 - re-entry from rcv illegal - * /
//					PCURDEV->isItemCritical(changedItemNumber) && / * stevev---new filter * /
//					changedItemNumber != DEVICE_RESPONSECODE  &&
//					g_chWriteEnable	&&
//  					changedItemNumber != DEVICE_COMM48_STATUS	&&
//					changedItemNumber != DEVICE_COMM_STATUS    )
//				{
//					::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, changedItemNumber, NULL);
//					 g_chWriteEnable = false;
//				}
/// *				DATA_QUALITY_T dataQuality = ((hCVar *)pIB)->getDataQuality();
//
//				if(!(pIB->IsDynamic()) && (changedItemNumber != DEVICE_RESPONSECODE) && g_chMenuInitialized &&
//					(changedItemNumber != DEVICE_COMM48_STATUS) && (changedItemNumber != DEVICE_COMM_STATUS)
//					&& (dataQuality != DA_NOT_VALID) && (dataQuality != DA_STALEUNK))
//				{
//					::PostMessage(hwndMainWindow, WM_SDC_MENU_CHANGED, changedItemNumber, NULL);
//				}* / / * VMKP added on 030204 * /
//				else if ( isChanged )
//					{
//					::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, changedItemNumber, NULL);
//					}
//					else
//					{
//					::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGED, changedItemNumber, NULL);
//					}
//				}
//			} break;
//		case STRT_actPkt:	/ * start holding information * /
//			{	inActionPkt = true; 
//				pstActPktList.clear();
//			} break;
//		case END_actPkt:    / * act on held information * /
//			{
//				LOGIT(CERR_LOG,"ERROR: post action packet termination w/o start.\n");
//				inActionPkt = false; // redundant
//				pstActPktList.clear();
//			}// fall out
//		case NO_change:  / * same as original * /
//			{
//				/ * no operation * /;
//			}
//		}// endswitch
//	}// endelse - not in a post action packet
//	*****************/
//
//	
//    CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd(); // POB, 2 Aug 2004
//	HWND hwndMainWindow   = pMainWnd->GetSafeHwnd();		// POB, 2 Aug 2004
//
//	// deal with the copy-over first
//	if ( (isChanged == IS_changed)  &&						/* value change */
//		 (m_pDoc != NULL) && (m_pDoc->pCurDev != NULL) &&	/* we have pointers to work with */
//		 (SUCCESS == (m_pDoc->pCurDev->getItemBySymNumber(changedItemNumber, &pIB))) && /* item*/
//		 (pIB != NULL) && ( pIB->getIType() == iT_Variable)  ) // only variables are copied
//	{
//		pIV = (hCVar*)pIB;
//		pIV->CancelIt();
//	}
//
//    if (inActionPkt)
//    {
//    	switch (isChanged)
//    	{
//    	case IS_changed: /* same as original */
//    		{
//    			pstActPktList.push_back(changedItemNumber);
//    		} 
//			break;
//		case STR_changed:
//			{
//    			pstActStrList.push_back(changedItemNumber);
//			}
//			break;
//    	case STRT_actPkt:	/* start holding information */
//    		{
//    			LOGIT(CERR_LOG,"ERROR: post action packet start w/o termination.\n");
//    			inActionPkt = true; // redundant
//    			pstActPktList.clear();pstActStrList.clear();
//    		} break;
//    	case END_actPkt:    /* act on held information */
//    		{
//    			if (hwndMainWindow != NULL)
//				{
//					// structure first
//					for ( itemIDlist_t::iterator iT = pstActStrList.begin(); 
//						  iT != pstActStrList.end();                ++iT    )
//					{
//						::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, (*iT), NULL);
//					}
//					// then values
//					for (  iT = pstActStrList.begin();  iT != pstActStrList.end();     ++iT)
//					{
//						if ( (*iT) != DEVICE_RESPONSECODE   &&
//  							 (*iT) != DEVICE_COMM48_STATUS	&&
//							 (*iT) != DEVICE_COMM_STATUS    )
//						::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, (*iT), NULL);
//						// else do not send the message
//					}
//				}// else - no window to sent to - just exit
//
//    			inActionPkt = false; // redundant
//    			pstActPktList.clear();  pstActStrList.clear();
//    		}break;
//    	case NO_change:  /* 18nov05 - no longer a legal input. */
//		default:
//			{
//				; // no op eration
//			}
//			break;
//    	}// endswitch
//    }
//    else /* not in packet's    hold till actions complete */
//    {
//    	switch (isChanged)
//    	{
//    	case IS_changed: /* value has changed */
//    		{    
//    			if (hwndMainWindow != NULL)
//    			{
//  				::PostMessage(hwndMainWindow, WM_SDC_VAR_CHANGEDVALUE, changedItemNumber, NULL);
//				g_chWriteEnable = false;
//  				}		
//    		} 
//			break;
//		case STR_changed:
//			{
//    			if (hwndMainWindow != NULL)
//    			{
//  				::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, changedItemNumber, NULL);
//  				}	
//			}
//			break;
//    	case STRT_actPkt:	/* start holding information */
//    		{	inActionPkt = true; 
//    			pstActPktList.clear();pstActStrList.clear();
//    		} break;
//    	case END_actPkt:    /* act on held information */
//    		{
//    			LOGIT(CERR_LOG,"ERROR: post action packet termination w/o start.\n");
//    			inActionPkt = false; // redundant
//    			pstActPktList.clear();pstActStrList.clear();
//    		}// fall out
//    	case NO_change:  /* same as original */
//		default:
//    		{
//    			;/* no operation */;
//    		}
//    	}// endswitch
//    }// endelse - not in a post action packet
//}
/* stevev - end mods 2/26/04 */

/*Vibhor 120204: Start of Code*/

// this is an asycronous callback from the dispatcher
// the dispatcher handles the internal data acquasition and then calls this function	
void  HARTDECommInfc::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
	RETURNCODE rc = SUCCESS;
	unsigned char cmdNo;
	string   hs;
	hCitemBase* pIB = NULL;
	hCVar* pVr = NULL;
	hCBitEnum *pBe = NULL;
	BYTE byCommResp = 0;

		/*stevev - 2-13-04 modified Vibhor's NotifyCmd48 code */
		// just in constructor   bool m_bMoreStatusSent = false;

		/*<START>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/
		if(m_skipCount == 0)
		{
			if(NULL != m_pDoc->ptrCMD48Dlg)	/* Reset the suppression flag */
			{
				m_pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();
			}
		}
		/*<END>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/

		if ( thisMsgCycle.pCmd )
		{
			cmdNo = thisMsgCycle.pCmd->getCmdNumber();
		}
		else
		{
			cmdNo = 0xff;
		}
		/* temporary */
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"     Cmd:%03d RC:0x%02X  DS:0x%02X\n", 
												cmdNo, thisMsgCycle.respCdVal, thisMsgCycle.devStatVal);
		
		if (thisMsgCycle.respCdVal != 0)
		{
			if (thisMsgCycle.respCdVal < 0x80)// isa response code
			{
				//hCRespCodeList		respCds;
				for ( hCRespCodeList::iterator iT = thisMsgCycle.respCds.begin(); 
																iT < thisMsgCycle.respCds.end(); iT++)
				{// iT isa ptr2a hCrespCode
					if (iT->getVal() == thisMsgCycle.respCdVal)
					{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
					// leave narrow for now::: 	hs = iT->getDescStr();
						hs = iT->get_DescStr();
#else
						hs = iT->get_DescStr();
#endif
						/* should pop up the response code dialog */

						/*Vibhor 060204: Start of Code */
						/*Decided that we'll just show a Message Box for RespCode and Comm Status*/

					//	AfxMessageBox(hs.c_str());

						/*Vibhor 060204: End of Code */
						LOGIF(LOGP_COMM_PKTS)(CLOG_LOG|UI_LOG,"     Cmd:%d  RC:%s", cmdNo, hs.c_str());
					}
				}
/*Vibhor 220104: Start of Code*/
/*Decided fix for PAR 5299 "Command Not Implemented not being logged",
In realtime this won't generally happen,and its more intended towards,
the DD Developer usage of SDC. Since the standard response codes are
not available anywhere,	we need to just log the response code returned.	
*/
				if (hs.empty())
				{
					LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,
						"ERROR: No response code string found for cmd %d RC:0x%02X",
						cmdNo,thisMsgCycle.respCdVal);
					
					/*Vibhor 060204: Start of Code */

					char strBuff[65];
					
					sprintf(strBuff,"Received a Response Code RC: 0x%02X for Command: %d",
									thisMsgCycle.respCdVal,cmdNo);
				//	AfxMessageBox(strBuff);

					/*Vibhor 060204: End of Code */
				}
/*Vibhor 220104: End of Code*/

				/* VMKP 300104 Uddate the Response code here (7 bits of the first byte).
				  This will come only when No communicaton bit is set in the response
				  code */
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					//Commented by ANOOP since we are not displaying the response code
/*Vibhor 220204: Start of Code*/
//Uncommented : This will clear the Comm Status Tab
					m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(0);//sjv - changed to zero
/*Vibhor 220204: End of Code*/
				}		
				
			}
			else // isa comm error
			{// symbol DEVICE_COMM_STATUS
/*Vibhor 060204: Start of Code*/
				byCommResp = thisMsgCycle.respCdVal & ~ 0x80;

				rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB);
				if ( rc == SUCCESS && pIB != NULL )
				{
					pVr = (hCVar*) pIB; // a cast operator
					if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
					{// make it a bit enum
						pBe = (hCBitEnum*)pVr;
						if (pBe)
						{
							unsigned char msk = 1;
							do
							{// for each bit set
								if ( byCommResp & msk )
								{
									rc = pBe->procureString(msk, hs);
									if ( rc == SUCCESS && ! hs.empty() )
									{//	log the string
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
										CString localStr;
										AsciiToUnicode(hs.c_str(), localStr);

										AfxMessageBox(localStr);
#else
										AfxMessageBox(hs.c_str());
#endif
//										AfxMessageBox(hs.c_str());
						
										LOGIT(UI_LOG,"Comm Status: %s",hs.c_str());
			}
									else
									{
										char strBuff[85];
										sprintf(strBuff,"Comm Error : No Description Available for bit value: 0x%02X for Command: %d",
														msk,cmdNo);
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
										CString localStr;
										AsciiToUnicode(strBuff, localStr);

										AfxMessageBox(localStr);
#else
										AfxMessageBox(strBuff);
#endif
	//									AfxMessageBox(strBuff);

										LOGIT(UI_LOG,"No Description Avaliable for %d in Comm Status",msk);
			}
								}// else - not set, don't output string
							}while( (msk<<=1)  != 0);
						}
						else
						{
							LOGIT(CERR_LOG,"Comm Status: failed to cast from hCVar.");
			}
		}
					else
					{
						LOGIT(CERR_LOG,"Comm Status: failed to cast from hCItemBase to bit enum.");
					}

				}/*Endif rc = SUCCESS*/

				LOGIT(CLOG_LOG|UI_LOG,"     Cmd:%d  RC:0x%02X Comm error response code",cmdNo, thisMsgCycle.respCdVal);
				/* VMKP 300104 Uddate the Communication status (7 bits of the first byte).
				  This will come only when communicaton bit is set in the response
				  code */
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(byCommResp);  //MOdifed by ANOOP17FEB2004
		}
			}/* end else  sa comm error */

		}/*endif thisMsgCycle.respCdVal != 0*/
		else
		{
	//Resp Code = 0 , so no Comm Error,clear the comm lights in dialog	, this clears previously set bits if, any
			if(NULL != m_pDoc->ptrCMD48Dlg)
			{
				m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(thisMsgCycle.respCdVal);
			}

		}

		if ( ! m_hadConfigChanged )
		{// haven't had a config changed
			if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
			{// haven't had one, have one now
				// leave it set, it'll be handled later
				/* stevev - 2/17/04 - callback to mainframe for event */
				if (m_pMainFrm)
				{
					if (m_pMainFrm->ConfigChanged() == SUCCESS )// let it deal with it
					{
						m_hadConfigChanged = true;		// event - config changed to on

						/* VMKP added on 020404:  Rebuild the menu if unexpected Config change
								 bit is set,  May be a secondary configuration might have changed
								something in the device */
						// else we don't have the pointers to do this yet
						HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);
						if (hwndMainWindow != NULL)
						{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead

							::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, NULL, NULL);
						}
						/* VMKP added on 030204 */
					}
					//else - couldn't handle it; leave it so we will deal with it later
				}
				/* stevev - 2/17/04 - end */
			}
			// else - haven't had one, still don't have one
			// no operation
		}
		else 
		{// had one, filter the configuration changed bit
			if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
			{// had one, have one now
				thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); // clear it for further processing
			}
			else // had one, don't have one now
			{
				LOGIT(UI_LOG|CLOG_LOG,"Configuration Changed bit has Cleared.");
				m_hadConfigChanged = false;
				if(NULL != m_pDoc->ptrCMD48Dlg)
				{
					m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
				}
			}
		}
		if (m_pDoc == NULL || m_pDoc->pCurDev == NULL)
		{
			LOGIT(CERR_LOG,"ERROR: comm interface call without a path to the device.\n");
			return;
		}

		if((m_bMoreStatusSent == true) && (cmdNo == 48))
		{
			m_bMoreStatusSent = false;
		}
		else //either bMoreStatus == false OR cmdNo != 48
			{
			if((m_skipCount == 0) && (thisMsgCycle.devStatVal != 0))
				{
				//If Dialog exists update Device Status
			if(NULL != m_pDoc->ptrCMD48Dlg)
				{
				m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
			}
			rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
				if( rc == SUCCESS && pIB != NULL )
			{
				pVr = (hCVar*) pIB; // a cast operator
				if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
				{// make it a bit enum
					pBe = (hCBitEnum*)pVr;
					if (pBe)
					{
						unsigned char msk = 1;
						do
						{// for each bit set
							if ( thisMsgCycle.devStatVal & msk )
							{
									if(msk == DS_MORESTATUSAVAIL   && 
									   thisMsgCycle.devStatVal != DS_MORESTATUSAVAIL)//stevev prevent no popup
								{
									continue; // loop the while
								}
								rc = pBe->procureString(msk, hs);
								if ( rc == SUCCESS && ! hs.empty() )
								{//	log the string
										
										m_pDoc->m_strDisplay = hs.c_str();
						
										CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd;

										pMainWnd->SendMessage(WM_NOTIFY_RESPCODE);

										if(true == m_pDoc->m_bIgnoreRespCode)
										{
											m_skipCount = m_pDoc->m_uSuppressionCnt;
											if(true == m_pDoc->m_bIgnoreRespCode)
											{			
												/* VMKP 300104 add an indicator in the extended device status
												tab saying "Updations Suppressed"  */
												if(NULL != m_pDoc->ptrCMD48Dlg)
												{
													m_pDoc->ptrCMD48Dlg->UpdateSuppressMessageTab();	//27FEB2004 Modified  by ANOOP since the message suppressed was not getting displayed properly.*/
												}
											}

											break;
										}

									LOGIT(UI_LOG,"Device Status: %s",hs.c_str());
								}
									else
									{
										LOGIT(UI_LOG,"No Description Avaliable for %d in Device Status",msk);
									}
							}// else - not set, don't output string
						}while( (msk<<=1)  != 0);
					}
					else
					{
						LOGIT(CERR_LOG,"Device Status: failed to cast from hCVar.");
					}
				}
				else
				{
					LOGIT(CERR_LOG,"Device Status: failed to cast from hCItemBase to bit enum.");
				}			
				}//Endif rc == SUCCESS

				if(thisMsgCycle.devStatVal & DS_MORESTATUSAVAIL)// We aren't in our owm Command 48 reply
				{
					//Send Command 48
					//stevev - rc = m_pDoc->pCurDev->sendMethodCmd(48,-1);
					rc = m_pDoc->pCurDev->sendCommand(48,-1);
					m_bMoreStatusSent = true;

				}/*Endif DS_MORESTATUSAVAIL*/

			}
			else //either m_skipCount != 0 OR thisMsgCycle.devStatVal = 0
			{

				if(thisMsgCycle.devStatVal == 0)
				{
					m_skipCount = 0;
					
					if(NULL != m_pDoc->ptrCMD48Dlg)
					{			
						m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
					}

					if(NULL != m_pDoc->ptrCMD48Dlg)
			{
						m_pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();

			}	

		}
				else //simply decrement the skipCount
		{
					m_skipCount--;
		}

		}

		}/*End else  bMoreStatus == false OR cmdNo != 48*/

		if(cmdNo == 48)
		{

			LOGIT(UI_LOG,"Received Command 48 response");
			// need to expand the bits here

		/* VMKP 300104 Update the Command 48 response bytes (Extended Device status bytes)
			here, if u reach here you need to remove the "supressions removed" message
			from the extended device status tab and the update it */
			if(NULL != m_pDoc->ptrCMD48Dlg)
			{			
/*Vibhor 130204: Start of Code*/	
			//was	HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Status"));

				HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Condition"));
/*Vibhor 130204: End of Code*/	
				if(NULL != hwndCMD48Window)  //Added by ANOOP Fix for crash 15FEB2004
				{
					m_pDoc->ptrCMD48Dlg->m_hWnd=hwndCMD48Window;

					if(false == m_pDoc->ptrCMD48Dlg->m_ExtendedTabCreated )
					{
						::PostMessage(hwndCMD48Window, WM_CREATE_EXTENDEDTAB, NULL, NULL);
		}
					else
					{
						::PostMessage(hwndCMD48Window, WM_UPDATE_EXTENDEDTAB, NULL, NULL);
						
		}
	
		}
			
			}
			
		}

			
}/*End notifyRCDScmd48*/

/*Vibhor 120204: End of Code*/

// gets an empty packet for generating a command 
//		(gives caller ownership of the packet*)
//		pass back ownership with Sendxxx OR putMTPacket if command send aborted
//
hPkt* HARTDECommInfc::getMTPacket(bool pendOnMT)
{
	hPkt* pR = thePktQueue.GetEmptyObject(pendOnMT);
//clog <<"got  MT   Pkt "<<hex << pR   << dec << endl;
	return pR;
}

// returns the packet memory back to the interface 
//		(caller relinguishes ownership of the packet*)
//							
void  HARTDECommInfc::putMTPacket(hPkt* pPkt)
{	
//clog <<"return MT Pkt "<<hex << pPkt << dec << endl;
    thePktQueue.ReturnMTObject( pPkt );
}


RETURNCODE 
HARTDECommInfc::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = SUCCESS;

	hPkt* pPkt = getMTPacket() ;

	//if ( pPkt != NULL && m_enableComm )
	if ( pPkt != NULL && (m_CommStatus < commNC) )// OK or NoDev
	{
		pPkt->cmdNum = cmdNum;
		memcpy(pPkt->theData,pData,dataCnt);
		pPkt->dataCnt = dataCnt;
		return (SendPkt(pPkt, timeout, cmdStatus));
	}
	else
	{
		return DDL_COMM_QUEUE_FAILURE;
	}

}


// puts a packet into the send queue 	(takes ownership of the packet*)
RETURNCODE HARTDECommInfc::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = SUCCESS;

	if ( m_CommStatus > commNoDev ) // NC,shuttingdown,shutdown  was: ! m_enableComm )
	{
		thePktQueue.ReturnMTObject( pPkt );
		return DDL_COMM_SHUTDOWNINPROGRESS;
	}

	if ( pPkt == NULL)
	{
		cerr <<"ERROR: SendPkt has a null for a packet - discarding."<<endl;
		rc = DDL_COMM_PARAMETER_ERROR;
	}
	else
	{
		pPkt->timeout = timeout;
		// update Packet Log here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!XMIT
#ifdef DEBUG_TRANSACTION /* top of this file*/
		clog << "Queuing cmd " <<dec<< (int)(pPkt->cmdNum) << " with 0x" << hex << 
												(int)(pPkt->dataCnt) << dec << " bytes data."<<endl;		
#endif
//PutObject2Head(pPkt)
		thePktQueue.PutObject2Que(pPkt);// triggers event to get it sent
	}
	return rc;
}


// puts a packet into the send queue 	(takes ownership of the packet*)
RETURNCODE HARTDECommInfc::SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc = SUCCESS;

	if ( m_CommStatus > commNoDev ) // NC,shuttingdown,shutdown  was: ! m_enableComm )
	{
		thePktQueue.ReturnMTObject( pPkt );
		return DDL_COMM_SHUTDOWNINPROGRESS;
	}

	if ( pPkt == NULL)
	{
		cerr <<"ERROR: SendPriorityPkt has a null for a packet - discarding."<<endl;
		rc = DDL_COMM_PARAMETER_ERROR;
	}
	else
	{
		pPkt->timeout = timeout;
		// update Packet Log here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!XMIT
#ifdef DEBUG_TRANSACTION /* top of this file*/
		clog << "Que2Head cmd " <<dec<< (int)(pPkt->cmdNum) << " with 0x" << hex << 
									(int)(pPkt->dataCnt) << dec << " bytes data."<<endl;		
#endif
		thePktQueue.PutObject2Head(pPkt);// triggers event to get it sent
	}
	return rc;
}

// how many are in the send queue ( 0 == empty )
int HARTDECommInfc::sendQsize(void)
{
	return thePktQueue.GetQueueState();
}

// how many will fit	    
int	HARTDECommInfc::sendQmax(void) 
{// we may need to count #in queue + #still empty
	return DOCCOMMQSIZE;
}
	
// setup connection 	
RETURNCODE  HARTDECommInfc::initComm(void)
{
	RETURNCODE rc = SUCCESS;
	BYTE bzPortNumber;
	USES_CONVERSION;
	CComBSTR     bstrCommPort(m_pDoc->m_copyOfCmdLine.cl_Url);
	sscanf(OLE2A(bstrCommPort.m_str),"COM%d",&bzPortNumber);
	HCMRESULT retval = hartdemodem->InitializeModemDEInterface(bzPortNumber,9600,DOCCOMMRETRIES,0,15);

	if (retval == HART_DE_MODEM_COM_PORT_INIT_FAILED)
	{
		LOGIT(UI_LOG,(char *)"COM Port Initialization Failed");
		AfxMessageBox(_T("COM Port Initialization Failed"));
		rc = APP_CMD_COMM_ERR;
	}
	else if (retval == HART_DE_MODEM_NOT_PRESENT)
	{
		LOGIT(UI_LOG,(char *)"No HARTDE Modem Present");
		AfxMessageBox(_T("No HARTDE Modem Present"));
		rc = APP_HARTDE_MODEM_NOT_PRESENT;
	}
	else if (retval == HART_DE_MODEM_SET_PROTOCOL_FAIL)
	{
		LOGIT(UI_LOG,(char *)"HARTDE Modem Protocol set failed");
		AfxMessageBox(_T("HARTDE Modem Protocol set failed"));
		rc = APP_HART_DE_MODEM_SET_PROTOCOL_FAIL;
	}

	RegisterWaitForSingleObject(& hRegisteredTask, thePktQueue.GetArrivalEvent(), 
							gfHARTDEWaitAndTimerCallback,(void*)this, INFINITE, WT_EXECUTEDEFAULT);
	// generate background task to handle the xmit packets and call our 
	// using RegisterWait ForSingleObject(QueueEvent)
	return rc;
}

RETURNCODE  HARTDECommInfc::shutdownComm(void)
{
	RETURNCODE rc = SUCCESS;

clog <<"< doc-comm infc shutting down."<<endl;
	if (m_CommStatus > commNC )	// shuttingdown|shutdown  was:  ! m_enableComm)
	{// we're already shutdown, just leave
clog <<"< communications interface already gone (*)"<<endl;
		return rc;
	}// else do the sequence

	/*
	m_CommDisabled =		// will be true when it's shutdown
	m_enableComm           = false; // disable any more sends
	*/
	m_CommStatus = commShuttingDown;
clog <<"< comm status set to ShuttingDown."<<endl;
//clog <<"< communications interface setting disabled false and enable false. ";
//clog <<"   "<< m_CommDisabled << "   "<< m_enableComm <<" (4)"<<endl;
/* stevev 9/19/03 - this is no longer needed in the shutdown sequence
	if ( thePktQueue.GetQueueState()>0)
	{
		m_msgCompleteHandshake = false;//force it to wait for the message finished
	}
	else
	{
		m_msgCompleteHandshake = true;// let it fall through
	}
	do
	{
		Sleep(100);
	}
	while (thePktQueue.GetQueueState()>0 || // wait for the send queue to empty
		(! m_msgCompleteHandshake ) );      // and the last message to be handled
	// the command reception will process one at a time
*/
#if 0	
	if (hRegisteredTask != 0)
	{
		UnregisterWait( hRegisteredTask ) ;
		hRegisteredTask = 0;
clog <<"< communications interface has unregistered the task. (15)"<<endl;
	}
	int c = 0;
	do
	{
		Sleep(10);
		c++;
	}
	while (  (m_CommStatus < commShutDown )  && c < 1000 );// wait 10 secs 4 the comm infc to be shutdown
	/*
	while (  (! m_CommDisabled )  && c < 1000 );// wait 10 secs 4 the comm infc to be shutdown
	*/
/* stevev 9/19/03 - replaced
	while (  thePktQueue.GetQueueState()>0 ||    // wait for the send queue to empty
		      (! m_CommDisabled )             ); // and the comm infc to be shutdown
*/
	if ( c >= 1000 )
	{
clog <<"< communications interface has not set m_CommDisabled. (18)"<<endl;
		cerr 
			<<"ERROR: communication interface timed out while waiting for shutdown to complete."
			<<endl;
	}
clog <<"< communications interface is done. (19)"<<endl;
#endif

//	hartdemodem->UnInitializeModemDEInterface();
//	delete hartdemodem;
	return rc;
}

RETURNCODE HARTDECommInfc::GetIdentity(Indentity_t& retIdentity, BYTE pollAddr)
{
	RETURNCODE rc = FAILURE;
	DWORD devpresent;

	if ( m_pDoc == NULL || pollAddr >= DOCCOMMMAXPOLLADDR)  //pComm == NULL ||
	{
		return DDL_COMM_PARAMETER_ERROR;
	}
	

	retIdentity.clear();

//	m_pollAddr   = m_pDoc->m_copyOfCmdLine.cl_PollAddr;

	VARIANT_BOOL isPresent;
	CComBSTR     bstrCommPort(m_pDoc->m_copyOfCmdLine.cl_Url);

//	HRESULT hr = pComm->IsInstrumentAtPollAddress(bstrCommPort, pollAddr, &isPresent);

	devpresent = hartdemodem->GetHARTDevUniqueID(pollAddr);
	
	if (devpresent)
		isPresent= true;
	else
		isPresent= false;


	if (isPresent) 
	{
		m_pollAddr = pollAddr;
		if ( (rc = connect()) == SUCCESS )
		{
			clog << "Successful connection at poll address " << (int)m_pollAddr << endl;
		}
		else
		{// connection failure
			cerr << "ERROR: Device Identity could not be established." << endl;
		}
	}
	else
	{
		cerr << "ERROR: Device is not at Poll Address " << m_pollAddr << endl;
		rc = DDL_COMM_DEVICE_NOT_AT_POLL;
	}

	if ( rc == SUCCESS && m_connected )
	{// aquire identity
		hPkt xP, rP;
		xP.cmdNum = 0;
		xP.dataCnt= 0;
		rc = rawSend(&xP, &rP);
		if ( rc == SUCCESS)
		{
			rc = ZeroPkt2Identity(&rP, retIdentity);
			// return the response code and identity
		}
		else
		{
			disconnect();
			m_connected = false;
		}
	}
	// else - just return the code

	return rc;
}

unsigned short HARTDECommInfc::FormHARTRequestPacket(hPkt* pSendPacket,BYTE *HARTMsg)
{
	BYTE checksum = 0,i;
	unsigned short index = 0;

	HARTMsg[index++] =  0x82; //Delimiter (secondary master)
	HARTMsg[index++] =	hartdemodem->slaveid[m_commHndl - 1].ManufacturerID;
	HARTMsg[index++] =	hartdemodem->slaveid[m_commHndl - 1].DeviceType;
	HARTMsg[index++] = 	(hartdemodem->slaveid[m_commHndl - 1].DeviceID >> 16) & 0xff;
	HARTMsg[index++] = 	(hartdemodem->slaveid[m_commHndl - 1].DeviceID >> 8) & 0xff;
	HARTMsg[index++] = 	(hartdemodem->slaveid[m_commHndl - 1].DeviceID) & 0xff;
	HARTMsg[index++] =  pSendPacket->cmdNum;
	HARTMsg[index++] =  pSendPacket->dataCnt;

	for (i=0; i < pSendPacket->dataCnt; i++)
		HARTMsg[index++] = pSendPacket->theData[i];

	for(i = 0 ; i < index ; i++)
		checksum ^= HARTMsg[i];

	HARTMsg[index]= checksum;

return index+1;
}


RETURNCODE HARTDECommInfc::rawSend(hPkt* pSendPacket, hPkt* pRecvPacket)
{
	RETURNCODE rc = SUCCESS;
	long    i;
	string  eStr;
	if (m_commHndl == 0 || ! m_connected || m_CommStatus == commNC)
	{
		if (pRecvPacket) pRecvPacket->dataCnt = 0;
		return DDL_COMM_PARAMETER_ERROR;
	}
	if ( (m_CommStatus > commNC/*was: ! m_enableComm*/) ||  
		pSendPacket == NULL || pRecvPacket == NULL )
	{
clog <<"> rawSend w/Comm not enabled - doing nothing. Cmd ";
if (pSendPacket) clog <<pSendPacket->cmdNum ;
clog <<" (10)"<<endl;
		if (pRecvPacket) pRecvPacket->dataCnt = 0;		
		return DDL_COMM_SHUTDOWNINPROGRESS; // do no work
	}
	
// New code for Packet dump Jeyabal
	CClient *pSocket = NULL;
	if (m_pDoc)
		pSocket = m_pDoc->GetSocket();
// End - New code for Packet dump Jeyabal


// New code for Packet dump Jeyabal
	if (pSocket)
	{
		char strBuff[MAX_PACKET_STRING];
		strcpy(strBuff,"Stx");

		char strTemp[MAX_PACKET_STRING];
		memset(strTemp,0,MAX_PACKET_STRING);

		sprintf(strTemp, " %s  %03d", strBuff, pSendPacket->cmdNum);
		strcpy(strBuff, strTemp);

		sprintf(strTemp, " %s  %02d", strBuff, pSendPacket->dataCnt);
		strcpy(strBuff, strTemp);

		// Pack bytes for Status bits
//			sprintf(strTemp,"%s %02x %02x ", strBuff, 0, 0);
		sprintf(strTemp,"%s        ", strBuff, 0, 0);
		//			if ( pPkt->dataCnt > 0 )
		//			{
		//				sprintf(strTemp,"%s __ __ ", strBuff);
		//			}
		//			else
		//			{
		//				sprintf(strTemp,"%s ", strBuff);
		//			}
		strcpy(strBuff, strTemp);
		int nCount;	// PAW		
		for (/*int PAW*/ nCount = 0; nCount < pSendPacket->dataCnt - (pSendPacket->dataCnt%2); nCount+=2)
		{
			sprintf(strTemp,"%02x%02x  ", pSendPacket->theData[nCount], pSendPacket->theData[nCount+1]);
			strcat(strBuff,strTemp);
		}
		for (nCount = 0; nCount < (pSendPacket->dataCnt%2); nCount++)
		{
			sprintf(strTemp,"%02x", pSendPacket->theData[nCount]);
			strcat(strBuff,strTemp);
		}
		int y = strlen(strBuff);
		strBuff[y++] = 22; strBuff[y] = '\0';
		pSocket->SendMessage(strBuff, y);
	}
// End - New code for Packet dump
	BYTE HARTMsg[300];
	BYTE HARTRespMesLength;

	unsigned short HARTReqMesLength = FormHARTRequestPacket(pSendPacket,HARTMsg);


	if (!HARTReqMesLength)
	{
		return HARTReqMesLength;
	}
	else
	{
#ifdef DEBUG_TRANSACTION /* top of this file*/		
		clog << "Sending cmd " << pSendPacket->cmdNum << " with 0x" << hex << 
									(int)(pSendPacket->dataCnt) <<dec<< " bytes data."<<endl;
#endif
		;// Log the packet to the serial logger
	}
 
/* VMKP added on 160204 */
	HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);

	if (hwndMainWindow != NULL)
	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
		::PostMessage(hwndMainWindow, WM_SDC_GLOW_RED, NULL, NULL);		
		systemSleep(1);
	}

	// stevev 28aug09 - copy some stuff
	pRecvPacket->timeout       = pSendPacket->timeout;
	pRecvPacket->transactionID = pSendPacket->transactionID;
	pRecvPacket->byCmdNum      = pSendPacket->byCmdNum;
   
/* END of VMKP added on 160204 */
//	if (m_pMainFrm) m_pMainFrm->SetTransmitterLight(true);//true == green
	// Send HART Command and Receive Reply-------------- status is a copy of the returned hr
	// Send HART Command and Receive Reply-------------- status is a copy of the returned hr
//	DWORD hr = pComm->Send(m_commHndl, varCommand, &varReply, &status );
   HCMRESULT hr = hartdemodem->SendCommand(HARTMsg,(UINT) HARTReqMesLength, NULL);
//	if (m_pMainFrm) m_pMainFrm->SetTransmitterLight(false);//true == Red

/* VMKP added on 160204 */
	if (hwndMainWindow != NULL)
	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
		::PostMessage(hwndMainWindow, WM_SDC_GLOW_GREEN, NULL, NULL);
	}
/* END of VMKP added on 160204 */


   
	
	if (hr != HART_DE_MODEM_SUCCESS)
	{
//		AfxMessageBox("Error In Communication with Device, Closing the Device...");
		getErrorStr(hr, eStr);
		cerr << "ERROR: rawSend SEND error: "<<eStr<<endl;
		clog << "ERROR: rawSend Send error. "<<endl;
		pRecvPacket->cmdNum  = pSendPacket->cmdNum; // put it back so we can find the msg queue
		pRecvPacket->dataCnt = 0xFF;
		pRecvPacket->timeout = (int)hr;
		rc = DDL_COMMUNICATIONS_FAILED;
	}
	else if (pSendPacket->cmdNum != HARTMsg[6]) // if received command number not equal to 
	{											// send one
//		AfxMessageBox("Error In Communication with Device, Closing the Device...");

		getErrorStr(hr, eStr);
		cerr << "ERROR: rawSend SEND error: "<<eStr<<endl;
		clog << "ERROR: rawSend Send error. "<<endl;
		pRecvPacket->cmdNum  = pSendPacket->cmdNum; // put it back so we can find the msg queue
		pRecvPacket->dataCnt = 0xFF;
		pRecvPacket->timeout = (int)hr;
		rc = DDL_COMMUNICATIONS_FAILED;

	}
	else
	{	
		HARTRespMesLength = hartdemodem->GetHARTResponse(HARTMsg);

		pRecvPacket->dataCnt = HARTMsg[7];
		pRecvPacket->cmdNum  = HARTMsg[6];
	
//		TRACE("Received Response for Command No. = %d\n\n",HARTMsg[6]);

		for (i=0; i < pRecvPacket->dataCnt; i++) 
		{
			pRecvPacket->theData[i] = HARTMsg[8+i];
		}

		pRecvPacket->byIsBurstMode = hartdemodem->slaveid[m_commHndl - 1].byIsBurstMode;
		pRecvPacket->byCmdNum = hartdemodem->slaveid[m_commHndl - 1].byCmdNum;

		// if no response, replace command as modem server overwrites it with 252
		if (HARTMsg[8] == 0xff) 
		{
			if (pRecvPacket->cmdNum != 252)
			{
//				LOGIT(CLOG_LOG,"WARNING: Received comm status of 0x%02x with command set to %d.\n",
//											commStat,pRecvPacket->cmdNum);
			}
			pRecvPacket->cmdNum = pSendPacket->cmdNum;
			pRecvPacket->dataCnt = 0xFF;
			pRecvPacket->timeout = (int)hr;
			rc = DDL_COMM_NO_RESPONSE;
		}
		else // responded
		{// try to keep up
			// if command 6 executed successfully, device polling address has changed
			if (pSendPacket->cmdNum == 6  &&  HARTMsg[8] == 0 && pRecvPacket->dataCnt >= 3) 
			{
				m_pollAddr = pRecvPacket->theData[2];
				// TODO: - set symbolID to value, set valid
			}
		}
	}
	return rc;
}

RETURNCODE HARTDECommInfc::connect(void)
{
	RETURNCODE rc = SUCCESS;
	if (m_connected)
		disconnect();

	// ** Establish a Connection **
	
    CComBSTR     bstrCommPort(LPCTSTR(m_pDoc->m_copyOfCmdLine.cl_Url));

//	HRESULT hr = pComm->ConnectByPoll(bstrCommPort, m_pollAddr, 0, &m_commHndl );

	DWORD devpresent = hartdemodem->GetHARTDevUniqueID(m_pollAddr);
	
	if (devpresent)
		m_commHndl= m_pollAddr + 1;
	else
		m_commHndl= false;
	
//	string erStr;
//	getErrorStr(hr, erStr);

	if (m_commHndl == 0) 
	{	// valid handles are non-zero
		cerr << "ERROR: Connection to polling address "<< (int)m_pollAddr << " failed."<<endl;
		m_connected = false;
		rc = DDL_COMM_CONBYPOLL_FAILED;
	}
   else 
   {
	   m_connected = true;
	   clog <<"Connected to device at polling address "<< (int)m_pollAddr <<endl;
	   // TODO: if pollAddress symbol exists, update it and set it to valid
   }

	m_pMainFrm = (CMainFrame *)(AfxGetApp()->m_pMainWnd);
   return rc;
}

RETURNCODE HARTDECommInfc::disconnect(void)
{
#if 0
	if (m_connected && m_commHndl) 
	{
		HRESULT hr = pComm->Disconnect(m_commHndl);
		if ( hr != 0 )
		{
			string erStr;
			getErrorStr(hr, erStr);
			cerr << "ERROR: Device disconnect error :"<<erStr<<endl;
			return DDL_COMM_DISCONNECT_FAILED;
		}
		else
		{
			clog <<"Disconnected from device at polling address "<< (int)m_pollAddr <<endl;
			// TODO: if polladdress symbol id exists, set to FF and invalid
		}
	}
#endif

	m_connected = false;
	m_pMainFrm  = NULL;
	return SUCCESS;
}

RETURNCODE HARTDECommInfc::Search(Indentity_t& retIdentity, BYTE& newPollAddr)
{
	RETURNCODE rc = SUCCESS;

#if 0
	if ( m_pDoc == NULL ) //pComm == NULL || 
	{
		return DDL_COMM_PARAMETER_ERROR;
	}	

	VARIANT_BOOL  isPresent;
	CComBSTR      bstrCommPort(m_pDoc->m_copyOfCmdLine.cl_Url);
	
	BYTE pollAddress = 0;
	BYTE holdAddress = m_pollAddr;
	bool deviceFound = false;
	HRESULT hr       = 0;

	retIdentity.clear();

	while (pollAddress < DOCCOMMMAXPOLLADDR) 
	{
		hr = pComm->IsInstrumentAtPollAddress(bstrCommPort, pollAddress, &isPresent);
		if (isPresent) 
		{
			rc = GetIdentity(retIdentity, pollAddress);// connects
			if ( rc == SUCCESS && m_connected )
			{
				deviceFound = true;
				break; // out of while loop
			}
			else
			{
				if ( m_connected )
				{	disconnect();}

				m_pollAddr = holdAddress;
				retIdentity.clear();
			}
		}
		else 
		{	// any error OR not found, increment and try again
			pollAddress++;
		}
	}//wend

	if (!deviceFound) 
	{
		cerr << "ERROR: docComm's Search cannot find a device" << endl;
		rc = DDL_COMM_SEARCH_FAILED;
	}
	else
	{
		newPollAddr = pollAddress;

	}
#endif
	return rc;
}

void  HARTDECommInfc::disableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: disable App Comm with no Document.");
	}
	else
	{
		m_pDoc->m_autoUpdateDisabled = true;
	}
}

void  HARTDECommInfc::enableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: enable App Comm with no Document.");
	}
	else
	{
		m_pDoc->m_autoUpdateDisabled = false;
	}
}


bool  HARTDECommInfc::appCommEnabled (void)
{
    if (m_pDoc == NULL)
    {
    	LOGIT(CERR_LOG,"ERROR: isEnable App Comm with no Document.");
		return false;
    }
    else
    {
    	return ! (m_pDoc->m_autoUpdateDisabled);
    }
}

void  HARTDECommInfc::getErrorStr(HRESULT hR, string& retStr)
{
	
	if (hR == S_OK) 
	{
		retStr = "HModemSvr: function was successful.";
	}
	else 
	if (hR == E_ACCESSDENIED) 
	{
		retStr = "HModemSvr: Cannot aquire exclusive rights.";
	}
	else 
	if (hR == E_FAIL) 
	{
		retStr = "HModemSvr: Unable to access connection to device.";
	}
	else 
	if (hR == E_INVALIDARG) 
	{
		retStr = "HModemSvr: Argument contains invalid characters.";
	}
	else 
	if (hR == E_OUTOFMEMORY) 
	{
		retStr = "HModemSvr: Unable to allocate memory.";
	}
	else 
	if (hR == E_HANDLE) 
	{
		retStr = "HModemSvr: Invalid handle.";
	}
	else 
	if (hR == E_NOTIMPL) 
	{
		retStr = "HModemSvr: Not implemented.";
	}
	//RPC_E_WRONG_THREAD = _HRESULT_TYPEDEF_(0x8001010EL)
	else 
	{
		char t[80];
		sprintf(t,"0x%x",hR);
		retStr = "HModemSvr: Error code " ;
		retStr += t;
	}
}


void  HARTDECommInfc::commInfcTask(waitStatus_t isTimerExpired)
{// called when a packet is inserted into the pktqueue
 // timer is infinite so the boolean should never be true
	// this is the task where the system pends on a response
	RETURNCODE rc = SUCCESS;
	hPkt* pSendPkt;
	hPkt  rcvPkt;
	rcvPkt.clear();
	cmdInfo_t myCmdInfo;myCmdInfo.clear();


// New code for Packet dump Jeyabal
	CClient *pSocket = NULL;
	if (m_pDoc)
		pSocket = m_pDoc->GetSocket();
// End - New code for Packet dump Jeyabal

	if (isTimerExpired)
	{
		clog << "Comm interface task called with a timer expiration."<<endl;
	}


	if ( m_CommStatus > commNoDev ) // was:  ! m_enableComm )
	{
clog <<"> commInfcTask w/Comm not enabled - flushing packet queue. (8)"<<endl;
		thePktQueue.FlushQueue();// forces fall through to cleanup at the end
	}
	else
	if ( thePktQueue.GetQueueState() <= 0 )
	{
clog <<"> commInfcTask w/empty queue - exiting. (98)"<<endl;
return;
	}

	while (thePktQueue.GetQueueState() > 0)
	{
		/* Enter Critical section of sendin command to HARTDE */
  		EnterCriticalSection(&HARTDESendCriticalSection);

		pSendPkt = thePktQueue.GetNextObject();
		if ( pSendPkt != NULL)
		{
			rc = rawSend(pSendPkt, &rcvPkt);//syncronous send
			if ( rc == SUCCESS)
			{	// parse the packet
				if (cpRcvService != NULL )
				{
					if ( m_CommStatus == commNoDev || m_CommStatus == commNC )
				{
						m_CommStatus = commOK;
					}

					// UPDATE Packet Log Here !!!!!!!!!!! rcvPkt !!!!!!RECEIVE				   
#ifdef DEBUG_TRANSACTION /* top of this file*/
					clog << "Recving cmd " << (int)(rcvPkt.cmdNum) <<
					" with 0x" << hex <<(int)(rcvPkt.dataCnt) <<dec<< " bytes data."<<endl;
#endif                  
					// New code for Packet dump Jeyabal
							if (pSocket)
							{
								char strBuff[MAX_PACKET_STRING];
								strcpy(strBuff,"Ack");

								char strTemp[MAX_PACKET_STRING];
								memset(strTemp,0,MAX_PACKET_STRING);

								sprintf(strTemp, " %s  %03d", strBuff, rcvPkt.cmdNum);
								strcpy(strBuff, strTemp);

								sprintf(strTemp, " %s  %02d", strBuff, rcvPkt.dataCnt);
								strcpy(strBuff, strTemp);

								if (rcvPkt.dataCnt >= 2)
								{
									// Pack bytes for Status bits
									sprintf(strTemp,"%s %02x %02x  ", strBuff, rcvPkt.theData[0], rcvPkt.theData[1]);
									strcpy(strBuff, strTemp);
								}
								
								for (int nCount = 2; nCount < rcvPkt.dataCnt; nCount+=2)
								{
									sprintf(strTemp,"%02x%02x  ", rcvPkt.theData[nCount], rcvPkt.theData[nCount+1]);
									strcat(strBuff,strTemp);
								}

								int y = strlen(strBuff);
								strBuff[y++] = 22; strBuff[y] = '\0';
								pSocket->SendMessage(strBuff, y);//strlen(strBuff));
							}
					// End - New code for Packet dump

					if ( m_CommStatus > commNoDev ) // was:   ! m_enableComm )
					{
						myCmdInfo.discardMsg = true;
					}
					cpRcvService->serviceReceivePacket(&rcvPkt,&myCmdInfo);
				}
				else//cpRcvService == NULL
				{
				   cerr <<"ERROR: no service function to the receive packet."<<endl;
				   rc = DDL_COMM_PROGRAMMER_ERROR;
				}
			}
			else // send failed, //else // rawsend not success
			{//handle it
				clog << "Comm interface task raw send failed with "<<rc<<"."<<endl;
				if (cpRcvService != NULL )
				{
					if ( m_CommStatus > commNoDev ) // was:   ! m_enableComm )
					{
						myCmdInfo.discardMsg = true;
					}
					/* At times On Device/Exit, this (serviceReceivePacket) is calling 
						after closing the device, to avoid this check for the device close
						before calling serviceReceivePacket */
					if(m_pDoc->pCurDev)
						cpRcvService->serviceReceivePacket(&rcvPkt,&myCmdInfo);// triggers event w/ msgCyc
				}
				if ((long)(rcvPkt.timeout) == E_FAIL) 
				{
					if ( m_CommStatus == commOK)
					{
						m_CommStatus = commNoDev;
					}
				}
				if (myCmdInfo.pOtherData && ((hMsgCycle_t*)myCmdInfo.pOtherData)->actionsEn )
				{// we have a host error
		// won't work for methods >>>>>>>>>>>>>>>>>>>>>>			
					if ( m_pDoc ) 
					{  // we have a doc ptr && not shutdown scenario
						string  eStr;
						long    status = (long)(rcvPkt.timeout);	
						getErrorStr(status, eStr);
						m_pDoc->HostCommErrorCallback(status, eStr); //can't do this if actions disabled (in a method)
						clog <<">> Raw send Failed >>>>>>>>"<<endl;
					}
					else 
					{ 
						cerr << "ERROR: no document pointer in commInfcTask."<<endl;
					}
				}
				thePktQueue.FlushQueue();
				clog <<"> PacketQueue Flushed."<<endl;
			}
		}
		//else queue error, we should never have gotton here
		if ( pSendPkt != NULL )
		{
			thePktQueue.ReturnMTObject( pSendPkt );
		}
		rcvPkt.clear();
	}//wend there are more packets to send

		/* Enter Critical section of sendin command to HARTDE */
  		LeaveCriticalSection(&HARTDESendCriticalSection);

	if ( m_CommStatus > commNoDev ) // was:   ! m_enableComm )
	{
clog <<"> commInfcTask w/Comm not enabled - flushing packet queue. (8)"<<endl;
		thePktQueue.FlushQueue();

		// m_CommDisabled = true;// handshake, we be done
		m_CommStatus = commShutDown;
clog <<"> commInfcTask w/Comm not enabled - commDisabled now true, returning. (15)"<<endl;
		//return;// don't do anything else
	}
	if ( myCmdInfo.pOtherData ) delete myCmdInfo.pOtherData;
	return;

}



/*************************************************************************************************
 *
 *   $History: HARTDECommInfc.cpp $
 * 
 * *****************  Version 3  *****************
 * User:        Date:     Time: 
 * Updated in $
 * Added/Verified All files had HART standard headers and Footers.
 *
 *************************************************************************************************
 */
