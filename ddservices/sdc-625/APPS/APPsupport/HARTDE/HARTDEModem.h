/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL INC IS PROHIBITED  EXCEPT AS AUTHORIZED      */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2003 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/**************************************************************************
**
** APPLICATION: HART/DE Modem Interface
** HEADER NAME: HARTDEModem.h
**
** PURPOSE: Contains class and Member function definitions for the HART/DE Modem
**			Interface
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0	12/01/04	Prasad S	initial release after the code review
**
** Date...:12/01/04			
** Author.:Prasad S
** Comment:made from file
**
***************************************************************************/

/**************************************************************************/
#ifndef _HARTDEMODEM_H
#define _HARTDEMODEM_H

#include "defines.h"
#include "HARTDEDefs.h"
#include "spm.h"

//#define		_HARTDE_DEBUG_			/* enables debug statements */

/* Structure to store all the 16 device 38-bit Unique Identifiers
   along with their poll addresse */
typedef struct
{
	BYTE poll_address;
	BYTE ManufacturerID;
	BYTE DeviceType;
	UINT DeviceID;
	BYTE byIsBurstMode;
	BYTE byCmdNum;
}Slave_ID;

#define	MAX_HART_MESSAGE_SIZE			300 /* Maximum HART Message size*/

/* Structure to store the subscription parameters of the modem */
typedef struct
{
	BYTE Subscribe_Flag;
	BYTE Parameter_val[2];
}SubscribedModemParameter;

enum HCMRESULT
{
	//HARTDE Modem messages
	HART_DE_MODEM_INIT_SUCCESS = 5000,
	HART_DE_MODEM_COM_PORT_INIT_FAILED,
	HART_DE_MODEM_DEV_ADDR_WRONG,
	HART_DE_MODEM_NOT_PRESENT,
	HART_DE_MODEM_SET_PROTOCOL_FAIL,
	HART_DE_MODEM_SCAN_THREAD_CREATE_FAIL,
	HART_DE_MODEM_DEV_SCAN_THREAD_CREATE_FAIL,
	HART_DE_MODEM_BURST_MODE_THREAD_CREATE_FAIL,
	HART_DE_MODEM_SUCCESS,
	HART_DE_MODEM_FAIL,
	HART_DE_MODEM_WRITE_PARAM_SUCCESS,
	HART_DE_MODEM_WRITE_PARAM_FAIL,
	HART_DE_MODEM_PREAMBLES,
	HART_DE_MODEM_MODE,
	HART_DE_MODEM_PROTOCOL,
	HART_DE_MODEM_FIRMWARE_VERSION,
	HART_DE_MODEM_STATUS,
	HART_DE_MODEM_MAX_COMMANDS,
	HART_DE_MODEM_PARAMETER_ERROR
};


/* ---- Definitions: ------------------------------------------------------ */
class CHartDEModem
{
private:

	/* Handle and thread id of the Device scan thread */
	HANDLE m_hDevScanThread;
	DWORD  m_dwDevScanThreadId;

	/* Handle and thread id of the Modem Parameter scan thread */
	HANDLE m_hModemScanThread;
	DWORD  m_dwModemScanThreadId;

	/* Handle and thread id of the Burst Mode Handler Parameter scan thread */
	HANDLE m_hBurstHandleThread;
	DWORD  m_dwBustHandleThreadId;

	BYTE m_byExitThreads;

public:

	/* class for accessing serial port */
	CSPM cspm;

	/* Global Queues and variable required for writing and reading data to the
	  serial port */
	CMessageQueue pInputQueue,pOutputQueue;
	ErrorCallbackFunction *pfErrorFunction;
	BYTE out_buf[MAX_HART_MESSAGE_SIZE],in_buf[MAX_HART_MESSAGE_SIZE],HART_response[MAX_HART_MESSAGE_SIZE];
	UINT out_buf_len,in_buf_len;
	/* The Critical Section Objects for the HART Device and HART modem */
	CRITICAL_SECTION HARTDevCriticalSection,HARTModemCriticalSection;
	BYTE DevStartAddr,DevEndAddr,ClientRequest,RetryCount;
	Slave_ID slaveid[MAX_MODEM_HART_DEVICES];

	/* Subscribed modem parameters */
	SubscribedModemParameter modemparam[HART_DE_MODEM_MAX_READ_COMMANDS];

	/* Constructor */
	CHartDEModem();

	/* Destructor */
	~CHartDEModem();

	/* Initialize HARTDE Modem, the required paramters are
	  Port number to be initialized, , Serial port Baud rate,
	  Number of retries for Port to open,
	  Device Poll addresses (Starting and Ending address) */
	HCMRESULT InitializeModemDEInterface(  BYTE nPortNumber
										, UINT nBaudRate
										, BYTE byRetryCount
										, BYTE byDevStartAddr
										, BYTE byDevEndAddr
									   );
	/* UnInitialize HARTDE Modem Parameters */
	HCMRESULT UnInitializeModemDEInterface();



	/* Method by with command to be sent to the HART/DE Modem */
	HCMRESULT WriteParameters(DWORD* CommandID,
								BYTE *Data,
								UINT NoofCommands,
								DWORD dwTransId);


	/* Method by with command to be sent to the Hart device */
	HCMRESULT SendCommand(BYTE *pCmdBuff
						,UINT nCmdBuffSize
						,DWORD dwTransId);

	/* Scans the network starting from byDevStartAddr and ending at
	   byDevEndAddr */
	HCMRESULT GetNetworkHierarchy(void);

	/* Rebuild the network starting from byDevStartAddr and ending at
	   byDevEndAddr */
	HCMRESULT RebuildNetworkHierarchy(void);


	/* Subscribe to the HART/DE Modem parameters */
	HCMRESULT SubscribeModemParameters( DWORD *nParameters
							 , UINT nParamCount
							 , DWORD dwTransId);

	/* UnSubscribe to the HART/DE Modem parameters */
	HCMRESULT UnSubscribeModemParameters(DWORD *nParameters
								, UINT nParamCount
								, DWORD dwTransId);

	/* The Device scanning thread, for scanning devices connected to HART/DE modem */
	DWORD DeviceScanning();

	/* Reads the HART device unique ID using polling address */
	DWORD GetHARTDevUniqueID(BYTE);

	/* scan a single device for specified polling address */
	DWORD ScanNetworkDevice(BYTE);

	/* scan HART/DE Modem parameters */
	DWORD ScanHARTDEModemParameters();

	BYTE IsBurstMessage();

	/* The Burst Message Handling thread */
	DWORD HandleBurstMessage();

	/* Get modem command for specified modem address with HART command message */
	UINT GetModemCommand(BYTE , BYTE *, UINT);

	/* Send the command to modem and get the response */
	DWORD SendAndGetDataFromModem();

	/* Get HART DE Modem status */
	BYTE GetHARTDEModemStatus();

	/* Get HART DE Modem Protocol to HART */
	BYTE SetHARTDEModemProtocol();

	/* Extract HART response from modem response */
	UINT GetHARTResponse(BYTE *);

	/* Validate HART device response */
	BYTE ValidateHARTResponse(BYTE *,BYTE);


	BYTE GetHARTDEModemProtocol();
	BYTE GetHARTDENetworkState();
};


#endif /* End of HARTDEModem.h */
