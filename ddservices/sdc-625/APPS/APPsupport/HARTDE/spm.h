/*C*/
/***************************************************************************/
/*                                                                         */
/*                  HONEYWELL CONFIDENTIAL & PROPRIETARY                   */
/*                                                                         */
/*    THIS  WORK   CONTAINS  VALUABLE   CONFIDENTIAL  AND  PROPRIETARY     */
/*    INFORMATION:   DISCLOSURE;   USE  OR  REPRODUCTION   OUTSIDE  OF     */
/*    HONEYWELL INTERNATIONAL IS PROHIBITED  EXCEPT AS AUTHORIZED	       */
/*    IN  WRITING.  THIS UNPUBLISHED WORK IS PROTECTED  BY THE LAWS OF     */
/*    THE  UNITED  STATES  AND  OTHER  COUNTRIES.   IN  THE  EVENT  OF     */
/*    PUBLICATION   THE  FOLLOWING  NOTICE  SHALL  APPLY:                  */
/*    "COPYRIGHT 2002 HONEYWELL INTERNATIONAL ALL RIGHTS RESERVED."        */
/*                                                                         */
/***************************************************************************/
/*I*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
** HEADER NAME: SPM.h
**
** PURPOSE: The header file that contains the definitions of the SPM class
**	
**
**
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 07-May-02  TSR Prasad  initial release
**
** Date...: 07-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/
#ifndef SPM_H
#define SPM_H

/* --- Standard Includes ------------------------------------------------- */

/* --- Module Includes --------------------------------------------------- */
#include "queue.h"
#include "spmif.h"

/* --- Definitions ------------------------------------------------------- */
/* Enumerations for flow control*/
enum FLOW_CONTROL
{
	NO_FLOW_CONTROL
	, HARDWARE_FLOW_CONTROL
	, XON_XOFF_FLOW_CONTROL
};

/* The maximum length of the Port Name */
#define MAX_PORT_NAME_LENGTH		50

/* The maximum size of the buffer used send & recieve messages from the port */
#define SPM_MAX_BUFFER_SIZE			1300

/* The number of times a write to port is retried before raising the error */
#define NUMBER_OF_WRITE_RETRIES		10

/* The time interval in millisecs between 2 message writes to a port */
#define WRITE_INTERVAL				0

/* The time interval in millisecs to sleep when there is no data to be written
or no data to read */
#define IDLE_INTERVAL				10


/* Communication timeout definitions */
/* Maximum time, in milliseconds, allowed to elapse between the arrival of 
two characters on the communications line */
#define	READ_INTERVAL_TIMEOUT		MAXDWORD

/* Multiplier, in milliseconds, used to calculate the total time-out period 
for read operations. For each read operation, this value is multiplied by the requested 
number of bytes to be read */
#define READ_TOTAL_TIMEOUT_MULT		0

/* Constant, in milliseconds, used to calculate the total time-out period for 
read operations. */
#define READ_TOTAL_TIMEOUT_CONST	0

/* Multiplier, in milliseconds, used to calculate the total time-out period for 
write operations. For each write operation, this value is multiplied by the number of 
bytes to be written.  */
#define WRITE_TOTAL_TIMEOUT_MULT	0

/* Constant, in milliseconds, used to calculate the total time-out period for write 
operations. For each write operation, this value is added to the product of the 
WriteTotalTimeoutMultiplier member and the number of bytes to be written.  */
#define WRITE_TOTAL_TIMEOUT_CONST	0

/* The type definition of the function pointer which is invoked on a COM port 
error condition */
typedef	void (*ErrorCallbackFunction) ();

class CSPM
{
public:

	/* Flag that is set to TRUE when a port is open */
	BOOL m_bPortOpen;

private:

	/* Flag that is set to TRUE when initialization is done */
	BOOL m_bInitialized;

	/* Flag that must be set when we need to close the port, the status of this flag
	is only valid if m_bPortOpen is TRUE */
	BOOL m_bClosePort;

	/* Flag that would be set if we need to reconfigure the port */
	BOOL m_bReconfigureFlag;

	/* Flag that would be set if we need to Uninitialize */
	BOOL m_bUninitFlag;

	/* The pointers to the input and output message queues */
	CMessageQueue *m_pSPMInputQueue, *m_pSPMOutputQueue;

	/* The handle to the event that is set when a new message arrives in the input queue */
	HANDLE m_hNewMessageInQueue;

	/* 
	Handle to event set to indicate to the thread reading/writing the port that it has to
	stop processing 
	*/
	HANDLE m_hStopEvent;

	/* Handle and thread id of the thread */
	HANDLE m_hCommReadThread;
	DWORD  m_dwReadThreadId;
	HANDLE m_hCommWriteThread;
	DWORD  m_dwWriteThreadId;

	/* The current port open and its parameters */
	int m_iPortNumber;
	DWORD m_dwBaud;
	int m_iParity;
	BYTE m_byDataBits;
	int m_iStopbits; 
	FLOW_CONTROL m_fcFlowControl;

	/* The handle to the port */
	HANDLE m_hPort;

	/* The pointer thats invoked on COM port error */
	ErrorCallbackFunction *m_pfErrorFunction;

public:
	CSPM();
	~CSPM();

	void Initialize(
					CMessageQueue *pInputQueue
					, CMessageQueue *pOutputQueue
					, ErrorCallbackFunction *pfErrorFunction
					, int iPort = 1
					, DWORD dwBaud = CBR_9600
					, int iParity = ODDPARITY
					, BYTE byDataBits = 8
					, int iStopbits = ONESTOPBIT
					, FLOW_CONTROL fcFlowControl = NO_FLOW_CONTROL
					);
	void UnInitialize();

	/* Opens a COM Port, returns TRUE on success, FALSE on failure */
	BOOL OpenComm(
				int iPort
				, DWORD dwBaud 
				, int iParity
				, BYTE byDataBits
				, int iStopbits 
				, FLOW_CONTROL fcFlowControl 
				);

	/* Closes a COM Port, returns TRUE on success, FALSE on failure */
	BOOL CloseComm(
				int nPort
				);

	/* 
	Returns TRUE  & details of COM Port open, returns FALSE if no port is currently
	open 
	*/
	BOOL GetPortSettings(
				int *iPort
				, DWORD *pdwBaud 
				, int *piParity
				, BYTE *pbyDataBits
				, int *piStopbits 
				, FLOW_CONTROL *pfcFlowControl 
				);


	BOOL SetPortSettings(
			int iPort
			, DWORD dwBaud 
			, int iParity
			, BYTE byDataBits
			, int iStopbits 
			, FLOW_CONTROL fcFlowControl 
			);

	/* The main read and write thread of execution */
	DWORD CommReadThread();
	DWORD CommWriteThread();
};

/* The dummy thread function that takes pointer to the CSPM object
and calls its CommReadThread function */
DWORD __stdcall COMReadHandlerThread(void *pParam);

/* The dummy thread function that takes pointer to the CSPM object
and calls its CommWriteThread function */
DWORD __stdcall COMWriteHandlerThread(void *pParam);

#endif /* SPM_H */

