/*************************************************************************************************
 *
 * $Workfile: HARTDECommInfc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 */

#if !defined(AFX_HARTDECOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)
#define AFX_HARTDECOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DDLCommInfc.h : header file - for both DDLopc and DDLpipe
//
#include "SDC625Doc.h"
#include "HARTsupport.h"
#include "DDLBaseComm.h"

#include "HARTDEModem.h"

//#include <initguid.h>
#include "IHartComm.h"

#include "HUtil.h"


/////////////////////////////////////////////////////////////////////////////
// CdocCommInfc 
#ifdef  DOCCOMMQSIZE
#undef  DOCCOMMQSIZE
#endif
#define DOCCOMMQSIZE	    2	/* the growby size */
#ifdef  DOCCOMMRETRIES
#undef  DOCCOMMRETRIES
#endif
#define DOCCOMMRETRIES	    3
#define DOCCOMMMAXPOLLADDR	64	/* from 16 stevev 18sep08 - hart 6 goes higher */
class CMainFrame;

class HARTDECommInfc:public CbaseComm
{
// Attributes
protected:
// use base class	CDDIdeDoc*  m_pDoc;
	CMainFrame*  m_pMainFrm;	/* stevev - for light control */

	Indentity_t m_thisIdentity;
	long        m_commHndl; 
	HANDLE      hRegisteredTask;
	commStatus_t m_CommStatus;	// enabling all rolled into one

	bool         m_hadConfigChanged;
	int          m_skipCount;  // how many we have skipped

/*Vibhor 120204: Start of Code*/

	bool         m_bMoreStatusSent;

	/*Vibhor 120204: End of Code*/

public:
	hCPktQueue      thePktQueue;
	IHartComm*      pComm;
	IHartUtilities* pUtil;

	/* Critical section for Handling HARTDE Send commands */
	CRITICAL_SECTION HARTDESendCriticalSection;

   
	BYTE        m_pollAddr;
	bool        m_connected;
	CHartDEModem *hartdemodem;

// Implementation
public:
	HARTDECommInfc(CDDIdeDoc* pD = NULL);
	virtual ~HARTDECommInfc();

// Operations
protected:		// helper functions
	RETURNCODE connect   (void);
	RETURNCODE disconnect(void);
	void       getErrorStr(HRESULT hR, string& retStr);
	RETURNCODE rawSend(hPkt* pSendPacket, hPkt* pRecvPacket);

public:
	// use parent's setRcvService(hCsvcRcvPkt* cpRcvSvc) // sets the dispatcher's service routine

	hPkt* getMTPacket(bool pendOnMT=false);// gets an empty packet for generating a command 
								//		(gives caller ownership of the packet*)
	void  putMTPacket(hPkt*);	// returns the packet memory back to the interface 
								//		(this takes ownership of the packet*)
	RETURNCODE
		  SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
						        // puts a packet into the send queue  
								//		(takes ownership of the packet*)
	RETURNCODE 
		SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
						        // puts a packet into the front of the send queue  
								//		(takes ownership of the packet*)

	RETURNCODE		
		  SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

	int sendQsize(void); 	    // how many are in the send queue ( 0 == empty )
	int	sendQmax(void) ;		// how many will fit
	RETURNCODE  initComm(void);	// setup connection (called after instantiation of device)
	RETURNCODE  shutdownComm(void);
	RETURNCODE  GetIdentity(Indentity_t& retIdentity, BYTE pollAddr);

//stevev 25jan06 use basecomm
//	void  notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged = NO_change);

	// DDIde specific calls
	RETURNCODE Search(Indentity_t& retIdentity, BYTE& newPollAddr);
	void       commInfcTask(waitStatus_t isTimerExpired);
	unsigned short FormHARTRequestPacket(hPkt* pSendPacket,BYTE *HARTMsg);

		void enableComm(void){
		m_CommStatus = commOK;
	};
	/*
		m_enableComm   = true;
		m_CommDisabled = false;};
	*/
	void  disableAppCommRequests(void);
	void  enableAppCommRequests (void);
	bool  appCommEnabled        (void); 

	void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);

	void setHostAddress(BYTE ha)
		{   if ( ha != 1 ) ha = 2; 
//			pComm->put_bPrimaryMaster(ha);
		};
	CDDIdeDoc* GetDocument(void){ if( m_pDoc) return m_pDoc;
				else return ((CDDIdeDoc*)((CDDIdeApp*)AfxGetApp())->pDoc);}

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HARTDECOMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)
	
/*************************************************************************************************
 * NOTES:
 *  The registered gfWaitAndTimerCallbackfunction is triggered each time there is an entry into 
 *the queue.  It is a 'C' routine that just calls this class's commInfcTask method.
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: HARTDECommInfc.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:52a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Verify/Add HART standard headers and footers
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
