
/*************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003 - 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
**      MODULE: Queue.cpp
**
** PURPOSE: Contains implementation for the functions that manipulate the 
**	message queues.
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 03-May-02  TSR Prasad  initial release
**
** Date...: 03-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/

/* --- Standard Includes ------------------------------------------------- */
#include <windows.h>

/* --- Module Includes ------------------------------------------------- */
//#include <errorlog.h>
#include "Queue.h"

#define _DEBUG_MESSAGEQUEx

/* ----------------------------------------------------------------------- */
/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::InitializeQueues
**
** PURPOSE		: Initializes the critical sections and the event objects
**   
** PARAMETER	: none
**	
** RETURN VALUE	: none
****************************************************************************/
void CMessageQueue::InitializeQueue()
{
	/* Initialize the Critical Section Objects */
	InitializeCriticalSection(&QueueCriticalSection);

	/* Create the event that handles is set whenever the Input que is written 
	into  */
	hEventNewMessageInQueue = CreateEvent
										(
										  NULL
										, FALSE
										, FALSE
										, NULL
										);

	/* Check if event was created */
	if(hEventNewMessageInQueue == NULL)
	{
		/* It wasn't ... start cribbing !!! */
#ifdef _DEBUG_MESSAGEQUE
		LogMessage("InitializeQueues: Event Creation Failed !!!");
#endif /* _DEBUG_MESSAGEQUE */
	}

	return;
}/* End of Function: InitializeQueue() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::UnInitializeQueue
**
** PURPOSE		: Empties the Queue, UnInitializes the critical sections 
**				 and the event objects
**   
** PARAMETER	: none
**	
** RETURN VALUE	: none
****************************************************************************/
void CMessageQueue::UnInitializeQueue()
{
	void *pvQueueElement;

	/* Clear the Queue*/
	EnterCriticalSection(&QueueCriticalSection);
		
#ifdef _DEBUG_MESSAGEQUE
	LogMessage("UnInitializeQueues: size of Queue: %d"
		, MessageQueue.size()
		);
#endif /* _DEBUG_MESSAGEQUE */

	/* Deallocate the memory for each element */
	while (MessageQueue.size() != 0)
	{
		pvQueueElement = MessageQueue.front();

		/* Now just pop it out !!!! */
		MessageQueue.pop_front();

		/* Deallocate the memory allocated to it. */
		delete pvQueueElement;
	}

	LeaveCriticalSection(&QueueCriticalSection);

	/* UnInitialize the Critical Section Objects */
	DeleteCriticalSection(&QueueCriticalSection);

	/* Close the event Handle */
	CloseHandle(hEventNewMessageInQueue);
}/* End of Function: UnInitializeQueue() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::AddMessageToQueue
**
** PURPOSE		: Adds the pointer to the byte stream to the Queue
**   
** PARAMETER	: Void Pointer to the byte stream to be added to the queue
**	
** RETURN VALUE	: none
****************************************************************************/
void CMessageQueue::AddMessageToQueue(void *pQueueElememt)
{

	EnterCriticalSection(&QueueCriticalSection);

	/* Check if the que can hold the message */
	if(MessageQueue.size() < MessageQueue.max_size())
	{
		/* Yes it can push it at the back !!! */
		MessageQueue.push_back((void *)pQueueElememt);

#ifdef _DEBUG_MESSAGEQUE
		LogMessage("AddMessageToQueue: Message pushed into Queue");
#endif /* _DEBUG_MESSAGEQUE */
	}
	else
	{
		/* Yo here we are ..... log the holy error !*/

#ifdef _DEBUG_MESSAGEQUE
		LogMessage("AddMessageToQueue: Input Queue is Full !!! ");
#endif /* _DEBUG_MESSAGEQUE */

	}

	LeaveCriticalSection(&QueueCriticalSection);

	/* Set the event to inform others that that this queue has been written into */
	SetEvent(hEventNewMessageInQueue);

	return;

}/* End of Function: AddMessageToQueue() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::GetMessageFromQueue
**
** PURPOSE		: Gets the pointer to the byte stream to the Queue
**   
** PARAMETER	: none 
**	
** RETURN VALUE	: Void Pointer to the byte Stream or NULL
****************************************************************************/
void *CMessageQueue::GetMessageFromQueue()
{
	void *pvQueueElement;

	EnterCriticalSection(&QueueCriticalSection);

	/* First Lets Check if the Queue is empty */
	if(MessageQueue.empty() == TRUE)
	{
		/* Nothing in the queue... just return a NULL */
		pvQueueElement = NULL;
	}
	else
	{
		/* Hey there's something in the queue */
		/* First get the pointer to the first element */
		pvQueueElement = MessageQueue.front();

		/* Now just pop it out !!!! */
		MessageQueue.pop_front();
	}

	LeaveCriticalSection(&QueueCriticalSection);

	/* Give the pointer back to the caller */
	return pvQueueElement;
}/* End of Function: GetMessageFromQueue() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::GetQueueEventHandle
**
** PURPOSE		: Return the event Handle for the client to Wait on !!!
**   
** PARAMETER	: None  
**	
** RETURN VALUE	: Handle to the event to wait on
****************************************************************************/
HANDLE CMessageQueue::GetQueueEventHandle()
{
	return hEventNewMessageInQueue;
}/* End of Function: GetQueueEventHandle() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CMessageQueue::IsQueueEmpty
**
** PURPOSE		: Return the TRUE if queue is empty
**   
** PARAMETER	: None  
**	
** RETURN VALUE	: Returns TRUE if queue is empty 
****************************************************************************/
BOOL CMessageQueue::IsQueueEmpty()
{
	BOOL bStatus;

	EnterCriticalSection(&QueueCriticalSection);
	
	WORD wSize = MessageQueue.size();

	if (wSize != 0)
	{
		bStatus = FALSE;
	}
	else
	{
		bStatus = TRUE;
	}
	LeaveCriticalSection(&QueueCriticalSection);

	return bStatus;
}/* End of Function: IsQueueEmpty() */

/***************************************************************************
** FUNCTION NAME: CMessageQueue::PeekMessageFromQueue
**
** PURPOSE		: Return message from the head of the queue, if exists
**   
** PARAMETER	: None  
**	
** RETURN VALUE	: Returns BYTE stream if queue is not empty 
****************************************************************************/

void *CMessageQueue::PeekMessageFromQueue( int nPos )
{
	void *pvQueueElement;

	EnterCriticalSection(&QueueCriticalSection);

	/* First Lets Check if the Queue is empty */
	if(MessageQueue.empty() == TRUE)
	{
		/* Nothing in the queue... just return a NULL */
		pvQueueElement = NULL;
	}
	else
	{
		/* Hey there's something in the queue */
		/* First get the pointer to the first element */
		if( nPos >= MessageQueue.size() )
			pvQueueElement = NULL;
		else
		{
			if(MessageQueue.size() == 1 )
			{
				QueueIterator = MessageQueue.begin();
				pvQueueElement = (*QueueIterator);
			}
			else
			{
				int nTemp = -1;
				QueueIterator = MessageQueue.begin();
				nTemp++;
				while(QueueIterator != MessageQueue.end())
				{	if( nTemp == nPos )
						break;
					else
					{
					QueueIterator++;
					nTemp++;
					}
				}
				pvQueueElement = (*QueueIterator);
			}

		}
	}

	LeaveCriticalSection(&QueueCriticalSection);

	/* Give the pointer back to the caller */
	return pvQueueElement;
}/* End of Function: GetMessageFromQueue() */
/***************************************************************************
** FUNCTION NAME: CMessageQueue::DeleteMessageFromQueue
**
** PURPOSE		: Pops message from the queue
**   
** PARAMETER	: None  
**	
** RETURN VALUE	: Returns void
****************************************************************************/


void CMessageQueue::DeleteMessageFromQueue( int nPos )
{
	void *pLocalMessageQueue = NULL;

	EnterCriticalSection(&QueueCriticalSection);
	int nTempPos = 0;
	for( QueueIterator = MessageQueue.begin();QueueIterator != MessageQueue.end();QueueIterator++ )
	{
			if( nTempPos == nPos )
			{
				MessageQueue.erase( QueueIterator );
				break;
			}
			nTempPos++;
	}
	LeaveCriticalSection(&QueueCriticalSection);
}






/* ------------------------ End Of File --------------------------------- */


