
/*************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003 - 2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
*/
/**************************************************************************
**
** APPLICATION: HARTSoftwareMux 
**      MODULE: SPM.cpp
**
** PURPOSE: Contains implementation for the Serial Port Manager module.
**
** REVISION HISTORY:
**  Rev  Date      Programmer    Comment
**  ---- --------- ------------- ------------------------------------------
**  0.0 07-May-02  TSR Prasad  initial release
**
** Date...: 07-May-2002  
** Author.: TSR Prasad
** Comment: made from file 
**
***************************************************************************/

/**************************************************************************/
#pragma warning (disable : 4244) 

#include "stdafx.h"
/* --- Standard Includes ------------------------------------------------- */
#include <windows.h>
//#include <process.h>

/* --- Module Includes --------------------------------------------------- */
#include "spm.h"
//#include "errorlog.h"
//#include <atlbase.h>

//#define SHOW_ERROR_DIALOG

/* ----------------------------------------------------------------------- */
/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::CSPM
**
** PURPOSE		: The class constructor
**   
** PARAMETER	: Initializes the member variables to default values.
**	
** RETURN VALUE	: none
****************************************************************************/
CSPM::CSPM()
{
	/* Set all the parameters to their default values */

	/* We are not yet initialized */
	m_bInitialized = FALSE;

	/* We are not yet ready to uninint */
	m_bUninitFlag = FALSE;

	/* The port is not yet open */
	m_bPortOpen = FALSE;

	m_bClosePort = FALSE;

	/* Set the default parameters for the ports, by default we are open COM1 with 
	1200 baud, No parity, 8 data bits,  1 stop bit and no flow control */
	m_iPortNumber = 1;
	m_dwBaud = CBR_9600;
	m_iParity = ODDPARITY;
	m_byDataBits = 8;
	m_iStopbits = ONESTOPBIT; 
	m_fcFlowControl = NO_FLOW_CONTROL;

	m_hStopEvent = NULL;
	m_hCommReadThread = NULL;
	m_hCommWriteThread = NULL;

	return;
}/* End of Function: CSPM::CSPM() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::Initialize
**
** PURPOSE		: Initializes the SPM
**   
** PARAMETER	: The pointers to SPM input and output queue objects
**	
** RETURN VALUE	: none
****************************************************************************/
void CSPM::Initialize(
						CMessageQueue *pInputQueue
						, CMessageQueue *pOutputQueue
						, ErrorCallbackFunction *pfErrorFunction
						, int iPort 
						, DWORD dwBaud
						, int iParity
						, BYTE byDataBits
						, int iStopbits
						, FLOW_CONTROL fcFlowControl
					  )
{
	/* Check if we are already initialized */
	if (m_bInitialized == TRUE)
	{
		//LogMessage("CSPM::Initialize:- Error...Initialize function called twice!");
		return;
	}

	/* Check for NULLs */
	if((pInputQueue == NULL) || (pOutputQueue == NULL))
	{
		//LogMessage("CSPM::Initialize:- Queue pointers are NULL, Init failed !");
		return;
	}

	if(pfErrorFunction == NULL)
	{
		//LogMessage("CSPM::Initialize:- NULL callback func pointer, Init failed !");
		return;
	}

	m_iPortNumber = iPort;
	m_dwBaud = dwBaud;
	m_iParity = iParity;
	m_byDataBits = byDataBits;
	m_iStopbits = iStopbits; 
	m_fcFlowControl = fcFlowControl;

	/* Store the queue pointers */
	m_pSPMInputQueue = pInputQueue;
	m_pSPMOutputQueue = pOutputQueue;

	/* Store the output queue event */
	m_hNewMessageInQueue = pOutputQueue->GetQueueEventHandle();

	/* Store the function pointer */
	m_pfErrorFunction = pfErrorFunction;

	/* Create the stop event */
	m_hStopEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
	if (m_hStopEvent == NULL)
	{
		//LogMessage("CSPM::Initialize:- CreateEvent Failed, Init failed !");
		return;
	}

	/* Create the COMM thread */
	m_hCommReadThread = (HANDLE)CreateThread
							(
							NULL
							, 0
							, (DWORD  (__stdcall *)(void*)) \
									(LPTHREAD_START_ROUTINE)(COMReadHandlerThread)
							, this
							, 0
							, &m_dwReadThreadId
							);
	if (m_hCommReadThread == NULL)
	{
		//LogMessage("CSPM::Initialize:- CreateThread Failed, Init failed !");

		/* Close the event we had created earlier */
		CloseHandle(m_hStopEvent);
		m_hStopEvent = NULL;
		return;
	}

	/* Create the COMM thread */
	m_hCommWriteThread = (HANDLE)CreateThread
							(
							NULL
							, 0
							, (DWORD  (__stdcall *)(void*)) \
									(LPTHREAD_START_ROUTINE)(COMWriteHandlerThread)
							, this
							, 0
							, &m_dwWriteThreadId
							);
	if (m_hCommWriteThread == NULL)
	{
		//LogMessage("CSPM::Initialize:- CreateThread Failed, Init failed !");

		/* Close the event we had created earlier */
		CloseHandle(m_hStopEvent);
		m_hStopEvent = NULL;
		return;
	}

	/* Now we are ready to spin !*/
	m_bInitialized = TRUE;

	m_bUninitFlag = FALSE;
	return;
}/* End of Function: CSPM::Initialize() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::UnInitialize
**
** PURPOSE		: Uninitializes the SPM
**   
** PARAMETER	: none
**	
** RETURN VALUE	: none
****************************************************************************/
void CSPM::UnInitialize()
{
	/* If the event handle is NULL... then we are already unitilized just return..*/
	if (m_hStopEvent == NULL)
	{
		return;
	}

	//LogMessage("CSPM::UnInitialize:- UnInitializing ......");

	/* Lets set the uninit flag */
	m_bUninitFlag = TRUE;

	/* Lets set the stop event*/
	SetEvent(m_hStopEvent);

	/* Wait for the thread to complete */
	//LogMessage("CSPM::UnInitialize:- Waiting for the read thread to close.");
	DWORD dwWaitResult = WaitForSingleObject(m_hCommReadThread, 2000);
	//LogMessage("CSPM::UnInitialize:- Waiting for the write thread to close.");
	dwWaitResult = WaitForSingleObject(m_hCommWriteThread, 2000);
	/* No checks for return types as there is not much one can do !*/

	/* Close the handles and set back all flag values to defaults */
	CloseHandle(m_hStopEvent);
	CloseHandle(m_hCommReadThread);
	CloseHandle(m_hCommWriteThread);
	if (m_hPort != NULL)
	{
		CloseHandle(m_hPort);
	}

	/* We are no longer initialized */
	m_bInitialized = FALSE;

	/* The port is not yet open */
	m_bPortOpen = FALSE;

	/* Set the default parameters for the ports, by default we are opne COM1 with 
	1200 baud, No parity, 8 data bits,  1 stop bit and no flow control */
	m_iPortNumber = 1;
	m_dwBaud = CBR_9600;
	m_iParity = ODDPARITY;
	m_byDataBits = 8;
	m_iStopbits = ONESTOPBIT; 
	m_fcFlowControl = NO_FLOW_CONTROL;

	m_hStopEvent = NULL;
	m_hCommReadThread = NULL;
	m_hCommWriteThread = NULL;

	//LogMessage("CSPM::UnInitialize:- Uninitialize Complete.");
	return;
}/* End of Function: CSPM::UnInitialize() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::OpenComm
**
** PURPOSE		: Opens a COM Port
**   
** PARAMETER	: 
**				iPort - The port number to open (1 for COM1,...) 
**				dwBaud - The baud rate 
**				iParity - Parity bit(s)
**				byDataBits - Number of Data bit(s)
**				iStopbits - Stop bit(s)
**				fcFlowControl - FlowControl... takes a FLOW_CONTROL enum value
**	
** RETURN VALUE	: TRUE on success, FALSE on failure
****************************************************************************/
BOOL CSPM::OpenComm(
			int iPort
			, DWORD dwBaud 
			, int iParity
			, BYTE byDataBits
			, int iStopbits 
			, FLOW_CONTROL fcFlowControl 
			)
{
	/* If the stop event handle is NULL... then we are unitilized just return..*/
	if (m_hStopEvent == NULL)
	{
		return FALSE;
	}

	/* Check if we are already initialized  */
	if (m_bInitialized == TRUE)
	{
		/* We are... so return a error */
		return FALSE;
	}


	//LogMessage("CSPM::OpenComm:- Opening Comm port with new parameters");

	/* If we are here then we should reconfigure */
	m_iPortNumber = iPort;
	m_dwBaud = dwBaud;
	m_iParity = iParity;
	m_byDataBits = byDataBits;
	m_iStopbits = iStopbits; 
	m_fcFlowControl = fcFlowControl;

	/* Lets set the reconfigure flag */
	m_bReconfigureFlag = TRUE;

	/* Lets set the stop event*/
	SetEvent(m_hStopEvent);

	return TRUE;
}/* End of Function: CSPM::OpenComm() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::CloseComm
**
** PURPOSE		: Closes a COM Port
**   
** PARAMETER	: 
**				iPort - The port number to open (1 for COM1,...), fails if 
**	the port to be closed does not match the port open, succeeds if there is
**	no port to be closed.
**	
** RETURN VALUE	: TRUE on success, FALSE on failure
****************************************************************************/
BOOL CSPM::CloseComm(
			int nPort
			)
{
	/* If the stop event handle is NULL... then we are unitilized just return..*/
	if (m_hStopEvent == NULL)
	{
		return TRUE;
	}

	/* Check if we are already not initialized */
	if (m_bInitialized == FALSE)
	{
		/* We are not... just return */
		return TRUE;
	}

	/* If Port is already closed... just return back */
	if (m_bClosePort == TRUE)
	{
		//LogMessage("CSPM::CloseComm:- Asked to close port when port is already closed.");
	}

	/* Lets set the reconfigure flag */
	m_bClosePort = TRUE;

	/* Lets set the stop event*/
	SetEvent(m_hStopEvent);

	//LogMessage("CSPM::CloseComm:- Requested to close Comm port !");
	return TRUE;
}/* End of Function: CSPM::CloseComm() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::GetPortSettings
**
** PURPOSE		: Returns details of COM Port open, returns FALSE if no port
**	is currently open or if a open request is currently ongoing
**   
** PARAMETER	: The parameters returned below are valid only on a TRUE 
**		return value.
**				iPort - The port number to open (1 for COM1,...) 
**				dwBaud - The baud rate 
**				iParity - Parity bit(s)
**				byDataBits - Number of Data bit(s)
**				iStopbits - Stop bit(s)
**				fcFlowControl - FlowControl... takes a FLOW_CONTROL enum value
**	
** RETURN VALUE	: TRUE on success, FALSE on failure
****************************************************************************/
BOOL CSPM::GetPortSettings(
			int *piPort
			, DWORD *pdwBaud 
			, int *piParity
			, BYTE *pbyDataBits
			, int *piStopbits 
			, FLOW_CONTROL *pfcFlowControl 
			)
{
	/* If the stop event handle is NULL... then we are unitilized just return..*/
	if (m_hStopEvent == NULL)
	{
		return FALSE;
	}

	/* If we are not initialized... then just return false */
	if (m_bInitialized == FALSE)
	{
		return FALSE;
	}

	*piPort = m_iPortNumber;
	*pdwBaud = m_dwBaud;
	*piParity = m_iParity;
	*pbyDataBits = m_byDataBits;
	*piStopbits = m_iStopbits;
	*pfcFlowControl = m_fcFlowControl;
	return TRUE;
}/* End of Function: CSPM::GetPortSettings() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::SetPortSettings
**
** PURPOSE		: Set the currently open port parameters to the the ones 
**	passed in the parameter. If Port passed doesnt match the port currently 
**	open the function fails.
**   
** PARAMETER	: 
**				iPort - The port number to open (1 for COM1,...) 
**				dwBaud - The baud rate 
**				iParity - Parity bit(s)
**				byDataBits - Number of Data bit(s)
**				iStopbits - Stop bit(s)
**				fcFlowControl - FlowControl... takes a FLOW_CONTROL enum value
**	
** RETURN VALUE	: TRUE on success, FALSE on failure
****************************************************************************/
BOOL CSPM::SetPortSettings(
			int iPort
			, DWORD dwBaud 
			, int iParity
			, BYTE byDataBits
			, int iStopbits 
			, FLOW_CONTROL fcFlowControl 
			)
{
	/* If the stop event handle is NULL... then we are unitilized just return..*/
	if (m_hStopEvent == NULL)
	{
		return FALSE;
	}

	/* If we are not initialized... then just return false */
	if (m_bInitialized == FALSE)
	{
		//LogMessage("CSPM::SetPortSettings:- Port is not yet open, Open Comm must be called.");
		/* We are... so return a error */
		return FALSE;
	}

	/* we are already initialized */
	/* Check if the settings are for a diffrent port */
	if (iPort != m_iPortNumber)
	{
		//LogMessage("CSPM::SetPortSettings:- Error.... Port numbers mismatched.");
		//LogMessage("Current Open Port: %d, Port in Parameter: %d.", m_iPortNumber, iPort);
		/* We are... so return a error */
		return FALSE;
	}

	//LogMessage("CSPM::SetPortSettings:- Changing port with new parameters");

	/* If we are here then we should reconfigure */
	m_dwBaud = dwBaud;
	m_iParity = iParity;
	m_byDataBits = byDataBits;
	m_iStopbits = iStopbits; 
	m_fcFlowControl = fcFlowControl;

	/* Lets set the reconfigure flag */
	m_bReconfigureFlag = TRUE;

	/* Lets set the stop event*/
	SetEvent(m_hStopEvent);

	return TRUE;
}/* End of Function: CSPM::SetPortSettings() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::CommWriteThread
**
** PURPOSE		: The thread that writes data into the port.
**   
** PARAMETER	: None
**	
** RETURN VALUE	: Always -1
****************************************************************************/
DWORD CSPM::CommWriteThread()
{
	while (TRUE)
	{
		if (m_bUninitFlag)
		{
			/* Holy cow... we should !! */
			//LogMessage("CSPM::CommWriteThread:- Asked to Uninitialize ");
			return -1;
		}

		/* first we have to wait till everything is initialized */
		while ((m_bInitialized == FALSE) || (m_bReconfigureFlag == TRUE))
		{
			Sleep(1000);
		}

		/* Check if we need to write some data as well */
		DWORD dwWaitResult = WaitForSingleObject(m_hNewMessageInQueue, 3000);
		if (dwWaitResult == WAIT_OBJECT_0)
		{
			/* Start sending data till there is nothing to send */
			WORD wCount = 0;
			if (m_pSPMOutputQueue->IsQueueEmpty())
			{
				//LogMessage("CSPM::CommWriteThread:- queue empty, but event set");
				//printf("CSPM::CommWriteThread:- queue empty, but event set\n");
			}

			do
			{
				wCount++;
				DWORD dwNumberofBytesWritten = 0;

				SPMStruct *pspmStruct;

				/* Lets get the data from the queue */
				pspmStruct = (SPMStruct *)m_pSPMOutputQueue->GetMessageFromQueue();

				if (pspmStruct == NULL)
				{
					//LogMessage("CSPM::CommWriteThread:- queue empty, but event set");
					continue;
				}


				DWORD dwTotalBytesWritten = 0;
				WORD wWriteRetryCount = 0;

				/* Write until the entire message is written, or the retry
				limit is reached */
				while
					( (dwTotalBytesWritten != pspmStruct->wMessageSize)
					&& (wWriteRetryCount < NUMBER_OF_WRITE_RETRIES) )
				{
					/* It was... so lets write some data */
                    /*	BOOL bReturnValue 
						= WriteFile
							(
								m_hPort
								, (void *)(pspmStruct->pbyMessage )
								, (pspmStruct->wMessageSize)
								, &dwNumberofBytesWritten
								, NULL
							);*/


					/* It was... so lets write some data */
					BOOL bReturnValue 
						= WriteFile
							(
								m_hPort
								, (void *)(pspmStruct->pbyMessage + dwTotalBytesWritten)
								, (pspmStruct->wMessageSize - dwTotalBytesWritten)
								, &dwNumberofBytesWritten
								, NULL
							);


					dwTotalBytesWritten += dwNumberofBytesWritten;
					wWriteRetryCount++;

					/* Check for errors */
					DWORD dwCommErrors;
					ClearCommError(m_hPort, &dwCommErrors, NULL);
					/* First show the warnings */
					if ((dwCommErrors & CE_BREAK) == CE_BREAK)
					{
						 /* Break in comms will not be regarded as an error */
						//LogMessage("CSPM::CommWriteThread:- Warning:- Break detected in communications.");
					}					 
					if ( 
						 ((dwCommErrors & CE_FRAME) == CE_FRAME) ||
						 ((dwCommErrors & CE_IOE) == CE_IOE) ||
						 ((dwCommErrors & CE_OVERRUN) == CE_OVERRUN) ||
						 ((dwCommErrors & CE_MODE) == CE_MODE) ||
						 ((dwCommErrors & CE_RXOVER) == CE_RXOVER) ||
						 ((dwCommErrors & CE_RXPARITY) == CE_RXPARITY) ||
						 ((dwCommErrors & CE_TXFULL) == CE_TXFULL)
						 )
					{
						//LogMessage("CSPM::CommWriteThread:- Comm error: 0x%04x", dwCommErrors);
						//LogMessage("CSPM::CommWriteThread:- Waiting for reconfiguration !");
						//(*m_pfErrorFunction)();

						#ifdef SHOW_ERROR_DIALOG
												ShowUserErrorDialog
													(
								"COM Port configuration error. Please check the COM Port parameters."
													);
						#endif /* SHOW_ERROR_DIALOG */

						/* Now we have to reinitialised now and we dont have a request to
						reconfigure */
						m_bInitialized = FALSE;
						m_bReconfigureFlag = FALSE;
						break;
					}					 
				}

				/* Check if the the retry limit was exceeded */
				if ( (dwTotalBytesWritten != pspmStruct->wMessageSize)
					&& (wWriteRetryCount >= NUMBER_OF_WRITE_RETRIES) )
				{
					/* Yo... we need to raise the red flag */
					//LogMessage("CSPM::CommWriteThread:- Write to port failed.. retry limit exceeded");

					/* We need to get reconfigured */
					/*m_bInitialized = FALSE;
					m_bReconfigureFlag = FALSE;*/

					/* Free the memory for the buffer & the structures */
					delete pspmStruct->pbyMessage;
					delete pspmStruct;

					/* Break out of the read - write loop */
					//break;
				}
				else
				{

#ifdef SPM_DEBUG
				//LogMessage(
					"CSPM::CommWriteThread:- %d bytes written to port."
					, dwTotalBytesWritten
					);
#endif /* SPM_DEBUG */

					/* Sleep for the time between 2 messages */
					//Sleep(WRITE_INTERVAL);

					/* If we are here then the data is well written down */
					/* Free the memory for the buffer & the structures */
					delete pspmStruct->pbyMessage;
					delete pspmStruct;
				}
	
				/* Do this until write queue is empty */
			} while(m_pSPMOutputQueue->IsQueueEmpty() == FALSE);

		}/* End of write if */
		/* if we are here then we are ready to go */
	}

	return -1;

}

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::CommReadThread
**
** PURPOSE		: The main thread of execution that:
**		- Opens the port and sets the port parameters
**		- Reads from the port and writes the bytes read to the input queue
**		- Reads from the output queue and writes the bytes read to the port
**		- Handles changes the port parameters 
**		- Handles closing of the port and opening of a new one
**	Returns only on a exit request. On Port errors it stops working with that
**	port and continues to wait for a reconfiguration request. 
**   
** PARAMETER	: None
**	
** RETURN VALUE	: Always -1
****************************************************************************/
DWORD CSPM::CommReadThread()
{
	char szPortName[MAX_PORT_NAME_LENGTH];
	BOOL bReturnValue;

	/* Prepare the port name */
	sprintf(szPortName,"COM%d:", m_iPortNumber);
	
	/* Start with setting the port parameters */
	m_bReconfigureFlag = TRUE;

	CComBSTR bstrPortName(szPortName);

	m_hPort = CreateFile
					(
						bstrPortName.m_str
						, GENERIC_READ | GENERIC_WRITE
						, 0 /* Always zero as we use the port exclusively, no sharing */
						, NULL /* No security attributes */
						, OPEN_EXISTING
						, 0 
						, NULL
					);
	if (m_hPort == INVALID_HANDLE_VALUE)
	{
//		(*m_pfErrorFunction)();
		//LogMessage("CSPM::CommReadThread:- Failed to open port %s", szPortName);
		//printf("CSPM::CommReadThread:- Failed to open port %s\n", szPortName);
		m_bReconfigureFlag = FALSE;
		m_bInitialized = FALSE;
		m_bPortOpen = FALSE;
	}
	else
	{
		/* Set the COMM Mask */
		DWORD dwCommMask = EV_RXCHAR | EV_TXEMPTY;
		bReturnValue = SetCommMask(m_hPort, dwCommMask);

		//printf("CSPM::CommReadThread:- Opened port %s successfully\n", szPortName);

		/* Set the communication timeouts */
		COMMTIMEOUTS commTimeouts;

		commTimeouts.ReadIntervalTimeout = READ_INTERVAL_TIMEOUT;
		commTimeouts.ReadTotalTimeoutMultiplier  = READ_TOTAL_TIMEOUT_MULT;
		commTimeouts.ReadTotalTimeoutConstant = READ_TOTAL_TIMEOUT_CONST;
		commTimeouts.WriteTotalTimeoutMultiplier = WRITE_TOTAL_TIMEOUT_MULT;
		commTimeouts.WriteTotalTimeoutConstant = WRITE_TOTAL_TIMEOUT_CONST;

		bReturnValue = EscapeCommFunction(m_hPort, SETDTR);
		bReturnValue = SetCommTimeouts (m_hPort, &commTimeouts);
		if(bReturnValue == FALSE)
		{
			//LogMessage("CSPM::CommReadThread:- SetCommTimeouts failed.");
			//LogMessage("CSPM::CommReadThread:- Error in initializing COM Port.");

			(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
			ShowUserErrorDialog
				(
		"COM Port configuration error. Please check the COM Port parameters."
				);
#endif /* SHOW_ERROR_DIALOG */
			/* Our initialization is not yet complete !*/
			m_bInitialized = FALSE;
			/* Nor do we have a request to reconfigure */
			m_bReconfigureFlag = FALSE;
		}
		else
		{
			m_bInitialized = TRUE;
		}

		/* Set the comm port open flag to TRUE */
		m_bPortOpen = TRUE;
	}

	/* Lets loop forever until we are asked to close !*/
	while (TRUE)
	{
		/* If the port initilization has failed and there is no request for 
		reconfiguration, then we need to wait for a reconfig request */
		if ( (m_bInitialized == FALSE) && (m_bReconfigureFlag == FALSE) )
		{
			Sleep(2000);

			/* Then wait for ever for something to happen */
			DWORD dwWaitResult = WaitForSingleObject(m_hStopEvent, 0);

			/* If the wait was abandoned for some reason */
			if (dwWaitResult != WAIT_OBJECT_0)
			{
				/* Then just continue to wait */
				continue;
			}

			/* If we are here then the event was set !! */
			/* Check if we need to die */
			if (m_bUninitFlag)
			{
				/* Holy cow... we should !! */
				//LogMessage("CSPM::CommReadThread:- Asked to Uninitialize ");
				return -1;
			}

			/* Check if we have to reconfigure */
			if (m_bReconfigureFlag)
			{
				//LogMessage("CSPM::CommReadThread:- Asked to reconfigure... ");
				continue;
			}
		}

		/* We are not initialised, and we have a reconfig request... what we were
		waiting... yooo hoo... lets re-inintialize */
		if ( (m_bInitialized == FALSE) && (m_bReconfigureFlag == TRUE) )
		{
			//LogMessage("CSPM::CommReadThread:- Trying to open port with new parameters...");

			/* Prepare the port name */
			sprintf(szPortName,"COM%d:", m_iPortNumber);
	
			CComBSTR bstrPortName(szPortName);

			m_hPort = CreateFile
						(
						bstrPortName.m_str
							, GENERIC_READ | GENERIC_WRITE
							, 0 /* Always zero as we use the port exclusively, no sharing */
							, NULL /* No security attributes */
							, OPEN_EXISTING
							, 0 
							, NULL
						);
			if (m_hPort == INVALID_HANDLE_VALUE)
			{
				//LogMessage("CSPM::CommReadThread:- Failed to open port: %s", szPortName);
				(*m_pfErrorFunction)();
				m_bReconfigureFlag = FALSE;
				continue;
			}

			/* Set the COMM Mask */
			DWORD dwCommMask = EV_RXCHAR | EV_TXEMPTY;
			bReturnValue = SetCommMask(m_hPort, dwCommMask);

			/* Set the communication timeouts */
			COMMTIMEOUTS commTimeouts;

			commTimeouts.ReadIntervalTimeout = READ_INTERVAL_TIMEOUT;
			commTimeouts.ReadTotalTimeoutMultiplier  = READ_TOTAL_TIMEOUT_MULT;
			commTimeouts.ReadTotalTimeoutConstant = READ_TOTAL_TIMEOUT_CONST;
			commTimeouts.WriteTotalTimeoutMultiplier = WRITE_TOTAL_TIMEOUT_MULT;
			commTimeouts.WriteTotalTimeoutConstant = WRITE_TOTAL_TIMEOUT_CONST;

			bReturnValue = SetCommTimeouts (m_hPort, &commTimeouts);
			if(bReturnValue == FALSE)
			{
				//LogMessage("CSPM::CommReadThread:- SetCommTimeouts failed.");
				//LogMessage("CSPM::CommReadThread:- Error in initializing COM Port.");

				(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
				ShowUserErrorDialog
					(
			"COM Port configuration error. Please check the COM Port parameters."
					);
#endif /* SHOW_ERROR_DIALOG */
				/* Our initialization is not yet complete !*/
				m_bInitialized = FALSE;
				m_bReconfigureFlag = FALSE;
				continue;
			}
			else
			{
				m_bInitialized = TRUE;
			}

			/* Set the comm port open flag to TRUE */
			m_bPortOpen = TRUE;

			//LogMessage("CSPM::CommReadThread:- Port initialized with new parameters !");
		}

		/* The port is initialized, but still we need to reconfigure some
		COM port parameters... this is fine.... */
		if ( (m_bInitialized == TRUE) && (m_bReconfigureFlag == TRUE) )
		{
			DCB dcb;

			/* Get then device control block */
			bReturnValue = GetCommState(m_hPort,&dcb);			
			/* If we have failed ... */
			if (bReturnValue == FALSE)
			{
				/* The crib ..... */
				//LogMessage("CSPM::CommReadThread:- Error... Failed to get communications state.");
				//LogMessage("CSPM::CommReadThread:- Reconfiguration failed.");
				(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
				ShowUserErrorDialog
					(
			"COM Port configuration error. Please check the COM Port parameters."
					);
#endif /* SHOW_ERROR_DIALOG */
				/* We'll just continue to run with old configuration */
				m_bReconfigureFlag = FALSE;
				continue;
			}

			/* The DCB is got... lets do the settings */
			dcb.BaudRate = m_dwBaud;
			dcb.Parity = m_iParity;
			dcb.ByteSize = m_byDataBits;
			dcb.StopBits = m_iStopbits;

			dcb.fOutxCtsFlow = FALSE;
			dcb.fOutxDsrFlow = FALSE;
			dcb.fDtrControl = DTR_CONTROL_ENABLE;
			dcb.fDsrSensitivity = FALSE;
			dcb.fRtsControl = RTS_CONTROL_DISABLE;

			dcb.fTXContinueOnXoff = 1;
			dcb.fAbortOnError = 1;
//			dcb.fTXContinueOnXoff = FALSE;
//			dcb.fAbortOnError = FALSE;
			dcb.XonLim = 0x800;
			dcb.XoffLim = 0x200;
			dcb.ErrorChar = 0x00;
			dcb.EofChar = 0x00;
			dcb.EvtChar = 0x00;
			dcb.fNull = FALSE;

//			dcb.fOutX = FALSE;
//			dcb.fInX = FALSE;
//			dcb.XonLim = 0;
//			dcb.XoffLim = 0;
			dcb.fNull = FALSE;

			switch(m_fcFlowControl)
			{
				case NO_FLOW_CONTROL:
					{
						dcb.fOutxCtsFlow = FALSE;
						dcb.fOutxDsrFlow = FALSE;
						dcb.fOutX = FALSE;
						dcb.fInX = FALSE;
						break;
					}
				case HARDWARE_FLOW_CONTROL:
					{
						dcb.fOutxCtsFlow = TRUE;
						dcb.fOutxDsrFlow = TRUE;
						dcb.fOutX = FALSE;
						dcb.fInX = FALSE;
						break;
					}
				case XON_XOFF_FLOW_CONTROL:
					{
						dcb.fOutxCtsFlow = FALSE;
						dcb.fOutxDsrFlow = FALSE;
						dcb.fOutX = TRUE;
						dcb.fInX = TRUE;
						dcb.XonChar = 0x11;
						dcb.XoffChar = 0x13;
						dcb.XoffLim = 100;
						dcb.XonLim = 100;
						break;
					}
				default:
					{
						//LogMessage("CSPM::CommReadThread:- Unknown flow control type: %d", m_fcFlowControl);
						(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
						ShowUserErrorDialog
							(
				"COM Port configuration error. Please check the COM Port parameters."
							);
#endif /* SHOW_ERROR_DIALOG */
						m_bReconfigureFlag = FALSE;
						continue;
					}
			}

			/* Now lets set the dcb back */
			bReturnValue = SetCommState(m_hPort,&dcb);
			DWORD dwLastError = GetLastError();
			/* If we have failed ... */
			if (bReturnValue == FALSE)
			{
				/* The crib ..... */
				//LogMessage("CSPM::CommReadThread:- Failed to set communications state. Error:%d", GetLastError());
				//LogMessage("CSPM::CommReadThread:- Reconfiguration failed.");
//				try
				{
					(*m_pfErrorFunction)();
				}
/*				catch(...)
				{
#ifdef SHOW_ERROR_DIALOG
					ShowUserErrorDialog
						(
"COM Port configuration error. Please check the COM Port parameters."
						);
#endif // SHOW_ERROR_DIALOG 
				}*/
				
#ifdef SHOW_ERROR_DIALOG
				ShowUserErrorDialog
					(
			"COM Port configuration error. Please check the COM Port parameters."
					);
#endif /* SHOW_ERROR_DIALOG */
				/* We'll just continue to run with old configuration */
				m_bReconfigureFlag = FALSE;
				continue;
			}

//			bReturnValue = EscapeCommFunction(m_hPort, SETDTR);

			/* Configuration done... so lets set flag to false */
			m_bReconfigureFlag = FALSE;
		}

		/* If we are here... then we just keep running */
		if ( (m_bInitialized == TRUE) && (m_bReconfigureFlag == FALSE) )
		{
			/* Check if the stop event is set ?*/
			DWORD dwWaitResult = WaitForSingleObject(m_hStopEvent, 0);
			if (dwWaitResult == WAIT_OBJECT_0)
			{
				/* It was... so we need to see what to do */
				/* Check if we need to die */
				if (m_bUninitFlag)
				{
					/* Holy cow... we should !! */
					//LogMessage("CSPM::CommReadThread:- Asked to Uninitialize ");
					return -1;
				}

				/* Check if we have to close the port */
				if (m_bClosePort == TRUE)
				{
					/* Close the port */
					bReturnValue = CloseHandle(m_hPort);
					if (bReturnValue == FALSE)
					{
						//LogMessage("CSPM::CommReadThread:- Error Closing port.");
					}

					/* Now we are not initialised and we dont have a request to
					reconfigure */
					m_bInitialized = FALSE;
					m_bReconfigureFlag = FALSE;

					//LogMessage("CSPM::CommReadThread:- Comm port closed...!");

					m_bClosePort = FALSE;

					continue;
				}

				/* Check if we are asked to reconfigure ? */
				if (m_bReconfigureFlag == TRUE)
				{
					/* Yes... */
					continue;
				}


			}

			BOOL bDataAvailable;


			do
			{
				/* First read some data */
				BYTE pbyBuffer[SPM_MAX_BUFFER_SIZE];
				DWORD dwNumberofBytesToRead = SPM_MAX_BUFFER_SIZE, dwNumberofBytesRead;

				bDataAvailable = FALSE;

				bReturnValue = ReadFile
									(
										m_hPort
										, (void *)pbyBuffer
										, dwNumberofBytesToRead
										, &dwNumberofBytesRead
										, NULL
									);

				/* Check for errors */
				DWORD dwCommErrors;
				ClearCommError(m_hPort, &dwCommErrors, NULL);
				/* First show the warnings */
				if ((dwCommErrors & CE_BREAK) == CE_BREAK)
				{
					 /* Break in comms will not be regarded as an error */
					//LogMessage("CSPM::CommReadThread:- Warning:- Break detected in communications.");
				}					 
				if ( 
					 ((dwCommErrors & CE_FRAME) == CE_FRAME) ||
					 ((dwCommErrors & CE_IOE) == CE_IOE) ||
					 ((dwCommErrors & CE_OVERRUN) == CE_OVERRUN) ||
					 ((dwCommErrors & CE_MODE) == CE_MODE) ||
					 ((dwCommErrors & CE_RXOVER) == CE_RXOVER) ||
					 ((dwCommErrors & CE_RXPARITY) == CE_RXPARITY) ||
					 ((dwCommErrors & CE_TXFULL) == CE_TXFULL)
					 )
				{
					//LogMessage("CSPM::CommReadThread:- Comm error: 0x%04x", dwCommErrors);
					//LogMessage("CSPM::CommReadThread:- Waiting for reconfiguration !");
//					(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
					ShowUserErrorDialog
						(
				"COM Port configuration error. Please check the COM Port parameters."
						);
#endif /* SHOW_ERROR_DIALOG */

					/* Now we have to reinitialised now and we dont have a request to
					reconfigure */
					m_bInitialized = FALSE;
					m_bReconfigureFlag = FALSE;
					continue;
				}					 

				/* Check if we have some messages */
				if ((bReturnValue == TRUE) && (dwNumberofBytesRead > 0))
				{
					/* Yes we do... */
					BYTE *pQueueBuffer;
					SPMStruct *pspmStruct;

					/* Allocate memory for the data... */
					pQueueBuffer = new BYTE[dwNumberofBytesRead];
					if (pQueueBuffer == NULL)
					{
						//LogMessage("CSPM::CommReadThread:- Unable to allocate memory.");
						(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
						ShowUserErrorDialog
							(
			"Unable to allocate memory. Close some applications to free some memory."
							);
#endif /* SHOW_ERROR_DIALOG */
						return -1;
					}
					/* ... and for the structure that holds it */
					pspmStruct = new SPMStruct;
					if (pspmStruct == NULL)
					{
						//LogMessage("CSPM::CommReadThread:- Unable to allocate memory.");
						(*m_pfErrorFunction)();
#ifdef SHOW_ERROR_DIALOG
						ShowUserErrorDialog
							(
			"Unable to allocate memory. Close some applications to free some memory."
							);
#endif /* SHOW_ERROR_DIALOG */
						return -1;
					}
					/* assign the values */
					pspmStruct->wMessageSize = dwNumberofBytesRead;
					pspmStruct->pbyMessage = pQueueBuffer;

					/*..... copy the data....*/
					memcpy(pQueueBuffer, pbyBuffer, dwNumberofBytesRead);
					/*.... and add it to the queue !!! */
					m_pSPMInputQueue->AddMessageToQueue((void *)pspmStruct);

#ifdef SPM_DEBUG
					LogMessage(
						"CSPM::CommReadThread:- %d bytes received & added to queue."
						, dwNumberofBytesRead
						);
#endif /* SPM_DEBUG */

					bDataAvailable = TRUE;
				}

			}while (bDataAvailable == TRUE);


			Sleep(IDLE_INTERVAL);

			/* If we are here... then dont have much to do... lets 
			check for change in configurations etc */
			continue;
		}

	}
	
	return -1;
}/* End of Function: CSPM::CommReadThread() */

/*F*/
/***************************************************************************
** FUNCTION NAME: COMReadHandlerThread
**
** PURPOSE		: The thread function created by CSPM::Initialize... its just
** a dummy function, which calls the CSPM::CommReadThread function...
**   
** PARAMETER	: this pointer to the object which created it.
**	
** RETURN VALUE	: Always -1
****************************************************************************/
DWORD __stdcall COMReadHandlerThread(void *pParam)
{
	CSPM *pSPMObj = (CSPM *)pParam;

	DWORD dwRetVal = pSPMObj->CommReadThread();

	//LogMessage("CSPM Comm Thread stopping.");
	return dwRetVal;
}/* End of Function: CSPM::COMHandlerThread() */

/*F*/
/***************************************************************************
** FUNCTION NAME: COMWriteHandlerThread
**
** PURPOSE		: The thread function created by CSPM::Initialize... its just
** a dummy function, which calls the CSPM::CommWriteThread function...
**   
** PARAMETER	: this pointer to the object which created it.
**	
** RETURN VALUE	: Always -1
****************************************************************************/
DWORD __stdcall COMWriteHandlerThread(void *pParam)
{
	CSPM *pSPMObj = (CSPM *)pParam;

	DWORD dwRetVal = pSPMObj->CommWriteThread();

	//LogMessage("CSPM Comm Thread stopping.");
	return dwRetVal;
}/* End of Function: CSPM::COMHandlerThread() */

/*F*/
/***************************************************************************
** FUNCTION NAME: CSPM::~CSPM
**
** PURPOSE		: The destructor guy... does nothing !
**   
** PARAMETER	: none
**	
** RETURN VALUE	: none
****************************************************************************/
CSPM::~CSPM()
{

}/* End of Function: CSPM::~CSPM() */
