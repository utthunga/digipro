/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: RecvMsg.cpp $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/XmtrLlc/RecvMsg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   Receive Message
 ***************************************************************************/

/***************************************************************************
 * $History: RecvMsg.cpp $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

//#include "stdafx.h"
#include "stdwinstuff.h"

#include <crtdbg.h>

#include "Hart.h"
#include "XmtrLlc.h"
#include "HartLlcMsg.h"
#include "RecvMsg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

///////////////////////////////////////////////////////////////////////////////
//
// File Global Data
//
typedef struct {
	E_RECV_STATE eState;
	E_RECV_STATE (CRecvMsg::*pfSvc)(unsigned char cIn, unsigned char cStatus);
} FSM;

#if _MSC_VER > 1300
const FSM g_stRecvFsm[] = {
	{ GET_SOM,    &CRecvMsg::WaitForSOM},
	{ GET_ADDR,   &CRecvMsg::GetAddr},
	{ GET_EXPAND, &CRecvMsg::GetExpand},
	{ GET_CMD,    &CRecvMsg::GetCmd},
	{ GET_BCNT,   &CRecvMsg::GetBCnt},
	{ GET_DATA,   &CRecvMsg::GetData},
	{ GET_BCC,    &CRecvMsg::GetBCC},
	{ WAIT_EOM,   &CRecvMsg::WaitForEOM},
	{ EXIT, 0}
};
#else
const FSM g_stRecvFsm[] = {
	{ GET_SOM, CRecvMsg::WaitForSOM},
	{ GET_ADDR, CRecvMsg::GetAddr},
	{ GET_EXPAND, CRecvMsg::GetExpand},
	{ GET_CMD, CRecvMsg::GetCmd},
	{ GET_BCNT, CRecvMsg::GetBCnt},
	{ GET_DATA, CRecvMsg::GetData},
	{ GET_BCC, CRecvMsg::GetBCC},
	{ WAIT_EOM, CRecvMsg::WaitForEOM},
	{ EXIT, 0}
};
#endif

///////////////////////////////////////////////////////////////////////////////
//
// CRecvMsg
//
// simple constructors and destructors
//
CRecvMsg::CRecvMsg()
{
	TRACE(_T("CRecvMsg::CRecvMsg.\n"));
	InitMsg();
}

CRecvMsg::~CRecvMsg()
{
	TRACE(_T("CRecvMsg::~CRecvMsg: delete message in progress.\n"));
	delete m_pMsg;
}

///////////////////////////////////////////////////////////////////////////////
//
// InitMsg
//
// intitializes the msg structure, sets up for waitForSOM
//
unsigned char CRecvMsg::InitMsg(void)
{
	TRACE(_T("CRecvMsg::InitMsg.\n"));
	m_eState = GET_SOM;
#if _MSC_VER >= 1300
	m_pfSvc = &CRecvMsg::WaitForSOM;
#else
	m_pfSvc = WaitForSOM;
#endif
	m_pMsg = new CHartLlcMsg();

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// WaitForSOM
//
// Receives BYTE characters, looks for 2 0xff's and a valid delimiter. If
// receives bad data then resets preamble count. Once delimiter recieved,
// initializes the function pointer.
//
E_RECV_STATE CRecvMsg::WaitForSOM(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::WaitForSOM.\n"));

	// if bad status (FE, PE, OE) reset preamble count and toss the unsigned char
	//
	TRACE(_T("WaitForSOM: check status.\n"));
	if (cStatus) {
		if (cStatus & COMM_GAP) {	// If gap then no message
			m_pMsg->m_eMsgStat = NO_MSG;
			return EXIT;
		}
		m_pMsg->m_cPreambles = 0;
	}

	// if a preamble then bump the count
	//
	else if (cIn == 0xFF) {
		m_pMsg->m_cPreambles++;
		TRACE(_T("WaitForSOM: found a preamble, have %d.\n"), m_pMsg->m_cPreambles);
	}

	// if m_cPreambles >= 2 then we can check for a delimiter
	//
	else if (m_pMsg->m_cPreambles > 1) {
		TRACE(_T("WaitForSOM: look for delimiter.\n"));
		switch (cIn & FRAME_MASK) {

		// valid del???  save the del and setup for the next state
		//
		case PSK_STX:
		case PSK_ACK:
		case PSK_BACK:
		case STX:
		case ACK:
		case BACK:
			TRACE(_T("WaitForSOM: found the delimiter, start the checksum.\n"));
			m_pMsg->m_cBcc = cIn;		// init the m_cBcc
			m_pMsg->m_cDelimiter = cIn;
			m_pcBuffer = &m_pMsg->m_cAddr1;

			// how long is the address ?
			//
			if (m_pMsg->m_cDelimiter & LONG_FRAME) {
				TRACE(_T("WaitForSOM: long frame delimiter.\n"));
				m_cBufferCount = 5;
			}
			else {
				TRACE(_T("WaitForSOM: short frame delimiter.\n"));
				m_cBufferCount = 1;
			}
			TRACE(_T("WaitForSOM: return state GET_ADDR.\n"));
			return GET_ADDR;

		default:			// not a valid delimiter. start counting over
			TRACE(_T("WaitForSOM: invalid delimiter, restart.\n"));
			m_pMsg->m_cPreambles = 0;
		}
	}
	TRACE(_T("WaitForSOM: return NULL.\n"));
	return (E_RECV_STATE) NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetAddr
//
// Receives address bytes until m_cBufferCount == 0 then sets up for the next state,
// If expansion then GET_EXPAND, else GET_CMD.
//
E_RECV_STATE CRecvMsg::GetAddr(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetAddr.\n"));
	if (cStatus) {				 // bad status during address,  ignore the msg
		TRACE(_T("GetAddr: address error, ignore the message.\n"));
		m_pMsg->m_eMsgStat = ADDR_ERR;
		return WAIT_EOM;
	}

	TRACE(_T("GetAddr: store the byte, update the checksum.\n"));
	*m_pcBuffer++ = cIn;		// store the byte
	m_pMsg->m_cBcc ^= cIn;	  // continue the BCC calc
	m_cBufferCount --;
	if (!m_cBufferCount) {	   // Last byte: change state

		// expansion is bits 5 & 6
		//
		TRACE(_T("GetAddr: have the address, check for expansion bytes.\n"));
		m_cBufferCount = (m_pMsg->m_cDelimiter & EXPANDED) >> 5;
		if (m_cBufferCount) {	 // have expansion
			TRACE(_T("GetAddr: have expansion bytes, return state GET_EXPAND.\n"));
			m_pcBuffer = m_pMsg->m_caExpand;
			return GET_EXPAND;
		}
		else {					// its zero.  look for the command #
			TRACE(_T("GetAddr: no expansion bytes, return state GET_CMD.\n"));
			return GET_CMD;
		}
	}
	TRACE(_T("GetAddr: return NULL.\n"));
	return (E_RECV_STATE) NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetExpand
//
// Receives address bytes until m_cBufferCount == 0 then sets up for the
// next state, GET_CMD
//
// TODO: how to handle errors.
//
E_RECV_STATE CRecvMsg::GetExpand(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetExpand.\n"));

	// bad status during address,  ASSuME ignore the msg
	//
	if (cStatus) {
		TRACE(_T("GetExpand: expansion error, ignore the message.\n"));
		m_pMsg->m_eMsgStat = EXP_ERR;
		return WAIT_EOM;
	}
	//TRACE(_T("GetExpand: store the byte, update the checksum.\n"));
	*m_pcBuffer++ = cIn;		// store the byte
	m_pMsg->m_cBcc ^= cIn;	// continue the BCC calc
	m_cBufferCount--;
	if (!m_cBufferCount) {	 // Last byte: change state
		TRACE(_T("GetExpand: return state GET_CMD.\n"));
		return GET_CMD;
	}
	TRACE(_T("GetExpand: return NULL.\n"));
	return (E_RECV_STATE) NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetCmd
//
// Receives the command #
//
E_RECV_STATE CRecvMsg::GetCmd(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetCmd.\n"));

	// bad status during command, store the problem
	//
	if (cStatus) {
		if (cStatus & COMM_GAP) {	// If gap then no message
			TRACE(_T("GetCmd: gap error, ignore the message.\n"));
			m_pMsg->m_eMsgStat = GAP_ERR;
			return WAIT_EOM;
		}
		else {
			m_pMsg->m_cCommStat = cStatus;
			if (cStatus & COMM_PE) {
				TRACE(_T("GetCmd: parity error, record it.\n"));
				m_pMsg->m_eMsgStat = PE;
			}
			else if (cStatus & COMM_FE) {
				TRACE(_T("GetCmd: framing error, record it.\n"));
				m_pMsg->m_eMsgStat = FE;
			}
			else if (cStatus & COMM_OE) {
				TRACE(_T("GetCmd: overrun error, record it.\n"));
				m_pMsg->m_eMsgStat = OE;
			}
		}
	}
	TRACE(_T("GetCmd: store the byte, update the checksum.\n"));
	m_pMsg->m_cCommand = cIn;
	m_pMsg->m_cBcc ^= cIn;	// continue the BCC calc
	TRACE(_T("GetCmd: return state GET_BCNT.\n"));
	return GET_BCNT;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetBCnt
//
// Receives the byte count. If error then can't frame, let's split
//
E_RECV_STATE CRecvMsg::GetBCnt(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetBCnt.\n"));

	// bad status during byte count,  can't frame the message
	//
	if (cStatus) {
		TRACE(_T("GetBCnt: status error, ignore the message.\n"));
		m_pMsg->m_eMsgStat = BC_ERR;
		return WAIT_EOM;
	}
	TRACE(_T("GetBCnt: update the checksum.\n"));
	m_pMsg->m_cBcc ^= cIn;		// continue the BCC calc

	// setup to receive the data field.  watch it! data field could be
	//   empty. if so next state is GET_BCC.
	//
	if(m_pMsg->m_cByteCount = cIn) {
		TRACE(_T("GetBCnt: have data, return state GET_DATA.\n"));
		m_cBufferCount = cIn;
		m_pcBuffer = m_pMsg->m_caData;
		return GET_DATA;
	}
	else {					  // its zero.  look for the BCC
		TRACE(_T("GetBCnt: return state GET_BCC.\n"));
		return GET_BCC;
	}
}

///////////////////////////////////////////////////////////////////////////////
//
// GetData
//
// Receives bytes and places them in the data field until m_cBufferCount == 0,
// then sets up for the next state (get bcc).
//
E_RECV_STATE CRecvMsg::GetData(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetData.\n"));

	// bad status during data, store the problem
	//
	if (cStatus) {
		if (cStatus & COMM_GAP) {	// If gap then no message
			TRACE(_T("GetData: gap error, ignore the message.\n"));
			m_pMsg->m_eMsgStat = GAP_ERR;
			return WAIT_EOM;
		}

		// if error already stored skip this error
		//
		if (!m_pMsg->m_eMsgStat) {
			TRACE(_T("GetData: have status error, been recorded yet?\n"));
			m_pMsg->m_cCommStat= cStatus;
			if (cStatus & COMM_PE) {
				TRACE(_T("GetData: no, have parity error.\n"));
				m_pMsg->m_eMsgStat = PE;
			}
			else if (cStatus & COMM_FE) {
				TRACE(_T("GetData: no, have framing error.\n"));
				m_pMsg->m_eMsgStat = FE;
			}
			else if (cStatus & COMM_OE) {
				TRACE(_T("GetData: no, have overrun error.\n"));
				m_pMsg->m_eMsgStat = OE;
			}
		}
	}
	TRACE(_T("GetData: store the byte, update the checksum.\n"));
	*m_pcBuffer++ = cIn;		// store the byte
	m_pMsg->m_cBcc ^= cIn;	// continue the BCC calc
	m_cBufferCount--;
	if (! m_cBufferCount) {		// setup to receive the BCC
		TRACE(_T("GetData: return state GET_BCC.\n"));
		return GET_BCC;
	}
	TRACE(_T("GetData: return NULL.\n"));
	return (E_RECV_STATE) NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetBCC
//
// Receives and checks the BCC
//
E_RECV_STATE CRecvMsg::GetBCC(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::GetBCC.\n"));
	if (cStatus) {		// bad status store the problem

		// if error already stored skip this error
		//
		if (!m_pMsg->m_eMsgStat) {
			m_pMsg->m_cCommStat |= cStatus;
			if (cStatus & COMM_PE) {
				TRACE(_T("GetBCC: have parity error.\n"));
				m_pMsg->m_eMsgStat = PE;
			}
			else if (cStatus & COMM_FE) {
				TRACE(_T("GetBCC: have framing error.\n"));
				m_pMsg->m_eMsgStat = FE;
			}
			else if (cStatus & COMM_OE) {
				TRACE(_T("GetBCC: have overrun error.\n"));
				m_pMsg->m_eMsgStat = OE;
			}
		}
	}
	if (m_pMsg->m_cBcc != cIn) {	// BCC wrong
		TRACE(_T("GetBCC: checksum doesn't match message.\n"));
		m_pMsg->m_cCommStat |= COMM_BCCE;
	}
	TRACE(_T("GetBCC: store the checksum, return state EXIT.\n"));
	m_pMsg->m_cBcc = cIn;				// store the BCC
	m_pMsg->m_bComplete = true;
	return EXIT;
}

///////////////////////////////////////////////////////////////////////////////
//
// WaitForEOM
//
E_RECV_STATE CRecvMsg::WaitForEOM(unsigned char cIn, unsigned char cStatus)
{
	TRACE(_T("CRecvMsg::WaitForEOM.\n"));
	if (cStatus & COMM_GAP) {	// If gap then we are done
		TRACE(_T("WaitForEOM: gap, message is done, return state EXIT.\n"));
		m_pMsg->m_eMsgStat = GAP_ERR;
		return EXIT;
	}
	TRACE(_T("WaitForEOM: return NULL.\n"));
	return (E_RECV_STATE) NULL;
}

///////////////////////////////////////////////////////////////////////////////
//
// BuildMsg
//
// This task is devoted to receiving a HART message.
//
CHartLlcMsg *CRecvMsg::BuildMsg(bool bTimedOut,	// TRUE if the read timed out
							WORD* pwBytesRead,		// number of bytes read
							unsigned char *pcInBuf,			// buffer with received data
							DWORD dwCommErr		// receive error codes from ClearCommError
	)
{
	TRACE(_T("CRecvMsg::BuildMsg.\n"));
	unsigned char cStatus = 0;
	DWORD dwBytesProcessed = 0;
	DWORD dwBytesRead = (DWORD) *pwBytesRead;
	DWORD dwBytesTotal = dwBytesRead;
	CHartLlcMsg *pDoneMsg = 0;
	while (dwBytesRead > 0) {
		dwBytesProcessed++;
		TRACE(_T("BuildMsg: process byte %d of %d.\n"),
				dwBytesProcessed, dwBytesTotal);
		if (dwBytesRead == 1) {	// set up the status the way HART likes it
			TRACE(_T("BuildMsg: read 1 byte, check for error.\n"));
			cStatus = (dwCommErr & CE_RXPARITY) ? COMM_PE : 0;
			cStatus |= (dwCommErr & CE_FRAME) ? COMM_FE : 0;
			cStatus |= (dwCommErr & CE_OVERRUN) ? COMM_OE : 0;
		}

		// dispatch the state service routine
		//   if not null we have a state change
		//
		TRACE(_T("BuildMsg: dispatch the state service routine.\n"));
		if (m_eState = (this->*m_pfSvc)(*pcInBuf++, cStatus)) {

			// set the state service routine
			//
			unsigned char cCount;
			TRACE(_T("BuildMsg: state service routine had non-null return.\n"));
			for (cCount = 0; g_stRecvFsm[cCount].eState < m_eState; cCount++)
				;
			m_pfSvc = g_stRecvFsm[cCount].pfSvc;
		}
		else
			TRACE(_T("BuildMsg: state service routine had null return.\n"));

		// check to see if we have a complete message!
		//
		if (m_eState == EXIT) {
			TRACE(_T("BuildMsg: have a complete message.\n"));
			pDoneMsg = m_pMsg;
			InitMsg();
		}
		TRACE(_T("BuildMsg: finished processing byte %d of %d.\n"),
				dwBytesProcessed, dwBytesTotal);
		dwBytesRead--;
	}
	TRACE(_T("BuildMsg: finished processing or no data.\n"));

	// if gap then toss the message and start over
	//
	if (bTimedOut) {
		TRACE(_T("BuildMsg: timed out, toss the message, start again.\n"));
		delete m_pMsg;
		InitMsg();
	}
	return pDoneMsg;
}
