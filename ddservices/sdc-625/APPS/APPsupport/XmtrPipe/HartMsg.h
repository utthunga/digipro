/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: HartMsg.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/Include/HartMsg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   HARTMsg
 ***************************************************************************/

/***************************************************************************
 * $History: HartMsg.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/Include
 * 
 ***************************************************************************/
#if !defined(AFX_HARTMSG_H__08586351_AF2D_11D2_A7DC_0000E8D6C3CA__INCLUDED_)
#define AFX_HARTMSG_H__08586351_AF2D_11D2_A7DC_0000E8D6C3CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Hart.h"

class DLLCLASS_EXPORT CHartMsg : public CObject
{
public:
	CHartMsg();	// default constructor usually for use with frame object
	CHartMsg(E_FRAME_TYPE eType,		//construct a long frame command
			unsigned char* pcLongAddr,
			int   cCmd = 1,				// from TBYTE 29jul05 stevev
			unsigned char cBytes = 0,				// no data bytes
			unsigned char* pcBuf = 0,
			unsigned char cMaster = 0x80,		// primary master
			bool bInBurstMode = FALSE	// not in burst mode
	);
	CHartMsg(E_FRAME_TYPE eType,		//construct a short frame command
			unsigned char cShortAddr = 0,
			int   cCmd = 1,				// from TBYTE 29jul05 stevev
			unsigned char cBytes = 0,				// no data bytes
			unsigned char* pcBuf = 0,
			unsigned char cMaster = 0x80,		// primary master
			bool bInBurstMode = FALSE	// not in burst mode
	);
	virtual ~CHartMsg();

	CHartMsg& operator=(CHartMsg& HartMsg);

	void InitMsg(unsigned char cPreambles, E_FRAME_TYPE eType, unsigned char* pcLongAddr, int cCmd,// cmd from TBYTE 29jul05 stevev
					unsigned char cBytes, unsigned char* pcBuf, unsigned char cMaster, bool bInBurstMode);

	//
	// address utilities
	//
	unsigned char MasterAddress (unsigned char cMaster) {
		m_cAddr1 = (m_cAddr1 & ~MSTR) | (cMaster & MSTR);
		return m_cAddr1 & MSTR;
	}

	bool IsLongAddress(void) {
		return ((m_cDelimiter & LONG_FRAME) ? true : false);
	}
	bool IsBroadcastAddress(void);

	bool CompareShortAddress(unsigned char cAddr);
	bool CompareLongAddress(unsigned char *pcAddr);
	bool CompareTag(unsigned char cTag[6]);
	bool CompareLTag(unsigned char cLTag[32]);

	void SetShortAddress(unsigned char cAddr) {
		m_cAddr1 = POLLING_ADDR & cAddr;
	}
	void SetLongAddress(unsigned char *pcAddr);
	unsigned char MasterAddress (void) { 
		return m_cAddr1 & MSTR;
	}

	//
	// other utilities
	//
	unsigned char NPreambles(void) {
		return m_cPreambles;
	}
	unsigned char NPreambles(unsigned char cPreambles) {
		return m_cPreambles = cPreambles;
	}
	unsigned char NFrameExpansionBytes(void) {
		return (m_cDelimiter & EXPANDED) << 5;
	}
	E_FRAME_TYPE FrameType(void) {
		TRACE(_T("CHartMsg::FrameType(): 0x%0.2x & 0x1F.\n"), m_cDelimiter);
		return (E_FRAME_TYPE)(m_cDelimiter & FRAME_MASK);
	}
	E_FRAME_TYPE FrameType(E_FRAME_TYPE eType) {
		TRACE(_T("CHartMsg::FrameType(eType): del -> (0x%0.2x & 0x80) | 0x%0.2x.\n"),
				m_cDelimiter, eType);
		return (E_FRAME_TYPE)(m_cDelimiter = ((m_cDelimiter & LONG_FRAME) | eType));
		// return (E_FRAME_TYPE)(m_cDelimiter = eType & FRAME_MASK);
	}
	bool InBurstMode(void) {
		return (m_cAddr1 & BURSTING) ? true : false;
	}
	bool InBurstMode(bool bMode) {
		m_cAddr1 = (m_cAddr1 & ~BURSTING) | ((bMode) ? BURSTING : 0);
		return InBurstMode();
	}

	//
	// app layer access
	//
	WORD Cmd();
	//TBYTE Cmd(TBYTE cCmdNo) {// cmd from TBYTE 29jul05 stevev		
	int Cmd(int cCmdNo) 
	{	return m_cCommand = cCmdNo;
	}
	unsigned char NBytesData(void) {
		return m_cByteCount;
	}
	unsigned char* GetMsgBytes(unsigned char cMaxBytes, unsigned char* pcBuf);

	//
	// fills the data field starting immediately after the byte count field
	//   assumes caller is handling response code and device status
	//
	void PutMsgBytes(unsigned char cBytes, unsigned char* pcBuf);

	//
	// access to the status bytes
	//
	unsigned char DevStat(void) {
		return m_caData[1];
	}
	unsigned char DevStat(unsigned char cDeviceStatus) {
		return m_caData[1] = cDeviceStatus;
	}
	unsigned char RCode(void) {
		return (m_caData[0] & 0x80) ? 0: m_caData[0];
	}
	unsigned char RCode(unsigned char cResponseCode) {
		return m_caData[0] = cResponseCode & 0x7F;
	}
	unsigned char CommStat(void) {
		return (m_caData[0] & 0x80) ? m_caData[0]: 0;
	}
	unsigned char CommStat(unsigned char cCommStatus) {
		return m_caData[0] = cCommStatus | 0x80;
	}
	//
	// access to the received message staus
	//   makes the current message into a reply. (1)turns around the frame type
	//   (2)writes the burst mode bit, (3) writes the response code and device status,
	//   (4)writes the rest of the data field, (5) sets the response preamble length
	//
	// sets the byte count in the reply message to nData+2
	//
	bool MakeIntoReplyMsg(unsigned char cPreambles, unsigned char cResponseCode, unsigned char cDeviceStatus,
								unsigned char cData, unsigned char* pcData);

	bool MakeIntoReplyMsg(unsigned char cData, unsigned char* pcData);

	void clear(void);
	//
	// actual message data
	//
	unsigned char m_cPreambles;		// counts the number of preambles */
	unsigned char m_cDelimiter;		// the delimiter byte */
	unsigned char m_cAddr1;			// mstr, bursting, (mfr_addr || poll_addr) */
	unsigned char m_cDevType;			// manufacturer assigned devicec type */
	unsigned char m_caDevID[3];		// device ID, unique for every unit shipped of this device type */
	unsigned char m_caExpand[3];		// potential expansion bytes */
	int   m_cCommand;			// command number */	// from TBYTE 29jul05 stevev
	unsigned char m_cByteCount;		// byte count */
	unsigned char m_caData[255];		// the data portion of the message */
	unsigned char m_cBcc;				// the block check character, i.e. the check byte */
    unsigned char m_cDribble;       // number of dribble bytes after check byte
    unsigned char m_cCommStat;		// commStatus generated by the receive message framer

	bool m_bComplete;			// indicates that the frame object successfully created the msg
	E_MSG_STAT m_eMsgStat;	// keep track of the received message's health
	unsigned char m_comm_channel;    // just to keep track


protected:
};

#endif // !defined(AFX_HARTMSG_H__08586351_AF2D_11D2_A7DC_0000E8D6C3CA__INCLUDED_)
