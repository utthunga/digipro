/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: RecvMsg.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/XmtrLlc/RecvMsg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 * Receive Message
 ***************************************************************************/

/***************************************************************************
 * $History: RecvMsg.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

#if !defined(AFX_RECVMSG_H__C3B85421_B070_11D2_A7DE_0000E8D6C3CA__INCLUDED_)
#define AFX_RECVMSG_H__C3B85421_B070_11D2_A7DE_0000E8D6C3CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Hart.h"
#include "HartLlcMsg.h"

/////////////////////////////////////////////////////////////////////////////
//
// CRecvMsg
//
class CRecvMsg : public CObject
{
public:
	CRecvMsg();
	virtual ~CRecvMsg();

	// state servicing routines used to construct a message
	//
	unsigned char InitMsg(void);	//sets up the state machine to receive a message

	// tries to get two ff's and a valid delimiter
	//
	E_RECV_STATE WaitForSOM(unsigned char cIn, unsigned char cStatus);

	E_RECV_STATE GetAddr(unsigned char cIn, unsigned char cStatus);	//gets the address
	E_RECV_STATE GetExpand(unsigned char cIn, unsigned char cStatus);	//gets the frame expansion
	E_RECV_STATE GetCmd(unsigned char cIn, unsigned char cStatus);		//gets the command
	E_RECV_STATE GetBCnt(unsigned char cIn, unsigned char cStatus);	//gets the byte count
	E_RECV_STATE GetData(unsigned char cIn, unsigned char cStatus);	//gets the data bytes
	E_RECV_STATE GetBCC(unsigned char cIn, unsigned char cStatus);		//gets the check byte

	// waits for end of message (no carrer or gap), used when there
	//   is a fatal error (e.g. bad byte count)
	//
	E_RECV_STATE WaitForEOM(unsigned char cIn, unsigned char cStatus);

	// BuildMsg assumes that it will be provided a block of data
	//   via ReadFile, the return from ReadFile is checked, bTimeOut and
	//   dwCommErr are set accordingly. dwCommErr is returned from the
	// ClearCommError routine, also it is assumed the last byte
	//   in the buffer is the one that the errror is associated with
	//
	// BuildMsg returns a Msg object when a message is successfully created,
	//   otherwise it returns null
	//
	CHartLlcMsg* BuildMsg(bool bTimedOut,		// TRUE if the read timed out
								WORD* pwBytesRead,	// address of number of bytes read
								unsigned char *pcInBuf,		// buffer with received data
								DWORD dwCommErr		// receive error codes from ClearCommError
	);

	// used to track the state of the message construction
	//
	E_RECV_STATE (CRecvMsg::*m_pfSvc)(unsigned char cIn, unsigned char cStatus);
	E_RECV_STATE m_eState;

	CHartLlcMsg* m_pMsg;				// the message being built

	// down counter-used to track where we are in
	//   loading the (header / data) buffer
	//
	unsigned char m_cBufferCount;

	// used for filling the message
	//
	unsigned char* m_pcBuffer;
};

#endif // !defined(AFX_RECVMSG_H__C3B85421_B070_11D2_A7DE_0000E8D6C3CA__INCLUDED_)
