/*************************************************************************************************
 *
 * $Workfile: $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the Xmtr class to handle the pipe portion of the transaction (replaces serial)
 */
 

#include "stdwinstuff.h"
#include "PipeStuff.h"
#include "XmtrLlc.h"

#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
#include <process.h>    /* _beginthread, _endthread */
#endif

#include "logging.h"

#ifndef LOGPIPES
bool lE = FALSE;
#else
bool lE = TRUE;	
#endif

#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
unsigned __stdcall HandlePipe ( void* lpvParam )  // Function called as a thread
#else
DWORD HandlePipe( void* lpvParam ) // param is a ptr 2 a CXmtrPipe 
#endif
{
   CXmtrPipe* pPipeCom = (CXmtrPipe*)lpvParam;

   pPipeCom->pipeThread();

#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
   _endthreadex(0);
#else
   ExitThread(0);
#endif
   return 0;
}


CXmtrPipe::CXmtrPipe()
{
	pMutexCtrl = pipeMutex.getMutexControl("PipeMutex");
	dwThreadID = 0;
	hThread = hDieEvent = hSendEvent = hDeadEvent = NULL;
	memset(&(Pipe[0]),0,sizeof(PIPEINST)*INSTANCES);
	memset(&(hEvents[0]),0,sizeof(HANDLE)*(INSTANCES+2));
	memset(&(hXmitBuffer[0]),0,XMTBUFFLEN);
	pClient = NULL;
}
CXmtrPipe::~CXmtrPipe()
{
	if (hDieEvent != NULL && hSendEvent != NULL )
	{
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>EXIT: send pipe die event cmd.\n");
		SetEvent(hDieEvent);
		WaitForSingleObject(hDeadEvent, 500);
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>EXIT: got dead reply or timd out.\n");
		CloseHandle(hDeadEvent);
		CloseHandle(hThread);
	}
	
	// HOMZ - Resolve Memory Leaks
	if (pMutexCtrl != NULL)
	{   // getMutexControl called; Allocated in the constructor by global_operator_new.
		// ------------------------------>  new hCmutexCntrl(mutexHandle, pNm) ;
		delete pMutexCtrl;
	}

}


int CXmtrPipe::initialize(void)
{
	// we spin off the thread to handle the comm
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
	hThread = (HANDLE) _beginthreadex( 
		NULL,					// no security
		0,						// default stack size
		&HandlePipe,            // Address of the function
		(LPVOID) (this),		// Argument list is the Comm
		0,						// starts not suspended
		&dwThreadID);			// returns threadId
#else
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) HandlePipe, // the func
		(LPVOID) (this),			 // pass the comm
		0,						// starts not suspended
		&dwThreadID);			// returns threadId
#endif

	if (hThread == NULL)
	{
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Pipe thread creation failed.\n");
		return FAILURE;
	}
	else
	{
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Pipe thread creation successful.\n");
		return SUCCESS;
	}

}


void	CXmtrPipe::pipeThread(void)		//the infinite loop
{
   DWORD i, k, dwWait, cbBytes, dwErr; 
   BOOL fSuccess; 
   LPTSTR lpszPipename = PIPENAME; 
//	pMutexCtrl->aquireMutex();
//	pMutexCtrl->relinquishMutex();
	TCHAR errMsg[128];

   CHartMsg* pReply = NULL;

	// create pipe(s)
// The initial loop creates several instances of a named pipe 
// along with an event object for each instance.  An 
// overlapped ConnectNamedPipe operation is started for 
// each instance. 
 
	pMutexCtrl->aquireMutex(hCevent::FOREVER);//+++++++++++++++++++++++++++++++++++++
   for (i = 0; i < INSTANCES; i++) 
   {  // Create an event object for this instance. 
 
      hEvents[i] = CreateEvent( 
         NULL,    // no security attribute 
         TRUE,    // manual-reset event 
         TRUE,    // initial state = signaled 
         NULL);   // unnamed event object 

      if (hEvents[i] == NULL) 
         cerr << "ERROR: Event did not construct."<< endl;
	  else
	  {
			LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Creating pipe.\n");
		  Pipe[i].oOverlap.hEvent = hEvents[i]; 
		  Pipe[i].oOverlap.Internal    = 0;
		  Pipe[i].oOverlap.InternalHigh= 0;
		  Pipe[i].oOverlap.Offset      = 0;
		  Pipe[i].oOverlap.OffsetHigh  = 0;
 
		  Pipe[i].hPipeInst = CreateNamedPipe( 
			 lpszPipename,            // pipe name 
			 PIPE_ACCESS_DUPLEX |     // read/write access 
			 FILE_FLAG_OVERLAPPED,    // overlapped mode 
			 PIPE_TYPE_MESSAGE |      // message-type pipe 
			 PIPE_READMODE_MESSAGE |  // message-read mode 
			 PIPE_WAIT,               // blocking mode 
			 INSTANCES,               // number of instances 
			 PIPEBUFFERSIZE,          // output buffer size 
			 PIPEBUFFERSIZE,          // input buffer size 
			 PIPE_TIMEOUT,            // client time-out 
			 NULL);                   // no security attributes 

		  if (Pipe[i].hPipeInst == INVALID_HANDLE_VALUE) 
		  { 	cerr << "ERROR: Named pipe did not create."<< endl;
				Pipe[i].dwState = ERROR_STATE;
				/*** 16jan06 ***/
				LPVOID lpMsgBuf;
				FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &lpMsgBuf,
					0,
					NULL 
				);
				// Process any inserts in lpMsgBuf.
				// ...
				// Display the string.
				cerr <<"ERROR: Pipe failed to create? Error:"<<(LPCTSTR)lpMsgBuf<<endl;
				// Free the buffer.
				LocalFree( lpMsgBuf );

				/*** 16jan06 end */
				LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Pipe creation failed.\n");
		  }
		  else
		  {// Call the subroutine to connect to the new client
 
				LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Pipe (%d) creation successful.(%s)\n",i,lpszPipename);
				Pipe[i].dwState = ConnectToNewClient( 
				 Pipe[i].hPipeInst, 
				 &Pipe[i].oOverlap); 
			  // Connect & rdidle need no action
			  if (Pipe[i].dwState == ERROR_STATE)
			  {
				  DisconnectAndReconnect(i); // if still in error_state...so be it
			  }
 
		  }// endif
	  }//endif
   } // next

   // setup the die event
   hDieEvent  = CreateEvent( 
							 NULL,    // no security attribute 
							 TRUE,    // manual-reset event 
							 FALSE,   // initial state = NOTsignaled 
							 NULL);   // unnamed event object    
   hDeadEvent = CreateEvent( // the handshake to dieevent
							 NULL,    // no security attribute 
							 FALSE,   // auto-reset event 
							 FALSE,   // initial state = NOTsignaled 
							 NULL);   // unnamed event object 

   hEvents[INSTANCES]   = hDieEvent;
   hSendEvent = CreateEvent( 
							 NULL,    // no security attribute 
							 FALSE,   // auto-reset event 
							 FALSE,   // initial state = NOTsignaled 
							 NULL);   // unnamed event object 
   hEvents[INSTANCES+1] = hSendEvent; //pktRcvdQueue.GetArrivalEvent();// we need to write
	pMutexCtrl->relinquishMutex();//--------------------------------------
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*DieEvent  Hndl:0x%04x\n",hDieEvent);
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*DeadEvent Hndl:0x%04x\n",hDeadEvent);
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*SendEvent Hndl:0x%04x\n",hSendEvent);
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*Entering infinite loop\n");
#ifdef _DEBUG
	const char *eventName[INSTANCES+2] = { "Pipe zero" ,"Die Event" , "Send Event" };//"Pipe one" , 
#endif
	int lastObj = INSTANCES+1; // ie WAIT_OBJECt_0 to WAIT_OBJECT_0 + count -1 (or INSTANCES+2-1)
   while (1)  /* thread's infinite loop * * * * * * * * * * * * * * * * * * * * * * * * * * * */
   { // wait for a client 

   // Wait for the event object to be signaled, indicating 
   // completion of an overlapped read, write, or 
   // connect operation. (assume only one triggers this return, others will show up after loop)
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* entering wait for multiple objects.\n"); 
      dwWait = WaitForMultipleObjects( 
         INSTANCES+2,  // number of event objects (+1, die only, +2 die and write queue )
         hEvents,      // array of event objects 
         FALSE,        // does not wait for all 
         INFINITE);    // waits indefinitely 
 
   // dwWait shows which event completed the operation. 
	  if ( dwWait >= WAIT_OBJECT_0 && dwWait <= (WAIT_OBJECT_0 + lastObj) )
	  {
			i = dwWait - WAIT_OBJECT_0;
	  }
	  else
	  if ( dwWait >= WAIT_ABANDONED_0 && dwWait <= (WAIT_ABANDONED_0 + lastObj) )
	  {
#ifdef _DEBUG
		  int y = (dwWait - WAIT_ABANDONED_0);
		  LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* WAIT_ABANDONED #%d (%s)\n",y,eventName[y]);
#endif
			i = -2;
	  }
	  else // has to be FAILED
	  {
		  LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* WAIT FAILED 0x%08x )\n");
			i = dwWait;
	  }

 
	  LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got 0x%x from the wait.\n",i);
	  if (i == (INSTANCES)) // the die scenario
	  {
		  LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* Pipe Handler is dying.\n");
		  break; // out of infinite loop
	  }
	  else
	  if (i == (INSTANCES+1)) // a write is ready to go
	  {	  
		  // assume there is only one packet at a time for now(no burst & reply at the same time)
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* entering write sequence.\n");		  
		pClient = GetClientByPort(PIPE_PORT, ONLY_CLIENT);// only 1 port & 1 client for now
		if (!pClient)
		{
			cerr <<"ERROR: Pipe has no clients???"<<endl;
		}
		else
		{
//		  wrtPkt = pktRcvdQueue.GetNextObject();
			pReply = pClient->m_pSlaveReply;

			// convert cmd/bytecnt/data to string and send it through the pipe

		  if ( pReply == NULL )
		  {
			  cerr << "ERROR: empty transmit queue on transmit event."<<endl;
			  LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* client's reply pointer is empty. \n");
			  continue; // loop to try again
		  }
		  else
		  {
			  k = pReply->m_cAddr1; // pri/sec master doubles as the channel number
			  if (k < 0 || k > (INSTANCES-1) ) 
			  {
				  cerr <<"ERROR: Packet channel out of range (" << k << ")"<<endl;
//				  pktRcvdQueue.ReturnMTObject(wrtPkt); 
				  pReply = NULL;
				  continue; // loop to try again
			  }
			  else
			  {// we have a pkt & channel
				  if ( Pipe[k].dwState != READING_STATE)//only valid in the waiting for read state
				  {
					  // programmer error - do not change overlap state
//					pktRcvdQueue.ReturnMTObject(wrtPkt); 
//					wrtPkt = NULL;
					pReply = NULL;
					cerr <<"ERROR: Pipe not idle at Write (" << k 
														<< ", "<< Pipe[k].dwState<< ")"<<endl;
					continue; // loop to try again
				  }
				  else
				  {// cancelio, write, >> readIdle || writeex
						pMutexCtrl->aquireMutex(hCevent::FOREVER);//+++++++++++++++++++++++++++++++++++++++++++++   
						if ( CancelIo(Pipe[k].hPipeInst) ) // success - stops the overlapped Read
						{
							Pipe[k].dwNxtState = sendMsg(k,pReply);//wrtPkt);
							LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*Message Sent\n");
						}
						else
						{// cancelio failed  error
							dwErr = GetLastError();
							cerr << "ERROR: Pipe("<<k<<") failed to cancel io. errCode:"
																					<<dwErr<<endl;
							// don't change overlapped state
							Pipe[k].dwNxtState = CONNECTING_STATE;
						}// end-else cancelio success
						i = k; // for next state processing
						pMutexCtrl->relinquishMutex();//-----------------------------------------
				  }// endelse - currently in the idle-read state
				  // if we got here then we tried to cancelio
			  }// end-else we have a packet and channel
			  // if we get here, we tried to canel io			  
		  }// end-else we have a write packet
		  // if we get here - discard the packet
//			pktRcvdQueue.ReturnMTObject(wrtPkt); 
//			wrtPkt = NULL;
		  pReply = NULL;
		}//endelse - we have a client
	  }// endif - the event was a write ready
	  else	  
      if (i < 0 || i > (INSTANCES- 1))// )) // not a pipe
	  {	
LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* error: pipe channel out of range from wait.\n");
         cerr << "ERROR: Pipe channel (from event) out of range.("<<i<<")"<< endl;
		 // don't change overlapped state **********
		 Pipe[k].dwNxtState = CONNECTING_STATE;
	  }
	  else // not write, event channel valid
	  {// Get the result if the operation was pending. 	
		  	  
		pClient = GetClientByPort(PIPE_PORT, ONLY_CLIENT);// only 1 port & 1 client for now
		pMutexCtrl->aquireMutex(hCevent::FOREVER);//+++++++++++++++++++++++++++++++++++++
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got the pipe mutex.\n");
		 // we're ALWAYS PENDING: except error state
	
		if ( Pipe[i].dwState == ERROR_STATE )
		{// possible channel not connected
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* current pipe state is ERROR.\n");
			Pipe[i].dwNxtState = Pipe[i].dwState;// do a discon/recon			
			pMutexCtrl->relinquishMutex();//16jan06 -----------------------------------------
		}
		else
		{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* getting overlapped result.\n");
			fSuccess = GetOverlappedResult( 
				Pipe[i].hPipeInst, // handle to pipe 
				&(Pipe[i].oOverlap), // OVERLAPPED structure 
				&cbBytes,          // bytes transferred 
				FALSE);            // do not wait 

			Pipe[i].cbToWrite = cbBytes; // to pass it around by pipe number
			errMsg[0] = '\0';
			if (! fSuccess) 
			{
				dwErr = GetLastError();
				
				FormatMessage( 
					/*FORMAT_MESSAGE_ALLOCATE_BUFFER |	/ DWORD dwFlags*/
					FORMAT_MESSAGE_FROM_SYSTEM | 
					       FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,								/*LPCVOID lpSource*/
					dwErr,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					errMsg,
					128,
					NULL 
					);
			}
			switch (Pipe[i].dwState) 
			{ // Pending connect operation 
			case CONNECTING_STATE: 
				if (! fSuccess) 
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result with an error in the CONNECTING state\n");
				   cerr << "ERROR: Connect to name pipe failed.(" << i << ")" <<
					   " error:" << dwErr << "{" << errMsg <<"}"<< endl;
					Pipe[i].dwNxtState = ERROR_STATE; // causes a disconnect/reconnect
				}
				else
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result, in CONNECTING state, going to IDLE_RD.( %d )\n",cbBytes);
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			 // Pending read completion operation 
			case READING_STATE: 
				if (! fSuccess || cbBytes == 0) 
				{ 
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result with an error in the READING state. read: %d\n",cbBytes);
				   cerr << "ERROR: ReadingEx state failed.(pipe: "<< i << ")" <<  
					   " error:" << dwErr << "{" << errMsg <<"}"<< endl;
					Pipe[i].dwNxtState = ERROR_STATE; 
				  //DisconnectAndReconnect(i); 
				  //continue; 
				} 
				else // success
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result, in READING state, parsing msg then to IDLE_RD. %d\n",cbBytes);
				   // preparse packet process
				   // execute RcvMsg in dispatch
					rcv_Msg(i);
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			 // Pending write completion operation 
			case WRITING_STATE: 
				if (! fSuccess || cbBytes != Pipe[i].cbToWrite) 
				{  
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result with an error in the WRITING state. wrote: %d"
											" Wanted to write: %d\n",cbBytes,Pipe[i].cbToWrite);
				   cerr << "ERROR: WritingEx state failed.("<<i<<")" <<  
					   " error:" << dwErr << "{" << errMsg <<"}"<< endl;
					Pipe[i].dwNxtState = ERROR_STATE; 
				  //DisconnectAndReconnect(i); 
				  //continue; 
				} 
				else // success
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result, in WRITING state, going to IDLE_RD.\n");
					Pipe[i].dwNxtState = IDLE_RD_STATE; 
				}
			break; 

			case IDLE_RD_STATE: // this is a temporary, next state only
				// this occurs when a client is waiting during connect - fall to nxtstate
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result, in IDLE_RD ,anomoly>, going to state %d\n", Pipe[i].dwState);
				Pipe[i].dwNxtState = Pipe[i].dwState;
			break;  // nxt state handler will deal with it

			case ERROR_STATE: // won't happen
			default: 
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* got result, in ERROR state - not supposed to happen.\n");
				// error loop from below
				Pipe[i].dwNxtState = ERROR_STATE; 
			break;
			}  // endswitch
			pMutexCtrl->relinquishMutex();//-----------------------------------------
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* gave up Pipe mutex\n");
		}// endelse not in error state
	  }// end-else all event handling

 
	  // HANDLE THE NxtState	    
 
	  switch (Pipe[i].dwNxtState) 
	  { 
	  case CONNECTING_STATE:		/* no-op in nxtState */
		  {
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:CONNECTING\n");
			  ;// no-op
		  }
		  break;
	  case READING_STATE:			/* readingEx -- illegal in next state      */
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:READING ** ERROR **\n");
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>WARNING: Next State == READING_STATE.\n");// fall through
	  case WRITING_STATE:			/* writingEx -- extended             */
		  {
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:WRITING (extended)\n");
			  Pipe[i].dwState    = Pipe[i].dwNxtState;
			  Pipe[i].dwNxtState = CONNECTING_STATE;
		  }
		  break;

	  case ERROR_STATE:				/* forces disconnect/reconnect in nxtstate */
		  { 
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:ERROR (will disconnect/reconnect)\n");
				DisconnectAndReconnect(i); // state to connecting or error or idle read
		  }
		  break;
	  case IDLE_RD_STATE:			/* reading an internal temporary next state */
		  // The pipe instance is connected to the client 
		  // A new overlapped read is needed to pend on another msg. 
		  {
				pMutexCtrl->aquireMutex(hCevent::FOREVER);//+++++++++++++++++++++++++++++++++++++
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:IDLE_RD - have mutex\n");
			  while (1) // until break
			  {
				  Pipe[i].cbToWrite = 0;
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*    : Reading overlapped file.\n");
				fSuccess = ReadFile( 
				   Pipe[i].hPipeInst, 
				   Pipe[i].chBuf, 
				   PIPEBUFFERSIZE, 
				   &cbBytes, 
				   &(Pipe[i].oOverlap) ); 

				dwErr = GetLastError();
			 // The read operation completed successfully (msg was waiting).  
				if (fSuccess && cbBytes != 0) 
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*    : got (%d)data - process it\n",cbBytes); 
					Pipe[i].cbToWrite = cbBytes; // to pass it around by pipe number
					// preparse packet process
					// execute RcvMsg in dispatch
					rcv_Msg(i);
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*    : loop to see if there is any more in the pipe.\n"); 
					continue; // loops through the while() to read any other messages in the pipe
				} 
				else 
				if ( (! fSuccess)  && (dwErr == ERROR_IO_PENDING ) )
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*    : got (%d)data - but more IO_PENDING - go to READING state\n",cbBytes); 
					// the normal pending state
					  Pipe[i].cbToWrite  = cbBytes; // to pass it around by pipe number
					  Pipe[i].dwState    = READING_STATE;
					  Pipe[i].dwNxtState = CONNECTING_STATE;
					  // fall through to break out of read loop and loop to pend on read/write
				}
				else // ! success || bytes == 0
				{
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>*    : got (%d)" 
			" data but don't know what's wrong.Err: %d  next = ERROR state\n",cbBytes,dwErr);
					Pipe[i].dwState    = ERROR_STATE;
					Pipe[i].dwNxtState = ERROR_STATE;
					// fall through to break out of read loop and loop to disconnect/reconnect
				}
				break; // always break unless continue above
			  }// wend
			  
				pMutexCtrl->relinquishMutex();//-----------------------------------------
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* NXT:IDLE_RD - gave up mutex\n");
			}
			break;  
 
         default: 
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* error: invalid pipe NXT state.\n");
            cerr << "ERROR: invalid pipe Next state (" << Pipe[i].dwNxtState << ")"<<endl;
		} // end switch  
 LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* Loop to execute again----------\n");
  } // wend infinity


	// if it closes 
	// disconnect active pipes, close all handles
	for (i = 0; i < INSTANCES; i++)
	{
		if (Pipe[i].dwState != ERROR_STATE)
		{
			DisconnectNamedPipe(Pipe[i].hPipeInst);
			CloseHandle(Pipe[i].hPipeInst);
			CloseHandle(Pipe[i].oOverlap.hEvent);
		}
	}   
	SetEvent(hDeadEvent);   // the killer must close this handle
	CloseHandle(hDieEvent);
	CloseHandle(hSendEvent);
    // pktRcvdQueue.must close its own handles
	// exit thread
LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>Exiting infinite loop (thread death).\n");
	
}



// DisconnectAndReconnect(DWORD) 
// This function is called when an error occurs or when the client 
// closes its handle to the pipe. Disconnect from this client, then 
// call ConnectNamedPipe to wait for another client to connect. 
 
VOID CXmtrPipe::DisconnectAndReconnect(DWORD i) 
{ 
// Disconnect the pipe instance. 
 
	pMutexCtrl->aquireMutex(hCevent::FOREVER);//+++++++++++++++++++++++++++++++++++++

//	TRACE(_T("PipeHandler's Discon/Recon called.\n"));
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>PipeHandler's Discon/Recon called on %d pipe.\n",i);
   if (! DisconnectNamedPipe(Pipe[i].hPipeInst) ) 
   {
      cerr << "ERROR: Disconnect failed on pipe " << i <<"."<<endl;
   }
   else
   { // Call a subroutine to connect to the new client. 
LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* Disconnected %d - Reconcecting.\n",i);
	   Pipe[i].dwState = ConnectToNewClient( 
		  Pipe[i].hPipeInst, 
		  &Pipe[i].oOverlap); 
 
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>  New Reconnect state: \n");
		if ( Pipe[i].dwState == CONNECTING_STATE )
		{
			LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>      CONNECTING_STATE\n");
		}
		else
		{
			LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>      IDLE_RD_STATE\n");
		}
   }
	pMutexCtrl->relinquishMutex();//--------------------------------------
} 
 
// ConnectToNewClient(HANDLE, LPOVERLAPPED) 
// This function is called to start an overlapped connect operation. 
// It returns a new state:   
// CONNECTING_STATE @ overlapped in progress
// ERROR_STATE      @ error, >>>>  caller must handle this one. < suggest a discon/recon >
// IDLE_RD_STATE    @ client was waiting, we are connected!(the event was set so no action needed)
 
DWORD CXmtrPipe::ConnectToNewClient(HANDLE hPipe, LPOVERLAPPED lpo) 
{ 
   BOOL fConnected;
   DWORD exitstate, le; 
 
// Start an overlapped connection for this pipe instance. 
   fConnected = ConnectNamedPipe(hPipe, lpo); 
 
// Overlapped ConnectNamedPipe is zero on success. 
   if (fConnected) 
   {
	  cerr << "ERROR: connect to named pipe succeeded incorrctly." <<endl;
	  exitstate = ERROR_STATE; // will try again later
   }
   else // returned 0
   { 
	   switch ( le = GetLastError()) 
	   { 
	   // The overlapped connection in progress. 
	   case ERROR_IO_PENDING: 
		 exitstate = CONNECTING_STATE; // normal, nobody-connected wait state
		 break; 
		 // client closed it's handle but we haven't disconnected
	   case ERROR_NO_DATA:
		 exitstate = ERROR_STATE;      // force a disconnect/reconnect
		 break; 
 
	   // Client is already connected, so signal an event to force a read.  
	   case ERROR_PIPE_CONNECTED: 
		  {
			 exitstate = IDLE_RD_STATE;
		  }
		  break; 
 
	   // If an error occurs during the connect operation... 
	   default: 
		  {
			cerr <<"ERROR: connect to named pipe with unknown error.("<<le<<")"<<endl; 
			exitstate = ERROR_STATE;
		  }
	   } // endswitch
   }// end if-else connection

   if ( exitstate == IDLE_RD_STATE )
   {// make the handler wakeup and take action
	    if (! (SetEvent(lpo->hEvent)) )
		{
			exitstate = ERROR_STATE;
		}
   }
 
   return exitstate; 
} 



// handles the packet write function for the pipe < was a big section in the state machine above >
// calls pkt2Msg
// returns nxtState
// DWORD CXmtrPipe::sendPkt(int pipeNum, hPkt* pPkt2Wrt) 
DWORD CXmtrPipe::sendMsg(int pipeNum, CHartMsg* pMsg2Wrt) 	// returns nxtState
{
	DWORD returnState = IDLE_RD_STATE;// the normal continue state
	DWORD dwErr,  cbBytes = 0;
	int t;
	BOOL  fSuccess;

	if ( msg2str(t, pMsg2Wrt) == SUCCESS ) // generate a pipe string from hart message
	{
		Pipe[pipeNum].cbToWrite = t;
		strncpy(((char*)&(Pipe[pipeNum].chBuf[0])),hXmitBuffer,t);
hXmitBuffer[t] = '\0';
LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* Writing |%s|\n",hXmitBuffer);
		fSuccess = WriteFile( 
		   Pipe[pipeNum].hPipeInst, 
		   Pipe[pipeNum].chBuf, 
		   Pipe[pipeNum].cbToWrite, 
		   &cbBytes,           // return value
		   &Pipe[pipeNum].oOverlap);  
		if ( fSuccess )
		{
			if (cbBytes == Pipe[pipeNum].cbToWrite)
			{
				returnState = IDLE_RD_STATE;// we're done, and successful too
			}
			else // success but incorrectly written - odd
			{
				cerr << "ERROR: success but wanted to write "
									<< Pipe[pipeNum].cbToWrite << "...wrote "<< cbBytes << endl;
				returnState = ERROR_STATE;  // force a disconnect/reconnect
			}
		}
		else // write - not success
		{
			dwErr = GetLastError(); 
			if (dwErr == ERROR_IO_PENDING) 
			{ 
				cerr << " WriteEx: want to write "<< Pipe[pipeNum].cbToWrite 
																<< "...wrote "<< cbBytes << endl;
				returnState = WRITING_STATE;// extended write
			} 
			else
			{
				returnState = ERROR_STATE;  // force a disconnect
				cerr<< "ERROR:  Write error: " << dwErr << endl;
			}
		}// endif write success (or not)
	}
	else
	{// msg 2 str failed for write -- should never happen 
		// (it'll generate an error msg)
		cerr << "ERROR: string from message failed!."<<endl;
		returnState = IDLE_RD_STATE;// we cancelled so restart the read pend
	}// end else message generation

	return returnState;
}

//    returns SUCCESS||FAILURE, data in buffer
int CXmtrPipe::msg2str(int& strLen, CHartMsg* pMsg2Wrt)
{
	
//	TBYTE m_cCommand;			// command number */
//	TBYTE m_cByteCount;		// byte count */
//	TBYTE m_caData[255];		// the data portion of the message */
//	TBYTE m_cBcc;				// the block check character, i.e. the check byte */
//    TBYTE m_cDribble;       // number of dribble bytes after check byte
//    TBYTE m_cCommStat;		// commStatus generated by the receive message framer

//	bool m_bComplete;			// indicates that the frame object successfully created the msg
//	E_MSG_STAT m_eMsgStat;	// keep track of the received message's health

	int bufLoc = 0;
	int y, i,L;
	RETURNCODE retVal = FAILURE;// deal with failure first
	hXmitBuffer[0] = '\0';
	strLen = 0;

	if ( pMsg2Wrt != NULL )
	{
		L = y = sprintf(hXmitBuffer, "%02x %02x ", pMsg2Wrt->m_cCommand, pMsg2Wrt->m_cByteCount);// y == 6
		if ( pMsg2Wrt->m_cCommand < 256 && y != 6 )
		{
			cerr << "ERROR: sprintf error in sendMsg. (expected 6, got "<< y <<")"<<endl;
			hXmitBuffer[0] = '\0';
			retVal     = FAILURE;
		}
		else
		if ( pMsg2Wrt->m_cCommand > 255 && pMsg2Wrt->m_cCommand < 4096 && y != 7 )
		{
			cerr << "ERROR: sprintf error in sendMsg. (expected 7, got "<< y <<")"<<endl;
			hXmitBuffer[0] = '\0';
			retVal     = FAILURE;
		}
		else
		if ( pMsg2Wrt->m_cCommand > 4095 && pMsg2Wrt->m_cCommand < 65536 && y != 8 )
		{
			cerr << "ERROR: sprintf error in sendMsg. (expected 8, got "<< y <<")"<<endl;
			hXmitBuffer[0] = '\0';
			retVal     = FAILURE;
		}
		else
		{
			for ( i = 0; i < pMsg2Wrt->m_cByteCount; i++ )
			{
				y += sprintf( &(hXmitBuffer[y]),"%02x",pMsg2Wrt->m_caData[i]);
			}
			if ( y != ((pMsg2Wrt->m_cByteCount*2)+L) )
			{
				cerr << "ERROR: sprintf error in sendMsg. (expected "<< ((pMsg2Wrt->m_cByteCount*2)+L) <<
					" got "<< y <<")"<<endl;
				hXmitBuffer[0] = '\0';
				retVal = FAILURE;
			}
			else
			{// OK, send it				
				strLen = y;
				retVal = SUCCESS;
			}
		}
	}
	// else return failure

	return retVal;
}

// we have a message, 
// preparse to hartmsg for dll interface
// call dispatch recieve function
int   CXmtrPipe::rcv_Msg(int pipeNum)         // zero == success, doesMsg2Pkt, Pkt to rcv
{
	int rc = 0;// success

	// we need to:
	// get the data from the pipe ir Pipe[i].chBuf has the data
	// put it into the client m_pHartMsg structure
	// send the windows message
Pipe[pipeNum].chBuf[Pipe[pipeNum].cbToWrite] = '\0';
LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*>* Received |%s|\n",Pipe[pipeNum].chBuf);

	if ( (rc = parseStr2Msg(pipeNum, Pipe[pipeNum].chBuf, Pipe[pipeNum].cbToWrite, 
														&(pClient->m_Msg)) ) == SUCCESS )
	{
		pClient->m_pHartMsg =  &(pClient->m_Msg);
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*= Signalling Client for pipe #%d\n",pipeNum);
		pClient->m_pClientWnd->PostMessage(pClient->m_dwClientNotifyEvent,
			(WPARAM)Pipe[pipeNum].cbToWrite,(LPARAM)pClient);
	}
	else
	{// couldn't parse it
		pClient->m_Msg.clear();
	}


	return rc;
}


int CXmtrPipe::parseStr2Msg(int pipeNumber, BYTE* msgBuf, DWORD bufLen, CHartMsg* destMsg)
{
	RETURNCODE rc = SUCCESS;
	int  tmpInt = -1, c = 0,L;
	char shortStr[5] = {'\0','\0','\0','\0','\0'};

	if ( destMsg == NULL || msgBuf == NULL || bufLen < 6 )// 5 is cmd & 00 byte count
	{
		cerr <<"ERROR: parameter is null in parseStr2Msg."<<endl;
		rc = APP_PARAMETER_ERR;
	}
	else
	{// parameters OK
		destMsg->clear();
		//if (pipeNumber == 0)
		//{
		//	destMsg->m_cAddr1 = PRI;
		//}
		//else
		//{
		//	destMsg->m_cAddr1 = SEC;
		//}
		destMsg->m_cAddr1 = pipeNumber;

		if ( msgBuf[bufLen-1] == '\0' )
		{	bufLen--;				}

		if ( msgBuf[2] == ' ' )
		{	
			L = 2;	}
		else
		if ( msgBuf[3] == ' ' )
		{	
			L = 3;	}
		else
		if ( msgBuf[4] == ' ' )
		{	
			L = 4;	}
		else
		{	
			L = 0;	}
		if ( L > 0 )
		{
			shortStr[0] = msgBuf[0]; shortStr[1] = msgBuf[1];
			if ( L > 2 )
			{
				shortStr[2] = msgBuf[2];
				if ( L > 3 )
				{
					shortStr[3] = msgBuf[3];
					c = sscanf(shortStr,"%04x",&tmpInt);
				}
				else
				{
					c = sscanf(shortStr,"%03x",&tmpInt);
				}
			}
			else
			{
				c = sscanf(shortStr,"%02x",&tmpInt);
			}
			if ( c == 1 )
			{
				destMsg->m_cCommand = tmpInt;
			}
			else
			{
				destMsg->m_cCommand = -1;
				cerr << "ERROR: scanf of |" << shortStr << "| failed with " <<
																	c << " scanned."<<endl;
			}
			L++;
			msgBuf = &(msgBuf[L]);
			bufLen -= L;
			if ( bufLen > 2 )
			{
				if ( msgBuf[2] != ' ' )
				{
					cerr << "ERROR: message buffer has incorrect delimiting. |"<<
																			msgBuf<< "|"<<endl;
					rc = APP_COMMAND_ERROR;
				}
				// else fall through
			}
			else // it had to be 5 or we wouldn't get here
			{
				bufLen++; // to match later
			}
			shortStr[0] = msgBuf[0]; shortStr[1] = msgBuf[1];

			if ( rc == SUCCESS )
			{// continue
				sscanf(shortStr,"%02x",&tmpInt);
				destMsg->m_cByteCount = tmpInt;
				msgBuf = &(msgBuf[3]);
				bufLen -= 3;
				if ( bufLen != destMsg->m_cByteCount * 2) // 2 ascii bytes per data byte
				{
					cerr << "ERROR: message buffer has incorrect size. BufSize="<<bufLen<<
						 " ByteCnt="<< destMsg->m_cByteCount << endl;
					rc = APP_COMMAND_ERROR;
				}
				else
				{// good match, do the data
					int y,z;
					for (y = 0, z = 0; y < destMsg->m_cByteCount; y++, z+=2)
					{
						shortStr[0] = msgBuf[z]; shortStr[1] = msgBuf[z+1];
						sscanf(shortStr,"%02x",&tmpInt);
						destMsg->m_caData[y] = tmpInt;
					}
				}
			}
			// else just return the rc

		}
		else
		{
			cerr << "ERROR: message buffer has incorrect delimiting. |"<< msgBuf<< "|"<<endl;
			rc = APP_COMMAND_ERROR;
		}
	}

	return rc;
}
