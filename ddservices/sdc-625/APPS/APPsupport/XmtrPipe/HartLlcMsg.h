/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: HartLlcMsg.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/Include/HartLlcMsg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   HartLlcMsg
 ***************************************************************************/

/***************************************************************************
 * $History: HartLlcMsg.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/Include
 * 
 ***************************************************************************/
#if !defined(AFX_HARTLLCMSG_H__1E59A573_CB36_11D2_A7F8_0000E8D6C3CA__INCLUDED_)
#define AFX_HARTLLCMSG_H__1E59A573_CB36_11D2_A7F8_0000E8D6C3CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//#include "Serial.h"		/* for the DLL */
#include "PipeStuff.h"
#include "HartMsg.h"

class CHartLlcMsg : public CHartMsg  
{
public:
	CHartLlcMsg();
	unsigned char XmitMsg(CSerial* pSerial);

	virtual ~CHartLlcMsg();
};

#endif // !defined(AFX_HARTLLCMSG_H__1E59A573_CB36_11D2_A7F8_0000E8D6C3CA__INCLUDED_)
