/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: Hart.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/Include/Hart.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 * Hart.h : This file defines the HART communication error byte
 * 
 *   Bit #6 Vertical Parity Error - The parity of one or more of the bytes
 *      received by the device was incorrect.
 *   Bit #5 Overrun Error - At least one byte of data in the receive buffer of
 *      the UART was overwritten before it was read.
 *   Bit #4 Framing Error - The Stop Bit of one or more bytes received by the
 *      device was not detected by the UART.
 *   Bit #3 Longitudinal Parity Error - The Longitudinal Parity calculated by
 *      the device did not match the Longitudinal Parity byte at the end of
 *      the message.
 *   Bit #2 Reserved, set to zero.
 *   Bit #1 Buffer Overflow - The message was too long for the receive buffer
 *       of the device.
 *   Bit #0 Undefined - Not defined at this time.
 ***************************************************************************/

/***************************************************************************
 * $History: Hart.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/Include
 * 
 ***************************************************************************/

#if !defined(AFX_XMTR_H__2DE1EE41_B09E_11d2_A7DE_0000E8D6C3CA__INCLUDED_)
#define AFX_XMTR_H__2DE1EE41_B09E_11d2_A7DE_0000E8D6C3CA__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//                       DLL INTERFACE STUFF

#ifdef _USEXMTR_DLL   //--------------------use Dynamic library
  #pragma message("_USEXMTR_DLL in hart.h.")
//EXTERN_C     is  extern "C"
//AFX_EXT_API  is '__declspec(dllimport)'
  #ifdef DLLFUNC
    #undef DLLFUNC
  #endif
  #define DLLFUNC EXTERN_C AFX_EXT_API
// #define DLLFUNC EXTERN_C AFX_EXT_API HRESULT CALLBACK

  #define DLLCLASS_EXPORT  AFX_EXT_CLASS       /* extention dll class export */
#else // ------------------------------------use a linked (Pipe) library
  #ifdef DLLFUNC
    #undef DLLFUNC
  #endif
  #define DLLFUNC EXTERN_C
  #define DLLCLASS_EXPORT  /* empty               no class export */
#endif

class CClient;

///////////////////////////////////////////////////////////////////////////////
//
// HART Data
//
#define L_ADDR									5
#define S_ADDR									1

// the definition of the delimiter byte
//
#define LONG_FRAME							0x80
#define EXPANDED								0x60
#define FRAME_MASK							0x67

// TAG, long TAG
#define TAG_LENGTH							6
#define LTAG_LENGTH							32
// definition used in first address byte
//
#define MSTR									0x80
#define PRI										0x80
#define SEC										0x00
#define BURSTING								0x40
#define MFR_ADDR								0x3F
#define POLLING_ADDR							0x3F

// defines for CSerial, CHartMsg, CRecvMsg
//
#define DCB_FSK								"baud=1200 parity=O data=8 stop=1"
#define DCB_PSK								"baud=19200 parity=O data=8 stop=1"
// #define DCB_FSK								"1200,O,8,1"
// #define DCB_PSK								"19200,O,8,1"

#define LINE_LENGTH							80
#define MAX_CLIENTS							10
#define MAX_READ_BUF							300
#define MAX_WRITE_BUF						300
#define MAX_PORT								40
#define PURGE_FLAGS						(	PURGE_TXABORT |	\
													PURGE_TXCLEAR |	\
													PURGE_RXABORT |	\
													PURGE_RXCLEAR		)

#define SHORT_BUF								5
#define STALL_TIME							10


// defining the status word to be carried along with a message
//
typedef enum {
	ESUCCESS =	0,
	NO_MSG,					// no message i.e. never saw the SOM
	ADDR_ERR,				// error receiving the address
	EXP_ERR,					// error while getting the expansion bytes
	BC_ERR,					// error receiving the byte count
	PE, FE, OE,				// UART error (command or data fields only
	BCCE,						// bad BCC
	GAP_ERR					// this one is left as an exercise for the student 
} E_MSG_STAT;

// used to index the virtual function pointers for accessing header data
//
enum { GETCMD, GETBC, PUTCMD, PUTBC, EXPAND };


// there can be several different headers that come with a HART message
// we will define 8 different images using structures to map these combinations
//
// start with long frame messages with differing expansion bytes
//
typedef enum { FSK = 0x01, PSK } E_PHL;

typedef enum {
	PSK_STX		=	0x0A,
	PSK_ACK		=	0x0E,
	PSK_BACK		=	0x09,

	STX			=	0x02,
	ACK			=	0x06,
	BACK			=	0x01,
} E_FRAME_TYPE;

typedef enum {
	// first commStatus as defined in the HART specs
	//
	COMM_PE		=	0x40,
	COMM_OE		=	0x20,
	COMM_FE		=	0x10,
	COMM_BCCE	=	0x08,		// Block Check Character
	COMM_BOFE	=	0x02,		// Buffer Over Flow

	// implementation specific status information
	//
	COMM_GAP		=	0x01,		// Gap Error
	COMM_CTS		=	0x04,		// CTS state change
	COMM_DCD		=	0x05		// DCD state change
} E_COMM_ERR;

typedef enum {
	//NULL = 0,
	GET_SOM		=	1,	
	GET_ADDR,
	GET_EXPAND,
	GET_CMD,
	GET_BCNT,
	GET_DATA,
	GET_BCC,
	WAIT_EOM,
	EXIT			=	0xFF
} E_RECV_STATE;

// Serial port info structure
//
typedef struct {
	HANDLE hCommPort;
	DWORD dwCommError;

	WORD wBytesRead;
	WORD wBytesWritten;
	bool bInUse;
	unsigned char nothing[3];							// filler

	unsigned char cWriteBuffer[MAX_WRITE_BUF];
	unsigned char cReadBuffer[MAX_READ_BUF];

	COMMTIMEOUTS stComTimeOuts;
	DCB stDCB;
} COMM_DATA, *P_COMM_DATA;

//
// END HART Data
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Client Data
//

// what serial port will be used?
//
typedef enum {
	COM1,
	COM2,
	COM3,
	COM4,
	COM5,
	COM6,
	COM7,
	COM8,
	COM9,
	COM10,
	COM11,
	COM12,
	COM13,
	COM14,
	COM15,
	COM16,
	COM17,
	COM18,
	COM19,
	COM20,
	COM21,
	COM22,
	COM23,
	COM24,
	COM25,
	COM26,
	COM27,
	COM28,
	COM29,
	COM30,
	COM31,
	COM32,
	COM33,
	COM34,
	COM35,
	COM36,
	COM37,
	COM38,
	COM39,
	COM40,
} COMM_PORT;

// what type of client are you?
//
typedef enum {
	// monitor client-displays HART bus traffic
	CT_MONITOR = 1,
	// HART slave device
	CT_DEVICE
} CLIENTTYPE;

// data used to connect to XmtrLlc.dll
//
typedef struct {
	COMM_PORT ePort;				// input - you provide
	CLIENTTYPE eType;				// input - you provide

	DWORD dwClientNotifyEvent;	// input - you provide
	CClient* pClient;				// output - returned to you

} CONNECT_DATA, *P_CONNECT_DATA;

//
// END Client Data
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// DCB structure
//
// typedef struct _DCB { // dcb
// 	DWORD DCBlength;			// sizeof(DCB) 
// 	DWORD BaudRate;			// current baud rate 
// 	DWORD fBinary: 1;			// binary mode, no EOF check 
// 	DWORD fParity: 1;			// enable parity checking 
// 	DWORD fOutxCtsFlow:1;	// CTS output flow control 
// 	DWORD fOutxDsrFlow:1;	// DSR output flow control 
// 	DWORD fDtrControl:2;		// DTR flow control type 
// 	DWORD fDsrSensitivity:1;	// DSR sensitivity 
// 	DWORD fTXContinueOnXoff:1;	// XOFF continues Tx 
// 	DWORD fOutX: 1;			// XON/XOFF out flow control 
// 	DWORD fInX: 1;				// XON/XOFF in flow control 
// 	DWORD fErrorChar: 1;		// enable error replacement 
// 	DWORD fNull: 1;			// enable null stripping 
// 	DWORD fRtsControl:2;		// RTS flow control 
// 	DWORD fAbortOnError:1;	// abort reads/writes on error 
// 	DWORD fDummy2:17;			// reserved 
// 	WORD wReserved;			// not currently used 
// 	WORD XonLim;				// transmit XON threshold 
// 	WORD XoffLim;				// transmit XOFF threshold 
// 	BYTE ByteSize;				// number of bits/byte, 4-8 
// 	BYTE Parity;				// 0-4=no,odd,even,mark,space 
// 	BYTE StopBits;				// 0,1,2 = 1, 1.5, 2 
// 	char XonChar;				// Tx and Rx XON character 
// 	char XoffChar;				// Tx and Rx XOFF character 
// 	char ErrorChar;			// error replacement character 
// 	char EofChar;				// end of input character 
// 	char EvtChar;				// received event character 
// 	WORD wReserved1;			// reserved; do not use } DCB;
//
///////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_XMTR_H__2DE1EE41_B09E_11d2_A7DE_0000E8D6C3CA__INCLUDED_)
