/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: HartMsg.cpp $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/XmtrLlc/HartMsg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   HART Message
 ***************************************************************************/

/***************************************************************************
 * $History: HartMsg.cpp $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

//#include "stdafx.h"
#include "stdwinstuff.h"
#include <crtdbg.h>

#include "XmtrLlc.h"
#include "Hart.h"
//#include "Serial.h"
#include "PipeStuff.h"
#include "HartMsg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

///////////////////////////////////////////////////////////////////////////////
//
// CHartMsg
//
// default constructor usually for use with frame object
//
CHartMsg::CHartMsg()	// default constructor usually for use with frame object
{
	TRACE(_T("CHartMsg::CHartMsg().\n"));
	m_bComplete = false;
	m_eMsgStat = (E_MSG_STAT)SUCCESS;
	m_cCommStat = 0;
	m_cPreambles = 0;
	m_cDelimiter = 0;
	m_cAddr1 = 0;
	m_cDevType = 0;
	m_caDevID[0] = m_caDevID[1] = m_caDevID[2] = 0;
	m_caExpand[0] = m_caExpand[1] = m_caExpand[2] = 0;
	m_cCommand = 0;
	m_cByteCount = 0;
	m_caData[0] = 0;
	m_cBcc = 0;
    m_cDribble = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// ~CHartMsg
//
// default destructor
//
CHartMsg::~CHartMsg()
{
	TRACE(_T("CHartMsg::~CHartMsg.\n"));
}

///////////////////////////////////////////////////////////////////////////////
//
// CHartMsg
//
// Construct a long frame command
//
//CHartMsg::CHartMsg(E_FRAME_TYPE eType, TBYTE* pcLongAddr, TBYTE cCmd, TBYTE cBytes,
CHartMsg::CHartMsg(E_FRAME_TYPE eType, unsigned char* pcLongAddr, int cCmd, unsigned char cBytes,
						 unsigned char* pcBuf, unsigned char cMaster, bool bInBurstMode)
{
	TRACE(_T("CHartMsg::CHartMsg.\n"));
	InitMsg(5, eType, pcLongAddr, cCmd, cBytes, pcBuf, cMaster, bInBurstMode);
}

///////////////////////////////////////////////////////////////////////////////
//
//	CHartMsg
//
// Construct a short frame command
//
//CHartMsg::CHartMsg(E_FRAME_TYPE eType, TBYTE cShortAddr, TBYTE cCmd, TBYTE cBytes,
CHartMsg::CHartMsg(E_FRAME_TYPE eType, unsigned char cShortAddr, int cCmd, unsigned char cBytes,
						 unsigned char* pcBuf, unsigned char cMaster, bool bInBurstMode)
{
	TRACE(_T("CHartMsg::CHartMsg.\n"));
	unsigned char cLongAddr[5];
	unsigned char cCount;

	for (cCount = 0; cCount<5; cCount++) {
		cLongAddr[cCount] = 0;
	}
	cLongAddr[0] = cShortAddr;

	InitMsg(5, eType, cLongAddr, cCmd, cBytes, pcBuf, cMaster, bInBurstMode);
}

///////////////////////////////////////////////////////////////////////////////
//
//	CHartMsg
//
// = operator
//
CHartMsg& CHartMsg::operator=(CHartMsg& HartMsg)
{
	m_cPreambles = HartMsg.m_cPreambles;
	m_cDelimiter = HartMsg.m_cDelimiter;
	m_cAddr1 = HartMsg.m_cAddr1;
	m_cDevType = HartMsg.m_cDevType;
	memcpy(m_caDevID, HartMsg.m_caDevID, 3);
	memcpy(m_caExpand, HartMsg.m_caExpand, 3);
	m_cCommand = HartMsg.m_cCommand;
	m_cByteCount = HartMsg.m_cByteCount;
	memcpy(m_caData, HartMsg.m_caData, 255);
	m_cBcc = HartMsg.m_cBcc;
	m_bComplete = HartMsg.m_bComplete;
	m_eMsgStat = HartMsg.m_eMsgStat;
	m_cCommStat = HartMsg.m_cCommStat;
    m_cDribble = HartMsg.m_cDribble;
  	return(*this);
}

///////////////////////////////////////////////////////////////////////////////
//
// InitMsg
//
// Construct a long frame command
//
void CHartMsg::InitMsg(unsigned char cPreambles, E_FRAME_TYPE eType, unsigned char* pcLongAddr, int cCmd,// cmd from TBYTE 29jul05 stevev	
									unsigned char cBytes, unsigned char* pcBuf, unsigned char cMaster, bool bInBurstMode)
{
	TRACE(_T("CHartMsg::InitMsg.\n"));
	m_cPreambles = cPreambles;

	// the delimiter
	//
	m_cDelimiter = LONG_FRAME;
	FrameType(eType);

	// the address
	//
	SetLongAddress(pcLongAddr);
	InBurstMode(bInBurstMode);
	MasterAddress(cMaster);
	m_cCommand = cCmd;
	PutMsgBytes(cBytes, pcBuf);
	m_bComplete = true;
	m_eMsgStat = (E_MSG_STAT)SUCCESS;
	m_cCommStat = 0;
}

///////////////////////////////////////////////////////////////////////////////
//
// IsBroadcastAddress(void);
//
bool CHartMsg::IsBroadcastAddress(void)
{
	TRACE(_T("CHartMsg::IsBroadcastAddress.\n"));
	return (((m_cAddr1 & MFR_ADDR) | m_cDevType | m_caDevID[0] | m_caDevID[1] | m_caDevID[2]) ? false : true);
}

///////////////////////////////////////////////////////////////////////////////
//
// CompareShortAddress(TBYTE cPollAddr);
//
bool CHartMsg::CompareShortAddress(unsigned char cPollAddr)
{
	TRACE(_T("CHartMsg::CompareShortAddress.\n"));
	if ((m_cAddr1 ^ cPollAddr) & POLLING_ADDR)
	{
		return false;	// no address match
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//
// CompareLongAddress
//
bool CHartMsg::CompareLongAddress(unsigned char *pcAddr)
{
	TRACE(_T("CHartMsg::CompareLongAddress.\n"));
	if (((m_cAddr1 ^ *pcAddr++) & MFR_ADDR) ||
		(m_cDevType ^ *pcAddr++) ||
		(m_caDevID[0] ^ *pcAddr++) ||
		(m_caDevID[1] ^ *pcAddr++) ||
		(m_caDevID[2] ^ *pcAddr))
	{
		return false;	// no address match
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//
// CompareTag
//
bool CHartMsg::CompareTag(unsigned char cTag[6])
{
	TRACE(_T("CHartMsg::CompareTag.\n"));
	if ((m_caData[0] ^ cTag[0]) ||
		(m_caData[1] ^ cTag[1]) ||
		(m_caData[2] ^ cTag[2]) ||
		(m_caData[3] ^ cTag[3]) ||
		(m_caData[4] ^ cTag[4]) ||
		(m_caData[5] ^ cTag[5]))
	{
		return false;	// no address match
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//
// CompareLTag
//
bool CHartMsg::CompareLTag(unsigned char cLTag[32])
{
	TRACE(_T("CHartMsg::CompareLTag.\n"));
	if ((m_caData[0] ^ cLTag[0]) ||
		(m_caData[1] ^ cLTag[1]) ||
		(m_caData[2] ^ cLTag[2]) ||
		(m_caData[3] ^ cLTag[3]) ||
		(m_caData[4] ^ cLTag[4]) ||
		(m_caData[5] ^ cLTag[5]) ||
		(m_caData[6] ^ cLTag[6]) ||
		(m_caData[7] ^ cLTag[7]) ||
		(m_caData[8] ^ cLTag[8]) ||
		(m_caData[9] ^ cLTag[9]) ||
		(m_caData[10] ^ cLTag[10]) ||
		(m_caData[11] ^ cLTag[11]) ||
		(m_caData[12] ^ cLTag[12]) ||
		(m_caData[13] ^ cLTag[13]) ||
		(m_caData[14] ^ cLTag[14]) ||
		(m_caData[15] ^ cLTag[15]) ||
		(m_caData[16] ^ cLTag[16]) ||
		(m_caData[17] ^ cLTag[17]) ||
		(m_caData[18] ^ cLTag[18]) ||
		(m_caData[19] ^ cLTag[19]) ||
		(m_caData[20] ^ cLTag[20]) ||
		(m_caData[21] ^ cLTag[21]) ||
		(m_caData[22] ^ cLTag[22]) ||
		(m_caData[23] ^ cLTag[23]) ||
		(m_caData[24] ^ cLTag[24]) ||
		(m_caData[25] ^ cLTag[25]) ||
		(m_caData[26] ^ cLTag[26]) ||
		(m_caData[27] ^ cLTag[27]) ||
		(m_caData[28] ^ cLTag[28]) ||
		(m_caData[29] ^ cLTag[29]) ||
		(m_caData[30] ^ cLTag[30]) ||
		(m_caData[31] ^ cLTag[31]))
	{
		return false;	// no address match
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////
//
// SetLongAddress
//
void CHartMsg::SetLongAddress(unsigned char *pcAddr)
{
	TRACE(_T("CHartMsg::SetLongAddress.\n"));
	m_cAddr1  = (m_cAddr1 & ~MFR_ADDR) | (*pcAddr++ & MFR_ADDR);
	m_cDevType = *pcAddr++;
	m_caDevID[0] = *pcAddr++;
	m_caDevID[1] = *pcAddr++;
	m_caDevID[2] = *pcAddr;
}

///////////////////////////////////////////////////////////////////////////////
//
// GetMsgData
//
unsigned char* CHartMsg::GetMsgBytes(unsigned char cMaxBytes, unsigned char* pcBuf)
{
	TRACE(_T("CHartMsg::GetMsgBytes.\n"));
	return (unsigned char*) memcpy(pcBuf, m_caData, cMaxBytes);
}

///////////////////////////////////////////////////////////////////////////////
// PutMsgBytes
//
// fills the data field starting immediately after the byte count field
// assumes caller is handling response code and device status
//
void CHartMsg::PutMsgBytes(unsigned char cBytes, unsigned char* pcBuf)
{
	TRACE(_T("CHartMsg::PutMsgBytes.\n"));
	m_cByteCount = cBytes;
	memcpy(m_caData, pcBuf, cBytes);
}

///////////////////////////////////////////////////////////////////////////////
//
// MakeIntoReplyMsg
//
// Makes the current message into a reply:
// (1) turns around the frame type
// (2) writes the burst mode bit
// (3) writes the response code and device status
// (4) writes the rest of the data field
// (5) sets the response preamble length
//
// Sets the byte count in the reply message to nData + 2
//
// Returns
//   true			if sucessful
//   false			if failed (eg tried to turn a BACK around)
//
bool CHartMsg::MakeIntoReplyMsg(unsigned char cPreambles, unsigned char cResponseCode, unsigned char cDeviceStatus,
													 unsigned char cData, unsigned char* pcData)
{
	TRACE(_T("CHartMsg::MakeIntoReplyMsg(Long Form).\n"));
	m_cPreambles = cPreambles;
	TRACE(_T("CHartMsg::MakeIntoReplyMsg(): preambes %d.\n"), m_cPreambles);

	RCode(cResponseCode);
	TRACE(_T("CHartMsg::MakeIntoReplyMsg: response code 0x%0.2x.\n"), m_caData[0]);
	DevStat(cDeviceStatus);
	TRACE(_T("CHartMsg::MakeIntoReplyMsg: device status 0x%0.2x.\n"), m_caData[1]);

	return MakeIntoReplyMsg(cData, pcData);
}

bool CHartMsg::MakeIntoReplyMsg(unsigned char cData, unsigned char* pcData)
{
	TRACE(_T("CHartMsg::MakeIntoReplyMsg(Short Form).\n"));
	if (cData) {
		TRACE(_T("CHartMsg::MakeIntoReplyMsg(): copy %d bytes of data.\n"), cData);
		memcpy (&m_caData[2], pcData, cData);
	}
	m_cByteCount = cData + 2;

	switch (FrameType()) {
	case PSK_STX:
		TRACE(_T("CHartMsg::MakeIntoReplyMsg(): frame type PSK STX.\n"));
		FrameType(PSK_ACK);
		TRACE(_T("CHartMsg::MakeIntoReplyMsg(): delimiter -> 0x%0.2x.\n"), m_cDelimiter);
		break;

	case STX:
		TRACE(_T("CHartMsg::MakeIntoReplyMsg(): frame type STX.\n"));
		FrameType(ACK);
		TRACE(_T("CHartMsg::MakeIntoReplyMsg(): delimiter -> 0x%0.2x.\n"), m_cDelimiter);
		break;

	default:
		return false;
	}
	return true;
}

WORD CHartMsg::Cmd()
{
	WORD  expCmd;
	WORD  nonexpCmd;
	WORD  cmdNum;
	int offset;

	unsigned char Delimiter = m_cDelimiter &(0x7F);

	if (Delimiter == ACK || Delimiter == PSK_ACK || Delimiter == BACK)
		offset = 2; // Cmd 31 (slave response), status bytes are before data bytes
	else offset = 0;

	unsigned char ExpandCmd[2] = {m_caData[1 + offset],m_caData[0 + offset]}; // for windows, LSB first
	expCmd = *((WORD *)ExpandCmd);

	unsigned char noExpandCmd[2] = {m_cCommand,0x00}; // for windows, LSB first
	nonexpCmd = *((WORD *)noExpandCmd);

	//Check to see if the Host is using the extended commands (Command 31)?
	// and for command 31, the Byte count must be 2 for Master and 4 for Slave
	// and the extended command number is NOT in the forbidden range of 0 to 511.
	if (m_cCommand == 31  && m_cByteCount >= (2 + offset) && expCmd >= 512)
		cmdNum = expCmd;
	else cmdNum = nonexpCmd;

	return cmdNum;
}

void CHartMsg::clear(void)
{
	m_cPreambles =
	m_cDelimiter =
	m_cAddr1     =
	m_cDevType   =
	m_cCommand   =
	m_cByteCount =
	m_cBcc       =
    m_cDribble   =
    m_cCommStat  =  0;

	m_caDevID[0] = m_caDevID[1] = m_caDevID[2]  = 0;
	m_caExpand[0]= m_caExpand[1]= m_caExpand[2] = 0;

	m_bComplete = false;
	m_eMsgStat  = ESUCCESS;
}
