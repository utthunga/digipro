/*************************************************************************************************
 *
 * $Workfile: $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the Xmtr class to handle the pipe portion of the transaction (replaces serial)
 */
 

#ifndef _PIPESTUFF_H
#define _PIPESTUFF_H
#ifdef INC_DEBUG
#pragma message("In PipeStuff.h") 
#endif

// in lieu of stdafx.h
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include "NamedPipe.h"

#include <windows.h> 

//#include "ddbCommInfc.h"
//#include "ddbQueue.h"
#include "ddbGeneral.h"
#include "HARTsupport.h"
#include "ddbMutex.h"
#include "Hart.h"
#include "HartMsg.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::PipeStuff.h") 
#endif

//used to avoid all those ifdefs
class CSerial
{
public:
	COMM_DATA m_stCommPort;
	HRESULT   Write(DWORD* pdwWriteBytes)
	{ cerr << "ERROR:Serial write attempt at dummy port."<<endl;
	    TRACE(_T("ERROR:Serial write attempt at dummy port.\n")); 
	return -1;  };
};

#define PIPE_PORT   COM1
#define ONLY_CLIENT 0

#define PIPE_TIMEOUT   15000  /* mS (15 S) */

 
#define CONNECTING_STATE 0		/* no-op in nxtState */
#define IDLE_RD_STATE    1	/* reading an internal temorary next state */
#define READING_STATE    2	/* readingEx */
#define WRITING_STATE    3	/* writingEx */
#define ERROR_STATE      0xf0	/* forces disconnect/reconnect in nxtstate */
#define INSTANCES     1  /* only two masters are allowed in HART */
						/* reduce to one so we don't have to implement 2 */


typedef struct 
{ 
   OVERLAPPED oOverlap; 
   HANDLE     hPipeInst; 
   BYTE       chBuf[PIPEBUFFERSIZE]; 
   DWORD      cbToWrite; 
   DWORD      dwState; 
   DWORD      dwNxtState; 
} PIPEINST, *LPPIPEINST; 


#define XMTBUFFLEN	525 
// (255 data + cmd + dataLen)  X 2ascii/byte + 2 space delimiters
// pipe format: CC LL D1D2D3...  with all bytes represented as 2 ascii Hex value
//   CC is command, LL is data len, D1 is databyte 1 etc
// so command 3 with 4 data looks like '03 04 2D4EAABC' 

class CXmtrPipe
{
#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
	unsigned int dwThreadID;
#else
	DWORD  dwThreadID;
#endif
	HANDLE hThread;			// track that other thread
	HANDLE hDieEvent;		// tell the pipe handler that it's time to go home
	HANDLE hDeadEvent;		// handshake event for when the task is exiting
// make public  	HANDLE hSendEvent;		// set by the writer to tell us to send
//	HANDLE hDataChngEvent;	// let us know when someone wrote our data
		// class  memory - one per pipe
	PIPEINST Pipe[INSTANCES]; 
	HANDLE   hEvents[INSTANCES+2]; // one per instance + writeQ & the die event

	char   hXmitBuffer[XMTBUFFLEN];

	CClient* pClient; // our current client pointer

	// private prototypes
	VOID  DisconnectAndReconnect(DWORD); 
	DWORD ConnectToNewClient(HANDLE, LPOVERLAPPED); 
	//VOID  GetDataToWriteToClient(LPPIPEINST); 

//	int   pkt2Msg(hPkt* pPkt, BYTE* msgBuff);   // pkt to write to xmit byte string
//	DWORD sendPkt(int pipeNum, hPkt* pPkt2Wrt); // returns nxtState
	int   msg2str(int& strLen, CHartMsg* pMsg2Wrt);// returns SUCCESS||FAILURE, data in buffer
	int   rcv_Msg(int pipeNum);                 // zero == success, doesMsg2Pkt, Pkt to rcv
//	int   parseMsg2PktNinfo(BYTE* msgBuf, DWORD bufLen, hPkt* pPkt, cmdInfo_t* pCmdInfo);
	int   parseStr2Msg(int pipeNumber, BYTE* msgBuf, DWORD bufLen, CHartMsg* destMsg);


	hCmutex  pipeMutex;
	hCmutex::hCmutexCntrl* pMutexCtrl;

public:

	CXmtrPipe();
	~CXmtrPipe();
	int      initialize(void);
	void     pipeThread(void);
	//  handles the packet write function for the pipe < was a big section in the state machine >
	DWORD sendMsg(int pipeNum, CHartMsg* pMsg2Wrt) ;	// returns nxtState


	HANDLE hSendEvent;		// set by the writer to tell us to send
};


#endif // _PIPESTUFF_H
