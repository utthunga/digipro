/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: HartLlcMsg.cpp $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/XmtrLlc/HartLlcMsg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   HART LLC message
 ***************************************************************************/

/***************************************************************************
 * $History: HartLlcMsg.cpp $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

//#include "stdafx.h"
#include "stdwinstuff.h"
#include <crtdbg.h>

//#include "Hart.h"
//#include "Serial.h"
#include "XmtrLlc.h"

#include "HartLlcMsg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHartLlcMsg::CHartLlcMsg()
{
	TRACE(_T("CHartLlcMsg::CHartLlcMsg().\n"));
}

CHartLlcMsg::~CHartLlcMsg()
{
	TRACE(_T("CHartLlcMsg::~CHartLlcMsg().\n"));
}

///////////////////////////////////////////////////////////////////////////////
//
// XmitMsg
//
// This file contains the transmit state machine for the HART Link Layer
// Control.
//
unsigned char CHartLlcMsg::XmitMsg(CSerial* pSerial)
{
	TRACE(_T("CHartLlcMsg::XmitMsg.\n"));
	unsigned char *pcBuf;
	unsigned char cCount;
	unsigned char *pcChar;
	DWORD nBytes;
	unsigned char cBcc;
	// DWORD nDummy;

	////////////////////////////////////////////////////////////////////////////
	//
	// frame the message
	//
	// add preambles
	//
	pcBuf = pSerial->m_stCommPort.cWriteBuffer;
	memset(pcBuf, 0xFF, m_cPreambles);
	nBytes = m_cPreambles;
	pcBuf += nBytes;
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the preambles.\n"));

	// add delimiter, adjust checksum
	//
	*pcBuf++ = m_cDelimiter;
	nBytes++;
	cBcc = m_cDelimiter;		// start the BCC calc
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the delimiter.\n"));

	// add address, adjust checksum
	//
	*pcBuf++ = m_cAddr1;
	nBytes++;						// send the first address byte
	cBcc ^= m_cAddr1;			// continue the BCC calc
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the address 1st byte.\n"));

	// long or short frame?
	//
	if (m_cDelimiter & LONG_FRAME) {
		pcChar = &m_cDevType;
		for (cCount = 4; cCount ; cCount--) {
			cBcc ^= *pcChar;
			*pcBuf++ = *pcChar++;
			nBytes++;
		}
		TRACE(_T("CHartLlcMsg::XmitMsg: frame the long address.\n"));
	}

	// expansion bytes?
	//
	if (cCount = ((m_cDelimiter & EXPANDED) >> 5)) {
		pcChar = m_caExpand;
		for ( ; cCount ; cCount--) {
			cBcc ^= *pcChar;
			*pcBuf++ = *pcChar++;
			nBytes++;
		}
		TRACE(_T("CHartLlcMsg::XmitMsg: frame the expansion bytes.\n"));
	}

	// add command
	//
	*pcBuf++ = m_cCommand;
	nBytes++;		// send the command
	cBcc ^= m_cCommand;
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the command.\n"));

	// add bytecount
	//
	*pcBuf++ = cCount = m_cByteCount; nBytes++;	// send the byte count
	cBcc ^= m_cByteCount;
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the byte count.\n"));

	// add data and checksum
	//
	pcChar = m_caData;						// send the data field
	while (cCount) {
		cBcc ^= *pcChar;
		*pcBuf++ = *pcChar++; nBytes++;
		cCount--;
	}
	
	if (m_cBcc) // if Check sum Flag is set (Override)
		cBcc = m_cBcc; // use override value instead of calculated value
	
	*pcBuf++ = cBcc; nBytes++;			// finally the BCC
	
	
	TRACE(_T("CHartLlcMsg::XmitMsg: frame the checksum.\n"));

    TRACE(_T("CHartLlcMsg::XmitMsg: Num of dribble bytes = %d\n"),m_cDribble);
    for (int i = 0; i < m_cDribble; i++) // dribble bytes?
    {
        *pcBuf++ = 0xFE; 
        nBytes++;			
    }

 	// write the message
	//
	pSerial->Write(&nBytes);

	return 0;
}
