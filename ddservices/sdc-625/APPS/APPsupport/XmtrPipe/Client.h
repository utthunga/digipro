/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: Client.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/Include/Client.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   Client
 ***************************************************************************/

/***************************************************************************
 * $History: Client.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/Include
 * 
 ***************************************************************************/

#if !defined(AFX_CLIENT_H__8855FAAE_B1EC_11D2_97A4_003B50C10700__INCLUDED_)
#define AFX_CLIENT_H__8855FAAE_B1EC_11D2_97A4_003B50C10700__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Hart.h"
#ifdef _USEXMTR_DLL
#include "Serial.h"
#else
#include "PipeStuff.h"
#endif
#include "HartMsg.h"

class DLLCLASS_EXPORT CClient : public CObject
{
public:
	unsigned char m_acAddr[L_ADDR];
	unsigned char m_acLTag[LTAG_LENGTH];
	unsigned char m_acTag[TAG_LENGTH];
	unsigned char m_cPoll;
	DWORD m_dwClientNotifyEvent; // the message sent to  m_pClientWnd

	COMM_PORT  m_ePort;
	CLIENTTYPE m_eType;

	HANDLE     m_hLlcNotifyEvent;// the send m_pSlaveReply event 
	CHartMsg*  m_pHartMsg;

	CWnd*      m_pClientWnd;
#ifdef _USEXMTR_DLL
	CSerial*   m_pSerial;
#pragma message("Configured to use a serial port driver.")
#else /* use pipe */
	CXmtrPipe* m_pPipe;
#endif

	CHartMsg*  m_pSlaveReply;
	CHartMsg*  m_pNothing;

	CHartMsg   m_Msg;

	CClient();
	~CClient();
	HRESULT InitInstance(COMM_PORT ePort);
	CClient* GetClient() {
		return this;
	}
	bool memIsDLL;
};

#endif // !defined(AFX_CLIENT_H__8855FAAE_B1EC_11D2_97A4_003B50C10700__INCLUDED_)
