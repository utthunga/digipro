/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: XmtrLlc.h $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/Include/XmtrLlc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   XMTRLlc
 ***************************************************************************/

/***************************************************************************
 * $History: XmtrLlc.h $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/Include
 * 
 ***************************************************************************/

#include <afxtempl.h>       // MFC Templates

#ifndef _USEXMTR_DLL
 #include "Hart.h"
 #include "Client.h"
 #include "PipeStuff.h"
 #undef  DLLFUNC
 #define DLLFUNC EXTERN_C HRESULT 
 #define DLLCLASS_EXPORT  /* empty               no class export */
#endif
/*
#ifdef _U SEXMTR_DLL
//EXTERN_C     is  extern "C"
//AFX_EXT_API  is '__declspec(dllimport)'
#define DLLFUNC EXTERN_C AFX_EXT_API
// #define DLLFUNC EXTERN_C AFX_EXT_API HRESULT CALLBACK
#else // use a piped library
#define DLLFUNC EXTERN_C
#endif
*/
///////////////////////////////////////////////////////////////////////////////
// Exported Functions
//

// Called to connect a client to XmtrLlc.dll
//    call from doc's constructor or new document handler
//
DLLFUNC HSlaveConnect(P_CONNECT_DATA pstConnectData);

// Called to configure a client window to send messages to
//    call from doc's constructor or new document handler
//
DLLFUNC HSlaveConfigWnd(CClient* pClient, CWnd* pWnd);

// Called to register a client's polling addrress
//    call from doc's constructor or new document handler
//    after calling HSlaveConnect()
//
DLLFUNC HSlaveConfigPoll(CClient* pClient, unsigned char& cPoll);// 22jun10 from::>TBYTE& cPoll);

// Called to register a client's unique ID
//    call from doc's constructor or new document handler
//    after calling HSlaveConnect()
//
DLLFUNC HSlaveConfigUid(CClient* pClient, unsigned char acAddr[5]);// 22jun10 from::>TBYTE acAddr[5]);

// Called to register a client's TAG or long TAG
//    call from doc's constructor or new document handler
//    after calling HSlaveConnect()
//
DLLFUNC HSlaveConfigTag(CClient* pClient, TCHAR* szTag);
DLLFUNC HSlaveConfigLongTag(CClient* pClient, TCHAR* szLongTag);

// Called to disconnect a client from XmtrLlc.dll
//    call from doc's destructor
//
DLLFUNC HSlaveDisconnect(CClient* pClient);

// Returns the current client count for the specified port
//
DLLFUNC GetClientCount(COMM_PORT ePort);

///////////////////////////////////////////////////////////////////////////////
//
// Internal-not available for client use
//
#ifndef _USEXMTR_DLL
#define CSERIAL CSerialPipe

// 22jun10 from::>CArray<TBYTE, TBYTE&>* GetClientPollArray();
CArray<unsigned char, unsigned char&>* GetClientPollArray();
CTypedPtrMap<CMapPtrToWord, CClient*, WORD>* GetClientTypeMap();
CArray<CString, CString&>* GetClientUidArray();
CTypedPtrMap<CMapWordToPtr, WORD, CClient*>* GetPollClientMap();
CTypedPtrMap<CMapPtrToWord, CSerial *, WORD>* GetSerialPortMap();
CTypedPtrMap<CMapWordToPtr, WORD, CClient*>* GetUidClientMap();
int DecrementClientCount(COMM_PORT ePort, CClient* pClient);
int IncrementClientCount(COMM_PORT ePort, CClient* pClient);
CClient* GetClientByPort(COMM_PORT ePort, int nIndex);
#endif
///////////////////////////////////////////////////////////////////////////////
