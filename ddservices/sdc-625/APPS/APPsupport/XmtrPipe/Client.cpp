/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: Client.cpp $
 * $Archive: /HCF Tools/XMTR-MOD/Xmtr/XmtrLlc/Client.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   Client
 ***************************************************************************/

/***************************************************************************
 * $History: Client.cpp $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

//#include "stdafx.h"
#include "stdwinstuff.h"
#include <crtdbg.h>

#include "Hart.h"
#include "XmtrLlc.h"
//#include "Serial.h"
#include "Client.h"

// #ifdef _DEBUG
// #define new DEBUG_NEW
// #undef THIS_FILE
// static char THIS_FILE[] = __FILE__;
// #endif

// Map for serial object <-> serial port
//
CTypedPtrMap<CMapPtrToWord, CSerial*, WORD>* pSerialPortMap;

///////////////////////////////////////////////////////////////////////////////
//
// Constructor/Destructor
//
CClient::CClient() : m_cPoll(0),
							m_dwClientNotifyEvent(NULL),
							m_ePort(COM1),
							m_eType(CT_MONITOR),
							m_hLlcNotifyEvent(NULL),
							m_pClientWnd(NULL),
							m_pHartMsg(&m_Msg),
#ifdef _USEXMTR_DLL
							m_pSerial(NULL),
#else /* use pipe */
							m_pPipe(NULL),
#endif
							m_pSlaveReply(NULL)
							,m_pNothing(NULL)
{
	TRACE(_T("CClient::CClient.\n"));
	ZeroMemory(m_acAddr, sizeof(m_acAddr));
	ZeroMemory(m_acLTag, sizeof(m_acLTag));
	ZeroMemory(m_acTag, sizeof(m_acTag));
}

CClient::~CClient()
{
	TRACE(_T("CClient::~CClient.\n"));
	pSerialPortMap = GetSerialPortMap();
	_ASSERTE(pSerialPortMap != NULL);

	TRACE(_T("~CClient: adjust client count.\n"));
	DecrementClientCount(m_ePort, this);

#ifdef _USEXMTR_DLL
		// if no more clients, terminate the port monitor
		//
		if (!GetClientCount(m_ePort)) {
			TRACE(_T("~CClient: terminate the port monitor.\n"));
			SetEvent(m_pSerial->m_hTerminateEvent);
		}

		TRACE(_T("~CClient: find serial-port map.\n"));
		if (pSerialPortMap->Lookup(m_pSerial, (WORD&) m_ePort)) {
			TRACE(_T("~CClient: remove serial-port map entries.\n"));
			pSerialPortMap->RemoveKey(m_pSerial);
		}

		if (!GetClientCount(m_ePort)) {
			TRACE(_T("~CClient: wait for port monitor to terminate.\n"));
			WaitForSingleObject(m_pSerial->m_hNotifyEvent, 5 * STALL_TIME);
		}

		// delete the serial object
		//
		TRACE(_T("~CClient: delete serial object.\n"));
		delete (m_pSerial);
#else /* use pipe */
		// only a single client at this time, destructor kills the thread
		delete m_pPipe; m_pPipe = NULL;
#endif
}

///////////////////////////////////////////////////////////////////////////////
//
// InitInstance
//
// Create a serial object on a specific port
//
HRESULT CClient::InitInstance(COMM_PORT ePort)
{
	TRACE(_T("CClient::InitInstance.\n"));
	HRESULT hr = E_FAIL;
	pSerialPortMap = GetSerialPortMap();
	_ASSERTE(pSerialPortMap != NULL);


	// are there any clients on this port
	//
	int nClients = GetClientCount(m_ePort);
	if (!nClients) {

		// no, then increment the client count for this port,
		//    add the client object to the list
		//
		TRACE(_T("InitInstance: bump client count, capture client.\n"));
		IncrementClientCount(m_ePort, this);

#ifdef _USEXMTR_DLL
			// and create the serial object
			//
			m_pSerial = new CSerial;
			_ASSERTE(m_pSerial != NULL);
			m_pSerial->m_ePort = m_ePort;

			// now initialize it
			//
			hr = m_pSerial->InitInstance();
			if (SUCCEEDED(hr)) {
				m_hLlcNotifyEvent = m_pSerial->m_hNotifyEvent;
				pSerialPortMap->SetAt(m_pSerial, (WORD&) m_ePort);
				hr = S_OK;
			}
			else // 'else' code added by POB, 9/2/99
			{	//Prevent Windows asserts and memory leaks
				TRACE(_T("InitInstance: adjust client count, serial port failed.\n"));
				DecrementClientCount(m_ePort, this);
				// delete the serial object
				//
				TRACE(_T("InitInstance: delete serial object.\n"));
				delete (m_pSerial);
			}
#else // use pipe class
			m_pPipe = new CXmtrPipe; 
			_ASSERTE(m_pPipe != NULL);

			if (m_pPipe->initialize() != SUCCESS )// starts the thread...
			{// thread didn't startup 
				TRACE(_T("InitInstance: adjust client count, pipe startup failed.\n"));
				DecrementClientCount(m_ePort, this);
				// delete the pipe object
				//
				TRACE(_T("InitInstance: delete pipe object.\n"));
				delete (m_pPipe);
				hr = -1;
			}
			else
			{
				hr = S_OK;
			}
#endif
	}
	else // we already have client(s)
	{
		bool bNotDone = true;
		CSerial* pSerial;
		POSITION stPos = pSerialPortMap->GetStartPosition();
		WORD wPort;

		// yes, so increment the client count for this port
		//
		TRACE(_T("InitInstance: increment the client count.\n"));
		IncrementClientCount(m_ePort, this);

			// find the serial object for this port
			//
			while (bNotDone) {
				pSerialPortMap->GetNextAssoc(stPos, pSerial, wPort);
				if (m_ePort == (COMM_PORT) wPort)
					bNotDone = false;
			}

			// found him?
			//
			if (bNotDone)
				return(hr = E_FAIL);
#ifdef _USEXMTR_DLL
			m_pSerial = pSerial;
			m_pSerial->m_ePort = (COMM_PORT) wPort;
			// no-op at else
#endif
	}
	return(hr);
}
