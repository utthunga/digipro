
// windows interface
// stdwinstuff.h


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if _MSC_VER >= 1300
#define  WINVER      0x0500
#define _WIN32_WINNT 0x0500
//  0x0500 targets the Windows 2000 operating system. Other valid values include:
//  0x0501 for Windows XP, 
//  0x0502 for Windows Server 2003, 
//  0x0600 for Windows Vista, and 
//  0x0601 for Windows 7.
//
//   Compiler                           _MSC_VER value
//   --------                           --------------
// C Compiler version 6.0                  600
//   C/C++ compiler version 7.0              700
//   Visual C++, Windows, version 1.0        800
//   Visual C++, 32-bit, version 1.0         800
//   Visual C++, Windows, version 2.0        900
//   Visual C++, 32-bit, version 2.x         900
//   Visual C++, 32-bit, version 4.0         1000
//   Visual C++, 32-bit, version 5.0         1100
//   Visual C++, 32-bit, version 6.0         1200
//   Microsoft Visual C++ .NET 2003          1310 
//   Visual C++ 2005                         1400

#endif


#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#ifdef _RLSTRACE
#ifndef _DEBUG
	#include <fstream.h>
	#include <iostream.h>
	#define CMSTRINGDEF
	class CMString : CString
	{
	public:
		ofstream* fb;
		CMString();
		~CMString();
		void trace(LPCTSTR lpszFormat, ...);
	};

	extern CMString mytrace;
	#undef TRACE
	#define TRACE mytrace.trace
	//#endif

#endif	// _DEBUG
#endif	// _RLSTRACE

