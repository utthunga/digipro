/***************************************************************************
 * SourceSafe Header
 *
 * $Workfile: XmtrLlc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 ***************************************************************************/

/***************************************************************************
 * Copyright 2003, Hart Communication Foundation
 ***************************************************************************/

/***************************************************************************
 * File Description:
 *   XmtrLlc.cpp : Defines the initialization routines for the DLL.
 ***************************************************************************/

/***************************************************************************
 * $History: XmtrLlc.cpp $
 * 
 * *****************  Version 1  *****************
 * User: Sean         Date: 3/04/03    Time: 3:29p
 * Created in $/HCF Tools/XMTR-MOD/Xmtr/XmtrLlc
 * 
 ***************************************************************************/

//#include "stdafx.h"
#include "stdwinstuff.h"

#include <afxtempl.h>
#include <crtdbg.h>

#include "Hart.h"
#include "XmtrLlc.h"
//#include "Serial.h"
#include "HartLlcMsg.h"
#include "Client.h" // will include serial if needed

#include "PipeStuff.h"

/* * * * * * * * * * * * * * * * * * * * *
 MODIFIED TO HANDLE PIPES - NOT SERIAL PORTS
 * * * * * * * * * * * * * * * * * * * * */

// #ifdef _DEBUG
// #define new DEBUG_NEW
// #undef THIS_FILE
// static char THIS_FILE[] = __FILE__;
// #endif

///////////////////////////////////////////////////////////////////////////////
//
// Global Data
//

// Instance handle received from LibMain
//
// HINSTANCE ghInst;

// Map for client <-> serial port
//
static CTypedPtrMap<CMapPtrToWord, CClient*, WORD> gClientPortMap;

// Map for serial object <-> serial port
//
static CTypedPtrMap<CMapPtrToWord, CSerial*, WORD> gSerialPortMap;

// Clients by port, client count
//
static CClient* g_aClients[MAX_PORT][MAX_CLIENTS];
static int g_nClients[MAX_PORT];

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConnect
//
// Register a slave/monitor device
//
DLLFUNC HSlaveConnect(P_CONNECT_DATA pstConnectData)
{
	LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect.\n");
	HRESULT hr = E_FAIL;

	pstConnectData->pClient = NULL;
	if (pstConnectData->ePort < MAX_PORT && pstConnectData->ePort >= 0) {

		// create a new client object
		//
		CClient* pClient = new CClient;
		_ASSERTE(pClient != NULL);
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: client 0x%0.8x.\n", (int) pClient);

		// save the port
		//
		pClient->m_ePort = pstConnectData->ePort;
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: save client port %d.\n", pClient->m_ePort);

		// save the client type
		//
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: client type %d.\n", (WORD) pstConnectData->eType);
		pClient->m_eType = pstConnectData->eType;

		// map the client id <-> port
		//
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: save in client port map.\n");
		gClientPortMap.SetAt(pClient, pstConnectData->ePort);

		// save client notify message (sent to window)
		//
		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: save client notify event.\n");
		pClient->m_dwClientNotifyEvent = pstConnectData->dwClientNotifyEvent;

		// the pointer to the client
		//
		pstConnectData->pClient = pClient;
 		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: returned client 0x%0.8x.\n", pstConnectData->pClient);

		LOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"*+ HSlaveConnect: initialize the client.\n");
		hr = pClient->InitInstance(pClient->m_ePort);

	}
	return (hr);
}



///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConfigWnd
//
// Specify the window to send client messages to
//
DLLFUNC HSlaveConfigWnd(CClient* pClient, CWnd* pWnd)
{
	TRACE(_T("HSlaveConfigWnd.\n"));
	HRESULT hr = E_FAIL;
	WORD wPort;
	if (!pWnd)
		return (hr);
	if (gClientPortMap.Lookup(pClient, wPort)) {
		pClient->m_pClientWnd = pWnd;
		TRACE(_T("HSlaveConfigWnd: client window 0x%0.8x.\n"), pWnd);
		hr = S_OK;
	}
	return (hr = S_OK);
}

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConfigPoll
//
// Capture a slave/monitor device polling address
//
DLLFUNC HSlaveConfigPoll(CClient* pClient, unsigned char& cPoll) // 22jun10 from::> TBYTE& cPoll)
{
	TRACE(_T("HSlaveConfigPoll: client 0x%0.8x, poll addr. 0x%0.2x.\n"),
			pClient, cPoll);
	HRESULT hr = E_FAIL;
	WORD wPort;
	if (gClientPortMap.Lookup(pClient, wPort)) {
		pClient->m_cPoll = cPoll;
		TRACE(_T("HSlaveConfigPoll: poll address %0.2x.\n"), pClient->m_cPoll);
		hr = S_OK;
	}
	return (hr);
}

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConfigUid
//
// Capture a slave/monitor device unique address
//
DLLFUNC HSlaveConfigUid(CClient *pClient, unsigned char acAddr[L_ADDR])// 22jun10 from::>TBYTE acAddr[L_ADDR])
{
	acAddr[0] &= MFR_ADDR; // mask off bits 6-7
	TRACE(_T("HSlaveConfigUid: client 0x%0.8x, uniq addr. %0.2x %0.2x %0.2x %0.2x %0.2x.\n"),
			pClient, acAddr[0], acAddr[1], acAddr[2], acAddr[3], acAddr[4]);
	HRESULT hr = E_FAIL;
	WORD    wPort;
	if (gClientPortMap.Lookup(pClient, wPort)) {
		CopyMemory(pClient->m_acAddr, acAddr, L_ADDR);
		TRACE(_T("HSlaveConfigUid: unique id %0.2x %0.2x %0.2x %0.2x %0.2x.\n"),
				pClient->m_acAddr[0], pClient->m_acAddr[1], pClient->m_acAddr[2],
				pClient->m_acAddr[3], pClient->m_acAddr[4]);
		hr = S_OK;
	}
	return (hr);
}

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConfigTag
//
// Capture a slave/monitor device TAG
//
DLLFUNC HSlaveConfigTag(CClient* pClient, TCHAR* pszTag)
{
	TCHAR* pTag = pszTag;
	TRACE(_T("HSlaveConfigTag: client 0x%0.8x, TAG %0.2x %0.2x %0.2x %0.2x %0.2x %0.2x.\n"),
			pClient, (*pTag)++, (*pTag)++, (*pTag)++, (*pTag)++, (*pTag)++, *pTag);
	HRESULT hr = E_FAIL;
	WORD    wPort;
	if (gClientPortMap.Lookup(pClient, wPort)) {
		CopyMemory(pClient->m_acTag, pszTag, TAG_LENGTH);
		TRACE(_T("HSlaveConfigTag: TAG %0.2x %0.2x %0.2x %0.2x %0.2x %0.2x.\n"),
				pClient->m_acTag[0], pClient->m_acTag[1], pClient->m_acTag[2],
				pClient->m_acTag[3], pClient->m_acTag[4], pClient->m_acTag[5]);
		hr = S_OK;
	}
	return (hr);
}

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveConfigLongTag
//
// Capture a slave/monitor device long TAG
//
DLLFUNC HSlaveConfigLongTag(CClient* pClient, TCHAR* pszLongTag)
{
	TRACE(_T("HSlaveConfigLongTag: client 0x%0.8x, TAG. %s.\n"), pClient, pszLongTag);
	HRESULT hr = E_FAIL;
	WORD    wPort;
	if (gClientPortMap.Lookup(pClient, wPort)) {
		CopyMemory(pClient->m_acLTag, pszLongTag, LTAG_LENGTH);
		hr = S_OK;
		TRACE(_T("HSlaveConfigLongTag: long TAG %s.\n"), pClient->m_acLTag);
	}

	return (hr);
}

///////////////////////////////////////////////////////////////////////////////
//
// HSlaveDisconnect
//
// Un-register a slave/monitor device
//
DLLFUNC HSlaveDisconnect(CClient* pClient)
{
	TRACE(_T("HSlaveDisconnect.\n"));
	HRESULT hr = E_FAIL;

	_ASSERTE(pClient != NULL);

	// Now remove entry from client port map and delete the client
	//
	TRACE(_T("HSlaveDisconnect: delete from client port map.\n"));
	gClientPortMap.RemoveKey(pClient);

	TRACE(_T("HSlaveDisconnect: delete client.\n"));
	delete (pClient);

	hr = S_OK;
	return (hr);
}

///////////////////////////////////////////////////////////////////////////////
//
// GetSerialPortMap
//
// Return a pointer to the serial object serial port map
//
CTypedPtrMap<CMapPtrToWord, CSerial*, WORD>* GetSerialPortMap()
{
	TRACE(_T("GetSerialPortMap.\n"));
	return(&gSerialPortMap);
}

///////////////////////////////////////////////////////////////////////////////
//
// GetClientCount
//
// Return the client count for port ePort
//
DLLFUNC GetClientCount(COMM_PORT ePort)
{
	TRACE(_T("GetClientCount: port %d, %d client(s).\n"),
			ePort, g_nClients[ePort]);
	return(g_nClients[ePort]);
}

///////////////////////////////////////////////////////////////////////////////
//
// IncrementClientCount
//
// Increment the client count for port ePort, save the client object
//
int IncrementClientCount(COMM_PORT ePort, CClient* pClient)
{
	if (g_nClients[ePort] == MAX_CLIENTS)
		return -1;
	int nCount = (g_nClients[ePort]++);
	g_aClients[ePort][nCount] = pClient;
	TRACE(_T("IncrementClientCount: port %d, %d clients.\n"),
			ePort, g_nClients[ePort]);
	return(g_nClients[ePort]);
}

///////////////////////////////////////////////////////////////////////////////
//
// DecrementClientCount
//
// Decrement the client count for port ePort, remove the client object
//
int DecrementClientCount(COMM_PORT ePort, CClient* pClient)
{
	int nCount;

	if (g_nClients[ePort] == 0)
		return -1;
	for (nCount = 0; nCount < MAX_CLIENTS; nCount++) {
		if (g_aClients[ePort][nCount] == pClient) {
			g_aClients[ePort][nCount] = NULL;
			break;
		}
	}
	g_nClients[ePort] -= 1;
	TRACE(_T("DecrementClientCount: port %d, %d clients.\n"),
			ePort, g_nClients[ePort]);
	return(g_nClients[ePort]);
}

///////////////////////////////////////////////////////////////////////////////
//
// GetClientByPort
//
// Get a client object for port ePort at index nIndex
//
CClient* GetClientByPort(COMM_PORT ePort, int nIndex)
{
//	TRACE(_T("GetClientByPort: port %d, index %d.\n"), ePort, nIndex);
	return(g_aClients[ePort][nIndex]);
}
