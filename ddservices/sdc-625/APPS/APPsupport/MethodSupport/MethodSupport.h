#ifndef METHODSUPPORT_H
#define METHODSUPPORT_H

#pragma warning (disable : 4786)

using namespace std;

class SDC625Controller;

#include "ddbMethSupportInfc.h"
#include "ddbMethod.h"
#include "SDC625Controller.h"

class MethodSupport : public hCMethSupport
{
private:
    int   clientCount;
public:

    MethodSupport();
    ~MethodSupport();

    void Initialize(SDC625Controller* sdc);

    void PreExecute(hCmethodCall& mc);
    void PostExecute(hCmethodCall& mc);

    void PreExecute();
    void PostExecute();

    //function that calls the method execute function
    RETURNCODE DoMethodSupportExecute(hCmethodCall& MethCall);

    bool hasClients(void){ return (clientCount != 0); };

    // handles the display of methods UI
    bool MethodDisplay(ACTION_UI_DATA structMethodsUIData,
                           ACTION_USER_INPUT_DATA& structUserInputData);

    // returns the number of methods currently running
    int  numberMethodsRunning();

   /* image index, UI looks up image, converts pixels X pixels
       to rows X cols & returns*/
    bool UIimageSize(int imgIndex, unsigned& xCols,
                     unsigned& yRows, bool isInline);
    /* string ptr, UI extracts the string(it knows what lang),
       converts lines X chars then to rows X cols & returns */

    bool UIstringSize(string* c_str,unsigned& xCols,
                      unsigned& yRows, int heightLimit, bool isDialog = false);

    virtual RETURNCODE DoMethodSupportExecuteForMS(hCmethodCall& MethCall,
                                     hCddbDevice *pCurDev);
    virtual void PreExecuteForMS(hCmethodCall& mc,
                                 hCddbDevice *pCurDev);

private:

   SDC625Controller* m_pSDController;

};

#endif // METHODSUPPORT_H
