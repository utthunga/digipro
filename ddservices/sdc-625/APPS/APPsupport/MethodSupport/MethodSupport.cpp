#include "MethodSupport.h"
#include "ddbDevice.h"
#include "MEE.h"
#include "AdaptorDef.h"
#include "flk_log4cplus/Log4cplusAdapter.h"
#include "flk_log4cplus_defs.h"

MethodSupport::MethodSupport()
{
    bEnableDynamicDisplay = false;
    m_pSDController = nullptr;

}

MethodSupport::~MethodSupport()
{

}

void MethodSupport::Initialize(SDC625Controller* sdc)
{
    m_pSDController = sdc;
}

#ifdef _DEBUG
void MethodSupport::PreExecute(hCmethodCall& mc)
#else
void MethodSupport::PreExecute()
#endif
{

}

RETURNCODE MethodSupport::DoMethodSupportExecute(hCmethodCall& methCall)
{
    bool bReturnSuccess = false;
    int retVal = SUCCESS;
    string label, help;

    if (nullptr == m_pSDController)
    {
        LOGF_ERROR(CPMAPP,"m_pSDController instance is null!.");
        return false;
    }

    hCmethodCall *pThisMethCall = &methCall;
    hCddbDevice* pDev = m_pSDController->getCurrDevice();

    if( nullptr == pDev ||  nullptr == methCall.m_pMeth )
    {
        return FAILURE;
    }

    methCall.m_pMeth->Label(label);
    methCall.m_pMeth->Help(help);

    SDC625Controller::s_hartMEData.methodID = methCall.m_pMeth->getID();
    SDC625Controller::s_hartMEData.methodName = label;
    SDC625Controller::s_hartMEData.methodHelp = help;


    if(pThisMethCall->source == msrc_ACTION ||
                pThisMethCall->source == msrc_CMD_ACT)
        {
            if (pThisMethCall->paramList.size() != 1 )
            {
                LOGF_ERROR(CPMAPP,"Call to execute action not executed because "
                               "the item ID of the concerned variable is not provided.\n");
                retVal = APP_PARAMETER_ERR;
            }
            else
            {
            	/*
            	 * <Adesh>: upcall and downcall is handled here,not in adaptor. Two get
            	 * the status of each edit method.
            	 */

                if (pThisMethCall->source != msrc_CMD_ACT)
                {
                    m_pSDController->methodStartUpcall();
                }

                pThisMethCall->m_MethState = mc_Running;
                bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,pThisMethCall->methodID,
                                               pThisMethCall->paramList[0].vValue.varSymbolID);

                if (pThisMethCall->source != msrc_CMD_ACT)
                {
                     m_pSDController->methodCompleteUpcall();
                }
            }
        }
    else
	{
    	retVal = APP_TYPE_UNKNOWN;
		return FAILURE;
	}

    if (false == bReturnSuccess)
    {
        retVal = FAILURE;
    }

    return retVal;
}

#ifdef _DEBUG
void MethodSupport::PostExecute(hCmethodCall& mc)
#else
void MethodSupport::PostExecute()
#endif
{
    // does nothing for now
}

int  MethodSupport::numberMethodsRunning()
{
    // does nothing for now
    return 0;
}

bool MethodSupport::MethodDisplay(ACTION_UI_DATA structMethodsUIData,
                                  ACTION_USER_INPUT_DATA& structUserInputData)
{
    int retVal = SUCCESS;

    if (nullptr == m_pSDController)
    {
        LOGF_ERROR(CPMAPP,"m_pSDController instance is null!.");
        return false;
    }
    hCddbDevice* pDev = m_pSDController->getCurrDevice();
    // Send method aborted signal to UI
    if(structMethodsUIData.bMethodAbortedSignalToUI == true)
    {
    	pDev->pMEE->latestMethodStatus = me_Aborted;
    }

    if( 0 == structMethodsUIData.userInterfaceDataType)
    {
         m_pSDController->methodErrorUpcall(structMethodsUIData);
    }
    else
    {
        switch(structMethodsUIData.userInterfaceDataType)
        {
        case TEXT_MESSAGE:
        {
            if (structMethodsUIData.bUserAcknowledge)
            {
                retVal = m_pSDController->displayAckUpcall(structMethodsUIData);
            }
            else if (0 != structMethodsUIData.uDelayTime)
            {
                retVal = m_pSDController->displayDelayUpcall(structMethodsUIData);
            }
        }
        break;
        case COMBO:
        {
            retVal = m_pSDController->displayMenuUpcall(structMethodsUIData,
                                                            structUserInputData);
        }
        break;
        case EDIT:
        case HARTDATE:
        case TIME:
            {
                retVal = m_pSDController->displayGetUpcall(structMethodsUIData,
                                                           structUserInputData);
            }
            break;
        case VARIABLES_CHANGED_MSG:
            {
                //TODO (Bhuvana) : yet to implement
            }
            break;
        case MENU:
            {
                //TODO (Bhuvana) : yet to implement
            }
            break;
        case PLOT:
        default:
            //TODO (Bhuvana) : send error up call for not supported type
            break;
        }
    }

    bEnableDynamicDisplay = false;

    if(SDC625Controller::s_hartMEData.isAborted)
    {
        return false;
    }

    return true;
}

bool MethodSupport::UIimageSize(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline)
{
     //TODO (BHUVANA) : Yet to implement
    return false;
}

bool MethodSupport::
    UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit,bool isDialog)
{
    // does nothing for now
    return false;
}

void MethodSupport::PreExecuteForMS(hCmethodCall& mc, hCddbDevice *pCurDev)
{
     // does nothing for now
}

RETURNCODE MethodSupport::DoMethodSupportExecuteForMS(hCmethodCall& methCall, hCddbDevice *pCurDev)
{
    bool bReturnSuccess = false;
    RETURNCODE    rc   = SUCCESS;

    hCmethodCall *pThisMethCall = &methCall;
    hCddbDevice* pDev = pCurDev;

    if (nullptr == pDev || nullptr == methCall.m_pMeth)
    {
        return FAILURE;
    }

    if(pThisMethCall->source == msrc_ACTION ||
            pThisMethCall->source == msrc_CMD_ACT)
    {
        if (pThisMethCall->paramList.size() != 1 )
        {
            LOGF_ERROR(CPMAPP,"Call to execute action not executed because "
                           "the item ID of the concerned variable is not provided.\n");
            rc = APP_PARAMETER_ERR;
        }
        else
        {
            pThisMethCall->m_MethState = mc_Running;
            bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,pThisMethCall->methodID,
                                           pThisMethCall->paramList[0].vValue.varSymbolID);
        }
    }
    else if (pThisMethCall->source == msrc_EXTERN)
    {
        // Sends method startup call
        m_pSDController->methodStartUpcall();

        pThisMethCall->m_MethState = mc_Running;

        // Executes the user selected method
        bReturnSuccess =
                        pDev->pMEE->ExecuteMethod(pDev,
                          pThisMethCall->methodID);

        // Sends method complete upcall for successful method execution
        m_pSDController->methodCompleteUpcall();
    }
    else
    {
        rc = APP_TYPE_UNKNOWN;
        return FAILURE;
    }

    return SUCCESS;
}
