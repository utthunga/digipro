#ifndef _COUTAVOID_H
#define _COUTAVOID_H

#include <string>
using namespace std;

#define endl NULL
#define hex NULL
#define dec NULL
#define setw(n)	n

class COUT
{

public:
	COUT operator<<(char *str)
	{
		return *this;
	}

	COUT operator<<(string str)
	{
		return *this;
	}

	COUT operator<<(string *str)
	{
		return *this;
	}

	COUT operator<<(const char *str)
	{
		return *this;
	}

	COUT operator<<(int x)
	{
		return *this;
	}

	COUT operator<<(unsigned int x)
	{
		return *this;
	}

	COUT operator<<(unsigned long x)
	{
		return *this;
	}

	COUT operator<<(unsigned short x)
	{
		return *this;
	}

	COUT operator<<(unsigned char x)
	{
		return *this;
	}

	COUT operator<<(long x)
	{
		return *this;
	}

	COUT operator<<(short x)
	{
		return *this;
	}

	COUT operator<<(char x)
	{
		return *this;
	}

	COUT operator<<(float x)
	{
		return *this;
	}

	COUT operator<<(void *x)
	{
		return *this;
	}

	void flush()
	{
	}

};

class CERR
{

public:
	CERR operator<<(char *str)
	{
		return *this;
	}

	CERR operator<<(string str)
	{
		return *this;
	}

	CERR operator<<(string *str)
	{
		return *this;
	}

	CERR operator<<(const char *str)
	{
		return *this;
	}

	CERR operator<<(int x)
	{
		return *this;
	}

	CERR operator<<(unsigned int x)
	{
		return *this;
	}

	CERR operator<<(unsigned long x)
	{
		return *this;
	}

	CERR operator<<(unsigned short x)
	{
		return *this;
	}

	CERR operator<<(unsigned char x)
	{
		return *this;
	}

	CERR operator<<(long x)
	{
		return *this;
	}

	CERR operator<<(short x)
	{
		return *this;
	}

	CERR operator<<(char x)
	{
		return *this;
	}


	CERR operator<<(float x)
	{
		return *this;
	}

	CERR operator<<(void *x)
	{
		return *this;
	}

	void flush()
	{
	}

};

class CLOG
{

public:
	CLOG operator<<(char *str)
	{
		return *this;
	}

	CLOG operator<<(string str)
	{
		return *this;
	}

	CLOG operator<<(string *str)
	{
		return *this;
	}

	CLOG operator<<(const char *str)
	{
		return *this;
	}

	CLOG operator<<(int x)
	{
		return *this;
	}

	CLOG operator<<(unsigned int x)
	{
		return *this;
	}

	CLOG operator<<(unsigned long x)
	{
		return *this;
	}

	CLOG operator<<(unsigned short x)
	{
		return *this;
	}

	CLOG operator<<(unsigned char x)
	{
		return *this;
	}

	CLOG operator<<(long x)
	{
		return *this;
	}

	CLOG operator<<(short x)
	{
		return *this;
	}

	CLOG operator<<(char x)
	{
		return *this;
	}

	CLOG operator<<(float x)
	{
		return *this;
	}

	CLOG operator<<(void *x)
	{
		return *this;
	}

	void flush()
	{
	}

};


#ifndef _COUTCERRCLOG_
extern COUT cout;
extern CERR cerr;
extern CLOG clog;
#else
COUT cout;
CERR cerr;
CLOG clog;
#endif

#endif