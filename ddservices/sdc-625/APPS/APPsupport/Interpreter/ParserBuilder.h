
#if !defined(AFX_PARSERBUILDER_H__0E7B5DED_1503_4C07_92CA_13E1564F27AB__INCLUDED_)
#define AFX_PARSERBUILDER_H__0E7B5DED_1503_4C07_92CA_13E1564F27AB__INCLUDED_

class CGrammarNode;
class CToken;
class CLexicalAnalyzer;

#if defined(__GNUC__)
#include "ParserDeclarations.h"
#else
enum STATEMENT_TYPE;
#endif // __GNUC__

class CParserBuilder  
{
public:
	CParserBuilder();
	virtual ~CParserBuilder();

	CGrammarNode*	CreateParser(
		CLexicalAnalyzer* plexAnal,
		STATEMENT_TYPE stmt_type);
};

#endif
