#if !defined(AFX_RESPCODEDEVSTAT_H__52CE5087_FE62_485A_8194_9F0E6FDF03B0__INCLUDED_)
#define AFX_RESPCODEDEVSTAT_H__52CE5087_FE62_485A_8194_9F0E6FDF03B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RespCodeDevStat.h : header file
//

#include "SDC625Doc.h"
#include "sdcDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CRespCodeDevStat dialog
/* stevev 09jun06 - make it an sdc Dialog
class CRespCodeDevStat : public CDialog
*/
class CRespCodeDevStat : public sdCDialog	/* end 09jun06 */
{
// Construction
public:
	CRespCodeDevStat(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRespCodeDevStat)
	enum { IDD = IDD_RESPCODE_DEVSTAT };
	CButton	m_bOk;
	CButton	m_bIgnore;
	CSpinButtonCtrl	m_Spin;
	UINT	m_nSuppressCnt;
	CString	m_strDispMsg;
	//}}AFX_DATA
	
	BOOL m_bIgnoreFlag;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRespCodeDevStat)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRespCodeDevStat)
	afx_msg void OnIgnore();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESPCODEDEVSTAT_H__52CE5087_FE62_485A_8194_9F0E6FDF03B0__INCLUDED_)
