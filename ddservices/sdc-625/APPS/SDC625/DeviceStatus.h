#if !defined(AFX_DEVICESTATUS_H__522691C6_7B15_41FB_9A91_25DB7EC33AC0__INCLUDED_)
#define AFX_DEVICESTATUS_H__522691C6_7B15_41FB_9A91_25DB7EC33AC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DeviceStatus.h : header file
//

#include "DDLVariable.h"

#define MAX_STATUS_BITS 8
/////////////////////////////////////////////////////////////////////////////
// CDeviceStatus dialog

class CDeviceStatus : public CDialog
{
// Construction
public:
	CDeviceStatus(CDDLVariable *pDevStatus, CDDLVariable *pCommStatus, CWnd* pParent = NULL);   // standard constructor
	~CDeviceStatus();

// Dialog Data
	//{{AFX_DATA(CDeviceStatus)
	enum { IDD = IDD_COMMAND48 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDeviceStatus)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDeviceStatus)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CDDLVariable *m_pVarDevStatus;
	CDDLVariable *m_pVarCommStatus;

	CButton *m_pDevStatusArray[MAX_STATUS_BITS];
	CButton *m_pCommStatusArray[MAX_STATUS_BITS];
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVICESTATUS_H__522691C6_7B15_41FB_9A91_25DB7EC33AC0__INCLUDED_)
