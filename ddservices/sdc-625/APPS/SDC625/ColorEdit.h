#if !defined(AFX_COLOREDIT_H__8CC250B2_E94F_11D3_A108_0000E8D6C4AD__INCLUDED_)
#define AFX_COLOREDIT_H__8CC250B2_E94F_11D3_A108_0000E8D6C4AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ColorEdit.h : header file
//
#define _ColorEdit  1

#include "logging.h"

#ifndef PES_UNDEFINED
#define PES_UNDEFINED       0x0000
#define PES_NUM_UNSIGNEDINT 0x0001
#define PES_NUM_INT         0x0002
#define PES_NUM_FLOAT       0x0003
#define PES_DATE            0x0004 /*Added by ANOOP */
#define PES_NUM_HEX         0x0005  /* added by stevev 3oct07*/
#define PES_TIME_FMT        0x0006  /* added by stevev 6aug08*/
#define PES_24TIME_FMT      0x0007  /* added by stevev 20jul09*/
#define PES_ALL             0xFFFF


inline int printfType2PES_type(wchar_t printfType)
{    if (printfType == L'f' || printfType == L'e' || printfType == L'g' ||
         printfType == L'F' || printfType == L'E' || printfType == L'G'){return PES_NUM_FLOAT;}
else if ( printfType == L'd' ||  printfType == L'i')            { return PES_NUM_INT;  }
else if ( printfType == L'u' )                                  { return PES_NUM_UNSIGNEDINT;}
else if ( printfType == L'x' ||  printfType == L'X' )           { return PES_NUM_HEX;  }
else if ( printfType == L's' )                                  { return PES_ALL;  }
else if ( printfType == L't' )                                  { return PES_TIME_FMT;  }
else if ( printfType == L'r' )                                  { return PES_24TIME_FMT;  }
else if ( printfType == L'k' )  /*encoded type*/                { return PES_DATE;  }
else                                                            { return PES_UNDEFINED;}
};

#endif  //PES_UNDEFINED
/////////////////////////////////////////////////////////////////////////////
// CColorEdit window

class CColorEdit : public CEdit
{
    DECLARE_DYNAMIC(CColorEdit)

// Construction
public:
    CColorEdit();

// Attributes
public:

// Operations
public:
    void Highlight(BOOL state = TRUE);
    BOOL SubclassEdit(UINT nID, CWnd* pParent, WORD wParseStyle,int,int,int);
    void Enable(BOOL bEnable);
    BOOL IsEnabled();
    void setFormatInfo(int style, int max_Chrs,int decimal_Lt,int char_cnt);

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CColorEdit)
    public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    protected:
    virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
    //}}AFX_VIRTUAL

// Implementation
public:
    virtual ~CColorEdit();

    // Generated message map functions
protected:
    //{{AFX_MSG(CColorEdit)
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    // Removed OnLButtonDown, POB - 5/1/2014
    afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
    //}}AFX_MSG

    DECLARE_MESSAGE_MAP()
private:
    BOOL m_state;
    BOOL m_isEnabled;
    COLORREF m_BkColor;
    CBrush m_brush;
    static const COLORREF m_YELLOW;
    static const COLORREF m_WHITE;
    int nMax_Chars;
    int nDecimal_Limit;
    int nLeft_of_decimal;
    WORD  m_wParseStyle;      // C++ member data
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLOREDIT_H__8CC250B2_E94F_11D3_A108_0000E8D6C4AD__INCLUDED_)
