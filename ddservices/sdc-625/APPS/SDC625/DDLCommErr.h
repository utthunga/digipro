/*************************************************************************************************
 *
 * $Workfile:  $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "DDLCommErr.h"
 */

#ifndef _DDLCOMMERROR_H
#define _DDLCOMMERROR_H

#define DDL_COMM_ERROR_BASE  3000

#define DDL_COMM_PARAMETER_ERROR	(DDL_COMM_ERROR_BASE +  0)
#define DDL_COMM_CONBYPOLL_FAILED	(DDL_COMM_ERROR_BASE +  1)
#define DDL_COMM_DEVICE_NOT_AT_POLL	(DDL_COMM_ERROR_BASE +  2)
#define DDL_COMM_DISCONNECT_FAILED	(DDL_COMM_ERROR_BASE +  3)
#define DDL_COMM_SEARCH_FAILED		(DDL_COMM_ERROR_BASE +  4)
#define DDL_COMM_MEMORY_FAILURE		(DDL_COMM_ERROR_BASE +  5)
#define DDL_COMM_UTIL_FAILED		(DDL_COMM_ERROR_BASE +  6)
#define DDL_COMMUNICATIONS_FAILED	(DDL_COMM_ERROR_BASE +  7)
#define DDL_COMM_EXTRACTION_FAILED	(DDL_COMM_ERROR_BASE +  8)
#define DDL_COMM_NO_RESPONSE		(DDL_COMM_ERROR_BASE +  9)
#define DDL_COMM_PROGRAMMER_ERROR	(DDL_COMM_ERROR_BASE + 10)
#define DDL_COMM_QUEUE_FAILURE		(DDL_COMM_ERROR_BASE + 11)
#define DDL_COMM_SHUTDOWNINPROGRESS	(DDL_COMM_ERROR_BASE + 12)
#define DDL_COMM_CONBYTAG_FAILED	(DDL_COMM_ERROR_BASE + 13)  // timj added 4/6/04
#define DDL_COMM_UNKNOWN_UNIV_REV	(DDL_COMM_ERROR_BASE + 14)	// stevev added 12feb07


#endif//_DDLCOMMERROR_H

 /*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History:  $
 *  
 *************************************************************************************************
 */
