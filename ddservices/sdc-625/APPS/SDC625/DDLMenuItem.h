/*************************************************************************************************
 *
 * $Workfile: DDLMenuItem.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface for the CDDLMenuItem class
 * #include "DDLMenuItem.h"	
 */
#if !defined(AFX_DDLMENUITEM_H__A86184E6_6AFD_4FDE_991F_D455A6391AB5__INCLUDED_)
#define AFX_DDLMENUITEM_H__A86184E6_6AFD_4FDE_991F_D455A6391AB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLBase.h"

class CDDLMenuItem : public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLMenuItem)

public:
	CDDLMenuItem(CDDLBase* item, unsigned qual);
	virtual ~CDDLMenuItem();

#ifdef _DEBUG
	unsigned parentID;
	string srcLocation;
#endif
// assignment operators
    operator CString(); // used to get a value from an object (VARABLES only)

// Special menu handling
    void LoadContextMenu(CMenu *pMenu, int& nPos);
    void Execute();
    void OnUpdateMenuExecute(CCmdUI* pCmdUI);
    void DisplayValue();
	//Vibhor 1405 : Strat of Code
    void Execute(CString szMenuSourceName,CString strHelp); // Modified the Definition
	//Vibhor 1405 : Strat of Code

// Overrides
	virtual CDDLMenu* getMenuPtr(void);
    virtual BOOL IsMethod();// PAW 03/03/09 added BOOL.stevev 12aug10 - to bool
    BOOL IsVariable();
    BOOL IsMenu();
    CString GetRelation();
	CString GetUnitStr();
    CString GetHelp();
    void SetHelp( CString strHelp);
    CString GetName();
    void SetName( CString strName );
	void UpdateChildList(void);

	BOOL IsReadOnly();
    BOOL IsDisplayValue();  // Value is set for DISPLAY_VALUE
	BOOL IsReview();	    // only difference about REVIEW menu is all items
							// VARIABLEs and all are DISPLAY_VALUE
	bool HasNoLabel();
	bool HasNoUnit();
	bool IsInline();

	bool HasOverline();
	bool HasARemote();

    BOOL IsValid();
	/*Vibhor 170304: Start of Code*/
	BOOL IsEditDisplay();
	/*Vibhor 170304: End of Code*/
    void SetValidity(BOOL valid);
    CDDLBasePtrList_t& GetChildList();
/*    CDDLBase * GetParent();
    void SetParent( CDDLBase* pParent );*/
    HTREEITEM GetTreeItem();
    void SetTreeItem(HTREEITEM hTreeItem);

	unsigned long GetID();
// Menu specific
    BOOL SetQualifer(unsigned qualifier);

    CDDLBase* m_item; // The item on the menu

private:
    bool m_bIsReview;
    bool m_bIsDsplyVal;
    bool m_bIsReadOnly;

	bool m_bHasNoLabel;
	bool m_bHasNoUnit;
	bool m_bIsInline; // images only
	bool m_bHasOverline;// previous was Separator
	bool m_bHasRemote;  // has a remote popup to hold item
};

#endif // !defined(AFX_DDLMENUITEM_H__A86184E6_6AFD_4FDE_991F_D455A6391AB5__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLMenuItem.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
