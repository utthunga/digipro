/*************************************************************************************************
 *
 * $Workfile: SDC625Doc.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      interface of the CDDIdeDoc class
 * #include "SDC625Doc.h"
 */

#if !defined(AFX_DDIDEDOC_H__0A35016B_401C_4BB6_BE9E_E3DE1A98E911__INCLUDED_)
#define AFX_DDIDEDOC_H__0A35016B_401C_4BB6_BE9E_E3DE1A98E911__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "HartTrans.h"    // Added by ClassView
#include "DDLMain.h"
#include "DDLMenu.h"
//#include "DDLDevice.h"
#include "ddbDeviceMgr.h"
#include "DDLMenuItem.h"
#include "DDLVariable.h"
#include "DDLEDisplay.h"
#include "DDLMethod.h"
#include "SDC625TreeView.h"
#include "SDC625ListView.h"
#include "Client.h"

//#ifndef IS_OLE
#include "NamedPipe.h"
#include "ddlCommSimminfc.h"
#ifndef XMTRDD
#include "DDLCommInfc.h"
#include "HARTDECommInfc.h" /*VMKP added*/
#include "SDCImage.h"
#include "Command48_PropSheet.h" //Added by ANOOP 
#else
#define SDC_ImageList int
#define SDClangImage  void
class CbaseComm;
class CCommand48_PropSheet;
class CMainFrame;
#define CMainFrame void
#endif

#include "HARTsupport.h"
#include "FileSupport.h"    // addd stevev 09nov04


//#else   // it IS_OLE
//#import <HartOpc.exe>
//#import <HartOpcSelect.dll>
//#include "DDLopcCommInfc.h"
//#endif //IS_OLE

/***************************************************************************
 * Global Definitions
 ***************************************************************************/

#define MAX 256 // Size of data Array for Request buffer
    
#define RESP_CODE           0
#define DEVICE_STATUS       1
#define COMM_STATUS         2
#define MAX_PACKET_STRING 1000

/*Vibhor 090204: Start of Code*/

#define DEFAULT_SUPPRESS_CNT 500    

/*Vibhor 090204: End of Code*/


// stevev 4/2/4::
#define DFLT_REPOLLTIME     1000 /* mS */
#define DFLT_POLLRETRIES    5    /* number of retries for busy/delayed response*/

/*
 * Hints for updating the views.
 */
enum
{
/* 06dec05 stevev - new notification system 
     // lHint value              pHint
      UPDATE_REBUILD_ALL     //  not used
   ,  UPDATE_REBUILD_LIST    //  pHdw Parent of list to rebuild
   ,  UPDATE_DELETE_BELOW    //  pHdw whose children will be deleted
   ,  UPDATE_REMOVE_ITEM     //  pHdw which will be deleted
   ,  UPDATE_ADD_ITEM        //  pHdw of item to add.
   ,  UPDATE_ITEM             //  pHdw of item to update.
   ,  UPDATE_WINDOW_ITEM     // similar to update item -usually a value-change item

   ,  window view will use most of these */
      UPDATE_T_DO_NOTHING,  //0 CView::OnInitialUpdate() does a OnUpdate(NULL, 0, NULL);  
      UPDATE_T_DRAW_HOLD    //1 set (tree) redraw off till release message
   ,  UPDATE_T_DRAW_DRAW    //2 set (tree) redraw back on
   /* new update plan */
   ,  UPDATE_STRUCTURE     // 3 for treeview - will send an update list view
   ,  UPDATE_ADD_DEVICE    // 4 for treeview - will send an update list view
   ,  UPDATE_LIST_VIEW     // 5 for listview to update as current selection
   ,  UPDATE_REMOVE_DEVICE // 6 for treeview  - will send an update list view
   ,  UPDATE_ONE_VALUE     // 7 for listview  - single value, label, unit updated
   ,  UPDATE_TREE_VIEW     // 8 works like add_device just for tree&list 
   ,  UPDATE_DELETE_ITEM   // 9 list item ceases to exist..we must strip tree of oointers to it
                           //   31jul07-crashed when root_menu both a table and a window & we get a struct change
};

/*
 * List View Column Identifiers
 */
enum
{
     COL_NAME,
     COL_DATA,
     COL_UNITS,
};

// Note: we add support for IID_IDDIde to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {1A24E574-3FE1-41D0-849A-A1D7AD2209B8}
/*static const IID IID_IDDIde =
{ 0x1a24e574, 0x3fe1, 0x41d0, { 0x84, 0x9a, 0xa1, 0xd7, 0xad, 0x22, 0x9, 0xb8 } };orginal Wizard stuff*/

// uuid(9AF50C9D-D462-11D2-97A4-0000A001344B)
static const IID IID__IHartCommEvents = 
{0x9AF50C9D,0xD462,0x11D2,{0x97,0xA4,0x00,0x00,0xA0,0x01,0x34,0x4B}};

// forward reference
// class CdocCommInfc;

// other's access to our stuff
#define PDEVMGR (GetDocument()->pDevMgr)
#define PCURDEV (GetDocument()->pCurDev)

/* Prasad start of code */
#define DYNAMIC_VAR_MENU_NAME   _T("Dynamic Variables")
/* End of code */

class CDDIdeDoc : public CDocument
{
protected: // create from serialization only
    CDDIdeDoc();
    DECLARE_DYNCREATE(CDDIdeDoc)

    struct Transaction 
    {
        int   nCmdNum;
        int   nTransID;
        long  lHandle;
    };

    BOOL reGenerateComm(void);

    /* stevev 4/6/04 */
    int retryTime;// mS  delayed response/busy delay before retry - from preferences
    int retryCount;//    number of retries before checking with user for abort
    /* end stevev */
    bool m_autoUpdateDisabled;// made private 15nov11
    vector<bool>  appComStack;// moved here   15nov11

// Attributes
public:
    hCDeviceManger* pDevMgr;
    hCddbDevice*    pCurDev;
    CbaseComm*      pCommInfc;
    DWORD           m_dwCookie;
    /* Removed CDDLMain pointer - It is no longer used as tree control root menu, POB - 5/18/2014 */
    /* Removed root menu pointer. It is moved to CTableChildFrame, POB - 5/18/2014 */
    /* Removed commented out code, POB - 5/20/2014 */
    CDDLBase *      m_pMenuSource; // Item that is selected for pop-up menu
    Indentity_t     m_currIdentity;// identity of current device
    cmdLineInfo_t   m_copyOfCmdLine;
    CClient *       m_pSocket;

/* Prasad start of code */
    CDDLBase *      m_pDynamicVariables;    
    /* stevev moved from a static to a class var */ 
    int iNumberOfDynamicVars;
/* End of code */
#ifndef _CONSOLE
    CCommand48_PropSheet *ptrCMD48Dlg; //ANOOP 31JAN04
#endif
/* stevev 30mar05 - keep the status bar */
    CStatusBar* m_pStatusBar;

/*<START>Modified by Anoop moved from the Add block in CDDIdeDoc to the class */
    itemID_t DynamicVariableItemIds[1000];
    // moved to the class....static int iNumberOfDynamicVars = 0;
/*<END>Modified by Anoop moved from the Add block in CDDIdeDoc to the class */


// took private 15nov11 --  bool m_autoUpdateDisabled;
    bool m_enteredFileNew;
// TEMP make this a global...   bool m_doingIdle;       // stevev27jul05 - to handshake destruction
    bool m_bSocketConnected;

    //prashant oct 2003'
    CMethodSupport* m_pMethSupport;
    bool m_bVariableEditedByUser;

  /*Vibhor 050204: Start of Code*/

    bool m_bIgnoreRespCode;
    
    unsigned int m_uSuppressionCnt;

    CString m_strDisplay;

  /*Vibhor 050204: End of Code*/

    /* stevev 09nov04 - file stuff */
    CsdcFileSupport* m_pFileSup;
    /* end stevev */
    /*stevev 8jan5 - */
    SDC_ImageList    m_ImageList;
    /* end stevev 8jan5 */

// Operations
public:

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDDIdeDoc)
    public:
    virtual BOOL OnNewDocument();
    virtual void Serialize(CArchive& ar);
    virtual void OnCloseDocument();
    //}}AFX_VIRTUAL
    virtual BOOL DoSave(LPCTSTR lpszPathName, BOOL bReplace = TRUE);


// Implementation
public:
    CbaseComm* GetDeviceComm();// generic comm interface

    BOOL DoIdle(LONG lCount);
    // stevev 5/11/04 - sniff the dd file
    void sniffFile(void);

    /* modified stevev 9/22/03
    void genDev(void){if (m_pTreeView) m_pTreeView->generateDevice(); };
    */
    void genDev(void);// code moved to .cpp file 27aug12
    void generateDevice(void);                        /* moved from treeview 05aug05 */
    LRESULT CloseDevice(WPARAM wParam, LPARAM lParam);/* moved from treeview 05aug05 */

    // made m_autoUpdateDisabled private 15nov11 to encapsulate functionality and allow
    //      a critical section to read/write in the future
    void  disableAppCommRequests(void);    
    void  enableAppCommRequests(void);
    bool  appCommEnabled (void);

    /* functionality moved to treeview void Add(ulong menuItemID, CDDLBase* pLevel);stevev 05dec05 */
    void HostCommErrorCallback(long status, string& errStr);
//  RETURNCODE CnctToDevice(long& handle);
    RETURNCODE AttachToSegment();

// HART Transactions que functions
    RETURNCODE QueueTrans(int cmdNum, BYTE* pData, BYTE dataCnt, long lHandle, int transNum );
    RETURNCODE DeQueueTrans( int& cmdNum, BYTE* pdata, BYTE dataCnt, UINT& cmdStatus,long lHandle );

// Sync Send
    RETURNCODE Send(int Cmd, BYTE* pData, BYTE& ByteCount, int timeout, UINT& cmdStatus, long handle); // Sync

    void ReadVariables(vector <hCitemBase*>& VariablesToRead);

    bool    ready2close(); // stevev 11jul07 - there are conditions where it'll lock up.
    virtual ~CDDIdeDoc();

    CClient *GetSocket();

    /* stevev 4/6/04 - stub outs*/
    int         getPollRetryTime() { return (retryTime); };// temporary use default
    int         getPollRetryMax()  { return (retryCount);};// need to get these from the preferences
    /* end stevev */
    /* stevev 09nov04 add files */
    string getFileSupPath(void);
    CsdcFileSupport* GetDeviceFile(void);// instantiates only (NULL if fails)
    /* end stevev */
    /* stevev 08jan05 */
    RETURNCODE    AddImage(unsigned idx, string& sLang,unsigned int siz,BYTE* pRaw);
    SDClangImage* GetImage(int idx, char* lang = NULL);
    /* end stevev 8jan5 */
    void generateDirFile(aCentry* pRoot,CString& fileName );/* from treeview 05aug05 */

#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

private:
// Async Send
    RETURNCODE AsyncSend(int Cmd, BYTE ByteCount, BYTE * pData, long handle); // Async
    RETURNCODE Array2Identity(int dataCnt, char* DataStr); // loads m_currIdentity


protected:
    HRESULT AdviseSink(IUnknown * pUnkCP, IUnknown * pUnk, const IID & iid, LPDWORD pdw);
// Generated message map functions
protected:
    //{{AFX_MSG(CDDIdeDoc)
        // NOTE - the ClassWizard will add and remove member functions here.
        //    DO NOT EDIT what you see in these blocks of generated code !
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
#ifdef IS_OLE
    // Generated OLE dispatch map functions
    //{{AFX_DISPATCH(CDDIdeDoc)
    afx_msg long AsyncResponse(long lHandle, const VARIANT FAR& varReply);
    //}}AFX_DISPATCH
    DECLARE_DISPATCH_MAP()
    DECLARE_INTERFACE_MAP()
#else
    afx_msg long AsyncResponse(long lHandle, const VARIANT FAR& varReply);
#endif //IS_OLE
private:
    CRITICAL_SECTION m_csQue;
    CTypedPtrList<CPtrList, Transaction*> m_MsgQue;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDIDEDOC_H__0A35016B_401C_4BB6_BE9E_E3DE1A98E911__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDIdeDoc.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:52a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Verify/Add HART standard headers and footers
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
