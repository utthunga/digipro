/*************************************************************************************************
 *
 * $Workfile: $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "ddlCommSimminfc.h".		
 */
#if !defined(AFX_COMMSIMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625X__INCLUDED_)
#define AFX_COMMSIMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625X__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DDLCommInfc.h : header file - for both DDLopc and DDLpipe
//
#ifndef _CONSOLE
#include "SDC625Doc.h"
#endif
#include "HARTsupport.h"
#include "DDLBaseComm.h"


class CMainFrame;
/////////////////////////////////////////////////////////////////////////////
// CdocSimmInfc thread

class CdocSimmInfc : public CbaseComm // hCcommInfc
{
protected:
// use base class	CDDIdeDoc*  pDoc;
	CMainFrame*  m_pMainFrm;	/* stevev - for light control */

	Indentity_t thisIdentity;
	HANDLE      commHndl;    // from the doc (pipe or opc)
//Added by ANOOP below
	bool         m_hadConfigChanged;
	int          m_skipCount;  // how many we have skipped

/*Vibhor 120204: Start of Code*/

	bool         m_bMoreStatusSent;

	/*Vibhor 120204: End of Code*/

	commStatus_t m_CommStatus;	// enabling all rolled into one

	BYTE hstAddr;

public:
	CdocSimmInfc(CDDIdeDoc* pD = NULL);
	virtual ~CdocSimmInfc();          

// Attributes
public:
	hCPktQueue thePktQueue;
		
	HANDLE m_pComHARTSvr; // HANDLE and long are the same size 

// Operations
public:
	// use parent's setRcvService(hCsvcRcvPkt* cpRcvSvc) // sets the dispatcher's service routine

	hPkt* getMTPacket(bool pendOnMT=false);// gets an empty packet for generating a command 
								//		(gives caller ownership of the packet*)
	void  putMTPacket(hPkt*);	// returns the packet memory back to the interface 
								//		(this takes ownership of the packet*)
	RETURNCODE
		  SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
						        // puts a packet into the send queue  
								//		(takes ownership of the packet*)
	virtual
		RETURNCODE 
			SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus);
			// puts a packet into the front of the send queue(takes ownership of the pkt*)
	RETURNCODE		
		  SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus);

	int sendQsize(void); 	    // how many are in the send queue ( 0 == empty )
	int	sendQmax(void) ;		// how many will fit
	void  clearQueue(void);
	
	RETURNCODE  initComm(void);	// setup connection (called after instantiation of device)
	RETURNCODE  shutdownComm(void);
	RETURNCODE  GetIdentity(Indentity_t& retIdentity,BYTE pollAddr);
	void enableComm(void){
		m_CommStatus = commOK;
	};

// stevev 25jan06 -- use the DDLBaseComm version of this routine ********	
//	void  notifyAppVarChange(itemID_t changedItemNumber,  NUA_t isChanged = NO_change);
	void  notifyRCDScmd48(hMsgCycle_t& thisMsgCycle);
	void  stxSocketDump(hPkt* pPkt);// stevev 4/20/04 - extract the logging to a separate func
	void  ackSocketDump(hPkt* pPkt);// stevev 4/20/04 - extract the logging to a separate func

	void setHostAddress(BYTE ha); // nop in simulation
	short setNumberOfRetries(short newRetries){return 0;};// no-op in simulation
	
	void  disableAppCommRequests(void);
	void  enableAppCommRequests (void);
	bool  appCommEnabled        (void); 

	// DDIde specific calls
	RETURNCODE Search(Indentity_t& retIdentity, BYTE& newPollAddr);

// Implementation
protected:
	// format conversion
	RETURNCODE  pipe2pkt(BYTE* pipeBuf, DWORD pipeLen, hPkt* pPkt);
	RETURNCODE  pkt2pipe(hPkt* pPkt, BYTE* pipeBuf, DWORD& pipeLen);
	RETURNCODE  Array2Identity(int dataCnt, char* DataStr);// fills thisIdentity

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMSIMMINFC_H__20E7D19B_5081_4458_BC37_BBC706B3625E__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLCommInfc.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:52a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Verify/Add HART standard headers and footers
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
