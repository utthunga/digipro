/*************************************************************************************************
 *
 * $Workfile: DDIdeTreeView.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface of the CDDIdeTreeView class
 * #include "DDIdeTreeView.h".		
 */
#if !defined(AFX_DDIDETREEVIEW_H__405F8DF2_9A2F_41DB_BBED_ED4430B400F9__INCLUDED_)
#define AFX_DDIDEVIEW_H__405F8DF2_9A2F_41DB_BBED_ED4430B400F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "StdAfx.h"
#include "ddbDeviceMgr.h"
//#include "MethodSupport.h"
#include "ddbParserInfc.h"
//#include "SDC625_drawTypes.h"
#include "DDLBase.h"

#include "logging.h"

class CdrawTbl;
class CDDIdeDoc;
class CTableChildFrame;

class CDDIdeTreeView : public CTreeView
{
protected: // create from serialization only
	CDDIdeTreeView();
	DECLARE_DYNCREATE(CDDIdeTreeView)

//	void generateDevice(void); // common to browse and oninitialupdate

// Attributes
public:
	CDDIdeDoc* GetDocument();
//no longer used	void generateDevice(void); // common to browse and oninitialupdate
								//made public to generate from thr app
/* VMKP added on 030204 */    
	CDDLBase *ReturnTreeItemId(unsigned long lItemId,unsigned long lParentItemId, CDDLBase *pBase);
/* VMKP added on 030204 */    
// Operations
public:
    void BuildTreeFrom( CDDLBase * pBase );
	int  GetTreeIncidence( ulong symID, CDDLBasePtrList_t& returnList);// stevev 29nov05
	int  DeleteBranchIncidence( ulong symID, CDDLBase * pStartBase);//stevev 25may11

// helpers
	int FillMenuList( CDDLBase* pNewItem, bool loclReview );//13apr11-formally part AddSubMenu
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDIdeTreeView)
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
/*	afx_msg LRESULT OnCloseDevice(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGenDevice(WPARAM wParam, LPARAM lParam);moved to mainfrm &doc 05aug05*/

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~*");} return CTreeView::WindowProc(message,wParam,lParam);};
#endif
// Implementation
public:
	virtual ~CDDIdeTreeView();

#define P_TABLE_FRM   (m_pTableFrame)
#define P_LISTVIEW    (m_pTableFrame->m_pListView)
	CTableChildFrame*  m_pTableFrame;
	CdrawTbl * m_pTwin;	// This point to the CdrawTbl object that invoked the frame window

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void generateDirFile(aCentry* pRoot, CString& fileName );

protected:
    CImageList m_ctlImage;
    void WriteProfileSettings();
    void SelectParent( CDDLBase *pBase );
    void AddOneItem( CDDLBase *pBase );
	void ExpandTree( HTREEITEM Item, UINT nCode );
	// stevev 29nov05
	int  GetBranchIncidence( ulong symID, 
							CDDLBasePtrList_t& returnList,CDDLBase * pStartBase);
	int  deleteSelfNbranch(CDDLBase *pItem) ;
	int  deleteBranch(CDDLBase *pItem) ;
	int  AddRootMenu( void ); // for doc's current device
	int  AddSubMenu( CDDLBase* pNewItem, CDDLBase* pBase );


	// use the doc's version:::hCDeviceManger* pDevMgr;
	// use the doc's version:::hCdeviceSrvc *  pCurDev;

// Generated message map functions
protected:
	//{{AFX_MSG(CDDIdeTreeView)
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMenuHelp();
	afx_msg void OnMainBrowse();
	afx_msg void OnDeviceDetails();
	afx_msg void OnViewFaceplate();
	afx_msg void OnUpdateMainBrowse(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewFaceplate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeviceDetails(CCmdUI* pCmdUI);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnExpandTree();
	afx_msg void OnCollapseTree();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
/* moved to mainfrm */	
//	afx_msg void OnViewMorestatus();
//	afx_msg void OnUpdateViewMorestatus(CCmdUI* pCmdUI);
#ifdef _DEBUG
	bool TVexists;
#endif
};

#ifndef _DEBUG  // debug version in DDIdeView.cpp
inline CDDIdeDoc* CDDIdeTreeView::GetDocument()
   { return (CDDIdeDoc*)m_pDocument; }
#else
 ;
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDIDEREEVIEW_H__405F8DF2_9A2F_41DB_BBED_ED4430B400F9__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDIdeTreeView.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:52a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Verify/Add HART standard headers and footers
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
