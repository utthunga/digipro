#if !defined(AFX_WINDOWCHILDFRM_H__AB75259F_B3D9_4732_8DF9_092AAA391685__INCLUDED_)
#define AFX_WINDOWCHILDFRM_H__AB75259F_B3D9_4732_8DF9_092AAA391685__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Window_ChildFrm.h : header file
//
#include "StdAfx.h"
#include "Child_Frm.h"

#ifndef VIRTUALCALLERR
#define VIRTUALCALLERR  (2)
#endif
#include "logging.h"

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame frame

class CWindowChildFrame : public CChildFrame
{
	DECLARE_DYNCREATE(CWindowChildFrame)
protected:
	CWindowChildFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWindowChildFrame)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~2");} return CMDIChildWnd::WindowProc(message,wParam,lParam);};
#endif

// Implementation
protected:
	virtual ~CWindowChildFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CWindowChildFrame)
	afx_msg void OnClose();
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINDOWCHILDFRM_H__AB75259F_B3D9_4732_8DF9_092AAA391685__INCLUDED_)
