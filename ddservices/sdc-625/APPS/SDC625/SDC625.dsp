# Microsoft Developer Studio Project File - Name="SDC625" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=SDC625 - WIN32 DEBUG
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SDC625.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SDC625.mak" CFG="SDC625 - WIN32 DEBUG"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SDC625 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "SDC625 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "SDC625 - Win32 ReleaseWithSymbols" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SDC625 - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FD /c
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Ot /I "..\..\Apps\Appsupport\SDCImage" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\HARTDE" /I "..\APPsupport\ParserInfc" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\CommonClasses\FlexGrid" /I "..\..\CommonClasses\ImgSupport\CxImage" /D "_UNICODE" /D "UNICODE" /D "DBG_PVFC" /D "XMTR" /D "NDEBUG" /D "IS_SDC" /D "WIN32" /D "_WINDOWS" /D "RK_STATIC" /D "_USE_PLOT_LIB" /D "_AFXDLL" /FD /Zm"400" /c
# SUBTRACT CPP /Ox /Oa /Oi /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ..\APPsupport\HARTDE\Release\HARTDELib.lib setupapi.lib regkey.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /incremental:yes /machine:I386 /nodefaultlib:"LIBC.lib" /nodefaultlib:"nafxcw.lib" /nodefaultlib:"uafxcw.lib" /nodefaultlib:"mfc42.lib" /nodefaultlib:"mfcs42.lib" /libpath:"..\lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SDC625 - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FD /GZ /c
# ADD CPP /nologo /Zp1 /MDd /W3 /Gm /GX /Zi /Od /I "..\..\Apps\Appsupport\SDCImage" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\HARTDE" /I "..\APPsupport\ParserInfc" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\CommonClasses\FlexGrid" /I "..\..\CommonClasses\ImgSupport\CxImage" /D "_UNICODE" /D "UNICODE" /D "STARTSEQ_LOG" /D "XMTR" /D "_DEBUG" /D "IS_SDC" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "RK_STATIC" /D "_USE_PLOT_LIB" /Fr /FD /Zm200 /I /GZ /fixed:no /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ..\APPsupport\HARTDE\Debug\HARTDELib.lib regkey.lib setupapi.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /incremental:no /debug /machine:I386 /nodefaultlib:"nafxcwd.lib" /nodefaultlib:"uafxcwd.lib" /nodefaultlib:"libcd.lib" /nodefaultlib:"LIBC.lib" /nodefaultlib:"MSVCRT" /nodefaultlib:"mfc42d" /nodefaultlib:"mfcs42d" /nodefaultlib:"mfco42d" /pdbtype:sept /libpath:"..\lib" /libpath:"..\AppSupport\SDCimage\lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SDC625 - Win32 ReleaseWithSymbols"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "SDC625___Win32_ReleaseWithSymbols"
# PROP BASE Intermediate_Dir "SDC625___Win32_ReleaseWithSymbols"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "SDC625___Win32_ReleaseWithSymbols"
# PROP Intermediate_Dir "SDC625___Win32_ReleaseWithSymbols"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /Zp1 /MD /W3 /GX /O2 /I "..\..\Apps\Appsupport\SDCImage" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\HARTDE" /I "..\APPsupport\ParserInfc" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\CommonClasses\FlexGrid" /I "..\..\CommonClasses\ImgSupport\CxImage" /D "XMTR" /D "NDEBUG" /D "IS_SDC" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "RK_STATIC" /D "_USE_PLOT_LIB" /FD /Zm"400" /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /Zp1 /MD /W3 /GX /Zi /Ot /I "..\..\Apps\Appsupport\SDCImage" /I "..\..\Apps\Appsupport\DDParser" /I "..\..\Device.Lib" /I "..\appsupport\HARTDE" /I "..\APPsupport\ParserInfc" /I ".\\" /I "..\..\CommonClasses\ChkListCtrl\EditClasses" /I "..\..\CommonClasses\ChkListCtrl" /I "..\..\CommonClasses\Arguments\include" /I "..\..\Common" /I "..\AppSupport\DevServices" /I "..\..\Communications\include" /I "..\..\CommonClasses\FlexGrid" /I "..\..\CommonClasses\ImgSupport\CxImage" /D "_UNICODE" /D "UNICODE" /D "DBG_PVFC" /D "XMTR" /D "NDEBUG" /D "IS_SDC" /D "WIN32" /D "_WINDOWS" /D "RK_STATIC" /D "_USE_PLOT_LIB" /D "_AFXDLL" /FR /FD /Zm"400" /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\APPsupport\HARTDE\Release\HARTDELib.lib regkey.lib /nologo /subsystem:windows /incremental:yes /machine:I386 /nodefaultlib:"LIBC.lib" /nodefaultlib:"nafxcw.lib" /nodefaultlib:"uafxcw.lib" /libpath:"..\lib"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 ..\APPsupport\HARTDE\Release\HARTDELib.lib regkey.lib setupapi.lib /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /nodefaultlib:"LIBC.lib" /nodefaultlib:"nafxcw.lib" /nodefaultlib:"uafxcw.lib" /nodefaultlib:"mfc42" /nodefaultlib:"mfcs42" /libpath:"..\lib" /fixed:no
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "SDC625 - Win32 Release"
# Name "SDC625 - Win32 Debug"
# Name "SDC625 - Win32 ReleaseWithSymbols"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\CommonClasses\Arguments\src\argcargv.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\Arguments\src\Arguments.cpp
# End Source File
# Begin Source File

SOURCE=.\Bit.cpp
# End Source File
# Begin Source File

SOURCE=.\Bit_Enum.cpp
# End Source File
# Begin Source File

SOURCE=.\BitEnumDispValDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\APPsupport\SDCimage\BtnST.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\CheckComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\Child_Frm.cpp
# End Source File
# Begin Source File

SOURCE=.\Client.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\ClrComboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorEdit.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\comboBox.cpp
# End Source File
# Begin Source File

SOURCE=.\Command48_PropSheet.cpp
# End Source File
# Begin Source File

SOURCE=..\APPsupport\DevServices\ddb_EventMutex.cpp
# End Source File
# Begin Source File

SOURCE=.\ddl_ini.cpp

!IF  "$(CFG)" == "SDC625 - Win32 Release"

!ELSEIF  "$(CFG)" == "SDC625 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "SDC625 - Win32 ReleaseWithSymbols"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DDLBase.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLBaseComm.cpp
# End Source File
# Begin Source File

SOURCE=.\ddlCommInfc.cpp
# End Source File
# Begin Source File

SOURCE=.\ddlCommSimmInfc.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLEDisplay.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLMain.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLMenuItem.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLMethod.cpp
# End Source File
# Begin Source File

SOURCE=.\DDLVariable.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDselect.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDselectSplitter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\DDSelectTree.cpp
# End Source File
# Begin Source File

SOURCE=.\DevCommStatPropPage.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceDetails.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceStatus.cpp
# End Source File
# Begin Source File

SOURCE=.\DevStatPageOne.cpp
# End Source File
# Begin Source File

SOURCE=.\Dyn_Controls.cpp
# End Source File
# Begin Source File

SOURCE=.\Dyn_DialogItemEx.cpp
# End Source File
# Begin Source File

SOURCE=.\EDisplayDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ErrLogView.cpp
# End Source File
# Begin Source File

SOURCE=.\FacePlate.cpp
# End Source File
# Begin Source File

SOURCE=.\FileSupport.cpp
# End Source File
# Begin Source File

SOURCE=.\FileVersion.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCell.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCellBase.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCtrl.cpp
# ADD CPP /I "..\..\Apps\SDC625"
# SUBTRACT CPP /I "..\..\Apps\Appsupport\SDCImage" /I "..\..\CommonClasses\ImgSupport\CxImage"
# End Source File
# Begin Source File

SOURCE=..\APPsupport\HARTDE\HARTDECommInfc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\InPlaceEdit.cpp
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\logging.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MainListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\MethodDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MethodEditCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\MethodSupport.cpp

!IF  "$(CFG)" == "SDC625 - Win32 Release"

# ADD CPP /I "..\APPsupport\MEE" /I "..\AppSupport\Interpreter" /I "..\AppSupport\BuiltinLib"

!ELSEIF  "$(CFG)" == "SDC625 - Win32 Debug"

# ADD CPP /I "..\APPsupport\MEE" /I "..\AppSupport\Interpreter" /I "..\AppSupport\BuiltinLib"

!ELSEIF  "$(CFG)" == "SDC625 - Win32 ReleaseWithSymbols"

# ADD CPP /I "..\AppSupport\MEE" /I "..\AppSupport\Interpreter" /I "..\AppSupport\BuiltinLib"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\paredit.cpp
# End Source File
# Begin Source File

SOURCE=.\PrefDialog1.cpp
# End Source File
# Begin Source File

SOURCE=.\Preferences1.cpp
# End Source File
# Begin Source File

SOURCE=..\..\common\registerWaits.cpp
# End Source File
# Begin Source File

SOURCE=.\RespCodeDevStat.cpp
# End Source File
# Begin Source File

SOURCE=.\SButton.cpp
# End Source File
# Begin Source File

SOURCE=.\Scope_Ctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625.cpp

!IF  "$(CFG)" == "SDC625 - Win32 Release"

!ELSEIF  "$(CFG)" == "SDC625 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "SDC625 - Win32 ReleaseWithSymbols"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SDC625.odl
# End Source File
# Begin Source File

SOURCE=.\SDC625.rc
# End Source File
# Begin Source File

SOURCE=.\SDC625_drawLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625_drawTypes.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625_HiddenView.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625_WindowView.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625Doc.cpp

!IF  "$(CFG)" == "SDC625 - Win32 Release"

!ELSEIF  "$(CFG)" == "SDC625 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "SDC625 - Win32 ReleaseWithSymbols"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SDC625ListView.cpp
# End Source File
# Begin Source File

SOURCE=.\SDC625TreeView.cpp
# End Source File
# Begin Source File

SOURCE=.\sdCDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\SdcView.cpp
# End Source File
# Begin Source File

SOURCE=.\SDMV_Template.cpp
# End Source File
# Begin Source File

SOURCE=.\Server.cpp
# End Source File
# Begin Source File

SOURCE=.\STab_Ctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\COMMON\strptime.cpp
# End Source File
# Begin Source File

SOURCE=.\Table_ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\URL_LinkButton.cpp
# End Source File
# Begin Source File

SOURCE=.\UsefulSplitterWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\VariableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Wait_Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WaoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WaveEditDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Window_ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\Window_Map.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\args.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\Arguments\include\Arguments.h
# End Source File
# Begin Source File

SOURCE=.\BASETSD.H
# End Source File
# Begin Source File

SOURCE=.\Bit.h
# End Source File
# Begin Source File

SOURCE=.\BitEnumDispValDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\CellRange.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\CheckComboBox.h
# End Source File
# Begin Source File

SOURCE=.\Client.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\ChkListCtrl\EditClasses\ClrComboBox.h
# End Source File
# Begin Source File

SOURCE=.\ColorEdit.h
# End Source File
# Begin Source File

SOURCE="..\..\Common Classes\ChkListCtrl\EditClasses\comboBox.h"
# End Source File
# Begin Source File

SOURCE=.\Command48_PropSheet.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\ddbDevice.h
# End Source File
# Begin Source File

SOURCE=.\DDLBase.h
# End Source File
# Begin Source File

SOURCE=.\DDLCommInfc.h
# End Source File
# Begin Source File

SOURCE=.\DDLDevice.h
# End Source File
# Begin Source File

SOURCE=.\DDLEDisplay.h
# End Source File
# Begin Source File

SOURCE=.\DDLMain.h
# End Source File
# Begin Source File

SOURCE=.\DDLMenu.h
# End Source File
# Begin Source File

SOURCE=.\DDLMenuItem.h
# End Source File
# Begin Source File

SOURCE=.\DDLMethod.h
# End Source File
# Begin Source File

SOURCE=.\DDLVariable.h
# End Source File
# Begin Source File

SOURCE=.\DevCommStatPropPage.h
# End Source File
# Begin Source File

SOURCE=.\DeviceDetails.h
# End Source File
# Begin Source File

SOURCE=.\DeviceStatus.h
# End Source File
# Begin Source File

SOURCE=.\DevStatPageOne.h
# End Source File
# Begin Source File

SOURCE=.\Dyn_Controls.h
# End Source File
# Begin Source File

SOURCE=.\Dyn_DialogItemEx.h
# End Source File
# Begin Source File

SOURCE=.\EDisplayDlg.h
# End Source File
# Begin Source File

SOURCE=.\ErrLogView.h
# End Source File
# Begin Source File

SOURCE=.\FacePlate.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCell.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCellBase.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\GridCtrl.h
# End Source File
# Begin Source File

SOURCE=..\APPsupport\HARTDE\HARTDECommInfc.h
# End Source File
# Begin Source File

SOURCE=.\HartTrans.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\InPlaceEdit.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MainListCtrl.h
# End Source File
# Begin Source File

SOURCE=..\..\CommonClasses\FlexGrid\MemDC.h
# End Source File
# Begin Source File

SOURCE=.\MethodDlg.h
# End Source File
# Begin Source File

SOURCE=.\MethodEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\MethodsDefs.h
# End Source File
# Begin Source File

SOURCE=.\MethodSupport.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\NamedPipe.h
# End Source File
# Begin Source File

SOURCE=.\paredit.h
# End Source File
# Begin Source File

SOURCE=.\PrefDialog.h
# End Source File
# Begin Source File

SOURCE=.\Preferences.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RespCodeDevStat.h
# End Source File
# Begin Source File

SOURCE=.\SButton.h
# End Source File
# Begin Source File

SOURCE=.\SDC625.h
# End Source File
# Begin Source File

SOURCE=.\SDC625_drawTypes.h
# End Source File
# Begin Source File

SOURCE=.\SDC625_HiddenView.h
# End Source File
# Begin Source File

SOURCE=.\SDC625Doc.h
# End Source File
# Begin Source File

SOURCE=.\SDC625ListView.h
# End Source File
# Begin Source File

SOURCE=.\SDC625TreeView.h
# End Source File
# Begin Source File

SOURCE=.\SDC625WindowView.h
# End Source File
# Begin Source File

SOURCE=.\SdcView.h
# End Source File
# Begin Source File

SOURCE=.\Server.h
# End Source File
# Begin Source File

SOURCE=.\StaticImage.h
# End Source File
# Begin Source File

SOURCE=.\Table_ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\URL_LinkButton.h
# End Source File
# Begin Source File

SOURCE=.\UsefulSplitterWnd.h
# End Source File
# Begin Source File

SOURCE=.\VariableDlg.h
# End Source File
# Begin Source File

SOURCE=.\VersionNo.h
# End Source File
# Begin Source File

SOURCE=.\WaoDlg.h
# End Source File
# Begin Source File

SOURCE=.\WaveEditDlg.h
# End Source File
# Begin Source File

SOURCE=.\Window_ChildFrm.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap_2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00022.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor2.cur
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\LED_ICON_01.ico
# End Source File
# Begin Source File

SOURCE=.\res\LED_ICON_02.ico
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\off1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\off1a.bmp
# End Source File
# Begin Source File

SOURCE=.\res\on1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\on1a.bmp
# End Source File
# Begin Source File

SOURCE=.\res\SDC625.ico
# End Source File
# Begin Source File

SOURCE=.\res\SDC625.rc2
# End Source File
# Begin Source File

SOURCE=.\res\SDC625Doc.ico
# End Source File
# Begin Source File

SOURCE=.\res\smallicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar_disabled.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar_Enabled.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\SDC625.reg
# End Source File
# Begin Source File

SOURCE=..\VersionMacro.DSM
# End Source File
# End Target
# End Project
