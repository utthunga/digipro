#if !defined(AFX_SBUTTON_H__6067694A_A6A2_4358_96F3_2AE7A7DABDBA__INCLUDED_)
#define AFX_SBUTTON_H__6067694A_A6A2_4358_96F3_2AE7A7DABDBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSButton window

class CSButton : public CButton
{
	DECLARE_DYNAMIC(CSButton)

// Construction
public:
	CSButton();

// Attributes
public:

protected:

// Operations
public:

protected:
	void SetToolTipText(CString sTip=_T(""));
	void SizeToContent();
	LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSButton)
	protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSButton)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT/*void PAW 03/03/09 */OnMouseHover(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnMouseLeave(WPARAM wparam, LPARAM lparam);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	CToolTipCtrl m_ToolTip;
	CString m_strLabel;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SBUTTON_H__6067694A_A6A2_4358_96F3_2AE7A7DABDBA__INCLUDED_)
