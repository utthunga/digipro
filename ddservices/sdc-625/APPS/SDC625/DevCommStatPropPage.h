#if !defined(AFX_DEVCOMMSTATPROPPAGE_H__72184A3E_1AC3_4324_B1E0_3BEEA81CE2EA__INCLUDED_)
#define AFX_DEVCOMMSTATPROPPAGE_H__72184A3E_1AC3_4324_B1E0_3BEEA81CE2EA__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DevCommStatPropPage.h : header file
//
#include "DDLVariable.h"
#define MAX_STATUS_BITS 8
#define MAX_COMM_STATUS_BITS 8		//Modifed by ANOOP 17FEB2004
#define MAX_RANGE_VAL 20


/////////////////////////////////////////////////////////////////////////////
// CDevCommStatPropPage dialog
typedef struct cmd48_response//cmd48_response
{
	unsigned int nVal;
	CString szDesc;
	bool bIsDisplayed;
	CString szByteIdentity;  //Added by ANOOP
} CMD48_RESPONSE;

class CDevCommStatPropPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CDevCommStatPropPage)

// Construction
/*	CStatic m_StaticMsg;
	BOOL bSuppress_MsgDisplayed;
	BOOL bStaticCtrl_Created;	*/
public:
	CDevCommStatPropPage();
	CDevCommStatPropPage(CDDLVariable *pVarDevStatus);
	CDevCommStatPropPage(CDDLVariable *pVarDevStatus,	CDDLVariable *pVarCommStatus);
	~CDevCommStatPropPage();
	BOOL OnUpdateDeviceStatus(unsigned char); 
	BOOL OnUpdateCommrespStatus(unsigned char); 
/*	BOOL OnAddSuppressMessage();
	BOOL OnRemoveSuppressMessage();	*/

// Dialog Data
	//{{AFX_DATA(CDevCommStatPropPage)
	enum { 
			IDD_ONE = IDD_COMMUNI_RESP_STATUS_PROP_PAGE,
			IDD_TWO = IDD_DEVICE_STATUS_PROP_PAGE
		};
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

	BOOL bCommStatusDisplayed;
	BOOL bDeviceStatusDisplayed;
	CDDLVariable *m_pVarDevStatus;
	CDDLVariable *m_pVarCommStatus;
#ifdef GE_BUILD
	virtual void OnOK();	// 00.01.09
	int nPage;
	#define TOTAL_PROP_PAGES 25  //Added by ANOOP
	CString szByteId[TOTAL_PROP_PAGES];
	unsigned char num_buttons;
	void UpdatePageInfo(void);
	// PAW 04/06/09 end
#endif
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDevCommStatPropPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDevCommStatPropPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BYTE byArrPreviousCommResp[8];	//Modifed by ANOOP 17FEB2004
	BYTE byArrPresentCommResp[8];	//Modifed by ANOOP 17FEB2004

	BYTE byArrPreviousDeviceResp[8];
	BYTE byArrPresentDeviceResp[8];
	

	CBitmapButton *m_pBtnArray_DevStat[MAX_STATUS_BITS];
	CStatic *m_pStaticArray_DevStat[MAX_STATUS_BITS];
	
	CBitmapButton *m_pBtnArray_Commn[MAX_STATUS_BITS];
	CStatic *m_pStaticArray_Commn[MAX_STATUS_BITS];
	BOOL bDeviceStatus;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVCOMMSTATPROPPAGE_H__72184A3E_1AC3_4324_B1E0_3BEEA81CE2EA__INCLUDED_)
