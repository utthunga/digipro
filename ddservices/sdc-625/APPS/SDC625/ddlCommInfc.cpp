    /*********************************************************************************************
     *
     * $Workfile: ddlCommInfc.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
     **
     *********************************************************************************************
     * The content of this file is the 
     *     Proprietary and Confidential property of the HART Communication Foundation
     * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
     *********************************************************************************************
     *
     * Description:
     *		docCommInfc.cpp : implementation file
	 *
	 * stevev 18jan05 - added special thread - see notes and thread at the end of the module
     */
    
#define SUPPORT_HART_SERVER 1
/**/

    #include "logging.h"
    #include "stdafx.h"
    #include "SDC625.h"
	#include "messageUI.h"	/* added 02jan08 - stevev */
    
    // For ATL's variant support
    #include <atlbase.h>
#if _MSC_VER < 1400 /* pre 2005 */
    #include <atlimpl.cpp>
#endif
    #include <objbase.h>
    
    #include "HModemSvr.h"
    #include "HModemSvr_i.c"
    
    #include "IHartComm_i.c"
    
    #include "HUtil_i.c"
    
#include "HSelect.h"

    #include "DDLCommInfc.h"
    #include "DDLCommErr.h"
    
  // timj begin 4/5/04
  #include "HartServer_i.c"
  #include "HSelect_i.c"
  #include "Preferences1.h"		
  // timj end 4/5/04
  
  
    #include "registerWaits.h"
    
    #include "HARTsupport.h"
    
    /*Vibhor 040204: Start of Code*/
    
    #include "Mainfrm.h"
    
    /*Vibhor 040204: End of Code*/
    
    #ifdef _DEBUG
    #define new DEBUG_NEW
    #undef THIS_FILE
    static char THIS_FILE[] = __FILE__;
    
    #endif
    
  //#define DEBUG_TRANSACTION
/**** moved to DDLBaseComm.cpp 25jan06 - stevev ***    
 **   const char* HartException::except1020 = "Unable to initialize communications COM";
 **   const char* HartException::except1021 = "Unable to get Hart Utilities dll";
 **   const char* HartException::except1022 = "Unable to get Hart Modem Connection";
 **   const char* HartException::except1023 = "Unable to get Hart Server Connection";
 **   const char* HartException::except1024 = "Unable to get Hart Select dll";
 **/   
  /******************************Vibhor 180304: Start of Note ******************************
   Now we are updating the Device Condition dialog box on timer (if it has atleaset one
   dynamic var). So suppression message on extended tabs isn't required anymore.
   For this the calls to AddSuppressMessageTab and UpdateSuppressMessageTab have been
   commented out
  ******************************Vibhor 180304: End of Note *********************************/
  

class COMthread : public COMptr_t
{
public:

	/* stevev 18jan05 - for handshakes with thread */
	hCevent     m_evtInitialized;// from COM init thread (handshake: started & ended)
	hCevent     m_evtShutdownNow;// to   COM init (and shutdown) thread

	CWinThread*     pCOMthread;  // this thread OWNS the COM objects

	// implementation
public:
	COMthread():COMptr_t(), pCOMthread(NULL){};
	~COMthread();

	DWORD connectComm(CbaseComm* pBaseComm);// returns 0=success
	DWORD disConnectComm(CbaseComm* pBaseComm);

	static DWORD    conCOM(COMptr_t* p2sPtrs);// used by threads to initialize
	static DWORD disconCOM(COMptr_t* p2sPtrs);

};

static COMthread COMcontainer;// singleton!!!!!!!

// stevev 9sep10 - 
void killContainer(void)
{
	//try
	COMcontainer.~COMthread();// stop it without deleting it...
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"COMthread.COMcontainer - shutdown called\n");
}
/* stevev 18jan05 forward decl for new thread */
static UINT initializeCOMMultiThreaded( LPVOID pParam );

/*************************************************************************************************
*  Call back for the registered tasks
*  These are wrappers for task's global C callback to get to the class's task methods
*
*/

//////////////////////////////////////////////////////////////////////////////////////////////////
// This is the real-time communications task. It triggers when an object becomes ready in 
//	the packet queue.  It then sends the packet and waits for a reply
VOID CALLBACK gfWaitAndTimerCallback(
      PVOID   lpParameter,      // thread data(Context)
      waitStatus_t TimerOrWaitFired)	// reason -- true == Timeout/death, 
										//			false == event signaled
{	
    CdocCommInfc* pCommInfc = (CdocCommInfc*)lpParameter;
    // execute this class's send event callback
    if (pCommInfc != NULL)
    {
    	pCommInfc->commInfcTask(TimerOrWaitFired);// will send the packet in proper form 
    }
    else
    {
		LOGIT(CERR_LOG,"> gfWaitAndTimerCallback has a null comm Interface ptr. (7x)\n");
    }
    // exit to loop and wait again forever
    return;
}


    
    /////////////////////////////////////////////////////////////////////////////
    // CdocCommInfc - this merely instaniates it, call init to initialize
																	// # initial pkts
    CdocCommInfc::CdocCommInfc(CDDIdeDoc* pD) : /*m_pDoc(pD),*/ thePktQueue()
    {
		m_pDoc = pD;
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.constructor - Entry\n");

    	m_thisIdentity.clear();
    	m_commHndl   = 0;
    	m_pollAddr   = 0xff;
    	pComm        = NULL;
    	pUtil        = NULL;
  		pServer		 = NULL;
  		pSelect      = NULL;
  		pSelect3     = NULL;
    	m_connected  = false;
		m_pending    = false;
    	/*
    	m_enableComm = true;  // set to false to start the shutdown
    	m_CommDisabled=false; // set true when shutdown complete
    	*/
    	m_CommStatus = commInitialize;// all of comm state rolled into one
    	m_hadConfigChanged  = false;
    	m_skipCount         = 0;
    
    	hRegisteredTask     = 0;
    
    /*Vibhor 120204: Start of Code*/
    	
    	m_bMoreStatusSent = false;
    	
    /*Vibhor 120204: Start of Code*/
    
    /* stevev 2/12/04 - try to get light working */
    	m_pMainFrm = NULL;
  // stevev 4/2/4::
  		lastDRstate = 0;// no delayed response
  		rePollCount = 0;
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.constructor -  Exit\n");

		thePktQueue.initialize(DOCCOMMQSIZE);
    }
    
CdocCommInfc::~CdocCommInfc()
{   // Cleanup
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.destructor - Entry\n");

	int c = 0;
	while (m_pending)
	{
		Sleep(25);
		c++;
	}

	if ( pComm != NULL || m_connected )
	{
		shutdownComm();
	}
//    if (hRegisteredTask != 0)
//    {
//    	UnregisterWait( hRegisteredTask ) ;
//    	hRegisteredTask = 0;
//    }
//	disconnect(); // just in case
  //VMKP 260304
  	thePktQueue.destroy();
  //VMKP 260304

//	pComm   = NULL;
//	pUtil   = NULL;
//	pServer = NULL;
//	pSelect = NULL; 
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.destructor -  Exit\n");
}
    
/****** the following code was moved to DDLBaseCOmm.cpp 25jan06 **************************** 
	removed from here 23may11 -stevev- see earlier version for contemts
 ****************************************** end code move 25jan06 **************************/
/*Vibhor 120204: Start of Code*/

// this is an asycronous callback from the dispatcher
// the dispatcher handles the internal data acquasition and then calls this function    
void  CdocCommInfc::notifyRCDScmd48(hMsgCycle_t& thisMsgCycle)
{
    RETURNCODE rc = SUCCESS;
    unsigned int cmdNo;		//stevev 20sep07-need wide command numbers...
    tstring   hs;
    hCitemBase* pIB = NULL;
    hCVar* pVr = NULL;
    hCBitEnum *pBe = NULL;
    BYTE byCommResp = 0;

        /*stevev - 2-13-04 modified Vibhor's NotifyCmd48 code */
        // just in constructor   bool m_bMoreStatusSent = false;

        /*<START>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/
/*Vibhor 190304: Start of Code */
/*      if(m_skipCount == 0)
        {
          if(NULL != m_pDoc->ptrCMD48Dlg) // Reset the suppression flag 
            {
                m_pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();
            }
        }
*/
/*Vibhor 190304: End of Code*/
        /*<END>27FEB2004 Added by ANOOP since the message suppressed was not getting displayed properly.*/
        
        if ( thisMsgCycle.pCmd )
        {
            cmdNo = thisMsgCycle.pCmd->getCmdNumber();
        }
        else
        {
            cmdNo = 0xff;
        }
        /* temporary */
        LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"     Cmd:%03d RC:0x%02X  DS:0x%02X\n", 
                                                cmdNo, thisMsgCycle.respCdVal, thisMsgCycle.devStatVal);
        
        if (thisMsgCycle.respCdVal != 0)
        {
            if (thisMsgCycle.respCdVal < 0x80)// isa response code
            {
				unsigned long t = 0;
				unsigned long c = thisMsgCycle.respCdVal;// stevev 4sep07 - moved from below 
                //hCRespCodeList        respCds;
                for ( hCRespCodeList::iterator iT = thisMsgCycle.respCds.begin(); 
                                                                iT < thisMsgCycle.respCds.end(); iT++)
                {// iT isa ptr2a hCrespCode
                    if (iT->getVal() == c)// stevev 4sep07 - was::>  thisMsgCycle.respCdVal)
                    {
	// stevev - WIDE2NARROW char interface \/ \/ \/
#ifdef _UNICODE
                        hs = iT->getDescStr();
#else
                        hs = iT->get_DescStr();
#endif
						t  = iT->getType();// stevev 4sep07 - moved from below to only fill when valid
						break; // out of for loop
                    }
                }// next response code
/*Vibhor 220104: Start of Code*/
/*Decided fix for PAR 5299 "Command Not Implemented not being logged",
In realtime this won't generally happen,and its more intended towards,
the DD Developer usage of SDC. Since the standard response codes are
not available anywhere, we need to just log the response code returned. 
*/
                if (hs.empty()) // not found
                {
					hs = M_UI_NO_RESP_CD;
					/* the stuff that was here gets done in the next block */
                }
				// else use what we found above
/*Vibhor 220104: End of Code*/
				
				TCHAR x[32];
				_stprintf(x,M_UI_NO_RESP_CD_FORMAT,cmdNo,thisMsgCycle.respCdVal);
				hs += x;

                /*Vibhor 060204: Start of Code */
                /*Decided that we'll just show a Message Box for RespCode and Comm Status*/

				/* stevev 8feb05 - if the response code has no type (==0) then we will 
					set it according to spec 307 */
				// moved it above to only fill when valid::>unsigned long t = iT->getType();
				// moved it above ::>unsigned long c = thisMsgCycle.respCdVal;
				if (t==0)
				{/* code moved to response code static so all can use */
					t = hCrespCode::getType((unsigned char)(c & 0x00ff));
				}  
                /*Vibhor 060204: End of Code */
                LOGIT(CLOG_LOG|UI_LOG,_T("%s\n"), hs.c_str());
				hs += M_UI_CONTINUE_QUEST;

				/* it has a type: warnings do not pop up a box, they just get Logged*/
										// stevev 5sep07-use appCommEnabled to try & stop re-entry
				if ( RESPCD_ISERROR(t) && appCommEnabled())
				{	// stevev 9aug07 - fix for par 709
					int r = AfxMessageBox(hs.c_str(),MB_YESNO | MB_ICONEXCLAMATION  );
					if ( r == IDCANCEL   || r == IDNO  )
					{	
						CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd; 		
						HWND hwndMainWindow = pMainWnd->GetSafeHwnd(); 

    					if (hwndMainWindow != NULL)
    					{
							// stevev 5sep07- - turn some stuff off first
							disableAppCommRequests();
							DEBUGLOG(CLOG_LOG,"Notify cmd 48 disables appComm\n");
    						::PostMessage(hwndMainWindow, WM_SDC_CLOSEDEVICE, NULL, NULL);
    					}
					}
				}  


                /* VMKP 300104 Uddate the Response code here (7 bits of the first byte).
                  This will come only when No communicaton bit is set in the response
                  code */
                if(NULL != m_pDoc->ptrCMD48Dlg)
                {
                    //Commented by ANOOP since we are not displaying the response code
/*Vibhor 220204: Start of Code*/
//Uncommented : This will clear the Comm Status Tab
                    m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(0);//sjv - changed to zero
/*Vibhor 220204: End of Code*/
                }       
                
            }
            else // isa comm error
            {// symbol DEVICE_COMM_STATUS
/*Vibhor 060204: Start of Code*/
                byCommResp = thisMsgCycle.respCdVal & ~ 0x80;

                rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM_STATUS, &pIB);
                if ( rc == SUCCESS && pIB != NULL )
                {
                    pVr = (hCVar*) pIB; // a cast operator
                    if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
                    {// make it a bit enum
                        pBe = (hCBitEnum*)pVr;
                        if (pBe)
                        {
                            unsigned char msk = 1;
                            do
                            {// for each bit set
                                if ( byCommResp & msk )
                                {
                                    rc = pBe->procureString(msk, hs);
                                    if ( rc == SUCCESS && ! hs.empty() )
                                    {// log the string
                        
                                        AfxMessageBox(hs.c_str());
                        
                                        LOGIT(UI_LOG, "Comm Status: %s",hs.c_str());
                                    }
                                    else
                                    {
                                        TCHAR strBuff[85];
                                        _stprintf(strBuff,M_UI_NO_RESP_CD_4_BIT, msk,cmdNo);
                                        AfxMessageBox(strBuff);

                                        LOGIT(UI_LOG,"No Description Avaliable for %d in Comm Status",msk);
                                    }
                                }// else - not set, don't output string
                            }while( (msk<<=1)  != 0);
                        }
                        else
                        {
                            LOGIT(CERR_LOG,"Comm Status: failed to cast from hCVar.");
                        }
                    }
                    else
                    {
                        LOGIT(CERR_LOG,"Comm Status: failed to cast from hCItemBase to bit enum.");
                    }

                }/*Endif rc = SUCCESS*/

                LOGIT(CLOG_LOG|UI_LOG,"     Cmd:%d  RC:0x%02X Comm error response code",cmdNo, thisMsgCycle.respCdVal);
                /* VMKP 300104 Uddate the Communication status (7 bits of the first byte).
                  This will come only when communicaton bit is set in the response
                  code */
                if(NULL != m_pDoc->ptrCMD48Dlg)
                {
                    m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(byCommResp);  //MOdifed by ANOOP17FEB2004
                }
            }/* end else  sa comm error */

        }/*endif thisMsgCycle.respCdVal != 0*/
        else
        {
    //Resp Code = 0 , so no Comm Error,clear the comm lights in dialog  , this clears previously set bits if, any
            if(NULL != m_pDoc->ptrCMD48Dlg)
            {
                m_pDoc->ptrCMD48Dlg->UpdateCommStatusOnNotification(thisMsgCycle.respCdVal);
            }

        }

        if ( ! m_hadConfigChanged )
        {// haven't had a config changed
            if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
            {// haven't had one, have one now
LOGIT(CLOG_LOG,"CONFIG: new Config Changed.\n");
                // leave it set, it'll be handled later
                /* stevev - 2/17/04 - callback to mainframe for event */
                if (m_pMainFrm)
                {
					rc = m_pMainFrm->ConfigChanged();
     				if ( rc == APP_CMD_RESPCODE_ERR )// unexpected but handled
					{// clear it for further processing
						thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); 
					}
					else
     				if (rc == SUCCESS )// expected and handled
/* hold for now - - - - - - - - - - - - - - - - - - - - - - - 
//LOGIT(CLOG_LOG,"------->Config Changed in notifyRCDScmd48. Doing ConfigChanged.\n");
!>                     if (m_pMainFrm->m_MainFrameState == FS_ISCOMMITTING)// it is expected
!>                     {
!>                         thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); // don't pop-up for it
!>                         // leave ! m_hadConfigChanged
!>                     }
!>                     else
- - - - - - - - - - - - - - - - - - - - - - - */    
                    {                       
                        m_hadConfigChanged = true;      // event - config changed to on
						// clear it for further processing
						thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); 
/* hold for now - - - - - - - - - - - - - - - - - - - - - - - 
 
!>                         / * VMKP added on 020404:  Rebuild the menu if unexpected Config change
!>                                  bit is set,  May be a secondary configuration might have changed
!>                                 something in the device * /
!>                         // else we don't have the pointers to do this yet
!>                         HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName);
!>                         if (hwndMainWindow != NULL)
!>                         {// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
!> 
!>                             ::PostMessage(hwndMainWindow, WM_SDC_STRUCTURE_CHANGED, NULL, NULL);
!>                         }
!>                         / * VMKP added on 030204 * /
!> 
!>                     }
!>                     if (m_pMainFrm->ConfigChanged() != SUCCESS )// let it deal with it
!>                     {
!>                         m_hadConfigChanged = false;     // event - config changed to on 
- - - - - - - - - - - - - - - - - - - - - - - */
                   }
                    //else - couldn't handle it; leave it so we will deal with it later
                }
                /* stevev - 2/17/04 - end */
            }
            // else - haven't had one, still don't have one
            // no operation
        }
        else 
        {// had one, filter the configuration changed bit
            if (thisMsgCycle.devStatVal & DS_CONFIGCHANGED)
            {// had one, have one now
LOGIT(CLOG_LOG,"CONFIG: another Config Changed.\n");
                thisMsgCycle.devStatVal &= (~DS_CONFIGCHANGED); // clear it for further processing
            }
            else // had one, don't have one now
            {
                LOGIT(UI_LOG|CLOG_LOG,"CONFIG: Configuration Changed bit has Cleared.\n");
				if (m_pMainFrm)
                {
     				m_pMainFrm->ConfigUNChanged();
				}
                m_hadConfigChanged = false;
                if(NULL != m_pDoc->ptrCMD48Dlg)
                {
                    m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
                }
            }
        }
        if (m_pDoc == NULL || m_pDoc->pCurDev == NULL)
        {
            LOGIT(CERR_LOG,"ERROR: comm interface call without a path to the device.\n");
            return;
        }

        if((m_bMoreStatusSent == true) && (cmdNo == 48))
        {
            m_bMoreStatusSent = false;
        }
        else //either bMoreStatus == false OR cmdNo != 48
        {
            if((m_skipCount == 0) && (thisMsgCycle.devStatVal != 0))
            {
                //If Dialog exists update Device Status
    			if(NULL != m_pDoc->ptrCMD48Dlg)
    			{
    				m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
    			}
                rc = m_pDoc->pCurDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
                if( rc == SUCCESS && pIB != NULL )
                {
                    pVr = (hCVar*) pIB; // a cast operator
                    if ( pVr != NULL && pVr->VariableType() == vT_BitEnumerated)
                    {// make it a bit enum
                        pBe = (hCBitEnum*)pVr;
                        if (pBe)
                        {
                            unsigned char msk = 1;
                            do
                            {// for each bit set
                                if ( thisMsgCycle.devStatVal & msk )
                                {
                                    if(msk == DS_MORESTATUSAVAIL   && 
                                       thisMsgCycle.devStatVal != DS_MORESTATUSAVAIL)//stevev prevent no popup
                                    {
                                        continue; // loop the while
                                    }
                                    rc = pBe->procureString(msk, hs);
                                    if ( rc == SUCCESS && ! hs.empty() )
                                    {// log the string
                                        
                                        m_pDoc->m_strDisplay = hs.c_str();
                        
                                        CMainFrame *pMainWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd; 

                                        pMainWnd->SendMessage(WM_NOTIFY_RESPCODE);
										//stevev 19jan05 - try post //pMainWnd->PostMessage(WM_NOTIFY_RESPCODE);

                                        if(true == m_pDoc->m_bIgnoreRespCode)
                                        {
                                            m_skipCount = m_pDoc->m_uSuppressionCnt;
                                            if(true == m_pDoc->m_bIgnoreRespCode)
                                            {           
                                                /* VMKP 300104 add an indicator in the extended device status
                                                tab saying "Updations Suppressed"  */
/*Vibhor 180304: Start of Code*/
/*                                              if(NULL != m_pDoc->ptrCMD48Dlg)
                                                {
                                                 m_pDoc->ptrCMD48Dlg->UpdateSuppressMessageTab();    //27FEB2004 Modified  by ANOOP since the message suppressed was not getting displayed properly
                                                }
*/
/*Vibhor 180304: End of Code*/
                                            }

                                            break;
                                        }

                                        LOGIT(UI_LOG,"Device Status: %s",hs.c_str());
                                    }
                                    else
                                    {
                                        LOGIT(UI_LOG,"No Description Avaliable for %d in Device Status",msk);
                                    }
                                }// else - not set, don't output string
                            }while( (msk<<=1)  != 0);
                        }
                        else
                        {
                            LOGIT(CERR_LOG,"Device Status: failed to cast from hCVar.");
                        }
                    }
                    else
                    {
                        LOGIT(CERR_LOG,"Device Status: failed to cast from hCItemBase to bit enum.");
                    }           
                }//Endif rc == SUCCESS

                if(thisMsgCycle.devStatVal & DS_MORESTATUSAVAIL)// We aren't in our owm Command 48 reply
                {
                    //Send Command 48
                    //stevev - rc = m_pDoc->pCurDev->sendMethodCmd(48,-1);
                    rc = m_pDoc->pCurDev->sendCommand(48,-1);
                    m_bMoreStatusSent = true;

                }/*Endif DS_MORESTATUSAVAIL*/

            }
            else //either m_skipCount != 0 OR thisMsgCycle.devStatVal = 0
            {

                if(thisMsgCycle.devStatVal == 0)
                {
                    m_skipCount = 0;
                    
                    if(NULL != m_pDoc->ptrCMD48Dlg)
                    {           
                        m_pDoc->ptrCMD48Dlg->UpdateDeviceStatusOnNotification(thisMsgCycle.devStatVal);
                    }
/*Vibhor 180304: Start of Code*/
/*                  if(NULL != m_pDoc->ptrCMD48Dlg)
                    {
                        m_pDoc->ptrCMD48Dlg->RemoveSuppressMessageTab();

                    }
*/
/*Vibhor 180304: End of Code*/
                }
                else //simply decrement the skipCount
                {
                    m_skipCount--;
                }

            }

        }/*End else  bMoreStatus == false OR cmdNo != 48*/

        if(cmdNo == 48)
        {

          LOGIT(CLOG_LOG,"Received Command 48 response");
            // need to expand the bits here

        /* VMKP 300104 Update the Command 48 response bytes (Extended Device status bytes)
            here, if u reach here you need to remove the "supressions removed" message
            from the extended device status tab and the update it */
            if(NULL != m_pDoc->ptrCMD48Dlg)
            {           
/*Vibhor 130204: Start of Code*/    
            //was   HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Status"));

                HWND hwndCMD48Window = ::FindWindow(NULL,_T("Device Condition"));
/*Vibhor 130204: End of Code*/  
                if(NULL != hwndCMD48Window)  //Added by ANOOP Fix for crash 15FEB2004
                {
                    m_pDoc->ptrCMD48Dlg->m_hWnd=hwndCMD48Window;

                    if(false == m_pDoc->ptrCMD48Dlg->m_ExtendedTabCreated )
                    {
                        ::PostMessage(hwndCMD48Window, WM_CREATE_EXTENDEDTAB, NULL, NULL);
                    }
                    else
                    {
                        ::PostMessage(hwndCMD48Window, WM_UPDATE_EXTENDEDTAB, NULL, NULL);
                        
                    }
    
                }
            
            }
            
        }

            
}/*End notifyRCDScmd48*/

/*Vibhor 120204: End of Code*/
    
    // gets an empty packet for generating a command 
    //		(gives caller ownership of the packet*)
    //		pass back ownership with Sendxxx OR putMTPacket if command send aborted
    //
    hPkt* CdocCommInfc::getMTPacket(bool pendOnMT)
    {
    	hPkt* pR = thePktQueue.GetEmptyObject(pendOnMT);
    //clog <<"got  MT   Pkt "<<hex << pR   << dec << endl;
    	return pR;
    }
    			
    // returns the packet memory back to the interface 
    //		(caller relinguishes ownership of the packet*)
    //							
    void  CdocCommInfc::putMTPacket(hPkt* pPkt)
    {	
    //clog <<"return MT Pkt "<<hex << pPkt << dec << endl;
        thePktQueue.ReturnMTObject( pPkt );
    }
    
    
    RETURNCODE 
    CdocCommInfc::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
    {
    	RETURNCODE rc = SUCCESS;
    
    	hPkt* pPkt = getMTPacket() ;
    
    	//if ( pPkt != NULL && m_enableComm )
    	if ( pPkt != NULL && (m_CommStatus < commNC && m_CommStatus > commInitialize) )
    	{// OK or NoDev
    		pPkt->cmdNum = cmdNum;
    		memcpy(pPkt->theData,pData,dataCnt);
    		pPkt->dataCnt = dataCnt;
    		return (SendPkt(pPkt, timeout, cmdStatus));
    	}
    	else
    	{
    		return DDL_COMM_QUEUE_FAILURE;
    	}
    
    }
    
    
    // puts a packet into the send queue 	(takes ownership of the packet*)
    RETURNCODE CdocCommInfc::SendPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
    {
    	RETURNCODE rc = SUCCESS;
    					
    	if ( m_CommStatus > commNoDev ) // NC,shuttingdown,shutdown  was: ! m_enableComm )
    	{
    		thePktQueue.ReturnMTObject( pPkt );
    		LOGIT(CLOG_LOG,"  SendPkt has Comm shutting down.\n");
    		return DDL_COMM_SHUTDOWNINPROGRESS;
    	}
    
    	if ( pPkt == NULL)
    	{
    		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: SendPkt has a null for a packet - discarding.\n");
    		rc = DDL_COMM_PARAMETER_ERROR;
    	}
    	else
    	{
    		pPkt->timeout = timeout;
    		// update Packet Log here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!XMIT
    #ifdef DEBUG_TRANSACTION /* top of this file*/
    		clog << "Queuing cmd " <<dec<< (int)(pPkt->cmdNum) << " with 0x" << hex << 
    										(int)(pPkt->dataCnt) << dec << " bytes data."<<endl;		
    #endif
    //PutObject2Head(pPkt)
//LOGIT(CLOG_LOG,"  Putting Pkt w/ Cntl = %d\n", (int)(pPkt->m_thrdCntl.threadAction));
    		int c = thePktQueue.PutObject2Que(pPkt);// triggers event to get it sent
//LOGIT(CLOG_LOG,"pktQ now has %d objects (%d) p%08p\n", c,thePktQueue.GetQueueState(),&thePktQueue);
    	}
    	return rc;
    }
    
    
    // puts a packet into the send queue 	(takes ownership of the packet*)
    RETURNCODE CdocCommInfc::SendPriorityPkt(hPkt* pPkt, int timeout, cmdInfo_t& cmdStatus)
    {
    	RETURNCODE rc = SUCCESS;
    
    	if ( m_CommStatus > commNoDev ) // NC,shuttingdown,shutdown  was: ! m_enableComm )
    	{
    		thePktQueue.ReturnMTObject( pPkt );
    		LOGIT(CLOG_LOG,"  SendPriorityPkt has Comm shutting down.\n");
    		return DDL_COMM_SHUTDOWNINPROGRESS;
    	}
    
    	if ( pPkt == NULL)
    	{
    		LOGIT(CLOG_LOG|CERR_LOG,
				"ERROR: SendPriorityPkt has a null for a packet - discarding.\n");
    		rc = DDL_COMM_PARAMETER_ERROR;
    	}
    	else
    	{
    		pPkt->timeout = timeout;
    		// update Packet Log here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!XMIT
    #ifdef DEBUG_TRANSACTION /* top of this file*/
    		clog << "Que2Head cmd " <<dec<< (int)(pPkt->cmdNum) << " with 0x" << hex << 
    									(int)(pPkt->dataCnt) << dec << " bytes data."<<endl;		
    #endif
    		thePktQueue.PutObject2Head(pPkt);// triggers event to get it sent
    	}
    	return rc;
    }
    
    // how many are in the send queue ( 0 == empty )
    int CdocCommInfc::sendQsize(void)
    {
    	return thePktQueue.GetQueueState();
    }
    
    // how many will fit	    
    int	CdocCommInfc::sendQmax(void) 
    {// we may need to count #in queue + #still empty
    	return DOCCOMMQSIZE;
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////	
// setup connection 
//
RETURNCODE  CdocCommInfc::initComm(void)
{
    RETURNCODE rc = SUCCESS;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.initComm - Entry\n");


	if ( m_CommStatus != commInitialize )// this should never happen, if it does, we have a collision
	{
		LOGIT(CLOG_LOG|CERR_LOG,
			"ERROR: comm status NOT 'initialize' while trying to init.\n");
		return FAILURE;
	}

    rc = COMcontainer.connectComm(this);

/* removed 26aug05::
	/ * stevev 18jan05 - init moved to thread at the end of this module * /
	/ * end stevev 18jan05 */
	
//	m_CommStatus = commNC;
 
    if ( rc == SUCCESS )
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.initComm - Starting packetQueue response Task.\n");

    	// generate background task to handle the xmit packets and call our interface task
    	// using RegisterWait ForSingleObject(QueueEvent)
    	RegisterWaitForSingleObject(& hRegisteredTask, thePktQueue.GetArrivalEvent(), 
    							gfWaitAndTimerCallback,(void*)this, /*REASONABLE_TIME*/INFINITE, 
    							WT_EXECUTEDEFAULT | WT_NORM_PLUS_TWO
#ifdef _DEBUG
								,"Packet Queue communication task waiting for packet"
#endif
							);// stevev added priority
    }
	else
	{// we failed to connect
		LOGIT(CLOG_LOG|CERR_LOG,".ERROR: comm driver failed to connect.\n");
		return FAILURE;
	}


    m_hadConfigChanged  = false;
    m_skipCount         = 0;

	hPkt* pPkt = getMTPacket(false);
	if (pPkt)
	{
		cmdInfo_t cs;
		pPkt->m_thrdCntl.threadAction = hThreadCntl::thd_Init;
		rc = SendPriorityPkt(pPkt, 30000, cs);

		// init pkt is a fire-and-forget  (no handshake)
	}
	// we have to wait for it to finish tho
	int cnt = 0;
#ifndef USE_ONE_THREAD_CONTAINER
	while ( pComm == NULL && cnt < 3000 )
#else
	while ( COMcontainer.pComm == NULL && cnt < 3000 )
#endif
	{
		systemSleep(10);
		cnt++;
	}
#ifndef USE_ONE_THREAD_CONTAINER
	if ( cnt >= 3000 || pComm == NULL)
#else
	if ( cnt >= 3000 || COMcontainer.pComm == NULL)
#endif
	{
		rc = FAILURE;
	}
#ifndef USE_ONE_THREAD_CONTAINER
#else
	else
	{
		pComm    = COMcontainer.pComm;
		pUtil    = COMcontainer.pUtil;
		pSelect  = COMcontainer.pSelect;
		pSelect3 = COMcontainer.pSelect3;
		pServer  = COMcontainer.pServer;
		suppressModemSrvr = COMcontainer.suppressModemSrvr;
	}
#endif

	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.initComm -  Exit w/RC=%d\n",rc);
    return rc;
}// end initComm()



RETURNCODE  CdocCommInfc::shutdownComm(void)
{
    RETURNCODE rc = SUCCESS;
	/* note that the COM object will not be released until this class is destructed! */
	
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc-comm infc shutting down.\n");
    
    if (m_CommStatus > commNC )	// shuttingdown|shutdown  was:  ! m_enableComm)
    {// we're already shutdown, just leave
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc-comm communications interface already gone (*)\n");
    	return rc;
    }// else do the sequence
    
    /*
    m_CommDisabled =		// will be true when it's shutdown
    m_enableComm   = false; // disable any more sends
    */
    m_CommStatus = commShuttingDown;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc-comm comm status set to ShuttingDown.\n");
    
    if (hRegisteredTask != 0)
    {
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> doc-comm Stopping Communications Thread.\n");
    	_UnregisterWait( hRegisteredTask, 20000 ) ;
    	hRegisteredTask = 0;
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
								"< doc-comm Communications Thread stopped. (15)\n");
    }
    int c = 0;
    do
    {
    	systemSleep(10);
    	c++;
    }
    while (  (m_CommStatus < commShutDown )  && c < 1000 );// wait 10 secs 4 the comm infc
    
    if ( c >= 1000 )
    {
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
							"< doc-comm communications interface has not set m_CommDisabled.(18)\n");
    	LOGIT(CERR_LOG|CLOG_LOG,"ERROR: communication interface timed out "
												"while waiting for shutdown to complete.\n");
    }
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< doc-comm  communications interface is done. (19)\n");
    
    return rc;
}



RETURNCODE CdocCommInfc::GetIdentity(Indentity_t& retIdentity, BYTE pollAddr)
{
    RETURNCODE rc = FAILURE;
	bool comInited= false;
	hPkt* pPk = NULL;
	cmdInfo_t cif;
	hCevent*  pEv = new hCevent;
	hCevent::hCeventCntrl* pec = pEv->getEventControl();

// timj begin 4/6/04  pollAddr == 0xff is legitimate
 	if ( pComm == NULL || m_pDoc == NULL  || 
		 ( (pollAddr >= DOCCOMMMAXPOLLADDR) && (pollAddr != EMPTY_POLL_ADDR) )
	   )
// timj end   4/6/04  pollAddr == 0xff is legitimate
    {
    	return DDL_COMM_PARAMETER_ERROR;
    }    

    retIdentity.clear();

/* timj - removed 3/19/04 to accomdate HART Server 
//	m_pollAddr   = m_pDoc->m_copyOfCmdLine.cl_PollAddr;

    VARIANT_BOOL isPresent;
    CComBSTR     bstrCommPort(m_pDoc->m_copyOfCmdLine.cl_Url);
    HRESULT hr = pComm->IsInstrumentAtPollAddress(bstrCommPort, pollAddr, &isPresent);

    if (isPresent) 
    {
    	m_pollAddr = pollAddr;
*/
/***
	DWORD pktThread;
 	if ( hRegisteredTask != 0 )
	{
		pktThread = GetWaitThreadID( hRegisteredTask);
		if ( pktThread != GetCurrentThreadId() )
		{// temporaraly init multithreaded to get the info - If we are not using COM this
		 // should have no effect what-so-ever.
			HRESULT hR = CoInitializeEx(NULL, COINIT_MULTITHREADED);
			if (  FAILED( hR ) )
			{
				if ( hR = RPC_E_CHANGED_MODE )
				{
				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: GetIdentity COM-wrong Mode\n");
				}
				else
				if ( hR = S_FALSE )
				{
				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: GetIdentity COM already done\n");
				}
				else
				{
				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: GetIdentity could not initialize COM\n");
				}
    		}
			else
			{
				comInited = true;
			}
		}
	}
****/
	rc  = FAILURE;
	pPk = getMTPacket();
	if (pPk != NULL)
	{
    	pPk->cmdNum = 0;
    	pPk->dataCnt= 0;
		pPk->m_thrdCntl.threadAction = hThreadCntl::thd_Connect;
		pPk->m_thrdCntl.threadLPARAM = pEv;
	//	LOGIT(CLOG_LOG,"  Ident sending Connect packet.\n");
		rc = SendPkt(pPk, 0, cif);
		m_pending = true;
		rc = pec->pendOnEvent();
		m_pending = false;
	//	LOGIT(CLOG_LOG,"  Ident Got Connect packet event.\n");
		putMTPacket(pPk);
	}
	else
	{ LOGIT(CERR_LOG,"No packet from getMTPacket()\n");

//    if ( (rc = connect()) == SUCCESS )
//    {
// 		LOGIT(CLOG_LOG,"Successful connection at poll address %d\n", (int)m_pollAddr);
//    }
//    else
//    {// connection failure
//    	LOGIT(CERR_LOG,"ERROR: Device Identity could not be established.\n");
    }
/* timj - removed 3/19/04 to accomdate HART Server 
    }
    else
    {
    	string erStr;
    	getErrorStr(hr, erStr);

  	cerr << "ERROR: Device is not at Poll Address " << (int)m_pollAddr << endl;
    	cerr << "     : " << erStr << endl;
    	rc = DDL_COMM_DEVICE_NOT_AT_POLL;
    }
*/


    if ( rc == SUCCESS && m_connected )
    {// aquire identity
//    	hPkt xP, rP;
//    	xP.cmdNum = 0;
//    	xP.dataCnt= 0;
//
//	rc = rawSend(&xP, &rP);// stevev now logs both ways

		rc  = FAILURE;
		pPk = getMTPacket();
		if (pPk != NULL)
		{
    		pPk->cmdNum = 0;
    		pPk->dataCnt= 0;
			pPk->m_thrdCntl.threadAction = hThreadCntl::thd_RawSnd;
			pPk->m_thrdCntl.threadLPARAM = pEv;
		//	LOGIT(CLOG_LOG,"  Ident Sending Cmd 0 packet.\n");
			rc = SendPkt(pPk, 0, cif);
			m_pending = true;
			rc = pec->pendOnEvent();
			m_pending = false;
			//rc = pec->waitOnEvent();
    		if ( rc != SUCCESS )
			{
				LOGIT(CLOG_LOG,"  Ident's pend on raw send failed.\n");
				// return the rc
			}
			else // - OK, keep going
			{
				LOGIT(CLOG_LOG,"  Ident's raw send was successful.\n");
    			rc = ZeroPkt2Identity(pPk, retIdentity);
    			// return the response code and identity

				// stevev 09mar11 - If the identify sees a configuration changed, say so
				if ( pPk->dataCnt > 1 && (pPk->theData[1] & 0x40) == 0x40 )
				{// we have rc & ds  and ds has config changed bit
					m_hadConfigChanged = true;
				}// end 09 mar11
			
				retIdentity.PollAddr = m_pollAddr;// actual connection location
			}
			putMTPacket(pPk);
		}
    }
	else
	if ( rc == SUCCESS ) // and NOT connected
	{
		rc = FAILURE;
	}
    // else - just return the code

// 	if ( comInited )
//	{
//		CoUninitialize(); // undo what we did, we only need it during this call @startup
//	}
	
	delete pec;
	delete pEv;
    return rc;
}
    
    
/* raw send returns with datacnt = 0xff && timeout == raw hr return value */
RETURNCODE CdocCommInfc::rawSend(hPkt* pSendPacket, hPkt* pRecvPacket)
{
    RETURNCODE rc = SUCCESS;
    HRESULT hr;
    long    status;
    BYTE    b, c, hdwrStat, commStat;
    string  eStr;
  	bool retryBusy = false;// default to doOnce
  	int i;// general count int stevev 4/13/04

//	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd(); //  POB, 2 Aug 2004 see below PAW 13/03/09
	CMainFrame * pMainWnd = (CMainFrame *) AfxGetApp ()->GetMainWnd (); // see above PAW 13/03/09 
  

    if (m_commHndl == 0 || ! m_connected || m_CommStatus == commNC)
    {
    	if (pRecvPacket) pRecvPacket->dataCnt = 0;
    	return DDL_COMM_PARAMETER_ERROR;
    }
    
 	if ( (m_CommStatus > commNC) || pSendPacket == NULL || pRecvPacket == NULL )
 	{
    	DEBUGLOG(CLOG_LOG,"> rawSend w/Comm not enabled - doing nothing. Cmd");
		DEBUGLOG(CLOG_LOG," %d\n", (pSendPacket)? pSendPacket->cmdNum : 10) ;

    	if (pRecvPacket) pRecvPacket->dataCnt = 0;		
    	return DDL_COMM_SHUTDOWNINPROGRESS; // do no work
    }
    
 /* stevev - 4/13/04 - modify packet for extended command numbers */
 	if (pSendPacket->cmdNum > 255)
 	{
 		for ( i = pSendPacket->dataCnt ; i > 0; i--)
 		{// make room for the command number
 			pSendPacket->theData[i+1] = pSendPacket->theData[i-1];
 		}
 		pSendPacket->dataCnt += 2;
 		pSendPacket->theData[0] = pSendPacket->cmdNum >> 8;		
 		pSendPacket->theData[1] = pSendPacket->cmdNum & 0xff;		
 		pSendPacket->byCmdNum = 31;
 	}
 	else
 	{
 		pSendPacket->byCmdNum = pSendPacket->cmdNum;
 	}
 /* end stevev 4/13/04 */
	    
    // allocate buffer space to communicate with COM component
    CComVariant varCommand;
 	CComVariant varReply;//                                    /*\/ stevev 4/13/04 */
 	hr = pUtil->AllocateCommandSpace(&varCommand, pSendPacket->byCmdNum, pSendPacket->dataCnt);
 	// apparently deallocated at function exit
    if (hr != S_OK)
    {
    	getErrorStr(hr, eStr);
    	LOGIT(CERR_LOG,"ERROR: rawSend allocation error: %s\n",eStr.c_str());
    	pRecvPacket->dataCnt = 0xFF;
    	pRecvPacket->timeout = (int)hr;
 		return DDL_COMM_MEMORY_FAILURE;
 	}
 	
 	do // while (retryBusy)
 	{	
 #ifdef _DEBUG
 		if (retryBusy) 
 		{
			LOGIT(CLOG_LOG,"DR: Resending Cmd:%d\n",pSendPacket->cmdNum);
 		}
 #endif	
 		varReply.Clear();
 		retryBusy = false; // reset to false on loop, code only sets to true...
 		rc = SUCCESS;
 		if ( (m_CommStatus > commNC/*was: ! m_enableComm*/) ||  
 			pSendPacket == NULL || pRecvPacket == NULL )
 		{
 			DEBUGLOG(CLOG_LOG,"> rawSend w/Comm not enabled - doing nothing. Cmd (%d)\n",
				(pSendPacket) ? pSendPacket->cmdNum : 10);

 			if (pRecvPacket) 
				pRecvPacket->dataCnt = 0;
 			return DDL_COMM_SHUTDOWNINPROGRESS; // do no work
 		}
 	
 		stxSocketDump(pSendPacket);

		// stevev 28aug09 - copy some stuff
		pRecvPacket->timeout       = pSendPacket->timeout;
		pRecvPacket->transactionID = pSendPacket->transactionID;
		pRecvPacket->byCmdNum      = pSendPacket->byCmdNum;
		pRecvPacket->m_thrdCntl    = pSendPacket->m_thrdCntl;

 
    		// copy message bytes into command buffer
 		for ( i = 0; (i < pSendPacket->dataCnt) && (rc==SUCCESS); i++) 
    	{
    		b = pSendPacket->theData[i];
    		hr = pUtil->InsertByte(&varCommand, i, b);
    		if (hr != S_OK)
    		{
    			getErrorStr(hr, eStr);
 				LOGIT(CERR_LOG,"ERROR: rawSend insertion error: %s\n",eStr.c_str());
    			pRecvPacket->dataCnt = 0xFF;
    			pRecvPacket->timeout = (int)hr;
    			rc = DDL_COMM_UTIL_FAILED;
    		}
    	}
    	if ( rc != SUCCESS)
    	{
 #ifdef DEBUG_TRANSACTION /* top of this file*/		
 			clog << "ERROR: UTIL.Insert failed."<<endl;
 #endif		
    		return rc;
    	}
    	else
    	{
    #ifdef DEBUG_TRANSACTION /* top of this file*/	
 			clog << "Xmitting cmd " << (int)(pSendPacket->cmdNum) << " with 0x" << hex << 
    										(int)(pSendPacket->dataCnt) <<dec<< " bytes data."<<endl;
    #endif
    	}
     
    	/* VMKP added on 160204 */
    	// HWND hwndMainWindow = ::FindWindow(NULL,g_pchWindowName); // POB, 2 Aug 2004

    	HWND hwndMainWindow = pMainWnd->GetSafeHwnd(); // POB, 2 Aug 2004

    	if (hwndMainWindow != NULL)
    	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    		::PostMessage(hwndMainWindow, WM_SDC_GLOW_RED, NULL, NULL);		
			systemSleep(1);
//try
// no worky  pMainWnd->SetTransmitterLight(false);
    	}
    	/* END of VMKP added on 160204 */
    
    	// Send HART Command and Receive Reply-------------- status is a copy of the returned hr
    	hr = pComm->Send(m_commHndl, varCommand, &varReply, &status );
    
    	/* VMKP added on 160204 */
		/* stevev 06jul09 - timer turns it off now **
    	if (hwndMainWindow != NULL)
    	{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    		::PostMessage(hwndMainWindow, WM_SDC_GLOW_GREEN, NULL, NULL);
//try
// no worky  pMainWnd->SetTransmitterLight(true);
    	}
		*** end timer 4 off ***/
    	/* END of VMKP added on 160204 */
    
    	/* added for a patch - covers weirdness in modem server */
    	if (hr == E_FAIL) 
    	{
    		if (hwndMainWindow != NULL)
    		{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead

				::PostMessage(hwndMainWindow, WM_SDC_GLOW_RED, NULL, NULL);		
				systemSleep(1);
    		}
#ifdef DEBUG_TRANSACTION /* top of this file*/		
 			clog << "Xmitting cmd " << pSendPacket->cmdNum << " AGAIN with 0x" << hex << 
 									(int)(pSendPacket->dataCnt) <<dec<< " bytes data."<<endl;
#endif
			// no internal retries in methods
			if (! pSendPacket->m_thrdCntl.fromMethod )
			{
 		// send it again - double the retries - a little odd but it works 
    		hr = pComm->Send(m_commHndl, varCommand, &varReply, &status );
			}

			/** stevev 06jul09 - timer turns it off now ***
    		if (hwndMainWindow != NULL)
    		{// NOTE; NOTE:  crashes if lparam is not null, used two messages instead
    			::PostMessage(hwndMainWindow, WM_SDC_GLOW_GREEN, NULL, NULL);
    		}
			**** end timer 4 off ***/
    	}
    	
    	if (hr != S_OK)
    	{
    		getErrorStr(hr, eStr);

/* temp- causes exception			LOGIT(CLOG_LOG|CERR_LOG,"ERROR: rawSend SEND error: %s\n",eStr);
*/			pRecvPacket->cmdNum  = pSendPacket->cmdNum; // put it back so we can find the msg queue
    		pRecvPacket->dataCnt = 0xFF;
 			pRecvPacket->timeout = (int)hr; // multiplexed to hold error

			DEBUGLOG(CLOG_LOG,"ERROR: COM returned 0x%04x from comm p%08p (%s)\n",
																(int)hr, pComm,	eStr.c_str());
    		rc = DDL_COMMUNICATIONS_FAILED;
    	}
 		else // send success
    	{	
 			b = 0xff;  /*pRecvPacket->dataCnt & 0xFF;		made no sense stevev 4/13/04 */
 			c = 0xff;  /*pRecvPacket->byCmdNum  & 0xFFFF;	made no sense stevev 4/13/04 */
    		hr = pUtil->CheckReply(varReply, &c, &b, &hdwrStat, &commStat);
    		pRecvPacket->dataCnt   = b;
 			pRecvPacket->byCmdNum  = c;/* stevev 4/13/04 */

			DEBUGLOG(CLOG_LOG,"         Raw rcvPkt: Cmd: %d with %d bytes.\n", (int)c, (int)b);
    
    		if (hr != S_OK)// can't reach this code...sjv 4aug09
    		{
    			getErrorStr(hr, eStr);
 				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: rawSend REPLY error: %s\n",eStr.c_str());
 				pRecvPacket->cmdNum  = pSendPacket->cmdNum;/* stevev 4/13/04 */
    			pRecvPacket->dataCnt = 0xFF;
    			pRecvPacket->timeout = (int)hr;

				DEBUGLOG(CLOG_LOG,"ERROR: UTIL returned 0x%04x\n", hr);

    			rc = DDL_COMM_EXTRACTION_FAILED;
    		}
    		else
    		{
    			rc = SUCCESS;
    		}

    		// re-encode the useful info
    		pRecvPacket->theData[0] = commStat;
    		pRecvPacket->theData[1] = hdwrStat;

    		for (i=0; i < pRecvPacket->dataCnt; i++) 
    		{
    			b  = 0;
    			hr = pUtil->ExtractByte(varReply, i, &b);
    			pRecvPacket->theData[i+2] = b;
    		}

 			// notice that the datacnt is not receive packet's data length (still len-2)
 /* stevev - 4/13/04 - modify receive packet for extended command numbers */
 			if (pRecvPacket->byCmdNum == 31 && pRecvPacket->dataCnt >= 2)
 			{
 				pRecvPacket->cmdNum = (pRecvPacket->theData[2] << 8) + (pRecvPacket->theData[3]);
 				for ( i = 2;  i < pRecvPacket->dataCnt; i++)
 				{// i is dest, i+2 is source
 					pRecvPacket->theData[i] = pRecvPacket->theData[i+2];
 				}
 				// the pRecvPacket->dataCnt stays {+2 for data and status, -2 for command number}
 			}
 			else
 			{
    			pRecvPacket->dataCnt += 2;	// modem server does not count the two status bytes
 				pRecvPacket->cmdNum   = pRecvPacket->byCmdNum;
 			}
 /* end stevev 4/13/04 */
    
    		// if no response, replace command as modem server overwrites it with 252
    		if (commStat == 0xff) 
    		{
    			if (pRecvPacket->cmdNum != 252)
    			{
				LOGIT(CLOG_LOG,
 						  "WARNING: Received comm status of 0x%02x with command set to %d.\n",
    						   commStat,pRecvPacket->cmdNum);
    			}
    			pRecvPacket->cmdNum  = pSendPacket->cmdNum;
    			pRecvPacket->dataCnt = 0xFF;
    			pRecvPacket->timeout = (int)hr;
    			rc = DDL_COMM_NO_RESPONSE;
    		}
    		else // responded
    		{// try to keep up
    			// if command 6 executed successfully, device polling address has changed
    			if (pSendPacket->cmdNum == 6  &&  commStat == 0 && pRecvPacket->dataCnt >= 3) 
    			{
    				m_pollAddr = pRecvPacket->theData[2];
    				// TODO: - set symbolID to value, set valid
    			}
 				else
 				if ( ( commStat >= rc_Busy        && 
 					   commStat <= rc_DR_Conflict && 
 					   pRecvPacket->dataCnt < 3 ) 
 					|| lastDRstate > rc_NoError ) // we are in delayed response
 				{// we have a busy/delayed response or we have been in DR
 					DEBUGLOG(CLOG_LOG,"DR: HandleDelayed for Cmd: %d with RC:0x%02x "
						"Last State:0x%02x\n",pSendPacket->cmdNum,(int)commStat,lastDRstate);
 					rc = handleDelayedResp(commStat);
 					/*a NEGATIVE RETURNCODE requests the caller to resend the command */
 					if ( rc < 0 )
 					{
 						retryBusy = true;
 					}
 					else // - SUCCESS or error - fall out
 					{
 						retryBusy = false;
 					}
 				}
 				// else - nothing special going on - leave retry false and fall out
 			}
 			ackSocketDump(pRecvPacket);
 
 		}//endelse - send success
 
 	}while(retryBusy);
{// block scope these debug variables...DEBUG CODE...
// see what the retries are here.....
//short y = 0;
//pComm->get_nRetries(&y);
//int z = y;
//DEBUGLOG(CLOG_LOG,"Post-Send retries reported as %d.\n",z);
} 		
    return rc;
}
    
 
/* This is run in the CommInfcTask with UI ties - beware */    
    
RETURNCODE CdocCommInfc::connect(void)
{
    RETURNCODE rc = SUCCESS;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.connect - Entry\n");

	HRESULT hr = 0;

    if (m_connected)
    {	disconnect();
		m_connected = false;
	}

    // ** Establish a Connection **
    
    /* timj 3/19/04 begin */
    if (pServer)
    {
    	// Using HART Server
		setMessageTime(HART_SERVER_TIMEOUT);
    	if (pSelect != NULL || pSelect3 != NULL)
    	{
    		CComBSTR empty("");
    		CComBSTR strTag = m_pDoc->m_copyOfCmdLine.cl_Url;
    		if (m_pDoc->m_copyOfCmdLine.cl_Url == "") 
    		{
    			long lStatus;
				if (pSelect3 != NULL)
				{
     				hr = pSelect3->SelectOpcTag( pServer, empty, &strTag, &lStatus );
				}
				else
				{
    				hr = pSelect->SelectOpcTag( pServer, empty, &strTag, &lStatus );
				}
    			if (FAILED(hr)) 
    			{
    				LOGIT(CERR_LOG|UI_LOG,"ERROR: Unable to SelectOpcTag.\n");
    				m_connected = false;
    				rc = DDL_COMM_CONBYTAG_FAILED;
    			}
    		}
    		
    		if (rc == SUCCESS)
    		{// we have a tag from select or command line
    			hr = pComm->ConnectByTag( empty, strTag, 0, &m_commHndl );
    			if (FAILED(hr)) 
    			{  
    				if (m_pDoc->m_copyOfCmdLine.cl_Url != "") // we haven't failed before
    				{// command line was wrong, give user a chance to select it
    					long lStatus;
						if (pSelect3 != NULL)
						{
    						hr = pSelect3->SelectOpcTag( pServer, empty, &strTag, &lStatus );
						}
						else
						{
    						hr = pSelect->SelectOpcTag( pServer, empty, &strTag, &lStatus );
						}
    					if (FAILED(hr)) 
    					{
    						LOGIT(CERR_LOG|UI_LOG,"ERROR: Unable to SelectOpcTag(2).\n");
    						m_connected = false;
    						rc = DDL_COMM_CONBYTAG_FAILED;
    					}
						else
						{// second selection gave us a tag
    						hr = pComm->ConnectByTag( empty, strTag, 0, &m_commHndl );
    						if (FAILED(hr)) 
    						{  
  								m_connected = false;
    							rc = DDL_COMM_CONBYTAG_FAILED;
							}
    						else
    						{
    							m_connected = true;
    							LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,
									L"Successful HART server connection at tag %s\n", 
																		strTag.m_str);
    						}
						}
    				} // we had our selection chance
					if ( rc != SUCCESS || m_connected == false )
					{
    					LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,
					"ERROR: Connection to tag %s failed.\n",m_pDoc->m_copyOfCmdLine.cl_Url);
					}
				}
    			else
    			{
    				m_connected = true;
    				LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,
						"Successful HART server connection at tag %s\n", 
												m_pDoc->m_copyOfCmdLine.cl_Url);
    			}
    		}
    	}
    	else
    	{
    		LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,
						"ERROR: Unable to connect to SelectOpcTag.\n");
    		m_connected = false;
    		rc = DDL_COMM_CONBYTAG_FAILED;
    	}

    }
    else
    {
    	// using modem server

    	// fetch preferred settings
    	CPreferences pref;

    	// get serial port

    	CComBSTR bstrCommPort;
    	//if ( m_pDoc->m_copyOfCmdLine.cl_Url == "") 
    	if ( m_pDoc->m_copyOfCmdLine.srcUrl == Arguments::src_Default) 
		{
    		bstrCommPort = pref.SearchSerialPort;
			LOGIT(CLOG_LOG,L"   Set Comm Port to default '%s'\n",pref.SearchSerialPort);
			// we need to modify it so others know what we'er doing
			((CDDIdeApp*)AfxGetApp())->m_CmdLine.cl_Url = pref.SearchSerialPort;
			((CDDIdeApp*)AfxGetApp())->m_CmdLine.srcUrl = Arguments::src_External;
    	}
    	else
    	{
    		bstrCommPort = m_pDoc->m_copyOfCmdLine.cl_Url;
			LOGIT(CLOG_LOG,L"   Set Comm Port to '%s'\n",m_pDoc->m_copyOfCmdLine.cl_Url);
    	}
    		
		m_pollAddr = m_pDoc->m_copyOfCmdLine.cl_PollAddr;  

		bool keepTrying;
		do
		{	// get poll address HARTServer.COM1
			// stevev 25sep09 - moved here to enable common code
    		long status;
    		BYTE pa = EMPTY_POLL_ADDR;	  // pa returned here
			BYTE pe = EMPTY_POLL_ADDR;    // stevev new 25sep09 - end point of scan

			keepTrying =  false;
			if (m_pollAddr == EMPTY_POLL_ADDR || 
				/* m_pDoc->m_copyOfCmdLine.srcPollAddr != Arguments::src_CmdLine ) */
				m_pDoc->m_copyOfCmdLine.srcPollAddr == Arguments::src_Default ) // allow ini & env
    		{
    			// no polling address on command line
    			// use modem server preferences to get poll address

    			switch (pref.SearchOption) 
				{
    				case SearchPollAddr0: 
    					{
    						m_pollAddr = 0;
							DEBUGLOG(CLOG_LOG,L"     :  Search Find Zero. \n");
    						break;
    					}

    				case SearchFindFirst: 
    					{
    						// search for device beginning at poll address 0
    						// if no device found, leave poll address at EMPTY_POLL_ADDR and it will fail
    						// on ConnectByPoll

    						m_pollAddr = EMPTY_POLL_ADDR;
    						for (BYTE pa = 0; pa < DOCCOMMMAXPOLLADDR; pa++) 
    						{
								DEBUGLOG(CLOG_LOG,"     :  Search Find First. %d\n",(int)pa);

    							VARIANT_BOOL isPresent;
    							HRESULT hr = 
								pComm->IsInstrumentAtPollAddress(bstrCommPort, pa, &isPresent);
    							if (isPresent) 
    							{
									DEBUGLOG(CLOG_LOG,"     :  Found @ PA: %d.\n",(int)pa);
    								m_pollAddr = pa;
    								break; // out of for loop
    							}
    						}
							if (m_pollAddr == EMPTY_POLL_ADDR)
							{
								DEBUGLOG(CLOG_LOG,"     :  Search Find First Failed.  \n");
							}
    						break;
    					}

    				case SearchPollAddrN: 
    					{
    						// poll addr specified on preferences dialog

    						m_pollAddr = pref.SearchPollAddr;
							LOGIT(CLOG_LOG,"   Set Poll Addr to %d\n",m_pollAddr);
    						break;
    					}

    				case SearchPollAddr15: 
    					{
							DEBUGLOG(CLOG_LOG,"     :  Search 15.\n");
							pa = 0;
							pe = 15;
						}
						/* fall thru to process */

    				case SearchPollAddr31: 
    					{
							if ( pa == EMPTY_POLL_ADDR )
							{
								DEBUGLOG(CLOG_LOG,"     :  Search 31.\n");
								pa = 0;
								pe = 31;
							}
						}
						/* fall thru to process */
						
    				case SearchPollAddr63: 
    					{
							if ( pa == EMPTY_POLL_ADDR )
							{
								DEBUGLOG(CLOG_LOG,"     :  Search 63.\n");
								pa = 0;
								pe = 63;
							}
						}
						/* fall thru to process */

    				case SearchPollAddrRange: 
    					{
							if ( pa == EMPTY_POLL_ADDR )
							{
								pa = pref.SearchPollAddrStart;
								pe = pref.SearchPollAddrEnd;
								DEBUGLOG(CLOG_LOG,"     :  Search range %d to %d.\n",pa,pe);
							}

							/*********** we're it...process now ***********/
							
    						// use SelectTag function to allow user to select a device
    						// if no device is selected, leave poll address at EMPTY_POLL_ADDR and it will fail
    						// on ConnectByPoll
    						m_pollAddr = EMPTY_POLL_ADDR;
							HRESULT hr = S_OK;
#if  0  // try this
CComVariant srtpoint;
CComVariant endpoint;
srtpoint.bVal = pa;
srtpoint.vt   = VT_UI1;
endpoint.bVal = pe;
endpoint.vt   = VT_UI1;
    						HRESULT hr = pSelect->SelectTag(pComm, bstrCommPort, &srtpoint, &endpoint,/*was &pa, &pe,*/ &status);
#else							
							if (pSelect3 != NULL)
							{
    							hr = pSelect3->SelectTagRange(pComm, bstrCommPort, &pa, &pe, &status);
							}
							else
							{
    							hr = pSelect->SelectTag(pComm, bstrCommPort, &pa, &status);
							}
#endif
    						if (hr == S_OK) 
    						{
								DEBUGLOG(CLOG_LOG,
											  "     :  Poll came back with PA: %d.\n",(int)pa);
    							m_pollAddr = pa;
    						}
							else
							{
								DEBUGLOG(CLOG_LOG,"     :  Poll came back with error "
									                  "0x%04x and status 0x%04x.\n",hr,status);
							}
    						break;
    					}

    				default:
    					{
    						ASSERT(0);  // mustn't be here
    						break;
    					}

    			}  // end switch
    		}

    		// connect to device
			DEBUGLOG(CLOG_LOG,"     :  Attempt connection at @ PA: %d.\n",(int)m_pollAddr);
    		hr = pComm->ConnectByPoll(bstrCommPort, m_pollAddr, 0, &m_commHndl );
    		if (FAILED(hr)) 
    		{
				TRACE(_T("        FAILED get by Poll.\n"));

				if ( hr >= 0x80000000 ) //see ...\vc98\Include\WINERROR.h
				{
					DEBUGLOG(CLOG_LOG,"ERROR:  Win RPC_E_SERVERFAULT.  Facility:%d  Code:%d\n",
						((hr>>15)&0x7ff),(hr&0xffff) );
				}

				LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Connection to polling address %d failed.\n",
																			(int)m_pollAddr);
    			m_connected = false;
				int userRetVal;
				CString billString;
				if (m_pollAddr == EMPTY_POLL_ADDR)
				{
				billString.Format(M_UI_SEARCH_POLL_FAIL);
				}
				else
				{
				billString.Format(M_UI_CONNECT_POLL_FAIL,m_pollAddr);
				}
				if ( ! m_pending )
				{
					userRetVal = AfxMessageBox(billString,MB_RETRYCANCEL | MB_ICONQUESTION);
					if ( userRetVal == IDRETRY )
					{
						m_pollAddr = EMPTY_POLL_ADDR;
						keepTrying =  true;
						DEBUGLOG(CLOG_LOG,"     :  Loop to Retry.\n");
					}
					else // user says stop
					{
    					rc = DDL_COMM_CONBYPOLL_FAILED;
						keepTrying =  false;
					}
				}
				else
				{
    				rc = DDL_COMM_SHUTDOWNINPROGRESS;
					keepTrying =  false;
				}
    		}
    		else
    		{
    			m_connected = true;
    			LOGIT(CLOG_LOG,
				"Successful modem server connection at poll address %d.\n", (int)m_pollAddr);
    		}  
		} while (keepTrying);
    }
    /* timj 3/19/04 end*/
    
    string erStr;
    getErrorStr(hr, erStr);

    if (m_commHndl == 0) 
    {	// valid handles are non-zero
     	//cerr << "ERROR: Connection to polling address "<< (int)m_pollAddr << " failed."<<endl;
        //cerr << "     : " << erStr << endl;
		LOGIT(CERR_LOG,"     : %s\n", erStr.c_str());
    	/* timj 3/19/04
    	m_connected = false;
    	rc = DDL_COMM_CONBYPOLL_FAILED;
    	*/
    }
    /* timj begin 3/19/04
   else 
   {
       m_connected = true;
       clog <<"Connected to device at polling address "<< (int)m_pollAddr <<endl;
       // TODO: if pollAddress symbol exists, update it and set it to valid
   }
   end timj */
	
    m_pMainFrm = (CMainFrame *)(AfxGetApp()->m_pMainWnd);

	LOGIF(LOGP_START_STOP)(CLOG_LOG,"docComm.connect -  Exit\n");

    return rc;
}


    
 /* Note: a NEGATIVE RETURNCODE requests the caller to resend the command */
 RETURNCODE CdocCommInfc::handleDelayedResp(int respCd)
 {
 // handle all comers	
 //	if (respCd < rc_Busy || respCd > rc_DR_Conflict)
 //	{// somebody is confused about how to call us
 //		return APP_PARAMETER_ERR;
 //	}
	 RETURNCODE rc;
 	int nextTryTime = 0;
 	int numberTries = 0;
 	int prevDRstate = 0;
 	int DRcond      = 0;
 
 	if ( m_pDoc == NULL )
 	{// use default
 		nextTryTime = DFLT_REPOLLTIME;
 		numberTries = DFLT_POLLRETRIES;
 	}
 	else
 	{// use user selected preferences
 		nextTryTime = m_pDoc->getPollRetryTime();
 		numberTries = m_pDoc->getPollRetryMax();
 	}
 	prevDRstate = lastDRstate;
 	DRcond = (prevDRstate << 8) | (respCd);
 
 	string errStr;
 
 	/* note to self - state analysis in DelayedBusyProcess.xls */
 	switch (DRcond)
 	{
 	//			 	  was state		       latest State 
 	case 0x0000:	/*rc_NoError		| rc_NoError    */
 	case 0x2000:	/*rc_Busy			| rc_NoError    */
 	case 0x2100:	/*rc_DR_Initiate	| rc_NoError    */
 	case 0x2200:	/*rc_DR_Running		| rc_NoError    */
 	case 0x2400:	/*rc_DR_Conflict	| rc_NoError    */
 		{// the more normal exits
 			lastDRstate = respCd;// zero
 			rePollCount = 0;
 			rc = SUCCESS;
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: exit SUCCESS w/ RC:0x%02x\n",respCd);
 		}
 		break;
 	case 0x0022:	/*rc_NoError		| rc_DR_Running */
 	case 0x2022:	/*rc_Busy			| rc_DR_Running */
 	case 0x2422:	/*rc_DR_Conflict	| rc_DR_Running */
 		{// Missing initiate state (missed a message?)[reset cnter, enter loop, issue UI warning]
 			LOGIT(UI_LOG|CERR_LOG,"WARNING: DR_INITIATE state was missed in Delayed Response.\n");
 		}
 		// fall thru to normal processing
 	case 0x0020:	/*rc_NoError		| rc_Busy       */
 	case 0x0021:	/*rc_NoError		| rc_DR_Initiate*/
 	case 0x0024:	/*rc_NoError		| rc_DR_Conflict*/
 	case 0x2021:	/*rc_Busy			| rc_DR_Initiate*/
 	case 0x2024:	/*rc_Busy			| rc_DR_Conflict*/
 	case 0x2122:	/*rc_DR_Initiate	| rc_DR_Running */
 	case 0x2421:	/*rc_DR_Conflict	| rc_DR_Initiate*/
 		{// the more normal state transitions (reset counter, enter loop)
 			rePollCount = 0;
 			systemSleep(nextTryTime);
 			lastDRstate = respCd;// latest-State
 			rc = -1; // request a resend
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: request a resend w/ RC:0x%02x\n",respCd);
 		}
 		break;
 	case 0x2120:	/*rc_DR_Initiate	| rc_Busy       */
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Initiate to Busy\n";}
 	case 0x2124:	/*rc_DR_Initiate	| rc_DR_Conflict*/
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Initiate to Conflict\n";}
 	case 0x2220:	/*rc_DR_Running		| rc_Busy       */
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Running to Busy\n";}
 	case 0x2224:	/*rc_DR_Running		| rc_DR_Conflict*/
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Running to Conflict\n";}
 	case 0x2420:	/*rc_DR_Conflict	| rc_Busy       */
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Conflict to Busy\n";}
 		{// abnormal state transitions...[reset cnter, enter loop, issue UI error]			
 			LOGIT(UI_LOG|CERR_LOG,(char*)errStr.c_str());
 			rePollCount = 0;
 			systemSleep(nextTryTime);
 			lastDRstate = respCd;// latest-State
 			rc = -1; // request a resend
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: request a resend w/ RC:0X%02x\n",respCd);
 		}
 		break;
 	case 0x2121:	/*rc_DR_Initiate	| rc_DR_Initiate*/
 		// warning loop - [incr cnter, enter loop, issue UI warning]// invalid state transition
 		if (errStr.empty())
 		{errStr = "WARNING: DR_INITIATE state was sent twice in a row in Delayed Response.\n";}
 	case 0x2221:	/*rc_DR_Running		| rc_DR_Initiate*/
 		// error loop - [incr cnter, enter loop, issue UI error] // bad state transition
 		if (errStr.empty())
 		{errStr = "ERROR:Delayed Response illegal transition from Running to Initiate\n";}
 		LOGIT(UI_LOG|CERR_LOG,(char*)errStr.c_str());
 		// fall through to normal incr handling
 	case 0x2020:	/*rc_Busy			| rc_Busy       */
 	case 0x2222:	/*rc_DR_Running		| rc_DR_Running */
 	case 0x2424:	/*rc_DR_Conflict	| rc_DR_Conflict*/
 		{//normal non-transition [incr cnter, continue loop]
 			// test loop counter and abort as required
 			rePollCount++;
 			if ( rePollCount < numberTries )
 			{
 				systemSleep(nextTryTime);
 				lastDRstate = respCd;// latest-State
 				rc = -1; // request a resend
				LOGIF(LOGP_COMM_PKTS)
				(CLOG_LOG,L"DR: request a resend w/ RC: 0x%02x try %d of %d\n",
												respCd,rePollCount,numberTries);
 			}
 			else // beyond threshold
 			{
 				rc = getDelayedRetryOK();
 				if ( rc = APP_USER_OKED )
 				{
 					rePollCount = 0;
 					systemSleep(nextTryTime);
 					lastDRstate = respCd;// latest-State
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: User continue: resend w/ RC:0x%02x "
											"try %d of %d\n",respCd,rePollCount,numberTries);
 					rc = -1; // request a resend
 				}
 				else // stop - or error return
 				{// the only exit that allows a DR_XXXXX or busy response code to goto the app
 					rePollCount = 0;
 					lastDRstate = rc_NoError;// latest-State
 					rc = APP_USER_CANCELED;  // request an exit
 				}
 			}
 		}
 		break;
 	case 0x0023:	/*rc_NoError		| rc_DR_Dead    */
 	case 0x2023:	/*rc_Busy			| rc_DR_Dead    */
 	case 0x2123:	/*rc_DR_Initiate	| rc_DR_Dead    */
 	case 0x2223:	/*rc_DR_Running		| rc_DR_Dead    */
 	case 0x2423:	/*rc_DR_Conflict	| rc_DR_Dead    */
 		{// dead exit...[reset wasState, dead-exit]
 			lastDRstate = rc_NoError;// latest-State
 			rePollCount = 0;
 			rc = SUCCESS; // exit like all is well
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: dead exit SUCCESS w/ RC:0x%02x\n",respCd);
 		}
 		break;
 
 
 // these cannot happen...when we get a dead state we clear the was-state to rc_NoError
 //	case 0x2300:	/*rc_DR_Dead		| rc_NoError    */
 //	case 0x2320:	/*rc_DR_Dead		| rc_Busy       */
 //	case 0x2321:	/*rc_DR_Dead		| rc_DR_Initiate*/
 //	case 0x2322:	/*rc_DR_Dead		| rc_DR_Running */
 //	case 0x2323:	/*rc_DR_Dead		| rc_DR_Dead    */
 //	case 0x2324:	/*rc_DR_Dead		| rc_DR_Conflict*/
 
 	default:
 		{// exit like it never existed
 			LOGIT(CERR_LOG,"Unknown State Transition: 0x%04x\n",DRcond);
 			lastDRstate = rc_NoError;// latest-State
 			rePollCount = 0;
 			rc = SUCCESS;// allow caller to process the message
			LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"DR: default exit SUCCESS w/ RC:0x%02x\n",respCd);
 		}
 		break;
 	}// endswitch
 
 	return rc;
 }
 
 // stevev 4/2/04 - extract the logging to a separate func
 void CdocCommInfc::stxSocketDump(hPkt* pPkt)
 {// New code for Packet dump Jeyabal
 	CClient *pSocket = NULL;
	BOOL isPri = FALSE;

 	if (m_pDoc)
 		pSocket = m_pDoc->GetSocket();
 
 	if (pSocket)
 	{
		
		pComm->get_bPrimaryMaster(&isPri);
 		char strBuff[MAX_PACKET_STRING];
if ( isPri )
 		strcpy(strBuff,"STX");
else
 		strcpy(strBuff,"Stx");
 
 		char strTemp[MAX_PACKET_STRING];
 		memset(strTemp,0,MAX_PACKET_STRING);
 
 		sprintf(strTemp, " %s  %04d", strBuff, pPkt->cmdNum);
 		strcpy(strBuff, strTemp);

 		sprintf(strTemp, " %s  %02d", strBuff, pPkt->dataCnt);
 		strcpy(strBuff, strTemp);
 
 		// Pack bytes for Status bits
 //			sprintf(strTemp,"%s %02x %02x ", strBuff, 0, 0);
 		sprintf(strTemp,"%s        ", strBuff, 0, 0);
 		//			if ( pPkt->dataCnt > 0 )
 		//			{
 		//				sprintf(strTemp,"%s __ __ ", strBuff);
 		//			}
 		//			else
 		//			{
 		//				sprintf(strTemp,"%s ", strBuff);
 		//			}
 		strcpy(strBuff, strTemp);
 		
 		for (int nCount = 0; nCount < pPkt->dataCnt - (pPkt->dataCnt%2); nCount+=2)
 		{
 			sprintf(strTemp,"%02x%02x  ", pPkt->theData[nCount], pPkt->theData[nCount+1]);
 			strcat(strBuff,strTemp);
 		}
 		// stevev 4/2/04 - user complaint about too many bytes in the output
 		//for (nCount = 0; nCount < (pPkt->dataCnt%2); nCount++)
 		//{
 		//	sprintf(strTemp,"%02x", pPkt->theData[nCount]);
 		//	strcat(strBuff,strTemp);
 		//}
 		if (pPkt->dataCnt%2)
 		{
 			sprintf(strTemp,"%02x", pPkt->theData[pPkt->dataCnt-1]);
 			strcat(strBuff,strTemp);
 		}
 		// stevev - end 4/2/4 --
 		int y = strlen(strBuff);
 		strBuff[y++] = 22; strBuff[y] = '\0';
 		pSocket->SendMessage(strBuff, y);
 	}
 // End - New code for Packet dump
 }
 // end stevev 4/2/04
 
 // stevev 4/9/04 - extract the logging to a separate func
 void CdocCommInfc::ackSocketDump(hPkt* pPkt)
 {// New code for Packet dump Jeyabal
 	CClient *pSocket = NULL;
 	if (m_pDoc)
 		pSocket = m_pDoc->GetSocket();
 
 	if (pSocket)
 	{
 		char strBuff[MAX_PACKET_STRING];
 		strcpy(strBuff,"Ack");
 
 		char strTemp[MAX_PACKET_STRING];
 		memset(strTemp,0,MAX_PACKET_STRING);
 
 		sprintf(strTemp, " %s  %04d", strBuff, pPkt->cmdNum);
 		strcpy(strBuff, strTemp);
 
 		sprintf(strTemp, " %s  %02d", strBuff, pPkt->dataCnt);
 		strcpy(strBuff, strTemp);
 
 		if (pPkt->dataCnt >= 2)
 		{
 			// Pack bytes for Status bits
 			sprintf(strTemp,"%s %02x %02x  ", strBuff, pPkt->theData[0], pPkt->theData[1]);
 			strcpy(strBuff, strTemp);
 		}
 		
 		if (pPkt->dataCnt < 0xf0U )
 		{
 			for (int nCount = 2; nCount < pPkt->dataCnt- (pPkt->dataCnt%2); nCount+=2)
 			{
 				sprintf(strTemp,"%02x%02x  ", pPkt->theData[nCount], pPkt->theData[nCount+1]);
 				strcat(strBuff,strTemp);
 			}
	 
 			if (pPkt->dataCnt%2)
 			{
 				sprintf(strTemp,"%02x", pPkt->theData[pPkt->dataCnt-1]);
 				strcat(strBuff,strTemp);
 			}
		}
		else
		{// error packet
 			strcat(strBuff," <<<<<<<<<<<<");
		}

 		int y = strlen(strBuff);
 		strBuff[y++] = 22; strBuff[y] = '\0';
 		pSocket->SendMessage(strBuff, y);//strlen(strBuff));
 	}
 // End - New code for Packet dump
 }
 // end stevev 4/9/04
 

    
    RETURNCODE CdocCommInfc::disconnect(void)
    {
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> docComm.disconnect - Entry\n");

    	if (pComm != NULL && m_connected && m_commHndl) 
    	{
    		HRESULT hr = pComm->Disconnect(m_commHndl);
    		if ( hr != 0 )
    		{
    			string erStr;
    			getErrorStr(hr, erStr);
    			LOGIT(CERR_LOG|UI_LOG,"ERROR: Device disconnect error :%s\n",erStr.c_str());
    			return DDL_COMM_DISCONNECT_FAILED;
    		}
    		else
    		{
    			LOGIT(CLOG_LOG,
					"Disconnected from device at polling address %d.\n", (int)m_pollAddr);
    			// TODO: if polladdress symbol id exists, set to FF and invalid
    		}
    	}
    
    	m_connected = false;
    	m_pMainFrm  = NULL;

		LOGIF(LOGP_START_STOP)(CLOG_LOG,">docComm.disconnect -  Exit\n");

    	return SUCCESS;
    }
    
    RETURNCODE CdocCommInfc::Search(Indentity_t& retIdentity, BYTE& newPollAddr)
    {
    	RETURNCODE rc = SUCCESS;
    
    	if ( pComm == NULL || m_pDoc == NULL )
    	{
    		return DDL_COMM_PARAMETER_ERROR;
    	}	
    
    	VARIANT_BOOL  isPresent;
    	CComBSTR      bstrCommPort(m_pDoc->m_copyOfCmdLine.cl_Url);
    	
    	BYTE pollAddress = 0;
    	BYTE holdAddress = m_pollAddr;
    	bool deviceFound = false;
    	HRESULT hr       = 0;
    
    	retIdentity.clear();
    
    	while (pollAddress < DOCCOMMMAXPOLLADDR) 
    	{
    		hr = pComm->IsInstrumentAtPollAddress(bstrCommPort, pollAddress, &isPresent);
    		if (isPresent) 
    		{
    			rc = GetIdentity(retIdentity, pollAddress);// connects
    			if ( rc == SUCCESS && m_connected )
    			{
    				deviceFound = true;
    				break; // out of while loop
    			}
    			else
    			{
    				if ( m_connected )
    				{	disconnect();}
    
    				m_pollAddr = holdAddress;
    				retIdentity.clear();
    			}
    		}
    		else 
    		{	// any error OR not found, increment and try again
    			pollAddress++;
    		}
    	}//wend
    
    	if (!deviceFound) 
    	{
    		//cerr << "ERROR: docComm's Search cannot find a device" << endl;
			LOGIT(CERR_LOG,"ERROR: docComm's Search cannot find a device\n");
    		rc = DDL_COMM_SEARCH_FAILED;
    	}
    	else
    	{
    		newPollAddr = pollAddress;
    
    	}
    
    	return rc;
    }
    
void  CdocCommInfc::disableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: disable App Comm with no Document.");
	}
	else
	{
		m_pDoc->disableAppCommRequests();
	}
}
    
void  CdocCommInfc::enableAppCommRequests(void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: enable App Comm with no Document.");
	}
	else
	{
		m_pDoc->enableAppCommRequests();
	}
}

bool  CdocCommInfc::appCommEnabled (void)
{
	if (m_pDoc == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: isEnable App Comm with no Document.");
		return false;
	}
	else
	{
		return m_pDoc->appCommEnabled();
	}
}

    void  CdocCommInfc::getErrorStr(HRESULT hR, string& retStr)
    {// values from winerror.h
    	
    	if (hR == S_OK) 
    	{
    		retStr = "HModemSvr: function was successful.";
    	}
    	else 
    	if (hR == E_ACCESSDENIED) //0x80070005L
    	{
    		retStr = "HModemSvr: Cannot aquire exclusive rights.";
    	}
    	else 
    	if (hR == E_FAIL) //0x80004005L
    	{
    		retStr = "HModemSvr: Unable to access connection to device.";
    	}
    	else 
    	if (hR == E_INVALIDARG) //0x80070057L
    	{
    		retStr = "HModemSvr: Argument contains invalid characters.";
    	}
    	else 
    	if (hR == E_OUTOFMEMORY) //0x8007000EL
    	{
    		retStr = "HModemSvr: Unable to allocate memory.";
    	}
    	else 
    	if (hR == E_HANDLE) //0x80070006L
    	{
    		retStr = "HModemSvr: Invalid handle.";
    	}
    	else 
    	if (hR == E_NOTIMPL) //0x80004001L
    	{
    		retStr = "HModemSvr: Not implemented.";
    	}
    	//RPC_E_WRONG_THREAD = _HRESULT_TYPEDEF_(0x8001010EL)
    	else 
    	{
    		char t[80];
    		sprintf(t,"0x%x",hR);
    		retStr = "HModemSvr: Error code " ;
    		retStr += t;
    	}
    }
    

	
/**********************************************************************************************
* clearQueue is used where FlushQueue is needed but all threads pending on done-event must be 
* released.
*  we have an issue with the msgCycle queue too.  There is one there for each of these.
*  we have an issue with all those variables left in the PENDING state too.
*
* >>>>caller should check m_CommStatus (for transition to ShuttingDown) when this function returns
*/
void  CdocCommInfc::clearQueue(void)
{	
    hPkt*           pSendPkt;

	while (thePktQueue.GetQueueState() > 0)// while not empty
	{
		pSendPkt = thePktQueue.GetNextObject(10);
    	if ( pSendPkt != NULL)
    	{
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Stop)
			{// de-init
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a Stop.\n");
				m_CommStatus = commShuttingDown;
			}
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_RawSnd) 
			{// identity interface
			 // ptr is event and we return data in the packet and the caller returns it
				LOGIT(CLOG_LOG,"Comm thread clearQueue found a RawSend.\n");
				if (pSendPkt->m_thrdCntl.threadLPARAM != NULL)
				{
					hCevent::hCeventCntrl* pEc = 
						((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
					if (pEc)
					{
						pEc->triggerEvent();
						delete pEc;
						delete ((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM));
						pSendPkt->m_thrdCntl.threadLPARAM = NULL;
					}// error - do nothing
				}// else no-op
			}// else we b done
    		thePktQueue.ReturnMTObject( pSendPkt );
		}// else an error, skip it
	}//wend more packets left
    thePktQueue.FlushQueue();// just in case

	/**** Now deal with all those variables that are out there pending ***/

    if (m_pDoc != NULL && m_pDoc->pCurDev != NULL)
	{
		CVarList* pVlist = (CVarList*)(m_pDoc->pCurDev->getListPtr(iT_Variable));
		if (pVlist != NULL)
		{
			for(CVarList::iterator VLIT = pVlist->begin(); VLIT != pVlist->end(); VLIT++)
			{// ptr 2a ptr 2a hCVar ---- for each device variable
				hCVar* pVar = (hCVar*)(*VLIT);
				if (pVar == NULL)
					continue;// should never happen
				INSTANCE_DATA_STATE_T tSt;
				tSt = pVar->getDataState();
				if ( tSt == IDS_PENDING )
				{
					pVar->markItemState(IDS_INVALID);// force a new read
				}// else, leave it alone
			}//next
		}//else list is unavailable
	}//we are in bas shape, just leave

	/**** and, finally, clear the msgQueue too ***/
	// stevev 10/11/10 - it may not exist yet...
	if (cpRcvService)
		cpRcvService->serviceMsgCycleQue(0);// clear it all
}


/**********************************************************************************************
* Task method, will be run under a registered task thread
* called once per packet-inserted-into-packetQueue
*/

void  CdocCommInfc::commInfcTask(waitStatus_t isTimerExpired)
{// called when a packet is inserted into the pktqueue
 // timer is infinite so the boolean should never be true
    // this is the task where the system pends on a response
    RETURNCODE rc = SUCCESS;
    hPkt* pSendPkt;
    hPkt  rcvPkt;
/* VMKP added on 020404 */
    rcvPkt.clear();
/* VMKP added on 020404 */
    cmdInfo_t myCmdInfo;  myCmdInfo.clear();
// New code for Packet dump Jeyabal
    CClient *pSocket = NULL;
    if (m_pDoc)
    	pSocket = m_pDoc->GetSocket();
// End - New code for Packet dump Jeyabal

    if (isTimerExpired == evt_timeout)
    {
    	LOGIT(CLOG_LOG,"Comm interface task called with a timer expiration.\n");
    }
	//                == NotConnected | shuttingDown || shutdown
    if ( m_CommStatus > commNoDev || isTimerExpired == evt_lastcall) 
    {
		if ( isTimerExpired != evt_lastcall)
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
						   "> commInfcTask w/Comm not enabled - flushing packet queue. (8)\n");
		else
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
							"> commInfcTask last-call - flushing packet queue. (8a)\n");
    	/// replace with below....thePktQueue.FlushQueue();// forces fall through
		clearQueue();
    }
    else
    if ( thePktQueue.GetQueueState() <= 0 )
    {
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
						"> commInfcTask w/empty queue - exiting. (98) p%08p\n",&thePktQueue);
		return;
    }

    while (thePktQueue.GetQueueState() > 0)// while not empty (not recently flushed)
    {
    	pSendPkt = thePktQueue.GetNextObject(11);
		DEBUGLOGIF( LOGP_COMM_PKTS )(CLOG_LOG,"PKTQUEUE:> %p gotten.\n",pSendPkt);
    	if ( pSendPkt != NULL)
    	{
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Init)
			{// set it up
				LOGIT(CLOG_LOG,"Comm thread Initializing.\n");
#ifndef USE_ONE_THREAD_CONTAINER
				COMcontainer.conCOM(this);
#else
				COMcontainer.conCOM(&COMcontainer);
#endif
				if (pComm != NULL)
				{// verify it's functional
					short tstNumber = 0;
					HRESULT hR = pComm->get_nRetries(&tstNumber);
					if ( FAILED(hR) )
					{
						LOGIT(CLOG_LOG,"init thread failed to get COM.\n");
						m_CommStatus = commShuttingDown;
						LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Comm thread did NOT Initialize.\n");
					}
					else
					{
						setHostAddress(GetDocument()->m_copyOfCmdLine.cl_HstAddr);

						m_CommStatus = commOK;
						LOGIT(CLOG_LOG,"Comm thread Initialized OK as Address %hd.(has %d in the Q)\n",
							getHostAddress(), thePktQueue.GetQueueState());
					}
				}
				else
				{// error - failed
					m_CommStatus = commShuttingDown;
				}
    			thePktQueue.ReturnMTObject( pSendPkt );// 10jan11-return it to end MIA log
				return; // stevev 8sep05
			}
			else
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Stop)
			{// de-init
				LOGIT(CLOG_LOG,"Comm thread Stopping.\n");
				m_CommStatus = commShuttingDown;
    			// the rest of the cleanup is done at the end of the this function				
				clearQueue();
			}
			else
			if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_Connect)
			{// de-init
				LOGIT(CLOG_LOG,"Comm thread Connecting.\n");
				rc = connect();
				hCevent::hCeventCntrl* pEc = 
					((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
				if (pEc)
				{
					LOGIT(CLOG_LOG,"commInfcTask:Connect control:triggering calling event.\n");
					pEc->triggerEvent();
					delete pEc;
				}
				else // the event would trigger the packet return
    				thePktQueue.ReturnMTObject( pSendPkt );// 10jan11-return it to end MIA log
				return; // stevev 8sep05
			}
			else 
			{// send || sendRaw
				if LOGTHIS(LOGP_COMM_PKTS)
				{
					struct _timeb sendTime;
					_ftime( &sendTime );
  #ifndef  _USE_32BIT_TIME_T /* timeb.h...#define _timeb      __timeb64 */
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%I64d.%hd Sending   Cmd %d  ",
								sendTime.time, sendTime.millitm, pSendPkt->cmdNum  );
  #else
					LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"%d.%d Sending   Cmd %d  ",
								sendTime.time, sendTime.millitm, pSendPkt->cmdNum);
  #endif
					pSendPkt->Log();
				}
    			rc = rawSend(pSendPkt, &rcvPkt);//syncronous send
    			if ( rc == SUCCESS)
    			{	
					if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_RawSnd) 
					{// identity interface
					 // ptr is event and we return data in the packet and the caller returns it
						LOGIT(CLOG_LOG,"Comm did a RawSend.\n");
						if (pSendPkt->m_thrdCntl.threadLPARAM != NULL)
						{
							hCevent::hCeventCntrl* pEc = 
							((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
							*pSendPkt = rcvPkt;
							if (pEc)
							{
								pEc->triggerEvent();
								delete pEc;
								// trigger event will trigger the sender to return the  packet
							}
						}
						else
							*pSendPkt = rcvPkt;
						return;
					}
					// else process as normal
					// parse the packet
    				if (cpRcvService != NULL )
    				{
    					if ( m_CommStatus == commNoDev || m_CommStatus == commNC )
    					{
    						m_CommStatus = commOK;
    					}

    					// UPDATE Packet Log Here !!!!!!!!!!! rcvPkt !!!!!!RECEIVE			   
	#ifdef DEBUG_TRANSACTION /* top of this file*/
    					LOGIT(CLOG_LOG,"Recving cmd %d with %d bytes data.\n",
										(int)(rcvPkt.cmdNum),(int)(rcvPkt.dataCnt) );
	#endif                  
	/*	commented out code removed 25aug05 - see earlier versions for contents    */
						
						if ( m_CommStatus > commNoDev ) // was:   ! m_enableComm )
    					{
    						myCmdInfo.discardMsg = true;
    					}
						if LOGTHIS(LOGP_COMM_PKTS)
						{
							LOGIT(CLOG_LOG,"RECEIVED "); 
							rcvPkt.Log();
						}
    					cpRcvService->serviceReceivePacket(&rcvPkt,&myCmdInfo);
    				}
    				else//cpRcvService == NULL
    				{
    				   LOGIT(CERR_LOG,"ERROR: no service function to the receive packet.\n");
    				   rc = DDL_COMM_PROGRAMMER_ERROR;
    				}				
    			}
    			else // send failed, //else // rawsend not success
    			{//handle it
    				LOGIT(CLOG_LOG,"Comm interface task raw send failed with RC:%d\n",rc);
    				if (cpRcvService != NULL )
    				{
    					if ( m_CommStatus > commNoDev ) // was:   ! m_enableComm )
    					{
    						myCmdInfo.discardMsg = true;
    					}
						if LOGTHIS(LOGP_COMM_PKTS)
						{
							LOGIT(CLOG_LOG,"RECEIVED  "); 
							rcvPkt.Log();
						}
    					cpRcvService->serviceReceivePacket(&rcvPkt,&myCmdInfo);
																	// triggers event w/ msgCyc
    				}
					// stevev 11jul07 - locks up on identity failure, add the else
					else
					{
						if ( pSendPkt->m_thrdCntl.threadAction == hThreadCntl::thd_RawSnd)
						{// identity interface
						 // ptr is event and we return data in the packet 
							LOGIT(CLOG_LOG,"Comm failed the RawSend.\n");
							if (pSendPkt->m_thrdCntl.threadLPARAM != NULL)
							{
								hCevent::hCeventCntrl* pEc = 
							((hCevent*)(pSendPkt->m_thrdCntl.threadLPARAM))->getEventControl();
								*pSendPkt = rcvPkt;
								if (pEc)
								{
									pEc->triggerEvent();
									delete pEc;
								}
							}
							else
								*pSendPkt = rcvPkt;
							return;
						}
					}
    				if ((long)(rcvPkt.timeout) == E_FAIL) 
    				{
    					if ( m_CommStatus == commOK)
    					{
    						m_CommStatus = commNoDev;
    					}
    				}
					// note: pOtherData is a copy of the msgcycle
    				if (myCmdInfo.pOtherData && 
											((hMsgCycle_t*)myCmdInfo.pOtherData)->actionsEn )
    				{// we have a host error
    		// won't work for methods >>>>>>>>>>>>>>>>>>>>>>			
    					if ( m_pDoc ) 
    					{  // we have a doc ptr && not shutdown scenario

							if ( ((hMsgCycle_t*)myCmdInfo.pOtherData)->m_cmdOrginator != co_METHOD )
							{// only for non methods---
    							string  eStr;
    							long    status = (long)(rcvPkt.timeout);	
    							getErrorStr(status, eStr);
// this should have a counter for 3 misses in lenient mode09mar11
 m_pDoc->HostCommErrorCallback(status, eStr); //can't do this if actions disabled (in a method)
							}
    						LOGIT(CLOG_LOG,">> Raw send Failed >>>>>>>>\n");
    					}
    					else 
    					{ 
    					   LOGIT(CERR_LOG|CLOG_LOG,"ERROR: no doc pointer in commInfcTask.\n");
    					}
    				}
    				// replace with below....thePktQueue.FlushQueue();
					clearQueue();
    				LOGIT(CLOG_LOG,"> PacketQueue Flushed.\n");
    			}			
			}
    	}
    	//else queue error, we should never have gotton here
    	if ( pSendPkt != NULL )
    	{
    		thePktQueue.ReturnMTObject( pSendPkt );
    	}
    	rcvPkt.clear();
    }//wend there are more packets to send

    if ( m_CommStatus > commNoDev  || isTimerExpired == evt_lastcall) 
	{
		if ( isTimerExpired != evt_lastcall)
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> commInfcTask w/Comm not enabled "
							" into cleanup - flushing packet queue. (8-1)\n");
		else
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> commInfcTask last-call"
							" into cleanup - flushing packet queue. (8a1)\n");
    	// replace with below....thePktQueue.FlushQueue();
		clearQueue();
    	if (pSocket)
    	{
    		char strBuff[MAX_PACKET_STRING];
    		sprintf(strBuff,"Shutting down communications.%c%c",31,22,0 );
    		int y = strlen(strBuff);
    		pSocket->SendMessage(strBuff, y);
    	}
		LOGIF(LOGP_START_STOP)(CLOG_LOG,
							"> commInfcTask in cleanup - sending NULL to rawSend. (9)\n");
    	rc = rawSend(NULL, &rcvPkt);//let send shutdown
    	if (cpRcvService != NULL)
    	{
    		myCmdInfo.discardMsg = true;
    		myCmdInfo.clrNexit   = true;
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"> commInfcTask in cleanup - "
												"sending MT to serviceReceivePacket. (11)\n");
    		cpRcvService->serviceReceivePacket(&rcvPkt,&myCmdInfo);//allow dispatcher to clear
    	}	
	
		disconnect();	
		COMcontainer.disconCOM(this);

    	// m_CommDisabled = true;// handshake, we be done
    	m_CommStatus = commShutDown;
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"> commInfcTask in cleanup - "
												"commDisabled now true, returning. (15)\n");
    }
    if ( myCmdInfo.pOtherData ) delete myCmdInfo.pOtherData;
    return;

}// end commInfcTask
    
    /* stevev 4/6/04 */
    
    RETURNCODE	CdocCommInfc::getDelayedRetryOK()
    { 
    	RETURNCODE rc ;
    
    	if ( m_pDoc == NULL || m_pMainFrm == NULL )
    	{
    		return APP_USER_CANCELED;
    	}
    	CString ms;
    	ms.Format(M_UI_NO_RESP_CONTINUE_QUEST,
    							m_pDoc->getPollRetryTime() * m_pDoc->getPollRetryMax()/1000);
    
    	int r =	
			m_pMainFrm->MessageBox(ms, M_UI_DELAYED_RESP, MB_RETRYCANCEL | MB_ICONQUESTION );
    	switch(r)
    	{
    	case IDRETRY:
    		rc = APP_USER_OKED;
    		break;
    	case IDCANCEL:
    	case IDABORT:
    	default:
    		rc = APP_USER_CANCELED;
    	}
    
    	return rc;
    }
    
    /* end stevev 4/6/04 */

/* stevev 18jan05 - New init comm capabilities */
/****
 *  Mr Gates needs an 'apartment' to initialize a COM object that is multi-threaded
 *  so that it does not interfere with ActiveX components in the main thread that may  
 *  ONLY be single-threaded COM objects.
 *  Indications are that an 'apartment' is a task.  This is it.
 *  We initialize the COM and then handshake with the calling class to deal with errors
 *  and such.
 *	We then just pend for shutdown since we are multithreaded and other threads do the work
 *  Note that this class is in the UI portion of the system so we can use mfc & Bill's stuff.
 ****/
UINT initializeCOMMultiThreaded( LPVOID pParam )
{		
	RETURNCODE rc = SUCCESS;

	COMthread* pCom = (COMthread*)pParam;
	if (pCom == NULL)
	{
		LOGIT(CERR_LOG|UI_LOG,L"ERROR: Comm Interface initializer with inadequate info.\n");
		return -1;
	}

	LOGIT(CLOG_LOG,L"> doc-comm infc thread Initializing.\n");
	
	rc = COMcontainer.conCOM(pCom);

  	// timj begin 4/7/04
  	if (pCom->pComm == NULL) 
  	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,L"> doc-comm  initializeCOM failed to get a pComm.\n");
		return rc;// should already be logged
 	}

    /*
     * modem server retrying on error
     */
//these give a purify error - appears to be in Bill's stuff -
    pCom->pComm->put_nRetries(DOCCOMMRETRIES);

    pCom->pComm->put_bPrimaryMaster(0);	// we default to secondary master 
										// 0 == false = NOT-Primary

	LOGIT(CLOG_LOG,"%s\n","> doc-comm completed, pending on abort.");
	// lo longer   pCom->enableComm();

 	// timj end 4/7/04	
	hCevent::hCeventCntrl*	pDoneEvt = pCom->m_evtInitialized.getEventControl();
	pDoneEvt->triggerEvent();// allow main task to continue
	
	/*******************************************************************************************
	 * we are finished initializing, successful or not
	 * we now wait for the handshake to kill this thread
	 *******************************************************************************************
	*/	
	hCevent::hCeventCntrl*	pStopEvt = pCom->m_evtShutdownNow.getEventControl();
	pStopEvt->pendOnEvent();// wait forever

	// forever is here - undo what we did above
	LOGIT(CLOG_LOG,"%s\n","> doc-comm got an abort event.");	  
	
	// assume all are disconnected!!! disConnectComm();

    COMcontainer.disconCOM(pCom);

	LOGIT(CLOG_LOG,"%s\n","> COM thread finished: exit, triggering done event.");
	
	pDoneEvt->triggerEvent();// allow main task to continue

	delete pDoneEvt;
	delete pStopEvt;

	return 0; // we are done - self destruct
}
/* end stevev 18jan05 */
    

DWORD COMthread::conCOM(COMptr_t* p2sPtrs)
{
	HRESULT hr = 0;
	int state = 0;  // success-type: bit 0-success on second try(1 = second try), bit 1-srvr1st
					// note that all failures throw with an error message
	if (p2sPtrs == NULL)
	{
		LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"connect2COM called with no parameter\n");
		return APP_PARAMETER_ERR;
	}

	if ( (p2sPtrs->pSelect3 != 0 || p2sPtrs->pSelect != 0 ) && 
		 (p2sPtrs->pServer  != 0 || p2sPtrs->pComm   != 0 )  )
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,L"connect2COM conCOM already has connected.\n");
		return 0; // we have been here before, don't redo it.
	}
#ifndef USE_ONE_THREAD_CONTAINER
	else
	if(  (COMcontainer.pSelect3 != 0 || COMcontainer.pSelect !=0  ) &&
		 (COMcontainer.pServer  != 0 || COMcontainer.pComm   != 0 )  )
	{
		p2sPtrs->pComm	 = COMcontainer.pComm;
		p2sPtrs->pUtil   = COMcontainer.pUtil;
		p2sPtrs->pSelect = COMcontainer.pSelect;
		p2sPtrs->pSelect3= COMcontainer.pSelect3;
		p2sPtrs->pServer = COMcontainer.pServer;	
		return 0; // it's OK
	}
#endif
    try {
  		/*
  		 * timj - If HART Server is available, use it.  Otherwise, use the HART Modem Server
  		 */

    	USES_CONVERSION;

    	// Initialize COM
    	//if( FAILED( CoInitialize( NULL ) ) ) 
    	if (  FAILED( CoInitializeEx(NULL, COINIT_MULTITHREADED) ) )//we need 'em in another task
    	{
			LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1020);
    		throw HartException(1020);//"Unable to initialize COM\n"
    	}

    	// ** Get Pointers to the HART Tool interfaces **
    	hr = CoCreateInstance( CLSID_HartUtilities, NULL, CLSCTX_SERVER,
											IID_IHartUtilities, (void **) &(p2sPtrs->pUtil) );
    	if( FAILED( hr ) ) 
    	{
    		LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1021);
    		throw HartException(1021);//"Unable to get Hart Utilities pointer\n"
    	}
     
  		/* timj 3/19/04 begin */
/**   const char* HartException::except1022 = "Unable to get Hart Modem Connection";
 **   const char* HartException::except1023 = "Unable to get Hart Server Connection";
 **/
		bool useSrvrFirst = ((CDDIdeApp*)AfxGetApp())->m_CmdLine.cl_UseServer;// sjv-17may07

  		// get pointer to new HART device selection services
  		hr = CoCreateInstance( CLSID_HartSelectDevice, NULL, CLSCTX_SERVER,
  							   IID_IHartSelect3, (void **) &(p2sPtrs->pSelect3) );
		// hr == 80004002 No such interface supported
  		if( FAILED( hr ) )
  		{	
			p2sPtrs->pSelect3 = NULL;
  			LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",
				"Unable to get the new Hart Select Connection.\n"
				"You may be limited to a max scanning address of 15.");
			
  			// get pointer to HART device selection services
  			hr = CoCreateInstance( CLSID_HartSelectDevice, NULL, CLSCTX_SERVER,
  								   IID_IHartSelect2, (void **) &(p2sPtrs->pSelect) );
  			if( FAILED( hr ) )
  			{
				p2sPtrs->pSelect = NULL;
  				LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1024);
  				throw HartException(1024);//"Unable to get Hart Select2 pointer\n"
  			}
  		}

		if ( useSrvrFirst ) //  sjv-17may07
		{
			state = 2; // srvr1st
#ifdef SUPPORT_HART_SERVER
  			// try to load HART Server
 			hr = CoCreateInstance( CLSID_OPCServer, NULL, CLSCTX_SERVER, 
//											IID_IUnknown, (void **) &(pCom->pServer) );
											IID_IUnknown, (void **) &(p2sPtrs->pServer) );
			LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Try to load HART Server returned 0x%08x\n",hr);
//0x80040154  REGDB_E_CLASSNOTREG
//0x80040110  CLASS_E_NOAGGREGATION
//0x80004002  E_NOINTERFACE
//0x80004003  E_POINTER
#else
			hr = -1; // force modem server to load
#endif //SUPPORT_HART_SERVER
		}
		else// sjv-17may07
		{
			// state = 0; modem first
    		hr = CoCreateInstance( CLSID_HartModemComm, NULL, CLSCTX_SERVER,
    							     IID_IHartComm, (void **) &(p2sPtrs->pComm) );
			LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Try to load Modem Server returned 0x%08x\n",hr);
		}

  		if( FAILED(hr) )
  		{// priority failed, try second choice
			state |= 1; // first failed, try second
			if ( useSrvrFirst ) //  sjv-17may07
			{
  				// FAILED, HART Server not installed on this computer, so use Modem Server
    			hr = CoCreateInstance( CLSID_HartModemComm, NULL, CLSCTX_SERVER,
    							     IID_IHartComm, (void **) &(p2sPtrs->pComm) );
				LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Try to load Modem Server returned 0x%08x.\n",hr);
			}
			else //  sjv-17may07
			{
#ifdef SUPPORT_HART_SERVER
  				// try to load HART Server
 				hr = CoCreateInstance( CLSID_OPCServer, NULL, CLSCTX_SERVER, 
//											IID_IUnknown, (void **) &(pCom->pServer) );
											IID_IUnknown, (void **) &(p2sPtrs->pServer) );
				LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Try to load HART Server returned 0x%08x.\n",hr);
#else
				hr = -1; // force modem server to load
#endif //SUPPORT_HART_SERVER
			}
    		if( FAILED( hr ) ) // second choice didn't make it
    		{
				if ( useSrvrFirst ) //  sjv-17may07
				{
    				LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s (return code %d)\n",
													(char *)HartException::except1022,hr);
    				throw HartException(1022);//"Unable to get Hart Modem Connection"
				}
				else
				{
    				LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s (return code %d)\n",
													(char *)HartException::except1023,hr);
    				throw HartException(1023);//"Unable to get Hart Server Connection"
				}
    		}
			else 
			if (! useSrvrFirst) // ie use modem server first we are successful second try is HART server
			{
  				// HART Server exists, get a communication pointer.
  				hr = CoCreateInstance( CLSID_HartComm, NULL, CLSCTX_SERVER,
  										  IID_IHartComm, (void **) &(p2sPtrs->pComm) );
  				if( FAILED( hr ) ) 
  				{
  					LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1023);
  					throw HartException(1023);//"Unable to get Hart Server Connection"
  				}
				else
				{				
					LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Loaded the HART Server\n");
				}
			}
			// else, just continue as OK
			else // successful second try w/ user server first..ie modem server
			{				
				LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Loaded the Modem Server\n");
			}
  		}
  		else // first priority succeeded
		if ( useSrvrFirst) // we are successful first HART server
  		{
  			// HART Server exists, get a communication pointer.
  			hr = CoCreateInstance( CLSID_HartComm, NULL, CLSCTX_SERVER,
  									  IID_IHartComm, (void **) &(p2sPtrs->pComm) );
  			if( FAILED( hr ) ) 
  			{
  				LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1023);
  				throw HartException(1023);//"Unable to get Hart Server Connection"
  			}
			else
			{				
				LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Loaded the HART Server.\n");
			}
  		}
		// else we are a successful Modem server first priority
		else
		{
			LOGIF(LOGP_START_STOP)(CLOG_LOG,">  Loaded the Modem Server.\n");
		}
		LOGIT(CLOG_LOG,">          Setup %sCOM for %s Server\n",(state & 0x01)?"back-up ":"",
																(state & 0x02)?"OPC":"Modem");
		LOGIT(CLOG_LOG,"%s\n","> doc-comm COMthread instantiated");
  		/* timj 3/19/04 end */

    }// end try
    	
    catch (HartException e) 
    {
    	hr = e.completionCode;
    	AfxMessageBox(e.errorStr());
    }
    catch ( ... ) 
    {	// shouldn't be here!
//		test->console.print("Unknown exception captured in test\n");
		LOGIF(LOGP_START_STOP)(CLOG_LOG,L"connect2COM caught a system exception.\n");
    	hr = APP_CMD_COMM_ERR;//status = HT6ABORT;
    }
	/* removed 18jan05 */
	LOGIF(LOGP_START_STOP)(CLOG_LOG,L"connect2COM returning %#x.\n", (unsigned)hr);
	return hr;
}


DWORD COMthread::disconCOM(COMptr_t* p2sPtrs)
{
	DWORD rc = SUCCESS;

	try 
	{	// Disconnect if necessary

		if( p2sPtrs->pComm != NULL )  
		{
    		//done at disconnect now
			//if( getCommHandle() != 0L ) 
    		//{
    		//	pComm->Disconnect( getCommHandle() );
    		//}
    		p2sPtrs->pComm->Release();
    		p2sPtrs->pComm = NULL;
		}
		if ( p2sPtrs->pUtil != NULL )
		{
    		p2sPtrs->pUtil->Release();
    		p2sPtrs->pUtil = NULL;
		}
  		// timj begin 4/6/04
  		if ( p2sPtrs->pServer != NULL )
  		{
  			p2sPtrs->pServer->Release();
  			p2sPtrs->pServer = NULL;
  		}
  		if ( p2sPtrs->pSelect != NULL )
  		{
  			p2sPtrs->pSelect->Release();
  			p2sPtrs->pSelect = NULL;
  		}
  		if ( p2sPtrs->pSelect3 != NULL )
  		{
  			p2sPtrs->pSelect3->Release();
  			p2sPtrs->pSelect3 = NULL;
  		}
  		// timj end 4/6/04

		CoUninitialize();
#ifndef USE_ONE_THREAD_CONTAINER
		COMcontainer.pComm   = NULL;
		COMcontainer.pUtil   = NULL;
		COMcontainer.pSelect = NULL;
		COMcontainer.pSelect3= NULL;
		COMcontainer.pServer = NULL;	
#endif
	}

	catch ( ... ) 
	{	// shouldn't be here!
		LOGIT(CERR_LOG|CLOG_LOG, "Unknown exception occurred during COM's thread cleanup\n");
		rc = APP_CMD_COMM_ERR;
	}
	return rc;
}

DWORD COMthread::disConnectComm(CbaseComm* pBaseComm)
{
	CdocCommInfc* pCI = (CdocCommInfc*)pBaseComm;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"disConnectComm - Entry\n");

	if ( pCI == NULL )
	{
		LOGIT(CLOG_LOG|CERR_LOG,"disConnectComm called with no communication pointer.\n");
		return -1;
	}
	if( pCI->getCommHandle() != 0L ) 
    {
    	pCI->pComm->Disconnect( pCI->getCommHandle() );
    }
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"disConnectComm -  Exit\n");

	return SUCCESS;
}

/* connect the COM to the commInfcTask thread */
DWORD COMthread::connectComm(CbaseComm* pBaseComm)
{
	RETURNCODE rc = SUCCESS;
	CdocCommInfc* pCI = (CdocCommInfc*)pBaseComm;
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"connectComm - Entry\n");
	if ( pCI == NULL )
	{
		LOGIT(CLOG_LOG|CERR_LOG,"connectComm called with no communication pointer.\n");
		return -1;
	}

	// determine if we already have a COM owner thread
	if (pCOMthread == NULL)
	{// generate COM owner	
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"connectComm - Starting COM thread.\n");

		hCevent::hCeventCntrl*	pDoneEvt = m_evtInitialized.getEventControl();

		pCOMthread = 
			     AfxBeginThread(initializeCOMMultiThreaded,&COMcontainer,THREAD_PRIORITY_NORMAL);
		if (pCOMthread == NULL)
		{
			LOGIT(CLOG_LOG|CERR_LOG,"connectComm failed to create a thread.\n");
			return FAILURE;
		}

		rc = pDoneEvt->pendOnEvent(30000);    // max - half minute to initialize

		if ( rc != SUCCESS && pComm != NULL )
		{// we had a startup error
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"connectComm - Done Event failed.\n");
			hCevent::hCeventCntrl*	pStopEvt = m_evtShutdownNow.getEventControl();
			pStopEvt->triggerEvent();
			int cnt = 50;// 5 seconds
			while (pComm != NULL && cnt)
			{
				systemSleep(100);
				cnt--;
			}
			return FAILURE;
		}
		else
		if ( pComm == NULL )
		{
			LOGIF(LOGP_START_STOP)(CLOG_LOG,"connectComm - Thread failed to connect to COM.\n");
			return FAILURE;
		}
	}// else proceed

	LOGIF(LOGP_START_STOP)(CLOG_LOG,"connectComm - Got Comm thread.\n");

/* test for a threading problem
	short tstNumber = 0;
	if (COMcontainer.pComm)
	{
		HRESULT hR = COMcontainer.pComm->get_nRetries(&tstNumber);
		if (hR == RPC_E_WRONG_THREAD)
		{
			LOGIT(CLOG_LOG,"connectCom got the threading error.\n");
			
    		hR = CoCreateInstance( CLSID_HartModemComm, NULL, CLSCTX_SERVER,
    								 IID_IHartComm, (void **) &(COMcontainer.pComm) );
    		if( FAILED( hR ) ) 
    		{
    			LOGIT(CLOG_LOG|CERR_LOG|UI_LOG,"%s\n",(char *)HartException::except1022);
    			throw HartException(1022);//"Unable to get Hart Modem Connection"
    		}
			else
			{// see if we solved it
				hR = COMcontainer.pComm->get_nRetries(&tstNumber);
				if (! FAILED(hR) )
				{
				LOGIT(CLOG_LOG,"connectCom got a new instance p%04p\n",COMcontainer.pComm);
				}
				else
				{
				LOGIT(CLOG_LOG,"connectCom Failed to get a new instance.\n");
				}
			}
		}
#ifdef _DEBUG
		else
		{
			LOGIT(CLOG_LOG,"connectCom has a good instance p%04p\n",COMcontainer.pComm);
		}
#endif
	}
// end test
*/

//	pCI->pComm   = COMcontainer.pComm;
//	pCI->pUtil   = COMcontainer.pUtil;

//	pCI->pSelect = COMcontainer.pSelect;
//	pCI->pServer = COMcontainer.pServer;

	return 0; // success!
}

COMthread::~COMthread()
{
	if (pCOMthread != NULL)
	{// DE-generate device

		if ( pComm != NULL || pUtil != NULL ||  pServer != NULL || pSelect != NULL )
		{// anybody left
    		try 
    		{	// Disconnect if necessary
    
    			if( pComm != NULL )  
    			{
    				// this should have been done a loong time ago
					//if( m_commHndl ) 
    				//{
    				//	pComm->Disconnect( m_commHndl );
    				//}
    				pComm->Release();
    				pComm = NULL;
    			}
    			if ( pUtil != NULL )
    			{
    				pUtil->Release();
    				pUtil = NULL;
    			}
  				// timj begin 4/6/04
  				if ( pServer != NULL )
  				{
  					pServer->Release();
  					pServer = NULL;
  				}
  				if ( pSelect != NULL )
  				{
  					pSelect->Release();
  					pSelect = NULL;
  				}
  				if ( pSelect3 != NULL )
  				{
  					pSelect3->Release();
  					pSelect3 = NULL;
  				}
  				// timj end 4/6/04
    
    			CoUninitialize();
    		}
    
    		catch ( ... ) 
    		{	// shouldn't be here!
    			LOGIT(CLOG_LOG,"Unknown exception occurred during COMthread cleanup\n");
    		}
		}
	}

	hCevent::hCeventCntrl*	pStopEvt = m_evtShutdownNow.getEventControl();
	pStopEvt->triggerEvent();
	hCevent::hCeventCntrl*	pDoneEvt = m_evtInitialized.getEventControl();
	pStopEvt->pendOnEvent(2000);

	delete pStopEvt;
	delete pDoneEvt;
	pCOMthread = NULL;
    
}


/*************************************************************************************************
 *
 *   $History: DDLCommInfc.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
