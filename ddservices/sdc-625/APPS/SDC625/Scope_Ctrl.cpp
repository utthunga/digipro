/************************************************************************************************/
// ScopeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "Scope_Ctrl.h"
#include "WaveEditDlg.h"

#include "SDCthreadSync.h"
#include "Dyn_Controls.h"

#include <wtypes.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define EMPHASIS_WIDTH		2
#define DATA_LINE_WIDTH		0


//int nIndex = 0;
//int nXIndex =0;
//int nCnt = 0;
char chstrIndex[100];
//static int nItemIndex =0;
unsigned uChartLimitIndx;

/* DEBUG */
int scopeCnt=0;
/* end DEBUG */

#define IDC_PLOT_START 15555
#define IDC_PLOT_END   15555 + 100

unsigned uControlId = IDC_PLOT_START;

unsigned uSrcWindowId = IDC_PLOT_START + 1234;

/*COLOR DEFINITIONS*/

#define BLACK   0x000000                        
#define SILVER  0xC0C0C0                        
#define GRAY    0x808080                        
#define WHITE   0xFFFFFF                        
#define MAROON  0x800000                        
#define RED     0xFF0000                        
#define ORANGE  0xFFA500                        
#define PURPLE  0x800080                        
#define FUCHSIA 0xFF00FF                                
#define GREEN   0x008000
#define LIME    0x00FF00
#define OLIVE   0x808000
#define YELLOW  0xFFFF00
#define NAVY    0x000080
#define BLUE    0x0000FF
#define TEAL    0x008080
#define AQUA    0x00FFFF

/*Moved black ish colors to end*/
#define COLORSET    { BLUE,		RED,		GREEN,		PURPLE,	 \
					  NAVY,     MAROON,		LIME,		FUCHSIA, \
					  AQUA,		ORANGE,		TEAL,		BLACK,   \
                      YELLOW,	GRAY,		OLIVE,      SILVER,	 \
					  WHITE	}

//#define COLORCOUNT  17

//const unsigned colors[COLORCOUNT] = COLORSET;

#define CONVERT_COLORREF_TO_OLE(CR)	( (CR & 0xFF) << 16 ) + (CR & 0xFF00) + ( (CR & 0xFF0000) >> 16 ) 

/////////////////////////////////////////////////////////////////////////////
// CColorHandler

const unsigned CColorHandler::m_colors[COLORCOUNT] = COLORSET;

// Constructor
CColorHandler::CColorHandler()
{
	ClearColors();

	m_color = BLACK;
}

// Destructor
CColorHandler::~CColorHandler()
{
}

/* show a spec'd linecolor has been used */
void CColorHandler:: UseColor(unsigned long linecolor)
{
	int i;
	for ( i = 0; i < COLORCOUNT; i++)
	{
		if ( m_colors[i] == linecolor )
		{
			m_used[i] = true;
			return;
		}
	}
	return;
}

unsigned long CColorHandler::GetNextColor(void)
{
	int i;
	
	for ( i = 0; i < COLORCOUNT; i++)
	{
		if ( ! m_used[i]  )
		{
			m_used[i] = true;
			return m_colors[i];
		}
	}
	
	// if we get here, all colors are used
	// wrap around and reuse
	for ( i = 0; i < COLORCOUNT; m_used[i++] = false);
	m_used[0] = true;

	return m_colors[0];
}

UINT32 CColorHandler::GetRealColor(UINT32 ddColor)
{
	if(ddColor == -1) // if the color == -1 then the the DD did not specify a color
	{
		ddColor = GetNextColor(); // Choose a color for the user
	}

	m_color = ddColor;

	return ddColor;
}

UINT32 CColorHandler::GetOLEColor(UINT32 ddColor)
{
	// Get the correct color
	UINT32 color = GetRealColor(ddColor);
	
	// Convert to OLE color and return
	return ( CONVERT_COLORREF_TO_OLE(color) );
}

void CColorHandler::ClearColors(void)	// PAW 03/03/09 void
{
	for(int i = 0; i < COLORCOUNT; m_used[i++]=0);
}

UINT32 CColorHandler::GetCurrentColor()
{
	return m_color;
}

UINT32 CColorHandler::GetCurrentOLEColor()
{
	return ( CONVERT_COLORREF_TO_OLE(m_color) );
}

/////////////////////////////////////////////////////////////////////////////
// stevev - added defines for sizing

#define BAR_SCALE_WIDTH		30
#define BAR_SCALE_HEIGHT	20
#define BAR_SCALE2BAR		 5	/*iComponentX.SetTickMargin(5)*/
#define BAR_SMALL_WIDTH		10
#define BAR_TWIXT_WIDTH		20
#define BAR_LARGE_WIDTH		30

#define BAR_EDGE_MARGIN     15
#define BAR_MAX_MEMBERS      3


/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl

IMPLEMENT_DYNAMIC( CScopeCtrl, CStatic )

//UINT CScopeCtrl::PaintChartComponentProc(LPVOID pParam);// PAW 03/03/09 removed

CScopeCtrl::CScopeCtrl()
{
#ifdef _USE_PLOT_LIB

//	IDC_TESTCHART =123457;
	m_piPlotX=NULL;
	m_pWnd=NULL;
	m_currentWaveList.clear();
	m_currentPointLists.clear();

	m_channelIdxsList.clear();// stevev 08may07 add to track 'em
	m_ListOfLimitIndexLists.clear();// stevev 08may07 add to track 'em
	m_yAxisList.clear();// stevev 08may07 add to track 'em
	// stevev 8may07-notused:m_bIsParentMethodDlg = false;
	m_bRefreshFlag = false;
	m_bReady       = false;

	uControlId +=1;
#ifdef _DEBUG
	m_ControlId = uControlId;
#endif
	m_piPlotX1 = NULL;
//	m_piAngGauge = NULL;		Delete by POB, 9 Aug 2005
//	m_piScopeX = NULL;
//	m_piLinearGauge = NULL;
	chartType = cT_UNKNOWN;
	uSrcWindowId +=1;
	m_iSourcecount = 0;   // Added by POB, 12 Aug 2005
	m_iSrcRectSpan = 0;	  // Added by POB, 12 Aug 2005

	waiting4.push_back(HaveDataEvent.getEventControl());  // stevev 15aug05
	waiting4.push_back(QuitThreadEvent.getEventControl());// stevev 15aug05
	/* Arun 120505 Start of code */

	InitializeCriticalSection(&csPublishedData);

	datapoints.clear();

	/* End of code */

#endif
	CMainFrame * pMainWnd = GETMAINFRAME;// use the real one...(CMainFrame *) AfxGetMainWnd();
	if (pMainWnd->GetDocument())
	{
		m_pDoc = pMainWnd->m_pDoc;
	}
	else
	{
clog<<"Scope did not get a document."<<endl;
		m_pDoc = NULL;
	}

	m_iSubHandle = -1;
	//Added By Anil June 29 2005 --starts here
	m_nCycleTime = 0;
	m_nLENGTH = 0;
	//Added By Anil June 29 2005 --starts here

	hThread     = NULL; // stevev 16aug05
	m_pItemBase = NULL; // stevev 21aug07

	// Initialize the legend buttons to null
	for (int i = 0; i < 10; i++)
	{

		m_Legend[i].legendButton = NULL;
	}
}


/*** stevev 02feb06 - changed the order of execution and removed commented-out code
     see previous versions for old code and old order
	 Note that the code did not change, just when we do each step.
***/
CScopeCtrl::~CScopeCtrl()
{
	RETURNCODE rc = SUCCESS;
#ifdef _USE_PLOT_LIB

/* stevev 02feb06 - change order of execution - (1) - unsubscribe to stop other threads update */	
	if((m_iSubHandle >= 0) && (NULL != m_pDoc) && (NULL != m_pDoc->pCurDev))
	{
TRACE(_T("Scope Unsubscribing handle 0x%x in thread 0x%x\n"),m_iSubHandle,GetCurrentThreadId());
		rc = m_pDoc->pCurDev->unsubscribe(m_iSubHandle);
	}

/* stevev 02feb06 - change order of execution - (2) - execute exit actions */		
	if((NULL != m_pDoc) && (NULL != m_pDoc->pCurDev))
	{
		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			for(ddbItemList_t::iterator iT= m_currentWaveList.begin();iT != m_currentWaveList.end(); iT++)
			{
				hCwaveform *pWave = (hCwaveform*)(*iT);

				rc = pWave->doExitActions();
				if(SUCCESS != rc)
				{//simply log the fact	
				
				LOGIT(CLOG_LOG|UI_LOG,"Waveform (0x%04x)'s Exit Actions Failed or were Aborted",pWave->getID());
				
				}
			}
		//}		
		/** stevev 16may07 - execute refresh actions **/
		hCsource* pSource;
		for(ddbItemList_t::iterator isT = m_currentSourceList.begin(); 
													isT != m_currentSourceList.end(); isT++)
		{
			pSource = (hCsource*)(*isT);

			if(!(pSource->IsValid()))
			{						//Skip the invalid sources
				continue;
			}
			rc = pSource->doExitActions();  
		} // next source
		/** end refresh action execution **/	
	}

/* stevev 02feb06 - change order of execution - (3) - shutdown the other thread */	
	DEBUGLOG(CLOG_LOG,"ScopeControl destructor trigger Quit Event...\n");
	waiting4[1]->triggerEvent();// stevev 15aug05

	while(hThread != NULL)
	{
		systemSleep(10);
	}
	TRACE(_T("control thread exited (~SC)\n"));

/* stevev 02feb06 - change order of execution - (4) - release the iPlot objects */
	if (m_piPlotX)
	{
		m_piPlotX->Release();
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"  Release Plot X Cntl 0x%04x\n",m_ControlId);
#endif
	}

	if(m_piPlotX1)
	{
		m_piPlotX1->Release();
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"  Release Plot X1 Cntl 0x%04x\n",m_ControlId);
#endif
	}

    for(AngGaugeList::iterator iTa = m_AngGaugeList.begin(); iTa != m_AngGaugeList.end();iTa++)
	{
		if(true == (*iTa).bLogGauge)
		{
			(*iTa).piAngLogGauge->Release();
		}
		else
		{
		(*iTa).piAngGauge->Release();
		}
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"  Release Angular Cntl 0x%04x\n",m_ControlId);
#endif
	}
	for(LinearGaugeList::iterator iTg = m_LinearGaugeList.begin(); iTg != m_LinearGaugeList.end();iTg++)
	{
		(*iTg)->Release();
#ifdef _DEBUG
		LOGIT(CLOG_LOG,"  Release Linear Cntl 0x%04x\n",m_ControlId);
#endif
	}

/* stevev 02feb06 - change order of execution - (5) - delete owned windows */	
	CWnd *pWnd = NULL;
	for(vector <CWnd*>::iterator iTw = srcWindowList.begin(); iTw != srcWindowList.end(); iTw)
	{
		pWnd = (CWnd*)(*iTw);
		if (pWnd)
		{
			delete pWnd;
			pWnd = NULL;
		}
		else
		{
			TRACE(_T("Scope Destructor has a Listed Window without a Window.\n"));
		}
	}

	if(m_pWnd != 0)
	{
if (m_pWnd->m_hWnd == 0)
{
	TRACE(_T("no window\n"));
}
/* delete was commented out - Verify...*/
		delete m_pWnd ;
// CStatic does this ... DestroyWindow();
	}

/* stevev 02feb06 - change order of execution - (6) - clear and delete internals */	
	m_currentWaveList.clear();

	m_channelIdxsList.clear();// stevev 08may07 
	m_ListOfLimitIndexLists.clear();// stevev 08may07 
	m_yAxisList.clear();// stevev 08may07 

	m_pDoc = NULL;

	datapoints.clear();

	DeleteCriticalSection(&csPublishedData);

	/* End of code */
	delete waiting4[1];// stevev 15aug05
	delete waiting4[0];// stevev 15aug05

#endif /* _USE_PLOT_LIB */

	// Deal with any unreleased memory
	for (int i = 0; i < 10; i++)
	{
		if (m_Legend[i].legendButton)
		{
			delete m_Legend[i].legendButton;
			m_Legend[i].legendButton = NULL;
		}
	}
}


BEGIN_MESSAGE_MAP(CScopeCtrl, CStatic)
	//{{AFX_MSG_MAP(CScopeCtrl)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_LEGEND_BUTTON, OnLegendButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CScopeCtrl, CStatic) 
	ON_EVENT_RANGE(CScopeCtrl, IDC_PLOT_START, IDC_PLOT_END ,55  /* OnClickLegend */
				   ,OnLegendClick,VTS_I4 VTS_I4)
	ON_EVENT_RANGE(CScopeCtrl, IDC_PLOT_START, IDC_PLOT_END ,33  /* OnBeforeZoomBox  */
				   ,OnZoomBox, VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PBOOL)
END_EVENTSINK_MAP()

BOOL CScopeCtrl::OnPopBox(UINT ctrlID, VARIANT_BOOL * Cancel,long * screenX, long * screenY)
{
	return FALSE;
}
void CScopeCtrl::GetIDsOfNames()
{
/***      REFIID riid = IID_NULL;
	  OLECHAR * rgszNames[2]={L"OnClickLegend",L"OnBeforeZoomBox"};
      UINT cNames = 2;
      LCID lcid = 0;
      DISPID  rgDispId[2* 32];
	  ITypeInfo m_ptinfo = m_piPlotX;

HRESULT rl = DispGetIDsOfNames(m_ptinfo, rgszNames, cNames, rgDispId);
***/
      return ;
}

/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl message handlers

int CScopeCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CStatic::OnCreate(lpCreateStruct) == -1)
		return -1;

	RECT rect;
	GetClientRect(&rect);
	rect.top =rect.top +10;
	rect.left = rect.left +10;
	rect.bottom = rect.bottom -10;
	rect.right = rect.right -10;
((CDialog *)GetParent())->MapDialogRect(&rect);

	m_rect = rect;

	return 0;

}/*End OnCreate*/


int CScopeCtrl::CreateChartControl(hCchart * pChart,bool bIsParentAMeth)
{
	ASSERT(pChart);
	int iRetVal = SUCCESS;
#ifdef _USE_PLOT_LIB
	RECT rect;
	
	// stevev 8may07-notused:if(false == bIsParentAMeth)
	//{
		// stevev 8may07-notused:m_bIsParentMethodDlg = false; //forked from a menu
		rect = m_rect;
	//}
	// stevev 8may07-notused:else
	//{
	// stevev 8may07-notused:	GetClientRect(&rect);
	// stevev 8may07-notused:	m_bIsParentMethodDlg = true; //forked from a method
	//}
	m_pWnd = new CWnd;

	if(NULL==m_pWnd)
	{
		AfxMessageBox(M_UI_SCOPE_NO_CWND);
		return FAILURE;
	}

	chartType = pChart->getChartType();

	switch(chartType)
	{
		case cT_Gauge:
			{
				//delete the m_Wnd - Added by POB, 9 Aug 2005
				if(m_pWnd)
				{
					delete m_pWnd;
					m_pWnd = NULL;
				}
				iRetVal = CreateAngularGauge(pChart, rect);
			}
			break;
		case cT_Horiz:
		case cT_Vert :
			{
				//delete the m_Wnd
				if(m_pWnd)
				{
					delete m_pWnd;
					m_pWnd = NULL;
				}
				iRetVal = CreateLinearGauge(pChart,rect);
			}
			break;
		case cT_Scope:
			{
				iRetVal = CreateScopeChart(rect);
			}
			break;
		case cT_Strip:
			{
				iRetVal = CreateStripChart(rect);

			}
			break;
		case cT_Sweep:
			{
				iRetVal = CreateSweepChart(rect);

			}
			break;
		case cT_Undefined:	
		case cT_UNKNOWN:
		default:
			{
				LOGIT(CERR_LOG,"CScopeCtrl::CreateChartControl got an invalid chart type !!\n");
				return FAILURE;
			}
	}
#else
	/*
	 * This is an example of loading a bitmap in a static frame
	 * It is a place holder for future graphs and charts
	 */	
	BOOL result;
/*DEBUG*/
if ( scopeCnt )	
	result =	m_Bitmap.LoadBitmap(IDB_BITMAP2);//was IDB_BITMAP_TEST);
else
{	scopeCnt++;
	result =	m_Bitmap.LoadBitmap(IDB_BITMAP3);//was IDB_BITMAP_TEST);
}
	HBITMAP m_hBitmap =(HBITMAP) (m_Bitmap);

	SetBitmap( m_hBitmap );

#endif

	return iRetVal;
}/*End CreareChartControl*/

#ifdef _USE_PLOT_LIB
int CScopeCtrl::CreateGraphControl(bool bIsParentAMeth)
{
	RECT rect;

	
#ifdef _USE_PLOT_LIB
	CString LicenseString = "{8D1C01BB-5D2E-4AC8-8E0B-1110A67C73E8}";
	BSTR LicenseKey = LicenseString.AllocSysString();

	// stevev 8may07-notused:if(false == bIsParentAMeth)
	//{
	// stevev 8may07-notused:	m_bIsParentMethodDlg = false; //forked from a menu
		rect = m_rect;
	//}
	// stevev 8may07-notused:else
	//{
	// stevev 8may07-notused:	GetClientRect(&rect);
	// stevev 8may07-notused:	m_bIsParentMethodDlg = true; //forked from a method
	//}
	m_pWnd = new CWnd;

	if(NULL==m_pWnd)
		AfxMessageBox(M_UI_SCOPE_NO_CWND);

	IUnknown *pIUnknown =NULL; 

	long lVal= m_pWnd->CreateControl( iPlotLibrary::CLSID_iXYPlotX /*"{1791C036-8981-492A-BD28-F2331BC9B7C7}"*/
							,_T("HEllO")
							,WS_TABSTOP
							,rect
							,this
							,uControlId//IDC_TESTCHART
							,NULL
							,FALSE
							,LicenseKey);
	if(0==lVal)
	{
		AfxMessageBox(M_UI_SCOPE_CHART_FAIL);
		return 0;
	}
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"  Created Graph Cntl 0x%04x\n",uControlId);
	}
#endif

    m_pWnd->SetFont(this->GetFont());
    
	pIUnknown = m_pWnd->GetControlUnknown ();

	if(NULL==pIUnknown)
	{
		AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
		return 0;
	}
	
	pIUnknown ->QueryInterface (_uuidof(iPlotLibrary::IiXYPlotX) , (LPVOID*)&m_piPlotX);

	/****test - no workee  **
//	double result;
	long dispid;
	HRESULT hr;
	CString cx = "OnClickLegend";
	BSTR szMember = cx.AllocSysString();
	hr = m_piPlotX->GetIDsOfNames(IID_NULL, (OLECHAR FAR*FAR*)&szMember, 1,0, &dispid);
	::SysFreeString(szMember);	
//GetProperty(dispid, VT_R8, (void*)&result);	
//return result;	
********** end test **/

	m_piPlotX->DataView[0]->PutBackGroundColor(0x00FFFFFF);

	m_piPlotX->DataView[0]->PutBackGroundTransparent(false);

	m_piPlotX->PutBackGroundGradientEnabled(TRUE);

	m_piPlotX->PutBackGroundGradientDirection(iPlotLibrary::ifdTopBottom);
	
	m_piPlotX->PutBackGroundGradientStartColor(0x0099FFFF);

	m_piPlotX->PutBackGroundGradientStopColor(0x0);

	m_piPlotX->ToolBar[0]->PutSmallButtons(TRUE);

	m_piPlotX->ToolBar[0]->PutFlatButtons(TRUE);

#else
	/*
	 * This is an example of loading a bitmap in a static frame
	 * It is a place holder for future graphs and charts
	 */	
	BOOL result;
/*DEBUG*/
if ( scopeCnt )	
	result =	m_Bitmap.LoadBitmap(IDB_BITMAP2);//was IDB_BITMAP_TEST);
else
{	scopeCnt++;
	result =	m_Bitmap.LoadBitmap(IDB_BITMAP3);//was IDB_BITMAP_TEST);
}
	HBITMAP m_hBitmap =(HBITMAP) (m_Bitmap);

	SetBitmap( m_hBitmap );

#endif

	return SUCCESS;
}/*End CreateGraphControl*/

#endif

#ifdef _USE_PLOT_LIB

int CScopeCtrl::InitChart(hCchart *pChart, CString Label,ddbItemList_t *pSourceList)
{
	int rc = SUCCESS;

	ASSERT(pChart);

	m_pItemBase = pChart;

	// Get the label for the Chart so it can be displayed on the window that holds the chart control(s)
	// Added by POB, 11 Aug 2005
	SetWindowText(Label);

	DEBUGLOG(CLOG_LOG,"Init Chart Type %s\n",chartTypeStrings[chartType]);
	switch(chartType)
	{
		case cT_Gauge:
			{
				rc = InitAngularGauge(pChart,Label,pSourceList);
			}
			break;
		case cT_Horiz:
		case cT_Vert :
			{
				rc = InitLinearGauge(pChart,Label,pSourceList);
			}
			break;
		case cT_Scope:
			{
				rc = InitScopeChart(pChart,Label,pSourceList);
			}
			break;
		case cT_Strip:	
			{
				rc = InitStripChart(pChart,Label,pSourceList);
			}
			break;
		case cT_Sweep:
			{
				rc = InitSweepChart(pChart,Label,pSourceList);

			}
			break;
		case cT_Undefined:	
		case cT_UNKNOWN:
		default:
			{
				LOGIT(CERR_LOG,"CScopeCtrl::InitChart got an invalid chart type !!\n");
				return FAILURE;
			}
	}

	return rc;


	
	
}/*Endif InitChart*/

int CScopeCtrl::InitGraph(hCgraph *pGraph, CString Label,ddbItemList_t *pWaveList)
{
	int rc = SUCCESS;
	CString tempName;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	BSTR bstrText;
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;


	// Get the label for the graph so it can be displayed on the window control that holds the graph control
	// Added by POB, 11 Aug 2005
	SetWindowText(Label);

	// Set the title with graph label
	// Using the graph's label in the static window
	// NO need to use it again here. - Modified by POB, 8 Aug 2005
	// TO DO:: May want to variable's label or waveforms's member descriptions here
	Label = " ";

	// Set the title with Graph label
	bstrText = Label.AllocSysString();

	m_piPlotX->PutTitleText(bstrText);

	m_piPlotX->PutTitleFontColor(0x00333333);//80% grey

	// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
	//{
		ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
	//}
	
	// unused hCgroupItemList wvList(m_pDoc->pCurDev->devHndl());

	if(pGraph)
	{
		m_pItemBase = pGraph;
		// Initialize the member waveform list
		// POB, 12 Aug 2005 - replaced duplicate code 
		if(InitWaveforms(pGraph))
		{
			return FAILURE;
		}

		if(SUCCESS == rc)
		{

			hCaxis *pXaxis = pGraph->getXAxis();
			if(pXaxis)
			{

			/* stevev 09jul13 - I cannot make the X axis Autosize.  I've done everything in
				the IOComp manual and it doesn't work.  
				I'm saving off the defined information so we can adjust the X axis at display
				time.  If both min & max are defined, no need to redefine at display time.
			**/
				xAxDefined = xadef_none;
				rc = pXaxis->Label(tmpStr);
					
				rc = pXaxis->getMinValue(tmpCV);
				if(SUCCESS == rc && tmpCV.vIsValid)
				{
					m_piPlotX->XAxis[0]->PutMin((double)tmpCV);
					xAxDefined |= xadef_min;

					rc = pXaxis->getMaxValue(tmpCV);
					if(SUCCESS == rc && tmpCV.vIsValid)
					{
						m_piPlotX->XAxis[0]->PutSpan(
									(double)tmpCV - m_piPlotX->XAxis[0]->GetMin());
						xAxDefined |= xadef_max;
					}
				}// leave it none

				if(pXaxis->isLogarithmic())
				{
					m_piPlotX->XAxis[0]->PutScaleType(ipstLog10);
				}
				if(SUCCESS == pXaxis->getUnitString(tmpStr))
				{
					tempName = tmpStr.c_str();
                    TRACE(_T("tempName = %s\n"), tempName );
				}
				else
				{
					tempName = " ";
				}
				bstrText = tempName.AllocSysString();
				m_piPlotX->XAxis[0]->PutTitle(bstrText);

				m_piPlotX->XAxis[0]->PutTitleShow(TRUE);
				m_piPlotX->XAxis[0]->PutTitleMargin(0);
			
			}//endif pAxis
			
			/* stevev 20apr07 - new graph functionality***/
			tmpCV.clear();
			rc = pGraph->getCycleTime(tmpCV);	
			if ( rc != SUCCESS )
			{
				m_nCycleTime = 0;//no refresh
			}
			else
			{
				m_nCycleTime = (int)tmpCV;
			}
			/** end 20apr07 functionality addition **/
		}//endif SUCCESS == rc

	}//endif pGraph

	return SUCCESS;

}/*End InitGraph*/

#endif


void CScopeCtrl::Plot(pointValueList_t xyList, int iChannel)
{
#ifdef _USE_PLOT_LIB
	double xVal,yVal;
	unsigned i;
	for(i = 0; i < xyList.size(); i++)
	{
		xVal = (double)xyList[i].vx;
		yVal = (double)xyList[i].vy;
		m_piPlotX->Channel[iChannel]->AddXY(xVal,yVal);
//		m_piPlotX->Channel[ChannelCnt]->put_DataMarkerShow (i,TRUE);
	}
#endif /* #ifdef _USE_PLOT_LIB */

}/*End Plot*/


// stevev 08may07 - return limit index for tracking
unsigned CScopeCtrl :: Plot(pointValue_t point,int iChannel,unsigned uColor)
{
	/*This guy plots a vertical / horizontal waveform depending upon, which varient
	(vx / vy )  is valid*/

	unsigned uLimitIndx;
#ifdef _USE_PLOT_LIB
	uLimitIndx = m_piPlotX->AddLimit();
	if(point.vx.vIsValid)
	{
		
		m_piPlotX->Limit[uLimitIndx]->Style = iplsLineX;  //was iplsLineY Vibhor: 221204
		m_piPlotX->Limit[uLimitIndx]->Color = uColor;
		m_piPlotX->Limit[uLimitIndx]->Line1Position = (double)point.vx;

	}
	else
	{
		m_piPlotX->Limit[uLimitIndx]->Style = iplsLineY;  //was iplsLineX Vibhor: 221204
		m_piPlotX->Limit[uLimitIndx]->Color = uColor;
		m_piPlotX->Limit[uLimitIndx]->Line1Position = (double)point.vy;
	}
#endif /*#ifdef _USE_PLOT_LIB*/
	return uLimitIndx;
}/*End Plot*/


void CScopeCtrl::ShowControl()
{
	if(m_pWnd)
	{
		m_pWnd->ShowWindow(SW_SHOW);
	}
	if(windowList.size())
	{
		CWnd *pWnd = NULL;
		for(vector<CWnd*>::iterator iTw = windowList.begin(); iTw != windowList.end();iTw++)
		{
			pWnd = (CWnd*)(*iTw);
			pWnd->ShowWindow(SW_SHOW);
		}
	}
}


void CScopeCtrl::HideControl()
{
	if(m_pWnd)
	{
		m_pWnd->ShowWindow(SW_HIDE);
	}
	if(windowList.size())
	{
		CWnd *pWnd = NULL;
		for(vector<CWnd*>::iterator iTw = windowList.begin(); iTw != windowList.end();iTw++)
		{
			pWnd = (CWnd*)(*iTw);
			pWnd->ShowWindow(SW_HIDE);
		}
	}
}

long CScopeCtrl::SetYaxis(hCaxis *pYaxis)
{	
	long lIndex = -1;

#ifdef _USE_PLOT_LIB
	RETURNCODE rc;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	CValueVarient tmpCV;
	BSTR bstrText;
	CString tempName;
	bool isXY = (m_piPlotX != NULL );

	if(pYaxis)
	{			
		if ( isXY ) lIndex = m_piPlotX-> AddYAxis();
		else        lIndex = m_piPlotX1->AddYAxis();

		rc = pYaxis->Label(tmpStr);
			
		rc = pYaxis->getMinValue(tmpCV);

		if(SUCCESS == rc && tmpCV.vIsValid)
		{
			if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutMin((double)tmpCV);
			else        m_piPlotX1->YAxis[lIndex]->PutMin((double)tmpCV);
		}
		rc = pYaxis->getMaxValue(tmpCV);
		if(SUCCESS == rc && tmpCV.vIsValid)
		{
			if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutSpan(((double)tmpCV - m_piPlotX-> YAxis[lIndex]->GetMin()));
			else        m_piPlotX1->YAxis[lIndex]->PutSpan(((double)tmpCV - m_piPlotX1->YAxis[lIndex]->GetMin()));
		}
		if(pYaxis->isLogarithmic())
		{
			if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutScaleType(ipstLog10);
			else        m_piPlotX1->YAxis[lIndex]->PutScaleType(ipstLog10);
		}
		if(SUCCESS == pYaxis->getUnitString(tmpStr))
		{
			tempName = tmpStr.c_str();
		}
		else
		{
			tempName = " ";
		}
		bstrText = tempName.AllocSysString();
		if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutTitle(bstrText);
		else        m_piPlotX1->YAxis[lIndex]->PutTitle(bstrText);

		if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutTitleShow(TRUE);
		else        m_piPlotX1->YAxis[lIndex]->PutTitleShow(TRUE);
		if ( isXY ) m_piPlotX-> YAxis[lIndex]->PutTitleMargin(0);
		else        m_piPlotX1->YAxis[lIndex]->PutTitleMargin(0);
	}//endif pYAxis
#endif
	return lIndex;
}

bool CScopeCtrl::DisplayWaveforms(itemID_t iid)//hCgraph *pGraph,ddbItemList_t waveList)
{// we probably need to see if the symbol number is us, or one of our waveforms..
#ifdef _USE_PLOT_LIB
	bool resized = false;
	int rc = SUCCESS;
	hCwaveform *pWave = NULL;
	unsigned uWaveColor = -1;
	BSTR bstrText;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	CString tempName;
	int iChannelNo = 0, iTmpChIdx;
//n	ddbItemList_t    subsList;
//CValueVarient tmpCV;
	m_piPlotX->RemoveAllChannels();
	m_channelIdxsList.clear();// stevev 08may07
	m_EMPchannelIdxsList.clear();
	m_piPlotX->RemoveAllLimits();
	m_ListOfLimitIndexLists.clear();// stevev 08may07
	unsigned iWaveformIndex = 0;
	pointValueList_t XYList;
	long lIndex = 0;
	
	CValueVarient tmpCV;
	 
	bool bFirstWaveform = true; // set to true if the first waveform has an axis defined 
//	m_piPlotX->RemoveAllYAxes();
//	m_yAxisList.clear();

	vector<unsigned> mtlist;
	vector<unsigned> wrklist;
	
	/* stevev 19apr07 - turn off subscription if there is one */
	if((m_iSubHandle >= 0) && (NULL != m_pDoc) && (NULL != m_pDoc->pCurDev))
	{
TRACE(_T("Scope/Waveform Unsubscribing handle 0x%x in thread 0x%x\n"),m_iSubHandle,GetCurrentThreadId());
		rc = m_pDoc->pCurDev->unsubscribe(m_iSubHandle);
		m_iSubHandle = -1;
	}
	/* end turn off subscription */

	int waveNumber = 0;
	for(ddbItemList_t::iterator iT = m_currentWaveList.begin();iT != m_currentWaveList.end();++iT, waveNumber++)
	{
		pWave = (hCwaveform*)(*iT);
				
		if(!(pWave->IsValid()))
		{						//Skip the invalid sources
			m_channelIdxsList.push_back(-1);
			m_EMPchannelIdxsList.push_back(-1);
			m_ListOfLimitIndexLists.push_back(mtlist);
			m_yAxisList[iWaveformIndex] = -1;

			iWaveformIndex++;
			continue;
		}

		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
		if(false == m_bRefreshFlag)
		{
			rc = pWave->doInitActions();
		}
		// stevev 8may07 - first time needs init followed by refresh,,,else //refresh Waveforms
		//{
		rc = pWave->doRefreshActions();  // There are problems here, comment out for now - POB, 15 Aug 2005
		//}
		//}
/*		else  // This should not happen.  rc should always be zero when it starts the 'for' loop or it is not being handled very well below
		{
			rc = SUCCESS;
		}*/
		
		if(SUCCESS != rc)
		{
			LOGIT(CLOG_LOG|UI_LOG,"Waveform (0x%04x) %s 's Actions Failed or were Aborted",pWave->getID(),tmpStr.c_str());
		/* stevev 8apr05 we have to insert an empty list to make the counts come out right*/		
			XYList.clear();
			if(m_bRefreshFlag && m_currentPointLists.size() > iWaveformIndex)
			{/*if we r under refresh, there will already b an entry for this waveform, 
			   so erase it first , and then inser the current pointList*/
			   m_currentPointLists.erase(m_currentPointLists.begin() + iWaveformIndex);
			   m_currentPointLists.insert((m_currentPointLists.begin() + iWaveformIndex),XYList);
			}
			else
			{ //we are constructing the graph for the first time, simply push back the pointList
				m_currentPointLists.push_back(XYList);
			}

			m_channelIdxsList.push_back(-1);
			m_EMPchannelIdxsList.push_back(-1);
			m_ListOfLimitIndexLists.push_back(mtlist);
			m_yAxisList[iWaveformIndex] = -1;

			iWaveformIndex++; //increment the waveform index
			
			rc = SUCCESS; // clear any error so that we can graph out the next waveform - POB, 18 Oct 2005

			continue;//Don't show this waveform
		}
		// else action success...continue processing this waveform
	
		uWaveColor = -1; //Initialize the source color

		pWave->getLineColor(uWaveColor);// if none, leaves it -1
		
		if ( m_bRefreshFlag )
			rc = pWave->getPointList(XYList,subsList);   // get values for current pointset
		else
			rc = pWave->getNewPointList(XYList,subsList);// recalcs conditionals & references & gets values

		if(rc == SUCCESS && XYList.size() > 0 )
		{			
			iChannelNo = m_piPlotX->AddChannel ();
			m_channelIdxsList.push_back(iChannelNo); 

			hCaxis *pYaxis = pWave->getYAxis();
			if(pYaxis)
			{
				if(true == bFirstWaveform )
				{
					m_piPlotX->RemoveAllYAxes();
					for (unsigned k = 0; k < m_yAxisList.size();k++)
					{	m_yAxisList[k] = -1;  }
				}
				
				lIndex = SetYaxis(pYaxis);
				m_yAxisList[iWaveformIndex] = lIndex;
				//Associate the Y axis with proper channel
//comment out till a response is given from iocomp...
//generates exception				
				m_piPlotX->Channel[iChannelNo]->PutYAxisName(m_piPlotX->YAxis[lIndex]->GetName());
			}
			else
			{
				m_yAxisList[iWaveformIndex] = -1;
			}//endif pYAxis
		
			lineType_t lineType = pWave->getLineType();
			switch(lineType)
			{
				case lT_Undefined:		// 0	
				case lT_Data:			// 1	DATA_LINETYPE		
				case lT_LowLow:			// 2	LOWLOW_LINETYPE		
				case lT_Low:			// 3	LOW_LINETYPE		
				case lT_High:			// 4	HIGH_LINETYPE		
				case lT_HighHigh:		// 5	HIGHHIGH_LINETYPE	
				case lT_Unknown:		// 7
					break;				
				case lT_Trans:			// 6	TRANSPARENT_LINETYPE
					m_piPlotX->Channel[iChannelNo]->PutTraceVisible(FALSE);
					m_piPlotX->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
					m_piPlotX->Channel[iChannelNo]->PutMarkersStyle(ipmsCircle);
					m_piPlotX->Channel[iChannelNo]->PutMarkersVisible(TRUE);
					break;
				case lT_Data1:			//= ( lineTypeOffset + 1):
					m_piPlotX->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 1);
					break;
				case lT_Data2:
					m_piPlotX->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 2);
					break;
				case lT_Data3: //Everything after this is same
				case lT_Data4:
				case lT_Data5:
				case lT_Data6:
				case lT_Data7:
				case lT_Data8:
				case lT_Data9:
					m_piPlotX->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 3);
					break;
			}
		}
		else //Can't Plot this waveform if the point list has no valid points (for any reason)
		{
			LOGIT(CLOG_LOG|UI_LOG,"Waveform (0x%04x) %s has no valid points\n",
														pWave->getID(),pWave->getName().c_str());
		/* stevev 8apr05 we have to insert an empty list to make the counts come out right*/	
			XYList.clear();
			if(m_bRefreshFlag && m_currentPointLists.size() > iWaveformIndex)
			{/*if we r under refresh, there will already b an entry for this waveform, 
			   so erase it first , and then inser the current pointList*/
			   m_currentPointLists.erase(m_currentPointLists.begin() + iWaveformIndex);
			   m_currentPointLists.insert((m_currentPointLists.begin() + iWaveformIndex),XYList);
			}
			else
			{ //we are constructing the graph for the first time, simply push back the pointList
				m_currentPointLists.push_back(XYList);
			}
			m_channelIdxsList.push_back(-1);
			m_EMPchannelIdxsList.push_back(-1);
			m_ListOfLimitIndexLists.push_back(mtlist);
			m_yAxisList[iWaveformIndex] = -1;

			iWaveformIndex++; //increment the waveform index
			continue;
		}
/*		if(SUCCESS == pWave->getLineColor(uWaveColor))               // deleted by POB, 12 Aug 2005
		{
			m_piPlotX->Channel[iChannelNo]->PutColor(uWaveColor);
		}*/
	// Get the color for this line
		m_piPlotX->Channel[iChannelNo]->PutColor(m_colorSrc.GetOLEColor(uWaveColor));

		if(pWave->needsEmphasis())
		{
			m_piPlotX->Channel[iChannelNo]->PutTraceLineWidth(EMPHASIS_WIDTH);
		}

		waveformType_t waveType = pWave->getWaveFrmType();	
			
			

		switch(waveType)
		{
			case wT_Y_Time:
			case wT_Y_X:
				{						
					Plot(XYList,iChannelNo);
					m_ListOfLimitIndexLists.push_back(mtlist);	

					// stevev 09jul13 - deal with xaxis scaling
					double xv = 0.00;
					bool   set= false;
					hCaxis *pXaxis = ((hCgraph *)m_pItemBase)->getXAxis();
#ifdef _DEBUG
	string scopeName = m_pItemBase->getName();
#else
	string scopeName;
#endif
					if(pXaxis)
					{
						rc = pXaxis->getMinValue(tmpCV);
						if(SUCCESS == rc && tmpCV.vIsValid)
						{
							xv = (double)tmpCV;
							set = true;
							DEBUGLOG(CLOG_LOG,"## %s Scope - Axis X min value = %f\n",
								scopeName.c_str(),xv);
						}
						else
						{
						DEBUGLOG(CLOG_LOG,"## %s Scope - No Min-Value\n",scopeName.c_str());
						}
					}
					else
					{
						DEBUGLOG(CLOG_LOG,"## %s Scope - No X axis\n",scopeName.c_str());
					}
					if ( ! set )
					{
						xv = m_piPlotX->Channel[iChannelNo]->GetXMin();
						DEBUGLOG(CLOG_LOG,"## %s Scope - Data X min value = %f\n",
																		scopeName.c_str(),xv);
					}
					m_piPlotX->XAxis[0]->PutMin(xv);
					DEBUGLOG(CLOG_LOG,"###%s ScopeMin set to %f\n",scopeName.c_str(),xv);

					set = false;	
					if(pXaxis)
					{
						rc = pXaxis->getMaxValue(tmpCV);
						if(SUCCESS == rc && tmpCV.vIsValid)
						{
							xv = (double)tmpCV;
							set = true;
							DEBUGLOG(CLOG_LOG,"## %s Scope - Axis X max value = %f\n",
																	scopeName.c_str(),xv);
						}
						else
						{
						DEBUGLOG(CLOG_LOG,"## %s Scope - No Max-Value\n",scopeName.c_str());
						}
					}
					if ( ! set )
					{
						xv = m_piPlotX->Channel[iChannelNo]->GetXMax();
						DEBUGLOG(CLOG_LOG,"## %s Scope - Data X max value = %f\n",
																	scopeName.c_str(),xv);
					}
					xv = xv - m_piPlotX->XAxis[0]->GetMin();
					m_piPlotX->XAxis[0]->PutSpan(xv);
					DEBUGLOG(CLOG_LOG,"###%s ScopeSpan = %f\n",scopeName.c_str(),xv);

					////
				}
				break;
			case wT_Horiz:
			case wT_Vert:
				{
					unsigned t;
					wrklist.clear();
					for(unsigned i = 0; i < XYList.size() ; i++)
					{
						unsigned prevColor = m_piPlotX->Channel[iChannelNo]->GetColor();
							
						t = Plot(XYList[i],iChannelNo,prevColor); 
						wrklist.push_back(t);										
					}
					m_ListOfLimitIndexLists.push_back(wrklist);			
				}
				break;
			case wT_Undefined:
			case wT_Unknown:
			default:
				m_ListOfLimitIndexLists.push_back(mtlist);		
				break;
		}

		EnumTriad_t waveTri;
		waveTri = m_WaveExtraList.at(waveNumber);

		if (waveTri.descS.empty() )
		{
			rc = pWave->Label(tmpStr);
		}
		else
		{
			tmpStr = waveTri.descS;
		}

		tempName = tmpStr.c_str();

		// We need to check to see if the DD only has blanks in the label
		// if so we need to make sure that it is empty when checked below
		CString test = tempName;
		test.Remove(' '); // remove only blank characters

		if(SUCCESS != rc || test.IsEmpty()) // rc == FAILURE when there is no label in the DD
		{
			// It appears that the graph control will not allow an empty string for a legend even when no label is found in the DD.
			// The control will default to "Channel 1, Channel 2, Channel 3, etc.
			// Let's change it to something more practical like Waveform 1,Waveform 2, Waveform3, etc
//			tempName = " ";
			tempName.Format(_T("Waveform %d"), iChannelNo + 1); // iChannelNo is zero based, add 1 so that it will start with one and not zero

			// Fix bug when Label was missing 
			rc = SUCCESS;  // clear any error so that we can graph out the next waveform - POB, 18 Oct 2005
		}

		bstrText = tempName.AllocSysString();

		m_piPlotX->Channel[iChannelNo]->put_TitleText(bstrText);

		/*Store the current point list for this waveform*/
		//if(m_bRefreshFlag) // stevev 04jan07 - empty on initial refresh
		if(m_bRefreshFlag && m_currentPointLists.size() > iWaveformIndex)
		{/*if we r under refresh, there will already b an entry for this waveform, 
		   so erase it first , and then inser the current pointList*/
		   m_currentPointLists.erase(m_currentPointLists.begin() + iWaveformIndex);
		   m_currentPointLists.insert((m_currentPointLists.begin() + iWaveformIndex),XYList);
		}
		else
		{ //we are constructing the graph for the first time, simply push back the pointList
			m_currentPointLists.push_back(XYList);
		}
		XYList.clear();
		rc = pWave->getKeyPoints(XYList,subsList);
		iTmpChIdx = iChannelNo;// channel number of original trace

		if(SUCCESS == rc && XYList.size() > 0)
		{
			iChannelNo = m_piPlotX->AddChannel();
			m_EMPchannelIdxsList.push_back(iChannelNo);
			m_piPlotX->Channel[iChannelNo]->PutColor(m_piPlotX->Channel[iTmpChIdx]->GetColor());
			m_piPlotX->Channel[iChannelNo]->PutVisibleInLegend(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutTraceVisible(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersVisible(TRUE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersStyle(ipmsSquare);	
			Plot(XYList,iChannelNo);
			//	m_piPlotX->Channel[iChannelNo]->PutDataMarkerStyle(ipmsSquare);
		}
		else
		{
			m_EMPchannelIdxsList.push_back(-1);
		}
		XYList.clear();
		
		iWaveformIndex++;

		bFirstWaveform = false;
					
	}//next waveform

	/*Now run another loop on the currentWaveList to plot the KeyPoints for respective waveforms
	  IMP: Here again we'll add a channel for each waveform having a key point list, BUT this 
	  channel, A) won't show up in legend B) won't have a trace */
	/* stevev - 10may07 - moved inside waveform loop
	int iTmpChIdx = 0;
	for(iT= m_currentWaveList.begin();iT != m_currentWaveList.end(); iT++)
	{
		
		pWave = (hCwaveform*)(*iT);

		rc = pWave->getKeyPoints(XYList);

		if(SUCCESS == rc && XYList.size() > 0)
		{
			m_channelIdxsList[iWaveformIndex] = iChannelNo = m_piPlotX->AddChannel();
			m_piPlotX->Channel[iChannelNo]->PutColor(m_piPlotX->Channel[iTmpChIdx]->GetColor());
			m_piPlotX->Channel[iChannelNo]->PutVisibleInLegend(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutTraceVisible(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersVisible(TRUE);
			m_piPlotX->Channel[iChannelNo]->PutMarkersStyle(ipmsSquare);	
			Plot(XYList,iChannelNo);
			//	m_piPlotX->Channel[iChannelNo]->PutDataMarkerStyle(ipmsSquare);
		}
		iChannelNo++;
		iTmpChIdx++;

	}//endfor
	**** end of move to inside waveform loop ******/

	if(	m_nCycleTime != 0 && subsList.size() > 0)
	{
		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle, subsList, (hCpubsub*)this, m_nCycleTime);
	}
	//normal in graphs...
	//else
	//{
	//	LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	//}

	return  resized;
#else
return false;
#endif

}/*End DisplayWaveforms*/

void CScopeCtrl::StaleDynamic(void)
{
#ifdef _USE_PLOT_LIB
	hCwaveform *pWave = NULL;
	if ( m_nCycleTime != 0 )   return;// we'll be doing pub/sub

	if ( m_piPlotX == NULL || m_iSubHandle >= 0 )
	{	return;  // we are a chart so we use pub/sub technology or a graph with a cycletime
	}
	// else we are a graph without cycletime
	ddbItemList_t::iterator iT;
	for( iT = m_currentWaveList.begin(); iT != m_currentWaveList.end(); iT++)
	{
		pWave = (hCwaveform*)(*iT);
		if (pWave)
			pWave->markDynStale();
	}
#endif
}

void CScopeCtrl::UpdateWaveforms(itemID_t iid) // this is update item
{
#ifdef _USE_PLOT_LIB
	hCwaveform *pWave = NULL;
	unsigned wfIndex = 0;

	for(ddbItemList_t::iterator iT = m_currentWaveList.begin(); iT != m_currentWaveList.end(); iT++)
	{
		pWave = (hCwaveform*)(*iT);

		if(!(pWave->IsValid()))
		{						//Skip the invalid forms
			wfIndex++;
			continue;
		}
		bool isApt = pWave->isThisApoint(iid);
		if ( pWave->getID() == iid || isApt)
			UpdateOneForm(wfIndex);// one waveform at a time

		wfIndex++;

	}// next waveform
#endif
}
void CScopeCtrl::UpdateOneForm(unsigned fromDex) 
{
#ifdef _USE_PLOT_LIB
#ifdef _DEBUG
	if      (m_currentWaveList.size() != m_channelIdxsList.size())
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: %s Waveform internal channel list is out of sync.(%d vs %d)\n",
											(m_pItemBase)?m_pItemBase->getName().c_str():"Unknown",	
											m_currentWaveList.size(), m_channelIdxsList.size());
	else if (m_currentWaveList.size() != m_ListOfLimitIndexLists.size())
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Waveform internal limit list is out of sync.\n");
	else if (m_currentWaveList.size() != m_yAxisList.size())
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Waveform internal axis list is out of sync.\n");
	// else all match correctly
#endif

	RETURNCODE rc = SUCCESS;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008

	if (fromDex >= m_channelIdxsList.size() )
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Waveform updating a non-existent channel.(%d indexing %d channels)\n",
											fromDex, m_channelIdxsList.size());
		return;
	}
	int iChannelNo     = m_channelIdxsList[fromDex];
	hCwaveform *pWave  = (hCwaveform *)m_currentWaveList[fromDex];
	hCaxis *pYaxis     = pWave->getYAxis();;
	unsigned AxisIdx   = m_yAxisList[fromDex];
	uintList_t* pLmtLst= &(m_ListOfLimitIndexLists[fromDex]);
	unsigned LimitIdx  = 0;
	unsigned EMPchannel= m_EMPchannelIdxsList[fromDex];
	pointValueList_t XYList;

	rc = pWave->doRefreshActions();
	if(SUCCESS != rc)
	{
		LOGIT(CLOG_LOG|UI_LOG,"Waveform (0x%04x) %s 's Actions Failed or were Aborted",
			pWave->getID(),tmpStr.c_str());
	}

	rc = pWave->getPointList(XYList,subsList);// get values for current pointset

	if(rc == SUCCESS && XYList.size() > 0 )
	{
		//m_piPlotX->Channel[iChannelNo].DeletePoints(pC->GetCount());
		//m_piPlotX->Channel[iChannelNo].DeletePoints(m_piPlotX->Channel[iChannelNo].GetCount());   

		if(pYaxis && ((int)AxisIdx) >= 0)
		{
			m_piPlotX->DeleteYAxis(AxisIdx);
			m_yAxisList[fromDex] = SetYaxis(pYaxis);// reset with new

			//Associate the Y axis with proper channel
			m_piPlotX->Channel[iChannelNo]->PutYAxisName(
											m_piPlotX->YAxis[m_yAxisList[fromDex]]->GetName());
		
		}//endif pYAxis
		if ( pLmtLst->size() > 0 )
		{
			uintList_t::reverse_iterator LmtIT;
			//for ( uintLstIT_t LmtIT = pLmtLst->begin(); LmtIT != pLmtLst->end();++LmtIT)
			for ( LmtIT = pLmtLst->rbegin(); LmtIT != pLmtLst->rend();++LmtIT)
			{
				LimitIdx = (unsigned) (*LmtIT);
				if (((int)LimitIdx) >= 0)
					m_piPlotX->DeleteLimit(LimitIdx);
			}
			pLmtLst->clear();
			m_ListOfLimitIndexLists[fromDex].clear();
		}
		
		waveformType_t waveType = pWave->getWaveFrmType();

		
		switch(waveType)
		{
			case wT_Y_Time:
			case wT_Y_X:
				{
					int c = m_piPlotX->Channel[iChannelNo]->GetCount();
					m_piPlotX->Channel[iChannelNo]->DeletePoints(c);
					Plot(XYList,iChannelNo);		
				}
				break;
			case wT_Horiz:
			case wT_Vert:
				{
					unsigned t;
					for(unsigned i = 0; i < XYList.size() ; i++)
					{
						unsigned prevColor = m_piPlotX->Channel[iChannelNo]->GetColor();
							
						t = Plot(XYList[i],iChannelNo,prevColor); 
						m_ListOfLimitIndexLists[fromDex].push_back(t);										
					}			
				}
				break;
			case wT_Undefined:
			case wT_Unknown:
			default:
				break;
		}// end switch waveformtype

	}
	else
	{
		LOGIT(CLOG_LOG|UI_LOG,"Waveform (0x%04x) %s has no valid points\n",
			pWave->getID(),tmpStr.c_str());
	}
	if ( ((int)EMPchannel) >= 0 )
	{// we have keypoints
		
		int c = m_piPlotX->Channel[EMPchannel]->GetCount();
		m_piPlotX->Channel[EMPchannel]->DeletePoints(c);
	
		XYList.clear();
		rc = pWave->getKeyPoints(XYList,subsList);
		Plot(XYList,EMPchannel);		
	}
#endif
}// end UpdateOneForm

bool CScopeCtrl::RefreshWaveforms(itemID_t iid)
{// return true if size change
	bool bret = false;
#ifdef _USE_PLOT_LIB
	m_bRefreshFlag = true;
   
	/*Before redrawing remove all Y axes except the first one*/
	long lYAxisCnt = m_piPlotX->GetYAxisCount();
	
	for(long lIndex = lYAxisCnt; lIndex > 1; lIndex --)
	{
		m_piPlotX->DeleteYAxis(lIndex - 1);
	}
	
	m_colorSrc.ClearColors();

	bret = DisplayWaveforms(iid);

	m_bRefreshFlag = false;
#endif
    return bret;
}/*End RefreshWaveforms*/

/*OnPopupMenuLegend OnLegendRgtClick(UINT ctrlID, unsigned lIndex)
void::OnOnPopupMenuLegendIplotx1(long Index, BOOL FAR* Cancel, long ScreenX, long ScreenY)
{
	CMenu menu;
	UpdateData(FALSE);
	VERIFY(menu.LoadMenu(IDR_MYPOPUPMENU));
	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ScreenX, ScreenY, AfxGetMainWnd());
	*Cancel = TRUE;
}*/

BOOL CScopeCtrl::OnLegendClick(UINT ctrlID, unsigned lIndex)
{
#ifdef _USE_PLOT_LIB
	RETURNCODE rc;
	hCreferenceList tmpRefListX(m_pDoc->pCurDev->devHndl());
	hCreferenceList tmpRefListY(m_pDoc->pCurDev->devHndl());
//	hCreferenceList::iterator iT;
//stevev 11sep13	unsigned uHdlg;
	UINT64 uHdlg;
	pointValueList_t XYList;
	pointValue_t tmpPoint;
	CWaveEditDlg *pWvEdDlg = NULL;

	int iEditDlgRet = -1; //-1 for a cancel or ok without change, 0 for ok with change

	//NOTE : Add code to handle waveform editing here
	if(lIndex < 0 || lIndex >= m_currentWaveList.size())
		return FALSE; //Index out of bounds
	
	hCwaveform *pWave = NULL;

	pWave = (hCwaveform*)m_currentWaveList.at(lIndex);

	if(pWave)
	{
		rc = pWave->getHandling(uHdlg);

		if(SUCCESS == rc)
		{
			if(uHdlg == READ_HANDLING)
			{ //The waveform is non editable !!!, so just return
				return TRUE;
			}
			else
			{//Waveform is editable but allow editing if and only if all referred vars are editable
				XYList = m_currentPointLists.at(lIndex);
				switch(pWave->getWaveFrmType())
				{
					case wT_Y_Time:
					case wT_Horiz: //These have only Y values
						{
							//Got the references
							tmpRefListY = pWave->getCurrentYRefs();

							pWvEdDlg = new CWaveEditDlg(m_pDoc->pCurDev->devHndl(),pWave->getWaveFrmType(),XYList,NULL,&tmpRefListY);
							
							iEditDlgRet = pWvEdDlg->DoModal();

						}
						break;
					case wT_Y_X:
						{
							tmpRefListX = pWave->getCurrentXRefs();
							tmpRefListY = pWave->getCurrentYRefs();
							pWvEdDlg = new CWaveEditDlg(m_pDoc->pCurDev->devHndl(),pWave->getWaveFrmType(),XYList,&tmpRefListX,&tmpRefListY);
							
							iEditDlgRet = pWvEdDlg->DoModal();
				
						}
						break;
					
					case wT_Vert:
						{
							
							tmpRefListX = pWave->getCurrentXRefs();
							pWvEdDlg = new CWaveEditDlg(m_pDoc->pCurDev->devHndl(),pWave->getWaveFrmType(),XYList,&tmpRefListX,NULL);
							
							iEditDlgRet = pWvEdDlg->DoModal();

					
						}
						break;
					case wT_Undefined:
					case wT_Unknown:
						break;

				}//End Switch
				
				if(pWvEdDlg)
				{
					delete pWvEdDlg;
					pWvEdDlg = NULL;
				}
				if(0 == iEditDlgRet)
				{
					//m_pDoc->UpdateAllViews( NULL, UPDATE_WINDOW_ITEM, (CObject *)0);
					// changed to the following 06dec05 stevev					
					m_pDoc->UpdateAllViews( NULL, UPDATE_STRUCTURE, (CObject *)0);
				}
			
			
			}//end else

		
		}//endif SUCCESS
		else //failed or not supplied
			return FALSE;

	}
	else //could not retrieve waveform
		return FALSE;
#endif // _USE_PLOT_LIB
	return TRUE;
}

BOOL CScopeCtrl::OnZoomBox(UINT ctrlID, long * Left,long * Top,long * Right,long * Bottom,VARIANT_BOOL * Cancel)
{
#ifdef _USE_PLOT_LIB
	//refresh (fetch new data & redraw) waveforms here
	if(TRUE == m_bRefreshFlag)
		return TRUE;
	* Cancel = TRUE;
	RefreshWaveforms(0);
#endif
	return TRUE;
}

bool CScopeCtrl::UpdatePlot(long type,long SymID)
{// return true if scope size changed
	bool br = false;
#ifdef _USE_PLOT_LIB
	if(m_piPlotX) //Refresh is required only for Graphs
	{
		if (type == UPDATE_STRUCTURE)
		{
			br = RefreshWaveforms(SymID);
		}
		else
		if (type == UPDATE_ONE_VALUE)
		{// value will not change size
			UpdateWaveforms(SymID);
		}
		else
		{// skip unknown type
		}
	}
#endif
	return br;
}


void CScopeCtrl::DisplayChart(hCchart *pChart)
{
	ASSERT(pChart);
#ifdef _USE_PLOT_LIB
	switch(chartType)
	{
		case cT_Gauge:
			{
				DisplayAngularGauge();
			}
			break;
		case cT_Horiz:
		case cT_Vert :
			{
				DisplayLinearGauge();
			}
			break;
		case cT_Scope:	
			{
				DisplayScopeChart();
			}
			break;
		case cT_Strip:
			{	
				DisplayStripChart();
			}
			break;	
		case cT_Sweep:
			{
				DisplaySweepChart();

			}
			break;
		case cT_Undefined:	
		case cT_UNKNOWN:
		default:
			{
				LOGIT(CERR_LOG,"CScopeCtrl::DisplayChart got an invalid chart type !!\n");
				return;
			}
	}
#endif
	return;
}

// PaintChartComponentProc is now a static member of CScopeCtrl class - POB, 12 Aug 2005
UINT CScopeCtrl::PaintChartComponentProc(LPVOID pParam)
{  
	RETURNCODE rc = SUCCESS;
//	CComPtr<iPlotLibrary::IiPlotX> PlotComponent;
	static int iSpanCtr  = 0; // This is like a time tick
	static int iCycleCtr = 0; // This will be multiple of the span, incremented every
							 // time iSpanCtr = Span
	int iChannelIndex = 0;
	int iPtrIndex = 0;
//    ThreadData* pData = (ThreadData*)pParam;
//	PlotComponent     = pData->PlotComponent;
	dataPointList_t currDataLst;
	bool bPublishedData = false;

	double	nCycleTime = 0.0;
	double	nCurrentXaxisPos = 0.0;
	int nXaxisLabel = 0;
	int nSweepCount = 0;
	int nTickIndex = 0;
	int nLENGTH = 0;
	

	CScopeCtrl *pScpCtrl = (CScopeCtrl*)pParam;
	nCycleTime = pScpCtrl->m_nCycleTime;
	nCycleTime /= 1000;
	if ( nCycleTime <= 0 ) nCycleTime = 1.0;// default to 1 sec for now********************************
	nLENGTH = pScpCtrl->m_nLENGTH;

	while (nLENGTH == 0 )
	{
		systemSleep(10);	// wait for the other thread to catch up (and finish initializing)
		nLENGTH = pScpCtrl->m_nLENGTH;
		if (nLENGTH < 0 )
		{
			nLENGTH = pScpCtrl->m_nLENGTH = 0;
		}
	}
	DWORD waitResult;
	
	/* Arun 120505 Start of code */

//	while(WaitForSingleObject(pScpCtrl->QuitThreadEvent,0))
	//   will return 0 when the quitthread is found, else timeout and others are bigger...
	// stevev 15aug05 replaced while(systemWaitSingleObject(pScpCtrl->QuitThreadEvent,100))
	for EVER
	{
		waitResult = systemWaitMultiple(pScpCtrl->waiting4, hCevent::FOREVER);	// stevev 15aug05
		if (ISQUITSIGNAL( pScpCtrl-> ) )
		{
			break;// out of forever loop
		}
		else
		if ( ! ISDATA( pScpCtrl-> ) )
		{// wait abandoned or other error
			LOGIT(CERR_LOG,"Scope Wait for Data returned an error.\n");
			break;
		}
		// else - process ==========end stevev 15aug05 ===========================================
		/** stevev 16may07 - execute refresh actions **/
#ifdef _USE_PLOT_LIB
		hCsource* pSource;
		for(ddbItemList_t::iterator isT = pScpCtrl->m_currentSourceList.begin(); 
												isT != pScpCtrl->m_currentSourceList.end(); isT++)
		{
			pSource = (hCsource*)(*isT);

			if(!(pSource->IsValid()))
			{						//Skip the invalid sources
				continue;
			}
			rc = pSource->doRefreshActions();  
		} // next source
		/** end refresh action execution **/
		EnterCriticalSection(&(pScpCtrl->csPublishedData));
		if(pScpCtrl->datapoints.size())
		{
			currDataLst = pScpCtrl->datapoints.front();
			pScpCtrl->datapoints.erase(pScpCtrl->datapoints.begin());
			bPublishedData = true;

		}
		LeaveCriticalSection(&(pScpCtrl->csPublishedData));

		if(!bPublishedData)
			continue;

		/* End of code */

		//dataPointList_t currDataLst = pScpCtrl->dataList;

		dataPointList_t::iterator iT;

		dataPoint_t wrkPt;
		
		switch(pScpCtrl->chartType)
		{
		case cT_Gauge:
			{
				// NOTE: Each iPtrIndex is the individual arrow (pointers) on the circular gauge
				//		 Each value in the currDataLst represents a subscribed VARIABLE in the DD
				//		 There is NOT a one to one correlation between gauges an arrows (ie, there could
				//       two pointers on the 1st gauge and one pointer on the 2nd gauge

				iT = currDataLst.begin();		// Set the data iterator at the begining of the data

				for(CScopeCtrl::AngGaugeList::iterator iTa = pScpCtrl->m_AngGaugeList.begin(); iTa != pScpCtrl->m_AngGaugeList.end(); iTa++)
				{
					CScopeCtrl::AngGauge angGauge = (*iTa);
					

					iPtrIndex = 0;		// Each Gauge control will have its 1st pointer as index zero, so reinitialize


				    for(; iT != currDataLst.end(); iT++, iPtrIndex++) // The pointer and data needs to increment together
					{
						wrkPt = (*iT);
						
						if (iPtrIndex <  angGauge.nSrcMembers)	// Have we reached the same number of pointers as we do members?
						{
							if(angGauge.bLogGauge == true)
							{
								angGauge.piAngLogGauge->SetPointersPosition(iPtrIndex,(double)wrkPt.vd);
							}
							else
							{
							angGauge.piAngGauge->SetPointersPosition(iPtrIndex,(double)wrkPt.vd);	// set the pointer with its value

							}
						}
						else
						{
							break;	// There are no more members in this source, go to the next gange control
						}
					}
	
				}
			}
			break;
		case cT_Horiz:
		case cT_Vert :
			{
				// NOTE:  Each PtrIndex represents a single bar graph
				//        Each value in the currDataLst represents a subscribed VARIABLE in the DD
				//        There is a one to one relationship between PtrIndex & currDataLst
				for(iT = currDataLst.begin(),iPtrIndex = 0;iT != currDataLst.end(); iT++ ,iPtrIndex++)
				{
					wrkPt = (*iT);
#ifdef _USE_PLOT_LIB
					pScpCtrl->m_LinearGaugeList.at(iPtrIndex)->PutPosition((double)wrkPt.vd);
#endif
				}
			}
			break;
		/* Arun 120505 Start of code */
		/* Sweep, Strip and Scope chart (eDDL) */
		case cT_Scope:
		{
			//Way to manupulate:
			//Check Whether ur X axis is less than the span defined. 
			//If true then Put the variable points on the Grapg
			//Else We have crossed the Span ....
			
			if(nCurrentXaxisPos  < (pScpCtrl->m_piPlotX1->XAxis[0]->Span))
			{
				//nCurrentXaxisPos ++; //Increament by the Cycle Time//Commented by Anil
				for(iT = currDataLst.begin(),iChannelIndex = 0;
					iT != currDataLst.end();                    iT++ ,iChannelIndex++)
				{
					wrkPt = (*iT);
					//AddYElapsedSeconds((double)wrkPt.vd);
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->
						AddXY((double)nCurrentXaxisPos ,(double)(wrkPt.vd)); 
					// stevev 12aug05 TRACE(_T("nCurrentXaxisPos : %u\n"), nCurrentXaxisPos );
				}
				nCurrentXaxisPos += nCycleTime;
			}
			else
			{
				//we need to fill the graph till the Span 
				//exactly at lenght(nLENGTH) this crashes So value has to be little less than nLENGTH and fill it up
				int iTempCount = pScpCtrl->m_piPlotX1->XAxis[0]->GetTickListCount();
				float ffraction = (float)nLENGTH/((iTempCount -1)*10000);
				for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
				{
					wrkPt = (*iT);
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->AddXY((double)nLENGTH-ffraction ,(double)wrkPt.vd); //AddYElapsedSeconds((double)wrkPt.vd);
					//TRACE(_T("nCurrentXaxisPos : %u\n"), nLENGTH-0.00000000000001);
				}

				//Now Sweep has to be increased and nCurrentXaxisPos should be the number which 
				//remains after nLENGTH
				nSweepCount++;
				nCurrentXaxisPos = nCurrentXaxisPos -nLENGTH ;
				nXaxisLabel = nSweepCount*nLENGTH;//needs fix
			
				//For Scope u need to Clear all through the nLENGTH
				for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
				{
					wrkPt = (*iT);
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->Clear(); 
				}
				

				//Now apply the Label for the next weep
				if(nSweepCount!= 0) //Apply the label from only second sweep as First sweep label is by default ( ie 10 - 100)
					{
					
//hold						pScpCtrl->m_piPlotX1->XAxis[0]->PutTickListCustom(TRUE);

					for( int icount =0; icount<iTempCount;icount++)
					
					{
						sprintf(chstrIndex,"%d",nXaxisLabel+ icount*(nLENGTH/(iTempCount - 1)));
//hold						pScpCtrl->m_piPlotX1->XAxis[0]->PutTickListItemLabel(icount,chstrIndex); //Apply the Label
					}
				
				}
		
				
				//now we have to fill from zero to the remaining, hence do thid as belo
				if(nCurrentXaxisPos>0)
				{
					for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
				{
						wrkPt = (*iT);
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->AddXY((double)0 ,(double)wrkPt.vd); 
						//TRACE(_T("nCurrentXaxisPos : %u\n"), 0 );
				}
					for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
					{
						wrkPt = (*iT);
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->AddXY((double)nCurrentXaxisPos ,(double)wrkPt.vd); 
						//TRACE(_T("nCurrentXaxisPos : %u\n"), nCurrentXaxisPos );
					}
					}
				}
		}
		break;
		case cT_Strip:	
		{
			for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
			{
				wrkPt = (*iT);
				pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->AddXY((double)nCurrentXaxisPos ,(double)wrkPt.vd); 
				//TRACE(_T("nCurrentXaxisPos : %u\n"), nCurrentXaxisPos );
			}
			//nCurrentXaxisPos ++;
			nCurrentXaxisPos += nCycleTime;

		}
		break;
		case cT_Sweep:
		{	//Way to manipulate:
			//Check Whether ur X axis is grater than the nTickIndex*(nLENGTH/10) that means u need
			//  to label that interval 
			//If so, u may have situation where u may have to label more than one interval..hence
			//  this while loop				
			int iTempCount = pScpCtrl->m_piPlotX1->XAxis[0]->GetTickListCount();
			if(nCurrentXaxisPos >= (nTickIndex*(nLENGTH/10)) && (nSweepCount!= 0))
			{				
				while(nCurrentXaxisPos >= (nTickIndex*(nLENGTH/10)))
				{
					pScpCtrl->m_piPlotX1->XAxis[0]->PutTickListCustom(TRUE);
					sprintf(chstrIndex,"%d",nXaxisLabel + nTickIndex*(nLENGTH/10));
					pScpCtrl->m_piPlotX1->XAxis[0]->PutTickListItemLabel(nTickIndex,chstrIndex);
					if(	nTickIndex >=  (iTempCount - 1))
						break;
					nTickIndex = nTickIndex +1;
				}
			}

			//Way to manipulate:
			//Check Whether ur X axis is less than the span defined. 
			//If true then Put the variable points on the Grapg
			//Else We have crossed the Span ....
			if(nCurrentXaxisPos  < (pScpCtrl->m_piPlotX1->XAxis[0]->Span))
			{

				for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
				{
					wrkPt = (*iT);
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->DataY[(long)nCurrentXaxisPos ] = (double)wrkPt.vd;
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->PutDataEmpty((long)nCurrentXaxisPos ,false);
					pScpCtrl->m_piPlotX1->Limit[uChartLimitIndx]->Line1Position = nCurrentXaxisPos ;

				}

				nCurrentXaxisPos += nCycleTime;//Added By Anil
			}
			else
			{
				//we need to fill the graph till the Span 
				//exactly at lenght(nLENGTH) this crashes So value has to be little less than nLENGTH and fill it up
				float ffraction = (float)nLENGTH/((iTempCount -1)*10000);
				for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
				{
					wrkPt = (*iT);						
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->DataY[(long)(nLENGTH -ffraction)] = (double)wrkPt.vd;
					pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->PutDataEmpty((long)(nLENGTH -ffraction),false);
					pScpCtrl->m_piPlotX1->Limit[uChartLimitIndx]->Line1Position = (double)nLENGTH - ffraction;

				}
				//Now Sweep has to be increased and nCurrentXaxisPos should be the number which remains after nLENGTH
				nCurrentXaxisPos = nCurrentXaxisPos -nLENGTH ;
				nSweepCount++;				
				nXaxisLabel = nSweepCount*nLENGTH;
				nTickIndex = 0;

				//now we have to fill from zero to the remaining, hence do thid as belo
				if(nCurrentXaxisPos>0)
				{
					for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
					{
						wrkPt = (*iT);
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->DataY[0] = (double)wrkPt.vd;
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->PutDataEmpty(0 ,false);
						pScpCtrl->m_piPlotX1->Limit[uChartLimitIndx]->Line1Position = 0 ;
					}
					for(iT = currDataLst.begin(),iChannelIndex = 0;iT != currDataLst.end(); iT++ ,iChannelIndex++)
					{
						wrkPt = (*iT);
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->DataY[(int)nCurrentXaxisPos ] = (double)wrkPt.vd;
						pScpCtrl->m_piPlotX1->Channel[iChannelIndex]->PutDataEmpty((int)nCurrentXaxisPos ,false);
						pScpCtrl->m_piPlotX1->Limit[uChartLimitIndx]->Line1Position = nCurrentXaxisPos ;
					}
				}
			}
		}
		break;
		/* End of Sweep, Strip and Scope chart (eDDL) */
		/* End of code */
		case cT_Undefined:	
		case cT_UNKNOWN:
		default:
			{
				LOGIT(CERR_LOG,"PaintChartComponentProc got an invalid chart type !!\n");
				return FAILURE;
			}
		}// endswitch
	
	bPublishedData = false;
	
	/*		if((double)iSpanCtr == PlotComponent->XAxis[0]->GetSpan())
		{
			iCycleCtr ++;
			PlotComponent->XAxis[0]->PutMin((double)iSpanCtr*iCycleCtr);
			iSpanCtr = 0;
		}
	
	TRACE(_T("SpanCtr = %d\n"),iSpanCtr);
	iSpanCtr++;
	*/
#endif
	}//end of while

	pScpCtrl->hThread = NULL; // we're about to kill it
	
	return 0;
}



int CScopeCtrl::publish(dataStructList_t dsL)
{
	int rc = SUCCESS;
		
	static bool bPaintThreadStarted = false;
	
//	PlotComponent = m_piPlotX1;
//	thrdData.dataList = dsL.at(0).dpL;

	dataList = dsL.at(0).dpL;

//	TRACE(_T("Got a publish @ %d\n"),dsL.at(0).timestamp);

//	if(!bPaintThreadStarted)
//	{
//		AfxBeginThread(PaintChartComponentProc, this, THREAD_PRIORITY_NORMAL);
//		bPaintThreadStarted = true;
//	}
	//TODO Add code here to extract the data values and display
#ifdef _DEBUG
	dataPointList_t::iterator dplIT;
	TRACE(L"Subcription got:\n");
	LOGIT(CLOG_LOG,L"Subcription got:\n");
	for (dplIT = dataList.begin(); dplIT != dataList.end(); ++ dplIT)
	{
		dataPoint_t * pPv = &(*dplIT);
		TRACE(L"    item 0x%04x  value = %s\n",pPv->iID,((wstring)(pPv->vd)).c_str());
		LOGIT(CLOG_LOG,L"    item 0x%04x  value = %s\n",pPv->iID,((wstring)(pPv->vd)).c_str());
	}
#endif
#ifdef _USE_PLOT_LIB
	if ( m_piPlotX != NULL )
	{// we are a graph - return, updated from data values
		return rc;
	}
	/* Arun 120505 Start of code */
	EnterCriticalSection(&csPublishedData);
	datapoints.push_back(dataList);
	LeaveCriticalSection(&csPublishedData);
	/* End of code */
#endif
	waiting4[0]->triggerEvent();

	return rc;
}


/*Vibhor 250205: Start of Code*/

int CScopeCtrl::CreateStripChart(RECT rect)
{
	CString LicenseString = "{307D3160-CE91-4613-9BEF-C55598920802}";
	BSTR LicenseKey = LicenseString.AllocSysString();

	IUnknown *pIUnknown =NULL;
#ifdef _USE_PLOT_LIB
	long lVal= m_pWnd->CreateControl( iPlotLibrary::CLSID_iPlotX/*"{1791C036-8981-492A-BD28-F2331BC9B7C7}"*/
							,_T("HEllO")
							,WS_TABSTOP
							,rect
							,this
							,uControlId//IDC_TESTCHART
							,NULL
							,FALSE
							,LicenseKey);
	if(0==lVal)
	{
		AfxMessageBox(M_UI_SCOPE_STCHART_FAIL);
		return FAILURE;
	}
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"  Created Strip Cntl 0x%04x\n",uControlId);
	}
#endif

	pIUnknown = m_pWnd->GetControlUnknown ();
	if(NULL==pIUnknown)
	{
		AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
		return FAILURE;
	}
	
	pIUnknown ->QueryInterface (_uuidof(iPlotLibrary::IiPlotX) , (LPVOID*)&m_piPlotX1);


	m_piPlotX1->DataView[0]->PutBackGroundColor(0x00FFFFFF);

	m_piPlotX1->DataView[0]->PutBackGroundTransparent(false);

	m_piPlotX1->PutBackGroundGradientEnabled(TRUE);

	m_piPlotX1->PutBackGroundGradientDirection(iPlotLibrary::ifdTopBottom);
	
	m_piPlotX1->PutBackGroundGradientStartColor(0x00C0C0C0);	// POB, 16 Aug 2005 - Changed background color to differentiate between charts and graphs

	m_piPlotX1->PutBackGroundGradientStopColor(0x0);

	m_piPlotX1->ToolBar[0]->PutSmallButtons(TRUE);

	m_piPlotX1->ToolBar[0]->PutFlatButtons(TRUE);

	// we spin off the thread to handle the published data
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) PaintChartComponentProc, // the func
		(LPVOID) (this),			 // pass the scopectrl instance
		0,						// starts not suspended
		&dwThreadID);			// returns threadId

	if (hThread == NULL)
	{
		clog << "control thread creation failed."<<endl;
		return FAILURE;
	}
	else
	{
		clog << "control thread creation successful.1"<<endl;
		return SUCCESS;
	}
#endif
	return SUCCESS;

}/*End CreateStripChart()*/

int CScopeCtrl::CreateScopeChart(RECT rect)
{

	/* Arun 120505 Start of code */
	CString LicenseString = "{307D3160-CE91-4613-9BEF-C55598920802}";
	BSTR LicenseKey = LicenseString.AllocSysString();
#ifdef _USE_PLOT_LIB
	IUnknown *pIUnknown =NULL;

	long lVal= m_pWnd->CreateControl( iPlotLibrary::CLSID_iPlotX/*"{1791C036-8981-492A-BD28-F2331BC9B7C7}"*/
							,_T("HEllO")
							,WS_TABSTOP
							,rect
							,this
							,uControlId//IDC_TESTCHART
							,NULL
							,FALSE
							,LicenseKey);
	if(0==lVal)
	{
		AfxMessageBox(M_UI_SCOPE_SCCHART_FAIL);
		return FAILURE;
	}
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"  Created Chart X Cntl 0x%04x\n",uControlId);
	}
#endif

	pIUnknown = m_pWnd->GetControlUnknown ();
	if(NULL==pIUnknown)
	{
		AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
		return FAILURE;
	}
	
	pIUnknown ->QueryInterface (_uuidof(iPlotLibrary::IiPlotX) , (LPVOID*)&m_piPlotX1);

	m_piPlotX1->DataView[0]->PutBackGroundColor(0x00FFFFFF);

	m_piPlotX1->DataView[0]->PutBackGroundTransparent(false);

	m_piPlotX1->PutBackGroundGradientEnabled(TRUE);

	m_piPlotX1->PutBackGroundGradientDirection(iPlotLibrary::ifdTopBottom);
	
	m_piPlotX1->PutBackGroundGradientStartColor(0x00C0C0C0);	// POB, 16 Aug 2005 - Changed background color to differentiate between charts and graphs

	m_piPlotX1->PutBackGroundGradientStopColor(0x0);

	m_piPlotX1->ToolBar[0]->PutSmallButtons(TRUE);

	m_piPlotX1->ToolBar[0]->PutFlatButtons(TRUE);

	// we spin off the thread to handle the published data
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) PaintChartComponentProc, // the func
		(LPVOID) (this),			 // pass the scopectrl instance
		0,						// starts not suspended
		&dwThreadID);			// returns threadId

	if (hThread == NULL)
	{
		clog << "control thread creation failed."<<endl;
		return FAILURE;
	}
	else
	{
		clog << "control thread creation successful.2"<<endl;
	return SUCCESS;
	}
#endif
	return SUCCESS;
	/* End of code */

}/*End CreateScopeChart()*/


int CScopeCtrl::CreateSweepChart(RECT rect)
{
	/* Arun 120505 Start of code */

	CString LicenseString = "{307D3160-CE91-4613-9BEF-C55598920802}";
	BSTR LicenseKey = LicenseString.AllocSysString();
#ifdef _USE_PLOT_LIB
	IUnknown *pIUnknown =NULL;

	long lVal= m_pWnd->CreateControl( iPlotLibrary::CLSID_iPlotX/*"{1791C036-8981-492A-BD28-F2331BC9B7C7}"*/
							,_T("HEllO")
							,WS_TABSTOP
							,rect
							,this
							,uControlId//IDC_TESTCHART
							,NULL
							,FALSE
							,LicenseKey);
	if(0==lVal)
	{
		AfxMessageBox(M_UI_SCOPE_SWCHART_FAIL);
		return FAILURE;
	}
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG,"  Created Sweep Cntl 0x%04x\n",uControlId);
	}
#endif

	pIUnknown = m_pWnd->GetControlUnknown ();
	if(NULL==pIUnknown)
	{
		AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
		return FAILURE;
	}
	
	pIUnknown ->QueryInterface (_uuidof(iPlotLibrary::IiPlotX) , (LPVOID*)&m_piPlotX1);

	m_piPlotX1->DataView[0]->PutBackGroundColor(0x00FFFFFF);

	m_piPlotX1->DataView[0]->PutBackGroundTransparent(false);

	m_piPlotX1->PutBackGroundGradientEnabled(TRUE);

	m_piPlotX1->PutBackGroundGradientDirection(iPlotLibrary::ifdTopBottom);
	
	m_piPlotX1->PutBackGroundGradientStartColor(0x00C0C0C0);	// POB, 16 Aug 2005 - Changed background color to differentiate between charts and graphs

	m_piPlotX1->PutBackGroundGradientStopColor(0x0);

	m_piPlotX1->ToolBar[0]->PutSmallButtons(TRUE);

	m_piPlotX1->ToolBar[0]->PutFlatButtons(TRUE);

	// we spin off the thread to handle the published data
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) PaintChartComponentProc, // the func
		(LPVOID) (this),			 // pass the scopectrl instance
		0,						// starts not suspended
		&dwThreadID);			// returns threadId

	if (hThread == NULL)
	{
		clog << "control thread creation failed."<<endl;
		return FAILURE;
	}
	else
	{
		clog << "control thread creation successful.3"<<endl;
		return SUCCESS;
	}
#endif
	return SUCCESS;

	/* End of code */

}/*End CreateSweepChart()*/

int CScopeCtrl::CreateAngularGauge(hCchart * pChart, RECT rect)
{
#ifdef _USE_PLOT_LIB
	RECT gaugeRect;
	CString LicenseStringAng = "{C5412DDA-2E2F-11D3-85BF-00105AC8B715}";//changed the string name Anil September 19 2005
	BSTR LicenseKeyAng = LicenseStringAng.AllocSysString();//changed the string name Anil September 19 2005
	CString LicenseStringAngLog = "{E5F63BB3-D2A6-11D3-85C2-00A0CC3A58C9}";
	BSTR LicenseKeyAngLog = LicenseStringAngLog.AllocSysString();
	RECT srcRect;
	int rc = SUCCESS;
	int iSourceIndex = 0;
	int iSourcecount = 0;
	int iSrcRectSpan,iMemRectHorizSpan,iMemRectVertSpan;
	unsigned uSourceColor = -1;
	isAnalogLibrary::IiAngularGaugeX *piAngGauge;	// New code to allow more than one source to be displayed per GAUGE chart
	iProfessionalLibrary::IiAngularLogGaugeX *piAngLogGauge;//Anil September 19 2005
//	AngGauge angGauge;		
	ddbItemList_t::iterator iT;
	hCsource *pSource = NULL;	

	iSrcRectSpan = iMemRectHorizSpan = iMemRectVertSpan = 0;

//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	// Initialize the member source list
	// POB, 12 Aug 2005 - replaced duplicate code 
	if(InitSources(pChart))
	{
		return FAILURE;
	}
	
	srcRect = rect;	// This is our 1st time through so use the Chart's rectangle as the Source's - POB, 12 Aug 2005
	
	windowList.clear();

	for( iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
			iSourceIndex++;
			continue;
		}

		// Create the region that the source will occupy - POB, 12 Auge 2005
		CreateSourceRect(srcRect);

		CWnd *pSrcWnd = NULL;		// Added by POB, 8 Aug 2005
				
		pSrcWnd = new CWnd;			// Added by POB, 8 Aug 2005

		/*Create Static box for Source*/
		int iRetCode = pSrcWnd->Create(_T("STATIC")	// Modified by POB, 8 Aug 2005, make it a static control
									  ,M_UI_MT_STR
									  // Don't use group boxes, they can be confused with MENU of STYLE GROUP
									  ,WS_VISIBLE | WS_CHILD |SS_CENTER/*| WS_BORDER*//*| BS_GROUPBOX*/	// Modified by POB, 8 Aug 2005
									  ,/*rect*/srcRect										// Modified by POB, 9 Aug 2005
									  ,this
									  ,uSrcWindowId);
		if(0 == iRetCode)
			return FAILURE;

		// Make sure we use the same font from in this object
		// is used for this source sub-control - Added by POB, 8 Aug 2005
		pSrcWnd->SetFont(this->GetFont());

		// Initialize the source label w/ a generic string
		pSrcWnd->SetWindowText(_T("Source Label"));

		gaugeRect.top = 15;
		gaugeRect.left = 15;
		//gaugeRect.right = rect.right - rect.left - 10;
		//gaugeRect.bottom = rect.bottom - rect.top - 10;
		gaugeRect.right = srcRect.right - srcRect.left - 10;
		gaugeRect.bottom = srcRect.bottom - srcRect.top - 10;

		IUnknown *pIUnknown =NULL;

		CWnd *pGaugeWnd = new CWnd;
		hCaxis *pYaxis = NULL;
		pYaxis = pSource->getYAxis();
		AngGauge angGauge;
		if(pYaxis /*&& pYaxis->IsValid()*/)
		{
			if(pYaxis->isLogarithmic())
			{
				angGauge.bLogGauge = true;	
			}
		}

		///////////////////////   Show the labels of the VARIABLES for each Source ///////////////////////////////////////
		RECT srcMemRect;

		srcMemRect.left = srcRect.left + 20;				
		srcMemRect.top = srcRect.bottom - 10;
		srcMemRect.right = srcRect.right - 20;	
		srcMemRect.bottom = srcMemRect.top + 20;


		CWnd * pSrcMemberWnd = new CWnd;
		iRetCode = pSrcMemberWnd->Create(_T("BUTTON")	// Modified by POB, 5 Aug 2005, make it a static control						
		  ,M_UI_MT_STR
		  ,STYLE_BUTTON//WS_VISIBLE | WS_CHILD |SS_CENTER
		  ,srcMemRect
		  ,this
		  ,IDC_LEGEND_BUTTON);

		pSrcMemberWnd->SetWindowText(M_UI_SCOPE_LEGEND);

		// Make sure we use the same font from in this object
		// is used for this source member control
		pSrcMemberWnd->SetFont(this->GetFont());

	
		m_Legend[iSourceIndex].legendButton = (CButton *) pSrcMemberWnd;;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//		CWnd *pGaugeWnd = new CWnd;

		if(angGauge.bLogGauge == true)
		{
			long lVal= pGaugeWnd->CreateControl(iProfessionalLibrary::CLSID_iAngularLogGaugeX
									,_T("HEllO")
									,WS_TABSTOP
									,gaugeRect
									,pSrcWnd
									,uControlId//IDC_TESTCHART
									,NULL
									,FALSE
									,LicenseKeyAngLog);
			if(0==lVal)
			{
				AfxMessageBox(M_UI_SCOPE_ANGUAGE_FAIL);
				return FAILURE;
			}
			#ifdef _DEBUG
			else
			{
				LOGIT(CLOG_LOG,"  Created Angular Cntl 0x%04x in 0x04x\n",uControlId,m_ControlId);
			}
			#endif

			pIUnknown = pGaugeWnd->GetControlUnknown ();
			if(NULL==pIUnknown)
			{
				AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
				return FAILURE;
			}
			
			pIUnknown ->QueryInterface (_uuidof(iProfessionalLibrary::IiAngularLogGaugeX) , (LPVOID*)&piAngLogGauge);

			if (piAngLogGauge == NULL)
			{
				return FAILURE;
			}
			angGauge.piAngLogGauge = piAngLogGauge;

		}
		else
		{
		long lVal= pGaugeWnd->CreateControl(isAnalogLibrary::CLSID_iAngularGaugeX
								,_T("HEllO")
								,WS_TABSTOP
								,gaugeRect
								,pSrcWnd
								,uControlId//IDC_TESTCHART
								,NULL
								,FALSE
									,LicenseKeyAng);
		if(0==lVal)
		{
			AfxMessageBox(M_UI_SCOPE_ANGUAGE_FAIL);
			return FAILURE;
		}
#ifdef _DEBUG
		else
		{
			LOGIT(CLOG_LOG,"  Created Angular Cntl 0x%04x in 0x04x\n",uControlId,m_ControlId);
		}
			#endif

		pIUnknown = pGaugeWnd->GetControlUnknown ();
		if(NULL==pIUnknown)
		{
			AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
			return FAILURE;
		}
		
		pIUnknown ->QueryInterface (_uuidof(isAnalogLibrary::IiAngularGaugeX) , (LPVOID*)&piAngGauge);

		if (iSourceIndex == 0)
		{
		//			m_piAngGauge = piAngGauge;
			}
			angGauge.piAngGauge = piAngGauge;
		}
		
//		m_AngGaugeList.push_back(piAngGauge); //push it on the AngGaugeList;
		windowList.push_back(pGaugeWnd);

		// Put data on the AngGaugeList
//		angGauge.piAngGauge = piAngGauge; //Anil September 19 2005 commented and moved in respective if else
		angGauge.pSrcWnd = pSrcWnd;
		angGauge.nSrcMembers = 0;
		angGauge.srcRect = srcRect;

		m_AngGaugeList.push_back(angGauge);
		
	   /* We're done with one varlist of one source, move on source rect for next one */
		//srcRect.left = srcRect.right;
		//srcRect.right = srcRect.right + iSrcRectSpan;
		uSrcWindowId++;
		iSourceIndex++;

    }
	// we spin off the thread to handle the published data
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) PaintChartComponentProc, // the func
		(LPVOID) (this),			 // pass the scopectrl instance
		0,						// starts not suspended
		&dwThreadID);			// returns threadId

	if (hThread == NULL)
	{
		DEBUGLOG(CLOG_LOG,"Angular control thread creation failed.\n");
		return FAILURE;
	}
	else
	{		
		DEBUGLOG(CLOG_LOG,"  Angular control thread creation successful.4\n");
		m_nLENGTH = -1;
		return SUCCESS;
	}
	//Added By Anil June 28 2005 --Ends here

#endif

	return SUCCESS;
}


int CScopeCtrl::CreateLinearGauge(hCchart * pChart,RECT rect)
{
#ifdef _USE_PLOT_LIB
	int rc = SUCCESS;
	CString LinearGaugeLicenseString = "{C5412DED-2E2F-11D3-85BF-00105AC8B715}";
	BSTR LinearGaugeLicenseKey = LinearGaugeLicenseString.AllocSysString();

	isAnalogLibrary::IiLinearGaugeX *piLinearGauge = NULL;

	IUnknown *pIUnknown =NULL;
	wstring tmpStr, hepStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	tstring srcCaptionStr = M_UI_MT_STR;			// Modfied by POB, 5 Aug 2005 
	bool bSourceLogarithmic = false;
	hCsource *pSource = NULL;
	hCaxis *pYaxis = NULL;
	RECT srcRect,memberRect,flippedRect; //Vibhor 060405: Added flippedRect
	int /*iSrcRectSpan,*/iMemRectHorizSpan,iMemRectVertSpan;

	/*iSrcRectSpan =*/ iMemRectHorizSpan = iMemRectVertSpan = 0;

	int iMemMargin  = 0;
	int iBarWidth   = 0;// is major width
	int iMinTickLen = 0;

	ddbItemList_t varPtrList;
	// stevev01dec06-use the scope control version::ddbItemList_t subsList;
	long lVal;
	CValueVarient tmpCV;
	/* Calculate number of components required */
	int ivCnt,iSourceMemberIndex = 0;
	int iSourceIndex = 0;
//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	hCgroupItemList srcMemberList(m_pDoc->pCurDev->devHndl());
	//CColorHandler  colorSrc;	POB, 12 Aug 2005 - this is now a member of the class

	unsigned uSourceColor = -1;

	// Initialize the member source list
	// POB, 12 Aug 2005 - replaced duplicate code  
	if(InitSources(pChart))
	{
		return FAILURE;
	}
	else
	{
		DEBUGLOG(CLOG_LOG,"  Creating Linear Gage, Init'd sources.\n");
		m_nLENGTH = -1;
	}

	srcRect = rect;	// This is our 1st time through so use the Chart's rectangle as the Source's - POB, 12 Aug 2005
	
	windowList.clear();
	subsList.clear();//stevev 04jun07 - moved from inside of loop

	for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
//			iSourceIndex++;		// POB, 12 Aug 2005, not using it right now
			continue;
		}
		bSourceLogarithmic = false;

		// Get the label for the source so it can be displayed on the window
		pSource->Label(srcCaptionStr);	// Added by POB, 5 Aug 2005

		rc = pSource->getMemberList(srcMemberList);

		if(rc == SUCCESS)
		{
			rc = srcMemberList.getItemPtrs(varPtrList);
			if(SUCCESS == rc && varPtrList.size() > 0)
			{	
				int validMemberCount = 0;
				ddbItemList_t::iterator iTv;
				for(iTv = varPtrList.begin();iTv != varPtrList.end(); iTv++) //Vibhor 060405: Modified condition
				{
					if ((*iTv)->IsValid())
					{
						validMemberCount++;
					}
				}
				if (validMemberCount == 0) // none are valid
				{				
//					iSourceIndex++;		   // POB, 12 Aug 2005, not using it right now
					continue;// next source
				}

				pYaxis = pSource->getYAxis();
				if(pYaxis /*&& pYaxis->IsValid()*/)
				{
					if(pYaxis->isLogarithmic())
					{
						bSourceLogarithmic = true;	
					}
				}
				/**************** TODO ***************
				 else - set the axis to auto scale
				 *************************************/

				// Create the region that the source will occupy - POB, 12 Auge 2005
				CreateSourceRect(srcRect);

				CWnd *pSrcGrpWnd = NULL;
				
				pSrcGrpWnd = new CWnd;
				// generate the source window
				int iRetCode = pSrcGrpWnd->Create(_T("STATIC")	// Modified by POB, 5 Aug 2005, make it a static control						
								  ,M_UI_MT_STR
							      // Don't use group boxes, they can be confused with MENU of STYLE GROUP
								  ,WS_VISIBLE | WS_CHILD |SS_CENTER /*| BS_GROUPBOX*/	// Modified by POB, 5 Aug 2005
								  ,srcRect
								  ,this
								  ,uSrcWindowId);

//TRACE(_T("    Source:#%2d  l=%4d, t=%4d, r= %4d, b=%4d"),iSourceIndex, // POB, 12 Aug 2005, not using source index right now
//	  srcRect.left, srcRect.top, srcRect.right, srcRect.bottom);
			
				///////////////////////   Show the labels of the VARIABLES for each Source ///////////////////////////////////////
				RECT srcMemRect;

				srcMemRect.left = srcRect.left + 20;				
				srcMemRect.top = srcRect.bottom - 10;
				srcMemRect.right = srcRect.right - 20;	
				srcMemRect.bottom = srcMemRect.top + 20;


				CWnd * pSrcMemberWnd = new CWnd;
				iRetCode = pSrcMemberWnd->Create(_T("BUTTON")	// Modified by POB, 5 Aug 2005, make it a static control						
				  ,M_UI_MT_STR
				  ,STYLE_BUTTON//WS_VISIBLE | WS_CHILD |SS_CENTER
				  ,srcMemRect
				  ,this
				  ,IDC_LEGEND_BUTTON);

				pSrcMemberWnd->SetWindowText(M_UI_SCOPE_LEGEND);

				// Make sure we use the same font from in this object
				// is used for this source member control
				pSrcMemberWnd->SetFont(this->GetFont());

			
				m_Legend[iSourceIndex].legendButton = (CButton *) pSrcMemberWnd;;

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				// Make sure we use the same font from in this object
				// is used for this source sub-control - Added by POB, 8 Aug 2005
				pSrcGrpWnd->SetFont(this->GetFont());

				if(validMemberCount > BAR_MAX_MEMBERS)
				{
					validMemberCount = BAR_MAX_MEMBERS;
				}

				// Set the title for the Source' Window
				pSrcGrpWnd->SetWindowText(srcCaptionStr.c_str());
				
				/***** TODO:  this needs to be improved!!!!*********/				
				unsigned width;// needs to handle height too!!!!!!!
				pChart->getWidth(width);
				int iBarWidth  = 0;
				int iBarMargin = 0;
				
				if (width > 3 )  //  DFLTWID_INDX - MEDIUM or more
				{
					iBarWidth = BAR_LARGE_WIDTH;
					iBarMargin= BAR_EDGE_MARGIN;
				}
				else if (width == 3 )  // SMALL
				{
					iBarWidth = BAR_TWIXT_WIDTH;
					iBarMargin= (BAR_EDGE_MARGIN/3)*2;
				}
				else if (width < 3 )  //Less than SMALL
				{
					iBarWidth = BAR_SMALL_WIDTH;
					iBarMargin= (BAR_EDGE_MARGIN/3);
				}
				iMinTickLen = (iBarWidth/2)-1;
				/********* improve above calculation ****************/


				if(chartType == cT_Vert)
				{
					iMemRectHorizSpan = iBarWidth + BAR_SCALE2BAR;// width incr per bar
						//(srcRect.right- srcRect.left - BAR_SCALE_WIDTH )/ validMemberCount;
					iMemRectVertSpan  = srcRect.bottom - srcRect.top;

					iMemMargin = // edge to first member (with scale)
					((srcRect.right- srcRect.left)-
					 ((validMemberCount * iMemRectHorizSpan) + BAR_SCALE_WIDTH )  
					)	/ 2 ;// center it
					// calc first rect

					memberRect.left   = iMemMargin;
					memberRect.top    = BAR_EDGE_MARGIN;// we have to have room for the title
					memberRect.right  = iMemMargin + iMemRectHorizSpan + BAR_SCALE_WIDTH ;// width?
					memberRect.bottom = iMemRectVertSpan -  iBarMargin              ;// height?
TRACE(_T("--  VERT\n"));
				}
				else
				{
					iMemRectVertSpan = iBarWidth + BAR_SCALE2BAR;
						// (srcRect.bottom- srcRect.top-BAR_SCALE_HEIGHT)/validMemberCount;
					iMemRectHorizSpan = srcRect.right- srcRect.left - 5;//5 right margin
					
					iMemMargin = // edge to first member (with scale)
					((srcRect.bottom- srcRect.top) -
					 (	(validMemberCount * iMemRectVertSpan) + BAR_SCALE_HEIGHT  )
					) / 2 ;// center it

					memberRect.bottom = (srcRect.bottom- srcRect.top) - iMemMargin;//iMemRectVertSpan  -5;
					memberRect.left   = iBarMargin;
					memberRect.top    = memberRect.bottom - iMemRectVertSpan  - BAR_SCALE_WIDTH ;
					memberRect.right  = iMemRectHorizSpan;
TRACE(_T("-- HORIZ\n"));
				}


				// srcCaptionStr = "";	// Modfied by POB, 5 Aug 2005 		
				uSourceColor = -1; //Initialize the source color

				pSource->getLineColor(uSourceColor);// if none, leaves it -1
				// stevev 04jun07 - moved this outside the loop...subsList.clear();//stevev01dec06
				for(ivCnt = 0,iSourceMemberIndex=0,iTv = varPtrList.begin();
				   (ivCnt < validMemberCount) && iTv != varPtrList.end(); 
				   iTv++,iSourceMemberIndex++) //Vibhor 060405: Modified condition
				{
					CWnd *pWnd = NULL;
					piLinearGauge = NULL;
/*Vibhor 060405: Start of Code Modification*/
					flippedRect = memberRect; 
					if(chartType == cT_Horiz && ivCnt == 0) // Handle the screw-up in the control
					{
						//flippedRect.right = memberRect.bottom;
						//flippedRect.bottom = memberRect.right;
						flippedRect.right  = memberRect.bottom - memberRect.top;// + BAR_SCALE_HEIGHT+BAR_SCALE2BAR;
						flippedRect.bottom = memberRect.top + memberRect.right - memberRect.left-3;
					}
					else if (chartType == cT_Horiz && ivCnt > 0)
					{
						flippedRect.right = memberRect.bottom - memberRect.top + BAR_SCALE2BAR;// assume height
						flippedRect.bottom = memberRect.top + memberRect.right - memberRect.left-3;// assume width relation
					}
/*Vibhor 060405: End of Code Modification*/
					if((*iTv)->IsValid())
					{
						bool stringFull = false;
						if(!bSourceLogarithmic)
						{
							/*iComponentX.SetShowTickLabels(TRUE);
							iComponentX.SetShowTicksAxis(TRUE);
							iComponentX.SetShowTicksMajor(TRUE);
							iComponentX.SetShowTicksMajor(TRUE);
							
							 iComponentX.SetOrientationTickMarks(1);  //iosTopLeft/iosBottomRight 

							
							*/

							pWnd = new CWnd;
							lVal = pWnd->CreateControl(isAnalogLibrary::CLSID_iLinearGaugeX
									,_T("HEllO")
									,WS_TABSTOP
									,flippedRect  //Vibhor 060405: Changed
									,pSrcGrpWnd
									,uControlId
									,NULL
									,FALSE
									,LinearGaugeLicenseKey);
							if(0==lVal)
							{
								AfxMessageBox(M_UI_SCOPE_LNGUAGE_FAIL);
								return FAILURE;
							}
#ifdef _DEBUG
							else
							{
								LOGIT(CLOG_LOG,"  Created Linear Cntl 0x%04x in 0x04x\n",uControlId,m_ControlId);
							}
#endif

							pIUnknown = pWnd->GetControlUnknown ();
							if(NULL==pIUnknown)
							{
								AfxMessageBox(M_UI_SCOPE_NO_IUNKNOWN);
								return FAILURE;
							}

								
							pIUnknown->QueryInterface (_uuidof(isAnalogLibrary::IiLinearGaugeX) , (LPVOID*)&piLinearGauge);

						//	piLinearGauge->PutEndsMargin(7);
							if(chartType == cT_Horiz)
							{
								piLinearGauge->PutOrientation(ioHorizontal);
//temp								piLinearGauge->PutOffsetX(-7);
								piLinearGauge->PutAutoCenter(TRUE);
							
TRACE(_T("    Member:#%2d  l=%4d, t=%4d, r= %4d, b=%4d\n"),ivCnt,
	  memberRect.left, memberRect.top, memberRect.right, memberRect.bottom);							
TRACE(_T("    Member:#%2d  l=%4d, t=%4d, r= %4d, b=%4d\n"),ivCnt,
	  flippedRect.left, flippedRect.top, flippedRect.right, flippedRect.bottom);
							}
							else
							{
//temp								piLinearGauge->PutOffsetY(-7);
								piLinearGauge->PutAutoCenter(TRUE);
							}
							
							piLinearGauge->PutPointerStyle(ilgpsColorBar); //3 
/*							if(-1 != uSourceColor) //if the source has color defined
							{
								piLinearGauge->PutPointerColor(uSourceColor);
							}
							else // if == -1
							{
								piLinearGauge->PutPointerColor(colorSrc.GetNextColor());
							}*/
							
							// Get the color for this bar graph
							piLinearGauge->PutPointerColor(m_colorSrc.GetOLEColor(uSourceColor));

							if(pYaxis /*&& pYaxis->IsValid()*/)
							{
								rc = pYaxis->getMinValue(tmpCV);
								if(SUCCESS == rc && tmpCV.vIsValid)
									piLinearGauge->PutPositionMin((double)tmpCV);
								rc = pYaxis->getMaxValue(tmpCV);
								if(SUCCESS == rc && tmpCV.vIsValid)
									piLinearGauge->PutPositionMax((double)tmpCV);
							}
							piLinearGauge->PutTickMajorCount(6);  // was 11 (for 10 tick marks) - POB, 4 April 2005
							piLinearGauge->PutTickMinorCount(1);  // was  4 - POB, 4 April 2005
							
							piLinearGauge->PutTickMajorLength(iBarWidth);  // set scale AND bar width
							piLinearGauge->PutTickMinorLength((iBarWidth/2)-1);  // was 20
							
							piLinearGauge->PutTickMajorStyle(isAnalogLibrary::ibsLowered);
							piLinearGauge->PutTickMinorAlignment(/*itmnaCenter*/itmnaOutside);
							piLinearGauge->PutTickMinorStyle(isAnalogLibrary::ibsRaised);
/* temporary				piLinearGauge->PutShowTicksAxis(FALSE);
*/



piLinearGauge->PutShowTicksAxis(TRUE);
if(ivCnt>0)
{
piLinearGauge->PutShowTickLabels(FALSE);
//piLinearGauge->PutShowTicksAxis(TRUE);
piLinearGauge->PutShowTicksMajor(TRUE);
}
//else
//{
//}
if(chartType == cT_Horiz)
{
piLinearGauge->PutOrientationTickMarks(isAnalogLibrary::iosBottomRight);  //iosTopLeft / iosBottomRight 
//piLinearGauge->PutBorderStyle(isAnalogLibrary::ibsLowered); 
}
else
{
piLinearGauge->PutOrientationTickMarks(isAnalogLibrary::iosTopLeft);  //iosTopLeft / iosBottomRight 
}

							m_LinearGaugeList.push_back(piLinearGauge); //push it on the linearGaugeList;
							windowList.push_back(pWnd);
						}
					/*	else
						{
							//create log gauge
						}
					*/	
						//  Get the member description for each source member
						//  tmpStr = srcMemberList.at(iSourceMemberIndex).getDesc().procureVal(); // Added back by POB, 19 Oct 2005
						
						//  Get the variable label for the source member
						hCreference & itemRef = srcMemberList.at(iSourceMemberIndex).getRef(); // Added by POB, 27 Oct 2005

						itemRef.getItemLabel(tmpStr, stringFull,hepStr); // Added by POB, 27 Oct 2005
						
						m_Legend[iSourceIndex].srcMember[iSourceMemberIndex].strName = tmpStr.c_str();
						
						// Now save the color that this source member is using
						m_Legend[iSourceIndex].srcMember[iSourceMemberIndex].uColor = m_colorSrc.GetCurrentOLEColor();

						//move the memberRect for next member
						if(chartType == cT_Vert)
						{
							memberRect.left = memberRect.right;
							memberRect.right = memberRect.right + iMemRectHorizSpan ;
						}
						else
						{
							memberRect.bottom = memberRect.top;
							memberRect.top    = memberRect.bottom - iMemRectVertSpan;
						}
						ivCnt++;
						uControlId++;
					/*	if(0 == iSourceMemberIndex)  // Modfied by POB, 5 Aug 2005
						{
							srcCaptionStr = tmpStr;
						}
						else
						{
							srcCaptionStr = srcCaptionStr + " && " + tmpStr;
						}*/

						subsList.push_back(*iTv);

					}//endif IsValid
				
				}//endfor iTv (next member)
			
			// Group box for source deleted, POB - 4 August 2005
			//pSrcGrpWnd->SetWindowText(_T(srcCaptionStr.c_str()));

			/*We're done with one varlist of one source, move on source rect for next one*/
			//srcRect.left = srcRect.right;
			//srcRect.right = srcRect.right + m_iSrcRectSpan;

			uSrcWindowId++;
			//windowList.push_back(pSrcGrpWnd);
			}//endif SUCCESS == rc && varPtrList.size() > 0  else, just skip to the next source


		}//rc == SUCCESS, else, just skip to the next source
		
		iSourceIndex++; // POB, 12 Aug 2005, not using it right now

	}//endfor
/*** Moved to DisplayLinearGauge() by stevev01dec06
*	if(	subsList.size() > 0)
*	{
*		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
*	}
*	else
*	{
*		
*		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
*	
*	}
**** end Moved ************/
//Added By Anil June 28 2005 --starts here
	hThread = CreateThread(
		NULL,					// no security
		0,						// default stack size
		(LPTHREAD_START_ROUTINE) PaintChartComponentProc, // the func
		(LPVOID) (this),			 // pass the scopectrl instance
		0,						// starts not suspended
		&dwThreadID);			// returns threadId

	if (hThread == NULL)
	{
		DEBUGLOG(CLOG_LOG, "paint control thread creation failed.\n");
		return FAILURE;
	}
	else
	{
		DEBUGLOG(CLOG_LOG, "paint control thread creation successful.5\n");
		return SUCCESS;
	}

	//Added By Anil June 28 2005 --Ends here
#endif
	return SUCCESS;
	
}
/*Vibhor 250205: End of Code*/

/*Vibhor 010305: Start of Code*/

int CScopeCtrl::InitStripChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList)
{
#ifdef _USE_PLOT_LIB
	RETURNCODE rc = SUCCESS;

	CString tempName;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	BSTR bstrText;
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;
	
	// Using the chart's label in the static window for consistency with the meter charts
	// NO need to use it again here. - Modified by POB, 8 Aug 2005
	// TO DO:: May want to source's label here.  Currently, only the source's member descriptions are shown as legends
	Label = " ";
	
	// Set the title with Chart label
	
	bstrText = Label.AllocSysString();

	m_piPlotX1->PutTitleText(bstrText);

	m_piPlotX1->PutTitleFontColor(0x00333333);//80% grey

	// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
	//{
		ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
	//}

//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	if(pChart)
	{
		// Initialize the member source list
		// POB, 12 Aug 2005 - replaced duplicate code  
		if(InitSources(pChart))
		{
			DEBUGLOG(CLOG_LOG|CERR_LOG,"Chart Init sources failed.\n");
			return FAILURE;
		}

		if(SUCCESS == rc)
		{
			rc = pChart->getLength(tmpCV);
			m_nLENGTH = (((tmpCV).vValue).iIntConst)/1000;
			DEBUGLOG(CLOG_LOG,"Strip Chart set length to %d\n",m_nLENGTH);
			if(SUCCESS == rc && tmpCV.vIsValid)
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)tmpCV/1000);
			}
			else
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)600); //default
			}
			tmpCV.clear();

			rc = pChart->getCycleTime(tmpCV);

			//Added By Anil June 29 2005 --starts here			
			m_nCycleTime = (((tmpCV).vValue).iIntConst);
			tmpCV.clear();
			//Added By Anil June 29 2005 --Ends here
			// stevev 15aug05 - start
			if ( m_nCycleTime == 0 ) m_nCycleTime = 1000;
			// stevev 15aug05 - end


			/* Arun 120505 Start of code */

			m_piPlotX1->XAxis[0]->PutCursorUseDefaultFormat(FALSE);
			m_piPlotX1->XAxis[0]->PutLabelsFormatStyle(iptfValue);

			/* End of code */
			
		}//endif SUCCESS == rc


	}//endif pChart
#endif
	return SUCCESS;

}/*End InitStripChart()*/

int CScopeCtrl::InitScopeChart(hCchart *pChart,CString Label,ddbItemList_t *pSourceList)
{
#ifdef _USE_PLOT_LIB
	RETURNCODE rc = SUCCESS;

	CString tempName;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	BSTR bstrText;
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;
	
	// Using the chart's label in the static window for consistency with the meter charts
	// NO need to use it again here. - Modified by POB, 8 Aug 2005
	// TO DO:: May want to source's label here.  Currently, only the source's member descriptions are shown as legends
	Label = " ";

	// Set the title with Chart label

	bstrText = Label.AllocSysString();

	m_piPlotX1->PutTitleText(bstrText);

	m_piPlotX1->PutTitleFontColor(0x00333333);//80% grey

	// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
	//{
		ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
	//}

//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	if(pChart)
	{
		// Initialize the member source list
		// POB, 12 Aug 2005 - replaced duplicate code 
		if(InitSources(pChart))
		{
			return FAILURE;
		}

		if(SUCCESS == rc)
		{
			rc = pChart->getLength(tmpCV);
			m_nLENGTH = (((tmpCV).vValue).iIntConst)/1000;
			DEBUGLOG(CLOG_LOG,"Scope Chart set length to %d\n",m_nLENGTH);
			if(SUCCESS == rc && tmpCV.vIsValid)
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)tmpCV/1000);
			}
			else
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)600); //default
			}
			tmpCV.clear();

			/* Arun 120505 Start of code */
			m_piPlotX1->XAxis[0]->PutCursorUseDefaultFormat(TRUE);
			m_piPlotX1->XAxis[0]->PutLabelsFormatStyle(iptfValue);
			m_piPlotX1->XAxis[0]->PutTrackingEnabled(FALSE);
			m_piPlotX1->XAxis[0]->PutEnabled(FALSE);

			//The axis does not grow and it starts again from 0
			m_piPlotX1->XAxis[0]->PutDesiredIncrement((double)1);

			m_piPlotX1->XAxis[0]->PutTickListCustom(FALSE);

			m_piPlotX1->XAxis[0]->PutMinorCount(0);

			
			//Added By Anil June 29 2005 --starts here
			rc = pChart->getCycleTime(tmpCV);			
			m_nCycleTime = (((tmpCV).vValue).iIntConst);			
			// stevev 01dec06 - start
			if ( m_nCycleTime == 0 ) m_nCycleTime = 1000;
			// stevev 01dec06 - end

			tmpCV.clear();
			//Added By Anil June 29 2005 --Ends here


			/* End of code */

		}//endif SUCCESS == rc


	}//endif pChart
#endif
	return SUCCESS;

}/*End InitScopeChart()*/

int CScopeCtrl::InitSweepChart(hCchart * pChart, CString Label,ddbItemList_t *pSourceList)
{
#ifdef _USE_PLOT_LIB
	RETURNCODE rc = SUCCESS;

	CString tempName;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	BSTR bstrText;
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;
	
	// Using the chart's label in the static window for consistency with the meter charts
	// NO need to use it again here. - Modified by POB, 8 Aug 2005
	// TO DO:: May want to source's label here.  Currently, only the source's member descriptions are shown as legends
	Label = " ";

	// Set the title with Chart label

	bstrText = Label.AllocSysString();

	m_piPlotX1->PutTitleText(bstrText);

	m_piPlotX1->PutTitleFontColor(0x00333333);//80% grey

	// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
	//{
		ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
	//}

//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	if(pChart)
	{
		// Initialize the member source list
		// POB, 12 Aug 2005 - replaced duplicate code 
		if(InitSources(pChart))
		{
			return FAILURE;
		}

		if(SUCCESS == rc)
		{
			rc = pChart->getLength(tmpCV);
			m_nLENGTH = (((tmpCV).vValue).iIntConst)/1000;
			DEBUGLOG(CLOG_LOG,"Sweep Chart set length to %d\n",m_nLENGTH);
			if(SUCCESS == rc && tmpCV.vIsValid)
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)tmpCV/1000);
			}
			else
			{
				m_piPlotX1->XAxis[0]->PutSpan((double)600); //default
			}
			tmpCV.clear();

			/* Arun 120505 Start of code */

			m_piPlotX1->XAxis[0]->PutCursorUseDefaultFormat(TRUE);
			m_piPlotX1->XAxis[0]->PutLabelsFormatStyle(iptfValue);
			m_piPlotX1->XAxis[0]->PutTrackingEnabled(FALSE);
			m_piPlotX1->XAxis[0]->PutEnabled(FALSE);

			//The axis does not grow and it starts again from 0
			m_piPlotX1->XAxis[0]->PutDesiredIncrement((double)1);

			m_piPlotX1->XAxis[0]->PutTickListCustom(FALSE);

			m_piPlotX1->XAxis[0]->PutMinorCount(0);

			uChartLimitIndx = m_piPlotX1->AddLimit();

			m_piPlotX1->Limit[uChartLimitIndx]->Style = iplsLineX;  
			m_piPlotX1->Limit[uChartLimitIndx]->Color = 0x00000000;

			//Added By Anil June 29 2005 --starts here			
			rc = pChart->getCycleTime(tmpCV);
			m_nCycleTime = (((tmpCV).vValue).iIntConst);			
			// stevev 01dec06 - start
			if ( m_nCycleTime == 0 ) m_nCycleTime = 1000;
			// stevev 01dec06 - end
			tmpCV.clear();
			//Added By Anil June 29 2005 --Ends here


			/* End of code */

		}//endif SUCCESS == rc


	}//endif pChart
#endif
	return SUCCESS;

}/*End InitSweepChart()*/

int CScopeCtrl::InitAngularGauge(hCchart * pChart, CString Label,ddbItemList_t *pSourceList)
{
#ifdef _USE_PLOT_LIB
	RETURNCODE rc = SUCCESS;

	CString tempName;
	wstring tmpStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	BSTR bstrText;

	long lRadius,lVertSpan,lHorizSpan; //Vibhor 060405: Added
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;

	// Set the title with graph label
	// Using the graph's label in the static window
	// NO need to use it again here. - Modified by POB, 8 Aug 2005
	// TO DO:: May want to variable's label or waveforms's member descriptions here
	Label = " ";

	bstrText = Label.AllocSysString();

//	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl()); // Deleted by POB - 9 Aug 2005

/*Since disposition on multi-source angular gauges is pending, we will just consider the 
  first source only for display for now. But we'll keep the code to fetch the rest of the sources and
  their item ptrs*/

/*	if(pChart)  // Deleted by POB - 9 Aug 2005
	{
		
		if(NULL == pSourceList)
		{
			rc  = pChart->getSourceList(srcList);
			ddbItemList_t srcPtrList;
			rc = srcList.getItemPtrs(srcPtrList);
			if(SUCCESS == rc && srcPtrList.size() > 0)
			{
				m_currentSourceList = srcPtrList;
			}
			else
				return FAILURE;
		}
		else
		{
			for(ddbItemList_t:: iterator iT = pSourceList->begin();iT != pSourceList->end();iT++)
			{
				m_currentSourceList.push_back((hCitemBase*)(*iT));
			}

			rc = SUCCESS;

		}

	}//endif pChart*/

	/*Vibhor 060405: Start of the code ; BugFix: gauge touching horizontal boundaries*/
//	lVertSpan = m_rect.bottom - m_rect.top - 100;		// moved down below by POB, 9 Aug 2005
//	lHorizSpan = m_rect.right - m_rect.left - 100;	
//	lRadius = (lVertSpan < lHorizSpan) ? (lVertSpan /2):(lHorizSpan / 2);
	/*Vibhor 060405: End of the code*/

	for(AngGaugeList::iterator iTa = m_AngGaugeList.begin(); iTa != m_AngGaugeList.end(); iTa++)
	{
		AngGauge angGauge = (*iTa);

		lVertSpan  = angGauge.srcRect.bottom - angGauge.srcRect.top  - 100;
		lHorizSpan = angGauge.srcRect.right  - angGauge.srcRect.left - 100;
		lRadius = (lVertSpan < lHorizSpan) ? (lVertSpan /2):(lHorizSpan / 2);

		if(angGauge.bLogGauge == true)
		{
			angGauge.piAngLogGauge->put_ArcRadius(lRadius);
			angGauge.piAngLogGauge->put_Label1Text(bstrText);
			angGauge.piAngLogGauge->put_Label1OffsetX(0);
			angGauge.piAngLogGauge->put_Label1OffsetY(-(lRadius/2));
			angGauge.piAngLogGauge->put_ShowLabel1(TRUE);
			angGauge.piAngLogGauge->put_ArcRangeDegrees(300);
			angGauge.piAngLogGauge->put_ArcStartDegrees(240);
			angGauge.piAngLogGauge->put_ShowHub(TRUE);
			angGauge.piAngLogGauge->put_ShowOuterArcRadius(TRUE);
			angGauge.piAngLogGauge->put_Label2OffsetY((lRadius/2));
			angGauge.piAngLogGauge->put_Transparent(TRUE);
			angGauge.piAngLogGauge->put_TickLabelPrecision(0);
			angGauge.piAngLogGauge->put_TickLabelMargin(7);

		}
		else
		{
		angGauge.piAngGauge->PutArcRadius(lRadius);
		angGauge.piAngGauge->PutLabel1Text(bstrText);
		angGauge.piAngGauge->PutLabel1OffsetX(0);
		angGauge.piAngGauge->PutLabel1OffsetY(-(lRadius/2));
		angGauge.piAngGauge->PutShowLabel1(TRUE);
		angGauge.piAngGauge->PutArcRangeDegrees(300);
		angGauge.piAngGauge->PutArcStartDegrees(240);
		angGauge.piAngGauge->PutShowHub(FALSE);
		angGauge.piAngGauge->PutShowOuterArcRadius(TRUE);
		angGauge.piAngGauge->PutPointerStyle(iagpsTriangle);
		angGauge.piAngGauge->PutLabel2OffsetY((lRadius/2));
		angGauge.piAngGauge->PutTransparent(TRUE);
		angGauge.piAngGauge->PutTickMajorCount(11);
		angGauge.piAngGauge->PutTickLabelPrecision(0);
		angGauge.piAngGauge->PutTickMinorCount(9);
		angGauge.piAngGauge->PutTickLabelMargin(7);
	}
	}
	
	if (pChart)
	{
		CValueVarient tmpCV;
		//Added By stevev01dec06--starts here
		rc = pChart->getCycleTime(tmpCV);			
		m_nCycleTime = (((tmpCV).vValue).iIntConst);
		if ( m_nCycleTime == 0 ) m_nCycleTime = 1000;
		// stevev 01dec06 - end

		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
		//}
		rc =SUCCESS;
	}
	else
	{
		rc = FAILURE;
	}
#endif	
	return SUCCESS;
}/*End InitAngularGauge()*/

int CScopeCtrl::InitLinearGauge(hCchart *pChart,CString Label,ddbItemList_t *pSourceList)
{
	RETURNCODE rc = SUCCESS;
#ifdef _USE_PLOT_LIB
/*#if 0
	CString tempName;
	string tmpStr;
	BSTR bstrText;
	
	CValueVarient tmpCV;

	if(NULL == m_pDoc)
		return FAILURE;

	bstrText = Label.AllocSysString();

	
	m_piLinearGauge->PutAutoCenter(TRUE);
	m_piLinearGauge->PutPointerStyle(ilgpsColorBar); //3 
	m_piLinearGauge->PutPointerColor(0x000080FF);
	m_piLinearGauge->PutTickMajorLength(40);
	m_piLinearGauge->PutTickMajorStyle(isAnalogLibrary::ibsLowered);
	m_piLinearGauge->PutTickMinorAlignment(itmnaCenter);
	m_piLinearGauge->PutTickMinorStyle(isAnalogLibrary::ibsRaised);
	//m_piLinearGauge->PutOrientation(ioHorizontal);
	m_piLinearGauge->PutShowTicksAxis(FALSE);
	m_piLinearGauge->PutPosition((double)25);

#endif*/

	if (pChart)
	{
		CValueVarient tmpCV;
		//Added By stevev01dec06--starts here
		rc = pChart->getCycleTime(tmpCV);			
		m_nCycleTime = (((tmpCV).vValue).iIntConst);
		if ( m_nCycleTime == 0 ) m_nCycleTime = 1000;
		// stevev 01dec06 - end

		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			ShowControl();//m_pWnd->ShowWindow (SW_SHOW);
		//}
		rc =SUCCESS;
	}
	else
	{
		rc = FAILURE;
	}

#endif
	return rc;

}/*End InitLinearGauge()*/


void CScopeCtrl::DisplayStripChart()
{
	int rc = SUCCESS;
#ifdef _USE_PLOT_LIB
	hCsource *pSource = NULL;
	hCaxis *pYaxis = NULL;
	unsigned uSourceColor = -1;
	BSTR bstrText;
	wstring tmpStr,hepStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	bool    stringFull = false;
	CString tempName;
	int iChannelNo = 0;
	CValueVarient tmpCV;
	ddbItemList_t varPtrList;
	ddbItemList_t subsList;
	hCgroupItemList srcMemberList(m_pDoc->pCurDev->devHndl());
	m_piPlotX1->RemoveAllChannels();

	int iSourceIndex = 0;
	int iSrcMemberIndex = 0;
	long lIndex = -1;

	bool bFirstSource = true; // set to true if the first source has an axis defined

	bool bParentSameSource = false;

	
	for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
			iSourceIndex++;
			continue;
		}
	
		/* added to deal with actions - from waveform handling, stevev 30apr07*/
		// stevev 16may07 - refresh always done, initActions done in InitSources()
		//if(false == m_bRefreshFlag)
		//{
		//	rc = pSource->doInitActions();
		//}
		//else //refresh Source Data
		//{
			rc = pSource->doRefreshActions();  
		//}

		if(SUCCESS != rc)
		{
			LOGIT(CLOG_LOG|UI_LOG,"Source (0x%04x) %s 's Actions Failed or were Aborted",pSource->getID(),tmpStr.c_str());
	
			iSourceIndex++; //increment the source index
			
			rc = SUCCESS; // clear any error so that we can graph out the next source
			continue;//Don't show this source
		}
		/*end added from waveforms, stevev 30apr07*/


		bParentSameSource = false;

		pYaxis = NULL;
					
		rc = pSource->getMemberList(srcMemberList);

		iSrcMemberIndex = 0;

		if(rc == SUCCESS)
		{
			rc = srcMemberList.getItemPtrs(varPtrList);
			if(SUCCESS == rc && varPtrList.size() > 0)
			{
				pYaxis = pSource->getYAxis();
				if(pYaxis /*&& pYaxis->IsValid()*/)
				{
					if(true == bFirstSource )
					{
						m_piPlotX1->RemoveAllYAxes();
					}
					lIndex = SetYaxis(pYaxis);
								
				}//endif pYAxis

				uSourceColor = -1; //Initialize the source color

				pSource->getLineColor(uSourceColor);// if none, leaves it -1
				
				for(ddbItemList_t::iterator iTv = varPtrList.begin();iTv != varPtrList.end(); iTv++,iSrcMemberIndex++)
				{
					if((*iTv)->IsValid())
					{
						m_piPlotX1->AddChannel();

						lineType_t lineType = pSource->getLineType();
						switch(lineType)
						{
							case lT_Undefined:		// 0	
							case lT_Data:			// 1	DATA_LINETYPE		
							case lT_LowLow:			// 2	LOWLOW_LINETYPE		
							case lT_Low:			// 3	LOW_LINETYPE		
							case lT_High:			// 4	HIGH_LINETYPE		
							case lT_HighHigh:		// 5	HIGHHIGH_LINETYPE	
							case lT_Unknown:		// 7
								break;				
							case lT_Trans:			// 6	TRANSPARENT_LINETYPE
								m_piPlotX1->Channel[iChannelNo]->PutTraceVisible(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersStyle(ipmsCircle);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersVisible(TRUE);
								break;
							case lT_Data1:			//= ( lineTypeOffset + 1):
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 1);
								break;
							case lT_Data2:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 2);
								break;
							case lT_Data3: //Everything after this is same
							case lT_Data4:
							case lT_Data5:
							case lT_Data6:
							case lT_Data7:
							case lT_Data8:
							case lT_Data9:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 3);
								break;
						}
/*Every member in the same source gets the same color, either from the DD or by the component*/
					/*	if(SUCCESS == pSource->getLineColor(uSourceColor))
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(uSourceColor);
						}
						else if(bParentSameSource == true)
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(m_piPlotX1->Channel[iChannelNo -1]->GetColor());
						}

						else
						{//Coming here for the first time for this source so get a new color
							bParentSameSource = true;
						}*/

						// Get the color for this line
						m_piPlotX1->Channel[iChannelNo]->PutColor(m_colorSrc.GetOLEColor(uSourceColor));

						if(pSource->needsEmphasis())
						{
							m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(EMPHASIS_WIDTH);
						}

						//Associate the channel with proper Y Axis

						if(pYaxis) //The parent source has its own Y axis
						{
							m_piPlotX1->Channel[iChannelNo]->PutYAxisName(m_piPlotX1->YAxis[lIndex]->GetName());
						}

						//tmpStr = srcMemberList.at(iSrcMemberIndex).getDesc().procureVal();

						//  Get the variable label for the source member
						hCreference & itemRef = srcMemberList.at(iSrcMemberIndex).getRef(); // Added by POB, 27 Oct 2005

						itemRef.getItemLabel(tmpStr,stringFull,hepStr); // Added by POB, 27 Oct 2005

						tempName = tmpStr.c_str();
						
						bstrText = tempName.AllocSysString();
						
						m_piPlotX1->Channel[iChannelNo++]->put_TitleText(bstrText);

						//Push this var on subscription list

						subsList.push_back((*iTv));
						
					}//endif isValid()
					// else skip it
				
				}//endfor - next VarPtr from list
			
			}//endif varPtrList.size() > 0
			else
			{
				LOGIT(CLOG_LOG|UI_LOG,"Source (0x%04x) %s has no valid points\n",pSource->getID(),tmpStr.c_str());
				iSourceIndex++; //increment the Source Index
				continue;
			}

			iSourceIndex++;

			bFirstSource = false;
		}//endif have members

	}//next Source

	if(	subsList.size() > 0)
	{
		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
	}
	else
	{
		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	}
#endif	
	
	return;
}/*End DisplayStripChart()*/

void CScopeCtrl::DisplayScopeChart()
{
	int rc = SUCCESS;

#ifdef _USE_PLOT_LIB
	hCsource *pSource = NULL;
	hCaxis *pYaxis = NULL;
	unsigned uSourceColor = -1;
	BSTR bstrText;
	wstring tmpStr,hepStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	bool    stringFull = false;
	CString tempName;
	int iChannelNo = 0;
	CValueVarient tmpCV;
	ddbItemList_t varPtrList;
	ddbItemList_t subsList;
	hCgroupItemList srcMemberList(m_pDoc->pCurDev->devHndl());
	m_piPlotX1->RemoveAllChannels();

	int iSourceIndex = 0;
	int iSrcMemberIndex = 0;
	long lIndex = -1;

	bool bFirstSource = true; // set to true if the first source has an axis defined

	bool bParentSameSource = false;
	
	for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
			iSourceIndex++;
			continue;
		}

		bParentSameSource = false;

		pYaxis = NULL;
				
		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			if(false == m_bRefreshFlag)
			{
				rc = SUCCESS;//	Handle later rc = pWave->doInitActions();
			}
			else //refresh Waveforms
			{
				rc = SUCCESS;// Handle later rc = pWave->doRefreshActions();
			}
		//}
		// stevev 8may07-notused:else
		//{
		// stevev 8may07-notused:	rc = SUCCESS;
		//}
		
		if(SUCCESS != rc)
		{ // Just see this may not be required here
			iSourceIndex++; //increment the source index
			continue;//Don't show this waveform
		}
			
		rc = pSource->getMemberList(srcMemberList);

		iSrcMemberIndex = 0;

		if(rc == SUCCESS)
		{
			rc = srcMemberList.getItemPtrs(varPtrList);
			if(SUCCESS == rc && varPtrList.size() > 0)
			{
				pYaxis = pSource->getYAxis();
				if(pYaxis /*&& pYaxis->IsValid()*/)
				{
					if(true == bFirstSource )
					{
						m_piPlotX1->RemoveAllYAxes();
					}
						
					lIndex = SetYaxis(pYaxis);
			
				}//endif pYAxis

				uSourceColor = -1; //Initialize the source color

				pSource->getLineColor(uSourceColor);// if none, leaves it -1
				
				for(ddbItemList_t::iterator iTv = varPtrList.begin();iTv != varPtrList.end(); iTv++,iSrcMemberIndex++)
				{
					if((*iTv)->IsValid())
					{
						m_piPlotX1->AddChannel();
#ifndef _DEBUG // stevev 25apr2017 - this generates a popup error box for each iteration with iocomp version 5 in debug
						//Add empty points based on the span of the X axis
						for(int i =0; i< m_piPlotX1->XAxis[0]->Span; i++)
							m_piPlotX1->Channel[iChannelNo]->AddXEmpty((double)i);
#endif
						lineType_t lineType = pSource->getLineType();
						switch(lineType)
						{
							case lT_Undefined:		// 0	
							case lT_Data:			// 1	DATA_LINETYPE		
							case lT_LowLow:			// 2	LOWLOW_LINETYPE		
							case lT_Low:			// 3	LOW_LINETYPE		
							case lT_High:			// 4	HIGH_LINETYPE		
							case lT_HighHigh:		// 5	HIGHHIGH_LINETYPE	
							case lT_Unknown:		// 7
								break;				
							case lT_Trans:			// 6	TRANSPARENT_LINETYPE
								m_piPlotX1->Channel[iChannelNo]->PutTraceVisible(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersStyle(ipmsCircle);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersVisible(TRUE);
								break;
							case lT_Data1:			//= ( lineTypeOffset + 1):
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 1);
								break;
							case lT_Data2:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 2);
								break;
							case lT_Data3: //Everything after this is same
							case lT_Data4:
							case lT_Data5:
							case lT_Data6:
							case lT_Data7:
							case lT_Data8:
							case lT_Data9:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 3);
								break;
						}
/*Every member in the same source gets the same color, either from the DD or by the component*/
/*						if(SUCCESS == pSource->getLineColor(uSourceColor))
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(uSourceColor);
						}
						else if(bParentSameSource == true)
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(m_piPlotX1->Channel[iChannelNo -1]->GetColor());
						}
						else
						{//Coming here for the first time for this source so get a new color
							bParentSameSource = true;
						}*/

						// Get the color for this line
						m_piPlotX1->Channel[iChannelNo]->PutColor(m_colorSrc.GetOLEColor(uSourceColor));

						if(pSource->needsEmphasis())
						{
							m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(EMPHASIS_WIDTH);
						}

						//Associate the channel with proper Y Axis

						if(pYaxis) //The parent source has its own Y axis
						{
							m_piPlotX1->Channel[iChannelNo]->PutYAxisName(m_piPlotX1->YAxis[lIndex]->GetName());
						}

						//tmpStr = srcMemberList.at(iSrcMemberIndex).getDesc().procureVal();

						//  Get the variable label for the source member
						hCreference & itemRef = srcMemberList.at(iSrcMemberIndex).getRef(); // Added by POB, 27 Oct 2005

						itemRef.getItemLabel(tmpStr,stringFull, hepStr); // Added by POB, 27 Oct 2005

						tempName = tmpStr.c_str();
						
						bstrText = tempName.AllocSysString();
						
						m_piPlotX1->Channel[iChannelNo++]->put_TitleText(bstrText);

						//Push this var on subscription list

						subsList.push_back((*iTv));
							
						
					}//endif isValid()
				
				}//endfor
			
			}//endif varPtrList.size() > 0
			else
			{
				LOGIT(CLOG_LOG|UI_LOG,"Source (0x%04x) %s has no valid points\n",pSource->getID(),tmpStr.c_str());
				iSourceIndex++; //increment the Source Index
				continue;
			}

			iSourceIndex++;

			bFirstSource = false;
		}//endif rc == SUCCESS

	}//endfor SourceList

	if(	subsList.size() > 0)
	{
		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
	}
	else
	{
		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	}
	
#endif	
	return;

}/*End DisplayScopeChart()*/

void CScopeCtrl::DisplaySweepChart()
{
#ifdef _USE_PLOT_LIB
	int rc = SUCCESS;
	hCsource *pSource = NULL;
	hCaxis *pYaxis = NULL;
	unsigned uSourceColor = -1;
	BSTR bstrText;
	wstring tmpStr, hepStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	bool    stringFull;
	CString tempName;
	int iChannelNo = 0;
	CValueVarient tmpCV;
	ddbItemList_t varPtrList;
	ddbItemList_t subsList;
	hCgroupItemList srcMemberList(m_pDoc->pCurDev->devHndl());
	m_piPlotX1->RemoveAllChannels();

	int iSourceIndex = 0;
	int iSrcMemberIndex = 0;
	long lIndex = -1;

	bool bFirstSource = true; // set to true if the first source has an axis defined

	bool bParentSameSource = false;
	
	for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
			iSourceIndex++;
			continue;
		}

		bParentSameSource = false;

		pYaxis = NULL;
				
		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			if(false == m_bRefreshFlag)
			{
				rc = SUCCESS;//	Handle later rc = pWave->doInitActions();
			}
			else //refresh Waveforms
			{
				rc = SUCCESS;// Handle later rc = pWave->doRefreshActions();
			}
		//}
		// stevev 8may07-notused:else
		//{
		// stevev 8may07-notused:	rc = SUCCESS;
		//}
		
		if(SUCCESS != rc)
		{ // Just see this may not be required here
			iSourceIndex++; //increment the source index
			continue;//Don't show this waveform
		}
			
		rc = pSource->getMemberList(srcMemberList);

		iSrcMemberIndex = 0;

		if(rc == SUCCESS)
		{
			rc = srcMemberList.getItemPtrs(varPtrList);
			if(SUCCESS == rc && varPtrList.size() > 0)
			{
				pYaxis = pSource->getYAxis();
				if(pYaxis /*&& pYaxis->IsValid()*/)
				{
					if(true == bFirstSource )
					{
						m_piPlotX1->RemoveAllYAxes();
					}
						
					lIndex = SetYaxis(pYaxis);
			
				}//endif pYAxis

				uSourceColor = -1; //Initialize the source color

				pSource->getLineColor(uSourceColor);// if none, leaves it -1

				for(ddbItemList_t::iterator iTv = varPtrList.begin();iTv != varPtrList.end(); iTv++,iSrcMemberIndex++)
				{
					if((*iTv)->IsValid())
					{
						m_piPlotX1->AddChannel();

						//Add empty points based on the span of the X axis
						for(int i =0; i< m_piPlotX1->XAxis[0]->Span; i++)
							m_piPlotX1->Channel[iChannelNo]->AddXEmpty((double)i);

						lineType_t lineType = pSource->getLineType();
						switch(lineType)
						{
							case lT_Undefined:		// 0	
							case lT_Data:			// 1	DATA_LINETYPE		
							case lT_LowLow:			// 2	LOWLOW_LINETYPE		
							case lT_Low:			// 3	LOW_LINETYPE		
							case lT_High:			// 4	HIGH_LINETYPE		
							case lT_HighHigh:		// 5	HIGHHIGH_LINETYPE	
							case lT_Unknown:		// 7
								break;				
							case lT_Trans:			// 6	TRANSPARENT_LINETYPE
								m_piPlotX1->Channel[iChannelNo]->PutTraceVisible(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersAllowIndividual(FALSE);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersStyle(ipmsCircle);
								m_piPlotX1->Channel[iChannelNo]->PutMarkersVisible(TRUE);
								break;
							case lT_Data1:			//= ( lineTypeOffset + 1):
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 1);
								break;
							case lT_Data2:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 2);
								break;
							case lT_Data3: //Everything after this is same
							case lT_Data4:
							case lT_Data5:
							case lT_Data6:
							case lT_Data7:
							case lT_Data8:
							case lT_Data9:
								m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(DATA_LINE_WIDTH + 3);
								break;
						}
/*Every member in the same source gets the same color, either from the DD or by the component*/
/*						if(SUCCESS == pSource->getLineColor(uSourceColor))
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(uSourceColor);
						}
						else if(bParentSameSource == true)
						{
							m_piPlotX1->Channel[iChannelNo]->PutColor(m_piPlotX1->Channel[iChannelNo -1]->GetColor());
						}
						else
						{//Coming here for the first time for this source so get a new color
							bParentSameSource = true;
						}*/

						// Get the color for this line
						m_piPlotX1->Channel[iChannelNo]->PutColor(m_colorSrc.GetOLEColor(uSourceColor));

						if(pSource->needsEmphasis())
						{
							m_piPlotX1->Channel[iChannelNo]->PutTraceLineWidth(EMPHASIS_WIDTH);
						}

						//Associate the channel with proper Y Axis

						if(pYaxis) //The parent source has its own Y axis
						{
							m_piPlotX1->Channel[iChannelNo]->PutYAxisName(m_piPlotX1->YAxis[lIndex]->GetName());
						}

						//tmpStr = srcMemberList.at(iSrcMemberIndex).getDesc().procureVal();

						//  Get the variable label for the source member
						hCreference & itemRef = srcMemberList.at(iSrcMemberIndex).getRef(); // Added by POB, 27 Oct 2005

						itemRef.getItemLabel(tmpStr, stringFull,hepStr); // Added by POB, 27 Oct 2005

						tempName = tmpStr.c_str();
						
						bstrText = tempName.AllocSysString();
						
						m_piPlotX1->Channel[iChannelNo++]->put_TitleText(bstrText);

						//Push this var on subscription list

						subsList.push_back((*iTv));
							
						
					}//endif isValid()
				
				}//endfor
			
			}//endif varPtrList.size() > 0
			else
			{
				LOGIT(CLOG_LOG|UI_LOG,"Source (0x%04x) %s has no valid points\n",pSource->getID(),tmpStr.c_str());
				iSourceIndex++; //increment the Source Index
				continue;
			}

			iSourceIndex++;

			bFirstSource = false;
		}//endif rc == SUCCESS

	}//endfor SourceList

	if(	subsList.size() > 0)
	{
		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
	}
	else
	{
		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	}
	
#endif	
	return;

}/*End DisplaySweepChart()*/

void CScopeCtrl::DisplayAngularGauge()
{
#ifdef _USE_PLOT_LIB
	int rc = SUCCESS;
	hCsource *pSource = NULL;
	hCaxis *pYaxis = NULL;
	unsigned uSourceColor = -1;
	BSTR bstrText;
	wstring tmpStr, hepStr;     // Use wstring for UTF-8 support, POB - 8 Aug 2008
	bool    stringFull = false;
	tstring srcCaptionStr;
	CString tempName;
	int iPointerNo = 0;
	CValueVarient tmpCV;
	ddbItemList_t varPtrList;
	ddbItemList_t subsList;
	hCgroupItemList srcMemberList(m_pDoc->pCurDev->devHndl());

	// Modified by POB, 9 Aug 2005	- Linear Gauge does not use DisplayLinearGauge() so we are going to ignore DisplayAngularGauge() for now

	int iSourceIndex = 0;
	int iSrcMemberIndex = 0;
	long lIndex = 0;

	bool bFirstSource = true; // set to true if the first source has an axis defined

	bool bParentSameSource = false;
	
//	m_piAngGauge->RemoveAllPointers();    POB, 8 Aug 2005, Moved down below

	//for now we'll take the first valid source and skip everything else
	for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
	{
		pSource = (hCsource*)(*iT);

		if(!(pSource->IsValid()))
		{						//Skip the invalid sources
			iSourceIndex++;
			continue;
		}

		bParentSameSource = false;

        if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
		{
			m_AngGaugeList.at(iSourceIndex).piAngLogGauge->RemoveAllPointers();
		}
		else
		{
        m_AngGaugeList.at(iSourceIndex).piAngGauge->RemoveAllPointers();   // Added by POB, 8 Aug 2005
		}

		// Get the label for the source so it can be displayed on the window - Added by POB, 8 Aug 2005
		pSource->Label(srcCaptionStr);
		
		// Set the title for the Source' Window
		m_AngGaugeList.at(iSourceIndex).pSrcWnd->SetWindowText(srcCaptionStr.c_str());

		pYaxis = NULL;

		// stevev 8may07-notused:if(false == m_bIsParentMethodDlg)
		//{
			if(false == m_bRefreshFlag)
			{
				rc = SUCCESS;//	Handle later rc = pWave->doInitActions();
			}
			else //refresh Waveforms
			{
				rc = SUCCESS;// Handle later rc = pWave->doRefreshActions();
			}
		//}
		// stevev 8may07-notused:else
		//{
		// stevev 8may07-notused:	rc = SUCCESS;
		//}
		
		if(SUCCESS != rc)
		{ // Just see this may not be required here
			iSourceIndex++; //increment the source index
			continue;//Don't show this waveform
		}

		rc = pSource->getMemberList(srcMemberList);

		iSrcMemberIndex = 0;

		if(rc == SUCCESS)
		{
			rc = srcMemberList.getItemPtrs(varPtrList);
			if(SUCCESS == rc && varPtrList.size() > 0)
			{
				pYaxis = pSource->getYAxis();
				if(pYaxis /*&& pYaxis->IsValid()*/)
				{	
					rc = pYaxis->getMinValue(tmpCV);

					if(SUCCESS == rc && tmpCV.vIsValid)
					{
//						m_piAngGauge->PutPositionMin((double)tmpCV);
						if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
						{
							if((double)tmpCV <= 0)
							{
								m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMin(1.00);

							}
							else
							{
								m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMin((double)tmpCV);
							}
						}
						else
						{
						m_AngGaugeList.at(iSourceIndex).piAngGauge->PutPositionMin((double)tmpCV);   // Replaced by POB, 8 Aug 2005

						}
					}
					rc = pYaxis->getMaxValue(tmpCV);
					if(SUCCESS == rc && tmpCV.vIsValid)
					{
//						m_piAngGauge->PutPositionMax((double)tmpCV);
						if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
						{
							if((double)tmpCV <= 0)
							{
								m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMax(100);

							}
							else
							{
								m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMax((double)tmpCV);
							}
						}
						else
						{
						m_AngGaugeList.at(iSourceIndex).piAngGauge->PutPositionMax((double)tmpCV);   // Replaced by POB, 8 Aug 2005
					}
					}
					if(SUCCESS == pYaxis->getUnitString(tmpStr))
					{
						tempName = tmpStr.c_str();
					}
					else
					{
						tempName = " ";
					}
					bstrText = tempName.AllocSysString();
//					m_piAngGauge->PutLabel2Text(bstrText);
					if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
					{
						m_AngGaugeList.at(iSourceIndex).piAngLogGauge->put_Label2Text(bstrText);
						m_AngGaugeList.at(iSourceIndex).piAngLogGauge->put_ShowLabel2(TRUE);
						m_AngGaugeList.at(iSourceIndex).piAngLogGauge->put_Label2OffsetX(0);

					}
					else
					{

					m_AngGaugeList.at(iSourceIndex).piAngGauge->PutLabel2Text(bstrText);			// Replaced by POB, 8 Aug 2005
//					m_piAngGauge->PutShowLabel2(TRUE);
					m_AngGaugeList.at(iSourceIndex).piAngGauge->PutShowLabel2(TRUE);				// Replaced by POB, 8 Aug 2005
//					m_piAngGauge->PutLabel2OffsetX(0);
					m_AngGaugeList.at(iSourceIndex).piAngGauge->PutLabel2OffsetX(0);				// Replaced by POB, 8 Aug 2005
					}					
					
				
				}
				else
				{
					if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
					{
						m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMin((double)0);
						m_AngGaugeList.at(iSourceIndex).piAngLogGauge->PutPositionMax((double)100);

					}
					else
					{
					//hard code till you get reply from Wally
	//					m_piAngGauge->PutPositionMin((double)0);
					m_AngGaugeList.at(iSourceIndex).piAngGauge->PutPositionMin((double)0);			// Replaced by POB, 8 Aug 2005
	//					m_piAngGauge->PutPositionMax((double)100);
					m_AngGaugeList.at(iSourceIndex).piAngGauge->PutPositionMax((double)100);		// Replaced by POB, 8 Aug 2005
					}

				}
				
				uSourceColor = -1; //Initialize the source color

				pSource->getLineColor(uSourceColor);

				for(ddbItemList_t::iterator iTv = varPtrList.begin();iTv != varPtrList.end(); iTv++,iSrcMemberIndex++)
				{
					if((*iTv)->IsValid()) //we show only valid members
					{
						if(iSrcMemberIndex > 0) //One pointer is there by default
						{
//							m_piAngGauge->AddPointer();
							if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
							{
								m_AngGaugeList.at(iSourceIndex).piAngLogGauge->AddPointer();
							}
							else
							{
							m_AngGaugeList.at(iSourceIndex).piAngGauge->AddPointer();

							}
						}
						subsList.push_back((*iTv));
/*						if(-1 != uSourceColor) //if Color coming from DD
						{
//							m_piAngGauge->SetPointersColor(iSrcMemberIndex,uSourceColor);
							m_AngGaugeList.at(iSourceIndex).piAngGauge->SetPointersColor(iSrcMemberIndex,uSourceColor);				// Replaced by POB, 8 Aug 2005
							
						}
						else
						{
//							m_piAngGauge->SetPointersColor(iSrcMemberIndex,colors[iSrcMemberIndex]);
							m_AngGaugeList.at(iSourceIndex).piAngGauge->SetPointersColor(iSrcMemberIndex,colors[iSrcMemberIndex]); // Replaced by POB, 8 Aug 2005
						}*/

						// Get the color for this pointer
						if(m_AngGaugeList.at(iSourceIndex).bLogGauge == true)
						{
							m_AngGaugeList.at(iSourceIndex).piAngLogGauge->SetPointersColor(iSrcMemberIndex, m_colorSrc.GetOLEColor(uSourceColor));
						}
						else
						{
						m_AngGaugeList.at(iSourceIndex).piAngGauge->SetPointersColor(iSrcMemberIndex, m_colorSrc.GetOLEColor(uSourceColor)); // Replaced by POB, 12 Aug 2005
						}
						
						//  Get the member description for each source member
						//  tmpStr = srcMemberList.at(iSrcMemberIndex).getDesc().procureVal(); // Added back by POB, 19 Oct 2005

						//  Get the variable label for the source member
						hCreference & itemRef = srcMemberList.at(iSrcMemberIndex).getRef(); // Added by POB, 27 Oct 2005

						itemRef.getItemLabel(tmpStr, stringFull, hepStr); // Added by POB, 27 Oct 2005

						m_Legend[iSourceIndex].srcMember[iSrcMemberIndex].strName = tmpStr.c_str();
						
						// Now save the color that this source member is using
						m_Legend[iSourceIndex].srcMember[iSrcMemberIndex].uColor = m_colorSrc.GetCurrentOLEColor();

/*						
						tmpStr = srcMemberList.at(iSrcMemberIndex).getDesc().procureVal(); 

						if(0 == iSrcMemberIndex)	//Deleted by POB, 8 Aug 2005
						{
							srcCaptionStr = tmpStr;		
						}
						else
						{
							srcCaptionStr = srcCaptionStr + " && " + tmpStr;
						}*/


					}//endif

				}//endfor
//				m_pWnd->SetWindowText(srcCaptionStr.c_str());					// Moved to the top of function by POB, 10 Aug 2005
//				break;															// Deleted by POB, 9 Aug 2005
				
				// Place the appropriate number of source members on the list	- POB, 10 Aug 2005
				m_AngGaugeList.at(iSourceIndex).nSrcMembers = iSrcMemberIndex;

				iSourceIndex++; //increment the source index					// Added by POB, 10 Aug 2005
						
			}//endif
			else
			{
				LOGIT(CLOG_LOG|UI_LOG,"Source (0x%04x) %s has no valid points\n",pSource->getID(),tmpStr.c_str());
				iSourceIndex++; //increment the Source Index
				break; //break for now ; continue;

			}
		}//endif rc == SUCCESS


	}//endfor

	if(	subsList.size() > 0)
	{
		rc = m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
	}
	else
	{
		
		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	
	}
#endif
	return;

}/*End DisplayAngularGauge()*/

void CScopeCtrl::DisplayLinearGauge()
{
#ifdef _USE_PLOT_LIB
	if(	subsList.size() > 0)
	{
		m_pDoc->pCurDev->subscribe(m_iSubHandle,subsList,(hCpubsub*)this,m_nCycleTime);//Replaced Cycle time from 1000 to  m_nCycleTime by Anil
	}
	else
	{
		
		LOGIT(CLOG_LOG|UI_LOG,"Nothing to subscribe !!\n");	
	
	}
#endif
	return;
}

/*End DisplayLinearGauge()*/

// The code in InitSources is used many times to for mulitple types of CHARTs
// Its purpose is to reduce duplication - POB, 12 Aug 2005 
int CScopeCtrl::InitSources(hCchart * pChart)
{
	ddbItemList_t srcPtrList;
	int rc = SUCCESS;
	unsigned uSourceColor = -1;

	hCgroupItemList srcList(m_pDoc->pCurDev->devHndl());

	if(pChart)
	{
		rc = pChart->getSourceList(srcList);
		rc = srcList.getItemPtrs(srcPtrList);
		
		if(SUCCESS == rc && srcPtrList.size() > 0)
		{
			m_currentSourceList = srcPtrList;
			rc = SUCCESS;
		}
		else
		{
			rc = FAILURE;
		}
	}

	if (rc == SUCCESS)
	{
		hCsource* pSrc;
		// Reserve the colors used by this Source
		for(ddbItemList_t::iterator iT = m_currentSourceList.begin(); iT != m_currentSourceList.end(); iT++)
		{
			pSrc = (hCsource*)(*iT);
			if(pSrc->IsValid())
			{	
				m_iSourcecount++;
				pSrc->doInitActions();
			}
			else
			{
				continue;
			}

			// register the colors whether they are valid or not
			if ( ((hCsource*)(*iT))->getLineColor(uSourceColor) == SUCCESS && uSourceColor >= 0 )
			{
				m_colorSrc.UseColor(uSourceColor);
			}
		}
	}

	if (!m_iSourcecount)
	{
		rc =  FAILURE;
	}

	return rc;
}

// This function will use the rectangle from the previous source
// in order to calculate the next source's placement
// If it is the 1st source, rect will be the Chart's rectangle - POB, 12 Aug 2005
void CScopeCtrl::CreateSourceRect(RECT & rect)
{
	// Use srcRect as a scratch pad
	RECT srcRect = rect;

	if(m_iSourcecount)
	{
		// This needs to be called only once
		if (m_iSrcRectSpan == 0)
		{
			// 1st time through - create span
			// rect should be the rectangle of the entire chart
			m_iSrcRectSpan = (rect.right - rect.left)/m_iSourcecount;
				
			srcRect.right = srcRect.left + m_iSrcRectSpan - 1;
		}
		else
		{
			srcRect.left = srcRect.right;
			srcRect.right = srcRect.right + m_iSrcRectSpan;
		}
	}

	rect = srcRect;
}

// The code in InitWaveform is used similiarly to InitSource
// to handle the line color attribute in the DD - POB, 12 Aug 2005 
int CScopeCtrl::InitWaveforms(hCgraph * pGraph)
{
	ddbItemList_t wvPtrList;
	int rc = SUCCESS;
	unsigned uWaveformColor = -1;
	int iWaveformcount = 0;

	hCgroupItemList wvList(m_pDoc->pCurDev->devHndl());

	if(pGraph)
	{
		rc = pGraph->getWaveformList(wvList);//a hCgroupItemList
		rc = wvList.getItemPtrs(wvPtrList);
		
		if(SUCCESS == rc && wvPtrList.size() > 0)
		{
			m_currentWaveList = wvPtrList;
			rc = SUCCESS;
		}
		else
		{
			rc = FAILURE;
		}
		// now we need the junk to do the new naming requirements
		vector<hCgroupItemDescriptor>::iterator iTgip;
		hCgroupItemDescriptor* pTgip;
		EnumTriad_t eTri;
		for (iTgip = wvList.begin(); iTgip != wvList.end(); ++iTgip )
		{// isa ptr to a hCgroupItemDescriptor
			pTgip = &(*iTgip);
			eTri  =  (*pTgip);
			m_WaveExtraList.push_back(eTri);
			eTri.clear();
		}
		if (m_currentWaveList.size() != m_WaveExtraList.size())
		{
			LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"Error: %d valid waveforms but %d elements.\n",
				m_currentWaveList.size(), m_WaveExtraList.size());
		}
	}

	if (rc == SUCCESS)
	{
		vector<unsigned> mtlist;
		// Reserve the colors used by this Waveform
		// stevev 08may07 - init the index lists while we're here
		for(ddbItemList_t::iterator iT = m_currentWaveList.begin(); iT != m_currentWaveList.end(); iT++)
		{
			m_channelIdxsList.push_back((unsigned)-1);
			m_yAxisList.push_back((unsigned)-1);
			m_ListOfLimitIndexLists.push_back(mtlist);
			m_EMPchannelIdxsList.push_back((unsigned)-1);

			if(((hCwaveform*)(*iT))->IsValid())
			{	
				iWaveformcount++;
			}
			else
			{
				continue;
			}

			// register the colors whether they are valid or not
			if ( ((hCwaveform*)(*iT))->getLineColor(uWaveformColor) == SUCCESS && uWaveformColor >= 0 )
			{
				m_colorSrc.UseColor(uWaveformColor);
			}
		}
	}
#ifdef _DEBUG
	if (	iWaveformcount != m_currentWaveList.size() || 
			m_currentWaveList.size() != m_channelIdxsList.size())
	{//m_currentWaveList.size(), m_channelIdxsList.size());
		rc = SUCCESS;// it really doesn't
	}
	else
	{
	//	LOGIT(CERR_LOG,"CORRECT: %s Waveform internal channel list is IN sync.(%d vs %d)\n",
	//							(m_pItemBase)?m_pItemBase->getName().c_str():"Unknown",	
	//							m_currentWaveList.size(), m_channelIdxsList.size());
	}
#endif

	if (!iWaveformcount)
	{
		rc =  FAILURE;
	}

	return rc;
}


void CScopeCtrl::OnLegendButton() 
{
	// TODO: Add your control notification handler code here
	CRect rect(0,0,0,0);
	
	DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));

//	m_Legend.legendButton[0]->GetWindowRect(&rect);

	for (int i = 0; i < 10; i++)
	{

		if (m_Legend[i].legendButton)
		{
			m_Legend[i].legendButton->GetWindowRect(&rect);


			if (rect.PtInRect(point))
			{
//				MessageBox(m_Legend[i].srcMember[0].strName);

				// Give the correct source to the dialog
				m_LegendDlg.m_pSource = &m_Legend[i];
				
				// run the dialog
				m_LegendDlg.DoModal();
				break;
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLegendDlg dialog


CLegendDlg::CLegendDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLegendDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLegendDlg)
	m_strLegend0 = _T("");
	m_strLegend1 = _T("");
	m_strLegend2 = _T("");
	m_strLegend3 = _T("");
	m_strLegend4 = _T("");
	m_strLegend5 = _T("");
	m_strLegend6 = _T("");
	m_strLegend7 = _T("");
	//}}AFX_DATA_INIT
}


void CLegendDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLegendDlg)
	DDX_Control(pDX, IDC_LEGEND7_BUTTON, m_legendButton7);
	DDX_Control(pDX, IDC_LEGEND6_BUTTON, m_legendButton6);
	DDX_Control(pDX, IDC_LEGEND5_BUTTON, m_legendButton5);
	DDX_Control(pDX, IDC_LEGEND4_BUTTON, m_legendButton4);
	DDX_Control(pDX, IDC_LEGEND3_BUTTON, m_legendButton3);
	DDX_Control(pDX, IDC_LEGEND2_BUTTON, m_legendButton2);
	DDX_Control(pDX, IDC_LEGEND1_BUTTON, m_legendButton1);
	DDX_Control(pDX, IDC_LEGEND0_BUTTON, m_legendButton0);
	DDX_Text(pDX, IDC_LEGEND0_STATIC, m_strLegend0);
	DDX_Text(pDX, IDC_LEGEND1_STATIC, m_strLegend1);
	DDX_Text(pDX, IDC_LEGEND2_STATIC, m_strLegend2);
	DDX_Text(pDX, IDC_LEGEND3_STATIC, m_strLegend3);
	DDX_Text(pDX, IDC_LEGEND4_STATIC, m_strLegend4);
	DDX_Text(pDX, IDC_LEGEND5_STATIC, m_strLegend5);
	DDX_Text(pDX, IDC_LEGEND6_STATIC, m_strLegend6);
	DDX_Text(pDX, IDC_LEGEND7_STATIC, m_strLegend7);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLegendDlg, CDialog)
	//{{AFX_MSG_MAP(CLegendDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLegendDlg message handlers

BOOL CLegendDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CScopeCtrl::Source * pSource = (CScopeCtrl::Source *) m_pSource;

	if (pSource)
	{
		m_strLegend0 = pSource->srcMember[0].strName;
		m_strLegend1 = pSource->srcMember[1].strName;
		m_strLegend2 = pSource->srcMember[2].strName;
		m_strLegend3 = pSource->srcMember[3].strName;
		m_strLegend4 = pSource->srcMember[4].strName;
		m_strLegend5 = pSource->srcMember[5].strName;
		m_strLegend6 = pSource->srcMember[6].strName;
		m_strLegend7 = pSource->srcMember[7].strName;

		m_legendButton0.SetColorOn(pSource->srcMember[0].uColor);
		m_legendButton1.SetColorOn(pSource->srcMember[1].uColor);
		m_legendButton2.SetColorOn(pSource->srcMember[2].uColor);
		m_legendButton3.SetColorOn(pSource->srcMember[3].uColor);
		m_legendButton4.SetColorOn(pSource->srcMember[4].uColor);
		m_legendButton5.SetColorOn(pSource->srcMember[5].uColor);
		m_legendButton6.SetColorOn(pSource->srcMember[6].uColor);
		m_legendButton7.SetColorOn(pSource->srcMember[7].uColor);
	}

	// Remove any legends if they do not exist
//	BOOL state = m_strLegend0.IsEmpty();
	m_legendButton0.ShowWindow(!m_strLegend0.IsEmpty());
	m_legendButton1.ShowWindow(!m_strLegend1.IsEmpty());
	m_legendButton2.ShowWindow(!m_strLegend2.IsEmpty());
	m_legendButton3.ShowWindow(!m_strLegend3.IsEmpty());
	m_legendButton4.ShowWindow(!m_strLegend4.IsEmpty());
	m_legendButton5.ShowWindow(!m_strLegend5.IsEmpty());
	m_legendButton6.ShowWindow(!m_strLegend6.IsEmpty());
	m_legendButton7.ShowWindow(!m_strLegend7.IsEmpty());

	UpdateData(FALSE); // Send data to dialog box

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


/************************************************************************************************
 ******** execution sequence for scope control ******************
CdrawBase::createMenu cycles through the child list and executes a createControl on each one.
CdrawScope::createControl gets the devobj item ptr and 
if a Graph (static w/ waveforms)			if a Chart (dynamic,time,w/sources)
	CdrawBase::createControl					CdrawBase::createControl	
		[ base contruction: Cwnd, dyncntrl]
	get the CScopeCtrl ptr						get the CScopeCtrl ptr
	CScopeCtrl::CreateGraphControl				CScopeCtrl::CreateChartControl
		does window:CreateControl(library..)		determines type & calls CreateXXXChart
		sets the library control's background &misc	 ie:CScopeCtrl::CreateScopeChart
															window::CreateControl(library..)
															sets library control's bckgrnd &misc
															creates PaintChartComponentProc running
						-------CScopeCtrl::PaintChartComponentProc---------
						gets CScopeCtrl's m_nCycleTime to a local <<<<<<<< error, uninitialized
						HaveDataEvent{0}, QuitThreadEvent{1} waiting4 are filled at contruction
						forever loop
							wait for one of waiting4 signals [forever]
							@ quit - break the for loop
							@ NOT havedata - break the for loop 
							//else is have data
							if datapoints, get the front currDataLst
							BY CHART TYPE
							ie cT_Scope----------
								for each indataLst
									do an AddXY on the on the point @ nCurrentXaxisPos
									nCurrentXaxisPos += local-CycleTime
							loop forever
						--- just return when done							
	CScopeCtrl::InitGraph						CScopeCtrl::InitChart
													call correct Init func
													ie CScopeCtrl::InitScopeChart
		set title & text								set title & text
		show control									show control
		CScopeCtrl::InitWaveforms						CScopeCtrl::InitSources
			get list of waveforms							get list of sources
			for each waveform:register color				for each source: register color
		setup Xaxis										setup Xaxis
		getCycleTime() from dev obj						getCycleTime() from dev obj	
	CScopeCtrl::DisplayWaveforms				CScopeCtrl::DisplayChart
													call correct DisplayXXXChartType
													ie CScopeCtrl::DisplayScopeChart
		for each item in the currentWaveList			for each item in the currentSourceList
			if NOT refresh flag - doInitActions
			else -           - doRefreshActions
			get color and pointList							add Y axis as required
			add X & Y axis as required						get color and memberList	
			set line parameters inc emphasis				for each varPtr member
			get type and call Plot(XYlist..)					set line parameters inc emphasis
			handle memory cleanup								add to legend
			plot the keypoints                                  put item into subscription list
														subscribe to the subscription list
************************************************************************************************/
