/*************************************************************************************************
 *
 * $Workfile: HartTrans.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		HartTrans.cpp: implementation of the CHartTrans class.
 */

#include "stdafx.h"
#include "DDIde.h"
#include "HartTrans.h"
#include "DDIdeDoc.h" // this includes DDLBase.h

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHartTrans::CHartTrans()
{
    TRACE("CHartTrans::CHartTrans()\n");
    m_pDoc =  NULL;
    m_handle = 0;
}

// Copy Constructor
CHartTrans::CHartTrans(const CHartTrans& HartTrans)
{
    TRACE("CHartTrans::CHartTrans() - Copy Constructor\n");
    m_pDoc = HartTrans.m_pDoc;	
}

CHartTrans::~CHartTrans()
{
    TRACE("CHartTrans::~CHartTrans()\n");
}

// reset the comm state
void CHartTrans::reset(void)
{
	clog << "Reset HartTrans is currently empty."<<endl;
	if (m_pDoc == NULL)
    {	Initialize();
	}
}

RETURNCODE CHartTrans::Initialize()
{
    TRACE("CHartTrans::Initialize()\n");

    /*
     * Get pointer to document if not already done so
     */
    if (m_pDoc == NULL)
    {
        CDocManager * pManager = AfxGetApp()->m_pDocManager;
	    ASSERT ( pManager != NULL ); 

	    POSITION posTemplate = pManager->GetFirstDocTemplatePosition();
	    while ( posTemplate !=NULL )
        {
		      // get the next template
		      CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
		      POSITION posDoc = pTemplate->GetFirstDocPosition();
		      while( posDoc !=NULL )
		      {
			        m_pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
		      }
        }
    }

    return ( (m_pDoc != NULL) ? SUCCESS : APP_NEEDS_REINIT);
}

RETURNCODE CHartTrans::Connect(unsigned char addr)
{
	// Put of HART Select dialog box for user to select instrument to read
    return (m_pDoc->CnctToDevice(m_handle));
}

// Async Send
RETURNCODE CHartTrans::SendCmd(int transNum, int cmdNum, BYTE* pData, BYTE dataCnt, int timeout)
{
    TRACE("CHartTrans::SendCmd() - Async\n");
/*  Later - Need to deal with timeout */

    return ( m_pDoc->QueueTrans(cmdNum, pData, dataCnt, m_handle, transNum) );
}

// Async Reply
RETURNCODE CHartTrans::GetReply(int& transNum, int& cmdNum, BYTE* pData, BYTE dataCnt, cmdInfo_t& cmdStatus)
{
    TRACE("CHartTrans::GetReply\n");

    return ST_OK;
}

// Sync Send
RETURNCODE CHartTrans::SendCmd(int cmdNum, BYTE* pData, BYTE& dataCnt, int timeout, cmdInfo_t& cmdStatus)
{
	RETURNCODE rc;

    //TRACE("CHartTrans::SendCmd() - Sync\n");
#ifdef IS_OLE
    return (m_pDoc-> Send(cmdNum, pData, dataCnt, cmdStatus, m_handle) );
#else // pipe 
	// cmdNum is now itemID requested <0 is id request>
	// pData is just that - ascii value of value (unquoted, null terminated)
	// dataCnt number of bytes to send and return value for number received (strlen of pData)
	//			Note: Programmer is responsible for pData being large enough for the resonse
	// timeout is now isWrite
	// cmdStatus is unused

	DWORD  cbWritten, cbRead;
	BOOL   isSuccess;	
	HANDLE localHand = (HANDLE)m_handle;
	char writeBuffer[128];
	BYTE* pBuf;
	BYTE  cntBuf;

	if ( cmdNum  == 0 )
	{
		strcpy((char*)pData,"READ 0");
		cntBuf = 6;
		pBuf = pData;
	}
	else
	if ( timeout /*isWrite*/ )
	{                   // symid  valLen  
		sprintf(writeBuffer,"WRITE %d %d \"", (unsigned int)cmdNum,dataCnt);//,respBuf
		cntBuf = strlen(writeBuffer);
		memcpy(writeBuffer +cntBuf, pData,dataCnt);//value
		cntBuf += dataCnt;
		writeBuffer[cntBuf++] = '\"';
		writeBuffer[cntBuf] = '\0';
		pBuf = (BYTE*)(&(writeBuffer[0]));
	}
	else
	{
		sprintf((char*)pData,"READ %d",(unsigned int)cmdNum);
		cntBuf = strlen((char*)pData);
		pBuf = pData;
	}
		
	isSuccess = WriteFile( 
		  localHand,          // pipe handle 
		  pBuf,           // message 
		  cntBuf,         // message length 
		  &cbWritten,             // bytes written 
		  NULL);                  // not overlapped 
	if (! isSuccess) 
	{//MyErrExit("WriteFile"); 
	  LOGIT(CERR_LOG, "ERROR: file WRITE failed.\n");
	  rc = APP_ER_FAILED;
	}
	else
	{ 
	   do 
	   { // Read from the pipe. 

			isSuccess = ReadFile( 
				 localHand,	// pipe handle 
				 pData,			// buffer to receive reply 
				 PIPEBUFFERSIZE,// size of buffer 
				 &cbRead,		// number of bytes read 
				 NULL);			// not overlapped 

			if (! isSuccess && GetLastError() != ERROR_MORE_DATA) 
			{
				LOGIT(CERR_LOG,"ERROR: reading from pipe.\n");
				cbRead = 0;
				rc = APP_ER_FAILED;
				break; 
			}
			else
			{
				dataCnt = cbRead;
				rc = SUCCESS;
			}

		  // Reply from the pipe is written to STDOUT. 

		  //if (! WriteFile(GetStdHandle(STD_OUTPUT_HANDLE), 
		//	 chBuf, cbRead, &cbWritten, NULL)) 
		  //{
		//	 break; 
		  //}
		  pData[cbRead] = '\0'; // be sure it's terminated
		  LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"RECEIVED |%s| message.\n",pData);		// was=>  <<pData<<"| message."<<endl;
//		  TRACE("RECEIVED |%s| message.\n",pData);


	   } while (! isSuccess);  // repeat loop if ERROR_MORE_DATA 
	   if ( ! timeout)
	   {
		rc = ParseCmd(pData, dataCnt, cmdStatus);
	   }
	}
	return rc;
#endif
}

// entry  buffer,  cnt,     empty status
// return dataPtr, dataCnt, full cmdInfoStruct // data moved to top of buffer
RETURNCODE CHartTrans::ParseCmd(BYTE* pData, BYTE& dataCnt, cmdInfo_t& cmdInfo)
{
	RETURNCODE rc = SUCCESS;

#ifndef IS_OLE		//just for pipes right now
//	parse the ID,space,responseCode,space,dataLen,space,quote,data,quote

	cmdInfo.clear();

	const int bufsize = 200;
	//char buf[bufsize];
	char* buf = (char*)pData;
	char wrkBuf[64];
	char *seps = " "; // only a space
	char *p;
	int  len = 0;
	char buf2[bufsize];
	int wrkInt;
	
		
	memcpy(buf2, pData, dataCnt);// hold the message
		
	// ID<put in address>
	p = strtok(buf, seps);// replaces tok w/null
	if ( p != NULL)
	{
		len += (strlen(p) + 1);
		strcpy(wrkBuf, p);
		wrkInt = atoi(wrkBuf);
	}
	else
	{
		wrkInt = 0;
	}
	// cmd | ID
	cmdInfo.command = wrkInt;

	
	// responseCode
	p = strtok(NULL, seps);
	if ( p != NULL )
	{
		len += (strlen(p) + 1);
		strcpy(wrkBuf, p);
		wrkInt = atoi(wrkBuf);
	}
	else
	{
		wrkInt = 0xFF;
	}
	cmdInfo.RespCd = wrkInt & 0x00FF;
	
	// dataLen
	p = strtok(NULL, seps);
	if ( p != NULL )
	{
		len += (strlen(p) + 1);
		strcpy(wrkBuf, p);
		wrkInt = atoi(wrkBuf);
	}
	else
	{
		wrkInt = 0;
	}
	// leave it in wrkInt

	// value
	p = buf2 + len;				// find field in buf2, keeping commas embedded in 3rd field
	if ( p[0] == '\"')
	{	
		memcpy(pData,++p,wrkInt);
		pData[wrkInt] = '\0';   // just to help visualization
	}
	else
	{//error
		rc = FAILURE;
	}	// erase trailing quote
		

#endif // NOT OLE
	return rc;
}


/*************************************************************************************************
 *
 *   $History: HartTrans.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
