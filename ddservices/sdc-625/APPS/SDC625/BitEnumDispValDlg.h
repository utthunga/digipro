#if !defined(AFX_BITENUMDISPVALDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_)
#define AFX_BITENUMDISPVALDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitEnumDispValDlg.h : header file
//

#include "SDC625Doc.h"
#include "sdcDialog.h" // POB - 8 Aug 2008

#define MAX_STAT_BITS ((sizeof(int))*8 ) /* stevev-26jul07 - was::> 8*/

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValDlg dialog

class CBitEnumDispValDlg : public sdCDialog  // POB - 8 Aug 2008
{
// Construction
	CValueVarient		m_pVarval;
	hCEnum              *pEn;
	CDDIdeDoc			*m_pDoc;
	CButton				*m_pBtnArray[MAX_STAT_BITS];
	CString				strStatusString[MAX_STAT_BITS];
	BYTE				byStatusValue[MAX_STAT_BITS];	//Added by ANOOP 06APR2004
	//int					nBitCount;
public:
	CBitEnumDispValDlg(CValueVarient tmpVar,hCVar *pVar,CDDIdeDoc *pDoc,CWnd* pParent = NULL);   // standard constructor
	~CBitEnumDispValDlg();
// Dialog Data
	//{{AFX_DATA(CBitEnumDispValDlg)
	enum { IDD = IDD_BITENUMVAL_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitEnumDispValDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBitEnumDispValDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	
};



/**************************************** 26jul07 *************************************/
/////////////////////////////////////////////////////////////////////////////
// CDlgScrollable dialog

class CDlgScrollable : public CDialog
{
	CValueVarient		m_pVarval;
	hCEnum              *pEn;
	CDDIdeDoc			*m_pDoc;
	CButton				*m_pBtnArray[MAX_STAT_BITS];
	CString				strStatusString[MAX_STAT_BITS];
	unsigned			byStatusValue[MAX_STAT_BITS];	//Added by ANOOP 06APR2004

// Construction
public:
	int					nBitCount;
	void EndDrag();
	//CDlgScrollable(CWnd* pParent = NULL);   // standard constructor
	CDlgScrollable(CValueVarient tmpVar,hCVar *pVar,CDDIdeDoc *pDoc,CWnd* pParent); 
	~CDlgScrollable();

	HCURSOR m_hCursor1, m_hCursor2;

	// dialog size as you see in the resource view (original size)
	CRect	m_rcOriginalRect;

	// dragging
	BOOL	m_bDragging;
	CPoint	m_ptDragPoint;

	// actual scroll position
	int		m_nScrollPos;
    int     m_hScrollPos;

	// actual dialog height
	int		m_nCurHeight;

    int     m_hCurWidth;// client coords

// Dialog Data
	//{{AFX_DATA(CDlgScrollable)
	enum { IDD = IDD_SCROLLABLECHILD };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgScrollable)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgScrollable)
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_BITENUMDISPVALDLG_H__0576A884_E079_4926_B03B_C9C60E8D8A85__INCLUDED_)
