// WindowMap.cpp : implementation file
//

// Each control in a Window View will have CWnd object.  All information 
// about this control will including its pointer to the object will
// be contained in this class

// If there are 3 edit controls then their will be 3 CWindowMap objects
// The IdWindowMap object will take care of all the controls for each
// particular child window.

#include "stdafx.h"
//#include "SDC625.h"
#include "Window_Map.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWindowMap

CWindowMap::CWindowMap()
{
//	AfxMessageBox("CWindowMap default contructor");
	m_pWnd = NULL;
}

CWindowMap::~CWindowMap()
{
//	AfxMessageBox("CWindowMap destroyed");
//	if (m_pWnd)
//		delete m_pWnd;
}

CWindowMap::CWindowMap(CWnd * pWnd)
{
//	AfxMessageBox("CWindowMap pWnd contructor");
	m_pWnd   = pWnd;
	nUnitID  = 0;
	nLabelID = 0;
}

// Copy Constructor
CWindowMap::CWindowMap(const CWindowMap & WindowMap)
{
//	AfxMessageBox("CWindowMap copy constructor");
	m_pWnd = WindowMap.m_pWnd;
	nUnitID  = WindowMap.nUnitID;
	nLabelID = WindowMap.nLabelID;
}

// Overloaded assignment operator
const CWindowMap &CWindowMap::operator=(const CWindowMap & WindowMap)
{
	if (&WindowMap != this) // avoids self assignment
	{
		m_pWnd = WindowMap.m_pWnd;
		nUnitID  = WindowMap.nUnitID;
		nLabelID = WindowMap.nLabelID;
	}

	return *this;
}

/*BEGIN_MESSAGE_MAP(CWindowMap, CWnd)
	//{{AFX_MSG_MAP(CWindowMap)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()*/


/////////////////////////////////////////////////////////////////////////////
// CWindowMap message handlers
