/**********************************************************************************************
 *
 * $Workfile: LabelDialog.h $
 * 29Jul11 - stevev
 *     
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2011, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		Dialog to show the entire label
 *
 *
 *		
 * #include "LabelDialog.h"
 */

/* dialog classes in SDC::
	class CDlgScrollable : public CDialog
	class CDeviceDetails : public CDialog
	class CDeviceStatus :  public CDialog
											class CDynControls : public 	sdCDialog// CDialog
											class CMenuDlg : public CdrawDlg
	class CEDisplayDlg :   public CDialog
	class CMethodDlg :     public    sdCDialog//  CDialog
	class CRespCodeDevStat:public CDialog
	class CVariableDlg :   public CDialog
	class CWaoDlg :        public CDialog
	class CWaveEditDlg :   public CDialog
**********************************************************************************************/
#ifndef _LABEL_DIALOG_H
#define _LABEL_DIALOG_H

#ifdef INC_DEBUG
#pragma message("In LabelDialog.h") 
#endif

#include "sdcDialog.h"

#ifdef INC_DEBUG
#pragma message("    Finished includes from LabelDialog.h") 
#endif

class CLabelDlg : public sdCDialog
{
// Construction
public:
	CLabelDlg( CString strCaption, CString strMessage, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDeviceDetails)
	enum { IDD = IDD_DISPLAY_DIALOG };
	//}}AFX_DATA

    void Display();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLabelDlg)
	protected:   
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLabelDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString	m_strMessage;
	CString m_strCaption;
};

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
class CHelpBox : public CLabelDlg  
{
public:
	CHelpBox( CString strCaption, CString strHelp, CWnd* pParent = NULL);
	virtual ~CHelpBox();
};

#endif // _LABEL_DIALOG_H