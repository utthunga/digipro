// ErrLogView.cpp: implementation of the CErrLogView class.
//
//////////////////////////////////////////////////////////////////////

#pragma warning (disable : 4786)
#include "stdafx.h"
#include "SDC625.h"
//#include "errorlog.h"
#include "logging.h"
#include "ErrLogView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define MAX_BUFF_SIZE 20000

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView

IMPLEMENT_DYNCREATE(CErrLogView, CListView)

BEGIN_MESSAGE_MAP(CErrLogView, CListView)
	//{{AFX_MSG_MAP(CDDIdeListView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView drawing

CErrLogView::CErrLogView() //: CListView()
{
	m_nMessagesShown =0;
	m_pList =NULL;
	m_byInitialized = FALSE; //Added by ANOOP
	timerID = 0;
}

CErrLogView::~CErrLogView()
{
}

void CErrLogView::OnInitialUpdate()
{
	if(!m_byInitialized)	//Modifed by ANOOP
	{
		CListView::OnInitialUpdate() ;
		m_pList = new CListCtrl();
		
		RECT rect;
		GetClientRect(&rect);

	// Create list ctrl
		if (m_pList )
		{
			m_pList ->Create(LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SINGLESEL, rect, this,1);
			m_pList ->ShowWindow(SW_SHOW);
		}

		m_pList->DeleteAllItems();
		while( m_pList->DeleteColumn(0))
		{
		}

	// Add a column 
/*Vibhor 130204: Start of Code*/

// was	CString strTitle = "Error Messages";

		CString strTitle = "Event-Status Log";

	/*Vibhor 130204: End of Code*/
		m_pList->InsertColumn( 0, strTitle, LVCFMT_LEFT, /*450*/ rect.right - 20, 0 );
		
		// Get Number of messages 
		int nMessages;
		char chMessageVal[MAX_BUFF_SIZE];

		LOGIT(UI_LOG,"Latest Messages:");
		GetNoOfLogMsgs(&nMessages);		
			m_nMessagesShown =0;
			m_nMesgShown =0;

		for(int nCount = m_nMessagesShown ;nCount < nMessages; nCount++)
		{
			if(GetLogMsgFromLog(nCount,chMessageVal,MAX_BUFF_SIZE))
			{
	// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
				CString ucStr;
				AsciiToUnicode(chMessageVal, ucStr);
				m_pList->InsertItem (nCount,ucStr);
#else
				m_pList->InsertItem (nCount,chMessageVal);
#endif
			}
		}

		//Let us not show existing messages 
		m_nMessagesShown = nMessages + 1;

		m_nMesgShown = m_nMessagesShown;

		
		m_byInitialized = TRUE;
	}
	
	m_nMessagesShown = m_nMesgShown;
	

	// Create timer
	CreateTimer();		//ANOOP COmmented and modified the function below
	
}


int CErrLogView::CreateTimer()
{
	timerID = SetTimer (ERRLOG_TIMER, 10 * ONE_SECOND, NULL);
	if (!timerID) 
	{
		MessageBox (_T ("SetTimer failed"), _T ("Error"),
			MB_ICONSTOP | MB_OK);
		return -1;
	}
	return 0;
}

// On timer Function  Log messages in UI
void CErrLogView::OnTimer(UINT nIDEvent) 
{
	int nMessages =0;
	char chMessageVal[MAX_BUFF_SIZE];

	CEdit *pEdit =NULL;
	CListBox *pListBox =NULL;
	int i=0;
	int nLBItemCount=0;

	switch(nIDEvent)
	{
	case ERRLOG_TIMER:
		GetNoOfLogMsgs(&nMessages);	
		i = m_nMessagesShown;
		for(i= m_nMessagesShown ; i < nMessages; i++)
		{
			if(GetLogMsgFromLog(i,chMessageVal,MAX_BUFF_SIZE))
			{
	// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
				CString ucStr;
				AsciiToUnicode(chMessageVal, ucStr);
				m_pList->InsertItem (i,ucStr);
#else
				m_pList->InsertItem (i,chMessageVal);
#endif
			}
		}
		m_nMessagesShown = nMessages;
		break;
	default:
		break;
	}
//	CListView::OnTimer (nIDEvent);
}

void CErrLogView::OnSize(UINT nType, int cx, int cy)
{
	CListView::OnSize(nType, cx, cy);

	if (m_pList != NULL)
		m_pList->MoveWindow (0, 0, cx, cy);
}

void CErrLogView::OnDestroy ()
{
	CListView::OnDestroy ();
	delete m_pList;
	m_pList  = NULL;
	if (timerID)
		KillTimer(timerID);
	timerID = 0;
}

/*<START>Added by ANOOP */
void CErrLogView::DisplayLogMessage()
{
	int nMessages =0;
	char chMessageVal[MAX_BUFF_SIZE];

	CEdit *pEdit =NULL;
	CListBox *pListBox =NULL;
	int i=0;
	int nLBItemCount=0;


	GetNoOfLogMsgs(&nMessages);		
	for(i= m_nMessagesShown ;i < nMessages;i++)
	{
		if(GetLogMsgFromLog(i,chMessageVal,MAX_BUFF_SIZE))
		{
// stevev - WIDE2NARROW char interface
#ifdef _UNICODE
			CString ucStr;
			AsciiToUnicode(chMessageVal, ucStr);
			m_pList->InsertItem (i,ucStr);
#else
			m_pList->InsertItem (i,chMessageVal);
#endif
		}
	}
	m_nMessagesShown = nMessages;
	m_nMesgShown = 	m_nMessagesShown;
	return;
}
/*<END>Added by ANOOP */

/*<START>03FEB2004 Added by ANOOP For cleaning up the error log when new device is clicked	*/
void CErrLogView::EmptyListCtrl()
{
	if(NULL != m_pList)
	{
		ClearLog();
		m_pList->DeleteAllItems();
	}
	return;
}
/*<END>03FEB2004 Added by ANOOP For cleaning up the error log when new device is clicked	*/
