// ColorButton.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "SButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSButton

IMPLEMENT_DYNAMIC( CSButton, CButton )

CSButton::CSButton()
{
	m_strLabel = "";
}

CSButton::~CSButton()
{
}


BEGIN_MESSAGE_MAP(CSButton, CButton)
	//{{AFX_MSG_MAP(CSButton)
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSButton message handlers

void CSButton::SetToolTipText(CString sTip)
{
	//Use default tooltip if sTip is empty
	if (sTip.IsEmpty())
	{
		sTip = "Unknown";
	}

	if (m_ToolTip.m_hWnd == NULL)
	{
		m_ToolTip.Create(this,TTS_ALWAYSTIP|TTS_NOPREFIX);
	}
	
	int count = m_ToolTip.GetToolCount();
	
	// If there is no tooltip defined then add it
	if (m_ToolTip.GetToolCount() == 0)
	{
		CRect rectBtn; 
		GetClientRect(rectBtn);
		m_ToolTip.AddTool(this, sTip);
		m_ToolTip.SetDelayTime( 5000, 100 );
		m_ToolTip.Activate(TRUE);
	}

	// Set text for tooltip
	m_ToolTip.UpdateTipText(sTip, this, 1);
}

void CSButton::OnMouseMove(UINT nFlags, CPoint point) 
{
/*	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = HOVER_DEFAULT;
		m_bTracking = _TrackMouseEvent(&tme);
	}*/

	CButton::OnMouseMove(nFlags, point);
}

LRESULT/*void PAW 03/03/09*/ CSButton::OnMouseHover(WPARAM wparam, LPARAM lparam) 
{
//	GetParent()->PostMessage(WM_RBUTTONDOWN);
	return (NULL);	// PAW 03/03/09
}

LRESULT CSButton::OnMouseLeave(WPARAM wparam, LPARAM lparam)
{
/*	m_bTracking = FALSE;
	m_bHover=FALSE;
//	Invalidate();

	if (m_ToolTip.m_hWnd)
	{
		if (m_ToolTip.GetToolCount() != 0)
		{
			m_ToolTip.Activate(FALSE);
		}
	}*/
		
	return 0;
}

void CSButton::OnRButtonDown(UINT nFlags, CPoint point) 
{
    // Ignore CPoint value.  It will not satisfy CdrawBase::HitTest
    // Get a clean version, POB - 4/14/2014
    DWORD dwPosition = GetMessagePos();
    
    GetParent()->PostMessage(WM_RBUTTONDOWN, nFlags, dwPosition);
	CButton::OnRButtonDown(nFlags, point);
}

BOOL CSButton::PreTranslateMessage(MSG* pMsg) 
{
	if (m_ToolTip.m_hWnd)
	{
		m_ToolTip.RelayEvent(pMsg);
	}

	return CButton::PreTranslateMessage(pMsg);
}


/*
 * NOTE:  This is duplicate code (with some modifications) copied from CMainListCtrl.
 * Eventually, we need to clean this up where all the objects are calling one function.
 */
LPCTSTR CSButton::MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
	static const _TCHAR szThreeDots[]=_T("...");

	int nStringLen=lstrlen(lpszLong);
	
   
   
   if(nStringLen==0 )
		return(lpszLong);   
	SIZE size;
#if 1
   if (pDC->m_hAttribDC != NULL)
   {
       // NOTE: use GetTextExtentPoint32 in Win32 for better results
   if ( GetTextExtentExPoint(/*pDC->GetSafeHdc()*/pDC->m_hAttribDC, lpszLong, nStringLen,0,NULL,NULL, &size) == 0 )
      {//error
		TRACE(_T("TextExtent ------- Failure.\n"));
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();

TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif
		   return lpszLong;
      }
#ifdef xxx_DEBUG

CFont* pDCfont = pDC->GetCurrentFont( ) ;
CWnd*  pDCwin  = pDC->GetWindow( ) ;
int    hRes = pDC->GetDeviceCaps( HORZRES ) ;
int    vRes = pDC->GetDeviceCaps( VERTRES ) ;
int    hlpX = pDC->GetDeviceCaps( LOGPIXELSX) ;
int    nCol = pDC->GetDeviceCaps( NUMCOLORS) ;
int    nTxC = pDC->GetDeviceCaps( TEXTCAPS   );
int    rOP2 = pDC->GetROP2();
int    mapMd= pDC->GetMapMode();
CSize  extSz= pDC->GetViewportExt( ) ;
CSize  strSz= pDC->GetOutputTextExtent( lpszLong, nStringLen ) ;
TEXTMETRIC Metrics ;
pDC->GetTextMetrics( &Metrics ) ;

#endif

   //  TRACE(_T("TextExtent Success.\n"));
   }
//   if(size.cx + nOffset <=nColumnLen )
   if(size.cx + nOffset < nColumnLen )
		return(lpszLong);
#else
	if(nStringLen==0 || ((pDC->GetTextExtent(lpszLong,nStringLen).cx + nOffset) <=nColumnLen) )
		return(lpszLong);
#endif

	static _TCHAR szShort[MAX_PATH];

	lstrcpy(szShort,lpszLong);
	int nAddLen=pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

	for(int i=nStringLen-1; i>0; i--)
	{
		szShort[i]=0;
//		if(pDC->GetTextExtent(szShort,i).cx+nOffset+nAddLen<=nColumnLen)
		if(pDC->GetTextExtent(szShort,i).cx+nOffset+nAddLen < nColumnLen)
			break;
	}

	lstrcat(szShort,szThreeDots);

	return(szShort);
}

void CSButton::SizeToContent()
{
	int nMaxLenText = 0;
	int nOffset = 8;
	
	CClientDC dc (this);

	dc.SelectObject (GetFont());

	// Obtain label:
	CString strLabel;
	GetWindowText (strLabel);

	CRect rectClient;
	GetClientRect (rectClient);

	nMaxLenText = (rectClient.Width() * 3) - 5;

	//Get the URL text size
	CRect rectText = rectClient;
	dc.DrawText (strLabel, rectText, DT_SINGLELINE | DT_CALCRECT);
	
	if (rectText.Width() >= rectClient.Width())
	{
		/* 
		 *  We need to resize the button so that the text will fit 
		 */
		
		// Convert client coords to the parents client coords
		ClientToScreen(rectText);
		CWnd* pParent = GetParent();
		
		if (pParent)
		{
			pParent->ScreenToClient(rectText);
		}

		int diff = 0;
		int widthButton = 0;

		if (rectText.Width() > nMaxLenText/*(rectClient.Width() * 3) - 5*/)
		{
			// Calculate the differences in width so that we can center the button
			// Maxium text length minus the curent button size
			diff = nMaxLenText  /*(rectClient.Width() * 3 - 5)*/ - rectClient.Width();
			
			// Add some space
			rectText.left-=nOffset;
			widthButton = nMaxLenText + nOffset*2;
			
			SetToolTipText(strLabel);
			
			// We have updated the string label of the button so save the value here
			// so that this function is not called again on the same name
			m_strLabel = MakeShortString(&dc, strLabel, widthButton, nOffset+2);

			SetWindowText((LPCTSTR)m_strLabel);
		}
		else
		{
			// Calculate the differences in width so that we can center the button
			// Text size needed minus the curent button size
			diff = rectText.Width() - rectClient.Width();

			// Add some space
			rectText.left-=nOffset;
			rectText.right+=nOffset;

			widthButton = rectText.Width();
			SetWindowText((LPCTSTR)strLabel);
			//CWnd::OnSetText(0,strLabel);
		}

		// Resize the control
		MoveWindow(rectText.left-diff/2, rectText.top, widthButton/* rectText.Width()*/, rectClient.Height(), TRUE);
	}
}

HBRUSH CSButton::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	/* 
	 * NOTE: This function gets called by windows some time after
	 *       the SetWindowText is called and before the button
	 *       gets drawn.  For that reason, this function appears 
	 *		 to be an accptable location to call "SizeToContent"
	 */

	CString strLabel;

	// Obtain the label of the button
	GetWindowText (strLabel);

	// Call only when text has been updated 
	if (strLabel != m_strLabel)
	{
		m_strLabel = strLabel;
		SizeToContent();
	}

	// NOTE: Return a non-NULL brush if the parent's handler should not be called
	return NULL;
}
