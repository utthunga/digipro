#if !defined(AFX_COMMAND48_PROPSHEET_H__65D624D1_788F_499A_9882_4B267A900A8D__INCLUDED_)
#define AFX_COMMAND48_PROPSHEET_H__65D624D1_788F_499A_9882_4B267A900A8D__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Command48_PropSheet.h : header file
//


#include "stdafx.h"
#include "DDLBase.h"
//#include "DDLDevice.h"	
#include "DDLVariable.h"
#include "DevCommStatPropPage.h"
#include "DevStatPageOne.h"
#include "MainFrm.h" 

//#include "DevCommStatPropPage.h" //Vibhor 270904: Removed duplicate include



/////////////////////////////////////////////////////////////////////////////
// CCommand48_PropSheet
/* stevev 26jan 05 - was 17, spec says 0 - 24...This should be the ONLY numeric definition */
#define TOTAL_PROP_PAGES 25  //Added by ANOOP


class CMainFrame;



class CCommand48_PropSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CCommand48_PropSheet)
	CArray<CMD48_RESPONSE,CMD48_RESPONSE &> m_Arr;	
	CDevStatPageOne *m_pDevStatPageOne[TOTAL_PROP_PAGES];
	CDevCommStatPropPage *m_pCommStatus;
	CDevCommStatPropPage *m_pDeviceStatus;
	CArray<CMD48_RESPONSE,CMD48_RESPONSE &> cmd48resp_PreviousArray;
	CArray<CMD48_RESPONSE,CMD48_RESPONSE &> cmd48resp_PresentArray;
	int nTot_PresentCount;
	int nTot_PreviousCount;
	CMainFrame *m_pMainFrm;
	int nTot_Count;
	int nExtendedInfoTabs;
	
// Construction
public:
//	CCommand48_PropSheet(CDDLVariable *pDevStat, CDDLVariable *pCommStat,CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_Array,int nTotCount,CMainFrame *ptrMainFrm,UINT nIDCaption,CWnd* pParentWnd=NULL,UINT iSelectPage=0);
	CCommand48_PropSheet(CDDLVariable *pDevStat, CDDLVariable *pCommStat,CMainFrame *ptrMainFrm,UINT nIDCaption,CWnd* pParentWnd=NULL,UINT iSelectPage=0);
	int nSheetCnt;
	BOOL m_ExtendedTabCreated;
	BOOL GetExtendedDeviceStatusInfo( CArray<CMD48_RESPONSE,CMD48_RESPONSE &> & ,int &); 
	BOOL CreateIndividualTabs(CArray<CMD48_RESPONSE,CMD48_RESPONSE &> &m_Array,int);
//	CCommand48_PropSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
//	CCommand48_PropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes



public:
	BOOL UpdateCommStatusOnNotification(unsigned char); 	
	BOOL UpdateDeviceStatusOnNotification(unsigned char);
	BOOL UpdateSuppressMessageTab();
	BOOL AddSuppressMessageTab();
	BOOL RemoveSuppressMessageTab();
	BOOL CreateExtendedInfoTab();
	BOOL UpdateExtendedInfoTab();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommand48_PropSheet)
	public:
	virtual BOOL OnInitDialog(); 
	virtual BOOL DestroyWindow(); 
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCommand48_PropSheet();

protected:
	CDDLVariable *m_pDevStat;
	CDDLVariable *m_pCommStat;
	// Generated message map functions
protected:
	
	//{{AFX_MSG(CCommand48_PropSheet)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	afx_msg LRESULT OnMessageCreateExtendedTab(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageUpdateExtendedTab(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
	
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMAND48_PROPSHEET_H__65D624D1_788F_499A_9882_4B267A900A8D__INCLUDED_)
