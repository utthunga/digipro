#if !defined(AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_)
#define AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScopeCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScopeCtrl window

class CScopeCtrl : public CStatic
{
// Construction
public:
	CScopeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScopeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	CBitmap m_Bitmap;
	virtual ~CScopeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CScopeCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCOPECTRL_H__FB6C00AB_8A1C_463E_9415_7288C1EEF896__INCLUDED_)
