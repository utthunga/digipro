// ColorEdit.cpp : implementation file

#include "stdafx.h"
#include "ColorEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorEdit

const COLORREF CColorEdit::m_WHITE = RGB(255, 255, 255);
const COLORREF CColorEdit::m_YELLOW = RGB(255, 255, 0);

IMPLEMENT_DYNAMIC( CColorEdit, CEdit )

CColorEdit::CColorEdit()
{
    m_BkColor = m_WHITE;
    m_state = FALSE;
    m_wParseStyle = 0;
    nLeft_of_decimal=0;
    m_isEnabled = TRUE;  //by default, the control is always enabled, POB - 28 Aug 2008
}

CColorEdit::~CColorEdit()
{
}

BEGIN_MESSAGE_MAP(CColorEdit, CEdit)
    //{{AFX_MSG_MAP(CColorEdit)
    ON_WM_RBUTTONDOWN()
    ON_WM_LBUTTONDOWN()
    ON_WM_CHAR()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorEdit message handlers

BOOL CColorEdit::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
    if (message != WM_CTLCOLOREDIT)
    {
        return CEdit::OnChildNotify(message, wParam, lParam, pLResult);
    }
    
    HDC hdcChild = (HDC) wParam;
    SetBkColor(hdcChild, m_BkColor);
    HBRUSH brush = (HBRUSH)m_brush;
    brush = CreateSolidBrush(m_BkColor);

    //Send what would have been the return value of OnCtlColor() - the 
    // brush handle - back in pLResult;
    *pLResult = (LRESULT)(brush);
    // Return TRUE to indicate the message was handled

    return TRUE;
}

void CColorEdit::Highlight(BOOL state)
{
    // The control should not be able to be changed by the user
    // while its disable so prevent any 'yellow' highlighting, POB - 28 Aug 2008
    if (m_isEnabled)
    {
        m_state = state;

        if (m_state == TRUE)
        {
            m_BkColor = m_YELLOW;
        }
        else
        {
            m_BkColor = m_WHITE;
        }

        Invalidate();
    }
}

void CColorEdit::OnRButtonDown(UINT nFlags, CPoint point) 
{
    // Ignore CPoint value.  It will not satisfy CdrawBase::HitTest
    // Get a clean version, POB - 4/14/2014
    DWORD dwPosition = GetMessagePos();
    
    GetParent()->PostMessage(WM_RBUTTONDOWN, nFlags, dwPosition);

    CEdit::OnRButtonDown(nFlags, point);
}

// Removed CColorEdit::OnLButtonDown
// Posting a message to the parent does not appear to
// do anything, POB - 5/1/2014

BOOL CColorEdit::PreTranslateMessage(MSG* pMsg) 
{
    if (pMsg->message == WM_KEYDOWN)
    {
        if (pMsg->wParam == VK_RETURN)
        {
            int id = GetDlgCtrlID();
            WPARAM wParam = MAKEWPARAM(id, VK_RETURN);

            GetParent()->PostMessage(WM_COMMAND,wParam, NULL );
            return TRUE;
        }
        // Deleted commented out code - POB, 15 Aug 2008

        if (pMsg->wParam == VK_ESCAPE)
        {
            int id = GetDlgCtrlID();
            WPARAM wParam = MAKEWPARAM(id, VK_ESCAPE);

            GetParent()->PostMessage(WM_COMMAND,wParam, NULL );
            return TRUE;
        }
    }

    return CEdit::PreTranslateMessage(pMsg);
}

void CColorEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    char *pdest=NULL;
    CString szText="",szDisp_val_old="",szDisp_val_new="";
    int nFirstPos = 0,nLastPos = 0;
    BOOL bChar=FALSE;
    bool makeNoise = false;
    
    // When the control is disabled, we do not want any 'key stroke'
    // to be entered by the user.  This does not prevent the 'tab'
    // to cause a loss of focus.  However, the 'enter' key does not
    // function when the control is disabled, POB - 28 Aug, 2008
    if (!m_isEnabled)
    {
        return;
    }
    
    GetWindowText(szText);
    szDisp_val_old=szText;

    if ( ! GetModify( ) )// first time in here
    {
        SetLimitText(nMax_Chars);
    }
    // above is more accurate, the value can change due to conditionals etc
    //if ( TRUE == szText.IsEmpty() )
    //{
    //  SetLimitText(nMax_Chars);
    //}
    
    if( m_wParseStyle == PES_ALL )
    {
        CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
    }
    else
    {
        if( m_wParseStyle == PES_NUM_UNSIGNEDINT ) 
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 )
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        else if( m_wParseStyle == PES_NUM_INT )
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 || nChar == 0x2D )
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        // stevev 3oct07
        else if( m_wParseStyle == PES_NUM_HEX )
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x08 || 
                ( nChar >= 0x41 && nChar <= 0x46 ) || ( nChar >= 0x61 && nChar <= 0x66 )
                || nChar == 0x58 ||  nChar == 0x78  )
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        else if ( m_wParseStyle == PES_NUM_FLOAT)
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x2E ||  
                    nChar == 0x08 || nChar == 0x2D || nChar == 0x2D ||
                    nChar == 0x2B || nChar == 0x45 || nChar == 0x65  )// added eE+ 28nov11
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        else if ( m_wParseStyle == PES_DATE) /*Added by ANOOP */
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x2F || nChar == 0x08 )
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        else if ( m_wParseStyle == PES_TIME_FMT) // Added TIME support, POB - 15 Aug 2008
        {
            if( ( nChar >= 0x30 && nChar <= 0x39 ) || nChar == 0x3A /* : */ || 
                  nChar == 0x41 /* A */ || nChar == 0x50 /* P */ || nChar == 0x4D /* M */ ||
                  nChar == 0x08 /* BS */|| 
                  nChar == 0x61 /* a */ || nChar == 0x70 /* p */ || nChar == 0x6D /* m */ ||
                  nChar == 0x20 /*(space)*/ )
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }
        else if ( m_wParseStyle == PES_24TIME_FMT) /*Added by stevev - 24 hr format */
        {
            if( (nChar >= 0x30 && nChar <= 0x39) || nChar == 0x3A /*:*/|| nChar == 0x08 /*BS*/)
            {
                CEdit::OnChar(nChar, nRepCnt, nFlags);  // permitted
            }
            else
                makeNoise = true;
        }       
    }
    if (makeNoise)
        //CEdit::OnChar(0x07, 1, nFlags);  
        //Beep(750,300);
        MessageBeep(0);


    GetWindowText(szText);
    szDisp_val_new=szText;  

    //if(m_wParseStyle == PES_NUM_FLOAT)
    if (m_wParseStyle != PES_UNDEFINED && m_wParseStyle != PES_ALL)
    {// all numeric types
        if(FALSE == szText.IsEmpty() && szText.Find(_T('.')) >= 0)
        {// floats are the only ones with DPs allowed
            int nRight_dec = decimalsRight(szText); // Matches paredit.cpp, POB - 15 Aug 2008

            if ( ( nRight_dec > nDecimal_Limit) && nDecimal_Limit > 0 ) // -1 is unlimited
            {
                SetWindowText(szDisp_val_old);// put it back
                // Don't allow cursor to go to the first character position,
                //if the values decimal position count equal to the display string 
                //decimal portion value 
                UINT tmpLen = szDisp_val_old.GetLength();
                this->SetSel(tmpLen,tmpLen,FALSE);
                CEdit::OnChar(0x07, 1, nFlags); // try to beep          
            }
            // else decimals OK, box should control total length
        }
        // else nothing to the right of the decimal point
    }
    // else we don't limit

    /*  this has no possible use...
      The first half is redundant
      The second part is wrong...we don't shorten their field just because they put in
      too many decimal places.  They may want to go and make it negative or add a digit
      to the left of the DP.  Shortening the field is just rude.
    if ( m_wParseStyle == PES_ALL )
    {
        if(FALSE == szText.IsEmpty())
        {
            SetLimitText(nMax_Chars);
        }
    }
    else
    {
        if (  (IsCharAlphaNumeric((TCHAR)nChar) && !IsCharAlpha((TCHAR)nChar)) ) // Matches paredit.cpp, POB - 15 Aug 2008
        {// it's numeric
            if(FALSE == szText.IsEmpty() && szText.Find(_T('.')) >= 0)
            {// has a dec pt already
                int len = decimalsRight(szText);
                    
                GetSel(nFirstPos,nLastPos);// get location of currently selected text
                // stevev24jun08 - do not limit at zero decimal places (that's the default)
                if ( ( len == nDecimal_Limit && nDecimal_Limit > 0)  && ( szText.GetLength() < nMax_Chars ))
                {// limit any more text input
                    SetLimitText(szText.GetLength());
                }
            }
        }   
    }
    ***/
}

BOOL CColorEdit::SubclassEdit(UINT nID, CWnd* pParent, WORD wParseStyle,
                              int nMax_Chrs,int nDecimal_Lt,int nChar_cnt)
{
    setFormatInfo(wParseStyle, nMax_Chrs, nDecimal_Lt, nChar_cnt);
    return SubclassDlgItem(nID, pParent);
}

void CColorEdit::setFormatInfo(int style, int max_Chrs,int decimal_Lt,int char_cnt)
{
    nMax_Chars      = max_Chrs;
    nDecimal_Limit  = decimal_Lt;
    nLeft_of_decimal= char_cnt;
    m_wParseStyle   = style;
    
    SetLimitText(nMax_Chars);
}
// This funtion allows for both the enabling and disabling of key strokes by the user.
// When a function is disabled, it is still allowed to receive, EN_SETFOCUS and EN_KILLFOCUS, POB - 28 Aug 2008
void CColorEdit::Enable(BOOL bEnable)
{
    m_isEnabled = bEnable;  
}

// This funtion gives information on whether the control is enabled or disabled, POB - 28 Aug 2008
BOOL CColorEdit::IsEnabled()
{
    return m_isEnabled;
}
