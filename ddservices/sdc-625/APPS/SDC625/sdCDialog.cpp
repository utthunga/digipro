/*************************************************************************************************
 *
 * $Workfile: sdcDialog.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		sdcDialog.cpp
 *
 *		
 * 
 */

#include "stdafx.h"
#include "SDC625.h"
#include "sdcDialog.h"
#include "MainFrm.h"
#include "LabelDialog.h"

#ifdef _DEBUG
#include "logging.h"
#endif

extern void systemFlush(void);


BOOL sdCDialog::OnIdle(LONG lCount)
{
TRACE(_T("OnIdle-sdCDialog level\n"));
return FALSE;
}

int sdCDialog::DoModal()
{ 
#ifndef NOTDEFD
		// can be constructed with a resource template or InitModalIndirect
	ASSERT(m_lpszTemplateName != NULL || m_hDialogTemplate != NULL ||
		m_lpDialogTemplate != NULL);

	// load resource as necessary
	LPCDLGTEMPLATE lpDialogTemplate = m_lpDialogTemplate;
	HGLOBAL hDialogTemplate = m_hDialogTemplate;
	HINSTANCE hInst = AfxGetResourceHandle();
	if (m_lpszTemplateName != NULL)
	{
		hInst = AfxFindResourceHandle(m_lpszTemplateName, RT_DIALOG);
		HRSRC hResource = ::FindResource(hInst, m_lpszTemplateName, RT_DIALOG);
		hDialogTemplate = LoadResource(hInst, hResource);
	}
	if (hDialogTemplate != NULL)
		lpDialogTemplate = (LPCDLGTEMPLATE)LockResource(hDialogTemplate);

	// return -1 in case of failure to load the dialog template resource
	if (lpDialogTemplate == NULL)
		return -1;

	// disable parent (before creating dialog)
	//HWND hWndParent = PreModal();
	h_sdc_WndParent = PreModal();
	AfxUnhookWindowCreate();
	BOOL bEnableParent = FALSE;
/**** from 
	if (hWndParent != NULL && ::IsWindowEnabled(hWndParent))
	{
		::EnableWindow(hWndParent, FALSE);
		bEnableParent = TRUE;
	}
***** to **/
	if (h_sdc_WndParent != NULL)
	{ 
		if (::IsWindowEnabled(h_sdc_WndParent))
		{ 
			bEnableParent = TRUE;
		}
//		::EnableWindow(hWndParent, FALSE);// call reguardless so we can manage a modal stack
		::SendMessage(h_sdc_WndParent,WM_ENABLE,FALSE,3);
	}
/**** end change ***/
	TRY
	{
		TRACE(_T(">> sdCDialog::DoModal CreatingDlgIndirect.(0x%x)\n"),GetCurrentThreadId());
		// create modeless dialog
		AfxHookWindowCreate(this);
		if (CreateDlgIndirect(lpDialogTemplate,
						CWnd::FromHandle(h_sdc_WndParent), hInst))
		{
			if (m_nFlags & WF_CONTINUEMODAL)
			{
				// enter modal loop
				DWORD dwFlags = MLF_SHOWONIDLE;
				if (GetStyle() & DS_NOIDLEMSG)
					dwFlags |= MLF_NOIDLEMSG;
				TRACE(_T(">> sdCDialog::DoModal entering modal loop.(0x%x)\n"),GetCurrentThreadId());
				VERIFY(sdcRunModalLoop(dwFlags) == m_nModalResult);
			}

			TRACE(_T(">> sdCDialog::DoModal out of modal loop,exiting.(0x%x)\n"),GetCurrentThreadId());
			// hide the window before enabling the parent, etc.
			if (m_hWnd != NULL)
				SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW|
					SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
		}
	}
	CATCH_ALL(e)
	{
		do { e->Delete(); } while (0);	// same as DELETE_EXCEPTION(e);
		TRACE(_T("**** sdCDialog::DoModal EXCEPTION EXIT.(0x%x)\n"),GetCurrentThreadId());
		m_nModalResult = -1;
	}
	END_CATCH_ALL

	TRACE(_T("**** sdCDialog::DoModal pre critical section.(0x%x)\n"),GetCurrentThreadId());
EnterCriticalSection( &lcsDialog );
	TRACE(_T("**** sdCDialog::DoModal critical section 001.(0x%x)\n"),GetCurrentThreadId());
	if (bEnableParent)
		::EnableWindow(h_sdc_WndParent, TRUE);	
	TRACE(_T("**** sdCDialog::DoModal critical section 002.(0x%x)\n"),GetCurrentThreadId());
// stevev 29jun07 - the following never returns in some re-entran-menu scenarios
//				  - the cause is unknown. - For now, let the OS figure out the active window.
//temp	if (h_sdc_WndParent != NULL && ::GetActiveWindow() == m_hWnd)
//temp		::SetActiveWindow(h_sdc_WndParent);
	TRACE(_T("**** sdCDialog::DoModal critical section 003.(0x%x)\n"),GetCurrentThreadId());

	if (::IsWindow(m_hWnd) )
	{	// destroy modal window
		TRACE(_T("**** sdCDialog::DoModal critical section 004.(0x%x)\n"),GetCurrentThreadId());
		DestroyWindow();
	}
	TRACE(_T("**** sdCDialog::DoModal critical section 005.(0x%x)\n"),GetCurrentThreadId());
	PostModal();
	TRACE(_T("**** sdCDialog::DoModal critical section 006.(0x%x)\n"),GetCurrentThreadId());

	// unlock/free resources as necessary
	if (m_lpszTemplateName != NULL || m_hDialogTemplate != NULL)
		UnlockResource(hDialogTemplate);
	TRACE(_T("**** sdCDialog::DoModal critical section 007.(0x%x)\n"),GetCurrentThreadId());
	if (m_lpszTemplateName != NULL)
		FreeResource(hDialogTemplate);
	TRACE(_T("**** sdCDialog::DoModal critical section 008.(0x%x)\n"),GetCurrentThreadId());

LeaveCriticalSection( &lcsDialog );

	TRACE(_T("**** sdCDialog::DoModal returning post critical section.(0x%x)\n"),GetCurrentThreadId());
	return m_nModalResult;
#else
	int r = 0;
	r = sdcModl();
	return  r;
#endif
}

int sdCDialog::sdcModl()
{
	// can be constructed with a resource template or InitModalIndirect
	ASSERT(m_lpszTemplateName != NULL || m_hDialogTemplate != NULL ||
		m_lpDialogTemplate != NULL);

	// load resource as necessary
	LPCDLGTEMPLATE lpDialogTemplate = m_lpDialogTemplate;
	HGLOBAL hDialogTemplate = m_hDialogTemplate;
	HINSTANCE hInst = AfxGetResourceHandle();
	if (m_lpszTemplateName != NULL)
	{
		hInst = AfxFindResourceHandle(m_lpszTemplateName, RT_DIALOG);
		HRSRC hResource = ::FindResource(hInst, m_lpszTemplateName, RT_DIALOG);
		hDialogTemplate = LoadResource(hInst, hResource);
	}
	if (hDialogTemplate != NULL)
		lpDialogTemplate = (LPCDLGTEMPLATE)LockResource(hDialogTemplate);

	// return -1 in case of failure to load the dialog template resource
	if (lpDialogTemplate == NULL)
		return -1;

	// disable parent (before creating dialog)
	//HWND hWndParent = PreModal();
	h_sdc_WndParent = PreModal();
	AfxUnhookWindowCreate();
	BOOL bEnableParent = FALSE;
/**** from 
	if (hWndParent != NULL && ::IsWindowEnabled(hWndParent))
	{
		::EnableWindow(hWndParent, FALSE);
		bEnableParent = TRUE;
	}
***** to **/
	if (h_sdc_WndParent != NULL)
	{ 
		if (::IsWindowEnabled(h_sdc_WndParent))
		{ 
			bEnableParent = TRUE;
		}
//		::EnableWindow(hWndParent, FALSE);// call reguardless so we can manage a modal stack
		::SendMessage(h_sdc_WndParent,WM_ENABLE,FALSE,3);
	}
/**** end change ***/
	TRY
	{
		// create modeless dialog
		AfxHookWindowCreate(this);
		if (CreateDlgIndirect(lpDialogTemplate,
						CWnd::FromHandle(h_sdc_WndParent), hInst))
		{
			if (m_nFlags & WF_CONTINUEMODAL)
			{
				// enter modal loop
				DWORD dwFlags = MLF_SHOWONIDLE;
				if (GetStyle() & DS_NOIDLEMSG)
					dwFlags |= MLF_NOIDLEMSG;

				VERIFY(sdcRunModalLoop(dwFlags) == m_nModalResult);
			}

			// hide the window before enabling the parent, etc.
			if (m_hWnd != NULL)
				SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW|
					SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
		}
	}
	CATCH_ALL(e)
	{
		do { e->Delete(); } while (0);	// same as DELETE_EXCEPTION(e);
		m_nModalResult = -1;
	}
	END_CATCH_ALL

EnterCriticalSection( &lcsDialog );
	if (bEnableParent)
		::EnableWindow(h_sdc_WndParent, TRUE);
	if (h_sdc_WndParent != NULL && ::GetActiveWindow() == m_hWnd)
		::SetActiveWindow(h_sdc_WndParent);

	if (::IsWindow(m_hWnd) )
		// destroy modal window
		DestroyWindow();
	PostModal();

	// unlock/free resources as necessary
	if (m_lpszTemplateName != NULL || m_hDialogTemplate != NULL)
		UnlockResource(hDialogTemplate);
	if (m_lpszTemplateName != NULL)
		FreeResource(hDialogTemplate);

LeaveCriticalSection( &lcsDialog );

	return m_nModalResult;
}

int enablePumpLog = 0;

int sdCDialog::sdcRunModalLoop(DWORD dwFlags)
{
	ASSERT(::IsWindow(m_hWnd)); // window must be created
	ASSERT(!(m_nFlags & WF_MODALLOOP)); // window must not already be in modal state

	// for tracking the idle time state
	BOOL bIdle = TRUE;
	LONG lIdleCount = 0;
	BOOL bShowIdle = (dwFlags & MLF_SHOWONIDLE) && !(GetStyle() & WS_VISIBLE);
	HWND hWndParent = ::GetParent(m_hWnd);
/// new stevev 18oct05
	if ( hWndParent == NULL )
	{
		hWndParent = h_sdc_WndParent;
	}
	m_nFlags |= (WF_MODALLOOP|WF_CONTINUEMODAL);

#ifdef _WIN32_WCE  // PAW check this 05/05/09 - 
//note that vs2005 reports m_msgCur as NOT a member of CWinThread from AfxGetThread()
	MSG* pMsg = &AfxGetThread()->m_msgCur; 
#else
	MSG* pMsg = &AfxGetThreadState()->m_msgCur;	// PAW 03/03/09 see above 
#endif

	// acquire and dispatch messages until the modal state is done
	for (;;)
	{
		ASSERT(ContinueModal());

		// phase1: check to see if we can do idle work
		while (bIdle &&
			!::PeekMessage(pMsg, NULL, NULL, NULL, PM_NOREMOVE))
		{
			ASSERT(ContinueModal());

			// show the dialog when the message queue goes idle
			if (bShowIdle)
			{
				ShowWindow(SW_SHOWNORMAL);
				UpdateWindow();
				bShowIdle = FALSE;
			}

			// call OnIdle while in bIdle state
			if (!(dwFlags & MLF_NOIDLEMSG) && hWndParent != NULL && lIdleCount == 0)
			{
				// send WM_ENTERIDLE to the parent
				::SendMessage(hWndParent, WM_ENTERIDLE, MSGF_DIALOGBOX, (LPARAM)m_hWnd);
				Sleep(0);
			}
			if ((dwFlags & MLF_NOKICKIDLE) ||
				!SendMessage(WM_KICKIDLE, MSGF_DIALOGBOX, lIdleCount++))
			{
				// stop idle processing next time
				bIdle = FALSE;
			}
		}

		// phase2: pump messages while available
		do
		{
			ASSERT(ContinueModal());
#ifdef _DEBUG
		if ( pMsg->message == WM_ENABLE )//0x0a
		{
			WPARAM lowParam = pMsg->wParam;// FALSE is off
			LPARAM lolParam = pMsg->lParam;
		}
		if (enablePumpLog)
		LOGIT(CLOG_LOG,"Pumping: hWnd= %08x Msg: %04x Wparam: %02x Lparam: %04x\n",
						(unsigned)m_hWnd,pMsg->message,pMsg->wParam,pMsg->lParam);
#endif
if ( IsWindow(m_hWnd) )
{
			// pump message, but quit on WM_QUIT
			if (!AfxGetThread()->PumpMessage())
			{
				AfxPostQuitMessage(0);
				return -1;
			}
}
#ifdef _DEBUG
else
{
	LOGIT(CLOG_LOG,"sdCDialog::sdcRunModalLoop asked to pump a message without a valid window.\n");
}
#endif
			// show the window when certain special messages rec'd
			if (bShowIdle &&
				(pMsg->message == 0x118 || pMsg->message == WM_SYSKEYDOWN))
			{
				ShowWindow(SW_SHOWNORMAL);
				UpdateWindow();
				bShowIdle = FALSE;
			}

			if (!ContinueModal())
				goto ExitModal;

			// reset "no idle" state after pumping "normal" message
			if (AfxGetThread()->IsIdleMessage(pMsg))
			{
				bIdle = TRUE;
				lIdleCount = 0;
			}
		} while (::PeekMessage(pMsg, NULL, NULL, NULL, PM_NOREMOVE));
	}

ExitModal:
	m_nFlags &= ~(WF_MODALLOOP|WF_CONTINUEMODAL);
	return m_nModalResult;
}


void sdCDialog::OnCancel() 
{	// this is where EndDialog() is called...

	ASSERT(::IsWindow(m_hWnd));

	if (m_nFlags & (WF_MODALLOOP|WF_CONTINUEMODAL))
	{
//try: 23jun06
//		EndModalLoop(IDCANCEL);
		sdcEndDialog(IDCANCEL);
	}

//send the enable message to main frme!!!!!!!!!!!!!
//may not be required	m_pParentWnd
// apparently endDialog does this already...	::SendMessage(h_sdc_WndParent,WM_ENABLE,TRUE,0);
}


void sdCDialog::sdcEndDialog(int nResult)
{
	ASSERT(::IsWindow(m_hWnd));

	if (m_nFlags & (WF_MODALLOOP|WF_CONTINUEMODAL))
	{//mfc does a::	EndModalLoop(IDCANCEL);
	 // we do it inline
		
		// in mfc...ASSERT(::IsWindow(m_hWnd));

		// this result will be returned from CWnd::RunModalLoop
		m_nModalResult = nResult;
		
		// make sure a message goes through to exit the modal loop
		if (m_nFlags & WF_CONTINUEMODAL)
		{
			m_nFlags &= ~WF_CONTINUEMODAL;
			// in mfc::  PostMessage(WM_NULL);
			::PostMessage(m_hWnd,WM_NULL,0,0);
			TRACE(_T("sdCDialog::sdcEndDialog post NULL msg to hWnd %x in 0x%x\n"),m_hWnd,GetCurrentThreadId());
		}
		// end inline
	}

// as per mfc...
	TRACE(_T("sdCDialog::sdcEndDialog Sending Enable to window %x in 0x%x\n"),h_sdc_WndParent,GetCurrentThreadId());
	::SendMessage(h_sdc_WndParent,WM_ENABLE,TRUE,1);
	TRACE(_T("sdCDialog::sdcEndDialog NOT doing EndDialog in 0x%x\n"),GetCurrentThreadId());
//	::EndDialog(m_hWnd, nResult);
	
	// there is log evidence that EndDialog gets messages dealt with before returning.
	// systemFlush - may not be handling 'em correctly since the logs showed Updates and other executions
	// while this does not
	systemFlush();

}


void sdCDialog::OnOK()
{
	if (!UpdateData(TRUE))
	{
		TRACE(_T("UpdateData failed during dialog termination.\n"));
		// the UpdateData routine will set focus to correct item
		return;
	}
	sdcEndDialog(IDOK);
}

/* code moved 11/27/13 POB */


