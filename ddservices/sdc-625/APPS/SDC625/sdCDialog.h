/*************************************************************************************************
 *
 * $Workfile: sdcDialog.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		common overload of bills DoModal()
 *
 *
 *		
 * #include "sdcDialog.h"
 */

#ifndef _SDCDIALOG_H
#define _SDCDIALOG_H


#include <afxpriv.h>

#define MAX_HELPSTRING_LENGTH  80

/* evrything else is the parent's */
class sdCDialog : public CDialog 
{
protected:
	HWND h_sdc_WndParent;

	CRITICAL_SECTION  lcsDialog;  /* critical section for dialog deletion */

public:
	sdCDialog() { CDialog(); InitializeCriticalSection(&lcsDialog);} ;
	virtual 
		~sdCDialog() {DeleteCriticalSection( &lcsDialog );};

	
BOOL OnIdle(LONG lCount);
int  sdcRunModalLoop(DWORD dwFlags);
void OnCancel();

	
	sdCDialog(LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL) : CDialog(lpszTemplateName,pParentWnd)
																{InitializeCriticalSection(&lcsDialog);};
	sdCDialog(UINT nIDTemplate, CWnd* pParentWnd = NULL) : CDialog(nIDTemplate,pParentWnd)
																{InitializeCriticalSection(&lcsDialog);};

	int DoModal();// code still available after Do Modal overridden
	int sdcModl();

	void sdcEndDialog(int nResult);
	void EndDialog(int nResult){ sdcEndDialog(nResult);};

	void OnOK();
};




/******************

void CDialog::EndDialog(int nResult)
{
	ASSERT(::IsWindow(m_hWnd));

	if (m_nFlags & (WF_MODALLOOP|WF_CONTINUEMODAL))
		EndModalLoop(nResult);

	::EndDialog(m_hWnd, nResult);
}

void CWnd::EndModalLoop(int nResult)
{
	ASSERT(::IsWindow(m_hWnd));

	// this result will be returned from CWnd::RunModalLoop
	m_nModalResult = nResult;

	// make sure a message goes through to exit the modal loop
	if (m_nFlags & WF_CONTINUEMODAL)
	{
		m_nFlags &= ~WF_CONTINUEMODAL;
		PostMessage(WM_NULL);
	}
}
******************/

/* code moved 11/27/13 POB */
#endif // _SDCDIALOG_H