/*********************************************************************************************
 *
 * $Workfile: DDIdeListView.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *********************************************************************************************
 *
 * Description:
 *      interface of the CDDIdeListView class
 *
 * 22mar11 - removed intermediate class 'CSdcView' & consolodated into CDDIdeListView
 *
 * #include "DDIdeListView.h".      
 */

#if !defined(AFX_DDIDELISTVIEW_H__4D3CFD69_5042_424C_84D7_DDB79BFD51AA__INCLUDED_)
#define AFX_DDIDELISTVIEW_H__4D3CFD69_5042_424C_84D7_DDB79BFD51AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "StdAfx.h"
/***************************************************************************
 * Global Definitions
 ***************************************************************************/

#define MAX_LIST_COLUMNS      4

//prashant oct 2003
#define MAX_HELPSTRING_LENGTH  80

class CDDIdeDoc;
class CMainListCtrl;
class CDDLMenu;

//22mar11::>#include "SdcView.h"

#include "logging.h"

//22mar11::>class CSdcView;
/////////////////////////////////////////////////////////////////////////////
// CDDIdeListView view

class CDDIdeListView : public CView//22mar11::>CSdcView
{
    CDDLMenu* m_pListedMenu;// what's on the screen...usually matches tableview's selection

protected:
    CDDIdeListView();           // protected constructor used by dynamic creation
    DECLARE_DYNCREATE(CDDIdeListView)

    CMainListCtrl* m_pCtrl;
    bool ctrlInitialized;

// Attributes
public: 
#define P_TREEVIEW    (m_pTableFrame->m_pTreeView)
    CTableChildFrame* m_pTableFrame;// holds the ptr for the listview

// Operations
public:
    CDDIdeDoc* GetDocument();
    void BuildListFrom( CDDLBase* pBase);
    int  findBySymbolNumber(ITEM_ID symNum);
    int  remove(ITEM_ID symbolID);// table has to be able to remove an item


    CMainListCtrl* GetListCtrl(){return m_pCtrl;};

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDDIdeListView)
public:
    virtual void OnInitialUpdate();
    virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
protected:
    virtual void OnDraw(CDC* pDC);      // overridden to draw this view
    //}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam );
#endif
// Implementation
protected:
    virtual ~CDDIdeListView();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
#endif

protected:
    void GetProfileSettings();
    void WriteProfileSettings();
    void GetColumnWidthString( CString *pstrColWidths );
    void GetMainWindowSizeString( CString *pstrWndSize );
    CImageList m_ctlImage;
    int m_anColWidth[MAX_LIST_COLUMNS];
    // now using ptr in parent  CMainListCtrl *m_pList;

    // Generated message map functions
protected:
    //{{AFX_MSG(CDDIdeListView)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);//22mar11
    //Deadcode - removed afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult); POB - 4/13/2014
    //Deadcode - removed afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult); POB - 4/14/2014
    afx_msg void OnMenuHelp();
    afx_msg void OnMenuLabel();
    afx_msg void OnMenuDisplayValue();
    afx_msg void OnMenuExecute();
    afx_msg void OnUpdateMenuExecute(CCmdUI* pCmdUI);
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
    afx_msg void OnDeviceDetails();
    afx_msg void OnUpdateDeviceDetails(CCmdUI* pCmdUI);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
/* moved to mainfrm */  
//  afx_msg void OnViewMorestatus();
//  afx_msg void OnUpdateViewMorestatus(CCmdUI* pCmdUI);
};

#ifndef _DEBUG  // debug version in DDIdeView.cpp
inline CDDIdeDoc* CDDIdeListView::GetDocument()
{ return reinterpret_cast<CDDIdeDoc*>(m_pDocument); }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDIDELISTVIEW_H__4D3CFD69_5042_424C_84D7_DDB79BFD51AA__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDIdeListView.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
