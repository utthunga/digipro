/*************************************************************************************************
 *
 * $Workfile: DDLEDisplay.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface for the CDDLEDisplay class.
 * #include "DDLEDisplay.h".		
 */

#if !defined(AFX_DDLEDISPLAY_H__D17D5BED_4ABB_4BEA_9DA1_68FC9AC0A8F9__INCLUDED_)
#define AFX_DDLEDISPLAY_H__D17D5BED_4ABB_4BEA_9DA1_68FC9AC0A8F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLVariable.h"//#include "DDLBase.h"

#include "ddbEditDisplay.h"
#include "DDLMenu.h"

class CDDLEDisplay : public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLEDisplay)

public:

	CDDLEDisplay(hCitemBase* pIB=NULL);
	CDDLEDisplay(itemInfo_t& pIi);
	// commented out stevev 01dec05--CDDLEDisplay(hCeditDisplay* pEditDisplay);
	virtual ~CDDLEDisplay();

// Special menu handling
	void Execute();
    void LoadContextMenu(CMenu *pMenu, int& nPos);
	hCeditDisplay* baseItemPtr(void) ;
	/*Vibhor 170304: Start of Code*/
	BOOL IsEditDisplay();
	BOOL IsChanged();
	/*Vibhor 170304: End of Code*/

// Access Functions

private:
		hCeditDisplay* m_pEditDispPtr;

};

#endif // !defined(AFX_DDLEDISPLAY_H__D17D5BED_4ABB_4BEA_9DA1_68FC9AC0A8F9__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLEDisplay.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
