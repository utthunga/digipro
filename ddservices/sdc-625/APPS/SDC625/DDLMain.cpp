/*************************************************************************************************
 *
 * $Workfile: DDLMain.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDLMain.cpp: implementation of the CDDLMain class.
 */
#pragma warning (disable : 4786)
#include "stdafx.h"
#include "SDC625.h"
#include "DDLMain.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************************************************************
 * Private Function Prototypes for this file
 ***************************************************************************/

/* class CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */

//IMPLEMENT_DYNAMIC( CDDLMain, CDDLBase)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*CDDLMain::CDDLMain(hCitemBase* pIB):CDDLBase(pIB)
{

}

CDDLMain::~CDDLMain()
{

}*/
//////////////////////////////////////////////////////////////////////
// Overrides
//////////////////////////////////////////////////////////////////////

/*void CDDLMain::Execute()
{


}

void CDDLMain::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_MAIN;    
}*/



/*************************************************************************************************
 *
 *   $History: DDLMain.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
