#if !defined(AFX_WAVEEDITDLG_H__525482C4_2312_4484_A7CE_3B1A1A7A58C0__INCLUDED_)
#define AFX_WAVEEDITDLG_H__525482C4_2312_4484_A7CE_3B1A1A7A58C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaveEditDlg.h : header file
//

#include "ddbdefs.h"
#include "graphicInfc.h"
#include "ddbreferenceNexpression.h"


#include "GridCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CWaveEditDlg dialog

class CWaveEditDlg : public CDialog
{
// Construction
public:
	CWaveEditDlg(DevInfcHandle_t h,waveformType_t waveType,pointValueList_t & wvPointList
				 ,hCreferenceList *pXRefs = NULL
				 ,hCreferenceList *pYRefs = NULL
				 ,CWnd* pParent =NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWaveEditDlg)
	enum { IDD = IDD_WAVE_EDIT };
	CString	m_WaveTypeName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaveEditDlg)
	public:
	virtual int DoModal();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CGridCtrl m_GridCtrl; //The FlexGrid control object

	waveformType_t m_WaveType;

	pointValueList_t *m_pWavePts;

	hCreferenceList *m_pXRefs;

	hCreferenceList *m_pYRefs;

	BOOL m_bWaveEdited;

	// Generated message map functions
	//{{AFX_MSG(CWaveEditDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAVEEDITDLG_H__525482C4_2312_4484_A7CE_3B1A1A7A58C0__INCLUDED_)
