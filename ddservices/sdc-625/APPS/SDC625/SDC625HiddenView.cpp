// SDC625HiddenView.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "SDC625HiddenView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDC625HiddenView

IMPLEMENT_DYNCREATE(CSDC625HiddenView, CView)

CSDC625HiddenView::CSDC625HiddenView()
{
}

CSDC625HiddenView::~CSDC625HiddenView()
{
}


BEGIN_MESSAGE_MAP(CSDC625HiddenView, CView)
	//{{AFX_MSG_MAP(CSDC625HiddenView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSDC625HiddenView drawing

void CSDC625HiddenView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CSDC625HiddenView diagnostics

#ifdef _DEBUG
void CSDC625HiddenView::AssertValid() const
{
	CView::AssertValid();
}

void CSDC625HiddenView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSDC625HiddenView message handlers
