// WaveEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "ddbItemBase.h"
#include "ddbVar.h"
#include "WaveEditDlg.h"
#include "stdafx.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWaveEditDlg dialog


CWaveEditDlg::CWaveEditDlg( DevInfcHandle_t h,waveformType_t waveType,pointValueList_t & wvPointList
						   ,hCreferenceList *pXRefs /*NULL*/
						   ,hCreferenceList *pYRefs /*NULL*/
						   ,CWnd* pParent /*=NULL*/)
	: CDialog(CWaveEditDlg::IDD, pParent)
{
	m_WaveType = waveType;
	m_pXRefs = pXRefs;
	m_pYRefs = pYRefs;
	m_pWavePts = &wvPointList;
	m_bWaveEdited = FALSE;

	//{{AFX_DATA_INIT(CWaveEditDlg)
	m_WaveTypeName = _T(" ");
		
	//}}AFX_DATA_INIT
}


void CWaveEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWaveEditDlg)
	DDX_Text(pDX, IDC_STATIC_WAVETYPE, m_WaveTypeName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWaveEditDlg, CDialog)
	//{{AFX_MSG_MAP(CWaveEditDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWaveEditDlg message handlers

BOOL CWaveEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	this->SetWindowText(_T("Wave Edit Dialog"));

	//referenceList::iterator itX;
	//referenceList::iterator itY;
	pointValue_t tmpPoint;

	//int iPtIndex;

	CRect rect;
	GetClientRect(rect);

	RECT rect1;

	rect1.top = rect.top + 40;
	rect1.bottom = rect.bottom -40;
	rect1.left = rect.left + 30;
	rect1.right = rect.right- 35;

	
	m_GridCtrl.Create(rect1, this, 147893);

		// fill it up with stuff
	m_GridCtrl.ShowWindow (SW_SHOW);
	m_GridCtrl.SetEditable(TRUE);
	m_GridCtrl.EnableDragAndDrop(TRUE);
	m_GridCtrl.SetRowCount(m_pWavePts->size()+1);
	m_GridCtrl.SetFixedRowCount(1);
	m_GridCtrl.SetFixedColumnCount();


	
	switch(m_WaveType)
		{
			case wT_Y_Time:
			case wT_Horiz:
				{
					if(!m_pYRefs)
						return FALSE;
					if(m_WaveType == wT_Y_Time)
					{
						m_WaveTypeName = _T("Y_T");	
					}
					else
					{
						m_WaveTypeName = _T("Horizontal");
					}

					
					m_GridCtrl.SetColumnCount(2);
				
					for (int row = 0; row < m_GridCtrl.GetRowCount(); row++)
						for (int col = 0; col < m_GridCtrl.GetColumnCount(); col++)
						{ 
							GV_ITEM Item;
							Item.mask = GVIF_TEXT|GVIF_FORMAT;
							Item.row = row;
							Item.col = col;
							if (row == 0 && col == 1)  {
								Item.nFormat = DT_CENTER|DT_WORDBREAK;
								Item.strText.Format(_T("Y"),col);
							}
							if (col == 0 && row > 0) {
								Item.nFormat = DT_RIGHT|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								Item.strText.Format(_T("%d"),row);
							} 
							if(row > 0 && col > 0){
								Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								tmpPoint = m_pWavePts->at(row -1);
								Item.strText.Format(_T("%f"),(float)tmpPoint.vy);
								if(m_pYRefs->at(row-1).getITYPE() != iT_Variable)
								{
									BOOL bRet =	m_GridCtrl.SetItemBkColour(row,col,RGB(200,200,200));
									m_GridCtrl.SetItemState(row,col,GVIS_READONLY);

								}
							}
							m_GridCtrl.SetItem(&Item);
							
						}

									
				}
				break;
			case wT_Y_X:
				{
					if(!m_pXRefs)
						return FALSE;
					if(!m_pYRefs)
						return FALSE;
					m_WaveTypeName = _T("X_Y");	

					m_GridCtrl.SetColumnCount(3);

					for (int row = 0; row < m_GridCtrl.GetRowCount(); row++)
						for (int col = 0; col < m_GridCtrl.GetColumnCount(); col++)
						{ 
							GV_ITEM Item;
							Item.mask = GVIF_TEXT|GVIF_FORMAT;
							Item.row = row;
							Item.col = col;
							if (row == 0 && col == 1)  {
								Item.nFormat = DT_CENTER|DT_WORDBREAK;
								Item.strText.Format(_T("X"),col);
							}
							if (row == 0 && col == 2)  {
								Item.nFormat = DT_CENTER|DT_WORDBREAK;
								Item.strText.Format(_T("Y"),col);
							}
							if (col == 0 && row > 0) {
								Item.nFormat = DT_RIGHT|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								Item.strText.Format(_T("%d"),row);
							} 
							if(row > 0)
							{
								Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								tmpPoint = m_pWavePts->at(row -1);
								if(col == 1)
								{
									Item.strText.Format(_T("%f"),(float)tmpPoint.vx);
									if(m_pXRefs->at(row-1).getITYPE() != iT_Variable)
									{
										BOOL bRet =	m_GridCtrl.SetItemBkColour(row,col,RGB(200,200,200));
										m_GridCtrl.SetItemState(row,col,GVIS_READONLY);
									}
								}
								else if(col ==2)
								{
									Item.strText.Format(_T("%f"),(float)tmpPoint.vy);
									if(m_pYRefs->at(row-1).getITYPE() != iT_Variable)
									{
										BOOL bRet =	m_GridCtrl.SetItemBkColour(row,col,RGB(200,200,200));
										m_GridCtrl.SetItemState(row,col,GVIS_READONLY);
									}
								}

							}//endif row > 0
								
							m_GridCtrl.SetItem(&Item);
							
						}//endfor

				}
				break;
			case wT_Vert:
				{
					if(!m_pXRefs)
						return FALSE;
					m_WaveTypeName = _T("Vertical");

					m_GridCtrl.SetColumnCount(2);
				
					for (int row = 0; row < m_GridCtrl.GetRowCount(); row++)
						for (int col = 0; col < m_GridCtrl.GetColumnCount(); col++)
						{ 
							GV_ITEM Item;
							Item.mask = GVIF_TEXT|GVIF_FORMAT;
							Item.row = row;
							Item.col = col;
							if (row == 0 && col == 1)  {
								Item.nFormat = DT_CENTER|DT_WORDBREAK;
								Item.strText.Format(_T("X"),col);
							}
							if (col == 0 && row > 0) {
								Item.nFormat = DT_RIGHT|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								Item.strText.Format(_T("%d"),row);
							} 
							if(row > 0 && col > 0){
								Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
								tmpPoint = m_pWavePts->at(row -1);
								Item.strText.Format(_T("%f"),(float)tmpPoint.vx);
								if(m_pXRefs->at(row-1).getITYPE() != iT_Variable)
								{
									BOOL bRet =	m_GridCtrl.SetItemBkColour(row,col,RGB(200,200,200));
									m_GridCtrl.SetItemState(row,col,GVIS_READONLY);

								}
							}
							m_GridCtrl.SetItem(&Item);
							
						}

					
				}
				break;
			case wT_Undefined:
			case wT_Unknown:
			default:
				{
					m_WaveTypeName = _T("Unknown");
				}
				break;
		}

	

	this->UpdateData(FALSE);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWaveEditDlg::OnOK() 
{
	// TODO: Add extra validation here
	hCitemBase* pI = NULL;

	pointValue_t tmpPoint;
	
	CValueVarient value;

	RETURNCODE rc = SUCCESS;
	hCVar *pVar = NULL;


	for (int row = 1; row < m_GridCtrl.GetRowCount(); row++)
		for (int col = 1; col < m_GridCtrl.GetColumnCount(); col++)
		{ 
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.row = row;
			Item.col = col;
			m_GridCtrl.GetItem(&Item);
			if(((m_WaveType == wT_Y_Time || m_WaveType == wT_Horiz) && col == 1)
				||(m_WaveType == wT_Y_X && col == 2))
			{
				if(m_pYRefs->at(row-1).getITYPE() == iT_Variable)
				{
					if((float)m_pWavePts->at(row-1).vy == (float)_ttof(Item.strText))
						continue; 
					else
					{
						rc = m_pYRefs->at(row-1).resolveID(pI);
						if(SUCCESS == rc && NULL != pI)
						{
							pVar = (hCVar*)pI;
							switch(pVar->VariableType())
							{
								case vT_FloatgPt:
									value = (float)_ttof(Item.strText);
									break;
								case vT_Integer:
									value = (int)_ttoi(Item.strText);
								case vT_Unsigned:
								case vT_BitEnumerated:
								case vT_Enumerated:
									value = (unsigned)_ttoi(Item.strText);
							}
							pVar->setDispValue(value);
							pVar->setWriteStatus(1);//user written
							m_bWaveEdited = TRUE;
						}
				
						
					}
				}
			}//endif

			if((m_WaveType == wT_Vert && col == 1)||(m_WaveType == wT_Y_X && col == 1))
			{
				if(m_pXRefs->at(row-1).getITYPE() == iT_Variable)
				{
					if((float)m_pWavePts->at(row-1).vx == (float)_ttof(Item.strText))
						continue; 
					else
					{
						rc = m_pXRefs->at(row-1).resolveID(pI);
						if(SUCCESS == rc && NULL != pI)
						{
							pVar = (hCVar*)pI;
							switch(pVar->VariableType())
							{
								case vT_FloatgPt:
									value = (float)_ttof(Item.strText);
									break;
								case vT_Integer:
									value = (int)_ttoi(Item.strText);
								case vT_Unsigned:
								case vT_BitEnumerated:
								case vT_Enumerated:
									value = (unsigned)_ttoi(Item.strText);
							}
							pVar->setDispValue(value);
							pVar->setWriteStatus(1);//user written
							m_bWaveEdited = TRUE;
						}
				
						
					}
				}
			}//endif

		}//endfor
		

	CDialog::OnOK();
}

void CWaveEditDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

int CWaveEditDlg::DoModal() 
{
	// TODO: Add your specialized code here and/or call the base class

	int iRet = CDialog::DoModal();

	switch(iRet)
	{
		case IDOK:
			{
				if(m_bWaveEdited)
					return 0;
				else
					return -1;
			}
			break;
		case IDCANCEL:
		default:
			return -1;
			break;
	}
	
}
