// BitEnumDispValDlg.cpp : implementation file
//
// used on root menu - list view

#include "stdafx.h"
#include "sdc625.h"
#include "BitEnumDispValDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValDlg dialog


CBitEnumDispValDlg::CBitEnumDispValDlg(CValueVarient tmpVar,hCVar *phCVar,CDDIdeDoc *pDoc,CWnd* pParent /*=NULL*/)
	: sdCDialog(CBitEnumDispValDlg::IDD, pParent),m_pVarval(tmpVar),pEn((hCEnum *)phCVar),m_pDoc(pDoc)
{
	for (int nCount = 0; nCount < MAX_STAT_BITS; nCount++)
	{
		m_pBtnArray[nCount]   = NULL;
		byStatusValue[nCount] = 0;	//Added by ANOOP 06APR2004
	}
}

CBitEnumDispValDlg::~CBitEnumDispValDlg()
{
}

void CBitEnumDispValDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitEnumDispValDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBitEnumDispValDlg, CDialog)
	//{{AFX_MSG_MAP(CBitEnumDispValDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValDlg message handlers


BOOL CBitEnumDispValDlg::OnInitDialog() 
{	
    sdCDialog::OnInitDialog();// has to be done early to initialize the controls

	CDlgScrollable* m_pdlgScroll=new CDlgScrollable(m_pVarval,pEn,m_pDoc,this);

/** the window portion of that dialog is not generated yet.  It resizes itself in init dialog.

    //Modified the size of the rectangle to be a larger constant size, POB - 4 Aug 2008
	CRect rc(20,10,370, 350);// l,t,r,b

	// was::> CRect rc(20,10,280, 5+((m_pdlgScroll->nBitCount+1)*15));// l,t,r,b
	//GetDlgItem(IDC_PLACEHOLDER)->GetWindowRect(rc);
	//ScreenToClient(&rc);

	m_pdlgScroll->MoveWindow(rc);
****/
//	m_pdlgScroll->Create(IDD_SCROLLABLECHILD, WS_CHILD|WS_VISIBLE , rc, this, 100);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


/****************************************** stevev 26jul07 **********************************************/

/////////////////////////////////////////////////////////////////////////////
// CDlgScrollable dialog

CDlgScrollable::CDlgScrollable(CValueVarient tmpVar,hCVar *pVar,CDDIdeDoc *pDoc,CWnd* pParent) 
	: CDialog(CDlgScrollable::IDD, pParent),m_pVarval(tmpVar),pEn((hCEnum *)pVar),m_pDoc(pDoc)
{
	m_bDragging=FALSE;

	ptr2EnumDesc_t				pEnum = NULL; // added 26jul07
	RETURNCODE					rt;
	int							i=0;
		
	nBitCount=0;
	for (int nCount = 0; nCount < MAX_STAT_BITS; nCount++)
	{
		m_pBtnArray[nCount]   = NULL;
		byStatusValue[nCount] = 0;	//Added by ANOOP 06APR2004
	}

	if ( pEn == NULL ) return;

	hCenumList eBitEnumList(pEn->devHndl());
	
	if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS )
	{
		cerr << "ERROR: No eList for bit enum."<<endl;
	}
	else
	{
		nBitCount=eBitEnumList.size();
		if (nBitCount>MAX_STAT_BITS)
		{
			LOGIT(CERR_LOG|UI_LOG,"Bit enum has too many bits to display.\n");
			return;
		}
		for (hCenumList::iterator iT = eBitEnumList.begin(); iT < eBitEnumList.end(); iT++)
		{// isa ptr2EnumDesc_t
			pEnum = (ptr2EnumDesc_t)&(*iT);// PAW 03/03/09 &(*) added
			// stevev - WIDE2NARROW char interface modified to make the following work
			strStatusString[i] = ((wstring)pEnum->descS).c_str(); // for UTF-8 support, POB - 1 Aug 2008;
			byStatusValue[i++] = pEnum->val;
		}
	}

	// modeless dialog - don't forget to force the
	// WS_CHILD style in the resource template and remove
	// caption and system menu
	Create(CDlgScrollable::IDD,pParent);

}

CDlgScrollable::~CDlgScrollable()
{
	for (int nCount = 0; nCount < nBitCount; nCount++)
	{
		if(NULL != m_pBtnArray[nCount] )
		{
			delete m_pBtnArray[nCount];
		}
	}
}

void CDlgScrollable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgScrollable)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgScrollable, CDialog)
	//{{AFX_MSG_MAP(CDlgScrollable)
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgScrollable message handlers

BOOL CDlgScrollable::OnInitDialog() 
{
	//>> moved from parent 03aug11
	CRect rc(20,10,420/*370*/, 345);// l,t,r,b
	MoveWindow(rc);

	unsigned int		nVal = (unsigned int)m_pVarval;
	CRect	rectDevStatus(10,10,400/*260*/,42/*25*/);//l,t,r,b
	for (int nCount = 0; nCount < nBitCount; nCount++)
	{
		m_pBtnArray[nCount] = new CButton();
		m_pBtnArray[nCount]->Create(strStatusString[nCount], WS_CHILD|WS_VISIBLE |BS_3STATE
			 /*|BS_AUTOCHECKBOX | WS_DISABLED*/|BS_MULTILINE, rectDevStatus, this, 100);

        // Set the parent's Font for the control, POB - 4 Aug 2008
        m_pBtnArray[nCount]->SetFont(GetFont());

//		if (nVal & nByteVal)
		if(byStatusValue[nCount] & nVal)	//Added by ANOOP 06APR2004
		{
			m_pBtnArray[nCount]->SetCheck(BST_CHECKED);
		}

//		m_pBtnArray[nCount]->LockWindowUpdate();  
//		nByteVal = nByteVal >> 1;

		rectDevStatus.top = rectDevStatus.bottom + 9/*15*/;
		rectDevStatus.bottom = rectDevStatus.top + 32/*15*/;
	}
	
	// save the original size
	GetWindowRect(m_rcOriginalRect);


	// initial scroll position
	m_nScrollPos = m_hScrollPos = 0; 
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgScrollable::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nDelta;
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	switch (nSBCode)
	{
	case SB_LINEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;

		nDelta = min(max(nMaxPos/20,5),nMaxPos-m_nScrollPos);
		break;

	case SB_LINEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos/20,5),m_nScrollPos);
		break;
	case SB_PAGEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;
		nDelta = min(max(nMaxPos/10,5),nMaxPos-m_nScrollPos);
		break;
	case SB_THUMBTRACK:
	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_nScrollPos;
		break;

	case SB_PAGEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos/10,5),m_nScrollPos);
		break;
	
         default:
		return;
	}
	m_nScrollPos += nDelta;
	SetScrollPos(SB_VERT,m_nScrollPos,TRUE);
	ScrollWindow(0,-nDelta);
	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CDlgScrollable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_nCurHeight = cy;

	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_ALL; 
	si.nMin = 0;
	si.nMax = m_rcOriginalRect.Height();
	si.nPage = cy;
	si.nPos = 0;
    SetScrollInfo(SB_VERT, &si, TRUE); 	

}

BOOL CDlgScrollable::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	if (zDelta<0)
	{
		if (m_nScrollPos < nMaxPos)
		{
			zDelta = min(max(nMaxPos/20,5),nMaxPos-m_nScrollPos);

			m_nScrollPos += zDelta;
			SetScrollPos(SB_VERT,m_nScrollPos,TRUE);
			ScrollWindow(0,-zDelta);
		}
	}
	else
	{
		if (m_nScrollPos > 0)
		{
			zDelta = -min(max(nMaxPos/20,5),m_nScrollPos);

			m_nScrollPos += zDelta;
			SetScrollPos(SB_VERT,m_nScrollPos,TRUE);
			ScrollWindow(0,-zDelta);
		}
	}
	
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}

void CDlgScrollable::OnCancel() 
{
}

void CDlgScrollable::OnOK() 
{
}

void CDlgScrollable::OnLButtonDown(UINT nFlags, CPoint point) 
{
	m_bDragging=TRUE;
	SetCapture();

	m_ptDragPoint=point;

	SetCursor(m_hCursor2);
	
	CDialog::OnLButtonDown(nFlags, point);
}

void CDlgScrollable::OnLButtonUp(UINT nFlags, CPoint point) 
{
	EndDrag();
	
	CDialog::OnLButtonUp(nFlags, point);
}

void CDlgScrollable::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (m_bDragging)
	{
		int nDelta=m_ptDragPoint.y-point.y;
		m_ptDragPoint=point;

		int nNewPos=m_nScrollPos+nDelta;

		if (nNewPos<0)
			nNewPos=0;
		else if (nNewPos>m_rcOriginalRect.Height() - m_nCurHeight)
			nNewPos=m_rcOriginalRect.Height() - m_nCurHeight;

		nDelta=nNewPos-m_nScrollPos;
		m_nScrollPos=nNewPos;

		SetScrollPos(SB_VERT,m_nScrollPos,TRUE);
		ScrollWindow(0,-nDelta);
	}
	
	CDialog::OnMouseMove(nFlags, point);
}

void CDlgScrollable::OnKillFocus(CWnd* pNewWnd) 
{
	CDialog::OnKillFocus(pNewWnd);

	EndDrag();
}

void CDlgScrollable::EndDrag()
{
	m_bDragging=FALSE;
	ReleaseCapture();
	SetCursor(m_hCursor1);
}

BOOL CDlgScrollable::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	BOOL bRet=TRUE;

	if (nHitTest==HTCLIENT)
	{
		SetCursor(m_hCursor1);
		bRet=FALSE;
	}
	else
		bRet=CDialog::OnSetCursor(pWnd,nHitTest,message);

	return bRet;
}

// Eventually we need to add a horizontal scrollbar, POB - 4 Aug 2008
void CDlgScrollable::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
/*	int nDelta;
	int nMaxPos = m_rcOriginalRect.Width() - m_hCurWidth;

	switch (nSBCode)
	{
	case SB_LINERIGHT:
		if (m_hScrollPos >= nMaxPos)
			return;
		nDelta = convRect.right;
		break;

	case SB_LINELEFT:
		if (m_hScrollPos <= 0)
			return;
		nDelta = 0 - m_hScrollPos;//was: -ONECOL;
		break;

    case SB_PAGERIGHT:
		if (m_hScrollPos >= nMaxPos)
			return;
		if (m_hScrollPos+hPage >=nMaxPos)
			nDelta = nMaxPos - m_hScrollPos;
		else
			nDelta = hPage;
		break;

	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_hScrollPos;
		break;

	case SB_PAGELEFT:
		if (m_hScrollPos <= 0)
			return;
		if (m_hScrollPos-hPage <= 0)
			nDelta = - m_hScrollPos;
		else
			nDelta = -hPage;//-min(nMaxPos/10,m_vScrollPos);
		break;
	
         default:
		return;
	}
	m_hScrollPos += nDelta;
	int bPos = (((float)m_hScrollPos/(float)nMaxPos)*(float)m_rect.Width());
	SetScrollPos(SB_HORZ,bPos,TRUE);// now in parts of whole
	ScrollWindow(-nDelta,0);// in device units unless window has style: CS_OWNDC or CS_CLASSDC
	if (nSBCode == SB_ENDSCROLL)// stevev 12jul07 to get the images painted
		Invalidate();*/
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
} 