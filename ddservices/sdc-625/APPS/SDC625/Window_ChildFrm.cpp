// WindowChildFrm.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "Window_ChildFrm.h"
#include "MainFrm.h"
#include "SDC625_WindowView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame

IMPLEMENT_DYNCREATE(CWindowChildFrame, CChildFrame)

BEGIN_MESSAGE_MAP(CWindowChildFrame, CChildFrame)
	//{{AFX_MSG_MAP(CWindowChildFrame)
	ON_WM_CLOSE()
	ON_WM_MDIACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame construction/destruction

CWindowChildFrame::CWindowChildFrame()
{
}

CWindowChildFrame::~CWindowChildFrame()
{
}

BOOL CWindowChildFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	cs.lpszName = _T("Window View");
	cs.style &= ~FWS_ADDTOTITLE;

	return CChildFrame::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame diagnostics

#ifdef _DEBUG
void CWindowChildFrame::AssertValid() const
{
	CChildFrame::AssertValid();
}

void CWindowChildFrame::Dump(CDumpContext& dc) const
{
	CChildFrame::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CWindowChildFrame message handlers

void CWindowChildFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
	
	CChildFrame::OnClose();

	pMainWnd->PostMessage(WM_SDC_SIZECHILD);
}

void CWindowChildFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"CWindowChildFrame OnMDIActivate. %s\n",
														(bActivate)?"Activate":"DEactivate");
	if (! bActivate)// we don't need to do anything on deactivae
		return;

	// call the base class to let standard processing switch to
	// the top-level menu associated with this window
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	Sleep(100);

	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();

	// Build the window's menu
	CMenu* pTopMenu = pMainWnd->GetMenu();
	CMenu* pViewMenu = NULL;
	CString strMenu;

	for(int pos = 0; pos < ((int)pTopMenu->GetMenuItemCount()); pos++)
	{
		pTopMenu->GetMenuString(pos, strMenu, MF_BYPOSITION);

		if (strMenu == "&View")
		{
			// A match is found!!

			// removes popup menu @ pos, deletes it,replaces it with ViewMwnu
			//                     pos is position & next param is a handle
			pTopMenu->ModifyMenu(pos, MF_BYPOSITION|MF_POPUP, 
									  (UINT)(pMainWnd->m_ViewMenu.GetSafeHmenu()), strMenu);
			break;
		}
	}

	// Give View a pointer to its child frame
	// The view can then update the window text of the frame
	CSDC625WindowView * pView =  (CSDC625WindowView *) GetActiveView();

	/* we moved this up to OnWindow - it regularly crashed here
	if (pView && pView->IDD == IDD_SDC625_FORM)// only for the window view...
	{
		pView->m_pWindowFrame = this;
	}
	****/
}
