// BitEnumDispValDlg.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "BitEnumDispValWriteDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValWriteDlg dialog


CBitEnumDispValWriteDlg::CBitEnumDispValWriteDlg(CValueVarient tmpVar,hCVar *phCVar,CDDIdeDoc *pDoc,CWnd* pParent /*=NULL*/)
	: CDialog(CBitEnumDispValWriteDlg::IDD, pParent)
{
	hCitemBase					*pItm = NULL;
	EnumTriad_t					localData;// triad
	RETURNCODE					rt;
	int							i=0;
	
	pVar	= NULL;
	m_pDoc = pDoc;
	m_pVarval=tmpVar;
	if(NULL != phCVar )
	{
		pVar=phCVar;
	}
		
	nBitCount=0;
	for (int nCount = 0; nCount < MAX_STAT_BITS; nCount++)
	{
		m_pBtnArray[nCount] = NULL;
		byStatusValue[nCount] = 0;	//Added by ANOOP 06APR2004
	}

	
	hCEnum* pEn  = (hCEnum*)pVar;
	hCenumList eBitEnumList(pEn->devHndl());
	
	if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS )
	{
		cerr << "ERROR: No eList for bit enum."<<endl;
	}
	else
	{
		nBitCount=eBitEnumList.size();
		for (hCenumList::iterator iT = eBitEnumList.begin(); iT < eBitEnumList.end(); iT++)
		{
			localData = *iT;
			string ss= localData.descS;
			CString tmpStr = ss.c_str();
			strStatusString[i]=tmpStr;	//Added by ANOOP 06APR2004
			byStatusValue[i++] = localData.val;	//Added by ANOOP 06APR2004
		}
	}
}


CBitEnumDispValWriteDlg::~CBitEnumDispValWriteDlg()
{
	for (int nCount = 0; nCount < nBitCount; nCount++)
	{
		if(NULL != m_pBtnArray[nCount] )
		{
			delete m_pBtnArray[nCount];
		}
	}
}



void CBitEnumDispValWriteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitEnumDispValWriteDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBitEnumDispValWriteDlg, CDialog)
	//{{AFX_MSG_MAP(CBitEnumDispValWriteDlg)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitEnumDispValWriteDlg message handlers


BOOL CBitEnumDispValWriteDlg::OnInitDialog() 
{
	//CDialog::OnInitDialog();
	int		nVal = m_pVarval;
//	int		nByteVal = 0x80; 
//	CRect	rectDevStatus(30,60,290,86);
#ifndef _WIN32_WCE
	CRect	rectDevStatus(30,20,260,35);
#else
	CRect	rectDevStatus(15,15,290,31);
#endif

#ifdef _WIN32_WCE
	CenterWindow(GetDesktopWindow());	// center to the hpc screen
#endif
	
	for (int nCount = 0; nCount < nBitCount; nCount++)
	{
		m_pBtnArray[nCount] = new CButton();
		m_pBtnArray[nCount]->Create(strStatusString[nCount], WS_CHILD|WS_VISIBLE |BS_AUTOCHECKBOX/*BS_AUTOCHECKBOX | WS_DISABLED */, rectDevStatus, this, 100);

//		if (nVal & nByteVal)
		if(byStatusValue[nCount] & nVal)	//Added by ANOOP 06APR2004
		{
			m_pBtnArray[nCount]->SetCheck(BST_CHECKED);
		}
//		m_pBtnArray[nCount]->LockWindowUpdate();  
//		nByteVal = nByteVal >> 1;

#ifndef _WIN32_WCE
		rectDevStatus.top = rectDevStatus.bottom + 15;
		rectDevStatus.bottom = rectDevStatus.top + 15;
#else
		rectDevStatus.top = rectDevStatus.bottom + 10;
		rectDevStatus.bottom = rectDevStatus.top + 16;
#endif		
	}	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CBitEnumDispValWriteDlg::OnClose()
{
	unsigned int		nVal =	0;
	EnumTriad_t					localData;// triad
	RETURNCODE					rt;
	hCEnum* pEn  = (hCEnum*)pVar;
	hCenumList eBitEnumList(pEn->devHndl());
	hCenumList::iterator iT;
	
	if ( (rt = pEn->procureList(eBitEnumList)) != SUCCESS )
	{
		cerr << "ERROR: No eList for bit enum."<<endl;
	}
	else
	{
		nBitCount=eBitEnumList.size();
		iT = eBitEnumList.begin();
	}

	for (int nCount = 0; nCount < nBitCount; nCount++)
	{
		if(NULL != m_pBtnArray[nCount] )
		{
			localData = *iT;
			if (m_pBtnArray[nCount]->GetCheck() == BST_CHECKED)
			{
				nVal += localData.val;
			}
			iT++;
		}
	}
	
	if(pVar	!= NULL)
	{
		CValueVarient	localVar;
		localVar = nVal;
		pVar->setDispValue(localVar);
	}
	
	CDialog::OnClose();
}

