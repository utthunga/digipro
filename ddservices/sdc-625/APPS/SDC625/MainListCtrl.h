#if !defined(AFX_MAINLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_)
#define AFX_MAINLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MainListCtrl.h : header file
//
#include "StdAfx.h"
#include "SDC625Doc.h"
#include "SDC625ListView.h"

#include "logging.h"

LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);
/////////////////////////////////////////////////////////////////////////////
// CMainListCtrl window

class CMainListCtrl : public CListCtrl
{
// Construction
public:
    CMainListCtrl();

// Attributes
public: 
    CTableChildFrame* m_pTableFrame;    // holds the ptr for the listview. Fixed defect #4257, , POB - 4/11/2014

// Operations
public:
    void ResetSelection();
    CDDIdeDoc* GetDocument();
// Overrides
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
/// use static function
//  LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);

    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMainListCtrl)
    protected:
    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
    //}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~$");} return CListCtrl::WindowProc(message,wParam,lParam);};
#endif

// Implementation
public:
    virtual ~CMainListCtrl();

    // Generated message map functions
protected:
    //{{AFX_MSG(CMainListCtrl)
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
    //}}AFX_MSG

    DECLARE_MESSAGE_MAP()

    int             m_nSelectedItem;
    CImageList      *m_pImageList;

    CFont font; // made more static sjv 29nov05
};

/////////////////////////////////////////////////////////////////////////////
// CDlgListCtrl window

class CDlgListCtrl : public CListCtrl
{
// Construction
public:
    CDlgListCtrl();

// Attributes
public:

// Operations
protected:
/// use static function
//  LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);

// Overrides
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDlgListCtrl)
    //}}AFX_VIRTUAL

// Implementation
public:
    virtual ~CDlgListCtrl();

    // Generated message map functions
protected:
    //{{AFX_MSG(CDlgListCtrl)
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    //}}AFX_MSG

    DECLARE_MESSAGE_MAP()
private:
    int m_nSelectedItem;
    CImageList * m_pImageList;

    void SelectItem();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_)
