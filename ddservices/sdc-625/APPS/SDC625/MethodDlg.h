/*************************************************************************************************
 *
 * $Workfile: MethodDlg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "MethodDlg.h".		
 */

#if !defined(AFX_METHODDLG_H__14577483_1A11_443C_B71A_156E800E03EC__INCLUDED_)
#define AFX_METHODDLG_H__14577483_1A11_443C_B71A_156E800E03EC__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MethodDlg.h : header file
//
#include "sdcDialog.h"
#include "ddbEvent.h"
#include "MethodsDefs.h"
#include "MethodEditCtrl.h"
#include "ddbMethod.h"
#include "Scope_Ctrl.h"
#include "CheckComboBox.h"

#include "SDC625_drawTypes.h"

/////////////////////////////////////////////////////////////////////////////
// CMenuDlg dialog  TEMPORARY   TEMPORARY   TEMPORARY   TEMPORARY   TEMPORARY 

class CMenuDlg : public CdrawDlg	// CDialog 
{	
public:
	ACTION_UI_DATA         m_structUIData;
	ACTION_USER_INPUT_DATA m_structUserInput;

/* stevev 27jul05 - we are going across devObj boundry:use event	HANDLE m_hUserAckEvent; */
	hCevent  m_hCevent;
	hCevent::hCeventCntrl* p_hCevtCntl;
	
	BOOL m_dlgReady;		// handshake w/ support, set to true to put support into pendOnEvent 


	void SetCaption(CString szName){/*tmp-MT*/}; // Initialize the Caption , with method name
	void SendClose(void);
	
	CMenuDlg(ACTION_UI_DATA* pAUD = NULL, CWnd* pParent = NULL, CdrawBase* pP = NULL);   // standard constructor
	~CMenuDlg(); 
	
	int RunDialog();

	//functions called by device object to send and receive UI info
	BOOL ShowMethodsData(ACTION_UI_DATA structUiData);
	BOOL GetMethodsUserInputData(ACTION_USER_INPUT_DATA& structUserInput){/*tmp-MT*/return FALSE;};

	
	BOOL m_MethodAborted;
	BOOL m_NextButtonSet;
};
/////////////////////////////////////////////////////////////////////////////
// END  CMenuDlg    TEMPORARY   TEMPORARY   TEMPORARY   TEMPORARY   TEMPORARY 




/////////////////////////////////////////////////////////////////////////////
// CMethodDlg dialog

//forward declaration of methods UI data structures
struct ACTION_UI_DATA;
struct ACTION_USER_INPUT_DATA;

//static int nNextCount =0; //temp static variable . used to unit test UI

//prashant oct 2003
#define MAX_HELPSTRING_LENGTH  80

//Anoop 12jan2004 ( The length of the max value that can be entered through a handheld. )
#define HANDHELD_FLOAT_MAXVAL_LEN 12
#define MAX_DATE_LENGTH 10 //Added by ANOOP

class CMethodEditCtrl;
class CMethodDlg : public  sdCDialog//  CDialog
{
// Construction
private:
	CString m_szCaption; // Caption of the dialog box :Name of the method


	//prashant
//attributes
	CRichEditCtrl m_RichEditDisplay;
	CRect m_DisplayRichEditRect;
	CRect m_UserClientRect;
  BOOL m_bTextOverflow;//This flag indicates if the text to be displayed overflows the client area.
  BOOL m_bTextDisplayOnly;//This flag indicates if only text is displayed or text is displayed along with other controls.
  //    CDateTimeCtrl m_DateTimeCtrl;
  	CRect m_DateTimeRect; // used for time

	HMODULE  m_hModRichEdit;

	CFont m_Font;

	CMethodEditCtrl m_EditBox;
	CRect m_EditCtrlRect;
	//VARIANT  m_varUserEditValue;  // the value entered by the user in the edit box
	CValueVarient m_varUserEditValue;  // the value entered by the user in the edit box

	CComboBox m_ComboBox;
	CRect m_ComboCtrlRect;
	CStringList m_ComboElementsList;
	CUIntArray  m_ComboElementValueList;
	int m_nCurrentComboSelection;
	CCheckComboBox* m_pEnumCheckBox;

	COleDateTime m_OleDateTime;


	int m_nPreviousDisplayControl;

	CString m_szMethodName;
	CString m_szHelp;

	CScopeCtrl *m_pScope;

//methods

	void SetUpDisplayRichEditRect(CString strString);
	void SetUpInitialClientAreaRect();
	void SetUpEditBoxRect();
	void CreateComboList();
	void SetUpComboBoxRect(int nCount,CString strLongestString);

	void RemovePreviousControls();

	BOOL ValidateAndUpdateEditBoxValue() ;
	void SetUpEditBox(int nPrecision = 6) ;
	void SetUpDateTimeRect();
	void GetDateFromString(CString strDate);



public:
	ACTION_UI_DATA         m_structUIData;
	ACTION_USER_INPUT_DATA m_structUserInput;

/* stevev 27jul05 - we are going across devObj boundry:use event	HANDLE m_hUserAckEvent; */
	hCevent m_hCevent;
	hCevent::hCeventCntrl* p_hCevtCntl;

	BOOL m_bAbortBtnClicked;
	BOOL m_bNextBtnClicked; //Added by Prashant 17FEB2004 used only for dynamic updation of variable on methods dialog
	
	BOOL m_MethodAborted;
	BOOL m_NextButtonSet;// to record the next operation- set by generator/cleared by consumer

	//hCmethod*   m_pddbMeth;
	hCmethodCall m_MethCall;

  //prashant 230204
   BOOL m_bDialogVisible;
  CRect m_InitialDialogRect;
  //prashant 230204 end
  BOOL m_dlgReady;


	void SetCaption(CString szName); // Initialize the Caption , with method name
	
	// was CMethodDlg(hCmethodCall& MethCall,CString szMethodname,CString szHelp,CWnd* pParent = NULL);   // standard constructor
	
	CMethodDlg(hCmethodCall& MethCall,CWnd* pParent = NULL);   // standard constructor
	~CMethodDlg(); 
	
	

	/* BOOL  StartMethodExecution(); */

	//functions called by device object to send and receive UI info
	BOOL ShowMethodsData(ACTION_UI_DATA structUiData);// to drawmethod
	BOOL GetMethodsUserInputData(ACTION_USER_INPUT_DATA& structUserInput);

  BOOL ShowDialog();



// Dialog Data
	//{{AFX_DATA(CMethodDlg)
	enum { IDD = IDD_DIALOG_METHOD };
	CButton	m_ctlAbort;
	CStatic	m_ctlStat;
	CButton	m_ctlNext;
	CButton	m_ctlHelp;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMethodDlg)
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMethodDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnBtnHelp();
	afx_msg void OnBtnNext();
	afx_msg void OnBtnAbort();
	afx_msg void OnCancel();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	  afx_msg LRESULT OnDrawMethodUI(WPARAM wParam, LPARAM);
		afx_msg LRESULT OnGetMethodUserInput(WPARAM wParam, LPARAM);
	DECLARE_MESSAGE_MAP()
		
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_METHODDLG_H__14577483_1A11_443C_B71A_156E800E03EC__INCLUDED_)

//Vibhor : End of Code
