/**********************************************************************************************
 *
 * $Workfile: MethodSupport.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		MethodSupport.cpp : implementation file
 */
#include "stdafx.h"
#include "sdc625.h"

#include "MethodSupport.h"
#include "MethodDlg.h"
#include "MEE.h" // 07mar07 to get execution specifics

#include "SDCthreadSync.h"
#include "ddbMutex.h"

const hCmethodCall mtMethCall;// an error return structure

#include "SDC625Doc.h" // added 01nov06 to support layout
#include "ddbMenuLayout.h"
#include "ximage.h"		// for pixel stuff


CMethodSupport::CMethodSupport(CDDIdeDoc* pD):pDoc(pD)
{ 
	bEnableDynamicDisplay = false; //Added by Prashant 17FEB2004 for updation of dynamic vars 
									//										 in a method dialog

	m_pddbMeth = NULL;
	m_bInvokedForNonUIAction = false;
	methodStack.clear();
	clientCount = 0;
	m_hwndMain  = 0;
	pActUIdata   = NULL;
	m_pMainFrm   = NULL;
    m_CommErrorOccured = false;
	pCurrentMethodCall = NULL;
} 

CMethodSupport::	~CMethodSupport()
{
	if ( stackSize() > 0 )
	{
		deque<hCmethodCall*>::iterator dqMIT;
		//vector<hCmethodCall*>::iterator dqMIT;
		for (dqMIT = methodStack.begin(); dqMIT !=  methodStack.end(); ++ dqMIT)
		{// is ptr2ptr 2 methodcall
			hCmethodCall* pMC = (hCmethodCall*)(*dqMIT);
			if (pMC->methodThreadID == 0) _CrtDbgBreak();
			RAZE(pMC->m_pMethodDlg);
			RAZE(pMC->m_pMenuDlg);
		}
		methodStack.clear();
	}
}

/*  handle several methods */
hCmethodCall* CMethodSupport::currentMethod()
{
	unsigned thisThreadID = GetCurrentThreadId();
	if ( pCurrentMethodCall == NULL || pCurrentMethodCall->methodThreadID != thisThreadID)
	{
		pCurrentMethodCall = retrieveMethod(thisThreadID);
		if (pCurrentMethodCall == NULL)// we've been removed...should never happen
		{
			LOGIT(CLOG_LOG,L"MethodSupport method removed while still running.\n");
			TRACE(L"### MethodSupport: thread removed while in use (%#x).\n", thisThreadID);
		}
	}
	return pCurrentMethodCall;
}

void CMethodSupport::checkMethodStack(void) // only used in debugging
{
	bool found = false;
	deque<hCmethodCall*>::iterator dqMIT;
	//vector<hCmethodCall*>::iterator dqMIT;
	for (dqMIT = methodStack.begin(); dqMIT !=  methodStack.end(); ++ dqMIT)
	{// is ptr2ptr 2 methodcall
		hCmethodCall* pMC = (hCmethodCall*)(*dqMIT);
		if (pMC->methodThreadID == 0) _CrtDbgBreak();// break on error
	}
	DEBUGLOG(CLOG_LOG,L"MethodSupport MethodStack Passed.\n");
}
void CMethodSupport::pushMethod(hCmethodCall* pNewMeth)
{//deque<hCmethodCall*> methodStack;
	//19jan11 - stevev - made this push unique so switching tasks won't push multiple copies
	bool found = false;
	deque<hCmethodCall*>::iterator dqMIT;
	//vector<hCmethodCall*>::iterator dqMIT;
	for (dqMIT = methodStack.begin(); dqMIT !=  methodStack.end(); ++ dqMIT)
	{// is ptr2ptr 2 methodcall
		hCmethodCall* pMC = (hCmethodCall*)(*dqMIT);
		if (pMC->methodThreadID == 0) _CrtDbgBreak();
		if (pMC->methodThreadID == pNewMeth->methodThreadID)
		{
			found = true;
			// keep going to test all of it for zero thread....break; // out of for loop
		}
	}
	if ( ! found )
	{
		pNewMeth->methodThreadID = GetCurrentThreadId();
		methodStack.push_front(pNewMeth);
		//methodStack.push_back(pNewMeth);	
		TRACE(_T("    Pushed %#x the on stack...now has %d entries.\n"),
												pNewMeth->methodThreadID, methodStack.size() );
		//test
		int y = methodStack.size();
		if ( (methodStack.at(y-1)->methodThreadID == pNewMeth->methodThreadID) &&
			 (pNewMeth->methodThreadID != NULL) )
		{// all is well
		}
		else
		{
			TRACE(L"We gotta mess here\n");
		}
		// end test
	}
	else
	{
		TRACE(_T("Tried to Push on stack... one (of the %d) matches this (%#x) thread.\n"),
												methodStack.size(),pNewMeth->methodThreadID );
		//test
		int y = methodStack.size();
		if ( (methodStack.at(y-1)->methodThreadID == pNewMeth->methodThreadID) && 
			 (pNewMeth->methodThreadID  != NULL) )
		{// all is well
		}
		else
		{
			TRACE(L"We gotta mess here\n");
		}
		// end test
	}
}
hCmethodCall* CMethodSupport::popMethod( void )
{
	hCmethodCall* r = NULL;
	if (methodStack.size() > 0)
	{
		r = methodStack.front();
		// don't remove it....methodStack.pop_front();
	}
	// else - return the null	
	if ( r == NULL )
	{
		TRACE(_T("Failed to pop a thread off the stack of %u\n"),methodStack.size());
	}
	else
	{
		TRACE(_T("    Popped Thrd %#x off stack...has %u\n"), r->methodThreadID,
																		methodStack.size());
	}

	return r;
}
hCmethodCall* CMethodSupport::retrieveMethod( unsigned thrdID )
{
	hCmethodCall* r = NULL;
	if (methodStack.size() > 0)
	{
		//r = methodStack.front();
		//methodStack.pop_front();
		
		deque<hCmethodCall*>::iterator dqMIT;		
		//vector<hCmethodCall*>::iterator dqMIT;
		for (dqMIT = methodStack.begin(); dqMIT !=  methodStack.end(); ++ dqMIT)
		{// is ptr2ptr 2 methodcall
			hCmethodCall* pMC = (hCmethodCall*)(*dqMIT);
			if (pMC->methodThreadID == 0) _CrtDbgBreak();// debugging
			if (pMC->methodThreadID == thrdID)
			{
				r = pMC;
				TRACE(_T("    Retrieved Thrd %#x off stack...has %u\n"),  
																	thrdID,methodStack.size());
				// keep going to test all of it for zero thread....break; // out of for loop
			}
		}
	}
	// else - return the null	
	if ( r == NULL )
	{
		TRACE(_T("Failed to retrieve thread %#x off the stack\n"),thrdID);
	}

	return r;
}
hCmethodCall* CMethodSupport::removeMethod( unsigned thrdID )// does not delete, returns ptr
{
	hCmethodCall* r = NULL;
	if (methodStack.size() > 0)
	{		
		deque<hCmethodCall*>::iterator dqMIT;	
		//vector<hCmethodCall*>::iterator dqMIT;
		for (dqMIT = methodStack.begin(); dqMIT !=  methodStack.end(); ++ dqMIT)
		{// is ptr2ptr 2 methodcall
			hCmethodCall* pMC = (hCmethodCall*)(*dqMIT);
			if (pMC->methodThreadID == 0) _CrtDbgBreak();// debugging
			if (pMC->methodThreadID == thrdID)
			{
				r = pMC;
				(*dqMIT) = NULL;
				methodStack.erase(dqMIT);
				TRACE(_T("    Removed Thrd %#x from stack...now %d (top: %#x)\n"),   
								thrdID,methodStack.size(),
								(methodStack.size())?methodStack.front()->methodThreadID:0);
				break; // out of for loop
			}
		}
		CHKSTK;
	}
	// else - return the null	
	if ( r == NULL )
	{
		TRACE(_T("Failed to remove thread %u from the stack. Stack still has %d.\n"),
																	thrdID,methodStack.size());
	}

	return r;
}

HWND CMethodSupport::getBaseHwnd()
{ 
	if (m_pMainFrm == NULL)
	{
		m_pMainFrm  = (CMainFrame*)(AfxGetApp()->m_pMainWnd);
	}

	if (m_hwndMain == 0 )
	{		
		m_hwndMain = m_pMainFrm->GetSafeHwnd();
	} 
	return m_hwndMain;
}


/********************************************************************************************
 # Function Name  : PreExecute

 # Prototype      :  void PreExecute()


 # Description    :  This function used to do any initializations.Currently it does nothing.

 # Parameters     :   none
**********************************************************************************************/

#define  DLGCOMPLETETIME	(3000/200) // was 5

#ifdef _DEBUG
void CMethodSupport::PreExecute(hCmethodCall& mc)
#else
void CMethodSupport::PreExecute()
#endif
{
	clientCount++;

#ifdef _DEBUG
	LOGIT(CLOG_LOG,L">>  Method PreExecute (%s [%#x]) incremented to %d clients.\n",
							mc.m_pMeth->getWName().c_str(),GetCurrentThreadId(),clientCount);
#endif

TRACE(_T(">>  Method PreExecute incremented to %d clients.\n"),clientCount);

	CHKSTK;
	getBaseHwnd(); // fill 'em if needed

	/* stevev 01jun07 - we gotta stop sending */
	if ( pDoc == NULL ) 
		return;
	hCddbDevice* pDev = pDoc->pCurDev;// we'll need this
	pDev->pCmdDispatch->disableAppCommRequests();
	DEBUGLOG(CLOG_LOG,"MethodSupport PreExecute disables appComm\n");

/**** re-entrant mee means this should happen -- stevev 07mar07
	if (m_pMethodDlg != NULL) // don't allow a new start before the old one has stopped
	{
		for ( int z = 0; z < DLGCOMPLETETIME; z++ )
		{
			if (m_pMethodDlg != NULL)
			{
			//	SleepWithMessageLoop(200);
				systemSleep(200);
			}
			else
				break; // out of for loop
		}
#ifdef _DEBUG
		if ( z >= DLGCOMPLETETIME && m_pMethodDlg != NULL)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"Dialog has not stopped before the next method execution.\n");
			TRACE("Dialog has not stopped before the next method execution.\n");
		}
#endif
	}
	// else - all is well
****/
}

/**********************************************************************************************
 # Function Name  : DoMethodExecute

 # Prototype      :  DoMethodExecute(hCmethod* pM)


 # Description    :  This function called by the device object to execute pre/post actions and
										by DDL method to execute a user selected method.
											
 # Parameters     :  The method description class

 # Returns        :  SUCCESS (0) or failure code

 * 19jan11 - reworked this method and MethodDisplay to allow a maximum of one dialog per thread
		   - This is to prevent the debacle that occurs with a lot of post read/write functions
			and a method tries to run. The pointers got so crossed up the app would crash.
 * 01aug12 - refine by allowing actions to skip the dialog mutex.  This was locking up with
			post edit actions on a MenuDisplay() screen.
**********************************************************************************************/

RETURNCODE CMethodSupport::DoMethodSupportExecute(hCmethodCall& MethCall)
{
	bool          bReturnSuccess;
	RETURNCODE    rc   = SUCCESS;

	hCitemBase*   pIB  = NULL;
	hCVar* pVar        = NULL;
	CValueVarient tempVariant;
	int           iCurrDevStat;
	bool        isAction = (MethCall.source == msrc_ACTION || MethCall.source == msrc_CMD_ACT);

	hCmethodCall *pThisMethCall = &MethCall;// we need a stack variable to keep this thread's
											// info from being changed in another thread.
	DEBUGLOG(CLOG_LOG,L"*** DoMethodSupportExecute: Entry.\n");
//	CValueVarient retVal;
	if ( pDoc == NULL ) 
		return FAILURE;
	hCddbDevice* pDev = pDoc->pCurDev;// we'll need this
	unsigned  int meeCnt = pDev->getMEEdepth();

	// commented out code removed 07mar07
	if(MethCall.m_pMeth == NULL )
		return false;

	wstring wMethNm;
	wMethNm  = MethCall.m_pMeth->getWName();

	// stevev 17jan11 - add dialog pointer protection
	hCmutex::hCmutexCntrl*   pDlgMutex = m_DialogsMtx.getMutexControl("MethodSupportExecute");
	if ( ! isAction )// 01aug12 - let actions slide
		pDlgMutex->aquireMutex(hCevent::FOREVER);// waits forever

	unsigned myThreadID = GetCurrentThreadId();
	pushMethod(pThisMethCall);		// put it in the library...now does this uniquely
	DEBUGLOG(CLOG_LOG,L"*** DoMethodExecute: Entry Acquired for  %s (%#x)\n",
																wMethNm.c_str(),myThreadID);

	if ( meeCnt > 0 )// we are not the first ones in
	{
		if ( pCurrentMethodCall == NULL )
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d MEEs with NO currently running method.\n",
																					   meeCnt);
			TRACE(L"Method Stack Error: <<<< %d MEEs with NO currently running method.\n",
																					meeCnt);
			if ( ! isAction )// 01aug12 - let actions slide	
				pDlgMutex->relinquishMutex();
			m_DialogsMtx.returnMutexControl(pDlgMutex);

			return false;
		}
		// else somebody was running and all is well (we have the mutex)
		
		pCurrentMethodCall = pThisMethCall;// we have a new current
		DEBUGLOG(CLOG_LOG,L"*** DoMethodExecute: current method set to %s (%#x)\n",
																wMethNm.c_str(),myThreadID);
		if (pCurrentMethodCall->m_pMenuDlg != NULL || pCurrentMethodCall->m_pMethodDlg != NULL)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"Warning:Method execution start with active dialog(s).\n");
		}
#ifdef _DEBUG
		if (stackSize() != (clientCount-1))
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with %d clients.\n",
																	stackSize(),clientCount);
		}
		if (stackSize() != meeCnt)
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with %d MEEs.\n",stackSize(),meeCnt);
		}
#endif
	}
	else
	{// we are the first one in
		pCurrentMethodCall = pThisMethCall;// we have a new current
		DEBUGLOG(CLOG_LOG,L"*** DoMethodExecute: first current method set to '%s' (%#x)\n",
																wMethNm.c_str(),myThreadID);
#ifdef _DEBUG
		if (  clientCount > 1 )// clients are inc/dec in pre-post execute functions,should be 1
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d clients with no methods running.\n",
																				clientCount);
		}
#endif
	}

	if ( ! isAction )// 01aug12 - let actions slide	
	{
	DEBUGLOG(CLOG_LOG,L"*** DoMethodExecute: relinquish Mutex for thread(%#x).\n",myThreadID);
	pDlgMutex->relinquishMutex();// don't hold it during execution
	}

	// stevev 24jan11 - DlgMutex protects pCurrentMethodCall.... we don't have the mutex
	//			so we can't use the pointer!
	//if called from an action
	// stevev 24jan11 
	//if(pCurrentMethodCall->source == msrc_ACTION||pCurrentMethodCall->source == msrc_CMD_ACT)
	if(pThisMethCall->source == msrc_ACTION || pThisMethCall->source == msrc_CMD_ACT)
	{
		// stevev 24jan11 if (pCurrentMethodCall->paramList.size() != 1 )
		if (pThisMethCall->paramList.size() != 1 )
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to execute action not executed because "
					       "the item ID of the concerned variable is not provided.\n");
			rc = APP_PARAMETER_ERR;
		}
		else
		{
			LOGIF(LOGP_DD_INFO)(CLOG_LOG,"Start Action method ==========================\n");
			TRACE(L"Start Action method ===============================(%#x)\n", myThreadID);
			// stevev 24jan11 pCurrentMethodCall->m_MethState = mc_Running;
			pThisMethCall->m_MethState = mc_Running;
			//first element in the parameter list is the variable item ID of interest
			// stevev 24jan11 bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,pCurrentMethodCall->methodID,
			// stevev 24jan11 	                          pCurrentMethodCall->paramList[0].vValue.varSymbolID);
			bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev,pThisMethCall->methodID,
				                          pThisMethCall->paramList[0].vValue.varSymbolID);
			LOGIF(LOGP_DD_INFO)(CLOG_LOG,"END Action method ============================\n");
			TRACE(L"End Action method =================================(%#x)\n", myThreadID);
		}
	}
	else
	// stevev 24jan11if (pCurrentMethodCall->source == msrc_EXTERN)
	if (pThisMethCall->source == msrc_EXTERN)
	{
#ifdef _DEBUG
		LOGIT(CLOG_LOG,L"Start User method ===============================%s in (%#x)\n",
															wMethNm.c_str(),myThreadID);
#else
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"Start User method ============================\n");
#endif
		// stevev 24jan11pCurrentMethodCall->m_MethState = mc_Running;
		pThisMethCall->m_MethState = mc_Running;
		// stevev 24jan11 remove:
		//       ReturnSuccess = pDev->pMEE->ExecuteMethod(pDev, pCurrentMethodCall->methodID);
		bReturnSuccess = pDev->pMEE->ExecuteMethod(pDev, pThisMethCall->methodID);	
#ifdef _DEBUG
		LOGIT(CLOG_LOG,L"End__ User method ===========================%s in (%#x)\n", 
																wMethNm.c_str(),myThreadID);
#else
		LOGIF(LOGP_COMM_PKTS)(CLOG_LOG,"End__ User method ============================\n");
#endif	
	}
	else
	{
		LOGIT(CLOG_LOG,"INVALID method === ===============================\n");
		rc = APP_TYPE_UNKNOWN;
	}
	
	if ( ! isAction )// 01aug12 - let actions slide
	{
		pDlgMutex->aquireMutex(hCevent::FOREVER);// waits forever
	}
	hCmethodCall *pMC = currentMethod();// resets the pCurrentMethod to this thread's

	if (pMC == NULL || pCurrentMethodCall == NULL)
	{
		LOGIT(CERR_LOG,"ERROR: Execution failure (was not pushed on the stack)\n");		
		TRACE(_T("*** DoMethodExecute <<<< current method missing...\n"));	
		
		if ( ! isAction )// 01aug12 - let actions slide
			pDlgMutex->relinquishMutex();
		m_DialogsMtx.returnMutexControl(pDlgMutex);
		return false;
	}

	LOGIF(LOGP_DD_INFO)(CLOG_LOG,
		L"*** DoMethodExecute: Post Execute acquired Mutex for thread (%#x).\n",myThreadID);
	// verify it's still my method
	if ( pCurrentMethodCall->methodThreadID != myThreadID)
	{
		pCurrentMethodCall = retrieveMethod(myThreadID);
		if (pCurrentMethodCall == NULL)// we've been removed...should never happen
		{
			LOGIT(CLOG_LOG,L"MethodSupport method removed before closing(programmer error)\n");
			TRACE(L"*** DoMethodExecute: ERROR!!!!   this thread removed %s(%#x).\n",
																wMethNm.c_str(),myThreadID);
			if ( ! isAction )// 01aug12 - let actions slide
					pDlgMutex->relinquishMutex();
			m_DialogsMtx.returnMutexControl(pDlgMutex);			
			MethCall.m_MethState = mc_NoTExist;
			return 2; // error return
		}		
		LOGIF(LOGP_DD_INFO)(CLOG_LOG,_T("*** DoMethodExecute: current method reset to %#x.\n"),
														pCurrentMethodCall->methodThreadID);
	}
	else
	{
		LOGIF(LOGP_DD_INFO)(CLOG_LOG,
			L"*** DoMethodExecute: current method call already set to me: %s(%#x).\n",
																wMethNm.c_str(),myThreadID);
	}

	// this is post execute activity::>
	if (pCurrentMethodCall->m_pMethodDlg)
	{ //close the methods dialog after the method execution is over	

		// we need to have a counter of dialog users so we close the dialog when everybody is done
		pCurrentMethodCall->m_MethState = mc_Closing;
		TRACE(L"*** DoMethodExecute:'%s' send Method-Dialog close message.\n",wMethNm.c_str());
		pCurrentMethodCall->m_pMethodDlg->SendMessage(WM_CLOSE);// we'll check it in a bit
	}

	if (pCurrentMethodCall->m_pMenuDlg)
	{//close the menu dialog after the method execution is over
		// no need to count this one, kill it when you leave(if we aren't action)
		pCurrentMethodCall->m_pMenuDlg->SendClose();// we'll check it in a bit
	}

	if (pCurrentMethodCall->source != msrc_CMD_ACT)
	{// @ Not cmd action - handle command 38
		rc = pDev->getItemBySymNumber(DEVICE_COMM48_STATUS, &pIB);
		if(SUCCESS == rc && pIB != NULL)
		{
			pVar = (hCVar*)pIB;
			// modified for KROHNE device error 10/18/04 stevev
			tempVariant  = pVar->getRealValue();
			iCurrDevStat = (int) tempVariant;
			if(iCurrDevStat & DS_CONFIGCHANGED)
			{// It's assumed that Command 38 is being supported by the device
				rc = pDev->sendMethodCmd(38,-1);
			}
		}
		else
		{
			LOGIT(CLOG_LOG,L"MethodSupport: Command status item Not found.\n");
		}
	}// else command activity will deal with response code and status

	// kill any menu/method displays left over :: we told 'em to die - now check it out
	int z;// PAW 03/03/09	
	for ( /*int*/ z = 0; z < 30; z++ )
	{
		// Wait for complete deletion - just a ! ready will have a cross-thread pointer failure
		//if ( (m_pMethodDlg != NULL && m_pMethodDlg->m_dlgReady)   // have a method dialog
		//   ||(m_pMenuDlg   != NULL && m_pMenuDlg->m_dlgReady)    )//have a menuDisplay dialog
		//if (m_pMethodDlg != NULL || m_pMenuDlg   != NULL )
		if (pCurrentMethodCall->m_pMethodDlg != NULL || pCurrentMethodCall->m_pMenuDlg != NULL)
		{
			systemSleep(200);
		}
		else
		{
			break; // out of for loop
		}
	}// next

	if ( z >= 30 )
	{
		LOGIT(CLOG_LOG,"Timed out on method dialog deletion.<EXCEPTION IMMINENT>\n");
		TRACE(L"*** Method dialog deletion timed out.\n"); 
	} 

//stevev: The thread alloc'd it, it should delete it:: do  it there 	RAZE(m_pMenuDlg);
//stevev: The thread alloc'd it, it should delete it:: do  it there 	RAZE(m_pMethodDlg);
// this is actually being done below

	LOGIT(CLOG_LOG,"End method ======================================\n");
	
	if ( rc == SUCCESS && ! bReturnSuccess)
	{
		rc = FAILURE;
		DEBUGLOG(CLOG_LOG,"MethodSupport has Execution Failed but all Returns successful.\n");
	}// if both success, leave it, if rc has an error code, leave it

	if ( stackSize() > 0 )// we pushed ' em on entry
	{
		pCurrentMethodCall = removeMethod(myThreadID);
		LOGIF(LOGP_DD_INFO)(CLOG_LOG,
				L"*** DoMethodExecute: '%s' remove method from the stack.\n",wMethNm.c_str());
		if ( &MethCall == pCurrentMethodCall)
		{
			pCurrentMethodCall = popMethod(); // we be done with the old, put previous on
			if (pCurrentMethodCall == NULL)
			{
				if (stackSize() > 0)
				{
					TRACE(L"                   : Current Method NULL with %u still on the "
																	L"stack.\n",stackSize());
				}
				else
				{
					TRACE(L"                   : Current Method NULL with stack empty.\n");
				}
			}
			else
			{
				TRACE(L"                   : Current Method reset to %#x .\n",
														 pCurrentMethodCall->methodThreadID);
			}
		}
		else
		{
			TRACE(L"                   : Current Method NOT cleared.(Pointer mismatch)\n");
		}
	}
	else
	{
		pCurrentMethodCall = NULL; // we be done
		TRACE(L"*** DoMethodExecute: '%s' Nothing On the stack: null method anyway.\n",
																			wMethNm.c_str());
	}

	MethCall.m_MethState = mc_NoTExist;
	if(rc == FAILURE)
	{	//m_ctlStat.SetWindowText("Method Execution Failed");
		if (pDev->pMEE->latestMethodStatus == me_Aborted)
		{
			LOGIT(CERR_LOG|CLOG_LOG,"Method execution aborted.  selfExecute() function returned an "
			"error.\n\t\t(method abort always returns an error)\n");
		}
		else
		{
		LOGIT(CERR_LOG|CLOG_LOG,"Method execution failed.  selfExecute() has returned an error.\n");
		// return rc;   
		// Used to return false; false == zero and SUCCESS == 0.  This is a defect.  
        // This means that it always returned success even when there was a problem or abort in
        //  the method.  This needs to return FAILURE. Methods that fail should not allow the
        //  next method to run in a list or allow an event (i.e., Edit) to occur if there is an
		//  abort or method
        // failure, POB - 14 Aug 2008
		}
	}
	CHKSTK;
	// after latest status
	
	if ( ! isAction )// 01aug12 - let actions slide
		pDlgMutex->relinquishMutex();
	m_DialogsMtx.returnMutexControl(pDlgMutex);

	LOGIF(LOGP_DD_INFO)(CLOG_LOG,
			L"*** DoMethodExecute: exit release Mutex %s(%#x).\n",wMethNm.c_str(),myThreadID);
		

	if(m_CommErrorOccured == true)
	{
		return 1;
	}
	else
	{
        // Used to return the previous method code instead of the current return code. 
		// This does make sense; it shows an error for a previous method execution when the
		// current method does not have one. Now, we return the current "return code" instead,
		// POB - 14 Aug 2008.
		return rc; //MethCall.m_pMeth->GetLastMethodsReturnCode();  
	}
}


/**********************************************************************************************
 # Function Name  : PostExecute

 # Prototype      :  void PostExecute()


 # Description    :  This function is used to do the cleanup, if any.Currently this funcyion
										 doesn't do anything.

 # Parameters     :   none
**********************************************************************************************/
#ifdef _DEBUG
void CMethodSupport::PostExecute(hCmethodCall& mc)
#else
void CMethodSupport::PostExecute()
#endif
{
	// dec client count
// moved to end	clientCount--;
#ifdef _DEBUG
	TRACE(_T(">>  Method PostExecute (%s [%#x]) with %d clients.\n"),
						mc.m_pMeth->getWName().c_str(),GetCurrentThreadId(),clientCount);
#else
	TRACE(_T(">>  Method PostExecute with %d clients.\n"),clientCount);
#endif
	CHKSTK;
	/* stevev 01jun07 - commands cannot be sent during method execution */
	if ( pDoc == NULL ) 
		return;
	hCddbDevice* pDev = pDoc->pCurDev;// we'll need this
	
	pDev->pCmdDispatch->enableAppCommRequests();
	DEBUGLOG(CLOG_LOG,"MethodSupport PostExecute enables appComm\n");


/* execute method has popped the stack*/
//&	if (m_pMethodDlg)
//&	{ //close the methods dialog after the method execution is over
//&	
//&TRACE(">> Method PostExecute - sending CLOSE message to the dialog.(0x%x)\n",
//																		GetCurrentThreadId());
//&		m_pMethodDlg->SendMessage(WM_CLOSE);
//&		for ( int z = 0; z < 30; z++ )
//&		{
//&			if (m_pMethodDlg != NULL && m_pMethodDlg->m_dlgReady)
//&			{
//&			//	SleepWithMessageLoop(200);
//&				systemSleep(200);
//&			}
//&			else
//&			{
//&				break; // out of for loop
//&			}
//&		}
//		if (m_pMethodDlg != NULL)
//			delete m_pMethodDlg;
//&TRACE(">>Method PostExecute post pend %s. deleting dialog.\n",  (z>=30)? "-Expired-":""   );
//&		// sjv 30jan06 - stacked dialogs return; // we only allow one display per
//&	}
//&	if (m_pMenuDlg)
//&	{//close the menu dialog after the method execution is over
//&TRACE(">> Method PostExecute -sending CLOSE message to the displaymenu.(0x%x)\n",
	//																	GetCurrentThreadId());
//&		m_pMenuDlg->SendClose();
//&		for ( int z = 0; z < 30; z++ )
//&		{
//&			if (m_pMenuDlg != NULL && m_pMenuDlg->m_dlgReady)
//&			{
//&			//	SleepWithMessageLoop(200);
//&				systemSleep(200);
//&			}
//&			else
//&				break; // out of for loop
//&		}
//&TRACE(">>Method PostExecute post pend %s. deleting displaymenu.\n",(z>=30)?"-Expired-":"");
//&		if (m_pMenuDlg != NULL)
//&		{
//&			delete m_pMenuDlg;
//&		}
//&	}


//&	if ( stackSize() > 0 )
//&	{
//&TRACE("  PostExecute pops one off the stack.\n");
//&		hCmethodCall* pMC = currentMethod();
//&		// the new active stuff
//&		m_pMethodDlg = pMC->m_pMethodDlg;
//&		m_pMenuDlg   = pMC->m_pMenuDlg;	
//&	}
//&	else
//&	{// just in case...
// another one is already starting up
// this blows it out of the water: trust StartMethodDisplayProc to handle it
//		TRACE("  PostExecute Clears the pointers.\n");
//		m_pMethodDlg = NULL;
//		m_pMenuDlg   = NULL;	
//&	}
// moved from top
	clientCount--;

#ifdef _DEBUG
	if ( clientCount == 0 )
	{// if zero, send close, delete
		TRACE(_T(">>  Method PostExecute now has Zero clients.\n"));
		if (stackSize() != clientCount)
		{
			LOGIT(CERR_LOG,"Method Stack Error: %d stack with NO clients.\n",stackSize());
		}
	}
	else
	{
#ifdef _DEBUG
		TRACE(_T(">>  Method PostExecute (%s [%#x]) now has %d clients.\n"),
							mc.m_pMeth->getWName().c_str(),GetCurrentThreadId(),clientCount);
#else
		TRACE(_T(">>  Method PostExecute now has %d clients.\n"),clientCount);
#endif
	}
#endif
//TRACE("  PostExecute exits.\n");
}

/**********************************************************************************************
 # Function Name  : numberMethodsRunning

 # Prototype      : int numberMethodsRunning()

 # Description    : Returns the number of methods currently running or a negative number @error
					Checks stack, client count and MEE count match.
                 
 # Parameters     : none
**********************************************************************************************/

int  CMethodSupport::numberMethodsRunning() 
{
	return clientCount;
}

/**********************************************************************************************
 # Function Name  : StartMethodDisplayProc

 # Prototype      :  UINT StartMethodDisplayProc(LPVOID ddlMethod)


 # Description    :  This is the global thread function that is called to execute the dialog in
										 a seperate thread.
                 
 # Parameters     :   LPVOID ddlMethod
**********************************************************************************************/
UINT StartMethodDisplayProc(LPVOID MethDlg)
{	
	// Can't pass CWnds between threads, so pass HWND
	// Create CWnd object from passed in HWND

TRACE(_T(">> StartMethodDisplayProc thread (0x%x) is started.\n "),GetCurrentThreadId());

	CMethodSupport* pMethSupp = (CMethodSupport*)MethDlg;
	if ( pMethSupp == NULL || pMethSupp->pCurrentMethodCall == NULL ||  
		 pMethSupp->pCurrentMethodCall->m_pMethodDlg != NULL)
	{
		return false;
	}
	else
	{//hCmethodCall& MethCall,CString szMethodname,CString szHelp,CWnd* pParent 
		pMethSupp->pCurrentMethodCall->m_pMethodDlg = 
											new CMethodDlg(*(pMethSupp->pCurrentMethodCall), 
												CWnd::FromHandle(pMethSupp->getBaseHwnd()) );
		if (pMethSupp->pCurrentMethodCall->m_pMethodDlg)
		{
TRACE(_T(">> Dialog Display-Proc starting modal.(0x%x)\n"),GetCurrentThreadId());
// try to stop the dialog lock-up issue:pMethSupp->pCurrentMethodCall->m_pMethodDlg->DoModal();
			pMethSupp->pCurrentMethodCall->m_pMethodDlg->sdcModl();
		// it is possible that the methodcall has disappeared before the modal dialog returns
			if (pMethSupp == NULL || 
				pMethSupp->pCurrentMethodCall == NULL  ||
				pMethSupp->pCurrentMethodCall->m_pMethodDlg == NULL )
			{
TRACE(_T(">> Dialog Display-Proc finished modal without method.(0x%x) "),GetCurrentThreadId());
return true;
			}
			pMethSupp->pCurrentMethodCall->m_pMethodDlg->m_dlgReady = false;
TRACE(_T(">> Dialog Display-Proc finished modal.(0x%x) "),GetCurrentThreadId());
			delete pMethSupp->pCurrentMethodCall->m_pMethodDlg;
			pMethSupp->pCurrentMethodCall->m_pMethodDlg = NULL;
			TRACE(L"and cleared the dlg ptr in thread %#x\n",
												pMethSupp->pCurrentMethodCall->methodThreadID);
		}
		else
		{
			return false;
		}
	}
TRACE(_T(">> StartMethodDisplayProc thread (0x%x) is terminated.\n "),GetCurrentThreadId());
	return true;
}

// temp to check parameter
//CMethodSupport* pMS = NULL;
/**********************************************************************************************
 # Function Name  : StartDisplayMenuProc

 # Prototype      :  UINT StartDisplayMenuProc(LPVOID ddlMethodSup)


 # Description    :  This is the global thread function that is called to execute the dialog in
										 a seperate thread.
                 
 # Parameters     :   LPVOID ddlMethod
**********************************************************************************************/
UINT StartDisplayMenuProc(LPVOID ddlMethodSup)
{	
	// Can't pass CWnds between threads, so pass HWND
	// Create CWnd object from passed in HWND

TRACE(_T(">> StartDisplayMenuProc thread (0x%x) is started.\n "),GetCurrentThreadId());

	CMethodSupport* pMethSupp = (CMethodSupport*)ddlMethodSup;
//if ( pMS != pMethSupp )
//{
//	LOGIT(CERR_LOG,"No param ptr.\n");
//}
	if ( pMethSupp == NULL || pMethSupp->pCurrentMethodCall == NULL ||  
		 pMethSupp->pCurrentMethodCall->m_pMenuDlg != NULL         )
	{
		return false;
	}
	else
	{//hCmethodCall& MethCall,CString szMethodname,CString szHelp,CWnd* pParent 
		bool holdUpdateState = true;
		
		pMethSupp->pCurrentMethodCall->m_pMenuDlg = new CMenuDlg(pMethSupp->pActUIdata, 
			/* new 20oct05 */                pMethSupp->m_pMainFrm);
		// originally::						 CWnd::FromHandle(pMethSupp->getBaseHwnd()) );
		if (pMethSupp->pCurrentMethodCall->m_pMenuDlg)
		{
			if ( pMethSupp->m_pMainFrm != NULL && pMethSupp->m_pMainFrm->GetDocument()  )
			{
				// enable's aren't pushed so we have to remember where it was
				//holdUpdateState = pMethSupp->m_pMainFrm->m_pDoc->m_autoUpdateDisabled;
				holdUpdateState = pMethSupp->m_pMainFrm->m_pDoc->appCommEnabled();
				//pMethSupp->m_pMainFrm->m_pDoc->m_autoUpdateDisabled = false;
				pMethSupp->m_pMainFrm->m_pDoc->enableAppCommRequests();
				DEBUGLOG(CLOG_LOG,"MethodSupport MethodDisplay enables appComm\n");

			}
TRACE(_T(">> DisplayMenu Menu-Proc starting modal.(0x%x)\n"),GetCurrentThreadId());
//temp hold			pMethSupp->m_pMenuDlg->DoModal();

			pMethSupp->pCurrentMethodCall->m_pMenuDlg->RunDialog();

			pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_dlgReady = false;
			if ( pMethSupp->m_pMainFrm != NULL && pMethSupp->m_pMainFrm->GetDocument()  )
			{	
				if ( holdUpdateState )
				{
					pMethSupp->m_pMainFrm->m_pDoc->enableAppCommRequests();
					DEBUGLOG(CLOG_LOG,"MethodSupport MethodDisplay enables appComm again.\n");
				}
				else
				{
					pMethSupp->m_pMainFrm->m_pDoc->disableAppCommRequests();
					DEBUGLOG(CLOG_LOG,"MethodSupport MethodDisplay disables appComm\n");
				}
			}
			// process button data
			if ( pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_bAbortBtnClicked )
			{	pMethSupp->m_buttonSelection = -1;
			}
			else
			if ( pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_bNextBtnClicked )
			{	pMethSupp->m_buttonSelection = 0;
			}
			else
			if ( pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_bButOneClicked )
			{	pMethSupp->m_buttonSelection = 1;
			}
			else
			if ( pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_bButTwoClicked )
			{	pMethSupp->m_buttonSelection = 2;
			}
			else
			if ( pMethSupp->pCurrentMethodCall->m_pMenuDlg->m_bButThrClicked )
			{	pMethSupp->m_buttonSelection = 3;
			}
			else
			{// we got serious problems
				pMethSupp->m_buttonSelection = -2;
			}

			pMethSupp->pCurrentMethodCall->m_pMenuDlg->p_hCevtCntl->triggerEvent();
	

			delete pMethSupp->pCurrentMethodCall->m_pMenuDlg;
			pMethSupp->pCurrentMethodCall->m_pMenuDlg = NULL;
			systemSleep(20);// added sleep to clear notifications & moved delete down 25sep07
TRACE(_T(">> DisplayMenu Menu-Proc finished modal.\n"));
		}
		else
		{
			return false;
		}
	}
TRACE(_T(">> StartDisplayMenuProc thread (0x%x) is terminated.\n "),GetCurrentThreadId());
	return true;
}

/**********************************************************************************************
# Function Name	: MethodDisplay

# Prototype     :  void MethodDisplay(ACTION_UI_DATA structMethodsUIData,
												ACTION_USER_INPUT_DATA& structUserInputData)


# Description   :  This function is called from the builtins to display messages to the user &
					get the user inputs.  It calls the function in MethodsDlg to diaplay the 
					information. It than waits for the event when the user clicks next.It then 
					calls the MethodsDlg function to get the user input values.
		
# Parameters    : ACTION_UI_DATA structMethodsUIData -  This structure contains all information
														about the data to be dispalyed.
				  ACTION_USER_INPUT_DATA& structUserInputDats - This structure contains all the
														information entered by the user.

  # return value:  false == ABORT button pressed true == not

**********************************************************************************************/
#define USE_METHOD_THREAD
bool CMethodSupport::MethodDisplay(ACTION_UI_DATA structMethodsUIData,
												   ACTION_USER_INPUT_DATA& structUserInputData)
{
	hCmethodCall* pMC = NULL;

	DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay >>>> Entry (0x%x)\n",GetCurrentThreadId());

/* stevev - changed the message box so it is independent of the method dialog: error freeze-up
	freeze-up puts up a method UI screen with no buttons enabled,does the message box & exits.
	the method UI screen stays forever - have to kill sdc ***/
	if ( structMethodsUIData.userInterfaceDataType == VARIABLES_CHANGED_MSG )
	{// short circuit dialog interaction		
		DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay <<<< Display DrawMethod Changed.\n");	
		//display the message indicating that some variable values have changed during 
		// method execution and the user has to commit or cancel these changes once he exits 
		// the methods dialog.
		CString strMsg;
		strMsg.LoadString(IDS_VAR_VALS_CHANGED);
		AfxMessageBox(strMsg);
		return true;
	}

	hCmutex::hCmutexCntrl*   pDlgMutex = m_DialogsMtx.getMutexControl("MethodDisplay");
	pDlgMutex->aquireMutex(hCevent::FOREVER);// waits forever

	pMC = currentMethod();// resets the pCurrentMethod to this thread's

	if (pMC == NULL || pMC->m_pMeth == NULL)
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Execution failure (was not pushed on the stack)\n");
		pDlgMutex->relinquishMutex();
		m_DialogsMtx.returnMutexControl(pDlgMutex);	
		return false;
	}
	
	wstring wMethNm;
	wMethNm  = pMC->m_pMeth->getWName();
	unsigned thisThreadID = GetCurrentThreadId();

	DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * aquired mutex (0x%x).\n",thisThreadID);

	if ( structMethodsUIData.userInterfaceDataType != MENU )
	{// do standard method display
		if (pMC->m_pMethodDlg == NULL) 
		{// generate a new one
#ifdef _DEBUG
			LOGIT(CLOG_LOG,">> MS.MethodDisplay * Dialog Does not exist - spin a thread.\n");
			CWinThread* newThread = 
#endif
			AfxBeginThread(StartMethodDisplayProc, (LPVOID)this);

#ifdef _DEBUG
			LOGIT(CLOG_LOG,">> MS.MethodDisplay * Dialog thread spun (%#x) from (0x%x).\n",
														newThread->m_nThreadID, thisThreadID);
#endif
			// pend on ready
			int z;
			for (  z = 0; z < 30; z++ )
			{
				if ( pMC->m_pMethodDlg == NULL || ! pMC->m_pMethodDlg->m_dlgReady )
				{
					systemSleep(200);
				}
				else
					break;
			}
			if (z >= 30)
			{
				LOGIT(CERR_LOG|CLOG_LOG,"ERROR: Method failed to generate a dialog.\n");
				pDlgMutex->relinquishMutex();
				m_DialogsMtx.returnMutexControl(pDlgMutex);
				return false;
			}
			else//  continue working
			{
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * generated a Method dialog..\n");
			}

		}
		else  // we already have a method dialog up
		{
#if 0 // assume we want to keep displaying on the same dialog
			LOGIT(CERR_LOG,">> MS.MethodDisplay <<<< '%s' tried to display while a dialog was"
																" active.\n",wMethNm.c_str());
			TRACE(        L">> MS.MethodDisplay <<<< '%s' tried to display while a dialog was"
															L" active.\n",wMethNm.c_str());
			pDlgMutex->relinquishMutex();
			m_DialogsMtx.returnMutexControl(pDlgMutex);

			return false;
#endif
		}
	}
	else // request is type menu - do displayMenu()
	{		
		pActUIdata = & structMethodsUIData;
		if (pMC->m_pMenuDlg == NULL) 
		{// generate a new one in a 'User-Interface Thread'
// temp to check param
//pMS = this;
#ifdef _DEBUG
			TRACE(_T(">> MS.MethodDisplay * DisplayMenu doesn't exist - spin a thread.\n"));
			CWinThread* newThread = 
#endif			
			
			AfxBeginThread(StartDisplayMenuProc, (LPVOID)this);

#ifdef _DEBUG

			TRACE(_T(">> MS.MethodDisplay * DisplayMenu thread (%#x) from (0x%x).\n"),
														newThread->m_nThreadID,thisThreadID);
#endif

			// pend on dialog constructed
			int z;	
			for ( z = 0; z < 30; z++ )
			{
				if (pMC->m_pMenuDlg == NULL || ! pMC->m_pMenuDlg->m_dlgReady)
				{
					systemSleep(200);
				}
				else
					break;
			}
			if (z >= 30)
			{
				LOGIT(CERR_LOG|CLOG_LOG,">> MS.MethodDisplay <<<< Menu failed to startup.\n");
				pDlgMutex->relinquishMutex();
				m_DialogsMtx.returnMutexControl(pDlgMutex);
				return false;
			}
			else
			{// success - continue working
TRACE(_T(">> MS.MethodDisplay * generated a DisplayMenu dialog..\n"));
			}			
		}
		// stevev 26jan06 - return error at second displaymenu
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,L">> MS.MethodDisplay <<<< '%s' attempted re-entry to "
													L"DisplayMenu().\n",wMethNm.c_str());
			TRACE(                  L">> MS.MethodDisplay <<<< '%s' attempted re-entry to "
													L"DisplayMenu().\n",wMethNm.c_str());
			pDlgMutex->relinquishMutex();
			m_DialogsMtx.returnMutexControl(pDlgMutex);
			return false;
		}
	}

	//call function in MethodsDlg to display the information got from builtins
	bool bReturn = true;	
	if ( structMethodsUIData.userInterfaceDataType != MENU )
	{
		if ( pMC->m_pMethodDlg->m_hWnd == 0 || ! ::IsWindow(pMC->m_pMethodDlg->m_hWnd))
		{
			LOGIT(CLOG_LOG,">> MS.MethodDisplay Method support has a Dialog with an invalid Window.\n");
			LOGIT(CERR_LOG|CLOG_LOG,L">> MS.MethodDisplay <<<< '%s' has a Dialog with an " 
													L"invalid Window..\n",wMethNm.c_str());
			pDlgMutex->relinquishMutex();
			m_DialogsMtx.returnMutexControl(pDlgMutex);
			return false;
		}
		else
		{
			DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - Dialog sending msg.\n");
			pMC->m_pMethodDlg->ShowMethodsData(structMethodsUIData);// send message
			DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - Dialog finished sending msg.Sleep.\n");
			systemSleep(300);// try to get the dialog to pump some messages
		}
/*  stevev 26jul05 - the related states vs actions of the 3 parameters:
					bUserAcknowledge, bDisplayDynamic, and bEnableAbortOnly 
					and the internal  bEnableDynamicDisplay are unclear at best.
	// reworked below ///////////////////////////////////////////////////////////
Removed the old, commented out version 01nov06, 
	you can view an earlier version if the contents are needed
***** end //  stevev 26jul05  / / / ** */

/*  NOTES stevev 26jul05 -----------------
	see excel sheet: tasks_Methods.xls::MethSupportStates. NEW table
	summary::>				
	Next			Abort		loc.Dyn	NxtBtn	AbtBtn	str.Dyn	BtnEvt	retVal	GetUserInput
	don't care		true		clear	clear	clear	clear	clear	false	
	true			false		clear	clear			clear	clear	true	Execute
	don't care		true		clear	clear	clear	clear	clear	false	
	true			false				clear					clear	true	Execute
 -----------------------end Notes 26jul05 */
		if(bEnableDynamicDisplay)
		{// poll UI, don't pend on it!!!
			/* stevev 11nov14 - crashed when lost communication during method display: 
								m_pMethodDlg had been nulled */
			if( pMC && (pMC->m_pMethodDlg) && pMC->m_pMethodDlg->m_MethodAborted)
			{
				bEnableDynamicDisplay = false;
				pMC->m_pMethodDlg->m_bNextBtnClicked  = false;
				pMC->m_pMethodDlg->m_bAbortBtnClicked = false;
				// stevev 27jul05 ResetEvent(m_pMethodDlg->m_hUserAckEvent);
				pMC->m_pMethodDlg->p_hCevtCntl->clearEvent();
        		structMethodsUIData.bDisplayDynamic = false;
				// we leave m_MethodAborted for future calls
				bReturn = false;
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * detected Abort button clicked.\n");
			}
			else
			if(pMC && (pMC->m_pMethodDlg) && pMC->m_pMethodDlg->m_NextButtonSet)
			{
				bEnableDynamicDisplay = false;
				pMC->m_pMethodDlg->m_bNextBtnClicked = false;
        		structMethodsUIData.bDisplayDynamic = false;	
				// stevev 27jul05 ResetEvent(m_pMethodDlg->m_hUserAckEvent);
				pMC->m_pMethodDlg->p_hCevtCntl->clearEvent();
				pMC->m_pMethodDlg->m_NextButtonSet = false;
				bReturn = true;
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - detected Next button clicked "
														" so -GetMethodsUserInputData\n");
        		pMC->m_pMethodDlg->GetMethodsUserInputData(structUserInputData);	
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - Finished - GetMethodsUserInputData\n");
			}
			else			
			if( pMC == NULL || (pMC->m_pMethodDlg)== NULL )
			{
				bEnableDynamicDisplay = false;
        		structMethodsUIData.bDisplayDynamic = false;
				bReturn = false;
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * detected Dialog Shut down.\n");
			}
			// else no-op just return
			else
			{
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay -Nothing was clicked.\n");
			}
		}
		else
		{// pend and then act on it
			bReturn = true;
			if (structMethodsUIData.bUserAcknowledge && structMethodsUIData.uDelayTime == 0)
			{
				structMethodsUIData.uDelayTime = hCevent::FOREVER;
			}
			DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * Dialog pending on button. @ 0x%08p from (0x%x)\n",
												pMC->m_pMethodDlg->p_hCevtCntl,thisThreadID);

			pMC->m_pMethodDlg->p_hCevtCntl->pendOnEvent(structMethodsUIData.uDelayTime);

			DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * Dialog finished pending on button.\n");
			if( pMC->m_pMethodDlg->m_bAbortBtnClicked)// stevev 29may14 - breturn is always true
			{
				bEnableDynamicDisplay = false;
				pMC->m_pMethodDlg->m_bNextBtnClicked  = false;
				pMC->m_pMethodDlg->m_bAbortBtnClicked = false;
				//ResetEvent(m_pMethodDlg->m_hUserAckEvent);
        		structMethodsUIData.bDisplayDynamic = false;
				pMC->m_pMethodDlg->m_NextButtonSet = false;
				// we leave method aborted
				bReturn = false;
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay * Dialog shows Abort pressed.\n");
			}
			else
			if(pMC->m_pMethodDlg->m_bNextBtnClicked)
			{
				// not this state    bEnableDynamicDisplay = false;
				pMC->m_pMethodDlg->m_bNextBtnClicked = false;
				pMC->m_pMethodDlg->m_NextButtonSet = false;
        		// not this state    structMethodsUIData.bDisplayDynamic = false;	
				//ResetEvent(m_pMethodDlg->m_hUserAckEvent);
				bReturn = true;
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - Dialog shows Next pressed - getting input.\n");
        		pMC->m_pMethodDlg->GetMethodsUserInputData(structUserInputData);	
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay - Dialog shows Next pressed - finished getting input.\n");
			}
			// else - (almost) 
			//        impossible to get an event w/ no error and have both of these false
			// we won't log or anything for now
			else
			{			
				DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay Dialog shows neither Abort NOR Next pressed.\n");		
			}
		}
	}
	else // a menu
	{
// pend on event!!!!!!!!!!!!!!!!
		bReturn = true;
		m_buttonSelection = -100; // no button selected yet
		if (structMethodsUIData.bUserAcknowledge && structMethodsUIData.uDelayTime == 0)
		{
			structMethodsUIData.uDelayTime = hCevent::FOREVER;
		}
		TRACE(_T(">> MS.MethodDisplay * DisplayMenu pending on button. @ 0x%08p from(0x%x)\n"),
													pMC->m_pMenuDlg->p_hCevtCntl,thisThreadID);
		pMC->m_pMenuDlg->p_hCevtCntl->pendOnEvent(structMethodsUIData.uDelayTime);

		TRACE(_T(">> MS.MethodDisplay * DisplayMenu finished pending on button.\n"));
//TRACE(">> DisplayMenu finished pending on button.\n");
	/*** moved to dialog thread - stevev 25sep07 ***
	if ( m_pMenuDlg->m_bAbortBtnClicked )
	{	structUserInputData.nComboSelection = -1;
		bReturn = false;
	}
	else
	if ( m_pMenuDlg->m_bNextBtnClicked )
	{	structUserInputData.nComboSelection = 0;
	}
	else
	if ( m_pMenuDlg->m_bButOneClicked )
	{	structUserInputData.nComboSelection = 1;
	}
	else
	if ( m_pMenuDlg->m_bButTwoClicked )
	{	structUserInputData.nComboSelection = 2;
	}
	else
	if ( m_pMenuDlg->m_bButThrClicked )
	{	structUserInputData.nComboSelection = 3;
	}
	else
	{// we got serious problems
		structUserInputData.nComboSelection = -2;
		bReturn = false;
	}
	*** end moved to dialog thread - stevev 25sep07 ***/
		structUserInputData.nComboSelection = m_buttonSelection;
		if (m_buttonSelection < 0 ) bReturn = false; 
		TRACE(_T(">> MS.MethodDisplay * combo selected %d\n"),m_buttonSelection);

//TRACE(">> DisplayMenu shows %d Button pressed.\n",structUserInputData.nComboSelection);
	//sjv 27mar07 this is done in the displaymenu thread::>RAZE( m_pMenuDlg );
	//sjv 27mar07 this is done in the displaymenu thread::>pMC = currentMethod();
	//sjv 27mar07 this is done in the displaymenu thread::>if (pMC)
	//sjv 27mar07 this is done in the displaymenu thread::>	pMC->m_pMenuDlg = NULL;

/*		if( ! bReturn ||  m_pMenuDlg->m_bAbortBtnClicked)
		{//structUserInputData.nComboSelection
			bEnableDynamicDisplay = false;
			m_pMenuDlg->m_bNextBtnClicked  = false;
			m_pMenuDlg->m_bAbortBtnClicked = false;
        	structMethodsUIData.bDisplayDynamic = false;
			//ResetEvent(m_pMethodDlg->m_hUserAckEvent);
        	structMethodsUIData.bDisplayDynamic = false;
			bReturn = false;
TRACE(">> Dialog shows Abort pressed.\n");
		}
		else
		if(m_pMenuDlg->m_bNextBtnClicked)
		{
			m_pMenuDlg->m_bNextBtnClicked = false;
			bReturn = true;
TRACE(">> Dialog shows Next pressed - getting input.\n");
        	m_pMenuDlg->GetMethodsUserInputData(structUserInputData);				
TRACE(">> Dialog shows Next pressed - finished getting input.\n");															
		}
		// else - (almost) impossible to get an event w/ no error and have both of these false
		// we won't log or anything for now
#ifdef _DEBUG
		else
		{			
TRACE(">> Dialog shows neither Abort NOR Next pressed.\n");		
		}
#endif // _DEBUG
		*/
	}

	DEBUGLOG(CLOG_LOG,">> MS.MethodDisplay <<<< Exit w/ %s @ 0x%08p (0x%x)relinquish\n",
		(bReturn)?"true":"false",this,thisThreadID);
	pDlgMutex->relinquishMutex();
	m_DialogsMtx.returnMutexControl(pDlgMutex);

	return bReturn;
}


/**********************************************************************************************
 # Function Name  : UIimageSize

 # Prototype      : bool UIimageSize(int imgIndex, unsigned& xCols, unsigned& yRows)

 # Description    : UI looks up indexed image (using active language), 
					converts its xpixels-by-ypixels to cols-by-rows
					We assume that an INLINE image is a constant size and is handled externally
					This would only be called for non-inline images.

 # Parameters     : image index from the image item,
                    the x & y return values

 # Returns        : true on error, false on no-error (all OK)

 modified 29nov10 to return various widths instead of 1 or 5.

**********************************************************************************************/


bool CMethodSupport::UIimageSize(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline)
{
	// stevev 10oct07 - Reported by paul:  if ( pDoc == NULL || imgIndex <= 0)
	if ( pDoc == NULL || imgIndex < 0)
	{
		return true;
	}

	SDClangImage* pImg = pDoc->GetImage(imgIndex);// defaults to NULL language

	if ( pImg == NULL )
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: imageSize could not find image %d.\n",imgIndex);
		return true;
	}

	DWORD HgtPixels = pImg->GetHeight();
	DWORD WidPixels = pImg->GetWidth();

	if ( isInline )
		xCols = 1;
	else
	// stevev 29nov10..no more::>	xCols = COLS_SUPPORTED;// always (since not inline)
	{
		xCols = WidPixels / COL_WIDTH;
		if ( (xCols * COL_WIDTH) < WidPixels ) 
			xCols++;
	}

	if (isInline && WidPixels > COL_WIDTH)
	{// modify the pixel height to get a good vertical
		pImg->SetDrawMode(DM_STRETCH2FITASPECT);// for the coming draw
		float dstpersrc = (float)COL_WIDTH / (float)WidPixels;
		// a guess to get a good size here
		// the following assumes an equal horiz & vert resolution- ok for a guess
		HgtPixels = (DWORD)((HgtPixels * dstpersrc) + 0.5);
		// now do setsize
	}

	if ( xCols > COLS_SUPPORTED )
	{// we have to adjust image to fit.
		pImg->SetDrawMode(DM_STRETCH2FITASPECT);// for the coming draw
		float dstpersrc = (float)(COL_WIDTH*COLS_SUPPORTED) / (float)WidPixels;
		// a guess to get a good size here
		// the following assumes an equal horiz & vert resolution- ok for a guess
		HgtPixels = (DWORD)((HgtPixels * dstpersrc) + 0.5);// round to nearest pixel height
		// now do setsize
		xCols = COLS_SUPPORTED;// added 27jul12
	}


	yRows = 1;  // has to take up at least one

	if ( HgtPixels > INITIAL_ROWHEIGHT )
	{	// there is no maximum row, add what is needed by the image
		yRows += (HgtPixels-INITIAL_ROWHEIGHT)/ADDITIONAL_ROWHEIGHT;
		// if there are any more pixels (remainder) they will take up another row
		if ((HgtPixels-INITIAL_ROWHEIGHT)%ADDITIONAL_ROWHEIGHT)
		{
			yRows++;
		}
	}
	// else - leave it one
	return false;//success
}


/**********************************************************************************************
 # Function Name  : UIstringSize

 # Prototype      : bool UIstringSize
							(string* c_str, unsigned& xCols, int heightLimit, unsigned& yRows)

 # Description    : UI looks up string from string (using active language), 
					converts its chars to lines X chars and then to cols-by-rows

 # Parameters     : (const) string from the device object,
                    the x & y return values - X always equals ONE
					heightLimit is maximum number of rows the string can take up 
					(including scroll bars)

 # Returns        : true on error, false on no-error (all OK), yRows = 0 on error

 # Notes:         changes of 24aug12 made the sizing much better but it seems to be about
					15 - 20% off in its number of characters/row selection (among other things)
**********************************************************************************************/
#ifdef _DEBUG
// #define LOG_STRSIZE 1 /* comment out to remove logging from  the following method */
#endif 

bool CMethodSupport::
	UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit,bool isDialog)
{
//	xCols = yRows = 0;
//	return true; // stub out an error

// assumes  the text on the main window will match that of the dialogs
	if ( c_str == NULL ) return true;

	xCols = yRows = 1;

	CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:
										 // was:>(CMainFrame *) AfxGetMainWnd();// to get DC
	ASSERT(::IsWindow(pMainWnd->m_hWnd));

	if (c_str->empty())// special consideration of empty constant string
	{
		//was ts = pMainDC->GetTextExtent("X");
		return false; // empty string is a 1 X 1 solution with no error
	}

	TEXTMETRIC tm;
	CSize      ts, ts2, ts3, ts4;

	CDC* pMainDC = pMainWnd->GetDC();
/*  change font to the constant string font...; */
	CFont font;
	LOGFONT LogFont;
	memset(&LogFont, 0x00, sizeof(LogFont));
	_tcscpy(LogFont.lfFaceName, CONST_STRING_FONT );
	LogFont.lfHeight = CONST_STRING_HIGT;

	font.CreateFontIndirect(&LogFont);
	// Create an in-memory DC compatible with the
	// display DC we're using to paint
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(pMainDC);

	CFont* pOrigFont = dcMemory.SelectObject( &font );
	// pMainDC->GetTextMetrics(&tm);
	dcMemory.GetTextMetrics(&tm);// all text metrics are in logical units

 // stevev - WIDE2NARROW char interface
	CString localStr;
#ifdef _UNICODE
	AsciiToUnicode(c_str->c_str(), localStr);
#else
	localStr = c_str->c_str();
#endif
// we're using form view so dialog is the same as windows
//	if (isDialog)
//	{
//		;// not sure what to do yet....
//		// needs to::> dialogDC->GetTextExtent(c_str->c_str(), c_str->length());
//	}
//	else
//	{

	/* NOTE:  Dialog units are used in dialogue box templates. One vertical dialog unit is 
	 *        1/8 of the height of the dialogue box font. One horizontal dialog unit is 
	 *        1/4 of the average width of the dialogue box font. 
	 ***/
		//ts = pMainDC->GetTextExtent(c_str->c_str(), c_str->length());// logical units
		//ts = dcMemory.GetTextExtent(c_str->c_str(), c_str->length());// logical units
		ts = dcMemory.GetTextExtent(localStr, c_str->length());// logical units		
		dcMemory.LPtoDP(&ts);// now in pixels (device units)
//	}

	// moved from below 24aug12
	int strLen    = c_str->length();
	int strLoc    = 0;
// info test
	int avgPixWid = (ts.cx / ((strLen/2) + 1)) / 2;// should equal baseunitx

	int baseUnitY = tm.tmHeight;
	int baseUnitX = min(  (int)((   ((double)ts.cx) / ((double)strLen)   ) + 0.5),  
		                  tm.tmAveCharWidth);   //aka avg-char-width

/* DPtoLP  device-units to logical-units;  LPtoDP -- both take CSize or CRect pointer */
/*// tmPitchAndFamily flags - low order 4 bits
#define TMPF_FIXED_PITCH    0x01
#define TMPF_VECTOR             0x02
#define TMPF_DEVICE             0x08
#define TMPF_TRUETYPE       0x04
#define FF_DONTCARE         (0<<4)  // Don't care or don't know. 
#define FF_ROMAN            (1<<4)  // Variable stroke width, serifed.  Times Roman,  etc. 
#define FF_SWISS            (2<<4)  // Variable stroke width, sans-serifed. Helvetica, Swiss...
#define FF_MODERN           (3<<4)  // Constant stroke width, serifed or sans-serifed.  
									//                               Pica, Elite, Courier, etc.
#define FF_SCRIPT           (4<<4)  // Cursive, etc. 
#define FF_DECORATIVE       (5<<4)  // Old English, etc. 
*/
	int origWidth, boxWidth,  
		origHight, boxHight,
		origIncHgt, boxIncHight;
	origWidth  = boxWidth =  COLUMSTEPSIZE * 3;// in dialog units
	origHight  = boxHight =  ROWSTEPSIZE;
	origIncHgt = boxIncHight=ROWSTEPSIZE + ROWGAP;
/*
	RECT convertRect;// we need to convert the box size to pixels (device units)
	convertRect.top    = convertRect.left = 0;
	convertRect.right  = boxWidth;
	convertRect.bottom = boxHight;
	MapDialogRect(pMainWnd->m_hWnd, &convertRect);
	boxWidth = convertRect.right;
	boxHight = convertRect.bottom;

	convertRect.top    = convertRect.left = 0;
	convertRect.right  = boxWidth;
	convertRect.bottom = boxIncHight;
	MapDialogRect(pMainWnd->m_hWnd, &convertRect);
	boxIncHight = convertRect.right;

	//CSize dbgSize(boxWidth,boxHight);//(cx,cy)
	//dcMemory.LPtoDP(&dbgSize);
	//boxWidth = dbgSize.cx;
	//boxHight = dbgSize.cy;
	//dbgSize.cy = boxIncHight;
	//dcMemory.LPtoDP(&dbgSize);
	//boxIncHight = dbgSize.cy;
***/
/*   Dialog units to pixels  */
	boxWidth   = MulDiv( boxWidth,    baseUnitX, 4 );
	boxHight   = MulDiv( boxHight,    baseUnitY, 8 );
	boxIncHight= MulDiv( boxIncHight, baseUnitY, 8 );

	ts2.cx = tm.tmAveCharWidth;// logical units
	ts2.cy = tm.tmHeight;	
	dcMemory.LPtoDP(&ts2);// now in pixels (device units)
	tm.tmAveCharWidth = ts2.cx;

	ts2.cx = tm.tmExternalLeading;// logical units
	ts2.cy = tm.tmHeight;	
	dcMemory.LPtoDP(&ts2);// now in pixels (device units)
	tm.tmExternalLeading = ts2.cx;

//	int avgNumCharPerLine = 1;
// we're using form view so dialog is the same as windows
//	if (isDialog)
//	{
//		;// not sure what to do yet....
//	}
//	else
//	{
	// base units are in logical units.
	// from...int avgNumCharPerLine = boxWidth/baseUnitX;// truncate, don't round
	int avgNumCharPerLine = boxWidth/tm.tmAveCharWidth;// truncate, don't round
//	}

	int wrkLen, idx;
	string wrkStr;
	const string delims(" \t\f\v");//space,tab,formfeed,verticalTab
	int usedHeightPixels = 0, availHeightPixels = boxIncHight;// boxHight
#ifdef LOG_STRSIZE
	LOGIT(CLOG_LOG,"@\tEntry String:'%s'\n",c_str->c_str());
#endif
	while ( strLen )
	{
		if (strLen >= avgNumCharPerLine)
		{
			wrkLen = avgNumCharPerLine;
		}
		else // it is >0 && < avg
		{
			wrkLen = strLen;
		}
		wrkStr = c_str->substr(strLoc,wrkLen);


		while(wrkLen)// this loop is for when the initial string is too big
		{
			idx    = wrkStr.find("\n");  // is there an embedded newline
			if (idx != string::npos ) 
			{// isa Newline..
				wrkStr = wrkStr.substr(0,idx+1);// includes \n
				wrkLen = wrkStr.length();
			}
			else // no newline, check other breaks
			if (wrkLen > avgNumCharPerLine) // we're still too long
			{// no newline:: detect correct end-of-line
				idx    = delims.find(wrkStr[wrkLen-1]);// is last char a delim? 
				if (idx == string::npos ) 
				{// no, it's a character
					idx = wrkStr.find_last_of(delims);//index of rightmost char that is a delim
					if (idx != string::npos ) 
					{// idx is the index of a delim(space/formfeed/tab
						wrkStr = wrkStr.substr(0,idx+1);// includes delim
						wrkLen = wrkStr.length();
					}
					//else.. there is no delim , use the string of characters as-is
				}
				//else its already a delim
			}
			// else the wrkStr & WrkLen is good as-is
			// we have a correct wrkStr && wrkLen
			//ts = pMainDC->GetTextExtent(wrkStr.c_str(), wrkStr.length());// logical units
			 // stevev - WIDE2NARROW char interface
	CString localStr;
#ifdef _UNICODE
	AsciiToUnicode(wrkStr.c_str(), localStr);
#else
	localStr = wrkStr.c_str();
#endif
			//ts = dcMemory.GetTextExtent(wrkStr.c_str(), wrkStr.length());// logical units
			ts = dcMemory.GetTextExtent(localStr, wrkStr.length());// logical units			
			dcMemory.LPtoDP(&ts);// now in pixels (device units)
			if ( ts.cx <= boxWidth )
			{
				usedHeightPixels += ts.cy;
				if (wrkLen < strLen )// was......yRows != 1)
				{
					usedHeightPixels += tm.tmExternalLeading;// between rows
				}
#ifdef LOG_STRSIZE
//			LOGIT(CLOG_LOG,"@\t");
			LOGIT(CLOG_LOG,"@\t'%s' is %d pixels long (of %d) & %d tall leaving %d avail.\n",
					wrkStr.c_str(),ts.cx,boxWidth,ts.cy, availHeightPixels-usedHeightPixels );
#endif
				if (usedHeightPixels > availHeightPixels)
				{
					availHeightPixels += boxIncHight;
					yRows++;
#ifdef LOG_STRSIZE
			LOGIT(CLOG_LOG,"@\tUsedRows incremented to %d. Remaining now %d\n",yRows,
														availHeightPixels-usedHeightPixels );
#endif
				}
				strLen -= wrkLen;
				strLoc += wrkLen;
				wrkLen = 0;// will not loop too long
			}
			else
			{// too big, we gotta move left and loop
				wrkStr = wrkStr.substr(0,wrkLen-1);
				wrkLen = wrkStr.length();
				// will loop on too long - 
			}
		}// wend - string too big, we moved left one
	}//wend - we still have length in the string
	
	// when done...
	dcMemory.SelectObject( pOrigFont );// probably not needed
	font.DeleteObject();

	return false;

/* I don't have the history on the following but is was discarded with the earlier return false

	int boxUnitCharHeight = (ts.cy * 8)/baseUnitY;// dialog units
	int lineCnt   = 0;
	int boxHeight = 0;
	int rowCnt    = 1;

	ts2.cx = (ts.cx * 4)/baseUnitX; // convert pixels to dialog units 
	ts2.cy = (ts.cy * 8)/baseUnitY; // char height in box(dialog) units
	int acw = (int) ((((float) ts2.cx) / ((float) strLen))+0.5);  
	// was::  (tm.tmAveCharWidth * 4)/baseUnitX;

	//convert to Pixels
	CSize pixelBoxSize;
	pixelBoxSize.cx = (boxWidth * baseUnitX) / 4;
	pixelBoxSize.cy = ( ROWSTEPSIZE * baseUnitY ) / 8;// max box height (1st row)
	int rowHeightAdder = ((ROWSTEPSIZE + ROWGAP) * baseUnitY) / 8;// Pixels

	CString locStr = c_str->c_str();
	strLen = locStr.GetLength();
	int tmpW = (int)  ( (((float)pixelBoxSize.cx)/((float)acw)) + .5);// num char per line
	CString aLine = locStr.Left(tmpW);
	int y = aLine.ReverseFind(' ');
	if ( y > 0 )
	{
		aLine = aLine.Left(y+1);
		ts3 = pMainDC->GetTextExtent(aLine);
		ts4.cx = (ts3.cx * 4)/baseUnitX; // convert pixels to dialog units
		ts4.cy = (ts3.cy * 8)/baseUnitY;
	}
	
	pMainWnd->ReleaseDC(pMainDC);
***/
/*	
pRect->left   = m_nCurColumn;
pRect->top    = m_nCurRow;
// pRect->bottom = m_nCurRow    + ( ( pBase->rectSize.yval) * ( ROWSTEPSIZE + ROWGAP ) );
pRect->bottom = m_nCurRow + ((8 * 2) * pBase->rectSize.yval);  
												// 2 text lines per row, 8 pixels in height 
pRect->right  = -10 + m_nCurColumn + ( ( pBase->rectSize.xval) * ( COLUMSTEPSIZE * 3  ) );
  -10 + m_nCurColumn + ( ( pBase->rectSize.xval) * ( COLUMSTEPSIZE * 3  ) ) - m_nCurColumn
= -10 + ( ( pBase->rectSize.xval) * ( COLUMSTEPSIZE * 3  ) )	// assume width = 1 col
= -10 + ( COLUMSTEPSIZE * 3 );
  m_nCurRow + ((8 * 2) * pBase->rectSize.yval) - m_nCurRow
= ((8 * 2) * pBase->rectSize.yval)							// assume ( to start) 1 row tall
= (8 * 2)
OR
  m_nCurRow    + ( ( pBase->rectSize.yval) * ( ROWSTEPSIZE + ROWGAP ) ) - m_nCurRow
= ( ( pBase->rectSize.yval) * ( ROWSTEPSIZE + ROWGAP ) )	// assume ( to start) 1 row tall
= ROWSTEPSIZE + ROWGAP = total height of the area
*/
	return true;// erroneous...
}

/*
int offset = rectClient.bottom; 
CFont *pOldFont = pDC->SelectObject(&m_font);
CSize size = pDC->GetTextExtent(_T("TEST"));
pDC->SelectObject(pOldFont);
while(pos)
*/