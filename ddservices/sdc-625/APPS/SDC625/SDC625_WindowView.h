#if !defined(AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_)
#define AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SDC625_WindowView.h : header file
//

#include "StdAfx.h"
#include "Window_Map.h"
#include "ddbDefs.h"
#include "SDC625_drawTypes.h"
#include "Dyn_Controls.h"

#include "logging.h"

/////////////////////////////////////////////////////////////////////////////
// CSDC625WindowView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CSDC625WindowView : public CFormView//,  CdrawWin
{
protected:
	CSDC625WindowView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSDC625WindowView)

	ulong mySymNum;		/*stevev 8/25/o4 expansion*/
	CPoint m_point;
	CToolTipCtrl m_ToolTip;
	BOOL m_bTracking;
	CdrawBase* m_pHelp;

// Form Data
public:
	//{{AFX_DATA(CSDC625WindowView)
	enum { IDD = IDD_SDC625_FORM };
	//}}AFX_DATA

// Attributes
public:
	CDDIdeDoc* GetDocument();

// Operations
public:
	CFrameWnd* m_pWindowFrame;
	CdrawWin * m_pDrawWin;	// This point to the CdrawWin object that invoked the frame window

	void Initialize(ulong id);
	typedef map< UINT, CWindowMap > IdWindowMap;
	IdWindowMap m_WndCntrlMap;
	
	void SetToolTipText(CString* spText, BOOL bActivate = TRUE);
	void layoutWindow(void);// sizes and locates
	void DestroySymbol(ulong symblID);
	void Rebuild(void); // replace all in window with re-layed out controls

protected:
	void InitToolTip();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDC625WindowView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);  // Added OnNotify, POB - 5/5/2014
	//}}AFX_VIRTUAL

#ifdef FNDMSG
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam )
{if (message==FNDMSG){LOGIT(CLOG_LOG,"~%");} return CFormView::WindowProc(message,wParam,lParam);};
#endif

// Implementation
protected:
	virtual ~CSDC625WindowView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSDC625WindowView)
	afx_msg void OnDestroy();
	afx_msg void OnChangeEdit2();
	afx_msg void OnMenuLabel();
	afx_msg void OnMenuDisplayValue();	
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnMouseHover(WPARAM wparam, LPARAM lparam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMenuHelp();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	BOOL m_isCntrlsReady;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDC625WINDOWVIEW_H__3FDA3CB2_7F38_4CEC_846C_89BDB7F37693__INCLUDED_)
