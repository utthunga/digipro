/*************************************************************************************************
 *
 * $Workfile: EDisplayDlg.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      EDisplayDlg.cpp : implementation file
 */

#include "stdafx.h"
#include "SDC625.h"
#include "DDLEDisplay.h"
#include "EDisplayDlg.h"
#include "VariableDlg.h"
#include "SDC625ListView.h" // Column definitions
#include "SDC625Doc.h"
#include "WaoDlg.h"
#include "LabelDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifndef _WIN32_WCE
 #define    ACTIVATE_CLAUSE     NMITEMACTIVATE
 #define ACTIVE_BUTTONS     TPM_LEFTALIGN | TPM_RIGHTBUTTON
#else   // CE    no right button 29/04/09 PAW 
 #define    ACTIVATE_CLAUSE     NMLISTVIEW
 #define ACTIVE_BUTTONS     TPM_LEFTALIGN 
#endif
/////////////////////////////////////////////////////////////////////////////
// CEDisplayDlg dialog


CEDisplayDlg::CEDisplayDlg(CDDLEDisplay* pEDsply, CWnd* pParent /*=NULL*/)
    : CDialog(CEDisplayDlg::IDD, pParent)
{
    ASSERT(pEDsply != NULL);

    //{{AFX_DATA_INIT(CEDisplayDlg)
    //}}AFX_DATA_INIT

    m_pEDsply= pEDsply;
    m_Test = 0;

}
void CEDisplayDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CEDisplayDlg)
    DDX_Control(pDX, IDC_EDIT_LIST, m_cntlEdit);
    DDX_Control(pDX, IDC_DSPLY_LIST, m_cntlDsply);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEDisplayDlg, CDialog)
    //{{AFX_MSG_MAP(CEDisplayDlg)
    ON_WM_DESTROY()
    ON_NOTIFY(NM_RCLICK, IDC_EDIT_LIST, OnEditRclick)
    ON_NOTIFY(NM_RCLICK, IDC_DSPLY_LIST, OnDsplyRclick)
    ON_WM_TIMER()
    ON_NOTIFY(NM_DBLCLK, IDC_EDIT_LIST, OnDblclkEditList)
#ifdef GE_BUILD
    ON_NOTIFY(NM_CLICK, IDC_EDIT_LIST, OnDblclkEditList)// 00.01.11
#endif
    ON_COMMAND(ID_MENU_EXECUTE, OnMenuExecute)
    ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
    ON_UPDATE_COMMAND_UI(ID_MENU_EXECUTE, OnUpdateMenuExecute)
    /*ON_WM_ENABLE()*/
    //}}AFX_MSG_MAP
    ON_WM_INITMENUPOPUP()   // Need this since ON_UPDATE_COMMAND_UI is not supported in CDialog for pop-up menus.
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEDisplayDlg message handlers

BOOL CEDisplayDlg::OnInitDialog() 
{
#ifdef GE_BUILD
    SetCursor(LoadCursor(NULL, IDC_WAIT));  // add hourglass cursor PAW 23/07/09
    ShowCursor(TRUE);   // add hourglass cursor PAW 23/07/09
#endif
    CDialog::OnInitDialog();
    
    int       nRow = 0;
    LV_ITEM   lv_item;
    CString   strTitle, strTemp;
    
    /*
     * set the CListCtrl attributes
     */
 //   m_cntlDsply.SetExtendedStyle(LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SINGLESEL); this is interesting save


    /*
     * Create the Image List
     */
/*    m_ctlImage.Create(IDB_IMAGELIST,16,0,RGB(255,0,255));
    m_ctlImage.SetBkColor(GetSysColor(COLOR_WINDOW)); */  //Vibhor 130603 : Commented the code

    // Vibhor 130603 : Start of Code
    m_ctlImage.Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    CBitmap bmp;
    bmp.LoadBitmap(IDB_IMAGELIST);
    
    m_ctlImage.Add(&bmp, RGB(255,0,255));
    
    // Vibhor 130603 : End of Code

    /*
     * Attach images to List
     */
    m_cntlDsply.SetImageList(&m_ctlImage, LVSIL_SMALL);
    m_cntlEdit.SetImageList(&m_ctlImage, LVSIL_SMALL);
    strTitle = "Item";
    m_cntlDsply.InsertColumn( COL_NAME, strTitle, LVCFMT_LEFT, 100, 0);
    m_cntlEdit.InsertColumn( COL_NAME, strTitle, LVCFMT_LEFT, 100, 0);
    strTitle = "Value";
    m_cntlDsply.InsertColumn( COL_DATA, strTitle, LVCFMT_RIGHT, 100, 0);
    m_cntlEdit.InsertColumn( COL_DATA, strTitle, LVCFMT_RIGHT, 100, 0);
    strTitle = "Unit";
    m_cntlDsply.InsertColumn( COL_UNITS, strTitle, LVCFMT_LEFT, 100, 0);
    m_cntlEdit.InsertColumn( COL_UNITS, strTitle, LVCFMT_LEFT, 100, 0);

    /*
     * How many objects in this CList? Not sure since there are two lists
     */
//  m_cntlDsply.SetItemCount( m_pEDsply->m_ChildList.GetCount() );

    /*
     * Initialize the LV_ITEM structure that's used to populate the list
     */
    lv_item.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
    lv_item.iSubItem = COL_NAME;

    vector<hCVar*> varList;// we own the ptrs, not what they point to
    //string ty;
    wstring ty; // for UTF-8 support, POB - 1 Aug 2008

    hCeditDisplay *pEDPtr = m_pEDsply->baseItemPtr();
#ifdef GE_BUILD
    number_of_items = 0;            // 00.01.10
#endif
    if (pEDPtr->aquireEditList(varList) == 0)
    {
        //TO FIX PAR 5244
        //we need to do a reverse iteration here because the list of variables is coming in the reverse
        //order of that in the DD.
        for (vector<hCVar*>::reverse_iterator iT = varList.rbegin(); iT < varList.rend(); iT++)
        {
            CDDLVariable *pVar = new CDDLVariable(*iT);

            if (pVar->IsVariable())
            {
                // Make sure the Variable is valid before inserting it into list
                if (pVar->IsValid())
                {
                    // Determine which list it belongs to
                //  if(!pVar->IsReadOnly())
                    if(1)
                    {
                        //This should be an edit item
                        ((hCitemBase *)*iT)->Label(ty);
                        strTemp = ty.c_str();
                        pVar->SetName(strTemp);
#ifdef GE_BUILD
// 00.01.10 revise dialog box name
                        if (number_of_items < 3)
                        {   
                            if (number_of_items == 0)
                                dialog_title = _T("Edit ") + strTemp;
                            else
                                dialog_title += _T(" & ") + strTemp;
                            number_of_items++;
                        }
// 00.01.10 end
#endif
                        lv_item.iItem = 0; 
                        lv_item.pszText = (LPTSTR)(LPCTSTR)strTemp;
                        lv_item.lParam = (LPARAM)pVar;
                        lv_item.iImage = pVar->GetImage(); // Added function to get image ID; POB - 5/16/2014
                        nRow = m_cntlEdit.InsertItem(&lv_item);
    
                        /* stevev 18may09 - special operations are not required...
                        /* Removed commented out code, POB - 5/20/2014 */
                        strTemp = *pVar;   // Update data

                        m_cntlEdit.SetItem(nRow,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
                        strTemp = pVar->GetUnitStr();
                        m_cntlEdit.SetItem(nRow,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);

                    }
                    else /*Vibhor 310304: Start of Code*/
                    { // Temporary fix for PAR 5345 : Just logging this codition for developers
                        unsigned long uVarID = pVar->GetID();
                        unsigned long uEditDispID = ((hCitemBase*)pEDPtr)->getID();
                        LOGIT(CERR_LOG|UI_LOG,"0x%04x is a Read Only item in edit list of EditDisplay 0x%04x !!! \n",uVarID,uEditDispID);
                    }
                    /*Vibhor 310304: End of Code*/
                }
            
            }

        }
        varList.clear();
    }
#ifdef GE_BUILD
// 00.01.10 start
    SetWindowText(dialog_title);
// 00.01.10 end
#endif
    if ( pEDPtr->aquireDispList(varList) == 0)
    {
        //TO FIX PAR 5244
        //we need to do a reverse iteration here because the list of variables is coming in the reverse
        //order of that in the DD.
        for (vector<hCVar*>::reverse_iterator iT = varList.rbegin(); iT < varList.rend(); iT++)
        {
            CDDLVariable *pVar = new CDDLVariable(*iT);

            if (pVar->IsVariable())
            {
                // Make sure the Variable is valid before inserting it into list
                if (pVar->IsValid())
                {
                    // Determine which list it belongs to
                //  if(pVar->IsReadOnly())
                //  {
                        //This should be a display item
                        //This should be an edit item
                        ((hCitemBase *)*iT)->Label(ty);
                        strTemp = ty.c_str();

                        pVar->SetName(strTemp);

                        lv_item.iItem = 0; //Zero - Last in, last in the list  //nIdx
                        lv_item.pszText = (LPTSTR)(LPCTSTR)strTemp;
                        lv_item.lParam = (LPARAM)pVar;
                        lv_item.iImage = pVar->GetImage(); // Added function to get image ID; POB - 5/16/2014
                        nRow = m_cntlDsply.InsertItem(&lv_item);

                        /* stevev 18may09 - special operations are not required...
                        // Check for type Enum
                        vector<EnumTriad_t> eList;
                        
                        if (pVar->GetType() == TYPE_ENUM)
                        {
                            if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                            {
                                int nTemp = *pVar;
                                strTemp = pVar->GetEnumString(nTemp);
                            }
                        }
                        else
                        *******************/
                        {
                            strTemp = *pVar;   // Update data
                        }

                        m_cntlDsply.SetItem(nRow,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
                        strTemp = pVar->GetUnitStr();
                        m_cntlDsply.SetItem(nRow,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
                    //}
                }
            
            }

        }

        varList.clear();
    }

	wstring s;
	pEDPtr->Label(s);

	CString Label = s.c_str();
	SetWindowText(Label);

    /* VMKP Added code to get the values of the variables immediately after 
      invoking the Edit dialog box (on 200104) */
    // Read the values before updating
    CDDIdeDoc *pDoc = GetDocument();

    if (pDoc != NULL)
    {
        CDDLVariable* pVar  = NULL;
        CDDLBase* pBase     = NULL;
        vector <hCitemBase*> pVariablesToRead;

        // Read the values before updating
        CDDIdeDoc *pDoc = GetDocument();

        int nCount = m_cntlDsply.GetItemCount();
        int i;  // PAW 03/03/09
        for (/*int PAW 03/03/09 */ i=0;i < nCount;i++)
        {
            pVar = (CDDLVariable *)m_cntlDsply.GetItemData(i);

            if (pVar->IsVariable())
            {
                pVariablesToRead.push_back((hCitemBase *)pVar->m_pddbVar);

                strTemp = pVar->GetName();
                m_cntlDsply.SetItem( i, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);

                /* stevev 18may09 - special operations are not required...
                // Check for type Enum
                vector<EnumTriad_t> eList;
                
                if (pVar->GetType() == TYPE_ENUM)
                {
                    if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                    {
                        int nTemp = *pVar;
                        strTemp = pVar->GetEnumString(nTemp);
                    }
                }
                else
                ****************************/
                {
                    strTemp = *pVar;   // Update data
                }

                m_cntlDsply.SetItem(i,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
                strTemp = pVar->GetUnitStr();
                m_cntlDsply.SetItem(i,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
            }
        }    

        nCount = m_cntlEdit.GetItemCount();

        for (i=0;i < nCount;i++)
        {
            pVar = (CDDLVariable *)m_cntlEdit.GetItemData(i);

            if (pVar->IsVariable())
            {
                pVariablesToRead.push_back((hCitemBase *)pVar->m_pddbVar);

                strTemp = pVar->GetName();
                m_cntlEdit.SetItem( i, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);

                /* stevev 18may09 - special operations are not required...
                // Check for type Enum
                vector<EnumTriad_t> eList;
                
                if (pVar->GetType() == TYPE_ENUM)
                {
                    if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                    {
                        int nTemp = *pVar;
                        strTemp = pVar->GetEnumString(nTemp);
                    }
                }
                else
                **************************************/
                {
                    strTemp = *pVar;   // Update data
                }

                m_cntlEdit.SetItem(i,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
                strTemp = pVar->GetUnitStr();
                m_cntlEdit.SetItem(i,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
            }
        }    

#ifndef GE_BUILD
            pDoc->ReadVariables(pVariablesToRead);
            /* Do the Service Reads */
            pDoc->pCurDev->pCmdDispatch->ServiceReads();
#else
            //PO using the above is very slow
            pDoc->ReadVariablesAsCritical(pVariablesToRead);
#endif  
            UpdateData(FALSE);
    }

    /* VMKP Modified on 200104*/

    /*
     * Start a timer to update the Variables once a second.
     */
#ifndef _WIN32_WCE
    //int result = SetTimer(EDSPLY_TIMER, 2000, NULL);
    int result = SetTimer(EDSPLY_TIMER, 5000, NULL);
    if (!result)
        MessageBox(M_UI_NO_TIMER);
#else  
    SetCursor(NULL);    // add hourglass cursor PAW 23/07/09      
#endif  


    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}


void CEDisplayDlg::OnDestroy() 
{
    CDialog::OnDestroy();
    int nCount; // PAW 03/03/09
    // Clean up
    for (/*int PAW 03/03/09 */nCount = 0; nCount < m_cntlEdit.GetItemCount(); nCount++)
    {
        CDDLVariable *pVar = (CDDLVariable *)m_cntlEdit.GetItemData(nCount);
        if (pVar)
        {
            delete pVar;
            pVar = NULL;
        }
    }

    // Clean up
    for (nCount = 0; nCount < m_cntlDsply.GetItemCount(); nCount++)
    {
        CDDLVariable *pVar = (CDDLVariable *)m_cntlDsply.GetItemData(nCount);
        if (pVar)
        {
            delete pVar;
            pVar = NULL;
        }
    }
    
    KillTimer(EDSPLY_TIMER);
 }

void CEDisplayDlg::OnEditRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
    CDDLBase*      pItem;
    CMenu          menu;
    CMenu*         pSubmenu;
    CPoint         ptClient;
    CDDIdeDoc * pDoc = NULL;
    LV_HITTESTINFO HitTestInfo;
    int nPos;

    ACTIVATE_CLAUSE* plistItem = (ACTIVATE_CLAUSE*)pNMHDR;
    /*
     * Get the mouse position associated with this message
     * and see if it is on a tree item.
     */
 
    /*
     * Retrieve the class pointer for this item,
     * then use it to fill the list Control
     */
    int index = plistItem->iItem; // Get the index where this item is in control

    DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));
    ptClient = point;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
//    index = m_cntlEdit.HitTest( &HitTestInfo ); /* Hit test not working outside CListView*/
//    if( LVHT_ONITEM & HitTestInfo.flags )
    if (index != -1)
    {
        /*
         * The click was on a list item.
         * Get the CDDLBase pointer from the selected object
         * and have it load the menu.
         */
        pItem = (CDDLBase *)m_cntlEdit.GetItemData( index );
        pItem->LoadContextMenu( &menu, nPos ) ;

        if( pItem )
        {
            pDoc = GetDocument();
            pDoc->m_pMenuSource = pItem;
            pSubmenu = menu.GetSubMenu(nPos);
            pSubmenu->TrackPopupMenu( ACTIVE_BUTTONS,
                                      point.x, point.y, this);
        }
    }

    /*
     * Set *pResult to 0 if you want MFC's default call to OnRClick which will result
     * in a call to OnContextMenu() as well which we don't want here, so set to 1.
     */
    *pResult = 1;
}

void CEDisplayDlg::OnDsplyRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
    CDDLBase*      pItem;
    CMenu          menu;
    CMenu*         pSubmenu;
    CPoint         ptClient;
//CDDIdeDoc * pDoc = NULL;
    LV_HITTESTINFO HitTestInfo;
    int nPos;

    ACTIVATE_CLAUSE* plistItem = (ACTIVATE_CLAUSE*)pNMHDR;
    /*
     * Get the mouse position associated with this message
     * and see if it is on a tree item.
     */
 
    /*
     * Retrieve the class pointer for this item,
     * then use it to fill the list Control
     */
    int index = plistItem->iItem; // Get the index where this item is in control

    DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));
    ptClient = point;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
//    index = m_cntlEdit.HitTest( &HitTestInfo ); /* Hit test not working outside CListView*/
//    if( LVHT_ONITEM & HitTestInfo.flags )
    if (index != -1)
    {
        /*
         * The click was on a list item.
         * Get the CDDLBase pointer from the selected object
         * and have it load the menu.
         */
        pItem = (CDDLBase *)m_cntlDsply.GetItemData( index );
        pItem->LoadContextMenu( &menu, nPos ) ;

        if( pItem )
        {
//            pDoc = GetDocument();
//            pDoc->m_pMenuSource = pItem;
            pSubmenu = menu.GetSubMenu(nPos);
            // stevev 08oct10:was:> pSubmenu->TrackPopupMenu( ACTIVE_BUTTONS,
            //                          point.x, point.y, AfxGetMainWnd() ); 
            pSubmenu->TrackPopupMenu( ACTIVE_BUTTONS,
                                      point.x, point.y, GETMAINFRAME );
        }
    }

    /*
     * Set *pResult to 0 if you want MFC's default call to OnRClick which will result
     * in a call to OnContextMenu() as well which we don't want here, so set to 1.
     */
    *pResult = 1;
}

CDDIdeDoc* CEDisplayDlg::GetDocument()
{
    CDDIdeDoc * pDoc = NULL;
    /*
     * Get pointer to document if not already done so
     */
    CDocManager * pManager = AfxGetApp()->m_pDocManager;
    ASSERT ( pManager != NULL ); 

    POSITION posTemplate = pManager->GetFirstDocTemplatePosition();
    while ( posTemplate !=NULL )
    {
        // get the next template
        CDocTemplate * pTemplate = pManager->GetNextDocTemplate(posTemplate);
        POSITION posDoc = pTemplate->GetFirstDocPosition();
        while( posDoc !=NULL )
        {
            pDoc = (CDDIdeDoc *)pTemplate->GetNextDoc(posDoc);
        }
    }

    return pDoc;
}

void CEDisplayDlg::OnTimer(UINT nIDEvent) 
{
//  MessageBeep(0xFFFFFFFF);   // Beep test
    
    CDDLVariable* pVar  = NULL;
    CDDLBase* pBase     = NULL;
    CString strTemp;
    vector <hCitemBase*> pVariablesToRead;

    // Read the values before updating
    CDDIdeDoc *pDoc = GetDocument();

    if (pDoc == NULL)
        return;

    int nCount = m_cntlDsply.GetItemCount();
    int i;  // PAW 03/03/09
    for (/*int PAW 03/03/09*/ i=0;i < nCount;i++)
    {
        pVar = (CDDLVariable *)m_cntlDsply.GetItemData(i);

        if (pVar->IsVariable())
        {   /* VMKP Modified on 200104 */
            if(pVar->IsDynamic())
        {
            pVariablesToRead.push_back((hCitemBase *)pVar->m_pddbVar);
            }
            /* VMKP Modified on 200104 */
            strTemp = pVar->GetName();
            m_cntlDsply.SetItem( i, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);

            /* stevev 18may09 - special operations are not required...
            // Check for type Enum
            vector<EnumTriad_t> eList;
            
            if (pVar->GetType() == TYPE_ENUM)
            {
                if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                {
                    int nTemp = *pVar;
                    strTemp = pVar->GetEnumString(nTemp);
                }
            }
            else
            *************************************/
            {
                strTemp = *pVar;   // Update data
            }

            m_cntlDsply.SetItem(i,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
            strTemp = pVar->GetUnitStr();
            m_cntlDsply.SetItem(i,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
        }
    }    

    nCount = m_cntlEdit.GetItemCount();

    for (i=0;i < nCount;i++)
    {
        pVar = (CDDLVariable *)m_cntlEdit.GetItemData(i);

        if (pVar->IsVariable())
        {/* VMKP Modified on 200104 */
            if(pVar->IsDynamic())
        {
            pVariablesToRead.push_back((hCitemBase *)pVar->m_pddbVar);
            }
            /* VMKP Modified on 200104 */
            strTemp = pVar->GetName();
            m_cntlEdit.SetItem( i, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);

            /* stevev 18may09 - special operations are not required...
            // Check for type Enum
            vector<EnumTriad_t> eList;
            
            if (pVar->GetType() == TYPE_ENUM)
            {
                if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
                {
                    int nTemp = *pVar;
                    strTemp = pVar->GetEnumString(nTemp);
                }
            }
            else
            *************************************/
            {
                strTemp = *pVar;   // Update data
            }

            m_cntlEdit.SetItem(i,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
            strTemp = pVar->GetUnitStr();
            m_cntlEdit.SetItem(i,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
        }
    }    


    if (pDoc)
    {
        pDoc->ReadVariables(pVariablesToRead);
        /* Do the Service Reads */
        pDoc->pCurDev->pCmdDispatch->ServiceReads();
    }

    UpdateData(FALSE);

    /* VMKP commented on 200104 */
    CDialog::OnTimer(nIDEvent);
}


void CEDisplayDlg::Update(CDDLBase *pBase)
{
    CDDLMenuItem* pItem = ( CDDLMenuItem* )pBase;
    CString strTemp;
    LVFINDINFO lvf;
    int  nIdx;

    lvf.flags = LVFI_PARAM;
    lvf.lParam = (LPARAM)pItem;

    nIdx = m_cntlEdit.FindItem( &lvf, -1 );
    if( nIdx != -1 && pBase->IsVariable())
    {
        CDDLVariable *pVar = (CDDLVariable *) pBase;

        strTemp = pItem->GetName();
        m_cntlEdit.SetItem( nIdx, COL_NAME, LVIF_TEXT, strTemp ,0,0,0,0);

        /* stevev 18may09 - special operations are not required...
        vector<EnumTriad_t> eList;
        if (pVar->GetType() == TYPE_ENUM)
        {
            if (pVar->GetEnumList(eList) == SUCCESS && eList.size() > 0 )
            {
                int nTemp = *pVar;
                strTemp = pVar->GetEnumString(nTemp);
            }
        }
        else
        **************************/
        {
            strTemp = *pVar;   // Update data
        }

        m_cntlEdit.SetItem(nIdx,COL_DATA ,LVIF_TEXT, strTemp, 0,0,0,0);
        strTemp = pItem->GetUnitStr();
        m_cntlEdit.SetItem(nIdx,COL_UNITS, LVIF_TEXT, strTemp ,0,0,0,0);
    }
}

void CEDisplayDlg::OnDblclkEditList(NMHDR* pNMHDR, LRESULT* pResult) 
{
    POSITION pos = m_cntlEdit.GetFirstSelectedItemPosition();
    //RETURNCODE rc;
    if (pos != NULL)
    {
        int nItem = m_cntlEdit.GetNextSelectedItem(pos);
        if (nItem != -1)
        {
            CDDLVariable *pVar = (CDDLVariable *)m_cntlEdit.GetItemData(nItem);
            CDDLBase  *pParent = pVar->GetParent();
            BOOL bRO = 0;
            if (pParent && pParent->IsReadOnly() ) bRO = 1;
            if (!pVar->IsReadOnly())
            {           
                // This is a defect - The EDIT_DISPLAY pre-edit's action was already called
                // in CDDLEDisplay::Execute(). //Do not call it again here.
                // If the pre-edit's actiosn fail, then this dialog will never be invoked.
                //
                // The purpose of calling OnDblclkEditList() is to invoke the VARIABLEs
                // Edit Dialog so that the variable can be edited.  If the VARIABLE has any
                // pre-edit actions then it will be handled in WM_DO_EXECUTE below,  POB - 27 Aug 2008
                
                /*if ( m_pEDsply != NULL && m_pEDsply->baseItemPtr() != NULL )
                {
                    if (m_pEDsply->baseItemPtr()->doPreEditActs() != SUCCESS )
                    {// we don't display ! 
                        return;
                    }
                }*/

                // If this VARIABLE is in a write as one, we need to
                // call the write as one dialog, POB - 27 Aug 2008
                ITEM_ID lWaoRel = pVar->m_pddbVar->getWaoRel();

                if (lWaoRel > 0)
                {
//#define USE_TAG_POLL_IN_SDC625// PAW 04/09/09
#ifdef USE_TAG_POLL_IN_SDC625// PAW 04/09/09
                    signed char select_address; // PAW 13/05/09
                    CWaoDlg WaoDlg(pVar->m_pddbVar, lWaoRel, 0, &select_address);// mode & select_address replaced 04/09/09 PAW
#else
                    CWaoDlg WaoDlg(pVar->m_pddbVar, lWaoRel);
#endif
                    int nRet = WaoDlg.DoModal();
                }
                else
                {
                    // stevev 08oct10:was:>
                    //AfxGetMainWnd()->SendMessage(WM_DO_EXECUTE, bRO,(LPARAM)pVar->m_pddbVar);
                    GETMAINFRAME->SendMessage(WM_DO_EXECUTE, bRO,(LPARAM)pVar->m_pddbVar);
                }

                // This is a defect - The EDIT_DISPLAY post-edit's action will be called
                // in CDDLEDisplay::Execute() when this dialog closes.  Do not call it again here, POB - 27 Aug 2008

                /*if ( m_pEDsply != NULL && m_pEDsply->baseItemPtr() != NULL )
                {
                    m_pEDsply->baseItemPtr()->doPstEditActs();//return code doesn't matter
                }*///                                               we're leaving anyway
/************************************************************************************
                if ( pParent ) 
                    bRO = pParent->IsReadOnly(); 
                CVariableDlg dlg(pVar->m_pddbVar, bRO);
                //call the pre edit action method
                pVar->m_pddbVar->doPreEditActs();
                int nRet = dlg.DoModal();

                // stevev 5aug08 - if the pre-edit action ran, the post edit action must run 
                        //call post edit action
        //stevev 22aug07-par727-do_always           if(pVar->m_pddbVar->isChanged())
        //          {
                rc = pVar->m_pddbVar->doPstEditActs();
                if(rc != SUCCESS)
                {
                    LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
                    //return ;
                }

                if(IDOK == nRet)
                {                   
                    rc = pVar->m_pddbVar->doPstUserActs();
                    if(rc != SUCCESS)
                    {
                        LOGIT(CERR_LOG|UI_LOG,"Call to post user edit "
                                                                "action returned an error.\n");
                    }
                }
******************************************************************************************/
                Update(pVar);
            }
        }
    }
    
    *pResult = 0;
}

void CEDisplayDlg::OnMenuExecute() 
{
    LRESULT Result;

    OnDblclkEditList(NULL, &Result);
}

void CEDisplayDlg::OnUpdateMenuExecute(CCmdUI* pCmdUI) 
{
    POSITION pos = m_cntlEdit.GetFirstSelectedItemPosition();
    
    int nItem = m_cntlEdit.GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_cntlEdit.GetItemData(nItem);
        
        if (pBase)
        {
            pBase->OnUpdateMenuExecute(pCmdUI);
        }
    }
}

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
void CEDisplayDlg::OnMenuHelp() 
{
    CString strItemCaption;

    // New code added by Jeyabal

    POSITION pos = m_cntlEdit.GetFirstSelectedItemPosition();
    int nItem = m_cntlEdit.GetNextSelectedItem(pos);

    if (nItem != -1)
    {
        CDDLBase *pBase = (CDDLBase *)m_cntlEdit.GetItemData(nItem);
        CString strName = pBase->GetName();
        CString strHelp = pBase->GetHelp();

        CHelpBox helpBox(strName, strHelp);
        helpBox.Display();
    }
    /*   code removed 13aug08 */
}

// CDialog based classes do not support ON_UPDATE_COMMAND_UI for pop-up menus.
// Must add code to support the enabling/disabling of menu items
void CEDisplayDlg::OnInitMenuPopup(CMenu *pPopupMenu, UINT nIndex,BOOL bSysMenu)
{
    ASSERT(pPopupMenu != NULL);
    // Check the enabled state of various menu items.

    CCmdUI state;
    state.m_pMenu = pPopupMenu;
    ASSERT(state.m_pOther == NULL);
    ASSERT(state.m_pParentMenu == NULL);

    // Determine if menu is popup in top-level menu and set m_pOther to
    // it if so (m_pParentMenu == NULL indicates that it is secondary popup).
    HMENU hParentMenu;
    if (AfxGetThreadState()->m_hTrackingMenu == pPopupMenu->m_hMenu)
        state.m_pParentMenu = pPopupMenu;    // Parent == child for tracking popup.
    else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
    {
        CWnd* pParent = this;
           // Child windows don't have menus--need to go to the top!
        if (pParent != NULL &&
           (hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
        {
           int nIndexMax = ::GetMenuItemCount(hParentMenu);
           for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
           {
            if (::GetSubMenu(hParentMenu, nIndex) == pPopupMenu->m_hMenu)
            {
                // When popup is found, m_pParentMenu is containing menu.
                state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
                break;
            }
           }
        }
    }

    state.m_nIndexMax = pPopupMenu->GetMenuItemCount();
    for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
      state.m_nIndex++)
    {
        state.m_nID = pPopupMenu->GetMenuItemID(state.m_nIndex);
        if (state.m_nID == 0)
           continue; // Menu separator or invalid cmd - ignore it.

        ASSERT(state.m_pOther == NULL);
        ASSERT(state.m_pMenu != NULL);
        if (state.m_nID == (UINT)-1)
        {
           // Possibly a popup menu, route to first item of that popup.
           state.m_pSubMenu = pPopupMenu->GetSubMenu(state.m_nIndex);
           if (state.m_pSubMenu == NULL ||
            (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
            state.m_nID == (UINT)-1)
           {
            continue;       // First item of popup can't be routed to.
           }
           state.DoUpdate(this, TRUE);   // Popups are never auto disabled.
        }
        else
        {
           // Normal menu item.
           // Auto enable/disable if frame window has m_bAutoMenuEnable
           // set and command is _not_ a system command.
           state.m_pSubMenu = NULL;
           state.DoUpdate(this, FALSE);
        }

        // Adjust for menu deletions and additions.
        UINT nCount = pPopupMenu->GetMenuItemCount();
        if (nCount < state.m_nIndexMax)
        {
           state.m_nIndex -= (state.m_nIndexMax - nCount);
           while (state.m_nIndex < nCount &&
            pPopupMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
           {
            state.m_nIndex++;
           }
        }
        state.m_nIndexMax = nCount;
    }
} 

/*************************************************************************************************
 *
 *   $History: EDisplayDlg.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
#ifdef GE_BUILD
// PAW revise selection to remove need for double click 24/07/09
//void CEDisplayDlg::OnDblclkEditList1()//00.01.11
void CEDisplayDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
#ifdef USED // call a different way - this causes BIG problems PAW 20/09/09 00.01.07
// old code crashed after the CVariableDlg box was closed PAW 20/09/09 00.01.07
    NMHDR* pNMHDR;
    LRESULT* pResult;
    OnDblclkEditList(pNMHDR, pResult);
#else
    POSITION pos = m_cntlEdit.GetFirstSelectedItemPosition();
    //RETURNCODE rc;
    if (pos != NULL)
    {
        int nItem = m_cntlEdit.GetNextSelectedItem(pos);
        if (nItem != -1)
        {
            CDDLVariable *pVar = (CDDLVariable *)m_cntlEdit.GetItemData(nItem);
            CDDLBase  *pParent = pVar->GetParent();
            BOOL bRO = 0;
            if (pParent && pParent->IsReadOnly() ) bRO = 1;
            if (!pVar->IsReadOnly())
            {           
                // This is a defect - The EDIT_DISPLAY pre-edit's action was already called
                // in CDDLEDisplay::Execute(). //Do not call it again here.
                // If the pre-edit's actiosn fail, then this dialog will never be invoked.
                //
                // The purpose of calling OnDblclkEditList() is to invoke the VARIABLEs
                // Edit Dialog so that the variable can be edited.  If the VARIABLE has any
                // pre-edit actions then it will be handled in WM_DO_EXECUTE below,  POB - 27 Aug 2008
                
                /*if ( m_pEDsply != NULL && m_pEDsply->baseItemPtr() != NULL )
                {
                    if (m_pEDsply->baseItemPtr()->doPreEditActs() != SUCCESS )
                    {// we don't display ! 
                        return;
                    }
                }*/

                // If this VARIABLE is in a write as one, we need to
                // call the write as one dialog, POB - 27 Aug 2008
                ITEM_ID lWaoRel = pVar->m_pddbVar->getWaoRel();

                if (lWaoRel > 0)
                {
                    CWaoDlg WaoDlg(pVar->m_pddbVar, lWaoRel);
                    int nRet = WaoDlg.DoModal();
                }
                else
                {
                    AfxGetMainWnd()->SendMessage(WM_DO_EXECUTE, bRO,(LPARAM)pVar->m_pddbVar);
                }

                // This is a defect - The EDIT_DISPLAY post-edit's action will be called
                // in CDDLEDisplay::Execute() when this dialog closes.  Do not call it again here, POB - 27 Aug 2008

                /*if ( m_pEDsply != NULL && m_pEDsply->baseItemPtr() != NULL )
                {
                    m_pEDsply->baseItemPtr()->doPstEditActs();//return code doesn't matter
                }*///                                               we're leaving anyway
/************************************************************************************
                if ( pParent ) 
                    bRO = pParent->IsReadOnly(); 
                CVariableDlg dlg(pVar->m_pddbVar, bRO);
                //call the pre edit action method
                pVar->m_pddbVar->doPreEditActs();
                int nRet = dlg.DoModal();

                // stevev 5aug08 - if the pre-edit action ran, the post edit action must run 
                        //call post edit action
        //stevev 22aug07-par727-do_always           if(pVar->m_pddbVar->isChanged())
        //          {
                rc = pVar->m_pddbVar->doPstEditActs();
                if(rc != SUCCESS)
                {
                    LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
                    //return ;
                }

                if(IDOK == nRet)
                {                   
                    rc = pVar->m_pddbVar->doPstUserActs();
                    if(rc != SUCCESS)
                    {
                        LOGIT(CERR_LOG|UI_LOG,"Call to post user edit "
                                                                "action returned an error.\n");
                    }
                }
******************************************************************************************/
                Update(pVar);
            }
        }
    }
#endif
}
#endif // GE_BUILD  




