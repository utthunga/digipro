// DeviceStatus.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "DeviceStatus.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDeviceStatus dialog

CString strStatusString[] = {"Device malfunctioning", 
							 "Configuration changed",
							 "Cold start",
							 "More status available",
							 "PV AO fixed",
							 "PV AO saturated",
							 "Non primary variable out of limits",
							 "Primary variable out of limits"
};

CString strCommStatusString[] = {"Parity error",
							"Overrun error",
							"Framing error",		
							"Checksum error",
							"(reserved)",
							"Rx buffer overflow",
							"(undefined)"
};

CDeviceStatus::CDeviceStatus(CDDLVariable *pDevStatus, CDDLVariable *pCommStatus, CWnd* pParent /*=NULL*/)
	: CDialog(CDeviceStatus::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDeviceStatus)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pVarDevStatus		= pDevStatus;
	m_pVarCommStatus	= pCommStatus;

	for (int nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		m_pDevStatusArray[nCount] = NULL;
		m_pCommStatusArray[nCount] = NULL;
	}
}

CDeviceStatus::~CDeviceStatus()
{
	for (int nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		if (m_pDevStatusArray[nCount])
			delete m_pDevStatusArray[nCount];

		if (m_pCommStatusArray[nCount])
			delete m_pCommStatusArray[nCount];
	}
}

void CDeviceStatus::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDeviceStatus)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDeviceStatus, CDialog)
	//{{AFX_MSG_MAP(CDeviceStatus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDeviceStatus message handlers

// NOTE:   CDeviceStatus class is not being used in SDC.  The CDevCommStatPropPage is being used instead, POB - 2 Aug 2008

BOOL CDeviceStatus::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect rectDevStatus(47,72,290,86);
	CRect rectCommStatus(358,72,558,86);

	if (m_pVarDevStatus == NULL || m_pVarCommStatus == NULL)
		return FALSE;

	if (m_pVarDevStatus->GetType() != TYPE_BITENUM)
		return FALSE;

	int		nCount = 0;
	int		nVal = *m_pVarDevStatus;
	int		nByteVal = 0x80;  // 2nd Byte contains the Device status
							  // refer Status coding page under technical overview manual

    CFont* pFont = GetFont(); //Get the font from the Property Page Dialog for UTF-8 support, POB - 2 Aug 2008

	for (nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		m_pDevStatusArray[nCount] = new CButton();
		m_pDevStatusArray[nCount]->Create(strStatusString[nCount], WS_CHILD|WS_VISIBLE |BS_AUTOCHECKBOX | WS_DISABLED, rectDevStatus, this, 100);

		if (nVal & nByteVal)
		{
			m_pDevStatusArray[nCount]->SetCheck(BST_CHECKED);
		}

		nByteVal = nByteVal >> 1;

		rectDevStatus.top = rectDevStatus.bottom + 10;
		rectDevStatus.bottom = rectDevStatus.top + 16;

        // Set the parent Fonts for the control, POB - 2 Aug 2008
        m_pDevStatusArray[nCount]->SetFont(pFont);

	}


	if (m_pVarCommStatus == NULL) // If there is no Comm error then the value is NULL
		return TRUE;

	nByteVal = 0x8000;
	nVal = *m_pVarCommStatus;

	for (nCount = 0; nCount < MAX_STATUS_BITS; nCount++)
	{
		if (nCount == 4 || nCount == 6) // Unused bits
			continue;

		m_pCommStatusArray[nCount] = new CButton();
		m_pCommStatusArray[nCount]->Create(strCommStatusString[nCount], WS_CHILD|WS_VISIBLE |BS_AUTOCHECKBOX | WS_DISABLED   , rectCommStatus, this, 100);

		if (nVal & nByteVal)
		{
			m_pCommStatusArray[nCount]->SetCheck(BST_CHECKED);
		}

		nByteVal = nByteVal >> 1;

		rectCommStatus.top = rectCommStatus.bottom + 10;
		rectCommStatus.bottom = rectCommStatus.top + 16;

        // Set the parent's Font for the control, POB - 2 Aug 2008
        m_pCommStatusArray[nCount]->SetFont(pFont);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
