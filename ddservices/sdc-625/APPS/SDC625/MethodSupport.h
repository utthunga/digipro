/*************************************************************************************************
 *
 * $Workfile: MethodSupport.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		MethodSupport.h
 * 
 */

#ifndef _METHODSUPPORT_H
#define _METHODSUPPORT_H

#pragma warning (disable : 4786) 

#include < basetsd.h >
#include < string >
#include < vector >
#include < deque >

using namespace std;

#include "ddbMethSupportInfc.h"
#include "methodinterfacedefs.h"
#include "ddbMethod.h"

#ifdef _DEBUG
#define CHKSTK   checkMethodStack()
#else
#define CHKSTK
#endif
class CMenuDlg; 
class CMethodDlg;
class CMainFrame;
class CDDIdeDoc;

class CMethodSupport : public hCMethSupport
{
private:
		//CMethodDlg* m_pMethodDlg;
		hCmethod*   m_pddbMeth;

		//this is set to TRUE if the method is not invoked from the UI by the user. 
		BOOL m_bInvokedForNonUIAction;

		deque<hCmethodCall*> methodStack;
		//vector<hCmethodCall*> methodStack;
		int   clientCount;

	void pushMethod(hCmethodCall* pNewMeth);// adds a new current
	hCmethodCall* popMethod(void); // returns last in (leaves it there)
	hCmethodCall* retrieveMethod( unsigned thrdID );// from the stack (leaves it there)
	hCmethodCall* removeMethod( unsigned thrdID );  // deletes from stack, returns ptr
	void checkMethodStack(void); // only used in debugging
	int  stackSize() { return methodStack.size();};// test 4 stack errors
	HWND m_hwndMain;		// a 'safe' handle to the main window

	CDDIdeDoc* pDoc; // required for layout help


public:	
	hCmethodCall* pCurrentMethodCall;// multiple threads need this info

public:
	CMethodSupport(CDDIdeDoc* pD);

	~CMethodSupport();

	//attributes
	// stevev 19jan11 - no longer used:use MethodCall's ptrs  CMethodDlg* m_pMethodDlg;
	// stevev 19jan11 - no longer used:use MethodCall's ptrs  CMenuDlg*   m_pMenuDlg;	// 
	hCmutex     m_DialogsMtx;
	ACTION_UI_DATA* pActUIdata; // for access to method display info in display menu
	CMainFrame* m_pMainFrm; // the actual mainframe class - not a 'safe' cwnd class
	int        m_buttonSelection;

  bool m_CommErrorOccured;
  bool hasClients(void){ return (clientCount != 0); };

	//methods
	HWND getBaseHwnd();
	hCmethodCall* currentMethod();// sees current

		//function to be implemented in derived class that handles initializations
	//void PreExecute();
#ifdef _DEBUG
	void PreExecute(hCmethodCall& mc);
	void PostExecute(hCmethodCall& mc);
#else
	void PreExecute();
	//function to be implemented in derived class that handles cleanup to be done
	void PostExecute();
#endif
	
	//function that calls the method execute function
	RETURNCODE DoMethodSupportExecute(hCmethodCall& MethCall);


	//function to be implemented in derived class that handles the display of methods UI 
	bool MethodDisplay(ACTION_UI_DATA structMethodsUIData,ACTION_USER_INPUT_DATA& structUserInputData);

	// function to be implemented in derived class that returns the number of methods currently running
	int  numberMethodsRunning(); // should check that the number is correct as well

	/* 01nov06 - tack-on some UI interfaces needed for layout calcs */
	//   image index, UI looks up image, converts pixels X pixels to rows X cols & returns
	bool UIimageSize(int imgIndex, unsigned& xCols, unsigned& yRows, bool isInline);
	//   string ptr, UI extracts the string(it knows what lang), converts lines X chars then to rows X cols & returns
	bool UIstringSize(string* c_str,unsigned& xCols, unsigned& yRows, int heightLimit, bool isDialog = false);

};

#endif //_METHODSUPPORT_H
