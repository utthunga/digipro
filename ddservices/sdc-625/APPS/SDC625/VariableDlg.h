/*************************************************************************************************
 *
 * $Workfile: VariableDlg.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "VariableDlg.h".		
 */

#if !defined(AFX_VARIABLEDLG_H__73AE607C_674E_48E8_AD12_C5E46CCDFACE__INCLUDED_)
#define AFX_VARIABLEDLG_H__73AE607C_674E_48E8_AD12_C5E46CCDFACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/***************************************************************************
 * Private Definitions for this file
 ***************************************************************************/
#include "paredit.h"
#define VRBL_TIMER          2
#define ONE_SECOND       1000   // 1000 milliseconds (1 second)

#include "sdcDialog.h"/* stevev 09jun06 */

/* stevev 28jul08 - keep track of what edit box is active */
typedef enum vdboxtype_e
{
	unknownBoxType,
	chkComboType,
	editBoxType,
	comboBoxType,
	dateBoxType
} 
/* typedef */ vdBoxtype_t;
/*
 * Forward declaration of the doc to avoid a circular dependency
 */
class CDDIdeDoc;
class CCheckComboBox;
/* stevev 09jun06 - make it an sdc Dialog
class CVariableDlg : public CDialog
*/
class CVariableDlg : public sdCDialog	/* end 09jun06 */
{
// Construction
public:
	//CVariableDlg(CDDLVariable* pVar,CWnd* pParent = NULL);   // standard constructor
	CVariableDlg(hCVar* pVar,BOOL parentRO, CWnd* pParent = NULL);   // standard constructor
	virtual ~CVariableDlg();	/*<START>Added by ANOOP 09APR2004 to fix the memory leaks	*/
protected:
// Dialog Data
	//{{AFX_DATA(CVariableDlg)
	enum { IDD = IDD_EDIT_VAR_DIALOG };
	CStatic	m_statDsply;  /* above edit box */
	CParsedEdit	m_cntrlEdit;
	CComboBox	m_cntrlCombo;
	CDateTimeCtrl m_DateTimeCtrl;	//Added by ANOOP 05MAR2004
	CString	m_enumString;
	/* float	m_fVarEdit;
	stevev 08feb06 - changed to double - float wont hold a full sized int!!!! */
	double     m_fVarEdit;
	/* VMKP changed from Int type to Float on 180204 
	There are variables in Logix, Promag etc, whose
	display format and Edit Format are %x.xf, If this is
	 type, the binding was not allowing to enter a float
	value for float edit format, Any way anoop is handling
	Integer type Variable editing properly in preedit.cpp, 
	not to allow a . for an integer type variable */
	/* float	m_nVarEdit;  
	stevev 08feb06 - changed to double - float wont hold a full sized int!!!! */
	double  m_nVarEdit;
	/* END of VMKP change on 180204 */
    /*stevev 08feb06 - duplicated changes above for UINT	m_uVarEdit;*/
	double	m_uVarEdit;
	CString	m_strVarEdit;
	CString	m_strDsply;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVariableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
    void DDX_Format(CDataExchange* pDX, int nIDC, /* sjv 08feb06 was::float*/double& value);
    void DDX_Format(CDataExchange* pDX, int nIDC, int& value);
    void DDV_MinMaxFloat(CDataExchange* pDX, float value, float* pMinVal, float* pMaxVal, UINT nSize = 1);
    void DDV_MinMaxInt(CDataExchange* pDX, int value, int* pMinVal, int* pMaxVal, UINT nSize =1);
    void FailMinMaxWithFormat(CDataExchange* pDX, long* pMinVal, long* pMaxVal, UINT nSize,
        LPCTSTR lpszFormat, UINT nIDPrompt);
    void CVariableDlg::FailMinMaxReal(CDataExchange* pDX, double* pMinVal, double* pMaxVal, UINT nSize,
        int precision, UINT nIDPrompt);

	void GetDateFromString(CString);

	void Float_Format_before_Display(CString ,CString,CString & );
	
#ifdef _DEBUG
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
#endif

    // Generated message map functions
	//{{AFX_MSG(CVariableDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeVariableCombo();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void Update();
    UINT m_nSize;
    //int* m_pnMin;
    //int* m_pnMax;
    //float* m_pfMin;
    //float* m_pfMax;
    CString m_dsplFormat;
//    CString m_edtFormat;
	//CDDLVariable* m_pVar;
	hCVar*  m_pVar;
	CCheckComboBox *m_pEnumCheckBox; // List box for Bit Enum
	int nMax_Chars;
	int nDecimal_Limit;
	vdBoxtype_t theBoxType;
	BOOL parentISreadOnly;
	
	CFont m_Font;// stevev 10jul2017

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VARIABLEDLG_H__73AE607C_674E_48E8_AD12_C5E46CCDFACE__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: VariableDlg.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
