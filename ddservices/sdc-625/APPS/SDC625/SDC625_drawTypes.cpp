/*************************************************************************************************
 *
 * $Workfile: SDC625drawTyupdatepes.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the SDC625drawTypes class  
 *		8/26/04	sjv	creation
 */

#include "ddbDefs.h"
#include "SDC625_drawTypes.h"
#include "Dyn_Controls.h"
#include "SDC625_WindowView.h"
#include "graphicInfc.h"
#include "ddbMenuLayout.h"
#include "WaoDlg.h"
#include "LabelDialog.h"
#include "MethodSupport.h"  // to get the pointer in Execute

// moved to ddbMenuLayout.h #define TAB_HGT_ROWS	1	/* number of rows the tab-control overhead takes */
// moved to ddbMenuLayout.h#define GRP_HGT_ROWS	1	/* number of rows the group overhead (label & box) takes */

// TEMPORARY::\/
extern const int horizSizes[] ;// moved to ddbMenu  = HORIZSCOPESIZES;
extern const int vert_Sizes[] ;// moved to ddbMenu  = VERT_SCOPESIZES;

#ifdef _DEBUG
#define LOGBIRTH	/* comment out to stop creation logging */
int ldepth = 1;    // global- can only log one birth at a time
#endif

/* stevev - keeping track of last focus is usful for debugging and may be required */
unsigned  last_focus = 0;// this is a control id that last had the focus.

/* * * * * * * * * *  BASE  * * * * * * * * * */

CString getNumericFormat(CValueVarient v)
{ 	
	CString format;

	switch (v.vType)
	{
	case v.isIntConst:
		if (v.vIsUnsigned)
		{
			if		( v.vSize == 0)		format = L"";
			else if ( v.vSize == 1)		format = L"%4u";
			else if ( v.vSize == 2 )	format = L"%6u";
			else if ( v.vSize <= 4 )	format = L"%11u";
			else if ( v.vSize <= 8 )	format = L"%20u";
		}
		else
		{
			if		( v.vSize == 0)		format = L"";
			else if ( v.vSize == 1)		format = L"%4d";
			else if ( v.vSize == 2 )	format = L"%6d";
			else if ( v.vSize <= 4 )	format = L"%11d";
			else if ( v.vSize <= 8 )	format = L"%20d";
		}
		break;

	case v.isVeryLong:
		if (v.vIsUnsigned)				format = L"%I64u";
		else							format = L"%I64d";
		break;

	case v.isFloatConst:
		if (v.vIsDouble)				format = L"%18.8g";
		else							format = L"%12.5g";
		break;

	default:
		format = L"";
		break;
	};
	return format;
}

CdrawBase::CdrawBase(menuItemDrawingStyles_t t, CdrawBase* pP):mdsType(t),m_pParentDraw(pP)  
{	
	m_pDynCtrl = NULL; 
	m_cntrlID = ( (CDDIdeApp *) AfxGetApp())->getUniqueID();
	m_symblID = 0;
	// stevev 10aug05 START
	class_history.parentStyle = t; 
	class_history.inReview    = false;	// stevev 31aug07 - new (this may have to get a value)
	if ( t == mds_dialog)
	{
		class_history.dlgInPath = true;
	}
	else
	if ( t == mds_methDialog)
	{
		class_history.dlgInPath = true;
		class_history.inMethDlg = true;
	}
	else
	if ( t == mds_menu)
	{
		class_history.inPopMenu = true;
	}
	// else leave 'em be
	//topLeft.xval=topLeft.yval = da_size.xval=da_size.yval = 0; 
	m_MyLoc.clear();
	m_nPage = 0; m_pDynTabCtrl = NULL;
	m_bIsReadOnly = FALSE;
	// no longer used 11sep07 m_bTriggerEdit= FALSE;
	m_bIsNoLabel  = FALSE;
	m_bIsNoUnit   = FALSE;
	m_bIsInline   = FALSE;
	m_bIsDisplayVal=FALSE;
	m_bIsReview   = FALSE;
	m_bIsAroot    = FALSE;
	m_bIsAbutton  = FALSE;

	m_pRef      = NULL;
	m_pItem     = NULL;
//x	m_pMenuTree = NULL;
	m_pStartTree= NULL;
	m_pHoldTree = NULL;
	m_pNewTree  = NULL;
	m_itemIndex = -1;
	// stevev 10aug05 END
#ifdef DO_PUBLISH
	sub_handle = -1;
	sub_List.clear();
#ifdef _DEBUG
	pubsParent = this;
#endif
#endif
	// stevev 6sep07 - add focus msgs - set in CDynControls::AddDlgControl()
	Set_Focus_Msg = 0;
	Kil_Focus_Msg = 0;

	inActions = false; // stevev-reentry protections

    m_pItemRef = NULL; // Set to NULL, reference to menu item so we can get the label when 
    				   //              referenced via a collection/array, POB, 13 Aug 2008
};	

// This base call must be called by all chidren who have other controls
// (ie, Variables) to delete beyond that of the main control
// NOTE:  Each child must delete its additional control and then call the
//        base destructor
CdrawBase::~CdrawBase()  
{	
#ifdef DO_PUBLISH	
	if ( sub_handle >= 0 && m_pItem != NULL )
	{
TRACE(_T("DrawBase Unsubscribing handle 0x%x in thread 0x%x\n"),sub_handle,GetCurrentThreadId());
		hCglobalServiceInterface::myDevicePtr(m_pItem->devHndl())->unsubscribe(sub_handle);
	}
#endif
	if (m_pDynCtrl)
	{
		CWnd * ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		if (ptr)
		{
			// Destroy the main control (CWnd object)
			ptr->DestroyWindow();
		}

		// Now, Delete the Dynamic Control object's memory
		delete m_pDynCtrl;
		m_pDynCtrl = NULL;
	}
	for (DrawListIterator_t i = m_DrawList.begin(); i < m_DrawList.end(); ++i)
	{	delete (*i); }
	m_DrawList.clear();

    // Delete reference object if it exists, POB - 13 Aug 2008
    if (m_pItemRef)
    {
        delete m_pItemRef;
        m_pItemRef = NULL;
    }
}

ITEM_ID CdrawBase::FillSymb(hCreference& rRef)
{
	RETURNCODE rc = SUCCESS;
	
	CValueVarient msk;

	if (rRef.getRefType() == rT_via_BitEnum)
	{
		if ( rRef.resolveExpr(msk) == SUCCESS && 
			(msk.vType == CValueVarient::isIntConst || msk.vType == CValueVarient::isVeryLong))
		{
			m_itemIndex = msk;
		}
		else
		{
			// Error - Could not resolve mask
			TRACE(_T("CdrawBase::FillSymb - Could not resolve mask\n"));
			m_itemIndex = -1;
		}	
	}

/*	*/

	if (  ( rc = rRef.resolveID(m_pItem,false)  != SUCCESS ) || m_pItem == NULL )
	{
		m_symblID = 0;
	}
	else
	{
		m_symblID = m_pItem->getID();
	}

    // Create reference object based on the one the one passed into FillSymb()
    // in order so that we can get the label of the refernce - could be different from item's label
    // To get an updated item name, we must call FillSymb() again probably
    // after it has been destroyed and re-created, POB - 14 Aug 2008
    m_pItemRef = new hCreference(rRef);

	return m_symblID;
}


CDDIdeDoc* CdrawBase::GetDocument()
{	
	CDDIdeDoc* pDoc = NULL;
//	CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();
//	CMainFrame * pmainWnd = (CMainFrame *)(AfxGetApp()->m_pMainWnd);
	CMainFrame * pMainWnd = GETMAINFRAME;// get the actual document

	if (pMainWnd->GetDocument())
	{
		pDoc = pMainWnd->m_pDoc;
	}
	else
	{
clog<<"DrawBase did not get a document"<<endl;
	}

	return pDoc;
}

CdrawBase* CdrawBase::newDraw(menuItemDrawingStyles_t dt)
{
	CdrawBase* prv = NULL;

	switch (dt)
	{
	case mds_window:	prv = (CdrawBase*)new CdrawWin(this);		break;
	case mds_dialog:	prv = (CdrawBase*)new CdrawDlg(this);		break;
	case mds_page:		prv = (CdrawBase*)new CdrawPage(this);		break;
	case mds_group:		prv = (CdrawBase*)new CdrawGroup(this);		break;
	case mds_menu:		prv = (CdrawBase*)new CdrawMenu(this);		break;
	case mds_table:		prv = (CdrawBase*)new CdrawTbl(this);		break;
	case mds_methDialog:prv = NULL; LOGIT(CERR_LOG,"NO DISPLAY MENU IMPLEMENTED\n"); break;

	case mds_var:		prv = (CdrawBase*)new CdrawVar(this);		break;
	case mds_index:		prv = (CdrawBase*)new CdrawIndex(this);		break;
	case mds_edDisp:	prv = (CdrawBase*)new CdrawEdDisp(this);	break;
	case mds_enum:		prv = (CdrawBase*)new CdrawEnum(this);		break;
	case mds_bitenum:	prv = (CdrawBase*)new CdrawBitEnum(this);	break;
	case mds_bit:		prv = (CdrawBase*)new CdrawBit(this);		break;
	case mds_method:	prv = (CdrawBase*)new CdrawMeth(this);		break;
	case mds_image:		prv = (CdrawBase*)new CdrawImage(this);		break;
	case mds_scope:		prv = (CdrawBase*)new CdrawScope(this);		break;
	case mds_separator:	prv = (CdrawBase*)new CdrawSep(0,(this));	break;
	case mds_newline:	prv = (CdrawBase*)new CdrawSep(1,(this));	break;
	case mds_constString:prv= (CdrawBase*)new CdrawStr(this);		break;
	case mds_tab:		prv = (CdrawBase*)new CdrawTab(this);		break;
	case mds_grid:		prv = (CdrawBase*)new CdrawGrid(this);		break;
	case mds_blank:		prv = (CdrawBase*)new CdrawBlank(this);		break;
	}

	return prv;
}

/**** no use shown, remove for now ***
// generic menu filler - fills from symbID...
int CdrawBase::fillList(char* listName) // name is for debugging only
{		
	if (!m_symblID)
		return 0; // can't fill from the ether

	menuItemList_t localMenuList;
	int c = getList(m_symblID, localMenuList, class_history );

	if ( c <= 0 )
	{
		TRACE(_T("ERROR: nothing in %s list.\n"),listName);
		return c;
	}

	CdrawBase*     pcdb = NULL;
	ulong          uCID = 0;
	int            i;

	menuItemList_t::iterator iT;
	for (iT = localMenuList.begin(); iT < localMenuList.end(); iT++)
	{
		CdrawBase* pP = newDraw(iT->getDrawAs());

		// Check to see if the mds type should be an index
		// TEMPORARY - until Steve gets this fixed
		if (pP)
		{
			pP->class_history = iT->getHistory();
			if ( pP->mdsType == mds_var)
			{
				pP->FillSymb(iT->getRef());
				hCVar* pVar = (hCVar*) pP->GetItem();
				if (pVar->VariableType() == TYPE_INDEX)
				{
					delete pP;
					pP = newDraw(mds_index);
					pP->class_history = iT->getHistory();
					pVar = (hCVar*) pP->GetItem();
				}
			}
		}

		// WORK TO DO:  Verify the if statements after we integrate
		if (pP)
		{	
//isa menuitem			pP->m_pMenuTree   = (hCmenuTree*)iT;	// will point into the main tree
//isa menuitem			pP->m_MyLoc.setValue(iT->topleft,iT->hv_size);
			pP->class_history = iT->getHistory();
			if ( iT->getDrawAs() == mds_separator || // order of tests are IMPORTANT!
				 iT->getDrawAs() == mds_newline   ||
				 pP->FillSymb(iT->getRef()) != 0  || 
				 iT->getDrawAs() == mds_constString )
			{					
				COPY_QUALIFIER(iT,pP);// from,to
				pP->FillSelf(0    ,menuHist_t());
				i = m_DrawList.size(); // next index
				m_DrawList.push_back(pP);
				m_CID2DrawMap[pP->m_cntrlID] = i;
				m_SID2DrawMap[pP->m_symblID] = i;
			}
			else
			{
				delete (pP);
				TRACE(_T("%s fill ERROR: could not resolve menu item reference.\n"),listName);
			}
		}
		else
		{
			TRACE(_T("%s fill ERROR: could not generate a new Draw item.\n"),listName);
		}
	}// next menu item
	return ( m_DrawList.size() );
}
*************** end temp test *****/

#if 0 /* we want a finer grain - rowbreak will do the total reset now */
		/* a helpful macro */
		#define SECTIONRESET { DDSizeLocType tS = 0;\
		/* get array max */		for ( zt = 0; zt < COLS_SUPPORTED; zt++) \
								{if (maxRow[zt] > tS) tS = maxRow[zt];} \
		/* set 'em all to that*/for ( zt = 0; zt < COLS_SUPPORTED; maxRow[zt++]=tS) ;\
		/* reset the cursor */	cursor.yval = tS + 1;		cursor.xval = 1; }
#else
/* a helpful macro */
#define SECTIONRESET(w) { DDSizeLocType tS = 0;\
/* get array max */		for ( zt = 0; zt < w; zt++) \
						{if (maxRow[zt] > tS) tS = maxRow[zt];} \
/* set 'em all to that*/for ( zt = 0; zt < w; maxRow[zt++]=tS) ;\
/* reset the cursor */	cursor.yval = tS + 1;		cursor.xval = 1; }
#endif

//#define MAXmaxRow ((maxRow[0]>maxRow[1])?\
// ((maxRow[0]>maxRow[2])?maxRow[0]:maxRow[2])\
//:((maxRow[1]>maxRow[2])?maxRow[1]:maxRow[2]))

/* places underlings, sizes self for the more complex types... */
int CdrawBase::menuSize(void)
{
	pnt_t cursor = { 1,1 };
	int zt;

	DDSizeLocType maxRow[COLS_SUPPORTED];// max used row by column

	for ( zt = 0; zt < COLS_SUPPORTED; maxRow[zt++]=0) ;

	DrawListIterator_t i;
	// for each item
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{// fill the page
		(*i)->FillSize();
	}
#ifdef _DEBUG
	if (GetItem() != NULL)
	{
		LOGIT(CLOG_LOG,"}}}} Start Content List    of 0x%04x (%s)\n",
			GetItem()->getID(),GetItem()->getName().c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"}}}} Start Content List with null self.\n");
	}
#endif
	// for each item
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		
		if (mdsType == mds_image)
		{
			clog <<" Got an Image at the top level."<<endl;;
		}

		if ( (*i)->mdsType == mds_separator )
		{// column break the cursor
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"separator on window ";
			}
			#endif
			cursor.xval ++;
			if ( cursor.xval > COLS_SUPPORTED ) // won't fit - wrap it
				SECTIONRESET(1) ;/************/
			cursor.yval = maxRow[cursor.xval-1] + 1; // whether we reset or not
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"cursor: x "<< cursor.xval <<"  y "<<cursor.yval<<endl;;
			}
			#endif
			continue; // skip the sizing
		}
		else
		if ((*i)->mdsType == mds_newline)
		{
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"row break on window ";
			}
			#endif
		//  form invisible line by setting all to the largest
			int max = -1;
			for ( zt = 0; zt < COLS_SUPPORTED; zt++) 
			{	if (maxRow[zt] > max) max = maxRow[zt];	}
			for ( zt = 0; zt < COLS_SUPPORTED; maxRow[zt++]=max) ;
			SECTIONRESET(1);
			cursor.yval = maxRow[cursor.xval-1] + 1; 
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"cursor: x "<< cursor.xval <<"  y "<<cursor.yval<<endl;;
			}
			#endif
			continue; // skip the sizing
		}
		else
		if ( ((*i)->m_MyLoc.da_size.xval - 1 ) + cursor.xval <= COLS_SUPPORTED )
		{// fits - insert it

			DDSizeLocType tS = 0;\
			/* get array max */		
			for ( zt = cursor.xval-1; 
			      zt < ((cursor.xval-1)+((*i)->m_MyLoc.da_size.xval)); 
			      zt++) 
			{if (maxRow[zt] > tS) tS = maxRow[zt];}

			/* set 'em all to that*/	
			for ( zt = cursor.xval-1; 
			      zt < ((cursor.xval-1)+((*i)->m_MyLoc.da_size.xval)); 
			      maxRow[zt++]=tS) ;

			cursor.yval = tS + 1;
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<" Fits: cursor: wx "<< cursor.xval <<" wy "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval;
			}
			else
			if (mdsType == mds_image)
			{
				clog <<" Fits: cursor:ix "<< cursor.xval <<" iy "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval;
			}
			else
			if (mdsType == mds_constString)
			{
				clog <<" Fits: cursor:sx "<< cursor.xval <<" sy "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval;
			}
			if (mdsType == mds_blank)
			{
				clog <<" Fits: cursor:bx "<< cursor.xval <<" by "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval;
			}
			else
			{
				clog <<" Fits: cursor: x "<< cursor.xval <<"  y "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval;
				
				//else
				//{
				//	LOGIT(CLOG_LOG," Fits: cursor: x %d   y %d   w:%d  h:%d  0x%04x (%s)\n",
				//		cursor.xval,cursor.yval,(*i)->da_size.xval,(*i)->da_size.yval,
				//		(*i)->GetItem()->getID(),(*i)->GetItem()->getName().c_str());
				//}
			}
			
			if ((*i)->GetItem() == NULL)
			{
				clog<<" with No DeviceObject Item Ptr."<<endl;
			}
			else
			{
				LOGIT(CLOG_LOG,"  0x%04x (%s)\n",
					(*i)->GetItem()->getID(),(*i)->GetItem()->getName().c_str());
			}
			#endif
		}
		else // doesn't fit
		if ( cursor.xval > 1 ) // if we aren't in the first column, we can get some size
		{// do a section reset
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"NoFit: cursor: x "<< cursor.xval <<"  y "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval <<endl;;
			}
			#endif
			SECTIONRESET((*i)->m_MyLoc.da_size.xval);
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"Reset: cursor: x "<< cursor.xval <<"  y "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval ;
				if ((*i)->GetItem() != NULL) // tabs have null
					LOGIT(CLOG_LOG,"  0x%04x (%s)\n",
						(*i)->GetItem()->getID(),(*i)->GetItem()->getName().c_str());
				else
					clog<<endl;
			}
			#endif
		}
		else // we have to truncate it, so
		{// insert it anyway 
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"Force: cursor: x "<< cursor.xval <<"  y "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval <<endl;;
			}
			#endif
			if ( (*i)->m_MyLoc.da_size.xval > COLS_SUPPORTED ) 
			{
				(*i)->m_MyLoc.da_size.xval = COLS_SUPPORTED;
			}
			#ifdef _DEBUG
			if (mdsType == mds_window)
			{
				clog <<"     : cursor: x "<< cursor.xval <<"  y "<<cursor.yval
					 <<"   w:" <<(*i)->m_MyLoc.da_size.xval << "  h:"<<(*i)->m_MyLoc.da_size.yval <<endl;;
			}
			#endif
		}
		(*i)->m_MyLoc.topleft.xval = cursor.xval;
		(*i)->m_MyLoc.topleft.yval = cursor.yval;
		DDSizeLocType newVert = maxRow[cursor.xval-1] + (*i)->m_MyLoc.da_size.yval;
		for ( int y = 1, zt = cursor.xval-1; y <= (*i)->m_MyLoc.da_size.xval; y++, zt++)
		{
			maxRow[zt] = newVert;
		}
		cursor.yval += (*i)->m_MyLoc.da_size.yval;
	}// next item
// wtf	SECTIONRESET;

	m_MyLoc.da_size.yval = maxRow[0];
	for ( zt = 1; zt < COLS_SUPPORTED; zt++ )
	{
		if ( maxRow[zt] > m_MyLoc.da_size.yval )
		{
			m_MyLoc.da_size.yval = maxRow[zt];
		}
	}
	DDSizeLocType   maxRight = 0;
	// we need to handle: Var Sep Sep Var  situation
	for ( zt = 0; zt < COLS_SUPPORTED; zt++ )
	{
		if ( maxRow[zt] != 0 )
		{
			maxRight = zt;// 0 - 2
		}
	}
	m_MyLoc.da_size.xval = maxRight+1;// needs 1 - 3

	
#ifdef _DEBUG
	if (GetItem() != NULL)
	{
		LOGIT(CLOG_LOG,"{{{{ Finished Content List    of 0x%04x (%s)\n",
			GetItem()->getID(),GetItem()->getName().c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"{{{{ Finished Content List with null self.\n");
	}
#endif
	return 0;// no error
}

void CdrawBase::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	topLeftParent.xval += (m_MyLoc.topleft.xval -1);
	topLeftParent.yval += (m_MyLoc.topleft.yval -1);

	//16jul12-stevev: mul is number of columns to add to the x start to center this item
	double mul    = ((double)m_MyLoc.canvasSize-(double)m_MyLoc.rowSize)/2;	
	if (mdsType == mds_page || mdsType == mds_tab)
	{
		mul = 0;// pages are never offset
		centeringOffset = 0;
	}

	if (!m_symblID && m_pRef != NULL)
	{
		FillSymb(*m_pRef);
	}
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"\nNew DynControl for 0x%04x  (%s)\n",
								m_symblID, menuStyleStrings[mdsType] );

	m_pDynCtrl = new CDynControls(topLeftParent, mul, pWnd, centeringOffset);
	m_pDynCtrl->AddDlgControl(this, centeringOffset);  
		
	// Now create the controls
	m_pDynCtrl->Create(pWnd);

	// Initialize this control
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->AddPageCtrl(m_cntrlID, m_nPage); 
	}
}


// prototype
//	LOGIT(CLOG_LOG,"CREATE:%s\n",Space( ldepth*2 ));

void CdrawBase::createMenu(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CdrawBase * pBase;
#ifdef LOGBIRTH		/* only defined in debug mode (top of this file) */ 
	if ( m_pItem )
	{	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"\nCREATE[%d]: Creating 0x%04x (%s)\n",
										ldepth,	m_pItem->getID(),m_pItem->getName().c_str());
	LOGIT(CLOG_LOG,"             T:%d  L: %d      H: %d  W: %d  O: %d  as %s \n",
			topLeftParent.yval,topLeftParent.xval,m_MyLoc.da_size.yval,m_MyLoc.da_size.xval,
			centeringOffset,   menuStyleStrings[mdsType]);
	}
#endif
//	int offset = 0;
//	if ( mdsType == mds_group )
//	{
//		offset = ((CdrawGroup*)this)->myPixelOffset;// + topLeftOffset.xval;
//	}
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Now create the controls on its children
		pBase = (*i);
#if defined( _DEBUG ) && defined( DO_PUBLISH )
		if ( pBase->GetItem() != NULL && (pBase->GetItem())->IsVariable() )
			{
				hCVar* pVr = (hCVar*)pBase->GetItem();
				if ( pVr->IsDynamic() )
				{
					sub_List.push_back(pBase->GetItem());
				}
			}
#endif
		// This code is used to make sure that if we have a window or dialog or 
		// Table (TBD) that lives in another window, dialog, group, page that we handle it
		// as a button. - change the type here
		if ( mdsType < mds_var ) // we are a menu
		/*	 mdsType == mds_window || mdsType == mds_dialog || mdsType == mds_page  || 
			 mdsType == mds_group  || mdsType == mds_menu   || mdsType == mds_table ||  
			 mdsType == mds_methDialog  ) /x* this isa a menu */
		{						 // this child is one of three
			if ( pBase->mdsType == mds_window || 
				 pBase->mdsType == mds_table  || 
				 pBase->mdsType == mds_dialog   )// menues too??
			{
				pBase->mdsType = mds_parent;// make a button
			}
#ifdef already_done_above_DO_PUBLISH
			if ( pBase->GetItem() != NULL && (pBase->GetItem())->IsVariable() )
			{
				hCVar* pVr = (hCVar*)pBase->GetItem();
				if ( pVr->IsDynamic() )
				{
					sub_List.push_back(pBase->GetItem());
				}
			}
#endif
		}

/*DEBUG*/
if (pBase->mdsType == mds_scope)
{	TRACE(_T("++SCOPE CONTROL\n"));
}
/* end DEBUG */

		pBase->m_nPage = m_nPage;  // this may never be used (ie, no Page) by the control itself 
		pBase->m_pDynTabCtrl = m_pDynTabCtrl;
//		pnt_t  tmpPt = {offset, 0};
//		pBase->createControl(pWnd, topLeftParent, tmpPt);
		pBase->createControl(pWnd, topLeftParent, centeringOffset);
	}// next in child-list
#ifdef DO_PUBLISH
	if ( sub_List.size() > 0 && m_pItem != NULL )
	{
		int rc = hCglobalServiceInterface::myDevicePtr(m_pItem->devHndl())->subscribe
								(sub_handle, sub_List, (hCpubsub*)this, WINDOW_UPDATE_RATE);
		TRACE(_T("Drawbase subscribes to %d vars  hx%02x symbol 0x%04x px%p\n"),
			sub_List.size(),sub_handle,m_symblID,this);
	}
#endif
#ifdef LOGBIRTH	
	
	if ( m_pItem )
	LOGIF(LOGP_LAYOUT)(CLOG_LOG,"\nCREATE[%d]--- Finished: 0x%04x ---\n", 
																	ldepth,	m_pItem->getID() );
#endif
}


#ifdef DO_PUBLISH
RETURNCODE CdrawBase::publish(dataStructList_t dsL)
{ 
	/* we don't do anything here - Display is updated by subscription's resulting notifications*/
TRACE(_T("DrawBase -do nothing publish method!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"));
	return SUCCESS;
}
#endif



#ifdef DCDEBUG
//	fbuf[1024];
void CdrawBase::dumpSelf(CFile& fi, int spaces)
{
	char tbuf[30];
	switch (mdsType)
	{
	case mds_window: strcpy(tbuf,"Window"); break;
	case mds_dialog: strcpy(tbuf,"Dialog"); break;
	case mds_page:   strcpy(tbuf,"Page  "); break;
	case mds_group:  strcpy(tbuf,"Group "); break;
	case mds_menu:   strcpy(tbuf,"Menu  "); break;
	case mds_table:  strcpy(tbuf,"Table "); break;
	case mds_tab:    strcpy(tbuf,"TabCtl"); break;

	case mds_var:    strcpy(tbuf,"Var   "); break;
case mds_separator:  strcpy(tbuf,"-Sep- "); break;
case mds_newline:    strcpy(tbuf,"-NewLn-");break;
	/*mds_edDisp:
	mds_enum:
	mds_bitenum,
	mds_bit,
	mds_method,
	mds_image,
	mds_scope,		// charts and graphs
	mds_constString
	mds_blank       */
	default:
		sprintf(tbuf,"0x%02x (%s)",(int)mdsType,menuStyleStrings[mdsType]);
	}
	sprintf(fbuf,"%*s top:%d lft:%d   wid:%d  hgt:%d\n",spaces,tbuf,
		m_MyLoc.topleft.yval , m_MyLoc.topleft.xval, m_MyLoc.da_size.xval , m_MyLoc.da_size.yval );
	fi.Write(fbuf, strlen(fbuf));
	DrawListIterator_t i;
	// for each item
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{// fill the page
		(*i)->dumpSelf(fi, spaces+4);
	}
}
#endif

// This function handles the changes made to the control by the user
// or by other window events
void CdrawBase::Execute(WORD wMessageID, LPARAM lParam)  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014
{
	// Does nothing
	// Override this method in the child object to handle
	// the control id

}

BOOL CdrawBase::ExecuteCID(WORD wControlID, WORD wMessageID, LPARAM lParam)  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014
{
	CdrawBase * pBase;
	BOOL result = FALSE;

	if (m_cntrlID == wControlID)
	{
		if (wMessageID && wMessageID == Set_Focus_Msg)
		{
			if (last_focus == wControlID) 
			{
				TRACE(_T("$$$$ Cntrl 0x%04x SET_FOCUS_AGAIN\n"),m_cntrlID);
				wMessageID = WM_SET_FOCUS_AGAIN;// set to 'regain' focus message... 
			}
			else // new
			{
				TRACE(_T("$$$$ Cntrl 0x%04x SET_FOCUS\n"),m_cntrlID);
				last_focus = wControlID; // record the global
				wMessageID = EN_SETFOCUS;// set to 'standard' focus message... 
			}
		}
		else
		if (wMessageID && wMessageID == Kil_Focus_Msg)
		{
			TRACE(_T("$$$$ Cntrl 0x%04x KILL_FOCUS\n"),m_cntrlID);
			wMessageID = EN_KILLFOCUS;// set to 'standard' focus message... 
		}
		else
		{
#ifdef x_DEBUG
			TRACE(_T("$$$$ Cntrl 0x%04x got Msg 0x%04x\n"),m_cntrlID,wMessageID);
#endif // debug
		}
#ifdef RLSDBG
		LOGIT(CLOG_LOG,"NOTE: Execute Matched.\n");
#endif
		TRACE(L">>>>  Matched this control %s(%d), executing.",GetLabel(),wControlID);
		// Handle the event here
		if (Pre(wMessageID))
		{
			TRACE(L"------> Pre MsgID(%#X) succeded.(executing)\n",wMessageID);
#ifdef RLSDBG
		LOGIT(CLOG_LOG,"NOTE: doing Execute 0x%x from thread 0x%04x.\n",wMessageID,GetCurrentThreadId());
#endif
            Execute(wMessageID, lParam);  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014
#ifdef RLSDBG
		LOGIT(CLOG_LOG,"NOTE: doing Post from thread 0x%04x.\n",GetCurrentThreadId());
#endif
			Post(wMessageID);	// Handle any post events (i,e Post Edit, ect)
			
			// Found the control so return TRUE
			result = TRUE;
		}
#ifdef _DEBUG
		else
		{
			TRACE(L"------> Pre MsgID(%#X) failed.\n",wMessageID);
		}
#endif
		TRACE(L">>>>  Matched this control (%d), finished.\n",wControlID);
	}
	else
	{
		// Iterate through child list
		DrawList_t::iterator i;
		for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{
			// Look for this control id in its children
			pBase = (*i);

            result = pBase->ExecuteCID(wControlID, wMessageID, lParam);  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014

			if (result)
			{
				break;
			}
		}
	}
#ifdef RLSDBG
	LOGIT(CLOG_LOG,"NOTE: Exiting  ExecuteCID  from thread 0x%04x.\n",GetCurrentThreadId());
#endif
		
	return result;
}

void CdrawBase::Destroy(ulong symblID)
{
	CdrawBase * pBase, **ppBase;
	DrawList_t::iterator i;
	BOOL bPageDecrease = FALSE;
	BOOL bMainMenu = FALSE;

#ifdef DO_PUBLISH
	/* this was added here since the desctructor is not always called !!!!!!! */
	if ( sub_handle >= 0 && m_pItem != NULL )
	{
TRACE(_T("DrawBase Destroy Unsubscribing handle 0x%x in thread 0x%x\n"),sub_handle,GetCurrentThreadId());
		hCglobalServiceInterface::myDevicePtr(m_pItem->devHndl())->unsubscribe(sub_handle);
		sub_handle = 0;// if destructor is executed, it won't try again
		sub_List.clear();
	}
#endif

	if (symblID == 0 && mdsType == mds_menu)
	{
		// CdrawMain, Separators, Constant Strings and Images do not have symbols so
		// all their symbol IDs will be zero.
		// We need to return here because the code below will cause bad
		// things to happen
		//return;
		bMainMenu = TRUE;
	}

	if (symblID == m_symblID && !bMainMenu) // if entry symbol ID matches mine
	{	// We need to delete all the children for this one and all the
		// ones underneath
		for (i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{
			ppBase = (CdrawBase**)&(*i);
			pBase  = *ppBase;

			// Using child's symbol ID, forces a delete on all children underneath
			pBase->Destroy(pBase->m_symblID); 
			delete pBase;
		}

		// Since we have destroyed all the children, empty out the draw list
		// contents
		if (m_DrawList.size()) // seems to be crashing when clearing an empty vector
			m_DrawList.clear();
	}
	else // symbol id doesnt match mine
	{
#ifdef _DEBUG
		int y = 0;
#endif
		// We need to iterate through the children to find the one that matches
		//stevev changed to a while loop for vs8
		//for (i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		i = m_DrawList.begin();
		while (i != m_DrawList.end())
		{
#ifdef _DEBUG
		y++;
#endif
			pBase = *i;

			if (bPageDecrease)
			{	// This must be of type mds_page since it can only be set by mds_tab

				// A previous Page has been deleted
				// Need to bump up each page by one for each page (ie, 1->0, 2->1, 3->2, etc)
				// (ie, decrement each page by one)
				pBase->m_nPage--; 
			}

			pBase->Destroy(symblID);
			
			if (symblID == pBase->m_symblID)  // if entry symbol id matches child's symbol ID
			{
				// If "this" is a tab, then the child must be a page so decrement the page by one
				if (mdsType == mds_tab)
				{
					m_nPage--;  // decrease the number of pages by one.
					bPageDecrease = TRUE;
				}

				// if the parent to this item is a menu we need to remove its link from the menu
				DeleteMenu(pBase);
				
				delete pBase;          // Parent will delete the actual child that it does match
				pBase = NULL;
				i = m_DrawList.erase(i);	// vs8 automagically moves the value of i to a valid ptr
				// stevev 24feb10 - removed other erase code that was used for the 'for' loop
			}// endif target found
			else

			// Make sure that the child has not been deleted above
			// stevev 24feb10 - improve logix via else...if (pBase)
			{
				// If the child is a tab and there are no pages under it
				// we need to go ahead and delete the object to remove
				// the tab window frame.
				if (pBase->mdsType == mds_tab && !pBase->m_nPage)
				{
					// No pages exist! 
					// Do an automatic delete
					delete pBase;
					pBase = NULL;

					i = m_DrawList.erase(i);// stevev 24feb10 - while loop
				}
				else
				{
					if ( i != m_DrawList.end())
						i++;// we have to increment for the while loop
				}
				
				// If the child is a window, we need to cycle through the window's
				// Cdraw objects looking for one that matches this symbol for destruction
				if(pBase->mdsType == mds_window)
				{
					CdrawWin * pdrawWin = (CdrawWin *)pBase;
					if (pdrawWin->m_pWindow)
					{
						CChildFrame * pChildFrame = (CChildFrame *)pdrawWin->m_pWindow;
						
						if (::IsWindow(pChildFrame->m_hWnd))
						{
							CSDC625WindowView * pView =  (CSDC625WindowView *) pChildFrame->GetActiveView();
							pView->DestroySymbol(symblID);
						}
					}
				}
			}
		}
	}
}

BOOL CdrawBase::DisplayValue(CPoint point)
{
	CdrawBase * pBase = NULL;
	BOOL result = FALSE;

	pBase = HitTest(point);

	if (pBase)
	{	
        result = pBase->DisplayValue();
	}

	return result;
}

BOOL CdrawBase::DisplayValue()
{
    return FALSE;
}

void CdrawBase::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDMAIN;    
}

BOOL CdrawBase::Pre(WORD wMessageID)
{	
	if (GetItem()) // Check to see if item is NULL
	{
		if (GetItem()->IsVariable())
		{
			hCVar* pVar = (hCVar*) GetItem();

			// If this is loop warning variable and the device has
			// change, show this warning to the user
			if (pVar->m_bLoopWarnVariable /*&& !pVar->isChanged()*/)
			{
				CString strLoopWarnMessage,strLoopWarnCaption;
				strLoopWarnMessage.LoadString(IDS_LOOP_WARN_VARIABLE_MESSAGE);
				strLoopWarnCaption.LoadString(IDS_LOOP_CHANGE_WARNING);
						CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:was:>
				int ret = GETMAINFRAME->MessageBox(			 // stevev 08oct10:was:>AfxGetMainWnd()->MessageBox(
					strLoopWarnMessage,strLoopWarnCaption,MB_ICONEXCLAMATION|MB_YESNO);
		
				if(ret == IDNO)
				{
					return FALSE;
				}
			}
		}
	}

	return TRUE;
}

//Shared by CdrawVar & CdrawEnum 
BOOL CdrawBase::Post(WORD wMessageID)
{
	if (GetItem()) // Check to see if item is NULL
	{
		if (GetItem()->IsVariable() && ! inActions)
		{
			hCVar* pVar = (hCVar*) GetItem();

//stevev 22aug07-par727-do_always			if (pVar->isChanged())/*** NOTE: I thought these were Always executed???*/
//			{
                // If the contol is not enabled, it is probably due to the pre-edit method aborted or it failed,
                // In this case, make sure the post edit is not executed, POB - 28 Aug 2008
                if (IsEnabled())
                {
				inActions = true;
				RETURNCODE rc = pVar->doPstEditActs();
				inActions = false;
						
				if(rc != SUCCESS)
				{
					LOGIT(CERR_LOG|UI_LOG,"Call to post edit action returned an error.\n");
				}
                }

                // Make sure that the control is enabled before exiting the control, POB - 28 Aug 2008
				if (mdsType < mds_var)// non-primative to get rid of error message
					Enable(true);

/* #ifdef XMTRDD... xmtr will never show a window/dialog so this will never occur - remove it
				rc = pVar->doPstUserActs();
						
				if(rc != SUCCESS)
				{
					LOGIT(CERR_LOG|UI_LOG,"Call to post user edit action returned an error.\n");
				}
endif \\ XMTRDD **/
				// Update the display value
				Update(UPDATE_ONE_VALUE);
//			}
		}
	}

	return TRUE;
}

CString CdrawBase::GetLabel()
{
	wstring szLabel, dummyHelp;// try wide 28jan08
	bool    stringFull = false;


    if (m_pItemRef)
    {
        m_pItemRef->getItemLabel(szLabel, stringFull,dummyHelp); // could be different from item label 
    }
    
    // If string name is NULL (empty), the fillSymb() was either not called prior
    // to this function or the item is not referenced (ie, root menu), POB - 13 Aug 2008
    // Go ahead and call the Item's Label so that the object has a name.
    if ( szLabel.empty() && GetItem() )
	{
		m_pItem->Label(szLabel);
	}
        
#ifdef _DEBUG
	char* pC = new char[szLabel.size()+1];
	UnicodeToASCII(szLabel.c_str(), pC, szLabel.size()+1);
	delete[] pC;
#endif


    TRACE(_T("CdrawBase::GetLabel() - Item's Label = %s\n"), szLabel.c_str());

	return szLabel.c_str();
}

/*Vibhor 240904: Start of Code*/

CString CdrawBase::GetHelp()
{
	
	wstring szHelp, dummyLabel;
	bool    stringFull = false;

    if (m_pItemRef)
    {
        m_pItemRef->getItemLabel(dummyLabel,stringFull, szHelp);  
    }
    
    // If string name is NULL (empty), the fillSymb() was either not called prior
    // to this function or the item is not referenced (ie, root menu), POB - 13 Aug 2008
    // Go ahead and call the Item's Label so that the object has a name.
    if ( szHelp.empty() && GetItem() )
	{
		m_pItem->Help(szHelp);
	}

    TRACE(_T("CdrawBase::GetHelp() - Item's Help = %s\n"), szHelp.c_str());

	return szHelp.c_str();
}

/*Vibhor 240904: End of Code*/

CRect CdrawBase::GetRect()
{
	CWnd * ptr;
	CRect rect(0,0,0,0);

	ptr = m_pDynCtrl->GetDlgCtrl(m_cntrlID);

	if (ptr)
	{
		ptr->GetWindowRect(&rect);
	}

	return rect;
}

/*
 * Update all the controls with this symbol number
 * This function is called when a value has changed in the device
 * and the screen needs to be updated.
 * This acts like an CView::OnUpdate.
 *
 * stevev 17oct06 - modified to return Size_Changed (T), no-change(F)
 *					to tell the caller if it must recalculate its layout.
 * stevev 08dec06 - moved most of this to the layout class - CdrawLayout
 *					Item updates are now called directly, not thru here
 *					This one has been modified to show an error and return.
 */
bool CdrawBase::Update(long type,long SymID)
{
	LOGIT(CLOG_LOG,"--- BASE version of Update() called.\n");
	return false;

	CdrawBase * pBase;
#ifdef XX_DEBUG
	string st;
	if (type == -1 )                      st = "-1";
	else if (type == UPDATE_STRUCTURE)    st = "structure";
	else if (type == UPDATE_ADD_DEVICE)   st = "new device";
	else if (type == UPDATE_REMOVE_DEVICE)st = "kill device";
	else if (type == UPDATE_ONE_VALUE)    st = "value";
	else                                  st = type;
	DEBUGLOG(CLOG_LOG,"MSG:CdrawBase.Update %s  %s 0x%04x\n",
													st.c_str(),menuStyleStrings[mdsType],SymID);
#endif
	bool result = false; // true @ this control's size changed
//if ( SymID < 0x0F )
//{
//	pBase = NULL; // for a brkpt
//}
ASSERT(SymID > 0x0F);
	bool subres;         // return val from called update
	ulong symbol = SymID;

	if (SymID == -1)//pre 04dec06 was 0)
	{
		// Update All controls
		symbol = m_symblID;
	}
// dependency should take care of this...check later
	if ( mdsType == mds_var)
	{
		hCVar* pVar = (hCVar*) GetItem();
		if ( pVar->getUnitVarId() == SymID )
		{
			symbol = m_symblID;// make us update
			type   = UPDATE_STRUCTURE;// force all of it
		}
	}

	/*Vibhor 123104: Start of Code*/

	if(mdsType == mds_scope || mdsType == mds_grid) /* Added grid to update - POB, 4 Oct 05 */
	{// stevev 8may07-this should never occur.overload of Update should put us directly into scope
		subres = Update(type,SymID);
	}

	/*Vibhor 123104: End of Code*/
	else	//stevev 17oct06
	if (m_symblID == symbol)
	{
		DEBUGLOG(CLOG_LOG,"              Match 0x%04x\n",SymID);
		// Handle the event here         X
		subres = Update(type,SymID);
	}
	
	// Iterate through child list
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		//DEBUGLOG(CLOG_LOG,"+ + + + + + + + + + + \n");
		// Look for this control id in its children
		pBase = (*i);

		//This probably will need to be over-ridden in the child objects
		// Call the base class on this child object to start the search
		// on it and its children
		/*result = pBase->Update(SymID);*/
		pBase->CdrawBase::Update(type, SymID);
		//DEBUGLOG(CLOG_LOG,"- - - - - - - - - - - \n");
	}
		
	return result;
}


void CdrawBase::UpdateAll()
{
	Update(-1);
}

BOOL CdrawBase::DisplayHelp(CPoint point)
{
    CdrawBase * pBase = NULL;
    /* code removed 13aug08 paulb */
    BOOL result = FALSE;
    //* code removed 13aug08 paulb */

    pBase = HitTest(point);

    if (pBase)
    {		
        // modified 12aug08 paulb
        CHelpBox helpBox(pBase->GetLabel(), pBase->GetHelp());
        helpBox.Display();
        result = TRUE;// POB Aug08
    }

    return result;
}

BOOL CdrawBase::DisplayLabel(CPoint point)
{
	CdrawBase * pBase = NULL;
	BOOL result = FALSE;

	pBase = HitTest(point);

	if (pBase)
	{		
		CLabelDlg dlgLabel("", pBase->GetLabel());  // When displaying label, provide no caption
		dlgLabel.sdcModl();// DoModal();
        result = TRUE;
	}

	return result;
}

/* 
 * CdrawBase::HitTest: This function finds a CdrawBase object that lives
 * at a certain point in a window
 * 
 * return:  
 *
 *  TRUE  - Fount a control        (pointer to CdrawBase)
 *  FALSE - Did not find a control (NULL)
 *
 */

/***************************************************************************
 * FUNCTION NAME: CdrawBase::HitTest
 *
 * DESCRIPTION:
 *   Finds a CdrawBase object that lives at a certain point in a window
 *
 * ARGUMENTS:
 *   CPoint  The point where the mouse is located
 *
 * RETURNS:
 *   TRUE    Found a control        (pointer to CdrawBase)
 *	 FALSE   Did not find a control (NULL)
 *
 ***************************************************************************/
CdrawBase * CdrawBase::HitTest(CPoint point)
{
	CWnd * ptr;
	CTabCtrl * pTabCtrl;
	CdrawBase * pChild;
	CdrawBase * pFound = NULL;
	CdrawBase * pBase = NULL;
	CRect rect(0,0,0,0);
	CString strHelp;
	int curPage = -1;

	if (m_pDynCtrl)
	{
		// Get the rectangular region in the windon for this DDL control
		rect = GetRect();

		// Check to see if this draw type is a "tab"
		if (mdsType == mds_tab)
		{
			pTabCtrl = m_pDynCtrl->GetTabWnd();

			if (pTabCtrl)
			{
				// Get the current page selection (zero based)
				//curPage = pTabCtrl->GetCurFocus();
				curPage = pTabCtrl->GetCurSel();
			}
		}
	}
	else
	{
		// If this is a page, the Dynamic Control object (above)
		// will be NULL.  Therefore, we need access to its parent's
		// (CdrawTab) Dynamic Control object which is held in the 
		// m_pDynTabCtrl member
		if (mdsType == mds_page)
		{
			if (m_pDynTabCtrl)
			{
				ptr = m_pDynTabCtrl->GetDlgCtrl(m_cntrlID);
				ptr->GetWindowRect(&rect);
			}
		}

		if (mdsType == mds_window)
		{
			CMainFrame * pMainWnd = (CMainFrame *)(AfxGetApp()->m_pMainWnd);
			CFrameWnd* pFrame = pMainWnd->GetActiveFrame();
			CView * pView = pFrame->GetActiveView();
			pView->GetWindowRect(&rect);
		}
	}

	// Iterate through child list
	DrawList_t::iterator i;
	for ( i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Look for this control id in its children
		pChild = (*i);

		if (pChild->mdsType == mds_page && pChild->m_nPage != curPage)
		{	
			// if this child is a "page" and is not currently selected
			// continue through the child list until the one that is
			// selected is found
			continue;
		}
		pFound = pChild->HitTest(point);

		if (pFound)
		{
			// We have found a child that matches the hit test
			pBase = pFound;
			break;
		}
	}

	// if non of its children are found then
	// use this (its parent) as the help menu
	if (!pFound && rect.PtInRect(point))
	{
		pBase = this;
		strHelp = pBase->GetHelp();
	}

	return pBase;
}

hCitemBase* CdrawBase::GetItem()
{	
	if (!m_pItem && m_symblID)
	{	RETURNCODE rc;
//x		if (m_pMenuTree==NULL || m_pMenuTree->pItemBase == NULL)
//x		{
			CDDIdeDoc* pDoc = GetDocument();
			if ( pDoc == NULL )
			{
				return 0;
			}

			hCitemBase*    p_localItem;

			if (PCURDEV == NULL )
			{
				return NULL ; /// error -  no device!!!!!!!!!!!!
			}
			rc = PCURDEV->getItemBySymNumber(m_symblID, &p_localItem);
			if ( rc != SUCCESS || p_localItem == NULL )
			{
				return NULL; // error:  item I am supposed to be doesn't exist !!!!!!!!!!!!
			}

			m_pItem = p_localItem;
//x		}
//x		else // both not-null
//x		{
//x			m_pItem = m_pMenuTree->pItemBase;
//x		}
	}

	return m_pItem;
}

BOOL CdrawBase::Cancel(ulong SymID)
{
	CdrawBase * pBase;
	BOOL result = FALSE; // TRUE = at least once control was updated
	ulong symbol = SymID;

	if (SymID == 0)
	{
		// Cancel All controls
		symbol = m_symblID;
	}

	if (m_symblID == symbol)
	{
		// Handle the event here
		
		// Right now only variables are canceled
		if (GetItem())
		{
			if (GetItem()->IsVariable())
			{
			//	hCVar* pVar = (hCVar*) GetItem();
				((hCVar*) GetItem())->CancelIt();	// Cancel the change
			//	Update();			// refresh the display value
				result = TRUE;
			}
		}
	}

	// Iterate through child list
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Look for this control id in its children
		pBase = (*i);

		//Call cancel on this child
		result = pBase->Cancel(SymID);
	}

	return result;
}

//DEL BOOL CdrawBase::Test(ulong SymID)
//DEL {
//DEL 	CdrawBase * pBase;
//DEL 	BOOL result = FALSE; // TRUE = at least once control was updated
//DEL 	ulong symbol = SymID;
//DEL 
//DEL 	if (SymID == 0)
//DEL 	{
//DEL 		// Update All controls
//DEL 		symbol = m_symblID;
//DEL 	}
//DEL 
//DEL 	if (m_symblID == symbol)
//DEL 	{
//DEL 		// Handle the event here
//DEL //		Update();
//DEL 		if (GetItem())
//DEL 		{
//DEL 			if (GetItem()->IsVariable())
//DEL 			{
//DEL 				TRACE(_T("CdrawBase::Test, Dyncontrol = %d, symbolID = %d, ctrID = %d\n"), m_pDynCtrl, m_symblID, m_cntrlID);
//DEL 				result = TRUE;
//DEL 			}
//DEL 		}
//DEL 	}
//DEL 
//DEL 	// Iterate through child list
//DEL 	DrawList_t::iterator i;
//DEL 	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
//DEL 	{
//DEL 		// Look for this control id in its children
//DEL 		pBase = (*i);
//DEL 
//DEL 		//This probably will need to be over-ridden in the child objects
//DEL 		result = pBase->Test(SymID);
//DEL 
//DEL 	}
//DEL 
//DEL 		
//DEL 	return result;
//DEL }

int CdrawBase::GetIndex()
{
	return m_itemIndex;
}

void CdrawBase::SetIndex(int itemIndex)
{
	m_itemIndex = itemIndex;
}

// IsValid is not currently being called in the application
// This code may be deleted later on
BOOL CdrawBase::IsValid()
{
	BOOL bResult = TRUE;  // always assume the item is valid

	if (GetItem())
	{
		bResult = GetItem()->IsValid();
	}

	return bResult;
}


void CdrawBase::Enable(BOOL bEnable)
{
	LOGIT(CERR_LOG|CLOG_LOG,"Programmer Error: drawBase::Enable Called.\n");
}

BOOL CdrawBase::IsEnabled()
{
    return TRUE;
}

/* * * * * * * * * * VARIABLE * * * * * * * * * */

CdrawVar::~CdrawVar()
{
	CWnd * ptr;

	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

		if (ptr)
		{
			// Destroy the Label control (CWnd object)
			ptr->DestroyWindow();
		}

		ptr = m_pDynCtrl->GetDlgCtrl( m_nUnitID );
		
		if (ptr)
		{
			// Destroy the Unit Relation control (CWnd object)
			ptr->DestroyWindow();
		}
	}
#ifdef PUBLISHTEST
	if ( sub_handle >= 0 )
	{
TRACE(_T("~DrawVar Unsubscribing handle 0x%x in thread 0x%x\n"),sub_handle,GetCurrentThreadId());
		hCglobalServiceInterface::myDevicePtr((GetItem())->devHndl())->unsubscribe(sub_handle);
	}
#endif
}

#ifdef PUBLISHTEST
RETURNCODE CdrawVar::publish(dataStructList_t dsL)
{ 
	dataStructList_t::iterator iU;
	
	for ( iU = dsL.begin(); iU != dsL.end(); ++iU)
	{
		clog<<"PUBLISHED: "<<m_symblID<<" @ "<< iU->timestamp << " with " << iU->dpL.size() <<" items"<< endl;
	}
	return SUCCESS;
}
#endif

void CdrawVar::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CColorEdit * pEdit;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent,centeringOffset);

	// Initialize this control
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->AddPageCtrl(m_nLabelID, m_nPage);
		m_pDynTabCtrl->AddPageCtrl(m_nUnitID,  m_nPage);
	}

	pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

#ifdef PUBLISHTEST
	ddbItemList_t sublist;
	if (GetItem())
	{
		sublist.push_back(GetItem());
TRACE(_T("Var is subscribing w/handle 0x%02x symbol 0x%04x\n"),sub_handle,m_symblID);
		int rc = 
		hCglobalServiceInterface::myDevicePtr((GetItem())->devHndl())
				->subscribe(sub_handle, sublist,(hCpubsub*)this,WINDOW_UPDATE_RATE);
	}
#endif

	// Setup the type of variable
	SetupType(pWnd);
	
	// This is the initial update
	Update(UPDATE_ADD_DEVICE);

	// This is only needed to produce the yellow highligthing 
	// for all all changed variables on its initial update
	if (((hCVar*) GetItem())->isChanged())
	{
		//The user has made a change
		pEdit->Highlight(TRUE);
	}
	else
	{
		pEdit->Highlight(FALSE);
	}
}

void CdrawVar::Execute(WORD wMessageID, LPARAM lParam)
{
	if ( m_pDynCtrl == NULL )
	{// this condition should have been filtered long before we get here, but just in case...
		LOGIT(CLOG_LOG,"CdrawVar.Execute %s 0x%04x does not have a DynCtrl Yet!\n",
			m_symblID);
		return ;
	}	

	CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	hCVar* pVar        = (hCVar*) GetItem();
	CString strValue;

	// kill focus or return, others are filtered or we don't care

	// if the user has hit the enter button or changed the focus
	// away from this control (EN_KILLFOCUS below), write the
	// value to the display value.

	// when return is hit, we gat a VK_RETURN.  If the value is outofrange a messagebox
	// will try to be launched but has to send a kill focus message to us.  We handle the 
	// kill focus in a rentrant manner (well) but it fails outofrange and puts up a messagebox
	// when the user acknowledges the box, it closes, finishing this routine, and the initial
	// box (that sent the lost-focus message)comes up and repeats what the first one said.
	if( (wMessageID == VK_RETURN) ) //  handled diferently now... ||	// take edit, keep focus
	{
		amReturning = true;
		WriteVal();
		amReturning = false;
	}
	else
	if ( !amReturning && wMessageID == EN_KILLFOCUS)   // take edit, loose focus
	{	// Get the changes from the user
		WriteVal();
	}

    if (pEdit)
    {
	if (pVar->isChanged())
	{
		//The user has made a change
		pEdit->Highlight(TRUE);
	}
	else
	{
		pEdit->Highlight(FALSE);
	}
}
}
/*2 UPDATE_STRUCTURE     
 *3 UPDATE_ADD_DEVICE   
 *5 UPDATE_REMOVE_DEVICE 
 *6 UPDATE_ONE_VALUE     
 */
bool CdrawVar::Update(long type,long SymID)
{
	CWnd * ptr;
	hCVar* pVar = NULL;

	if ( GetItem()->IsVariable() )
		pVar = (hCVar*) GetItem();
	else
	{
		LOGIT(CERR_LOG,"CdrawVar.Update %s 0x%04x is NOT a Variable!\n",m_symblID);
		return true;
	}

	if ( SymID != m_symblID && SymID != -1 && SymID != pVar->getUnitVarId())// -1 matches all
		return false; //   - it is not for us...

	/*** it is for us ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -variable-\n",SymID);
#ifdef _DEBUG
	pVar->isChanged();// to get a log out
#endif
	bool result = false; // true @ this control's size changed

    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawVar.Update %s 0x%04x does not have a DynCtrl Yet!\n","Var",m_symblID);
		return result;
	}
	DEBUGLOG(CLOG_LOG,"MSG:Updating %s (%s) Variable 0x%04x (%s) to %s\n",
		(type == UPDATE_STRUCTURE)?"STRUCT":((type == UPDATE_ONE_VALUE)?"VALUE":""),
		menuStyleStrings[mdsType],	pVar->getID(),pVar->getName().c_str(), 
		/*pVar->getDisplayValueString()*/TStr2AStr(pVar->getDispValueString()).c_str());

	

	if ( type == UPDATE_STRUCTURE || type == UPDATE_ADD_DEVICE || SymID == pVar->getUnitVarId())
	{// update structure or add device
		// Update the Label 
		// ** ptr  to Label Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
		if (ptr)
		{
			CString t = GetLabel();
			ptr->SetWindowText(t);
			ptr->Invalidate();// stevev 07dec06
		}

		// Initalize the unit string
		// ** ptr  to Unit Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_nUnitID );
		if (ptr)
		{	//30nov06 - use the more complete version:was::>
			//									ptr->SetWindowText( pVar->getUnitStr().c_str() );
			tstring t;
			pVar->getUnitString(t);
			ptr->SetWindowText(t.c_str());
			ptr->Invalidate();// stevev 07dec06
            TRACE(_T("CdrawVar::Update() - Engr Unit String  = %s\n"), t.c_str());
		}

		// ** ptr  to Value Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		if (ptr)
		{	// Check for read-only attributes
			ptr->EnableWindow(!(pVar->IsReadOnly()) && !m_bIsReadOnly);
		}
	}// else update value or remove devic

	if(type == UPDATE_ONE_VALUE || type == UPDATE_ADD_DEVICE)
	{	// update its value	
		// Run any Refresh action--- stevev 07dec06 - my choice, only refresh on VALUE	
		RETURNCODE rc = pVar->doRefreshActs();
		if(rc != SUCCESS)// stevev 07dec06 - refresh method error looses update
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to variable refresh action returned an error.\n");
			return false; // An error - allow this to work immediately again
		}	
			
		// NOTE:  This will cause a EN_CHANGE to occur
		//        to turn off any yellow highligting that has
		//        ocurred.
		// ** ptr  to Value Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		ptr->SetWindowText(ReadVal());

	}
	else
	{// remove device - nothing now, just be sure to get a ptr
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	}

	ptr->Invalidate();

	// This variable has been updated.  Any editing has been erased 
	// as a result of this update.  Allow for the editing to begin 
	// again
	// no longer used 11sep07  m_bTriggerEdit = FALSE;
	return result;
}

BOOL CdrawVar::DisplayValue()
{
    BOOL result = FALSE;

    hCVar* pVar = (hCVar*) GetItem();

    CString strValue(ReadVal());

    // Get the engineering units
    tstring t;
    pVar->getUnitString(t);
    CString strUnits(t.c_str());

    if (!strUnits.IsEmpty())
    {
        strValue = strValue + " " + strUnits;
    }

    // Added to ensure passwords only shows astreks
    if (pVar->IsPassword())
    {
        for (int i=0; i < strValue.GetLength(); i++)
        {
            strValue.SetAt(i, '*' );
        }
    }

    // Show the variable value using the CLabelDlg class
    CLabelDlg dlgVarValue(GetLabel(), strValue);
    dlgVarValue.sdcModl();// DoModal();
    result = TRUE;

	return result;
}

void CdrawVar::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDVAR;    
}

void CdrawVar::Edit()
{
	ITEM_ID lWaoRel = 0;
	hCVar*  pVar    = (hCVar*) GetItem();

	if ( pVar->IsReadOnly() || m_bIsReadOnly )
	{
		// Can not edit a variable that is "read-only"
		return;
	}	
	//This will act as a way to trigger an edit event
	// no longer used 11sep07 if (m_bTriggerEdit || inActions)
	if (inActions)
	{	// The process has already started.
		// Don't need to run it again
		return;
	}

	// Run any pre-edit action
	if (GetItem()->IsVariable())
	{
		lWaoRel = pVar->getWaoRel();// set info while we have the pointer
	}
	else
	{
		return; // error
	}

	// no longer used 11sep07 m_bTriggerEdit = TRUE;
	TRACE(_T("CdrawVar::Edit()- Edit event triggered\n"));

	if (lWaoRel > 0 && pVar != NULL)
	{
		//CDDLVariable * pV = new CDDLVariable(pVar);
		//CWaoDlg WaoDlg(pV, lWaoRel);
		//above replaced 6aug08
		CWaoDlg WaoDlg(pVar, lWaoRel);

		inActions = true;			 // bracket the modal - for now
		int nRet  = WaoDlg.DoModal();// does pre edit actions...
		inActions = false;

        // If there was a post edit action on any of the variables in the WAO, it has already been executed,
        // do not allow it to be executed again.
        Enable(false);  

		//delete pV;
		// no longer used 11sep07  m_bTriggerEdit = FALSE;// once we are back from modal, the edit process is over.stevev 21aug07
		return;
	}
	else if (pVar != NULL)// just an edit
	{
		inActions = true;
		RETURNCODE rc = pVar->doPreEditActs();
		inActions = false;
			
		if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
            Enable(false); // The method has either failed or been aborted, do not let the user edit the value, POB - 28, Aug 2008
			return; // An error - allow this to work immediately again
		}
		// Show the Edit Display Format
		CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
        
        if (pEdit == NULL)
        {
            return;
        }

        // Normally this is handled on the CdrawVar::Execute()
        // But if this is on a Grid control, the edit control
        // has just been created, so we need to indicate again
        // that the value has changed, POB - 5/5/2014
        if (pVar->isChanged())
        {
            //The user has made a change
            pEdit->Highlight(TRUE);
        }
        else
        {
            pEdit->Highlight(FALSE);
        }
        
		pEdit->SetWindowText(ReadEditFormatVal());// stevev 13jun11 - re-enabled this line
        
		// reset the format, it could change due to conditionals
		int nMax_Chars,nDecimal_Limit;
		wstring eFmt;
		int type  = (VarType)( pVar->getEditFormatInfo(nMax_Chars, nDecimal_Limit, &eFmt) );
        
		pEdit->setFormatInfo(printfType2PES_type(type), nMax_Chars,nDecimal_Limit,nMax_Chars);
        
	}
	// else just return with no activity (error)
}


// Pre-Execute
BOOL CdrawVar::Pre(WORD wMessageID)
{
	if ( m_pDynCtrl == NULL )
	{
		return FALSE;
	}

	CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    
    if (pEdit== NULL)
    {
        return FALSE;
    }
   
	hCVar* pVar        = (hCVar*) GetItem();
	BOOL bResult       = TRUE;
	CString strValue;

/** now handled with focus events
	// Call this right after the user is done editing
	if (wMessageID == VK_RETURN)
	{
		TRACE(_T("User has made a change and is ready to set the value\n"));

		pEdit->GetWindowText(strValue);

		if (strValue == ReadVal() || strValue == ReadEditFormatVal())
		{
			// SetWindowText has been inacted but there is no 
			// real change.
			// Need the execute to work (return TRUE)
			//
			// We don't want any warning message
			// from the base class or pre-edit method
			// so we must return
			return TRUE;
		}
	
		if (!IsValid())
		{
			return FALSE;
		}

		bResult = CdrawBase::Pre(wMessageID);

		if (!bResult)
		{
			// This event takes place before the display value is
			// changed so there is no need to call CancelIt(), just
			// undo the change
			pEdit->Undo();
		}
	}
	*** no longer return, kill-focus **/

	if (wMessageID == EN_SETFOCUS)// prepare to edit
	{// the device state and window state should have been filtered before we were called
		Edit();// pre-edit actions done here
		return FALSE;// don't execute, this is all the pre-edit we need to do
	}

	if (  (wMessageID == EN_KILLFOCUS) //take edit, loose focus
		||(wMessageID == VK_RETURN)   )//take edit, keep focus
	{	
		/*- Now doing data validation checking in the WriteVal() function, POB - 13 Aug 2008
		if (!IsValid())
		{
			return FALSE;// in case it went invalid while we were editting
		}
		*/

		pEdit->GetWindowText(strValue);

		if (strValue == ReadVal() || strValue == ReadEditFormatVal())
		{	// SetWindowText has been enacted but there is no real change.
			// Need the execute to work (return TRUE)
			//
			// We don't want any warning message (skip base Pre) or pre-edit actions
			// so we must return true
			return TRUE;
		}
		// else isChanged so continue
		bResult = CdrawBase::Pre(wMessageID);// loop warning vars

		if (!bResult) // loop warning cancelled...
		{	// This event takes place before the Device Object display value has been
			// changed so there is no need to call CancelIt(), just undo the change
			pEdit->Undo();
		}
		// else - exit to run execute()
	}

	if (wMessageID == VK_ESCAPE)//undo edit, keep focus
	{
		pEdit->Undo();
		return FALSE; // we do not want to execute but keep on editting
	}

	return bResult;
}

// Post-Execute
BOOL CdrawVar::Post(WORD wMessageID)
{
	BOOL bResult = TRUE;

	// A change and editing is finished?
	//if(wMessageID == VK_RETURN)
	if( wMessageID == EN_KILLFOCUS )  
	{
		//Call base class for common action 
		bResult = CdrawBase::Post(wMessageID);
		
		/* Move to CdrawBase::Post - POB, 10 Dec 2004 */
		// Update the display value
		// Update();

		/* Move to CdrawVar::Update - POB, 10 Dec 2004 */
		// The edit process is accomplished.  Allow for it 
		// to begin again
        // m_bTriggerEdit = FALSE;
	}

	return bResult;
}

CRect CdrawVar::GetRect()
{
	CWnd * ptr;
	CRect rect(0,0,0,0);
	CRect rect2(0,0,0,0);

	// Call the base class to get the primary rectangle
	rect = CdrawBase::GetRect();

	// Determine the remaining portion of the CdrawVar
	// rectangle
	ptr = m_pDynCtrl->GetDlgCtrl(m_nLabelID);
	
	if (ptr)
	{
		ptr->GetWindowRect(&rect2);
		rect.left = rect2.left;
	}

/*	ptr = m_pDynCtrl->GetDlgCtrl(m_nUnitID);
	
	if (ptr)
	{
		ptr->GetWindowRect(&rect2);
		rect.right = rect2.right;
	}*/

	return rect;
}

CString CdrawVar::ReadVal()
{
	CString strValue;
	hCVar* pVar;
		
	pVar = (hCVar*) GetItem();
	
/*807    strValue = pVar->getDispValueString().c_str();
*/
	wstring value;
	value = pVar->getValidDispValueString();
	strValue = value.c_str();
/* 807 end */

    if (pVar->VariableType() == vT_HartDate)
	{
        if (strValue.IsEmpty())
        {
	        strValue = "00/00/00";
        }
	}

	return strValue;
}

void CdrawVar::WriteVal()
{
    RETURNCODE rc = SUCCESS;
	CString strValue;
	hCVar* pVar;
	CValueVarient value;

	CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	pEdit->GetWindowText(strValue);

    // No Need to trim spaces here.  validateTrim() will
    // handle all necessary triming, POB - 5/9/2014;

	pVar = (hCVar*) GetItem();
	int entryStatus = pVar->getWriteStatus();

	/* stevev 05aug11 - exiting an unread variable on a window marks it as user written
		even if the user never touched it.  It then has to show a value from then on.
		Changed to not set user written unless the user changes the value.
	**/
	BOOL userChanged = pEdit->GetModify( );		
	
	if (userChanged)
	{	// Removed data validation code. All data validation is being done in device object 
		// (i.e., setDisplayValueString), POB - 13 Aug 2008

		pVar->setWriteStatus(1);

		wstring ls;

		ls = (LPCTSTR)strValue;

		rc = pVar->setDisplayValueString(ls);
	}

    if ( rc )
	{// problem: tell the user and reset the screen and exit with the box active

        // Removed no longer used code - 13 Aug 2008	
        pVar->setWriteStatus(entryStatus);// return it to original
        // Removed no longer used code - 13 Aug 2008
		
        if ( rc == APP_STRING_NOT_ASCII )
		{
			AfxMessageBox(M_UI_NON_ASCII_STR);
		}
		else
		if( rc == APP_OUT_OF_RANGE_ERR )
		{
			AfxMessageBox(M_UI_INVALID_ENTRY);
		}
		else
		if( rc == APP_PARSE_FAILURE )
		{
/* temporary suppression due to popup when you mouse to another box without modifying 
the unitialized original			
			CString tmp;
			tmp = M_UI_PARSE_NUM_FAIL;
			tmp.Format(L"%s (%s)in %s",M_UI_PARSE_NUM_FAIL,strValue,pVar->getWName().c_str());
			AfxMessageBox(tmp);
**** end temporary suppression ***/			
		}
		else
		{
			AfxMessageBox(M_UI_INPUT_FAILURE);
		}
	}
}

CString CdrawVar::ReadEditFormatVal()
{
	CString strValue;
	hCVar* pVar;
	//CString strDisp_val;
	
	pVar = (hCVar*) GetItem();
	// stevev 20sep07 - get rid of extra 'stuff'
	if (pVar)
    {
		strValue = pVar->getEditValueString().c_str();
	}
	
	return strValue;
/* code removed 13aug08 paulb */
}

/* code removed 13aug08 paulb */

void CdrawVar::FormatFloat(CString strEdt_fmt,CString val,CString &strDisp_val )
{
	int len=0;
	
	if ( strEdt_fmt.Find(_T('%')) != -1 )
	{
		TCHAR buf[10]={0};
		if(strEdt_fmt.Find(_T('f')) != -1)
		{
			if ( strEdt_fmt.Find(_T('.')) != -1)
			{
				CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find(_T('.'))-1);
				_tcscpy(buf,(LPCTSTR)tmp);
				len=_ttoi(buf);
				if ( len == 0 ) len=HANDHELD_FLOAT_MAXVAL_LEN;
			}	
			else
			{
				CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find(_T('f'))-1);
				if(tmp.IsEmpty())
				{
					len=HANDHELD_FLOAT_MAXVAL_LEN;
				}
				else
				{
					_tcscpy(buf,(LPCTSTR)tmp);
					len=_ttoi(buf);
				}				
			}
		}
/*<START>Added by ANOOP 29MAR2004 to support the %g format	*/
		else if(strEdt_fmt.Find(_T('g')) != -1)
		{
			if ( strEdt_fmt.Find(_T('.')) != -1)
			{
				CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find(_T('.'))-1);
				_tcscpy(buf,(LPCTSTR)tmp);
				len=_ttoi(buf);
				if ( len == 0 ) len=HANDHELD_FLOAT_MAXVAL_LEN;
			}	
			else
			{
				CString tmp=strEdt_fmt.Mid(1,strEdt_fmt.Find(_T('g'))-1);
				if(tmp.IsEmpty())
				{
					len=HANDHELD_FLOAT_MAXVAL_LEN;
				}
				else
				{
					_tcscpy(buf,(LPCTSTR)tmp);
					len=_ttoi(buf);
				}
				
			}
		}

/*<END>Added by ANOOP 29MAR2004 to support the %g format	*/
		
		if( val.Find(_T('.')) != -1)
		{
			CString tmpVal=val.Mid( 0,val.Find(_T('.')) );
			int tmpLen=tmpVal.GetLength();

			if( tmpLen  < len)
			{
				int Tmp= len - tmpLen;
				strDisp_val=val.Mid( 0,tmpLen + Tmp );
			}
			else if( tmpLen == len )
			{
				strDisp_val=val;
			}
			else if ( tmpLen  > len)
			{
				int Tmp=  tmpLen - len;
				strDisp_val=val.Mid( 0,tmpLen - Tmp );
			}

		}
		else
		{
			CString tmpVal=val.Mid( 0,val.GetLength() );
			int tmpLen=tmpVal.GetLength();

			if( tmpLen  < len)
			{
				int Tmp= len - tmpLen;
				strDisp_val=val.Mid( 0,tmpLen + Tmp );
			}
			else if( tmpLen == len )
			{
				strDisp_val=val.Mid( 0,tmpLen);
			}
			else if ( tmpLen  > len)
			{
				int Tmp=  tmpLen - len;
				strDisp_val=val.Mid( 0,tmpLen - Tmp );
			}

		}
	}
}

void CdrawVar::SetupType(CWnd * pWnd)
{
	int nMax_Chars;
	int nDecimal_Limit;
	WORD  Parse_style;
	hCVar* pVar;

	pVar = (hCVar*) GetItem();

	CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	
	/* massive code removal from this reoutine...13aug08 paulb */
    if (pVar->VariableType() == vT_HartDate)
	{
        nMax_Chars=MAX_DATE_LENGTH;
		pEdit->SetLimitText(nMax_Chars); 
		pEdit->SubclassEdit(0x7F, pWnd, PES_DATE,nMax_Chars,0,0);
	}
    else /// its the catch-all edit box
	{
		wstring eFmt;
        int type  = (VarType)( pVar->getEditFormatInfo( nMax_Chars, nDecimal_Limit, &eFmt) );
		Parse_style = printfType2PES_type(type);

		if (pVar->IsPassword() )
		{
			pEdit->SetPasswordChar('*');
		}
					
		if (nMax_Chars >0)
		{
			pEdit->SetLimitText(nMax_Chars); 
		}
		pEdit->SubclassEdit(0x7F, pWnd, Parse_style,nMax_Chars,nDecimal_Limit,nMax_Chars);
	}// end else is catch-all edit box
}

// Removed CdrawVar::IsValid() function. This function was doing data verification that is now being completely checked
// in the device object.  Furthermore, the IsValid() function is wrongly named since it misleads the developer
// in believing it is checking for DD VALIDITY state, POB - 13 Aug 2008

void  CdrawVar::staleDynamics(void)
{	
	hCVar* pVar = (hCVar*) GetItem();
	
	// too easy  if (pVar->IsDynamic() )
	if ( pVar->IsValid()   &&   
		 pVar->IsDynamic() &&	/*This is not an unconfirmed write*/						
		(pVar->getDataState() != IDS_NEEDS_WRITE && (! pVar->isChanged()) ) &&		 
		!pVar->IsWriteOnly() && /* don't try and read write-only variables*/
		pVar->getDataState() != IDS_STALE   )// don't do it redundently
	{
		DEBUGLOG(CLOG_LOG,"WinStale:");
		pVar->MakeStale();
	}// else do nothing
}

void CdrawVar::Enable(BOOL bEnable)
{
    CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    pEdit->Enable(bEnable);
}


BOOL CdrawVar::IsEnabled()
{
    CColorEdit * pEdit = (CColorEdit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    return pEdit->IsEnabled();
}

/* * * * * * * * * * VARIABLE INDEX * * * * * * * * * */
// stevev - 28jan05
// POB - 31 March 05, completed
CdrawIndex::~CdrawIndex()
{
	CWnd * ptr;

	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

		if (ptr)
		{
			// Destroy the Label control (CWnd object)
			ptr->DestroyWindow();
		}
	}
}

void CdrawIndex::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
// stevev shows 2B unused 29jan09...	CClrComboBox * pVarIndex;
	CStringArray enumList;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->AddPageCtrl(m_nLabelID, m_nPage);
	}
/******* this action is now done in the common area of Update() below **************
	pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	// Get the enumeration list to populate the combo box
	GetValidValueList(enumList);

	// Populate the combo box
	for (int i=0; i < enumList.GetSize(); i++)
	{
		CString List = enumList.GetAt(i);

		if (List != "")
		{
			int nIndex = pVarIndex->AddString(enumList.GetAt(i));
			pVarIndex->SetItemData(nIndex, i );  // i holds the DD index value of the device
		}
	}
*********** end now-done-in-Update **********/
    
    // Height of the non-drop down portion of the combo box
    // so that matches the size of the edit box
	CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    pVarIndex->SetItemHeight(-1, ROWSTEPSIZE+8);

	// This is the initial update
	Update(UPDATE_ADD_DEVICE);

	// This is no need to call Execute() since Update() above will
	// make that call for us
}

void CdrawIndex::Execute(WORD wMessageID, LPARAM lParam)
{
	if ( m_pDynCtrl == NULL )
	{// this condition should have been filtered long before we get here, but just in case...
		LOGIT(CLOG_LOG,"CdrawIndex.Execute %s 0x%04x does not have a DynCtrl Yet!\n",
			m_symblID);
		return ;
	}	
	CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCVar* pVar              = (hCVar*) GetItem();
	CString strValue;

	// Determine if the user has made a change to the
	// combo box
	//if(wMessageID == CBN_CLOSEUP)
	if( (wMessageID == VK_RETURN)  ||	// take edit, keep focus
		(wMessageID == EN_KILLFOCUS) )  // take edit, loose focus
	{
		// Get the changes from the user
		WriteVal();
	}

	if (pVar->isChanged())
	{
		//The user has made a change
		pVarIndex->Highlight(TRUE);
	}
	else
	{
		pVarIndex->Highlight(FALSE);
	}
/*** stevev 11sep07 - these are handled in Pre
	if(wMessageID == EN_SETFOCUS)
	{

	}

	if (wMessageID == CBN_DROPDOWN)
	{
		// Using WM_LBUTTONDOWN (as in the case CdrawVar)
		// for calling Edit() does not work in CComboBox
		// It has the negative effect of calling CBN_SELENDCANCEL
		// instead so the dropdown window is non-fuctional
		Edit();
	}
***/
}

bool CdrawIndex::Update(long type,long SymID)
{
	CWnd * ptr;
	hCVar* pVar = NULL;
	CClrComboBox * pVarIndex;
	CStringArray enumList;

	if ( SymID != m_symblID && SymID != -1 )// -1 matches all
		return false; //   - it is not for us...

	/*** it is for us ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -index-\n",SymID);
	bool result = false; // true @ this control's size changed

    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawIndex.Update %s 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
		return false;
	}
	if ( GetItem()->IsVariable() )
		pVar = (hCVar*) GetItem();
	else
	{
		LOGIT(CERR_LOG,"CdrawIndex.Update %s 0x%04x is NOT a Variable!\n",m_symblID);
		return false;
	}
	DEBUGLOG(CLOG_LOG,"MSG:Updating %s (%s) Variable 0x%04x (%s) to %s\n",
		(type == UPDATE_STRUCTURE)?"STRUCT":((type == UPDATE_ONE_VALUE)?"VALUE":""),menuStyleStrings[mdsType],
		pVar->getID(),pVar->getName().c_str(), pVar->getDispValueString().c_str());
	
	// always - populate the list to the latest valid values
	pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	pVarIndex->ResetContent();// clears everything

	// Get the enumeration list to populate the combo box
	GetValidValueList(enumList);

	// Populate the combo box
	for (int i=0; i < enumList.GetSize(); i++)
	{
		CString List = enumList.GetAt(i);

		if (List != "")
		{
			int nIndex = pVarIndex->AddString(enumList.GetAt(i));
			pVarIndex->SetItemData(nIndex, i );  // i holds the DD index value of the device
		}
	}

	if ( type == UPDATE_STRUCTURE || type == UPDATE_ADD_DEVICE )
	{// update structure or add device
		// Update the Label 
		// ** ptr  to Label Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
			ptr->Invalidate();// stevev 07dec06
		}
		// done above  pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		// Check for read-only attributes
		pVarIndex->EnableWindow(!(pVar->IsReadOnly()) && !m_bIsReadOnly);

	}// else update value or remove device

	if(type == UPDATE_ONE_VALUE || type == UPDATE_ADD_DEVICE)
	{	// update its value	
		// Run any Refresh action--- stevev 07dec06 - my choice, only refresh on VALUE	
		RETURNCODE rc = pVar->doRefreshActs();
		if(rc != SUCCESS)// stevev 07dec06 - refresh method error looses update
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to variable refresh action returned an error.\n");
			return false; // An error - allow this to work immediately again
		}	

		// done above pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );


		// Now set the actual string in the combo box with its
		// enum representation of its value
		CString value = ReadVal();
		// we have to update valid value list before we can do this::>
		int index = pVarIndex->FindStringExact( 0, value );

		pVarIndex->SetCurSel(index);
	}
	else
	{// remove device - nothing now, just be sure to get a ptr
	// done above 	pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	}
	// There is no SetWindowText() even here so there is no windows
	// update.  Therefore Execute will not be called and any yellow
	// highlight will not be turned on/off appropriately
	// Call it here!
	Execute(CBN_SELCHANGE);

	// This variable has been updated.  Any editing has been erased 
	// as a result of this update.  Allow for the editing to begin 
	// again
	// no longer used 11sep07  m_bTriggerEdit = FALSE;
	return result;
}

BOOL CdrawIndex::DisplayValue()
{
    BOOL result = FALSE;

    hCVar* pVar = (hCVar*) GetItem();

    CString strValue(ReadVal());

    // Show the variable value using the CLabelDlg class
    CLabelDlg dlgVarValue(GetLabel(), strValue);
    dlgVarValue.sdcModl();// DoModal();
    result = TRUE;

	return result;
}

void CdrawIndex::LoadContextMenu(CMenu *pMenu, int& nPos)
{
	pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDENUM;
}

void CdrawIndex::Edit()
{
	ITEM_ID lWaoRel = 0;
	hCVar*  pVar    = NULL;

	if ( ((hCVar*) GetItem())->IsReadOnly() || m_bIsReadOnly )
	{
		// Can not edit a variable that is "read-only"
		return;
	}

	//This will act as a way to trigger an edit event
	// no longer used 11sep07  if (m_bTriggerEdit)
	if (inActions)
	{
		// The process has already started.
		// Don't need to run it again
		return;
	}

	// Run any pre-edit action
	if (GetItem()->IsVariable())
	{
		pVar = (hCVar*) GetItem();
		lWaoRel = pVar->getWaoRel();// set info while we have the pointer
	}
	else
	{
		return;
	}

	TRACE(_T("CdrawIndex::Edit()- Edit event triggered\n"));

	if (lWaoRel > 0 && pVar != NULL)
	{
		//CDDLVariable * pV = new CDDLVariable(pVar);
		//CWaoDlg WaoDlg(pV, lWaoRel);
		//above replaced 6aug08
		CWaoDlg WaoDlg(pVar, lWaoRel);

		inActions = true;			 // bracket the modal - for now
		int nRet  = WaoDlg.DoModal();// does pre edit actions...
		inActions = false;

        // If there was a post edit action on any of the variables in the WAO, it has already been executed,
        // do not allow it to be executed again.
        Enable(false);

		//delete pV;
		return;
	}
	else if (pVar != NULL)// just an edit
	{
		inActions = true;
		RETURNCODE rc = pVar->doPreEditActs();
		inActions = false;
			
		if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
            Enable(false); // The method has either failed or been aborted, do not let the user edit the value, POB - 28, Aug 2008
			return; // An error - allow this to work immediately again
		}
	}

	/****** stevev 15nov05 ************/
	/* the pre-edit actions will probably change the dropdown */
	CClrComboBox *  pVarEnum;
	CStringArray enumList;

    if ( m_pDynCtrl != NULL )
	{
		pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
        
        if (pVarEnum)
        {
            // Normally this is handled on the CdrawIndex::Execute()
            // But if this is on a Grid control, the edit control
            // has just been created, so we need to indicate again
            // that the value has changed, POB - 5/5/2014
            if (pVar->isChanged())
            {
                //The user has made a change
                pVarEnum->Highlight(TRUE);
            }
            else
            {
                pVarEnum->Highlight(FALSE);
            }
            
            
		pVarEnum->ResetContent();// clears all (list and box)
		// Get the enumeration list to populate the combo box
		//GetEnumList(enumList);	
		GetValidValueList(enumList);

		// Populate the combo boxr
		for (int i=0; i < enumList.GetSize(); i++)
		{
			//pVarEnum->AddString(enumList.GetAt(i));
			CString List = enumList.GetAt(i);

			if (List != "")
			{
				int nIndex = pVarEnum->AddString(enumList.GetAt(i));
				pVarEnum->SetItemData(nIndex, i );  // i holds the DD index value of the device
			}
		}
		// Now reset the actual string in the combo box with its
		// enum representation of its value
		CString value = ReadVal();
		int index     = pVarEnum->FindStringExact( 0, value );

		pVarEnum->SetCurSel(index);
	}
    }
	/* end  stevev 15nov05 ************/

	// no longer used 11sep07  m_bTriggerEdit = TRUE;
}

BOOL CdrawIndex::Pre(WORD wMessageID)
{
	if ( m_pDynCtrl == NULL )
	{
		return FALSE;
	}
	CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCVar* pVar = (hCVar*) GetItem();
	BOOL bResult = TRUE;
	CString strValue;

/** now handled with focus events
	if (wMessageID == CBN_CLOSEUP)
	{
		pVarIndex->GetWindowText(strValue);

		if (strValue == ReadVal())
		{
			// Need the execute to work (return TRUE)
			// but we don't want any warning message
			// from the base class so we must return
			return TRUE;
		}

		bResult = CdrawBase::Pre(wMessageID);

		if (!bResult)
		{
			// This event takes place before the display value is
			// changed so there is no need to call CancelIt(), just
			// undo the change
			// pVarEnum->Undo();
			// NOTE:  It would be nice to do an undo here, but it
			//        does not exist, for the moment I will re-read.
			Update(UPDATE_STRUCTURE);
		}
	}
	*** end handled with focus events **/

	if (wMessageID == EN_SETFOCUS)// prepare to edit
	{// the device state and window state should have been filtered before we were called
		Edit();// pre-edit actions done here
		return FALSE;// don't execute, this is all the pre-edit we need to do
	}
	
	if (  (wMessageID == EN_KILLFOCUS) //take edit, loose focus
		||(wMessageID == VK_RETURN)    )//take edit, keep focus
	{	
		/* - Now doing data validation checking in the WriteVal() function, POB - 13 Aug 2008
		if (!IsValid())
		{
			return FALSE;// in case it went invalid while we were editting
		}
		*/

		pVarIndex->GetWindowText(strValue);

		if (strValue == ReadVal())
		{	// Need the execute to work (return TRUE)
			// We don't want any warning message (skip base Pre) or pre-edit actions
			// so we must return early
			return TRUE;
		}
		// else isChanged so continue
		bResult = CdrawBase::Pre(wMessageID);// loop warning vars

		if (!bResult) // loop warning cancelled...
		{	// This event takes place before the Device Object display value has been
			// changed so there is no need to call CancelIt(), just undo the change
			//since pVarIndex->Undo();does not exist, we force a re-read & update
			Update(UPDATE_ADD_DEVICE);// _device does structure and value
		}
		// else - exit to run execute()
	}

	if (wMessageID == VK_ESCAPE)//undo edit, keep focus
	{
		//since pVarEnum->Undo();does not exist, we force a re-read & update
		Update(UPDATE_ADD_DEVICE);// _device does structure and value
	}

	return bResult;
}

BOOL CdrawIndex::Post(WORD wMessageID)
{
	BOOL bResult = TRUE;

	//if(wMessageID == CBN_CLOSEUP)
	if( wMessageID == EN_KILLFOCUS )  
	{
		//Call base class for common action 
		bResult = CdrawBase::Post(wMessageID);
		
		/* Move to CdrawBase::Post - POB, 10 Dec 2004 */
		// Update the display value
		// Update();

		/* Move to CdrawEnum::Update() - POB, 10 Dec 2004 */
		// The edit process is accomplished.  Allow for it 
		// to begin again
		//	m_bTriggerEdit = FALSE;
	}

	return bResult;
}

CRect CdrawIndex::GetRect()
{
	CWnd * ptr;
	CRect rect(0,0,0,0);
	CRect rect2(0,0,0,0);

	// Call the base class to get the primary rectangle
	rect = CdrawBase::GetRect();

	// Determine the remaining portion of the CdrawIndex
	// rectangle
	ptr = m_pDynCtrl->GetDlgCtrl(m_nLabelID);
	
	if (ptr)
	{
		ptr->GetWindowRect(&rect2);
		rect.left = rect2.left;
	}

	return rect;
}

CString CdrawIndex::ReadVal()
{
	CString strValue;
	CString strIndex;
	wstring sIndex;
	hCindex* pIndex;

	hCVar* pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_INDEX)
	{
		pIndex = (hCindex*)pVar;
/*807		sIndex = pIndex->getDispValueTString();
*/
		sIndex = pIndex->getValidDispValueString();
		// stevev 12sep07 ++  stevev 17mar09-added the is local for 807 bug
		if ( sIndex.empty() && pVar->IsLocal() )// also in CdrawIndex::GetValidValueList() must match
		{
			TCHAR c[32];
			_stprintf(c,_T("%d"),(int)(pIndex->getDispValue()));
			sIndex = c;
		}
		// end stevev 12sep07 --
		strIndex = sIndex.c_str();
	}

	return strIndex;
}

void CdrawIndex::WriteVal()
{
	hCVar* pVar;
	CValueVarient val;
	int nIndex;
	UINT uValue;

	CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_INDEX)
	{
		hCindex* pIndex = (hCindex*)pVar;

		nIndex = pVarIndex->GetCurSel(); //get a value

		uValue = pVarIndex->GetItemData(nIndex);
		val = uValue;

		pIndex->setDispValue(val);
		pIndex->setWriteStatus(1);// user written
	}
}

void CdrawIndex::GetValidValueList(CStringArray& enumList)
{
	hCVar* pVar;
	hCindex* pIndex;
	RETURNCODE rt = SUCCESS;
	hCenumList eList(0);
	/*  10jun05 - these can now be arrays, lists or item arrays 
	vector<hCitemArray*> ArrayPtrList;
	vector<hCitemArray*> ::iterator aplIT;// ptr 2a ptr 2a hCitemArray
	****/
	vector<hCitemBase*> ArrayPtrList;
	vector<hCitemBase*> ::iterator aplIT;// ptr 2a ptr 2a hCitemArray
	/* end 10jun05 */
	hCgroupItemDescriptor	*pGID=NULL;
	UIntList_t			uintList;
	int cnt = 0;
		
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_INDEX)
	{
		pIndex = (hCindex*)pVar;

		if ( pIndex->pIndexed->getAllArrayPtrs(ArrayPtrList) == SUCCESS && 
			 ArrayPtrList.size() > 0  )
		{
			for (aplIT = ArrayPtrList.begin();aplIT < ArrayPtrList.end(); aplIT++)
			{
				(*aplIT)->getAllindexValues(uintList);
				
				cnt = uintList.size();
				
				if(cnt >0)
				{
					for(UIntList_t::iterator it = uintList.begin(); it < uintList.end();  it++ )
					{
						if(SUCCESS == (*aplIT)->getByIndex((*it),&pGID) )
						{
							//hCddlString tmpDDlStr(pGID->getDesc());
							//wstring tmpstr= (wstring)((wstringEx_t)tmpDDlStr);//stevev 03jan08  was::>tmpDDlStr.procureVal();
							wstring tmpstr = (wstring)pGID->getDesc();
							// stevev 12sep07 ++
							if ( tmpstr.empty() )// also in CdrawIndex::ReadVal() must match
							{
								wchar_t c[32];
								wsprintf(c,L"%d",(*it));
								tmpstr = c;
							}
							// end stevev 12sep07 --
							enumList.SetAtGrow((*it), tmpstr.c_str());
						}
					}
				}
			}
		}


/*		rt = pIndex->procureList(eList);
		if ( rt == SUCCESS && eList.size() > 0 )
		{
			EnumTriad_t wrkTriad;
			for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
			{
				wrkTriad = *it;
				enumList.Add(wrkTriad.descS.c_str());
			}
		}*/
	}
}

void  CdrawIndex::staleDynamics(void)
{	
	hCVar* pVar = (hCVar*) GetItem();
	
	// too easy  if (pVar->IsDynamic() )
	if ( pVar->IsValid()   &&   
		 pVar->IsDynamic() &&	/*This is not an unconfirmed write*/						
		(pVar->getDataState() != IDS_NEEDS_WRITE && (! pVar->isChanged()) ) &&		 
		!pVar->IsWriteOnly()  &&/* don't try and read write-only variables*/
		pVar->getDataState() != IDS_STALE   )// don't do it redundently
	{
		pVar->MakeStale();
	}// else do nothing
}
	
void CdrawIndex::Enable(BOOL bEnable)
{
    CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    pVarIndex->Enable(bEnable);
}

BOOL CdrawIndex::IsEnabled()
{
    CClrComboBox * pVarIndex = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    return pVarIndex->IsEnabled();
}

	
/* * * * * * * * * * VARIABLE ENUM * * * * * * * * * */
// POB - 2 Sep 2004
CdrawEnum::~CdrawEnum()
{
	CWnd * ptr;

	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

		if (ptr && ::IsWindow(ptr->m_hWnd))
		{
			// Destroy the Label control (CWnd object)
			ptr->DestroyWindow();
		}
	}
}

void CdrawEnum::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	//09dec05 CComboBox * pVarEnum;
	CClrComboBox * pVarEnum;
	CStringArray enumList;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->AddPageCtrl(m_nLabelID, m_nPage);
	}

//	pVarEnum = (CComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	// Get the enumeration list to populate the combo box
	GetEnumList(enumList);

	// Populate the combo boxr
	int i; // PAW 03/03/09
	for (/*int PAW */i=0; i < enumList.GetSize(); i++)
	{ 
		pVarEnum->AddString(enumList.GetAt(i));
	}
//////////////////////////////////////////////////////////////////////////////////////////////////
	// Modify width of dropdown portion
	// Find the longest string in the combo box.
	CString str;
	CSize   sz;
	int     dx     = 0;
	int     strLen = 0;
	CDC*    pDC      = pVarEnum->GetDC();
	CFont*  pFont    = pVarEnum->GetFont();// added sjv 17jan12 to measure using actual font
	CFont*  pOldFont = pDC->SelectObject(pFont);// select it into the DC for measurement
	// get this fonts's metrics
	TEXTMETRIC      tm;
	pDC->GetTextMetrics(&tm);
	
	for (i=0;i < pVarEnum->GetCount();i++)
	{
	   pVarEnum->GetLBText( i, str );
	   sz = pDC->GetTextExtent(str);

//	   sz.cx += tm.tmAveCharWidth;//add an average character width to the end to avoid clipping
	   // this comes out exactly right when no scroll bar present

	   if (sz.cx > dx)
	   {
		  dx = sz.cx;
		  strLen = str.GetLength();
	   }
	}

	pDC->SelectObject(pOldFont);// put the old font back
	pVarEnum->ReleaseDC(pDC);   // give the DC back

	// Adjust the width for the vertical scroll bar and the left and right border.
	//dx += ::GetSystemMetrics(SM_CXVSCROLL) + 2*::GetSystemMetrics(SM_CXEDGE);
	// this was replaced by the other methods used here
	dx +=  2*::GetSystemMetrics(SM_CXEDGE);
	if (pVarEnum->GetCount() > NORMAL_DROP_ROWS)
	{
        // If there are many dropdown items then move the scrollbar away from the text for
        // better readability
		dx += ::GetSystemMetrics(SM_CXVSCROLL) ;
		dx += 4;// needs a bit more to get separation from vert scroll bar
	}

	if ( dx > ( 2 * ((COLUMSTEPSIZE*3) + COLUMNGAP)) )
	{
        // Add horizontal scrollbars to the very long horizontal charcter strings
        // in the dropdown list
		pVarEnum->SetDroppedWidth(2 * ((COLUMSTEPSIZE*3) + COLUMNGAP));
		pVarEnum->SetHorizontalExtent(dx);
	}
	else
	{
		// Set the width of the list box so that every item is completely visible
		pVarEnum->SetDroppedWidth(dx);
	}

    // Height of the non-drop down portion of the combo box
    // so that matches the size of the edit box
    pVarEnum->SetItemHeight(-1, ROWSTEPSIZE+8);

//////////////////////////////////////////////////////////////////////////////////////////////////
	// This is the initial update
	Update(UPDATE_ADD_DEVICE);

	// This is no need to call Execute() since Update() above will
	// make that call for us
}

void CdrawEnum::Execute(WORD wMessageID, LPARAM lParam)
{
	if ( m_pDynCtrl == NULL )
	{// this condition should have been filtered long before we get here, but just in case...
		LOGIT(CLOG_LOG,"CdrawEnum.Execute %s 0x%04x does not have a DynCtrl Yet!\n",
			m_symblID);
		return ;
	}	
	CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCVar* pVar = (hCVar*) GetItem();
	CString strValue;

	// Determine if the user has made a change to the
	// combo box
	//if(wMessageID == CBN_CLOSEUP)
	if( (wMessageID == VK_RETURN)  ||	// take edit, keep focus
		(wMessageID == EN_KILLFOCUS) )  // take edit, loose focus
	{
		// Get the changes from the user
		WriteVal();
	}

	if (pVar->isChanged())
	{
		//The user has made a change
		pVarEnum->Highlight(TRUE);
	}
	else
	{
		pVarEnum->Highlight(FALSE);
	}
/*** stevev 11sep07 - these are handled in Pre
	if(wMessageID == EN_SETFOCUS)
	{

	}

	if (wMessageID == CBN_DROPDOWN)
	{
		// Using WM_LBUTTONDOWN (as in the case CdrawVar)
		// for calling Edit() does not work in CComboBox
		// It has the negative effect of calling CBN_SELENDCANCEL
		// instead so the dropdown window is non-fuctional
		Edit();
	}
***/
}

bool CdrawEnum::Update(long type,long SymID)
{
	CWnd * ptr;
	hCVar* pVar = NULL;
	CClrComboBox * pVarEnum;

	if ( SymID != m_symblID && SymID != -1 )// -1 matches all
		return false; //   - it is not for us...

	/*** it is for us ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -enum-\n",SymID);
	bool result = false; // true @ this control's size changed

    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawEnum.Update %s 0x%04x does not have a DynCtrl Yet!\n",
														m_pItem->getName().c_str(),m_symblID);
		return false;
	}
	if ( GetItem()->IsVariable() )
		pVar = (hCVar*) GetItem();
	else
	{
		LOGIT(CERR_LOG,"CdrawEnum.Update %s 0x%04x is NOT a Variable!\n",m_symblID);
		return false;
	}
	DEBUGLOG(CLOG_LOG,"MSG:Updating %s (%s) Variable 0x%04x (%s) to %s\n",
		(type == UPDATE_STRUCTURE)?"STRUCT":((type == UPDATE_ONE_VALUE)?"VALUE":""),menuStyleStrings[mdsType],
		pVar->getID(),pVar->getName().c_str(), pVar->getDispValueString().c_str());
	
	
	if ( type == UPDATE_STRUCTURE || type == UPDATE_ADD_DEVICE )
	{// update structure or add device
		// Update the Label 
		// ** ptr  to Label Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
			ptr->Invalidate();// stevev 07dec06
		}
		pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		// Check for read-only attributes
		pVarEnum->EnableWindow(! pVar->IsReadOnly() && !m_bIsReadOnly);
	}// else update value or remove device

	if(type == UPDATE_ONE_VALUE || type == UPDATE_ADD_DEVICE)
	{	// update its value	
		// Run any Refresh action--- stevev 07dec06 - my choice, only refresh on VALUE	
		RETURNCODE rc = pVar->doRefreshActs();
		if(rc != SUCCESS)// stevev 07dec06 - refresh method error looses update
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to variable refresh action returned an error.\n");
			return false; // An error - allow this to work immediately again
		}	
		pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		
		// Now set the actual string in the combo box with its
		// enum representation of its value
		CString value = ReadVal();
		int index = pVarEnum->FindStringExact( 0, value );

		pVarEnum->SetCurSel(index);
	}
	else
	{// remove device - nothing now, just be sure to get a ptr
		pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	}
	
	// There is no SetWindowText() even here so there is no windows
	// update.  Therefore Execute will not be called and any yellow
	// highlight will not be turned on/off appropriately
	// Call it here!
	Execute(CBN_SELCHANGE);

	// This variable has been updated.  Any editing has been erased 
	// as a result of this update.  Allow for the editing to begin 
	// again
	// no longer used 11sep07  m_bTriggerEdit = FALSE;
	return result;
}

BOOL CdrawEnum::DisplayValue()
{
    BOOL result = FALSE;

    hCVar* pVar = (hCVar*) GetItem();

    CString strValue(ReadVal());

    // Show the variable value using the CLabelDlg class
    CLabelDlg dlgVarValue(GetLabel(), strValue);
    dlgVarValue.sdcModl();// DoModal();
    result = TRUE;

	return result;
}

void CdrawEnum::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDENUM;    
}

void CdrawEnum::Edit()
{
	ITEM_ID lWaoRel = 0;
	hCVar*  pVar    = NULL;

	if ( ((hCVar*) GetItem())->IsReadOnly() || m_bIsReadOnly )
	{
		// Can not edit a variable that is "read-only"
		return;
	}

	//This will act as a way to trigger an edit event
	// no longer used 11sep07  if (m_bTriggerEdit)
	if (inActions)
	{
		// The process has already started.
		// Don't need to run it again
		return;
	}

	// Run any pre-edit action
	if (GetItem()->IsVariable())
	{
		pVar = (hCVar*) GetItem();
		lWaoRel = pVar->getWaoRel();// set info while we have the pointer
	}
	else
	{
		return;
	}

	TRACE(_T("CdrawEnum::Edit()- Edit event triggered\n"));

	if (lWaoRel > 0 && pVar != NULL)
	{
		//CDDLVariable * pV = new CDDLVariable(pVar);
		//CWaoDlg WaoDlg(pV, lWaoRel);
		//above replaced 6aug08
		CWaoDlg WaoDlg(pVar, lWaoRel);

		inActions = true;			 // bracket the modal - for now
		int nRet  = WaoDlg.DoModal();// does pre edit actions...
		inActions = false;

        // If there was a post edit action on any of the variables in the WAO, it has already been executed,
        // do not allow it to be executed again.
        Enable(false);

		//delete pV;
		return;
	}
	else if (pVar != NULL)// just an edit
	{
		inActions = true;
		RETURNCODE rc = pVar->doPreEditActs();
		inActions = false;
			
		if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
            Enable(false); // The method has either failed or been aborted, do not let the user edit the value, POB - 28, Aug 2008
			return; // An error - allow this to work immediately again
		}
	}
	/****** stevev 15nov05 ************/
	/* the pre-edit actions will probably change the dropdown */
	//CComboBox *  pVarEnum;
	CClrComboBox *  pVarEnum;
	CStringArray enumList;
	int index = -1;

    if ( m_pDynCtrl != NULL )
	{
		pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
               
        if (pVarEnum)
        {
            // Normally this is handled on the CdrawEnum::Execute()
            // But if this is on a Grid control, the edit control
            // has just been created, so we need to indicate again
            // that the value has changed, POB - 5/5/2014
            if (pVar->isChanged())
            {
                //The user has made a change
                pVarEnum->Highlight(TRUE);
            }
            else
            {
                pVarEnum->Highlight(FALSE);
            }

		pVarEnum->ResetContent();// clears all (list and box)
		// Get the enumeration list to populate the combo box
		GetEnumList(enumList);

		// Populate the combo boxr
		for (int i=0; i < enumList.GetSize(); i++)
		{
			pVarEnum->AddString(enumList.GetAt(i));
		}
		// Now reset the actual string in the combo box with its
		// enum representation of its value
		CString value = ReadVal();
		index     = pVarEnum->FindStringExact( 0, value );

		pVarEnum->SetCurSel(index);
        }

	}

	/* end  stevev 15nov05 ************/
	// no longer used 11sep07  m_bTriggerEdit = TRUE;
//test
//LOGIT(CLOG_LOG,"[[[[[[ BOX: cnt:%d, Height:%d {@%d},  Width:%d\n",
//	  pVarEnum->GetCount(),pVarEnum->GetItemHeight(index),index,pVarEnum->GetDroppedWidth());
//CRect r;	pVarEnum->GetDroppedControlRect(&r);
//LOGIT(CLOG_LOG,"[[[[[[ RCT: lft:%d, top:%d,  rgt:%d bot:%d\n",
//	  r.left,r.top,r.right,r.bottom);
//////////////////////
#ifndef _DEBUG
//pVarEnum->GetParent()->ScreenToClient(&r);
//r.bottom += 65;
//pVarEnum->MoveWindow(&r);
#endif
/////////////////////
// end test
}

BOOL CdrawEnum::Pre(WORD wMessageID)
{
	if ( m_pDynCtrl == NULL )
	{
		return FALSE;
	}
	CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCVar* pVar             = (hCVar*) GetItem();
	BOOL bResult            = TRUE;
	CString strValue;
/** now handled with focus events
	if (wMessageID == CBN_CLOSEUP)
	{
		pVarEnum->GetWindowText(strValue);

		if (strValue == ReadVal())
		{
			// Need the execute to work (return TRUE)
			// but we don't want any warning message
			// from the base class so we must return
			return TRUE;
		}

		bResult = CdrawBase::Pre(wMessageID);

		if (!bResult)
		{
			// This event takes place before the display value is
			// changed so there is no need to call CancelIt(), just
			// undo the change
			// pVarEnum->Undo();
			// NOTE:  It would be nice to do an undo here, but it
			//        does not exist, for the moment I will re-read.
			Update(UPDATE_STRUCTURE);
		}
	}
	*** end handled with focus events **/

	if (wMessageID == EN_SETFOCUS)// prepare to edit
	{// the device state and window state should have been filtered before we were called
		Edit();// pre-edit actions done here
		return FALSE;// don't execute, this is all the pre-edit we need to do
	}

	if (  (wMessageID == EN_KILLFOCUS) //take edit, loose focus
		||(wMessageID == VK_RETURN)   )//take edit, keep focus
	{	
		/* - Now doing data validation checking in the WriteVal() function, POB - 13 Aug 2008
		if (!IsValid())
		{
			return FALSE;// in case it went invalid while we were editting
		}
		*/

        // Check for read-only attributes
		pVarEnum->EnableWindow(! pVar->IsReadOnly() && !m_bIsReadOnly);

		pVarEnum->GetWindowText(strValue);

		if (strValue == ReadVal())
		{	// Need the execute to work (return TRUE)
			// We don't want any warning message (skip base Pre) or pre-edit actions
			// so we must return early
			return TRUE;
		}
		// else isChanged so continue
		bResult = CdrawBase::Pre(wMessageID);// loop warning vars

		if (!bResult) // loop warning cancelled...
		{	// This event takes place before the Device Object display value has been
			// changed so there is no need to call CancelIt(), just undo the change
			//since pVarEnum->Undo();does not exist, we force a re-read & update
			Update(UPDATE_STRUCTURE);
		}
		// else - exit to run execute()
	}

	if (wMessageID == VK_ESCAPE)//undo edit, keep focus
	{
		//since pVarEnum->Undo();does not exist, we force a re-read & update
		Update(UPDATE_STRUCTURE);
	}

	return bResult;
}

BOOL CdrawEnum::Post(WORD wMessageID)
{
	BOOL bResult = TRUE;

	//if(wMessageID == CBN_CLOSEUP)
	if( wMessageID == EN_KILLFOCUS )  
	{
		//Call base class for common action 
		bResult = CdrawBase::Post(wMessageID);
		
		/* Move to CdrawBase::Post - POB, 10 Dec 2004 */
		// Update the display value
		// Update();

		/* Move to CdrawEnum::Update() - POB, 10 Dec 2004 */
		// The edit process is accomplished.  Allow for it 
		// to begin again
		//	m_bTriggerEdit = FALSE;
	}

	return bResult;
}

CRect CdrawEnum::GetRect()
{
	CWnd * ptr;
	CRect rect(0,0,0,0);
	CRect rect2(0,0,0,0);

	// Call the base class to get the primary rectangle
	rect = CdrawBase::GetRect();

	// Determine the remaining portion of the CdrawEnum
	// rectangle
	ptr = m_pDynCtrl->GetDlgCtrl(m_nLabelID);
	
	if (ptr)
	{
		ptr->GetWindowRect(&rect2);
		rect.left = rect2.left;
	}

	return rect;
}

CString CdrawEnum::ReadVal()
{
	CString strValue;
	CString strEnum;
//	int uEnum;

	hCVar* pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_ENUM)
	{
//		hCEnum* pEnum = (hCEnum*)pVar;
//		strValue = pEnum->getDispValueString().c_str();

//		sscanf( (LPCTSTR )strValue, "%d", &uEnum );
		hCEnum* pEnum = (hCEnum*)pVar;
/*807		uEnum = pEnum->getDispValue();
*/
		//string sEnum;
        wstring sEnum; // Use wstring for UTF-8 support, POB - 1 Aug 2008
/*807		pEnum->procureString(uEnum, sEnum);
*/
/*807*/	sEnum = pEnum->getValidDispValueString();

		strEnum = sEnum.c_str();
	}

	return strEnum;
}

void CdrawEnum::WriteVal()
{
	CString strValue;
	hCVar* pVar;
	CValueVarient val;
	tstring szValue;

	CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	pVarEnum->GetWindowText(strValue);
		
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_ENUM)
	{
		UINT uValue;
		hCEnum* pEnum = (hCEnum*)pVar;

		szValue = strValue;
		pEnum->procureValue (szValue, uValue);// input string, get a value
		
		val = uValue;
		pEnum->setDispValue(val);
		pEnum->setWriteStatus(1);// user written
	}
}

void CdrawEnum::GetEnumList(CStringArray& enumList)
{
	hCVar* pVar;
	hCEnum* pEnum;
	RETURNCODE rt = SUCCESS;
	hCenumList eList(0);
		
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_ENUM)
	{
		pEnum = (hCEnum*)pVar;

		rt = pEnum->procureList(eList);
		
		if ( rt == SUCCESS && eList.size() > 0 )
		{
			EnumTriad_t wrkTriad;
			
			for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
			{
				wrkTriad = *it;
				enumList.Add(wrkTriad.descS.c_str());
			}
		}
	}
}


void  CdrawEnum::staleDynamics(void)
{	
	hCVar* pVar = (hCVar*) GetItem();
	
	// too easy  if (pVar->IsDynamic() )
	if ( pVar->IsValid()   &&   
		 pVar->IsDynamic() &&	/*This is not an unconfirmed write*/						
		(pVar->getDataState() != IDS_NEEDS_WRITE && (! pVar->isChanged()) ) &&		 
		!pVar->IsWriteOnly() && /* don't try and read write-only variables*/
		pVar->getDataState() != IDS_STALE   )// don't do it redundently
	{
		pVar->MakeStale();
	}// else do nothing
}

void CdrawEnum::Enable(BOOL bEnable)
{
    CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    pVarEnum->Enable(bEnable);
}

BOOL CdrawEnum::IsEnabled()
{
    CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
    return pVarEnum->IsEnabled();
}

/* * * * * * * * * * BIT ENUM * * * * * * * * * */
// POB - 8 Sep 2004
CdrawBitEnum::~CdrawBitEnum()
{
	CWnd * ptr;

	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

		if (ptr)
		{
			// Destroy the Label control (CWnd object)
			ptr->DestroyWindow();
		}
	}
}

#if 0 // use base class
int CdrawBitEnum::FillSelf(ulong selfSymID,menuHist_t hist)// does it all now	
{
	hCVar* pVar;
	hCEnum* pEnum;
	RETURNCODE rt = SUCCESS;
	hCenumList eList(0);
	int enumSize = 0;
	CString listName = "Bit";
	int i;

	if ( selfSymID == 0 )
	{
		if ( m_symblID == 0 )
		{
			TRACE(_T("BitEnum.fillself ERROR: not enough info.\n"));
			return 0;
		}
		else
		{
			selfSymID = m_symblID;
		}
	}

	m_symblID = selfSymID;

	// Get the number of bits in the Bit Enum
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_BITENUM)
	{
		pEnum = (hCEnum*)pVar;

		rt = pEnum->procureList(eList);
		
		enumSize = eList.size();
	}

	// We need to create a CdrawBit for each of the bits in the Enum
	for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
	{
		CdrawBase* pP = newDraw(mds_bit);
		// WORK TO DO:  Verify the if statements after we integrate
		if (pP)
		{
			pP->FillSelf(m_symblID    ,menuHist_t());
			pP->SetIndex((*it).val);
			i = m_DrawList.size(); // next index
			m_DrawList.push_back(pP);
			m_CID2DrawMap[pP->m_cntrlID] = i;
			m_SID2DrawMap[pP->m_symblID] = i;

			
			pP->m_bIsReadOnly = m_bIsReadOnly;
			pP->m_bIsNoLabel  = m_bIsNoLabel;
			pP->m_bIsNoUnit   = m_bIsNoUnit;
			pP->m_bIsInline   = m_bIsInline;
			pP->m_bIsDisplayVal=m_bIsDisplayVal;
			pP->m_bIsReview   = m_bIsReview;
		}
		else
		{
			TRACE(_T("%s fill ERROR: could not generate a new Draw item.\n"),listName);
		}
	}// next menu item

	return ( m_DrawList.size() );
}
#endif //0 

void CdrawBitEnum::FillSize(void)	
{	
	// Bit Enums are not a menu type and do not have menu items.
	// However, in order to have each bit show up individually as
	// a separate control, we must simulate this as if its a GROUP
	// menu with a series of bit references.
	int y = menuSize();
	m_MyLoc.da_size.yval += (GRP_HGT_ROWS); // add a row for the label
}

void CdrawBitEnum::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;
	unsigned myPixelOffset;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	if ( m_pDynCtrl != NULL )
	{
		//myPixelOffset = m_pDynCtrl->xOffset();
		myPixelOffset = m_pDynCtrl->xOffset() + centeringOffset;
	}
	else
	{
		myPixelOffset = 0;
	}
	centeringOffset = myPixelOffset;

	// Initialize this control
	ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	ptr->SetWindowText(GetLabel());

	// Get origin converted
	topLeftParent.xval += (m_MyLoc.topleft.xval -1);
	topLeftParent.yval += (m_MyLoc.topleft.yval -1);

	// Add group label size offset
	topLeftParent.yval += GRP_HGT_ROWS;

#ifdef LOGBIRTH
	ldepth++;
#endif
	// Now create its menu items// use a zero offset for the bits
	createMenu(pWnd, topLeftParent, centeringOffset);
#ifdef LOGBIRTH
	ldepth--;
#endif

}

bool CdrawBitEnum::Update(long type,long SymID)
{
	if ( SymID != m_symblID && SymID != -1 )// -1 matches all
		return false; //   - it is not for us...

	DEBUGLOG(CLOG_LOG,"MSG:CdrawBitEnum.Update %s 0x%04x in 0x%04x\n",
		     menuStyleStrings[mdsType],SymID,m_symblID);
	bool result = false; // true @ this control's size changed

	if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawBitEnum.Update 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
		return false;
	}

	hCVar* pVar = NULL;
	if ( GetItem()->IsVariable() )
		pVar = (hCVar*) GetItem();
	else
	{
		LOGIT(CERR_LOG,"CdrawBitEnum.Update 0x%04x is NOT a Variable!\n",m_symblID);
		return true;
	}

	CWnd * ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

	/* stevev 22aug07 - par 765 - yes, virginia, it's damn thin - re-written */
	/* this seems a little thin....  stevev 07dec06 
	// Update the Label
	ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
	
	if (ptr)
	{
		ptr->SetWindowText(GetLabel());
	}*/

	if ( type == UPDATE_STRUCTURE || type == UPDATE_ADD_DEVICE )
	{	// Update the Label 
		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
			ptr->Invalidate();
		}
	}

	if(type == UPDATE_ONE_VALUE || type == UPDATE_ADD_DEVICE)
	{	// update its value	
		// Run any Refresh action--- stevev 07dec06 - my choice, only refresh on VALUE	
		RETURNCODE rc = pVar->doRefreshActs();
		if(rc != SUCCESS)// stevev 07dec06 - refresh method error looses update
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to variable refresh action returned an error.\n");
			return false; // An error - allow this to work immediately again
		}
		// Iterate through child list
		DrawList_t::iterator i;
		for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{	//call update for all the individual bits...
			CdrawBase *pDB = (CdrawBase *)(*i);
			pDB->Update(type, -1);
		}
	}

	return result;
}

BOOL CdrawBitEnum::DisplayValue()
{
    BOOL result = FALSE;

    hCVar* pVar = (hCVar*) GetItem();

    //There is no CdrawBitEnum::ReadVal()
    // must get the information directly 
    wstring value;
    value = pVar->getValidDispValueString();
    CString strValue(value.c_str());

    // Show the variable value using the CLabelDlg class
    CLabelDlg dlgVarValue(GetLabel(), strValue);
    dlgVarValue.sdcModl();// DoModal();
    result = TRUE;

	return result;
}

void CdrawBitEnum::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDENUM;    
}

void CdrawBitEnum::GetEnumList(CStringArray& enumList)
{
	hCVar* pVar;
	hCEnum* pEnum;
	RETURNCODE rt = SUCCESS;
	hCenumList eList(0);
		
	pVar = (hCVar*) GetItem();
	
	if (pVar->VariableType() == TYPE_BITENUM)
	{
		pEnum = (hCEnum*)pVar;

		rt = pEnum->procureList(eList);
		
		if ( rt == SUCCESS && eList.size() > 0 )
		{
			EnumTriad_t wrkTriad;
			
			for(vector<hCenumDesc>::iterator it = eList.begin(); it < eList.end(); it++)
			{
				wrkTriad = *it;
				enumList.SetAtGrow(wrkTriad.val,wrkTriad.descS.c_str());
			}
		}
	}
}

void  CdrawBitEnum::staleDynamics(void)
{	
	hCVar* pVar = (hCVar*) GetItem();
	
	// too easy  if (pVar->IsDynamic() )
	if ( pVar->IsValid()   &&   
		 pVar->IsDynamic() &&	/*This is not an unconfirmed write*/						
		(pVar->getDataState() != IDS_NEEDS_WRITE && (! pVar->isChanged()) ) &&		 
		!pVar->IsWriteOnly() && /* don't try and read write-only variables*/
		pVar->getDataState() != IDS_STALE   )// don't do it redundently
	{
		pVar->MakeStale();
	}// else do nothing
}

/* * * * * * * * * * BIT * * * * * * * * * */
// POB - 16 Sep 2004
void CdrawBit::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CBit * pBit;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->AddPageCtrl(m_nLabelID, m_nPage);
	}

	pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	// Set the bit color based on class type
	hCattrClass* pVarClass = ((hCVar*) GetItem())->getClassAttr();

	if (pVarClass)
	{
		// bitstring.bitstringVal
		hCbitString* pbs = (hCbitString*) (pVarClass->procure());
		
		if (pbs != NULL)
		{
			ulong classBS = pbs->getBitStr();
			
			if ( classBS & maskDiagnostic )
			{
				pBit->SetColor(CLR_DIAGNOSTIC);
			}
		}
	}

	// This is the initial update
	Update(UPDATE_ADD_DEVICE);

	// This is only needed to produce the yellow highligthing 
	// for all all changed variables on its initial update
	if (((hCVar*) GetItem())->isChanged())
	{
		//The user has made a change
		pBit->Highlight(TRUE);
	}
	else
	{
		pBit->Highlight(FALSE);
	}

}

void CdrawBit::Enable(BOOL bEnable)// stevev 19sep14 - intended to do nothing
{
	// function implemented to do nothing but remove the programmer error message
	return;
}

void CdrawBit::Execute(WORD wMessageID, LPARAM lParam)
{
	CBit * pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCBitEnum* pVar = (hCBitEnum*) GetItem();
	CWnd * pLabel = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
    CValueVarient dispVal, realVal;

	// Determine if the user has made a change
	if(wMessageID == 0)
	{
		if (pBit->Toggle())
		{	
	//		pLabel->SetWindowText("On");	
		}
		else
		{
	//		pLabel->SetWindowText("Off");
		}

		// Get the changes from the user
		WriteVal();
	}


    // Fix bug in bit enum - POB, 24 March 2006
    // isChanged() examines the entire bit enum
    // This has been modified so that the
    // individual bits are examined for changes 
    // instead of the entire bit enum
    realVal = pVar->getRealValue(GetIndex());
    dispVal = pVar->getDispValue(GetIndex(), false);

//	if (pVar->isChanged())
    if (dispVal.vValue.bIsTrue != realVal.vValue.bIsTrue)
	{
		//The user has made a change
		pBit->Highlight(TRUE);
	}
	else
	{
		pBit->Highlight(FALSE);
	}
}

bool CdrawBit::Update(long type,long SymID)
{
	CWnd * ptr;
	CBit * pBit;

	if ( SymID != m_symblID && SymID != -1 )// -1 matches all
		return false; //   - it is not for us...

	/*** it is for us ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -bit-\n",SymID);
	bool result = false; // true @ this control's size changed

    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawBit.Update %s 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
		return false;
	}	

	if ( type == UPDATE_STRUCTURE || type == UPDATE_ADD_DEVICE )
	{// update structure or add device
		// Update the Label 
		// ** ptr  to Label Control
		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );
		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
			ptr->Invalidate();// stevev 07dec06
		}
		pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		if (pBit)
			// Check for read-only attributes
			pBit->EnableWindow(!((hCVar*) GetItem())->IsReadOnly() && !m_bIsReadOnly);
	}// else update value or remove device

	if(type == UPDATE_ONE_VALUE || type == UPDATE_ADD_DEVICE)
	{	// update its value	
		// assume refresh actions are handled elsewhere...

		pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		if (pBit)
			// We need to update its value
			pBit->SetOn(ReadVal());
	}
	else
	{
		pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	}

	// There is no SetWindowText() even here so there is no windows
	// update.  Therefore Execute will not be called and any yellow
	// highlight will not be turned on/off appropriately
	// Call it here, but use a message ID that should not be seen!
	Execute(0xFFFF);
	
	// This variable has been updated.  Any editing has been erased 
	// as a result of this update.  Allow for the editing to begin 
	// again
	// no longer used 11sep07  m_bTriggerEdit = FALSE;
	return result;
}

BOOL CdrawBit::DisplayValue()
{
    BOOL result = FALSE;

    hCBitEnum* pVar = (hCBitEnum*) GetItem();

    CString strValue = ((BOOL)ReadVal())? BIT_SET : BIT_CLR;

    // Show the variable value using the CLabelDlg class
    CLabelDlg dlgVarValue(GetLabel(), strValue);
    dlgVarValue.sdcModl();// DoModal();
    result = TRUE;

	return result;
}

void CdrawBit::LoadContextMenu(CMenu* pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_WNDENUM;   //for the bit reference, use the same menu as the Enum 
}

void CdrawBit::Edit()
{
	if ( ((hCVar*) GetItem())->IsReadOnly() || m_bIsReadOnly )
	{
		// Can not edit a variable that is "read-only"
		return;
	}

	//This will act as a way to trigger an edit event
	// no longer used 11sep07  if (m_bTriggerEdit)
	if (inActions)
	{
		// The process has already started.
		// Don't need to run it again
		return;
	}

	// Run any pre-edit action
	if (GetItem()->IsVariable())
	{
		hCVar* pVar = (hCVar*) GetItem();

		RETURNCODE rc = pVar->doPreEditActs();
			
		if(rc != SUCCESS)
		{
			LOGIT(CERR_LOG|UI_LOG,"Call to pre edit action returned an error.\n");
			return; // An error - allow this to work immediately again
		}
	}

	// no longer used 11sep07  m_bTriggerEdit = TRUE;
}

BOOL CdrawBit::Pre(WORD wMessageID)
{
	if ( m_pDynCtrl == NULL )
	{
		return FALSE;
	}
	CClrComboBox * pVarEnum = (CClrComboBox *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	hCVar* pVar = (hCVar*) GetItem();
	BOOL bResult = TRUE;
	CString strValue;

	if (wMessageID == 0)
	{
		// Start the edit process here.
		//
		// The Pre() function normally handles only pre-validation events but it
		// is being used in this class to handle the start of an edit proces..
		// Typically the other draw objects will use the WM_LBUTTONDOWN message posted
		// to its parent to initiate an edit process, but for buttons, it needs the 
		// WM_LBUTTONDOWN message so that the button can actually perform its 
		// "pressed down" action so that the Execute() will still be called.
		Edit();

		bResult = CdrawBase::Pre(wMessageID);

	}

	return bResult;
}

CString CdrawBit::GetLabel()
{
	//string szLabel;
    wstring szLabel; // Use wstring for UTF-8 support, POB - 1 Aug 2008

	if (GetItem())
	{
		if (mdsType == mds_bit)
		{
			hCBitEnum* pVar = (hCBitEnum*) m_pItem;
			pVar->procureString(m_itemIndex,szLabel);
		}
		else
		{
			m_pItem->Label(szLabel);
		}
	}

	return szLabel.c_str();
}

CString CdrawBit::GetHelp()
{
	hCVar* pVar;
	hCEnum* pEnum;
	RETURNCODE rt = SUCCESS;
	hCenumList eList(0);
	//string szHelp;
    wstring szHelp; // Use wstring for UTF-8 support, POB - 1 Aug 2008
	
	pVar = (hCVar*) GetItem();

	if (pVar->VariableType() == TYPE_BITENUM)
	{
		pEnum = (hCEnum*)pVar;

		rt = pEnum->procureList(eList);
		
		if ( rt == SUCCESS && eList.size() > 0 )
		{
			hCenumDesc* pDesc = eList.matchValue(GetIndex());
			szHelp = pDesc->helpS;
		}
	}

	return szHelp.c_str();
}

CRect CdrawBit::GetRect()
{
	CWnd * ptr;
	CRect rect(0,0,0,0);
	CRect rect2(0,0,0,0);

	// Call the base class to get the primary rectangle
	rect = CdrawBase::GetRect();

	// Determine the remaining portion of the CdrawBit
	// rectangle
	// NOTE:  The label is on the right hand side of the
	//        bit control
	ptr = m_pDynCtrl->GetDlgCtrl(m_nLabelID);
	
	if (ptr)
	{
		ptr->GetWindowRect(&rect2);
		rect.right = rect2.right;
	}

	return rect;
}

BOOL CdrawBit::ReadVal()
{
	BOOL bBit;
	hCBitEnum* pVar;

	pVar = (hCBitEnum*) GetItem();
	
//	if (pVar->VariableType() == TYPE_ENUM)
	{
		bBit = pVar->getDispValue(GetIndex(), false);	
	}

	return bBit;
}

void CdrawBit::WriteVal()
{
	hCBitEnum* pVar;
	CValueVarient value;

	CBit * pBit = (CBit *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	pVar = (hCBitEnum*) GetItem();

//	value = pBit->IsOn();
	int nValue;

	if (pBit->IsOn())
	{
		nValue = GetIndex();//1 << mask2value(GetIndex());
	}
	else
	{
		nValue = 0;
	}
	
	value = nValue;

	pVar->setDispValue(value,  GetIndex());
	pVar->setWriteStatus(1);// user written
}


void  CdrawBit::staleDynamics(void)
{	
	hCVar* pVar = (hCVar*) GetItem();
	
	// too easy  if (pVar->IsDynamic() )
	if ( pVar->IsValid()   &&   
		 pVar->IsDynamic() &&	/*This is not an unconfirmed write*/						
		(pVar->getDataState() != IDS_NEEDS_WRITE && (! pVar->isChanged()) ) &&		 
		!pVar->IsWriteOnly() && /* don't try and read write-only variables*/
		pVar->getDataState() != IDS_STALE   )// don't do it redundently
	{
		pVar->MakeStale();
	}// else do nothing
}

/* * * * * * * * * * EDIT DISPLAY * * * * * * * * * */
// POB - 2 Sep 2004
void CdrawEdDisp::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	ptr->SetWindowText(GetLabel());
}

void CdrawEdDisp::Execute(WORD wMessageID, LPARAM lParam)
{
	/*Vibhor 240904: Start of Code*/

	hCitemBase *pItemBase = this->GetItem();

	hCeditDisplay *pEditDisplay = (hCeditDisplay*)pItemBase; 
	CDDLEDisplay  *pEDisplay = new CDDLEDisplay(pEditDisplay);
	pEDisplay->SetName(GetLabel());
    pEDisplay->SetID(pEditDisplay->getID());

	pEDisplay->Execute();

	/*Vibhor 240904: End of Code*/
	delete (pEDisplay);
}

/* * * * * * * * * * METHOD * * * * * * * * * */
// POB - 2 Sep 2004
void CdrawMeth::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	ptr->SetWindowText(GetLabel());
}

void CdrawMeth::Execute(WORD wMessageID, LPARAM lParam)
{
	/*Vibhor 240904: Start of Code*/
	hCmethodCall MethodCall;

	hCitemBase *pItemBase = this->GetItem();
	CDDIdeDoc *pDoc = GetDocument();

	hCmethod * pMeth = (hCmethod*)pItemBase; // create a DDB method
			
	MethodCall.m_pMeth = pMeth;
	MethodCall.source   = msrc_EXTERN;
	MethodCall.methodID = pMeth->getID();
	MethodCall.paramList.clear();

	if ( hCglobalServiceInterface::
						myDevicePtr(pMeth->devHndl())->getMethodSupportPtr()->hasClients() )
	{		
		// stevev 17jan11 - to stop the no-activity when a method is pressed
		int y = 0;
		while ( 1 )
		{
			CMethodSupport* pMS = (CMethodSupport*)
			hCglobalServiceInterface::myDevicePtr(pMeth->devHndl())->getMethodSupportPtr();
			if (pMS != NULL && pMS->pCurrentMethodCall != NULL)
			{
				if ((pMS->pCurrentMethodCall->source == msrc_ACTION   ||
					 pMS->pCurrentMethodCall->source == msrc_CMD_ACT) ||
					 pMS->pCurrentMethodCall->m_MethState == mc_Closing )
				{// the current is an action
					Sleep(25);// try to let it complete...sleep and then loop
					y++;
				}
				else
				{// the current source is an execute - another execute is erroneous
					LOGIT(UI_LOG,"Method was called illegally (%s).  "
									"It will not be executed.\n",pMeth->getName().c_str());
					TRACE(L"Method called with other functions running."
															L"-Aborted before it starts\n");
					return;
				}
			} 
			if ( ! pMS->hasClients() )
			{// go ahead and execute
				break;// out of infinite while
			}
			if ( y >= 40 )// 1 sec
			{
				LOGIT(UI_LOG,"Executing action stalled.'%s' cannot execute.\n",
																	pMeth->getName().c_str());
				TRACE(L"        Executing action stalled. Called method cannot execute.\n");
				return;
			}
		}// wend
	}

//	CDDLMethod * pMethod = new CDDLMethod(MethodCall,pDoc->m_pMethSupport,pItemBase); //Who deletes this ??
	CDDLMethod * pMethod = new CDDLMethod(MethodCall,GetDocument()->pCurDev,pItemBase); //Who deletes this ??
	TRACE(L"      CDDLMethod executes.\n");
	pMethod->Execute(this->GetLabel(),this->GetHelp());
		
	/*Vibhor 240904: End of Code*/
	delete (pMethod);
}

/* * * * * * * * * * MENU * * * * * * * * * */

/* * * * * * * * * * WINDOW * * * * * * * * * */

/* * * * * * * * * * DIALOG * * * * * * * * * */

/* * * * * * * * * * TAB-CNTRL * * * * * * * * * */

/* * * * * * * * * * GROUP * * * * * * * * * */

/* * * * * * * * * * PAGE * * * * * * * * * */

/* * * * * * * * * * TABLE * * * * * * * * * */

/* * * * * * * * * * MAIN * * * * * * * * * */



/* * * * * * * * * * SCOPE * * * * * * * * * * * */

void CdrawScope::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CScopeCtrl * pScope;
	hCgraph *pGraph = NULL;
	hCwaveform *pWave = NULL;
	hCchart *pChart = NULL;
	int iRet = -1;
	
	hCitemBase *pItemBase = this->GetItem();

	if (NULL != pItemBase)
	{
		if(pItemBase->getIType() == iT_Graph )
		{
			pGraph = (hCgraph*)pItemBase;
			if(pGraph)
			{
				if(pGraph->IsValid())
				{
					// call base class
					CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
					// Initialize this control
#ifdef _USE_PLOT_LIB
					pScope = (CScopeCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
					if(NULL == pScope)
					{
						LOGIT(CERR_LOG,"Failed to obtain scope control for graph\n");
						return;
					}
					iRet = pScope->CreateGraphControl();
					if(SUCCESS != iRet)//Vibhor 250205 : Handle Failure
					{
						LOGIT(CERR_LOG|UI_LOG,"Failed to create graph control for %u\n",pGraph->getID());
						return;
					}
					pScope->InitGraph(pGraph, GetLabel());
//DisplayWaveforms does init-actions that generate updates that need m_bReady to be set 
					pScope->m_bReady = true;
					pScope->DisplayWaveforms(0);
//was				pScope->m_bReady = true;
#endif /*#ifdef _USE_PLOT_LIB*/
				}
			}
		}
		else if (pItemBase->getIType() == iT_Chart )
		{
			pChart = (hCchart*)pItemBase;
			if(pChart->IsValid())
			{
					// call base class
					CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
					// Initialize this control
#ifdef _USE_PLOT_LIB
					pScope = (CScopeCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

					if(NULL == pScope)
					{
						LOGIT(CERR_LOG,"Failed to obtain scope control for chart\n");
						return;
					}
					
					iRet = pScope->CreateChartControl(pChart);

					if(SUCCESS != iRet)//Vibhor 250205 : Handle Failure
					{
						LOGIT(CERR_LOG|UI_LOG,"Failed to create chart control for %u\n",pChart->getID());
						return;
					}
					pScope->InitChart(pChart,GetLabel());
					pScope->DisplayChart(pChart);
					pScope->m_bReady = true;					
#endif /*#ifdef _USE_PLOT_LIB*/
				
			}
			
		}
		else
		{
			;// unknown scope type error
		}
	}
	else
	{
		//error
	}

#if 0	
	pScope->SetTitle(GetLabel());

	
#endif
}

void CdrawScope::FillSize(void)	
{
	hCitemBase* pI = GetItem(); 
	H = DFLTHGT_INDX;//was DFLTHGT; Vibhor 030105
	W = DFLTWID_INDX;//was DFLTWID; Vibhor 030105// takes care of the else clauses

	if  ( pI )
	{
		if (pI->getIType() == iT_Graph)
		{
			hCgraph* pG = (hCgraph*)pI;
			if ( pG->getHeight(H) != SUCCESS )
			{	H = DFLTHGT_INDX;//was DFLTHGT; Vibhor 030105
			}
			if ( pG->getWidth(W)  != SUCCESS )
			{	W = DFLTWID_INDX;//was DFLTWID; Vibhor 030105
			}
		}
		else
		if (pI->getIType() == iT_Chart)
		{
			hCchart* pC = (hCchart*)pI;
			if ( pC->getHeight(H) != SUCCESS)
			{	H = DFLTHGT_INDX;//was DFLTHGT; Vibhor 030105
			}
			if ( pC->getWidth(W)  != SUCCESS)
			{	W = DFLTWID_INDX;//was DFLTWID; Vibhor 030105
			}
		}
#ifdef _DEBUG
		else
		{
			cerr << "INCORRECT TYPE for a scope control."<<endl;
		}
#endif
	}

	m_MyLoc.da_size.xval = horizSizes[W];//3;
	m_MyLoc.da_size.yval = vert_Sizes[H];//11;//5;
}

void CdrawScope::Execute(WORD wMessageID, LPARAM lParam)
{
//	CScopeCtrl * pScope = (CScopeCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
}


bool CdrawScope::Update(long type,long SymID)
{
	unsigned uTemp = m_cntrlID;
	CScopeCtrl * pScope = NULL;

	DEBUGLOG(CLOG_LOG,"MSG:CdrawScope.Update       %s  0x%04x\n",
		     menuStyleStrings[mdsType],m_symblID);
	bool result = false; // true @ this control's size changed

    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawScope.Update %s 0x%04x does not have a DynCtrl Yet!\n",
					menuStyleStrings[mdsType],m_symblID);// sjv 24jul07 - bad things were happening
		return false;
	}// may go null during shutdown

	pScope = (CScopeCtrl*)m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	if (pScope)
	{
		if ( ! pScope->m_bReady)
		{
			LOGIT(CLOG_LOG,"CdrawScope.Update %s 0x%04x not ready Yet!\n",
						menuStyleStrings[mdsType],m_symblID);
			return false;
		}// skip update until initial display is complete
		pScope->UpdatePlot(type,SymID);
	}

	return result;
}

void CdrawScope::staleDynamics(void)
{
	CScopeCtrl * pScope = NULL;
    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawScope.staleDynamics %s 0x%04x does not have a DynCtrl Yet!\n",
																	"Scope",			m_symblID);
		return;
	}// may go null during shutdown

	pScope = (CScopeCtrl*)m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	if (pScope)
		pScope->StaleDynamic();
};

/* * * * * * * * * * MORE BASE * * * * * * * * * */

/* removed original test data generator 14nov06 */

int CdrawBase::getList(UINT item, menuItemList_t& vmi,menuHist_t hist )
{
	RETURNCODE     rc       = SUCCESS;
	hCitemBase*    p_myItem = GetItem();

	/* sjv 30mar05 - modify to handle all menu lists
	 * ------------ remove ------------ code removed 13nov06 (see an earlier version for content)
	 */

	/* +++++++++++ added 30mar05 sjv +++++++++++*/
	if (p_myItem->getIType() == iT_Menu )
	{
		rc =  ((hCmenu *)p_myItem)->aquire(vmi, hist);//was mdsType );
	}
	else
	if ( p_myItem->getIType() == iT_Collection )
	{
		rc =  ((hCcollection *)p_myItem)->aquire(vmi, hist);//was mdsType );
	}
/* wally didn't like the symetrical aspect of this construction
	removed 13nov06  (see an earlier version for content)
*/
	else
	{// no menu item lists
		return 0; // error:  I am supposed to have an item list  !!!!!!!!!!!!
	}// use hard coded style - TODO incorporate dialog
	/* end add sjv 30mar05 */ 
	if ( rc != SUCCESS )
	{
		return 0; // error
	}
	return vmi.size();
}

/* * * * * * * * * * CONSTANT STRING * * * * * * * * * */
// POB - 14 Oct 2004

void CdrawStr::FillSize(void)	
{
	m_MyLoc.da_size.xval = 1;
	m_MyLoc.da_size.yval = 1;

	/* stevev 17dec 04 - test sizing */
	/* we are going to calculate in pixels but giv length in dialog units */
	CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:was:> CMainFrame * pMainWnd = (CMainFrame *) AfxGetMainWnd();// to get DC
	ASSERT(::IsWindow(pMainWnd->m_hWnd));

	TEXTMETRIC tm;
	CSize      ts, ts2, ts3, ts4;

	CDC* pMainDC = pMainWnd->GetDC();
	/* change to Dialog font if different from mainframe font */
	/* CFont* entryFont = pMainDC->SelectObject(pDialogFont); */
	pMainDC->GetTextMetrics(&tm);
	// 12dec05 - special consideration of empty constant string
	if (mdsType == mds_constString && m_constString.IsEmpty( ))
	{
		ts = pMainDC->GetTextExtent("X");
	}
	else // end of 12dec05 code
		ts = pMainDC->GetTextExtent(m_constString);

	int strLen    = m_constString.GetLength();
	int avgPixWid = (int) ((((float) ts.cx) / ((float) strLen))+0.5);

	int baseUnitY = tm.tmHeight;
	int baseUnitX = (ts.cx / ((strLen/2) + 1)) / 2;

	int boxUnitCharHeight = (ts.cy * 8)/baseUnitY;// dialog units
	int lineCnt   = 0;
	int boxHeight = 0;
	int rowCnt    = 1;

	ts2.cx = (ts.cx * 4)/baseUnitX; // convert pixels to dialog units 
	ts2.cy = (ts.cy * 8)/baseUnitY; // char height in box(dialog) units
	int acw = (int) ((((float) ts2.cx) / ((float) strLen))+0.5);  // was:: (tm.tmAveCharWidth * 4)/baseUnitX;

	int width = -10 + ( COLUMSTEPSIZE * 3 );
	int hight = 16;
	/*convert to Pixels*/
	CSize pixelBoxSize;
	pixelBoxSize.cx = (width * baseUnitX) / 4;
	pixelBoxSize.cy = ( ROWSTEPSIZE * baseUnitY ) / 8;// max box height (1st row)
	int rowHeightAdder = ((ROWSTEPSIZE + ROWGAP) * baseUnitY) / 8;// Pixels

	CString locStr = m_constString;
	strLen = locStr.GetLength();
	//int tmpW = (int)  ( (((float)pixelBoxSize.cx)/((float)avgPixWid)) + .5);// num char per line
	int tmpW = (int)  ( (((float)pixelBoxSize.cx)/((float)acw)) + .5);// num char per line
	CString aLine = locStr.Left(tmpW);
	int y = aLine.ReverseFind(' ');
	if ( y > 0 )
	{
		aLine = aLine.Left(y+1);
		ts3 = pMainDC->GetTextExtent(aLine);
		ts4.cx = (ts3.cx * 4)/baseUnitX; // convert pixels to dialog units
		ts4.cy = (ts3.cy * 8)/baseUnitY;
	}
	/* change back from Dialog font if different from mainframe font */
	/* pMainDC->SelectObject(entryFont); */
	pMainWnd->ReleaseDC(pMainDC);
/*	
pRect->left   = m_nCurColumn;
pRect->top    = m_nCurRow;
// pRect->bottom = m_nCurRow    + ( ( pBase->da_size.yval) * ( ROWSTEPSIZE + ROWGAP ) );
pRect->bottom = m_nCurRow + ((8 * 2) * pBase->da_size.yval);  // 2 text lines per row, 8 pixels in height 
pRect->right  = -10 + m_nCurColumn + ( ( pBase->da_size.xval) * ( COLUMSTEPSIZE * 3  ) );
  -10 + m_nCurColumn + ( ( pBase->da_size.xval) * ( COLUMSTEPSIZE * 3  ) ) - m_nCurColumn
= -10 + ( ( pBase->da_size.xval) * ( COLUMSTEPSIZE * 3  ) )	// assume width = 1 col
= -10 + ( COLUMSTEPSIZE * 3 );
  m_nCurRow + ((8 * 2) * pBase->da_size.yval) - m_nCurRow
= ((8 * 2) * pBase->da_size.yval)								// assume ( to start) 1 row tall
= (8 * 2)
OR
  m_nCurRow    + ( ( pBase->da_size.yval) * ( ROWSTEPSIZE + ROWGAP ) ) - m_nCurRow
= ( ( pBase->da_size.yval) * ( ROWSTEPSIZE + ROWGAP ) )		// assume ( to start) 1 row tall
= ROWSTEPSIZE + ROWGAP = total height of the area
*/
};

void CdrawStr::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;
	LOGFONT LogFont;
	CFont * pFont = new CFont; // memory leak - need to delete

	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	ptr->SetWindowText(GetLabel());// puts the string into the static control

	// Now we need a fixed font type to make sure
	// that partial characters do not appear on another
	// row
	memset(&LogFont, 0x00, sizeof(LogFont));
	_tcscpy(LogFont.lfFaceName, CONST_STRING_FONT );
	LogFont.lfHeight = CONST_STRING_HIGT;

	pFont->CreateFontIndirect(&LogFont);
	
//	ptr->SetFont(pFont);


}

ITEM_ID CdrawStr::FillSymb(hCreference& rRef)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient   strVal;

	m_symblID = 0;


	if (( rc = rRef.resolveExpr(strVal) ) == SUCCESS  && 
        ( strVal.vType == CValueVarient::isString   || 
          strVal.vType == CValueVarient::isWideString )  &&
		/* stevev 12dec05 - change to the following strVal.sStringVal.size() > 0 )*/
		strVal.vIsValid )
	{
        m_constString = ((wstring)strVal).c_str(); // Use wstring for UTF-8 support, POB - 1 Aug 2008
	}
	else
	{//error
		m_constString = L"UNRESOLVED CONSTANT STRING";
	}
	return m_symblID;
}

void CdrawStr::LoadContextMenu(CMenu *pMenu, int& nPos)
{
    pMenu->LoadMenu( IDR_MENU_POPUP );
    nPos = POPUP_STRING;    
}

/* * * * * * * * * * BLANK REG * * * * * * * * * * */
// SJV - 13feb15

// use the base class...void CdrawBlank::FillSize(void)	

// use the base class...void CdrawBlank::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)


/* * * * * * * * * * IMAGE * * * * * * * * * */

CdrawImage::~CdrawImage()
{
	// Delete any draw type object that has not been
	if(m_pBase)
	{
		delete m_pBase;
		m_pBase = NULL;
	}
}

/* sjv 1Apr05 - Image is an item, use base class fillsym
ITEM_ID CdrawImage::FillSymb(hCreference& rRef)
{
	RETURNCODE rc = SUCCESS;
	
	CValueVarient msk;

	if (rRef.getRefType() == rT_Image_id)
	{
		if ( rRef.resolveExpr(msk) == SUCCESS && 
			 msk.vType == CValueVarient::isIntConst )
		{
			m_itemIndex = msk;
			CDDIdeDoc*  pDoc = GetDocument();
			if (pDoc != NULL)
			{
				m_pImage = pDoc->GetImage(m_itemIndex);
			}
			else
			{
				m_pImage = NULL;
				m_itemIndex = -1;
				rc = APP_ATTR_NOT_SUPPLIED;
			}
clog<<"Image number "
<<m_itemIndex<<((m_pImage)?" FillSymb'd.":" failed to FillSymb."  )<<endl;
		}
		else
		{
			// Error - Could not resolve mask
			TRACE(_T("CdrawBase::FillSymb - Could not resolve mask\n"));
clog <<"Image FillSymb - Could not resolve mask"<<endl;
			m_itemIndex = -1;
			rc = APP_ATTR_NOT_SUPPLIED;
		}	
	}
	m_symblID = 0;

	return 1;// non zero success
}
end sjv remove 1Apr05 */

int CdrawImage::FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre)	
{
	if ( selfSymID == 0 )
	{
		if ( m_symblID == 0 )
		{
			TRACE(_T("group.fillself ERROR: not enough info.\n"));
			return 0;
		}
		else
		{
			selfSymID = m_symblID;
		}
	}
	m_symblID = selfSymID;
	m_itemIndex = -1;

	// Fill Stuff
	hCimageItem*	pImageItem = (hCimageItem*)GetItem();
	if (pImageItem != NULL)
	{
		m_itemIndex = pImageItem->getImageIndex();
	}
	if (m_itemIndex >= 0)
	{
		CDDIdeDoc*  pDoc = GetDocument();
		if (pDoc != NULL)
		{
			m_pImage = pDoc->GetImage(m_itemIndex);
		}
		else
		{
			m_pImage = NULL;
			m_itemIndex = -1;
			return 0;
		}
#ifdef _DEBUG /* moving gif */
		int y = -1;
		if ( m_pImage )
			y = m_pImage->GetNumFrames();
		DEBUGLOG(CLOG_LOG,L"Image 0x%04x w/ index number %d with %d frames%s",pImageItem->getID(), m_itemIndex,y,
									((m_pImage)?L" FillSymb'd.\n":L" failed to FillSymb.\n"));
#endif
		return ( 1 );
	}
	else
	{// error - count is zero images
		return ( 0 );
	}
}

void CdrawImage::FillSize(void)
{// never called... 27jul12
	if ( m_pImage == NULL )
	{
		cerr << "ERROR: drawImage::FillSize called without an image."<<endl;
clog<<"Image FillSize has no image pointer."<<endl;
		return;
	}

	int trueWidth = 1;/* for debugging */

	DWORD HgtPixels = m_pImage->GetHeight();
	DWORD WidPixels = m_pImage->GetWidth();

	if (WidPixels > COL_WIDTH)
	{
		trueWidth++;
		if (WidPixels > (2*COL_WIDTH))
		{
			trueWidth++;
		}
	}
	// that was fun, now set the value

	if ( m_bIsInline )
	{
		m_MyLoc.da_size.xval = 1;
	}
	else
	{
		m_MyLoc.da_size.xval = 3;
	}
	
	/* scale the vertical to meet an oversized horizontal - ONLY on INLINE*/
	if (m_bIsInline && WidPixels > COL_WIDTH)
	{// modify the pixel height to get a good vertical
		m_pImage->SetDrawMode(DM_STRETCH2FITASPECT);
		float dstpersrc = (float)COL_WIDTH / (float)WidPixels;
		// a guess to get a good size here
		// the following assumes an equal horiz & vert resolution- ok for a guess
		HgtPixels = (DWORD)(((float) HgtPixels * dstpersrc) + 0.5);
	}


	m_MyLoc.da_size.yval = 1;
	if ( HgtPixels > INITIAL_ROWHEIGHT )
	{
		m_MyLoc.da_size.yval += (HgtPixels-INITIAL_ROWHEIGHT)/ADDITIONAL_ROWHEIGHT;
		if ((HgtPixels-INITIAL_ROWHEIGHT)%ADDITIONAL_ROWHEIGHT)
		{
			m_MyLoc.da_size.yval++;
		}
	}
	// else - leave it one

	

clog<<"Image FillSize "<<m_itemIndex<< " is "<<HgtPixels <<" high by "<< WidPixels<<" wide"<<endl;
}

void CdrawImage::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	DEBUGLOG(CLOG_LOG,L"Image creating control\n");
	
	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	DEBUGLOG(CLOG_LOG,L"Image created  control\n");

	// Initialize this control
	Update(UPDATE_ADD_DEVICE);
}

void CdrawImage::Execute(WORD wMessageID, LPARAM lParam)
{
// resolve conditional
	
	hCimageItem*  pIi = (hCimageItem*)GetItem();
	menuItemDrawingStyles_e drawStyle;
	string linkStr;
	hCmenu * pMenu;
	
	if ( pIi->isLinkString() )
	{
		return; //FALSE; we didn't handle it
	}
	else
	{
		hCitemBase*		pItem;
		if ( pIi->getLink(pItem) == SUCCESS && pItem != NULL )
		{
			if ( ! pItem->IsValid() )
			{
				{// this is for development only, real hosts don't need to do this
					CString strMessage;
					wstring lstr;
					pItem->Label(lstr);
					strMessage.Format(_T("Image Linked to an Invalid Item: (%ls)"), 
																		lstr.c_str() );
					AfxMessageBox(strMessage);
				}

				return;
			}
			itemType_t iTm = pItem->getIType();
			
			switch(pItem->getIType())
			{
				case iT_Menu:
					// Determine the style of the MENU construct
					// This is similar code to that used in 
					// CdrawMain::FillSelf()
					pMenu = (hCmenu*)pItem;
					
					if (pMenu->getStyle() == menuStyleWindow)
					{
						drawStyle = mds_window;
					}
					else
					{
						int    iTy = pMenu->getStyle();
						if (iTy != menuStyleDialog && iTy != 0)
						{// we are coercing... insert warning to error files (as per Wally 15oct09 @12:50pm)
							string sTy(menuStyleStrings[iTy-1]);
							LOGIT(UI_LOG|CERR_LOG|CLOG_LOG,"WARNING: Menu of Style %s has been "
								"coerced to a a menu of style Dialog for Image LINK!\n", sTy.c_str());
						}
						drawStyle = mds_dialog;  // Default style
					}
					break;

				case iT_EditDisplay:
					drawStyle = mds_edDisp;
					break;

				case iT_Method:
					drawStyle = mds_method;
					break;

				default:
					LOGIT(CERR_LOG|UI_LOG,"Illegal link item type.\n");
					// leave it empty
					return;// FALSE;
			}

			if (!m_pBase)
			{
				m_pBase = newDraw(drawStyle);
				if (drawStyle == mds_window)
				{// force it to be a button so it will lay itself out again
					m_pBase->m_bIsAbutton = true;
				}
				m_pBase->FillSelf(pItem->getID(),menuHist_t(),  NULL);
			}

			m_pBase->Execute(0);
			// Use the parameter zero since there is no meaningful messageID in this case
			
			if (drawStyle != mds_window)
			{
				delete m_pBase;
				m_pBase = NULL;
			}
		}
		else
		{
			LOGIT(CERR_LOG,"Could not get the link item.\n");
		}
	}
}

bool CdrawImage::Update(long type,long SymID)
{
	CButtonST * pImgCntrl;

	if ( SymID != m_symblID && SymID != -1 )// -1 matches all
		return false; //   - it is not for us...

	/*** it is for us do on any type:: ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -image-\n",SymID);
	bool result = false; // true @ this control's size changed

	DEBUGLOG(CLOG_LOG,"MSG:CdrawImage.Update %s 0x%04x in 0x%04x\n",
		     menuStyleStrings[mdsType],SymID,m_symblID);
	
    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawImage.Update %s 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
		return false;
	}
	pImgCntrl = (CButtonST *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

	
	// Determine if there is a link assoicated with this image
	hCimageItem * pIi = (hCimageItem*)GetItem();
		
	hCitemBase * pItem = NULL;
//LOGIT(CLOG_LOG,"Image Update++++++++++++++++++++++++++++\n");
/*	if ( pIi->getLink(pItem) == SUCCESS)
	{
		pImgCntrl->ActivateLinkDraw(pItem);
	}*/

	// getLink returns SUCCESS (ie, zero) if there is a link
	//         returns FAILURE (ie, one) if there is not.

////////////////////////////////////////////////
	int newIndex   = -1;
	bool isChanged = false;
	if (pIi != NULL)
	{
		newIndex = pIi->getImageIndex();
	}
	if ( newIndex >=0 && newIndex != m_itemIndex ) // got one and it's changed
	{
		m_itemIndex = newIndex;
		CDDIdeDoc*  pDoc = GetDocument();
		if (pDoc != NULL)
		{
			m_pImage = pDoc->GetImage(m_itemIndex);
			if (m_pImage != NULL && pImgCntrl != NULL)
			{
				pImgCntrl->SetImage(m_pImage);
			}
		}
		else
		{
			m_pImage = NULL;
			m_itemIndex = -1;
		}
clog<<"New Image number "
<<m_itemIndex<<((m_pImage)?" Update.":" failed to get an image."  )<<endl;
	}
	// else - no change or no image
//////////////////////////////////////////////////////

	pImgCntrl->ActivateLinkDraw(!pIi->getLink(pItem));// always draw it
	return result;
}


/* * * * * * * * * * GRID * * * * * * * * * */
// POB - 25 Jul 2005
CdrawGrid::~CdrawGrid()
{
//	CWnd * ptr;

	if (m_pDynCtrl)
	{
/*		ptr = m_pDynCtrl->GetDlgCtrl( m_nLabelID );

		if (ptr)
		{
			// Destroy the Label control (CWnd object)
			ptr->DestroyWindow();
		}*/
	}

    if (m_pPsudoDynCtrl)
    {
        delete m_pPsudoDynCtrl;
}
}

int CdrawGrid::FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre)	
{
	if ( selfSymID == 0 )
	{
		if ( m_symblID == 0 )
		{
			TRACE(_T("group.fillself ERROR: not enough info.\n"));
			return 0;
		}
		else
		{
			selfSymID = m_symblID;
		}
	}

	m_symblID = selfSymID;

//grid's fillList has no menuitems and will return 0;	return (fillList("Grid"));
	return 0;
}

void CdrawGrid::FillSize(void)	
{	
	hCgrid* pGrid;
	
	UINT Height = DFLTHGT_INDX;
	UINT Width  = DFLTWID_INDX;

	// Set Height attribute settings
	// Proposed new settings
	// These need to be changed to the
	// the "golden" rectangle
	// Once the values are determined then
	// we need to change the horizSizes[] and
	// vert_Sizes[] arrays to fix the charts
	// and graphs as well
/*	int xxSmallH = 3;
	int xSmallH = 5;
	int SmallH = 9;
	int MediumH = 11;
	int LargeH = 15;
	int xLargeH = 18;
	int xxLargeH = 21;

	// Set Width attribute settings
	int xxSmallW = 1;		// 1 column
	int xSmallW = 1;		// 1 column
	int SmallW = 2;			// 2 column
	int MediumW = 2;		// 2 column
	int LargeW = 3;			// 3 column
	int xLargeW = 3;		// 3 column
	int xxLargeW = 3;		// 3 column*/

	pGrid = (hCgrid*) GetItem();

	if (pGrid->getHeight(Height) != SUCCESS)
	{	
		Height = DFLTHGT_INDX;
	}

	if (pGrid->getWidth(Width) != SUCCESS)
	{	
		Width = DFLTWID_INDX;
	}

	m_MyLoc.da_size.xval = horizSizes[Width];
	m_MyLoc.da_size.yval = vert_Sizes[Height];
}

void CdrawGrid::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);

	// Initialize this control
	InitGrid();

	// This is the initial update on the grid control itself
	// It does not update any variables that live in the control
	// The variables are Initially updated in FillGrid()
	Update(UPDATE_ADD_DEVICE);
}

void CdrawGrid::Execute(WORD wMessageID, LPARAM lParam)  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014
{
    /*
     * CdrawGrid::Execute handles the GRID Controls
     * Notification messages based on user interaction
     * the contents of the Grids Vectors.
     * This function allows for the Editing of
     * VARIABLEs values and the dsiplay of other
     * refererenced DDL items.
     *
     * NOTE:  Currently, only constants, VARIABLEs 
     * and IMAGEs referenced on a GRID are supported.
     * Currently, the display/editing  of
	 * Bit Enumerated VARIABLEs and Images require a 
     * seperate dialog, POB - 5/7/2014
     */

    NM_GRIDVIEW * pNMGV = (NM_GRIDVIEW* )lParam;

    if (pNMGV  == NULL )
    {
        return;
}

    CGridCtrl * pGridCtrl;
    GV_ITEM Item;
    Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_PARAM|GVIF_STATE;

    if ( m_pDynCtrl == NULL )
    {
        LOGIT(CLOG_LOG,"CdrawGrid.Execute 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
        return;
    }

    pGridCtrl = (CGridCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

    Item.row = pNMGV->iRow;
    Item.col = pNMGV->iColumn;

    pGridCtrl->GetItem(&Item);

    CdrawBase * pBase = (CdrawBase *) Item.lParam;

    int i = 0;

    if (pBase)
    {
        if (pNMGV->hdr.code == NM_DBLCLK)
        {
            // User has doubled clicked on a Grid Cell
            if ( pBase->mdsType == mds_var     || 
                 pBase->mdsType == mds_enum    ||
                 pBase->mdsType == mds_bit     ||
                 pBase->mdsType == mds_index    )
            {
                /*
                 * A Double Clicks on (non-enumerated) VARIABLEs
                 * implies in line editing which is controlled by the
                 * GVN_BEGINLABELEDIT, GVN_ONEINPLACEDIT and GVN_ENDLABELEDIT
                 * messages
                 */
                return;
            }

            /* 
             * All other DDL Items (including like Bit Enumerated),
             * we will need to create a psuedo menu and execute a
             * dialog with this CdrawBase object.
             */
            DevInfcHandle_t dh;
            hCdeviceSrvc*   pD = NULL; 
            if (m_pItem)
            {
                dh = m_pItem->devHndl();
                pD = m_pItem->devPtr();
            }
            else
            if(m_pRef)
            {
                dh = m_pRef->devHndl();
                pD = m_pRef->devPtr();
            }
            else
            {// we can't get device services
                dh = -1;
            }

            if ( pD )   // won't work without services from the device
            {
                hCreference tempref(dh);
                hCbitString tempqual(dh);
                hCmenu* pMenu = NULL;
                BOOL bIsReadOnly = FALSE;
                
                hCmenuItem bemi(dh);
                tempref.setRef(pBase->m_symblID, 0, iT_Variable);
                bemi.setReference(tempref);

                if (pBase->mdsType == mds_bitenum)
                {
                    CdrawVar * pdrawVar = (CdrawVar *) pBase;
                    bIsReadOnly = ((hCVar*) pdrawVar->GetItem())->IsReadOnly();
                }

                tempqual.setBitStr(  ((bIsReadOnly || !pGridCtrl->IsEditable())?READ_ONLY_ITEM:0) | 
                                     ((pBase->m_bIsDisplayVal)?DISPLAY_VALUE_ITEM:0) | 
                                     ((pBase->m_bIsReview)?REVIEW_ITEM:0)   );
                bemi.setQualifier(tempqual);
                bemi.setHistory(pBase->class_history);        
                
                bemi.setDrawAs( pBase->mdsType );
                pMenu = (hCmenu*) dynamic_cast<hCddbDevice*>(pD)->newPsuedoMenu(&bemi);
                pMenu->setStyle(menuStyleDialog);

                //make a new drawdlg
                CdrawDlg* pDlg = new CdrawDlg(NULL);
                pDlg->m_pItem = pMenu;
                pDlg->m_symblID = pMenu->getID();
                pDlg->m_bSingleMenuItem = TRUE;  // Ignore Layout rules - start in 1st column

                //execute
                pDlg->Execute(NULL);

                delete pDlg;

                // We need to do a refresh of this value
                // CdrawGrid::Update() may not be called
                // if there is a change
                Update(UPDATE_ONE_VALUE, pBase->m_symblID);
            }
        }
       
        if (pNMGV->hdr.code == GVN_BEGINLABELEDIT)
        {
            /*
             * The user has double clicked on the label to begin
             * "inline editing"...
             * Make sure that we give the Grid control
             * the correct control ID before edit control is created.
             */
            pGridCtrl->m_cntrlInPlaceID = pBase->m_cntrlID;
        }

        if (pNMGV->hdr.code == GVN_ONINPLACEDIT)
        {
            // The inline edit control is now fully instaniated...
            // We can now use it
            wMessageID = EN_SETFOCUS;   // Inform the cdrawBase object that it now has the Grid Cell focus
            pBase->ExecuteCID(pBase->m_cntrlID, wMessageID);
        }

        if (pNMGV->hdr.code == GVN_ENDLABELEDIT)
        {
            wMessageID = EN_KILLFOCUS;   // Inform the cdrawBase object that it now has the Grid Cell focus
            pBase->ExecuteCID(pBase->m_cntrlID, wMessageID);

            /*
             * We need to do a refresh of this value!
             *
             * This is especially important if the value entered
             * has been rejected due to a bad entered value or one
             * out of range.
             */
            Update(UPDATE_ONE_VALUE, pBase->m_symblID);
        }

        if (pNMGV->hdr.code == GVN_SELCHANGED)
        {
            /*
             * The user has actually changed the actual focus!
             *
             * Since this is not a loss of focus due to another 
             * application or even am execution of a  pre/post
             * methods, we need to reset 'last_focus' to zero.
             */
            last_focus = 0;
        }
    }
}

bool CdrawGrid::Update(long type,long SymID)
{
//	CWnd * ptr;
	CGridCtrl * pGridCtrl;
	GV_ITEM Item;
	Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_PARAM|GVIF_STATE;
	ulong sym;
	// stevev 11sep13  unsigned handling;
	UINT64 handling;

	// this kinda defeats the purpose... stevev 12jul07
	//if ( SymID != m_symblID && SymID != -1 )// -1 matches all
	//	return false; //   - it is not for us...

	/*** it is for us ***/
	DEBUGLOG(CLOG_LOG,"              Match 0x%04x  -enum-\n",SymID);
	bool result = false; // true @ this control's size changed

	DEBUGLOG(CLOG_LOG,"MSG:CdrawGrid.Update %s 0x%04x in 0x%04x\n",
		     menuStyleStrings[mdsType],SymID,m_symblID);
	
    if ( m_pDynCtrl == NULL )
	{
		LOGIT(CLOG_LOG,"CdrawGrid.Update 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
		return false;
	}
	pGridCtrl = (CGridCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

/* stevev 01feb06 - notes on symbol numbers:: 
	we can overload a symbol number to zero (error - bad symbol #) or 
	negative one (unknown symbol) - both illegal numbers	
	We CANNOT test for these two values using <0 or > 0...
	High value symbol numbers ARE legal!!
*/
//	if (ptr)
// stevev 01feb06 was:	if (SymID < 0)  // Determine if the update is on the grid ( - 1 means update itself)
	if (SymID == -1)
	{
		// Update the Label
		// The grid control will only show this label if the 
		// WS_CAPTION is used during the CDynControls::AddDlgControl()
		pGridCtrl->SetWindowText(GetLabel());
		
		/* 
		 * Make sure that the cell that holds the header
		 * can be read
		 */
		// The strings for the headers for sizes using X_SMALL when
		// do not work welll when ExpandToFit() used after
		// AutoSize() - also see comments below
		// TO DO:  AutoSize() should be used when the scroll bars are showing
		//pGridCtrl->AutoSize(GVS_BOTH);
		
		/*
		 * Use the full width of the rectangular region
		 * if scroll bars are not shown
		 */
		// TO DO:  Only call ExpandToFit() if the scroll bar
		// is not showing (ie, after AutoSize() is called)
		// Right now ExpandToFit() for all cases
		// Please note that X_SMALL might cause strings for
		//label and header not to show up fully
// stevev 12jul07 - try a different tact...was>		pGridCtrl->ExpandToFit();
//		pGridCtrl->ExpandColumnsToFit();// stevev 12jul07
		
		// Check for read-only attributes
		((hCgrid*) GetItem())->getHandling(handling);
	
		BOOL access = handling & WRITE_HANDLING;	// 2 = editable, 0 = non-editiable
//stevev 8sep10...this won't let a read-only window draw		pGridCtrl->EnableWindow(handling & WRITE_HANDLING);
		pGridCtrl->SetEditable(handling & WRITE_HANDLING);
		pGridCtrl->ExpandColumnsToFit();// stevev 9sep10-new requirement
	}
	
// stevev 01feb06 (see note above) was::	if (SymID > 0) 
	// Determine if any children existing on the control needs updating
// stevev 12jul07 - cells have to be updated all the time 
//	else // the zero is tested before use below
//	{
		// Update any variables existing on the grid
		for (int col = 0; col < pGridCtrl->GetColumnCount(); col++)
		{
			for(int row = 0; row < pGridCtrl->GetRowCount(); row++)
			{
				sym = 0;
				Item.row = row;
				Item.col = col;

				pGridCtrl->GetItem(&Item);

				CdrawBase * pBase = (CdrawBase *) Item.lParam;
				
				if ( ! pBase)
				{
					continue; // if no item, then no update required
				}
					
				sym = pBase->m_symblID;

				if ((sym && sym == SymID) || SymID == -1 )
				{
					CString strValue;

                    if ( pBase->mdsType == mds_var     || 
                         pBase->mdsType == mds_enum    ||
                         pBase->mdsType == mds_bitenum ||
                         pBase->mdsType == mds_bit     ||
                         pBase->mdsType == mds_index    )
                    {
					CdrawVar * pdrawVar = (CdrawVar *) pBase;
					DEBUGLOG(CLOG_LOG,"MSG:Grid scan matchd 0x%04x in R:%d C:%d\n",
													        SymID,row,col);

					// Check for read-only attributes
//					ptr->EnableWindow(!((hCVar*) GetItem())->IsReadOnly() && !m_bIsReadOnly);
					
                        if (pBase->mdsType == mds_bitenum)
                        {
                            // Currently, Bit Enumerated variables require a seperate window
                            // for editing
                            Item.nState |= GVIS_READONLY; // Set the cell to read-only
                        }
                        else
                        {
					if (((hCVar*) pdrawVar->GetItem())->IsReadOnly() ||
						! pGridCtrl->IsEditable() )// stevev 8sep10 - all readonly if grid read only
					{
						Item.nState |= GVIS_READONLY; // Set the cell to read-only
					}
					else
					{
						Item.nState &= ~GVIS_READONLY; // Allow the cell to be edited
					}
                        }

                        // We need to indicate if the display value has changed but not yet
                        // written to the device, POB - 5/5/2014
                        if (((hCVar*) pdrawVar->GetItem())->isChanged())
                        {
                            pGridCtrl->SetItemBkColour(row, col, RGB(255, 255, 0));
                        }
                        else
                        {
                            pGridCtrl->SetItemBkColour(row, col);
                        }

					strValue= pdrawVar->ReadVal();
                    }
                    else
                    {
                        /* 
                         * For now, all other DDL types will display the label.
                         * This includes, Images, charts, graphs, etc.
                         *
                         * NOTE:  The label will act as a link/button.
                         */
                        strValue= pBase->GetLabel();
                        Item.nState |= GVIS_READONLY; // Set the cell to read-only
                    }

					// Get the updated value
					Item.strText = strValue;

					// Update the item
					pGridCtrl->SetItem(&Item);

					// Refresh the control so that the update will show immediately
					pGridCtrl->Refresh();
				}
			}
		}
//	}


	// This variable has been updated.  Any editing has been erased 
	// as a result of this update.  Allow for the editing to begin 
	// again
	// no longer used 11sep07  m_bTriggerEdit = FALSE;// this is a no-op...
	return result;
}

void CdrawGrid::InitGrid()
{
	CGridCtrl * pGridCtrl;
	pGridCtrl = (CGridCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	int rc = SUCCESS;
	hCgrid* pGrid;
	gridDesc_t   grdDesc;
	CDDIdeDoc* pDoc = NULL;
	GV_ITEM Item;
	int row = 0;
	int col = 0;
	int * I = 0;
	int * J = 0;
	int vectors;
	int size;
	float fltVal = 0;
	BOOL isHeader = FALSE; // Assume all headers are empty strings
    pnt_t temp = { 1, 1 };  // Added to suuport m_pPsudoDynCtrl, POB - 5/5/2014

	if (!pGridCtrl)
	{
		return;
	}

    /*
     * Each CdrawBase child requires a CDynControls parent window
     * The parent window in this case is the Grid Control
     * All CdrawBase children will use this one control, POB - 5/5/2014
     */
    m_pPsudoDynCtrl = new CDynControls(temp, 0, pGridCtrl, 0);

	pGrid = (hCgrid*) GetItem();

	if(pGrid)
	{
		// Read the DD Information and its data				
		rc = pGrid->getVectorList(grdDesc);

		size = grdDesc.size();

		if (rc == SUCCESS && grdDesc.size() > 0)
		{
			LOGIT(CLOG_LOG,"Got %d vectors.\n",grdDesc.size());

			vectors = grdDesc.size(); 

			// Get the values for the grid
			pGridCtrl->EnableDragAndDrop(TRUE);
			COLORREF t = //::GetSysColor(CTLCOLOR_EDIT);
				pGridCtrl->GetDefaultCell(false,false)->GetBackClr();
			pGridCtrl->SetGridBkColor(t);// try a white background..stevev 12jul07..
												//     was> ::GetSysColor(CTLCOLOR_DLG));
			pGridCtrl->ExpandColumnsToFit();// stevev 9sep10-new requirement

			/*
			 * Determine grid orientation
			 */
			gridOrient_t orient;

			pGrid->getOrient(orient);

			if (orient == gd_Horizontal)
			{
				pGridCtrl->SetFixedColumnCount(1);	// This forces a row to be read only, but it has lines between cells
				pGridCtrl->SetRowCount(vectors);	// Number of vectors
				pGridCtrl->SetGridLines(GVL_BOTH);  // changed from HORZ 9sep10
				
				/*
				 * Here we are going to insert items into the gird
				 * from left to right
				 * and then move down to the next row
				 */
				J = &col;
				I = &row;
			}
			else // Vertical Orientation - default
			{
				pGridCtrl->SetFixedRowCount(1);
				pGridCtrl->SetColumnCount(vectors);	  // Number of vectors
				pGridCtrl->SetGridLines(GVL_BOTH);	 // changed from VERT 9sep10
				
				/*
				 * Here we are going to insert items into the grid
				 * from top to bottom
				 * and then move over to the top of the next column
				 */
				I = &col;
				J = &row;
			}

			for(gridDesc_t::iterator iT = grdDesc.begin(); iT != grdDesc.end(); iT++, (*I)++) // I points to column in vertical config
			{
				// Get the Vector
				gridGroupDesc * pGridGroup = (gridGroupDesc*)(&(*iT));// PAW 03/03/09 added &*
				
				varientList_t vectorValues = pGridGroup->vectorValues;
				
				int size = vectorValues.size();

				(*J) = 0;
				
				if (orient == gd_Horizontal)
				{
					if (size + 1 > pGridCtrl->GetColumnCount())
					{
						pGridCtrl->SetColumnCount(size + 1);	// number of vector values + Heading (ie, 1) 
					}
				}
				else
				{
					if (size + 1 > pGridCtrl->GetRowCount())
					{
						pGridCtrl->SetRowCount(size + 1);		// number of vector values + Heading (ie, 1) 
					}
				}

				// Cycle through the list of values for this vector
				// stevev 8sep10 - you can't decrement a begin() vector...make into a while loop
				// for (varientList_t::iterator vT = vectorValues.begin(); vT != vectorValues.end(); vT++, (*J)++) // J points to row in vertical config
				varientList_t::iterator vT = vectorValues.begin();// stevev 8sep10
				while(vT != vectorValues.end())// stevev 8sep10
				{ 
					Item.row     = row; 
					Item.col     = col;
					Item.strText = "";
					Item.lParam  = 0;
					Item.nState  = 0;

					// Row Zero is the header row in Vertical example and Column zero is the header for horizontal
					if ( (row == 0 && orient == gd_Vertical) || (col == 0 && orient == gd_Horizontal))
					{
						// Set the headings
						Item.nFormat = DT_CENTER|DT_WORDBREAK;
						Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_PARAM;

						// Obtain the Header text from vector
						Item.strText = pGridGroup->headerValue.c_str();
					// stevev 8sep10 -you can't decrement a begin() iterator 	vT--;	// row  = 0 is the vertical heading not the vector value.  Since Vector 0 belongs on row 1, we need to decrement the iterator
					//    just neglect to increment it....
						pGridCtrl->SetItem(&Item);

						if (Item.strText != " ") // The device object is currently providing a space between "" for an empty string
						{
							isHeader = TRUE; // At least one header does not have an empty string
						}
					}
					else
					{
						CValueVarient varient = (*vT);
						CString numfmt = getNumericFormat(varient);
						
						Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
                        Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_PARAM|GVIF_STATE|GVIF_TYPE; // GVIF_TYPE added to handle different inline editing controls, POB - 5/5/2014

						switch (varient.vType)
						{	
							// Constant Integer
							case CValueVarient::isIntConst:
								//Item.strText.Format(_T("%d"),varient.vValue.iIntConst);	//  [8/14/2014 timj]
								Item.strText.Format(numfmt,varient.vValue.iIntConst);
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;

							// Constant Integer - very long integers, Steve - 14 Aug 2008	//  [8/14/2014 timj]
							case CValueVarient::isVeryLong:
								//Item.strText.Format(_T("%I64d"),varient.vValue.longlongVal);
								Item.strText.Format(numfmt,varient.vValue.longlongVal);
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;

							// Constant Float
							case CValueVarient::isFloatConst:
								//Item.strText.Format(_T("%f"),(float)varient.vValue.fFloatConst); //  [8/14/2014 timj]
								Item.strText.Format(numfmt,varient.vValue.fFloatConst);
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;
							
							// Constant String
							case CValueVarient::isString:
								Item.strText = ((string)varient).c_str();
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;
							
							// Constant Wide String - UTF8 support, Steve - 14 Aug 2008
							case CValueVarient::isWideString:
								Item.strText = ((wstring)varient).c_str();
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;

							// Constant boolean
							case CValueVarient::isBool:
								if ( varient.valueIsZero() )
									Item.strText = "FALSE";
								else
									Item.strText = "TRUE";
								Item.nState = GVIS_READONLY;
								pGridCtrl->SetItem(&Item);
								break;

							case CValueVarient::isSymID:
							{
								int sym = varient.vValue.varSymbolID;
							
								hCitemBase* pIB;
								
								if (PCURDEV)
								{
									PCURDEV->getItemBySymNumber(sym, &pIB);
								}
					
								CdrawBase* pP = NULL;

                                // Determine if it is a VARIABLE, POB - 5/5/2014
                                if (pIB && pIB->getIType() == iT_Variable)
                                {                        
									hCVar* pVar = (hCVar*) pIB;

									switch(pVar->VariableType())
									{
										case vT_Password:
										case vT_FloatgPt:
										case vT_Double:
										case vT_Unsigned:
										case vT_Integer:
										case vT_PackedAscii:
										case vT_Ascii:
										case vT_HartDate:
										case vT_TimeValue:
											if (pP == NULL)													
											{
												pP = newDraw(mds_var);											
											}
											// common code
											pP->FillSelf(sym, menuHist_t(), NULL);
											pP->m_pDynCtrl = m_pPsudoDynCtrl;
											pP->Set_Focus_Msg  = EN_SETFOCUS;
											pP->Kil_Focus_Msg  = EN_KILLFOCUS;
											Item.lParam = (long)pP;
											if (pVar->VariableType()==vT_Password)
												Item.mask |= GVIF_PASSWD;				// mask on
											Item.strText = "";
											Item.nEdType = 0;  // 0 = Editbox control
											pGridCtrl->SetItem(&Item);
											Item.mask = ~Item.mask & GVIF_PASSWD;		// mask off
											break;

										case vT_BitEnumerated:
											if (pP == NULL)
											{
												pP = newDraw(mds_bitenum);
											}
											pP->FillSelf(sym, menuHist_t(mds_grid), NULL);
											pP->m_pDynCtrl = m_pPsudoDynCtrl;
											pP->Set_Focus_Msg  = EN_SETFOCUS;
											pP->Kil_Focus_Msg  = EN_KILLFOCUS;
											Item.lParam = (long)pP;
											Item.strText = "";
											Item.nEdType = -1;  // -1 = no edit control, 
											//    we are lanuching a seperate dialog window
											pGridCtrl->SetItem(&Item);
											break;

										case vT_Enumerated:
											if (pP == NULL)	
											{
												pP = newDraw(mds_enum);	
											}
										case vT_Index:
											if (pP == NULL)	
											{
												pP = newDraw(mds_index);	
											}
											pP->FillSelf(sym, menuHist_t(mds_grid), NULL);
											pP->m_pDynCtrl = m_pPsudoDynCtrl;
											pP->Set_Focus_Msg  = EN_SETFOCUS;
											pP->Kil_Focus_Msg  = EN_KILLFOCUS;
											Item.lParam = (long)pP;
											Item.strText = "";
											Item.nEdType = 1;  // 1 = ComboBox control
											pGridCtrl->SetItem(&Item);
											break;
										// error - no default supplied!!!!!!!!!!!!!!!!!!!!!
									}
									// Initial update on variable
									Update(UPDATE_ADD_DEVICE,sym);
								}
                                else // not pIB or not a variable
								if (pIB)
                                {   /*
                                     * Create all other possible CdrawBase objects
                                     *
                                     * NOTE:  Currently, only Images are being
                                     * passed down in the above vector list,
                                     * POB - 5/7/2014
                                     */
                                    switch(pIB->getIType())
                                    {
                                        case iT_Method:
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_method);
                                            }
                                        case iT_Menu:
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_menu);
                                            }
                                        case iT_EditDisplay:
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_edDisp);
                                            }
                                        case iT_Grid:
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_grid);
                                            }
                                        case iT_Chart:
                                        case iT_Graph:
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_scope);
                                            }
                                        case iT_Image: 
                                            if (pP == NULL)
                                            {
                                                pP = newDraw(mds_image);
                                            }
                                        // common code
                                        pP->FillSelf(sym, menuHist_t(), NULL);
                                        pP->m_pDynCtrl = m_pPsudoDynCtrl;
                                        Item.lParam = (long)pP;
                                        Item.strText = "";
                                        pGridCtrl->SetItem(&Item);										
										// error - no default supplied!!!!!!!!!!!!!!!!!!!!!
                                    }// endswitch
                                }// endelse
                            }
							break;

							default:
								// stevev 12jul07 - invalid,isOpcode,isDepIndex varients are discarded
								break;
						}//endswitch
						
						vT++;   // stevev 8sep10 - increment here in the else, not in the if
					}//endelse
					(*J)++; // stevev 8sep10 - always increment J
				}// next vT
			}

			// Delete the header if all the headers are empty strings
			if (!isHeader)
			{
				if (orient == gd_Horizontal)
				{
					pGridCtrl->DeleteColumn(0);
				}
				else
				{
					pGridCtrl->DeleteRow(0);
				}
			}
			Update();
		}
	}
}

// Added staleDynamics() to continously update
// VARIABLEs with class DYNAMIC, POB - 5/1/2014
void  CdrawGrid::staleDynamics(void)
{
    CGridCtrl * pGridCtrl;
    GV_ITEM Item;
    Item.mask = GVIF_TEXT|GVIF_FORMAT|GVIF_PARAM|GVIF_STATE;

    if ( m_pDynCtrl == NULL )
    {
        LOGIT(CLOG_LOG,"CdrawGrid.Update 0x%04x does not have a DynCtrl Yet!\n",m_symblID);
        return;
    }

    pGridCtrl = (CGridCtrl *) m_pDynCtrl->GetDlgCtrl( m_cntrlID );

    if (pGridCtrl)
    {
        // Update any variables existing on the grid
        for (int col = 0; col < pGridCtrl->GetColumnCount(); col++)
        {
            for(int row = 0; row < pGridCtrl->GetRowCount(); row++)
            {
                Item.row = row;
                Item.col = col;

                pGridCtrl->GetItem(&Item);

                CdrawBase * pBase = (CdrawBase *) Item.lParam;
                
                if (!pBase)
                {
                    continue; // if no item, then no stale indication is required
                }

                pBase->staleDynamics();
            }
        }
    }
}