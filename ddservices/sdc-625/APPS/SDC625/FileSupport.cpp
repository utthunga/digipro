/*************************************************************************************************
 *
 * $Workfile: FileSupport.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		FileSupport.cpp : Implements the OS file interface to the device object.
 */

#include "FileSupport.h"
#include "ddbGeneral.h"
#undef ARGUMENTS_H
#include "args.h"		// for environment def

#define BIGBUFFER_LEN  0x0100000

CsdcFileSupport::~CsdcFileSupport()
{
	if (hFile != INVALID_HANDLE_VALUE) // one is opened
	{ 
		CloseHandle(hFile); 
		hFile = INVALID_HANDLE_VALUE;
	}
	if ( pBigBuffer )
	{
		delete[] pBigBuffer;
	}
}

//function to be implemented in derived class that handles initializations 
RETURNCODE CsdcFileSupport::OpenFileFile(Indentity_t* deviceID, itemID_t fileID, bool clearOnOpen)
{
	RETURNCODE rc = FAILURE;
	tstring fileSpec, fileName;

	if (hFile != INVALID_HANDLE_VALUE) // one is opened
	{ 
		LOGIT(CERR_LOG,"ERROR: Tried to open a file while one was opened.\n");
		CloseHandle(hFile); 
		hFile = INVALID_HANDLE_VALUE;
	}
	if ( m_strFilePath.empty() )
	{
#ifndef _WIN32_WCE	// PAW 30/04/09 function missing
		fileSpec = getFileSupPath();
#endif
		if ( fileSpec.empty() || fileSpec == _T("") )
		{
			LOGIT(CERR_LOG,"ERROR: could not get a file path.\n");
			return FILE_RV_FAILED;
		}
		LOGIT(CLOG_LOG,_T("Got the file path: |%s|\n"),fileSpec.c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,_T("Had the file path: |%s|\n"),m_strFilePath.c_str());
		fileSpec = m_strFilePath;
	}
	fileName = createFileName(deviceID, fileID);
	if ( fileName.empty() || fileName == _T("") )
	{
		LOGIT(CERR_LOG,"ERROR: could not get a file name.\n");
		return FILE_RV_FAILED;
	}
	if ( fileSpec[fileSpec.length()] != _T('\\') )
	{
		fileSpec += _T("\\");
	}
	fileSpec+=fileName;

	DWORD openMode = OPEN_EXISTING;
	if (clearOnOpen)
	{
		LOGIT(CLOG_LOG,"Setting file to open empty.\n");
		openMode = TRUNCATE_EXISTING;
	}

	hFile = CreateFile(fileSpec.c_str(),// open the filename 
		GENERIC_READ|GENERIC_WRITE,     // open for reading 
		0,                              // do not share 
		NULL,                           // no security 
		openMode,                  // existing file only 
#ifdef _DEBUG
		FILE_FLAG_WRITE_THROUGH,
#else
		FILE_ATTRIBUTE_NORMAL,          // normal file 
#endif
		NULL);                          // no attr. template 

	if (hFile == INVALID_HANDLE_VALUE) // probably does not exist
	{  
		hFile = CreateFile(fileSpec.c_str(),// open the filename 
			GENERIC_READ|GENERIC_WRITE,     // open for reading 
			0,                              // do not share 
			NULL,                           // no security 
			OPEN_ALWAYS,					// open or create 
#ifdef _DEBUG
			FILE_FLAG_WRITE_THROUGH,
#else
			FILE_ATTRIBUTE_NORMAL,          // normal file 
#endif
			NULL);                          // no attr. template 

		if (hFile == INVALID_HANDLE_VALUE) 
		{ 
			LOGIT(CERR_LOG,_T("ERROR: Create file '%s' failed.\n"),fileSpec.c_str());
			rc = FILE_RV_FAILED;
		} 
		else
		{
			LOGIT(CLOG_LOG,_T("File '%s' opened.\n"),fileSpec.c_str());
			rc = FILE_RV_FILE_CREATED;
		}
	}
	else
	{
		LOGIT(CLOG_LOG,_T("File '%s' opened.\n"),fileSpec.c_str());
		rc = SUCCESS; // opened an existing file
	}
	return rc;
}

/* whichever type is opened */
//function to be implemented in derived class that handles cleanup 
RETURNCODE CsdcFileSupport::CloseFile(void)
{
	RETURNCODE rc = SUCCESS;	
	BOOL   bResult = FALSE;
	DWORD  bytesWritten = 0;
	
	if ( pBigBuffer != NULL && BigBuffPoint > 0 )
	{// flush the buffer before closing
		bResult = ::WriteFile(hFile, pBigBuffer, BigBuffPoint, &bytesWritten, NULL);
		if ( bytesWritten != BigBuffPoint || bResult == 0 )
		{
			LOGIT(CERR_LOG,"ERROR: FILE's buffer failed to flush at close.\n");
			rc = FILE_RV_FAILED;
		}
		else
		{
			BigBuffPoint = 0;
			rc = SUCCESS;
		}
	}


	if (hFile != INVALID_HANDLE_VALUE) // none opened
	{ 
		CloseHandle(hFile); 
		hFile = INVALID_HANDLE_VALUE;
	}
	if ( pBigBuffer != NULL )
	{
		delete[] pBigBuffer;
		pBigBuffer = NULL;
	}

	return rc;
}

//function to be implemented in derived class that actually reads the file info
RETURNCODE CsdcFileSupport::ReadFile(uchar* buffer, int& buffLen, ulong location)
{
	RETURNCODE rc = FAILURE;
	BOOL  bResult = FALSE;
	DWORD  bytesRead, newloc = 0;

	if ( buffer == NULL || buffLen == 0 )
	{
		return APP_PARAMETER_ERR;
	}

	if (hFile == INVALID_HANDLE_VALUE) // none opened
	{
		return FILE_RV_NOT_AVAILABLE;
	}

	if ( location )
	{
		newloc = SetFilePointer(hFile,location,NULL,FILE_BEGIN);
		if (newloc != location)
		{
			return FILE_RV_SEEK_FAILED;
		}
	}
	
	bResult = ::ReadFile(hFile, buffer, buffLen, &bytesRead, NULL);  // returns false on failure
	if (bResult &&  bytesRead == 0) 
	{// we're at the end of the file 
		buffLen = 0;
		buffer[0] = '\0';// stevev 27jul06 - added for clarification during debugging (non-functional)
		rc = FILE_RV_EOF;
	}
	else
	if (bResult)
	{
		buffLen = bytesRead;
		rc = SUCCESS;
	}
	else // read failed - not eof
	{
		buffLen = bytesRead;// in case it got something
		rc = FILE_RV_FAILED; 
	}

	return rc;
}

//function to be implemented in derived class that actually writes the file info
RETURNCODE CsdcFileSupport::WriteFile(uchar* buffer, int buffLen)
{	
	RETURNCODE rc = FAILURE;
	BOOL   bResult = FALSE;
	DWORD  bytesWritten, newloc = 0;
	int diff = 0;

	if ( buffer == NULL || buffLen == 0 )
	{
		return APP_PARAMETER_ERR;
	}
	if (hFile == INVALID_HANDLE_VALUE) // none opened
	{
		return FILE_RV_NOT_AVAILABLE;
	}

	if ( pBigBuffer == NULL )
	{
		pBigBuffer   = new uchar[BIGBUFFER_LEN];
		BigBuffPoint = 0;
		if (pBigBuffer == NULL)
		{
			return ( FILE_RV_BUF_FAIL );
		}
	}
	diff = BIGBUFFER_LEN - BigBuffPoint - buffLen;
	if ( diff < 0 )
	{
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"*        file writing.");
							logTime();// isa logif start_stop (has newline)
		//bResult = ::WriteFile(hFile, buffer, buffLen, &bytesWritten, NULL);
		bResult = ::WriteFile(hFile, pBigBuffer, BigBuffPoint, &bytesWritten, NULL);
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"*        file written.");
							logTime();// isa logif start_stop (has newline)
		if ( bytesWritten != BigBuffPoint || bResult == 0 )
		{
			//rc = FILE_RV_FAILED;
			return (FILE_RV_FAILED);
		}
		else
		{
			BigBuffPoint = 0;
			rc = SUCCESS;
		}
	}
	diff = BIGBUFFER_LEN - BigBuffPoint - buffLen;
	if ( diff < 0 )
	{
		return ( FILE_RV_BUF_FAIL );
	}
	else
	{
		memcpy((&(pBigBuffer[BigBuffPoint])),buffer,buffLen);
		BigBuffPoint += buffLen;
			rc = SUCCESS;
	}
	

	return rc;
}


tstring CsdcFileSupport::createFileName(Indentity_t* deviceID, itemID_t fileID)
{
	tstring retName;
	TCHAR b[256];

	_stprintf(b,_T("%04X%04x_%08x_%04x.%s"),deviceID->wManufacturer,deviceID->wDeviceType,
		    deviceID->dwDeviceId,fileID,FILE_EXTENSION);
	retName = b;
	return retName;
}
#ifndef _WIN32_WCE	// PAW 30/04/09 function missing
tstring CsdcFileSupport::getFileSupPath(void)
{
	TCHAR   tmpEnv[MAX_PATH];

	if ( m_strFilePath.empty() )
	{
		DWORD tmpCnt = GetEnvironmentVariable(FILE_STORE_ENV, tmpEnv, MAX_PATH );
		if ( tmpCnt != 0 )
		{// set to environment
			m_strFilePath = tmpEnv;
		}
		// else - fall thru to fill from default
	}
	// else - fall thru to fill from default
	if ( m_strFilePath.empty() )// still empty...
	{
		tstring tmpS;
		tmpS = (LPCTSTR)m_copyOfCmdLine.cl_DbP;
		if ( tmpS[tmpS.length()] != _T('\\') )
		{
			tmpS += _T("\\");
		}
		tmpS += _T("..\\Files");

		{// check for existance

			WIN32_FIND_DATA FindFileData;
			HANDLE hFind;

			hFind = FindFirstFile(tmpS.c_str(), &FindFileData);

			if (hFind == INVALID_HANDLE_VALUE) 
			{// doesn't exist
				if (CreateDirectory(tmpS.c_str(),NULL) )// success
				{
					hFind = FindFirstFile(tmpS.c_str(), &FindFileData);

					if (hFind == INVALID_HANDLE_VALUE) 
					{// doesn't exist
						m_strFilePath = _T(""); // error return
					}
					else
					{// get the full path string
						FindClose(hFind);
						DWORD r = GetLongPathName(tmpS.c_str(),tmpEnv,MAX_PATH);
						if (r > MAX_PATH || r == 0 ) // error
						{
							m_strFilePath = _T(""); // error return
						}
						else
						{
							m_strFilePath = tmpEnv;
						}
					}
				}
				else// 0 on failure
				{
					m_strFilePath = _T(""); // error return
				}
			} 
			else 
			{// exists
				FindClose(hFind);
				DWORD r = GetLongPathName(tmpS.c_str(),tmpEnv,MAX_PATH);
				if (r > MAX_PATH || r == 0 ) // error
				{
					m_strFilePath = _T(""); // error return
				}
				else
				{
					m_strFilePath = tmpEnv;
				}
			}
		}// end check existance
	}

	return m_strFilePath;
}
#endif
