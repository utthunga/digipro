/**********************************************************************************************
 *
 * $Workfile: SDC625_drawLayout.cpp $
 * 13nov06-stevev - stevev
 *     Moved from SDC625_drawTypes.cpp
 *		The file was just too big for Bill to work 
 *
 *	This holds all of the menu classes
 *
 **********************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2006, HART Communication Foundation, All Rights Reserved 
 **********************************************************************************************
 *
 * Description:
 *		home of the SDC625 CdrawLayout,CDrawWin,  CDrawDlg, CdrawTab, CdrawPage
 *                       CdrawGroup, CdrawMenu, CdrawTbl, and CdrawMain  classes
 *		
 */


#include "SDC625_WindowView.h"
#include "Dyn_Controls.h"
#include "SDC625_drawTypes.h"
#include "ddbMenuLayout.h"


#ifdef _DEBUG
 #ifndef STARTSEQ_LOG
  #define STARTSEQ_LOG
 #endif
#else
 #ifdef STARTSEQ_LOG
  #undef STARTSEQ_LOG
 #endif
#endif

extern const int horizSizes[] ;
extern const int vert_Sizes[] ;

const pnt_t dfltOfst = {0,0};

/**********************************************************************************************
 * CdrawLayout
 *
 *	Common parent to the layout classes that do layout  ie CdrawWin and CdrawDlg at this time
 *		Note that a parentHist - parentStyle of do-not-use is a Don't Care here
 *  This is called for menu items to go through their children
 *********************************************************************************************/


int  CdrawLayout::FillSelf(ulong symID,menuHist_t parentHist,hCmenuTree* pMTre)// does it all
{	

if (m_symblID == 0x40aa )
{
	LOGIT(CLOG_LOG,L"");
}
	if ( symID == 0 && m_symblID == 0 )
	{
		TRACE(_T("layout.FillSelf ERROR: no symbol number.\n"));
		return 0;
	}
	else
	if (m_symblID == 0) // symID is not zero
	{
		 m_symblID = symID;
	}
#ifdef _DEBUG
	else
	if(symID != 0)// m_symblID has a value too
	{
		if (symID != m_symblID)
		{
			LOGIT(CLOG_LOG|CERR_LOG,L"ERROR: item 0x%04x trying to be filled as 0x%04x\n",
																			  m_symblID,symID);
			return 0;
		}
	}//else symID == 0 && m_symblID != 0
#endif
	// else m_symblID full
	/* stevev - 15nov06 - stop the tree at one of three */
	// note to self: class_history is filled for my children's derivation, not for mine
	//    a do-not-use at this level isa don't-care, MAIN level parent (a mds_menu)
	// **22mar07 CH2Parent** class_history converted to be the parent's draw-as. 
	//						 We must change to our draw-as before we pass it to our children.
	//                       
	if ( ( parentHist.inPopMenu || 
		  (parentHist.parentStyle == mds_window || 
		   parentHist.parentStyle == mds_dialog ||
		   parentHist.parentStyle == mds_table  || 
		   parentHist.parentStyle == mds_page   ||
		   parentHist.parentStyle == mds_group  ||
		   parentHist.parentStyle == mds_tab   )   ) && 
		 ( mdsType == mds_window || mdsType == mds_dialog || mdsType == mds_table ) )
	{// this will be a button, don't go down till executed
		m_bIsAbutton = TRUE;
		return 0;/*one of three:: do not to recurse down (page&group=illegal, menu=continue)*/
	}
	/* end stevev 15nov06 */


    if ( parentHist.parentStyle == mds_grid && mdsType == mds_bitenum)
    {
        // CdrawBitEnum behaves as a link/button on Grids.
        // Therefore, since m_pStartTree is NULL, we must return here,
        // POB - 5/6/2014
        m_bIsAbutton = TRUE;
        return 0;
    }

	hCmenu* pMenu = (hCmenu*)GetItem();

	if ( pMenu == NULL || 
		( pMenu->getIType() != iT_Menu       && 
		  pMenu->getIType() != iT_Collection &&
		  (          pMenu->getIType()     != iT_Variable || 
		   ((hCVar*)pMenu)->VariableType() != vT_BitEnumerated
		  )
		)  
	   )
	{// error
		LOGIT(CERR_LOG,L"ERROR: window.fillSelf ::> no menu item.\n");
		return 0;
	}

	/* now fill a menu-tree or use the passed-in one */
	class_history = parentHist;			// **22mar07 CH2Parent** ...was 2::>
																// copy all the bools passed in
	menuHist_t hist4MyChildren;				// **22mar07 CH2Parent**
	hist4MyChildren = parentHist;			// **22mar07 CH2Parent**
	hist4MyChildren.parentStyle = mdsType; // children to see my draw-as type
											// **22mar07 CH2Parent**

	if (mdsType == mds_dialog)				// **22mar07 CH2Parent**
	{										// **22mar07 CH2Parent**
		hist4MyChildren.dlgInPath   = true;	// **22mar07 CH2Parent** 

		if (parentHist.inMethDlg)// our top-level parent is the only one to set this
		{	
			hist4MyChildren.parentStyle = mds_methDialog; // **22mar07 CH2Parent**
		}
		// else - leave it mds_dialog		// **22mar07 CH2Parent**
	}
    else 		    // added paul, **06 Aug 2008
    if (mdsType == mds_window)
	{										
		hist4MyChildren.wndInPath   = true;	 
	}				// end added 6aug08
	else									// **22mar07 CH2Parent**
	if (mdsType == mds_menu)				// **22mar07 CH2Parent**
	{	
		hist4MyChildren.inPopMenu = true;  // **22mar07 CH2Parent**
	}


	// **22mar07 CH2Parent**class_history.parentStyle=mdsType;//children to see my draw-as type
	// stevev 22may13 - keep the tree...hCmenuTree* pMenuTree = NULL;
	bool weOwnTree = FALSE;
//x	if (m_pMenuTree == NULL )// should only be null in window,dialog or table
	if (m_bIsAbutton || m_bIsAroot || mdsType == mds_dialog // stevev 12feb15 expanded to cover a window too   ) 
		|| mdsType == mds_window)// didn't work ... || mdsType == mds_page )
	{	//then make it	// if we got here it needs to be filled (not on a menu)-from execute
	
		ASSERT(pMTre == NULL);// we should be starting anew
		ASSERT(mdsType == mds_window|| mdsType==mds_dialog     || mdsType == mds_table ||
			   ( (!parentHist.inPopMenu) && mdsType==mds_menu) ||/*generate pop-up */
			   (m_bIsAroot && mdsType==mds_menu));	/* main's first fill */

//ASSERT(parentHist.parentStyle == mds_do_not_use);/// sjv test - it should be empty...
		m_pStartTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
		weOwnTree = true; // get it deleted		
		m_pStartTree->setDrawAs(mdsType);// 21mar07 - pass in the draw-as so histories can change
		LOGIT(CLOG_LOG,">>>>>> starting tree arrangement {%s}<<<<<<\n",
																	menuStyleStrings[mdsType]);

		// 04jun07 - menues defaulting to page on a window----change
		// if ( ((hCmenu*)pMenu)->aquireTree(pMenu, pMenuTree, parentHist) )
		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, m_pStartTree, hist4MyChildren) )
		{
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Tree shows size changed\n");	
			// if the start tree is empty. this will all be a change
		}
		else
		{
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Tree shows no change\n");
		}

//sjv 03jan07		m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->hv_size);
		//m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->da_size);
		m_MyLoc = *m_pStartTree;// stevev 28oct10 - use leafData
		LOGIT(CLOG_LOG,">>>>>> ended tree arrangement  w/%d items <<<<<<\n",m_pStartTree->branchList.size());
				
// try 
		if (m_pHoldTree == NULL)
		{
			m_pHoldTree = new hCmenuTree(*m_pStartTree);
		}
		else
		{
			(*m_pHoldTree) = (*m_pStartTree);
		}
		if (m_pHoldTree->pItemBase)
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"StartTree Held as item 0x%04x\n",m_pHoldTree->pItemBase->getID());
		else
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"StartTree Held but item-base is null\n");
// end try
		m_pStartTree->setCanvas();
	}
/* painted the right stuff in the wrong place
	else // try to deal with a page redraw here
	if (mdsType == mds_page)
	{	//then make it	// if we got here it needs to be filled (not on a menu)-from execute
	
		if(pMTre != NULL) // a recursion
		{
			m_pStartTree = pMTre;
		}
		else
		if (m_pStartTree == NULL)
		{
			m_pStartTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
			weOwnTree = true; // get it deleted		
		}// else use what we were given
		m_pStartTree->setDrawAs(mdsType);// 21mar07 - pass in the draw-as so histories can change
		LOGIT(CLOG_LOG,">>>>>> starting tree arrangement {%s} >>>>>>>\n",
																	menuStyleStrings[mdsType]);
		////////////////////////////////////////////////////////////////////////////////////////
		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, m_pStartTree, hist4MyChildren) )
		{
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Tree shows size changed.\n");	
			// if the start tree is empty. this will all be a change
		}
		else
		{
			LOGIF(LOGP_LAYOUT)(CLOG_LOG,"Tree shows no change.\n");
		}

//sjv 03jan07		m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->hv_size);
		//m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->da_size);
		m_MyLoc = *m_pStartTree;// stevev 28oct10 - use leafData
		LOGIT(CLOG_LOG,">>>>>> ended tree arrangement  w/%d items >>>>>>\n",m_pStartTree->branchList.size());
				
// try 
		if (m_pHoldTree == NULL)
		{
			m_pHoldTree = new hCmenuTree(*m_pStartTree);
		}
		else
		{
			(*m_pHoldTree) = (*m_pStartTree);
		}
		if (m_pHoldTree->pItemBase)
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"StartTree Held as item 0x%04x\n",m_pHoldTree->pItemBase->getID());
		else
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"StartTree Held but item-base is null\n");
// end try
		m_pStartTree->setCanvas();
	}
***/
	else
	if(pMTre != NULL) // a recursion
	{
		m_pStartTree = pMTre;
		weOwnTree = FALSE;// let the owner delete it
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"StartTree set to passed-in tree. len(%d)\n",m_pStartTree->branchList.size());
	}
	else
	{
		m_pStartTree = NULL;
		weOwnTree = FALSE;
		LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: Laying out a menu that doesn't exist.\n",m_pStartTree->branchList.size());
	}
		
		
//x	else // we are rebuilding - see if it has been aquired already
//x	if (m_pNewTree)
//x	{// it's already laid out, just replace it in the tree
//x		if ( m_pMenuTree->pParentTree == NULL)
//x		{
//x			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: No parent pointer in menu tree item.\n");
//x			//return 0;
//x			RAZE( m_pNewTree );
//x		}
//x		else
//x		{
//x			m_pMenuTree->pParentTree->replace(m_pMenuTree,/*with*/m_pNewTree);
//x			// done in replace...delete m_pMenuTree;
//x			m_pNewTree  = NULL;// signal reallocation is required.
//x		}
//x	}
//x	else 
//x	{/** assume it is a good tree ***
//x		ASSERT(mdsType == mds_window|| mdsType==mds_dialog  ||
//x			   mdsType == mds_table ||((!parentHist.inPopMenu)&&mdsType==mds_menu) ||
//x			   (m_bIsAroot&&mdsType==mds_menu));
//x
//x		// easyest way to clear the tree is to delete it
//x		delete m_pMenuTree;
//x		m_pMenuTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
//x				TRACE(_T(">>>>>> starting.tree.arrangement<<<<<<\n"));
//x				LOGIT(CLOG_LOG,">>>>>> starting.tree.arrangement <<<<<<\n");
//x		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, *m_pMenuTree, parentHist) )
//x		{
//x			TRACE(_T("Tree shows size changed\n"));	// if the start tree is empty. this will all be a change
//x		}
//x		else
//x		{
//x			TRACE(_T("Tree shows no change\n"));
//x		}
//x		m_MyLoc.setValue(m_pMenuTree->topleft,m_pMenuTree->hv_size);
//x				TRACE(_T(">>>>>> ended.tree.arrangement <<<<<<\n"));
//x				LOGIT(CLOG_LOG,">>>>>> ended.tree.arrangement  <<<<<<\n");
//x	 **** end assume 11dec06 ***/
//x	}
	ASSERT(m_pStartTree != 0);
#ifdef xx_DEBUG
	m_pStartTree->dumpSelf();
	LOGIT(CLOG_LOG,L"---------- finished Menu Tree Dump ------------\n\n");
#endif

	if ( m_pStartTree->branchList.size() <= 0 
		/*try*/ && m_pStartTree->getDrawAs() != mds_group)
	{//
		if (  (m_pStartTree->getDrawAs() == mds_window || 
			   m_pStartTree->getDrawAs() == mds_dialog || 
			   m_pStartTree->getDrawAs() == mds_menu   || 
			   m_pStartTree->getDrawAs() == mds_table    )
//sjv 03jan07			&&(pMenuTree->hv_size.xval == 1 && pMenuTree->hv_size.yval == 1)  )
			&&(m_pStartTree->da_size.xval == 1 && m_pStartTree->da_size.yval == 1)  )
		{// we are but a button upon the @$$ of another
// temp2allow translation always			
//			m_pMenuTree->setDrawAs(mds_parent); //paul's psuedonym for button!?!?
			if ( weOwnTree )
			{
				if ( !m_bIsAbutton )
					LOGIT(CLOG_LOG,L"Odd menu structure.\n");
				delete m_pStartTree;
				m_pStartTree = NULL;// force it to recalc tree @ execute
			}
		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: programmer: No menu tree.\n");
		}
		return 0;
	}
	if ( m_DrawList.size() >0 )
	{
		LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: Layout build has items in its draw list already.\n");
	}


#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,L"    Menu 0x%04x  ", m_symblID);
	if (m_pItem)
	{
		LOGIT(CLOG_LOG,"(%s)\n", m_pItem->getName().c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"\n");
	}
 #endif
#endif

	tree2List(m_pStartTree, hist4MyChildren); // populate the DrawList from the tree

#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"    --------end menu list----\n");
 #endif
#endif
	if ( weOwnTree )
	{
		delete m_pStartTree;
		m_pStartTree = NULL;
	}

	return ( m_DrawList.size() );
}

/*********************************************************************************************
 * a former piece of code from FillSelf above
 * broke it out to make the function smaller and to
 * possibly use it in Rebuild
 *
 ********************************************************************************************/
int  CdrawLayout::tree2List(hCmenuTree* ptree, menuHist_t& childrenHist)
{
	if (ptree ==  NULL)
		return FAILURE;

	
	ulong          uCID = 0;
	ulong		   LastTabCtrl = 0; 
	int            i;

	menuItemDrawingStyles_t mds;    
	menuTrePtrLstIT_t iT;
	hv_point_t        ltPages;
	hCmenuTree*       p_menuTre;
	
	/* we need to populate our drawlist to matchup with the branchlist */
	for (iT = ptree->branchList.begin(); iT < ptree->branchList.end(); iT++)
	{// isa ptr2aptr2a hCmenuTree
		p_menuTre = (*iT);
		mds = p_menuTre->getDrawAs(); 
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,"        Drawing item as  %s ",menuStyleStrings[mds]);
 #endif
#endif		
		if (mds == mds_page )       
		{	
			if (mdsType == mds_menu)
			{
				LOGIT(CERR_LOG|CLOG_LOG,"ERROR:Layout got a page on a popup-menu.\n");
			}
			CdrawTab* pDT = NULL;     // we are not already in a tab
			if ( LastTabCtrl == 0 || !(p_menuTre->topleft == ltPages))	
			{						// or the tap starts at a new location on the page
				ltPages = p_menuTre->topleft;
				i = m_DrawList.size();// the next index
				pDT        = new CdrawTab(this);
				LastTabCtrl= pDT->m_cntrlID;
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG," [We have a first page, making tab] \n");
 #endif
#endif		
				if (pDT != NULL && LastTabCtrl != 0 )
				{
//sjv 03jan07
//pDT->m_MyLoc.setValue((*iT)->topleft,(*iT)->hv_size);//should already have done a max
//pDT->m_MyLoc.setValue(p_menuTre->topleft,(*iT)->da_size);//should already have done a max
					pDT->m_MyLoc = *p_menuTre;// this branch
				  //pP->m_pMenuTree not filled - psuedo-class has no tree entry
					pDT->class_history = p_menuTre->getHistory();
					m_DrawList.push_back(pDT);
					m_CID2DrawMap[LastTabCtrl] = i;// the index we just inserted
				  //m_SID2DrawMap not filled with psuedo-drawclass (no symbol#)
				}
				else // invalid or failure
				{
					TRACE(_T("ERROR: Tab creation failed.\n"));
					LOGIT(CLOG_LOG,"Initial Tab creation failed -page skipped\n");
					RAZE(pDT);
					continue; // the for loop
				}
			}
			else
			{// we have a tab running
				pDT = (CdrawTab*) m_DrawList[m_CID2DrawMap[LastTabCtrl]];// it better exist
			}
			// insert page
			uCID = pDT->AddPage( p_menuTre );
		}
		else
		if (mds != mds_tab && mds != mds_parent && mds != mds_do_not_use)//&& != mds_page )
		{// not an illegal-unusable type
			LastTabCtrl = 0;  // we are no longer in a tab sequence
			
			CdrawBase* pP = newDraw(mds);
if (mds == mds_bitenum)
	LOGIT(CLOG_LOG,L" ben ");// bit e num
if (mds == mds_bit)
	LOGIT(CLOG_LOG,L" enb ");// e num's bit
			if (pP)
			{
				//pP->m_pMenuTree   = (hCmenuTree*)(*iT);	// will point into the main tree
//sjv 03jan07	pP->m_MyLoc.setValue((*iT)->topleft,(*iT)->hv_size);
//sjv 28oct10   pP->m_MyLoc.setValue(p_menuTre->topleft,(*iT)->da_size);
				pP->m_MyLoc = *p_menuTre;// as leafData
				pP->class_history = p_menuTre->getHistory();
				if (  mds == mds_separator                || //order of tests are IMPORTANT!
					  mds == mds_newline                  || // first two have no ref
					     pP->FillSymb(p_menuTre->getRef()) > 0|| //  not zero at a valid ref
					  mds == mds_constString    ||            //) FillSymb would give 0 for this 
					  mds == mds_blank                        )// FillSymb would give 0 for this 
				{		
#ifdef _DEBUG
	if (pP->m_pItem && pP->m_pItem->IsVariable() )
		if 	( ((hCVar*) pP->m_pItem)->VariableType() == TYPE_INDEX && pP->mdsType != mds_index)
			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: index draw-as is not set to mds_index.\n");
 #ifdef STARTSEQ_LOG
					if (pP->m_pItem)
						LOGIT(CLOG_LOG," ( %s ) -    ",pP->m_pItem->getName().c_str());
					if ( mds < mds_var ) // a menu
						LOGIT(CLOG_LOG,"\n");// FillSelf Below will statrt a new list
 #endif
#endif						
					COPY_QUALIFIER(p_menuTre,pP);// from,to
					if(p_menuTre->idx) pP->SetIndex(p_menuTre->idx);
// **22mar07 CH2Parent** pP->FillSelf(0    ,class_history,(hCmenuTree*)(*iT));//menuHist_t());
					pP->FillSelf(0    ,childrenHist,p_menuTre);//menuHist_t());	// recurse
					i = m_DrawList.size();  // next index
					m_DrawList.push_back(pP);
					m_CID2DrawMap[pP->m_cntrlID] = i;
					m_SID2DrawMap[pP->m_symblID] = i;
				}
				else
				{
					delete pP;
					LOGIT(CERR_LOG|CLOG_LOG,
						L"ERROR: CdrawLayout could not resolve menu item reference.\n");
				}
			}// had a grid test in here in orig code
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,"ERROR: could not generate a new Draw item.(%s)\n",
																		menuStyleStrings[mds]);
			}
		}
		else // an illegal-unusable type
		{
			LOGIT(CLOG_LOG,"ERROR: Got an unusable type (%s) in the branch list.\n",
																		menuStyleStrings[mds]);
		}
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"\n");
 #endif
#endif	
	}// next menu item
	return SUCCESS;
}

/**********************************************************************************************
 * Chk_Self
 *
 *	Does fillSelf without changing self - to see if we need to rebuild
 *  Returns hasChanged - this tentative layout came out different than the
 *
 * NOTE This is a wrapper around a trial aquireTree. It is discarded when done.
 *	    We could optimize to re-use this layout but it is way too complex to do now (15dec06)
 *********************************************************************************************/


bool  CdrawLayout::Chk_Self(ulong symID,menuHist_t parentHist)
{	
if (m_symblID == 0x4775 )
{
	LOGIT(CLOG_LOG,L"");
}
	if ( symID == 0 && m_symblID == 0 )
	{
		TRACE(_T("layout.Chk_Self ERROR: no symbol number.\n"));
		return 0;
	}
	else
	if (m_symblID == 0) // symID is not zero
	{
		 m_symblID = symID;
	}
#ifdef _DEBUG
	else
	if(symID != 0)// m_symblID has a value too
	{
		if (symID != m_symblID)
		{
			LOGIT(CLOG_LOG|CERR_LOG,L"ERROR: item 0x%04x trying to be filled as 0x%04x\n",
																			  m_symblID,symID);
			return 0;
		}
	}//else symID == 0 && m_symblID != 0
#endif
	// else m_symblID full
	/* stevev - 15nov06 - stop the tree at one of three */
	// note to self: class_history is filled for my children's derivation, not for mine
	//    a do-not-use at this level isa don't-care, MAIN level parent (a mds_menu)
//x	if ( parentHist.inPopMenu  && 
//x		 ( mdsType == mds_window || mdsType == mds_dialog || mdsType == mds_table ) )
//x	{// we are a button - stop here
//x		return 0;/*one of three:: do not to recurse down (page&group=illegal, menu=continue)*/
//x	}
	/* end stevev 15nov06 */
	if ( m_bIsAbutton && m_DrawList.size() <= 0 )
	{// we are a button that has never been executed
		return 0;
	}



	bool                b = false;
	menuHist_t   holdHist = parentHist;
	hCmenuTree*     pTree = NULL;
	hCmenu*         pMenu = (hCmenu*)GetItem();

	if ( pMenu == NULL || 
		( pMenu->getIType() != iT_Menu       && 
		  pMenu->getIType() != iT_Collection &&
		 (pMenu->getIType() != iT_Variable || 
		  ((hCVar*)pMenu)->VariableType() != vT_BitEnumerated
		 )
		)  
	   )
	{// error
		LOGIT(CERR_LOG,L"ERROR: window.chk_Self ::> no menu item.\n");
		return 0;
	}

	class_history = parentHist;          // copy all the bools passed in
	class_history.parentStyle = mdsType; // children to see my draw-as type

//x	if(mdsType == mds_window || mdsType == mds_dialog || mdsType == mds_table ||/*have layout*/
//x		((!parentHist.inPopMenu)&&mdsType==mds_menu) || (m_bIsAroot&&mdsType==mds_menu)   )
//XX21may13 - try again...
//XX	if(mdsType == mds_window || mdsType == mds_dialog || mdsType == mds_table )/* have layout*/
		if(mdsType < mds_menu )/* have layout - all but menu and table */
	{	//then make it
//x        if (m_pMenuTree != NULL && m_pMenuTree->branchList.size() > 0)
//x		{
//x		pTestTree = (hCmenuTree*) new hCmenuTree(*m_pMenuTree);//was::> (pMenu->devHndl());	
//x		}
//x		else
//x		if((m_pMenuTree  != NULL && m_pMenuTree->branchList.size() == 0)&&
//x		   (m_pStartTree != NULL && m_pStartTree->branchList.size() > 0))
//x		{
//x		pTestTree = (hCmenuTree*) new hCmenuTree(*m_pStartTree);//was::> (pMenu->devHndl());
//x		}
//x		else
//x		{// should never get here
//x			LOGIT(CERR_LOG,"ERROR: programmer - got to check the menu before it exists.\n");
//x		}			
		pTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
		pTree->setDrawAs(mdsType); // the only way to pass draw-as to self (pMenu)
		// stevev 06aug10 - pass loc & size so there is something to compare to
//sjv 28oct10		pTree->topleft = m_MyLoc.topLeftPt;
//sjv 28oct10		pTree->hv_size = m_MyLoc.rectSize;
		*pTree = m_MyLoc;//sjv 28oct10

#ifdef _DEBUG
		((hCmenu*)pMenu)->acquireEntryCnt = 0;
#endif
				LOGIT(CLOG_LOG,L">>>>>> starting check arrangement <<<<<<\n");
//22may13 - try to get an actual change...		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, pTree, 
//29apr14 - try the held tree    if ( ((hCmenu*)pMenu)->aquireTree(pMenu, m_pStartTree, parentHist) )
		RAZE( pTree );
		if (m_pHoldTree)
			pTree = m_pHoldTree;
		else
			pTree = m_pStartTree;

		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, pTree, parentHist) )
		{// chk_self is always the topmost level - ie it matches the symbolID
				LOGIT(CLOG_LOG,L">>>>>>> Tree shows size changed <<<<<<<\n");
/*30apr14 - reinstated to try again*/			b = true;
			//  b = false;// almost always true because the size coming in has been tweaked to
			//  to have the right width and height of the biggest page. Size is all that matters
			// in the current comparision.
		}
		else
		{
				LOGIT(CLOG_LOG,L">>>>>> Tree shows no size change <<<<<<\n");
			b = false;
		}
		
//x		m_MyLoc.setValue(m_pMenuTree->topleft,m_pMenuTree->da_size);//stevev 07dec06 was::> 
//x																//   >>>m_pMenuTree->hv_size);
//x				LOGIT(CLOG_LOG,">>>>>> ended check arrangement  <<<<<<\n");
	}
	else
	{
		LOGIT(CLOG_LOG,"Menu with no layout. Checks to No-Change.(%s)\n",
																menuStyleStrings[mdsType]);
		b = false;
	}
	class_history = holdHist;  
				
	return b;
}


// for the arrange() technique of generating a menu (dev-obj based)
// for items that have children ( ie menues )
// must have a menutree
void CdrawLayout::createMenu(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
#if 0 // traverse the tree
	hCmenuTree* ptree = NULL;
	menuTreLstIT_t it;

	for ( it = m_pMenuTree->branchList.begin(); it != m_pMenuTree->branchList.end(); ++ it) 
	{// for each subtree
		// isa ptr 2a hCmenuTree
		ptree = (hCmenuTree*)it;
		// convert type if it needs to be a button
		if ( m_pMenuTree->getDrawAs() < mds_var ) // we are a menu
		{						 
			if ( ptree->getDrawAs() == mds_window || 
				 ptree->getDrawAs() == mds_table  || 
				 ptree->getDrawAs() == mds_dialog    )
			{// this child is one of three
				ptree->setDrawAs(mds_parent);// make a button
			}
			/***/			
		ptree->m_nPage = m_nPage;// this may never be used (ie, no Page) by the control itself 
		ptree->m_pDynTabCtrl = m_pDynTabCtrl;
		ptree->createControl(pWnd, topLeftParent);
			/***/
		}

	}// next tree branch
#else // traverse the drawlist
	CdrawBase::createMenu(pWnd, topLeftParent, centeringOffset);
#endif
}


void CdrawLayout::staleDynamics(void)
{	// we are a menu type and cannot be dynamic-do children
	if (rebuilding)
	{
		return; // we be busy, go somewhere else
	}
	for ( DrawList_t::iterator i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{// ptr2ptr2 CdrawBase
		(*i)->staleDynamics();
	}
}


/* sjv 04dec06 resize support */
// returns true if size/existance changed
bool CdrawLayout::Update(long type, long SymID)
{
	CdrawBase * pBase;
	bool z = false;

	if (SymID >= 0 && SymID < 0x10 )
	{
		LOGIT(CERR_LOG,L"ERROR: parameter incorrect to update-layout.(0x%04x)\n",SymID);
		return false;// no re-layout, nothing happened
	}

	if ( SymID == m_symblID )
	{// structure, add-device, remove-device  and  // it's for us (a menu)
//x		if ( (m_pMenuTree != NULL && m_pMenuTree->branchList.size() > 0)     ||
//x			 ( (m_pMenuTree != NULL && m_pMenuTree->branchList.size() == 0)&&
//x			   (m_pStartTree != NULL && m_pStartTree->branchList.size() > 0)
//x			 )
//x			)
//x		{// only legal uses
//x			LOGIT(CLOG_LOG,"");// no op
//x		}
//x		else
//x		{
//x			// has never been executed, no reason to update
//x			LOGIT(CLOG_LOG,"Update before execute of a menu.\n");
//x			return FALSE;
//x		}// else its time  to work
#ifdef _DEBUG
		if ( type == UPDATE_ONE_VALUE)// should never happen
		{
			LOGIT(CERR_LOG,L"ERROR: Layout received menu notification for a value change.\n");
			return false;
		}
#endif
		DEBUGLOG(CLOG_LOG,L"              Match 0x%04x  -layout-\n",SymID);
		if (rebuilding)
			return false;

		z = Chk_Self(SymID,menuHist_t(class_history));//04dec06 - resize check(static)
		// if size change,    return true
		// else rebuild self, return false
		if ( ! z )
		{
			LOGIT(CLOG_LOG,L"    Checked as NO change.\n");// we don't change size
			// rebuild this menu, return z (false)- we have been told to rebuild (SymID)
			// stevev 13feb15 - untill we optimize and are able to build a small piece
			//			of a branch, we will just act like size changed when we are told 
			//			to update (ie SymID== ours) and force a parent to rebuild.
			//Rebuild(m_symblID,NULL);
			//z = false;
			z = true;
		}
		else
		{
			LOGIT(CLOG_LOG,L"    Checked as HAS change.\n");
			//  return z (true), our parent builds us
		}
	}
	else // not a match, SymID is -1 or its for sombody else
	{		// Iterate through child list
		bool anychg = false, anybuild = false; // size change ,  struct change
		DrawList_t::iterator i;
		for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{	// Look for this control id in its children
			pBase = (CdrawBase *)(*i);
			// Call the base class on this child object to search on it and/or its children
			// stevev 02sept08 - some types don't update...
			if ( pBase->mdsType != mds_separator	&&
				 pBase->mdsType != mds_newline		&&
				 pBase->mdsType != mds_constString  && 
				 pBase->mdsType != mds_blank		&& // stevev 13feb15
				 pBase->mdsType != mds_method		&& //stevev ?????????????
				 pBase->mdsType != mds_edDisp		 //stevev ?????????????
				 )
			{
			anychg   |= pBase->Update(type, SymID);
			anybuild |= (pBase->m_symblID== SymID);
			}
			//else	anychg |= false;
		}

		if (mdsType == mds_tab)
		{// if I'm a tab and any page changed size, I need my parent to rebuild me (win|dlg)			
			if ( anychg )
			{
				return true;
			}
			else //  no size change, check for structure change
			if(anybuild)
			{// we had a structure change
				for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
				{	// Look for this control id in its children
					pBase = (CdrawBase *)(*i);
					if (pBase->m_symblID == SymID)
					{// a tab's child must be a page
						((CdrawPage*)pBase)->Rebuild(SymID,NULL);/// we will return false, no size change
					}// else no diffs, fall through
				}// next
			}// else nothing has happened here, fall through to exit
		}// else continue

		if ( anychg )
		{// a child has changed size
			z = Chk_Self(m_symblID,menuHist_t(class_history));//22jan15 if a child changed, see if we did
			DEBUGLOGIF(LOGP_LAYOUT)(CLOG_LOG,"Layout::Update has checked.\n");
			if ( z )// our child changed size and that made us change size so make our parent rebuild 
			{
				LOGIT(CLOG_LOG,L"    Checked as HAS changed in this Layout.\n");
				z = true;
				//  return z (true), our parent builds us
			}
			else
			{//  our child changed size but we didn't change 

				/* this works great if you have a rebuild that will properly build a piece
				   of the menu layout.... we don't right now so I'm going to skip this 
				   for pages.  stevev 04mar15 ****/
				if (mdsType != mds_page)
				{
				//rebuild self and return false
				LOGIT(CLOG_LOG,L"    Checked as NO change in this Layout.\n");
				// rebuild this menu, return z (false)
				Rebuild(m_symblID,NULL);
				z = false;
				}
				else
				{
					z = true;// we couldn't rebuild the page so make our parent rebuild
				}
			}
		}
		else
		{
			z = false;
		}
	}


	return z;
}


void CdrawLayout::setProtection(long SymID,bool makeRebuilding)
{
	CdrawBase * pBase;
	if ( SymID == m_symblID )
	{
		rebuilding = makeRebuilding;
		// no need to go down, messages should not go beyond this point
	}	
	else // not a match, SymID is -1 or its for sombody else
	{		// Iterate through child list
		DrawList_t::iterator i;
		for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{	// Look for this control id in its children
			pBase = (CdrawBase *)(*i);
			
			pBase->setProtection(SymID, makeRebuilding);
		}
	}
}

bool CdrawLayout::Rebuild(long SymID, CWnd *m_pWindow)
{
	LOGIT(CLOG_LOG,"      Rebuild CdrawLayout - ERROR ERROR.\n");
	return false;
}
/**********************************************************************************************
 * CdrawWin  implementation
 *
 *	The draw class responsible for layout of the Window menu style                  
 *
 *********************************************************************************************/
 
// Not used for dialog...

CdrawWin::~CdrawWin()
{
	// Destroy all the controls that live in this window
//	Destroy(m_symblID);

	if(m_pWindow && ::IsWindow(m_pWindow->m_hWnd))
	{
		((CMDIChildWnd*)m_pWindow)->DestroyWindow();
	}
//	RAZE(m_pMenuTree);
}



void CdrawWin::FillSize(void)	
{
	// This is a menu of style WINDOW, but
	// if it lives on a menu of style DIALOG, PAGE, GROUP, WINDOW, etc
	// it needs to be a button
//	m_MyLoc.rectSize.xval = m_MyLoc.rectSize.yval = 1;
	m_MyLoc.da_size.xval = m_MyLoc.da_size.yval = 1;
}


void CdrawWin::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;

	// There will probably be a two controls for this draw type
	// (1) Parent button and (2) child frame

	// Create a parent button control if this item lives
	// on a window or dialog
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
	
	// There is a bug in the FillSelf() that builds the menu items
	// past (whether it is in a CMenu list or a Window or a Dialog)
	// the cdrawWin or cdrawDlg control if it lives in either in a
	// window or dialog.
	// FIX:  To be safe, destroy the entire list under the button
	// This will prevent a crash that can occur during an update
	Destroy(m_symblID);

	// Initialize the Label
	// parent control (ie, button)
	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
		}
	}

	m_nPage = 0;// stevev 08jun07 fix crash on a window on a page that is not the first page
	m_pDynTabCtrl = NULL; // stevev 31may2017 try this to keep the tab from going to zero on window button press

	// put back the original mds type
	mdsType = mds_window;
}

void CdrawWin::Execute(WORD wMessageID, LPARAM lParam)  // Added LPARAM parameter to support Grid notifications, POB - 5/5/2014
{
	CMainFrame * pMainWnd = GETMAINFRAME;
							// stevev 08oct10:was:>(CMainFrame *) AfxGetMainWnd();// msg target
	
	//Prevent muliple windows from any given object
	if (!m_pWindow)
	{
#ifdef RLSDBG
		LOGIT(CLOG_LOG,
		L"NOTE: CdrawWin::Execute 0x%x from thread 0x%04x.\n",wMessageID,GetCurrentThreadId());
#endif
		// Create child window (modeless)
		pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)this);
#ifdef RLSDBG
		LOGIT(CLOG_LOG
			,L"NOTE: CdrawWin::Posted message from thread 0x%04x.\n",GetCurrentThreadId());
#endif
	}
	else
	{
		if(::IsWindow(m_pWindow->m_hWnd))
		{
			CMDIChildWnd* pChildFrame = ( CMDIChildWnd* )m_pWindow;
			pChildFrame->MDIActivate();
		}
	}
}

bool CdrawWin::Rebuild(long SymID, CWnd *pWindow) 
{// we have a window & page number if rqrd, destroy, aquire, list, create
	// we are the window so pWindow is unused in this class
	bool r = false, c = false; // no change
	if ( rebuilding )
	{
		LOGIF(LOGP_LAYOUT)(CLOG_LOG,"CdrawWin::Rebuild called with rebuilding ON.\n");
		return false;
	}
	CdrawBase *  pBase;
	CdrawTab *  pTab;

	// Iterate through child list and get the active page, if there is one
	int activePage = -1;
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{	// Look for a tab in the window
		pBase = (CdrawBase *)(*i);
		if (pBase->mdsType == mds_tab && pBase->m_pDynCtrl != NULL)
		{// child is a tab and the window has been created before			
			pTab = dynamic_cast<CdrawTab*>(pBase);
			activePage = pTab->m_pDynCtrl->GetActiveTabPage();
		}
	}

//	Destroy(m_symblID);// destroy children

	if (m_pWindow)// we are the target (we're a window)
	{// for now, redo the whole thing	
        /*
         * Ensure m_pWindow is not NULL.  
         * CdrawMain::Update() calls CdrawWin::Rebuild() with m_pWindow = NULL 
         * thus causing the code below to crash, POB - 8/17/2014
         */
		CChildFrame * pChildFrame = (CChildFrame *)m_pWindow;
		
		if (::IsWindow(pChildFrame->m_hWnd))
		{
			CSDC625WindowView * pView =  (CSDC625WindowView *)pChildFrame->GetActiveView();
			BOOL hasVert = FALSE, hasHorz = FALSE;
			pView->CheckScrollBars(hasHorz, hasVert);
			CPoint startScrollPos = pView->GetScrollPosition( );// upper left corner position
			
			pView->ScrollToPosition(CPoint(0,0));// drawing expects this

			pView->Rebuild();

			pView->ScrollToPosition(startScrollPos);// upper left corner position

			for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
			{	// Look for a tab in the window
				pBase = (CdrawBase *)(*i);
				if (pBase->mdsType == mds_tab && pBase->m_pDynCtrl != NULL)
				{// child is a tab and the window has been created before			
					pTab = dynamic_cast<CdrawTab*>(pBase);
					pTab->m_pDynCtrl->SetActiveTabPage(activePage);
				}
			}
		}
	}
	return false;// nobody cares if a window resizes
}


/**********************************************************************************************
 * CdrawDlg  implementation
 *
 *	The draw class responsible for layout of the Dialog menu style                  
 *
 *********************************************************************************************/

// This is very similar to CdrawWin - May want the
// CdrawWin to be its base class
CdrawDlg::CdrawDlg(CdrawBase* pParent) : CdrawLayout(mds_dialog,pParent)
{
	m_pButtonTxt  =NULL;	/*hWnd=0;*/	
	m_exButtonCnt = 0;
	m_isReady     =0;
	m_pMnFrm      = NULL;
	m_bAbortBtnClicked    = m_bNextBtnClicked =  m_bButOneClicked = 
	m_bButTwoClicked      = m_bButThrClicked  =  m_bSingleMenuItem = 0;
}

// Copy Constructor
//DEL CdrawDlg::CdrawDlg(const CdrawDlg & drawDlg) : CdrawBase(mds_dialog)
//DEL {
//DEL 	m_pButtonTxt       = drawDlg.m_pButtonTxt;
//DEL 	m_exButtonCnt      = drawDlg.m_exButtonCnt;
//DEL 	m_isReady          = drawDlg.m_isReady;
//DEL 	
//DEL 	m_bAbortBtnClicked = drawDlg.m_bAbortBtnClicked;
//DEL 	m_bNextBtnClicked  = drawDlg.m_bNextBtnClicked;
//DEL 	m_bButOneClicked   = drawDlg.m_bButOneClicked,
//DEL 	m_bButTwoClicked   = drawDlg.m_bButTwoClicked;
//DEL 	m_bButThrClicked   = drawDlg.m_bButThrClicked;
//DEL }


CdrawDlg::~CdrawDlg()
{
	// Destroy all the controls that live in this window
	//	Destroy(m_symblID);
 
/*	if(m_pWindow)
	{
 		m_pWindow->DestroyWindow();
 	}*/
}

void CdrawDlg::FillSize(void)	
{
	// This is a menu of style DIALOG, but
	// if it lives on a menu of style DIALOG
	// or WINDOW, it needs to be a button
	m_MyLoc.da_size.xval = m_MyLoc.da_size.yval = 1;	
}

void CdrawDlg::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;

	// There will probably be a two controls for this draw type
	// (1) Parent button and (2) Dialog template


	// Create a parent button control if this item lives
	// on a window or dialog
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
	
	// There is a bug in the FillSelf() that builds the menu items
	// past (whether it is in a CMenu list or a Window or a Dialog)
	// the cdrawWin or cdrawDlg control if it lives in either in a
	// window or dialog.
	// FIX:  To be safe, destroy the entire list under the button
	// This will prevent a crash that can occur during an update
	Destroy(m_symblID);

	// Create a Dialog object to draw the controls on
	// NOTE:  This will probably be a CDynControls
	//        object that we call DoModal()

	// Initialize the Label for our parent control (ie, button)
	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
		}
	}

	// Put back the original mds type
	mdsType = mds_dialog;

	// Set then page number to zero for the dialog.  The dialog
	// may have a Tab control on it and we don't want to pass
	// this number to any Tab control that might be residing on
	// a dialog when it is in DoModal().  The Tab needs to 
	// always start out with its Page number set to zero since
	// it uses a zero index (ie, starts with zero.  A Problem
	// occurs when parent Dialog button lives on a Page and then
	// has a Page living on it when the dialog is instaniated.
	m_nPage = 0;

	m_pDynTabCtrl = NULL; // stevev 31may2017 try this to keep the tab from going to zero on dialog button press
}

/*  message is count of extra buttons in pButtonTxt */
void CdrawDlg::Execute(WORD wMessageID, LPARAM lParam)
{
	CMainFrame * pMainWnd = NULL;

	if (m_pMnFrm == NULL)
	{
// go back to dependable stevev 08oct10		pMainWnd = (CMainFrame *) AfxGetMainWnd();
												// copy of a CwndClass tied to the main window
		pMainWnd = GETMAINFRAME;// sent to new'd dyncontrol
	}
	else
	{
		pMainWnd = m_pMnFrm;
	}
	
	m_exButtonCnt = wMessageID;

	// There is a bug in the FillSelf() that builds the menu items
	// past (whether it is in a CMenu list or a Window or a Dialog)
	// the cdrawWin or cdrawDlg control if it lives in either in a
	// window or dialog.  If this is called through a menu item, the
	// FillSelf() will never be called, therefore we need to call it
	// below.  However, if a dialog lives in a window, FillSelf() will
	// be called twice.
	// FIX:  (OPTIONAL) To be safe, destroy the entire list and start again.
	Destroy(m_symblID);
	// dialogs become independent when executed - modify history to reflect this
	menuHist_t thisHistory;
	// **22mar07 CH2Parent** hldHistory = class_history;
	thisHistory.dlgInPath      = true;// **22mar07 CH2Parent** 
	thisHistory.grpDepthHit    = false;// **22mar07 CH2Parent** 
	thisHistory.inPopMenu      = false;// **22mar07 CH2Parent** 
	thisHistory.inReview       = false;// added stevev 31aug07 - this will have to passed
	thisHistory.parentStyle    = mds_do_not_use;// **22mar07 CH2Parent** my parent is nothing
	if (wMessageID)
	{
		thisHistory.inMethDlg  = true;// **22mar07 CH2Parent** 
		// **22mar07 CH2Parent** class_history.parentStyle= mds_methDialog;	// more restrictive
	}
	else
	{
		thisHistory.inMethDlg  = false;
		// **22mar07 CH2Parent** class_history.parentStyle= mds_dialog;		// than this one
	}

    if (m_bSingleMenuItem)
    {
        thisHistory.expTblDlg = true;  // Ignore Layout rules - start in 1st column, POB - 3/30/2014
    }
    else
    {
        thisHistory.expTblDlg = false;  // Layout is normal 3 column minimum canvas, POB - 3/30/2014
    }

	// **22mar07 CH2Parent** FillSelf(m_symblID,menuHist_t(class_history),NULL);
												// stevev 28feb07-we filled it,should use it
	FillSelf(m_symblID,thisHistory,NULL);// **22mar07 CH2Parent** 

#ifndef ARRANGE_IT
	menuSize();
#endif

	/////////////// Template design///////////////////////////
	pnt_t temp = {1,1};

	CRect Rect;

	//02aug12-stevev: mul is number of columns to add to the x start to center this item
	double mul    = ((double)m_MyLoc.canvasSize-(double)m_MyLoc.rowSize)/2;	
	//double mul = (double)m_MyLoc.canvasSize/(double)m_MyLoc.rowSize;

	CDynControls DialogTemplate(temp, mul, pMainWnd); // dialog built on the stack

	DialogTemplate.m_pdrawDlg = this;//&drawDlg; //this;

	// Use the CRect "bottom" member for height and the "right" member for width
	// so the dialog calculations are consistent with that of the controls
	DialogTemplate.SetCtrlRect(Rect, this /*&drawDlg*/ /*this*/,0);// assume zero offset 4 now

	int height = Rect.bottom;
	int width  = Rect.right;
	
    //Adding the width of the vertical scoll var is not needed here - removed code, POB - 3/30/2014
	//Update dialogtemplate boundaries 
	DialogTemplate.m_DialogTemplate.x = 0;
	DialogTemplate.m_DialogTemplate.y = 0;
	DialogTemplate.m_DialogTemplate.cx = width;
	DialogTemplate.m_DialogTemplate.cy = height;
	DialogTemplate.m_DialogTemplate.style = 
		WS_VISIBLE | WS_CAPTION /*| WS_VSCROLL | WS_HSCROLL*/ | WS_SYSMENU;// | WS_THICKFRAME;
	
	// stevev- trying to resolve an array bounds error::> 	int button = DialogTemplate.DoModal();
	// this doesn't work...see below     int button = DialogTemplate.sdcModl();
	/* 
     * Modal dialog
     * It is mandatory that we use DialogTemplate.DoModal() here instead of sdcModl()
     * DialogTemplate.DoModal() calls CDynControls::BuildDlgTemplate() which creates the
     * necessary dialog resources template and then calls sdcModl() to display the dialog.
     * Calling DialogTemplate.sdcModl() directly will fail because the parameter
     * m_lpDialogTemplate will be NULL and the function will return without generating the dialog.
     */
    // TO DO:  It appears that the unresolved array bound errors mentioned above appears to exist
    // in the BuildDlgTemplate() prior to sdcModl(), POB - 5/23/2014
	int button = DialogTemplate.DoModal(); 
	// hWnd already gone...DialogTemplate.SendMessage(WM_CLOSE);

	// We don't want to have any children living under this dialog
	// destroy them on the way out
	Destroy(m_symblID);
	// **22mar07 CH2Parent** class_history = hldHistory;

	if (pMainWnd->m_pDoc && m_exButtonCnt == 0)// have a doc and we're NOT displayMenu
	{
		// A symbol number zero will produce an "update all"
		ulong symbol = 0;
//		pMainWnd->m_pDoc->UpdateAllViews( NULL, UPDATE_WINDOW_ITEM, (CObject *)symbol );
	}

	if (m_exButtonCnt > 0)
	{
		TRACE(_T(">>>>>> DisplayMenu drawDlg finished DoModal.\n"));
	}
}

bool CdrawDlg::Rebuild(long SymID, CWnd * m_pWindow)
{
	LOGIT(CLOG_LOG,"      Rebuild CdrawDlg\n");
	return false;
}
/**********************************************************************************************
 * CdrawTab  implementation
 *
 *	The draw class responsible for layout of the Page menu style                  
 *
 *********************************************************************************************/


void CdrawTab::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CdrawBase::createControl(pWnd, topLeftParent);// can't have a centering offset

	// Get origin converted
	topLeftParent.xval += (m_MyLoc.topleft.xval -1);
	topLeftParent.yval += (m_MyLoc.topleft.yval -1);

	// Add group label size offset
	topLeftParent.yval += TAB_HGT_ROWS;

	// Now create menus for its Pages

	//createMenu(pWnd, topLeftParent);
	CdrawBase * pBase;
	
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Now create the controls on its children
		// This must be a page since it lives in a Tab
		pBase = (*i);

		// This must be a CdrawPage since it lives in a CdrawTab
		// Allow it access to the tab control id
		// NOTE: The page needs the control ID of its parent (ie,
		//       this) in order to access Help using HitTest()
		pBase->m_cntrlID  = m_cntrlID;
//		pBase->m_pDynCtrl = m_pDynCtrl;
		
		/*
		 * Set the "Tab" Dynamic Control to this page control
		 * All objects buried in the depths of its child pages
		 * will have access to this tab control (it its
		 * not NULL)
		 */
		pBase->m_pDynTabCtrl = m_pDynCtrl;
		
		// Set its page number (zero index)
		pBase->m_nPage    = m_nPage;

		pBase->createControl(pWnd, topLeftParent);// we can't have an offset

		// increment page number for any other page
		m_nPage++;
	}
	///////////// show the first one now /////////////////
	m_pDynCtrl->setFirstPage();
}

/* this is a tab-control-only method */
RETURNCODE  CdrawTab::AddPage(hCmenuItem& mi)
{
	RETURNCODE rc = SUCCESS;
	ITEM_ID   iID = 0; 
	int         i = -1;
	ASSERT(mi.getDrawAs()==mds_page);

	CdrawBase* pP = newDraw(mi.getDrawAs());

	if (pP)
	{
		pP->class_history = mi.getHistory();
		if ( pP->FillSymb(mi.getRef()) > 0 )
		{
			COPY_QUALIFIER( (&mi),pP);// from,to
// **22mar07 CH2Parent** pP->FillSelf(0 ,menuHist_t(),NULL);// all pages start at the beginning
			pP->FillSelf(0,menuHist_t(pP->class_history),NULL);//  pages start at the beginning
			i = m_DrawList.size(); // next index
			m_DrawList.push_back(pP);
			m_CID2DrawMap[pP->m_cntrlID] = i;
			m_SID2DrawMap[pP->m_symblID] = i;
		}
		else
		{
			delete pP;
			TRACE(_T("ERROR: could not resolve page reference.\n"));
		}
	}
	else
	{
		rc = FAILURE;
	}
	return rc;	
}


RETURNCODE  CdrawTab::AddPage(hCmenuTree* pmt)
{
	RETURNCODE rc = SUCCESS;
	ITEM_ID   iID = 0; 
	int         i = -1;
	ASSERT(pmt->getDrawAs()==mds_page);

	CdrawBase* pP = newDraw(mds_page);

	if (pP)
	{
//x		pP->m_pMenuTree   = pmt;			  // will point into the main tree
//sjv 03jan07		pP->m_MyLoc.setValue(pmt->topleft,pmt->hv_size);
//sjv 28oct10		pP->m_MyLoc.setValue(pmt->topleft,pmt->da_size);
		pP->m_MyLoc = *pmt;
		pP->class_history = pmt->getHistory();// now duplicate data
		if ( pP->FillSymb(pmt->getRef()) > 0 )
		{
			COPY_QUALIFIER(pmt,pP);// from,to
// **22mar07 CH2Parent** pP->FillSelf(0,menuHist_t(), pmt);//0 means use the internal symbol # 
		pP->FillSelf(0,menuHist_t(pP->class_history),pmt);//0 means use the internal symbol # 
			i = m_DrawList.size(); // next index
			m_DrawList.push_back(pP);
			m_CID2DrawMap[pP->m_cntrlID] = i;
			m_SID2DrawMap[pP->m_symblID] = i;
		}
		else
		{
			delete pP;
			TRACE(_T("ERROR: could not resolve page reference.\n"));
		}
	}
	else
	{
		rc = FAILURE;
	}
	return rc;	
}

void CdrawTab::FillSize(void)	
{// max size of all pages
	DDSizeLocType maxColsWide  = 0;
	DDSizeLocType maxRows_Tall = 0;

	DrawListIterator_t i;
	// for each page
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{// ptr2aptr2a CdrawBase...fill the page
		CdrawBase* pDb = *i;
		pDb->FillSize();
		// take the max size
		if (pDb->m_MyLoc.da_size.xval > maxColsWide)
		{
			maxColsWide = pDb->m_MyLoc.da_size.xval;
		}
		if (pDb->m_MyLoc.da_size.yval > maxRows_Tall)
		{
			maxRows_Tall = pDb->m_MyLoc.da_size.yval;
		}
	}

	m_MyLoc.da_size.xval = maxColsWide;
	m_MyLoc.da_size.yval = maxRows_Tall + TAB_HGT_ROWS;// add space for tabs
};

bool CdrawTab::Rebuild(long SymID, CWnd *pWindow)// returns size-changed
{	LOGIT(CLOG_LOG,"       We got into a Tab's Rebuild.\n");

	bool r = false, c = false; // no change
	
	//get parent window ptr
	if ( pWindow == NULL )
	{// this always works and is exactly the same as m_pDynCtrl->GetParent();
		CMainFrame * pMainWnd = (CMainFrame *)(AfxGetApp()->m_pMainWnd);
		CFrameWnd* pFrame = pMainWnd->GetActiveFrame();
		CView * pView = pFrame->GetActiveView();
		pWindow = dynamic_cast<CWnd*>(pView);
	}
	LOGIF(LOGP_DD_INFO)(CLOG_LOG,"Tab:Rebuild has a window.Destroying\n");
	//destroy self
	Destroy(m_symblID);//self
	LOGIF(LOGP_DD_INFO)(CLOG_LOG,"Tab:Rebuild has destroyed itself.Fill\n");
	if (m_pStartTree)
		m_pStartTree->destroy();
	//fillself
	FillSelf(SymID,class_history,m_pStartTree);
	LOGIF(LOGP_DD_INFO)(CLOG_LOG,"Tab:Rebuild has filled itself.Create\n");
	//createmenu
	pnt_t p = {1,1};
	createMenu(pWindow, p);
	LOGIF(LOGP_DD_INFO)(CLOG_LOG,"Tab:Rebuild has created a Menu.Exit\n");

	// if we are being asked to rebuild, something changed size
/*********************** first try ************************
	pnt_t tlp = {1,1};
	// Get origin converted
	tlp.xval += (m_MyLoc.topleft.xval -1);
	tlp.yval += (m_MyLoc.topleft.yval -1);

	// Add group label size offset
	tlp.yval += TAB_HGT_ROWS;

	if ( ! rebuilding )
	{// Iterate through child list
		DrawList_t::iterator i;
		for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
		{	// Look for this control id in its children
			pBase = (CdrawBase *)(*i);
			if (pBase->mdsType < mds_methDialog || pBase->mdsType == mds_tab)
			{			
				pPage = dynamic_cast<CdrawPage*>(pBase);
				if (pPage->rebuilding)// this is our target
				{
// rebuild page from here
					pPage->Destroy(pPage->m_symblID);
					pPage->tree2List(pPage->m_pStartTree, pPage->class_history);

					pPage->createMenu(pWindow, tlp);// no centering offset allowed
					if ( pPage->m_pParentDraw && 
						 pPage->m_pParentDraw->m_pDynCtrl && 
						 pPage->m_pParentDraw->m_pDynCtrl->GetTabWnd() != 0 )
					{
						pPage->m_pParentDraw->m_pDynCtrl->GetTabWnd()->SetCurSel(-1);
					}
					pWindow->Invalidate();
				}
				else
				{// go on down
					c = c || pPage->Rebuild(SymID, NULL);// pages
				}
			}// you can't rebuild primatives
		}
	}//else  no need to go down, this is the bottom

	if (rebuilding || c)// we are the target or a child resized
	{// we have to resize all the pages to the new canvas & return true	for window resize
		LOGIT(CLOG_LOG,"We are the target or a child resized.\n");
	}
	******************************************************end first try ***/
	return false;// nobody cares if a window resizes
};
/**********************************************************************************************
 * CdrawPage  implementation
 *
 *	The draw class is responsible for layout of the Page menu style                  
 *
 *********************************************************************************************/


CdrawPage::~CdrawPage()
{
	if (m_pDynTabCtrl)
	{
		m_pDynTabCtrl->RemoveTabPage(m_nPage);
	}
}

//int CdrawPage::FillSelf(ulong selfSymID)	
//{
//	if ( selfSymID == 0 )
//	{
//		if ( m_symblID == 0 )
//		{
//			TRACE(_T("page.fillself ERROR: not enough info.\n"));
//			return 0;
//		}
//		else
//		{
//			selfSymID = m_symblID;
//		}
//	}
//
//	m_symblID = selfSymID;
//
//	return (fillList("Page"));
//}

void CdrawPage::FillSize(void)	
{
	int y = menuSize();
}


void CdrawPage::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	//  There is actually no seperate control for a page.
	//  It is a manipulation of the tab control
	
	// Initialize this control
//	m_pDynCtrl->AddTabPage(GetLabel(), m_nPage);
	m_pDynTabCtrl->AddTabPage(GetLabel(), m_nPage);

	/* 
	 * All objects buried in the depths of its pages
	 * will have access to this tab control (it its
	 * not NULL)
	 * m_pDynTabCtrl = CdrawTab::m_pDynCtrl;
	 * NOTE:  m_pDynCtrl = NULL for all pages
	 */

#ifdef LOGBIRTH
	ldepth++;
#endif
	createMenu(pWnd, topLeftParent);// no centering offset allowed
#ifdef LOGBIRTH
	ldepth--;
#endif
}
// returns size-changed
bool CdrawPage::Rebuild(long SymID, CWnd *m_pWindow)
{	
	if (m_pWindow == NULL)
	{
		if ( m_DrawList.size() > 0 )
		{
			m_pWindow = m_DrawList[0]->m_pDynCtrl->GetParent();
			if ( m_pWindow == NULL )
			{// we can't draw anything
				LOGIT(CLOG_LOG,"Page's REBUILD called and can't get a draw window.\n");
				return false;// an error
			}// else we're ready to go
		}
		else
		{// else we don't care if we have a window if we have nothing to draw
			return false;// size didn't change
		}		
	}

	Destroy(m_symblID);//	destroy self
	if (m_pStartTree)
		m_pStartTree->destroy();
	FillSelf(m_symblID, class_history, m_pStartTree); //fillself
	pnt_t p = {0,0};// pages are always 0 w/ 0 offset
	createMenu(m_pWindow, p);// create menu self
	return false;
};
/**********************************************************************************************
 * CdrawGroup
 *
 *	The draw class is responsible for layout of the Group menu style  
 *
 *********************************************************************************************/


//int CdrawGroup::FillSelf(ulong selfSymID)	
//{
//	if ( selfSymID == 0 )
//	{
//		if ( m_symblID == 0 )
//		{
//			TRACE(_T("group.fillself ERROR: not enough info.\n"));
//			return 0;
//		}
//		else
//		{
//			selfSymID = m_symblID;
//		}
//	}
//
//	m_symblID = selfSymID;
//
//	return (fillList("Group"));
//}

void CdrawGroup::FillSize(void)	
{	
	int y = menuSize();
	m_MyLoc.da_size.yval += (GRP_HGT_ROWS); // add a row for the label
}


void CdrawGroup::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset ) 
{
	CWnd * ptr;

	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
	
	if ( m_pDynCtrl != NULL )
	{
		//myPixelOffset = m_pDynCtrl->xOffset();
		myPixelOffset = m_pDynCtrl->xOffset() + centeringOffset;
	}
	else
	{
		myPixelOffset = 0;
	}
	centeringOffset = myPixelOffset;
	// Initialize this control
	ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
	ptr->SetWindowText(GetLabel());

	// Get origin converted
	topLeftParent.xval += (m_MyLoc.topleft.xval -1);
	topLeftParent.yval += (m_MyLoc.topleft.yval -1);

	// Add group label size offset
	topLeftParent.yval += GRP_HGT_ROWS;
#ifdef LOGBIRTH
	ldepth++;
#endif
	// Now create its menu items
	createMenu(pWnd, topLeftParent, centeringOffset);
#ifdef LOGBIRTH
	ldepth--;
#endif
}

/**********************************************************************************************
 * CdrawMenu
 *
 *	The draw class is responsible for layout of the Menu menu style  (ie popup)
 *
 *********************************************************************************************/

// POB - 8 Sep 2004
#if 0 /* stevev 15nov06 - use base class' branch traversal ----------------------------------*/

int CdrawMenu::FillSelf(ulong selfSymID)	
{
	int dH = 0;

	if ( selfSymID == 0 )
	{
		if ( m_symblID == 0 )
		{
			TRACE(_T("group.fillself ERROR: not enough info.\n"));
			return 0;
		}
		else
		{
			selfSymID = m_symblID;
		}
	}
	m_symblID = selfSymID;

	// Fill List
	menuItemList_t localMenuList;
	int c = getList(m_symblID, localMenuList, class_history );

	if ( c <= 0 )
	{
		TRACE(_T("ERROR: nothing in fill self menu list.\n"));
		return c;
	}

	CdrawBase*     pcdb = NULL;
	ulong          uCID = 0;
	int            i;
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,L"    Menu 0x%04x  ", m_symblID);
	if (m_pItem)
	{
		LOGIT(CLOG_LOG,"(%s)\n", m_pItem->getName().c_str());
	}
	else
	{
		LOGIT(CLOG_LOG,"\n");
	}
#endif
#endif
	menuItemList_t::iterator iT;
	for (iT = localMenuList.begin(); iT < localMenuList.end(); iT++)
	{   //hCmenuItem     i;
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,"        Drawing item as  %s ",menuStyleStrings[iT->getDrawAs()+1]);
#endif
#endif
		CdrawBase* pP;
	//	if (iT->getDrawAs() == mds_menu )
		{
			pP = newDraw(iT->getDrawAs());
		}
	/*	else
		{
			pP = newDraw(mds_menu_entry);
		}*/

		// WORK TO DO:  Verify the if statements after we integrate
		if (pP)
		{	
//this is a menulist	pP->m_pMenuTree   = (hCmenuTree*)iT;   // will point into the main tree
			pP->class_history = iT->getHistory();
			if ( iT->getDrawAs() == mds_separator || // order of tests are IMPORTANT!
				 iT->getDrawAs() == mds_newline   ||
				 pP->FillSymb(iT->getRef()) > 0   || 
				 iT->getDrawAs() == mds_constString )
			{					
				COPY_QUALIFIER(iT,pP);// from,to
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
		if (pP->m_pItem)
		LOGIT(CLOG_LOG," ( %s ) -",pP->m_pItem->getName().c_str());
#endif
#endif	
				pP->FillSelf(0);
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
		if (pP->m_pItem)
		LOGIT(CLOG_LOG,L"-");
#endif
#endif	
				i = m_DrawList.size(); // next index
				m_DrawList.push_back(pP);
				m_CID2DrawMap[pP->m_cntrlID] = i;
				m_SID2DrawMap[pP->m_symblID] = i;
			}
			else
			{
				delete pP;
				TRACE(_T("Menu fill ERROR: could not resolve menu item reference.\n"));
			}
		}
		else
		{
			TRACE(_T("Menu fill ERROR: could not generate a new Draw item.\n"));
		}
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
#pragma message("In StartSeq")
		LOGIT(CLOG_LOG,L"\n");
#endif
#endif	
	}// next menu item
#ifdef _DEBUG
#ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"--------end menu list----\n");
#endif
#endif

	return ( m_DrawList.size() );
}
#endif // using base class---------------------------------------------------------------------

// added paul 13aug08 ***>
// This is is only used to fill the Menu Button.  The FillSelf() in base class will be used for
/// all other cdrawMenu objects
int CdrawMenu::FillButton()   
{
	// Here we will add each entry menu
	// to this drawlist (m_DrawList)
	UINT item = m_symblID;
	menuItemList_t vmi;
	hCmenu * pMenu;
	menuItemDrawingStyles_e drawStyle = mds_menu;  // Default style
	menuHist_t     local_history;
	//int i;

    hCmenuTree* pMTre = NULL;

	hCitemBase * p_myItem = NULL;

	CDDIdeDoc* pDoc = GetDocument();

	if (!pDoc)
	{
		return FALSE;
	}	

	if ( !pDoc->pCurDev )
	{
		return FALSE ; /// error -  no device!!!!!!!!!!!!
	}

	RETURNCODE rc = pDoc->pCurDev->getItemBySymNumber(item, &p_myItem);

	if ( rc != SUCCESS || p_myItem == NULL )
	{
		return 0; // error:  item I am supposed to be doesn't exist !!!!!!!!!!!!
	}

	if (p_myItem->getIType() != iT_Menu)
	{
		return 0; // error:  I am supposed to be a menu of type menu !!!!!!!!!!!!
	}

	pMenu = (hCmenu*)p_myItem;
	
	// The entry menu does not live on another menu
	// (ie, no parent menu) we need to set the default
	// style here
	// DEFAULT = TABLE
	switch(pMenu->getStyle())
	{
		case menuStyleWindow:
			drawStyle = mds_window;
			break;

		case menuStyleDialog:
			drawStyle = mds_dialog;
			break;

		case menuStylePage:  // NOT ALLOWED - translate to table
			break;

		case menuStyleGroup: // NOT ALLOWED - translate to table
			break;

		case menuStyleMenu:
			drawStyle = mds_menu;
			break;

		case menuStyleTable:  // The default is already Table
			drawStyle = mds_table; // set it anyway
			break;

		case menuStyleUnknown: // leave it the default style table
		case menuStyleNone:
		default: 
			break;

	}

	if (m_symblID == 0x40aa )
    {
	    LOGIT(CLOG_LOG,L"");
    }

	if (m_symblID == 0 )
	{
		TRACE(_T("layout.FillSelf ERROR: no symbol number.\n"));
		return 0;
	}

	m_bIsAbutton = TRUE;

	if ( pMenu == NULL || 
		( pMenu->getIType() != iT_Menu       && 
		  pMenu->getIType() != iT_Collection &&
		 (pMenu->getIType() != iT_Variable || 
		  ((hCVar*)pMenu)->VariableType() != vT_BitEnumerated
		 )
		)  
	   )
	{// error
		LOGIT(CERR_LOG,L"ERROR: window.fillSelf ::> no menu item.\n");
		return 0;
	}

    // This is not correct history in all situations, POB - 4 Auug 2008 - NEED TO DO!
    //Class history has already been created

	/* now fill a menu-tree or use the passed-in one */
	menuHist_t hist4MyChildren;				// **22mar07 CH2Parent**

    hist4MyChildren = class_history;			// **22mar07 CH2Parent**
	hist4MyChildren.parentStyle = mdsType; // children to see my draw-as type
											// **22mar07 CH2Parent**	
    if (mdsType == mds_dialog)				// **22mar07 CH2Parent**
	{										// **22mar07 CH2Parent**
		hist4MyChildren.dlgInPath   = true;	// **22mar07 CH2Parent** 
		
        if (class_history.inMethDlg)// our top-level parent is the only one to set this
		{	
            hist4MyChildren.parentStyle = mds_methDialog; 
        }// **22mar07 CH2Parent**
		// else - leave it mds_dialog		// **22mar07 CH2Parent**
	}
    else if (mdsType == mds_window)		    // **06 Aug 2008
	{										
		hist4MyChildren.wndInPath   = true;	 
	}
	else if (mdsType == mds_menu)			// **22mar07 CH2Parent**
	{	
        hist4MyChildren.inPopMenu = true;  
    }// **22mar07 CH2Parent**
	
// **22mar07 CH2Parent**class_history.parentStyle = mdsType; // children to see my draw-as type
	hCmenuTree* pMenuTree = NULL;
	
    bool weOwnTree = FALSE;
//x	if (m_pMenuTree == NULL )// should only be null in window,dialog or table
	
    if (m_bIsAbutton || m_bIsAroot || mdsType == mds_dialog) 
	{	//then make it	// if we got here it needs to be filled (not on a menu)-from execute
		
        ASSERT(pMTre == NULL);// we should be starting anew
		
        ASSERT(mdsType == mds_window|| mdsType==mds_dialog     || mdsType == mds_table ||
			   ( (!class_history.inPopMenu) && mdsType==mds_menu) ||/*generate pop-up */
			   (m_bIsAroot && mdsType==mds_menu));	/* main's first fill */

//ASSERT(class_history.parentStyle == mds_do_not_use);/// sjv test - it should be empty...
	
        pMenuTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
		
        weOwnTree = true; // get it deleted		
		
        pMenuTree->setDrawAs(mdsType);// 21mar07 - pass in the draw-as so histories can change

#ifdef _DEBUG
		((hCmenu*)pMenu)->acquireEntryCnt = 0;
#endif				
        TRACE(_T(">>>>>> starting tree arrangement from button <<<<<<\n"));
		
        LOGIT(CLOG_LOG,L">>>>>> starting tree arrangement from button <<<<<<\n");
		
        // 04jun07 - menues defaulting to page on a window----change
		// if ( ((hCmenu*)pMenu)->aquireTree(pMenu, pMenuTree, class_history) )
		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, pMenuTree, hist4MyChildren) )
		{
			TRACE(_T("Tree shows size changed\n"));	
			// if the start tree is empty. this will all be a change
		}
		else
		{
			TRACE(_T("Tree shows no change\n"));
		}
//sjv 03jan07		m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->hv_size);
		
//sjv 28oct10		m_MyLoc.setValue(pMenuTree->topleft,pMenuTree->da_size);
		m_MyLoc = *pMenuTree;
				
        TRACE(_T(">>>>>> ended tree arrangement <<<<<<\n"));
		LOGIT(CLOG_LOG,L">>>>>> ended tree arrangement<<<<<<\n");
	}
	else if(pMTre != NULL) // a recursion
	{
		pMenuTree = pMTre;
		weOwnTree = FALSE;// let the owner delete it
	}
	else
	{
		pMenuTree = NULL;
		weOwnTree = FALSE;
		LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: Laying out a menu that doesn't exist.\n");
	}
		
		
//x	else // we are rebuilding - see if it has been aquired already
//x	if (m_pNewTree)
//x	{// it's already laid out, just replace it in the tree
//x		if ( m_pMenuTree->pParentTree == NULL)
//x		{
//x			LOGIT(CERR_LOG|CLOG_LOG,"ERROR: No parent pointer in menu tree item.\n");
//x			//return 0;
//x			RAZE( m_pNewTree );
//x		}
//x		else
//x		{
//x			m_pMenuTree->pParentTree->replace(m_pMenuTree,/*with*/m_pNewTree);
//x			// done in replace...delete m_pMenuTree;
//x			m_pNewTree  = NULL;// signal reallocation is required.
//x		}
//x	}
//x	else 
//x	{/** assume it is a good tree ***
//x		ASSERT(mdsType == mds_window|| mdsType==mds_dialog  ||
//x			   mdsType == mds_table ||((!parentHist.inPopMenu)&&mdsType==mds_menu) ||
//x			   (m_bIsAroot&&mdsType==mds_menu));
//x
//x		// easyest way to clear the tree is to delete it
//x		delete m_pMenuTree;
//x		m_pMenuTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
//x				TRACE(_T(">>>>>> starting.tree.arrangement<<<<<<\n"));
//x				LOGIT(CLOG_LOG,">>>>>> starting.tree.arrangement <<<<<<\n");
//x		if ( ((hCmenu*)pMenu)->aquireTree(pMenu, *m_pMenuTree, parentHist) )
//x		{
//x			TRACE(_T("Tree shows size changed\n"));	
//x			// if the start tree is empty. this will all be a change
//x		}
//x		else
//x		{
//x			TRACE(_T("Tree shows no change\n"));
//x		}
//x		m_MyLoc.setValue(m_pMenuTree->topleft,m_pMenuTree->hv_size);
//x				TRACE(_T(">>>>>> ended.tree.arrangement <<<<<<\n"));
//x				LOGIT(CLOG_LOG,">>>>>> ended.tree.arrangement  <<<<<<\n");
//x	 **** end assume 11dec06 ***/
//x	}
	ASSERT(pMenuTree != 0);
#ifdef xx_DEBUG
	pMenuTree->dumpSelf();
	LOGIT(CLOG_LOG,L"---------- finished Menu Tree Dump ------------\n\n");
#endif

	if ( pMenuTree->branchList.size() <= 0)
	{//
		if (  (pMenuTree->getDrawAs() == mds_window || 
			   pMenuTree->getDrawAs() == mds_dialog || 
			   pMenuTree->getDrawAs() == mds_menu   || 
			   pMenuTree->getDrawAs() == mds_table    )
//sjv 03jan07			&&(pMenuTree->hv_size.xval == 1 && pMenuTree->hv_size.yval == 1)  )
			&&(pMenuTree->da_size.xval == 1 && pMenuTree->da_size.yval == 1)  )
		{// we are but a button upon the @#$#% of another
// temp2allow translation always
//			m_pMenuTree->setDrawAs(mds_parent); //paul's psuedonym for button!?!?
			if ( weOwnTree )
			{
				if ( !m_bIsAbutton )
					LOGIT(CLOG_LOG,L"Odd menu structure.\n");
				delete pMenuTree;
				pMenuTree = NULL;// force it to recalc tree @ execute
			}
		}
		else
		{
			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: programmer: No menu tree..\n");
		}
		return 0;
	}
	if ( m_DrawList.size() >0 )
	{
		LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: Layout build has items in its draw list already.\n");
	}


	ulong          uCID = 0;
	ulong		   LastTabCtrl = 0; 
	int            i;
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,"    Menu 0x%04x  ", m_symblID);
	if (m_pItem)
	{
		LOGIT(CLOG_LOG,"(%s)\n", m_pItem->getName().c_str());// non unicode name
	}
	else
	{
		LOGIT(CLOG_LOG,"\n");
	}
 #endif
#endif
	menuItemDrawingStyles_t mds;    
	menuTrePtrLstIT_t iT;
	hCmenuTree*       pMt;

	/* we need to populate our drawlist to matchup with the branchlist */
	for (iT = pMenuTree->branchList.begin(); iT < pMenuTree->branchList.end(); iT++)
	{// isa ptr2aptr2a hCmenuTree
		pMt = *iT;
		mds = pMt->getDrawAs(); 
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"        Drawing item as  %s ",menuStyleStrings[mds]);
 #endif
#endif		
		if (mds == mds_page )       
		{	
			if (mdsType == mds_menu)
			{
				LOGIT(CERR_LOG|CLOG_LOG,L"ERROR:Layout got a page on a popup-menu.\n");
			}
			CdrawTab* pDT = NULL;
			if ( LastTabCtrl == 0 )	// we are not already in a tab
			{
				i = m_DrawList.size();// the next index
				pDT        = new CdrawTab(this);
				LastTabCtrl= pDT->m_cntrlID;
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L" [We have a first page, making tab] ");
 #endif
#endif		
				if (pDT != NULL && LastTabCtrl != 0 )
				{
					pDT->m_MyLoc = *pMt;
				  //pP->m_pMenuTree not filled - psuedo-class has no tree entry
					pDT->class_history = pMt->getHistory();
					m_DrawList.push_back(pDT);
					m_CID2DrawMap[LastTabCtrl] = i;// the index we just inserted
				  //m_SID2DrawMap not filled with psuedo-drawclass (no symbol#)
				}
				else // invalid or failure
				{
					TRACE(_T("ERROR: Tab creation failed.\n"));
					LOGIT(CLOG_LOG,L"Initial Tab creation failed -page skipped\n");
					RAZE(pDT);
					continue; // the for loop
				}
			}
			else
			{// we have a tab running
				pDT = (CdrawTab*) m_DrawList[m_CID2DrawMap[LastTabCtrl]];// it better exist
			}
			// insert page
			uCID = pDT->AddPage( pMt );
		}
		else
		if (mds != mds_tab && mds != mds_parent && mds != mds_do_not_use)//&& != mds_page )
		{// not an illegal-unusable type
			LastTabCtrl = 0;  // we are no longer in a tab sequence
			
			CdrawBase* pP = newDraw(mds);
if (mds == mds_bitenum)
	LOGIT(CLOG_LOG,L"");
if (mds == mds_bit)
	LOGIT(CLOG_LOG,L"");
			if (pP)
			{
				//pP->m_pMenuTree   = (hCmenuTree*)(*iT);	// will point into the main tree
//sjv 03jan07	pP->m_MyLoc.setValue((*iT)->topleft,(*iT)->hv_size);
//sjv 28oct10	pP->m_MyLoc.setValue(pMt->topleft,pMt->da_size);
				pP->m_MyLoc = *pMt;
				pP->class_history = pMt->getHistory();
				if (  mds == mds_separator                || //order of tests are IMPORTANT!
					  mds == mds_newline                  || // first two have no ref
					     pP->FillSymb(pMt->getRef()) > 0|| //  not zero at a valid ref
					  mds == mds_constString            ||//) FillSymb would give 0 for this 
					  mds == mds_blank                    )// FillSymb would give 0 for this 
				{		
#ifdef _DEBUG
	if (pP->m_pItem && pP->m_pItem->IsVariable() )
		if 	( ((hCVar*) pP->m_pItem)->VariableType() == TYPE_INDEX && pP->mdsType != mds_index)
			LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: index draw-as is not set to mds_index.\n");
 #ifdef STARTSEQ_LOG
					if (pP->m_pItem)
						LOGIT(CLOG_LOG," ( %s ) -",pP->m_pItem->getName().c_str());//bounds 12feb14
					if (mds < mds_var)
						LOGIT(CLOG_LOG,L"\n");
 #endif
#endif						
					COPY_QUALIFIER(pMt,pP);// from,to
					if(pMt->idx) pP->SetIndex(pMt->idx);
// **22mar07 CH2Parent** pP->FillSelf(0 ,class_history,(hCmenuTree*)(*iT));//menuHist_t());	
					pP->FillSelf(0    ,hist4MyChildren,pMt);//menuHist_t());		// recurse
					i = m_DrawList.size();  // next index
					m_DrawList.push_back(pP);
					m_CID2DrawMap[pP->m_cntrlID] = i;
					m_SID2DrawMap[pP->m_symblID] = i;
				}
				else
				{
					delete pP;
					LOGIT(CERR_LOG|CLOG_LOG,
						L"ERROR: CdrawLayout could not resolve menu item reference.\n");
				}
			}// had a grid test in here in orig code
			else
			{
				LOGIT(CERR_LOG|CLOG_LOG,L"ERROR: could not generate a new Draw item.(%s)\n",
																		menuStyleStrings[mds]);
			}
		}
		else // an illegal-unusable type
		{
			LOGIT(CLOG_LOG,L"ERROR: Got an unusable type (%s) in the branch list.\n",
																		menuStyleStrings[mds]);
		}
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"\n");
 #endif
#endif	
	}// next menu item
#ifdef _DEBUG
 #ifdef STARTSEQ_LOG
		LOGIT(CLOG_LOG,L"    --------end menu list----\n");
 #endif
#endif
	if ( weOwnTree )
	{
		delete pMenuTree;
		pMenuTree = NULL;
	}
	return ( m_DrawList.size() );
}
// *** end *** added paul 13aug08 ***>

void CdrawMenu::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CdrawBase * pBase;
	CWnd * ptr;

    if (!m_pDynCtrl)  // added paul 13aug08 ***>
    {
	// call base class
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
    }
#ifdef _DEBUG
	else
	{
		LOGIT(CLOG_LOG," CDrawMenu::CreateControl with a DynCtrl already populated!\n");
	}
#endif

	// Initialize this control

	if (m_pMenu)
	{
		// This menu lives on another menu, so this should be a pop-up
		// Add its label to the parent menu
		m_pDynCtrl->AppendMenu( this, GetLabel() );
	}
	else
	{
		// This menu does not live on another menu
		// It is probably a "button" on a window or 
		// or dialog.
		//
		// Update its label now
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );
		if (ptr)	// added stevev 07dec16
			ptr->SetWindowText(GetLabel());
		// removed paul 13aug08 ***>
		//return; // children will be generated when this button selected
	}

	// Create its children
	// Similar to CdrawBase:: createMenu
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Now create the controls on its children
		pBase = (*i);

		if (pBase->mdsType == mds_menu)
		// This must be another menu  since it lives in a CdrawMenu
		// Allow it access to the top level menu control
		{
			CdrawMenu * pdrawMenu = (CdrawMenu *) pBase;
			pdrawMenu->m_cntrlID  = m_cntrlID;
			
			// Give the child menu access to the parents menu (this)
			// so that its pop-up menu can be placed on this objects
	        // menu
			pdrawMenu->m_pMenu = m_pDynCtrl->GetMenu();//&(m_pDynCtrl->m_pMenuButton->m_Menu);

			// Call this function on all its children
			pdrawMenu->createControl(pWnd, topLeftParent, centeringOffset);

		}
		else /* stevev 14nov06 - don't know how we got along without this for so long*/
		if (pBase->mdsType == mds_separator || pBase->mdsType == mds_newline)
		{
			m_pMenu->AppendMenu(MF_SEPARATOR);// we gotta have a ptr or we'd a exited
		}
		else
		{// Get the label on the menu item and add to the menu
			m_pDynCtrl->AppendMenu( pBase, pBase->GetLabel() );
		}
	}// next child from drawlist

	m_nPage = 0;// stevev 08jun07 fix crash on a window on a page that is not the first page
	m_pDynTabCtrl = NULL; // stevev 31may2017 try this to keep the tab from going to zero on menu button press
}
// added paul 13aug08 ***>
void CdrawMenu::Execute(WORD wMessageID, LPARAM lParam)
{
	// There is a bug in the FillSelf() that builds the menu items
	// past (whether it is in a CMenu list or a Window or a Dialog)
	// the cdrawWin or cdrawDlg control if it lives in either in a
	// window or dialog.  If this is called through a menu item, the
	// FillSelf() will never be called, therefore we need to call it
	// below.  However, if a dialog lives in a window, FillSelf() will
	// be called twice.
	// FIX:  (OPTIONAL) To be safe, destroy the entire list and start again
    //
    // NOTE: If Destroy is called at the begining, a CdrawWindow that is invoked,
    //       in a previous cdrawMenu call, will be deleted - comment the 
    //       Destroy() out, POB - 4 Aug 2008
	//Destroy(m_symblID);

    int size = m_DrawList.size();

    if (size == 0)
    {
        // Only call this once - Calling it more causes the drawlist to continually
        // grow.  Thus showing same items on the pop-up menu to be generated multiple
        // times.  At this time, we can not call Destroy() above, which would noramally
        // fix this issue (see notes above), POB - 4 Aug 2008
	    FillButton();
        
        //Now create the menus
        pnt_t loc = {0,0};
		// stevev 08oct10:was:>
        createControl(GETMAINFRAME,loc);///AfxGetMainWnd(),loc);
    }


    // Pop up a menu for this button control.  We are no longer useing the 
    // CURLLinkButton::OnClicked().  Now calling it here for more contorl, POB - 4 Aug 2008
	CRect menuRect = GetRect();
	CPoint point = menuRect.TopLeft();// get the top-left position of button as dropdownpoint

	m_pDynCtrl->GetMenu()->TrackPopupMenu( TPM_LEFTALIGN | TPM_LEFTBUTTON,
                                         point.x, point.y, GETMAINFRAME );//AfxGetMainWnd() );

    // Every time a menu item is deleted the items move back to the first position
    // (ie, pos == 0). Therefore, do not increment pos in order to delete the 
    // entire list
//  for(int pos = 0; pos < m_pDynCtrl->GetMenu()->GetMenuItemCount(); /*pos++*/)
//	{
//      m_pDynCtrl->GetMenu()->RemoveMenu(pos,MF_BYPOSITION);
//      CMenu * pMenu = m_pDynCtrl->GetMenu()->GetSubMenu(pos);
//	}


    // We don't want to have any children living under this dialog
	// destroy them on the way out
    // NOTE:  Calling the Destroy() does remove the children not currently being used
    //        after the user has selected the appropriate menu item from the pop-up menu, 
    //        but it also has a bad side affect of removing the child entry from the window
    //        hierarchy that was seleted Therefore the item selected by the 
    //        user is not displayed.  Comment the Destroy() until we can find a better 
    //        design, POB - 4 Aug 2008
    //         
	//Destroy(m_symblID);

}
// ***end***  added paul 13aug08 ***>

// Poor name - This function is changed to DeleteMenu since memory is being destroyed.
void CdrawMenu::DeleteMenu(CdrawBase * pChild)
{
	CString strMenu;
	// This child, is a popup menu, we need to delete this menu
	// and free up its memory

	for(unsigned pos = 0; pos < m_pDynCtrl->GetMenu()->GetMenuItemCount(); pos++)
	{
		m_pDynCtrl->GetMenu()->GetMenuString(pos, strMenu, MF_BYPOSITION);

		if (pChild->GetLabel() == strMenu)
		{
			// A match is found!!

			// If this child, is a popup menu, we need to delete this menu
			// and free up its memory
			m_pDynCtrl->GetMenu()->DeleteMenu(pos,MF_BYPOSITION);
		}
	}
}

bool CdrawMenu::Rebuild(long SymID, CWnd *m_pWindow)
{
	if (m_pItem)
	{
		string tN; tN = m_pItem->getName() ;
		LOGIT(CLOG_LOG,"      Rebuild CdrawMenu 0x%04x (%s)\n",m_pItem->getID(),tN.c_str());

	}
	else
	{
	LOGIT(CLOG_LOG,"      Rebuild CdrawMenu\n");
	}

	if (m_DrawList.size() == 1)
	{
		CdrawBase* pB = m_DrawList.at(0);
		if (pB->mdsType == mds_window)
		{
			((CdrawWin*)pB)->Rebuild(SymID, NULL);
		}
	}
	else
	{
	if (m_pStartTree)
		m_pStartTree->destroy();
	Destroy(m_symblID);//	destroy self
	FillSelf(m_symblID, class_history, m_pStartTree); //fillself
	pnt_t p = {0,0};// pages are always 0 w/ 0 offset
	createMenu(m_pWindow, p);// create menu self
	}

	return false;
}

/**********************************************************************************************
 * CdrawTable
 *
 *	The draw class is responsible for layout of the Table menu style  (not implemented 14nov06)
 *
 *********************************************************************************************/

CdrawTbl::~CdrawTbl()
{
	if(m_pTable && ::IsWindow(m_pTable->m_hWnd))
	{
		m_pTable->DestroyWindow();
	}
}

void CdrawTbl::FillSize(void)	
{
	// This is a menu of style TABLE, but
	// if it lives on a menu of style DIALOG, PAGE, GROUP, WINDOW, etc
	// it needs to be a button
	m_MyLoc.da_size.xval = m_MyLoc.da_size.yval = 1;
}

void CdrawTbl::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CWnd * ptr;

	// There will probably be a two controls for this draw type
	// (1) Parent button and (2) child frame

	// Create a parent button control if this item lives
	// on a window or dialog
	CdrawBase::createControl(pWnd, topLeftParent, centeringOffset);
	
	// There is a bug in the FillSelf() that builds the menu items
	// past (whether it is in a CMenu list or a Window or a Dialog)
	// the cdrawWin or cdrawDlg control if it lives in either in a
	// window or dialog.
	// FIX:  To be safe, destroy the entire list under the button
	// This will prevent a crash that can occur during an update
	Destroy(m_symblID);

	// Initialize the Label
	// parent control (ie, button)
	if (m_pDynCtrl)
	{
		ptr = m_pDynCtrl->GetDlgCtrl( m_cntrlID );

		if (ptr)
		{
			ptr->SetWindowText(GetLabel());
		}
	}

	m_nPage = 0;// stevev 08jun07 fix crash on a window on a page that is not the first page
	m_pDynTabCtrl = NULL; // stevev 31may2017 try this to keep the tab from going to zero on window button press

	// put back the original mds type
	mdsType = mds_table;
}

void CdrawTbl::Execute(WORD wMessageID, LPARAM lParam)
{
	CMainFrame * pMainWnd = GETMAINFRAME;// stevev 08oct10:
									//was:>(CMainFrame *)AfxGetMainWnd();// post msg & activate
	
	//Prevent muliple windows from any given object
	if (!m_pTable)
	{// Create child window (modeless)
		pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)this);
	}
	else
	{
		if(::IsWindow(m_pTable->m_hWnd))
		{
			CMDIChildWnd* pChildFrame = ( CMDIChildWnd* )m_pTable;
			pChildFrame->MDIActivate();
		}
	}
}

bool CdrawTbl::Rebuild(long SymID)
{
	LOGIT(CLOG_LOG,"      Rebuild CdrawTbl\n");
	return false;
}
/**********************************************************************************************
 * CdrawMain
 *
 *	The draw class that holds all others - has No Parent!
 *
 *********************************************************************************************/

// POB - 20 Sep 2004
// The entry menus are a unique animal.  These are menus that do not live on 
// a another menu so there is no menu list to get.  The solution is to use
// one CdrawMenu object that now becomes the parent to these entry menus

int CdrawMain::FillSelf(ulong selfSymID,menuHist_t hist,hCmenuTree* pMTre)// does it all now
{
	// Here we will add each entry menu
	// to this drawlist (m_DrawList)
	UINT item = selfSymID;
	menuItemList_t vmi;
	hCmenu * pMenu;
	menuItemDrawingStyles_e drawStyle = mds_table;  // Default style
	menuHist_t     local_history;
	int i;

	hCitemBase * p_myItem = NULL;

	CDDIdeDoc* pDoc = GetDocument();

	if (!pDoc)
	{
		return FALSE;
	}	

	if ( !pDoc->pCurDev )
	{
		return FALSE ; /// error -  no device!!!!!!!!!!!!
	}

	RETURNCODE rc = pDoc->pCurDev->getItemBySymNumber(item, &p_myItem);

	if ( rc != SUCCESS || p_myItem == NULL )
	{
		return 0; // error:  item I am supposed to be doesn't exist !!!!!!!!!!!!
	}

	if (p_myItem->getIType() != iT_Menu)
	{
		return 0; // error:  I am supposed to be a menu of type menu !!!!!!!!!!!!
	}
	
	local_history.dlgInPath  = false;
	local_history.inMethDlg  = false;
	local_history.grpDepthHit= false;
	local_history.inPopMenu  = true;		// stevev 09aug05
	local_history.parentStyle= mds_menu;	// stevev 09aug05
	local_history.inReview   = false;		// stevev 31aug07 - new

	pMenu = (hCmenu*)p_myItem;
	
	// The entry menu does not live on another menu
	// (ie, no parent menu) we need to set the default
	// style here
	// DEFAULT = TABLE
	switch(pMenu->getStyle())
	{
		case menuStyleWindow:
			drawStyle = mds_window;
			break;

		case menuStyleDialog:
			drawStyle = mds_dialog;
			break;

		case menuStylePage:  // NOT ALLOWED - translate to table
			break;

		case menuStyleGroup: // NOT ALLOWED - translate to table
			break;

		case menuStyleMenu:
			drawStyle = mds_menu;
			break;

		case menuStyleTable:  // The default is already Table
			drawStyle = mds_table; // set it anyway
			break;

		case menuStyleUnknown: // leave it the default style table
		case menuStyleNone:
		default: 
			break;

	}

	CdrawBase* pP;


	if(item == VIEW_MENU)
	{
		// Special Case:
		// This is a menu called view_menu
		// It is important to Wally that we never put the label of this menu on the "View"
		// menu.  We only put its childrent on the menu.
		// Let's go get its children
		menuItemList_t localMenuList;
		menuItemList_t::iterator iT;
		/* stevev 26oct04 */
		menuItemDrawingStyles_t tmp = mdsType;// hold for restoration
		mdsType = mds_menu;		 // isa menu for this operation
		/* end stevev 26oct04 */
		/* stevev 14nov06 */		
		hCitemBase*	tpib =  m_pItem; 
		ulong       tsid =  m_symblID;
		m_symblID   = selfSymID; // make this look like that temporarily
		/* end stevev 14nov06 */
		// done local_history.inPopMenu   = true;		// stevev 01aug05
		// done local_history.parentStyle = mds_menu;	// stevev 01aug05

		int c = getList(selfSymID, localMenuList, local_history );
		/* stevev 14nov06 -continued */
		m_pItem   = tpib;
		m_symblID = tsid;
		mdsType   = tmp;/* stevev 26oct04 -- moved up 14nov06*/
		/* end stevev 14nov06 ...    */	
		menuItemDrawingStyles_t thisStyle;

		for (iT = localMenuList.begin(); iT < localMenuList.end(); iT++)
		{// for each item in the view_list menu::
			hCmenuItem* pCast = (hCmenuItem*)&(*iT);// PAW 03/03/09 &(*) added
			thisStyle = pCast->getDrawAs();
			pP = newDraw(thisStyle);

			if (pP)
			{
//this is a menulist  pP->m_pMenuTree   = (hCmenuTree*)iT;
				pP->class_history = pCast->getHistory();
				if ( thisStyle == mds_separator || // order of these tests are IMPORTANT!
					 thisStyle == mds_newline   ||
					 pP->FillSymb(pCast->getRef()) > 0   || 
				     thisStyle == mds_constString )
				{					
					COPY_QUALIFIER(pCast,pP);// from,to
					pP->FillSelf(0    ,local_history, NULL);// local_history is the parent here
//x					if (pP->m_pMenuTree == NULL )
//x					{
//x						pP->m_pMenuTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
//x						pP->m_pMenuTree->operator=(*iT);
//x					}
					i = m_DrawList.size(); // next index
					m_DrawList.push_back(pP);
					m_CID2DrawMap[pP->m_cntrlID] = i;
					m_SID2DrawMap[pP->m_symblID] = i;
					
					// Get the 1st child living in view_menu and capture
					// its symbol number.  This way we know where to place
					// a "seperator" so that we can keep the HCF menus
					// seperate from view_menu items.
					if (!m_symToSep)
					{
						m_symToSep = pP->m_symblID;
					}

					// Get all the symbol ids of the items living in the
					// VIEW_MENU
					m_ViewSymList.Add(pP->m_symblID);
				}
				else
				{
					delete pP;
					TRACE(_T("ERROR: CdrawMain could not resolve menu item reference.\n"));
				}
			}
			else if (thisStyle == mds_do_not_use)
			{
				TRACE(_T(" NOTE: got a Do Not Use draw style in Main menu.\n"));
			}
			else
			{
				TRACE(_T("ERROR: could not generate a new Draw item in Main menu..\n"));
			}
		}
		// moved it up - 14nov06
	}
	else // not VIEW_MENU
	{// just put it on self's list
		//local_history's inititalization moved more global stevev 14dec06

		if ( (drawStyle != mds_do_not_use) && (drawStyle != mds_parent) )
		{
			// Put this one entry menu on Window header's "View" menu
			pP = newDraw(drawStyle);
			if (pP)
			{
				//pP->m_pMenuTree   = (hCmenuTree*)iT;
				pP->m_bIsAroot    = TRUE;
				pP->class_history = local_history;
				pP->FillSelf(item    , hist, NULL);//menuHist_t());
//x				if (pP->m_pMenuTree == NULL )
//x				{
//x					pP->m_pMenuTree = (hCmenuTree*) new hCmenuTree(pMenu->devHndl());
//x					pP->m_pMenuTree->operator=(*pMenu);
//x				}
				i = m_DrawList.size(); // next index
				m_DrawList.push_back(pP);
				m_CID2DrawMap[pP->m_cntrlID] = i;
				m_SID2DrawMap[pP->m_symblID] = i;
			}
			else
			{
				TRACE(_T("MainMenu fill ERROR: could not generate a new Draw item.(%d)\n"),
																					drawStyle);
			}
		}
		// else - discard quietly
	}
#ifdef _DEBUG /* test dev object menu selection */

	hCmenuTree localTree(pMenu->devHndl());
	menuHist_t	cls_history;
	cls_history.parentStyle = mdsType;
TRACE(_T(">>>>>> starting tree <<<<<<\n"));
LOGIT(CLOG_LOG,L">>>>>> starting tree <<<<<<\n");
	if ( ((hCmenu*)pMenu)->aquireTree(pMenu, &localTree, cls_history) )
	{
		TRACE(_T("Tree shows size changed\n"));
	}
	else
	{
		TRACE(_T("Tree shows no change\n"));
	}
TRACE(_T(">>>>>> ended tree <<<<<<\n"));
LOGIT(CLOG_LOG,L">>>>>> ended tree <<<<<<\n");

#endif

	CMainFrame * pMainWnd = GETMAINFRAME;
	if ( item == ROOT_MENU)	
		pMainWnd->PostMessage(WM_SDC_ONWINDOW, (WPARAM)pP, NULL);//cdrawbase ptr
		//pMainWnd->SendMessage(WM_SDC_ONWINDOW, (WPARAM)pP, NULL);


	return ( m_DrawList.size() );
}

void CdrawMain::FillSize(void)
{
	// For some strange reason I can not make this an inline function without
	// getting an unresolved external symbol link "LNK2001" error
}

void CdrawMain::createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset)
{
	CString strLabel = "???";
	CdrawBase * pBase;

	if(!m_pMenu || m_DrawList.size() == 0)
	{
		return;
	}

	// We need to put a seperator on the "View" menu
	// to seperate the resource file items from the following
	// DDL entry menus.
	m_pMenu->AppendMenu(MF_SEPARATOR);

	// Find the Menu enteries and create them	
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{
		// Now create the controls on its children
		pBase = (*i);

		if (m_symToSep == pBase->m_symblID)
		{
			m_pMenu->AppendMenu(MF_SEPARATOR);
		}

		if (pBase->mdsType == mds_menu)
		{
			//Give the view menu to the CdrawMenu object
			CdrawMenu * pdrawMenu = (CdrawMenu *) pBase;
			pdrawMenu->m_pMenu = m_pMenu;

			pBase->createControl(pWnd, topLeftParent);
		}
		else 
		if (pBase->mdsType == mds_separator || pBase->mdsType == mds_newline)
		{
			m_pMenu->AppendMenu(MF_SEPARATOR);
		}
		else
		{
			// Attach the control id to the View menu
			m_pMenu->AppendMenu(MF_STRING, pBase->m_cntrlID, pBase->GetLabel());
		}
	}
}

void CdrawMain::Destroy(ulong symblID)
{
	int nMenuItems;
	int nViewItem;

	if (symblID == VIEW_MENU)
	{
		nMenuItems = m_pMenu->GetMenuItemCount();

		for (nViewItem = 0; nViewItem < m_ViewSymList.GetSize(); nViewItem++)
		{
			// Destroy all the Cdraw objects with this symbol living on 
			// the VIEW_MENU
			// This Destroy will call this CdrawMain::Destroy() again
			// The intent of calling it again is to use the 
			// CdrawBase::Destroy() below
			Destroy(m_ViewSymList.GetAt(nViewItem));
		}// next

		// If there were View Menu Items added to the "Windows Menu"
		// we need to do some clean up.  We also need to deal with 
		// the SEPARATORS as well
		if (nViewItem) 
		{

			// 1st, remove all items from the View Symbol array list
			m_ViewSymList.RemoveAll();

   			// The new count should be the difference between what was deleted
   			// in the VIEW_MENU.  If it is not, then there must be some DDL 
   			// SEPARATORS to contend with.  Remove them here.
   			while ((int)(m_pMenu->GetMenuItemCount()) > nMenuItems - nViewItem)
			{
   				m_pMenu->DeleteMenu(m_pMenu->GetMenuItemCount()-1,MF_BYPOSITION);
   			}
			
   			// Now remove the MFC seperator that divides the VIEW_MENU items
   			// from the primary menu (Manin DDL entry menus) enteries 
   			// (should be the last entry)
   			m_pMenu->DeleteMenu(m_pMenu->GetMenuItemCount()-1,MF_BYPOSITION);
   		}
	}
	
	// Destroy the symbol ID.
	// NOTE:  This call will also destroy the VIEW_MENU symbol ID if it 
	//        is being use in any other DDL entry menu (or on down).
	CdrawBase::Destroy(symblID);
}

void CdrawMain::AddMenus(CWnd * pWnd)
{
	menuHist_t  psuedohist(mds_menu);// these are all built like they're on a menu
	psuedohist.inPopMenu = true;
	pnt_t loc = {0,0};

	// Search for "root_menu"
	FillSelf(ROOT_MENU,psuedohist,NULL);

	// Search for "offline_root_menu"
	FillSelf(OFFLINE_ROOT_MENU,psuedohist,NULL);

	// Search for "diagnostic_root_menu"
	FillSelf(DIAGNOSTIC_ROOT_MENU,psuedohist,NULL);
	
	// Search for "process_variables_root_menu"
	FillSelf(PROCESS_VARIABLES_ROOT_MENU,psuedohist,NULL);

	// Search for "device_root_menu"
	FillSelf(DEVICE_ROOT_MENU,psuedohist,NULL);

	// Search for "Maintenance_root_menu"
	FillSelf(MAINTENANCE_ROOT_MENU,psuedohist,NULL);

	// Search for "view_menu"
	FillSelf(VIEW_MENU,psuedohist,NULL);
	
	//Now create the menus
	createControl(pWnd,loc);
}

void CdrawMain::DeleteAllMenus()
{
	int nMenuItems = m_pMenu->GetMenuItemCount();

#ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,L"< mainFrm.RemoveAllMenus ROOT \n");
#endif
	Destroy(ROOT_MENU);

	// Search for "offline_root_menu"
	Destroy(1002);

	// Search for "diagnostic_root_menu"
	Destroy(1018);
	
	// Search for "process_variables_root_menu"
	Destroy(1019);

	// Search for "device_root_menu"
	Destroy(1020);

#ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,L"< mainFrm.RemoveAllMenus VIEW \n");
#endif
	// Search for "view_menu"
	Destroy(VIEW_MENU);

	// If the number items at this point are different than the number
	// before all the calls to Destroy() above then there were items
	// deleted and the SEPARATOR needs to be removed.
	// If they are equal then no change was performed to the Window's
	// menu and we need to skip this DeleteMenu() call.
	
	int newcount = m_pMenu->GetMenuItemCount();
	
	if (m_pMenu->GetMenuItemCount() != nMenuItems)
	{
   		// Now remove the seperator that divides the resource file items
   		// from the primary menu enteries (ie, root menu, offline root menu, etc.)
		// The separtator should now be the last entry since all the above
		// entry menus have been deleted.
   		m_pMenu->DeleteMenu(m_pMenu->GetMenuItemCount()-1,MF_BYPOSITION);
	}
#ifdef STARTSEQ_LOG
	LOGIT(CLOG_LOG,L"< mainFrm.RemoveAllMenus Exit \n");
#endif

}

void CdrawMain::DeleteMenu(CdrawBase * pChild)
{
	CString strMenu;

	// Go through windows "View Menu" looking up the names of the 
	// item that lives on this MFC menu until we find the one
	// that matches.
	for(unsigned pos = 0; pos < m_pMenu->GetMenuItemCount(); pos++)
	{
		if (!m_pMenu->GetMenuItemID(pos) )
		{
			// A zero ID means it is probably a separator on MFCs
			// "View Menu".  Skip for now.  Go on to the next item
			// in the menu.
			continue;
		}

		m_pMenu->GetMenuString(pos, strMenu, MF_BYPOSITION);

		if (pChild->GetLabel() == strMenu)
		{
			// A match is found!!

			// If this child, is a popup menu, we need to delete this menu
			// and free up its memory
			m_pMenu->DeleteMenu(pos,MF_BYPOSITION);
			break;
		}
	}
}

bool CdrawMain::Update(long type,long SymID) // update entry point
{// it will never be us, do the children..
	CdrawBase *  pBase   = NULL, *pLastBase = NULL;
	CdrawLayout* pLayout = NULL;
	// Iterate through child list
	bool anychg = false, didchng = false;// debug purposes
	unsigned changeCnt = 0;
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)// for each base menu
	{	// Look for this control id in its children
		pBase = (CdrawBase *)(*i);
		// Call the base class on this child object to search on it and/or its children
		// stevev 02sept08 - some types don't update...
		if ( pBase->mdsType != mds_separator &&
			 pBase->mdsType != mds_newline )
		{
			didchng = pBase->Update(type, SymID);
		}
		else
			didchng = false;

		if ( didchng ) // for each base menu that changed...
		{
			if ( pBase->mdsType == mds_window || pBase->mdsType == mds_dialog || 
				 pBase->mdsType == mds_menu   || pBase->mdsType == mds_table  )
			{
				setProtection( SymID,true);
				
				pLayout = dynamic_cast<CdrawLayout*>( pBase );
				if (pLayout)
					pLayout->Rebuild(SymID, NULL);
				else
					LOGIT(CERR_LOG,"ERROR: Item on MainMenu would not cast to Layout.\n");

				setProtection( SymID,false);
			}
			else
			{
				LOGIT(CLOG_LOG|CERR_LOG,"ERROR: improper type on main menu.\n");// should never
			}
		}
		anychg |= didchng;
		didchng = false;
	}// next child
	return false;// i didn't change...
}

void CdrawMain::setProtection(long SymID,bool makeRebuilding)
{
	CdrawBase * pBase;
	// Iterate through child list
	DrawList_t::iterator i;
	for (  i = m_DrawList.begin(); i != m_DrawList.end(); ++i)
	{	// Look for this control id in its children
		pBase = (CdrawBase *)(*i);
		
		pBase->setProtection(SymID, makeRebuilding);
	}
}

/* no longer used...polymorphism is easier...
bool CdrawMain::Rebuild(CdrawBase * pB, long SymID) 
{
	bool r = false; // no error
	if ( pB->mdsType == mds_window )
	{
		CdrawWin* pW = dynamic_cast<CdrawWin*>( pB );
		if (pW)
			r = pW->Rebuild(SymID);
		else
			LOGIT(CERR_LOG,"ERROR: Item marked as Window would not cast to CdrawWin.\n");
	}
	else
	if( pB->mdsType == mds_dialog )
	{		
		CdrawDlg* pD = dynamic_cast<CdrawDlg*>( pB );
		if (pD)
			pD->Rebuild(SymID);
		else
			LOGIT(CERR_LOG,"ERROR: Item marked as Dialog would not cast to CdrawDlg.\n");
	}
	else
	if ( pB->mdsType == mds_table )
	{
		CdrawTbl* pT = dynamic_cast<CdrawTbl*>( pB );
		if (pT)
			pT->Rebuild(SymID);
		else
			LOGIT(CERR_LOG,"ERROR: Item marked as Table would not cast to CdrawTbl.\n");
	}
	else
	if ( pB->mdsType == mds_menu )
	{
		CdrawMenu* pM = dynamic_cast<CdrawMenu*>( pB );
		if (pM)
			pM->Rebuild(SymID);
		else
			LOGIT(CERR_LOG,"ERROR: Item marked as Table would not cast to CdrawMenu.\n");
	}
	else
	{// error
		LOGIT(CERR_LOG|CLOG_LOG,L"Ineligible menu type: %s\n",menuStyleStrings[pB->mdsType]);
		r = true;
	}
	return r;
}
*** end no longer used **/