/*************************************************************************************************
 *
 * $Workfile: DDLVariable.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		interface for the CDDLVariable class.
 * #include "DDLVariable.h".		
 */
#if !defined(AFX_DDLVARIABLE_H__18E1FD9B_EC2C_4031_814B_FFCBAB546CF5__INCLUDED_)
#define AFX_DDLVARIABLE_H__18E1FD9B_EC2C_4031_814B_FFCBAB546CF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLBase.h"
#include "ddbDevice.h"
//#include "DDLDevice.h"
#include "DDLMenu.h"
#include "DDLexpansion.h"

/***************************************************************************
 * Global Definitions
 ***************************************************************************/

// bit enum strings...
// these need to be replaced by a call to get the sdc dd string (of proper lang)
#define BIT_SET   L"Set"
#define BIT_CLR   L"Clear"

/*
 * Variable Identification Types.
 */ 
enum VarType
{
    TYPE_NONE     =0,
	TYPE_DUMMY,
	TYPE_INTEGER,	// 2
	TYPE_UNSIGN_INT,
	TYPE_FLT,
	TYPE_DBL,		// 5
	TYPE_ENUM,
	TYPE_BITENUM,
	TYPE_INDEX,
	TYPE_STRING,	// packed_ascii
	TYPE_DUMMY02,	//10 is packed ascii in the rest of the program
	TYPE_PASSWORD,
	TYPE_DUMMY03,	// FF type BITSTRING
	TYPE_DATE,
	TYPE_TIMEVALUE = 20,		
	TYPE_BIT		// single bit from bit-enum +stevev 17may11
};

class CDDLVariable : public CDDLexpTbl, public CDDLBase
{
    DECLARE_DYNAMIC(CDDLVariable)

public:
	CDDLVariable(hCitemBase* pIB = NULL);
	CDDLVariable(itemInfo_t& pIi);// sets help too
/* 21mar05 replaced in above     CDDLVariable(hCVar* pddbVar); */

	virtual ~CDDLVariable();

    struct DDLVariant 
    {
        VarType  vt;
        BYTE     bVal;   
        float    fltVal;   
        double   dblVal;   
        BOOL     boolVal;   
        int      intVal; 
        CString  strVal;
//        CMap<int,int,CString,CString> strEnum;
        CArray<double,double> min;
        CArray<double,double> max;
    };

// assignment operators
    const CDDLVariable& operator=(const float& fValue);
    const CDDLVariable& operator=(const int& nValue);
    const CDDLVariable& operator=(const CString& strValue);

// cast operators
    operator float();
    operator int();
    operator CString();

// Special menu handling
	void Execute();
    void LoadContextMenu(CMenu* pMenu, int& nPos);
    void OnUpdateMenuExecute(CCmdUI* pCmdUI);
    void DisplayValue();

// Overrides
    CString GetRelation();
	BOOL IsValid();
    BOOL IsVariable();
    BOOL IsReadOnly();  // This deals with the handling (ie, WRITE/READ), if TRUE - no WRITE
	BOOL IsDynamic();
	BOOL IsChanged();
 
// Variable specific
	//vector<EnumTriad_t>& GetEnumList(void);
	RETURNCODE GetEnumList(vector<EnumTriad_t>& retList);
	RETURNCODE GetBitEnumList(CArray<EnumTriad_t,EnumTriad_t&> &subDataList, int &valLen, ulong &value);

	ulong GetId();
	void Cancel();
/* /*<REMOVE> stevev 020304 - PAR 5256 - use the baseclass name 
	CString GetName();
*/
    CString GetEnumNext(int& nEnum);
    CString GetEnumStart(int& nEnum);
    int GetEnumSize();
    CString GetEnumString(int nEnum);
    float* GetMinFloat(UINT& size);
    float* GetMaxFloat(UINT& size);
    void SetMinMax(float min, float max);
    int* GetMin(UINT& size);
    int* GetMax(UINT& size);
	void SetMinMax(int min, int max);
    BYTE GetCharLength();
   	BOOL SetCharLength(BYTE cLength);
    // deprecated 08feb11  CString GetEditFormat();
	BOOL SetEditFormat(CString strFormat);
    BOOL SetHandling(BYTE handling);
    void SetRelation (CString strRel);
    void SetRelation (hCVar* pVar);
    int Write();
    VarType GetType();
    void SetType(VarType type);
	hCddbDevice* GetDevicePtr();
	CString GetUnitStr();

private:
    vector<hCenumDesc*>::iterator m_enumPos;// never used
//    POSITION m_enumPos;// never used
	unsigned m_BitMask;// for TYPE_BIT
    BYTE m_cLength;
    CString m_constUnits;
//    CString m_dsplyFormat;
    CString m_edtFormat;
    BOOL m_bIsDsplyVal;
    BOOL m_bIsReadOnly;
    DDLVariant m_varData;
    hCVar* m_pUnitRelVar;

	void initVariable(hCitemBase* pIB);// PAW 03/03/09 void added

public:
    hCVar* m_pddbVar;
};

#endif // !defined(AFX_DDLVARIABLE_H__18E1FD9B_EC2C_4031_814B_FFCBAB546CF5__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLVariable.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
