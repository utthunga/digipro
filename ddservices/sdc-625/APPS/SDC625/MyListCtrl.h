#if !defined(AFX_MYLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_)
#define AFX_MYLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MainListCtrl.h : header file
//
#include "SDC625Doc.h"
#include "SDC625ListView.h"
/////////////////////////////////////////////////////////////////////////////
// CMyListCtrl window

class CMyListCtrl : public CListCtrl
{
// Construction
public:
	CMyListCtrl();

	// Attributes
public:
// Operations
	CDDIdeDoc* GetDocument();

public:

// Overrides
	void OnContextMenu(NMHDR *pNMHDR, LRESULT *pResult);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyListCtrl)
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINLISTCTRL_H__D20DF602_A049_4731_B7D1_BDFB83FC06C0__INCLUDED_)
