/*************************************************************************************************
 *
 * $Workfile: SDC625_drawTypes.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This describes the various controls that we have to draw on the screen.
 *	
 *
 *		
 * #include "SDC625_drawTypes.h"
 */


#ifndef _DRAW_TYPES_
#define _DRAW_TYPES_
 
#include "pvfc.h"
#include "stdafx.h"
#include "sdc625.h"
#include "SDC625Doc.h"

#include "ddbItemBase.h"
#include "ddbGeneral.h"
#include "ddbDefs.h"
#include "ddbMenu.h"
#include "ddbMenuLayout.h"

// 08nov06      #define DO_PUBLISH
#undef PUBLISHTEST	/* 08nov06  */

#ifdef DO_PUBLISH
#include "ddbPublish.h"
#endif

#include "ximage.h"

#pragma warning (disable:4786)
#include <map>
using namespace std;


#ifdef _TESTDATA
#include "testData.h"
#endif

#ifdef _DEBUG
#define DCDEBUG 0
#define DMPFLGS  (CFile::modeCreate | CFile::modeReadWrite)
#endif

// moved to ddbMenuLayout.h sjv 26oct06 #define DDSizeLocType             int  /* may have to be float in the future for partial row/cols */
#define HANDHELD_FLOAT_MAXVAL_LEN 12
#define MAX_DATE_LENGTH           10

class CDynControls;
class CdrawBase;

typedef vector<CdrawBase*> DrawList_t;  // ordered list of contents
#define DrawListIterator_t DrawList_t::iterator 

typedef map< ulong, int > Id2DrawMap_t; // symbolID2vectorIndex AND CntrlID2vectorindex
#define DrawIterator_t    Id2DrawMap_t::iterator  /* typedef doesn't work correctly */

typedef struct pnt_s
{
	DDSizeLocType xval;
	DDSizeLocType yval;
}
/* typedef */ pnt_t;

extern const pnt_t dfltOfst;
 
enum
{
/*  POPUP_MAIN     = 0,  
    POPUP_MENU,
    POPUP_VARIABLE,
    POPUP_EDITDISPLAY,
    POPUP_WRITEASONE,
    POPUP_METHOD,
	POPUP_WNDMAIN = 6,
	POPUP_WNDVAR,
	POPUP_WNDENUM*/
};


// moved to ddbMenuLayout.h 20oct06 -------------------------------------------------
//#define COLS_SUPPORTED  3
//
//#define DFLTHGT		11	// medium
//#define DFLTWID		 3	// medium
//#define DFLTHGT_INDX  4 //Vibhor Added 030105
//#define DFLTWID_INDX  4 //Vibhor Added 030105
//
///* map horiz sizes to columns */
//#define HORIZSCOPESIZES  { \
///*	gS_Undefined,		 0	*/						DFLTWID, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,\
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	COLS_SUPPORTED-1,\
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	COLS_SUPPORTED,\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	COLS_SUPPORTED,\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	COLS_SUPPORTED,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	COLS_SUPPORTED,\
///*	gS_Unknown			 8	MAX_SIZE			*/	COLS_SUPPORTED }
//
///* map vertc sizes to rows */
//#define VERT_SCOPESIZES  { \
///*	gS_Undefined,		 0	*/						DFLTHGT, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	DFLTHGT-5, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	DFLTHGT-5,\
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	DFLTHGT-2,\
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	DFLTHGT,\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	DFLTHGT+2,\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	DFLTHGT+5,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	DFLTHGT+10,\
///*	gS_Unknown			 8	MAX_SIZE			*/	DFLTHGT }
///******************************************************************************/
//#ifdef GOLDENRECTANGLE
//#if ( COLS_SUPPORTED == 3 )
///* map horiz sizes to columns */
//#define HORIZSCOPESIZES  { \
///*	gS_Undefined,		 0	*/						3, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,/*7*/\
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	2,\
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	2,/*13*/\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	3,\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	3,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	3,/*20*/\
///*	gS_Unknown			 8	MAX_SIZE			*/	3 }
//
///* map vertc sizes to rows */
//#define VERT_SCOPESIZES  { \
///*	gS_Undefined,		 0	*/						20, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	3, \
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	7, \
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	13,\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	16,\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	18,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	20,\
///*	gS_Unknown			 8	MAX_SIZE			*/	20 }
//#endif
//
//#if ( COLS_SUPPORTED == 5 )
///* map horiz sizes to columns */
//#define HORIZSCOPESIZES  { \
///*	gS_Undefined,		 0	*/						3, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	1, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	1,/* 7*/\
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	2,/*13*/\
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	3,/*20*/\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	4,/*26*/\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	5,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	5,/*33*/\
///*	gS_Unknown			 8	MAX_SIZE			*/	3 }
//
///* map vertc sizes to rows */
//#define VERT_SCOPESIZES  { \
///*	gS_Undefined,		 0	*/						20, \
///*	gS_xx_Small,		 1	XX_SMALL_DISPSIZE	*/	3, \
///*	gS__xSmall,			 2	X_SMALL_DISPSIZE	*/	7, \
///*	gS_Small,			 3	SMALL_DISPSIZE		*/	13, \
///*	gS_Medium,			 4	MEDIUM_DISPSIZE		*/	20,\
///*	gS_Large,			 5	LARGE_DISPSIZE		*/	26,\
///*	gS__x_Large,		 6	X_LARGE_DISPSIZE	*/	29,\
///*	gS_xx_Large,		 7	XX_LARGE_DISPSIZE	*/	33,\
///*	gS_Unknown			 8	MAX_SIZE			*/	20 }
//#endif
//#endif
// ---------------------------end moved

#ifdef DO_PUBLISH
class CdrawBase : public hCpubsub
{
protected:
	int           sub_handle;
	ddbItemList_t sub_List;
#else
class CdrawBase 
{
public:
	virtual void  staleDynamics(void) PVFC( "CdrawBaseA" );// required of all draw classes
	bool  StructChange(long SymID) { return( Update(UPDATE_STRUCTURE,SymID));};
	bool  Value_Change(long SymID) { return( Update(UPDATE_ONE_VALUE,SymID));};
protected:
#endif
	DrawList_t   m_DrawList;			// this owns the memory - delete each on exit
	Id2DrawMap_t m_CID2DrawMap;
	Id2DrawMap_t m_SID2DrawMap;

	bool  inActions;// to prevent actions incurring actions

	CDDIdeDoc* GetDocument();
	int getList(UINT item, menuItemList_t& vmi, menuHist_t hist ); // returns size or 0 on error
	CdrawBase* newDraw(menuItemDrawingStyles_t dt);
//	int fillList(char* listName);

public:
	int menuSize(void); // size self as a menu - no reason to protect publicized 10nov06 sjv
	CdrawBase(menuItemDrawingStyles_t t, CdrawBase* pParent);
	virtual ~CdrawBase();

	hCreference*			m_pRef;
	hCitemBase*		        m_pItem; // made public to help debugging
	CDynControls*           m_pDynCtrl;
	ulong                   m_cntrlID;
	ulong                   m_symblID;
	menuItemDrawingStyles_t mdsType;		// my draw-as type
	menuHist_t				class_history;	// the history I pass to my children (stevev 01aug05)
	//pnt_t topLeft;
	//pnt_t rectSize;
	hv_location_t           m_MyLoc;
	int						m_itemIndex;
	
	// for menues------------------------
	//hCmenuTree*             m_pMenuTree;  // points into the top level's tree,
										  //   do not destroy unless m_bIsAroot (you don't own it)
	hCmenuTree*				m_pStartTree; // the tree after execute (from menu or button)
	hCmenuTree*             m_pNewTree;   // recalc's different tree or NULL if the same(unused)
	hCmenuTree*				m_pHoldTree;  // temporary try

	CdrawBase*              m_pParentDraw;

	UINT m_nPage;  // page number the object lives on (if non-zero), for cdrawTab it is the number of child pages that live on it
	CDynControls*           m_pDynTabCtrl;

    hCreference *           m_pItemRef;     // Copy of reference to menu item so we can get the 
    						//   label when referenced via a collection/array, POB, 13 Aug 2008
    
	// no longer used 11sep07  BOOL					m_bTriggerEdit;
	BOOL					m_bIsReadOnly;  // Qualifier READ_ONLY
	BOOL					m_bIsNoLabel;   // Qualifier NO_LABEL
	BOOL					m_bIsNoUnit;    // Qualifier NO_UNIT
	BOOL					m_bIsInline;    // Qualifier 
	BOOL					m_bIsDisplayVal;// Qualifier
	BOOL					m_bIsReview;    // Qualifier 
	BOOL                    m_bIsOverLine;	// Previous was Separator on a table

	BOOL					m_bIsAroot;		// true for those items on MAIN
	BOOL					m_bIsAbutton;	// when this is represented as a button
	// stevev 6sep07-add item-specific focus messages
	unsigned				Set_Focus_Msg;// zero unless control has focus messages
	unsigned				Kil_Focus_Msg;

	virtual ITEM_ID FillSymb(hCreference& rRef);

	virtual /* void  FillSize(void) = 0;-- most used default */
	void FillSize(void)	{m_MyLoc.da_size.xval=m_MyLoc.da_size.yval = 1;};

	virtual int   FillSelf(ulong selfSymID,menuHist_t hist,hCmenuTree* pMTre) RPVFC( "CdrawBaseB",0 );// does it all now)

	virtual 
		void createControl(CWnd * pWnd, pnt_t  topLeftParent, int centeringOffset = 0);
	virtual void createMenu(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    virtual void Execute(WORD wMessageID, LPARAM lParam = 0);  // Added LPARAM parameter, POB - 5/5/2014
	virtual void Destroy(ulong symblID);
	virtual void DeleteMenu(CdrawBase * pChild){}; // Name change, function deletes memory, POB - 5 Aug 2008 
	virtual bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	virtual void LoadContextMenu(CMenu *pMenu, int& nPos);
	virtual void Edit(){};
	virtual void Set(){};
	
	virtual BOOL Pre(WORD wMessageID);
	virtual BOOL Post(WORD wMessageID);

	virtual CString GetLabel();
	virtual CString GetHelp(); //Vibhor 240904: Added, POB - 6 May 2005, made virtual 
	virtual CRect   GetRect();

	hCitemBase* GetItem();
    BOOL ExecuteCID(WORD wControlID, WORD wMessageID, LPARAM lParam = 0);  // Added LPARAM parameter, POB - 5/5/2014
	BOOL DisplayHelp(CPoint point);
    BOOL DisplayLabel(CPoint point);
    BOOL DisplayValue(CPoint point);
    virtual BOOL DisplayValue();
    
	void UpdateAll();
	CdrawBase * HitTest(CPoint point);
	virtual BOOL Cancel(ulong SymID);
	int GetIndex();
	void SetIndex(int itemIndex);
	BOOL IsValid();
	virtual void setProtection(long SymID,bool makeRebuilding){return;};//MT except layout
    virtual void Enable(BOOL bEnable = TRUE); 
    virtual BOOL IsEnabled();

#ifdef DCDEBUG
	char fbuf[1024];
	virtual void dumpSelf(CFile& fi, int spaces);
#endif
#ifdef DO_PUBLISH
	RETURNCODE publish(dataStructList_t dsL);
#endif
};


/*************************************************************************************************
 * The following nine classes are implemented in SDC625_drawMenu.cpp                  
 *
 ************************************************************************************************/
// layout is essentially all menues or menu-like structures
class CdrawLayout : public CdrawBase
{
public:
	CdrawLayout(menuItemDrawingStyles_t mdt, CdrawBase* pParent) : CdrawBase(mdt,pParent) 
	{	rebuilding=false;};
	virtual ~CdrawLayout()  {};// MT for now

	bool rebuilding;// true to stop messages/stales going down

public:
	int  FillSelf(ulong selfSymID,menuHist_t hist,hCmenuTree* pMTre);// does it all now
	bool Chk_Self(ulong selfSymID,menuHist_t hist);//04dec06 - resize check
	//void createControl(CWnd * pWnd, pnt_t topLeftParent);
	void createMenu(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
	void  staleDynamics(void);
	/*sjv 04dec06 resize support */
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void setProtection(long SymID,bool makeRebuilding);
	int  tree2List(hCmenuTree* ptree, menuHist_t& childrenHist);// helper

	virtual bool Rebuild(long SymID, CWnd *m_pWindow);// returns size-changed
};
 
class CdrawWin : public CdrawLayout	//was CdrawBase
{
public:
	CWnd *      m_pWindow; // This points to the frame window that is opened

public:
	CdrawWin(CdrawBase* pParent) : CdrawLayout(mds_window,pParent){m_pWindow = NULL; };
	virtual ~CdrawWin();
	void FillSize(void);
	// use base class'   int  FillSelf(ulong selfSymID);
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	int  Arrange(ulong symID);
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Rebuild(long SymID, CWnd *m_pWindow);// returns size-changed
};


class CdrawDlg : public CdrawLayout  // was CdrawBase
{
public:
	TCHAR * m_pButtonTxt;
	int    m_exButtonCnt;
//	HWND     hWnd;
	int    m_isReady;
	CMainFrame* m_pMnFrm;
	
	BOOL m_bAbortBtnClicked;	// same as cancel
	BOOL m_bNextBtnClicked;		// next or first button
	BOOL m_bButOneClicked;		// possible extra buttons
	BOOL m_bButTwoClicked;
	BOOL m_bButThrClicked;
    BOOL m_bSingleMenuItem;     // Ignore Layout rules - start in 1st column

public:
	CdrawDlg(CdrawBase* pParent);
	virtual ~CdrawDlg();
	CDDIdeDoc* GetDocument(){return CdrawBase::GetDocument();};
	void FillSize(void);
	// use base class'   int  FillSelf(ulong selfSymID);
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID = 0, LPARAM lParam = 0 );
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Rebuild(long SymID,CWnd * m_pWindow);// returns size-changed
};

// added a couple

class CdrawTab : public CdrawLayout  // was CdrawBase
{
public:	

public:
//	CdrawTab() : CdrawBase(mds_tab){m_nPage = 0;};
	CdrawTab(CdrawBase* pParent) : CdrawLayout(mds_tab,pParent){m_nPage = 0;};
	void FillSize(void);
	int  FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) { return 0; };/* this does nothing (psuedo item) */ 

	/* Tab only method */
	RETURNCODE  AddPage(hCmenuItem& mi );
	RETURNCODE  AddPage(hCmenuTree* pmt);
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Rebuild(long SymID, CWnd *pWindow);// returns size-changed
};


class CdrawPage : public CdrawLayout  // was CdrawBase
{
public:

public:
	CdrawPage(CdrawBase* pParent) : CdrawLayout(mds_page,pParent){};
	virtual ~CdrawPage();
	void FillSize(void);
	// use base class'   int  FillSelf(ulong selfSymID) ;
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
	// use parent's (CdrawLayout)   void  staleDynamics(void);// returns size-changed
	bool Rebuild(long SymID, CWnd *m_pWindow);// returns size-changed
};


class CdrawGroup : public CdrawLayout  // was CdrawBase
{
public:
	unsigned myPixelOffset;// groups that are centered must pass down the offset

public:
	CdrawGroup(CdrawBase* pParent) : CdrawLayout(mds_group,pParent),myPixelOffset(0){};
	void FillSize(void);
	// use base class'   int  FillSelf(ulong selfSymID);
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
	// use parent's (CdrawLayout)   void  staleDynamics(void);// returns size-changed
	bool Rebuild(long SymID, CWnd *m_pWindow)// returns size-changed
	{	LOGIT(CERR_LOG,"       We got into a Group's Rebuild.\n");
		return false;
	};
};


class CdrawMenu : public CdrawLayout	//was CdrawBase
{
public:
	CMenu * m_pMenu;  // pointer to parent CMenu for addition of pop-up: POB - 15 Sep 2004

public:
	CdrawMenu(CdrawBase* pParent) : CdrawLayout(mds_menu,pParent){m_pMenu = NULL;};
	// use base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;}; // POB - 2 Sep 2004
	// use base class'   int FillSelf(ulong selfSymID); // POB - 2 Sep 2004
	// \/ paulb 18aug08 \/
    int  FillButton();  // Special version of FillSelf filling drawMenu living WINDOW/DIALOG, POB - 5 Aug 2008
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0); 
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	void DeleteMenu(CdrawBase * pChild); // Name change, function deletes memory, POB - 5 Aug 2008
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Rebuild(long SymID, CWnd *m_pWindow);// returns size-changed
};


class CdrawTbl : public CdrawLayout	//was CdrawBase
{
public:
	CWnd * m_pTable;	// This point to the frame window that is opened
	
public:
//	CdrawTbl() : CdrawBase(mds_table){m_pTable = NULL;};
	CdrawTbl(CdrawBase* pParent) : CdrawLayout(mds_table,pParent){m_pTable = NULL;};
	virtual ~CdrawTbl();
	void FillSize(void);
	/* change to use CdrawLayout's version 14mar11
	int FillSelf(ulong selfSymID,menuHist_t hist,hCmenuTree* pMTre);//moved2 drawLayout 14mar11
	**/
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Rebuild(long SymID);// returns size-changed
};



class CdrawMain : public CdrawMenu
{
public:
	void AddMenus(CWnd * pWnd);
	void DeleteAllMenus(); // Name change, function deletes memory, POB - 5 Aug 2008

	ulong m_symToSep; // symbol number of 1st menu item that lives on view menu (if applicable) 
	CArray<int,int> m_ViewSymList;

public:
	CdrawMain(CdrawBase* pParent=NULL) : CdrawMenu(pParent){m_symToSep=0;};
	void FillSize(void);
	int  FillSelf(ulong selfSymID,menuHist_t hist, hCmenuTree* pMTre);// does it all now
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
	void Destroy(ulong symblID);
	void DeleteMenu(CdrawBase * pChild); // Name change, function deletes memory, POB - 5 Aug 2008
	// use parent's (CdrawLayout)   void  staleDynamics(void);
	bool Update(long type=-1,long SymID = -1);// update entry point
	void setProtection(long SymID,bool makeRebuilding);
	bool Rebuild(long SymID, CWnd *m_pWindow)		// returns size-changed
	{	LOGIT(CERR_LOG,"       We got into Main's Rebuild.\n");
		return false;
	};

	// helper function(s)
	/* no longer used  bool Rebuild(CdrawBase * pB) ; */
};

/*************************************************************************************************
 * end of classes in SDC625_drawMenu.cpp     
 ************************************************************************************************/
 

class CdrawVar : public CdrawBase
{
public:
	CString ReadVal();
	void WriteVal();
	CString ReadEditFormatVal();
	// stevev - WIDE2NARROW char interface   
//	string GetEditFormat();  // Device object now creates Edit Format, POB - 1 Aug 2008
	void FormatFloat(CString, CString, CString & );
	void SetupType(CWnd * pWnd);
	
	ulong m_nUnitID;
	ulong m_nLabelID;

	bool  amReturning; // set while handling a VK_RETURN msg.  See Execute()

public:
	CdrawVar(CdrawBase* pParent) : CdrawBase(mds_var,pParent),amReturning(false),m_nLabelID(0),
		m_nUnitID(0)
	{};

	~CdrawVar();
	// use basevoid FillSize(void)	{rectSize.xval=rectSize.yval = 1;};
	int  FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
					{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; };
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void LoadContextMenu(CMenu *pMenu, int& nPos);
    BOOL DisplayValue();
	void Edit();
	BOOL Pre(WORD wMessageID);
	BOOL Post(WORD wMessageID);
	CRect GetRect();
	void staleDynamics(void);
    void Enable(BOOL bEnable = TRUE);
    BOOL IsEnabled();
};

class CdrawIndex : public CdrawBase
{
public:
	CString ReadVal();
	void WriteVal();
	void GetValidValueList(CStringArray& enumList);

	ulong m_nLabelID;

public:
	CdrawIndex(CdrawBase* pParent) : CdrawBase(mds_index,pParent),m_nLabelID(0){};

	~CdrawIndex();
	//use base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;};
	int  FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
							{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; };
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void LoadContextMenu(CMenu *pMenu, int& nPos);
    BOOL DisplayValue();
	void Edit();
	BOOL Pre(WORD wMessageID);
	BOOL Post(WORD wMessageID);
	CRect GetRect();
	void staleDynamics(void);
    void Enable(BOOL bEnable = TRUE);
    BOOL IsEnabled();
};


class CdrawStr : public CdrawBase
{
public:
	CString m_constString;

public:
	CdrawStr(CdrawBase* pParent) : CdrawBase(mds_constString,pParent){};
	ITEM_ID FillSymb(hCreference& rRef);
	void    FillSize(void);//	{rectSize.xval=rectSize.yval = 1;}; // POB - 14 Oct 2004
	int     FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
			{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; }; // POB - 14 Oct 2004
	void    createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0); 
	CString GetLabel() { return m_constString; };
    void LoadContextMenu(CMenu *pMenu, int& nPos);
	void staleDynamics(void){/*do nothing*/};
};

class CdrawBlank : public CdrawBase
{
public:
	CdrawBlank(CdrawBase* pParent) : CdrawBase(mds_blank,pParent){};
	ITEM_ID FillSymb(hCreference& rRef){ m_symblID = 0; return 0; }; // SJV - 13feb15
	int     FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
											{ m_symblID = 0; return 1; }; // SJV - 13feb15
	CString GetLabel() { return ""; };
	void staleDynamics(void){/*do nothing*/};
};

class CdrawEdDisp : public CdrawBase
{
public:

public:
	CdrawEdDisp(CdrawBase* pParent) : CdrawBase(mds_edDisp,pParent){};
	//use base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;}; // POB - 2 Sep 2004
	int FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
			{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; }; // POB - 2 Sep 2004
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	void staleDynamics(void) { /* done inside dialog by OnTimer*/; };
};


class CdrawEnum : public CdrawBase
{
public:
	CString ReadVal();
	void WriteVal();
	void GetEnumList(CStringArray& enumList);

	ulong m_nLabelID;  // POB - 2 Sep 2004

public:
	CdrawEnum(CdrawBase* pParent) : CdrawBase(mds_enum,pParent),m_nLabelID(0){};
	~CdrawEnum();
	//use base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;}; // POB - 2 Sep 2004
	int FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
			{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; }; // POB - 2 Sep 2004
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0); 
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void LoadContextMenu(CMenu *pMenu, int& nPos);
    BOOL DisplayValue();
	void Edit();
	BOOL Pre(WORD wMessageID);
	BOOL Post(WORD wMessageID);
	CRect GetRect();
	void staleDynamics(void);
    void Enable(BOOL bEnable = TRUE);
    BOOL IsEnabled();
};


class CdrawBitEnum : public CdrawLayout  // try -- was: CdrawBase
{
public:
	void GetEnumList(CStringArray& enumList);
	
	ulong m_nLabelID;  // POB - 8 Sep 2004

public:
	CdrawBitEnum(CdrawBase* pParent) : /*CdrawBase*/CdrawLayout(mds_bitenum,pParent),m_nLabelID(0){};
	~CdrawBitEnum();
	void FillSize(void); // POB - 31 Mar 2005
	// use new base class   int FillSelf(ulong selfSymID,menuHist_t hist);// does it all now// POB - 31 Mar 2005
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);   
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void LoadContextMenu(CMenu *pMenu, int& nPos);
    BOOL DisplayValue();
	void staleDynamics(void);
};


class CdrawBit : public CdrawBase
{
public:
	BOOL ReadVal();
	void WriteVal();
	
	ulong m_nLabelID;  // POB - 16 Sep 2004

public:
	CdrawBit(CdrawBase* pParent) : CdrawBase(mds_bit,pParent),m_nLabelID(0){};
	// use base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;}; // POB - 16 Sep 2004
	int FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
				{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; }; // POB - 7 Sep 2004
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);   
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	void Enable(BOOL bEnable); // stevev 19sep14 - inserted to remove the error message
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
    void LoadContextMenu(CMenu *pMenu, int& nPos);
    BOOL DisplayValue();
	void Edit();
	BOOL Pre(WORD wMessageID);
	CString GetLabel();
	CString GetHelp();
	CRect GetRect();
	void staleDynamics(void);
};


class CdrawMeth : public CdrawBase
{
public:

public:
	CdrawMeth(CdrawBase* pParent) : CdrawBase(mds_method,pParent){};
	// usae base void FillSize(void)	{rectSize.xval=rectSize.yval = 1;}; // POB - 2 Sep 2004
	int FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
				{ if ( selfSymID != 0) m_symblID = selfSymID; return 1; }; // POB - 2 Sep 2004
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0); 
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	void staleDynamics(void){/*do nothing*/};
};


class CdrawImage : public CdrawBase
{
public:
	CxImage* m_pImage;
	CdrawBase * m_pBase;	// This points to a draw object - POB, 14 Oct 2005 

public:
	CdrawImage(CdrawBase* pParent) : CdrawBase(mds_image,pParent), m_pImage(NULL) {m_pBase = 0;};
	~CdrawImage();			// POB - 14 Oct 2005

	// changed 1Apr05 sjv ITEM_ID FillSymb(hCreference& rRef);
	void FillSize(void);
	int  FillSelf(ulong selfSomething,menuHist_t pHst,hCmenuTree* pMTre);
						// changed 1Apr05 sjv { return 0; };//everything done in FillSymb
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0); 
    void Execute(WORD wMessageID, LPARAM lParam = 0);
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void staleDynamics(void){/*do nothing*/};
};


class CdrawScope : public CdrawBase	// charts and graphs
{
	unsigned H, W;

public:

public:
	CdrawScope(CdrawBase* pParent) : CdrawBase(mds_scope,pParent), H(DFLTHGT), W(DFLTWID){};
	void FillSize(void);  //{/*TODO*/};
	int  FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre) 
				{/* TODO: implement */ return 0; };
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);
									//Vibhor 311204: Start of Code
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
	void staleDynamics(void);//stevev 10may07-execute at graph..was::>{/*uses pub-sub*/};
};


class CdrawSep : public CdrawBase
{
	bool isRowBreak;
public:

public:
	CdrawSep(bool rowbrk = 0,CdrawBase* pParent=NULL ) 
		: CdrawBase((rowbrk)?mds_newline:mds_separator , pParent){isRowBreak=rowbrk;};
	void FillSize(void)	{m_MyLoc.da_size.clear();};//  rectSize.xval=rectSize.yval = 0;};
	int  FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre)  { return 0; };/* this does nothing (psuedo item) */
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0) 
		{ return; };/* this does nothing */
	void staleDynamics(void){/*do nothing*/};
};

class CdrawGrid : public CdrawBase
{
public:
	void InitGrid();

    CDynControls * m_pPsudoDynCtrl; // m_pPsudoDynCtrl added to support the psudo drawbase children, POB - 5/6/2014

public:
    CdrawGrid(CdrawBase* pParent) : CdrawBase(mds_grid,pParent){m_pPsudoDynCtrl = NULL;};
	~CdrawGrid();
	void FillSize(void);
	int FillSelf(ulong selfSymID,menuHist_t pHst,hCmenuTree* pMTre);
	void createControl(CWnd * pWnd, pnt_t topLeftParent, int centeringOffset = 0);
    void Execute(WORD wMessageID, LPARAM lParam = 0);  // Added LPARAM parameter, POB - 5/5/2014
	bool Update(long type=-1,long SymID = -1);// returns true if size/existance changed
    void staleDynamics(void);  // Added to support class DYNAMIC VARIABLEs, POB - 5/1/2014
};


#endif /* _DRAW_TYPES_ */
