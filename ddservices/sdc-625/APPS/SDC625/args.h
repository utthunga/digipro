/*************************************************************************************************
 *
 * $Workfile: args.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		sets up the arguments for this particular application
 *		created 2/6/2 from code from the code page - see original
 * #include "args.h".		
 */

#ifdef ARGUMENTS_H	/* we are included in Arguments.h just for the defines */

#define INI_FILE_SECTION _T( "Settings"	)/* location in app.ini file ie: [DEFAULT_VALUES] */

#else  /* the rest of the file */
#pragma warning (disable : 4786) 
#include "Arguments.h"
#include <iostream>

using namespace std;


#define DBDD_PATH   "\\hcf\\ddl\\library"	/* "C:\\hcf\\ddl\\dev"      / * default path */

// instantiation sequence a) command line b) ini file/registry, c) environment, d) default val

// the strings we use in the environment and/or the ini file
// default to _T("") if you want to undefine 'em
#define NOT_SUPPLIED	_T("NoT_SuPpLiEd")
// cout file
#define ARG_STDOUT_INI	_T("SDC_STDOUT")
#define ARG_STDOUT_ENV	_T("SDC_STDOUT_FILE")
#define ARG_STDOUT_DFLT	NOT_SUPPLIED
// clog file
#define ARG_STDLOG_INI	_T("SDC_STDLOG")
#define ARG_STDLOG_ENV	_T("SDC_STDLOG_FILE")
#define ARG_STDLOG_DFLT	NOT_SUPPLIED
// cerr file
#define ARG_STDERR_INI	_T("SDC_STDERR")
#define ARG_STDERR_ENV	_T("SDC_STDERR_FILE")
#define ARG_STDERR_DFLT	NOT_SUPPLIED
// url 
#define ARG_URL_INI		_T("SearchSerialPort")
#define ARG_URL_ENV		_T("SDC_URL")
#define ARG_URL_DFLT	_T("COM1")
// DD release Directory
#define ARG_RELEASE_INI		_T("RELEASE_DIR")			/* not defined now */
#define ARG_RELEASE_ENV		_T("TOK_RELEASE_DIR")
#define ARG_RELEASE_DFLT	_T( DBDD_PATH )
// PollingAddress
#define ARG_POLLADR_INI		_T("SearchPollAddr")
#define ARG_POLLADR_ENV		_T("SDC_POLL_ADDRESS")
#define ARG_POLLADR_DFLT	_T("255")					/* new default 7sep05 EMPTY_POLL_ADDR was: "0" */
// Lit18 directory
#define ARG_LIT18_INI		_T("LIT18_DIR")				/* not defined now */
#define ARG_LIT18_ENV		_T("SDC_LIT18_DIR")
#define ARG_LIT18_DFLT		_T(".\\devicedocs")
// Modem Option
#define ARG_MODEM_INI		_T("MODEM_TYPE")			/* not defined now */
#define ARG_MODEM_ENV		_T("SDC_MODEM_TYPE")
#define ARG_MODEM_DFLT		_T("HART")
// host address 
#define ARG_HSTADR_INI		_T("HOST_ADDRESS")			/* not defined now */
#define ARG_HSTADR_ENV		_T("SDC_HOST_ADDRESS")
#define ARG_HSTADR_DFLT		_T("2")
// restrictiveness 
#define ARG_RESTRICT_INI		_T("RESTRICTIVNESS")
#define ARG_RESTRICT_ENV		_T("SDC_RESTRICTION")
#define ARG_RESTRICT_DFLT		_T("S")/*'S'tandard, 'L'ibral */
// simulation
//-- no parameters
/** from here down are not currently supported **/
// force faceplate
//-- no parameters
// language
#define ARG_LANG_INI		_T("LANGUAGE_CODE")			/* not defined now */
#define ARG_LANG_ENV		_T("SDC_LANG_CODE")
#define ARG_LANG_DFLT		_T("|en|")
// field device identification
#define ARG_FULLID_INI		_T("FULL_DEV_IDENT")		/* not defined now */
#define ARG_FULLID_ENV		_T("SDC_FULL_IDENT")
#define ARG_FULLID_DFLT		NOT_SUPPLIED
/*** removed 07feb07 going to hart 7 - use field device id above
// ddkey field device identification
#define ARG_KEYID_INI		_T("KEY_DEV_IDENT")			
#define ARG_KEYID_ENV		_T("SDC_KEY_IDENT")
#define ARG_KEYID_DFLT		_T("26800500")
/*** end removal ****/
// logging permissions
#define ARG_LOGGING_INI     _T("LOGGING_LEVEL")	
#define ARG_LOGGING_ENV     _T("SDC_LOGGING_LEV")
#define ARG_LOGGING_DFLT    _T("0x00000000")
//-V use HART server if available - false == Do not use HART server unless you have to
#define ARG_USEHARTSVR_INI	_T("USE_HART_SERVER_IF_AVAILABLE") /* 0 or 1 */
#define	ARG_USEHARTSVR_ENV  _T("SDC_TRY2USE_HART_SERVER")
#define ARG_USEHARTSVR_DFLT _T("1") /*true - server has priority*/

// changed to single macro stevev 13apr15...SUPPORTED_VERSION_MAX...in ddbDeviceMgr.h
#define ARG_MAXVER_INI		_T("MAX_VERSION_SUPPORT")
#define ARG_MAXVER_ENV		_T("SDC_MAX_VER_2_LOAD")
#define ARG_MAXVER_DFLT		XSTR(SUPPORTED_VERSION_MAX)
#define XSTR(X) ARG_STRINGIFIER(X)
#define ARG_STRINGIFIER(s)  L ## #s

/***********  not a command line option - but an environment variable ******/
#define FILE_STORE_ENV _T("SDC_FILE_DIR") /* is where the DD File construct stores/fetches itself**/
/*****  defaults to  'theArgs['B']["databaseDirectory"]\..\Files'         **/

/********************* used ****************
ABCDEFGHILMNOPRSTUV
********************used in xmtr************
KYQ
*********************available**************
JWXZ
********************************************/


class DDSargs : public Arguments
{
public:
	DDSargs( tstring strCommandName, tstring strDescription=_T(""), tstring strOptionmarkers=_T("-"))
										:Arguments( strCommandName, strDescription, strOptionmarkers )
	{	// Add option "-h" with explanation...
		AddOption(_T("h|H|?"), _T("display usage help"), true);	// true ==> this is the 'Usage' output option

		// Add option "-o" with non optional parameter
		{
			Arguments::Option	cOpt( _T("O|0|o"), _T("output filespec") );
			cOpt.AddRequiredArgument( _T("outputFileName"),				// index name
									  _T("file name with optional path"), // description (help)
										ARG_STDOUT_INI,					// ini
										ARG_STDOUT_ENV,					// env
										ARG_STDOUT_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-G" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("G|g"), _T("file spec for the logging information") );
			cOpt.AddRequiredArgument(_T("logFileName"),				// index name
						_T("log file name with optional path\n\t\t\tNOTE: -G option is for testing only"),
										ARG_STDLOG_INI,					// ini
										ARG_STDLOG_ENV,					// env
										ARG_STDLOG_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-E" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("E|e"), _T("file spec for the error information") );
			cOpt.AddRequiredArgument(_T("errorFileName"),				// index name
					_T("error log file name with optional path\n\t\t\tNOTE: -E option is for testing only"),
										ARG_STDERR_INI,					// ini
										ARG_STDERR_ENV,					// env
										ARG_STDERR_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}


		// Add option "-U" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("U|u"), _T("communication 'url'") );
			cOpt.AddRequiredArgument(_T("CommPortPath"),					// index name
							_T("communications port ie.COM1|COM2|COMx"),// help
										ARG_URL_INI,					// ini
										ARG_URL_ENV,					// env
										ARG_URL_DFLT );					// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-B" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("B|b"),_T("Device Library directory") );
			cOpt.AddRequiredArgument(_T("databaseDirectory"),			// index name
					_T("the 'Release' directory (with dictionaries)"),	// help
										ARG_RELEASE_INI,				// ini
										ARG_RELEASE_ENV,				// env
										ARG_RELEASE_DFLT );				// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-p" with a non optional parameter...is expected to be used with the 'url'
		{
			Arguments::Option	cOpt( _T("P|p"), _T("Polling Address") );
			cOpt.AddRequiredArgument(_T("PollAddr"),			// index name
						_T("Polling address (0 to 15)"),	// help
										ARG_POLLADR_INI,	// ini
										ARG_POLLADR_ENV,	// env
										ARG_POLLADR_DFLT );	// default value if not in ini||env
				// timj begin 04/05/04  address 255 signals no command line choice
			    //cOpt.AddArgument( "", ,_T(""),"255" );
				// timj end 04/05/04
			AddOption( cOpt );  
		}
		// Add option "-M" with a non optional parameter...supporting lit18
		{
			Arguments::Option	cOpt( _T("M|m"), _T("lit18 help directory") );
			cOpt.AddRequiredArgument(_T("clHelpDir"),			// index name
				_T("Directory storing the lit18 *.pdf files"),	// help
										ARG_LIT18_INI,			// ini
										ARG_LIT18_ENV,			// env
										ARG_LIT18_DFLT );		// default value if not in ini||env
			AddOption( cOpt );
		}
		// Add option "-C" with a non optional parameter...is expected to be used with the 'url'
		{
			Arguments::Option	cOpt( _T("C|c"), _T("Modem Type Selection") );
			cOpt.AddRequiredArgument(_T("Modem "),			// index name
										_T("HART | HONHARTDE "),// help
										ARG_MODEM_INI,		// ini
										ARG_MODEM_ENV,		// env
										ARG_MODEM_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		} /* VMKP added */
		// Add option "-A" with a optional parameter...host address
		{
			Arguments::Option	cOpt( _T("A|a"), _T("Host Address") );
			cOpt.AddRequiredArgument(_T("HostAddr "),			// index name
			_T("Host address (1 or 2: Primary or Secondary)"),	// help
										ARG_HSTADR_INI,		// ini
										ARG_HSTADR_ENV,		// env
										ARG_HSTADR_DFLT );	// default value if not in ini||env
			AddOption( cOpt );  
		}
		// Add option "-S" with no parameters
		{
			AddOption(_T("S|s"), _T("connect to simulated XMTR-DD"), false);	
		}


		// Add option "-F" with no parameters
		{
			AddOption(_T("F|f"), _T("force a faceplate Display"), false);	
		}
		// Add option "-l" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("L|l|1"), _T("language") );
			cOpt.AddRequiredArgument(_T("langCode"),			// index name
				_T("ISO 639 specified language descriptor string"),	// help
										ARG_LANG_INI,		// ini
										ARG_LANG_ENV,		// env
										ARG_LANG_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		}		
		//wants -i 'hartrev/mfg/devtype/devrev/devid'
		// Add option "-i" with a non optional parameter...
		{
			Arguments::Option	cOpt( _T("I|i"), _T("identification of field device") );
			cOpt.AddRequiredArgument(_T("FldID "),			// index name
  _T("h/[mm]mm/[tt]tt/rr/D format where:\n\t\th\tthe HART revision level (5|6)\n")
 _T("\t\tmm\ttwo/four hex digits of the Manufacturer ID\n")
 _T("\t\ttt\ttwo/four hex digit device type code\n")
 _T("\t\trr\ttwo hex digit device revision number\n\t\tD\tthe device's unique ID"),
										ARG_FULLID_INI,		// ini
										ARG_FULLID_ENV,		// env
										ARG_FULLID_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		}
		/*** removed 07feb07 going to hart 7 - use field device id above
		// Add option "-K" with a non optional parameter...
		{
			Arguments::Option	cOpt( "K|k", "DDkey" );
			cOpt.AddRequiredArgument(	"clDdkey ",			// index name
						"DDkey string for device identity",	// help
										ARG_KEYID_INI,		// ini
										ARG_KEYID_ENV,		// env
										ARG_KEYID_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		}
		*** end removal 07feb07 ***/

		// Add option "-D" with a non optional parameter...---undocumented HART utility
		{
			Arguments::Option	cOpt( _T("D|d"), _T("DDdir") );
			cOpt.AddOptionalArgument(_T("DDdirFile"),		// index name
					_T("Generate a directory listing and exit"),	// help
										_T("DIRFILE"),		// ini
										_T("DIRFILE"),		// env
										_T(".\\ddDir.txt") );	// default value if not in ini||env
			AddOption( cOpt );
		}

		// Add option "-R" with a non optional parameter...---undocumented HART restriction
		{
			Arguments::Option	cOpt( _T("R|r"), _T("Restrictiveness") );
			cOpt.AddOptionalArgument(_T("Restriction"),		// index name
					_T("Restrictiveness Level(aka Compatability Mode)"),	// help
										ARG_RESTRICT_INI,		// ini
										ARG_RESTRICT_ENV,		// env
										ARG_RESTRICT_DFLT );	// default value if not in ini||env
			AddOption( cOpt );
		}

		// Add option "-T" with a non optional parameter...---undocumented HART logging
		{
			Arguments::Option	cOpt( _T("T|t"), _T("Log Permission" ));
			cOpt.AddOptionalArgument(_T("Log_Permission"),		// index name
					_T("Bit-Enum for Logging Level"),	// help
										ARG_LOGGING_INI,		// ini
		/*MUST use 0x...*/				ARG_LOGGING_ENV,		// env
										ARG_LOGGING_DFLT );	   // default value if not in ini||env
			AddOption( cOpt );
		}

		// Add option "-V" with a non optional parameter...---1 = true- Use HART server if avalable
		{//													  0 = false Suppress HART Server loading unless modem server not there
			Arguments::Option	cOpt( _T("V|v"), _T("Use HART server if available") );
			cOpt.AddOptionalArgument(_T("HARTServerFirstChoice"),		// false = Use Modem Server if possible
					_T("1 = Use HART server if available, 0 = Use Modem Server If available"),	// help
										ARG_USEHARTSVR_INI,		// ini
										ARG_USEHARTSVR_ENV,		// env
										ARG_USEHARTSVR_DFLT );  // default value if not in ini||env
			AddOption( cOpt );
		}

		// Add option "-N" with a non optional parameter...---undocumented max effVersion to support
		{
			Arguments::Option	cOpt( _T("N|n"), _T("Max EFF version to load" ));
			cOpt.AddOptionalArgument(_T("Max_EFF_Ver"),		// index name
					_T("5 to 10 version number"),	// help
										ARG_MAXVER_INI,		// ini
		                				ARG_MAXVER_ENV,		// env
										ARG_MAXVER_DFLT );	   // default value if not in ini||env
			AddOption( cOpt );
		}

		// no straight command line arguments on this command line
	};
	/* we are going to override this for awhile */
	void CopyFromRegistry(void);

};

#endif  /* ARGUMENTS_H */
	
/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: args.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */	
