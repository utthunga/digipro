/*************************************************************************************************
 *
 * $Workfile: DDLMain.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      interface for the CDDLMain class.
 * #include "DDLMain.h".        
 */

#if !defined(AFX_DDLMAIN_H__4F2A6293_567F_4C54_8AB8_D30290255902__INCLUDED_)
#define AFX_DDLMAIN_H__4F2A6293_567F_4C54_8AB8_D30290255902__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DDLBase.h"

/* class CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */

/*class CDDLMain : public CDDLBase  
{
    DECLARE_DYNAMIC(CDDLMain)

public:
    CDDLMain(hCitemBase* pIB=NULL);
    virtual ~CDDLMain();

    virtual BOOL IsTop() { return TRUE; };// override  DDLBase
// Special menu handling
    void Execute();
    void LoadContextMenu(CMenu *pMenu, int& nPos);

};*/

#endif // !defined(AFX_DDLMAIN_H__4F2A6293_567F_4C54_8AB8_D30290255902__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDLMain.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
