#if !defined(AFX_FLASH_H__225852FC_D7B8_42C3_82F8_F4E771453157__INCLUDED_)
#define AFX_FLASH_H__225852FC_D7B8_42C3_82F8_F4E771453157__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Flash.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFlash dialog

class CFlash : public CDialog
{
// Construction
public:
	CFlash(CWnd* pParent = NULL);   // standard constructor
	CBrush m_bkBrush;
	CBrush m_StaticBrush;
	CBrush m_EditBrush;
	

// Dialog Data
	//{{AFX_DATA(CFlash)
	enum { IDD = IDD_FLASH };
	CStatic	m_Head;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFlash)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	CEdit *pEdit;
	CStatic *static_txt;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFlash)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLASH_H__225852FC_D7B8_42C3_82F8_F4E771453157__INCLUDED_)
