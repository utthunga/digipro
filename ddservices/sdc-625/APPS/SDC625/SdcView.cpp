     // SdcView.cpp : implementation file
//

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"
#include "MainListCtrl.h"
#include "SdcView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdcView

IMPLEMENT_DYNCREATE(CSdcView, CView)

CSdcView::CSdcView():m_pTableFrame(NULL)
{
	//m_pCtrl = NULL;	
	m_pCtrl = new CMainListCtrl();
}

CSdcView::~CSdcView()
{
	if (m_pCtrl != NULL)
		delete m_pCtrl;
}


BEGIN_MESSAGE_MAP(CSdcView, CView)
	//{{AFX_MSG_MAP(CSdcView)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdcView drawing

void CSdcView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// NOTE: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CSdcView diagnostics

#ifdef _DEBUG
void CSdcView::AssertValid() const
{
	CView::AssertValid();
}

void CSdcView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSdcView message handlers
void CSdcView::OnInitialUpdate() 
{
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< sdcView.OnInitialUpdate base class update.\n");
	CView::OnInitialUpdate();
	if (m_pCtrl == NULL)
		return;
	// moved to the constructor....m_pCtrl = new CMainListCtrl();
	m_pCtrl->m_pTableFrame = m_pTableFrame;

	RECT rect;
	GetClientRect(&rect);

	if (m_pCtrl)
	{
		m_pCtrl->Create(LVS_OWNERDRAWFIXED | LVS_REPORT | LVS_SHAREIMAGELISTS | LVS_SINGLESEL, rect, this,1);
		LOGIF(LOGP_START_STOP)(CLOG_LOG,"< sdcView.OnInitialUpdate created, showing list view window.\n");
		m_pCtrl->ShowWindow(SW_SHOW);
	}

	//to fix PAR 5113
	//remove the header control style HDS_FULLDRAG so that the list control gets the HDN_ENDTRACK
	//message rather than the HDN_ITEMCHANGING message.
	BOOL bIsSet;
	if( SystemParametersInfo( SPI_GETDRAGFULLWINDOWS, 0, &bIsSet, 0 ) )
	{
		if( bIsSet )
		{
			HWND hWndHeaderCtrl = ::GetWindow( m_pCtrl->m_hWnd, GW_CHILD );
			if( hWndHeaderCtrl != NULL )
			{
				DWORD dwStyle = ::GetWindowLong( hWndHeaderCtrl ,GWL_STYLE );
			    dwStyle &= ~HDS_FULLDRAG;
			    ::SetWindowLong( hWndHeaderCtrl , GWL_STYLE, dwStyle );
			}
		}
	}	
	LOGIF(LOGP_START_STOP)(CLOG_LOG,"< sdcView.OnInitialUpdate Exit.\n");
		
}

void CSdcView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);

	if (m_pCtrl != NULL)
		m_pCtrl->MoveWindow (0, 0, cx, cy);
}


