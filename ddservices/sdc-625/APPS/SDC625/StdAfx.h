/*************************************************************************************************
 *
 * $Workfile: StdAfx.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 * #include "StdAfx.h"	
 */
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__32EA5F37_2A1D_4DEE_8C65_BBF4DE66CFD8__INCLUDED_)
#define AFX_STDAFX_H__32EA5F37_2A1D_4DEE_8C65_BBF4DE66CFD8__INCLUDED_
//#pragma message("StdAfx apps/sdc625")
// stevev - allow windows NT & 2000 capabilities
#if _MSC_VER < 1500
#define _WIN32_WINNT 0x0400
#else // 1500 or more 
#define _WIN32_WINNT 0x0501
#endif

//force logging if msg found - in release version too
#define FNDMSG 0x1305


/* The windows message constant for variable change WM_USER=0x400..normally*/
#define WM_SDC_VAR_CHANGED		 WM_USER + 10
#define WM_SDC_STRUCTURE_CHANGED WM_USER + 11
#define WM_SDC_VAR_CHANGEDVALUE	 WM_USER + 12
#define WM_SDC_CLOSEDEVICE		 WM_USER + 13
#define WM_SDC_GENERATEDEVICE    WM_USER + 14
#define WM_CREATE_EXTENDEDTAB	 WM_USER + 15  //Added by ANOOP
#define WM_UPDATE_EXTENDEDTAB	 WM_USER + 16  //Added by ANOOP
#define WM_DESTROY_CMD48DIALOG	 WM_USER + 17  //Added by ANOOP
#define WM_SDC_GLOW_GREEN		 WM_USER + 18  //Added by VMKP on 160204
#define WM_SDC_GLOW_RED			 WM_USER + 19  //Added by VMKP on 160204
#define WM_SDC_SIZECHILD         WM_USER + 20  //Added by POB on 3 Aug 2004
#define WM_SDC_ONWINDOW          WM_USER + 21  //Added by POB on 12 Aug 2004
#define WM_SDC_ONDIALOG          WM_USER + 22  //Added by POB on 8 Oct 2004

#define WM_DRAWMETHODUI			 WM_USER + 23  // was +1000 in methodDlg.cpp (sjv 5oct05)
#define WM_GETMETHODUSERINPUT    WM_USER + 24  // was +1001 in methodDlg.cpp (sjv 5oct05)

#define WM_DLG_ABORT             WM_USER + 25  //Added by sjv 5oct05
#define WM_DLG_OK                WM_USER + 26  //Added by sjv 5oct05
#define WM_DLG_BUT01             WM_USER + 27  //Added by sjv 5oct05-extra button displaymenu
#define WM_DLG_BUT02             WM_USER + 28  //Added by sjv 5oct05
#define WM_DLG_BUT03             WM_USER + 29  //Added by sjv 5oct05

#define WM_LINK_OBJ				 WM_USER + 30  // Added by sjv 7oct05
#define WM_ENABLE_WIN			 WM_USER + 31  // added sjv 27jun06
#define WM_SDC_WAITCLOSEDLG		 WM_USER + 32  // Added sjv 28sep06 (for showing a close window)
#define WM_SET_FOCUS_AGAIN       WM_USER + 33  // Added stevev 11sep07 (for regaining focus after pre-edit action with a UI)
#define WM_DO_EXECUTE            WM_USER + 34  // Added stevev 05aug08 we have to execute from
//												  outside the child-tree (so we can redraw the
//												  child tree at will)
#define WM_SDC_ELEMENT_DELETED	 WM_USER + 35  // Added stevev 23may11 a list element no longer
//												  exists, strip pointers to it in the UI

/*Vibhor 060204: Start of Code*/
#define WM_NOTIFY_RESPCODE		WM_USER + 500
/*Vibhor 060204: End of Code*/

/* stevev 140ct05 - central timer Registration !! */
#define VIEW_TIMER          1 
#define EDSPLY_TIMER        3
#define ERRLOG_TIMER		5 /* originally WM_USER + 200*/
#define FACE_PLATE_TIMER    6
#define GRID_TIMER          7
#define DLG_TIMER			8

#define ONE_SECOND       1000   // 1000 milliseconds (1 second)

/* stevev 25oct05 - AfxGetMainWnd()  returns the main window of the current thread
	Using it to get CMainframe assumes that you are always in the UI main thread
	- bad assumption
	- use the following macro instead
*/
#define GETMAINFRAME ((CMainFrame *)(AfxGetApp()->m_pMainWnd))
//                   
/* end of code */
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#pragma warning (disable : 4786)

#include <afxwin.h>         // MFC core and standard components
#undef _RICHEDIT_VER // allow a newer version
#include <richedit.h>
#include <afxext.h>         // MFC extensions
#include <afxcview.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxtempl.h>       // MFC Templates 
#endif // _AFX_NO_AFXCMN_SUPPORT



/***************************************************************************************/
/* Prasad start of code 05/19/03 */
#ifdef _UNICODE
#if _MSC_VER < 1400	/* VS8 (2005) defines this */
extern double _wtof(const TCHAR* a );
#endif
#ifndef __TCHAR_DEFINED
#define _ttof( a ) _wtof( a )
#endif
//#pragma message("Unicode defined")
#define tcerr	wcerr
#define tclog	wclog
#define tcout	wcout
#else
#define _ttof( a ) atof( a )
//#pragma message("Unicode NOT defined")
#define tcerr	cerr
#define tclog	clog
#define tcout	cout
#endif



extern TCHAR g_pchWindowName[];
/* End of code */

/* VMKP added on 030204*/
extern char g_chWriteEnable;
extern char g_chMenuInitialized;
/* VMKP added on 030204*/

#ifdef INC_DEBUG
#pragma message("In StdAfx.h") 
#endif

#include <string>
#include <algorithm>
#include <afxdisp.h>
using namespace std;

#ifdef INC_DEBUG
#pragma message("    Finished Includes::StdAfx.h") 
#endif
// stevev - WIDE2NARROW char interface
int __fastcall UnicodeToASCII(LPCWSTR pszW, LPSTR pRetA, int lenRetA);
int __fastcall UnicodeToASCII(CString &cstr, LPSTR pRetA, int lenRetA);
int __fastcall AsciiToUnicode(LPCSTR pszA,   LPWSTR pszW, int lenRetW);
int __fastcall AsciiToUnicode(LPCSTR pszA, CString& retStr);
int __fastcall decimalsRight(CString &cstr);


// format info..left will be max char, returns 1of duxifeg as type
//int getFormatInfo(string fmt, int& leftOdec, int& rightOdec, bool isEdit = false);


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__32EA5F37_2A1D_4DEE_8C65_BBF4DE66CFD8__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: StdAfx.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
