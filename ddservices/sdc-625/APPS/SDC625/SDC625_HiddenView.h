#if !defined(AFX_SDC625HIDDENVIEW_H__6C5FC81E_E99C_4FAB_936E_4122A99F7915__INCLUDED_)
#define AFX_SDC625HIDDENVIEW_H__6C5FC81E_E99C_4FAB_936E_4122A99F7915__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SDC625_HiddenView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSDC625HiddenView view

class CSDC625HiddenView : public CView
{
protected:
	CSDC625HiddenView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSDC625HiddenView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDC625HiddenView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSDC625HiddenView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CSDC625HiddenView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDC625HIDDENVIEW_H__6C5FC81E_E99C_4FAB_936E_4122A99F7915__INCLUDED_)
