/*************************************************************************************************
 *
 * $Workfile: DDIdeTreeView.cpp $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *      DDIdeView.cpp : implementation of the CDDIdeTreeView class
 */

#include "stdafx.h"
#include "SDC625.h"
#include "SDC625Doc.h"
#include "SDC625_drawTypes.h"
#include "SDC625TreeView.h"
#include "Server.h"
#include "DeviceDetails.h"
#include "DeviceStatus.h"
#include "FacePlate.h"
#include "logging.h"
#include "Command48_PropSheet.h"
#include "FileSupport.h"
#include "sdcDialog.h"
#include "LabelDialog.h"
#include "DDLexpansion.h"

/* Prasad start of code 05/19/03 */
TCHAR g_pchWindowName[1000];
/* end of code */
/*<START>For setting the window title in case of class tab view Anoop*/
TCHAR g_pchClassTabWndName[1000];
/* VMKP added on 030204*/
char g_chWriteEnable = FALSE;
char g_chMenuInitialized = FALSE;
/* VMKP added on 030204*/

/* <END> For setting the window title in case of class tab view Anoop */
extern CDDIdeApp theApp;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;

//#define STARTTIME_CHECK /* also in ddbDevice.cpp, ddbCommand.cpp, ddbDeviceMgr.cpp */
  #ifndef STARTSEQ_LOG
   #define STARTSEQ_LOG
  #endif
#endif


#ifdef _DEBUG
DWORD startOfInstantiation = 0;
DWORD lastTickCnt = 0, thisCnt = 0;
//26aug08 #define LOG_MSGS 1
//#define LOGBUILD 0
#endif
UINT Scan(LPVOID doc);
/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView

IMPLEMENT_DYNCREATE(CDDIdeTreeView, CTreeView)

BEGIN_MESSAGE_MAP(CDDIdeTreeView, CTreeView)
    //{{AFX_MSG_MAP(CDDIdeTreeView)
    ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
    ON_WM_DESTROY()
    ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
    ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
    ON_COMMAND(ID_MENU_HELP, OnMenuHelp)
    ON_COMMAND(ID_MAIN_BROWSE, OnMainBrowse)
    ON_COMMAND(ID_DEVICE_DETAILS, OnDeviceDetails)
    ON_COMMAND(ID_VIEW_FACEPLATE, OnViewFaceplate)
    ON_UPDATE_COMMAND_UI(ID_MAIN_BROWSE, OnUpdateMainBrowse)
    ON_UPDATE_COMMAND_UI(ID_VIEW_FACEPLATE, OnUpdateViewFaceplate)
    ON_UPDATE_COMMAND_UI(ID_DEVICE_DETAILS, OnUpdateDeviceDetails)
    ON_WM_CREATE()
    ON_COMMAND(ID_EXPAND_TREE, OnExpandTree)
    ON_COMMAND(ID_COLLAPSE_TREE, OnCollapseTree)
    //}}AFX_MSG_MAP
    // Standard printing commands
    ON_COMMAND(ID_FILE_PRINT, CTreeView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_DIRECT, CTreeView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_PREVIEW, CTreeView::OnFilePrintPreview)
END_MESSAGE_MAP()
/* moved to mainfrm */  
//  ON_MESSAGE(WM_SDC_CLOSEDEVICE, OnCloseDevice)
//  ON_MESSAGE(WM_SDC_GENERATEDEVICE, OnGenDevice)
//  ON_COMMAND(ID_VIEW_MORESTATUS, OnViewMorestatus)
//  ON_UPDATE_COMMAND_UI(ID_VIEW_MORESTATUS, OnUpdateViewMorestatus)

bool glblReview = false;

/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView construction/destruction

CDDIdeTreeView::CDDIdeTreeView()
{   
    TRACE(_T(">> CDDIdeTreeView instantiation...\n"));
    /* Turn on the desired attributes */
    m_dwDefaultStyle |= (DWORD)(
            TVS_HASLINES  |       // Lines link child items to their parent.
            TVS_LINESATROOT |     // Lines link child items to the root of the hierarchy.
            TVS_HASBUTTONS |      // Adds a button to the left of each parent item.
            TVS_DISABLEDRAGDROP | // Control won't send TVN_BEGINDRAG notifications.
            TVS_SHOWSELALWAYS );  // Causes selection to persist when control loses focus.

    /* Turn others off */
    m_dwDefaultStyle &= ~((DWORD)(
            TVS_EDITLABELS  ));    // Allows the user to edit the labels of tree view items.
    
    m_pTableFrame = NULL;
    m_pTwin       = NULL;
#ifdef _DEBUG
    TVexists = true;
#endif
}

CDDIdeTreeView::~CDDIdeTreeView()
{
    CDDIdeDoc* pDoc = GetDocument();

    if (m_pTableFrame)
        ((CTableChildFrame*)m_pTableFrame)->m_pTreeView = NULL;

#ifdef _DEBUG
    TVexists = false;
#endif
}

BOOL CDDIdeTreeView::PreCreateWindow(CREATESTRUCT& cs)
{
//  TRACE(_T(">> CDDIdeTreeView PreCreate...\n"));
    return CTreeView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView drawing

void CDDIdeTreeView::OnDraw(CDC* pDC)
{
    CDDIdeDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    // TODO: add draw code for native data here
}

void CDDIdeTreeView::OnInitialUpdate()
{
//  CTreeView::OnInitialUpdate();
    TRACE(_T(">> CDDIdeTreeView initial update...\n"));
/***stevev 18mar11 - this is now done in tableframe's mdiactivate routine
    this function runs wayy earlier than the mdiactivate so the tableframe pointers
    are not set when this function exists...***/
    
    TRACE(_T(">> CDDIdeTreeView initial update - Exit\n"));
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView printing

BOOL CDDIdeTreeView::OnPreparePrinting(CPrintInfo* pInfo)
{
    // default preparation
    return DoPreparePrinting(pInfo);
}

void CDDIdeTreeView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
    // TODO: add extra initialization before printing
}

void CDDIdeTreeView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
    // TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView diagnostics

#ifdef _DEBUG
void CDDIdeTreeView::AssertValid() const
{
    CTreeView::AssertValid();
}

void CDDIdeTreeView::Dump(CDumpContext& dc) const
{
    CTreeView::Dump(dc);
}

CDDIdeDoc* CDDIdeTreeView::GetDocument() // non-debug version is inline
{
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDDIdeDoc)));
    return (CDDIdeDoc*)m_pDocument;
}
#endif //_DEBUG


bool skipDeletion = false;//for debugging crash after deletion
/////////////////////////////////////////////////////////////////////////////
// CDDIdeTreeView message handlers

void CDDIdeTreeView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
    CTreeCtrl&  tree = GetTreeCtrl();
    CDDLBase *pBase   = ( CDDLBase* )pHint;
    CDDLBase *pParent = NULL;
    CDDLBase *pSelected = NULL;
    CDDLBase *pValid = NULL;
    CDDIdeDoc* pDoc = GetDocument();
    CString   strTemp;
    HTREEITEM hRoot = NULL;
    CTableChildFrame* pTblFrm = static_cast<CTableChildFrame*>(m_pTableFrame);

    CDDLBasePtrList_t l_IncidenceList;
//LOGIT(CLOG_LOG,L"E_>");
    if (pTblFrm != NULL )
        pSelected = pTblFrm->getCurrentSelection();

    if (pDoc && pDoc->pCurDev == 0)
        return;

//LOGIT(CLOG_LOG,L"  1");
//    tree.SetRedraw( FALSE );
    switch( lHint )
    {
/* this was only used on intitial update - removed 06dec05
 *      case UPDATE_REBUILD_ALL: // ignore pHint
 */
        /* Removed commented out code, POB - 5/20/2014 */

        case UPDATE_LIST_VIEW:  
            {
                /* Removed commented out code, POB - 5/20/2014 */

                if ( pBase == NULL )
                    break;
//LOGIT(CLOG_LOG,L"  4");
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateListView 0x%04x\n",pBase->GetID());
#endif
                /* Removed commented out code, POB - 5/20/2014 */

                HTREEITEM localItem = pBase->GetTreeItem();
                TRACE("Treeview Selecting handle %#08x (0x%04x)\n",localItem,pBase->GetID());
                tree.Select(localItem,TVGN_DROPHILITE | TVGN_CARET);// color it AND select it
                tree.EnsureVisible(localItem);
//LOGIT(CLOG_LOG,L"  5");

                if (m_pTableFrame)
                {
                    // Update the child frame's text with the current menu selection
                    m_pTableFrame->SetWindowText(pBase->GetName());
    //LOGIT(CLOG_LOG,L"  6");
                }
                TRACE("Treeview Selected  handle %#08x (0x%04x)\n",localItem,pBase->GetID());
//LOGIT(CLOG_LOG,L"  7");
            }
            break;

/* 06dec05 stevev - this is now embedded in the structurechanged action
 *        case UPDATE_DELETE_BELOW:  // pBase is parent.  delete below.
 ***/
        /* Removed commented out code, POB - 5/20/2014 */

        case UPDATE_REMOVE_DEVICE:
            {                       
    //TRACE(L"Update -- Remove Device.\n"); 
//LOGIT(CLOG_LOG,L"  8");
                if( GetDocument() && 
                    GetDocument()->pCurDev && 
                    GetDocument()->pCurDev->getMEEdepth() > 0 )
                { // do not delete if we are inside a method!!{ would delete the runner of the method}
//LOGIT(CLOG_LOG,L"  \n");
                    return;
                }
//LOGIT(CLOG_LOG,L"  9");
#ifdef LOG_MSG
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateRemoveDevice 0x%04x (0x04x)\n",pBase->GetID(),(int)pBase);
#endif
            LOGIF(LOGP_START_STOP)(CLOG_LOG,"< TreeView::OnUpdate got REMOVE_DEVICE.\n");
            
            /* 
             * There can now be mutiple Table windows opened.
             * The Table Frame owns the "root" (ie, top level)
             * Delete it here, POB - 5/18/2014
             */
            deleteBranch(pTblFrm->m_pRoot);
            
            CTreeCtrl& tree = GetTreeCtrl();
            
            if (tree)
            {
                tree.DeleteItem(pTblFrm->m_pRoot->GetTreeItem());
            }
            pTblFrm->m_pRoot->DeleteList();
            delete pTblFrm->m_pRoot;
            pTblFrm->m_pRoot = NULL;
            }
            break;

/* 06dec05 stevev - this msg was never sent
 *       case UPDATE_REMOVE_ITEM:   // pBase is item to remove.
***/

            /* Removed commented out code, POB - 5/20/2014 */

/* 06dec05 - stevev this is now done a) in add device or b) in structurechanged
 *        case UPDATE_ADD_ITEM:   // pBase is item to add.
***/

            /* Removed commented out code, POB - 5/20/2014 */

/* 06dec05 stevev - new name...
        case UPDATE_ITEM:       // pBase is item to update (symID only)
***/
        case UPDATE_STRUCTURE:
            {           
    //TRACE(L"Update -- Structure.\n");
//LOGIT(CLOG_LOG,L" 13");
                if( GetDocument()->pCurDev->getMEEdepth() > 0 )
                { // do not delete if we are inside a method!!{ would delete the runner of the method}
//LOGIT(CLOG_LOG,L" 14\n");
                    return;
                }
//LOGIT(CLOG_LOG,L" 15");

                unsigned long symID = (unsigned long)pHint;
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateStructure 0x%04x\n",symID);//pBase->GetID());
#endif
                /* Removed commented out code, POB - 5/20/2014 */
                if ( symID == 0L ) 
                {
//LOGIT(CLOG_LOG,L"15.5\n");
                    return;
                }
//LOGIT(CLOG_LOG,L" 16");
                /*
                find list of all tree-items of this menu
                */
            
                if ( GetTreeIncidence(symID /*pBase->GetID()*/, l_IncidenceList) > 0 )// gets incidence in tree
                {// exists - continue with the rest
                 // for first:  
                    //determine if validity changed (is not valid now)
                    POSITION pos = l_IncidenceList.GetHeadPosition();
//LOGIT(CLOG_LOG,L" 17");
                    CDDLBase *pItem = (CDDLBase*) l_IncidenceList.GetNext( pos );// pos is on second
//LOGIT(CLOG_LOG,L" 18");
                    int flag = 0;// 0 - noop, 1 -validity diff, 2 - itemList delta, 4 - label change, 4 error

                    if (! pItem->IsValid() )
                    {// no longer valid
                     // @ TRUE - update this parent item and parent items for each in list  
//LOGIT(CLOG_LOG,L" 19");
                        flag = 1;
                    }
                    else
                    {
                    /* @ FALSE - still valid:
                        determine if the content of the item list is the same
                        generate a new CDDLBasePtrList_t for the current menu item
                        verify that it is the same info as currently in the child list
                        */
//LOGIT(CLOG_LOG,L" 20");
                        itemInfoList_t hCmenuLst, CDDLmenuLst;
//LOGIT(CLOG_LOG,L" 21");
                        if ( pItem->IsMenu() )
                        {
//)LOGIT(CLOG_LOG,L" 22 (0x%04x)",pItem->GetID());
                            CDDLMenu* pM = NULL;
                            /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
                            if ( pItem->IsKindOf( RUNTIME_CLASS( CDDLMenu )) )
                            {
                                pM = (CDDLMenu*)pItem;
                            }
                            else
                            if ( pItem->IsKindOf( RUNTIME_CLASS( CDDLMenuItem )) && 
                                 ((CDDLMenuItem*)pItem)->m_item != NULL )
                            {
                                pM = (CDDLMenu*)((CDDLMenuItem*)pItem)->m_item;
                            }
                            else
                            {// we have issues
                                break;
                            }
                            pM->GetMenuItemList(hCmenuLst);// from hCmenu
//LOGIT(CLOG_LOG,L" 23");
                            pM->GetExistingList(CDDLmenuLst);// from children
//LOGIT(CLOG_LOG,L" 24");

                            if ( ! pM->areSame(hCmenuLst,CDDLmenuLst) )// compare item IDs ONLY
                            {
//LOGIT(CLOG_LOG,L" 25");
                            // @ TRUE - for each in list, delete below and rebuild (with label too)
                                flag = 2;
#ifdef _DEBUG
    CString lft,rgt;
    lft = pItem->GetParent()->GetCurrentName(pItem);
    rgt = pItem->GetName();
#endif
                                if ( pItem->GetParent()->GetCurrentName(pItem) != pItem->GetName() )
                                {
//LOGIT(CLOG_LOG,L" 26");
                                // @ TRUE - for each in list, change their lable
                                    flag |= 4;
                                }
//LOGIT(CLOG_LOG,L" 27");
                            }
                            else
                            {
//LOGIT(CLOG_LOG,L" 28");
                            // @ FALSE - verify the label changed
                                if ( pItem->GetParent()->GetCurrentName(pItem) != pItem->GetName() )
                                {
                                // @ TRUE - for each in list, change their lable
                                    flag = 4;
                                }
                                else
                                {
                                // @ FALSE- ERROR: log it as not changed!!
                                    flag = 8;
                                }
//LOGIT(CLOG_LOG,L" 29");
                            }
//LOGIT(CLOG_LOG,L" 30");
                        }// at not menu, we aren't interested (should never happen)
                    }// end validity test
//)LOGIT(CLOG_LOG,L" 31 (%d)",flag);
                    if ( flag > 0 && flag < 8 && pItem != NULL )
                    {
                        CDDLBase *pParent = NULL;

                        do // for all incidence's
                        {
//LOGIT(CLOG_LOG,L" 32");
                            HTREEITEM GetParentItem(  HTREEITEM hItem );
                            pParent = pItem->GetParent();
//LOGIT(CLOG_LOG,L" 33");
                            if ( (flag & 1) == 1 )
                            {// a menu-item is no longer valid
                                deleteSelfNbranch(pItem) ; // will go to parent
                                // the removal of tree items (above) should trigger a repaint
                                // 20may11 pTblFrm->m_pSelection = pParent;
                                pTblFrm->setSelection2parent();
//LOGIT(CLOG_LOG,L" 34");
                            }
                            else
                            {
//LOGIT(CLOG_LOG,L" 35");
                                if ( (flag & 2) == 2 )
                                {// this menu still good, it's children are different
#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE MAIN  delete + rebuild branch 0x%04x\n", ROOT_MENU);
#endif

                                    //deleteSelfNbranch(pItem) ; // will go to parent
                                    deleteBranch(pItem); //pSelected is no longer pointing to good memory (yet not NULL) after it returns here, POB - 5/21/2014
//LOGIT(CLOG_LOG,L" 36");
                                    pItem->DeleteList();
                                    
                                    if ( ! skipDeletion){//for debugging crash after deletion
//LOGIT(CLOG_LOG,L" 37");
                                        // this adds it to its parent where it still is
                                        //AddSubMenu( pItem, pParent );// put it back right
                                        // do the parts that matter
                                        FillMenuList(pItem, (pItem->IsReview())?true:false);
                                        // it's already in the parent
                                    }//for debugging crash after deletion
                                }
                                if ( (flag & 4) == 4 )
                                {// label only (may occur with above)
#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE MAIN  changes the name 0x%04x\n", ROOT_MENU);
#endif
//LOGIT(CLOG_LOG,L" 38");
                                    strTemp = pItem->GetParent()->GetCurrentName(pItem);//was: pItem->GetName();
//LOGIT(CLOG_LOG,L" 39");
                                    pItem->SetName(strTemp);
//LOGIT(CLOG_LOG,L" 40");
                                    tree.SetItem( pItem->GetTreeItem(), TVIF_TEXT, strTemp,
                                                                                0, 0, 0, 0, 0);
//LOGIT(CLOG_LOG,L" 41");
                                }
//LOGIT(CLOG_LOG,L" 42");
                            }
//LOGIT(CLOG_LOG,L" 43");
                            if (pos != NULL )
                            {
                                pItem   = (CDDLBase*) l_IncidenceList.GetNext( pos );
                            }
                            else
                            {
                                pItem = NULL;
                            }
//LOGIT(CLOG_LOG,L" 44");
                        }while ( pItem != NULL ); // next incident
    // send update list selection
//LOGIT(CLOG_LOG,L" 45");
                    }
                    else
                    {
//LOGIT(CLOG_LOG,L" 46");
// temporarily off                      LOGIT(CERR_LOG,"ERROR: message to update but no differences found.\n");
                    }
//LOGIT(CLOG_LOG,L" 47");
                }// else no incidence of this item
//LOGIT(CLOG_LOG,L" 48");
            }
            break;
/***************** 06dec05 - no longer used
        case UPDATE_WINDOW_ITEM:
            {
#ifdef LOG_MSGS
            if ( pBase )
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateWindowItem::UnHandledMsg 0x%04x\n",
      (int)pBase);
            else
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateWindowItem::UnHandledMsg 0x0000\n");
#endif
            }break;
*****/
            /* new 29nov05 stevev */
        case UPDATE_T_DRAW_HOLD:    // set redraw off till release message
            {           
    //LOGIT(CLOG_LOG,L"Update -- Draw Hold.\n");    
//LOGIT(CLOG_LOG,L" 49"); 
                TRACE(L"Update -- Draw HOLD<<<<<.\n");
                
                // We need to ensure our tree selection
                // is on a valid menu
                if (pSelected)
                {   
                    /*
                     * Find the 1st object (including this
                     * selection) which is valid
                     * and has no invalid parents.
                     */
                    pValid = pSelected->FindValidBranch();
                    
                    if (pValid)
                    {
                        pSelected = pValid;
                    }
                    else
                    {
                        // Unable a find a valid parent
                        HTREEITEM hRoot = GetTreeCtrl().GetRootItem();
                        pValid = (CDDLBase*)(GetTreeCtrl().GetItemData(hRoot));

                        if (pValid)
                        {
                            // Choose the root tree CDDLBase
                            // a our selection
                            pSelected = pValid;
                        }
                    }

                    /* 
                     * Remember the selected menu so we can try to get it back when we're through
                     * destroying and rebuilding the entire menu structure, POB - 5/18/2014
                     */
                    pTblFrm->m_UserSelect.selectedName = pSelected->GetName(); // Currently for debug purposes only
                    pTblFrm->m_UserSelect.selectedID = pSelected->GetID();

                    if (pSelected->GetParent())
                    {
                        pTblFrm->m_UserSelect.parentID = pSelected->GetParent()->GetID();
                    }
                    else
                    {
                        pTblFrm->m_UserSelect.parentID = 0;
                    }
                }
                tree.SetRedraw( FALSE );
            }break;
        case UPDATE_T_DRAW_DRAW:
            {           
                TRACE(L"Update -- Draw Draw>>>>>.\n");
                tree.SetRedraw( TRUE );
  
                /* 
                 * Now, we will attempt to restore the valid menu selection 
                 * after the rebuilding of tree control, POB - 5/20/2014
                 */
                P_TABLE_FRM->setCurrentSelection(P_TABLE_FRM->m_UserSelect);
            }break;
// 02dec05 - we are generating an entire device for doc's CurDev - root id is passed in
        case UPDATE_ADD_DEVICE:
        case UPDATE_TREE_VIEW:  // stevev 31jul07
            {       
    //TRACE(L"Update -- Tree View.\n");
//LOGIT(CLOG_LOG,L" 50");
                if( GetDocument()->pCurDev->getMEEdepth() <= 0 )
                { // do not delete if we are inside a method!!{ would delete the runner of the method}
//LOGIT(CLOG_LOG,L" 51");
                    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< TreeView::OnUpdate got ADD_DEVICE.\n");
                    if ( ((unsigned long)pBase) == ROOT_MENU)
                    {
                    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< TreeView::OnUpdate got ADD_DEVICER.\n");
                        AddRootMenu();
                    }
                    else
                    {
                        LOGIF(LOGP_START_STOP)
                                        (CLOG_LOG,"< TreeView::OnUpdate got ADD_DEVICEO.\n");
                        itemInfo_t local;
                        itemID_t  symNum = (unsigned long)pBase;

                        if ( pDoc == NULL || pDoc->pCurDev == NULL ) return;

                        CTreeCtrl&  tree = GetTreeCtrl();

                        // stevev 18apr11 - try to stop duplicate copies of menues on the tree                      
                        tree.DeleteAllItems();

                        if (pDoc->pCurDev->getItemBySymNumber(symNum,&local.p_Ib) == SUCCESS &&
                             local.p_Ib != NULL )
                        {
                            local.p_Ib->Label(local.labelStr);
                            local.p_Ib->Help (local.help_Str);
                            local.labelFilled = true;// for better or worse
                            pTblFrm->m_pRoot = new CDDLMenu(local);
                    #ifdef LOGBUILD
                        LOGIT(CLOG_LOG,"TREE MAIN  adds an item 0x%04x\n", ROOT_MENU);
                    #endif
                        LOGIF(LOGP_START_STOP)
                            (CLOG_LOG,L"< TreeView::ADD_DEVICE adds item '%s'.\n",
                                                                    local.labelStr.c_str());
                            int rr = AddSubMenu(pTblFrm->m_pRoot, NULL );// insert and all under it
                        }//else -error
                    }//endelse
                    
                    /*
                     * Determine if the user has previously selected a menu on the tree
                     * POB - 5/18/2014
                     */
                    if (lHint == UPDATE_ADD_DEVICE)
                    {
                        // No Symbol ID selected yet...
                        // We are starting up for the 1st time.
                        // Use default menu
                        P_TABLE_FRM->setSelectionDefault();// Get the default menu
                        tree.Expand( P_TABLE_FRM->getCurrentSelection()->GetTreeItem(), TVE_COLLAPSE );
                    }
                }// else discard message
//LOGIT(CLOG_LOG,L" 53");               
            }
            break;

        case UPDATE_ONE_VALUE:
            {           
    TRACE(L"TreeView.OnUpdate -- Value of 0x%04x.\n",(int)pHint);
#ifdef LOG_MSGS
//LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateOneValue::Not used here\n");
#endif
            }break;
        case UPDATE_DELETE_ITEM:
            {               
                unsigned long symID = (unsigned long)pHint;
                LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"TreeView.OnUpdate:Element To Delete: "
                                                                            L"0x%04x\n",symID);
                TRACE(L"TreeView.OnUpdate:Element To Delete: 0x%04x\n",symID);
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UpdateDeleteItem 0x%04x\n",symID);
#endif
                if ( symID == 0L ) 
                {
                    return;
                }
                int cnt = DeleteBranchIncidence(symID, pTblFrm->m_pRoot);  /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
                LOGIF(LOGP_NOTIFY)(CLOG_LOG,L"TreeView.OnUpdate:Deleted %d incidence\n",cnt);
            }
            break;
        default:
            {
#ifdef LOG_MSGS
LOGIT(CLOG_LOG,"MSG:TreeView. OnUpdate - UnknownMsg (%d)\n",lHint);
#endif
            }
            break;
     }//endswitch
//LOGIT(CLOG_LOG,L" 54\n");

//     tree.SetRedraw( TRUE );
    return; 
}

/* stevev 01dec05 - we must keep the CDDL tree and the view tree syncronized! */
// stevev - 26may11 - nobody is checking the return value...change it to be:
// returns number of items deleted ... error isn't possible
int CDDIdeTreeView::deleteSelfNbranch(CDDLBase *pItem) 
{
    int r = 0; // none
    CDDLBase * pParent= pItem->GetParent();
    HTREEITEM  hItem  = pItem->GetTreeItem();   
    CTreeCtrl& tree   = GetTreeCtrl();

    tree.SelectItem( hItem );// set this node active (incase selection is currently node below)

#ifdef LOGBUILD
    unsigned long holdID = pItem->GetID();
    LOGIT(CLOG_LOG,"TREE ------- 0x%05x deleting itself\n", holdID);
#endif

    // delete children - they should all be menues
    r += deleteBranch(pItem);// no error possible

    if ( pParent)
    {// goto parent
        tree.SelectItem( pParent->GetTreeItem() ); // set parent node active
        tree.DeleteItem( hItem );// get the tree-item & get rid of it
        r++;
        // stevev 18aug11 - the SelectItem above generates an updateListview so we could
        //    have a whole new list...the remove() in the deleteBranch() above won't be enough
        P_LISTVIEW->remove( pItem->GetID() );
        //delete pItem;
        pParent->RemoveNdeleteChild(pItem);// by pointer
    }
    else // - no place to go, let things fall where they may...
    {
        LOGIT(CERR_LOG,"Attempt to Delete tree item that has no parent.\n");
        // will return 0 - nothing deleted
    }
#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE ------- 0x%05x finished-------\n", holdID);
#endif
    return r;
}

// stevev - 26may11 - nobody is checking the return value...change it to be:
// returns number of items deleted ... no errors possible
int CDDIdeTreeView::deleteBranch(CDDLBase *pItem) // everything under pItem
{
    int r = 0; // no deletions
    CDDLBase * pChildItm  = NULL;
    HTREEITEM  hItem  = NULL;
    HTREEITEM  hChild = NULL;   
    HTREEITEM  hNxtChild = NULL;
    CTreeCtrl& tree   = GetTreeCtrl();
#ifdef DETECT_SELECT
    CDDIdeDoc* pDoc   = GetDocument();
#endif
    hItem = pItem->GetTreeItem();
    // stevev 17aug11 - remove it from the listview (if its there)
    unsigned itmID = pItem->GetID();
    P_LISTVIEW->remove(itmID); //from listview if it exists
    // caller will delete pItem

    if( tree.ItemHasChildren(hItem) ) // this tree node has leaves/branches
    {
        hChild = tree.GetChildItem(hItem);
        TRACE("tree.deleteBranch 0x%04x has children\n",pItem->GetID());
        while (hChild != NULL)
        {// one child
            pChildItm  = (CDDLBase*) tree.GetItemData( hChild );
            unsigned chldID = pChildItm->GetID();
#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE 0x%05x deleting child 0x%04x  at 0x%08x\n",
        pItem->GetID(), pChildItm->GetID(),hChild);
#endif
            hNxtChild = tree.GetNextItem(hChild, TVGN_NEXT);// get next before its gone

            if ( pChildItm->IsMenu() ) // child may have children
            {
                TRACE("tree.deleteBranch 0x%04x deleting child menu 0x%04x\n",
                                                          pItem->GetID(),pChildItm->GetID());
                ASSERT( pItem == pChildItm->GetParent() );
                r += deleteBranch(pChildItm);     // anything under the child in tree-recurse
            }
            else // just delete the entry
            {// should never happen (non-menu in tree)
                TRACE("tree.deleteBranch 0x%04x deleting item 0x%04x\n",
                                                          pItem->GetID(),pChildItm->GetID());
            }
#ifdef DETECT_SELECT
            if (pDoc && pDoc->m_pSelection == pChildItm )
                pDoc->m_pSelection = 0;
#endif
            P_LISTVIEW->remove(chldID);          //from listview if it exists
            pItem->RemoveNdeleteChild(pChildItm);// from pItem's childList only, by ptr addr
            tree.DeleteItem(hChild);             // from the tree 
            TRACE("tree.deleteBranch removed 0x%04x from the tree.\n",chldID);
            r++;
            hChild = hNxtChild;
        }
    }
    TRACE("tree.deleteBranch 0x%04x is finished\n", pItem->GetID());
    return r;
}

/* end code 01dec05 */

int CDDIdeTreeView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CTreeView::OnCreate(lpCreateStruct) == -1)
        return -1;
    TRACE(_T(">> CDDIdeTreeView on create...\n"));
    CDDIdeDoc* pDoc = GetDocument();

    /*
     * Create the Image List
     */
    // Vibhor 100603 : Start of Code
    m_ctlImage.Create(16,16,ILD_TRANSPARENT | ILC_COLOR32, 0,0);
    CBitmap bmp;
    bmp.LoadBitmap(IDB_IMAGELIST);
    m_ctlImage.Add(&bmp, RGB(255,0,255));
    // Vibhor 100603 : End of Code

    /*
     * Attach image list to Tree
     */
    CTreeCtrl& tree = GetTreeCtrl();
    tree.SetImageList(&m_ctlImage, TVSIL_NORMAL);

    TRACE(_T(">> CDDIdeTreeView on create - Exit\n"));
    return 0;
}

void CDDIdeTreeView::AddOneItem( CDDLBase *pBase )
{
    CTreeCtrl& tree = GetTreeCtrl();
    CDDLBase *pParent = pBase->GetParent();
    HTREEITEM hTreeParent;
    HTREEITEM hInsertAfter;
    CString strName;
    int nmLen = 0;
    unsigned long symID = 0;

    /*
     * The hTreeParent is the treeitem of our parent.
     */
    if( pParent )
    {
        hTreeParent = pParent->GetTreeItem();
        hInsertAfter = TVI_LAST;
    }
    else
    {
        hTreeParent = NULL;
        hInsertAfter = TVI_ROOT;
    }

    if( pBase->IsMenu() ) /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
    {
        // don't add anything other than menus to tree.
        TV_INSERTSTRUCT InsertStruct;
        memset( &InsertStruct, 0, sizeof( TV_INSERTSTRUCT ) );
        strName = pBase->GetName();
        nmLen = strName.GetLength();
        InsertStruct.hParent              = hTreeParent;
        InsertStruct.hInsertAfter         = hInsertAfter;
        InsertStruct.item.mask            = TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
        InsertStruct.item.cchTextMax      = 0;
        InsertStruct.item.pszText         = (LPTSTR)((LPCTSTR)strName);//stevev was::> (char *)(const char*)strName;
        InsertStruct.item.lParam          = (LPARAM)pBase;

        /*
         * All root menus on the tree will have the 
         * same appearance, POB - 5/22/2014
         */
        symID = pBase->GetID();

        if ( symID == ROOT_MENU || symID == OFFLINE_ROOT_MENU ||                        // Supporting all root menus and 
             symID == DIAGNOSTIC_ROOT_MENU || symID == PROCESS_VARIABLES_ROOT_MENU ||   // fixed defect by using symbol ID
             symID == DEVICE_ROOT_MENU || symID == MAINTENANCE_ROOT_MENU )              // instead of string name, POB - 5/23/2014
        {
            InsertStruct.item.iImage          = 05;
            InsertStruct.item.iSelectedImage  = 05;
        }
        else
        {
            // For all other menus, show them as folders
            InsertStruct.item.iImage          = 0;
            InsertStruct.item.iSelectedImage  = 4;  
        }
        pBase->SetTreeItem(tree.InsertItem( &InsertStruct ));
    }
}

void CDDIdeTreeView::SelectParent( CDDLBase *pBase )
{
     CTreeCtrl& tree = GetTreeCtrl();
    CDDLBase* pParent;

    if( NULL != pBase && NULL != (pParent = pBase->GetParent()) )
    {
        tree.SelectItem( pParent->GetTreeItem() );
    }
    else
    {
        ((CTableChildFrame*)m_pTableFrame)->setSelectionDefault();
    }
}

void CDDIdeTreeView::BuildTreeFrom(CDDLBase *pBase)
{
    CTreeCtrl& tree = GetTreeCtrl();
    TRACE(_T(">> CDDIdeTreeView.BuildTreeFrom Entry\n"));

    /*
     * Clear any children in the tree.
     */
//  while( (NULL != pBase->m_hTreeItem) &&
//            (0 != tree.ItemHasChildren( pBase->m_hTreeItem ) ) )
/*
<START>Commented by Anoop since the loop was infinite in some cases
    while( (NULL != pBase->GetTreeItem()) &&
            (0 != tree.ItemHasChildren( pBase->GetTreeItem() ) ) )
    {
//      HTREEITEM hItem = tree.GetChildItem( pBase->m_hTreeItem );
        HTREEITEM hItem = tree.GetChildItem( pBase->GetTreeItem() );
        tree.DeleteItem( hItem );
    }
<END>Commented by Anoop since the loop was infinite in some cases
*/
     AddOneItem( pBase );
/*<END>Commented by Anoop since the loop was infinite in some cases*/
    /*
     * Now insert the children.
     */
//  POSITION pos = pBase->m_ChildList.GetHeadPosition();
    POSITION pos = pBase->GetChildList().GetHeadPosition();
    while( pos )
    {
//      CDDLBase* pChild = pBase->m_ChildList.GetNext( pos );
        CDDLBase* pChild = pBase->GetChildList().GetNext( pos );
        BuildTreeFrom( pChild );
    }
    TRACE(_T(">> CDDIdeTreeView.BuildTreeFrom - Exit\n"));
}


/* VMKP added on 030104 */    // find the CDDLbase with item-id with the parent-item-id in BaseItem on down
CDDLBase *CDDIdeTreeView::ReturnTreeItemId(unsigned long    lItemId,unsigned long lParentItemId, CDDLBase *pBaseItem)
{
    CTreeCtrl& tree = GetTreeCtrl();
    CDDIdeDoc* m_pDoc = GetDocument();
    CDDLBase *pBase = pBaseItem;
    CDDLBase *pItem,*pNextItem;

    HTREEITEM hItem = pBase->GetTreeItem();
    HTREEITEM hNextItem;

    if(hItem != NULL)
    {
          pItem = (CDDLBase *)tree.GetItemData(hItem);

          if((pItem->GetID() == lItemId)  )//&& (pItem->GetParent()->GetID() == lParentItemId))
          {
              if ( lParentItemId == 0 || (pItem->GetParent()->GetID() == lParentItemId))
                return pItem;
          }

          if(tree.ItemHasChildren(hItem))
          {
              HTREEITEM hChildItem = tree.GetChildItem(hItem);
              CDDLBase *pChildItem;
              HTREEITEM hNextChildItem;

              while(hChildItem != NULL)
              {
                pChildItem = (CDDLBase *)tree.GetItemData(hChildItem);
                CDDLBase *ptmpBase = ReturnTreeItemId(lItemId,lParentItemId,pChildItem);
                
                if(ptmpBase)
                    return ptmpBase;
                
                hNextChildItem = tree.GetNextItem(hChildItem, TVGN_NEXT);
                hChildItem = hNextChildItem;
              }
            
              hNextItem = tree.GetNextItem(hItem, TVGN_NEXT);
             
              if(hNextItem != NULL)
              {
                pNextItem = (CDDLBase *)tree.GetItemData(hNextItem);
                CDDLBase *ptmpBase  = ReturnTreeItemId(lItemId,lParentItemId,pNextItem);
    
                if(ptmpBase)
                    return ptmpBase;
              }
          }
    }
  

return NULL;
}
/* VMKP added on 030104 */    


void CDDIdeTreeView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
    NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

    CDDIdeDoc* pDoc = GetDocument();

    TRACE(L"Tree OnSelChanged.\n");
    /*
     * Retrieve the class pointer for this item,
     * then use it to fill the list view.
     */
    CDDLBase *pBase = (CDDLBase *)pNMTreeView->itemNew.lParam;

    // 31jul12 we can't update all views since we could have several treeviews open
    //  pDoc->UpdateAllViews( NULL, UPDATE_LIST_VIEW ,P_TABLE_FRM->getCurrentSelection());//20may11
    
    /*
     * CDDIdeTreeView::OnUpdate() with UPDATE_LIST_VIEW calls tree.Select() and 
     * this will bring us here in OnSelchanged() unnecessarily again.  Therefore, 
     * we only need to update the Listview, POB - 4/13/2014
     */ 
    //OnUpdate( NULL, UPDATE_LIST_VIEW ,P_TABLE_FRM->getCurrentSelection());  //POB - 4/13/2014
    P_LISTVIEW->OnUpdate( NULL, UPDATE_LIST_VIEW ,P_TABLE_FRM->getCurrentSelection());

    /*
     *  Set *pResult to 0 if you want MFC's default call, else set to 1
     */
    *pResult = 1;
//  *pResult = 0;
}

/***************************************************************************
 * FUNCTION NAME: CDDIdeTreeView::WriteProfileSettings
 *
 * DESCRIPTION:
 *    Gets the size of the tree view, and writes the cx coordinate ( which
 *   is essentially getting the splitter bar position) to the registry
 *
 * ARGUMENTS:
 *   none
 *
 * RETURNS:
 *   none
 *
 ***************************************************************************/
void CDDIdeTreeView::WriteProfileSettings()
{
     CWinApp *pApp = AfxGetApp();
     CRect rect;
     //CString strSplitterBarPos;
     GetClientRect( &rect );
     CSize size( rect.Size() );
     wchar_t string_buffer[MAX_PATH];
     memset(string_buffer,0x0000, MAX_PATH*sizeof(wchar_t));

     /*
      * Note: WriteProfileInt() doesn't behave properly when using the registry
      */
     //I can't believe somebody actually progeammed this line.....
     //                            _itot( size.cx, (LPTSTR)((LPCTSTR)strSplitterBarPos), 10 );
     _itow(size.cx,string_buffer,10);
     BOOL bReturn = pApp->WriteProfileString( SETTINGS, SPLITTER_BAR, string_buffer);
}

void CDDIdeTreeView::OnDestroy() 
{
    CTreeView::OnDestroy();
    LOGIF(LOGP_START_STOP)(CLOG_LOG,"< TreeView::OnDestroy recording position.\n");
    
    OnUpdate(this, UPDATE_REMOVE_DEVICE, NULL);  // must delete all tree objects when closing tree view - POB, 5/17/2014

    // NOTE: Add your message handler code here
    WriteProfileSettings();

    // Set the CdrawTbl object that invoke this to NULL
    if (m_pTwin)
    {
        m_pTwin->m_pTable = NULL;
    }
}

void CDDIdeTreeView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
    //CDDIdeDoc* pDoc = GetDocument();
    CDDLBase* pBase = NULL;

    /*
     * Retrieve the class pointer for this item,
     * then execute
     */
//20may11    pBase = P_TABLE_FRM->m_pSelection; // We already know the tree item selection
    pBase = P_TABLE_FRM->getCurrentSelection();

    if (pBase != NULL)
        pBase->Execute();

    *pResult = 0;
}

void CDDIdeTreeView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
    // TODO: Add your control notification handler code here
    CDDLBase*      pItem;
    CDDIdeDoc*     pDoc;
    CMenu          menu;
    CMenu          *pSubmenu;
    CPoint         ptClient;
    TV_HITTESTINFO HitTestInfo;
    int nPos;

    /*
     * Get the mouse position associated with this message
     * and see if it is on a tree item.
     */
    DWORD dwPosition = GetMessagePos();
    CPoint point(LOWORD(dwPosition), HIWORD(dwPosition));
    CTreeCtrl& tree = GetTreeCtrl();
    ptClient = point;
    ScreenToClient(&ptClient);
    HitTestInfo.pt = ptClient;
    tree.HitTest( &HitTestInfo );

    if( TVHT_ONITEM & HitTestInfo.flags )
    {   /*
         * Get the CHdwBase pointer from the selected object
         * and have it load the menu.  Must save the pointer
         * so the frame knows where to process the menu selection.
         */
        pItem = (CDDLBase *)tree.GetItemData(HitTestInfo.hItem);        

        if( pItem )
        {
            pItem->LoadContextMenu( &menu, nPos ) ;
            pDoc = GetDocument();
            if(NULL != pDoc)
            {
                pDoc->m_pMenuSource = pItem;
/*<START>Anoop For fixing the PAR 5286. In the tree view context menu should be displayed only when the select menu item is online */   
        // 20may11 - lets not do this for now       P_TABLE_FRM->m_pSelection = pItem;
                
                pSubmenu = menu.GetSubMenu(nPos);
                pSubmenu->TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
                    point.x, point.y, GETMAINFRAME);// stevev 08oct10:was:>AfxGetMainWnd() );
            }
/*<END>Anoop For fixing the PAR 5286. In the tree view context menu should be displayed only when the select menu item is online */                 
        }
    }

    /*
     * Set *pResult to 0 if you want MFC's default call to OnRClick which will result
     * in a call to OnContextMenu() as well which we don't want here, so set to 1.
     */
    *pResult = 1;
}

// Move all the common code to class CHelpBox for displaying HELP for all DDL objects
// in both the TABLE and WINDOW styles, POB - 1 Aug 2008
void CDDIdeTreeView::OnMenuHelp() 
{
    CString strItemCaption;
    CDDIdeDoc* pDoc = GetDocument();

    // New code added by Jeyabal
    CTreeCtrl& tree = GetTreeCtrl();
    if (tree)
    {
        HTREEITEM hItem = tree.GetSelectedItem();
        TVITEM item;
        memset(&item,0,sizeof(TVITEM));         // added stevev 19dec05 - crashed on XP
        item.mask  = TVIF_HANDLE | TVIF_PARAM;  // added stevev 19dec05 - crashed on XP
        item.hItem = hItem;     

        BOOL bWorked = tree.GetItem(&item);
        LOGIT(CLOG_LOG,"  Tree-Help: Got item's item. (%s)\n",((bWorked)?"OK":"FAIL"));
        if (bWorked)
        {
            CDDLBase *pBase = (CDDLBase *)item.lParam;
            
            if (pBase)
            {
                pBase -> DisplayHelp();
            }

            /* code moved 11/27/13 POB */
/* code removed 13aug08 - paulb */
        }
    }
}

void CDDIdeTreeView::OnMainBrowse() 
{
    CDDIdeDoc* pDoc = GetDocument();
    RETURNCODE rc = SUCCESS;
    TRACE(_T(">> CDDIdeTreeView main browse...\n"));
    return;
}

#if 0 /* moved to document 05aug05  */
stevev 30nov05 : seems to work great, removing moved code - see doc or previous versions for old contents
#endif // moved to the doc
/*
<START> Commented by Anoop for PAR 5132 and 5179 Moved the commit to a common main frame call
    stevev 30nov05 - removed commented code that was moved - see mainframe or previous versions
<END> Commented by Anoop for PAR 5132 and 5179 Moved the commit to a common main frame call 
*/


void CDDIdeTreeView::OnDeviceDetails() 
{
    CDeviceDetails  dDeviceDetails(GetDocument());
    dDeviceDetails.DoModal();
}


/* * OnViewMorestatus has been moved to the mainfrm * * */

                    

void CDDIdeTreeView::OnViewFaceplate() 
{
    CFacePlate *pDialog = NULL;

    pDialog = new CFacePlate(this);
    //Check if new succeeded and a valid pointer to a dialog object is 
    //created.
    if(pDialog != NULL)
    {
        BOOL ret = pDialog->Create(IDD_FACE_PLATE_NEW, this);
        pDialog->ShowWindow(SW_SHOW);
    }
}

void CDDIdeTreeView::OnUpdateMainBrowse(CCmdUI* pCmdUI) 
{
    if (PCURDEV != NULL)
        pCmdUI->Enable(FALSE);
}
/*************************************************************************************************
 *
 *   $History: DDIdeTreeView.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:59a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified All files had HART standard headers and Footers.
 * 
 *************************************************************************************************
 */
bool faceplate_exists = true;
void CDDIdeTreeView::OnUpdateViewFaceplate(CCmdUI* pCmdUI) 
{
    RETURNCODE rc = SUCCESS;

    pCmdUI->Enable(FALSE);

    // Get the current device for getting the Process Variable item
    hCddbDevice* pCurDevice = GetDocument()->pCurDev;
    if (pCurDevice)
    {
        hCitemBase *pItemBase = NULL;

        // In the absense of Face Plate definition we are using this Process variable
        // eventualy this needs to change
        if ( (!faceplate_exists) || pCurDevice->getItemBySymNumber(DEVICE_FACE_PLATE, &pItemBase) != SUCCESS)
        {
            faceplate_exists = false;
            pCmdUI->Enable(FALSE);
        }
        else
            pCmdUI->Enable(TRUE);
    }   
}
/*
    30nov05 - removed commented code - see previous versions for contents
<END> Commented by Anoop for PAR 5132 and 5179 Moved the commit to a common main frame call 
*/



void CDDIdeTreeView::OnUpdateDeviceDetails(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(FALSE);

    //CDDIdeDoc *pDoc = GetDocument();

    if (P_TABLE_FRM != NULL)
    {
        //CDDLBase *pBase =  P_TABLE_FRM->m_pSelection; // pBase points to DDL Menu
        CDDLBase *pBase =  P_TABLE_FRM->getCurrentSelection();
        if (PCURDEV != NULL)
        {
/* VMKP Modified on 210104 */
            if ( ROOT_MENU == pBase->GetID()) 
/*          CString strTmp = pBase->GetName();
            if ( 0 == strTmp.CompareNoCase("Online") ) */
                pCmdUI->Enable(TRUE);
/* VMKP Modified on 210104 */
        }                   
    }   
}


#if 0 /* moved to ducument 05aug05 stevev */
    30nov05 - removed commented code - see document or previous versions for contents
#endif // moved to documant

void CDDIdeTreeView::OnExpandTree() 
{
    //CDDIdeDoc* pDoc = GetDocument();
    
    TRACE(L"Tree OnExpandTree.\n");

    //ExpandTree( P_TABLE_FRM->m_pSelection->GetTreeItem(), TVE_EXPAND );
    ExpandTree( P_TABLE_FRM->getCurrentSelection()->GetTreeItem(), TVE_EXPAND );
}

void CDDIdeTreeView::OnCollapseTree() 
{
    //CDDIdeDoc* pDoc = GetDocument();

    TRACE(L"Tree OnCollapseTree.\n");

    ExpandTree( P_TABLE_FRM->getCurrentSelection()->GetTreeItem(), TVE_COLLAPSE );
    ExpandTree( P_TABLE_FRM->getCurrentSelection()->GetTreeItem(), TVE_COLLAPSE );
}

void CDDIdeTreeView::ExpandTree(HTREEITEM Item, UINT nCode)
{
    CDDIdeDoc* pDoc = GetDocument();
    CTreeCtrl& tree = GetTreeCtrl();

    HTREEITEM hChildItem;
    HTREEITEM hNextItem;

    // We already know the tree item selection
    tree.Expand( Item, nCode );

    // Expand/collapse all of the children of this item.
    if ( tree.ItemHasChildren( Item ) )
    {
       hChildItem = tree.GetChildItem( Item );

       while (hChildItem != NULL)
       {
          // Expand on its children
          ExpandTree( hChildItem, nCode );
          
          // Get the next child in this item
          hNextItem = tree.GetNextItem( hChildItem, TVGN_NEXT );
          hChildItem = hNextItem;
       }
    }
}


// stevev new code 29nov05
int CDDIdeTreeView::GetTreeIncidence( ulong symID, CDDLBasePtrList_t& returnList)
{
    CTableChildFrame* pTblFrm = static_cast<CTableChildFrame*>(m_pTableFrame);
    CDDLBase * pBase = NULL;
    
    if(pTblFrm->m_pRoot)
    {
        pBase = pTblFrm->m_pRoot;
    }
/* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */
    return GetBranchIncidence(symID, returnList,pBase);
}

// returns negative number on error or number removed
int CDDIdeTreeView::DeleteBranchIncidence( ulong symID, CDDLBase * pStartBase)
{
    int RemovalCount = 0, retVal = 0;
    CDDLBase* pItem = NULL;
    CTreeCtrl& tree = GetTreeCtrl();
    HTREEITEM hItem = pStartBase->GetTreeItem();

    if(hItem != NULL) // start has a tree location
    {
        pItem = (CDDLBase *) tree.GetItemData(hItem);
#ifdef _DEBUG
        if (pItem != pStartBase)
        {
            assert(0);
        }
#endif
        if( pItem->GetID() == symID ) //startbase is the target symbol
        {
            RemovalCount += deleteSelfNbranch(pItem);// delete it
        }
        else // not us, see if we contain it
        {// check the item's child list for the item
            // we have to check the listview before we delete it from the child list or the
            //  listview won't know the symbol id
            CDDLBase* pSelection = P_TABLE_FRM->getCurrentSelection();
            int listfind = 0;
            //if ( pSelection->GetID() == pItem->GetID() )
            //{
                listfind = P_LISTVIEW->remove(symID);// will remove if it is found
            //}
            RemovalCount += pItem->deleteLeaf(symID);// list will get rid of it in the listview

             // recurse any sub branches
            if(tree.ItemHasChildren(hItem))
            {
                CDDLBase *pChildItem;
                HTREEITEM hChildItem = tree.GetChildItem(hItem);            

                while(hChildItem != NULL)
                {
                    pChildItem = (CDDLBase *) tree.GetItemData(hChildItem);
                    RemovalCount += DeleteBranchIncidence( symID, pChildItem);// recurse
                    
                    hChildItem = tree.GetNextSiblingItem(hChildItem);
                }
            }// else - no children - we're done here
        }
    }
    else
    {// we're done at a null item
        DEBUGLOG(CLOG_LOG,L"Treeview.MSG: Delete branch incidence with no tree presence.\n");
        RemovalCount = -1;
    }
    return RemovalCount;
}


int CDDIdeTreeView::GetBranchIncidence( ulong symID, 
                                        CDDLBasePtrList_t& returnList,CDDLBase * pStartBase)
{   
    CDDLBase* pItem = NULL;
    CTreeCtrl& tree = GetTreeCtrl();
    HTREEITEM hItem = pStartBase->GetTreeItem();

    if(hItem != NULL)
    {
        pItem = (CDDLBase *) tree.GetItemData(hItem);
        if( pItem->GetID() == symID ) 
        {
            returnList.AddTail(pItem);
            // a menu is not allowed to contain itself in any branch so pItem will have no incidence
        }
        else
        if(tree.ItemHasChildren(hItem))
        {
            CDDLBase *pChildItem;
            HTREEITEM hChildItem = tree.GetChildItem(hItem);            

            while(hChildItem != NULL)
            {
                pChildItem = (CDDLBase *) tree.GetItemData(hChildItem);
                GetBranchIncidence( symID, returnList, pChildItem);// curse the branch
                
                hChildItem = tree.GetNextSiblingItem(hChildItem);
            }
        }// else - no children - we're done here
    }// we're done at a null item
    return returnList.GetCount();
}
// stevev end new code 29nov05
// stevev 30nov05 - tree generation code from doc

int CDDIdeTreeView::AddRootMenu( void ) // for doc's current device
{
    CDDIdeDoc* pDoc = GetDocument();
    itemInfo_t local;
     CTableChildFrame* pTblFrm = static_cast<CTableChildFrame*>(m_pTableFrame);

    if ( pDoc == NULL || pDoc->pCurDev == NULL ) return 0;

    CTreeCtrl&  tree = GetTreeCtrl();
    
    /* CDDLMain is no longer used as tree control root menu, POB - 5/18/2014 */

    if ( pDoc->pCurDev->getItemBySymNumber(ROOT_MENU, &local.p_Ib) == SUCCESS && 
         local.p_Ib != NULL )
    {
        local.p_Ib->Label(local.labelStr);
        local.p_Ib->Help (local.help_Str);
        local.labelFilled = true;// for better or worse
        
        /*
         * The Table Frame owns its own "root" (ie, top level) menu
         * Add it here!
         */
        pTblFrm->m_pRoot = new CDDLMenu(local);

#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE MAIN  adds an item 0x%04x\n", ROOT_MENU);
#endif
        // Root menu is not a menu item, POB - 5/22/2014
        // CDDLMain is no longer used as tree control root menu, POB - 5/18/2014
        int rr = AddSubMenu(pTblFrm->m_pRoot, NULL );// insert and all under it
        
        /* Removed commented out code, POB - 5/20/2014 */

        /*  No Longer selecting and expanding the tree control here - 5/20/2014 */

        /* Removed commented out code, POB - 5/20/2014 */
        return rr;
    }
    //else -error
    return 0;
}

int CDDIdeTreeView::FillMenuList( CDDLBase* pNewItem, bool loclReview )
{
    int r = 0; // failure ( or number entered )
    CDDLBase*       pBase;
    
    CDDLMenu*       pMenu;
    CDDLString*     pString;
    CDDLImage*      pImage;
    CDDLScope*      pScope;
    CDDLGrid*       pGrid;
    CDDLVariable*   pVariable;
    CDDLEDisplay*   pEDisplay;
    CDDLMethod*     pMethod;
    hCmethodCall    MethodCall;
    bool            isColl = false;

    CDDIdeDoc* pDoc = GetDocument();

    /** 13apr11 - transfered the whole thing from AddSubMenu (so we can use it elsewhere)**/
    /** no longer just root...
    ***/

    /* Removed commented out code, POB - 5/20/2014 */

    /* 20may11 -- the tree control should do the selection for us
     **/

    /* Removed commented out code, POB - 5/20/2014 */

    pMenu = pNewItem->getMenuPtr();
    if (pMenu == NULL)// there is no menu there
    {
        return r;
    }
    pMenu->DeleteList();
    // acquire menu-item-list
    itemInfoList_t childLst;        
    pMenu->GetMenuItemList(childLst);// from hCmenu
    // gets the hCmenu list and translates it to a itemInfo_t list

    pBase = pNewItem; // this menu has to be the parent to the items about to be added
    if (pMenu != NULL  && pMenu->m_pItemBase != NULL &&
        pMenu->m_pItemBase->getTypeVal() == COLLECTION_ITYPE )// parent isa collection
    {
        isColl = true;
    }
    else
    {
        isColl = false;
    }

    itemInfo_t *pItmInfo;
    // will skip (not list) if undrawable Or Invalid
    // for each menu-item-info (itmbaseptr, label & qualifier) 
    for ( itmInfoIT_t iT = childLst.begin(); iT != childLst.end(); ++iT)
    {
        if ( pDoc->pCurDev->devState == ds_Closing )
        {// we aborted the dd
            break;
        }
        
        // generate child by type - legacy menues only have a few legal types
        pItmInfo = &(*iT);// stevev 30mar11 - stop using iterator as pointer
        // 04apr11 - constant string has an empty item id
#ifdef _DEBUG
        unsigned localID = NULL;
        if (pItmInfo->p_Ib)  localID = pItmInfo->p_Ib->getID();
#endif
    /* stevev 09apr10 - bug 3034 - subsequent errors: the call to 
        new CDDLMenu(*iT) // the parameter is a reference to itemInfo_t
        doesn't always work as expected.  Copying to a local and passing
        that refereence removed the crash */
    /* try again with vs 2008--- 30mar11 

        /* Removed commented out code, POB - 5/20/2014 */

        unsigned typeVal = 0;
        if (pItmInfo->p_Ib)
        {
            typeVal = pItmInfo->p_Ib->getTypeVal();
        }
        else // p_Ib is null
        if(pItmInfo->labelFilled)
        {
            typeVal = iT_MaxType+2;// constant string
        }
        else// no Ib, no string...empty
        {
            DEBUGLOG(CLOG_LOG,"Item in menu list that has no content.\n");
            continue;// skip this one
        }

        unsigned localQual = pItmInfo->qual;
        if (loclReview)// entry global value
        {
            if (typeVal == iT_Menu || typeVal == iT_Collection)
            {
                localQual |= REVIEW_ITEM;// be sure sub sub menues are review too
            }
            else
            {
                localQual |= READ_ONLY_ITEM;// other items will be RO
            }
        }
        if ( isColl )// when parent isa collection, we gotta force a DISPLAY_VALUE
        {
/* 27jul11 - disable the collection default DISPLAY_VALUE on tables till we get a definitive
            decision drom the DD working group
   16aug11 - I can't wait anymore. I hope they decide to go this way.
****/
            localQual |= DISPLAY_VALUE_ITEM;// 26jul11-we need to tell folks to show the values
        }

        switch (typeVal)
        {
        case iT_Menu:
        case iT_Collection:
        {// item is Menu|collection-------------                
        //see 09apr10 note above::>
        // sets HELP & as much as it can from itembase
        /* try again with vs 2008--- 30mar11
        **/
            /* Removed commented out code, POB - 5/20/2014 */
            pMenu     = new CDDLMenu( (*pItmInfo) );                
            pNewItem  = new CDDLMenuItem(pMenu, localQual);   // sets ID
            // menu can have: IsReview(), bool HasOverline();
            TRACE("  FillMenuList:Item 0x%04x Menu being AddSubMenu'd\n",pNewItem->GetID());
            //     if it's a review-menu && static Flag is NOT already review
            if ( pItmInfo->qual & REVIEW_ITEM )//spec error par 802 - stevev 22aug07
            {
                glblReview = true;  //  set static Flag- In Review
                // stevev 22aug07 par 802 -
            }
            r += AddSubMenu( pNewItem, pBase );//----------------------------- re-curse
            glblReview = loclReview;// stevev 27oct09 - 
                                        //   put it back after the review has been inserted
            // stevev 06apr11 - we always reset gbl to local...local is always entry value
        }
        break;
        case (iT_MaxType+2):
        {// item isa constant string -----------------
            pString   = new CDDLString((*pItmInfo)); // sets HELP
            pNewItem  = new CDDLMenuItem(pString, localQual);   // sets ID
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_Variable:
        {// item is Variable---------------------
            pVariable = new CDDLVariable(*pItmInfo);//new Variable(itmInfo)// sets HELP etc
            pNewItem  = new CDDLMenuItem(pVariable, localQual);// added qual 04apr11
            // now done in item constructor
            //  set parent (4 access to qualifier later)
            //     see if static Flag is review menu
            /* 04apr11 - now done in menu item constructor for all menu items
            ** end 04apr11 **/

            /* Removed commented out code, POB - 5/20/2014 */

            TRACE("  FillMenuList:Item 0x%04x Vari being AddSubMenu'd\n",pNewItem->GetID());
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_EditDisplay:
        {// item isa Edit Display-----------------
            pEDisplay = new CDDLEDisplay((*pItmInfo)); // new EdDisp(itmInfo);// sets HELP
            pNewItem  = new CDDLMenuItem(pEDisplay, localQual);
            /* Removed commented out code, POB - 5/20/2014 */
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_Method:
        {// item isa Method------------------------
         //    get Info (item ptr, type,id,no-params)
            MethodCall.m_pMeth  = (hCmethod*)(pItmInfo->p_Ib);
            MethodCall.source   = msrc_EXTERN;
            MethodCall.methodID = pItmInfo->p_Ib->getID();
            MethodCall.paramList.clear();               
            // new Method(otherInfo)
            
            /* Removed commented out code, POB - 5/20/2014 */
            pMethod  = new CDDLMethod(MethodCall,pDoc->pCurDev,*iT);
            pNewItem = new CDDLMenuItem(pMethod, localQual);
//tempIBptr = pNewItem->m_pItemBase;
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_Chart:  
        case iT_Graph:
        {// item isa Image-----------------
            pScope    = new CDDLScope((*pItmInfo)); // sets HELP
            pNewItem  = new CDDLMenuItem(pScope, localQual);   // sets ID
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_Grid:
        {// item isa Image-----------------
            pGrid     = new CDDLGrid((*pItmInfo)); // sets HELP
            pNewItem  = new CDDLMenuItem(pGrid, localQual);   // sets ID
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        case iT_Image:
        {// item isa Image-----------------
            pImage    = new CDDLImage((*pItmInfo)); // sets HELP
            pNewItem  = new CDDLMenuItem(pImage, localQual);   // sets ID
            r += AddSubMenu( pNewItem, pBase );//------------------------------- re-curse
        }
        break;
        //case iT_Template:
        case iT_Blob: // default it for now
        default:
        {
  //    LOGIT an error - invalid item type on a legacy menu
        }
        break;
        }// endswitch
#ifdef _DEBUG
        if ( typeVal == iT_Variable && (localQual & DISPLAY_VALUE_ITEM) == 0 )
        {
            LOGIT(CLOG_LOG,"No display_value on a menu entry.\n");
        }
#endif 
    }// next menu item
    /* end of complete transfer 13apr11 **/
    return r;
}

//24mar11 - pBase null means to add newitem as root
//
int CDDIdeTreeView::AddSubMenu( CDDLBase* pNewItem, CDDLBase* pBase )
{
    int r = 0; // failure ( or number entered )
    //unused   unsigned int  qualifier;;
    CTreeCtrl& tree = GetTreeCtrl();
    bool loclReview = glblReview; // stevev 22aug07 par 802 - local holds entry value of global


    CDDIdeDoc* pDoc = GetDocument();

    if ( pDoc->pCurDev->devState == ds_Closing )
    {// we aborted the dd
        return 0;
    }

    // verify good parameters
    if ( pNewItem == NULL )// we handle base differently now......24mar11...|| pBase == NULL )
    {   return 0;   }
    // insert into child-List 
#ifdef LOGBUILD
    LOGIT(CLOG_LOG,"TREE 0x%04x  adds an item 0x%04x ", pBase->GetID(),pNewItem->GetID());
    if (pNewItem->IsMenu())
        LOGIT(CLOG_LOG,"(menu)\n");
    else
        LOGIT(CLOG_LOG,"\n");
#endif
    if (pBase != NULL)
    {//     set child's parent to pBase (this),  add to ChildList
        if ( pBase->AddChild(pNewItem) )// true if success
            //          duplicate ADD functionality w/o sending msg
            AddOneItem( pNewItem );//---inserts it into the TREE 
    }
    else  // new 24mar11- null base then null newitems parent and addoneitem will put it 2 root
    {
        CDDLBase *pHoldBase = pNewItem->GetParent();
        pNewItem->SetParent( NULL );
        AddOneItem( pNewItem );//---inserts it into the TREE 
        pNewItem->SetParent( pHoldBase );// put it back in case somebody else has designs on it
    }
    
    if (pNewItem->IsMenu() )    // if it's a menu type (menu|collection)
    {
        r = FillMenuList( pNewItem, loclReview );

    }  // else - not a menu type---    we're done, no recursion required -- nop
// stevev 22aug07 par 802 - if (loclReview) // if local Flag is In Review
// stevev 22aug07 par 802 - {// we set it in this cursion
// stevev 22aug07 par 802 -glblReview = false;  //  set static Flag- NOT In Review
// stevev 22aug07 par 802 -}
#ifdef _DEBUG
    else
    {
        glblReview = loclReview;// just to get a breakpoint
    }
#endif

    glblReview = loclReview;// stevev 22aug07 par802 - put back to entry value (setter clears)

    return r;
}
// end code stevev 30nov05
