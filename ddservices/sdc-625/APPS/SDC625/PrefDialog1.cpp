// PrefDialog.cpp : implementation file
//

#include "stdafx.h"
#include "sdc625.h"
#include "PrefDialog1.h"
#include "Preferences1.h"
#include "DDLCommInfc.h"
#include <setupapi.h>
#include <devguid.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrefDialog dialog


CPrefDialog::CPrefDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPrefDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrefDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPrefDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrefDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrefDialog, CDialog)
	//{{AFX_MSG_MAP(CPrefDialog)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_BN_CLICKED(IDC_RADIO5, OnRadio5)
	ON_BN_CLICKED(IDC_RADIO6, OnRadio6)
	ON_BN_CLICKED(IDC_RADIO7, OnRadio7)
	ON_CBN_SELCHANGE(IDC_SEARCH_SERIAL_PORT, OnSelchangeSearchSerialPort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrefDialog message handlers

void CPrefDialog::OnOK() 
{
	CEdit *edit;
	CString str;
	CButton *button;
	CComboBox* combo;
	int i;				// index

	// save preferences from private comm interface object into ini file 
	CPreferences pref;

	// serial port from combo box
	combo = (CComboBox*) GetDlgItem(IDC_SEARCH_SERIAL_PORT);
	i = combo->GetCurSel();
	if (i != CB_ERR)
	{
		combo->GetLBText(i, pref.SearchSerialPort);
	}

	// search by
	combo = (CComboBox*) GetDlgItem(IDC_SEARCH_BY);
	if (combo) 
	{
		i = combo->GetCurSel();
		if (i != CB_ERR)
		{
			combo->GetLBText(i, pref.SearchBy);
		}
	}

	// search options

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA);
	edit->GetWindowText(str);
	if (CheckPollAddr(str)) 
	{
		return;
	}
	pref.SearchPollAddr = _ttoi(str);

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA1);
	edit->GetWindowText(str);
	if (CheckPollAddr(str)) 
	{
		return;
	}
	pref.SearchPollAddrStart = _ttoi(str);

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA2);
	edit->GetWindowText(str);
	if (CheckPollAddr(str)) 
	{
		return;
	}
	pref.SearchPollAddrEnd = _ttoi(str);

	if (pref.SearchPollAddrStart > pref.SearchPollAddrEnd) 
	{
		// begin user brain damage repair
		int temp = pref.SearchPollAddrStart;
		pref.SearchPollAddrStart = pref.SearchPollAddrEnd;
		pref.SearchPollAddrEnd = temp;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO1);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddr0;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO2);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchFindFirst;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO3);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddrN;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO4);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddr15;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO5);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddr31;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO6);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddr63;
	}

	button = (CButton*) GetDlgItem(IDC_RADIO7);
	if (button->GetCheck() == 1) 
	{
		pref.SearchOption = SearchPollAddrRange;
	}

	// save prefernces in ini file
	pref.Write();

	// TO DO: This needs to be a dynamic change so that "New" will implement
	// the preferences
	// Work-around:  The application needs to be restarted in order for serial port and
	// poll addresses to be used by the modem server
 
	MessageBox(M_UI_PLEASE_RESTART);

	CDialog::OnOK();
}

BOOL CPrefDialog::OnInitDialog() 
{
	CEdit *edit;
	CButton *button;
	CComboBox* combo;
	CString str;		// utility string
	int i, j;			// index
	int size;			// count
	int   /*nIdx,*/ nListIdx, nSpNum, /*nCurrSpNum,*/ nSpNumTemp = 0;
	//ULONG uDataSize;
    //ULONG uValueSize;
	TCHAR   szData[100];
    //TCHAR szValue[100];
	//LONG  lResult;
	//DWORD dwType;
	//HKEY  hKey; /* handle to registry key containing codepage info */
	//HANDLE   hPort;
	CString  strPortName;
	CString  strTemp;
    HDEVINFO hDevInfo;
    DWORD    index;

    SP_DEVINFO_DATA DeviceInfoData;
    GUID InterfaceClassGuid = GUID_DEVCLASS_PORTS;

	CDialog::OnInitDialog();

	// read preferences from ini file into a private comm interface object
	CPreferences pref;

	// serial port in combo box
	// find the drop-down value that matches the current preference
	combo = (CComboBox*) GetDlgItem(IDC_SEARCH_SERIAL_PORT);

	 /*
      * Show the current configured serial port.  
      * Note:  If port address is already assigned, it will report
      * unavailable, must update dialog here.
      */
	//_stscanf(pref.SearchSerialPort, _T("COM%u"), &nCurrSpNum);
    //pCurSerial = (CStatic*) GetDlgItem(IDC_SERIAL_PORT_STATIC);
    //pCurSerial->SetWindowText(pref.SearchSerialPort);


	/* open the key containing the serial port info */
/*	if(!RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Hardware\\DeviceMap\\SerialComm"),
				     0, KEY_QUERY_VALUE, &hKey))
	{
		for(nIdx = 0; nIdx >= 0; nIdx++)
		{
			uDataSize = sizeof(szData);
			uValueSize = sizeof(szValue) * sizeof(TCHAR);
		   
			
			lResult = RegEnumValue(hKey, nIdx, szValue, &uValueSize, NULL,
								   &dwType, (BYTE *)szData, &uDataSize);
		   
			if(ERROR_NO_MORE_ITEMS == lResult)
			{
				break;
			}*/

    // Create a HDEVINFO with all present devices.
    hDevInfo = SetupDiGetClassDevs(&InterfaceClassGuid,
                                    0, // Enumerator
                                    0,
                                    DIGCF_PRESENT/* | DIGCF_ALLCLASSES*/ );

    if (hDevInfo == INVALID_HANDLE_VALUE)
    {
        // Insert error handling here.
        return FALSE;
    }

    // Enumerate through all devices in Set.

    DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    for (index=0;SetupDiEnumDeviceInfo(hDevInfo,index,
         &DeviceInfoData);index++)
    {
        DWORD DataT;
        LPTSTR buffer = NULL;
        DWORD buffersize = 0;
        CString strDescription = "";
        CString strFriendName  = "";

        //
        // Call function with null to begin with,
        // then use the returned buffer size
        // to Alloc the buffer. Keep calling until
        // success or an unknown failure.
        //
        while (!SetupDiGetDeviceRegistryProperty(
               hDevInfo,
               &DeviceInfoData,
               SPDRP_FRIENDLYNAME,
               &DataT,
               (PBYTE)buffer,
               buffersize,
               &buffersize))
        {
            if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
            {
                // Change the buffer size.
                if (buffer)
                {
                    LocalFree(buffer);
                }

                buffer = (TCHAR *)LocalAlloc(LPTR,buffersize);
            }
            else
            {
                // Insert error handling here.
                break;
            }
        }
 
        strFriendName = buffer;

        if (strFriendName.Find(_T("COM")) >= 0)
        {
            // remove and parathesis from the string (typically there)
            strFriendName.Remove( '(' );
            strFriendName.Remove( ')' );

            int ret = strFriendName.Find(_T("COM"));

            int length = strFriendName.GetLength();

            strFriendName = strFriendName.Mid(ret,length);

            // remove any spaces (if any) at the end of the string 
            strFriendName.TrimRight();

            lstrcpy(szData, strFriendName);

            _stscanf( szData, _T("COM%u"), &nSpNum );

            // Show all availabe serial ports in combo box (even if they are in use)
            // Determine if this serial port string name already exist in the combo box list
			if (combo->FindStringExact(-1, szData) == LB_ERR)
			{
				// The combo box does not have this serial port name
				// Add it here
				nListIdx = combo->AddString(szData);
				combo->SetItemData(nListIdx, nSpNum);
			}

        /*    else // new functionality for com ports that exist but are currently in use
            {
                strFriendName = strFriendName + " (in use)";
                // Determine if this serial port string name already exist in the combo box list
				if (combo->FindStringExact(-1, strFriendName) == LB_ERR)
				{
					// The combo box does not have this serial port name
					// Add it here
					nListIdx = combo->AddString(strFriendName);
					combo->SetItemData(nListIdx, nSpNum);
				}
            }*/
        }

        if (buffer)
        {
            LocalFree(buffer);
        }
    }// next

    if ( GetLastError()!=NO_ERROR &&
         GetLastError()!=ERROR_NO_MORE_ITEMS )
    {
        // Insert error handling here.
        return FALSE;
    }

    //  Cleanup
    SetupDiDestroyDeviceInfoList(hDevInfo);

/*			_stscanf(szData, _T("COM%u"), &nSpNum);

			// 
			// Don't display serial port if it is not a valid serial port or
			// it is currently being used by another deivce 
			//
			strPortName.Format("\\\\.\\Com%d", nSpNum);

			hPort = CreateFile(strPortName, GENERIC_READ | GENERIC_WRITE, 
				               0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if( hPort == INVALID_HANDLE_VALUE && nSpNum == nCurrSpNum ||
				hPort != INVALID_HANDLE_VALUE                          )
			{
				// Determine if this serial port string name already exist in the combo box list
				if (combo->FindStringExact(-1, szData) == LB_ERR)
				{
					// The combo box does not have this serial port name
					// Add it here
					nListIdx = combo->AddString(szData);
					combo->SetItemData(nListIdx, nSpNum);
				}
			}

			if( hPort != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hPort);
			}
		}

		RegCloseKey( hKey );
	}*/

	size = combo->GetCount();
 
	// Now, sort by serial port number
	// instead of sorting by text name
	for (i = 0; i < size; i++)
	{
		for(j = 0; j < size - 1; j++)
		{
			// found matching item, set combo selection
			if (combo->GetItemData(j) > combo->GetItemData(j+1))
			{
				// Swap the strings

				// Save the info at this index
				nSpNumTemp = combo->GetItemData(j);
				combo->GetLBText(j, strTemp);

				// move the string at index at j+1 up 
				// be deleting the string we just saved
				combo->DeleteString(j);

				// now insert the one we saved after the 
				// the one we just moved up on the list
				combo->InsertString(j+1, strTemp);
				combo->SetItemData(j+1, nSpNumTemp);
			}
		}
	}
	
	// serial port in combo box
	// find the drop-down value that matches the current preference
	for (i = 0; i < size; i++)
	{
		CString str;
		combo->GetLBText(i, str);
		if (str == pref.SearchSerialPort) 
		{
			// found matching item, set combo selection
			combo->SetCurSel(i);
			break;
		}
	}

    // Determine if the current serial port is in use (it should be) or
    // unavailable (which it would if it was a removed USB modem port)
    OnSelchangeSearchSerialPort();

	// search by
	combo = (CComboBox*) GetDlgItem(IDC_SEARCH_BY);
	if (combo) 
	{
		combo->SetCurSel(0);		// Poll Address
		combo->EnableWindow(FALSE);	// other options not available for now
	}

	// search options

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA);
	str.Format(_T("%d"), pref.SearchPollAddr);
	edit->SetWindowText(str);

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA1);
	str.Format(_T("%d"), pref.SearchPollAddrStart);
	edit->SetWindowText(str);

	edit = (CEdit*) GetDlgItem(IDC_EDIT_PA2);
	str.Format(_T("%d"), pref.SearchPollAddrEnd);
	edit->SetWindowText(str);


	switch (pref.SearchOption) {
		case SearchPollAddr0: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO1);
				button->SetCheck(1);
				break;
			}

		case SearchFindFirst: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO2);
				button->SetCheck(1);
				break;
			}

		case SearchPollAddrN: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO3);
				button->SetCheck(1);
				break;
			}

		case SearchPollAddr15: 
				button = (CButton*) GetDlgItem(IDC_RADIO4);
				button->SetCheck(1);
			{
				break;
			}

		case SearchPollAddr31: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO5);
				button->SetCheck(1);
				break;
			}

		case SearchPollAddr63: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO6);
				button->SetCheck(1);
				break;
			}

		case SearchPollAddrRange: 
			{
				button = (CButton*) GetDlgItem(IDC_RADIO7);
				button->SetCheck(1);
				break;
			}

	}  // end switch

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrefDialog::OnRadio1() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio2() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio3() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio4() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio5() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio6() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrefDialog::OnRadio7() 
{
	// TODO: Add your control notification handler code here
	
}

bool CPrefDialog::CheckPollAddr(CString &pa)
{
	// check for well-formed poll address
	// return true on error

	CString errmsg;
	errmsg.Format(M_UI_POLLING_RNG, DOCCOMMMAXPOLLADDR - 1);

	pa.TrimLeft();
	pa.TrimRight();
	int i;// PAW 03/03/09
	for (/*int PAW 03/03/09*/i = 0; i < pa.GetLength(); i++)
	{
		if (pa[i] < _T('0') ||  pa[i] > _T('9'))
		{
			AfxMessageBox(errmsg);
			return true;
		}
	}

	i = _ttoi(pa);
	if (i < 0 || i >= DOCCOMMMAXPOLLADDR) 
	{
		AfxMessageBox(errmsg);
		return true;
	}

	return false;
}

void CPrefDialog::OnSelchangeSearchSerialPort() 
{
	// TODO: Add your control notification handler code here

    CComboBox * pCombo;
    CStatic * pCurSerial;
    CString strCurSelect, strPortName;
    HANDLE   hPort = INVALID_HANDLE_VALUE;

    pCurSerial = (CStatic*) GetDlgItem(IDC_SERIAL_PORT_STATIC);
    pCombo = (CComboBox*)   GetDlgItem(IDC_SEARCH_SERIAL_PORT);

    int index = pCombo->GetCurSel();

	if ( index >= 0 )
	{
		// Get string name from the combo box that the user just selected
		pCombo->GetLBText(index, strCurSelect);

		pCurSerial->SetWindowText(strCurSelect);

		strPortName = "\\\\.\\" + strCurSelect;

		hPort = CreateFile(strPortName, GENERIC_READ | GENERIC_WRITE, 
						   0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	}
    // If the serial port is no longer available (ie removed USB port) and the user
    // had previously selected it in the preferences, show that it is unavalable here.
	if( hPort == INVALID_HANDLE_VALUE /* replaced below... && strCurSelect == "" ) */
		&& index < 0 )
	{
        // read preferences from ini file into a private comm interface object
	    CPreferences pref;

        strCurSelect = pref.SearchSerialPort + " (unavailable)";

	}
    else if ( hPort == INVALID_HANDLE_VALUE )
    {
        strCurSelect = "(in use)";
    }
    else
    {
        strCurSelect = "(available)";
    }

    pCurSerial->SetWindowText(strCurSelect);

	if( hPort != INVALID_HANDLE_VALUE )
	{
		CloseHandle(hPort);
	}
	
}
