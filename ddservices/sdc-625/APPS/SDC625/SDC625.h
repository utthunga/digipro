/*************************************************************************************************
 *
 * $Workfile: SDC625.h $
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		main header file for the DDIDE application
 * #include "SDC625.h"	
 */

#if !defined(AFX_DDIDE_H__B8CFB53D_0D14_4CA2_8602_8A34C5348DED__INCLUDED_)
#define AFX_DDIDE_H__B8CFB53D_0D14_4CA2_8602_8A34C5348DED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "SDMV_Template.h"
#include "CmdLineInfo.h"

#include "ddbGeneral.h"

/***************************************************************************
 * Global Definitions
 ***************************************************************************/

/*
 * Shared by the app and the list view
 */
#define SETTINGS              _T("Settings")
#define COL_WIDTHS            _T("Column Widths")
#define MAIN_APP_WND_SIZE     _T("Window Rect Size")
#define SPLITTER_BAR          _T("Splitter Bar Loc")
#define MAX_REG_STRLEN        32
#define DFLT_WINDOW_SIZE      _T("110|110|750|590|")

#define WINDOW_UPDATE_RATE    2000  /* every 2 seconds */

#define CONST_STRING_FONT	_T("Microsoft Sans Serif")		/*_T("MS Sans Serif")*/
#define CONST_STRING_HIGT	8                   /* was 15  */


// Moved to (included) cmdlineinfo.h
// timj begin 04/05/04  default polladdr is -1 so that we can detect no -p on cmd line
//#define	EMPTY_POLL_ADDR			0xff
// timj end 04/05/04

/***************************************************************************
 * Global Data Types
 ***************************************************************************/

/***************************************************************************
 * Global Variables
 ***************************************************************************/

/////////////////////////////////////////////////////////////////////////////
// CDDIdeApp:
// See DDIde.cpp for the implementation of this class
//

class CDDIdeApp : public CWinApp
{
	static unsigned long nextID;
	static unsigned long firstID;
public:
    CDDIdeApp();
	virtual
		~CDDIdeApp();
#ifdef ENUM_ALLOC_DEBUG
	int Run();
#endif
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDDIdeApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL


// Implementation - Methods
	int handleOptions(CString& rStr);
	cmdLineInfo_t& getCmdLineInfo(void) {return m_CmdLine;};
	unsigned long getUniqueID(void); // a global service
	bool isUniqueID(unsigned long id); // a global service
// Implementation - Data
	cmdLineInfo_t      m_CmdLine;
//	COleTemplateServer m_server;		// Server object for document creation
	CMultiDocTemplate* m_pWindowTemplate;
	CMultiDocTemplate* m_pTableTemplate;
	CSDMVTemplate* m_pDocTemplate;

	CFrameTemplate* perrorlogtemplate;

	/*CDDIdeDoc  void *        m_pDoc;*/
	/*CDDIdeDoc*/void *        pDoc;

	//{{AFX_MSG(CDDIdeApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	afx_msg void OnFileOpen();
	afx_msg void OnFilePreferences();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	//afx_msg void OnViewPV();
	//afx_msg void OnViewPVAO();
	//afx_msg void OnViewPVURV();
};

extern CDDIdeApp theApp;// the program global application accessor

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DDIDE_H__B8CFB53D_0D14_4CA2_8602_8A34C5348DED__INCLUDED_)

/*************************************************************************************************
 * NOTES:
 *************************************************************************************************
 */
/*************************************************************************************************
 *
 *   $History: DDIde.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 5/05/03    Time: 11:18a
 * Updated in $/DD Tools/DDB/Apps/DDIde
 * Added/Verified that all files had the HART standard HEader and Footer
 * Blocks
 * 
 *************************************************************************************************
 */
